<%-- 
    Document   : home
    Created on : Sep 26, 2017, 2:21:02 AM
    Author     : ravi
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <script src='dwr/interface/TcAction.js'></script>
        <script src='dwr/engine.js'></script>
        <script src='dwr/util.js'></script>  
        <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
        <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Automation Reports</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
    </head>
    <body>
        <style>
            td,th
            {
                width:100px; 
                /*word-wrap:break-word;*/
            }
        </style>
        <script>
            bodyonload();
            function bodyonload()
            {
                TcAction.getAllModule(function getAllModule(map) {

                    dwr.util.removeAllOptions("module");
                    dwr.util.addOptions("module", map)
                });
            }
            function getresult()
            {
				document.getElementById('result').style.display = 'none';
                var moduleis = document.getElementById('module').value;
                TcAction.getModuleDetails(moduleis, {callback: function (data) {
                        document.getElementById("Details").innerHTML = data;
                    }
                });
				document.getElementById('result').style.display = '';
            }
            function delresult()
            {
                var moduleis = document.getElementById('module').value;
                TcAction.delModuleDetails(moduleis, {callback: function (data) {
                        if (data == "delete successfully")
                        {
                            document.forms[0].action = "TcAction.htm";
                            document.forms[0].method.value = "homepage";
                            document.forms[0].submit();
                        } else
                            alert("hello");
                    }
                });
            }
            function sbm()
            {
                alert("hello");
                document.forms[0].action = "TcAction.htm";
                document.forms[0].method.value = "detail";
                document.forms[0].submit();
            }


        </script>
        <form method="post">
           
            <div>

                <table>
                    <tr>
                        <td><select id="module" name="module" /></td>
                        <td><input type="button" name="result" id="result" value="click to get result" onclick="getresult()"/></td>
                    </tr>
                </table>

            </div>
            <div id="Details" ></div> 


            <input type="hidden" name="method" id="method"/>
        </form>
    </body>
</html>
