package browserhandling;

import java.net.URL;
import java.util.HashMap;
import java.util.Map;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.HttpCommandExecutor;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.remote.SessionId;

public class Browser {

	private static Map<Long, SessionId> a1 = new HashMap<Long, SessionId>();
	private static Map<SessionId, URL> b1 = new HashMap<SessionId, URL>();

	public WebDriver getBrowser(Long threadId) throws Exception {
		WebDriver driver;
		SessionId s = getInstanceOfBrowser(threadId);
		URL u = null;

		if (s != null) {
			u = b1.get(s);
			BrowserProperty bp = new BrowserProperty();
			driver = bp.createDriverFromSession(s, u);
		} else {
			WebDriverClass d = new WebDriverClass();
			driver = d.openBrowser();

			HttpCommandExecutor executor = (HttpCommandExecutor) ((RemoteWebDriver) driver).getCommandExecutor();
			u = executor.getAddressOfRemoteServer();
			s = ((RemoteWebDriver) driver).getSessionId();

			b1.put(s, u);

			a1.put(threadId, s);
		}

		return driver;
	}

	private SessionId getInstanceOfBrowser(Long threadId) {
		SessionId sessionid;
		sessionid = a1.get(threadId);
		if (sessionid == null) {
			return null;
		} else {
			return sessionid;
		}
	}

}
