package browserhandling;

import org.openqa.selenium.WebDriver;
import org.testng.Reporter;
import org.testng.annotations.Test;

class ClassA {
	int y = 5;

	public ClassA(int y) {
		this.y = y;
		System.out.println("ClassA" + y);
	}

	@Test(groups = "testbrowser")
	public void openGmail() throws Exception {
		Browser b = new Browser();
		WebDriver driver = b.getBrowser(Thread.currentThread().getId());
		driver.get("http://www.gmail.com");
		Reporter.log(driver.getCurrentUrl());
		Thread.sleep(10000);

		long thread_id = Thread.currentThread().getId();
		System.out.println(thread_id);

	}

	@Test(groups = "testbrowser")
	public void openGoogle() throws Exception {
		Browser b = new Browser();
		WebDriver driver = b.getBrowser(Thread.currentThread().getId());
		driver.get("http://www.google.com");
		Reporter.log(driver.getCurrentUrl());
		Thread.sleep(10000);

		long thread_id = Thread.currentThread().getId();
		System.out.println(thread_id);

	}

	@Test(groups = "testbrowser")
	public void openyahoo() throws Exception {
		Browser b = new Browser();
		WebDriver driver = b.getBrowser(Thread.currentThread().getId());
		driver.get("http://www.yahoo.com");
		Reporter.log(driver.getCurrentUrl());
		Thread.sleep(10000);

		long thread_id = Thread.currentThread().getId();
		System.out.println(thread_id);
	}

	@Test(groups = "testbbrowserInfo.get(t)rowser")
	public void openyoutube() throws Exception {
		Browser b = new Browser();
		WebDriver driver = b.getBrowser(Thread.currentThread().getId());
		driver.get("http://www.youtube.com");
		Reporter.log(driver.getCurrentUrl());
		Thread.sleep(10000);

		long thread_id = Thread.currentThread().getId();
		System.out.println(thread_id);

	}

}
