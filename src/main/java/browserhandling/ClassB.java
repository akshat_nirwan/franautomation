package browserhandling;

import org.openqa.selenium.WebDriver;
import org.testng.Reporter;
import org.testng.annotations.Test;

class ClassB extends ClassA {

	private int x = 10;

	public ClassB(int x) {
		super(x);
		this.x = 25;
		System.out.println("ClassB" + this.x);
	}

	WebDriver driver;
	Browser b = new Browser();

	@Test(groups = "testbrowser")
	public void openfacebook() throws Exception {

		System.out.println(Thread.currentThread().getId());
		WebDriver driver = b.getBrowser(Thread.currentThread().getId());

		driver.get("http://www.facebook.com");
		Reporter.log(driver.getCurrentUrl());
		Thread.sleep(10000);
		Reporter.log("" + Thread.currentThread());
		Reporter.log(driver.getWindowHandle());
	}

	@Test(groups = "testbrowser")
	public void openSeleniumhq() throws Exception {
		System.out.println(Thread.currentThread().getId());
		WebDriver driver = b.getBrowser(Thread.currentThread().getId());
		driver.get("http://docs.seleniumhq.org/");
		Reporter.log(driver.getCurrentUrl());
		Thread.sleep(10000);
		Reporter.log("" + Thread.currentThread());
		Reporter.log(driver.getWindowHandle());
	}

	@Test(groups = "testbrowser")
	public void openABCGo() throws Exception {
		System.out.println(Thread.currentThread().getId());
		WebDriver driver = b.getBrowser(Thread.currentThread().getId());
		driver.get("http://abc.go.com/");
		Reporter.log(driver.getCurrentUrl());
		Thread.sleep(10000);
		Reporter.log("" + Thread.currentThread());
		Reporter.log(driver.getWindowHandle());
	}

	@Test(dependsOnGroups = {}, alwaysRun = true)
	public void closeAllBrowser() {
		System.out.println("Ater group test cases");
	}

}
