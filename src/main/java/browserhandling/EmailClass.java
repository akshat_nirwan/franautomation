package browserhandling;

import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.internet.MimeMultipart;

import com.builds.utilities.BaseUtil;

public class EmailClass {

	public static void main(String[] arg) throws Exception {

		BaseUtil util = new BaseUtil();
		util.readMailBox("subject", "messagebody", "salesautomation@staffex.com", "sdg@1a@Hfs");

		/*
		 * String host = "mail.franconnect.com"; String user =
		 * "salesautomation@staffex.com"; String password = "sdg@1a@Hfs";
		 * 
		 * Properties properties = System.getProperties(); Session session =
		 * Session.getDefaultInstance(properties); Store store =
		 * session.getStore("pop3"); store.connect(host, user, password); Folder
		 * inbox = store.getFolder("Inbox"); inbox.open(Folder.READ_ONLY);
		 * 
		 * Message[] messages = inbox.getMessages();
		 * 
		 * if (messages.length == 0) System.out.println("No messages found.");
		 * 
		 * for (int i = 0; i < messages.length; i++) { // stop after listing ten
		 * messages if (i > 10) { System.exit(0); inbox.close(true);
		 * store.close(); }
		 * 
		 * System.out.println("Message " + (i + 1)); System.out.println(
		 * "From : " + messages[i].getFrom()[0]); System.out.println(
		 * "Subject : " + messages[i].getSubject()); System.out.println(
		 * "Sent Date : " + messages[i].getSentDate());
		 * System.out.println(getTextFromMessage(messages[i]));
		 * 
		 * }
		 * 
		 * inbox.close(true); store.close();
		 */

	}

	private static String getTextFromMessage(Message message) throws Exception {
		String result = "";
		if (message.isMimeType("text/plain")) {
			result = message.getContent().toString();
		} else if (message.isMimeType("multipart/*")) {
			MimeMultipart mimeMultipart = (MimeMultipart) message.getContent();
			result = getTextFromMimeMultipart(mimeMultipart);
		}
		return result;
	}

	private static String getTextFromMimeMultipart(MimeMultipart mimeMultipart) throws Exception {
		String result = "";
		int count = mimeMultipart.getCount();
		for (int i = 0; i < count; i++) {
			BodyPart bodyPart = mimeMultipart.getBodyPart(i);
			if (bodyPart.isMimeType("text/plain")) {
				result = result + "\n" + bodyPart.getContent();
				break; // without break same text appears twice in my tests
			} else if (bodyPart.isMimeType("text/html")) {
				String html = (String) bodyPart.getContent();
				result = result + "\n" + org.jsoup.Jsoup.parse(html).text();
			} else if (bodyPart.getContent() instanceof MimeMultipart) {
				result = result + getTextFromMimeMultipart((MimeMultipart) bodyPart.getContent());
			}
		}
		return result;
	}
}
