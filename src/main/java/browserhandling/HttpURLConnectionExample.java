package browserhandling;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

public class HttpURLConnectionExample {

	private static final String USER_AGENT = "Mozilla/5.0";

	private static final String POST_URL = "https://qa.franconnectqa.net/fc/rest/dataservices/query";

	private static final String POST_PARAMS = "userName=FRANC0NN3CT_API";

	public static void main(String[] args) throws IOException {
		sendPOST();
		System.out.println("POST DONE");
	}

	private static void sendPOST() throws IOException {
		URL obj = new URL(POST_URL);
		HttpURLConnection con = (HttpURLConnection) obj.openConnection();
		con.setRequestMethod("POST");
		con.setRequestProperty("User-Agent", USER_AGENT);

		String clientCode = "clientCode=FRANC0NN3CT_API&&key=pHNsc6x5mBksJDfo&&fromWhere=jsp&&responseType=xml&&module=fs&&subModule=lead&&operation=retrieve&&isISODate=no&&roleTypeForQuery=-1";

		//
		// For POST only - START
		con.setDoOutput(true);
		OutputStream os = con.getOutputStream();
		os.write(clientCode.getBytes());
		os.write(POST_PARAMS.getBytes());

		os.flush();
		os.close();
		// For POST only - END

		int responseCode = con.getResponseCode();
		System.out.println("POST Response Code :: " + responseCode);

		if (responseCode == HttpURLConnection.HTTP_OK) { // success
			BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
			String inputLine;
			StringBuffer response = new StringBuffer();

			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}
			in.close();

			// print result
			System.out.println(response.toString());
		} else {
			System.out.println("POST request not worked");
		}

		if (responseCode == HttpURLConnection.HTTP_OK) { // success
		} else {
			System.out.println("POST request not worked");
		}

	}

}
