package browserhandling;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Reporter;
import org.testng.annotations.Test;

public class Login {
	@Test
	public void login()
	{
		System.setProperty("webdriver.chrome.driver","C:\\Selenium_Test_Input\\exe\\chromedriver.exe");
		WebDriver driver=new ChromeDriver();
		driver.get("https://qa.franconnectqa.net/fc/");
		driver.manage().window().maximize();
		driver.findElement(By.id("user_id")).sendKeys("adm");
		driver.findElement(By.id("password")).sendKeys("t0n1ght");
		driver.findElement(By.id("ulogin")).click();
		// We need to verify if page is loaded
		List<WebElement> dropdown=driver.findElements(By.id("dropdown"));
		if(dropdown.size()>0)
		{
			System.out.println("Logged in Successfully");
			Reporter.log("Logged in Successfully");
		}
		
	}
	
}
