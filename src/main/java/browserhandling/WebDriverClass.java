package browserhandling;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.Test;

public class WebDriverClass {

	@Test
	public WebDriver openBrowser() throws Exception {
		/*
		 * URL url = new URL("http://192.168.8.199:4444/wd/hub");
		 * 
		 * DesiredCapabilities capabilities = DesiredCapabilities.chrome();
		 * RemoteWebDriver driver=new RemoteWebDriver(url, capabilities);
		 */

		System.setProperty("webdriver.chrome.driver", "C:\\Selenium_Test_Input\\exe\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		return driver;
	}
}
