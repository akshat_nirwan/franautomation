package com.builds.generics;

import java.util.Calendar;
import java.util.Date;
import java.util.Properties;

import javax.mail.Flags;
import javax.mail.Folder;
import javax.mail.Message;
import javax.mail.Session;
import javax.mail.Store;

import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

public class MailsDelete {

	
	public Store mailconnect(String mailId, String password) throws Exception
	{
		String hostName = "mail2.staffex.com";
		String userName = mailId;
		String provider = "pop3";
		Properties props = new Properties();

		// Create Session
		Session session = Session.getDefaultInstance(props, null);
		Store store = session.getStore(provider);
		store.connect(hostName, userName, password);
		return store;
	}
	
	@Parameters({ "mailId", "password" })
	@Test
	public void deleteMails(@Optional String mailId, @Optional String password) throws Exception {
		Store store = mailconnect(mailId, password);
		
		Folder emailFolder = store.getFolder("INBOX");
		emailFolder.open(Folder.READ_WRITE);

		Message[] messages = emailFolder.getMessages();
		System.out.println(mailId + " : messages.length : " + messages.length);
		int count = 0;

		try {
			if(messages.length>=250)
			{
				for (int i = 0; i < 1000 && i < messages.length; i++) {
	
					Message message = messages[i];
					Calendar cal = Calendar.getInstance();
					cal.add(Calendar.DAY_OF_MONTH, -1);
					Date minDate = new Date(cal.getTimeInMillis()); 
					if (message.getSentDate() != null && message.getSentDate().before(minDate)) {
						System.out.println("count : " + count++);
						String ans = "Y";
						if ("Y".equals(ans) || "y".equals(ans)) {
							message.setFlag(Flags.Flag.DELETED, true);
						} else if ("n".equals(ans)) {
							break;
						}
					}
					messages = emailFolder.getMessages();
				}
			}
		} catch (Exception e) {
			emailFolder.close(true);
			store.close();
		}finally {
			emailFolder.close(true);
			store.close();
		}
	}
}
