package com.builds.generics;

import java.util.ArrayList;
import java.util.List;

import org.testng.TestNG;

public class RunTestCase {

	public static void main(String[] args) {
		TestNG runner = new TestNG();

		// Create a list of String
		List<String> suitefiles = new ArrayList<String>();
		String current = System.getProperty("user.dir");
		// Add xml file which you have to execute
		suitefiles.add(current + "//StartTestRavi.xml");

		// now set xml file for execution
		runner.setTestSuites(suitefiles);

		// finally execute the runner using run method
		runner.run();

	}

}
