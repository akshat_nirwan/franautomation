package com.builds.reporting;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class DBRelated {
	static String DB_URL = "jdbc:mysql://192.168.8.199/FCAUTOMATION";
	static String user = "root";
	static String password = "root";

	public static void main(String args[]) {
		System.out.println();
	}

	public void InsertIntoDB(String test_WorkFlow_Id, String description, String testStatus, String Module)
			throws SQLException {
		Connection conn = null;
		PreparedStatement ps = null;
		PreparedStatement preparedStatement = null;
		try {

			conn = DriverManager.getConnection(DB_URL, user, password);
			conn.setAutoCommit(false);
			String sql = "INSERT INTO TC_DESC VALUES('" + test_WorkFlow_Id + "','" + description + "')";
			System.out.println(sql);
			ps = conn.prepareStatement(sql);
			try {
				ps.executeUpdate();
				conn.commit();
			} catch (Exception ex) {
				conn.rollback();
				System.out.println("Duplicate entry of test data");
			}
			Date date = new Date();
			SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
			String currentdate = format.format(date);
			Calendar cal = Calendar.getInstance();
			format = new SimpleDateFormat("HH:mm");
			String timeis = format.format(cal.getTime());
			sql = "INSERT INTO TC_STATUS(DATE,STATUS,MODULE,TC_ID,time) VALUES('" + currentdate + "','" + testStatus
					+ "','" + Module + "','" + test_WorkFlow_Id + "','" + timeis + "')";
			System.out.println("Sql is : " + sql);
			preparedStatement = conn.prepareStatement(sql);
			preparedStatement.executeUpdate();
			conn.commit();
		} catch (Exception e) {
			conn.rollback();
			e.printStackTrace();
		} finally {
			preparedStatement.close();
			ps.close();
			conn.close();
		}

	}
}
