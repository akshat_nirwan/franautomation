package com.builds.reporting;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.Date;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import org.testng.Reporter;

import com.builds.utilities.FranconnectUtil;

public class EmailTemplate {

	public String readEmailFormat() throws FileNotFoundException {
		File file = null;
		if (System.getProperty("os.name").toLowerCase().contains("win"))

		{
			file = new File(FranconnectUtil.config.get("inputDirectory") + "\\template.html");
		} else if (System.getProperty("os.name").toLowerCase().contains("lin")) {
			file = new File(FranconnectUtil.config.get("inputDirectory") + "//template.html");
		}

		String mailBody = "";
		try {
			BufferedReader reader = new BufferedReader(new FileReader(file.getAbsoluteFile()));
			String line;
			while ((line = reader.readLine()) != null) {
				mailBody = mailBody + line;
			}
			reader.close();
			return mailBody;
		} catch (Exception e) {
			System.err.format("Exception occurred trying to read '%s'.", file.getAbsoluteFile());
			e.printStackTrace();
			return null;
		}
	}

	public void sendEmail(String SMTP_HOST_NAME, String SMTP_PORT, String mailSubject, String mailBody, String mailTo,
			String mailCC, String mailBCC, String mailFrom, String replyTo, String fileName) throws Exception {

		// boolean emailSend = true;

		Properties props = new Properties();

		props.put("mail.smtp.host", SMTP_HOST_NAME);
		props.put("mail.smtp.auth", "false");
		props.put("mail.debug", "true");
		props.put("mail.smtp.port", SMTP_PORT);
		props.put("mail.debug", "false");

		Session session = Session.getInstance(props);
		Message message = new MimeMessage(session);

		try {
			message.setFrom(new InternetAddress(mailFrom));

			try {
				message.setReplyTo(new InternetAddress[] { new InternetAddress(replyTo) });
			} catch (Exception e) {
				Reporter.log(e.getMessage());
			}

			if (mailTo != null && mailTo.length() > 0) {
				if (mailTo.contains(",")) {
					String[] email = mailTo.split(",");

					InternetAddress[] toAddresses = new InternetAddress[email.length];

					int counter = 0;
					for (String recipient : email) {
						toAddresses[counter] = new InternetAddress(recipient.trim());
						counter++;
					}
					message.setRecipients(Message.RecipientType.TO, toAddresses);
				} else {
					InternetAddress[] toAddresses = { new InternetAddress(mailTo) };
					message.setRecipients(Message.RecipientType.TO, toAddresses);
				}
			}

			if (mailCC != null && mailCC.length() > 0) {
				if (mailCC.contains(",")) {
					String[] emailcc = mailCC.split(",");

					InternetAddress[] ccAddresses = new InternetAddress[emailcc.length];

					int counter = 0;
					for (String recipient : emailcc) {
						ccAddresses[counter] = new InternetAddress(recipient.trim());
						counter++;
					}
					message.setRecipients(Message.RecipientType.CC, ccAddresses);
				} else {
					InternetAddress[] toAddresses = { new InternetAddress(mailCC) };
					message.setRecipients(Message.RecipientType.CC, toAddresses);
				}
			}

			if (mailBCC != null && mailBCC.length() > 0) {
				if (mailBCC != null && mailBCC.contains(",")) {
					String[] emailBcc = mailBCC.split(",");

					InternetAddress[] bccAddresses = new InternetAddress[emailBcc.length];

					int counter = 0;
					for (String recipient : emailBcc) {
						bccAddresses[counter] = new InternetAddress(recipient.trim());
						counter++;
					}
					message.setRecipients(Message.RecipientType.BCC, bccAddresses);
				} else {
					InternetAddress[] toAddresses = { new InternetAddress(mailBCC) };
					message.setRecipients(Message.RecipientType.BCC, toAddresses);
				}
			}
			message.setSubject(mailSubject);
			message.setSentDate(new Date());
			if (mailBody == null) {
				mailBody = "";
			}

			// creates message part
			MimeBodyPart messageBodyPart = new MimeBodyPart();
			messageBodyPart.setContent(mailBody, "text/html");

			// creates multi-part
			Multipart multipart = new MimeMultipart();
			multipart.addBodyPart(messageBodyPart);

			// Adding Attachment

			String[] files = null;
			if (fileName != null && fileName.length() > 0 && fileName.contains(",")) {
				files = fileName.split(",");
			}

			if (files != null && files.length > 1) {
				MimeBodyPart attachPart = null;
				for (String f : files) {
					File file = new File(f);
					if (file.exists()) {
						attachPart = new MimeBodyPart();
						fileName = file.getAbsolutePath();
						attachPart.attachFile(fileName);
						multipart.addBodyPart(attachPart);
					}
				}
			} else {
				if (fileName != null && fileName.length() > 0) {
					File file = new File(fileName);
					fileName = null;

					MimeBodyPart attachPart = new MimeBodyPart();
					if (file.exists() == true) {
						fileName = file.getAbsolutePath();
						attachPart.attachFile(fileName);
						multipart.addBodyPart(attachPart);
					}
				}
			}

			try {
				message.setContent(multipart);
				Transport.send(message);
			} catch (Exception e) {
				Reporter.log(e.getMessage());
			}
		} catch (Exception e2) {
			Reporter.log(e2.toString());
			e2.printStackTrace();
		}
	}

}
