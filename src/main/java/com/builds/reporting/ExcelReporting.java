package com.builds.reporting;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.BorderStyle;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.testng.annotations.Test;

public class ExcelReporting {
	@Test
	public String createExcelFile(String filePath) throws IOException {
		File file = new File(filePath);
		if (file.exists() == false) {
			FileOutputStream fout = new FileOutputStream(file);
			Workbook workbook = new HSSFWorkbook();


			createSheet(workbook,"Reports");
			createSheet(workbook,"Detailed Logs");
			
			workbook.write(fout);
			workbook.close();
			fout.close();
			fout.flush();
		}
		return file.getAbsolutePath();
	}
	
	private void createSheet(Workbook workbook, String sheetName)
	{
		
		CellStyle style = workbook.createCellStyle();
		style.setFillForegroundColor(HSSFColor.GREY_25_PERCENT.index);
		style.setFillPattern(FillPatternType.SOLID_FOREGROUND);
		style.setBorderBottom(BorderStyle.THIN);
		style.setBorderRight(BorderStyle.THIN);

		Font font = workbook.createFont();

		font.setBold(true);
		font.setFontName("Times New Roman");
		font.setFontHeightInPoints((short) 11);

		style.setFont(font);
		Sheet worksheet = workbook.createSheet(sheetName);
	
		Row rHeader = worksheet.createRow(0);

		Cell suite_Name = rHeader.createCell(0);
		suite_Name.setCellValue("Suite Name");
		suite_Name.setCellStyle(style);

		Cell suiteNameVal = rHeader.createCell(1);
		suiteNameVal.setCellValue("");
		suiteNameVal.setCellStyle(style);

		Cell system_IP = rHeader.createCell(2);
		system_IP.setCellValue("System IP");
		system_IP.setCellStyle(style);

		Cell systemIPVal = rHeader.createCell(3);
		systemIPVal.setCellValue("");
		systemIPVal.setCellStyle(style);

		rHeader = worksheet.createRow(1);

		Cell cellModules = rHeader.createCell(0);
		cellModules.setCellValue("Modules");
		cellModules.setCellStyle(style);

		Cell cellModulesVal = rHeader.createCell(1);
		cellModulesVal.setCellValue("");
		cellModulesVal.setCellStyle(style);

		Cell totalTime = rHeader.createCell(2);
		totalTime.setCellValue("Total time taken (mins)");
		totalTime.setCellStyle(style);

		Cell totalTimeVal = rHeader.createCell(3);
		totalTimeVal.setCellValue("");
		totalTimeVal.setCellStyle(style);

		rHeader = worksheet.createRow(2);

		Cell os = rHeader.createCell(0);
		os.setCellValue("OS");
		os.setCellStyle(style);

		Cell osVal = rHeader.createCell(1);
		osVal.setCellValue("");
		osVal.setCellStyle(style);

		Cell totalTC = rHeader.createCell(2);
		totalTC.setCellValue("Total number of test cases");
		totalTC.setCellStyle(style);

		Cell totalTCVal = rHeader.createCell(3);
		totalTCVal.setCellValue("");
		totalTCVal.setCellStyle(style);

		rHeader = worksheet.createRow(3);

		Cell Browser = rHeader.createCell(0);
		Browser.setCellValue("Browser");
		Browser.setCellStyle(style);

		Cell BrowserVal = rHeader.createCell(1);
		BrowserVal.setCellValue("");
		BrowserVal.setCellStyle(style);

		Cell tcPassed = rHeader.createCell(2);
		tcPassed.setCellValue("Test Cases Passed");
		tcPassed.setCellStyle(style);

		Cell tcPassedVal = rHeader.createCell(3);
		tcPassedVal.setCellValue("");
		tcPassedVal.setCellStyle(style);

		rHeader = worksheet.createRow(4);

		Cell codeBranch = rHeader.createCell(0);
		codeBranch.setCellValue("Code branch");
		codeBranch.setCellStyle(style);

		Cell codeBranchVal = rHeader.createCell(1);
		codeBranchVal.setCellValue("");
		codeBranchVal.setCellStyle(style);

		Cell tcFailed = rHeader.createCell(2);
		tcFailed.setCellValue("Test Cases Failed");
		tcFailed.setCellStyle(style);

		Cell tcFailedVal = rHeader.createCell(3);
		tcFailedVal.setCellValue("");
		tcFailedVal.setCellStyle(style);

		rHeader = worksheet.createRow(5);

		Cell Date = rHeader.createCell(0);
		Date.setCellValue("Date");
		Date.setCellStyle(style);

		Cell DateVal = rHeader.createCell(1);
		DateVal.setCellValue("");
		DateVal.setCellStyle(style);

		Cell tcSkipped = rHeader.createCell(2);
		tcSkipped.setCellValue("Test Cases Skipped");
		tcSkipped.setCellStyle(style);

		Cell tcSkippedVal = rHeader.createCell(3);
		tcSkippedVal.setCellValue("");
		tcSkippedVal.setCellStyle(style);

		rHeader = worksheet.createRow(6);
		rHeader = worksheet.createRow(7);

		CellStyle stylebold = workbook.createCellStyle();
		Font fontbold = workbook.createFont();
		stylebold.setFont(fontbold);
		fontbold.setBold(true);

		Cell cHeader = rHeader.createCell(0);
		cHeader.setCellValue("Start_Time");
		cHeader.setCellStyle(stylebold);

		cHeader = rHeader.createCell(1);
		cHeader.setCellValue("Test_WorkFlow_Id");
		cHeader.setCellStyle(stylebold);

		cHeader = rHeader.createCell(2);
		cHeader.setCellValue("Test_WorkFlow_Description");
		cHeader.setCellStyle(stylebold);

		cHeader = rHeader.createCell(3);
		cHeader.setCellValue("Test Steps");
		cHeader.setCellStyle(stylebold);

		cHeader = rHeader.createCell(4);
		cHeader.setCellValue("Test Status");
		cHeader.setCellStyle(stylebold);

		cHeader = rHeader.createCell(5);
		cHeader.setCellValue("Screenshot");
		cHeader.setCellStyle(stylebold);

		cHeader = rHeader.createCell(6);
		cHeader.setCellValue("Exception");
		cHeader.setCellStyle(stylebold);
		
		cHeader = rHeader.createCell(7);
		cHeader.setCellValue("Time Taken (Secs)");
		cHeader.setCellStyle(stylebold);

		worksheet.createFreezePane(0, 8);
	}
}
