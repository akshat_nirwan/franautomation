package com.builds.reporting;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Reporter;
import org.testng.annotations.Test;

import com.builds.utilities.FranconnectUtil;

public class Screenshot {

	@Test
	public void captureScreenshot() {
		System.setProperty("webdriver.chrome.driver",
				FranconnectUtil.config.get("inputDirectory") + "\\exe\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		driver.get("http://cluster.franconnect.net/fcautomation/");
	}

	public void clickElement(WebDriver driver, WebElement element) throws Exception {
		moveToElement(driver, element);
		boolean isElementClickable = false;

		for (int x = 0; x < 3; x++) {
			try {
				if (isElementClickable == false) {
					element.click();
					isElementClickable = true;
				}
			} catch (Exception e) {
				Reporter.log(e.getMessage());
				isElementClickable = false;
				Thread.sleep(5000);
			}
		}
		if (isElementClickable == false) {
			throw new Exception("Element not clickable : " + element);
		}
	}

	public void moveToElement(WebDriver driver, WebElement element) throws Exception {
		boolean elementFound = false;

		for (int x = 0; x < 3; x++) {
			if (elementFound == false) {
				try {
					Thread.sleep(500);
					((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", element);
					elementFound = true;
					Thread.sleep(50);
				} catch (Exception e) {
					elementFound = false;
					Thread.sleep(5000);
				}
			}
		}

		if (elementFound == false) {
			throw new Exception("Unable to find element on page  :  " + element);
		}

	}

}
