package com.builds.reporting;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.LinkedList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.commons.lang.WordUtils;
import org.apache.poi.common.usermodel.HyperlinkType;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;

import com.builds.utilities.FranconnectUtil;

public class Testng_Result {
	ExcelReporting xls = new ExcelReporting();
	FranconnectUtil fc = new FranconnectUtil();

	public void convertTestNgReportToXLS(String module, String xmlPath, String xlsPath) {

		try {
			File file = new File(xmlPath);

			DocumentBuilder dBuilder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
			Document doc = dBuilder.parse(file);

			String duration = "1";

			if (doc.hasChildNodes()) {
				NodeList ns = doc.getElementsByTagName("suite");
				duration = ns.item(0).getAttributes().getNamedItem("duration-ms").getNodeValue();
				long time = Long.valueOf(duration);
				if (time > 0) {
					time = time / 1000 / 60;
					if (time < 1) {
						time = 1;
					}
				}

				duration = String.valueOf(time);
			}

			if (doc.hasChildNodes()) {
				NodeList ns = doc.getElementsByTagName("test-method");
				printNodeInfo(module, ns, xlsPath, "Reports", duration);
				printNodeInfo(module, ns, xlsPath, "Detailed Logs", duration);
			}

		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}

	private void printNodeInfo(String module, NodeList nodeList, String xlsFilePath, String sheetName, String duration)
			throws Exception {

		File file = new File(xlsFilePath);

		FileInputStream fis = new FileInputStream(file.getAbsolutePath());
		Workbook wb = WorkbookFactory.create(fis);
		Sheet sh = wb.getSheet(sheetName);

		CellStyle style_yellow = wb.createCellStyle();
		style_yellow.setFillForegroundColor(HSSFColor.LIGHT_YELLOW.index);
		style_yellow.setFillPattern(FillPatternType.SOLID_FOREGROUND);

		CellStyle style_red = wb.createCellStyle();
		style_red.setFillForegroundColor(HSSFColor.RED.index);
		style_red.setFillPattern(FillPatternType.SOLID_FOREGROUND);

		CellStyle style = wb.createCellStyle();
		style.setWrapText(true);

		Row row = null;

		Cell cell0 = null;
		Cell cell1 = null;
		Cell cell2 = null;
		Cell cell3 = null;
		Cell cell4 = null;
		Cell cell5 = null;
		Cell cell6 = null;
		Cell cell7 = null;

		String[][] resultSummary = readtestmethod_Fromtestng_report(nodeList, module);

		int rowCounter = 9;
		int y = 0;

		for (int x = 0; x < resultSummary.length; x++) {

			if (resultSummary[x] != null) {
				
				
				if (sheetName.equalsIgnoreCase("Reports") || (sheetName.equalsIgnoreCase("Detailed Logs") && (resultSummary[x][4].equalsIgnoreCase("Fail") || resultSummary[x][4].equalsIgnoreCase("Skip")))){
					
					row = sh.createRow(rowCounter + y);
					y++;
					row.setRowStyle(style);

					try {
						cell0 = row.createCell(0);
					} catch (Exception e) {
						cell0 = row.getCell(0);
					}
					cell0.setCellStyle(style);

					try {
						cell1 = row.createCell(1);
					} catch (Exception e) {
						cell1 = row.getCell(1);
					}
					cell1.setCellStyle(style);

					try {
						cell2 = row.createCell(2);
					} catch (Exception e) {
						cell2 = row.getCell(2);
					}
					cell2.setCellStyle(style);

					try {
						cell3 = row.createCell(3);
					} catch (Exception e) {
						cell3 = row.getCell(3);
					}
					cell3.setCellStyle(style);

					try {
						cell4 = row.createCell(4);
					} catch (Exception e) {
						cell4 = row.getCell(4);
					}
					cell4.setCellStyle(style);

					try {
						cell5 = row.createCell(5);
					} catch (Exception e) {
						cell5 = row.getCell(5);
					}
					cell5.setCellStyle(style);

					try {
						cell6 = row.createCell(6);
					} catch (Exception e) {
						cell6 = row.getCell(6);
					}
					cell6.setCellStyle(style);
					cell6.setCellStyle(style);

					try {
						cell7 = row.createCell(7);
					} catch (Exception e) {
						cell7 = row.getCell(7);
					}

					cell0.setCellValue(resultSummary[x][0]);
					cell1.setCellValue(resultSummary[x][1]);
					cell2.setCellValue(resultSummary[x][2]);

					if (sheetName.equalsIgnoreCase("Reports")) {
						cell3.setCellValue(resultSummary[x][3]);
					} else {
						cell3.setCellValue(resultSummary[x][8]);
					}

					cell4.setCellValue(resultSummary[x][4]);
					cell5.setCellValue(resultSummary[x][5]);

					if (resultSummary[x][5] != null && resultSummary[x][5].length() > 0) {
						CellStyle style_link = wb.createCellStyle();
						Font hlink_font = wb.createFont();
						hlink_font.setUnderline(Font.U_SINGLE);
						hlink_font.setColor(IndexedColors.BLUE.getIndex());
						style_link.setFont(hlink_font);
						CreationHelper createHelper = wb.getCreationHelper();
						org.apache.poi.ss.usermodel.Hyperlink link = createHelper
								.createHyperlink(HyperlinkType.DOCUMENT);
						link = createHelper.createHyperlink(HyperlinkType.FILE);
						link.setAddress(cell5.getStringCellValue());
						cell5.setHyperlink((org.apache.poi.ss.usermodel.Hyperlink) link);
						cell5.setCellStyle(style_link);
					}

					cell6.setCellValue(resultSummary[x][6]);
					cell7.setCellValue(resultSummary[x][7]);
				}
			}
		}

		markDuplidateFailedTestCaseAsSkipped(sh);

		int totalTestCases = 0;
		int tcPassed = 0;
		int tcFailed = 0;
		int tcSkipped = 0;

		for (int x = 9; x < sh.getLastRowNum() + 1; x++) {

			Row statusRow = null;

			try {
				statusRow = sh.getRow(x);
			} catch (Exception e) {

			}
			Cell statusCell = null;

			try {
				statusCell = statusRow.getCell(4);

				if (statusCell.getStringCellValue().equalsIgnoreCase("Pass")) {
					tcPassed++;
				}
				if (statusCell.getStringCellValue().equalsIgnoreCase("Fail")) {
					tcFailed++;
					statusCell.setCellStyle(style_red);
				}
				if (statusCell.getStringCellValue().equalsIgnoreCase("Skip")) {
					tcSkipped++;
					statusCell.setCellStyle(style_yellow);
				}

			} catch (Exception e) {

			}
			totalTestCases++;
		}

		Row row0 = sh.getRow(0);
		Row row1 = sh.getRow(1);
		Row row2 = sh.getRow(2);
		Row row3 = sh.getRow(3);
		Row row4 = sh.getRow(4);
		Row row5 = sh.getRow(5);

		try {
			String suiteName = FranconnectUtil.config.get("SUITE_NAME").trim();

			if (suiteName != null && suiteName.length() > 0) {
				row0.getCell(1).setCellValue(WordUtils.capitalize(suiteName));
			}

			String IPADDRESS = FranconnectUtil.config.get("IPADDRESS");

			if (IPADDRESS != null && IPADDRESS.length() > 0) {
				row0.getCell(3).setCellValue(FranconnectUtil.config.get("IPADDRESS"));
			}

			row1.getCell(1).setCellValue(WordUtils.capitalize(module.trim()));
			row1.getCell(3).setCellValue(duration);
			row2.getCell(1).setCellValue(WordUtils.capitalize(fc.codeUtil().getOSInfo()));
			row2.getCell(3).setCellValue(String.valueOf(totalTestCases));
			row3.getCell(1).setCellValue(WordUtils.capitalize(FranconnectUtil.config.get("browserName")));
			row3.getCell(3).setCellValue(String.valueOf(tcPassed));
			row4.getCell(1).setCellValue(fc.codeUtil().getBuildVersion(FranconnectUtil.config.get("buildUrl")));
			row4.getCell(3).setCellValue(String.valueOf(tcFailed));
			row5.getCell(1).setCellValue(fc.codeUtil().getCurrentDateForReport());
			row5.getCell(3).setCellValue(String.valueOf(tcSkipped));
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}

		sh.autoSizeColumn(0);
		sh.autoSizeColumn(1);
		sh.autoSizeColumn(2);
		sh.autoSizeColumn(3);
		sh.autoSizeColumn(4);
		sh.autoSizeColumn(5);
		sh.autoSizeColumn(6);
		sh.autoSizeColumn(7);

		FileOutputStream fout = new FileOutputStream(file.getAbsolutePath());
		wb.write(fout);
		wb.close();
		fout.close();
		fout.flush();

	}

	private String[][] readtestmethod_Fromtestng_report(NodeList nodeList, String module) {
		int nodeListSize = nodeList.getLength();

		String[][] resultSummary = new String[nodeListSize][9];

		for (int x = 0; x < nodeListSize; x++) {
			String Start_Time = "";
			String Test_WorkFlow_Id = "";
			String Test_WorkFlow_Description = "";
			String TestSteps = "";
			String detailedTestSteps = "";
			List<File> vid = new LinkedList<File>();
			String TestStatus = nodeList.item(x).getAttributes().getNamedItem("status").getNodeValue();
			String Screenshot = "";
			String exceptionMessage = "";
			String timeTaken = String.valueOf(
					Long.parseLong(nodeList.item(x).getAttributes().getNamedItem("duration-ms").getNodeValue()) / 1000);

			NodeList testMethodChilds = nodeList.item(x).getChildNodes();
			for (int p = 0; p < testMethodChilds.getLength(); p++) {
				NodeList testMethodSubChilds = testMethodChilds.item(p).getChildNodes();
				for (int a = 0; a < testMethodSubChilds.getLength(); a++) {
					if (testMethodSubChilds.item(a).getNodeName().equalsIgnoreCase("message")) {
						exceptionMessage = testMethodSubChilds.item(a).getTextContent();
						exceptionMessage = exceptionMessage.trim();
						if (exceptionMessage.contains("ExceptionMessage# :")) {
							exceptionMessage = exceptionMessage.replace("ExceptionMessage# :", "");
						}
					}
				}
				if (testMethodChilds.item(p).getNodeName().equalsIgnoreCase("reporter-output")) {
					NodeList lineTag = testMethodChilds.item(p).getChildNodes();

					for (int y = 0; y < lineTag.getLength(); y++) {
						String newSteps = lineTag.item(y).getTextContent().trim();
						if (newSteps != null && newSteps.length() > 0 && !"".equalsIgnoreCase(newSteps)
								&& newSteps.startsWith("Test Steps# :")) {
							newSteps = newSteps.replace("Test Steps# :", "");
							newSteps = newSteps.replaceAll(" +", " ").trim();
							// replaces double spaces with single
							TestSteps = TestSteps + "\n" + newSteps;
						}

						if (newSteps != null && newSteps.length() > 0 && !"".equalsIgnoreCase(newSteps)
								&& newSteps.startsWith("Detailed Test Steps# :")) {
							newSteps = newSteps.replace("Detailed Test Steps# :", "");
							newSteps = newSteps.replaceAll(" +", " ").trim();
							// replaces double spaces with single
							detailedTestSteps = detailedTestSteps + "\n" + newSteps;
						}

						if (newSteps != null && newSteps.length() > 0 && !"".equalsIgnoreCase(newSteps)
								&& newSteps.startsWith("Captured Image# : ")) {
							newSteps = newSteps.replace("Captured Image# : ", "");
							newSteps = newSteps.replaceAll(" +", " ").trim();
							// replaces double spaces with single
							File f = new File(newSteps);
							vid.add(f);
						}

						if (newSteps != null && newSteps.length() > 0 && !"".equalsIgnoreCase(newSteps)
								&& newSteps.startsWith("Start_Time# :")) {
							newSteps = newSteps.replace("Start_Time# :", "");
							if (newSteps != null) {
								String cseparated = "";
								if (newSteps.contains(",")) {
									String[] str = newSteps.split(",");
									for (String s : str) {
										cseparated = cseparated.concat("\n").concat(s.trim());
									}
									newSteps = cseparated.trim();
								} else {
									Start_Time = newSteps;
								}
							}
						}

						if (newSteps != null && newSteps.length() > 0 && !"".equalsIgnoreCase(newSteps)
								&& newSteps.startsWith("Test_WorkFlow_Id# :")) {
							newSteps = newSteps.replace("Test_WorkFlow_Id# :", "");
							if (newSteps != null) {
								WordUtils.capitalize(newSteps);
								Test_WorkFlow_Id = newSteps.trim();
							}
						}

						if (newSteps != null && newSteps.length() > 0 && !"".equalsIgnoreCase(newSteps)
								&& newSteps.startsWith("Test_WorkFlow_Id_ALT# :")) {
							newSteps = newSteps.replace("Test_WorkFlow_Id_ALT# :", "");
							if (newSteps != null) {
								WordUtils.capitalize(newSteps);
								Test_WorkFlow_Id = newSteps.trim();
							}
						}

						if (newSteps != null && newSteps.length() > 0 && !"".equalsIgnoreCase(newSteps)
								&& newSteps.startsWith("Test_WorkFlow_Description# :")) {
							newSteps = newSteps.replace("Test_WorkFlow_Description# :", "");
							if (newSteps != null) {
								WordUtils.capitalize(newSteps);
								Test_WorkFlow_Description = newSteps.trim();
							}
						}
						if (newSteps != null && newSteps.length() > 0 && !"".equalsIgnoreCase(newSteps)
								&& newSteps.startsWith("Screenshot# :")) {
							newSteps = newSteps.replace("Screenshot# :", "");
							if (newSteps != null) {
								WordUtils.capitalize(newSteps);
								Screenshot = newSteps.trim();
							}
						}
					}

					resultSummary[x][0] = Start_Time;
					resultSummary[x][1] = Test_WorkFlow_Id;
					resultSummary[x][2] = getFormattedText(Test_WorkFlow_Description, 30);
					resultSummary[x][3] = getFormattedText(TestSteps, 30);
					resultSummary[x][4] = TestStatus;
					resultSummary[x][5] = Screenshot;
					resultSummary[x][6] = getMinimumExceptionMessage(exceptionMessage);
					resultSummary[x][7] = timeTaken;
					resultSummary[x][8] = detailedTestSteps;

					// createig vieo
					// String videolocation="D:\\video.mp4";
					String videolocation = FranconnectUtil.config.get("modulePath") + "\\video";
					boolean f = new File(videolocation).mkdirs();
					videolocation += "\\" + Test_WorkFlow_Id + ".mp4";
					// ScreenRecordingExample.createVideo(vid,videolocation);
					DBRelated dbr = new DBRelated();
					try {
						if (FranconnectUtil.config.get("inserttoDB").equals("Yes")) {
							dbr.InsertIntoDB(Test_WorkFlow_Id, resultSummary[x][3], TestStatus, module);
						}
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			}
		}

		return resultSummary;
	}

	public String getMinimumExceptionMessage(String expMessage) {
		String str1 = "(Session info:";
		if (expMessage != null && expMessage.length() > 0) {
			if (expMessage.contains(str1)) {
				expMessage = expMessage.substring(0, expMessage.indexOf(str1));
				if (expMessage != null) {
					expMessage = expMessage.trim();
				}
			}
		}

		expMessage = removeStringFromException(expMessage);

		return expMessage;
	}

	public String getFormattedText(String steps, int lineMaxChars) {

		/*
		 * 
		 * if(steps!=null && steps.length()>0) { String par = ""; { String[]
		 * tempSteps = steps.split(" "); String line = ""; for(int
		 * x=0;x<tempSteps.length;x++) { String word = tempSteps[x]; line =
		 * line.concat(" ").concat(word);
		 * 
		 * if(line!=null && line.length()>40) { par =
		 * par.concat("\n").concat(line.trim()); line = ""; } } if(line!=null &&
		 * line.length()>0) { par = par.concat("\n").concat(line.trim()); } }
		 * return par; } else { return steps; }
		 * 
		 */

		if (steps != null) {
			steps = steps.trim();
		}

		return steps;
	}

	private void markDuplidateFailedTestCaseAsSkipped(Sheet sheet) {
		Row row = null;
		Row comparerow = null;
		for (int x = 0; x < sheet.getLastRowNum(); x++) {
			row = sheet.getRow(x);
			if (row != null) {
				Cell cellid = row.getCell(1);
				Cell cell = row.getCell(6);
				if (cell != null) {
					String exceptionMessage = cell.getStringCellValue();

					if (exceptionMessage != null && exceptionMessage.length() > 0
							&& !exceptionMessage.contains("Possible Duplicate Failed Reason # ")) {

						for (int y = x + 1; y < sheet.getLastRowNum() + 1; y++) {
							comparerow = sheet.getRow(y);
							if (comparerow != null) {
								Cell cellcompare = comparerow.getCell(6);
								Cell cellstatus = comparerow.getCell(4);

								if (cellcompare != null) {
									String exceptionMessageCompared = cellcompare.getStringCellValue();
									if (exceptionMessage.equalsIgnoreCase(exceptionMessageCompared)) {
										cellcompare.setCellValue("Possible Duplicate Failed Reason # "
												+ cellid.getStringCellValue() + "\n" + exceptionMessageCompared);
										cellstatus.setCellValue("Skip");
									}
								}
							}
						}
					}
				}
			}
		}
	}

	public String removeStringFromException(String exceptionMessage) {
		String newText = "";
		String validationText = "";

		if (exceptionMessage.contains("[[ChromeDriver:")) {
			validationText = "[[ChromeDriver:";
		} else if (exceptionMessage.contains("[[RemoteWebDriver:")) {
			validationText = "[[RemoteWebDriver:";
		} else if (exceptionMessage.contains("[[FirefoxWebDriver:")) {
			validationText = "[[FirefoxWebDriver:";
		}

		try {
			if (exceptionMessage.contains(validationText) && validationText.length() > 0) {
				int start = exceptionMessage.indexOf(validationText);
				System.out.println(start);

				String subStr = exceptionMessage.substring(start);

				if (subStr != null && subStr.length() > 0) {
					if (subStr.contains("]")) {
						int end = subStr.indexOf("]");
						try {
							newText = exceptionMessage.substring(start, start + end + 1);
						} catch (Exception e) {

						}
					}
				}
			}

			if (newText != null && newText.length() > 0) {
				newText = exceptionMessage.replace(newText, "");
			} else {
				newText = exceptionMessage;
			}
		} catch (Exception e) {
			newText = exceptionMessage;
		}

		return newText;
	}

}
