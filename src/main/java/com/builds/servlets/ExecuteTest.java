package com.builds.servlets;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class ExecuteTest
 */
public class ExecuteTest extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public ExecuteTest() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) {
		// TODO Auto-generated method stub
		try {
			response.getWriter().append("Served at: ").append(request.getContextPath());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) {
		// TODO Auto-generated method stub
		Map<String, String> config = new HashMap<String, String>();

		config.put("buildUrl", request.getParameter("buildUrl1"));
		config.put("userName", request.getParameter("userName1"));
		config.put("password", request.getParameter("password1"));
		config.put("browserName", request.getParameter("browserName"));
		config.put("emailIds", request.getParameter("emailId"));
		config.put("category", request.getParameter("category"));
		config.put("subcategory", request.getParameter("subcategory"));
		config.put("moduleList", request.getParameter("moduleList"));
		config.put("buildUrl2", request.getParameter("buildUrl2"));
		config.put("userName2", request.getParameter("userName2"));
		config.put("password2", request.getParameter("password2"));
		config.put("ipAddressTxt", request.getParameter("ipAddressTxt"));
		config.put("ipaddress", request.getParameter("ipaddress"));
		config.put("serverIPAdd", request.getParameter("serverIPAdd"));
		config.put("webformstatus", "true");

		/*
		 * BaseUtil blt = new BaseUtil(); try { blt.runTestScript(config); }
		 * catch (Exception e) { // TODO Auto-generated catch block
		 * e.printStackTrace(); }
		 */
	}
}
