package com.builds.servlets;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.builds.utilities.ScriptExecuter;

/**
 * Servlet implementation class ExecuteTestngXml
 */
public class ExecuteTestngXml extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public ExecuteTestngXml() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) {
		// TODO Auto-generated method stub
		try {
			response.getWriter().append("Served at: ").append(request.getContextPath());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) {
		System.out.println(">>>ExecuteTestngXml");
		/*
		 * Browser.buildUrl = request.getParameter("buildUrl1");
		 * Browser.userName = request.getParameter("userName1");
		 * Browser.password = request.getParameter("password1");
		 * 
		 * Browser.browserName = request.getParameter("browserName");
		 * Browser.emailIds = request.getParameter("emailId"); Browser.category
		 * =request.getParameter("category"); Browser.subcategory
		 * =request.getParameter("subcategory"); Browser.moduleList =
		 * request.getParameterValues("moduleList"); Browser.buildUrl2
		 * =request.getParameter("buildUrl2"); Browser.userName2
		 * =request.getParameter("userName2"); Browser.password2
		 * =request.getParameter("password2"); Browser.systemIp =
		 * request.getParameter("ipAddressTxt"); Browser.systemipstatus =
		 * request.getParameter("ipaddress"); Browser.serverIPAdd =
		 * request.getParameter("serverIPAdd");
		 * 
		 * System.out.println("browserName : "+Browser.browserName);
		 * System.out.println("emailId : "+Browser.emailIds);
		 * System.out.println("category : "+Browser.category);
		 * System.out.println("subcategory : "+Browser.subcategory);
		 * System.out.println("buildUrl1 : "+Browser.buildUrl);
		 * System.out.println("userName1 : "+Browser.userName);
		 * System.out.println("password1 : "+Browser.password);
		 * System.out.println("buildUrl2 : "+Browser.buildUrl2);
		 * System.out.println("userName2 : "+Browser.userName2);
		 * System.out.println("password2 : "+Browser.password2);
		 * System.out.println("ipAddress : "+Browser.systemIp);
		 * System.out.println("ipAddress Status : "+Browser.systemipstatus);
		 * System.out.println("serverIPAdd : "+Browser.serverIPAdd);
		 */

		Map<String, String> config = new HashMap<String, String>();

		config.put("buildUrl", request.getParameter("buildUrl1"));
		config.put("userName", request.getParameter("userName1"));
		config.put("password", request.getParameter("password1"));
		config.put("browserName", request.getParameter("browserName"));
		config.put("emailIds", request.getParameter("emailId"));
		config.put("category", request.getParameter("category"));
		config.put("subcategory", request.getParameter("subcategory"));
		config.put("moduleList", request.getParameter("moduleList"));
		config.put("buildUrl2", request.getParameter("buildUrl2"));
		config.put("userName2", request.getParameter("userName2"));
		config.put("password2", request.getParameter("password2"));
		config.put("ipAddressTxt", request.getParameter("ipAddressTxt"));
		config.put("ipaddress", request.getParameter("ipaddress"));
		config.put("serverIPAdd", request.getParameter("serverIPAdd"));
		config.put("webformstatus", "true");

		System.out.println(config.get("buildUrl"));
		System.out.println(config.get("userName"));
		System.out.println(config.get("password"));
		System.out.println(config.get("browserName"));
		System.out.println(config.get("emailIds"));
		System.out.println(config.get("category"));
		System.out.println(config.get("subcategory"));
		System.out.println(config.get("moduleList"));
		System.out.println(config.get("buildUrl2"));
		System.out.println(config.get("userName2"));
		System.out.println(config.get("password2"));
		System.out.println(config.get("ipAddressTxt"));
		System.out.println(config.get("ipaddress"));
		System.out.println(config.get("serverIPAdd"));
		System.out.println(config.get("webformstatus"));

		try {
			ScriptExecuter.runTestScript();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			response.sendRedirect("confirmation.html");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
