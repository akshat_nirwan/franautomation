package com.builds.servlets;

import java.io.IOException;

import javax.mail.MessagingException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.builds.utilities.ScriptExecuter;

/**
 * Servlet implementation class ProductAutomation
 */
public class ProductAutomation extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public ProductAutomation() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) {
		try {
			response.getWriter().append("Served at: ").append(request.getContextPath());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) {

		String buildUrl = request.getParameter("buildUrl");
		String browserName = request.getParameter("browserName");
		String emailId = request.getParameter("emailId");
		String ipaddresstxt = request.getParameter("ipaddresstxt");

		String category = request.getParameter("category");
		String usertype = request.getParameter("usertype");
		String functions = request.getParameter("functions");

		String buildUrl1 = request.getParameter("buildUrl1");
		String userName1 = request.getParameter("userName1");
		String password1 = request.getParameter("password1");

		String buildUrl2 = request.getParameter("buildUrl2");
		String userName2 = request.getParameter("userName2");
		String password2 = request.getParameter("password2");

		String status = "Not Working";
		try {
			status = ScriptExecuter.runScript(buildUrl, browserName, emailId, category, ipaddresstxt, usertype,
					functions, buildUrl1, userName1, password1, buildUrl2, userName2, password2);
		} catch (MessagingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("Automation Status : " + status);
		try {
			response.sendRedirect("confirmation.html");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}