package com.builds.test.User;

import java.util.Map;

import org.openqa.selenium.WebDriver;
import org.testng.Reporter;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.builds.test.fs.AdminFranchiseSalesManageFormGeneratorPageTest;
import com.builds.uimaps.crm.AdminCRMManageFormGeneratorPage;
import com.builds.utilities.FranconnectUtil;
import com.builds.utilities.TestCase;

public class demo {

	FranconnectUtil fc = new FranconnectUtil();

	@DataProvider(name = "TextArea")
	public Object[][] getData1() {
		int count = 1;
		Object[][] data = new Object[count][1];
		for (int i = 1; i <= count; i++) {
			int j = i - 1;
			data[j][0] = String.valueOf(i);
		}

		return data;
	}

	@Test(groups = { "salesFormGenerate", "600TextArea", "1000Yatin" }, dataProvider = "TextArea")
	@TestCase(createdOn = "2017-07-26", updatedOn = "2017-08-17", testCaseId = "TC_Sales_ValidateFieldAllValue_TextArea", testCaseDescription = "Verify Tab in TextArea")
	private void ValidateFieldInDropDownTab(String count) throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData_FormGenerator_count(testCaseId, count);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		testCaseId = testCaseId + "_data" + count;
		Reporter.log("Test_WorkFlow_Id_ALT# : " + testCaseId);
		// ExtentTest test = BaseUtil.reports.startTest(testCaseId);

		try {
			AdminCRMManageFormGeneratorPage pobj = new AdminCRMManageFormGeneratorPage(driver);
			// fc.utobj().printTestStep(test, testCaseId, "Hitting Build Url and
			// Then Login");
			String tabName = "New Tab";
			String fieldName = "Field";
			String sectionName = "New Section";
			AdminFranchiseSalesManageFormGeneratorPageTest p1 = new AdminFranchiseSalesManageFormGeneratorPageTest();
			if (count.equals("1")) {
				driver = fc.loginpage().login(driver);
				// fc.utobj().printTestStep(test, testCaseId, "Go to Admin >
				// Sales > Manage Form Generator");
				fc.crm().crm_common().adminCRMManageFormGeneratorLnk(driver);
				// fc.utobj().printTestStep(test, testCaseId, "Manage Form
				// Generator > Adding New Tab");
				// tabName = p1.addCRMTabName(driver, tabName);
				// fc.utobj().clickPartialLinkText(driver, tabName);
				//// fc.utobj().printTestStep(test, testCaseId, "Adding Section
				// Name");
				// sectionName = p1.addSectionName(driver, sectionName);
				// fc.utobj().printTestStep(test, testCaseId, "Adding DropDown
				// Field Name with " + fieldName);
			}
			for (int i = 0; i < 1000; i++) {
				p1.addFieldName1(driver, "custom info1", fieldName + String.valueOf(i), dataSet, testCaseId);
				Thread.sleep(4000);
				System.out.println("adding new field" + i);
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
			// test.log(LogStatus.PASS, "Logout and quit");
			// BaseUtil.reports.endTest(test);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
			// test.log(LogStatus.FAIL, " Logout and quit with " +
			// e.toString());
			// BaseUtil.reports.endTest(test);
		}

	}

}
