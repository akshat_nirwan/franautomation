package com.builds.test.admin;

import org.openqa.selenium.WebDriver;

import com.builds.uimaps.admin.AddCorporateUser_Testing_UI;
import com.builds.utilities.FranconnectUtil;

public class AddCorporateUser_Testing {

	
	
	public void fillUserInfo(WebDriver driver, CorporateUser corpuser) throws Exception
	{
		AddCorporateUser_Testing_UI ui = new AddCorporateUser_Testing_UI(driver);
		FranconnectUtil fc = new FranconnectUtil();
		
		fc.utobj().printTestStep("Fill User Info");
		
		if(corpuser.getLoginId()!=null)
		{
			fc.utobj().sendKeys(driver, ui.userNameTxt, corpuser.getLoginId());
		}
		if(corpuser.getFirstname()!=null)
		{
			fc.utobj().sendKeys(driver, ui.firstNameTxt, corpuser.getFirstname());
		}
		if(corpuser.getLastname()!=null)
		{
			fc.utobj().sendKeys(driver, ui.lastNameTxt, corpuser.getLastname());
		}
	}
	
	
	public void submit(WebDriver driver) throws Exception
	{
		FranconnectUtil fc = new FranconnectUtil();
		
		AddCorporateUser_Testing_UI ui = new AddCorporateUser_Testing_UI(driver);
		
		fc.utobj().printTestStep("Click Submit");
		
		fc.utobj().clickElement(driver, ui.SubmitButton);
	}
	
	
}
