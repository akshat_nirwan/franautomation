package com.builds.test.admin;

import java.util.Map;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Test;

import com.builds.test.common.CorporateUser;
import com.builds.uimaps.admin.AdminAccessControlLoginLogsSystemDetailsPage;
import com.builds.uimaps.admin.AdminAccessControlUserAccountSummaryPage;
import com.builds.utilities.FranconnectUtil;
import com.builds.utilities.TestCase;

public class AdminAccessControlLoginLogsSystemDetailsPageTest {
	FranconnectUtil fc = new FranconnectUtil();

	@Test(groups = { "admin"})
	@TestCase(createdOn = "2017-10-31", updatedOn = "2017-10-31", testCaseId = "TC_Admin_LoginLogs_001", testCaseDescription = "Verify Login Logs for Corporate User")
	private void loginLogsCorporateUser() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			AdminAccessControlLoginLogsSystemDetailsPage pobj = new AdminAccessControlLoginLogsSystemDetailsPage(
					driver);
			fc.utobj().printTestStep("Navigate To Admin > Users > Manage Corporate Users");

			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporate_user = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			String emailId = "commonautomation@staffex.com";

			CorporateUser corpUser = new CorporateUser();
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corpUser.setEmail(emailId);
			corpUser = corporate_user.createDefaultUser(driver, corpUser);

			fc.utobj().printTestStep("Navigate To Admin > Access Control > Login Logs > Users not Logged In");
			fc.adminpage().openLoginLogs(driver);
			fc.utobj().clickElement(driver, pobj.usersNotLoggedInTab);

			fc.utobj().printTestStep("Verify User Should Be Listed At Users not Logged In Tab");
			fc.utobj().selectDropDownByValue(driver, pobj.userType, "0");
			fc.utobj().clickElement(driver, pobj.searchBtn);
			fc.utobj().showAll(driver);

			boolean isTextPresent = fc.utobj().assertLinkPartialText(driver, corpUser.getuserFullName());

			if (isTextPresent == false) {
				fc.utobj().throwsException("Not able to verify user at Not Logged in Tab");
			}

			fc.utobj().printTestStep("Logged Out");
			fc.home_page().logout(driver);

			fc.utobj().printTestStep("Login With Corporate User");
			fc.loginpage().loginWithParameter(driver, corpUser.getUserName(), corpUser.getPassword());

			fc.utobj().printTestStep("Navigate To Admin > Access Control > Login Logs > Users not Logged In");
			fc.adminpage().openLoginLogs(driver);
			fc.utobj().clickElement(driver, pobj.usersNotLoggedInTab);

			fc.utobj().printTestStep("Verify User Should Not Be Listed At Users not Logged In Tab");
			fc.utobj().selectDropDownByValue(driver, pobj.userType, "0");
			fc.utobj().clickElement(driver, pobj.searchBtn);
			fc.utobj().showAll(driver);

			boolean isTextPresent1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[@class='bText12lnk' and contains(text(),'" + corpUser.getuserFullName() + "')]");
			if (isTextPresent1 == true) {
				fc.utobj()
						.throwsException("Not able to verify that User should not be listed at User Not Logged In Tab");
			}

			fc.utobj().printTestStep("Verify User Should Be Listed At Current Logged In Users Tab");
			fc.utobj().clickElement(driver, pobj.currentLoggedUserTab);
			fc.utobj().showAll(driver);

			boolean isTextPresent2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text(),'" + corpUser.getuserFullName() + "')]");

			if (isTextPresent2 == false) {
				fc.utobj().throwsException(
						"Not able to verify that User should be listed at Current Logged In Users Tab");
			}

			fc.utobj().printTestStep("Logged Out");
			fc.home_page().logout(driver);

			fc.utobj().printTestStep("Login With Admin User");
			fc.loginpage().loginWithParameter(driver, config.get("userName"), config.get("password"));

			fc.utobj().printTestStep("Navigate To Admin > Access Control > Login Logs > Users Logged In Tab");
			fc.adminpage().openLoginLogs(driver);
			fc.utobj().clickElement(driver, pobj.userLoggedInTab);

			fc.utobj().printTestStep("Verify User Should Be Listed At User Logged In Tab");

			fc.utobj().selectDropDownByValue(driver, pobj.userdetails, "0");
			fc.utobj().clickElement(driver, pobj.searchBtn);
			fc.utobj().showAll(driver);

			boolean isTextPresent3 = fc.utobj().assertLinkPartialText(driver, corpUser.getuserFullName());
			if (isTextPresent3 == false) {
				fc.utobj().throwsException("Not able to verify that User should be listed at User Logged In Tab");
			}

			fc.utobj().printTestStep("Verify User Should Not Be Listed At Current Logged In Users Tab");
			fc.utobj().clickElement(driver, pobj.currentLoggedUserTab);
			fc.utobj().showAll(driver);

			boolean isTextPresent4 = fc.utobj().assertLinkPartialText(driver, corpUser.getuserFullName());
			if (isTextPresent4 == true) {
				fc.utobj().throwsException(
						"Not able to verify that User Should not be listed at Current Logged In User Tab");
			}

			/*
			 * Commented Due this can only verify if we create user by front not by restAPi
			 */
			
			
			
			/*fc.utobj().printTestStep("Navigate To Admin > Access Control > User Account Summary");
			fc.adminpage().userAccountSummary(driver);

			fc.utobj().printTestStep("Select Inquiry Date > All");
			fc.utobj().selectDropDownByValue(driver,
					new AdminAccessControlUserAccountSummaryPage(driver).enquiryDateSelect, "55");
			fc.utobj().clickElement(driver, new AdminAccessControlUserAccountSummaryPage(driver).viewReport);

			fc.utobj().printTestStep(
					"Verify The User should be listed in user created list after clicking over user created count");
			fc.utobj().clickElement(driver, new AdminAccessControlUserAccountSummaryPage(driver).corporateUserCount);
			fc.utobj().selectDropDownByValue(driver, new AdminAccessControlUserAccountSummaryPage(driver).userSelect,
					"0");
			fc.utobj().showAll(driver);

			boolean isTextPresent5 = fc.utobj().assertPageSource(driver, corpUser.getuserFullName());
			if (isTextPresent5 == false) {
				fc.utobj().throwsException(
						"Not able to verify that The User should be listed in user created list after clicking over user created count");
			}*/
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups ={"admin"})
	@TestCase(createdOn = "2017-10-31", updatedOn = "2017-10-31", testCaseId = "TC_Admin_LoginLogs_002", testCaseDescription = "Verify Login Logs for Regional User")
	private void loginlogsRegionalUser() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			AdminAccessControlLoginLogsSystemDetailsPage pobj = new AdminAccessControlLoginLogsSystemDetailsPage(
					driver);

			AdminUsersManageRegionalUsersAddRegionalUserPageTest regional_user = new AdminUsersManageRegionalUsersAddRegionalUserPageTest();
			String userName = fc.utobj().generateTestData("TestReg");
			String password = "t0n1ght123";
			String emailId = "commonautomation@staffex.com";
			String regionName = fc.utobj().generateTestData("TestR");

			fc.utobj().printTestStep("Navigate To Area Management Page > Add Region");
			AdminAreaRegionAddAreaRegionPageTest area_page = new AdminAreaRegionAddAreaRegionPageTest();
			area_page.addAreaRegion(driver, regionName);

			fc.utobj().printTestStep("Navigate To Admin > Users > Manage Regional Users");
			fc.utobj().printTestStep("Add Regional User");
			regional_user.addRegionalUser(driver, userName, password, regionName, emailId);

			String userFullName = userName + " " + userName;

			fc.utobj().printTestStep("Navigate To Admin > Access Control > Login Logs > Users not Logged In");
			fc.adminpage().openLoginLogs(driver);
			fc.utobj().clickElement(driver, pobj.usersNotLoggedInTab);

			fc.utobj().printTestStep("Verify User Should Be Listed At Users not Logged In Tab");
			fc.utobj().selectDropDownByValue(driver, pobj.userType, "2");
			fc.utobj().clickElement(driver, pobj.searchBtn);
			fc.utobj().showAll(driver);

			boolean isTextPresent = fc.utobj().assertLinkPartialText(driver, userFullName);
			if (isTextPresent == false) {
				fc.utobj().throwsException("Not able to verify user at Not Logged in Tab");
			}

			fc.utobj().printTestStep("Logged Out");
			fc.home_page().logout(driver);

			fc.utobj().printTestStep("Login With Regional User");
			fc.loginpage().loginWithParameter(driver, userName, password);

			fc.utobj().printTestStep("Logged Out");
			fc.home_page().logout(driver);

			fc.utobj().printTestStep("Login With Admin User");
			fc.loginpage().loginWithParameter(driver, config.get("userName"), config.get("password"));

			fc.utobj().printTestStep("Navigate To Admin > Access Control > Login Logs > Users Logged In Tab");
			fc.adminpage().openLoginLogs(driver);
			fc.utobj().clickElement(driver, pobj.userLoggedInTab);

			fc.utobj().printTestStep("Verify User Should Be Listed At User Logged In Tab");

			fc.utobj().selectDropDownByValue(driver, pobj.userdetails, "2");
			fc.utobj().clickElement(driver, pobj.searchBtn);
			fc.utobj().showAll(driver);

			boolean isTextPresent3 = fc.utobj().assertLinkPartialText(driver, userFullName);
			if (isTextPresent3 == false) {
				fc.utobj().throwsException("Not able to verify that User should be listed at User Logged In Tab");
			}

			fc.utobj().printTestStep("Verify User Should Not Be Listed At Current Logged In Users Tab");
			fc.utobj().clickElement(driver, pobj.currentLoggedUserTab);
			fc.utobj().showAll(driver);

			boolean isTextPresent4 = fc.utobj().assertLinkPartialText(driver, userFullName);
			if (isTextPresent4 == true) {
				fc.utobj().throwsException(
						"Not able to verify that User Should not be listed at Current Logged In User Tab");
			}

			fc.utobj().printTestStep("Navigate To Admin > Access Control > User Account Summary");
			fc.adminpage().userAccountSummary(driver);

			fc.utobj().printTestStep("Select Inquiry Date > All");
			fc.utobj().selectDropDownByValue(driver,
					new AdminAccessControlUserAccountSummaryPage(driver).enquiryDateSelect, "55");
			fc.utobj().clickElement(driver, new AdminAccessControlUserAccountSummaryPage(driver).viewReport);

			fc.utobj().printTestStep(
					"Verify The User should be listed in user created list after clicking over user created count");
			fc.utobj().clickElement(driver, new AdminAccessControlUserAccountSummaryPage(driver).regionalUserCount);
			fc.utobj().selectDropDownByValue(driver, new AdminAccessControlUserAccountSummaryPage(driver).userSelect,
					"2");
			fc.utobj().showAll(driver);

			boolean isTextPresent5 = fc.utobj().assertLinkPartialText(driver, userFullName);
			if (isTextPresent5 == false) {
				fc.utobj().throwsException(
						"Not able to verify that The User should be listed in user created list after clicking over user created count");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "admin"})
	@TestCase(createdOn = "2017-10-31", updatedOn = "2017-10-31", testCaseId = "TC_Admin_LoginLogs_003", testCaseDescription = "Verify Login Logs for Divisional User")
	private void loginlogsDivsionalUser() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			AdminAccessControlLoginLogsSystemDetailsPage pobj = new AdminAccessControlLoginLogsSystemDetailsPage(
					driver);

			AdminUsersManageDivisionalUsersAddDivisionalUsersPageTest divisional_user = new AdminUsersManageDivisionalUsersAddDivisionalUsersPageTest();
			String userName = fc.utobj().generateTestData("TestDiv");
			String password = "t0n1ght123";
			String emailId = "commonautomation@staffex.com";
			String divisionName = fc.utobj().generateTestData("TestD");

			fc.utobj().printTestStep("Add Division");
			AdminDivisionAddDivisionPageTest p1 = new AdminDivisionAddDivisionPageTest();
			p1.addDivision(driver, divisionName);

			fc.utobj().printTestStep("Navigate To Admin > Users > Manage Divisional Users");
			fc.utobj().printTestStep("Add Divisional User");
			divisional_user.addDivisionalUser(driver, userName, password, divisionName, emailId);
			String userFullName = userName + " " + userName;

			fc.utobj().printTestStep("Navigate To Admin > Access Control > Login Logs > Users not Logged In");
			fc.adminpage().openLoginLogs(driver);
			fc.utobj().clickElement(driver, pobj.usersNotLoggedInTab);

			fc.utobj().printTestStep("Verify User Should Be Listed At Users not Logged In Tab");
			fc.utobj().selectDropDownByValue(driver, pobj.userType, "6");
			fc.utobj().clickElement(driver, pobj.searchBtn);
			fc.utobj().showAll(driver);

			boolean isTextPresent = fc.utobj().assertLinkPartialText(driver, userFullName);
			if (isTextPresent == false) {
				fc.utobj().throwsException("Not able to verify user at Not Logged in Tab");
			}

			fc.utobj().printTestStep("Logged Out");
			fc.home_page().logout(driver);

			fc.utobj().printTestStep("Login With Divisional User");
			fc.loginpage().loginWithParameter(driver, userName, password);

			fc.utobj().printTestStep("Logged Out");
			fc.home_page().logout(driver);

			fc.utobj().printTestStep("Login With Admin User");
			fc.loginpage().loginWithParameter(driver, config.get("userName"), config.get("password"));

			fc.utobj().printTestStep("Navigate To Admin > Access Control > Login Logs > Users Logged In Tab");
			fc.adminpage().openLoginLogs(driver);
			fc.utobj().clickElement(driver, pobj.userLoggedInTab);

			fc.utobj().printTestStep("Verify User Should Be Listed At User Logged In Tab");

			fc.utobj().selectDropDownByValue(driver, pobj.userdetails, "6");
			fc.utobj().clickElement(driver, pobj.searchBtn);
			fc.utobj().showAll(driver);

			boolean isTextPresent3 = fc.utobj().assertLinkPartialText(driver, userFullName);
			if (isTextPresent3 == false) {
				fc.utobj().throwsException("Not able to verify that User should be listed at User Logged In Tab");
			}

			fc.utobj().printTestStep("Verify User Should Not Be Listed At Current Logged In Users Tab");
			fc.utobj().clickElement(driver, pobj.currentLoggedUserTab);
			fc.utobj().showAll(driver);

			boolean isTextPresent4 = fc.utobj().assertLinkPartialText(driver, userFullName);
			if (isTextPresent4 == true) {
				fc.utobj().throwsException(
						"Not able to verify that User Should not be listed at Current Logged In User Tab");
			}

			fc.utobj().printTestStep("Navigate To Admin > Access Control > User Account Summary");
			fc.adminpage().userAccountSummary(driver);

			fc.utobj().printTestStep("Select Inquiry Date > All");
			fc.utobj().selectDropDownByValue(driver,
					new AdminAccessControlUserAccountSummaryPage(driver).enquiryDateSelect, "55");
			fc.utobj().clickElement(driver, new AdminAccessControlUserAccountSummaryPage(driver).viewReport);

			fc.utobj().printTestStep(
					"Verify The User should be listed in user created list after clicking over user created count");
			fc.utobj().clickElement(driver, new AdminAccessControlUserAccountSummaryPage(driver).divisionalUserCount);
			fc.utobj().selectDropDownByValue(driver, new AdminAccessControlUserAccountSummaryPage(driver).userSelect,
					"6");
			fc.utobj().showAll(driver);

			boolean isTextPresent5 = fc.utobj().assertLinkPartialText(driver, userFullName);
			if (isTextPresent5 == false) {
				fc.utobj().throwsException(
						"Not able to verify that The User should be listed in user created list after clicking over user created count");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "admin" })
	@TestCase(createdOn = "2017-10-31", updatedOn = "2017-10-31", testCaseId = "TC_Admin_LoginLogs_004", testCaseDescription = "Verify Login Logs for Franchise User")
	private void loginlogsFranchiseUser() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		// Map<String,String> dataSet = fc.utobj().readTestData("admin",
		// testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			AdminAccessControlLoginLogsSystemDetailsPage pobj = new AdminAccessControlLoginLogsSystemDetailsPage(
					driver);

			fc.utobj().printTestStep("Navigate To Admin > Franchise Location > Add Franchise Location");
			fc.utobj().printTestStep("Add Franchise Location");
			String regionName = fc.utobj().generateTestData("Test");
			String storeType = fc.utobj().generateTestData("Test");
			String password = "t0n1ght123";
			String confirmPassword = password;
			String emailId = "commonautomation@staffex.com";
			String franchiseName = "TestFName";
			String centerName = fc.utobj().generateTestData("TestCenter");
			String openingDate = fc.utobj().getCurrentDateUSFormat();
			String loginId = fc.utobj().generateTestData("TestF");
			String ownerFirstName = fc.utobj().generateTestData("Testfname");
			String ownerLastName = fc.utobj().generateTestData("Testlname");
			AdminFranchiseLocationAddFranchiseLocationPageTest franchise = new AdminFranchiseLocationAddFranchiseLocationPageTest();
			franchise.addFranchiseLocationWithUser(driver, regionName, storeType, franchiseName, centerName,
					openingDate, password, confirmPassword, loginId, ownerFirstName, ownerLastName, config, emailId);
			String userFullName = ownerFirstName + " " + ownerLastName;

			fc.utobj().printTestStep("Navigate To Admin > Access Control > Login Logs > Users not Logged In");
			fc.adminpage().openLoginLogs(driver);
			fc.utobj().clickElement(driver, pobj.usersNotLoggedInTab);

			fc.utobj().printTestStep("Verify User Should Be Listed At Users not Logged In Tab");
			fc.utobj().selectDropDownByValue(driver, pobj.userType, "1");
			fc.utobj().clickElement(driver, pobj.searchBtn);
			fc.utobj().showAll(driver);

			boolean isTextPresent = fc.utobj().assertLinkPartialText(driver, userFullName);
			if (isTextPresent == false) {
				fc.utobj().throwsException("Not able to verify user at Not Logged in Tab");
			}

			fc.utobj().printTestStep("Logged Out");
			fc.home_page().logout(driver);

			fc.utobj().printTestStep("Login With Franchise User");
			fc.loginpage().loginWithParameter(driver, loginId, password);

			fc.utobj().printTestStep("Logged Out");
			fc.home_page().logout(driver);

			fc.utobj().printTestStep("Login With Admin User");
			fc.loginpage().loginWithParameter(driver, config.get("userName"), config.get("password"));

			fc.utobj().printTestStep("Navigate To Admin > Access Control > Login Logs > Users Logged In Tab");
			fc.adminpage().openLoginLogs(driver);
			fc.utobj().clickElement(driver, pobj.userLoggedInTab);

			fc.utobj().printTestStep("Verify User Should Be Listed At User Logged In Tab");

			fc.utobj().selectDropDownByValue(driver, pobj.userdetails, "1");
			fc.utobj().clickElement(driver, pobj.searchBtn);
			fc.utobj().showAll(driver);

			boolean isTextPresent3 = fc.utobj().assertLinkPartialText(driver, userFullName);
			if (isTextPresent3 == false) {
				fc.utobj().throwsException("Not able to verify that User should be listed at User Logged In Tab");
			}

			fc.utobj().printTestStep("Verify User Should Not Be Listed At Current Logged In Users Tab");
			fc.utobj().clickElement(driver, pobj.currentLoggedUserTab);
			fc.utobj().showAll(driver);

			boolean isTextPresent4 = fc.utobj().assertLinkPartialText(driver, userFullName);
			if (isTextPresent4 == true) {
				fc.utobj().throwsException(
						"Not able to verify that User Should not be listed at Current Logged In User Tab");
			}

			fc.utobj().printTestStep("Navigate To Admin > Access Control > User Account Summary");
			fc.adminpage().userAccountSummary(driver);

			fc.utobj().printTestStep("Select Inquiry Date > All");
			fc.utobj().selectDropDownByValue(driver,
					new AdminAccessControlUserAccountSummaryPage(driver).enquiryDateSelect, "55");
			fc.utobj().clickElement(driver, new AdminAccessControlUserAccountSummaryPage(driver).viewReport);

			fc.utobj().printTestStep(
					"Verify The User should be listed in user created list after clicking over user created count");
			fc.utobj().clickElement(driver, new AdminAccessControlUserAccountSummaryPage(driver).franchiseUserCount);
			fc.utobj().selectDropDownByValue(driver, new AdminAccessControlUserAccountSummaryPage(driver).userSelect,
					"1");
			fc.utobj().showAll(driver);

			boolean isTextPresent5 = fc.utobj().assertLinkPartialText(driver, userFullName);
			if (isTextPresent5 == false) {
				fc.utobj().throwsException(
						"Not able to verify that The User should be listed in user created list after clicking over user created count");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}
}
