package com.builds.test.admin;

import org.openqa.selenium.WebDriver;

import com.builds.uimaps.admin.AdminAccessControlSearchedLogsPage;
import com.builds.utilities.FranconnectUtil;

public class AdminAccessControlSearchedLogsPageTest {

	FranconnectUtil fc=new FranconnectUtil();
	
	public void searchedLogs(WebDriver driver,SearchedLogs searchedLogs) throws Exception {
		AdminAccessControlSearchedLogsPage pobj=new AdminAccessControlSearchedLogsPage(driver);
		fc.adminpage().adminAccessControlSearchedLogs(driver);
		fc.utobj().sendKeys(driver, pobj.searchedText, searchedLogs.getSearchedText());
		if (searchedLogs.getSearchedDate().equalsIgnoreCase("Today")) {
			fc.utobj().selectDropDownByVisibleText(driver, pobj.searchedDate, searchedLogs.getSearchedDate());
		}
		fc.utobj().clickElement(driver, pobj.searchBtn);
	}
}
