package com.builds.test.admin;

import java.util.Map;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Test;

import com.builds.uimaps.admin.AdminAreaRegionAddAreaRegionPage;
import com.builds.utilities.FranconnectUtil;
import com.builds.utilities.TestCase;

public class AdminAreaRegionAddAreaRegionPageTest {
	FranconnectUtil fc = new FranconnectUtil();

	@Test(groups = { "admin" })
	@TestCase(createdOn = "2017-10-31", updatedOn = "2017-10-31", testCaseId = "TC_Admin_AreaRegion_002", testCaseDescription = "Verify the Area / Region (States Based) is getting added / modified and deleted.")
	public void verifyAreaRegionStateAddModifyDelete() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("sales", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			AdminAreaRegionAddAreaRegionPage pobj = new AdminAreaRegionAddAreaRegionPage(driver);
			String regionName = fc.utobj().generateTestData(dataSet.get("regionName"));
			String state = dataSet.get("state");
			String country = dataSet.get("country");
			fc.utobj().printTestStep("Go to Admin > Area / Region > Add Area / Region");
			addAreaRegionWithState(driver, regionName, country, state);
			fc.utobj().showAll(driver);
			boolean isRegionAdded = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[contains(text(),'" + regionName + "')]");
			if (isRegionAdded == false) {
				fc.utobj().throwsException("Area/Region not getting added(group by State)");
			}
			fc.utobj().printTestStep("Modify and change the Area Region Info > Submit");
			fc.utobj().actionImgOption(driver, regionName, "Modify");
			String newRegionName = fc.utobj().generateTestData(dataSet.get("newRegionName"));
			fc.utobj().sendKeys(driver, pobj.aregRegionName, newRegionName);
			fc.utobj().clickElement(driver, pobj.Submit);
			fc.utobj().clickElement(driver, pobj.nextBtn);
			fc.utobj().showAll(driver);
			fc.utobj().printTestStep(" Verify that the Area / Region is modified");

			isRegionAdded = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[contains(text(),'" + newRegionName + "')]");

			if (isRegionAdded == false) {
				fc.utobj().throwsException("Area/Region not getting modified(group by State)");
			}
			fc.utobj().printTestStep("Delete the Area / Region");
			fc.utobj().actionImgOption(driver, newRegionName, "Delete");
			fc.utobj().acceptAlertBox(driver);
			fc.utobj().printTestStep("Verify that the Area / Region is not coming in the Manage Area / Region Page.");
			isRegionAdded = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[contains(text(),'" + newRegionName + "')]");
			if (isRegionAdded == true) {
				fc.utobj().throwsException("Area/Region not getting Deleted(group by State)");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}

	}

	@Test(groups = { "admin"})
	@TestCase(createdOn = "2017-10-31", updatedOn = "2017-10-31", testCaseId = "TC_Admin_AreaRegion_003", testCaseDescription = "Verify the Area / Region (Zip Codes) is getting added / modified and deleted.")
	public void verifyAreaRegionZipCodeAddModifyDelete() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("sales", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			AdminAreaRegionAddAreaRegionPage pobj = new AdminAreaRegionAddAreaRegionPage(driver);

			String regionName = fc.utobj().generateTestData(dataSet.get("regionName"));
			String newRegionName = fc.utobj().generateTestData(dataSet.get("newRegionName"));
			String zipCode = dataSet.get("zipCode");
			addAreaRegionWithZipCode(driver, regionName, zipCode);
			fc.utobj().showAll(driver);
			boolean isRegionAdded = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[contains(text(),'" + regionName + "')]");
			if (isRegionAdded == false) {
				fc.utobj().throwsException("Area/Region not getting added(group by ZipCode)");
			}
			fc.utobj().printTestStep("Modify and change the Area Region Info > Submit");
			fc.utobj().actionImgOption(driver, regionName, "Modify");
			fc.utobj().sendKeys(driver, pobj.aregRegionName, newRegionName);
			fc.utobj().clickElement(driver, pobj.Submit);
			fc.utobj().clickElement(driver, pobj.nextBtn);
			fc.utobj().showAll(driver);
			fc.utobj().printTestStep(" Verify that the Area / Region is modified");
			fc.utobj().showAll(driver);
			isRegionAdded = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[contains(text(),'" + newRegionName + "')]");

			if (isRegionAdded == false) {
				fc.utobj().throwsException("Area/Region not getting modified(group by State)");
			}
			fc.utobj().printTestStep("Delete the Area / Region");
			fc.utobj().actionImgOption(driver, newRegionName, "Delete");
			fc.utobj().acceptAlertBox(driver);

			fc.utobj().printTestStep("Verify that the Area / Region is not coming in the Manage Area / Region Page.");
			fc.utobj().showAll(driver);
			isRegionAdded = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[contains(text(),'" + newRegionName + "')]");
			if (isRegionAdded == true) {
				fc.utobj().throwsException("Area/Region not getting Deleted(group by State)");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}

	}

	@Test(groups = { "admin1121212" })
	@TestCase(createdOn = "2017-10-31", updatedOn = "2017-10-31", testCaseId = "TC_Admin_AreaRegion_004", testCaseDescription = "Verify the Area / Region(County) is getting added / modified and deleted.")
	public void verifyAreaRegionCountyAddModifyDelete() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("sales", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			AdminAreaRegionAddAreaRegionPage pobj = new AdminAreaRegionAddAreaRegionPage(driver);

			String regionName = fc.utobj().generateTestData(dataSet.get("regionName"));
			String newRegionName = fc.utobj().generateTestData(dataSet.get("newRegionName"));

			fc.utobj().printTestStep("Go to Admin > Area / Region > Add Area / Region");
			addAreaRegionWithCounty(driver, regionName);
			fc.utobj().showAll(driver);
			boolean isRegionAdded = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[contains(text(),'" + regionName + "')]");
			if (isRegionAdded == false) {
				fc.utobj().throwsException("Area/Region not getting added(group by ZipCode)");
			}
			fc.utobj().printTestStep("Modify and change the Area Region Info > Submit");
			fc.utobj().actionImgOption(driver, regionName, "Modify");
			fc.utobj().sendKeys(driver, pobj.aregRegionName, newRegionName);
			fc.utobj().clickElement(driver, pobj.Submit);
			fc.utobj().clickElement(driver, pobj.nextBtn);
			fc.utobj().showAll(driver);
			fc.utobj().printTestStep(" Verify that the Area / Region is modified");
			isRegionAdded = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[contains(text(),'" + newRegionName + "')]");
			if (isRegionAdded == false) {
				fc.utobj().throwsException("Area/Region not getting modified(group by State)");
			}
			fc.utobj().printTestStep("Delete the Area / Region");
			fc.utobj().actionImgOption(driver, newRegionName, "Delete");
			fc.utobj().acceptAlertBox(driver);
			fc.utobj().printTestStep("Verify that the Area / Region is not coming in the Manage Area / Region Page.");
			isRegionAdded = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[contains(text(),'" + newRegionName + "')]");
			if (isRegionAdded == true) {
				fc.utobj().throwsException("Area/Region not getting Deleted(group by State)");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}

	}

	public String addAreaRegion(WebDriver driver, String regionName) throws Exception {
			try {
				fc.adminpage().adminAreaRegionAddAreaRegionPage(driver);
				AdminAreaRegionAddAreaRegionPage pobj = new AdminAreaRegionAddAreaRegionPage(driver);
				fc.utobj().selectDropDown(driver, pobj.category, "Domestic");
				fc.utobj().sendKeys(driver, pobj.aregRegionName, regionName);
				fc.utobj().selectDropDown(driver, pobj.groupBy, "Zip Codes");
				fc.utobj().clickElement(driver, pobj.all);
				fc.utobj().clickElement(driver, pobj.commaSeparated);
				String zipCode = fc.utobj().generateRandomNumber6Digit();
				fc.utobj().sendKeys(driver, pobj.ziplist, zipCode);
				fc.utobj().clickElement(driver, pobj.Submit);

				boolean isPresent=false;
				
				try {
					fc.utobj().clickElement(driver, pobj.nextBtn);
					isPresent=true;
				} catch (Exception e) {
					isPresent=false;
				}
				
				if (isPresent==false) {
					fc.utobj().dismissAlertBox(driver);
					fc.utobj().getElement(driver, pobj.displayBtn).isDisplayed();
					fc.utobj().clickElement(driver, pobj.backBtn);
					zipCode = fc.utobj().generateRandomNumber();
					fc.utobj().clickElement(driver, pobj.all);
					fc.utobj().sendKeys(driver, pobj.ziplist, zipCode);
					fc.utobj().clickElement(driver, pobj.Submit);
					fc.utobj().clickElement(driver, pobj.nextBtn);
				}
			} catch (Exception e) {
				fc.utobj().throwsException("Not able to add Area/Region");
			}
		return regionName;
	}

	public String addAreaRegionWithZipCode(WebDriver driver, String regionName, String zipCode) throws Exception {

		String testCaseId = "TC_addAreaRegionAdmin_01";
		if (fc.utobj().validate(testCaseId)) {
			try {
				fc.adminpage().adminAreaRegionAddAreaRegionPage(driver);
				AdminAreaRegionAddAreaRegionPage pobj = new AdminAreaRegionAddAreaRegionPage(driver);
				fc.utobj().selectDropDown(driver, pobj.category, "Domestic");
				fc.utobj().sendKeys(driver, pobj.aregRegionName, regionName);
				fc.utobj().selectDropDown(driver, pobj.groupBy, "Zip Codes");
				fc.utobj().clickElement(driver, pobj.all);
				fc.utobj().clickElement(driver, pobj.commaSeparated);
				fc.utobj().sendKeys(driver, pobj.ziplist, zipCode);
				fc.utobj().clickElement(driver, pobj.Submit);
				fc.utobj().clickElement(driver, pobj.nextBtn);
				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException("was not able to add Area Region");
		}
		return regionName;
	}

	public String addAreaRegionWithState(WebDriver driver, String regionName, String Country, String state)
			throws Exception {

		fc.adminpage().adminAreaRegionAddAreaRegionPage(driver);
		AdminAreaRegionAddAreaRegionPage pobj = new AdminAreaRegionAddAreaRegionPage(driver);
		fc.utobj().selectDropDown(driver, pobj.category, "Domestic");
		fc.utobj().sendKeys(driver, pobj.aregRegionName, regionName);

		fc.utobj().selectDropDown(driver, pobj.groupBy, "States");
		fc.utobj().selectDropDownByVisibleText(driver, pobj.groupBy, "States");
		fc.utobj().clickElement(driver, pobj.all);
		fc.utobj().clickElement(driver, pobj.firstStateChkBox);
		fc.utobj().clickElement(driver, pobj.Submit);
		fc.utobj().clickElement(driver, pobj.nextBtn);
		return regionName;
	}

	public String addAreaRegionWithCounty(WebDriver driver, String regionName) throws Exception {

		fc.adminpage().adminAreaRegionAddAreaRegionPage(driver);
		AdminAreaRegionAddAreaRegionPage pobj = new AdminAreaRegionAddAreaRegionPage(driver);
		fc.utobj().selectDropDown(driver, pobj.category, "Domestic");
		fc.utobj().sendKeys(driver, pobj.aregRegionName, regionName);

		fc.utobj().selectDropDown(driver, pobj.groupBy, "County");
		fc.utobj().selectDropDownByVisibleText(driver, pobj.groupBy, "States");
		fc.utobj().clickElement(driver, pobj.all);
		fc.utobj().clickElement(driver, pobj.firstStateChkBox);
		fc.utobj().clickElement(driver, pobj.Submit);
		fc.utobj().clickElement(driver, pobj.nextBtn);
		return regionName;
	}

	public String addAreaRegionwithSQlite(WebDriver driver, String regionName,Map<String, String> dataSet) throws Exception {

		try {
			fc.adminpage().adminAreaRegionAddAreaRegionPage(driver);
			AdminAreaRegionAddAreaRegionPage pobj = new AdminAreaRegionAddAreaRegionPage(driver);
			fc.utobj().selectDropDown(driver, pobj.category,dataSet.get("RegCategory"));
			fc.utobj().sendKeys(driver, pobj.aregRegionName, regionName);
			fc.utobj().selectDropDown(driver, pobj.groupBy, dataSet.get("Groupby"));
			fc.utobj().clickElement(driver, pobj.all);
			fc.utobj().clickElement(driver, pobj.commaSeparated);
			String zipCode = fc.utobj().generateRandomNumber6Digit();
			fc.utobj().sendKeys(driver, pobj.ziplist, zipCode);
			fc.utobj().clickElement(driver, pobj.Submit);

			boolean isPresent=false;
			
			try {
				fc.utobj().clickElement(driver, pobj.nextBtn);
				isPresent=true;
			} catch (Exception e) {
				isPresent=false;
			}
			
			if (isPresent==false) {
				fc.utobj().dismissAlertBox(driver);
				fc.utobj().getElement(driver, pobj.displayBtn).isDisplayed();
				fc.utobj().clickElement(driver, pobj.backBtn);
				zipCode = fc.utobj().generateRandomNumber();
				fc.utobj().clickElement(driver, pobj.all);
				fc.utobj().sendKeys(driver, pobj.ziplist, zipCode);
				fc.utobj().clickElement(driver, pobj.Submit);
				fc.utobj().clickElement(driver, pobj.nextBtn);
			}
		} catch (Exception e) {
			fc.utobj().throwsException("Not able to add Area/Region");
		}
	return regionName;

		
	}
}
