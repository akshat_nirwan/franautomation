package com.builds.test.admin;

import java.util.List;
import java.util.Map;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.Optional;
import org.testng.annotations.Test;

import com.builds.uimaps.admin.AdminConfigurationConfigureCallStatusPage;
import com.builds.utilities.FranconnectUtil;
import com.builds.utilities.TestCase;

public class AdminConfigurationConfigureCallStatusPageTest {
	FranconnectUtil fc = new FranconnectUtil();

	@Test(groups = { "admin" })
	@TestCase(createdOn = "2017-11-06", updatedOn = "2017-11-06", testCaseId = "TC_Admin_Configure_Call_Status_001", testCaseDescription = "Verify Configure Call Status")
	private void callStatus() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("admin", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			AdminConfigurationConfigureCallStatusPage pobj = new AdminConfigurationConfigureCallStatusPage(driver);

			fc.utobj().printTestStep("Admin > Configuration > Configure Call Status");
			fc.adminpage().adminConfigureCallStatus(driver);

			fc.utobj().printTestStep("Add Call Status With Blank Text");
			fc.utobj().clickElement(driver, pobj.addCallStatus);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().sendKeys(driver, pobj.callStatusName, "");
			fc.utobj().clickElement(driver, pobj.addBtn);

			String isAlertTextPresent1 = fc.utobj().acceptAlertBox(driver);

			fc.utobj().printTestStep("Verify That Call Status Should Not Be Added With Blank Text");
			if (!isAlertTextPresent1.contains("Please enter Call Status.")) {
				fc.utobj().throwsException("Not able to verify that Blank Call Status should not be added");
			}

			fc.utobj().clickElement(driver, pobj.closeBtn);
			fc.utobj().switchFrameToDefault(driver);

			String callStatusName = fc.utobj().generateTestData(dataSet.get("callStatus"));
			fc.utobj().printTestStep("Add Call Status");
			fc.utobj().clickElement(driver, pobj.addCallStatus);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().sendKeys(driver, pobj.callStatusName, callStatusName);
			fc.utobj().clickElement(driver, pobj.addBtn);
			fc.utobj().clickElement(driver, pobj.closeBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Verify That Added Call Status");
			boolean isTextPresent = fc.utobj().searchInSelectBoxSingleValue(driver, pobj.listing, callStatusName);
			if (isTextPresent == false) {
				fc.utobj().throwsException("Not able to add Call Status");
			}

			fc.utobj().printTestStep("Modified Call Status");
			fc.utobj().selectDropDownByVisibleTextTrimed(driver, pobj.listing, callStatusName);
			fc.utobj().clickElement(driver, pobj.modifyBtn);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			callStatusName = fc.utobj().generateTestData("TestCM");
			fc.utobj().sendKeys(driver, pobj.callStatusName, callStatusName);
			fc.utobj().clickElement(driver, pobj.modifyBtn);
			fc.utobj().clickElement(driver, pobj.closeBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Verify The Modified Call Status");
			boolean isTextPresent1 = fc.utobj().searchInSelectBoxSingleValue(driver, pobj.listing, callStatusName);
			if (isTextPresent1 == false) {
				fc.utobj().throwsException("Not able to Modify Added Call Status");
			}

			String callStatus1 = fc.utobj().generateTestData("TestCa");
			fc.utobj().printTestStep("Add Another Call Status");
			fc.utobj().clickElement(driver, pobj.addCallStatus);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().sendKeys(driver, pobj.callStatusName, callStatus1);
			fc.utobj().clickElement(driver, pobj.addBtn);
			fc.utobj().clickElement(driver, pobj.closeBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Change Type Sequence");
			fc.utobj().selectDropDownByVisibleTextTrimed(driver, pobj.listing, callStatus1);
			fc.utobj().clickElement(driver, pobj.moveUp);
			fc.utobj().clickElement(driver, pobj.changeTypeSequence);

			fc.utobj().printTestStep("Verify The Change Type Sequence");

			Select sl = new Select(pobj.listing);
			List<WebElement> listWeb = sl.getOptions();
			String tempText = null;
			int a = 0;
			int b = 0;

			for (int i = 0; i < listWeb.size(); i++) {
				tempText = listWeb.get(i).getText();

				if (tempText != null && tempText.length() > 0) {

					if (tempText.equalsIgnoreCase(callStatus1)) {
						a = i;

					}
					if (tempText.equalsIgnoreCase(callStatusName)) {
						b = i;
					}
				}
			}

			if (a > b) {
				fc.utobj().throwsException("was not able to Change Call Status sequence");
			}

			fc.utobj().printTestStep("Verify That Duplicate Call Status Should not be added");
			fc.utobj().clickElement(driver, pobj.addCallStatus);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().sendKeys(driver, pobj.callStatusName, callStatus1);
			fc.utobj().clickElement(driver, pobj.addBtn);

			String isAlertTextPresent = fc.utobj().acceptAlertBox(driver);

			if (!isAlertTextPresent.contains("Call Status already exists")) {
				fc.utobj().throwsException("Not able to verify that duplicate Call Status should not be added");
			}

			fc.utobj().clickElement(driver, pobj.closeBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Add Call Status With Special Character");
			fc.utobj().clickElement(driver, pobj.addCallStatus);
			String taskTypeSp = fc.utobj().generateRandomSpecialChar();
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().sendKeys(driver, pobj.callStatusName, taskTypeSp);
			fc.utobj().clickElement(driver, pobj.addBtn);
			fc.utobj().clickElement(driver, pobj.closeBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Verify That Call Status With Special Character Should Be Added");
			boolean isTextPresent12 = fc.utobj().searchInSelectBoxSingleValue(driver, pobj.listing, taskTypeSp);
			if (isTextPresent12 == false) {
				fc.utobj()
						.throwsException("Not able to Verify That Call Status With Special Character Should Be Added");
			}

			fc.utobj().printTestStep("Delete The Added Call Status");
			fc.utobj().selectDropDownByVisibleTextTrimed(driver, pobj.listing, callStatusName);
			fc.utobj().clickElement(driver, pobj.deleteBtn);
			fc.utobj().acceptAlertBox(driver);

			boolean isTextPresent11 = fc.utobj().searchInSelectBoxSingleValue(driver, pobj.listing, callStatusName);
			if (isTextPresent11 == true) {
				fc.utobj().throwsException("Not able delete the added Call Status");
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	public String addCallStatus(WebDriver driver, @Optional() String callStatusName) throws Exception {

		String testCaseId = "TC_addCallStatusAdmin_01";
		if (fc.utobj().validate(testCaseId)) {
			try {
				boolean isValAdded = false;
				fc.adminpage().adminConfigureCallStatus(driver);
				AdminConfigurationConfigureCallStatusPage pobj = new AdminConfigurationConfigureCallStatusPage(driver);

				fc.utobj().clickElement(driver, pobj.addCallStatus);
				String windowHandle = driver.getWindowHandle();
				fc.commonMethods().switch_cboxIframe_frameId(driver);
				callStatusName = fc.utobj().generateTestData(callStatusName);
				callStatusName = callStatusName.concat(fc.utobj().generateRandomChar());
				fc.utobj().sendKeys(driver, pobj.callStatusName, callStatusName);
				fc.utobj().clickElement(driver, pobj.addBtn);

				fc.utobj().clickElement(driver, pobj.closeBtn);
				driver.switchTo().window(windowHandle);
				isValAdded = fc.utobj().searchInSelectBoxSingleValue(driver, pobj.listing, callStatusName);
				if (isValAdded == false) {
					fc.utobj().throwsException("Call Status Not Added!");
				}

				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException("was not able to add Call Status");
		}
		return callStatusName;
	}
}
