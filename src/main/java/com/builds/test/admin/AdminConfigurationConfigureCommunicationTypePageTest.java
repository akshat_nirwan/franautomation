package com.builds.test.admin;

import java.util.List;
import java.util.Map;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.Optional;
import org.testng.annotations.Test;

import com.builds.uimaps.admin.AdminConfigurationConfigureCommunicationTypePage;
import com.builds.utilities.FranconnectUtil;
import com.builds.utilities.TestCase;

public class AdminConfigurationConfigureCommunicationTypePageTest {
	FranconnectUtil fc = new FranconnectUtil();

	@Test(groups = { "admin" })
	@TestCase(createdOn = "2017-11-06", updatedOn = "2017-11-06", testCaseId = "TC_Admin_Configure_Communication_Type_001", testCaseDescription = "Verify Configure Communication Type")
	private void callStatus() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("admin", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			AdminConfigurationConfigureCommunicationTypePage pobj = new AdminConfigurationConfigureCommunicationTypePage(
					driver);

			fc.utobj().printTestStep("Admin > Configuration > Configure Communication Type");
			fc.adminpage().adminConfigurationCommunicationType(driver);

			fc.utobj().printTestStep("Add Communication Type With Blank Text");
			fc.utobj().clickElement(driver, pobj.addCommunicationType);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().sendKeys(driver, pobj.communicationTypeName, "");
			fc.utobj().clickElement(driver, pobj.addBtn);

			String isAlertTextPresent1 = fc.utobj().acceptAlertBox(driver);

			fc.utobj().printTestStep("Verify That Communication Type Should Not Be Added With Blank Text");
			if (!isAlertTextPresent1.contains("Please enter Communication Type.")) {
				fc.utobj().throwsException("Not able to verify that Blank Communication Type should not be added");
			}
			fc.utobj().clickElement(driver, pobj.CancelBtn);
			fc.utobj().switchFrameToDefault(driver);

			String communicationTypeName = fc.utobj().generateTestData("Testct");
			fc.utobj().printTestStep("Add Communication Type");
			fc.utobj().clickElement(driver, pobj.addCommunicationType);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().sendKeys(driver, pobj.communicationTypeName, communicationTypeName);
			fc.utobj().clickElement(driver, pobj.addBtn);
			fc.utobj().clickElement(driver, pobj.closeBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Verify That Added Communication Type");
			boolean isTextPresent = fc.utobj().searchInSelectBoxSingleValue(driver, pobj.listing,
					communicationTypeName);
			if (isTextPresent == false) {
				fc.utobj().throwsException("Not able to add Communication Type");
			}

			fc.utobj().printTestStep("Modified Communication Type");
			fc.utobj().selectDropDownByVisibleTextTrimed(driver, pobj.listing, communicationTypeName);
			fc.utobj().clickElement(driver, pobj.modifyBtn);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			communicationTypeName = fc.utobj().generateTestData("TestCtm");
			fc.utobj().sendKeys(driver, pobj.communicationTypeName, communicationTypeName);
			fc.utobj().clickElement(driver, pobj.modifyBtn);
			fc.utobj().clickElement(driver, pobj.closeBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Verify The Modified Communication Type");
			boolean isTextPresent1 = fc.utobj().searchInSelectBoxSingleValue(driver, pobj.listing,
					communicationTypeName);
			if (isTextPresent1 == false) {
				fc.utobj().throwsException("Not able to Modify Added Communication Type");
			}

			String callStatus1 = fc.utobj().generateTestData("TestCa");
			fc.utobj().printTestStep("Add Another Communication Type");
			fc.utobj().clickElement(driver, pobj.addCommunicationType);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().sendKeys(driver, pobj.communicationTypeName, callStatus1);
			fc.utobj().clickElement(driver, pobj.addBtn);
			fc.utobj().clickElement(driver, pobj.closeBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Change Type Sequence");
			fc.utobj().selectDropDownByVisibleTextTrimed(driver, pobj.listing, callStatus1);
			fc.utobj().clickElement(driver, pobj.moveUp);
			fc.utobj().clickElement(driver, pobj.changeTypeSequence);

			fc.utobj().printTestStep("Verify The Change Type Sequence");

			Select sl = new Select(pobj.listing);
			List<WebElement> listWeb = sl.getOptions();
			String tempText = null;
			int a = 0;
			int b = 0;

			for (int i = 0; i < listWeb.size(); i++) {
				tempText = listWeb.get(i).getText();

				if (tempText != null && tempText.length() > 0) {

					if (tempText.equalsIgnoreCase(callStatus1)) {
						a = i;

					}
					if (tempText.equalsIgnoreCase(communicationTypeName)) {
						b = i;
					}
				}
			}

			if (a > b) {
				fc.utobj().throwsException("was not able to Change Communication Type sequence");
			}

			fc.utobj().printTestStep("Verify That Duplicate Communication Type Should not be added");
			fc.utobj().clickElement(driver, pobj.addCommunicationType);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().sendKeys(driver, pobj.communicationTypeName, callStatus1);
			fc.utobj().clickElement(driver, pobj.addBtn);

			String isAlertTextPresent = fc.utobj().acceptAlertBox(driver);

			if (!isAlertTextPresent.contains("Communication Type already exists")) {
				fc.utobj().throwsException("Not able to verify that duplicate Communication Type should not be added");
			}

			fc.utobj().clickElement(driver, pobj.CancelBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Add Communication Type With Special Character");
			fc.utobj().clickElement(driver, pobj.addCommunicationType);
			String taskTypeSp = fc.utobj().generateRandomSpecialChar();
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().sendKeys(driver, pobj.communicationTypeName, taskTypeSp);
			fc.utobj().clickElement(driver, pobj.addBtn);
			fc.utobj().clickElement(driver, pobj.closeBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Verify That Communication Type With Special Character Should Be Added");
			boolean isTextPresent12 = fc.utobj().searchInSelectBoxSingleValue(driver, pobj.listing, taskTypeSp);
			if (isTextPresent12 == false) {
				fc.utobj().throwsException(
						"Not able to Verify That Communication Type With Special Character Should Be Added");
			}

			fc.utobj().printTestStep("Delete The Added Communication Type");
			fc.utobj().selectDropDownByVisibleTextTrimed(driver, pobj.listing, communicationTypeName);
			fc.utobj().clickElement(driver, pobj.deleteBtn);
			fc.utobj().acceptAlertBox(driver);

			boolean isTextPresent11 = fc.utobj().searchInSelectBoxSingleValue(driver, pobj.listing,
					communicationTypeName);
			if (isTextPresent11 == true) {
				fc.utobj().throwsException("Not able delete the added Communication Type");
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	public String addCommunicationType(WebDriver driver, @Optional() String communicationType) throws Exception {

		String testCaseId = "TC_addCommunicationType_01";
		if (fc.utobj().validate(testCaseId)) {
			try {
				boolean isValFound = false;
				fc.adminpage().adminConfigurationCommunicationType(driver);
				AdminConfigurationConfigureCommunicationTypePage pobj = new AdminConfigurationConfigureCommunicationTypePage(
						driver);

				fc.utobj().clickElement(driver, pobj.addCommunicationType);
				fc.commonMethods().switch_cboxIframe_frameId(driver);
				communicationType = fc.utobj().generateTestData(communicationType);
				communicationType = communicationType.concat(fc.utobj().generateRandomChar());
				fc.utobj().sendKeys(driver, pobj.communicationTypeName, communicationType);
				fc.utobj().clickElement(driver, pobj.addBtn);

				fc.utobj().clickElement(driver, pobj.closeBtn);
				isValFound = fc.utobj().searchInSelectBoxSingleValue(driver, pobj.listing, communicationType);
				if (isValFound == false) {
					fc.utobj().throwsException("Call Name Not Added!");
				}

				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException("was not able to add Communication Type");

		}
		return communicationType;
	}
}
