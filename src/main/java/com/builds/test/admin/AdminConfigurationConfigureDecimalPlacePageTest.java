package com.builds.test.admin;

import org.openqa.selenium.WebDriver;
import org.testng.Reporter;

import com.builds.uimaps.admin.AdminConfigurationConfigureDecimalPlacePage;
import com.builds.utilities.FranconnectUtil;

public class AdminConfigurationConfigureDecimalPlacePageTest {
	FranconnectUtil fc = new FranconnectUtil();

	public void configureDecimalPlace(WebDriver driver, String decimalPlaces) throws Exception {

		Reporter.log("Navigate To Admin > Configuration >  Configure Decimal Place");

		fc.adminpage().adminConfigurationDecimalPlace(driver);
		AdminConfigurationConfigureDecimalPlacePage pobj = new AdminConfigurationConfigureDecimalPlacePage(driver);

		switch (decimalPlaces) {
		case "one": {
			fc.utobj().clickElement(driver, pobj.decimal1);
			break;
		}
		case "two": {
			fc.utobj().clickElement(driver, pobj.decimal2);
			break;
		}

		case "three": {
			fc.utobj().clickElement(driver, pobj.decimal3);
			break;
		}

		case "four": {
			fc.utobj().clickElement(driver, pobj.decimal4);
			break;
		}
		default:
			break;
		}
		fc.utobj().clickElement(driver, pobj.updateBtn);
	}
}
