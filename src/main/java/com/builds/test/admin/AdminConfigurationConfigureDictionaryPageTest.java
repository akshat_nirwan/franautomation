package com.builds.test.admin;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Test;

import com.builds.uimaps.admin.AdminConfigurationConfigureDictionaryPage;
import com.builds.utilities.FranconnectUtil;
import com.builds.utilities.TestCase;

public class AdminConfigurationConfigureDictionaryPageTest {
	FranconnectUtil fc = new FranconnectUtil();

	@Test(groups = { "admin" })
	@TestCase(createdOn = "2017-11-02", updatedOn = "2017-11-02", testCaseId = "TC_Admin_Configure_Dictionary_001", testCaseDescription = "Verify Add , Search , Modify and Delete Of Dictionary Word")
	private void configureDictionary() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		// Map<String,String> dataSet = fc.utobj().readTestData("admin",
		// testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			AdminConfigurationConfigureDictionaryPage pobj = new AdminConfigurationConfigureDictionaryPage(driver);
			fc.utobj().printTestStep("Navigate To Admin > COnfiguration >  Configure Dictionary");
			fc.adminpage().adminConfigurationConfigureDictionary(driver);

			fc.utobj().printTestStep("Add New Word");
			String wordName = fc.utobj().generateRandomChar();
			fc.utobj().clickElement(driver, pobj.addNewWords);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().sendKeys(driver, pobj.wordName, wordName);
			fc.utobj().clickElement(driver, pobj.addBtn);
			fc.utobj().clickElement(driver, pobj.closeBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Search The Newly Added Word");
			fc.utobj().sendKeys(driver, pobj.searchField, wordName);
			fc.utobj().clickElement(driver, pobj.searchButton);

			boolean isTextPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//td[contains(text(),'" + wordName + "')]");
			if (isTextPresent == false) {
				fc.utobj().throwsException("was not able to verify the added Word By Search");
			}

			fc.utobj().printTestStep("Delete The Added Word");
			fc.utobj().actionImgOption(driver, wordName, "Delete");

			fc.utobj().acceptAlertBox(driver);

			fc.utobj().printTestStep("Search The Deleted Word");
			fc.utobj().sendKeys(driver, pobj.searchField, wordName);
			fc.utobj().clickElement(driver, pobj.searchButton);

			fc.utobj().printTestStep("Verify The Deleted Word");
			boolean isDeletedText = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//td[contains(text(),'No records found.')]");
			if (isDeletedText == false) {
				fc.utobj().throwsException("Not able to delete the word");
			}

			fc.utobj().printTestStep("Add The Another Word");
			String wordName1 = fc.utobj().generateRandomChar();
			fc.utobj().clickElement(driver, pobj.addNewWords);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().sendKeys(driver, pobj.wordName, wordName1);
			fc.utobj().clickElement(driver, pobj.addBtn);
			fc.utobj().clickElement(driver, pobj.closeBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Modify The Newly Added Word");
			fc.utobj().printTestStep("Search The Word");
			fc.utobj().sendKeys(driver, pobj.searchField, wordName1);
			fc.utobj().clickElement(driver, pobj.searchButton);

			fc.utobj().actionImgOption(driver, wordName1, "Modify");
			wordName1 = fc.utobj().generateRandomChar();
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().sendKeys(driver, pobj.wordName, wordName1);
			fc.utobj().clickElement(driver, pobj.addBtn);
			fc.utobj().clickElement(driver, pobj.closeBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Verify The Modified Word");
			fc.utobj().printTestStep("Search The Word");
			fc.utobj().sendKeys(driver, pobj.searchField, wordName1);
			fc.utobj().clickElement(driver, pobj.searchButton);

			boolean isTextPresent1 = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//td[contains(text(),'" + wordName1 + "')]");
			if (isTextPresent1 == false) {
				fc.utobj().throwsException("was not able to verify the Modified Word By Search");
			}

			fc.utobj().printTestStep("Delete The Dictionary Word Through Delete Button");
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//td[contains(text(),'" + wordName1 + "')]/ancestor::tr/td/input[@name='selectWordCB']"));
			fc.utobj().clickElement(driver, pobj.delete);
			fc.utobj().acceptAlertBox(driver);

			fc.utobj().printTestStep("Search The Deleted Word");
			fc.utobj().sendKeys(driver, pobj.searchField, wordName1);
			fc.utobj().clickElement(driver, pobj.searchButton);

			fc.utobj().printTestStep("Verify The Deleted Word");
			boolean isDeletedText1 = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//td[contains(text(),'No records found.')]");
			if (isDeletedText1 == false) {
				fc.utobj().throwsException("Not able to delete the word");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "admin" })
	@TestCase(createdOn = "2017-11-06", updatedOn = "2017-11-06", testCaseId = "TC_Admin_Configure_Dictionary_002", testCaseDescription = "Verify dictionary word validation")
	private void configureDictionary002() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		try {
			driver = fc.loginpage().login(driver);
			AdminConfigurationConfigureDictionaryPage pobj = new AdminConfigurationConfigureDictionaryPage(driver);
			fc.utobj().printTestStep("Navigate To Admin > COnfiguration >  Configure Dictionary");
			fc.adminpage().adminConfigurationConfigureDictionary(driver);

			fc.utobj().printTestStep("Add a word without any text > the word should not be submitted");
			fc.utobj().printTestStep("Add New Word");
			fc.utobj().clickElement(driver, pobj.addNewWords);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().sendKeys(driver, pobj.wordName, "");
			fc.utobj().clickElement(driver, pobj.addBtn);

			String alertText = fc.utobj().acceptAlertBox(driver);

			if (!alertText.contains("Please enter Word")) {
				fc.utobj().throwsException("Not able to verify that word without any text should not be submitted");
			}
			fc.utobj().clickElement(driver, pobj.closeBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Verify That Duplicate word should not be added");
			fc.utobj().printTestStep("Add New Word");
			String wordName = fc.utobj().generateRandomChar();
			fc.utobj().clickElement(driver, pobj.addNewWords);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().sendKeys(driver, pobj.wordName, wordName);
			fc.utobj().clickElement(driver, pobj.addBtn);
			fc.utobj().clickElement(driver, pobj.closeBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Add New Word With Same Name");
			fc.utobj().clickElement(driver, pobj.addNewWords);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().sendKeys(driver, pobj.wordName, wordName);
			fc.utobj().clickElement(driver, pobj.addBtn);

			if (!"Word already exists.".equals(fc.utobj().acceptAlertBox(driver))) {
				fc.utobj().throwsException("Not able to verify that Duplicate word should not be added");
			}
			fc.utobj().clickElement(driver, pobj.closeBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "admin", "admin201032" })
	@TestCase(createdOn = "2017-11-06", updatedOn = "2017-11-06", testCaseId = "TC_Admin_Configure_Dictionary_003", testCaseDescription = "Verify multiple words are added in dictionary at one time")
	private void configureDictionary003() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		// Map<String,String> dataSet = fc.utobj().readTestData("admin",
		// testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			AdminConfigurationConfigureDictionaryPage pobj = new AdminConfigurationConfigureDictionaryPage(driver);
			fc.utobj().printTestStep("Navigate To Admin > COnfiguration >  Configure Dictionary");
			fc.adminpage().adminConfigurationConfigureDictionary(driver);

			String wordFirst = fc.utobj().generateRandomChar();
			String wordSecond = fc.utobj().generateRandomChar();
			String wordThird = fc.utobj().generateRandomChar();
			fc.utobj().printTestStep("Add A multiple words");
			fc.utobj().clickElement(driver, pobj.addNewWords);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().sendKeys(driver, pobj.wordName, wordFirst + "," + wordSecond + "," + wordThird);
			fc.utobj().clickElement(driver, pobj.addBtn);
			fc.utobj().clickElement(driver, pobj.closeBtn);

			// fc.commonMethods().Click_Close_Input_ByValue(driver);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Verify The Added Multiple Words");

			fc.utobj().printTestStep("Search The First Word Added Word");
			fc.utobj().sendKeys(driver, pobj.searchField, wordFirst);
			fc.utobj().clickElement(driver, pobj.searchButton);

			boolean isTextPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//td[contains(text(),'" + wordFirst + "')]");
			if (isTextPresent == false) {
				fc.utobj().throwsException("was not able to verify the First added Word By Search");
			}

			fc.utobj().printTestStep("Search The Second Word Added Word");
			fc.utobj().sendKeys(driver, pobj.searchField, wordSecond);
			fc.utobj().clickElement(driver, pobj.searchButton);

			boolean isTextPresent1 = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//td[contains(text(),'" + wordSecond + "')]");
			if (isTextPresent1 == false) {
				fc.utobj().throwsException("was not able to verify the Second added Word By Search");
			}

			fc.utobj().printTestStep("Search The Third Word Added Word");
			fc.utobj().sendKeys(driver, pobj.searchField, wordThird);
			fc.utobj().clickElement(driver, pobj.searchButton);

			boolean isTextPresent2 = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//td[contains(text(),'" + wordThird + "')]");
			if (isTextPresent2 == false) {
				fc.utobj().throwsException("was not able to verify the Third added Word By Search");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}
}
