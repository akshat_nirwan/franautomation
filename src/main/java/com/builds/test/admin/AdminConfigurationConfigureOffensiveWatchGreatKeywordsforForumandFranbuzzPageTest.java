package com.builds.test.admin;

import java.util.List;
import java.util.Map;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.Test;

import com.builds.test.common.CorporateUser;
import com.builds.uimaps.admin.AdminConfigurationConfigureOffensiveWatchGreatKeywordsforForumandFranbuzzPage;
import com.builds.utilities.FranconnectUtil;
import com.builds.utilities.TestCase;

public class AdminConfigurationConfigureOffensiveWatchGreatKeywordsforForumandFranbuzzPageTest {
	FranconnectUtil fc = new FranconnectUtil();

	@Test(groups = { "admin" })
	@TestCase(createdOn = "2017-11-07", updatedOn = "2017-11-07", testCaseId = "TC_Admin_Configure_Offensive_Word_001", testCaseDescription = "Verify that Offensive words are being added ,modified and removed from the list of words in Offensive keywords")
	private void offensiveKeyword() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("admin", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			AdminConfigurationConfigureOffensiveWatchGreatKeywordsforForumandFranbuzzPage pobj = new AdminConfigurationConfigureOffensiveWatchGreatKeywordsforForumandFranbuzzPage(
					driver);
			fc.utobj().printTestStep("Navigate To Admin > Configuration > Configure Offensive KeyWords");
			fc.adminpage().adminConfigurationConfigureOffensiveWatchGreatKeywords(driver);

			fc.utobj().printTestStep("Click Over Offensive Keyword Tab");
			fc.utobj().clickElement(driver, pobj.offensiveWord);

			fc.utobj().printTestStep("Add Offensive KeyWord without any text");
			fc.utobj().clickElement(driver, pobj.addOffensiveWord);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().sendKeys(driver, pobj.keywordName, "");
			fc.utobj().clickElement(driver, pobj.addBtn);

			fc.utobj().printTestStep("Verify validation alert should come");

			String alertText = fc.utobj().acceptAlertBox(driver);

			if (!alertText.equalsIgnoreCase("Please enter Keyword.")) {
				fc.utobj().throwsException("Not able to verify that Add Offensive Keyword without any text");
			}
			fc.utobj().clickElement(driver, pobj.cancleBtn);

			fc.utobj().printTestStep("Add Offensive KeyWords with text");
			String offensiveKeyword = fc.utobj().generateTestData(dataSet.get("offensiveKeyword"));
			fc.utobj().clickElement(driver, pobj.addOffensiveWord);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().sendKeys(driver, pobj.keywordName, offensiveKeyword);
			fc.utobj().clickElement(driver, pobj.addBtn);
			fc.utobj().clickElement(driver, pobj.closeBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Verify The Added Offensive Keyword");
			List<WebElement> listElement = new Select(pobj.selectOffensiveWord).getOptions();

			boolean isTextPresent = false;

			for (WebElement webElement : listElement) {
				String addedText = webElement.getText();

				if (addedText != null && addedText.length() > 0) {

					addedText = addedText.trim();

					if (addedText.equalsIgnoreCase(offensiveKeyword)) {
						isTextPresent = true;
						break;
					}
				}
			}

			if (isTextPresent == false) {
				fc.utobj().throwsException("Not able to added Offensive Keyword");
			}

			fc.utobj().printTestStep("Modify Added Offensive KeyWords");
			fc.utobj().selectDropDown(driver, pobj.selectOffensiveWord, offensiveKeyword);
			fc.utobj().clickElement(driver, pobj.modifyOffensiveWord);
			offensiveKeyword = fc.utobj().generateTestData("TestMo");
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().sendKeys(driver, pobj.keywordName, offensiveKeyword);
			fc.utobj().clickElement(driver, pobj.modifyCboxBtn);
			fc.utobj().clickElement(driver, pobj.closeBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Verify The Modified Offensive Keyword");
			List<WebElement> listElement1 = new Select(pobj.selectOffensiveWord).getOptions();

			boolean isTextPresent1 = false;

			for (WebElement webElement : listElement1) {
				String addedText = webElement.getText();

				if (addedText != null && addedText.length() > 0) {

					addedText = addedText.trim();

					if (addedText.equalsIgnoreCase(offensiveKeyword)) {
						isTextPresent1 = true;
						break;
					}
				}
			}

			if (isTextPresent1 == false) {
				fc.utobj().throwsException("Not able to Modify added Offensive Keyword");
			}

			fc.utobj().printTestStep("Add another Offensive KeyWords with same name");
			fc.utobj().clickElement(driver, pobj.addOffensiveWord);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().sendKeys(driver, pobj.keywordName, offensiveKeyword);
			fc.utobj().clickElement(driver, pobj.addBtn);

			fc.utobj().printTestStep("Verify The Validation Of duplicate offensive word");
			String alertText1 = fc.utobj().acceptAlertBox(driver);

			if (!alertText1.contains("Entered Keyword already exist under Offensive Keywords Section")) {
				fc.utobj().throwsException("Not able to verify that add duplicate offensive Keyword");
			}

			fc.utobj().clickElement(driver, pobj.cancleBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Delete Offensive KeyWord");
			fc.utobj().selectDropDown(driver, pobj.selectOffensiveWord, offensiveKeyword);
			fc.utobj().clickElement(driver, pobj.removeyOffensiveWord);
			fc.utobj().acceptAlertBox(driver);

			fc.utobj().printTestStep("Verify The Delete Offensive Keyword");
			List<WebElement> listElement2 = new Select(pobj.selectOffensiveWord).getOptions();

			boolean isTextPresent2 = false;

			for (WebElement webElement : listElement2) {
				String addedText = webElement.getText();

				if (addedText != null && addedText.length() > 0) {

					addedText = addedText.trim();

					if (addedText.equalsIgnoreCase(offensiveKeyword)) {
						isTextPresent2 = true;
						break;
					}
				}
			}

			if (isTextPresent2 == true) {
				fc.utobj().throwsException("Not able to Delete Offensive Keyword");
			}

			fc.utobj().clickElement(driver, pobj.saveBtn);

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "admin" })
	@TestCase(createdOn = "2017-11-07", updatedOn = "2017-11-07", testCaseId = "TC_Admin_Configure_Watch_Keyword_001", testCaseDescription = "Verify that Watch words are being added ,modified and removed from the list of words in Watch keywords")
	private void watchKeyword() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("admin", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			AdminConfigurationConfigureOffensiveWatchGreatKeywordsforForumandFranbuzzPage pobj = new AdminConfigurationConfigureOffensiveWatchGreatKeywordsforForumandFranbuzzPage(
					driver);
			fc.utobj().printTestStep("Navigate To Admin > Configuration > Configure Watch KeyWords");
			fc.adminpage().adminConfigurationConfigureOffensiveWatchGreatKeywords(driver);

			fc.utobj().printTestStep("Click Over Watch Keyword Tab");
			fc.utobj().clickElement(driver, pobj.offensiveWord);

			fc.utobj().printTestStep("Add Watch KeyWord without any text");
			fc.utobj().clickElement(driver, pobj.addWatchWord);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().sendKeys(driver, pobj.keywordName, "");
			fc.utobj().clickElement(driver, pobj.addBtn);

			fc.utobj().printTestStep("Verify validation alert should come");

			String alertText = fc.utobj().acceptAlertBox(driver);

			if (!alertText.equalsIgnoreCase("Please enter Keyword.")) {
				fc.utobj().throwsException("Not able to verify that Add Watch Keyword without any text");
			}
			fc.utobj().clickElement(driver, pobj.cancleBtn);

			fc.utobj().printTestStep("Add Watch KeyWords with text");
			String watchKeyword = fc.utobj().generateTestData(dataSet.get("watchKeyword"));
			fc.utobj().clickElement(driver, pobj.addWatchWord);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().sendKeys(driver, pobj.keywordName, watchKeyword);
			fc.utobj().clickElement(driver, pobj.addBtn);
			fc.utobj().clickElement(driver, pobj.closeBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Verify The Added Watch Keyword");
			List<WebElement> listElement = new Select(pobj.selectWatchWord).getOptions();

			boolean isTextPresent = false;

			for (WebElement webElement : listElement) {
				String addedText = webElement.getText();

				if (addedText != null && addedText.length() > 0) {

					addedText = addedText.trim();

					if (addedText.equalsIgnoreCase(watchKeyword)) {
						isTextPresent = true;
						break;
					}
				}
			}

			if (isTextPresent == false) {
				fc.utobj().throwsException("Not able to added Watch Keyword");
			}

			fc.utobj().printTestStep("Modify Added Watch KeyWords");
			fc.utobj().selectDropDown(driver, pobj.selectWatchWord, watchKeyword);
			fc.utobj().clickElement(driver, pobj.modifyWatchWord);
			watchKeyword = fc.utobj().generateTestData("TestMw");
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().sendKeys(driver, pobj.keywordName, watchKeyword);
			fc.utobj().clickElement(driver, pobj.modifyCboxBtn);
			fc.utobj().clickElement(driver, pobj.closeBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Verify The Modified Watch Keyword");
			List<WebElement> listElement1 = new Select(pobj.selectWatchWord).getOptions();

			boolean isTextPresent1 = false;

			for (WebElement webElement : listElement1) {
				String addedText = webElement.getText();

				if (addedText != null && addedText.length() > 0) {

					addedText = addedText.trim();

					if (addedText.equalsIgnoreCase(watchKeyword)) {
						isTextPresent1 = true;
						break;
					}
				}
			}

			if (isTextPresent1 == false) {
				fc.utobj().throwsException("Not able to Modify added Watch Keyword");
			}

			fc.utobj().printTestStep("Add another Watch KeyWords with same name");
			fc.utobj().clickElement(driver, pobj.addWatchWord);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().sendKeys(driver, pobj.keywordName, watchKeyword);
			fc.utobj().clickElement(driver, pobj.addBtn);

			fc.utobj().printTestStep("Verify The Validation Of duplicate Watch word");
			String alertText1 = fc.utobj().acceptAlertBox(driver);

			if (!alertText1.contains("Entered Keyword already exist in Watch Keywords Section")) {
				fc.utobj().throwsException("Not able to verify that add duplicate Watch Keyword");
			}

			fc.utobj().clickElement(driver, pobj.cancleBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Delete Watch KeyWord");
			fc.utobj().selectDropDown(driver, pobj.selectWatchWord, watchKeyword);
			fc.utobj().clickElement(driver, pobj.removeyWatchWord);
			fc.utobj().acceptAlertBox(driver);

			fc.utobj().printTestStep("Verify The Delete Watch Keyword");
			List<WebElement> listElement2 = new Select(pobj.selectWatchWord).getOptions();

			boolean isTextPresent2 = false;

			for (WebElement webElement : listElement2) {
				String addedText = webElement.getText();

				if (addedText != null && addedText.length() > 0) {

					addedText = addedText.trim();

					if (addedText.equalsIgnoreCase(watchKeyword)) {
						isTextPresent2 = true;
						break;
					}
				}
			}

			if (isTextPresent2 == true) {
				fc.utobj().throwsException("Not able to Delete Watch Keyword");
			}

			fc.utobj().clickElement(driver, pobj.saveBtn);

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "admin" })
	@TestCase(createdOn = "2017-11-07", updatedOn = "2017-11-07", testCaseId = "TC_Admin_Configure_Great_Keyword_001", testCaseDescription = "Verify that Great words are being added ,modified and removed from the list of words in Watch keywords")
	private void greatKeyword() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("admin", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			AdminConfigurationConfigureOffensiveWatchGreatKeywordsforForumandFranbuzzPage pobj = new AdminConfigurationConfigureOffensiveWatchGreatKeywordsforForumandFranbuzzPage(
					driver);
			fc.utobj().printTestStep("Navigate To Admin > Configuration > Configure Great KeyWords");
			fc.adminpage().adminConfigurationConfigureOffensiveWatchGreatKeywords(driver);

			fc.utobj().printTestStep("Click Over Great Keyword Tab");
			fc.utobj().clickElement(driver, pobj.offensiveWord);

			fc.utobj().printTestStep("Add Great KeyWord without any text");
			fc.utobj().clickElement(driver, pobj.addGreatWord);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().sendKeys(driver, pobj.keywordName, "");
			fc.utobj().clickElement(driver, pobj.addBtn);

			fc.utobj().printTestStep("Verify validation alert should come");

			String alertText = fc.utobj().acceptAlertBox(driver);

			if (!alertText.equalsIgnoreCase("Please enter Keyword.")) {
				fc.utobj().throwsException("Not able to verify that Add Great Keyword without any text");
			}
			fc.utobj().clickElement(driver, pobj.cancleBtn);

			fc.utobj().printTestStep("Add Great KeyWords with text");
			String greatKeyword = fc.utobj().generateTestData(dataSet.get("greatKeyword"));
			fc.utobj().clickElement(driver, pobj.addGreatWord);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().sendKeys(driver, pobj.keywordName, greatKeyword);
			fc.utobj().clickElement(driver, pobj.addBtn);
			fc.utobj().clickElement(driver, pobj.closeBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Verify The Added Great Keyword");
			List<WebElement> listElement = new Select(pobj.selectGreatWord).getOptions();

			boolean isTextPresent = false;

			for (WebElement webElement : listElement) {
				String addedText = webElement.getText();

				if (addedText != null && addedText.length() > 0) {

					addedText = addedText.trim();

					if (addedText.equalsIgnoreCase(greatKeyword)) {
						isTextPresent = true;
						break;
					}
				}
			}

			if (isTextPresent == false) {
				fc.utobj().throwsException("Not able to added Great Keyword");
			}

			fc.utobj().printTestStep("Modify Added Great KeyWords");
			fc.utobj().selectDropDown(driver, pobj.selectGreatWord, greatKeyword);
			fc.utobj().clickElement(driver, pobj.modifyGreatWord);
			greatKeyword = fc.utobj().generateTestData("TestMw");
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().sendKeys(driver, pobj.keywordName, greatKeyword);
			fc.utobj().clickElement(driver, pobj.modifyCboxBtn);
			fc.utobj().clickElement(driver, pobj.closeBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Verify The Modified Great Keyword");
			List<WebElement> listElement1 = new Select(pobj.selectGreatWord).getOptions();

			boolean isTextPresent1 = false;

			for (WebElement webElement : listElement1) {
				String addedText = webElement.getText();

				if (addedText != null && addedText.length() > 0) {

					addedText = addedText.trim();

					if (addedText.equalsIgnoreCase(greatKeyword)) {
						isTextPresent1 = true;
						break;
					}
				}
			}

			if (isTextPresent1 == false) {
				fc.utobj().throwsException("Not able to Modify added Great Keyword");
			}

			fc.utobj().printTestStep("Add another Great KeyWords with same name");
			fc.utobj().clickElement(driver, pobj.addGreatWord);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().sendKeys(driver, pobj.keywordName, greatKeyword);
			fc.utobj().clickElement(driver, pobj.addBtn);

			fc.utobj().printTestStep("Verify The Validation Of duplicate Great word");
			String alertText1 = fc.utobj().acceptAlertBox(driver);

			if (!alertText1.contains("Entered Keyword already exist under Great Keywords Section")) {
				fc.utobj().throwsException("Not able to verify that add duplicate Great Keyword");
			}

			fc.utobj().clickElement(driver, pobj.cancleBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Delete Great KeyWord");
			fc.utobj().selectDropDown(driver, pobj.selectGreatWord, greatKeyword);
			fc.utobj().clickElement(driver, pobj.removeyGreatWord);
			fc.utobj().acceptAlertBox(driver);

			fc.utobj().printTestStep("Verify The Delete Great Keyword");
			List<WebElement> listElement2 = new Select(pobj.selectGreatWord).getOptions();

			boolean isTextPresent2 = false;

			for (WebElement webElement : listElement2) {
				String addedText = webElement.getText();

				if (addedText != null && addedText.length() > 0) {

					addedText = addedText.trim();

					if (addedText.equalsIgnoreCase(greatKeyword)) {
						isTextPresent2 = true;
						break;
					}
				}
			}

			if (isTextPresent2 == true) {
				fc.utobj().throwsException("Not able to Delete Great Keyword");
			}

			fc.utobj().clickElement(driver, pobj.saveBtn);

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "admin"})
	@TestCase(createdOn = "2017-11-07", updatedOn = "2017-11-07", testCaseId = "TC_Admin_Configure_Email_Contacts_001", testCaseDescription = "Verify The Configuration of Email Contacts For New Users")
	private void emailContactsNewUser() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("admin", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			AdminConfigurationConfigureOffensiveWatchGreatKeywordsforForumandFranbuzzPage pobj = new AdminConfigurationConfigureOffensiveWatchGreatKeywordsforForumandFranbuzzPage(
					driver);
			fc.utobj().printTestStep(
					"Navigate To Admin > Configuration > Configure Offensive Watch Great Keywords for Forumand Franbuzz Page");
			fc.adminpage().adminConfigurationConfigureOffensiveWatchGreatKeywords(driver);

			fc.utobj().printTestStep("Navigate To Admin > Configuration > Email Contacts");
			fc.utobj().clickElement(driver, pobj.offensiveEmailContactTab);

			fc.utobj().printTestStep("Add New Contact For New User");
			fc.utobj().clickElement(driver, pobj.addNewContact);
			fc.commonMethods().switch_cboxIframe_frameId(driver);

			if (!fc.utobj().isSelected(driver, pobj.newUserRBtn)) {
				fc.utobj().clickElement(driver, pobj.newUserRBtn);
			}
			String firstName = fc.utobj().generateTestData(dataSet.get("firstName"));
			String lastName = fc.utobj().generateTestData(dataSet.get("lastName"));
			String emailId = "commonautomation@staffex.com";
			fc.utobj().sendKeys(driver, pobj.firstName, firstName);
			fc.utobj().sendKeys(driver, pobj.lastName, lastName);
			fc.utobj().sendKeys(driver, pobj.emailID, emailId);

			if (!fc.utobj().isSelected(driver,pobj.franBuzzPostChk)) {
				fc.utobj().clickElement(driver, pobj.franBuzzPostChk);
			}

			if (!fc.utobj().isSelected(driver, pobj.forumPostChk)) {
				fc.utobj().clickElement(driver, pobj.forumPostChk);
			}
			fc.utobj().clickElement(driver, pobj.createBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Verify The Added New Contact");
			fc.utobj().showAll(driver);
			boolean isFirstNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//td[contains(text(),'" + firstName + "')]");
			if (isFirstNamePresent == false) {
				fc.utobj().throwsException("Not able to add New Contact");
			}

			fc.utobj().printTestStep("Modify the Added New Contact");
			fc.utobj().actionImgOption(driver, firstName, "Modify");

			fc.commonMethods().switch_cboxIframe_frameId(driver);
			firstName = fc.utobj().generateTestData("Testfname");
			fc.utobj().sendKeys(driver, pobj.firstName, firstName);
			fc.utobj().sendKeys(driver, pobj.lastName, lastName);
			fc.utobj().sendKeys(driver, pobj.emailID, emailId);
			fc.utobj().clickElement(driver, pobj.saveBtnAtCbox);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Verify The Modified New Contact");
			fc.utobj().showAll(driver);
			boolean isFirstNamePresent1 = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//td[contains(text(),'" + firstName + "')]");
			if (isFirstNamePresent1 == false) {
				fc.utobj().throwsException("Not able to Modify the Added New Contact");
			}

			fc.utobj().printTestStep("Delete the Modified New Contact");
			fc.utobj().actionImgOption(driver, firstName, "Delete");
			fc.utobj().acceptAlertBox(driver);

			fc.utobj().printTestStep("Verify The Deleted New Contact");
			fc.utobj().showAll(driver);
			boolean isFirstNamePresent2 = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//td[contains(text(),'" + firstName + "')]");
			if (isFirstNamePresent2 == true) {
				fc.utobj().throwsException("Not able to Delete the Modified New Contact");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "admin21"})
	@TestCase(createdOn = "2017-11-07", updatedOn = "2017-11-07", testCaseId = "TC_Admin_Configure_Email_Contacts_002", testCaseDescription = "Verify The Configuration of Email Contacts For Existing Users")
	private void emailContactsExistingUser() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("admin", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			AdminConfigurationConfigureOffensiveWatchGreatKeywordsforForumandFranbuzzPage pobj = new AdminConfigurationConfigureOffensiveWatchGreatKeywordsforForumandFranbuzzPage(
					driver);

			fc.utobj().printTestStep("Navigate To Admin > Users > Manage Corporate Users");
			String emailId = dataSet.get("emailId");
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporate_user = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();

			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporate_user.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			fc.utobj().printTestStep(
					"Navigate To Admin > Configuration > Configure Offensive Watch Great Keywords for Forumand Franbuzz Page");
			fc.adminpage().adminConfigurationConfigureOffensiveWatchGreatKeywords(driver);

			fc.utobj().printTestStep("Navigate To Admin > Configuration > Email Contacts");
			fc.utobj().clickElement(driver, pobj.offensiveEmailContactTab);

			fc.utobj().printTestStep("Add New Contact For Existing User");
			fc.utobj().clickElement(driver, pobj.addNewContact);
			fc.commonMethods().switch_cboxIframe_frameId(driver);

			if (!fc.utobj().isSelected(driver, pobj.existUserRBtn)){
				fc.utobj().clickElement(driver, pobj.existUserRBtn);
			}

			fc.utobj().selectDropDown(driver, pobj.corporateuser, corpUser.getuserFullName());
			if (!fc.utobj().isSelected(driver, pobj.franBuzzPostChk)) {
				fc.utobj().clickElement(driver, pobj.franBuzzPostChk);
			}

			if (!fc.utobj().isSelected(driver,pobj.forumPostChk)) {
				fc.utobj().clickElement(driver, pobj.forumPostChk);
			}
			fc.utobj().clickElement(driver, pobj.createBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Verify The Added New Contact");
			fc.utobj().showAll(driver);
			boolean isFirstNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text(),'" + corpUser.getuserFullName() + "')]");
			if (isFirstNamePresent == false) {
				fc.utobj().throwsException("Not able to add New Contact");
			}

			fc.utobj().printTestStep("Modify the Added New Contact");
			fc.utobj().actionImgOption(driver, corpUser.getuserFullName(), "Modify");
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().selectDropDown(driver, pobj.corporateuser, corpUser.getuserFullName());
			if (!fc.utobj().isSelected(driver, pobj.franBuzzPostChk)) {
				fc.utobj().clickElement(driver, pobj.franBuzzPostChk);
			}

			if (!fc.utobj().isSelected(driver, pobj.forumPostChk)) {
				fc.utobj().clickElement(driver, pobj.forumPostChk);
			}
			fc.utobj().clickElement(driver, pobj.saveBtnAtCbox);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Verify The Modified New Contact");
			fc.utobj().showAll(driver);
			boolean isFirstNamePresent1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text(),'" + corpUser.getuserFullName() + "')]");
			if (isFirstNamePresent1 == false) {
				fc.utobj().throwsException("Not able to Modify Added New Contact");
			}

			fc.utobj().printTestStep("Delete the Modified New Contact");
			fc.utobj().actionImgOption(driver, corpUser.getuserFullName(), "Delete");
			fc.utobj().acceptAlertBox(driver);

			fc.utobj().printTestStep("Verify The Deleted New Contact");
			fc.utobj().showAll(driver);
			boolean isFirstNamePresent2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text(),'" + corpUser.getuserFullName() + "')]");
			if (isFirstNamePresent2 == true) {
				fc.utobj().throwsException("Not able to Delete the Modified New Contact");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	public String addOffensiveWord(WebDriver driver, String offensiveKeyword) throws Exception {
		AdminConfigurationConfigureOffensiveWatchGreatKeywordsforForumandFranbuzzPage pobj = new AdminConfigurationConfigureOffensiveWatchGreatKeywordsforForumandFranbuzzPage(
				driver);
		fc.adminpage().adminConfigurationConfigureOffensiveWatchGreatKeywords(driver);
		fc.utobj().clickElement(driver, pobj.offensiveWord);
		fc.utobj().clickElement(driver, pobj.addOffensiveWord);
		fc.commonMethods().switch_cboxIframe_frameId(driver);
		fc.utobj().sendKeys(driver, pobj.keywordName, offensiveKeyword);
		fc.utobj().clickElement(driver, pobj.addBtn);
		fc.utobj().clickElement(driver, pobj.closeBtn);
		fc.utobj().switchFrameToDefault(driver);

		return offensiveKeyword;
	}

	public String watchWord(WebDriver driver, String watchKeyword) throws Exception {
		AdminConfigurationConfigureOffensiveWatchGreatKeywordsforForumandFranbuzzPage pobj = new AdminConfigurationConfigureOffensiveWatchGreatKeywordsforForumandFranbuzzPage(
				driver);
		fc.adminpage().adminConfigurationConfigureOffensiveWatchGreatKeywords(driver);
		fc.utobj().clickElement(driver, pobj.offensiveWord);
		fc.utobj().clickElement(driver, pobj.addWatchWord);
		fc.commonMethods().switch_cboxIframe_frameId(driver);
		fc.utobj().sendKeys(driver, pobj.keywordName, watchKeyword);
		fc.utobj().clickElement(driver, pobj.addBtn);
		fc.utobj().clickElement(driver, pobj.closeBtn);
		fc.utobj().switchFrameToDefault(driver);

		return watchKeyword;
	}

	public String greatWord(WebDriver driver, String greatKeyword) throws Exception {
		AdminConfigurationConfigureOffensiveWatchGreatKeywordsforForumandFranbuzzPage pobj = new AdminConfigurationConfigureOffensiveWatchGreatKeywordsforForumandFranbuzzPage(
				driver);
		fc.adminpage().adminConfigurationConfigureOffensiveWatchGreatKeywords(driver);
		fc.utobj().clickElement(driver, pobj.offensiveWord);
		fc.utobj().clickElement(driver, pobj.addGreatWord);
		fc.commonMethods().switch_cboxIframe_frameId(driver);
		fc.utobj().sendKeys(driver, pobj.keywordName, greatKeyword);
		fc.utobj().clickElement(driver, pobj.addBtn);
		fc.utobj().clickElement(driver, pobj.closeBtn);
		fc.utobj().switchFrameToDefault(driver);

		return greatKeyword;
	}

	public void enableAnddisableConfigurationOffensiveKeyword(WebDriver driver, boolean flag) throws Exception {
		AdminConfigurationConfigureOffensiveWatchGreatKeywordsforForumandFranbuzzPage pobj = new AdminConfigurationConfigureOffensiveWatchGreatKeywordsforForumandFranbuzzPage(
				driver);

		fc.adminpage().adminConfigurationConfigureOffensiveWatchGreatKeywords(driver);
		fc.utobj().clickElement(driver, pobj.offensiveWord);
		if (flag == true) {

			if (!fc.utobj().isSelected(driver, pobj.oEnable)) {
				fc.utobj().clickElement(driver, pobj.oEnable);
			}
		} else if (flag == false) {
			if (fc.utobj().isSelected(driver, pobj.oEnable)) {
				fc.utobj().clickElement(driver, pobj.oEnable);
			}
		}

		fc.utobj().clickElement(driver, pobj.saveBtn);

	}

	public void enableAnddisableConfigurationWatchKeyword(WebDriver driver, boolean flag) throws Exception {
		AdminConfigurationConfigureOffensiveWatchGreatKeywordsforForumandFranbuzzPage pobj = new AdminConfigurationConfigureOffensiveWatchGreatKeywordsforForumandFranbuzzPage(
				driver);

		fc.adminpage().adminConfigurationConfigureOffensiveWatchGreatKeywords(driver);
		fc.utobj().clickElement(driver, pobj.offensiveWord);
		if (flag == true) {

			if (!fc.utobj().isSelected(driver, pobj.wEnable)) {
				fc.utobj().clickElement(driver, pobj.wEnable);
			}
		} else if (flag == false) {
			if (fc.utobj().isSelected(driver, pobj.wEnable)) {
				fc.utobj().clickElement(driver, pobj.wEnable);
			}
		}
		fc.utobj().clickElement(driver, pobj.saveBtn);
	}

	public void enableAnddisableConfigurationGreatKeyword(WebDriver driver, boolean flag) throws Exception {
		AdminConfigurationConfigureOffensiveWatchGreatKeywordsforForumandFranbuzzPage pobj = new AdminConfigurationConfigureOffensiveWatchGreatKeywordsforForumandFranbuzzPage(
				driver);

		fc.adminpage().adminConfigurationConfigureOffensiveWatchGreatKeywords(driver);
		fc.utobj().clickElement(driver, pobj.offensiveWord);
		if (flag == true) {

			if (!fc.utobj().isSelected(driver, pobj.gEnable)) {
				fc.utobj().clickElement(driver, pobj.gEnable);
			}
		} else if (flag == false) {
			if (fc.utobj().isSelected(driver, pobj.gEnable)) {
				fc.utobj().clickElement(driver, pobj.gEnable);
			}
		}
		fc.utobj().clickElement(driver, pobj.saveBtn);
	}

	public void addEmailContactUser(WebDriver driver, String emailId) throws Exception {
		AdminConfigurationConfigureOffensiveWatchGreatKeywordsforForumandFranbuzzPage pobj = new AdminConfigurationConfigureOffensiveWatchGreatKeywordsforForumandFranbuzzPage(
				driver);
		fc.adminpage().adminConfigurationConfigureOffensiveWatchGreatKeywords(driver);
		fc.utobj().clickElement(driver, pobj.offensiveEmailContactTab);
		fc.utobj().clickElement(driver, pobj.addNewContact);
		fc.commonMethods().switch_cboxIframe_frameId(driver);

		if (!fc.utobj().isSelected(driver, pobj.newUserRBtn)) {
			fc.utobj().clickElement(driver, pobj.newUserRBtn);
		}
		String firstName = fc.utobj().generateTestData("Testfname");
		String lastName = fc.utobj().generateTestData("Testlname");
		fc.utobj().sendKeys(driver, pobj.firstName, firstName);
		fc.utobj().sendKeys(driver, pobj.lastName, lastName);
		fc.utobj().sendKeys(driver, pobj.emailID, emailId);

		if (!fc.utobj().isSelected(driver, pobj.franBuzzPostChk)) {
			fc.utobj().clickElement(driver, pobj.franBuzzPostChk);
		}

		if (!fc.utobj().isSelected(driver, pobj.forumPostChk)) {
			fc.utobj().clickElement(driver, pobj.forumPostChk);
		}
		fc.utobj().clickElement(driver, pobj.createBtn);
		fc.utobj().switchFrameToDefault(driver);
	}
}
