package com.builds.test.admin;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.builds.uimaps.admin.AdminConfigurationConfigurePasswordSettingsPage;
import com.builds.utilities.FranconnectUtil;

public class AdminConfigurationConfigurePasswordSettingsPageTest {

	FranconnectUtil fc = new FranconnectUtil();

	public void ConfigurePasswordSettings(WebDriver driver) throws Exception {

		String testCaseId = "TC_ConfigurePasswordSettings_01";
		if (fc.utobj().configurationTestCaseToExecute(testCaseId)) {
			try {
				fc.adminpage().adminConfigurationConfigurePasswordSetting(driver);
				AdminConfigurationConfigurePasswordSettingsPage pobj = new AdminConfigurationConfigurePasswordSettingsPage(
						driver);
				List<WebElement> isForcelyChanged = pobj.isForcelyChangePassword;
				if (fc.utobj().isSelected(driver, isForcelyChanged.get(0))) {
					fc.utobj().clickElement(driver, isForcelyChanged.get(1));
				} else {
					// nothing
				}
				List<WebElement> frequentlyChangePassword = pobj.frequentlyChangePassword;
				if (fc.utobj().isSelected(driver, frequentlyChangePassword.get(0))) {
					fc.utobj().clickElement(driver, frequentlyChangePassword.get(1));
				} else {
					// do nothing
				}
				fc.utobj().sendKeys(driver, pobj.minNoOfChar, "8");
				fc.utobj().sendKeys(driver, pobj.maxNoOfChar, "30");
				List<WebElement> canUseSpecialAndCapital = pobj.useSpecialAndCapital;
				if (fc.utobj().isSelected(driver, canUseSpecialAndCapital.get(1))) {
					fc.utobj().clickElement(driver, canUseSpecialAndCapital.get(0));
				} else {
					// do nothing
				}
				fc.utobj().sendKeys(driver, pobj.minNoOfSpecialChar, "0");
				fc.utobj().sendKeys(driver, pobj.minNoOfCapitalChar, "0");
				fc.utobj().sendKeys(driver, pobj.minNoOfDigit, "0");
				fc.utobj().sendKeys(driver, pobj.noOfPasswordUsed, "0");
				if (fc.utobj().isSelected(driver, pobj.changeLockUser.get(0))) {
					fc.utobj().clickElement(driver, pobj.changeLockUser.get(1));
				}

				fc.utobj().clickElement(driver, pobj.save);

				fc.commonMethods().switch_cboxIframe_frameId(driver);
				fc.utobj().clickElement(driver, pobj.frameNo);
				fc.utobj().switchFrameToDefault(driver);

				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {

		}
	}
}
