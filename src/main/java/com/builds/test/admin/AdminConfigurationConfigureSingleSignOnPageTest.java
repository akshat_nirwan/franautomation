package com.builds.test.admin;

import java.util.Map;

import org.openqa.selenium.WebDriver;

import com.builds.uimaps.admin.AdminConfigurationConfigureSingleSignOnPage;
import com.builds.utilities.FranconnectUtil;

public class AdminConfigurationConfigureSingleSignOnPageTest {

	FranconnectUtil fc = new FranconnectUtil();

	public void LinkSSO(WebDriver driver, String linkTitle, String description, Map<String, String> config,
			String fileName, String targetUrl, String urlTocken1, String urlTocken2, String userName) throws Exception {

		AdminConfigurationConfigureSingleSignOnPage pobj = new AdminConfigurationConfigureSingleSignOnPage(driver);
		fc.hub().hub_common().adminTheHubConfigureSingleSignOnPage(driver);
		fc.utobj().clickElement(driver, pobj.lnksTab);
		fc.utobj().clickElement(driver, pobj.addNewLink);

		if (!fc.utobj().isSelected(driver,fc.utobj().getElement(driver, pobj.samlAuthenticationNo))) {
			fc.utobj().clickElement(driver, pobj.samlAuthenticationNo);
		}

		if (!fc.utobj().isSelected(driver, pobj.thehubHome)) {
			fc.utobj().clickElement(driver, pobj.thehubHome);
		}

		fc.utobj().sendKeys(driver, pobj.title, linkTitle);
		fc.utobj().sendKeys(driver, pobj.title, description);

		String fileNamePath = fc.utobj().getFilePathFromTestData(fileName);
		fc.utobj().sendKeys(driver, pobj.imageName, fileNamePath);

		fc.utobj().sendKeys(driver, pobj.targetURL, targetUrl);

		if (!fc.utobj().isSelected(driver, pobj.loginAuthinticationYes)) {
			fc.utobj().clickElement(driver, pobj.loginAuthinticationYes);
		}

		if (!fc.utobj().isSelected(driver, pobj.methodGet)) {
			fc.utobj().clickElement(driver, pobj.methodGet);
		}

		fc.utobj().sendKeys(driver, pobj.urlTocken1, urlTocken1);
		fc.utobj().sendKeys(driver, pobj.urlTocken2, urlTocken2);

		fc.utobj().clickElement(driver, pobj.continueBtn);
		fc.utobj().clickElement(driver, pobj.skipTestBtn);

		if (!fc.utobj().isSelected(driver, pobj.nowRdBtn)) {
			fc.utobj().clickElement(driver, pobj.nowRdBtn);
		}

		fc.utobj().clickElement(driver, pobj.submitBtn);

		fc.utobj().sendKeys(driver, pobj.searchCorpUsers, userName);
		fc.utobj().clickElement(driver, pobj.searchCorporateImgBtn);
		fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
				".//td[contains(text(),'" + userName + "')]/ancestor::tr/td/input[@name='contactidcorp']"));
		fc.utobj().clickElement(driver, pobj.addBtn);
		fc.utobj().clickElement(driver, pobj.CchkMain);
		fc.utobj().clickElement(driver, pobj.saveBtn);
	}
}
