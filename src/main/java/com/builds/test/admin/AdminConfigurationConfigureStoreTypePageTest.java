package com.builds.test.admin;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.Test;

import com.builds.uimaps.admin.AdminConfigurationConfigureStoreTypePage;
import com.builds.utilities.FranconnectUtil;
import com.builds.utilities.TestCase;

public class AdminConfigurationConfigureStoreTypePageTest {

	FranconnectUtil fc = new FranconnectUtil();

	@Test(groups = { "admin10101"})
	@TestCase(createdOn = "2017-11-02", updatedOn = "2017-11-02", testCaseId = "TC_Admin_StoreType_001", testCaseDescription = "Verify Store Type link in Admin's Configuration: Add, Modify and Delete links are working correctly")
	private void storeType() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());
		
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			AdminConfigurationConfigureStoreTypePage pobj = new AdminConfigurationConfigureStoreTypePage(driver);

			fc.utobj().printTestStep("Admin > Configuration > Configure Store Type");
			fc.adminpage().adminConfigurationConfigureStoreType(driver);

			fc.utobj().printTestStep("Add Store Type With Blank Text");
			fc.utobj().clickElement(driver, pobj.addStoreType);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().sendKeys(driver, pobj.storeType, "");
			fc.utobj().clickElement(driver, pobj.addBtn);

			String isAlertTextPresent1 = fc.utobj().acceptAlertBox(driver);
			fc.utobj().printTestStep("Verify That Store Type Should Not Be Added With Blank Text");
			if (!isAlertTextPresent1.contains("Please enter Store Type")) {
				fc.utobj().throwsException("Not able to verify that Blank Store type should not be added");
			}
			fc.utobj().clickElement(driver, pobj.cancelBtn);
			fc.utobj().switchFrameToDefault(driver);

			String storeType = fc.utobj().generateTestData("TestS");
			fc.utobj().printTestStep("Add Store Type");
			fc.utobj().clickElement(driver, pobj.addStoreType);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().sendKeys(driver, pobj.storeType, storeType);
			fc.utobj().clickElement(driver, pobj.addBtn);
			fc.utobj().clickElement(driver, pobj.closeBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Verify That Added Store Type");
			boolean isTextPresent = fc.utobj().searchInSelectBoxSingleValue(driver, pobj.listing, storeType);
			if (isTextPresent == false) {
				fc.utobj().throwsException("Not able to add Store Type");
			}

			fc.utobj().printTestStep("Modified Store Type");
			fc.utobj().selectDropDownByVisibleTextTrimed(driver, pobj.listing, storeType);
			fc.utobj().clickElement(driver, pobj.modifyBtn);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			storeType = fc.utobj().generateTestData("TestM");
			fc.utobj().sendKeys(driver, pobj.storeType, storeType);
			fc.utobj().clickElement(driver, pobj.modifyBtn);
			fc.utobj().clickElement(driver, pobj.closeBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Verify The Modified Store Type");
			boolean isTextPresent1 = fc.utobj().searchInSelectBoxSingleValue(driver, pobj.listing, storeType);
			if (isTextPresent1 == false) {
				fc.utobj().throwsException("Not able to Modify Added Store Type");
			}

			String storeType1 = fc.utobj().generateTestData("TestC");
			fc.utobj().printTestStep("Add Another Store Type");
			fc.utobj().clickElement(driver, pobj.addStoreType);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().sendKeys(driver, pobj.storeType, storeType1);
			fc.utobj().clickElement(driver, pobj.addBtn);
			fc.utobj().clickElement(driver, pobj.closeBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Change Type Sequence");
			fc.utobj().selectDropDownByVisibleTextTrimed(driver, pobj.listing, storeType1);
			fc.utobj().clickElement(driver, pobj.moveUp);
			fc.utobj().clickElement(driver, pobj.changeTypeSequence);

			fc.utobj().printTestStep("Verify The Change Type Sequence");

			Select sl = new Select(pobj.listing);
			List<WebElement> listWeb = sl.getOptions();
			String tempText = null;
			int a = 0;
			int b = 0;

			for (int i = 0; i < listWeb.size(); i++) {
				tempText = listWeb.get(i).getText();

				if (tempText != null && tempText.length() > 0) {

					if (tempText.equalsIgnoreCase(storeType1)) {
						a = i;

					}
					if (tempText.equalsIgnoreCase(storeType)) {
						b = i;
					}
				}
			}

			if (a > b) {
				fc.utobj().throwsException("was not able to Change Type sequence");
			}

			fc.utobj().printTestStep("Verify That Duplicate Store Type Should not be added");
			fc.utobj().clickElement(driver, pobj.addStoreType);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().sendKeys(driver, pobj.storeType, storeType1);
			fc.utobj().clickElement(driver, pobj.addBtn);

			String isAlertTextPresent = fc.utobj().acceptAlertBox(driver);

			if (!isAlertTextPresent.contains("Store Type already exists")) {
				fc.utobj().throwsException("Not able to verify that duplicate Store type should not be added");
			}

			fc.utobj().clickElement(driver, pobj.cancelBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Add Store Type With Special Character");
			fc.utobj().clickElement(driver, pobj.addStoreType);
			String taskTypeSp = fc.utobj().generateRandomSpecialChar();
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().sendKeys(driver, pobj.storeType, taskTypeSp);
			fc.utobj().clickElement(driver, pobj.addBtn);
			fc.utobj().clickElement(driver, pobj.closeBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Verify That Store Type With Special Character Should Be Added");
			boolean isTextPresent12 = fc.utobj().searchInSelectBoxSingleValue(driver, pobj.listing, taskTypeSp);
			if (isTextPresent12 == false) {
				fc.utobj().throwsException("Not able to Verify That Store Type With Special Character Should Be Added");
			}

			fc.utobj().printTestStep("Delete The Added Store Type");
			fc.utobj().selectDropDownByVisibleTextTrimed(driver, pobj.listing, storeType);
			fc.utobj().clickElement(driver, pobj.deleteBtn);
			fc.utobj().acceptAlertBox(driver);

			boolean isTextPresent11 = fc.utobj().searchInSelectBoxSingleValue(driver, pobj.listing, storeType);
			if (isTextPresent11 == true) {
				fc.utobj().throwsException("Not able delete the added Store Type");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	public String addStoreType(WebDriver driver, String storeType) throws Exception {

		String testCaseId = "TC_addStoreTypeAdminConfiguration_01";
		if (fc.utobj().validate(testCaseId)) {
			try {
				fc.adminpage().adminConfigurationConfigureStoreType(driver);
				AdminConfigurationConfigureStoreTypePage pobj = new AdminConfigurationConfigureStoreTypePage(driver);
				fc.utobj().clickElement(driver, pobj.addStoreType);
				fc.commonMethods().switch_cboxIframe_frameId(driver);

				fc.utobj().sendKeys(driver, pobj.storeType, storeType);
				fc.utobj().clickElement(driver, pobj.addBtn);
				fc.utobj().clickElement(driver, pobj.closeBtn);
				fc.utobj().switchFrameToDefault(driver);

				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException("was not able to Add Store Type");

		}
		return storeType;
	}
}
