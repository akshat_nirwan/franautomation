package com.builds.test.admin;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.builds.uimaps.admin.AdminConfigurationConfigureTaskEmailsPage;
import com.builds.utilities.FranconnectUtil;

public class AdminConfigurationConfigureTaskEmailsPageTest {
	FranconnectUtil fc = new FranconnectUtil();

	public String configureTaskCompletionEmail(WebDriver driver) throws Exception {

		String subject = "Task Completion Notification";

		AdminConfigurationConfigureTaskEmailsPage pobj = new AdminConfigurationConfigureTaskEmailsPage(driver);
		fc.adminpage().openConfigureTaskEmailsPage(driver);
		fc.utobj().clickElement(driver, pobj.configureTaskCompletionTab);

		WebElement element = fc.utobj().getElement(driver, pobj.sendEmailNotificationYes);

		if (!fc.utobj().isSelected(driver,element)) {
			fc.utobj().clickElement(driver, pobj.sendEmailNotificationYes);
		}
		fc.utobj().sendKeys(driver, pobj.subject, subject);
		fc.utobj().clickElement(driver, pobj.configureBtn);

		return subject;
	}
}
