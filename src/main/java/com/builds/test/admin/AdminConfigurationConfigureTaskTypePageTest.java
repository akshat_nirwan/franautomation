package com.builds.test.admin;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.Test;

import com.builds.uimaps.admin.AdminConfigurationConfigureTaskTypePage;
import com.builds.utilities.FranconnectUtil;
import com.builds.utilities.TestCase;

public class AdminConfigurationConfigureTaskTypePageTest {
	FranconnectUtil fc = new FranconnectUtil();

	@Test(groups = { "admin" })
	@TestCase(createdOn = "2017-11-01", updatedOn = "2017-11-01", testCaseId = "TC_Admin_TaskType_001", testCaseDescription = "Verify Add ,Modify and Delete Task Type")
	private void taskType() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		String taskTypeName = fc.utobj().generateTestData("Testtasktype");

		try {
			driver = fc.loginpage().login(driver);
			AdminConfigurationConfigureTaskTypePage pobj = new AdminConfigurationConfigureTaskTypePage(driver);

			fc.utobj().printTestStep("Admin > Configuration > Configure Task Type");
			fc.adminpage().adminConfigurationConfigureTaskType(driver);

			fc.utobj().printTestStep("Verify That Blank Task Type Should not be added");
			fc.utobj().clickElement(driver, pobj.addTaskType);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().sendKeys(driver, pobj.taskType, "");
			fc.utobj().clickElement(driver, pobj.addBtn);

			String isAlertTextPresent1 = fc.utobj().acceptAlertBox(driver);

			if (!isAlertTextPresent1.contains("Please enter Task Type")) {
				fc.utobj().throwsException("Not able to verify that Blank task type should not be added");
			}
			fc.utobj().clickElement(driver, pobj.cancelBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Add Task Type");
			fc.utobj().clickElement(driver, pobj.addTaskType);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().sendKeys(driver, pobj.taskType, taskTypeName);
			fc.utobj().clickElement(driver, pobj.addBtn);
			fc.utobj().clickElement(driver, pobj.closeBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Verify The Added Task Type");
			boolean isTextPresent = fc.utobj().searchInSelectBoxSingleValue(driver, pobj.listing, taskTypeName);
			if (isTextPresent == false) {
				fc.utobj().throwsException("Not able to add Task Type");
			}

			fc.utobj().printTestStep("Modified Added Task Type");
			fc.utobj().selectDropDownByVisibleTextTrimed(driver, pobj.listing, taskTypeName);
			fc.utobj().clickElement(driver, pobj.modifyBtn);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			taskTypeName = fc.utobj().generateTestData("Testtasktypem");
			fc.utobj().sendKeys(driver, pobj.taskType, taskTypeName);
			fc.utobj().clickElement(driver, pobj.modifyBtn);
			fc.utobj().clickElement(driver, pobj.closeBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Verify The Modified Task Type");
			boolean isTextPresent1 = fc.utobj().searchInSelectBoxSingleValue(driver, pobj.listing, taskTypeName);
			if (isTextPresent1 == false) {
				fc.utobj().throwsException("Not able to Modify Added Task Type");
			}

			String taskTypeName1 = fc.utobj().generateTestData("Testtypean");
			fc.utobj().printTestStep("Add Another Task Type");
			fc.utobj().clickElement(driver, pobj.addTaskType);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().sendKeys(driver, pobj.taskType, taskTypeName1);
			fc.utobj().clickElement(driver, pobj.addBtn);
			fc.utobj().clickElement(driver, pobj.closeBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Change Type Sequence");
			fc.utobj().selectDropDownByVisibleTextTrimed(driver, pobj.listing, taskTypeName1);
			fc.utobj().clickElement(driver, pobj.moveUp);
			fc.utobj().clickElement(driver, pobj.changeTypeSequence);

			fc.utobj().printTestStep("Verify The Change Type Sequence");

			Select sl = new Select(pobj.listing);
			List<WebElement> listWeb = sl.getOptions();
			String tempText = null;
			int a = 0;
			int b = 0;

			for (int i = 0; i < listWeb.size(); i++) {
				tempText = listWeb.get(i).getText();

				if (tempText != null && tempText.length() > 0) {

					if (tempText.equalsIgnoreCase(taskTypeName1)) {
						a = i;

					}
					if (tempText.equalsIgnoreCase(taskTypeName)) {
						b = i;
					}
				}
			}

			if (a > b) {
				fc.utobj().throwsException("was not able to Change Type sequence");
			}

			fc.utobj().printTestStep("Verify That Duplicate Task Type Should not be added");
			fc.utobj().clickElement(driver, pobj.addTaskType);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().sendKeys(driver, pobj.taskType, taskTypeName1);
			fc.utobj().clickElement(driver, pobj.addBtn);

			String isAlertTextPresent = fc.utobj().acceptAlertBox(driver);

			if (!isAlertTextPresent.contains("Task Type already exists")) {
				fc.utobj().throwsException("Not able to verify that duplicate task type should not be added");
			}
			fc.utobj().clickElement(driver, pobj.cancelBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Add Task Type With Special Character");
			fc.utobj().clickElement(driver, pobj.addTaskType);
			String taskTypeSp = fc.utobj().generateRandomSpecialChar();
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().sendKeys(driver, pobj.taskType, taskTypeSp);
			fc.utobj().clickElement(driver, pobj.addBtn);
			fc.utobj().clickElement(driver, pobj.closeBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Verify That Task Type With Special Character Should Be Added");
			boolean isTextPresent12 = fc.utobj().searchInSelectBoxSingleValue(driver, pobj.listing, taskTypeSp);
			if (isTextPresent12 == false) {
				fc.utobj().throwsException("Not able to Verify That Task Type With Special Character Should Be Added");
			}

			fc.utobj().printTestStep("Delete The Added Task Type");
			fc.utobj().selectDropDownByVisibleTextTrimed(driver, pobj.listing, taskTypeName);
			fc.utobj().clickElement(driver, pobj.deleteBtn);
			fc.utobj().acceptAlertBox(driver);

			boolean isTextPresent11 = fc.utobj().searchInSelectBoxSingleValue(driver, pobj.listing, taskTypeName);
			if (isTextPresent11 == true) {
				fc.utobj().throwsException("Not able delete the added Task Type");
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}
}
