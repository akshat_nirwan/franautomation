package com.builds.test.admin;

import java.util.Map;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Test;

import com.builds.uimaps.admin.AdminConfigurationPIIConfigurationPage;
import com.builds.utilities.FranconnectUtil;

public class AdminConfigurationPIIConfigurationPageTest {
	FranconnectUtil fc = new FranconnectUtil();

	@Test
	public void offPII(WebDriver driver, Map<String, String> config) throws Exception {

		String testCaseId = "TC_offPII_01";
		if (fc.utobj().validate(testCaseId)) {
			try {
				driver = fc.loginpage().login();
				fc.adminpage().adminConfigurationPIIConfiguration(driver);
				AdminConfigurationPIIConfigurationPage pobj = new AdminConfigurationPIIConfigurationPage(driver);
				fc.utobj().clickElement(driver, pobj.passwordFunctionalityOff);
				fc.utobj().sendKeys(driver, pobj.confirmPasswordText, "piisupervisior");
				fc.utobj().clickElement(driver, pobj.saveBtn);

				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException("was not able to offPII ");

		}
	}
}
