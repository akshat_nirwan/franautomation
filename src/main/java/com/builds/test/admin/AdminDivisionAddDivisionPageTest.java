package com.builds.test.admin;

import java.util.Map;

import org.openqa.selenium.WebDriver;
import org.testng.Reporter;
import org.testng.annotations.Test;

import com.builds.uimaps.admin.AdminDivisionAddDivisionPage;
import com.builds.uimaps.admin.AdminDivisionManageDivisionPage;
import com.builds.utilities.FranconnectUtil;
import com.builds.utilities.TestCase;

public class AdminDivisionAddDivisionPageTest {
	FranconnectUtil fc = new FranconnectUtil();

	@Test(groups = { "admin"})
	@TestCase(createdOn = "2017-11-01", updatedOn = "2017-11-01", testCaseId = "TC_Admin_ManageDivision_001", testCaseDescription = "Verify Add ,Modify and Delete Division")
	private void manageDivision() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		// Map<String,String> dataSet = fc.utobj().readTestData("admin",
		// testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		String divisionName = fc.utobj().generateTestData("TestDiv");

		try {
			driver = fc.loginpage().login(driver);

			fc.utobj().printTestStep("Navigate To  Admin > Hidden Links > Configure New Hierarchy Level");
			AdminHiddenLinksConfigureNewHierarchyLevelPageTest hierarchyPage = new AdminHiddenLinksConfigureNewHierarchyLevelPageTest();
			hierarchyPage.ConfigureNewHierarchyLevel(driver);

			fc.utobj().printTestStep("Navigate To Admin > Division > Manage Division");
			AdminDivisionAddDivisionPage pobj = new AdminDivisionAddDivisionPage(driver);
			fc.utobj().printTestStep("Add Division");
			fc.adminpage().adminDivisionAddDivision(driver);

			fc.utobj().sendKeys(driver, pobj.DivisionName, divisionName);
			fc.utobj().clickElement(driver, pobj.submit);

			fc.utobj().printTestStep("Select Division Name By Multi Drop Down");
			fc.utobj().selectValFromMultiSelect(driver, pobj.selectDivisionName, divisionName);
			fc.utobj().selectDropDownByValue(driver, pobj.creationDateSelect, "55");
			fc.utobj().clickElement(driver, pobj.searchButton);

			fc.utobj().printTestStep("Verify The Added Division");
			boolean isDivTextPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(@class,'botBorder') and contains(text(),'" + divisionName + "')]");
			if (isDivTextPresent == false) {
				fc.utobj().throwsException("was not able to verify the added Division");
			}

			fc.utobj().printTestStep("Modify The Added Division");
			fc.utobj().actionImgOption(driver, divisionName, "Modify");
			divisionName = fc.utobj().generateTestData("TestDM");
			fc.utobj().sendKeys(driver, pobj.DivisionName, divisionName);
			fc.utobj().clickElement(driver, pobj.submit);

			fc.utobj().printTestStep("Select Division Name By Multi Drop Down");
			fc.utobj().selectValFromMultiSelect(driver, pobj.selectDivisionName, divisionName);
			fc.utobj().selectDropDownByValue(driver, pobj.creationDateSelect, "55");
			fc.utobj().clickElement(driver, pobj.searchButton);

			fc.utobj().printTestStep("Verify The Modified Division");
			boolean isDivTextPresent1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(@class,'botBorder') and contains(text(),'" + divisionName + "')]");
			if (isDivTextPresent1 == false) {
				fc.utobj().throwsException("was not able to verify the Modified Division");
			}

			fc.utobj().printTestStep("Delete The Modified Division");
			fc.utobj().actionImgOption(driver, divisionName, "Delete");
			fc.utobj().acceptAlertBox(driver);
			fc.utobj().refresh(driver);

			fc.utobj().printTestStep("Verify The Deleted Division");
			boolean isDivPresent = fc.utobj().assertMultiSelect(driver, pobj.selectDivisionName, divisionName);
			if (isDivPresent) {
				fc.utobj().throwsException("Not able to delete Division");
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	public void addDivision(WebDriver driver, String divisionName) throws Exception {

		String testCaseId = "TC_Add_Division_Admin_02";
		if (fc.utobj().validate(testCaseId)) {
			try {

				// Division Configure
				AdminHiddenLinksConfigureNewHierarchyLevelPageTest hierarchyPage = new AdminHiddenLinksConfigureNewHierarchyLevelPageTest();
				hierarchyPage.ConfigureNewHierarchyLevel(driver);
				AdminDivisionAddDivisionPage pobj = new AdminDivisionAddDivisionPage(driver);
				fc.adminpage().adminDivisionAddDivision(driver);

				fc.utobj().sendKeys(driver, pobj.DivisionName, divisionName);
				
				try {
					if (fc.config.get("moduleList").toLowerCase().contains("FcSanity".toLowerCase())) {
						
						if (!fc.utobj().isSelected(driver, pobj.zipMode)) {
							fc.utobj().clickElement(driver, pobj.zipMode);
						}
						fc.utobj().sendKeys(driver, pobj.zipList, fc.utobj().generateRandomNumber6Digit());
					}
				} catch (Exception e) {
				}
				fc.utobj().clickElement(driver, pobj.submit);

				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException("was not able to add Division Admin");

		}
	}

	public void addDivisionByGeographyZipCode(WebDriver driver, String divisionName, String zipCode,
			Map<String, String> config) throws Exception {

		String testCaseId = "TC_addDivisionByGeographyZipCode_01";
		if (fc.utobj().validate(testCaseId)) {
			try {
				
				fc.utobj().printTestStep("Navigate To  Admin > Hidden Links > Configure New Hierarchy Level");
				AdminHiddenLinksConfigureNewHierarchyLevelPageTest hierarchyPage = new AdminHiddenLinksConfigureNewHierarchyLevelPageTest();
				hierarchyPage.ConfigureNewHierarchyLevel(driver);
				AdminDivisionAddDivisionPage pobj = new AdminDivisionAddDivisionPage(driver);
				fc.adminpage().adminDivisionAddDivision(driver);
				fc.utobj().sendKeys(driver, pobj.DivisionName, divisionName);
//				fc.utobj().clickElement(driver, pobj.ZipCode);
//				fc.utobj().sendKeys(driver, pobj.zipTextArea, zipCode);
				fc.utobj().clickElement(driver, pobj.submit);

				try {
					driver.switchTo().alert().dismiss();
				} catch (Exception e) {

				}

				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException("was not able to add Division By Geography Zip Code");

		}
	}

	public String addDivisionName(WebDriver driver, String divisionName) throws Exception {

		String testCaseId = "TC_addDivisionName_01";
		if (fc.utobj().validate(testCaseId)) {
			try {
				// Division Configure
				AdminHiddenLinksConfigureNewHierarchyLevelPageTest hierarchyPage = new AdminHiddenLinksConfigureNewHierarchyLevelPageTest();
				hierarchyPage.ConfigureNewHierarchyLevel(driver);

				AdminDivisionAddDivisionPage pobj = new AdminDivisionAddDivisionPage(driver);

				fc.adminpage().adminDivisionAddDivision(driver);

				divisionName = fc.utobj().generateTestData(divisionName);
				fc.utobj().sendKeys(driver, pobj.DivisionName, divisionName);
				fc.utobj().clickElement(driver, pobj.submit);

				AdminDivisionManageDivisionPage objManagerDivision = new AdminDivisionManageDivisionPage(driver);

				try {

					fc.utobj().clickElement(driver, objManagerDivision.lnkShowAll);
				} catch (Exception e) {

				}

				boolean boolDivName = fc.utobj().assertPageSource(driver, divisionName);
				if (boolDivName == true) {
					Reporter.log("Division / Brand creation successfull !!!");
				} else {
					fc.utobj().printBugStatus("Division / Brand creation failed !!!");
				}
				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException("was not able to add Division Name");

		}
		return divisionName;
	}
}
