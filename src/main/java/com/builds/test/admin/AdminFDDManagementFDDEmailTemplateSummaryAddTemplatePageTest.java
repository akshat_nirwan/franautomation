package com.builds.test.admin;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Optional;

import com.builds.uimaps.admin.AdminFDDManagementFDDEmailTemplateSummaryAddTemplatePage;
import com.builds.uimaps.admin.AdminFDDManagementFDDEmailTemplateSummaryPage;
import com.builds.utilities.FranconnectUtil;

public class AdminFDDManagementFDDEmailTemplateSummaryAddTemplatePageTest {
	FranconnectUtil fc = new FranconnectUtil();

	public String configureFDDEmailTemplateSummary(WebDriver driver, @Optional() String mailTitle,
			@Optional() String mailSubject) throws Exception {

		String testCaseId = "TC_configureFDDEmailTemplateSummary_01";
		if (fc.utobj().validate(testCaseId)) {
			try {
				fc.adminpage().adminPage(driver);

				fc.utobj().clickLink(driver, "FDD Email Template summary");

				AdminFDDManagementFDDEmailTemplateSummaryPage pobj = new AdminFDDManagementFDDEmailTemplateSummaryPage(
						driver);

				fc.utobj().clickElement(driver, pobj.addTemplateBtn);
				AdminFDDManagementFDDEmailTemplateSummaryAddTemplatePage pobj2 = new AdminFDDManagementFDDEmailTemplateSummaryAddTemplatePage(
						driver);

				mailTitle = fc.utobj().generateTestData(mailTitle);

				fc.utobj().sendKeys(driver, pobj2.mailTitle, mailTitle);
				fc.utobj().sendKeys(driver, pobj2.mailSubject, mailSubject + fc.utobj().generateRandomNumber());

				String windowHandle = driver.getWindowHandle();
				driver.switchTo().frame(fc.utobj().getElement(driver, pobj2.htmlFrame));

				fc.utobj().sendKeys(driver, pobj2.mailText, "Dear $FIRST_NAME$ ,"

						+ "We are pleased to send you a copy of our Franchise Disclosure Document (FDD) , Set forth below is a Username, Password and Website information for accessing and downloading our Franchise Disclosure Document."

						+ "The FDD is an important document and provides you with some basic information about our company, the franchise that we offer, and our franchise system Please carefully read the entire FDD (including all of the exhibits)."

						+ "The FDD is in PDF format. You will need a copy of Adobe Reader. You probably already have this on your computer, but in case you don't, You can download a free version of Adobe Acrobat Reader by going to www.adobe.com.)"

						+ "Username: $USERID$"

						+ "Password: $PASSWORD$"

						+ "Website: $URL_TO_DOWNLOAD_FDD$"

						+ "When you open the FDD, please click on the link on the first page. That page is the receipt, which just confirms that you have received the document."

						+ "Alternatively, you can receive the FDD (a) as an Adobe Portable Document Format (PDF) File on a CD ROM disc, (b) as a PDF attachment to an e-mail message, or (c) on paper as a two-sided hardcopy. If you prefer to have the FDD delivered in one of these other ways, please reply to this e-mail indicating which method you prefer."

						+ "If you choose to download the PDF or receive it via email, you will need internet access and a valid e-mail account, in addition to Adobe Acrobat Reader. You will also need access to a printer to print the Receipt exhibit, and also to print a paper copy for yourself."

						+ "The FDD is confidential, subject to the mutual confidentiality agreement you have signed, and contains proprietary information, including trade secrets. Neither the FDD nor any of the information contained in the FDD may be reproduced or disclosed to any person under any circumstances without our written permission."

						+ "$SENDER_NAME$");

				driver.switchTo().window(windowHandle);

				fc.utobj().clickElement(driver, pobj2.submitBtn);

				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException("was not able to configure FDD Email Template Summary");

		}
		return mailTitle;
	}
}
