package com.builds.test.admin;

import java.util.Map;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Optional;

import com.builds.uimaps.admin.AdminFDDManagementITEM23RECEIPTSummaryAddITEM23RECEIPTPage;
import com.builds.uimaps.admin.AdminFDDManagementITEM23RECEIPTSummaryPage;
import com.builds.utilities.FranconnectUtil;

public class AdminFDDManagementITEM23RECEIPTSummaryPageTest {
	FranconnectUtil fc = new FranconnectUtil();

	/*
	 * @Parameters({ "item23Title" })
	 * 
	 * @Test(groups = { "commonModule", "add", "Call Status"}) public String
	 * addITEM23RECEIPTS(@Optional()String item23Title) throws Exception {
	 * String testCaseId = "";
	 * 
	 * WebDriver driver = fc.loginpage().login();
	 * 
	 * Map<String, String> dataSet = fc.utobj().readTestData(testCaseId);
	 * 
	 * fc.adminpage().item23ReceiptSummaryLnk( driver);
	 * 
	 * AdminFDDManagementITEM23RECEIPTSummaryPage pobj = new
	 * AdminFDDManagementITEM23RECEIPTSummaryPage(driver);
	 * fc.utobj().clickElement(driver,pobj.addItem23ReceiptBtn);
	 * AdminFDDManagementITEM23RECEIPTSummaryAddITEM23RECEIPTPage pobj2 = new
	 * AdminFDDManagementITEM23RECEIPTSummaryAddITEM23RECEIPTPage(driver);
	 * item23Title = fc.utobj().generateTestData(item23Title);
	 * 
	 * fc.utobj().sendKeys(driver,pobj2.templateTitle, item23Title);
	 * 
	 * 
	 * String content =
	 * "$BROKER_NAME$ \n$BROKER_AGENCY$ \n$FRANCHISE_SELLER_NAME$ \n$BUSINESS_NAME$ \n$ADDRESS$ \n$STATE$ \n $COUNTRY$ \n$PHONE$ \n$DATE$"
	 * ;
	 * 
	 * fc.utobj().sendKeys(driver,pobj2.item23ReceiptTextArea,content);
	 * fc.utobj().sendKeys(driver,pobj2.businessName, "FranConnect Inc");
	 * fc.utobj().sendKeys(driver,pobj2.addressTxt, "B-5, Park Avenue");
	 * fc.utobj().selectDropDown(driver,pobj2.countryDrp, "USA");
	 * fc.utobj().selectDropDown(driver,pobj2.stateDrp, "Alaska");
	 * fc.utobj().sendKeys(driver,pobj2.phone, "9998887777");
	 * fc.utobj().clickElement(driver,pobj2.previewBtn);
	 * fc.utobj().clickElement(driver,pobj2.saveBtn); return item23Title;
	 * 
	 * }
	 */

	public String addITEM23RECEIPTS(WebDriver driver, @Optional() String item23Title, Map<String, String> config)
			throws Exception {

		String testCaseId = "TC_addITEM23RECEIPTS_01";
		if (fc.utobj().validate(testCaseId)) {
			try {
				fc.adminpage().item23ReceiptSummaryLnk(driver);

				AdminFDDManagementITEM23RECEIPTSummaryPage pobj = new AdminFDDManagementITEM23RECEIPTSummaryPage(
						driver);

				fc.utobj().clickElement(driver, pobj.addItem23ReceiptBtn);
				AdminFDDManagementITEM23RECEIPTSummaryAddITEM23RECEIPTPage pobj2 = new AdminFDDManagementITEM23RECEIPTSummaryAddITEM23RECEIPTPage(
						driver);
				item23Title = fc.utobj().generateTestData(item23Title);

				fc.utobj().sendKeys(driver, pobj2.templateTitle, item23Title);

				String content = "$BROKER_NAME$ \n$BROKER_AGENCY$ \n$FRANCHISE_SELLER_NAME$ \n$BUSINESS_NAME$ \n$ADDRESS$ \n$STATE$ \n $COUNTRY$ \n$PHONE$ \n$DATE$";

				fc.utobj().sendKeys(driver, pobj2.item23ReceiptTextArea, content);
				fc.utobj().sendKeys(driver, pobj2.businessName, "FranConnect Inc");
				fc.utobj().sendKeys(driver, pobj2.addressTxt, "B-5, Park Avenue");
				fc.utobj().selectDropDown(driver, pobj2.countryDrp, "USA");

				fc.utobj().selectDropDown(driver, pobj2.stateDrp, "Alaska");
				fc.utobj().sendKeys(driver, pobj2.phone, "9998887777");
				fc.utobj().clickElement(driver, pobj2.previewBtn);
				fc.utobj().clickElement(driver, pobj2.saveBtn);

				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException("was not able to add ITEM RECEIPTS");

		}
		return item23Title;
	}

	/*
	 * @Parameters({ "fddName" })
	 * 
	 * @Test(groups = { "commonModule", "add", "Call Status"}) public String
	 * modifyITEM23RECEIPTS(@Optional()String fddName) throws Exception { String
	 * testCaseId = "";
	 * 
	 * WebDriver driver = fc.loginpage().login();
	 * 
	 * Map<String, String> dataSet = fc.utobj().readTestData(testCaseId);
	 * 
	 * fc.adminpage().item23ReceiptSummaryLnk( driver); return fddName;
	 * 
	 * }
	 */

	/*
	 * @Parameters({ "fddName" })
	 * 
	 * @Test(groups = { "commonModule", "add", "Call Status"}) public String
	 * deleteITEM23RECEIPTS(@Optional()String fddName) throws Exception { String
	 * testCaseId = "";
	 * 
	 * WebDriver driver = fc.loginpage().login();
	 * 
	 * Map<String, String> dataSet = fc.utobj().readTestData(testCaseId);
	 * 
	 * fc.adminpage().item23ReceiptSummaryLnk( driver); return fddName;
	 * 
	 * }
	 */

	/*
	 * @Parameters({ "fddName" })
	 * 
	 * @Test(groups = { "commonModule", "add", "Call Status"}) public String
	 * pagingITEM23RECEIPTS(@Optional()String fddName) throws Exception { String
	 * testCaseId = "";
	 * 
	 * WebDriver driver = fc.loginpage().login();
	 * 
	 * Map<String, String> dataSet = fc.utobj().readTestData(testCaseId);
	 * 
	 * fc.adminpage().item23ReceiptSummaryLnk( driver); return fddName;
	 * 
	 * }
	 */

	/*
	 * @Parameters({ "fddName" })
	 * 
	 * @Test(groups = { "commonModule", "add", "Call Status"}) public String
	 * sortingITEM23RECEIPTS(@Optional()String fddName) throws Exception {
	 * String testCaseId = "";
	 * 
	 * WebDriver driver = fc.loginpage().login();
	 * 
	 * Map<String, String> dataSet = fc.utobj().readTestData(testCaseId);
	 * 
	 * fc.adminpage().item23ReceiptSummaryLnk( driver); return fddName;
	 * 
	 * }
	 */

	public String addITEM23RECEIPTS_DS(WebDriver driver, String item23Title) throws Exception {

		String testCaseId = "TC_addITEM23RECEIPTS_DS_01";
		if (fc.utobj().validate(testCaseId)) {
			try {
				fc.adminpage().item23ReceiptSummaryLnk(driver);

				AdminFDDManagementITEM23RECEIPTSummaryPage pobj = new AdminFDDManagementITEM23RECEIPTSummaryPage(
						driver);

				fc.utobj().clickElement(driver, pobj.addItem23ReceiptBtn);
				AdminFDDManagementITEM23RECEIPTSummaryAddITEM23RECEIPTPage pobj2 = new AdminFDDManagementITEM23RECEIPTSummaryAddITEM23RECEIPTPage(
						driver);
				item23Title = fc.utobj().generateTestData(item23Title);

				fc.utobj().sendKeys(driver, pobj2.templateTitle, item23Title);

				String content = "$BROKER_NAME$ \n$BROKER_AGENCY$ \n$FRANCHISE_SELLER_NAME$ \n$BUSINESS_NAME$ \n$ADDRESS$ \n$STATE$ \n $COUNTRY$ \n$PHONE$ \n$DATE$";

				fc.utobj().sendKeys(driver, pobj2.item23ReceiptTextArea, content);
				fc.utobj().sendKeys(driver, pobj2.businessName, "FranConnect Inc");
				fc.utobj().sendKeys(driver, pobj2.addressTxt, "B-5, Park Avenue");
				fc.utobj().selectDropDown(driver, pobj2.countryDrp, "USA");

				fc.utobj().selectDropDown(driver, pobj2.stateDrp, "Alaska");
				fc.utobj().sendKeys(driver, pobj2.phone, "9998887777");
				fc.utobj().clickElement(driver, pobj2.previewBtn);
				fc.utobj().clickElement(driver, pobj2.saveBtn);

				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException("was not able to add ITEM RECEIPTS_DS");

		}
		return item23Title;
	}
}
