package com.builds.test.admin;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Optional;

import com.builds.uimaps.admin.AdminFDDmanagementConfigureEmailsentPriortoFDDEmailPage;
import com.builds.utilities.FranconnectUtil;

public class AdminFDDmanagementConfigureEmailsentPriortoFDDEmailPageTest {
	FranconnectUtil fc = new FranconnectUtil();

	/*
	 * @Parameters({ "emailSubject","timeInterval","emailContent" })
	 * 
	 * @Test(groups = { "commonModule", "add", "Add Lead"}) public void
	 * configureEmailSentPriortoFDDEmail(@Optional()String
	 * emailSubject,@Optional()String timeInterval,@Optional()String
	 * emailContent) throws Exception { String testCaseId = "";
	 * 
	 * WebDriver driver = fc.loginpage().login();
	 * 
	 * Map<String, String> dataSet = fc.utobj().readTestData(testCaseId);
	 * 
	 * fc.adminpage().adminPage( driver); fc.utobj().clickLink(driver,
	 * "Configure Email sent Prior to FDD Email");
	 * AdminFDDmanagementConfigureEmailsentPriortoFDDEmailPage pobj = new
	 * AdminFDDmanagementConfigureEmailsentPriortoFDDEmailPage(driver);
	 * fc.utobj().clickElement(driver,pobj.sendEmailNotificationYes);
	 * fc.utobj().sendKeys(driver,pobj.mailSubject,
	 * "Shortly you will receive the FDD mail.");
	 * fc.utobj().sendKeys(driver,pobj.timeInterval, "0"); String windowHandle =
	 * driver.getWindowHandle();
	 * driver.switchTo().frame(fc.utobj().getElement(driver,
	 * pobj.htmlFrame)); fc.utobj().getElement(driver,
	 * pobj.mailText).clear(); //
	 * fc.utobj().getElement(driver,pobj.mailText).clear();
	 * fc.utobj().sendKeys(driver,pobj.mailText,
	 * "We will send you a separate e-mail with a link to FDD. If you do not have a high-speed internet connection, it could take a long time to download the FDD. You also need a copy of ?Adobe Reader,? which you may already have on your computer, or which you can download for free at www.adobe.com. "
	 * ); driver.switchTo().window(windowHandle);
	 * fc.utobj().clickElement(driver,pobj.saveBtn); }
	 */

	public void configureEmailSentPriortoFDDEmail(WebDriver driver, @Optional() String emailSubject,
			@Optional() String timeInterval, @Optional() String emailContent) throws Exception {

		String testCaseId = "TC_configureEmailSentPriortoFDDEmail_02";
		if (fc.utobj().validate(testCaseId)) {
			try {
				fc.adminpage().adminPage(driver);

				fc.utobj().clickLink(driver, "Configure Email sent Prior to FDD Email");
				AdminFDDmanagementConfigureEmailsentPriortoFDDEmailPage pobj = new AdminFDDmanagementConfigureEmailsentPriortoFDDEmailPage(
						driver);
				fc.utobj().clickElement(driver, pobj.sendEmailNotificationYes);
				fc.utobj().sendKeys(driver, pobj.mailSubject, "Shortly you will receive the FDD mail.");
				fc.utobj().sendKeys(driver, pobj.timeInterval, "0");
				String windowHandle = driver.getWindowHandle();
				driver.switchTo().frame(fc.utobj().getElement(driver, pobj.htmlFrame));
				fc.utobj().getElement(driver, pobj.mailText).clear();
				fc.utobj().sendKeys(driver, pobj.mailText,
						"We will send you a separate e-mail with a link to FDD. If you do not have a high-speed internet connection, it could take a long time to download the FDD. You also need a copy of ?Adobe Reader,? which you may already have on your computer, or which you can download for free at www.adobe.com. ");
				driver.switchTo().window(windowHandle);
				fc.utobj().clickElement(driver, pobj.saveBtn);

				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException("was not able to configure Email Sent Prior to FDD Email");

		}
	}
}
