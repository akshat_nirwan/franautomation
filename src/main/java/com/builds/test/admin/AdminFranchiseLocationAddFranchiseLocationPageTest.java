package com.builds.test.admin;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.Reporter;
import org.testng.annotations.Test;

import com.builds.uimaps.admin.AdminAreaRegionAddAreaRegionPage;
import com.builds.uimaps.admin.AdminFranchiseLocationAddFranchiseLocationPage;
import com.builds.uimaps.admin.AdminFranchiseLocationManageFranchiseLocationsAddUserPage;
import com.builds.uimaps.admin.AdminFranchiseLocationManageFranchiseLocationsPage;
import com.builds.utilities.FranconnectUtil;

public class AdminFranchiseLocationAddFranchiseLocationPageTest {
	FranconnectUtil fc = new FranconnectUtil();

	@Test(groups = { "adminsanity" })
	public void addFranchiseLocation() throws Exception {
		Reporter.log("TC_Admin_FranchiseLocation_001 : Admin > Add Franchise Location\n");
		Reporter.log("###################################################################");
		String testCaseId = "TC_Admin_FranchiseLocation_001";

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("admin", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		String franchiseName = fc.utobj().generateTestData(dataSet.get("franchiseName"));

		try {
			driver = fc.loginpage().login(driver);
			String regionName = dataSet.get("regionName");
			regionName = fc.utobj().generateTestData(regionName);
			String storeType = dataSet.get("storeType");
			storeType = fc.utobj().generateTestData(storeType);
			String centerName = dataSet.get("centerName");
			centerName = fc.utobj().generateTestData(centerName);
			String openingDate = dataSet.get("openingDate");
			String loginId = fc.utobj().generateTestData(dataSet.get("loginId"));
			String password = dataSet.get("password");
			String confirmPassword = dataSet.get("confirmPassword");
			String ownerFirstName = fc.utobj().generateTestData(dataSet.get("ownerFirstName"));
			String ownerLastName = fc.utobj().generateTestData(dataSet.get("ownerLastName"));
			String emailId = "testautomation";
			addFranchiseLocationWithUser(driver, regionName, storeType, franchiseName, centerName, openingDate,
					password, confirmPassword, loginId, ownerFirstName, ownerLastName, config, emailId);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	public String addFranchiseLocationWithUser(WebDriver driver, String regionName, String storeType,
			String franchiseName, String centerName, String openingDate, String password, String confirmPassword,
			String loginId, String ownerFirstName, String ownerLastName, Map<String, String> config, String emailId)
			throws Exception {

		String testCaseId = "TC_addFranchiseLocationWithUserAdmin_02";
		if (fc.utobj().validate(testCaseId)) {
			try {
				AdminAreaRegionAddAreaRegionPageTest p1 = new AdminAreaRegionAddAreaRegionPageTest();
				regionName = p1.addAreaRegion(driver, regionName);
				AdminConfigurationConfigureStoreTypePageTest p2 = new AdminConfigurationConfigureStoreTypePageTest();
				storeType = p2.addStoreType(driver, storeType);

				fc.adminpage().adminAddNewFranchiseLocationLnk(driver);
				AdminFranchiseLocationAddFranchiseLocationPage pobj = new AdminFranchiseLocationAddFranchiseLocationPage(
						driver);
				franchiseName = fc.utobj().generateTestData(franchiseName);
				fc.utobj().sendKeys(driver, pobj.franchiseeID, franchiseName);
				fc.utobj().selectDropDownByVisibleTextTrimed(driver, pobj.areaRegion, regionName);
				fc.utobj().sendKeys(driver, pobj.centerName, centerName);
				fc.utobj().selectDropDownByVisibleTextTrimed(driver, pobj.storeTypeId, storeType);
				fc.utobj().sendKeys(driver, pobj.openingDate, openingDate);
				fc.utobj().sendKeys(driver, pobj.city, "Test City");
				fc.utobj().selectDropDown(driver, pobj.stateID, 1);
				fc.utobj().sendKeys(driver, pobj.storePhone, "8989898989");
				// fc.utobj().sendKeys(driver, pobj.fax, "9988668899");
				fc.utobj().sendKeys(driver, pobj.ownerFirstName, ownerFirstName);
				fc.utobj().sendKeys(driver, pobj.ownerLastName, ownerLastName);
				fc.utobj().clickElement(driver, pobj.submit);
				AdminFranchiseLocationManageFranchiseLocationsAddUserPage pobj2 = new AdminFranchiseLocationManageFranchiseLocationsAddUserPage(
						driver);
				fc.utobj().sendKeys(driver, pobj2.loginId, loginId);
				fc.utobj().sendKeys(driver, pobj2.password, password);
				fc.utobj().sendKeys(driver, pobj2.confirmPassword, confirmPassword);
				fc.utobj().selectValFromMultiSelect(driver, pobj2.rolesId, "Default Franchise Role");
				fc.utobj().selectDropDown(driver, pobj2.state, 1);
				fc.utobj().sendKeys(driver, pobj2.phone1, "1234567890");
				fc.utobj().sendKeys(driver, pobj2.phoneExt1, "2");
				fc.utobj().sendKeys(driver, pobj2.email, emailId);
				fc.utobj().clickElement(driver, pobj2.submit);
				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException("was not able to add Franchise Location With User");

		}
		return franchiseName;
	}

	public String addFranchiseLocation(WebDriver driver, String regionName, String storeType, String franchiseName,
			String centerName, String openingDate, String ownerFirstName, String ownerLastName,
			Map<String, String> config) throws Exception {

		String testCaseId = "TC_addFranchiseLocationAdmin_06";
		if (fc.utobj().validate(testCaseId)) {
			try {
				AdminAreaRegionAddAreaRegionPageTest p1 = new AdminAreaRegionAddAreaRegionPageTest();
				regionName = p1.addAreaRegion(driver, regionName);
				AdminConfigurationConfigureStoreTypePageTest p2 = new AdminConfigurationConfigureStoreTypePageTest();
				storeType = p2.addStoreType(driver, storeType);

				fc.adminpage().adminAddNewFranchiseLocationLnk(driver);
				AdminFranchiseLocationAddFranchiseLocationPage pobj = new AdminFranchiseLocationAddFranchiseLocationPage(
						driver);
				fc.utobj().sendKeys(driver, pobj.franchiseeID, franchiseName);

				fc.utobj().selectDropDownByVisibleTextTrimed(driver, pobj.areaRegion, regionName);

				fc.utobj().sendKeys(driver, pobj.centerName, centerName);
				fc.utobj().selectDropDownByVisibleTextTrimed(driver, pobj.storeTypeId, storeType);
				fc.utobj().sendKeys(driver, pobj.openingDate, openingDate);
				fc.utobj().sendKeys(driver, pobj.city, "Test City");
				fc.utobj().selectDropDown(driver, pobj.stateID, 1);
				fc.utobj().sendKeys(driver, pobj.storePhone, "8989898989");

				fc.utobj().sendKeys(driver, pobj.ownerFirstName, ownerFirstName);
				fc.utobj().sendKeys(driver, pobj.ownerLastName, ownerLastName);
				fc.utobj().clickElement(driver, pobj.submit);

				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException("was not able to add Franchise Location Admin");

		}
		return franchiseName;
	}

	public String addFranchiseLocationZC(WebDriver driver, String regionName, String storeType, String franchiseName,
			String centerName, String openingDate, String ownerFirstName, String ownerLastName,
			Map<String, String> config) throws Exception {

		String testCaseId = "TC_addFranchiseLocationAdmin_06";
		if (fc.utobj().validate(testCaseId)) {
			try {

				boolean isStorePresent = false;

				AdminAreaRegionAddAreaRegionPageTest p1 = new AdminAreaRegionAddAreaRegionPageTest();
				regionName = p1.addAreaRegion(driver, regionName);
				AdminConfigurationConfigureStoreTypePageTest p2 = new AdminConfigurationConfigureStoreTypePageTest();

				try {
					storeType = p2.addStoreType(driver, storeType);
					isStorePresent = true;

				} catch (Exception e) {
					isStorePresent = false;
				}

				fc.adminpage().adminAddNewFranchiseLocationLnk(driver);
				AdminFranchiseLocationAddFranchiseLocationPage pobj = new AdminFranchiseLocationAddFranchiseLocationPage(
						driver);
				fc.utobj().sendKeys(driver, pobj.franchiseeID, franchiseName);

				fc.utobj().selectDropDownByVisibleTextTrimed(driver, pobj.areaRegion, regionName);

				fc.utobj().sendKeys(driver, pobj.centerName, centerName);

				if (isStorePresent) {
					fc.utobj().selectDropDownByVisibleTextTrimed(driver, pobj.storeTypeId, storeType);
				}
				fc.utobj().sendKeys(driver, pobj.openingDate, openingDate);
				fc.utobj().sendKeys(driver, pobj.city, "Test City");
				fc.utobj().selectDropDown(driver, pobj.stateID, 1);
				fc.utobj().sendKeys(driver, pobj.storePhone, "8989898989");

				fc.utobj().sendKeys(driver, pobj.ownerFirstName, ownerFirstName);
				fc.utobj().sendKeys(driver, pobj.ownerLastName, ownerLastName);
				fc.utobj().clickElement(driver, pobj.submit);

				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException("was not able to add Franchise Location Admin");

		}
		return franchiseName;
	}

	// *
	public String fillFranchiseLocationDetailsWithMU(WebDriver driver, String userName, String muId, String regionName,
			String storeType, String franchiseName, String centerName, String openingDate, String password,
			String confirmPassword, String loginId, String firstName, String lastName, Map<String, String> config,
			String emailId) throws Exception {

		String testCaseId = "TC_fillFranchiseLocationDetailsWithMU_01";
		if (fc.utobj().validate(testCaseId)) {
			try {

				fc.adminpage().adminAddNewFranchiseLocationLnk(driver);
				AdminFranchiseLocationAddFranchiseLocationPage pobj = new AdminFranchiseLocationAddFranchiseLocationPage(
						driver);
				franchiseName = fc.utobj().generateTestData(franchiseName);
				fc.utobj().sendKeys(driver, pobj.franchiseeID, franchiseName);
				fc.utobj().selectDropDownByVisibleTextTrimed(driver, pobj.areaRegion, regionName);
				fc.utobj().sendKeys(driver, pobj.centerName, centerName);
				fc.utobj().selectDropDownByVisibleTextTrimed(driver, pobj.storeTypeId, storeType);
				fc.utobj().sendKeys(driver, pobj.openingDate, openingDate);
				fc.utobj().sendKeys(driver, pobj.city, "Test City");
				fc.utobj().selectDropDown(driver, pobj.regionNo, 2);
				Thread.sleep(1000);
				fc.utobj().selectDropDown(driver, pobj.stateID, 1);
				fc.utobj().sendKeys(driver, pobj.storePhone, "8989898989");
				// fc.utobj().sendKeys(driver, pobj.fax, "9988668899");
				fc.utobj().clickElement(driver, pobj.ownerTypeExistingOwners);
				multiSelectListBox(driver, pobj.buttonmsChoice, pobj.inputsearchInputMultiple, userName, pobj.checkAll);
				fc.utobj().clickElement(driver, pobj.buttonmsChoice);
				fc.utobj().sendKeys(driver, pobj.muidTxtBox, muId);
				fc.utobj().clickElement(driver, pobj.submit);
				AdminFranchiseLocationManageFranchiseLocationsAddUserPage pobj2 = new AdminFranchiseLocationManageFranchiseLocationsAddUserPage(
						driver);
				fc.utobj().sendKeys(driver, pobj2.loginId, loginId);
				fc.utobj().sendKeys(driver, pobj2.password, password);
				fc.utobj().sendKeys(driver, pobj2.confirmPassword, confirmPassword);
				fc.utobj().selectValFromMultiSelect(driver, pobj2.rolesId, "Default Franchise Role");
				fc.utobj().selectDropDown(driver, pobj2.salutation, "Dr.");
				fc.utobj().sendKeys(driver, pobj2.firstName, firstName);
				fc.utobj().sendKeys(driver, pobj2.lastName, lastName);
				fc.utobj().selectDropDown(driver, pobj2.state, 1);
				fc.utobj().sendKeys(driver, pobj2.phone1, "1234567890");
				fc.utobj().sendKeys(driver, pobj2.phoneExt1, "2");
				fc.utobj().sendKeys(driver, pobj2.email, emailId);
				fc.utobj().clickElement(driver, pobj2.submit);
				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException("was not able to fill Franchise Location Details With MU");
		}
		return franchiseName;
	}

	/*
	 * This method clicks on element without move instruction.
	 */

	public void multiSelectListBox(WebDriver driver, WebElement str, WebElement str1, String searchTxt, WebElement checkBox)
			throws Exception {
		System.out.println("abc");
		WebElement multiSelectbox = fc.utobj().getElement(driver, str1);
		fc.utobj().clickElement(driver, multiSelectbox);

		WebElement newElement = multiSelectbox.findElement(By.xpath("./div/div/input[@class='searchInputMultiple']"));
		fc.utobj().sendKeys(driver, newElement, searchTxt);

		WebElement checkboxElement = null;
		try {
			checkboxElement = multiSelectbox.findElement(By.xpath(".//input[@id='selectAll']"));
			checkboxElement.click();
		} catch (Exception e) {
			Reporter.log(e.getMessage());
			fc.utobj().throwsException("Unable to click Element : " + checkboxElement);
		}

	}

	/*
	 * @Parameters({ "franchiseId", "regionName" })
	 * 
	 * @Test(groups = { "atat" }) public String
	 * addFranchiseLocation_All(@Optional() String franchiseId, @Optional()
	 * String regionName) throws Exception {
	 * 
	 * String testCaseId = "";
	 * 
	 * WebDriver driver = fc.loginpage().login();
	 * 
	 * Map<String, String> dataSet = fc.utobj().readTestData(testCaseId);
	 * 
	 * fc.adminpage().adminAddNewFranchiseLocationLnk(driver);
	 * AdminFranchiseLocationAddFranchiseLocationPage pobj = new
	 * AdminFranchiseLocationAddFranchiseLocationPage( driver); franchiseId =
	 * fc.utobj().generateTestData(franchiseId); fc.utobj().sendKeys(driver,
	 * pobj.franchiseeID, franchiseId); fc.utobj().sendKeys(driver,
	 * pobj.centerName, "0000"); fc.utobj().selectDropDown(driver,
	 * pobj.areaRegion, 1); // fc.utobj().selectDropDown(driver,
	 * pobj.storeTypeId, 1); // fc.utobj().sendKeys(driver, pobj.openingDate,
	 * "12/22/2015"); fc.utobj().sendKeys(driver, pobj.city, "test city");
	 * fc.utobj().selectDropDown(driver, pobj.stateID, 1); //
	 * fc.utobj().sendKeys(driver, pobj.storePhone, "0001");
	 * fc.utobj().sendKeys(driver, pobj.ownerFirstName, "test");
	 * fc.utobj().sendKeys(driver, pobj.ownerLastName, "contact test");
	 * fc.utobj().clickElement(driver, pobj.submit);
	 * 
	 * return franchiseId; }
	 */

	public String addFranchiseLocation_All(WebDriver driver, String franchiseId, String regionName, String storeType)
			throws Exception {

		String testCaseId = "TC_addFranchiseLocation_AllAdmin_01";
		if (fc.utobj().validate(testCaseId)) {
			try {
				AdminAreaRegionAddAreaRegionPageTest addRegion = new AdminAreaRegionAddAreaRegionPageTest();
				addRegion.addAreaRegion(driver, regionName);

				AdminConfigurationConfigureStoreTypePageTest addStoreType = new AdminConfigurationConfigureStoreTypePageTest();
				addStoreType.addStoreType(driver, storeType);
				fc.adminpage().adminAddNewFranchiseLocationLnk(driver);

				AdminFranchiseLocationAddFranchiseLocationPage pobj = new AdminFranchiseLocationAddFranchiseLocationPage(
						driver);
				fc.utobj().sendKeys(driver, pobj.franchiseeID, franchiseId);
				fc.utobj().sendKeys(driver, pobj.centerName, "0000");

				fc.utobj().selectDropDown(driver, pobj.areaRegion, regionName);

				fc.utobj().selectDropDown(driver, pobj.storeTypeId, storeType);

				String curDate = fc.utobj().getFutureDateUSFormat(-365);

				fc.utobj().sendKeys(driver, pobj.reportPeriodStartDate, curDate);

				fc.utobj().sendKeys(driver, pobj.openingDate, curDate);
				fc.utobj().sendKeys(driver, pobj.city, "test city");

				fc.utobj().selectDropDown(driver, pobj.stateID, 1);
				fc.utobj().sendKeys(driver, pobj.storePhone, "0001");
				fc.utobj().sendKeys(driver, pobj.ownerFirstName, "firstName");
				fc.utobj().sendKeys(driver, pobj.ownerLastName, "lastName");
				fc.utobj().clickElement(driver, pobj.submit);

				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException("was not able to add Franchise Location_All");

		}
		return franchiseId;
	}

	public String addFranchiseLocation_All_Agreement(WebDriver driver, String franchiseId, String regionName,
			String storeType, String agreementVersion, Map<String, String> config) throws Exception {

		String testCaseId = "TC_Add_Franchise_Location_All_Admin_Agreement_Version";
		if (fc.utobj().validate(testCaseId)) {
			try {
				AdminAreaRegionAddAreaRegionPageTest addRegion = new AdminAreaRegionAddAreaRegionPageTest();
				addRegion.addAreaRegion(driver, regionName);

				AdminConfigurationConfigureStoreTypePageTest addStoreType = new AdminConfigurationConfigureStoreTypePageTest();
				addStoreType.addStoreType(driver, storeType);
								
				fc.adminpage().adminAddNewFranchiseLocationLnk(driver);

				AdminFranchiseLocationAddFranchiseLocationPage pobj = new AdminFranchiseLocationAddFranchiseLocationPage(driver);
				
				fc.utobj().sendKeys(driver, pobj.franchiseeID, franchiseId);
				
				fc.utobj().sendKeys(driver, pobj.centerName, "0000");

				fc.utobj().selectDropDown(driver, pobj.areaRegion, regionName);

				fc.utobj().selectDropDown(driver, pobj.storeTypeId, "Default");

				fc.utobj().selectDropDown(driver, pobj.versionID, agreementVersion);

				String curDate = fc.utobj().getFutureDateUSFormat(-365);
				fc.utobj().sendKeys(driver, pobj.reportPeriodStartDate, curDate);
				fc.utobj().sendKeys(driver, pobj.openingDate, curDate);
				fc.utobj().sendKeys(driver, pobj.city, "test city");

				fc.utobj().selectDropDown(driver, pobj.stateID, 1);
				fc.utobj().sendKeys(driver, pobj.storePhone, "0001");
				fc.utobj().sendKeys(driver, pobj.ownerFirstName, "firstName");
				fc.utobj().sendKeys(driver, pobj.ownerLastName, "lastName");
				fc.utobj().clickElement(driver, pobj.submit);

				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
				
			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException("was not able to add Franchise Location_All");

		}
		return franchiseId;
	}

	public String addFranchiseLocationWithDivision(WebDriver driver, Map<String, String> config, String franchiseId,
			String regionName, String storeType, String divisionName) throws Exception {

		String testCaseId = "TC_Add_Franchise_Location_With_Division";
		if (fc.utobj().validate(testCaseId)) {
			try {
				AdminAreaRegionAddAreaRegionPageTest addRegion = new AdminAreaRegionAddAreaRegionPageTest();
				addRegion.addAreaRegion(driver, regionName);

				AdminConfigurationConfigureStoreTypePageTest addStoreType = new AdminConfigurationConfigureStoreTypePageTest();
				addStoreType.addStoreType(driver, storeType);

				AdminDivisionAddDivisionPageTest division_page = new AdminDivisionAddDivisionPageTest();
				division_page.addDivision(driver, divisionName);

				fc.adminpage().adminAddNewFranchiseLocationLnk(driver);
				AdminFranchiseLocationAddFranchiseLocationPage pobj = new AdminFranchiseLocationAddFranchiseLocationPage(
						driver);

				fc.utobj().sendKeys(driver, pobj.franchiseeID, franchiseId);
				fc.utobj().sendKeys(driver, pobj.centerName, "0000");

				fc.utobj().selectDropDown(driver, pobj.areaRegion, regionName);
				fc.utobj().selectDropDown(driver, pobj.drpDivisionBrand, divisionName);
				fc.utobj().selectDropDown(driver, pobj.storeTypeId, storeType);

				String curDate = fc.utobj().getCurrentDateUSFormat();
				fc.utobj().sendKeys(driver, pobj.openingDate, curDate);
				fc.utobj().sendKeys(driver, pobj.city, "test city");
				fc.utobj().selectDropDown(driver, pobj.stateID, 1);
				fc.utobj().sendKeys(driver, pobj.storePhone, "0001");
				fc.utobj().sendKeys(driver, pobj.ownerFirstName, "firstName");
				fc.utobj().sendKeys(driver, pobj.ownerLastName, "lastName");
				fc.utobj().clickElement(driver, pobj.submit);

				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException("was not able to add Franchise Location_All");

		}
		return franchiseId;
	}

	public String addFranchiseLocation_AllMuid(WebDriver driver, String franchiseId, String regionName,
			String storeType, String firstName, String lastName, Map<String, String> config) throws Exception {

		String testCaseId = "TC_Add_Franchise_Location_AllMuid_01";
		if (fc.utobj().validate(testCaseId)) {
			try {
				AdminAreaRegionAddAreaRegionPageTest addRegion = new AdminAreaRegionAddAreaRegionPageTest();
				addRegion.addAreaRegion(driver, regionName);

				AdminConfigurationConfigureStoreTypePageTest addStoreType = new AdminConfigurationConfigureStoreTypePageTest();
				addStoreType.addStoreType(driver, storeType);

				fc.adminpage().adminAddNewFranchiseLocationLnk(driver);
				AdminFranchiseLocationAddFranchiseLocationPage pobj = new AdminFranchiseLocationAddFranchiseLocationPage(
						driver);
				fc.utobj().sendKeys(driver, pobj.franchiseeID, franchiseId);
				fc.utobj().sendKeys(driver, pobj.centerName, "0000");

				fc.utobj().selectDropDown(driver, pobj.areaRegion, regionName);

				fc.utobj().selectDropDown(driver, pobj.storeTypeId, storeType);
				String curDate = fc.utobj().getCurrentDateUSFormat();
				fc.utobj().sendKeys(driver, pobj.openingDate, curDate);
				fc.utobj().sendKeys(driver, pobj.city, "test city");

				fc.utobj().selectDropDown(driver, pobj.stateID, 1);

				fc.utobj().sendKeys(driver, pobj.storePhone, "0001");
				
				if (!fc.utobj().isSelected(driver, pobj.ownerTypeNewOwner)) {
					fc.utobj().clickElement(driver, pobj.ownerTypeNewOwner);
				}
				
				fc.utobj().sendKeys(driver, pobj.ownerFirstName, firstName);
				fc.utobj().sendKeys(driver, pobj.ownerLastName, lastName);
				fc.utobj().clickElement(driver, pobj.submit);

				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException("was not able to add Franchise Location_All Muid");

		}
		return franchiseId;
	}


	@Test(groups = { "atat" })
	public void addFranchiseLocationWithMU() throws Exception {
		Reporter.log("TC_FIM_Add_MU_001 : Admin > Add Region / Store / Location / User Add MU \n");
		Reporter.log("###################################################################");
		String testCaseId = "TC_FIM_Add_MU_001";

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("admin", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		String franchiseName = dataSet.get("franchiseName");

		try {
			driver = fc.loginpage().login(driver);
			String regionName = dataSet.get("regionName");
			regionName = fc.utobj().generateTestData(regionName);
			String storeType = dataSet.get("storeType");
			storeType = fc.utobj().generateTestData(storeType);
			String centerName = dataSet.get("centerName");
			centerName = fc.utobj().generateTestData(centerName);
			String openingDate = dataSet.get("openingDate");
			String loginId = fc.utobj().generateTestData(dataSet.get("loginId"));
			String password = dataSet.get("password");
			String confirmPassword = dataSet.get("confirmPassword");
			String muId = dataSet.get("muId");
			muId = fc.utobj().generateTestData("muId");
			String ownerFirstName = fc.utobj().generateTestData(dataSet.get("ownerFirstName"));
			String ownerLastName = fc.utobj().generateTestData(dataSet.get("ownerLastName"));
			String emailId = "testautomation@franqa.net";
			franchiseName = addFranchiseLocationWithUser(driver, regionName, storeType, franchiseName, centerName,
					openingDate, password, confirmPassword, loginId, ownerFirstName, ownerLastName, config, emailId);
			String userName = ownerFirstName + " " + ownerLastName;
			AdminFranchiseLocationManageFranchiseLocationsPage pobj = new AdminFranchiseLocationManageFranchiseLocationsPage(
					driver);
			fc.utobj().clickElement(driver, pobj.addFranchiseLocation);

			loginId = fc.utobj().generateTestData(dataSet.get("loginId"));
			String firstName = fc.utobj().generateTestData(dataSet.get("firstName"));
			String lastName = fc.utobj().generateTestData(dataSet.get("lastName"));

			fillFranchiseLocationDetailsWithMU(driver, userName, muId, regionName, storeType, franchiseName, centerName,
					openingDate, password, confirmPassword, loginId, firstName, lastName, config, emailId);

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	public String franchiseLocationWithMUID(WebDriver driver, String franchiseeID, String regionName, String centerName,
			String openingDate, String storeType, String franchiseUserName, String muId, Map<String, String> config)
			throws Exception {

		String testCaseId = "TC_franchiseLocationWithMUID_02";
		if (fc.utobj().validate(testCaseId)) {
			try {
				// regionName = fc.utobj().generateTestData(regionName);

				fc.adminpage().adminAddNewFranchiseLocationLnk(driver);
				AdminFranchiseLocationAddFranchiseLocationPage pobj = new AdminFranchiseLocationAddFranchiseLocationPage(
						driver);
				fc.utobj().sendKeys(driver, pobj.franchiseeID, franchiseeID);
				fc.utobj().selectDropDownByVisibleTextTrimed(driver, pobj.areaRegion, regionName);
				fc.utobj().sendKeys(driver, pobj.centerName, centerName);
				fc.utobj().selectDropDownByVisibleTextTrimed(driver, pobj.storeTypeId, storeType);
				fc.utobj().sendKeys(driver, pobj.openingDate, openingDate);
				fc.utobj().sendKeys(driver, pobj.city, "Test City");
				fc.utobj().selectDropDown(driver, pobj.regionNo, 2);
				Thread.sleep(1000);
				fc.utobj().selectDropDown(driver, pobj.stateID, 1);
				fc.utobj().sendKeys(driver, pobj.storePhone, "8989898989");
				fc.utobj().clickElement(driver, pobj.ownerTypeExistingOwners);
				fc.utobj().selectValFromMultiSelect(driver, pobj.selectOwnerMulti, franchiseUserName);
				fc.utobj().sendKeys(driver, pobj.muidTxtBox, muId);
				fc.utobj().clickElement(driver, pobj.submit);

				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException("was not able to add franchise Location With MUID");

		}
		return muId;
	}

	public String addFranchiseLocation_AllWithFName(WebDriver driver, String franchiseId, String regionName,
			String storeType, String firstName) throws Exception {

		String testCaseId = "TC_addFranchiseLocation_AllWithFName_01";
		if (fc.utobj().validate(testCaseId)) {
			try {
				// Area Region
				AdminAreaRegionAddAreaRegionPageTest addRegion = new AdminAreaRegionAddAreaRegionPageTest();
				addRegion.addAreaRegion(driver, regionName);

				// Store Type

				AdminConfigurationConfigureStoreTypePageTest addStoreType = new AdminConfigurationConfigureStoreTypePageTest();
				addStoreType.addStoreType(driver, storeType);

				fc.adminpage().adminAddNewFranchiseLocationLnk(driver);
				AdminFranchiseLocationAddFranchiseLocationPage pobj = new AdminFranchiseLocationAddFranchiseLocationPage(
						driver);
				fc.utobj().sendKeys(driver, pobj.franchiseeID, franchiseId);
				fc.utobj().sendKeys(driver, pobj.centerName, "0000");

				fc.utobj().selectDropDown(driver, pobj.areaRegion, regionName);

				fc.utobj().selectDropDown(driver, pobj.storeTypeId, storeType);

				DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
				Date date = new Date();
				String curDate = dateFormat.format(date);
				System.out.println(curDate);
				fc.utobj().sendKeys(driver, pobj.openingDate, curDate);
				fc.utobj().sendKeys(driver, pobj.city, "test city");

				fc.utobj().selectDropDown(driver, pobj.stateID, 1);

				fc.utobj().sendKeys(driver, pobj.storePhone, "0001");
				fc.utobj().sendKeys(driver, pobj.ownerFirstName, firstName);
				fc.utobj().sendKeys(driver, pobj.ownerLastName, firstName);
				fc.utobj().clickElement(driver, pobj.submit);

				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException("was not able to add Franchise Location_All With First Name");

		}
		return franchiseId;
	}

	public String addFranchiseLocationForMkt(WebDriver driver, String franchiseId, String regionName, String firstName,
			String lastName) throws Exception {

		String testCaseId = "TC_addFranchiseLocationForMkt_07";
		if (fc.utobj().validate(testCaseId)) {
			try {
				// Area Region
				AdminAreaRegionAddAreaRegionPageTest addRegion = new AdminAreaRegionAddAreaRegionPageTest();
				addRegion.addAreaRegion(driver, regionName);

				fc.adminpage().adminAddNewFranchiseLocationLnk(driver);
				AdminFranchiseLocationAddFranchiseLocationPage pobj = new AdminFranchiseLocationAddFranchiseLocationPage(
						driver);
				fc.utobj().sendKeys(driver, pobj.franchiseeID, franchiseId);
				fc.utobj().sendKeys(driver, pobj.centerName, "0000");

				fc.utobj().selectDropDown(driver, pobj.areaRegion, regionName);

				fc.utobj().sendKeys(driver, pobj.openingDate, fc.utobj().getCurrentDateUSFormat());
				fc.utobj().sendKeys(driver, pobj.address, "Street Address");
				fc.utobj().sendKeys(driver, pobj.zipcode, "224122");

				fc.utobj().sendKeys(driver, pobj.city, "test city");

				fc.utobj().selectDropDown(driver, pobj.stateID, 1);
				fc.utobj().sendKeys(driver, pobj.storeFax, "11111");
				fc.utobj().sendKeys(driver, pobj.storePhone, "0001");
				fc.utobj().selectDropDown(driver, pobj.storeTypeId, "Default");
				fc.utobj().sendKeys(driver, pobj.storeEmail, "crmautomation@staffex.com");
				fc.utobj().sendKeys(driver, pobj.storeWebSite, "https://www.google.com");
				fc.utobj().sendKeys(driver, pobj.ownerFirstName, firstName);
				fc.utobj().sendKeys(driver, pobj.ownerLastName, lastName);
				fc.utobj().clickElement(driver, pobj.submit);

				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException("was not able to add Franchise Location for Mkt");

		}
		return franchiseId;
	}
	
	//vipinS
	         public String AddfranchiseuserwithlocationforMKT(WebDriver driver,String franchiseId, String regionName, String firstName,
			     String lastName,String loginId,String Role,String userType, String Owner_New_or_Existing, String existingownerifrequired) throws Exception
	         {
	        	 String testCaseId = "";
		try {
			AdminAreaRegionAddAreaRegionPageTest addRegion = new AdminAreaRegionAddAreaRegionPageTest();
			addRegion.addAreaRegion(driver, regionName);

			fc.adminpage().adminAddNewFranchiseLocationLnk(driver);
			AdminFranchiseLocationAddFranchiseLocationPage pobj = new AdminFranchiseLocationAddFranchiseLocationPage(
					driver);
			fc.utobj().sendKeys(driver, pobj.franchiseeID, franchiseId);
			fc.utobj().sendKeys(driver, pobj.centerName, fc.utobj().generateTestData("0"));

			fc.utobj().selectDropDown(driver, pobj.areaRegion, regionName);

			fc.utobj().sendKeys(driver, pobj.openingDate, fc.utobj().getCurrentDateUSFormat());
			fc.utobj().sendKeys(driver, pobj.address, "Street Address");
			fc.utobj().sendKeys(driver, pobj.zipcode, "224122");

			fc.utobj().sendKeys(driver, pobj.city, "test city");

			fc.utobj().selectDropDown(driver, pobj.stateID, 1);
			fc.utobj().sendKeys(driver, pobj.storeFax, "11111");
			fc.utobj().sendKeys(driver, pobj.storePhone, "0001");
			fc.utobj().selectDropDown(driver, pobj.storeTypeId, "Default");
			fc.utobj().sendKeys(driver, pobj.storeEmail, "crmautomation@staffex.com");
			fc.utobj().sendKeys(driver, pobj.storeWebSite, "https://www.google.com");
			if(Owner_New_or_Existing.equalsIgnoreCase("New")) {
					if(!fc.utobj().isSelected(driver, pobj.Newownercheckbox)) {
					fc.utobj().clickElement(driver, pobj.Newownercheckbox);
					
				}
				
			}else if(Owner_New_or_Existing.equalsIgnoreCase("Existing")) {
				if(!fc.utobj().isSelected(driver, pobj.existingownercheckbox)) {
					fc.utobj().clickElement(driver, pobj.existingownercheckbox);
					fc.utobj().sleep(500);
					fc.utobj().clickElement(driver, pobj.SelectOwner);
					fc.utobj().sendKeys(driver, pobj.Search_bar, existingownerifrequired);;
					fc.utobj().sleep(1000);
					fc.utobj().clickElement(driver, pobj.Search_bar_button);
				}
			}
			if(Owner_New_or_Existing.equalsIgnoreCase("New")) 
			{
				
				fc.utobj().sendKeys(driver, pobj.ownerFirstName, firstName);
				fc.utobj().sendKeys(driver, pobj.ownerLastName, lastName);
			}
			else if (Owner_New_or_Existing.equalsIgnoreCase("Existing"))
			{
			 System.out.println("already");
			}
			
			fc.utobj().clickElement(driver, pobj.submit);
			
			
			AdminFranchiseLocationManageFranchiseLocationsAddUserPage pobj2 = new AdminFranchiseLocationManageFranchiseLocationsAddUserPage(
					driver);
			fc.utobj().sendKeys(driver, pobj2.loginId, loginId);
			fc.utobj().sendKeys(driver, pobj2.password, "fran1234");
			fc.utobj().sendKeys(driver, pobj2.confirmPassword, "fran1234");
			fc.utobj().selectValFromMultiSelect(driver, pobj2.rolesId, Role);
			if (Owner_New_or_Existing.equalsIgnoreCase("Existing")) {
				
				fc.utobj().sendKeys(driver, pobj2.salutation, "Mr.");
			}
			if(Owner_New_or_Existing.equalsIgnoreCase("New")) 
			{
				fc.utobj().printTestStep("first name already filled");
			}
			else if (Owner_New_or_Existing.equalsIgnoreCase("Existing"))
			{
				fc.utobj().sendKeys(driver, pobj2.firstName, firstName);
				fc.utobj().sendKeys(driver, pobj2.lastName, lastName);
			}
			fc.utobj().selectDropDown(driver, pobj2.state, 1);
			fc.utobj().sendKeys(driver, pobj2.phone1, "1234567890");
			fc.utobj().sendKeys(driver, pobj2.phoneExt1, "2");
			fc.utobj().sendKeys(driver, pobj2.email, fc.utobj().generateTestData("selenoumauto")+"@gmail.com");
			fc.utobj().clickElement(driver, pobj2.submit);
			
			
		}catch (Exception e) {
			fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
		}
		return loginId;
	}
	
	         public String AddfranchiseuserwithlocationforMKT_existingregion(WebDriver driver,String franchiseId, String regionName, String firstName,
				     String lastName,String loginId,String Role,String userType, String Owner_New_or_Existing,String existingownerifrequired) throws Exception
		         {
		        	 String testCaseId = "";
			try {
				
				fc.adminpage().adminAddNewFranchiseLocationLnk(driver);
				AdminFranchiseLocationAddFranchiseLocationPage pobj = new AdminFranchiseLocationAddFranchiseLocationPage(
						driver);
				fc.utobj().sendKeys(driver, pobj.franchiseeID, franchiseId);
				fc.utobj().sendKeys(driver, pobj.centerName, fc.utobj().generateTestData("0"));

				fc.utobj().selectDropDown(driver, pobj.areaRegion, regionName);

				fc.utobj().sendKeys(driver, pobj.openingDate, fc.utobj().getCurrentDateUSFormat());
				fc.utobj().sendKeys(driver, pobj.address, "Street Address");
				fc.utobj().sendKeys(driver, pobj.zipcode, "224122");

				fc.utobj().sendKeys(driver, pobj.city, "test city");

				fc.utobj().selectDropDown(driver, pobj.stateID, 1);
				fc.utobj().sendKeys(driver, pobj.storeFax, "11111");
				fc.utobj().sendKeys(driver, pobj.storePhone, "0001");
				fc.utobj().selectDropDown(driver, pobj.storeTypeId, "Default");
				fc.utobj().sendKeys(driver, pobj.storeEmail, "crmautomation@staffex.com");
				fc.utobj().sendKeys(driver, pobj.storeWebSite, "https://www.google.com");
				if(Owner_New_or_Existing.equalsIgnoreCase("New")) {
					if(!fc.utobj().isSelected(driver, pobj.Newownercheckbox)) {
						fc.utobj().clickElement(driver, pobj.Newownercheckbox);
						//fc.utobj().sendKeys(driver, pobj.ownerFirstName, firstName);
						//fc.utobj().sendKeys(driver, pobj.ownerLastName, lastName);
					}
					
				}else if(Owner_New_or_Existing.equalsIgnoreCase("Existing")) {
					if(!fc.utobj().isSelected(driver, pobj.existingownercheckbox)) {
						fc.utobj().clickElement(driver, pobj.existingownercheckbox);
						fc.utobj().sleep(500);
						fc.utobj().clickElement(driver, pobj.SelectOwner);
						fc.utobj().sendKeys(driver, pobj.Search_bar, existingownerifrequired);;
						fc.utobj().sleep(1000);
						fc.utobj().clickElement(driver, pobj.Search_bar_button);
						fc.utobj().clickElement(driver, ".//input[@id='selectAll']");
						fc.utobj().sendKeys(driver, pobj.muidTxtBox, fc.utobj().generateTestData("1"));
					
					}
				}
				if(Owner_New_or_Existing.equalsIgnoreCase("New")) 
				{
					
					fc.utobj().sendKeys(driver, pobj.ownerFirstName, firstName);
					fc.utobj().sendKeys(driver, pobj.ownerLastName, lastName);
				}
				else if (Owner_New_or_Existing.equalsIgnoreCase("Existing"))
				{
				 System.out.println("already");
				}
				fc.utobj().clickElement(driver, pobj.submit);
				
				
				AdminFranchiseLocationManageFranchiseLocationsAddUserPage pobj2 = new AdminFranchiseLocationManageFranchiseLocationsAddUserPage(
						driver);
				fc.utobj().sendKeys(driver, pobj2.loginId, loginId);
				fc.utobj().sendKeys(driver, pobj2.password, "fran1234");
				fc.utobj().sendKeys(driver, pobj2.confirmPassword, "fran1234");
				fc.utobj().selectValFromMultiSelect(driver, pobj2.rolesId, Role);
				if (Owner_New_or_Existing.equalsIgnoreCase("Existing")) {
					
					fc.utobj().sendKeys(driver, pobj2.salutation, "Mr.");
				}
				if(Owner_New_or_Existing.equalsIgnoreCase("New")) 
				{
					fc.utobj().printTestStep("first name already filled");
				}
				else if (Owner_New_or_Existing.equalsIgnoreCase("Existing"))
				{
					fc.utobj().sendKeys(driver, pobj2.firstName, firstName);
					fc.utobj().sendKeys(driver, pobj2.lastName, lastName);
				}
				fc.utobj().selectDropDown(driver, pobj2.state, 1);
				fc.utobj().sendKeys(driver, pobj2.phone1, "1234567890");
				fc.utobj().sendKeys(driver, pobj2.phoneExt1, "2");
				fc.utobj().sendKeys(driver, pobj2.email, fc.utobj().generateTestData("selenoumauto")+"@gmail.com");
				fc.utobj().clickElement(driver, pobj2.submit);
				
				
			}catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
			return loginId;
		}
	         
	         
	         
	         

	public String addFranchiseLocationWithUserZCWithRegionNameExist(WebDriver driver, String regionName,
			String storeType, String franchiseName, String centerName, String openingDate, String password,
			String confirmPassword, String loginId, String ownerFirstName, String ownerLastName,
			Map<String, String> config, String emailId) throws Exception {

		String testCaseId = "TC_addFranchiseLocationWithUserAdmin_02";
		try {

			fc.adminpage().adminAddNewFranchiseLocationLnk(driver);
			AdminFranchiseLocationAddFranchiseLocationPage pobj = new AdminFranchiseLocationAddFranchiseLocationPage(
					driver);
			franchiseName = fc.utobj().generateTestData(franchiseName);
			fc.utobj().sendKeys(driver, pobj.franchiseeID, franchiseName);
			fc.utobj().selectDropDownByVisibleTextTrimed(driver, pobj.areaRegion, regionName);
			fc.utobj().sendKeys(driver, pobj.centerName, centerName);
			// fc.utobj().selectDropDownByVisibleTextTrimed(pobj.storeTypeId,
			// storeType);
			fc.utobj().selectDropDown(driver, pobj.storeTypeId, storeType);
			fc.utobj().sendKeys(driver, pobj.openingDate, openingDate);
			fc.utobj().sendKeys(driver, pobj.city, "Test City");
			fc.utobj().selectDropDown(driver, pobj.stateID, 1);
			fc.utobj().sendKeys(driver, pobj.storePhone, "8989898989");
			// fc.utobj().sendKeys(driver, pobj.fax, "9988668899");
			fc.utobj().sendKeys(driver, pobj.ownerFirstName, ownerFirstName);
			fc.utobj().sendKeys(driver, pobj.ownerLastName, ownerLastName);
			//New Line Added
			fc.utobj().selectDropDownByVisibleTextTrimed(driver, pobj.storeTypeId,"Default");
			
			fc.utobj().clickElement(driver, pobj.submit);
			AdminFranchiseLocationManageFranchiseLocationsAddUserPage pobj2 = new AdminFranchiseLocationManageFranchiseLocationsAddUserPage(
					driver);
			fc.utobj().sendKeys(driver, pobj2.loginId, loginId);
			fc.utobj().sendKeys(driver, pobj2.password, password);
			fc.utobj().sendKeys(driver, pobj2.confirmPassword, confirmPassword);
			fc.utobj().selectValFromMultiSelect(driver, pobj2.rolesId, "Default Franchise Role");
			fc.utobj().selectDropDown(driver, pobj2.state, 1);
			fc.utobj().sendKeys(driver, pobj2.phone1, "1234567890");
			fc.utobj().sendKeys(driver, pobj2.phoneExt1, "2");
			fc.utobj().sendKeys(driver, pobj2.email, emailId);
			fc.utobj().clickElement(driver, pobj2.submit);
			fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
		}
		return franchiseName;
	}

	public String addFranchiseLocationWithUserZC(WebDriver driver, String regionName, String storeTyp,
			String franchiseName, String centerName, String openingDate, String password, String confirmPassword,
			String loginId, String ownerFirstName, String ownerLastName, Map<String, String> config, String emailId)
			throws Exception {

		String testCaseId = "TC_addFranchiseLocationWithUserAdmin_02";
		try {
			AdminAreaRegionAddAreaRegionPageTest p1 = new AdminAreaRegionAddAreaRegionPageTest();
			regionName = p1.addAreaRegion(driver, regionName);

			fc.adminpage().adminAddNewFranchiseLocationLnk(driver);
			AdminFranchiseLocationAddFranchiseLocationPage pobj = new AdminFranchiseLocationAddFranchiseLocationPage(
					driver);
			franchiseName = fc.utobj().generateTestData(franchiseName);
			fc.utobj().sendKeys(driver, pobj.franchiseeID, franchiseName);
			fc.utobj().selectDropDownByVisibleTextTrimed(driver, pobj.areaRegion, regionName);
			fc.utobj().sendKeys(driver, pobj.centerName, centerName);
			fc.utobj().sendKeys(driver, pobj.openingDate, openingDate);
			fc.utobj().sendKeys(driver, pobj.city, "Test City");
			fc.utobj().selectDropDown(driver, pobj.stateID, 1);
			fc.utobj().sendKeys(driver, pobj.storePhone, "8989898989");
			fc.utobj().selectDropDown(driver, pobj.storeTypeId, "Default");
			fc.utobj().sendKeys(driver, pobj.ownerFirstName, ownerFirstName);
			fc.utobj().sendKeys(driver, pobj.ownerLastName, ownerLastName);
			fc.utobj().clickElement(driver, pobj.submit);
			AdminFranchiseLocationManageFranchiseLocationsAddUserPage pobj2 = new AdminFranchiseLocationManageFranchiseLocationsAddUserPage(
					driver);
			fc.utobj().sendKeys(driver, pobj2.loginId, loginId);
			fc.utobj().sendKeys(driver, pobj2.password, password);
			fc.utobj().sendKeys(driver, pobj2.confirmPassword, confirmPassword);
			fc.utobj().selectValFromMultiSelect(driver, pobj2.rolesId, "Default Franchise Role");
			fc.utobj().selectDropDown(driver, pobj2.state, 1);
			fc.utobj().sendKeys(driver, pobj2.phone1, "1234567890");
			fc.utobj().sendKeys(driver, pobj2.phoneExt1, "2");
			fc.utobj().sendKeys(driver, pobj2.email, emailId);
			fc.utobj().clickElement(driver,pobj2.submit);
			
			fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
		}
		return franchiseName;
	}

	public String addFranchiseLocationForMktwithoutaddingarearegion(WebDriver driver, String franchiseId, String regionName, String firstName,
			String lastName,Map<String,String> dataSet) throws Exception {


		String testCaseId = "TC_addFranchiseLocationForMkt_07";
		if (fc.utobj().validate(testCaseId)) {
			try {
				// Area Region
				/*AdminAreaRegionAddAreaRegionPageTest addRegion = new AdminAreaRegionAddAreaRegionPageTest();
				addRegion.addAreaRegion(driver, regionName);
*/
				fc.adminpage().adminAddNewFranchiseLocationLnk(driver);
				AdminFranchiseLocationAddFranchiseLocationPage pobj = new AdminFranchiseLocationAddFranchiseLocationPage(
						driver);
				fc.utobj().sendKeys(driver, pobj.franchiseeID, franchiseId);
				fc.utobj().sendKeys(driver, pobj.centerName,dataSet.get("Francentername"));

				fc.utobj().selectDropDown(driver, pobj.areaRegion, regionName);

				fc.utobj().sendKeys(driver, pobj.openingDate, fc.utobj().getCurrentDateUSFormat());
				fc.utobj().sendKeys(driver, pobj.address,dataSet.get("Franaddress"));
				fc.utobj().sendKeys(driver, pobj.zipcode,dataSet.get("Fanzipcode"));

				fc.utobj().sendKeys(driver, pobj.city,dataSet.get("Francity"));

				fc.utobj().selectDropDown(driver, pobj.stateID, dataSet.get("Franstateid"));
				fc.utobj().sendKeys(driver, pobj.storeFax,dataSet.get("FranstoreFax"));
				fc.utobj().sendKeys(driver, pobj.storePhone,dataSet.get("FranstorePhone"));
				fc.utobj().selectDropDown(driver, pobj.storeTypeId,dataSet.get("FranstoreTypeId"));
				fc.utobj().sendKeys(driver, pobj.storeEmail,dataSet.get("FranstoreEmail")+"@staffex.com");
				fc.utobj().sendKeys(driver, pobj.storeWebSite,dataSet.get("FranstoreWebSite"));
				fc.utobj().sendKeys(driver, pobj.ownerFirstName, firstName);
				fc.utobj().sendKeys(driver, pobj.ownerLastName, lastName);
				fc.utobj().clickElement(driver, pobj.submit);

				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException("was not able to add Franchise Location for Mkt");

		}
		return franchiseId;
	
		
	}
	
	public String addFranchiseLocationForMktwithoutaddingarearegionwithexistingowner(WebDriver driver, String franchiseId, String regionName, String firstName,
			String lastName,Map<String,String> dataSet) throws Exception {


		String testCaseId = "TC_addFranchiseLocationForMkt_07";
		if (fc.utobj().validate(testCaseId)) {
			try {
				// Area Region
				/*AdminAreaRegionAddAreaRegionPageTest addRegion = new AdminAreaRegionAddAreaRegionPageTest();
				addRegion.addAreaRegion(driver, regionName);
*/
				fc.adminpage().adminAddNewFranchiseLocationLnk(driver);
				AdminFranchiseLocationAddFranchiseLocationPage pobj = new AdminFranchiseLocationAddFranchiseLocationPage(
						driver);
				fc.utobj().sendKeys(driver, pobj.franchiseeID, franchiseId);
				fc.utobj().sendKeys(driver, pobj.centerName,dataSet.get("Francentername"));

				fc.utobj().selectDropDown(driver, pobj.areaRegion, regionName);

				fc.utobj().sendKeys(driver, pobj.openingDate, fc.utobj().getCurrentDateUSFormat());
				fc.utobj().sendKeys(driver, pobj.address,dataSet.get("Franaddress"));
				fc.utobj().sendKeys(driver, pobj.zipcode,dataSet.get("Fanzipcode"));

				fc.utobj().sendKeys(driver, pobj.city,dataSet.get("Francity"));

				fc.utobj().selectDropDown(driver, pobj.stateID, dataSet.get("Franstateid"));
				fc.utobj().sendKeys(driver, pobj.storeFax,dataSet.get("FranstoreFax"));
				fc.utobj().sendKeys(driver, pobj.storePhone,dataSet.get("FranstorePhone"));
				fc.utobj().selectDropDown(driver, pobj.storeTypeId,dataSet.get("FranstoreTypeId"));
				fc.utobj().sendKeys(driver, pobj.storeEmail,dataSet.get("FranstoreEmail")+"@staffex.com");
				fc.utobj().sendKeys(driver, pobj.storeWebSite,dataSet.get("FranstoreWebSite"));
				/*fc.utobj().sendKeys(driver, pobj.ownerFirstName, firstName);
				fc.utobj().sendKeys(driver, pobj.ownerLastName, lastName);*/
				fc.utobj().clickElement(driver, pobj.existingowner);
				fc.crm().crm_common().MultiSelect_DropDown(driver, firstName+" "+lastName);
				fc.utobj().sendKeys(driver, pobj.existingownerName, fc.utobj().generateTestData(dataSet.get("FranExistingowner")));
				fc.utobj().clickElement(driver, pobj.submit);

				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException("was not able to add Franchise Location for Mkt");

		}
		return franchiseId;
	
		
	}

	
	
	
}