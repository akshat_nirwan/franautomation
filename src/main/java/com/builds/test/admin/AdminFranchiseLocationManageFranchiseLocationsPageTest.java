package com.builds.test.admin;

import org.openqa.selenium.WebDriver;

import com.builds.test.common.Location;
import com.builds.uimaps.admin.AdminFranchiseLocationManageFranchiseLocationsPageUI;
import com.builds.utilities.FranconnectUtil;

public class AdminFranchiseLocationManageFranchiseLocationsPageTest {

	FranconnectUtil fc = new FranconnectUtil();

	public AdminFranchiseLocationManageFranchiseLocationsPageTest SearchFranchise(WebDriver driver, Location location) throws Exception {

		AdminFranchiseLocationManageFranchiseLocationsPageUI ui = new AdminFranchiseLocationManageFranchiseLocationsPageUI(
				driver);
		fc.utobj().printTestStep("Search Frachise");

		if (location.getFranchiseID() != null) {
			fc.utobj().selectValFromMultiSelect(driver, ui.FranshieID, location.getFranchiseID());
		}
		fc.utobj().clickElement(driver, ui.Search);
		return this;

	}

	public void VerifyLocationExists(WebDriver driver, Location location) throws Exception {
		
		fc.utobj().printTestStep("Verify Location Exists");
		if(fc.utobj().assertLinkText(driver, location.getFranchiseID())==false) {
			fc.utobj().throwsException("Location not Found.");
			
		}
		

	
}
	public AdminFranchiseLocationManageFranchiseLocationsPageTest SearchEntity(WebDriver driver, Location location2) throws Exception {

		AdminFranchiseLocationManageFranchiseLocationsPageUI ui = new AdminFranchiseLocationManageFranchiseLocationsPageUI(
				driver);
		fc.utobj().printTestStep("Search Created Entity");

		if (location2.getEntityName() != null) {
			fc.utobj().selectValFromMultiSelect(driver, ui.Entity, location2.getEntityName());
		}
		fc.utobj().clickElement(driver, ui.Search);
		return this;

	}

	public void VerifyEntityExists(WebDriver driver, Location location2) throws Exception {
		
		fc.utobj().printTestStep("Verify Created Entity Exists");
		if(fc.utobj().assertPageSource(driver, location2.getEntityName())==false) {
			fc.utobj().throwsException("Created Entity not Found.");
			
		}
		
	}
	

	public AdminFranchiseLocationManageFranchiseLocationsPageTest SearchDeletedFranchiseByRegion(WebDriver driver, Location location) throws Exception {

		AdminFranchiseLocationManageFranchiseLocationsPageUI ui = new AdminFranchiseLocationManageFranchiseLocationsPageUI(
				driver);
		fc.utobj().printTestStep("Search Frachise");

		if (location.getFranchiseID() != null) {
			fc.utobj().selectValFromMultiSelect(driver, ui.AreaRegion, location.getAreaRegion());
		}
		fc.utobj().clickElement(driver, ui.Search);
		return this;

	}

	public void VerifyDeletedLocation(WebDriver driver, Location location) throws Exception {
		
		fc.utobj().printTestStep("Verify Location is Deleted");
		if(fc.utobj().assertNotInPageSource(driver, location.getFranchiseID())==true)
		{
			fc.utobj().throwsException("Location not Found.");
			
		}
}
}