package com.builds.test.admin;

import java.util.List;
import java.util.Map;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.builds.uimaps.admin.AdminHiddenLinksConfigureFieldOpsSettingsPage;
import com.builds.utilities.FranconnectUtil;

public class AdminHiddenLinksConfigureFieldOpsSettingsPageTest {
	FranconnectUtil fc = new FranconnectUtil();

	public void configureFieldOpsSettingsFranchiseSelfAuditYesOwnerScoringYes(WebDriver driver,
			Map<String, String> config) throws Exception {

		String testCaseId = "TC_configureFieldOpsSettingsFranchiseSelfAuditYesOwnerScoringYes_01";
		if (fc.utobj().configurationTestCaseToExecute(testCaseId)) {
			try {
				AdminHiddenLinksConfigureFieldOpsSettingsPage pobj = new AdminHiddenLinksConfigureFieldOpsSettingsPage(
						driver);
				fc.adminpage().adminHiddenLinksConfigureFieldOpsSettings(driver);
				List<WebElement> list = pobj.ownerScore;
				if (fc.utobj().isSelected(driver,list.get(0))) {
					// do nothing
				} else {
					fc.utobj().clickElement(driver, list.get(0));
				}
				List<WebElement> list1 = pobj.franCanCreateVisit;
				if (fc.utobj().isSelected(driver,list1.get(0))) {
					// do nothing
				} else {
					fc.utobj().clickElement(driver, list1.get(0));
				}
				fc.utobj().clickElement(driver, pobj.saveBtn);

				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {

		}
	}

	public void configureFieldOpsSettingsLogPrivateCommentYes(WebDriver driver, Map<String, String> config)
			throws Exception {

		String testCaseId = "TC_configureFieldOpsSettingsLogPrivateCommentYes_01";
		if (fc.utobj().configurationTestCaseToExecute(testCaseId)) {
			try {
				AdminHiddenLinksConfigureFieldOpsSettingsPage pobj = new AdminHiddenLinksConfigureFieldOpsSettingsPage(
						driver);

				fc.adminpage().adminHiddenLinksConfigureFieldOpsSettings(driver);

				if (!fc.utobj().isSelected(driver, pobj.privateCommentsYes)) {
					fc.utobj().clickElement(driver, pobj.privateCommentsYes);
				}

				fc.utobj().clickElement(driver, pobj.saveBtn);

				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {

		}
	}

	public void allowQuestionsNumberingInSectionWiseYes(WebDriver driver, Map<String, String> config) throws Exception {

		String testCaseId = "TC_allowQuestionsNumberingInSectionWiseYes_01";
		if (fc.utobj().configurationTestCaseToExecute(testCaseId)) {
			try {

				AdminHiddenLinksConfigureFieldOpsSettingsPage pobj = new AdminHiddenLinksConfigureFieldOpsSettingsPage(
						driver);

				fc.adminpage().adminHiddenLinksConfigureFieldOpsSettings(driver);

				List<WebElement> list = pobj.questionNumbering;
				if (fc.utobj().isSelected(driver,list.get(0))) {
					// do nothing
				} else {
					fc.utobj().clickElement(driver, list.get(0));
				}

				fc.utobj().clickElement(driver, pobj.saveBtn);

				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {

		}
	}

	public void allowQuestionsNumberingInSectionWiseNo(WebDriver driver, Map<String, String> config) throws Exception {

		String testCaseId = "TC_allowQuestionsNumberingInSectionWiseNo_01";
		if (fc.utobj().configurationTestCaseToExecute(testCaseId)) {
			try {
				AdminHiddenLinksConfigureFieldOpsSettingsPage pobj = new AdminHiddenLinksConfigureFieldOpsSettingsPage(
						driver);
				fc.adminpage().adminHiddenLinksConfigureFieldOpsSettings(driver);
				List<WebElement> list = pobj.questionNumbering;
				if (fc.utobj().isSelected(driver,list.get(1))) {
					// do nothing
				} else {
					fc.utobj().clickElement(driver, list.get(1));
				}
				fc.utobj().clickElement(driver, pobj.saveBtn);

				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {

		}
	}
}
