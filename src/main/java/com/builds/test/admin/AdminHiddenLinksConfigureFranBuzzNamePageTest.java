package com.builds.test.admin;

import java.util.Map;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Test;

import com.builds.uimaps.admin.AdminHiddenLinksConfigureFranBuzzNamePage;
import com.builds.utilities.FranconnectUtil;

public class AdminHiddenLinksConfigureFranBuzzNamePageTest {
	FranconnectUtil fc = new FranconnectUtil();

	@Test
	public void configureFranBuzzName(Map<String, String> config, WebDriver driver) throws Exception {

		String testCaseId = "TC_configureFranBuzzNameAdmin_01";
		if (fc.utobj().validate(testCaseId)) {
			try {
				driver = fc.loginpage().login();
				fc.adminpage().adminHiddenLinksConfigureFranBuzzName(driver);
				AdminHiddenLinksConfigureFranBuzzNamePage pobj = new AdminHiddenLinksConfigureFranBuzzNamePage(driver);
				fc.utobj().clickElement(driver, pobj.modifyBtn);
				fc.utobj().sendKeys(driver, pobj.franbuzzNameTextBox, "FranOnline");
				fc.utobj().clickElement(driver, pobj.SaveBtn);
				fc.utobj().clickElement(driver, pobj.backBtn);

				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException("was not able to configure FranBuzz Name");

		}
	}
}
