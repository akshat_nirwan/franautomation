package com.builds.test.admin;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.Reporter;

import com.builds.uimaps.admin.AdminHiddenLinksConfigureNewHierarchyLevelPage;
import com.builds.utilities.FranconnectUtil;

public class AdminHiddenLinksConfigureNewHierarchyLevelPageTest {
	FranconnectUtil fc = new FranconnectUtil();

	public void ConfigureNewHierarchyLevel(WebDriver driver) throws Exception {

		try {
			AdminHiddenLinksConfigureNewHierarchyLevelPage pobj = new AdminHiddenLinksConfigureNewHierarchyLevelPage(
					driver);
			
			fc.commonMethods().getModules().openAdminPage(driver);
			
			//Commented by vimal
			/*List<WebElement> divisionManagement = driver.findElements(By.xpath(".//a[@qat_adminlink='Division']"));
			List<WebElement> manageDivisionalUserLink = driver.findElements(By.xpath(".//a[@qat_adminlink='Divisional Users']"));
			List<WebElement> manageDivisionUserLink = driver.findElements(By.xpath(".//a[@qat_adminlink='Manage Division']"));
			
			if(divisionManagement.size()<1 || manageDivisionalUserLink.size() < 1 || manageDivisionUserLink.size()<1)
			{*/
			
				AdminPageTest p2 = new AdminPageTest();
				p2.openConfigureNewHierarchyLevel(driver);
				
				boolean isSelectedYes = false;

				if (!fc.utobj().isSelected(driver, pobj.isDivisionConfigureDisplayY)) {
					fc.utobj().clickElement(driver, pobj.isDivisionConfigureDisplayY);
					isSelectedYes = true;
				}

				fc.utobj().sendKeys(driver, pobj.divisionLabel, "Division");
				fc.utobj().sendKeys(driver, pobj.divisionUserLabel, "Divisional Users");
				fc.utobj().sendKeys(driver, pobj.divisionUserAbbr, "DU");

				try {
					if (!fc.utobj().isSelected(driver, pobj.lstEnableMultiBrandFDDRadiocheck)) {
						fc.utobj().clickElement(driver, pobj.lstEnableMultiBrandFDDRadiocheck);
					}
				} catch (Exception e) {
				}
				if (isSelectedYes) {
					fc.utobj().clickElement(driver, pobj.newUserOff);
				}
				fc.utobj().clickElement(driver, pobj.submitBtn);
			
			
		} catch (Exception e) {
			Reporter.log("Not able to configure new hierarchy level : "+e.getMessage().toString());
			fc.utobj().throwsException("Not able to configure new hierarchy level");
		}
	}

	public String configureAdditionalUserLevel(WebDriver driver, String configurationLabel,
			String configurationAbbreviation) throws Exception {

		AdminHiddenLinksConfigureNewHierarchyLevelPage pobj = new AdminHiddenLinksConfigureNewHierarchyLevelPage(
				driver);
		
		
		fc.commonMethods().getModules().openAdminPage(driver);
		List<WebElement> divisionManagement = driver.findElements(By.xpath(".//a[@qat_adminlink='Division']"));
		List<WebElement> manageDivisionalUserLink = driver.findElements(By.xpath(".//a[@qat_adminlink='Divisional Users']"));
		List<WebElement> manageDivisionUserLink = driver.findElements(By.xpath(".//a[@qat_adminlink='Manage Division']"));
		
		
		if(divisionManagement.size()<1 || manageDivisionalUserLink.size() < 1 || manageDivisionUserLink.size()<0)
		{
			AdminPageTest p2 = new AdminPageTest();
			p2.openConfigureNewHierarchyLevel(driver);
			
			if (!fc.utobj().isSelected(driver, pobj.isDivisionConfigureDisplayY)) {
				fc.utobj().clickElement(driver, pobj.isDivisionConfigureDisplayY);
			}

			fc.utobj().sendKeys(driver, pobj.divisionLabel, "Division");
			fc.utobj().sendKeys(driver, pobj.divisionUserLabel, "Divisional Users");
			fc.utobj().sendKeys(driver, pobj.divisionUserAbbr, "DU");

			try {
				if (!fc.utobj().isSelected(driver, pobj.lstEnableMultiBrandFDDRadiocheck)) {
					fc.utobj().clickElement(driver, pobj.lstEnableMultiBrandFDDRadiocheck);
				}
			} catch (Exception e) {
			}
		}else {
			AdminPageTest p2 = new AdminPageTest();
			p2.openConfigureNewHierarchyLevel(driver);
		}

		
		
		if (fc.utobj().isSelected(driver, pobj.newUserOff)) {
			fc.utobj().clickElement(driver, pobj.newUserOn);
			fc.utobj().sendKeys(driver, pobj.configurationLabelList.get(0), configurationLabel);
			fc.utobj().sendKeys(driver, pobj.configurationAbbrList.get(0), configurationAbbreviation);
			fc.utobj().clickElement(driver, pobj.submitBtn);
			
		} else if (fc.utobj().isSelected(driver, pobj.newUserOn)) {

			int temp=0;
			
			for (int i = 0; i < pobj.configurationLabelList.size(); i++) {
				
				String text=pobj.configurationLabelList.get(i).getAttribute("value");
				if (text!=null && !text.isEmpty()) {
					temp=temp+i;
				}else {
					break;
				}
			}
			
			
			fc.utobj().clickElement(driver, pobj.addNewUserLevelImg.get(pobj.addNewUserLevelImg.size()-1));
			
			List<WebElement> element=pobj.configurationLabelList;
			List<WebElement> elementAbbrList=pobj.configurationAbbrList;
			
			fc.utobj().sendKeys(driver, element.get(temp+1), configurationLabel);
			fc.utobj().sendKeys(driver, elementAbbrList.get(temp+1), configurationAbbreviation);
			fc.utobj().clickElement(driver, pobj.submitBtn);
		}
		return configurationLabel;
	}
}
