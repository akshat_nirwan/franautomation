package com.builds.test.admin;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Test;

import com.builds.test.infomgr.AdminInfoMgrCommonMethods;
import com.builds.uimaps.admin.AdminPage;
import com.builds.uimaps.common.FCHomePage;
import com.builds.utilities.FranconnectUtil;

public class AdminPageTest {
	FranconnectUtil fc = new FranconnectUtil();

	private AdminInfoMgrCommonMethods adminInfoMgrCommonMethods = null;

	public AdminInfoMgrCommonMethods adminInfoMgr() {
		if (adminInfoMgrCommonMethods == null) {
			adminInfoMgrCommonMethods = new AdminInfoMgrCommonMethods();
		}
		return adminInfoMgrCommonMethods;
	}

	public void marketing(WebDriver driver) throws Exception {
		fc.utobj().printTestStep("Go to Marketing");
		fc.home_page().openMarketingPage(driver);
	}

	@Test
	public void openCorporateUserPage(WebDriver driver) throws Exception {
		fc.utobj().printTestStep("Go to Admin > Users > Manage Corporate Users ");

		AdminPage pobj = new AdminPage(driver); // Opens Admin Page
		fc.utobj().clickElement(driver, pobj.corporateUserLnk);// clicks on
	}

	@Test
	public void openRegionalUserPage(WebDriver driver) throws Exception {
		fc.utobj().printTestStep("Go to Admin > Users > Manage Regional Users ");

		AdminPage pobj = new AdminPage(driver); // Opens Admin Page
		fc.utobj().clickElement(driver, pobj.regionalUserLnk); // clicks on
																// regional user
																// page
	}

	@Test
	public void openFranchiseUserPage(WebDriver driver) throws Exception {
		fc.utobj().printTestStep("Go to Admin > Users > Manage Franchise Users");

		AdminPage pobj = new AdminPage(driver); // Opens Admin Page
		fc.utobj().clickElement(driver, pobj.franchiseUserLnk); // clicks on
																// franchise
																// user page
	}

	@Test
	public void openRolesPage(WebDriver driver) throws Exception {
		fc.utobj().printTestStep("Go to Admin > Users > Roles");

		AdminPage pobj = new AdminPage(driver);
		fc.utobj().clickElement(driver, pobj.rolesLnk);
	}

	@Test
	public void openSuppliersPage(WebDriver driver) throws Exception {
		fc.utobj().printTestStep("Go to Admin > Users > Suppliers Details");

		AdminPage pobj = new AdminPage(driver);
		fc.utobj().clickElement(driver, pobj.suppliersLnk);
	}

	/*
	 * Admin > Franchise Sales Pages Starts
	 * *************************************************************************
	 * *********************************************************************
	 */

	@Test
	public void openFranchiseSalesStatusPage(WebDriver driver) throws Exception {
		fc.utobj().printTestStep("Go to Admin > Sales > Lead Status");

		AdminPage pobj = new AdminPage(driver);
		fc.utobj().clickElement(driver, pobj.statusLnk);
	}

	@Test
	public void openAdminSalesAssignLeadOwners(WebDriver driver) throws Exception {
		fc.utobj().printTestStep("Go to Admin > Sales > Assign Lead Owners");

		AdminPage pobj = new AdminPage(driver);
		fc.utobj().clickElement(driver, pobj.assignLeadOwners);
	}

	@Test
	public void openAssignLeadOwnerbySalesTerritories(WebDriver driver) throws Exception {
		openAdminSalesAssignLeadOwners(driver);
		fc.utobj().printTestStep("Go to Admin > Sales > Assign Lead Owners > Assign Lead Owner by Sales Territories");

		AdminPage pobj = new AdminPage(driver);
		fc.utobj().clickElement(driver, pobj.assignLeadOwnerbySalesTerritories);
		fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
				".//*[@id='siteMainTable']/tbody/tr/td/table/tbody/tr[2]/td[2]/table/tbody/tr[3]/td/input[1]"));
	}

	@Test
	public void openFranchiseSourcePage(WebDriver driver) throws Exception {
		fc.utobj().printTestStep("Go to Admin > Sales > Lead Source Category");

		AdminPage pobj = new AdminPage(driver);
		fc.utobj().clickElement(driver, pobj.sourceLnk);
	}

	@Test
	public void openConfigureLeadKilledReasonPage(WebDriver driver) throws Exception {
		fc.utobj().printTestStep("Go to Admin > Sales > Lead Killed Reason");

		AdminPage pobj = new AdminPage(driver);
		fc.utobj().clickElement(driver, pobj.configureLeadKilledReasonLnk);
	}

	@Test
	public void openConfigureCoApplicantRelationshipPage(WebDriver driver) throws Exception {
		fc.utobj().printTestStep("Go to Admin > Sales > Configure Co-Applicant Relationship ");

		AdminPage pobj = new AdminPage(driver);
		fc.utobj().clickElement(driver, pobj.configureCoApplicantRelationshipLnk);
	}

	@Test
	public void openFranchiseSalesManageWebFormGenerator(WebDriver driver) throws Exception {
		fc.utobj().printTestStep("Go to Admin > Sales > Manage Web Form Generator ");

		AdminPage pobj = new AdminPage(driver);
		fc.utobj().clickElement(driver, pobj.manageWebFormGenerator);
	}

	@Test
	public void openConfigureServiceListsPage(WebDriver driver) throws Exception {
		fc.utobj().printTestStep("Go to Admin > Sales > Configure Service Lists ");

		AdminPage pobj = new AdminPage(driver);
		fc.utobj().clickElement(driver, pobj.configureServiceListsLnk);
	}

	@Test
	public void openForecastRatingPage(WebDriver driver) throws Exception {
		fc.utobj().printTestStep("Go to Admin > Sales > Forecast Rating ");

		AdminPage pobj = new AdminPage(driver);
		fc.utobj().clickElement(driver, pobj.forecastRatingLnk);
	}

	public void openLeadRatingPage(WebDriver driver) throws Exception {
		fc.utobj().printTestStep("Go to Admin > Sales > Lead Rating");

		AdminPage pobj = new AdminPage(driver);
		fc.utobj().clickElement(driver, pobj.configureLeadRatingLnk);
	}

	public void openMarketingCodePage(WebDriver driver) throws Exception {
		fc.utobj().printTestStep("Go to Admin > Sales > Lead Marketing Code");

		AdminPage pobj = new AdminPage(driver);
		fc.utobj().clickElement(driver, pobj.marketingCodeLnk);
	}

	public void openBrokersTypeConfigurationPage(WebDriver driver) throws Exception {
		fc.utobj().printTestStep("Go to Admin > Sales > Broker Type Configuration");

		AdminPage pobj = new AdminPage(driver);
		fc.utobj().clickElement(driver, pobj.brokersTypeConfigurationLnk);
	}

	public void openBrokersAgencyPage(WebDriver driver) throws Exception {
		fc.utobj().printTestStep("Go to Admin > Sales > Broker Agency Summary");

		AdminPage pobj = new AdminPage(driver);
		fc.utobj().clickElement(driver, pobj.brokersAgencyLnk);
	}

	public void openConfigureOptOutStatusPage(WebDriver driver) throws Exception {
		fc.utobj().printTestStep("Go to Admin > Sales > Configure Opt-out status");

		AdminPage pobj = new AdminPage(driver);
		fc.utobj().clickElement(driver, pobj.configureOptOutStatus);
	}

	public void openConfigureQualificationChecklistsPage(WebDriver driver) throws Exception {
		fc.utobj().printTestStep("Go to Admin > Sales > Qualification Checklist");

		AdminPage pobj = new AdminPage(driver);
		fc.utobj().clickElement(driver, pobj.configureQualificationChecklists);
	}

	public void openSalesTerritoryPage(WebDriver driver) throws Exception {
		fc.utobj().printTestStep("Go to Admin > Sales > Manage Sales Territories ");

		AdminPage pobj = new AdminPage(driver);
		fc.utobj().clickElement(driver, pobj.salesTerritories);
	}

	public void openAssignLeadOwnersPage(WebDriver driver) throws Exception {
		fc.utobj().printTestStep("Go to Admin > Sales > Assign Lead Owners");

		AdminPage pobj = new AdminPage(driver);
		fc.utobj().clickElement(driver, pobj.assignLeadOwners);
	}

	public void openSetupFranchiseSalesLeadOwnersPage(WebDriver driver) throws Exception {
		fc.utobj().printTestStep("Go to Admin > Sales > Setup Sales Lead Owners");

		AdminPage pobj = new AdminPage(driver);
		fc.utobj().clickElement(driver, pobj.setupFranchiseSalesLeadOwners);
	}

	public void openDefineUnregisteredStatesProvincesPage(WebDriver driver) throws Exception {
		fc.utobj().printTestStep("Go to Admin > Sales > Unregistered States / Provinces");

		AdminPage pobj = new AdminPage(driver);
		fc.utobj().clickElement(driver, pobj.defineUnregisteredStatesProvinces);
	}

	/*
	 * public void openSetupEmailCampaignTriggersPage(WebDriver driver) throws
	 * Exception { fc.utobj().printTestStep(
	 * "Go to Setup Email Campaign Triggers Page");
	 * 
	 * AdminPage pobj = new AdminPage(driver);
	 * fc.utobj().clickElement(driver,pobj.setupEmailCampaignTriggers); }
	 */

	public void openAdminFinEnableDisableAreaFranchiseforEFTLnk(WebDriver driver) throws Exception {
		fc.utobj().printTestStep("Go to Admin > Finance > Enable / Disable Franchise(s) for EFT");

		AdminPage pobj = new AdminPage(driver);
		fc.utobj().clickElement(driver, pobj.enableDisableAreaFranchiseforEFTLnk);
	}

	public void openConfigureTaskEmailsPage(WebDriver driver) throws Exception {
		fc.utobj().printTestStep("Go to Admin > Configuration > Configure Task Emails ");
		AdminPage pobj = new AdminPage(driver);
		fc.adminpage().adminPage(driver);
		fc.utobj().clickElement(driver, pobj.configureTaskEmails);
	}

	/*
	 * public void openSetupAutomatedTasksPage(WebDriver driver) throws
	 * Exception { fc.utobj().printTestStep("Go to Setup Automated Tasks Page");
	 * 
	 * AdminPage pobj = new AdminPage(driver);
	 * fc.utobj().clickElement(driver,pobj.setupAutomatedTasks); }
	 */

	public void openConfigureDuplicateCriteriaPage(WebDriver driver) throws Exception {
		fc.utobj().printTestStep("Go to Admin > Sales > Configure Duplicate Criteria");

		AdminPage pobj = new AdminPage(driver);
		fc.utobj().clickElement(driver, pobj.configureDuplicateCriteria);
	}
	
	public void openConfigureSalesEmailParsingServer(WebDriver driver) throws Exception {
		fc.utobj().printTestStep("Go to Admin > Sales > Hidden Links > Configure Sales Email Parsing Server");
		
		AdminPage pobj = new AdminPage(driver);
		fc.utobj().clickElement(driver, pobj.configureSalesEmailParsingServer);
	}

	public void openManageFormGeneratorPage(WebDriver driver) throws Exception {
		fc.utobj().printTestStep("Go to Admin > Sales > Manage Web Form Generator ");
		AdminPage pobj = new AdminPage(driver);
		fc.utobj().clickElement(driver, pobj.manageFormGenerator);
	}

	public void openConfigureSearchCriteriaforTopSearchPage(WebDriver driver) throws Exception {
		fc.utobj().printTestStep("Go to Admin > Sales > Configure Search Criteria for API / Plugins ");

		AdminPage pobj = new AdminPage(driver);
		fc.utobj().clickElement(driver, pobj.configureSearchCriteriaforTopSearch);
	}

	public void openManageWebFormGeneratorPage(WebDriver driver) throws Exception {
		fc.utobj().printTestStep("Go to Admin > Sales > Manage Web Form Generator ");

		AdminPage pobj = new AdminPage(driver);
		fc.utobj().clickElement(driver, pobj.manageWebFormGenerator);
	}

	/*
	 * Intranet Pages
	 */

	public void openStoriesRSSFeedsandHomePageViewLnk(WebDriver driver) throws Exception {
		fc.utobj().printTestStep("Go to Admin > The Hub > Stories");

		AdminPage pobj = new AdminPage(driver);
		fc.utobj().clickElement(driver, pobj.storiesRSSFeedsandHomePageViewLnk);
	}

	public void openTheHubNewsLnk(WebDriver driver) throws Exception {
		fc.utobj().printTestStep("Go to Admin > The Hub > News");

		AdminPage pobj = new AdminPage(driver);
		fc.utobj().clickElement(driver, pobj.newsLnk);
	}

	public void openIntranetFolderLnk(WebDriver driver) throws Exception {
		fc.utobj().printTestStep("Go to Admin > The Hub > Library");

		AdminPage pobj = new AdminPage(driver);
		fc.utobj().clickElement(driver, pobj.libraryLnk);
	}

	public void openHubLibraryLogsLnk(WebDriver driver) throws Exception {
		fc.utobj().printTestStep("Go to Admin > The Hub > Library");

		AdminPage pobj = new AdminPage(driver);
		fc.utobj().clickElement(driver, pobj.libraryLogs);
	}

	public void openIntranetEPollLnk(WebDriver driver) throws Exception {
		fc.utobj().printTestStep("Go to Admin > The Hub > EPoll");

		AdminPage pobj = new AdminPage(driver);
		fc.utobj().clickElement(driver, pobj.ePollLnk);
	}

	public void openRelatedLinksLnk(WebDriver driver) throws Exception {
		fc.utobj().printTestStep("Go to Admin > The Hub > Related Links ");

		AdminPage pobj = new AdminPage(driver);
		fc.utobj().clickElement(driver, pobj.relatedLinksLnk);
	}

	public void openArchiveAlertsLnk(WebDriver driver) throws Exception {
		fc.utobj().printTestStep("Go to Admin > The Hub > Archive Alerts ");

		AdminPage pobj = new AdminPage(driver);
		fc.utobj().clickElement(driver, pobj.archiveAlertsLnk);
	}

	public void openArchiveMessagesLnk(WebDriver driver) throws Exception {
		fc.utobj().printTestStep("Go to Admin > The Hub > Archive Messages ");

		AdminPage pobj = new AdminPage(driver);
		fc.utobj().clickElement(driver, pobj.archiveMessagesLnk);
	}
	
	public void openConfigureServerLnk(WebDriver driver) throws Exception {
		fc.utobj().printTestStep("Go to Admin > The Hub > Configure Server ");
		AdminPage pobj = new AdminPage(driver);
		fc.utobj().clickElement(driver, pobj.hubConfigureServer);
	}

	public void openNewsLogsLnk(WebDriver driver) throws Exception {
		fc.utobj().printTestStep("Go to Admin > News Logs ");
		AdminPage pobj = new AdminPage(driver);
		fc.utobj().clickElement(driver, pobj.newsLogs);
	}

	public void openConfigureFranBuzzNameLnk(WebDriver driver) throws Exception {
		fc.utobj().printTestStep("Go to Admin > The Hub > Configure FranBuzz Name");
		AdminPage pobj = new AdminPage(driver);
		fc.utobj().clickElement(driver, pobj.configureFranBuzzNameLnk);
	}

	public void openConfigureSingleSignOnLnk(WebDriver driver) throws Exception {
		fc.utobj().printTestStep("Go to Admin > The Hub > Configure Single Sign On");
		AdminPage pobj = new AdminPage(driver);
		fc.utobj().clickElement(driver, pobj.configureSingleSignOn);
	}

	/*
	 * 
	 * admin > hidden links below
	 * 
	 */

	public void openConfigureFranBuzzName(WebDriver driver) throws Exception {
		fc.utobj().printTestStep("Go to Admin > The Hub > Configure FranBuzz Name");

		AdminPage pobj = new AdminPage(driver);
		fc.utobj().clickElement(driver, pobj.configureFranBuzzName);
	}

	public void openConfigureFieldOpsSettings(WebDriver driver) throws Exception {
		fc.utobj().printTestStep("Go to Admin > Hidden Links > Configure Field Ops Settings");

		AdminPage pobj = new AdminPage(driver);
		fc.utobj().clickElement(driver, pobj.configureFieldOpsSettings);
	}

	public void openLinkLeadWithExistingLeadorOwner(WebDriver driver) throws Exception {
		fc.utobj().printTestStep("Go to Admin > Hidden Links > Link Lead With Existing Lead / Owner ");

		AdminPage pobj = new AdminPage(driver);
		fc.utobj().clickElement(driver, pobj.linkLeadWithExisting);
	}

	public void openConfigureNewHierarchyLevel(WebDriver driver) throws Exception {
		fc.utobj().printTestStep("Go to Admin > Hidden Links > Configure New Hierarchy Level ");

		AdminPage pobj = new AdminPage(driver);
		fc.utobj().clickElement(driver, pobj.configureNewHierarchyLevel);
	}

	public void openConfigureProvenMatchIntegration(WebDriver driver) throws Exception {
		fc.utobj().printTestStep("Go to Admin > Hidden Links > Configure Proven Match Integration");

		AdminPage pobj = new AdminPage(driver);
		fc.utobj().clickElement(driver, pobj.configureProvenMatchIntegration);
	}

	public void openConfigureProvenMatchIntegrationDetails(WebDriver driver) throws Exception {
		fc.utobj().printTestStep("Go to Admin > Sales > Configure Proven Match Integration Details");

		AdminPage pobj = new AdminPage(driver);
		fc.utobj().clickElement(driver, pobj.ConfigureProvenMatchIntegrationDetails);
	}

	public void openConfigureNathanProfilerIntegration(WebDriver driver) throws Exception {
		fc.utobj().printTestStep("Go to Admin > Hidden Links > Configure Nathan Profiler Integration");

		AdminPage pobj = new AdminPage(driver);
		fc.utobj().clickElement(driver, pobj.configureProvenMatchIntegration);
	}

	public void openConfigureNathanProfilerIntegrationDetails(WebDriver driver) throws Exception {
		fc.utobj().printTestStep("Go to Admin > Sales > Configure Nathan Profiler Integration Details");

		AdminPage pobj = new AdminPage(driver);
		fc.utobj().clickElement(driver, pobj.ConfigureNathanProfilerIntegrationDetails);
	}

	/*
	 * Admin > Pwise
	 * 
	 */

	public void openManageVisitForm(WebDriver driver) throws Exception {
		fc.utobj().printTestStep("Go to Admin > Field Ops > Manage Visit Form ");

		AdminPage pobj = new AdminPage(driver);
		fc.utobj().clickElement(driver, pobj.manageVisitFormLnk);
	}

	public void openQuestionLibraryLnk(WebDriver driver) throws Exception {
		fc.utobj().printTestStep("Go to Admin > Field Ops > Question Library ");

		AdminPage pobj = new AdminPage(driver);
		fc.utobj().clickElement(driver, pobj.questionLibraryLnk);
	}

	public void actionLibraryLnk(WebDriver driver) throws Exception {
		fc.utobj().printTestStep("Go to Admin > Field Ops > Action Library ");

		AdminPage pobj = new AdminPage(driver);
		fc.utobj().clickElement(driver, pobj.actionLibraryLnk);
	}

	public void qaTabIntegrationLnk(WebDriver driver) throws Exception {
		fc.utobj().printTestStep("Go to Admin > Field Ops > QA History Tab Integration ");
		fc.adminpage().adminPage(driver);
		AdminPage pobj = new AdminPage(driver);
		fc.utobj().clickElement(driver, pobj.qaTabIntegrationLnk);
	}

	public void manageAuditorLnk(WebDriver driver) throws Exception {
		fc.utobj().printTestStep("Go to manage Auditor Lnk");

		AdminPage pobj = new AdminPage(driver);
		fc.utobj().clickElement(driver, pobj.manageAuditorLnk);
	}

	public void configureReminderAlertsForTasksLnk(WebDriver driver) throws Exception {
		fc.utobj().printTestStep("Go to configure Reminder Alerts For Tasks Lnk");

		AdminPage pobj = new AdminPage(driver);
		fc.utobj().clickElement(driver, pobj.configureReminderAlertsForTasksLnk);
	}

	public void configureScoreLnk(WebDriver driver) throws Exception {
		fc.utobj().printTestStep("Go to Admin > Field Ops > Configure Score");

		AdminPage pobj = new AdminPage(driver);
		fc.utobj().clickElement(driver, pobj.configureScoreLnk);
	}

	public void configureAlertEmailTriggersForTasksLnk(WebDriver driver) throws Exception {
		fc.utobj().printTestStep("Go to configure Alert Email Triggers For Tasks Lnk");

		AdminPage pobj = new AdminPage(driver);
		fc.utobj().clickElement(driver, pobj.configureAlertEmailTriggersForTasksLnk);
	}

	public void configureMediaLibraryLnk(WebDriver driver) throws Exception {
		fc.utobj().printTestStep("Go to Admin > Field Ops > Configure Media Library");

		AdminPage pobj = new AdminPage(driver);
		fc.utobj().clickElement(driver, pobj.configureMediaLibraryLnk);
	}

	public void openActionLibrary(WebDriver driver) throws Exception {
		fc.utobj().printTestStep("Go to Admin > Field Ops > Action Library ");

		AdminPage pobj = new AdminPage(driver);
		fc.utobj().clickElement(driver, pobj.actionLibraryLnk);
	}

	public void openManageConsultant(WebDriver driver) throws Exception {
		fc.utobj().printTestStep("Go to Admin > Field Ops > Manage Consultant ");

		AdminPage pobj = new AdminPage(driver);
		fc.utobj().clickElement(driver, pobj.manageConsultantLnk);
	}

	public void openConfigureScheduleVisitEmailContent(WebDriver driver) throws Exception {
		fc.utobj().printTestStep("Go to Admin > Field Ops > Configure Visit Schedule Email Content");

		AdminPage pobj = new AdminPage(driver);
		fc.utobj().clickElement(driver, pobj.ConfigureScheduleVisitEmailContentLnk);
	}

	public void openDeleteFieldOpsVisits(WebDriver driver) throws Exception {
		fc.utobj().printTestStep("Go to Admin > Field Ops > Delete Field Ops Visits");

		AdminPage pobj = new AdminPage(driver);
		fc.utobj().clickElement(driver, pobj.deleteFieldOpsVisitsLnk);
	}

	/*
	 * admin > support
	 */

	public void openManageFAQsLnk(WebDriver driver) throws Exception {
		fc.utobj().printTestStep("Go to Admin > Support > Manage FAQs");

		AdminPage pobj = new AdminPage(driver);
		fc.utobj().clickElement(driver, pobj.ManageFAQsLnk);
	}

	public void openManageDepartmentLnk(WebDriver driver) throws Exception {
		fc.utobj().printTestStep("Go to Admin > Support > Manage Department");

		AdminPage pobj = new AdminPage(driver);
		fc.utobj().clickElement(driver, pobj.manageDepartmentLnk);
	}

	public void openManageContactInformationLnk(WebDriver driver) throws Exception {
		fc.utobj().printTestStep("Go to Admin > Support > Manage Contact Information");

		AdminPage pobj = new AdminPage(driver);
		fc.utobj().clickElement(driver, pobj.manageContactInformationLnk);
	}

	public void openManageMessageTemplatesLnk(WebDriver driver) throws Exception {
		fc.utobj().printTestStep("Go to Admin > Support > Manage Message Templates");

		AdminPage pobj = new AdminPage(driver);
		fc.utobj().clickElement(driver, pobj.manageMessageTemplatesLnk);
	}

	public void openConfigureTicketStatusLnk(WebDriver driver) throws Exception {
		fc.utobj().printTestStep("Go to Admin > Support > Ticket Status ");

		AdminPage pobj = new AdminPage(driver);
		fc.utobj().clickElement(driver, pobj.configureTicketStatusLnk);
	}

	/*
	 * admin > franchise opener
	 */

	public void openTaskChecklistLnk(WebDriver driver) throws Exception {
		fc.utobj().printTestStep("Go to Admin > Opener > Task Checklist ");

		AdminPage pobj = new AdminPage(driver);
		fc.utobj().clickElement(driver, pobj.taskChecklistLnk);
	}

	public void openEquipmentChecklistLnk(WebDriver driver) throws Exception {
		fc.utobj().printTestStep("Go to Admin > Opener > Equipment Checklist ");

		AdminPage pobj = new AdminPage(driver);
		fc.utobj().clickElement(driver, pobj.equipmentChecklistLnk);
	}

	public void openDocumentChecklistLnk(WebDriver driver) throws Exception {
		fc.utobj().printTestStep("Go to Admin > Opener > Document Checklist ");

		AdminPage pobj = new AdminPage(driver);
		fc.utobj().clickElement(driver, pobj.documentChecklistLnk);
	}

	public void openPictureChecklistLnk(WebDriver driver) throws Exception {
		fc.utobj().printTestStep("Go to Admin > Opener > Picture Checklist");

		AdminPage pobj = new AdminPage(driver);
		fc.utobj().clickElement(driver, pobj.pictureChecklistLnk);
	}

	public void openSecondaryChecklistLnk(WebDriver driver) throws Exception {
		fc.utobj().printTestStep("Go to Admin > Opener > Secondary Checklists ");

		AdminPage pobj = new AdminPage(driver);
		fc.utobj().clickElement(driver, pobj.secondaryChecklistLnk);
	}

	public void openResponsibleDepartmentLnk(WebDriver driver) throws Exception {
		fc.utobj().printTestStep("Go to Admin > Opener > Responsible Department");

		AdminPage pobj = new AdminPage(driver);
		fc.utobj().clickElement(driver, pobj.responsibleDepartmentLnk);
	}

	public void openOverdueAlertFrequencyLnk(WebDriver driver) throws Exception {
		fc.utobj().printTestStep("Go to Admin > Opener > Overdue Alert Frequency ");

		AdminPage pobj = new AdminPage(driver);
		fc.utobj().clickElement(driver, pobj.overdueAlertFrequencyLnk);
	}

	public void openAlertEmailContentLnk(WebDriver driver) throws Exception {
		fc.utobj().printTestStep("Go to Admin > Opener > Add Alert Email Content ");

		AdminPage pobj = new AdminPage(driver);
		fc.utobj().clickElement(driver, pobj.alertEmailContentLnk);
	}

	public void openCustomizeProfilesLnk(WebDriver driver) throws Exception {
		fc.utobj().printTestStep("Go to Admin > Opener > Customize Profiles");

		AdminPage pobj = new AdminPage(driver);
		fc.utobj().clickElement(driver, pobj.customizeProfilesLnk);
	}

	public void openManageReferenceDatesLnk(WebDriver driver) throws Exception {
		fc.utobj().printTestStep("Go to Admin > Opener > Manage Reference Dates");

		AdminPage pobj = new AdminPage(driver);
		fc.utobj().clickElement(driver, pobj.manageReferenceDatesLnk);
	}

	public void openManageGroupsLnk(WebDriver driver) throws Exception {
		fc.utobj().printTestStep("Go to Admin > Opener > Manage Groups");

		AdminPage pobj = new AdminPage(driver);
		fc.utobj().clickElement(driver, pobj.manageGroupsLnk);
	}

	public void openConfigureProjectStatusLnk(WebDriver driver) throws Exception {
		fc.utobj().printTestStep("Go to Admin > Opener > Configure Project Status");

		AdminPage pobj = new AdminPage(driver);
		fc.utobj().clickElement(driver, pobj.configureProjectStatusLnk);
	}

	public void openConfigureChecklistDisplaySettingLnk(WebDriver driver) throws Exception {
		fc.utobj().printTestStep("Go to Admin > Opener > Configure Checklist Display Setting");

		AdminPage pobj = new AdminPage(driver);
		fc.utobj().clickElement(driver, pobj.configureChecklistDisplaySettings);
	}

	public void openConfigureAlertEmailTriggersTasksLnk(WebDriver driver) throws Exception {
		fc.utobj().printTestStep("Go to Admin > Opener > Configure Alert Email Triggers for Tasks");

		AdminPage pobj = new AdminPage(driver);
		fc.utobj().clickElement(driver, pobj.configureAlertEmailTriggersTask);

	}

	public void openConfigureMilestoneDateTriggersLnk(WebDriver driver) throws Exception {
		fc.utobj().printTestStep("Go to Admin > Opener > Milestone Date Triggers ");

		AdminPage pobj = new AdminPage(driver);
		fc.utobj().clickElement(driver, pobj.ConfigureMilestoneDateTriggers);

	}

	/*
	 * admin > franchise location
	 */

	public void openAddNewFranchiseLocation(WebDriver driver) throws Exception {
		fc.utobj().printTestStep("Go to Admin > Franchise Location > Add Franchise Location ");

		AdminPage pobj = new AdminPage(driver);
		fc.utobj().clickElement(driver, pobj.addNewFranchiseLocationLnk);
	}

	public void openManageFranchiseLocation(WebDriver driver) throws Exception {
		fc.utobj().printTestStep("Go to Admin > Franchise Location > Manage Franchise Locations ");

		AdminPage pobj = new AdminPage(driver);
		fc.utobj().clickElement(driver, pobj.manageFranchiseLocationsLnk);
	}
	
	/*
	 * Admin > Hidden Links > Delete Franchises Location
	 */

	public AdminFranchiseLocationManageFranchiseLocationsPageTest openDeleteFranchiseeLocation(WebDriver driver) throws Exception {
		fc.utobj().printTestStep("Go to Admin > Hidden Links> Delete Franchises Location");
		fc.adminpage().adminPage(driver);
		AdminPage pobj = new AdminPage(driver);
		fc.utobj().clickElement(driver, pobj.deleteFranchiseeLocationLnk);
		return new AdminFranchiseLocationManageFranchiseLocationsPageTest();
		
	}
	
	// Admin > Manage Area / Region
	
	public void openManageAreaRegion(WebDriver driver) throws Exception {
		fc.utobj().printTestStep("Go to Admin > Manage Area / Region");

		AdminPage pobj = new AdminPage(driver);

		fc.utobj().clickElement(driver, pobj.manageAreaRegionLnk);
	}

	/*
	 * admin > Area / Region / Add Area / Region
	 */

	public void openAddNewAreaRegion(WebDriver driver) throws Exception {
		fc.utobj().printTestStep("Go to Admin > Area / Region > Add Area / Region");

		AdminPage pobj = new AdminPage(driver);

		fc.utobj().clickElement(driver, pobj.addNewAreaRegionLnk);
	}

	/*
	 * admin > Division_US Management / Manage Division_US / Add Division_US
	 */
	public void openManageDivision_US(WebDriver driver) throws Exception {
		fc.utobj().printTestStep("Go to Admin > Division > Manage Division ");

		AdminPage pobj = new AdminPage(driver);
		fc.utobj().clickElement(driver, pobj.manageDivisionLnk);
	}

	public void openAddDivision(WebDriver driver) throws Exception {
		fc.utobj().printTestStep("Go to Admin > Division > Add Division ");

		AdminPage pobj = new AdminPage(driver);

		fc.utobj().clickElement(driver, pobj.addDivisionLnk);
	}

	/*
	 * Click over divisional User
	 */

	public void openDivisionalUserLnk(WebDriver driver) throws Exception {
		fc.utobj().printTestStep("Go to  Admin > Users > Manage Divisional Users ");

		AdminPage pobj = new AdminPage(driver);
		fc.utobj().clickElement(driver, pobj.divisionalUser);

		fc.utobj().clickElement(driver, pobj.addDivisionalUserLnk);
	}

	public void openadminUsersManageDivisionalUsersLnk(WebDriver driver) throws Exception {
		fc.utobj().printTestStep("Go to  Admin > Users > Manage Divisional Users ");
		AdminPage pobj = new AdminPage(driver);
		adminPage(driver);
		fc.utobj().clickElement(driver, pobj.divisionalUser);
	}

	/*
	 * admin > Training
	 */

	public void openCourseManagementLnk(WebDriver driver) throws Exception {
		fc.utobj().printTestStep("Go to Admin > Training > Course Management");

		AdminPage pobj = new AdminPage(driver);
		fc.utobj().clickElement(driver, pobj.courseManagementLnk);
	}

	public void openPlanandCertificateLnk(WebDriver driver) throws Exception {
		fc.utobj().printTestStep("Go to Admin > Training > Plans & Certificates");

		AdminPage pobj = new AdminPage(driver);
		fc.utobj().clickElement(driver, pobj.PlanAndCertificatesLnk);
	}

	public void openQuestionLibraryTrainingLnk(WebDriver driver) throws Exception {
		fc.utobj().printTestStep("Go to Admin > Training > Question Library");

		AdminPage pobj = new AdminPage(driver);
		fc.utobj().clickElement(driver, pobj.questionLibraryTrainingLnk);
	}

	public void openConfigureServerTrainginLnk(WebDriver driver) throws Exception {
		fc.utobj().printTestStep("Go to Admin > Training > Configure Server");

		AdminPage pobj = new AdminPage(driver);
		fc.utobj().clickElement(driver, pobj.configureServerTrainingLnk);
	}

	public void openConfigureInviteEmail(WebDriver driver) throws Exception {
		fc.utobj().printTestStep("Go to Admin > Training > Configure Invite Email");

		AdminPage pobj = new AdminPage(driver);
		fc.utobj().clickElement(driver, pobj.configureInviteEmailLnk);
	}

	/*
	 * admin > fim
	 */

	public void openAdminFimTriggersAndAuditingPage(WebDriver driver) throws Exception {
		fc.utobj().printTestStep("Go to Admin > Info Mgr > Triggers and Auditing");

		AdminPage pobj = new AdminPage(driver);
		fc.utobj().clickElement(driver, pobj.triggersAndAuditingLnk);
	}

	public void openAdminFimConfigureOwnershipTransferStatusPage(WebDriver driver) throws Exception {
		fc.utobj().printTestStep("Go to Admin > Info Mgr > Configure Ownership Transfer Status ");

		AdminPage pobj = new AdminPage(driver);
		fc.utobj().clickElement(driver, pobj.configureOwnershipTransferStatusLnk);
	}

	public void openAdminFimConfigureOtherAddressHeadingPage(WebDriver driver) throws Exception {
		fc.utobj().printTestStep("Go to Admin > Info Mgr > Configure Other Address Heading ");

		AdminPage pobj = new AdminPage(driver);
		fc.utobj().clickElement(driver, pobj.configureOtherAddressesHeadingLnk);
	}

	@Test
	public void openInfoMgrManageFormGeneratorPage(WebDriver driver) throws Exception {
		fc.utobj().printTestStep("Go to Admin > Info Mgr > Manage Form Generator");

		AdminPage pobj = new AdminPage(driver);
		fc.utobj().clickElement(driver, pobj.infoMgrManageWebFormGeneratorLink);
	}
	/*
	 * admin > configuration
	 */

	public void openAdminConfigurationConfigureStoreType(WebDriver driver) throws Exception {
		fc.utobj().printTestStep("Go to Admin > Configuration > Configure Store Type ");

		AdminPage pobj = new AdminPage(driver);
		// pobj.configureStoreTypeLnk.click(driver);
		fc.utobj().clickElement(driver, pobj.configureStoreTypeLnk);
	}

	public void openAdminConfigurationConfigureTaskType(WebDriver driver) throws Exception {
		fc.utobj().printTestStep("Go to Admin > Configuration > Configure Task Type");

		AdminPage pobj = new AdminPage(driver);
		fc.utobj().clickElement(driver, pobj.configureTaskType);
	}

	public void openAdminConfigurationConfigureOffensiveWatchGreatKeywords(WebDriver driver) throws Exception {
		fc.utobj().printTestStep(
				"Go to Admin > Configuration > Configure Offensive Watch Great Keywords for Forum and FranBuzz");
		AdminPage pobj = new AdminPage(driver);
		fc.utobj().clickElement(driver, pobj.configureOffensiveWord);
	}

	public void openAdminConfigurationConfigureDictionary(WebDriver driver) throws Exception {
		fc.utobj().printTestStep("Go to Admin > Configuration > Configure Dictionary");

		AdminPage pobj = new AdminPage(driver);
		fc.utobj().clickElement(driver, pobj.configureDictionaryLnk);
	}

	public void openAdminadminConfigureQuickLinks(WebDriver driver) throws Exception {
		fc.utobj().printTestStep("Go to Admin > Configuration > Configure Quick Links");

		AdminPage pobj = new AdminPage(driver);
		fc.utobj().clickElement(driver, pobj.configureQuickLnk);
	}

	public void openAdminConfigurationPIIConfiguration(WebDriver driver) throws Exception {
		fc.utobj().printTestStep("Go to Admin  > Configuration > PII Configuration");

		AdminPage pobj = new AdminPage(driver);
		fc.utobj().clickElement(driver, pobj.piiConfigurationLnk);
	}

	public void openAdminConfigurationDecimalPlace(WebDriver driver) throws Exception {
		fc.utobj().printTestStep("Go to Admin > Configuration > Configure Decimal Place");

		AdminPage pobj = new AdminPage(driver);
		fc.utobj().clickElement(driver, pobj.configureDecimalPlacesLnk);
	}

	public void openAdminConfigurationConfigureCalendarStartDay(WebDriver driver) throws Exception {
		fc.utobj().printTestStep("Go to Admin > Configuration > Configure Calendar Start Day");

		AdminPage pobj = new AdminPage(driver);
		fc.utobj().clickElement(driver, pobj.configureCalendarStartDayLnk);
	}

	public void openAdminConfigureCalendarEventCategories(WebDriver driver) throws Exception {
		fc.utobj().printTestStep("Go to Admin > Configuration > Configure Calender Event Categories");

		AdminPage pobj = new AdminPage(driver);
		fc.utobj().clickElement(driver, pobj.configureCalendarEventCategoriesLnk);
	}

	public void openAdminConfigurationConfigureTaskDisplayOnCalendar(WebDriver driver) throws Exception {
		fc.utobj().printTestStep("Go to Admin > Configuration > Configure Task Display On Calendar");

		AdminPage pobj = new AdminPage(driver);
		fc.utobj().clickElement(driver, pobj.configureTaskDisplayOnCalendarLnk);
	}

	public void openAdminConfigureCallStatus(WebDriver driver) throws Exception {
		fc.utobj().printTestStep("Go to Admin > Configuration > Configure Call Status ");

		AdminPage pobj = new AdminPage(driver);
		fc.utobj().clickElement(driver, pobj.configureCallStatusLnk);
	}

	public void openAdminConfigurationCommunicationType(WebDriver driver) throws Exception {
		fc.utobj().printTestStep("Go to Admin > Configuration > Configure Communication Type ");

		AdminPage pobj = new AdminPage(driver);
		fc.utobj().clickElement(driver, pobj.configureCommunicationTypeLnk);
	}

	public void openAdminConfigureSiteClearance(WebDriver driver) throws Exception {
		fc.utobj().printTestStep("Go to Admin > Hidden Links > Configure Site Clearance ");

		AdminPage pobj = new AdminPage(driver);
		fc.utobj().clickElement(driver, pobj.configureSiteClearance);
	}

	public void openConfigurePasswordSettings(WebDriver driver) throws Exception {
		fc.utobj().printTestStep("Go to Admin > Configuration > Configure Password Settings ");

		AdminPage pobj = new AdminPage(driver);
		fc.utobj().clickElement(driver, pobj.ConfigurePasswordSettings);

	}

	/*
	 * 
	 */

	public void openFddManagementLnk(WebDriver driver) throws Exception {
		fc.utobj().printTestStep("Go to Admin > FDD Management > FDD Management");

		AdminPage pobj = new AdminPage(driver);
		fc.utobj().clickElement(driver, pobj.fddManagementLnk);
	}

	public void openFddEmailTemplateSummaryLnk(WebDriver driver) throws Exception {
		fc.utobj().printTestStep("Admin > FDD Management > FDD Email Template summary");

		AdminPage pobj = new AdminPage(driver);
		fc.utobj().clickElement(driver, pobj.fddEmailTemplateSummaryLnk);
	}

	public void openlogOnCredentialsDurationLnk(WebDriver driver) throws Exception {
		fc.utobj().printTestStep("Go to Admin > FDD Management > Log on credentials duration");

		AdminPage pobj = new AdminPage(driver);
		fc.utobj().clickElement(driver, pobj.logOnCredentialsDurationLnk);
	}

	public void openItem23ReceiptSummaryLnk(WebDriver driver) throws Exception {
		fc.utobj().printTestStep("Go to Admin > FDD Management > ITEM 23 - RECEIPT Summary");

		AdminPage pobj = new AdminPage(driver);
		fc.utobj().clickElement(driver, pobj.item23ReceiptSummaryLnk);
	}

	public void openConfigureEmailSentPriorToFDDEmailLnk(WebDriver driver) throws Exception {
		fc.utobj().printTestStep("Go to Admin > FDD Management > Configure Email sent Prior to FDD Email ");

		AdminPage pobj = new AdminPage(driver);
		fc.utobj().clickElement(driver, pobj.configureEmailSentPriorToFDDEmailLnk);
	}

	public void openConfigureEmailsentPriortoFDDExpirationLnk(WebDriver driver) throws Exception {
		fc.utobj().printTestStep("Go to Admin > FDD Management > Configure Email sent Prior to FDD Expiration ");

		AdminPage pobj = new AdminPage(driver);
		fc.utobj().clickElement(driver, pobj.configureEmailsentPriortoFDDExpirationLnk);
	}

	/*
	 * Access Control Access Control
	 */

	public void openDeletedLogsLnk(WebDriver driver) throws Exception {
		fc.utobj().printTestStep("Go to Admin > Access Control > Deleted Logs ");

		fc.adminpage().adminPage(driver);
		AdminPage pobj = new AdminPage(driver);
		fc.utobj().clickElement(driver, pobj.deletedLogsLnk);
	}

	public void openLoginLogs(WebDriver driver) throws Exception {
		fc.utobj().printTestStep("Go to Admin > Access Control > Login Logs > System Details ");
		AdminPage pobj = new AdminPage(driver);
		fc.adminpage().adminPage(driver);
		fc.utobj().clickElement(driver, pobj.loginLogsLnk);
	}
	
	public void openSearchedLogs(WebDriver driver) throws Exception {
		fc.utobj().printTestStep("Go to Admin > Access Control > Searched Logs");
		AdminPage pobj = new AdminPage(driver);
		fc.utobj().clickElement(driver, pobj.hubSearchedLogsLnk);
	}

	public void userAccountSummary(WebDriver driver) throws Exception {
		fc.utobj().printTestStep("Go to Admin > Access Control > User Account Summary ");
		fc.adminpage().adminPage(driver);
		AdminPage pobj = new AdminPage(driver);
		fc.utobj().clickElement(driver, pobj.userAccountSummary);
	}

	/*
	 * admin > FIN
	 */

	public void openAdminFinAgreementVersionsPage(WebDriver driver) throws Exception {
		fc.utobj().printTestStep("Go to Admin > Finance > Agreement Versions");
		fc.adminpage().adminPage(driver);
		AdminPage pobj = new AdminPage(driver);
		fc.utobj().clickElement(driver, pobj.agreementVersionsLnk);
	}

	public void openAdminFinConfigureAdditionalInvoiceItemPage(WebDriver driver) throws Exception {
		fc.utobj().printTestStep("Go to Admin > Finance > Additional Invoice Item(s) Summary");
		fc.adminpage().adminPage(driver);
		AdminPage pobj = new AdminPage(driver);
		fc.utobj().clickElement(driver, pobj.configureAdditionalInvoiceItemLnk);
	}

	public void openAdminFinConfigureHeadingsforSalesPage(WebDriver driver) throws Exception {
		fc.utobj().printTestStep("Go to Admin > Finance > Configure Headings for Sales Page");
		fc.adminpage().adminPage(driver);
		AdminPage pobj = new AdminPage(driver);
		fc.utobj().clickElement(driver, pobj.configureHeadingsforSalesLnk);
	}
	
	public void openAdminFinConfigureCategoriesforSalesReportPage(WebDriver driver) throws Exception {
		fc.utobj().printTestStep("Go to Admin > Finance > Configure Categories for Sales Report");
		fc.adminpage().adminPage(driver);
		AdminPage pobj = new AdminPage(driver);
		fc.utobj().clickElement(driver, pobj.configureCategoriesforSalesReportLnk);
	}

	public void openAdminFinConfigureNonFinancialKPICategoriesforSalesReportLnk(WebDriver driver) throws Exception {
		fc.utobj().printTestStep("Go to Admin > Finance > Non-Financial / KPI Categories Summary");
		fc.adminpage().adminPage(driver);
		AdminPage pobj = new AdminPage(driver);
		fc.utobj().clickElement(driver, pobj.configureNonFinancialKPICategoriesforSalesReportLnk);
	}

	public void openAdminFinConfigureFinancialsDocumenttobeUploadedLnk(WebDriver driver) throws Exception {
		fc.utobj().printTestStep("Go to Admin Admin > Finance > Configure Finance Document(s) to be Uploaded");
		fc.adminpage().adminPage(driver);
		AdminPage pobj = new AdminPage(driver);
		fc.utobj().clickElement(driver, pobj.configureFinancialsDocumenttobeUploadedLnk);
	}

	public void openAdminFinConfigureProfitLossCategoriesLnk(WebDriver driver) throws Exception {
		fc.utobj().printTestStep("Go to Admin > Finance > Configure Profit & Loss Categories");
		fc.adminpage().adminPage(driver);
		AdminPage pobj = new AdminPage(driver);
		fc.utobj().clickElement(driver, pobj.configureProfitLossCategoriesLnk);
	}

	public void openAdminFinEnableDisableFranchiseforEFTLnk(WebDriver driver) throws Exception {
		fc.utobj().printTestStep("Go to Admin > Finance > Enable / Disable Area Franchise(s) for EFT");
		fc.adminpage().adminPage(driver);
		AdminPage pobj = new AdminPage(driver);
		fc.utobj().clickElement(driver, pobj.enableDisableFranchiseforEFTLnk);
	}

	public void openAdminFinDeleteSalesReportsLnk(WebDriver driver) throws Exception {
		fc.utobj().printTestStep("Go to  Admin > Finance > Delete Sales Reports  > Sales");
		fc.adminpage().adminPage(driver);
		AdminPage pobj = new AdminPage(driver);
		fc.utobj().clickElement(driver, pobj.deleteSalesReport);
	}

	public void openAdminFinConfigureTaxRatesLnk(WebDriver driver) throws Exception {
		fc.utobj().printTestStep("Go to Admin > Hidden Links > Configure Tax Rates");
		fc.adminpage().adminPage(driver);
		AdminPage pobj = new AdminPage(driver);
		fc.utobj().clickElement(driver, pobj.configureTaxRates);
	}

	public void openAdminCRMContactTypeConfigurationLnk(WebDriver driver) throws Exception {
		fc.utobj().printTestStep("Go to Admin > CRM > Contact Type Configuration ");

		AdminPage pobj = new AdminPage(driver);
		fc.utobj().clickElement(driver, pobj.contactTypeConfiguration);
	}

	public void openAdminCRMConfigureStatusLnk(WebDriver driver) throws Exception {
		fc.utobj().printTestStep("Go to Admin > CRM > Configure Status");

		AdminPage pobj = new AdminPage(driver);
		fc.utobj().clickElement(driver, pobj.configureStatus);
	}

	public void openAdminCRMConfigureMediumLnk(WebDriver driver) throws Exception {
		fc.utobj().printTestStep("Go to Admin > CRM > Configure Medium");

		AdminPage pobj = new AdminPage(driver);
		fc.utobj().clickElement(driver, pobj.configureMedium);
	}

	public void openAdminCRMSourceSummaryLnk(WebDriver driver) throws Exception {
		fc.utobj().printTestStep("Go to Admin > CRM > Source Summary");

		AdminPage pobj = new AdminPage(driver);
		fc.utobj().clickElement(driver, pobj.contactSource);
	}

	public void openAdminCRMConfigureLeadTypeLnk(WebDriver driver) throws Exception {
		fc.utobj().printTestStep("Go to Admin > CRM > Configure Lead Type");

		AdminPage pobj = new AdminPage(driver);
		fc.utobj().clickElement(driver, pobj.configureLeadType);
	}

	public void openAdminCRMConfigureIndustryLnk(WebDriver driver) throws Exception {
		fc.utobj().printTestStep("Go to Admin > CRM > Configure Industry");

		AdminPage pobj = new AdminPage(driver);
		fc.utobj().clickElement(driver, pobj.configureIndustry);
	}

	public void openAdminCRMConfigureOpportunityStagesLnk(WebDriver driver) throws Exception {
		fc.utobj().printTestStep("Go to Admin > CRM> Configure Opportunity Stages");

		AdminPage pobj = new AdminPage(driver);
		fc.utobj().clickElement(driver, pobj.configureOpportunityStages);
	}

	public void openAdminCRMConfigureEmailContentLnk(WebDriver driver) throws Exception {
		fc.utobj().printTestStep("Go to Admin Sales Configure Email Content Lnk");

		AdminPage pobj = new AdminPage(driver);
		fc.utobj().clickElement(driver, pobj.configureEmailContent);
	}

	public void openAdminSalesConfigureEmailContentSenttoLeadOwner(WebDriver driver) throws Exception {
		fc.utobj().printTestStep("Go to Admin CRM Configure Email ContentLnk");

		AdminPage pobj = new AdminPage(driver);
		fc.adminpage().adminPage(driver);
		fc.utobj().clickElement(driver, pobj.salesConfigureEmailContent);
	}

	public void openAdminCRMConfigureEmailContentSenttoContactOwner(WebDriver driver) throws Exception {
		fc.utobj().printTestStep("Go to Admin CRM Configure Email ContentLnk");

		AdminPage pobj = new AdminPage(driver);
		fc.adminpage().adminPage(driver);
		fc.utobj().clickElement(driver, pobj.configureEmailContentcrm);
	}

	public void openAdminCRMConfigureCampaignEmailCategoryLnk(WebDriver driver) throws Exception {
		fc.utobj().printTestStep("Go to Admin > CRM > Configure Campaign Email Category");

		AdminPage pobj = new AdminPage(driver);
		fc.utobj().clickElement(driver, pobj.configureCampaignEmailCategory);
	}

	public void openAdminCRMManageProductServiceCategoryLnk(WebDriver driver) throws Exception {
		fc.utobj().printTestStep("Go to Admin > CRM > Manage Product / Service & Category > Manage Product / Service");

		AdminPage pobj = new AdminPage(driver);
		fc.utobj().clickElement(driver, pobj.manageProductServiceCategory);
	}

	public void openAdminCRMManageWebFormGeneratorLnk(WebDriver driver) throws Exception {
		fc.utobj().printTestStep("Go to Admin > CRM > Manage Web Form Generator");

		AdminPage pobj = new AdminPage(driver);
		fc.utobj().clickElement(driver, pobj.manageWebFormGeneratorLink);
	}

	// ***************************************************************************//

	public void adminUsersRolesPage(WebDriver driver) throws Exception {
		fc.utobj().printTestStep("Go to Admin > Users > Roles");
		// Admin > Users > Roles
		fc.adminpage().adminPage(driver);
		AdminPageTest p2 = new AdminPageTest();
		p2.openRolesPage(driver);
	}

	public void adminPage(WebDriver driver) throws Exception {
		fc.utobj().printTestStep("Go to Admin Section");
		fc.home_page().openAdminPage(driver);
	}

	public void adminUsersRolesAddNewRolePage(WebDriver driver) throws Exception {
		fc.utobj().printTestStep("Go to Admin > Users > Roles >  Add New Role");
		// Admin > Users > Roles > Add New Role
		adminUsersRolesPage(driver);
		AdminUsersRolesPageTest p1 = new AdminUsersRolesPageTest();
		p1.openAddRolesPage(driver);
	}

	public void adminUsersManageCorporateUsersPage(WebDriver driver) throws Exception {
		fc.utobj().printTestStep("Go to admin Users Manage Corporate Users Page");
		// Admin > Users > Manage Corporate Users
		adminPage(driver);
		AdminPageTest p2 = new AdminPageTest();
		p2.openCorporateUserPage(driver);
	}

	public void adminUsersManageCorporateUsersAddCorporateUserPage(WebDriver driver) throws Exception {
		fc.utobj().printTestStep("Go to Admin > Users > Manage Corporate Users > Add  Corporate User");
		// Admin > Users > Manage Corporate Users > Add Corporate User
		adminUsersManageCorporateUsersPage(driver);
		Thread.sleep(2000);
		AdminUsersManageCorporateUsersPageTest p1 = new AdminUsersManageCorporateUsersPageTest();
		p1.addCorporateUsers(driver);
	}

	public void adminUsersManageRegionalUsersPage(WebDriver driver) throws Exception {
		fc.utobj().printTestStep("Go to admin Users Admin > Users > Manage Regional Users");
		adminPage(driver);
		AdminPageTest p2 = new AdminPageTest();
		p2.openRegionalUserPage(driver);
	}

	public void adminUsersManageRegionalUsersAddRegionalUserPage(WebDriver driver) throws Exception {
		fc.utobj().printTestStep("Go to Admin > Users > Manage Regional Users > Add Regional User");
		adminUsersManageRegionalUsersPage(driver);
		AdminUsersManageRegionalUsersPageTest p1 = new AdminUsersManageRegionalUsersPageTest();
		p1.openAddRegionalUser(driver);
	}

	public void adminUsersManageFranchiseUsersPage(WebDriver driver) throws Exception {
		fc.utobj().printTestStep("Go to Admin > Users > Manage Franchise Users");
		adminPage(driver);
		AdminPageTest p2 = new AdminPageTest();
		p2.openFranchiseUserPage(driver);
	}

	public void adminAccessControlLoginLogs(WebDriver driver) throws Exception {
		fc.utobj().printTestStep("Go to Admin > Access Control > Login Logs");
		adminPage(driver);
		AdminPageTest p2 = new AdminPageTest();
		p2.openLoginLogs(driver);
	}
	
	public void adminAccessControlSearchedLogs(WebDriver driver) throws Exception {
		fc.utobj().printTestStep("Go to Admin > Access Control > The Hub Searched Logs");
		adminPage(driver);
		AdminPageTest p2 = new AdminPageTest();
		p2.openSearchedLogs(driver);
	}

	public void adminAreaRegionAddAreaRegionPage(WebDriver driver) throws Exception {
		fc.utobj().printTestStep("Go to Admin > Area / Region > Add Area / Region");
		adminPage(driver);
		AdminPageTest p2 = new AdminPageTest();
		p2.openAddNewAreaRegion(driver);
	}

	public void adminDivision_USManageDivision_USPage(WebDriver driver) throws Exception {
		fc.utobj().printTestStep("Go to Admin > Division > Manage Division");
		adminPage(driver);
		AdminPageTest p2 = new AdminPageTest();
		p2.openManageDivision_US(driver);
	}

	public void adminDivisionAddDivision(WebDriver driver) throws Exception {
		fc.utobj().printTestStep("Go to Admin > Division");
		adminPage(driver);
		AdminPageTest p2 = new AdminPageTest();
		p2.openAddDivision(driver);
	}

	public void adminUserDivisionalUserAddDivisionalUser(WebDriver driver) throws Exception {
		fc.utobj().printTestStep("Go to admin User Divisional User Add Divisional User");
		adminPage(driver);
		AdminPageTest p2 = new AdminPageTest();
		p2.openDivisionalUserLnk(driver);
	}

	public void adminUsersSupplierDetailsAddSupplier(WebDriver driver) throws Exception {
		fc.utobj().printTestStep("Go to admin Users Supplier Details Add Supplier");
		// Admin > Users > Supplier Details > Add Supplier
		adminPage(driver);
		AdminPageTest p2 = new AdminPageTest();
		p2.openSuppliersPage(driver);
		AdminUsersSuppliersDetailsPageTest p1 = new AdminUsersSuppliersDetailsPageTest();
		p1.addNewSupplier(driver);
	}

	public void configureEmailsentPriortoFDDExpirationLnk(WebDriver driver) throws Exception {
		fc.utobj().printTestStep("Go to configure Email sent Prior to FDD Expiration Lnk");
		fc.adminpage().adminPage(driver);
		AdminPageTest p2 = new AdminPageTest();
		p2.openConfigureEmailsentPriortoFDDExpirationLnk(driver);
	}

	public void openSmartConnectModulePage(WebDriver driver) throws Exception {
		fc.utobj().printTestStep("Go to SmartConnectModule");
		//fc.utobj().getElementByXpath(driver, ".//*[@id='test1']/span/a/span[contains(text(),'More')]/span").click();
		fc.home_page().openSmartConnectModule(driver);

	}

	public void configureEmailSentPriorToFDDEmailLnk(WebDriver driver) throws Exception {
		fc.utobj().printTestStep("Go to configureEmailSentPriorToFDDEmailLnk");
		fc.adminpage().adminPage(driver);
		AdminPageTest p2 = new AdminPageTest();
		p2.openConfigureEmailSentPriorToFDDEmailLnk(driver);
	}

	public void item23ReceiptSummaryLnk(WebDriver driver) throws Exception {
		fc.utobj().printTestStep("Go to item23ReceiptSummaryLnk");
		fc.adminpage().adminPage(driver);
		AdminPageTest p2 = new AdminPageTest();
		p2.openItem23ReceiptSummaryLnk(driver);
	}

	public void logOnCredentialsDurationLnk(WebDriver driver) throws Exception {
		fc.utobj().printTestStep("Go to logOnCredentialsDurationLnk");
		fc.adminpage().adminPage(driver);
		AdminPageTest p2 = new AdminPageTest();
		p2.openlogOnCredentialsDurationLnk(driver);
	}

	public void fddEmailTemplateSummaryLnk(WebDriver driver) throws Exception {
		fc.utobj().printTestStep("Go to fddEmailTemplateSummaryLnk");
		fc.adminpage().adminPage(driver);
		AdminPageTest p2 = new AdminPageTest();
		p2.openFddEmailTemplateSummaryLnk(driver);
	}

	public void fddManagementLnk(WebDriver driver) throws Exception {
		fc.utobj().printTestStep("Go to fddManagementLnk");
		fc.adminpage().adminPage(driver);
		AdminPageTest p2 = new AdminPageTest();
		p2.openFddManagementLnk(driver);
	}

	public void adminConfigurationCommunicationType(WebDriver driver) throws Exception {
		fc.utobj().printTestStep("Go to Admin Configuration Communication Type");
		fc.adminpage().adminPage(driver);
		AdminPageTest p2 = new AdminPageTest();
		p2.openAdminConfigurationCommunicationType(driver);
	}

	public void adminConfigureSiteClearance(WebDriver driver) throws Exception {
		fc.utobj().printTestStep("Go to Admin Configure Site Clearance");
		fc.adminpage().adminPage(driver);
		AdminPageTest p2 = new AdminPageTest();
		p2.openAdminConfigureSiteClearance(driver);
	}

	public void adminConfigureCallStatus(WebDriver driver) throws Exception {
		fc.utobj().printTestStep("Go to Admin Configure Call Status");
		fc.adminpage().adminPage(driver);
		AdminPageTest p2 = new AdminPageTest();
		p2.openAdminConfigureCallStatus(driver);
	}

	public void adminConfigurationConfigureTaskDisplayOnCalendar(WebDriver driver) throws Exception {
		fc.utobj().printTestStep("Go to adminConfigurationConfigureTaskDisplayOnCalendar");
		fc.adminpage().adminPage(driver);
		AdminPageTest p2 = new AdminPageTest();
		p2.openAdminConfigurationConfigureTaskDisplayOnCalendar(driver);
	}

	public void adminConfigureCalendarEventCategories(WebDriver driver) throws Exception {
		fc.utobj().printTestStep("Go to Admin > Configuration > Configure Calender Event Categories");
		fc.adminpage().adminPage(driver);
		AdminPageTest p2 = new AdminPageTest();
		p2.openAdminConfigureCalendarEventCategories(driver);
	}

	public void adminConfigurationConfigureCalendarStartDay(WebDriver driver) throws Exception {
		fc.utobj().printTestStep("Go to Admin > Configuration > Configure Calendar Start Day");
		fc.adminpage().adminPage(driver);
		AdminPageTest p2 = new AdminPageTest();
		p2.openAdminConfigurationConfigureCalendarStartDay(driver);
	}

	public void adminConfigurationDecimalPlace(WebDriver driver) throws Exception {
		fc.utobj().printTestStep("Go to Admin > Configuration > Configure Decimal Place");
		fc.adminpage().adminPage(driver);
		AdminPageTest p2 = new AdminPageTest();
		p2.openAdminConfigurationDecimalPlace(driver);
	}

	public void adminConfigurationPIIConfiguration(WebDriver driver) throws Exception {
		fc.utobj().printTestStep("Go to Admin > Configuration > PII Configuration ");
		fc.adminpage().adminPage(driver);
		AdminPageTest p2 = new AdminPageTest();
		p2.openAdminConfigurationPIIConfiguration(driver);
	}

	public void adminConfigurationConfigureDictionary(WebDriver driver) throws Exception {
		fc.utobj().printTestStep("Go to Admin > Configuration > Configure Dictionary");
		fc.adminpage().adminPage(driver);
		AdminPageTest p2 = new AdminPageTest();
		p2.openAdminConfigurationConfigureDictionary(driver);
	}

	public void adminConfigureQuickLinks(WebDriver driver) throws Exception {
		fc.utobj().printTestStep("Go to Admin > Configuration > Configure Quick Links ");
		fc.adminpage().adminPage(driver);
		AdminPageTest p2 = new AdminPageTest();
		p2.openAdminadminConfigureQuickLinks(driver);
	}

	public void adminConfigurationConfigureTaskType(WebDriver driver) throws Exception {
		fc.utobj().printTestStep("Go to Admin > Configuration > Configure Task Type");
		fc.adminpage().adminPage(driver);
		AdminPageTest p2 = new AdminPageTest();
		p2.openAdminConfigurationConfigureTaskType(driver);
	}

	public void adminConfigurationConfigureOffensiveWatchGreatKeywords(WebDriver driver) throws Exception {
		fc.utobj().printTestStep(
				"Go to Admin > Configuration > Configure Offensive Watch Great Keywords for Forum and FranBuzz");
		fc.adminpage().adminPage(driver);
		AdminPageTest p2 = new AdminPageTest();
		p2.openAdminConfigurationConfigureOffensiveWatchGreatKeywords(driver);
	}

	public void adminConfigurationConfigurePasswordSetting(WebDriver driver) throws Exception {
		fc.utobj().printTestStep("Go to Admin > Configuration > Configure Password Settings");
		fc.adminpage().adminPage(driver);
		AdminPageTest p2 = new AdminPageTest();
		p2.openConfigurePasswordSettings(driver);
	}

	public void adminConfigurationConfigureStoreType(WebDriver driver) throws Exception {
		fc.utobj().printTestStep("Go to Admin > Configuration > Configure Store Type");
		fc.adminpage().adminPage(driver);
		AdminPageTest p2 = new AdminPageTest();
		p2.openAdminConfigurationConfigureStoreType(driver);
	}

	public void adminHiddenLinksConfigureFieldOpsSettings(WebDriver driver) throws Exception {
		fc.utobj().printTestStep("Go to Admin > Hidden Links > Configure Field Ops Settings");
		fc.adminpage().adminPage(driver);
		AdminPageTest p2 = new AdminPageTest();
		p2.openConfigureFieldOpsSettings(driver);

	}

	public void adminHiddenLinksLinkLeadWithExistingLeadorOwner(WebDriver driver) throws Exception {
		fc.utobj().printTestStep("Go to Admin > Hidden Links > Link Lead With Existing Lead / Owner");
		fc.adminpage().adminPage(driver);
		AdminPageTest p2 = new AdminPageTest();
		p2.openLinkLeadWithExistingLeadorOwner(driver);

	}

	public void adminHiddenLinksConfigureNewHierarchyLevel(WebDriver driver) throws Exception {
		fc.utobj().printTestStep("Go to Admin > Hidden Links > Configure New Hierarchy Level");
		fc.adminpage().adminPage(driver);
		AdminPageTest p2 = new AdminPageTest();
		p2.openConfigureNewHierarchyLevel(driver);

	}

	public void adminHiddenLinksConfigureFranBuzzName(WebDriver driver) throws Exception {
		fc.utobj().printTestStep("Go to adminHiddenLinksConfigureFranBuzzName");
		fc.adminpage().adminPage(driver);

	}

	public void adminAddNewFranchiseLocationLnk(WebDriver driver) throws Exception {
		fc.utobj().printTestStep("Go to Admin > Franchise Location > Add Franchise Location ");
		fc.adminpage().adminPage(driver);
		AdminPageTest p2 = new AdminPageTest();
		p2.openAddNewFranchiseLocation(driver);
	}

	public AdminFranchiseLocationManageFranchiseLocationsPageTest manageFranchiseLocationLnk(WebDriver driver) throws Exception {
		fc.utobj().printTestStep("Go to Admin > Franchise Location > Manage Franchise Locations ");
		fc.adminpage().adminPage(driver);
		AdminPageTest p2 = new AdminPageTest();
		p2.openManageFranchiseLocation(driver);
		return new AdminFranchiseLocationManageFranchiseLocationsPageTest();
	}

	public void openAdminCRMCRMConfigureLastContactedFieldLnk(WebDriver driver) throws Exception {
		fc.utobj().printTestStep("Go to Admin > CRM > Configure Last Contacted Field");
		fc.adminpage().adminPage(driver);
		AdminPage pobj = new AdminPage(driver);
		fc.utobj().clickElement(driver, pobj.manageConfigureLastContactedFieldLink);
	}
	public void openAdminFinanceSetupPreferencesLnk(WebDriver driver) throws Exception {
		fc.utobj().printTestStep("Go to Admin > Finance > Finance Setup Preferences");
		fc.adminpage().adminPage(driver);
		AdminPage pobj = new AdminPage(driver);
		fc.utobj().clickElement(driver, pobj.financeSetupPreferencesLnk);
	}

	public void openAdminHiddenLinkConfigureTaxRateLnk(WebDriver driver) throws Exception {
		fc.utobj().printTestStep("Go to Admin > Hidden Link > Configure Tax Rate");
		fc.adminpage().adminPage(driver);
		AdminPage pobj = new AdminPage(driver);
		fc.utobj().clickElement(driver, pobj.hiddenLinkConfigureTaxRate);
	}
	
	// Anukaran
	public void openAdminCRMAssignZip(WebDriver driver) throws Exception {
		fc.utobj().printTestStep("Go to Admin > CRM > Manage Web Form Generator");

		AdminPage pobj = new AdminPage(driver);
		fc.utobj().clickElement(driver, pobj.assingnZipToFranUser);
	}

	// Anukaran
	public void openAdminCRMAssociateZip(WebDriver driver) throws Exception {
		fc.utobj().printTestStep("Go to Admin > CRM > Manage Form Generator");

		AdminPage pobj = new AdminPage(driver);
		fc.utobj().clickElement(driver, pobj.associateWithZiplnk);
	}

	// Anukaran
	public void openAdminCRMAddZip(WebDriver driver) throws Exception {
		fc.utobj().printTestStep("Go to Admin > Zip / Postal Code Locator > Add New Zip / Postal Codes");

		AdminPage pobj = new AdminPage(driver);
		fc.utobj().clickElement(driver, pobj.AddNewZiplnk);
	}

	// Anukaran
	public void openAdminCRMDefaultOwner(WebDriver driver) throws Exception {
		fc.utobj().printTestStep("Go to AdminCRMDefaultOwner");

		AdminPage pobj = new AdminPage(driver);
		fc.utobj().clickElement(driver, pobj.setDefaultOwner);
	}

	// Anukaran
	public void openAdminCRMDefaultContact(WebDriver driver) throws Exception {
		fc.utobj().printTestStep("Go to AdminCRMDefaultContact");

		AdminPage pobj = new AdminPage(driver);
		fc.utobj().clickElement(driver, pobj.setDefaultContact);
	}

	public void openAdminCRMManageFormGeneratorLnk(WebDriver driver) throws Exception {
		fc.utobj().printTestStep("Go to Admin > Info Mgr > Manage Form Generator");
		AdminPage pobj = new AdminPage(driver);
		fc.utobj().clickElement(driver, pobj.crmManageFormGenerator);
	}

	// Anukaran
	public void openAdminZipPostalCodeLocatorAddNewLnk(WebDriver driver) throws Exception {
		adminPage(driver);
		AdminPageTest p2 = new AdminPageTest();

		// change by inzmam for sync build
		marketing(driver);
		p2.openAdminCRMAddZip(driver);
	}

	// Anukaran
	public void openAdminZipPostalCodeLocatorAssociateLnk(WebDriver driver) throws Exception {
		fc.utobj().printTestStep("Go to Admin > Zip / Postal Code Locator > Zip Locator-Zip / Postal Code Lookup");
		adminPage(driver);
		AdminPageTest p2 = new AdminPageTest();

		// change by inzmam for sync build
		marketing(driver);
		p2.openAdminCRMAssociateZip(driver);
	}

	// Anukaran
	public void openAdminConfigureCountries(WebDriver driver) throws Exception {
		fc.utobj().printTestStep("Go to Admin > Configuration > Configure Countries");

		AdminPage pobj = new AdminPage(driver);
		fc.utobj().clickElement(driver, pobj.configureCountriesLnk);
	}

	public void openOptionsQuickLinks(WebDriver driver) throws Exception {
		fc.utobj().printTestStep("Go to Options > Configure Quicks Links");
		AdminPage pobj = new AdminPage(driver);
		FCHomePage home_page = new FCHomePage(driver);
		fc.utobj().clickElement(driver, home_page.userOptions);
		fc.utobj().clickElement(driver, home_page.options);

		try {
			fc.utobj().clickElement(driver, pobj.quickLink);
		} catch (Exception e) {
			fc.utobj().clickElement(driver, pobj.moreBtn);
			fc.utobj().clickElement(driver, pobj.quickLink);
		}
	}
	
	public void openSearch(WebDriver driver) throws Exception {
		fc.utobj().printTestStep("Go to Admin drop down > Search");
		FCHomePage home_page = new FCHomePage(driver);
		fc.utobj().clickElement(driver, home_page.userOptions);
		fc.utobj().clickElement(driver, home_page.search);
	}
}
