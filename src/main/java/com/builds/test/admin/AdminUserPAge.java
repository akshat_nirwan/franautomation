package com.builds.test.admin;

import java.util.Map;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Test;

import com.builds.utilities.FranconnectUtil;
import com.builds.utilities.TestCase;

public class AdminUserPAge {

	FranconnectUtil fc = new FranconnectUtil();

	@Test(groups = { "sales_2154" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "TC_Admin_AddCorporateUser_001", testCaseDescription = "Add Corpreate User with basic info")
	void changeStatusActionMenu_WithoutRemarks() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		Map<String,String> dataSet = fc.utobj().readTestData("sales", testCaseId);

		try {
			driver = fc.loginpage().login(driver);
			
			
			fc.commonMethods().getModules().openAdminPage(driver);
			fc.adminpage().openCorporateUserPage(driver);
			
				
			
			ManageCorporateUsers_Testing co = new ManageCorporateUsers_Testing();
			co.clickAddCorporateUser(driver);
			
			AddCorporateUser_Testing cu = new AddCorporateUser_Testing();
			
			CorporateUser u = new CorporateUser();
			u.setLoginId("adm");
			u.setFirstname("test");
			u.setLastname("fran");
			
			cu.fillUserInfo(driver,u);
			cu.submit(driver);
			
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}
	
}
