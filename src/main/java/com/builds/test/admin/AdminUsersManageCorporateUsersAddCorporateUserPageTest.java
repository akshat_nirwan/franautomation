package com.builds.test.admin;

import org.openqa.selenium.WebDriver;

import com.builds.test.common.CorporateUser;
import com.builds.test.common.CorporateUserTest;

public class AdminUsersManageCorporateUsersAddCorporateUserPageTest extends CorporateUserTest {
	public CorporateUser createDefaultUser(WebDriver driver, CorporateUser corpUser) throws Exception {

		CorporateUserTest corpTest = new CorporateUserTest();
		corpTest.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

		return corpUser;
	}
}
