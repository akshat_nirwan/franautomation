package com.builds.test.admin;

import org.openqa.selenium.WebDriver;

import com.builds.uimaps.admin.AdminUsersManageCorporateUsersPage;
import com.builds.uimaps.admin.AdminUsersManageDivisionalUsersAddDivisionalUsersPage;
import com.builds.utilities.FranconnectUtil;

public class AdminUsersManageCorporateUsersPageTest {
	FranconnectUtil fc = new FranconnectUtil();

	public void addCorporateUsers(WebDriver driver) throws Exception {

		FranconnectUtil fc = new FranconnectUtil();
		fc.utobj().printTestStep("Click on Add Corporate User Button");

		AdminUsersManageDivisionalUsersAddDivisionalUsersPage ui = new AdminUsersManageDivisionalUsersAddDivisionalUsersPage(
				driver);
		fc.utobj().clickElement(driver, ui.addCorporateUserButton);

	}

	public void searchCorporateUsers(WebDriver driver, String userName) throws Exception {

		try {
			AdminUsersManageCorporateUsersPage pobj = new AdminUsersManageCorporateUsersPage(driver);
			fc.utobj().sendKeys(driver, pobj.search, userName);
			fc.utobj().clickElement(driver, pobj.searchButton);
			boolean isTrue = fc.utobj().assertPageSource(driver, userName);
			if (isTrue == false) {
				fc.utobj().throwsException("Not able to search added Corporate user");
			}
		} catch (Exception e) {
			fc.utobj().throwsException(e.getMessage().toString());
		}
	}
}
