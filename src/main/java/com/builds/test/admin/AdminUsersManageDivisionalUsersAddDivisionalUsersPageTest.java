package com.builds.test.admin;

import java.util.Map;

import org.openqa.selenium.WebDriver;
import org.testng.Reporter;
import org.testng.annotations.Test;

import com.builds.uimaps.admin.AdminUsersManageCorporateUsersAddCorporateUserPage;
import com.builds.uimaps.admin.AdminUsersManageDivisionalUsersAddDivisionalUsersPage;
import com.builds.utilities.FranconnectUtil;
import com.builds.utilities.TestCase;

public class AdminUsersManageDivisionalUsersAddDivisionalUsersPageTest {
	FranconnectUtil fc = new FranconnectUtil();

	@Test(groups = { "sales" , "TC_Admin_User_003" }) //
	@TestCase(createdOn = "2017-10-24", updatedOn = "2017-10-24", testCaseId = "TC_Admin_User_003", testCaseDescription = "Verify the Divisional User is getting added, modified and deleted.")
	public void addDivisionalUser() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("sales", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			String userName = fc.utobj().generateTestData(dataSet.get("corpUser"));
			String divisionName = fc.utobj().generateTestData(dataSet.get("divisionName"));
			String userNameNew = fc.utobj().generateTestData(dataSet.get("corpUserNew"));
			String emailId = "salesautomation@staffex.com";

			fc.utobj().printTestStep("Admin > Users > Manage Divisional Users > Add User > Fill Details and Save");
			AdminUsersManageCorporateUsersAddCorporateUserPage pobj = new AdminUsersManageCorporateUsersAddCorporateUserPage(
					driver);
			addDivisionalUser(driver, userName, divisionName, emailId);
			fc.utobj().printTestStep("Validate the User is added.");
			fc.utobj().sendKeys(driver, pobj.searchCorpUser, userName);
			fc.utobj().clickElement(driver, pobj.searchCorpUserBtn);
			boolean isRegUserAdded = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//*[contains(text(),'" + userName + "')]");
			if (isRegUserAdded == false) {
				fc.utobj().throwsException("Divisional user not getting added");
			}
			fc.utobj().printTestStep(" Now modify the user info > Save");
			fc.utobj().actionImgOption(driver, userName, "Modify");
			fc.utobj().sendKeys(driver, pobj.firstName, userNameNew);
			fc.utobj().sendKeys(driver, pobj.lastName, userNameNew);
			fc.utobj().clickElement(driver, pobj.submit);
			fc.utobj().printTestStep(" Validate the User Info is getting modified.");

			fc.utobj().sendKeys(driver, pobj.searchCorpUser, userNameNew);
			fc.utobj().clickElement(driver, pobj.searchCorpUserBtn);
			isRegUserAdded = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//*[contains(text(),'" + userNameNew + "')]");
			if (isRegUserAdded == false) {
				fc.utobj().throwsException("Divisional user not getting Modified");
			}

			fc.utobj().printTestStep("Delete the User");
			fc.utobj().actionImgOption(driver, userNameNew, "Delete");
			fc.utobj().acceptAlertBox(driver);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.commonMethods().Click_Close_Input_ByValue(driver);
			fc.utobj().printTestStep(" Validate the user is deleted from the user summary page.");
			isRegUserAdded = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//*[contains(text(),'" + userNameNew + "')]");
			if (isRegUserAdded == true) {
				fc.utobj().throwsException("Divisional user not getting deleted");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	public String addDivisionalUser(WebDriver driver, String userName, String password, String divisionName,
			String emailId) throws Exception {
			try {
				AdminUsersManageDivisionalUsersAddDivisionalUsersPage pobj = new AdminUsersManageDivisionalUsersAddDivisionalUsersPage(
						driver);

				fc.adminpage().adminUserDivisionalUserAddDivisionalUser(driver);

				fc.utobj().sendKeys(driver, pobj.userName, userName);
				fc.utobj().sendKeys(driver, pobj.password, password);
				fc.utobj().sendKeys(driver, pobj.confirmPassword, password);
				fc.utobj().selectValFromMultiSelect(driver, pobj.roleMultiSelectBtn,"Default Division Role");
				fc.utobj().selectDropDown(driver, pobj.timeZone, "GMT -06:00 US/Canada/Central");

				String i18n = FranconnectUtil.config.get("i18Files");
				if (i18n != null && !i18n.isEmpty()) {
					fc.utobj().selectDropDown(driver, pobj.userLanguage, "French");
				}else {
					fc.utobj().selectDropDownByValue(driver, pobj.userLanguage, "en");
				}
				fc.utobj().selectValFromMultiSelect(driver, pobj.multiselectDivision, divisionName);
				fc.utobj().sendKeys(driver, pobj.firstName, userName);
				fc.utobj().sendKeys(driver, pobj.lastName, userName);
				fc.utobj().sendKeys(driver, pobj.city, "11");
				fc.utobj().selectDropDown(driver, pobj.country, "USA");
				fc.utobj().selectDropDown(driver, pobj.state, "Alabama");
				fc.utobj().sendKeys(driver, pobj.phone1, "1234567890");
				fc.utobj().sendKeys(driver, pobj.phoneExt1, "2");
				fc.utobj().sendKeys(driver, pobj.email, emailId);

				try {
					if (fc.utobj().isSelected(driver,pobj.auditor)) {
					} else {
						fc.utobj().clickElement(driver, pobj.auditor);
					}
				} catch (Exception e) {
				}
				fc.utobj().clickElement(driver, pobj.submit);
			} catch (Exception e) {
				fc.utobj().throwsException("Not able to add Divisional User");
			}
		return userName;
	}

	public String addDivisionalUser(WebDriver driver, String userName, String divisionName, String emailId)
			throws Exception {

			try {

				AdminDivisionAddDivisionPageTest p1 = new AdminDivisionAddDivisionPageTest();
				p1.addDivision(driver, divisionName);

				AdminUsersManageDivisionalUsersAddDivisionalUsersPage pobj = new AdminUsersManageDivisionalUsersAddDivisionalUsersPage(
						driver);
				fc.adminpage().adminUserDivisionalUserAddDivisionalUser(driver);

				fc.utobj().sendKeys(driver, pobj.userName, userName);
				fc.utobj().sendKeys(driver, pobj.password, "fran1234");
				fc.utobj().sendKeys(driver, pobj.confirmPassword, "fran1234");

				fc.utobj().selectValFromMultiSelect(driver, pobj.roleMultiSelectBtn, /*"Default Divisional Role"*/"Default Division Role");

				fc.utobj().selectDropDown(driver, pobj.timeZone, "GMT -06:00 US/Canada/Central");

				String i18n = FranconnectUtil.config.get("i18Files");
				if (i18n != null && !i18n.isEmpty()) {
					fc.utobj().selectDropDown(driver, pobj.userLanguage, "French");
				}else {
					fc.utobj().selectDropDownByValue(driver, pobj.userLanguage, "en");
				}
				fc.utobj().selectValFromMultiSelect(driver, pobj.multiselectDivision, divisionName);
				fc.utobj().sendKeys(driver, pobj.firstName, userName);
				fc.utobj().sendKeys(driver, pobj.lastName, userName);
				fc.utobj().sendKeys(driver, pobj.city, "11");
				fc.utobj().selectDropDown(driver, pobj.country, "USA");
				fc.utobj().selectDropDown(driver, pobj.state, "Alabama");
				fc.utobj().sendKeys(driver, pobj.phone1, "1234567890");
				fc.utobj().sendKeys(driver, pobj.phoneExt1, "2");
				fc.utobj().sendKeys(driver, pobj.email, emailId);
				fc.utobj().clickElement(driver, pobj.submit);
			} catch (Exception e) {
				fc.utobj().throwsException("Not able to add Divisional User");
			}
		return userName;
	}

	public String addDivisionalUserWithDivisionNameExist(WebDriver driver, String userName, String divisionName,
			Map<String, String> config, String emailId) throws Exception {

		String testCaseId = "TC_addDivisionalUser_04";
		if (fc.utobj().validate(testCaseId)) {
			try {

				AdminUsersManageDivisionalUsersAddDivisionalUsersPage pobj = new AdminUsersManageDivisionalUsersAddDivisionalUsersPage(
						driver);
				fc.adminpage().adminUserDivisionalUserAddDivisionalUser(driver);

				fc.utobj().sendKeys(driver, pobj.userName, userName);
				fc.utobj().sendKeys(driver, pobj.password, "T0n1ght1");
				fc.utobj().sendKeys(driver, pobj.confirmPassword, "T0n1ght1");


				fc.utobj().selectValFromMultiSelect(driver, pobj.roleMultiSelectBtn, /*"Default Divisional Role"*/"Default Division Role");

				fc.utobj().selectDropDown(driver, pobj.timeZone, "GMT -06:00 US/Canada/Central");

				String i18n = FranconnectUtil.config.get("i18Files");
				if (i18n != null && !i18n.isEmpty()) {
					fc.utobj().selectDropDown(driver, pobj.userLanguage, "French");
				}else {
					fc.utobj().selectDropDownByValue(driver, pobj.userLanguage, "en");
				}
				fc.utobj().selectValFromMultiSelect(driver, pobj.multiselectDivision, divisionName);
				

				fc.utobj().sendKeys(driver, pobj.firstName, userName);
				fc.utobj().sendKeys(driver, pobj.lastName, userName);

				fc.utobj().sendKeys(driver, pobj.city, "11");

				fc.utobj().selectDropDown(driver, pobj.country, "United Kingdom");

				fc.utobj().selectDropDown(driver, pobj.state, "Wales");

				fc.utobj().sendKeys(driver, pobj.phone1, "1234567890");
				fc.utobj().sendKeys(driver, pobj.phoneExt1, "2");
				fc.utobj().sendKeys(driver, pobj.email, emailId);
				fc.utobj().clickElement(driver, pobj.submit);

				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException("was not able to add Divisional User");

		}
		return userName;
	}

	public String addDivisionalUserWithRole(WebDriver driver, String userName, String divisionName, String roleName,
			String password, String emailId) throws Exception {

		try {

			AdminDivisionAddDivisionPageTest p1 = new AdminDivisionAddDivisionPageTest();
			p1.addDivision(driver, divisionName);
			AdminUsersManageDivisionalUsersAddDivisionalUsersPage pobj = new AdminUsersManageDivisionalUsersAddDivisionalUsersPage(
					driver);

			fc.adminpage().adminUserDivisionalUserAddDivisionalUser(driver);

			fc.utobj().sendKeys(driver, pobj.userName, userName);
			fc.utobj().sendKeys(driver, pobj.password, password);
			fc.utobj().sendKeys(driver, pobj.confirmPassword, password);

			fc.utobj().selectValFromMultiSelect(driver, pobj.roleMultiSelectBtn, roleName);

			fc.utobj().selectDropDown(driver, pobj.timeZone, "GMT -06:00 US/Canada/Central");

			String i18n = FranconnectUtil.config.get("i18Files");
			if (i18n != null && !i18n.isEmpty()) {
				fc.utobj().selectDropDown(driver, pobj.userLanguage, "French");
			} else {
				fc.utobj().selectDropDownByValue(driver, pobj.userLanguage, "en");
			}
			fc.utobj().selectValFromMultiSelect(driver, pobj.multiselectDivision, divisionName);
			
			
			fc.utobj().sendKeys(driver, pobj.firstName, userName);
			fc.utobj().sendKeys(driver, pobj.lastName, userName);
			fc.utobj().sendKeys(driver, pobj.city, "11");
			fc.utobj().selectDropDown(driver, pobj.country, "United Kingdom");
			fc.utobj().selectDropDown(driver, pobj.state, "Wales");
			fc.utobj().sendKeys(driver, pobj.phone1, "1234567890");
			fc.utobj().sendKeys(driver, pobj.phoneExt1, "2");
			fc.utobj().sendKeys(driver, pobj.email, emailId);
			fc.utobj().clickElement(driver, pobj.submit);
		} catch (Exception e) {
			fc.utobj().printTestStep("Not able to add Divisional User");
			Reporter.log(e.getMessage().toString());
		}
		return userName;
	}

	public String addNewUserLevelUserWithRole(WebDriver driver, String userName, String divisionName, String roleName,
			String password, Map<String, String> config, String emailId, String userLevelName) throws Exception {

		String testCaseId = "TC_addDivisionalUserWithRole_06";
		if (fc.utobj().validate(testCaseId)) {
			try {

				AdminDivisionAddDivisionPageTest p1 = new AdminDivisionAddDivisionPageTest();
				p1.addDivision(driver, divisionName);
				AdminUsersManageDivisionalUsersAddDivisionalUsersPage pobj = new AdminUsersManageDivisionalUsersAddDivisionalUsersPage(
						driver);

				fc.adminpage().adminUserDivisionalUserAddDivisionalUser(driver);

				fc.utobj().sendKeys(driver, pobj.userName, userName);
				fc.utobj().sendKeys(driver, pobj.password, password);
				fc.utobj().sendKeys(driver, pobj.confirmPassword, password);

				fc.utobj().selectValFromMultiSelect(driver, pobj.roleMultiSelectBtn, roleName);

				fc.utobj().selectDropDown(driver, pobj.timeZone, "GMT -06:00 US/Canada/Central");

				String i18n = FranconnectUtil.config.get("i18Files");
				if (i18n != null && !i18n.isEmpty()) {
					fc.utobj().selectDropDown(driver, pobj.userLanguage, "French");
				}else {
					fc.utobj().selectDropDownByValue(driver, pobj.userLanguage, "en");
				}
				fc.utobj().selectValFromMultiSelect(driver, pobj.multiselectDivision, divisionName);
				fc.utobj().sendKeys(driver, pobj.firstName, userName);
				fc.utobj().sendKeys(driver, pobj.lastName, userName);
				fc.utobj().sendKeys(driver, pobj.city, "11");
				fc.utobj().selectDropDown(driver, pobj.country, "United Kingdom");
				fc.utobj().selectDropDown(driver, pobj.state, "Wales");
				fc.utobj().sendKeys(driver, pobj.phone1, "1234567890");
				fc.utobj().sendKeys(driver, pobj.phoneExt1, "2");
				fc.utobj().sendKeys(driver, pobj.email, emailId);
				fc.utobj().clickElement(driver, pobj.submit);

				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException("was not able to add Divisional User With Role");
		}
		return userName;
	}
}