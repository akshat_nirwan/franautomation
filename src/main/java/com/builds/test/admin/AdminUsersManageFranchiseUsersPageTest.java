package com.builds.test.admin;

import java.util.Map;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Optional;
import org.testng.annotations.Test;

import com.builds.uimaps.admin.AdminFranchiseLocationManageFranchiseLocationsPage;
import com.builds.uimaps.admin.AdminUsersManageCorporateUsersAddCorporateUserPage;
import com.builds.uimaps.admin.AdminUsersManageFranchiseUsers;
import com.builds.utilities.FranconnectUtil;
import com.builds.utilities.TestCase;

public class AdminUsersManageFranchiseUsersPageTest {
	FranconnectUtil fc = new FranconnectUtil();

	public void searchFranchiseUserByMUID(WebDriver driver, String muId, String userName, Map<String, String> config)
			throws Exception {

		String testCaseId = "TC_searchFranchiseUserByMUID_01";
		if (fc.utobj().validate(testCaseId)) {
			try {
				boolean isLnkFound = false;
				AdminUsersManageFranchiseUsers pobj = new AdminUsersManageFranchiseUsers(driver);
				muId = muId + " (" + userName + ")";
				fc.utobj().selectDropDown(driver, pobj.muidDrp, muId);
				fc.utobj().clickElement(driver, pobj.searchButton);
				isLnkFound = fc.utobj().assertLinkPartialText(driver, userName);
				if (isLnkFound == false) {
					fc.utobj().throwsException("MUID Not found!");
				}

				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException("was not able to search Franchise User By MUID");

		}
	}

	@Test(groups = { "admin"})
	@TestCase(createdOn = "2017-10-24", updatedOn = "2017-10-24", testCaseId = "TC_Admin_User_004", testCaseDescription = "Verify the Franchise User is getting added, modified and deleted.")
	public void checkLockOptionSites() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("sales", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			fc.utobj().printTestStep("Verify the Franchise User is getting added, modified and deleted.");
			AdminUsersManageCorporateUsersAddCorporateUserPage pobj = new AdminUsersManageCorporateUsersAddCorporateUserPage(
					driver);
			AdminFranchiseLocationAddFranchiseLocationPageTest franchiseLocation = new AdminFranchiseLocationAddFranchiseLocationPageTest();

			String franchiseId = fc.utobj().generateTestData(dataSet.get("franchiseId"));
			String regionName = fc.utobj().generateTestData(dataSet.get("regionName"));
			String password = "T0n1ght1";
			String userNameF = fc.utobj().generateTestData(dataSet.get("userNameF"));
			String firstNameF = fc.utobj().generateTestData(dataSet.get("firstNameF"));
			String storeType = fc.utobj().generateTestData(dataSet.get("storeType"));
			String roleName = "Default Franchise Role";
			String emailId = "salesautomation@staffex.com";

			String franchiseID = franchiseLocation.addFranchiseLocation_AllWithFName(driver, franchiseId, regionName,
					storeType, firstNameF);
			AdminUsersManageManageFranchiseUsersPageTest addFranUser = new AdminUsersManageManageFranchiseUsersPageTest();
			addFranUser.addFranchiseUser(driver, userNameF, password, franchiseId, roleName, emailId);

			fc.utobj().printTestStep("Validate the User is added.");
			fc.utobj().sendKeys(driver, pobj.searchCorpUser, firstNameF);
			fc.utobj().clickElement(driver, pobj.searchCorpUserBtn);
			boolean isRegUserAdded = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[contains(text(),'"+firstNameF+"')]");
			if (isRegUserAdded == false) {
				fc.utobj().throwsException("Frachisee user not getting added");
			}
			fc.utobj().printTestStep("Now modify the user info > Save");
			fc.utobj().actionImgOption(driver, firstNameF, "Modify");
			fc.utobj().sendKeys(driver, pobj.jobTitle, "JobTitle");
			fc.utobj().clickElement(driver, pobj.submit);

			fc.utobj().printTestStep("Validate the User Info is getting modified.");
			fc.utobj().sendKeys(driver, pobj.searchCorpUser, firstNameF);
			fc.utobj().clickElement(driver, pobj.searchCorpUserBtn);
			isRegUserAdded = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//*[contains(text(),'JobTitle')]");
			if (isRegUserAdded == false) {
				fc.utobj().throwsException("Frachise user not getting Modified");
			}

			fc.utobj().printTestStep("Delete the User");
			fc.utobj().actionImgOption(driver, firstNameF, "Delete");
			fc.utobj().acceptAlertBox(driver);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().clickElement(driver, pobj.Delete_Input_ByValue);
			fc.utobj().clickElement(driver, pobj.Close_Input_ByValue);
			fc.utobj().printTestStep(" Validate the user is deleted from the user summary page.");
			
			isRegUserAdded = fc.utobj().assertPageSource(driver, "No records found.");
			if (isRegUserAdded == false) {
				fc.utobj().throwsException("Franchise user not getting deleted");
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}

	}

	public void searchFranchiseUserByFranchiseId(WebDriver driver, @Optional() String franchiseId,
			Map<String, String> config) throws Exception {

		String testCaseId = "TC_searchFranchiseUserByFranchiseId_01";
		if (fc.utobj().validate(testCaseId)) {
			try {

				boolean isLnkFound = false;
				AdminFranchiseLocationManageFranchiseLocationsPage pobj = new AdminFranchiseLocationManageFranchiseLocationsPage(
						driver);
				fc.utobj().selectDropDown(driver, pobj.franchiseId, franchiseId);
				fc.utobj().clickElement(driver, pobj.searchButton);
				isLnkFound = fc.utobj().assertLinkPartialText(driver, franchiseId);
				if (isLnkFound == false) {
					fc.utobj().throwsException("MUID Not found!");
				}

				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException("was not able to search Franchise User By FranchiseId");

		}
	}
}
