package com.builds.test.admin;

import java.util.List;
import java.util.Map;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.builds.uimaps.admin.AdminUsersManageManageFranchiseUsersPage;
import com.builds.utilities.FranconnectUtil;

public class AdminUsersManageManageFranchiseUsersPageTest {
	FranconnectUtil fc = new FranconnectUtil();

	@Parameters({ "userName" })
	@Test(groups = { "commonModule", "add", "Broker Agency" })
	public String addFranchiseUser(@Optional() String userName, Map<String, String> config) throws Exception {
		WebDriver driver = fc.loginpage().login();
		String regionName = "Test";
		AdminAreaRegionAddAreaRegionPageTest p1 = new AdminAreaRegionAddAreaRegionPageTest();
		regionName = p1.addAreaRegion(driver, regionName);
		String franchiseName = "Test";
		String storeType = "Test";
		AdminFranchiseLocationAddFranchiseLocationPageTest addFranId = new AdminFranchiseLocationAddFranchiseLocationPageTest();
		franchiseName = addFranId.addFranchiseLocation_All(driver, franchiseName, regionName, storeType);
		AdminUsersManageManageFranchiseUsersPage pobj = new AdminUsersManageManageFranchiseUsersPage(driver);
		fc.utobj().sendKeys(driver, pobj.userName, userName);
		fc.utobj().sendKeys(driver, pobj.password, "t0n1ght1");
		fc.utobj().sendKeys(driver, pobj.confirmPassword, "t0n1ght1");
		fc.utobj().selectDropDown(driver, pobj.type, "Normal User");
		fc.utobj().selectDropDown(driver, pobj.expiryDays, "Not Applicable");
		fc.utobj().selectValFromMultiSelect(driver, pobj.selectRole, "Default Franchise Role");
		
		
		fc.utobj().selectDropDown(driver, pobj.timeZone, "GMT -06:00 US/Canada/Central");
		fc.utobj().sendKeys(driver, pobj.jobTitle, "11");
		fc.utobj().sendKeys(driver, pobj.address, "11");
		fc.utobj().sendKeys(driver, pobj.city, "11");
		fc.utobj().selectDropDown(driver, pobj.country, "United Kingdom");
		fc.utobj().sendKeys(driver, pobj.zipcode, "11");
		fc.utobj().selectDropDown(driver, pobj.state, "Wales");
		fc.utobj().sendKeys(driver, pobj.phone1, "1234567890");
		fc.utobj().sendKeys(driver, pobj.phoneExt1, "2");
		fc.utobj().sendKeys(driver, pobj.phone2, "4444455555");
		fc.utobj().sendKeys(driver, pobj.phoneExt2, "3");
		fc.utobj().sendKeys(driver, pobj.fax, "33333344444");
		fc.utobj().sendKeys(driver, pobj.mobile, "3333344444");
		fc.utobj().sendKeys(driver, pobj.email, "test@franconnect.com");
		fc.utobj().clickElement(driver, pobj.isBillable);
		fc.utobj().clickElement(driver, pobj.submit);

		return userName;
	}

	public String addFranchiseUser(WebDriver driver, String userName, String password, String franchiseId,
			String roleName, String emailId) throws Exception {

		String testCaseId = "TC_addFranchiseUserAdmin_01";
		if (fc.utobj().validate(testCaseId)) {
			try {
				AdminConfigurationConfigurePasswordSettingsPageTest configurePassword = new AdminConfigurationConfigurePasswordSettingsPageTest();
				configurePassword.ConfigurePasswordSettings(driver);

				AdminUsersManageManageFranchiseUsersPage pobj = new AdminUsersManageManageFranchiseUsersPage(driver);
				fc.adminpage().adminUsersManageFranchiseUsersPage(driver);
				fc.utobj().clickElement(driver, pobj.addFranchiseUserBtn);
				List<WebElement> list = pobj.franchiseIdMuid;
				try {
					if (fc.utobj().isSelected(driver,list.get(0))) {
					} else {
						fc.utobj().clickElement(driver, list.get(0));
					}
				} catch (Exception e) {

				}
				fc.utobj().selectDropDown(driver, pobj.franchiseeNo, franchiseId);
				fc.utobj().clickElement(driver, pobj.addUserBtn);

				String i18n = FranconnectUtil.config.get("i18Files");
				if (i18n != null && !i18n.isEmpty()) {
					fc.utobj().selectDropDown(driver, pobj.userLanguage, "French");
				}else {
					fc.utobj().selectDropDownByValue(driver, pobj.userLanguage, "en");
				}

				fc.utobj().sendKeys(driver, pobj.userName, userName);
				fc.utobj().sendKeys(driver, pobj.password, password);
				fc.utobj().sendKeys(driver, pobj.confirmPassword, password);
				fc.utobj().selectValFromMultiSelect(driver, pobj.selectRole, roleName);
				fc.utobj().selectDropDown(driver, pobj.timeZone, "GMT -06:00 US/Canada/Central");
				fc.utobj().selectDropDown(driver, pobj.userType, "Owner");

				fc.utobj().sendKeys(driver, pobj.city, "11");
				fc.utobj().selectDropDown(driver, pobj.country, "USA");

				fc.utobj().selectDropDown(driver, pobj.state, "Alabama");

				fc.utobj().sendKeys(driver, pobj.phone1, "1234567890");
				fc.utobj().sendKeys(driver, pobj.phoneExt1, "2");

				fc.utobj().sendKeys(driver, pobj.email, emailId);

				fc.utobj().clickElement(driver, pobj.submit);

				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException("was not able to add Franchise User Admin");

		}
		return userName;
	}

	public String addFranchiseUserForMkt(WebDriver driver, String userName, String password, String franchiseId,
			String roleName, Map<String, String> config, String emailId) throws Exception {

		String testCaseId = "TC_addFranchiseUserForMkt_01";
		if (fc.utobj().validate(testCaseId)) {
			try {
				AdminConfigurationConfigurePasswordSettingsPageTest configurePassword = new AdminConfigurationConfigurePasswordSettingsPageTest();
				configurePassword.ConfigurePasswordSettings(driver);

				AdminUsersManageManageFranchiseUsersPage pobj = new AdminUsersManageManageFranchiseUsersPage(driver);
				fc.adminpage().adminUsersManageFranchiseUsersPage(driver);
				fc.utobj().clickElement(driver, pobj.addFranchiseUserBtn);
				List<WebElement> list = pobj.franchiseIdMuid;
				try {
					if (fc.utobj().isSelected(driver,list.get(0))) {
					} else {
						fc.utobj().clickElement(driver, list.get(0));
					}
				} catch (Exception e) {
				}
				fc.utobj().selectDropDown(driver, pobj.franchiseeNo, franchiseId);
				fc.utobj().clickElement(driver, pobj.addUserBtn);
				fc.utobj().sendKeys(driver, pobj.userName, userName);
				fc.utobj().sendKeys(driver, pobj.password, password);
				fc.utobj().sendKeys(driver, pobj.confirmPassword, password);
				fc.utobj().selectValFromMultiSelect(driver, pobj.selectRole, roleName);
				fc.utobj().selectDropDown(driver, pobj.timeZone, "GMT -06:00 US/Canada/Central");
				fc.utobj().selectDropDown(driver, pobj.userType, "Owner");
				fc.utobj().sendKeys(driver, pobj.city, "11");
				fc.utobj().selectDropDown(driver, pobj.country, "USA");
				fc.utobj().selectDropDown(driver, pobj.state, "Alabama");
				fc.utobj().sendKeys(driver, pobj.phone1, "1234567890");
				fc.utobj().sendKeys(driver, pobj.phoneExt1, "2");
				fc.utobj().sendKeys(driver, pobj.email, emailId);
				fc.utobj().clickElement(driver, pobj.submit);

				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException("was not able to add Franchise User For Mkt");

		}

		return userName;
	}

	public String addMUIDUser(WebDriver driver, String userName, String password, String muId, String firstMuidName,
			Map<String, String> config, String emailId) throws Exception {

		String testCaseId = "TC_addMUIDUserAdmin_01";
		if (fc.utobj().validate(testCaseId)) {
			try {
				AdminUsersManageManageFranchiseUsersPage pobj = new AdminUsersManageManageFranchiseUsersPage(driver);

				fc.adminpage().adminUsersManageFranchiseUsersPage(driver);
				fc.utobj().clickElement(driver, pobj.addFranchiseUserBtn);
				List<WebElement> list = pobj.franchiseIdMuid;
				if (fc.utobj().isSelected(driver,list.get(1))) {
				} else {
					fc.utobj().clickElement(driver, list.get(1));
				}

				fc.utobj().selectDropDownByPartialText(driver, pobj.muidValueSelect, muId);
				fc.utobj().clickElement(driver, pobj.addUserBtn);

				fc.utobj().sendKeys(driver, pobj.userName, userName);
				fc.utobj().sendKeys(driver, pobj.password, password);
				fc.utobj().sendKeys(driver, pobj.confirmPassword, password);
				fc.utobj().selectValFromMultiSelect(driver, pobj.selectRole, "Default Franchise Role");
				fc.utobj().selectDropDown(driver, pobj.timeZone, "GMT -06:00 US/Canada/Central");
				fc.utobj().selectDropDown(driver, pobj.userType, "New Employee");
				fc.utobj().selectDropDownByValue(driver, pobj.userLanguage, "en");
				fc.utobj().selectDropDown(driver, pobj.salutation, "Mr.");
				fc.utobj().sendKeys(driver, pobj.firstName, firstMuidName);
				fc.utobj().sendKeys(driver, pobj.lastName, "lastName");
				fc.utobj().sendKeys(driver, pobj.city, "11");
				fc.utobj().selectDropDown(driver, pobj.country, "United Kingdom");
				fc.utobj().selectDropDown(driver, pobj.state, "Wales");
				fc.utobj().sendKeys(driver, pobj.phone1, "1234567890");
				fc.utobj().sendKeys(driver, pobj.phoneExt1, "2");
				fc.utobj().sendKeys(driver, pobj.email, emailId);
				fc.utobj().clickElement(driver, pobj.submit);
				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException("was not able to add MUIDUSer");

		}
		return userName;
	}

	public String addFranchiseUserEmployee(WebDriver driver, String userName, String password, String franchiseId,
			String roleName, Map<String, String> config, String emailId) throws Exception {

		String testCaseId = "TC_addFranchiseUserAdmin_02";
		if (fc.utobj().validate(testCaseId)) {
			try {
				AdminConfigurationConfigurePasswordSettingsPageTest configurePassword = new AdminConfigurationConfigurePasswordSettingsPageTest();
				configurePassword.ConfigurePasswordSettings(driver);

				AdminUsersManageManageFranchiseUsersPage pobj = new AdminUsersManageManageFranchiseUsersPage(driver);

				fc.adminpage().adminUsersManageFranchiseUsersPage(driver);

				fc.utobj().clickElement(driver, pobj.addFranchiseUserBtn);

				List<WebElement> list = pobj.franchiseIdMuid;
				try {
					if (fc.utobj().isSelected(driver,list.get(0))) {
					} else {
						fc.utobj().clickElement(driver, list.get(0));
					}
				} catch (Exception e) {

				}
				fc.utobj().selectDropDown(driver, pobj.franchiseeNo, franchiseId);
				fc.utobj().clickElement(driver, pobj.addUserBtn);

				String i18n = FranconnectUtil.config.get("i18Files");
				if (i18n != null && !i18n.isEmpty()) {
					fc.utobj().selectDropDown(driver, pobj.userLanguage, "French");
				}else {
					fc.utobj().selectDropDownByValue(driver, pobj.userLanguage, "en");
				}

				fc.utobj().sendKeys(driver, pobj.userName, userName);
				fc.utobj().sendKeys(driver, pobj.password, password);
				fc.utobj().sendKeys(driver, pobj.confirmPassword, password);
				
				fc.utobj().selectValFromMultiSelect(driver, pobj.selectRole, roleName);
				
				
				fc.utobj().selectDropDown(driver, pobj.timeZone, "GMT -06:00 US/Canada/Central");
				fc.utobj().selectDropDown(driver, pobj.userType, "New Employee");
				fc.utobj().selectDropDownByValue(driver, pobj.salutation, "0");
				fc.utobj().sendKeys(driver, pobj.firstName, userName);
				fc.utobj().sendKeys(driver, pobj.lastName, userName);
				fc.utobj().sendKeys(driver, pobj.city, "11");
				fc.utobj().selectDropDown(driver, pobj.country, "USA");
				fc.utobj().selectDropDown(driver, pobj.state, "Alabama");
				fc.utobj().sendKeys(driver, pobj.phone1, "1234567890");
				fc.utobj().sendKeys(driver, pobj.phoneExt1, "2");
				fc.utobj().sendKeys(driver, pobj.email, emailId);
				fc.utobj().clickElement(driver, pobj.submit);
				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException("was not able to add Franchise User Admin");

		}
		return userName;
	}

	public String addFranchiseUserwithoutchangeinsettings(WebDriver driver, String userName, String password, String franchiseId,
			String roleName, String emailId,Map<String,String> dataSet) throws Exception {


		String testCaseId = "TC_addFranchiseUserAdmin_01";
		if (fc.utobj().validate(testCaseId)) {
			try {
				/*AdminConfigurationConfigurePasswordSettingsPageTest configurePassword = new AdminConfigurationConfigurePasswordSettingsPageTest();
				configurePassword.ConfigurePasswordSettings(driver);*/

				AdminUsersManageManageFranchiseUsersPage pobj = new AdminUsersManageManageFranchiseUsersPage(driver);
				fc.adminpage().adminUsersManageFranchiseUsersPage(driver);
				fc.utobj().clickElement(driver, pobj.addFranchiseUserBtn);
				List<WebElement> list = pobj.franchiseIdMuid;
				try {
					if (fc.utobj().isSelected(driver,list.get(0))) {
					} else {
						fc.utobj().clickElement(driver, list.get(0));
					}
				} catch (Exception e) {

				}
				fc.utobj().selectDropDown(driver, pobj.franchiseeNo, franchiseId);
				fc.utobj().clickElement(driver, pobj.addUserBtn);

				String i18n = FranconnectUtil.config.get("i18Files");
				if (i18n != null && !i18n.isEmpty()) {
					fc.utobj().selectDropDown(driver, pobj.userLanguage,dataSet.get("Franlanguage1"));
				}else {
					fc.utobj().selectDropDownByValue(driver, pobj.userLanguage,dataSet.get("Franlanguage"));
				}

				fc.utobj().sendKeys(driver, pobj.userName, userName);
				fc.utobj().sendKeys(driver, pobj.password, password);
				fc.utobj().sendKeys(driver, pobj.confirmPassword, password);
				fc.utobj().selectValFromMultiSelect(driver, pobj.selectRole, roleName);
				fc.utobj().selectDropDown(driver, pobj.timeZone, dataSet.get("Frantimezone"));
				fc.utobj().selectDropDown(driver, pobj.userType, dataSet.get("Franusertype"));

				fc.utobj().sendKeys(driver, pobj.city, dataSet.get("Francity"));
				fc.utobj().selectDropDown(driver, pobj.country, dataSet.get("Francountry"));

				fc.utobj().selectDropDown(driver, pobj.state, dataSet.get("Franstate"));

				fc.utobj().sendKeys(driver, pobj.phone1, dataSet.get("Franphone1"));
				fc.utobj().sendKeys(driver, pobj.phoneExt1, dataSet.get("Franphoneext1"));

				fc.utobj().sendKeys(driver, pobj.email, emailId);

				fc.utobj().clickElement(driver, pobj.submit);

				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException("was not able to add Franchise User Admin");

		}
		return userName;
	
	}
	
	//VipinG
	public String addFranchiseEmployeeUserwithoutchangeinsettings(WebDriver driver, String userName, String password, String franchiseId,
			String roleName, String emailId,Map<String,String> dataSet) throws Exception {



		String testCaseId = "TC_addFranchiseUserAdmin_01";
		if (fc.utobj().validate(testCaseId)) {
			try {
				/*AdminConfigurationConfigurePasswordSettingsPageTest configurePassword = new AdminConfigurationConfigurePasswordSettingsPageTest();
				configurePassword.ConfigurePasswordSettings(driver);*/

				AdminUsersManageManageFranchiseUsersPage pobj = new AdminUsersManageManageFranchiseUsersPage(driver);
				fc.adminpage().adminUsersManageFranchiseUsersPage(driver);
				fc.utobj().clickElement(driver, pobj.addFranchiseUserBtn);
				List<WebElement> list = pobj.franchiseIdMuid;
				try {
					if (fc.utobj().isSelected(driver,list.get(0))) {
					} else {
						fc.utobj().clickElement(driver, list.get(0));
					}
				} catch (Exception e) {

				}
				fc.utobj().selectDropDown(driver, pobj.franchiseeNo, franchiseId);
				fc.utobj().clickElement(driver, pobj.addUserBtn);

				String i18n = FranconnectUtil.config.get("i18Files");
				if (i18n != null && !i18n.isEmpty()) {
					fc.utobj().selectDropDown(driver, pobj.userLanguage,dataSet.get("Franemplanguage1"));
				}else {
					fc.utobj().selectDropDownByValue(driver, pobj.userLanguage, dataSet.get("Franemplanguage"));
				}

				fc.utobj().sendKeys(driver, pobj.userName, userName);
				fc.utobj().sendKeys(driver, pobj.password, password);
				fc.utobj().sendKeys(driver, pobj.confirmPassword, password);
				fc.utobj().selectValFromMultiSelect(driver, pobj.selectRole, roleName);
				fc.utobj().selectDropDown(driver, pobj.timeZone, dataSet.get("Franemptimezone"));
				fc.utobj().selectDropDown(driver, pobj.userType, dataSet.get("Franempusertype"));

				fc.utobj().sendKeys(driver, pobj.city,dataSet.get("Franempcity"));
				fc.utobj().selectDropDown(driver, pobj.country, dataSet.get("Franempcountry"));

				fc.utobj().selectDropDown(driver, pobj.state, dataSet.get("Franempstate"));

				fc.utobj().sendKeys(driver, pobj.phone1, dataSet.get("Franempphone1"));
				fc.utobj().sendKeys(driver, pobj.phoneExt1, dataSet.get("Franempphoneext1"));

				fc.utobj().sendKeys(driver, pobj.email, emailId);
				
				fc.utobj().selectDropDownByVisibleText(driver, pobj.salutation,dataSet.get("Franempsautation"));
				String firstName=fc.utobj().generateTestData(dataSet.get("Franemployeefirstname"));
				String lastName=fc.utobj().generateTestData(dataSet.get("Franemployeelastname"));
				fc.utobj().sendKeys(driver, pobj.firstName,firstName);
				fc.utobj().sendKeys(driver, pobj.lastName,lastName);

				fc.utobj().clickElement(driver, pobj.submit);

				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException("was not able to add Franchise User Admin");

		}
		return userName;
	}
	
	
	public String addFranchiseEmployeeUserwithoutchangeinsettingswithfirstnamelastname(WebDriver driver, String userName, String password, String franchiseId,
			String roleName, String emailId,Map<String,String> dataSet) throws Exception {



		String testCaseId = "TC_addFranchiseUserAdmin_01";
		if (fc.utobj().validate(testCaseId)) {
			try {
				/*AdminConfigurationConfigurePasswordSettingsPageTest configurePassword = new AdminConfigurationConfigurePasswordSettingsPageTest();
				configurePassword.ConfigurePasswordSettings(driver);*/

				AdminUsersManageManageFranchiseUsersPage pobj = new AdminUsersManageManageFranchiseUsersPage(driver);
				fc.adminpage().adminUsersManageFranchiseUsersPage(driver);
				fc.utobj().clickElement(driver, pobj.addFranchiseUserBtn);
				List<WebElement> list = pobj.franchiseIdMuid;
				try {
					if (fc.utobj().isSelected(driver,list.get(0))) {
					} else {
						fc.utobj().clickElement(driver, list.get(0));
					}
				} catch (Exception e) {

				}
				fc.utobj().selectDropDown(driver, pobj.franchiseeNo, franchiseId);
				fc.utobj().clickElement(driver, pobj.addUserBtn);

				String i18n = FranconnectUtil.config.get("i18Files");
				if (i18n != null && !i18n.isEmpty()) {
					fc.utobj().selectDropDown(driver, pobj.userLanguage,dataSet.get("Franemplanguage1"));
				}else {
					fc.utobj().selectDropDownByValue(driver, pobj.userLanguage, dataSet.get("Franemplanguage"));
				}

				fc.utobj().sendKeys(driver, pobj.userName, userName);
				fc.utobj().sendKeys(driver, pobj.password, password);
				fc.utobj().sendKeys(driver, pobj.confirmPassword, password);
				fc.utobj().selectValFromMultiSelect(driver, pobj.selectRole, roleName);
				fc.utobj().selectDropDown(driver, pobj.timeZone, dataSet.get("Franemptimezone"));
				fc.utobj().selectDropDown(driver, pobj.userType, dataSet.get("Franempusertype"));

				fc.utobj().sendKeys(driver, pobj.city,dataSet.get("Franempcity"));
				fc.utobj().selectDropDown(driver, pobj.country, dataSet.get("Franempcountry"));

				fc.utobj().selectDropDown(driver, pobj.state, dataSet.get("Franempstate"));

				fc.utobj().sendKeys(driver, pobj.phone1, dataSet.get("Franempphone1"));
				fc.utobj().sendKeys(driver, pobj.phoneExt1, dataSet.get("Franempphoneext1"));

				fc.utobj().sendKeys(driver, pobj.email, emailId);
				
				fc.utobj().selectDropDownByVisibleText(driver, pobj.salutation,dataSet.get("Franempsautation"));
				//String firstName=fc.utobj().generateTestData(dataSet.get("Franemployeefirstname"));
				//String lastName=fc.utobj().generateTestData(dataSet.get("Franemployeelastname"));
				fc.utobj().sendKeys(driver, pobj.firstName,userName);
				fc.utobj().sendKeys(driver, pobj.lastName,userName);

				fc.utobj().clickElement(driver, pobj.submit);

				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException("was not able to add Franchise User Admin");

		}
		return userName;
	}
	
}
