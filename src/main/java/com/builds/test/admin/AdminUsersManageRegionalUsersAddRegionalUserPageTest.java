package com.builds.test.admin;

import java.util.HashMap;
import java.util.Map;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Reporter;
import org.testng.annotations.Test;

import com.builds.test.fs.AddLeadFromAllSources;
import com.builds.test.salesTest.Retry;
import com.builds.uimaps.admin.AdminUsersManageCorporateUsersAddCorporateUserPage;
import com.builds.uimaps.admin.AdminUsersManageManageFranchiseUsersPage;
import com.builds.uimaps.fs.SearchUI;
import com.builds.utilities.FranconnectUtil;
import com.builds.utilities.TestCase;

public class AdminUsersManageRegionalUsersAddRegionalUserPageTest {
	FranconnectUtil fc = new FranconnectUtil();

	@Test(groups = { "sales", "salesRegMod" , "TC_Admin_User_002" }) // Verified // Akshat
	@TestCase(createdOn = "2017-10-16", updatedOn = "2017-10-16", testCaseId = "TC_Admin_User_002", testCaseDescription = " Verify the Regional User is getting added, modified and deleted.")
	public void verifyRegionalUserModify() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("sales", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			String userName = fc.utobj().generateTestData(dataSet.get("corpUser"));
			String regionName = fc.utobj().generateTestData(dataSet.get("regionName"));
			String userNameNew = fc.utobj().generateTestData(dataSet.get("corpUserNew"));
			String emailId = "salesautomation@staffex.com";

			fc.utobj().printTestStep("Admin > Users > Manage Regional Users > Add User > Fill Details and Save");
			AdminUsersManageCorporateUsersAddCorporateUserPage pobj = new AdminUsersManageCorporateUsersAddCorporateUserPage(
					driver);
			addRegionalUser(driver, userName, regionName, emailId);
			fc.utobj().printTestStep("Validate the User is added.");
			fc.utobj().sendKeys(driver, pobj.searchCorpUser, userName);
			fc.utobj().clickElement(driver, pobj.searchCorpUserBtn);
			boolean isRegUserAdded = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//*[contains(text(),'" + userName + "')]");
			if (isRegUserAdded == false) {
				fc.utobj().throwsException("Regional user not getting added");
			}
			fc.utobj().printTestStep(" Now modify the user info > Save");
			fc.utobj().actionImgOption(driver, userName, "Modify");
			fc.utobj().sendKeys(driver, pobj.firstName, userNameNew);
			fc.utobj().sendKeys(driver, pobj.lastName, userNameNew);
			fc.utobj().clickElement(driver, pobj.submit);

			fc.utobj().printTestStep(" Validate the User Info is getting modified.");
			fc.utobj().sendKeys(driver, pobj.searchCorpUser, userNameNew);
			fc.utobj().clickElement(driver, pobj.searchCorpUserBtn);
			Thread.sleep(2000);
			isRegUserAdded = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//*[contains(text(),'" + userNameNew + "')]");
			if (isRegUserAdded == false) {
				fc.utobj().throwsException("Regional user not getting Modified");
			}

			fc.utobj().printTestStep("Delete the User");
			fc.utobj().actionImgOption(driver, userNameNew, "Delete");
			fc.utobj().acceptAlertBox(driver);

			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().clickElement(driver, pobj.Close_Input_ByValue);
			Thread.sleep(2000);
			fc.utobj().printTestStep(" Validate the user is deleted from the user summary page.");
			isRegUserAdded = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//*[contains(text(),'" + userNameNew + "')]");
			if (isRegUserAdded == true) {
				fc.utobj().throwsException("Regional user not getting Deleted");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}

	}

	@Test(/*retryAnalyzer = Retry.class ,*/ groups = { "sales" , "TC_Sales_RegionalUser_002" }) // Verified : PASSED 
	@TestCase(createdOn = "2017-10-16", updatedOn = "2017-10-16", testCaseId = "TC_Sales_RegionalUser_002", testCaseDescription = " Verify the Regional User is getting added, modified and deleted.")
	public void verifyRegionalUserLeadsView() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("sales", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			AddLeadFromAllSources addLead = new AddLeadFromAllSources();
			AdminAreaRegionAddAreaRegionPageTest regionObj = new AdminAreaRegionAddAreaRegionPageTest();
			SearchUI search_page = new SearchUI(driver);

			String userName = fc.utobj().generateTestData(dataSet.get("corpUser"));
			String regionName = fc.utobj().generateTestData(dataSet.get("regionName"));
			String emailId = "salesautomation@staffex.com";
			String password = "T0n1ght123";
			String roleName = "Default Regional Role";
			String state = dataSet.get("state");
			String zipCode = fc.utobj().generateRandomNumber6Digit();
			boolean isLeadVerified = false;

			fc.utobj().printTestStep("Create Region");
			fc.utobj().printTestStep("Create Regional User with the region (With zip code)");
			regionObj.addAreaRegionWithZipCode(driver, regionName, zipCode);
			addRegionalUser(driver, userName, password, regionName, emailId);

			fc.sales().sales_common().fsModule(driver);
			String firstName = fc.utobj().generateTestData(dataSet.get("firstName"));
			String lastName = fc.utobj().generateTestData(dataSet.get("lastName"));
			String firstName2 = fc.utobj().generateTestData(dataSet.get("firstName2"));
			String lastName2 = fc.utobj().generateTestData(dataSet.get("lastName2"));
			String leadFullName = firstName + " " + lastName;
			String leadFullName2 = firstName2 + " " + lastName2;
			String country = dataSet.get("country");
			String leadOwner = userName;
			String leadSourceCategory = "Import";
			String leadSourceDetails = "None";

			fc.utobj().printTestStep("Add a lead with this region and user should be different.");
			Map<String, String> leadInfo = new HashMap<String, String>();
			leadInfo.put("firstName", firstName);
			leadInfo.put("lastName", lastName);
			leadInfo.put("country", country);
			leadInfo.put("state", state);
			leadInfo.put("email", emailId);
			leadInfo.put("zipCode", zipCode);
			leadInfo.put("leadOwner", "Based on Assignment Rules");
			leadInfo.put("leadSourceCategory", leadSourceCategory);
			leadInfo.put("leadSourceDetails", leadSourceDetails);
			addLead.addLeadFromSystem(driver, leadInfo);

			fc.utobj().printTestStep("Add another lead assigned to this user");
			Map<String, String> leadInfo2 = new HashMap<String, String>();
			leadInfo2.put("firstName", firstName2);
			leadInfo2.put("lastName", lastName2);
			leadInfo2.put("country", country);
			leadInfo2.put("state", state);
			leadInfo2.put("email", emailId);
			leadInfo2.put("leadOwner", leadOwner);
			leadInfo2.put("leadSourceCategory", leadSourceCategory);
			leadInfo2.put("leadSourceDetails", leadSourceDetails);
			addLead.addLeadFromSystem(driver, leadInfo2);

			fc.utobj().printTestStep(" login with the regional user");
			fc.home_page().logout(driver);
			fc.loginpage().loginWithParameter(driver, userName, "T0n1ght123");
			fc.utobj().printTestStep("Through SOLR search verify that the lead is visible to this lead.");
			boolean isSearchTrue = false;
			for (int i = 0; i < 3; i++) {
				if (isSearchTrue == false) {
					fc.utobj().sendKeys(driver, search_page.topSearchField, leadFullName);
					Thread.sleep(3000);
					if (i == 2) {
						Thread.sleep(2000);
					}
					fc.utobj().clickEnterOnElement(driver, search_page.topSearchField);
					fc.utobj().clickElementByJS(driver, fc.utobj().getElementByXpath(driver, ".//*[contains(text(),'Sales') and @class='link-style ellipsis ng-binding']/ancestor::a"));
					// fc.utobj().clickElement(driver, ".//span[contains(text(),'Sales')]");

					try {
						fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
								".//custom[contains(text(),'" + leadFullName + "')]"));
						isSearchTrue = true;
					} catch (Exception e) {
						Reporter.log(e.getMessage());
						System.out.println(e.getMessage());
					}
				}
			}

			if (isSearchTrue == false) {
				fc.utobj().throwsException("was not able to search Lead through Top Search");
			}
			isSearchTrue = false;
			for (int i = 0; i < 3; i++) {
				if (isSearchTrue == false) {
					fc.utobj().sendKeys(driver, search_page.topSearchField, leadFullName2);
					Thread.sleep(3000);
					if (i == 2) {
						Thread.sleep(2000);
					}
					fc.utobj().clickEnterOnElement(driver, search_page.topSearchField);
					fc.utobj().clickElementByJS(driver, fc.utobj().getElementByXpath(driver, ".//*[contains(text(),'Sales') and @class='link-style ellipsis ng-binding']/ancestor::a"));
					fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//span/*[contains(text(),'" + leadFullName2 + "')]"));
					isSearchTrue = true;
					isLeadVerified = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//*[contains(text(),'" + firstName2 + "')]");

				}
			}

			if (isSearchTrue == false) {
				fc.utobj().throwsException("was not able to search Lead through Top Search");
			}
			if (isLeadVerified = false) {
				fc.utobj().throwsException("Lead not verified at primary info page");
			}
			
			System.out.println("Done");
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}

	}

	@Test(groups = { "sales_old" })
	@TestCase(createdOn = "2017-10-27", updatedOn = "2017-10-27", testCaseId = "TC_Sales_RegionalUser_001", testCaseDescription = " Verify the Regional User is getting added, modified and deleted.")
	public void verifyRegionalUserSateLeadsView() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("sales", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			AddLeadFromAllSources addLead = new AddLeadFromAllSources();
			AdminAreaRegionAddAreaRegionPageTest regionObj = new AdminAreaRegionAddAreaRegionPageTest();
			SearchUI search_page = new SearchUI(driver);

			String userName = fc.utobj().generateTestData(dataSet.get("corpUser"));
			String regionName = fc.utobj().generateTestData(dataSet.get("regionName"));
			String emailId = "salesautomation@staffex.com";
			String password = "T0n1ght123";
			String roleName = "Default Regional Role";
			String state = dataSet.get("state");
			String country = dataSet.get("country");
			boolean isLeadVerified = false;
			String zipCode = fc.utobj().generateRandomNumber6Digit();

			fc.utobj().printTestStep("Create Region");
			fc.utobj().printTestStep(" Create Regional User with the region (with state)");
			regionObj.addAreaRegionWithZipCode(driver, regionName, zipCode);
			// regionObj.addAreaRegionWithState(driver, regionName, country,
			// state);
			addRegionalUser(driver, userName, password, regionName, emailId);

			fc.sales().sales_common().fsModule(driver);
			String firstName = fc.utobj().generateTestData(dataSet.get("firstName"));
			String lastName = fc.utobj().generateTestData(dataSet.get("lastName"));
			String firstName2 = fc.utobj().generateTestData(dataSet.get("firstName2"));
			String lastName2 = fc.utobj().generateTestData(dataSet.get("lastName2"));
			String leadFullName = firstName + " " + lastName;
			String leadFullName2 = firstName2 + " " + lastName2;
			String leadCountry = "USA";
			String leadState = "Alabama";
			String leadOwner = userName;
			String leadSourceCategory = "Import";
			String leadSourceDetails = "None";

			fc.utobj().printTestStep("Add a lead with this region and user should be different.");
			Map<String, String> leadInfo = new HashMap<String, String>();
			leadInfo.put("firstName", firstName);
			leadInfo.put("lastName", lastName);
			leadInfo.put("country", country);
			leadInfo.put("state", leadState);
			leadInfo.put("email", emailId);
			leadInfo.put("zipCode", zipCode);
			leadInfo.put("leadOwner", "Based on Assignment Rules");
			leadInfo.put("leadSourceCategory", leadSourceCategory);
			leadInfo.put("leadSourceDetails", leadSourceDetails);
			addLead.addLeadFromSystem(driver, leadInfo);

			fc.utobj().printTestStep(" Add another lead assigned to this user");
			Map<String, String> leadInfo2 = new HashMap<String, String>();
			leadInfo2.put("firstName", firstName2);
			leadInfo2.put("lastName", lastName2);
			leadInfo2.put("country", country);
			leadInfo2.put("state", leadState);
			leadInfo2.put("email", emailId);
			leadInfo2.put("zipCode", zipCode);
			leadInfo2.put("leadOwner", leadOwner);
			leadInfo2.put("leadSourceCategory", leadSourceCategory);
			leadInfo2.put("leadSourceDetails", leadSourceDetails);
			addLead.addLeadFromSystem(driver, leadInfo2);

			fc.utobj().printTestStep(" login with the regional user");
			fc.home_page().logout(driver);
			fc.loginpage().loginWithParameter(driver, userName, "T0n1ght123");
			fc.utobj().printTestStep(" Through SOLR search verify that the lead is visible to this lead.");
			boolean isSearchTrue = false;
			for (int i = 0; i < 3; i++) {
				if (isSearchTrue == false) {
					fc.utobj().sendKeys(driver, search_page.topSearchField, firstName);
					Thread.sleep(3000);
					if (i == 2) {
						Thread.sleep(2000);
					}
					fc.utobj().clickEnterOnElement(driver, search_page.topSearchField);

					try {
						fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
								".//span/*[contains(text(),'" + leadFullName + "')]"));
						isSearchTrue = true;
					} catch (Exception e) {
					}
				}
			}

			if (isSearchTrue == false) {
				fc.utobj().throwsException("was not able to search Lead assigned with same ZipCode through Top Search");
			}
			isLeadVerified = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//*[contains(text(),'" + firstName + "')]");
			isSearchTrue = false;
			for (int i = 0; i < 3; i++) {
				if (isSearchTrue == false) {
					fc.utobj().sendKeys(driver, search_page.topSearchField, leadFullName2);
					Thread.sleep(3000);
					if (i == 2) {
						Thread.sleep(2000);
					}
					fc.utobj().clickEnterOnElement(driver, search_page.topSearchField);

					try {
						fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
								".//span/*[contains(text(),'" + leadFullName2 + "')]"));
						isSearchTrue = true;
					} catch (Exception e) {
					}
				}
			}

			if (isSearchTrue == false) {
				fc.utobj().throwsException(
						"was not able to search Lead assigned to logged in Regional user through Top Search");
			}
			isLeadVerified = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//*[contains(text(),'" + firstName2 + "')]");
			if (isLeadVerified = false) {
				fc.utobj().throwsException("Lead not verified at primary info page");
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}

	}

	@Test(groups = { "sales" , "TC_Sales_RegionalUser_003" , "sales_re" }) // Fixed : Verified // Akshat // Edited : Fixed
	@TestCase(createdOn = "2017-10-27", updatedOn = "2017-10-27", testCaseId = "TC_Sales_RegionalUser_003", testCaseDescription = "Verify Regional User Leads as per the view")
	public void verifyRegionalUserCountyLeadsView() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("sales", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			AddLeadFromAllSources addLead = new AddLeadFromAllSources();
			AdminAreaRegionAddAreaRegionPageTest regionObj = new AdminAreaRegionAddAreaRegionPageTest();
			SearchUI search_page = new SearchUI(driver);

			String userName = fc.utobj().generateTestData(dataSet.get("corpUser"));
			String regionName = fc.utobj().generateTestData(dataSet.get("regionName"));
			String emailId = "salesautomation@staffex.com";
			String password = "T0n1ght123";
			String roleName = "Default Regional Role";
			String county = dataSet.get("county");
			String country = dataSet.get("country");
			boolean isLeadVerified = false;
			String zipCode = fc.utobj().generateRandomNumber6Digit();

			fc.utobj().printTestStep("Create Region");
			fc.utobj().printTestStep("Create Regional User with the region (With county)");
			regionObj.addAreaRegionWithZipCode(driver, regionName, zipCode);
			// regionObj.addAreaRegionWithCounty(driver, regionName);
			addRegionalUser(driver, userName, password, regionName, emailId);

			fc.sales().sales_common().fsModule(driver);
			String firstName = fc.utobj().generateTestData(dataSet.get("firstName"));
			String lastName = fc.utobj().generateTestData(dataSet.get("lastName"));
			String firstName2 = fc.utobj().generateTestData(dataSet.get("firstName2"));
			String lastName2 = fc.utobj().generateTestData(dataSet.get("lastName2"));
			String leadFullName = firstName + " " + lastName;
			String leadFullName2 = firstName2 + " " + lastName2;
			String leadCountry = "USA";
			String leadState = "Alabama";
			String leadOwner = userName;
			String leadSourceCategory = "Import";
			String leadSourceDetails = "None";

			fc.utobj().printTestStep(" Add a lead with this region and user should be different.");
			Map<String, String> leadInfo = new HashMap<String, String>();
			leadInfo.put("firstName", firstName);
			leadInfo.put("lastName", lastName);
			leadInfo.put("country", country);
			leadInfo.put("state", leadState);
			leadInfo.put("county", "Bibb");
			leadInfo.put("email", emailId);
			leadInfo.put("zipCode", zipCode);
			leadInfo.put("leadOwner", "Based on Assignment Rules");
			leadInfo.put("leadSourceCategory", leadSourceCategory);
			leadInfo.put("leadSourceDetails", leadSourceDetails);
			addLead.addLeadFromSystem(driver, leadInfo);

			fc.utobj().printTestStep(" Add another lead assigned to this user");
			Map<String, String> leadInfo2 = new HashMap<String, String>();
			leadInfo2.put("firstName", firstName2);
			leadInfo2.put("lastName", lastName2);
			leadInfo2.put("country", country);
			leadInfo2.put("state", leadState);
			leadInfo.put("county", "Bibb");
			leadInfo2.put("email", emailId);
			leadInfo2.put("zipCode", zipCode);
			leadInfo2.put("leadOwner", leadOwner);
			leadInfo2.put("leadSourceCategory", leadSourceCategory);
			leadInfo2.put("leadSourceDetails", leadSourceDetails);
			addLead.addLeadFromSystem(driver, leadInfo2);

			fc.utobj().printTestStep("login with the regional user");
			fc.home_page().logout(driver);
			fc.loginpage().loginWithParameter(driver, userName, "T0n1ght123");
			fc.utobj().printTestStep("Through SOLR search verify that the lead is visible to this lead.");
			boolean isSearchTrue = false;
			for (int i = 0; i < 3; i++) {
				if (isSearchTrue == false) {
					fc.utobj().sendKeys(driver, search_page.topSearchField, firstName);
					Thread.sleep(3000);
					if (i == 2) {
						Thread.sleep(2000);
					}
					fc.utobj().clickEnterOnElement(driver, search_page.topSearchField);
					fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='sales']"));

					try {
						fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
								".//span/*[contains(text(),'" + leadFullName + "')]"));
						isSearchTrue = true;
					} catch (Exception e) {
					}
				}
			}

			if (isSearchTrue == false) {
				fc.utobj().throwsException("was not able to search Lead 1 through Top Search");
			}
			isLeadVerified = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//*[contains(text(),'" + firstName + "')]");
			isSearchTrue = false;
			for (int i = 0; i < 3; i++) {
				if (isSearchTrue == false) {
					fc.utobj().sendKeys(driver, search_page.topSearchField, leadFullName2);
					Thread.sleep(3000);
					if (i == 2) {
						Thread.sleep(2000);
					}
					fc.utobj().clickEnterOnElement(driver, search_page.topSearchField);
					fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='sales']"));

					try {
						fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
								".//span/*[contains(text(),'" + leadFullName2 + "')]"));
						isSearchTrue = true;
					} catch (Exception e) {
					}
				}
			}

			if (isSearchTrue == false) {
				fc.utobj().throwsException("was not able to search Lead 2 through Top Search");
			}
			isLeadVerified = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//*[contains(text(),'" + firstName2 + "')]");
			if (isLeadVerified = false) {
				fc.utobj().throwsException("Lead not verified at primary info page");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			// e.printStackTrace();
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}

	}

	public void addRegionalUser(WebDriver driver, String userName, Map<String, String> config, String emailId)
			throws Exception {

		String testCaseId = "TC_Add_Regional_User_01";
		if (fc.utobj().validate(testCaseId)) {
			try {
				fc.adminpage().adminUsersManageRegionalUsersAddRegionalUserPage(driver);
				AdminUsersManageManageFranchiseUsersPage pobj = new AdminUsersManageManageFranchiseUsersPage(driver);
				fc.utobj().sendKeys(driver, pobj.userName, userName);
				fc.utobj().sendKeys(driver, pobj.password, "t0n1ght1");
				fc.utobj().selectValFromMultiSelect(driver, pobj.roleMultiSelectBtn, "Default Regional Role");
				Select timeZone = new Select(pobj.timeZone);
				timeZone.selectByVisibleText("GMT -06:00 US/Canada/Central");

				String i18n = FranconnectUtil.config.get("i18Files");
				if (i18n != null && !i18n.isEmpty()) {
					fc.utobj().selectDropDown(driver, pobj.userLanguage, "French");
				}else {
					fc.utobj().selectDropDownByValue(driver, pobj.userLanguage, "en");
				}

				pobj.firstName.sendKeys("Reg_FirstN");
				pobj.lastName.sendKeys("Reg_LastN");
				pobj.city.sendKeys("11");
				Select country = new Select(pobj.country);
				country.selectByVisibleText("USA");
				Select state = new Select(pobj.state);
				state.selectByVisibleText("Alabama");
				pobj.phone1.sendKeys("1234567890");
				pobj.phoneExt1.sendKeys("2");
				pobj.email.sendKeys(emailId);
				pobj.submit.click();

				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException("was not able to add Regional User");

		}
	}

	public void addRegionalUser(WebDriver driver, String userName, String password, String regionName, String emailId)
			throws Exception {

		String testCaseId = "TC_Add_Regional_User_02";
		if (fc.utobj().validate(testCaseId)) {
			try {
				fc.adminpage().adminUsersManageRegionalUsersAddRegionalUserPage(driver);
				AdminUsersManageManageFranchiseUsersPage pobj = new AdminUsersManageManageFranchiseUsersPage(driver);

				fc.utobj().sendKeys(driver, pobj.userName, userName);
				fc.utobj().sendKeys(driver, pobj.password, password);
				fc.utobj().sendKeys(driver, pobj.confirmPassword, password);
				fc.utobj().selectValFromMultiSelect(driver, pobj.roleMultiSelectBtn, "Default Regional Role");

				fc.utobj().selectDropDown(driver, pobj.timeZone, "GMT -06:00 US/Canada/Central");

				String i18n = FranconnectUtil.config.get("i18Files");
				if (i18n != null && !i18n.isEmpty()) {
					fc.utobj().selectDropDown(driver, pobj.userLanguage, "French");
				}else {
					fc.utobj().selectDropDownByValue(driver, pobj.userLanguage, "en");
				}

				fc.utobj().selectDropDown(driver, pobj.region, regionName);
				fc.utobj().sendKeys(driver, pobj.firstName, userName);
				fc.utobj().sendKeys(driver, pobj.lastName, userName);
				fc.utobj().sendKeys(driver, pobj.city, "11");
				fc.utobj().selectDropDown(driver, pobj.country, "USA");
				fc.utobj().sleep(2000);
				fc.utobj().selectDropDown(driver, pobj.state, "Alabama");
				fc.utobj().sendKeys(driver, pobj.phone1, "1234567890");
				fc.utobj().sendKeys(driver, pobj.phoneExt1, "22");
				fc.utobj().sendKeys(driver, pobj.email, emailId);
				pobj.submit.click();

				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException("was not able to add Regional User");

		}
	}

	public void addRegionalUser(WebDriver driver, String userName, String regionName, String emailId) throws Exception {

		String testCaseId = "TC_Add_Regional_User_03";
		if (fc.utobj().validate(testCaseId)) {
			try {
				AdminAreaRegionAddAreaRegionPageTest p1 = new AdminAreaRegionAddAreaRegionPageTest();
				p1.addAreaRegion(driver, regionName);

				fc.adminpage().adminUsersManageRegionalUsersAddRegionalUserPage(driver);
				AdminUsersManageManageFranchiseUsersPage pobj = new AdminUsersManageManageFranchiseUsersPage(driver);

				fc.utobj().sendKeys(driver, pobj.userName, userName);
				fc.utobj().sendKeys(driver, pobj.password, "T0n1ght1");
				fc.utobj().sendKeys(driver, pobj.confirmPassword, "T0n1ght1");
				fc.utobj().selectValFromMultiSelect(driver, pobj.roleMultiSelectBtn, "Default Regional Role");
				fc.utobj().selectDropDown(driver, pobj.timeZone, "GMT -06:00 US/Canada/Central");

				String i18n = FranconnectUtil.config.get("i18Files");
				if (i18n != null && !i18n.isEmpty()) {
					fc.utobj().selectDropDown(driver, pobj.userLanguage, "French");
				}else {
					fc.utobj().selectDropDownByValue(driver, pobj.userLanguage, "en");
				}
				fc.utobj().selectDropDown(driver, pobj.region, regionName);
				fc.utobj().sendKeys(driver, pobj.firstName, userName);
				fc.utobj().sendKeys(driver, pobj.lastName, userName);
				fc.utobj().sendKeys(driver, pobj.city, "11");
				fc.utobj().selectDropDown(driver, pobj.country, "USA");

				fc.utobj().selectDropDown(driver, pobj.state, "Alabama");
				fc.utobj().sendKeys(driver, pobj.phone1, "1234567890");
				fc.utobj().sendKeys(driver, pobj.phoneExt1, "22");
				fc.utobj().sendKeys(driver, pobj.email, emailId);
				fc.utobj().clickElement(driver, pobj.submit);

				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException("was not able to add Regional User");

		}
	}

	public String addRegionalUserWithRole(WebDriver driver, String userName, String password, String regionName,
			String roleName, String emailId) throws Exception {

		try {
			AdminAreaRegionAddAreaRegionPageTest p1 = new AdminAreaRegionAddAreaRegionPageTest();
			p1.addAreaRegion(driver, regionName);

			fc.adminpage().adminUsersManageRegionalUsersAddRegionalUserPage(driver);
			AdminUsersManageManageFranchiseUsersPage pobj = new AdminUsersManageManageFranchiseUsersPage(driver);

			fc.utobj().sendKeys(driver, pobj.userName, userName);
			fc.utobj().sendKeys(driver, pobj.password, password);
			fc.utobj().sendKeys(driver, pobj.confirmPassword, password);
			fc.utobj().selectValFromMultiSelect(driver, pobj.selectRole, roleName);
			fc.utobj().selectDropDown(driver, pobj.timeZone, "GMT -06:00 US/Canada/Central");

			String i18n = FranconnectUtil.config.get("i18Files");
			if (i18n != null && !i18n.isEmpty()) {
				fc.utobj().selectDropDown(driver, pobj.userLanguage, "French");
			} else {
				fc.utobj().selectDropDownByValue(driver, pobj.userLanguage, "en");
			}
			fc.utobj().selectDropDown(driver, pobj.region, regionName);
			fc.utobj().sendKeys(driver, pobj.firstName, userName);
			fc.utobj().sendKeys(driver, pobj.lastName, userName);
			fc.utobj().sendKeys(driver, pobj.city, "11");
			fc.utobj().selectDropDown(driver, pobj.country, "USA");
			fc.utobj().selectDropDown(driver, pobj.state, "Alabama");
			fc.utobj().sendKeys(driver, pobj.phone1, "1234567890");
			fc.utobj().sendKeys(driver, pobj.email, emailId);
			fc.utobj().clickElement(driver, pobj.submit);
		} catch (Exception e) {
		}
		return userName = userName + " " + userName;
	}

	public void addRegionalUserWithRoleState(WebDriver driver, String userName, String password, String regionName,
			String roleName, Map<String, String> config, String emailId, String state) throws Exception {

		String testCaseId = "TC_Add_Regional_User_With_Role_02";
		if (fc.utobj().validate(testCaseId)) {
			try {
				AdminAreaRegionAddAreaRegionPageTest p1 = new AdminAreaRegionAddAreaRegionPageTest();
				p1.addAreaRegion(driver, regionName);

				fc.adminpage().adminUsersManageRegionalUsersAddRegionalUserPage(driver);
				AdminUsersManageManageFranchiseUsersPage pobj = new AdminUsersManageManageFranchiseUsersPage(driver);

				fc.utobj().sendKeys(driver, pobj.userName, userName);
				fc.utobj().sendKeys(driver, pobj.password, password);
				fc.utobj().sendKeys(driver, pobj.confirmPassword, password);
				fc.utobj().selectValFromMultiSelect(driver, pobj.selectRole, roleName);
				fc.utobj().selectDropDown(driver, pobj.timeZone, "GMT -06:00 US/Canada/Central");
				String i18n = FranconnectUtil.config.get("i18Files");
				if (i18n != null && !i18n.isEmpty()) {
					fc.utobj().selectDropDown(driver, pobj.userLanguage, "French");
				}else {
					fc.utobj().selectDropDownByValue(driver, pobj.userLanguage, "en");
				}

				fc.utobj().selectDropDown(driver, pobj.region, regionName);
				fc.utobj().sendKeys(driver, pobj.firstName, userName);
				fc.utobj().sendKeys(driver, pobj.lastName, userName);
				fc.utobj().sendKeys(driver, pobj.city, "11");
				fc.utobj().selectDropDown(driver, pobj.country, "United Kingdom");
				fc.utobj().selectDropDown(driver, pobj.state, state);
				fc.utobj().sendKeys(driver, pobj.phone1, "1234567890");
				fc.utobj().sendKeys(driver, pobj.email, emailId);
				fc.utobj().clickElement(driver, pobj.submit);

				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException("Was not able to add Regional User With Role");

		}
	}

	public void addRegionalUserZC(WebDriver driver, String userName, String regionName, Map<String, String> config,
			String emailId) throws Exception {

		String testCaseId = "TC_Add_Regional_User_03";
		try {
			AdminAreaRegionAddAreaRegionPageTest p1 = new AdminAreaRegionAddAreaRegionPageTest();
			p1.addAreaRegion(driver, regionName);

			fc.adminpage().adminUsersManageRegionalUsersAddRegionalUserPage(driver);
			AdminUsersManageManageFranchiseUsersPage pobj = new AdminUsersManageManageFranchiseUsersPage(driver);

			fc.utobj().sendKeys(driver, pobj.userName, userName);
			fc.utobj().sendKeys(driver, pobj.password, "T0n1ght1");
			fc.utobj().sendKeys(driver, pobj.confirmPassword, "T0n1ght1");
			fc.utobj().selectValFromMultiSelect(driver, pobj.roleMultiSelectBtn, "Default Regional Role");

			String i18n = FranconnectUtil.config.get("i18Files");
			if (i18n != null && !i18n.isEmpty()) {
				fc.utobj().selectDropDown(driver, pobj.userLanguage, "French");
			}else {
				fc.utobj().selectDropDownByValue(driver, pobj.userLanguage, "en");
			}

			fc.utobj().selectDropDown(driver, pobj.timeZone, "GMT -06:00 US/Canada/Central");
			fc.utobj().selectDropDown(driver, pobj.region, regionName);

			fc.utobj().sendKeys(driver, pobj.firstName, userName);
			fc.utobj().sendKeys(driver, pobj.lastName, userName);
			fc.utobj().sendKeys(driver, pobj.city, "11");
			fc.utobj().selectDropDown(driver, pobj.country, "USA");
			fc.utobj().selectDropDown(driver, pobj.state, "Alabama");
			fc.utobj().sendKeys(driver, pobj.phone1, "1234567890");
			fc.utobj().sendKeys(driver, pobj.phoneExt1, "22");
			fc.utobj().sendKeys(driver, pobj.email, emailId);
			fc.utobj().clickElement(driver, pobj.submit);
			fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
		}
	}

	public void addRegionalUserZCWithRegionNameExist(WebDriver driver, String userName, String regionName,
			Map<String, String> config, String emailId) throws Exception {
		String testCaseId = "TC_Add_Regional_User_04";
		try {
			fc.adminpage().adminUsersManageRegionalUsersAddRegionalUserPage(driver);
			AdminUsersManageManageFranchiseUsersPage pobj = new AdminUsersManageManageFranchiseUsersPage(driver);

			fc.utobj().sendKeys(driver, pobj.userName, userName);
			fc.utobj().sendKeys(driver, pobj.password, "T0n1ght1");
			fc.utobj().sendKeys(driver, pobj.confirmPassword, "T0n1ght1");
			fc.utobj().selectValFromMultiSelect(driver, pobj.roleMultiSelectBtn, "Default Regional Role");
			fc.utobj().selectDropDown(driver, pobj.timeZone, "GMT -06:00 US/Canada/Central");
			String i18n = FranconnectUtil.config.get("i18Files");
			if (i18n != null && !i18n.isEmpty()) {
				fc.utobj().selectDropDown(driver, pobj.userLanguage, "French");
			}else {
				fc.utobj().selectDropDownByValue(driver, pobj.userLanguage, "en");
			}

			fc.utobj().selectDropDown(driver, pobj.region, regionName);
			fc.utobj().sendKeys(driver, pobj.firstName, userName);
			fc.utobj().sendKeys(driver, pobj.lastName, userName);
			fc.utobj().sendKeys(driver, pobj.city, "11");
			fc.utobj().selectDropDown(driver, pobj.country, "USA");
			fc.utobj().selectDropDown(driver, pobj.state, "Alabama");
			fc.utobj().sendKeys(driver, pobj.phone1, "1234567890");
			fc.utobj().sendKeys(driver, pobj.phoneExt1, "22");
			fc.utobj().sendKeys(driver, pobj.email, emailId);
			fc.utobj().clickElement(driver, pobj.submit);
			fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
		}

	}

	public void addRegionalUserwithunamepass(WebDriver driver, String userName, String regionName,
			String password,Map<String, String> dataSet, String role) throws Exception {


		String testCaseId = "TC_Add_Regional_User_03";
		if (fc.utobj().validate(testCaseId)) {
			try {
				AdminAreaRegionAddAreaRegionPageTest p1 = new AdminAreaRegionAddAreaRegionPageTest();
				p1.addAreaRegionwithSQlite(driver, regionName,dataSet);

				fc.adminpage().adminUsersManageRegionalUsersAddRegionalUserPage(driver);
				AdminUsersManageManageFranchiseUsersPage pobj = new AdminUsersManageManageFranchiseUsersPage(driver);

				fc.utobj().sendKeys(driver, pobj.userName, userName);
				fc.utobj().sendKeys(driver, pobj.password, password);
				fc.utobj().sendKeys(driver, pobj.confirmPassword, password);
				//fc.utobj().selectValFromMultiSelect(driver, pobj.roleMultiSelectBtn, dataSet.get("Regrole"));
				fc.utobj().selectValFromMultiSelect(driver, pobj.roleMultiSelectBtn,role);
				fc.utobj().selectDropDown(driver, pobj.timeZone, dataSet.get("Regtimezone"));

				String i18n = FranconnectUtil.config.get("i18Files");
				if (i18n != null && !i18n.isEmpty()) {
					fc.utobj().selectDropDown(driver, pobj.userLanguage,dataSet.get("Reglanguage1"));
				}else {
					fc.utobj().selectDropDownByValue(driver, pobj.userLanguage,dataSet.get("Reglanguage"));
				}
				fc.utobj().selectDropDown(driver, pobj.region, regionName);
				fc.utobj().sendKeys(driver, pobj.firstName, userName);
				fc.utobj().sendKeys(driver, pobj.lastName, userName);
				fc.utobj().sendKeys(driver, pobj.city,dataSet.get("Regcity"));
				fc.utobj().selectDropDown(driver, pobj.country,dataSet.get("Regcounty"));

				fc.utobj().selectDropDown(driver, pobj.state,dataSet.get("Regstate"));
				fc.utobj().sendKeys(driver, pobj.phone1,dataSet.get("Regphone1"));
				fc.utobj().sendKeys(driver, pobj.phoneExt1,dataSet.get("phoneExt1"));
				String emailId="crmautomation@staffex.com";//fc.utobj().generateTestData(dataSet.get("Regemail"))+"@staffex.com";
				fc.utobj().sendKeys(driver, pobj.email, emailId);
				fc.utobj().clickElement(driver, pobj.submit);

				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException("was not able to add Regional User");

		}
	
	}

	public void addRegionalUserwithunamepasswithoutaddingarea(WebDriver driver, String userName, String regionName,
			String password,Map<String, String> dataSet, String role) throws Exception {



		String testCaseId = "TC_Add_Regional_User_03";
		if (fc.utobj().validate(testCaseId)) {
			try {
				
				fc.adminpage().adminUsersManageRegionalUsersAddRegionalUserPage(driver);
				AdminUsersManageManageFranchiseUsersPage pobj = new AdminUsersManageManageFranchiseUsersPage(driver);

				fc.utobj().sendKeys(driver, pobj.userName, userName);
				fc.utobj().sendKeys(driver, pobj.password, password);
				fc.utobj().sendKeys(driver, pobj.confirmPassword, password);
				//fc.utobj().selectValFromMultiSelect(driver, pobj.roleMultiSelectBtn, dataSet.get("Regrole"));
				fc.utobj().selectValFromMultiSelect(driver, pobj.roleMultiSelectBtn,role);
				fc.utobj().selectDropDown(driver, pobj.timeZone, dataSet.get("Regtimezone"));

				String i18n = FranconnectUtil.config.get("i18Files");
				if (i18n != null && !i18n.isEmpty()) {
					fc.utobj().selectDropDown(driver, pobj.userLanguage,dataSet.get("Reglanguage1"));
				}else {
					fc.utobj().selectDropDownByValue(driver, pobj.userLanguage,dataSet.get("Reglanguage"));
				}
				fc.utobj().selectDropDown(driver, pobj.region, regionName);
				fc.utobj().sendKeys(driver, pobj.firstName, userName);
				fc.utobj().sendKeys(driver, pobj.lastName, userName);
				fc.utobj().sendKeys(driver, pobj.city,dataSet.get("Regcity"));
				fc.utobj().selectDropDown(driver, pobj.country,dataSet.get("Regcounty"));

				fc.utobj().selectDropDown(driver, pobj.state,dataSet.get("Regstate"));
				fc.utobj().sendKeys(driver, pobj.phone1,dataSet.get("Regphone1"));
				fc.utobj().sendKeys(driver, pobj.phoneExt1,dataSet.get("phoneExt1"));
				String emailId=fc.utobj().generateTestData(dataSet.get("Regemail"))+"@staffex.com";
				fc.utobj().sendKeys(driver, pobj.email, emailId);
				fc.utobj().clickElement(driver, pobj.submit);

				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException("was not able to add Regional User");

		}
	
	
		
	}
}
