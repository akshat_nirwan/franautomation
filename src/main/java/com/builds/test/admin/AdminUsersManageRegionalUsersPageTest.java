package com.builds.test.admin;

import org.openqa.selenium.WebDriver;

import com.builds.uimaps.admin.AdminUsersManageRegionalUsersPage;
import com.builds.utilities.FranconnectUtil;

public class AdminUsersManageRegionalUsersPageTest {
	FranconnectUtil fc = new FranconnectUtil();

	public void openAddRegionalUser(WebDriver driver) throws Exception {

		AdminUsersManageRegionalUsersPage pobj = new AdminUsersManageRegionalUsersPage(driver);
		fc.utobj().clickElement(driver, pobj.addRegionalUserBtn);
	}
}
