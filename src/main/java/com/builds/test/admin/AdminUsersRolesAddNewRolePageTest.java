package com.builds.test.admin;

import java.util.List;
import java.util.Map;
import java.util.Set;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Test;

import com.builds.uimaps.admin.AdminUsersRolesAddNewRolePage;
import com.builds.utilities.FranconnectUtil;
import com.builds.utilities.TestCase;

public class AdminUsersRolesAddNewRolePageTest {
	FranconnectUtil fc = new FranconnectUtil();

	@Test(groups = { "thehub", "hub_role"})
	@TestCase(createdOn = "2018-01-02", updatedOn = "2018-01-02", testCaseDescription = "Verify Under The Hub Role section, there should be new privilege like Can Export Contacts Label and Can View Messages Label For Regional Level", testCaseId = "TC_Hub_Admin_Role_01")
	private void canExportContactsLnkRegLevel() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			AdminUsersRolesAddNewRolePage roles_page = new AdminUsersRolesAddNewRolePage(driver);

			fc.utobj().printTestStep("Navigate to Admin > Roles > Add New Role");
			fc.adminpage().adminUsersRolesAddNewRolePage(driver);

			fc.utobj().printTestStep("Select Regional Level from drop down");
			fc.utobj().selectDropDownByValue(driver, roles_page.roleType, "2");

			fc.utobj().clickElementWithoutMove(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='floatMenu']/ul/li/img"));
			fc.utobj().clickElementByJS(driver,	fc.utobj().getElementByXpath(driver, ".//*[@id='qtip-0-content']//a[contains(text() , 'Can Manage The Hub')]"));

			fc.utobj().printTestStep(
					"Verify Under The Hub Role section, there should be new privilege like Can Export Contacts Label");
			boolean isTextPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					"//a[@name='mod9']/ancestor::tr/td[2]//td[contains(text(),'Can Export Contacts')]");

			if (isTextPresent == false) {
				fc.utobj().throwsException(
						"Not able to verify that Under The Hub Role section, there should be new privilege like Can Export Contacts");
			}

			fc.utobj().printTestStep(
					"Verify Under The Hub Role section, there should be privilege like Can View Messages Label");
			boolean isTextPresent1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[@name='mod9']/ancestor::tr/td[2]//td[contains(text(),'Can View Messages')]");

			if (isTextPresent1 == false) {
				fc.utobj().throwsException(
						"Not able to verify that Under The Hub Role section, there should be privilege like Can View Messages");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "thehub", "hub_role"})
	@TestCase(createdOn = "2018-01-02", updatedOn = "2018-01-02", testCaseDescription = "Verify Under The Hub Role section, there should be new privilege like Can Export Contacts Label and Can View Messages Label For Franchise Level", testCaseId = "TC_Hub_Admin_Role_02")
	private void canExportContactsLnkFranchiseLevel() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			AdminUsersRolesAddNewRolePage roles_page = new AdminUsersRolesAddNewRolePage(driver);

			fc.utobj().printTestStep("Navigate to Admin > Roles > Add New Role");
			fc.adminpage().adminUsersRolesAddNewRolePage(driver);

			fc.utobj().printTestStep("Select Franchise Level from drop down");
			
			
			fc.utobj().selectDropDownByValue(driver, roles_page.roleType, "0");
			

			fc.utobj().printTestStep(
					"Verify Under The Hub Role section, there should be new privilege like Can Export Contacts Label");
			boolean isTextPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(@name,'mod')]/parent::*/following-sibling::td//td[contains(text(),'#')]/following-sibling::td[contains(text(),'"
							+ fc.utobj().translateString("Can Export Contacts") + "')]");

			if (isTextPresent == false) {
				fc.utobj().throwsException(
						"Not able to verify that Under The Hub Role section, there should be new privilege like Can Export Contacts");
			}

			fc.utobj().printTestStep(
					"Verify Under The Hub Role section, there should be privilege like Can View Messages Label");
			boolean isTextPresent1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(@name,'mod')]/parent::*/following-sibling::td//td[contains(text(),'#')]/following-sibling::td[contains(text(),'"
							+ fc.utobj().translateString("Can View Messages") + "')]");

			if (isTextPresent1 == false) {
				fc.utobj().throwsException(
						"Not able to verify that Under The Hub Role section, there should be privilege like Can View Messages");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "thehub", "hub_role" })
	@TestCase(createdOn = "2018-01-02", updatedOn = "2018-01-02", testCaseDescription = "Verify Under The Hub Role section, there should be new privilege like Can Download Audit Report Label , Can View Messages and Can Export Contacts for Corporate Level", testCaseId = "TC_Hub_Admin_Role_03")
	private void canDownloadAuditReportCorporateLevel() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			AdminUsersRolesAddNewRolePage roles_page = new AdminUsersRolesAddNewRolePage(driver);

			fc.utobj().printTestStep("Navigate to Admin > Roles > Add New Role");
			fc.adminpage().adminUsersRolesAddNewRolePage(driver);

			fc.utobj().printTestStep("Select Corporate Level from drop down");
			
			fc.utobj().selectDropDownByValue(driver, roles_page.roleType, "1");

			fc.utobj().printTestStep(
					"Verify Under The Hub Role section, there should be new privilege like Can Download Audit Report Label");
			boolean isTextPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text(),'#')]/following-sibling::td[contains(text(),'"
							+ fc.utobj().translateString("Can Download Audit Report") + "')]");

			if (isTextPresent == false) {
				fc.utobj().throwsException(
						"Not able to verify that Under The Hub Role section, there should be new privilege like Can Download Audit Report Label");
			}

			fc.utobj().printTestStep(
					"Verify Under The Hub Role section, there should be privilege like Can Export Contacts Label");
			boolean isTextPresent1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text(),'#')]/following-sibling::td[contains(text(),'"
							+ fc.utobj().translateString("Can View Messages") + "')]");

			if (isTextPresent1 == false) {
				fc.utobj().throwsException(
						"Not able to verify that Under The Hub Role section, there should be privilege like Can Export Contacts");
			}

			fc.utobj().printTestStep(
					"Verify Under The Hub Role section, there should be privilege like Can View Messages Label");
			boolean isTextPresent2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text(),'#')]/following-sibling::td[contains(text(),'"
							+ fc.utobj().translateString("Can Export Contacts") + "')]");

			if (isTextPresent2 == false) {
				fc.utobj().throwsException(
						"Not able to verify that Under The Hub Role section, there should be privilege like Can View Messages");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "thehub", "hub_role" })
	@TestCase(createdOn = "2018-01-03", updatedOn = "2018-01-03", testCaseDescription = "Verify On deselecting the can view Alert Privileges, Can Administer Privileges should get deselected automatically by All Users Role Type", testCaseId = "TC_Hub_Admin_Role_04")
	private void canViewAlertCorporateLevel() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			AdminUsersRolesAddNewRolePage roles_page = new AdminUsersRolesAddNewRolePage(driver);
			
			fc.utobj().printTestStep("Configure New Hierarchy Level if disabled for Division");
			AdminHiddenLinksConfigureNewHierarchyLevelPageTest hierarchy_level=new AdminHiddenLinksConfigureNewHierarchyLevelPageTest();
			hierarchy_level.ConfigureNewHierarchyLevel(driver);
			

			fc.utobj().printTestStep("Navigate to Admin > Roles > Add New Role");
			fc.adminpage().adminUsersRolesAddNewRolePage(driver);

			fc.utobj().printTestStep("Select Corporate Level from drop down");
			
			fc.utobj().selectDropDownByValue(driver, roles_page.roleType, "1");

			fc.utobj().printTestStep("Deselect the can view Alert Privileges");
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver,
							".//td[contains(text(),'#')]/following-sibling::td[contains(text(),'"
									+ fc.utobj().translateString("Can View Alerts")
									+ "')]/ancestor::tr/td/input[@id='14']"));

			fc.utobj().printTestStep("Verify Can Administer Alerts Privileges should get deselected");

			boolean isCheckd = false;
			try {
				isCheckd = fc.utobj().isSelected(driver,
						fc.utobj().getElementByXpath(driver,
								".//td[contains(text(),'#')]/following-sibling::td[contains(text(),'"
										+ fc.utobj().translateString("Can Administer Alerts")
										+ "')]/ancestor::tr/td/input[@id='14']")
						);
			} catch (Exception e) {
				isCheckd = true;
			}

			if (isCheckd) {
				fc.utobj().throwsException(
						"On deselecting the can view Alert Privileges, Can Administer Alerts Privilege is not getting deselect automatically");
			}

			fc.utobj().printTestStep("Select Division Level from drop down");
			
			fc.utobj().selectDropDownByValue(driver, roles_page.roleType, "5");

			fc.utobj().printTestStep("Deselect the can view Alert Privileges");
			
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver,
							".//td[contains(text(),'#')]/following-sibling::td[contains(text(),'"
									+ fc.utobj().translateString("Can View Alerts")
									+ "')]/ancestor::tr/td/input[@id='14']"));

			fc.utobj().printTestStep("Verify Can Administer Alerts Privileges should get deselected");

			try {
				isCheckd = fc.utobj().isSelected(driver,fc.utobj()
						.getElementByXpath(driver,
								".//td[contains(text(),'#')]/following-sibling::td[contains(text(),'"
										+ fc.utobj().translateString("Can Administer Alerts")
										+ "')]/ancestor::tr/td/input[@id='14']"));
						
			} catch (Exception e) {
				isCheckd = true;
			}

			if (isCheckd) {
				fc.utobj().throwsException(
						"On deselecting the can view Alert Privileges, Can Administer Alerts Privilege is not getting deselect automatically");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "thehub", "hub_role" })
	@TestCase(createdOn = "2018-01-03", updatedOn = "2018-01-03", testCaseDescription = "Verify After deselecting Message header privilege checkboxes, sub privilege checkboxes should get deselected automatically", testCaseId = "TC_Hub_Admin_Role_05")
	private void canViewMessages() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			AdminUsersRolesAddNewRolePage roles_page = new AdminUsersRolesAddNewRolePage(driver);
			fc.utobj().printTestStep("Configure New Hierarchy Level if disabled for Division");
			AdminHiddenLinksConfigureNewHierarchyLevelPageTest hierarchy_level=new AdminHiddenLinksConfigureNewHierarchyLevelPageTest();
			hierarchy_level.ConfigureNewHierarchyLevel(driver);
			

			fc.utobj().printTestStep("Navigate to Admin > Roles > Add New Role");
			fc.adminpage().adminUsersRolesAddNewRolePage(driver);

			fc.utobj().printTestStep("Select Corporate Level from drop down");
			
			fc.utobj().selectDropDownByValue(driver, roles_page.roleType, "1");

			fc.utobj().printTestStep("Deselect Can View Messages checkbox if selected already");
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver,
							".//td[contains(text(),'#')]/following-sibling::td[contains(text(),'"
									+ fc.utobj().translateString("Can View Messages")
									+ "')]/ancestor::tr/td/input[@id='14']"));

			fc.utobj().printTestStep(
					"Verify all sub Privileges under Can View Messages should get deselected automatically");

			boolean isCheckd = false;
			try {
				isCheckd =  fc.utobj().isSelected(driver,fc.utobj()
						.getElementByXpath(driver,
								".//td[contains(text(),'#')]/following-sibling::td[contains(text(),'"
										+ fc.utobj().translateString("Can Export Contacts")
										+ "')]/ancestor::tr/td/input[@id='14']")
						);
			} catch (Exception e) {
				isCheckd = true;
			}

			if (isCheckd) {
				fc.utobj().throwsException(
						"On deselecting the Can View Messages Privileges, Can Export Contacts Privilege is not getting deselect automatically");
			}

			try {
				isCheckd = fc.utobj().isSelected(driver,fc.utobj()
						.getElementByXpath(driver,
								".//td[contains(text(),'#')]/following-sibling::td[contains(text(),'"
										+ fc.utobj().translateString("Can Download Audit Report")
										+ "')]/ancestor::tr/td/input[@id='14']")
						);
			} catch (Exception e) {
				isCheckd = true;
			}

			if (isCheckd) {
				fc.utobj().throwsException(
						"On deselecting the Can View Messages Privileges, Can Download Audit Report Privilege is not getting deselect automatically");
			}

			fc.utobj().printTestStep("Select Division Level from drop down");
			
			fc.utobj().selectDropDownByValue(driver, roles_page.roleType, "5");

			fc.utobj().printTestStep("Deselect the Can View Messages checkbox if selected already");
			
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver,
							".//td[contains(text(),'#')]/following-sibling::td[contains(text(),'"
									+ fc.utobj().translateString("Can View Messages")
									+ "')]/ancestor::tr/td/input[@id='14']"));

			fc.utobj().printTestStep(
					"Verify all sub Privileges under Can View Messages privilege should get deselected automatically");

			try {
				isCheckd =  fc.utobj().isSelected(driver,fc.utobj()
						.getElementByXpath(driver,
								".//td[contains(text(),'#')]/following-sibling::td[contains(text(),'"
										+ fc.utobj().translateString("Can Export Contacts")
										+ "')]/ancestor::tr/td/input[@id='14']")
						);
			} catch (Exception e) {
				isCheckd = true;
			}

			if (isCheckd) {
				fc.utobj().throwsException(
						"On deselecting the Can View Messages Privileges, Can Export Contacts Privilege is not getting deselect automatically");
			}

			try {
				isCheckd =  fc.utobj().isSelected(driver,fc.utobj()
						.getElementByXpath(driver,
								".//td[contains(text(),'#')]/following-sibling::td[contains(text(),'"
										+ fc.utobj().translateString("Can Download Audit Report")
										+ "')]/ancestor::tr/td/input[@id='14']")
						);
			} catch (Exception e) {
				isCheckd = true;
			}

			if (isCheckd) {
				fc.utobj().throwsException(
						"On deselecting the Can View Messages Privileges, Can Download Audit Report Privilege is not getting deselect automatically");
			}

			fc.utobj().printTestStep("Select Regional Level from drop down");
			
			fc.utobj().selectDropDownByValue(driver, roles_page.roleType, "2");

			fc.utobj().printTestStep("Deselect the Can View Messages checkbox if selected already");
			
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver,
							".//td[contains(text(),'#')]/following-sibling::td[contains(text(),'"
									+ fc.utobj().translateString("Can View Messages")
									+ "')]/ancestor::tr/td/input[@id='99']"));

			fc.utobj().printTestStep(
					"Verify all sub Privileges under Can View Messages privilege should get deselected automatically");

			try {
				isCheckd =  fc.utobj().isSelected(driver,fc.utobj()
						.getElementByXpath(driver,
								".//td[contains(text(),'#')]/following-sibling::td[contains(text(),'"
										+ fc.utobj().translateString("Can Export Contacts")
										+ "')]/ancestor::tr/td/input[@id='99']")
						);
			} catch (Exception e) {
				isCheckd = true;
			}

			if (isCheckd) {
				fc.utobj().throwsException(
						"On deselecting the Can View Messages Privileges, Can Export Contacts Privilege is not getting deselect automatically");
			}

			fc.utobj().printTestStep("Select Franchise Level from drop down");
			
			fc.utobj().selectDropDownByValue(driver, roles_page.roleType, "0");

			fc.utobj().printTestStep("Deselect the Can View Messages checkbox if selected already");
			
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver,
							".//td[contains(text(),'#')]/following-sibling::td[contains(text(),'"
									+ fc.utobj().translateString("Can View Messages")
									+ "')]/ancestor::tr/td/input[@id='45']"));

			fc.utobj().printTestStep(
					"Verify all sub Privileges under Can View Messages privilege should get deselected automatically");

			try {
				isCheckd = fc.utobj().isSelected(driver,fc.utobj()
						.getElementByXpath(driver,
								".//td[contains(text(),'#')]/following-sibling::td[contains(text(),'"
										+ fc.utobj().translateString("Can Export Contacts")
										+ "')]/ancestor::tr/td/input[@id='45']")
						);
			} catch (Exception e) {
				isCheckd = true;
			}

			if (isCheckd) {
				fc.utobj().throwsException(
						"On deselecting the Can View Messages Privileges, Can Export Contacts Privilege is not getting deselect automatically");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "thehub", "hub_role" , "hubfail0514"})
	@TestCase(createdOn = "2018-01-08", updatedOn = "2018-01-08", testCaseDescription = "Verify On deselecting the can view Related Links Privileges, Can Administer Privileges should get deselected automatically by Corporate/Divisional Users Role Type", testCaseId = "TC_Hub_Admin_Role_06")
	private void canViewRelatedLnkCorporateLevel() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			AdminUsersRolesAddNewRolePage roles_page = new AdminUsersRolesAddNewRolePage(driver);

			fc.utobj().printTestStep("Configure New Hierarchy Level if disabled for Division");
			AdminHiddenLinksConfigureNewHierarchyLevelPageTest hierarchy_level=new AdminHiddenLinksConfigureNewHierarchyLevelPageTest();
			hierarchy_level.ConfigureNewHierarchyLevel(driver);
			
			fc.utobj().printTestStep("Navigate to Admin > Roles > Add New Role");
			fc.adminpage().adminUsersRolesAddNewRolePage(driver);

			fc.utobj().printTestStep("Select Corporate Level from drop down");
			fc.utobj().selectDropDownByValue(driver, roles_page.roleType, "1");

			fc.utobj().printTestStep("Deselect the can view Related Links Privileges");
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver,
							".//td[contains(text(),'#')]/following-sibling::td[contains(text(),'"
									+ fc.utobj().translateString("Can View Related Links")
									+ "')]/ancestor::tr/td/input[@id='14']"));

			fc.utobj().printTestStep("Verify Can Administer Related Links Privileges should get deselected");

			boolean isCheckd = false;
			try {
				isCheckd =  fc.utobj().isSelected(driver,fc.utobj()
						.getElementByXpath(driver,
								".//td[contains(text(),'#')]/following-sibling::td[contains(text(),'"
										+ fc.utobj().translateString("Can Administer Related Links")
										+ "')]/ancestor::tr/td/input[@id='14']")
						);
			} catch (Exception e) {
				isCheckd = true;
			}

			if (isCheckd) {
				fc.utobj().throwsException(
						"On deselecting the can view Related Links Privileges, Can Administer Related Links Privilege is not getting deselect automatically");
			}

			fc.utobj().printTestStep("Select Division Level from drop down");
			fc.utobj().selectDropDownByValue(driver, roles_page.roleType, "5");

			fc.utobj().printTestStep("Deselect the can view Related Links Privileges");
			
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver,
							".//td[contains(text(),'#')]/following-sibling::td[contains(text(),'"
									+ fc.utobj().translateString("Can View Related Links")
									+ "')]/ancestor::tr/td/input[@id='14']"));

			fc.utobj().printTestStep("Verify Can Administer Related Links Privileges should get deselected");

			try {
				isCheckd =  fc.utobj().isSelected(driver,fc.utobj()
						.getElementByXpath(driver,
								".//td[contains(text(),'#')]/following-sibling::td[contains(text(),'"
										+ fc.utobj().translateString("Can Administer Related Links")
										+ "')]/ancestor::tr/td/input[@id='14']")
						);
			} catch (Exception e) {
				isCheckd = true;
			}

			if (isCheckd) {
				fc.utobj().throwsException(
						"On deselecting the can view Related Links Privileges, Can Administer Related Links Privilege is not getting deselect automatically");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	/*
	 * Can View Library Privileges
	 */

	@Test(groups = { "thehub", "hub_role" })
	@TestCase(createdOn = "2018-01-08", updatedOn = "2018-01-08", testCaseDescription = "Verify On deselecting the can view Library Privileges, Can Administer Library and its all other sub Privileges should get deselected automatically By All Users Level", testCaseId = "TC_Hub_Admin_Role_07")
	private void canViewLibraryPrivileges() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			AdminUsersRolesAddNewRolePage roles_page = new AdminUsersRolesAddNewRolePage(driver);
			
			fc.utobj().printTestStep("Configure New Hierarchy Level if disabled for Division");
			AdminHiddenLinksConfigureNewHierarchyLevelPageTest hierarchy_level=new AdminHiddenLinksConfigureNewHierarchyLevelPageTest();
			hierarchy_level.ConfigureNewHierarchyLevel(driver);

			fc.utobj().printTestStep("Navigate to Admin > Roles > Add New Role");
			fc.adminpage().adminUsersRolesAddNewRolePage(driver);

			fc.utobj().printTestStep("Select Corporate Level from drop down");
			
			fc.utobj().selectDropDownByValue(driver, roles_page.roleType, "1");

			fc.utobj().printTestStep("Deselect Can View Library checkbox if selected already");
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver,
							".//td[contains(text(),'#')]/following-sibling::td[contains(text(),'"
									+ fc.utobj().translateString("Can View Library")
									+ "')]/ancestor::tr/td/input[@id='14']"));

			fc.utobj().printTestStep(
					"Verify all sub Privileges under Can View Library should get deselected automatically");

			boolean isCheckd = false;

			try {
				isCheckd =  fc.utobj().isSelected(driver,fc.utobj()
						.getElementByXpath(driver,
								".//td[contains(text(),'#')]/following-sibling::td[contains(text(),'"
										+ fc.utobj().translateString("Can Administer Library")
										+ "')]/ancestor::tr/td/input[@id='14']")
						);
			} catch (Exception e) {
				isCheckd = true;
			}

			if (isCheckd) {
				fc.utobj().throwsException(
						"On deselecting the Can View Library Privileges, Can Administer Library Privilege is not getting deselect automatically");
			}

			try {
				isCheckd =  fc.utobj().isSelected(driver,fc.utobj()
						.getElementByXpath(driver,
								".//td[contains(text(),'#')]/following-sibling::td[contains(text(),'"
										+ fc.utobj().translateString("Can Upload Library Documents")
										+ "')]/ancestor::tr/td/input[@id='14']")
						);
			} catch (Exception e) {
				isCheckd = true;
			}

			if (isCheckd) {
				fc.utobj().throwsException(
						"On deselecting the Can View Library Privileges, Can Upload Library Documents Privilege is not getting deselect automatically");
			}

			try {
				isCheckd =  fc.utobj().isSelected(driver,fc.utobj()
						.getElementByXpath(driver,
								".//td[contains(text(),'#')]/following-sibling::td[contains(text(),'"
										+ fc.utobj().translateString("Can Modify Library Documents")
										+ "')]/ancestor::tr/td/input[@id='14']")
						);
			} catch (Exception e) {
				isCheckd = true;
			}

			if (isCheckd) {
				fc.utobj().throwsException(
						"On deselecting the Can View Library Privileges, Can Modify Library Documents Privilege is not getting deselect automatically");
			}

			try {
				isCheckd =  fc.utobj().isSelected(driver,fc.utobj()
						.getElementByXpath(driver,
								".//td[contains(text(),'#')]/following-sibling::td[contains(text(),'"
										+ fc.utobj().translateString("Can Delete Library Documents")
										+ "')]/ancestor::tr/td/input[@id='14']")
						);
			} catch (Exception e) {
				isCheckd = true;
			}

			if (isCheckd) {
				fc.utobj().throwsException(
						"On deselecting the Can View Library Privileges, Can Delete Library Documents Privilege is not getting deselect automatically");
			}

			try {
				isCheckd =  fc.utobj().isSelected(driver,fc.utobj()
						.getElementByXpath(driver,
								".//td[contains(text(),'#')]/following-sibling::td[contains(text(),'"
										+ fc.utobj().translateString("Can view Library Document Version History")
										+ "')]/ancestor::tr/td/input[@id='14']")
						);
			} catch (Exception e) {
				isCheckd = true;
			}

			if (isCheckd) {
				fc.utobj().throwsException(
						"On deselecting the Can View Library Privileges, Can view Library Document Version History Privilege is not getting deselect automatically");
			}

			try {
				isCheckd =  fc.utobj().isSelected(driver,fc.utobj()
						.getElementByXpath(driver,
								".//td[contains(text(),'#')]/following-sibling::td[contains(text(),'"
										+ fc.utobj().translateString("Can delete Comments")
										+ "')]/ancestor::tr/td/input[@id='14']")
						);
			} catch (Exception e) {
				isCheckd = true;
			}

			if (isCheckd) {
				fc.utobj().throwsException(
						"On deselecting the Can View Library Privileges, Can delete Comments Privilege is not getting deselect automatically");
			}

			fc.utobj().printTestStep("Select Division Level from drop down");
			
			fc.utobj().selectDropDownByValue(driver, roles_page.roleType, "5");

			fc.utobj().printTestStep("Deselect the Can View Library checkbox if selected already");
			
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver,
							".//td[contains(text(),'#')]/following-sibling::td[contains(text(),'"
									+ fc.utobj().translateString("Can View Library")
									+ "')]/ancestor::tr/td/input[@id='14']"));

			fc.utobj().printTestStep(
					"Verify all sub Privileges under Can View Library privilege should get deselected automatically");

			try {
				isCheckd =  fc.utobj().isSelected(driver,fc.utobj()
						.getElementByXpath(driver,
								".//td[contains(text(),'#')]/following-sibling::td[contains(text(),'"
										+ fc.utobj().translateString("Can Administer Library")
										+ "')]/ancestor::tr/td/input[@id='14']")
						);
			} catch (Exception e) {
				isCheckd = true;
			}

			if (isCheckd) {
				fc.utobj().throwsException(
						"On deselecting the Can View Library Privileges, Can Administer Library Privilege is not getting deselect automatically");
			}

			try {
				isCheckd =  fc.utobj().isSelected(driver,fc.utobj()
						.getElementByXpath(driver,
								".//td[contains(text(),'#')]/following-sibling::td[contains(text(),'"
										+ fc.utobj().translateString("Can Upload Library Documents")
										+ "')]/ancestor::tr/td/input[@id='14']")
						);
			} catch (Exception e) {
				isCheckd = true;
			}

			if (isCheckd) {
				fc.utobj().throwsException(
						"On deselecting the Can View Library Privileges, Can Upload Library Documents Privilege is not getting deselect automatically");
			}

			try {
				isCheckd =  fc.utobj().isSelected(driver,fc.utobj()
						.getElementByXpath(driver,
								".//td[contains(text(),'#')]/following-sibling::td[contains(text(),'"
										+ fc.utobj().translateString("Can Modify Library Documents")
										+ "')]/ancestor::tr/td/input[@id='14']")
						);
			} catch (Exception e) {
				isCheckd = true;
			}

			if (isCheckd) {
				fc.utobj().throwsException(
						"On deselecting the Can View Library Privileges, Can Modify Library Documents Privilege is not getting deselect automatically");
			}

			try {
				isCheckd =  fc.utobj().isSelected(driver,fc.utobj()
						.getElementByXpath(driver,
								".//td[contains(text(),'#')]/following-sibling::td[contains(text(),'"
										+ fc.utobj().translateString("Can Delete Library Documents")
										+ "')]/ancestor::tr/td/input[@id='14']")
						);
			} catch (Exception e) {
				isCheckd = true;
			}

			if (isCheckd) {
				fc.utobj().throwsException(
						"On deselecting the Can View Library Privileges, Can Delete Library Documents Privilege is not getting deselect automatically");
			}

			try {
				isCheckd =  fc.utobj().isSelected(driver,fc.utobj()
						.getElementByXpath(driver,
								".//td[contains(text(),'#')]/following-sibling::td[contains(text(),'"
										+ fc.utobj().translateString("Can view Library Document Version History")
										+ "')]/ancestor::tr/td/input[@id='14']")
						);
			} catch (Exception e) {
				isCheckd = true;
			}

			if (isCheckd) {
				fc.utobj().throwsException(
						"On deselecting the Can View Library Privileges, Can view Library Document Version History Privilege is not getting deselect automatically");
			}

			try {
				isCheckd = fc.utobj().isSelected(driver,fc.utobj()
						.getElementByXpath(driver,
								".//td[contains(text(),'#')]/following-sibling::td[contains(text(),'"
										+ fc.utobj().translateString("Can delete Comments")
										+ "')]/ancestor::tr/td/input[@id='14']")
						);
			} catch (Exception e) {
				isCheckd = true;
			}

			if (isCheckd) {
				fc.utobj().throwsException(
						"On deselecting the Can View Library Privileges, Can delete Comments Privilege is not getting deselect automatically");
			}

			fc.utobj().printTestStep("Select Regional Level from drop down");
			
			fc.utobj().selectDropDownByValue(driver, roles_page.roleType, "2");

			fc.utobj().printTestStep("Deselect the Can View Library checkbox if selected already");
			
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver,
							".//td[contains(text(),'#')]/following-sibling::td[contains(text(),'"
									+ fc.utobj().translateString("Can View Library")
									+ "')]/ancestor::tr/td/input[@id='99']"));

			fc.utobj().printTestStep(
					"Verify all sub Privileges under Can View Library privilege should get deselected automatically");

			try {
				isCheckd =  fc.utobj().isSelected(driver,fc.utobj()
						.getElementByXpath(driver,
								".//td[contains(text(),'#')]/following-sibling::td[contains(text(),'"
										+ fc.utobj().translateString("Can Upload Library Documents")
										+ "')]/ancestor::tr/td/input[@id='99']")
						);
			} catch (Exception e) {
				isCheckd = true;
			}

			if (isCheckd) {
				fc.utobj().throwsException(
						"On deselecting the Can View Library Privileges, Can Upload Library Documents Privilege is not getting deselect automatically");
			}

			try {
				isCheckd =  fc.utobj().isSelected(driver,fc.utobj()
						.getElementByXpath(driver,
								".//td[contains(text(),'#')]/following-sibling::td[contains(text(),'"
										+ fc.utobj().translateString("Can Modify Library Documents")
										+ "')]/ancestor::tr/td/input[@id='99']")
						);
			} catch (Exception e) {
				isCheckd = true;
			}

			if (isCheckd) {
				fc.utobj().throwsException(
						"On deselecting the Can View Library Privileges, Can Modify Library Documents Privilege is not getting deselect automatically");
			}

			try {
				isCheckd =  fc.utobj().isSelected(driver,fc.utobj()
						.getElementByXpath(driver,
								".//td[contains(text(),'#')]/following-sibling::td[contains(text(),'"
										+ fc.utobj().translateString("Can Delete Library Documents")
										+ "')]/ancestor::tr/td/input[@id='99']")
						);
			} catch (Exception e) {
				isCheckd = true;
			}

			if (isCheckd) {
				fc.utobj().throwsException(
						"On deselecting the Can View Library Privileges, Can Delete Library Documents Privilege is not getting deselect automatically");
			}

			try {
				isCheckd = fc.utobj().isSelected(driver,fc.utobj()
						.getElementByXpath(driver,
								".//td[contains(text(),'#')]/following-sibling::td[contains(text(),'"
										+ fc.utobj().translateString("Can view Library Document Version History")
										+ "')]/ancestor::tr/td/input[@id='99']")
						);
			} catch (Exception e) {
				isCheckd = true;
			}

			if (isCheckd) {
				fc.utobj().throwsException(
						"On deselecting the Can View Library Privileges, Can view Library Document Version History Privilege is not getting deselect automatically");
			}

			fc.utobj().printTestStep("Select Franchise Level from drop down");
			
			fc.utobj().selectDropDownByValue(driver, roles_page.roleType, "0");

			fc.utobj().printTestStep("Deselect the Can View Library checkbox if selected already");
			
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver,
							".//td[contains(text(),'#')]/following-sibling::td[contains(text(),'"
									+ fc.utobj().translateString("Can View Library")
									+ "')]/ancestor::tr/td/input[@id='45']"));

			fc.utobj().printTestStep(
					"Verify all sub Privileges under Can View Messages privilege should get deselected automatically");

			try {
				isCheckd = fc.utobj().isSelected(driver,fc.utobj()
						.getElementByXpath(driver,
								".//td[contains(text(),'#')]/following-sibling::td[contains(text(),'"
										+ fc.utobj().translateString("Can Upload Library Documents")
										+ "')]/ancestor::tr/td/input[@id='45']")
						);
			} catch (Exception e) {
				isCheckd = true;
			}

			if (isCheckd) {
				fc.utobj().throwsException(
						"On deselecting the Can View Library Privileges, Can Upload Library Documents Privilege is not getting deselect automatically");
			}

			try {
				isCheckd = fc.utobj().isSelected(driver,fc.utobj()
						.getElementByXpath(driver,
								".//td[contains(text(),'#')]/following-sibling::td[contains(text(),'"
										+ fc.utobj().translateString("Can Modify Library Documents")
										+ "')]/ancestor::tr/td/input[@id='45']")
						);
			} catch (Exception e) {
				isCheckd = true;
			}

			if (isCheckd) {
				fc.utobj().throwsException(
						"On deselecting the Can View Library Privileges, Can Modify Library Documents Privilege is not getting deselect automatically");
			}
			try {
				isCheckd = fc.utobj().isSelected(driver,fc.utobj()
						.getElementByXpath(driver,
								".//td[contains(text(),'#')]/following-sibling::td[contains(text(),'"
										+ fc.utobj().translateString("Can Delete Library Documents")
										+ "')]/ancestor::tr/td/input[@id='45']")
						);
			} catch (Exception e) {
				isCheckd = true;
			}

			if (isCheckd) {
				fc.utobj().throwsException(
						"On deselecting the Can View Library Privileges, Can Delete Library Documents Privilege is not getting deselect automatically");
			}
			try {
				isCheckd = fc.utobj().isSelected(driver,fc.utobj()
						.getElementByXpath(driver,
								".//td[contains(text(),'#')]/following-sibling::td[contains(text(),'"
										+ fc.utobj().translateString("Can view Library Document Version History")
										+ "')]/ancestor::tr/td/input[@id='45']")
						);
			} catch (Exception e) {
				isCheckd = true;
			}

			if (isCheckd) {
				fc.utobj().throwsException(
						"On deselecting the Can View Library Privileges, Can view Library Document Version History Privilege is not getting deselect automatically");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "thehub", "hub_role" })
	@TestCase(createdOn = "2018-01-09", updatedOn = "2018-01-09", testCaseDescription = "Verify On deselecting the can view epoll Privileges,Can Manage EPoll Privileges should get deselected automatically by Corporate/Divisional Users Role Type", testCaseId = "TC_Hub_Admin_Role_08")
	private void canViewEpoll() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			AdminUsersRolesAddNewRolePage roles_page = new AdminUsersRolesAddNewRolePage(driver);

			fc.utobj().printTestStep("Configure New Hierarchy Level if disabled for Division");
			AdminHiddenLinksConfigureNewHierarchyLevelPageTest hierarchy_level=new AdminHiddenLinksConfigureNewHierarchyLevelPageTest();
			hierarchy_level.ConfigureNewHierarchyLevel(driver);
			
			fc.utobj().printTestStep("Navigate to Admin > Roles > Add New Role");
			fc.adminpage().adminUsersRolesAddNewRolePage(driver);

			fc.utobj().printTestStep("Select Corporate Level from drop down");
			
			fc.utobj().selectDropDownByValue(driver, roles_page.roleType, "1");

			fc.utobj().printTestStep("Deselect the Can View EPoll Privileges");
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver,
							".//td[contains(text(),'#')]/following-sibling::td[contains(text(),'"
									+ fc.utobj().translateString("Can View EPoll")
									+ "')]/ancestor::tr/td/input[@id='14']"));

			fc.utobj().printTestStep("Verify Can Manage EPoll Privileges should get deselected");

			boolean isCheckd = false;
			try {
				isCheckd = fc.utobj().isSelected(driver,fc.utobj()
						.getElementByXpath(driver,
								".//td[contains(text(),'#')]/following-sibling::td[contains(text(),'"
										+ fc.utobj().translateString("Can Manage EPoll")
										+ "')]/ancestor::tr/td/input[@id='14']")
						);
			} catch (Exception e) {
				isCheckd = true;
			}

			if (isCheckd) {
				fc.utobj().throwsException(
						"On deselecting the Can View EPoll Privileges, Can Manage EPoll Privilege is not getting deselect automatically");
			}

			fc.utobj().printTestStep("Select Division Level from drop down");
			fc.utobj().selectDropDownByValue(driver, roles_page.roleType, "5");

			fc.utobj().printTestStep("Deselect the Can View EPoll Privileges");
			
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver,
							".//td[contains(text(),'#')]/following-sibling::td[contains(text(),'"
									+ fc.utobj().translateString("Can View EPoll")
									+ "')]/ancestor::tr/td/input[@id='14']"));

			fc.utobj().printTestStep("Verify Can Administer Related Links Privileges should get deselected");

			try {
				 fc.utobj().isSelected(driver,fc.utobj()
						.getElementByXpath(driver,
								".//td[contains(text(),'#')]/following-sibling::td[contains(text(),'"
										+ fc.utobj().translateString("Can Manage EPoll")
										+ "')]/ancestor::tr/td/input[@id='14']")
						);
			} catch (Exception e) {
				isCheckd = true;
			}

			if (isCheckd) {
				fc.utobj().throwsException(
						"On deselecting the Can View EPoll Privileges, Can Manage EPoll Privilege is not getting deselect automatically");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	/*
	 * Can View News
	 */

	@Test(groups = { "thehub", "hub_role" })
	@TestCase(createdOn = "2018-01-09", updatedOn = "2018-01-09", testCaseDescription = "Verify On deselecting the can view News Privileges, Can Administer News and its all other sub Privileges should get deselected automatically By All Users Level", testCaseId = "TC_Hub_Admin_Role_09")
	private void canViewNewsPrivileges() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			AdminUsersRolesAddNewRolePage roles_page = new AdminUsersRolesAddNewRolePage(driver);

			fc.utobj().printTestStep("Configure New Hierarchy Level if disabled for Division");
			AdminHiddenLinksConfigureNewHierarchyLevelPageTest hierarchy_level=new AdminHiddenLinksConfigureNewHierarchyLevelPageTest();
			hierarchy_level.ConfigureNewHierarchyLevel(driver);
			
			fc.utobj().printTestStep("Navigate to Admin > Roles > Add New Role");
			fc.adminpage().adminUsersRolesAddNewRolePage(driver);

			fc.utobj().printTestStep("Select Corporate Level from drop down");
			
			fc.utobj().selectDropDownByValue(driver, roles_page.roleType, "1");

			fc.utobj().printTestStep("Deselect can view News checkbox if selected already");
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver,
							".//td[contains(text(),'#')]/following-sibling::td[contains(text(),'"
									+ fc.utobj().translateString("Can View News")
									+ "')]/ancestor::tr/td/input[@id='14']"));

			fc.utobj()
					.printTestStep("Verify all sub Privileges under Can View News should get deselected automatically");

			boolean isCheckd = false;

			try {
				 fc.utobj().isSelected(driver,fc.utobj()
						.getElementByXpath(driver,
								".//td[contains(text(),'#')]/following-sibling::td[contains(text(),'"
										+ fc.utobj().translateString("Can Administer News")
										+ "')]/ancestor::tr/td/input[@id='14']")
						);
			} catch (Exception e) {
				isCheckd = true;
			}

			if (isCheckd) {
				fc.utobj().throwsException(
						"On deselecting the Can View News Privileges, Can Administer News Privilege is not getting deselect automatically");
			}

			try {
				 fc.utobj().isSelected(driver,fc.utobj()
						.getElementByXpath(driver,
								".//td[contains(text(),'#')]/following-sibling::td[contains(text(),'"
										+ fc.utobj().translateString("Can Upload News Items")
										+ "')]/ancestor::tr/td/input[@id='14']")
						);
			} catch (Exception e) {
				isCheckd = true;
			}

			if (isCheckd) {
				fc.utobj().throwsException(
						"On deselecting the Can View News Privileges, Can Upload News Items Documents Privilege is not getting deselect automatically");
			}

			try {
				 fc.utobj().isSelected(driver,fc.utobj()
						.getElementByXpath(driver,
								".//td[contains(text(),'#')]/following-sibling::td[contains(text(),'"
										+ fc.utobj().translateString("Can Modify News Items")
										+ "')]/ancestor::tr/td/input[@id='14']")
						);
			} catch (Exception e) {
				isCheckd = true;
			}

			if (isCheckd) {
				fc.utobj().throwsException(
						"On deselecting the Can View News Privileges, Can Modify News Items Documents Privilege is not getting deselect automatically");
			}

			try {
				 fc.utobj().isSelected(driver,fc.utobj()
						.getElementByXpath(driver,
								".//td[contains(text(),'#')]/following-sibling::td[contains(text(),'"
										+ fc.utobj().translateString("Can Delete News Items")
										+ "')]/ancestor::tr/td/input[@id='14']")
						);
			} catch (Exception e) {
				isCheckd = true;
			}

			if (isCheckd) {
				fc.utobj().throwsException(
						"On deselecting the Can View News Privileges, Can Delete News Items Documents Privilege is not getting deselect automatically");
			}

			fc.utobj().printTestStep("Select Division Level from drop down");
			
			fc.utobj().selectDropDownByValue(driver, roles_page.roleType, "5");

			fc.utobj().printTestStep("Deselect the Can View News checkbox if selected already");
			
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver,
							".//td[contains(text(),'#')]/following-sibling::td[contains(text(),'"
									+ fc.utobj().translateString("Can View News")
									+ "')]/ancestor::tr/td/input[@id='14']"));

			fc.utobj().printTestStep(
					"Verify all sub Privileges under Can View News privilege should get deselected automatically");

			try {
				 fc.utobj().isSelected(driver,fc.utobj()
						.getElementByXpath(driver,
								".//td[contains(text(),'#')]/following-sibling::td[contains(text(),'"
										+ fc.utobj().translateString("Can Administer News")
										+ "')]/ancestor::tr/td/input[@id='14']")
						);
			} catch (Exception e) {
				isCheckd = true;
			}

			if (isCheckd) {
				fc.utobj().throwsException(
						"On deselecting the Can View News Privileges, Can Administer News Privilege is not getting deselect automatically");
			}

			try {
				 fc.utobj().isSelected(driver,fc.utobj()
						.getElementByXpath(driver,
								".//td[contains(text(),'#')]/following-sibling::td[contains(text(),'"
										+ fc.utobj().translateString("Can Upload News Items")
										+ "')]/ancestor::tr/td/input[@id='14']")
						);
			} catch (Exception e) {
				isCheckd = true;
			}

			if (isCheckd) {
				fc.utobj().throwsException(
						"On deselecting the Can View News Privileges, Can Modify News Items Privilege is not getting deselect automatically");
			}

			try {
				 fc.utobj().isSelected(driver,fc.utobj()
						.getElementByXpath(driver,
								".//td[contains(text(),'#')]/following-sibling::td[contains(text(),'"
										+ fc.utobj().translateString("Can Modify News Items")
										+ "')]/ancestor::tr/td/input[@id='14']")
						);
			} catch (Exception e) {
				isCheckd = true;
			}

			if (isCheckd) {
				fc.utobj().throwsException(
						"On deselecting the Can View News Privileges, Can Modify News Items Privilege is not getting deselect automatically");
			}

			try {
				 fc.utobj().isSelected(driver,fc.utobj()
						.getElementByXpath(driver,
								".//td[contains(text(),'#')]/following-sibling::td[contains(text(),'"
										+ fc.utobj().translateString("Can Delete News Items")
										+ "')]/ancestor::tr/td/input[@id='14']")
						);
			} catch (Exception e) {
				isCheckd = true;
			}

			if (isCheckd) {
				fc.utobj().throwsException(
						"On deselecting the Can View News Privileges, Can Delete News Items Privilege is not getting deselect automatically");
			}

			fc.utobj().printTestStep("Select Regional Level from drop down");
			
			fc.utobj().selectDropDownByValue(driver, roles_page.roleType, "2");

			fc.utobj().printTestStep("Deselect the Can View News checkbox if selected already");
			
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver,
							".//td[contains(text(),'#')]/following-sibling::td[contains(text(),'"
									+ fc.utobj().translateString("Can View News")
									+ "')]/ancestor::tr/td/input[@id='99']"));

			fc.utobj().printTestStep(
					"Verify all sub Privileges under Can View News privilege should get deselected automatically");

			try {
				 fc.utobj().isSelected(driver,fc.utobj()
						.getElementByXpath(driver,
								".//td[contains(text(),'#')]/following-sibling::td[contains(text(),'"
										+ fc.utobj().translateString("Can Upload News Items")
										+ "')]/ancestor::tr/td/input[@id='99']")
						);
			} catch (Exception e) {
				isCheckd = true;
			}

			if (isCheckd) {
				fc.utobj().throwsException(
						"On deselecting the Can View News Privileges, Can Upload News Items Privilege is not getting deselect automatically");
			}

			try {
				 fc.utobj().isSelected(driver,fc.utobj()
						.getElementByXpath(driver,
								".//td[contains(text(),'#')]/following-sibling::td[contains(text(),'"
										+ fc.utobj().translateString("Can Modify News Items")
										+ "')]/ancestor::tr/td/input[@id='99']")
						);
			} catch (Exception e) {
				isCheckd = true;
			}

			if (isCheckd) {
				fc.utobj().throwsException(
						"On deselecting the Can View News Privileges, Can Modify News Items Privilege is not getting deselect automatically");
			}

			try {
				 fc.utobj().isSelected(driver,fc.utobj()
						.getElementByXpath(driver,
								".//td[contains(text(),'#')]/following-sibling::td[contains(text(),'"
										+ fc.utobj().translateString("Can Delete News Items")
										+ "')]/ancestor::tr/td/input[@id='99']")
						);
			} catch (Exception e) {
				isCheckd = true;
			}

			if (isCheckd) {
				fc.utobj().throwsException(
						"On deselecting the Can View News Privileges, Can Delete News Items Privilege is not getting deselect automatically");
			}

			fc.utobj().printTestStep("Select Franchise Level from drop down");
			
			fc.utobj().selectDropDownByValue(driver, roles_page.roleType, "0");

			fc.utobj().printTestStep("Deselect the Can View News checkbox if selected already");
			
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver,
							".//td[contains(text(),'#')]/following-sibling::td[contains(text(),'"
									+ fc.utobj().translateString("Can View News")
									+ "')]/ancestor::tr/td/input[@id='45']"));

			fc.utobj().printTestStep(
					"Verify all sub Privileges under Can View News privilege should get deselected automatically");

			try {
				 fc.utobj().isSelected(driver,fc.utobj()
						.getElementByXpath(driver,
								".//td[contains(text(),'#')]/following-sibling::td[contains(text(),'"
										+ fc.utobj().translateString("Can Upload News Items")
										+ "')]/ancestor::tr/td/input[@id='45']")
						);
			} catch (Exception e) {
				isCheckd = true;
			}

			if (isCheckd) {
				fc.utobj().throwsException(
						"On deselecting the Can View News Privileges, Can Upload News Items Privilege is not getting deselect automatically");
			}

			try {
				 fc.utobj().isSelected(driver,fc.utobj()
						.getElementByXpath(driver,
								".//td[contains(text(),'#')]/following-sibling::td[contains(text(),'"
										+ fc.utobj().translateString("Can Modify News Items")
										+ "')]/ancestor::tr/td/input[@id='45']")
						);
			} catch (Exception e) {
				isCheckd = true;
			}

			if (isCheckd) {
				fc.utobj().throwsException(
						"On deselecting the Can View News Privileges, Can Modify News Items Privilege is not getting deselect automatically");
			}
			try {
				 fc.utobj().isSelected(driver,fc.utobj()
						.getElementByXpath(driver,
								".//td[contains(text(),'#')]/following-sibling::td[contains(text(),'"
										+ fc.utobj().translateString("Can Delete News Items")
										+ "')]/ancestor::tr/td/input[@id='45']")
						);
			} catch (Exception e) {
				isCheckd = true;
			}

			if (isCheckd) {
				fc.utobj().throwsException(
						"On deselecting the Can View News Privileges, Can Delete News Items Privilege is not getting deselect automatically");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	public String addCorporateRoles(WebDriver driver, String roleName) throws Exception {

		try {
			fc.adminpage().adminUsersRolesAddNewRolePage(driver);

			AdminUsersRolesAddNewRolePage pobj = new AdminUsersRolesAddNewRolePage(driver);
			fc.utobj().selectDropDownByValue(driver, pobj.roleType, "1");

			fc.utobj().sendKeys(driver, pobj.roleName, roleName);
			fc.utobj().clickElement(driver, pobj.submit);

			AdminUsersRolesAddRoleAddUsersforRolePageTest p2 = new AdminUsersRolesAddRoleAddUsersforRolePageTest();
			p2.assignLater(driver);

			boolean recordCount = fc.utobj().assertLinkText(driver, roleName);
			if (recordCount == false) {
				fc.utobj().throwsException("Unable to Add Role!");
			}
		} catch (Exception e) {
			fc.utobj().throwsException("was not able Add Corporate Roles");

		}
		return roleName;
	}

	public String deleteRoles(WebDriver driver, String roleName, Map<String, String> config) throws Exception {

		String testCaseId = "TC_deleteCorporateRoles_01";
		if (fc.utobj().validate(testCaseId)) {
			try {
				fc.adminpage().adminPage(driver);
				AdminPageTest p2 = new AdminPageTest();
				p2.openRolesPage(driver);
				fc.utobj().actionImgOption(driver, roleName, "Delete");
				fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//*[@value='Delete']"));

			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException("was not able Delete Corporate Roles");

		}
		return roleName;
	}

	public void addCorporateRolesWithOnlyTrainingModule(WebDriver driver, Map<String, String> config, String roleName)
			throws Exception {

		String testCaseId = "TC_addCorporateRolesWithOnlyTrainingModule_01";
		if (fc.utobj().validate(testCaseId)) {
			try {
				fc.adminpage().adminUsersRolesAddNewRolePage(driver);
				AdminUsersRolesAddNewRolePage pobj = new AdminUsersRolesAddNewRolePage(driver);
				String testData = roleName;
				fc.utobj().selectDropDown(driver, pobj.roleType, "Corporate Level");
				fc.utobj().sendKeys(driver, pobj.roleName, testData);

				if (fc.utobj().isSelected(driver,pobj.allPrivileges)) {
					fc.utobj().clickElement(driver, pobj.allPrivileges);
				}

				if (fc.utobj().isSelected(driver, pobj.canManageTraining)) {
					// do nothing
				} else {
					fc.utobj().clickElement(driver, pobj.canManageTraining);

				}
				fc.utobj().clickElement(driver, pobj.submit);
				AdminUsersRolesAddRoleAddUsersforRolePageTest p2 = new AdminUsersRolesAddRoleAddUsersforRolePageTest();
				p2.assignLater(driver);

				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException("was not able to add Corporate Roles for training Only");

		}
	}

	public void addRegionalRoleWithOnlyTrainingModule(WebDriver driver, Map<String, String> config, String roleName)
			throws Exception {

		String testCaseId = "TC_addRegionalRoleWithOnlyTrainingModule_01";
		if (fc.utobj().validate(testCaseId)) {
			try {
				driver = fc.loginpage().login(driver);
				fc.adminpage().adminUsersRolesAddNewRolePage(driver);
				AdminUsersRolesAddNewRolePage pobj = new AdminUsersRolesAddNewRolePage(driver);

				String testData = roleName;
				fc.utobj().selectDropDownByValue(driver, pobj.roleType, "2");

				fc.utobj().sendKeys(driver, pobj.roleName, testData);

				if (fc.utobj().isSelected(driver,pobj.allPrivileges)) {
					fc.utobj().clickElement(driver, pobj.allPrivileges);
				}

				if (fc.utobj().isSelected(driver, pobj.canManageTraining)) {
				} else {
					fc.utobj().clickElement(driver, pobj.canManageTraining);

				}

				fc.utobj().clickElement(driver, pobj.submit);
				AdminUsersRolesAddRoleAddUsersforRolePageTest p2 = new AdminUsersRolesAddRoleAddUsersforRolePageTest();
				p2.assignLater(driver);

				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException("was not able to add Regional Role For Training Module Only");

		}
	}

	public void addDivisionalRoleWithOnlyTrainingModule(WebDriver driver, Map<String, String> config, String roleName)
			throws Exception {

		String testCaseId = "TC_addDivisionalRoleWithOnlyTrainingModule_01";
		if (fc.utobj().validate(testCaseId)) {
			try {
				fc.utobj().printTestStep("Navigate To  Admin > Hidden Links > Configure New Hierarchy Level");
				AdminHiddenLinksConfigureNewHierarchyLevelPageTest hierarchyPage = new AdminHiddenLinksConfigureNewHierarchyLevelPageTest();
				hierarchyPage.ConfigureNewHierarchyLevel(driver);

				fc.adminpage().adminUsersRolesAddNewRolePage(driver);
				AdminUsersRolesAddNewRolePage pobj = new AdminUsersRolesAddNewRolePage(driver);
				String testData = roleName;

				fc.utobj().selectDropDownByValue(driver, pobj.roleType, "5");

				fc.utobj().sendKeys(driver, pobj.roleName, testData);

				if (fc.utobj().isSelected(driver,pobj.allPrivileges)) {
					fc.utobj().clickElement(driver, pobj.allPrivileges);
				}

				if (fc.utobj().isSelected(driver, pobj.canManageTraining)) {
					// do nothing
				} else {
					fc.utobj().clickElement(driver, pobj.canManageTraining);

				}

				fc.utobj().clickElement(driver, pobj.submit);
				AdminUsersRolesAddRoleAddUsersforRolePageTest p2 = new AdminUsersRolesAddRoleAddUsersforRolePageTest();
				p2.assignLater(driver);

				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException("was not able to Add Divisional Role With Only Training Module");

		}
	}

	public void addFranchiselRoleWithOnlyTrainingModule(WebDriver driver, Map<String, String> config, String roleName)
			throws Exception {

		String testCaseId = "TC_addFranchiselRoleWithOnlyTrainingModule_01";
		if (fc.utobj().validate(testCaseId)) {
			try {
				fc.adminpage().adminUsersRolesAddNewRolePage(driver);
				AdminUsersRolesAddNewRolePage pobj = new AdminUsersRolesAddNewRolePage(driver);
				String testData = roleName;

				fc.utobj().selectDropDownByValue(driver, pobj.roleType, "0");

				fc.utobj().sendKeys(driver, pobj.roleName, testData);

				fc.utobj().check(pobj.allPrivileges, "yes");

				if (fc.utobj().isSelected(driver, pobj.canManageTraining)) {
					fc.utobj().clickElement(driver, pobj.canManageTraining);
				} else {
					// do nothing

				}
				fc.utobj().clickElement(driver, pobj.takepartincourse);
				fc.utobj().clickElement(driver, pobj.canviewevents);

				fc.utobj().clickElement(driver, pobj.submit);
				AdminUsersRolesAddRoleAddUsersforRolePageTest p2 = new AdminUsersRolesAddRoleAddUsersforRolePageTest();
				p2.assignLater(driver);

				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException("was not able to add Franchise Role With Tarining Module Only");

		}
	}

	public void addFranchiselRoleWithFieldpsAccess(WebDriver driver, Map<String, String> config, String roleName)
			throws Exception {

		String testCaseId = "TC_addFranchiselRoleWithFieldpsAccess_01";
		if (fc.utobj().validate(testCaseId)) {
			try {
				fc.adminpage().adminUsersRolesAddNewRolePage(driver);
				AdminUsersRolesAddNewRolePage pobj = new AdminUsersRolesAddNewRolePage(driver);
				String testData = roleName;
				fc.utobj().selectDropDown(driver, pobj.roleType, "Franchise Level");

				fc.utobj().sendKeys(driver, pobj.roleName, testData);

				if (fc.utobj().isSelected(driver,pobj.allPrivileges)) {
					fc.utobj().clickElement(driver, pobj.allPrivileges);
				}

				if (!fc.utobj().isSelected(driver, pobj.canManageFieldOps)) {
					fc.utobj().clickElement(driver, pobj.canManageFieldOps);
				} else {
					// do nothing

				}

				// fc.utobj().clickElement(driver,
				// fc.utobj().getElementByXpath(driver,".//*[contains(text () ,
				// 'Grants
				// Access to Start Visits')]/ancestor::tr/td/input")));
				fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
						".//*[contains(text () , 'Grants Access to Delete Visits')]/ancestor::tr/td/input"));

				fc.utobj().clickElement(driver, pobj.submit);
				AdminUsersRolesAddRoleAddUsersforRolePageTest p2 = new AdminUsersRolesAddRoleAddUsersforRolePageTest();
				p2.assignLater(driver);

				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException("was not able to add Franchise Role With FieldOps Access");

		}
	}

	public void removePrivilegeFromRole(WebDriver driver, String roleType, List<String> privilegeName,
			Map<String, String> config) throws Exception {

		String testCaseId = "TC_removePrivilegeFromRole_01";
		if (fc.utobj().validate(testCaseId)) {
			try {
				fc.adminpage().adminUsersRolesAddNewRolePage(driver);
				AdminUsersRolesAddNewRolePage pobj = new AdminUsersRolesAddNewRolePage(driver);
				fc.utobj().selectDropDown(driver, pobj.roleType, roleType);

				for (int x = 0; x < privilegeName.size(); x++) {
					String privilege = privilegeName.get(x);

					fc.utobj().check(fc.utobj().getElementByXpath(driver,
							".//*[contains(text(),'" + privilege + "')]//ancestor::tr[@class='bText12b']//input"),
							"uncheck");
				}

				Thread.sleep(30000);

				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException("was not able to remove Privilage For Role");
		}
	}

	public void addRolesWithModulesPrivileges(WebDriver driver, String roleType, String roleName, String moduleName,
			Map<String, String> privileges, String testCaseId) throws Exception {

		fc.utobj().printTestStep("Admin > Users > Roles > Add New Role");
		fc.adminpage().adminUsersRolesAddNewRolePage(driver);
		AdminUsersRolesAddNewRolePage pobj = new AdminUsersRolesAddNewRolePage(driver);


		if (roleType.equalsIgnoreCase("Corporate Role")) {
			fc.utobj().selectDropDownByValue(driver, pobj.roleType, "1");

		} else if (roleType.equalsIgnoreCase("Division Level")) {
			fc.utobj().selectDropDownByValue(driver, pobj.roleType, "5");

		} else if (roleType.equalsIgnoreCase("Regional Level")) {
			fc.utobj().selectDropDownByValue(driver, pobj.roleType, "2");

		} else if (roleType.equalsIgnoreCase("Franchise Level")) {
			fc.utobj().selectDropDownByValue(driver, pobj.roleType, "0");
		}

		fc.utobj().sendKeys(driver, pobj.roleName, roleName);

		if ("The Hub".equalsIgnoreCase(moduleName)) {

			Set<String> keySet = privileges.keySet();

			for (String s : keySet) {
				if (s.contains("Can View Messages")) {
					if (privileges.get("Can View Messages").equalsIgnoreCase("Yes")) {

						if (!fc.utobj().isSelected(driver, pobj.canViewMessages)) {
							fc.utobj().clickElement(driver, pobj.canViewMessages);
						}

					} else if (privileges.get("Can View Messages").equalsIgnoreCase("No")) {

						if (fc.utobj().isSelected(driver, pobj.canViewMessages)) {
							fc.utobj().clickElement(driver, pobj.canViewMessages);
						}
					}
				} else if (s.contains("Can View Library")) {
					if (privileges.get("Can View Library").equalsIgnoreCase("Yes")) {

						if (!fc.utobj().isSelected(driver, pobj.canViewLibrary)) {
							fc.utobj().clickElement(driver, pobj.canViewLibrary);
						}

					} else if (privileges.get("Can View Library").equalsIgnoreCase("No")) {

						if (fc.utobj().isSelected(driver, pobj.canViewLibrary)) {
							fc.utobj().clickElement(driver, pobj.canViewLibrary);
						}
					}
				} else if (s.contains("Can View What's New")) {
					if (privileges.get("Can View What's New").equalsIgnoreCase("Yes")) {

						if (!fc.utobj().isSelected(driver, pobj.canViewWhatNew)) {
							fc.utobj().clickElement(driver, pobj.canViewWhatNew);
						}

					} else if (privileges.get("Can View What's New").equalsIgnoreCase("No")) {

						if (fc.utobj().isSelected(driver, pobj.canViewWhatNew)) {
							fc.utobj().clickElement(driver, pobj.canViewWhatNew);
						}
					}
				} else if (s.contains("Can View EPoll")) {
					if (privileges.get("Can View EPoll").equalsIgnoreCase("Yes")) {

						if (!fc.utobj().isSelected(driver, pobj.canViewEPoll)) {
							fc.utobj().clickElement(driver, pobj.canViewEPoll);
						}

					} else if (privileges.get("Can View EPoll").equalsIgnoreCase("No")) {

						if (fc.utobj().isSelected(driver, pobj.canViewEPoll)) {
							fc.utobj().clickElement(driver, pobj.canViewEPoll);
						}
					}
				} else if (s.contains("Can View Related Links")) {
					if (privileges.get("Can View Related Links").equalsIgnoreCase("Yes")) {

						if (!fc.utobj().isSelected(driver, pobj.canViewRelatedLinks)) {
							fc.utobj().clickElement(driver, pobj.canViewRelatedLinks);
						}

					} else if (privileges.get("Can View Related Links").equalsIgnoreCase("No")) {

						if (fc.utobj().isSelected(driver, pobj.canViewRelatedLinks)) {
							fc.utobj().clickElement(driver, pobj.canViewRelatedLinks);
						}
					}
				} else if (s.contains("Can View Alerts")) {
					if (privileges.get("Can View Alerts").equalsIgnoreCase("Yes")) {

						if (!fc.utobj().isSelected(driver, pobj.canViewAlerts)) {
							fc.utobj().clickElement(driver, pobj.canViewAlerts);
						}

					} else if (privileges.get("Can View Alerts").equalsIgnoreCase("No")) {

						if (fc.utobj().isSelected(driver, pobj.canViewAlerts)) {
							fc.utobj().clickElement(driver, pobj.canViewAlerts);
						}
					}
				} else if (s.contains("Can View News")) {
					if (privileges.get("Can View News").equalsIgnoreCase("Yes")) {

						if (!fc.utobj().isSelected(driver, pobj.canViewNews)) {
							fc.utobj().clickElement(driver, pobj.canViewNews);
						}

					} else if (privileges.get("Can View News").equalsIgnoreCase("No")) {

						if (fc.utobj().isSelected(driver, pobj.canViewNews)) {
							fc.utobj().clickElement(driver, pobj.canViewNews);
						}
					}
				} else if (s.contains("Can View Corporate Users")) {

					if (privileges.get("Can View Corporate Users").equalsIgnoreCase("Yes")) {

						if (!fc.utobj().isSelected(driver, pobj.canViewCorporateUsers)) {
							fc.utobj().clickElement(driver, pobj.canViewCorporateUsers);
						}

					} else if (privileges.get("Can View Corporate Users").equalsIgnoreCase("No")) {

						if (fc.utobj().isSelected(driver, pobj.canViewCorporateUsers)) {
							fc.utobj().clickElement(driver, pobj.canViewCorporateUsers);
						}
					}
				} else if (s.contains("Can View Franchise Users")) {
					if (privileges.get("Can View Franchise Users").equalsIgnoreCase("Yes")) {

						if (!fc.utobj().isSelected(driver, pobj.canViewFranchiseUsers)) {
							fc.utobj().clickElement(driver, pobj.canViewFranchiseUsers);
						}

					} else if (privileges.get("Can View Franchise Users").equalsIgnoreCase("No")) {

						if (fc.utobj().isSelected(driver, pobj.canViewFranchiseUsers)) {
							fc.utobj().clickElement(driver, pobj.canViewFranchiseUsers);
						}
					}
				} else if (s.contains("Can View Suppliers")) {
					if (privileges.get("Can View Suppliers").equalsIgnoreCase("Yes")) {

						if (!fc.utobj().isSelected(driver, pobj.canViewSuppliers)) {
							fc.utobj().clickElement(driver, pobj.canViewSuppliers);
						}

					} else if (privileges.get("Can View Suppliers").equalsIgnoreCase("No")) {

						if (fc.utobj().isSelected(driver, pobj.canViewSuppliers)) {
							fc.utobj().clickElement(driver, pobj.canViewSuppliers);
						}
					}
				} else if (s.contains("Can View Regional Users")) {
					if (privileges.get("Can View Regional Users").equalsIgnoreCase("Yes")) {

						if (!fc.utobj().isSelected(driver, pobj.canViewRegionalUsers)) {
							fc.utobj().clickElement(driver, pobj.canViewRegionalUsers);
						}

					} else if (privileges.get("Can View Regional Users").equalsIgnoreCase("No")) {

						if (fc.utobj().isSelected(driver, pobj.canViewRegionalUsers)) {
							fc.utobj().clickElement(driver, pobj.canViewRegionalUsers);
						}
					}
				} else if (s.contains("Can View Regional Users")) {
					if (privileges.get("Can View Regional Users").equalsIgnoreCase("Yes")) {

						if (!fc.utobj().isSelected(driver, pobj.canViewRegionalUsers)) {
							fc.utobj().clickElement(driver, pobj.canViewRegionalUsers);
						}

					} else if (privileges.get("Can View Regional Users").equalsIgnoreCase("No")) {

						if (fc.utobj().isSelected(driver, pobj.canViewRegionalUsers)) {
							fc.utobj().clickElement(driver, pobj.canViewRegionalUsers);
							
						}
					}
				}
			}
		}else if (moduleName.equalsIgnoreCase("CRM")) {
			
			Set<String> keySet = privileges.keySet();
			for (String s : keySet) {
				
				//VipinG
				if (s.contains("Can View Campaign Center")) {
					if (privileges.get("Can View Campaign Center").equalsIgnoreCase("Yes")) {

						if (!fc.utobj().isSelected(driver, pobj.Can_View_Campaign_Center)) {
							fc.utobj().clickElement(driver, pobj.Can_View_Campaign_Center);
						}

					} else if (privileges.get("Can View Campaign Center").equalsIgnoreCase("No")) {

						if (fc.utobj().isSelected(driver, pobj.Can_View_Campaign_Center)) {
							fc.utobj().clickElement(driver, pobj.Can_View_Campaign_Center);
						}
					}
				}
				//
				
				
				if (s.contains("Can Manage Form Generator")) {
					if (privileges.get("Can Manage Form Generator").equalsIgnoreCase("Yes")) {

						if (!fc.utobj().isSelected(driver, pobj.Can_Manage_Form_Generator)) {
							fc.utobj().clickElement(driver, pobj.Can_Manage_Form_Generator);
						}

					} else if (privileges.get("Can Manage Form Generator").equalsIgnoreCase("No")) {

						if (fc.utobj().isSelected(driver, pobj.Can_Manage_Form_Generator)) {
							fc.utobj().clickElement(driver, pobj.Can_Manage_Form_Generator);
						}
					}
				}
				
				if (s.contains("Can Configure Tab Generator")) {
					if (privileges.get("Can Configure Tab Generator").equalsIgnoreCase("Yes")) {

						if (!fc.utobj().isSelected(driver, pobj.Can_Configure_Tab_Generator)) {
							fc.utobj().clickElement(driver, pobj.Can_Configure_Tab_Generator);
						}

					} else if (privileges.get("Can Configure Tab Generator").equalsIgnoreCase("No")) {

						if (fc.utobj().isSelected(driver, pobj.Can_Configure_Tab_Generator)) {
							fc.utobj().clickElement(driver, pobj.Can_Configure_Tab_Generator);
						}
					}
				}
				
				if (s.contains("Can Manage Web Form Generator")) {
					if (privileges.get("Can Manage Web Form Generator").equalsIgnoreCase("Yes")) {

						if (!fc.utobj().isSelected(driver, pobj.Can_Manage_Web_Form_Generator)) {
							fc.utobj().clickElement(driver, pobj.Can_Manage_Web_Form_Generator);
						}

					} else if (privileges.get("Can Manage Web Form Generator").equalsIgnoreCase("No")) {

						if (fc.utobj().isSelected(driver, pobj.Can_Manage_Web_Form_Generator)) {
							fc.utobj().clickElement(driver, pobj.Can_Manage_Web_Form_Generator);
						}
					}
				}
				
				if (s.contains("Can View All Contacts")) {
					if (privileges.get("Can View All Contacts").equalsIgnoreCase("Yes")) {

						if (!fc.utobj().isSelected(driver, pobj.Can_View_All_Contacts)) {
							fc.utobj().clickElement(driver, pobj.Can_View_All_Contacts);
						}

					} else if (privileges.get("Can View All Contacts").equalsIgnoreCase("No")) {

						if (fc.utobj().isSelected(driver, pobj.Can_View_All_Contacts)) {
							fc.utobj().clickElement(driver, pobj.Can_View_All_Contacts);
						}
					}
				}
				
				if (s.contains("Can Add / Modify Contacts")) {
					if (privileges.get("Can Add / Modify Contacts").equalsIgnoreCase("Yes")) {

						if (!fc.utobj().isSelected(driver, pobj.Can_Add_Modify_Contacts)) {
							fc.utobj().clickElement(driver, pobj.Can_Add_Modify_Contacts);
						}

					} else if (privileges.get("Can Add / Modify Contacts").equalsIgnoreCase("No")) {

						if (fc.utobj().isSelected(driver, pobj.Can_Add_Modify_Contacts)) {
							fc.utobj().clickElement(driver, pobj.Can_Add_Modify_Contacts);
						}
					}
				}
				
				if (s.contains("Can Convert Contact to Lead")) {
					if (privileges.get("Can Convert Contact to Lead").equalsIgnoreCase("Yes")) {

						if (!fc.utobj().isSelected(driver, pobj.Can_Convert_Contact_to_Lead)) {
							fc.utobj().clickElement(driver, pobj.Can_Convert_Contact_to_Lead);
						}

					} else if (privileges.get("Can Convert Contact to Lead").equalsIgnoreCase("No")) {

						if (fc.utobj().isSelected(driver, pobj.Can_Convert_Contact_to_Lead)) {
							fc.utobj().clickElement(driver, pobj.Can_Convert_Contact_to_Lead);
						}
					}
				}
				
				if (s.contains("Can Delete Contacts")) {
					if (privileges.get("Can Delete Contacts").equalsIgnoreCase("Yes")) {

						if (!fc.utobj().isSelected(driver, pobj.Can_Delete_Contacts)) {
							fc.utobj().clickElement(driver, pobj.Can_Delete_Contacts);
						}

					} else if (privileges.get("Can Delete Contacts").equalsIgnoreCase("No")) {

						if (fc.utobj().isSelected(driver, pobj.Can_Delete_Contacts)) {
							fc.utobj().clickElement(driver, pobj.Can_Delete_Contacts);
						}
					}
				}
				
				if (s.contains("Can View All Opportunities")) {
					if (privileges.get("Can View All Opportunities").equalsIgnoreCase("Yes")) {

						if (!fc.utobj().isSelected(driver, pobj.Can_View_All_Opportunities)) {
							fc.utobj().clickElement(driver, pobj.Can_View_All_Opportunities);
						}

					} else if (privileges.get("Can View All Opportunities").equalsIgnoreCase("No")) {

						if (fc.utobj().isSelected(driver, pobj.Can_View_All_Opportunities)) {
							fc.utobj().clickElement(driver, pobj.Can_View_All_Opportunities);
						}
					}
				}
				
				if (s.contains("Can View All Contact Upload History")) {
					if (privileges.get("Can View All Contact Upload History").equalsIgnoreCase("Yes")) {

						if (!fc.utobj().isSelected(driver, pobj.Can_View_All_Contact_Upload_History)) {
							fc.utobj().clickElement(driver, pobj.Can_View_All_Contact_Upload_History);
						}

					} else if (privileges.get("Can View All Contact Upload History").equalsIgnoreCase("No")) {

						if (fc.utobj().isSelected(driver, pobj.Can_View_All_Contact_Upload_History)) {
							fc.utobj().clickElement(driver, pobj.Can_View_All_Contact_Upload_History);
						}
					}
				}
				
				if (s.contains("Can View All Leads")) {
					if (privileges.get("Can View All Leads").equalsIgnoreCase("Yes")) {

						if (!fc.utobj().isSelected(driver, pobj.Can_View_All_Leads)) {
							fc.utobj().clickElement(driver, pobj.Can_View_All_Leads);
						}

					} else if (privileges.get("Can View All Leads").equalsIgnoreCase("No")) {

						if (fc.utobj().isSelected(driver, pobj.Can_View_All_Leads)) {
							fc.utobj().clickElement(driver, pobj.Can_View_All_Leads);
						}
					}
				}
				
				if (s.contains("Can Add / Modify Leads")) {
					if (privileges.get("Can Add / Modify Leads").equalsIgnoreCase("Yes")) {

						if (!fc.utobj().isSelected(driver, pobj.Can_Add_Modify_Leads)) {
							fc.utobj().clickElement(driver, pobj.Can_Add_Modify_Leads);
						}

					} else if (privileges.get("Can Add / Modify Leads").equalsIgnoreCase("No")) {

						if (fc.utobj().isSelected(driver, pobj.Can_Add_Modify_Leads)) {
							fc.utobj().clickElement(driver, pobj.Can_Add_Modify_Leads);
						}
					}
				}
				
				if (s.contains("Can Convert Lead to Contact")) {
					if (privileges.get("Can Convert Lead to Contact").equalsIgnoreCase("Yes")) {

						if (!fc.utobj().isSelected(driver, pobj.Can_Convert_Lead_to_Contact)) {
							fc.utobj().clickElement(driver, pobj.Can_Convert_Lead_to_Contact);
						}

					} else if (privileges.get("Can Convert Lead to Contact").equalsIgnoreCase("No")) {

						if (fc.utobj().isSelected(driver, pobj.Can_Convert_Lead_to_Contact)) {
							fc.utobj().clickElement(driver, pobj.Can_Convert_Lead_to_Contact);
						}
					}
				}
				
				if (s.contains("Can Delete Leads")) {
					if (privileges.get("Can Delete Leads").equalsIgnoreCase("Yes")) {

						if (!fc.utobj().isSelected(driver, pobj.Can_Delete_Leads)) {
							fc.utobj().clickElement(driver, pobj.Can_Delete_Leads);
						}

					} else if (privileges.get("Can Delete Leads").equalsIgnoreCase("No")) {

						if (fc.utobj().isSelected(driver, pobj.Can_Delete_Leads)) {
							fc.utobj().clickElement(driver, pobj.Can_Delete_Leads);
						}
					}
				}
				
				if (s.contains("Can Manage Private Groups")) {
					if (privileges.get("Can Manage Private Groups").equalsIgnoreCase("Yes")) {

						if (!fc.utobj().isSelected(driver, pobj.Can_Manage_Private_Groups)) {
							fc.utobj().clickElement(driver, pobj.Can_Manage_Private_Groups);
						}

					} else if (privileges.get("Can Manage Private Groups").equalsIgnoreCase("No")) {

						if (fc.utobj().isSelected(driver, pobj.Can_Manage_Private_Groups)) {
							fc.utobj().clickElement(driver, pobj.Can_Manage_Private_Groups);
						}
					}
				}
				
				if (s.contains("Can View CM Document")) {
					if (privileges.get("Can View CM Document").equalsIgnoreCase("Yes")) {

						if (!fc.utobj().isSelected(driver, pobj.Can_View_CM_Document)) {
							fc.utobj().clickElement(driver, pobj.Can_View_CM_Document);
						}

					} else if (privileges.get("Can View CM Document").equalsIgnoreCase("No")) {

						if (fc.utobj().isSelected(driver, pobj.Can_View_CM_Document)) {
							fc.utobj().clickElement(driver, pobj.Can_View_CM_Document);
						}
					}
				}
				
				if (s.contains("Can Add / Modify CM Document")) {
					if (privileges.get("Can Add / Modify CM Document").equalsIgnoreCase("Yes")) {

						if (!fc.utobj().isSelected(driver, pobj.Can_Add_Modify_CM_Document)) {
							fc.utobj().clickElement(driver, pobj.Can_Add_Modify_CM_Document);
						}

					} else if (privileges.get("Can Add / Modify CM Document").equalsIgnoreCase("No")) {

						if (fc.utobj().isSelected(driver, pobj.Can_Add_Modify_CM_Document)) {
							fc.utobj().clickElement(driver, pobj.Can_Add_Modify_CM_Document);
						}
					}
				}
				
				if (s.contains("Can Delete CM Document")) {
					if (privileges.get("Can Delete CM Document").equalsIgnoreCase("Yes")) {

						if (!fc.utobj().isSelected(driver, pobj.Can_Delete_CM_Document)) {
							fc.utobj().clickElement(driver, pobj.Can_Delete_CM_Document);
						}

					} else if (privileges.get("Can Delete CM Document").equalsIgnoreCase("No")) {

						if (fc.utobj().isSelected(driver, pobj.Can_Delete_CM_Document)) {
							fc.utobj().clickElement(driver, pobj.Can_Delete_CM_Document);
						}
					}
				}
				
				if (s.contains("Can View Accounts")) {
					if (privileges.get("Can View Accounts").equalsIgnoreCase("Yes")) {

						if (!fc.utobj().isSelected(driver, pobj.Can_View_Accounts)) {
							fc.utobj().clickElement(driver, pobj.Can_View_Accounts);
						}

					} else if (privileges.get("Can View Accounts").equalsIgnoreCase("No")) {

						if (fc.utobj().isSelected(driver, pobj.Can_View_Accounts)) {
							fc.utobj().clickElement(driver, pobj.Can_View_Accounts);
						}
					}
				}
				
				if (s.contains("Can Add / Modify Accounts")) {
					if (privileges.get("Can Add / Modify Accounts").equalsIgnoreCase("Yes")) {

						if (!fc.utobj().isSelected(driver, pobj.Can_Add_Modify_Accounts)) {
							fc.utobj().clickElement(driver, pobj.Can_Add_Modify_Accounts);
						}

					} else if (privileges.get("Can Add / Modify Accounts").equalsIgnoreCase("No")) {

						if (fc.utobj().isSelected(driver, pobj.Can_Add_Modify_Accounts)) {
							fc.utobj().clickElement(driver, pobj.Can_Add_Modify_Accounts);
						}
					}
				}
				
				if (s.contains("Can Delete Accounts")) {
					if (privileges.get("Can Delete Accounts").equalsIgnoreCase("Yes")) {

						if (!fc.utobj().isSelected(driver, pobj.Can_Delete_Accounts)) {
							fc.utobj().clickElement(driver, pobj.Can_Delete_Accounts);
						}

					} else if (privileges.get("Can Delete Accounts").equalsIgnoreCase("No")) {

						if (fc.utobj().isSelected(driver, pobj.Can_Delete_Accounts)) {
							fc.utobj().clickElement(driver, pobj.Can_Delete_Accounts);
						}
					}
				}
				
				if (s.contains("Can View Tasks")) {
					if (privileges.get("Can View Tasks").equalsIgnoreCase("Yes")) {

						if (!fc.utobj().isSelected(driver, pobj.Can_View_Tasks)) {
							fc.utobj().clickElement(driver, pobj.Can_View_Tasks);
						}

					} else if (privileges.get("Can View Tasks").equalsIgnoreCase("No")) {

						if (fc.utobj().isSelected(driver, pobj.Can_View_Tasks)) {
							fc.utobj().clickElement(driver, pobj.Can_View_Tasks);
						}
					}
				}
				
				if (s.contains("Can Add / Modify Tasks")) {
					if (privileges.get("Can Add / Modify Tasks").equalsIgnoreCase("Yes")) {

						if (!fc.utobj().isSelected(driver, pobj.Can_Add_Modify_Tasks)) {
							fc.utobj().clickElement(driver, pobj.Can_Add_Modify_Tasks);
						}

					} else if (privileges.get("Can Add / Modify Tasks").equalsIgnoreCase("No")) {

						if (fc.utobj().isSelected(driver, pobj.Can_Add_Modify_Tasks)) {
							fc.utobj().clickElement(driver, pobj.Can_Add_Modify_Tasks);
						}
					}
				}
				
				if (s.contains("Can Delete Tasks")) {
					if (privileges.get("Can Delete Tasks").equalsIgnoreCase("Yes")) {

						if (!fc.utobj().isSelected(driver, pobj.Can_Delete_Tasks)) {
							fc.utobj().clickElement(driver, pobj.Can_Delete_Tasks);
						}

					} else if (privileges.get("Can Delete Tasks").equalsIgnoreCase("No")) {

						if (fc.utobj().isSelected(driver, pobj.Can_Delete_Tasks)) {
							fc.utobj().clickElement(driver, pobj.Can_Delete_Tasks);
						}
					}
				}
				
				if (s.contains("Can View Transaction")) {
					if (privileges.get("Can View Transaction").equalsIgnoreCase("Yes")) {

						if (!fc.utobj().isSelected(driver, pobj.Can_View_Transaction)) {
							fc.utobj().clickElement(driver, pobj.Can_View_Transaction);
						}

					} else if (privileges.get("Can View Transaction").equalsIgnoreCase("No")) {

						if (fc.utobj().isSelected(driver, pobj.Can_View_Transaction)) {
							fc.utobj().clickElement(driver, pobj.Can_View_Transaction);
						}
					}
				}
				
				if (s.contains("Can Add/Modify Transaction")) {
					if (privileges.get("Can Add/Modify Transaction").equalsIgnoreCase("Yes")) {

						if (!fc.utobj().isSelected(driver, pobj.Can_Add_Modify_Transaction)) {
							fc.utobj().clickElement(driver, pobj.Can_Add_Modify_Transaction);
						}

					} else if (privileges.get("Can Add/Modify Transaction").equalsIgnoreCase("No")) {

						if (fc.utobj().isSelected(driver, pobj.Can_Add_Modify_Transaction)) {
							fc.utobj().clickElement(driver, pobj.Can_Add_Modify_Transaction);
						}
					}
				}
				
				if (s.contains("Can Delete Transaction")) {
					if (privileges.get("Can Delete Transaction").equalsIgnoreCase("Yes")) {

						if (!fc.utobj().isSelected(driver, pobj.Can_Delete_Transaction)) {
							fc.utobj().clickElement(driver, pobj.Can_Delete_Transaction);
						}

					} else if (privileges.get("Can Delete Transaction").equalsIgnoreCase("No")) {

						if (fc.utobj().isSelected(driver, pobj.Can_Delete_Transaction)) {
							fc.utobj().clickElement(driver, pobj.Can_Delete_Transaction);
						}
					}
				}
				
				if (s.contains("Can Add / Modify / Delete Media in Media Library")) {
					if (privileges.get("Can Add / Modify / Delete Media in Media Library").equalsIgnoreCase("Yes")) {

						if (!fc.utobj().isSelected(driver, pobj.Can_Add_Modify_Delete_Media_in_Media_Library)) {
							fc.utobj().clickElement(driver, pobj.Can_Add_Modify_Delete_Media_in_Media_Library);
						}

					} else if (privileges.get("Can Add / Modify / Delete Media in Media Library").equalsIgnoreCase("No")) {

						if (fc.utobj().isSelected(driver, pobj.Can_Add_Modify_Delete_Media_in_Media_Library)) {
							fc.utobj().clickElement(driver, pobj.Can_Add_Modify_Delete_Media_in_Media_Library);
						}
					}
				}
				
				
				if (s.contains("Can View Email Subscription Report")) {
					if (privileges.get("Can View Email Subscription Report").equalsIgnoreCase("Yes")) {

						if (!fc.utobj().isSelected(driver, pobj.Can_View_Email_Subscription_Report)) {
							fc.utobj().clickElement(driver, pobj.Can_View_Email_Subscription_Report);
						}

					} else if (privileges.get("Can View Email Subscription Report").equalsIgnoreCase("No")) {

						if (fc.utobj().isSelected(driver, pobj.Can_View_Email_Subscription_Report)) {
							fc.utobj().clickElement(driver, pobj.Can_View_Email_Subscription_Report);
						}
					}
				}
				
				if (s.contains("Can View Private Folders / Templates of Other Users")) {
					if (privileges.get("Can View Private Folders / Templates of Other Users ").equalsIgnoreCase("Yes")) {

						if (!fc.utobj().isSelected(driver, pobj.Can_View_Private_Campaign_Tempaltes_Other_User)) {
							fc.utobj().clickElement(driver, pobj.Can_View_Private_Campaign_Tempaltes_Other_User);
						}

					} else if (privileges.get("Can View Private Folders / Templates of Other Users ").equalsIgnoreCase("No")) {

						if (fc.utobj().isSelected(driver, pobj.Can_View_Private_Campaign_Tempaltes_Other_User)) {
							fc.utobj().clickElement(driver, pobj.Can_View_Private_Campaign_Tempaltes_Other_User);
						}
					}
				}
				
				
				
				
				
				
			}
		}

		fc.utobj().clickElement(driver, pobj.submit);
		AdminUsersRolesAddRoleAddUsersforRolePageTest p2 = new AdminUsersRolesAddRoleAddUsersforRolePageTest();
		p2.assignLater(driver);

		fc.utobj().printTestStep("Verify The Added Role");
		boolean isTextPresent = fc.utobj().assertPageSource(driver, roleName);
		if (isTextPresent == false) {
			fc.utobj().throwsException("Added Corporate Role is not present at Admin > Users > Roles Page");
		}
			
	}
		
	public void addRoles(WebDriver driver,Roles roles) throws Exception {

		fc.utobj().printTestStep("Admin > Users > Roles > Add New Role");
		fc.adminpage().adminUsersRolesAddNewRolePage(driver);
		AdminUsersRolesAddNewRolePage pobj = new AdminUsersRolesAddNewRolePage(driver);

		if (roles.getRoleType().equalsIgnoreCase("Corporate")) {
			fc.utobj().selectDropDownByValue(driver, pobj.roleType, "1");

		} else if (roles.getRoleType().equalsIgnoreCase("Division")) {
			fc.utobj().selectDropDownByValue(driver, pobj.roleType, "5");

		} else if (roles.getRoleType().equalsIgnoreCase("Regional")) {
			fc.utobj().selectDropDownByValue(driver, pobj.roleType, "2");

		} else if (roles.getRoleType().equalsIgnoreCase("Franchise")) {
			fc.utobj().selectDropDownByValue(driver, pobj.roleType, "0");
		}

		fc.utobj().sendKeys(driver, pobj.roleName, roles.getRoleName());

		fc.utobj().clickElement(driver, pobj.submit);
		AdminUsersRolesAddRoleAddUsersforRolePageTest p2 = new AdminUsersRolesAddRoleAddUsersforRolePageTest();
		p2.assignLater(driver);

		fc.utobj().printTestStep("Verify The Added Role");
		boolean isTextPresent = fc.utobj().assertPageSource(driver, roles.getRoleName());
		if (isTextPresent == false) {
			fc.utobj().throwsException("Added Role is not present at Admin > Users > Roles Page");
		}
	}
}
