package com.builds.test.admin;

import org.openqa.selenium.WebDriver;

import com.builds.uimaps.admin.AdminUsersRolesAddRoleAddUsersforRolePage;
import com.builds.utilities.FranconnectUtil;

public class AdminUsersRolesAddRoleAddUsersforRolePageTest {
	FranconnectUtil fc = new FranconnectUtil();

	public void assignLater(WebDriver driver) throws Exception {

		AdminUsersRolesAddRoleAddUsersforRolePage pobj = new AdminUsersRolesAddRoleAddUsersforRolePage(driver);
		fc.utobj().clickElement(driver, pobj.assignLater);
	}
}
