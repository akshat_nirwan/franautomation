package com.builds.test.admin;

import org.openqa.selenium.WebDriver;

import com.builds.uimaps.admin.AdminUsersRolesPage;
import com.builds.utilities.FranconnectUtil;

public class AdminUsersRolesPageTest {
	FranconnectUtil fc = new FranconnectUtil();

	public void openAddRolesPage(WebDriver driver) throws Exception {

		AdminUsersRolesPage pobj = new AdminUsersRolesPage(driver);
		// Actions actobj=new Actions(Browser.driver);
		// actobj.moveToElement(pobj.addNewRoleBtn);
		fc.utobj().clickElement(driver, pobj.addNewRoleBtn);
	}
}
