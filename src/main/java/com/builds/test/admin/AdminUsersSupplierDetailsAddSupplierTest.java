package com.builds.test.admin;

import java.util.Map;

import org.openqa.selenium.WebDriver;

import com.builds.uimaps.admin.AdminUsersSupplierDetailsAddSupplierPage;
import com.builds.utilities.FranconnectUtil;

public class AdminUsersSupplierDetailsAddSupplierTest {
	FranconnectUtil fc = new FranconnectUtil();

	public String addSupplier(WebDriver driver, String supplierName, Map<String, String> config, String emailId)
			throws Exception {

		String testCaseId = "TC_addSupplier_01";
		if (fc.utobj().validate(testCaseId)) {
			try {
				fc.adminpage().adminUsersSupplierDetailsAddSupplier(driver);
				AdminUsersSupplierDetailsAddSupplierPage pobj = new AdminUsersSupplierDetailsAddSupplierPage(driver);

				fc.utobj().sendKeys(driver, pobj.supplierName, supplierName);
				fc.utobj().selectDropDown(driver, pobj.rating, "Vendor");

				fc.utobj().sendKeys(driver, pobj.firstName, "test");
				fc.utobj().sendKeys(driver, pobj.lastName, "test");
				fc.utobj().sendKeys(driver, pobj.address, "test");
				fc.utobj().sendKeys(driver, pobj.city, "test1");

				fc.utobj().selectDropDown(driver, pobj.country, "USA");

				fc.utobj().sendKeys(driver, pobj.zipcode, "test");
				fc.utobj().selectDropDown(driver, pobj.state, "Alabama");

				fc.utobj().sendKeys(driver, pobj.phone1, "99877897889");
				fc.utobj().sendKeys(driver, pobj.phone1Extn, "234");
				fc.utobj().sendKeys(driver, pobj.emailID, emailId);

				// fc.utobj().selectDropDown(driver,pobj.vendorType, "Corporate
				// Vendor");
				
				if (!fc.utobj().isSelected(driver, pobj.viewableByFranchisees)) {
					fc.utobj().clickElement(driver, pobj.viewableByFranchisees);
				}

				fc.utobj().clickElement(driver, pobj.radioNo);
				String categoryName = fc.utobj().generateTestData("CatName");
				fc.utobj().sendKeys(driver, pobj.categoryNewTxBx, categoryName);

				fc.utobj().clickElement(driver, pobj.submit);

				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException("was not able to add Supplier");

		}
		return supplierName;
	}
}
