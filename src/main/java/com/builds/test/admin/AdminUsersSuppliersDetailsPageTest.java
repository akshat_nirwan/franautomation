package com.builds.test.admin;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Test;

import com.builds.uimaps.admin.AdminUsersSuppliersDetailsPage;
import com.builds.utilities.FranconnectUtil;

public class AdminUsersSuppliersDetailsPageTest {
	FranconnectUtil fc = new FranconnectUtil();

	@Test
	public void addNewSupplier(WebDriver driver) throws Exception {

		AdminUsersSuppliersDetailsPage pobj = new AdminUsersSuppliersDetailsPage(driver);
		fc.utobj().clickElement(driver, pobj.addNewSupplier);
	}
}
