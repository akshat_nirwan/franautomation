package com.builds.test.admin;

import org.openqa.selenium.WebDriver;

import com.builds.test.common.Location;
import com.builds.uimaps.admin.LocationUI;
import com.builds.utilities.FranconnectUtil;

public class LocationData {

	public LocationData filllocationInfo(WebDriver driver, Location loc) throws Exception {
		LocationUI ui = new LocationUI(driver);
		FranconnectUtil fc = new FranconnectUtil();

		fc.utobj().printTestStep("Fill Location Info");

		if (loc.getFranchiseID() != null) {
			fc.utobj().sendKeys(driver, ui.FranchiseID, loc.getFranchiseID());
		}
		if (loc.getCenterName() != null) {
			fc.utobj().sendKeys(driver, ui.CenterName, loc.getCenterName());
		}
		if (loc.getAreaRegion() != null) {
			fc.utobj().selectDropDown(driver, ui.AreaRegion, loc.getAreaRegion());
		}
		if (loc.getBrands() != null) {
			fc.utobj().selectDropDown(driver, ui.Brands, loc.getBrands());
		}

		if (loc.getLicenseNo() != null) {
			fc.utobj().sendKeys(driver, ui.LicenseNo, loc.getLicenseNo());
		}

		if (loc.getStoreType() != null) {
			fc.utobj().selectDropDown(driver, ui.StoreType, loc.getStoreType());
		}

		if (loc.getCorporateLocation() != null) {
			fc.utobj().selectDropDown(driver, ui.CorporateLocation, loc.getCorporateLocation());
		}

		if (loc.getAgreementVersion() != null) {
			fc.utobj().selectDropDown(driver, ui.AgreementVersion, loc.getAgreementVersion());
		}
		if (loc.getRoyaltyReportingStartDate() != null) {
			fc.utobj().sendKeys(driver, ui.RoyaltyReportingStartDate, loc.getRoyaltyReportingStartDate());
		}

		if (loc.getTaxRate() != null) {
			fc.utobj().sendKeys(driver, ui.TaxRate, loc.getTaxRate());
		}
		if (loc.getFBC() != null) {
			fc.utobj().selectDropDown(driver, ui.FBC, loc.getFBC());
		}
		if (loc.getOpeningDate() != null) {
			fc.utobj().sendKeys(driver, ui.OpeningDate, loc.getOpeningDate());
		}

		if (loc.getStreetAddress() != null) {
			fc.utobj().sendKeys(driver, ui.StreetAddress, loc.getStreetAddress());
		}
		if (loc.getAddress2() != null) {
			fc.utobj().sendKeys(driver, ui.Address2, loc.getAddress2());
		}

		if (loc.getCity() != null) {
			fc.utobj().sendKeys(driver, ui.City, loc.getCity());
		}

		if (loc.getCountry() != null) {
			fc.utobj().selectDropDown(driver, ui.Country, loc.getCountry());
		}

		if (loc.getState() != null) {
			fc.utobj().selectDropDown(driver, ui.State, loc.getState());
		}

		if (loc.getZipCode() != null) {
			fc.utobj().sendKeys(driver, ui.ZipCode, loc.getZipCode());
		}

		if (loc.getPhone() != null) {
			fc.utobj().sendKeys(driver, ui.Phone, loc.getPhone());
		}

		if (loc.getPhoneExtension() != null) {
			fc.utobj().sendKeys(driver, ui.PhoneExtension, loc.getPhoneExtension());
		}
		if (loc.getFax() != null) {
			fc.utobj().sendKeys(driver, ui.Fax, loc.getFax());
		}
		if (loc.getMobile() != null) {
			fc.utobj().sendKeys(driver, ui.Mobile, loc.getMobile());
		}
		if (loc.getEmail() != null) {
			fc.utobj().sendKeys(driver, ui.Email, loc.getEmail());
		}
		if (loc.getWebsite() != null) {
			if (fc.utobj().isElementPresent(driver, ui.Website)) {
				fc.utobj().sendKeys(driver, ui.Website, loc.getWebsite());
			}
		}

		if (loc.getTitle() != null) {
			fc.utobj().selectDropDown(driver, ui.Title, loc.getTitle());
		}
		if (loc.getFirstName() != null) {
			fc.utobj().sendKeys(driver, ui.FirstName, loc.getFirstName());
		}
		if (loc.getLastName() != null) {
			fc.utobj().sendKeys(driver, ui.LastName, loc.getLastName());
		}
		if (loc.getCenterPhone() != null) {
			fc.utobj().sendKeys(driver, ui.CenterPhone, loc.getCenterPhone());
		}
		if (loc.getCenterPhoneExtesion() != null) {
			fc.utobj().sendKeys(driver, ui.CenterPhoneExtesion, loc.getCenterPhoneExtesion());
		}
		if (loc.getCenterEmail() != null) {
			fc.utobj().sendKeys(driver, ui.CenterEmail, loc.getCenterEmail());
		}
		if (loc.getCenterFax() != null) {
			fc.utobj().sendKeys(driver, ui.CenterFax, loc.getCenterFax());
		}
		if (loc.getCenterMobile() != null) {
			fc.utobj().sendKeys(driver, ui.CenterMobile, loc.getCenterMobile());
		}

		if (loc.getOwnerTypeIndividual() != null) {
			fc.utobj().clickElement(driver, ui.OwnerTypeIndividual);

		} else if (loc.getOwnerTypeIndividualEntity() != null) {
			fc.utobj().clickElement(driver, ui.OwnerTypeIndividualEntity);

			if (loc.getNewEntity() != null) {
				fc.utobj().clickElement(driver, ui.NewEntity);

				if (loc.getEntityType() != null) {
					fc.utobj().selectDropDown(driver, ui.EntityType, loc.getEntityType());
				}

				if (loc.getEntityName() != null) {
					fc.utobj().sendKeys(driver, ui.EntityName, loc.getEntityName());
				}

				if (loc.getTaxpayerID() != null) {
					fc.utobj().sendKeys(driver, ui.TaxpayerID, loc.getTaxpayerID());
				}

				if (loc.getCountryofFormationResidency() != null) {
					fc.utobj().selectDropDown(driver, ui.CountryofFormationResidency,
							loc.getCountryofFormationResidency());
				}

				if (loc.getStateofFormationResidency() != null) {
					fc.utobj().selectDropDown(driver, ui.StateofFormationResidency, loc.getStateofFormationResidency());

				}
			} else if (loc.getExistingEntity() != null) {
				fc.utobj().clickElement(driver, ui.ExistingEntity);

				if (loc.getExistingEntityName() != null) {
					// fc.utobj().selectDropDown(driver, ui.ExistingEntityName,
					// loc.getExistingEntityName());
					fc.utobj().selectDropDown(driver, ui.ExistingEntityName, loc.getExistingEntityName());
				}
			}
		}

		if (loc.getNewOwner() != null) {
			fc.utobj().clickElement(driver, ui.NewOwner);

			if (loc.getSalutation() != null) {
				fc.utobj().selectDropDown(driver, ui.Salutation, loc.getSalutation());
			}

			if (loc.getOwnerFirstName() != null) {
				fc.utobj().sendKeys(driver, ui.OwnerFirstName, loc.getOwnerFirstName());
			}
			if (loc.getOwnerLastName() != null) {
				fc.utobj().sendKeys(driver, ui.OwnerLastName, loc.getOwnerLastName());
			}
		} else if (loc.getExistingOwner() != null) {
			fc.utobj().clickElement(driver, ui.ExistingOwner);

			if (loc.getSelectOwners() != null) {
				fc.utobj().selectValFromMultiSelectwithoutSelectAll(driver, ui.SelectOwners, loc.getSelectOwners());
			}
			if (loc.getMUID() != null) {
				fc.utobj().sendKeys(driver, ui.MUID, loc.getMUID());

			}

		}

		return this;
	}

	public LocationData ModifylocationAndAddEntity(WebDriver driver, Location loc) throws Exception {
		LocationUI ui = new LocationUI(driver);
		FranconnectUtil fc = new FranconnectUtil();

		fc.utobj().printTestStep("Modify Location and Add Entity");

		if (loc.getNewEntity() != null) {
			fc.utobj().clickElement(driver, ui.NewEntity);

			if (loc.getEntityType() != null) {
				fc.utobj().selectDropDown(driver, ui.EntityType, loc.getEntityType());
			}

			if (loc.getEntityName() != null) {
				fc.utobj().sendKeys(driver, ui.EntityName, loc.getEntityName());
			}

			if (loc.getTaxpayerID() != null) {
				fc.utobj().sendKeys(driver, ui.TaxpayerID, loc.getTaxpayerID());
			}

			if (loc.getCountryofFormationResidency() != null) {
				fc.utobj().selectDropDown(driver, ui.CountryofFormationResidency, loc.getCountryofFormationResidency());
			}

			if (loc.getStateofFormationResidency() != null) {
				fc.utobj().selectDropDown(driver, ui.StateofFormationResidency, loc.getStateofFormationResidency());

			}
		} else if (loc.getExistingEntity() != null) {
			fc.utobj().clickElement(driver, ui.ExistingEntity);

			if (loc.getExistingEntityName() != null) {
				fc.utobj().selectDropDown(driver, ui.ExistingEntityName, loc.getExistingEntityName());
			}
		}
		return this;
	}

	public LocationData Modifylocationinfo(WebDriver driver, Location loc) throws Exception {
		LocationUI ui = new LocationUI(driver);
		FranconnectUtil fc = new FranconnectUtil();

		fc.utobj().printTestStep("Modify Location Info");

		if (loc.getFranchiseID() != null) {
			fc.utobj().sendKeys(driver, ui.FranchiseID, loc.getFranchiseID());
		}
		if (loc.getCenterName() != null) {
			fc.utobj().sendKeys(driver, ui.CenterName, loc.getCenterName());
		}
		if (loc.getAreaRegion() != null) {
			fc.utobj().selectDropDown(driver, ui.AreaRegion, loc.getAreaRegion());
		}
		if (loc.getBrands() != null) {
			fc.utobj().selectDropDown(driver, ui.Brands, loc.getBrands());
		}

		if (loc.getLicenseNo() != null) {
			fc.utobj().sendKeys(driver, ui.LicenseNo, loc.getLicenseNo());
		}

		if (loc.getStoreType() != null) {
			fc.utobj().sendKeys(driver, ui.StoreType, loc.getStoreType());
		}

		if (loc.getCorporateLocation() != null) {
			fc.utobj().selectDropDown(driver, ui.CorporateLocation, loc.getCorporateLocation());
		}

		if (loc.getAgreementVersion() != null) {
			fc.utobj().selectDropDown(driver, ui.AgreementVersion, loc.getAgreementVersion());
		}
		if (loc.getRoyaltyReportingStartDate() != null) {
			fc.utobj().sendKeys(driver, ui.RoyaltyReportingStartDate, loc.getRoyaltyReportingStartDate());
		}

		if (loc.getTaxRate() != null) {
			fc.utobj().sendKeys(driver, ui.TaxRate, loc.getTaxRate());
		}
		if (loc.getFBC() != null) {
			fc.utobj().sendKeys(driver, ui.FBC, loc.getFBC());
		}
		if (loc.getOpeningDate() != null) {
			fc.utobj().sendKeys(driver, ui.OpeningDate, loc.getOpeningDate());
		}

		if (loc.getStreetAddress() != null) {
			fc.utobj().sendKeys(driver, ui.StreetAddress, loc.getStreetAddress());
		}
		if (loc.getAddress2() != null) {
			fc.utobj().sendKeys(driver, ui.Address2, loc.getAddress2());
		}

		if (loc.getCity() != null) {
			fc.utobj().sendKeys(driver, ui.City, loc.getCity());
		}

		if (loc.getCountry() != null) {
			fc.utobj().selectDropDown(driver, ui.Country, loc.getCountry());
		}

		if (loc.getState() != null) {
			fc.utobj().selectDropDown(driver, ui.State, loc.getState());
		}

		if (loc.getZipCode() != null) {
			fc.utobj().sendKeys(driver, ui.ZipCode, loc.getZipCode());
		}

		if (loc.getPhone() != null) {
			fc.utobj().sendKeys(driver, ui.Phone, loc.getPhone());
		}

		if (loc.getPhoneExtension() != null) {
			fc.utobj().sendKeys(driver, ui.PhoneExtension, loc.getPhoneExtension());
		}
		if (loc.getFax() != null) {
			fc.utobj().sendKeys(driver, ui.Fax, loc.getFax());
		}
		if (loc.getMobile() != null) {
			fc.utobj().sendKeys(driver, ui.Mobile, loc.getMobile());
		}
		if (loc.getEmail() != null) {
			fc.utobj().sendKeys(driver, ui.Email, loc.getEmail());
		}
		if (loc.getWebsite() != null) {
			fc.utobj().sendKeys(driver, ui.Website, loc.getWebsite());
		}

		if (loc.getTitle() != null) {
			fc.utobj().selectDropDown(driver, ui.Title, loc.getTitle());
		}
		if (loc.getFirstName() != null) {
			fc.utobj().sendKeys(driver, ui.FirstName, loc.getFirstName());
		}
		if (loc.getLastName() != null) {
			fc.utobj().sendKeys(driver, ui.LastName, loc.getLastName());
		}
		if (loc.getCenterPhone() != null) {
			fc.utobj().sendKeys(driver, ui.CenterPhone, loc.getCenterPhone());
		}
		if (loc.getCenterPhoneExtesion() != null) {
			fc.utobj().sendKeys(driver, ui.CenterPhoneExtesion, loc.getCenterPhoneExtesion());
		}
		if (loc.getCenterEmail() != null) {
			fc.utobj().sendKeys(driver, ui.CenterEmail, loc.getCenterEmail());
		}
		if (loc.getCenterMobile() != null) {
			fc.utobj().sendKeys(driver, ui.CenterMobile, loc.getCenterMobile());
		}
		if (loc.getCenterFax() != null) {
			fc.utobj().sendKeys(driver, ui.CenterFax, loc.getCenterFax());
		}

		if (loc.getEntityType() != null) {
			fc.utobj().selectDropDown(driver, ui.EntityType, loc.getEntityType());
		}

		if (loc.getEntityName() != null) {
			fc.utobj().sendKeys(driver, ui.EntityName, loc.getEntityName());
		}

		/*
		 * if (loc.getTaxpayerID() != null) { fc.utobj().sendKeys(driver, ui.TaxpayerID,
		 * loc.getTaxpayerID()); }
		 */

		if (loc.getCountryofFormationResidency() != null) {
			fc.utobj().selectDropDown(driver, ui.CountryofFormationResidency, loc.getCountryofFormationResidency());
		}

		if (loc.getStateofFormationResidency() != null) {
			fc.utobj().selectDropDown(driver, ui.StateofFormationResidency, loc.getStateofFormationResidency());

		}

		return this;
	}

	public void submit(WebDriver driver) throws Exception {
		FranconnectUtil fc = new FranconnectUtil();

		LocationUI ui = new LocationUI(driver);

		fc.utobj().printTestStep("Click Submit");

		fc.utobj().clickElement(driver, ui.Submit);

	}

	public void Cancel(WebDriver driver) throws Exception {
		FranconnectUtil fc = new FranconnectUtil();

		LocationUI ui = new LocationUI(driver);

		fc.utobj().printTestStep("Click Cancel");

		fc.utobj().clickElement(driver, ui.Cancel);

	}
}
