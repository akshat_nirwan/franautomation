package com.builds.test.admin;

import org.openqa.selenium.WebDriver;

import com.builds.uimaps.admin.ManageCorporateUsersTestingUI;
import com.builds.utilities.FranconnectUtil;

public class ManageCorporateUsers_Testing {

	
	public void clickAddCorporateUser(WebDriver driver) throws Exception
	{
		ManageCorporateUsersTestingUI ui = new ManageCorporateUsersTestingUI(driver);
		
		FranconnectUtil fc = new FranconnectUtil();
		fc.utobj().printTestStep("Click on Add Corporate User Button");
		fc.utobj().clickElement(driver, ui.addCorporateUserButton);
	}
	
}
