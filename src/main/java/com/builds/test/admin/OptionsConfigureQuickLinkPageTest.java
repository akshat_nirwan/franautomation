package com.builds.test.admin;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.Reporter;

import com.builds.uimaps.common.CommonUI;
import com.builds.utilities.FranconnectUtil;

public class OptionsConfigureQuickLinkPageTest {

	FranconnectUtil fc = new FranconnectUtil();

	public void configureQuickLnk(WebDriver driver, String linkToBeDisplayed, boolean option) throws Exception {

		Reporter.log("Navigate To Options > Quick Link and configure Lnk");
		fc.adminpage().openOptionsQuickLinks(driver);

		WebElement element = fc.utobj().getElementByXpath(driver,
				".//td[contains(text(),'" + linkToBeDisplayed + "')]/following-sibling::td/input[@name='selectLink']");

		if (option == true) {
			if (!fc.utobj().isSelected(driver,element)) {
				fc.utobj().clickElement(driver, element);
			}
		} else if (option == false) {
			if (fc.utobj().isSelected(driver,element)) {
				fc.utobj().clickElement(driver, element);
			}
		}

		fc.utobj().clickElement(driver, new CommonUI(driver).Save_Input_ByValue);
	}
}
