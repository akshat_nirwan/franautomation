package com.builds.test.admin;

public class Roles {
	private String roleType;
	private String roleName;
	
	public void setRoleType(String roleType){
		this.roleType=roleType;
	}
	
	public String getRoleType(){
		return this.roleType;
	}
	
	public void setRoleName(String roleName){
		this.roleName=roleName;
	}
	
	public String getRoleName(){
		return this.roleName;
	}
}

