package com.builds.test.admin;

import org.openqa.selenium.WebDriver;

import com.builds.uimaps.admin.AddNewRoleUI;
import com.builds.uimaps.admin.AddUsersForRoleUI;
import com.builds.uimaps.admin.RolesUI;
import com.builds.utilities.FranconnectUtil;

public class RolesTest {
	
	/*
	 *  Akshat
	 */
	
	WebDriver driver;
	FranconnectUtil fc = new FranconnectUtil();
	
	public RolesTest(WebDriver driver) {
		this.driver = driver;
	}
	
	public void addNewRole_DivisionLevel_CannotAccessAllLeads(Roles roles) throws Exception
	{
		RolesUI rolesUI = new RolesUI(driver);
		AddNewRoleUI addNewRoleUI = new AddNewRoleUI(driver);
		fc.utobj().clickElement(driver, rolesUI.addNewRole_Btn);
		
		fc.utobj().selectDropDown(driver, addNewRoleUI.roleType_DropDown, roles.getRoleType());
		
		fc.utobj().sendKeys(driver, addNewRoleUI.roleName_TextBox, roles.getRoleName());
		if(fc.utobj().isSelected(driver, addNewRoleUI.canAccessAllLeads_checkBox))
		{
			fc.utobj().clickElement(driver, addNewRoleUI.canAccessAllLeads_checkBox);
		}
		
		fc.utobj().clickElement(driver, addNewRoleUI.submit_Btn);
		AddUsersForRoleUI addUsersForRoleUI = new AddUsersForRoleUI(driver);
		fc.utobj().clickElement(driver, addUsersForRoleUI.assignLater_Btn);
	}

	public void addNewRole_AnyLevel_CannotAccessAllLeads(Roles roles) throws Exception
	{
		RolesUI rolesUI = new RolesUI(driver);
		AddNewRoleUI addNewRoleUI = new AddNewRoleUI(driver);
		fc.utobj().clickElement(driver, rolesUI.addNewRole_Btn);
		
		fc.utobj().selectDropDown(driver, addNewRoleUI.roleType_DropDown, roles.getRoleType());
		
		fc.utobj().sendKeys(driver, addNewRoleUI.roleName_TextBox, roles.getRoleName());
		
		String privilegeDescription = "Can access all leads";
		
		if(fc.utobj().isSelected(driver, addNewRoleUI.checkBoxAgainst_PrivilegeDescription(driver, privilegeDescription)))
		{
			fc.utobj().clickElement(driver, addNewRoleUI.checkBoxAgainst_PrivilegeDescription(driver, privilegeDescription));
		}
		
		fc.utobj().clickElement(driver, addNewRoleUI.submit_Btn);
		AddUsersForRoleUI addUsersForRoleUI = new AddUsersForRoleUI(driver);
		fc.utobj().clickElement(driver, addUsersForRoleUI.assignLater_Btn);
	}
	
	public void addNewRole_AnyLevel_CannotAccessReports(Roles roles) throws Exception
	{
		RolesUI rolesUI = new RolesUI(driver);
		AddNewRoleUI addNewRoleUI = new AddNewRoleUI(driver);
		fc.utobj().clickElement(driver, rolesUI.addNewRole_Btn);
		
		fc.utobj().selectDropDown(driver, addNewRoleUI.roleType_DropDown, roles.getRoleType());
		
		fc.utobj().sendKeys(driver, addNewRoleUI.roleName_TextBox, roles.getRoleName());
		
		String privilegeDescription = "Grants access to view Reports ";
		
		if(fc.utobj().isSelected(driver, addNewRoleUI.checkBoxAgainst_PrivilegeDescription(driver, privilegeDescription)))
		{
			fc.utobj().clickElement(driver, addNewRoleUI.checkBoxAgainst_PrivilegeDescription(driver, privilegeDescription));
		}
		
		fc.utobj().clickElement(driver, addNewRoleUI.submit_Btn);
		AddUsersForRoleUI addUsersForRoleUI = new AddUsersForRoleUI(driver);
		fc.utobj().clickElement(driver, addUsersForRoleUI.assignLater_Btn);
	}
	
	public void addNewRole_AnyLevel_CannotAddNewLead(Roles roles) throws Exception
	{
		RolesUI rolesUI = new RolesUI(driver);
		AddNewRoleUI addNewRoleUI = new AddNewRoleUI(driver);
		fc.utobj().clickElement(driver, rolesUI.addNewRole_Btn);
		
		fc.utobj().selectDropDown(driver, addNewRoleUI.roleType_DropDown, roles.getRoleType());
		
		fc.utobj().sendKeys(driver, addNewRoleUI.roleName_TextBox, roles.getRoleName());
		
		String privilegeDescription = "Grants Privilege to Add New Lead ";
		
		if(fc.utobj().isSelected(driver, addNewRoleUI.checkBoxAgainst_PrivilegeDescription(driver, privilegeDescription)))
		{
			fc.utobj().clickElement(driver, addNewRoleUI.checkBoxAgainst_PrivilegeDescription(driver, privilegeDescription));
		}
		
		fc.utobj().clickElement(driver, addNewRoleUI.submit_Btn);
		AddUsersForRoleUI addUsersForRoleUI = new AddUsersForRoleUI(driver);
		fc.utobj().clickElement(driver, addUsersForRoleUI.assignLater_Btn);
	}
	
	public void addNewRole_AnyLevel_CannotDeleteLead(Roles roles) throws Exception
	{
		RolesUI rolesUI = new RolesUI(driver);
		AddNewRoleUI addNewRoleUI = new AddNewRoleUI(driver);
		fc.utobj().clickElement(driver, rolesUI.addNewRole_Btn);
		
		fc.utobj().selectDropDown(driver, addNewRoleUI.roleType_DropDown, roles.getRoleType());
		
		fc.utobj().sendKeys(driver, addNewRoleUI.roleName_TextBox, roles.getRoleName());
		
		String privilegeDescription = "Grants Privilege to Delete Leads ";
		
		if(fc.utobj().isSelected(driver, addNewRoleUI.checkBoxAgainst_PrivilegeDescription(driver, privilegeDescription)))
		{
			fc.utobj().clickElement(driver, addNewRoleUI.checkBoxAgainst_PrivilegeDescription(driver, privilegeDescription));
		}
		
		fc.utobj().clickElement(driver, addNewRoleUI.submit_Btn);
		AddUsersForRoleUI addUsersForRoleUI = new AddUsersForRoleUI(driver);
		fc.utobj().clickElement(driver, addUsersForRoleUI.assignLater_Btn);
	}

	

}
