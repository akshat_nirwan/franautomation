package com.builds.test.admin;

public class SearchedLogs {

	private String searchedText;
	private String SearchedDate;
	
	public String getSearchedDate() {
		return SearchedDate;
	}
	public void setSearchedDate(String searchedDate) {
		SearchedDate = searchedDate;
	}
	public String getSearchedText() {
		return searchedText;
	}
	public void setSearchedText(String searchedText) {
		this.searchedText = searchedText;
	}
	
}
