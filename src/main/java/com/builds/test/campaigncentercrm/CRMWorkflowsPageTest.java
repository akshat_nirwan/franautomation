package com.builds.test.campaigncentercrm;

import java.io.IOException;
import java.util.Map;

import javax.xml.parsers.ParserConfigurationException;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.Test;
import org.xml.sax.SAXException;

import com.builds.test.admin.AdminUsersManageCorporateUsersAddCorporateUserPageTest;
import com.builds.test.common.CorporateUser;
import com.builds.test.crm.AdminCRMConfigureStatusPageTest;
import com.builds.test.crm.AdminCRMContactTypeConfigurationPageTest;
import com.builds.test.crm.CRMCampaignCenterEmailTemplatesPageTest;
import com.builds.test.crm.CRMCampaignCenterPageTest;
import com.builds.uimaps.campaigncentercrm.CRMWorkflowsPage;
import com.builds.uimaps.crm.AdminCRMManageWebFormGeneratorPage;
import com.builds.utilities.FranconnectUtil;
import com.builds.utilities.TestCase;

public class CRMWorkflowsPageTest {

	FranconnectUtil fc = new FranconnectUtil();

	@Test(groups = "crmworkflow")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Create The Standard WorkFlow When Lead Is Added For Leads Matching Criteria Email Field is Not Empty", testCaseId = "TC_01_Create_Workflow")
	public void createWorkFlow01() throws IOException, Exception, ParserConfigurationException, SAXException {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crmworkflow", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			/*
			 * 
			 * String
			 * userName=fc.utobj().generateTestData(dataSet.get("userName"));
			 * String
			 * password=fc.utobj().generateTestData(dataSet.get("password"));
			 * String emailId="crmautomation@staffex.com";
			 * AdminUsersManageCorporateUsersAddCorporateUserPageTest
			 * addcorporateUserPage=new
			 * AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			 * addcorporateUserPage.addCorporateUser(driver, userName, password,
			 * config , emailId);
			 * 
			 * fc.home_page().logout(driver, config);
			 * fc.utobj().printTestStep(testCaseId, "Login With Corporate User"
			 * ); fc.loginpage().loginWithParameter(driver, userName, password);
			 */

			fc.utobj().printTestStep("Navigate To Admin > CRM > Configure Status Page");
			fc.utobj().printTestStep("Add Lead Status");
			String leadStatus = dataSet.get("leadStatus");
			AdminCRMConfigureStatusPageTest statusPage = new AdminCRMConfigureStatusPageTest();
			statusPage.addStatus(driver, leadStatus);

			fc.utobj().printTestStep("Navigate To CRM > Campaign Center");
			fc.crm().crm_common().CRMModule(driver);

			fc.utobj().printTestStep("Create Campaign For Send Campaign");

			String campaignName = dataSet.get("campaignName");
			String accessibility = dataSet.get("accessibility");
			String campaignType = dataSet.get("campaignType");
			String templateName = dataSet.get("templateName");
			String emailSubject = dataSet.get("emailSubject");
			String plainTextEditor = dataSet.get("plainTextEditor");
			CRMCampaignCenterPageTest campaignCenterPage = new CRMCampaignCenterPageTest();
			campaignCenterPage.createCampaign(driver, config, campaignName, accessibility, campaignType, templateName,
					emailSubject, plainTextEditor);

			fc.utobj().printTestStep("Create Template For Send Email");

			String templateNameForSendEmail = dataSet.get("templateNameForSendEmail");
			String emailSubjectForSendEmail = dataSet.get("emailSubjectForSendEmail");
			String plainTextEditorForSendEmail = dataSet.get("plainTextEditorForSendEmail");

			CRMCampaignCenterEmailTemplatesPageTest emailTemplatePage = new CRMCampaignCenterEmailTemplatesPageTest();
			emailTemplatePage.createTemplate(driver, config, templateNameForSendEmail, emailSubjectForSendEmail,
					plainTextEditorForSendEmail);

			fc.utobj().printTestStep("Navigate To CRM > Worflows Page");
			fc.utobj().printTestStep("Create Lead Type Standard Workflow");

			String workFlowName = dataSet.get("workFlowName");
			String triggerType = dataSet.get("triggerType");
			String conditionType = dataSet.get("conditionType");
			String availableFields = dataSet.get("availableFields");
			String taskSubject = dataSet.get("taskSubject");
			String taskPriority = dataSet.get("taskPriority");
			String taskStatus = dataSet.get("taskStatus");
			String matchingCondition = dataSet.get("matchingCondition");

			createWorkFlowLeadStandard(driver, config, dataSet, workFlowName, triggerType, conditionType,
					availableFields, matchingCondition, campaignName, leadStatus, templateNameForSendEmail, taskSubject,
					taskPriority, taskStatus);

			// String workFlowName="Workflow Name 01";

			fc.utobj().printTestStep("Verify The Created WorkFlow");
			searchWorkFLowByFilter(driver, workFlowName);

			boolean isWorkFlowNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[.='" + workFlowName + "']");
			if (isWorkFlowNamePresent == false) {
				fc.utobj().throwsException("was not able to verify the workflow");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "crmworkflow")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Create The Standard WorkFlow When Lead is Added For Leads Matching Criteria Status OF Lead Is Default[New]", testCaseId = "TC_02_Create_Workflow")
	public void createWorkFlow02() throws IOException, Exception, ParserConfigurationException, SAXException {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crmworkflow", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			/*
			 * 
			 * String
			 * userName=fc.utobj().generateTestData(dataSet.get("userName"));
			 * String
			 * password=fc.utobj().generateTestData(dataSet.get("password"));
			 * String emailId="crmautomation@staffex.com";
			 * AdminUsersManageCorporateUsersAddCorporateUserPageTest
			 * addcorporateUserPage=new
			 * AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			 * addcorporateUserPage.addCorporateUser(driver, userName, password,
			 * config , emailId);
			 * 
			 * fc.home_page().logout(driver, config);
			 * fc.utobj().printTestStep(testCaseId, "Login With Corporate User"
			 * ); fc.loginpage().loginWithParameter(driver, userName, password);
			 */

			fc.utobj().printTestStep("Navigate To Admin > CRM > Configure Status Page");
			fc.utobj().printTestStep("Add Lead Status");
			String leadStatus = dataSet.get("leadStatus");
			AdminCRMConfigureStatusPageTest statusPage = new AdminCRMConfigureStatusPageTest();
			statusPage.addStatus(driver, leadStatus);

			fc.utobj().printTestStep("Navigate To CRM > Campaign Center");
			fc.crm().crm_common().CRMModule(driver);

			fc.utobj().printTestStep("Create Campaign For Send Campaign");

			String campaignName = dataSet.get("campaignName");
			String accessibility = dataSet.get("accessibility");
			String campaignType = dataSet.get("campaignType");
			String templateName = dataSet.get("templateName");
			String emailSubject = dataSet.get("emailSubject");
			String plainTextEditor = dataSet.get("plainTextEditor");
			CRMCampaignCenterPageTest campaignCenterPage = new CRMCampaignCenterPageTest();
			campaignCenterPage.createCampaign(driver, config, campaignName, accessibility, campaignType, templateName,
					emailSubject, plainTextEditor);

			fc.utobj().printTestStep("Create Template For Send Email");

			String templateNameForSendEmail = dataSet.get("templateNameForSendEmail");
			String emailSubjectForSendEmail = dataSet.get("emailSubjectForSendEmail");
			String plainTextEditorForSendEmail = dataSet.get("plainTextEditorForSendEmail");

			CRMCampaignCenterEmailTemplatesPageTest emailTemplatePage = new CRMCampaignCenterEmailTemplatesPageTest();
			emailTemplatePage.createTemplate(driver, config, templateNameForSendEmail, emailSubjectForSendEmail,
					plainTextEditorForSendEmail);

			fc.utobj().printTestStep("Navigate To CRM > Worflows Page");
			fc.utobj().printTestStep("Create Lead Type Standard Workflow");

			String workFlowName = dataSet.get("workFlowName");
			String triggerType = dataSet.get("triggerType");
			String conditionType = dataSet.get("conditionType");
			String availableFields = dataSet.get("availableFields");
			String taskSubject = dataSet.get("taskSubject");
			String taskPriority = dataSet.get("taskPriority");
			String taskStatus = dataSet.get("taskStatus");
			String matchingCondition = dataSet.get("matchingCondition");

			createWorkFlowLeadStandard(driver, config, dataSet, workFlowName, triggerType, conditionType,
					availableFields, matchingCondition, campaignName, leadStatus, templateNameForSendEmail, taskSubject,
					taskPriority, taskStatus);

			fc.utobj().printTestStep("Verify The Created WorkFlow");
			searchWorkFLowByFilter(driver, workFlowName);

			boolean isWorkFlowNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[.='" + workFlowName + "']");
			if (isWorkFlowNamePresent == false) {
				fc.utobj().throwsException("was not able to verify the workflow");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "crmworkflow")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Create The Standard WorkFlow When Lead is Added For Leads Matching Criteria Email Subscription status is Set To Opted In", testCaseId = "TC_03_Create_Workflow")
	public void createWorkFlow03() throws IOException, Exception, ParserConfigurationException, SAXException {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crmworkflow", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			/*
			 * 
			 * String
			 * userName=fc.utobj().generateTestData(dataSet.get("userName"));
			 * String
			 * password=fc.utobj().generateTestData(dataSet.get("password"));
			 * String emailId="crmautomation@staffex.com";
			 * AdminUsersManageCorporateUsersAddCorporateUserPageTest
			 * addcorporateUserPage=new
			 * AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			 * addcorporateUserPage.addCorporateUser(driver, userName, password,
			 * config , emailId);
			 * 
			 * fc.home_page().logout(driver, config);
			 * fc.utobj().printTestStep(testCaseId, "Login With Corporate User"
			 * ); fc.loginpage().loginWithParameter(driver, userName, password);
			 */

			fc.utobj().printTestStep("Navigate To Admin > CRM > Configure Status Page");
			fc.utobj().printTestStep("Add Lead Status");
			String leadStatus = dataSet.get("leadStatus");
			AdminCRMConfigureStatusPageTest statusPage = new AdminCRMConfigureStatusPageTest();
			statusPage.addStatus(driver, leadStatus);

			fc.utobj().printTestStep("Navigate To CRM > Campaign Center");
			fc.crm().crm_common().CRMModule(driver);

			fc.utobj().printTestStep("Create Campaign For Send Campaign");

			String campaignName = dataSet.get("campaignName");
			String accessibility = dataSet.get("accessibility");
			String campaignType = dataSet.get("campaignType");
			String templateName = dataSet.get("templateName");
			String emailSubject = dataSet.get("emailSubject");
			String plainTextEditor = dataSet.get("plainTextEditor");
			CRMCampaignCenterPageTest campaignCenterPage = new CRMCampaignCenterPageTest();
			campaignCenterPage.createCampaign(driver, config, campaignName, accessibility, campaignType, templateName,
					emailSubject, plainTextEditor);

			fc.utobj().printTestStep("Create Template For Send Email");

			String templateNameForSendEmail = dataSet.get("templateNameForSendEmail");
			String emailSubjectForSendEmail = dataSet.get("emailSubjectForSendEmail");
			String plainTextEditorForSendEmail = dataSet.get("plainTextEditorForSendEmail");

			CRMCampaignCenterEmailTemplatesPageTest emailTemplatePage = new CRMCampaignCenterEmailTemplatesPageTest();
			emailTemplatePage.createTemplate(driver, config, templateNameForSendEmail, emailSubjectForSendEmail,
					plainTextEditorForSendEmail);

			fc.utobj().printTestStep("Navigate To CRM > Worflows Page");
			fc.utobj().printTestStep("Create Lead Type Standard Workflow");

			String workFlowName = dataSet.get("workFlowName");
			String triggerType = dataSet.get("triggerType");
			String conditionType = dataSet.get("conditionType");
			String availableFields = dataSet.get("availableFields");
			String taskSubject = dataSet.get("taskSubject");
			String taskPriority = dataSet.get("taskPriority");
			String taskStatus = dataSet.get("taskStatus");
			String matchingCondition = dataSet.get("matchingCondition");

			createWorkFlowLeadStandard(driver, config, dataSet, workFlowName, triggerType, conditionType,
					availableFields, matchingCondition, campaignName, leadStatus, templateNameForSendEmail, taskSubject,
					taskPriority, taskStatus);

			fc.utobj().printTestStep("Verify The Created WorkFlow");
			searchWorkFLowByFilter(driver, workFlowName);

			boolean isWorkFlowNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[.='" + workFlowName + "']");
			if (isWorkFlowNamePresent == false) {
				fc.utobj().throwsException("was not able to verify the workflow");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "crmworkflow")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Create The Standard WorkFlow When Lead is Added For Leads Matching Criteria Country And All States of It", testCaseId = "TC_04_Create_Workflow")
	public void createWorkFlow04() throws IOException, Exception, ParserConfigurationException, SAXException {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crmworkflow", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			/*
			 * 
			 * String
			 * userName=fc.utobj().generateTestData(dataSet.get("userName"));
			 * String
			 * password=fc.utobj().generateTestData(dataSet.get("password"));
			 * String emailId="crmautomation@staffex.com";
			 * AdminUsersManageCorporateUsersAddCorporateUserPageTest
			 * addcorporateUserPage=new
			 * AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			 * addcorporateUserPage.addCorporateUser(driver, userName, password,
			 * config , emailId);
			 * 
			 * fc.home_page().logout(driver, config);
			 * fc.utobj().printTestStep(testCaseId, "Login With Corporate User"
			 * ); fc.loginpage().loginWithParameter(driver, userName, password);
			 */

			fc.utobj().printTestStep("Navigate To Admin > CRM > Configure Status Page");
			fc.utobj().printTestStep("Add Lead Status");
			String leadStatus = dataSet.get("leadStatus");
			AdminCRMConfigureStatusPageTest statusPage = new AdminCRMConfigureStatusPageTest();
			statusPage.addStatus(driver, leadStatus);

			fc.utobj().printTestStep("Navigate To CRM > Campaign Center");
			fc.crm().crm_common().CRMModule(driver);

			fc.utobj().printTestStep("Create Campaign For Send Campaign");

			String campaignName = dataSet.get("campaignName");
			String accessibility = dataSet.get("accessibility");
			String campaignType = dataSet.get("campaignType");
			String templateName = dataSet.get("templateName");
			String emailSubject = dataSet.get("emailSubject");
			String plainTextEditor = dataSet.get("plainTextEditor");
			CRMCampaignCenterPageTest campaignCenterPage = new CRMCampaignCenterPageTest();
			campaignCenterPage.createCampaign(driver, config, campaignName, accessibility, campaignType, templateName,
					emailSubject, plainTextEditor);

			fc.utobj().printTestStep("Create Template For Send Email");

			String templateNameForSendEmail = dataSet.get("templateNameForSendEmail");
			String emailSubjectForSendEmail = dataSet.get("emailSubjectForSendEmail");
			String plainTextEditorForSendEmail = dataSet.get("plainTextEditorForSendEmail");

			CRMCampaignCenterEmailTemplatesPageTest emailTemplatePage = new CRMCampaignCenterEmailTemplatesPageTest();
			emailTemplatePage.createTemplate(driver, config, templateNameForSendEmail, emailSubjectForSendEmail,
					plainTextEditorForSendEmail);

			fc.utobj().printTestStep("Navigate To CRM > Worflows Page");
			fc.utobj().printTestStep("Create Lead Type Standard Workflow");

			String workFlowName = dataSet.get("workFlowName");
			String triggerType = dataSet.get("triggerType");
			String conditionType = dataSet.get("conditionType");
			String availableFields = dataSet.get("availableFields");
			String taskSubject = dataSet.get("taskSubject");
			String taskPriority = dataSet.get("taskPriority");
			String taskStatus = dataSet.get("taskStatus");
			String matchingCondition = dataSet.get("matchingCondition");

			createWorkFlowLeadStandard(driver, config, dataSet, workFlowName, triggerType, conditionType,
					availableFields, matchingCondition, campaignName, leadStatus, templateNameForSendEmail, taskSubject,
					taskPriority, taskStatus);

			fc.utobj().printTestStep("Verify The Created WorkFlow");
			searchWorkFLowByFilter(driver, workFlowName);

			boolean isWorkFlowNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[.='" + workFlowName + "']");
			if (isWorkFlowNamePresent == false) {
				fc.utobj().throwsException("was not able to verify the workflow");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "crmworkflow")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Create The Standard WorkFlow When Lead is Added For Leads Matching Criteria BirthDate And Set To It , is Between", testCaseId = "TC_05_Create_Workflow")
	public void createWorkFlow05() throws IOException, Exception, ParserConfigurationException, SAXException {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crmworkflow", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			/*
			 * 
			 * String
			 * userName=fc.utobj().generateTestData(dataSet.get("userName"));
			 * String
			 * password=fc.utobj().generateTestData(dataSet.get("password"));
			 * String emailId="crmautomation@staffex.com";
			 * AdminUsersManageCorporateUsersAddCorporateUserPageTest
			 * addcorporateUserPage=new
			 * AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			 * addcorporateUserPage.addCorporateUser(driver, userName, password,
			 * config , emailId);
			 * 
			 * fc.home_page().logout(driver, config);
			 * fc.utobj().printTestStep(testCaseId, "Login With Corporate User"
			 * ); fc.loginpage().loginWithParameter(driver, userName, password);
			 */

			fc.utobj().printTestStep("Navigate To Admin > CRM > Configure Status Page");
			fc.utobj().printTestStep("Add Lead Status");
			String leadStatus = dataSet.get("leadStatus");
			AdminCRMConfigureStatusPageTest statusPage = new AdminCRMConfigureStatusPageTest();
			statusPage.addStatus(driver, leadStatus);

			fc.utobj().printTestStep("Navigate To CRM > Campaign Center");
			fc.crm().crm_common().CRMModule(driver);

			fc.utobj().printTestStep("Create Campaign For Send Campaign");

			String campaignName = dataSet.get("campaignName");
			String accessibility = dataSet.get("accessibility");
			String campaignType = dataSet.get("campaignType");
			String templateName = dataSet.get("templateName");
			String emailSubject = dataSet.get("emailSubject");
			String plainTextEditor = dataSet.get("plainTextEditor");
			CRMCampaignCenterPageTest campaignCenterPage = new CRMCampaignCenterPageTest();
			campaignCenterPage.createCampaign(driver, config, campaignName, accessibility, campaignType, templateName,
					emailSubject, plainTextEditor);

			fc.utobj().printTestStep("Create Template For Send Email");

			String templateNameForSendEmail = dataSet.get("templateNameForSendEmail");
			String emailSubjectForSendEmail = dataSet.get("emailSubjectForSendEmail");
			String plainTextEditorForSendEmail = dataSet.get("plainTextEditorForSendEmail");

			CRMCampaignCenterEmailTemplatesPageTest emailTemplatePage = new CRMCampaignCenterEmailTemplatesPageTest();
			emailTemplatePage.createTemplate(driver, config, templateNameForSendEmail, emailSubjectForSendEmail,
					plainTextEditorForSendEmail);

			fc.utobj().printTestStep("Navigate To CRM > Worflows Page");
			fc.utobj().printTestStep("Create Lead Type Standard Workflow");

			String workFlowName = dataSet.get("workFlowName");
			String triggerType = dataSet.get("triggerType");
			String conditionType = dataSet.get("conditionType");
			String availableFields = dataSet.get("availableFields");
			String taskSubject = dataSet.get("taskSubject");
			String taskPriority = dataSet.get("taskPriority");
			String taskStatus = dataSet.get("taskStatus");
			String matchingCondition = dataSet.get("matchingCondition");

			createWorkFlowLeadStandard(driver, config, dataSet, workFlowName, triggerType, conditionType,
					availableFields, matchingCondition, campaignName, leadStatus, templateNameForSendEmail, taskSubject,
					taskPriority, taskStatus);

			fc.utobj().printTestStep("Verify The Created WorkFlow");
			searchWorkFLowByFilter(driver, workFlowName);

			boolean isWorkFlowNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[.='" + workFlowName + "']");
			if (isWorkFlowNamePresent == false) {
				fc.utobj().throwsException("was not able to verify the workflow");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	/*
	 * @Test(groups = "crmworkflow")
	 * 
	 * @TestCase(createdOn="2017-05-01",updatedOn="2018-06-14",
	 * testCaseDescription =
	 * "Create The Standard WorkFlow When Lead is Added For Leads Matching Criteria Status Of Lead Is Customized"
	 * , testCaseId = "TC_06_Create_Workflow") public void
	 * createWorkFlow06()throws IOException, Exception,
	 * ParserConfigurationException, SAXException {
	 * 
	 * String testCaseId =
	 * fc.utobj().readTestCaseInfo(this.getClass().getName()+"."+new
	 * Object(){}.getClass().getEnclosingMethod().getName());
	 * 
	 * Map<String, String> config =
	 * fc.utobj().readConfigurationFile(FranconnectUtil.path);
	 * Map<String,String> dataSet = fc.utobj().readTestData("crmworkflow",
	 * testCaseId); WebDriver driver = fc.utobj().openDriver(config);
	 * 
	 * try { driver = fc.loginpage().login(driver, config);
	 * 
	 * String userName=fc.utobj().generateTestData(dataSet.get("userName"));
	 * String password=fc.utobj().generateTestData(dataSet.get("password"));
	 * String emailId="crmautomation@staffex.com";
	 * AdminUsersManageCorporateUsersAddCorporateUserPageTest
	 * addcorporateUserPage=new
	 * AdminUsersManageCorporateUsersAddCorporateUserPageTest();
	 * addcorporateUserPage.addCorporateUser(driver, userName, password, config
	 * , emailId);
	 * 
	 * fc.home_page().logout(driver, config);
	 * fc.utobj().printTestStep(testCaseId, "Login With Corporate User");
	 * fc.loginpage().loginWithParameter(driver, userName, password);
	 * 
	 * fc.utobj().printTestStep(testCaseId,
	 * "Navigate To Admin > CRM > Configure Status Page");
	 * fc.utobj().printTestStep(testCaseId, "Add Lead Status"); String
	 * leadStatus=dataSet.get("leadStatus"); AdminCRMConfigureStatusPageTest
	 * statusPage=new AdminCRMConfigureStatusPageTest();
	 * statusPage.addStatus(driver, leadStatus, config);
	 * 
	 * fc.utobj().printTestStep(testCaseId,
	 * "Navigate To Admin > CRM > Configure Status Page");
	 * fc.utobj().printTestStep(testCaseId, "Add Lead Status"); String
	 * leadStatusCustomize=dataSet.get("selectLeadStatus");
	 * statusPage.addStatus(driver, leadStatusCustomize, config);
	 * 
	 * 
	 * fc.utobj().printTestStep(testCaseId, "Navigate To CRM > Campaign Center"
	 * ); fc.crm().crm_common().CRMModule(driver);
	 * 
	 * fc.utobj().printTestStep(testCaseId, "Create Campaign For Send Campaign"
	 * );
	 * 
	 * String campaignName=dataSet.get("campaignName"); String
	 * accessibility=dataSet.get("accessibility"); String
	 * campaignType=dataSet.get("campaignType"); String
	 * templateName=dataSet.get("templateName"); String
	 * emailSubject=dataSet.get("emailSubject"); String
	 * plainTextEditor=dataSet.get("plainTextEditor"); CRMCampaignCenterPageTest
	 * campaignCenterPage=new CRMCampaignCenterPageTest();
	 * campaignCenterPage.createCampaign(driver, config, campaignName,
	 * accessibility, campaignType, templateName, emailSubject,
	 * plainTextEditor);
	 * 
	 * fc.utobj().printTestStep(testCaseId, "Create Template For Send Email");
	 * 
	 * String templateNameForSendEmail=dataSet.get("templateNameForSendEmail");
	 * String emailSubjectForSendEmail=dataSet.get("emailSubjectForSendEmail");
	 * String
	 * plainTextEditorForSendEmail=dataSet.get("plainTextEditorForSendEmail");
	 * 
	 * CRMCampaignCenterEmailTemplatesPageTest emailTemplatePage=new
	 * CRMCampaignCenterEmailTemplatesPageTest();
	 * emailTemplatePage.createTemplate(driver, config,
	 * templateNameForSendEmail, emailSubjectForSendEmail,
	 * plainTextEditorForSendEmail);
	 * 
	 * fc.utobj().printTestStep(testCaseId, "Navigate To CRM > Worflows Page");
	 * fc.utobj().printTestStep(testCaseId, "Create Lead Type Standard Workflow"
	 * );
	 * 
	 * String workFlowName=dataSet.get("workFlowName"); String
	 * triggerType=dataSet.get("triggerType"); String
	 * conditionType=dataSet.get("conditionType"); String
	 * availableFields=dataSet.get("availableFields"); String
	 * taskSubject=dataSet.get("taskSubject"); String
	 * taskPriority=dataSet.get("taskPriority"); String
	 * taskStatus=dataSet.get("taskStatus"); String
	 * matchingCondition=dataSet.get("matchingCondition");
	 * 
	 * createWorkFlowLeadStandard(driver, config, dataSet, workFlowName,
	 * triggerType, conditionType, availableFields, matchingCondition,
	 * campaignName, leadStatus, templateNameForSendEmail, taskSubject,
	 * taskPriority, taskStatus);
	 * 
	 * fc.utobj().logoutAndQuitBrowser(driver, config, testCaseId); } catch
	 * (Exception e) {
	 * 
	 * fc.utobj().quitBrowserOnCatch(driver, config, e, testCaseId);} }
	 */

	@Test(groups = "crmworkflow")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Create The Standard WorkFlow When Contact Is Added For Contact Matching Criteria Email Field is Not Empty", testCaseId = "TC_07_Create_Workflow")
	public void createWorkFlow07() throws IOException, Exception, ParserConfigurationException, SAXException {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crmworkflow", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			/*
			 * 
			 * String
			 * userName=fc.utobj().generateTestData(dataSet.get("userName"));
			 * String
			 * password=fc.utobj().generateTestData(dataSet.get("password"));
			 * String emailId="crmautomation@staffex.com";
			 * AdminUsersManageCorporateUsersAddCorporateUserPageTest
			 * addcorporateUserPage=new
			 * AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			 * addcorporateUserPage.addCorporateUser(driver, userName, password,
			 * config , emailId);
			 * 
			 * fc.home_page().logout(driver, config);
			 * fc.utobj().printTestStep(testCaseId, "Login With Corporate User"
			 * ); fc.loginpage().loginWithParameter(driver, userName, password);
			 */

			fc.utobj().printTestStep("Navigate To Admin > CRM > Configure Status Page");
			fc.utobj().printTestStep("Add Contact Status");
			String contactStatus = dataSet.get("contactStatus");
			AdminCRMConfigureStatusPageTest statusPage = new AdminCRMConfigureStatusPageTest();
			statusPage.addStatus01(driver, contactStatus);

			fc.utobj().printTestStep("Navigate To CRM > Campaign Center");
			fc.crm().crm_common().CRMModule(driver);

			fc.utobj().printTestStep("Create Campaign For Send Campaign");
			String campaignName = dataSet.get("campaignName");
			String accessibility = dataSet.get("accessibility");
			String campaignType = dataSet.get("campaignType");
			String templateName = dataSet.get("templateName");
			String emailSubject = dataSet.get("emailSubject");
			String plainTextEditor = dataSet.get("plainTextEditor");
			CRMCampaignCenterPageTest campaignCenterPage = new CRMCampaignCenterPageTest();
			campaignCenterPage.createCampaign(driver, config, campaignName, accessibility, campaignType, templateName,
					emailSubject, plainTextEditor);

			fc.utobj().printTestStep("Create Template For Send Email");

			String templateNameForSendEmail = dataSet.get("templateNameForSendEmail");
			String emailSubjectForSendEmail = dataSet.get("emailSubjectForSendEmail");
			String plainTextEditorForSendEmail = dataSet.get("plainTextEditorForSendEmail");

			CRMCampaignCenterEmailTemplatesPageTest emailTemplatePage = new CRMCampaignCenterEmailTemplatesPageTest();
			emailTemplatePage.createTemplate(driver, config, templateNameForSendEmail, emailSubjectForSendEmail,
					plainTextEditorForSendEmail);

			fc.utobj().printTestStep("Navigate To CRM > Worflows Page");
			fc.utobj().printTestStep("Create Contact Type Standard Workflow");

			String workFlowName = dataSet.get("workFlowName");
			String triggerType = dataSet.get("triggerType");
			String conditionType = dataSet.get("conditionType");
			String availableFields = dataSet.get("availableFields");
			String taskSubject = dataSet.get("taskSubject");
			String taskPriority = dataSet.get("taskPriority");
			String taskStatus = dataSet.get("taskStatus");
			String matchingCondition = dataSet.get("matchingCondition");

			createWorkFlowContactStandard(driver, config, dataSet, workFlowName, triggerType, conditionType,
					availableFields, matchingCondition, campaignName, contactStatus, templateNameForSendEmail,
					taskSubject, taskPriority, taskStatus);

			fc.utobj().printTestStep("Verify The Created WorkFlow");
			searchWorkFLowByFilter(driver, workFlowName);

			boolean isWorkFlowNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[.='" + workFlowName + "']");
			if (isWorkFlowNamePresent == false) {
				fc.utobj().throwsException("was not able to verify the workflow");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "crmworkflow")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Create The Standard WorkFlow When Contact Is Added For Contact Matching Criteria Status Of Contact is Default[New]", testCaseId = "TC_08_Create_Workflow")
	public void createWorkFlow08() throws IOException, Exception, ParserConfigurationException, SAXException {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crmworkflow", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			/*
			 * 
			 * String
			 * userName=fc.utobj().generateTestData(dataSet.get("userName"));
			 * String
			 * password=fc.utobj().generateTestData(dataSet.get("password"));
			 * String emailId="crmautomation@staffex.com";
			 * AdminUsersManageCorporateUsersAddCorporateUserPageTest
			 * addcorporateUserPage=new
			 * AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			 * addcorporateUserPage.addCorporateUser(driver, userName, password,
			 * config , emailId);
			 * 
			 * fc.home_page().logout(driver, config);
			 * fc.utobj().printTestStep(testCaseId, "Login With Corporate User"
			 * ); fc.loginpage().loginWithParameter(driver, userName, password);
			 */

			fc.utobj().printTestStep("Navigate To Admin > CRM > Configure Status Page");
			fc.utobj().printTestStep("Add Contact Status");
			String contactStatus = dataSet.get("contactStatus");
			AdminCRMConfigureStatusPageTest statusPage = new AdminCRMConfigureStatusPageTest();
			statusPage.addStatus01(driver, contactStatus);

			fc.utobj().printTestStep("Navigate To CRM > Campaign Center");
			fc.crm().crm_common().CRMModule(driver);

			fc.utobj().printTestStep("Create Campaign For Send Campaign");

			String campaignName = dataSet.get("campaignName");
			String accessibility = dataSet.get("accessibility");
			String campaignType = dataSet.get("campaignType");
			String templateName = dataSet.get("templateName");
			String emailSubject = dataSet.get("emailSubject");
			String plainTextEditor = dataSet.get("plainTextEditor");
			CRMCampaignCenterPageTest campaignCenterPage = new CRMCampaignCenterPageTest();
			campaignCenterPage.createCampaign(driver, config, campaignName, accessibility, campaignType, templateName,
					emailSubject, plainTextEditor);

			fc.utobj().printTestStep("Create Template For Send Email");

			String templateNameForSendEmail = dataSet.get("templateNameForSendEmail");
			String emailSubjectForSendEmail = dataSet.get("emailSubjectForSendEmail");
			String plainTextEditorForSendEmail = dataSet.get("plainTextEditorForSendEmail");

			CRMCampaignCenterEmailTemplatesPageTest emailTemplatePage = new CRMCampaignCenterEmailTemplatesPageTest();
			emailTemplatePage.createTemplate(driver, config, templateNameForSendEmail, emailSubjectForSendEmail,
					plainTextEditorForSendEmail);

			fc.utobj().printTestStep("Navigate To CRM > Worflows Page");
			fc.utobj().printTestStep("Create Contact Type Standard Workflow");

			String workFlowName = dataSet.get("workFlowName");
			String triggerType = dataSet.get("triggerType");
			String conditionType = dataSet.get("conditionType");
			String availableFields = dataSet.get("availableFields");
			String taskSubject = dataSet.get("taskSubject");
			String taskPriority = dataSet.get("taskPriority");
			String taskStatus = dataSet.get("taskStatus");
			String matchingCondition = dataSet.get("matchingCondition");

			createWorkFlowContactStandard(driver, config, dataSet, workFlowName, triggerType, conditionType,
					availableFields, matchingCondition, campaignName, contactStatus, templateNameForSendEmail,
					taskSubject, taskPriority, taskStatus);

			fc.utobj().printTestStep("Verify The Created WorkFlow");
			searchWorkFLowByFilter(driver, workFlowName);

			boolean isWorkFlowNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[.='" + workFlowName + "']");
			if (isWorkFlowNamePresent == false) {
				fc.utobj().throwsException("was not able to verify the workflow");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "crmworkflow")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Create The Standard WorkFlow When Contact Is Added For Contact Matching Criteria Email Subscription status is Set To Opted In", testCaseId = "TC_09_Create_Workflow")
	public void createWorkFlow09() throws IOException, Exception, ParserConfigurationException, SAXException {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crmworkflow", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			/*
			 * 
			 * String
			 * userName=fc.utobj().generateTestData(dataSet.get("userName"));
			 * String
			 * password=fc.utobj().generateTestData(dataSet.get("password"));
			 * String emailId="crmautomation@staffex.com";
			 * AdminUsersManageCorporateUsersAddCorporateUserPageTest
			 * addcorporateUserPage=new
			 * AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			 * addcorporateUserPage.addCorporateUser(driver, userName, password,
			 * config , emailId);
			 * 
			 * fc.home_page().logout(driver, config);
			 * fc.utobj().printTestStep(testCaseId, "Login With Corporate User"
			 * ); fc.loginpage().loginWithParameter(driver, userName, password);
			 */

			fc.utobj().printTestStep("Navigate To Admin > CRM > Configure Status Page");
			fc.utobj().printTestStep("Add Contact Status");
			String contactStatus = dataSet.get("contactStatus");
			AdminCRMConfigureStatusPageTest statusPage = new AdminCRMConfigureStatusPageTest();
			statusPage.addStatus01(driver, contactStatus);

			fc.utobj().printTestStep("Navigate To CRM > Campaign Center");
			fc.crm().crm_common().CRMModule(driver);

			fc.utobj().printTestStep("Create Campaign For Send Campaign");

			String campaignName = dataSet.get("campaignName");
			String accessibility = dataSet.get("accessibility");
			String campaignType = dataSet.get("campaignType");
			String templateName = dataSet.get("templateName");
			String emailSubject = dataSet.get("emailSubject");
			String plainTextEditor = dataSet.get("plainTextEditor");
			CRMCampaignCenterPageTest campaignCenterPage = new CRMCampaignCenterPageTest();
			campaignCenterPage.createCampaign(driver, config, campaignName, accessibility, campaignType, templateName,
					emailSubject, plainTextEditor);

			fc.utobj().printTestStep("Create Template For Send Email");

			String templateNameForSendEmail = dataSet.get("templateNameForSendEmail");
			String emailSubjectForSendEmail = dataSet.get("emailSubjectForSendEmail");
			String plainTextEditorForSendEmail = dataSet.get("plainTextEditorForSendEmail");

			CRMCampaignCenterEmailTemplatesPageTest emailTemplatePage = new CRMCampaignCenterEmailTemplatesPageTest();
			emailTemplatePage.createTemplate(driver, config, templateNameForSendEmail, emailSubjectForSendEmail,
					plainTextEditorForSendEmail);

			fc.utobj().printTestStep("Navigate To CRM > Worflows Page");
			fc.utobj().printTestStep("Create Contact Type Standard Workflow");

			String workFlowName = dataSet.get("workFlowName");
			String triggerType = dataSet.get("triggerType");
			String conditionType = dataSet.get("conditionType");
			String availableFields = dataSet.get("availableFields");
			String taskSubject = dataSet.get("taskSubject");
			String taskPriority = dataSet.get("taskPriority");
			String taskStatus = dataSet.get("taskStatus");
			String matchingCondition = dataSet.get("matchingCondition");

			createWorkFlowContactStandard(driver, config, dataSet, workFlowName, triggerType, conditionType,
					availableFields, matchingCondition, campaignName, contactStatus, templateNameForSendEmail,
					taskSubject, taskPriority, taskStatus);

			fc.utobj().printTestStep("Verify The Created WorkFlow");
			searchWorkFLowByFilter(driver, workFlowName);

			boolean isWorkFlowNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[.='" + workFlowName + "']");
			if (isWorkFlowNamePresent == false) {
				fc.utobj().throwsException("was not able to verify the workflow");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "crmworkflow")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Create The Standard WorkFlow When Contact Is Added For Contact Matching Criteria Country And All States of It", testCaseId = "TC_10_Create_Workflow")
	public void createWorkFlow10() throws IOException, Exception, ParserConfigurationException, SAXException {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crmworkflow", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			/*
			 * 
			 * String
			 * userName=fc.utobj().generateTestData(dataSet.get("userName"));
			 * String
			 * password=fc.utobj().generateTestData(dataSet.get("password"));
			 * String emailId="crmautomation@staffex.com";
			 * AdminUsersManageCorporateUsersAddCorporateUserPageTest
			 * addcorporateUserPage=new
			 * AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			 * addcorporateUserPage.addCorporateUser(driver, userName, password,
			 * config , emailId);
			 * 
			 * fc.home_page().logout(driver, config);
			 * fc.utobj().printTestStep(testCaseId, "Login With Corporate User"
			 * ); fc.loginpage().loginWithParameter(driver, userName, password);
			 */

			fc.utobj().printTestStep("Navigate To Admin > CRM > Configure Status Page");
			fc.utobj().printTestStep("Add Contact Status");
			String contactStatus = dataSet.get("contactStatus");
			AdminCRMConfigureStatusPageTest statusPage = new AdminCRMConfigureStatusPageTest();
			statusPage.addStatus01(driver, contactStatus);

			fc.utobj().printTestStep("Navigate To CRM > Campaign Center");
			fc.crm().crm_common().CRMModule(driver);

			fc.utobj().printTestStep("Create Campaign For Send Campaign");

			String campaignName = dataSet.get("campaignName");
			String accessibility = dataSet.get("accessibility");
			String campaignType = dataSet.get("campaignType");
			String templateName = dataSet.get("templateName");
			String emailSubject = dataSet.get("emailSubject");
			String plainTextEditor = dataSet.get("plainTextEditor");
			CRMCampaignCenterPageTest campaignCenterPage = new CRMCampaignCenterPageTest();
			campaignCenterPage.createCampaign(driver, config, campaignName, accessibility, campaignType, templateName,
					emailSubject, plainTextEditor);

			fc.utobj().printTestStep("Create Template For Send Email");

			String templateNameForSendEmail = dataSet.get("templateNameForSendEmail");
			String emailSubjectForSendEmail = dataSet.get("emailSubjectForSendEmail");
			String plainTextEditorForSendEmail = dataSet.get("plainTextEditorForSendEmail");

			CRMCampaignCenterEmailTemplatesPageTest emailTemplatePage = new CRMCampaignCenterEmailTemplatesPageTest();
			emailTemplatePage.createTemplate(driver, config, templateNameForSendEmail, emailSubjectForSendEmail,
					plainTextEditorForSendEmail);

			fc.utobj().printTestStep("Navigate To CRM > Worflows Page");
			fc.utobj().printTestStep("Create Contact Type Standard Workflow");

			String workFlowName = dataSet.get("workFlowName");
			String triggerType = dataSet.get("triggerType");
			String conditionType = dataSet.get("conditionType");
			String availableFields = dataSet.get("availableFields");
			String taskSubject = dataSet.get("taskSubject");
			String taskPriority = dataSet.get("taskPriority");
			String taskStatus = dataSet.get("taskStatus");
			String matchingCondition = dataSet.get("matchingCondition");

			createWorkFlowContactStandard(driver, config, dataSet, workFlowName, triggerType, conditionType,
					availableFields, matchingCondition, campaignName, contactStatus, templateNameForSendEmail,
					taskSubject, taskPriority, taskStatus);

			fc.utobj().printTestStep("Verify The Created WorkFlow");
			searchWorkFLowByFilter(driver, workFlowName);

			boolean isWorkFlowNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[.='" + workFlowName + "']");
			if (isWorkFlowNamePresent == false) {
				fc.utobj().throwsException("was not able to verify the workflow");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "crmworkflow")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Create The Standard WorkFlow When Contact Is Added For Contact Matching Criteria BirthDate And Set To It , is Between", testCaseId = "TC_11_Create_Workflow")
	public void createWorkFlow11() throws IOException, Exception, ParserConfigurationException, SAXException {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crmworkflow", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			/*
			 * 
			 * String
			 * userName=fc.utobj().generateTestData(dataSet.get("userName"));
			 * String
			 * password=fc.utobj().generateTestData(dataSet.get("password"));
			 * String emailId="crmautomation@staffex.com";
			 * AdminUsersManageCorporateUsersAddCorporateUserPageTest
			 * addcorporateUserPage=new
			 * AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			 * addcorporateUserPage.addCorporateUser(driver, userName, password,
			 * config , emailId);
			 * 
			 * fc.home_page().logout(driver, config);
			 * fc.utobj().printTestStep(testCaseId, "Login With Corporate User"
			 * ); fc.loginpage().loginWithParameter(driver, userName, password);
			 */

			fc.utobj().printTestStep("Navigate To Admin > CRM > Configure Status Page");
			fc.utobj().printTestStep("Add Contact Status");
			String contactStatus = dataSet.get("contactStatus");
			AdminCRMConfigureStatusPageTest statusPage = new AdminCRMConfigureStatusPageTest();
			statusPage.addStatus01(driver, contactStatus);

			fc.utobj().printTestStep("Navigate To CRM > Campaign Center");
			fc.crm().crm_common().CRMModule(driver);

			fc.utobj().printTestStep("Create Campaign For Send Campaign");

			String campaignName = dataSet.get("campaignName");
			String accessibility = dataSet.get("accessibility");
			String campaignType = dataSet.get("campaignType");
			String templateName = dataSet.get("templateName");
			String emailSubject = dataSet.get("emailSubject");
			String plainTextEditor = dataSet.get("plainTextEditor");
			CRMCampaignCenterPageTest campaignCenterPage = new CRMCampaignCenterPageTest();
			campaignCenterPage.createCampaign(driver, config, campaignName, accessibility, campaignType, templateName,
					emailSubject, plainTextEditor);

			fc.utobj().printTestStep("Create Template For Send Email");

			String templateNameForSendEmail = dataSet.get("templateNameForSendEmail");
			String emailSubjectForSendEmail = dataSet.get("emailSubjectForSendEmail");
			String plainTextEditorForSendEmail = dataSet.get("plainTextEditorForSendEmail");

			CRMCampaignCenterEmailTemplatesPageTest emailTemplatePage = new CRMCampaignCenterEmailTemplatesPageTest();
			emailTemplatePage.createTemplate(driver, config, templateNameForSendEmail, emailSubjectForSendEmail,
					plainTextEditorForSendEmail);

			fc.utobj().printTestStep("Navigate To CRM > Worflows Page");
			fc.utobj().printTestStep("Create Contact Type Standard Workflow");

			String workFlowName = dataSet.get("workFlowName");
			String triggerType = dataSet.get("triggerType");
			String conditionType = dataSet.get("conditionType");
			String availableFields = dataSet.get("availableFields");
			String taskSubject = dataSet.get("taskSubject");
			String taskPriority = dataSet.get("taskPriority");
			String taskStatus = dataSet.get("taskStatus");
			String matchingCondition = dataSet.get("matchingCondition");

			createWorkFlowContactStandard(driver, config, dataSet, workFlowName, triggerType, conditionType,
					availableFields, matchingCondition, campaignName, contactStatus, templateNameForSendEmail,
					taskSubject, taskPriority, taskStatus);

			fc.utobj().printTestStep("Verify The Created WorkFlow");
			searchWorkFLowByFilter(driver, workFlowName);

			boolean isWorkFlowNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[.='" + workFlowName + "']");
			if (isWorkFlowNamePresent == false) {
				fc.utobj().throwsException("was not able to verify the workflow");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	/*
	 * @Test(groups = "crmworkflow")
	 * 
	 * @TestCase(createdOn="2017-05-01",updatedOn="2018-06-14",
	 * testCaseDescription =
	 * "Create The Standard WorkFlow When Contact Is Added For Contact Matching Criteria Status Of Contact Is Customized"
	 * , testCaseId = "TC_12_Create_Workflow") public void
	 * createWorkFlow12()throws IOException, Exception,
	 * ParserConfigurationException, SAXException {
	 * 
	 * String testCaseId =
	 * fc.utobj().readTestCaseInfo(this.getClass().getName()+"."+new
	 * Object(){}.getClass().getEnclosingMethod().getName());
	 * 
	 * Map<String, String> config =
	 * fc.utobj().readConfigurationFile(FranconnectUtil.path);
	 * Map<String,String> dataSet = fc.utobj().readTestData("crmworkflow",
	 * testCaseId); WebDriver driver = fc.utobj().openDriver(config);
	 * 
	 * try { driver = fc.loginpage().login(driver, config);
	 * 
	 * String userName=fc.utobj().generateTestData(dataSet.get("userName"));
	 * String password=fc.utobj().generateTestData(dataSet.get("password"));
	 * String emailId="crmautomation@staffex.com";
	 * AdminUsersManageCorporateUsersAddCorporateUserPageTest
	 * addcorporateUserPage=new
	 * AdminUsersManageCorporateUsersAddCorporateUserPageTest();
	 * addcorporateUserPage.addCorporateUser(driver, userName, password, config
	 * , emailId);
	 * 
	 * fc.home_page().logout(driver, config);
	 * fc.utobj().printTestStep(testCaseId, "Login With Corporate User");
	 * fc.loginpage().loginWithParameter(driver, userName, password);
	 * 
	 * fc.utobj().printTestStep(testCaseId,
	 * "Navigate To Admin > CRM > Configure Status Page");
	 * fc.utobj().printTestStep(testCaseId, "Add Contact Status"); String
	 * contactStatus=dataSet.get("contactStatus");
	 * AdminCRMConfigureStatusPageTest statusPage=new
	 * AdminCRMConfigureStatusPageTest(); statusPage.addStatus01(driver,
	 * contactStatus, config);
	 * 
	 * 
	 * fc.utobj().printTestStep(testCaseId,
	 * "Navigate To Admin > CRM > Configure Status Page");
	 * fc.utobj().printTestStep(testCaseId, "Add Contact Status"); String
	 * contactStatusCustomize=dataSet.get("selectContactStatus");
	 * statusPage.addStatus01(driver, contactStatusCustomize, config);
	 * 
	 * fc.utobj().printTestStep(testCaseId, "Navigate To CRM > Campaign Center"
	 * ); fc.crm().crm_common().CRMModule(driver);
	 * 
	 * fc.utobj().printTestStep(testCaseId, "Create Campaign For Send Campaign"
	 * );
	 * 
	 * String campaignName=dataSet.get("campaignName"); String
	 * accessibility=dataSet.get("accessibility"); String
	 * campaignType=dataSet.get("campaignType"); String
	 * templateName=dataSet.get("templateName"); String
	 * emailSubject=dataSet.get("emailSubject"); String
	 * plainTextEditor=dataSet.get("plainTextEditor"); CRMCampaignCenterPageTest
	 * campaignCenterPage=new CRMCampaignCenterPageTest();
	 * campaignCenterPage.createCampaign(driver, config, campaignName,
	 * accessibility, campaignType, templateName, emailSubject,
	 * plainTextEditor);
	 * 
	 * fc.utobj().printTestStep(testCaseId, "Create Template For Send Email");
	 * 
	 * String templateNameForSendEmail=dataSet.get("templateNameForSendEmail");
	 * String emailSubjectForSendEmail=dataSet.get("emailSubjectForSendEmail");
	 * String
	 * plainTextEditorForSendEmail=dataSet.get("plainTextEditorForSendEmail");
	 * 
	 * CRMCampaignCenterEmailTemplatesPageTest emailTemplatePage=new
	 * CRMCampaignCenterEmailTemplatesPageTest();
	 * emailTemplatePage.createTemplate(driver, config,
	 * templateNameForSendEmail, emailSubjectForSendEmail,
	 * plainTextEditorForSendEmail);
	 * 
	 * fc.utobj().printTestStep(testCaseId, "Navigate To CRM > Worflows Page");
	 * fc.utobj().printTestStep(testCaseId,
	 * "Create Contact Type Standard Workflow");
	 * 
	 * String workFlowName=dataSet.get("workFlowName"); String
	 * triggerType=dataSet.get("triggerType"); String
	 * conditionType=dataSet.get("conditionType"); String
	 * availableFields=dataSet.get("availableFields"); String
	 * taskSubject=dataSet.get("taskSubject"); String
	 * taskPriority=dataSet.get("taskPriority"); String
	 * taskStatus=dataSet.get("taskStatus"); String
	 * matchingCondition=dataSet.get("matchingCondition");
	 * 
	 * createWorkFlowContactStandard(driver, config, dataSet, workFlowName,
	 * triggerType, conditionType, availableFields, matchingCondition,
	 * campaignName, contactStatus, templateNameForSendEmail, taskSubject,
	 * taskPriority, taskStatus);
	 * 
	 * fc.utobj().logoutAndQuitBrowser(driver, config, testCaseId); } catch
	 * (Exception e) {
	 * 
	 * fc.utobj().quitBrowserOnCatch(driver, config, e, testCaseId);} }
	 */

	@Test(groups = "crmworkflow")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Create The Standard WorkFlow When Contact Is Added For Contact Matching Criteria Contact Type In", testCaseId = "TC_13_Create_Workflow")
	public void createWorkFlow13() throws IOException, Exception, ParserConfigurationException, SAXException {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crmworkflow", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			/*
			 * 
			 * String
			 * userName=fc.utobj().generateTestData(dataSet.get("userName"));
			 * String
			 * password=fc.utobj().generateTestData(dataSet.get("password"));
			 * String emailId="crmautomation@staffex.com";
			 * AdminUsersManageCorporateUsersAddCorporateUserPageTest
			 * addcorporateUserPage=new
			 * AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			 * addcorporateUserPage.addCorporateUser(driver, userName, password,
			 * config , emailId);
			 * 
			 * fc.home_page().logout(driver, config);
			 * fc.utobj().printTestStep(testCaseId, "Login With Corporate User"
			 * ); fc.loginpage().loginWithParameter(driver, userName, password);
			 */

			fc.utobj().printTestStep("Navigate To Admin > CRM > Configure Status Page");
			fc.utobj().printTestStep("Add Contact Status");
			String contactStatus = dataSet.get("contactStatus");
			AdminCRMConfigureStatusPageTest statusPage = new AdminCRMConfigureStatusPageTest();
			statusPage.addStatus01(driver, contactStatus);

			fc.utobj().printTestStep("Navigate To Admin > CRM > Contact Type Configureation");
			fc.utobj().printTestStep("Add Contact Type");
			String contactType = dataSet.get("selectcontactType");
			AdminCRMContactTypeConfigurationPageTest contactTypeConfigureationPage = new AdminCRMContactTypeConfigurationPageTest();
			contactTypeConfigureationPage.addContactType(driver, contactType);

			fc.utobj().printTestStep("Navigate To CRM > Campaign Center");
			fc.crm().crm_common().CRMModule(driver);

			fc.utobj().printTestStep("Create Campaign For Send Campaign");

			String campaignName = dataSet.get("campaignName");
			String accessibility = dataSet.get("accessibility");
			String campaignType = dataSet.get("campaignType");
			String templateName = dataSet.get("templateName");
			String emailSubject = dataSet.get("emailSubject");
			String plainTextEditor = dataSet.get("plainTextEditor");
			CRMCampaignCenterPageTest campaignCenterPage = new CRMCampaignCenterPageTest();
			campaignCenterPage.createCampaign(driver, config, campaignName, accessibility, campaignType, templateName,
					emailSubject, plainTextEditor);

			fc.utobj().printTestStep("Create Template For Send Email");

			String templateNameForSendEmail = dataSet.get("templateNameForSendEmail");
			String emailSubjectForSendEmail = dataSet.get("emailSubjectForSendEmail");
			String plainTextEditorForSendEmail = dataSet.get("plainTextEditorForSendEmail");

			CRMCampaignCenterEmailTemplatesPageTest emailTemplatePage = new CRMCampaignCenterEmailTemplatesPageTest();
			emailTemplatePage.createTemplate(driver, config, templateNameForSendEmail, emailSubjectForSendEmail,
					plainTextEditorForSendEmail);

			fc.utobj().printTestStep("Navigate To CRM > Worflows Page");
			fc.utobj().printTestStep("Create Contact Type Standard Workflow");

			String workFlowName = dataSet.get("workFlowName");
			String triggerType = dataSet.get("triggerType");
			String conditionType = dataSet.get("conditionType");
			String availableFields = dataSet.get("availableFields");
			String taskSubject = dataSet.get("taskSubject");
			String taskPriority = dataSet.get("taskPriority");
			String taskStatus = dataSet.get("taskStatus");
			String matchingCondition = dataSet.get("matchingCondition");

			createWorkFlowContactStandard(driver, config, dataSet, workFlowName, triggerType, conditionType,
					availableFields, matchingCondition, campaignName, contactStatus, templateNameForSendEmail,
					taskSubject, taskPriority, taskStatus);

			fc.utobj().printTestStep("Verify The Created WorkFlow");
			searchWorkFLowByFilter(driver, workFlowName);

			boolean isWorkFlowNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[.='" + workFlowName + "']");
			if (isWorkFlowNamePresent == false) {
				fc.utobj().throwsException("was not able to verify the workflow");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	/*
	 * @Test(groups = "crmworkflownouse")
	 * 
	 * @TestCase(createdOn="2017-05-01",updatedOn="2018-06-14",
	 * testCaseDescription =
	 * "Create The Standard WorkFlow When Lead Is Added For All Leads",
	 * testCaseId = "TC_14_Create_Workflow") public void
	 * createWorkFlow14()throws IOException, Exception,
	 * ParserConfigurationException, SAXException {
	 * 
	 * String testCaseId =
	 * fc.utobj().readTestCaseInfo(this.getClass().getName()+"."+new
	 * Object(){}.getClass().getEnclosingMethod().getName());
	 * 
	 * Map<String, String> config =
	 * fc.utobj().readConfigurationFile(FranconnectUtil.path);
	 * Map<String,String> dataSet = fc.utobj().readTestData("crmworkflow",
	 * testCaseId); WebDriver driver = fc.utobj().openDriver(config);
	 * 
	 * try { driver = fc.loginpage().login(driver, config);
	 * 
	 * String userName=fc.utobj().generateTestData(dataSet.get("userName"));
	 * String password=fc.utobj().generateTestData(dataSet.get("password"));
	 * AdminUsersManageCorporateUsersAddCorporateUserPageTest
	 * addcorporateUserPage=new
	 * AdminUsersManageCorporateUsersAddCorporateUserPageTest();
	 * addcorporateUserPage.addCorporateUser(driver, userName, password, config
	 * , emailId);
	 * 
	 * fc.home_page().logout(driver, config);
	 * fc.utobj().printTestStep(testCaseId, "Login With Corporate User");
	 * fc.loginpage().loginWithParameter(driver, userName, password);
	 * 
	 * fc.utobj().printTestStep(testCaseId,
	 * "Navigate To Admin > CRM > Configure Status Page");
	 * fc.utobj().printTestStep(testCaseId, "Add Lead Status"); String
	 * leadStatus=dataSet.get("leadStatus"); AdminCRMConfigureStatusPageTest
	 * statusPage=new AdminCRMConfigureStatusPageTest();
	 * statusPage.addStatus(driver, leadStatus, config);
	 * 
	 * fc.utobj().printTestStep(testCaseId, "Navigate To CRM > Campaign Center"
	 * ); fc.crm().crm_common().CRMModule(driver);
	 * 
	 * fc.utobj().printTestStep(testCaseId, "Create Campaign For Send Campaign"
	 * );
	 * 
	 * String campaignName=dataSet.get("campaignName"); String
	 * accessibility=dataSet.get("accessibility"); String
	 * campaignType=dataSet.get("campaignType"); String
	 * templateName=dataSet.get("templateName"); String
	 * emailSubject=dataSet.get("emailSubject"); String
	 * plainTextEditor=dataSet.get("plainTextEditor"); CRMCampaignCenterPageTest
	 * campaignCenterPage=new CRMCampaignCenterPageTest();
	 * campaignCenterPage.createCampaign(driver, config, campaignName,
	 * accessibility, campaignType, templateName, emailSubject,
	 * plainTextEditor);
	 * 
	 * fc.utobj().printTestStep(testCaseId, "Create Template For Send Email");
	 * 
	 * String templateNameForSendEmail=dataSet.get("templateNameForSendEmail");
	 * String emailSubjectForSendEmail=dataSet.get("emailSubjectForSendEmail");
	 * String
	 * plainTextEditorForSendEmail=dataSet.get("plainTextEditorForSendEmail");
	 * 
	 * CRMCampaignCenterEmailTemplatesPageTest emailTemplatePage=new
	 * CRMCampaignCenterEmailTemplatesPageTest();
	 * emailTemplatePage.createTemplate(driver, config,
	 * templateNameForSendEmail, emailSubjectForSendEmail,
	 * plainTextEditorForSendEmail);
	 * 
	 * fc.utobj().printTestStep(testCaseId, "Navigate To CRM > Worflows Page");
	 * fc.utobj().printTestStep(testCaseId, "Create Lead Type Standard Workflow"
	 * );
	 * 
	 * String workFlowName=dataSet.get("workFlowName"); String
	 * triggerType=dataSet.get("triggerType"); String
	 * conditionType=dataSet.get("conditionType"); String availableFields="";
	 * String taskSubject=dataSet.get("taskSubject"); String
	 * taskPriority=dataSet.get("taskPriority"); String
	 * taskStatus=dataSet.get("taskStatus"); String matchingCondition="";
	 * 
	 * createWorkFlowLeadStandard(driver, config, dataSet, workFlowName,
	 * triggerType, conditionType, availableFields, matchingCondition,
	 * campaignName, leadStatus, templateNameForSendEmail, taskSubject,
	 * taskPriority, taskStatus);
	 * 
	 * fc.utobj().logoutAndQuitBrowser(driver, config, testCaseId); } catch
	 * (Exception e) {
	 * 
	 * fc.utobj().quitBrowserOnCatch(driver, config, e, testCaseId);} }
	 * 
	 * 
	 * 
	 * @Test(groups = "crmworkflownouse")
	 * 
	 * @TestCase(createdOn="2017-05-01",updatedOn="2018-06-14",
	 * testCaseDescription =
	 * "Create The Standard WorkFlow When Contact Is Added For All Contacts",
	 * testCaseId = "TC_15_Create_Workflow") public void
	 * createWorkFlow15()throws IOException, Exception,
	 * ParserConfigurationException, SAXException {
	 * 
	 * String testCaseId =
	 * fc.utobj().readTestCaseInfo(this.getClass().getName()+"."+new
	 * Object(){}.getClass().getEnclosingMethod().getName());
	 * 
	 * Map<String, String> config =
	 * fc.utobj().readConfigurationFile(FranconnectUtil.path);
	 * Map<String,String> dataSet = fc.utobj().readTestData("crmworkflow",
	 * testCaseId); WebDriver driver = fc.utobj().openDriver(config);
	 * 
	 * try { driver = fc.loginpage().login(driver, config);
	 * 
	 * String userName=fc.utobj().generateTestData(dataSet.get("userName"));
	 * String password=fc.utobj().generateTestData(dataSet.get("password"));
	 * AdminUsersManageCorporateUsersAddCorporateUserPageTest
	 * addcorporateUserPage=new
	 * AdminUsersManageCorporateUsersAddCorporateUserPageTest();
	 * addcorporateUserPage.addCorporateUser(driver, userName, password, config
	 * , emailId);
	 * 
	 * fc.home_page().logout(driver, config);
	 * fc.utobj().printTestStep(testCaseId, "Login With Corporate User");
	 * fc.loginpage().loginWithParameter(driver, userName, password);
	 * 
	 * fc.utobj().printTestStep(testCaseId,
	 * "Navigate To Admin > CRM > Configure Status Page");
	 * fc.utobj().printTestStep(testCaseId, "Add Contact Status"); String
	 * contactStatus=dataSet.get("contactStatus");
	 * AdminCRMConfigureStatusPageTest statusPage=new
	 * AdminCRMConfigureStatusPageTest(); statusPage.addStatus01(driver,
	 * contactStatus, config);
	 * 
	 * fc.utobj().printTestStep(testCaseId, "Navigate To CRM > Campaign Center"
	 * ); fc.crm().crm_common().CRMModule(driver);
	 * 
	 * fc.utobj().printTestStep(testCaseId, "Create Campaign For Send Campaign"
	 * );
	 * 
	 * String campaignName=dataSet.get("campaignName"); String
	 * accessibility=dataSet.get("accessibility"); String
	 * campaignType=dataSet.get("campaignType"); String
	 * templateName=dataSet.get("templateName"); String
	 * emailSubject=dataSet.get("emailSubject"); String
	 * plainTextEditor=dataSet.get("plainTextEditor"); CRMCampaignCenterPageTest
	 * campaignCenterPage=new CRMCampaignCenterPageTest();
	 * campaignCenterPage.createCampaign(driver, config, campaignName,
	 * accessibility, campaignType, templateName, emailSubject,
	 * plainTextEditor);
	 * 
	 * fc.utobj().printTestStep(testCaseId, "Create Template For Send Email");
	 * 
	 * String templateNameForSendEmail=dataSet.get("templateNameForSendEmail");
	 * String emailSubjectForSendEmail=dataSet.get("emailSubjectForSendEmail");
	 * String
	 * plainTextEditorForSendEmail=dataSet.get("plainTextEditorForSendEmail");
	 * 
	 * CRMCampaignCenterEmailTemplatesPageTest emailTemplatePage=new
	 * CRMCampaignCenterEmailTemplatesPageTest();
	 * emailTemplatePage.createTemplate(driver, config,
	 * templateNameForSendEmail, emailSubjectForSendEmail,
	 * plainTextEditorForSendEmail);
	 * 
	 * fc.utobj().printTestStep(testCaseId, "Navigate To CRM > Worflows Page");
	 * fc.utobj().printTestStep(testCaseId,
	 * "Create Contact Type Standard Workflow");
	 * 
	 * String workFlowName=dataSet.get("workFlowName"); String
	 * triggerType=dataSet.get("triggerType"); String
	 * conditionType=dataSet.get("conditionType"); String availableFields="";
	 * String taskSubject=dataSet.get("taskSubject"); String
	 * taskPriority=dataSet.get("taskPriority"); String
	 * taskStatus=dataSet.get("taskStatus"); String matchingCondition="";
	 * 
	 * createWorkFlowContactStandard(driver, config, dataSet, workFlowName,
	 * triggerType, conditionType, availableFields, matchingCondition,
	 * campaignName, contactStatus, templateNameForSendEmail, taskSubject,
	 * taskPriority, taskStatus);
	 * 
	 * fc.utobj().logoutAndQuitBrowser(driver, config, testCaseId); } catch
	 * (Exception e) {
	 * 
	 * fc.utobj().quitBrowserOnCatch(driver, config, e, testCaseId);} }
	 * 
	 * 
	 * @Test(groups = "crmworkflownouse")
	 * 
	 * @TestCase(createdOn="2017-05-01",updatedOn="2018-06-14",
	 * testCaseDescription =
	 * "Create The Standard WorkFlow When Lead Is Modified For All Leads",
	 * testCaseId = "TC_16_Create_Workflow") public void
	 * createWorkFlow16()throws IOException, Exception,
	 * ParserConfigurationException, SAXException {
	 * 
	 * String testCaseId =
	 * fc.utobj().readTestCaseInfo(this.getClass().getName()+"."+new
	 * Object(){}.getClass().getEnclosingMethod().getName());
	 * 
	 * Map<String, String> config =
	 * fc.utobj().readConfigurationFile(FranconnectUtil.path);
	 * Map<String,String> dataSet = fc.utobj().readTestData("crmworkflow",
	 * testCaseId); WebDriver driver = fc.utobj().openDriver(config);
	 * 
	 * try { driver = fc.loginpage().login(driver, config);
	 * 
	 * String userName=fc.utobj().generateTestData(dataSet.get("userName"));
	 * String password=fc.utobj().generateTestData(dataSet.get("password"));
	 * AdminUsersManageCorporateUsersAddCorporateUserPageTest
	 * addcorporateUserPage=new
	 * AdminUsersManageCorporateUsersAddCorporateUserPageTest();
	 * addcorporateUserPage.addCorporateUser(driver, userName, password, config
	 * , emailId);
	 * 
	 * fc.home_page().logout(driver, config);
	 * fc.utobj().printTestStep(testCaseId, "Login With Corporate User");
	 * fc.loginpage().loginWithParameter(driver, userName, password);
	 * 
	 * fc.utobj().printTestStep(testCaseId,
	 * "Navigate To Admin > CRM > Configure Status Page");
	 * fc.utobj().printTestStep(testCaseId, "Add Lead Status"); String
	 * leadStatus=dataSet.get("leadStatus"); AdminCRMConfigureStatusPageTest
	 * statusPage=new AdminCRMConfigureStatusPageTest();
	 * statusPage.addStatus(driver, leadStatus, config);
	 * 
	 * fc.utobj().printTestStep(testCaseId, "Navigate To CRM > Campaign Center"
	 * ); fc.crm().crm_common().CRMModule(driver);
	 * 
	 * fc.utobj().printTestStep(testCaseId, "Create Campaign For Send Campaign"
	 * );
	 * 
	 * String campaignName=dataSet.get("campaignName"); String
	 * accessibility=dataSet.get("accessibility"); String
	 * campaignType=dataSet.get("campaignType"); String
	 * templateName=dataSet.get("templateName"); String
	 * emailSubject=dataSet.get("emailSubject"); String
	 * plainTextEditor=dataSet.get("plainTextEditor"); CRMCampaignCenterPageTest
	 * campaignCenterPage=new CRMCampaignCenterPageTest();
	 * campaignCenterPage.createCampaign(driver, config, campaignName,
	 * accessibility, campaignType, templateName, emailSubject,
	 * plainTextEditor);
	 * 
	 * fc.utobj().printTestStep(testCaseId, "Create Template For Send Email");
	 * 
	 * String templateNameForSendEmail=dataSet.get("templateNameForSendEmail");
	 * String emailSubjectForSendEmail=dataSet.get("emailSubjectForSendEmail");
	 * String
	 * plainTextEditorForSendEmail=dataSet.get("plainTextEditorForSendEmail");
	 * 
	 * CRMCampaignCenterEmailTemplatesPageTest emailTemplatePage=new
	 * CRMCampaignCenterEmailTemplatesPageTest();
	 * emailTemplatePage.createTemplate(driver, config,
	 * templateNameForSendEmail, emailSubjectForSendEmail,
	 * plainTextEditorForSendEmail);
	 * 
	 * fc.utobj().printTestStep(testCaseId, "Navigate To CRM > Worflows Page");
	 * fc.utobj().printTestStep(testCaseId, "Create Lead Type Standard Workflow"
	 * );
	 * 
	 * String workFlowName=dataSet.get("workFlowName"); String
	 * triggerType=dataSet.get("triggerType"); String
	 * conditionType=dataSet.get("conditionType"); String availableFields="";
	 * String taskSubject=dataSet.get("taskSubject"); String
	 * taskPriority=dataSet.get("taskPriority"); String
	 * taskStatus=dataSet.get("taskStatus"); String matchingCondition="";
	 * 
	 * createWorkFlowLeadStandard(driver, config, dataSet, workFlowName,
	 * triggerType, conditionType, availableFields, matchingCondition,
	 * campaignName, leadStatus, templateNameForSendEmail, taskSubject,
	 * taskPriority, taskStatus);
	 * 
	 * fc.utobj().logoutAndQuitBrowser(driver, config, testCaseId); } catch
	 * (Exception e) {
	 * 
	 * fc.utobj().quitBrowserOnCatch(driver, config, e, testCaseId);} }
	 * 
	 * 
	 * 
	 * @Test(groups = "crmworkflownouse")
	 * 
	 * @TestCase(createdOn="2017-05-01",updatedOn="2018-06-14",
	 * testCaseDescription =
	 * "Create The Standard WorkFlow When Contact Is Modified For All Contacts",
	 * testCaseId = "TC_17_Create_Workflow") public void
	 * createWorkFlow17()throws IOException, Exception,
	 * ParserConfigurationException, SAXException {
	 * 
	 * String testCaseId =
	 * fc.utobj().readTestCaseInfo(this.getClass().getName()+"."+new
	 * Object(){}.getClass().getEnclosingMethod().getName());
	 * 
	 * Map<String, String> config =
	 * fc.utobj().readConfigurationFile(FranconnectUtil.path);
	 * Map<String,String> dataSet = fc.utobj().readTestData("crmworkflow",
	 * testCaseId); WebDriver driver = fc.utobj().openDriver(config);
	 * 
	 * try { driver = fc.loginpage().login(driver, config);
	 * 
	 * String userName=fc.utobj().generateTestData(dataSet.get("userName"));
	 * String password=fc.utobj().generateTestData(dataSet.get("password"));
	 * AdminUsersManageCorporateUsersAddCorporateUserPageTest
	 * addcorporateUserPage=new
	 * AdminUsersManageCorporateUsersAddCorporateUserPageTest();
	 * addcorporateUserPage.addCorporateUser(driver, userName, password, config
	 * , emailId);
	 * 
	 * fc.home_page().logout(driver, config);
	 * fc.utobj().printTestStep(testCaseId, "Login With Corporate User");
	 * fc.loginpage().loginWithParameter(driver, userName, password);
	 * 
	 * fc.utobj().printTestStep(testCaseId,
	 * "Navigate To Admin > CRM > Configure Status Page");
	 * fc.utobj().printTestStep(testCaseId, "Add Contact Status"); String
	 * contactStatus=dataSet.get("contactStatus");
	 * AdminCRMConfigureStatusPageTest statusPage=new
	 * AdminCRMConfigureStatusPageTest(); statusPage.addStatus01(driver,
	 * contactStatus, config);
	 * 
	 * fc.utobj().printTestStep(testCaseId, "Navigate To CRM > Campaign Center"
	 * ); fc.crm().crm_common().CRMModule(driver);
	 * 
	 * fc.utobj().printTestStep(testCaseId, "Create Campaign For Send Campaign"
	 * );
	 * 
	 * String campaignName=dataSet.get("campaignName"); String
	 * accessibility=dataSet.get("accessibility"); String
	 * campaignType=dataSet.get("campaignType"); String
	 * templateName=dataSet.get("templateName"); String
	 * emailSubject=dataSet.get("emailSubject"); String
	 * plainTextEditor=dataSet.get("plainTextEditor"); CRMCampaignCenterPageTest
	 * campaignCenterPage=new CRMCampaignCenterPageTest();
	 * campaignCenterPage.createCampaign(driver, config, campaignName,
	 * accessibility, campaignType, templateName, emailSubject,
	 * plainTextEditor);
	 * 
	 * fc.utobj().printTestStep(testCaseId, "Create Template For Send Email");
	 * 
	 * String templateNameForSendEmail=dataSet.get("templateNameForSendEmail");
	 * String emailSubjectForSendEmail=dataSet.get("emailSubjectForSendEmail");
	 * String
	 * plainTextEditorForSendEmail=dataSet.get("plainTextEditorForSendEmail");
	 * 
	 * CRMCampaignCenterEmailTemplatesPageTest emailTemplatePage=new
	 * CRMCampaignCenterEmailTemplatesPageTest();
	 * emailTemplatePage.createTemplate(driver, config,
	 * templateNameForSendEmail, emailSubjectForSendEmail,
	 * plainTextEditorForSendEmail);
	 * 
	 * fc.utobj().printTestStep(testCaseId, "Navigate To CRM > Worflows Page");
	 * fc.utobj().printTestStep(testCaseId,
	 * "Create Contact Type Standard Workflow");
	 * 
	 * String workFlowName=dataSet.get("workFlowName"); String
	 * triggerType=dataSet.get("triggerType"); String
	 * conditionType=dataSet.get("conditionType"); String availableFields="";
	 * String taskSubject=dataSet.get("taskSubject"); String
	 * taskPriority=dataSet.get("taskPriority"); String
	 * taskStatus=dataSet.get("taskStatus"); String matchingCondition="";
	 * 
	 * createWorkFlowContactStandard(driver, config, dataSet, workFlowName,
	 * triggerType, conditionType, availableFields, matchingCondition,
	 * campaignName, contactStatus, templateNameForSendEmail, taskSubject,
	 * taskPriority, taskStatus);
	 * 
	 * fc.utobj().logoutAndQuitBrowser(driver, config, testCaseId); } catch
	 * (Exception e) {
	 * 
	 * fc.utobj().quitBrowserOnCatch(driver, config, e, testCaseId);} }
	 * 
	 * 
	 * @Test(groups = "crmworkflownouse")
	 * 
	 * @TestCase(createdOn="2017-05-01",updatedOn="2018-06-14",
	 * testCaseDescription =
	 * "Create The Standard WorkFlow When Lead Is Added Or Modified For All Leads"
	 * , testCaseId = "TC_18_Create_Workflow") public void
	 * createWorkFlow18()throws IOException, Exception,
	 * ParserConfigurationException, SAXException {
	 * 
	 * String testCaseId =
	 * fc.utobj().readTestCaseInfo(this.getClass().getName()+"."+new
	 * Object(){}.getClass().getEnclosingMethod().getName());
	 * 
	 * Map<String, String> config =
	 * fc.utobj().readConfigurationFile(FranconnectUtil.path);
	 * Map<String,String> dataSet = fc.utobj().readTestData("crmworkflow",
	 * testCaseId); WebDriver driver = fc.utobj().openDriver(config);
	 * 
	 * try { driver = fc.loginpage().login(driver, config);
	 * 
	 * String userName=fc.utobj().generateTestData(dataSet.get("userName"));
	 * String password=fc.utobj().generateTestData(dataSet.get("password"));
	 * AdminUsersManageCorporateUsersAddCorporateUserPageTest
	 * addcorporateUserPage=new
	 * AdminUsersManageCorporateUsersAddCorporateUserPageTest();
	 * addcorporateUserPage.addCorporateUser(driver, userName, password, config
	 * , emailId);
	 * 
	 * fc.home_page().logout(driver, config);
	 * fc.utobj().printTestStep(testCaseId, "Login With Corporate User");
	 * fc.loginpage().loginWithParameter(driver, userName, password);
	 * 
	 * fc.utobj().printTestStep(testCaseId,
	 * "Navigate To Admin > CRM > Configure Status Page");
	 * fc.utobj().printTestStep(testCaseId, "Add Lead Status"); String
	 * leadStatus=dataSet.get("leadStatus"); AdminCRMConfigureStatusPageTest
	 * statusPage=new AdminCRMConfigureStatusPageTest();
	 * statusPage.addStatus(driver, leadStatus, config);
	 * 
	 * fc.utobj().printTestStep(testCaseId, "Navigate To CRM > Campaign Center"
	 * ); fc.crm().crm_common().CRMModule(driver);
	 * 
	 * fc.utobj().printTestStep(testCaseId, "Create Campaign For Send Campaign"
	 * );
	 * 
	 * String campaignName=dataSet.get("campaignName"); String
	 * accessibility=dataSet.get("accessibility"); String
	 * campaignType=dataSet.get("campaignType"); String
	 * templateName=dataSet.get("templateName"); String
	 * emailSubject=dataSet.get("emailSubject"); String
	 * plainTextEditor=dataSet.get("plainTextEditor"); CRMCampaignCenterPageTest
	 * campaignCenterPage=new CRMCampaignCenterPageTest();
	 * campaignCenterPage.createCampaign(driver, config, campaignName,
	 * accessibility, campaignType, templateName, emailSubject,
	 * plainTextEditor);
	 * 
	 * fc.utobj().printTestStep(testCaseId, "Create Template For Send Email");
	 * 
	 * String templateNameForSendEmail=dataSet.get("templateNameForSendEmail");
	 * String emailSubjectForSendEmail=dataSet.get("emailSubjectForSendEmail");
	 * String
	 * plainTextEditorForSendEmail=dataSet.get("plainTextEditorForSendEmail");
	 * 
	 * CRMCampaignCenterEmailTemplatesPageTest emailTemplatePage=new
	 * CRMCampaignCenterEmailTemplatesPageTest();
	 * emailTemplatePage.createTemplate(driver, config,
	 * templateNameForSendEmail, emailSubjectForSendEmail,
	 * plainTextEditorForSendEmail);
	 * 
	 * fc.utobj().printTestStep(testCaseId, "Navigate To CRM > Worflows Page");
	 * fc.utobj().printTestStep(testCaseId, "Create Lead Type Standard Workflow"
	 * );
	 * 
	 * String workFlowName=dataSet.get("workFlowName"); String
	 * triggerType=dataSet.get("triggerType"); String
	 * conditionType=dataSet.get("conditionType"); String availableFields="";
	 * String taskSubject=dataSet.get("taskSubject"); String
	 * taskPriority=dataSet.get("taskPriority"); String
	 * taskStatus=dataSet.get("taskStatus"); String matchingCondition="";
	 * 
	 * createWorkFlowLeadStandard(driver, config, dataSet, workFlowName,
	 * triggerType, conditionType, availableFields, matchingCondition,
	 * campaignName, leadStatus, templateNameForSendEmail, taskSubject,
	 * taskPriority, taskStatus);
	 * 
	 * fc.utobj().logoutAndQuitBrowser(driver, config, testCaseId); } catch
	 * (Exception e) {
	 * 
	 * fc.utobj().quitBrowserOnCatch(driver, config, e, testCaseId);} }
	 * 
	 * 
	 * 
	 * @Test(groups = "crmworkflownouse")
	 * 
	 * @TestCase(createdOn="2017-05-01",updatedOn="2018-06-14",
	 * testCaseDescription =
	 * "Create The Standard WorkFlow When Contact Is Added Or Modified For All Contacts"
	 * , testCaseId = "TC_19_Create_Workflow") public void
	 * createWorkFlow19()throws IOException, Exception,
	 * ParserConfigurationException, SAXException {
	 * 
	 * String testCaseId =
	 * fc.utobj().readTestCaseInfo(this.getClass().getName()+"."+new
	 * Object(){}.getClass().getEnclosingMethod().getName());
	 * 
	 * Map<String, String> config =
	 * fc.utobj().readConfigurationFile(FranconnectUtil.path);
	 * Map<String,String> dataSet = fc.utobj().readTestData("crmworkflow",
	 * testCaseId); WebDriver driver = fc.utobj().openDriver(config);
	 * 
	 * try { driver = fc.loginpage().login(driver, config);
	 * 
	 * String userName=fc.utobj().generateTestData(dataSet.get("userName"));
	 * String password=fc.utobj().generateTestData(dataSet.get("password"));
	 * AdminUsersManageCorporateUsersAddCorporateUserPageTest
	 * addcorporateUserPage=new
	 * AdminUsersManageCorporateUsersAddCorporateUserPageTest();
	 * addcorporateUserPage.addCorporateUser(driver, userName, password, config
	 * , emailId);
	 * 
	 * fc.home_page().logout(driver, config);
	 * fc.utobj().printTestStep(testCaseId, "Login With Corporate User");
	 * fc.loginpage().loginWithParameter(driver, userName, password);
	 * 
	 * fc.utobj().printTestStep(testCaseId,
	 * "Navigate To Admin > CRM > Configure Status Page");
	 * fc.utobj().printTestStep(testCaseId, "Add Contact Status"); String
	 * contactStatus=dataSet.get("contactStatus");
	 * AdminCRMConfigureStatusPageTest statusPage=new
	 * AdminCRMConfigureStatusPageTest(); statusPage.addStatus01(driver,
	 * contactStatus, config);
	 * 
	 * fc.utobj().printTestStep(testCaseId, "Navigate To CRM > Campaign Center"
	 * ); fc.crm().crm_common().CRMModule(driver);
	 * 
	 * fc.utobj().printTestStep(testCaseId, "Create Campaign For Send Campaign"
	 * );
	 * 
	 * String campaignName=dataSet.get("campaignName"); String
	 * accessibility=dataSet.get("accessibility"); String
	 * campaignType=dataSet.get("campaignType"); String
	 * templateName=dataSet.get("templateName"); String
	 * emailSubject=dataSet.get("emailSubject"); String
	 * plainTextEditor=dataSet.get("plainTextEditor"); CRMCampaignCenterPageTest
	 * campaignCenterPage=new CRMCampaignCenterPageTest();
	 * campaignCenterPage.createCampaign(driver, config, campaignName,
	 * accessibility, campaignType, templateName, emailSubject,
	 * plainTextEditor);
	 * 
	 * fc.utobj().printTestStep(testCaseId, "Create Template For Send Email");
	 * 
	 * String templateNameForSendEmail=dataSet.get("templateNameForSendEmail");
	 * String emailSubjectForSendEmail=dataSet.get("emailSubjectForSendEmail");
	 * String
	 * plainTextEditorForSendEmail=dataSet.get("plainTextEditorForSendEmail");
	 * 
	 * CRMCampaignCenterEmailTemplatesPageTest emailTemplatePage=new
	 * CRMCampaignCenterEmailTemplatesPageTest();
	 * emailTemplatePage.createTemplate(driver, config,
	 * templateNameForSendEmail, emailSubjectForSendEmail,
	 * plainTextEditorForSendEmail);
	 * 
	 * fc.utobj().printTestStep(testCaseId, "Navigate To CRM > Worflows Page");
	 * fc.utobj().printTestStep(testCaseId,
	 * "Create Contact Type Standard Workflow");
	 * 
	 * String workFlowName=dataSet.get("workFlowName"); String
	 * triggerType=dataSet.get("triggerType"); String
	 * conditionType=dataSet.get("conditionType"); String availableFields="";
	 * String taskSubject=dataSet.get("taskSubject"); String
	 * taskPriority=dataSet.get("taskPriority"); String
	 * taskStatus=dataSet.get("taskStatus"); String matchingCondition="";
	 * 
	 * createWorkFlowContactStandard(driver, config, dataSet, workFlowName,
	 * triggerType, conditionType, availableFields, matchingCondition,
	 * campaignName, contactStatus, templateNameForSendEmail, taskSubject,
	 * taskPriority, taskStatus);
	 * 
	 * fc.utobj().logoutAndQuitBrowser(driver, config, testCaseId); } catch
	 * (Exception e) {
	 * 
	 * fc.utobj().quitBrowserOnCatch(driver, config, e, testCaseId);} }
	 */

	@Test(groups = "crmworkflow")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Create The Standard WorkFlow When Lead Is Modified For Leads Matching Criteria Email Field is Not Empty", testCaseId = "TC_20_Create_Workflow")
	public void createWorkFlow20() throws IOException, Exception, ParserConfigurationException, SAXException {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crmworkflow", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			/*
			 * 
			 * String
			 * userName=fc.utobj().generateTestData(dataSet.get("userName"));
			 * String
			 * password=fc.utobj().generateTestData(dataSet.get("password"));
			 * String emailId="crmautomation@staffex.com";
			 * AdminUsersManageCorporateUsersAddCorporateUserPageTest
			 * addcorporateUserPage=new
			 * AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			 * addcorporateUserPage.addCorporateUser(driver, userName, password,
			 * config , emailId);
			 * 
			 * fc.home_page().logout(driver, config);
			 * fc.utobj().printTestStep(testCaseId, "Login With Corporate User"
			 * ); fc.loginpage().loginWithParameter(driver, userName, password);
			 */

			fc.utobj().printTestStep("Navigate To Admin > CRM > Configure Status Page");
			fc.utobj().printTestStep("Add Lead Status");
			String leadStatus = dataSet.get("leadStatus");
			AdminCRMConfigureStatusPageTest statusPage = new AdminCRMConfigureStatusPageTest();
			statusPage.addStatus(driver, leadStatus);

			fc.utobj().printTestStep("Navigate To CRM > Campaign Center");
			fc.crm().crm_common().CRMModule(driver);

			fc.utobj().printTestStep("Create Campaign For Send Campaign");

			String campaignName = dataSet.get("campaignName");
			String accessibility = dataSet.get("accessibility");
			String campaignType = dataSet.get("campaignType");
			String templateName = dataSet.get("templateName");
			String emailSubject = dataSet.get("emailSubject");
			String plainTextEditor = dataSet.get("plainTextEditor");
			CRMCampaignCenterPageTest campaignCenterPage = new CRMCampaignCenterPageTest();
			campaignCenterPage.createCampaign(driver, config, campaignName, accessibility, campaignType, templateName,
					emailSubject, plainTextEditor);

			fc.utobj().printTestStep("Create Template For Send Email");

			String templateNameForSendEmail = dataSet.get("templateNameForSendEmail");
			String emailSubjectForSendEmail = dataSet.get("emailSubjectForSendEmail");
			String plainTextEditorForSendEmail = dataSet.get("plainTextEditorForSendEmail");

			CRMCampaignCenterEmailTemplatesPageTest emailTemplatePage = new CRMCampaignCenterEmailTemplatesPageTest();
			emailTemplatePage.createTemplate(driver, config, templateNameForSendEmail, emailSubjectForSendEmail,
					plainTextEditorForSendEmail);

			fc.utobj().printTestStep("Navigate To CRM > Worflows Page");
			fc.utobj().printTestStep("Create Lead Type Standard Workflow");

			String workFlowName = dataSet.get("workFlowName");
			String triggerType = dataSet.get("triggerType");
			String conditionType = dataSet.get("conditionType");
			String availableFields = dataSet.get("availableFields");
			String taskSubject = dataSet.get("taskSubject");
			String taskPriority = dataSet.get("taskPriority");
			String taskStatus = dataSet.get("taskStatus");
			String matchingCondition = dataSet.get("matchingCondition");

			createWorkFlowLeadStandard(driver, config, dataSet, workFlowName, triggerType, conditionType,
					availableFields, matchingCondition, campaignName, leadStatus, templateNameForSendEmail, taskSubject,
					taskPriority, taskStatus);

			fc.utobj().printTestStep("Verify The Created WorkFlow");
			searchWorkFLowByFilter(driver, workFlowName);

			boolean isWorkFlowNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[.='" + workFlowName + "']");
			if (isWorkFlowNamePresent == false) {
				fc.utobj().throwsException("was not able to verify the workflow");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "crmworkflow")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Create The Standard WorkFlow When Lead is Modified For Leads Matching Criteria Status OF Lead Is Default[New]", testCaseId = "TC_21_Create_Workflow")
	public void createWorkFlow21() throws IOException, Exception, ParserConfigurationException, SAXException {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crmworkflow", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			/*
			 * 
			 * String
			 * userName=fc.utobj().generateTestData(dataSet.get("userName"));
			 * String
			 * password=fc.utobj().generateTestData(dataSet.get("password"));
			 * String emailId="crmautomation@staffex.com";
			 * AdminUsersManageCorporateUsersAddCorporateUserPageTest
			 * addcorporateUserPage=new
			 * AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			 * addcorporateUserPage.addCorporateUser(driver, userName, password,
			 * config , emailId);
			 * 
			 * fc.home_page().logout(driver, config);
			 * fc.utobj().printTestStep(testCaseId, "Login With Corporate User"
			 * ); fc.loginpage().loginWithParameter(driver, userName, password);
			 */

			fc.utobj().printTestStep("Navigate To Admin > CRM > Configure Status Page");
			fc.utobj().printTestStep("Add Lead Status");
			String leadStatus = dataSet.get("leadStatus");
			AdminCRMConfigureStatusPageTest statusPage = new AdminCRMConfigureStatusPageTest();
			statusPage.addStatus(driver, leadStatus);

			fc.utobj().printTestStep("Navigate To CRM > Campaign Center");
			fc.crm().crm_common().CRMModule(driver);

			fc.utobj().printTestStep("Create Campaign For Send Campaign");

			String campaignName = dataSet.get("campaignName");
			String accessibility = dataSet.get("accessibility");
			String campaignType = dataSet.get("campaignType");
			String templateName = dataSet.get("templateName");
			String emailSubject = dataSet.get("emailSubject");
			String plainTextEditor = dataSet.get("plainTextEditor");
			CRMCampaignCenterPageTest campaignCenterPage = new CRMCampaignCenterPageTest();
			campaignCenterPage.createCampaign(driver, config, campaignName, accessibility, campaignType, templateName,
					emailSubject, plainTextEditor);

			fc.utobj().printTestStep("Create Template For Send Email");

			String templateNameForSendEmail = dataSet.get("templateNameForSendEmail");
			String emailSubjectForSendEmail = dataSet.get("emailSubjectForSendEmail");
			String plainTextEditorForSendEmail = dataSet.get("plainTextEditorForSendEmail");

			CRMCampaignCenterEmailTemplatesPageTest emailTemplatePage = new CRMCampaignCenterEmailTemplatesPageTest();
			emailTemplatePage.createTemplate(driver, config, templateNameForSendEmail, emailSubjectForSendEmail,
					plainTextEditorForSendEmail);

			fc.utobj().printTestStep("Navigate To CRM > Worflows Page");
			fc.utobj().printTestStep("Create Lead Type Standard Workflow");

			String workFlowName = dataSet.get("workFlowName");
			String triggerType = dataSet.get("triggerType");
			String conditionType = dataSet.get("conditionType");
			String availableFields = dataSet.get("availableFields");
			String taskSubject = dataSet.get("taskSubject");
			String taskPriority = dataSet.get("taskPriority");
			String taskStatus = dataSet.get("taskStatus");
			String matchingCondition = dataSet.get("matchingCondition");

			createWorkFlowLeadStandard(driver, config, dataSet, workFlowName, triggerType, conditionType,
					availableFields, matchingCondition, campaignName, leadStatus, templateNameForSendEmail, taskSubject,
					taskPriority, taskStatus);

			fc.utobj().printTestStep("Verify The Created WorkFlow");
			searchWorkFLowByFilter(driver, workFlowName);

			boolean isWorkFlowNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[.='" + workFlowName + "']");
			if (isWorkFlowNamePresent == false) {
				fc.utobj().throwsException("was not able to verify the workflow");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "crmworkflow")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Create The Standard WorkFlow When Lead is Modified For Leads Matching Criteria Email Subscription status is Set To Opted In", testCaseId = "TC_22_Create_Workflow")
	public void createWorkFlow22() throws IOException, Exception, ParserConfigurationException, SAXException {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crmworkflow", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			/*
			 * 
			 * String
			 * userName=fc.utobj().generateTestData(dataSet.get("userName"));
			 * String
			 * password=fc.utobj().generateTestData(dataSet.get("password"));
			 * String emailId="crmautomation@staffex.com";
			 * AdminUsersManageCorporateUsersAddCorporateUserPageTest
			 * addcorporateUserPage=new
			 * AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			 * addcorporateUserPage.addCorporateUser(driver, userName, password,
			 * config , emailId);
			 * 
			 * fc.home_page().logout(driver, config);
			 * fc.utobj().printTestStep(testCaseId, "Login With Corporate User"
			 * ); fc.loginpage().loginWithParameter(driver, userName, password);
			 */

			fc.utobj().printTestStep("Navigate To Admin > CRM > Configure Status Page");
			fc.utobj().printTestStep("Add Lead Status");
			String leadStatus = dataSet.get("leadStatus");
			AdminCRMConfigureStatusPageTest statusPage = new AdminCRMConfigureStatusPageTest();
			statusPage.addStatus(driver, leadStatus);

			fc.utobj().printTestStep("Navigate To CRM > Campaign Center");
			fc.crm().crm_common().CRMModule(driver);

			fc.utobj().printTestStep("Create Campaign For Send Campaign");

			String campaignName = dataSet.get("campaignName");
			String accessibility = dataSet.get("accessibility");
			String campaignType = dataSet.get("campaignType");
			String templateName = dataSet.get("templateName");
			String emailSubject = dataSet.get("emailSubject");
			String plainTextEditor = dataSet.get("plainTextEditor");
			CRMCampaignCenterPageTest campaignCenterPage = new CRMCampaignCenterPageTest();
			campaignCenterPage.createCampaign(driver, config, campaignName, accessibility, campaignType, templateName,
					emailSubject, plainTextEditor);

			fc.utobj().printTestStep("Create Template For Send Email");

			String templateNameForSendEmail = dataSet.get("templateNameForSendEmail");
			String emailSubjectForSendEmail = dataSet.get("emailSubjectForSendEmail");
			String plainTextEditorForSendEmail = dataSet.get("plainTextEditorForSendEmail");

			CRMCampaignCenterEmailTemplatesPageTest emailTemplatePage = new CRMCampaignCenterEmailTemplatesPageTest();
			emailTemplatePage.createTemplate(driver, config, templateNameForSendEmail, emailSubjectForSendEmail,
					plainTextEditorForSendEmail);

			fc.utobj().printTestStep("Navigate To CRM > Worflows Page");
			fc.utobj().printTestStep("Create Lead Type Standard Workflow");

			String workFlowName = dataSet.get("workFlowName");
			String triggerType = dataSet.get("triggerType");
			String conditionType = dataSet.get("conditionType");
			String availableFields = dataSet.get("availableFields");
			String taskSubject = dataSet.get("taskSubject");
			String taskPriority = dataSet.get("taskPriority");
			String taskStatus = dataSet.get("taskStatus");
			String matchingCondition = dataSet.get("matchingCondition");

			createWorkFlowLeadStandard(driver, config, dataSet, workFlowName, triggerType, conditionType,
					availableFields, matchingCondition, campaignName, leadStatus, templateNameForSendEmail, taskSubject,
					taskPriority, taskStatus);

			fc.utobj().printTestStep("Verify The Created WorkFlow");
			searchWorkFLowByFilter(driver, workFlowName);

			boolean isWorkFlowNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[.='" + workFlowName + "']");
			if (isWorkFlowNamePresent == false) {
				fc.utobj().throwsException("was not able to verify the workflow");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "crmworkflow")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Create The Standard WorkFlow When Lead is Modified For Leads Matching Criteria Country And All States of It", testCaseId = "TC_23_Create_Workflow")
	public void createWorkFlow23() throws IOException, Exception, ParserConfigurationException, SAXException {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crmworkflow", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			/*
			 * 
			 * String
			 * userName=fc.utobj().generateTestData(dataSet.get("userName"));
			 * String
			 * password=fc.utobj().generateTestData(dataSet.get("password"));
			 * String emailId="crmautomation@staffex.com";
			 * AdminUsersManageCorporateUsersAddCorporateUserPageTest
			 * addcorporateUserPage=new
			 * AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			 * addcorporateUserPage.addCorporateUser(driver, userName, password,
			 * config , emailId);
			 * 
			 * fc.home_page().logout(driver, config);
			 * fc.utobj().printTestStep(testCaseId, "Login With Corporate User"
			 * ); fc.loginpage().loginWithParameter(driver, userName, password);
			 */

			fc.utobj().printTestStep("Navigate To Admin > CRM > Configure Status Page");
			fc.utobj().printTestStep("Add Lead Status");
			String leadStatus = dataSet.get("leadStatus");
			AdminCRMConfigureStatusPageTest statusPage = new AdminCRMConfigureStatusPageTest();
			statusPage.addStatus(driver, leadStatus);

			fc.utobj().printTestStep("Navigate To CRM > Campaign Center");
			fc.crm().crm_common().CRMModule(driver);

			fc.utobj().printTestStep("Create Campaign For Send Campaign");

			String campaignName = dataSet.get("campaignName");
			String accessibility = dataSet.get("accessibility");
			String campaignType = dataSet.get("campaignType");
			String templateName = dataSet.get("templateName");
			String emailSubject = dataSet.get("emailSubject");
			String plainTextEditor = dataSet.get("plainTextEditor");
			CRMCampaignCenterPageTest campaignCenterPage = new CRMCampaignCenterPageTest();
			campaignCenterPage.createCampaign(driver, config, campaignName, accessibility, campaignType, templateName,
					emailSubject, plainTextEditor);

			fc.utobj().printTestStep("Create Template For Send Email");

			String templateNameForSendEmail = dataSet.get("templateNameForSendEmail");
			String emailSubjectForSendEmail = dataSet.get("emailSubjectForSendEmail");
			String plainTextEditorForSendEmail = dataSet.get("plainTextEditorForSendEmail");

			CRMCampaignCenterEmailTemplatesPageTest emailTemplatePage = new CRMCampaignCenterEmailTemplatesPageTest();
			emailTemplatePage.createTemplate(driver, config, templateNameForSendEmail, emailSubjectForSendEmail,
					plainTextEditorForSendEmail);

			fc.utobj().printTestStep("Navigate To CRM > Worflows Page");
			fc.utobj().printTestStep("Create Lead Type Standard Workflow");

			String workFlowName = dataSet.get("workFlowName");
			String triggerType = dataSet.get("triggerType");
			String conditionType = dataSet.get("conditionType");
			String availableFields = dataSet.get("availableFields");
			String taskSubject = dataSet.get("taskSubject");
			String taskPriority = dataSet.get("taskPriority");
			String taskStatus = dataSet.get("taskStatus");
			String matchingCondition = dataSet.get("matchingCondition");

			createWorkFlowLeadStandard(driver, config, dataSet, workFlowName, triggerType, conditionType,
					availableFields, matchingCondition, campaignName, leadStatus, templateNameForSendEmail, taskSubject,
					taskPriority, taskStatus);

			fc.utobj().printTestStep("Verify The Created WorkFlow");
			searchWorkFLowByFilter(driver, workFlowName);

			boolean isWorkFlowNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[.='" + workFlowName + "']");
			if (isWorkFlowNamePresent == false) {
				fc.utobj().throwsException("was not able to verify the workflow");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "crmworkflow")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Create The Standard WorkFlow When Lead is Modified For Leads Matching Criteria BirthDate And Set To It , is Between", testCaseId = "TC_24_Create_Workflow")
	public void createWorkFlow24() throws IOException, Exception, ParserConfigurationException, SAXException {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crmworkflow", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			/*
			 * 
			 * String
			 * userName=fc.utobj().generateTestData(dataSet.get("userName"));
			 * String
			 * password=fc.utobj().generateTestData(dataSet.get("password"));
			 * String emailId="crmautomation@staffex.com";
			 * AdminUsersManageCorporateUsersAddCorporateUserPageTest
			 * addcorporateUserPage=new
			 * AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			 * addcorporateUserPage.addCorporateUser(driver, userName, password,
			 * config , emailId);
			 * 
			 * fc.home_page().logout(driver, config);
			 * fc.utobj().printTestStep(testCaseId, "Login With Corporate User"
			 * ); fc.loginpage().loginWithParameter(driver, userName, password);
			 */

			fc.utobj().printTestStep("Navigate To Admin > CRM > Configure Status Page");
			fc.utobj().printTestStep("Add Lead Status");
			String leadStatus = dataSet.get("leadStatus");
			AdminCRMConfigureStatusPageTest statusPage = new AdminCRMConfigureStatusPageTest();
			statusPage.addStatus(driver, leadStatus);

			fc.utobj().printTestStep("Navigate To CRM > Campaign Center");
			fc.crm().crm_common().CRMModule(driver);

			fc.utobj().printTestStep("Create Campaign For Send Campaign");

			String campaignName = dataSet.get("campaignName");
			String accessibility = dataSet.get("accessibility");
			String campaignType = dataSet.get("campaignType");
			String templateName = dataSet.get("templateName");
			String emailSubject = dataSet.get("emailSubject");
			String plainTextEditor = dataSet.get("plainTextEditor");
			CRMCampaignCenterPageTest campaignCenterPage = new CRMCampaignCenterPageTest();
			campaignCenterPage.createCampaign(driver, config, campaignName, accessibility, campaignType, templateName,
					emailSubject, plainTextEditor);

			fc.utobj().printTestStep("Create Template For Send Email");

			String templateNameForSendEmail = dataSet.get("templateNameForSendEmail");
			String emailSubjectForSendEmail = dataSet.get("emailSubjectForSendEmail");
			String plainTextEditorForSendEmail = dataSet.get("plainTextEditorForSendEmail");

			CRMCampaignCenterEmailTemplatesPageTest emailTemplatePage = new CRMCampaignCenterEmailTemplatesPageTest();
			emailTemplatePage.createTemplate(driver, config, templateNameForSendEmail, emailSubjectForSendEmail,
					plainTextEditorForSendEmail);

			fc.utobj().printTestStep("Navigate To CRM > Worflows Page");
			fc.utobj().printTestStep("Create Lead Type Standard Workflow");

			String workFlowName = dataSet.get("workFlowName");
			String triggerType = dataSet.get("triggerType");
			String conditionType = dataSet.get("conditionType");
			String availableFields = dataSet.get("availableFields");
			String taskSubject = dataSet.get("taskSubject");
			String taskPriority = dataSet.get("taskPriority");
			String taskStatus = dataSet.get("taskStatus");
			String matchingCondition = dataSet.get("matchingCondition");

			createWorkFlowLeadStandard(driver, config, dataSet, workFlowName, triggerType, conditionType,
					availableFields, matchingCondition, campaignName, leadStatus, templateNameForSendEmail, taskSubject,
					taskPriority, taskStatus);

			fc.utobj().printTestStep("Verify The Created WorkFlow");
			searchWorkFLowByFilter(driver, workFlowName);

			boolean isWorkFlowNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[.='" + workFlowName + "']");
			if (isWorkFlowNamePresent == false) {
				fc.utobj().throwsException("was not able to verify the workflow");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "crmworkflow")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Create The Standard WorkFlow When Lead is Modified For Leads Matching Criteria Status Of Lead Is Customized", testCaseId = "TC_25_Create_Workflow")
	public void createWorkFlow25() throws IOException, Exception, ParserConfigurationException, SAXException {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crmworkflow", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			/*
			 * 
			 * String
			 * userName=fc.utobj().generateTestData(dataSet.get("userName"));
			 * String
			 * password=fc.utobj().generateTestData(dataSet.get("password"));
			 * String emailId="crmautomation@staffex.com";
			 * AdminUsersManageCorporateUsersAddCorporateUserPageTest
			 * addcorporateUserPage=new
			 * AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			 * addcorporateUserPage.addCorporateUser(driver, userName, password,
			 * config , emailId);
			 * 
			 * fc.home_page().logout(driver, config);
			 * fc.utobj().printTestStep(testCaseId, "Login With Corporate User"
			 * ); fc.loginpage().loginWithParameter(driver, userName, password);
			 */

			fc.utobj().printTestStep("Navigate To Admin > CRM > Configure Status Page");
			fc.utobj().printTestStep("Add Lead Status");
			String leadStatus = dataSet.get("leadStatus");
			AdminCRMConfigureStatusPageTest statusPage = new AdminCRMConfigureStatusPageTest();
			statusPage.addStatus(driver, leadStatus);

			fc.utobj().printTestStep("Navigate To Admin > CRM > Configure Status Page");
			fc.utobj().printTestStep("Add Lead Status");
			String leadStatusCustomize = dataSet.get("selectLeadStatus");
			statusPage.addStatus(driver, leadStatusCustomize);

			fc.utobj().printTestStep("Navigate To CRM > Campaign Center");
			fc.crm().crm_common().CRMModule(driver);

			fc.utobj().printTestStep("Create Campaign For Send Campaign");

			String campaignName = dataSet.get("campaignName");
			String accessibility = dataSet.get("accessibility");
			String campaignType = dataSet.get("campaignType");
			String templateName = dataSet.get("templateName");
			String emailSubject = dataSet.get("emailSubject");
			String plainTextEditor = dataSet.get("plainTextEditor");
			CRMCampaignCenterPageTest campaignCenterPage = new CRMCampaignCenterPageTest();
			campaignCenterPage.createCampaign(driver, config, campaignName, accessibility, campaignType, templateName,
					emailSubject, plainTextEditor);

			fc.utobj().printTestStep("Create Template For Send Email");

			String templateNameForSendEmail = dataSet.get("templateNameForSendEmail");
			String emailSubjectForSendEmail = dataSet.get("emailSubjectForSendEmail");
			String plainTextEditorForSendEmail = dataSet.get("plainTextEditorForSendEmail");

			CRMCampaignCenterEmailTemplatesPageTest emailTemplatePage = new CRMCampaignCenterEmailTemplatesPageTest();
			emailTemplatePage.createTemplate(driver, config, templateNameForSendEmail, emailSubjectForSendEmail,
					plainTextEditorForSendEmail);

			fc.utobj().printTestStep("Navigate To CRM > Worflows Page");
			fc.utobj().printTestStep("Create Lead Type Standard Workflow");

			String workFlowName = dataSet.get("workFlowName");
			String triggerType = dataSet.get("triggerType");
			String conditionType = dataSet.get("conditionType");
			String availableFields = dataSet.get("availableFields");
			String taskSubject = dataSet.get("taskSubject");
			String taskPriority = dataSet.get("taskPriority");
			String taskStatus = dataSet.get("taskStatus");
			String matchingCondition = dataSet.get("matchingCondition");

			createWorkFlowLeadStandard(driver, config, dataSet, workFlowName, triggerType, conditionType,
					availableFields, matchingCondition, campaignName, leadStatus, templateNameForSendEmail, taskSubject,
					taskPriority, taskStatus);

			fc.utobj().printTestStep("Verify The Created WorkFlow");
			searchWorkFLowByFilter(driver, workFlowName);

			boolean isWorkFlowNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[.='" + workFlowName + "']");
			if (isWorkFlowNamePresent == false) {
				fc.utobj().throwsException("was not able to verify the workflow");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "crmworkflow")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Create The Standard WorkFlow When Contact Is Modified For Contact Matching Criteria Email Field is Not Empty", testCaseId = "TC_26_Create_Workflow")
	public void createWorkFlow26() throws IOException, Exception, ParserConfigurationException, SAXException {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crmworkflow", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			/*
			 * 
			 * String
			 * userName=fc.utobj().generateTestData(dataSet.get("userName"));
			 * String
			 * password=fc.utobj().generateTestData(dataSet.get("password"));
			 * String emailId="crmautomation@staffex.com";
			 * AdminUsersManageCorporateUsersAddCorporateUserPageTest
			 * addcorporateUserPage=new
			 * AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			 * addcorporateUserPage.addCorporateUser(driver, userName, password,
			 * config , emailId);
			 * 
			 * fc.home_page().logout(driver, config);
			 * fc.utobj().printTestStep(testCaseId, "Login With Corporate User"
			 * ); fc.loginpage().loginWithParameter(driver, userName, password);
			 */

			fc.utobj().printTestStep("Navigate To Admin > CRM > Configure Status Page");
			fc.utobj().printTestStep("Add Contact Status");
			String contactStatus = dataSet.get("contactStatus");
			AdminCRMConfigureStatusPageTest statusPage = new AdminCRMConfigureStatusPageTest();
			statusPage.addStatus01(driver, contactStatus);

			fc.utobj().printTestStep("Navigate To CRM > Campaign Center");
			fc.crm().crm_common().CRMModule(driver);

			fc.utobj().printTestStep("Create Campaign For Send Campaign");

			String campaignName = dataSet.get("campaignName");
			String accessibility = dataSet.get("accessibility");
			String campaignType = dataSet.get("campaignType");
			String templateName = dataSet.get("templateName");
			String emailSubject = dataSet.get("emailSubject");
			String plainTextEditor = dataSet.get("plainTextEditor");
			CRMCampaignCenterPageTest campaignCenterPage = new CRMCampaignCenterPageTest();
			campaignCenterPage.createCampaign(driver, config, campaignName, accessibility, campaignType, templateName,
					emailSubject, plainTextEditor);

			fc.utobj().printTestStep("Create Template For Send Email");

			String templateNameForSendEmail = dataSet.get("templateNameForSendEmail");
			String emailSubjectForSendEmail = dataSet.get("emailSubjectForSendEmail");
			String plainTextEditorForSendEmail = dataSet.get("plainTextEditorForSendEmail");

			CRMCampaignCenterEmailTemplatesPageTest emailTemplatePage = new CRMCampaignCenterEmailTemplatesPageTest();
			emailTemplatePage.createTemplate(driver, config, templateNameForSendEmail, emailSubjectForSendEmail,
					plainTextEditorForSendEmail);

			fc.utobj().printTestStep("Navigate To CRM > Worflows Page");
			fc.utobj().printTestStep("Create Contact Type Standard Workflow");

			String workFlowName = dataSet.get("workFlowName");
			String triggerType = dataSet.get("triggerType");
			String conditionType = dataSet.get("conditionType");
			String availableFields = dataSet.get("availableFields");
			String taskSubject = dataSet.get("taskSubject");
			String taskPriority = dataSet.get("taskPriority");
			String taskStatus = dataSet.get("taskStatus");
			String matchingCondition = dataSet.get("matchingCondition");

			createWorkFlowContactStandard(driver, config, dataSet, workFlowName, triggerType, conditionType,
					availableFields, matchingCondition, campaignName, contactStatus, templateNameForSendEmail,
					taskSubject, taskPriority, taskStatus);

			fc.utobj().printTestStep("Verify The Created WorkFlow");
			searchWorkFLowByFilter(driver, workFlowName);

			boolean isWorkFlowNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[.='" + workFlowName + "']");
			if (isWorkFlowNamePresent == false) {
				fc.utobj().throwsException("was not able to verify the workflow");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "crmworkflow")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Create The Standard WorkFlow When Contact Is Modified For Contact Matching Criteria Status Of Contact is Default[New]", testCaseId = "TC_27_Create_Workflow")
	public void createWorkFlow27() throws IOException, Exception, ParserConfigurationException, SAXException {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crmworkflow", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			/*
			 * 
			 * String
			 * userName=fc.utobj().generateTestData(dataSet.get("userName"));
			 * String
			 * password=fc.utobj().generateTestData(dataSet.get("password"));
			 * String emailId="crmautomation@staffex.com";
			 * AdminUsersManageCorporateUsersAddCorporateUserPageTest
			 * addcorporateUserPage=new
			 * AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			 * addcorporateUserPage.addCorporateUser(driver, userName, password,
			 * config , emailId);
			 * 
			 * fc.home_page().logout(driver, config);
			 * fc.utobj().printTestStep(testCaseId, "Login With Corporate User"
			 * ); fc.loginpage().loginWithParameter(driver, userName, password);
			 */

			fc.utobj().printTestStep("Navigate To Admin > CRM > Configure Status Page");
			fc.utobj().printTestStep("Add Contact Status");
			String contactStatus = dataSet.get("contactStatus");
			AdminCRMConfigureStatusPageTest statusPage = new AdminCRMConfigureStatusPageTest();
			statusPage.addStatus01(driver, contactStatus);

			fc.utobj().printTestStep("Navigate To CRM > Campaign Center");
			fc.crm().crm_common().CRMModule(driver);

			fc.utobj().printTestStep("Create Campaign For Send Campaign");

			String campaignName = dataSet.get("campaignName");
			String accessibility = dataSet.get("accessibility");
			String campaignType = dataSet.get("campaignType");
			String templateName = dataSet.get("templateName");
			String emailSubject = dataSet.get("emailSubject");
			String plainTextEditor = dataSet.get("plainTextEditor");
			CRMCampaignCenterPageTest campaignCenterPage = new CRMCampaignCenterPageTest();
			campaignCenterPage.createCampaign(driver, config, campaignName, accessibility, campaignType, templateName,
					emailSubject, plainTextEditor);

			fc.utobj().printTestStep("Create Template For Send Email");

			String templateNameForSendEmail = dataSet.get("templateNameForSendEmail");
			String emailSubjectForSendEmail = dataSet.get("emailSubjectForSendEmail");
			String plainTextEditorForSendEmail = dataSet.get("plainTextEditorForSendEmail");

			CRMCampaignCenterEmailTemplatesPageTest emailTemplatePage = new CRMCampaignCenterEmailTemplatesPageTest();
			emailTemplatePage.createTemplate(driver, config, templateNameForSendEmail, emailSubjectForSendEmail,
					plainTextEditorForSendEmail);

			fc.utobj().printTestStep("Navigate To CRM > Worflows Page");
			fc.utobj().printTestStep("Create Contact Type Standard Workflow");

			String workFlowName = dataSet.get("workFlowName");
			String triggerType = dataSet.get("triggerType");
			String conditionType = dataSet.get("conditionType");
			String availableFields = dataSet.get("availableFields");
			String taskSubject = dataSet.get("taskSubject");
			String taskPriority = dataSet.get("taskPriority");
			String taskStatus = dataSet.get("taskStatus");
			String matchingCondition = dataSet.get("matchingCondition");

			createWorkFlowContactStandard(driver, config, dataSet, workFlowName, triggerType, conditionType,
					availableFields, matchingCondition, campaignName, contactStatus, templateNameForSendEmail,
					taskSubject, taskPriority, taskStatus);

			fc.utobj().printTestStep("Verify The Created WorkFlow");
			searchWorkFLowByFilter(driver, workFlowName);

			boolean isWorkFlowNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[.='" + workFlowName + "']");
			if (isWorkFlowNamePresent == false) {
				fc.utobj().throwsException("was not able to verify the workflow");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "crmworkflow")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Create The Standard WorkFlow When Contact Is Modified For Contact Matching Criteria Email Subscription status is Set To Opted In", testCaseId = "TC_28_Create_Workflow")
	public void createWorkFlow28() throws IOException, Exception, ParserConfigurationException, SAXException {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crmworkflow", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			/*
			 * 
			 * String
			 * userName=fc.utobj().generateTestData(dataSet.get("userName"));
			 * String
			 * password=fc.utobj().generateTestData(dataSet.get("password"));
			 * String emailId="crmautomation@staffex.com";
			 * AdminUsersManageCorporateUsersAddCorporateUserPageTest
			 * addcorporateUserPage=new
			 * AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			 * addcorporateUserPage.addCorporateUser(driver, userName, password,
			 * config , emailId);
			 * 
			 * fc.home_page().logout(driver, config);
			 * fc.utobj().printTestStep(testCaseId, "Login With Corporate User"
			 * ); fc.loginpage().loginWithParameter(driver, userName, password);
			 */

			fc.utobj().printTestStep("Navigate To Admin > CRM > Configure Status Page");
			fc.utobj().printTestStep("Add Contact Status");
			String contactStatus = dataSet.get("contactStatus");
			AdminCRMConfigureStatusPageTest statusPage = new AdminCRMConfigureStatusPageTest();
			statusPage.addStatus01(driver, contactStatus);

			fc.utobj().printTestStep("Navigate To CRM > Campaign Center");
			fc.crm().crm_common().CRMModule(driver);

			fc.utobj().printTestStep("Create Campaign For Send Campaign");

			String campaignName = dataSet.get("campaignName");
			String accessibility = dataSet.get("accessibility");
			String campaignType = dataSet.get("campaignType");
			String templateName = dataSet.get("templateName");
			String emailSubject = dataSet.get("emailSubject");
			String plainTextEditor = dataSet.get("plainTextEditor");
			CRMCampaignCenterPageTest campaignCenterPage = new CRMCampaignCenterPageTest();
			campaignCenterPage.createCampaign(driver, config, campaignName, accessibility, campaignType, templateName,
					emailSubject, plainTextEditor);

			fc.utobj().printTestStep("Create Template For Send Email");

			String templateNameForSendEmail = dataSet.get("templateNameForSendEmail");
			String emailSubjectForSendEmail = dataSet.get("emailSubjectForSendEmail");
			String plainTextEditorForSendEmail = dataSet.get("plainTextEditorForSendEmail");

			CRMCampaignCenterEmailTemplatesPageTest emailTemplatePage = new CRMCampaignCenterEmailTemplatesPageTest();
			emailTemplatePage.createTemplate(driver, config, templateNameForSendEmail, emailSubjectForSendEmail,
					plainTextEditorForSendEmail);

			fc.utobj().printTestStep("Navigate To CRM > Worflows Page");
			fc.utobj().printTestStep("Create Contact Type Standard Workflow");

			String workFlowName = dataSet.get("workFlowName");
			String triggerType = dataSet.get("triggerType");
			String conditionType = dataSet.get("conditionType");
			String availableFields = dataSet.get("availableFields");
			String taskSubject = dataSet.get("taskSubject");
			String taskPriority = dataSet.get("taskPriority");
			String taskStatus = dataSet.get("taskStatus");
			String matchingCondition = dataSet.get("matchingCondition");

			createWorkFlowContactStandard(driver, config, dataSet, workFlowName, triggerType, conditionType,
					availableFields, matchingCondition, campaignName, contactStatus, templateNameForSendEmail,
					taskSubject, taskPriority, taskStatus);

			fc.utobj().printTestStep("Verify The Created WorkFlow");
			searchWorkFLowByFilter(driver, workFlowName);

			boolean isWorkFlowNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[.='" + workFlowName + "']");
			if (isWorkFlowNamePresent == false) {
				fc.utobj().throwsException("was not able to verify the workflow");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "crmworkflow")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Create The Standard WorkFlow When Contact Is Modified For Contact Matching Criteria Country And All States of It", testCaseId = "TC_29_Create_Workflow")
	public void createWorkFlow29() throws IOException, Exception, ParserConfigurationException, SAXException {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crmworkflow", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			/*
			 * 
			 * String
			 * userName=fc.utobj().generateTestData(dataSet.get("userName"));
			 * String
			 * password=fc.utobj().generateTestData(dataSet.get("password"));
			 * String emailId="crmautomation@staffex.com";
			 * AdminUsersManageCorporateUsersAddCorporateUserPageTest
			 * addcorporateUserPage=new
			 * AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			 * addcorporateUserPage.addCorporateUser(driver, userName, password,
			 * config , emailId);
			 * 
			 * fc.home_page().logout(driver, config);
			 * fc.utobj().printTestStep(testCaseId, "Login With Corporate User"
			 * ); fc.loginpage().loginWithParameter(driver, userName, password);
			 */

			fc.utobj().printTestStep("Navigate To Admin > CRM > Configure Status Page");
			fc.utobj().printTestStep("Add Contact Status");
			String contactStatus = dataSet.get("contactStatus");
			AdminCRMConfigureStatusPageTest statusPage = new AdminCRMConfigureStatusPageTest();
			statusPage.addStatus01(driver, contactStatus);

			fc.utobj().printTestStep("Navigate To CRM > Campaign Center");
			fc.crm().crm_common().CRMModule(driver);

			fc.utobj().printTestStep("Create Campaign For Send Campaign");

			String campaignName = dataSet.get("campaignName");
			String accessibility = dataSet.get("accessibility");
			String campaignType = dataSet.get("campaignType");
			String templateName = dataSet.get("templateName");
			String emailSubject = dataSet.get("emailSubject");
			String plainTextEditor = dataSet.get("plainTextEditor");
			CRMCampaignCenterPageTest campaignCenterPage = new CRMCampaignCenterPageTest();
			campaignCenterPage.createCampaign(driver, config, campaignName, accessibility, campaignType, templateName,
					emailSubject, plainTextEditor);

			fc.utobj().printTestStep("Create Template For Send Email");

			String templateNameForSendEmail = dataSet.get("templateNameForSendEmail");
			String emailSubjectForSendEmail = dataSet.get("emailSubjectForSendEmail");
			String plainTextEditorForSendEmail = dataSet.get("plainTextEditorForSendEmail");

			CRMCampaignCenterEmailTemplatesPageTest emailTemplatePage = new CRMCampaignCenterEmailTemplatesPageTest();
			emailTemplatePage.createTemplate(driver, config, templateNameForSendEmail, emailSubjectForSendEmail,
					plainTextEditorForSendEmail);

			fc.utobj().printTestStep("Navigate To CRM > Worflows Page");
			fc.utobj().printTestStep("Create Contact Type Standard Workflow");

			String workFlowName = dataSet.get("workFlowName");
			String triggerType = dataSet.get("triggerType");
			String conditionType = dataSet.get("conditionType");
			String availableFields = dataSet.get("availableFields");
			String taskSubject = dataSet.get("taskSubject");
			String taskPriority = dataSet.get("taskPriority");
			String taskStatus = dataSet.get("taskStatus");
			String matchingCondition = dataSet.get("matchingCondition");

			createWorkFlowContactStandard(driver, config, dataSet, workFlowName, triggerType, conditionType,
					availableFields, matchingCondition, campaignName, contactStatus, templateNameForSendEmail,
					taskSubject, taskPriority, taskStatus);

			fc.utobj().printTestStep("Verify The Created WorkFlow");
			searchWorkFLowByFilter(driver, workFlowName);

			boolean isWorkFlowNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[.='" + workFlowName + "']");
			if (isWorkFlowNamePresent == false) {
				fc.utobj().throwsException("was not able to verify the workflow");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "crmworkflow")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Create The Standard WorkFlow When Contact Is Modified For Contact Matching Criteria BirthDate And Set To It , is Between", testCaseId = "TC_30_Create_Workflow")
	public void createWorkFlow30() throws IOException, Exception, ParserConfigurationException, SAXException {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crmworkflow", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			/*
			 * 
			 * String
			 * userName=fc.utobj().generateTestData(dataSet.get("userName"));
			 * String
			 * password=fc.utobj().generateTestData(dataSet.get("password"));
			 * String emailId="crmautomation@staffex.com";
			 * AdminUsersManageCorporateUsersAddCorporateUserPageTest
			 * addcorporateUserPage=new
			 * AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			 * addcorporateUserPage.addCorporateUser(driver, userName, password,
			 * config , emailId);
			 * 
			 * fc.home_page().logout(driver, config);
			 * fc.utobj().printTestStep(testCaseId, "Login With Corporate User"
			 * ); fc.loginpage().loginWithParameter(driver, userName, password);
			 */

			fc.utobj().printTestStep("Navigate To Admin > CRM > Configure Status Page");
			fc.utobj().printTestStep("Add Contact Status");
			String contactStatus = dataSet.get("contactStatus");
			AdminCRMConfigureStatusPageTest statusPage = new AdminCRMConfigureStatusPageTest();
			statusPage.addStatus01(driver, contactStatus);

			fc.utobj().printTestStep("Navigate To CRM > Campaign Center");
			fc.crm().crm_common().CRMModule(driver);

			fc.utobj().printTestStep("Create Campaign For Send Campaign");

			String campaignName = dataSet.get("campaignName");
			String accessibility = dataSet.get("accessibility");
			String campaignType = dataSet.get("campaignType");
			String templateName = dataSet.get("templateName");
			String emailSubject = dataSet.get("emailSubject");
			String plainTextEditor = dataSet.get("plainTextEditor");
			CRMCampaignCenterPageTest campaignCenterPage = new CRMCampaignCenterPageTest();
			campaignCenterPage.createCampaign(driver, config, campaignName, accessibility, campaignType, templateName,
					emailSubject, plainTextEditor);

			fc.utobj().printTestStep("Create Template For Send Email");

			String templateNameForSendEmail = dataSet.get("templateNameForSendEmail");
			String emailSubjectForSendEmail = dataSet.get("emailSubjectForSendEmail");
			String plainTextEditorForSendEmail = dataSet.get("plainTextEditorForSendEmail");

			CRMCampaignCenterEmailTemplatesPageTest emailTemplatePage = new CRMCampaignCenterEmailTemplatesPageTest();
			emailTemplatePage.createTemplate(driver, config, templateNameForSendEmail, emailSubjectForSendEmail,
					plainTextEditorForSendEmail);

			fc.utobj().printTestStep("Navigate To CRM > Worflows Page");
			fc.utobj().printTestStep("Create Contact Type Standard Workflow");

			String workFlowName = dataSet.get("workFlowName");
			String triggerType = dataSet.get("triggerType");
			String conditionType = dataSet.get("conditionType");
			String availableFields = dataSet.get("availableFields");
			String taskSubject = dataSet.get("taskSubject");
			String taskPriority = dataSet.get("taskPriority");
			String taskStatus = dataSet.get("taskStatus");
			String matchingCondition = dataSet.get("matchingCondition");

			createWorkFlowContactStandard(driver, config, dataSet, workFlowName, triggerType, conditionType,
					availableFields, matchingCondition, campaignName, contactStatus, templateNameForSendEmail,
					taskSubject, taskPriority, taskStatus);

			fc.utobj().printTestStep("Verify The Created WorkFlow");
			searchWorkFLowByFilter(driver, workFlowName);

			boolean isWorkFlowNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[.='" + workFlowName + "']");
			if (isWorkFlowNamePresent == false) {
				fc.utobj().throwsException("was not able to verify the workflow");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "crmworkflow")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Create The Standard WorkFlow When Contact Is Modified For Contact Matching Criteria Status Of Contact Is Customized", testCaseId = "TC_31_Create_Workflow")
	public void createWorkFlow31() throws IOException, Exception, ParserConfigurationException, SAXException {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crmworkflow", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			/*
			 * 
			 * String
			 * userName=fc.utobj().generateTestData(dataSet.get("userName"));
			 * String
			 * password=fc.utobj().generateTestData(dataSet.get("password"));
			 * String emailId="crmautomation@staffex.com";
			 * AdminUsersManageCorporateUsersAddCorporateUserPageTest
			 * addcorporateUserPage=new
			 * AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			 * addcorporateUserPage.addCorporateUser(driver, userName, password,
			 * config , emailId);
			 * 
			 * fc.home_page().logout(driver, config);
			 * fc.utobj().printTestStep(testCaseId, "Login With Corporate User"
			 * ); fc.loginpage().loginWithParameter(driver, userName, password);
			 */

			fc.utobj().printTestStep("Navigate To Admin > CRM > Configure Status Page");
			fc.utobj().printTestStep("Add Contact Status");
			String contactStatus = dataSet.get("contactStatus");
			AdminCRMConfigureStatusPageTest statusPage = new AdminCRMConfigureStatusPageTest();
			statusPage.addStatus01(driver, contactStatus);

			fc.utobj().printTestStep("Navigate To Admin > CRM > Configure Status Page");
			fc.utobj().printTestStep("Add Contact Status");
			String contactStatusCustomize = dataSet.get("selectContactStatus");
			statusPage.addStatus01(driver, contactStatusCustomize);

			fc.utobj().printTestStep("Navigate To CRM > Campaign Center");
			fc.crm().crm_common().CRMModule(driver);

			fc.utobj().printTestStep("Create Campaign For Send Campaign");

			String campaignName = dataSet.get("campaignName");
			String accessibility = dataSet.get("accessibility");
			String campaignType = dataSet.get("campaignType");
			String templateName = dataSet.get("templateName");
			String emailSubject = dataSet.get("emailSubject");
			String plainTextEditor = dataSet.get("plainTextEditor");
			CRMCampaignCenterPageTest campaignCenterPage = new CRMCampaignCenterPageTest();
			campaignCenterPage.createCampaign(driver, config, campaignName, accessibility, campaignType, templateName,
					emailSubject, plainTextEditor);

			fc.utobj().printTestStep("Create Template For Send Email");

			String templateNameForSendEmail = dataSet.get("templateNameForSendEmail");
			String emailSubjectForSendEmail = dataSet.get("emailSubjectForSendEmail");
			String plainTextEditorForSendEmail = dataSet.get("plainTextEditorForSendEmail");

			CRMCampaignCenterEmailTemplatesPageTest emailTemplatePage = new CRMCampaignCenterEmailTemplatesPageTest();
			emailTemplatePage.createTemplate(driver, config, templateNameForSendEmail, emailSubjectForSendEmail,
					plainTextEditorForSendEmail);

			fc.utobj().printTestStep("Navigate To CRM > Worflows Page");
			fc.utobj().printTestStep("Create Contact Type Standard Workflow");

			String workFlowName = dataSet.get("workFlowName");
			String triggerType = dataSet.get("triggerType");
			String conditionType = dataSet.get("conditionType");
			String availableFields = dataSet.get("availableFields");
			String taskSubject = dataSet.get("taskSubject");
			String taskPriority = dataSet.get("taskPriority");
			String taskStatus = dataSet.get("taskStatus");
			String matchingCondition = dataSet.get("matchingCondition");

			createWorkFlowContactStandard(driver, config, dataSet, workFlowName, triggerType, conditionType,
					availableFields, matchingCondition, campaignName, contactStatus, templateNameForSendEmail,
					taskSubject, taskPriority, taskStatus);

			fc.utobj().printTestStep("Verify The Created WorkFlow");
			searchWorkFLowByFilter(driver, workFlowName);

			boolean isWorkFlowNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[.='" + workFlowName + "']");
			if (isWorkFlowNamePresent == false) {
				fc.utobj().throwsException("was not able to verify the workflow");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "crmworkflow")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Create The Standard WorkFlow When Contact Is Modified For Contact Matching Criteria Contact Type In", testCaseId = "TC_32_Create_Workflow")
	public void createWorkFlow32() throws IOException, Exception, ParserConfigurationException, SAXException {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crmworkflow", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			/*
			 * 
			 * String
			 * userName=fc.utobj().generateTestData(dataSet.get("userName"));
			 * String
			 * password=fc.utobj().generateTestData(dataSet.get("password"));
			 * String emailId="crmautomation@staffex.com";
			 * AdminUsersManageCorporateUsersAddCorporateUserPageTest
			 * addcorporateUserPage=new
			 * AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			 * addcorporateUserPage.addCorporateUser(driver, userName, password,
			 * config , emailId);
			 * 
			 * fc.home_page().logout(driver, config);
			 * fc.utobj().printTestStep(testCaseId, "Login With Corporate User"
			 * ); fc.loginpage().loginWithParameter(driver, userName, password);
			 */

			fc.utobj().printTestStep("Navigate To Admin > CRM > Configure Status Page");
			fc.utobj().printTestStep("Add Contact Status");
			String contactStatus = dataSet.get("contactStatus");
			AdminCRMConfigureStatusPageTest statusPage = new AdminCRMConfigureStatusPageTest();
			statusPage.addStatus01(driver, contactStatus);

			fc.utobj().printTestStep("Navigate To Admin > CRM > Contact Type Configureation");
			fc.utobj().printTestStep("Add Contact Type");
			String contactType = dataSet.get("selectcontactType");
			AdminCRMContactTypeConfigurationPageTest contactTypeConfigureationPage = new AdminCRMContactTypeConfigurationPageTest();
			contactTypeConfigureationPage.addContactType(driver, contactType);

			fc.utobj().printTestStep("Navigate To CRM > Campaign Center");
			fc.crm().crm_common().CRMModule(driver);

			fc.utobj().printTestStep("Create Campaign For Send Campaign");

			String campaignName = dataSet.get("campaignName");
			String accessibility = dataSet.get("accessibility");
			String campaignType = dataSet.get("campaignType");
			String templateName = dataSet.get("templateName");
			String emailSubject = dataSet.get("emailSubject");
			String plainTextEditor = dataSet.get("plainTextEditor");
			CRMCampaignCenterPageTest campaignCenterPage = new CRMCampaignCenterPageTest();
			campaignCenterPage.createCampaign(driver, config, campaignName, accessibility, campaignType, templateName,
					emailSubject, plainTextEditor);

			fc.utobj().printTestStep("Create Template For Send Email");

			String templateNameForSendEmail = dataSet.get("templateNameForSendEmail");
			String emailSubjectForSendEmail = dataSet.get("emailSubjectForSendEmail");
			String plainTextEditorForSendEmail = dataSet.get("plainTextEditorForSendEmail");

			CRMCampaignCenterEmailTemplatesPageTest emailTemplatePage = new CRMCampaignCenterEmailTemplatesPageTest();
			emailTemplatePage.createTemplate(driver, config, templateNameForSendEmail, emailSubjectForSendEmail,
					plainTextEditorForSendEmail);

			fc.utobj().printTestStep("Navigate To CRM > Worflows Page");
			fc.utobj().printTestStep("Create Contact Type Standard Workflow");

			String workFlowName = dataSet.get("workFlowName");
			String triggerType = dataSet.get("triggerType");
			String conditionType = dataSet.get("conditionType");
			String availableFields = dataSet.get("availableFields");
			String taskSubject = dataSet.get("taskSubject");
			String taskPriority = dataSet.get("taskPriority");
			String taskStatus = dataSet.get("taskStatus");
			String matchingCondition = dataSet.get("matchingCondition");

			createWorkFlowContactStandard(driver, config, dataSet, workFlowName, triggerType, conditionType,
					availableFields, matchingCondition, campaignName, contactStatus, templateNameForSendEmail,
					taskSubject, taskPriority, taskStatus);

			fc.utobj().printTestStep("Verify The Created WorkFlow");
			searchWorkFLowByFilter(driver, workFlowName);

			boolean isWorkFlowNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[.='" + workFlowName + "']");
			if (isWorkFlowNamePresent == false) {
				fc.utobj().throwsException("was not able to verify the workflow");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "crmworkflow")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Create The Standard WorkFlow When Lead Is Added Or Modified For Leads Matching Criteria Email Field is Not Empty", testCaseId = "TC_33_Create_Workflow")
	public void createWorkFlow33() throws IOException, Exception, ParserConfigurationException, SAXException {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crmworkflow", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			/*
			 * 
			 * String
			 * userName=fc.utobj().generateTestData(dataSet.get("userName"));
			 * String
			 * password=fc.utobj().generateTestData(dataSet.get("password"));
			 * String emailId="crmautomation@staffex.com";
			 * AdminUsersManageCorporateUsersAddCorporateUserPageTest
			 * addcorporateUserPage=new
			 * AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			 * addcorporateUserPage.addCorporateUser(driver, userName, password,
			 * config , emailId);
			 * 
			 * fc.home_page().logout(driver, config);
			 * fc.utobj().printTestStep(testCaseId, "Login With Corporate User"
			 * ); fc.loginpage().loginWithParameter(driver, userName, password);
			 */

			fc.utobj().printTestStep("Navigate To Admin > CRM > Configure Status Page");
			fc.utobj().printTestStep("Add Lead Status");
			String leadStatus = dataSet.get("leadStatus");
			AdminCRMConfigureStatusPageTest statusPage = new AdminCRMConfigureStatusPageTest();
			statusPage.addStatus(driver, leadStatus);

			fc.utobj().printTestStep("Navigate To CRM > Campaign Center");
			fc.crm().crm_common().CRMModule(driver);

			fc.utobj().printTestStep("Create Campaign For Send Campaign");

			String campaignName = dataSet.get("campaignName");
			String accessibility = dataSet.get("accessibility");
			String campaignType = dataSet.get("campaignType");
			String templateName = dataSet.get("templateName");
			String emailSubject = dataSet.get("emailSubject");
			String plainTextEditor = dataSet.get("plainTextEditor");
			CRMCampaignCenterPageTest campaignCenterPage = new CRMCampaignCenterPageTest();
			campaignCenterPage.createCampaign(driver, config, campaignName, accessibility, campaignType, templateName,
					emailSubject, plainTextEditor);

			fc.utobj().printTestStep("Create Template For Send Email");

			String templateNameForSendEmail = dataSet.get("templateNameForSendEmail");
			String emailSubjectForSendEmail = dataSet.get("emailSubjectForSendEmail");
			String plainTextEditorForSendEmail = dataSet.get("plainTextEditorForSendEmail");

			CRMCampaignCenterEmailTemplatesPageTest emailTemplatePage = new CRMCampaignCenterEmailTemplatesPageTest();
			emailTemplatePage.createTemplate(driver, config, templateNameForSendEmail, emailSubjectForSendEmail,
					plainTextEditorForSendEmail);

			fc.utobj().printTestStep("Navigate To CRM > Worflows Page");
			fc.utobj().printTestStep("Create Lead Type Standard Workflow");

			String workFlowName = dataSet.get("workFlowName");
			String triggerType = dataSet.get("triggerType");
			String conditionType = dataSet.get("conditionType");
			String availableFields = dataSet.get("availableFields");
			String taskSubject = dataSet.get("taskSubject");
			String taskPriority = dataSet.get("taskPriority");
			String taskStatus = dataSet.get("taskStatus");
			String matchingCondition = dataSet.get("matchingCondition");

			createWorkFlowLeadStandard(driver, config, dataSet, workFlowName, triggerType, conditionType,
					availableFields, matchingCondition, campaignName, leadStatus, templateNameForSendEmail, taskSubject,
					taskPriority, taskStatus);

			fc.utobj().printTestStep("Verify The Created WorkFlow");
			searchWorkFLowByFilter(driver, workFlowName);

			boolean isWorkFlowNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[.='" + workFlowName + "']");
			if (isWorkFlowNamePresent == false) {
				fc.utobj().throwsException("was not able to verify the workflow");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "crmworkflow")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Create The Standard WorkFlow When Lead is Added Or Modifies For Leads Matching Criteria Status OF Lead Is Default[New]", testCaseId = "TC_34_Create_Workflow")
	public void createWorkFlow34() throws IOException, Exception, ParserConfigurationException, SAXException {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crmworkflow", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			/*
			 * 
			 * String
			 * userName=fc.utobj().generateTestData(dataSet.get("userName"));
			 * String
			 * password=fc.utobj().generateTestData(dataSet.get("password"));
			 * String emailId="crmautomation@staffex.com";
			 * AdminUsersManageCorporateUsersAddCorporateUserPageTest
			 * addcorporateUserPage=new
			 * AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			 * addcorporateUserPage.addCorporateUser(driver, userName, password,
			 * config , emailId);
			 * 
			 * fc.home_page().logout(driver, config);
			 * fc.utobj().printTestStep(testCaseId, "Login With Corporate User"
			 * ); fc.loginpage().loginWithParameter(driver, userName, password);
			 */

			fc.utobj().printTestStep("Navigate To Admin > CRM > Configure Status Page");
			fc.utobj().printTestStep("Add Lead Status");
			String leadStatus = dataSet.get("leadStatus");
			AdminCRMConfigureStatusPageTest statusPage = new AdminCRMConfigureStatusPageTest();
			statusPage.addStatus(driver, leadStatus);

			fc.utobj().printTestStep("Navigate To CRM > Campaign Center");
			fc.crm().crm_common().CRMModule(driver);

			fc.utobj().printTestStep("Create Campaign For Send Campaign");

			String campaignName = dataSet.get("campaignName");
			String accessibility = dataSet.get("accessibility");
			String campaignType = dataSet.get("campaignType");
			String templateName = dataSet.get("templateName");
			String emailSubject = dataSet.get("emailSubject");
			String plainTextEditor = dataSet.get("plainTextEditor");
			CRMCampaignCenterPageTest campaignCenterPage = new CRMCampaignCenterPageTest();
			campaignCenterPage.createCampaign(driver, config, campaignName, accessibility, campaignType, templateName,
					emailSubject, plainTextEditor);

			fc.utobj().printTestStep("Create Template For Send Email");

			String templateNameForSendEmail = dataSet.get("templateNameForSendEmail");
			String emailSubjectForSendEmail = dataSet.get("emailSubjectForSendEmail");
			String plainTextEditorForSendEmail = dataSet.get("plainTextEditorForSendEmail");

			CRMCampaignCenterEmailTemplatesPageTest emailTemplatePage = new CRMCampaignCenterEmailTemplatesPageTest();
			emailTemplatePage.createTemplate(driver, config, templateNameForSendEmail, emailSubjectForSendEmail,
					plainTextEditorForSendEmail);

			fc.utobj().printTestStep("Navigate To CRM > Worflows Page");
			fc.utobj().printTestStep("Create Lead Type Standard Workflow");

			String workFlowName = dataSet.get("workFlowName");
			String triggerType = dataSet.get("triggerType");
			String conditionType = dataSet.get("conditionType");
			String availableFields = dataSet.get("availableFields");
			String taskSubject = dataSet.get("taskSubject");
			String taskPriority = dataSet.get("taskPriority");
			String taskStatus = dataSet.get("taskStatus");
			String matchingCondition = dataSet.get("matchingCondition");

			createWorkFlowLeadStandard(driver, config, dataSet, workFlowName, triggerType, conditionType,
					availableFields, matchingCondition, campaignName, leadStatus, templateNameForSendEmail, taskSubject,
					taskPriority, taskStatus);

			fc.utobj().printTestStep("Verify The Created WorkFlow");
			searchWorkFLowByFilter(driver, workFlowName);

			boolean isWorkFlowNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[.='" + workFlowName + "']");
			if (isWorkFlowNamePresent == false) {
				fc.utobj().throwsException("was not able to verify the workflow");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "crmworkflow")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Create The Standard WorkFlow When Lead is Added Or Modified For Leads Matching Criteria Email Subscription status is Set To Opted In", testCaseId = "TC_35_Create_Workflow")
	public void createWorkFlow35() throws IOException, Exception, ParserConfigurationException, SAXException {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crmworkflow", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			/*
			 * 
			 * String
			 * userName=fc.utobj().generateTestData(dataSet.get("userName"));
			 * String
			 * password=fc.utobj().generateTestData(dataSet.get("password"));
			 * String emailId="crmautomation@staffex.com";
			 * AdminUsersManageCorporateUsersAddCorporateUserPageTest
			 * addcorporateUserPage=new
			 * AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			 * addcorporateUserPage.addCorporateUser(driver, userName, password,
			 * config , emailId);
			 * 
			 * fc.home_page().logout(driver, config);
			 * fc.utobj().printTestStep(testCaseId, "Login With Corporate User"
			 * ); fc.loginpage().loginWithParameter(driver, userName, password);
			 */

			fc.utobj().printTestStep("Navigate To Admin > CRM > Configure Status Page");
			fc.utobj().printTestStep("Add Lead Status");
			String leadStatus = dataSet.get("leadStatus");
			AdminCRMConfigureStatusPageTest statusPage = new AdminCRMConfigureStatusPageTest();
			statusPage.addStatus(driver, leadStatus);

			fc.utobj().printTestStep("Navigate To CRM > Campaign Center");
			fc.crm().crm_common().CRMModule(driver);

			fc.utobj().printTestStep("Create Campaign For Send Campaign");

			String campaignName = dataSet.get("campaignName");
			String accessibility = dataSet.get("accessibility");
			String campaignType = dataSet.get("campaignType");
			String templateName = dataSet.get("templateName");
			String emailSubject = dataSet.get("emailSubject");
			String plainTextEditor = dataSet.get("plainTextEditor");
			CRMCampaignCenterPageTest campaignCenterPage = new CRMCampaignCenterPageTest();
			campaignCenterPage.createCampaign(driver, config, campaignName, accessibility, campaignType, templateName,
					emailSubject, plainTextEditor);

			fc.utobj().printTestStep("Create Template For Send Email");

			String templateNameForSendEmail = dataSet.get("templateNameForSendEmail");
			String emailSubjectForSendEmail = dataSet.get("emailSubjectForSendEmail");
			String plainTextEditorForSendEmail = dataSet.get("plainTextEditorForSendEmail");

			CRMCampaignCenterEmailTemplatesPageTest emailTemplatePage = new CRMCampaignCenterEmailTemplatesPageTest();
			emailTemplatePage.createTemplate(driver, config, templateNameForSendEmail, emailSubjectForSendEmail,
					plainTextEditorForSendEmail);

			fc.utobj().printTestStep("Navigate To CRM > Worflows Page");
			fc.utobj().printTestStep("Create Lead Type Standard Workflow");

			String workFlowName = dataSet.get("workFlowName");
			String triggerType = dataSet.get("triggerType");
			String conditionType = dataSet.get("conditionType");
			String availableFields = dataSet.get("availableFields");
			String taskSubject = dataSet.get("taskSubject");
			String taskPriority = dataSet.get("taskPriority");
			String taskStatus = dataSet.get("taskStatus");
			String matchingCondition = dataSet.get("matchingCondition");

			createWorkFlowLeadStandard(driver, config, dataSet, workFlowName, triggerType, conditionType,
					availableFields, matchingCondition, campaignName, leadStatus, templateNameForSendEmail, taskSubject,
					taskPriority, taskStatus);

			fc.utobj().printTestStep("Verify The Created WorkFlow");
			searchWorkFLowByFilter(driver, workFlowName);

			boolean isWorkFlowNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[.='" + workFlowName + "']");
			if (isWorkFlowNamePresent == false) {
				fc.utobj().throwsException("was not able to verify the workflow");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "crmworkflow")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Create The Standard WorkFlow When Lead is Added Or Modified For Leads Matching Criteria Country And All States of It", testCaseId = "TC_36_Create_Workflow")
	public void createWorkFlow36() throws IOException, Exception, ParserConfigurationException, SAXException {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crmworkflow", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			/*
			 * 
			 * String
			 * userName=fc.utobj().generateTestData(dataSet.get("userName"));
			 * String
			 * password=fc.utobj().generateTestData(dataSet.get("password"));
			 * String emailId="crmautomation@staffex.com";
			 * AdminUsersManageCorporateUsersAddCorporateUserPageTest
			 * addcorporateUserPage=new
			 * AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			 * addcorporateUserPage.addCorporateUser(driver, userName, password,
			 * config , emailId);
			 * 
			 * fc.home_page().logout(driver, config);
			 * fc.utobj().printTestStep(testCaseId, "Login With Corporate User"
			 * ); fc.loginpage().loginWithParameter(driver, userName, password);
			 */

			fc.utobj().printTestStep("Navigate To Admin > CRM > Configure Status Page");
			fc.utobj().printTestStep("Add Lead Status");
			String leadStatus = dataSet.get("leadStatus");
			AdminCRMConfigureStatusPageTest statusPage = new AdminCRMConfigureStatusPageTest();
			statusPage.addStatus(driver, leadStatus);

			fc.utobj().printTestStep("Navigate To CRM > Campaign Center");
			fc.crm().crm_common().CRMModule(driver);

			fc.utobj().printTestStep("Create Campaign For Send Campaign");

			String campaignName = dataSet.get("campaignName");
			String accessibility = dataSet.get("accessibility");
			String campaignType = dataSet.get("campaignType");
			String templateName = dataSet.get("templateName");
			String emailSubject = dataSet.get("emailSubject");
			String plainTextEditor = dataSet.get("plainTextEditor");
			CRMCampaignCenterPageTest campaignCenterPage = new CRMCampaignCenterPageTest();
			campaignCenterPage.createCampaign(driver, config, campaignName, accessibility, campaignType, templateName,
					emailSubject, plainTextEditor);

			fc.utobj().printTestStep("Create Template For Send Email");

			String templateNameForSendEmail = dataSet.get("templateNameForSendEmail");
			String emailSubjectForSendEmail = dataSet.get("emailSubjectForSendEmail");
			String plainTextEditorForSendEmail = dataSet.get("plainTextEditorForSendEmail");

			CRMCampaignCenterEmailTemplatesPageTest emailTemplatePage = new CRMCampaignCenterEmailTemplatesPageTest();
			emailTemplatePage.createTemplate(driver, config, templateNameForSendEmail, emailSubjectForSendEmail,
					plainTextEditorForSendEmail);

			fc.utobj().printTestStep("Navigate To CRM > Worflows Page");
			fc.utobj().printTestStep("Create Lead Type Standard Workflow");

			String workFlowName = dataSet.get("workFlowName");
			String triggerType = dataSet.get("triggerType");
			String conditionType = dataSet.get("conditionType");
			String availableFields = dataSet.get("availableFields");
			String taskSubject = dataSet.get("taskSubject");
			String taskPriority = dataSet.get("taskPriority");
			String taskStatus = dataSet.get("taskStatus");
			String matchingCondition = dataSet.get("matchingCondition");

			createWorkFlowLeadStandard(driver, config, dataSet, workFlowName, triggerType, conditionType,
					availableFields, matchingCondition, campaignName, leadStatus, templateNameForSendEmail, taskSubject,
					taskPriority, taskStatus);

			fc.utobj().printTestStep("Verify The Created WorkFlow");
			searchWorkFLowByFilter(driver, workFlowName);

			boolean isWorkFlowNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[.='" + workFlowName + "']");
			if (isWorkFlowNamePresent == false) {
				fc.utobj().throwsException("was not able to verify the workflow");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "crmworkflow")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Create The Standard WorkFlow When Lead is Added Or Modified For Leads Matching Criteria BirthDate And Set To It , is Between", testCaseId = "TC_37_Create_Workflow")
	public void createWorkFlow37() throws IOException, Exception, ParserConfigurationException, SAXException {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crmworkflow", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			/*
			 * 
			 * String
			 * userName=fc.utobj().generateTestData(dataSet.get("userName"));
			 * String
			 * password=fc.utobj().generateTestData(dataSet.get("password"));
			 * String emailId="crmautomation@staffex.com";
			 * AdminUsersManageCorporateUsersAddCorporateUserPageTest
			 * addcorporateUserPage=new
			 * AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			 * addcorporateUserPage.addCorporateUser(driver, userName, password,
			 * config , emailId);
			 * 
			 * fc.home_page().logout(driver, config);
			 * fc.utobj().printTestStep(testCaseId, "Login With Corporate User"
			 * ); fc.loginpage().loginWithParameter(driver, userName, password);
			 */

			fc.utobj().printTestStep("Navigate To Admin > CRM > Configure Status Page");
			fc.utobj().printTestStep("Add Lead Status");
			String leadStatus = dataSet.get("leadStatus");
			AdminCRMConfigureStatusPageTest statusPage = new AdminCRMConfigureStatusPageTest();
			statusPage.addStatus(driver, leadStatus);

			fc.utobj().printTestStep("Navigate To CRM > Campaign Center");
			fc.crm().crm_common().CRMModule(driver);

			fc.utobj().printTestStep("Create Campaign For Send Campaign");

			String campaignName = dataSet.get("campaignName");
			String accessibility = dataSet.get("accessibility");
			String campaignType = dataSet.get("campaignType");
			String templateName = dataSet.get("templateName");
			String emailSubject = dataSet.get("emailSubject");
			String plainTextEditor = dataSet.get("plainTextEditor");
			CRMCampaignCenterPageTest campaignCenterPage = new CRMCampaignCenterPageTest();
			campaignCenterPage.createCampaign(driver, config, campaignName, accessibility, campaignType, templateName,
					emailSubject, plainTextEditor);

			fc.utobj().printTestStep("Create Template For Send Email");

			String templateNameForSendEmail = dataSet.get("templateNameForSendEmail");
			String emailSubjectForSendEmail = dataSet.get("emailSubjectForSendEmail");
			String plainTextEditorForSendEmail = dataSet.get("plainTextEditorForSendEmail");

			CRMCampaignCenterEmailTemplatesPageTest emailTemplatePage = new CRMCampaignCenterEmailTemplatesPageTest();
			emailTemplatePage.createTemplate(driver, config, templateNameForSendEmail, emailSubjectForSendEmail,
					plainTextEditorForSendEmail);

			fc.utobj().printTestStep("Navigate To CRM > Worflows Page");
			fc.utobj().printTestStep("Create Lead Type Standard Workflow");

			String workFlowName = dataSet.get("workFlowName");
			String triggerType = dataSet.get("triggerType");
			String conditionType = dataSet.get("conditionType");
			String availableFields = dataSet.get("availableFields");
			String taskSubject = dataSet.get("taskSubject");
			String taskPriority = dataSet.get("taskPriority");
			String taskStatus = dataSet.get("taskStatus");
			String matchingCondition = dataSet.get("matchingCondition");

			createWorkFlowLeadStandard(driver, config, dataSet, workFlowName, triggerType, conditionType,
					availableFields, matchingCondition, campaignName, leadStatus, templateNameForSendEmail, taskSubject,
					taskPriority, taskStatus);

			fc.utobj().printTestStep("Verify The Created WorkFlow");
			searchWorkFLowByFilter(driver, workFlowName);

			boolean isWorkFlowNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[.='" + workFlowName + "']");
			if (isWorkFlowNamePresent == false) {
				fc.utobj().throwsException("was not able to verify the workflow");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "crmworkflow")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Create The Standard WorkFlow When Lead is Added Or Modified For Leads Matching Criteria Status Of Lead Is Customized", testCaseId = "TC_38_Create_Workflow")
	public void createWorkFlow38() throws IOException, Exception, ParserConfigurationException, SAXException {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crmworkflow", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			/*
			 * 
			 * String
			 * userName=fc.utobj().generateTestData(dataSet.get("userName"));
			 * String
			 * password=fc.utobj().generateTestData(dataSet.get("password"));
			 * String emailId="crmautomation@staffex.com";
			 * AdminUsersManageCorporateUsersAddCorporateUserPageTest
			 * addcorporateUserPage=new
			 * AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			 * addcorporateUserPage.addCorporateUser(driver, userName, password,
			 * config , emailId);
			 * 
			 * fc.home_page().logout(driver, config);
			 * fc.utobj().printTestStep(testCaseId, "Login With Corporate User"
			 * ); fc.loginpage().loginWithParameter(driver, userName, password);
			 */

			fc.utobj().printTestStep("Navigate To Admin > CRM > Configure Status Page");
			fc.utobj().printTestStep("Add Lead Status");
			String leadStatus = dataSet.get("leadStatus");
			AdminCRMConfigureStatusPageTest statusPage = new AdminCRMConfigureStatusPageTest();
			statusPage.addStatus(driver, leadStatus);

			fc.utobj().printTestStep("Navigate To Admin > CRM > Configure Status Page");
			fc.utobj().printTestStep("Add Lead Status");
			String leadStatusCustomize = dataSet.get("selectLeadStatus");
			statusPage.addStatus(driver, leadStatusCustomize);

			fc.utobj().printTestStep("Navigate To CRM > Campaign Center");
			fc.crm().crm_common().CRMModule(driver);

			fc.utobj().printTestStep("Create Campaign For Send Campaign");

			String campaignName = dataSet.get("campaignName");
			String accessibility = dataSet.get("accessibility");
			String campaignType = dataSet.get("campaignType");
			String templateName = dataSet.get("templateName");
			String emailSubject = dataSet.get("emailSubject");
			String plainTextEditor = dataSet.get("plainTextEditor");
			CRMCampaignCenterPageTest campaignCenterPage = new CRMCampaignCenterPageTest();
			campaignCenterPage.createCampaign(driver, config, campaignName, accessibility, campaignType, templateName,
					emailSubject, plainTextEditor);

			fc.utobj().printTestStep("Create Template For Send Email");

			String templateNameForSendEmail = dataSet.get("templateNameForSendEmail");
			String emailSubjectForSendEmail = dataSet.get("emailSubjectForSendEmail");
			String plainTextEditorForSendEmail = dataSet.get("plainTextEditorForSendEmail");

			CRMCampaignCenterEmailTemplatesPageTest emailTemplatePage = new CRMCampaignCenterEmailTemplatesPageTest();
			emailTemplatePage.createTemplate(driver, config, templateNameForSendEmail, emailSubjectForSendEmail,
					plainTextEditorForSendEmail);

			fc.utobj().printTestStep("Navigate To CRM > Worflows Page");
			fc.utobj().printTestStep("Create Lead Type Standard Workflow");

			String workFlowName = dataSet.get("workFlowName");
			String triggerType = dataSet.get("triggerType");
			String conditionType = dataSet.get("conditionType");
			String availableFields = dataSet.get("availableFields");
			String taskSubject = dataSet.get("taskSubject");
			String taskPriority = dataSet.get("taskPriority");
			String taskStatus = dataSet.get("taskStatus");
			String matchingCondition = dataSet.get("matchingCondition");

			createWorkFlowLeadStandard(driver, config, dataSet, workFlowName, triggerType, conditionType,
					availableFields, matchingCondition, campaignName, leadStatus, templateNameForSendEmail, taskSubject,
					taskPriority, taskStatus);

			fc.utobj().printTestStep("Verify The Created WorkFlow");
			searchWorkFLowByFilter(driver, workFlowName);

			boolean isWorkFlowNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[.='" + workFlowName + "']");
			if (isWorkFlowNamePresent == false) {
				fc.utobj().throwsException("was not able to verify the workflow");
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "crmworkflow")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Create The Standard WorkFlow When Contact Is Added Or Modified For Contact Matching Criteria Email Field is Not Empty", testCaseId = "TC_39_Create_Workflow")
	public void createWorkFlow39() throws IOException, Exception, ParserConfigurationException, SAXException {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crmworkflow", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			/*
			 * 
			 * String
			 * userName=fc.utobj().generateTestData(dataSet.get("userName"));
			 * String
			 * password=fc.utobj().generateTestData(dataSet.get("password"));
			 * String emailId="crmautomation@staffex.com";
			 * AdminUsersManageCorporateUsersAddCorporateUserPageTest
			 * addcorporateUserPage=new
			 * AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			 * addcorporateUserPage.addCorporateUser(driver, userName, password,
			 * config , emailId);
			 * 
			 * fc.home_page().logout(driver, config);
			 * fc.utobj().printTestStep(testCaseId, "Login With Corporate User"
			 * ); fc.loginpage().loginWithParameter(driver, userName, password);
			 */

			fc.utobj().printTestStep("Navigate To Admin > CRM > Configure Status Page");
			fc.utobj().printTestStep("Add Contact Status");
			String contactStatus = dataSet.get("contactStatus");
			AdminCRMConfigureStatusPageTest statusPage = new AdminCRMConfigureStatusPageTest();
			statusPage.addStatus01(driver, contactStatus);

			fc.utobj().printTestStep("Navigate To CRM > Campaign Center");
			fc.crm().crm_common().CRMModule(driver);

			fc.utobj().printTestStep("Create Campaign For Send Campaign");

			String campaignName = dataSet.get("campaignName");
			String accessibility = dataSet.get("accessibility");
			String campaignType = dataSet.get("campaignType");
			String templateName = dataSet.get("templateName");
			String emailSubject = dataSet.get("emailSubject");
			String plainTextEditor = dataSet.get("plainTextEditor");
			CRMCampaignCenterPageTest campaignCenterPage = new CRMCampaignCenterPageTest();
			campaignCenterPage.createCampaign(driver, config, campaignName, accessibility, campaignType, templateName,
					emailSubject, plainTextEditor);

			fc.utobj().printTestStep("Create Template For Send Email");

			String templateNameForSendEmail = dataSet.get("templateNameForSendEmail");
			String emailSubjectForSendEmail = dataSet.get("emailSubjectForSendEmail");
			String plainTextEditorForSendEmail = dataSet.get("plainTextEditorForSendEmail");

			CRMCampaignCenterEmailTemplatesPageTest emailTemplatePage = new CRMCampaignCenterEmailTemplatesPageTest();
			emailTemplatePage.createTemplate(driver, config, templateNameForSendEmail, emailSubjectForSendEmail,
					plainTextEditorForSendEmail);

			fc.utobj().printTestStep("Navigate To CRM > Worflows Page");
			fc.utobj().printTestStep("Create Contact Type Standard Workflow");

			String workFlowName = dataSet.get("workFlowName");
			String triggerType = dataSet.get("triggerType");
			String conditionType = dataSet.get("conditionType");
			String availableFields = dataSet.get("availableFields");
			String taskSubject = dataSet.get("taskSubject");
			String taskPriority = dataSet.get("taskPriority");
			String taskStatus = dataSet.get("taskStatus");
			String matchingCondition = dataSet.get("matchingCondition");

			createWorkFlowContactStandard(driver, config, dataSet, workFlowName, triggerType, conditionType,
					availableFields, matchingCondition, campaignName, contactStatus, templateNameForSendEmail,
					taskSubject, taskPriority, taskStatus);

			fc.utobj().printTestStep("Verify The Created WorkFlow");
			searchWorkFLowByFilter(driver, workFlowName);

			boolean isWorkFlowNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[.='" + workFlowName + "']");
			if (isWorkFlowNamePresent == false) {
				fc.utobj().throwsException("was not able to verify the workflow");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "crmworkflow")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Create The Standard WorkFlow When Contact Is Added Or Modified For Contact Matching Criteria Status Of Contact is Default[New]", testCaseId = "TC_40_Create_Workflow")
	public void createWorkFlow40() throws IOException, Exception, ParserConfigurationException, SAXException {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crmworkflow", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			/*
			 * 
			 * String
			 * userName=fc.utobj().generateTestData(dataSet.get("userName"));
			 * String
			 * password=fc.utobj().generateTestData(dataSet.get("password"));
			 * String emailId="crmautomation@staffex.com";
			 * AdminUsersManageCorporateUsersAddCorporateUserPageTest
			 * addcorporateUserPage=new
			 * AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			 * addcorporateUserPage.addCorporateUser(driver, userName, password,
			 * config , emailId);
			 * 
			 * fc.home_page().logout(driver, config);
			 * fc.utobj().printTestStep(testCaseId, "Login With Corporate User"
			 * ); fc.loginpage().loginWithParameter(driver, userName, password);
			 */

			fc.utobj().printTestStep("Navigate To Admin > CRM > Configure Status Page");
			fc.utobj().printTestStep("Add Contact Status");
			String contactStatus = dataSet.get("contactStatus");
			AdminCRMConfigureStatusPageTest statusPage = new AdminCRMConfigureStatusPageTest();
			statusPage.addStatus01(driver, contactStatus);

			fc.utobj().printTestStep("Navigate To CRM > Campaign Center");
			fc.crm().crm_common().CRMModule(driver);

			fc.utobj().printTestStep("Create Campaign For Send Campaign");

			String campaignName = dataSet.get("campaignName");
			String accessibility = dataSet.get("accessibility");
			String campaignType = dataSet.get("campaignType");
			String templateName = dataSet.get("templateName");
			String emailSubject = dataSet.get("emailSubject");
			String plainTextEditor = dataSet.get("plainTextEditor");
			CRMCampaignCenterPageTest campaignCenterPage = new CRMCampaignCenterPageTest();
			campaignCenterPage.createCampaign(driver, config, campaignName, accessibility, campaignType, templateName,
					emailSubject, plainTextEditor);

			fc.utobj().printTestStep("Create Template For Send Email");

			String templateNameForSendEmail = dataSet.get("templateNameForSendEmail");
			String emailSubjectForSendEmail = dataSet.get("emailSubjectForSendEmail");
			String plainTextEditorForSendEmail = dataSet.get("plainTextEditorForSendEmail");

			CRMCampaignCenterEmailTemplatesPageTest emailTemplatePage = new CRMCampaignCenterEmailTemplatesPageTest();
			emailTemplatePage.createTemplate(driver, config, templateNameForSendEmail, emailSubjectForSendEmail,
					plainTextEditorForSendEmail);

			fc.utobj().printTestStep("Navigate To CRM > Worflows Page");
			fc.utobj().printTestStep("Create Contact Type Standard Workflow");

			String workFlowName = dataSet.get("workFlowName");
			String triggerType = dataSet.get("triggerType");
			String conditionType = dataSet.get("conditionType");
			String availableFields = dataSet.get("availableFields");
			String taskSubject = dataSet.get("taskSubject");
			String taskPriority = dataSet.get("taskPriority");
			String taskStatus = dataSet.get("taskStatus");
			String matchingCondition = dataSet.get("matchingCondition");

			createWorkFlowContactStandard(driver, config, dataSet, workFlowName, triggerType, conditionType,
					availableFields, matchingCondition, campaignName, contactStatus, templateNameForSendEmail,
					taskSubject, taskPriority, taskStatus);

			fc.utobj().printTestStep("Verify The Created WorkFlow");
			searchWorkFLowByFilter(driver, workFlowName);

			boolean isWorkFlowNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[.='" + workFlowName + "']");
			if (isWorkFlowNamePresent == false) {
				fc.utobj().throwsException("was not able to verify the workflow");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "crmworkflow")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Create The Standard WorkFlow When Contact Is Added Or Modified For Contact Matching Criteria Email Subscription status is Set To Opted In", testCaseId = "TC_41_Create_Workflow")
	public void createWorkFlow41() throws IOException, Exception, ParserConfigurationException, SAXException {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crmworkflow", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			/*
			 * 
			 * String
			 * userName=fc.utobj().generateTestData(dataSet.get("userName"));
			 * String
			 * password=fc.utobj().generateTestData(dataSet.get("password"));
			 * String emailId="crmautomation@staffex.com";
			 * AdminUsersManageCorporateUsersAddCorporateUserPageTest
			 * addcorporateUserPage=new
			 * AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			 * addcorporateUserPage.addCorporateUser(driver, userName, password,
			 * config , emailId);
			 * 
			 * fc.home_page().logout(driver, config);
			 * fc.utobj().printTestStep(testCaseId, "Login With Corporate User"
			 * ); fc.loginpage().loginWithParameter(driver, userName, password);
			 */

			fc.utobj().printTestStep("Navigate To Admin > CRM > Configure Status Page");
			fc.utobj().printTestStep("Add Contact Status");
			String contactStatus = dataSet.get("contactStatus");
			AdminCRMConfigureStatusPageTest statusPage = new AdminCRMConfigureStatusPageTest();
			statusPage.addStatus01(driver, contactStatus);

			fc.utobj().printTestStep("Navigate To CRM > Campaign Center");
			fc.crm().crm_common().CRMModule(driver);

			fc.utobj().printTestStep("Create Campaign For Send Campaign");

			String campaignName = dataSet.get("campaignName");
			String accessibility = dataSet.get("accessibility");
			String campaignType = dataSet.get("campaignType");
			String templateName = dataSet.get("templateName");
			String emailSubject = dataSet.get("emailSubject");
			String plainTextEditor = dataSet.get("plainTextEditor");
			CRMCampaignCenterPageTest campaignCenterPage = new CRMCampaignCenterPageTest();
			campaignCenterPage.createCampaign(driver, config, campaignName, accessibility, campaignType, templateName,
					emailSubject, plainTextEditor);

			fc.utobj().printTestStep("Create Template For Send Email");

			String templateNameForSendEmail = dataSet.get("templateNameForSendEmail");
			String emailSubjectForSendEmail = dataSet.get("emailSubjectForSendEmail");
			String plainTextEditorForSendEmail = dataSet.get("plainTextEditorForSendEmail");

			CRMCampaignCenterEmailTemplatesPageTest emailTemplatePage = new CRMCampaignCenterEmailTemplatesPageTest();
			emailTemplatePage.createTemplate(driver, config, templateNameForSendEmail, emailSubjectForSendEmail,
					plainTextEditorForSendEmail);

			fc.utobj().printTestStep("Navigate To CRM > Worflows Page");
			fc.utobj().printTestStep("Create Contact Type Standard Workflow");

			String workFlowName = dataSet.get("workFlowName");
			String triggerType = dataSet.get("triggerType");
			String conditionType = dataSet.get("conditionType");
			String availableFields = dataSet.get("availableFields");
			String taskSubject = dataSet.get("taskSubject");
			String taskPriority = dataSet.get("taskPriority");
			String taskStatus = dataSet.get("taskStatus");
			String matchingCondition = dataSet.get("matchingCondition");

			createWorkFlowContactStandard(driver, config, dataSet, workFlowName, triggerType, conditionType,
					availableFields, matchingCondition, campaignName, contactStatus, templateNameForSendEmail,
					taskSubject, taskPriority, taskStatus);

			fc.utobj().printTestStep("Verify The Created WorkFlow");
			searchWorkFLowByFilter(driver, workFlowName);

			boolean isWorkFlowNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[.='" + workFlowName + "']");
			if (isWorkFlowNamePresent == false) {
				fc.utobj().throwsException("was not able to verify the workflow");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "crmworkflow")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Create The Standard WorkFlow When Contact Is Added Or Modified For Contact Matching Criteria Country And All States of It", testCaseId = "TC_42_Create_Workflow")
	public void createWorkFlow42() throws IOException, Exception, ParserConfigurationException, SAXException {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crmworkflow", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			/*
			 * 
			 * String
			 * userName=fc.utobj().generateTestData(dataSet.get("userName"));
			 * String
			 * password=fc.utobj().generateTestData(dataSet.get("password"));
			 * String emailId="crmautomation@staffex.com";
			 * AdminUsersManageCorporateUsersAddCorporateUserPageTest
			 * addcorporateUserPage=new
			 * AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			 * addcorporateUserPage.addCorporateUser(driver, userName, password,
			 * config , emailId);
			 * 
			 * fc.home_page().logout(driver, config);
			 * fc.utobj().printTestStep(testCaseId, "Login With Corporate User"
			 * ); fc.loginpage().loginWithParameter(driver, userName, password);
			 */

			fc.utobj().printTestStep("Navigate To Admin > CRM > Configure Status Page");
			fc.utobj().printTestStep("Add Contact Status");
			String contactStatus = dataSet.get("contactStatus");
			AdminCRMConfigureStatusPageTest statusPage = new AdminCRMConfigureStatusPageTest();
			statusPage.addStatus01(driver, contactStatus);

			fc.utobj().printTestStep("Navigate To CRM > Campaign Center");
			fc.crm().crm_common().CRMModule(driver);

			fc.utobj().printTestStep("Create Campaign For Send Campaign");

			String campaignName = dataSet.get("campaignName");
			String accessibility = dataSet.get("accessibility");
			String campaignType = dataSet.get("campaignType");
			String templateName = dataSet.get("templateName");
			String emailSubject = dataSet.get("emailSubject");
			String plainTextEditor = dataSet.get("plainTextEditor");
			CRMCampaignCenterPageTest campaignCenterPage = new CRMCampaignCenterPageTest();
			campaignCenterPage.createCampaign(driver, config, campaignName, accessibility, campaignType, templateName,
					emailSubject, plainTextEditor);

			fc.utobj().printTestStep("Create Template For Send Email");

			String templateNameForSendEmail = dataSet.get("templateNameForSendEmail");
			String emailSubjectForSendEmail = dataSet.get("emailSubjectForSendEmail");
			String plainTextEditorForSendEmail = dataSet.get("plainTextEditorForSendEmail");

			CRMCampaignCenterEmailTemplatesPageTest emailTemplatePage = new CRMCampaignCenterEmailTemplatesPageTest();
			emailTemplatePage.createTemplate(driver, config, templateNameForSendEmail, emailSubjectForSendEmail,
					plainTextEditorForSendEmail);

			fc.utobj().printTestStep("Navigate To CRM > Worflows Page");
			fc.utobj().printTestStep("Create Contact Type Standard Workflow");

			String workFlowName = dataSet.get("workFlowName");
			String triggerType = dataSet.get("triggerType");
			String conditionType = dataSet.get("conditionType");
			String availableFields = dataSet.get("availableFields");
			String taskSubject = dataSet.get("taskSubject");
			String taskPriority = dataSet.get("taskPriority");
			String taskStatus = dataSet.get("taskStatus");
			String matchingCondition = dataSet.get("matchingCondition");

			createWorkFlowContactStandard(driver, config, dataSet, workFlowName, triggerType, conditionType,
					availableFields, matchingCondition, campaignName, contactStatus, templateNameForSendEmail,
					taskSubject, taskPriority, taskStatus);

			fc.utobj().printTestStep("Verify The Created WorkFlow");
			searchWorkFLowByFilter(driver, workFlowName);

			boolean isWorkFlowNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[.='" + workFlowName + "']");
			if (isWorkFlowNamePresent == false) {
				fc.utobj().throwsException("was not able to verify the workflow");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "crmworkflow")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Create The Standard WorkFlow When Contact Is Added Or Modified For Contact Matching Criteria BirthDate And Set To It , is Between", testCaseId = "TC_43_Create_Workflow")
	public void createWorkFlow43() throws IOException, Exception, ParserConfigurationException, SAXException {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crmworkflow", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			/*
			 * 
			 * String
			 * userName=fc.utobj().generateTestData(dataSet.get("userName"));
			 * String
			 * password=fc.utobj().generateTestData(dataSet.get("password"));
			 * String emailId="crmautomation@staffex.com";
			 * AdminUsersManageCorporateUsersAddCorporateUserPageTest
			 * addcorporateUserPage=new
			 * AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			 * addcorporateUserPage.addCorporateUser(driver, userName, password,
			 * config , emailId);
			 * 
			 * fc.home_page().logout(driver, config);
			 * fc.utobj().printTestStep(testCaseId, "Login With Corporate User"
			 * ); fc.loginpage().loginWithParameter(driver, userName, password);
			 */

			fc.utobj().printTestStep("Navigate To Admin > CRM > Configure Status Page");
			fc.utobj().printTestStep("Add Contact Status");
			String contactStatus = dataSet.get("contactStatus");
			AdminCRMConfigureStatusPageTest statusPage = new AdminCRMConfigureStatusPageTest();
			statusPage.addStatus01(driver, contactStatus);

			fc.utobj().printTestStep("Navigate To CRM > Campaign Center");
			fc.crm().crm_common().CRMModule(driver);

			fc.utobj().printTestStep("Create Campaign For Send Campaign");

			String campaignName = dataSet.get("campaignName");
			String accessibility = dataSet.get("accessibility");
			String campaignType = dataSet.get("campaignType");
			String templateName = dataSet.get("templateName");
			String emailSubject = dataSet.get("emailSubject");
			String plainTextEditor = dataSet.get("plainTextEditor");
			CRMCampaignCenterPageTest campaignCenterPage = new CRMCampaignCenterPageTest();
			campaignCenterPage.createCampaign(driver, config, campaignName, accessibility, campaignType, templateName,
					emailSubject, plainTextEditor);

			fc.utobj().printTestStep("Create Template For Send Email");

			String templateNameForSendEmail = dataSet.get("templateNameForSendEmail");
			String emailSubjectForSendEmail = dataSet.get("emailSubjectForSendEmail");
			String plainTextEditorForSendEmail = dataSet.get("plainTextEditorForSendEmail");

			CRMCampaignCenterEmailTemplatesPageTest emailTemplatePage = new CRMCampaignCenterEmailTemplatesPageTest();
			emailTemplatePage.createTemplate(driver, config, templateNameForSendEmail, emailSubjectForSendEmail,
					plainTextEditorForSendEmail);

			fc.utobj().printTestStep("Navigate To CRM > Worflows Page");
			fc.utobj().printTestStep("Create Contact Type Standard Workflow");

			String workFlowName = dataSet.get("workFlowName");
			String triggerType = dataSet.get("triggerType");
			String conditionType = dataSet.get("conditionType");
			String availableFields = dataSet.get("availableFields");
			String taskSubject = dataSet.get("taskSubject");
			String taskPriority = dataSet.get("taskPriority");
			String taskStatus = dataSet.get("taskStatus");
			String matchingCondition = dataSet.get("matchingCondition");

			createWorkFlowContactStandard(driver, config, dataSet, workFlowName, triggerType, conditionType,
					availableFields, matchingCondition, campaignName, contactStatus, templateNameForSendEmail,
					taskSubject, taskPriority, taskStatus);

			fc.utobj().printTestStep("Verify The Created WorkFlow");
			searchWorkFLowByFilter(driver, workFlowName);

			boolean isWorkFlowNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[.='" + workFlowName + "']");
			if (isWorkFlowNamePresent == false) {
				fc.utobj().throwsException("was not able to verify the workflow");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "crmworkflow")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Create The Standard WorkFlow When Contact Is Added Or Modified For Contact Matching Criteria Status Of Contact Is Customized", testCaseId = "TC_44_Create_Workflow")
	public void createWorkFlow44() throws IOException, Exception, ParserConfigurationException, SAXException {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crmworkflow", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			/*
			 * 
			 * String
			 * userName=fc.utobj().generateTestData(dataSet.get("userName"));
			 * String
			 * password=fc.utobj().generateTestData(dataSet.get("password"));
			 * String emailId="crmautomation@staffex.com";
			 * AdminUsersManageCorporateUsersAddCorporateUserPageTest
			 * addcorporateUserPage=new
			 * AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			 * addcorporateUserPage.addCorporateUser(driver, userName, password,
			 * config , emailId);
			 * 
			 * fc.home_page().logout(driver, config);
			 * fc.utobj().printTestStep(testCaseId, "Login With Corporate User"
			 * ); fc.loginpage().loginWithParameter(driver, userName, password);
			 */

			fc.utobj().printTestStep("Navigate To Admin > CRM > Configure Status Page");
			fc.utobj().printTestStep("Add Contact Status");
			String contactStatus = dataSet.get("contactStatus");
			AdminCRMConfigureStatusPageTest statusPage = new AdminCRMConfigureStatusPageTest();
			statusPage.addStatus01(driver, contactStatus);

			fc.utobj().printTestStep("Navigate To Admin > CRM > Configure Status Page");
			fc.utobj().printTestStep("Add Contact Status");
			String contactStatusCustomize = dataSet.get("selectContactStatus");
			statusPage.addStatus01(driver, contactStatusCustomize);

			fc.utobj().printTestStep("Navigate To CRM > Campaign Center");
			fc.crm().crm_common().CRMModule(driver);

			fc.utobj().printTestStep("Create Campaign For Send Campaign");

			String campaignName = dataSet.get("campaignName");
			String accessibility = dataSet.get("accessibility");
			String campaignType = dataSet.get("campaignType");
			String templateName = dataSet.get("templateName");
			String emailSubject = dataSet.get("emailSubject");
			String plainTextEditor = dataSet.get("plainTextEditor");
			CRMCampaignCenterPageTest campaignCenterPage = new CRMCampaignCenterPageTest();
			campaignCenterPage.createCampaign(driver, config, campaignName, accessibility, campaignType, templateName,
					emailSubject, plainTextEditor);

			fc.utobj().printTestStep("Create Template For Send Email");

			String templateNameForSendEmail = dataSet.get("templateNameForSendEmail");
			String emailSubjectForSendEmail = dataSet.get("emailSubjectForSendEmail");
			String plainTextEditorForSendEmail = dataSet.get("plainTextEditorForSendEmail");

			CRMCampaignCenterEmailTemplatesPageTest emailTemplatePage = new CRMCampaignCenterEmailTemplatesPageTest();
			emailTemplatePage.createTemplate(driver, config, templateNameForSendEmail, emailSubjectForSendEmail,
					plainTextEditorForSendEmail);

			fc.utobj().printTestStep("Navigate To CRM > Worflows Page");
			fc.utobj().printTestStep("Create Contact Type Standard Workflow");

			String workFlowName = dataSet.get("workFlowName");
			String triggerType = dataSet.get("triggerType");
			String conditionType = dataSet.get("conditionType");
			String availableFields = dataSet.get("availableFields");
			String taskSubject = dataSet.get("taskSubject");
			String taskPriority = dataSet.get("taskPriority");
			String taskStatus = dataSet.get("taskStatus");
			String matchingCondition = dataSet.get("matchingCondition");

			createWorkFlowContactStandard(driver, config, dataSet, workFlowName, triggerType, conditionType,
					availableFields, matchingCondition, campaignName, contactStatus, templateNameForSendEmail,
					taskSubject, taskPriority, taskStatus);

			fc.utobj().printTestStep("Verify The Created WorkFlow");
			searchWorkFLowByFilter(driver, workFlowName);

			boolean isWorkFlowNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[.='" + workFlowName + "']");
			if (isWorkFlowNamePresent == false) {
				fc.utobj().throwsException("was not able to verify the workflow");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "crmworkflow")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Create The Standard WorkFlow When Contact Is Added Or Modified For Contact Matching Criteria Contact Type In", testCaseId = "TC_45_Create_Workflow")
	public void createWorkFlow45() throws IOException, Exception, ParserConfigurationException, SAXException {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crmworkflow", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			/*
			 * 
			 * String
			 * userName=fc.utobj().generateTestData(dataSet.get("userName"));
			 * String
			 * password=fc.utobj().generateTestData(dataSet.get("password"));
			 * String emailId="crmautomation@staffex.com";
			 * AdminUsersManageCorporateUsersAddCorporateUserPageTest
			 * addcorporateUserPage=new
			 * AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			 * addcorporateUserPage.addCorporateUser(driver, userName, password,
			 * config , emailId);
			 * 
			 * fc.home_page().logout(driver, config);
			 * fc.utobj().printTestStep(testCaseId, "Login With Corporate User"
			 * ); fc.loginpage().loginWithParameter(driver, userName, password);
			 */

			fc.utobj().printTestStep("Navigate To Admin > CRM > Configure Status Page");
			fc.utobj().printTestStep("Add Contact Status");
			String contactStatus = dataSet.get("contactStatus");
			AdminCRMConfigureStatusPageTest statusPage = new AdminCRMConfigureStatusPageTest();
			statusPage.addStatus01(driver, contactStatus);

			fc.utobj().printTestStep("Navigate To Admin > CRM > Contact Type Configureation");
			fc.utobj().printTestStep("Add Contact Type");
			String contactType = dataSet.get("selectcontactType");
			AdminCRMContactTypeConfigurationPageTest contactTypeConfigureationPage = new AdminCRMContactTypeConfigurationPageTest();
			contactTypeConfigureationPage.addContactType(driver, contactType);

			fc.utobj().printTestStep("Navigate To CRM > Campaign Center");
			fc.crm().crm_common().CRMModule(driver);

			fc.utobj().printTestStep("Create Campaign For Send Campaign");

			String campaignName = dataSet.get("campaignName");
			String accessibility = dataSet.get("accessibility");
			String campaignType = dataSet.get("campaignType");
			String templateName = dataSet.get("templateName");
			String emailSubject = dataSet.get("emailSubject");
			String plainTextEditor = dataSet.get("plainTextEditor");
			CRMCampaignCenterPageTest campaignCenterPage = new CRMCampaignCenterPageTest();
			campaignCenterPage.createCampaign(driver, config, campaignName, accessibility, campaignType, templateName,
					emailSubject, plainTextEditor);

			fc.utobj().printTestStep("Create Template For Send Email");

			String templateNameForSendEmail = dataSet.get("templateNameForSendEmail");
			String emailSubjectForSendEmail = dataSet.get("emailSubjectForSendEmail");
			String plainTextEditorForSendEmail = dataSet.get("plainTextEditorForSendEmail");

			CRMCampaignCenterEmailTemplatesPageTest emailTemplatePage = new CRMCampaignCenterEmailTemplatesPageTest();
			emailTemplatePage.createTemplate(driver, config, templateNameForSendEmail, emailSubjectForSendEmail,
					plainTextEditorForSendEmail);

			fc.utobj().printTestStep("Navigate To CRM > Worflows Page");
			fc.utobj().printTestStep("Create Contact Type Standard Workflow");

			String workFlowName = dataSet.get("workFlowName");
			String triggerType = dataSet.get("triggerType");
			String conditionType = dataSet.get("conditionType");
			String availableFields = dataSet.get("availableFields");
			String taskSubject = dataSet.get("taskSubject");
			String taskPriority = dataSet.get("taskPriority");
			String taskStatus = dataSet.get("taskStatus");
			String matchingCondition = dataSet.get("matchingCondition");

			createWorkFlowContactStandard(driver, config, dataSet, workFlowName, triggerType, conditionType,
					availableFields, matchingCondition, campaignName, contactStatus, templateNameForSendEmail,
					taskSubject, taskPriority, taskStatus);

			fc.utobj().printTestStep("Verify The Created WorkFlow");
			searchWorkFLowByFilter(driver, workFlowName);

			boolean isWorkFlowNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[.='" + workFlowName + "']");
			if (isWorkFlowNamePresent == false) {
				fc.utobj().throwsException("was not able to verify the workflow");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "crmworkflow")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Create The Date Based WorkFlow of BirthDay Type And Date of Execution At Same Day When Lead Is Added For Leads Matching Criteria Email Field is Not Empty", testCaseId = "TC_46_Create_Workflow")
	public void createWorkFlow46() throws IOException, Exception, ParserConfigurationException, SAXException {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crmworkflow", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			/*
			 * 
			 * String
			 * userName=fc.utobj().generateTestData(dataSet.get("userName"));
			 * String
			 * password=fc.utobj().generateTestData(dataSet.get("password"));
			 * String emailId="crmautomation@staffex.com";
			 * AdminUsersManageCorporateUsersAddCorporateUserPageTest
			 * addcorporateUserPage=new
			 * AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			 * addcorporateUserPage.addCorporateUser(driver, userName, password,
			 * config , emailId);
			 * 
			 * fc.home_page().logout(driver, config);
			 * fc.utobj().printTestStep(testCaseId, "Login With Corporate User"
			 * ); fc.loginpage().loginWithParameter(driver, userName, password);
			 */

			fc.utobj().printTestStep("Navigate To Admin > CRM > Configure Status Page");
			fc.utobj().printTestStep("Add Lead Status");
			String leadStatus = dataSet.get("leadStatus");
			AdminCRMConfigureStatusPageTest statusPage = new AdminCRMConfigureStatusPageTest();
			statusPage.addStatus(driver, leadStatus);

			fc.utobj().printTestStep("Navigate To CRM > Campaign Center");
			fc.crm().crm_common().CRMModule(driver);

			fc.utobj().printTestStep("Create Campaign For Send Campaign");

			String campaignName = dataSet.get("campaignName");
			String accessibility = dataSet.get("accessibility");
			String campaignType = dataSet.get("campaignType");
			String templateName = dataSet.get("templateName");
			String emailSubject = dataSet.get("emailSubject");
			String plainTextEditor = dataSet.get("plainTextEditor");
			CRMCampaignCenterPageTest campaignCenterPage = new CRMCampaignCenterPageTest();
			campaignCenterPage.createCampaign(driver, config, campaignName, accessibility, campaignType, templateName,
					emailSubject, plainTextEditor);

			fc.utobj().printTestStep("Create Template For Send Email");

			String templateNameForSendEmail = dataSet.get("templateNameForSendEmail");
			String emailSubjectForSendEmail = dataSet.get("emailSubjectForSendEmail");
			String plainTextEditorForSendEmail = dataSet.get("plainTextEditorForSendEmail");

			CRMCampaignCenterEmailTemplatesPageTest emailTemplatePage = new CRMCampaignCenterEmailTemplatesPageTest();
			emailTemplatePage.createTemplate(driver, config, templateNameForSendEmail, emailSubjectForSendEmail,
					plainTextEditorForSendEmail);

			fc.utobj().printTestStep("Navigate To CRM > Worflows Page");
			fc.utobj().printTestStep("Create Lead Type Date Based Workflow");

			String workFlowName = dataSet.get("workFlowName");
			String triggerType = dataSet.get("triggerType");
			String conditionType = dataSet.get("conditionType");
			String availableFields = dataSet.get("availableFields");
			String taskSubject = dataSet.get("taskSubject");
			String taskPriority = dataSet.get("taskPriority");
			String taskStatus = dataSet.get("taskStatus");
			String matchingCondition = dataSet.get("matchingCondition");
			String dateOfExecution = dataSet.get("dateOfExecution");

			createWorkFlowLeadDateBased(driver, config, dataSet, workFlowName, triggerType, dateOfExecution,
					conditionType, availableFields, matchingCondition, campaignName, leadStatus,
					templateNameForSendEmail, taskSubject, taskPriority, taskStatus);

			fc.utobj().printTestStep("Verify The Created WorkFlow");
			searchWorkFLowByFilter(driver, workFlowName);

			boolean isWorkFlowNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[.='" + workFlowName + "']");
			if (isWorkFlowNamePresent == false) {
				fc.utobj().throwsException("was not able to verify the workflow");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "crmworkflow")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Create The Date Based WorkFlow of BirthDay Type And Date of Execution At Same Day When Lead is Added For Leads Matching Criteria Status OF Lead Is Default[New]", testCaseId = "TC_47_Create_Workflow")
	public void createWorkFlow47() throws IOException, Exception, ParserConfigurationException, SAXException {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crmworkflow", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			/*
			 * 
			 * String
			 * userName=fc.utobj().generateTestData(dataSet.get("userName"));
			 * String
			 * password=fc.utobj().generateTestData(dataSet.get("password"));
			 * String emailId="crmautomation@staffex.com";
			 * AdminUsersManageCorporateUsersAddCorporateUserPageTest
			 * addcorporateUserPage=new
			 * AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			 * addcorporateUserPage.addCorporateUser(driver, userName, password,
			 * config , emailId);
			 * 
			 * fc.home_page().logout(driver, config);
			 * fc.utobj().printTestStep(testCaseId, "Login With Corporate User"
			 * ); fc.loginpage().loginWithParameter(driver, userName, password);
			 */

			fc.utobj().printTestStep("Navigate To Admin > CRM > Configure Status Page");
			fc.utobj().printTestStep("Add Lead Status");
			String leadStatus = dataSet.get("leadStatus");
			AdminCRMConfigureStatusPageTest statusPage = new AdminCRMConfigureStatusPageTest();
			statusPage.addStatus(driver, leadStatus);

			fc.utobj().printTestStep("Navigate To CRM > Campaign Center");
			fc.crm().crm_common().CRMModule(driver);

			fc.utobj().printTestStep("Create Campaign For Send Campaign");

			String campaignName = dataSet.get("campaignName");
			String accessibility = dataSet.get("accessibility");
			String campaignType = dataSet.get("campaignType");
			String templateName = dataSet.get("templateName");
			String emailSubject = dataSet.get("emailSubject");
			String plainTextEditor = dataSet.get("plainTextEditor");
			CRMCampaignCenterPageTest campaignCenterPage = new CRMCampaignCenterPageTest();
			campaignCenterPage.createCampaign(driver, config, campaignName, accessibility, campaignType, templateName,
					emailSubject, plainTextEditor);

			fc.utobj().printTestStep("Create Template For Send Email");

			String templateNameForSendEmail = dataSet.get("templateNameForSendEmail");
			String emailSubjectForSendEmail = dataSet.get("emailSubjectForSendEmail");
			String plainTextEditorForSendEmail = dataSet.get("plainTextEditorForSendEmail");

			CRMCampaignCenterEmailTemplatesPageTest emailTemplatePage = new CRMCampaignCenterEmailTemplatesPageTest();
			emailTemplatePage.createTemplate(driver, config, templateNameForSendEmail, emailSubjectForSendEmail,
					plainTextEditorForSendEmail);

			fc.utobj().printTestStep("Navigate To CRM > Worflows Page");
			fc.utobj().printTestStep("Create Lead Type Date Based Workflow");

			String workFlowName = dataSet.get("workFlowName");
			String triggerType = dataSet.get("triggerType");
			String conditionType = dataSet.get("conditionType");
			String availableFields = dataSet.get("availableFields");
			String taskSubject = dataSet.get("taskSubject");
			String taskPriority = dataSet.get("taskPriority");
			String taskStatus = dataSet.get("taskStatus");
			String matchingCondition = dataSet.get("matchingCondition");
			String dateOfExecution = dataSet.get("dateOfExecution");

			createWorkFlowLeadDateBased(driver, config, dataSet, workFlowName, triggerType, dateOfExecution,
					conditionType, availableFields, matchingCondition, campaignName, leadStatus,
					templateNameForSendEmail, taskSubject, taskPriority, taskStatus);

			fc.utobj().printTestStep("Verify The Created WorkFlow");
			searchWorkFLowByFilter(driver, workFlowName);

			boolean isWorkFlowNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[.='" + workFlowName + "']");
			if (isWorkFlowNamePresent == false) {
				fc.utobj().throwsException("was not able to verify the workflow");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "crmworkflow")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Create The Date Based WorkFlow of BirthDay Type And Date of Execution At Same Day When Lead is Added For Leads Matching Criteria Email Subscription status is Set To Opted In", testCaseId = "TC_48_Create_Workflow")
	public void createWorkFlow48() throws IOException, Exception, ParserConfigurationException, SAXException {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crmworkflow", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			/*
			 * 
			 * String
			 * userName=fc.utobj().generateTestData(dataSet.get("userName"));
			 * String
			 * password=fc.utobj().generateTestData(dataSet.get("password"));
			 * String emailId="crmautomation@staffex.com";
			 * AdminUsersManageCorporateUsersAddCorporateUserPageTest
			 * addcorporateUserPage=new
			 * AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			 * addcorporateUserPage.addCorporateUser(driver, userName, password,
			 * config , emailId);
			 * 
			 * fc.home_page().logout(driver, config);
			 * fc.utobj().printTestStep(testCaseId, "Login With Corporate User"
			 * ); fc.loginpage().loginWithParameter(driver, userName, password);
			 */

			fc.utobj().printTestStep("Navigate To Admin > CRM > Configure Status Page");
			fc.utobj().printTestStep("Add Lead Status");
			String leadStatus = dataSet.get("leadStatus");
			AdminCRMConfigureStatusPageTest statusPage = new AdminCRMConfigureStatusPageTest();
			statusPage.addStatus(driver, leadStatus);

			fc.utobj().printTestStep("Navigate To CRM > Campaign Center");
			fc.crm().crm_common().CRMModule(driver);

			fc.utobj().printTestStep("Create Campaign For Send Campaign");

			String campaignName = dataSet.get("campaignName");
			String accessibility = dataSet.get("accessibility");
			String campaignType = dataSet.get("campaignType");
			String templateName = dataSet.get("templateName");
			String emailSubject = dataSet.get("emailSubject");
			String plainTextEditor = dataSet.get("plainTextEditor");
			CRMCampaignCenterPageTest campaignCenterPage = new CRMCampaignCenterPageTest();
			campaignCenterPage.createCampaign(driver, config, campaignName, accessibility, campaignType, templateName,
					emailSubject, plainTextEditor);

			fc.utobj().printTestStep("Create Template For Send Email");

			String templateNameForSendEmail = dataSet.get("templateNameForSendEmail");
			String emailSubjectForSendEmail = dataSet.get("emailSubjectForSendEmail");
			String plainTextEditorForSendEmail = dataSet.get("plainTextEditorForSendEmail");

			CRMCampaignCenterEmailTemplatesPageTest emailTemplatePage = new CRMCampaignCenterEmailTemplatesPageTest();
			emailTemplatePage.createTemplate(driver, config, templateNameForSendEmail, emailSubjectForSendEmail,
					plainTextEditorForSendEmail);

			fc.utobj().printTestStep("Navigate To CRM > Worflows Page");
			fc.utobj().printTestStep("Create Lead Type Date Based Workflow");

			String workFlowName = dataSet.get("workFlowName");
			String triggerType = dataSet.get("triggerType");
			String conditionType = dataSet.get("conditionType");
			String availableFields = dataSet.get("availableFields");
			String taskSubject = dataSet.get("taskSubject");
			String taskPriority = dataSet.get("taskPriority");
			String taskStatus = dataSet.get("taskStatus");
			String matchingCondition = dataSet.get("matchingCondition");
			String dateOfExecution = dataSet.get("dateOfExecution");

			createWorkFlowLeadDateBased(driver, config, dataSet, workFlowName, triggerType, dateOfExecution,
					conditionType, availableFields, matchingCondition, campaignName, leadStatus,
					templateNameForSendEmail, taskSubject, taskPriority, taskStatus);

			fc.utobj().printTestStep("Verify The Created WorkFlow");
			searchWorkFLowByFilter(driver, workFlowName);

			boolean isWorkFlowNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[.='" + workFlowName + "']");
			if (isWorkFlowNamePresent == false) {
				fc.utobj().throwsException("was not able to verify the workflow");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "crmworkflow")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Create The Date Based WorkFlow of BirthDay Type And Date of Execution At Same Day When Lead is Added For Leads Matching Criteria Country And All States of It", testCaseId = "TC_49_Create_Workflow")
	public void createWorkFlow49() throws IOException, Exception, ParserConfigurationException, SAXException {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crmworkflow", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			/*
			 * 
			 * String
			 * userName=fc.utobj().generateTestData(dataSet.get("userName"));
			 * String
			 * password=fc.utobj().generateTestData(dataSet.get("password"));
			 * String emailId="crmautomation@staffex.com";
			 * AdminUsersManageCorporateUsersAddCorporateUserPageTest
			 * addcorporateUserPage=new
			 * AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			 * addcorporateUserPage.addCorporateUser(driver, userName, password,
			 * config , emailId);
			 * 
			 * fc.home_page().logout(driver, config);
			 * fc.utobj().printTestStep(testCaseId, "Login With Corporate User"
			 * ); fc.loginpage().loginWithParameter(driver, userName, password);
			 */

			fc.utobj().printTestStep("Navigate To Admin > CRM > Configure Status Page");
			fc.utobj().printTestStep("Add Lead Status");
			String leadStatus = dataSet.get("leadStatus");
			AdminCRMConfigureStatusPageTest statusPage = new AdminCRMConfigureStatusPageTest();
			statusPage.addStatus(driver, leadStatus);

			fc.utobj().printTestStep("Navigate To CRM > Campaign Center");
			fc.crm().crm_common().CRMModule(driver);

			fc.utobj().printTestStep("Create Campaign For Send Campaign");

			String campaignName = dataSet.get("campaignName");
			String accessibility = dataSet.get("accessibility");
			String campaignType = dataSet.get("campaignType");
			String templateName = dataSet.get("templateName");
			String emailSubject = dataSet.get("emailSubject");
			String plainTextEditor = dataSet.get("plainTextEditor");
			CRMCampaignCenterPageTest campaignCenterPage = new CRMCampaignCenterPageTest();
			campaignCenterPage.createCampaign(driver, config, campaignName, accessibility, campaignType, templateName,
					emailSubject, plainTextEditor);

			fc.utobj().printTestStep("Create Template For Send Email");

			String templateNameForSendEmail = dataSet.get("templateNameForSendEmail");
			String emailSubjectForSendEmail = dataSet.get("emailSubjectForSendEmail");
			String plainTextEditorForSendEmail = dataSet.get("plainTextEditorForSendEmail");

			CRMCampaignCenterEmailTemplatesPageTest emailTemplatePage = new CRMCampaignCenterEmailTemplatesPageTest();
			emailTemplatePage.createTemplate(driver, config, templateNameForSendEmail, emailSubjectForSendEmail,
					plainTextEditorForSendEmail);

			fc.utobj().printTestStep("Navigate To CRM > Worflows Page");
			fc.utobj().printTestStep("Create Lead Type Date Based Workflow");

			String workFlowName = dataSet.get("workFlowName");
			String triggerType = dataSet.get("triggerType");
			String conditionType = dataSet.get("conditionType");
			String availableFields = dataSet.get("availableFields");
			String taskSubject = dataSet.get("taskSubject");
			String taskPriority = dataSet.get("taskPriority");
			String taskStatus = dataSet.get("taskStatus");
			String matchingCondition = dataSet.get("matchingCondition");
			String dateOfExecution = dataSet.get("dateOfExecution");

			createWorkFlowLeadDateBased(driver, config, dataSet, workFlowName, triggerType, dateOfExecution,
					conditionType, availableFields, matchingCondition, campaignName, leadStatus,
					templateNameForSendEmail, taskSubject, taskPriority, taskStatus);

			fc.utobj().printTestStep("Verify The Created WorkFlow");
			searchWorkFLowByFilter(driver, workFlowName);

			boolean isWorkFlowNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[.='" + workFlowName + "']");
			if (isWorkFlowNamePresent == false) {
				fc.utobj().throwsException("was not able to verify the workflow");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "crmworkflow")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Create The Date Based WorkFlow of BirthDay Type And Date of Execution At Same Day When Lead is Added For Leads Matching Criteria BirthDate And Set To It , is Between", testCaseId = "TC_50_Create_Workflow")
	public void createWorkFlow50() throws IOException, Exception, ParserConfigurationException, SAXException {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crmworkflow", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			/*
			 * 
			 * String
			 * userName=fc.utobj().generateTestData(dataSet.get("userName"));
			 * String
			 * password=fc.utobj().generateTestData(dataSet.get("password"));
			 * String emailId="crmautomation@staffex.com";
			 * AdminUsersManageCorporateUsersAddCorporateUserPageTest
			 * addcorporateUserPage=new
			 * AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			 * addcorporateUserPage.addCorporateUser(driver, userName, password,
			 * config , emailId);
			 * 
			 * fc.home_page().logout(driver, config);
			 * fc.utobj().printTestStep(testCaseId, "Login With Corporate User"
			 * ); fc.loginpage().loginWithParameter(driver, userName, password);
			 */

			fc.utobj().printTestStep("Navigate To Admin > CRM > Configure Status Page");
			fc.utobj().printTestStep("Add Lead Status");
			String leadStatus = dataSet.get("leadStatus");
			AdminCRMConfigureStatusPageTest statusPage = new AdminCRMConfigureStatusPageTest();
			statusPage.addStatus(driver, leadStatus);

			fc.utobj().printTestStep("Navigate To CRM > Campaign Center");
			fc.crm().crm_common().CRMModule(driver);

			fc.utobj().printTestStep("Create Campaign For Send Campaign");

			String campaignName = dataSet.get("campaignName");
			String accessibility = dataSet.get("accessibility");
			String campaignType = dataSet.get("campaignType");
			String templateName = dataSet.get("templateName");
			String emailSubject = dataSet.get("emailSubject");
			String plainTextEditor = dataSet.get("plainTextEditor");
			CRMCampaignCenterPageTest campaignCenterPage = new CRMCampaignCenterPageTest();
			campaignCenterPage.createCampaign(driver, config, campaignName, accessibility, campaignType, templateName,
					emailSubject, plainTextEditor);

			fc.utobj().printTestStep("Create Template For Send Email");

			String templateNameForSendEmail = dataSet.get("templateNameForSendEmail");
			String emailSubjectForSendEmail = dataSet.get("emailSubjectForSendEmail");
			String plainTextEditorForSendEmail = dataSet.get("plainTextEditorForSendEmail");

			CRMCampaignCenterEmailTemplatesPageTest emailTemplatePage = new CRMCampaignCenterEmailTemplatesPageTest();
			emailTemplatePage.createTemplate(driver, config, templateNameForSendEmail, emailSubjectForSendEmail,
					plainTextEditorForSendEmail);

			fc.utobj().printTestStep("Navigate To CRM > Worflows Page");
			fc.utobj().printTestStep("Create Lead Type Date Based Workflow");

			String workFlowName = dataSet.get("workFlowName");
			String triggerType = dataSet.get("triggerType");
			String conditionType = dataSet.get("conditionType");
			String availableFields = dataSet.get("availableFields");
			String taskSubject = dataSet.get("taskSubject");
			String taskPriority = dataSet.get("taskPriority");
			String taskStatus = dataSet.get("taskStatus");
			String matchingCondition = dataSet.get("matchingCondition");
			String dateOfExecution = dataSet.get("dateOfExecution");

			createWorkFlowLeadDateBased(driver, config, dataSet, workFlowName, triggerType, dateOfExecution,
					conditionType, availableFields, matchingCondition, campaignName, leadStatus,
					templateNameForSendEmail, taskSubject, taskPriority, taskStatus);

			fc.utobj().printTestStep("Verify The Created WorkFlow");
			searchWorkFLowByFilter(driver, workFlowName);

			boolean isWorkFlowNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[.='" + workFlowName + "']");
			if (isWorkFlowNamePresent == false) {
				fc.utobj().throwsException("was not able to verify the workflow");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	/*
	 * @Test(groups = "crmworkflow")
	 * 
	 * @TestCase(createdOn="2017-05-01",updatedOn="2018-06-14",
	 * testCaseDescription =
	 * "Create The Date Based WorkFlow of BirthDay Type And Date of Execution At Same Day When Lead is Added For Leads Matching Criteria Status Of Lead Is Customized"
	 * , testCaseId = "TC_51_Create_Workflow") public void
	 * createWorkFlow51()throws IOException, Exception,
	 * ParserConfigurationException, SAXException {
	 * 
	 * String testCaseId =
	 * fc.utobj().readTestCaseInfo(this.getClass().getName()+"."+new
	 * Object(){}.getClass().getEnclosingMethod().getName());
	 * 
	 * Map<String, String> config =
	 * fc.utobj().readConfigurationFile(FranconnectUtil.path);
	 * Map<String,String> dataSet = fc.utobj().readTestData("crmworkflow",
	 * testCaseId); WebDriver driver = fc.utobj().openDriver(config);
	 * 
	 * try { driver = fc.loginpage().login(driver, config);
	 * 
	 * String userName=fc.utobj().generateTestData(dataSet.get("userName"));
	 * String password=fc.utobj().generateTestData(dataSet.get("password"));
	 * String emailId="crmautomation@staffex.com";
	 * AdminUsersManageCorporateUsersAddCorporateUserPageTest
	 * addcorporateUserPage=new
	 * AdminUsersManageCorporateUsersAddCorporateUserPageTest();
	 * addcorporateUserPage.addCorporateUser(driver, userName, password, config
	 * , emailId);
	 * 
	 * fc.home_page().logout(driver, config);
	 * fc.utobj().printTestStep(testCaseId, "Login With Corporate User");
	 * fc.loginpage().loginWithParameter(driver, userName, password);
	 * 
	 * fc.utobj().printTestStep(testCaseId,
	 * "Navigate To Admin > CRM > Configure Status Page");
	 * fc.utobj().printTestStep(testCaseId, "Add Lead Status"); String
	 * leadStatus=dataSet.get("leadStatus"); AdminCRMConfigureStatusPageTest
	 * statusPage=new AdminCRMConfigureStatusPageTest();
	 * statusPage.addStatus(driver, leadStatus, config);
	 * 
	 * fc.utobj().printTestStep(testCaseId,
	 * "Navigate To Admin > CRM > Configure Status Page");
	 * fc.utobj().printTestStep(testCaseId, "Add Lead Status"); String
	 * leadStatusCustomize=dataSet.get("selectLeadStatus");
	 * statusPage.addStatus(driver, leadStatusCustomize, config);
	 * 
	 * 
	 * fc.utobj().printTestStep(testCaseId, "Navigate To CRM > Campaign Center"
	 * ); fc.crm().crm_common().CRMModule(driver);
	 * 
	 * fc.utobj().printTestStep(testCaseId, "Create Campaign For Send Campaign"
	 * );
	 * 
	 * String campaignName=dataSet.get("campaignName"); String
	 * accessibility=dataSet.get("accessibility"); String
	 * campaignType=dataSet.get("campaignType"); String
	 * templateName=dataSet.get("templateName"); String
	 * emailSubject=dataSet.get("emailSubject"); String
	 * plainTextEditor=dataSet.get("plainTextEditor"); CRMCampaignCenterPageTest
	 * campaignCenterPage=new CRMCampaignCenterPageTest();
	 * campaignCenterPage.createCampaign(driver, config, campaignName,
	 * accessibility, campaignType, templateName, emailSubject,
	 * plainTextEditor);
	 * 
	 * fc.utobj().printTestStep(testCaseId, "Create Template For Send Email");
	 * 
	 * String templateNameForSendEmail=dataSet.get("templateNameForSendEmail");
	 * String emailSubjectForSendEmail=dataSet.get("emailSubjectForSendEmail");
	 * String
	 * plainTextEditorForSendEmail=dataSet.get("plainTextEditorForSendEmail");
	 * 
	 * CRMCampaignCenterEmailTemplatesPageTest emailTemplatePage=new
	 * CRMCampaignCenterEmailTemplatesPageTest();
	 * emailTemplatePage.createTemplate(driver, config,
	 * templateNameForSendEmail, emailSubjectForSendEmail,
	 * plainTextEditorForSendEmail);
	 * 
	 * fc.utobj().printTestStep(testCaseId, "Navigate To CRM > Worflows Page");
	 * fc.utobj().printTestStep(testCaseId, "Create Lead Type Standard Workflow"
	 * );
	 * 
	 * String workFlowName=dataSet.get("workFlowName"); String
	 * triggerType=dataSet.get("triggerType"); String
	 * conditionType=dataSet.get("conditionType"); String
	 * availableFields=dataSet.get("availableFields"); String
	 * taskSubject=dataSet.get("taskSubject"); String
	 * taskPriority=dataSet.get("taskPriority"); String
	 * taskStatus=dataSet.get("taskStatus"); String
	 * matchingCondition=dataSet.get("matchingCondition"); String
	 * dateOfExecution=dataSet.get("dateOfExecution");
	 * 
	 * createWorkFlowLeadDateBased(driver, config, dataSet, workFlowName,
	 * triggerType, dateOfExecution, conditionType, availableFields,
	 * matchingCondition, campaignName, leadStatus, templateNameForSendEmail,
	 * taskSubject, taskPriority, taskStatus);
	 * 
	 * fc.utobj().logoutAndQuitBrowser(driver, config, testCaseId); } catch
	 * (Exception e) {
	 * 
	 * fc.utobj().quitBrowserOnCatch(driver, config, e, testCaseId);} }
	 */

	/*
	 * @Test(groups = "crmworkflownouse")
	 * 
	 * @TestCase(createdOn="2017-05-01",updatedOn="2018-06-14",
	 * testCaseDescription =
	 * "Create The Date Based WorkFlow of BirthDay Type And Date of Execution At Same Day When Lead Is Added For All Leads"
	 * , testCaseId = "TC_52_Create_Workflow") public void
	 * createWorkFlow52()throws IOException, Exception,
	 * ParserConfigurationException, SAXException {
	 * 
	 * String testCaseId =
	 * fc.utobj().readTestCaseInfo(this.getClass().getName()+"."+new
	 * Object(){}.getClass().getEnclosingMethod().getName());
	 * 
	 * Map<String, String> config =
	 * fc.utobj().readConfigurationFile(FranconnectUtil.path);
	 * Map<String,String> dataSet = fc.utobj().readTestData("crmworkflow",
	 * testCaseId); WebDriver driver = fc.utobj().openDriver(config);
	 * 
	 * try { driver = fc.loginpage().login(driver, config);
	 * 
	 * String userName=fc.utobj().generateTestData(dataSet.get("userName"));
	 * String password=fc.utobj().generateTestData(dataSet.get("password"));
	 * AdminUsersManageCorporateUsersAddCorporateUserPageTest
	 * addcorporateUserPage=new
	 * AdminUsersManageCorporateUsersAddCorporateUserPageTest();
	 * addcorporateUserPage.addCorporateUser(driver, userName, password, config
	 * , emailId);
	 * 
	 * fc.home_page().logout(driver, config);
	 * fc.utobj().printTestStep(testCaseId, "Login With Corporate User");
	 * fc.loginpage().loginWithParameter(driver, userName, password);
	 * 
	 * fc.utobj().printTestStep(testCaseId,
	 * "Navigate To Admin > CRM > Configure Status Page");
	 * fc.utobj().printTestStep(testCaseId, "Add Lead Status"); String
	 * leadStatus=dataSet.get("leadStatus"); AdminCRMConfigureStatusPageTest
	 * statusPage=new AdminCRMConfigureStatusPageTest();
	 * statusPage.addStatus(driver, leadStatus, config);
	 * 
	 * fc.utobj().printTestStep(testCaseId, "Navigate To CRM > Campaign Center"
	 * ); fc.crm().crm_common().CRMModule(driver);
	 * 
	 * fc.utobj().printTestStep(testCaseId, "Create Campaign For Send Campaign"
	 * );
	 * 
	 * String campaignName=dataSet.get("campaignName"); String
	 * accessibility=dataSet.get("accessibility"); String
	 * campaignType=dataSet.get("campaignType"); String
	 * templateName=dataSet.get("templateName"); String
	 * emailSubject=dataSet.get("emailSubject"); String
	 * plainTextEditor=dataSet.get("plainTextEditor"); CRMCampaignCenterPageTest
	 * campaignCenterPage=new CRMCampaignCenterPageTest();
	 * campaignCenterPage.createCampaign(driver, config, campaignName,
	 * accessibility, campaignType, templateName, emailSubject,
	 * plainTextEditor);
	 * 
	 * fc.utobj().printTestStep(testCaseId, "Create Template For Send Email");
	 * 
	 * String templateNameForSendEmail=dataSet.get("templateNameForSendEmail");
	 * String emailSubjectForSendEmail=dataSet.get("emailSubjectForSendEmail");
	 * String
	 * plainTextEditorForSendEmail=dataSet.get("plainTextEditorForSendEmail");
	 * 
	 * CRMCampaignCenterEmailTemplatesPageTest emailTemplatePage=new
	 * CRMCampaignCenterEmailTemplatesPageTest();
	 * emailTemplatePage.createTemplate(driver, config,
	 * templateNameForSendEmail, emailSubjectForSendEmail,
	 * plainTextEditorForSendEmail);
	 * 
	 * fc.utobj().printTestStep(testCaseId, "Navigate To CRM > Worflows Page");
	 * fc.utobj().printTestStep(testCaseId, "Create Lead Type Standard Workflow"
	 * );
	 * 
	 * String workFlowName=dataSet.get("workFlowName"); String
	 * triggerType=dataSet.get("triggerType"); String
	 * conditionType=dataSet.get("conditionType"); String availableFields="";
	 * String taskSubject=dataSet.get("taskSubject"); String
	 * taskPriority=dataSet.get("taskPriority"); String
	 * taskStatus=dataSet.get("taskStatus"); String matchingCondition=""; String
	 * dateOfExecution=dataSet.get("dateOfExecution");
	 * 
	 * createWorkFlowLeadDateBased(driver, config, dataSet, workFlowName,
	 * triggerType, dateOfExecution, conditionType, availableFields,
	 * matchingCondition, campaignName, leadStatus, templateNameForSendEmail,
	 * taskSubject, taskPriority, taskStatus);
	 * 
	 * fc.utobj().logoutAndQuitBrowser(driver, config, testCaseId); } catch
	 * (Exception e) {
	 * 
	 * fc.utobj().quitBrowserOnCatch(driver, config, e, testCaseId);} }
	 */

	@Test(groups = "crmworkflow")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Create The Date Based WorkFlow of BirthDay Type And Date of Execution At Same Day When Contact Is Added For Contact Matching Criteria Email Field is Not Empty", testCaseId = "TC_53_Create_Workflow")
	public void createWorkFlow53() throws IOException, Exception, ParserConfigurationException, SAXException {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crmworkflow", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			/*
			 * 
			 * String
			 * userName=fc.utobj().generateTestData(dataSet.get("userName"));
			 * String
			 * password=fc.utobj().generateTestData(dataSet.get("password"));
			 * String emailId="crmautomation@staffex.com";
			 * AdminUsersManageCorporateUsersAddCorporateUserPageTest
			 * addcorporateUserPage=new
			 * AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			 * addcorporateUserPage.addCorporateUser(driver, userName, password,
			 * config , emailId);
			 * 
			 * fc.home_page().logout(driver, config);
			 * fc.utobj().printTestStep(testCaseId, "Login With Corporate User"
			 * ); fc.loginpage().loginWithParameter(driver, userName, password);
			 */

			fc.utobj().printTestStep("Navigate To Admin > CRM > Configure Status Page");
			fc.utobj().printTestStep("Add Contact Status");
			String contactStatus = dataSet.get("contactStatus");
			AdminCRMConfigureStatusPageTest statusPage = new AdminCRMConfigureStatusPageTest();
			statusPage.addStatus01(driver, contactStatus);

			fc.utobj().printTestStep("Navigate To CRM > Campaign Center");
			fc.crm().crm_common().CRMModule(driver);

			fc.utobj().printTestStep("Create Campaign For Send Campaign");

			String campaignName = dataSet.get("campaignName");
			String accessibility = dataSet.get("accessibility");
			String campaignType = dataSet.get("campaignType");
			String templateName = dataSet.get("templateName");
			String emailSubject = dataSet.get("emailSubject");
			String plainTextEditor = dataSet.get("plainTextEditor");
			CRMCampaignCenterPageTest campaignCenterPage = new CRMCampaignCenterPageTest();
			campaignCenterPage.createCampaign(driver, config, campaignName, accessibility, campaignType, templateName,
					emailSubject, plainTextEditor);

			fc.utobj().printTestStep("Create Template For Send Email");

			String templateNameForSendEmail = dataSet.get("templateNameForSendEmail");
			String emailSubjectForSendEmail = dataSet.get("emailSubjectForSendEmail");
			String plainTextEditorForSendEmail = dataSet.get("plainTextEditorForSendEmail");

			CRMCampaignCenterEmailTemplatesPageTest emailTemplatePage = new CRMCampaignCenterEmailTemplatesPageTest();
			emailTemplatePage.createTemplate(driver, config, templateNameForSendEmail, emailSubjectForSendEmail,
					plainTextEditorForSendEmail);

			fc.utobj().printTestStep("Navigate To CRM > Worflows Page");
			fc.utobj().printTestStep("Create Contact Type Date Based Workflow");

			String workFlowName = dataSet.get("workFlowName");
			String triggerType = dataSet.get("triggerType");
			String conditionType = dataSet.get("conditionType");
			String availableFields = dataSet.get("availableFields");
			String taskSubject = dataSet.get("taskSubject");
			String taskPriority = dataSet.get("taskPriority");
			String taskStatus = dataSet.get("taskStatus");
			String matchingCondition = dataSet.get("matchingCondition");
			String dateOfExecution = dataSet.get("dateOfExecution");

			createWorkFlowContactDateBased(driver, config, dataSet, workFlowName, triggerType, dateOfExecution,
					conditionType, availableFields, matchingCondition, campaignName, contactStatus,
					templateNameForSendEmail, taskSubject, taskPriority, taskStatus);

			fc.utobj().printTestStep("Verify The Created WorkFlow");
			searchWorkFLowByFilter(driver, workFlowName);

			boolean isWorkFlowNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[.='" + workFlowName + "']");
			if (isWorkFlowNamePresent == false) {
				fc.utobj().throwsException("was not able to verify the workflow");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "crmworkflow")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Create The Date Based WorkFlow of BirthDay Type And Date of Execution At Same Day When Contact Is Added For Contact Matching Criteria Status Of Contact is Default[New]", testCaseId = "TC_54_Create_Workflow")
	public void createWorkFlow54() throws IOException, Exception, ParserConfigurationException, SAXException {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crmworkflow", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			/*
			 * 
			 * String
			 * userName=fc.utobj().generateTestData(dataSet.get("userName"));
			 * String
			 * password=fc.utobj().generateTestData(dataSet.get("password"));
			 * String emailId="crmautomation@staffex.com";
			 * AdminUsersManageCorporateUsersAddCorporateUserPageTest
			 * addcorporateUserPage=new
			 * AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			 * addcorporateUserPage.addCorporateUser(driver, userName, password,
			 * config , emailId);
			 * 
			 * fc.home_page().logout(driver, config);
			 * fc.utobj().printTestStep(testCaseId, "Login With Corporate User"
			 * ); fc.loginpage().loginWithParameter(driver, userName, password);
			 */

			fc.utobj().printTestStep("Navigate To Admin > CRM > Configure Status Page");
			fc.utobj().printTestStep("Add Contact Status");
			String contactStatus = dataSet.get("contactStatus");
			AdminCRMConfigureStatusPageTest statusPage = new AdminCRMConfigureStatusPageTest();
			statusPage.addStatus01(driver, contactStatus);

			fc.utobj().printTestStep("Navigate To CRM > Campaign Center");
			fc.crm().crm_common().CRMModule(driver);

			fc.utobj().printTestStep("Create Campaign For Send Campaign");

			String campaignName = dataSet.get("campaignName");
			String accessibility = dataSet.get("accessibility");
			String campaignType = dataSet.get("campaignType");
			String templateName = dataSet.get("templateName");
			String emailSubject = dataSet.get("emailSubject");
			String plainTextEditor = dataSet.get("plainTextEditor");
			CRMCampaignCenterPageTest campaignCenterPage = new CRMCampaignCenterPageTest();
			campaignCenterPage.createCampaign(driver, config, campaignName, accessibility, campaignType, templateName,
					emailSubject, plainTextEditor);

			fc.utobj().printTestStep("Create Template For Send Email");

			String templateNameForSendEmail = dataSet.get("templateNameForSendEmail");
			String emailSubjectForSendEmail = dataSet.get("emailSubjectForSendEmail");
			String plainTextEditorForSendEmail = dataSet.get("plainTextEditorForSendEmail");

			CRMCampaignCenterEmailTemplatesPageTest emailTemplatePage = new CRMCampaignCenterEmailTemplatesPageTest();
			emailTemplatePage.createTemplate(driver, config, templateNameForSendEmail, emailSubjectForSendEmail,
					plainTextEditorForSendEmail);

			fc.utobj().printTestStep("Navigate To CRM > Worflows Page");
			fc.utobj().printTestStep("Create Contact Type Date Based Workflow");

			String workFlowName = dataSet.get("workFlowName");
			String triggerType = dataSet.get("triggerType");
			String conditionType = dataSet.get("conditionType");
			String availableFields = dataSet.get("availableFields");
			String taskSubject = dataSet.get("taskSubject");
			String taskPriority = dataSet.get("taskPriority");
			String taskStatus = dataSet.get("taskStatus");
			String matchingCondition = dataSet.get("matchingCondition");
			String dateOfExecution = dataSet.get("dateOfExecution");

			createWorkFlowContactDateBased(driver, config, dataSet, workFlowName, triggerType, dateOfExecution,
					conditionType, availableFields, matchingCondition, campaignName, contactStatus,
					templateNameForSendEmail, taskSubject, taskPriority, taskStatus);

			fc.utobj().printTestStep("Verify The Created WorkFlow");
			searchWorkFLowByFilter(driver, workFlowName);

			boolean isWorkFlowNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[.='" + workFlowName + "']");
			if (isWorkFlowNamePresent == false) {
				fc.utobj().throwsException("was not able to verify the workflow");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "crmworkflow")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Create The Date Based WorkFlow of BirthDay Type And Date of Execution At Same Day When Contact Is Added For Contact Matching Criteria Email Subscription status is Set To Opted In", testCaseId = "TC_55_Create_Workflow")
	public void createWorkFlow55() throws IOException, Exception, ParserConfigurationException, SAXException {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crmworkflow", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			/*
			 * 
			 * String
			 * userName=fc.utobj().generateTestData(dataSet.get("userName"));
			 * String
			 * password=fc.utobj().generateTestData(dataSet.get("password"));
			 * String emailId="crmautomation@staffex.com";
			 * AdminUsersManageCorporateUsersAddCorporateUserPageTest
			 * addcorporateUserPage=new
			 * AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			 * addcorporateUserPage.addCorporateUser(driver, userName, password,
			 * config , emailId);
			 * 
			 * fc.home_page().logout(driver, config);
			 * fc.utobj().printTestStep(testCaseId, "Login With Corporate User"
			 * ); fc.loginpage().loginWithParameter(driver, userName, password);
			 */

			fc.utobj().printTestStep("Navigate To Admin > CRM > Configure Status Page");
			fc.utobj().printTestStep("Add Contact Status");
			String contactStatus = dataSet.get("contactStatus");
			AdminCRMConfigureStatusPageTest statusPage = new AdminCRMConfigureStatusPageTest();
			statusPage.addStatus01(driver, contactStatus);

			fc.utobj().printTestStep("Navigate To CRM > Campaign Center");
			fc.crm().crm_common().CRMModule(driver);

			fc.utobj().printTestStep("Create Campaign For Send Campaign");

			String campaignName = dataSet.get("campaignName");
			String accessibility = dataSet.get("accessibility");
			String campaignType = dataSet.get("campaignType");
			String templateName = dataSet.get("templateName");
			String emailSubject = dataSet.get("emailSubject");
			String plainTextEditor = dataSet.get("plainTextEditor");
			CRMCampaignCenterPageTest campaignCenterPage = new CRMCampaignCenterPageTest();
			campaignCenterPage.createCampaign(driver, config, campaignName, accessibility, campaignType, templateName,
					emailSubject, plainTextEditor);

			fc.utobj().printTestStep("Create Template For Send Email");

			String templateNameForSendEmail = dataSet.get("templateNameForSendEmail");
			String emailSubjectForSendEmail = dataSet.get("emailSubjectForSendEmail");
			String plainTextEditorForSendEmail = dataSet.get("plainTextEditorForSendEmail");

			CRMCampaignCenterEmailTemplatesPageTest emailTemplatePage = new CRMCampaignCenterEmailTemplatesPageTest();
			emailTemplatePage.createTemplate(driver, config, templateNameForSendEmail, emailSubjectForSendEmail,
					plainTextEditorForSendEmail);

			fc.utobj().printTestStep("Navigate To CRM > Worflows Page");
			fc.utobj().printTestStep("Create Contact Type Date Based Workflow");

			String workFlowName = dataSet.get("workFlowName");
			String triggerType = dataSet.get("triggerType");
			String conditionType = dataSet.get("conditionType");
			String availableFields = dataSet.get("availableFields");
			String taskSubject = dataSet.get("taskSubject");
			String taskPriority = dataSet.get("taskPriority");
			String taskStatus = dataSet.get("taskStatus");
			String matchingCondition = dataSet.get("matchingCondition");
			String dateOfExecution = dataSet.get("dateOfExecution");

			createWorkFlowContactDateBased(driver, config, dataSet, workFlowName, triggerType, dateOfExecution,
					conditionType, availableFields, matchingCondition, campaignName, contactStatus,
					templateNameForSendEmail, taskSubject, taskPriority, taskStatus);

			fc.utobj().printTestStep("Verify The Created WorkFlow");
			searchWorkFLowByFilter(driver, workFlowName);

			boolean isWorkFlowNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[.='" + workFlowName + "']");
			if (isWorkFlowNamePresent == false) {
				fc.utobj().throwsException("was not able to verify the workflow");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "crmworkflow")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Create The Date Based WorkFlow of BirthDay Type And Date of Execution At Same Day When Contact Is Added For Contact Matching Criteria Country And All States of It", testCaseId = "TC_56_Create_Workflow")
	public void createWorkFlow56() throws IOException, Exception, ParserConfigurationException, SAXException {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crmworkflow", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			/*
			 * 
			 * String
			 * userName=fc.utobj().generateTestData(dataSet.get("userName"));
			 * String
			 * password=fc.utobj().generateTestData(dataSet.get("password"));
			 * String emailId="crmautomation@staffex.com";
			 * AdminUsersManageCorporateUsersAddCorporateUserPageTest
			 * addcorporateUserPage=new
			 * AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			 * addcorporateUserPage.addCorporateUser(driver, userName, password,
			 * config , emailId);
			 * 
			 * fc.home_page().logout(driver, config);
			 * fc.utobj().printTestStep(testCaseId, "Login With Corporate User"
			 * ); fc.loginpage().loginWithParameter(driver, userName, password);
			 */

			fc.utobj().printTestStep("Navigate To Admin > CRM > Configure Status Page");
			fc.utobj().printTestStep("Add Contact Status");
			String contactStatus = dataSet.get("contactStatus");
			AdminCRMConfigureStatusPageTest statusPage = new AdminCRMConfigureStatusPageTest();
			statusPage.addStatus01(driver, contactStatus);

			fc.utobj().printTestStep("Navigate To CRM > Campaign Center");
			fc.crm().crm_common().CRMModule(driver);

			fc.utobj().printTestStep("Create Campaign For Send Campaign");

			String campaignName = dataSet.get("campaignName");
			String accessibility = dataSet.get("accessibility");
			String campaignType = dataSet.get("campaignType");
			String templateName = dataSet.get("templateName");
			String emailSubject = dataSet.get("emailSubject");
			String plainTextEditor = dataSet.get("plainTextEditor");
			CRMCampaignCenterPageTest campaignCenterPage = new CRMCampaignCenterPageTest();
			campaignCenterPage.createCampaign(driver, config, campaignName, accessibility, campaignType, templateName,
					emailSubject, plainTextEditor);

			fc.utobj().printTestStep("Create Template For Send Email");

			String templateNameForSendEmail = dataSet.get("templateNameForSendEmail");
			String emailSubjectForSendEmail = dataSet.get("emailSubjectForSendEmail");
			String plainTextEditorForSendEmail = dataSet.get("plainTextEditorForSendEmail");

			CRMCampaignCenterEmailTemplatesPageTest emailTemplatePage = new CRMCampaignCenterEmailTemplatesPageTest();
			emailTemplatePage.createTemplate(driver, config, templateNameForSendEmail, emailSubjectForSendEmail,
					plainTextEditorForSendEmail);

			fc.utobj().printTestStep("Navigate To CRM > Worflows Page");
			fc.utobj().printTestStep("Create Contact Type Date Based Workflow");

			String workFlowName = dataSet.get("workFlowName");
			String triggerType = dataSet.get("triggerType");
			String conditionType = dataSet.get("conditionType");
			String availableFields = dataSet.get("availableFields");
			String taskSubject = dataSet.get("taskSubject");
			String taskPriority = dataSet.get("taskPriority");
			String taskStatus = dataSet.get("taskStatus");
			String matchingCondition = dataSet.get("matchingCondition");
			String dateOfExecution = dataSet.get("dateOfExecution");

			createWorkFlowContactDateBased(driver, config, dataSet, workFlowName, triggerType, dateOfExecution,
					conditionType, availableFields, matchingCondition, campaignName, contactStatus,
					templateNameForSendEmail, taskSubject, taskPriority, taskStatus);

			fc.utobj().printTestStep("Verify The Created WorkFlow");
			searchWorkFLowByFilter(driver, workFlowName);

			boolean isWorkFlowNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[.='" + workFlowName + "']");
			if (isWorkFlowNamePresent == false) {
				fc.utobj().throwsException("was not able to verify the workflow");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "crmworkflow")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Create The Date Based WorkFlow of BirthDay Type And Date of Execution At Same Day When Contact Is Added For Contact Matching Criteria BirthDate And Set To It , is Between", testCaseId = "TC_57_Create_Workflow")
	public void createWorkFlow57() throws IOException, Exception, ParserConfigurationException, SAXException {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crmworkflow", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			/*
			 * 
			 * String
			 * userName=fc.utobj().generateTestData(dataSet.get("userName"));
			 * String
			 * password=fc.utobj().generateTestData(dataSet.get("password"));
			 * String emailId="crmautomation@staffex.com";
			 * AdminUsersManageCorporateUsersAddCorporateUserPageTest
			 * addcorporateUserPage=new
			 * AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			 * addcorporateUserPage.addCorporateUser(driver, userName, password,
			 * config , emailId);
			 * 
			 * fc.home_page().logout(driver, config);
			 * fc.utobj().printTestStep(testCaseId, "Login With Corporate User"
			 * ); fc.loginpage().loginWithParameter(driver, userName, password);
			 */

			fc.utobj().printTestStep("Navigate To Admin > CRM > Configure Status Page");
			fc.utobj().printTestStep("Add Contact Status");
			String contactStatus = dataSet.get("contactStatus");
			AdminCRMConfigureStatusPageTest statusPage = new AdminCRMConfigureStatusPageTest();
			statusPage.addStatus01(driver, contactStatus);

			fc.utobj().printTestStep("Navigate To CRM > Campaign Center");
			fc.crm().crm_common().CRMModule(driver);

			fc.utobj().printTestStep("Create Campaign For Send Campaign");

			String campaignName = dataSet.get("campaignName");
			String accessibility = dataSet.get("accessibility");
			String campaignType = dataSet.get("campaignType");
			String templateName = dataSet.get("templateName");
			String emailSubject = dataSet.get("emailSubject");
			String plainTextEditor = dataSet.get("plainTextEditor");
			CRMCampaignCenterPageTest campaignCenterPage = new CRMCampaignCenterPageTest();
			campaignCenterPage.createCampaign(driver, config, campaignName, accessibility, campaignType, templateName,
					emailSubject, plainTextEditor);

			fc.utobj().printTestStep("Create Template For Send Email");

			String templateNameForSendEmail = dataSet.get("templateNameForSendEmail");
			String emailSubjectForSendEmail = dataSet.get("emailSubjectForSendEmail");
			String plainTextEditorForSendEmail = dataSet.get("plainTextEditorForSendEmail");

			CRMCampaignCenterEmailTemplatesPageTest emailTemplatePage = new CRMCampaignCenterEmailTemplatesPageTest();
			emailTemplatePage.createTemplate(driver, config, templateNameForSendEmail, emailSubjectForSendEmail,
					plainTextEditorForSendEmail);

			fc.utobj().printTestStep("Navigate To CRM > Worflows Page");
			fc.utobj().printTestStep("Create Contact Type Date Based Workflow");

			String workFlowName = dataSet.get("workFlowName");
			String triggerType = dataSet.get("triggerType");
			String conditionType = dataSet.get("conditionType");
			String availableFields = dataSet.get("availableFields");
			String taskSubject = dataSet.get("taskSubject");
			String taskPriority = dataSet.get("taskPriority");
			String taskStatus = dataSet.get("taskStatus");
			String matchingCondition = dataSet.get("matchingCondition");
			String dateOfExecution = dataSet.get("dateOfExecution");

			createWorkFlowContactDateBased(driver, config, dataSet, workFlowName, triggerType, dateOfExecution,
					conditionType, availableFields, matchingCondition, campaignName, contactStatus,
					templateNameForSendEmail, taskSubject, taskPriority, taskStatus);

			fc.utobj().printTestStep("Verify The Created WorkFlow");
			searchWorkFLowByFilter(driver, workFlowName);

			boolean isWorkFlowNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[.='" + workFlowName + "']");
			if (isWorkFlowNamePresent == false) {
				fc.utobj().throwsException("was not able to verify the workflow");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	/*
	 * @Test(groups = "crmworkflow")
	 * 
	 * @TestCase(createdOn="2017-05-01",updatedOn="2018-06-14",
	 * testCaseDescription =
	 * "Create The Date Based WorkFlow of BirthDay Type And Date of Execution At Same Day When Contact Is Added For Contact Matching Criteria Status Of Contact Is Customized"
	 * , testCaseId = "TC_58_Create_Workflow") public void
	 * createWorkFlow58()throws IOException, Exception,
	 * ParserConfigurationException, SAXException {
	 * 
	 * String testCaseId =
	 * fc.utobj().readTestCaseInfo(this.getClass().getName()+"."+new
	 * Object(){}.getClass().getEnclosingMethod().getName());
	 * 
	 * Map<String, String> config =
	 * fc.utobj().readConfigurationFile(FranconnectUtil.path);
	 * Map<String,String> dataSet = fc.utobj().readTestData("crmworkflow",
	 * testCaseId); WebDriver driver = fc.utobj().openDriver(config);
	 * 
	 * try { driver = fc.loginpage().login(driver, config);
	 * 
	 * String userName=fc.utobj().generateTestData(dataSet.get("userName"));
	 * String password=fc.utobj().generateTestData(dataSet.get("password"));
	 * AdminUsersManageCorporateUsersAddCorporateUserPageTest
	 * addcorporateUserPage=new
	 * AdminUsersManageCorporateUsersAddCorporateUserPageTest();
	 * addcorporateUserPage.addCorporateUser(driver, userName, password, config
	 * , emailId);
	 * 
	 * fc.home_page().logout(driver, config);
	 * fc.utobj().printTestStep(testCaseId, "Login With Corporate User");
	 * fc.loginpage().loginWithParameter(driver, userName, password);
	 * 
	 * fc.utobj().printTestStep(testCaseId,
	 * "Navigate To Admin > CRM > Configure Status Page");
	 * fc.utobj().printTestStep(testCaseId, "Add Contact Status"); String
	 * contactStatus=dataSet.get("contactStatus");
	 * AdminCRMConfigureStatusPageTest statusPage=new
	 * AdminCRMConfigureStatusPageTest(); statusPage.addStatus01(driver,
	 * contactStatus, config);
	 * 
	 * 
	 * fc.utobj().printTestStep(testCaseId,
	 * "Navigate To Admin > CRM > Configure Status Page");
	 * fc.utobj().printTestStep(testCaseId, "Add Contact Status"); String
	 * contactStatusCustomize=dataSet.get("selectContactStatus");
	 * statusPage.addStatus01(driver, contactStatusCustomize, config);
	 * 
	 * fc.utobj().printTestStep(testCaseId, "Navigate To CRM > Campaign Center"
	 * ); fc.crm().crm_common().CRMModule(driver);
	 * 
	 * fc.utobj().printTestStep(testCaseId, "Create Campaign For Send Campaign"
	 * );
	 * 
	 * String campaignName=dataSet.get("campaignName"); String
	 * accessibility=dataSet.get("accessibility"); String
	 * campaignType=dataSet.get("campaignType"); String
	 * templateName=dataSet.get("templateName"); String
	 * emailSubject=dataSet.get("emailSubject"); String
	 * plainTextEditor=dataSet.get("plainTextEditor"); CRMCampaignCenterPageTest
	 * campaignCenterPage=new CRMCampaignCenterPageTest();
	 * campaignCenterPage.createCampaign(driver, config, campaignName,
	 * accessibility, campaignType, templateName, emailSubject,
	 * plainTextEditor);
	 * 
	 * fc.utobj().printTestStep(testCaseId, "Create Template For Send Email");
	 * 
	 * String templateNameForSendEmail=dataSet.get("templateNameForSendEmail");
	 * String emailSubjectForSendEmail=dataSet.get("emailSubjectForSendEmail");
	 * String
	 * plainTextEditorForSendEmail=dataSet.get("plainTextEditorForSendEmail");
	 * 
	 * CRMCampaignCenterEmailTemplatesPageTest emailTemplatePage=new
	 * CRMCampaignCenterEmailTemplatesPageTest();
	 * emailTemplatePage.createTemplate(driver, config,
	 * templateNameForSendEmail, emailSubjectForSendEmail,
	 * plainTextEditorForSendEmail);
	 * 
	 * fc.utobj().printTestStep(testCaseId, "Navigate To CRM > Worflows Page");
	 * fc.utobj().printTestStep(testCaseId,
	 * "Create Contact Type Standard Workflow");
	 * 
	 * String workFlowName=dataSet.get("workFlowName"); String
	 * triggerType=dataSet.get("triggerType"); String
	 * conditionType=dataSet.get("conditionType"); String
	 * availableFields=dataSet.get("availableFields"); String
	 * taskSubject=dataSet.get("taskSubject"); String
	 * taskPriority=dataSet.get("taskPriority"); String
	 * taskStatus=dataSet.get("taskStatus"); String
	 * matchingCondition=dataSet.get("matchingCondition"); String
	 * dateOfExecution=dataSet.get("dateOfExecution");
	 * 
	 * createWorkFlowContactDateBased(driver, config, dataSet, workFlowName,
	 * triggerType, dateOfExecution, conditionType, availableFields,
	 * matchingCondition, campaignName, contactStatus, templateNameForSendEmail,
	 * taskSubject, taskPriority, taskStatus);
	 * 
	 * 
	 * fc.utobj().logoutAndQuitBrowser(driver, config, testCaseId); } catch
	 * (Exception e) {
	 * 
	 * fc.utobj().quitBrowserOnCatch(driver, config, e, testCaseId);} }
	 */

	@Test(groups = "crmworkflow")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Create The Date Based WorkFlow of BirthDay Type And Date of Execution At Same Day When Contact Is Added For Contact Matching Criteria Contact Type In", testCaseId = "TC_59_Create_Workflow")
	public void createWorkFlow59() throws IOException, Exception, ParserConfigurationException, SAXException {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crmworkflow", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			/*
			 * 
			 * String
			 * userName=fc.utobj().generateTestData(dataSet.get("userName"));
			 * String
			 * password=fc.utobj().generateTestData(dataSet.get("password"));
			 * String emailId="crmautomation@staffex.com";
			 * AdminUsersManageCorporateUsersAddCorporateUserPageTest
			 * addcorporateUserPage=new
			 * AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			 * addcorporateUserPage.addCorporateUser(driver, userName, password,
			 * config , emailId);
			 * 
			 * fc.home_page().logout(driver, config);
			 * fc.utobj().printTestStep(testCaseId, "Login With Corporate User"
			 * ); fc.loginpage().loginWithParameter(driver, userName, password);
			 */

			fc.utobj().printTestStep("Navigate To Admin > CRM > Configure Status Page");
			fc.utobj().printTestStep("Add Contact Status");
			String contactStatus = dataSet.get("contactStatus");
			AdminCRMConfigureStatusPageTest statusPage = new AdminCRMConfigureStatusPageTest();
			statusPage.addStatus01(driver, contactStatus);

			fc.utobj().printTestStep("Navigate To Admin > CRM > Contact Type Configureation");
			fc.utobj().printTestStep("Add Contact Type");
			String contactType = dataSet.get("selectcontactType");
			AdminCRMContactTypeConfigurationPageTest contactTypeConfigureationPage = new AdminCRMContactTypeConfigurationPageTest();
			contactTypeConfigureationPage.addContactType(driver, contactType);

			fc.utobj().printTestStep("Navigate To CRM > Campaign Center");
			fc.crm().crm_common().CRMModule(driver);

			fc.utobj().printTestStep("Create Campaign For Send Campaign");

			String campaignName = dataSet.get("campaignName");
			String accessibility = dataSet.get("accessibility");
			String campaignType = dataSet.get("campaignType");
			String templateName = dataSet.get("templateName");
			String emailSubject = dataSet.get("emailSubject");
			String plainTextEditor = dataSet.get("plainTextEditor");
			CRMCampaignCenterPageTest campaignCenterPage = new CRMCampaignCenterPageTest();
			campaignCenterPage.createCampaign(driver, config, campaignName, accessibility, campaignType, templateName,
					emailSubject, plainTextEditor);

			fc.utobj().printTestStep("Create Template For Send Email");

			String templateNameForSendEmail = dataSet.get("templateNameForSendEmail");
			String emailSubjectForSendEmail = dataSet.get("emailSubjectForSendEmail");
			String plainTextEditorForSendEmail = dataSet.get("plainTextEditorForSendEmail");

			CRMCampaignCenterEmailTemplatesPageTest emailTemplatePage = new CRMCampaignCenterEmailTemplatesPageTest();
			emailTemplatePage.createTemplate(driver, config, templateNameForSendEmail, emailSubjectForSendEmail,
					plainTextEditorForSendEmail);

			fc.utobj().printTestStep("Navigate To CRM > Worflows Page");
			fc.utobj().printTestStep("Create Contact Type Date Based Workflow");

			String workFlowName = dataSet.get("workFlowName");
			String triggerType = dataSet.get("triggerType");
			String conditionType = dataSet.get("conditionType");
			String availableFields = dataSet.get("availableFields");
			String taskSubject = dataSet.get("taskSubject");
			String taskPriority = dataSet.get("taskPriority");
			String taskStatus = dataSet.get("taskStatus");
			String matchingCondition = dataSet.get("matchingCondition");
			String dateOfExecution = dataSet.get("dateOfExecution");

			createWorkFlowContactDateBased(driver, config, dataSet, workFlowName, triggerType, dateOfExecution,
					conditionType, availableFields, matchingCondition, campaignName, contactStatus,
					templateNameForSendEmail, taskSubject, taskPriority, taskStatus);

			fc.utobj().printTestStep("Verify The Created WorkFlow");
			searchWorkFLowByFilter(driver, workFlowName);

			boolean isWorkFlowNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[.='" + workFlowName + "']");
			if (isWorkFlowNamePresent == false) {
				fc.utobj().throwsException("was not able to verify the workflow");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	/*
	 * @Test(groups = "crmworkflownouse")
	 * 
	 * @TestCase(createdOn="2017-05-01",updatedOn="2018-06-14",
	 * testCaseDescription =
	 * "Create The Date Based WorkFlow of BirthDay Type And Date of Execution At Same Day When Contact Is Added For All Contacts"
	 * , testCaseId = "TC_60_Create_Workflow") public void
	 * createWorkFlow60()throws IOException, Exception,
	 * ParserConfigurationException, SAXException {
	 * 
	 * String testCaseId =
	 * fc.utobj().readTestCaseInfo(this.getClass().getName()+"."+new
	 * Object(){}.getClass().getEnclosingMethod().getName());
	 * 
	 * Map<String, String> config =
	 * fc.utobj().readConfigurationFile(FranconnectUtil.path);
	 * Map<String,String> dataSet = fc.utobj().readTestData("crmworkflow",
	 * testCaseId); WebDriver driver = fc.utobj().openDriver(config);
	 * 
	 * try { driver = fc.loginpage().login(driver, config);
	 * 
	 * String userName=fc.utobj().generateTestData(dataSet.get("userName"));
	 * String password=fc.utobj().generateTestData(dataSet.get("password"));
	 * AdminUsersManageCorporateUsersAddCorporateUserPageTest
	 * addcorporateUserPage=new
	 * AdminUsersManageCorporateUsersAddCorporateUserPageTest();
	 * addcorporateUserPage.addCorporateUser(driver, userName, password, config
	 * , emailId);
	 * 
	 * fc.home_page().logout(driver, config);
	 * fc.utobj().printTestStep(testCaseId, "Login With Corporate User");
	 * fc.loginpage().loginWithParameter(driver, userName, password);
	 * 
	 * fc.utobj().printTestStep(testCaseId,
	 * "Navigate To Admin > CRM > Configure Status Page");
	 * fc.utobj().printTestStep(testCaseId, "Add Contact Status"); String
	 * contactStatus=dataSet.get("contactStatus");
	 * AdminCRMConfigureStatusPageTest statusPage=new
	 * AdminCRMConfigureStatusPageTest(); statusPage.addStatus01(driver,
	 * contactStatus, config);
	 * 
	 * fc.utobj().printTestStep(testCaseId, "Navigate To CRM > Campaign Center"
	 * ); fc.crm().crm_common().CRMModule(driver);
	 * 
	 * fc.utobj().printTestStep(testCaseId, "Create Campaign For Send Campaign"
	 * );
	 * 
	 * String campaignName=dataSet.get("campaignName"); String
	 * accessibility=dataSet.get("accessibility"); String
	 * campaignType=dataSet.get("campaignType"); String
	 * templateName=dataSet.get("templateName"); String
	 * emailSubject=dataSet.get("emailSubject"); String
	 * plainTextEditor=dataSet.get("plainTextEditor"); CRMCampaignCenterPageTest
	 * campaignCenterPage=new CRMCampaignCenterPageTest();
	 * campaignCenterPage.createCampaign(driver, config, campaignName,
	 * accessibility, campaignType, templateName, emailSubject,
	 * plainTextEditor);
	 * 
	 * fc.utobj().printTestStep(testCaseId, "Create Template For Send Email");
	 * 
	 * String templateNameForSendEmail=dataSet.get("templateNameForSendEmail");
	 * String emailSubjectForSendEmail=dataSet.get("emailSubjectForSendEmail");
	 * String
	 * plainTextEditorForSendEmail=dataSet.get("plainTextEditorForSendEmail");
	 * 
	 * CRMCampaignCenterEmailTemplatesPageTest emailTemplatePage=new
	 * CRMCampaignCenterEmailTemplatesPageTest();
	 * emailTemplatePage.createTemplate(driver, config,
	 * templateNameForSendEmail, emailSubjectForSendEmail,
	 * plainTextEditorForSendEmail);
	 * 
	 * fc.utobj().printTestStep(testCaseId, "Navigate To CRM > Worflows Page");
	 * fc.utobj().printTestStep(testCaseId,
	 * "Create Contact Type Standard Workflow");
	 * 
	 * String workFlowName=dataSet.get("workFlowName"); String
	 * triggerType=dataSet.get("triggerType"); String
	 * conditionType=dataSet.get("conditionType"); String availableFields="";
	 * String taskSubject=dataSet.get("taskSubject"); String
	 * taskPriority=dataSet.get("taskPriority"); String
	 * taskStatus=dataSet.get("taskStatus"); String matchingCondition=""; String
	 * dateOfExecution=dataSet.get("dateOfExecution");
	 * 
	 * createWorkFlowContactDateBased(driver, config, dataSet, workFlowName,
	 * triggerType, dateOfExecution, conditionType, availableFields,
	 * matchingCondition, campaignName, contactStatus, templateNameForSendEmail,
	 * taskSubject, taskPriority, taskStatus);
	 * 
	 * 
	 * fc.utobj().logoutAndQuitBrowser(driver, config, testCaseId); } catch
	 * (Exception e) {
	 * 
	 * fc.utobj().quitBrowserOnCatch(driver, config, e, testCaseId);} }
	 */

	@Test(groups = "crmworkflow")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Create The Date Based WorkFlow of Anniversary Type And Date of Execution At After Days When Lead Is Added For Leads Matching Criteria Email Field is Not Empty", testCaseId = "TC_61_Create_Workflow")
	public void createWorkFlow61() throws IOException, Exception, ParserConfigurationException, SAXException {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crmworkflow", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			/*
			 * 
			 * String
			 * userName=fc.utobj().generateTestData(dataSet.get("userName"));
			 * String
			 * password=fc.utobj().generateTestData(dataSet.get("password"));
			 * String emailId="crmautomation@staffex.com";
			 * AdminUsersManageCorporateUsersAddCorporateUserPageTest
			 * addcorporateUserPage=new
			 * AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			 * addcorporateUserPage.addCorporateUser(driver, userName, password,
			 * config , emailId);
			 * 
			 * fc.home_page().logout(driver, config);
			 * fc.utobj().printTestStep(testCaseId, "Login With Corporate User"
			 * ); fc.loginpage().loginWithParameter(driver, userName, password);
			 */

			fc.utobj().printTestStep("Navigate To Admin > CRM > Configure Status Page");
			fc.utobj().printTestStep("Add Lead Status");
			String leadStatus = dataSet.get("leadStatus");
			AdminCRMConfigureStatusPageTest statusPage = new AdminCRMConfigureStatusPageTest();
			statusPage.addStatus(driver, leadStatus);

			fc.utobj().printTestStep("Navigate To CRM > Campaign Center");
			fc.crm().crm_common().CRMModule(driver);

			fc.utobj().printTestStep("Create Campaign For Send Campaign");

			String campaignName = dataSet.get("campaignName");
			String accessibility = dataSet.get("accessibility");
			String campaignType = dataSet.get("campaignType");
			String templateName = dataSet.get("templateName");
			String emailSubject = dataSet.get("emailSubject");
			String plainTextEditor = dataSet.get("plainTextEditor");
			CRMCampaignCenterPageTest campaignCenterPage = new CRMCampaignCenterPageTest();
			campaignCenterPage.createCampaign(driver, config, campaignName, accessibility, campaignType, templateName,
					emailSubject, plainTextEditor);

			fc.utobj().printTestStep("Create Template For Send Email");

			String templateNameForSendEmail = dataSet.get("templateNameForSendEmail");
			String emailSubjectForSendEmail = dataSet.get("emailSubjectForSendEmail");
			String plainTextEditorForSendEmail = dataSet.get("plainTextEditorForSendEmail");

			CRMCampaignCenterEmailTemplatesPageTest emailTemplatePage = new CRMCampaignCenterEmailTemplatesPageTest();
			emailTemplatePage.createTemplate(driver, config, templateNameForSendEmail, emailSubjectForSendEmail,
					plainTextEditorForSendEmail);

			fc.utobj().printTestStep("Navigate To CRM > Worflows Page");
			fc.utobj().printTestStep("Create Lead Type Date Based Workflow");

			String workFlowName = dataSet.get("workFlowName");
			String triggerType = dataSet.get("triggerType");
			String conditionType = dataSet.get("conditionType");
			String availableFields = dataSet.get("availableFields");
			String taskSubject = dataSet.get("taskSubject");
			String taskPriority = dataSet.get("taskPriority");
			String taskStatus = dataSet.get("taskStatus");
			String matchingCondition = dataSet.get("matchingCondition");
			String dateOfExecution = dataSet.get("dateOfExecution");

			createWorkFlowLeadDateBased(driver, config, dataSet, workFlowName, triggerType, dateOfExecution,
					conditionType, availableFields, matchingCondition, campaignName, leadStatus,
					templateNameForSendEmail, taskSubject, taskPriority, taskStatus);

			fc.utobj().printTestStep("Verify The Created WorkFlow");
			searchWorkFLowByFilter(driver, workFlowName);

			boolean isWorkFlowNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[.='" + workFlowName + "']");
			if (isWorkFlowNamePresent == false) {
				fc.utobj().throwsException("was not able to verify the workflow");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "crmworkflow")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Create The Date Based WorkFlow of Anniversary Type And Date of Execution At After Days When Lead is Added For Leads Matching Criteria Status OF Lead Is Default[New]", testCaseId = "TC_62_Create_Workflow")
	public void createWorkFlow62() throws IOException, Exception, ParserConfigurationException, SAXException {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crmworkflow", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			/*
			 * 
			 * String
			 * userName=fc.utobj().generateTestData(dataSet.get("userName"));
			 * String
			 * password=fc.utobj().generateTestData(dataSet.get("password"));
			 * String emailId="crmautomation@staffex.com";
			 * AdminUsersManageCorporateUsersAddCorporateUserPageTest
			 * addcorporateUserPage=new
			 * AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			 * addcorporateUserPage.addCorporateUser(driver, userName, password,
			 * config , emailId);
			 * 
			 * fc.home_page().logout(driver, config);
			 * fc.utobj().printTestStep(testCaseId, "Login With Corporate User"
			 * ); fc.loginpage().loginWithParameter(driver, userName, password);
			 */

			fc.utobj().printTestStep("Navigate To Admin > CRM > Configure Status Page");
			fc.utobj().printTestStep("Add Lead Status");
			String leadStatus = dataSet.get("leadStatus");
			AdminCRMConfigureStatusPageTest statusPage = new AdminCRMConfigureStatusPageTest();
			statusPage.addStatus(driver, leadStatus);

			fc.utobj().printTestStep("Navigate To CRM > Campaign Center");
			fc.crm().crm_common().CRMModule(driver);

			fc.utobj().printTestStep("Create Campaign For Send Campaign");

			String campaignName = dataSet.get("campaignName");
			String accessibility = dataSet.get("accessibility");
			String campaignType = dataSet.get("campaignType");
			String templateName = dataSet.get("templateName");
			String emailSubject = dataSet.get("emailSubject");
			String plainTextEditor = dataSet.get("plainTextEditor");
			CRMCampaignCenterPageTest campaignCenterPage = new CRMCampaignCenterPageTest();
			campaignCenterPage.createCampaign(driver, config, campaignName, accessibility, campaignType, templateName,
					emailSubject, plainTextEditor);

			fc.utobj().printTestStep("Create Template For Send Email");

			String templateNameForSendEmail = dataSet.get("templateNameForSendEmail");
			String emailSubjectForSendEmail = dataSet.get("emailSubjectForSendEmail");
			String plainTextEditorForSendEmail = dataSet.get("plainTextEditorForSendEmail");

			CRMCampaignCenterEmailTemplatesPageTest emailTemplatePage = new CRMCampaignCenterEmailTemplatesPageTest();
			emailTemplatePage.createTemplate(driver, config, templateNameForSendEmail, emailSubjectForSendEmail,
					plainTextEditorForSendEmail);

			fc.utobj().printTestStep("Navigate To CRM > Worflows Page");
			fc.utobj().printTestStep("Create Lead Type Date Based Workflow");

			String workFlowName = dataSet.get("workFlowName");
			String triggerType = dataSet.get("triggerType");
			String conditionType = dataSet.get("conditionType");
			String availableFields = dataSet.get("availableFields");
			String taskSubject = dataSet.get("taskSubject");
			String taskPriority = dataSet.get("taskPriority");
			String taskStatus = dataSet.get("taskStatus");
			String matchingCondition = dataSet.get("matchingCondition");
			String dateOfExecution = dataSet.get("dateOfExecution");

			createWorkFlowLeadDateBased(driver, config, dataSet, workFlowName, triggerType, dateOfExecution,
					conditionType, availableFields, matchingCondition, campaignName, leadStatus,
					templateNameForSendEmail, taskSubject, taskPriority, taskStatus);

			fc.utobj().printTestStep("Verify The Created WorkFlow");
			searchWorkFLowByFilter(driver, workFlowName);

			boolean isWorkFlowNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[.='" + workFlowName + "']");
			if (isWorkFlowNamePresent == false) {
				fc.utobj().throwsException("was not able to verify the workflow");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "crmworkflow")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Create The Date Based WorkFlow of Anniversary Type And Date of Execution At After Days When Lead is Added For Leads Matching Criteria Email Subscription status is Set To Opted In", testCaseId = "TC_63_Create_Workflow")
	public void createWorkFlow63() throws IOException, Exception, ParserConfigurationException, SAXException {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crmworkflow", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			/*
			 * 
			 * String
			 * userName=fc.utobj().generateTestData(dataSet.get("userName"));
			 * String
			 * password=fc.utobj().generateTestData(dataSet.get("password"));
			 * String emailId="crmautomation@staffex.com";
			 * AdminUsersManageCorporateUsersAddCorporateUserPageTest
			 * addcorporateUserPage=new
			 * AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			 * addcorporateUserPage.addCorporateUser(driver, userName, password,
			 * config , emailId);
			 * 
			 * fc.home_page().logout(driver, config);
			 * fc.utobj().printTestStep(testCaseId, "Login With Corporate User"
			 * ); fc.loginpage().loginWithParameter(driver, userName, password);
			 */

			fc.utobj().printTestStep("Navigate To Admin > CRM > Configure Status Page");
			fc.utobj().printTestStep("Add Lead Status");
			String leadStatus = dataSet.get("leadStatus");
			AdminCRMConfigureStatusPageTest statusPage = new AdminCRMConfigureStatusPageTest();
			statusPage.addStatus(driver, leadStatus);

			fc.utobj().printTestStep("Navigate To CRM > Campaign Center");
			fc.crm().crm_common().CRMModule(driver);

			fc.utobj().printTestStep("Create Campaign For Send Campaign");

			String campaignName = dataSet.get("campaignName");
			String accessibility = dataSet.get("accessibility");
			String campaignType = dataSet.get("campaignType");
			String templateName = dataSet.get("templateName");
			String emailSubject = dataSet.get("emailSubject");
			String plainTextEditor = dataSet.get("plainTextEditor");
			CRMCampaignCenterPageTest campaignCenterPage = new CRMCampaignCenterPageTest();
			campaignCenterPage.createCampaign(driver, config, campaignName, accessibility, campaignType, templateName,
					emailSubject, plainTextEditor);

			fc.utobj().printTestStep("Create Template For Send Email");

			String templateNameForSendEmail = dataSet.get("templateNameForSendEmail");
			String emailSubjectForSendEmail = dataSet.get("emailSubjectForSendEmail");
			String plainTextEditorForSendEmail = dataSet.get("plainTextEditorForSendEmail");

			CRMCampaignCenterEmailTemplatesPageTest emailTemplatePage = new CRMCampaignCenterEmailTemplatesPageTest();
			emailTemplatePage.createTemplate(driver, config, templateNameForSendEmail, emailSubjectForSendEmail,
					plainTextEditorForSendEmail);

			fc.utobj().printTestStep("Navigate To CRM > Worflows Page");
			fc.utobj().printTestStep("Create Lead Type Date Based Workflow");

			String workFlowName = dataSet.get("workFlowName");
			String triggerType = dataSet.get("triggerType");
			String conditionType = dataSet.get("conditionType");
			String availableFields = dataSet.get("availableFields");
			String taskSubject = dataSet.get("taskSubject");
			String taskPriority = dataSet.get("taskPriority");
			String taskStatus = dataSet.get("taskStatus");
			String matchingCondition = dataSet.get("matchingCondition");
			String dateOfExecution = dataSet.get("dateOfExecution");

			createWorkFlowLeadDateBased(driver, config, dataSet, workFlowName, triggerType, dateOfExecution,
					conditionType, availableFields, matchingCondition, campaignName, leadStatus,
					templateNameForSendEmail, taskSubject, taskPriority, taskStatus);

			fc.utobj().printTestStep("Verify The Created WorkFlow");
			searchWorkFLowByFilter(driver, workFlowName);

			boolean isWorkFlowNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[.='" + workFlowName + "']");
			if (isWorkFlowNamePresent == false) {
				fc.utobj().throwsException("was not able to verify the workflow");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "crmworkflow")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Create The Date Based WorkFlow of Anniversary Type And Date of Execution At After Days When Lead is Added For Leads Matching Criteria Country And All States of It", testCaseId = "TC_64_Create_Workflow")
	public void createWorkFlow64() throws IOException, Exception, ParserConfigurationException, SAXException {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crmworkflow", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			/*
			 * 
			 * String
			 * userName=fc.utobj().generateTestData(dataSet.get("userName"));
			 * String
			 * password=fc.utobj().generateTestData(dataSet.get("password"));
			 * String emailId="crmautomation@staffex.com";
			 * AdminUsersManageCorporateUsersAddCorporateUserPageTest
			 * addcorporateUserPage=new
			 * AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			 * addcorporateUserPage.addCorporateUser(driver, userName, password,
			 * config , emailId);
			 * 
			 * fc.home_page().logout(driver, config);
			 * fc.utobj().printTestStep(testCaseId, "Login With Corporate User"
			 * ); fc.loginpage().loginWithParameter(driver, userName, password);
			 */

			fc.utobj().printTestStep("Navigate To Admin > CRM > Configure Status Page");
			fc.utobj().printTestStep("Add Lead Status");
			String leadStatus = dataSet.get("leadStatus");
			AdminCRMConfigureStatusPageTest statusPage = new AdminCRMConfigureStatusPageTest();
			statusPage.addStatus(driver, leadStatus);

			fc.utobj().printTestStep("Navigate To CRM > Campaign Center");
			fc.crm().crm_common().CRMModule(driver);

			fc.utobj().printTestStep("Create Campaign For Send Campaign");

			String campaignName = dataSet.get("campaignName");
			String accessibility = dataSet.get("accessibility");
			String campaignType = dataSet.get("campaignType");
			String templateName = dataSet.get("templateName");
			String emailSubject = dataSet.get("emailSubject");
			String plainTextEditor = dataSet.get("plainTextEditor");
			CRMCampaignCenterPageTest campaignCenterPage = new CRMCampaignCenterPageTest();
			campaignCenterPage.createCampaign(driver, config, campaignName, accessibility, campaignType, templateName,
					emailSubject, plainTextEditor);

			fc.utobj().printTestStep("Create Template For Send Email");

			String templateNameForSendEmail = dataSet.get("templateNameForSendEmail");
			String emailSubjectForSendEmail = dataSet.get("emailSubjectForSendEmail");
			String plainTextEditorForSendEmail = dataSet.get("plainTextEditorForSendEmail");

			CRMCampaignCenterEmailTemplatesPageTest emailTemplatePage = new CRMCampaignCenterEmailTemplatesPageTest();
			emailTemplatePage.createTemplate(driver, config, templateNameForSendEmail, emailSubjectForSendEmail,
					plainTextEditorForSendEmail);

			fc.utobj().printTestStep("Navigate To CRM > Worflows Page");
			fc.utobj().printTestStep("Create Lead Type Date Based Workflow");

			String workFlowName = dataSet.get("workFlowName");
			String triggerType = dataSet.get("triggerType");
			String conditionType = dataSet.get("conditionType");
			String availableFields = dataSet.get("availableFields");
			String taskSubject = dataSet.get("taskSubject");
			String taskPriority = dataSet.get("taskPriority");
			String taskStatus = dataSet.get("taskStatus");
			String matchingCondition = dataSet.get("matchingCondition");
			String dateOfExecution = dataSet.get("dateOfExecution");

			createWorkFlowLeadDateBased(driver, config, dataSet, workFlowName, triggerType, dateOfExecution,
					conditionType, availableFields, matchingCondition, campaignName, leadStatus,
					templateNameForSendEmail, taskSubject, taskPriority, taskStatus);

			fc.utobj().printTestStep("Verify The Created WorkFlow");
			searchWorkFLowByFilter(driver, workFlowName);

			boolean isWorkFlowNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[.='" + workFlowName + "']");
			if (isWorkFlowNamePresent == false) {
				fc.utobj().throwsException("was not able to verify the workflow");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "crmworkflow")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Create The Date Based WorkFlow of Anniversary Type And Date of Execution At After Days When Lead is Added For Leads Matching Criteria BirthDate And Set To It , is Between", testCaseId = "TC_65_Create_Workflow")
	public void createWorkFlow65() throws IOException, Exception, ParserConfigurationException, SAXException {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crmworkflow", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			/*
			 * 
			 * String
			 * userName=fc.utobj().generateTestData(dataSet.get("userName"));
			 * String
			 * password=fc.utobj().generateTestData(dataSet.get("password"));
			 * String emailId="crmautomation@staffex.com";
			 * AdminUsersManageCorporateUsersAddCorporateUserPageTest
			 * addcorporateUserPage=new
			 * AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			 * addcorporateUserPage.addCorporateUser(driver, userName, password,
			 * config , emailId);
			 * 
			 * fc.home_page().logout(driver, config);
			 * fc.utobj().printTestStep(testCaseId, "Login With Corporate User"
			 * ); fc.loginpage().loginWithParameter(driver, userName, password);
			 */

			fc.utobj().printTestStep("Navigate To Admin > CRM > Configure Status Page");
			fc.utobj().printTestStep("Add Lead Status");
			String leadStatus = dataSet.get("leadStatus");
			AdminCRMConfigureStatusPageTest statusPage = new AdminCRMConfigureStatusPageTest();
			statusPage.addStatus(driver, leadStatus);

			fc.utobj().printTestStep("Navigate To CRM > Campaign Center");
			fc.crm().crm_common().CRMModule(driver);

			fc.utobj().printTestStep("Create Campaign For Send Campaign");

			String campaignName = dataSet.get("campaignName");
			String accessibility = dataSet.get("accessibility");
			String campaignType = dataSet.get("campaignType");
			String templateName = dataSet.get("templateName");
			String emailSubject = dataSet.get("emailSubject");
			String plainTextEditor = dataSet.get("plainTextEditor");
			CRMCampaignCenterPageTest campaignCenterPage = new CRMCampaignCenterPageTest();
			campaignCenterPage.createCampaign(driver, config, campaignName, accessibility, campaignType, templateName,
					emailSubject, plainTextEditor);

			fc.utobj().printTestStep("Create Template For Send Email");

			String templateNameForSendEmail = dataSet.get("templateNameForSendEmail");
			String emailSubjectForSendEmail = dataSet.get("emailSubjectForSendEmail");
			String plainTextEditorForSendEmail = dataSet.get("plainTextEditorForSendEmail");

			CRMCampaignCenterEmailTemplatesPageTest emailTemplatePage = new CRMCampaignCenterEmailTemplatesPageTest();
			emailTemplatePage.createTemplate(driver, config, templateNameForSendEmail, emailSubjectForSendEmail,
					plainTextEditorForSendEmail);

			fc.utobj().printTestStep("Navigate To CRM > Worflows Page");
			fc.utobj().printTestStep("Create Lead Type Date Based Workflow");

			String workFlowName = dataSet.get("workFlowName");
			String triggerType = dataSet.get("triggerType");
			String conditionType = dataSet.get("conditionType");
			String availableFields = dataSet.get("availableFields");
			String taskSubject = dataSet.get("taskSubject");
			String taskPriority = dataSet.get("taskPriority");
			String taskStatus = dataSet.get("taskStatus");
			String matchingCondition = dataSet.get("matchingCondition");
			String dateOfExecution = dataSet.get("dateOfExecution");

			createWorkFlowLeadDateBased(driver, config, dataSet, workFlowName, triggerType, dateOfExecution,
					conditionType, availableFields, matchingCondition, campaignName, leadStatus,
					templateNameForSendEmail, taskSubject, taskPriority, taskStatus);

			fc.utobj().printTestStep("Verify The Created WorkFlow");
			searchWorkFLowByFilter(driver, workFlowName);

			boolean isWorkFlowNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[.='" + workFlowName + "']");
			if (isWorkFlowNamePresent == false) {
				fc.utobj().throwsException("was not able to verify the workflow");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	/*
	 * @Test(groups = "crmworkflow")
	 * 
	 * @TestCase(createdOn="2017-05-01",updatedOn="2018-06-14",
	 * testCaseDescription =
	 * "Create The Date Based WorkFlow of Anniversary Type And Date of Execution At After Days When Lead is Added For Leads Matching Criteria Status Of Lead Is Customized"
	 * , testCaseId = "TC_66_Create_Workflow") public void
	 * createWorkFlow66()throws IOException, Exception,
	 * ParserConfigurationException, SAXException {
	 * 
	 * String testCaseId =
	 * fc.utobj().readTestCaseInfo(this.getClass().getName()+"."+new
	 * Object(){}.getClass().getEnclosingMethod().getName());
	 * 
	 * Map<String, String> config =
	 * fc.utobj().readConfigurationFile(FranconnectUtil.path);
	 * Map<String,String> dataSet = fc.utobj().readTestData("crmworkflow",
	 * testCaseId); WebDriver driver = fc.utobj().openDriver(config);
	 * 
	 * try { driver = fc.loginpage().login(driver, config);
	 * 
	 * String userName=fc.utobj().generateTestData(dataSet.get("userName"));
	 * String password=fc.utobj().generateTestData(dataSet.get("password"));
	 * AdminUsersManageCorporateUsersAddCorporateUserPageTest
	 * addcorporateUserPage=new
	 * AdminUsersManageCorporateUsersAddCorporateUserPageTest();
	 * addcorporateUserPage.addCorporateUser(driver, userName, password, config
	 * , emailId);
	 * 
	 * fc.home_page().logout(driver, config);
	 * fc.utobj().printTestStep(testCaseId, "Login With Corporate User");
	 * fc.loginpage().loginWithParameter(driver, userName, password);
	 * 
	 * fc.utobj().printTestStep(testCaseId,
	 * "Navigate To Admin > CRM > Configure Status Page");
	 * fc.utobj().printTestStep(testCaseId, "Add Lead Status"); String
	 * leadStatus=dataSet.get("leadStatus"); AdminCRMConfigureStatusPageTest
	 * statusPage=new AdminCRMConfigureStatusPageTest();
	 * statusPage.addStatus(driver, leadStatus, config);
	 * 
	 * fc.utobj().printTestStep(testCaseId,
	 * "Navigate To Admin > CRM > Configure Status Page");
	 * fc.utobj().printTestStep(testCaseId, "Add Lead Status"); String
	 * leadStatusCustomize=dataSet.get("selectLeadStatus");
	 * statusPage.addStatus(driver, leadStatusCustomize, config);
	 * 
	 * 
	 * fc.utobj().printTestStep(testCaseId, "Navigate To CRM > Campaign Center"
	 * ); fc.crm().crm_common().CRMModule(driver);
	 * 
	 * fc.utobj().printTestStep(testCaseId, "Create Campaign For Send Campaign"
	 * );
	 * 
	 * String campaignName=dataSet.get("campaignName"); String
	 * accessibility=dataSet.get("accessibility"); String
	 * campaignType=dataSet.get("campaignType"); String
	 * templateName=dataSet.get("templateName"); String
	 * emailSubject=dataSet.get("emailSubject"); String
	 * plainTextEditor=dataSet.get("plainTextEditor"); CRMCampaignCenterPageTest
	 * campaignCenterPage=new CRMCampaignCenterPageTest();
	 * campaignCenterPage.createCampaign(driver, config, campaignName,
	 * accessibility, campaignType, templateName, emailSubject,
	 * plainTextEditor);
	 * 
	 * fc.utobj().printTestStep(testCaseId, "Create Template For Send Email");
	 * 
	 * String templateNameForSendEmail=dataSet.get("templateNameForSendEmail");
	 * String emailSubjectForSendEmail=dataSet.get("emailSubjectForSendEmail");
	 * String
	 * plainTextEditorForSendEmail=dataSet.get("plainTextEditorForSendEmail");
	 * 
	 * CRMCampaignCenterEmailTemplatesPageTest emailTemplatePage=new
	 * CRMCampaignCenterEmailTemplatesPageTest();
	 * emailTemplatePage.createTemplate(driver, config,
	 * templateNameForSendEmail, emailSubjectForSendEmail,
	 * plainTextEditorForSendEmail);
	 * 
	 * fc.utobj().printTestStep(testCaseId, "Navigate To CRM > Worflows Page");
	 * fc.utobj().printTestStep(testCaseId, "Create Lead Type Standard Workflow"
	 * );
	 * 
	 * String workFlowName=dataSet.get("workFlowName"); String
	 * triggerType=dataSet.get("triggerType"); String
	 * conditionType=dataSet.get("conditionType"); String
	 * availableFields=dataSet.get("availableFields"); String
	 * taskSubject=dataSet.get("taskSubject"); String
	 * taskPriority=dataSet.get("taskPriority"); String
	 * taskStatus=dataSet.get("taskStatus"); String
	 * matchingCondition=dataSet.get("matchingCondition"); String
	 * dateOfExecution=dataSet.get("dateOfExecution");
	 * 
	 * createWorkFlowLeadDateBased(driver, config, dataSet, workFlowName,
	 * triggerType, dateOfExecution, conditionType, availableFields,
	 * matchingCondition, campaignName, leadStatus, templateNameForSendEmail,
	 * taskSubject, taskPriority, taskStatus);
	 * 
	 * fc.utobj().logoutAndQuitBrowser(driver, config, testCaseId); } catch
	 * (Exception e) {
	 * 
	 * fc.utobj().quitBrowserOnCatch(driver, config, e, testCaseId);} }
	 * 
	 * 
	 * @Test(groups = "crmworkflownouse")
	 * 
	 * @TestCase(createdOn="2017-05-01",updatedOn="2018-06-14",
	 * testCaseDescription =
	 * "Create The Date Based WorkFlow of Anniversary Type And Date of Execution At After Days When Lead Is Added For All Leads"
	 * , testCaseId = "TC_67_Create_Workflow") public void
	 * createWorkFlow67()throws IOException, Exception,
	 * ParserConfigurationException, SAXException {
	 * 
	 * String testCaseId =
	 * fc.utobj().readTestCaseInfo(this.getClass().getName()+"."+new
	 * Object(){}.getClass().getEnclosingMethod().getName());
	 * 
	 * Map<String, String> config =
	 * fc.utobj().readConfigurationFile(FranconnectUtil.path);
	 * Map<String,String> dataSet = fc.utobj().readTestData("crmworkflow",
	 * testCaseId); WebDriver driver = fc.utobj().openDriver(config);
	 * 
	 * try { driver = fc.loginpage().login(driver, config);
	 * 
	 * String userName=fc.utobj().generateTestData(dataSet.get("userName"));
	 * String password=fc.utobj().generateTestData(dataSet.get("password"));
	 * AdminUsersManageCorporateUsersAddCorporateUserPageTest
	 * addcorporateUserPage=new
	 * AdminUsersManageCorporateUsersAddCorporateUserPageTest();
	 * addcorporateUserPage.addCorporateUser(driver, userName, password, config
	 * , emailId);
	 * 
	 * fc.home_page().logout(driver, config);
	 * fc.utobj().printTestStep(testCaseId, "Login With Corporate User");
	 * fc.loginpage().loginWithParameter(driver, userName, password);
	 * 
	 * fc.utobj().printTestStep(testCaseId,
	 * "Navigate To Admin > CRM > Configure Status Page");
	 * fc.utobj().printTestStep(testCaseId, "Add Lead Status"); String
	 * leadStatus=dataSet.get("leadStatus"); AdminCRMConfigureStatusPageTest
	 * statusPage=new AdminCRMConfigureStatusPageTest();
	 * statusPage.addStatus(driver, leadStatus, config);
	 * 
	 * fc.utobj().printTestStep(testCaseId, "Navigate To CRM > Campaign Center"
	 * ); fc.crm().crm_common().CRMModule(driver);
	 * 
	 * fc.utobj().printTestStep(testCaseId, "Create Campaign For Send Campaign"
	 * );
	 * 
	 * String campaignName=dataSet.get("campaignName"); String
	 * accessibility=dataSet.get("accessibility"); String
	 * campaignType=dataSet.get("campaignType"); String
	 * templateName=dataSet.get("templateName"); String
	 * emailSubject=dataSet.get("emailSubject"); String
	 * plainTextEditor=dataSet.get("plainTextEditor"); CRMCampaignCenterPageTest
	 * campaignCenterPage=new CRMCampaignCenterPageTest();
	 * campaignCenterPage.createCampaign(driver, config, campaignName,
	 * accessibility, campaignType, templateName, emailSubject,
	 * plainTextEditor);
	 * 
	 * fc.utobj().printTestStep(testCaseId, "Create Template For Send Email");
	 * 
	 * String templateNameForSendEmail=dataSet.get("templateNameForSendEmail");
	 * String emailSubjectForSendEmail=dataSet.get("emailSubjectForSendEmail");
	 * String
	 * plainTextEditorForSendEmail=dataSet.get("plainTextEditorForSendEmail");
	 * 
	 * CRMCampaignCenterEmailTemplatesPageTest emailTemplatePage=new
	 * CRMCampaignCenterEmailTemplatesPageTest();
	 * emailTemplatePage.createTemplate(driver, config,
	 * templateNameForSendEmail, emailSubjectForSendEmail,
	 * plainTextEditorForSendEmail);
	 * 
	 * fc.utobj().printTestStep(testCaseId, "Navigate To CRM > Worflows Page");
	 * fc.utobj().printTestStep(testCaseId, "Create Lead Type Standard Workflow"
	 * );
	 * 
	 * String workFlowName=dataSet.get("workFlowName"); String
	 * triggerType=dataSet.get("triggerType"); String
	 * conditionType=dataSet.get("conditionType"); String availableFields="";
	 * String taskSubject=dataSet.get("taskSubject"); String
	 * taskPriority=dataSet.get("taskPriority"); String
	 * taskStatus=dataSet.get("taskStatus"); String matchingCondition=""; String
	 * dateOfExecution=dataSet.get("dateOfExecution");
	 * 
	 * createWorkFlowLeadDateBased(driver, config, dataSet, workFlowName,
	 * triggerType, dateOfExecution, conditionType, availableFields,
	 * matchingCondition, campaignName, leadStatus, templateNameForSendEmail,
	 * taskSubject, taskPriority, taskStatus);
	 * 
	 * fc.utobj().logoutAndQuitBrowser(driver, config, testCaseId); } catch
	 * (Exception e) {
	 * 
	 * fc.utobj().quitBrowserOnCatch(driver, config, e, testCaseId);} }
	 */

	@Test(groups = "crmworkflow")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Create The Date Based WorkFlow of Anniversary Type And Date of Execution At After Days When Contact Is Added For Contact Matching Criteria Email Field is Not Empty", testCaseId = "TC_68_Create_Workflow")
	public void createWorkFlow68() throws IOException, Exception, ParserConfigurationException, SAXException {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crmworkflow", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			/*
			 * 
			 * String
			 * userName=fc.utobj().generateTestData(dataSet.get("userName"));
			 * String
			 * password=fc.utobj().generateTestData(dataSet.get("password"));
			 * String emailId="crmautomation@staffex.com";
			 * AdminUsersManageCorporateUsersAddCorporateUserPageTest
			 * addcorporateUserPage=new
			 * AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			 * addcorporateUserPage.addCorporateUser(driver, userName, password,
			 * config , emailId);
			 * 
			 * fc.home_page().logout(driver, config);
			 * fc.utobj().printTestStep(testCaseId, "Login With Corporate User"
			 * ); fc.loginpage().loginWithParameter(driver, userName, password);
			 */

			fc.utobj().printTestStep("Navigate To Admin > CRM > Configure Status Page");
			fc.utobj().printTestStep("Add Contact Status");
			String contactStatus = dataSet.get("contactStatus");
			AdminCRMConfigureStatusPageTest statusPage = new AdminCRMConfigureStatusPageTest();
			statusPage.addStatus01(driver, contactStatus);

			fc.utobj().printTestStep("Navigate To CRM > Campaign Center");
			fc.crm().crm_common().CRMModule(driver);

			fc.utobj().printTestStep("Create Campaign For Send Campaign");

			String campaignName = dataSet.get("campaignName");
			String accessibility = dataSet.get("accessibility");
			String campaignType = dataSet.get("campaignType");
			String templateName = dataSet.get("templateName");
			String emailSubject = dataSet.get("emailSubject");
			String plainTextEditor = dataSet.get("plainTextEditor");
			CRMCampaignCenterPageTest campaignCenterPage = new CRMCampaignCenterPageTest();
			campaignCenterPage.createCampaign(driver, config, campaignName, accessibility, campaignType, templateName,
					emailSubject, plainTextEditor);

			fc.utobj().printTestStep("Create Template For Send Email");

			String templateNameForSendEmail = dataSet.get("templateNameForSendEmail");
			String emailSubjectForSendEmail = dataSet.get("emailSubjectForSendEmail");
			String plainTextEditorForSendEmail = dataSet.get("plainTextEditorForSendEmail");

			CRMCampaignCenterEmailTemplatesPageTest emailTemplatePage = new CRMCampaignCenterEmailTemplatesPageTest();
			emailTemplatePage.createTemplate(driver, config, templateNameForSendEmail, emailSubjectForSendEmail,
					plainTextEditorForSendEmail);

			fc.utobj().printTestStep("Navigate To CRM > Worflows Page");
			fc.utobj().printTestStep("Create Contact Type Date Based Workflow");

			String workFlowName = dataSet.get("workFlowName");
			String triggerType = dataSet.get("triggerType");
			String conditionType = dataSet.get("conditionType");
			String availableFields = dataSet.get("availableFields");
			String taskSubject = dataSet.get("taskSubject");
			String taskPriority = dataSet.get("taskPriority");
			String taskStatus = dataSet.get("taskStatus");
			String matchingCondition = dataSet.get("matchingCondition");
			String dateOfExecution = dataSet.get("dateOfExecution");

			createWorkFlowContactDateBased(driver, config, dataSet, workFlowName, triggerType, dateOfExecution,
					conditionType, availableFields, matchingCondition, campaignName, contactStatus,
					templateNameForSendEmail, taskSubject, taskPriority, taskStatus);

			fc.utobj().printTestStep("Verify The Created WorkFlow");
			searchWorkFLowByFilter(driver, workFlowName);

			boolean isWorkFlowNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[.='" + workFlowName + "']");
			if (isWorkFlowNamePresent == false) {
				fc.utobj().throwsException("was not able to verify the workflow");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "crmworkflow")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Create The Date Based WorkFlow of Anniversary Type And Date of Execution At After Days When Contact Is Added For Contact Matching Criteria Status Of Contact is Default[New]", testCaseId = "TC_69_Create_Workflow")
	public void createWorkFlow69() throws IOException, Exception, ParserConfigurationException, SAXException {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crmworkflow", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			/*
			 * 
			 * String
			 * userName=fc.utobj().generateTestData(dataSet.get("userName"));
			 * String
			 * password=fc.utobj().generateTestData(dataSet.get("password"));
			 * String emailId="crmautomation@staffex.com";
			 * AdminUsersManageCorporateUsersAddCorporateUserPageTest
			 * addcorporateUserPage=new
			 * AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			 * addcorporateUserPage.addCorporateUser(driver, userName, password,
			 * config , emailId);
			 * 
			 * fc.home_page().logout(driver, config);
			 * fc.utobj().printTestStep(testCaseId, "Login With Corporate User"
			 * ); fc.loginpage().loginWithParameter(driver, userName, password);
			 */

			fc.utobj().printTestStep("Navigate To Admin > CRM > Configure Status Page");
			fc.utobj().printTestStep("Add Contact Status");
			String contactStatus = dataSet.get("contactStatus");
			AdminCRMConfigureStatusPageTest statusPage = new AdminCRMConfigureStatusPageTest();
			statusPage.addStatus01(driver, contactStatus);

			fc.utobj().printTestStep("Navigate To CRM > Campaign Center");
			fc.crm().crm_common().CRMModule(driver);

			fc.utobj().printTestStep("Create Campaign For Send Campaign");

			String campaignName = dataSet.get("campaignName");
			String accessibility = dataSet.get("accessibility");
			String campaignType = dataSet.get("campaignType");
			String templateName = dataSet.get("templateName");
			String emailSubject = dataSet.get("emailSubject");
			String plainTextEditor = dataSet.get("plainTextEditor");
			CRMCampaignCenterPageTest campaignCenterPage = new CRMCampaignCenterPageTest();
			campaignCenterPage.createCampaign(driver, config, campaignName, accessibility, campaignType, templateName,
					emailSubject, plainTextEditor);

			fc.utobj().printTestStep("Create Template For Send Email");

			String templateNameForSendEmail = dataSet.get("templateNameForSendEmail");
			String emailSubjectForSendEmail = dataSet.get("emailSubjectForSendEmail");
			String plainTextEditorForSendEmail = dataSet.get("plainTextEditorForSendEmail");

			CRMCampaignCenterEmailTemplatesPageTest emailTemplatePage = new CRMCampaignCenterEmailTemplatesPageTest();
			emailTemplatePage.createTemplate(driver, config, templateNameForSendEmail, emailSubjectForSendEmail,
					plainTextEditorForSendEmail);

			fc.utobj().printTestStep("Navigate To CRM > Worflows Page");
			fc.utobj().printTestStep("Create Contact Type Date Based Workflow");

			String workFlowName = dataSet.get("workFlowName");
			String triggerType = dataSet.get("triggerType");
			String conditionType = dataSet.get("conditionType");
			String availableFields = dataSet.get("availableFields");
			String taskSubject = dataSet.get("taskSubject");
			String taskPriority = dataSet.get("taskPriority");
			String taskStatus = dataSet.get("taskStatus");
			String matchingCondition = dataSet.get("matchingCondition");
			String dateOfExecution = dataSet.get("dateOfExecution");

			createWorkFlowContactDateBased(driver, config, dataSet, workFlowName, triggerType, dateOfExecution,
					conditionType, availableFields, matchingCondition, campaignName, contactStatus,
					templateNameForSendEmail, taskSubject, taskPriority, taskStatus);

			fc.utobj().printTestStep("Verify The Created WorkFlow");
			searchWorkFLowByFilter(driver, workFlowName);

			boolean isWorkFlowNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[.='" + workFlowName + "']");
			if (isWorkFlowNamePresent == false) {
				fc.utobj().throwsException("was not able to verify the workflow");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "crmworkflow")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Create The Date Based WorkFlow of Anniversary Type And Date of Execution At After Days When Contact Is Added For Contact Matching Criteria Email Subscription status is Set To Opted In", testCaseId = "TC_70_Create_Workflow")
	public void createWorkFlow70() throws IOException, Exception, ParserConfigurationException, SAXException {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crmworkflow", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			/*
			 * 
			 * String
			 * userName=fc.utobj().generateTestData(dataSet.get("userName"));
			 * String
			 * password=fc.utobj().generateTestData(dataSet.get("password"));
			 * String emailId="crmautomation@staffex.com";
			 * AdminUsersManageCorporateUsersAddCorporateUserPageTest
			 * addcorporateUserPage=new
			 * AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			 * addcorporateUserPage.addCorporateUser(driver, userName, password,
			 * config , emailId);
			 * 
			 * fc.home_page().logout(driver, config);
			 * fc.utobj().printTestStep(testCaseId, "Login With Corporate User"
			 * ); fc.loginpage().loginWithParameter(driver, userName, password);
			 */

			fc.utobj().printTestStep("Navigate To Admin > CRM > Configure Status Page");
			fc.utobj().printTestStep("Add Contact Status");
			String contactStatus = dataSet.get("contactStatus");
			AdminCRMConfigureStatusPageTest statusPage = new AdminCRMConfigureStatusPageTest();
			statusPage.addStatus01(driver, contactStatus);

			fc.utobj().printTestStep("Navigate To CRM > Campaign Center");
			fc.crm().crm_common().CRMModule(driver);

			fc.utobj().printTestStep("Create Campaign For Send Campaign");

			String campaignName = dataSet.get("campaignName");
			String accessibility = dataSet.get("accessibility");
			String campaignType = dataSet.get("campaignType");
			String templateName = dataSet.get("templateName");
			String emailSubject = dataSet.get("emailSubject");
			String plainTextEditor = dataSet.get("plainTextEditor");
			CRMCampaignCenterPageTest campaignCenterPage = new CRMCampaignCenterPageTest();
			campaignCenterPage.createCampaign(driver, config, campaignName, accessibility, campaignType, templateName,
					emailSubject, plainTextEditor);

			fc.utobj().printTestStep("Create Template For Send Email");

			String templateNameForSendEmail = dataSet.get("templateNameForSendEmail");
			String emailSubjectForSendEmail = dataSet.get("emailSubjectForSendEmail");
			String plainTextEditorForSendEmail = dataSet.get("plainTextEditorForSendEmail");

			CRMCampaignCenterEmailTemplatesPageTest emailTemplatePage = new CRMCampaignCenterEmailTemplatesPageTest();
			emailTemplatePage.createTemplate(driver, config, templateNameForSendEmail, emailSubjectForSendEmail,
					plainTextEditorForSendEmail);

			fc.utobj().printTestStep("Navigate To CRM > Worflows Page");
			fc.utobj().printTestStep("Create Contact Type Date Based Workflow");

			String workFlowName = dataSet.get("workFlowName");
			String triggerType = dataSet.get("triggerType");
			String conditionType = dataSet.get("conditionType");
			String availableFields = dataSet.get("availableFields");
			String taskSubject = dataSet.get("taskSubject");
			String taskPriority = dataSet.get("taskPriority");
			String taskStatus = dataSet.get("taskStatus");
			String matchingCondition = dataSet.get("matchingCondition");
			String dateOfExecution = dataSet.get("dateOfExecution");

			createWorkFlowContactDateBased(driver, config, dataSet, workFlowName, triggerType, dateOfExecution,
					conditionType, availableFields, matchingCondition, campaignName, contactStatus,
					templateNameForSendEmail, taskSubject, taskPriority, taskStatus);

			fc.utobj().printTestStep("Verify The Created WorkFlow");
			searchWorkFLowByFilter(driver, workFlowName);

			boolean isWorkFlowNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[.='" + workFlowName + "']");
			if (isWorkFlowNamePresent == false) {
				fc.utobj().throwsException("was not able to verify the workflow");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "crmworkflow")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Create The Date Based WorkFlow of Anniversary Type And Date of Execution At After Days When Contact Is Added For Contact Matching Criteria Country And All States of It", testCaseId = "TC_71_Create_Workflow")
	public void createWorkFlow71() throws IOException, Exception, ParserConfigurationException, SAXException {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crmworkflow", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			/*
			 * 
			 * String
			 * userName=fc.utobj().generateTestData(dataSet.get("userName"));
			 * String
			 * password=fc.utobj().generateTestData(dataSet.get("password"));
			 * String emailId="crmautomation@staffex.com";
			 * AdminUsersManageCorporateUsersAddCorporateUserPageTest
			 * addcorporateUserPage=new
			 * AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			 * addcorporateUserPage.addCorporateUser(driver, userName, password,
			 * config , emailId);
			 * 
			 * fc.home_page().logout(driver, config);
			 * fc.utobj().printTestStep(testCaseId, "Login With Corporate User"
			 * ); fc.loginpage().loginWithParameter(driver, userName, password);
			 */

			fc.utobj().printTestStep("Navigate To Admin > CRM > Configure Status Page");
			fc.utobj().printTestStep("Add Contact Status");
			String contactStatus = dataSet.get("contactStatus");
			AdminCRMConfigureStatusPageTest statusPage = new AdminCRMConfigureStatusPageTest();
			statusPage.addStatus01(driver, contactStatus);

			fc.utobj().printTestStep("Navigate To CRM > Campaign Center");
			fc.crm().crm_common().CRMModule(driver);

			fc.utobj().printTestStep("Create Campaign For Send Campaign");

			String campaignName = dataSet.get("campaignName");
			String accessibility = dataSet.get("accessibility");
			String campaignType = dataSet.get("campaignType");
			String templateName = dataSet.get("templateName");
			String emailSubject = dataSet.get("emailSubject");
			String plainTextEditor = dataSet.get("plainTextEditor");
			CRMCampaignCenterPageTest campaignCenterPage = new CRMCampaignCenterPageTest();
			campaignCenterPage.createCampaign(driver, config, campaignName, accessibility, campaignType, templateName,
					emailSubject, plainTextEditor);

			fc.utobj().printTestStep("Create Template For Send Email");

			String templateNameForSendEmail = dataSet.get("templateNameForSendEmail");
			String emailSubjectForSendEmail = dataSet.get("emailSubjectForSendEmail");
			String plainTextEditorForSendEmail = dataSet.get("plainTextEditorForSendEmail");

			CRMCampaignCenterEmailTemplatesPageTest emailTemplatePage = new CRMCampaignCenterEmailTemplatesPageTest();
			emailTemplatePage.createTemplate(driver, config, templateNameForSendEmail, emailSubjectForSendEmail,
					plainTextEditorForSendEmail);

			fc.utobj().printTestStep("Navigate To CRM > Worflows Page");
			fc.utobj().printTestStep("Create Contact Type Date Based Workflow");

			String workFlowName = dataSet.get("workFlowName");
			String triggerType = dataSet.get("triggerType");
			String conditionType = dataSet.get("conditionType");
			String availableFields = dataSet.get("availableFields");
			String taskSubject = dataSet.get("taskSubject");
			String taskPriority = dataSet.get("taskPriority");
			String taskStatus = dataSet.get("taskStatus");
			String matchingCondition = dataSet.get("matchingCondition");
			String dateOfExecution = dataSet.get("dateOfExecution");

			createWorkFlowContactDateBased(driver, config, dataSet, workFlowName, triggerType, dateOfExecution,
					conditionType, availableFields, matchingCondition, campaignName, contactStatus,
					templateNameForSendEmail, taskSubject, taskPriority, taskStatus);

			fc.utobj().printTestStep("Verify The Created WorkFlow");
			searchWorkFLowByFilter(driver, workFlowName);

			boolean isWorkFlowNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[.='" + workFlowName + "']");
			if (isWorkFlowNamePresent == false) {
				fc.utobj().throwsException("was not able to verify the workflow");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "crmworkflow")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Create The Date Based WorkFlow of Anniversary Type And Date of Execution At After Days When Contact Is Added For Contact Matching Criteria BirthDate And Set To It , is Between", testCaseId = "TC_72_Create_Workflow")
	public void createWorkFlow72() throws IOException, Exception, ParserConfigurationException, SAXException {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crmworkflow", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			/*
			 * 
			 * String
			 * userName=fc.utobj().generateTestData(dataSet.get("userName"));
			 * String
			 * password=fc.utobj().generateTestData(dataSet.get("password"));
			 * String emailId="crmautomation@staffex.com";
			 * AdminUsersManageCorporateUsersAddCorporateUserPageTest
			 * addcorporateUserPage=new
			 * AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			 * addcorporateUserPage.addCorporateUser(driver, userName, password,
			 * config , emailId);
			 * 
			 * fc.home_page().logout(driver, config);
			 * fc.utobj().printTestStep(testCaseId, "Login With Corporate User"
			 * ); fc.loginpage().loginWithParameter(driver, userName, password);
			 */

			fc.utobj().printTestStep("Navigate To Admin > CRM > Configure Status Page");
			fc.utobj().printTestStep("Add Contact Status");
			String contactStatus = dataSet.get("contactStatus");
			AdminCRMConfigureStatusPageTest statusPage = new AdminCRMConfigureStatusPageTest();
			statusPage.addStatus01(driver, contactStatus);

			fc.utobj().printTestStep("Navigate To CRM > Campaign Center");
			fc.crm().crm_common().CRMModule(driver);

			fc.utobj().printTestStep("Create Campaign For Send Campaign");

			String campaignName = dataSet.get("campaignName");
			String accessibility = dataSet.get("accessibility");
			String campaignType = dataSet.get("campaignType");
			String templateName = dataSet.get("templateName");
			String emailSubject = dataSet.get("emailSubject");
			String plainTextEditor = dataSet.get("plainTextEditor");
			CRMCampaignCenterPageTest campaignCenterPage = new CRMCampaignCenterPageTest();
			campaignCenterPage.createCampaign(driver, config, campaignName, accessibility, campaignType, templateName,
					emailSubject, plainTextEditor);

			fc.utobj().printTestStep("Create Template For Send Email");

			String templateNameForSendEmail = dataSet.get("templateNameForSendEmail");
			String emailSubjectForSendEmail = dataSet.get("emailSubjectForSendEmail");
			String plainTextEditorForSendEmail = dataSet.get("plainTextEditorForSendEmail");

			CRMCampaignCenterEmailTemplatesPageTest emailTemplatePage = new CRMCampaignCenterEmailTemplatesPageTest();
			emailTemplatePage.createTemplate(driver, config, templateNameForSendEmail, emailSubjectForSendEmail,
					plainTextEditorForSendEmail);

			fc.utobj().printTestStep("Navigate To CRM > Worflows Page");
			fc.utobj().printTestStep("Create Contact Type Date Based Workflow");

			String workFlowName = dataSet.get("workFlowName");
			String triggerType = dataSet.get("triggerType");
			String conditionType = dataSet.get("conditionType");
			String availableFields = dataSet.get("availableFields");
			String taskSubject = dataSet.get("taskSubject");
			String taskPriority = dataSet.get("taskPriority");
			String taskStatus = dataSet.get("taskStatus");
			String matchingCondition = dataSet.get("matchingCondition");
			String dateOfExecution = dataSet.get("dateOfExecution");

			createWorkFlowContactDateBased(driver, config, dataSet, workFlowName, triggerType, dateOfExecution,
					conditionType, availableFields, matchingCondition, campaignName, contactStatus,
					templateNameForSendEmail, taskSubject, taskPriority, taskStatus);

			fc.utobj().printTestStep("Verify The Created WorkFlow");
			searchWorkFLowByFilter(driver, workFlowName);

			boolean isWorkFlowNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[.='" + workFlowName + "']");
			if (isWorkFlowNamePresent == false) {
				fc.utobj().throwsException("was not able to verify the workflow");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	/*
	 * @Test(groups = "crmworkflow")
	 * 
	 * @TestCase(createdOn="2017-05-01",updatedOn="2018-06-14",
	 * testCaseDescription =
	 * "Create The Date Based WorkFlow of Anniversary Type And Date of Execution At After Days When Contact Is Added For Contact Matching Criteria Status Of Contact Is Customized"
	 * , testCaseId = "TC_73_Create_Workflow") public void
	 * createWorkFlow73()throws IOException, Exception,
	 * ParserConfigurationException, SAXException {
	 * 
	 * String testCaseId =
	 * fc.utobj().readTestCaseInfo(this.getClass().getName()+"."+new
	 * Object(){}.getClass().getEnclosingMethod().getName());
	 * 
	 * Map<String, String> config =
	 * fc.utobj().readConfigurationFile(FranconnectUtil.path);
	 * Map<String,String> dataSet = fc.utobj().readTestData("crmworkflow",
	 * testCaseId); WebDriver driver = fc.utobj().openDriver(config);
	 * 
	 * try { driver = fc.loginpage().login(driver, config);
	 * 
	 * String userName=fc.utobj().generateTestData(dataSet.get("userName"));
	 * String password=fc.utobj().generateTestData(dataSet.get("password"));
	 * AdminUsersManageCorporateUsersAddCorporateUserPageTest
	 * addcorporateUserPage=new
	 * AdminUsersManageCorporateUsersAddCorporateUserPageTest();
	 * addcorporateUserPage.addCorporateUser(driver, userName, password, config
	 * , emailId);
	 * 
	 * fc.home_page().logout(driver, config);
	 * fc.utobj().printTestStep(testCaseId, "Login With Corporate User");
	 * fc.loginpage().loginWithParameter(driver, userName, password);
	 * 
	 * fc.utobj().printTestStep(testCaseId,
	 * "Navigate To Admin > CRM > Configure Status Page");
	 * fc.utobj().printTestStep(testCaseId, "Add Contact Status"); String
	 * contactStatus=dataSet.get("contactStatus");
	 * AdminCRMConfigureStatusPageTest statusPage=new
	 * AdminCRMConfigureStatusPageTest(); statusPage.addStatus01(driver,
	 * contactStatus, config);
	 * 
	 * 
	 * fc.utobj().printTestStep(testCaseId,
	 * "Navigate To Admin > CRM > Configure Status Page");
	 * fc.utobj().printTestStep(testCaseId, "Add Contact Status"); String
	 * contactStatusCustomize=dataSet.get("selectContactStatus");
	 * statusPage.addStatus01(driver, contactStatusCustomize, config);
	 * 
	 * fc.utobj().printTestStep(testCaseId, "Navigate To CRM > Campaign Center"
	 * ); fc.crm().crm_common().CRMModule(driver);
	 * 
	 * fc.utobj().printTestStep(testCaseId, "Create Campaign For Send Campaign"
	 * );
	 * 
	 * String campaignName=dataSet.get("campaignName"); String
	 * accessibility=dataSet.get("accessibility"); String
	 * campaignType=dataSet.get("campaignType"); String
	 * templateName=dataSet.get("templateName"); String
	 * emailSubject=dataSet.get("emailSubject"); String
	 * plainTextEditor=dataSet.get("plainTextEditor"); CRMCampaignCenterPageTest
	 * campaignCenterPage=new CRMCampaignCenterPageTest();
	 * campaignCenterPage.createCampaign(driver, config, campaignName,
	 * accessibility, campaignType, templateName, emailSubject,
	 * plainTextEditor);
	 * 
	 * fc.utobj().printTestStep(testCaseId, "Create Template For Send Email");
	 * 
	 * String templateNameForSendEmail=dataSet.get("templateNameForSendEmail");
	 * String emailSubjectForSendEmail=dataSet.get("emailSubjectForSendEmail");
	 * String
	 * plainTextEditorForSendEmail=dataSet.get("plainTextEditorForSendEmail");
	 * 
	 * CRMCampaignCenterEmailTemplatesPageTest emailTemplatePage=new
	 * CRMCampaignCenterEmailTemplatesPageTest();
	 * emailTemplatePage.createTemplate(driver, config,
	 * templateNameForSendEmail, emailSubjectForSendEmail,
	 * plainTextEditorForSendEmail);
	 * 
	 * fc.utobj().printTestStep(testCaseId, "Navigate To CRM > Worflows Page");
	 * fc.utobj().printTestStep(testCaseId,
	 * "Create Contact Type Standard Workflow");
	 * 
	 * String workFlowName=dataSet.get("workFlowName"); String
	 * triggerType=dataSet.get("triggerType"); String
	 * conditionType=dataSet.get("conditionType"); String
	 * availableFields=dataSet.get("availableFields"); String
	 * taskSubject=dataSet.get("taskSubject"); String
	 * taskPriority=dataSet.get("taskPriority"); String
	 * taskStatus=dataSet.get("taskStatus"); String
	 * matchingCondition=dataSet.get("matchingCondition"); String
	 * dateOfExecution=dataSet.get("dateOfExecution");
	 * 
	 * createWorkFlowContactDateBased(driver, config, dataSet, workFlowName,
	 * triggerType, dateOfExecution, conditionType, availableFields,
	 * matchingCondition, campaignName, contactStatus, templateNameForSendEmail,
	 * taskSubject, taskPriority, taskStatus);
	 * 
	 * 
	 * fc.utobj().logoutAndQuitBrowser(driver, config, testCaseId); } catch
	 * (Exception e) {
	 * 
	 * fc.utobj().quitBrowserOnCatch(driver, config, e, testCaseId);} }
	 */

	@Test(groups = "crmworkflow")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Create The Date Based WorkFlow of Anniversary Type And Date of Execution At After Days When Contact Is Added For Contact Matching Criteria Contact Type In", testCaseId = "TC_74_Create_Workflow")
	public void createWorkFlow74() throws IOException, Exception, ParserConfigurationException, SAXException {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crmworkflow", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			/*
			 * 
			 * String
			 * userName=fc.utobj().generateTestData(dataSet.get("userName"));
			 * String
			 * password=fc.utobj().generateTestData(dataSet.get("password"));
			 * String emailId="crmautomation@staffex.com";
			 * AdminUsersManageCorporateUsersAddCorporateUserPageTest
			 * addcorporateUserPage=new
			 * AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			 * addcorporateUserPage.addCorporateUser(driver, userName, password,
			 * config , emailId);
			 * 
			 * fc.home_page().logout(driver, config);
			 * fc.utobj().printTestStep(testCaseId, "Login With Corporate User"
			 * ); fc.loginpage().loginWithParameter(driver, userName, password);
			 */

			fc.utobj().printTestStep("Navigate To Admin > CRM > Configure Status Page");
			fc.utobj().printTestStep("Add Contact Status");
			String contactStatus = dataSet.get("contactStatus");
			AdminCRMConfigureStatusPageTest statusPage = new AdminCRMConfigureStatusPageTest();
			statusPage.addStatus01(driver, contactStatus);

			fc.utobj().printTestStep("Navigate To Admin > CRM > Contact Type Configureation");
			fc.utobj().printTestStep("Add Contact Type");
			String contactType = dataSet.get("selectcontactType");
			AdminCRMContactTypeConfigurationPageTest contactTypeConfigureationPage = new AdminCRMContactTypeConfigurationPageTest();
			contactTypeConfigureationPage.addContactType(driver, contactType);

			fc.utobj().printTestStep("Navigate To CRM > Campaign Center");
			fc.crm().crm_common().CRMModule(driver);

			fc.utobj().printTestStep("Create Campaign For Send Campaign");

			String campaignName = dataSet.get("campaignName");
			String accessibility = dataSet.get("accessibility");
			String campaignType = dataSet.get("campaignType");
			String templateName = dataSet.get("templateName");
			String emailSubject = dataSet.get("emailSubject");
			String plainTextEditor = dataSet.get("plainTextEditor");
			CRMCampaignCenterPageTest campaignCenterPage = new CRMCampaignCenterPageTest();
			campaignCenterPage.createCampaign(driver, config, campaignName, accessibility, campaignType, templateName,
					emailSubject, plainTextEditor);

			fc.utobj().printTestStep("Create Template For Send Email");

			String templateNameForSendEmail = dataSet.get("templateNameForSendEmail");
			String emailSubjectForSendEmail = dataSet.get("emailSubjectForSendEmail");
			String plainTextEditorForSendEmail = dataSet.get("plainTextEditorForSendEmail");

			CRMCampaignCenterEmailTemplatesPageTest emailTemplatePage = new CRMCampaignCenterEmailTemplatesPageTest();
			emailTemplatePage.createTemplate(driver, config, templateNameForSendEmail, emailSubjectForSendEmail,
					plainTextEditorForSendEmail);

			fc.utobj().printTestStep("Navigate To CRM > Worflows Page");
			fc.utobj().printTestStep("Create Contact Type Date Based Workflow");

			String workFlowName = dataSet.get("workFlowName");
			String triggerType = dataSet.get("triggerType");
			String conditionType = dataSet.get("conditionType");
			String availableFields = dataSet.get("availableFields");
			String taskSubject = dataSet.get("taskSubject");
			String taskPriority = dataSet.get("taskPriority");
			String taskStatus = dataSet.get("taskStatus");
			String matchingCondition = dataSet.get("matchingCondition");
			String dateOfExecution = dataSet.get("dateOfExecution");

			createWorkFlowContactDateBased(driver, config, dataSet, workFlowName, triggerType, dateOfExecution,
					conditionType, availableFields, matchingCondition, campaignName, contactStatus,
					templateNameForSendEmail, taskSubject, taskPriority, taskStatus);

			fc.utobj().printTestStep("Verify The Created WorkFlow");
			searchWorkFLowByFilter(driver, workFlowName);

			boolean isWorkFlowNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[.='" + workFlowName + "']");
			if (isWorkFlowNamePresent == false) {
				fc.utobj().throwsException("was not able to verify the workflow");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "crmworkflowTest")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Create The Event Based WorkFlow When Lead Is Added From Existing Web Form For Leads Matching Criteria Email Field is Not Empty", testCaseId = "TC_84_Create_Workflow")
	public void createWorkFlow84() throws IOException, Exception, ParserConfigurationException, SAXException {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crmworkflow", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			AdminCRMManageWebFormGeneratorPage pobj = new AdminCRMManageWebFormGeneratorPage(driver);

			String emailId = "crmautomation@staffex.com";
			AdminUsersManageCorporateUsersAddCorporateUserPageTest addcorporateUserPage = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();

			CorporateUser corpUser = new CorporateUser();
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corpUser.setEmail(emailId);

			corpUser = addcorporateUserPage.createDefaultUser(driver, corpUser);

			/*
			 * fc.home_page().logout(driver, config);
			 * fc.utobj().printTestStep(testCaseId, "Login With Corporate User"
			 * ); fc.loginpage().loginWithParameter(driver, userName, password);
			 */

			fc.utobj().printTestStep("Navigate To Admin > CRM > Configure Status Page");
			fc.utobj().printTestStep("Add Lead Status");
			String leadStatus = dataSet.get("leadStatus");
			AdminCRMConfigureStatusPageTest statusPage = new AdminCRMConfigureStatusPageTest();
			statusPage.addStatus(driver, leadStatus);

			fc.utobj().printTestStep("Navigate To Admin > CRM > Manage Web From Generator");
			fc.utobj().printTestStep("Create A Lead Web Form");

			fc.crm().crm_common().adminCRMManageWebFormGeneratorLnk(driver);
			fc.utobj().clickElement(driver, pobj.createNewForm);
			String formName = dataSet.get("formName");

			fc.utobj().sendKeys(driver, pobj.formName, formName);
			String formTitle = dataSet.get("formTitle");
			fc.utobj().sendKeys(driver, pobj.formDisplayTitle, formTitle);
			if (!pobj.displayFormTitleCheckBox.isSelected()) {
				fc.utobj().clickElement(driver, pobj.displayFormTitleCheckBox);
			}

			if (!fc.utobj().getElement(driver, pobj.formTypeLeads).isSelected()) {
				fc.utobj().clickElement(driver, pobj.formTypeLeads);
			}
			fc.utobj().selectDropDown(driver, pobj.formFormat, "Single Page");
			fc.utobj().selectDropDown(driver, pobj.columnCount, "1");
			fc.utobj().selectDropDown(driver, pobj.filedLabelAlignment, "Left");

			fc.utobj().sendKeys(driver, pobj.iframeWidth, "100%");
			fc.utobj().sendKeys(driver, pobj.iframeHeight, "500");

			String formUrl = dataSet.get("formUrl");
			fc.utobj().sendKeys(driver, pobj.formUrl, formUrl);

			fc.utobj().clickElement(driver, pobj.saveNextBtn);

			fc.utobj().clickElement(driver, pobj.leadInfo);
			fc.utobj().sendKeys(driver, pobj.searchField, "First Name");

			fc.utobj().dragAndDropElement(driver, driver.findElement(By.xpath(".//a[.='First Name']")),
					pobj.defaultDropHere);

			fc.utobj().sendKeys(driver, pobj.searchField, "Email");

			fc.utobj().dragAndDropElement(driver, driver.findElement(By.xpath(".//a[.='Email']")),
					pobj.defaultDropHere);

			fc.utobj().sendKeys(driver, pobj.searchField, "Birthdate");

			fc.utobj().dragAndDropElement(driver, driver.findElement(By.xpath(".//a[.='Birthdate']")),
					pobj.defaultDropHere);

			fc.utobj().sendKeys(driver, pobj.searchField, "Country");

			fc.utobj().dragAndDropElement(driver, driver.findElement(By.xpath(".//a[.='Country']")),
					pobj.defaultDropHere);

			fc.utobj().sendKeys(driver, pobj.searchField, "State / Province");

			fc.utobj().dragAndDropElement(driver, driver.findElement(By.xpath(".//a[.='State / Province']")),
					pobj.defaultDropHere);

			fc.utobj().clickElement(driver, pobj.saveAndNextBtnDesign);

			if (!fc.utobj().getElement(driver, pobj.assignToCorporate).isSelected()) {

				fc.utobj().clickElement(driver, pobj.assignToCorporate);
				fc.utobj().selectDropDown(driver, pobj.selectCorporateUser, corpUser.getuserFullName());
			}
			fc.utobj().clickElement(driver, pobj.finishBtn);
			if (!pobj.hostURL.isSelected()) {
				fc.utobj().clickElement(driver, pobj.hostURL);
			}

			/* String urlText=pobj.hostCodeBox.getText().trim(); */
			fc.utobj().clickElement(driver, pobj.okBtn);

			fc.utobj().printTestStep("Navigate To CRM > Campaign Center");
			fc.crm().crm_common().CRMModule(driver);

			fc.utobj().printTestStep("Create Campaign For Send Campaign");

			String campaignName = dataSet.get("campaignName");
			String accessibility = dataSet.get("accessibility");
			String campaignType = dataSet.get("campaignType");
			String templateName = dataSet.get("templateName");
			String emailSubject = dataSet.get("emailSubject");
			String plainTextEditor = dataSet.get("plainTextEditor");
			CRMCampaignCenterPageTest campaignCenterPage = new CRMCampaignCenterPageTest();
			campaignCenterPage.createCampaign(driver, config, campaignName, accessibility, campaignType, templateName,
					emailSubject, plainTextEditor);

			fc.utobj().printTestStep("Create Template For Send Email");

			String templateNameForSendEmail = dataSet.get("templateNameForSendEmail");
			String emailSubjectForSendEmail = dataSet.get("emailSubjectForSendEmail");
			String plainTextEditorForSendEmail = dataSet.get("plainTextEditorForSendEmail");

			CRMCampaignCenterEmailTemplatesPageTest emailTemplatePage = new CRMCampaignCenterEmailTemplatesPageTest();
			emailTemplatePage.createTemplate(driver, config, templateNameForSendEmail, emailSubjectForSendEmail,
					plainTextEditorForSendEmail);

			fc.utobj().printTestStep("Navigate To CRM > Worflows Page");
			fc.utobj().printTestStep("Create Lead Type Event Based Workflow");

			String workFlowName = dataSet.get("workFlowName");
			String triggerType = dataSet.get("triggerType");
			String conditionType = dataSet.get("conditionType");
			String availableFields = dataSet.get("availableFields");
			String taskSubject = dataSet.get("taskSubject");
			String taskPriority = dataSet.get("taskPriority");
			String taskStatus = dataSet.get("taskStatus");
			String matchingCondition = dataSet.get("matchingCondition");

			createWorkFlowLeadEventBased(driver, config, dataSet, workFlowName, triggerType, conditionType,
					availableFields, matchingCondition, campaignName, leadStatus, templateNameForSendEmail, taskSubject,
					taskPriority, taskStatus);

			fc.utobj().printTestStep("Verify The Created WorkFlow");
			searchWorkFLowByFilter(driver, workFlowName);

			boolean isWorkFlowNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[.='" + workFlowName + "']");
			if (isWorkFlowNamePresent == false) {
				fc.utobj().throwsException("was not able to verify the workflow");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "crmworkflowTest")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Create The Event Based WorkFlow When Lead Is Added From Existing Web Form For Leads Matching Criteria Status OF Lead Is Default[New]", testCaseId = "TC_85_Create_Workflow")
	public void createWorkFlow85() throws IOException, Exception, ParserConfigurationException, SAXException {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crmworkflow", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			AdminCRMManageWebFormGeneratorPage pobj = new AdminCRMManageWebFormGeneratorPage(driver);

			String emailId = "crmautomation@staffex.com";
			AdminUsersManageCorporateUsersAddCorporateUserPageTest addcorporateUserPage = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser = new CorporateUser();
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corpUser.setEmail(emailId);
			corpUser = addcorporateUserPage.createDefaultUser(driver, corpUser);

			/*
			 * fc.home_page().logout(driver, config);
			 * fc.utobj().printTestStep(testCaseId, "Login With Corporate User"
			 * ); fc.loginpage().loginWithParameter(driver, userName, password);
			 */

			fc.utobj().printTestStep("Navigate To Admin > CRM > Configure Status Page");
			fc.utobj().printTestStep("Add Lead Status");
			String leadStatus = dataSet.get("leadStatus");
			AdminCRMConfigureStatusPageTest statusPage = new AdminCRMConfigureStatusPageTest();
			statusPage.addStatus(driver, leadStatus);

			fc.utobj().printTestStep("Navigate To Admin > CRM > Manage Web From Generator");
			fc.utobj().printTestStep("Create A Lead Web Form");

			fc.crm().crm_common().adminCRMManageWebFormGeneratorLnk(driver);
			fc.utobj().clickElement(driver, pobj.createNewForm);
			String formName = dataSet.get("formName");

			fc.utobj().sendKeys(driver, pobj.formName, formName);
			String formTitle = dataSet.get("formTitle");
			fc.utobj().sendKeys(driver, pobj.formDisplayTitle, formTitle);
			if (!pobj.displayFormTitleCheckBox.isSelected()) {
				fc.utobj().clickElement(driver, pobj.displayFormTitleCheckBox);
			}

			if (!fc.utobj().getElement(driver, pobj.formTypeLeads).isSelected()) {
				fc.utobj().clickElement(driver, pobj.formTypeLeads);
			}
			fc.utobj().selectDropDown(driver, pobj.formFormat, "Single Page");
			fc.utobj().selectDropDown(driver, pobj.columnCount, "1");
			fc.utobj().selectDropDown(driver, pobj.filedLabelAlignment, "Left");

			fc.utobj().sendKeys(driver, pobj.iframeWidth, "100%");
			fc.utobj().sendKeys(driver, pobj.iframeHeight, "500");

			String formUrl = dataSet.get("formUrl");
			fc.utobj().sendKeys(driver, pobj.formUrl, formUrl);

			fc.utobj().clickElement(driver, pobj.saveNextBtn);

			fc.utobj().clickElement(driver, pobj.leadInfo);
			fc.utobj().sendKeys(driver, pobj.searchField, "First Name");

			fc.utobj().dragAndDropElement(driver, driver.findElement(By.xpath(".//a[.='First Name']")),
					pobj.defaultDropHere);

			fc.utobj().sendKeys(driver, pobj.searchField, "Email");

			fc.utobj().dragAndDropElement(driver, driver.findElement(By.xpath(".//a[.='Email']")),
					pobj.defaultDropHere);

			fc.utobj().sendKeys(driver, pobj.searchField, "Birthdate");

			fc.utobj().dragAndDropElement(driver, driver.findElement(By.xpath(".//a[.='Birthdate']")),
					pobj.defaultDropHere);

			fc.utobj().sendKeys(driver, pobj.searchField, "Country");

			fc.utobj().dragAndDropElement(driver, driver.findElement(By.xpath(".//a[.='Country']")),
					pobj.defaultDropHere);

			fc.utobj().sendKeys(driver, pobj.searchField, "State / Province");

			fc.utobj().dragAndDropElement(driver, driver.findElement(By.xpath(".//a[.='State / Province']")),
					pobj.defaultDropHere);

			fc.utobj().clickElement(driver, pobj.saveAndNextBtnDesign);

			if (!fc.utobj().getElement(driver, pobj.assignToCorporate).isSelected()) {

				fc.utobj().clickElement(driver, pobj.assignToCorporate);
				fc.utobj().selectDropDown(driver, pobj.selectCorporateUser, corpUser.getuserFullName());
			}
			fc.utobj().clickElement(driver, pobj.finishBtn);
			if (!pobj.hostURL.isSelected()) {
				fc.utobj().clickElement(driver, pobj.hostURL);
			}

			/* String urlText=pobj.hostCodeBox.getText().trim(); */
			fc.utobj().clickElement(driver, pobj.okBtn);

			fc.utobj().printTestStep("Navigate To CRM > Campaign Center");
			fc.crm().crm_common().CRMModule(driver);

			fc.utobj().printTestStep("Create Campaign For Send Campaign");

			String campaignName = dataSet.get("campaignName");
			String accessibility = dataSet.get("accessibility");
			String campaignType = dataSet.get("campaignType");
			String templateName = dataSet.get("templateName");
			String emailSubject = dataSet.get("emailSubject");
			String plainTextEditor = dataSet.get("plainTextEditor");
			CRMCampaignCenterPageTest campaignCenterPage = new CRMCampaignCenterPageTest();
			campaignCenterPage.createCampaign(driver, config, campaignName, accessibility, campaignType, templateName,
					emailSubject, plainTextEditor);

			fc.utobj().printTestStep("Create Template For Send Email");

			String templateNameForSendEmail = dataSet.get("templateNameForSendEmail");
			String emailSubjectForSendEmail = dataSet.get("emailSubjectForSendEmail");
			String plainTextEditorForSendEmail = dataSet.get("plainTextEditorForSendEmail");

			CRMCampaignCenterEmailTemplatesPageTest emailTemplatePage = new CRMCampaignCenterEmailTemplatesPageTest();
			emailTemplatePage.createTemplate(driver, config, templateNameForSendEmail, emailSubjectForSendEmail,
					plainTextEditorForSendEmail);

			fc.utobj().printTestStep("Navigate To CRM > Worflows Page");
			fc.utobj().printTestStep("Create Lead Type Event Based Workflow");

			String workFlowName = dataSet.get("workFlowName");
			String triggerType = dataSet.get("triggerType");
			String conditionType = dataSet.get("conditionType");
			String availableFields = dataSet.get("availableFields");
			String taskSubject = dataSet.get("taskSubject");
			String taskPriority = dataSet.get("taskPriority");
			String taskStatus = dataSet.get("taskStatus");
			String matchingCondition = dataSet.get("matchingCondition");

			createWorkFlowLeadStandard(driver, config, dataSet, workFlowName, triggerType, conditionType,
					availableFields, matchingCondition, campaignName, leadStatus, templateNameForSendEmail, taskSubject,
					taskPriority, taskStatus);

			fc.utobj().printTestStep("Verify The Created WorkFlow");
			searchWorkFLowByFilter(driver, workFlowName);

			boolean isWorkFlowNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[.='" + workFlowName + "']");
			if (isWorkFlowNamePresent == false) {
				fc.utobj().throwsException("was not able to verify the workflow");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "crmworkflowTest")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Create The Event Based WorkFlow When Lead is Added From Existing Web form For Leads Matching Criteria Email Subscription status is Set To Opted In", testCaseId = "TC_86_Create_Workflow")
	public void createWorkFlow86() throws IOException, Exception, ParserConfigurationException, SAXException {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crmworkflow", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			AdminCRMManageWebFormGeneratorPage pobj = new AdminCRMManageWebFormGeneratorPage(driver);

			String emailId = "crmautomation@staffex.com";
			AdminUsersManageCorporateUsersAddCorporateUserPageTest addcorporateUserPage = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser = new CorporateUser();
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corpUser.setEmail(emailId);
			corpUser = addcorporateUserPage.createDefaultUser(driver, corpUser);

			/*
			 * fc.home_page().logout(driver, config);
			 * fc.utobj().printTestStep(testCaseId, "Login With Corporate User"
			 * ); fc.loginpage().loginWithParameter(driver, userName, password);
			 */

			fc.utobj().printTestStep("Navigate To Admin > CRM > Configure Status Page");
			fc.utobj().printTestStep("Add Lead Status");
			String leadStatus = dataSet.get("leadStatus");
			AdminCRMConfigureStatusPageTest statusPage = new AdminCRMConfigureStatusPageTest();
			statusPage.addStatus(driver, leadStatus);

			fc.utobj().printTestStep("Navigate To Admin > CRM > Manage Web From Generator");
			fc.utobj().printTestStep("Create A Lead Web Form");

			fc.crm().crm_common().adminCRMManageWebFormGeneratorLnk(driver);
			fc.utobj().clickElement(driver, pobj.createNewForm);
			String formName = dataSet.get("formName");

			fc.utobj().sendKeys(driver, pobj.formName, formName);
			String formTitle = dataSet.get("formTitle");
			fc.utobj().sendKeys(driver, pobj.formDisplayTitle, formTitle);
			if (!pobj.displayFormTitleCheckBox.isSelected()) {
				fc.utobj().clickElement(driver, pobj.displayFormTitleCheckBox);
			}

			if (!fc.utobj().getElement(driver, pobj.formTypeLeads).isSelected()) {
				fc.utobj().clickElement(driver, pobj.formTypeLeads);
			}
			fc.utobj().selectDropDown(driver, pobj.formFormat, "Single Page");
			fc.utobj().selectDropDown(driver, pobj.columnCount, "1");
			fc.utobj().selectDropDown(driver, pobj.filedLabelAlignment, "Left");

			fc.utobj().sendKeys(driver, pobj.iframeWidth, "100%");
			fc.utobj().sendKeys(driver, pobj.iframeHeight, "500");

			String formUrl = dataSet.get("formUrl");
			fc.utobj().sendKeys(driver, pobj.formUrl, formUrl);

			fc.utobj().clickElement(driver, pobj.saveNextBtn);

			fc.utobj().clickElement(driver, pobj.leadInfo);
			fc.utobj().sendKeys(driver, pobj.searchField, "First Name");

			fc.utobj().dragAndDropElement(driver, driver.findElement(By.xpath(".//a[.='First Name']")),
					pobj.defaultDropHere);

			fc.utobj().sendKeys(driver, pobj.searchField, "Email");

			fc.utobj().dragAndDropElement(driver, driver.findElement(By.xpath(".//a[.='Email']")),
					pobj.defaultDropHere);

			fc.utobj().sendKeys(driver, pobj.searchField, "Birthdate");

			fc.utobj().dragAndDropElement(driver, driver.findElement(By.xpath(".//a[.='Birthdate']")),
					pobj.defaultDropHere);

			fc.utobj().sendKeys(driver, pobj.searchField, "Country");

			fc.utobj().dragAndDropElement(driver, driver.findElement(By.xpath(".//a[.='Country']")),
					pobj.defaultDropHere);

			fc.utobj().sendKeys(driver, pobj.searchField, "State / Province");

			fc.utobj().dragAndDropElement(driver, driver.findElement(By.xpath(".//a[.='State / Province']")),
					pobj.defaultDropHere);

			fc.utobj().clickElement(driver, pobj.saveAndNextBtnDesign);

			if (!fc.utobj().getElement(driver, pobj.assignToCorporate).isSelected()) {

				fc.utobj().clickElement(driver, pobj.assignToCorporate);
				fc.utobj().selectDropDown(driver, pobj.selectCorporateUser, corpUser.getuserFullName());
			}
			fc.utobj().clickElement(driver, pobj.finishBtn);
			if (!pobj.hostURL.isSelected()) {
				fc.utobj().clickElement(driver, pobj.hostURL);
			}

			/* String urlText=pobj.hostCodeBox.getText().trim(); */
			fc.utobj().clickElement(driver, pobj.okBtn);

			fc.utobj().printTestStep("Navigate To CRM > Campaign Center");
			fc.crm().crm_common().CRMModule(driver);

			fc.utobj().printTestStep("Create Campaign For Send Campaign");

			String campaignName = dataSet.get("campaignName");
			String accessibility = dataSet.get("accessibility");
			String campaignType = dataSet.get("campaignType");
			String templateName = dataSet.get("templateName");
			String emailSubject = dataSet.get("emailSubject");
			String plainTextEditor = dataSet.get("plainTextEditor");
			CRMCampaignCenterPageTest campaignCenterPage = new CRMCampaignCenterPageTest();
			campaignCenterPage.createCampaign(driver, config, campaignName, accessibility, campaignType, templateName,
					emailSubject, plainTextEditor);

			fc.utobj().printTestStep("Create Template For Send Email");

			String templateNameForSendEmail = dataSet.get("templateNameForSendEmail");
			String emailSubjectForSendEmail = dataSet.get("emailSubjectForSendEmail");
			String plainTextEditorForSendEmail = dataSet.get("plainTextEditorForSendEmail");

			CRMCampaignCenterEmailTemplatesPageTest emailTemplatePage = new CRMCampaignCenterEmailTemplatesPageTest();
			emailTemplatePage.createTemplate(driver, config, templateNameForSendEmail, emailSubjectForSendEmail,
					plainTextEditorForSendEmail);

			fc.utobj().printTestStep("Navigate To CRM > Worflows Page");
			fc.utobj().printTestStep("Create Lead Type Event Based Workflow");

			String workFlowName = dataSet.get("workFlowName");
			String triggerType = dataSet.get("triggerType");
			String conditionType = dataSet.get("conditionType");
			String availableFields = dataSet.get("availableFields");
			String taskSubject = dataSet.get("taskSubject");
			String taskPriority = dataSet.get("taskPriority");
			String taskStatus = dataSet.get("taskStatus");
			String matchingCondition = dataSet.get("matchingCondition");

			createWorkFlowLeadStandard(driver, config, dataSet, workFlowName, triggerType, conditionType,
					availableFields, matchingCondition, campaignName, leadStatus, templateNameForSendEmail, taskSubject,
					taskPriority, taskStatus);

			fc.utobj().printTestStep("Verify The Created WorkFlow");
			searchWorkFLowByFilter(driver, workFlowName);

			boolean isWorkFlowNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[.='" + workFlowName + "']");
			if (isWorkFlowNamePresent == false) {
				fc.utobj().throwsException("was not able to verify the workflow");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "crmworkflowTest")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Create The Event Based WorkFlow When Lead is Added From Existing Web Form For Leads Matching Criteria Country And All States of It", testCaseId = "TC_87_Create_Workflow")
	public void createWorkFlow87() throws IOException, Exception, ParserConfigurationException, SAXException {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crmworkflow", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			AdminCRMManageWebFormGeneratorPage pobj = new AdminCRMManageWebFormGeneratorPage(driver);

			String emailId = "crmautomation@staffex.com";
			AdminUsersManageCorporateUsersAddCorporateUserPageTest addcorporateUserPage = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser = new CorporateUser();
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corpUser.setEmail(emailId);
			corpUser = addcorporateUserPage.createDefaultUser(driver, corpUser);

			/*
			 * fc.home_page().logout(driver, config);
			 * fc.utobj().printTestStep(testCaseId, "Login With Corporate User"
			 * ); fc.loginpage().loginWithParameter(driver, userName, password);
			 */

			fc.utobj().printTestStep("Navigate To Admin > CRM > Configure Status Page");
			fc.utobj().printTestStep("Add Lead Status");
			String leadStatus = dataSet.get("leadStatus");
			AdminCRMConfigureStatusPageTest statusPage = new AdminCRMConfigureStatusPageTest();
			statusPage.addStatus(driver, leadStatus);

			fc.utobj().printTestStep("Navigate To Admin > CRM > Manage Web From Generator");
			fc.utobj().printTestStep("Create A Lead Web Form");

			fc.crm().crm_common().adminCRMManageWebFormGeneratorLnk(driver);
			fc.utobj().clickElement(driver, pobj.createNewForm);
			String formName = dataSet.get("formName");

			fc.utobj().sendKeys(driver, pobj.formName, formName);
			String formTitle = dataSet.get("formTitle");
			fc.utobj().sendKeys(driver, pobj.formDisplayTitle, formTitle);
			if (!pobj.displayFormTitleCheckBox.isSelected()) {
				fc.utobj().clickElement(driver, pobj.displayFormTitleCheckBox);
			}

			if (!fc.utobj().getElement(driver, pobj.formTypeLeads).isSelected()) {
				fc.utobj().clickElement(driver, pobj.formTypeLeads);
			}
			fc.utobj().selectDropDown(driver, pobj.formFormat, "Single Page");
			fc.utobj().selectDropDown(driver, pobj.columnCount, "1");
			fc.utobj().selectDropDown(driver, pobj.filedLabelAlignment, "Left");

			fc.utobj().sendKeys(driver, pobj.iframeWidth, "100%");
			fc.utobj().sendKeys(driver, pobj.iframeHeight, "500");

			String formUrl = dataSet.get("formUrl");
			fc.utobj().sendKeys(driver, pobj.formUrl, formUrl);

			fc.utobj().clickElement(driver, pobj.saveNextBtn);

			fc.utobj().clickElement(driver, pobj.leadInfo);
			fc.utobj().sendKeys(driver, pobj.searchField, "First Name");

			fc.utobj().dragAndDropElement(driver, driver.findElement(By.xpath(".//a[.='First Name']")),
					pobj.defaultDropHere);

			fc.utobj().sendKeys(driver, pobj.searchField, "Email");

			fc.utobj().dragAndDropElement(driver, driver.findElement(By.xpath(".//a[.='Email']")),
					pobj.defaultDropHere);

			fc.utobj().sendKeys(driver, pobj.searchField, "Birthdate");

			fc.utobj().dragAndDropElement(driver, driver.findElement(By.xpath(".//a[.='Birthdate']")),
					pobj.defaultDropHere);

			fc.utobj().sendKeys(driver, pobj.searchField, "Country");

			fc.utobj().dragAndDropElement(driver, driver.findElement(By.xpath(".//a[.='Country']")),
					pobj.defaultDropHere);

			fc.utobj().sendKeys(driver, pobj.searchField, "State / Province");

			fc.utobj().dragAndDropElement(driver, driver.findElement(By.xpath(".//a[.='State / Province']")),
					pobj.defaultDropHere);

			fc.utobj().clickElement(driver, pobj.saveAndNextBtnDesign);

			if (!fc.utobj().getElement(driver, pobj.assignToCorporate).isSelected()) {

				fc.utobj().clickElement(driver, pobj.assignToCorporate);
				fc.utobj().selectDropDown(driver, pobj.selectCorporateUser, corpUser.getuserFullName());
			}
			fc.utobj().clickElement(driver, pobj.finishBtn);
			if (!pobj.hostURL.isSelected()) {
				fc.utobj().clickElement(driver, pobj.hostURL);
			}

			/* String urlText=pobj.hostCodeBox.getText().trim(); */
			fc.utobj().clickElement(driver, pobj.okBtn);

			fc.utobj().printTestStep("Navigate To CRM > Campaign Center");
			fc.crm().crm_common().CRMModule(driver);

			fc.utobj().printTestStep("Create Campaign For Send Campaign");

			String campaignName = dataSet.get("campaignName");
			String accessibility = dataSet.get("accessibility");
			String campaignType = dataSet.get("campaignType");
			String templateName = dataSet.get("templateName");
			String emailSubject = dataSet.get("emailSubject");
			String plainTextEditor = dataSet.get("plainTextEditor");
			CRMCampaignCenterPageTest campaignCenterPage = new CRMCampaignCenterPageTest();
			campaignCenterPage.createCampaign(driver, config, campaignName, accessibility, campaignType, templateName,
					emailSubject, plainTextEditor);

			fc.utobj().printTestStep("Create Template For Send Email");

			String templateNameForSendEmail = dataSet.get("templateNameForSendEmail");
			String emailSubjectForSendEmail = dataSet.get("emailSubjectForSendEmail");
			String plainTextEditorForSendEmail = dataSet.get("plainTextEditorForSendEmail");

			CRMCampaignCenterEmailTemplatesPageTest emailTemplatePage = new CRMCampaignCenterEmailTemplatesPageTest();
			emailTemplatePage.createTemplate(driver, config, templateNameForSendEmail, emailSubjectForSendEmail,
					plainTextEditorForSendEmail);

			fc.utobj().printTestStep("Navigate To CRM > Worflows Page");
			fc.utobj().printTestStep("Create Lead Type Event Based Workflow");

			String workFlowName = dataSet.get("workFlowName");
			String triggerType = dataSet.get("triggerType");
			String conditionType = dataSet.get("conditionType");
			String availableFields = dataSet.get("availableFields");
			String taskSubject = dataSet.get("taskSubject");
			String taskPriority = dataSet.get("taskPriority");
			String taskStatus = dataSet.get("taskStatus");
			String matchingCondition = dataSet.get("matchingCondition");

			createWorkFlowLeadStandard(driver, config, dataSet, workFlowName, triggerType, conditionType,
					availableFields, matchingCondition, campaignName, leadStatus, templateNameForSendEmail, taskSubject,
					taskPriority, taskStatus);

			fc.utobj().printTestStep("Verify The Created WorkFlow");
			searchWorkFLowByFilter(driver, workFlowName);

			boolean isWorkFlowNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[.='" + workFlowName + "']");
			if (isWorkFlowNamePresent == false) {
				fc.utobj().throwsException("was not able to verify the workflow");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "crmworkflowTest")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Create The Event Based WorkFlow When Lead is Added From Existing web Form For Leads Matching Criteria BirthDate And Set To It , is Between", testCaseId = "TC_88_Create_Workflow")
	public void createWorkFlow88() throws IOException, Exception, ParserConfigurationException, SAXException {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crmworkflow", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			AdminCRMManageWebFormGeneratorPage pobj = new AdminCRMManageWebFormGeneratorPage(driver);

			String emailId = "crmautomation@staffex.com";
			AdminUsersManageCorporateUsersAddCorporateUserPageTest addcorporateUserPage = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser = new CorporateUser();
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corpUser.setEmail(emailId);
			corpUser = addcorporateUserPage.createDefaultUser(driver, corpUser);

			/*
			 * fc.home_page().logout(driver, config);
			 * fc.utobj().printTestStep(testCaseId, "Login With Corporate User"
			 * ); fc.loginpage().loginWithParameter(driver, userName, password);
			 */

			fc.utobj().printTestStep("Navigate To Admin > CRM > Configure Status Page");
			fc.utobj().printTestStep("Add Lead Status");
			String leadStatus = dataSet.get("leadStatus");
			AdminCRMConfigureStatusPageTest statusPage = new AdminCRMConfigureStatusPageTest();
			statusPage.addStatus(driver, leadStatus);

			fc.utobj().printTestStep("Navigate To Admin > CRM > Manage Web From Generator");
			fc.utobj().printTestStep("Create A Lead Web Form");

			fc.crm().crm_common().adminCRMManageWebFormGeneratorLnk(driver);
			fc.utobj().clickElement(driver, pobj.createNewForm);
			String formName = dataSet.get("formName");

			fc.utobj().sendKeys(driver, pobj.formName, formName);
			String formTitle = dataSet.get("formTitle");
			fc.utobj().sendKeys(driver, pobj.formDisplayTitle, formTitle);
			if (!pobj.displayFormTitleCheckBox.isSelected()) {
				fc.utobj().clickElement(driver, pobj.displayFormTitleCheckBox);
			}

			if (!fc.utobj().getElement(driver, pobj.formTypeLeads).isSelected()) {
				fc.utobj().clickElement(driver, pobj.formTypeLeads);
			}
			fc.utobj().selectDropDown(driver, pobj.formFormat, "Single Page");
			fc.utobj().selectDropDown(driver, pobj.columnCount, "1");
			fc.utobj().selectDropDown(driver, pobj.filedLabelAlignment, "Left");

			fc.utobj().sendKeys(driver, pobj.iframeWidth, "100%");
			fc.utobj().sendKeys(driver, pobj.iframeHeight, "500");

			String formUrl = dataSet.get("formUrl");
			fc.utobj().sendKeys(driver, pobj.formUrl, formUrl);

			fc.utobj().clickElement(driver, pobj.saveNextBtn);

			fc.utobj().clickElement(driver, pobj.leadInfo);
			fc.utobj().sendKeys(driver, pobj.searchField, "First Name");

			fc.utobj().dragAndDropElement(driver, driver.findElement(By.xpath(".//a[.='First Name']")),
					pobj.defaultDropHere);

			fc.utobj().sendKeys(driver, pobj.searchField, "Email");

			fc.utobj().dragAndDropElement(driver, driver.findElement(By.xpath(".//a[.='Email']")),
					pobj.defaultDropHere);

			fc.utobj().sendKeys(driver, pobj.searchField, "Birthdate");

			fc.utobj().dragAndDropElement(driver, driver.findElement(By.xpath(".//a[.='Birthdate']")),
					pobj.defaultDropHere);

			fc.utobj().sendKeys(driver, pobj.searchField, "Country");

			fc.utobj().dragAndDropElement(driver, driver.findElement(By.xpath(".//a[.='Country']")),
					pobj.defaultDropHere);

			fc.utobj().sendKeys(driver, pobj.searchField, "State / Province");

			fc.utobj().dragAndDropElement(driver, driver.findElement(By.xpath(".//a[.='State / Province']")),
					pobj.defaultDropHere);

			fc.utobj().clickElement(driver, pobj.saveAndNextBtnDesign);

			if (!fc.utobj().getElement(driver, pobj.assignToCorporate).isSelected()) {

				fc.utobj().clickElement(driver, pobj.assignToCorporate);
				fc.utobj().selectDropDown(driver, pobj.selectCorporateUser, corpUser.getuserFullName());
			}
			fc.utobj().clickElement(driver, pobj.finishBtn);
			if (!pobj.hostURL.isSelected()) {
				fc.utobj().clickElement(driver, pobj.hostURL);
			}

			/* String urlText=pobj.hostCodeBox.getText().trim(); */
			fc.utobj().clickElement(driver, pobj.okBtn);

			fc.utobj().printTestStep("Navigate To CRM > Campaign Center");
			fc.crm().crm_common().CRMModule(driver);

			fc.utobj().printTestStep("Create Campaign For Send Campaign");

			String campaignName = dataSet.get("campaignName");
			String accessibility = dataSet.get("accessibility");
			String campaignType = dataSet.get("campaignType");
			String templateName = dataSet.get("templateName");
			String emailSubject = dataSet.get("emailSubject");
			String plainTextEditor = dataSet.get("plainTextEditor");
			CRMCampaignCenterPageTest campaignCenterPage = new CRMCampaignCenterPageTest();
			campaignCenterPage.createCampaign(driver, config, campaignName, accessibility, campaignType, templateName,
					emailSubject, plainTextEditor);

			fc.utobj().printTestStep("Create Template For Send Email");

			String templateNameForSendEmail = dataSet.get("templateNameForSendEmail");
			String emailSubjectForSendEmail = dataSet.get("emailSubjectForSendEmail");
			String plainTextEditorForSendEmail = dataSet.get("plainTextEditorForSendEmail");

			CRMCampaignCenterEmailTemplatesPageTest emailTemplatePage = new CRMCampaignCenterEmailTemplatesPageTest();
			emailTemplatePage.createTemplate(driver, config, templateNameForSendEmail, emailSubjectForSendEmail,
					plainTextEditorForSendEmail);

			fc.utobj().printTestStep("Navigate To CRM > Worflows Page");
			fc.utobj().printTestStep("Create Lead Type Event Based Workflow");

			String workFlowName = dataSet.get("workFlowName");
			String triggerType = dataSet.get("triggerType");
			String conditionType = dataSet.get("conditionType");
			String availableFields = dataSet.get("availableFields");
			String taskSubject = dataSet.get("taskSubject");
			String taskPriority = dataSet.get("taskPriority");
			String taskStatus = dataSet.get("taskStatus");
			String matchingCondition = dataSet.get("matchingCondition");

			createWorkFlowLeadStandard(driver, config, dataSet, workFlowName, triggerType, conditionType,
					availableFields, matchingCondition, campaignName, leadStatus, templateNameForSendEmail, taskSubject,
					taskPriority, taskStatus);

			fc.utobj().printTestStep("Verify The Created WorkFlow");
			searchWorkFLowByFilter(driver, workFlowName);

			boolean isWorkFlowNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[.='" + workFlowName + "']");
			if (isWorkFlowNamePresent == false) {
				fc.utobj().throwsException("was not able to verify the workflow");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "crmworkflowTest")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Create The Event Based WorkFlow When Lead Status is Changed For Leads Matching Criteria Email Field is Not Empty", testCaseId = "TC_89_Create_Workflow")
	public void createWorkFlow89() throws IOException, Exception, ParserConfigurationException, SAXException {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crmworkflow", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			/*
			 * 
			 * String
			 * userName=fc.utobj().generateTestData(dataSet.get("userName"));
			 * String
			 * password=fc.utobj().generateTestData(dataSet.get("password"));
			 * String emailId="crmautomation@staffex.com";
			 * AdminUsersManageCorporateUsersAddCorporateUserPageTest
			 * addcorporateUserPage=new
			 * AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			 * addcorporateUserPage.addCorporateUser(driver, userName, password,
			 * config , emailId);
			 * 
			 * fc.home_page().logout(driver, config);
			 * fc.utobj().printTestStep(testCaseId, "Login With Corporate User"
			 * ); fc.loginpage().loginWithParameter(driver, userName, password);
			 */

			fc.utobj().printTestStep("Navigate To Admin > CRM > Configure Status Page");
			fc.utobj().printTestStep("Add Lead Status");
			String leadStatus = dataSet.get("leadStatus");
			AdminCRMConfigureStatusPageTest statusPage = new AdminCRMConfigureStatusPageTest();
			statusPage.addStatus(driver, leadStatus);

			fc.utobj().printTestStep("Add Lead Status For Changed Status");
			String leadStatusChanged = dataSet.get("leadStatusChanged");
			statusPage.addStatus(driver, leadStatusChanged);

			fc.utobj().printTestStep("Navigate To CRM > Campaign Center");
			fc.crm().crm_common().CRMModule(driver);

			fc.utobj().printTestStep("Create Campaign For Send Campaign");

			String campaignName = dataSet.get("campaignName");
			String accessibility = dataSet.get("accessibility");
			String campaignType = dataSet.get("campaignType");
			String templateName = dataSet.get("templateName");
			String emailSubject = dataSet.get("emailSubject");
			String plainTextEditor = dataSet.get("plainTextEditor");
			CRMCampaignCenterPageTest campaignCenterPage = new CRMCampaignCenterPageTest();
			campaignCenterPage.createCampaign(driver, config, campaignName, accessibility, campaignType, templateName,
					emailSubject, plainTextEditor);

			fc.utobj().printTestStep("Create Template For Send Email");

			String templateNameForSendEmail = dataSet.get("templateNameForSendEmail");
			String emailSubjectForSendEmail = dataSet.get("emailSubjectForSendEmail");
			String plainTextEditorForSendEmail = dataSet.get("plainTextEditorForSendEmail");

			CRMCampaignCenterEmailTemplatesPageTest emailTemplatePage = new CRMCampaignCenterEmailTemplatesPageTest();
			emailTemplatePage.createTemplate(driver, config, templateNameForSendEmail, emailSubjectForSendEmail,
					plainTextEditorForSendEmail);

			fc.utobj().printTestStep("Navigate To CRM > Worflows Page");
			fc.utobj().printTestStep("Create Lead Type Event Based Workflow");

			String workFlowName = dataSet.get("workFlowName");
			String triggerType = dataSet.get("triggerType");
			String conditionType = dataSet.get("conditionType");
			String availableFields = dataSet.get("availableFields");
			String taskSubject = dataSet.get("taskSubject");
			String taskPriority = dataSet.get("taskPriority");
			String taskStatus = dataSet.get("taskStatus");
			String matchingCondition = dataSet.get("matchingCondition");

			createWorkFlowLeadEventBased(driver, config, dataSet, workFlowName, triggerType, conditionType,
					availableFields, matchingCondition, campaignName, leadStatus, templateNameForSendEmail, taskSubject,
					taskPriority, taskStatus);

			fc.utobj().printTestStep("Verify The Created WorkFlow");
			searchWorkFLowByFilter(driver, workFlowName);

			boolean isWorkFlowNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[.='" + workFlowName + "']");
			if (isWorkFlowNamePresent == false) {
				fc.utobj().throwsException("was not able to verify the workflow");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "crmworkflowTest")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Create The Event Based WorkFlow When Contact Is Added From Exisitng WebForm For Contact Matching Criteria Email Field is Not Empty", testCaseId = "TC_90_Create_Workflow")
	public void createWorkFlow90() throws IOException, Exception, ParserConfigurationException, SAXException {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crmworkflow", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			String emailId = "crmautomation@staffex.com";
			AdminUsersManageCorporateUsersAddCorporateUserPageTest addcorporateUserPage = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser = new CorporateUser();
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corpUser.setEmail(emailId);
			corpUser = addcorporateUserPage.createDefaultUser(driver, corpUser);

			/*
			 * fc.home_page().logout(driver, config);
			 * fc.utobj().printTestStep(testCaseId, "Login With Corporate User"
			 * ); fc.loginpage().loginWithParameter(driver, userName, password);
			 */

			fc.utobj().printTestStep("Navigate To Admin > CRM > Configure Status Page");
			fc.utobj().printTestStep("Add Contact Status");
			String contactStatus = dataSet.get("contactStatus");
			AdminCRMConfigureStatusPageTest statusPage = new AdminCRMConfigureStatusPageTest();
			statusPage.addStatus01(driver, contactStatus);

			// Add Web Form For Add Contact

			fc.utobj().printTestStep("Add Contact Web Form");
			String formName = dataSet.get("formName");
			String formTitle = dataSet.get("formTitle");
			String formUrl = dataSet.get("formUrl");

			AdminCRMManageWebFormGeneratorPage pobj = new AdminCRMManageWebFormGeneratorPage(driver);
			fc.crm().crm_common().adminCRMManageWebFormGeneratorLnk(driver);

			fc.utobj().clickElement(driver, pobj.createNewForm);
			fc.utobj().sendKeys(driver, pobj.formName, formName);
			fc.utobj().sendKeys(driver, pobj.formDisplayTitle, formTitle);
			if (!pobj.displayFormTitleCheckBox.isSelected()) {
				fc.utobj().clickElement(driver, pobj.displayFormTitleCheckBox);
			}

			if (!fc.utobj().getElement(driver, pobj.formTypeContact).isSelected()) {
				fc.utobj().clickElement(driver, pobj.formTypeContact);
			}
			fc.utobj().selectDropDown(driver, pobj.formFormat, "Single Page");

			fc.utobj().selectDropDown(driver, pobj.columnCount, "1");
			fc.utobj().selectDropDown(driver, pobj.filedLabelAlignment, "Left");

			fc.utobj().sendKeys(driver, pobj.iframeWidth, "100%");
			fc.utobj().sendKeys(driver, pobj.iframeHeight, "500");

			fc.utobj().sendKeys(driver, pobj.formUrl, formUrl);

			fc.utobj().clickElement(driver, pobj.saveNextBtn);

			// design Page
			fc.utobj().clickElement(driver, pobj.primaryInfo);
			fc.utobj().sendKeys(driver, pobj.searchField, "Birthdate");

			fc.utobj().dragAndDropElement(driver, driver.findElement(By.xpath(".//a[.='Birthdate']")),
					pobj.defaultDropHere);

			fc.utobj().sendKeys(driver, pobj.searchField, "Country");

			fc.utobj().dragAndDropElement(driver, driver.findElement(By.xpath(".//a[.='Country']")),
					pobj.defaultDropHere);

			fc.utobj().sendKeys(driver, pobj.searchField, "State / Province");

			fc.utobj().dragAndDropElement(driver, driver.findElement(By.xpath(".//a[.='State / Province']")),
					pobj.defaultDropHere);

			fc.utobj().clickElement(driver, pobj.saveAndNextBtnDesign);

			// fc.utobj().selectDropDown(driver, pobj.contactType, contactType);
			if (!fc.utobj().getElement(driver, pobj.assignToCorporate).isSelected()) {
				fc.utobj().clickElement(driver, pobj.assignToCorporate);
			}
			fc.utobj().selectDropDown(driver, pobj.selectCorporateUser, corpUser.getuserFullName());

			fc.utobj().clickElement(driver, pobj.finishBtn);

			if (!pobj.hostURL.isSelected()) {
				fc.utobj().clickElement(driver, pobj.hostURL);
			}

			// String urlText=pobj.hostCodeBox.getText().trim();
			fc.utobj().clickElement(driver, pobj.okBtn);

			fc.utobj().printTestStep("Navigate To CRM > Campaign Center");
			fc.crm().crm_common().CRMModule(driver);

			fc.utobj().printTestStep("Create Campaign For Send Campaign");

			String campaignName = dataSet.get("campaignName");
			String accessibility = dataSet.get("accessibility");
			String campaignType = dataSet.get("campaignType");
			String templateName = dataSet.get("templateName");
			String emailSubject = dataSet.get("emailSubject");
			String plainTextEditor = dataSet.get("plainTextEditor");
			CRMCampaignCenterPageTest campaignCenterPage = new CRMCampaignCenterPageTest();
			campaignCenterPage.createCampaign(driver, config, campaignName, accessibility, campaignType, templateName,
					emailSubject, plainTextEditor);

			fc.utobj().printTestStep("Create Template For Send Email");

			String templateNameForSendEmail = dataSet.get("templateNameForSendEmail");
			String emailSubjectForSendEmail = dataSet.get("emailSubjectForSendEmail");
			String plainTextEditorForSendEmail = dataSet.get("plainTextEditorForSendEmail");

			CRMCampaignCenterEmailTemplatesPageTest emailTemplatePage = new CRMCampaignCenterEmailTemplatesPageTest();
			emailTemplatePage.createTemplate(driver, config, templateNameForSendEmail, emailSubjectForSendEmail,
					plainTextEditorForSendEmail);

			fc.utobj().printTestStep("Navigate To CRM > Worflows Page");
			fc.utobj().printTestStep("Create Contact Type Event Based Workflow");

			String workFlowName = dataSet.get("workFlowName");
			String triggerType = dataSet.get("triggerType");
			String conditionType = dataSet.get("conditionType");
			String availableFields = dataSet.get("availableFields");
			String taskSubject = dataSet.get("taskSubject");
			String taskPriority = dataSet.get("taskPriority");
			String taskStatus = dataSet.get("taskStatus");
			String matchingCondition = dataSet.get("matchingCondition");

			createWorkFlowContactEventBased(driver, config, dataSet, workFlowName, triggerType, conditionType,
					availableFields, matchingCondition, campaignName, contactStatus, templateNameForSendEmail,
					taskSubject, taskPriority, taskStatus);

			fc.utobj().printTestStep("Verify The Created WorkFlow");
			searchWorkFLowByFilter(driver, workFlowName);

			boolean isWorkFlowNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[.='" + workFlowName + "']");
			if (isWorkFlowNamePresent == false) {
				fc.utobj().throwsException("was not able to verify the workflow");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "crmworkflowTest")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Create The Event Based WorkFlow When Contact Is Added From Existing Web Form For Contact Matching Criteria Status Of Contact is Default[New]", testCaseId = "TC_91_Create_Workflow")
	public void createWorkFlow91() throws IOException, Exception, ParserConfigurationException, SAXException {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crmworkflow", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			String emailId = "crmautomation@staffex.com";
			AdminUsersManageCorporateUsersAddCorporateUserPageTest addcorporateUserPage = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser = new CorporateUser();
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corpUser.setEmail(emailId);
			corpUser = addcorporateUserPage.createDefaultUser(driver, corpUser);

			/*
			 * fc.home_page().logout(driver, config);
			 * fc.utobj().printTestStep(testCaseId, "Login With Corporate User"
			 * ); fc.loginpage().loginWithParameter(driver, userName, password);
			 */

			fc.utobj().printTestStep("Navigate To Admin > CRM > Configure Status Page");
			fc.utobj().printTestStep("Add Contact Status");
			String contactStatus = dataSet.get("contactStatus");
			AdminCRMConfigureStatusPageTest statusPage = new AdminCRMConfigureStatusPageTest();
			statusPage.addStatus01(driver, contactStatus);

			// Add Web Form For Add Contact

			fc.utobj().printTestStep("Add Contact Web Form");
			String formName = dataSet.get("formName");
			String formTitle = dataSet.get("formTitle");
			String formUrl = dataSet.get("formUrl");

			AdminCRMManageWebFormGeneratorPage pobj = new AdminCRMManageWebFormGeneratorPage(driver);
			fc.crm().crm_common().adminCRMManageWebFormGeneratorLnk(driver);

			fc.utobj().clickElement(driver, pobj.createNewForm);
			fc.utobj().sendKeys(driver, pobj.formName, formName);
			fc.utobj().sendKeys(driver, pobj.formDisplayTitle, formTitle);
			if (!pobj.displayFormTitleCheckBox.isSelected()) {
				fc.utobj().clickElement(driver, pobj.displayFormTitleCheckBox);
			}

			if (!fc.utobj().getElement(driver, pobj.formTypeContact).isSelected()) {
				fc.utobj().clickElement(driver, pobj.formTypeContact);
			}
			fc.utobj().selectDropDown(driver, pobj.formFormat, "Single Page");

			fc.utobj().selectDropDown(driver, pobj.columnCount, "1");
			fc.utobj().selectDropDown(driver, pobj.filedLabelAlignment, "Left");

			fc.utobj().sendKeys(driver, pobj.iframeWidth, "100%");
			fc.utobj().sendKeys(driver, pobj.iframeHeight, "500");

			fc.utobj().sendKeys(driver, pobj.formUrl, formUrl);

			fc.utobj().clickElement(driver, pobj.saveNextBtn);

			// design Page
			fc.utobj().clickElement(driver, pobj.primaryInfo);
			fc.utobj().sendKeys(driver, pobj.searchField, "Birthdate");

			fc.utobj().dragAndDropElement(driver, driver.findElement(By.xpath(".//a[.='Birthdate']")),
					pobj.defaultDropHere);

			fc.utobj().sendKeys(driver, pobj.searchField, "Country");

			fc.utobj().dragAndDropElement(driver, driver.findElement(By.xpath(".//a[.='Country']")),
					pobj.defaultDropHere);

			fc.utobj().sendKeys(driver, pobj.searchField, "State / Province");

			fc.utobj().dragAndDropElement(driver, driver.findElement(By.xpath(".//a[.='State / Province']")),
					pobj.defaultDropHere);

			fc.utobj().clickElement(driver, pobj.saveAndNextBtnDesign);

			// fc.utobj().selectDropDown(driver, pobj.contactType, contactType);
			if (!fc.utobj().getElement(driver, pobj.assignToCorporate).isSelected()) {
				fc.utobj().clickElement(driver, pobj.assignToCorporate);
			}
			fc.utobj().selectDropDown(driver, pobj.selectCorporateUser, corpUser.getuserFullName());

			fc.utobj().clickElement(driver, pobj.finishBtn);

			if (!pobj.hostURL.isSelected()) {
				fc.utobj().clickElement(driver, pobj.hostURL);
			}

			// String urlText=pobj.hostCodeBox.getText().trim();
			fc.utobj().clickElement(driver, pobj.okBtn);

			fc.utobj().printTestStep("Navigate To CRM > Campaign Center");
			fc.crm().crm_common().CRMModule(driver);

			fc.utobj().printTestStep("Create Campaign For Send Campaign");

			String campaignName = dataSet.get("campaignName");
			String accessibility = dataSet.get("accessibility");
			String campaignType = dataSet.get("campaignType");
			String templateName = dataSet.get("templateName");
			String emailSubject = dataSet.get("emailSubject");
			String plainTextEditor = dataSet.get("plainTextEditor");
			CRMCampaignCenterPageTest campaignCenterPage = new CRMCampaignCenterPageTest();
			campaignCenterPage.createCampaign(driver, config, campaignName, accessibility, campaignType, templateName,
					emailSubject, plainTextEditor);

			fc.utobj().printTestStep("Create Template For Send Email");

			String templateNameForSendEmail = dataSet.get("templateNameForSendEmail");
			String emailSubjectForSendEmail = dataSet.get("emailSubjectForSendEmail");
			String plainTextEditorForSendEmail = dataSet.get("plainTextEditorForSendEmail");

			CRMCampaignCenterEmailTemplatesPageTest emailTemplatePage = new CRMCampaignCenterEmailTemplatesPageTest();
			emailTemplatePage.createTemplate(driver, config, templateNameForSendEmail, emailSubjectForSendEmail,
					plainTextEditorForSendEmail);

			fc.utobj().printTestStep("Navigate To CRM > Worflows Page");
			fc.utobj().printTestStep("Create Contact Type Event Based Workflow");

			String workFlowName = dataSet.get("workFlowName");
			String triggerType = dataSet.get("triggerType");
			String conditionType = dataSet.get("conditionType");
			String availableFields = dataSet.get("availableFields");
			String taskSubject = dataSet.get("taskSubject");
			String taskPriority = dataSet.get("taskPriority");
			String taskStatus = dataSet.get("taskStatus");
			String matchingCondition = dataSet.get("matchingCondition");

			createWorkFlowContactEventBased(driver, config, dataSet, workFlowName, triggerType, conditionType,
					availableFields, matchingCondition, campaignName, contactStatus, templateNameForSendEmail,
					taskSubject, taskPriority, taskStatus);

			fc.utobj().printTestStep("Verify The Created WorkFlow");
			searchWorkFLowByFilter(driver, workFlowName);

			boolean isWorkFlowNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[.='" + workFlowName + "']");
			if (isWorkFlowNamePresent == false) {
				fc.utobj().throwsException("was not able to verify the workflow");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "crmworkflowTest")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Create The Event Based WorkFlow When Contact Is Added For Contact Matching Criteria Email Subscription status is Set To Opted In", testCaseId = "TC_92_Create_Workflow")
	public void createWorkFlow92() throws IOException, Exception, ParserConfigurationException, SAXException {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crmworkflow", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			String emailId = "crmautomation@staffex.com";

			AdminUsersManageCorporateUsersAddCorporateUserPageTest addcorporateUserPage = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser = new CorporateUser();
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corpUser.setEmail(emailId);
			corpUser = addcorporateUserPage.createDefaultUser(driver, corpUser);

			/*
			 * fc.home_page().logout(driver, config);
			 * fc.utobj().printTestStep(testCaseId, "Login With Corporate User"
			 * ); fc.loginpage().loginWithParameter(driver, userName, password);
			 */

			fc.utobj().printTestStep("Navigate To Admin > CRM > Configure Status Page");
			fc.utobj().printTestStep("Add Contact Status");
			String contactStatus = dataSet.get("contactStatus");
			AdminCRMConfigureStatusPageTest statusPage = new AdminCRMConfigureStatusPageTest();
			statusPage.addStatus01(driver, contactStatus);

			// Add Web Form For Add Contact

			fc.utobj().printTestStep("Add Contact Web Form");
			String formName = dataSet.get("formName");
			String formTitle = dataSet.get("formTitle");
			String formUrl = dataSet.get("formUrl");

			AdminCRMManageWebFormGeneratorPage pobj = new AdminCRMManageWebFormGeneratorPage(driver);
			fc.crm().crm_common().adminCRMManageWebFormGeneratorLnk(driver);

			fc.utobj().clickElement(driver, pobj.createNewForm);
			fc.utobj().sendKeys(driver, pobj.formName, formName);
			fc.utobj().sendKeys(driver, pobj.formDisplayTitle, formTitle);
			if (!pobj.displayFormTitleCheckBox.isSelected()) {
				fc.utobj().clickElement(driver, pobj.displayFormTitleCheckBox);
			}

			if (!fc.utobj().getElement(driver, pobj.formTypeContact).isSelected()) {
				fc.utobj().clickElement(driver, pobj.formTypeContact);
			}
			fc.utobj().selectDropDown(driver, pobj.formFormat, "Single Page");

			fc.utobj().selectDropDown(driver, pobj.columnCount, "1");
			fc.utobj().selectDropDown(driver, pobj.filedLabelAlignment, "Left");

			fc.utobj().sendKeys(driver, pobj.iframeWidth, "100%");
			fc.utobj().sendKeys(driver, pobj.iframeHeight, "500");

			fc.utobj().sendKeys(driver, pobj.formUrl, formUrl);

			fc.utobj().clickElement(driver, pobj.saveNextBtn);

			// design Page
			fc.utobj().clickElement(driver, pobj.primaryInfo);
			fc.utobj().sendKeys(driver, pobj.searchField, "Birthdate");

			fc.utobj().dragAndDropElement(driver, driver.findElement(By.xpath(".//a[.='Birthdate']")),
					pobj.defaultDropHere);

			fc.utobj().sendKeys(driver, pobj.searchField, "Country");

			fc.utobj().dragAndDropElement(driver, driver.findElement(By.xpath(".//a[.='Country']")),
					pobj.defaultDropHere);

			fc.utobj().sendKeys(driver, pobj.searchField, "State / Province");

			fc.utobj().dragAndDropElement(driver, driver.findElement(By.xpath(".//a[.='State / Province']")),
					pobj.defaultDropHere);

			fc.utobj().clickElement(driver, pobj.saveAndNextBtnDesign);

			// fc.utobj().selectDropDown(driver, pobj.contactType, contactType);
			if (!fc.utobj().getElement(driver, pobj.assignToCorporate).isSelected()) {
				fc.utobj().clickElement(driver, pobj.assignToCorporate);
			}
			fc.utobj().selectDropDown(driver, pobj.selectCorporateUser, corpUser.getuserFullName());

			fc.utobj().clickElement(driver, pobj.finishBtn);

			if (!pobj.hostURL.isSelected()) {
				fc.utobj().clickElement(driver, pobj.hostURL);
			}

			// String urlText=pobj.hostCodeBox.getText().trim();
			fc.utobj().clickElement(driver, pobj.okBtn);

			fc.utobj().printTestStep("Navigate To CRM > Campaign Center");
			fc.crm().crm_common().CRMModule(driver);

			fc.utobj().printTestStep("Create Campaign For Send Campaign");

			String campaignName = dataSet.get("campaignName");
			String accessibility = dataSet.get("accessibility");
			String campaignType = dataSet.get("campaignType");
			String templateName = dataSet.get("templateName");
			String emailSubject = dataSet.get("emailSubject");
			String plainTextEditor = dataSet.get("plainTextEditor");
			CRMCampaignCenterPageTest campaignCenterPage = new CRMCampaignCenterPageTest();
			campaignCenterPage.createCampaign(driver, config, campaignName, accessibility, campaignType, templateName,
					emailSubject, plainTextEditor);

			fc.utobj().printTestStep("Create Template For Send Email");

			String templateNameForSendEmail = dataSet.get("templateNameForSendEmail");
			String emailSubjectForSendEmail = dataSet.get("emailSubjectForSendEmail");
			String plainTextEditorForSendEmail = dataSet.get("plainTextEditorForSendEmail");

			CRMCampaignCenterEmailTemplatesPageTest emailTemplatePage = new CRMCampaignCenterEmailTemplatesPageTest();
			emailTemplatePage.createTemplate(driver, config, templateNameForSendEmail, emailSubjectForSendEmail,
					plainTextEditorForSendEmail);

			fc.utobj().printTestStep("Navigate To CRM > Worflows Page");
			fc.utobj().printTestStep("Create Contact Type Event Based Workflow");

			String workFlowName = dataSet.get("workFlowName");
			String triggerType = dataSet.get("triggerType");
			String conditionType = dataSet.get("conditionType");
			String availableFields = dataSet.get("availableFields");
			String taskSubject = dataSet.get("taskSubject");
			String taskPriority = dataSet.get("taskPriority");
			String taskStatus = dataSet.get("taskStatus");
			String matchingCondition = dataSet.get("matchingCondition");

			createWorkFlowContactEventBased(driver, config, dataSet, workFlowName, triggerType, conditionType,
					availableFields, matchingCondition, campaignName, contactStatus, templateNameForSendEmail,
					taskSubject, taskPriority, taskStatus);

			fc.utobj().printTestStep("Verify The Created WorkFlow");
			searchWorkFLowByFilter(driver, workFlowName);

			boolean isWorkFlowNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[.='" + workFlowName + "']");
			if (isWorkFlowNamePresent == false) {
				fc.utobj().throwsException("was not able to verify the workflow");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "crmworkflowTest")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Create The Event Based WorkFlow When Contact Is Added From Existing Web Form For Contact Matching Criteria Country And All States of It", testCaseId = "TC_93_Create_Workflow")
	public void createWorkFlow93() throws IOException, Exception, ParserConfigurationException, SAXException {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crmworkflow", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			String emailId = "crmautomation@staffex.com";
			AdminUsersManageCorporateUsersAddCorporateUserPageTest addcorporateUserPage = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser = new CorporateUser();
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corpUser.setEmail(emailId);
			corpUser = addcorporateUserPage.createDefaultUser(driver, corpUser);

			/*
			 * fc.home_page().logout(driver, config);
			 * fc.utobj().printTestStep(testCaseId, "Login With Corporate User"
			 * ); fc.loginpage().loginWithParameter(driver, userName, password);
			 */

			fc.utobj().printTestStep("Navigate To Admin > CRM > Configure Status Page");
			fc.utobj().printTestStep("Add Contact Status");
			String contactStatus = dataSet.get("contactStatus");
			AdminCRMConfigureStatusPageTest statusPage = new AdminCRMConfigureStatusPageTest();
			statusPage.addStatus01(driver, contactStatus);

			// Add Web Form For Add Contact

			fc.utobj().printTestStep("Add Contact Web Form");
			String formName = dataSet.get("formName");
			String formTitle = dataSet.get("formTitle");
			String formUrl = dataSet.get("formUrl");

			AdminCRMManageWebFormGeneratorPage pobj = new AdminCRMManageWebFormGeneratorPage(driver);
			fc.crm().crm_common().adminCRMManageWebFormGeneratorLnk(driver);

			fc.utobj().clickElement(driver, pobj.createNewForm);
			fc.utobj().sendKeys(driver, pobj.formName, formName);
			fc.utobj().sendKeys(driver, pobj.formDisplayTitle, formTitle);
			if (!pobj.displayFormTitleCheckBox.isSelected()) {
				fc.utobj().clickElement(driver, pobj.displayFormTitleCheckBox);
			}

			if (!fc.utobj().getElement(driver, pobj.formTypeContact).isSelected()) {
				fc.utobj().clickElement(driver, pobj.formTypeContact);
			}
			fc.utobj().selectDropDown(driver, pobj.formFormat, "Single Page");

			fc.utobj().selectDropDown(driver, pobj.columnCount, "1");
			fc.utobj().selectDropDown(driver, pobj.filedLabelAlignment, "Left");

			fc.utobj().sendKeys(driver, pobj.iframeWidth, "100%");
			fc.utobj().sendKeys(driver, pobj.iframeHeight, "500");

			fc.utobj().sendKeys(driver, pobj.formUrl, formUrl);

			fc.utobj().clickElement(driver, pobj.saveNextBtn);

			// design Page
			fc.utobj().clickElement(driver, pobj.primaryInfo);
			fc.utobj().sendKeys(driver, pobj.searchField, "Birthdate");

			fc.utobj().dragAndDropElement(driver, driver.findElement(By.xpath(".//a[.='Birthdate']")),
					pobj.defaultDropHere);

			fc.utobj().sendKeys(driver, pobj.searchField, "Country");

			fc.utobj().dragAndDropElement(driver, driver.findElement(By.xpath(".//a[.='Country']")),
					pobj.defaultDropHere);

			fc.utobj().sendKeys(driver, pobj.searchField, "State / Province");

			fc.utobj().dragAndDropElement(driver, driver.findElement(By.xpath(".//a[.='State / Province']")),
					pobj.defaultDropHere);

			fc.utobj().clickElement(driver, pobj.saveAndNextBtnDesign);

			// fc.utobj().selectDropDown(driver, pobj.contactType, contactType);
			if (!fc.utobj().getElement(driver, pobj.assignToCorporate).isSelected()) {
				fc.utobj().clickElement(driver, pobj.assignToCorporate);
			}
			fc.utobj().selectDropDown(driver, pobj.selectCorporateUser, corpUser.getuserFullName());

			fc.utobj().clickElement(driver, pobj.finishBtn);

			if (!pobj.hostURL.isSelected()) {
				fc.utobj().clickElement(driver, pobj.hostURL);
			}

			// String urlText=pobj.hostCodeBox.getText().trim();
			fc.utobj().clickElement(driver, pobj.okBtn);

			fc.utobj().printTestStep("Navigate To CRM > Campaign Center");
			fc.crm().crm_common().CRMModule(driver);

			fc.utobj().printTestStep("Create Campaign For Send Campaign");

			String campaignName = dataSet.get("campaignName");
			String accessibility = dataSet.get("accessibility");
			String campaignType = dataSet.get("campaignType");
			String templateName = dataSet.get("templateName");
			String emailSubject = dataSet.get("emailSubject");
			String plainTextEditor = dataSet.get("plainTextEditor");
			CRMCampaignCenterPageTest campaignCenterPage = new CRMCampaignCenterPageTest();
			campaignCenterPage.createCampaign(driver, config, campaignName, accessibility, campaignType, templateName,
					emailSubject, plainTextEditor);

			fc.utobj().printTestStep("Create Template For Send Email");

			String templateNameForSendEmail = dataSet.get("templateNameForSendEmail");
			String emailSubjectForSendEmail = dataSet.get("emailSubjectForSendEmail");
			String plainTextEditorForSendEmail = dataSet.get("plainTextEditorForSendEmail");

			CRMCampaignCenterEmailTemplatesPageTest emailTemplatePage = new CRMCampaignCenterEmailTemplatesPageTest();
			emailTemplatePage.createTemplate(driver, config, templateNameForSendEmail, emailSubjectForSendEmail,
					plainTextEditorForSendEmail);

			fc.utobj().printTestStep("Navigate To CRM > Worflows Page");
			fc.utobj().printTestStep("Create Contact Type Event Based Workflow");

			String workFlowName = dataSet.get("workFlowName");
			String triggerType = dataSet.get("triggerType");
			String conditionType = dataSet.get("conditionType");
			String availableFields = dataSet.get("availableFields");
			String taskSubject = dataSet.get("taskSubject");
			String taskPriority = dataSet.get("taskPriority");
			String taskStatus = dataSet.get("taskStatus");
			String matchingCondition = dataSet.get("matchingCondition");

			createWorkFlowContactEventBased(driver, config, dataSet, workFlowName, triggerType, conditionType,
					availableFields, matchingCondition, campaignName, contactStatus, templateNameForSendEmail,
					taskSubject, taskPriority, taskStatus);

			fc.utobj().printTestStep("Verify The Created WorkFlow");
			searchWorkFLowByFilter(driver, workFlowName);

			boolean isWorkFlowNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[.='" + workFlowName + "']");
			if (isWorkFlowNamePresent == false) {
				fc.utobj().throwsException("was not able to verify the workflow");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "crmworkflowTest")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Create The Event Based WorkFlow When Contact Is Added From Existing Web Form For Contact Matching Criteria BirthDate And Set To It , is Between", testCaseId = "TC_94_Create_Workflow")
	public void createWorkFlow94() throws IOException, Exception, ParserConfigurationException, SAXException {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crmworkflow", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			String emailId = "crmautomation@staffex.com";
			AdminUsersManageCorporateUsersAddCorporateUserPageTest addcorporateUserPage = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser = new CorporateUser();
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corpUser.setEmail(emailId);
			corpUser = addcorporateUserPage.createDefaultUser(driver, corpUser);

			/*
			 * fc.home_page().logout(driver, config);
			 * fc.utobj().printTestStep(testCaseId, "Login With Corporate User"
			 * ); fc.loginpage().loginWithParameter(driver, userName, password);
			 */

			fc.utobj().printTestStep("Navigate To Admin > CRM > Configure Status Page");
			fc.utobj().printTestStep("Add Contact Status");
			String contactStatus = dataSet.get("contactStatus");
			AdminCRMConfigureStatusPageTest statusPage = new AdminCRMConfigureStatusPageTest();
			statusPage.addStatus01(driver, contactStatus);

			// Add Web Form For Add Contact

			fc.utobj().printTestStep("Add Contact Web Form");
			String formName = dataSet.get("formName");
			String formTitle = dataSet.get("formTitle");
			String formUrl = dataSet.get("formUrl");

			AdminCRMManageWebFormGeneratorPage pobj = new AdminCRMManageWebFormGeneratorPage(driver);
			fc.crm().crm_common().adminCRMManageWebFormGeneratorLnk(driver);

			fc.utobj().clickElement(driver, pobj.createNewForm);
			fc.utobj().sendKeys(driver, pobj.formName, formName);
			fc.utobj().sendKeys(driver, pobj.formDisplayTitle, formTitle);
			if (!pobj.displayFormTitleCheckBox.isSelected()) {
				fc.utobj().clickElement(driver, pobj.displayFormTitleCheckBox);
			}

			if (!fc.utobj().getElement(driver, pobj.formTypeContact).isSelected()) {
				fc.utobj().clickElement(driver, pobj.formTypeContact);
			}
			fc.utobj().selectDropDown(driver, pobj.formFormat, "Single Page");

			fc.utobj().selectDropDown(driver, pobj.columnCount, "1");
			fc.utobj().selectDropDown(driver, pobj.filedLabelAlignment, "Left");

			fc.utobj().sendKeys(driver, pobj.iframeWidth, "100%");
			fc.utobj().sendKeys(driver, pobj.iframeHeight, "500");

			fc.utobj().sendKeys(driver, pobj.formUrl, formUrl);

			fc.utobj().clickElement(driver, pobj.saveNextBtn);

			// design Page
			fc.utobj().clickElement(driver, pobj.primaryInfo);
			fc.utobj().sendKeys(driver, pobj.searchField, "Birthdate");

			fc.utobj().dragAndDropElement(driver, driver.findElement(By.xpath(".//a[.='Birthdate']")),
					pobj.defaultDropHere);

			fc.utobj().sendKeys(driver, pobj.searchField, "Country");

			fc.utobj().dragAndDropElement(driver, driver.findElement(By.xpath(".//a[.='Country']")),
					pobj.defaultDropHere);

			fc.utobj().sendKeys(driver, pobj.searchField, "State / Province");

			fc.utobj().dragAndDropElement(driver, driver.findElement(By.xpath(".//a[.='State / Province']")),
					pobj.defaultDropHere);

			fc.utobj().clickElement(driver, pobj.saveAndNextBtnDesign);

			// fc.utobj().selectDropDown(driver, pobj.contactType, contactType);
			if (!fc.utobj().getElement(driver, pobj.assignToCorporate).isSelected()) {
				fc.utobj().clickElement(driver, pobj.assignToCorporate);
			}
			fc.utobj().selectDropDown(driver, pobj.selectCorporateUser, corpUser.getuserFullName());

			fc.utobj().clickElement(driver, pobj.finishBtn);

			if (!pobj.hostURL.isSelected()) {
				fc.utobj().clickElement(driver, pobj.hostURL);
			}

			// String urlText=pobj.hostCodeBox.getText().trim();
			fc.utobj().clickElement(driver, pobj.okBtn);

			fc.utobj().printTestStep("Navigate To CRM > Campaign Center");
			fc.crm().crm_common().CRMModule(driver);

			fc.utobj().printTestStep("Create Campaign For Send Campaign");

			String campaignName = dataSet.get("campaignName");
			String accessibility = dataSet.get("accessibility");
			String campaignType = dataSet.get("campaignType");
			String templateName = dataSet.get("templateName");
			String emailSubject = dataSet.get("emailSubject");
			String plainTextEditor = dataSet.get("plainTextEditor");
			CRMCampaignCenterPageTest campaignCenterPage = new CRMCampaignCenterPageTest();
			campaignCenterPage.createCampaign(driver, config, campaignName, accessibility, campaignType, templateName,
					emailSubject, plainTextEditor);

			fc.utobj().printTestStep("Create Template For Send Email");

			String templateNameForSendEmail = dataSet.get("templateNameForSendEmail");
			String emailSubjectForSendEmail = dataSet.get("emailSubjectForSendEmail");
			String plainTextEditorForSendEmail = dataSet.get("plainTextEditorForSendEmail");

			CRMCampaignCenterEmailTemplatesPageTest emailTemplatePage = new CRMCampaignCenterEmailTemplatesPageTest();
			emailTemplatePage.createTemplate(driver, config, templateNameForSendEmail, emailSubjectForSendEmail,
					plainTextEditorForSendEmail);

			fc.utobj().printTestStep("Navigate To CRM > Worflows Page");
			fc.utobj().printTestStep("Create Contact Type Event Based Workflow");

			String workFlowName = dataSet.get("workFlowName");
			String triggerType = dataSet.get("triggerType");
			String conditionType = dataSet.get("conditionType");
			String availableFields = dataSet.get("availableFields");
			String taskSubject = dataSet.get("taskSubject");
			String taskPriority = dataSet.get("taskPriority");
			String taskStatus = dataSet.get("taskStatus");
			String matchingCondition = dataSet.get("matchingCondition");

			createWorkFlowContactEventBased(driver, config, dataSet, workFlowName, triggerType, conditionType,
					availableFields, matchingCondition, campaignName, contactStatus, templateNameForSendEmail,
					taskSubject, taskPriority, taskStatus);

			fc.utobj().printTestStep("Verify The Created WorkFlow");
			searchWorkFLowByFilter(driver, workFlowName);

			boolean isWorkFlowNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[.='" + workFlowName + "']");
			if (isWorkFlowNamePresent == false) {
				fc.utobj().throwsException("was not able to verify the workflow");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "crmworkflowTest")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Create The Event Based WorkFlow When Contact Is Added From Existing Web Form For Contact Matching Criteria Contact Type In", testCaseId = "TC_95_Create_Workflow")
	public void createWorkFlow95() throws IOException, Exception, ParserConfigurationException, SAXException {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crmworkflow", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			String emailId = "crmautomation@staffex.com";
			AdminUsersManageCorporateUsersAddCorporateUserPageTest addcorporateUserPage = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser = new CorporateUser();
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corpUser.setEmail(emailId);
			corpUser = addcorporateUserPage.createDefaultUser(driver, corpUser);

			/*
			 * fc.home_page().logout(driver, config);
			 * fc.utobj().printTestStep(testCaseId, "Login With Corporate User"
			 * ); fc.loginpage().loginWithParameter(driver, userName, password);
			 */

			fc.utobj().printTestStep("Navigate To Admin > CRM > Configure Status Page");
			fc.utobj().printTestStep("Add Contact Status");
			String contactStatus = dataSet.get("contactStatus");
			AdminCRMConfigureStatusPageTest statusPage = new AdminCRMConfigureStatusPageTest();
			statusPage.addStatus01(driver, contactStatus);

			fc.utobj().printTestStep("Navigate To Admin > CRM > Contact Type Configureation");
			fc.utobj().printTestStep("Add Contact Type");
			String contactType = dataSet.get("selectcontactType");
			AdminCRMContactTypeConfigurationPageTest contactTypeConfigureationPage = new AdminCRMContactTypeConfigurationPageTest();
			contactTypeConfigureationPage.addContactType(driver, contactType);

			// Add Web Form For Add Contact

			fc.utobj().printTestStep("Add Contact Web Form");
			String formName = dataSet.get("formName");
			String formTitle = dataSet.get("formTitle");
			String formUrl = dataSet.get("formUrl");

			AdminCRMManageWebFormGeneratorPage pobj = new AdminCRMManageWebFormGeneratorPage(driver);
			fc.crm().crm_common().adminCRMManageWebFormGeneratorLnk(driver);

			fc.utobj().clickElement(driver, pobj.createNewForm);
			fc.utobj().sendKeys(driver, pobj.formName, formName);
			fc.utobj().sendKeys(driver, pobj.formDisplayTitle, formTitle);
			if (!pobj.displayFormTitleCheckBox.isSelected()) {
				fc.utobj().clickElement(driver, pobj.displayFormTitleCheckBox);
			}

			if (!fc.utobj().getElement(driver, pobj.formTypeContact).isSelected()) {
				fc.utobj().clickElement(driver, pobj.formTypeContact);
			}
			fc.utobj().selectDropDown(driver, pobj.formFormat, "Single Page");

			fc.utobj().selectDropDown(driver, pobj.columnCount, "1");
			fc.utobj().selectDropDown(driver, pobj.filedLabelAlignment, "Left");

			fc.utobj().sendKeys(driver, pobj.iframeWidth, "100%");
			fc.utobj().sendKeys(driver, pobj.iframeHeight, "500");

			fc.utobj().sendKeys(driver, pobj.formUrl, formUrl);

			fc.utobj().clickElement(driver, pobj.saveNextBtn);

			// design Page
			fc.utobj().clickElement(driver, pobj.primaryInfo);
			fc.utobj().sendKeys(driver, pobj.searchField, "Birthdate");

			fc.utobj().dragAndDropElement(driver, driver.findElement(By.xpath(".//a[.='Birthdate']")),
					pobj.defaultDropHere);

			fc.utobj().sendKeys(driver, pobj.searchField, "Country");

			fc.utobj().dragAndDropElement(driver, driver.findElement(By.xpath(".//a[.='Country']")),
					pobj.defaultDropHere);

			fc.utobj().sendKeys(driver, pobj.searchField, "State / Province");

			fc.utobj().dragAndDropElement(driver, driver.findElement(By.xpath(".//a[.='State / Province']")),
					pobj.defaultDropHere);

			fc.utobj().clickElement(driver, pobj.saveAndNextBtnDesign);

			fc.utobj().selectDropDown(driver, pobj.contactType, contactType);
			if (!fc.utobj().getElement(driver, pobj.assignToCorporate).isSelected()) {
				fc.utobj().clickElement(driver, pobj.assignToCorporate);
			}
			fc.utobj().selectDropDown(driver, pobj.selectCorporateUser, corpUser.getuserFullName());

			fc.utobj().clickElement(driver, pobj.finishBtn);

			if (!pobj.hostURL.isSelected()) {
				fc.utobj().clickElement(driver, pobj.hostURL);
			}

			// String urlText=pobj.hostCodeBox.getText().trim();
			fc.utobj().clickElement(driver, pobj.okBtn);

			fc.utobj().printTestStep("Navigate To CRM > Campaign Center");
			fc.crm().crm_common().CRMModule(driver);

			fc.utobj().printTestStep("Create Campaign For Send Campaign");

			String campaignName = dataSet.get("campaignName");
			String accessibility = dataSet.get("accessibility");
			String campaignType = dataSet.get("campaignType");
			String templateName = dataSet.get("templateName");
			String emailSubject = dataSet.get("emailSubject");
			String plainTextEditor = dataSet.get("plainTextEditor");
			CRMCampaignCenterPageTest campaignCenterPage = new CRMCampaignCenterPageTest();
			campaignCenterPage.createCampaign(driver, config, campaignName, accessibility, campaignType, templateName,
					emailSubject, plainTextEditor);

			fc.utobj().printTestStep("Create Template For Send Email");

			String templateNameForSendEmail = dataSet.get("templateNameForSendEmail");
			String emailSubjectForSendEmail = dataSet.get("emailSubjectForSendEmail");
			String plainTextEditorForSendEmail = dataSet.get("plainTextEditorForSendEmail");

			CRMCampaignCenterEmailTemplatesPageTest emailTemplatePage = new CRMCampaignCenterEmailTemplatesPageTest();
			emailTemplatePage.createTemplate(driver, config, templateNameForSendEmail, emailSubjectForSendEmail,
					plainTextEditorForSendEmail);

			fc.utobj().printTestStep("Navigate To CRM > Worflows Page");
			fc.utobj().printTestStep("Create Contact Type Event Based Workflow");

			String workFlowName = dataSet.get("workFlowName");
			String triggerType = dataSet.get("triggerType");
			String conditionType = dataSet.get("conditionType");
			String availableFields = dataSet.get("availableFields");
			String taskSubject = dataSet.get("taskSubject");
			String taskPriority = dataSet.get("taskPriority");
			String taskStatus = dataSet.get("taskStatus");
			String matchingCondition = dataSet.get("matchingCondition");

			createWorkFlowContactEventBased(driver, config, dataSet, workFlowName, triggerType, conditionType,
					availableFields, matchingCondition, campaignName, contactStatus, templateNameForSendEmail,
					taskSubject, taskPriority, taskStatus);

			fc.utobj().printTestStep("Verify The Created WorkFlow");
			searchWorkFLowByFilter(driver, workFlowName);

			boolean isWorkFlowNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[.='" + workFlowName + "']");
			if (isWorkFlowNamePresent == false) {
				fc.utobj().throwsException("was not able to verify the workflow");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "crmworkflowTest")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Create The Event Based WorkFlow When Contact Status Is Changed For Contact Matching Criteria Email Field is Not Empty", testCaseId = "TC_96_Create_Workflow")
	public void createWorkFlow96() throws IOException, Exception, ParserConfigurationException, SAXException {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crmworkflow", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			/*
			 * 
			 * String
			 * userName=fc.utobj().generateTestData(dataSet.get("userName"));
			 * String
			 * password=fc.utobj().generateTestData(dataSet.get("password"));
			 * String emailId="crmautomation@staffex.com";
			 * AdminUsersManageCorporateUsersAddCorporateUserPageTest
			 * addcorporateUserPage=new
			 * AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			 * addcorporateUserPage.addCorporateUser(driver, userName, password,
			 * config , emailId);
			 * 
			 * fc.home_page().logout(driver, config);
			 * fc.utobj().printTestStep(testCaseId, "Login With Corporate User"
			 * ); fc.loginpage().loginWithParameter(driver, userName, password);
			 */

			fc.utobj().printTestStep("Navigate To Admin > CRM > Configure Status Page");
			fc.utobj().printTestStep("Add Contact Status");
			String contactStatus = dataSet.get("contactStatus");
			AdminCRMConfigureStatusPageTest statusPage = new AdminCRMConfigureStatusPageTest();
			statusPage.addStatus01(driver, contactStatus);

			String contactStatusChanged = dataSet.get("contactStatusChanged");
			statusPage.addStatus01(driver, contactStatusChanged);

			fc.utobj().printTestStep("Navigate To CRM > Campaign Center");
			fc.crm().crm_common().CRMModule(driver);

			fc.utobj().printTestStep("Create Campaign For Send Campaign");

			String campaignName = dataSet.get("campaignName");
			String accessibility = dataSet.get("accessibility");
			String campaignType = dataSet.get("campaignType");
			String templateName = dataSet.get("templateName");
			String emailSubject = dataSet.get("emailSubject");
			String plainTextEditor = dataSet.get("plainTextEditor");
			CRMCampaignCenterPageTest campaignCenterPage = new CRMCampaignCenterPageTest();
			campaignCenterPage.createCampaign(driver, config, campaignName, accessibility, campaignType, templateName,
					emailSubject, plainTextEditor);

			fc.utobj().printTestStep("Create Template For Send Email");

			String templateNameForSendEmail = dataSet.get("templateNameForSendEmail");
			String emailSubjectForSendEmail = dataSet.get("emailSubjectForSendEmail");
			String plainTextEditorForSendEmail = dataSet.get("plainTextEditorForSendEmail");

			CRMCampaignCenterEmailTemplatesPageTest emailTemplatePage = new CRMCampaignCenterEmailTemplatesPageTest();
			emailTemplatePage.createTemplate(driver, config, templateNameForSendEmail, emailSubjectForSendEmail,
					plainTextEditorForSendEmail);

			fc.utobj().printTestStep("Navigate To CRM > Worflows Page");
			fc.utobj().printTestStep("Create Contact Type Event Based Workflow");

			String workFlowName = dataSet.get("workFlowName");
			String triggerType = dataSet.get("triggerType");
			String conditionType = dataSet.get("conditionType");
			String availableFields = dataSet.get("availableFields");
			String taskSubject = dataSet.get("taskSubject");
			String taskPriority = dataSet.get("taskPriority");
			String taskStatus = dataSet.get("taskStatus");
			String matchingCondition = dataSet.get("matchingCondition");

			createWorkFlowContactEventBased(driver, config, dataSet, workFlowName, triggerType, conditionType,
					availableFields, matchingCondition, campaignName, contactStatus, templateNameForSendEmail,
					taskSubject, taskPriority, taskStatus);

			fc.utobj().printTestStep("Verify The Created WorkFlow");
			searchWorkFLowByFilter(driver, workFlowName);

			boolean isWorkFlowNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[.='" + workFlowName + "']");
			if (isWorkFlowNamePresent == false) {
				fc.utobj().throwsException("was not able to verify the workflow");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	/*
	 * @Test(groups = "crmworkflownouse")
	 * 
	 * @TestCase(createdOn="2017-05-01",updatedOn="2018-06-14",
	 * testCaseDescription =
	 * "Create The Date Based WorkFlow of Anniversary Type And Date of Execution At After Days When Contact Is Added For All Contacts"
	 * , testCaseId = "TC_75_Create_Workflow") public void
	 * createWorkFlow75()throws IOException, Exception,
	 * ParserConfigurationException, SAXException {
	 * 
	 * String testCaseId =
	 * fc.utobj().readTestCaseInfo(this.getClass().getName()+"."+new
	 * Object(){}.getClass().getEnclosingMethod().getName());
	 * 
	 * Map<String, String> config =
	 * fc.utobj().readConfigurationFile(FranconnectUtil.path);
	 * Map<String,String> dataSet = fc.utobj().readTestData("crmworkflow",
	 * testCaseId); WebDriver driver = fc.utobj().openDriver(config);
	 * 
	 * try { driver = fc.loginpage().login(driver, config);
	 * 
	 * String userName=fc.utobj().generateTestData(dataSet.get("userName"));
	 * String password=fc.utobj().generateTestData(dataSet.get("password"));
	 * AdminUsersManageCorporateUsersAddCorporateUserPageTest
	 * addcorporateUserPage=new
	 * AdminUsersManageCorporateUsersAddCorporateUserPageTest();
	 * addcorporateUserPage.addCorporateUser(driver, userName, password, config
	 * , emailId);
	 * 
	 * fc.home_page().logout(driver, config);
	 * fc.utobj().printTestStep(testCaseId, "Login With Corporate User");
	 * fc.loginpage().loginWithParameter(driver, userName, password);
	 * 
	 * fc.utobj().printTestStep(testCaseId,
	 * "Navigate To Admin > CRM > Configure Status Page");
	 * fc.utobj().printTestStep(testCaseId, "Add Contact Status"); String
	 * contactStatus=dataSet.get("contactStatus");
	 * AdminCRMConfigureStatusPageTest statusPage=new
	 * AdminCRMConfigureStatusPageTest(); statusPage.addStatus01(driver,
	 * contactStatus, config);
	 * 
	 * fc.utobj().printTestStep(testCaseId, "Navigate To CRM > Campaign Center"
	 * ); fc.crm().crm_common().CRMModule(driver);
	 * 
	 * fc.utobj().printTestStep(testCaseId, "Create Campaign For Send Campaign"
	 * );
	 * 
	 * String campaignName=dataSet.get("campaignName"); String
	 * accessibility=dataSet.get("accessibility"); String
	 * campaignType=dataSet.get("campaignType"); String
	 * templateName=dataSet.get("templateName"); String
	 * emailSubject=dataSet.get("emailSubject"); String
	 * plainTextEditor=dataSet.get("plainTextEditor"); CRMCampaignCenterPageTest
	 * campaignCenterPage=new CRMCampaignCenterPageTest();
	 * campaignCenterPage.createCampaign(driver, config, campaignName,
	 * accessibility, campaignType, templateName, emailSubject,
	 * plainTextEditor);
	 * 
	 * fc.utobj().printTestStep(testCaseId, "Create Template For Send Email");
	 * 
	 * String templateNameForSendEmail=dataSet.get("templateNameForSendEmail");
	 * String emailSubjectForSendEmail=dataSet.get("emailSubjectForSendEmail");
	 * String
	 * plainTextEditorForSendEmail=dataSet.get("plainTextEditorForSendEmail");
	 * 
	 * CRMCampaignCenterEmailTemplatesPageTest emailTemplatePage=new
	 * CRMCampaignCenterEmailTemplatesPageTest();
	 * emailTemplatePage.createTemplate(driver, config,
	 * templateNameForSendEmail, emailSubjectForSendEmail,
	 * plainTextEditorForSendEmail);
	 * 
	 * fc.utobj().printTestStep(testCaseId, "Navigate To CRM > Worflows Page");
	 * fc.utobj().printTestStep(testCaseId,
	 * "Create Contact Type Standard Workflow");
	 * 
	 * String workFlowName=dataSet.get("workFlowName"); String
	 * triggerType=dataSet.get("triggerType"); String
	 * conditionType=dataSet.get("conditionType"); String availableFields="";
	 * String taskSubject=dataSet.get("taskSubject"); String
	 * taskPriority=dataSet.get("taskPriority"); String
	 * taskStatus=dataSet.get("taskStatus"); String matchingCondition=""; String
	 * dateOfExecution=dataSet.get("dateOfExecution");
	 * 
	 * createWorkFlowContactDateBased(driver, config, dataSet, workFlowName,
	 * triggerType, dateOfExecution, conditionType, availableFields,
	 * matchingCondition, campaignName, contactStatus, templateNameForSendEmail,
	 * taskSubject, taskPriority, taskStatus);
	 * 
	 * 
	 * fc.utobj().logoutAndQuitBrowser(driver, config, testCaseId); } catch
	 * (Exception e) {
	 * 
	 * fc.utobj().quitBrowserOnCatch(driver, config, e, testCaseId);} }
	 * 
	 * 
	 * 
	 * @Test(groups = "crmworkflow")
	 * 
	 * @TestCase(createdOn="2017-05-01",updatedOn="2018-06-14",
	 * testCaseDescription =
	 * "Create The Date Based WorkFlow of Last Purchase Date Type And Date of Execution At After Days When Contact Is Added For Contact Matching Criteria Email Field is Not Empty"
	 * , testCaseId = "TC_76_Create_Workflow") public void
	 * createWorkFlow76()throws IOException, Exception,
	 * ParserConfigurationException, SAXException {
	 * 
	 * String testCaseId =
	 * fc.utobj().readTestCaseInfo(this.getClass().getName()+"."+new
	 * Object(){}.getClass().getEnclosingMethod().getName());
	 * 
	 * Map<String, String> config =
	 * fc.utobj().readConfigurationFile(FranconnectUtil.path);
	 * Map<String,String> dataSet = fc.utobj().readTestData("crmworkflow",
	 * testCaseId); WebDriver driver = fc.utobj().openDriver(config);
	 * 
	 * try { driver = fc.loginpage().login(driver, config);
	 * 
	 * String userName=fc.utobj().generateTestData(dataSet.get("userName"));
	 * String password=fc.utobj().generateTestData(dataSet.get("password"));
	 * AdminUsersManageCorporateUsersAddCorporateUserPageTest
	 * addcorporateUserPage=new
	 * AdminUsersManageCorporateUsersAddCorporateUserPageTest();
	 * addcorporateUserPage.addCorporateUser(driver, userName, password, config
	 * , emailId);
	 * 
	 * fc.home_page().logout(driver, config);
	 * fc.utobj().printTestStep(testCaseId, "Login With Corporate User");
	 * fc.loginpage().loginWithParameter(driver, userName, password);
	 * 
	 * fc.utobj().printTestStep(testCaseId,
	 * "Navigate To Admin > CRM > Configure Status Page");
	 * fc.utobj().printTestStep(testCaseId, "Add Contact Status"); String
	 * contactStatus=dataSet.get("contactStatus");
	 * AdminCRMConfigureStatusPageTest statusPage=new
	 * AdminCRMConfigureStatusPageTest(); statusPage.addStatus01(driver,
	 * contactStatus, config);
	 * 
	 * fc.utobj().printTestStep(testCaseId, "Navigate To CRM > Campaign Center"
	 * ); fc.crm().crm_common().CRMModule(driver);
	 * 
	 * fc.utobj().printTestStep(testCaseId, "Create Campaign For Send Campaign"
	 * );
	 * 
	 * String campaignName=dataSet.get("campaignName"); String
	 * accessibility=dataSet.get("accessibility"); String
	 * campaignType=dataSet.get("campaignType"); String
	 * templateName=dataSet.get("templateName"); String
	 * emailSubject=dataSet.get("emailSubject"); String
	 * plainTextEditor=dataSet.get("plainTextEditor"); CRMCampaignCenterPageTest
	 * campaignCenterPage=new CRMCampaignCenterPageTest();
	 * campaignCenterPage.createCampaign(driver, config, campaignName,
	 * accessibility, campaignType, templateName, emailSubject,
	 * plainTextEditor);
	 * 
	 * fc.utobj().printTestStep(testCaseId, "Create Template For Send Email");
	 * 
	 * String templateNameForSendEmail=dataSet.get("templateNameForSendEmail");
	 * String emailSubjectForSendEmail=dataSet.get("emailSubjectForSendEmail");
	 * String
	 * plainTextEditorForSendEmail=dataSet.get("plainTextEditorForSendEmail");
	 * 
	 * CRMCampaignCenterEmailTemplatesPageTest emailTemplatePage=new
	 * CRMCampaignCenterEmailTemplatesPageTest();
	 * emailTemplatePage.createTemplate(driver, config,
	 * templateNameForSendEmail, emailSubjectForSendEmail,
	 * plainTextEditorForSendEmail);
	 * 
	 * fc.utobj().printTestStep(testCaseId, "Navigate To CRM > Worflows Page");
	 * fc.utobj().printTestStep(testCaseId,
	 * "Create Contact Type Standard Workflow");
	 * 
	 * String workFlowName=dataSet.get("workFlowName"); String
	 * triggerType=dataSet.get("triggerType"); String
	 * conditionType=dataSet.get("conditionType"); String
	 * availableFields=dataSet.get("availableFields"); String
	 * taskSubject=dataSet.get("taskSubject"); String
	 * taskPriority=dataSet.get("taskPriority"); String
	 * taskStatus=dataSet.get("taskStatus"); String
	 * matchingCondition=dataSet.get("matchingCondition"); String
	 * dateOfExecution=dataSet.get("dateOfExecution");
	 * 
	 * createWorkFlowContactDateBased(driver, config, dataSet, workFlowName,
	 * triggerType, dateOfExecution, conditionType, availableFields,
	 * matchingCondition, campaignName, contactStatus, templateNameForSendEmail,
	 * taskSubject, taskPriority, taskStatus);
	 * 
	 * fc.utobj().logoutAndQuitBrowser(driver, config, testCaseId); } catch
	 * (Exception e) {
	 * 
	 * fc.utobj().quitBrowserOnCatch(driver, config, e, testCaseId);} }
	 * 
	 * 
	 * 
	 * @Test(groups = "crmworkflow")
	 * 
	 * @TestCase(createdOn="2017-05-01",updatedOn="2018-06-14",
	 * testCaseDescription =
	 * "Create The Date Based WorkFlow of Last Purchase Date Type And Date of Execution At After Days When Contact Is Added For Contact Matching Criteria Status Of Contact is Default[New]"
	 * , testCaseId = "TC_77_Create_Workflow") public void
	 * createWorkFlow77()throws IOException, Exception,
	 * ParserConfigurationException, SAXException {
	 * 
	 * String testCaseId =
	 * fc.utobj().readTestCaseInfo(this.getClass().getName()+"."+new
	 * Object(){}.getClass().getEnclosingMethod().getName());
	 * 
	 * Map<String, String> config =
	 * fc.utobj().readConfigurationFile(FranconnectUtil.path);
	 * Map<String,String> dataSet = fc.utobj().readTestData("crmworkflow",
	 * testCaseId); WebDriver driver = fc.utobj().openDriver(config);
	 * 
	 * try { driver = fc.loginpage().login(driver, config);
	 * 
	 * String userName=fc.utobj().generateTestData(dataSet.get("userName"));
	 * String password=fc.utobj().generateTestData(dataSet.get("password"));
	 * AdminUsersManageCorporateUsersAddCorporateUserPageTest
	 * addcorporateUserPage=new
	 * AdminUsersManageCorporateUsersAddCorporateUserPageTest();
	 * addcorporateUserPage.addCorporateUser(driver, userName, password, config
	 * , emailId);
	 * 
	 * fc.home_page().logout(driver, config);
	 * fc.utobj().printTestStep(testCaseId, "Login With Corporate User");
	 * fc.loginpage().loginWithParameter(driver, userName, password);
	 * 
	 * fc.utobj().printTestStep(testCaseId,
	 * "Navigate To Admin > CRM > Configure Status Page");
	 * fc.utobj().printTestStep(testCaseId, "Add Contact Status"); String
	 * contactStatus=dataSet.get("contactStatus");
	 * AdminCRMConfigureStatusPageTest statusPage=new
	 * AdminCRMConfigureStatusPageTest(); statusPage.addStatus01(driver,
	 * contactStatus, config);
	 * 
	 * fc.utobj().printTestStep(testCaseId, "Navigate To CRM > Campaign Center"
	 * ); fc.crm().crm_common().CRMModule(driver);
	 * 
	 * fc.utobj().printTestStep(testCaseId, "Create Campaign For Send Campaign"
	 * );
	 * 
	 * String campaignName=dataSet.get("campaignName"); String
	 * accessibility=dataSet.get("accessibility"); String
	 * campaignType=dataSet.get("campaignType"); String
	 * templateName=dataSet.get("templateName"); String
	 * emailSubject=dataSet.get("emailSubject"); String
	 * plainTextEditor=dataSet.get("plainTextEditor"); CRMCampaignCenterPageTest
	 * campaignCenterPage=new CRMCampaignCenterPageTest();
	 * campaignCenterPage.createCampaign(driver, config, campaignName,
	 * accessibility, campaignType, templateName, emailSubject,
	 * plainTextEditor);
	 * 
	 * fc.utobj().printTestStep(testCaseId, "Create Template For Send Email");
	 * 
	 * String templateNameForSendEmail=dataSet.get("templateNameForSendEmail");
	 * String emailSubjectForSendEmail=dataSet.get("emailSubjectForSendEmail");
	 * String
	 * plainTextEditorForSendEmail=dataSet.get("plainTextEditorForSendEmail");
	 * 
	 * CRMCampaignCenterEmailTemplatesPageTest emailTemplatePage=new
	 * CRMCampaignCenterEmailTemplatesPageTest();
	 * emailTemplatePage.createTemplate(driver, config,
	 * templateNameForSendEmail, emailSubjectForSendEmail,
	 * plainTextEditorForSendEmail);
	 * 
	 * fc.utobj().printTestStep(testCaseId, "Navigate To CRM > Worflows Page");
	 * fc.utobj().printTestStep(testCaseId,
	 * "Create Contact Type Standard Workflow");
	 * 
	 * String workFlowName=dataSet.get("workFlowName"); String
	 * triggerType=dataSet.get("triggerType"); String
	 * conditionType=dataSet.get("conditionType"); String
	 * availableFields=dataSet.get("availableFields"); String
	 * taskSubject=dataSet.get("taskSubject"); String
	 * taskPriority=dataSet.get("taskPriority"); String
	 * taskStatus=dataSet.get("taskStatus"); String
	 * matchingCondition=dataSet.get("matchingCondition"); String
	 * dateOfExecution=dataSet.get("dateOfExecution");
	 * 
	 * createWorkFlowContactDateBased(driver, config, dataSet, workFlowName,
	 * triggerType, dateOfExecution, conditionType, availableFields,
	 * matchingCondition, campaignName, contactStatus, templateNameForSendEmail,
	 * taskSubject, taskPriority, taskStatus);
	 * 
	 * fc.utobj().logoutAndQuitBrowser(driver, config, testCaseId); } catch
	 * (Exception e) {
	 * 
	 * fc.utobj().quitBrowserOnCatch(driver, config, e, testCaseId);} }
	 * 
	 * 
	 * @Test(groups = "crmworkflow")
	 * 
	 * @TestCase(createdOn="2017-05-01",updatedOn="2018-06-14",
	 * testCaseDescription =
	 * "Create The Date Based WorkFlow of Last Purchase Date Type And Date of Execution At After Days When Contact Is Added For Contact Matching Criteria Email Subscription status is Set To Opted In"
	 * , testCaseId = "TC_78_Create_Workflow") public void
	 * createWorkFlow78()throws IOException, Exception,
	 * ParserConfigurationException, SAXException {
	 * 
	 * String testCaseId =
	 * fc.utobj().readTestCaseInfo(this.getClass().getName()+"."+new
	 * Object(){}.getClass().getEnclosingMethod().getName());
	 * 
	 * Map<String, String> config =
	 * fc.utobj().readConfigurationFile(FranconnectUtil.path);
	 * Map<String,String> dataSet = fc.utobj().readTestData("crmworkflow",
	 * testCaseId); WebDriver driver = fc.utobj().openDriver(config);
	 * 
	 * try { driver = fc.loginpage().login(driver, config);
	 * 
	 * String userName=fc.utobj().generateTestData(dataSet.get("userName"));
	 * String password=fc.utobj().generateTestData(dataSet.get("password"));
	 * AdminUsersManageCorporateUsersAddCorporateUserPageTest
	 * addcorporateUserPage=new
	 * AdminUsersManageCorporateUsersAddCorporateUserPageTest();
	 * addcorporateUserPage.addCorporateUser(driver, userName, password, config
	 * , emailId);
	 * 
	 * fc.home_page().logout(driver, config);
	 * fc.utobj().printTestStep(testCaseId, "Login With Corporate User");
	 * fc.loginpage().loginWithParameter(driver, userName, password);
	 * 
	 * fc.utobj().printTestStep(testCaseId,
	 * "Navigate To Admin > CRM > Configure Status Page");
	 * fc.utobj().printTestStep(testCaseId, "Add Contact Status"); String
	 * contactStatus=dataSet.get("contactStatus");
	 * AdminCRMConfigureStatusPageTest statusPage=new
	 * AdminCRMConfigureStatusPageTest(); statusPage.addStatus01(driver,
	 * contactStatus, config);
	 * 
	 * fc.utobj().printTestStep(testCaseId, "Navigate To CRM > Campaign Center"
	 * ); fc.crm().crm_common().CRMModule(driver);
	 * 
	 * fc.utobj().printTestStep(testCaseId, "Create Campaign For Send Campaign"
	 * );
	 * 
	 * String campaignName=dataSet.get("campaignName"); String
	 * accessibility=dataSet.get("accessibility"); String
	 * campaignType=dataSet.get("campaignType"); String
	 * templateName=dataSet.get("templateName"); String
	 * emailSubject=dataSet.get("emailSubject"); String
	 * plainTextEditor=dataSet.get("plainTextEditor"); CRMCampaignCenterPageTest
	 * campaignCenterPage=new CRMCampaignCenterPageTest();
	 * campaignCenterPage.createCampaign(driver, config, campaignName,
	 * accessibility, campaignType, templateName, emailSubject,
	 * plainTextEditor);
	 * 
	 * fc.utobj().printTestStep(testCaseId, "Create Template For Send Email");
	 * 
	 * String templateNameForSendEmail=dataSet.get("templateNameForSendEmail");
	 * String emailSubjectForSendEmail=dataSet.get("emailSubjectForSendEmail");
	 * String
	 * plainTextEditorForSendEmail=dataSet.get("plainTextEditorForSendEmail");
	 * 
	 * CRMCampaignCenterEmailTemplatesPageTest emailTemplatePage=new
	 * CRMCampaignCenterEmailTemplatesPageTest();
	 * emailTemplatePage.createTemplate(driver, config,
	 * templateNameForSendEmail, emailSubjectForSendEmail,
	 * plainTextEditorForSendEmail);
	 * 
	 * fc.utobj().printTestStep(testCaseId, "Navigate To CRM > Worflows Page");
	 * fc.utobj().printTestStep(testCaseId,
	 * "Create Contact Type Standard Workflow");
	 * 
	 * String workFlowName=dataSet.get("workFlowName"); String
	 * triggerType=dataSet.get("triggerType"); String
	 * conditionType=dataSet.get("conditionType"); String
	 * availableFields=dataSet.get("availableFields"); String
	 * taskSubject=dataSet.get("taskSubject"); String
	 * taskPriority=dataSet.get("taskPriority"); String
	 * taskStatus=dataSet.get("taskStatus"); String
	 * matchingCondition=dataSet.get("matchingCondition"); String
	 * dateOfExecution=dataSet.get("dateOfExecution");
	 * 
	 * createWorkFlowContactDateBased(driver, config, dataSet, workFlowName,
	 * triggerType, dateOfExecution, conditionType, availableFields,
	 * matchingCondition, campaignName, contactStatus, templateNameForSendEmail,
	 * taskSubject, taskPriority, taskStatus);
	 * 
	 * 
	 * fc.utobj().logoutAndQuitBrowser(driver, config, testCaseId); } catch
	 * (Exception e) {
	 * 
	 * fc.utobj().quitBrowserOnCatch(driver, config, e, testCaseId);} }
	 * 
	 * 
	 * @Test(groups = "crmworkflow")
	 * 
	 * @TestCase(createdOn="2017-05-01",updatedOn="2018-06-14",
	 * testCaseDescription =
	 * "Create The Date Based WorkFlow of Last Purchase Date Type And Date of Execution At After Days When Contact Is Added For Contact Matching Criteria Country And All States of It"
	 * , testCaseId = "TC_79_Create_Workflow") public void
	 * createWorkFlow79()throws IOException, Exception,
	 * ParserConfigurationException, SAXException {
	 * 
	 * String testCaseId =
	 * fc.utobj().readTestCaseInfo(this.getClass().getName()+"."+new
	 * Object(){}.getClass().getEnclosingMethod().getName());
	 * 
	 * Map<String, String> config =
	 * fc.utobj().readConfigurationFile(FranconnectUtil.path);
	 * Map<String,String> dataSet = fc.utobj().readTestData("crmworkflow",
	 * testCaseId); WebDriver driver = fc.utobj().openDriver(config);
	 * 
	 * try { driver = fc.loginpage().login(driver, config);
	 * 
	 * String userName=fc.utobj().generateTestData(dataSet.get("userName"));
	 * String password=fc.utobj().generateTestData(dataSet.get("password"));
	 * AdminUsersManageCorporateUsersAddCorporateUserPageTest
	 * addcorporateUserPage=new
	 * AdminUsersManageCorporateUsersAddCorporateUserPageTest();
	 * addcorporateUserPage.addCorporateUser(driver, userName, password, config
	 * , emailId);
	 * 
	 * fc.home_page().logout(driver, config);
	 * fc.utobj().printTestStep(testCaseId, "Login With Corporate User");
	 * fc.loginpage().loginWithParameter(driver, userName, password);
	 * 
	 * fc.utobj().printTestStep(testCaseId,
	 * "Navigate To Admin > CRM > Configure Status Page");
	 * fc.utobj().printTestStep(testCaseId, "Add Contact Status"); String
	 * contactStatus=dataSet.get("contactStatus");
	 * AdminCRMConfigureStatusPageTest statusPage=new
	 * AdminCRMConfigureStatusPageTest(); statusPage.addStatus01(driver,
	 * contactStatus, config);
	 * 
	 * fc.utobj().printTestStep(testCaseId, "Navigate To CRM > Campaign Center"
	 * ); fc.crm().crm_common().CRMModule(driver);
	 * 
	 * fc.utobj().printTestStep(testCaseId, "Create Campaign For Send Campaign"
	 * );
	 * 
	 * String campaignName=dataSet.get("campaignName"); String
	 * accessibility=dataSet.get("accessibility"); String
	 * campaignType=dataSet.get("campaignType"); String
	 * templateName=dataSet.get("templateName"); String
	 * emailSubject=dataSet.get("emailSubject"); String
	 * plainTextEditor=dataSet.get("plainTextEditor"); CRMCampaignCenterPageTest
	 * campaignCenterPage=new CRMCampaignCenterPageTest();
	 * campaignCenterPage.createCampaign(driver, config, campaignName,
	 * accessibility, campaignType, templateName, emailSubject,
	 * plainTextEditor);
	 * 
	 * fc.utobj().printTestStep(testCaseId, "Create Template For Send Email");
	 * 
	 * String templateNameForSendEmail=dataSet.get("templateNameForSendEmail");
	 * String emailSubjectForSendEmail=dataSet.get("emailSubjectForSendEmail");
	 * String
	 * plainTextEditorForSendEmail=dataSet.get("plainTextEditorForSendEmail");
	 * 
	 * CRMCampaignCenterEmailTemplatesPageTest emailTemplatePage=new
	 * CRMCampaignCenterEmailTemplatesPageTest();
	 * emailTemplatePage.createTemplate(driver, config,
	 * templateNameForSendEmail, emailSubjectForSendEmail,
	 * plainTextEditorForSendEmail);
	 * 
	 * fc.utobj().printTestStep(testCaseId, "Navigate To CRM > Worflows Page");
	 * fc.utobj().printTestStep(testCaseId,
	 * "Create Contact Type Standard Workflow");
	 * 
	 * String workFlowName=dataSet.get("workFlowName"); String
	 * triggerType=dataSet.get("triggerType"); String
	 * conditionType=dataSet.get("conditionType"); String
	 * availableFields=dataSet.get("availableFields"); String
	 * taskSubject=dataSet.get("taskSubject"); String
	 * taskPriority=dataSet.get("taskPriority"); String
	 * taskStatus=dataSet.get("taskStatus"); String
	 * matchingCondition=dataSet.get("matchingCondition"); String
	 * dateOfExecution=dataSet.get("dateOfExecution");
	 * 
	 * createWorkFlowContactDateBased(driver, config, dataSet, workFlowName,
	 * triggerType, dateOfExecution, conditionType, availableFields,
	 * matchingCondition, campaignName, contactStatus, templateNameForSendEmail,
	 * taskSubject, taskPriority, taskStatus);
	 * 
	 * 
	 * fc.utobj().logoutAndQuitBrowser(driver, config, testCaseId); } catch
	 * (Exception e) {
	 * 
	 * fc.utobj().quitBrowserOnCatch(driver, config, e, testCaseId);} }
	 * 
	 * 
	 * 
	 * @Test(groups = "crmworkflow")
	 * 
	 * @TestCase(createdOn="2017-05-01",updatedOn="2018-06-14",
	 * testCaseDescription =
	 * "Create The Date Based WorkFlow of Last Purchase Date Type And Date of Execution At After Days When Contact Is Added For Contact Matching Criteria BirthDate And Set To It , is Between"
	 * , testCaseId = "TC_80_Create_Workflow") public void
	 * createWorkFlow80()throws IOException, Exception,
	 * ParserConfigurationException, SAXException {
	 * 
	 * String testCaseId =
	 * fc.utobj().readTestCaseInfo(this.getClass().getName()+"."+new
	 * Object(){}.getClass().getEnclosingMethod().getName());
	 * 
	 * Map<String, String> config =
	 * fc.utobj().readConfigurationFile(FranconnectUtil.path);
	 * Map<String,String> dataSet = fc.utobj().readTestData("crmworkflow",
	 * testCaseId); WebDriver driver = fc.utobj().openDriver(config);
	 * 
	 * try { driver = fc.loginpage().login(driver, config);
	 * 
	 * String userName=fc.utobj().generateTestData(dataSet.get("userName"));
	 * String password=fc.utobj().generateTestData(dataSet.get("password"));
	 * AdminUsersManageCorporateUsersAddCorporateUserPageTest
	 * addcorporateUserPage=new
	 * AdminUsersManageCorporateUsersAddCorporateUserPageTest();
	 * addcorporateUserPage.addCorporateUser(driver, userName, password, config
	 * , emailId);
	 * 
	 * fc.home_page().logout(driver, config);
	 * fc.utobj().printTestStep(testCaseId, "Login With Corporate User");
	 * fc.loginpage().loginWithParameter(driver, userName, password);
	 * 
	 * fc.utobj().printTestStep(testCaseId,
	 * "Navigate To Admin > CRM > Configure Status Page");
	 * fc.utobj().printTestStep(testCaseId, "Add Contact Status"); String
	 * contactStatus=dataSet.get("contactStatus");
	 * AdminCRMConfigureStatusPageTest statusPage=new
	 * AdminCRMConfigureStatusPageTest(); statusPage.addStatus01(driver,
	 * contactStatus, config);
	 * 
	 * fc.utobj().printTestStep(testCaseId, "Navigate To CRM > Campaign Center"
	 * ); fc.crm().crm_common().CRMModule(driver);
	 * 
	 * fc.utobj().printTestStep(testCaseId, "Create Campaign For Send Campaign"
	 * );
	 * 
	 * String campaignName=dataSet.get("campaignName"); String
	 * accessibility=dataSet.get("accessibility"); String
	 * campaignType=dataSet.get("campaignType"); String
	 * templateName=dataSet.get("templateName"); String
	 * emailSubject=dataSet.get("emailSubject"); String
	 * plainTextEditor=dataSet.get("plainTextEditor"); CRMCampaignCenterPageTest
	 * campaignCenterPage=new CRMCampaignCenterPageTest();
	 * campaignCenterPage.createCampaign(driver, config, campaignName,
	 * accessibility, campaignType, templateName, emailSubject,
	 * plainTextEditor);
	 * 
	 * fc.utobj().printTestStep(testCaseId, "Create Template For Send Email");
	 * 
	 * String templateNameForSendEmail=dataSet.get("templateNameForSendEmail");
	 * String emailSubjectForSendEmail=dataSet.get("emailSubjectForSendEmail");
	 * String
	 * plainTextEditorForSendEmail=dataSet.get("plainTextEditorForSendEmail");
	 * 
	 * CRMCampaignCenterEmailTemplatesPageTest emailTemplatePage=new
	 * CRMCampaignCenterEmailTemplatesPageTest();
	 * emailTemplatePage.createTemplate(driver, config,
	 * templateNameForSendEmail, emailSubjectForSendEmail,
	 * plainTextEditorForSendEmail);
	 * 
	 * fc.utobj().printTestStep(testCaseId, "Navigate To CRM > Worflows Page");
	 * fc.utobj().printTestStep(testCaseId,
	 * "Create Contact Type Standard Workflow");
	 * 
	 * String workFlowName=dataSet.get("workFlowName"); String
	 * triggerType=dataSet.get("triggerType"); String
	 * conditionType=dataSet.get("conditionType"); String
	 * availableFields=dataSet.get("availableFields"); String
	 * taskSubject=dataSet.get("taskSubject"); String
	 * taskPriority=dataSet.get("taskPriority"); String
	 * taskStatus=dataSet.get("taskStatus"); String
	 * matchingCondition=dataSet.get("matchingCondition"); String
	 * dateOfExecution=dataSet.get("dateOfExecution");
	 * 
	 * createWorkFlowContactDateBased(driver, config, dataSet, workFlowName,
	 * triggerType, dateOfExecution, conditionType, availableFields,
	 * matchingCondition, campaignName, contactStatus, templateNameForSendEmail,
	 * taskSubject, taskPriority, taskStatus);
	 * 
	 * 
	 * fc.utobj().logoutAndQuitBrowser(driver, config, testCaseId); } catch
	 * (Exception e) {
	 * 
	 * fc.utobj().quitBrowserOnCatch(driver, config, e, testCaseId);} }
	 * 
	 * 
	 * @Test(groups = "crmworkflow")
	 * 
	 * @TestCase(createdOn="2017-05-01",updatedOn="2018-06-14",
	 * testCaseDescription =
	 * "Create The Date Based WorkFlow of Last Purchase Date Type And Date of Execution At After Days When Contact Is Added For Contact Matching Criteria Status Of Contact Is Customized"
	 * , testCaseId = "TC_81_Create_Workflow") public void
	 * createWorkFlow81()throws IOException, Exception,
	 * ParserConfigurationException, SAXException {
	 * 
	 * String testCaseId =
	 * fc.utobj().readTestCaseInfo(this.getClass().getName()+"."+new
	 * Object(){}.getClass().getEnclosingMethod().getName());
	 * 
	 * Map<String, String> config =
	 * fc.utobj().readConfigurationFile(FranconnectUtil.path);
	 * Map<String,String> dataSet = fc.utobj().readTestData("crmworkflow",
	 * testCaseId); WebDriver driver = fc.utobj().openDriver(config);
	 * 
	 * try { driver = fc.loginpage().login(driver, config);
	 * 
	 * String userName=fc.utobj().generateTestData(dataSet.get("userName"));
	 * String password=fc.utobj().generateTestData(dataSet.get("password"));
	 * AdminUsersManageCorporateUsersAddCorporateUserPageTest
	 * addcorporateUserPage=new
	 * AdminUsersManageCorporateUsersAddCorporateUserPageTest();
	 * addcorporateUserPage.addCorporateUser(driver, userName, password, config
	 * , emailId);
	 * 
	 * fc.home_page().logout(driver, config);
	 * fc.utobj().printTestStep(testCaseId, "Login With Corporate User");
	 * fc.loginpage().loginWithParameter(driver, userName, password);
	 * 
	 * fc.utobj().printTestStep(testCaseId,
	 * "Navigate To Admin > CRM > Configure Status Page");
	 * fc.utobj().printTestStep(testCaseId, "Add Contact Status"); String
	 * contactStatus=dataSet.get("contactStatus");
	 * AdminCRMConfigureStatusPageTest statusPage=new
	 * AdminCRMConfigureStatusPageTest(); statusPage.addStatus01(driver,
	 * contactStatus, config);
	 * 
	 * 
	 * fc.utobj().printTestStep(testCaseId,
	 * "Navigate To Admin > CRM > Configure Status Page");
	 * fc.utobj().printTestStep(testCaseId, "Add Contact Status"); String
	 * contactStatusCustomize=dataSet.get("selectContactStatus");
	 * statusPage.addStatus01(driver, contactStatusCustomize, config);
	 * 
	 * fc.utobj().printTestStep(testCaseId, "Navigate To CRM > Campaign Center"
	 * ); fc.crm().crm_common().CRMModule(driver);
	 * 
	 * fc.utobj().printTestStep(testCaseId, "Create Campaign For Send Campaign"
	 * );
	 * 
	 * String campaignName=dataSet.get("campaignName"); String
	 * accessibility=dataSet.get("accessibility"); String
	 * campaignType=dataSet.get("campaignType"); String
	 * templateName=dataSet.get("templateName"); String
	 * emailSubject=dataSet.get("emailSubject"); String
	 * plainTextEditor=dataSet.get("plainTextEditor"); CRMCampaignCenterPageTest
	 * campaignCenterPage=new CRMCampaignCenterPageTest();
	 * campaignCenterPage.createCampaign(driver, config, campaignName,
	 * accessibility, campaignType, templateName, emailSubject,
	 * plainTextEditor);
	 * 
	 * fc.utobj().printTestStep(testCaseId, "Create Template For Send Email");
	 * 
	 * String templateNameForSendEmail=dataSet.get("templateNameForSendEmail");
	 * String emailSubjectForSendEmail=dataSet.get("emailSubjectForSendEmail");
	 * String
	 * plainTextEditorForSendEmail=dataSet.get("plainTextEditorForSendEmail");
	 * 
	 * CRMCampaignCenterEmailTemplatesPageTest emailTemplatePage=new
	 * CRMCampaignCenterEmailTemplatesPageTest();
	 * emailTemplatePage.createTemplate(driver, config,
	 * templateNameForSendEmail, emailSubjectForSendEmail,
	 * plainTextEditorForSendEmail);
	 * 
	 * fc.utobj().printTestStep(testCaseId, "Navigate To CRM > Worflows Page");
	 * fc.utobj().printTestStep(testCaseId,
	 * "Create Contact Type Standard Workflow");
	 * 
	 * String workFlowName=dataSet.get("workFlowName"); String
	 * triggerType=dataSet.get("triggerType"); String
	 * conditionType=dataSet.get("conditionType"); String
	 * availableFields=dataSet.get("availableFields"); String
	 * taskSubject=dataSet.get("taskSubject"); String
	 * taskPriority=dataSet.get("taskPriority"); String
	 * taskStatus=dataSet.get("taskStatus"); String
	 * matchingCondition=dataSet.get("matchingCondition"); String
	 * dateOfExecution=dataSet.get("dateOfExecution");
	 * 
	 * createWorkFlowContactDateBased(driver, config, dataSet, workFlowName,
	 * triggerType, dateOfExecution, conditionType, availableFields,
	 * matchingCondition, campaignName, contactStatus, templateNameForSendEmail,
	 * taskSubject, taskPriority, taskStatus);
	 * 
	 * 
	 * fc.utobj().logoutAndQuitBrowser(driver, config, testCaseId); } catch
	 * (Exception e) {
	 * 
	 * fc.utobj().quitBrowserOnCatch(driver, config, e, testCaseId);} }
	 * 
	 * 
	 * 
	 * @Test(groups = "crmworkflow")
	 * 
	 * @TestCase(createdOn="2017-05-01",updatedOn="2018-06-14",
	 * testCaseDescription =
	 * "Create The Date Based WorkFlow of Last Purchase Date Type And Date of Execution At After Days When Contact Is Added For Contact Matching Criteria Contact Type In"
	 * , testCaseId = "TC_82_Create_Workflow") public void
	 * createWorkFlow82()throws IOException, Exception,
	 * ParserConfigurationException, SAXException {
	 * 
	 * String testCaseId =
	 * fc.utobj().readTestCaseInfo(this.getClass().getName()+"."+new
	 * Object(){}.getClass().getEnclosingMethod().getName());
	 * 
	 * Map<String, String> config =
	 * fc.utobj().readConfigurationFile(FranconnectUtil.path);
	 * Map<String,String> dataSet = fc.utobj().readTestData("crmworkflow",
	 * testCaseId); WebDriver driver = fc.utobj().openDriver(config);
	 * 
	 * try { driver = fc.loginpage().login(driver, config);
	 * 
	 * String userName=fc.utobj().generateTestData(dataSet.get("userName"));
	 * String password=fc.utobj().generateTestData(dataSet.get("password"));
	 * AdminUsersManageCorporateUsersAddCorporateUserPageTest
	 * addcorporateUserPage=new
	 * AdminUsersManageCorporateUsersAddCorporateUserPageTest();
	 * addcorporateUserPage.addCorporateUser(driver, userName, password, config
	 * , emailId);
	 * 
	 * fc.home_page().logout(driver, config);
	 * fc.utobj().printTestStep(testCaseId, "Login With Corporate User");
	 * fc.loginpage().loginWithParameter(driver, userName, password);
	 * 
	 * fc.utobj().printTestStep(testCaseId,
	 * "Navigate To Admin > CRM > Configure Status Page");
	 * fc.utobj().printTestStep(testCaseId, "Add Contact Status"); String
	 * contactStatus=dataSet.get("contactStatus");
	 * AdminCRMConfigureStatusPageTest statusPage=new
	 * AdminCRMConfigureStatusPageTest(); statusPage.addStatus01(driver,
	 * contactStatus, config);
	 * 
	 * fc.utobj().printTestStep(testCaseId,
	 * "Navigate To Admin > CRM > Contact Type Configureation");
	 * fc.utobj().printTestStep(testCaseId, "Add Contact Type"); String
	 * contactType=dataSet.get("selectcontactType");
	 * AdminCRMContactTypeConfigurationPageTest
	 * contactTypeConfigureationPage=new
	 * AdminCRMContactTypeConfigurationPageTest();
	 * contactTypeConfigureationPage.addContactType(driver, contactType,
	 * config);
	 * 
	 * fc.utobj().printTestStep(testCaseId, "Navigate To CRM > Campaign Center"
	 * ); fc.crm().crm_common().CRMModule(driver);
	 * 
	 * fc.utobj().printTestStep(testCaseId, "Create Campaign For Send Campaign"
	 * );
	 * 
	 * String campaignName=dataSet.get("campaignName"); String
	 * accessibility=dataSet.get("accessibility"); String
	 * campaignType=dataSet.get("campaignType"); String
	 * templateName=dataSet.get("templateName"); String
	 * emailSubject=dataSet.get("emailSubject"); String
	 * plainTextEditor=dataSet.get("plainTextEditor"); CRMCampaignCenterPageTest
	 * campaignCenterPage=new CRMCampaignCenterPageTest();
	 * campaignCenterPage.createCampaign(driver, config, campaignName,
	 * accessibility, campaignType, templateName, emailSubject,
	 * plainTextEditor);
	 * 
	 * fc.utobj().printTestStep(testCaseId, "Create Template For Send Email");
	 * 
	 * String templateNameForSendEmail=dataSet.get("templateNameForSendEmail");
	 * String emailSubjectForSendEmail=dataSet.get("emailSubjectForSendEmail");
	 * String
	 * plainTextEditorForSendEmail=dataSet.get("plainTextEditorForSendEmail");
	 * 
	 * CRMCampaignCenterEmailTemplatesPageTest emailTemplatePage=new
	 * CRMCampaignCenterEmailTemplatesPageTest();
	 * emailTemplatePage.createTemplate(driver, config,
	 * templateNameForSendEmail, emailSubjectForSendEmail,
	 * plainTextEditorForSendEmail);
	 * 
	 * fc.utobj().printTestStep(testCaseId, "Navigate To CRM > Worflows Page");
	 * fc.utobj().printTestStep(testCaseId,
	 * "Create Contact Type Standard Workflow");
	 * 
	 * String workFlowName=dataSet.get("workFlowName"); String
	 * triggerType=dataSet.get("triggerType"); String
	 * conditionType=dataSet.get("conditionType"); String
	 * availableFields=dataSet.get("availableFields"); String
	 * taskSubject=dataSet.get("taskSubject"); String
	 * taskPriority=dataSet.get("taskPriority"); String
	 * taskStatus=dataSet.get("taskStatus"); String
	 * matchingCondition=dataSet.get("matchingCondition"); String
	 * dateOfExecution=dataSet.get("dateOfExecution");
	 * 
	 * createWorkFlowContactDateBased(driver, config, dataSet, workFlowName,
	 * triggerType, dateOfExecution, conditionType, availableFields,
	 * matchingCondition, campaignName, contactStatus, templateNameForSendEmail,
	 * taskSubject, taskPriority, taskStatus);
	 * 
	 * 
	 * fc.utobj().logoutAndQuitBrowser(driver, config, testCaseId); } catch
	 * (Exception e) {
	 * 
	 * fc.utobj().quitBrowserOnCatch(driver, config, e, testCaseId);} }
	 * 
	 * 
	 * @Test(groups = "crmworkflownouse")
	 * 
	 * @TestCase(createdOn="2017-05-01",updatedOn="2018-06-14",
	 * testCaseDescription =
	 * "Create The Date Based WorkFlow of Last Purchase Date Type And Date of Execution At After Days When Contact Is Added For All Contacts"
	 * , testCaseId = "TC_83_Create_Workflow") public void
	 * createWorkFlow83()throws IOException, Exception,
	 * ParserConfigurationException, SAXException {
	 * 
	 * String testCaseId =
	 * fc.utobj().readTestCaseInfo(this.getClass().getName()+"."+new
	 * Object(){}.getClass().getEnclosingMethod().getName());
	 * 
	 * Map<String, String> config =
	 * fc.utobj().readConfigurationFile(FranconnectUtil.path);
	 * Map<String,String> dataSet = fc.utobj().readTestData("crmworkflow",
	 * testCaseId); WebDriver driver = fc.utobj().openDriver(config);
	 * 
	 * try { driver = fc.loginpage().login(driver, config);
	 * 
	 * String userName=fc.utobj().generateTestData(dataSet.get("userName"));
	 * String password=fc.utobj().generateTestData(dataSet.get("password"));
	 * AdminUsersManageCorporateUsersAddCorporateUserPageTest
	 * addcorporateUserPage=new
	 * AdminUsersManageCorporateUsersAddCorporateUserPageTest();
	 * addcorporateUserPage.addCorporateUser(driver, userName, password, config
	 * , emailId);
	 * 
	 * fc.home_page().logout(driver, config);
	 * fc.utobj().printTestStep(testCaseId, "Login With Corporate User");
	 * fc.loginpage().loginWithParameter(driver, userName, password);
	 * 
	 * fc.utobj().printTestStep(testCaseId,
	 * "Navigate To Admin > CRM > Configure Status Page");
	 * fc.utobj().printTestStep(testCaseId, "Add Contact Status"); String
	 * contactStatus=dataSet.get("contactStatus");
	 * AdminCRMConfigureStatusPageTest statusPage=new
	 * AdminCRMConfigureStatusPageTest(); statusPage.addStatus01(driver,
	 * contactStatus, config);
	 * 
	 * fc.utobj().printTestStep(testCaseId, "Navigate To CRM > Campaign Center"
	 * ); fc.crm().crm_common().CRMModule(driver);
	 * 
	 * fc.utobj().printTestStep(testCaseId, "Create Campaign For Send Campaign"
	 * );
	 * 
	 * String campaignName=dataSet.get("campaignName"); String
	 * accessibility=dataSet.get("accessibility"); String
	 * campaignType=dataSet.get("campaignType"); String
	 * templateName=dataSet.get("templateName"); String
	 * emailSubject=dataSet.get("emailSubject"); String
	 * plainTextEditor=dataSet.get("plainTextEditor"); CRMCampaignCenterPageTest
	 * campaignCenterPage=new CRMCampaignCenterPageTest();
	 * campaignCenterPage.createCampaign(driver, config, campaignName,
	 * accessibility, campaignType, templateName, emailSubject,
	 * plainTextEditor);
	 * 
	 * fc.utobj().printTestStep(testCaseId, "Create Template For Send Email");
	 * 
	 * String templateNameForSendEmail=dataSet.get("templateNameForSendEmail");
	 * String emailSubjectForSendEmail=dataSet.get("emailSubjectForSendEmail");
	 * String
	 * plainTextEditorForSendEmail=dataSet.get("plainTextEditorForSendEmail");
	 * 
	 * CRMCampaignCenterEmailTemplatesPageTest emailTemplatePage=new
	 * CRMCampaignCenterEmailTemplatesPageTest();
	 * emailTemplatePage.createTemplate(driver, config,
	 * templateNameForSendEmail, emailSubjectForSendEmail,
	 * plainTextEditorForSendEmail);
	 * 
	 * fc.utobj().printTestStep(testCaseId, "Navigate To CRM > Worflows Page");
	 * fc.utobj().printTestStep(testCaseId,
	 * "Create Contact Type Standard Workflow");
	 * 
	 * String workFlowName=dataSet.get("workFlowName"); String
	 * triggerType=dataSet.get("triggerType"); String
	 * conditionType=dataSet.get("conditionType"); String availableFields="";
	 * String taskSubject=dataSet.get("taskSubject"); String
	 * taskPriority=dataSet.get("taskPriority"); String
	 * taskStatus=dataSet.get("taskStatus"); String matchingCondition=""; String
	 * dateOfExecution=dataSet.get("dateOfExecution");
	 * 
	 * createWorkFlowContactDateBased(driver, config, dataSet, workFlowName,
	 * triggerType, dateOfExecution, conditionType, availableFields,
	 * matchingCondition, campaignName, contactStatus, templateNameForSendEmail,
	 * taskSubject, taskPriority, taskStatus);
	 * 
	 * 
	 * fc.utobj().logoutAndQuitBrowser(driver, config, testCaseId); } catch
	 * (Exception e) {
	 * 
	 * fc.utobj().quitBrowserOnCatch(driver, config, e, testCaseId);} }
	 */

	// aFor Lead Event Based WorkFlow

	// For All Lead Condition
	public void createWorkFlowLeadStandard(WebDriver driver, Map<String, String> config, Map<String, String> dataSet,
			String workFlowName, String triggerType, String conditionType, String availableFields,
			String matchingCondition, String campaignName, String leadStatus, String templateNameForSendEmail,
			String taskSubject, String taskPriority, String taskStatus) throws Exception {

		String testCaseId = "TC_Create_WorkFlow_Lead_Standard";

		if (fc.utobj().validate(testCaseId)) {

			try {

				CRMWorkflowsPage pobj = new CRMWorkflowsPage(driver);
				fc.utobj().clickElement(driver, pobj.workflowsLink);

				fc.utobj().clickElement(driver, pobj.createWorkFLowLink);

				// fc.utobj().switchFrame(driver, pobj.frame);

				fc.utobj().sendKeys(driver, pobj.workFlowName, workFlowName);
				fc.utobj().clickElement(driver, pobj.recordTypeLead);
				fc.utobj().clickElement(driver, pobj.workflowTypeStandard);
				fc.utobj().clickElement(driver, pobj.nextButton);

				// 1. :: Trigger :: When Lead is Added ::
				if (triggerType.equalsIgnoreCase("Added")) {

					fc.utobj().clickElement(driver, pobj.leadIsAdded);
					fc.utobj().clickElement(driver, pobj.nextButtonTrigger);

				} else if (triggerType.equalsIgnoreCase("Modified")) {

					fc.utobj().clickElement(driver, pobj.leadIsModified);
					fc.utobj().clickElement(driver, pobj.nextButtonTrigger);

				} else if (triggerType.equalsIgnoreCase("Added or Modified")) {

					fc.utobj().clickElement(driver, pobj.leadIsAddedOrModified);
					fc.utobj().clickElement(driver, pobj.nextButtonTrigger);
				}

				// 2. ::Condition ::

				if (conditionType.equalsIgnoreCase("All Leads")) {

					fc.utobj().clickElement(driver, pobj.allLeads);
					fc.utobj().clickElement(driver, pobj.nextBtnCondition);

				} else if (conditionType.equalsIgnoreCase("Leads Matching The Conditions")) {

					fc.utobj().clickElement(driver, pobj.leadsMatchingCondition);

					if (availableFields.equalsIgnoreCase("Birthdate")) {

						// for unique condition
						fc.utobj().clickElement(driver, pobj.availabelFieldsSelect);
						fc.utobj().sendKeys(driver,
								driver.findElement(By.xpath(".//*[@id='fc-drop-parentselectedColList']/div/div/input")),
								"First Name");
						fc.utobj().clickElement(driver, driver.findElement(By.xpath(
								".//*[@id='fc-drop-parentselectedColList']/div/ul/li/label/span[.='First Name']")));
						fc.utobj().clickElement(driver, pobj.availabelFieldsSelect);

						// Equals Test Data
						fc.utobj().selectDropDown(driver, pobj.firstNameSelectMatch, "Equals");
						fc.utobj().sendKeys(driver, pobj.firstNameField, dataSet.get("leadFirstName"));

						fc.utobj().clickElement(driver, pobj.availabelFieldsSelect);
						fc.utobj().sendKeys(driver,
								driver.findElement(By.xpath(".//*[@id='fc-drop-parentselectedColList']/div/div/input")),
								availableFields);

						fc.utobj().clickElement(driver,
								driver.findElement(
										By.xpath(".//*[@id='fc-drop-parentselectedColList']/div/ul/li/label/span[.='"
												+ availableFields + "']")));
						fc.utobj().clickElement(driver, pobj.availabelFieldsSelect);

						if (matchingCondition.equalsIgnoreCase("Is Between")) {

							fc.utobj().selectDropDown(driver, pobj.selectBirthDateMatch, "Is Between");
							fc.utobj().sendKeys(driver, pobj.birthdayFromDate, dataSet.get("fromDate"));
							fc.utobj().sendKeys(driver, pobj.birthdayToDate, dataSet.get("toDate"));
							fc.utobj().clickElement(driver, pobj.selectBirthDateMatch);
						}
					} else if (availableFields.equalsIgnoreCase("Email")) {

						// for unique condition
						fc.utobj().clickElement(driver, pobj.availabelFieldsSelect);
						fc.utobj().sendKeys(driver,
								driver.findElement(By.xpath(".//*[@id='fc-drop-parentselectedColList']/div/div/input")),
								"First Name");
						fc.utobj().clickElement(driver, driver.findElement(By.xpath(
								".//*[@id='fc-drop-parentselectedColList']/div/ul/li/label/span[.='First Name']")));
						fc.utobj().clickElement(driver, pobj.availabelFieldsSelect);

						// Equals Test Data
						fc.utobj().selectDropDown(driver, pobj.firstNameSelectMatch, "Equals");
						fc.utobj().sendKeys(driver, pobj.firstNameField, dataSet.get("leadFirstName"));

						// For Email Condition

						fc.utobj().clickElement(driver, pobj.availabelFieldsSelect);
						fc.utobj().sendKeys(driver,
								driver.findElement(By.xpath(".//*[@id='fc-drop-parentselectedColList']/div/div/input")),
								availableFields);

						fc.utobj().clickElement(driver,
								driver.findElement(
										By.xpath(".//*[@id='fc-drop-parentselectedColList']/div/ul/li/label/span[.='"
												+ availableFields + "']")));
						fc.utobj().clickElement(driver, pobj.availabelFieldsSelect);

						if (matchingCondition.equalsIgnoreCase("Is Not Empty")) {

							fc.utobj().selectDropDown(driver, pobj.selectLeadInfoEmailId, matchingCondition);
						}

					} else if (availableFields.equalsIgnoreCase("Country")) {

						// for unique condition
						fc.utobj().clickElement(driver, pobj.availabelFieldsSelect);
						fc.utobj().sendKeys(driver,
								driver.findElement(By.xpath(".//*[@id='fc-drop-parentselectedColList']/div/div/input")),
								"First Name");
						fc.utobj().clickElement(driver, driver.findElement(By.xpath(
								".//*[@id='fc-drop-parentselectedColList']/div/ul/li/label/span[.='First Name']")));
						fc.utobj().clickElement(driver, pobj.availabelFieldsSelect);

						// Equals Test Data
						fc.utobj().selectDropDown(driver, pobj.firstNameSelectMatch, "Equals");
						fc.utobj().sendKeys(driver, pobj.firstNameField, dataSet.get("leadFirstName"));

						fc.utobj().clickElement(driver, pobj.availabelFieldsSelect);
						fc.utobj().sendKeys(driver,
								driver.findElement(By.xpath(".//*[@id='fc-drop-parentselectedColList']/div/div/input")),
								availableFields);

						fc.utobj().clickElement(driver,
								driver.findElement(
										By.xpath(".//*[@id='fc-drop-parentselectedColList']/div/ul/li/label/span[.='"
												+ availableFields + "']")));
						fc.utobj().clickElement(driver, pobj.availabelFieldsSelect);

						if (matchingCondition.equalsIgnoreCase("In")) {

							fc.utobj().selectDropDown(driver, pobj.countryMatch, "In");
							fc.utobj().clickElement(driver, pobj.countrySelect);
							fc.utobj().clickElement(driver, fc.utobj().getElement(driver, pobj.countrySelect)
									.findElement(By.xpath("./div/ul/li/label/span[.='Select All']")));
							fc.utobj().clickElement(driver, fc.utobj().getElement(driver, pobj.countrySelect)
									.findElement(By.xpath("./div/ul/li/label/span[.='Select All']")));

							fc.utobj().sendKeys(driver, fc.utobj().getElement(driver, pobj.countrySelect)
									.findElement(By.xpath("./div/div/input")), dataSet.get("countryIn"));
							fc.utobj().clickElement(driver,
									fc.utobj().getElement(driver, pobj.countrySelect).findElement(
											By.xpath("./div/ul/li/label/span[.='" + dataSet.get("countryIn") + "']")));
							fc.utobj().clickElement(driver, pobj.countrySelect);
						}
					} else if (availableFields.equalsIgnoreCase("Status")) {

						// for unique condition
						fc.utobj().clickElement(driver, pobj.availabelFieldsSelect);
						fc.utobj().sendKeys(driver,
								driver.findElement(By.xpath(".//*[@id='fc-drop-parentselectedColList']/div/div/input")),
								"First Name");
						fc.utobj().clickElement(driver, driver.findElement(By.xpath(
								".//*[@id='fc-drop-parentselectedColList']/div/ul/li/label/span[.='First Name']")));
						fc.utobj().clickElement(driver, pobj.availabelFieldsSelect);

						// Equals Test Data
						fc.utobj().selectDropDown(driver, pobj.firstNameSelectMatch, "Equals");
						fc.utobj().sendKeys(driver, pobj.firstNameField, dataSet.get("leadFirstName"));

						fc.utobj().clickElement(driver, pobj.availabelFieldsSelect);
						fc.utobj().sendKeys(driver,
								driver.findElement(By.xpath(".//*[@id='fc-drop-parentselectedColList']/div/div/input")),
								availableFields);

						fc.utobj().clickElement(driver,
								driver.findElement(
										By.xpath(".//*[@id='fc-drop-parentselectedColList']/div/ul/li/label/span[.='"
												+ availableFields + "']")));
						fc.utobj().clickElement(driver, pobj.availabelFieldsSelect);

						if (matchingCondition.equalsIgnoreCase("In")) {

							fc.utobj().selectDropDown(driver, pobj.selectLeadStatusMatch, "In");

							fc.utobj().clickElement(driver, pobj.selectLeadStatus);
							fc.utobj().sendKeys(driver, fc.utobj().getElement(driver, pobj.selectLeadStatus)
									.findElement(By.xpath("./div/div/input")), dataSet.get("selectLeadStatus"));
							fc.utobj().clickElement(driver,
									fc.utobj().getElement(driver, pobj.selectLeadStatus).findElement(By.xpath(
											"./div/ul/li/label/span[.='" + dataSet.get("selectLeadStatus") + "']")));
							fc.utobj().clickElement(driver, pobj.selectLeadStatus);
						}

					} else if (availableFields.equalsIgnoreCase("Email Subscription Status")) {

						// for unique condition
						fc.utobj().clickElement(driver, pobj.availabelFieldsSelect);
						fc.utobj().sendKeys(driver,
								driver.findElement(By.xpath(".//*[@id='fc-drop-parentselectedColList']/div/div/input")),
								"First Name");
						fc.utobj().clickElement(driver, driver.findElement(By.xpath(
								".//*[@id='fc-drop-parentselectedColList']/div/ul/li/label/span[.='First Name']")));
						fc.utobj().clickElement(driver, pobj.availabelFieldsSelect);

						// Equals Test Data
						fc.utobj().selectDropDown(driver, pobj.firstNameSelectMatch, "Equals");
						fc.utobj().sendKeys(driver, pobj.firstNameField, dataSet.get("leadFirstName"));

						fc.utobj().clickElement(driver, pobj.availabelFieldsSelect);
						fc.utobj().sendKeys(driver,
								driver.findElement(By.xpath(".//*[@id='fc-drop-parentselectedColList']/div/div/input")),
								availableFields);

						fc.utobj().clickElement(driver,
								driver.findElement(
										By.xpath(".//*[@id='fc-drop-parentselectedColList']/div/ul/li/label/span[.='"
												+ availableFields + "']")));
						fc.utobj().clickElement(driver, pobj.availabelFieldsSelect);

						if (matchingCondition.equalsIgnoreCase("In")) {

							fc.utobj().selectDropDown(driver, pobj.selectEmailSubScriptionMatch, "In");

							fc.utobj().clickElement(driver, pobj.selectEmailSubscribe);
							fc.utobj()
									.sendKeys(driver,
											fc.utobj().getElement(driver, pobj.selectEmailSubscribe)
													.findElement(By.xpath("./div/div/input")),
											dataSet.get("emailStatus"));
							fc.utobj().clickElement(driver,
									fc.utobj().getElement(driver, pobj.selectEmailSubscribe)
											.findElement(By.xpath("./div/ul/li/label/span[contains(text () ,'"
													+ dataSet.get("emailStatus") + "')]")));
							fc.utobj().clickElement(driver, pobj.selectEmailSubscribe);
						}
					}

					fc.utobj().clickElement(driver, pobj.nextBtnCondition);
				}

				// 3. :: Action ::

				// 1.send Campaign
				fc.utobj().clickElement(driver, pobj.actionBtn);
				fc.utobj().clickElement(driver, pobj.sendCampaign);

				if (fc.utobj().getElement(driver, pobj.selectLocationAtCampaign).isDisplayed()) {
					fc.utobj().clickElement(driver, pobj.masterCampaign);
				}

				fc.utobj().sendKeys(driver, pobj.selectCampaign, campaignName);
				fc.utobj().clickElement(driver, pobj.saveBtn);

				// 2.change Status
				fc.utobj().clickElement(driver, pobj.actionBtn);
				fc.utobj().clickElement(driver, pobj.changesStatus);
				fc.utobj().selectDropDown(driver, pobj.selectStatus, leadStatus);
				fc.utobj().clickElement(driver, pobj.saveBtn);

				// 3.send Email
				fc.utobj().clickElement(driver, pobj.actionBtn);
				fc.utobj().clickElement(driver, pobj.sendEmail);

				fc.commonMethods().switch_frameClass_newLayoutcboxIframe(driver);

				// from/Reply :: Lead Owner ID
				// To :: Lead Email

				fc.utobj().selectDropDown(driver, pobj.selectTemplate, templateNameForSendEmail);
				fc.utobj().clickElement(driver, pobj.associateTemplateWithEmailButton);
				fc.utobj().switchFrameToDefault(driver);

				// 4.create Task
				fc.utobj().clickElement(driver, pobj.actionBtn);
				fc.utobj().clickElement(driver, pobj.createTask);

				fc.commonMethods().switch_frameClass_newLayoutcboxIframe(driver);

				fc.utobj().sendKeys(driver, pobj.subject, taskSubject);
				fc.utobj().selectDropDown(driver, pobj.priority, taskPriority);
				// fc.utobj().selectDropDown(driver, pobj.taskStatus,
				// taskStatus);

				String textVal = pobj.taskStatus
						.findElement(By.xpath("./option[contains(text () , '" + taskStatus + "')]"))
						.getAttribute("value").trim();
				fc.utobj().selectDropDownByValue(driver, pobj.taskStatus, textVal);

				// assign Task To Lead Owner
				// fc.utobj().getElement(driver,pobj.customOwner)
				try {
					if (fc.utobj().getElement(driver, pobj.customOwner).isDisplayed()) {
						fc.utobj().clickElement(driver, pobj.assignToLeadOwnerSwitch);
					}
				} catch (Exception e) {
				}

				// TimeLess task
				try {
					if (fc.utobj().getElement(driver, pobj.startTaskImmediatelySwitch).isDisplayed()) {
						fc.utobj().clickElement(driver, pobj.timeLessTaskSwitch);
					}
				} catch (Exception e) {
				}

				fc.utobj().clickElement(driver, pobj.createTaskButton);
				fc.utobj().switchFrameToDefault(driver);

				fc.utobj().clickElement(driver, pobj.createWorkFlowButton);

				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}

		} else {
			fc.utobj().throwsSkipException("Was not able to Create WorkFlow For Lead Standard Type");
		}
	}

	// For All Contacts Matching condiftion

	public void createWorkFlowContactStandard(WebDriver driver, Map<String, String> config, Map<String, String> dataSet,
			String workFlowName, String triggerType, String conditionType, String availableFields,
			String matchingCondition, String campaignName, String contactStatus, String templateNameForSendEmail,
			String taskSubject, String taskPriority, String taskStatus) throws Exception {

		String testCaseId = "TC_Create_WorkFlow_Contact_Standard";

		if (fc.utobj().validate(testCaseId)) {

			try {

				CRMWorkflowsPage pobj = new CRMWorkflowsPage(driver);
				fc.utobj().clickElement(driver, pobj.workflowsLink);

				fc.utobj().clickElement(driver, pobj.createWorkFLowLink);
				fc.utobj().sendKeys(driver, pobj.workFlowName, workFlowName);
				fc.utobj().clickElement(driver, pobj.recordTypeContact);
				fc.utobj().clickElement(driver, pobj.workflowTypeStandard);
				fc.utobj().clickElement(driver, pobj.nextButton);

				// 1. :: Trigger :: When Lead is Added ::
				if (triggerType.equalsIgnoreCase("Added")) {

					fc.utobj().clickElement(driver, pobj.leadIsAdded);
					fc.utobj().clickElement(driver, pobj.nextButtonTrigger);

				} else if (triggerType.equalsIgnoreCase("Modified")) {

					fc.utobj().clickElement(driver, pobj.leadIsModified);
					fc.utobj().clickElement(driver, pobj.nextButtonTrigger);

				} else if (triggerType.equalsIgnoreCase("Added or Modified")) {

					fc.utobj().clickElement(driver, pobj.leadIsAddedOrModified);
					fc.utobj().clickElement(driver, pobj.nextButtonTrigger);
				}

				// 2. ::Condition ::

				if (conditionType.equalsIgnoreCase("All Contacts")) {

					fc.utobj().clickElement(driver, pobj.allLeads);
					fc.utobj().clickElement(driver, pobj.nextBtnCondition);

				} else if (conditionType.equalsIgnoreCase("Contacts Matching the Conditions")) {

					fc.utobj().clickElement(driver, pobj.leadsMatchingCondition);

					if (availableFields.equalsIgnoreCase("Birthdate")) {

						// select FirstName
						fc.utobj().clickElement(driver, pobj.availabelFieldsSelect);
						fc.utobj().sendKeys(driver,
								driver.findElement(By.xpath(".//*[@id='fc-drop-parentselectedColList']/div/div/input")),
								"First Name");

						fc.utobj().clickElement(driver, driver.findElement(By.xpath(
								".//*[@id='fc-drop-parentselectedColList']/div/ul/li/label/span[.='First Name']")));
						fc.utobj().clickElement(driver, pobj.availabelFieldsSelect);

						// select First Name
						fc.utobj().selectDropDown(driver, pobj.firstNameSelectContactMatch, "Equals");
						fc.utobj().sendKeys(driver, pobj.contactFirstNameField, dataSet.get("contactFirstName"));

						fc.utobj().clickElement(driver, pobj.availabelFieldsSelect);
						fc.utobj().sendKeys(driver,
								driver.findElement(By.xpath(".//*[@id='fc-drop-parentselectedColList']/div/div/input")),
								availableFields);

						fc.utobj().clickElement(driver,
								driver.findElement(
										By.xpath(".//*[@id='fc-drop-parentselectedColList']/div/ul/li/label/span[.='"
												+ availableFields + "']")));
						fc.utobj().clickElement(driver, pobj.availabelFieldsSelect);

						if (matchingCondition.equalsIgnoreCase("Is Between")) {

							fc.utobj().selectDropDown(driver, pobj.selectBirthDateContact, "Is Between");
							fc.utobj().sendKeys(driver, pobj.selectFromDateContact, dataSet.get("fromDate"));
							fc.utobj().sendKeys(driver, pobj.selectToDateContact, dataSet.get("toDate"));
							fc.utobj().clickElement(driver, pobj.selectBirthDateContact);
						}
					} else if (availableFields.equalsIgnoreCase("Email")) {

						// select FirstName
						fc.utobj().clickElement(driver, pobj.availabelFieldsSelect);
						fc.utobj().sendKeys(driver,
								driver.findElement(By.xpath(".//*[@id='fc-drop-parentselectedColList']/div/div/input")),
								"First Name");

						fc.utobj().clickElement(driver, driver.findElement(By.xpath(
								".//*[@id='fc-drop-parentselectedColList']/div/ul/li/label/span[.='First Name']")));
						fc.utobj().clickElement(driver, pobj.availabelFieldsSelect);

						// select First Name
						fc.utobj().selectDropDown(driver, pobj.firstNameSelectContactMatch, "Equals");
						fc.utobj().sendKeys(driver, pobj.contactFirstNameField, dataSet.get("contactFirstName"));

						fc.utobj().clickElement(driver, pobj.availabelFieldsSelect);
						fc.utobj().sendKeys(driver,
								driver.findElement(By.xpath(".//*[@id='fc-drop-parentselectedColList']/div/div/input")),
								availableFields);

						fc.utobj().clickElement(driver,
								driver.findElement(
										By.xpath(".//*[@id='fc-drop-parentselectedColList']/div/ul/li/label/span[.='"
												+ availableFields + "']")));
						fc.utobj().clickElement(driver, pobj.availabelFieldsSelect);

						if (matchingCondition.equalsIgnoreCase("Is Not Empty")) {

							fc.utobj().selectDropDown(driver, pobj.selectEmailIdMatchContact, matchingCondition);
						}
					} else if (availableFields.equalsIgnoreCase("Country")) {

						// select FirstName
						fc.utobj().clickElement(driver, pobj.availabelFieldsSelect);
						fc.utobj().sendKeys(driver,
								driver.findElement(By.xpath(".//*[@id='fc-drop-parentselectedColList']/div/div/input")),
								"First Name");

						fc.utobj().clickElement(driver, driver.findElement(By.xpath(
								".//*[@id='fc-drop-parentselectedColList']/div/ul/li/label/span[.='First Name']")));
						fc.utobj().clickElement(driver, pobj.availabelFieldsSelect);

						// select First Name
						fc.utobj().selectDropDown(driver, pobj.firstNameSelectContactMatch, "Equals");
						fc.utobj().sendKeys(driver, pobj.contactFirstNameField, dataSet.get("contactFirstName"));

						fc.utobj().clickElement(driver, pobj.availabelFieldsSelect);
						fc.utobj().sendKeys(driver, fc.utobj().getElement(driver, pobj.availabelFieldsSelect)
								.findElement(By.xpath("./div/div/input")), availableFields);
						fc.utobj().clickElement(driver,
								fc.utobj().getElement(driver, pobj.availabelFieldsSelect).findElement(By.xpath(
										"./div/ul/li/label/input[contains(@value , 'cmContactDetails')]/following-sibling::span[.='Country']")));
						fc.utobj().clickElement(driver, pobj.availabelFieldsSelect);

						if (matchingCondition.equalsIgnoreCase("In")) {

							fc.utobj().selectDropDown(driver, pobj.selectCountryMatchCaontact, "In");
							fc.utobj().clickElement(driver, pobj.selectCountryContact);
							fc.utobj().clickElement(driver,
									fc.utobj().getElement(driver, pobj.selectCountryContact)
											.findElement(By.xpath("./div/ul/li/label/span[.='Select All']")));
							fc.utobj().clickElement(driver,
									fc.utobj().getElement(driver, pobj.selectCountryContact)
											.findElement(By.xpath("./div/ul/li/label/span[.='Select All']")));

							fc.utobj()
									.sendKeys(driver,
											fc.utobj().getElement(driver, pobj.selectCountryContact)
													.findElement(By.xpath("./div/div/input")),
											dataSet.get("countryIn"));
							fc.utobj().clickElement(driver,
									fc.utobj().getElement(driver, pobj.selectCountryContact).findElement(
											By.xpath("./div/ul/li/label/span[.='" + dataSet.get("countryIn") + "']")));
							fc.utobj().clickElement(driver, pobj.selectCountryContact);
						}
					} else if (availableFields.equalsIgnoreCase("Status")) {

						// select FirstName
						fc.utobj().clickElement(driver, pobj.availabelFieldsSelect);
						fc.utobj().sendKeys(driver,
								driver.findElement(By.xpath(".//*[@id='fc-drop-parentselectedColList']/div/div/input")),
								"First Name");

						fc.utobj().clickElement(driver, driver.findElement(By.xpath(
								".//*[@id='fc-drop-parentselectedColList']/div/ul/li/label/span[.='First Name']")));
						fc.utobj().clickElement(driver, pobj.availabelFieldsSelect);

						// select First Name
						fc.utobj().selectDropDown(driver, pobj.firstNameSelectContactMatch, "Equals");
						fc.utobj().sendKeys(driver, pobj.contactFirstNameField, dataSet.get("contactFirstName"));

						fc.utobj().clickElement(driver, pobj.availabelFieldsSelect);
						fc.utobj().sendKeys(driver,
								driver.findElement(By.xpath(".//*[@id='fc-drop-parentselectedColList']/div/div/input")),
								availableFields);

						fc.utobj().clickElement(driver,
								driver.findElement(
										By.xpath(".//*[@id='fc-drop-parentselectedColList']/div/ul/li/label/span[.='"
												+ availableFields + "']")));
						fc.utobj().clickElement(driver, pobj.availabelFieldsSelect);

						if (matchingCondition.equalsIgnoreCase("In")) {

							fc.utobj().selectDropDown(driver, pobj.selectContactStatusMatch, "In");

							fc.utobj().clickElement(driver, pobj.selectContactStatus);
							fc.utobj().sendKeys(driver, fc.utobj().getElement(driver, pobj.selectContactStatus)
									.findElement(By.xpath("./div/div/input")), dataSet.get("selectContactStatus"));
							fc.utobj().clickElement(driver, fc.utobj()
									.getElement(driver, pobj.selectContactStatus).findElement(By.xpath(
											"./div/ul/li/label/span[.='" + dataSet.get("selectContactStatus") + "']")));
							fc.utobj().clickElement(driver, pobj.selectContactStatus);
						}

					} else if (availableFields.equalsIgnoreCase("Email Subscription Status")) {

						// select FirstName
						fc.utobj().clickElement(driver, pobj.availabelFieldsSelect);
						fc.utobj().sendKeys(driver,
								driver.findElement(By.xpath(".//*[@id='fc-drop-parentselectedColList']/div/div/input")),
								"First Name");

						fc.utobj().clickElement(driver, driver.findElement(By.xpath(
								".//*[@id='fc-drop-parentselectedColList']/div/ul/li/label/span[.='First Name']")));
						fc.utobj().clickElement(driver, pobj.availabelFieldsSelect);

						// select First Name
						fc.utobj().selectDropDown(driver, pobj.firstNameSelectContactMatch, "Equals");
						fc.utobj().sendKeys(driver, pobj.contactFirstNameField, dataSet.get("contactFirstName"));

						fc.utobj().clickElement(driver, pobj.availabelFieldsSelect);
						fc.utobj().sendKeys(driver,
								driver.findElement(By.xpath(".//*[@id='fc-drop-parentselectedColList']/div/div/input")),
								availableFields);

						fc.utobj().clickElement(driver,
								driver.findElement(
										By.xpath(".//*[@id='fc-drop-parentselectedColList']/div/ul/li/label/span[.='"
												+ availableFields + "']")));
						fc.utobj().clickElement(driver, pobj.availabelFieldsSelect);

						if (matchingCondition.equalsIgnoreCase("In")) {

							fc.utobj().selectDropDown(driver, pobj.selectEmailSubscribeContact, "In");

							fc.utobj().clickElement(driver, pobj.selectEmailSubscribeStatusContact);
							fc.utobj().sendKeys(driver,
									fc.utobj().getElement(driver, pobj.selectEmailSubscribeStatusContact)
											.findElement(By.xpath("./div/div/input")),
									dataSet.get("emailStatus"));
							fc.utobj().clickElement(driver,
									fc.utobj().getElement(driver, pobj.selectEmailSubscribeStatusContact)
											.findElement(By.xpath("./div/ul/li/label/span[contains(text () ,'"
													+ dataSet.get("emailStatus") + "')]")));
							fc.utobj().clickElement(driver, pobj.selectEmailSubscribeStatusContact);
						}
					} else if (availableFields.equalsIgnoreCase("Contact Type")) {

						// select FirstName
						fc.utobj().clickElement(driver, pobj.availabelFieldsSelect);
						fc.utobj().sendKeys(driver,
								driver.findElement(By.xpath(".//*[@id='fc-drop-parentselectedColList']/div/div/input")),
								"First Name");

						fc.utobj().clickElement(driver, driver.findElement(By.xpath(
								".//*[@id='fc-drop-parentselectedColList']/div/ul/li/label/span[.='First Name']")));
						fc.utobj().clickElement(driver, pobj.availabelFieldsSelect);

						// select First Name
						fc.utobj().selectDropDown(driver, pobj.firstNameSelectContactMatch, "Equals");
						fc.utobj().sendKeys(driver, pobj.contactFirstNameField, dataSet.get("contactFirstName"));

						fc.utobj().clickElement(driver, pobj.availabelFieldsSelect);
						fc.utobj().sendKeys(driver,
								driver.findElement(By.xpath(".//*[@id='fc-drop-parentselectedColList']/div/div/input")),
								availableFields);

						fc.utobj().clickElement(driver,
								driver.findElement(
										By.xpath(".//*[@id='fc-drop-parentselectedColList']/div/ul/li/label/span[.='"
												+ availableFields + "']")));
						fc.utobj().clickElement(driver, pobj.availabelFieldsSelect);

						if (matchingCondition.equalsIgnoreCase("In")) {

							fc.utobj().selectDropDown(driver, pobj.selectContactTypeMatch, "In");

							fc.utobj().clickElement(driver, pobj.selectContactType);
							fc.utobj().sendKeys(driver, fc.utobj().getElement(driver, pobj.selectContactType)
									.findElement(By.xpath("./div/div/input")), dataSet.get("selectcontactType"));
							fc.utobj().clickElement(driver,
									fc.utobj().getElement(driver, pobj.selectContactType).findElement(By.xpath(
											"./div/ul/li/label/span[.='" + dataSet.get("selectcontactType") + "']")));
							fc.utobj().clickElement(driver, pobj.selectContactType);
						}
					}

					fc.utobj().clickElement(driver, pobj.nextBtnCondition);
				}

				// 3. :: Action ::

				// 1.send Campaign
				fc.utobj().clickElement(driver, pobj.actionBtn);
				fc.utobj().clickElement(driver, pobj.sendCampaign);

				if (fc.utobj().getElement(driver, pobj.selectLocationAtCampaign).isDisplayed()) {
					fc.utobj().clickElement(driver, pobj.masterCampaign);
				}

				fc.utobj().sendKeys(driver, pobj.selectCampaign, campaignName);
				fc.utobj().clickElement(driver, pobj.saveBtn);

				// 2.change Status
				fc.utobj().clickElement(driver, pobj.actionBtn);
				fc.utobj().clickElement(driver, pobj.changesStatus);
				fc.utobj().selectDropDown(driver, pobj.selectStatus, contactStatus);
				fc.utobj().clickElement(driver, pobj.saveBtn);

				// 3.send Email
				fc.utobj().clickElement(driver, pobj.actionBtn);
				fc.utobj().clickElement(driver, pobj.sendEmail);

				fc.commonMethods().switch_frameClass_newLayoutcboxIframe(driver);

				// from/Reply :: Lead Owner ID
				// To :: Lead Email

				fc.utobj().selectDropDown(driver, pobj.selectTemplate, templateNameForSendEmail);
				fc.utobj().clickElement(driver, pobj.associateTemplateWithEmailButton);
				fc.utobj().switchFrameToDefault(driver);

				// 4.create Task
				fc.utobj().clickElement(driver, pobj.actionBtn);
				fc.utobj().clickElement(driver, pobj.createTask);

				fc.commonMethods().switch_frameClass_newLayoutcboxIframe(driver);

				fc.utobj().sendKeys(driver, pobj.subject, taskSubject);
				fc.utobj().selectDropDown(driver, pobj.priority, taskPriority);
				// fc.utobj().selectDropDown(driver, pobj.taskStatus,
				// taskStatus);

				String textVal = pobj.taskStatus
						.findElement(By.xpath("./option[contains(text () , '" + taskStatus + "')]"))
						.getAttribute("value").trim();
				fc.utobj().selectDropDownByValue(driver, pobj.taskStatus, textVal);

				// assign Task To Lead Owner
				try {
					if (fc.utobj().getElement(driver, pobj.customOwner).isDisplayed()) {
						fc.utobj().clickElement(driver, pobj.assignToLeadOwnerSwitch);
					}
				} catch (Exception e) {
				}

				// TimeLess task
				try {
					if (fc.utobj().getElement(driver, pobj.startTaskImmediatelySwitch).isDisplayed()) {
						fc.utobj().clickElement(driver, pobj.timeLessTaskSwitch);
					}
				} catch (Exception e) {
				}

				fc.utobj().clickElement(driver, pobj.createTaskButton);
				fc.utobj().switchFrameToDefault(driver);

				fc.utobj().clickElement(driver, pobj.createWorkFlowButton);

				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException("Was not able to Create WorkFlow For Contact Standard Type");
		}
	}

	// For Date Based Contact Type
	public void createWorkFlowContactDateBased(WebDriver driver, Map<String, String> config,
			Map<String, String> dataSet, String workFlowName, String triggerType, String dateOfExecution,
			String conditionType, String availableFields, String matchingCondition, String campaignName,
			String contactStatus, String templateNameForSendEmail, String taskSubject, String taskPriority,
			String taskStatus) throws Exception {

		String testCaseId = "TC_Create_WorkFlow_Contact_Date_Based";

		if (fc.utobj().validate(testCaseId)) {

			try {

				CRMWorkflowsPage pobj = new CRMWorkflowsPage(driver);
				fc.utobj().clickElement(driver, pobj.workflowsLink);
				fc.utobj().clickElement(driver, pobj.createWorkFLowLink);
				fc.utobj().sendKeys(driver, pobj.workFlowName, workFlowName);
				fc.utobj().clickElement(driver, pobj.recordTypeContact);
				fc.utobj().clickElement(driver, pobj.workflowTypeDateBased);
				fc.utobj().clickElement(driver, pobj.nextButton);

				// 1. :: Trigger :: When Contact is Added ::

				// 1. :: Trigger :: Choose A DateField ::
				if (triggerType.equalsIgnoreCase("BirthDate")) {

					fc.utobj().selectDropDown(driver, pobj.selectADateField, "Birthday");

					if (dateOfExecution.equalsIgnoreCase("Same Day")) {

						fc.utobj().selectDropDown(driver, pobj.dateOfExecutionSelect, "Same Day");

					} else if (dateOfExecution.equalsIgnoreCase("Before")) {

						fc.utobj().selectDropDown(driver, pobj.dateOfExecutionSelect, "Before");
						fc.utobj().sendKeys(driver, pobj.daysDifference, dataSet.get("daysDifference"));

					} else if (dateOfExecution.equalsIgnoreCase("After")) {

						fc.utobj().selectDropDown(driver, pobj.dateOfExecutionSelect, "After");
						fc.utobj().sendKeys(driver, pobj.daysDifference, dataSet.get("daysDifference"));
					}
				} else if (triggerType.equalsIgnoreCase("Anniversary Date")) {

					fc.utobj().selectDropDown(driver, pobj.selectADateField, "Anniversary Date");

					if (dateOfExecution.equalsIgnoreCase("Same Day")) {

						fc.utobj().selectDropDown(driver, pobj.dateOfExecutionSelect, "Same Day");

					} else if (dateOfExecution.equalsIgnoreCase("Before")) {

						fc.utobj().selectDropDown(driver, pobj.dateOfExecutionSelect, "Before");
						fc.utobj().sendKeys(driver, pobj.daysDifference, dataSet.get("daysDifference"));

					} else if (dateOfExecution.equalsIgnoreCase("After")) {

						fc.utobj().selectDropDown(driver, pobj.dateOfExecutionSelect, "After");
						fc.utobj().sendKeys(driver, pobj.daysDifference, dataSet.get("daysDifference"));
					}
				} else if (triggerType.equalsIgnoreCase("Last Purchase Date")) {

					fc.utobj().selectDropDown(driver, pobj.selectADateField, "Last Purchase Date");

					if (dateOfExecution.equalsIgnoreCase("Same Day")) {

						fc.utobj().selectDropDown(driver, pobj.dateOfExecutionSelect, "Same Day");

					} else if (dateOfExecution.equalsIgnoreCase("After")) {

						fc.utobj().selectDropDown(driver, pobj.dateOfExecutionSelect, "After");
						fc.utobj().sendKeys(driver, pobj.daysDifference, dataSet.get("daysDifference"));
					}

				}

				fc.utobj().clickElement(driver, pobj.nextButtonTrigger);

				// 2. ::Condition ::

				if (conditionType.equalsIgnoreCase("All Contacts")) {

					fc.utobj().clickElement(driver, pobj.allLeads);
					fc.utobj().clickElement(driver, pobj.nextBtnCondition);

				} else if (conditionType.equalsIgnoreCase("Contacts Matching the Conditions")) {

					fc.utobj().clickElement(driver, pobj.leadsMatchingCondition);

					if (availableFields.equalsIgnoreCase("Birthdate")) {

						// select FirstName
						fc.utobj().clickElement(driver, pobj.availabelFieldsSelect);
						fc.utobj().sendKeys(driver,
								driver.findElement(By.xpath(".//*[@id='fc-drop-parentselectedColList']/div/div/input")),
								"First Name");

						fc.utobj().clickElement(driver, driver.findElement(By.xpath(
								".//*[@id='fc-drop-parentselectedColList']/div/ul/li/label/span[.='First Name']")));
						fc.utobj().clickElement(driver, pobj.availabelFieldsSelect);

						// select First Name
						fc.utobj().selectDropDown(driver, pobj.firstNameSelectContactMatch, "Equals");
						fc.utobj().sendKeys(driver, pobj.contactFirstNameField, dataSet.get("contactFirstName"));

						fc.utobj().clickElement(driver, pobj.availabelFieldsSelect);
						fc.utobj().sendKeys(driver,
								driver.findElement(By.xpath(".//*[@id='fc-drop-parentselectedColList']/div/div/input")),
								availableFields);

						fc.utobj().clickElement(driver,
								driver.findElement(
										By.xpath(".//*[@id='fc-drop-parentselectedColList']/div/ul/li/label/span[.='"
												+ availableFields + "']")));
						fc.utobj().clickElement(driver, pobj.availabelFieldsSelect);

						if (matchingCondition.equalsIgnoreCase("Is Between")) {

							fc.utobj().selectDropDown(driver, pobj.selectBirthDateContact, "Is Between");
							fc.utobj().sendKeys(driver, pobj.selectFromDateContact, dataSet.get("fromDate"));
							fc.utobj().sendKeys(driver, pobj.selectToDateContact, dataSet.get("toDate"));
							fc.utobj().clickElement(driver, pobj.selectBirthDateContact);
						}
					} else if (availableFields.equalsIgnoreCase("Email")) {

						// select FirstName
						fc.utobj().clickElement(driver, pobj.availabelFieldsSelect);
						fc.utobj().sendKeys(driver,
								driver.findElement(By.xpath(".//*[@id='fc-drop-parentselectedColList']/div/div/input")),
								"First Name");

						fc.utobj().clickElement(driver, driver.findElement(By.xpath(
								".//*[@id='fc-drop-parentselectedColList']/div/ul/li/label/span[.='First Name']")));
						fc.utobj().clickElement(driver, pobj.availabelFieldsSelect);

						// select First Name
						fc.utobj().selectDropDown(driver, pobj.firstNameSelectContactMatch, "Equals");
						fc.utobj().sendKeys(driver, pobj.contactFirstNameField, dataSet.get("contactFirstName"));

						fc.utobj().clickElement(driver, pobj.availabelFieldsSelect);
						fc.utobj().sendKeys(driver,
								driver.findElement(By.xpath(".//*[@id='fc-drop-parentselectedColList']/div/div/input")),
								availableFields);

						fc.utobj().clickElement(driver,
								driver.findElement(
										By.xpath(".//*[@id='fc-drop-parentselectedColList']/div/ul/li/label/span[.='"
												+ availableFields + "']")));
						fc.utobj().clickElement(driver, pobj.availabelFieldsSelect);

						if (matchingCondition.equalsIgnoreCase("Is Not Empty")) {

							fc.utobj().selectDropDown(driver, pobj.selectEmailIdMatchContact, matchingCondition);
						}
					} else if (availableFields.equalsIgnoreCase("Country")) {

						// select FirstName
						fc.utobj().clickElement(driver, pobj.availabelFieldsSelect);
						fc.utobj().sendKeys(driver,
								driver.findElement(By.xpath(".//*[@id='fc-drop-parentselectedColList']/div/div/input")),
								"First Name");

						fc.utobj().clickElement(driver, driver.findElement(By.xpath(
								".//*[@id='fc-drop-parentselectedColList']/div/ul/li/label/span[.='First Name']")));
						fc.utobj().clickElement(driver, pobj.availabelFieldsSelect);

						// select First Name
						fc.utobj().selectDropDown(driver, pobj.firstNameSelectContactMatch, "Equals");
						fc.utobj().sendKeys(driver, pobj.contactFirstNameField, dataSet.get("contactFirstName"));

						fc.utobj().clickElement(driver, pobj.availabelFieldsSelect);
						fc.utobj().sendKeys(driver, fc.utobj().getElement(driver, pobj.availabelFieldsSelect)
								.findElement(By.xpath("./div/div/input")), availableFields);
						fc.utobj().clickElement(driver,
								fc.utobj().getElement(driver, pobj.availabelFieldsSelect).findElement(By.xpath(
										"./div/ul/li/label/input[contains(@value , 'cmContactDetails')]/following-sibling::span[.='Country']")));
						fc.utobj().clickElement(driver, pobj.availabelFieldsSelect);

						if (matchingCondition.equalsIgnoreCase("In")) {

							fc.utobj().selectDropDown(driver, pobj.selectCountryMatchCaontact, "In");
							fc.utobj().clickElement(driver, pobj.selectCountryContact);
							fc.utobj().clickElement(driver,
									fc.utobj().getElement(driver, pobj.selectCountryContact)
											.findElement(By.xpath("./div/ul/li/label/span[.='Select All']")));
							fc.utobj().clickElement(driver,
									fc.utobj().getElement(driver, pobj.selectCountryContact)
											.findElement(By.xpath("./div/ul/li/label/span[.='Select All']")));

							fc.utobj()
									.sendKeys(driver,
											fc.utobj().getElement(driver, pobj.selectCountryContact)
													.findElement(By.xpath("./div/div/input")),
											dataSet.get("countryIn"));
							fc.utobj().clickElement(driver,
									fc.utobj().getElement(driver, pobj.selectCountryContact).findElement(
											By.xpath("./div/ul/li/label/span[.='" + dataSet.get("countryIn") + "']")));
							fc.utobj().clickElement(driver, pobj.selectCountryContact);
						}
					} else if (availableFields.equalsIgnoreCase("Status")) {

						// select FirstName
						fc.utobj().clickElement(driver, pobj.availabelFieldsSelect);
						fc.utobj().sendKeys(driver,
								driver.findElement(By.xpath(".//*[@id='fc-drop-parentselectedColList']/div/div/input")),
								"First Name");

						fc.utobj().clickElement(driver, driver.findElement(By.xpath(
								".//*[@id='fc-drop-parentselectedColList']/div/ul/li/label/span[.='First Name']")));
						fc.utobj().clickElement(driver, pobj.availabelFieldsSelect);

						// select First Name
						fc.utobj().selectDropDown(driver, pobj.firstNameSelectContactMatch, "Equals");
						fc.utobj().sendKeys(driver, pobj.contactFirstNameField, dataSet.get("contactFirstName"));

						fc.utobj().clickElement(driver, pobj.availabelFieldsSelect);
						fc.utobj().sendKeys(driver,
								driver.findElement(By.xpath(".//*[@id='fc-drop-parentselectedColList']/div/div/input")),
								availableFields);

						fc.utobj().clickElement(driver,
								driver.findElement(
										By.xpath(".//*[@id='fc-drop-parentselectedColList']/div/ul/li/label/span[.='"
												+ availableFields + "']")));
						fc.utobj().clickElement(driver, pobj.availabelFieldsSelect);

						if (matchingCondition.equalsIgnoreCase("In")) {

							fc.utobj().selectDropDown(driver, pobj.selectContactStatusMatch, "In");

							fc.utobj().clickElement(driver, pobj.selectContactStatus);
							fc.utobj().sendKeys(driver, fc.utobj().getElement(driver, pobj.selectContactStatus)
									.findElement(By.xpath("./div/div/input")), dataSet.get("selectContactStatus"));
							fc.utobj().clickElement(driver, fc.utobj()
									.getElement(driver, pobj.selectContactStatus).findElement(By.xpath(
											"./div/ul/li/label/span[.='" + dataSet.get("selectContactStatus") + "']")));
							fc.utobj().clickElement(driver, pobj.selectContactStatus);
						}

					} else if (availableFields.equalsIgnoreCase("Email Subscription Status")) {

						// select FirstName
						fc.utobj().clickElement(driver, pobj.availabelFieldsSelect);
						fc.utobj().sendKeys(driver,
								driver.findElement(By.xpath(".//*[@id='fc-drop-parentselectedColList']/div/div/input")),
								"First Name");

						fc.utobj().clickElement(driver, driver.findElement(By.xpath(
								".//*[@id='fc-drop-parentselectedColList']/div/ul/li/label/span[.='First Name']")));
						fc.utobj().clickElement(driver, pobj.availabelFieldsSelect);

						// select First Name
						fc.utobj().selectDropDown(driver, pobj.firstNameSelectContactMatch, "Equals");
						fc.utobj().sendKeys(driver, pobj.contactFirstNameField, dataSet.get("contactFirstName"));

						fc.utobj().clickElement(driver, pobj.availabelFieldsSelect);
						fc.utobj().sendKeys(driver,
								driver.findElement(By.xpath(".//*[@id='fc-drop-parentselectedColList']/div/div/input")),
								availableFields);

						fc.utobj().clickElement(driver,
								driver.findElement(
										By.xpath(".//*[@id='fc-drop-parentselectedColList']/div/ul/li/label/span[.='"
												+ availableFields + "']")));
						fc.utobj().clickElement(driver, pobj.availabelFieldsSelect);

						if (matchingCondition.equalsIgnoreCase("In")) {

							fc.utobj().selectDropDown(driver, pobj.selectEmailSubscribeContact, "In");

							fc.utobj().clickElement(driver, pobj.selectEmailSubscribeStatusContact);
							fc.utobj().sendKeys(driver,
									fc.utobj().getElement(driver, pobj.selectEmailSubscribeStatusContact)
											.findElement(By.xpath("./div/div/input")),
									dataSet.get("emailStatus"));
							fc.utobj().clickElement(driver,
									fc.utobj().getElement(driver, pobj.selectEmailSubscribeStatusContact)
											.findElement(By.xpath("./div/ul/li/label/span[contains(text () ,'"
													+ dataSet.get("emailStatus") + "')]")));
							fc.utobj().clickElement(driver, pobj.selectEmailSubscribeStatusContact);
						}
					} else if (availableFields.equalsIgnoreCase("Contact Type")) {

						// select FirstName
						fc.utobj().clickElement(driver, pobj.availabelFieldsSelect);
						fc.utobj().sendKeys(driver,
								driver.findElement(By.xpath(".//*[@id='fc-drop-parentselectedColList']/div/div/input")),
								"First Name");

						fc.utobj().clickElement(driver, driver.findElement(By.xpath(
								".//*[@id='fc-drop-parentselectedColList']/div/ul/li/label/span[.='First Name']")));
						fc.utobj().clickElement(driver, pobj.availabelFieldsSelect);

						// select First Name
						fc.utobj().selectDropDown(driver, pobj.firstNameSelectContactMatch, "Equals");
						fc.utobj().sendKeys(driver, pobj.contactFirstNameField, dataSet.get("contactFirstName"));

						fc.utobj().clickElement(driver, pobj.availabelFieldsSelect);
						fc.utobj().sendKeys(driver,
								driver.findElement(By.xpath(".//*[@id='fc-drop-parentselectedColList']/div/div/input")),
								availableFields);

						fc.utobj().clickElement(driver,
								driver.findElement(
										By.xpath(".//*[@id='fc-drop-parentselectedColList']/div/ul/li/label/span[.='"
												+ availableFields + "']")));
						fc.utobj().clickElement(driver, pobj.availabelFieldsSelect);

						if (matchingCondition.equalsIgnoreCase("In")) {

							fc.utobj().selectDropDown(driver, pobj.selectContactTypeMatch, "In");

							fc.utobj().clickElement(driver, pobj.selectContactType);
							fc.utobj().sendKeys(driver, fc.utobj().getElement(driver, pobj.selectContactType)
									.findElement(By.xpath("./div/div/input")), dataSet.get("selectcontactType"));
							fc.utobj().clickElement(driver,
									fc.utobj().getElement(driver, pobj.selectContactType).findElement(By.xpath(
											"./div/ul/li/label/span[.='" + dataSet.get("selectcontactType") + "']")));
							fc.utobj().clickElement(driver, pobj.selectContactType);
						}
					}

					fc.utobj().clickElement(driver, pobj.nextBtnCondition);
				}

				// 3. :: Action ::

				// 1.send Campaign
				fc.utobj().clickElement(driver, pobj.actionBtn);
				fc.utobj().clickElement(driver, pobj.sendCampaign);

				if (fc.utobj().getElement(driver, pobj.selectLocationAtCampaign).isDisplayed()) {
					fc.utobj().clickElement(driver, pobj.masterCampaign);
				}

				fc.utobj().sendKeys(driver, pobj.selectCampaign, campaignName);
				fc.utobj().clickElement(driver, pobj.saveBtn);

				// 2.change Status
				fc.utobj().clickElement(driver, pobj.actionBtn);
				fc.utobj().clickElement(driver, pobj.changesStatus);
				fc.utobj().selectDropDown(driver, pobj.selectStatus, contactStatus);
				fc.utobj().clickElement(driver, pobj.saveBtn);

				// 3.send Email
				fc.utobj().clickElement(driver, pobj.actionBtn);
				fc.utobj().clickElement(driver, pobj.sendEmail);

				fc.commonMethods().switch_frameClass_newLayoutcboxIframe(driver);

				// from/Reply :: Lead Owner ID
				// To :: Lead Email

				fc.utobj().selectDropDown(driver, pobj.selectTemplate, templateNameForSendEmail);
				fc.utobj().clickElement(driver, pobj.associateTemplateWithEmailButton);
				fc.utobj().switchFrameToDefault(driver);

				// 4.create Task
				fc.utobj().clickElement(driver, pobj.actionBtn);
				fc.utobj().clickElement(driver, pobj.createTask);

				fc.commonMethods().switch_frameClass_newLayoutcboxIframe(driver);

				fc.utobj().sendKeys(driver, pobj.subject, taskSubject);
				fc.utobj().selectDropDown(driver, pobj.priority, taskPriority);
				// fc.utobj().selectDropDown(driver, pobj.taskStatus,
				// taskStatus);

				String textVal = pobj.taskStatus
						.findElement(By.xpath("./option[contains(text () , '" + taskStatus + "')]"))
						.getAttribute("value").trim();
				fc.utobj().selectDropDownByValue(driver, pobj.taskStatus, textVal);

				// assign Task To Lead Owner
				try {
					if (fc.utobj().getElement(driver, pobj.customOwner).isDisplayed()) {
						fc.utobj().clickElement(driver, pobj.assignToLeadOwnerSwitch);
					}
				} catch (Exception e) {
				}

				// TimeLess task
				try {
					if (fc.utobj().getElement(driver, pobj.startTaskImmediatelySwitch).isDisplayed()) {
						fc.utobj().clickElement(driver, pobj.timeLessTaskSwitch);
					}
				} catch (Exception e) {
				}

				fc.utobj().clickElement(driver, pobj.createTaskButton);
				fc.utobj().switchFrameToDefault(driver);

				fc.utobj().clickElement(driver, pobj.createWorkFlowButton);

				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException("Was not able to Create WorkFlow For Contact Standard Type");
		}
	}

	// For Lead Date Based WorkFlow

	public void createWorkFlowLeadDateBased(WebDriver driver, Map<String, String> config, Map<String, String> dataSet,
			String workFlowName, String triggerType, String dateOfExecution, String conditionType,
			String availableFields, String matchingCondition, String campaignName, String leadStatus,
			String templateNameForSendEmail, String taskSubject, String taskPriority, String taskStatus)
			throws Exception {

		String testCaseId = "TC_Create_WorkFlow_Lead_Date_Based";

		if (fc.utobj().validate(testCaseId)) {

			try {

				CRMWorkflowsPage pobj = new CRMWorkflowsPage(driver);
				fc.utobj().clickElement(driver, pobj.workflowsLink);
				fc.utobj().clickElement(driver, pobj.createWorkFLowLink);
				fc.utobj().sendKeys(driver, pobj.workFlowName, workFlowName);
				fc.utobj().clickElement(driver, pobj.recordTypeLead);
				fc.utobj().clickElement(driver, pobj.workflowTypeDateBased);

				fc.utobj().clickElement(driver, pobj.nextButton);

				// 1. :: Trigger :: Choose A DateField ::
				if (triggerType.equalsIgnoreCase("BirthDate")) {

					fc.utobj().selectDropDown(driver, pobj.selectADateField, "Birthday");

					if (dateOfExecution.equalsIgnoreCase("Same Day")) {

						fc.utobj().selectDropDown(driver, pobj.dateOfExecutionSelect, "Same Day");

					} else if (dateOfExecution.equalsIgnoreCase("Before")) {

						fc.utobj().selectDropDown(driver, pobj.dateOfExecutionSelect, "Before");
						fc.utobj().sendKeys(driver, pobj.daysDifference, dataSet.get("daysDifference"));

					} else if (dateOfExecution.equalsIgnoreCase("After")) {

						fc.utobj().selectDropDown(driver, pobj.dateOfExecutionSelect, "After");
						fc.utobj().sendKeys(driver, pobj.daysDifference, dataSet.get("daysDifference"));
					}
				} else if (triggerType.equalsIgnoreCase("Anniversary Date")) {

					fc.utobj().selectDropDown(driver, pobj.selectADateField, "Anniversary Date");

					if (dateOfExecution.equalsIgnoreCase("Same Day")) {

						fc.utobj().selectDropDown(driver, pobj.dateOfExecutionSelect, "Same Day");

					} else if (dateOfExecution.equalsIgnoreCase("Before")) {

						fc.utobj().selectDropDown(driver, pobj.dateOfExecutionSelect, "Before");
						fc.utobj().sendKeys(driver, pobj.daysDifference, dataSet.get("daysDifference"));

					} else if (dateOfExecution.equalsIgnoreCase("After")) {

						fc.utobj().selectDropDown(driver, pobj.dateOfExecutionSelect, "After");
						fc.utobj().sendKeys(driver, pobj.daysDifference, dataSet.get("daysDifference"));
					}
				}

				fc.utobj().clickElement(driver, pobj.nextButtonTrigger);

				// 2. ::Condition ::

				if (conditionType.equalsIgnoreCase("All Leads")) {

					fc.utobj().clickElement(driver, pobj.allLeads);
					fc.utobj().clickElement(driver, pobj.nextBtnCondition);

				} else if (conditionType.equalsIgnoreCase("Leads Matching The Conditions")) {

					fc.utobj().clickElement(driver, pobj.leadsMatchingCondition);

					if (availableFields.equalsIgnoreCase("Birthdate")) {

						// for unique condition
						fc.utobj().clickElement(driver, pobj.availabelFieldsSelect);
						fc.utobj().sendKeys(driver,
								driver.findElement(By.xpath(".//*[@id='fc-drop-parentselectedColList']/div/div/input")),
								"First Name");
						fc.utobj().clickElement(driver, driver.findElement(By.xpath(
								".//*[@id='fc-drop-parentselectedColList']/div/ul/li/label/span[.='First Name']")));
						fc.utobj().clickElement(driver, pobj.availabelFieldsSelect);

						// Equals Test Data
						fc.utobj().selectDropDown(driver, pobj.firstNameSelectMatch, "Equals");
						fc.utobj().sendKeys(driver, pobj.firstNameField, dataSet.get("leadFirstName"));

						fc.utobj().clickElement(driver, pobj.availabelFieldsSelect);
						fc.utobj().sendKeys(driver,
								driver.findElement(By.xpath(".//*[@id='fc-drop-parentselectedColList']/div/div/input")),
								availableFields);

						fc.utobj().clickElement(driver,
								driver.findElement(
										By.xpath(".//*[@id='fc-drop-parentselectedColList']/div/ul/li/label/span[.='"
												+ availableFields + "']")));
						fc.utobj().clickElement(driver, pobj.availabelFieldsSelect);

						if (matchingCondition.equalsIgnoreCase("Is Between")) {

							fc.utobj().selectDropDown(driver, pobj.selectBirthDateMatch, "Is Between");
							fc.utobj().sendKeys(driver, pobj.birthdayFromDate, dataSet.get("fromDate"));
							fc.utobj().sendKeys(driver, pobj.birthdayToDate, dataSet.get("toDate"));
							fc.utobj().clickElement(driver, pobj.selectBirthDateMatch);
						}
					} else if (availableFields.equalsIgnoreCase("Email")) {

						// for unique condition
						fc.utobj().clickElement(driver, pobj.availabelFieldsSelect);
						fc.utobj().sendKeys(driver,
								driver.findElement(By.xpath(".//*[@id='fc-drop-parentselectedColList']/div/div/input")),
								"First Name");
						fc.utobj().clickElement(driver, driver.findElement(By.xpath(
								".//*[@id='fc-drop-parentselectedColList']/div/ul/li/label/span[.='First Name']")));
						fc.utobj().clickElement(driver, pobj.availabelFieldsSelect);

						// Equals Test Data
						fc.utobj().selectDropDown(driver, pobj.firstNameSelectMatch, "Equals");
						fc.utobj().sendKeys(driver, pobj.firstNameField, dataSet.get("leadFirstName"));

						fc.utobj().clickElement(driver, pobj.availabelFieldsSelect);
						fc.utobj().sendKeys(driver,
								driver.findElement(By.xpath(".//*[@id='fc-drop-parentselectedColList']/div/div/input")),
								availableFields);

						fc.utobj().clickElement(driver,
								driver.findElement(
										By.xpath(".//*[@id='fc-drop-parentselectedColList']/div/ul/li/label/span[.='"
												+ availableFields + "']")));
						fc.utobj().clickElement(driver, pobj.availabelFieldsSelect);

						if (matchingCondition.equalsIgnoreCase("Is Not Empty")) {

							fc.utobj().selectDropDown(driver, pobj.selectLeadInfoEmailId, matchingCondition);
						}

					} else if (availableFields.equalsIgnoreCase("Country")) {

						// for unique condition
						fc.utobj().clickElement(driver, pobj.availabelFieldsSelect);
						fc.utobj().sendKeys(driver,
								driver.findElement(By.xpath(".//*[@id='fc-drop-parentselectedColList']/div/div/input")),
								"First Name");
						fc.utobj().clickElement(driver, driver.findElement(By.xpath(
								".//*[@id='fc-drop-parentselectedColList']/div/ul/li/label/span[.='First Name']")));
						fc.utobj().clickElement(driver, pobj.availabelFieldsSelect);

						// Equals Test Data
						fc.utobj().selectDropDown(driver, pobj.firstNameSelectMatch, "Equals");
						fc.utobj().sendKeys(driver, pobj.firstNameField, dataSet.get("leadFirstName"));

						fc.utobj().clickElement(driver, pobj.availabelFieldsSelect);
						fc.utobj().sendKeys(driver,
								driver.findElement(By.xpath(".//*[@id='fc-drop-parentselectedColList']/div/div/input")),
								availableFields);

						fc.utobj().clickElement(driver,
								driver.findElement(
										By.xpath(".//*[@id='fc-drop-parentselectedColList']/div/ul/li/label/span[.='"
												+ availableFields + "']")));
						fc.utobj().clickElement(driver, pobj.availabelFieldsSelect);

						if (matchingCondition.equalsIgnoreCase("In")) {

							fc.utobj().selectDropDown(driver, pobj.countryMatch, "In");
							fc.utobj().clickElement(driver, pobj.countrySelect);
							fc.utobj().clickElement(driver, fc.utobj().getElement(driver, pobj.countrySelect)
									.findElement(By.xpath("./div/ul/li/label/span[.='Select All']")));
							fc.utobj().clickElement(driver, fc.utobj().getElement(driver, pobj.countrySelect)
									.findElement(By.xpath("./div/ul/li/label/span[.='Select All']")));

							fc.utobj().sendKeys(driver, fc.utobj().getElement(driver, pobj.countrySelect)
									.findElement(By.xpath("./div/div/input")), dataSet.get("countryIn"));
							fc.utobj().clickElement(driver,
									fc.utobj().getElement(driver, pobj.countrySelect).findElement(
											By.xpath("./div/ul/li/label/span[.='" + dataSet.get("countryIn") + "']")));
							fc.utobj().clickElement(driver, pobj.countrySelect);
						}
					} else if (availableFields.equalsIgnoreCase("Status")) {

						// for unique condition
						fc.utobj().clickElement(driver, pobj.availabelFieldsSelect);
						fc.utobj().sendKeys(driver,
								driver.findElement(By.xpath(".//*[@id='fc-drop-parentselectedColList']/div/div/input")),
								"First Name");
						fc.utobj().clickElement(driver, driver.findElement(By.xpath(
								".//*[@id='fc-drop-parentselectedColList']/div/ul/li/label/span[.='First Name']")));
						fc.utobj().clickElement(driver, pobj.availabelFieldsSelect);

						// Equals Test Data
						fc.utobj().selectDropDown(driver, pobj.firstNameSelectMatch, "Equals");
						fc.utobj().sendKeys(driver, pobj.firstNameField, dataSet.get("leadFirstName"));

						fc.utobj().clickElement(driver, pobj.availabelFieldsSelect);
						fc.utobj().sendKeys(driver,
								driver.findElement(By.xpath(".//*[@id='fc-drop-parentselectedColList']/div/div/input")),
								availableFields);

						fc.utobj().clickElement(driver,
								driver.findElement(
										By.xpath(".//*[@id='fc-drop-parentselectedColList']/div/ul/li/label/span[.='"
												+ availableFields + "']")));
						fc.utobj().clickElement(driver, pobj.availabelFieldsSelect);

						if (matchingCondition.equalsIgnoreCase("In")) {

							fc.utobj().selectDropDown(driver, pobj.selectLeadStatusMatch, "In");

							fc.utobj().clickElement(driver, pobj.selectLeadStatus);
							// fc.utobj().sendKeys(driver,
							// fc.utobj().getElement(driver,
							// pobj.selectLeadStatus).findElement(By.xpath("./div/div/input")),
							// dataSet.get("selectLeadStatus"));
							fc.utobj().clickElement(driver,
									fc.utobj().getElement(driver, pobj.selectLeadStatus).findElement(By.xpath(
											"./div/ul/li/label/span[.='" + dataSet.get("selectLeadStatus") + "']")));
							fc.utobj().clickElement(driver, pobj.selectLeadStatus);
						}

					} else if (availableFields.equalsIgnoreCase("Email Subscription Status")) {

						// for unique condition
						fc.utobj().clickElement(driver, pobj.availabelFieldsSelect);
						fc.utobj().sendKeys(driver,
								driver.findElement(By.xpath(".//*[@id='fc-drop-parentselectedColList']/div/div/input")),
								"First Name");
						fc.utobj().clickElement(driver, driver.findElement(By.xpath(
								".//*[@id='fc-drop-parentselectedColList']/div/ul/li/label/span[.='First Name']")));
						fc.utobj().clickElement(driver, pobj.availabelFieldsSelect);

						// Equals Test Data
						fc.utobj().selectDropDown(driver, pobj.firstNameSelectMatch, "Equals");
						fc.utobj().sendKeys(driver, pobj.firstNameField, dataSet.get("leadFirstName"));

						fc.utobj().clickElement(driver, pobj.availabelFieldsSelect);
						fc.utobj().sendKeys(driver,
								driver.findElement(By.xpath(".//*[@id='fc-drop-parentselectedColList']/div/div/input")),
								availableFields);

						fc.utobj().clickElement(driver,
								driver.findElement(
										By.xpath(".//*[@id='fc-drop-parentselectedColList']/div/ul/li/label/span[.='"
												+ availableFields + "']")));
						fc.utobj().clickElement(driver, pobj.availabelFieldsSelect);

						if (matchingCondition.equalsIgnoreCase("In")) {

							fc.utobj().selectDropDown(driver, pobj.selectEmailSubScriptionMatch, "In");

							fc.utobj().clickElement(driver, pobj.selectEmailSubscribe);
							fc.utobj()
									.sendKeys(driver,
											fc.utobj().getElement(driver, pobj.selectEmailSubscribe)
													.findElement(By.xpath("./div/div/input")),
											dataSet.get("emailStatus"));
							fc.utobj().clickElement(driver,
									fc.utobj().getElement(driver, pobj.selectEmailSubscribe)
											.findElement(By.xpath("./div/ul/li/label/span[contains(text () ,'"
													+ dataSet.get("emailStatus") + "')]")));
							fc.utobj().clickElement(driver, pobj.selectEmailSubscribe);
						}
					}

					fc.utobj().clickElement(driver, pobj.nextBtnCondition);
				}

				// 3. :: Action ::

				// 1.send Campaign
				fc.utobj().clickElement(driver, pobj.actionBtn);
				fc.utobj().clickElement(driver, pobj.sendCampaign);

				if (fc.utobj().getElement(driver, pobj.selectLocationAtCampaign).isDisplayed()) {
					fc.utobj().clickElement(driver, pobj.masterCampaign);
				}

				fc.utobj().sendKeys(driver, pobj.selectCampaign, campaignName);
				fc.utobj().clickElement(driver, pobj.saveBtn);

				// 2.change Status
				fc.utobj().clickElement(driver, pobj.actionBtn);
				fc.utobj().clickElement(driver, pobj.changesStatus);
				fc.utobj().selectDropDown(driver, pobj.selectStatus, leadStatus);
				fc.utobj().clickElement(driver, pobj.saveBtn);

				// 3.send Email
				fc.utobj().clickElement(driver, pobj.actionBtn);
				fc.utobj().clickElement(driver, pobj.sendEmail);

				fc.commonMethods().switch_frameClass_newLayoutcboxIframe(driver);

				// from/Reply :: Lead Owner ID
				// To :: Lead Email

				fc.utobj().selectDropDown(driver, pobj.selectTemplate, templateNameForSendEmail);
				fc.utobj().clickElement(driver, pobj.associateTemplateWithEmailButton);
				fc.utobj().switchFrameToDefault(driver);

				// 4.create Task
				fc.utobj().clickElement(driver, pobj.actionBtn);
				fc.utobj().clickElement(driver, pobj.createTask);

				fc.commonMethods().switch_frameClass_newLayoutcboxIframe(driver);

				fc.utobj().sendKeys(driver, pobj.subject, taskSubject);
				fc.utobj().selectDropDown(driver, pobj.priority, taskPriority);
				// fc.utobj().selectDropDown(driver, pobj.taskStatus,
				// taskStatus);

				String textVal = pobj.taskStatus
						.findElement(By.xpath("./option[contains(text () , '" + taskStatus + "')]"))
						.getAttribute("value").trim();
				fc.utobj().selectDropDownByValue(driver, pobj.taskStatus, textVal);

				// assign Task To Lead Owner
				try {
					if (fc.utobj().getElement(driver, pobj.customOwner).isDisplayed()) {
						fc.utobj().clickElement(driver, pobj.assignToLeadOwnerSwitch);
					}
				} catch (Exception e) {
				}

				// TimeLess task
				try {
					if (fc.utobj().getElement(driver, pobj.startTaskImmediatelySwitch).isDisplayed()) {
						fc.utobj().clickElement(driver, pobj.timeLessTaskSwitch);
					}
				} catch (Exception e) {
				}

				fc.utobj().clickElement(driver, pobj.createTaskButton);
				fc.utobj().switchFrameToDefault(driver);

				fc.utobj().clickElement(driver, pobj.createWorkFlowButton);

				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}

		} else {
			fc.utobj().throwsSkipException("Was not able to Create WorkFlow For Lead Standard Type");
		}
	}

	// For Lead Event Based WorkFlow

	public void createWorkFlowLeadEventBased(WebDriver driver, Map<String, String> config, Map<String, String> dataSet,
			String workFlowName, String triggerType, String conditionType, String availableFields,
			String matchingCondition, String campaignName, String leadStatus, String templateNameForSendEmail,
			String taskSubject, String taskPriority, String taskStatus) throws Exception {

		String testCaseId = "TC_Create_WorkFlow_Lead_Event_Based";

		if (fc.utobj().validate(testCaseId)) {

			try {

				CRMWorkflowsPage pobj = new CRMWorkflowsPage(driver);
				fc.utobj().clickElement(driver, pobj.workflowsLink);
				fc.utobj().clickElement(driver, pobj.createWorkFLowLink);
				fc.utobj().sendKeys(driver, pobj.workFlowName, workFlowName);
				fc.utobj().clickElement(driver, pobj.recordTypeLead);
				fc.utobj().clickElement(driver, pobj.workflowTypeEventBased);

				fc.utobj().clickElement(driver, pobj.nextButton);

				// 1. :: Trigger :: When do you want to execute the workflow::

				// Fill out a Webform (Existing Leads)
				if (triggerType.equalsIgnoreCase("Fill out a Webform")) {

					fc.utobj().clickElement(driver, pobj.selectTriggerEventBased);
					fc.utobj().sendKeys(driver, fc.utobj().getElement(driver, pobj.selectTriggerEventBased)
							.findElement(By.xpath("./div/div/input")), triggerType);
					fc.utobj().clickElement(driver,
							fc.utobj().getElement(driver, pobj.selectTriggerEventBased).findElement(
									By.xpath("./div/ul/li/label/span[contains(text () , '" + triggerType + "')]")));

					String formName = dataSet.get("formName");

					fc.utobj().clickElement(driver, pobj.selectWebFormName);
					fc.utobj().sendKeys(driver, fc.utobj().getElement(driver, pobj.selectWebFormName)
							.findElement(By.xpath("/div/div/input")), formName);
					fc.utobj().clickElement(driver, fc.utobj().getElement(driver, pobj.selectWebFormName)
							.findElement(By.xpath("./div/ul/li/label/span[contains(text () , '" + formName + "')]")));
					fc.utobj().clickElement(driver, pobj.selectWebFormName);

				} else if (triggerType.equalsIgnoreCase("Status Is Changed")) {

					fc.utobj().clickElement(driver, pobj.selectTriggerEventBased);
					fc.utobj().sendKeys(driver, fc.utobj().getElement(driver, pobj.selectTriggerEventBased)
							.findElement(By.xpath("./div/div/input")), triggerType);
					fc.utobj().clickElement(driver,
							fc.utobj().getElement(driver, pobj.selectTriggerEventBased).findElement(
									By.xpath("./div/ul/li/label/span[contains(text () , '" + triggerType + "')]")));

					String statusIsChanged = dataSet.get("leadStatusChanged");

					fc.utobj().clickElement(driver, pobj.selectStatusEventBased);
					fc.utobj().sendKeys(driver, fc.utobj().getElement(driver, pobj.selectStatusEventBased)
							.findElement(By.xpath("./div/div/input")), statusIsChanged);
					fc.utobj().clickElement(driver,
							fc.utobj().getElement(driver, pobj.selectStatusEventBased).findElement(
									By.xpath("./div/ul/li/label/span[contains(text () , '" + statusIsChanged + "')]")));
					fc.utobj().clickElement(driver, pobj.selectStatusEventBased);
				}

				fc.utobj().clickElement(driver, pobj.nextButtonTrigger);

				// 2. ::Condition ::

				if (conditionType.equalsIgnoreCase("All Leads")) {

					fc.utobj().clickElement(driver, pobj.allLeads);
					fc.utobj().clickElement(driver, pobj.nextBtnCondition);

				} else if (conditionType.equalsIgnoreCase("Leads Matching The Conditions")) {

					fc.utobj().clickElement(driver, pobj.leadsMatchingCondition);

					if (availableFields.equalsIgnoreCase("Birthdate")) {

						// for unique condition
						fc.utobj().clickElement(driver, pobj.availabelFieldsSelect);
						fc.utobj().sendKeys(driver,
								driver.findElement(By.xpath(".//*[@id='fc-drop-parentselectedColList']/div/div/input")),
								"First Name");
						fc.utobj().clickElement(driver, driver.findElement(By.xpath(
								".//*[@id='fc-drop-parentselectedColList']/div/ul/li/label/span[.='First Name']")));
						fc.utobj().clickElement(driver, pobj.availabelFieldsSelect);

						// Equals Test Data
						fc.utobj().selectDropDown(driver, pobj.firstNameSelectMatch, "Equals");
						fc.utobj().sendKeys(driver, pobj.firstNameField, dataSet.get("leadFirstName"));

						fc.utobj().clickElement(driver, pobj.availabelFieldsSelect);
						fc.utobj().sendKeys(driver,
								driver.findElement(By.xpath(".//*[@id='fc-drop-parentselectedColList']/div/div/input")),
								availableFields);

						fc.utobj().clickElement(driver,
								driver.findElement(
										By.xpath(".//*[@id='fc-drop-parentselectedColList']/div/ul/li/label/span[.='"
												+ availableFields + "']")));
						fc.utobj().clickElement(driver, pobj.availabelFieldsSelect);

						if (matchingCondition.equalsIgnoreCase("Is Between")) {

							fc.utobj().selectDropDown(driver, pobj.selectBirthDateMatch, "Is Between");
							fc.utobj().sendKeys(driver, pobj.birthdayFromDate, dataSet.get("fromDate"));
							fc.utobj().sendKeys(driver, pobj.birthdayToDate, dataSet.get("toDate"));
							fc.utobj().clickElement(driver, pobj.selectBirthDateMatch);
						}
					} else if (availableFields.equalsIgnoreCase("Email")) {

						// for unique condition
						fc.utobj().clickElement(driver, pobj.availabelFieldsSelect);
						fc.utobj().sendKeys(driver,
								driver.findElement(By.xpath(".//*[@id='fc-drop-parentselectedColList']/div/div/input")),
								"First Name");
						fc.utobj().clickElement(driver, driver.findElement(By.xpath(
								".//*[@id='fc-drop-parentselectedColList']/div/ul/li/label/span[.='First Name']")));
						fc.utobj().clickElement(driver, pobj.availabelFieldsSelect);

						// Equals Test Data
						fc.utobj().selectDropDown(driver, pobj.firstNameSelectMatch, "Equals");
						fc.utobj().sendKeys(driver, pobj.firstNameField, dataSet.get("leadFirstName"));

						fc.utobj().clickElement(driver, pobj.availabelFieldsSelect);
						fc.utobj().sendKeys(driver,
								driver.findElement(By.xpath(".//*[@id='fc-drop-parentselectedColList']/div/div/input")),
								availableFields);

						fc.utobj().clickElement(driver,
								driver.findElement(
										By.xpath(".//*[@id='fc-drop-parentselectedColList']/div/ul/li/label/span[.='"
												+ availableFields + "']")));
						fc.utobj().clickElement(driver, pobj.availabelFieldsSelect);

						if (matchingCondition.equalsIgnoreCase("Is Not Empty")) {

							fc.utobj().selectDropDown(driver, pobj.selectLeadInfoEmailId, matchingCondition);
						}

					} else if (availableFields.equalsIgnoreCase("Country")) {

						// for unique condition
						fc.utobj().clickElement(driver, pobj.availabelFieldsSelect);
						fc.utobj().sendKeys(driver,
								driver.findElement(By.xpath(".//*[@id='fc-drop-parentselectedColList']/div/div/input")),
								"First Name");
						fc.utobj().clickElement(driver, driver.findElement(By.xpath(
								".//*[@id='fc-drop-parentselectedColList']/div/ul/li/label/span[.='First Name']")));
						fc.utobj().clickElement(driver, pobj.availabelFieldsSelect);

						// Equals Test Data
						fc.utobj().selectDropDown(driver, pobj.firstNameSelectMatch, "Equals");
						fc.utobj().sendKeys(driver, pobj.firstNameField, dataSet.get("leadFirstName"));

						fc.utobj().clickElement(driver, pobj.availabelFieldsSelect);
						fc.utobj().sendKeys(driver,
								driver.findElement(By.xpath(".//*[@id='fc-drop-parentselectedColList']/div/div/input")),
								availableFields);

						fc.utobj().clickElement(driver,
								driver.findElement(
										By.xpath(".//*[@id='fc-drop-parentselectedColList']/div/ul/li/label/span[.='"
												+ availableFields + "']")));
						fc.utobj().clickElement(driver, pobj.availabelFieldsSelect);

						if (matchingCondition.equalsIgnoreCase("In")) {

							fc.utobj().selectDropDown(driver, pobj.countryMatch, "In");
							fc.utobj().clickElement(driver, pobj.countrySelect);
							fc.utobj().clickElement(driver, fc.utobj().getElement(driver, pobj.countrySelect)
									.findElement(By.xpath("./div/ul/li/label/span[.='Select All']")));
							fc.utobj().clickElement(driver, fc.utobj().getElement(driver, pobj.countrySelect)
									.findElement(By.xpath("./div/ul/li/label/span[.='Select All']")));

							fc.utobj().sendKeys(driver, fc.utobj().getElement(driver, pobj.countrySelect)
									.findElement(By.xpath("./div/div/input")), dataSet.get("countryIn"));
							fc.utobj().clickElement(driver,
									fc.utobj().getElement(driver, pobj.countrySelect).findElement(
											By.xpath("./div/ul/li/label/span[.='" + dataSet.get("countryIn") + "']")));
							fc.utobj().clickElement(driver, pobj.countrySelect);
						}
					} else if (availableFields.equalsIgnoreCase("Status")) {

						// for unique condition
						fc.utobj().clickElement(driver, pobj.availabelFieldsSelect);
						fc.utobj().sendKeys(driver,
								driver.findElement(By.xpath(".//*[@id='fc-drop-parentselectedColList']/div/div/input")),
								"First Name");
						fc.utobj().clickElement(driver, driver.findElement(By.xpath(
								".//*[@id='fc-drop-parentselectedColList']/div/ul/li/label/span[.='First Name']")));
						fc.utobj().clickElement(driver, pobj.availabelFieldsSelect);

						// Equals Test Data
						fc.utobj().selectDropDown(driver, pobj.firstNameSelectMatch, "Equals");
						fc.utobj().sendKeys(driver, pobj.firstNameField, dataSet.get("leadFirstName"));

						fc.utobj().clickElement(driver, pobj.availabelFieldsSelect);
						fc.utobj().sendKeys(driver,
								driver.findElement(By.xpath(".//*[@id='fc-drop-parentselectedColList']/div/div/input")),
								availableFields);

						fc.utobj().clickElement(driver,
								driver.findElement(
										By.xpath(".//*[@id='fc-drop-parentselectedColList']/div/ul/li/label/span[.='"
												+ availableFields + "']")));
						fc.utobj().clickElement(driver, pobj.availabelFieldsSelect);

						if (matchingCondition.equalsIgnoreCase("In")) {

							fc.utobj().selectDropDown(driver, pobj.selectLeadStatusMatch, "In");

							fc.utobj().clickElement(driver, pobj.selectLeadStatus);
							fc.utobj().sendKeys(driver, fc.utobj().getElement(driver, pobj.selectLeadStatus)
									.findElement(By.xpath("./div/div/input")), dataSet.get("selectLeadStatus"));
							fc.utobj().clickElement(driver,
									fc.utobj().getElement(driver, pobj.selectLeadStatus).findElement(By.xpath(
											"./div/ul/li/label/span[.='" + dataSet.get("selectLeadStatus") + "']")));
							fc.utobj().clickElement(driver, pobj.selectLeadStatus);
						}

					} else if (availableFields.equalsIgnoreCase("Email Subscription Status")) {

						// for unique condition
						fc.utobj().clickElement(driver, pobj.availabelFieldsSelect);
						fc.utobj().sendKeys(driver,
								driver.findElement(By.xpath(".//*[@id='fc-drop-parentselectedColList']/div/div/input")),
								"First Name");
						fc.utobj().clickElement(driver, driver.findElement(By.xpath(
								".//*[@id='fc-drop-parentselectedColList']/div/ul/li/label/span[.='First Name']")));
						fc.utobj().clickElement(driver, pobj.availabelFieldsSelect);

						// Equals Test Data
						fc.utobj().selectDropDown(driver, pobj.firstNameSelectMatch, "Equals");
						fc.utobj().sendKeys(driver, pobj.firstNameField, dataSet.get("leadFirstName"));

						fc.utobj().clickElement(driver, pobj.availabelFieldsSelect);
						fc.utobj().sendKeys(driver,
								driver.findElement(By.xpath(".//*[@id='fc-drop-parentselectedColList']/div/div/input")),
								availableFields);

						fc.utobj().clickElement(driver,
								driver.findElement(
										By.xpath(".//*[@id='fc-drop-parentselectedColList']/div/ul/li/label/span[.='"
												+ availableFields + "']")));
						fc.utobj().clickElement(driver, pobj.availabelFieldsSelect);

						if (matchingCondition.equalsIgnoreCase("In")) {

							fc.utobj().selectDropDown(driver, pobj.selectEmailSubScriptionMatch, "In");

							fc.utobj().clickElement(driver, pobj.selectEmailSubscribe);
							fc.utobj()
									.sendKeys(driver,
											fc.utobj().getElement(driver, pobj.selectEmailSubscribe)
													.findElement(By.xpath("./div/div/input")),
											dataSet.get("emailStatus"));
							fc.utobj().clickElement(driver,
									fc.utobj().getElement(driver, pobj.selectEmailSubscribe)
											.findElement(By.xpath("./div/ul/li/label/span[contains(text () ,'"
													+ dataSet.get("emailStatus") + "')]")));
							fc.utobj().clickElement(driver, pobj.selectEmailSubscribe);
						}
					}

					fc.utobj().clickElement(driver, pobj.nextBtnCondition);
				}

				// 3. :: Action ::

				// 1.send Campaign
				fc.utobj().clickElement(driver, pobj.actionBtn);
				fc.utobj().clickElement(driver, pobj.sendCampaign);

				if (fc.utobj().getElement(driver, pobj.selectLocationAtCampaign).isDisplayed()) {
					fc.utobj().clickElement(driver, pobj.masterCampaign);
				}

				fc.utobj().sendKeys(driver, pobj.selectCampaign, campaignName);
				fc.utobj().clickElement(driver, pobj.saveBtn);

				// 2.change Status
				fc.utobj().clickElement(driver, pobj.actionBtn);
				fc.utobj().clickElement(driver, pobj.changesStatus);
				fc.utobj().selectDropDown(driver, pobj.selectStatus, leadStatus);
				fc.utobj().clickElement(driver, pobj.saveBtn);

				// 3.send Email
				fc.utobj().clickElement(driver, pobj.actionBtn);
				fc.utobj().clickElement(driver, pobj.sendEmail);

				fc.commonMethods().switch_frameClass_newLayoutcboxIframe(driver);

				// from/Reply :: Lead Owner ID
				// To :: Lead Email

				fc.utobj().selectDropDown(driver, pobj.selectTemplate, templateNameForSendEmail);
				fc.utobj().clickElement(driver, pobj.associateTemplateWithEmailButton);
				fc.utobj().switchFrameToDefault(driver);

				// 4.create Task
				fc.utobj().clickElement(driver, pobj.actionBtn);
				fc.utobj().clickElement(driver, pobj.createTask);

				fc.commonMethods().switch_frameClass_newLayoutcboxIframe(driver);

				fc.utobj().sendKeys(driver, pobj.subject, taskSubject);
				fc.utobj().selectDropDown(driver, pobj.priority, taskPriority);
				// fc.utobj().selectDropDown(driver, pobj.taskStatus,
				// taskStatus);

				String textVal = pobj.taskStatus
						.findElement(By.xpath("./option[contains(text () , '" + taskStatus + "')]"))
						.getAttribute("value").trim();
				fc.utobj().selectDropDownByValue(driver, pobj.taskStatus, textVal);

				// assign Task To Lead Owner
				try {
					if (fc.utobj().getElement(driver, pobj.customOwner).isDisplayed()) {
						fc.utobj().clickElement(driver, pobj.assignToLeadOwnerSwitch);
					}
				} catch (Exception e) {
				}

				// TimeLess task
				try {
					if (fc.utobj().getElement(driver, pobj.startTaskImmediatelySwitch).isDisplayed()) {
						fc.utobj().clickElement(driver, pobj.timeLessTaskSwitch);
					}
				} catch (Exception e) {
				}

				fc.utobj().clickElement(driver, pobj.createTaskButton);
				fc.utobj().switchFrameToDefault(driver);

				fc.utobj().clickElement(driver, pobj.createWorkFlowButton);

				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}

		} else {
			fc.utobj().throwsSkipException("Was not able to Create WorkFlow For Lead Standard Type");
		}
	}

	// For Contact Event Based WorkFlow

	public void createWorkFlowContactEventBased(WebDriver driver, Map<String, String> config,
			Map<String, String> dataSet, String workFlowName, String triggerType, String conditionType,
			String availableFields, String matchingCondition, String campaignName, String contactStatus,
			String templateNameForSendEmail, String taskSubject, String taskPriority, String taskStatus)
			throws Exception {

		String testCaseId = "TC_Create_WorkFlow_Contact_Date_Based";

		if (fc.utobj().validate(testCaseId)) {

			try {

				CRMWorkflowsPage pobj = new CRMWorkflowsPage(driver);
				fc.utobj().clickElement(driver, pobj.workflowsLink);
				fc.utobj().clickElement(driver, pobj.createWorkFLowLink);
				fc.utobj().sendKeys(driver, pobj.workFlowName, workFlowName);
				fc.utobj().clickElement(driver, pobj.recordTypeContact);
				fc.utobj().clickElement(driver, pobj.workflowTypeEventBased);
				fc.utobj().clickElement(driver, pobj.nextButton);

				// 1. :: Trigger :: When Contact is Added ::

				if (triggerType.equalsIgnoreCase("Fill out a Webform")) {

					fc.utobj().clickElement(driver, pobj.selectTriggerEventBased);
					fc.utobj().sendKeys(driver, fc.utobj().getElement(driver, pobj.selectTriggerEventBased)
							.findElement(By.xpath("./div/div/input")), triggerType);
					fc.utobj().clickElement(driver,
							fc.utobj().getElement(driver, pobj.selectTriggerEventBased).findElement(
									By.xpath("./div/ul/li/label/span[contains(text () , '" + triggerType + "')]")));

					String formName = dataSet.get("formName");

					fc.utobj().clickElement(driver, pobj.selectWebFormName);
					fc.utobj().sendKeys(driver, fc.utobj().getElement(driver, pobj.selectWebFormName)
							.findElement(By.xpath("/div/div/input")), formName);
					fc.utobj().clickElement(driver, fc.utobj().getElement(driver, pobj.selectWebFormName)
							.findElement(By.xpath("./div/ul/li/label/span[contains(text () , '" + formName + "')]")));
					fc.utobj().clickElement(driver, pobj.selectWebFormName);
				} else if (triggerType.equalsIgnoreCase("Status Is Changed")) {

					fc.utobj().clickElement(driver, pobj.selectTriggerEventBased);
					fc.utobj().sendKeys(driver, fc.utobj().getElement(driver, pobj.selectTriggerEventBased)
							.findElement(By.xpath("./div/div/input")), triggerType);
					fc.utobj().clickElement(driver,
							fc.utobj().getElement(driver, pobj.selectTriggerEventBased).findElement(
									By.xpath("./div/ul/li/label/span[contains(text () , '" + triggerType + "')]")));

					String statusIsChanged = dataSet.get("contactStatusChanged");

					fc.utobj().clickElement(driver, pobj.selectStatusEventBased);
					fc.utobj().sendKeys(driver, fc.utobj().getElement(driver, pobj.selectStatusEventBased)
							.findElement(By.xpath("./div/div/input")), statusIsChanged);
					fc.utobj().clickElement(driver,
							fc.utobj().getElement(driver, pobj.selectStatusEventBased).findElement(
									By.xpath("./div/ul/li/label/span[contains(text () , '" + statusIsChanged + "')]")));
					fc.utobj().clickElement(driver, pobj.selectStatusEventBased);
				}

				fc.utobj().clickElement(driver, pobj.nextButtonTrigger);

				// 2. ::Condition ::

				if (conditionType.equalsIgnoreCase("All Contacts")) {

					fc.utobj().clickElement(driver, pobj.allLeads);
					fc.utobj().clickElement(driver, pobj.nextBtnCondition);

				} else if (conditionType.equalsIgnoreCase("Contacts Matching the Conditions")) {

					fc.utobj().clickElement(driver, pobj.leadsMatchingCondition);

					if (availableFields.equalsIgnoreCase("Birthdate")) {

						// select FirstName
						fc.utobj().clickElement(driver, pobj.availabelFieldsSelect);
						fc.utobj().sendKeys(driver,
								driver.findElement(By.xpath(".//*[@id='fc-drop-parentselectedColList']/div/div/input")),
								"First Name");

						fc.utobj().clickElement(driver, driver.findElement(By.xpath(
								".//*[@id='fc-drop-parentselectedColList']/div/ul/li/label/span[.='First Name']")));
						fc.utobj().clickElement(driver, pobj.availabelFieldsSelect);

						// select First Name
						fc.utobj().selectDropDown(driver, pobj.firstNameSelectContactMatch, "Equals");
						fc.utobj().sendKeys(driver, pobj.contactFirstNameField, dataSet.get("contactFirstName"));

						fc.utobj().clickElement(driver, pobj.availabelFieldsSelect);
						fc.utobj().sendKeys(driver,
								driver.findElement(By.xpath(".//*[@id='fc-drop-parentselectedColList']/div/div/input")),
								availableFields);

						fc.utobj().clickElement(driver,
								driver.findElement(
										By.xpath(".//*[@id='fc-drop-parentselectedColList']/div/ul/li/label/span[.='"
												+ availableFields + "']")));
						fc.utobj().clickElement(driver, pobj.availabelFieldsSelect);

						if (matchingCondition.equalsIgnoreCase("Is Between")) {

							fc.utobj().selectDropDown(driver, pobj.selectBirthDateContact, "Is Between");
							fc.utobj().sendKeys(driver, pobj.selectFromDateContact, dataSet.get("fromDate"));
							fc.utobj().sendKeys(driver, pobj.selectToDateContact, dataSet.get("toDate"));
							fc.utobj().clickElement(driver, pobj.selectBirthDateContact);
						}
					} else if (availableFields.equalsIgnoreCase("Email")) {

						// select FirstName
						fc.utobj().clickElement(driver, pobj.availabelFieldsSelect);
						fc.utobj().sendKeys(driver,
								driver.findElement(By.xpath(".//*[@id='fc-drop-parentselectedColList']/div/div/input")),
								"First Name");

						fc.utobj().clickElement(driver, driver.findElement(By.xpath(
								".//*[@id='fc-drop-parentselectedColList']/div/ul/li/label/span[.='First Name']")));
						fc.utobj().clickElement(driver, pobj.availabelFieldsSelect);

						// select First Name
						fc.utobj().selectDropDown(driver, pobj.firstNameSelectContactMatch, "Equals");
						fc.utobj().sendKeys(driver, pobj.contactFirstNameField, dataSet.get("contactFirstName"));

						fc.utobj().clickElement(driver, pobj.availabelFieldsSelect);
						fc.utobj().sendKeys(driver,
								driver.findElement(By.xpath(".//*[@id='fc-drop-parentselectedColList']/div/div/input")),
								availableFields);

						fc.utobj().clickElement(driver,
								driver.findElement(
										By.xpath(".//*[@id='fc-drop-parentselectedColList']/div/ul/li/label/span[.='"
												+ availableFields + "']")));
						fc.utobj().clickElement(driver, pobj.availabelFieldsSelect);

						if (matchingCondition.equalsIgnoreCase("Is Not Empty")) {

							fc.utobj().selectDropDown(driver, pobj.selectEmailIdMatchContact, matchingCondition);
						}
					} else if (availableFields.equalsIgnoreCase("Country")) {

						// select FirstName
						fc.utobj().clickElement(driver, pobj.availabelFieldsSelect);
						fc.utobj().sendKeys(driver,
								driver.findElement(By.xpath(".//*[@id='fc-drop-parentselectedColList']/div/div/input")),
								"First Name");

						fc.utobj().clickElement(driver, driver.findElement(By.xpath(
								".//*[@id='fc-drop-parentselectedColList']/div/ul/li/label/span[.='First Name']")));
						fc.utobj().clickElement(driver, pobj.availabelFieldsSelect);

						// select First Name
						fc.utobj().selectDropDown(driver, pobj.firstNameSelectContactMatch, "Equals");
						fc.utobj().sendKeys(driver, pobj.contactFirstNameField, dataSet.get("contactFirstName"));

						fc.utobj().clickElement(driver, pobj.availabelFieldsSelect);
						fc.utobj().sendKeys(driver, fc.utobj().getElement(driver, pobj.availabelFieldsSelect)
								.findElement(By.xpath("./div/div/input")), availableFields);
						fc.utobj().clickElement(driver,
								fc.utobj().getElement(driver, pobj.availabelFieldsSelect).findElement(By.xpath(
										"./div/ul/li/label/input[contains(@value , 'cmContactDetails')]/following-sibling::span[.='Country']")));
						fc.utobj().clickElement(driver, pobj.availabelFieldsSelect);

						if (matchingCondition.equalsIgnoreCase("In")) {

							fc.utobj().selectDropDown(driver, pobj.selectCountryMatchCaontact, "In");
							fc.utobj().clickElement(driver, pobj.selectCountryContact);
							fc.utobj().clickElement(driver,
									fc.utobj().getElement(driver, pobj.selectCountryContact)
											.findElement(By.xpath("./div/ul/li/label/span[.='Select All']")));
							fc.utobj().clickElement(driver,
									fc.utobj().getElement(driver, pobj.selectCountryContact)
											.findElement(By.xpath("./div/ul/li/label/span[.='Select All']")));

							fc.utobj()
									.sendKeys(driver,
											fc.utobj().getElement(driver, pobj.selectCountryContact)
													.findElement(By.xpath("./div/div/input")),
											dataSet.get("countryIn"));
							fc.utobj().clickElement(driver,
									fc.utobj().getElement(driver, pobj.selectCountryContact).findElement(
											By.xpath("./div/ul/li/label/span[.='" + dataSet.get("countryIn") + "']")));
							fc.utobj().clickElement(driver, pobj.selectCountryContact);
						}
					} else if (availableFields.equalsIgnoreCase("Status")) {

						// select FirstName
						fc.utobj().clickElement(driver, pobj.availabelFieldsSelect);
						fc.utobj().sendKeys(driver,
								driver.findElement(By.xpath(".//*[@id='fc-drop-parentselectedColList']/div/div/input")),
								"First Name");

						fc.utobj().clickElement(driver, driver.findElement(By.xpath(
								".//*[@id='fc-drop-parentselectedColList']/div/ul/li/label/span[.='First Name']")));
						fc.utobj().clickElement(driver, pobj.availabelFieldsSelect);

						// select First Name
						fc.utobj().selectDropDown(driver, pobj.firstNameSelectContactMatch, "Equals");
						fc.utobj().sendKeys(driver, pobj.contactFirstNameField, dataSet.get("contactFirstName"));

						fc.utobj().clickElement(driver, pobj.availabelFieldsSelect);
						fc.utobj().sendKeys(driver,
								driver.findElement(By.xpath(".//*[@id='fc-drop-parentselectedColList']/div/div/input")),
								availableFields);

						fc.utobj().clickElement(driver,
								driver.findElement(
										By.xpath(".//*[@id='fc-drop-parentselectedColList']/div/ul/li/label/span[.='"
												+ availableFields + "']")));
						fc.utobj().clickElement(driver, pobj.availabelFieldsSelect);

						if (matchingCondition.equalsIgnoreCase("In")) {

							fc.utobj().selectDropDown(driver, pobj.selectContactStatusMatch, "In");

							fc.utobj().clickElement(driver, pobj.selectContactStatus);
							fc.utobj().sendKeys(driver, fc.utobj().getElement(driver, pobj.selectContactStatus)
									.findElement(By.xpath("./div/div/input")), dataSet.get("selectContactStatus"));
							fc.utobj().clickElement(driver, fc.utobj()
									.getElement(driver, pobj.selectContactStatus).findElement(By.xpath(
											"./div/ul/li/label/span[.='" + dataSet.get("selectContactStatus") + "']")));
							fc.utobj().clickElement(driver, pobj.selectContactStatus);
						}

					} else if (availableFields.equalsIgnoreCase("Email Subscription Status")) {

						// select FirstName
						fc.utobj().clickElement(driver, pobj.availabelFieldsSelect);
						fc.utobj().sendKeys(driver,
								driver.findElement(By.xpath(".//*[@id='fc-drop-parentselectedColList']/div/div/input")),
								"First Name");

						fc.utobj().clickElement(driver, driver.findElement(By.xpath(
								".//*[@id='fc-drop-parentselectedColList']/div/ul/li/label/span[.='First Name']")));
						fc.utobj().clickElement(driver, pobj.availabelFieldsSelect);

						// select First Name
						fc.utobj().selectDropDown(driver, pobj.firstNameSelectContactMatch, "Equals");
						fc.utobj().sendKeys(driver, pobj.contactFirstNameField, dataSet.get("contactFirstName"));

						fc.utobj().clickElement(driver, pobj.availabelFieldsSelect);
						fc.utobj().sendKeys(driver,
								driver.findElement(By.xpath(".//*[@id='fc-drop-parentselectedColList']/div/div/input")),
								availableFields);

						fc.utobj().clickElement(driver,
								driver.findElement(
										By.xpath(".//*[@id='fc-drop-parentselectedColList']/div/ul/li/label/span[.='"
												+ availableFields + "']")));
						fc.utobj().clickElement(driver, pobj.availabelFieldsSelect);

						if (matchingCondition.equalsIgnoreCase("In")) {

							fc.utobj().selectDropDown(driver, pobj.selectEmailSubscribeContact, "In");

							fc.utobj().clickElement(driver, pobj.selectEmailSubscribeStatusContact);
							fc.utobj().sendKeys(driver,
									fc.utobj().getElement(driver, pobj.selectEmailSubscribeStatusContact)
											.findElement(By.xpath("./div/div/input")),
									dataSet.get("emailStatus"));
							fc.utobj().clickElement(driver,
									fc.utobj().getElement(driver, pobj.selectEmailSubscribeStatusContact)
											.findElement(By.xpath("./div/ul/li/label/span[contains(text () ,'"
													+ dataSet.get("emailStatus") + "')]")));
							fc.utobj().clickElement(driver, pobj.selectEmailSubscribeStatusContact);
						}
					} else if (availableFields.equalsIgnoreCase("Contact Type")) {

						// select FirstName
						fc.utobj().clickElement(driver, pobj.availabelFieldsSelect);
						fc.utobj().sendKeys(driver,
								driver.findElement(By.xpath(".//*[@id='fc-drop-parentselectedColList']/div/div/input")),
								"First Name");

						fc.utobj().clickElement(driver, driver.findElement(By.xpath(
								".//*[@id='fc-drop-parentselectedColList']/div/ul/li/label/span[.='First Name']")));
						fc.utobj().clickElement(driver, pobj.availabelFieldsSelect);

						// select First Name
						fc.utobj().selectDropDown(driver, pobj.firstNameSelectContactMatch, "Equals");
						fc.utobj().sendKeys(driver, pobj.contactFirstNameField, dataSet.get("contactFirstName"));

						fc.utobj().clickElement(driver, pobj.availabelFieldsSelect);
						fc.utobj().sendKeys(driver,
								driver.findElement(By.xpath(".//*[@id='fc-drop-parentselectedColList']/div/div/input")),
								availableFields);

						fc.utobj().clickElement(driver,
								driver.findElement(
										By.xpath(".//*[@id='fc-drop-parentselectedColList']/div/ul/li/label/span[.='"
												+ availableFields + "']")));
						fc.utobj().clickElement(driver, pobj.availabelFieldsSelect);

						if (matchingCondition.equalsIgnoreCase("In")) {

							fc.utobj().selectDropDown(driver, pobj.selectContactTypeMatch, "In");

							fc.utobj().clickElement(driver, pobj.selectContactType);
							fc.utobj().sendKeys(driver, fc.utobj().getElement(driver, pobj.selectContactType)
									.findElement(By.xpath("./div/div/input")), dataSet.get("selectcontactType"));
							fc.utobj().clickElement(driver,
									fc.utobj().getElement(driver, pobj.selectContactType).findElement(By.xpath(
											"./div/ul/li/label/span[.='" + dataSet.get("selectcontactType") + "']")));
							fc.utobj().clickElement(driver, pobj.selectContactType);
						}
					}

					fc.utobj().clickElement(driver, pobj.nextBtnCondition);
				}

				// 3. :: Action ::

				// 1.send Campaign
				fc.utobj().clickElement(driver, pobj.actionBtn);
				fc.utobj().clickElement(driver, pobj.sendCampaign);

				if (fc.utobj().getElement(driver, pobj.selectLocationAtCampaign).isDisplayed()) {
					fc.utobj().clickElement(driver, pobj.masterCampaign);
				}

				fc.utobj().sendKeys(driver, pobj.selectCampaign, campaignName);
				fc.utobj().clickElement(driver, pobj.saveBtn);

				// 2.change Status
				fc.utobj().clickElement(driver, pobj.actionBtn);
				fc.utobj().clickElement(driver, pobj.changesStatus);
				fc.utobj().selectDropDown(driver, pobj.selectStatus, contactStatus);
				fc.utobj().clickElement(driver, pobj.saveBtn);

				// 3.send Email
				fc.utobj().clickElement(driver, pobj.actionBtn);
				fc.utobj().clickElement(driver, pobj.sendEmail);

				fc.commonMethods().switch_frameClass_newLayoutcboxIframe(driver);

				// from/Reply :: Lead Owner ID
				// To :: Lead Email

				fc.utobj().selectDropDown(driver, pobj.selectTemplate, templateNameForSendEmail);
				fc.utobj().clickElement(driver, pobj.associateTemplateWithEmailButton);
				fc.utobj().switchFrameToDefault(driver);

				// 4.create Task
				fc.utobj().clickElement(driver, pobj.actionBtn);
				fc.utobj().clickElement(driver, pobj.createTask);

				fc.commonMethods().switch_frameClass_newLayoutcboxIframe(driver);

				fc.utobj().sendKeys(driver, pobj.subject, taskSubject);
				fc.utobj().selectDropDown(driver, pobj.priority, taskPriority);
				// fc.utobj().selectDropDown(driver, pobj.taskStatus,
				// taskStatus);

				String textVal = pobj.taskStatus
						.findElement(By.xpath("./option[contains(text () , '" + taskStatus + "')]"))
						.getAttribute("value").trim();
				fc.utobj().selectDropDownByValue(driver, pobj.taskStatus, textVal);

				// assign Task To Lead Owner
				try {
					if (fc.utobj().getElement(driver, pobj.customOwner).isDisplayed()) {
						fc.utobj().clickElement(driver, pobj.assignToLeadOwnerSwitch);
					}
				} catch (Exception e) {
				}

				// TimeLess task
				try {
					if (fc.utobj().getElement(driver, pobj.startTaskImmediatelySwitch).isDisplayed()) {
						fc.utobj().clickElement(driver, pobj.timeLessTaskSwitch);
					}
				} catch (Exception e) {
				}

				fc.utobj().clickElement(driver, pobj.createTaskButton);
				fc.utobj().switchFrameToDefault(driver);

				fc.utobj().clickElement(driver, pobj.createWorkFlowButton);

				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException("Was not able to Create WorkFlow For Contact Standard Type");
		}
	}

	public void searchWorkFLowByFilter(WebDriver driver, String workFlowName) throws Exception {
		CRMWorkflowsPage pobj = new CRMWorkflowsPage(driver);
		fc.utobj().clickElement(driver, pobj.showFilter);
		fc.utobj().sendKeys(driver, pobj.searchWorkFlow, workFlowName);
		fc.utobj().clickElement(driver, pobj.applyFilter);
		fc.utobj().clickElement(driver, pobj.hideFilter);
	}

}
