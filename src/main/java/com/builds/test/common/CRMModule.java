package com.builds.test.common;

import com.builds.test.crm.CRM_Common;

public class CRMModule {
	private CRM_Common crmobj = null;

	public CRM_Common crm_common() {
		if (crmobj == null) {
			crmobj = new CRM_Common();
		}
		return crmobj;
	}
}
