package com.builds.test.common;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.builds.test.admin.AdminHiddenLinksConfigureNewHierarchyLevelPageTest;
import com.builds.utilities.CodeUtility;
import com.builds.utilities.FranconnectUtil;

public class CommonDataAddition {
	FranconnectUtil fc = new FranconnectUtil();

	private String regionName = "AutRegion2018";
	private String divisionName = "AutDivision2018";

	private String regionalUserFirstName = "Autreg7777";
	private String regionalUserLastName = "Autreg777";
	private String regionalUserId = "autRegUser5478";
	private String regionalUserPwd = "autRegUser0111";
	
	
	private String locationName = "AutLocation2018";
	private String franchiseUserFirstName = "Autfrancf2018";
	private String franchiseUserLastName = "Autfrancl2018";


	public Region createDefaultRegion(WebDriver driver) throws Exception
	{
		boolean isRegionAdded = verifyRegionBasedOnLocationIsPresentThroughWebServices();
		
		Region region = null;
		if(isRegionAdded==false)
		{
			region = createRegionThroughBuild(driver);
		}
		return region;
	}
	
	public Location createDefaultLocation(WebDriver driver) throws Exception
	{
		boolean isLocationAdded = verifyLocationIsPresentThroughWebServices();
		
		Location location = null;
		if(isLocationAdded==false)
		{
			location = createLocationThroughWebService(driver);
		}
		return location; 
	}
	
	public RegionalUser createDefaultRegionalUserThroughWebService() throws Exception
	{
		boolean isRegionalUserAdded = verifyRegionalUserIsPresentThroughWebServices();
		
		RegionalUser ru = null;
		if(isRegionalUserAdded==false)
		{
			ru = addRegionalUserThroughWebService();
		}
		
		return ru;
	}

	public void createDefaultFranchiseeUserThroughWebService() throws Exception
	{
		boolean isFranchiseeUserAdded = verifyFranchiseUserIsPresentThroughWebServices();
		
		if(isFranchiseeUserAdded==false)
		{
			
		}
	}
	
	private RegionalUser addRegionalUserThroughWebService() throws Exception {
		RegionalUser ru = new RegionalUser();
		ru.setUserName(regionalUserId);
		ru.setPassword(regionalUserPwd);
		ru.setUserLanguage("English");
		ru.setTimezone("Asia/Kolkata");
		ru.setUserType("Normal");
		ru.setArea_Region(regionName);
		ru.setRole("Default Regional Role");
		ru.setFirstName(regionalUserFirstName);
		ru.setLastName(regionalUserLastName);
		ru.setAuditor("Y");
		ru.setExpiryDays("NA");
		ru.setAddress("XYZ Street");
		ru.setCity("NYC");
		ru.setState("New York");
		ru.setCountry("USA");
		ru.setZipcode("11002");
		ru.setPhone1("9869869869");
		ru.setPhoneExt1("99");
		ru.setPhone2("1122554422");
		ru.setPhoneExt2("12");
		ru.setFax("8889995555");
		ru.setMobile("9988668899");
		ru.setEmail("automation@franconnect.com");
		ru.setSendNotification("Y");
		
		String module = "admin";
		String subModule = "user";
		String isISODate = "no";

		String xmlString = RegionalUserXML(ru);
		WebService service = new WebService();
		String responseMessage = service.create(module, subModule, xmlString, isISODate);

		System.out.println(responseMessage);

		CodeUtility cu = new CodeUtility();
		System.out.println(cu.getValFromXML(responseMessage, "firstname"));
		
		return ru;
	}
	

	private void addDivision(WebDriver driver) throws Exception {
		AdminHiddenLinksConfigureNewHierarchyLevelPageTest hiddenLink = new AdminHiddenLinksConfigureNewHierarchyLevelPageTest();
		hiddenLink.ConfigureNewHierarchyLevel(driver);

		fc.utobj().clickElement(driver, driver.findElement(By.xpath(".//a[@qat_adminlink='Manage Division']")));

		List<WebElement> divisionOptions = driver.findElements(By.xpath(".//select[@id='divisionKey']/option"));

		boolean divisionExist = false;
		for (WebElement d : divisionOptions) {
			if (d.getText().equalsIgnoreCase(divisionName)) {
				divisionExist = true;
			}
		}
		if (divisionExist == false) {
			fc.utobj().clickElement(driver,
					driver.findElement(By.xpath(".//input[contains(@onclick,'addDivision()')]")));
			fc.utobj().sendKeys(driver, driver.findElement(By.name("divisionName")), divisionName);

			try {
				fc.utobj().acceptAlertBox(driver);
				fc.utobj().clickElement(driver, driver.findElement(By.xpath(".//input[@value='Submit']")));
			} catch (Exception e) {

			}

			fc.utobj().clickElement(driver, driver.findElement(By.name("button")));
		}

		fc.utobj().selectValFromMultiSelect(driver, driver.findElement(By.id("ms-parentdivisionKey")), divisionName);

		if (fc.utobj().assertPageSource(driver, divisionName) == false) {
			fc.utobj().throwsException("Division is Not getting added.");
		}
	}

	
	private Region createRegionThroughBuild(WebDriver driver) throws Exception
	{
		Region region = new Region();
		
		region.setCategory("Domestic");
		region.setAreaRegionName(regionName);
		region.setGroupBy("Zip Codes");
		region.setCountries("USA");
		region.setZipCodes("201201");
		
		fc.commonMethods().getModules().openAdminPage(driver);
		fc.adminpage().openManageAreaRegion(driver);

		List<WebElement> listOfOption = driver
				.findElements(By.xpath(".//option[contains(text(),'" + region.getAreaRegionName() + "')]"));

		if (listOfOption.size() < 1) {

			fc.utobj().clickElement(driver, ".//input[contains(@onclick,'addRegionForm')]");
			fc.utobj().selectDropDown(driver, driver.findElement(By.id("category")), region.getCategory());
			fc.utobj().sendKeys(driver, driver.findElement(By.name("regioname")), region.getAreaRegionName());
			fc.utobj().selectDropDown(driver, driver.findElement(By.name("groupBy")), region.getGroupBy());
			fc.utobj().clickElement(driver, ".//input[@name='country']/ancestor::td[1][contains(text(),'"
					+ region.getCountries() + "')]//input");
			fc.utobj().clickElement(driver, ".//input[@name='zipmode' and @value='1']");
			fc.utobj().sendKeys(driver, driver.findElement(By.name("ziplist")), region.getZipCodes());
			fc.utobj().clickElement(driver,
					driver.findElement(By.xpath(".//input[@name='button' and @value='Submit']")));

			boolean alertPresent = false;
			try {
				driver.switchTo().alert().accept();
				alertPresent = true;
			} catch (Exception e) {

			}

			if (alertPresent == false) {
				try {
					fc.utobj().clickElement(driver, driver.findElement(By.xpath(".//input[@value='Proceed']")));
				} catch (Exception e2) {

				}
				fc.utobj().clickElement(driver,
						driver.findElement(By.xpath(".//input[@type='button' and @value='Next']")));
			}
		}
		return region;
	}
	
	private boolean verifyLocationIsPresentThroughWebServices() throws Exception {
		
		String module = "admin";
		String subModule = "franchisee";
		String isISODate = "no";

		String xmlString = "<fcRequest><filter><franchiseeName>" + locationName + "</franchiseeName><areaID>"
				+ regionName + "</areaID></filter></fcRequest>";

		
		WebService service = new WebService();
		String responseMessage = service.retrieve(module, subModule, xmlString, isISODate);

		System.out.println(responseMessage);

		CodeUtility cu = new CodeUtility();
		System.out.println(cu.getValFromXML(responseMessage, "franchiseeName"));

		if (cu.getValFromXML(responseMessage, "franchiseeName").equalsIgnoreCase(locationName)) {
			return true;
		} else {
			return false;
		}
	}


	private boolean verifyRegionBasedOnLocationIsPresentThroughWebServices() throws Exception {
		
		String module = "admin";
		String subModule = "franchisee";
		String isISODate = "no";

		String xmlString = "<fcRequest><filter><franchiseeName>" + locationName + "</franchiseeName><areaID>"
				+ regionName + "</areaID></filter></fcRequest>";

		
		WebService service = new WebService();
		String responseMessage = service.retrieve(module, subModule, xmlString, isISODate);

		System.out.println(responseMessage);

		CodeUtility cu = new CodeUtility();
		System.out.println(cu.getValFromXML(responseMessage, "areaID"));

		if (cu.getValFromXML(responseMessage, "areaID").equalsIgnoreCase(regionName)) {
			return true;
		} else {
			return false;
		}
	}

	private boolean verifyFranchiseUserIsPresentThroughWebServices() throws Exception {
		String module = "admin";
		String subModule = "franchisee";
		String isISODate = "no";

		String xmlString = "<fcRequest><filter><franchiseeName>" + locationName + "</franchiseeName><areaID>"
				+ regionName + "</areaID></filter></fcRequest>";

		WebService service = new WebService();
		String responseMessage = service.retrieve(module, subModule, xmlString, isISODate);

		System.out.println(responseMessage);

		CodeUtility cu = new CodeUtility();
		System.out.println(cu.getValFromXML(responseMessage, "firstname"));

		if (cu.getValFromXML(responseMessage, "firstname").equalsIgnoreCase(franchiseUserFirstName)) {
			return true;
		} else {
			return false;
		}
	}

	private boolean verifyRegionalUserIsPresentThroughWebServices() throws Exception {
		String module = "admin";
		String subModule = "User";
		String isISODate = "no";

		String xmlString = "<fcRequest><filter><firstname>" + regionalUserFirstName + "</firstname><lastname>"
				+ regionalUserLastName + "</lastname></filter></fcRequest>";

		WebService service = new WebService();
		String responseMessage = service.retrieve(module, subModule, xmlString, isISODate);

		System.out.println("Response : " + responseMessage);

		CodeUtility cu = new CodeUtility();
		System.out.println(cu.getValFromXML(responseMessage, "franchiseeName"));

		if (cu.getValFromXML(responseMessage, "firstname").equalsIgnoreCase(regionalUserFirstName)
				&& cu.getValFromXML(responseMessage, "lastname").equalsIgnoreCase(regionalUserLastName)) {
			return true;
		} else {
			return false;
		}
	}

	
	
	/*
	 * This method will Add Location
	 */
	private Location createLocationThroughWebService(WebDriver driver) throws Exception {
		
		Location location = new Location();
		
		location.setCorporateLocation("No");
		location.setFranchiseID(locationName);
		location.setCenterName("NYC Center");
		location.setAreaRegion(regionName);
		location.setRoyaltyReportingStartDate("2017-12-01");
		location.setOpeningDate("2020-12-01");
		location.setStoreType("Default");
		location.setStreetAddress("XYZ Street");
		location.setAddress2("Street Address 2");
		location.setCity("NYC");
		location.setCountry("USA");
		location.setState("New York");
		location.setZipCode("10001");
		location.setPhone("8889998889");
		location.setPhoneExtension("89");
		location.setFax("8889998889");
		location.setMobile("9595959595");
		location.setEmail("commonautomation@franconnect.com");
		location.setTitle("Mr.");
		location.setFirstName("Contactfn");
		location.setLastName("Contactln");
		location.setPhone("9898989898");
		location.setPhoneExtension("98");
		location.setFax("0808080808");
		location.setMobile("9695969596");
		location.setEmail("location@franconnect.com");
		location.setOwnerTypeIndividualEntity("Individual");
		location.setNewOwner("New");
		location.setTitle("Mr.");
		location.setOwnerFirstName("Automation");
		location.setOwnerLastName("Owner");

		franchiseXML(location);

		String module = "admin";
		String subModule = "franchisee";
		String isISODate = "no";

		String xmlString = franchiseXML(location);
		WebService service = new WebService();
		String responseMessage = service.create(module, subModule, xmlString, isISODate);

		System.out.println(responseMessage);

		CodeUtility cu = new CodeUtility();
		System.out.println(cu.getValFromXML(responseMessage, "franchiseeName"));

		if (cu.getValFromXML(responseMessage, "franchiseeName").equalsIgnoreCase(location.getFranchiseID())) {
			System.out.println("Location is Created");
		} else {
			System.out.println("Unable to Create Location");
		}
		return location;
		
	}

	private String franchiseXML(Location location) {
		String xml = "<fcRequest>" + "<adminFranchisee>" + "<status>" + location.getCorporateLocation() + "</status>"
				+ "<franchiseeName>" + location.getFranchiseID() + "</franchiseeName>" + "<centerName>"
				+ location.getCenterName() + "</centerName>" + "<areaID>" + location.getAreaRegion() + "</areaID>"
				+ "<reportPeriodStartDate>" + location.getRoyaltyReportingStartDate() + "</reportPeriodStartDate>"
				+ "<openingDate>" + location.getOpeningDate() + "</openingDate>" + "<storeTypeId>"
				+ location.getStoreType() + "</storeTypeId>" + "<address>" + location.getStreetAddress() + "</address>"
				+ "<address2>" + location.getAddress2() + "</address2>" + "<city>" + location.getCity() + "</city>"
				+ "<country>" + location.getCountry() + "</country>" + "<state>" + location.getState() + "</state>"
				+ "<zipcode>" + location.getZipCode() + "</zipcode>" + "<storePhone>" + location.getPhone()
				+ "</storePhone>" + "<storePhoneExtn>" + location.getPhoneExtension() + "</storePhoneExtn>"
				+ "<storeFax>" + location.getFax() + "</storeFax>" + "<storeMobile>" + location.getMobile()
				+ "</storeMobile>" + "<storeEmail>" + location.getEmail() + "</storeEmail>" + "<contactTitle>"
				+ location.getTitle() + "</contactTitle>" + "<contactFirstName>" + location.getFirstName()
				+ "</contactFirstName>" + "<contactLastName>" + location.getLastName() + "</contactLastName>"
				+ "<phone1>" + location.getPhone() + "</phone1>" + "<phone1Extn>" + location.getPhoneExtension()
				+ "</phone1Extn>" + "<fax>" + location.getFax() + "</fax>" + "<mobile>" + location.getMobile()
				+ "</mobile>" + "<emailID>" + location.getEmail() + "</emailID>" + "<entityDetail>"
				+ location.getOwnerTypeIndividualEntity() + "</entityDetail>" + "<ownerType>" + location.getNewOwner()
				+ "</ownerType>" + "<ownerTitle>" + location.getTitle() + "</ownerTitle>" + "<firstName>"
				+ location.getOwnerFirstName() + "</firstName>" + "<lastName>" + location.getOwnerLastName()
				+ "</lastName>" + "</adminFranchisee>" + "</fcRequest>";

		return xml;
	}

	private String RegionalUserXML(RegionalUser ru) throws InterruptedException {
		String xml = "<fcRequest>" + "<adminUser>" + "<userID>" + ru.getUserName() + "</userID>" + "<password>"
				+ ru.getPassword() + "</password>" + "<userLevel>Regional</userLevel>" + "<userLanguage>"
				+ ru.getUserLanguage() + "</userLanguage>" + "<timezone>" + ru.getTimezone() + "</timezone>" + "<type>"
				+ ru.getUserType() + "</type>" + "<region>" + ru.getArea_Region() + "</region>" + "<role>"
				+ ru.getRole() + "</role>" + "<firstname>" + ru.getFirstName() + "</firstname>" + "<lastname>"
				+ ru.getLastName() + "</lastname>" + "<isAuditor>" + ru.getAuditor() + "</isAuditor>"
				+ "<expirationTime>" + ru.getExpiryDays() + "</expirationTime>" + "<address>" + ru.getAddress()
				+ "</address>" + "<city>" + ru.getCity() + "</city>" + "<state>" + ru.getState() + "</state>"
				+ "<zipCode>" + ru.getZipcode() + "</zipCode>" + "<country>" + ru.getCountry() + "</country>"
				+ "<phone1>" + ru.getPhone1() + "</phone1>" + "<phoneExt1>" + ru.getPhoneExt1() + "</phoneExt1>"
				+ "<phone2>" + ru.getPhone2() + "</phone2>" + "<phoneExt2>" + ru.getPhoneExt2() + "</phoneExt2>"
				+ "<fax>" + ru.getFax() + "</fax>" + "<mobile>" + ru.getMobile() + "</mobile>" + "<email>"
				+ ru.getEmail() + "</email>" + "<sendNotification>" + ru.getSendNotification() + "</sendNotification>"
				+ "<visibleToStore>" + "Y" + "</visibleToStore>" + "</adminUser>" + "</fcRequest>";
		
		System.out.println(">>>>>>>>>>>>>>>>>> "+xml);
		Thread.sleep(15000);
		
		return xml;
	}	
}
