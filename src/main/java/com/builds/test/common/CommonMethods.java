package com.builds.test.common;

import java.util.Map;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Test;

import com.builds.uimaps.admin.AdminHiddenLinksConfigureNewHierarchyLevelPage;
import com.builds.uimaps.common.CommonUI;
import com.builds.utilities.Browsers;
import com.builds.utilities.FranconnectUtil;

public class CommonMethods {
	FranconnectUtil fc = new FranconnectUtil();

	public CorporateUser addCorporateUser(WebDriver driver, CorporateUser corpUser) throws Exception {
		CorporateUserTest corpTest = new CorporateUserTest();
		corpTest.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);
		return corpUser;
	}
	
	public DivisionalUser addDivisionalUser(WebDriver driver, DivisionalUser divisionalUser) throws Exception {
		DivisionalUserTest divisionalUserTest = new DivisionalUserTest();
		divisionalUserTest.createDefaultDivisionalUser_Through_WebService_AfterLogin(driver, divisionalUser);
		return divisionalUser;
	}
	
	public RegionalUser addRegionalUser(WebDriver driver, RegionalUser regionalUser) throws Exception {
		RegionalUserTest regionalUserTest = new RegionalUserTest();
		regionalUserTest.createDefaultRegionalUser_Through_WebService_AfterLogin(driver, regionalUser);
		return regionalUser;
	}

	@Test(groups = { "ConfigureNewHierarchyLevel" })
	public void ConfigureNewHierarchyLevel() throws Exception {

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		String testCaseId = "TC_ConfigureNewHierarchyLevel_01";
		try {
			driver = fc.loginpage().login(driver);
			AdminHiddenLinksConfigureNewHierarchyLevelPage pobj = new AdminHiddenLinksConfigureNewHierarchyLevelPage(
					driver);
			fc.adminpage().adminHiddenLinksConfigureNewHierarchyLevel(driver);

			if (!fc.utobj().isSelected(driver, pobj.isDivisionConfigureDisplayY)) {
				fc.utobj().clickElement(driver, pobj.isDivisionConfigureDisplayY);
			}
			fc.utobj().sendKeys(driver, pobj.divisionLabel, "Division");
			fc.utobj().sendKeys(driver, pobj.divisionUserLabel, "Divisional Users");
			fc.utobj().sendKeys(driver, pobj.divisionUserAbbr, "DU");

			try {
				if (!fc.utobj().isSelected(driver, pobj.lstEnableMultiBrandFDDRadiocheck)) {
					fc.utobj().clickElement(driver, pobj.lstEnableMultiBrandFDDRadiocheck);
				}
			} catch (Exception e) {

			}
			fc.utobj().clickElement(driver, pobj.submitBtn);
			fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
		}
	}

	public void Click_Close_Button_ByValue(WebDriver driver) throws Exception {
		CommonUI common = new CommonUI(driver);

		fc.utobj().printTestStep("Click Close");
		fc.utobj().clickElement(driver, common.Close_Button_ByValue);
	}

	public void Click_Close_Input_ByValue(WebDriver driver) throws Exception {
		CommonUI common = new CommonUI(driver);

		fc.utobj().printTestStep("Click Close");
		fc.utobj().clickElement(driver, common.Close_Input_ByValue);
	}

	public void Click_Submit_Button_ByValue(WebDriver driver) throws Exception {
		CommonUI common = new CommonUI(driver);

		fc.utobj().printTestStep("Click Submit");
		fc.utobj().clickElement(driver, common.Submit_Button_ByValue);
	}

	public void Click_Submit_Input_ByValue(WebDriver driver) throws Exception {
		CommonUI common = new CommonUI(driver);

		fc.utobj().printTestStep("Click Submit");
		fc.utobj().clickElement(driver, common.Submit_Input_ByValue);
	}

	public void Click_Yes_Button_ByValue(WebDriver driver) throws Exception {
		CommonUI common = new CommonUI(driver);

		fc.utobj().printTestStep("Click Yes Button");
		fc.utobj().clickElement(driver, common.yes_Button_ByValue);
	}

	public void Click_Yes_Input_ByValue(WebDriver driver) throws Exception {
		CommonUI common = new CommonUI(driver);

		fc.utobj().printTestStep("Click Yes Button");
		fc.utobj().clickElement(driver, common.yes_Input_ByValue);
	}

	public void Click_No_Button_ByValue(WebDriver driver) throws Exception {
		CommonUI common = new CommonUI(driver);

		fc.utobj().printTestStep("Click No Button");
		fc.utobj().clickElement(driver, common.Submit_Button_ByValue);
	}

	public void Click_No_Input_ByValue(WebDriver driver) throws Exception {
		CommonUI common = new CommonUI(driver);

		fc.utobj().printTestStep("Click No Button");
		fc.utobj().clickElement(driver, common.no_Input_ByValue);
	}

	public void Click_ADD_MORE_Input_ByValue(WebDriver driver) throws Exception {
		CommonUI common = new CommonUI(driver);

		fc.utobj().printTestStep("Click ADD MORE");
		fc.utobj().clickElement(driver, common.ADD_MORE_Input_ByValue);
	}

	public void Click_Complete_Input_ByValue(WebDriver driver) throws Exception {
		CommonUI common = new CommonUI(driver);

		fc.utobj().printTestStep("Click on Complete");
		fc.utobj().clickElement(driver, common.Complete_Input_ByValue);
	}

	public void Click_LogATask_Input_ByValue(WebDriver driver) throws Exception {
		CommonUI common = new CommonUI(driver);

		fc.utobj().printTestStep("Click on Log A Task");
		fc.utobj().clickElement(driver, common.LogATask_Input_ByValue);
	}

	public void Click_LogACall_Input_ByValue(WebDriver driver) throws Exception {
		CommonUI common = new CommonUI(driver);

		fc.utobj().printTestStep("Click on Log A Call");
		fc.utobj().clickElement(driver, common.LogACall_Input_ByValue);
	}

	public void Click_Search_Input_ByValue(WebDriver driver) throws Exception {
		CommonUI common = new CommonUI(driver);

		fc.utobj().printTestStep("Click on Search");
		fc.utobj().clickElement(driver, common.Search_Input_ByValue);
	}

	public void Click_SendEmail_Input_ByValue(WebDriver driver) throws Exception {
		CommonUI common = new CommonUI(driver);

		fc.utobj().printTestStep("Click on Send Email");
		fc.utobj().clickElement(driver, common.SendEmail_Input_ByValue);
	}

	public void Click_Add_To_Group_Input_ByValue(WebDriver driver) throws Exception {
		CommonUI common = new CommonUI(driver);

		fc.utobj().printTestStep("Click on Add to Group");
		fc.utobj().clickElement(driver, common.Add_To_Group_Input_ByValue);
	}

	public void Click_Delete_Input_ByValue(WebDriver driver) throws Exception {
		CommonUI common = new CommonUI(driver);

		fc.utobj().printTestStep("Click on Delete");
		fc.utobj().clickElement(driver, common.Delete_Input_ByValue);
	}

	public void Click_Delete_Button_ByValue(WebDriver driver) throws Exception {

		CommonUI common = new CommonUI(driver);

		fc.utobj().printTestStep("Click on Delete");
		fc.utobj().clickElement(driver, common.Delete_Button_ByValue);
	}

	public void Click_Print_Input_ByValue(WebDriver driver) throws Exception {

		CommonUI common = new CommonUI(driver);

		fc.utobj().printTestStep("Click on Print");
		fc.utobj().clickElement(driver, common.Print_Input_ByValue);
	}

	public void Click_Save_Input_ByValue(WebDriver driver) throws Exception {

		CommonUI common = new CommonUI(driver);

		fc.utobj().printTestStep("Click on Save");
		fc.utobj().clickElement(driver, common.Save_Input_ByValue);
	}

	public void Click_Save_Button_ByValue(WebDriver driver) throws Exception {

		CommonUI common = new CommonUI(driver);

		fc.utobj().printTestStep("Click on Save");
		fc.utobj().clickElement(driver, common.Save_Button_ByValue);
	}

	public void Click_Reset_Input_ByValue(WebDriver driver) throws Exception {

		CommonUI common = new CommonUI(driver);

		fc.utobj().printTestStep("Click on Submit");
		fc.utobj().clickElement(driver, common.Reset_Input_ByValue);
	}

	public void Click_Add_Input_ByValue(WebDriver driver) throws Exception {

		CommonUI common = new CommonUI(driver);

		fc.utobj().printTestStep("Click on Add");
		fc.utobj().clickElement(driver, common.Add_Input_ByValue);
	}

	public void Click_Cancel_Input_ByValue(WebDriver driver) throws Exception {

		CommonUI common = new CommonUI(driver);

		fc.utobj().printTestStep("Click on Cancel");
		fc.utobj().clickElement(driver, common.Cancel_Input_ByValue);
	}

	public void Click_Cancel_Button_ByValue(WebDriver driver) throws Exception {

		CommonUI common = new CommonUI(driver);

		fc.utobj().printTestStep("Click on Cancel");
		fc.utobj().clickElement(driver, common.Cancel_Button_ByValue);
	}

	public void Click_modify_Button_ByValue(WebDriver driver) throws Exception {

		CommonUI common = new CommonUI(driver);

		fc.utobj().printTestStep("Click on Modify");
		fc.utobj().clickElement(driver, common.modify_Button_ByValue);
	}

	public void Click_Archive_Input_ByValue(WebDriver driver) throws Exception {

		CommonUI common = new CommonUI(driver);

		fc.utobj().printTestStep("Click on Archive");
		fc.utobj().clickElement(driver, common.Archive_Input_ByValue);
	}

	public void Click_Archive_Button_ByValue(WebDriver driver) throws Exception {

		CommonUI common = new CommonUI(driver);

		fc.utobj().printTestStep("Click on Archive");
		fc.utobj().clickElement(driver, common.Archive_Button_ByValue);
	}

	public void Click_Unarchive_Input_ByValue(WebDriver driver) throws Exception {

		CommonUI common = new CommonUI(driver);

		fc.utobj().printTestStep("Click on Unarchive");
		fc.utobj().clickElement(driver, common.Unarchive_Input_ByValue);
	}

	public void Click_Next_Input_ByValue(WebDriver driver) throws Exception {

		CommonUI common = new CommonUI(driver);

		fc.utobj().printTestStep("Click on Next");
		fc.utobj().clickElement(driver, common.Next_Input_ByValue);
	}

	public void Click_Previous_Input_ByValue(WebDriver driver) throws Exception {

		CommonUI common = new CommonUI(driver);

		fc.utobj().printTestStep("Click on Previous");
		fc.utobj().clickElement(driver, common.Previous_Input_ByValue);
	}

	public void Click_Export_as_Excel_Input_ByValue(WebDriver driver) throws Exception {

		CommonUI common = new CommonUI(driver);

		fc.utobj().printTestStep("Click on Export As Excel");
		fc.utobj().clickElement(driver, common.Export_as_Excel_Input_ByValue);
	}

	public void Click_Create_Input_ByValue(WebDriver driver) throws Exception {

		CommonUI common = new CommonUI(driver);

		fc.utobj().printTestStep("Click on Create");
		fc.utobj().clickElement(driver, common.Create_Input_ByValue);
	}

	public void Click_Create_Button_ByValue(WebDriver driver) throws Exception {

		CommonUI common = new CommonUI(driver);

		fc.utobj().printTestStep("Click on Create");
		fc.utobj().clickElement(driver, common.Create_Button_ByValue);
	}

	public void Click_More_Input_ByValue(WebDriver driver) throws Exception {

		CommonUI common = new CommonUI(driver);

		fc.utobj().printTestStep("Click on More");
		fc.utobj().clickElement(driver, common.More_Input_ByValue);
	}

	public void Click_Continue_Input_ByValue(WebDriver driver) throws Exception {

		CommonUI common = new CommonUI(driver);

		fc.utobj().printTestStep("Click on Continue");
		fc.utobj().clickElement(driver, common.Continue_Input_ByValue);
	}

	public void Click_Back_Input_ByValue(WebDriver driver) throws Exception {

		CommonUI common = new CommonUI(driver);

		fc.utobj().printTestStep("Click on Back");
		fc.utobj().clickElement(driver, common.Back_Input_ByValue);
	}

	public void Click_Create_New_Report_Input_ByValue(WebDriver driver) throws Exception {

		CommonUI common = new CommonUI(driver);

		fc.utobj().printTestStep("Click on Create New Report");
		fc.utobj().clickElement(driver, common.Create_New_Report_Input_ByValue);
	}

	public void Click_Save_View_Input_ByValue(WebDriver driver) throws Exception {

		CommonUI common = new CommonUI(driver);

		fc.utobj().printTestStep("Click on Save View");
		fc.utobj().clickElement(driver, common.Save_View_Input_ByValue);
	}

	public void Click_View_Report_Input_ByValue(WebDriver driver) throws Exception {

		CommonUI common = new CommonUI(driver);

		fc.utobj().printTestStep("Click on View Report");
		fc.utobj().clickElement(driver, common.View_Report_Input_ByValue);
	}

	public void Click_View_In_Map_Input_ByValue(WebDriver driver) throws Exception {

		CommonUI common = new CommonUI(driver);

		fc.utobj().printTestStep("Click on View In Map");
		fc.utobj().clickElement(driver, common.View_In_Map_Input_ByValue);
	}

	public void Click_Associate_With_Campaign_Input_ByValue(WebDriver driver) throws Exception {

		CommonUI common = new CommonUI(driver);

		fc.utobj().printTestStep("Click on Associate With Campaign");
		fc.utobj().clickElement(driver, common.Associate_With_Campaign_Input_ByValue);
	}

	public void Click_Create_Workflow_Input_ByValue(WebDriver driver) throws Exception {

		CommonUI common = new CommonUI(driver);

		fc.utobj().printTestStep("Click on Create Workflow");
		fc.utobj().clickElement(driver, common.Create_Workflow_Input_ByValue);
	}

	public void Click_Send_FDD_Input_ByValue(WebDriver driver) throws Exception {

		CommonUI common = new CommonUI(driver);

		fc.utobj().printTestStep("Click on Send FDD");
		fc.utobj().clickElement(driver, common.Send_FDD_Input_ByValue);
	}

	public void Click_ico_Filter_XPATH_ByDataRole(WebDriver driver) throws Exception {

		CommonUI common = new CommonUI(driver);

		fc.utobj().printTestStep("Click on Filter");
		fc.utobj().clickElement(driver, common.ico_Filter_XPATH_ByDataRole);
	}

	public void Click_searchTemplate_Input_ByID(WebDriver driver) throws Exception {

		CommonUI common = new CommonUI(driver);

		fc.utobj().printTestStep("Click on Search Template");
		fc.utobj().clickElement(driver, common.searchTemplate_Input_ByID);
	}

	public void Click_Search_Button_ByID(WebDriver driver) throws Exception {

		CommonUI common = new CommonUI(driver);

		fc.utobj().printTestStep("Click on Search");
		fc.utobj().clickElement(driver, common.Search_Button_ByID);
	}

	public void Click_ApplyFilters_Button_ByValue(WebDriver driver) throws Exception {

		CommonUI common = new CommonUI(driver);

		fc.utobj().printTestStep("Click on Search");
		fc.utobj().clickElement(driver, common.ApplyFilters_Button_ByValue);
	}

	public void Click_Search_Input_ByID(WebDriver driver) throws Exception {

		CommonUI common = new CommonUI(driver);

		fc.utobj().printTestStep("Click on Search");
		fc.utobj().clickElement(driver, common.Search_Input_ByID);
	}

	public WebDriver switch_frameClass_newLayoutcboxIframe(WebDriver driver) throws Exception {
		fc.utobj().printTestStep("Switch to Frame/Overlay : newLayoutcboxIframe");
		CommonUI common = new CommonUI(driver);
		fc.utobj().switchFrameByClass(driver, common.frameClass_newLayoutcboxIframe);
		return driver;
	}

	public void switch_cboxIframe_frameId(WebDriver driver) throws Exception {

		CommonUI common = new CommonUI(driver);

		fc.utobj().printTestStep("Switch to Frame/Overlay : cboxIframe");
		fc.utobj().switchFrame(driver, common.cboxIframe);
	}

	public void Click_yes_Input_ByValue(WebDriver driver) throws Exception {

		CommonUI common = new CommonUI(driver);

		fc.utobj().printTestStep("Click on Yes");
		fc.utobj().clickElement(driver, common.yes_Input_ByValue);
	}

	public void Click_no_Input_ByValue(WebDriver driver) throws Exception {

		CommonUI common = new CommonUI(driver);

		fc.utobj().printTestStep("Click on No");
		fc.utobj().clickElement(driver, common.no_Input_ByValue);
	}

	public void Click_yes_Button_ByValue(WebDriver driver) throws Exception {

		CommonUI common = new CommonUI(driver);

		fc.utobj().printTestStep("Click on Yes");
		fc.utobj().clickElement(driver, common.yes_Button_ByValue);
	}

	public void Click_no_Button_ByValue(WebDriver driver) throws Exception {

		CommonUI common = new CommonUI(driver);

		fc.utobj().printTestStep("Click on No");
		fc.utobj().clickElement(driver, common.no_Button_ByValue);
	}

	public void ClickCloseColorBox(WebDriver driver) throws Exception {

		CommonUI common = new CommonUI(driver);

		fc.utobj().printTestStep("Click Cross on Color Box");
		fc.utobj().clickElement(driver, common.cboxClose_Div_ByID);
	}

	public void Click_OK_Button_ByValue(WebDriver driver) throws Exception {

		CommonUI common = new CommonUI(driver);

		fc.utobj().printTestStep("Click on OK");
		fc.utobj().clickElement(driver, common.ok_Button_ByValue);
	}

	public void Click_OK_Input_ByValue(WebDriver driver) throws Exception {

		CommonUI common = new CommonUI(driver);

		fc.utobj().printTestStep("Click on OK");
		fc.utobj().clickElement(driver, common.ok_Input_ByValue);
	}

	public CorporateUser fillDefaultDataFor_CorporateUser(CorporateUser corpUser) throws Exception {

		if (corpUser.getUserName() == null) {
			corpUser.setUserName(fc.utobj().generateTestData("corp"));
		}
		if (corpUser.getPassword() == null) {
			corpUser.setPassword("fran1234");
		}
		if (corpUser.getConfirmPassword() == null) {
			corpUser.setConfirmPassword("fran1234");
		}
		if (corpUser.getUserType() == null) {
			corpUser.setUserType("Normal User");
		}
		if (corpUser.getRole() == null) {
			corpUser.setRole("Corporate Administrator");
		}
		if (corpUser.getTimezone() == null) {
			corpUser.setTimezone("GMT -05:00 US/Canada/Eastern");
		}
		if (corpUser.getFirstName() == null) {
			corpUser.setFirstName(fc.utobj().generateTestData("selenium"));
		}
		if (corpUser.getLastName() == null) {
			corpUser.setLastName(fc.utobj().generateTestData("corp user"));
		}
		if (corpUser.getCity() == null) {
			corpUser.setCity("Reston");
		}
		if (corpUser.getCountry() == null) {
			corpUser.setCountry("USA");
		}
		if (corpUser.getState() == null) {
			corpUser.setState("Virginia");
		}
		if (corpUser.getPhone1() == null) {
			corpUser.setPhone1(fc.utobj().generateRandomNumber6Digit() + 3333);
		}
		if (corpUser.getEmail() == null) {
			corpUser.setEmail(fc.utobj().generateTestData("e") + "@fran.com");
		}
		if (corpUser.getSendNotification() == null) {
			corpUser.setSendNotification("N");
		}
		if (corpUser.getIsBillable() == null) {
			corpUser.setIsBillable("N");
		}
		if (corpUser.getIsDaylight() == null) {
			corpUser.setIsDaylight("Y");
		}
		if (corpUser.getConsultant() == null) {
			corpUser.setConsultant("N");
		}

		return corpUser;
	}
	
	public DivisionalUser fillDefaultDataFor_DivisionalUser(DivisionalUser divisionalUser) throws Exception {

		if (divisionalUser.getUserName() == null) {
			divisionalUser.setUserName(fc.utobj().generateTestData("corp"));
		}
		if (divisionalUser.getPassword() == null) {
			divisionalUser.setPassword("fran1234");
		}
		if (divisionalUser.getConfirmPassword() == null) {
			divisionalUser.setConfirmPassword("fran1234");
		}
		if (divisionalUser.getUserType() == null) {
			divisionalUser.setUserType("Normal User");
		}
		if (divisionalUser.getRole() == null) {
			divisionalUser.setRole("Default Division Role");
		}
		if (divisionalUser.getTimezone() == null) {
			divisionalUser.setTimezone("GMT -05:00 US/Canada/Eastern");
		}
		if (divisionalUser.getFirstName() == null) {
			divisionalUser.setFirstName(fc.utobj().generateTestData("selenium"));
		}
		if (divisionalUser.getLastName() == null) {
			divisionalUser.setLastName(fc.utobj().generateTestData("corp user"));
		}
		if (divisionalUser.getCity() == null) {
			divisionalUser.setCity("Reston");
		}
		if (divisionalUser.getCountry() == null) {
			divisionalUser.setCountry("USA");
		}
		if (divisionalUser.getState() == null) {
			divisionalUser.setState("Virginia");
		}
		if (divisionalUser.getPhone1() == null) {
			divisionalUser.setPhone1(fc.utobj().generateRandomNumber6Digit() + 3333);
		}
		if (divisionalUser.getEmail() == null) {
			divisionalUser.setEmail(fc.utobj().generateTestData("e") + "@fran.com");
		}
		if (divisionalUser.getSendNotification() == null) {
			divisionalUser.setSendNotification("N");
		}
		if (divisionalUser.getIsBillable() == null) {
			divisionalUser.setIsBillable("N");
		}
		if (divisionalUser.getIsDaylight() == null) {
			divisionalUser.setIsDaylight("Y");
		}

		return divisionalUser;
	}


	public RegionalUser fillDefaultDataFor_RegionalUser(RegionalUser regUser) throws Exception {

		regUser.setUserName(fc.utobj().generateTestData("c"));
		regUser.setPassword("123456789");
		// regUser.setConfirmPassword("123456789");
		regUser.setUserType("Normal");
		regUser.setUserLanguage("English");
		regUser.setRole("Default Regional Role");
		regUser.setTimezone("Asia/Kolkata");
		regUser.setFirstName(fc.utobj().generateTestData("cu"));
		regUser.setLastName(fc.utobj().generateTestData("cu"));
		regUser.setCity("Reston");
		regUser.setExpiryDays("NA");
		regUser.setCountry("USA");
		regUser.setState("Virginia");
		regUser.setPhone1(fc.utobj().generateRandomNumber6Digit() + 3333);
		regUser.setEmail(fc.utobj().generateTestData("e") + "@fran.com");
		regUser.setSendNotification("N");
		regUser.setIsBillable("N");
		regUser.setIsDaylight("Y");
		return regUser;
	}

	public FCHomePageTest getModules() {
		return new FCHomePageTest();
	}
	
	public Browsers browsers()
	{
		return new Browsers();
	}
	
	public Location getDefaultDataFor_Location(Location location) throws Exception {

		
		//get data from db
		Map<String, String> dataSet = fc.utobj().readTestData("infomgr", "TC_Default_Data_For_Location");
/*		
		location.setFranchiseID(fc.utobj().generateTestData("FIM"));
		
		location.setCenterName(fc.utobj().generateTestData("FIM_Center"));
		location.setLicenseNo(fc.utobj().generateTestData("Lic"));
		location.setStoreType("Default");
		
		//location.setStoreType(dataSet.get("storeType"));
		
		//location.setAreaRegion("R1");
		location.setCorporateLocation("No");
		location.setOpeningDate(fc.utobj().getCurrentDateUSFormat());
		location.setExpectedOpeningDate(fc.utobj().getCurrentDateUSFormat());
		location.setStreetAddress("C-94 Sector 8 Noida");
		location.setAddress2("Loc Address 2");
		location.setCity(fc.utobj().generateTestData("FIMCity"));
		location.setCountry("USA");
		location.setState("Alabama");
		location.setZipCode("201301");
		location.setPhone(fc.utobj().generateRandomNumber6Digit() + 9876);
		location.setPhoneExtension("12345");
		location.setFax("54321");
		location.setMobile("9876543215");
		location.setEmail("vimal.sharma@franconnect.com");
		
		//location.setEmail(fc.utobj().generateRandomChar()+"@franconnect.com");
		
		location.setWebsite("http://www.franconnect.com");
		location.setTitle("Mr.");
		location.setFirstName(fc.utobj().generateTestData("FIM_F"));
		location.setLastName(fc.utobj().generateTestData("FIM_L"));
		location.setCenterPhone(fc.utobj().generateRandomNumber6Digit()+6547);
		location.setCenterPhoneExtesion(fc.utobj().generateRandomNumber()+ 2);
		location.setCenterEmail("fcprelive@gmail.com");
		location.setCenterFax("75414");
		location.setCenterMobile(fc.utobj().generateRandomNumber6Digit()+1234);
		location.setOwnerTypeIndividual("Individual ");
		location.setOwnerTypeIndividualEntity("Entity");
		location.setNewEntity("New Entity");
		location.setExistingEntity("Existing Entity");
		location.setEntityType("Corporation");
		location.setEntityName(fc.utobj().generateTestData("FIM_Entity"));	
		location.setTaxpayerID(fc.utobj().generateTestData("FIM_Taxpayer"));
		location.setCountryofFormationResidency("USA");
		location.setStateofFormationResidency("Alaska");
		location.setNewOwner("New Owner");
		location.setExistingOwner("Existing Owners");
		location.setExistingLeads("Existing Leads");
		location.setSalutation("Dr.");
		location.setOwnerFirstName(fc.utobj().generateTestData("FIM"));
		location.setOwnerLastName(fc.utobj().generateTestData("Owner"));
		location.setMoveDocuments("Yes");
		location.setMUID(fc.utobj().generateTestData("MUID_"));*/
		
		
		location.setFranchiseID(fc.utobj().generateTestData(dataSet.get("FranchiseeID")));
		
		location.setCenterName(fc.utobj().generateTestData(dataSet.get("CenterName")));
		location.setLicenseNo(fc.utobj().generateTestData(dataSet.get("LicenseNo")));
		location.setStoreType(dataSet.get("StoreType"));
		
		//location.setStoreType(dataSet.get("storeType"));
		
		//location.setAreaRegion("R1");
		location.setCorporateLocation(dataSet.get("CorporateLocation"));
		location.setOpeningDate(fc.utobj().getCurrentDateUSFormat());
		location.setExpectedOpeningDate(fc.utobj().getCurrentDateUSFormat());
		location.setStreetAddress(dataSet.get("StreetAddress"));
		location.setAddress2(dataSet.get("Loc Address 2"));
		location.setCity(fc.utobj().generateTestData(dataSet.get("City")));
		location.setCountry(dataSet.get("Country"));
		location.setState(dataSet.get("State"));
		location.setZipCode(dataSet.get("ZipCode"));
		location.setPhone(fc.utobj().generateRandomNumber6Digit() + 9876);
		location.setPhoneExtension(dataSet.get("PhoneExtension"));
		location.setFax(dataSet.get("Fax"));
		location.setMobile(dataSet.get("Mobile"));
		location.setEmail(dataSet.get("Email"));
		
		//location.setEmail(fc.utobj().generateRandomChar()+"@franconnect.com");
		
		location.setWebsite(dataSet.get("Website"));
		location.setTitle(dataSet.get("Title"));
		location.setFirstName(fc.utobj().generateTestData(dataSet.get("FIM_F")));
		location.setLastName(fc.utobj().generateTestData(dataSet.get("FIM_L")));
		location.setCenterPhone(fc.utobj().generateRandomNumber6Digit()+6547);
		location.setCenterPhoneExtesion(fc.utobj().generateRandomNumber()+ 2);
		location.setCenterEmail(dataSet.get("CenterEmail"));
		location.setCenterFax(dataSet.get("CenterFax"));
		location.setCenterMobile(fc.utobj().generateRandomNumber6Digit()+1234);
		location.setOwnerTypeIndividual("Individual ");
		location.setOwnerTypeIndividualEntity("Entity");
		location.setNewEntity("New Entity");
		location.setExistingEntity("Existing Entity");
		location.setEntityType("Corporation");
		location.setEntityName(fc.utobj().generateTestData(dataSet.get("EntityName")));	
		location.setTaxpayerID(fc.utobj().generateTestData("FIM_Taxpayer"));
		location.setCountryofFormationResidency("USA");
		location.setStateofFormationResidency("Alaska");
		location.setNewOwner("New Owner");
		location.setExistingOwner("Existing Owners");
		location.setExistingLeads("Existing Leads");
		location.setSalutation(dataSet.get("Salutation"));
		location.setOwnerFirstName(fc.utobj().generateTestData(dataSet.get("OwnerFirstName")));
		location.setOwnerLastName(fc.utobj().generateTestData(dataSet.get("OwnerLastName")));
		location.setMoveDocuments("Yes");
		location.setMUID(fc.utobj().generateTestData(dataSet.get("MUID")));
		return location;
	}

	public CorporateUser fillDefaultDataFor_CorporateUserwithSQLite(CorporateUser corpUser, Map<String, String> dataSet) throws Exception {


		if (corpUser.getUserName() == null) {
			String Corpuname=fc.utobj().generateTestData(dataSet.get("Corpuname"));
			corpUser.setUserName(Corpuname);
		}
		if (corpUser.getPassword() == null) {
			String Corppassword=dataSet.get("Corppassword");
			corpUser.setPassword(Corppassword);
		}
		if (corpUser.getConfirmPassword() == null) {
			String Corpconfirmpassword=dataSet.get("Corpconfirmpassword");
			corpUser.setConfirmPassword(Corpconfirmpassword);
		}
		if (corpUser.getUserType() == null) {
			String Corpusertype=dataSet.get("Corpusertype");
			corpUser.setUserType(Corpusertype);
		}
		if (corpUser.getRole() == null) {
			String Corprole=dataSet.get("Corprole");
			corpUser.setRole(Corprole);
		}
		if (corpUser.getTimezone() == null) {
			String Timezone=dataSet.get("Timezone");
			corpUser.setTimezone(Timezone);
		}
		if (corpUser.getFirstName() == null) {
			String Corpfirstname=fc.utobj().generateTestData(dataSet.get("Corpfirstname"));
			corpUser.setFirstName(Corpfirstname);
		}
		if (corpUser.getLastName() == null) {
			String Corplastname=fc.utobj().generateTestData(dataSet.get("Corplastname"));
			corpUser.setLastName(Corplastname);
		}
		if (corpUser.getCity() == null) {
			String Corpcity=dataSet.get("Corpcity");
			corpUser.setCity(Corpcity);
		}
		if (corpUser.getCountry() == null) {
			String Corpcountry=dataSet.get("Corpcountry");
			corpUser.setCountry(Corpcountry);
		}
		if (corpUser.getState() == null) {
			String Corpstate=dataSet.get("Corpstate");
			corpUser.setState(Corpstate);
		}
		if (corpUser.getPhone1() == null) {
			String Corpphone=dataSet.get("Corpphone");
			corpUser.setPhone1(fc.utobj().generateRandomNumber6Digit() + Corpphone);
		}
		if (corpUser.getEmail() == null) {
			String Corpemail=fc.utobj().generateTestData(dataSet.get("Corpemail"));
			corpUser.setEmail(Corpemail + "@staffex.com");
		}
		if (corpUser.getSendNotification() == null) {
			String Corpsendnotification=dataSet.get("Corpsendnotification");
			corpUser.setSendNotification(Corpsendnotification);
		}
		if (corpUser.getIsBillable() == null) {
			String Corpisbillable=dataSet.get("Corpisbillable");
			corpUser.setIsBillable(Corpisbillable);
		}
		if (corpUser.getIsDaylight() == null) {
			String Corpisdaylight=dataSet.get("Corpisdaylight");
			corpUser.setIsDaylight(Corpisdaylight);
		}
		if (corpUser.getConsultant() == null) {
			String Corpisconsultant=dataSet.get("Corpisconsultant");
			corpUser.setConsultant(Corpisconsultant);
		}

		return corpUser;
	
	}

}