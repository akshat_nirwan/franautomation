package com.builds.test.common;

public class ConfigureNewHierarchyLevel {
	
	private String enableNewHierarchyLevel;
	private String leadsExclusiveToNewHierarchyLevel;

	
	public String getLeadsExclusiveToNewHierarchyLevel() {
		return leadsExclusiveToNewHierarchyLevel;
	}

	public void setLeadsExclusiveToNewHierarchyLevel(String leadsExclusiveToNewHierarchyLevel) {
		this.leadsExclusiveToNewHierarchyLevel = leadsExclusiveToNewHierarchyLevel;
	}
	
	public String getEnableNewHierarchyLevel() {
		return enableNewHierarchyLevel;
	}

	public void setEnableNewHierarchyLevel(String enableNewHierarchyLevel) {
		this.enableNewHierarchyLevel = enableNewHierarchyLevel;
	}

	
	

}
