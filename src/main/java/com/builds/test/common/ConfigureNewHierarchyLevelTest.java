package com.builds.test.common;

import org.openqa.selenium.WebDriver;

import com.builds.uimaps.common.ConfigureNewHierarchyLevelUI;
import com.builds.utilities.FranconnectUtil;

public class ConfigureNewHierarchyLevelTest {

	WebDriver driver;
	FranconnectUtil fc = new FranconnectUtil();

	public ConfigureNewHierarchyLevelTest(WebDriver driver) {
		this.driver = driver;
	}

	public void EnableNewHierarchyLevel(ConfigureNewHierarchyLevel configureNewHierarchyLevel) throws Exception {
		ConfigureNewHierarchyLevelUI ui = new ConfigureNewHierarchyLevelUI(driver);

		if (configureNewHierarchyLevel.getEnableNewHierarchyLevel() != null) {
			if (configureNewHierarchyLevel.getEnableNewHierarchyLevel().equalsIgnoreCase("yes")) {
				if (ui.enableNewHierarchyLevel_Yes.isEnabled()) {
					fc.utobj().clickElement(driver, ui.enableNewHierarchyLevel_Yes);
				}
			}

			else if (configureNewHierarchyLevel.getEnableNewHierarchyLevel().equalsIgnoreCase("no")) {
				if (ui.enableNewHierarchyLevel_No.isEnabled()) {
					fc.utobj().clickElement(driver, ui.enableNewHierarchyLevel_No);
				}
			}
		}
		
		LeadsExclusiveToNewHierarchyLevel(configureNewHierarchyLevel);
		fc.utobj().clickElement(driver, ui.SaveBtn);
	}
	
	void LeadsExclusiveToNewHierarchyLevel(ConfigureNewHierarchyLevel configureNewHierarchyLevel) throws Exception {
		ConfigureNewHierarchyLevelUI ui = new ConfigureNewHierarchyLevelUI(driver);

		if (configureNewHierarchyLevel.getLeadsExclusiveToNewHierarchyLevel() != null) {
			if (configureNewHierarchyLevel.getLeadsExclusiveToNewHierarchyLevel().equalsIgnoreCase("yes")) {
				if (ui.leadsExclusiveToNewHierarchyLevel_Yes.isEnabled()) {
					fc.utobj().clickElement(driver, ui.leadsExclusiveToNewHierarchyLevel_Yes);
				}
			} else if (configureNewHierarchyLevel.getLeadsExclusiveToNewHierarchyLevel().equalsIgnoreCase("no")) {
				if (ui.leadsExclusiveToNewHierarchyLevel_No.isEnabled()) {
					fc.utobj().clickElement(driver, ui.leadsExclusiveToNewHierarchyLevel_No);
				}
			}
		}
	}

}
