package com.builds.test.common;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Test;

import com.builds.test.admin.AdminPageTest;
import com.builds.test.admin.AdminUsersManageCorporateUsersPageTest;
import com.builds.uimaps.common.CorporateUserUI;
import com.builds.utilities.CodeUtility;
import com.builds.utilities.FranconnectUtil;

public class CorporateUserTest {

	@Test
	private void addUser() throws Exception {
		FranconnectUtil fc = new FranconnectUtil();
		CorporateUser corpUser = new CorporateUser();
		corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);

		System.out.println(corpUser.getUserName());
		System.out.println(corpUser.getFirstName());
		System.out.println(corpUser.getLastName());

		WebDriver driver = null;
		try {
			createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);
		} catch (Exception e) {
			System.out.println("Error");
			// fc.utobj().quitBrowserOnCatch(driver, config, e, testCaseId);
		}
	}

	public CorporateUser createDefaultCorporateUser_Through_WebService_AfterLogin(WebDriver driver,
			CorporateUser corpUser) throws Exception {
		FranconnectUtil fc = new FranconnectUtil();

		String responseMessage = null;

		String module = "admin";
		String subModule = "user";
		String isISODate = "no";

		fc.utobj().printTestStep("Adding User through Web Services.");
		WebService rest = new WebService();

		String xmlString = "<fcRequest>" + "<adminUser>" + "<userID>" + corpUser.getUserName() + "</userID>"
				+ "<password>" + corpUser.getPassword() + "</password>" + "<userLevel>Corporate</userLevel>"
				+ "<timezone>" + corpUser.getTimezone() + "</timezone>" + "<type>" + corpUser.getUserType() + "</type>"
				+ "<expirationTime>" + corpUser.getExpiryDays() + "</expirationTime>" + "<role>" + corpUser.getRole()
				+ "</role>" + "<jobTitle>" + corpUser.getJobTitle() + "</jobTitle>" + "<firstname>"
				+ corpUser.getFirstName() + "</firstname>" + "<lastname>" + corpUser.getLastName() + "</lastname>"
				+ "<address>" + corpUser.getAddress() + "</address>" + "<city>" + corpUser.getCity() + "</city>"
				+ "<state>" + corpUser.getState() + "</state>" + "<zipCode>" + corpUser.getZipcode() + "</zipCode>"
				+ "<country>" + corpUser.getCountry() + "</country>" + "<phone1>" + corpUser.getPhone1() + "</phone1>"
				+ "<phoneExt1>" + corpUser.getPhoneExt1() + "</phoneExt1>" + "<phone2>" + corpUser.getPhone2()
				+ "</phone2>" + "<phoneExt2>" + corpUser.getPhoneExt2() + "</phoneExt2>" + "<fax>" + corpUser.getFax()
				+ "</fax>" + "<mobile>" + corpUser.getMobile() + "</mobile>" + "<email>" + corpUser.getEmail()
				+ "</email>" + "<sendNotification>" + corpUser.getSendNotification() + "</sendNotification>"
				+ "<loginUserIp>" + corpUser.getLoginUserIp() + "</loginUserIp>" + "<isBillable>"
				+ corpUser.getIsBillable() + "</isBillable>" + "<isDaylight>" + corpUser.getIsDaylight()
				+ "</isDaylight>" + "<isAuditor>" + corpUser.getConsultant() + "</isAuditor>" + "</adminUser>"
				+ "</fcRequest>";

		try {
			responseMessage = rest.create(module, subModule, xmlString, isISODate);
		} catch (Exception e) {

		}

		CodeUtility cu = new CodeUtility();
		if(cu.getValFromXML(responseMessage, "responseStatus").equalsIgnoreCase("Error"))
		{
			fc.utobj().throwsException("Unable to add User : "+responseMessage);
		}
		
		responseMessage = "success";/*
									 * verifyCorporateUser_ThroughWebService(
									 * corpUser);
									 */

		if (!responseMessage.contains("success")) {

			AdminUsersManageCorporateUsersPageTest manageCorporate = new AdminUsersManageCorporateUsersPageTest();
			AdminPageTest admin = new AdminPageTest();
			admin.adminPage(driver);
			admin.openCorporateUserPage(driver);

			manageCorporate.addCorporateUsers(driver);

			CorporateUserUI ui = new CorporateUserUI(driver);

			if (corpUser.getUserName() != null) {
				fc.utobj().sendKeys(driver, ui.userName, corpUser.getUserName() + "1");
			}

			if (corpUser.getPassword() != null) {
				fc.utobj().sendKeys(driver, ui.password, corpUser.getPassword());
			}

			if (corpUser.getConfirmPassword() != null) {
				fc.utobj().sendKeys(driver, ui.confirmPassword, corpUser.getConfirmPassword());
			}

			if (corpUser.getConfirmPassword() != null) {
				fc.utobj().sendKeys(driver, ui.confirmPassword, corpUser.getConfirmPassword());
			}

			if (corpUser.getUserType() != null) {
				fc.utobj().selectDropDownByVisibleText(driver, ui.userType, corpUser.getUserType());
			}

			if (corpUser.getRole() != null) {
				fc.utobj().selectValFromMultiSelect(driver, ui.role_MultiSelect, corpUser.getRole());
			}

			if (corpUser.getTimezone() != null) {
				fc.utobj().selectDropDownByVisibleText(driver, ui.timezone_Select, corpUser.getTimezone());
			}

			if (corpUser.getIsDaylight() != null) {
				fc.utobj().check(ui.isDaylight_Check, corpUser.getIsDaylight());
			}

			if (corpUser.getJobTitle() != null) {
				fc.utobj().sendKeys(driver, ui.jobTitle, corpUser.getJobTitle());
			}

			if (corpUser.getUserLanguage() != null) {
				fc.utobj().selectDropDownByVisibleText(driver, ui.userLanguage, corpUser.getUserLanguage());
			}

			if (corpUser.getFirstName() != null) {
				fc.utobj().sendKeys(driver, ui.firstName, corpUser.getFirstName());
			}

			if (corpUser.getLastName() != null) {
				fc.utobj().sendKeys(driver, ui.lastName, corpUser.getLastName());
			}

			if (corpUser.getCity() != null) {
				fc.utobj().sendKeys(driver, ui.city, corpUser.getCity());
			}

			if (corpUser.getCountry() != null) {
				fc.utobj().selectDropDownByVisibleText(driver, ui.country, corpUser.getCountry());
			}

			if (corpUser.getState() != null) {
				fc.utobj().selectDropDownByVisibleText(driver, ui.state, corpUser.getState());
			}

			if (corpUser.getPhone1() != null) {
				fc.utobj().sendKeys(driver, ui.phone1, corpUser.getPhone1());
			}

			if (corpUser.getEmail() != null) {
				fc.utobj().sendKeys(driver, ui.phone1, corpUser.getEmail());
			}

			fc.utobj().clickElement(driver, ui.submit);

		}

		return corpUser;
	}

	public String verifyCorporateUser_ThroughWebService(CorporateUser corpUser) throws Exception {
		String module = "admin";
		String subModule = "user";
		String isISODate = "no";

		String xmlString = "<fcRequest>" + "<filter>" + "<referenceId></referenceId>" + "<userID>"
				+ corpUser.getUserName() + "</userID>" + "<firstname>" + corpUser.getFirstName() + "</firstname>"
				+ "<lastname>" + corpUser.getLastName() + "</lastname>" + "</filter>" + "</fcRequest>";

		WebService service = new WebService();
		String responseMessage = service.retrieve(module, subModule, xmlString, isISODate);

		return responseMessage;

		// TODO : Check for user is added or not.
	}
}
