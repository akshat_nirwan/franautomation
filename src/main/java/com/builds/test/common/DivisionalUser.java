package com.builds.test.common;

public class DivisionalUser extends User {

	private String Division;

	public String getDivision() {
		return Division;
	}

	public void setDivision(String division) {
		Division = division;
	}

}
