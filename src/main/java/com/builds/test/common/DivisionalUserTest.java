package com.builds.test.common;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Test;

import com.builds.utilities.FranconnectUtil;

public class DivisionalUserTest {
	

	
	@Test
	private void addDivisionalUser() throws Exception {
		FranconnectUtil fc = new FranconnectUtil();
		DivisionalUser	 divisionalUser = new DivisionalUser();
		
		divisionalUser = fc.commonMethods().fillDefaultDataFor_DivisionalUser(divisionalUser);
		
		System.out.println(divisionalUser.getUserName());
		System.out.println(divisionalUser.getFirstName());
		System.out.println(divisionalUser.getLastName());

		WebDriver driver = null;
		try {
			createDefaultDivisionalUser_Through_WebService_AfterLogin(driver, divisionalUser);
		} catch (Exception e) {
			System.out.println("Error");
			// fc.utobj().quitBrowserOnCatch(driver, config, e, testCaseId);
		}
	}

	public DivisionalUser createDefaultDivisionalUser_Through_WebService_AfterLogin(WebDriver driver,
			DivisionalUser divisionalUser) throws Exception {
		FranconnectUtil fc = new FranconnectUtil();

		String responseMessage = null;

		String module = "admin";
		String subModule = "user";
		String isISODate = "no";

		fc.utobj().printTestStep("Adding Divisional User through Web Services.");
		WebService rest = new WebService();

		String xmlString = "<fcRequest>" + "<adminUser>" + "<userID>" + divisionalUser.getUserName() + "</userID>"
				+ "<password>" + divisionalUser.getPassword() + "</password>" + "<userLevel>Divisional</userLevel>"
				+ "<timezone>" + divisionalUser.getTimezone() + "</timezone>" + "<type>" + divisionalUser.getUserType() + "</type>"
				+ "<expirationTime>" + divisionalUser.getExpiryDays() + "</expirationTime>" + "<role>" + divisionalUser.getRole()
				+ "</role>" + "<jobTitle>" + divisionalUser.getJobTitle() + "</jobTitle>" + "<division>" + divisionalUser.getDivision() + "</division>" + "<userLanguage>English</userLanguage>" +  "<firstname>"
				+ divisionalUser.getFirstName() + "</firstname>" + "<lastname>" + divisionalUser.getLastName() + "</lastname>"
				+ "<address>" + divisionalUser.getAddress() + "</address>" + "<city>" + divisionalUser.getCity() + "</city>"
				+ "<state>" + divisionalUser.getState() + "</state>" + "<zipCode>" + divisionalUser.getZipcode() + "</zipCode>"
				+ "<country>" + divisionalUser.getCountry() + "</country>" + "<phone1>" + divisionalUser.getPhone1() + "</phone1>"
				+ "<phoneExt1>" + divisionalUser.getPhoneExt1() + "</phoneExt1>" + "<phone2>" + divisionalUser.getPhone2()
				+ "</phone2>" + "<phoneExt2>" + divisionalUser.getPhoneExt2() + "</phoneExt2>" + "<fax>" + divisionalUser.getFax()
				+ "</fax>" + "<mobile>" + divisionalUser.getMobile() + "</mobile>" + "<email>" + divisionalUser.getEmail()
				+ "</email>" + "<sendNotification>" + divisionalUser.getSendNotification() + "</sendNotification>"
				+ "<loginUserIp>" + divisionalUser.getLoginUserIp() + "</loginUserIp>" + "<isBillable>"
				+ divisionalUser.getIsBillable() + "</isBillable>" + "<isDaylight>" + divisionalUser.getIsDaylight()
				+ "</isDaylight>" + "<isAuditor>" + "y" + "</isAuditor>" + "</adminUser>"
				+ "</fcRequest>";

		try {
			responseMessage = rest.create(module, subModule, xmlString, isISODate);
		} catch (Exception e) {

		}
		
		System.out.println(responseMessage);
		
		/*
									 * verifyCorporateUser_ThroughWebService(
									 * corpUser);
									 */

	/*	if (!responseMessage.contains("success")) {

			AdminUsersManageCorporateUsersPageTest manageCorporate = new AdminUsersManageCorporateUsersPageTest();
			AdminPageTest admin = new AdminPageTest();
			admin.adminPage(driver);
			admin.openCorporateUserPage(driver);

			manageCorporate.addCorporateUsers(driver);

			CorporateUserUI ui = new CorporateUserUI(driver);
			Region

			if (corpUser.getUserName() != null) {
				fc.utobj().sendKeys(driver, ui.userName, corpUser.getUserName() + "1");
			}

			if (corpUser.getPassword() != null) {
				fc.utobj().sendKeys(driver, ui.password, corpUser.getPassword());
			}

			if (corpUser.getConfirmPassword() != null) {
				fc.utobj().sendKeys(driver, ui.confirmPassword, corpUser.getConfirmPassword());
			}

			if (corpUser.getConfirmPassword() != null) {
				fc.utobj().sendKeys(driver, ui.confirmPassword, corpUser.getConfirmPassword());
			}

			if (corpUser.getUserType() != null) {
				fc.utobj().selectDropDownByVisibleText(driver, ui.userType, corpUser.getUserType());
			}

			if (corpUser.getRole() != null) {
				fc.utobj().selectValFromMultiSelect(driver, ui.role_MultiSelect, corpUser.getRole());
			}

			if (corpUser.getTimezone() != null) {
				fc.utobj().selectDropDownByVisibleText(driver, ui.timezone_Select, corpUser.getTimezone());
			}

			if (corpUser.getIsDaylight() != null) {
				fc.utobj().check(ui.isDaylight_Check, corpUser.getIsDaylight());
			}

			if (corpUser.getJobTitle() != null) {
				fc.utobj().sendKeys(driver, ui.jobTitle, corpUser.getJobTitle());
			}

			if (corpUser.getUserLanguage() != null) {
				fc.utobj().selectDropDownByVisibleText(driver, ui.userLanguage, corpUser.getUserLanguage());
			}

			if (corpUser.getFirstName() != null) {
				fc.utobj().sendKeys(driver, ui.firstName, corpUser.getFirstName());
			}

			if (corpUser.getLastName() != null) {
				fc.utobj().sendKeys(driver, ui.lastName, corpUser.getLastName());
			}

			if (corpUser.getCity() != null) {
				fc.utobj().sendKeys(driver, ui.city, corpUser.getCity());
			}

			if (corpUser.getCountry() != null) {
				fc.utobj().selectDropDownByVisibleText(driver, ui.country, corpUser.getCountry());
			}

			if (corpUser.getState() != null) {
				fc.utobj().selectDropDownByVisibleText(driver, ui.state, corpUser.getState());
			}

			if (corpUser.getPhone1() != null) {
				fc.utobj().sendKeys(driver, ui.phone1, corpUser.getPhone1());
			}

			if (corpUser.getEmail() != null) {
				fc.utobj().sendKeys(driver, ui.phone1, corpUser.getEmail());
			}

			fc.utobj().clickElement(driver, ui.submit);

		}*/

		return divisionalUser;
	}



}
