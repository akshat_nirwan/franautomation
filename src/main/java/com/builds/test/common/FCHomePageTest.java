package com.builds.test.common;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.Reporter;
import org.testng.annotations.Test;

import com.builds.test.admin.AdminPageTest;
import com.builds.uimaps.common.FCHomePage;
import com.builds.utilities.FranconnectUtil;

import test.edge2.AdminPage;

public class FCHomePageTest extends FranconnectUtil {

	@Test
	public void clickSalesModule(WebDriver driver) throws Exception {

		if(clickModule(driver, "module_fs")==false)
		{
			utobj().throwsException("Unable to Click on Sales module");
		}
		
		
		
		
		/*FCHomePage pobj = new FCHomePage(driver);
		try {
			utobj().clickElement(driver, pobj.fsModule);
		} catch (Exception e) {
			utobj().clickElement(driver, pobj.moreLink);
			utobj().clickElement(driver, pobj.fsModule);
		}*/
	}

	public void openFIMModule(WebDriver driver) throws Exception {

		if(clickModule(driver, "module_fim")==false)
		{
			utobj().throwsException("Unable to Click on Info Mgr module");
		}
		
		
		
		/*FCHomePage pobj = new FCHomePage(driver);

		try {
			utobj().clickElement(driver, pobj.fimModule);
		} catch (Exception e) {
			utobj().clickElement(driver, pobj.moreLink);
			utobj().sleep(3000);
			utobj().clickElement(driver, pobj.fimModule);
		}

		utobj().printTestStep("Navigating to Information Manager Module");*/
	}

	public void openOpenerModule(WebDriver driver) throws Exception {
		
		if(clickModule(driver, "module_storeopener")==false)
		{
			utobj().throwsException("Unable to Click on Opener module");
		}
		
		
		/*FCHomePage pobj = new FCHomePage(driver);

		try {
			utobj().clickElement(driver, pobj.openerModule);
		} catch (Exception e) {
			utobj().clickElement(driver, pobj.moreLink);
			utobj().sleep(3000);
			utobj().clickElement(driver, pobj.openerModule);
		}

		utobj().printTestStep("Navigating to Opener Module");*/
	}

	@Test
	public void openIntranetModule(WebDriver driver) throws Exception {

		
		if(clickModule(driver, "module_intranet")==false)
		{
			utobj().throwsException("Unable to Click on The Hub module");
		}
		
		/*FCHomePage pobj = new FCHomePage(driver);

		try {
			utobj().clickElement(driver, pobj.intranetModule);
		} catch (Exception e) {
			utobj().clickElement(driver, pobj.moreLink);
			utobj().sleep(3000);
			utobj().clickElement(driver, pobj.intranetModule);
		}

		utobj().printTestStep("Navigating to Intranet Module");*/
	}

	@Test
	public AdminPageTest openAdminPage(WebDriver driver) throws Exception {

		FCHomePage pobj = new FCHomePage(driver);
		
		try {
			if (utobj().isElementPresent(pobj.adminGearIcon)) {
				try {
					utobj().clickElement(driver, pobj.adminGearIcon.get(0));
				} catch (Exception e) {
					utobj().clickElement(driver, pobj.adminGearIcon.get(0));
				}
			}else {
				try {
					utobj().clickElement(driver, pobj.userOptions);
					utobj().clickElement(driver, pobj.adminLink);
					utobj().printTestStep("Click on User > Admin Link");
				} catch (Exception E) {
					utobj().printTestStep("Click on User > Admin Link");
					utobj().clickElement(driver, pobj.userOptions);
					utobj().clickElement(driver, pobj.adminLink);
				}
			}
		} catch (Exception e) {
			Reporter.log("Went wrong with new gui admin: "+e.getMessage());
		}
		return new AdminPageTest();
	}

	public void openMarketingPage(WebDriver driver) throws Exception {

		FCHomePage pobj = new FCHomePage(driver);

		try {
			// utobj().clickElement(driver, pobj.marketingLink);
		} catch (Exception E) {
		}

	}

	@Test
	public void logout(WebDriver driver) throws Exception {
		try {
			FCHomePage pobj = new FCHomePage(driver);
			utobj().clickElement(driver, pobj.userOptions);
			utobj().clickElement(driver, pobj.logout);
			Reporter.log("Loging out");
		} catch (Exception e) {
			utobj().printBugStatus("Unable to logout from the build!");
		}
	}

	@Test
	public void openTrainingModule(WebDriver driver) throws Exception {

		
		if(clickModule(driver, "module_training")==false)
		{
			utobj().throwsException("Unable to Click on Training module");
		}
		
		/*FCHomePage pobj = new FCHomePage(driver);
		utobj().sleep(5000);

		try {
			utobj().clickElement(driver, pobj.trainingModule);
		} catch (Exception e) {
			utobj().clickElement(driver, pobj.moreLink);
			utobj().clickElement(driver, pobj.trainingModule);
		}

		utobj().printTestStep("Navigating to Training Module");*/
	}

	public void openSmartConnectModule(WebDriver driver) throws Exception {

		if(clickModule(driver, "module_smartconnect")==false)
		{
			utobj().throwsException("Unable to Click on Smart Connect module");
		}
		
		
		
		/*FCHomePage pobj = new FCHomePage(driver);
		utobj().sleep(5000);

		try {
			utobj().clickElement(driver, pobj.smartConnectModule);
		} catch (Exception e) {
			utobj().clickElement(driver, pobj.moreLink);
			utobj().clickElement(driver, pobj.smartConnectModule);
		}

		utobj().printTestStep("Navigating to SmartConnect");*/
	}
	
	public void openSupportModule(WebDriver driver) throws Exception {

		if(clickModule(driver, "module_support")==false)
		{
			utobj().throwsException("Unable to Click on Support module");
		}
		
		
		
		/*FCHomePage pobj = new FCHomePage(driver);
		utobj().sleep(5000);

		try {
			utobj().clickElement(driver, pobj.smartConnectModule);
		} catch (Exception e) {
			utobj().clickElement(driver, pobj.moreLink);
			utobj().clickElement(driver, pobj.smartConnectModule);
		}

		utobj().printTestStep("Navigating to SmartConnect");*/
	}

	public void openTheHubModule(WebDriver driver) throws Exception {

		if(clickModule(driver, "module_intranet")==false)
		{
			utobj().throwsException("Unable to Click on The Hub module");
		}
		
		
		/*FCHomePage pobj = new FCHomePage(driver);
		utobj().sleep(5000);

		try {
			utobj().clickElement(driver, pobj.theHubModule);
		} catch (Exception e) {
			utobj().clickElement(driver, pobj.moreLink);
			utobj().clickElement(driver, pobj.theHubModule);
		}

		utobj().printTestStep("Navigating to The Hub");*/

		/*
		 * crm().crm_common().adminCRMConfigureIndustryLnk(driver);
		 * crm().crm_common().CRMHomeLnk(driver);
		 */
	}

	public void openFinanceModule(WebDriver driver) throws Exception {

		
		if(clickModule(driver, "module_financials")==false)
		{
			utobj().throwsException("Unable to Click on Finance module");
		}
		
		/*FCHomePage pobj = new FCHomePage(driver);

		try {
			utobj().clickElement(driver, pobj.financeModule);
		} catch (Exception e) {
			utobj().clickElement(driver, pobj.moreLink);
			utobj().sleep(3000);
			utobj().clickElement(driver, pobj.moreFinance);
		}

		utobj().printTestStep("Navigating to Finance module");*/
	}

	@Test
	public void openFieldopsModule(WebDriver driver) throws Exception {

		if(clickModule(driver, "module_audit")==false)
		{
			utobj().throwsException("Unable to Click on Field Ops module");
		}
		
		
		/*FCHomePage pobj = new FCHomePage(driver);

		try {
			utobj().clickElement(driver, pobj.fieldopsModule);
		} catch (Exception e) {
			utobj().clickElement(driver, pobj.moreLink);
			utobj().sleep(3000);
			utobj().clickElement(driver, pobj.fieldopsModule);
		}

		utobj().printTestStep("Navigating to FieldOps module");*/
	}

	
	public FCHomePageTest openCRMModule(WebDriver driver) throws Exception {

		FCHomePage pobj = new FCHomePage(driver);
		boolean isModuleClickable=false;
		
		//New Change CRM : Navigation : 11:01:2018
		if (pobj.adminGearIcon.size() > 0) {
			//Click over marketing module
			try {
				
				if (utobj().isElementPresent(driver, ".//li[contains(@class ,'leftnav-first-li mktOn') and not(contains(@class ,'leftnav-first-li mktOn active'))]")) {
					utobj().clickElement(driver, driver.findElement(By.id("mktSubmenu")));
				}
				List<WebElement> listOfModules3 = driver
						.findElements(By.xpath(".//li[contains(@class ,'leftnav-first-li mktOn')]/ul/li"));
				for (WebElement w : listOfModules3) {
					if (w.getAttribute("id").contentEquals("module_cm")) {
						utobj().clickElement(driver, w);
						isModuleClickable=true;
					}
				}
			} catch (Exception e) {
				// TODO: handle exception
			}
			if (!isModuleClickable) {
				utobj().throwsException("Unable to Click on CRM module");
			}
		}else {
			try {
				WebElement marketinglabel = driver.findElement(By.xpath(".//a[@class = 'marketinglabel']"));
				marketinglabel.click();
				Thread.sleep(1000);
				WebElement marketinglist = driver.findElement(By.id("moreMenu-options2"));
				Thread.sleep(1000);
				List<WebElement> marketing_list = marketinglist.findElements(By.tagName("li"));

				boolean isCRMClicked = false;
				if (marketing_list.size() > 0) {
					for (WebElement SelectLi : marketing_list) {
						if (SelectLi.getText().equals("CRM")) {
							SelectLi.click();
							isCRMClicked = true;
							break;
						}
					}
				}
				if (isCRMClicked == false) {
					WebElement crm = driver.findElement(By.xpath(".//a[contains(text(),'CRM')]"));
					crm.click();
				}
			} catch (Exception e) {
				utobj().clickElement(driver, pobj.crmTab);
			}
		}
		
		utobj().printTestStep("Navigating to CRM module");
		return new FCHomePageTest();
	}

	private boolean clickModule(WebDriver driver, String moduleID) throws Exception {

		FCHomePage pobj = new FCHomePage(driver);

		//New Change : Navigation : 11:01:2018
		if (pobj.adminGearIcon.size() > 0) {
			List<WebElement> listOfModules = driver.findElements(By.xpath(".//div[@id='mCSB_1_container']/ul/li"));

			try {
				try {
					for (WebElement w : listOfModules) {						
						if (utobj().moveToElement(driver, w).getAttribute("id").contentEquals(moduleID)) {
							if (utobj().moveToElement(driver, w).getAttribute("class").contains("leftnav-first-li") && !utobj().moveToElement(driver, w).getAttribute("class").contains("active")){
								utobj().clickElementByJS(driver, /*driver.findElement(By.xpath(".//div[@id='mCSB_1_container']/ul/li/a"))*/w.findElement(By.xpath("./a")));
							}return true;
						}
					}
				} catch (Exception e) {
					// TODO: handle exception
				}

				//Click over marketing module
				try {
					utobj().clickElement(driver, driver.findElement(By.id("mktSubmenu")));
					List<WebElement> listOfModules3 = driver
							.findElements(By.xpath(".//li[@class='leftnav-first-li mktOn active']/ul/li"));
					for (WebElement w : listOfModules3) {
						if (utobj().moveToElement(driver, w).getAttribute("id").contentEquals(moduleID)) {
							if (utobj().moveToElement(driver, w).getAttribute("class").contentEquals("on leftnav-first-li")) {
								utobj().clickElement(driver, w);
							}
							return true;
						}
					}
				} catch (Exception e) {
					// TODO: handle exception
				}

			} catch (Exception e) {
				// TODO: handle exception
			}

		} else {
			List<WebElement> listOfModules = driver.findElements(By.xpath(".//div[@id='top_menu']/ul/li"));
			try {
				try {
					for (WebElement w : listOfModules) {
						if (w.getAttribute("id").contentEquals(moduleID)) {
							utobj().clickElement(driver, w);
							return true;
						}
					}
				} catch (Exception e) {
				}

				try {
					utobj().clickElement(driver, driver.findElement(By.xpath(".//div[@id='test1']/span/a")));
					List<WebElement> listOfModules2 = driver
							.findElements(By.xpath(".//div[@id='moreMenu-options1']/ul/li"));
					for (WebElement w : listOfModules2) {
						if (w.getAttribute("id").contentEquals(moduleID)) {
							utobj().clickElement(driver, w);
							return true;
						}
					}
				} catch (Exception e) {
				}
				try {
					utobj().clickElement(driver, driver.findElement(By.xpath(".//div[@id='test2']/span/a")));
					List<WebElement> listOfModules3 = driver
							.findElements(By.xpath(".//div[@id='moreMenu-options2']//ul/li//a"));
					for (WebElement w : listOfModules3) {
						if (w.getAttribute("href").contentEquals(moduleID)) {
							utobj().clickElement(driver, w);
							return true;
						}
					}
				} catch (Exception e) {

				}
			} catch (Exception e) {
			}
		}
		return false;
	}
	
	
	private boolean clickMarketingModule(WebDriver driver, String moduleID, String xpath) throws Exception {
		List<WebElement> listOfModules = driver.findElements(By.xpath(".//div[@id='top_menu']/ul/li"));
		try {
			try {
				for (WebElement w : listOfModules) {
					if (w.getAttribute("id").contentEquals(moduleID)) {
						utobj().clickElement(driver, w);
						return true;
					}
				}
			} catch (Exception e) {
			}

			try {
				utobj().clickElement(driver, driver.findElement(By.xpath(".//div[@id='test2']/span/a")));
				List<WebElement> listOfModules3 = driver
						.findElements(By.xpath(".//div[@id='moreMenu-options2']//ul/li"));
				for (WebElement w : listOfModules3) {
					if (driver.findElements(By.xpath("")).size()>0) {
						utobj().clickElement(driver, w);
						return true;
					}
				}
			} catch (Exception e) {

			}
		} catch (Exception e) {
		}
		return false;
	}
	
	

}
