package com.builds.test.common;

import com.builds.test.fieldops.FieldOps_Common;

public class FieldOpsModule {

	private FieldOps_Common FieldOpsobj = null;

	public FieldOps_Common fieldops_common() {
		if (FieldOpsobj == null) {
			FieldOpsobj = new FieldOps_Common();
		}
		return FieldOpsobj;
	}
}
