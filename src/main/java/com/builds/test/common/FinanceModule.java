package com.builds.test.common;

import com.builds.test.fin.FIN_Common;

public class FinanceModule {

	private FIN_Common financeobj = null;

	public FIN_Common finance_common() {
		if (financeobj == null) {
			financeobj = new FIN_Common();
		}
		return financeobj;
	}
}
