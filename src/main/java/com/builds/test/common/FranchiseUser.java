package com.builds.test.common;

public class FranchiseUser {

	private String loginID;
	private String password;
	private String confirmPassword;
	private String type;
	private String expirationTime;
	private String role;
	private String timeZone;
	private String AllowDST;
	private String userType;
	private String jobTitle;
	private String name;
	private String language;
	private String streetAddress;
	private String city;
	private String country;
	private String zippostalCode;
	private String state;
	private String phone1;
	private String phone1Extn;
	private String phone2;
	private String phone2Extn;
	private String fax;
	private String mobile;
	private String email;
	private String ipAddress;
	private String isBillable;
	private String birthDate;
	private String birthMonth;
	private String uploadUserPicture;
	private String sendNotification;
	
	public String getLoginID() {
		return loginID;
	}
	public void setLoginID(String loginID) {
		this.loginID = loginID;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getConfirmPassword() {
		return confirmPassword;
	}
	public void setConfirmPassword(String confirmPassword) {
		this.confirmPassword = confirmPassword;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getExpirationTime() {
		return expirationTime;
	}
	public void setExpirationTime(String expirationTime) {
		this.expirationTime = expirationTime;
	}
	public String getRole() {
		return role;
	}
	public void setRole(String role) {
		this.role = role;
	}
	public String getTimeZone() {
		return timeZone;
	}
	public void setTimeZone(String timeZone) {
		this.timeZone = timeZone;
	}
	public String getAllowDST() {
		return AllowDST;
	}
	public void setAllowDST(String allowDST) {
		AllowDST = allowDST;
	}
	public String getUserType() {
		return userType;
	}
	public void setUserType(String userType) {
		this.userType = userType;
	}
	public String getJobTitle() {
		return jobTitle;
	}
	public void setJobTitle(String jobTitle) {
		this.jobTitle = jobTitle;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getLanguage() {
		return language;
	}
	public void setLanguage(String language) {
		this.language = language;
	}
	public String getStreetAddress() {
		return streetAddress;
	}
	public void setStreetAddress(String streetAddress) {
		this.streetAddress = streetAddress;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public String getZippostalCode() {
		return zippostalCode;
	}
	public void setZippostalCode(String zippostalCode) {
		this.zippostalCode = zippostalCode;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getPhone1() {
		return phone1;
	}
	public void setPhone1(String phone1) {
		this.phone1 = phone1;
	}
	public String getPhone1Extn() {
		return phone1Extn;
	}
	public void setPhone1Extn(String phone1Extn) {
		this.phone1Extn = phone1Extn;
	}
	public String getPhone2() {
		return phone2;
	}
	public void setPhone2(String phone2) {
		this.phone2 = phone2;
	}
	public String getPhone2Extn() {
		return phone2Extn;
	}
	public void setPhone2Extn(String phone2Extn) {
		this.phone2Extn = phone2Extn;
	}
	public String getFax() {
		return fax;
	}
	public void setFax(String fax) {
		this.fax = fax;
	}
	public String getMobile() {
		return mobile;
	}
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getIpAddress() {
		return ipAddress;
	}
	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}
	public String getIsBillable() {
		return isBillable;
	}
	public void setIsBillable(String isBillable) {
		this.isBillable = isBillable;
	}
	public String getBirthDate() {
		return birthDate;
	}
	public void setBirthDate(String birthDate) {
		this.birthDate = birthDate;
	}
	public String getBirthMonth() {
		return birthMonth;
	}
	public void setBirthMonth(String birthMonth) {
		this.birthMonth = birthMonth;
	}
	public String getUploadUserPicture() {
		return uploadUserPicture;
	}
	public void setUploadUserPicture(String uploadUserPicture) {
		this.uploadUserPicture = uploadUserPicture;
	}
	public String getSendNotification() {
		return sendNotification;
	}
	public void setSendNotification(String sendNotification) {
		this.sendNotification = sendNotification;
	}
	
	
	
}
