package com.builds.test.common;

import com.builds.test.infomgr.InfoMgr_Common;

public class InfoMgrModule {

	public InfoMgrModule() {

	}

	private InfoMgr_Common infomgrobj = null;

	public InfoMgr_Common infomgr_common() {
		if (infomgrobj == null) {
			infomgrobj = new InfoMgr_Common();
		}
		return infomgrobj;
	}

}
