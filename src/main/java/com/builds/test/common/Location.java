package com.builds.test.common;

import java.util.Map;

public class Location {

	private String FranchiseID;
	private String CenterName;
	private String AreaRegion;
	private String Brands;
	private String FranchiseeConsultant;
	private String LicenseNo;
	private String StoreType;
	private String CorporateLocation;
	private String AgreementVersion;
	private String RoyaltyReportingStartDate;
	private String TaxRate;
	private String FBC;
	private String OpeningDate;
	private String ExpectedOpeningDate;
	private String StreetAddress;
	private String Address2;
	private String City;
	private String Country;
	private String ZipCode;
	private String State;
	private String Phone;
	private String PhoneExtension;
	private String Fax;
	private String Mobile;
	private String Email;
	private String Website;
	private String Title;
	private String FirstName;
	private String LastName;
	private String CenterPhone;
	private String CenterPhoneExtesion;
	private String CenterEmail;
	private String CenterFax;
	private String CenterMobile;
	private String OwnerTypeIndividual;
	private String OwnerTypeIndividualEntity;
	private String NewEntity;
	private String ExistingEntity;
	private String ExistingEntityName;
	private String EntityType;
	private String EntityName;
	private String TaxpayerID;
	private String CountryofFormationResidency;
	private String StateofFormationResidency;
	private String NewOwner;
	private String ExistingOwner;
	private String ExistingLeads;
	private String Salutation;
	private String OwnerFirstName;
	private String OwnerLastName;
	private String AddMore;
	private String SelectOwners;
	private String LeadSearchBox;
	private String MoveDocuments;
	private String MUID;
	private String Submit;
	private String Reset;
	private String Cancel;

	public String getFranchiseID() {
		return FranchiseID;
	}

	public void setFranchiseID(String franchiseID) {
		FranchiseID = franchiseID;
	}

	public String getCenterName() {
		return CenterName;
	}

	public void setCenterName(String centerName) {
		CenterName = centerName;
	}

	public String getAreaRegion() {
		return AreaRegion;
	}

	public void setAreaRegion(String areaRegion) {
		AreaRegion = areaRegion;
	}

	public String getBrands() {
		return Brands;
	}

	public void setBrands(String brands) {
		Brands = brands;
	}

	public String getFranchiseeConsultant() {
		return FranchiseeConsultant;
	}

	public void setFranchiseeConsultant(String franchiseeConsultant) {
		FranchiseeConsultant = franchiseeConsultant;
	}

	public String getLicenseNo() {
		return LicenseNo;
	}

	public void setLicenseNo(String licenseNo) {
		LicenseNo = licenseNo;
	}

	public String getStoreType() {
		return StoreType;
	}

	public void setStoreType(String storeType) {
		StoreType = storeType;
	}

	public String getCorporateLocation() {
		return CorporateLocation;
	}

	public void setCorporateLocation(String corporateLocation) {
		CorporateLocation = corporateLocation;
	}

	public String getAgreementVersion() {
		return AgreementVersion;
	}

	public void setAgreementVersion(String agreementVersion) {
		AgreementVersion = agreementVersion;
	}

	public String getRoyaltyReportingStartDate() {
		return RoyaltyReportingStartDate;
	}

	public void setRoyaltyReportingStartDate(String royaltyReportingStartDate) {
		RoyaltyReportingStartDate = royaltyReportingStartDate;
	}

	public String getTaxRate() {
		return TaxRate;
	}

	public void setTaxRate(String taxRate) {
		TaxRate = taxRate;
	}

	public String getFBC() {
		return FBC;
	}

	public void setFBC(String fBC) {
		FBC = fBC;
	}

	public String getOpeningDate() {
		return OpeningDate;
	}

	public String getExpectedOpeningDate() {
		return ExpectedOpeningDate;
	}

	public void setExpectedOpeningDate(String expectedOpeningDate) {
		ExpectedOpeningDate = expectedOpeningDate;
	}

	public void setOpeningDate(String openingDate) {
		OpeningDate = openingDate;
	}

	public String getStreetAddress() {
		return StreetAddress;
	}

	public void setStreetAddress(String streetAddress) {
		StreetAddress = streetAddress;
	}

	public String getAddress2() {
		return Address2;
	}

	public void setAddress2(String address2) {
		Address2 = address2;
	}

	public String getCity() {
		return City;
	}

	public void setCity(String city) {
		City = city;
	}

	public String getCountry() {
		return Country;
	}

	public void setCountry(String country) {
		Country = country;
	}

	public String getZipCode() {
		return ZipCode;
	}

	public void setZipCode(String zipCode) {
		ZipCode = zipCode;
	}

	public String getState() {
		return State;
	}

	public void setState(String state) {
		State = state;
	}

	public String getPhone() {
		return Phone;
	}

	public void setPhone(String phone) {
		Phone = phone;
	}

	public String getPhoneExtension() {
		return PhoneExtension;
	}

	public void setPhoneExtension(String phoneExtension) {
		PhoneExtension = phoneExtension;
	}

	public String getFax() {
		return Fax;
	}

	public void setFax(String fax) {
		Fax = fax;
	}

	public String getMobile() {
		return Mobile;
	}

	public void setMobile(String mobile) {
		Mobile = mobile;
	}

	public String getEmail() {
		return Email;
	}

	public void setEmail(String email) {
		Email = email;
	}

	public String getWebsite() {
		return Website;
	}

	public void setWebsite(String website) {
		Website = website;
	}

	public String getTitle() {
		return Title;
	}

	public void setTitle(String title) {
		Title = title;
	}

	public String getFirstName() {
		return FirstName;
	}

	public void setFirstName(String firstName) {
		FirstName = firstName;
	}

	public String getLastName() {
		return LastName;
	}

	public void setLastName(String lastName) {
		LastName = lastName;
	}

	public String getCenterPhone() {
		return CenterPhone;
	}

	public void setCenterPhone(String centerPhone) {
		CenterPhone = centerPhone;
	}

	public String getCenterPhoneExtesion() {
		return CenterPhoneExtesion;
	}

	public void setCenterPhoneExtesion(String centerPhoneExtesion) {
		CenterPhoneExtesion = centerPhoneExtesion;
	}

	public String getCenterEmail() {
		return CenterEmail;
	}

	public void setCenterEmail(String centerEmail) {
		CenterEmail = centerEmail;
	}

	public String getCenterFax() {
		return CenterFax;
	}

	public void setCenterFax(String centerFax) {
		CenterFax = centerFax;
	}

	public String getCenterMobile() {
		return CenterMobile;
	}

	public void setCenterMobile(String centerMobile) {
		CenterMobile = centerMobile;
	}

	public String getOwnerTypeIndividual() {
		return OwnerTypeIndividual;
	}

	public void setOwnerTypeIndividual(String ownerTypeIndividual) {
		OwnerTypeIndividual = ownerTypeIndividual;
	}

	public String getOwnerTypeIndividualEntity() {
		return OwnerTypeIndividualEntity;
	}

	public void setOwnerTypeIndividualEntity(String ownerTypeIndividualEntity) {
		OwnerTypeIndividualEntity = ownerTypeIndividualEntity;
	}

	public String getNewEntity() {
		return NewEntity;
	}

	public void setNewEntity(String newEntity) {
		NewEntity = newEntity;
	}

	public String getExistingEntity() {
		return ExistingEntity;
	}

	public void setExistingEntity(String existingEntity) {
		ExistingEntity = existingEntity;
	}

	public String getExistingEntityName() {
		return ExistingEntityName;
	}

	public void setExistingEntityName(String existingEntityName) {
		ExistingEntityName = existingEntityName;
	}

	public String getEntityType() {
		return EntityType;
	}

	public void setEntityType(String entityType) {
		EntityType = entityType;
	}

	public String getEntityName() {
		return EntityName;
	}

	public void setEntityName(String entityName) {
		EntityName = entityName;
	}

	public String getTaxpayerID() {
		return TaxpayerID;
	}

	public void setTaxpayerID(String taxpayerID) {
		TaxpayerID = taxpayerID;
	}

	public String getCountryofFormationResidency() {
		return CountryofFormationResidency;
	}

	public void setCountryofFormationResidency(String countryofFormationResidency) {
		CountryofFormationResidency = countryofFormationResidency;
	}

	public String getStateofFormationResidency() {
		return StateofFormationResidency;
	}

	public void setStateofFormationResidency(String stateofFormationResidency) {
		StateofFormationResidency = stateofFormationResidency;
	}

	public String getNewOwner() {
		return NewOwner;
	}

	public void setNewOwner(String newOwner) {
		NewOwner = newOwner;
	}

	public String getExistingOwner() {
		return ExistingOwner;
	}

	public void setExistingOwner(String existingOwner) {
		ExistingOwner = existingOwner;
	}

	public String getExistingLeads() {
		return ExistingLeads;
	}

	public void setExistingLeads(String existingLeads) {
		ExistingLeads = existingLeads;
	}

	public String getSalutation() {
		return Salutation;
	}

	public void setSalutation(String salutation) {
		Salutation = salutation;
	}

	public String getOwnerFirstName() {
		return OwnerFirstName;
	}

	public void setOwnerFirstName(String ownerFirstName) {
		OwnerFirstName = ownerFirstName;
	}

	public String getOwnerLastName() {
		return OwnerLastName;
	}

	public void setOwnerLastName(String ownerLastName) {
		OwnerLastName = ownerLastName;
	}

	public String getAddMore() {
		return AddMore;
	}

	public void setAddMore(String addMore) {
		AddMore = addMore;
	}

	public String getSelectOwners() {
		return SelectOwners;
	}

	public void setSelectOwners(String selectOwners) {
		SelectOwners = selectOwners;
	}

	public String getLeadSearchBox() {
		return LeadSearchBox;
	}

	public void setLeadSearchBox(String leadSearchBox) {
		LeadSearchBox = leadSearchBox;
	}

	public String getMoveDocuments() {
		return MoveDocuments;
	}

	public void setMoveDocuments(String moveDocuments) {
		MoveDocuments = moveDocuments;
	}

	public String getMUID() {
		return MUID;
	}

	public void setMUID(String mUID) {
		MUID = mUID;
	}

	public String getSubmit() {
		return Submit;
	}

	public void setSubmit(String submit) {
		Submit = submit;
	}

	public String getReset() {
		return Reset;
	}

	public void setReset(String reset) {
		Reset = reset;
	}

	public String getCancel() {
		return Cancel;
	}

	public void setCancel(String cancel) {
		Cancel = cancel;
	}

	public String getFullName(String firstName, String lastName) {
		return firstName + " " + lastName;
	}

}
