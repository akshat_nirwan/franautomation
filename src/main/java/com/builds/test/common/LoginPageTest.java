package com.builds.test.common;

import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.Reporter;

import com.builds.uimaps.common.FCHomePage;
import com.builds.uimaps.common.LoginPage;
import com.builds.utilities.FranconnectUtil;

public class LoginPageTest extends FranconnectUtil {
	FranconnectUtil fc = new FranconnectUtil();

	
	public WebDriver loginWithUserNameAndPassword(WebDriver driver,String userName,String password) throws Exception
	{
		
		LoginPage pobj = new LoginPage(driver);
		if (fc.utobj().isElementPresent(driver, pobj.userid) == false) {
			fc.utobj().throwsException("Login page not found!");
		}
		
		try {
			pobj = new LoginPage(driver);
			fc.utobj().printTestStep("Enter User Credentials");
			utobj().sendKeys(driver, pobj.userid, userName);
			utobj().sendKeys(driver, pobj.password, password);
			fc.utobj().printTestStep("Click on Login Button");
			utobj().clickElement(driver, pobj.login);
		} catch (Exception e) {
			Reporter.log(e.toString());
			utobj().throwsException("Issues with Login / Build is not accessible.");
		}

		FCHomePage verifyPage = new FCHomePage(driver);
		try {
			verifyPage.userOptions.isDisplayed();
		} catch (Exception e) {
			utobj().throwsException("Issue after hitting login button.");
		}
		
		return driver;
	}
	
	public WebDriver login(WebDriver driver) throws Exception {
		
		String buildUrl = config.get("buildUrl");
		fc.utobj().printTestStep("Enter Build Url");
		
		try
		{
			driver.get(buildUrl);
		}catch(Exception driverNotOpened)
		{
			
		}
		
		try
		{
			LoginPage pobj = new LoginPage(driver);
			if (fc.utobj().isElementPresent(driver, pobj.userid) == false) {
				throw new Exception();
			}
		}catch(Exception elementNotFound)
		{
			try
			{
				driver.get(buildUrl);
			}catch(Exception driverNotOpened)
			{
				fc.utobj().throwsException("Unable to open the Build (Wait time 3 minutes)");
			}
		}
		
		loginWithUserNameAndPassword(driver, config.get("userName"), config.get("password"));
		return driver;
	}

	public WebDriver loginBaseThreads(WebDriver driver) throws Exception {
		String buildUrl = config.get("buildUrl");
		try {
			
			buildUrl="https://automation1.franconnectqa.net/fc";
			if (buildUrl.contains("/fc/") || buildUrl.contains("/fc")) {
				buildUrl=buildUrl.replaceAll("/fc", "/basethreads/");
				System.out.println(buildUrl);
			}
		} catch (Exception e) {
			Reporter.log(e.getMessage() , true);
		}
		fc.utobj().printTestStep("Enter Build Url");

		try {
			driver.get(buildUrl);
		} catch (Exception driverNotOpened) {

		}

		try {
			LoginPage pobj = new LoginPage(driver);
			if (fc.utobj().isElementPresent(driver, pobj.userid) == false) {
				throw new Exception();
			}
		} catch (Exception elementNotFound) {
			try {
				driver.get(buildUrl);
			} catch (Exception driverNotOpened) {
				fc.utobj().throwsException("Unable to open the Build (Wait time 3 minutes)");
			}
		}

		LoginPage pobj = new LoginPage(driver);
		if (fc.utobj().isElementPresent(driver, pobj.userid) == false) {
			fc.utobj().throwsException("Login page not found!");
		}
		
		try {
			pobj = new LoginPage(driver);
			fc.utobj().printTestStep("Enter User Credentials");
			utobj().sendKeys(driver, pobj.userid, FranconnectUtil.config.get("userName"));
			utobj().sendKeys(driver, pobj.password, FranconnectUtil.config.get("password"));
			fc.utobj().printTestStep("Click on Login Button");
			utobj().clickElement(driver, pobj.loginBtBtn);
		} catch (Exception e) {
			Reporter.log(e.toString());
			utobj().throwsException("Issues with Login / Build is not accessible.");
		}

		FCHomePage verifyPage = new FCHomePage(driver);
		try {
			verifyPage.btLoggedOut.isDisplayed();
		} catch (Exception e) {
			utobj().throwsException("Issue after hitting login button.");
		}
		return driver;
	}
	
	
	public WebDriver login() throws Exception {

		WebDriver driver = null;
		LoginPage pobj = new LoginPage(driver);
		utobj().clickElement(driver, pobj.login);
		FCHomePage verifyPage = new FCHomePage(driver);
		Assert.assertTrue(verifyPage.userOptions.isEnabled());
		return driver;
	}

	public void loginWithParameter(WebDriver driver, String userName, String password) throws Exception {
		driver.navigate().to(FranconnectUtil.config.get("buildUrl"));
		
		loginWithUserNameAndPassword(driver, userName, password);
		
	}

}
