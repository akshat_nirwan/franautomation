package com.builds.test.common;

import java.util.ArrayList;
import java.util.List;

public class ManageAreaRegion {
	
	// Add Area Region
	private String Category;
	private String areaRegionName;
	private String groupBy;
	List<String> countries = new ArrayList<String>();
	List<String> states = new ArrayList<String>();
	private String zipCodes_type;
	private String listOfZipCodes_zipCodes;
	private String file_zipCodes;
	private ArrayList<String> counties = new ArrayList<String>();
	
	private String selectFranchiseeAssignment;

	public ArrayList<String> getCounties() {
		return counties;
	}
	public void setCounties(ArrayList<String> counties) {
		this.counties = counties;
	}
	public String getSelectFranchiseeAssignment() {
		return selectFranchiseeAssignment;
	}
	public void setSelectFranchiseeAssignment(String selectFranchiseeAssignment) {
		this.selectFranchiseeAssignment = selectFranchiseeAssignment;
	}
	
	public List<String> getStates() {
		return states;
	}
	public void setStates(List<String> states) {
		this.states = states;
	}

	
	public String getListOfZipCodes_zipCodes() {
		return listOfZipCodes_zipCodes;
	}
	public void setListOfZipCodes_zipCodes(String listOfZipCodes_zipCodes) {
		this.listOfZipCodes_zipCodes = listOfZipCodes_zipCodes;
	}
	public String getFile_zipCodes() {
		return file_zipCodes;
	}
	public void setFile_zipCodes(String file_zipCodes) {
		this.file_zipCodes = file_zipCodes;
	}

	public String getZipCodes_type() {
		return zipCodes_type;
	}
	public void setZipCodes_type(String zipCodes_type) {
		this.zipCodes_type = zipCodes_type;
	}
	
	public String getCategory() {
		return Category;
	}
	public void setCategory(String category) {
		Category = category;
	}
	public String getAreaRegionName() {
		return areaRegionName;
	}
	public void setAreaRegionName(String areaRegionName) {
		this.areaRegionName = areaRegionName;
	}
	public String getGroupBy() {
		return groupBy;
	}
	public void setGroupBy(String groupBy) {
		this.groupBy = groupBy;
	}
	public List<String> getCountries() {
		return countries;
	}
	public void setCountries(List<String> countries) {
		this.countries = countries;
	}
	
	
	
}
