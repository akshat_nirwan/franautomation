package com.builds.test.common;

import org.openqa.selenium.WebDriver;

import com.builds.uimaps.common.AddAreaRegionUI;
import com.builds.uimaps.common.ManageAreaRegionUI;
import com.builds.uimaps.common.SelectFranchiseeAssignmentAreaRegionUI;
import com.builds.utilities.FranconnectUtil;

public class ManageAreaRegionTest {
	
	private WebDriver driver;
	private FranconnectUtil fc = new FranconnectUtil();
	
	public ManageAreaRegionTest(WebDriver driver) {
		this.driver=driver;
	}
	
	public ManageAreaRegionTest addAreaRegion(ManageAreaRegion manageAreaRegion) throws Exception
	{
		ManageAreaRegionUI ui = new ManageAreaRegionUI(driver);
		AddAreaRegionUI addAreaRegionUI = new AddAreaRegionUI(driver);
		fc.utobj().printTestStep("Add Area Region");
		fc.utobj().clickElement(driver, ui.addAreaRegion_btn);
		fill_addAreaRegion(manageAreaRegion);
		fc.utobj().clickElement(driver, addAreaRegionUI.submit_btn);
		if (manageAreaRegion.getGroupBy().equalsIgnoreCase("County")) {
			if (manageAreaRegion.getCounties() != null) {
				for (String county : manageAreaRegion.getCounties()) {
					fc.utobj().clickElement(driver, addAreaRegionUI.selectCounty(driver, county));
				}

			}
			fc.utobj().clickElement(driver, addAreaRegionUI.NextBtn);
		}
		
		if (manageAreaRegion.getCategory().equalsIgnoreCase("International")) {
			if (manageAreaRegion.getStates() != null) {
				for (String state : manageAreaRegion.getStates()) {
					fc.utobj().clickElement(driver, addAreaRegionUI.selectState(driver, state));
				}

			}
			fc.utobj().clickElement(driver, addAreaRegionUI.NextBtn2);
		}
		fill_confirmation_clickNext(manageAreaRegion);
		return new ManageAreaRegionTest(driver);
	}

	void fill_addAreaRegion(ManageAreaRegion manageAreaRegion) throws Exception {
		fc.utobj().printTestStep("Fill data in Add Area Region");
		AddAreaRegionUI ui = new AddAreaRegionUI(driver);

		if (manageAreaRegion.getCategory() != null) {
			fc.utobj().selectDropDown(driver, ui.category_dropDown, manageAreaRegion.getCategory());
		}

		if (manageAreaRegion.getAreaRegionName() != null) {
			fc.utobj().sendKeys(driver, ui.areaRegionName_textBox, manageAreaRegion.getAreaRegionName());
		}

		if (manageAreaRegion.getGroupBy() != null) {
			fc.utobj().selectDropDown(driver, ui.groupBy_dropDown, manageAreaRegion.getGroupBy());
			
			if (manageAreaRegion.getGroupBy().equalsIgnoreCase("Zip Codes")) {
				if (manageAreaRegion.getZipCodes_type() != null) {
					if (manageAreaRegion.getZipCodes_type().equalsIgnoreCase("Enter a comma separated list of Zip Codes manually")) {
						fc.utobj().clickElement(driver, ui.zipCodes_EnterCommaSeparatedListZipCodesManually_radioBtn);
						fc.utobj().sendKeys(driver, ui.zipCodes_EnterCommaSeparatedListZipCodesManually_textBox,
								manageAreaRegion.getListOfZipCodes_zipCodes());
					}
					if (manageAreaRegion.getZipCodes_type().equalsIgnoreCase("Specify a file to Upload")) {
						fc.utobj().clickElement(driver, ui.zipCodes_SpecifyFileToUpload_radioBtn);
						fc.utobj().sendKeys(driver, ui.zipCodes_SpecifyFileToUpload_chooseFileBtn,
								manageAreaRegion.getFile_zipCodes());
					}

					if (manageAreaRegion.getCountries() != null) {
						for (String country : manageAreaRegion.getCountries()) {
							if (country.equalsIgnoreCase("USA")) {
								fc.utobj().clickElement(driver, ui.countryUSA);
							}
						}
					}
				}
			}

				else if (manageAreaRegion.getGroupBy().equalsIgnoreCase("County")) {
					fc.utobj().selectDropDown(driver, ui.groupBy, manageAreaRegion.getGroupBy());

					if (manageAreaRegion.getCountries() != null) {
						for (String country : manageAreaRegion.getCountries()) {
							if (country.equalsIgnoreCase("USA")) {
								fc.utobj().clickElement(driver, ui.countryUSA);
							}
						}
					}

					/*
					 * if (manageAreaRegion.getCountries() != null) { for
					 * (String country : manageAreaRegion.getCountries()) {
					 * fc.utobj().clickElement(driver, ui.countries(driver,
					 * country)); } }
					 */

					if (manageAreaRegion.getStates() != null) {
						for (String state : manageAreaRegion.getStates()) {
							fc.utobj().clickElement(driver, ui.selectState(driver, state));
						}
					}

				}

				else if (manageAreaRegion.getGroupBy().equalsIgnoreCase("States")) {
					fc.utobj().selectDropDown(driver, ui.groupBy, manageAreaRegion.getGroupBy());

					if (manageAreaRegion.getCountries() != null) {
						for (String country : manageAreaRegion.getCountries()) {
							if (country.equalsIgnoreCase("USA")) {
								fc.utobj().clickElement(driver, ui.countryUSA);
							} else {
								fc.utobj().clickElement(driver, ui.select_Country_GroupByCounty(driver, country));
							}
						}
					}

					if (manageAreaRegion.getCategory().equalsIgnoreCase("Domestic")) {
						if (manageAreaRegion.getStates() != null) {
							for (String state : manageAreaRegion.getStates()) {
								fc.utobj().clickElement(driver, ui.states(driver, state));
							}
						}
					}
				}
			}
		}
	

	public void verify_addAreaRegion(ManageAreaRegion manageAreaRegion) throws Exception
	{
		fc.utobj().printTestStep("Verify Add Area Region");
		if(! fc.utobj().assertLinkText(driver, manageAreaRegion.getAreaRegionName()))
		{
			fc.utobj().throwsException("Verify Add Area Region : Failed");
		}
	}
	
	void fill_confirmation_clickNext(ManageAreaRegion manageAreaRegion) throws Exception
	{
		SelectFranchiseeAssignmentAreaRegionUI ui = new SelectFranchiseeAssignmentAreaRegionUI(driver);
		if(manageAreaRegion.getSelectFranchiseeAssignment() != null)
		{
			if(manageAreaRegion.getSelectFranchiseeAssignment().equalsIgnoreCase("Assign Franchisees later"))
			{
				fc.utobj().clickElement(driver, ui.assignFranchiseeslater_radioBtn);
			}
			if(manageAreaRegion.getSelectFranchiseeAssignment().equalsIgnoreCase("Automatically assign Franchisees falling in this Area / Region"))
			{
				fc.utobj().clickElement(driver, ui.AutomaticallyAssignFranchiseesFallingInThisAreaRegion_radioBtn);
			}
			if(manageAreaRegion.getSelectFranchiseeAssignment().equalsIgnoreCase("Manually assign each Franchisee to this Area / Region"))
			{
				fc.utobj().clickElement(driver, ui.ManuallyAssignEachFranchiseeToThisAreaRegion_radioBtn);
			}
		}
		fc.utobj().clickElement(driver, ui.NextBtn);
	}

}
