package com.builds.test.common;

import org.openqa.selenium.WebDriver;

import com.builds.uimaps.common.AddAreaRegionUI;
import com.builds.uimaps.common.AddDivisionUI;
import com.builds.uimaps.common.ManageAreaRegionUI;
import com.builds.uimaps.common.ManageDivisionUI;
import com.builds.utilities.FranconnectUtil;

public class ManageDivisionTest {
	
	WebDriver driver;
	FranconnectUtil fc = new FranconnectUtil();
	
	public ManageDivisionTest(WebDriver driver) {
		this.driver=driver;
	}
	
	public ManageDivisionTest addDivision(AddDivision division) throws Exception
	{
		
		ManageDivisionUI manageDivisionUI = new ManageDivisionUI(driver);
		AddDivisionUI addDivisionUI = new AddDivisionUI(driver);
		fill_addDivision(division);
		fc.utobj().clickElement(driver, addDivisionUI.submitBtn);
		
		return new ManageDivisionTest(driver);
	}
	
	void fill_addDivision(AddDivision division) throws Exception
	{	
		AddDivisionUI addDivisionUI = new AddDivisionUI(driver);
		
		if(division.getDivisionName() != null)
		{
			fc.utobj().sendKeys(driver, addDivisionUI.divisionName_textBox, division.getDivisionName());
		}
	}

}
