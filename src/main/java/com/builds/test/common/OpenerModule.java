package com.builds.test.common;

import com.builds.test.opener.Opener_Common;

public class OpenerModule {

	private Opener_Common openerObj = null;

	public Opener_Common opener_common() {
		if (openerObj == null) {
			openerObj = new Opener_Common();
		}
		return openerObj;
	}
}
