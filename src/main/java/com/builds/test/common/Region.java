package com.builds.test.common;

public class Region {

	private String category;
	private String areaRegionName;
	private String groupBy;
	private String countries;
	private String zipCodes;
	
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	public String getAreaRegionName() {
		return areaRegionName;
	}
	public void setAreaRegionName(String areaRegionName) {
		this.areaRegionName = areaRegionName;
	}
	public String getGroupBy() {
		return groupBy;
	}
	public void setGroupBy(String groupBy) {
		this.groupBy = groupBy;
	}
	public String getCountries() {
		return countries;
	}
	public void setCountries(String countries) {
		this.countries = countries;
	}
	public String getZipCodes() {
		return zipCodes;
	}
	public void setZipCodes(String zipCodes) {
		this.zipCodes = zipCodes;
	}
	
}
