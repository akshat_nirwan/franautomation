package com.builds.test.common;

public class RegionalUser extends User {

	private String Area_Region;
	private String associateThisUserWithChecklistItems;

	public String getArea_Region() {
		return Area_Region;
	}

	public void setArea_Region(String area_Region) {
		Area_Region = area_Region;
	}

	public String getAssociateThisUserWithChecklistItems() {
		return associateThisUserWithChecklistItems;
	}

	public void setAssociateThisUserWithChecklistItems(String associateThisUserWithChecklistItems) {
		this.associateThisUserWithChecklistItems = associateThisUserWithChecklistItems;
	}

}
