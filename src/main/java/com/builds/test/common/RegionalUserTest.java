package com.builds.test.common;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Test;

import com.builds.test.admin.AdminPageTest;
import com.builds.test.admin.AdminUsersManageCorporateUsersPageTest;
import com.builds.uimaps.common.CorporateUserUI;
import com.builds.utilities.FranconnectUtil;

public class RegionalUserTest {
	
	@Test
	private void addRegionalUser() throws Exception {
		FranconnectUtil fc = new FranconnectUtil();
		RegionalUser regionalUser = new RegionalUser();
		
		regionalUser = fc.commonMethods().fillDefaultDataFor_RegionalUser(regionalUser);
		
		System.out.println(regionalUser.getUserName());
		System.out.println(regionalUser.getFirstName());
		System.out.println(regionalUser.getLastName());

		WebDriver driver = null;
		try {
			createDefaultRegionalUser_Through_WebService_AfterLogin(driver, regionalUser);
		} catch (Exception e) {
			System.out.println("Error");
			// fc.utobj().quitBrowserOnCatch(driver, config, e, testCaseId);
		}
	}

	public RegionalUser createDefaultRegionalUser_Through_WebService_AfterLogin(WebDriver driver,
			RegionalUser regionalUser) throws Exception {
		FranconnectUtil fc = new FranconnectUtil();

		String responseMessage = null;

		String module = "admin";
		String subModule = "user";
		String isISODate = "no";

		fc.utobj().printTestStep("Adding Regional User through Web Services.");
		WebService rest = new WebService();

		String xmlString = "<fcRequest>" + "<adminUser>" + "<userID>" + regionalUser.getUserName() + "</userID>"
				+ "<password>" + regionalUser.getPassword() + "</password>" + "<userLevel>Regional</userLevel>"
				+ "<timezone>" + regionalUser.getTimezone() + "</timezone>" + "<type>" + regionalUser.getUserType() + "</type>"
				+ "<expirationTime>" + regionalUser.getExpiryDays() + "</expirationTime>" + "<role>" + regionalUser.getRole()
				+ "</role>" + "<jobTitle>" + regionalUser.getJobTitle() + "</jobTitle>" + "<region>" + regionalUser.getArea_Region() + "</region>" + "<userLanguage>English</userLanguage>" +  "<firstname>"
				+ regionalUser.getFirstName() + "</firstname>" + "<lastname>" + regionalUser.getLastName() + "</lastname>"
				+ "<address>" + regionalUser.getAddress() + "</address>" + "<city>" + regionalUser.getCity() + "</city>"
				+ "<state>" + regionalUser.getState() + "</state>" + "<zipCode>" + regionalUser.getZipcode() + "</zipCode>"
				+ "<country>" + regionalUser.getCountry() + "</country>" + "<phone1>" + regionalUser.getPhone1() + "</phone1>"
				+ "<phoneExt1>" + regionalUser.getPhoneExt1() + "</phoneExt1>" + "<phone2>" + regionalUser.getPhone2()
				+ "</phone2>" + "<phoneExt2>" + regionalUser.getPhoneExt2() + "</phoneExt2>" + "<fax>" + regionalUser.getFax()
				+ "</fax>" + "<mobile>" + regionalUser.getMobile() + "</mobile>" + "<email>" + regionalUser.getEmail()
				+ "</email>" + "<sendNotification>" + regionalUser.getSendNotification() + "</sendNotification>"
				+ "<loginUserIp>" + regionalUser.getLoginUserIp() + "</loginUserIp>" + "<isBillable>"
				+ regionalUser.getIsBillable() + "</isBillable>" + "<isDaylight>" + regionalUser.getIsDaylight()
				+ "</isDaylight>" + "<isAuditor>" + "y" + "</isAuditor>" + "</adminUser>"
				+ "</fcRequest>";

		try {
			responseMessage = rest.create(module, subModule, xmlString, isISODate);
		} catch (Exception e) {

		}
		
		System.out.println(responseMessage);
		
		/*
									 * verifyCorporateUser_ThroughWebService(
									 * corpUser);
									 */

	/*	if (!responseMessage.contains("success")) {

			AdminUsersManageCorporateUsersPageTest manageCorporate = new AdminUsersManageCorporateUsersPageTest();
			AdminPageTest admin = new AdminPageTest();
			admin.adminPage(driver);
			admin.openCorporateUserPage(driver);

			manageCorporate.addCorporateUsers(driver);

			CorporateUserUI ui = new CorporateUserUI(driver);
			Region

			if (corpUser.getUserName() != null) {
				fc.utobj().sendKeys(driver, ui.userName, corpUser.getUserName() + "1");
			}

			if (corpUser.getPassword() != null) {
				fc.utobj().sendKeys(driver, ui.password, corpUser.getPassword());
			}

			if (corpUser.getConfirmPassword() != null) {
				fc.utobj().sendKeys(driver, ui.confirmPassword, corpUser.getConfirmPassword());
			}

			if (corpUser.getConfirmPassword() != null) {
				fc.utobj().sendKeys(driver, ui.confirmPassword, corpUser.getConfirmPassword());
			}

			if (corpUser.getUserType() != null) {
				fc.utobj().selectDropDownByVisibleText(driver, ui.userType, corpUser.getUserType());
			}

			if (corpUser.getRole() != null) {
				fc.utobj().selectValFromMultiSelect(driver, ui.role_MultiSelect, corpUser.getRole());
			}

			if (corpUser.getTimezone() != null) {
				fc.utobj().selectDropDownByVisibleText(driver, ui.timezone_Select, corpUser.getTimezone());
			}

			if (corpUser.getIsDaylight() != null) {
				fc.utobj().check(ui.isDaylight_Check, corpUser.getIsDaylight());
			}

			if (corpUser.getJobTitle() != null) {
				fc.utobj().sendKeys(driver, ui.jobTitle, corpUser.getJobTitle());
			}

			if (corpUser.getUserLanguage() != null) {
				fc.utobj().selectDropDownByVisibleText(driver, ui.userLanguage, corpUser.getUserLanguage());
			}

			if (corpUser.getFirstName() != null) {
				fc.utobj().sendKeys(driver, ui.firstName, corpUser.getFirstName());
			}

			if (corpUser.getLastName() != null) {
				fc.utobj().sendKeys(driver, ui.lastName, corpUser.getLastName());
			}

			if (corpUser.getCity() != null) {
				fc.utobj().sendKeys(driver, ui.city, corpUser.getCity());
			}

			if (corpUser.getCountry() != null) {
				fc.utobj().selectDropDownByVisibleText(driver, ui.country, corpUser.getCountry());
			}

			if (corpUser.getState() != null) {
				fc.utobj().selectDropDownByVisibleText(driver, ui.state, corpUser.getState());
			}

			if (corpUser.getPhone1() != null) {
				fc.utobj().sendKeys(driver, ui.phone1, corpUser.getPhone1());
			}

			if (corpUser.getEmail() != null) {
				fc.utobj().sendKeys(driver, ui.phone1, corpUser.getEmail());
			}

			fc.utobj().clickElement(driver, ui.submit);

		}*/

		return regionalUser;
	}

}
