package com.builds.test.common;

import com.builds.test.fs.Sales_Common;

public class SalesModule {
	private Sales_Common fsmoduleObj = null;

	public Sales_Common sales_common() {
		if (fsmoduleObj == null) {
			fsmoduleObj = new Sales_Common();
		}
		return fsmoduleObj;
	}
}
