package com.builds.test.common;

import org.openqa.selenium.WebDriver;
import org.testng.Reporter;

import com.builds.test.thehub.Library;
import com.builds.test.thehub.News;
import com.builds.uimaps.common.Search;
import com.builds.utilities.FranconnectUtil;

public class SearchTest {

	FranconnectUtil fc = new FranconnectUtil();

	public void searchNewsItems(WebDriver driver, News news) throws Exception {

		try {
			Search search = new Search(driver);
			fc.adminpage().openSearch(driver);
			fc.utobj().clickElement(driver, search.newsTab);
			fc.utobj().sendKeys(driver, search.searchWords, news.getNewsItemTitle());
			fc.utobj().selectDropDown(driver, search.newsCategory, "News");
			fc.utobj().clickElement(driver, search.searchBtn);
		} catch (Exception e) {
			Reporter.log(e.getMessage().toString());
		}
	}

	public void searchLibraryDocument(WebDriver driver,Library library) throws Exception {

		try {
			Search search = new Search(driver);
			fc.adminpage().openSearch(driver);
			fc.utobj().clickElement(driver, search.documentTab);
			fc.utobj().clickElement(driver, search.documentTab);
			fc.utobj().sendKeys(driver, search.searchDocByWords, library.getDocumentTitle());
			fc.utobj().clickElement(driver, search.searchBtn);
		} catch (Exception e) {
			Reporter.log(e.getMessage().toString());
		}
	}
	
	public void searchLibraryDocumentBySearchAllTheHubDocumentSearchBox(WebDriver driver,Library library) throws Exception {

		try {
			Search search = new Search(driver);
			fc.adminpage().openSearch(driver);
			fc.utobj().sendKeys(driver, search.searchAllDocument, library.getDocumentTitle());
			fc.utobj().clickElement(driver, search.searchAllDocumentIcon);
		} catch (Exception e) {
			Reporter.log(e.getMessage().toString());
		}
	}
}
