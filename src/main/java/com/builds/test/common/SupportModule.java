package com.builds.test.common;

import com.builds.test.support.Support_Common;

public class SupportModule {
	private Support_Common supportobj = null;

	public Support_Common support_common() {
		if (supportobj == null) {
			supportobj = new Support_Common();
		}
		return supportobj;
	}
}
