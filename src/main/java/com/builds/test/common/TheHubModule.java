package com.builds.test.common;

import com.builds.test.thehub.TheHub_Common;

public class TheHubModule {
	private TheHub_Common hubobj = null;

	public TheHub_Common hub_common() {
		if (hubobj == null) {
			hubobj = new TheHub_Common();
		}
		return hubobj;
	}
}
