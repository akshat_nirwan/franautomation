package com.builds.test.common;

import com.builds.test.training.Training_Common;

public class TrainingModule {
	private Training_Common trainingobj = null;

	public Training_Common training_common() {
		if (trainingobj == null) {
			trainingobj = new Training_Common();
		}
		return trainingobj;
	}
}
