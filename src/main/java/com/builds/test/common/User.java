package com.builds.test.common;

public class User {

	private String userName;
	private String password;
	private String confirmPassword;
	private String userType;
	private String expiryDays;
	private String role;
	private String timezone;
	private String isDaylight;
	private String jobTitle;
	private String userLanguage;
	private String userFirstName;
	private String userLastName;
	private String address;
	private String city;
	private String country;
	private String zipcode;
	private String state;
	private String phone1;
	private String phoneExt1;
	private String phone2;
	private String phoneExt2;
	private String fax;
	private String mobile;
	private String email;
	private String loginUserIp;
	private String isBillable;
	private String auditor;
	private String userPictureName;
	private String sendNotification;
	private String consultant;

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getConfirmPassword() {
		return confirmPassword;
	}

	public void setConfirmPassword(String confirmPassword) {
		this.confirmPassword = confirmPassword;
	}

	public String getUserType() {
		return userType;
	}

	public void setUserType(String userType) {
		this.userType = userType;
	}

	public String getExpiryDays() {
		return expiryDays;
	}

	public void setExpiryDays(String expiryDays) {
		this.expiryDays = expiryDays;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	public String getTimezone() {
		return timezone;
	}

	public void setTimezone(String timezone) {
		this.timezone = timezone;
	}

	public String getIsDaylight() {
		return isDaylight;
	}

	public void setIsDaylight(String isDaylight) {
		this.isDaylight = isDaylight;
	}

	public String getJobTitle() {
		return jobTitle;
	}

	public void setJobTitle(String jobTitle) {
		this.jobTitle = jobTitle;
	}

	public String getUserLanguage() {
		return userLanguage;
	}

	public void setUserLanguage(String userLanguage) {
		this.userLanguage = userLanguage;
	}

	public String getFirstName() {
		return userFirstName;
	}

	public void setFirstName(String firstName) {
		this.userFirstName = firstName;
	}

	public String getLastName() {
		return userLastName;
	}

	public void setLastName(String lastName) {
		this.userLastName = lastName;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getZipcode() {
		return zipcode;
	}

	public void setZipcode(String zipcode) {
		this.zipcode = zipcode;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getPhone1() {
		return phone1;
	}

	public void setPhone1(String phone1) {
		this.phone1 = phone1;
	}

	public String getPhoneExt1() {
		return phoneExt1;
	}

	public void setPhoneExt1(String phoneExt1) {
		this.phoneExt1 = phoneExt1;
	}

	public String getPhone2() {
		return phone2;
	}

	public void setPhone2(String phone2) {
		this.phone2 = phone2;
	}

	public String getPhoneExt2() {
		return phoneExt2;
	}

	public void setPhoneExt2(String phoneExt2) {
		this.phoneExt2 = phoneExt2;
	}

	public String getFax() {
		return fax;
	}

	public void setFax(String fax) {
		this.fax = fax;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getLoginUserIp() {
		return loginUserIp;
	}

	public void setLoginUserIp(String loginUserIp) {
		this.loginUserIp = loginUserIp;
	}

	public String getIsBillable() {
		return isBillable;
	}

	public void setIsBillable(String isBillable) {
		this.isBillable = isBillable;
	}

	public String getAuditor() {
		return auditor;
	}

	public void setAuditor(String auditor) {
		this.auditor = auditor;
	}

	public String getUserPictureName() {
		return userPictureName;
	}

	public void setUserPictureName(String userPictureName) {
		this.userPictureName = userPictureName;
	}

	public String getSendNotification() {
		return sendNotification;
	}

	public void setSendNotification(String sendNotification) {
		this.sendNotification = sendNotification;
	}

	public String getConsultant() {
		return consultant;
	}

	public void setConsultant(String consultant) {
		this.consultant = consultant;
	}

	public String getuserFullName() {
		String firstName = getFirstName();
		String lastName = getLastName();

		if (firstName == null) {
			firstName = "";
		}
		if (lastName == null) {
			lastName = "";
		}
		return (firstName + " " + lastName).trim();
	}

}
