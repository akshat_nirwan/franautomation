package com.builds.test.common;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import com.builds.utilities.FranconnectUtil;

/**
 * @author Rohit
 * 
 *         Created on 13-02-2018
 * 
 */

public class WebService {

	private String updatedPOST_URL_Create;
	private String updatedPOST_URL_Retrieve;
	private String updatePOST_URL_Update;

	private static final String USER_AGENT = "Mozilla/5.0";
	private static final String POST_PARAMS = "userName=FRANC0NN3CT_API";

	public WebService() throws Exception {

		String buildUrl = FranconnectUtil.config.get("buildUrl");
		System.out.println(buildUrl);
		if (buildUrl.endsWith("//") || buildUrl.endsWith("/") || buildUrl.endsWith("\\")) {
			// Do Nothing
		} else {
			buildUrl = buildUrl + "/";
		}

		updatedPOST_URL_Create = buildUrl + "rest/dataservices/create";
		updatedPOST_URL_Retrieve = buildUrl + "rest/dataservices/retrieve";
		updatePOST_URL_Update = buildUrl + "rest/dataservices/update";
	}

	public String create(String module, String subModule, String xmlString, String isISODate) throws Exception {
		FranconnectUtil fc = new FranconnectUtil();

		System.out.println("++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
		try {

			System.out.println(updatedPOST_URL_Create);
			URL obj = new URL(updatedPOST_URL_Create);
			HttpURLConnection con = (HttpURLConnection) obj.openConnection();
			con.setRequestMethod("POST");
			con.setRequestProperty("User-Agent", USER_AGENT);

			String clientCode = "clientCode=FRANC0NN3CT_API&&key=pHNsc6x5mBksJDfo&&fromWhere=jsp&&responseType=xml&&"
					+ "module=" + module + "&&subModule=" + subModule + "&&xmlString=" + xmlString
					+ "&&operation=create&&isISODate=no&&roleTypeForQuery=-1";

			System.out.println(clientCode);

			con.setDoOutput(true);
			OutputStream os = con.getOutputStream();
			os.write(clientCode.getBytes());
			os.write(POST_PARAMS.getBytes());

			os.flush();
			os.close();

			int responseCode = con.getResponseCode();

			if (responseCode == HttpURLConnection.HTTP_OK) { // success
				BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
				String inputLine;
				StringBuffer response = new StringBuffer();
				while ((inputLine = in.readLine()) != null) {
					response.append(inputLine);
				}
				in.close();
				return response.toString();
			} else {
				fc.utobj().throwsSkipException("WebService is not working." + "Response Code : " + responseCode);
			}
			if (responseCode == HttpURLConnection.HTTP_OK) { // success
			} else {
				fc.utobj().throwsSkipException("WebService is not working." + "Response Code : " + responseCode);
			}
		} catch (Exception e) {
			System.out.println(e.getMessage());
			fc.utobj().throwsSkipException("WebService is not working." + e.getMessage());
		}
	
		return "";

	}

	public String retrieve(String module, String subModule, String xmlString, String isISODate) throws Exception {
		FranconnectUtil fc = new FranconnectUtil();

		try {
			URL obj = new URL(updatedPOST_URL_Retrieve);
			HttpURLConnection con = (HttpURLConnection) obj.openConnection();
			con.setRequestMethod("POST");
			con.setRequestProperty("User-Agent", USER_AGENT);

			String clientCode = "clientCode=FRANC0NN3CT_API&&key=pHNsc6x5mBksJDfo&&fromWhere=jsp&&responseType=xml&&module="
					+ module + "&&subModule=" + subModule + "&&filterXML=" + xmlString + "&&isISODate=" + isISODate
					+ "&&roleTypeForQuery=-1";

			System.out.println("Client Code :============== " + clientCode);

			con.setDoOutput(true);
			OutputStream os = con.getOutputStream();
			os.write(clientCode.getBytes());
			os.write(POST_PARAMS.getBytes());

			os.flush();
			os.close();

			int responseCode = con.getResponseCode();

			if (responseCode == HttpURLConnection.HTTP_OK) { // success
				BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
				String inputLine;
				StringBuffer response = new StringBuffer();

				while ((inputLine = in.readLine()) != null) {
					response.append(inputLine);
				}
				in.close();

				return response.toString();
			} else {
				fc.utobj().throwsSkipException("WebService is not working." + "Response Code : " + responseCode);
			}

			if (responseCode == HttpURLConnection.HTTP_OK) { // success
			} else {
				fc.utobj().throwsSkipException("WebService is not working." + "Response Code : " + responseCode);
			}
		} catch (Exception e) {
			fc.utobj().throwsSkipException("WebService is not working." + e.getMessage());
		}

		return "";
	}
	
	public String update(String module, String subModule, String xmlString, String isISODate) throws Exception
	{
		FranconnectUtil fc = new FranconnectUtil();
		
		try{
			URL obj = new URL(updatePOST_URL_Update);
			HttpURLConnection con = (HttpURLConnection) obj.openConnection();
			con.setRequestMethod("POST");
			con.setRequestProperty("User-Agent", USER_AGENT);

			String clientCode = "clientCode=FRANC0NN3CT_API&&key=pHNsc6x5mBksJDfo&&fromWhere=jsp&&responseType=xml&&module="
					+ module + "&&subModule=" + subModule + "&&xmlString=" + xmlString + "&&isISODate=" + isISODate;

			System.out.println("Client Code :============== " + clientCode);
			
			con.setDoOutput(true);
			OutputStream os = con.getOutputStream();
			os.write(clientCode.getBytes());
			os.write(POST_PARAMS.getBytes());

			os.flush();
			os.close();

			int responseCode = con.getResponseCode();

			if (responseCode == HttpURLConnection.HTTP_OK) { // success
				BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
				String inputLine;
				StringBuffer response = new StringBuffer();

				while ((inputLine = in.readLine()) != null) {
					response.append(inputLine);
					
				}
				in.close();

				return response.toString();
			} else {
				fc.utobj().throwsSkipException("WebService is not working." + "Response Code : " + responseCode);
			}

			if (responseCode == HttpURLConnection.HTTP_OK) { // success
			} else {
				fc.utobj().throwsSkipException("WebService is not working." + "Response Code : " + responseCode);
			}
		}
		catch (Exception e) {
			fc.utobj().throwsSkipException("WebService is not working." + e.getMessage());
		}

		return "";

			
				}
}
