package com.builds.test.crm;

import org.openqa.selenium.WebDriver;

import com.builds.uimaps.crm.LeadUI;
import com.builds.utilities.FranconnectUtil;

public class Addleadpage {

	FranconnectUtil fc = new FranconnectUtil();

	public Addleadpage fillLeadDetails(WebDriver driver, Leadgetset lead) throws Exception {
		LeadUI leadui = new LeadUI(driver);
		if (lead.getTitle() != null) {

			fc.utobj().sendKeys(driver, leadui.title, lead.getTitle());
		}

		if (lead.getFirstname() != null) {

			fc.utobj().sendKeys(driver, leadui.firstname, lead.getFirstname());
		}

		if (lead.getLastname() != null) {
			fc.utobj().sendKeys(driver, leadui.lastname, lead.getLastname());
		}
		if (lead.getPrimarycontact() != null) {

			fc.utobj().selectDropDown(driver, leadui.primarycontact, lead.getPrimarycontact());
		}
		if (lead.getCompnayname() != null) {

			fc.utobj().sendKeys(driver, leadui.compnayname, lead.getCompnayname());
		}
		if (lead.getAssign() != null) {
			fc.utobj().sendKeys(driver, leadui.assign, lead.getAssign());
		}
		if (lead.getOwner() != null) {
			fc.utobj().selectDropDown(driver, leadui.owner, lead.getOwner());
		}
		if (lead.getAddress() != null) {
			fc.utobj().sendKeys(driver, leadui.address, lead.getAddress());
		}
		if (lead.getCity() != null) {
			fc.utobj().sendKeys(driver, leadui.city, lead.getCity());
		}
		if (lead.getCountry() != null) {
			fc.utobj().selectDropDown(driver, leadui.country, lead.getCountry());                                    
		}
		if (lead.getState() != null) {
			fc.utobj().selectDropDown(driver, leadui.state, lead.getState());
		}
		if (lead.getZipcode() != null) {
			fc.utobj().sendKeys(driver, leadui.zipcode, lead.getZipcode());
		}
		if (lead.getPhonenumber() != null) {
			fc.utobj().sendKeys(driver, leadui.phonenumber, lead.getPhonenumber());
		}
		if (lead.getExtension() != null) {
			fc.utobj().sendKeys(driver, leadui.extension, lead.getExtension());
		}
		if (lead.getFax() != null) {
			fc.utobj().sendKeys(driver, leadui.fax, lead.getFax());
		}
		if (lead.getMobile() != null) {
			fc.utobj().sendKeys(driver, leadui.mobile, lead.getMobile());
		}
		if (lead.getEmail() != null) {
			fc.utobj().sendKeys(driver, leadui.email, lead.getEmail());
		}
		if (lead.getAlternarteemail() != null) {
			fc.utobj().sendKeys(driver, leadui.alternarteemail, lead.getAlternarteemail());
		}
		if (lead.getSuffix() != null) {
			fc.utobj().sendKeys(driver, leadui.suffix, lead.getSuffix());
		}
		if (lead.getJobtitle() != null) {
			fc.utobj().sendKeys(driver, leadui.jobtitle, lead.getJobtitle());
		}

		if (lead.getBirthday() != null) {
			fc.utobj().sendKeys(driver, leadui.birthday, lead.getBirthday());
		}

		if (lead.getAnniversary() != null) {
			fc.utobj().sendKeys(driver, leadui.anniversary, lead.getAnniversary());
		}

		if (lead.getRating() != null) {
			fc.utobj().selectDropDownByPartialText(driver, leadui.Rating, lead.getRating());
		}
		if (lead.getSource() != null) {
			fc.utobj().selectDropDown(driver, leadui.source, lead.getSource());
		}
		if (lead.getSource1() != null) {
			try {
				fc.utobj().selectDropDown(driver, leadui.source1, lead.getSource1());
			} catch (Exception e) {
				fc.utobj().selectDropDownByIndex(driver, leadui.source1, 1);
			}
		}
		if (lead.getContactMedium() != null) {
			fc.utobj().selectDropDown(driver, leadui.contactMedium, lead.getContactMedium());
		}
		if (lead.getBesttime() != null) {
			fc.utobj().sendKeys(driver, leadui.Besttime, lead.getBesttime());
		}
		if (lead.getComment() != null) {
			fc.utobj().sendKeys(driver, leadui.comment, lead.getComment());
		}
		return this;

	}

	public void save(WebDriver driver) throws Exception {
		fc.utobj().printTestStep("Click on Add Lead save button");
		LeadUI leadui = new LeadUI(driver);
		fc.utobj().clickElement(driver, leadui.submit);

	}

	public void reset(WebDriver driver) throws Exception {
		fc.utobj().printTestStep("Click on Add Lead reset button");
		LeadUI leadui = new LeadUI(driver);
		fc.utobj().clickElement(driver, leadui.Reset);
	}

	public void Back(WebDriver driver) throws Exception {
		fc.utobj().printTestStep("Click on Add Lead Back button");
		LeadUI leadui = new LeadUI(driver);
		fc.utobj().clickElement(driver, leadui.Backbutton);
	}

	public void saveconvert(WebDriver driver) throws Exception {
		fc.utobj().printTestStep("Click on save and convert lead button");
		LeadUI leadui = new LeadUI(driver);
		fc.utobj().clickElement(driver, leadui.SaveAndConvertLead);
	}

	public void saveAndAnother(WebDriver driver) throws Exception {
		fc.utobj().printTestStep("Click on save and add another lead button");
		LeadUI leadui = new LeadUI(driver);
		fc.utobj().clickElement(driver, leadui.Save_And_Add_Another_Lead);
	}

	public void Groupoption_onAddlead(WebDriver driver,String value) throws Exception
	{
		fc.utobj().printTestStep("click on group option");
		LeadUI leadui = new LeadUI(driver);
		fc.utobj().clickElement(driver, leadui.Group);
		fc.utobj().printTestStep("search group name");
		fc.utobj().sendKeys(driver, leadui.Searchtab_insideGroup, value);
		Thread.sleep(2000);
		fc.utobj().clickElement(driver, leadui.Searchbtn_insideGroup);
		Thread.sleep(2000);
		fc.utobj().clickElement(driver, leadui.checkbox_insideGroup);
		
	}
	
}
