package com.builds.test.crm;

import java.util.Map;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Test;

import com.builds.uimaps.crm.AdminCRMConfigureCampaignEmailCategoryPage;
import com.builds.utilities.FranconnectUtil;
import com.builds.utilities.TestCase;

public class AdminCRMConfigureCampaignEmailCategoryPageTest {
	FranconnectUtil fc = new FranconnectUtil();

	@Test(groups = {"crm","errorpages1"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Add Category At Admin > CRM > Configure Campaign Email Category", testCaseId = "TC_35_Add_Category")
	public void addCategory() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			AdminCRMConfigureCampaignEmailCategoryPage pobj = new AdminCRMConfigureCampaignEmailCategoryPage(driver);
			fc.utobj().printTestStep("Navigate To Admin > CRM > Configure Campaign Email Category");
			fc.crm().crm_common().adminCRMConfigureCampaignEmailCategoryLnk(driver);
			fc.utobj().printTestStep("Add Category");
			fc.utobj().clickElement(driver, pobj.addCategoty);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			String categoryName = fc.utobj().generateTestData(dataSet.get("categoryName"));
			fc.utobj().sendKeys(driver, pobj.categoryName, categoryName);
			fc.utobj().clickElement(driver, pobj.addBtn);
			fc.utobj().clickElement(driver, pobj.closeBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Verify Category");

			boolean isTextPresent = fc.utobj().assertPageSource(driver, categoryName);
			if (isTextPresent == false) {
				fc.utobj().throwsException(
						"was not able to add category :: Admin > CRM > Configure Campaign Email Category");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	public void addCategory(WebDriver driver, String categoryName) throws Exception {

		String testCaseId = "TC_Add_Category_CRM";

		if (fc.utobj().validate(testCaseId)) {

			try {
				AdminCRMConfigureCampaignEmailCategoryPage pobj = new AdminCRMConfigureCampaignEmailCategoryPage(
						driver);
				fc.crm().crm_common().adminCRMConfigureCampaignEmailCategoryLnk(driver);
				fc.utobj().clickElement(driver, pobj.addCategoty);
				fc.commonMethods().switch_cboxIframe_frameId(driver);
				fc.utobj().sendKeys(driver, pobj.categoryName, categoryName);
				fc.utobj().clickElement(driver, pobj.addBtn);
				fc.utobj().clickElement(driver, pobj.closeBtn);
				fc.utobj().switchFrameToDefault(driver);

				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);

			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException("Was Not able to Add Category");
		}

	}

	@Test(groups = {"crm","errorpages1"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Modify Category At Admin > CRM > Configure Campaign Email Category", testCaseId = "TC_36_Modify_Category")
	public void modifyCategory() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			AdminCRMConfigureCampaignEmailCategoryPage pobj = new AdminCRMConfigureCampaignEmailCategoryPage(driver);
			fc.utobj().printTestStep("Navigate To Admin > CRM > Configure Campaign Email Category");
			fc.utobj().printTestStep("Add Category");
			String categoryName = fc.utobj().generateTestData(dataSet.get("categoryName"));
			addCategory(driver, categoryName);
			fc.utobj().printTestStep("Modify Category");
			fc.utobj().actionImgOption(driver, categoryName, "Modify");
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			categoryName = fc.utobj().generateTestData(dataSet.get("categoryName"));
			fc.utobj().sendKeys(driver, pobj.categoryName, categoryName);
			fc.utobj().clickElement(driver, pobj.modifyBtn);
			fc.utobj().clickElement(driver, pobj.closeBtn);
			fc.utobj().switchFrameToDefault(driver);
			fc.utobj().printTestStep("Verify Modify Category");

			boolean isTextPresent = fc.utobj().assertPageSource(driver, categoryName);
			if (isTextPresent == false) {
				fc.utobj().throwsException("was not able to Modify category");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = {"crm","errorpages1"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Delete Category At Admin > CRM > Configure Campaign Email Category", testCaseId = "TC_37_Delete_Category")
	public void deleteCategory() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			fc.utobj().printTestStep("Navigate To Admin > CRM > Configure Campaign Email Category");
			fc.utobj().printTestStep("Add Category");
			String categoryName = fc.utobj().generateTestData(dataSet.get("categoryName"));
			addCategory(driver, categoryName);
			fc.utobj().printTestStep("Delete Category");
			fc.utobj().actionImgOption(driver, categoryName, "Delete");
			fc.utobj().acceptAlertBox(driver);

			fc.utobj().printTestStep("Verify Category");

			boolean isTextPresent = fc.utobj().assertPageSource(driver, categoryName);
			if (isTextPresent == true) {
				fc.utobj().throwsException("was not able to Delete category");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}
}
