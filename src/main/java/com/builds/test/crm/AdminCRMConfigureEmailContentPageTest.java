package com.builds.test.crm;

import java.util.List;
import java.util.Map;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import com.builds.uimaps.crm.AdminCRMConfigureEmailContentPage;
import com.builds.utilities.FranconnectUtil;
import com.builds.utilities.TestCase;

public class AdminCRMConfigureEmailContentPageTest {
	FranconnectUtil fc = new FranconnectUtil();

	@Test(groups = {"crm","errorpages1"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2017-06-28", testCaseDescription = "Verify The Configure Email Content For Lead Email At Admin > CRM > Configure Email Content", testCaseId = "TC_31_Configure_Email_Content_Lead")
	public void configureEmailContentForLead() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			AdminCRMConfigureEmailContentPage pobj = new AdminCRMConfigureEmailContentPage(driver);
			fc.utobj().printTestStep("Navigate To Admin > CRM > Configure Email Content");
			fc.crm().crm_common().adminCRMConfigureEmailContentLnk(driver);
			fc.utobj().printTestStep("Configure Email Content For Lead");
			fc.utobj().clickElement(driver, pobj.leads);
			if (!fc.utobj().isSelected(driver, pobj.sendEmailNotification)) {
				fc.utobj().clickElement(driver, pobj.sendEmailNotification);
			}
			fc.utobj().sendKeys(driver, pobj.fromMail, dataSet.get("fromMail"));
			String emailContent = dataSet.get("emailContent") + fc.utobj().generateRandomNumber();
			fc.utobj().sendKeys(driver, pobj.emailContent, emailContent);
			fc.utobj().clickElement(driver, pobj.emailFields);
			if (!fc.utobj().isSelected(driver, pobj.selectAll)) {
				fc.utobj().clickElement(driver, pobj.selectAll);
			}
			fc.utobj().clickElement(driver, pobj.emailFields);
			fc.utobj().clickElement(driver, pobj.saveBtn);
			fc.utobj().printTestStep("Verify COnfigure Email Content For Lead Email");

			if (!pobj.emailContent.getText().trim().equalsIgnoreCase(emailContent)) {
				fc.utobj().throwsException("was not able to configure Email Content For Leads");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = {"crm","errorpages1"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Preview Email Content For Lead Email At Admin > CRM > Configure Email Content", testCaseId = "TC_32_Preview_Email_Content_Lead")
	public void previewEmailContentForLead() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			AdminCRMConfigureEmailContentPage pobj = new AdminCRMConfigureEmailContentPage(driver);
			fc.utobj().printTestStep("Navigate To Admin > CRM > Configure Email Content");
			fc.crm().crm_common().adminCRMConfigureEmailContentLnk(driver);
			fc.utobj().printTestStep("Configure Email Content For Lead");
			fc.utobj().clickElement(driver, pobj.leads);
			if (!fc.utobj().isSelected(driver, pobj.sendEmailNotification)) {
				fc.utobj().clickElement(driver, pobj.sendEmailNotification);
			}
			fc.utobj().sendKeys(driver, pobj.fromMail, dataSet.get("fromMail"));
			String emailContent = dataSet.get("emailContent") + fc.utobj().generateRandomNumber();
			fc.utobj().sendKeys(driver, pobj.emailContent, emailContent);
			fc.utobj().setToDefault(driver, pobj.emailFields);
			fc.utobj().selectValFromMultiSelect(driver, pobj.emailFields, "Lead Type");
			fc.utobj().clickElement(driver, pobj.saveBtn);
			fc.utobj().printTestStep("Verify Preview Email Contenet For Lead");

			fc.utobj().clickElement(driver, pobj.previewBtn);
			fc.commonMethods().switch_cboxIframe_frameId(driver);

			List<WebElement> elemntList = driver.findElements(By.xpath(".//*[@id='intro']//p"));
			boolean status = false;

			for (int i = 0; i < elemntList.size(); i++) {
				String text = elemntList.get(i).getText().trim();

				if (text.equalsIgnoreCase(emailContent)) {
					status = true;
					break;
				}
			}

			if (status == false) {
				fc.utobj().throwsException("was not able to verify Email Content");
			}

			fc.utobj().isTextDisplayed(driver, "Lead Type", "was not able to verify Email Fields");
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "crm", "crmconfemail","errorpages1" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2017-06-28", testCaseDescription = "Verify The Configure Email Content For Contacts Email At Admin > CRM > Configure Email Content", testCaseId = "TC_33_Configure_Email_Content_Contacts")
	public void configureEmailContentForContacts() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			AdminCRMConfigureEmailContentPage pobj = new AdminCRMConfigureEmailContentPage(driver);
			fc.utobj().printTestStep("Navigate To Admin > CRM > Configure Email Content");
			fc.crm().crm_common().adminCRMConfigureEmailContentLnk(driver);
			fc.utobj().printTestStep("Configure Email Content For Contect");
			fc.utobj().clickElement(driver, pobj.contacts);
			if (!fc.utobj().isSelected(driver, pobj.sendEmailNotification)) {
				fc.utobj().clickElement(driver, pobj.sendEmailNotification);
			}
			fc.utobj().sendKeys(driver, pobj.fromMail, dataSet.get("fromMail"));
			String emailContent = dataSet.get("emailContent") + fc.utobj().generateRandomNumber();
			fc.utobj().sendKeys(driver, pobj.emailContent, emailContent);
			fc.utobj().clickElement(driver, pobj.emailFields);
			if (!fc.utobj().isSelected(driver, pobj.selectAll)) {
				fc.utobj().clickElement(driver, pobj.selectAll);
			}
			fc.utobj().clickElement(driver, pobj.emailFields);
			fc.utobj().clickElement(driver, pobj.saveBtn);

			fc.utobj().printTestStep("Verify Configure Email Content For Contact");

			if (!pobj.emailContent.getText().trim().equalsIgnoreCase(emailContent)) {
				fc.utobj().throwsException("was not able to configure Email Content For Contacts");
			}
			
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "crm", "TC_34_Preview_Email_Content_Contacts","errorpages1" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Preview Email Content For Contacts Email :: Admin > CRM > Configure Email Content", testCaseId = "TC_34_Preview_Email_Content_Contacts")
	public void previewEmailContentForContacts() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			AdminCRMConfigureEmailContentPage pobj = new AdminCRMConfigureEmailContentPage(driver);
			fc.utobj().printTestStep("Navigate To Admin > CRM > Configure Email Content");
			fc.crm().crm_common().adminCRMConfigureEmailContentLnk(driver);
			fc.utobj().printTestStep("Configure Email Content For Contect");
			fc.utobj().clickElement(driver, pobj.contacts);
			if (!fc.utobj().isSelected(driver, pobj.sendEmailNotification)) {
				fc.utobj().clickElement(driver, pobj.sendEmailNotification);
			}
			fc.utobj().sendKeys(driver, pobj.fromMail, dataSet.get("fromMail"));
			String emailContent = dataSet.get("emailContent") + fc.utobj().generateRandomNumber();
			fc.utobj().sendKeys(driver, pobj.emailContent, emailContent);
			fc.utobj().setToDefault(driver, pobj.emailFields);
			fc.utobj().selectValFromMultiSelect(driver, pobj.emailFields, "Contact Type");
			fc.utobj().clickElement(driver, pobj.saveBtn);
			fc.utobj().clickElement(driver, pobj.previewBtn);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().printTestStep("Verify Preview Of Configure Email Content For Contect");
			List<WebElement> elemntList = driver.findElements(By.xpath(".//*[@id='intro']//p"));
			boolean status = false;

			for (int i = 0; i < elemntList.size(); i++) {
				String text = elemntList.get(i).getText().trim();

				if (text.equalsIgnoreCase(emailContent)) {
					status = true;
					break;
				}
			}

			if (status == false) {
				fc.utobj().throwsException("was not able to verify Email Content");
			}

			fc.utobj().isTextDisplayed(driver, "Contact Type", "was not able to verify Email Fields");
			fc.utobj().switchFrameToDefault(driver);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}

	}

	public void configureAllEmailContentLead(WebDriver driver) throws Exception {
		AdminCRMConfigureEmailContentPage pobj = new AdminCRMConfigureEmailContentPage(driver);
		fc.crm().crm_common().adminCRMConfigureEmailContentLnk(driver);
		fc.utobj().clickElement(driver, pobj.leads);
		if (!fc.utobj().isSelected(driver, pobj.sendEmailNotification)) {
			fc.utobj().clickElement(driver, pobj.sendEmailNotification);
		}
		fc.utobj().sendKeys(driver, pobj.fromMail, "support@franconnect.com");
		String emailContent = "A New Lead has been added with the following details:"
				+ fc.utobj().generateRandomNumber();
		fc.utobj().sendKeys(driver, pobj.emailContent, emailContent);
		fc.utobj().clickElement(driver, pobj.emailFields);
		if (!fc.utobj().isSelected(driver, pobj.selectAll)) {
			fc.utobj().clickElement(driver, pobj.selectAll);
		}
		fc.utobj().clickElement(driver, pobj.emailFields);
		fc.utobj().clickElement(driver, pobj.saveBtn);

		if (!pobj.emailContent.getText().trim().equalsIgnoreCase(emailContent)) {
			fc.utobj().throwsException("Not able verify Email content");
		}
	}

	public void configureAllEmailContentContact(WebDriver driver) throws Exception {
		AdminCRMConfigureEmailContentPage pobj = new AdminCRMConfigureEmailContentPage(driver);
		fc.crm().crm_common().adminCRMConfigureEmailContentLnk(driver);
		fc.utobj().clickElement(driver, pobj.contacts);
		if (!fc.utobj().isSelected(driver, pobj.sendEmailNotification)) {
			fc.utobj().clickElement(driver, pobj.sendEmailNotification);
		}
		fc.utobj().sendKeys(driver, pobj.fromMail, "support@franconnect.com");
		String emailContent = "A New Lead has been added with the following details:"
				+ fc.utobj().generateRandomNumber();
		fc.utobj().sendKeys(driver, pobj.emailContent, emailContent);
		
		fc.utobj().clickElement(driver, pobj.emailFields);
		if (!fc.utobj().isSelected(driver, pobj.selectAll)) {
			fc.utobj().clickElement(driver, pobj.selectAll);
		}
		fc.utobj().clickElement(driver, pobj.emailFields);
		fc.utobj().clickElement(driver, pobj.saveBtn);
		if (!pobj.emailContent.getText().trim().equalsIgnoreCase(emailContent)) {
			fc.utobj().throwsException("was not able to configure Email Content For Leads");
		}
	}
}
