package com.builds.test.crm;

import java.util.List;
import java.util.Map;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import com.builds.uimaps.crm.AdminCRMConfigureIndustryPage;
import com.builds.utilities.FranconnectUtil;
import com.builds.utilities.TestCase;

public class AdminCRMConfigureIndustryPageTest {
	FranconnectUtil fc = new FranconnectUtil();

	@Test(groups = "crm")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Add Industry At Admin > CRM > Configure Industry", testCaseId = "TC_25_Add_Industry")
	public void addIndustry() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			AdminCRMConfigureIndustryPage pobj = new AdminCRMConfigureIndustryPage(driver);

			fc.utobj().printTestStep("Navigating to Admin > CRM > Configure Industry Type");
			fc.utobj().printTestStep("Add Industry");
			fc.crm().crm_common().adminCRMConfigureIndustryLnk(driver);
			fc.utobj().clickElement(driver, pobj.addIndustry);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			String industry = fc.utobj().generateTestData(dataSet.get("industry"));
			fc.utobj().sendKeys(driver, pobj.indusrty, industry);
			String description = fc.utobj().generateTestData(dataSet.get("description"));
			fc.utobj().sendKeys(driver, pobj.description, description);
			fc.utobj().clickElement(driver, pobj.addBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Verify The Add Industry");
			nextPage(driver, industry, "was not able to add Industry");

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	public void addIndustry(WebDriver driver, String industry, String description) throws Exception {

		String testCaseId = "TC_Add_Industry";

		if (fc.utobj().validate(testCaseId)) {

			try {
				AdminCRMConfigureIndustryPage pobj = new AdminCRMConfigureIndustryPage(driver);

				fc.crm().crm_common().adminCRMConfigureIndustryLnk(driver);
				fc.utobj().clickElement(driver, pobj.addIndustry);
				fc.commonMethods().switch_cboxIframe_frameId(driver);
				fc.utobj().sendKeys(driver, pobj.indusrty, industry);
				fc.utobj().sendKeys(driver, pobj.description, description);
				fc.utobj().clickElement(driver, pobj.addBtn);
				fc.utobj().switchFrameToDefault(driver);

				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
			} catch (Exception e) {

				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}

		} else {
			fc.utobj().throwsSkipException("Was not able to add Indusrty At Admin > CRM > Configure Industry");
		}
	}

	@Test(groups = "crm")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Modify Industry At Admin > CRM > Configure Industry", testCaseId = "TC_26_Modify_Industry")
	public void modifyIndustry() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			AdminCRMConfigureIndustryPage pobj = new AdminCRMConfigureIndustryPage(driver);

			fc.utobj().printTestStep("Navigating to Admin > CRM > Configure Industry Type");
			fc.utobj().printTestStep("Add Industry");
			String industry = fc.utobj().generateTestData(dataSet.get("industry"));
			String description = fc.utobj().generateTestData(dataSet.get("description"));
			addIndustry(driver, industry, description);

			fc.utobj().printTestStep("Modify Industry");
			boolean status = false;
			boolean status1 = false;
			for (int i = 0; i < 100; i++) {

				List<WebElement> listElements = driver
						.findElements(By.xpath(".//*[contains(text () ,'" + industry + "')]"));

				if (listElements.size() > 0) {
					status = true;
					break;
				} else {
					status = false;
				}

				if (status == false) {

					try {
						fc.utobj().clickElement(driver, pobj.nextPage);
						status1 = true;
					} catch (Exception e) {
						status1 = false;
					}

					if (status1 == false) {
						break;
					}
				}
			}

			if (status == true) {
				fc.utobj().actionImgOption(driver, industry, "Modify");
				fc.commonMethods().switch_cboxIframe_frameId(driver);
				industry = fc.utobj().generateTestData(dataSet.get("industry"));
				fc.utobj().sendKeys(driver, pobj.indusrty, industry);
				description = fc.utobj().generateTestData(dataSet.get("description"));
				fc.utobj().sendKeys(driver, pobj.description, description);
				fc.utobj().clickElement(driver, pobj.saveBtn);
				fc.utobj().switchFrameToDefault(driver);
			}

			fc.utobj().printTestStep("Verify The Modify Industry");
			nextPage(driver, industry, "was not able to Modify Indusrty Type");

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "crm")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Delete Industry At Admin > CRM > Configure Industry", testCaseId = "TC_27_Delete_Industry")
	public void deleteIndustry() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			fc.utobj().printTestStep("Navigating to Admin > CRM > Configure Industry Type");
			fc.utobj().printTestStep("Add Industry");
			AdminCRMConfigureIndustryPage pobj = new AdminCRMConfigureIndustryPage(driver);
			String industry = fc.utobj().generateTestData(dataSet.get("industry"));
			String description = fc.utobj().generateTestData(dataSet.get("description"));
			addIndustry(driver, industry, description);

			fc.utobj().printTestStep("Delete Industry");

			boolean status = false;
			boolean status1 = false;
			for (int i = 0; i < 100; i++) {

				List<WebElement> listElements = driver
						.findElements(By.xpath(".//*[contains(text () ,'" + industry + "')]"));

				if (listElements.size() > 0) {
					status = true;
					break;
				} else {
					status = false;
				}

				if (status == false) {

					try {
						fc.utobj().clickElement(driver, pobj.nextPage);
						status1 = true;
					} catch (Exception e) {
						status1 = false;
					}

					if (status1 == false) {
						break;
					}
				}
			}

			if (status == true) {
				fc.utobj().actionImgOption(driver, industry, "Delete");
				fc.utobj().acceptAlertBox(driver);
			}

			fc.utobj().printTestStep("Verify The Delete Industry");
			nextPage(driver, industry, "was not able to Delete Indusrty Type");

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	public void nextPage(WebDriver driver, String industry, String exceptionMsg) throws Exception {

		AdminCRMConfigureIndustryPage pobj = new AdminCRMConfigureIndustryPage(driver);

		boolean status = false;
		boolean status1 = false;

		for (int i = 0; i < 100; i++) {

			List<WebElement> listElements = driver
					.findElements(By.xpath(".//*[contains(text () ,'" + industry + "')]"));

			if (listElements.size() > 0) {
				status = true;
				break;
			} else {
				status = false;
			}

			if (status == false) {

				try {
					fc.utobj().clickElement(driver, pobj.nextPage);
					status1 = true;
				} catch (Exception e) {
					status1 = false;
				}

				if (status1 == false) {
					break;
				}
			}
		}

		if (exceptionMsg.contains("Delete")) {

			if (status1 == false && status == true) {
				fc.utobj().throwsException(exceptionMsg);
			}

		} else {
			if (status1 == false && status == false) {
				fc.utobj().throwsException(exceptionMsg);
			}
		}
	}
}
