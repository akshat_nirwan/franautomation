package com.builds.test.crm;

import java.util.Map;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Test;

import com.builds.uimaps.crm.AdminCRMConfigureLastContactedFieldPage;
import com.builds.uimaps.crm.CRMContactsPage;
import com.builds.utilities.FranconnectUtil;
import com.builds.utilities.TestCase;

/* Harish Dwivedi */

public class AdminCRMConfigureLastContactedFieldPageTest {
	FranconnectUtil fc = new FranconnectUtil();

	@Test(groups = "crmLastUpdatedTestTask")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Configure Last Contacted Field At Admin > CRM > Configure Last Contacted Field", testCaseId = "TC_290_Verify_Configure_Last_Contacted_Field")
	public void addConfigureLastContactedField() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			fc.crm().crm_common().CRMLeadsLnk(driver);
			CRMContactsPage pobjCrm = new CRMContactsPage(driver);

			fc.utobj().clickElement(driver, pobjCrm.contactsLinks);
			boolean isUserPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//*[@id='result']/table/tbody/tr/td/table/tbody/tr/td[contains(text () ,'B1l02173078 C1g02173037')]");
			if (isUserPresent == false) {
				// fc.utobj().throwsException("was not able to verify Owner");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	public String createConfigureLastContactedField(WebDriver driver, Map<String, String> config) throws Exception {

		String testCaseId = "TC_290_Verify_Configure_Last_Contacted_Field_Admin";

		if (fc.utobj().validate(testCaseId)) {
			try {
				AdminCRMConfigureLastContactedFieldPage pobj = new AdminCRMConfigureLastContactedFieldPage(driver);

				try {
					fc.utobj().printTestStep("Navigate To Admin > CRM > Configure Last Contacted Field");
					fc.utobj().printTestStep("Modify Last Contacted Fields");
				} catch (Exception e) {
				}
				fc.crm().crm_common().adminCRMConfigureLastContactedFieldLnk(driver);

				fc.utobj().check(pobj.enable_1CheckBox, "yes");
				fc.utobj().check(pobj.enable_2CheckBox, "yes");
				fc.utobj().check(pobj.enable_3CheckBox, "yes");
				fc.utobj().check(pobj.enable_4CheckBox, "yes");

				fc.utobj().clickElement(driver, pobj.ModifyButton);

				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException("Was not able to create Group :: CRM > Groups");

		}
		return "";
	}

	/*
	 * public void contactsFilter(WebDriver driver , String contactType) throws
	 * Exception {
	 * 
	 * 
	 * CRMContactsPage pobj=new CRMContactsPage(driver); try {
	 * fc.utobj().clickElement(driver, pobj.showFilter); } catch (Exception e) {
	 * } fc.utobj().setToDefault(driver, pobj.contactOwnerSelect);
	 * fc.utobj().setToDefault(driver, pobj.franchiseIdSelect);
	 * fc.utobj().setToDefault(driver, pobj.contactTypeSelect); WebElement
	 * elementInput=fc.utobj().getElement(driver,
	 * pobj.contactTypeSelect).findElement(By.xpath("./div/div/input"));
	 * fc.utobj().selectValFromMultiSelect(driver, pobj.contactTypeSelect,
	 * elementInput, contactType); fc.utobj().setToDefault(driver,
	 * pobj.contactSourceSelect); fc.utobj().setToDefault(driver,
	 * pobj.statusSelect); fc.utobj().selectDropDown(driver, pobj.addDateSelect,
	 * "All"); fc.utobj().setToDefault(driver, pobj.emailStatusSelect); if
	 * (fc.utobj().getElement(driver,
	 * pobj.shoOnlyDupliateNo)) { fc.utobj().clickElement(driver,
	 * pobj.shoOnlyDuplicateNo); } fc.utobj().clickElement(driver,
	 * pobj.searchBtn); fc.utobj().clickElement(driver, pobj.hideFilter); }
	 */
}
