package com.builds.test.crm;

import java.util.Map;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Test;

import com.builds.uimaps.crm.AdminCRMConfigureLeadTypePage;
import com.builds.utilities.FranconnectUtil;
import com.builds.utilities.TestCase;

public class AdminCRMConfigureLeadTypePageTest {
	FranconnectUtil fc = new FranconnectUtil();

	@Test(groups = {"crm","helloworld111"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Add Lead Type At Admin > CRM > Configure Lead Type", testCaseId = "TC_22_Add_Lead_Type")
	public void addLeadType() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			AdminCRMConfigureLeadTypePage pobj = new AdminCRMConfigureLeadTypePage(driver);

			fc.utobj().printTestStep("Navigate To Admin > CRM > Configure Lead Type");
			fc.utobj().printTestStep("Add Lead Type");
			fc.crm().crm_common().adminCRMConfigureLeadTypeLnk(driver);
			fc.utobj().clickElement(driver, pobj.addLead);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			String leadType = fc.utobj().generateTestData(dataSet.get("leadType"));
			fc.utobj().sendKeys(driver, pobj.leadType, leadType);
			fc.utobj().clickElement(driver, pobj.addBtn);
			fc.utobj().clickElement(driver, pobj.closeBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Verify Add Lead Type");
			boolean isTextPresent = fc.utobj().assertPageSource(driver, leadType);
			if (isTextPresent == false) {
				fc.utobj().throwsException("was not able to add Lead Type");
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	public void addLeadType(WebDriver driver, String leadType) throws Exception {

		String testCaseId = "TC_Add_Lead_Type";

		if (fc.utobj().validate(testCaseId)) {

			try {
				AdminCRMConfigureLeadTypePage pobj = new AdminCRMConfigureLeadTypePage(driver);
				fc.crm().crm_common().adminCRMConfigureLeadTypeLnk(driver);
				fc.utobj().clickElement(driver, pobj.addLead);
				fc.commonMethods().switch_cboxIframe_frameId(driver);
				fc.utobj().sendKeys(driver, pobj.leadType, leadType);
				fc.utobj().clickElement(driver, pobj.addBtn);
				fc.utobj().clickElement(driver, pobj.closeBtn);
				fc.utobj().switchFrameToDefault(driver);
				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException("Was not able to add Lead Type :: Admin > CRM > Configure Lead Type");
		}
	}

	@Test(groups = "crm")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Modify Lead Type At Admin > CRM > Configure Lead Type", testCaseId = "TC_23_Modify_Lead_Type")
	public void modifyLeadType() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			AdminCRMConfigureLeadTypePage pobj = new AdminCRMConfigureLeadTypePage(driver);

			fc.utobj().printTestStep("Navigate To Admin > CRM > Configure Lead Type");
			fc.utobj().printTestStep("Add Lead Type");
			String leadType = fc.utobj().generateTestData(dataSet.get("leadType"));
			addLeadType(driver, leadType);

			fc.utobj().printTestStep("Modify Lead Type");
			fc.utobj().actionImgOption(driver, leadType, "Modify");
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			leadType = fc.utobj().generateTestData(dataSet.get("leadType"));
			fc.utobj().sendKeys(driver, pobj.leadType, leadType);
			fc.utobj().clickElement(driver, pobj.saveBtn);
			fc.utobj().clickElement(driver, pobj.closeBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Verify The Modify Lead Type");
			boolean isTextPresent = fc.utobj().assertPageSource(driver, leadType);
			if (isTextPresent == false) {
				fc.utobj().throwsException("was not able to modify Lead Type");
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "crm")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Delete Lead Type At Admin > CRM > Configure Lead Type", testCaseId = "TC_24_Delete_Lead_Type")
	public void deleteLeadType() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			fc.utobj().printTestStep("Navigate To Admin > CRM > Configure Lead Type");
			fc.utobj().printTestStep("Add Lead Type");
			String leadType = fc.utobj().generateTestData(dataSet.get("leadType"));
			addLeadType(driver, leadType);

			fc.utobj().printTestStep("Verify The Delete Lead Type");
			fc.utobj().actionImgOption(driver, leadType, "Delete");
			fc.utobj().acceptAlertBox(driver);
			boolean isTextPresent = fc.utobj().assertPageSource(driver, leadType);
			if (isTextPresent == true) {
				fc.utobj().throwsException("was not able to delete Lead Type");
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}
}
