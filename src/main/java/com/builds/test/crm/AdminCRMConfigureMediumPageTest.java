package com.builds.test.crm;

import java.util.Map;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import com.builds.uimaps.crm.AdminCRMConfigureMediumPage;
import com.builds.utilities.FranconnectUtil;
import com.builds.utilities.TestCase;

public class AdminCRMConfigureMediumPageTest {
	FranconnectUtil fc = new FranconnectUtil();

	@Test(groups = "crm")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Add Medium At Admin > CRM > Configure Medium", testCaseId = "TC_10_Add_Medium")
	public void addMedium() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			AdminCRMConfigureMediumPage pobj = new AdminCRMConfigureMediumPage(driver);

			fc.utobj().printTestStep("Navigating to Admin > CRM > Configure Medium");
			fc.utobj().printTestStep("Add Medium");
			fc.crm().crm_common().adminCRMConfigureMediumLnk(driver);
			fc.utobj().clickElement(driver, pobj.addMedium);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			String medium = fc.utobj().generateTestData(dataSet.get("medium"));
			fc.utobj().sendKeys(driver, pobj.medium, medium);
			String description = fc.utobj().generateTestData(dataSet.get("description"));
			fc.utobj().sendKeys(driver, pobj.description, description);
			fc.utobj().clickElement(driver, pobj.addBtn);
			fc.utobj().clickElement(driver, pobj.okBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().webDriverWait(driver, ".//*[contains(text () ,'" + medium + "')]");
			fc.utobj().printTestStep("Verify The Add Medium");
			WebElement element = fc.utobj().getElementByXpath(driver, ".//*[contains(text () ,'" + medium
					+ "')]/ancestor::tr/td[contains(text () ,'" + description + "')]");
			if (!element.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Medium Decription");
			}
			boolean isTextPresent = fc.utobj().assertPageSource(driver, medium);
			if (isTextPresent == false) {
				fc.utobj().throwsException("was not able to add Medium");
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	public void addMedium(WebDriver driver, String medium, String description) throws Exception {

		String testCaseId = "TC_Add_Medium";
		if (fc.utobj().validate(testCaseId)) {

			try {
				AdminCRMConfigureMediumPage pobj = new AdminCRMConfigureMediumPage(driver);
				fc.crm().crm_common().adminCRMConfigureMediumLnk(driver);
				fc.utobj().clickElement(driver, pobj.addMedium);
				fc.commonMethods().switch_cboxIframe_frameId(driver);
				fc.utobj().sendKeys(driver, pobj.medium, medium);
				fc.utobj().sendKeys(driver, pobj.description, description);
				fc.utobj().clickElement(driver, pobj.addBtn);
				fc.utobj().clickElement(driver, pobj.okBtn);

				fc.utobj().switchFrameToDefault(driver);
				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException("Was not able to Medium At Admin > CRM > Configure Medium.");
		}
	}

	@Test(groups = {"crm"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Modify Medium At Admin > CRM > Configure Medium", testCaseId = "TC_11_Modify_Medium")
	public void modifyMedium() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			fc.utobj().printTestStep("Navigating to Admin > CRM > Configure Medium");
			fc.utobj().printTestStep("Add Medium");
			AdminCRMConfigureMediumPage pobj = new AdminCRMConfigureMediumPage(driver);
			String medium = fc.utobj().generateTestData(dataSet.get("medium"));
			String description = fc.utobj().generateTestData(dataSet.get("description"));
			addMedium(driver, medium, description);

			fc.utobj().printTestStep("Modify Medium");

			fc.utobj().actionImgOption(driver, medium, "Modify");
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			medium = fc.utobj().generateTestData(dataSet.get("medium"));
			fc.utobj().sendKeys(driver, pobj.medium, medium);
			description = fc.utobj().generateTestData(dataSet.get("description"));
			fc.utobj().sendKeys(driver, pobj.description, description);
			fc.utobj().clickElement(driver, pobj.saveBtn);
			fc.utobj().clickElement(driver, pobj.okBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Verify The Modify Medium");

			boolean isTextPresent = fc.utobj().assertPageSource(driver, medium);
			if (isTextPresent == false) {
				fc.utobj().throwsException("was not able to modify Medium");
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "crm")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Delete Medium At Admin > CRM > Configure Medium", testCaseId = "TC_12_Delete_Medium")
	public void deleteMedium() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			fc.utobj().printTestStep("Navigating to Admin > CRM > Configure Medium");
			fc.utobj().printTestStep("Add Medium");
			String medium = fc.utobj().generateTestData(dataSet.get("medium"));
			String description = fc.utobj().generateTestData(dataSet.get("description"));
			addMedium(driver, medium, description);

			fc.utobj().printTestStep("Delete Medium");

			fc.utobj().actionImgOption(driver, medium, "Delete");
			fc.utobj().acceptAlertBox(driver);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Verify The Delete Medium");
			boolean isTextPresent = fc.utobj().assertPageSource(driver, medium);
			if (isTextPresent == true) {
				fc.utobj().throwsException("was not able to delete Medium");
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}
}
