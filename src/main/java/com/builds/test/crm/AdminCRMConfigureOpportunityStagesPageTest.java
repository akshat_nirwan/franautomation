package com.builds.test.crm;

import java.util.Map;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Test;

import com.builds.uimaps.crm.AdminCRMConfigureOpportunityStagesPage;
import com.builds.utilities.FranconnectUtil;
import com.builds.utilities.TestCase;

public class AdminCRMConfigureOpportunityStagesPageTest {
	FranconnectUtil fc = new FranconnectUtil();

	@Test(groups = "crm")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify the Add Stage At Admin > CRM > Configure Opportunity Stages", testCaseId = "TC_28_Add_Stage")
	public void addStage() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			AdminCRMConfigureOpportunityStagesPage pobj = new AdminCRMConfigureOpportunityStagesPage(driver);

			fc.utobj().printTestStep("Navigating to Admin > CRM >  Configure Opportunity Stages");
			fc.utobj().printTestStep("Add Stages");
			fc.crm().crm_common().adminCRMConfigureOpportunityStagesLnk(driver);
			fc.utobj().clickElement(driver, pobj.addStage);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			String stage = fc.utobj().generateTestData(dataSet.get("stage"));
			fc.utobj().sendKeys(driver, pobj.stage, stage);
			fc.utobj().clickElement(driver, pobj.addBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Verify The Add Stages");
			boolean isTextPresent = fc.utobj().assertPageSource(driver, stage);
			if (isTextPresent == false) {
				fc.utobj().throwsException("was not able to add Stage");
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	public void addStage(WebDriver driver, String stage) throws Exception {

		String testCaseId = "TC_Add_Stage_CRM";

		if (fc.utobj().validate(testCaseId)) {
			try {
				AdminCRMConfigureOpportunityStagesPage pobj = new AdminCRMConfigureOpportunityStagesPage(driver);
				fc.crm().crm_common().adminCRMConfigureOpportunityStagesLnk(driver);
				fc.utobj().clickElement(driver, pobj.addStage);
				fc.commonMethods().switch_cboxIframe_frameId(driver);

				fc.utobj().sendKeys(driver, pobj.stage, stage);
				fc.utobj().clickElement(driver, pobj.addBtn);
				fc.utobj().switchFrameToDefault(driver);

				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException("Was not able to add Stage At Admin > CRM > Configure Opportunity Stages");

		}
	}

	@Test(groups = "crm")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify the Modify Stage At Admin > CRM > Configure Opportunity Stages", testCaseId = "TC_29_Modify_Stages")
	public void modifyStage() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			AdminCRMConfigureOpportunityStagesPage pobj = new AdminCRMConfigureOpportunityStagesPage(driver);

			fc.utobj().printTestStep("Navigating to Admin > CRM >  Configure Opportunity Stages");
			fc.utobj().printTestStep("Add Stages");
			String stage = fc.utobj().generateTestData(dataSet.get("stage"));
			addStage(driver, stage);

			fc.utobj().printTestStep("Modify Stages");
			actionImgOption(driver, stage, "Modify");
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			stage = fc.utobj().generateTestData(dataSet.get("stage"));
			fc.utobj().sendKeys(driver, pobj.stage, stage);
			fc.utobj().clickElement(driver, pobj.modifyBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Verify The Modify Stages");
			boolean isTextPresent = fc.utobj().assertPageSource(driver, stage);
			if (isTextPresent == false) {
				fc.utobj().throwsException("was not able to Modify Stage");
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "crm")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify the Delete Stage At Admin > CRM > Configure Opportunity Stages", testCaseId = "TC_30_Delete_Stages")
	public void deleteStage() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			fc.utobj().printTestStep("Navigating to Admin > CRM >  Configure Opportunity Stages");
			fc.utobj().printTestStep("Add Stages");
			String stage = fc.utobj().generateTestData(dataSet.get("stage"));
			addStage(driver, stage);

			fc.utobj().printTestStep("Delete Stages");
			actionImgOption(driver, stage, "Delete");
			fc.utobj().acceptAlertBox(driver);

			fc.utobj().printTestStep("Verify The Delete Stages");
			boolean isTextPresent = fc.utobj().assertPageSource(driver, stage);
			if (isTextPresent == true) {
				fc.utobj().throwsException("was not able to Delete Stage");
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	public void actionImgOption(WebDriver driver, String task, String option) throws Exception {

		fc.utobj().moveToElement(driver,
				fc.utobj().getElementByXpath(driver, ".//*[@value='" + task + "']/../div/layer"));
		String alterText = fc.utobj().getElementByXpath(driver, ".//*[@value='" + task + "']/../div/layer")
				.getAttribute("id").trim();
		alterText = alterText.replace("Actions_dynamicmenu", "");
		alterText = alterText.replace("Bar", "");

		fc.utobj().clickElement(driver,
				fc.utobj().getElementByXpath(driver, ".//*[@value='" + task + "']/../div/layer/a/img"));
		fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
				".//div[@id='Actions_dynamicmenu" + alterText + "Menu']/span[contains(text () , '" + option + "')]"));
	}
}
