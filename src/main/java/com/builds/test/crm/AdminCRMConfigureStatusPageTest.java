package com.builds.test.crm;

import java.util.Map;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Test;

import com.builds.uimaps.crm.AdminCRMConfigureStatusPageUI;
import com.builds.utilities.FranconnectUtil;
import com.builds.utilities.TestCase;

public class AdminCRMConfigureStatusPageTest {
	FranconnectUtil fc = new FranconnectUtil();

	@Test(groups = { "crm"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify the Add Status At Admin > CRM > Configure Status [Leads]", testCaseId = "TC_04_Add_Status")
	public void addStatus() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			AdminCRMConfigureStatusPageUI pobj = new AdminCRMConfigureStatusPageUI(driver);

			fc.utobj().printTestStep("Navigating to Admin > CRM > ConfigureStatus");
			fc.utobj().printTestStep("Add Status Lead Type");
			fc.crm().crm_common().adminCRMConfigureStatusLnk(driver);
			fc.utobj().clickElement(driver, pobj.leads);
			fc.utobj().clickElement(driver, pobj.addStatus);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			String statusName = fc.utobj().generateTestData(dataSet.get("statusName"));
			fc.utobj().sendKeys(driver, pobj.statusTxBx, statusName);
			fc.utobj().clickElement(driver, pobj.addBtn);

			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Verify The Add Status Lead Type");
			boolean isTextPresent = fc.utobj().assertPageSource(driver, statusName);
			if (isTextPresent == false) {
				fc.utobj().throwsException("was not able to add Status");
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	public void addStatus(WebDriver driver, String statusName) throws Exception {

		String testCaseId = "TC_Add_Status_Lead";

		if (fc.utobj().validate(testCaseId)) {
			try {
				AdminCRMConfigureStatusPageUI pobj = new AdminCRMConfigureStatusPageUI(driver);

				fc.crm().crm_common().adminCRMConfigureStatusLnk(driver);
				fc.utobj().clickElement(driver, pobj.leads);
				fc.utobj().clickElement(driver, pobj.addStatus);
				fc.commonMethods().switch_cboxIframe_frameId(driver);
				fc.utobj().sendKeys(driver, pobj.statusTxBx, statusName);
				fc.utobj().clickElement(driver, pobj.addBtn);

				fc.utobj().switchFrameToDefault(driver);

				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException("Was not able to add Status Lead :: Admin > CRM > Configure Status");
		}
	}

	@Test(groups = { "crm" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify the Modify Status At Admin > CRM > Configure Status [Leads]", testCaseId = "TC_05_Modify_Status")
	public void modifyStatus() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			AdminCRMConfigureStatusPageUI pobj = new AdminCRMConfigureStatusPageUI(driver);

			fc.utobj().printTestStep("Navigating to Admin > CRM > Configure Status");
			fc.utobj().printTestStep("Add Status Lead Type");
			String statusName = fc.utobj().generateTestData(dataSet.get("statusName"));
			addStatus(driver, statusName);

			fc.utobj().printTestStep("Modify Status Lead Type");
			actionImgOption(driver, statusName, "Modify");
			fc.commonMethods().switch_cboxIframe_frameId(driver);

			statusName = fc.utobj().generateTestData(dataSet.get("statusName"));

			fc.utobj().sendKeys(driver, pobj.statusTxBx, statusName);
			fc.utobj().clickElement(driver, pobj.modifyBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Verify The Modify Status Lead Type");
			boolean isTextPresent = fc.utobj().assertPageSource(driver, statusName);
			if (isTextPresent == false) {
				fc.utobj().throwsException("was not able to modify Status");
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "crm" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify the  Delete Status At Admin > CRM > Configure Status [Leads]", testCaseId = "TC_06_Delete_Status")
	public void deleteStatus() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			fc.utobj().printTestStep("Navigating to Admin > CRM > Configure Status");
			fc.utobj().printTestStep("Add Status Lead Type");
			String statusName = fc.utobj().generateTestData(dataSet.get("statusName"));
			addStatus(driver, statusName);

			fc.utobj().printTestStep("Delete Status Lead Type");
			actionImgOption(driver, statusName, "Delete");
			fc.utobj().acceptAlertBox(driver);

			fc.utobj().printTestStep("Verify The Delete Status Lead Type");
			boolean isTextPresent = fc.utobj().assertPageSource(driver, statusName);
			if (isTextPresent == true) {
				fc.utobj().throwsException("was not able to delete Status");
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "crm")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Add Status At Admin > CRM > Configure Status [Contacts]", testCaseId = "TC_07_Add_Status")
	public void addStatus01() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			AdminCRMConfigureStatusPageUI pobj = new AdminCRMConfigureStatusPageUI(driver);

			fc.utobj().printTestStep("Navigating to Admin > CRM > Configure Status");
			fc.utobj().printTestStep("Add Status Contact Type");
			fc.crm().crm_common().adminCRMConfigureStatusLnk(driver);
			fc.utobj().clickElement(driver, pobj.contacts);
			fc.utobj().clickElement(driver, pobj.addStatus);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			String statusName = fc.utobj().generateTestData(dataSet.get("statusName"));
			fc.utobj().sendKeys(driver, pobj.statusTxBx, statusName);
			fc.utobj().clickElement(driver, pobj.addBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Verify Status Contact Type");
			boolean isTextPresent = fc.utobj().assertPageSource(driver, statusName);
			if (isTextPresent == false) {
				fc.utobj().throwsException("was not able to add Status");
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	public void addStatus01(WebDriver driver, String statusName) throws Exception {

		String testCaseId = "TC_Add_Status_Contact";

		if (fc.utobj().validate(testCaseId)) {
			try {
				AdminCRMConfigureStatusPageUI pobj = new AdminCRMConfigureStatusPageUI(driver);
				fc.crm().crm_common().adminCRMConfigureStatusLnk(driver);
				fc.utobj().clickElement(driver, pobj.contacts);
				fc.utobj().clickElement(driver, pobj.addStatus);
				fc.commonMethods().switch_cboxIframe_frameId(driver);
				fc.utobj().sendKeys(driver, pobj.statusTxBx, statusName);
				fc.utobj().clickElement(driver, pobj.addBtn);
				fc.utobj().switchFrameToDefault(driver);

				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException("Was not able to add Status for Contact :: Admin > CRM > Configure Status");
		}
	}

	@Test(groups = "crm")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify the Modify Status At Admin > CRM > Configure Status [Contacts]", testCaseId = "TC_08_Modify_Status")
	public void modifyStatus01() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			AdminCRMConfigureStatusPageUI pobj = new AdminCRMConfigureStatusPageUI(driver);

			fc.utobj().printTestStep("Navigating to Admin > CRM > Configure Status");
			fc.utobj().printTestStep("Add Status Contact Type");
			String statusName = fc.utobj().generateTestData(dataSet.get("statusName"));
			addStatus01(driver, statusName);

			fc.utobj().printTestStep("Modify Status Contact Type");
			actionImgOption(driver, statusName, "Modify");
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			statusName = fc.utobj().generateTestData(dataSet.get("statusName"));
			fc.utobj().sendKeys(driver, pobj.statusTxBx, statusName);
			fc.utobj().clickElement(driver, pobj.modifyBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Verify The Modify Status Contact Type");
			boolean isTextPresent = fc.utobj().assertPageSource(driver, statusName);
			if (isTextPresent == false) {
				fc.utobj().throwsException("was not able to modify Status");
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "crm")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify the Delete Status At Admin > CRM > Configure Status [Contacts]", testCaseId = "TC_09_Delete_Status")
	public void deleteStatus01() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			fc.utobj().printTestStep("Navigating to Admin > CRM > Configure Status");
			fc.utobj().printTestStep("Add Status Contact Type");
			String statusName = fc.utobj().generateTestData(dataSet.get("statusName"));
			addStatus01(driver, statusName);

			fc.utobj().printTestStep("Delete The Status");
			actionImgOption(driver, statusName, "Delete");
			fc.utobj().acceptAlertBox(driver);

			fc.utobj().printTestStep("Verify The Delete Status");
			boolean isTextPresent = fc.utobj().assertPageSource(driver, statusName);
			if (isTextPresent == true) {
				fc.utobj().throwsException("was not able to delete Status");
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	public void actionImgOption(WebDriver driver, String task, String option) throws Exception {

		fc.utobj().moveToElement(driver,
				fc.utobj().getElementByXpath(driver, ".//*[@value='" + task + "']/../div/layer"));
		String alterText = fc.utobj().getElementByXpath(driver, ".//*[@value='" + task + "']/../div/layer")
				.getAttribute("id").trim();
		alterText = alterText.replace("Actions_dynamicmenu", "");
		alterText = alterText.replace("Bar", "");

		fc.utobj().clickElement(driver,
				fc.utobj().getElementByXpath(driver, ".//*[@value='" + task + "']/../div/layer/a/img"));
		fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
				".//div[@id='Actions_dynamicmenu" + alterText + "Menu']/span[contains(text () , '" + option + "')]"));
	}
}
