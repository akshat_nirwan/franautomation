package com.builds.test.crm;

import java.util.Map;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Test;

import com.builds.uimaps.crm.AdminCRMContactTypeConfigurationPage;
import com.builds.utilities.FranconnectUtil;
import com.builds.utilities.TestCase;

public class AdminCRMContactTypeConfigurationPageTest {
	FranconnectUtil fc = new FranconnectUtil();

	@Test(groups = {"crm","errorpages1"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify the Add Contact Type At Admin > CRM > Contact Type Configuration", testCaseId = "TC_01_Add_Contact_Type")
	private void addContactType() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			AdminCRMContactTypeConfigurationPage pobj = new AdminCRMContactTypeConfigurationPage(driver);

			fc.utobj().printTestStep("Navigating to Admin > CRM > ContactTypeConfiguration");
			fc.utobj().printTestStep("Add Contact Type");
			fc.crm().crm_common().adminCRMContactTypeConfigurationLnk(driver);
			fc.utobj().clickElement(driver, pobj.addContactType);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			String contactType = fc.utobj().generateTestData(dataSet.get("contactType"));
			fc.utobj().sendKeys(driver, pobj.contactType, contactType);
			fc.utobj().clickElement(driver, pobj.addBtn);
			// fc.utobj().clickElement(driver, pobj.closeBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Verify Contact Type");
			boolean isTextPresent = fc.utobj().assertPageSource(driver, contactType);
			if (isTextPresent == false) {
				fc.utobj().throwsException("was not able to add Contact Type");
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	public void addContactType(WebDriver driver, String contactType) throws Exception {

		String testCaseId = "TC_Add_Contact_Type";

		if (fc.utobj().validate(testCaseId)) {
			try {
				AdminCRMContactTypeConfigurationPage pobj = new AdminCRMContactTypeConfigurationPage(driver);
				fc.crm().crm_common().adminCRMContactTypeConfigurationLnk(driver);
				fc.utobj().clickElement(driver, pobj.addContactType);
				fc.commonMethods().switch_cboxIframe_frameId(driver);
				fc.utobj().sendKeys(driver, pobj.contactType, contactType);
				fc.utobj().clickElement(driver, pobj.addBtn);
				fc.commonMethods().Click_Close_Input_ByValue(driver);
				
				fc.utobj().switchFrameToDefault(driver);

				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException(
					"was not able to add ContactType :: Admin > CRM > Contact Type Configuration.");
		}
	}

	@Test(groups = {"crm","errorpages1"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify the Modify Contact Type At Admin > CRM > Contact Type Configuration", testCaseId = "TC_02_Modify_Contact_Type")
	private void modifyContactType() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			fc.utobj().printTestStep("Navigating to Admin > CRM > ContactTypeConfiguration");
			fc.utobj().printTestStep("Add Contact Type");
			AdminCRMContactTypeConfigurationPage pobj = new AdminCRMContactTypeConfigurationPage(driver);
			String contactType = fc.utobj().generateTestData(dataSet.get("contactType"));
			addContactType(driver, contactType);

			fc.utobj().printTestStep("Modify Contact Type");
			fc.utobj().actionImgOption(driver, contactType, "Modify");
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			contactType = fc.utobj().generateTestData(dataSet.get("contactType"));
			fc.utobj().sendKeys(driver, pobj.contactType, contactType);
			fc.utobj().clickElement(driver, pobj.modifyBtn);
			// fc.utobj().clickElement(driver, pobj.closeBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Verify The Modify Contact Type");
			boolean isTextPresent = fc.utobj().assertPageSource(driver, contactType);
			if (isTextPresent == false) {
				fc.utobj().throwsException("was not able to modify Contact Type");
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = {"crm","errorpages1"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify the Delete Contact Type At Admin > CRM > Contact Type Configuration", testCaseId = "TC_03_Delete_Contact_Type")
	private void deleteContactType() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			fc.utobj().printTestStep("Navigating to Admin > CRM > ContactTypeConfiguration");
			fc.utobj().printTestStep("Add Contact Type");
			String contactType = fc.utobj().generateTestData(dataSet.get("contactType"));
			addContactType(driver, contactType);

			fc.utobj().printTestStep("Delete Contact Type");
			fc.utobj().actionImgOption(driver, contactType, "Delete");
			fc.utobj().acceptAlertBox(driver);

			fc.utobj().printTestStep("Verify The Delete Contact Type");
			boolean isTextPresent = fc.utobj().assertPageSource(driver, contactType);
			if (isTextPresent == true) {
				fc.utobj().throwsException("was not able to Delete Contact Type");
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}
}
