package com.builds.test.crm;

import java.io.IOException;
import java.util.Map;

import javax.xml.parsers.ParserConfigurationException;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.Test;
import org.xml.sax.SAXException;

import com.builds.test.infomgr.AdminInfoMgrCommonMethods;
import com.builds.uimaps.crm.CRMContactsPage;
import com.builds.uimaps.crm.CRMLeadsPage;
import com.builds.uimaps.infomgr.AdminInfoMgrManageFormGeneratorPage;
import com.builds.uimaps.infomgr.AdminInfoMgrManageFormGeneratorTabDetailsPage;
import com.builds.utilities.FranconnectUtil;
import com.builds.utilities.TestCase;

public class AdminCRMManageFormGeneratorPageTest {

	FranconnectUtil fc = new FranconnectUtil();

	/* Anukaran Mishra */

	@Test(groups = "crmmoduleExcludedwait")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Deactivate Birthday, Anniversary & Sub-Status Field >Change a Contact status >Status should get change", testCaseId = "TC_321_Verify_Sub_Staus_Deactivate")
	public void verifySubStausDeactivate() throws IOException, Exception, ParserConfigurationException, SAXException {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			CRMContactsPage pobj = new CRMContactsPage(driver);

			fc.crm().crm_common().adminCRMManageFormGeneratorLnk(driver);
			AdminInfoMgrCommonMethods adminCommonMethods = fc.adminpage().adminInfoMgr();
			AdminInfoMgrManageFormGeneratorPage manageFormGeneratorPage = new AdminInfoMgrManageFormGeneratorPage(
					driver);
			adminCommonMethods.NavigateToManageFormGeneratorTabs(driver, "Contact");

			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//*[@id='tabdetails']//a[contains(text(),'Primary Info')]"));
			try {
				fc.utobj().clickElement(driver, manageFormGeneratorPage.birthdateActiavte);
			} catch (Exception e) {
			}
			Thread.sleep(2000);
			try {
				fc.utobj().clickElement(driver, manageFormGeneratorPage.anniversaryActiavte);
			} catch (Exception e) {
			}
			Thread.sleep(2000);
			try {
				fc.utobj().clickElement(driver, manageFormGeneratorPage.subStatus);
			} catch (Exception e) {
			}
			Thread.sleep(2000);
			fc.utobj().printTestStep("Navigate To Admin > CRM > Contact Type Configuration");
			fc.utobj().printTestStep("Add Contact Type");
			AdminCRMContactTypeConfigurationPageTest contatcTypePage = new AdminCRMContactTypeConfigurationPageTest();
			String contactType = fc.utobj().generateTestData(dataSet.get("contactType"));
			contatcTypePage.addContactType(driver, contactType);

			fc.utobj().printTestStep("Navigate To Admin > CRM > Configure Status");
			fc.utobj().printTestStep("Add Status For Contact");
			AdminCRMConfigureStatusPageTest statusPage = new AdminCRMConfigureStatusPageTest();
			String statusName = fc.utobj().generateTestData(dataSet.get("status"));
			statusPage.addStatus01(driver, statusName);

			fc.utobj().printTestStep("Navigate To CRM > Contacts > Contacts Summary Page");
			fc.utobj().printTestStep("Add Contact");
			fc.crm().crm_common().CRMContactsLnk(driver);
			fc.utobj().clickElement(driver, pobj.addNew);
			fc.utobj().clickElement(driver, pobj.contactLnk);

			String firstName = fc.utobj().generateTestData(dataSet.get("firstName"));
			String lastName = fc.utobj().generateTestData(dataSet.get("lastName"));
			String primaryCMethod = dataSet.get("primaryCMethod");
			String assignTo = "Corporate";
			String regionName = "";
			String franchiseId = "";
			String title = "Mr.";
			String email = "automation@gmail.com";
			String userName = "FranConnect Administrator";
			/*
			 * addContactGeneric(driver, contactType, firstName, lastName,
			 * primaryCMethod,assignTo, "FranConnect Adminstrator", regionName,
			 * franchiseId, title, email, config);
			 */

			fc.utobj().selectDropDown(driver, pobj.contactType, contactType);
			fc.utobj().selectDropDown(driver, pobj.title, title);
			fc.utobj().sendKeys(driver, pobj.contactFirstName, firstName);
			fc.utobj().sendKeys(driver, pobj.contactLastName, lastName);
			fc.utobj().sendKeys(driver, pobj.suffix, "suffixTest");
			fc.utobj().sendKeys(driver, pobj.position, "TestJobTitle");
			fc.utobj().sendKeys(driver, pobj.address, "TestAddress");
			fc.utobj().sendKeys(driver, pobj.city, "TestCity");
			fc.utobj().selectDropDown(driver, pobj.country, "USA");
			fc.utobj().selectDropDown(driver, pobj.state, "Alabama");
			fc.utobj().selectDropDown(driver, pobj.primaryContactMethodSelect, primaryCMethod);
			fc.utobj().sendKeys(driver, pobj.bestTimeToContact, "12:00 PM EST");
			fc.utobj().sendKeys(driver, pobj.zipcode, "12345");
			fc.utobj().sendKeys(driver, pobj.phoneNumbers, "1236547896");
			fc.utobj().sendKeys(driver, pobj.extn, "12");
			fc.utobj().sendKeys(driver, pobj.faxNumbers, "1263985487");
			fc.utobj().sendKeys(driver, pobj.mobileNumbers, "8896724441");
			fc.utobj().sendKeys(driver, pobj.emailIds, email);
			fc.utobj().sendKeys(driver, pobj.alternateEmail, "test@gmail.com");
			// fc.utobj().sendKeys(driver, pobj.birthdate, "11/24/1980");
			// fc.utobj().sendKeys(driver, pobj.anniversarydate, "11/24/1999");

			if (assignTo.equalsIgnoreCase("Corporate")) {

				if (!fc.utobj().isSelected(driver, pobj.assignToCorporate)) {
					fc.utobj().clickElement(driver, pobj.assignToCorporate);
				}

				fc.utobj().selectDropDown(driver, pobj.selectCorporateUserContact, userName);

			} else if (assignTo.equalsIgnoreCase("Regional")) {

				if (!fc.utobj().isSelected(driver, pobj.assignToRegional)) {
					fc.utobj().clickElement(driver, pobj.assignToRegional);
				}

				fc.utobj().selectDropDown(driver, pobj.selectAreaRegion, regionName);
				fc.utobj().selectDropDown(driver, pobj.selectRegionalUser, userName);

			} else if (assignTo.equalsIgnoreCase("Franchise")) {

				if (!fc.utobj().isSelected(driver, pobj.assignToFranchise)) {
					fc.utobj().clickElement(driver, pobj.assignToFranchise);
				}

				fc.utobj().selectDropDown(driver, pobj.selectFranchiseId, franchiseId);
				fc.utobj().selectDropDown(driver, pobj.selectFranchiseUser, userName);

			}
			fc.utobj().sendKeys(driver, pobj.comments, "Test Comment");
			fc.utobj().clickElement(driver, pobj.saveBtn);

			// fc.utobj().clickElement(driver, pobj.changeStatus);
			fc.utobj().printTestStep("Change Status");
			// fc.utobj().clickElement(driver,
			// fc.utobj().getElementByXpath(driver,".//a[contains(text () ,
			// '"+firstName+" "+lastName+"')]")));
			fc.utobj().selectDropDown(driver, pobj.contactStatus, statusName);
			fc.utobj().clickElement(driver, pobj.changeStatus);

			boolean verifyStatusChange = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//*[@id='cmLeadStatusID']/option[contains(text(),'New')]");
			if (verifyStatusChange == false) {
				fc.utobj().throwsException("was not able to verify Status Change");
			}
			fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}

	}

	/* Anukaran Mishra */

	@Test(groups = "crmmodulewaitFormgen")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Deactivate Birthday, Anniversary & Sub-Status Field >Change a Contact status >Status should get change", testCaseId = "TC_322_Verify_FormGenerator_Custom_Fields")
	public void VerifyFormGeneratorCustomFields()
			throws IOException, Exception, ParserConfigurationException, SAXException {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			CRMContactsPage pobj = new CRMContactsPage(driver);

			fc.crm().crm_common().adminCRMManageFormGeneratorLnk(driver);
			AdminInfoMgrCommonMethods adminCommonMethods = fc.adminpage().adminInfoMgr();
			AdminInfoMgrManageFormGeneratorPage manageFormGeneratorPage = new AdminInfoMgrManageFormGeneratorPage(
					driver);
			AdminInfoMgrManageFormGeneratorTabDetailsPage ADM_form_gen = manageFormGeneratorPage.getAddTabDetailsPage();
			adminCommonMethods.NavigateToManageFormGeneratorTabs(driver, "Contact");
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//*[@id='tabdetails']//a[contains(text(),'Primary Info')]"));
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//*[@id='siteMainTable']//tr/td[contains(text(),'Contact Information')]/following-sibling::td/a[contains(text(),'Add New Field')]"));
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			String FieldName1 = "Text";
			FieldName1 = fc.utobj().generateTestData(FieldName1);
			fc.utobj().sendKeys(driver, ADM_form_gen.txtFieldName, FieldName1);
			// fc.utobj().clickElement(driver, ADM_form_gen.documentopt);
			fc.utobj().selectDropDown(driver, ADM_form_gen.FieldType, "Text");
			fc.utobj().clickElement(driver, ADM_form_gen.btnSumbitField);
			fc.utobj().switchFrameToDefault(driver);
			// fc.utobj().clickElement(driver,
			// fc.utobj().getElementByXpath(driver,".//*[@id='siteMainTable']//tr/td[contains(text(),'"+FieldName1+"')]/following-sibling::td[5]/a/img")));

			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//*[@id='siteMainTable']//tr/td[contains(text(),'Contact Information')]/following-sibling::td/a[contains(text(),'Add New Field')]"));
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			String FieldName2 = "Text Area";
			FieldName2 = fc.utobj().generateTestData(FieldName2);
			fc.utobj().sendKeys(driver, ADM_form_gen.txtFieldName, FieldName2);
			// fc.utobj().clickElement(driver, ADM_form_gen.documentopt);
			fc.utobj().selectDropDown(driver, ADM_form_gen.FieldType, "Text Area");
			fc.utobj().clickElement(driver, ADM_form_gen.btnSumbitField);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//*[@id='siteMainTable']//tr/td[contains(text(),'Contact Information')]/following-sibling::td/a[contains(text(),'Add New Field')]"));
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			String FieldName3 = "Date";
			FieldName3 = fc.utobj().generateTestData(FieldName3);
			fc.utobj().sendKeys(driver, ADM_form_gen.txtFieldName, FieldName3);
			// fc.utobj().clickElement(driver, ADM_form_gen.documentopt);
			fc.utobj().selectDropDown(driver, ADM_form_gen.FieldType, "Date");
			fc.utobj().clickElement(driver, ADM_form_gen.btnSumbitField);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//*[@id='siteMainTable']//tr/td[contains(text(),'Contact Information')]/following-sibling::td/a[contains(text(),'Add New Field')]"));
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			String FieldName4 = "DropDown";
			FieldName4 = fc.utobj().generateTestData(FieldName4);
			fc.utobj().sendKeys(driver, ADM_form_gen.txtFieldName, FieldName4);
			// fc.utobj().clickElement(driver, ADM_form_gen.documentopt);
			fc.utobj().selectDropDown(driver, ADM_form_gen.FieldType, "Drop-down");
			fc.utobj().sendKeys(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='row_1']/td[2]/input[1]"),
					"Opt" + FieldName4);

			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//*[@id='removeTDForAddMore']//*[contains(text(),'Add Option')]"));
			fc.utobj().sendKeys(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='row_2']/td[2]/input[1]"),
					"Opt2");
			fc.utobj().clickElement(driver, ADM_form_gen.btnSumbitField);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//*[@id='siteMainTable']//tr/td[contains(text(),'Contact Information')]/following-sibling::td/a[contains(text(),'Add New Field')]"));
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			String FieldName5 = "Radio";
			FieldName5 = fc.utobj().generateTestData(FieldName5);
			fc.utobj().sendKeys(driver, ADM_form_gen.txtFieldName, FieldName5);
			// fc.utobj().clickElement(driver, ADM_form_gen.documentopt);
			fc.utobj().selectDropDown(driver, ADM_form_gen.FieldType, "Radio");
			fc.utobj().sendKeys(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='row_1']/td[2]/input[1]"),
					"Opt" + FieldName5);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//*[@id='removeTDForAddMore']//*[contains(text(),'Add Option')]"));
			fc.utobj().sendKeys(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='row_2']/td[2]/input[1]"),
					"Opt2" + FieldName5);
			fc.utobj().clickElement(driver, ADM_form_gen.btnSumbitField);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//*[@id='siteMainTable']//tr/td[contains(text(),'Contact Information')]/following-sibling::td/a[contains(text(),'Add New Field')]"));
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			String FieldName6 = "Checkbox";
			FieldName6 = fc.utobj().generateTestData(FieldName6);
			fc.utobj().sendKeys(driver, ADM_form_gen.txtFieldName, FieldName6);
			// fc.utobj().clickElement(driver, ADM_form_gen.documentopt);
			fc.utobj().selectDropDown(driver, ADM_form_gen.FieldType, "Checkbox");
			fc.utobj().sendKeys(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='row_1']/td[2]/input[1]"),
					"Opt1");
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//*[@id='removeTDForAddMore']//*[contains(text(),'Add Option')]"));
			fc.utobj().sendKeys(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='row_2']/td[2]/input[1]"),
					"Opt2");
			fc.utobj().clickElement(driver, ADM_form_gen.btnSumbitField);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//*[@id='siteMainTable']//tr/td[contains(text(),'Contact Information')]/following-sibling::td/a[contains(text(),'Add New Field')]"));
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			String FieldName7 = "Test Field3";
			FieldName7 = fc.utobj().generateTestData(FieldName7);
			fc.utobj().sendKeys(driver, ADM_form_gen.txtFieldName, FieldName7);
			// fc.utobj().clickElement(driver, ADM_form_gen.documentopt);
			fc.utobj().selectDropDown(driver, ADM_form_gen.FieldType, "Numeric");
			fc.utobj().clickElement(driver, ADM_form_gen.btnSumbitField);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//*[@id='siteMainTable']//tr/td[contains(text(),'Contact Information')]/following-sibling::td/a[contains(text(),'Add New Field')]"));
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			String FieldName8 = "Multislect";
			FieldName8 = fc.utobj().generateTestData(FieldName8);
			fc.utobj().sendKeys(driver, ADM_form_gen.txtFieldName, FieldName8);
			// fc.utobj().clickElement(driver, ADM_form_gen.documentopt);
			fc.utobj().selectDropDown(driver, ADM_form_gen.FieldType, "Multi Select Drop-down");
			fc.utobj().sendKeys(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='row_1']/td[2]/input[1]"),
					"Mul" + FieldName8);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//*[@id='removeTDForAddMore']//*[contains(text(),'Add Option')]"));
			fc.utobj().sendKeys(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='row_2']/td[2]/input[1]"),
					"Opt2");
			fc.utobj().clickElement(driver, ADM_form_gen.btnSumbitField);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//*[@id='siteMainTable']//tr/td[contains(text(),'Contact Information')]/following-sibling::td/a[contains(text(),'Add New Field')]"));
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			String FieldName9 = "Test Field3";
			FieldName9 = fc.utobj().generateTestData(FieldName9);
			fc.utobj().sendKeys(driver, ADM_form_gen.txtFieldName, FieldName9);
			// fc.utobj().clickElement(driver, ADM_form_gen.documentopt);
			fc.utobj().selectDropDown(driver, ADM_form_gen.FieldType, "Document");
			fc.utobj().clickElement(driver, ADM_form_gen.btnSumbitField);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Navigate To Admin > CRM > Contact Type Configuration");
			fc.utobj().printTestStep("Add Contact Type");
			AdminCRMContactTypeConfigurationPageTest contatcTypePage = new AdminCRMContactTypeConfigurationPageTest();
			String contactType = fc.utobj().generateTestData(dataSet.get("contactType"));
			contatcTypePage.addContactType(driver, contactType);

			/*
			 * fc.utobj().printTestStep(testCaseId,
			 * "Navigate To Admin > CRM > Configure Status");
			 * fc.utobj().printTestStep(testCaseId, "Add Status For Contact");
			 * AdminCRMConfigureStatusPageTest statusPage=new
			 * AdminCRMConfigureStatusPageTest(); String
			 * statusName=fc.utobj().generateTestData(dataSet.get("status"));
			 * statusPage.addStatus01(driver, statusName, config);
			 */

			fc.utobj().printTestStep("Navigate To CRM > Contacts > Contacts Summary Page");
			fc.utobj().printTestStep("Add Contact");
			fc.crm().crm_common().CRMContactsLnk(driver);
			fc.utobj().clickElement(driver, pobj.addNew);
			fc.utobj().clickElement(driver, pobj.contactLnk);

			String firstName = fc.utobj().generateTestData(dataSet.get("firstName"));
			String lastName = fc.utobj().generateTestData(dataSet.get("lastName"));
			String primaryCMethod = dataSet.get("primaryCMethod");
			String assignTo = "Corporate";
			String regionName = "";
			String franchiseId = "";
			String title = "Mr.";
			String email = "automation@gmail.com";
			String userName = "FranConnect Administrator";

			fc.utobj().selectDropDown(driver, pobj.contactType, contactType);
			fc.utobj().selectDropDown(driver, pobj.title, title);
			fc.utobj().sendKeys(driver, pobj.contactFirstName, firstName);
			fc.utobj().sendKeys(driver, pobj.contactLastName, lastName);
			fc.utobj().sendKeys(driver, pobj.suffix, "suffixTest");
			fc.utobj().sendKeys(driver, pobj.position, "TestJobTitle");
			fc.utobj().sendKeys(driver, pobj.address, "TestAddress");
			fc.utobj().sendKeys(driver, pobj.city, "TestCity");
			fc.utobj().selectDropDown(driver, pobj.country, "USA");
			fc.utobj().selectDropDown(driver, pobj.state, "Alabama");
			fc.utobj().selectDropDown(driver, pobj.primaryContactMethodSelect, primaryCMethod);
			fc.utobj().sendKeys(driver, pobj.bestTimeToContact, "12:00 PM EST");
			fc.utobj().sendKeys(driver, pobj.zipcode, "12345");
			fc.utobj().sendKeys(driver, pobj.phoneNumbers, "1236547896");
			fc.utobj().sendKeys(driver, pobj.extn, "12");
			fc.utobj().sendKeys(driver, pobj.faxNumbers, "1263985487");
			fc.utobj().sendKeys(driver, pobj.mobileNumbers, "8896724441");
			fc.utobj().sendKeys(driver, pobj.emailIds, email);
			fc.utobj().sendKeys(driver, pobj.alternateEmail, "test@gmail.com");
			// fc.utobj().sendKeys(driver, pobj.birthdate, "11/24/1980");
			// fc.utobj().sendKeys(driver, pobj.anniversarydate, "11/24/1999");
			fc.utobj().sendKeys(driver,
					driver.findElement(
							By.xpath(".//td[contains(text () , '" + FieldName1 + "')]/following-sibling::td/input")),
					FieldName1);
			fc.utobj().sendKeys(driver,
					fc.utobj().getElementByXpath(driver,
							".//td[contains(text () , '" + FieldName2 + "')]/following-sibling::td/label/textarea"),
					FieldName2);
			fc.utobj().sendKeys(driver, fc.utobj().getElementByXpath(driver,
					".//td[contains(text () , '" + FieldName3 + "')]/following-sibling::td/table/tbody/tr/td/input"),
					"02/02/2017");
			fc.utobj()
					.clickElement(driver,
							fc.utobj().getElementByXpath(driver,
									".//td[contains(text () , '" + FieldName4
											+ "')]/following-sibling::td/select/option[contains(text(),'Opt"
											+ FieldName4 + "')]"));
			try {
				fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//td[contains(text () , '"
						+ FieldName5 + "')]/following-sibling::td/input[@type='radio' and @value='1']"));

			} catch (Exception e) {
				// TODO: handle exception
			}
			fc.utobj().sendKeys(driver,
					driver.findElement(
							By.xpath(".//td[contains(text () , '" + FieldName7 + "')]/following-sibling::td/input")),
					"99");
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,(".//td[contains(text () , '" + FieldName6 + "')]/following-sibling::td/input")));
			try {
				fc.utobj().clickElement(driver,
						fc.utobj().getElementByXpath(driver,
								".//td[contains(text () , '" + FieldName8
										+ "')]/following-sibling::td/div/div/ul/li/label[contains(text(),'Mul"
										+ FieldName8 + "')]/input"));
			} catch (Exception e) {
				// TODO: handle exception
			}
			String fileName = fc.utobj().getFilePathFromTestData(dataSet.get("fileName"));
			fc.utobj().sendKeys(driver, fc.utobj().getElementByXpath(driver,
					".//td[contains(text () , '" + FieldName9 + "')]/following-sibling::td/input[@type='file']"),
					fileName);

			if (assignTo.equalsIgnoreCase("Corporate")) {

				if (!fc.utobj().isSelected(driver, pobj.assignToCorporate)) {
					fc.utobj().clickElement(driver, pobj.assignToCorporate);
				}

				fc.utobj().selectDropDown(driver, pobj.selectCorporateUserContact, userName);

			} else if (assignTo.equalsIgnoreCase("Regional")) {

				if (!fc.utobj().isSelected(driver, pobj.assignToRegional)) {
					fc.utobj().clickElement(driver, pobj.assignToRegional);
				}

				fc.utobj().selectDropDown(driver, pobj.selectAreaRegion, regionName);
				fc.utobj().selectDropDown(driver, pobj.selectRegionalUser, userName);

			} else if (assignTo.equalsIgnoreCase("Franchise")) {

				if (!fc.utobj().isSelected(driver, pobj.assignToFranchise)) {
					fc.utobj().clickElement(driver, pobj.assignToFranchise);
				}

				fc.utobj().selectDropDown(driver, pobj.selectFranchiseId, franchiseId);
				fc.utobj().selectDropDown(driver, pobj.selectFranchiseUser, userName);

			}
			fc.utobj().sendKeys(driver, pobj.comments, "Test Comment");
			fc.utobj().clickElement(driver, pobj.saveBtn);

			fc.utobj().clickElement(driver, pobj.exportLink);
			fc.utobj().clickElement(driver, pobj.contactExportbtn);
			fc.utobj().clickElement(driver, pobj.proceedBtn);
			fc.utobj().clickElement(driver, pobj.searchDataBtn);

			boolean isVisibilityPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//*[contains(text () ,'" + FieldName1 + "')]");
			if (isVisibilityPresent == false) {
				fc.utobj().throwsException("was not able to " + FieldName1);
			}
			boolean isVisibilityPresent2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//*[contains(text () ,'" + FieldName2 + "')]");
			if (isVisibilityPresent2 == false) {
				fc.utobj().throwsException("was not able to " + FieldName2);
			}
			boolean isVisibilityPresent3 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//*[contains(text () ,'" + FieldName3 + "')]");
			if (isVisibilityPresent3 == false) {
				fc.utobj().throwsException("was not able to " + FieldName3);
			}
			try {
				boolean isVisibilityPresent4 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
						".//*[contains(text () ,'" + FieldName4 + "')]");
				if (isVisibilityPresent4 == false) {
					fc.utobj().throwsException("was not able to " + FieldName4);
				}
			} catch (Exception e) {
				// TODO: handle exception
			}
			try {
				boolean isVisibilityPresent8 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
						".//*[contains(text () ,'" + FieldName8 + "')]");
				if (isVisibilityPresent8 == false) {
					fc.utobj().throwsException("was not able to " + FieldName4);
				}
			} catch (Exception e) {
				// TODO: handle exception
			}
			boolean isVisibilityPresent5 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//*[contains(text () ,'" + FieldName7 + "')]");
			if (isVisibilityPresent5 == false) {
				fc.utobj().throwsException("was not able to " + FieldName6);
			}
			boolean isVisibilityPresent6 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//*[contains(text () ,'" + FieldName6 + "')]");
			if (isVisibilityPresent6 == false) {
				fc.utobj().throwsException("was not able to " + FieldName6);
			}

			fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}

	}

	/* Anukaran Mishra */

	@Test(groups = "crmmodulewaitformgen")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Manage Form Generator>Create all types of fields against Lead>Verify visibility against lead in export and search.", testCaseId = "TC_323_Verify_MangeFormGenerator_Custom_Fields_Lead")
	public void VerifyFormGeneratorCustomFieldsLeads()
			throws IOException, Exception, ParserConfigurationException, SAXException {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			CRMLeadsPage nnpobj = new CRMLeadsPage(driver);
			AdminCRMConfigureLeadTypePageTest npobj = new AdminCRMConfigureLeadTypePageTest();
			CRMContactsPage pobj = new CRMContactsPage(driver);

			fc.crm().crm_common().adminCRMManageFormGeneratorLnk(driver);
			AdminInfoMgrCommonMethods adminCommonMethods = fc.adminpage().adminInfoMgr();
			AdminInfoMgrManageFormGeneratorPage manageFormGeneratorPage = new AdminInfoMgrManageFormGeneratorPage(
					driver);
			AdminInfoMgrManageFormGeneratorTabDetailsPage ADM_form_gen = manageFormGeneratorPage.getAddTabDetailsPage();
			adminCommonMethods.NavigateToManageFormGeneratorTabs(driver, "Lead");
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//*[@id='tabdetails']//a[contains(text(),'Lead Info')]"));
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//*[@id='siteMainTable']//tr/td[contains(text(),'Lead Information')]/following-sibling::td/a[contains(text(),'Add New Field')]"));
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			String FieldName1 = "Text";
			FieldName1 = fc.utobj().generateTestData(FieldName1);
			fc.utobj().sendKeys(driver, ADM_form_gen.txtFieldName, FieldName1);
			// fc.utobj().clickElement(driver, ADM_form_gen.documentopt);
			fc.utobj().selectDropDown(driver, ADM_form_gen.FieldType, "Text");
			fc.utobj().clickElement(driver, ADM_form_gen.btnSumbitField);
			fc.utobj().switchFrameToDefault(driver);
			// fc.utobj().clickElement(driver,
			// fc.utobj().getElementByXpath(driver,".//*[@id='siteMainTable']//tr/td[contains(text(),'"+FieldName1+"')]/following-sibling::td[5]/a/img")));

			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//*[@id='siteMainTable']//tr/td[contains(text(),'Lead Information')]/following-sibling::td/a[contains(text(),'Add New Field')]"));
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			String FieldName2 = "Text Area";
			FieldName2 = fc.utobj().generateTestData(FieldName2);
			fc.utobj().sendKeys(driver, ADM_form_gen.txtFieldName, FieldName2);
			// fc.utobj().clickElement(driver, ADM_form_gen.documentopt);
			fc.utobj().selectDropDown(driver, ADM_form_gen.FieldType, "Text Area");
			fc.utobj().clickElement(driver, ADM_form_gen.btnSumbitField);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//*[@id='siteMainTable']//tr/td[contains(text(),'Lead Information')]/following-sibling::td/a[contains(text(),'Add New Field')]"));
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			String FieldName3 = "Date";
			FieldName3 = fc.utobj().generateTestData(FieldName3);
			fc.utobj().sendKeys(driver, ADM_form_gen.txtFieldName, FieldName3);
			// fc.utobj().clickElement(driver, ADM_form_gen.documentopt);
			fc.utobj().selectDropDown(driver, ADM_form_gen.FieldType, "Date");
			fc.utobj().clickElement(driver, ADM_form_gen.btnSumbitField);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//*[@id='siteMainTable']//tr/td[contains(text(),'Lead Information')]/following-sibling::td/a[contains(text(),'Add New Field')]"));
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			String FieldName4 = "DropDown";
			FieldName4 = fc.utobj().generateTestData(FieldName4);
			fc.utobj().sendKeys(driver, ADM_form_gen.txtFieldName, FieldName4);
			// fc.utobj().clickElement(driver, ADM_form_gen.documentopt);
			fc.utobj().selectDropDown(driver, ADM_form_gen.FieldType, "Drop-down");
			fc.utobj().sendKeys(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='row_1']/td[2]/input[1]"),
					"Opt" + FieldName4);

			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//*[@id='removeTDForAddMore']//*[contains(text(),'Add Option')]"));
			fc.utobj().sendKeys(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='row_2']/td[2]/input[1]"),
					"Opt2");
			fc.utobj().clickElement(driver, ADM_form_gen.btnSumbitField);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//*[@id='siteMainTable']//tr/td[contains(text(),'Lead Information')]/following-sibling::td/a[contains(text(),'Add New Field')]"));
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			String FieldName5 = "Radio";
			FieldName5 = fc.utobj().generateTestData(FieldName5);
			fc.utobj().sendKeys(driver, ADM_form_gen.txtFieldName, FieldName5);
			// fc.utobj().clickElement(driver, ADM_form_gen.documentopt);
			fc.utobj().selectDropDown(driver, ADM_form_gen.FieldType, "Radio");
			fc.utobj().sendKeys(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='row_1']/td[2]/input[1]"),
					"Opt" + FieldName5);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//*[@id='removeTDForAddMore']//*[contains(text(),'Add Option')]"));
			fc.utobj().sendKeys(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='row_2']/td[2]/input[1]"),
					"Opt2" + FieldName5);
			fc.utobj().clickElement(driver, ADM_form_gen.btnSumbitField);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//*[@id='siteMainTable']//tr/td[contains(text(),'Lead Information')]/following-sibling::td/a[contains(text(),'Add New Field')]"));
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			String FieldName6 = "Checkbox";
			FieldName6 = fc.utobj().generateTestData(FieldName6);
			fc.utobj().sendKeys(driver, ADM_form_gen.txtFieldName, FieldName6);
			// fc.utobj().clickElement(driver, ADM_form_gen.documentopt);
			fc.utobj().selectDropDown(driver, ADM_form_gen.FieldType, "Checkbox");
			fc.utobj().sendKeys(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='row_1']/td[2]/input[1]"),
					"Opt1");
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//*[@id='removeTDForAddMore']//*[contains(text(),'Add Option')]"));
			fc.utobj().sendKeys(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='row_2']/td[2]/input[1]"),
					"Opt2");
			fc.utobj().clickElement(driver, ADM_form_gen.btnSumbitField);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//*[@id='siteMainTable']//tr/td[contains(text(),'Lead Information')]/following-sibling::td/a[contains(text(),'Add New Field')]"));
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			String FieldName7 = "Test Field3";
			FieldName7 = fc.utobj().generateTestData(FieldName7);
			fc.utobj().sendKeys(driver, ADM_form_gen.txtFieldName, FieldName7);
			// fc.utobj().clickElement(driver, ADM_form_gen.documentopt);
			fc.utobj().selectDropDown(driver, ADM_form_gen.FieldType, "Numeric");
			fc.utobj().clickElement(driver, ADM_form_gen.btnSumbitField);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//*[@id='siteMainTable']//tr/td[contains(text(),'Lead Information')]/following-sibling::td/a[contains(text(),'Add New Field')]"));
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			String FieldName8 = "Multislect";
			FieldName8 = fc.utobj().generateTestData(FieldName8);
			fc.utobj().sendKeys(driver, ADM_form_gen.txtFieldName, FieldName8);
			// fc.utobj().clickElement(driver, ADM_form_gen.documentopt);
			fc.utobj().selectDropDown(driver, ADM_form_gen.FieldType, "Multi Select Drop-down");
			fc.utobj().sendKeys(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='row_1']/td[2]/input[1]"),
					"Mul" + FieldName8);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//*[@id='removeTDForAddMore']//*[contains(text(),'Add Option')]"));
			fc.utobj().sendKeys(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='row_2']/td[2]/input[1]"),
					"Opt2");
			fc.utobj().clickElement(driver, ADM_form_gen.btnSumbitField);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//*[@id='siteMainTable']//tr/td[contains(text(),'Lead Information')]/following-sibling::td/a[contains(text(),'Add New Field')]"));
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			String FieldName9 = "Test Field3";
			FieldName9 = fc.utobj().generateTestData(FieldName9);
			fc.utobj().sendKeys(driver, ADM_form_gen.txtFieldName, FieldName9);
			// fc.utobj().clickElement(driver, ADM_form_gen.documentopt);
			fc.utobj().selectDropDown(driver, ADM_form_gen.FieldType, "Document");
			fc.utobj().clickElement(driver, ADM_form_gen.btnSumbitField);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Navigate To Admin > CRM > Lead Type Configuration");
			fc.utobj().printTestStep("Add Lead Type");
			String leadType = fc.utobj().generateTestData(dataSet.get("leadType"));
			npobj.addLeadType(driver, leadType);

			/*
			 * fc.utobj().printTestStep(testCaseId,
			 * "Navigate To Admin > CRM > Contact Type Configuration");
			 * fc.utobj().printTestStep(testCaseId, "Add Contact Type");
			 * AdminCRMContactTypeConfigurationPageTest contatcTypePage = new
			 * AdminCRMContactTypeConfigurationPageTest(); String contactType =
			 * fc.utobj().generateTestData(dataSet.get("contactType"));
			 * contatcTypePage.addContactType(driver, contactType, config);
			 */

			/*
			 * fc.utobj().printTestStep(testCaseId,
			 * "Navigate To Admin > CRM > Configure Status");
			 * fc.utobj().printTestStep(testCaseId, "Add Status For Contact");
			 * AdminCRMConfigureStatusPageTest statusPage=new
			 * AdminCRMConfigureStatusPageTest(); String
			 * statusName=fc.utobj().generateTestData(dataSet.get("status"));
			 * statusPage.addStatus01(driver, statusName, config);
			 */

			fc.utobj().printTestStep("Navigate To CRM > Contacts > Contacts Summary Page");
			fc.utobj().printTestStep("Add Lead");
			fc.crm().crm_common().CRMLeadsLnk(driver);
			fc.utobj().clickElement(driver, nnpobj.addNew);
			fc.utobj().clickElement(driver, nnpobj.leadLnk);

			String firstName = fc.utobj().generateTestData(dataSet.get("firstName"));
			String lastName = fc.utobj().generateTestData(dataSet.get("lastName"));
			String primaryCMethod = dataSet.get("primaryCMethod");
			String assignTo = "Corporate";
			String title = "Mr.";
			String email = "automation@gmail.com";
			String userName = "FranConnect Administrator";
			String city = "city";
			String address = "address";

			fc.utobj().selectDropDown(driver, nnpobj.title, title);
			fc.utobj().sendKeys(driver, nnpobj.leadFirstName, firstName);
			fc.utobj().sendKeys(driver, nnpobj.leadLastName, lastName);
			fc.utobj().selectDropDown(driver, nnpobj.primaryContactMethodSelect, primaryCMethod);

			if (assignTo.equalsIgnoreCase("Corporate")) {
				if (!fc.utobj().isSelected(driver, nnpobj.assignToCorporate)) {
					fc.utobj().clickElement(driver, nnpobj.assignToCorporate);
				}
				fc.utobj().selectDropDown(driver, nnpobj.selectCorporateUser, userName);
			}
			fc.utobj().sendKeys(driver, nnpobj.address, address);
			fc.utobj().sendKeys(driver, nnpobj.city, city);
			fc.utobj().selectDropDown(driver, nnpobj.country, "USA");
			fc.utobj().selectDropDown(driver, nnpobj.state, "Alabama");
			fc.utobj().sendKeys(driver, nnpobj.zipcode, "12345");
			fc.utobj().sendKeys(driver, nnpobj.phoneNumbers, "1236547896");
			fc.utobj().sendKeys(driver, nnpobj.extn, "12");
			fc.utobj().sendKeys(driver, nnpobj.faxNumbers, "1263985487");
			fc.utobj().sendKeys(driver, nnpobj.mobileNumbers, "8896724441");
			fc.utobj().sendKeys(driver, nnpobj.emailIds, email);
			fc.utobj().sendKeys(driver, nnpobj.alternateEmail, "test@gmail.com");
			// fc.utobj().sendKeys(driver, nnpobj.suffix, suffix);
			// fc.utobj().sendKeys(driver, nnpobj.position, jobTitle);
			fc.utobj().sendKeys(driver, nnpobj.birthdate, "11/24/1980");
			fc.utobj().sendKeys(driver, nnpobj.anniversarydate, "11/24/1999");
			fc.utobj().selectDropDown(driver, nnpobj.leadType, leadType);
			fc.utobj().selectDropDown(driver, nnpobj.rating, dataSet.get("rating"));
			fc.utobj().sendKeys(driver, nnpobj.bestTimeToContact, "12:00 PM EST");
			// fc.utobj().sendKeys(driver, pobj.birthdate, "11/24/1980");
			// fc.utobj().sendKeys(driver, pobj.anniversarydate, "11/24/1999");
			fc.utobj().sendKeys(driver,
					driver.findElement(
							By.xpath(".//td[contains(text () , '" + FieldName1 + "')]/following-sibling::td/input")),
					FieldName1);
			fc.utobj().sendKeys(driver,
					fc.utobj().getElementByXpath(driver,
							".//td[contains(text () , '" + FieldName2 + "')]/following-sibling::td/label/textarea"),
					FieldName2);
			fc.utobj().sendKeys(driver, fc.utobj().getElementByXpath(driver,
					".//td[contains(text () , '" + FieldName3 + "')]/following-sibling::td/table/tbody/tr/td/input"),
					"02/02/2017");
			fc.utobj()
					.clickElement(driver,
							fc.utobj().getElementByXpath(driver,
									".//td[contains(text () , '" + FieldName4
											+ "')]/following-sibling::td/select/option[contains(text(),'Opt"
											+ FieldName4 + "')]"));
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//td[contains(text () , '"
					+ FieldName5 + "')]/following-sibling::td/input[@type='radio' and @value='1']"));
			fc.utobj().sendKeys(driver,
					driver.findElement(
							By.xpath(".//td[contains(text () , '" + FieldName7 + "')]/following-sibling::td/input")),
					"99");
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,(".//td[contains(text () , '" + FieldName6 + "')]/following-sibling::td/input")));
			try {
				fc.utobj().clickElement(driver,
						fc.utobj().getElementByXpath(driver,
								".//td[contains(text () , '" + FieldName8
										+ "')]/following-sibling::td/div/div/ul/li/label[contains(text(),'Mul"
										+ FieldName8 + "')]/input"));
			} catch (Exception e) {
				// TODO: handle exception
			}
			String fileName = fc.utobj().getFilePathFromTestData(dataSet.get("fileName"));
			fc.utobj().sendKeys(driver, fc.utobj().getElementByXpath(driver,
					".//td[contains(text () , '" + FieldName9 + "')]/following-sibling::td/input[@type='file']"),
					fileName);

			fc.utobj().clickElement(driver, nnpobj.saveBtn);

			fc.utobj().clickElement(driver, pobj.exportLink);
			fc.utobj().clickElement(driver, nnpobj.leadExportBtn);
			fc.utobj().clickElement(driver, pobj.proceedBtn);
			fc.utobj().clickElement(driver, pobj.searchDataBtn);

			boolean isVisibilityPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//*[contains(text () ,'" + FieldName1 + "')]");
			if (isVisibilityPresent == false) {
				fc.utobj().throwsException("was not able to " + FieldName1);
			}
			boolean isVisibilityPresent2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//*[contains(text () ,'" + FieldName2 + "')]");
			if (isVisibilityPresent2 == false) {
				fc.utobj().throwsException("was not able to " + FieldName2);
			}
			boolean isVisibilityPresent3 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//*[contains(text () ,'" + FieldName3 + "')]");
			if (isVisibilityPresent3 == false) {
				fc.utobj().throwsException("was not able to " + FieldName3);
			}
			try {
				boolean isVisibilityPresent4 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
						".//*[contains(text () ,'" + FieldName4 + "')]");
				if (isVisibilityPresent4 == false) {
					fc.utobj().throwsException("was not able to " + FieldName4);
				}
			} catch (Exception e) {
			}
			try {
				boolean isVisibilityPresent8 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
						".//*[contains(text () ,'" + FieldName8 + "')]");
				if (isVisibilityPresent8 == false) {
					fc.utobj().throwsException("was not able to " + FieldName4);
				}
			} catch (Exception e) {
			}
			boolean isVisibilityPresent5 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//*[contains(text () ,'" + FieldName7 + "')]");
			if (isVisibilityPresent5 == false) {
				fc.utobj().throwsException("was not able to " + FieldName6);
			}
			boolean isVisibilityPresent6 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//*[contains(text () ,'" + FieldName6 + "')]");
			if (isVisibilityPresent6 == false) {
				fc.utobj().throwsException("was not able to " + FieldName6);
			}

			fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}

	}

	/*
	 * 
	 * 
	 * //Anukaran Mishra
	 * 
	 * 
	 * @Test(groups = "crmmodule")
	 * 
	 * @TestCase(createdOn="2017-05-01",updatedOn="2018-06-14",
	 * testCaseDescription =
	 * "Add Section with all types of custom fields under the new tab > add a contact through External Form"
	 * , testCaseId = "TC_324_Verify_New_Conatact_Tab") public void
	 * verifyNewContactTab() throws IOException, Exception,
	 * ParserConfigurationException, SAXException { String testCaseId =
	 * fc.utobj().readTestCaseInfo(this.getClass().getName()+"."+new
	 * Object(){}.getClass().getEnclosingMethod().getName());
	 * 
	 * Map<String, String> config =
	 * fc.utobj().readConfigurationFile(FranconnectUtil.path);
	 * Map<String,String> dataSet = fc.utobj().readTestData("crm", testCaseId);
	 * AdminInfoMgrCommonMethods adminCommonMethods =
	 * fc.adminpage().adminInfoMgr(); WebDriver driver =
	 * fc.utobj().openDriver(config); //CRM_CorporateUser crmCU= new
	 * CRM_CorporateUser();
	 * 
	 * try { driver = fc.loginpage().login(driver, config); CRMLeadsPage pobj =
	 * new CRMLeadsPage(driver);
	 * 
	 * fc.utobj().printTestStep(testCaseId,
	 * "Navigate To Admin Users Manage Corporate User");
	 * fc.utobj().printTestStep(testCaseId, "Add Corporate User");
	 * AdminUsersManageCorporateUsersAddCorporateUserPageTest addCorporatePage =
	 * new AdminUsersManageCorporateUsersAddCorporateUserPageTest(); String
	 * CuserName = fc.utobj().generateTestData(dataSet.get("userName")); String
	 * userName = addCorporatePage.addCorporateUser(driver, CuserName, config);
	 * fc.home_page().logout(driver, config);
	 * fc.loginpage().loginWithParameter(driver, CuserName, "T0n1ght1");
	 * 
	 * fc.utobj().printTestStep(testCaseId,
	 * "Navigate To Admin > CRM > Manage Form Generator");
	 * fc.utobj().printTestStep(testCaseId, "Add New Tab"); //
	 * fc.crm().crm_common().CRMLeadsLnk( driver);
	 * 
	 * 
	 * fc.crm().crm_common().adminCRMManageFormGeneratorLnk( driver);
	 * fc.adminpage().adminInfoMgr(); AdminInfoMgrManageFormGeneratorPage
	 * manageFormGeneratorPage = new
	 * AdminInfoMgrManageFormGeneratorPage(driver);
	 * AdminInfoMgrManageFormGeneratorTabDetailsPage ADM_form_gen =
	 * manageFormGeneratorPage.getAddTabDetailsPage();
	 * adminCommonMethods.NavigateToManageFormGeneratorTabs(driver, "Contact");
	 * 
	 * fc.utobj().clickElement(driver, manageFormGeneratorPage.btnAddNewTab);
	 * fc.commonMethods().switch_cboxIframe_frameId(driver);
	 * AdminInfoMgrManageFormGeneratorAddTabPage objAddNewTabPage = new
	 * AdminInfoMgrManageFormGeneratorAddTabPage(driver); String newTabName =
	 * fc.utobj().generateTestData("New Tab"); fc.utobj().sendKeys(driver,
	 * objAddNewTabPage.txtDisplayName, newTabName);
	 * fc.utobj().clickElement(driver, objAddNewTabPage.multipleInputNo);
	 * fc.utobj().clickElement(driver, objAddNewTabPage.btnSubmit);
	 * fc.utobj().switchFrameToDefault(driver); fc.utobj().clickLink(driver,
	 * newTabName);
	 * 
	 * 
	 * fc.utobj().clickElement(driver, ADM_form_gen.btnAddSection);
	 * 
	 * fc.commonMethods().switch_cboxIframe_frameId(driver); String SectionName
	 * = "New Section"; SectionName = fc.utobj().generateTestData(SectionName);
	 * 
	 * fc.utobj().sendKeys(driver, ADM_form_gen.txtSectionName, SectionName);
	 * fc.utobj().clickElement(driver, ADM_form_gen.btnAddSectionName);
	 * fc.utobj().switchFrameToDefault(driver); fc.utobj().clickElement(driver,
	 * ADM_form_gen.btnAddField);
	 * fc.commonMethods().switch_cboxIframe_frameId(driver); String FieldName2=
	 * "Text Area"; FieldName2 = fc.utobj().generateTestData(FieldName2);
	 * fc.utobj().sendKeys(driver, ADM_form_gen.txtFieldName, FieldName2);
	 * //fc.utobj().clickElement(driver, ADM_form_gen.documentopt);
	 * fc.utobj().selectDropDown(driver, ADM_form_gen.FieldType, "Text Area");
	 * fc.utobj().clickElement(driver, ADM_form_gen.btnSumbitField);
	 * fc.utobj().switchFrameToDefault(driver);
	 * 
	 * fc.utobj().clickElement(driver, ADM_form_gen.btnAddField);
	 * fc.commonMethods().switch_cboxIframe_frameId(driver); String
	 * FieldName1="Text"; FieldName1 = fc.utobj().generateTestData(FieldName1);
	 * fc.utobj().sendKeys(driver, ADM_form_gen.txtFieldName, FieldName1);
	 * //fc.utobj().clickElement(driver, ADM_form_gen.documentopt);
	 * fc.utobj().selectDropDown(driver, ADM_form_gen.FieldType, "Text");
	 * fc.utobj().clickElement(driver, ADM_form_gen.btnSumbitField);
	 * fc.utobj().switchFrameToDefault(driver);
	 * 
	 * fc.utobj().clickElement(driver, ADM_form_gen.btnAddField);
	 * fc.commonMethods().switch_cboxIframe_frameId(driver); String
	 * FieldName3="Date"; FieldName3 = fc.utobj().generateTestData(FieldName3);
	 * fc.utobj().sendKeys(driver, ADM_form_gen.txtFieldName, FieldName3);
	 * //fc.utobj().clickElement(driver, ADM_form_gen.documentopt);
	 * fc.utobj().selectDropDown(driver, ADM_form_gen.FieldType, "Date");
	 * fc.utobj().clickElement(driver, ADM_form_gen.btnSumbitField);
	 * fc.utobj().switchFrameToDefault(driver);
	 * 
	 * 
	 * 
	 * 
	 * fc.utobj().clickElement(driver, ADM_form_gen.btnAddField);
	 * fc.commonMethods().switch_cboxIframe_frameId(driver); String
	 * FieldName4="DropDown"; FieldName4 =
	 * fc.utobj().generateTestData(FieldName4); fc.utobj().sendKeys(driver,
	 * ADM_form_gen.txtFieldName, FieldName4); //fc.utobj().clickElement(driver,
	 * ADM_form_gen.documentopt); fc.utobj().selectDropDown(driver,
	 * ADM_form_gen.FieldType, "Drop-down"); fc.utobj().sendKeys(driver,
	 * fc.utobj().getElementByXpath(driver,".//*[@id='row_1']/td[2]/input[1]")),
	 * "Opt"+FieldName4);
	 * 
	 * fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
	 * ".//*[@id='removeTDForAddMore']//*[contains(text(),'Add Option')]")));
	 * fc.utobj().sendKeys(driver,
	 * fc.utobj().getElementByXpath(driver,".//*[@id='row_2']/td[2]/input[1]")),
	 * "Opt2"); fc.utobj().clickElement(driver, ADM_form_gen.btnSumbitField);
	 * fc.utobj().switchFrameToDefault(driver);
	 * 
	 * fc.utobj().clickElement(driver, ADM_form_gen.btnAddField);
	 * fc.commonMethods().switch_cboxIframe_frameId(driver); String
	 * FieldName5="Radio"; FieldName5 = fc.utobj().generateTestData(FieldName5);
	 * fc.utobj().sendKeys(driver, ADM_form_gen.txtFieldName, FieldName5);
	 * //fc.utobj().clickElement(driver, ADM_form_gen.documentopt);
	 * fc.utobj().selectDropDown(driver, ADM_form_gen.FieldType, "Radio");
	 * fc.utobj().sendKeys(driver,
	 * fc.utobj().getElementByXpath(driver,".//*[@id='row_1']/td[2]/input[1]")),
	 * "Opt"+FieldName5); fc.utobj().clickElement(driver,
	 * fc.utobj().getElementByXpath(driver,
	 * ".//*[@id='removeTDForAddMore']//*[contains(text(),'Add Option')]")));
	 * fc.utobj().sendKeys(driver,
	 * fc.utobj().getElementByXpath(driver,".//*[@id='row_2']/td[2]/input[1]")),
	 * "Opt2"+FieldName5); fc.utobj().clickElement(driver,
	 * ADM_form_gen.btnSumbitField); fc.utobj().switchFrameToDefault(driver);
	 * 
	 * fc.utobj().clickElement(driver, ADM_form_gen.btnAddField);
	 * fc.commonMethods().switch_cboxIframe_frameId(driver); String
	 * FieldName6="Checkbox"; FieldName6 =
	 * fc.utobj().generateTestData(FieldName6); fc.utobj().sendKeys(driver,
	 * ADM_form_gen.txtFieldName, FieldName6); //fc.utobj().clickElement(driver,
	 * ADM_form_gen.documentopt); fc.utobj().selectDropDown(driver,
	 * ADM_form_gen.FieldType, "Checkbox"); fc.utobj().sendKeys(driver,
	 * fc.utobj().getElementByXpath(driver,".//*[@id='row_1']/td[2]/input[1]")),
	 * "Opt1"); fc.utobj().clickElement(driver,
	 * fc.utobj().getElementByXpath(driver,
	 * ".//*[@id='removeTDForAddMore']//*[contains(text(),'Add Option')]")));
	 * fc.utobj().sendKeys(driver,
	 * fc.utobj().getElementByXpath(driver,".//*[@id='row_2']/td[2]/input[1]")),
	 * "Opt2"); fc.utobj().clickElement(driver, ADM_form_gen.btnSumbitField);
	 * fc.utobj().switchFrameToDefault(driver);
	 * 
	 * fc.utobj().clickElement(driver, ADM_form_gen.btnAddField);
	 * fc.commonMethods().switch_cboxIframe_frameId(driver); String FieldName7=
	 * "Test Field3"; FieldName7 = fc.utobj().generateTestData(FieldName7);
	 * fc.utobj().sendKeys(driver, ADM_form_gen.txtFieldName, FieldName7);
	 * //fc.utobj().clickElement(driver, ADM_form_gen.documentopt);
	 * fc.utobj().selectDropDown(driver, ADM_form_gen.FieldType, "Numeric");
	 * fc.utobj().clickElement(driver, ADM_form_gen.btnSumbitField);
	 * fc.utobj().switchFrameToDefault(driver);
	 * 
	 * fc.utobj().clickElement(driver, ADM_form_gen.btnAddField);
	 * fc.commonMethods().switch_cboxIframe_frameId(driver); String
	 * FieldName8="Multislect"; FieldName8 =
	 * fc.utobj().generateTestData(FieldName8); fc.utobj().sendKeys(driver,
	 * ADM_form_gen.txtFieldName, FieldName8); //fc.utobj().clickElement(driver,
	 * ADM_form_gen.documentopt); fc.utobj().selectDropDown(driver,
	 * ADM_form_gen.FieldType, "Multi Select Drop-down");
	 * fc.utobj().sendKeys(driver,
	 * fc.utobj().getElementByXpath(driver,".//*[@id='row_1']/td[2]/input[1]")),
	 * "Mul"+FieldName8); fc.utobj().clickElement(driver,
	 * fc.utobj().getElementByXpath(driver,
	 * ".//*[@id='removeTDForAddMore']//*[contains(text(),'Add Option')]")));
	 * fc.utobj().sendKeys(driver,
	 * fc.utobj().getElementByXpath(driver,".//*[@id='row_2']/td[2]/input[1]")),
	 * "Opt2"); fc.utobj().clickElement(driver, ADM_form_gen.btnSumbitField);
	 * fc.utobj().switchFrameToDefault(driver);
	 * 
	 * 
	 * fc.utobj().clickElement(driver, ADM_form_gen.btnAddField);
	 * fc.commonMethods().switch_cboxIframe_frameId(driver); String FieldName9=
	 * "Test Field3"; FieldName9 = fc.utobj().generateTestData(FieldName9);
	 * fc.utobj().sendKeys(driver, ADM_form_gen.txtFieldName, FieldName9);
	 * //fc.utobj().clickElement(driver, ADM_form_gen.documentopt);
	 * fc.utobj().selectDropDown(driver, ADM_form_gen.FieldType, "Document");
	 * fc.utobj().clickElement(driver, ADM_form_gen.btnSumbitField);
	 * fc.utobj().switchFrameToDefault(driver);
	 * 
	 * 
	 * 
	 * AdminCRMManageWebFormGeneratorPageTest webFormPage = new
	 * AdminCRMManageWebFormGeneratorPageTest(); //create Web form (Lead) String
	 * status ="New"; String contactMedium ="Email"; String leadSource="Import";
	 * String sourceDetails="Import"; fc.utobj().printTestStep(testCaseId,
	 * "Navigate To Admin > CRM > Manage Web Form Generator");
	 * fc.utobj().printTestStep(testCaseId, "Create New Lead Form"); String
	 * formFormat="Single Page"; String
	 * header=fc.utobj().generateTestData(dataSet.get("header")); String
	 * textLabel=fc.utobj().generateTestData(dataSet.get("textLabel")); String
	 * fieldLabel=fc.utobj().generateTestData(dataSet.get("fieldLabel")); String
	 * footer=fc.utobj().generateTestData(dataSet.get("footer")); String
	 * assignTo="Default Owner"; String
	 * submissionType=dataSet.get("submissionType"); String
	 * formName=fc.utobj().generateTestData(dataSet.get("formName")); String
	 * formTitle=fc.utobj().generateTestData(dataSet.get("formTitle")); String
	 * description=fc.utobj().generateTestData(dataSet.get("description"));
	 * String formUrl=fc.utobj().generateTestData(dataSet.get("formUrl"));
	 * String sectionName=fc.utobj().generateTestData("sectionName"); String
	 * userNameReg=""; String userNameFranchise=""; String
	 * afterSubmission=dataSet.get("afterSubmission"); String
	 * tabName=fc.utobj().generateTestData(dataSet.get("tabName")); String
	 * redirectedUrl=dataSet.get("reUrl"); AdminCRMManageWebFormGeneratorPage
	 * CrmWebFormPage=new AdminCRMManageWebFormGeneratorPage(driver); String
	 * LName=fc.utobj().generateTestData(dataSet.get("Lastname")); String
	 * FName=fc.utobj().generateTestData(dataSet.get("Firstname"));
	 * //leadSource=sourceDetails; String userNameCorp=
	 * "FranConnect Administrator"; String regionName=""; String franchiseId="";
	 * 
	 * 
	 * 
	 * AdminCRMContactTypeConfigurationPageTest contatcTypePage = new
	 * AdminCRMContactTypeConfigurationPageTest();
	 * formUrl=fc.utobj().generateTestData(dataSet.get("formUrl")); String
	 * contactType = fc.utobj().generateTestData(dataSet.get("contactType"));
	 * contatcTypePage.addContactType(driver, contactType, config);
	 * //webFormPage.createNewFormContactType(driver, formName, formTitle,
	 * description, formFormat, submissionType, formUrl, sectionName, tabName,
	 * header, textLabel, fieldLabel, footer, contactType, status,
	 * contactMedium, leadSource, sourceDetails, assignTo, userNameCorp,
	 * regionName, userNameReg, franchiseId, userNameFranchise, afterSubmission,
	 * redirectedUrl, config);
	 * 
	 * 
	 * 
	 * 
	 * 
	 * 
	 * 
	 * String urlText=null; try { AdminCRMManageWebFormGeneratorPage
	 * WebForm,webForm=new AdminCRMManageWebFormGeneratorPage(driver);
	 * fc.crm().crm_common().adminCRMManageWebFormGeneratorLnk( driver);
	 * fc.utobj().clickElement(driver, webForm.createNewForm);
	 * fc.utobj().sendKeys(driver, webForm.formName, formName);
	 * fc.utobj().sendKeys(driver, webForm.formDisplayTitle, formTitle); if
	 * (!webForm.displayFormTitleCheckBox) {
	 * fc.utobj().clickElement(driver, webForm.displayFormTitleCheckBox); }
	 * fc.utobj().sendKeys(driver, webForm.formDescription, description);
	 * 
	 * if (!fc.utobj().isSelected(driver,
	 * webForm.formTypeContact)) { fc.utobj().clickElement(driver,
	 * webForm.formTypeContact); } if (formFormat.equalsIgnoreCase("Single Page"
	 * )) { fc.utobj().selectDropDown(driver, webForm.formFormat, "Single Page"
	 * );
	 * 
	 * }else if (formFormat.equalsIgnoreCase("Multi Page")) {
	 * fc.utobj().selectDropDown(driver, webForm.formFormat, "Multi Page");
	 * fc.utobj().selectDropDown(driver, webForm.submissionType,
	 * submissionType); } fc.utobj().selectDropDown(driver, webForm.columnCount,
	 * "1"); fc.utobj().selectDropDown(driver, webForm.filedLabelAlignment,
	 * "Left"); fc.utobj().selectDropDown(driver, webForm.formLanguage,
	 * "English"); fc.utobj().sendKeys(driver, webForm.iframeWidth, "100%");
	 * fc.utobj().sendKeys(driver, webForm.iframeHeight, "500");
	 * fc.utobj().sendKeys(driver, webForm.formUrl, formUrl);
	 * fc.utobj().clickElement(driver, webForm.saveNextBtn);
	 * 
	 * //1.Details Page
	 * 
	 * if (formFormat.equalsIgnoreCase("Single Page")) { //Add Section
	 * fc.utobj().clickElement(driver, webForm.addSection);
	 * fc.commonMethods().switch_cboxIframe_frameId(driver);
	 * fc.utobj().sendKeys(driver, webForm.sectionName, sectionName);
	 * fc.utobj().selectDropDown(driver, webForm.noOfCols, "1");
	 * fc.utobj().selectDropDown(driver, webForm.fieldLabelAlignmentSection,
	 * "Left"); fc.utobj().clickElement(driver, webForm.addBtn);
	 * fc.utobj().switchFrameToDefault(driver); }else if
	 * (formFormat.equalsIgnoreCase("Multi Page")) {
	 * fc.utobj().clickElement(driver, webForm.addTab);
	 * fc.commonMethods().switch_cboxIframe_frameId(driver);
	 * fc.utobj().sendKeys(driver, webForm.sectionName, tabName);
	 * fc.utobj().clickElement(driver, webForm.addBtn);
	 * fc.utobj().switchFrameToDefault(driver); } //add Header
	 * fc.utobj().doubleClickElement(driver, webForm.addHeader);
	 * fc.commonMethods().switch_cboxIframe_frameId(driver);
	 * fc.utobj().switchFrameById(driver, "textBlock_ifr"); WebElement
	 * editorTextArea=fc.utobj().getElementByXpath(driver,
	 * ".//*[@id='tinymce']/p")); Actions actions = new Actions(driver);
	 * actions.moveToElement(editorTextArea); actions.click();
	 * actions.sendKeys(header); fc.utobj().logReport(
	 * "Entering Value in Text Field", editorTextArea); fc.utobj().logReportMsg(
	 * "Entered Text", header); actions.build().perform();
	 * fc.utobj().switchFrameToDefault(driver);
	 * fc.commonMethods().switch_cboxIframe_frameId(driver);
	 * fc.utobj().clickElement(driver, webForm.saveBtn);
	 * fc.utobj().switchFrameToDefault(driver);
	 * 
	 * if (formFormat.equalsIgnoreCase("Multi Page")) {
	 * fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
	 * ".//*[contains(@id , 'defaultTab') and contains(@id , 'Tab') and contains(text () ,'Default Tab')]"
	 * ))); } //Add Text fc.utobj().doubleClickElement(driver,
	 * webForm.defaultSectionHeaderAddText);
	 * fc.commonMethods().switch_cboxIframe_frameId(driver);
	 * fc.utobj().sendKeys(driver, webForm.labelAddText, textLabel);
	 * fc.utobj().selectDropDown(driver, webForm.headerLabelAlignment, "Left");
	 * fc.utobj().clickElement(driver, webForm.saveBtn);
	 * fc.utobj().switchFrameToDefault(driver); //FormTool
	 * fc.utobj().clickElement(driver, webForm.availableFieldsTab);
	 * fc.utobj().clickElement(driver, webForm.formTools);
	 * fc.utobj().dragAndDropElement(driver, webForm.formToolText,
	 * webForm.defaultDropHere); fc.utobj().dragAndDropElement(driver,
	 * webForm.formToolCaptcha, webForm.defaultDropHere);
	 * fc.utobj().dragAndDropElement(driver, webForm.formToolIAgreeCheckBox,
	 * webForm.defaultDropHere);
	 * 
	 * //Contact Info fc.utobj().clickElement(driver, webForm.primaryInfo);
	 * fc.utobj().sendKeys(driver, webForm.searchField, "Title");
	 * fc.utobj().dragAndDropElement(driver,
	 * fc.utobj().getElementByXpath(driver,".//a[.='Title']")),
	 * webForm.defaultDropHere); fc.utobj().sendKeys(driver,
	 * webForm.searchField, "Address"); fc.utobj().dragAndDropElement(driver,
	 * fc.utobj().getElementByXpath(driver,".//a[.='Address']")),
	 * webForm.defaultDropHere); fc.utobj().sendKeys(driver,
	 * webForm.searchField, "City"); fc.utobj().dragAndDropElement(driver,
	 * fc.utobj().getElementByXpath(driver,".//a[.='City']")),
	 * webForm.defaultDropHere); fc.utobj().sendKeys(driver,
	 * webForm.searchField, "Country"); fc.utobj().dragAndDropElement(driver,
	 * fc.utobj().getElementByXpath(driver,".//a[.='Country']")),
	 * webForm.defaultDropHere); fc.utobj().sendKeys(driver,
	 * webForm.searchField, "Zip / Postal Code");
	 * fc.utobj().dragAndDropElement(driver,
	 * fc.utobj().getElementByXpath(driver,".//a[.='Zip / Postal Code']")),
	 * webForm.defaultDropHere); fc.utobj().sendKeys(driver,
	 * webForm.searchField, "State / Province");
	 * fc.utobj().dragAndDropElement(driver,
	 * fc.utobj().getElementByXpath(driver,".//a[.='State / Province']")),
	 * webForm.defaultDropHere); fc.utobj().sendKeys(driver,
	 * webForm.searchField, "Phone"); fc.utobj().dragAndDropElement(driver,
	 * fc.utobj().getElementByXpath(driver,".//a[.='Phone']")),
	 * webForm.defaultDropHere); fc.utobj().sendKeys(driver,
	 * webForm.searchField, "Phone Extension");
	 * fc.utobj().dragAndDropElement(driver,
	 * fc.utobj().getElementByXpath(driver,".//a[.='Phone Extension']")),
	 * webForm.defaultDropHere); fc.utobj().sendKeys(driver,
	 * webForm.searchField, "Fax"); fc.utobj().dragAndDropElement(driver,
	 * fc.utobj().getElementByXpath(driver,".//a[.='Fax']")),
	 * webForm.defaultDropHere); fc.utobj().sendKeys(driver,
	 * webForm.searchField, "Mobile"); fc.utobj().dragAndDropElement(driver,
	 * fc.utobj().getElementByXpath(driver,".//a[.='Mobile']")),
	 * webForm.defaultDropHere); fc.utobj().sendKeys(driver,
	 * webForm.searchField, "Alternate Email");
	 * fc.utobj().dragAndDropElement(driver,
	 * fc.utobj().getElementByXpath(driver,".//a[.='Alternate Email']")),
	 * webForm.defaultDropHere); fc.utobj().sendKeys(driver,
	 * webForm.searchField, "Job Title"); fc.utobj().dragAndDropElement(driver,
	 * fc.utobj().getElementByXpath(driver,".//a[.='Job Title']")),
	 * webForm.defaultDropHere); fc.utobj().sendKeys(driver,
	 * webForm.searchField, "Suffix"); fc.utobj().dragAndDropElement(driver,
	 * fc.utobj().getElementByXpath(driver,".//a[.='Suffix']")),
	 * webForm.defaultDropHere); fc.utobj().sendKeys(driver,
	 * webForm.searchField, "Birthdate"); fc.utobj().dragAndDropElement(driver,
	 * fc.utobj().getElementByXpath(driver,".//a[.='Birthdate']")),
	 * webForm.defaultDropHere); fc.utobj().sendKeys(driver,
	 * webForm.searchField, "Anniversary Date");
	 * fc.utobj().dragAndDropElement(driver,
	 * fc.utobj().getElementByXpath(driver,".//a[.='Anniversary Date']")),
	 * webForm.defaultDropHere); fc.utobj().sendKeys(driver,
	 * webForm.searchField, "How did you hear about us?(Contact Source)");
	 * fc.utobj().dragAndDropElement(driver,
	 * fc.utobj().getElementByXpath(driver,
	 * ".//a[contains(text () ,'How did you hear about us?(Contact Source)')]"
	 * )), webForm.defaultDropHere); fc.utobj().sendKeys(driver,
	 * webForm.searchField, "Best Time to Contact");
	 * fc.utobj().dragAndDropElement(driver,
	 * fc.utobj().getElementByXpath(driver,".//a[.='Best Time To Contact']")),
	 * webForm.defaultDropHere); fc.utobj().sendKeys(driver,
	 * webForm.searchField, "Comments"); fc.utobj().dragAndDropElement(driver,
	 * fc.utobj().getElementByXpath(driver,".//a[.='Comments']")),
	 * webForm.defaultDropHere); fc.utobj().sendKeys(driver,
	 * webForm.searchField, "Facebook"); fc.utobj().dragAndDropElement(driver,
	 * fc.utobj().getElementByXpath(driver,".//a[.='Facebook']")),
	 * webForm.defaultDropHere); fc.utobj().sendKeys(driver,
	 * webForm.searchField, "Twitter");
	 * 
	 * 
	 * fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
	 * ".//a[contains(text () ,'"+newTabName+"')]")));
	 * fc.utobj().dragAndDropElement(driver,
	 * fc.utobj().getElementByXpath(driver,".//a[.='Twitter']")),
	 * webForm.defaultDropHere); fc.utobj().dragAndDropElement(driver,
	 * fc.utobj().getElementByXpath(driver,".//a[contains(text(),'"+FieldName1+
	 * "')]")), webForm.defaultDropHere); fc.utobj().dragAndDropElement(driver,
	 * fc.utobj().getElementByXpath(driver,".//a[contains(text(),'"+FieldName2+
	 * "')]")), webForm.defaultDropHere); fc.utobj().dragAndDropElement(driver,
	 * fc.utobj().getElementByXpath(driver,".//a[contains(text(),'"+FieldName3+
	 * "')]")), webForm.defaultDropHere); fc.utobj().dragAndDropElement(driver,
	 * fc.utobj().getElementByXpath(driver,".//a[contains(text(),'"+FieldName4+
	 * "')]")), webForm.defaultDropHere); fc.utobj().dragAndDropElement(driver,
	 * fc.utobj().getElementByXpath(driver,".//a[contains(text(),'"+FieldName5+
	 * "')]")), webForm.defaultDropHere); fc.utobj().dragAndDropElement(driver,
	 * fc.utobj().getElementByXpath(driver,".//a[contains(text(),'"+FieldName6+
	 * "')]")), webForm.defaultDropHere); fc.utobj().dragAndDropElement(driver,
	 * fc.utobj().getElementByXpath(driver,".//a[contains(text(),'"+FieldName7+
	 * "')]")), webForm.defaultDropHere); fc.utobj().dragAndDropElement(driver,
	 * fc.utobj().getElementByXpath(driver,".//a[contains(text(),'"+FieldName8+
	 * "')]")), webForm.defaultDropHere);
	 * 
	 * if (formFormat.equalsIgnoreCase("Single Page")) { //add Text in custom
	 * section fc.utobj().doubleClickElement(driver,
	 * fc.utobj().getElementByXpath(driver,".//*[.='"+sectionName+
	 * "']/following-sibling::table//ul/li[@class='emptyList']")));
	 * fc.commonMethods().switch_cboxIframe_frameId(driver);
	 * fc.utobj().sendKeys(driver, webForm.labelAddText, fieldLabel);
	 * fc.utobj().selectDropDown(driver, webForm.headerLabelAlignment, "Left");
	 * fc.utobj().clickElement(driver, webForm.saveBtn);
	 * fc.utobj().switchFrameToDefault(driver);
	 * 
	 * //add Text Drop Here Custom fc.utobj().clickElement(driver,
	 * webForm.formTools); fc.utobj().dragAndDropElement(driver,
	 * webForm.formToolText,
	 * fc.utobj().getElementByXpath(driver,".//*[.='"+sectionName+
	 * "']/following-sibling::table//li[@class='emptyList']//p[.='Drop here.']"
	 * ))); }else if (formFormat.equalsIgnoreCase( "Multi Page")) {
	 * fc.utobj().clickElement(driver,
	 * fc.utobj().getElementByXpath(driver,".//li[.='"+tabName+"']")));
	 * 
	 * //add Text in custom section //click over double click String
	 * text=fc.utobj().getElementByXpath(driver,".//li[.='"+tabName+"']")).
	 * getAttribute( "id");
	 * 
	 * String tabName1=tabName.toLowerCase();
	 * text=text.replace("_"+tabName1+"_", ""); text=text.replace("_Tab", "");
	 * 
	 * String text1=fc.utobj().getElementByXpath(driver,
	 * ".//div[contains(@id , '"+text+ "')]/ul[contains(@id , '"
	 * +text+"')]/li")).getAttribute("id");
	 * text1=text1.replace("defaultSection_", "");
	 * 
	 * fc.utobj().doubleClickElement(driver,
	 * fc.utobj().getElementByXpath(driver, ".//ul[contains(@id , '"+text1+
	 * "')]/li[@class='emptyList']//p[.='Double click or drag & drop to add text.']"
	 * ))); fc.commonMethods().switch_cboxIframe_frameId(driver);
	 * fc.utobj().sendKeys(driver, webForm.labelAddText, fieldLabel);
	 * fc.utobj().selectDropDown(driver, webForm.headerLabelAlignment, "Left");
	 * fc.utobj().clickElement(driver, webForm.saveBtn);
	 * fc.utobj().switchFrameToDefault(driver);
	 * 
	 * //add Text Drop Here Custom fc.utobj().clickElement(driver,
	 * webForm.formTools); fc.utobj().dragAndDropElement(driver,
	 * webForm.formToolText, fc.utobj().getElementByXpath(driver,
	 * ".//ul[contains(@id , '"+text+ "')]/li//p[.='Drop here.']")));
	 * 
	 * } //add Footer fc.utobj().doubleClickElement(driver,
	 * webForm.addFooterDefault);
	 * fc.commonMethods().switch_cboxIframe_frameId(driver);
	 * fc.utobj().switchFrameById(driver, "textBlock_ifr");
	 * 
	 * WebElement editorTextArea1=fc.utobj().getElementByXpath(driver,
	 * ".//*[@id='tinymce']/p")); Actions actions1 = new Actions(driver);
	 * actions1.moveToElement(editorTextArea1); actions1.click();
	 * actions1.sendKeys(footer); fc.utobj().logReport(
	 * "Entering Value in Text Field", editorTextArea1);
	 * fc.utobj().logReportMsg("Entered Text", footer);
	 * actions1.build().perform(); fc.utobj().switchFrameToDefault(driver);
	 * fc.commonMethods().switch_cboxIframe_frameId(driver);
	 * fc.utobj().clickElement(driver, webForm.saveBtn);
	 * fc.utobj().switchFrameToDefault(driver); fc.utobj().clickElement(driver,
	 * webForm.saveAndNextBtnDesign);
	 * 
	 * //Setting fc.utobj().selectDropDown(driver, webForm.contactType,
	 * contactType); fc.utobj().selectDropDown(driver, webForm.cmLeadStatusID,
	 * status); fc.utobj().selectDropDown(driver, webForm.contactMediumSelect,
	 * contactMedium); fc.utobj().selectDropDown(driver, webForm.leadSource,
	 * leadSource); fc.utobj().selectDropDown(driver,
	 * webForm.leadSourceDetailsSelect, sourceDetails); if
	 * (assignTo.equalsIgnoreCase("default")) { if
	 * (!fc.utobj().isSelected(driver,
	 * webForm.assignToDefault)) { fc.utobj().clickElement(driver,
	 * webForm.assignToDefault); } }else if
	 * (assignTo.equalsIgnoreCase("Corporate")) { if
	 * (!fc.utobj().isSelected(driver,
	 * webForm.assignToCorporate)) {
	 * fc.utobj().clickElement(driver, webForm.assignToCorporate); }
	 * fc.utobj().selectDropDown(driver, webForm.selectCorporateUser,
	 * userNameCorp);
	 * 
	 * }else if (assignTo.equalsIgnoreCase("Regional")) { if
	 * (!fc.utobj().isSelected(driver,
	 * webForm.assignToRegional)) { fc.utobj().clickElement(driver,
	 * webForm.assignToRegional); } fc.utobj().selectDropDown(driver,
	 * webForm.selectAreaRegion, regionName); fc.utobj().selectDropDown(driver,
	 * webForm.selectRegionalUser, userNameReg);
	 * 
	 * }else if (assignTo.equalsIgnoreCase("Franchisee")) {
	 * 
	 * if (!fc.utobj().isSelected(driver,
	 * webForm.assignToFranchise)) {
	 * fc.utobj().clickElement(driver, webForm.assignToFranchise); }
	 * fc.utobj().selectDropDown(driver, webForm.selectFranchiseID,
	 * franchiseId); fc.utobj().selectDropDown(driver,
	 * webForm.selectFranchiseUser, userNameFranchise);
	 * 
	 * } if (afterSubmission.equalsIgnoreCase("message")) { if
	 * (!fc.utobj().isSelected(driver,
	 * webForm.afterSubmissionMsg)) {
	 * fc.utobj().clickElement(driver, webForm.afterSubmissionMsg); } }else if
	 * (afterSubmission.equalsIgnoreCase("url")) { if
	 * (!fc.utobj().isSelected(driver,
	 * webForm.afterSubmissionUrl)) {
	 * fc.utobj().clickElement(driver, webForm.afterSubmissionUrl); }
	 * fc.utobj().sendKeys(driver, webForm.redirectedUrl, redirectedUrl); }
	 * 
	 * fc.utobj().clickElement(driver, webForm.finishBtn);
	 * 
	 * if (!webForm.hostURL) { fc.utobj().clickElement(driver,
	 * webForm.hostURL); }
	 * 
	 * urlText=webForm.hostCodeBox.getText().trim();
	 * fc.utobj().clickElement(driver, webForm.okBtn);
	 * 
	 * } catch (Exception e) { fc.utobj().quitBrowserOnCatchBasicMethod(driver,
	 * config, e, testCaseId); }
	 * 
	 * 
	 * 
	 * 
	 * 
	 * String parentWindow=driver.getWindowHandle(); fc.utobj().sendKeys(driver,
	 * CrmWebFormPage.searchMyForm, formName); fc.utobj().clickElement(driver,
	 * CrmWebFormPage.searchMyFormBtn); fc.utobj().actionImgOption(driver,
	 * formName, "Launch & Test");
	 * 
	 * fc.utobj().printTestStep(testCaseId, "Launc And Test Form"); Set<String>
	 * allWindows2=driver.getWindowHandles(); for (String currentWindow :
	 * allWindows2) { if (!currentWindow.equalsIgnoreCase(parentWindow)) {
	 * driver.switchTo().window(currentWindow); String
	 * titleTextCurrent=driver.getTitle(); if
	 * (titleTextCurrent.equalsIgnoreCase(formTitle)) { //fill the form with
	 * data
	 * 
	 * fc.utobj().isTextDisplayed(driver, header,
	 * "was not able to verify Header"); fc.utobj().isTextDisplayed(driver,
	 * formTitle, "was not able to verify Form Title");
	 * //fc.utobj().isTextDisplayed(driver, textLabel,
	 * "was not able to verify Text Label"); fc.utobj().isTextDisplayed(driver,
	 * footer, "was not able to verify Footer");
	 * 
	 * fc.utobj().sendKeys(driver, CrmWebFormPage.contactFirstName, FName);
	 * fc.utobj().sendKeys(driver, CrmWebFormPage.contactLastName, LName);
	 * fc.utobj().selectDropDownByPartialText(driver, CrmWebFormPage.title,
	 * "Dr."); //fc.utobj().sendKeys(driver, CrmWebFormPage.companyName,
	 * "companyName"); fc.utobj().sendKeys(driver, CrmWebFormPage.address,
	 * "address"); fc.utobj().sendKeys(driver, CrmWebFormPage.city, "city");
	 * fc.utobj().selectDropDownByPartialText(driver, CrmWebFormPage.country,
	 * "USA"); //fc.utobj().sendKeys(driver, CrmWebFormPage.zipcode, zipCode);
	 * fc.utobj().selectDropDownByPartialText(driver, CrmWebFormPage.state,
	 * "Alabama"); fc.utobj().sendKeys(driver, CrmWebFormPage.phoneNumbers,
	 * "1236547896"); fc.utobj().sendKeys(driver, CrmWebFormPage.extn, "12");
	 * fc.utobj().sendKeys(driver, CrmWebFormPage.faxNumbers, "1236577896");
	 * fc.utobj().sendKeys(driver, CrmWebFormPage.mobileNumbers, "9874563212");
	 * fc.utobj().sendKeys(driver, CrmWebFormPage.emailIds,
	 * "jhsdhfs@gmail.com"); fc.utobj().sendKeys(driver,
	 * CrmWebFormPage.alternateEmail, "jhsdhdasdfs@gmail.com");
	 * fc.utobj().selectDropDownByPartialText(driver,
	 * CrmWebFormPage.cmSourceDetails, leadSource);
	 * fc.utobj().selectDropDownByPartialText(driver,
	 * CrmWebFormPage.howDidYouHearAboutUs, sourceDetails);
	 * 
	 * 
	 * fc.utobj().sendKeys(driver, fc.utobj().getElementByXpath(driver,
	 * ".//td[contains(text () , '"
	 * +FieldName1+"')]/following-sibling::td/input")), FieldName1);
	 * fc.utobj().sendKeys(driver, fc.utobj().getElementByXpath(driver,
	 * ".//td[contains(text () , '"
	 * +FieldName2+"')]/following-sibling::td/label/textarea")), FieldName2);
	 * fc.utobj().sendKeys(driver, fc.utobj().getElementByXpath(driver,
	 * ".//td[contains(text () , '"
	 * +FieldName3+"')]/following-sibling::td/table/tbody/tr/td/input")),
	 * "02/02/2017"); fc.utobj().clickElement(driver,
	 * fc.utobj().getElementByXpath(driver,".//td[contains(text () , '"
	 * +FieldName4+
	 * "')]/following-sibling::td/select/option[contains(text(),'Opt"+FieldName4
	 * +"')]"))); fc.utobj().clickElement(driver,
	 * fc.utobj().getElementByXpath(driver, ".//td[contains(text () , '"
	 * +FieldName5+
	 * "')]/following-sibling::td/input[@type='radio' and @value='1']")));
	 * fc.utobj().sendKeys(driver, fc.utobj().getElementByXpath(driver,
	 * ".//td[contains(text () , '"
	 * +FieldName7+"')]/following-sibling::td/input")), "99");
	 * fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
	 * ".//td[contains(text () , '"
	 * +FieldName6+"')]/following-sibling::td/input"))); try {
	 * fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
	 * ".//td[contains(text () , '"+FieldName8+
	 * "')]/following-sibling::td/div/div/ul/li/label[contains(text(),'Mul"+
	 * FieldName8+"')]/input"))); } catch (Exception e) {
	 * 
	 * }String fileName=fc.utobj().getFilePathFromTestData(config,
	 * dataSet.get("fileName"));
	 * fc.utobj().sendKeys(driver,fc.utobj().getElementByXpath(driver,
	 * ".//td[contains(text () , '"
	 * +FieldName9+"')]/following-sibling::td/input[@type='file']")),fileName);
	 * 
	 * 
	 * 
	 * 
	 * //fc.utobj().clickElement(driver, CrmWebFormPage.nextBtn);
	 * 
	 * fc.utobj().isTextDisplayed(driver, header,
	 * "was not able to verify Header"); fc.utobj().isTextDisplayed(driver,
	 * formTitle, "was not able to verify Form Title");
	 * //fc.utobj().isTextDisplayed(driver, tabName,
	 * "was not able to verify Tab Name"); //fc.utobj().isTextDisplayed(driver,
	 * fieldLabel, "was not able to verify Fiedl Label");
	 * fc.utobj().isTextDisplayed(driver, footer,
	 * "was not able to verify Footer"); fc.utobj().clickElement(driver,
	 * CrmWebFormPage.submitBtn); //fc.utobj().isTextDisplayed(driver,
	 * "Thank you for submitting your information, we will get back to you shortly."
	 * , "was not able to verify confirmation Msg");
	 * 
	 * driver.close(); }else{ driver.close();
	 * driver.switchTo().window(parentWindow); }
	 * driver.switchTo().window(parentWindow); } }
	 * 
	 * fc.crm().crm_common().CRMContactsLnk( driver);
	 * fc.utobj().clickElement(driver, pobj.addNew);
	 * fc.utobj().clickElement(driver, pobj.contactLnk);
	 * fc.utobj().clickElement(driver, pobj.contactsLinks);
	 * contactsFilter(driver, contactType); fc.utobj().clickElement(driver,
	 * fc.utobj().getElementByXpath(driver,".//a[contains(text () , '"+FName+" "
	 * +LName+"')]")));
	 * 
	 * 
	 * boolean isVisibilityPresent=fc.utobj().verifyCase(driver,
	 * ".//*[contains(text () ,'"+FieldName1+"')]"); if
	 * (isVisibilityPresent==false) { fc.utobj().throwsException(
	 * "was not able to "+FieldName1); } boolean
	 * isVisibilityPresent2=fc.utobj().verifyCase(driver,
	 * ".//*[contains(text () ,'"+FieldName2+"')]"); if
	 * (isVisibilityPresent2==false) { fc.utobj().throwsException(
	 * "was not able to "+FieldName2); } boolean
	 * isVisibilityPresent3=fc.utobj().verifyCase(driver,
	 * ".//*[contains(text () ,'"+FieldName3+"')]"); if
	 * (isVisibilityPresent3==false) { fc.utobj().throwsException(
	 * "was not able to "+FieldName3); } try { boolean isVisibilityPresent4 =
	 * fc.utobj().verifyCase(driver, ".//*[contains(text () ,'" + FieldName4 +
	 * "')]"); if (isVisibilityPresent4 == false) { fc.utobj().throwsException(
	 * "was not able to " + FieldName4); } } catch (Exception e) { } try {
	 * boolean isVisibilityPresent8 = fc.utobj().verifyCase(driver,
	 * ".//*[contains(text () ,'" + FieldName8 + "')]"); if
	 * (isVisibilityPresent8 == false) { fc.utobj().throwsException(
	 * "was not able to " + FieldName4); } } catch (Exception e) { } boolean
	 * isVisibilityPresent5=fc.utobj().verifyCase(driver,
	 * ".//*[contains(text () ,'"+FieldName7+"')]"); if
	 * (isVisibilityPresent5==false) { fc.utobj().throwsException(
	 * "was not able to "+FieldName6); } boolean
	 * isVisibilityPresent6=fc.utobj().verifyCase(driver,
	 * ".//*[contains(text () ,'"+FieldName6+"')]"); if
	 * (isVisibilityPresent6==false) { fc.utobj().throwsException(
	 * "was not able to "+FieldName6); } fc.utobj().printTestResultExcel(config,
	 * testCaseId, null, null,"Passed", null, null);
	 * fc.utobj().logoutAndQuitBrowser(driver, config, testCaseId);
	 * 
	 * } catch (Exception e) { fc.utobj().quitBrowserOnCatch(driver, config, e,
	 * testCaseId); } }
	 * 
	 * 
	 * 
	 * 
	 * 
	 * 
	 * 
	 * 
	 * 
	 * 
	 * 
	 * 
	 * 
	 * 
	 * 
	 * 
	 * 
	 * 
	 * 
	 * 
	 * 
	 * 
	 * 
	 * 
	 * //Anukaran Mishra
	 * 
	 * 
	 * @Test(groups = "crmmodule")
	 * 
	 * @TestCase(createdOn="2017-05-01",updatedOn="2018-06-14",
	 * testCaseDescription =
	 * "Add Section with all types of custom fields under the new tab > add a Lead through External Form"
	 * , testCaseId = "TC_325_Verify_New_Lead_Tab") public void
	 * verifyNewContactTab() throws IOException, Exception,
	 * ParserConfigurationException, SAXException { String testCaseId =
	 * fc.utobj().readTestCaseInfo(this.getClass().getName()+"."+new
	 * Object(){}.getClass().getEnclosingMethod().getName());
	 * 
	 * Map<String, String> config =
	 * fc.utobj().readConfigurationFile(FranconnectUtil.path);
	 * Map<String,String> dataSet = fc.utobj().readTestData("crm", testCaseId);
	 * AdminInfoMgrCommonMethods adminCommonMethods =
	 * fc.adminpage().adminInfoMgr(); WebDriver driver =
	 * fc.utobj().openDriver(config); //CRM_CorporateUser crmCU= new
	 * CRM_CorporateUser();
	 * 
	 * try { driver = fc.loginpage().login(driver, config); CRMLeadsPage pobj =
	 * new CRMLeadsPage(driver);
	 * 
	 * fc.utobj().printTestStep(testCaseId,
	 * "Navigate To Admin Users Manage Corporate User");
	 * fc.utobj().printTestStep(testCaseId, "Add Corporate User");
	 * AdminUsersManageCorporateUsersAddCorporateUserPageTest addCorporatePage =
	 * new AdminUsersManageCorporateUsersAddCorporateUserPageTest(); String
	 * CuserName = fc.utobj().generateTestData(dataSet.get("userName")); String
	 * userName = addCorporatePage.addCorporateUser(driver, CuserName, config);
	 * fc.home_page().logout(driver, config);
	 * fc.loginpage().loginWithParameter(driver, CuserName, "T0n1ght1");
	 * 
	 * fc.utobj().printTestStep(testCaseId,
	 * "Navigate To Admin > CRM > Manage Form Generator");
	 * fc.utobj().printTestStep(testCaseId, "Add New Tab"); //
	 * fc.crm().crm_common().CRMLeadsLnk( driver);
	 * 
	 * 
	 * fc.crm().crm_common().adminCRMManageFormGeneratorLnk( driver);
	 * fc.adminpage().adminInfoMgr(); AdminInfoMgrManageFormGeneratorPage
	 * manageFormGeneratorPage = new
	 * AdminInfoMgrManageFormGeneratorPage(driver);
	 * AdminInfoMgrManageFormGeneratorTabDetailsPage ADM_form_gen =
	 * manageFormGeneratorPage.getAddTabDetailsPage();
	 * adminCommonMethods.NavigateToManageFormGeneratorTabs(driver, "Lead");
	 * 
	 * fc.utobj().clickElement(driver, manageFormGeneratorPage.btnAddNewTab);
	 * fc.commonMethods().switch_cboxIframe_frameId(driver);
	 * AdminInfoMgrManageFormGeneratorAddTabPage objAddNewTabPage = new
	 * AdminInfoMgrManageFormGeneratorAddTabPage(driver); String newTabName =
	 * fc.utobj().generateTestData("New Tab"); fc.utobj().sendKeys(driver,
	 * objAddNewTabPage.txtDisplayName, newTabName);
	 * fc.utobj().clickElement(driver, objAddNewTabPage.multipleInputNo);
	 * fc.utobj().clickElement(driver, objAddNewTabPage.btnSubmit);
	 * fc.utobj().switchFrameToDefault(driver); fc.utobj().clickLink(driver,
	 * newTabName);
	 * 
	 * 
	 * fc.utobj().clickElement(driver, ADM_form_gen.btnAddSection);
	 * 
	 * fc.commonMethods().switch_cboxIframe_frameId(driver); String SectionName
	 * = "New Section"; SectionName = fc.utobj().generateTestData(SectionName);
	 * 
	 * fc.utobj().sendKeys(driver, ADM_form_gen.txtSectionName, SectionName);
	 * fc.utobj().clickElement(driver, ADM_form_gen.btnAddSectionName);
	 * fc.utobj().switchFrameToDefault(driver); fc.utobj().clickElement(driver,
	 * ADM_form_gen.btnAddField);
	 * fc.commonMethods().switch_cboxIframe_frameId(driver); String FieldName2=
	 * "Text Area"; FieldName2 = fc.utobj().generateTestData(FieldName2);
	 * fc.utobj().sendKeys(driver, ADM_form_gen.txtFieldName, FieldName2);
	 * //fc.utobj().clickElement(driver, ADM_form_gen.documentopt);
	 * fc.utobj().selectDropDown(driver, ADM_form_gen.FieldType, "Text Area");
	 * fc.utobj().clickElement(driver, ADM_form_gen.btnSumbitField);
	 * fc.utobj().switchFrameToDefault(driver);
	 * 
	 * fc.utobj().clickElement(driver, ADM_form_gen.btnAddField);
	 * fc.commonMethods().switch_cboxIframe_frameId(driver); String
	 * FieldName1="Text"; FieldName1 = fc.utobj().generateTestData(FieldName1);
	 * fc.utobj().sendKeys(driver, ADM_form_gen.txtFieldName, FieldName1);
	 * //fc.utobj().clickElement(driver, ADM_form_gen.documentopt);
	 * fc.utobj().selectDropDown(driver, ADM_form_gen.FieldType, "Text");
	 * fc.utobj().clickElement(driver, ADM_form_gen.btnSumbitField);
	 * fc.utobj().switchFrameToDefault(driver);
	 * 
	 * fc.utobj().clickElement(driver, ADM_form_gen.btnAddField);
	 * fc.commonMethods().switch_cboxIframe_frameId(driver); String
	 * FieldName3="Date"; FieldName3 = fc.utobj().generateTestData(FieldName3);
	 * fc.utobj().sendKeys(driver, ADM_form_gen.txtFieldName, FieldName3);
	 * //fc.utobj().clickElement(driver, ADM_form_gen.documentopt);
	 * fc.utobj().selectDropDown(driver, ADM_form_gen.FieldType, "Date");
	 * fc.utobj().clickElement(driver, ADM_form_gen.btnSumbitField);
	 * fc.utobj().switchFrameToDefault(driver);
	 * 
	 * 
	 * 
	 * 
	 * fc.utobj().clickElement(driver, ADM_form_gen.btnAddField);
	 * fc.commonMethods().switch_cboxIframe_frameId(driver); String
	 * FieldName4="DropDown"; FieldName4 =
	 * fc.utobj().generateTestData(FieldName4); fc.utobj().sendKeys(driver,
	 * ADM_form_gen.txtFieldName, FieldName4); //fc.utobj().clickElement(driver,
	 * ADM_form_gen.documentopt); fc.utobj().selectDropDown(driver,
	 * ADM_form_gen.FieldType, "Drop-down"); fc.utobj().sendKeys(driver,
	 * fc.utobj().getElementByXpath(driver,".//*[@id='row_1']/td[2]/input[1]")),
	 * "Opt"+FieldName4);
	 * 
	 * fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
	 * ".//*[@id='removeTDForAddMore']//*[contains(text(),'Add Option')]")));
	 * fc.utobj().sendKeys(driver,
	 * fc.utobj().getElementByXpath(driver,".//*[@id='row_2']/td[2]/input[1]")),
	 * "Opt2"); fc.utobj().clickElement(driver, ADM_form_gen.btnSumbitField);
	 * fc.utobj().switchFrameToDefault(driver);
	 * 
	 * fc.utobj().clickElement(driver, ADM_form_gen.btnAddField);
	 * fc.commonMethods().switch_cboxIframe_frameId(driver); String
	 * FieldName5="Radio"; FieldName5 = fc.utobj().generateTestData(FieldName5);
	 * fc.utobj().sendKeys(driver, ADM_form_gen.txtFieldName, FieldName5);
	 * //fc.utobj().clickElement(driver, ADM_form_gen.documentopt);
	 * fc.utobj().selectDropDown(driver, ADM_form_gen.FieldType, "Radio");
	 * fc.utobj().sendKeys(driver,
	 * fc.utobj().getElementByXpath(driver,".//*[@id='row_1']/td[2]/input[1]")),
	 * "Opt"+FieldName5); fc.utobj().clickElement(driver,
	 * fc.utobj().getElementByXpath(driver,
	 * ".//*[@id='removeTDForAddMore']//*[contains(text(),'Add Option')]")));
	 * fc.utobj().sendKeys(driver,
	 * fc.utobj().getElementByXpath(driver,".//*[@id='row_2']/td[2]/input[1]")),
	 * "Opt2"+FieldName5); fc.utobj().clickElement(driver,
	 * ADM_form_gen.btnSumbitField); fc.utobj().switchFrameToDefault(driver);
	 * 
	 * fc.utobj().clickElement(driver, ADM_form_gen.btnAddField);
	 * fc.commonMethods().switch_cboxIframe_frameId(driver); String
	 * FieldName6="Checkbox"; FieldName6 =
	 * fc.utobj().generateTestData(FieldName6); fc.utobj().sendKeys(driver,
	 * ADM_form_gen.txtFieldName, FieldName6); //fc.utobj().clickElement(driver,
	 * ADM_form_gen.documentopt); fc.utobj().selectDropDown(driver,
	 * ADM_form_gen.FieldType, "Checkbox"); fc.utobj().sendKeys(driver,
	 * fc.utobj().getElementByXpath(driver,".//*[@id='row_1']/td[2]/input[1]")),
	 * "Opt1"); fc.utobj().clickElement(driver,
	 * fc.utobj().getElementByXpath(driver,
	 * ".//*[@id='removeTDForAddMore']//*[contains(text(),'Add Option')]")));
	 * fc.utobj().sendKeys(driver,
	 * fc.utobj().getElementByXpath(driver,".//*[@id='row_2']/td[2]/input[1]")),
	 * "Opt2"); fc.utobj().clickElement(driver, ADM_form_gen.btnSumbitField);
	 * fc.utobj().switchFrameToDefault(driver);
	 * 
	 * fc.utobj().clickElement(driver, ADM_form_gen.btnAddField);
	 * fc.commonMethods().switch_cboxIframe_frameId(driver); String FieldName7=
	 * "Test Field3"; FieldName7 = fc.utobj().generateTestData(FieldName7);
	 * fc.utobj().sendKeys(driver, ADM_form_gen.txtFieldName, FieldName7);
	 * //fc.utobj().clickElement(driver, ADM_form_gen.documentopt);
	 * fc.utobj().selectDropDown(driver, ADM_form_gen.FieldType, "Numeric");
	 * fc.utobj().clickElement(driver, ADM_form_gen.btnSumbitField);
	 * fc.utobj().switchFrameToDefault(driver);
	 * 
	 * fc.utobj().clickElement(driver, ADM_form_gen.btnAddField);
	 * fc.commonMethods().switch_cboxIframe_frameId(driver); String
	 * FieldName8="Multislect"; FieldName8 =
	 * fc.utobj().generateTestData(FieldName8); fc.utobj().sendKeys(driver,
	 * ADM_form_gen.txtFieldName, FieldName8); //fc.utobj().clickElement(driver,
	 * ADM_form_gen.documentopt); fc.utobj().selectDropDown(driver,
	 * ADM_form_gen.FieldType, "Multi Select Drop-down");
	 * fc.utobj().sendKeys(driver,
	 * fc.utobj().getElementByXpath(driver,".//*[@id='row_1']/td[2]/input[1]")),
	 * "Mul"+FieldName8); fc.utobj().clickElement(driver,
	 * fc.utobj().getElementByXpath(driver,
	 * ".//*[@id='removeTDForAddMore']//*[contains(text(),'Add Option')]")));
	 * fc.utobj().sendKeys(driver,
	 * fc.utobj().getElementByXpath(driver,".//*[@id='row_2']/td[2]/input[1]")),
	 * "Opt2"); fc.utobj().clickElement(driver, ADM_form_gen.btnSumbitField);
	 * fc.utobj().switchFrameToDefault(driver);
	 * 
	 * 
	 * fc.utobj().clickElement(driver, ADM_form_gen.btnAddField);
	 * fc.commonMethods().switch_cboxIframe_frameId(driver); String FieldName9=
	 * "Test Field3"; FieldName9 = fc.utobj().generateTestData(FieldName9);
	 * fc.utobj().sendKeys(driver, ADM_form_gen.txtFieldName, FieldName9);
	 * //fc.utobj().clickElement(driver, ADM_form_gen.documentopt);
	 * fc.utobj().selectDropDown(driver, ADM_form_gen.FieldType, "Document");
	 * fc.utobj().clickElement(driver, ADM_form_gen.btnSumbitField);
	 * fc.utobj().switchFrameToDefault(driver);
	 * 
	 * 
	 * 
	 * AdminCRMManageWebFormGeneratorPageTest webFormPage = new
	 * AdminCRMManageWebFormGeneratorPageTest(); //create Web form (Lead)
	 * 
	 * 
	 * 
	 * 
	 * String status ="New"; String contactMedium ="Email"; String
	 * leadSource="Import"; String sourceDetails="Import";
	 * fc.utobj().printTestStep(testCaseId,
	 * "Navigate To Admin > CRM > Manage Web Form Generator");
	 * fc.utobj().printTestStep(testCaseId, "Create New Lead Form"); String
	 * formFormat="Single Page"; String
	 * header=fc.utobj().generateTestData(dataSet.get("header")); String
	 * textLabel=fc.utobj().generateTestData(dataSet.get("textLabel")); String
	 * fieldLabel=fc.utobj().generateTestData(dataSet.get("fieldLabel")); String
	 * footer=fc.utobj().generateTestData(dataSet.get("footer")); String
	 * assignTo="Default Owner"; String
	 * submissionType=dataSet.get("submissionType"); String
	 * formName=fc.utobj().generateTestData(dataSet.get("formName")); String
	 * formTitle=fc.utobj().generateTestData(dataSet.get("formTitle")); String
	 * description=fc.utobj().generateTestData(dataSet.get("description"));
	 * String formUrl=fc.utobj().generateTestData(dataSet.get("formUrl"));
	 * String sectionName=fc.utobj().generateTestData("sectionName"); String
	 * userNameReg=""; String userNameFranchise=""; String
	 * afterSubmission=dataSet.get("afterSubmission"); String
	 * tabName=fc.utobj().generateTestData(dataSet.get("tabName")); String
	 * redirectedUrl=dataSet.get("reUrl");
	 * 
	 * String userNameCorp="FranConnect Administrator"; String regionName="";
	 * String franchiseId=""; AdminCRMManageWebFormGeneratorPage
	 * CrmWebFormPage=new AdminCRMManageWebFormGeneratorPage(driver);
	 * //leadSource=sourceDetails;
	 * 
	 * webFormPage.createNewFormLeads(driver, formFormat, header, textLabel,
	 * fieldLabel, footer, assignTo, submissionType, formName, formTitle,
	 * description, formUrl, sectionName, userNameCorp, regionName, userNameReg,
	 * franchiseId, userNameFranchise, status, contactMedium, leadSource,
	 * sourceDetails, afterSubmission, redirectedUrl, tabName, config);
	 * 
	 * 
	 * 
	 * 
	 * 
	 * 
	 * 
	 * 
	 * String urlText=null; AdminCRMManageWebFormGeneratorPage webFormObj=new
	 * AdminCRMManageWebFormGeneratorPage(driver);
	 * fc.crm().crm_common().adminCRMManageWebFormGeneratorLnk( driver);
	 * fc.utobj().clickElement(driver, webFormObj.createNewForm);
	 * fc.utobj().sendKeys(driver, webFormObj.formName, formName);
	 * fc.utobj().sendKeys(driver, webFormObj.formDisplayTitle, formTitle); if
	 * (!webFormObj.displayFormTitleCheckBox) {
	 * fc.utobj().clickElement(driver, webFormObj.displayFormTitleCheckBox); }
	 * fc.utobj().sendKeys(driver, webFormObj.formDescription, description);
	 * 
	 * if (!fc.utobj().isSelected(driver,
	 * webFormObj.formTypeLeads)) { fc.utobj().clickElement(driver,
	 * webFormObj.formTypeLeads); } if (formFormat.equalsIgnoreCase(
	 * "Single Page")) { fc.utobj().selectDropDown(driver,
	 * webFormObj.formFormat, "Single Page");
	 * 
	 * }else if (formFormat.equalsIgnoreCase("Multi Page")) {
	 * fc.utobj().selectDropDown(driver, webFormObj.formFormat, "Multi Page");
	 * fc.utobj().selectDropDown(driver, webFormObj.submissionType,
	 * submissionType); } fc.utobj().selectDropDown(driver,
	 * webFormObj.columnCount, "1"); fc.utobj().selectDropDown(driver,
	 * webFormObj.filedLabelAlignment, "Left");
	 * fc.utobj().selectDropDown(driver, webFormObj.formLanguage, "English");
	 * fc.utobj().sendKeys(driver, webFormObj.iframeWidth, "100%");
	 * fc.utobj().sendKeys(driver, webFormObj.iframeHeight, "500");
	 * fc.utobj().sendKeys(driver, webFormObj.formUrl, formUrl);
	 * fc.utobj().clickElement(driver, webFormObj.saveNextBtn);
	 * 
	 * //1.Details Page
	 * 
	 * if (formFormat.equalsIgnoreCase("Single Page")) { //Add Section
	 * fc.utobj().clickElement(driver, webFormObj.addSection);
	 * fc.commonMethods().switch_cboxIframe_frameId(driver);
	 * fc.utobj().sendKeys(driver, webFormObj.sectionName, sectionName);
	 * fc.utobj().selectDropDown(driver, webFormObj.noOfCols, "1");
	 * fc.utobj().selectDropDown(driver, webFormObj.fieldLabelAlignmentSection,
	 * "Left"); fc.utobj().clickElement(driver, webFormObj.addBtn);
	 * fc.utobj().switchFrameToDefault(driver); }else if
	 * (formFormat.equalsIgnoreCase("Multi Page")) {
	 * fc.utobj().clickElement(driver, webFormObj.addTab);
	 * fc.commonMethods().switch_cboxIframe_frameId(driver);
	 * fc.utobj().sendKeys(driver, webFormObj.sectionName, tabName);
	 * fc.utobj().clickElement(driver, webFormObj.addBtn);
	 * fc.utobj().switchFrameToDefault(driver); } //add Header
	 * fc.utobj().doubleClickElement(driver, webFormObj.addHeader);
	 * fc.commonMethods().switch_cboxIframe_frameId(driver);
	 * fc.utobj().switchFrameById(driver, "textBlock_ifr"); WebElement
	 * editorTextArea=fc.utobj().getElementByXpath(driver,
	 * ".//*[@id='tinymce']/p")); Actions actions = new Actions(driver);
	 * actions.moveToElement(editorTextArea); actions.click();
	 * actions.sendKeys(header); fc.utobj().logReport(
	 * "Entering Value in Text Field", editorTextArea); fc.utobj().logReportMsg(
	 * "Entered Text", header); actions.build().perform();
	 * fc.utobj().switchFrameToDefault(driver);
	 * fc.commonMethods().switch_cboxIframe_frameId(driver);
	 * fc.utobj().clickElement(driver, webFormObj.saveBtn);
	 * fc.utobj().switchFrameToDefault(driver);
	 * 
	 * if (formFormat.equalsIgnoreCase("Multi Page")) {
	 * fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
	 * ".//*[contains(@id , 'defaultTab') and contains(@id , 'Tab') and contains(text () ,'Default Tab')]"
	 * ))); }
	 * 
	 * //Add Text fc.utobj().doubleClickElement(driver,
	 * webFormObj.defaultSectionHeaderAddText);
	 * fc.commonMethods().switch_cboxIframe_frameId(driver);
	 * fc.utobj().sendKeys(driver, webFormObj.labelAddText, textLabel);
	 * fc.utobj().selectDropDown(driver, webFormObj.headerLabelAlignment,
	 * "Left"); fc.utobj().clickElement(driver, webFormObj.saveBtn);
	 * fc.utobj().switchFrameToDefault(driver);
	 * 
	 * //FormTool fc.utobj().clickElement(driver,
	 * webFormObj.availableFieldsTab); fc.utobj().clickElement(driver,
	 * webFormObj.formTools); fc.utobj().dragAndDropElement(driver,
	 * webFormObj.formToolText, webFormObj.defaultDropHere);
	 * fc.utobj().dragAndDropElement(driver, webFormObj.formToolCaptcha,
	 * webFormObj.defaultDropHere); fc.utobj().dragAndDropElement(driver,
	 * webFormObj.formToolIAgreeCheckBox, webFormObj.defaultDropHere);
	 * 
	 * //Lead Info fc.utobj().clickElement(driver, webFormObj.leadInfo);
	 * fc.utobj().sendKeys(driver, webFormObj.searchField, "First Name");
	 * fc.utobj().dragAndDropElement(driver,
	 * fc.utobj().getElementByXpath(driver,".//a[.='First Name']")),
	 * webFormObj.defaultDropHere); fc.utobj().sendKeys(driver,
	 * webFormObj.searchField, "Title"); fc.utobj().dragAndDropElement(driver,
	 * fc.utobj().getElementByXpath(driver,".//a[.='Title']")),
	 * webFormObj.defaultDropHere); fc.utobj().sendKeys(driver,
	 * webFormObj.searchField, "Company"); fc.utobj().dragAndDropElement(driver,
	 * fc.utobj().getElementByXpath(driver,".//a[.='Company']")),
	 * webFormObj.defaultDropHere); fc.utobj().sendKeys(driver,
	 * webFormObj.searchField, "Address"); fc.utobj().dragAndDropElement(driver,
	 * fc.utobj().getElementByXpath(driver,".//a[.='Address']")),
	 * webFormObj.defaultDropHere); fc.utobj().sendKeys(driver,
	 * webFormObj.searchField, "City"); fc.utobj().dragAndDropElement(driver,
	 * fc.utobj().getElementByXpath(driver,".//a[.='City']")),
	 * webFormObj.defaultDropHere); fc.utobj().sendKeys(driver,
	 * webFormObj.searchField, "Country"); fc.utobj().dragAndDropElement(driver,
	 * fc.utobj().getElementByXpath(driver,".//a[.='Country']")),
	 * webFormObj.defaultDropHere); fc.utobj().sendKeys(driver,
	 * webFormObj.searchField, "Zip / Postal Code");
	 * fc.utobj().dragAndDropElement(driver,
	 * fc.utobj().getElementByXpath(driver,".//a[.='Zip / Postal Code']")),
	 * webFormObj.defaultDropHere); fc.utobj().sendKeys(driver,
	 * webFormObj.searchField, "State / Province");
	 * fc.utobj().dragAndDropElement(driver,
	 * fc.utobj().getElementByXpath(driver,".//a[.='State / Province']")),
	 * webFormObj.defaultDropHere); fc.utobj().sendKeys(driver,
	 * webFormObj.searchField, "Phone"); fc.utobj().dragAndDropElement(driver,
	 * fc.utobj().getElementByXpath(driver,".//a[.='Phone']")),
	 * webFormObj.defaultDropHere); fc.utobj().sendKeys(driver,
	 * webFormObj.searchField, "Phone Extension");
	 * fc.utobj().dragAndDropElement(driver,
	 * fc.utobj().getElementByXpath(driver,".//a[.='Phone Extension']")),
	 * webFormObj.defaultDropHere); fc.utobj().sendKeys(driver,
	 * webFormObj.searchField, "Fax"); fc.utobj().dragAndDropElement(driver,
	 * fc.utobj().getElementByXpath(driver,".//a[.='Fax']")),
	 * webFormObj.defaultDropHere); fc.utobj().sendKeys(driver,
	 * webFormObj.searchField, "Mobile"); fc.utobj().dragAndDropElement(driver,
	 * fc.utobj().getElementByXpath(driver,".//a[.='Mobile']")),
	 * webFormObj.defaultDropHere); fc.utobj().sendKeys(driver,
	 * webFormObj.searchField, "Email"); fc.utobj().dragAndDropElement(driver,
	 * fc.utobj().getElementByXpath(driver,".//a[.='Email']")),
	 * webFormObj.defaultDropHere); fc.utobj().sendKeys(driver,
	 * webFormObj.searchField, "Alternate Email");
	 * fc.utobj().dragAndDropElement(driver,
	 * fc.utobj().getElementByXpath(driver,".//a[.='Alternate Email']")),
	 * webFormObj.defaultDropHere); fc.utobj().sendKeys(driver,
	 * webFormObj.searchField, "Job Title");
	 * fc.utobj().dragAndDropElement(driver,
	 * fc.utobj().getElementByXpath(driver,".//a[.='Job Title']")),
	 * webFormObj.defaultDropHere); fc.utobj().sendKeys(driver,
	 * webFormObj.searchField, "Suffix"); fc.utobj().dragAndDropElement(driver,
	 * fc.utobj().getElementByXpath(driver,".//a[.='Suffix']")),
	 * webFormObj.defaultDropHere); fc.utobj().sendKeys(driver,
	 * webFormObj.searchField, "Birthdate");
	 * fc.utobj().dragAndDropElement(driver,
	 * fc.utobj().getElementByXpath(driver,".//a[.='Birthdate']")),
	 * webFormObj.defaultDropHere); fc.utobj().sendKeys(driver,
	 * webFormObj.searchField, "Anniversary Date");
	 * fc.utobj().dragAndDropElement(driver,
	 * fc.utobj().getElementByXpath(driver,".//a[.='Anniversary Date']")),
	 * webFormObj.defaultDropHere); fc.utobj().sendKeys(driver,
	 * webFormObj.searchField, "How did you hear about us?(Lead Source)");
	 * fc.utobj().dragAndDropElement(driver,
	 * fc.utobj().getElementByXpath(driver,
	 * ".//a[.='How did you hear about us?(Lead Source)']")),
	 * webFormObj.defaultDropHere); fc.utobj().sendKeys(driver,
	 * webFormObj.searchField, "Best Time to Contact");
	 * fc.utobj().dragAndDropElement(driver,
	 * fc.utobj().getElementByXpath(driver, ".//a[.='Best Time To Contact']")),
	 * webFormObj.defaultDropHere); fc.utobj().sendKeys(driver,
	 * webFormObj.searchField, "Comments");
	 * fc.utobj().dragAndDropElement(driver,
	 * fc.utobj().getElementByXpath(driver,".//a[.='Comments']")),
	 * webFormObj.defaultDropHere);
	 * 
	 * 
	 * fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
	 * ".//a[contains(text () ,'"+newTabName+"')]")));
	 * fc.utobj().dragAndDropElement(driver,
	 * fc.utobj().getElementByXpath(driver,".//a[.='Twitter']")),
	 * webFormObj.defaultDropHere); fc.utobj().dragAndDropElement(driver,
	 * fc.utobj().getElementByXpath(driver,".//a[contains(text(),'"+FieldName1+
	 * "')]")), webFormObj.defaultDropHere);
	 * fc.utobj().dragAndDropElement(driver,
	 * fc.utobj().getElementByXpath(driver,".//a[contains(text(),'"+FieldName2+
	 * "')]")), webFormObj.defaultDropHere);
	 * fc.utobj().dragAndDropElement(driver,
	 * fc.utobj().getElementByXpath(driver,".//a[contains(text(),'"+FieldName3+
	 * "')]")), webFormObj.defaultDropHere);
	 * fc.utobj().dragAndDropElement(driver,
	 * fc.utobj().getElementByXpath(driver,".//a[contains(text(),'"+FieldName4+
	 * "')]")), webFormObj.defaultDropHere);
	 * fc.utobj().dragAndDropElement(driver,
	 * fc.utobj().getElementByXpath(driver,".//a[contains(text(),'"+FieldName5+
	 * "')]")), webFormObj.defaultDropHere);
	 * fc.utobj().dragAndDropElement(driver,
	 * fc.utobj().getElementByXpath(driver,".//a[contains(text(),'"+FieldName6+
	 * "')]")), webFormObj.defaultDropHere);
	 * fc.utobj().dragAndDropElement(driver,
	 * fc.utobj().getElementByXpath(driver,".//a[contains(text(),'"+FieldName7+
	 * "')]")), webFormObj.defaultDropHere);
	 * fc.utobj().dragAndDropElement(driver,
	 * fc.utobj().getElementByXpath(driver,".//a[contains(text(),'"+FieldName8+
	 * "')]")), webFormObj.defaultDropHere);
	 * 
	 * 
	 * 
	 * 
	 * 
	 * if (formFormat.equalsIgnoreCase("Single Page")) { //add Text in custom
	 * section fc.utobj().doubleClickElement(driver,
	 * fc.utobj().getElementByXpath(driver,".//*[.='"+sectionName+
	 * "']/following-sibling::table//ul/li[@class='emptyList']")));
	 * fc.commonMethods().switch_cboxIframe_frameId(driver);
	 * fc.utobj().sendKeys(driver, webFormObj.labelAddText, fieldLabel);
	 * fc.utobj().selectDropDown(driver, webFormObj.headerLabelAlignment,
	 * "Left"); fc.utobj().clickElement(driver, webFormObj.saveBtn);
	 * fc.utobj().switchFrameToDefault(driver);
	 * 
	 * //add Text Drop Here Custom fc.utobj().clickElement(driver,
	 * webFormObj.formTools); fc.utobj().dragAndDropElement(driver,
	 * webFormObj.formToolText,
	 * fc.utobj().getElementByXpath(driver,".//*[.='"+sectionName+
	 * "']/following-sibling::table//li[@class='emptyList']//p[.='Drop here.']"
	 * ))); }else if (formFormat.equalsIgnoreCase( "Multi Page")) {
	 * fc.utobj().clickElement(driver,
	 * fc.utobj().getElementByXpath(driver,".//li[.='"+tabName+"']")));
	 * 
	 * //add Text in custom section //click over double click String
	 * text=fc.utobj().getElementByXpath(driver,".//li[.='"+tabName+"']")).
	 * getAttribute( "id");
	 * 
	 * String tabName1=tabName.toLowerCase();
	 * text=text.replace("_"+tabName1+"_", ""); text=text.replace("_Tab", "");
	 * 
	 * String text1=fc.utobj().getElementByXpath(driver,
	 * ".//div[contains(@id , '"+text+ "')]/ul[contains(@id , '"
	 * +text+"')]/li")).getAttribute("id");
	 * text1=text1.replace("defaultSection_", "");
	 * 
	 * fc.utobj().doubleClickElement(driver,
	 * fc.utobj().getElementByXpath(driver, ".//ul[contains(@id , '"+text1+
	 * "')]/li[@class='emptyList']//p[.='Double click or drag & drop to add text.']"
	 * ))); fc.commonMethods().switch_cboxIframe_frameId(driver);
	 * fc.utobj().sendKeys(driver, webFormObj.labelAddText, fieldLabel);
	 * fc.utobj().selectDropDown(driver, webFormObj.headerLabelAlignment,
	 * "Left"); fc.utobj().clickElement(driver, webFormObj.saveBtn);
	 * fc.utobj().switchFrameToDefault(driver);
	 * 
	 * //add Text Drop Here Custom fc.utobj().clickElement(driver,
	 * webFormObj.formTools); fc.utobj().dragAndDropElement(driver,
	 * webFormObj.formToolText, fc.utobj().getElementByXpath(driver,
	 * ".//ul[contains(@id , '"+text+ "')]/li//p[.='Drop here.']")));
	 * 
	 * } //add Footer fc.utobj().doubleClickElement(driver,
	 * webFormObj.addFooterDefault);
	 * fc.commonMethods().switch_cboxIframe_frameId(driver);
	 * fc.utobj().switchFrameById(driver, "textBlock_ifr");
	 * 
	 * WebElement editorTextArea1=fc.utobj().getElementByXpath(driver,
	 * ".//*[@id='tinymce']/p")); Actions actions1 = new Actions(driver);
	 * actions1.moveToElement(editorTextArea1); actions1.click();
	 * actions1.sendKeys(footer); fc.utobj().logReport(
	 * "Entering Value in Text Field", editorTextArea1);
	 * fc.utobj().logReportMsg("Entered Text", footer);
	 * actions1.build().perform(); fc.utobj().switchFrameToDefault(driver);
	 * fc.commonMethods().switch_cboxIframe_frameId(driver);
	 * fc.utobj().clickElement(driver, webFormObj.saveBtn);
	 * fc.utobj().switchFrameToDefault(driver); fc.utobj().clickElement(driver,
	 * webFormObj.saveAndNextBtnDesign);
	 * 
	 * //settings page fc.utobj().selectDropDown(driver,
	 * webFormObj.cmLeadStatusID, status); fc.utobj().selectDropDown(driver,
	 * webFormObj.contactMediumSelect, contactMedium);
	 * fc.utobj().selectDropDown(driver, webFormObj.leadSource, leadSource);
	 * fc.utobj().selectDropDown(driver, webFormObj.leadSourceDetailsSelect,
	 * sourceDetails); if (assignTo.equalsIgnoreCase("default")) { if
	 * (!webFormObj.assignToDefault) {
	 * fc.utobj().clickElement(driver, webFormObj.assignToDefault); } }else if
	 * (assignTo.equalsIgnoreCase("Corporate")) { if
	 * (!webFormObj.assignToCorporate) {
	 * fc.utobj().clickElement(driver, webFormObj.assignToCorporate); }
	 * fc.utobj().selectDropDown(driver, webFormObj.selectCorporateUser,
	 * userNameCorp);
	 * 
	 * }else if (assignTo.equalsIgnoreCase("Regional")) { if
	 * (!webFormObj.assignToRegional) {
	 * fc.utobj().clickElement(driver, webFormObj.assignToCorporate); }
	 * fc.utobj().selectDropDown(driver, webFormObj.selectAreaRegion,
	 * regionName); fc.utobj().selectDropDown(driver,
	 * webFormObj.selectAreaRegion, userNameReg);
	 * 
	 * }else if (assignTo.equalsIgnoreCase("Franchisee")) {
	 * 
	 * if (!webFormObj.assignToFranchise) {
	 * fc.utobj().clickElement(driver, webFormObj.assignToFranchise); }
	 * fc.utobj().selectDropDown(driver, webFormObj.selectFranchiseID,
	 * franchiseId); fc.utobj().selectDropDown(driver,
	 * webFormObj.selectFranchiseUser, userNameFranchise);
	 * 
	 * } if (afterSubmission.equalsIgnoreCase("message")) { if
	 * (!webFormObj.afterSubmissionMsg) {
	 * fc.utobj().clickElement(driver, webFormObj.afterSubmissionMsg); } }else
	 * if (afterSubmission.equalsIgnoreCase("url")) { if
	 * (!webFormObj.afterSubmissionUrl) {
	 * fc.utobj().clickElement(driver, webFormObj.afterSubmissionUrl); }
	 * fc.utobj().sendKeys(driver, webFormObj.redirectedUrl, redirectedUrl); }
	 * 
	 * fc.utobj().clickElement(driver, webFormObj.finishBtn);
	 * 
	 * if (!webFormObj.hostURL) { fc.utobj().clickElement(driver,
	 * webFormObj.hostURL); }
	 * 
	 * urlText=webFormObj.hostCodeBox.getText().trim();
	 * fc.utobj().clickElement(driver, webFormObj.okBtn);
	 * 
	 * 
	 * 
	 * 
	 * String LName=fc.utobj().generateTestData(dataSet.get("Lastname")); String
	 * FName=fc.utobj().generateTestData(dataSet.get("Firstname")); String
	 * parentWindow=driver.getWindowHandle(); fc.utobj().sendKeys(driver,
	 * CrmWebFormPage.searchMyForm, formName); fc.utobj().clickElement(driver,
	 * CrmWebFormPage.searchMyFormBtn); fc.utobj().actionImgOption(driver,
	 * formName, "Launch & Test");
	 * 
	 * fc.utobj().printTestStep(testCaseId, "Launc And Test Form"); Set<String>
	 * allWindows2=driver.getWindowHandles(); for (String currentWindow :
	 * allWindows2) { if (!currentWindow.equalsIgnoreCase(parentWindow)) {
	 * driver.switchTo().window(currentWindow); String
	 * titleTextCurrent=driver.getTitle(); if
	 * (titleTextCurrent.equalsIgnoreCase(formTitle)) { //fill the form with
	 * data
	 * 
	 * fc.utobj().isTextDisplayed(driver, header,
	 * "was not able to verify Header"); fc.utobj().isTextDisplayed(driver,
	 * formTitle, "was not able to verify Form Title");
	 * fc.utobj().isTextDisplayed(driver, textLabel,
	 * "was not able to verify Text Label"); fc.utobj().isTextDisplayed(driver,
	 * footer, "was not able to verify Footer");
	 * 
	 * fc.utobj().sendKeys(driver, CrmWebFormPage.leadLastName, LName);
	 * fc.utobj().sendKeys(driver, CrmWebFormPage.leadFirstName, FName);
	 * fc.utobj().selectDropDownByPartialText(driver, CrmWebFormPage.title,
	 * "Dr."); fc.utobj().sendKeys(driver, CrmWebFormPage.companyName,
	 * "companyName"); fc.utobj().sendKeys(driver, CrmWebFormPage.address,
	 * "address"); fc.utobj().sendKeys(driver, CrmWebFormPage.city, "city");
	 * fc.utobj().selectDropDownByPartialText(driver, CrmWebFormPage.country,
	 * "USA"); //fc.utobj().sendKeys(driver, CrmWebFormPage.zipcode, zipCode);
	 * fc.utobj().selectDropDownByPartialText(driver, CrmWebFormPage.state,
	 * "Alabama"); fc.utobj().sendKeys(driver, CrmWebFormPage.phoneNumbers,
	 * "1236547896"); fc.utobj().sendKeys(driver, CrmWebFormPage.extn, "12");
	 * fc.utobj().sendKeys(driver, CrmWebFormPage.faxNumbers, "1236577896");
	 * fc.utobj().sendKeys(driver, CrmWebFormPage.mobileNumbers, "9874563212");
	 * fc.utobj().sendKeys(driver, CrmWebFormPage.emailIds,
	 * "jhsdhfs@gmail.com"); fc.utobj().sendKeys(driver,
	 * CrmWebFormPage.alternateEmail, "jhsdhdasdfs@gmail.com");
	 * 
	 * fc.utobj().sendKeys(driver, fc.utobj().getElementByXpath(driver,
	 * ".//td[contains(text () , '"
	 * +FieldName1+"')]/following-sibling::td/input")), FieldName1);
	 * fc.utobj().sendKeys(driver, fc.utobj().getElementByXpath(driver,
	 * ".//td[contains(text () , '"
	 * +FieldName2+"')]/following-sibling::td/label/textarea")), FieldName2);
	 * fc.utobj().sendKeys(driver, fc.utobj().getElementByXpath(driver,
	 * ".//td[contains(text () , '"
	 * +FieldName3+"')]/following-sibling::td/table/tbody/tr/td/input")),
	 * "02/02/2017"); fc.utobj().clickElement(driver,
	 * fc.utobj().getElementByXpath(driver,".//td[contains(text () , '"
	 * +FieldName4+
	 * "')]/following-sibling::td/select/option[contains(text(),'Opt"+FieldName4
	 * +"')]"))); fc.utobj().clickElement(driver,
	 * fc.utobj().getElementByXpath(driver, ".//td[contains(text () , '"
	 * +FieldName5+
	 * "')]/following-sibling::td/input[@type='radio' and @value='1']")));
	 * fc.utobj().sendKeys(driver, fc.utobj().getElementByXpath(driver,
	 * ".//td[contains(text () , '"
	 * +FieldName7+"')]/following-sibling::td/input")), "99");
	 * fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
	 * ".//td[contains(text () , '"
	 * +FieldName6+"')]/following-sibling::td/input"))); try {
	 * fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
	 * ".//td[contains(text () , '"+FieldName8+
	 * "')]/following-sibling::td/div/div/ul/li/label[contains(text(),'Mul"+
	 * FieldName8+"')]/input"))); } catch (Exception e) {
	 * 
	 * }String fileName=fc.utobj().getFilePathFromTestData(config,
	 * dataSet.get("fileName"));
	 * fc.utobj().sendKeys(driver,fc.utobj().getElementByXpath(driver,
	 * ".//td[contains(text () , '"
	 * +FieldName9+"')]/following-sibling::td/input[@type='file']")),fileName);
	 * try{ fc.utobj().selectDropDownByPartialText(driver,
	 * CrmWebFormPage.leadSourceLaunch, leadSource);
	 * fc.utobj().selectDropDownByPartialText(driver,
	 * CrmWebFormPage.leadSourceDetailsLaunch, sourceDetails);
	 * //fc.utobj().clickElement(driver, CrmWebFormPage.nextBtn); }
	 * catch(Exception ew){} fc.utobj().isTextDisplayed(driver, header,
	 * "was not able to verify Header"); fc.utobj().isTextDisplayed(driver,
	 * formTitle, "was not able to verify Form Title");
	 * //fc.utobj().isTextDisplayed(driver, tabName,
	 * "was not able to verify Tab Name"); //fc.utobj().isTextDisplayed(driver,
	 * fieldLabel, "was not able to verify Fiedl Label");
	 * fc.utobj().isTextDisplayed(driver, footer,
	 * "was not able to verify Footer"); fc.utobj().clickElement(driver,
	 * CrmWebFormPage.submitBtn); //fc.utobj().isTextDisplayed(driver,
	 * "Thank you for submitting your information, we will get back to you shortly."
	 * , "was not able to verify confirmation Msg");
	 * 
	 * driver.close(); }else{ driver.close();
	 * driver.switchTo().window(parentWindow); }
	 * driver.switchTo().window(parentWindow); } }
	 * 
	 * CRMLeadsPageTest leadPageTest=new CRMLeadsPageTest(); CRMLeadsPage
	 * leadsPage = new CRMLeadsPage(driver); fc.crm().crm_common().CRMLeadsLnk(
	 * driver); //leadPageTest.searchLeadByOwner(driver, userNameF1+" "
	 * +userNameF1, "Open Leads"); fc.utobj().clickElement(driver,
	 * fc.utobj().getElementByXpath(driver,".//a[contains(text () , '"+FName+" "
	 * +LName+"')]")));
	 * 
	 * boolean isVisibilityPresent=fc.utobj().verifyCase(driver,
	 * ".//*[contains(text () ,'"+FieldName1+"')]"); if
	 * (isVisibilityPresent==false) { fc.utobj().throwsException(
	 * "was not able to "+FieldName1); } boolean
	 * isVisibilityPresent2=fc.utobj().verifyCase(driver,
	 * ".//*[contains(text () ,'"+FieldName2+"')]"); if
	 * (isVisibilityPresent2==false) { fc.utobj().throwsException(
	 * "was not able to "+FieldName2); } boolean
	 * isVisibilityPresent3=fc.utobj().verifyCase(driver,
	 * ".//*[contains(text () ,'"+FieldName3+"')]"); if
	 * (isVisibilityPresent3==false) { fc.utobj().throwsException(
	 * "was not able to "+FieldName3); } try { boolean isVisibilityPresent4 =
	 * fc.utobj().verifyCase(driver, ".//*[contains(text () ,'" + FieldName4 +
	 * "')]"); if (isVisibilityPresent4 == false) { fc.utobj().throwsException(
	 * "was not able to " + FieldName4); } } catch (Exception e) { } try {
	 * boolean isVisibilityPresent8 = fc.utobj().verifyCase(driver,
	 * ".//*[contains(text () ,'" + FieldName8 + "')]"); if
	 * (isVisibilityPresent8 == false) { fc.utobj().throwsException(
	 * "was not able to " + FieldName4); } } catch (Exception e) { } boolean
	 * isVisibilityPresent5=fc.utobj().verifyCase(driver,
	 * ".//*[contains(text () ,'"+FieldName7+"')]"); if
	 * (isVisibilityPresent5==false) { fc.utobj().throwsException(
	 * "was not able to "+FieldName6); } boolean
	 * isVisibilityPresent6=fc.utobj().verifyCase(driver,
	 * ".//*[contains(text () ,'"+FieldName6+"')]"); if
	 * (isVisibilityPresent6==false) { fc.utobj().throwsException(
	 * "was not able to "+FieldName6); }
	 * 
	 * fc.utobj().printTestResultExcel(config, testCaseId, null, null,"Passed",
	 * null, null); fc.utobj().logoutAndQuitBrowser(driver, config, testCaseId);
	 * 
	 * } catch (Exception e) { fc.utobj().quitBrowserOnCatch(driver, config, e,
	 * testCaseId); } }
	 * 
	 */
}
