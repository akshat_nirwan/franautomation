package com.builds.test.crm;

import java.util.List;
import java.util.Map;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import com.builds.test.admin.AdminConfigurationConfigureDecimalPlacePageTest;
import com.builds.uimaps.crm.AdminCRMManageProductServiceCategoryPage;
import com.builds.utilities.FranconnectUtil;
import com.builds.utilities.TestCase;

public class AdminCRMManageProductServiceCategoryPageTest {
	FranconnectUtil fc = new FranconnectUtil();

	@Test(groups = "crm")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Add Category At Admin > CRM > Manage Product , Service & Category", testCaseId = "TC_38_Add_Category")
	public void addCategory() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			AdminCRMManageProductServiceCategoryPage pobj = new AdminCRMManageProductServiceCategoryPage(driver);

			fc.utobj().printTestStep("Navigating to Admin > CRM >  Manage Product / Service & Category");
			fc.utobj().printTestStep("Add Category");

			fc.crm().crm_common().adminCRMManageProductServiceCategoryLnk(driver);

			fc.utobj().clickElement(driver, pobj.manageCategoryTab);
			fc.utobj().clickElement(driver, pobj.addCategoryBtn);
			String categoryName = fc.utobj().generateTestData(dataSet.get("categoryName"));
			fc.utobj().sendKeys(driver, pobj.categoryName, categoryName);
			String description = fc.utobj().generateTestData(dataSet.get("description"));
			fc.utobj().sendKeys(driver, pobj.description, description);
			fc.utobj().clickElement(driver, pobj.addBtn);
			fc.utobj().sendKeys(driver, pobj.categorySearch, categoryName);

			fc.utobj().printTestStep("Verify The Add Category");
			boolean isTextPresent = fc.utobj().assertPageSource(driver, categoryName);
			if (isTextPresent == false) {
				fc.utobj().throwsException("was not able to add Category");
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	public void addCategory(WebDriver driver, String categoryName, String description) throws Exception {

		String testCaseId = "TC_Add_Category_Product_CRM";

		if (fc.utobj().validate(testCaseId)) {
			try {
				AdminCRMManageProductServiceCategoryPage pobj = new AdminCRMManageProductServiceCategoryPage(driver);
				fc.crm().crm_common().adminCRMManageProductServiceCategoryLnk(driver);
				fc.utobj().clickElement(driver, pobj.manageCategoryTab);
				fc.utobj().clickElement(driver, pobj.addCategoryBtn);
				fc.utobj().sendKeys(driver, pobj.categoryName, categoryName);
				fc.utobj().sendKeys(driver, pobj.description, description);
				fc.utobj().clickElement(driver, pobj.addBtn);

				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException(
					"was not able to add Category for Product And Service :: Admin > CRM > Manage Product , Service & Category.");

		}
	}

	@Test(groups = {"crm"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Modify Category At Admin > CRM > Manage Product , Service & Category", testCaseId = "TC_39_Modify_Category_Details")
	public void modifyCategoryDetails() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			AdminCRMManageProductServiceCategoryPage pobj = new AdminCRMManageProductServiceCategoryPage(driver);

			fc.utobj().printTestStep("Navigating to Admin > CRM >  Manage Product / Service & Category");
			fc.utobj().printTestStep("Add Category");
			String categoryName = fc.utobj().generateTestData(dataSet.get("categoryName"));
			String description = fc.utobj().generateTestData(dataSet.get("description"));
			addCategory(driver, categoryName, description);
			fc.utobj().sendKeys(driver, pobj.categorySearch, categoryName);

			fc.utobj().printTestStep("Modify The Category");
			fc.utobj().actionImgOption(driver, categoryName, "Modify Details");
			categoryName = fc.utobj().generateTestData(dataSet.get("categoryName"));
			fc.utobj().sendKeys(driver, pobj.categoryName, categoryName);
			description = fc.utobj().generateTestData(dataSet.get("description"));
			fc.utobj().sendKeys(driver, pobj.description, description);
			fc.utobj().clickElement(driver, pobj.saveBtn);
			fc.utobj().sendKeys(driver, pobj.categorySearch, categoryName);

			fc.utobj().printTestStep("Verify The Modify Category");
			boolean isTextPresent = fc.utobj().assertPageSource(driver, categoryName);
			if (isTextPresent == false) {
				fc.utobj().throwsException("was not able to modify Category");
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "crm")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify the Delete Category At Admin > CRM > Manage Product , Service & Category", testCaseId = "TC_40_Delete_Category")
	public void deleteCategory() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			AdminCRMManageProductServiceCategoryPage pobj = new AdminCRMManageProductServiceCategoryPage(driver);

			fc.utobj().printTestStep("Navigating to Admin > CRM >  Manage Product / Service & Category");
			fc.utobj().printTestStep("Add Category");
			String categoryName = fc.utobj().generateTestData(dataSet.get("categoryName"));
			String description = fc.utobj().generateTestData(dataSet.get("description"));
			addCategory(driver, categoryName, description);
			fc.utobj().sendKeys(driver, pobj.categorySearch, categoryName);

			fc.utobj().printTestStep("Delete Category");
			fc.utobj().actionImgOption(driver, categoryName, "Delete");
			fc.utobj().acceptAlertBox(driver);

			fc.utobj().printTestStep("Verify Delete Category Category");
			boolean isTextPresent = fc.utobj().assertPageSource(driver, categoryName);
			if (isTextPresent == true) {
				fc.utobj().throwsException("was not able to delete Category");
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = {"crm"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify the Add Product Service At Admin > CRM > Manage Product , Service & Category", testCaseId = "TC_41_Add_Product_Service")
	public void addProductService() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			AdminCRMManageProductServiceCategoryPage pobj = new AdminCRMManageProductServiceCategoryPage(driver);

			fc.utobj().printTestStep("Navigating to Admin > CRM >  Manage Product / Service & Category");
			fc.utobj().printTestStep("Add Category");
			String categoryName = fc.utobj().generateTestData(dataSet.get("categoryName"));
			String description = fc.utobj().generateTestData(dataSet.get("description"));
			addCategory(driver, categoryName, description);

			fc.utobj().printTestStep("Add Product/Service");

			fc.utobj().clickElement(driver, pobj.manageProductServiceTab);
			fc.utobj().clickElement(driver, pobj.addProductServiceBtn);
			String productName = fc.utobj().generateTestData(dataSet.get("productName"));
			fc.utobj().sendKeys(driver, pobj.productName, productName);
			String oneLineDescription = fc.utobj().generateTestData(dataSet.get("oneLineDescription"));
			fc.utobj().sendKeys(driver, pobj.oneLineItemDescription, oneLineDescription);
			String rate = dataSet.get("rate");
			fc.utobj().sendKeys(driver, pobj.rate, rate);
			fc.utobj().selectDropDown(driver, pobj.categoryIDSelect, categoryName);
			if (!fc.utobj().isSelected(driver,pobj.activeCheck)) {
				fc.utobj().clickElement(driver, pobj.activeCheck);
			}
			fc.utobj().clickElement(driver, pobj.addBtn);
			fc.utobj().sendKeys(driver, pobj.productServiceSearch, productName);

			fc.utobj().sendKeys(driver, pobj.categorySearch, categoryName);

			fc.utobj().printTestStep("Verify The Add Product/Service");
			fc.utobj().sleep(3000);
			
			//Added //tr/td in code
			//.//*[@id='print123Div']//tr/td[contains(text () ,'Test41Productj30105123')]
			if (!fc.utobj()
					.getElementByXpath(driver, ".//*[@id='print123Div']//tr/td[contains(text () ,'" + productName + "')]")
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Product Name");
			}
			if (!driver
					.findElement(
							By.xpath(".//*[@id='print123Div']//*[contains(text () ,'" + oneLineDescription + "')]"))
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify One Line Description Name");
			}

			if (!fc.utobj()
					.getElementByXpath(driver, ".//*[@id='print123Div']//td[contains(text () ,'" + categoryName + "')]")
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Category Name");
			}

			rate = rate.concat(".00 / Unit");
			if (!fc.utobj().getElementByXpath(driver, ".//*[@id='print123Div']//*[contains(text () ,'" + rate + "')]")
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Rate");
			}
			if (!driver
					.findElement(By.xpath(".//*[@id='print123Div']//*[contains(text () ,'FranConnect Administrator')]"))
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Added By");
			}
			if (!fc.utobj().getElementByXpath(driver, ".//*[@id='print123Div']//td[contains(text () ,'Active')]")
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Status of Product");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	public void addProductService(WebDriver driver, String categoryName, String productName, String oneLineDescription,
			String rate) throws Exception {

		String testCaseId = "TC_Add_Product_Service";

		if (fc.utobj().validate(testCaseId)) {
			try {
				AdminCRMManageProductServiceCategoryPage pobj = new AdminCRMManageProductServiceCategoryPage(driver);
				fc.utobj().clickElement(driver, pobj.manageProductServiceTab);
				fc.utobj().clickElement(driver, pobj.addProductServiceBtn);
				fc.utobj().sendKeys(driver, pobj.productName, productName);
				fc.utobj().sendKeys(driver, pobj.oneLineItemDescription, oneLineDescription);
				fc.utobj().sendKeys(driver, pobj.rate, rate);
				fc.utobj().selectDropDown(driver, pobj.categoryIDSelect, categoryName);
				if (!fc.utobj().isSelected(driver,pobj.activeCheck)) {
					fc.utobj().clickElement(driver, pobj.activeCheck);
				}
				fc.utobj().clickElement(driver, pobj.addBtn);

				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException(
					"was not able to Add Product Service ::Admin > CRM > Manage Product , Service & Category. ");

		}
	}

	@Test(groups = { "crm" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify the View Product Service Details At Admin > CRM > Manage Product , Service & Category", testCaseId = "TC_42_View_Product_Service_Details")
	public void viewProductServicesDetails() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			AdminCRMManageProductServiceCategoryPage pobj = new AdminCRMManageProductServiceCategoryPage(driver);

			fc.utobj().printTestStep("Navigate To Admin > Configuration >  Configure Decimal Place");
			fc.utobj().printTestStep("Configure Decimal Place with two place");
			new AdminConfigurationConfigureDecimalPlacePageTest().configureDecimalPlace(driver, "two");

			fc.utobj().printTestStep("Navigating to Admin > CRM >  Manage Product / Service & Category");
			fc.utobj().printTestStep("Add Category");
			String categoryName = fc.utobj().generateTestData(dataSet.get("categoryName"));
			String description = fc.utobj().generateTestData(dataSet.get("description"));
			addCategory(driver, categoryName, description);

			fc.utobj().printTestStep("Add Product/Service");
			String productName = fc.utobj().generateTestData(dataSet.get("productName"));
			String oneLineDescription = fc.utobj().generateTestData(dataSet.get("oneLineDescription"));
			String rate = dataSet.get("rate");
			addProductService(driver, categoryName, productName, oneLineDescription, rate);

			fc.utobj().sendKeys(driver, pobj.productServiceSearch, productName);

			fc.utobj().sendKeys(driver, pobj.categorySearch, categoryName);
			
			
			fc.utobj().sleep(3000);
			fc.utobj().printTestStep("View Product Details And Verify");
			fc.utobj().actionImgOption(driver, productName, "View Details");

			if (!fc.utobj().getElementByXpath(driver,(".//td[contains(text () ,'Product / Service')]/following-sibling::td[contains(text () ,'"
							+ productName + "')]"))
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Product Service Name");
			}

			if (!fc.utobj().getElementByXpath(driver,".//td[contains(text () ,'One Line Description')]/following-sibling::td[contains(text () ,'"
							+ oneLineDescription + "')]")
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify One Line Description");
			}

			if (!driver
					.findElement(By.xpath(".//td[contains(text () ,'Rate')]/following-sibling::td[contains(text () ,'"
							+ dataSet.get("rate").concat(".00") + "')]"))
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Rate");
			}
			if (!fc.utobj().getElementByXpath(driver,(".//td[contains(text () ,'Category')]/following-sibling::td[contains(text () ,'"
							+ categoryName + "')]"))
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Category Name");
			}
			/*if (!driver
					.findElement(By
							.xpath(".//td[contains(text () ,'Status')]/following-sibling::td[contains(text () ,'Active')]"))
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Status");
			}*/
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = {"crm"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify the Modify Product Service At Admin > CRM > Manage Product , Service & Category", testCaseId = "TC_43_Modify_Product_Service_Details")
	public void modifyProductServiceDetails() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			AdminCRMManageProductServiceCategoryPage pobj = new AdminCRMManageProductServiceCategoryPage(driver);

			fc.utobj().printTestStep("Navigating to Admin > CRM >  Manage Product / Service & Category");
			fc.utobj().printTestStep("Add Category");
			String categoryName = fc.utobj().generateTestData(dataSet.get("categoryName"));
			String description = fc.utobj().generateTestData(dataSet.get("description"));
			addCategory(driver, categoryName, description);

			fc.utobj().printTestStep("Add Product/Service");
			String productName = fc.utobj().generateTestData(dataSet.get("productName"));
			String oneLineDescription = fc.utobj().generateTestData(dataSet.get("oneLineDescription"));
			String rate = dataSet.get("rate");
			addProductService(driver, categoryName, productName, oneLineDescription, rate);

			fc.utobj().sendKeys(driver, pobj.productServiceSearch, productName);
			fc.utobj().sendKeys(driver, pobj.categorySearch, categoryName);
			fc.utobj().printTestStep("Modify Details");
			fc.utobj().actionImgOption(driver, productName, "Modify Details");

			productName = fc.utobj().generateTestData(dataSet.get("productName"));
			fc.utobj().sendKeys(driver, pobj.productName, productName);
			oneLineDescription = fc.utobj().generateTestData(dataSet.get("oneLineDescription"));
			fc.utobj().sendKeys(driver, pobj.oneLineItemDescription, oneLineDescription);
			rate = dataSet.get("rate");
			fc.utobj().sendKeys(driver, pobj.rate, rate);
			fc.utobj().selectDropDown(driver, pobj.categoryIDSelect, categoryName);
			if (!fc.utobj().isSelected(driver,pobj.activeCheck)) {
				fc.utobj().clickElement(driver, pobj.activeCheck);
			}
			fc.utobj().clickElement(driver, pobj.saveBtn);
			fc.utobj().sendKeys(driver, pobj.productServiceSearch, productName);

			fc.utobj().sendKeys(driver, pobj.categorySearch, categoryName);
			fc.utobj().sleep(3000);
			fc.utobj().printTestStep("Verify The Modify Details");
			if (!fc.utobj()
					.getElementByXpath(driver, ".//*[@id='print123Div']//*[contains(text () ,'" + productName + "')]")
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Product Name");
			}
			if (!driver
					.findElement(
							By.xpath(".//*[@id='print123Div']//*[contains(text () ,'" + oneLineDescription + "')]"))
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify One Line Description Name");
			}

			if (!fc.utobj()
					.getElementByXpath(driver, ".//*[@id='print123Div']//td[contains(text () ,'" + categoryName + "')]")
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Category Name");
			}

			rate = rate.concat(".00 / Unit");
			if (!fc.utobj().getElementByXpath(driver, ".//*[@id='print123Div']//*[contains(text () ,'" + rate + "')]")
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Rate");
			}
			if (!driver
					.findElement(By.xpath(".//*[@id='print123Div']//*[contains(text () ,'FranConnect Administrator')]"))
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Added By");
			}
			if (!fc.utobj().getElementByXpath(driver, ".//*[@id='print123Div']//td[contains(text () ,'Active')]")
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Status of Product");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = {"crm" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify the Deactivate Product Service Details At Admin > CRM > Manage Product , Service & Category", testCaseId = "TC_44_Deactivate_Product_Service")
	public void deactiveProductService() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			AdminCRMManageProductServiceCategoryPage pobj = new AdminCRMManageProductServiceCategoryPage(driver);

			fc.utobj().printTestStep("Navigating to Admin > CRM >  Manage Product / Service & Category");
			fc.utobj().printTestStep("Add Category");
			String categoryName = fc.utobj().generateTestData(dataSet.get("categoryName"));
			String description = fc.utobj().generateTestData(dataSet.get("description"));
			addCategory(driver, categoryName, description);

			fc.utobj().printTestStep("Add Product/Service");
			String productName = fc.utobj().generateTestData(dataSet.get("productName"));
			String oneLineDescription = fc.utobj().generateTestData(dataSet.get("oneLineDescription"));
			String rate = dataSet.get("rate");
			addProductService(driver, categoryName, productName, oneLineDescription, rate);

			fc.utobj().sendKeys(driver, pobj.productServiceSearch, productName);

			fc.utobj().sendKeys(driver, pobj.categorySearch, categoryName);

			fc.utobj().printTestStep("Deactivate Product/Service");
			fc.utobj().actionImgOption(driver, productName, "Deactivate");
			fc.utobj().acceptAlertBox(driver);

			fc.utobj().sendKeys(driver, pobj.productServiceSearch, productName);

			fc.utobj().sendKeys(driver, pobj.categorySearch, categoryName);
			fc.utobj().sleep(3000);
			fc.utobj().printTestStep("Verify Deactivate Product/Service");

			if (!fc.utobj()
					.getElementByXpath(driver, ".//*[@id='print123Div']//td[contains(text () ,'" + productName + "')]")
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Product Name");
			}
			if (!driver
					.findElement(
							By.xpath(".//*[@id='print123Div']//td[contains(text () ,'" + oneLineDescription + "')]"))
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify One Line Description Name");
			}

			if (!fc.utobj()
					.getElementByXpath(driver, ".//*[@id='print123Div']//td[contains(text () ,'" + categoryName + "')]")
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Category Name");
			}

			rate = rate.concat(".00 / Unit");
			if (!fc.utobj().getElementByXpath(driver, ".//*[@id='print123Div']//td[contains(text () ,'" + rate + "')]")
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Rate");
			}
			if (!driver
					.findElement(By.xpath(".//*[@id='print123Div']//td[contains(text () ,'FranConnect Administrator')]"))
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Added By");
			}
			if (!fc.utobj().getElementByXpath(driver, ".//*[@id='print123Div']//td[contains(text () ,'Inactive')]")
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Status of Product");
			}

			String alterText = driver
					.findElement(By.xpath(
							".//*[contains(text () ,'" + productName + "')]/ancestor::tr/td/div[@id='menuBar']/layer"))
					.getAttribute("id").trim();
			alterText = alterText.replace("Actions_dynamicmenu", "");
			alterText = alterText.replace("Bar", "");

			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//*[contains(text () ,'" + productName + "')]/ancestor::tr/td/div[@id='menuBar']/layer/a/img"));
			List<WebElement> listElements = driver
					.findElements(By.xpath(".//*[@id='Actions_dynamicmenu" + alterText + "Menu']/div"));

			String text = null;
			boolean status = false;

			for (int i = 0; i < listElements.size(); i++) {

				text = listElements.get(i).getText().trim();
				if (text.equalsIgnoreCase("Activate")) {
					status = true;
					break;
				}
			}

			if (status == false) {
				fc.utobj().throwsException("was not able to verify Activate At Action img icon");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "crm" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Activate Product Service Details At Admin > CRM > Manage Product , Service & Category", testCaseId = "TC_45_Activate_Product_Service")
	public void activeProductService() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			AdminCRMManageProductServiceCategoryPage pobj = new AdminCRMManageProductServiceCategoryPage(driver);

			fc.utobj().printTestStep("Navigating to Admin > CRM >  Manage Product / Service & Category");
			fc.utobj().printTestStep("Add Category");
			String categoryName = fc.utobj().generateTestData(dataSet.get("categoryName"));
			String description = fc.utobj().generateTestData(dataSet.get("description"));
			addCategory(driver, categoryName, description);

			fc.utobj().printTestStep("Add Product/Service");
			String productName = fc.utobj().generateTestData(dataSet.get("productName"));
			String oneLineDescription = fc.utobj().generateTestData(dataSet.get("oneLineDescription"));
			String rate = dataSet.get("rate");
			addProductService(driver, categoryName, productName, oneLineDescription, rate);

			fc.utobj().sendKeys(driver, pobj.productServiceSearch, productName);

			fc.utobj().sendKeys(driver, pobj.categorySearch, categoryName);

			fc.utobj().printTestStep("Deactivate Product/Service");
			fc.utobj().actionImgOption(driver, productName, "Deactivate");
			fc.utobj().acceptAlertBox(driver);

			fc.utobj().sendKeys(driver, pobj.productServiceSearch, productName);

			fc.utobj().sendKeys(driver, pobj.categorySearch, categoryName);
			fc.utobj().sleep(3000);
			fc.utobj().printTestStep("Activate Product/Service");
			fc.utobj().actionImgOption(driver, productName, "Activate");
			fc.utobj().acceptAlertBox(driver);

			fc.utobj().sendKeys(driver, pobj.productServiceSearch, productName);

			fc.utobj().sendKeys(driver, pobj.categorySearch, categoryName);

			fc.utobj().printTestStep("Verify Product/Service");
			if (!fc.utobj()
					.getElementByXpath(driver, ".//*[@id='print123Div']//*[contains(text () ,'" + productName + "')]")
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Product Name");
			}
			if (!driver
					.findElement(
							By.xpath(".//*[@id='print123Div']//*[contains(text () ,'" + oneLineDescription + "')]"))
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify One Line Description Name");
			}

			if (!fc.utobj()
					.getElementByXpath(driver, ".//*[@id='print123Div']//td[contains(text () ,'" + categoryName + "')]")
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Category Name");
			}

			rate = rate.concat(".00 / Unit");
			if (!fc.utobj().getElementByXpath(driver, ".//*[@id='print123Div']//*[contains(text () ,'" + rate + "')]")
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Rate");
			}
			if (!driver
					.findElement(By.xpath(".//*[@id='print123Div']//*[contains(text () ,'FranConnect Administrator')]"))
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Added By");
			}
			if (!fc.utobj().getElementByXpath(driver, ".//*[@id='print123Div']//td[contains(text () ,'Active')]")
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Status of Product");
			}

			String alterText = driver
					.findElement(By.xpath(
							".//*[contains(text () ,'" + productName + "')]/ancestor::tr/td/div[@id='menuBar']/layer"))
					.getAttribute("id").trim();
			alterText = alterText.replace("Actions_dynamicmenu", "");
			alterText = alterText.replace("Bar", "");

			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//*[contains(text () ,'" + productName + "')]/ancestor::tr/td/div[@id='menuBar']/layer/a/img"));
			List<WebElement> listElements = driver
					.findElements(By.xpath(".//*[@id='Actions_dynamicmenu" + alterText + "Menu']/div"));

			String text = null;
			boolean status = false;

			for (int i = 0; i < listElements.size(); i++) {

				text = listElements.get(i).getText().trim();
				if (text.equalsIgnoreCase("Deactivate")) {
					status = true;
					break;
				}
			}

			if (status == false) {
				fc.utobj().throwsException("was not able to verify Activate At Action img icon");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}
}
