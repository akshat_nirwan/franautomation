package com.builds.test.crm;

import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.Test;

import com.builds.test.admin.AdminFranchiseLocationAddFranchiseLocationPageTest;
import com.builds.test.admin.AdminUsersManageCorporateUsersAddCorporateUserPageTest;
import com.builds.test.admin.AdminUsersManageManageFranchiseUsersPageTest;
import com.builds.test.admin.AdminUsersManageRegionalUsersAddRegionalUserPageTest;
import com.builds.test.common.CorporateUser;
import com.builds.uimaps.crm.AdminCRMManageWebFormGeneratorPage;
import com.builds.uimaps.crm.CRMHomePage;
import com.builds.uimaps.crm.CRMLeadsPage;
import com.builds.utilities.FranconnectUtil;
import com.builds.utilities.TestCase;

public class AdminCRMManageWebFormGeneratorPageTest {
	FranconnectUtil fc = new FranconnectUtil();

	@Test(groups = { "crmFormGenerator", "crmZC", "crmTest2224" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2017-06-14", testCaseDescription = "Verify The Create New Form At Admin > CRM > Manage Web Form Generator", testCaseId = "TC_46_Create_New_Form")
	public void createNewForm() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			AdminCRMManageWebFormGeneratorPage pobj = new AdminCRMManageWebFormGeneratorPage(driver);

			fc.utobj().printTestStep("Navigate To Admin > CRM > Manage Web Form Generator");
			fc.utobj().printTestStep("Create New Form");
			fc.crm().crm_common().adminCRMManageWebFormGeneratorLnk(driver);
			fc.utobj().clickElement(driver, pobj.createNewForm);
			String formName = fc.utobj().generateTestData(dataSet.get("formName"));
			fc.utobj().sendKeys(driver, pobj.formName, formName);
			String formTitle = fc.utobj().generateTestData(dataSet.get("formTitle"));
			fc.utobj().sendKeys(driver, pobj.formDisplayTitle, formTitle);
			if (!fc.utobj().isSelected(driver,pobj.displayFormTitleCheckBox)) {
				fc.utobj().clickElement(driver, pobj.displayFormTitleCheckBox);
			}

			String description = fc.utobj().generateTestData(dataSet.get("description"));
			fc.utobj().sendKeys(driver, pobj.formDescription, description);

			if (!fc.utobj().isSelected(driver, pobj.formTypeLeads)) {
				fc.utobj().clickElement(driver, pobj.formTypeLeads);
			}
			fc.utobj().selectDropDown(driver, pobj.formFormat, "Single Page");
			fc.utobj().selectDropDown(driver, pobj.columnCount, "1");
			fc.utobj().selectDropDown(driver, pobj.filedLabelAlignment, "Left");
			// fc.utobj().selectDropDown(driver, pobj.formLanguage, "English");
			fc.utobj().sendKeys(driver, pobj.iframeWidth, "100%");
			fc.utobj().sendKeys(driver, pobj.iframeHeight, "500");
			String formUrl = fc.utobj().generateTestData(dataSet.get("formUrl"));
			fc.utobj().sendKeys(driver, pobj.formUrl, formUrl);
			fc.utobj().clickElement(driver, pobj.saveNextBtn);

			// Add Section
			fc.utobj().clickElement(driver, pobj.addSection);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			String sectionName = fc.utobj().generateTestData(dataSet.get("sectionName"));
			fc.utobj().sendKeys(driver, pobj.sectionName, sectionName);
			fc.utobj().selectDropDown(driver, pobj.noOfCols, "1");
			fc.utobj().selectDropDown(driver, pobj.fieldLabelAlignmentSection, "Left");
			fc.utobj().clickElement(driver, pobj.addBtn);
			fc.utobj().switchFrameToDefault(driver);

			// add Header
			fc.utobj().doubleClickElement(driver, pobj.addHeader);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().switchFrameById(driver, "textBlock_ifr");
			String header = fc.utobj().generateTestData(dataSet.get("header"));

			WebElement editorTextArea = fc.utobj().getElementByXpath(driver, ".//*[@id='tinymce']/p");
			Actions actions = new Actions(driver);
			actions.moveToElement(editorTextArea);
			actions.click();
			actions.sendKeys(header);
			fc.utobj().logReport("Entering Value in Text Field", editorTextArea);
			fc.utobj().logReportMsg("Entered Text", header);
			actions.build().perform();
			fc.utobj().switchFrameToDefault(driver);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().clickElement(driver, pobj.saveBtn);
			fc.utobj().switchFrameToDefault(driver);

			// Add Text
			fc.utobj().doubleClickElement(driver, pobj.defaultSectionHeaderAddText);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			String textLabel = fc.utobj().generateTestData(dataSet.get("labelText"));
			fc.utobj().sendKeys(driver, pobj.labelAddText, textLabel);
			fc.utobj().selectDropDown(driver, pobj.headerLabelAlignment, "Left");
			fc.utobj().clickElement(driver, pobj.saveBtn);
			fc.utobj().switchFrameToDefault(driver);

			// FormTool
			fc.utobj().clickElement(driver, pobj.availableFieldsTab);
			fc.utobj().clickElement(driver, pobj.formTools);

			fc.utobj().dragAndDropElement(driver, pobj.formToolText, pobj.defaultDropHere);

			fc.utobj().dragAndDropElement(driver, pobj.formToolCaptcha, pobj.defaultDropHere);

			fc.utobj().dragAndDropElement(driver, pobj.formToolIAgreeCheckBoxNew, pobj.defaultDropHere);

			// Lead Info
			fc.utobj().clickElement(driver, pobj.leadInfo);
			fc.utobj().sendKeys(driver, pobj.searchField, "First Name");

			fc.utobj().dragAndDropElement(driver, pobj.firstNameLeadInfo, pobj.defaultDropHere);

			fc.utobj().sendKeys(driver, pobj.searchField, "Title");

			fc.utobj().dragAndDropElement(driver, pobj.titleLeadInfo, pobj.defaultDropHere);

			fc.utobj().sendKeys(driver, pobj.searchField, "Company");

			fc.utobj().dragAndDropElement(driver, pobj.companyLeadInfo, pobj.defaultDropHere);

			fc.utobj().sendKeys(driver, pobj.searchField, "Address");

			fc.utobj().dragAndDropElement(driver, pobj.addressLeadInfo, pobj.defaultDropHere);

			fc.utobj().sendKeys(driver, pobj.searchField, "City");

			fc.utobj().dragAndDropElement(driver, pobj.cityLeadInfo, pobj.defaultDropHere);

			fc.utobj().sendKeys(driver, pobj.searchField, "Country");

			fc.utobj().dragAndDropElement(driver, pobj.countryLeadInfo, pobj.defaultDropHere);

			fc.utobj().sendKeys(driver, pobj.searchField, "Zip / Postal Code");

			fc.utobj().dragAndDropElement(driver, pobj.zipPostalCodeLeadInfo, pobj.defaultDropHere);

			fc.utobj().sendKeys(driver, pobj.searchField, "State / Province");

			fc.utobj().dragAndDropElement(driver, pobj.stateProvinceLeadInfo, pobj.defaultDropHere);

			fc.utobj().sendKeys(driver, pobj.searchField, "Phone");

			fc.utobj().dragAndDropElement(driver, pobj.phoneLeadInfo, pobj.defaultDropHere);

			fc.utobj().sendKeys(driver, pobj.searchField, "Phone Extension");

			fc.utobj().dragAndDropElement(driver, pobj.phoneExtensionLeadInfo, pobj.defaultDropHere);

			fc.utobj().sendKeys(driver, pobj.searchField, "Fax");

			fc.utobj().dragAndDropElement(driver, pobj.faxLeadInfo, pobj.defaultDropHere);

			fc.utobj().sendKeys(driver, pobj.searchField, "Mobile");

			fc.utobj().dragAndDropElement(driver, pobj.mobileLeadInfo, pobj.defaultDropHere);

			fc.utobj().sendKeys(driver, pobj.searchField, "Email");

			fc.utobj().dragAndDropElement(driver, pobj.emailLeadInfo, pobj.defaultDropHere);

			fc.utobj().sendKeys(driver, pobj.searchField, "Alternate Email");

			fc.utobj().dragAndDropElement(driver, pobj.alternateEmailLeadInfo, pobj.defaultDropHere);

			fc.utobj().sendKeys(driver, pobj.searchField, "Job Title");

			fc.utobj().dragAndDropElement(driver, pobj.jobTitleLeadInfo, pobj.defaultDropHere);

			fc.utobj().sendKeys(driver, pobj.searchField, "Suffix");

			fc.utobj().dragAndDropElement(driver, pobj.suffixLeadInfo, pobj.defaultDropHere);

			fc.utobj().sendKeys(driver, pobj.searchField, "Birthdate");

			fc.utobj().dragAndDropElement(driver, pobj.birthdateLeadInfo, pobj.defaultDropHere);

			fc.utobj().sendKeys(driver, pobj.searchField, "Anniversary Date");

			fc.utobj().dragAndDropElement(driver, pobj.anniversaryDateLeadInfo, pobj.defaultDropHere);

			fc.utobj().sendKeys(driver, pobj.searchField, "How did you hear about us ? (Lead Source)");

			fc.utobj().dragAndDropElement(driver, pobj.hdyhauLSLeadInfo, pobj.defaultDropHere);

			fc.utobj().sendKeys(driver, pobj.searchField, "Best Time To Contact");

			fc.utobj().dragAndDropElement(driver, pobj.bestTimetoContactLeadInfo, pobj.defaultDropHere);

			fc.utobj().sendKeys(driver, pobj.searchField, "Comments");

			fc.utobj().dragAndDropElement(driver, pobj.commentsLeadInfo, pobj.defaultDropHere);

			// add Text in custom section
			fc.utobj().doubleClickElement(driver, fc.utobj().getElementByXpath(driver,(".//*[.='" + sectionName + "']/following-sibling::table//ul/li[@class='emptyList']")));
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			String fieldLabel = fc.utobj().generateTestData(dataSet.get("sectionLabelText"));
			fc.utobj().sendKeys(driver, pobj.labelAddText, fieldLabel);
			fc.utobj().selectDropDown(driver, pobj.headerLabelAlignment, "Left");
			fc.utobj().clickElement(driver, pobj.saveBtn);
			fc.utobj().switchFrameToDefault(driver);

			// add Text Drop Here Custom
			fc.utobj().clickElement(driver, pobj.formTools);
			fc.utobj().dragAndDropElement(driver, pobj.formToolText, fc.utobj().getElementByXpath(driver, ".//*[.='"
					+ sectionName + "']/following-sibling::table//li[@class='emptyList']//p[.='Drop here.']"));

			// add Footer
			fc.utobj().doubleClickElement(driver, pobj.addFooterDefault);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().switchFrameById(driver, "textBlock_ifr");
			String footer = fc.utobj().generateTestData(dataSet.get("footer"));

			WebElement editorTextArea1 = fc.utobj().getElementByXpath(driver, ".//*[@id='tinymce']/p");
			Actions actions1 = new Actions(driver);
			actions1.moveToElement(editorTextArea1);
			actions1.click();
			actions1.sendKeys(footer);
			fc.utobj().logReport("Entering Value in Text Field", editorTextArea1);
			fc.utobj().logReportMsg("Entered Text", footer);
			actions1.build().perform();
			fc.utobj().switchFrameToDefault(driver);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().clickElement(driver, pobj.saveBtn);
			fc.utobj().switchFrameToDefault(driver);

			// Save and Preview
			fc.utobj().printTestStep("Preview The Form");
			String parentWindow = driver.getWindowHandle();
			fc.utobj().clickElement(driver, pobj.saveAndPreviewBtn);

			Set<String> allWindows = driver.getWindowHandles();
			for (String currentWindow : allWindows) {
				if (!currentWindow.equalsIgnoreCase(parentWindow)) {
					driver.switchTo().window(currentWindow);
					String titleTextCurrent = driver.getTitle();
					if (titleTextCurrent.equalsIgnoreCase(formTitle)) {
						fc.utobj().isTextDisplayed(driver, header, "was not able to verify Header");
						fc.utobj().isTextDisplayed(driver, formTitle, "was not able to verify Form Title");
						fc.utobj().isTextDisplayed(driver, sectionName, "was not able to verify Section Name");
						fc.utobj().isTextDisplayed(driver, textLabel, "was not able to verify Label Text");
						fc.utobj().isTextDisplayed(driver, fieldLabel, "was not able to verify Section Label Text");
						fc.utobj().isTextDisplayed(driver, formTitle, "was not able to verify Footer");
						driver.close();
					} else {
						driver.close();
						driver.switchTo().window(parentWindow);
					}
					driver.switchTo().window(parentWindow);
				}
			}

			fc.utobj().clickElement(driver, pobj.saveAndNextBtnDesign);
			// settings page
			fc.utobj().selectDropDown(driver, pobj.cmLeadStatusID, "New");
			fc.utobj().selectDropDown(driver, pobj.contactMediumSelect, "Email");
			fc.utobj().selectDropDown(driver, pobj.leadSource, "Advertisement");
			fc.utobj().selectDropDown(driver, pobj.leadSourceDetailsSelect, "Magazine");
			if (!fc.utobj().isSelected(driver, pobj.assignToDefault)) {
				fc.utobj().clickElement(driver, pobj.assignToDefault);
			}
			if (!fc.utobj().isSelected(driver, pobj.afterSubmissionMsg)) {
				fc.utobj().clickElement(driver, pobj.afterSubmissionMsg);
			}

			// preview form at Setting page
			fc.utobj().printTestStep("Preview Form At Setting Page");
			fc.utobj().clickElement(driver, pobj.previewFormAtSettingPage);

			Set<String> allWindows1 = driver.getWindowHandles();
			for (String currentWindow : allWindows1) {
				if (!currentWindow.equalsIgnoreCase(parentWindow)) {
					driver.switchTo().window(currentWindow);
					String titleTextCurrent = driver.getTitle();
					if (titleTextCurrent.equalsIgnoreCase(formTitle)) {
						fc.utobj().isTextDisplayed(driver, header, "was not able to verify Header");
						fc.utobj().isTextDisplayed(driver, formTitle, "was not able to verify Form Title");
						fc.utobj().isTextDisplayed(driver, sectionName, "was not able to verify Section Name");
						fc.utobj().isTextDisplayed(driver, textLabel, "was not able to verify Label Text");
						fc.utobj().isTextDisplayed(driver, fieldLabel, "was not able to verify Section Label Text");
						fc.utobj().isTextDisplayed(driver, footer, "was not able to verify Footer");
						driver.close();
					} else {
						driver.close();
						driver.switchTo().window(parentWindow);
					}
					driver.switchTo().window(parentWindow);
				}
			}

			fc.utobj().clickElement(driver, pobj.finishBtn);
			if (!fc.utobj().isSelected(driver, pobj.hostURL)) {
				fc.utobj().clickElement(driver, pobj.hostURL);
			}

			// preview at Confirmation page
			fc.utobj().printTestStep("Preview Form At Confirmation Page");
			fc.utobj().clickElement(driver, pobj.previewFormAtConfirmationPage);

			Set<String> allWindows2 = driver.getWindowHandles();
			for (String currentWindow : allWindows2) {
				if (!currentWindow.equalsIgnoreCase(parentWindow)) {
					driver.switchTo().window(currentWindow);
					String titleTextCurrent = driver.getTitle();
					if (titleTextCurrent.equalsIgnoreCase(formTitle)) {
						fc.utobj().isTextDisplayed(driver, header, "was not able to verify Header");
						fc.utobj().isTextDisplayed(driver, formTitle, "was not able to verify Form Title");
						fc.utobj().isTextDisplayed(driver, sectionName, "was not able to verify Section Name");
						fc.utobj().isTextDisplayed(driver, textLabel, "was not able to verify Label Text");
						fc.utobj().isTextDisplayed(driver, fieldLabel, "was not able to verify Section Label Text");
						fc.utobj().isTextDisplayed(driver, formTitle, "was not able to verify Footer");
						driver.close();
					} else {
						driver.close();
						driver.switchTo().window(parentWindow);
					}
					driver.switchTo().window(parentWindow);
				}
			}

			String urlText = pobj.hostCodeBox.getText().trim();
			fc.utobj().clickElement(driver, pobj.okBtn);

			// verify at Home Page
			fc.utobj().printTestStep("Verify The Create At Manage Web Form Generator Page");
			fc.utobj().sendKeys(driver, pobj.searchMyForm, formName);
			fc.utobj().clickElement(driver, pobj.searchMyFormBtn);
			fc.utobj().isTextDisplayed(driver, formName, "was not able to verify Form Name");
			if (!fc.utobj().getElementByXpath(driver,
					".//*[contains(text () ,'" + formName + "')]/ancestor::tr/td[contains(text () ,'Single Page')]")
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Form Format");
			}
			if (!driver
					.findElement(By.xpath(
							".//*[contains(text () ,'" + formName + "')]/ancestor::tr/td[contains(text () ,'lead')]"))
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Form Type");
			}

			driver.get(urlText);
			fc.utobj().isTextDisplayed(driver, header, "was not able to verify Header");
			fc.utobj().isTextDisplayed(driver, formTitle, "was not able to verify Form Title");
			fc.utobj().isTextDisplayed(driver, sectionName, "was not able to verify Section Name");
			fc.utobj().isTextDisplayed(driver, textLabel, "was not able to verify Label Text");
			fc.utobj().isTextDisplayed(driver, fieldLabel, "was not able to verify Section Label Text");
			fc.utobj().isTextDisplayed(driver, formTitle, "was not able to verify Footer");

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	public String createNewFormLeads(WebDriver driver, String formFormat, String header, String textLabel,
			String fieldLabel, String footer, String assignTo, String submissionType, String formName, String formTitle,
			String description, String formUrl, String sectionName, String userNameCor, String regionName,
			String userNameReg, String franchiseId, String userNameFranchise, String status, String contactMedium,
			String leadSource, String sourceDetails, String afterSubmission, String redirectedUrl, String tabName,
			Map<String, String> config) throws Exception {

		String testCaseId = "TC_Create_New_Form_Lead";
		String urlText = null;

		if (fc.utobj().validate(testCaseId)) {
			try {

				AdminCRMManageWebFormGeneratorPage pobj = new AdminCRMManageWebFormGeneratorPage(driver);
				fc.crm().crm_common().adminCRMManageWebFormGeneratorLnk(driver);
				fc.utobj().clickElement(driver, pobj.createNewForm);
				fc.utobj().sendKeys(driver, pobj.formName, formName);
				fc.utobj().sendKeys(driver, pobj.formDisplayTitle, formTitle);
				if (!fc.utobj().isSelected(driver, pobj.displayFormTitleCheckBox)) {
					fc.utobj().clickElement(driver, pobj.displayFormTitleCheckBox);
				}
				fc.utobj().sendKeys(driver, pobj.formDescription, description);

				if (!fc.utobj().isSelected(driver, pobj.formTypeLeads)) {
					fc.utobj().clickElement(driver, pobj.formTypeLeads);
				}
				if (formFormat.equalsIgnoreCase("Single Page")) {
					fc.utobj().selectDropDown(driver, pobj.formFormat, "Single Page");

				} else if (formFormat.equalsIgnoreCase("Multi Page")) {
					fc.utobj().selectDropDown(driver, pobj.formFormat, "Multi Page");
					fc.utobj().selectDropDown(driver, pobj.submissionType, submissionType);
				}
				fc.utobj().selectDropDown(driver, pobj.columnCount, "1");
				fc.utobj().selectDropDown(driver, pobj.filedLabelAlignment, "Left");
				// fc.utobj().selectDropDown(driver, pobj.formLanguage,
				// "English");
				fc.utobj().sendKeys(driver, pobj.iframeWidth, "100%");
				fc.utobj().sendKeys(driver, pobj.iframeHeight, "500");
				fc.utobj().sendKeys(driver, pobj.formUrl, formUrl);
				fc.utobj().clickElement(driver, pobj.saveNextBtn);

				// 1.Details Page

				if (formFormat.equalsIgnoreCase("Single Page")) {
					// Add Section
					fc.utobj().clickElement(driver, pobj.addSection);
					fc.commonMethods().switch_cboxIframe_frameId(driver);
					fc.utobj().sendKeys(driver, pobj.sectionName, sectionName);
					fc.utobj().selectDropDown(driver, pobj.noOfCols, "1");
					fc.utobj().selectDropDown(driver, pobj.fieldLabelAlignmentSection, "Left");
					fc.utobj().clickElement(driver, pobj.addBtn);
					fc.utobj().switchFrameToDefault(driver);
				} else if (formFormat.equalsIgnoreCase("Multi Page")) {
					fc.utobj().clickElement(driver, pobj.addTab);
					fc.commonMethods().switch_cboxIframe_frameId(driver);
					fc.utobj().sendKeys(driver, pobj.sectionName, tabName);
					fc.utobj().clickElement(driver, pobj.addBtn);
					fc.utobj().switchFrameToDefault(driver);
				}
				// add Header
				fc.utobj().doubleClickElement(driver, pobj.addHeader);
				fc.commonMethods().switch_cboxIframe_frameId(driver);
				fc.utobj().switchFrameById(driver, "textBlock_ifr");
				WebElement editorTextArea = fc.utobj().getElementByXpath(driver, ".//*[@id='tinymce']/p");
				Actions actions = new Actions(driver);
				actions.moveToElement(editorTextArea);
				actions.click();
				actions.sendKeys(header);
				fc.utobj().logReport("Entering Value in Text Field", editorTextArea);
				fc.utobj().logReportMsg("Entered Text", header);
				actions.build().perform();
				fc.utobj().switchFrameToDefault(driver);
				fc.commonMethods().switch_cboxIframe_frameId(driver);
				fc.utobj().clickElement(driver, pobj.saveBtn);
				fc.utobj().switchFrameToDefault(driver);

				if (formFormat.equalsIgnoreCase("Multi Page")) {
					fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
							".//*[contains(@id , 'defaultTab') and contains(@id , 'Tab') and contains(text () ,'Default Tab')]"));
				}

				// Add Text
				fc.utobj().doubleClickElement(driver, pobj.defaultSectionHeaderAddText);
				fc.commonMethods().switch_cboxIframe_frameId(driver);
				fc.utobj().sendKeys(driver, pobj.labelAddText, textLabel);
				fc.utobj().selectDropDown(driver, pobj.headerLabelAlignment, "Left");
				fc.utobj().clickElement(driver, pobj.saveBtn);
				fc.utobj().switchFrameToDefault(driver);

				// FormTool
				fc.utobj().clickElement(driver, pobj.availableFieldsTab);
				fc.utobj().clickElement(driver, pobj.formTools);

				fc.utobj().dragAndDropElement(driver, pobj.formToolText, pobj.defaultDropHere);

				// Lead Info
				fc.utobj().clickElement(driver, pobj.leadInfo);
				fc.utobj().sendKeys(driver, pobj.searchField, "First Name");

				fc.utobj().dragAndDropElement(driver, fc.utobj().getElementByXpath(driver, ".//a[.='First Name']"),
						pobj.defaultDropHere);

				fc.utobj().sendKeys(driver, pobj.searchField, "Title");

				fc.utobj().dragAndDropElement(driver, fc.utobj().getElementByXpath(driver, ".//a[.='Title']"),
						pobj.defaultDropHere);

				fc.utobj().sendKeys(driver, pobj.searchField, "Company");

				fc.utobj().dragAndDropElement(driver, fc.utobj().getElementByXpath(driver, ".//a[.='Company']"),
						pobj.defaultDropHere);

				fc.utobj().sendKeys(driver, pobj.searchField, "Address");

				fc.utobj().dragAndDropElement(driver, fc.utobj().getElementByXpath(driver, ".//a[.='Address']"),
						pobj.defaultDropHere);

				fc.utobj().sendKeys(driver, pobj.searchField, "City");

				fc.utobj().dragAndDropElement(driver, fc.utobj().getElementByXpath(driver, ".//a[.='City']"),
						pobj.defaultDropHere);

				fc.utobj().sendKeys(driver, pobj.searchField, "Country");

				fc.utobj().dragAndDropElement(driver, fc.utobj().getElementByXpath(driver, ".//a[.='Country']"),
						pobj.defaultDropHere);

				fc.utobj().sendKeys(driver, pobj.searchField, "Zip / Postal Code");

				fc.utobj().dragAndDropElement(driver,
						fc.utobj().getElementByXpath(driver, ".//a[.='Zip / Postal Code']"), pobj.defaultDropHere);

				fc.utobj().sendKeys(driver, pobj.searchField, "State / Province");

				fc.utobj().dragAndDropElement(driver,
						fc.utobj().getElementByXpath(driver, ".//a[.='State / Province']"), pobj.defaultDropHere);

				fc.utobj().sendKeys(driver, pobj.searchField, "Phone");

				fc.utobj().dragAndDropElement(driver, fc.utobj().getElementByXpath(driver, ".//a[.='Phone']"),
						pobj.defaultDropHere);

				fc.utobj().sendKeys(driver, pobj.searchField, "Phone Extension");

				fc.utobj().dragAndDropElement(driver, fc.utobj().getElementByXpath(driver, ".//a[.='Phone Extension']"),
						pobj.defaultDropHere);

				fc.utobj().sendKeys(driver, pobj.searchField, "Fax");

				fc.utobj().dragAndDropElement(driver, fc.utobj().getElementByXpath(driver, ".//a[.='Fax']"),
						pobj.defaultDropHere);

				fc.utobj().sendKeys(driver, pobj.searchField, "Mobile");

				fc.utobj().dragAndDropElement(driver, fc.utobj().getElementByXpath(driver, ".//a[.='Mobile']"),
						pobj.defaultDropHere);

				fc.utobj().sendKeys(driver, pobj.searchField, "Email");

				fc.utobj().dragAndDropElement(driver, fc.utobj().getElementByXpath(driver, ".//a[.='Email']"),
						pobj.defaultDropHere);

				fc.utobj().sendKeys(driver, pobj.searchField, "Alternate Email");

				fc.utobj().dragAndDropElement(driver, fc.utobj().getElementByXpath(driver, ".//a[.='Alternate Email']"),
						pobj.defaultDropHere);

				fc.utobj().sendKeys(driver, pobj.searchField, "Job Title");

				fc.utobj().dragAndDropElement(driver, fc.utobj().getElementByXpath(driver, ".//a[.='Job Title']"),
						pobj.defaultDropHere);

				fc.utobj().sendKeys(driver, pobj.searchField, "Suffix");

				fc.utobj().dragAndDropElement(driver, fc.utobj().getElementByXpath(driver, ".//a[.='Suffix']"),
						pobj.defaultDropHere);

				fc.utobj().sendKeys(driver, pobj.searchField, "Birthdate");

				fc.utobj().dragAndDropElement(driver, fc.utobj().getElementByXpath(driver, ".//a[.='Birthdate']"),
						pobj.defaultDropHere);

				fc.utobj().sendKeys(driver, pobj.searchField, "Anniversary Date");

				fc.utobj().dragAndDropElement(driver,
						fc.utobj().getElementByXpath(driver, ".//a[.='Anniversary Date']"), pobj.defaultDropHere);

				/*fc.utobj().sendKeys(driver, pobj.searchField, "How did you hear about us?(Lead Source)");

				fc.utobj().dragAndDropElement(driver,
						fc.utobj().getElementByXpath(driver, ".//a[.='How did you hear about us?(Lead Source)']"),
						pobj.defaultDropHere);*/

				/*fc.utobj().sendKeys(driver, pobj.searchField, "Best Time to Contact");

				fc.utobj().dragAndDropElement(driver,
						fc.utobj().getElementByXpath(driver, ".//a[.='Best Time To Contact']"), pobj.defaultDropHere);

				fc.utobj().sendKeys(driver, pobj.searchField, "Comments");

				fc.utobj().dragAndDropElement(driver, fc.utobj().getElementByXpath(driver, ".//a[.='Comments']"),
						pobj.defaultDropHere);*/

				if (formFormat.equalsIgnoreCase("Single Page")) {
					// add Text in custom section
					fc.utobj().doubleClickElement(driver, fc.utobj().getElementByXpath(driver,
							".//*[.='" + sectionName + "']/following-sibling::table//ul/li[@class='emptyList']"));
					fc.commonMethods().switch_cboxIframe_frameId(driver);
					fc.utobj().sendKeys(driver, pobj.labelAddText, fieldLabel);
					fc.utobj().selectDropDown(driver, pobj.headerLabelAlignment, "Left");
					fc.utobj().clickElement(driver, pobj.saveBtn);
					fc.utobj().switchFrameToDefault(driver);

					// add Text Drop Here Custom
					fc.utobj().clickElement(driver, pobj.formTools);
					fc.utobj().dragAndDropElement(driver, pobj.formToolText,
							fc.utobj().getElementByXpath(driver, ".//*[.='" + sectionName
									+ "']/following-sibling::table//li[@class='emptyList']//p[.='Drop here.']"));

				} else if (formFormat.equalsIgnoreCase("Multi Page")) {
					fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//li[.='" + tabName + "']"));

					// add Text in custom section
					// click over double click
					String text = fc.utobj().getElementByXpath(driver, ".//li[.='" + tabName + "']").getAttribute("id");

					String tabName1 = tabName.toLowerCase();
					text = text.replace("_" + tabName1 + "_", "");
					text = text.replace("_Tab", "");

					String text1 = driver
							.findElement(By.xpath(
									".//div[contains(@id , '" + text + "')]/ul[contains(@id , '" + text + "')]/li"))
							.getAttribute("id");
					text1 = text1.replace("defaultSection_", "");

					fc.utobj().doubleClickElement(driver, fc.utobj().getElementByXpath(driver, ".//ul[contains(@id , '"
							+ text1 + "')]/li[@class='emptyList']//p[.='Double click or drag & drop to add text.']"));

					fc.commonMethods().switch_cboxIframe_frameId(driver);
					fc.utobj().sendKeys(driver, pobj.labelAddText, fieldLabel);
					fc.utobj().selectDropDown(driver, pobj.headerLabelAlignment, "Left");
					fc.utobj().clickElement(driver, pobj.saveBtn);
					fc.utobj().switchFrameToDefault(driver);

					// add Text Drop Here Custom
					fc.utobj().clickElement(driver, pobj.formTools);

					fc.utobj().dragAndDropElement(driver, pobj.formToolText, driver
							.findElement(By.xpath(".//ul[contains(@id , '" + text + "')]/li//p[.='Drop here.']")));

				}
				// add Footer
				fc.utobj().doubleClickElement(driver, pobj.addFooterDefault);
				fc.commonMethods().switch_cboxIframe_frameId(driver);
				fc.utobj().switchFrameById(driver, "textBlock_ifr");

				WebElement editorTextArea1 = fc.utobj().getElementByXpath(driver, ".//*[@id='tinymce']/p");
				Actions actions1 = new Actions(driver);
				actions1.moveToElement(editorTextArea1);
				actions1.click();
				actions1.sendKeys(footer);
				fc.utobj().logReport("Entering Value in Text Field", editorTextArea1);
				fc.utobj().logReportMsg("Entered Text", footer);
				actions1.build().perform();
				fc.utobj().switchFrameToDefault(driver);
				fc.commonMethods().switch_cboxIframe_frameId(driver);
				fc.utobj().clickElement(driver, pobj.saveBtn);
				fc.utobj().switchFrameToDefault(driver);
				fc.utobj().clickElement(driver, pobj.saveAndNextBtnDesign);

				// settings page
				fc.utobj().selectDropDown(driver, pobj.cmLeadStatusID, status);
				fc.utobj().selectDropDown(driver, pobj.contactMediumSelect, contactMedium);
				fc.utobj().selectDropDown(driver, pobj.leadSource, leadSource);
				fc.utobj().selectDropDown(driver, pobj.leadSourceDetailsSelect, sourceDetails);
				if (assignTo.equalsIgnoreCase("default")) {
					if (!fc.utobj().isSelected(driver, pobj.assignToDefault)) {
						fc.utobj().clickElement(driver, pobj.assignToDefault);
					}
				} else if (assignTo.equalsIgnoreCase("Corporate")) {
					if (!fc.utobj().isSelected(driver, pobj.assignToCorporate)) {
						fc.utobj().clickElement(driver, pobj.assignToCorporate);
					}
					fc.utobj().selectDropDown(driver, pobj.selectCorporateUser, userNameCor);

				} else if (assignTo.equalsIgnoreCase("Regional")) {
					if (!fc.utobj().isSelected(driver, pobj.assignToRegional)) {
						fc.utobj().clickElement(driver, pobj.assignToCorporate);
					}
					fc.utobj().selectDropDown(driver, pobj.selectAreaRegion, regionName);
					fc.utobj().selectDropDown(driver, pobj.selectAreaRegion, userNameReg);

				} else if (assignTo.equalsIgnoreCase("Franchisee")) {

					if (!fc.utobj().isSelected(driver, pobj.assignToFranchise)) {
						fc.utobj().clickElement(driver, pobj.assignToFranchise);
					}
					fc.utobj().selectDropDown(driver, pobj.selectFranchiseID, franchiseId);
					fc.utobj().selectDropDown(driver, pobj.selectFranchiseUser, userNameFranchise);

				}
				if (afterSubmission.equalsIgnoreCase("message")) {
					if (!fc.utobj().isSelected(driver, pobj.afterSubmissionMsg)) {
						fc.utobj().clickElement(driver, pobj.afterSubmissionMsg);
					}
				} else if (afterSubmission.equalsIgnoreCase("url")) {
					if (!fc.utobj().isSelected(driver, pobj.afterSubmissionUrl)) {
						fc.utobj().clickElement(driver, pobj.afterSubmissionUrl);
					}
					fc.utobj().sendKeys(driver, pobj.redirectedUrl, redirectedUrl);
				}

				fc.utobj().clickElement(driver, pobj.finishBtn);

				if (!fc.utobj().isSelected(driver, pobj.hostURL)) {
					fc.utobj().clickElement(driver, pobj.hostURL);
				}

				urlText = pobj.hostCodeBox.getText().trim();
				fc.utobj().clickElement(driver, pobj.okBtn);

				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException(
					"Was not able to create new Form For Lead :: Admin > CRM > Manage Web Form Generator");

		}
		return urlText;
	}

	//VipinG
	public String createNewFormLead(WebDriver driver, String formFormat, String header, String textLabel,
			String fieldLabel, String footer, String assignTo, String submissionType, String formName, String formTitle,
			String description, String formUrl, String sectionName, String userNameCor, String regionName,
			String userNameReg, String franchiseId, String userNameFranchise, String status, String contactMedium,
			String afterSubmission, String redirectedUrl, String tabName,
			Map<String, String> config) throws Exception {
		
		String urlText = null;
		
		String testCaseId = "Add_Lead_CRM_DashBoard";
		String area="Lead_Visibility";
		Map<String, String> dataSet = fc.utobj().readTestDatawithsqllite("crm", testCaseId, area);
		

		if (fc.utobj().validate(testCaseId)) {
			try {
				AdminCRMManageWebFormGeneratorPage pobj = new AdminCRMManageWebFormGeneratorPage(driver);
				fc.crm().crm_common().adminCRMManageWebFormGeneratorLnk(driver);
				fc.utobj().clickElement(driver, pobj.createNewForm);
				fc.utobj().sendKeys(driver, pobj.formName, formName);
				fc.utobj().sendKeys(driver, pobj.formDisplayTitle, formTitle);
				if (!fc.utobj().isSelected(driver, pobj.displayFormTitleCheckBox)) {
					fc.utobj().clickElement(driver, pobj.displayFormTitleCheckBox);
				}
				fc.utobj().sendKeys(driver, pobj.formDescription, description);

				if (!fc.utobj().isSelected(driver, pobj.formTypeLeads)) {
					fc.utobj().clickElement(driver, pobj.formTypeLeads);
				}
				if (formFormat.equalsIgnoreCase("Single Page")) {
					fc.utobj().selectDropDown(driver, pobj.formFormatV, "Single Page");

				} else if (formFormat.equalsIgnoreCase("Multi Page")) {
					fc.utobj().selectDropDown(driver, pobj.formFormat, "Multi Page");
					fc.utobj().selectDropDown(driver, pobj.submissionType, submissionType);
				}
				fc.utobj().selectDropDown(driver, pobj.columnCount,dataSet.get("columnCount"));
				fc.utobj().selectDropDown(driver, pobj.filedLabelAlignment,dataSet.get("filedLabelAlignment"));
				// fc.utobj().selectDropDown(driver, pobj.formLanguage,
				// "English");
				fc.utobj().sendKeys(driver, pobj.iframeWidth,dataSet.get("iframeWidth"));
				fc.utobj().sendKeys(driver, pobj.iframeHeight,dataSet.get("iframeHeight"));
				fc.utobj().sendKeys(driver, pobj.formUrl, formUrl);
				fc.utobj().clickElement(driver, pobj.saveNextBtn);

				// 1.Details Page

				if (formFormat.equalsIgnoreCase("Single Page")) {
					// Add Section
					fc.utobj().clickElement(driver, pobj.addSection);
					fc.commonMethods().switch_cboxIframe_frameId(driver);
					fc.utobj().sendKeys(driver, pobj.sectionName, sectionName);
					fc.utobj().selectDropDown(driver, pobj.noOfCols, "1");
					fc.utobj().selectDropDown(driver, pobj.fieldLabelAlignmentSection, "Left");
					fc.utobj().clickElement(driver, pobj.addBtn);
					fc.utobj().switchFrameToDefault(driver);
				} else if (formFormat.equalsIgnoreCase("Multi Page")) {
					fc.utobj().clickElement(driver, pobj.addTab);
					fc.commonMethods().switch_cboxIframe_frameId(driver);
					fc.utobj().sendKeys(driver, pobj.sectionName, tabName);
					fc.utobj().clickElement(driver, pobj.addBtn);
					fc.utobj().switchFrameToDefault(driver);
				}
				// add Header
				fc.utobj().doubleClickElement(driver, pobj.addHeader);
				fc.commonMethods().switch_cboxIframe_frameId(driver);
				fc.utobj().switchFrameById(driver, "textBlock_ifr");
				WebElement editorTextArea = fc.utobj().getElementByXpath(driver, ".//*[@id='tinymce']/p");
				Actions actions = new Actions(driver);
				actions.moveToElement(editorTextArea);
				actions.click();
				actions.sendKeys(header);
				fc.utobj().logReport("Entering Value in Text Field", editorTextArea);
				fc.utobj().logReportMsg("Entered Text", header);
				actions.build().perform();
				fc.utobj().switchFrameToDefault(driver);
				fc.commonMethods().switch_cboxIframe_frameId(driver);
				fc.utobj().clickElement(driver, pobj.saveBtn);
				fc.utobj().switchFrameToDefault(driver);

				if (formFormat.equalsIgnoreCase("Multi Page")) {
					fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
							".//*[contains(@id , 'defaultTab') and contains(@id , 'Tab') and contains(text () ,'Default Tab')]"));
				}

				// Add Text
				fc.utobj().doubleClickElement(driver, pobj.defaultSectionHeaderAddText);
				fc.commonMethods().switch_cboxIframe_frameId(driver);
				fc.utobj().sendKeys(driver, pobj.labelAddText, textLabel);
				fc.utobj().selectDropDown(driver, pobj.headerLabelAlignment, "Left");
				fc.utobj().clickElement(driver, pobj.saveBtn);
				fc.utobj().switchFrameToDefault(driver);

				// FormTool
				fc.utobj().clickElement(driver, pobj.availableFieldsTab);
				fc.utobj().clickElement(driver, pobj.formTools);

				fc.utobj().dragAndDropElement(driver, pobj.formToolText, pobj.defaultDropHere);

				// Lead Info
				fc.utobj().clickElement(driver, pobj.leadInfo);
				fc.utobj().sendKeys(driver, pobj.searchField, dataSet.get("FirstNameadd"));

				fc.utobj().dragAndDropElement(driver, fc.utobj().getElementByXpath(driver, ".//a[.='First Name']"),
						pobj.defaultDropHere);

				fc.utobj().sendKeys(driver, pobj.searchField, dataSet.get("Titleadd"));

				fc.utobj().dragAndDropElement(driver, fc.utobj().getElementByXpath(driver, ".//a[.='Title']"),
						pobj.defaultDropHere);

				fc.utobj().sendKeys(driver, pobj.searchField, dataSet.get("Companyadd"));

				fc.utobj().dragAndDropElement(driver, fc.utobj().getElementByXpath(driver, ".//a[.='Company']"),
						pobj.defaultDropHere);

				fc.utobj().sendKeys(driver, pobj.searchField, dataSet.get("Addressadd"));

				fc.utobj().dragAndDropElement(driver, fc.utobj().getElementByXpath(driver, ".//a[.='Address']"),
						pobj.defaultDropHere);

				fc.utobj().sendKeys(driver, pobj.searchField, dataSet.get("Cityadd"));

				fc.utobj().dragAndDropElement(driver, fc.utobj().getElementByXpath(driver, ".//a[.='City']"),
						pobj.defaultDropHere);

				fc.utobj().sendKeys(driver, pobj.searchField, dataSet.get("Countryadd"));

				fc.utobj().dragAndDropElement(driver, fc.utobj().getElementByXpath(driver, ".//a[.='Country']"),
						pobj.defaultDropHere);

				fc.utobj().sendKeys(driver, pobj.searchField, dataSet.get("Zipadd"));

				fc.utobj().dragAndDropElement(driver,
						fc.utobj().getElementByXpath(driver, ".//a[.='Zip / Postal Code']"), pobj.defaultDropHere);

				fc.utobj().sendKeys(driver, pobj.searchField, dataSet.get("Stateadd"));

				fc.utobj().dragAndDropElement(driver,
						fc.utobj().getElementByXpath(driver, ".//a[.='State / Province']"), pobj.defaultDropHere);

				fc.utobj().sendKeys(driver, pobj.searchField,dataSet.get("Phoneadd"));

				fc.utobj().dragAndDropElement(driver, fc.utobj().getElementByXpath(driver, ".//a[.='Phone']"),
						pobj.defaultDropHere);

				fc.utobj().sendKeys(driver, pobj.searchField, dataSet.get("phonextadd"));

				fc.utobj().dragAndDropElement(driver, fc.utobj().getElementByXpath(driver, ".//a[.='Phone Extension']"),
						pobj.defaultDropHere);

				fc.utobj().sendKeys(driver, pobj.searchField, dataSet.get("Faxadd"));

				fc.utobj().dragAndDropElement(driver, fc.utobj().getElementByXpath(driver, ".//a[.='Fax']"),
						pobj.defaultDropHere);

				fc.utobj().sendKeys(driver, pobj.searchField, dataSet.get("Mobileadd"));

				fc.utobj().dragAndDropElement(driver, fc.utobj().getElementByXpath(driver, ".//a[.='Mobile']"),
						pobj.defaultDropHere);

				fc.utobj().sendKeys(driver, pobj.searchField, dataSet.get("Emailadd"));

				fc.utobj().dragAndDropElement(driver, fc.utobj().getElementByXpath(driver, ".//a[.='Email']"),
						pobj.defaultDropHere);

				fc.utobj().sendKeys(driver, pobj.searchField, dataSet.get("AltrnEmailadd"));

				fc.utobj().dragAndDropElement(driver, fc.utobj().getElementByXpath(driver, ".//a[.='Alternate Email']"),
						pobj.defaultDropHere);

				fc.utobj().sendKeys(driver, pobj.searchField, dataSet.get("JobTtileadd"));

				fc.utobj().dragAndDropElement(driver, fc.utobj().getElementByXpath(driver, ".//a[.='Job Title']"),
						pobj.defaultDropHere);

				fc.utobj().sendKeys(driver, pobj.searchField, dataSet.get("Suffixadd"));

				fc.utobj().dragAndDropElement(driver, fc.utobj().getElementByXpath(driver, ".//a[.='Suffix']"),
						pobj.defaultDropHere);

				fc.utobj().sendKeys(driver, pobj.searchField, dataSet.get("BirthDateadd"));

				fc.utobj().dragAndDropElement(driver, fc.utobj().getElementByXpath(driver, ".//a[.='Birthdate']"),
						pobj.defaultDropHere);

				fc.utobj().sendKeys(driver, pobj.searchField, dataSet.get("Anniversaydateadd"));

				fc.utobj().dragAndDropElement(driver,
						fc.utobj().getElementByXpath(driver, ".//a[.='Anniversary Date']"), pobj.defaultDropHere);

				if (formFormat.equalsIgnoreCase("Single Page")) {
					// add Text in custom section
					fc.utobj().doubleClickElement(driver, fc.utobj().getElementByXpath(driver,
							".//*[.='" + sectionName + "']/following-sibling::table//ul/li[@class='emptyList']"));
					fc.commonMethods().switch_cboxIframe_frameId(driver);
					fc.utobj().sendKeys(driver, pobj.labelAddText, fieldLabel);
					fc.utobj().selectDropDown(driver, pobj.headerLabelAlignment, "Left");
					fc.utobj().clickElement(driver, pobj.saveBtn);
					fc.utobj().switchFrameToDefault(driver);

					// add Text Drop Here Custom
					fc.utobj().clickElement(driver, pobj.formTools);
					fc.utobj().dragAndDropElement(driver, pobj.formToolText,
							fc.utobj().getElementByXpath(driver, ".//*[.='" + sectionName
									+ "']/following-sibling::table//li[@class='emptyList']//p[.='Drop here.']"));

				} else if (formFormat.equalsIgnoreCase("Multi Page")) {
					fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//li[.='" + tabName + "']"));

					// add Text in custom section
					// click over double click
					String text = fc.utobj().getElementByXpath(driver, ".//li[.='" + tabName + "']").getAttribute("id");

					String tabName1 = tabName.toLowerCase();
					text = text.replace("_" + tabName1 + "_", "");
					text = text.replace("_Tab", "");

					String text1 = driver
							.findElement(By.xpath(
									".//div[contains(@id , '" + text + "')]/ul[contains(@id , '" + text + "')]/li"))
							.getAttribute("id");
					text1 = text1.replace("defaultSection_", "");

					fc.utobj().doubleClickElement(driver, fc.utobj().getElementByXpath(driver, ".//ul[contains(@id , '"
							+ text1 + "')]/li[@class='emptyList']//p[.='Double click or drag & drop to add text.']"));

					fc.commonMethods().switch_cboxIframe_frameId(driver);
					fc.utobj().sendKeys(driver, pobj.labelAddText, fieldLabel);
					fc.utobj().selectDropDown(driver, pobj.headerLabelAlignment, "Left");
					fc.utobj().clickElement(driver, pobj.saveBtn);
					fc.utobj().switchFrameToDefault(driver);

					// add Text Drop Here Custom
					fc.utobj().clickElement(driver, pobj.formTools);

					fc.utobj().dragAndDropElement(driver, pobj.formToolText, driver
							.findElement(By.xpath(".//ul[contains(@id , '" + text + "')]/li//p[.='Drop here.']")));

				}
				// add Footer
				fc.utobj().doubleClickElement(driver, pobj.addFooterDefault);
				fc.commonMethods().switch_cboxIframe_frameId(driver);
				fc.utobj().switchFrameById(driver, "textBlock_ifr");

				WebElement editorTextArea1 = fc.utobj().getElementByXpath(driver, ".//*[@id='tinymce']/p");
				Actions actions1 = new Actions(driver);
				actions1.moveToElement(editorTextArea1);
				actions1.click();
				actions1.sendKeys(footer);
				fc.utobj().logReport("Entering Value in Text Field", editorTextArea1);
				fc.utobj().logReportMsg("Entered Text", footer);
				actions1.build().perform();
				fc.utobj().switchFrameToDefault(driver);
				fc.commonMethods().switch_cboxIframe_frameId(driver);
				fc.utobj().clickElement(driver, pobj.saveBtn);
				fc.utobj().switchFrameToDefault(driver);
				fc.utobj().clickElement(driver, pobj.saveAndNextBtnDesign);

				// settings page
				/*fc.utobj().selectDropDown(driver, pobj.cmLeadStatusID, status);*/
				fc.utobj().selectDropDown(driver, pobj.contactMediumSelect, contactMedium);
				/*fc.utobj().selectDropDown(driver, pobj.leadSource, leadSource);
				fc.utobj().selectDropDown(driver, pobj.leadSourceDetailsSelect, sourceDetails);*/
				if (assignTo.equalsIgnoreCase("default")) {
					if (!fc.utobj().isSelected(driver, pobj.assignToDefault)) {
						fc.utobj().clickElement(driver, pobj.assignToDefault);
					}
				} else if (assignTo.equalsIgnoreCase("Corporate")) {
					if (!fc.utobj().isSelected(driver, pobj.assignToCorporate)) {
						fc.utobj().clickElement(driver, pobj.assignToCorporate);
					}
					fc.utobj().selectDropDown(driver, pobj.selectCorporateUser, dataSet.get("corpusername"));

				} else if (assignTo.equalsIgnoreCase("Regional")) {
					if (!fc.utobj().isSelected(driver, pobj.assignToRegional)) {
						fc.utobj().clickElement(driver, pobj.assignToCorporate);
					}
					fc.utobj().selectDropDown(driver, pobj.selectAreaRegion, regionName);
					fc.utobj().selectDropDown(driver, pobj.selectAreaRegion, userNameReg);

				} else if (assignTo.equalsIgnoreCase("Franchisee")) {

					if (!fc.utobj().isSelected(driver, pobj.assignToFranchise)) {
						fc.utobj().clickElement(driver, pobj.assignToFranchise);
					}
					fc.utobj().selectDropDown(driver, pobj.selectFranchiseID, franchiseId);
					fc.utobj().selectDropDown(driver, pobj.selectFranchiseUser, userNameFranchise);

				}
				if (afterSubmission.equalsIgnoreCase("message")) {
					if (!fc.utobj().isSelected(driver, pobj.afterSubmissionMsg)) {
						fc.utobj().clickElement(driver, pobj.afterSubmissionMsg);
					}
				} else if (afterSubmission.equalsIgnoreCase("url")) {
					if (!fc.utobj().isSelected(driver, pobj.afterSubmissionUrl)) {
						fc.utobj().clickElement(driver, pobj.afterSubmissionUrl);
					}
					fc.utobj().sendKeys(driver, pobj.redirectedUrl, redirectedUrl);
				}

				fc.utobj().clickElement(driver, pobj.finishBtn);

				if (!fc.utobj().isSelected(driver, pobj.hostURL)) {
					fc.utobj().clickElement(driver, pobj.hostURL);
				}

				urlText = pobj.hostCodeBox.getText().trim();
				fc.utobj().clickElement(driver, pobj.okBtn);

				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException(
					"Was not able to create new Form For Lead :: Admin > CRM > Manage Web Form Generator");

		}
		return urlText;
	}
	@Test(groups = "crmFormGenerator1234")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Modify New Form At Admin > CRM > Manage Web Form Generator", testCaseId = "TC_47_Modify_Form")
	public void modifyForm() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			AdminCRMManageWebFormGeneratorPage pobj = new AdminCRMManageWebFormGeneratorPage(driver);

			fc.utobj().printTestStep("Navigate To Admin > Users > Manage Corporate Users");
			fc.utobj().printTestStep("Add Corporate User");
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporatePage = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			String emailId = "crmautomation@staffex.com";
			CorporateUser corpUser = new CorporateUser();
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corpUser.setEmail(emailId);
			corpUser = corporatePage.createDefaultUser(driver, corpUser);

			fc.utobj().printTestStep("Navigate To Admin > Users > Manage Regional Users");
			fc.utobj().printTestStep("Add Regional User");
			AdminUsersManageRegionalUsersAddRegionalUserPageTest regionalUserPage = new AdminUsersManageRegionalUsersAddRegionalUserPageTest();
			String userNameReg1 = fc.utobj().generateTestData(dataSet.get("userNameReg"));
			String regionName1 = fc.utobj().generateTestData(dataSet.get("regionName"));
			regionalUserPage.addRegionalUser(driver, userNameReg1, regionName1, emailId);

			fc.utobj().printTestStep("Navigate To Admin > CRM > Configure Status");
			fc.utobj().printTestStep("Add Status");
			AdminCRMConfigureStatusPageTest statusPage = new AdminCRMConfigureStatusPageTest();
			String status = fc.utobj().generateTestData(dataSet.get("status"));
			statusPage.addStatus(driver, status);

			fc.utobj().printTestStep("Navigate To Admin > CRM > Configure Medium Page");
			fc.utobj().printTestStep("Add Medium");
			AdminCRMConfigureMediumPageTest mediumPage = new AdminCRMConfigureMediumPageTest();
			String contactMedium = fc.utobj().generateTestData(dataSet.get("medium"));
			String description = fc.utobj().generateTestData(dataSet.get("description"));
			mediumPage.addMedium(driver, contactMedium, description);

			fc.utobj().printTestStep("Navigate To Admin > CRM > Source Summary Page");
			fc.utobj().printTestStep("Add Source");
			AdminCRMSourceSummaryPageTest leadSourcePage = new AdminCRMSourceSummaryPageTest();
			String leadSource = fc.utobj().generateTestData(dataSet.get("source"));
			String displayOnWebPageText = fc.utobj().generateTestData(dataSet.get("text"));
			leadSourcePage.addSource(driver, leadSource, description, displayOnWebPageText);

			fc.utobj().printTestStep("Add Source Details");
			String sourceDetails = fc.utobj().generateTestData(dataSet.get("sourceDetails"));
			leadSourcePage.addSourceDetails(driver, leadSource, sourceDetails, displayOnWebPageText, description);

			fc.utobj().printTestStep("Navigate To Admin > CRM > Manage Web Form Generator");
			fc.utobj().printTestStep("Create New Lead Form");
			String formFormat = dataSet.get("formFormat");
			String header = fc.utobj().generateTestData(dataSet.get("header"));
			String textLabel = fc.utobj().generateTestData(dataSet.get("textLabel"));
			String fieldLabel = fc.utobj().generateTestData(dataSet.get("fieldLabel"));
			String footer = fc.utobj().generateTestData(dataSet.get("footer"));
			String assignTo = dataSet.get("assignTo");
			String submissionType = dataSet.get("submissionType");
			String formName = fc.utobj().generateTestData(dataSet.get("formName"));
			String formTitle = fc.utobj().generateTestData(dataSet.get("formTitle"));
			description = fc.utobj().generateTestData(dataSet.get("description"));
			String formUrl = fc.utobj().generateTestData(dataSet.get("formUrl"));
			String sectionName = "";
			String regionName = "";
			String userNameReg = "";
			String franchiseId = "";
			String userNameFranchise = "";
			String afterSubmission = dataSet.get("afterSubmission");
			String tabName = fc.utobj().generateTestData(dataSet.get("tabName"));
			String redirectedUrl = dataSet.get("reUrl");

			leadSource = sourceDetails = displayOnWebPageText;

			createNewFormLeads(driver, formFormat, header, textLabel, fieldLabel, footer, assignTo, submissionType,
					formName, formTitle, description, formUrl, sectionName, corpUser.getuserFullName(), regionName,
					userNameReg, franchiseId, userNameFranchise, status, contactMedium, leadSource, sourceDetails,
					afterSubmission, redirectedUrl, tabName, config);

			fc.utobj().sendKeys(driver, pobj.searchMyForm, formName);
			fc.utobj().clickElement(driver, pobj.searchMyFormBtn);

			fc.utobj().printTestStep("Modify Form");
			fc.utobj().actionImgOption(driver, formName, "Modify");

			formName = fc.utobj().generateTestData(dataSet.get("formName"));
			formTitle = fc.utobj().generateTestData(dataSet.get("formTitle"));
			description = fc.utobj().generateTestData(dataSet.get("description"));

			fc.utobj().sendKeys(driver, pobj.formName, formName);
			fc.utobj().sendKeys(driver, pobj.formDisplayTitle, formTitle);
			fc.utobj().sendKeys(driver, pobj.formDescription, description);
			fc.utobj().clickElement(driver, pobj.saveNextBtn);
			fc.utobj().clickElement(driver, pobj.saveAndNextBtnDesign);

			// setting page
			if (!fc.utobj().isSelected(driver, pobj.assignToRegional)) {
				fc.utobj().clickElement(driver, pobj.assignToRegional);
			}
			fc.utobj().selectDropDown(driver, pobj.selectAreaRegion, regionName1);
			userNameReg1 = userNameReg1 + " " + userNameReg1;
			fc.utobj().selectDropDown(driver, pobj.selectRegionalUser, userNameReg1);
			fc.utobj().clickElement(driver, pobj.afterSubmissionMsg);

			fc.utobj().clickElement(driver, pobj.finishBtn);
			if (!fc.utobj().isSelected(driver, pobj.hostURL)) {
				fc.utobj().clickElement(driver, pobj.hostURL);
			}
			fc.utobj().clickElement(driver, pobj.okBtn);
			fc.utobj().sendKeys(driver, pobj.searchMyForm, formName);
			fc.utobj().clickElement(driver, pobj.searchMyFormBtn);

			fc.utobj().printTestStep("Verify MOdify Form");
			fc.utobj().isTextDisplayed(driver, formName, "was not able to verify Form Name");
			if (!fc.utobj().getElementByXpath(driver,
					".//*[contains(text () ,'" + formName + "')]/ancestor::tr/td[contains(text () ,'Multi Page')]")
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Form Format");
			}
			if (!driver
					.findElement(By.xpath(
							".//*[contains(text () ,'" + formName + "')]/ancestor::tr/td[contains(text () ,'lead')]"))
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Form Type");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	public String createNewFormContactType(WebDriver driver, String formName, String formTitle, String description,
			String formFormat, String submissionType, String formUrl, String sectionName, String tabName, String header,
			String textLabel, String fieldLabel, String footer, String contactType, String status, String contactMedium,
			String leadSource, String sourceDetails, String assignTo, String userNameCor, String regionName,
			String userNameReg, String franchiseId, String userNameFranchise, String afterSubmission,
			String redirectedUrl, Map<String, String> config) throws Exception {

		String testCaseId = "TC_Create_New_Form_Contact";
		String urlText = null;

		if (fc.utobj().validate(testCaseId)) {
			try {
				AdminCRMManageWebFormGeneratorPage pobj = new AdminCRMManageWebFormGeneratorPage(driver);
				fc.crm().crm_common().adminCRMManageWebFormGeneratorLnk(driver);
				fc.utobj().clickElement(driver, pobj.createNewForm);
				fc.utobj().sendKeys(driver, pobj.formName, formName);
				fc.utobj().sendKeys(driver, pobj.formDisplayTitle, formTitle);
				if (!fc.utobj().isSelected(driver, pobj.displayFormTitleCheckBox)) {
					fc.utobj().clickElement(driver, pobj.displayFormTitleCheckBox);
				}
				fc.utobj().sendKeys(driver, pobj.formDescription, description);

				if (!fc.utobj().isSelected(driver, pobj.formTypeContact)) {
					fc.utobj().clickElement(driver, pobj.formTypeContact);
				}
				if (formFormat.equalsIgnoreCase("Single Page")) {
					fc.utobj().selectDropDown(driver, pobj.formFormat, "Single Page");

				} else if (formFormat.equalsIgnoreCase("Multi Page")) {
					fc.utobj().selectDropDown(driver, pobj.formFormat, "Multi Page");
					fc.utobj().selectDropDown(driver, pobj.submissionType, submissionType);
				}
				fc.utobj().selectDropDown(driver, pobj.columnCount, "1");
				fc.utobj().selectDropDown(driver, pobj.filedLabelAlignment, "Left");
				// fc.utobj().selectDropDown(driver, pobj.formLanguage,
				// "English");
				fc.utobj().sendKeys(driver, pobj.iframeWidth, "100%");
				fc.utobj().sendKeys(driver, pobj.iframeHeight, "500");
				fc.utobj().sendKeys(driver, pobj.formUrl, formUrl);
				fc.utobj().clickElement(driver, pobj.saveNextBtn);

				// 1.Details Page

				if (formFormat.equalsIgnoreCase("Single Page")) {
					// Add Section
					fc.utobj().clickElement(driver, pobj.addSection);
					fc.commonMethods().switch_cboxIframe_frameId(driver);
					fc.utobj().sendKeys(driver, pobj.sectionName, sectionName);
					fc.utobj().selectDropDown(driver, pobj.noOfCols, "1");
					fc.utobj().selectDropDown(driver, pobj.fieldLabelAlignmentSection, "Left");
					fc.utobj().clickElement(driver, pobj.addBtn);
					fc.utobj().switchFrameToDefault(driver);
				} else if (formFormat.equalsIgnoreCase("Multi Page")) {
					fc.utobj().clickElement(driver, pobj.addTab);
					fc.commonMethods().switch_cboxIframe_frameId(driver);
					fc.utobj().sendKeys(driver, pobj.sectionName, tabName);
					fc.utobj().clickElement(driver, pobj.addBtn);
					fc.utobj().switchFrameToDefault(driver);
				}
				// add Header
				fc.utobj().doubleClickElement(driver, pobj.addHeader);
				fc.commonMethods().switch_cboxIframe_frameId(driver);
				fc.utobj().switchFrameById(driver, "textBlock_ifr");
				WebElement editorTextArea = fc.utobj().getElementByXpath(driver, ".//*[@id='tinymce']/p");
				Actions actions = new Actions(driver);
				actions.moveToElement(editorTextArea);
				actions.click();
				actions.sendKeys(header);
				fc.utobj().logReport("Entering Value in Text Field", editorTextArea);
				fc.utobj().logReportMsg("Entered Text", header);
				actions.build().perform();
				fc.utobj().switchFrameToDefault(driver);
				fc.commonMethods().switch_cboxIframe_frameId(driver);
				fc.utobj().clickElement(driver, pobj.saveBtn);
				fc.utobj().switchFrameToDefault(driver);

				if (formFormat.equalsIgnoreCase("Multi Page")) {
					fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
							".//*[contains(@id , 'defaultTab') and contains(@id , 'Tab') and contains(text () ,'Default Tab')]"));
				}
				// Add Text
				fc.utobj().doubleClickElement(driver, pobj.defaultSectionHeaderAddText);
				fc.commonMethods().switch_cboxIframe_frameId(driver);
				fc.utobj().sendKeys(driver, pobj.labelAddText, textLabel);
				fc.utobj().selectDropDown(driver, pobj.headerLabelAlignment, "Left");
				fc.utobj().clickElement(driver, pobj.saveBtn);
				fc.utobj().switchFrameToDefault(driver);
				// FormTool
				fc.utobj().clickElement(driver, pobj.availableFieldsTab);
				fc.utobj().clickElement(driver, pobj.formTools);

				fc.utobj().dragAndDropElement(driver, pobj.formToolText, pobj.defaultDropHere);

				// fc.utobj().dragAndDropElement(driver, pobj.formToolCaptcha,
				// pobj.defaultDropHere);

				// fc.utobj().dragAndDropElement(driver,
				// pobj.formToolIAgreeCheckBox, pobj.defaultDropHere);

				// Contact Info
				fc.utobj().clickElement(driver, pobj.primaryInfo);
				fc.utobj().sendKeys(driver, pobj.searchField, "Title");

				fc.utobj().dragAndDropElement(driver, fc.utobj().getElementByXpath(driver, ".//a[.='Title']"),
						pobj.defaultDropHere);

				fc.utobj().sendKeys(driver, pobj.searchField, "Address");

				fc.utobj().dragAndDropElement(driver, fc.utobj().getElementByXpath(driver, ".//a[.='Address']"),
						pobj.defaultDropHere);

				fc.utobj().sendKeys(driver, pobj.searchField, "City");

				fc.utobj().dragAndDropElement(driver, fc.utobj().getElementByXpath(driver, ".//a[.='City']"),
						pobj.defaultDropHere);

				fc.utobj().sendKeys(driver, pobj.searchField, "Country");

				fc.utobj().dragAndDropElement(driver, fc.utobj().getElementByXpath(driver, ".//a[.='Country']"),
						pobj.defaultDropHere);

				fc.utobj().sendKeys(driver, pobj.searchField, "Zip / Postal Code");

				fc.utobj().dragAndDropElement(driver,
						fc.utobj().getElementByXpath(driver, ".//a[.='Zip / Postal Code']"), pobj.defaultDropHere);

				fc.utobj().sendKeys(driver, pobj.searchField, "State / Province");

				fc.utobj().dragAndDropElement(driver,
						fc.utobj().getElementByXpath(driver, ".//a[.='State / Province']"), pobj.defaultDropHere);

				fc.utobj().sendKeys(driver, pobj.searchField, "Phone");

				fc.utobj().dragAndDropElement(driver, fc.utobj().getElementByXpath(driver, ".//a[.='Phone']"),
						pobj.defaultDropHere);

				fc.utobj().sendKeys(driver, pobj.searchField, "Phone Extension");

				fc.utobj().dragAndDropElement(driver, fc.utobj().getElementByXpath(driver, ".//a[.='Phone Extension']"),
						pobj.defaultDropHere);

				fc.utobj().sendKeys(driver, pobj.searchField, "Fax");

				fc.utobj().dragAndDropElement(driver, fc.utobj().getElementByXpath(driver, ".//a[.='Fax']"),
						pobj.defaultDropHere);

				fc.utobj().sendKeys(driver, pobj.searchField, "Mobile");

				fc.utobj().dragAndDropElement(driver, fc.utobj().getElementByXpath(driver, ".//a[.='Mobile']"),
						pobj.defaultDropHere);

				fc.utobj().sendKeys(driver, pobj.searchField, "Alternate Email");

				fc.utobj().dragAndDropElement(driver, fc.utobj().getElementByXpath(driver, ".//a[.='Alternate Email']"),
						pobj.defaultDropHere);

				fc.utobj().sendKeys(driver, pobj.searchField, "Job Title");

				fc.utobj().dragAndDropElement(driver, fc.utobj().getElementByXpath(driver, ".//a[.='Job Title']"),
						pobj.defaultDropHere);

				fc.utobj().sendKeys(driver, pobj.searchField, "Suffix");

				fc.utobj().dragAndDropElement(driver, fc.utobj().getElementByXpath(driver, ".//a[.='Suffix']"),
						pobj.defaultDropHere);

				fc.utobj().sendKeys(driver, pobj.searchField, "Birthdate");

				fc.utobj().dragAndDropElement(driver, fc.utobj().getElementByXpath(driver, ".//a[.='Birthdate']"),
						pobj.defaultDropHere);

				fc.utobj().sendKeys(driver, pobj.searchField, "Anniversary Date");

				fc.utobj().dragAndDropElement(driver,
						fc.utobj().getElementByXpath(driver, ".//a[.='Anniversary Date']"), pobj.defaultDropHere);

				/*fc.utobj().sendKeys(driver, pobj.searchField, "How did you hear about us?(Contact Source)");

				fc.utobj().dragAndDropElement(driver,
						driver.findElement(
								By.xpath(".//a[contains(text () ,'How did you hear about us?(Contact Source)')]")),
						pobj.defaultDropHere);

				fc.utobj().sendKeys(driver, pobj.searchField, "Best Time to Contact");

				fc.utobj().dragAndDropElement(driver,
						fc.utobj().getElementByXpath(driver, ".//a[.='Best Time To Contact']"), pobj.defaultDropHere);*/

				fc.utobj().sendKeys(driver, pobj.searchField, "Comments");

				fc.utobj().dragAndDropElement(driver, fc.utobj().getElementByXpath(driver, ".//a[.='Comments']"),
						pobj.defaultDropHere);

				fc.utobj().sendKeys(driver, pobj.searchField, "Facebook");

				fc.utobj().dragAndDropElement(driver, fc.utobj().getElementByXpath(driver, ".//a[.='Facebook']"),
						pobj.defaultDropHere);

				fc.utobj().sendKeys(driver, pobj.searchField, "Twitter");

				fc.utobj().dragAndDropElement(driver, fc.utobj().getElementByXpath(driver, ".//a[.='Twitter']"),
						pobj.defaultDropHere);

				if (formFormat.equalsIgnoreCase("Single Page")) {
					// add Text in custom section
					fc.utobj().doubleClickElement(driver, fc.utobj().getElementByXpath(driver,
							".//*[.='" + sectionName + "']/following-sibling::table//ul/li[@class='emptyList']"));
					fc.commonMethods().switch_cboxIframe_frameId(driver);
					fc.utobj().sendKeys(driver, pobj.labelAddText, fieldLabel);
					fc.utobj().selectDropDown(driver, pobj.headerLabelAlignment, "Left");
					fc.utobj().clickElement(driver, pobj.saveBtn);
					fc.utobj().switchFrameToDefault(driver);

					// add Text Drop Here Custom
					fc.utobj().clickElement(driver, pobj.formTools);
					fc.utobj().dragAndDropElement(driver, pobj.formToolText,
							fc.utobj().getElementByXpath(driver, ".//*[.='" + sectionName
									+ "']/following-sibling::table//li[@class='emptyList']//p[.='Drop here.']"));

				} else if (formFormat.equalsIgnoreCase("Multi Page")) {
					fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//li[.='" + tabName + "']"));

					// add Text in custom section
					// click over double click
					String text = fc.utobj().getElementByXpath(driver, ".//li[.='" + tabName + "']").getAttribute("id");

					String tabName1 = tabName.toLowerCase();
					text = text.replace("_" + tabName1 + "_", "");
					text = text.replace("_Tab", "");

					String text1 = driver
							.findElement(By.xpath(
									".//div[contains(@id , '" + text + "')]/ul[contains(@id , '" + text + "')]/li"))
							.getAttribute("id");
					text1 = text1.replace("defaultSection_", "");

					fc.utobj().doubleClickElement(driver, fc.utobj().getElementByXpath(driver, ".//ul[contains(@id , '"
							+ text1 + "')]/li[@class='emptyList']//p[.='Double click or drag & drop to add text.']"));

					fc.commonMethods().switch_cboxIframe_frameId(driver);
					fc.utobj().sendKeys(driver, pobj.labelAddText, fieldLabel);
					fc.utobj().selectDropDown(driver, pobj.headerLabelAlignment, "Left");
					fc.utobj().clickElement(driver, pobj.saveBtn);
					fc.utobj().switchFrameToDefault(driver);

					// add Text Drop Here Custom
					fc.utobj().clickElement(driver, pobj.formTools);

					fc.utobj().dragAndDropElement(driver, pobj.formToolText, driver
							.findElement(By.xpath(".//ul[contains(@id , '" + text + "')]/li//p[.='Drop here.']")));

				}
				// add Footer
				fc.utobj().doubleClickElement(driver, pobj.addFooterDefault);
				fc.commonMethods().switch_cboxIframe_frameId(driver);
				fc.utobj().switchFrameById(driver, "textBlock_ifr");

				WebElement editorTextArea1 = fc.utobj().getElementByXpath(driver, ".//*[@id='tinymce']/p");
				Actions actions1 = new Actions(driver);
				actions1.moveToElement(editorTextArea1);
				actions1.click();
				actions1.sendKeys(footer);
				fc.utobj().logReport("Entering Value in Text Field", editorTextArea1);
				fc.utobj().logReportMsg("Entered Text", footer);
				actions1.build().perform();
				fc.utobj().switchFrameToDefault(driver);
				fc.commonMethods().switch_cboxIframe_frameId(driver);
				fc.utobj().clickElement(driver, pobj.saveBtn);
				fc.utobj().switchFrameToDefault(driver);
				fc.utobj().clickElement(driver, pobj.saveAndNextBtnDesign);

				// Setting
				fc.utobj().selectDropDown(driver, pobj.contactType, contactType);
				fc.utobj().selectDropDown(driver, pobj.cmLeadStatusID, status);
				fc.utobj().selectDropDown(driver, pobj.contactMediumSelect, contactMedium);
				fc.utobj().selectDropDown(driver, pobj.leadSource, leadSource);
				fc.utobj().selectDropDown(driver, pobj.leadSourceDetailsSelect, sourceDetails);
				if (assignTo.equalsIgnoreCase("default")) {
					if (!fc.utobj().isSelected(driver, pobj.assignToDefault)) {
						fc.utobj().clickElement(driver, pobj.assignToDefault);
					}
				} else if (assignTo.equalsIgnoreCase("Corporate")) {
					if (!fc.utobj().isSelected(driver, pobj.assignToCorporate)) {
						fc.utobj().clickElement(driver, pobj.assignToCorporate);
					}
					fc.utobj().selectDropDown(driver, pobj.selectCorporateUser, userNameCor);

				} else if (assignTo.equalsIgnoreCase("Regional")) {
					if (!fc.utobj().isSelected(driver, pobj.assignToRegional)) {
						fc.utobj().clickElement(driver, pobj.assignToRegional);
					}
					fc.utobj().selectDropDown(driver, pobj.selectAreaRegion, regionName);
					fc.utobj().selectDropDown(driver, pobj.selectRegionalUser, userNameReg);

				} else if (assignTo.equalsIgnoreCase("Franchisee")) {

					if (!fc.utobj().isSelected(driver, pobj.assignToFranchise)) {
						fc.utobj().clickElement(driver, pobj.assignToFranchise);
					}
					fc.utobj().selectDropDown(driver, pobj.selectFranchiseID, franchiseId);
					fc.utobj().selectDropDown(driver, pobj.selectFranchiseUser, userNameFranchise);

				}
				if (afterSubmission.equalsIgnoreCase("message")) {
					if (!fc.utobj().isSelected(driver, pobj.afterSubmissionMsg)) {
						fc.utobj().clickElement(driver, pobj.afterSubmissionMsg);
					}
				} else if (afterSubmission.equalsIgnoreCase("url")) {
					if (!fc.utobj().isSelected(driver, pobj.afterSubmissionUrl)) {
						fc.utobj().clickElement(driver, pobj.afterSubmissionUrl);
					}
					fc.utobj().sendKeys(driver, pobj.redirectedUrl, redirectedUrl);
				}

				fc.utobj().clickElement(driver, pobj.finishBtn);

				if (!fc.utobj().isSelected(driver, pobj.hostURL)) {
					fc.utobj().clickElement(driver, pobj.hostURL);
				}

				urlText = pobj.hostCodeBox.getText().trim();
				fc.utobj().clickElement(driver, pobj.okBtn);

				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException(
					"was not able to Create Form For Contact Type :: Admin > CRM > Manage Web Form Generator");

		}
		return urlText;
	}

	@Test(groups = "crmFormGenerator")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify the Create New Form Single Page with form type Contact At Admin > CRM > Manage Web Form Generator", testCaseId = "TC_48_Create_New_Form_Single_Contact")
	public void createNewFormSinglePageContact() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			AdminCRMManageWebFormGeneratorPage pobj = new AdminCRMManageWebFormGeneratorPage(driver);

			fc.utobj().printTestStep("Navigate To Admin > Users > Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporatePage = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			String emailId = "crmautomation@staffex.com";
			CorporateUser corpUser = new CorporateUser();
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corpUser.setEmail(emailId);
			corpUser = corporatePage.createDefaultUser(driver, corpUser);

			fc.utobj().printTestStep("Navigating to Admin > CRM > ContactType Configuration");
			fc.utobj().printTestStep("Add Contact Type");
			AdminCRMContactTypeConfigurationPageTest contactTypePage = new AdminCRMContactTypeConfigurationPageTest();
			String contactType = fc.utobj().generateTestData(dataSet.get("contactType"));
			contactTypePage.addContactType(driver, contactType);

			fc.utobj().printTestStep("Navigating to Admin > CRM > Configure Status");
			fc.utobj().printTestStep("Add Contact Type Status");
			AdminCRMConfigureStatusPageTest statusPage = new AdminCRMConfigureStatusPageTest();
			String status = fc.utobj().generateTestData(dataSet.get("status"));
			statusPage.addStatus01(driver, status);

			fc.utobj().printTestStep("Navigating to Admin > CRM > Configure Medium");
			fc.utobj().printTestStep("Add Medium");
			AdminCRMConfigureMediumPageTest mediumPage = new AdminCRMConfigureMediumPageTest();
			String contactMedium = fc.utobj().generateTestData(dataSet.get("medium"));
			String description = fc.utobj().generateTestData(dataSet.get("description"));
			mediumPage.addMedium(driver, contactMedium, description);

			fc.utobj().printTestStep("Navigating to Admin > CRM > Source Summary");
			fc.utobj().printTestStep("Add Source");
			AdminCRMSourceSummaryPageTest leadSourcePage = new AdminCRMSourceSummaryPageTest();
			String leadSource = fc.utobj().generateTestData(dataSet.get("source"));
			String displayOnWebPageText = fc.utobj().generateTestData(dataSet.get("text"));
			leadSourcePage.addSource(driver, leadSource, description, displayOnWebPageText);

			fc.utobj().printTestStep("Add Source Details");
			String sourceDetails = fc.utobj().generateTestData(dataSet.get("sourceDetails"));
			leadSourcePage.addSourceDetails(driver, leadSource, sourceDetails, displayOnWebPageText, description);

			fc.utobj().printTestStep("Navigate To Admin > CRM > Manage Web Form Generator");
			fc.utobj().printTestStep("Add New Form Contact Type");
			String formFormat = dataSet.get("formFormat");
			String header = fc.utobj().generateTestData(dataSet.get("header"));
			String textLabel = fc.utobj().generateTestData(dataSet.get("textLabel"));
			String fieldLabel = fc.utobj().generateTestData(dataSet.get("fieldLabel"));
			String footer = fc.utobj().generateTestData(dataSet.get("footer"));
			String assignTo = dataSet.get("assignTo");
			String submissionType = dataSet.get("submissionType");
			String formName = fc.utobj().generateTestData(dataSet.get("formName"));
			String formTitle = fc.utobj().generateTestData(dataSet.get("formTitle"));
			description = fc.utobj().generateTestData(dataSet.get("description"));
			String formUrl = fc.utobj().generateTestData(dataSet.get("formUrl"));
			String sectionName = fc.utobj().generateTestData(dataSet.get("sectionName"));
			String regionName = "";
			String userNameReg = "";
			String franchiseId = "";
			String userNameFranchise = "";
			String afterSubmission = dataSet.get("afterSubmission");
			String tabName = "";
			String redirectedUrl = "";
			leadSource = sourceDetails = displayOnWebPageText;

			String urlText = createNewFormContactType(driver, formName, formTitle, description, formFormat,
					submissionType, formUrl, sectionName, tabName, header, textLabel, fieldLabel, footer, contactType,
					status, contactMedium, leadSource, sourceDetails, assignTo, corpUser.getuserFullName(), regionName,
					userNameReg, franchiseId, userNameFranchise, afterSubmission, redirectedUrl, config);

			// verify at Home Page
			fc.utobj().printTestStep("Verify Contact Type Form");
			fc.utobj().sendKeys(driver, pobj.searchMyForm, formName);
			fc.utobj().clickElement(driver, pobj.searchMyFormBtn);
			fc.utobj().isTextDisplayed(driver, formName, "was not able to verify Form Name");
			if (!fc.utobj().getElementByXpath(driver,
					".//*[contains(text () ,'" + formName + "')]/ancestor::tr/td[contains(text () ,'Single Page')]")
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Form Format");
			}
			if (!fc.utobj().getElementByXpath(driver,".//*[contains(text () ,'" + formName + "')]/ancestor::tr/td[contains(text () ,'Contact')]")
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Form Type");
			}

			WebDriver driver2 = null;
			if (config.get("browserName").equalsIgnoreCase("firefox")) {
				driver2 = new FirefoxDriver();
				driver2.manage().window().maximize();
				driver2.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
				driver2.navigate().to(urlText);
			} else if (config.get("browserName").equalsIgnoreCase("chrome")) {
				System.setProperty("webdriver.chrome.driver", config.get("inputDirectory") + "\\exe\\chromedriver.exe");
				driver2 = new ChromeDriver();
				driver2.manage().window().maximize();
				driver2.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
				driver2.navigate().to(urlText);
			}
			fc.utobj().isTextDisplayed(driver2, header, "was not able to verify Header");
			fc.utobj().isTextDisplayed(driver2, formTitle, "was not able to verify Form Title");
			fc.utobj().isTextDisplayed(driver2, sectionName, "was not able to verify Section Name");
			fc.utobj().isTextDisplayed(driver2, textLabel, "was not able to verify Label Text");
			fc.utobj().isTextDisplayed(driver2, fieldLabel, "was not able to verify Section Label Text");
			fc.utobj().isTextDisplayed(driver2, formTitle, "was not able to verify Footer");
			driver2.quit();

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "crmFormGenerator")
	@TestCase(createdOn = "2017-05-0", updatedOn = "2018-06-14", testCaseDescription = "Verify The Create New Form Multi Page with form type Contact At Admin > CRM > Manage Web Form Generator", testCaseId = "TC_49_Create_New_Form_Multi_Contact")
	public void createNewFormMultiPageContact() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			AdminCRMManageWebFormGeneratorPage pobj = new AdminCRMManageWebFormGeneratorPage(driver);

			fc.utobj().printTestStep("Navigate To Admin > Users > Managae Regional User");
			fc.utobj().printTestStep("Add Regional User");
			AdminUsersManageRegionalUsersAddRegionalUserPageTest regionalUserPage = new AdminUsersManageRegionalUsersAddRegionalUserPageTest();
			String userNameReg = fc.utobj().generateTestData(dataSet.get("userNameReg"));
			String regionName = fc.utobj().generateTestData(dataSet.get("regionName"));
			String emailId = "crmautomation@staffex.com";
			regionalUserPage.addRegionalUser(driver, userNameReg, regionName, emailId);

			fc.utobj().printTestStep("Navigating to Admin > CRM > ContactType Configuration");
			fc.utobj().printTestStep("Add Contact Type");
			AdminCRMContactTypeConfigurationPageTest contactTypePage = new AdminCRMContactTypeConfigurationPageTest();
			String contactType = fc.utobj().generateTestData(dataSet.get("contactType"));
			contactTypePage.addContactType(driver, contactType);

			fc.utobj().printTestStep("Navigating to Admin > CRM > Configure Status");
			fc.utobj().printTestStep("Add Contact Type Status");
			AdminCRMConfigureStatusPageTest statusPage = new AdminCRMConfigureStatusPageTest();
			String status = fc.utobj().generateTestData(dataSet.get("status"));
			statusPage.addStatus01(driver, status);

			fc.utobj().printTestStep("Navigating to Admin > CRM > Configure Medium");
			fc.utobj().printTestStep("Add Medium");
			AdminCRMConfigureMediumPageTest mediumPage = new AdminCRMConfigureMediumPageTest();
			String contactMedium = fc.utobj().generateTestData(dataSet.get("medium"));
			String description = fc.utobj().generateTestData(dataSet.get("description"));
			mediumPage.addMedium(driver, contactMedium, description);

			fc.utobj().printTestStep("Navigating to Admin > CRM > Source Summary");
			fc.utobj().printTestStep("Add Source");
			AdminCRMSourceSummaryPageTest leadSourcePage = new AdminCRMSourceSummaryPageTest();
			String leadSource = fc.utobj().generateTestData(dataSet.get("source"));
			String displayOnWebPageText = fc.utobj().generateTestData(dataSet.get("text"));
			leadSourcePage.addSource(driver, leadSource, description, displayOnWebPageText);

			fc.utobj().printTestStep("Add Source Details");
			String sourceDetails = fc.utobj().generateTestData(dataSet.get("sourceDetails"));
			leadSourcePage.addSourceDetails(driver, leadSource, sourceDetails, displayOnWebPageText, description);

			fc.utobj().printTestStep("Navigate To Admin > CRM > Manage Web Form Generator");
			fc.utobj().printTestStep("Add New Form Contact Type");
			String formFormat = dataSet.get("formFormat");
			String header = fc.utobj().generateTestData(dataSet.get("header"));
			String textLabel = fc.utobj().generateTestData(dataSet.get("textLabel"));
			String fieldLabel = fc.utobj().generateTestData(dataSet.get("fieldLabel"));
			String footer = fc.utobj().generateTestData(dataSet.get("footer"));
			String assignTo = dataSet.get("assignTo");
			String submissionType = dataSet.get("submissionType");
			String formName = fc.utobj().generateTestData(dataSet.get("formName"));
			String formTitle = fc.utobj().generateTestData(dataSet.get("formTitle"));
			description = fc.utobj().generateTestData(dataSet.get("description"));
			String formUrl = fc.utobj().generateTestData(dataSet.get("formUrl"));
			String sectionName = "";
			String franchiseId = "";
			String userNameCor = "";
			String userNameFranchise = "";
			String afterSubmission = dataSet.get("afterSubmission");
			String tabName = fc.utobj().generateTestData(dataSet.get("tabName"));
			String redirectedUrl = dataSet.get("redirectedUrl");
			leadSource = sourceDetails = displayOnWebPageText;

			userNameReg = userNameReg + " " + userNameReg;

			String urlText = createNewFormContactType(driver, formName, formTitle, description, formFormat,
					submissionType, formUrl, sectionName, tabName, header, textLabel, fieldLabel, footer, contactType,
					status, contactMedium, leadSource, sourceDetails, assignTo, userNameCor, regionName, userNameReg,
					franchiseId, userNameFranchise, afterSubmission, redirectedUrl, config);

			// verify at Home Page
			fc.utobj().printTestStep("Verify The Form At Manage Web Form Generator Page");
			fc.utobj().sendKeys(driver, pobj.searchMyForm, formName);
			fc.utobj().clickElement(driver, pobj.searchMyFormBtn);
			fc.utobj().isTextDisplayed(driver, formName, "was not able to verify Form Name");
			if (!fc.utobj().getElementByXpath(driver,
					".//*[contains(text () ,'" + formName + "')]/ancestor::tr/td[contains(text () ,'Multi Page')]")
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Form Format");
			}
			if (!fc.utobj().getElementByXpath(driver,".//*[contains(text () ,'" + formName + "')]/ancestor::tr/td[contains(text () ,'Contact')]")
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Form Type");
			}

			WebDriver driver2 = null;
			if (config.get("browserName").equalsIgnoreCase("firefox")) {
				driver2 = new FirefoxDriver();
				driver2.manage().window().maximize();
				driver2.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
				driver2.navigate().to(urlText);
			} else if (config.get("browserName").equalsIgnoreCase("chrome")) {
				System.setProperty("webdriver.chrome.driver", config.get("inputDirectory") + "\\exe\\chromedriver.exe");
				driver2 = new ChromeDriver();
				driver2.manage().window().maximize();
				driver2.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
				driver2.navigate().to(urlText);
			}
			fc.utobj().isTextDisplayed(driver2, header, "was not able to verify Header");
			fc.utobj().isTextDisplayed(driver2, formTitle, "was not able to verify Form Title");
			fc.utobj().isTextDisplayed(driver2, sectionName, "was not able to verify Section Name");
			fc.utobj().isTextDisplayed(driver2, textLabel, "was not able to verify Label Text");
			fc.utobj().isTextDisplayed(driver2, fieldLabel, "was not able to verify Section Label Text");
			fc.utobj().isTextDisplayed(driver2, formTitle, "was not able to verify Footer");
			driver2.quit();

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "crmFormGenerator")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify the Preview Form  , Form type Lead with format single page At Admin > CRM > Manage Web Form Generator", testCaseId = "TC_50_Preview_Form")
	public void previewForm() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			AdminCRMManageWebFormGeneratorPage pobj = new AdminCRMManageWebFormGeneratorPage(driver);

			// add Franchise user
			fc.utobj().printTestStep("Navigate To Admin > Franchise Location > Add Franchise Location");
			fc.utobj().printTestStep("Add Franchise Location");
			AdminFranchiseLocationAddFranchiseLocationPageTest franchiseLocation = new AdminFranchiseLocationAddFranchiseLocationPageTest();
			String franchiseId = fc.utobj().generateTestData(dataSet.get("franchiseId"));
			String regionName = fc.utobj().generateTestData(dataSet.get("regionName"));
			String firstNameF = fc.utobj().generateTestData(dataSet.get("firstNameFid"));
			String lastNameF = fc.utobj().generateTestData(dataSet.get("lastNameFid"));
			franchiseLocation.addFranchiseLocationForMkt(driver, franchiseId, regionName, firstNameF, lastNameF);

			fc.utobj().printTestStep("Navigate To Admin > Users > Manage Franchise User");
			fc.utobj().printTestStep("Add Franchise User");
			AdminUsersManageManageFranchiseUsersPageTest franUser = new AdminUsersManageManageFranchiseUsersPageTest();
			String franUserName = fc.utobj().generateTestData(dataSet.get("franUserName"));
			String password = "t0n1ght123";
			String roleName = "Default Franchise Role";
			String emailId = "crmautomation@staffex.com";
			franUser.addFranchiseUser(driver, franUserName, password, franchiseId, roleName, emailId);

			fc.utobj().printTestStep("Navigate To Admin > CRM > Configure Status");
			fc.utobj().printTestStep("Add Status of Lead Type");
			AdminCRMConfigureStatusPageTest statusPage = new AdminCRMConfigureStatusPageTest();
			String status = fc.utobj().generateTestData(dataSet.get("status"));
			statusPage.addStatus(driver, status);

			fc.utobj().printTestStep("Navigating to Admin > CRM > Configure Medium");
			fc.utobj().printTestStep("Add Medium");
			AdminCRMConfigureMediumPageTest mediumPage = new AdminCRMConfigureMediumPageTest();
			String contactMedium = fc.utobj().generateTestData(dataSet.get("medium"));
			String description = fc.utobj().generateTestData(dataSet.get("description"));
			mediumPage.addMedium(driver, contactMedium, description);

			fc.utobj().printTestStep("Navigating to Admin > CRM > Source Summary");
			fc.utobj().printTestStep("Add Source");
			AdminCRMSourceSummaryPageTest leadSourcePage = new AdminCRMSourceSummaryPageTest();
			String leadSource = fc.utobj().generateTestData(dataSet.get("source"));
			String displayOnWebPageText = fc.utobj().generateTestData(dataSet.get("text"));
			leadSourcePage.addSource(driver, leadSource, description, displayOnWebPageText);

			fc.utobj().printTestStep("Add Source Details");
			String sourceDetails = fc.utobj().generateTestData(dataSet.get("sourceDetails"));
			leadSourcePage.addSourceDetails(driver, leadSource, sourceDetails, displayOnWebPageText, description);

			fc.utobj().printTestStep("Navigate To Admin > CRM > Manage Web Form Generator");
			fc.utobj().printTestStep("Add New Form Lead Type");

			String formFormat = dataSet.get("formFormat");
			String header = fc.utobj().generateTestData(dataSet.get("header"));
			String textLabel = fc.utobj().generateTestData(dataSet.get("textLabel"));
			String fieldLabel = fc.utobj().generateTestData(dataSet.get("fieldLabel"));
			String footer = fc.utobj().generateTestData(dataSet.get("footer"));
			String assignTo = dataSet.get("assignTo");
			String submissionType = dataSet.get("submissionType");
			String formName = fc.utobj().generateTestData(dataSet.get("formName"));
			String formTitle = fc.utobj().generateTestData(dataSet.get("formTitle"));
			description = fc.utobj().generateTestData(dataSet.get("description"));
			String formUrl = fc.utobj().generateTestData(dataSet.get("formUrl"));
			String sectionName = fc.utobj().generateTestData(dataSet.get("sectionName"));
			String userNameReg = "";
			String userNameCor = "";
			String userNameFranchise = firstNameF + " " + lastNameF;
			String afterSubmission = dataSet.get("afterSubmission");
			String tabName = "";
			String redirectedUrl = dataSet.get("reUrl");
			leadSource = sourceDetails = displayOnWebPageText;

			createNewFormLeads(driver, formFormat, header, textLabel, fieldLabel, footer, assignTo, submissionType,
					formName, formTitle, description, formUrl, sectionName, userNameCor, regionName, userNameReg,
					franchiseId, userNameFranchise, status, contactMedium, leadSource, sourceDetails, afterSubmission,
					redirectedUrl, tabName, config);
			String parentWindow = driver.getWindowHandle();
			fc.utobj().sendKeys(driver, pobj.searchMyForm, formName);
			fc.utobj().clickElement(driver, pobj.searchMyFormBtn);
			fc.utobj().actionImgOption(driver, formName, "Preview");

			fc.utobj().printTestStep("Preview The Form");

			fc.utobj().printTestStep("Verify Preview The Form");
			Set<String> allWindows2 = driver.getWindowHandles();
			for (String currentWindow : allWindows2) {
				if (!currentWindow.equalsIgnoreCase(parentWindow)) {
					driver.switchTo().window(currentWindow);
					String titleTextCurrent = driver.getTitle();
					if (titleTextCurrent.equalsIgnoreCase(formTitle)) {
						fc.utobj().isTextDisplayed(driver, header, "was not able to verify Header");
						fc.utobj().isTextDisplayed(driver, formTitle, "was not able to verify Form Title");
						fc.utobj().isTextDisplayed(driver, sectionName, "was not able to verify Section Name");
						fc.utobj().isTextDisplayed(driver, textLabel, "was not able to verify Label Text");
						fc.utobj().isTextDisplayed(driver, fieldLabel, "was not able to verify Section Label Text");
						fc.utobj().isTextDisplayed(driver, formTitle, "was not able to verify Footer");
						driver.close();
					} else {
						driver.close();
						driver.switchTo().window(parentWindow);
					}
					driver.switchTo().window(parentWindow);
				}
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = {"crmFormGenerator","crmviping"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify the Launch and Test Form , Form Type Lead with format Multi Page At Admin > CRM > Manage Web Form Generator", testCaseId = "TC_51_Launch_Test_Form")
	public void launchTestForm() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			AdminCRMManageWebFormGeneratorPage pobj = new AdminCRMManageWebFormGeneratorPage(driver);

			fc.utobj().printTestStep("Navigate To Admin > Users > Manage Corporate Users");
			fc.utobj().printTestStep("Add Corporate Users");
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporatePage = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			String emailId = "crmautomation@staffex.com";
			CorporateUser corpUser = new CorporateUser();
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corpUser.setEmail(emailId);
			corpUser = corporatePage.createDefaultUser(driver, corpUser);

			fc.utobj().printTestStep("Navigate To Admin > CRM > Configure Status");
			fc.utobj().printTestStep("Add Status of Lead Type");
			AdminCRMConfigureStatusPageTest statusPage = new AdminCRMConfigureStatusPageTest();
			String status = fc.utobj().generateTestData(dataSet.get("status"));
			statusPage.addStatus(driver, status);

			fc.utobj().printTestStep("Navigate To Admin > CRM > Configure Medium");
			fc.utobj().printTestStep("Add Medium");
			AdminCRMConfigureMediumPageTest mediumPage = new AdminCRMConfigureMediumPageTest();
			String contactMedium = fc.utobj().generateTestData(dataSet.get("medium"));
			String description = fc.utobj().generateTestData(dataSet.get("description"));
			mediumPage.addMedium(driver, contactMedium, description);

			fc.utobj().printTestStep("Navigate To Admin > CRM > Source Summary");
			fc.utobj().printTestStep("Add Source");
			AdminCRMSourceSummaryPageTest leadSourcePage = new AdminCRMSourceSummaryPageTest();
			String leadSource = fc.utobj().generateTestData(dataSet.get("source"));
			String displayOnWebPageText = fc.utobj().generateTestData(dataSet.get("text"));
			leadSourcePage.addSource(driver, leadSource, description, displayOnWebPageText);

			fc.utobj().printTestStep("Add Source Details");
			String sourceDetails = fc.utobj().generateTestData(dataSet.get("sourceDetails"));
			leadSourcePage.addSourceDetails(driver, leadSource, sourceDetails, displayOnWebPageText, description);

			fc.utobj().printTestStep("Navigate To Admin > CRM > Manage Web Form Generator");
			fc.utobj().printTestStep("Add New Form");
			String formFormat = dataSet.get("formFormat");
			String header = fc.utobj().generateTestData(dataSet.get("header"));
			String textLabel = fc.utobj().generateTestData(dataSet.get("textLabel"));
			String fieldLabel = fc.utobj().generateTestData(dataSet.get("fieldLabel"));
			String footer = fc.utobj().generateTestData(dataSet.get("footer"));
			String assignTo = dataSet.get("assignTo");
			String submissionType = dataSet.get("submissionType");
			String formName = fc.utobj().generateTestData(dataSet.get("formName"));
			String formTitle = fc.utobj().generateTestData(dataSet.get("formTitle"));
			description = fc.utobj().generateTestData(dataSet.get("description"));
			String formUrl = fc.utobj().generateTestData(dataSet.get("formUrl"));
			String sectionName = "";
			String userNameReg = "";
			String userNameFranchise = "";
			String regionName = "";
			String afterSubmission = dataSet.get("afterSubmission");
			String tabName = fc.utobj().generateTestData(dataSet.get("tabName"));
			String franchiseId = "";
			String redirectedUrl = "";
			leadSource = sourceDetails = displayOnWebPageText;

			createNewFormLeads(driver, formFormat, header, textLabel, fieldLabel, footer, assignTo, submissionType,
					formName, formTitle, description, formUrl, sectionName, corpUser.getuserFullName(), regionName,
					userNameReg, franchiseId, userNameFranchise, status, contactMedium, leadSource, sourceDetails,
					afterSubmission, redirectedUrl, tabName, config);
			String parentWindow = driver.getWindowHandle();
			fc.utobj().sendKeys(driver, pobj.searchMyForm, formName);
			fc.utobj().clickElement(driver, pobj.searchMyFormBtn);
			fc.utobj().actionImgOption(driver, formName, "Launch & Test");
			fc.utobj().sleep(5000);
			fc.utobj().printTestStep("Launch And Test Form");
			Set<String> allWindows2 = driver.getWindowHandles();
			for (String currentWindow : allWindows2) {
				if (!currentWindow.equalsIgnoreCase(parentWindow)) {
					driver.switchTo().window(currentWindow);
					fc.utobj().sleep(3000);
					String titleTextCurrent = driver.getTitle();
					if (titleTextCurrent.equalsIgnoreCase(formTitle)) {
						// fill the form with data

						fc.utobj().isTextDisplayed(driver, header, "was not able to verify Header");
						fc.utobj().isTextDisplayed(driver, formTitle, "was not able to verify Form Title");
						fc.utobj().isTextDisplayed(driver, textLabel, "was not able to verify Text Label");
						fc.utobj().isTextDisplayed(driver, footer, "was not able to verify Footer");

						fc.utobj().sendKeys(driver, pobj.leadLastName, "LName");
						fc.utobj().sendKeys(driver, pobj.leadFirstName, "FName");
						fc.utobj().selectDropDownByPartialText(driver, pobj.title, "Dr.");
						fc.utobj().sendKeys(driver, pobj.companyName, "companyName");
						fc.utobj().sendKeys(driver, pobj.address, "address");
						fc.utobj().sendKeys(driver, pobj.city, "city");
						fc.utobj().selectDropDownByPartialText(driver, pobj.country, "USA");
						fc.utobj().sendKeys(driver, pobj.zipcode, "124578");
						fc.utobj().selectDropDownByPartialText(driver, pobj.state, "Alabama");
						fc.utobj().sendKeys(driver, pobj.phoneNumbers, "1236547896");
						fc.utobj().sendKeys(driver, pobj.extn, "12");
						fc.utobj().sendKeys(driver, pobj.faxNumbers, "1236577896");
						fc.utobj().sendKeys(driver, pobj.mobileNumbers, "9874563212");
						fc.utobj().sendKeys(driver, pobj.emailIds, "jhsdhfs@gmail.com");
						fc.utobj().sendKeys(driver, pobj.alternateEmail, "jhsdhdasdfs@gmail.com");
						/*fc.utobj().selectDropDownByPartialText(driver, pobj.leadSourceLaunch, leadSource);

						fc.utobj().selectDropDownByPartialText(driver, pobj.leadSourceDetailsLaunch, sourceDetails);*/
						fc.utobj().clickElement(driver, pobj.nextBtn);

						fc.utobj().isTextDisplayed(driver, header, "was not able to verify Header");
						fc.utobj().isTextDisplayed(driver, formTitle, "was not able to verify Form Title");
						fc.utobj().isTextDisplayed(driver, tabName, "was not able to verify Tab Name");
						fc.utobj().isTextDisplayed(driver, fieldLabel, "was not able to verify Fiedl Label");
						fc.utobj().isTextDisplayed(driver, footer, "was not able to verify Footer");
						fc.utobj().clickElement(driver, pobj.submitBtn);
						fc.utobj().isTextDisplayed(driver,
								"Thank you for submitting your information, we will get back to you shortly.",
								"was not able to verify confirmation Msg");

						driver.close();
					} else {
						driver.close();
						driver.switchTo().window(parentWindow);
					}
					driver.switchTo().window(parentWindow);
				}
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	//VipinG
	public void launchTestFormlead(WebDriver driver) throws Exception {

		String testCaseId = "Add_Lead_CRM_DashBoard";
		String area="Lead_Visibility";
		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestDatawithsqllite("crm", testCaseId, area);
		
		CRMLeadsPage crmleadpage=new CRMLeadsPage(driver);

		try {
			
			AdminCRMManageWebFormGeneratorPage pobj = new AdminCRMManageWebFormGeneratorPage(driver);

			fc.utobj().printTestStep("Navigate To Admin > CRM > Manage Web Form Generator");
			fc.utobj().printTestStep("Add New Form");
			String formFormat = dataSet.get("formFormat");
			String header = fc.utobj().generateTestData(dataSet.get("header"));
			String textLabel = fc.utobj().generateTestData(dataSet.get("textLabel"));
			String fieldLabel = fc.utobj().generateTestData(dataSet.get("fieldLabel"));
			String footer = fc.utobj().generateTestData(dataSet.get("footer"));
			String assignTo = dataSet.get("assignTo");
			String submissionType = dataSet.get("submissionType");
			String formName = fc.utobj().generateTestData(dataSet.get("formName"));
			String formTitle = fc.utobj().generateTestData(dataSet.get("formTitle"));
			String description = fc.utobj().generateTestData(dataSet.get("description"));
			String formUrl = fc.utobj().generateTestData(dataSet.get("formUrl"));
			String sectionName = "";
			String userNameReg = "";
			String userNameFranchise = "";
			String regionName = "";
			String afterSubmission = dataSet.get("afterSubmission");
			String tabName = fc.utobj().generateTestData(dataSet.get("tabName"));
			String franchiseId = "";
			String redirectedUrl = "";
			String corpusername=dataSet.get("corpusername");
			String status=dataSet.get("status");
			String contactMedium=dataSet.get("contactMedium");
			
			createNewFormLead(driver, formFormat, header, textLabel, fieldLabel, footer, assignTo, submissionType,
					formName, formTitle, description, formUrl, sectionName, corpusername, regionName,
					userNameReg, franchiseId, userNameFranchise, status, contactMedium,
					afterSubmission, redirectedUrl, tabName, config);
			String parentWindow = driver.getWindowHandle();
			fc.utobj().sendKeys(driver, pobj.searchMyForm, formName);
			fc.utobj().clickElement(driver, pobj.searchMyFormBtn);
			fc.utobj().actionImgOption(driver, formName, dataSet.get("LaunchandTest"));
			fc.utobj().sleep(5000);
			fc.utobj().printTestStep("Launch And Test Form");
			Set<String> allWindows2 = driver.getWindowHandles();
			for (String currentWindow : allWindows2) {
				if (!currentWindow.equalsIgnoreCase(parentWindow)) {
					driver.switchTo().window(currentWindow);
					fc.utobj().sleep(3000);
					String titleTextCurrent = driver.getTitle();
					if (titleTextCurrent.equalsIgnoreCase(formTitle)) {
						// fill the form with data

						fc.utobj().isTextDisplayed(driver, header, "was not able to verify Header");
						fc.utobj().isTextDisplayed(driver, formTitle, "was not able to verify Form Title");
						fc.utobj().isTextDisplayed(driver, textLabel, "was not able to verify Text Label");
						fc.utobj().isTextDisplayed(driver, footer, "was not able to verify Footer");
						String firstName=fc.utobj().generateTestData(dataSet.get("firstName"));
						String lastName=fc.utobj().generateTestData(dataSet.get("lastName"));
						String fullName=firstName+" "+lastName;
						fc.utobj().sendKeys(driver, pobj.leadFirstName,firstName);
						fc.utobj().sendKeys(driver, pobj.leadLastName,lastName);
						fc.utobj().selectDropDownByPartialText(driver, pobj.title,dataSet.get("title"));
						fc.utobj().sendKeys(driver, pobj.companyName, dataSet.get("company"));
						fc.utobj().sendKeys(driver, pobj.address, dataSet.get("address"));
						fc.utobj().sendKeys(driver, pobj.city, dataSet.get("city"));
						fc.utobj().selectDropDownByPartialText(driver, pobj.country,dataSet.get("country"));
						fc.utobj().sendKeys(driver, pobj.zipcode,dataSet.get("zipcode"));
						fc.utobj().selectDropDownByPartialText(driver, pobj.state,dataSet.get("state"));
						fc.utobj().sendKeys(driver, pobj.phoneNumbers,dataSet.get("phoneNumbers"));
						fc.utobj().sendKeys(driver, pobj.extn,dataSet.get("extn"));
						fc.utobj().sendKeys(driver, pobj.faxNumbers,dataSet.get("faxNumbers"));
						fc.utobj().sendKeys(driver, pobj.mobileNumbers,dataSet.get("mobileNumbers"));
						String emailIds=fc.utobj().generateTestData(dataSet.get("emailIds"))+"@staffex.com";
						fc.utobj().sendKeys(driver, pobj.emailIds,emailIds);
						fc.utobj().sendKeys(driver, pobj.alternateEmail, dataSet.get("alternateEmail"));
						/*fc.utobj().selectDropDownByPartialText(driver, pobj.leadSourceLaunch, leadSource);

						fc.utobj().selectDropDownByPartialText(driver, pobj.leadSourceDetailsLaunch, sourceDetails);*/
						fc.utobj().clickElement(driver, pobj.nextBtn);

						fc.utobj().isTextDisplayed(driver, header, "was not able to verify Header");
						fc.utobj().isTextDisplayed(driver, formTitle, "was not able to verify Form Title");
						fc.utobj().isTextDisplayed(driver, tabName, "was not able to verify Tab Name");
						fc.utobj().isTextDisplayed(driver, fieldLabel, "was not able to verify Fiedl Label");
						fc.utobj().isTextDisplayed(driver, footer, "was not able to verify Footer");
						fc.utobj().clickElement(driver, pobj.submitBtn);
						fc.utobj().sleep(5000);
						fc.utobj().isTextDisplayed(driver,
								"Thank you for submitting your information, we will get back to you shortly.",
								"was not able to verify confirmation Msg");
						driver.close();
						driver.switchTo().window(parentWindow);
						
						fc.utobj().printTestStep("Navigate To CRM > Leads > Leads Summary");
						fc.crm().crm_common().CRMLeadsLnk(driver);
						fc.utobj().sendKeys(driver, crmleadpage.searchLeads, fullName);
						fc.utobj().clickElement(driver,"//*[@id='systemSearchBtn']");
						
						boolean verifyName = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[.='" + firstName + " " + lastName + "']");
						if (verifyName == false) {
							fc.utobj().throwsException("was not able to verify Name");
						}
						boolean ownerOfLead = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[.='" + firstName + " " + lastName
								+ "']/ancestor::tr/td[contains(text () , 'FranConnect Administrator')]");
						
						if (ownerOfLead == false) {
							fc.utobj().throwsException("was not able to verify owner of the lead");
						}
						boolean statusOfLead = fc.utobj().verifyElementOnVisible_ByXpath(driver,
								".//a[.='" + firstName + " " + lastName + "']/ancestor::tr/td[contains(text () , 'New')]");
						if (statusOfLead == false) {
							fc.utobj().throwsException("was not able to verify staus of the lead");
						}
					} else {
						driver.close();
						driver.switchTo().window(parentWindow);
					}
					
				}
			}
				
			fc.utobj().printTestStep("Lead Verifed by created with Web Form Generator");
		} catch (Exception e) {

			fc.utobj().printTestStep("Lead was not Verifed created with Web Form Generator");
		}
	}
	@Test(groups = "crmFormGenerator")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify the Copy Url , Form Type Lead with Format Single Page At Admin > CRM > Manage Web Form Generator", testCaseId = "TC_52_Copy_Url")
	public void copyUrl() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			AdminCRMManageWebFormGeneratorPage pobj = new AdminCRMManageWebFormGeneratorPage(driver);

			fc.utobj().printTestStep("Navigate To Admin > Users > Manage Corporate Users");
			fc.utobj().printTestStep("Add Corporate User");
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporatePage = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			String emailId = "crmautomation@staffex.com";
			CorporateUser corpUser = new CorporateUser();
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corpUser.setEmail(emailId);
			corpUser = corporatePage.createDefaultUser(driver, corpUser);

			fc.utobj().printTestStep("Navigate To Admin > CRM > Configure Status Page");
			fc.utobj().printTestStep("Add Status");
			AdminCRMConfigureStatusPageTest statusPage = new AdminCRMConfigureStatusPageTest();
			String status = fc.utobj().generateTestData(dataSet.get("status"));
			statusPage.addStatus(driver, status);

			fc.utobj().printTestStep("Navigate To Admin > CRM > Configure Medium");
			fc.utobj().printTestStep("Add Medium");
			AdminCRMConfigureMediumPageTest mediumPage = new AdminCRMConfigureMediumPageTest();
			String contactMedium = fc.utobj().generateTestData(dataSet.get("medium"));
			String description = fc.utobj().generateTestData(dataSet.get("description"));
			mediumPage.addMedium(driver, contactMedium, description);

			fc.utobj().printTestStep("Navigate To Admin > CRM > Source Summary");
			fc.utobj().printTestStep("Add Source");
			AdminCRMSourceSummaryPageTest leadSourcePage = new AdminCRMSourceSummaryPageTest();
			String leadSource = fc.utobj().generateTestData(dataSet.get("source"));
			String displayOnWebPageText = fc.utobj().generateTestData(dataSet.get("text"));
			leadSourcePage.addSource(driver, leadSource, description, displayOnWebPageText);

			fc.utobj().printTestStep("Add Source Details");
			String sourceDetails = fc.utobj().generateTestData(dataSet.get("sourceDetails"));
			leadSourcePage.addSourceDetails(driver, leadSource, sourceDetails, displayOnWebPageText, description);

			fc.utobj().printTestStep("Navigate To Admin > CRM > Manage Web Form Generator");
			fc.utobj().printTestStep("Add New Form");

			String formFormat = dataSet.get("formFormat");
			String header = fc.utobj().generateTestData(dataSet.get("header"));
			String textLabel = fc.utobj().generateTestData(dataSet.get("textLabel"));
			String fieldLabel = fc.utobj().generateTestData(dataSet.get("fieldLabel"));
			String footer = fc.utobj().generateTestData(dataSet.get("footer"));
			String assignTo = dataSet.get("assignTo");
			String submissionType = "";
			String formName = fc.utobj().generateTestData(dataSet.get("formName"));
			String formTitle = fc.utobj().generateTestData(dataSet.get("formTitle"));
			description = fc.utobj().generateTestData(dataSet.get("description"));
			String formUrl = fc.utobj().generateTestData(dataSet.get("formUrl"));
			String sectionName = fc.utobj().generateTestData(dataSet.get("sectionName"));
			String userNameReg = "";
			String userNameFranchise = "";
			String regionName = "";
			String afterSubmission = dataSet.get("afterSubmission");
			String tabName = "";
			String franchiseId = "";
			String redirectedUrl = "";
			leadSource = sourceDetails = displayOnWebPageText;

			String urlText = createNewFormLeads(driver, formFormat, header, textLabel, fieldLabel, footer, assignTo,
					submissionType, formName, formTitle, description, formUrl, sectionName, corpUser.getuserFullName(),
					regionName, userNameReg, franchiseId, userNameFranchise, status, contactMedium, leadSource,
					sourceDetails, afterSubmission, redirectedUrl, tabName, config);
			fc.utobj().sendKeys(driver, pobj.searchMyForm, formName);
			fc.utobj().clickElement(driver, pobj.searchMyFormBtn);
			fc.utobj().actionImgOption(driver, formName, "Copy URL");

			fc.utobj().printTestStep("Copy Url");

			fc.commonMethods().switch_cboxIframe_frameId(driver);
			if (!fc.utobj().isSelected(driver, pobj.hostURL)) {
				fc.utobj().clickElement(driver, pobj.hostURL);
			}
			String targetUrl = fc.utobj().getElementByXpath(driver, ".//*[@id='hostCodeBox']/a").getText().trim();

			fc.utobj().printTestStep("Verify Copy Url");

			if (!urlText.trim().equalsIgnoreCase(targetUrl)) {
				fc.utobj().throwsException("was not able to verify URL");
			}

			fc.utobj().switchFrameToDefault(driver);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "crmFormGenerator")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify the Deactivate Form , Form Type Contact with Format Single Page At Admin > CRM > Manage Web Form Generator", testCaseId = "TC_53_Deactivate_Form")
	public void deactiveForm() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			AdminCRMManageWebFormGeneratorPage pobj = new AdminCRMManageWebFormGeneratorPage(driver);

			fc.utobj().printTestStep("Navigate To Admin > Users > Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");

			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporatePage = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			String emailId = "crmautomation@staffex.com";
			CorporateUser corpUser = new CorporateUser();
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corpUser.setEmail(emailId);
			corpUser = corporatePage.createDefaultUser(driver, corpUser);

			fc.utobj().printTestStep("Navigating to Admin > CRM > ContactType Configuration");
			fc.utobj().printTestStep("Add Contact Type");
			AdminCRMContactTypeConfigurationPageTest contactTypePage = new AdminCRMContactTypeConfigurationPageTest();
			String contactType = fc.utobj().generateTestData(dataSet.get("contactType"));
			contactTypePage.addContactType(driver, contactType);

			fc.utobj().printTestStep("Navigating to Admin > CRM > Configure Status Page");
			fc.utobj().printTestStep("Add Status Contact Type");
			AdminCRMConfigureStatusPageTest statusPage = new AdminCRMConfigureStatusPageTest();
			String status = fc.utobj().generateTestData(dataSet.get("status"));
			statusPage.addStatus01(driver, status);

			fc.utobj().printTestStep("Navigating to Admin > CRM > Configure Medium");
			fc.utobj().printTestStep("Add Medium");
			AdminCRMConfigureMediumPageTest mediumPage = new AdminCRMConfigureMediumPageTest();
			String contactMedium = fc.utobj().generateTestData(dataSet.get("medium"));
			String description = fc.utobj().generateTestData(dataSet.get("description"));
			mediumPage.addMedium(driver, contactMedium, description);

			fc.utobj().printTestStep("Navigating to Admin > CRM > Source Summary");
			fc.utobj().printTestStep("Add SOurce");
			AdminCRMSourceSummaryPageTest leadSourcePage = new AdminCRMSourceSummaryPageTest();
			String leadSource = fc.utobj().generateTestData(dataSet.get("source"));
			String displayOnWebPageText = fc.utobj().generateTestData(dataSet.get("text"));
			leadSourcePage.addSource(driver, leadSource, description, displayOnWebPageText);

			fc.utobj().printTestStep("Add SOurce Details");
			String sourceDetails = fc.utobj().generateTestData(dataSet.get("sourceDetails"));
			leadSourcePage.addSourceDetails(driver, leadSource, sourceDetails, displayOnWebPageText, description);

			fc.utobj().printTestStep("Navigate To Admin > CRM > Manage Web Form Generator");
			fc.utobj().printTestStep("Add New Form");

			String formFormat = dataSet.get("formFormat");
			String header = fc.utobj().generateTestData(dataSet.get("header"));
			String textLabel = fc.utobj().generateTestData(dataSet.get("textLabel"));
			String fieldLabel = fc.utobj().generateTestData(dataSet.get("fieldLabel"));
			String footer = fc.utobj().generateTestData(dataSet.get("footer"));
			String assignTo = dataSet.get("assignTo");
			String submissionType = dataSet.get("submissionType");
			String formName = fc.utobj().generateTestData(dataSet.get("formName"));
			String formTitle = fc.utobj().generateTestData(dataSet.get("formTitle"));
			description = fc.utobj().generateTestData(dataSet.get("description"));
			String formUrl = fc.utobj().generateTestData(dataSet.get("formUrl"));
			String sectionName = fc.utobj().generateTestData(dataSet.get("sectionName"));
			String regionName = "";
			String userNameReg = "";
			String franchiseId = "";
			String userNameFranchise = "";
			String afterSubmission = dataSet.get("afterSubmission");
			String tabName = "";
			String redirectedUrl = "";
			leadSource = sourceDetails = displayOnWebPageText;

			createNewFormContactType(driver, formName, formTitle, description, formFormat, submissionType, formUrl,
					sectionName, tabName, header, textLabel, fieldLabel, footer, contactType, status, contactMedium,
					leadSource, sourceDetails, assignTo, corpUser.getuserFullName(), regionName, userNameReg,
					franchiseId, userNameFranchise, afterSubmission, redirectedUrl, config);

			fc.utobj().printTestStep("DeActivate The Form");
			fc.utobj().sendKeys(driver, pobj.searchMyForm, formName);
			fc.utobj().clickElement(driver, pobj.searchMyFormBtn);
			fc.utobj().actionImgOption(driver, formName, "Deactivate");
			fc.utobj().acceptAlertBox(driver);

			fc.utobj().sendKeys(driver, pobj.searchMyForm, formName);
			fc.utobj().clickElement(driver, pobj.searchMyFormBtn);

			// verify Activate
			fc.utobj().printTestStep("Verify The DeActivate Form");
			fc.utobj().moveToElement(driver, fc.utobj().getElementByXpath(driver,(".//*[contains(text () ,'" + formName + "')]/ancestor::tr/td/div[@id='menuBar']/layer")));
			String alterText = driver
					.findElement(By.xpath(
							".//*[contains(text () ,'" + formName + "')]/ancestor::tr/td/div[@id='menuBar']/layer"))
					.getAttribute("id").trim();
			alterText = alterText.replace("Actions_dynamicmenu", "");
			alterText = alterText.replace("Bar", "");

			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//*[contains(text () ,'" + formName + "')]/ancestor::tr/td/div[@id='menuBar']/layer/a/img"));

			boolean formStatus = false;
			List<WebElement> listElements = driver
					.findElements(By.xpath(".//div[@id='Actions_dynamicmenu" + alterText + "Menu']/span"));

			for (int i = 0; i < listElements.size(); i++) {

				if (listElements.get(i).getText().trim().equalsIgnoreCase("Activate")) {
					formStatus = true;
					break;
				}
			}

			if (formStatus == false) {
				fc.utobj().throwsException("was not able to deactivate Form");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "crmFormGenerator")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Activate Form ,  Form Type Contact with Format Single Page At Admin > CRM > Manage Web Form Generator", testCaseId = "TC_54_Activate_Form")
	public void activateForm() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			AdminCRMManageWebFormGeneratorPage pobj = new AdminCRMManageWebFormGeneratorPage(driver);

			fc.utobj().printTestStep("Navigate To Admin > Users > Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");

			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporatePage = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			String emailId = "crmautomation@staffex.com";
			CorporateUser corpUser = new CorporateUser();
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corpUser.setEmail(emailId);
			corpUser = corporatePage.createDefaultUser(driver, corpUser);

			fc.utobj().printTestStep("Navigating to Admin > CRM > ContactType Configuration");
			fc.utobj().printTestStep("Add Contact Type");
			AdminCRMContactTypeConfigurationPageTest contactTypePage = new AdminCRMContactTypeConfigurationPageTest();
			String contactType = fc.utobj().generateTestData(dataSet.get("contactType"));
			contactTypePage.addContactType(driver, contactType);

			fc.utobj().printTestStep("Navigating to Admin > CRM > Configure Status Page");
			fc.utobj().printTestStep("Add Status Contact Type");
			AdminCRMConfigureStatusPageTest statusPage = new AdminCRMConfigureStatusPageTest();
			String status = fc.utobj().generateTestData(dataSet.get("status"));
			statusPage.addStatus01(driver, status);

			fc.utobj().printTestStep("Navigating to Admin > CRM > Configure Medium");
			fc.utobj().printTestStep("Add Medium");
			AdminCRMConfigureMediumPageTest mediumPage = new AdminCRMConfigureMediumPageTest();
			String contactMedium = fc.utobj().generateTestData(dataSet.get("medium"));
			String description = fc.utobj().generateTestData(dataSet.get("description"));
			mediumPage.addMedium(driver, contactMedium, description);

			fc.utobj().printTestStep("Navigating to Admin > CRM > Source Summary");
			fc.utobj().printTestStep("Add SOurce");
			AdminCRMSourceSummaryPageTest leadSourcePage = new AdminCRMSourceSummaryPageTest();
			String leadSource = fc.utobj().generateTestData(dataSet.get("source"));
			String displayOnWebPageText = fc.utobj().generateTestData(dataSet.get("text"));
			leadSourcePage.addSource(driver, leadSource, description, displayOnWebPageText);

			fc.utobj().printTestStep("Add SOurce Details");
			String sourceDetails = fc.utobj().generateTestData(dataSet.get("sourceDetails"));
			leadSourcePage.addSourceDetails(driver, leadSource, sourceDetails, displayOnWebPageText, description);

			fc.utobj().printTestStep("Navigate To Admin > CRM > Manage Web Form Generator");
			fc.utobj().printTestStep("Add New Form");

			String formFormat = dataSet.get("formFormat");
			String header = fc.utobj().generateTestData(dataSet.get("header"));
			String textLabel = fc.utobj().generateTestData(dataSet.get("textLabel"));
			String fieldLabel = fc.utobj().generateTestData(dataSet.get("fieldLabel"));
			String footer = fc.utobj().generateTestData(dataSet.get("footer"));
			String assignTo = dataSet.get("assignTo");
			String submissionType = dataSet.get("submissionType");
			String formName = fc.utobj().generateTestData(dataSet.get("formName"));
			String formTitle = fc.utobj().generateTestData(dataSet.get("formTitle"));
			description = fc.utobj().generateTestData(dataSet.get("description"));
			String formUrl = fc.utobj().generateTestData(dataSet.get("formUrl"));
			String sectionName = fc.utobj().generateTestData(dataSet.get("sectionName"));
			String regionName = "";
			String userNameReg = "";
			String franchiseId = "";
			String userNameFranchise = "";
			String afterSubmission = dataSet.get("afterSubmission");
			String tabName = "";
			String redirectedUrl = "";
			leadSource = sourceDetails = displayOnWebPageText;

			createNewFormContactType(driver, formName, formTitle, description, formFormat, submissionType, formUrl,
					sectionName, tabName, header, textLabel, fieldLabel, footer, contactType, status, contactMedium,
					leadSource, sourceDetails, assignTo, corpUser.getuserFullName(), regionName, userNameReg,
					franchiseId, userNameFranchise, afterSubmission, redirectedUrl, config);

			fc.utobj().printTestStep("DeActivate Form");
			fc.utobj().sendKeys(driver, pobj.searchMyForm, formName);
			fc.utobj().clickElement(driver, pobj.searchMyFormBtn);
			fc.utobj().actionImgOption(driver, formName, "Deactivate");
			fc.utobj().acceptAlertBox(driver);

			fc.utobj().printTestStep("Activate Form");

			fc.utobj().sendKeys(driver, pobj.searchMyForm, formName);
			fc.utobj().clickElement(driver, pobj.searchMyFormBtn);
			fc.utobj().actionImgOption(driver, formName, "Activate");
			fc.utobj().acceptAlertBox(driver);

			// verify Deactivate
			fc.utobj().printTestStep("Verify Active Form");
			fc.utobj().moveToElement(driver, fc.utobj().getElementByXpath(driver,(".//*[contains(text () ,'" + formName + "')]/ancestor::tr/td/div[@id='menuBar']/layer")));
			String alterText = driver
					.findElement(By.xpath(
							".//*[contains(text () ,'" + formName + "')]/ancestor::tr/td/div[@id='menuBar']/layer"))
					.getAttribute("id").trim();
			alterText = alterText.replace("Actions_dynamicmenu", "");
			alterText = alterText.replace("Bar", "");
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//*[contains(text () ,'" + formName + "')]/ancestor::tr/td/div[@id='menuBar']/layer/a/img"));

			boolean formStatus = false;
			List<WebElement> listElements = driver
					.findElements(By.xpath(".//div[@id='Actions_dynamicmenu" + alterText + "Menu']/span"));

			for (int i = 0; i < listElements.size(); i++) {

				if (listElements.get(i).getText().trim().equalsIgnoreCase("Deactivate")) {
					formStatus = true;
					break;
				}
			}

			if (formStatus == false) {
				fc.utobj().throwsException("was not able to Activate Form");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "crmFormGenerator")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Multi Page with form type Contact At Admin > CRM > Manage Web Form Generator", testCaseId = "TC_55_Copy_Customize")
	public void copyAndCustomize() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			AdminCRMManageWebFormGeneratorPage pobj = new AdminCRMManageWebFormGeneratorPage(driver);

			fc.utobj().printTestStep("Navigating to Admin > Users > Manage Corporate Users");
			fc.utobj().printTestStep("Add Corporate Users");
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporatePage = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			String emailId = "crmautomation@staffex.com";
			CorporateUser corpUser = new CorporateUser();
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corpUser.setEmail(emailId);
			corpUser = corporatePage.createDefaultUser(driver, corpUser);

			fc.utobj().printTestStep("Navigating to Admin > Users > Managae Regional User");
			fc.utobj().printTestStep("Add Regional User");
			AdminUsersManageRegionalUsersAddRegionalUserPageTest regionalUserPage = new AdminUsersManageRegionalUsersAddRegionalUserPageTest();
			String userNameReg = fc.utobj().generateTestData(dataSet.get("userNameReg"));
			String regionName = fc.utobj().generateTestData(dataSet.get("regionName"));
			regionalUserPage.addRegionalUser(driver, userNameReg, regionName, emailId);

			fc.utobj().printTestStep("Navigating to Admin > CRM > ContactType Configuration");
			fc.utobj().printTestStep("Add Contact Type");
			AdminCRMContactTypeConfigurationPageTest contactTypePage = new AdminCRMContactTypeConfigurationPageTest();
			String contactType = fc.utobj().generateTestData(dataSet.get("contactType"));
			contactTypePage.addContactType(driver, contactType);

			fc.utobj().printTestStep("Navigating to Admin > CRM > Configure Status Page");
			fc.utobj().printTestStep("Add Status Contact Type");
			AdminCRMConfigureStatusPageTest statusPage = new AdminCRMConfigureStatusPageTest();
			String status = fc.utobj().generateTestData(dataSet.get("status"));
			statusPage.addStatus01(driver, status);

			fc.utobj().printTestStep("Navigating to Admin > CRM > Configure Medium");
			fc.utobj().printTestStep("Add Medium");
			AdminCRMConfigureMediumPageTest mediumPage = new AdminCRMConfigureMediumPageTest();
			String contactMedium = fc.utobj().generateTestData(dataSet.get("medium"));
			String description = fc.utobj().generateTestData(dataSet.get("description"));
			mediumPage.addMedium(driver, contactMedium, description);

			fc.utobj().printTestStep("Navigating to Admin > CRM > Source Summary");
			fc.utobj().printTestStep("Add Source");
			AdminCRMSourceSummaryPageTest leadSourcePage = new AdminCRMSourceSummaryPageTest();
			String leadSource = fc.utobj().generateTestData(dataSet.get("source"));
			String displayOnWebPageText = fc.utobj().generateTestData(dataSet.get("text"));
			leadSourcePage.addSource(driver, leadSource, description, displayOnWebPageText);

			fc.utobj().printTestStep("Add Source Details");
			String sourceDetails = fc.utobj().generateTestData(dataSet.get("sourceDetails"));
			leadSourcePage.addSourceDetails(driver, leadSource, sourceDetails, displayOnWebPageText, description);

			fc.utobj().printTestStep("Navigate To Admin > CRM > Manage Web Form Generator");
			fc.utobj().printTestStep("Add New Form");

			String formFormat = dataSet.get("formFormat");
			String header = fc.utobj().generateTestData(dataSet.get("header"));
			String textLabel = fc.utobj().generateTestData(dataSet.get("textLabel"));
			String fieldLabel = fc.utobj().generateTestData(dataSet.get("fieldLabel"));
			String footer = fc.utobj().generateTestData(dataSet.get("footer"));
			String assignTo = dataSet.get("assignTo");
			String submissionType = dataSet.get("submissionType");
			String formName = fc.utobj().generateTestData(dataSet.get("formName"));
			String formTitle = fc.utobj().generateTestData(dataSet.get("formTitle"));
			description = fc.utobj().generateTestData(dataSet.get("description"));
			String formUrl = fc.utobj().generateTestData(dataSet.get("formUrl"));
			String sectionName = "";
			String franchiseId = "";
			String userNameCor = "";
			String userNameFranchise = "";
			String afterSubmission = dataSet.get("afterSubmission");
			String tabName = fc.utobj().generateTestData(dataSet.get("tabName"));
			String redirectedUrl = dataSet.get("redirectedUrl");
			leadSource = sourceDetails = displayOnWebPageText;
			userNameReg = userNameReg + " " + userNameReg;

			createNewFormContactType(driver, formName, formTitle, description, formFormat, submissionType, formUrl,
					sectionName, tabName, header, textLabel, fieldLabel, footer, contactType, status, contactMedium,
					leadSource, sourceDetails, assignTo, userNameCor, regionName, userNameReg, franchiseId,
					userNameFranchise, afterSubmission, redirectedUrl, config);

			fc.utobj().sendKeys(driver, pobj.searchMyForm, formName);
			fc.utobj().clickElement(driver, pobj.searchMyFormBtn);
			fc.utobj().actionImgOption(driver, formName, "Copy and Customize");

			fc.utobj().printTestStep("Copy And Customize Form");

			formName = fc.utobj().generateTestData(dataSet.get("formName"));
			formTitle = fc.utobj().generateTestData(dataSet.get("formTitle"));
			description = fc.utobj().generateTestData(dataSet.get("description"));
			formUrl = fc.utobj().generateTestData(dataSet.get("formUrl"));
			fc.utobj().sendKeys(driver, pobj.formName, formName);
			fc.utobj().sendKeys(driver, pobj.formDisplayTitle, formTitle);
			if (!fc.utobj().isSelected(driver, pobj.displayFormTitleCheckBox)) {
				fc.utobj().clickElement(driver, pobj.displayFormTitleCheckBox);
			}
			fc.utobj().sendKeys(driver, pobj.formDescription, description);
			fc.utobj().sendKeys(driver, pobj.formUrl, formUrl);
			fc.utobj().clickElement(driver, pobj.saveNextBtn);

			// modifyHeader
			WebElement element = fc.utobj().getElementByXpath(driver,
					".//*[@id='page_header']//span[@id='page_header_text']/../following-sibling::td//span[@title='Modify']/i");
			Actions ac = new Actions(driver);
			ac.moveToElement(element).build().perform();

			JavascriptExecutor executor = (JavascriptExecutor) driver;
			executor.executeScript("arguments[0].click();", element);

			// modify HeaderText
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().switchFrameById(driver, "textBlock_ifr");
			WebElement editorTextArea = fc.utobj().getElementByXpath(driver, ".//*[@id='tinymce']/p");
			Actions actions = new Actions(driver);
			actions.moveToElement(editorTextArea);
			actions.click();
			editorTextArea.clear();
			header = fc.utobj().generateTestData(dataSet.get("header"));
			actions.sendKeys(header);
			fc.utobj().logReport("Entering Value in Text Field", editorTextArea);
			fc.utobj().logReportMsg("Entered Text", header);
			actions.build().perform();
			fc.utobj().switchFrameToDefault(driver);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().clickElement(driver, pobj.saveBtn);
			fc.utobj().switchFrameToDefault(driver);

			// modify Footer
			WebElement element1 = fc.utobj().getElementByXpath(driver,
					".//*[@id='page_footer']//span[@id='page_footer_text']/../following-sibling::td//span[@title='Modify']/i");
			ac.moveToElement(element1).build().perform();

			executor.executeScript("arguments[0].click();", element1);

			// modify FooterText
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().switchFrameById(driver, "textBlock_ifr");
			WebElement editorTextArea1 = fc.utobj().getElementByXpath(driver, ".//*[@id='tinymce']/p");
			Actions actions1 = new Actions(driver);
			actions1.moveToElement(editorTextArea1);
			actions1.click();
			editorTextArea1.clear();
			footer = fc.utobj().generateTestData(dataSet.get("footer"));
			actions1.sendKeys(footer);
			fc.utobj().logReport("Entering Value in Text Field", editorTextArea1);
			fc.utobj().logReportMsg("Entered Text", footer);
			actions1.build().perform();
			fc.utobj().switchFrameToDefault(driver);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().clickElement(driver, pobj.saveBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().clickElement(driver, pobj.saveAndNextBtnDesign);

			// setting page
			// change corporate user
			if (!fc.utobj().isSelected(driver, pobj.assignToCorporate)) {
				fc.utobj().clickElement(driver, pobj.assignToCorporate);
			}
			fc.utobj().selectDropDown(driver, pobj.selectCorporateUser, corpUser.getuserFullName());
			fc.utobj().clickElement(driver, pobj.finishBtn);

			if (!fc.utobj().isSelected(driver, pobj.hostURL)) {
				fc.utobj().clickElement(driver, pobj.hostURL);
			}

			fc.utobj().clickElement(driver, pobj.okBtn);

			// verify At Home Page
			fc.utobj().printTestStep("Verify Form At Home Page");
			fc.utobj().sendKeys(driver, pobj.searchMyForm, formName);
			fc.utobj().clickElement(driver, pobj.searchMyFormBtn);
			fc.utobj().isTextDisplayed(driver, formName, "was not able to verify Form Name");
			if (!fc.utobj().getElementByXpath(driver,
					".//*[contains(text () ,'" + formName + "')]/ancestor::tr/td[contains(text () ,'Multi Page')]")
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Form Format");
			}
			if (!fc.utobj().getElementByXpath(driver,".//*[contains(text () ,'" + formName + "')]/ancestor::tr/td[contains(text () ,'Contact')]")
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Form Type");
			}

			String parentWindow = driver.getWindowHandle();
			// preview form
			fc.utobj().printTestStep("Verify Form As A Preview");
			fc.utobj().sendKeys(driver, pobj.searchMyForm, formName);
			fc.utobj().clickElement(driver, pobj.searchMyFormBtn);
			fc.utobj().actionImgOption(driver, formName, "Preview");

			Set<String> allWindows2 = driver.getWindowHandles();
			for (String currentWindow : allWindows2) {
				if (!currentWindow.equalsIgnoreCase(parentWindow)) {
					driver.switchTo().window(currentWindow);
					String titleTextCurrent = driver.getTitle();
					if (titleTextCurrent.equalsIgnoreCase(formTitle)) {
						fc.utobj().isTextDisplayed(driver, header, "was not able to verify Header");
						fc.utobj().isTextDisplayed(driver, formTitle, "was not able to verify Form Title");
						fc.utobj().isTextDisplayed(driver, tabName, "was not able to verify Tab Name");
						fc.utobj().isTextDisplayed(driver, textLabel, "was not able to verify Label Text");
						fc.utobj().clickElement(driver,
								fc.utobj().getElementByXpath(driver, ".//a[contains(text () , '" + tabName + "')]"));
						fc.utobj().isTextDisplayed(driver, fieldLabel, "was not able to verify Section Label Text");
						fc.utobj().isTextDisplayed(driver, footer, "was not able to verify Footer");
						driver.close();
					} else {
						driver.close();
						driver.switchTo().window(parentWindow);
					}
					driver.switchTo().window(parentWindow);
				}
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	/* Harish Dwivedi TC_279_Verify_Copy_And_Launch */

	@Test(groups = " ")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify the Copy Url , Form Type Lead with Format Single Page At Admin > CRM > Manage Web Form Generator with corporate user login", testCaseId = "TC_279_Verify_Copy_And_Launch")
	public void copyUrlWithCorporateUserLogin() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			AdminCRMManageWebFormGeneratorPage pobj = new AdminCRMManageWebFormGeneratorPage(driver);

			fc.utobj().printTestStep("Navigate To Admin > Users > Manage Corporate Users");
			fc.utobj().printTestStep("Add Corporate User");
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporatePage = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			String userName = fc.utobj().generateTestData(dataSet.get("userName"));
			String emailId = "crmautomation@staffex.com";
			CorporateUser corpUser = new CorporateUser();
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corpUser.setEmail(emailId);
			corpUser = corporatePage.createDefaultUser(driver, corpUser);

			fc.home_page().logout(driver);
			fc.loginpage().loginWithParameter(driver, userName, "T0n1ght1");

			fc.crm().crm_common().adminCRMManageWebFormGeneratorLnk(driver);
			fc.utobj().printTestStep("Navigate To Admin > CRM > Manage Web Form Generator");
			String formName = "Contact Information";

			fc.utobj().showAll(driver);

			fc.utobj().actionImgOption(driver, formName, "Copy URL");

			fc.utobj().printTestStep("Copy Url");

			fc.commonMethods().switch_cboxIframe_frameId(driver);
			if (!fc.utobj().isSelected(driver, pobj.hostURL)) {
				fc.utobj().clickElement(driver, pobj.hostURL);
			}
			String targetUrl = fc.utobj().getElementByXpath(driver, ".//*[@id='hostCodeBox']/a").getText().trim();
			fc.utobj().switchFrameToDefault(driver);

			driver.navigate().to(targetUrl);

			boolean isFirstNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//*[@id='contactFirstName']");
			if (isFirstNamePresent == false) {
				fc.utobj().throwsException("was not able to verify Contact Form");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

}
