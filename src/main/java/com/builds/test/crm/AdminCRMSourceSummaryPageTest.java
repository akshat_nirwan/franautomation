package com.builds.test.crm;

import java.util.Map;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import com.builds.test.admin.AdminFranchiseLocationAddFranchiseLocationPageTest;
import com.builds.uimaps.crm.AdminCRMSourceSummaryPage;
import com.builds.utilities.FranconnectUtil;
import com.builds.utilities.TestCase;

public class AdminCRMSourceSummaryPageTest {
	FranconnectUtil fc = new FranconnectUtil();

	@Test(groups = "crm")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify the Add Source At Admin > CRM > Source Summary", testCaseId = "TC_13_Add_Source")
	public void addSource() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			AdminCRMSourceSummaryPage pobj = new AdminCRMSourceSummaryPage(driver);

			fc.utobj().printTestStep("Navigate To Admin > CRM > Source Summary");
			fc.utobj().printTestStep("Add Source");
			fc.crm().crm_common().adminCRMSourceSummaryLnk(driver);
			fc.utobj().clickElement(driver, pobj.sourceSummaryTab);
			fc.utobj().clickElement(driver, pobj.addSource);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			String source = fc.utobj().generateTestData(dataSet.get("source"));
			fc.utobj().sendKeys(driver, pobj.source, source);
			String description = fc.utobj().generateTestData(dataSet.get("description"));
			fc.utobj().sendKeys(driver, pobj.description, description);
			if (!fc.utobj().isSelected(driver, pobj.displayOnWebPage)) {
				fc.utobj().clickElement(driver, pobj.displayOnWebPage);
			}
			String displayOnWebPageText = fc.utobj().generateTestData(dataSet.get("text"));
			fc.utobj().sendKeys(driver, pobj.displayOnWebPageTxBx, displayOnWebPageText);
			fc.utobj().clickElement(driver, pobj.addBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().webDriverWait(driver, ".//*[contains(text () ,'" + source + "')]");

			fc.utobj().printTestStep("Verify Add Source");
			WebElement element = fc.utobj().getElementByXpath(driver, ".//*[contains(text () ,'" + source + "')]");
			fc.utobj().moveToElement(driver, element);
			WebElement element1 = fc.utobj().getElementByXpath(driver, ".//*[contains(text () , '" + source
					+ "')]/ancestor::tr/td[contains(text () ,'" + description + "')]");
			if (!element.isDisplayed() && !element1.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify add Source and source description");
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	public void addSource(WebDriver driver, String source, String description, String displayOnWebPageText)
			throws Exception {

		String testCaseId = "TC_Add_Source";

		if (fc.utobj().validate(testCaseId)) {
			try {
				AdminCRMSourceSummaryPage pobj = new AdminCRMSourceSummaryPage(driver);
				fc.crm().crm_common().adminCRMSourceSummaryLnk(driver);
				fc.utobj().clickElement(driver, pobj.sourceSummaryTab);

				fc.utobj().clickElement(driver, pobj.addSource);
				fc.commonMethods().switch_cboxIframe_frameId(driver);
				fc.utobj().sendKeys(driver, pobj.source, source);
				fc.utobj().sendKeys(driver, pobj.description, description);
				if (!fc.utobj().isSelected(driver, pobj.displayOnWebPage)) {
					fc.utobj().clickElement(driver, pobj.displayOnWebPage);
				}
				fc.utobj().sendKeys(driver, pobj.displayOnWebPageTxBx, displayOnWebPageText);
				fc.utobj().clickElement(driver, pobj.addBtn);
				fc.commonMethods().Click_Close_Input_ByValue(driver);
				fc.utobj().switchFrameToDefault(driver);

				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException("was not able to add Source :: Admin > CRM > Source Summary");

		}
	}

	@Test(groups = {"crm"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify the Modify Source At Admin > CRM > Source Summary", testCaseId = "TC_14_Modify_Source")
	public void modifySource() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			AdminCRMSourceSummaryPage pobj = new AdminCRMSourceSummaryPage(driver);

			fc.utobj().printTestStep("Navigate To Admin > CRM > Source Summary");
			fc.utobj().printTestStep("Add Source");
			String description = fc.utobj().generateTestData(dataSet.get("description"));
			String source = fc.utobj().generateTestData(dataSet.get("source"));
			String displayOnWebPageText = fc.utobj().generateTestData(dataSet.get("text"));
			addSource(driver, source, description, displayOnWebPageText);

			fc.utobj().webDriverWait(driver, ".//*[contains(text () ,'" + source + "')]");

			fc.utobj().printTestStep("Modify Source");
			fc.utobj().actionImgOption(driver, source, "Modify");
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			source = fc.utobj().generateTestData(dataSet.get("source"));
			fc.utobj().sendKeys(driver, pobj.source, source);
			description = fc.utobj().generateTestData(dataSet.get("description"));
			fc.utobj().sendKeys(driver, pobj.description, description);
			if (!fc.utobj().isSelected(driver, pobj.displayOnWebPage)) {
				fc.utobj().clickElement(driver, pobj.displayOnWebPage);
			}
			displayOnWebPageText = fc.utobj().generateTestData(dataSet.get("text"));
			fc.utobj().sendKeys(driver, pobj.displayOnWebPageTxBx, displayOnWebPageText);
			fc.utobj().clickElement(driver, pobj.saveBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().webDriverWait(driver, ".//*[contains(text () ,'" + source + "')]");

			fc.utobj().printTestStep("Verify Modify Source");
			WebElement element = fc.utobj().getElementByXpath(driver, ".//*[contains(text () ,'" + source + "')]");
			fc.utobj().moveToElement(driver, element);
			WebElement element1 = fc.utobj().getElementByXpath(driver, ".//*[contains(text () , '" + source
					+ "')]/ancestor::tr/td[contains(text () ,'" + description + "')]");
			if (!element.isDisplayed() && !element1.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify modify Source and source description");
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "crm")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify the Delete Source At Admin > CRM > Source Summary", testCaseId = "TC_15_Delete_Source")
	public void deleteSource() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			fc.utobj().printTestStep("Navigate To Admin > CRM > Source Summary");
			fc.utobj().printTestStep("Add Source");
			String description = fc.utobj().generateTestData(dataSet.get("description"));
			String source = fc.utobj().generateTestData(dataSet.get("source"));
			String displayOnWebPageText = fc.utobj().generateTestData(dataSet.get("text"));
			addSource(driver, source, description, displayOnWebPageText);

			fc.utobj().printTestStep("Delete Source");
			fc.utobj().actionImgOption(driver, source, "Delete");
			fc.utobj().acceptAlertBox(driver);

			fc.utobj().printTestStep("Verify Delete Source");
			boolean isTextPresent = fc.utobj().assertPageSource(driver, source);
			if (isTextPresent == true) {
				fc.utobj().throwsException("was not able to delete source");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "crm")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify the Add Source Details At Admin > CRM > Source Summary", testCaseId = "TC_16_Add_Source_Details")
	public void addSourceDetails() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			AdminCRMSourceSummaryPage pobj = new AdminCRMSourceSummaryPage(driver);

			fc.utobj().printTestStep("Navigate To Admin > CRM > Source Summary");
			fc.utobj().printTestStep("Add Source");
			String description = fc.utobj().generateTestData(dataSet.get("description"));
			String source = fc.utobj().generateTestData(dataSet.get("source"));
			String displayOnWebPageText = fc.utobj().generateTestData(dataSet.get("text"));
			addSource(driver, source, description, displayOnWebPageText);

			fc.utobj().printTestStep("Add Source Details");
			fc.utobj().clickElement(driver, pobj.addSourceDetails);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().selectDropDown(driver, pobj.sourceSelect, source);
			String sourceDetails = fc.utobj().generateTestData(dataSet.get("sourceDetails"));
			fc.utobj().sendKeys(driver, pobj.sourceDetails, sourceDetails);

			if (!fc.utobj().isSelected(driver, pobj.includePage)) {
				fc.utobj().clickElement(driver, pobj.includePage);
			}
			displayOnWebPageText = fc.utobj().generateTestData(dataSet.get("text"));
			fc.utobj().sendKeys(driver, pobj.cmSource3NameDisplay, displayOnWebPageText);
			description = fc.utobj().generateTestData(dataSet.get("description"));
			fc.utobj().sendKeys(driver, pobj.descriptionRemarks, description);
			fc.utobj().clickElement(driver, pobj.addBtn);

			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Verify Add Source Details");
			boolean isTextPresent = fc.utobj().assertPageSource(driver, sourceDetails);
			if (isTextPresent == false) {
				fc.utobj().throwsException("was not able to add Source details");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	public void addSourceDetails(WebDriver driver, String source, String sourceDetails, String displayOnWebPageText,
			String description) throws Exception {

		String testCaseId = "TC_Add_Source_Details";

		if (fc.utobj().validate(testCaseId)) {
			try {
				AdminCRMSourceSummaryPage pobj = new AdminCRMSourceSummaryPage(driver);

				fc.utobj().webDriverWait(driver, ".//*[contains(text () ,'" + source + "')]");
				fc.utobj().clickElement(driver, pobj.addSourceDetails);
				fc.commonMethods().switch_cboxIframe_frameId(driver);
				fc.utobj().selectDropDown(driver, pobj.sourceSelect, source);
				fc.utobj().sendKeys(driver, pobj.sourceDetails, sourceDetails);
				if (!fc.utobj().isSelected(driver, pobj.includePage)) {
					fc.utobj().clickElement(driver, pobj.includePage);
				}
				fc.utobj().sendKeys(driver, pobj.cmSource3NameDisplay, displayOnWebPageText);
				fc.utobj().sendKeys(driver, pobj.descriptionRemarks, description);
				fc.utobj().clickElement(driver, pobj.addBtn);
				fc.commonMethods().Click_Close_Input_ByValue(driver);
				fc.utobj().switchFrameToDefault(driver);

				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException("was not able to add Source Details:: Admin > CRM > Source Summary");

		}
	}

	@Test(groups = "crm")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify the Modify Source Details At Admin > CRM > Source Summary", testCaseId = "TC_17_Modify_Source_Details")
	public void modifySourceDetails() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			AdminCRMSourceSummaryPage pobj = new AdminCRMSourceSummaryPage(driver);

			fc.utobj().printTestStep("Navigate To Admin > CRM > Source Summary");
			fc.utobj().printTestStep("Add Source");
			String description = fc.utobj().generateTestData(dataSet.get("description"));
			String source = fc.utobj().generateTestData(dataSet.get("source"));
			String displayOnWebPageText = fc.utobj().generateTestData(dataSet.get("text"));
			addSource(driver, source, description, displayOnWebPageText);

			fc.utobj().printTestStep("Add Source Details");
			String sourceDetails = fc.utobj().generateTestData(dataSet.get("sourceDetails"));
			addSourceDetails(driver, source, sourceDetails, displayOnWebPageText, description);

			fc.utobj().printTestStep("Modify Source Details");
			fc.utobj().actionImgOption(driver, sourceDetails, "Modify");
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			sourceDetails = fc.utobj().generateTestData(dataSet.get("sourceDetails"));
			fc.utobj().sendKeys(driver, pobj.sourceDetails, sourceDetails);

			if (!fc.utobj().isSelected(driver, pobj.includePage)) {
				fc.utobj().clickElement(driver, pobj.includePage);
			}
			displayOnWebPageText = fc.utobj().generateTestData(dataSet.get("text"));
			fc.utobj().sendKeys(driver, pobj.cmSource3NameDisplay, displayOnWebPageText);
			description = fc.utobj().generateTestData(dataSet.get("description"));
			fc.utobj().sendKeys(driver, pobj.descriptionRemarks, description);
			fc.utobj().clickElement(driver, pobj.saveBtn);

			fc.utobj().printTestStep("Verify Modify Source Details");
			boolean isTextPresent = fc.utobj().assertPageSource(driver, sourceDetails);
			if (isTextPresent == false) {
				fc.utobj().throwsException("was not able to Modify Source details");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "crm")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify the Delete Source Details At Admin > CRM > Source Summary", testCaseId = "TC_18_Delete_Source_Details")
	public void deleteSourceDetails() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			fc.utobj().printTestStep("Navigate To Admin > CRM > Source Summary");
			fc.utobj().printTestStep("Add Source");
			String description = fc.utobj().generateTestData(dataSet.get("description"));
			String source = fc.utobj().generateTestData(dataSet.get("source"));
			String displayOnWebPageText = fc.utobj().generateTestData(dataSet.get("text"));
			addSource(driver, source, description, displayOnWebPageText);

			fc.utobj().printTestStep("Add Source Details");
			String sourceDetails = fc.utobj().generateTestData(dataSet.get("sourceDetails"));
			addSourceDetails(driver, source, sourceDetails, displayOnWebPageText, description);

			fc.utobj().printTestStep("Delete Source Details");
			fc.utobj().actionImgOption(driver, sourceDetails, "Delete");
			fc.utobj().acceptAlertBox(driver);

			fc.utobj().printTestStep("Verify Delete Source Details");
			boolean isTextPresent = fc.utobj().assertPageSource(driver, sourceDetails);
			if (isTextPresent == true) {
				fc.utobj().throwsException("was not able to Delete Source details");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = {"crm"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify the Add Source Details For Franchise At Admin > CRM > Franchise(s) Source Summary", testCaseId = "TC_19_Add_Source_Details")
	public void addSourceDetailsFranchise() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			AdminCRMSourceSummaryPage pobj = new AdminCRMSourceSummaryPage(driver);

			fc.utobj().printTestStep("Navigate To Admin > Franchise Location > Add Franchise Location");
			AdminFranchiseLocationAddFranchiseLocationPageTest franchiseLocation = new AdminFranchiseLocationAddFranchiseLocationPageTest();
			String franchiseName = fc.utobj().generateTestData(dataSet.get("franchiseName"));
			String regionName = fc.utobj().generateTestData(dataSet.get("regionName"));
			String firstNameF = fc.utobj().generateTestData(dataSet.get("firstNameFid"));
			String lastNameF = fc.utobj().generateTestData(dataSet.get("lastNameFid"));
			franchiseLocation.addFranchiseLocationForMkt(driver, franchiseName, regionName, firstNameF, lastNameF);

			fc.utobj().printTestStep("Navigate To Admin > CRM > Source Summary");
			fc.utobj().printTestStep("Add Source");
			String description = fc.utobj().generateTestData(dataSet.get("description"));
			String source = fc.utobj().generateTestData(dataSet.get("source"));
			String displayOnWebPageText = fc.utobj().generateTestData(dataSet.get("text"));
			addSource(driver, source, description, displayOnWebPageText);

			fc.utobj().printTestStep("Add Source Details For Franchise");

			fc.utobj().clickElement(driver, pobj.franchiseSourceSummaryTab);
			
			fc.utobj().sleep(5000);
	
			//fc.utobj().clickElement(driver, driver.findElement(By.linkText(franchiseName)));
			//change the XPATH
			WebElement element=driver.findElement(By.xpath(".//td[@class='aBullet']//a[contains(text(),'"+franchiseName+"')]"));
			JavascriptExecutor js = (JavascriptExecutor) driver;
			js.executeScript("arguments[0].click();", element);
			
			
			//fc.utobj().clickElement(driver,".//td[@class='aBullet']//a[contains(text(),'"+franchiseName+"')]");
			
			fc.utobj().clickElement(driver, pobj.addSource_Details_onFranchisee);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().selectDropDown(driver, pobj.sourceSelect, source);
			String sourceDetails = fc.utobj().generateTestData(dataSet.get("sourceDetails"));
			fc.utobj().sendKeys(driver, pobj.sourceDetails, sourceDetails);

			if (!fc.utobj().isSelected(driver, pobj.includePage)) {
				fc.utobj().clickElement(driver, pobj.includePage);
			}
			displayOnWebPageText = fc.utobj().generateTestData(dataSet.get("text"));
			fc.utobj().sendKeys(driver, pobj.cmSource3NameDisplay, displayOnWebPageText);
			description = fc.utobj().generateTestData(dataSet.get("description"));
			fc.utobj().sendKeys(driver, pobj.descriptionRemarks, description);
			fc.utobj().clickElement(driver, pobj.addBtn);

			fc.utobj().printTestStep("Verify Add Source Details For Franchise");
			boolean isTextPresent = fc.utobj().assertPageSource(driver, sourceDetails);
			if (isTextPresent == false) {
				fc.utobj().throwsException("was not able to add Source details");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	public void addSourceDetailsFranchise(WebDriver driver, String franchiseName, String source, String sourceDetails,
			String displayOnWebPageText, String description) throws Exception {

		String testCaseId = "TC_Add_Source_Deatils_Franchise";

		if (fc.utobj().validate(testCaseId)) {
			try {
				AdminCRMSourceSummaryPage pobj = new AdminCRMSourceSummaryPage(driver);

				fc.utobj().clickElement(driver, pobj.franchiseSourceSummaryTab);
				//fc.utobj().clickElementWithoutMove(driver.findElement(By.linkText(franchiseName)));
				fc.utobj().sleep(5000);
				WebElement element=driver.findElement(By.xpath(".//td[@class='aBullet']//a[contains(text(),'"+franchiseName+"')]"));
				JavascriptExecutor js = (JavascriptExecutor) driver;
				js.executeScript("arguments[0].click();", element);
				
				fc.utobj().clickElement(driver, pobj.addSource);
				fc.commonMethods().switch_cboxIframe_frameId(driver);
				fc.utobj().selectDropDown(driver, pobj.sourceSelect, source);
				fc.utobj().sendKeys(driver, pobj.sourceDetails, sourceDetails);

				if (!fc.utobj().isSelected(driver, pobj.includePage)) {
					fc.utobj().clickElement(driver, pobj.includePage);
				}
				fc.utobj().sendKeys(driver, pobj.cmSource3NameDisplay, displayOnWebPageText);
				fc.utobj().sendKeys(driver, pobj.descriptionRemarks, description);
				fc.utobj().clickElement(driver, pobj.addBtn);
				fc.commonMethods().Click_Close_Input_ByValue(driver);
				fc.utobj().switchFrameToDefault(driver);
				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException(
					"Was not able to add Source Details for Franchise :: Admin > CRM > Franchise(s) Source Summary");
		}
	}

	@Test(groups = "crm")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify the Modify Source Details At Admin > CRM > Franchise(s) Source Summary", testCaseId = "TC_20_Modify_Source_Details")
	public void modifySourceDetailsFranchise() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			AdminCRMSourceSummaryPage pobj = new AdminCRMSourceSummaryPage(driver);

			fc.utobj().printTestStep("Navigate To Admin > Franchise Location > Add Franchise Location");
			AdminFranchiseLocationAddFranchiseLocationPageTest franchiseLocation = new AdminFranchiseLocationAddFranchiseLocationPageTest();
			String franchiseName = fc.utobj().generateTestData(dataSet.get("franchiseName"));
			String regionName = fc.utobj().generateTestData(dataSet.get("regionName"));
			String firstNameF = fc.utobj().generateTestData(dataSet.get("firstNameFid"));
			String lastNameF = fc.utobj().generateTestData(dataSet.get("lastNameFid"));
			franchiseLocation.addFranchiseLocationForMkt(driver, franchiseName, regionName, firstNameF, lastNameF);

			fc.utobj().printTestStep("Navigate To Admin > CRM > Source Summary");
			fc.utobj().printTestStep("Add Source");
			String description = fc.utobj().generateTestData(dataSet.get("description"));
			String source = fc.utobj().generateTestData(dataSet.get("source"));
			String displayOnWebPageText = fc.utobj().generateTestData(dataSet.get("text"));
			addSource(driver, source, description, displayOnWebPageText);

			fc.utobj().printTestStep("Add Source Details For Franchise");

			String sourceDetails = fc.utobj().generateTestData(dataSet.get("sourceDetails"));
			displayOnWebPageText = fc.utobj().generateTestData(dataSet.get("text"));
			description = fc.utobj().generateTestData(dataSet.get("description"));
			addSourceDetailsFranchise(driver, franchiseName, source, sourceDetails, displayOnWebPageText, description);
			fc.utobj().sleep(3000);
			/*fc.utobj().webDriverWait(driver, ".//option[contains(text () ,'" + sourceDetails + "')]");*/

			fc.utobj().printTestStep("Modify Source Details for Franchise");
			fc.utobj().actionImgOption(driver, sourceDetails, "Modify");
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			sourceDetails = fc.utobj().generateTestData(dataSet.get("sourceDetails"));
			fc.utobj().sendKeys(driver, pobj.sourceDetails, sourceDetails);

			if (!fc.utobj().isSelected(driver, pobj.includePage)) {
				fc.utobj().clickElement(driver, pobj.includePage);
			}
			displayOnWebPageText = fc.utobj().generateTestData(dataSet.get("text"));
			fc.utobj().sendKeys(driver, pobj.cmSource3NameDisplay, displayOnWebPageText);
			description = fc.utobj().generateTestData(dataSet.get("description"));
			fc.utobj().sendKeys(driver, pobj.descriptionRemarks, description);
			fc.utobj().clickElement(driver, pobj.saveBtn);
			fc.utobj().switchFrameToDefault(driver);
			fc.utobj().sleep(3000);
			/*fc.utobj().webDriverWait(driver, ".//option[contains(text () ,'" + sourceDetails + "')]");*/

			fc.utobj().printTestStep("Verify Source Details For Franchise");
			boolean isTextPresent = fc.utobj().assertPageSource(driver, sourceDetails);
			if (isTextPresent == false) {
				fc.utobj().throwsException("was not able to modify Source details");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = {"crm"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify the Delete Source Details :: Admin > CRM > Franchise(s) Source Summary", testCaseId = "TC_21_Delete_Source_Details")
	public void deleteSourceDetailsFranchise() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			// add Franchise user
			fc.utobj().printTestStep("Navigate To Admin > Franchise Location > Add Franchise Location");
			AdminFranchiseLocationAddFranchiseLocationPageTest franchiseLocation = new AdminFranchiseLocationAddFranchiseLocationPageTest();
			String franchiseName = fc.utobj().generateTestData(dataSet.get("franchiseName"));
			String regionName = fc.utobj().generateTestData(dataSet.get("regionName"));
			String firstNameF = fc.utobj().generateTestData(dataSet.get("firstNameFid"));
			String lastNameF = fc.utobj().generateTestData(dataSet.get("lastNameFid"));
			franchiseLocation.addFranchiseLocationForMkt(driver, franchiseName, regionName, firstNameF, lastNameF);

			fc.utobj().printTestStep("Navigate To Admin > CRM > Source Summary");
			fc.utobj().printTestStep("Add Source");
			String description = fc.utobj().generateTestData(dataSet.get("description"));
			String source = fc.utobj().generateTestData(dataSet.get("source"));
			String displayOnWebPageText = fc.utobj().generateTestData(dataSet.get("text"));
			addSource(driver, source, description, displayOnWebPageText);

			fc.utobj().printTestStep("Add Source Details For Franchise");

			String sourceDetails = fc.utobj().generateTestData(dataSet.get("sourceDetails"));
			displayOnWebPageText = fc.utobj().generateTestData(dataSet.get("text"));
			description = fc.utobj().generateTestData(dataSet.get("description"));
			addSourceDetailsFranchise(driver, franchiseName, source, sourceDetails, displayOnWebPageText, description);

			//fc.utobj().webDriverWait(driver, ".//*[contains(text () ,'" + sourceDetails + "')]");
			fc.utobj().sleep(5000);
			fc.utobj().printTestStep("Delete Source Details For Franchise");
			fc.utobj().actionImgOption(driver, sourceDetails, "Delete");
			fc.utobj().acceptAlertBox(driver);

			fc.utobj().printTestStep("Verify Delete Source Details For Franchise");
			boolean isTextPresent = fc.utobj().assertPageSource(driver, sourceDetails);
			if (isTextPresent == true) {
				fc.utobj().throwsException("was not delete to add Source details");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}
}
