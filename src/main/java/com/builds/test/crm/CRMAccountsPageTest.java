package com.builds.test.crm;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.xml.parsers.ParserConfigurationException;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.Test;
import org.xml.sax.SAXException;

import com.builds.test.admin.AdminUsersManageCorporateUsersAddCorporateUserPageTest;
import com.builds.test.admin.AdminUsersManageRegionalUsersAddRegionalUserPageTest;
import com.builds.test.common.CorporateUser;
import com.builds.uimaps.crm.CRMAccountsPage;
import com.builds.uimaps.crm.CRMLeadsPage;
import com.builds.uimaps.crm.CRMOpportunitiesPageUI;
import com.builds.utilities.FranconnectUtil;
import com.builds.utilities.TestCase;

public class CRMAccountsPageTest {
	FranconnectUtil fc = new FranconnectUtil();

	@Test(groups = { "crm"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Add Account At CRM > Accounts > Accounts Summary", testCaseId = "TC_106_Add_Account")
	public void addAccount() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			CRMAccountsPage pobj = new CRMAccountsPage(driver);

			AdminCRMConfigureIndustryPageTest industryPage = new AdminCRMConfigureIndustryPageTest();
			String industry = fc.utobj().generateTestData(dataSet.get("industry"));
			String description = fc.utobj().generateTestData(dataSet.get("description"));
			fc.utobj().printTestStep("Navigating to Admin > CRM >  Configure Industry Type");
			fc.utobj().printTestStep("Add Industry");
			industryPage.addIndustry(driver, industry, description);

			fc.utobj().printTestStep("Navigate To CRM > Accouts > Accounts Summary");
			fc.utobj().printTestStep("Add Account");
			fc.crm().crm_common().CRMAccountsLnk(driver);
			fc.utobj().clickElement(driver, pobj.addNew);
			fc.utobj().clickElement(driver, pobj.accountLnk);
			String accountName = fc.utobj().generateTestData(dataSet.get("accountName"));
			fc.utobj().sendKeys(driver, pobj.accountName, accountName);
			if (!fc.utobj().isSelected(driver, pobj.B2BAccountType)) {
				fc.utobj().clickElement(driver, pobj.B2BAccountType);
			}
			fc.utobj().selectDropDown(driver, pobj.visibilityType, "All Network");
			if (fc.utobj().isSelected(driver, pobj.isParent)) {
				fc.utobj().clickElement(driver, pobj.isParent);
			}
			fc.utobj().sendKeys(driver, pobj.address, "AddressTest");
			fc.utobj().sendKeys(driver, pobj.city, "City");
			fc.utobj().selectDropDown(driver, pobj.country, "USA");
			fc.utobj().sendKeys(driver, pobj.zipcode, "123455");
			fc.utobj().selectDropDown(driver, pobj.state, "Alabama");
			fc.utobj().sendKeys(driver, pobj.phoneNumbers, "1236547896");
			fc.utobj().sendKeys(driver, pobj.extn, "123");
			fc.utobj().sendKeys(driver, pobj.faxNumbers, "1236654");
			fc.utobj().sendKeys(driver, pobj.mobileNumbers, "1478523698");
			fc.utobj().sendKeys(driver, pobj.emailIds, "crmautomation@staffex.com");
			fc.utobj().sendKeys(driver, pobj.noOfEmployee, "500");
			fc.utobj().selectDropDown(driver, pobj.industry, industry);
			fc.utobj().sendKeys(driver, pobj.sicCode, "SicCode");
			fc.utobj().sendKeys(driver, pobj.servicesProvided, "Service123");
			fc.utobj().sendKeys(driver, pobj.stockSymbol, "qwerty");

			if (!fc.utobj().isSelected(driver, pobj.addContactNo)) {
				fc.utobj().clickElement(driver, pobj.addContactNo);
			}
			fc.utobj().clickElement(driver, pobj.saveBtn);

			fc.utobj().clickElement(driver, pobj.accountsLink);
			searchAccountByIndustry(driver, industry);
 
			fc.utobj().printTestStep("Verify The Add Account");
			boolean isAccountNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[.='" + accountName + "']");
			if (isAccountNamePresent == false) {
				fc.utobj().throwsException("was not able to add Account");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	public void addAcocunt(WebDriver driver, String industry, String accountName, String visibility) throws Exception {

		String testCaseId = "TC_Add_Account_CRM";

		if (fc.utobj().validate(testCaseId)) {
			try {
				CRMAccountsPage pobj = new CRMAccountsPage(driver);
				fc.crm().crm_common().CRMAccountsLnk(driver);
				fc.utobj().sleep(1000);
				fc.utobj().clickElement(driver, pobj.addNew);
				fc.utobj().sleep(1000);
				fc.utobj().clickElement(driver, pobj.accountLnk);
				fc.utobj().sendKeys(driver, pobj.accountName, accountName);
				if (!fc.utobj().isSelected(driver, pobj.B2BAccountType)) {
					fc.utobj().clickElement(driver, pobj.B2BAccountType);
				}
				fc.utobj().selectDropDown(driver, pobj.visibilityType, visibility);
				if (fc.utobj().isSelected(driver, pobj.isParent)) {
					fc.utobj().clickElement(driver, pobj.isParent);
				}
				fc.utobj().sendKeys(driver, pobj.address, "AddressTest");
				fc.utobj().sendKeys(driver, pobj.city, "City");
				fc.utobj().selectDropDown(driver, pobj.country, "USA");
				fc.utobj().sendKeys(driver, pobj.zipcode, "123455");
				fc.utobj().selectDropDown(driver, pobj.state, "Alabama");
				fc.utobj().sendKeys(driver, pobj.phoneNumbers, "1236547896");
				fc.utobj().sendKeys(driver, pobj.extn, "123");
				fc.utobj().sendKeys(driver, pobj.faxNumbers, "1236654");
				fc.utobj().sendKeys(driver, pobj.mobileNumbers, "1478523698");
				fc.utobj().sendKeys(driver, pobj.emailIds, fc.utobj().generateTestData("crmautomation")+"@staffex.com");  //vipinS  crmautomation@staffex.com
				fc.utobj().sendKeys(driver, pobj.noOfEmployee, "500");
				fc.utobj().selectDropDown(driver, pobj.industry, industry);
				fc.utobj().sendKeys(driver, pobj.sicCode, "SicCode");
				fc.utobj().sendKeys(driver, pobj.servicesProvided, "Service123");
				fc.utobj().sendKeys(driver, pobj.stockSymbol, "qwerty");

				if (!fc.utobj().isSelected(driver, pobj.addContactNo)) {
					fc.utobj().clickElement(driver, pobj.addContactNo);
				}
				fc.utobj().clickElement(driver, pobj.saveBtn);

				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException("Was not able to add Account :: CRM > Accounts > Accounts Summary");
		}
	}

	@Test(groups = "crm")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Modify Account At CRM > Accounts > Accounts Summary", testCaseId = "TC_107_Modify_Account")
	public void modifyAccountActionImgae() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			CRMAccountsPage pobj = new CRMAccountsPage(driver);

			AdminCRMConfigureIndustryPageTest industryPage = new AdminCRMConfigureIndustryPageTest();
			String industry = fc.utobj().generateTestData(dataSet.get("industry"));
			String description = fc.utobj().generateTestData(dataSet.get("description"));
			fc.utobj().printTestStep("Navigating to Admin > CRM >  Configure Industry Type");
			fc.utobj().printTestStep("Add Industry");
			industryPage.addIndustry(driver, industry, description);

			String accountName = fc.utobj().generateTestData(dataSet.get("accountName"));
			String visibility = dataSet.get("visibility");
			fc.utobj().printTestStep("Navigating to CRM >  Accounts > Accounts Summary");
			fc.utobj().printTestStep("Add Account");
			addAcocunt(driver, industry, accountName, visibility);

			fc.utobj().clickElement(driver, pobj.accountsLink);
			searchAccountByIndustry(driver, industry);

			fc.utobj().printTestStep("Modify The Account");
			fc.utobj().actionImgOption(driver, accountName, "Modify");

			accountName = fc.utobj().generateTestData(dataSet.get("accountName"));
			fc.utobj().sendKeys(driver, pobj.accountName, accountName);
			if (!fc.utobj().isSelected(driver, pobj.B2CAccountType)) {
				fc.utobj().clickElement(driver, pobj.B2CAccountType);
			}

			String visibilityM = dataSet.get("visibilityM");
			fc.utobj().selectDropDown(driver, pobj.visibilityType, visibilityM);

			fc.utobj().sendKeys(driver, pobj.address, "AddressTest");
			fc.utobj().sendKeys(driver, pobj.city, "City");
			fc.utobj().selectDropDown(driver, pobj.country, "USA");
			fc.utobj().sendKeys(driver, pobj.zipcode, "123455");
			fc.utobj().selectDropDown(driver, pobj.state, "Alabama");
			fc.utobj().sendKeys(driver, pobj.phoneNumbers, "1236547896");
			fc.utobj().sendKeys(driver, pobj.extn, "123");
			fc.utobj().sendKeys(driver, pobj.faxNumbers, "1236654");
			fc.utobj().sendKeys(driver, pobj.mobileNumbers, "1478523698");
			fc.utobj().sendKeys(driver, pobj.emailIds, "crmautomation@staffex.com");

			if (!fc.utobj().isSelected(driver, pobj.addContactNo)) {
				fc.utobj().clickElement(driver, pobj.addContactNo);
			}
			fc.utobj().clickElement(driver, pobj.saveBtn);

			fc.utobj().clickElement(driver, pobj.accountsLink);
			try {
				fc.utobj().selectDropDown(driver, pobj.resultsPerPage, "500");
			} catch (Exception e) {
			}

			fc.utobj().printTestStep("Verify The Modify Account");
			boolean isAccountNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[.='" + accountName + "']");
			if (isAccountNamePresent == false) {
				fc.utobj().throwsException("was not able to modify Account");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = {"crm"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Add Contact In Account By Action Image Option At CRM > Accounts > Accounts Summary", testCaseId = "TC_108_Add_Contact")
	public void addContactActionImage() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			CRMAccountsPage pobj = new CRMAccountsPage(driver);

			CRMContactsPageTest contactsPage = new CRMContactsPageTest();

			/*
			 * fc.utobj().printTestStep(testCaseId,
			 * "Navigate To Admin Users Manage Corporate User");
			 * fc.utobj().printTestStep(testCaseId, "Add Corporate User");
			 * String emailId="crmautomation@staffex.com";
			 * AdminUsersManageCorporateUsersAddCorporateUserPageTest
			 * corporateUser=new
			 * AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			 * Map<String,String> userInfo = CorporateUser corpUser = new
			 * CorporateUser();corpUser.setEmail(emailId);corpUser =
			 * fc.CommonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			 * corporateUser.
			 * createDefaultCorporateUser_Through_WebService_AfterLogin(driver,
			 * corpUser);
			 */

			fc.utobj().printTestStep("Navigate To Admin Users Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			String emailId = "crmautomation@staffex.com";
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUser = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporateUser.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			fc.utobj().printTestStep("Navigating to Admin > CRM > ContactType Configuration");
			fc.utobj().printTestStep("Add Contact Type");
			AdminCRMContactTypeConfigurationPageTest contactTypePage = new AdminCRMContactTypeConfigurationPageTest();
			String contactType = fc.utobj().generateTestData(dataSet.get("contactType"));
			contactTypePage.addContactType(driver, contactType);

			AdminCRMConfigureIndustryPageTest industryPage = new AdminCRMConfigureIndustryPageTest();
			String industry = fc.utobj().generateTestData(dataSet.get("industry"));
			String description = fc.utobj().generateTestData(dataSet.get("description"));
			fc.utobj().printTestStep("Navigating to Admin > CRM >  Configure Industry Type");
			fc.utobj().printTestStep("Add Industry");
			industryPage.addIndustry(driver, industry, description);
			String accountName = fc.utobj().generateTestData(dataSet.get("accountName"));
			String visibility = dataSet.get("visibility");
			fc.utobj().printTestStep("Navigating to CRM >  Accounts > Accounts Summary");
			fc.utobj().printTestStep("Add Account");
			addAcocunt(driver, industry, accountName, visibility);

			fc.utobj().printTestStep("Navigating to CRM >  Accounts > Accounts Summary");
			fc.utobj().printTestStep("Add Contact");

			fc.utobj().clickElement(driver, pobj.accountsLink);
			searchAccountByIndustry(driver, industry);
			fc.utobj().actionImgOption(driver, accountName, "Add Contact");

			String cFname = fc.utobj().generateTestData(dataSet.get("cFname"));
			String cLname = fc.utobj().generateTestData(dataSet.get("cLname"));
			String primaryCMethod = dataSet.get("primaryCMethod");
			String assignTo = "Corporate";
			String regionName = "";
			String franchiseId = "";
			String title = dataSet.get("title");
			String email = "crmautomation@staffex.com";

			contactsPage.addContactGeneric(driver, contactType, cFname, cLname, primaryCMethod, assignTo,
					corpUser.getuserFullName(), regionName, franchiseId, title, email, config);

			fc.utobj().clickElement(driver, pobj.accountsLink);
			searchAccountByIndustry(driver, industry);

			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//a[.='" + accountName + "']/ancestor::tr/td/a[.='1']"));

			fc.utobj().printTestStep("Verify The Contact");
			boolean isContactNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () , '" + cFname + " " + cLname + "')]");
			if (isContactNamePresent == false) {
				fc.utobj().throwsException("was not able to verify name of contact");
			}
			boolean isContactTypePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[contains(text () , '" + cFname + " "
					+ cLname + "')]/ancestor::tr/td[contains(text () , '" + contactType + "')]");
			if (isContactTypePresent == false) {
				fc.utobj().throwsException("was not able to verify Contact type");
			}
			boolean isOwnerPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[contains(text () , '" + cFname + " " + cLname
					+ "')]/ancestor::tr/td[contains(text () , '" + corpUser.getuserFullName() + "')]");
			if (isOwnerPresent == false) {
				fc.utobj().throwsException("was not able to verify Owner Of Contact");
			}

			boolean isStatusPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[contains(text () , '" + cFname + " " + cLname
					+ "')]/ancestor::tr/td[contains(text () , 'New')]");
			if (isStatusPresent == false) {
				fc.utobj().throwsException("was not able to verify Status of Contact");
			}

			// verify at Contact Tab
			fc.utobj().clickElement(driver, pobj.accountsLink);
			searchAccountByIndustry(driver, industry);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//a[.='" + accountName + "']"));
			fc.utobj().clickElement(driver, pobj.contactsTab);

			boolean isContactNameAtContactPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + cFname + " " + cLname + "')]");
			if (isContactNameAtContactPresent == false) {
				fc.utobj().throwsException("was not able to verify Contact Name At Contacts Tab");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = {"crm"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2017-07-21", testCaseDescription = "Verify The Add Opportunity By Action Image Option At CRM > Accounts > Accounts Summary", testCaseId = "TC_109_Add_Opportunity")
	public void addOpportunityActionImage() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			CRMAccountsPage pobj = new CRMAccountsPage(driver);

			CRMContactsPageTest contactsPage = new CRMContactsPageTest();

			fc.utobj().printTestStep("Navigate To Admin Users Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			String emailId = "crmautomation@staffex.com";
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUser = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporateUser.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			fc.utobj().printTestStep("Navigating to Admin > CRM > ContactType Configuration");
			fc.utobj().printTestStep("Add Contact Type");
			AdminCRMContactTypeConfigurationPageTest contactTypePage = new AdminCRMContactTypeConfigurationPageTest();
			String contactType = fc.utobj().generateTestData(dataSet.get("contactType"));
			contactTypePage.addContactType(driver, contactType);
			fc.utobj().printTestStep("Navigating to Admin > CRM >  Configure Opportunity Stages");
			fc.utobj().printTestStep("Add Stage");
			AdminCRMConfigureOpportunityStagesPageTest stagePage = new AdminCRMConfigureOpportunityStagesPageTest();
			String stage = fc.utobj().generateTestData(dataSet.get("stage"));
			stagePage.addStage(driver, stage);

			AdminCRMConfigureIndustryPageTest industryPage = new AdminCRMConfigureIndustryPageTest();
			String industry = fc.utobj().generateTestData(dataSet.get("industry"));
			String description = fc.utobj().generateTestData(dataSet.get("description"));
			fc.utobj().printTestStep("Navigating to Admin > CRM >  Configure Industry Type");
			fc.utobj().printTestStep("Add Industry");
			industryPage.addIndustry(driver, industry, description);

			String accountName = fc.utobj().generateTestData(dataSet.get("accountName"));
			String visibility = dataSet.get("visibility");
			fc.utobj().printTestStep("Navigating to CRM >  Accounts > Accounts Summary");
			fc.utobj().printTestStep("Add Account");
			addAcocunt(driver, industry, accountName, visibility);

			fc.utobj().printTestStep("Navigating to CRM >  Accounts > Accounts Summary");
			fc.utobj().printTestStep("Add Contact");

			fc.utobj().clickElement(driver, pobj.accountsLink);
			searchAccountByIndustry(driver, industry);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//a[.='" + accountName + "']"));
			fc.utobj().clickElement(driver, pobj.contactsTab);
			fc.utobj().clickElement(driver, pobj.addContact);

			String cFname = fc.utobj().generateTestData(dataSet.get("cFname"));
			String cLname = fc.utobj().generateTestData(dataSet.get("cLname"));
			String primaryCMethod = dataSet.get("primaryCMethod");
			String assignTo = "Corporate";
			String regionName = "";
			String franchiseId = "";
			String title = dataSet.get("title");
			String email = fc.utobj().generateTestData("crmautomation")+"@staffex.com";//crmautomation@staffex.com";  vipinS

			contactsPage.addContactGenericWithAccount(driver, contactType, cFname, cLname, primaryCMethod, assignTo,
					corpUser.getuserFullName(), regionName, franchiseId, title, email, config, accountName); 

			fc.utobj().printTestStep("Add Opportunity");
			fc.utobj().clickElement(driver, pobj.accountsLink);
			searchAccountByIndustry(driver, industry);

			fc.utobj().actionImgOption(driver, accountName, "Add Opportunity");

			String opportunityName = fc.utobj().generateTestData(dataSet.get("opportunityName"));
			String contactName = cFname + " " + cLname;
			String opportunityType = dataSet.get("opportunityType");
			String opportunitySource = dataSet.get("opportunitySource");
			String salesAmount = dataSet.get("salesAmount");
			addOpportunity(driver, opportunityName, accountName, contactName, corpUser.getuserFullName(), stage,   
					opportunityType, opportunitySource, salesAmount, config);

			fc.utobj().clickElement(driver, pobj.accountsLink);
			searchAccountByIndustry(driver, industry);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//a[.='" + accountName + "']"));
			fc.utobj().clickElement(driver, pobj.opportunitiesTab);

			fc.utobj().printTestStep("Verify The Add Oportunity");
			fc.utobj().sleep(2000);
			boolean isOpportunityPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[.='" + opportunityName + "']");
			if (isOpportunityPresent == false) {
				fc.utobj().throwsException("was not able to verify Opportunity Name");
			}
			fc.utobj().sleep(2000);
			boolean isAssignToPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[.='" + opportunityName
					+ "']/ancestor::tr/td[contains(text () , '" + corpUser.getuserFullName() + "')]");
			if (isAssignToPresent == false) {
				fc.utobj().throwsException("was not able to verify Assign To");
			}
			fc.utobj().sleep(2000);
			boolean isAccountNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[.='" + opportunityName + "']/ancestor::tr/td/a[contains(text () , '" + accountName + "')]");
			if (isAccountNamePresent == false) {
				fc.utobj().throwsException("was not able to verify Account");
			}
			fc.utobj().sleep(2000);
			boolean isContactNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[.='" + opportunityName + "']/ancestor::tr/td/a[contains(text () , '" + contactName + "')]");
			if (isContactNamePresent == false) {
				fc.utobj().throwsException("was not able to verify Contact");
			}
			fc.utobj().sleep(2000);
			boolean isStagePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[.='" + opportunityName + "']/ancestor::tr/td[contains(text () , '" + stage + "')]");
			if (isStagePresent == false) {
				fc.utobj().throwsException("was not able to verify Stage");
			}
			fc.utobj().sleep(2000);
			boolean isSalesAmountPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[.='" + opportunityName
					+ "']/ancestor::tr/td[contains(text () , '" + salesAmount.concat(".00") + "')]");
			if (isSalesAmountPresent == false) {
				fc.utobj().throwsException("was not able to verify Sales Amount");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "crm" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Add Child Account By Action Image Option At CRM > Accounts > Accounts Summary", testCaseId = "TC_111_Add_Child_Account")
	public void addChildAccountActionImage() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			CRMAccountsPage pobj = new CRMAccountsPage(driver);

			AdminCRMConfigureIndustryPageTest industryPage = new AdminCRMConfigureIndustryPageTest();
			String industry = fc.utobj().generateTestData(dataSet.get("industry"));
			String description = fc.utobj().generateTestData(dataSet.get("description"));
			fc.utobj().printTestStep("Navigating to Admin > CRM >  Configure Industry Type");
			fc.utobj().printTestStep("Add Industry");
			industryPage.addIndustry(driver, industry, description);

			fc.utobj().printTestStep("Navigate To CRM > Accounts > Accounts Summary");
			String accountName = fc.utobj().generateTestData(dataSet.get("accountName"));
			String visibility = dataSet.get("visibility");
			addAcocunt(driver, industry, accountName, visibility);

			fc.utobj().clickElement(driver, pobj.accountsLink);
			searchAccountByIndustry(driver, industry);
			fc.utobj().printTestStep("Add Chilld Account");
			fc.utobj().actionImgOption(driver, accountName, "Add Child Account");

			String accountNameChild = fc.utobj().generateTestData(dataSet.get("accountName"));
			fc.utobj().sendKeys(driver, pobj.accountName, accountNameChild);
			if (!fc.utobj().isSelected(driver, pobj.B2BAccountType)) {
				fc.utobj().clickElement(driver, pobj.B2BAccountType);
			}
			fc.utobj().selectDropDown(driver, pobj.visibilityType, "All Network");
			fc.utobj().sendKeys(driver, pobj.address, "AddressTest");
			fc.utobj().sendKeys(driver, pobj.city, "City");
			fc.utobj().selectDropDown(driver, pobj.country, "USA");
			fc.utobj().sendKeys(driver, pobj.zipcode, "123455");
			fc.utobj().selectDropDown(driver, pobj.state, "Alabama");
			fc.utobj().sendKeys(driver, pobj.phoneNumbers, "1236547896");
			fc.utobj().sendKeys(driver, pobj.extn, "123");
			fc.utobj().sendKeys(driver, pobj.faxNumbers, "1236654");
			fc.utobj().sendKeys(driver, pobj.mobileNumbers, "1478523698");
			fc.utobj().sendKeys(driver, pobj.emailIds, "crmautomation@staffex.com");
			fc.utobj().sendKeys(driver, pobj.noOfEmployee, "500");
			fc.utobj().selectDropDown(driver, pobj.industry, industry);
			fc.utobj().sendKeys(driver, pobj.sicCode, "SicCode");
			fc.utobj().sendKeys(driver, pobj.servicesProvided, "Service123");
			fc.utobj().sendKeys(driver, pobj.stockSymbol, "qwerty");

			if (!fc.utobj().isSelected(driver, pobj.addContactNo)) {
				fc.utobj().clickElement(driver, pobj.addContactNo);
			}
			fc.utobj().clickElement(driver, pobj.saveBtn);
			fc.utobj().clickElement(driver, pobj.accountsLink);
			searchAccountByIndustry(driver, industry);

			fc.utobj().printTestStep("Verify The Child Account");
			boolean isChildAccountPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[.='" + accountNameChild + "']");
			if (isChildAccountPresent == false) {
				fc.utobj().throwsException("was not able to verify Child Account Name");
			}

			boolean isParentAccountPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[.='" + accountNameChild + "']/ancestor::tr/td/a[.='" + accountName + "']");
			if (isParentAccountPresent == false) {
				fc.utobj().throwsException("was not able to verify Parent Account Name");
			}

			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//a[.='" + accountName + "']"));
			fc.utobj().clickElement(driver, pobj.childAccounts);

			boolean isAccountNameChildPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[.='" + accountNameChild + "']");
			if (isAccountNameChildPresent == false) {
				fc.utobj().throwsException("was not able to Verify Child Account Name At Child Accounts Tab");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = {"crm"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Add Lead At CRM > Accounts > Accounts Summary", testCaseId = "TC_112_Add_Lead")
	public void addLead() throws IOException, Exception, ParserConfigurationException, SAXException {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			CRMLeadsPage pobj = new CRMLeadsPage(driver);

			CRMLeadsPageTest leadPage = new CRMLeadsPageTest();

			fc.utobj().printTestStep("Navigate To Admin Users Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			String emailId = "crmautomation@staffex.com";
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUser = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporateUser.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			fc.utobj().printTestStep("Navigate To Accounts > Accounts Summary");
			fc.utobj().printTestStep("Add Lead");
			fc.crm().crm_common().CRMAccountsLnk(driver);
			fc.utobj().clickElement(driver, pobj.addNew);
			fc.utobj().clickElement(driver, pobj.leadLnk);
			fc.utobj().selectDropDown(driver, pobj.title, dataSet.get("title"));
			String firstName = fc.utobj().generateTestData(dataSet.get("firstName"));
			fc.utobj().sendKeys(driver, pobj.leadFirstName, firstName);
			String lastName = fc.utobj().generateTestData(dataSet.get("lastName"));
			fc.utobj().sendKeys(driver, pobj.leadLastName, lastName);
			String company = fc.utobj().generateTestData(dataSet.get("company"));
			fc.utobj().sendKeys(driver, pobj.companyName, company);
			fc.utobj().selectDropDown(driver, pobj.primaryContactMethodSelect, dataSet.get("primaryContactMethod"));
			if (!fc.utobj().isSelected(driver, pobj.assignToCorporate)) {
				fc.utobj().clickElement(driver, pobj.assignToCorporate);
			}
			fc.utobj().selectDropDown(driver, pobj.selectCorporateUser, corpUser.getuserFullName());
			String address = fc.utobj().generateTestData(dataSet.get("address"));
			fc.utobj().sendKeys(driver, pobj.address, address);
			String city = fc.utobj().generateTestData(dataSet.get("city"));
			fc.utobj().sendKeys(driver, pobj.city, city);
			fc.utobj().selectDropDown(driver, pobj.country, "USA");
			fc.utobj().selectDropDown(driver, pobj.state, "Alabama");
			fc.utobj().sendKeys(driver, pobj.zipcode, "12345");
			fc.utobj().sendKeys(driver, pobj.phoneNumbers, "1236547896");
			fc.utobj().sendKeys(driver, pobj.extn, "12");
			fc.utobj().sendKeys(driver, pobj.faxNumbers, "1263985487");
			fc.utobj().sendKeys(driver, pobj.mobileNumbers, "8896724441");
			String email = dataSet.get("emailId");
			fc.utobj().sendKeys(driver, pobj.emailIds, email);
			fc.utobj().sendKeys(driver, pobj.alternateEmail, "test@gmail.com");
			String suffix = fc.utobj().generateTestData(dataSet.get("suffix"));
			fc.utobj().sendKeys(driver, pobj.suffix, suffix);
			String jobTitle = fc.utobj().generateTestData(dataSet.get("jobTitle"));
			fc.utobj().sendKeys(driver, pobj.position, jobTitle);
			fc.utobj().sendKeys(driver, pobj.birthdate, "11/24/1980");
			fc.utobj().sendKeys(driver, pobj.anniversarydate, "11/24/1999");
			fc.utobj().selectDropDown(driver, pobj.rating, dataSet.get("rating"));
			fc.utobj().sendKeys(driver, pobj.bestTimeToContact, "12:00 PM EST");
			String comment = fc.utobj().generateTestData(dataSet.get("comment"));
			fc.utobj().sendKeys(driver, pobj.comments, comment);
			fc.utobj().clickElement(driver, pobj.saveBtn);

			// verify at Home Page
			fc.utobj().printTestStep("Verify The Add Lead");
			fc.utobj().clickElement(driver, pobj.leadsLink);
			leadPage.searchLeadByOwner(driver, corpUser.getuserFullName(), "Open Leads");

			boolean verifyName = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[.='" + firstName + " " + lastName + "']");
			if (verifyName == false) {
				fc.utobj().throwsException("was not able to verify Name");
			}
			boolean ownerOfLead = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[.='" + firstName + " " + lastName
					+ "']/ancestor::tr/td[contains(text () , '" + corpUser.getuserFullName() + "')]");
			if (ownerOfLead == false) {
				fc.utobj().throwsException("was not able to verify owner of the lead");
			}
			boolean statusOfLead = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[.='" + firstName + " " + lastName + "']/ancestor::tr/td[contains(text () , 'New')]");
			if (statusOfLead == false) {
				fc.utobj().throwsException("was not able to verify staus of the lead");
			}
			boolean emailOfLead = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[.='" + firstName + " " + lastName
					+ "']/ancestor::tr/td/a[contains(text () , '" + email + "')]");
			if (emailOfLead == false) {
				fc.utobj().throwsException("was not able to verify email of the lead");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = {"crm"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Add Contact At CRM > Lead > Lead Summary", testCaseId = "TC_113_Add_Contact")
	public void addContact() throws IOException, Exception, ParserConfigurationException, SAXException {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			CRMLeadsPage pobj = new CRMLeadsPage(driver);

			CRMContactsPageTest contactsPage = new CRMContactsPageTest();

			fc.utobj().printTestStep("Navigate To Admin Users Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			String emailId = "crmautomation@staffex.com";
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUser = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporateUser.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			fc.utobj().printTestStep("Navigate To Admin > CRM > Contact Type Configuration");
			fc.utobj().printTestStep("Add Contact Type");
			AdminCRMContactTypeConfigurationPageTest contatcTypePage = new AdminCRMContactTypeConfigurationPageTest();
			String contactType = fc.utobj().generateTestData(dataSet.get("contactType"));
			contatcTypePage.addContactType(driver, contactType);

			/*
			 * fc.utobj().printTestStep(testCaseId,
			 * "Navigating to Admin > CRM > Configure Lead Type");
			 * fc.utobj().printTestStep(testCaseId, "Add Lead Type");
			 * AdminCRMConfigureLeadTypePageTest leadTypePage = new
			 * AdminCRMConfigureLeadTypePageTest(); String leadType =
			 * fc.utobj().generateTestData(dataSet.get("leadType"));
			 * leadTypePage.addLeadType(driver, leadType, config);
			 * 
			 * AdminCRMSourceSummaryPageTest addSourcePage = new
			 * AdminCRMSourceSummaryPageTest(); String source =
			 * fc.utobj().generateTestData(dataSet.get("source")); String
			 * description =
			 * fc.utobj().generateTestData(dataSet.get("description")); String
			 * displayOnWebPageText =
			 * fc.utobj().generateTestData(dataSet.get("text"));
			 * fc.utobj().printTestStep(testCaseId,
			 * "Navigating to Admin > CRM > Source Summary");
			 * fc.utobj().printTestStep(testCaseId, "Add Source");
			 * addSourcePage.addSource(driver, source, description,
			 * displayOnWebPageText, config);
			 * fc.utobj().printTestStep(testCaseId, "Add Source Details");
			 * String sourceDetails =
			 * fc.utobj().generateTestData(dataSet.get("sourceDetails"));
			 * addSourcePage.addSourceDetails(driver, source, sourceDetails,
			 * displayOnWebPageText, description, config);
			 * fc.utobj().printTestStep(testCaseId,
			 * "Navigating to Admin > CRM > Configure Medium");
			 * fc.utobj().printTestStep(testCaseId, "Add Medium");
			 * AdminCRMConfigureMediumPageTest mediumPage = new
			 * AdminCRMConfigureMediumPageTest(); String medium =
			 * fc.utobj().generateTestData(dataSet.get("medium"));
			 * mediumPage.addMedium(driver, medium, description, config);
			 */

			fc.utobj().printTestStep("Navigating to CRM >  Accounts > Accounts Summary");
			fc.utobj().printTestStep("Add Contact");

			fc.crm().crm_common().CRMAccountsLnk(driver);
			fc.utobj().clickElement(driver, pobj.addNew);
			fc.utobj().clickElement(driver, pobj.contactLnk);

			String firstName = fc.utobj().generateTestData(dataSet.get("firstName"));
			String lastName = fc.utobj().generateTestData(dataSet.get("lastName"));
			String primaryCMethod = dataSet.get("primaryCMethod");
			String assignTo = "Corporate";
			String regionName = "";
			String franchiseId = "";
			String title = dataSet.get("title");
			String email = "crmautomation@staffex.com";

			contactsPage.addContactGeneric(driver, contactType, firstName, lastName, primaryCMethod, assignTo,
					corpUser.getuserFullName(), regionName, franchiseId, title, email, config);

			fc.utobj().printTestStep("Verify Contact");
			boolean isContactTypePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () ,'Contact Type')]/ancestor::tr/td[.='" + contactType + "']");
			if (isContactTypePresent == false) {
				fc.utobj().throwsException("was not able to verify Contact Type");
			}
			boolean isContactNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () ,'Name')]/ancestor::tr/td[.='Mr. " + firstName + " " + lastName + "']");
			if (isContactNamePresent == false) {
				fc.utobj().throwsException("was not able to verify Contact Name");
			}
			boolean isPrimaryContactTypePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () ,'Primary Contact Method')]/ancestor::tr/td[.='Email']");
			if (isPrimaryContactTypePresent == false) {
				fc.utobj().throwsException("was not able to verify Primary Contact Type");
			}
			boolean isUserPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () ,'Assign To ')]/ancestor::tr/td[contains(text () , '"+corpUser.getuserFullName()+"')]");
			if (isUserPresent == false) {
				fc.utobj().throwsException("was not able to verify Assign To");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "crm")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify the Modify Account At CRM > Accounts > Accounts Info", testCaseId = "TC_115_Modify_Accounts")
	public void modifyAccountTab() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			CRMAccountsPage pobj = new CRMAccountsPage(driver);

			AdminCRMConfigureIndustryPageTest industryPage = new AdminCRMConfigureIndustryPageTest();
			String industry = fc.utobj().generateTestData(dataSet.get("industry"));
			String description = fc.utobj().generateTestData(dataSet.get("description"));
			fc.utobj().printTestStep("Navigating to Admin > CRM >  Configure Industry Type");
			fc.utobj().printTestStep("Add Industry");
			industryPage.addIndustry(driver, industry, description);

			fc.utobj().printTestStep("Navigate To CRM > Accounts > Accounts Summary");
			fc.utobj().printTestStep("Add Account");
			String accountName = fc.utobj().generateTestData(dataSet.get("accountName"));
			String visibility = dataSet.get("visibility");
			addAcocunt(driver, industry, accountName, visibility);

			fc.utobj().printTestStep("Modify Account");
			fc.utobj().clickElement(driver, pobj.accountsLink);
			searchAccountByIndustry(driver, industry);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//a[.='" + accountName + "']"));
			fc.utobj().clickElement(driver, pobj.modifyTab);

			accountName = fc.utobj().generateTestData(dataSet.get("accountName"));
			fc.utobj().sendKeys(driver, pobj.accountName, accountName);
			if (!fc.utobj().isSelected(driver, pobj.B2CAccountType)) {
				fc.utobj().clickElement(driver, pobj.B2CAccountType);
			}

			String visibilityM = dataSet.get("visibilityM");
			fc.utobj().selectDropDown(driver, pobj.visibilityType, visibilityM);

			fc.utobj().sendKeys(driver, pobj.address, "AddressTest");
			fc.utobj().sendKeys(driver, pobj.city, "City");
			fc.utobj().selectDropDown(driver, pobj.country, "USA");
			fc.utobj().sendKeys(driver, pobj.zipcode, "123455");
			fc.utobj().selectDropDown(driver, pobj.state, "Alabama");
			fc.utobj().sendKeys(driver, pobj.phoneNumbers, "1236547896");
			fc.utobj().sendKeys(driver, pobj.extn, "123");
			fc.utobj().sendKeys(driver, pobj.faxNumbers, "1236654");
			fc.utobj().sendKeys(driver, pobj.mobileNumbers, "1478523698");
			fc.utobj().sendKeys(driver, pobj.emailIds, "crmautomation@staffex.com");

			if (!fc.utobj().isSelected(driver, pobj.addContactNo)) {
				fc.utobj().clickElement(driver, pobj.addContactNo);
			}
			fc.utobj().clickElement(driver, pobj.saveBtn);

			fc.utobj().printTestStep("Verify The Modify Account");
			boolean isAccountNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () ,'Account Name')]/ancestor::tr/td[.='" + accountName + "']");
			if (isAccountNamePresent == false) {
				fc.utobj().throwsException("was not able to modify Account");
			}
			boolean isVisibilityPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () ,'Visibility ')]/ancestor::tr/td[.='" + visibilityM + "']");
			if (isVisibilityPresent == false) {
				fc.utobj().throwsException("was not able to verify visibility");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "crm")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Delete Account At CRM > Accounts > Accounts Info", testCaseId = "TC_116_Delete_Accounts")
	public void deleteAccountTab() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			CRMAccountsPage pobj = new CRMAccountsPage(driver);

			AdminCRMConfigureIndustryPageTest industryPage = new AdminCRMConfigureIndustryPageTest();
			String industry = fc.utobj().generateTestData(dataSet.get("industry"));
			String description = fc.utobj().generateTestData(dataSet.get("description"));
			fc.utobj().printTestStep("Navigating to Admin > CRM >  Configure Industry Type");
			fc.utobj().printTestStep("Add Industry");
			industryPage.addIndustry(driver, industry, description);

			String accountName = fc.utobj().generateTestData(dataSet.get("accountName"));
			String visibility = dataSet.get("visibility");
			fc.utobj().printTestStep("Navigating to CRM >  Accounts > Accounts Summary");
			fc.utobj().printTestStep("Add Account");
			addAcocunt(driver, industry, accountName, visibility);

			fc.utobj().clickElement(driver, pobj.accountsLink);
			searchAccountByIndustry(driver, industry);
			fc.utobj().printTestStep("Delete Account");
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//a[.='" + accountName + "']"));
			fc.utobj().clickElement(driver, pobj.deleteAccounTab);
			fc.utobj().acceptAlertBox(driver);

			searchAccountByIndustry(driver, industry);

			fc.utobj().printTestStep("Verify The Delete Account");
			boolean isAccountNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[.='" + accountName + "']");
			if (isAccountNamePresent == true) {
				fc.utobj().throwsException("was not able to add Account");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "crm")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Add Remarks At CRM > Accounts > Accounts Info", testCaseId = "TC_117_Add_Remarks")
	public void addRemarks() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			CRMAccountsPage pobj = new CRMAccountsPage(driver);

			AdminCRMConfigureIndustryPageTest industryPage = new AdminCRMConfigureIndustryPageTest();
			String industry = fc.utobj().generateTestData(dataSet.get("industry"));
			String description = fc.utobj().generateTestData(dataSet.get("description"));
			fc.utobj().printTestStep("Navigating to Admin > CRM >  Configure Industry Type");
			fc.utobj().printTestStep("Add Industry");
			industryPage.addIndustry(driver, industry, description);

			String accountName = fc.utobj().generateTestData(dataSet.get("accountName"));
			String visibility = dataSet.get("visibility");
			fc.utobj().printTestStep("Navigating to CRM >  Accounts > Accounts Summary");
			fc.utobj().printTestStep("Add Account");
			addAcocunt(driver, industry, accountName, visibility);

			fc.utobj().clickElement(driver, pobj.accountsLink);
			searchAccountByIndustry(driver, industry);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//a[.='" + accountName + "']"));

			fc.utobj().printTestStep("Add Remark");
			String remark = fc.utobj().generateTestData(dataSet.get("remark"));
			fc.utobj().sendKeys(driver, pobj.remarks, remark);
			fc.utobj().clickElement(driver, pobj.addBtn);

			fc.utobj().printTestStep("Verify Add Remarks");
			boolean isRemarksPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//*[@id='RemarksTd']/span[.='" + remark + "']");
			if (isRemarksPresent == false) {
				fc.utobj().throwsException("was not able to verify Remarks");
			}
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//*[@id='RemarksTd']/span[.='" + remark + "']"));
			fc.commonMethods().switch_cboxIframe_frameId(driver);

			boolean isRemarksPresentAtCbox = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () ,'Remarks')]/ancestor::tr/td[.='" + remark + "']");
			if (isRemarksPresentAtCbox == false) {
				fc.utobj().throwsException("was not able to verify Remarks at Cbox");
			}

			fc.utobj().clickElement(driver, pobj.closeBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "crm")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify the Modify Account At CRM > Accounts > Accounts Info", testCaseId = "TC_118_Modify_Account")
	public void modifyBottomBtn() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			CRMAccountsPage pobj = new CRMAccountsPage(driver);

			AdminCRMConfigureIndustryPageTest industryPage = new AdminCRMConfigureIndustryPageTest();
			String industry = fc.utobj().generateTestData(dataSet.get("industry"));
			String description = fc.utobj().generateTestData(dataSet.get("description"));
			fc.utobj().printTestStep("Navigating to Admin > CRM >  Configure Industry Type");
			fc.utobj().printTestStep("Add Industry");
			industryPage.addIndustry(driver, industry, description);

			String accountName = fc.utobj().generateTestData(dataSet.get("accountName"));
			String visibility = dataSet.get("visibility");
			fc.utobj().printTestStep("Navigating to CRM >  Accounts > Accounts Summary");
			fc.utobj().printTestStep("Add Account");
			addAcocunt(driver, industry, accountName, visibility);

			fc.utobj().clickElement(driver, pobj.accountsLink);
			searchAccountByIndustry(driver, industry);
			fc.utobj().printTestStep("Modify Account");
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//a[.='" + accountName + "']"));
			fc.utobj().clickElement(driver, pobj.modifyTabBtm);

			accountName = fc.utobj().generateTestData(dataSet.get("accountName"));
			fc.utobj().sendKeys(driver, pobj.accountName, accountName);
			if (!fc.utobj().isSelected(driver, pobj.B2CAccountType)) {
				fc.utobj().clickElement(driver, pobj.B2CAccountType);
			}

			String visibilityM = dataSet.get("visibilityM");
			fc.utobj().selectDropDown(driver, pobj.visibilityType, visibilityM);

			fc.utobj().sendKeys(driver, pobj.address, "AddressTest");
			fc.utobj().sendKeys(driver, pobj.city, "City");
			fc.utobj().selectDropDown(driver, pobj.country, "USA");
			fc.utobj().sendKeys(driver, pobj.zipcode, "123455");
			fc.utobj().selectDropDown(driver, pobj.state, "Alabama");
			fc.utobj().sendKeys(driver, pobj.phoneNumbers, "1236547896");
			fc.utobj().sendKeys(driver, pobj.extn, "123");
			fc.utobj().sendKeys(driver, pobj.faxNumbers, "1236654");
			fc.utobj().sendKeys(driver, pobj.mobileNumbers, "1478523698");
			fc.utobj().sendKeys(driver, pobj.emailIds, "crmautomation@staffex.com");

			if (!fc.utobj().isSelected(driver, pobj.addContactNo)) {
				fc.utobj().clickElement(driver, pobj.addContactNo);
			}
			fc.utobj().clickElement(driver, pobj.saveBtn);

			fc.utobj().printTestStep("Verify Modify Account");
			boolean isAccountNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () ,'Account Name')]/ancestor::tr/td[.='" + accountName + "']");
			if (isAccountNamePresent == false) {
				fc.utobj().throwsException("was not able to modify Account");
			}
			boolean isVisibilityPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () ,'Visibility ')]/ancestor::tr/td[.='" + visibilityM + "']");
			if (isVisibilityPresent == false) {
				fc.utobj().throwsException("was not able to verify visibility");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "crm")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Add Contact At CRM > Accounts > Contacts", testCaseId = "TC_119_Add_Contact")
	public void addContactAtContactTab() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			CRMAccountsPage pobj = new CRMAccountsPage(driver);

			CRMContactsPageTest contactsPage = new CRMContactsPageTest();

			fc.utobj().printTestStep("Navigate To Admin Users Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			String emailId = "crmautomation@staffex.com";
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUser = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporateUser.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);
			fc.utobj().printTestStep("Navigating to Admin > CRM > ContactType Configuration");
			fc.utobj().printTestStep("Add Contact Type");
			AdminCRMContactTypeConfigurationPageTest contactTypePage = new AdminCRMContactTypeConfigurationPageTest();
			String contactType = fc.utobj().generateTestData(dataSet.get("contactType"));
			contactTypePage.addContactType(driver, contactType);

			/*
			 * fc.utobj().printTestStep(testCaseId,
			 * "Navigating to Admin > CRM > Configure Lead Type");
			 * fc.utobj().printTestStep(testCaseId, "Add Lead Type");
			 * AdminCRMConfigureLeadTypePageTest leadTypePage=new
			 * AdminCRMConfigureLeadTypePageTest(); String
			 * leadType=fc.utobj().generateTestData(dataSet.get("leadType"));
			 * leadTypePage.addLeadType(driver, leadType, config);
			 * fc.utobj().printTestStep(testCaseId,
			 * "Navigating to Admin > CRM > Configure Medium");
			 * fc.utobj().printTestStep(testCaseId, "Add Medium");
			 * AdminCRMConfigureMediumPageTest mediumPage=new
			 * AdminCRMConfigureMediumPageTest(); String
			 * medium=fc.utobj().generateTestData(dataSet.get("medium")); String
			 * description=fc.utobj().generateTestData(dataSet.get("description"
			 * )); mediumPage.addMedium(driver, medium, description, config);
			 * 
			 * AdminCRMSourceSummaryPageTest addSourcePage = new
			 * AdminCRMSourceSummaryPageTest(); String source =
			 * fc.utobj().generateTestData(dataSet.get("source")); description =
			 * fc.utobj().generateTestData(dataSet.get("description")); String
			 * displayOnWebPageText =
			 * fc.utobj().generateTestData(dataSet.get("text"));
			 * fc.utobj().printTestStep(testCaseId,
			 * "Navigating to Admin > CRM > Source Summary");
			 * fc.utobj().printTestStep(testCaseId, "Add Source");
			 * addSourcePage.addSource(driver, source, description,
			 * displayOnWebPageText, config);
			 * fc.utobj().printTestStep(testCaseId, "Add Source Details");
			 * String sourceDetails =
			 * fc.utobj().generateTestData(dataSet.get("sourceDetails"));
			 * addSourcePage.addSourceDetails(driver, source, sourceDetails,
			 * displayOnWebPageText, description, config);
			 */

			AdminCRMConfigureIndustryPageTest industryPage = new AdminCRMConfigureIndustryPageTest();
			String industry = fc.utobj().generateTestData(dataSet.get("industry"));
			String description = fc.utobj().generateTestData(dataSet.get("description"));
			fc.utobj().printTestStep("Navigating to Admin > CRM >  Configure Industry Type");
			fc.utobj().printTestStep("Add Industry");
			industryPage.addIndustry(driver, industry, description);

			String accountName = fc.utobj().generateTestData(dataSet.get("accountName"));
			String visibility = dataSet.get("visibility");
			fc.utobj().printTestStep("Navigating to CRM >  Accounts > Accounts Summary");
			fc.utobj().printTestStep("Add Account");
			addAcocunt(driver, industry, accountName, visibility);

			fc.utobj().printTestStep("Navigating to CRM >  Accounts > Accounts Summary");
			fc.utobj().printTestStep("Add Contact");

			fc.utobj().clickElement(driver, pobj.accountsLink);
			searchAccountByIndustry(driver, industry);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//a[.='" + accountName + "']"));
			fc.utobj().clickElement(driver, pobj.contactsTab);
			fc.utobj().clickElement(driver, pobj.addContact);

			String cFname = fc.utobj().generateTestData(dataSet.get("cFname"));
			String cLname = fc.utobj().generateTestData(dataSet.get("cLname"));
			String primaryCMethod = dataSet.get("primaryCMethod");
			String assignTo = "Corporate";
			String regionName = "";
			String franchiseId = "";
			String title = dataSet.get("title");
			String email = "crmautomation@staffex.com";

			contactsPage.addContactGeneric(driver, contactType, cFname, cLname, primaryCMethod, assignTo,
					corpUser.getuserFullName(), regionName, franchiseId, title, email, config);

			fc.utobj().clickElement(driver, pobj.accountsLink);
			searchAccountByIndustry(driver, industry);

			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//a[.='" + accountName + "']"));
			fc.utobj().clickElement(driver, pobj.contactsTab);
			try {
				fc.utobj().selectDropDown(driver, pobj.resultsPerPage, "500");
			} catch (Exception e) {
			}

			fc.utobj().printTestStep("Verify Add Contact");
			boolean isContactNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () , '" + cFname + " " + cLname + "')]");
			if (isContactNamePresent == false) {
				fc.utobj().throwsException("was not able to verify name of contact");
			}
			boolean isContactTypePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[contains(text () , '" + cFname + " "
					+ cLname + "')]/ancestor::tr/td[contains(text () , '" + contactType + "')]");
			if (isContactTypePresent == false) {
				fc.utobj().throwsException("was not able to verify Contact type");
			}
			boolean isOwnerPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[contains(text () , '" + cFname + " " + cLname
					+ "')]/ancestor::tr/td[contains(text () , '" + corpUser.getuserFullName() + "')]");
			if (isOwnerPresent == false) {
				fc.utobj().throwsException("was not able to verify Owner Of Contact");
			}

			boolean isStatusPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[contains(text () , '" + cFname + " " + cLname
					+ "')]/ancestor::tr/td[contains(text () , 'New')]");
			if (isStatusPresent == false) {
				fc.utobj().throwsException("was not able to verify Status of Contact");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "crm"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify the Modify Contact At CRM > Accounts > Contacts", testCaseId = "TC_120_Modify_Contact_Details")
	public void modifyContactDetailsActionImage() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			CRMAccountsPage pobj = new CRMAccountsPage(driver);

			CRMContactsPageTest contactsPage = new CRMContactsPageTest();

			AdminUsersManageRegionalUsersAddRegionalUserPageTest regionalUserPage = new AdminUsersManageRegionalUsersAddRegionalUserPageTest();
			String userNameReg = fc.utobj().generateTestData(dataSet.get("userNameReg"));
			String region = fc.utobj().generateTestData(dataSet.get("regionName"));
			//String emailId = "crmautomation@staffex.com";
			String emailId=fc.utobj().generateTestData("crmautomation")+"@staffex.com";
			regionalUserPage.addRegionalUser(driver, userNameReg, region, emailId);

			AdminUsersManageCorporateUsersAddCorporateUserPageTest addCorporatePage = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			String userName = fc.utobj().generateTestData(dataSet.get("userName"));
			System.out.println(userName);
			fc.utobj().printTestStep("Navigate To Admin Users Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			CorporateUser corpUser = new CorporateUser();
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corpUser.setEmail(emailId);
			corpUser = addCorporatePage.createDefaultUser(driver, corpUser);
			String Corp_user_name =corpUser.getFirstName()+" "+corpUser.getLastName();
			System.out.println(Corp_user_name);
			fc.utobj().printTestStep("Navigating to Admin > CRM > ContactType Configuration");
			fc.utobj().printTestStep("Add Contact Type");
			AdminCRMContactTypeConfigurationPageTest contactTypePage = new AdminCRMContactTypeConfigurationPageTest();
			String contactType = fc.utobj().generateTestData(dataSet.get("contactType"));
			contactTypePage.addContactType(driver, contactType);

			AdminCRMConfigureIndustryPageTest industryPage = new AdminCRMConfigureIndustryPageTest();
			String industry = fc.utobj().generateTestData(dataSet.get("industry"));
			String description = fc.utobj().generateTestData(dataSet.get("description"));
			fc.utobj().printTestStep("Navigating to Admin > CRM >  Configure Industry Type");
			fc.utobj().printTestStep("Add Industry");
			industryPage.addIndustry(driver, industry, description);

			String accountName = fc.utobj().generateTestData(dataSet.get("accountName"));
			String visibility = dataSet.get("visibility");
			fc.utobj().printTestStep("Navigating to CRM >  Accounts > Accounts Summary");
			fc.utobj().printTestStep("Add Account");
			addAcocunt(driver, industry, accountName, visibility);

			fc.utobj().printTestStep("Navigating to CRM >  Accounts > Accounts Summary");
			fc.utobj().printTestStep("Add Contact");

			fc.utobj().clickElement(driver, pobj.accountsLink);
			searchAccountByIndustry(driver, industry);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//a[.='" + accountName + "']"));
			fc.utobj().clickElement(driver, pobj.contactsTab);
			fc.utobj().clickElement(driver, pobj.addContact);

			String cFname = fc.utobj().generateTestData(dataSet.get("cFname"));
			String cLname = fc.utobj().generateTestData(dataSet.get("cLname"));
			String primaryCMethod = dataSet.get("primaryCMethod");
			String assignTo = "Corporate";
			String regionName = "";
			String franchiseId = "";
			String title = dataSet.get("title");
			//String email = "crmautomation@staffex.com";
			String email=fc.utobj().generateTestData("crmautomation")+"@staffex.com";
			contactsPage.addContactGenericWithAccount(driver, contactType, cFname, cLname, primaryCMethod, assignTo,
					Corp_user_name, regionName, franchiseId, title, email, config, accountName);

			fc.utobj().clickElement(driver, pobj.accountsLink);
			try {
				fc.utobj().selectDropDown(driver, pobj.resultsPerPage, "500");
			} catch (Exception e) {
			}

			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//a[.='" + accountName + "']"));
			fc.utobj().clickElement(driver, pobj.contactsTab);
			/*
			 * try { fc.utobj().selectDropDown(driver, pobj.resultsPerPage,
			 * "500"); } catch (Exception e) { }
			 */

			fc.utobj().printTestStep("Modify Contact");
			fc.utobj().actionImgOption(driver, cFname + " " + cLname, "Modify Details");

			cFname = fc.utobj().generateTestData(dataSet.get("cFname"));
			cLname = fc.utobj().generateTestData(dataSet.get("cLname"));

			fc.utobj().sendKeys(driver, pobj.contactFirstName, cFname);
			fc.utobj().sendKeys(driver, pobj.contactLastName, cLname);

			if (!fc.utobj().isSelected(driver, pobj.assignToRegional)) {
				fc.utobj().clickElement(driver, pobj.assignToRegional);
			}

			fc.utobj().selectDropDown(driver, pobj.selectAreaRegion, region);
			fc.utobj().selectDropDown(driver, pobj.selectRegionalUser, userNameReg + " " + userNameReg);
			fc.utobj().clickElement(driver, pobj.saveBtn);

			fc.utobj().clickElement(driver, pobj.accountsLink);
			try {
				fc.utobj().selectDropDown(driver, pobj.resultsPerPage, "500");
			} catch (Exception e) {
			}

			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//a[.='" + accountName + "']"));
			fc.utobj().clickElement(driver, pobj.contactsTab);
			/*
			 * try { fc.utobj().selectDropDown(driver, pobj.resultsPerPage,
			 * "500"); } catch (Exception e) { }
			 */

			fc.utobj().printTestStep("Verify Modify Contact Details");
			boolean isContactNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () , '" + cFname + " " + cLname + "')]");
			if (isContactNamePresent == false) {
				fc.utobj().throwsException("was not able to verify name of contact");
			}
			boolean isContactTypePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[contains(text () , '" + cFname + " "
					+ cLname + "')]/ancestor::tr/td[contains(text () , '" + contactType + "')]");
			if (isContactTypePresent == false) {
				fc.utobj().throwsException("was not able to verify Contact type");
			}
			boolean isOwnerPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[contains(text () , '" + cFname + " " + cLname
					+ "')]/ancestor::tr/td[contains(text () , '" + userNameReg + " " + userNameReg + "')]");
			if (isOwnerPresent == false) {
				fc.utobj().throwsException("was not able to verify Owner Of Contact");
			}

			boolean isStatusPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[contains(text () , '" + cFname + " " + cLname
					+ "')]/ancestor::tr/td[contains(text () , 'New')]");
			if (isStatusPresent == false) {
				fc.utobj().throwsException("was not able to verify Status of Contact");
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "crm" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify the Log A Task By Action Image Option At CRM > Accounts > Contacts", testCaseId = "TC_121_Log_A_Task")
	public void logATaskActionImage() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			CRMAccountsPage pobj = new CRMAccountsPage(driver);

			CRMContactsPageTest contactsPage = new CRMContactsPageTest();
			fc.utobj().printTestStep("Navigate To Admin Users Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			String emailId = "crmautomation@staffex.com";
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUser = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporateUser.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);
			fc.utobj().printTestStep("Navigating to Admin > CRM > ContactType Configuration");
			fc.utobj().printTestStep("Add Contact Type");
			AdminCRMContactTypeConfigurationPageTest contactTypePage = new AdminCRMContactTypeConfigurationPageTest();
			String contactType = fc.utobj().generateTestData(dataSet.get("contactType"));
			contactTypePage.addContactType(driver, contactType);

			AdminCRMConfigureIndustryPageTest industryPage = new AdminCRMConfigureIndustryPageTest();
			String industry = fc.utobj().generateTestData(dataSet.get("industry"));
			String description = fc.utobj().generateTestData(dataSet.get("description"));
			fc.utobj().printTestStep("Navigating to Admin > CRM >  Configure Industry Type");
			fc.utobj().printTestStep("Add Industry");
			industryPage.addIndustry(driver, industry, description);

			String accountName = fc.utobj().generateTestData(dataSet.get("accountName"));
			String visibility = dataSet.get("visibility");
			fc.utobj().printTestStep("Navigating to CRM >  Accounts > Accounts Summary");
			fc.utobj().printTestStep("Add Account");
			addAcocunt(driver, industry, accountName, visibility);

			fc.utobj().printTestStep("Navigating to CRM >  Accounts > Accounts Summary");
			fc.utobj().printTestStep("Add Contact");

			fc.utobj().clickElement(driver, pobj.accountsLink);
			searchAccountByIndustry(driver, industry);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//a[.='" + accountName + "']"));
			fc.utobj().clickElement(driver, pobj.contactsTab);
			fc.utobj().clickElement(driver, pobj.addContact);

			String cFname = fc.utobj().generateTestData(dataSet.get("cFname"));
			String cLname = fc.utobj().generateTestData(dataSet.get("cLname"));
			String primaryCMethod = dataSet.get("primaryCMethod");
			String assignTo = "Corporate";
			String regionName = "";
			String franchiseId = "";
			String title = dataSet.get("title");
			String email = "crmautomation@staffex.com";

			contactsPage.addContactGenericWithAccount(driver, contactType, cFname, cLname, primaryCMethod, assignTo,
					corpUser.getuserFullName(), regionName, franchiseId, title, email, config, accountName);

			fc.utobj().clickElement(driver, pobj.accountsLink);
			searchAccountByIndustry(driver, industry);
			// fc.utobj().clickElement(driver,
			// fc.utobj().getElementByXpath(driver,".//a[.='"+accountName+"']")));
			fc.utobj().clickLink(driver, accountName);
			fc.utobj().clickElement(driver, pobj.contactsTab);

			fc.utobj().printTestStep("Log A Task");
			fc.utobj().actionImgOption(driver, cFname + " " + cLname, "Log a Task");

			fc.commonMethods().switch_cboxIframe_frameId(driver);
			if (!fc.utobj().isSelected(driver,pobj.radioOwner)) {
				fc.utobj().clickElement(driver, pobj.radioOwner);
			}
			String statusTask = dataSet.get("statusTask");
			fc.utobj().selectDropDown(driver, pobj.statusTask, statusTask);
			String subject = fc.utobj().generateTestData(dataSet.get("subject"));
			fc.utobj().sendKeys(driver, pobj.subject, subject);
			if (!fc.utobj().isSelected(driver, pobj.timeLessTask)) {
				fc.utobj().clickElement(driver, pobj.timeLessTask);
			}
			String priority = dataSet.get("priority");
			fc.utobj().selectDropDown(driver, pobj.priority, priority);
			fc.utobj().sendKeys(driver, pobj.comments, "Test Comment");
			fc.utobj().clickElement(driver, pobj.createBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//a[contains(text () ,'" + cFname + " " + cLname + "')]"));

			fc.utobj().printTestStep("Verify Log A task");
			boolean isSubjectPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[contains(text () ,'" + subject + "')]");
			if (isSubjectPresent == false) {
				fc.utobj().throwsException("was not able to verify subject");
			}
			boolean isUserPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[contains(text () ,'" + subject
					+ "')]/ancestor::tr/td/div[.='" + corpUser.getuserFullName() + "']");
			if (isUserPresent == false) {
				fc.utobj().throwsException("was not able to verify userName at Lead Info Page");
			}
			boolean isStatusOfTaskPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[contains(text () ,'" + subject
					+ "')]/ancestor::tr/td[contains(text () ,'" + statusTask + "')]");
			if (isStatusOfTaskPresent == false) {
				fc.utobj().throwsException("was not able to verify status of task");
			}
			boolean isPriorityPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[contains(text () ,'" + subject
					+ "')]/ancestor::tr/td[contains(text () ,'" + priority + "')]");
			if (isPriorityPresent == false) {
				fc.utobj().throwsException("was not able to verify Priority of task");
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "crm"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2017-06-27", testCaseDescription = "Verify the Log A Call By Action Image Options At CRM > Accounts > Contacts", testCaseId = "TC_122_Log_A_Call")
	public void logACallActionImage() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			CRMAccountsPage pobj = new CRMAccountsPage(driver);

			CRMContactsPageTest contactsPage = new CRMContactsPageTest();

			fc.utobj().printTestStep("Navigate To Admin Users Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			String emailId = "crmautomation@staffex.com";
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUser = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporateUser.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);
			fc.utobj().printTestStep("Navigating to Admin > CRM > ContactType Configuration");
			fc.utobj().printTestStep("Add Contact Type");
			AdminCRMContactTypeConfigurationPageTest contactTypePage = new AdminCRMContactTypeConfigurationPageTest();
			String contactType = fc.utobj().generateTestData(dataSet.get("contactType"));
			contactTypePage.addContactType(driver, contactType);

			AdminCRMConfigureIndustryPageTest industryPage = new AdminCRMConfigureIndustryPageTest();
			String industry = fc.utobj().generateTestData(dataSet.get("industry"));
			String description = fc.utobj().generateTestData(dataSet.get("description"));
			fc.utobj().printTestStep("Navigating to Admin > CRM >  Configure Industry Type");
			fc.utobj().printTestStep("Add Industry");
			industryPage.addIndustry(driver, industry, description);

			String accountName = fc.utobj().generateTestData(dataSet.get("accountName"));
			String visibility = dataSet.get("visibility");
			fc.utobj().printTestStep("Navigating to CRM >  Accounts > Accounts Summary");
			fc.utobj().printTestStep("Add Account");
			addAcocunt(driver, industry, accountName, visibility);

			fc.utobj().printTestStep("Navigating to CRM >  Accounts > Accounts Summary");
			fc.utobj().printTestStep("Add Contact");

			fc.utobj().clickElement(driver, pobj.accountsLink);
			searchAccountByIndustry(driver, industry);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//a[.='" + accountName + "']"));
			fc.utobj().clickElement(driver, pobj.contactsTab);
			fc.utobj().clickElement(driver, pobj.addContact);

			String cFname = fc.utobj().generateTestData(dataSet.get("cFname"));
			String cLname = fc.utobj().generateTestData(dataSet.get("cLname"));
			String primaryCMethod = dataSet.get("primaryCMethod");
			String assignTo = "Corporate";
			String regionName = "";
			String franchiseId = "";
			String title = dataSet.get("title");
			String email = "crmautomation@staffex.com";

			contactsPage.addContactGenericWithAccount(driver, contactType, cFname, cLname, primaryCMethod, assignTo,
					corpUser.getuserFullName(), regionName, franchiseId, title, email, config, accountName);

			fc.utobj().clickElement(driver, pobj.accountsLink);
			searchAccountByIndustry(driver, industry);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//a[.='" + accountName + "']"));
			fc.utobj().clickElement(driver, pobj.contactsTab);

			try {
				fc.utobj().selectDropDown(driver, pobj.resultsPerPage, "500");
			} catch (Exception e) {
			}

			fc.utobj().printTestStep("Log A Call");
			fc.utobj().actionImgOption(driver, cFname + " " + cLname, "Log a Call");
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			String callSubject = fc.utobj().generateTestData(dataSet.get("subject"));
			fc.utobj().sendKeys(driver, pobj.callSubject, callSubject);
			String callDate = fc.utobj().getCurrentDateAndTime();  //fc.utobj().getCurrentDateUSFormat();
			fc.utobj().sendKeys(driver, pobj.callDate, callDate);
			String callStatus = dataSet.get("callStatus");
			fc.utobj().selectDropDown(driver, pobj.callStatus, callStatus);
			String cType = dataSet.get("callType");
			fc.utobj().selectDropDown(driver, pobj.communicationType, cType);
			fc.utobj().sendKeys(driver, pobj.comments, "Test Comment");
			fc.utobj().clickElement(driver, pobj.saveBtn);
			fc.utobj().clickElement(driver, pobj.noBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//a[contains(text () ,'" + cFname + " " + cLname + "')]"));
			  String activehst = driver.findElement(By.id("ActivityHis")).getText();
				
				
				if (activehst.contains(callSubject))  {
					fc.utobj().printTestStep("call found in activity history");
					
				}else 
				{
					fc.utobj().printTestStep("Call not found in activity history");
				}
			
			/*fc.utobj().clickElement(driver, pobj.detailedHistoryLink);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().clickElement(driver, pobj.callsTabDH);

			fc.utobj().printTestStep("Verify Log A Call At Detailed History Frame");
			boolean isCallSubjectPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () ,'" + callSubject + "')]");
			if (isCallSubjectPresent == false) {
				fc.utobj().throwsException("was not able to verify Call Subject");
			}
			boolean isCommunicationTypePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () ,'" + cType + "')]");
			if (isCommunicationTypePresent == false) {
				fc.utobj().throwsException("was not able to verify communication type");
			}
			fc.utobj().clickElement(driver, pobj.closeBtn);
			fc.utobj().switchFrameToDefault(driver);*/

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = {"crm"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Add Remarks By Action Imgae Option At CRM > Account > Contact", testCaseId = "TC_124_Add_Remarks")
	public void addRemarksActionImage() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			CRMAccountsPage pobj = new CRMAccountsPage(driver);

			CRMContactsPageTest contactsPage = new CRMContactsPageTest();

			fc.utobj().printTestStep("Navigate To Admin Users Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			String emailId = "crmautomation@staffex.com";
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUser = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporateUser.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			fc.utobj().printTestStep("Navigating to Admin > CRM > ContactType Configuration");
			fc.utobj().printTestStep("Add Contact Type");
			AdminCRMContactTypeConfigurationPageTest contactTypePage = new AdminCRMContactTypeConfigurationPageTest();
			String contactType = fc.utobj().generateTestData(dataSet.get("contactType"));
			contactTypePage.addContactType(driver, contactType);

			/*
			 * fc.utobj().printTestStep(testCaseId,
			 * "Navigating to Admin > CRM > Configure Lead Type");
			 * fc.utobj().printTestStep(testCaseId, "Add Lead Type");
			 * AdminCRMConfigureLeadTypePageTest leadTypePage=new
			 * AdminCRMConfigureLeadTypePageTest(); String
			 * leadType=fc.utobj().generateTestData(dataSet.get("leadType"));
			 * leadTypePage.addLeadType(driver, leadType, config);
			 * fc.utobj().printTestStep(testCaseId,
			 * "Navigating to Admin > CRM > Configure Medium");
			 * fc.utobj().printTestStep(testCaseId, "Add Medium");
			 * AdminCRMConfigureMediumPageTest mediumPage=new
			 * AdminCRMConfigureMediumPageTest(); String
			 * medium=fc.utobj().generateTestData(dataSet.get("medium")); String
			 * description=fc.utobj().generateTestData(dataSet.get("description"
			 * )); mediumPage.addMedium(driver, medium, description, config);
			 * 
			 * AdminCRMSourceSummaryPageTest addSourcePage = new
			 * AdminCRMSourceSummaryPageTest(); String source =
			 * fc.utobj().generateTestData(dataSet.get("source")); description =
			 * fc.utobj().generateTestData(dataSet.get("description")); String
			 * displayOnWebPageText =
			 * fc.utobj().generateTestData(dataSet.get("text"));
			 * fc.utobj().printTestStep(testCaseId,
			 * "Navigating to Admin > CRM > Source Summary");
			 * fc.utobj().printTestStep(testCaseId, "Add Source");
			 * addSourcePage.addSource(driver, source, description,
			 * displayOnWebPageText, config);
			 * fc.utobj().printTestStep(testCaseId, "Add Source Details");
			 * String sourceDetails =
			 * fc.utobj().generateTestData(dataSet.get("sourceDetails"));
			 * addSourcePage.addSourceDetails(driver, source, sourceDetails,
			 * displayOnWebPageText, description, config);
			 */

			AdminCRMConfigureIndustryPageTest industryPage = new AdminCRMConfigureIndustryPageTest();
			String industry = fc.utobj().generateTestData(dataSet.get("industry"));
			String description = fc.utobj().generateTestData(dataSet.get("description"));
			fc.utobj().printTestStep("Navigating to Admin > CRM >  Configure Industry Type");
			fc.utobj().printTestStep("Add Industry");
			industryPage.addIndustry(driver, industry, description);

			String accountName = fc.utobj().generateTestData(dataSet.get("accountName"));
			String visibility = dataSet.get("visibility");
			fc.utobj().printTestStep("Navigating to CRM >  Accounts > Accounts Summary");
			fc.utobj().printTestStep("Add Account");
			addAcocunt(driver, industry, accountName, visibility);

			fc.utobj().printTestStep("Navigating to CRM >  Accounts > Accounts Summary");
			fc.utobj().printTestStep("Add Contact");

			fc.utobj().clickElement(driver, pobj.accountsLink);
			searchAccountByIndustry(driver, industry);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//a[.='" + accountName + "']"));
			fc.utobj().clickElement(driver, pobj.contactsTab);
			fc.utobj().clickElement(driver, pobj.addContact);

			String cFname = fc.utobj().generateTestData(dataSet.get("cFname"));
			String cLname = fc.utobj().generateTestData(dataSet.get("cLname"));
			String primaryCMethod = dataSet.get("primaryCMethod");
			String assignTo = "Corporate";
			String regionName = "";
			String franchiseId = "";
			String title = dataSet.get("title");
			String email = "crmautomation@staffex.com";

			contactsPage.addContactGenericWithAccount(driver, contactType, cFname, cLname, primaryCMethod, assignTo,
					corpUser.getuserFullName(), regionName, franchiseId, title, email, config, accountName);

			fc.utobj().clickElement(driver, pobj.accountsLink);
			searchAccountByIndustry(driver, industry);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//a[.='" + accountName + "']"));
			fc.utobj().clickElement(driver, pobj.contactsTab);
			try {
				fc.utobj().selectDropDown(driver, pobj.resultsPerPage, "500");
			} catch (Exception e) {
			}

			fc.utobj().printTestStep("Add Remarks");
			fc.utobj().actionImgOption(driver, cFname + " " + cLname, "Add Remarks");
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			String remarks = fc.utobj().generateTestData(dataSet.get("remark"));
			fc.utobj().sendKeys(driver, pobj.remarks, remarks);
			fc.utobj().clickElement(driver, pobj.addBtn);
			fc.utobj().clickElement(driver, pobj.closeBtn);
			fc.utobj().switchFrameToDefault(driver);
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//a[contains(text () ,'" + cFname + " " + cLname + "')]"));

			fc.utobj().printTestStep("Verify Remarks");
			boolean isRemarksPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[@title='Remark']/span[.='" + remarks + "']");
			if (isRemarksPresent == false) {
				fc.utobj().throwsException("was not able to verify remarks");
			}

			fc.utobj().clickElement(driver, pobj.detailedHistoryLink);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().clickElement(driver, pobj.remarksTabAtCbox);

			fc.utobj().printTestStep("Verify Remarks At Detailed History Frame");
			boolean isRemarksAtCboxPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () ,'" + remarks + "')]");
			if (isRemarksAtCboxPresent == false) {
				fc.utobj().throwsException("was not able to verify Remarks At Detailed History Cbox");
			}
			fc.utobj().clickElement(driver, pobj.closeBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	/*
	 * @Test(groups="crm") public void sendEmailActionImage() throws Exception {
	 * 
	 * Reporter.log(
	 * "TC_125_Send_Email : Send Email Action Image Options : CRM > Accounts > Contacts .\n"
	 * ); Reporter.log(
	 * "###########################################################################################"
	 * );
	 * 
	 * String testCaseId = "TC_125_Send_Email";
	 * 
	 * Map<String,String> config =
	 * fc.utobj().readConfigurationFile(FranconnectUtil.path);
	 * Map<String,String> dataSet = fc.utobj().readTestData("crm", testCaseId);
	 * WebDriver driver = fc.utobj().openDriver(config);
	 * 
	 * 
	 * try { driver=fc.loginpage().login(driver, config); CRMAccountsPage
	 * pobj=new CRMAccountsPage(driver);
	 * 
	 * String parentWindowsHandle=driver.getWindowHandle();
	 * 
	 * CRMContactsPageTest contactsPage=new CRMContactsPageTest();
	 * 
	 * AdminUsersManageCorporateUsersAddCorporateUserPageTest addCorporatePage =
	 * new AdminUsersManageCorporateUsersAddCorporateUserPageTest(); String
	 * userName = fc.utobj().generateTestData(dataSet.get("userName")); userName
	 * = addCorporatePage.addCorporateUser(driver, userName, config , emailId);
	 * 
	 * AdminCRMContactTypeConfigurationPageTest contactTypePage=new
	 * AdminCRMContactTypeConfigurationPageTest(); String
	 * contactType=fc.utobj().generateTestData(dataSet.get("contactType"));
	 * contactTypePage.addContactType(driver, contactType, config);
	 * 
	 * AdminCRMConfigureLeadTypePageTest leadTypePage=new
	 * AdminCRMConfigureLeadTypePageTest(); String
	 * leadType=fc.utobj().generateTestData(dataSet.get("leadType"));
	 * leadTypePage.addLeadType(driver, leadType, config);
	 * 
	 * AdminCRMConfigureMediumPageTest mediumPage=new
	 * AdminCRMConfigureMediumPageTest(); String
	 * medium=fc.utobj().generateTestData(dataSet.get("medium")); String
	 * description=fc.utobj().generateTestData(dataSet.get("description"));
	 * mediumPage.addMedium(driver, medium, description, config);
	 * 
	 * AdminCRMSourceSummaryPageTest addSourcePage = new
	 * AdminCRMSourceSummaryPageTest(); String source =
	 * fc.utobj().generateTestData(dataSet.get("source")); description =
	 * fc.utobj().generateTestData(dataSet.get("description")); String
	 * displayOnWebPageText = fc.utobj().generateTestData(dataSet.get("text"));
	 * addSourcePage.addSource(driver, source, description,
	 * displayOnWebPageText, config);
	 * 
	 * String sourceDetails =
	 * fc.utobj().generateTestData(dataSet.get("sourceDetails"));
	 * addSourcePage.addSourceDetails(driver, source, sourceDetails,
	 * displayOnWebPageText, description, config);
	 * 
	 * AdminCRMConfigureIndustryPageTest industryPage=new
	 * AdminCRMConfigureIndustryPageTest(); String
	 * industry=fc.utobj().generateTestData(dataSet.get("industry"));
	 * description=fc.utobj().generateTestData(dataSet.get("description"));
	 * industryPage.addIndustry(driver, industry, description, config);
	 * 
	 * String
	 * accountName=fc.utobj().generateTestData(dataSet.get("accountName"));
	 * String visibility=dataSet.get("visibility"); addAcocunt(driver, industry,
	 * accountName, visibility, config);
	 * 
	 * fc.utobj().clickElement(driver, pobj.accountsLink);
	 * searchAccountByIndustry(driver, industry);
	 * fc.utobj().clickElement(driver,
	 * fc.utobj().getElementByXpath(driver,".//a[.='"+accountName+"']")));
	 * fc.utobj().clickElement(driver, pobj.contactsTab);
	 * fc.utobj().clickElement(driver, pobj.addContact);
	 * 
	 * String cFname=fc.utobj().generateTestData(dataSet.get("cFname")); String
	 * cLname=fc.utobj().generateTestData(dataSet.get("cLname")); String
	 * primaryCMethod=dataSet.get("primaryCMethod"); String
	 * assignTo="Corporate"; String regionName=""; String franchiseId=""; String
	 * title=dataSet.get("title"); String email="crmautomation@staffex.com";
	 * 
	 * contactsPage.addContactGenericWithAccount(driver, contactType, cFname,
	 * cLname, primaryCMethod, leadType, medium, source, sourceDetails,
	 * assignTo, userName, regionName, franchiseId, title, email, config ,
	 * accountName);
	 * 
	 * //fc.utobj().sendKeys(driver, pobj.accountName, accountName);
	 * //fc.utobj().clickElement(driver, pobj.saveBtn);
	 * 
	 * fc.utobj().clickElement(driver, pobj.accountsLink);
	 * searchAccountByIndustry(driver, industry);
	 * fc.utobj().clickElement(driver,
	 * fc.utobj().getElementByXpath(driver,".//a[.='"+accountName+"']")));
	 * fc.utobj().clickElement(driver, pobj.contactsTab); try {
	 * fc.utobj().selectDropDown(driver, pobj.resultsPerPage, "500"); } catch
	 * (Exception e) { }
	 * 
	 * fc.utobj().actionImgOption(driver, cFname+" "+cLname, "Send Email");
	 * 
	 * if (!fc.utobj().isSelected(driver,
	 * pobj.leadOwnerId)) { fc.utobj().clickElement(driver,
	 * pobj.leadOwnerId); }
	 * 
	 * String mailCC = dataSet.get("mailCC"); fc.utobj().sendKeys(driver,
	 * pobj.mailCC, mailCC);
	 * 
	 * if (!pobj.showBcc) { fc.utobj().clickElement(driver,
	 * pobj.showBcc); } String mailBCC = dataSet.get("mailBCC");
	 * fc.utobj().sendKeys(driver, pobj.mailBCC, mailBCC);
	 * 
	 * String subjectMail =
	 * fc.utobj().generateTestData(dataSet.get("subjectMail"));
	 * fc.utobj().sendKeys(driver, pobj.subjectMail, subjectMail);
	 * 
	 * String editorText = fc.utobj().generateTestData(dataSet.get("mailText"));
	 * 
	 * fc.utobj().switchFrameById(driver, "ta_ifr");
	 * 
	 * Actions actions = new Actions(driver);
	 * actions.moveToElement(fc.utobj().isSelected(driver,
	 * pobj.editorTextArea)); actions.click(); actions.sendKeys(editorText);
	 * fc.utobj().logReport("Entering Value in Text Field",
	 * pobj.editorTextArea,driver); fc.utobj().logReportMsg("Entered Text",
	 * editorText); actions.build().perform();
	 * fc.utobj().switchFrameToDefault(driver); fc.utobj().clickElement(driver,
	 * pobj.sendEmailBtn); fc.utobj().switchFrameToDefault(driver);
	 * 
	 * fc.utobj().clickElement(driver, pobj.accountsLink);
	 * searchAccountByIndustry(driver, industry);
	 * fc.utobj().clickElement(driver,
	 * fc.utobj().getElementByXpath(driver,".//a[.='"+accountName+"']")));
	 * fc.utobj().clickElement(driver, pobj.contactsTab);
	 * fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
	 * ".//a[contains(text () , '"+cFname+" "+cLname+"')]")));
	 * fc.utobj().clickElement(driver, pobj.detailedHistoryLink);
	 * fc.commonMethods().switch_cboxIframe_frameId(driver);
	 * fc.utobj().clickElement(driver, pobj.emailsTab);
	 * fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
	 * ".//*[contains(text () ,'"+cFname+"')]")));
	 * 
	 * Set<String> allWindows=driver.getWindowHandles(); for (String
	 * currentWindow : allWindows) {
	 * 
	 * if (!currentWindow.equalsIgnoreCase(parentWindowsHandle)) {
	 * 
	 * driver.switchTo().window(currentWindow); try {
	 * 
	 * if (fc.utobj().getElementByXpath(driver,".//*[.='Email Details']"
	 * )).isDisplayed()) {
	 * 
	 * boolean isFromTextPresent=fc.utobj().verifyCase(driver,
	 * ".//*[contains(text () ,'From')]/ancestor::tr/td[contains(text () , '"
	 * +userName+"')]"); if (isFromTextPresent==false) {
	 * fc.utobj().throwsException("was not able to From Text"); }
	 * 
	 * boolean isToTextPresent=fc.utobj().verifyCase(driver,
	 * ".//*[contains(text () ,'To')]/ancestor::tr/td[contains(text () , '"
	 * +cFname+" "+cLname+"')]"); if (isToTextPresent==false) {
	 * fc.utobj().throwsException("was not able to To Text"); } boolean
	 * isSubjectTextPresent=fc.utobj().verifyCase(driver,
	 * ".//*[contains(text () ,'To')]/ancestor::tr/td[contains(text () , '"
	 * +subjectMail+"')]"); if (isSubjectTextPresent==false) {
	 * fc.utobj().throwsException("was not able to Subject"); } boolean
	 * isTextPresent=fc.utobj().verifyCase(driver, ".//*[.='"+editorText+"']");
	 * if (isTextPresent==false) { fc.utobj().throwsException(
	 * "was not able to verify Editor text"); }
	 * 
	 * }
	 * 
	 * } catch (Exception e) { driver.close(); } }
	 * driver.switchTo().window(parentWindowsHandle); }
	 * 
	 * fc.utobj().clickElement(driver, pobj.closeBtn);
	 * 
	 * 
	 * fc.utobj().logoutAndQuitBrowser(driver, config, testCaseId); } catch
	 * (Exception e) {
	 * 
	 * fc.utobj().quitBrowserOnCatch(driver, config, e, testCaseId);} }
	 */

	@Test(groups = { "crm" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Log A Task By Action Btn Option At CRM > Accounts > Contacts", testCaseId = "TC_126_Log_A_Task")
	public void logATaskActionBtn() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			CRMAccountsPage pobj = new CRMAccountsPage(driver);
			CRMContactsPageTest contactsPage = new CRMContactsPageTest();
			fc.utobj().printTestStep("Navigate To Admin Users Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			String emailId = "crmautomation@staffex.com";
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUser = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporateUser.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			fc.utobj().printTestStep("Navigating to Admin > CRM > ContactType Configuration");
			fc.utobj().printTestStep("Add Contact Type");
			AdminCRMContactTypeConfigurationPageTest contactTypePage = new AdminCRMContactTypeConfigurationPageTest();
			String contactType = fc.utobj().generateTestData(dataSet.get("contactType"));
			contactTypePage.addContactType(driver, contactType);

			AdminCRMConfigureIndustryPageTest industryPage = new AdminCRMConfigureIndustryPageTest();
			String industry = fc.utobj().generateTestData(dataSet.get("industry"));
			String description = fc.utobj().generateTestData(dataSet.get("description"));
			fc.utobj().printTestStep("Navigating to Admin > CRM >  Configure Industry Type");
			fc.utobj().printTestStep("Add Industry");
			industryPage.addIndustry(driver, industry, description);

			String accountName = fc.utobj().generateTestData(dataSet.get("accountName"));
			String visibility = dataSet.get("visibility");
			fc.utobj().printTestStep("Navigating to CRM >  Accounts > Accounts Summary");
			fc.utobj().printTestStep("Add Account");
			addAcocunt(driver, industry, accountName, visibility);

			fc.utobj().printTestStep("Navigating to CRM >  Accounts > Accounts Summary");
			fc.utobj().printTestStep("Add Contact");

			fc.utobj().clickElement(driver, pobj.accountsLink);
			searchAccountByIndustry(driver, industry);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//a[.='" + accountName + "']"));
			fc.utobj().clickElement(driver, pobj.contactsTab);
			fc.utobj().clickElement(driver, pobj.addContact);

			String cFname = fc.utobj().generateTestData(dataSet.get("cFname"));
			String cLname = fc.utobj().generateTestData(dataSet.get("cLname"));
			String primaryCMethod = dataSet.get("primaryCMethod");
			String assignTo = "Corporate";
			String regionName = "";
			String franchiseId = "";
			String title = dataSet.get("title");
			String email = "crmautomation@staffex.com";

			contactsPage.addContactGenericWithAccount(driver, contactType, cFname, cLname, primaryCMethod, assignTo,
					corpUser.getuserFullName(), regionName, franchiseId, title, email, config, accountName);

			fc.utobj().clickElement(driver, pobj.accountsLink);
			searchAccountByIndustry(driver, industry);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//a[.='" + accountName + "']"));
			fc.utobj().clickElement(driver, pobj.contactsTab);
			try {
				fc.utobj().selectDropDown(driver, pobj.resultsPerPage, "500");
			} catch (Exception e) {
			}
			// searchContactByContactType(driver, contactType);

			fc.utobj().printTestStep("Log A Task");
			
			fc.utobj().clickElement(driver, pobj.selectAllCheckBox);
			fc.utobj().selectActionMenuItemsWithTagA(driver, "Log a Task");
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			if (!fc.utobj().isSelected(driver, pobj.radioOwner)) {
				fc.utobj().clickElement(driver, pobj.radioOwner);
			}
			String statusTask = dataSet.get("statusTask");
			fc.utobj().selectDropDown(driver, pobj.statusTask, statusTask);
			String subject = fc.utobj().generateTestData(dataSet.get("subject"));
			fc.utobj().sendKeys(driver, pobj.subject, subject);
			if (!fc.utobj().isSelected(driver, pobj.timeLessTask)) {
				fc.utobj().clickElement(driver, pobj.timeLessTask);
			}
			String priority = dataSet.get("priority");
			fc.utobj().selectDropDown(driver, pobj.priority, priority);
			fc.utobj().sendKeys(driver, pobj.comments, "Test Comment");
			fc.utobj().clickElement(driver, pobj.createBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//a[contains(text () ,'" + cFname + " " + cLname + "')]"));

			fc.utobj().printTestStep("Verify Log A Task");
			boolean isSubjectPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[contains(text () ,'" + subject + "')]");
			if (isSubjectPresent == false) {
				fc.utobj().throwsException("was not able to verify subject");
			}
			boolean isUserPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[contains(text () ,'" + subject
					+ "')]/ancestor::tr/td/div[.='" + corpUser.getuserFullName() + "']");
			if (isUserPresent == false) {
				fc.utobj().throwsException("was not able to verify userName at Lead Info Page");
			}
			boolean isStatusOfTaskPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[contains(text () ,'" + subject
					+ "')]/ancestor::tr/td[contains(text () ,'" + statusTask + "')]");
			if (isStatusOfTaskPresent == false) {
				fc.utobj().throwsException("was not able to verify status of task");
			}
			boolean isPriorityPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[contains(text () ,'" + subject
					+ "')]/ancestor::tr/td[contains(text () ,'" + priority + "')]");
			if (isPriorityPresent == false) {
				fc.utobj().throwsException("was not able to verify Priority of task");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	/*
	 * @Test(groups="crm") public void sendEmailActionBtn() throws Exception {
	 * 
	 * Reporter.log(
	 * "TC_127_Send_Email : Send Email Action Btn Options : CRM > Accounts > Contacts .\n"
	 * ); Reporter.log(
	 * "###########################################################################################"
	 * );
	 * 
	 * String testCaseId = "TC_127_Send_Email";
	 * 
	 * Map<String,String> config =
	 * fc.utobj().readConfigurationFile(FranconnectUtil.path);
	 * Map<String,String> dataSet = fc.utobj().readTestData("crm", testCaseId);
	 * WebDriver driver = fc.utobj().openDriver(config);
	 * 
	 * 
	 * try { driver=fc.loginpage().login(driver, config); CRMAccountsPage
	 * pobj=new CRMAccountsPage(driver);
	 * 
	 * String parentWindowsHandle=driver.getWindowHandle();
	 * 
	 * CRMContactsPageTest contactsPage=new CRMContactsPageTest();
	 * 
	 * AdminUsersManageCorporateUsersAddCorporateUserPageTest addCorporatePage =
	 * new AdminUsersManageCorporateUsersAddCorporateUserPageTest(); String
	 * userName = fc.utobj().generateTestData(dataSet.get("userName")); userName
	 * = addCorporatePage.addCorporateUser(driver, userName, config , emailId);
	 * 
	 * AdminCRMContactTypeConfigurationPageTest contactTypePage=new
	 * AdminCRMContactTypeConfigurationPageTest(); String
	 * contactType=fc.utobj().generateTestData(dataSet.get("contactType"));
	 * contactTypePage.addContactType(driver, contactType, config);
	 * 
	 * AdminCRMConfigureLeadTypePageTest leadTypePage=new
	 * AdminCRMConfigureLeadTypePageTest(); String
	 * leadType=fc.utobj().generateTestData(dataSet.get("leadType"));
	 * leadTypePage.addLeadType(driver, leadType, config);
	 * 
	 * AdminCRMConfigureMediumPageTest mediumPage=new
	 * AdminCRMConfigureMediumPageTest(); String
	 * medium=fc.utobj().generateTestData(dataSet.get("medium")); String
	 * description=fc.utobj().generateTestData(dataSet.get("description"));
	 * mediumPage.addMedium(driver, medium, description, config);
	 * 
	 * AdminCRMSourceSummaryPageTest addSourcePage = new
	 * AdminCRMSourceSummaryPageTest(); String source =
	 * fc.utobj().generateTestData(dataSet.get("source")); description =
	 * fc.utobj().generateTestData(dataSet.get("description")); String
	 * displayOnWebPageText = fc.utobj().generateTestData(dataSet.get("text"));
	 * addSourcePage.addSource(driver, source, description,
	 * displayOnWebPageText, config);
	 * 
	 * String sourceDetails =
	 * fc.utobj().generateTestData(dataSet.get("sourceDetails"));
	 * addSourcePage.addSourceDetails(driver, source, sourceDetails,
	 * displayOnWebPageText, description, config);
	 * 
	 * AdminCRMConfigureIndustryPageTest industryPage=new
	 * AdminCRMConfigureIndustryPageTest(); String
	 * industry=fc.utobj().generateTestData(dataSet.get("industry"));
	 * description=fc.utobj().generateTestData(dataSet.get("description"));
	 * industryPage.addIndustry(driver, industry, description, config);
	 * 
	 * String
	 * accountName=fc.utobj().generateTestData(dataSet.get("accountName"));
	 * String visibility=dataSet.get("visibility"); addAcocunt(driver, industry,
	 * accountName, visibility, config);
	 * 
	 * fc.utobj().clickElement(driver, pobj.accountsLink);
	 * searchAccountByIndustry(driver, industry);
	 * fc.utobj().clickElement(driver,
	 * fc.utobj().getElementByXpath(driver,".//a[.='"+accountName+"']")));
	 * fc.utobj().clickElement(driver, pobj.contactsTab);
	 * fc.utobj().clickElement(driver, pobj.addContact);
	 * 
	 * String cFname=fc.utobj().generateTestData(dataSet.get("cFname")); String
	 * cLname=fc.utobj().generateTestData(dataSet.get("cLname")); String
	 * primaryCMethod=dataSet.get("primaryCMethod"); String
	 * assignTo="Corporate"; String regionName=""; String franchiseId=""; String
	 * title=dataSet.get("title"); String email="crmautomation@staffex.com";
	 * 
	 * contactsPage.addContactGenericWithAccount(driver, contactType, cFname,
	 * cLname, primaryCMethod, leadType, medium, source, sourceDetails,
	 * assignTo, userName, regionName, franchiseId, title, email, config ,
	 * accountName);
	 * 
	 * //fc.utobj().sendKeys(driver, pobj.accountName, accountName);
	 * //fc.utobj().clickElement(driver, pobj.saveBtn);
	 * 
	 * fc.utobj().clickElement(driver, pobj.accountsLink);
	 * searchAccountByIndustry(driver, industry);
	 * fc.utobj().clickElement(driver,
	 * fc.utobj().getElementByXpath(driver,".//a[.='"+accountName+"']")));
	 * fc.utobj().clickElement(driver, pobj.contactsTab); try {
	 * fc.utobj().selectDropDown(driver, pobj.resultsPerPage, "500"); } catch
	 * (Exception e) { }
	 * 
	 * fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
	 * ".//a[contains(text () ,'"+cFname+" "
	 * +cLname+"')]/ancestor::tr/td/input[@name='checkb']")));
	 * fc.utobj().selectActionMenuItemsWithTagA(driver, "Send Email");
	 * 
	 * if (!fc.utobj().isSelected(driver,
	 * pobj.leadOwnerId)) { fc.utobj().clickElement(driver,
	 * pobj.leadOwnerId); }
	 * 
	 * String mailCC = dataSet.get("mailCC"); fc.utobj().sendKeys(driver,
	 * pobj.mailCC, mailCC);
	 * 
	 * if (!pobj.showBcc) { fc.utobj().clickElement(driver,
	 * pobj.showBcc); } String mailBCC = dataSet.get("mailBCC");
	 * fc.utobj().sendKeys(driver, pobj.mailBCC, mailBCC);
	 * 
	 * String subjectMail =
	 * fc.utobj().generateTestData(dataSet.get("subjectMail"));
	 * fc.utobj().sendKeys(driver, pobj.subjectMail, subjectMail);
	 * 
	 * String editorText = fc.utobj().generateTestData(dataSet.get("mailText"));
	 * 
	 * fc.utobj().switchFrameById(driver, "ta_ifr");
	 * 
	 * Actions actions = new Actions(driver);
	 * actions.moveToElement(fc.utobj().isSelected(driver,
	 * pobj.editorTextArea)); actions.click(); actions.sendKeys(editorText);
	 * fc.utobj().logReport("Entering Value in Text Field",
	 * pobj.editorTextArea,driver); fc.utobj().logReportMsg("Entered Text",
	 * editorText); actions.build().perform();
	 * fc.utobj().switchFrameToDefault(driver); fc.utobj().clickElement(driver,
	 * pobj.sendEmailBtn); fc.utobj().switchFrameToDefault(driver);
	 * 
	 * fc.utobj().webDriverWait(driver,
	 * ".//a[@original-title='Manage Account details associated with contacts and leads']"
	 * );
	 * 
	 * fc.utobj().clickElement(driver, pobj.accountsLink);
	 * searchAccountByIndustry(driver, industry);
	 * fc.utobj().clickElement(driver,
	 * fc.utobj().getElementByXpath(driver,".//a[.='"+accountName+"']")));
	 * fc.utobj().clickElement(driver, pobj.contactsTab);
	 * fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
	 * ".//a[contains(text () , '"+cFname+" "+cLname+"']")));
	 * fc.utobj().clickElement(driver, pobj.detailedHistoryLink);
	 * fc.commonMethods().switch_cboxIframe_frameId(driver);
	 * fc.utobj().clickElement(driver, pobj.emailsTab);
	 * fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
	 * ".//*[contains(text () ,'"+cFname+"')]")));
	 * 
	 * Set<String> allWindows=driver.getWindowHandles(); for (String
	 * currentWindow : allWindows) {
	 * 
	 * if (!currentWindow.equalsIgnoreCase(parentWindowsHandle)) {
	 * 
	 * driver.switchTo().window(currentWindow); try {
	 * 
	 * if (fc.utobj().getElementByXpath(driver,".//*[.='Email Details']"
	 * )).isDisplayed()) {
	 * 
	 * boolean isFromTextPresent=fc.utobj().verifyCase(driver,
	 * ".//*[contains(text () ,'From')]/ancestor::tr/td[contains(text () , '"
	 * +userName+"')]"); if (isFromTextPresent==false) {
	 * fc.utobj().throwsException("was not able to From Text"); }
	 * 
	 * boolean isToTextPresent=fc.utobj().verifyCase(driver,
	 * ".//*[contains(text () ,'To')]/ancestor::tr/td[contains(text () , '"
	 * +cFname+" "+cLname+"')]"); if (isToTextPresent==false) {
	 * fc.utobj().throwsException("was not able to To Text"); } boolean
	 * isSubjectTextPresent=fc.utobj().verifyCase(driver,
	 * ".//*[contains(text () ,'To')]/ancestor::tr/td[contains(text () , '"
	 * +subjectMail+"')]"); if (isSubjectTextPresent==false) {
	 * fc.utobj().throwsException("was not able to Subject"); } boolean
	 * isTextPresent=fc.utobj().verifyCase(driver, ".//*[.='"+editorText+"']");
	 * if (isTextPresent==false) { fc.utobj().throwsException(
	 * "was not able to verify Editor text"); }
	 * 
	 * }
	 * 
	 * } catch (Exception e) { driver.close(); } }
	 * driver.switchTo().window(parentWindowsHandle); }
	 * 
	 * fc.utobj().clickElement(driver, pobj.closeBtn);
	 * fc.utobj().switchFrameToDefault(driver);
	 * 
	 * 
	 * fc.utobj().logoutAndQuitBrowser(driver, config, testCaseId); } catch
	 * (Exception e) {
	 * 
	 * fc.utobj().quitBrowserOnCatch(driver, config, e, testCaseId);} }
	 */

	@Test(groups = { "crm"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify the Merge Contacts At CRM > Accounts > Contacts", testCaseId = "TC_128_Merge_Contacts")
	public void mergeContactsActionBtn() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			CRMAccountsPage pobj = new CRMAccountsPage(driver);

			CRMContactsPageTest contactsPage = new CRMContactsPageTest();
			fc.utobj().printTestStep("Navigate To Admin Users Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			String emailId = fc.utobj().generateTestData("crmautomation")+"@staffex.com";//"crmautomation@staffex.com";//vipinS
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUser = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporateUser.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			fc.utobj().printTestStep("Navigating to Admin > CRM > ContactType Configuration");
			fc.utobj().printTestStep("Add Contact Type");
			AdminCRMContactTypeConfigurationPageTest contactTypePage = new AdminCRMContactTypeConfigurationPageTest();
			String contactType = fc.utobj().generateTestData(dataSet.get("contactType"));
			contactTypePage.addContactType(driver, contactType);

			AdminCRMConfigureIndustryPageTest industryPage = new AdminCRMConfigureIndustryPageTest();
			String industry = fc.utobj().generateTestData(dataSet.get("industry"));
			String description = fc.utobj().generateTestData(dataSet.get("description"));
			fc.utobj().printTestStep("Navigating to Admin > CRM >  Configure Industry Type");
			fc.utobj().printTestStep("Add Industry");
			industryPage.addIndustry(driver, industry, description);

			String accountName = fc.utobj().generateTestData(dataSet.get("accountName"));
			String visibility = dataSet.get("visibility");
			fc.utobj().printTestStep("Navigating to CRM >  Accounts > Accounts Summary");
			fc.utobj().printTestStep("Add Account");
			addAcocunt(driver, industry, accountName, visibility);

			fc.utobj().printTestStep("Navigating to CRM >  Accounts > Accounts Summary");
			fc.utobj().printTestStep("Add Contact");

			fc.utobj().clickElement(driver, pobj.accountsLink);
			searchAccountByIndustry(driver, industry);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//a[.='" + accountName + "']"));
			fc.utobj().clickElement(driver, pobj.contactsTab);
			fc.utobj().clickElement(driver, pobj.addContact);

			String cFname = fc.utobj().generateTestData(dataSet.get("cFname"));
			String cLname = fc.utobj().generateTestData(dataSet.get("cLname"));
			String primaryCMethod = dataSet.get("primaryCMethod");
			String assignTo = "Corporate";
			String regionName = "";
			String franchiseId = "";
			String title = dataSet.get("title");

			String email = fc.utobj().generateTestData("crmautomation")+"@staffex.com";//"crmautomation@staffex.com";//VipinS
			contactsPage.addContactGenericWithAccount(driver, contactType, cFname, cLname, primaryCMethod, assignTo,
					corpUser.getuserFullName(), regionName, franchiseId, title, email, config, accountName);

			
			fc.utobj().printTestStep("Add Another Contact");
			fc.utobj().clickElement(driver, pobj.accountsLink);
			searchAccountByIndustry(driver, industry);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//a[.='" + accountName + "']"));
			fc.utobj().clickElement(driver, pobj.contactsTab);
			fc.utobj().clickElement(driver, pobj.addContact);

			fc.utobj().selectDropDown(driver, pobj.contactType, contactType);
			fc.utobj().selectDropDown(driver, pobj.title, title);
			String cFname1 = fc.utobj().generateTestData(dataSet.get("cFname"));
			String cLname1 = fc.utobj().generateTestData(dataSet.get("cLname"));
			fc.utobj().sendKeys(driver, pobj.contactFirstName, cFname1);
			fc.utobj().sendKeys(driver, pobj.contactLastName, cLname1);

			if (!fc.utobj().isSelected(driver, pobj.assignToCorporate)) {
				fc.utobj().clickElement(driver, pobj.assignToCorporate);
			}
			fc.utobj().selectDropDown(driver, pobj.selectCorporateUserContact, corpUser.getuserFullName());
			fc.utobj().sendKeys(driver, pobj.accountName, accountName);
			fc.utobj().clickElement(driver, pobj.saveBtn);

			fc.utobj().printTestStep("Merge Contact");
			fc.utobj().clickElement(driver, pobj.accountsLink);
			searchAccountByIndustry(driver, industry);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//a[.='" + accountName + "']"));
			fc.utobj().clickElement(driver, pobj.contactsTab);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//input[@name='checkBox']"));
			fc.utobj().selectActionMenuItemsWithTagA(driver, "Merge Contacts");
			if (!fc.utobj().isSelected(driver, pobj.secondNameSelect)) {
				fc.utobj().clickElement(driver, pobj.secondNameSelect);
			}

			// pick second name
			String secondName = fc.utobj().getElementByXpath(driver, ".//*[@value='second' and @name='name']/..")
					.getText().trim();

			fc.utobj().sendKeys(driver, pobj.remarksAtMerge, "Merge Remarks");
			fc.utobj().clickElement(driver, pobj.mergeBtn);

			fc.utobj().clickElement(driver, pobj.accountsLink);
			searchAccountByIndustry(driver, industry);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//a[.='" + accountName + "']"));
			fc.utobj().clickElement(driver, pobj.contactsTab);

			fc.utobj().printTestStep("Verify Merge Contact");
			boolean isMergeContactNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondName + "')]");
			if (isMergeContactNamePresent == false) {
				fc.utobj().throwsException("was not able to verify contact Name");
			}

			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//a[contains(text () ,'" + secondName + "')]"));
			boolean isCNamePresentAtPrimaryInfo = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () ,'Name')]/ancestor::tr/td[.='" + title + " " + secondName + "']");
			if (isCNamePresentAtPrimaryInfo == false) {
				fc.utobj().throwsException("was not able to verify Contact Name At Primary Info Page");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = {"crm"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Change Owner By Action Btn Options At CRM > Accounts > Contacts", testCaseId = "TC_129_Change_Owner")
	public void changeOwnerActionBtn() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			CRMAccountsPage pobj = new CRMAccountsPage(driver);

			CRMContactsPageTest contactsPage = new CRMContactsPageTest();

			AdminUsersManageRegionalUsersAddRegionalUserPageTest regionalUserPage = new AdminUsersManageRegionalUsersAddRegionalUserPageTest();
			String userNameReg = fc.utobj().generateTestData(dataSet.get("userNameReg"));
			String region = fc.utobj().generateTestData(dataSet.get("regionName"));
			String emailId = "crmautomation@staffex.com";
			regionalUserPage.addRegionalUser(driver, userNameReg, region, emailId);

			fc.utobj().printTestStep("Navigate To Admin Users Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");

			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUser = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporateUser.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);
			fc.utobj().printTestStep("Navigating to Admin > CRM > ContactType Configuration");
			fc.utobj().printTestStep("Add Contact Type");
			AdminCRMContactTypeConfigurationPageTest contactTypePage = new AdminCRMContactTypeConfigurationPageTest();
			String contactType = fc.utobj().generateTestData(dataSet.get("contactType"));
			contactTypePage.addContactType(driver, contactType);

			AdminCRMConfigureIndustryPageTest industryPage = new AdminCRMConfigureIndustryPageTest();
			String industry = fc.utobj().generateTestData(dataSet.get("industry"));
			String description = fc.utobj().generateTestData(dataSet.get("description"));
			fc.utobj().printTestStep("Navigating to Admin > CRM >  Configure Industry Type");
			fc.utobj().printTestStep("Add Industry");
			industryPage.addIndustry(driver, industry, description);

			String accountName = fc.utobj().generateTestData(dataSet.get("accountName"));
			String visibility = dataSet.get("visibility");
			fc.utobj().printTestStep("Navigating to CRM >  Accounts > Accounts Summary");
			fc.utobj().printTestStep("Add Account");
			addAcocunt(driver, industry, accountName, visibility);

			fc.utobj().printTestStep("Navigating to CRM >  Accounts > Accounts Summary");
			fc.utobj().printTestStep("Add Contact");

			fc.utobj().clickElement(driver, pobj.accountsLink);
			searchAccountByIndustry(driver, industry);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//a[.='" + accountName + "']"));
			fc.utobj().clickElement(driver, pobj.contactsTab);
			fc.utobj().clickElement(driver, pobj.addContact);

			String cFname = fc.utobj().generateTestData(dataSet.get("cFname"));
			String cLname = fc.utobj().generateTestData(dataSet.get("cLname"));
			String primaryCMethod = dataSet.get("primaryCMethod");
			String assignTo = "Corporate";
			String regionName = "";
			String franchiseId = "";
			String title = dataSet.get("title");
			String email = fc.utobj().generateTestData("crmautomation")+"@staffex.com";   //"crmautomation@staffex.com";   vipin

			contactsPage.addContactGenericWithAccount(driver, contactType, cFname, cLname, primaryCMethod, assignTo,
					corpUser.getuserFullName(), regionName, franchiseId, title, email, config, accountName);

			fc.utobj().clickElement(driver, pobj.accountsLink);
			searchAccountByIndustry(driver, industry);
			fc.utobj().printTestStep("Change Owner");
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//a[.='" + accountName + "']"));
			fc.utobj().clickElement(driver, pobj.contactsTab);

			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//a[contains(text () ,'" + cFname + " " + cLname + "')]/ancestor::tr/td/input[@name='checkb']"));
			fc.utobj().selectActionMenuItemsWithTagA(driver, "Change Owner");

			fc.commonMethods().switch_cboxIframe_frameId(driver);
			if (!fc.utobj().isSelected(driver, pobj.selectRegionalUser1)) {
				fc.utobj().clickElement(driver, pobj.selectRegionalUser1);
			}
			fc.utobj().selectDropDown(driver, pobj.selectAreaRegion, region);
			fc.utobj().selectDropDown(driver, pobj.selectRegionalUser, userNameReg + " " + userNameReg);
			fc.utobj().clickElement(driver, pobj.assignBtn);
			fc.utobj().clickElement(driver, pobj.closeBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Verify Change Owner");
			boolean isOwnerPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[contains(text () ,'" + cFname + " " + cLname
					+ "')]/ancestor::tr/td[contains(text () ,'" + userNameReg + " " + userNameReg + "')]");
			if (isOwnerPresent == false) {
				fc.utobj().throwsException("was not able to verify Owner");
			}
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//a[contains(text () ,'" + cFname + " " + cLname + "')]"));

			boolean isRegionNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () ,'Area / Region')]/ancestor::tr/td[.='" + region + "']");
			if (isRegionNamePresent == false) {
				fc.utobj().throwsException("was not able to verify Region name CRM > Contact > Primary Info");
			}

			boolean isRegionalUserPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () ,'Assign To')]/ancestor::tr/td[contains(text () ,'" + userNameReg + " "
							+ userNameReg + "')]");
			if (isRegionalUserPresent == false) {
				fc.utobj().throwsException("was not able to verify Regional User 'Owner' CRM > Contact > Primary Info");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "crm")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Change Status By Action Btn Options At CRM > Accounts > Contacts", testCaseId = "TC_130_Change_Status")
	public void changeStatusActionBtn() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			CRMAccountsPage pobj = new CRMAccountsPage(driver);

			CRMContactsPageTest contactsPage = new CRMContactsPageTest();

			/*
			 * fc.utobj().printTestStep(testCaseId,
			 * "Navigate To Admin Users Manage Corporate User");
			 * fc.utobj().printTestStep(testCaseId, "Add Corporate User");
			 * String emailId="crmautomation@staffex.com";
			 * AdminUsersManageCorporateUsersAddCorporateUserPageTest
			 * corporateUser=new
			 * AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			 * Map<String,String> userInfo = CorporateUser corpUser = new
			 * CorporateUser();corpUser.setEmail(emailId);corpUser =
			 * fc.CommonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			 * corporateUser.
			 * createDefaultCorporateUser_Through_WebService_AfterLogin(driver,
			 * corpUser);
			 */

			fc.utobj().printTestStep("Navigate To Admin Users Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			String emailId = "crmautomation@staffex.com";
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUser = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporateUser.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			fc.utobj().printTestStep("Navigating to Admin > CRM > ContactType Configuration");
			fc.utobj().printTestStep("Add Contact Type");
			AdminCRMContactTypeConfigurationPageTest contactTypePage = new AdminCRMContactTypeConfigurationPageTest();
			String contactType = fc.utobj().generateTestData(dataSet.get("contactType"));
			contactTypePage.addContactType(driver, contactType);

			AdminCRMConfigureIndustryPageTest industryPage = new AdminCRMConfigureIndustryPageTest();
			String industry = fc.utobj().generateTestData(dataSet.get("industry"));
			String description = fc.utobj().generateTestData(dataSet.get("description"));
			fc.utobj().printTestStep("Navigating to Admin > CRM >  Configure Industry Type");
			fc.utobj().printTestStep("Add Industry");
			industryPage.addIndustry(driver, industry, description);

			AdminCRMConfigureStatusPageTest statusPage = new AdminCRMConfigureStatusPageTest();
			String statusName = fc.utobj().generateTestData(dataSet.get("status"));
			statusPage.addStatus01(driver, statusName);

			String accountName = fc.utobj().generateTestData(dataSet.get("accountName"));
			String visibility = dataSet.get("visibility");
			fc.utobj().printTestStep("Navigating to CRM >  Accounts > Accounts Summary");
			fc.utobj().printTestStep("Add Account");
			addAcocunt(driver, industry, accountName, visibility);

			fc.utobj().printTestStep("Navigating to CRM >  Accounts > Accounts Summary");
			fc.utobj().printTestStep("Add Contact");

			fc.utobj().clickElement(driver, pobj.accountsLink);
			searchAccountByIndustry(driver, industry);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//a[.='" + accountName + "']"));
			fc.utobj().clickElement(driver, pobj.contactsTab);
			fc.utobj().clickElement(driver, pobj.addContact);

			String cFname = fc.utobj().generateTestData(dataSet.get("cFname"));
			String cLname = fc.utobj().generateTestData(dataSet.get("cLname"));
			String primaryCMethod = dataSet.get("primaryCMethod");
			String assignTo = "Corporate";
			String regionName = "";
			String franchiseId = "";
			String title = dataSet.get("title");
			String email = "crmautomation@staffex.com";

			contactsPage.addContactGenericWithAccount(driver, contactType, cFname, cLname, primaryCMethod, assignTo,
					corpUser.getuserFullName(), regionName, franchiseId, title, email, config, accountName);

			fc.utobj().clickElement(driver, pobj.accountsLink);
			searchAccountByIndustry(driver, industry);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//a[.='" + accountName + "']"));
			fc.utobj().clickElement(driver, pobj.contactsTab);

			fc.utobj().printTestStep("Change Status");
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//a[contains(text () ,'" + cFname + " " + cLname + "')]/ancestor::tr/td/input[@name='checkb']"));
			fc.utobj().selectActionMenuItemsWithTagA(driver, "Change Status");

			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().selectDropDown(driver, pobj.statusChangeTo, statusName);
			fc.utobj().sendKeys(driver, pobj.remarksAtMerge, "Test Remarks");
			fc.utobj().clickElement(driver, pobj.changeBtn);
			fc.utobj().clickElement(driver, pobj.closeBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Verify Change Status");
			boolean isStatusPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[contains(text () ,'" + cFname + " " + cLname
					+ "')]/ancestor::tr/td[contains(text () ,'" + statusName + "')]");
			if (isStatusPresent == false) {
				fc.utobj().throwsException("was not able to verify Status Name");
			}

			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//a[contains(text () ,'" + cFname + " " + cLname + "')]"));
			boolean isStatusPresentAtInfo = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//select[@id='cmLeadStatusID']/option[@selected and .='" + statusName + "']");
			if (isStatusPresentAtInfo == false) {
				fc.utobj().throwsException("was not able to verify Status Name at Info Page");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = {"crm","vipin"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Change Contact Type By Action Btn Options At CRM > Accounts > Contacts", testCaseId = "TC_131_Change_Contact_Type")
	public void changeContactTypeActionBtn() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			CRMAccountsPage pobj = new CRMAccountsPage(driver);

			CRMContactsPageTest contactsPage = new CRMContactsPageTest();
			fc.utobj().printTestStep("Navigate To Admin Users Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			String emailId = "crmautomation@staffex.com";
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUser = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporateUser.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			fc.utobj().printTestStep("Navigating to Admin > CRM > ContactType Configuration");
			fc.utobj().printTestStep("Add Contact Type");
			AdminCRMContactTypeConfigurationPageTest contactTypePage = new AdminCRMContactTypeConfigurationPageTest();
			String contactType = fc.utobj().generateTestData(dataSet.get("contactType"));
			contactTypePage.addContactType(driver, contactType);

			AdminCRMConfigureIndustryPageTest industryPage = new AdminCRMConfigureIndustryPageTest();
			String industry = fc.utobj().generateTestData(dataSet.get("industry"));
			String description = fc.utobj().generateTestData(dataSet.get("description"));
			fc.utobj().printTestStep("Navigating to Admin > CRM >  Configure Industry Type");
			fc.utobj().printTestStep("Add Industry");
			industryPage.addIndustry(driver, industry, description);

			fc.utobj().printTestStep("Navigating to Admin > CRM > ContactType Configuration");
			fc.utobj().printTestStep("Add Contact Type");
			String contactType1 = fc.utobj().generateTestData(dataSet.get("contactType"));
			contactTypePage.addContactType(driver, contactType1);

			String accountName = fc.utobj().generateTestData(dataSet.get("accountName"));
			String visibility = dataSet.get("visibility");
			fc.utobj().printTestStep("Navigating to CRM >  Accounts > Accounts Summary");
			fc.utobj().printTestStep("Add Account");
			addAcocunt(driver, industry, accountName, visibility);

			fc.utobj().printTestStep("Navigating to CRM >  Accounts > Accounts Summary");
			fc.utobj().printTestStep("Add Contact");

			fc.utobj().clickElement(driver, pobj.accountsLink);
			searchAccountByIndustry(driver, industry);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//a[.='" + accountName + "']"));
			fc.utobj().clickElement(driver, pobj.contactsTab);
			fc.utobj().clickElement(driver, pobj.addContact);

			String cFname = fc.utobj().generateTestData(dataSet.get("cFname"));
			String cLname = fc.utobj().generateTestData(dataSet.get("cLname"));
			String primaryCMethod = dataSet.get("primaryCMethod");
			String assignTo = "Corporate";
			String regionName = "";
			String franchiseId = "";
			String title = dataSet.get("title");
			String email = "crmautomation@staffex.com";

			contactsPage.addContactGenericWithAccount(driver, contactType, cFname, cLname, primaryCMethod, assignTo,
					corpUser.getuserFullName(), regionName, franchiseId, title, email, config, accountName);

			fc.utobj().clickElement(driver, pobj.accountsLink);
			searchAccountByIndustry(driver, industry);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//a[.='" + accountName + "']"));
			fc.utobj().clickElement(driver, pobj.contactsTab);

			fc.utobj().printTestStep("Change Contact Type");
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//a[contains(text () ,'" + cFname + " " + cLname + "')]/ancestor::tr/td/input[@name='checkb']"));
			fc.utobj().selectActionMenuItemsWithTagA(driver, "Change Contact Type");

			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().selectDropDown(driver, pobj.contactType, contactType1);
			fc.utobj().clickElement(driver, pobj.changeBtn);
			fc.utobj().clickElement(driver, pobj.closeBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Verify Change Contact Type");
			boolean isStatusPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[contains(text () ,'" + cFname + " " + cLname
					+ "')]/ancestor::tr/td[contains(text () ,'" + contactType1 + "')]");
			if (isStatusPresent == false) {
				fc.utobj().throwsException("was not able to verify Contact Type");
			}

			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//a[contains(text () ,'" + cFname + " " + cLname + "')]"));
			boolean isStatusPresentAtInfo = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () ,'Contact Type')]/ancestor::tr/td[.='" + contactType1 + "']");
			if (isStatusPresentAtInfo == false) {
				fc.utobj().throwsException("was not able to verify Contact Type at Info Page");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "crm" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Contacts Associate With Campaign Action Btn Options At CRM > Accounts > Contacts", testCaseId = "TC_132_Associate_With_Email_Campaign")
	public void associateWithEmailCompaignActionBtn() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			CRMAccountsPage pobj = new CRMAccountsPage(driver);

			fc.utobj().printTestStep("Navigate To CRM >Campaign Center");
			fc.utobj().printTestStep("Add Campaign");
			CRMCampaignCenterPageTestForCRM campaignCenterPage = new CRMCampaignCenterPageTestForCRM();
			String campaignName = fc.utobj().generateTestData(dataSet.get("campaignName"));
			String campaignDescription = fc.utobj().generateTestData(dataSet.get("campaignDescription"));
			String campaignAccessibility = dataSet.get("campaignAccessibility");
			String category = dataSet.get("category");
			String templateName = fc.utobj().generateTestData(dataSet.get("templateName"));
			String mailSubject = dataSet.get("mailSubject");
			String templateText = fc.utobj().generateTestData(dataSet.get("templateText"));
			campaignCenterPage.createCampaign(driver, campaignName, campaignDescription, campaignAccessibility,
					category, templateName, mailSubject, templateText, config);

			CRMContactsPageTest contactsPage = new CRMContactsPageTest();
			fc.utobj().printTestStep("Navigate To Admin Users Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			String emailId = "crmautomation@staffex.com";
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUser = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporateUser.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);
			fc.utobj().printTestStep("Navigating to Admin > CRM > ContactType Configuration");
			fc.utobj().printTestStep("Add Contact Type");
			AdminCRMContactTypeConfigurationPageTest contactTypePage = new AdminCRMContactTypeConfigurationPageTest();
			String contactType = fc.utobj().generateTestData(dataSet.get("contactType"));
			contactTypePage.addContactType(driver, contactType);

			AdminCRMConfigureIndustryPageTest industryPage = new AdminCRMConfigureIndustryPageTest();
			String industry = fc.utobj().generateTestData(dataSet.get("industry"));
			String description = fc.utobj().generateTestData(dataSet.get("description"));
			fc.utobj().printTestStep("Navigating to Admin > CRM >  Configure Industry Type");
			fc.utobj().printTestStep("Add Industry");
			industryPage.addIndustry(driver, industry, description);

			String accountName = fc.utobj().generateTestData(dataSet.get("accountName"));
			String visibility = dataSet.get("visibility");
			fc.utobj().printTestStep("Navigating to CRM >  Accounts > Accounts Summary");
			fc.utobj().printTestStep("Add Account");
			addAcocunt(driver, industry, accountName, visibility);

			fc.utobj().printTestStep("Navigating to CRM >  Accounts > Accounts Summary");
			fc.utobj().printTestStep("Add Contact");

			fc.utobj().clickElement(driver, pobj.accountsLink);
			searchAccountByIndustry(driver, industry);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//a[.='" + accountName + "']"));
			fc.utobj().clickElement(driver, pobj.contactsTab);
			fc.utobj().clickElement(driver, pobj.addContact);

			String cFname = fc.utobj().generateTestData(dataSet.get("cFname"));
			String cLname = fc.utobj().generateTestData(dataSet.get("cLname"));
			String primaryCMethod = dataSet.get("primaryCMethod");
			String assignTo = "Corporate";
			String regionName = "";
			String franchiseId = "";
			String title = dataSet.get("title");
			String email = "crmautomation@staffex.com";

			contactsPage.addContactGenericWithAccount(driver, contactType, cFname, cLname, primaryCMethod, assignTo,
					corpUser.getuserFullName(), regionName, franchiseId, title, email, config, accountName);

			fc.utobj().clickElement(driver, pobj.accountsLink);
			searchAccountByIndustry(driver, industry);
			fc.utobj().printTestStep("Associate With Campaign");
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//a[.='" + accountName + "']"));
			fc.utobj().clickElement(driver, pobj.contactsTab);

			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//a[contains(text () ,'" + cFname + " " + cLname + "')]/ancestor::tr/td/input[@name='checkb']"));
			fc.utobj().selectActionMenuItemsWithTagA(driver, "Associate with Campaign");

			fc.utobj().clickElement(driver, pobj.filterContainer);
			fc.utobj().sendKeys(driver, pobj.searchCampaign, campaignName);
			fc.utobj().clickElement(driver, pobj.applyFilter);

			fc.utobj().clickElement(driver, new CRMLeadsPage(driver).sendCampaignBtn);
			fc.utobj().clickElement(driver, pobj.confirmBtn);

			fc.utobj().clickElement(driver, pobj.accountsLink);
			searchAccountByIndustry(driver, industry);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//a[.='" + accountName + "']"));
			fc.utobj().clickElement(driver, pobj.contactsTab);

			fc.utobj().printTestStep("Verify Associate With Campaign");
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//a[contains(text () ,'" + cFname + " " + cLname + "')]"));
			boolean isCampaignNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () ,'Promotional Campaign')]/following-sibling::td[.='" + campaignName + "']");
			if (isCampaignNamePresent == false) {
				fc.utobj().throwsException("was not able to verify Campign Name");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "crm"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Contact Add To Group By Action Btn Options At CRM > Accounts > Contacts", testCaseId = "TC_133_Add_To_Group")
	public void addToGroupActionBtn() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			CRMAccountsPage pobj = new CRMAccountsPage(driver);
			CRMGroupsPageTest groupPage = new CRMGroupsPageTest();

			CRMContactsPageTest contactsPage = new CRMContactsPageTest();
			fc.utobj().printTestStep("Navigate To Admin Users Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			String emailId = "crmautomation@staffex.com";
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUser = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporateUser.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);
			fc.utobj().printTestStep("Navigating to Admin > CRM > ContactType Configuration");
			fc.utobj().printTestStep("Add Contact Type");
			AdminCRMContactTypeConfigurationPageTest contactTypePage = new AdminCRMContactTypeConfigurationPageTest();
			String contactType = fc.utobj().generateTestData(dataSet.get("contactType"));
			contactTypePage.addContactType(driver, contactType);

			AdminCRMConfigureIndustryPageTest industryPage = new AdminCRMConfigureIndustryPageTest();
			String industry = fc.utobj().generateTestData(dataSet.get("industry"));
			String description = fc.utobj().generateTestData(dataSet.get("description"));
			fc.utobj().printTestStep("Navigating to Admin > CRM >  Configure Industry Type");
			fc.utobj().printTestStep("Add Industry");
			industryPage.addIndustry(driver, industry, description);

			String accountName = fc.utobj().generateTestData(dataSet.get("accountName"));
			String visibility = dataSet.get("visibility");
			fc.utobj().printTestStep("Navigating to CRM >  Accounts > Accounts Summary");
			fc.utobj().printTestStep("Add Account");
			addAcocunt(driver, industry, accountName, visibility);

			fc.utobj().printTestStep("Navigating to CRM >  Accounts > Accounts Summary");
			fc.utobj().printTestStep("Add Contact");

			fc.utobj().clickElement(driver, pobj.accountsLink);
			searchAccountByIndustry(driver, industry);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//a[.='" + accountName + "']"));
			fc.utobj().clickElement(driver, pobj.contactsTab);
			fc.utobj().clickElement(driver, pobj.addContact);

			String cFname = fc.utobj().generateTestData(dataSet.get("cFname"));
			String cLname = fc.utobj().generateTestData(dataSet.get("cLname"));
			String primaryCMethod = dataSet.get("primaryCMethod");
			String assignTo = "Corporate";
			String regionName = "";
			String franchiseId = "";
			String title = dataSet.get("title");
			String email = "crmautomation@staffex.com";

			contactsPage.addContactGenericWithAccount(driver, contactType, cFname, cLname, primaryCMethod, assignTo,
					corpUser.getuserFullName(), regionName, franchiseId, title, email, config, accountName);

			fc.utobj().clickElement(driver, pobj.accountsLink);
			searchAccountByIndustry(driver, industry);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//a[.='" + accountName + "']"));
			fc.utobj().clickElement(driver, pobj.contactsTab);

			fc.utobj().printTestStep("Add To Group");
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//a[contains(text () ,'" + cFname + " " + cLname + "')]/ancestor::tr/td/input[@name='checkb']"));
			fc.utobj().selectActionMenuItemsWithTagA(driver, "Add To Group");

			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().clickElement(driver, pobj.addToNewGroup);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Create New Group");
			String groupName = fc.utobj().generateTestData(dataSet.get("groupName"));
			String accessibility = "Public to all Users";
			groupPage.addToGroupRegular(driver, groupName, accessibility);

			/*
			 * fc.utobj().sendKeys(driver, pobj.searchAtGroup, groupName);
			 * fc.utobj().clickElement(driver, pobj.exactSearchGroup);
			 * fc.utobj().clickElement(driver, pobj.searchAccountBtn);
			 */

			new CRMGroupsPageTest().searchGroupsByFilter(driver, groupName, "Contact");
			String colunmName = "";
			int z = 0;

			// click over Recipients
			List<WebElement> listHead = driver
					.findElements(By.xpath(".//a[.='" + groupName + "']/ancestor::table/thead/tr/th"));
			System.out.println(listHead);
			for (int i = 0; i < listHead.size(); i++) {

				colunmName = listHead.get(i).getText().trim();
				if (colunmName.equalsIgnoreCase("Recipients")) {
					z = i;
					break;
				}
			}

			if (z != 0) {
				z = z + 1;
				fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
						".//a[.='" + groupName + "']/ancestor::tr/td[" + z + "]/a"));
			} else {
				fc.utobj().throwsException("was not able to find Recipients Column");
			}

			//searchContactByContactType(driver, contactType);
			fc.utobj().printTestStep("Verify The Contact Add To Group");
			boolean isContactNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + cFname + " " + cLname + "')]");
			if (isContactNamePresent == false) {
				fc.utobj().throwsException("was not able to add in Group");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "crm", "crmcheckinh2" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Archive Contacts by Action Btn Options At CRM > Accounts > Contacts", testCaseId = "TC_134_Archive_Contacts")
	public void archiveContactsActionBtn() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			CRMAccountsPage pobj = new CRMAccountsPage(driver);

			CRMContactsPageTest contactsPage = new CRMContactsPageTest();

			/*
			 * fc.utobj().printTestStep(testCaseId,
			 * "Navigate To Admin Users Manage Corporate User");
			 * fc.utobj().printTestStep(testCaseId, "Add Corporate User");
			 * String emailId="crmautomation@staffex.com";
			 * AdminUsersManageCorporateUsersAddCorporateUserPageTest
			 * corporateUser=new
			 * AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			 * Map<String,String> userInfo = CorporateUser corpUser = new
			 * CorporateUser();corpUser.setEmail(emailId);corpUser =
			 * fc.CommonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			 * corporateUser.
			 * createDefaultCorporateUser_Through_WebService_AfterLogin(driver,
			 * corpUser);
			 */

			fc.utobj().printTestStep("Navigate To Admin Users Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			String emailId = "crmautomation@staffex.com";
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUser = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporateUser.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			fc.utobj().printTestStep("Navigating to Admin > CRM > ContactType Configuration");
			fc.utobj().printTestStep("Add Contact Type");
			AdminCRMContactTypeConfigurationPageTest contactTypePage = new AdminCRMContactTypeConfigurationPageTest();
			String contactType = fc.utobj().generateTestData(dataSet.get("contactType"));
			contactTypePage.addContactType(driver, contactType);

			AdminCRMConfigureIndustryPageTest industryPage = new AdminCRMConfigureIndustryPageTest();
			String industry = fc.utobj().generateTestData(dataSet.get("industry"));
			String description = fc.utobj().generateTestData(dataSet.get("description"));
			fc.utobj().printTestStep("Navigating to Admin > CRM >  Configure Industry Type");
			fc.utobj().printTestStep("Add Industry");
			industryPage.addIndustry(driver, industry, description);

			String accountName = fc.utobj().generateTestData(dataSet.get("accountName"));
			String visibility = dataSet.get("visibility");
			fc.utobj().printTestStep("Navigating to CRM >  Accounts > Accounts Summary");
			fc.utobj().printTestStep("Add Account");
			addAcocunt(driver, industry, accountName, visibility);

			fc.utobj().printTestStep("Navigating to CRM >  Accounts > Accounts Summary");
			fc.utobj().printTestStep("Add Contact");

			fc.utobj().clickElement(driver, pobj.accountsLink);
			searchAccountByIndustry(driver, industry);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//a[.='" + accountName + "']"));
			fc.utobj().clickElement(driver, pobj.contactsTab);
			fc.utobj().clickElement(driver, pobj.addContact);

			String cFname = fc.utobj().generateTestData(dataSet.get("cFname"));
			String cLname = fc.utobj().generateTestData(dataSet.get("cLname"));
			String primaryCMethod = dataSet.get("primaryCMethod");
			String assignTo = "Corporate";
			String regionName = "";
			String franchiseId = "";
			String title = dataSet.get("title");
			String email = "crmautomation@staffex.com";

			contactsPage.addContactGenericWithAccount(driver, contactType, cFname, cLname, primaryCMethod, assignTo,
					corpUser.getuserFullName(), regionName, franchiseId, title, email, config, accountName);

			fc.utobj().clickElement(driver, pobj.accountsLink);
			searchAccountByIndustry(driver, industry);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//a[.='" + accountName + "']"));
			fc.utobj().clickElement(driver, pobj.contactsTab);

			fc.utobj().printTestStep("Archive Contact");
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//a[contains(text () ,'" + cFname + " " + cLname + "')]/ancestor::tr/td/input[@name='checkb']"));
			fc.utobj().selectActionMenuItemsWithTagA(driver, "Archive Contacts");
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().clickElement(driver, ".//input[@value = 'Archive']");
			fc.utobj().clickElement(driver, ".//input[@value = 'Close']");
			

			fc.utobj().printTestStep("Verify Archive Contact At Accounts > Contact");
			boolean isNoRecordPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//td[.='No records found.']");
			if (isNoRecordPresent == false) {
				fc.utobj().throwsException("was not able to Archive Contact");
			}
			fc.utobj().clickElement(driver, pobj.searchLink);
			fc.utobj().clickElement(driver, pobj.contactsTabAtSearch);
			if (!fc.utobj().isSelected(driver, pobj.archiveContacts)) {
				fc.utobj().clickElement(driver, pobj.archiveContacts);
			}
			fc.utobj().setToDefault(driver, pobj.assignTo);

			WebElement elementInput = fc.utobj().getElement(driver, pobj.assignTo)
					.findElement(By.xpath("./div/div/input"));

			fc.utobj().selectValFromMultiSelect(driver, pobj.assignTo, elementInput, corpUser.getuserFullName());
			fc.utobj().clickElement(driver, pobj.searchBtnAtSearch);

			fc.utobj().printTestStep("Verify Contact At Search");
			boolean isNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//*[contains(text () ,'" + cFname + " " + cLname + "')]");
			if (isNamePresent == false) {
				fc.utobj().throwsException("was not able to verify Archive Contacts");
			}
			boolean isOwnerPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//*[contains(text () ,'" + cFname + " " + cLname
					+ "')]/ancestor::tr/td[contains(text () , '" + corpUser.getuserFullName() + "')]");
			if (isOwnerPresent == false) {
				fc.utobj().throwsException("was not able to verify Owner of Archive Contacts");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = {"crm","deletecontact"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Delete Contacts By Action Btn Options At CRM > Accounts > Contacts", testCaseId = "TC_135_Delete_Contact")
	public void deleteContactActionBtn() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			CRMAccountsPage pobj = new CRMAccountsPage(driver);

			CRMContactsPageTest contactsPage = new CRMContactsPageTest();
			fc.utobj().printTestStep("Navigate To Admin Users Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			String emailId = "crmautomation@staffex.com";
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUser = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporateUser.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);
			fc.utobj().printTestStep("Navigating to Admin > CRM > ContactType Configuration");
			fc.utobj().printTestStep("Add Contact Type");
			AdminCRMContactTypeConfigurationPageTest contactTypePage = new AdminCRMContactTypeConfigurationPageTest();
			String contactType = fc.utobj().generateTestData(dataSet.get("contactType"));
			contactTypePage.addContactType(driver, contactType);

			AdminCRMConfigureIndustryPageTest industryPage = new AdminCRMConfigureIndustryPageTest();
			String industry = fc.utobj().generateTestData(dataSet.get("industry"));
			String description = fc.utobj().generateTestData(dataSet.get("description"));
			fc.utobj().printTestStep("Navigating to Admin > CRM >  Configure Industry Type");
			fc.utobj().printTestStep("Add Industry");
			industryPage.addIndustry(driver, industry, description);

			String accountName = fc.utobj().generateTestData(dataSet.get("accountName"));
			String visibility = dataSet.get("visibility");
			fc.utobj().printTestStep("Navigating to CRM >  Accounts > Accounts Summary");
			fc.utobj().printTestStep("Add Account");
			addAcocunt(driver, industry, accountName, visibility);

			fc.utobj().printTestStep("Navigating to CRM >  Accounts > Accounts Summary");
			fc.utobj().printTestStep("Add Contact");

			fc.utobj().clickElement(driver, pobj.accountsLink);
			searchAccountByIndustry(driver, industry);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//a[.='" + accountName + "']"));
			fc.utobj().clickElement(driver, pobj.contactsTab);
			fc.utobj().clickElement(driver, pobj.addContact);

			String cFname = fc.utobj().generateTestData(dataSet.get("cFname"));
			String cLname = fc.utobj().generateTestData(dataSet.get("cLname"));
			String primaryCMethod = dataSet.get("primaryCMethod");
			String assignTo = "Corporate";
			String regionName = "";
			String franchiseId = "";
			String title = dataSet.get("title");
			String email = fc.utobj().generateTestData("crmautomation")+"@staffex.com"; //"crmautomation@staffex.com";

			contactsPage.addContactGenericWithAccount(driver, contactType, cFname, cLname, primaryCMethod, assignTo,
					corpUser.getuserFullName(), regionName, franchiseId, title, email, config, accountName);

			fc.utobj().clickElement(driver, pobj.accountsLink);
			searchAccountByIndustry(driver, industry);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//a[.='" + accountName + "']"));
			fc.utobj().clickElement(driver, pobj.contactsTab);

			fc.utobj().printTestStep("Delete Contact");
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//a[contains(text () ,'" + cFname + " " + cLname + "')]/ancestor::tr/td/input[@name='checkb']"));
			fc.utobj().selectActionMenuItemsWithTagA(driver, "Delete");
			System.out.println("Clicked on Delete option in more menu");
			fc.utobj().sleep(2000);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().sleep(2000);
			fc.utobj().clickElement(driver, ".//input[@value = 'Delete']");
			fc.utobj().sleep(2000);
			fc.utobj().clickElement(driver, ".//input[@value = 'Close']");
			fc.utobj().switchFrameToDefault(driver);
			fc.utobj().sleep(2000);
			fc.utobj().printTestStep("Verify The Delete Contact");
			boolean isNoRecordPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//td[.='No records found.']");
			if (isNoRecordPresent == false) {
				fc.utobj().throwsException("was not able to Delete Contact");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = {"crm"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Log A Task By Action Bottom Btn Option At CRM > Accounts > Contacts", testCaseId = "TC_136_Log_A_Task")
	public void logATasksBottomBtn() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			CRMAccountsPage pobj = new CRMAccountsPage(driver);

			CRMContactsPageTest contactsPage = new CRMContactsPageTest();
			fc.utobj().printTestStep("Navigate To Admin Users Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			String emailId = "crmautomation@staffex.com";
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUser = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporateUser.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);
			fc.utobj().printTestStep("Navigating to Admin > CRM > ContactType Configuration");
			fc.utobj().printTestStep("Add Contact Type");
			AdminCRMContactTypeConfigurationPageTest contactTypePage = new AdminCRMContactTypeConfigurationPageTest();
			String contactType = fc.utobj().generateTestData(dataSet.get("contactType"));
			contactTypePage.addContactType(driver, contactType);

			AdminCRMConfigureIndustryPageTest industryPage = new AdminCRMConfigureIndustryPageTest();
			String industry = fc.utobj().generateTestData(dataSet.get("industry"));
			String description = fc.utobj().generateTestData(dataSet.get("description"));
			fc.utobj().printTestStep("Navigating to Admin > CRM >  Configure Industry Type");
			fc.utobj().printTestStep("Add Industry");
			industryPage.addIndustry(driver, industry, description);

			String accountName = fc.utobj().generateTestData(dataSet.get("accountName"));
			String visibility = dataSet.get("visibility");
			fc.utobj().printTestStep("Navigating to CRM >  Accounts > Accounts Summary");
			fc.utobj().printTestStep("Add Account");
			addAcocunt(driver, industry, accountName, visibility);

			fc.utobj().printTestStep("Navigating to CRM >  Accounts > Accounts Summary");
			fc.utobj().printTestStep("Add Contact");

			fc.utobj().clickElement(driver, pobj.accountsLink);
			searchAccountByIndustry(driver, industry);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//a[.='" + accountName + "']"));
			fc.utobj().clickElement(driver, pobj.contactsTab);
			fc.utobj().clickElement(driver, pobj.addContact);

			String cFname = fc.utobj().generateTestData(dataSet.get("cFname"));
			String cLname = fc.utobj().generateTestData(dataSet.get("cLname"));
			String primaryCMethod = dataSet.get("primaryCMethod");
			String assignTo = "Corporate";
			String regionName = "";
			String franchiseId = "";
			String title = dataSet.get("title");
			//String email = "crmautomation@staffex.com";
			String email =fc.utobj().generateTestData("crmautomation")+"@staffex.com";
			contactsPage.addContactGenericWithAccount(driver, contactType, cFname, cLname, primaryCMethod, assignTo,
					corpUser.getuserFullName(), regionName, franchiseId, title, email, config, accountName);

			fc.utobj().clickElement(driver, pobj.accountsLink);
			searchAccountByIndustry(driver, industry);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//a[.='" + accountName + "']"));
			fc.utobj().clickElement(driver, pobj.contactsTab);

			fc.utobj().printTestStep("Log A Task");
			// fc.utobj().actionImgOption(driver, cFname+" "+cLname, "Log a
			// Task");
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//a[contains(text () ,'" + cFname + " " + cLname + "')]/ancestor::tr/td/input[@name='checkb']"));
			fc.utobj().clickElement(driver, pobj.logaTaskBtmBtn);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			if (!fc.utobj().isSelected(driver, pobj.radioOwner)) {
				fc.utobj().clickElement(driver, pobj.radioOwner);
			}
			String statusTask = dataSet.get("statusTask");
			fc.utobj().selectDropDown(driver, pobj.statusTask, statusTask);
			String subject = fc.utobj().generateTestData(dataSet.get("subject"));
			fc.utobj().sendKeys(driver, pobj.subject, subject);
			if (!fc.utobj().isSelected(driver, pobj.timeLessTask)) {
				fc.utobj().clickElement(driver, pobj.timeLessTask);
			}
			String priority = dataSet.get("priority");
			fc.utobj().selectDropDown(driver, pobj.priority, priority);
			fc.utobj().sendKeys(driver, pobj.comments, "Test Comment");
			fc.utobj().clickElement(driver, pobj.createBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//a[contains(text () ,'" + cFname + " " + cLname + "')]"));

			fc.utobj().printTestStep("Verify Log A Task");
			boolean isSubjectPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[contains(text () ,'" + subject + "')]");
			if (isSubjectPresent == false) {
				fc.utobj().throwsException("was not able to verify subject");
			}
			boolean isUserPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[contains(text () ,'" + subject
					+ "')]/ancestor::tr/td/div[.='" + corpUser.getuserFullName() + "']");
			if (isUserPresent == false) {
				fc.utobj().throwsException("was not able to verify userName at Lead Info Page");
			}
			boolean isStatusOfTaskPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[contains(text () ,'" + subject
					+ "')]/ancestor::tr/td[contains(text () ,'" + statusTask + "')]");
			if (isStatusOfTaskPresent == false) {
				fc.utobj().throwsException("was not able to verify status of task");
			}
			boolean isPriorityPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[contains(text () ,'" + subject
					+ "')]/ancestor::tr/td[contains(text () ,'" + priority + "')]");
			if (isPriorityPresent == false) {
				fc.utobj().throwsException("was not able to verify Priority of task");
			}
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "crm")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Send Email By Action Bottom Btn Options At CRM > Accounts > Contacts", testCaseId = "TC_137_Send_Email")
	public void sendEmailBottomBtn() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			CRMAccountsPage pobj = new CRMAccountsPage(driver);

			String parentWindowsHandle = driver.getWindowHandle();

			CRMContactsPageTest contactsPage = new CRMContactsPageTest();
			fc.utobj().printTestStep("Navigate To Admin Users Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			String emailId = "crmautomation@staffex.com";
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUser = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporateUser.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);
			fc.utobj().printTestStep("Navigating to Admin > CRM > ContactType Configuration");
			fc.utobj().printTestStep("Add Contact Type");
			AdminCRMContactTypeConfigurationPageTest contactTypePage = new AdminCRMContactTypeConfigurationPageTest();
			String contactType = fc.utobj().generateTestData(dataSet.get("contactType"));
			contactTypePage.addContactType(driver, contactType);

			AdminCRMConfigureIndustryPageTest industryPage = new AdminCRMConfigureIndustryPageTest();
			String industry = fc.utobj().generateTestData(dataSet.get("industry"));
			String description = fc.utobj().generateTestData(dataSet.get("description"));
			fc.utobj().printTestStep("Navigating to Admin > CRM >  Configure Industry Type");
			fc.utobj().printTestStep("Add Industry");
			industryPage.addIndustry(driver, industry, description);

			String accountName = fc.utobj().generateTestData(dataSet.get("accountName"));
			String visibility = dataSet.get("visibility");
			fc.utobj().printTestStep("Navigating to CRM >  Accounts > Accounts Summary");
			fc.utobj().printTestStep("Add Account");
			addAcocunt(driver, industry, accountName, visibility);

			fc.utobj().printTestStep("Navigating to CRM >  Accounts > Accounts Summary");
			fc.utobj().printTestStep("Add Contact");

			fc.utobj().clickElement(driver, pobj.accountsLink);
			searchAccountByIndustry(driver, industry);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//a[.='" + accountName + "']"));
			fc.utobj().clickElement(driver, pobj.contactsTab);
			fc.utobj().clickElement(driver, pobj.addContact);

			String cFname = fc.utobj().generateTestData(dataSet.get("cFname"));
			String cLname = fc.utobj().generateTestData(dataSet.get("cLname"));
			String primaryCMethod = dataSet.get("primaryCMethod");
			String assignTo = "Corporate";
			String regionName = "";
			String franchiseId = "";
			String title = dataSet.get("title");
			//String email = "crmautomation@staffex.com";
			String email=fc.utobj().generateTestData("crmautomation")+"@staffex.com";
			contactsPage.addContactGenericWithAccount(driver, contactType, cFname, cLname, primaryCMethod, assignTo,
					corpUser.getuserFullName(), regionName, franchiseId, title, email, config, accountName);

			fc.utobj().clickElement(driver, pobj.accountsLink);
			searchAccountByIndustry(driver, industry);
			fc.utobj().printTestStep("Send Email");
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//a[.='" + accountName + "']"));
			fc.utobj().clickElement(driver, pobj.contactsTab);

			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//a[contains(text () ,'" + cFname + " " + cLname + "')]/ancestor::tr/td/input[@name='checkb']"));
			fc.utobj().clickElement(driver, pobj.sendEmailBtmBtn);

			if (!fc.utobj().isSelected(driver, pobj.leadOwnerId)) {
				fc.utobj().clickElement(driver, pobj.leadOwnerId);
			}

			String mailCC = dataSet.get("mailCC");
			fc.utobj().sendKeys(driver, pobj.mailCC, mailCC);

			if (!fc.utobj().isSelected(driver, pobj.showBcc)) {
				fc.utobj().clickElement(driver, pobj.showBcc);
			}
			String mailBCC = dataSet.get("mailBCC");
			fc.utobj().sendKeys(driver, pobj.mailBCC, mailBCC);

			String subjectMail = fc.utobj().generateTestData(dataSet.get("subjectMail"));
			fc.utobj().sendKeys(driver, pobj.subjectMail, subjectMail);

			String editorText = fc.utobj().generateTestData(dataSet.get("mailText"));

			fc.utobj().switchFrameById(driver, "ta_ifr");

			Actions actions = new Actions(driver);
			actions.moveToElement(fc.utobj().getElement(driver, pobj.editorTextArea));
			actions.click();
			actions.sendKeys(editorText);
			fc.utobj().logReport("Entering Value in Text Field",
					fc.utobj().getElement(driver, pobj.editorTextArea));
			fc.utobj().logReportMsg("Entered Text", editorText);
			actions.build().perform();
			fc.utobj().switchFrameToDefault(driver);
			fc.utobj().clickElement(driver, pobj.sendEmailBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().clickElement(driver, pobj.accountsLink);
			searchAccountByIndustry(driver, industry);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//a[.='" + accountName + "']"));
			fc.utobj().clickElement(driver, pobj.contactsTab);

			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//a[contains(text () , '" + cFname + " " + cLname + "')]"));
			fc.utobj().clickElement(driver, pobj.detailedHistoryLink);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().clickElement(driver, pobj.emailsTab);
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//*[contains(text () ,'" + cFname + "')]"));

			fc.utobj().printTestStep("Verify Send Email At Detailed History Frame");

			Set<String> allWindows = driver.getWindowHandles();
			for (String currentWindow : allWindows) {

				if (!currentWindow.equalsIgnoreCase(parentWindowsHandle)) {

					driver.switchTo().window(currentWindow);
					try {

						if (fc.utobj().getElementByXpath(driver, ".//*[.='Email Details']").isDisplayed()) {

							boolean isFromTextPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
									".//*[contains(text () ,'From')]/ancestor::tr/td[contains(text () , '"
											+ corpUser.getuserFullName() + "')]");
							if (isFromTextPresent == false) {
								fc.utobj().throwsException("was not able to From Text");
							}

							boolean isToTextPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
									".//*[contains(text () ,'To')]/ancestor::tr/td[contains(text () , '" + cFname + " "
											+ cLname + "')]");
							if (isToTextPresent == false) {
								fc.utobj().throwsException("was not able to To Text");
							}
							boolean isSubjectTextPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
									".//*[contains(text () ,'To')]/ancestor::tr/td[contains(text () , '" + subjectMail
											+ "')]");
							if (isSubjectTextPresent == false) {
								fc.utobj().throwsException("was not able to Subject");
							}
							boolean isTextPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//*[.='" + editorText + "']");
							if (isTextPresent == false) {
								fc.utobj().throwsException("was not able to verify Editor text");
							}

						}

					} catch (Exception e) {
						driver.close();
					}
				}
				driver.switchTo().window(parentWindowsHandle);
			}

			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().clickElement(driver, pobj.closeBtn);

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = {"crm"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Merge Contacts By Action Bottom Btn Options At CRM > Accounts > Contacts", testCaseId = "TC_138_Merge_Contact")
	public void mergeContactsBottomBtn() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			CRMAccountsPage pobj = new CRMAccountsPage(driver);

			CRMContactsPageTest contactsPage = new CRMContactsPageTest();
			fc.utobj().printTestStep("Navigate To Admin Users Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			String emailId = "crmautomation@staffex.com";
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUser = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporateUser.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);
			fc.utobj().printTestStep("Navigating to Admin > CRM > ContactType Configuration");
			fc.utobj().printTestStep("Add Contact Type");
			AdminCRMContactTypeConfigurationPageTest contactTypePage = new AdminCRMContactTypeConfigurationPageTest();
			String contactType = fc.utobj().generateTestData(dataSet.get("contactType"));
			contactTypePage.addContactType(driver, contactType);

			AdminCRMConfigureIndustryPageTest industryPage = new AdminCRMConfigureIndustryPageTest();
			String industry = fc.utobj().generateTestData(dataSet.get("industry"));
			String description = fc.utobj().generateTestData(dataSet.get("description"));
			fc.utobj().printTestStep("Navigating to Admin > CRM >  Configure Industry Type");
			fc.utobj().printTestStep("Add Industry");
			industryPage.addIndustry(driver, industry, description);

			String accountName = fc.utobj().generateTestData(dataSet.get("accountName"));
			String visibility = dataSet.get("visibility");
			fc.utobj().printTestStep("Navigating to CRM >  Accounts > Accounts Summary");
			fc.utobj().printTestStep("Add Account");
			addAcocunt(driver, industry, accountName, visibility);

			fc.utobj().printTestStep("Navigating to CRM >  Accounts > Accounts Summary");
			fc.utobj().printTestStep("Add Contact");

			fc.utobj().clickElement(driver, pobj.accountsLink);
			searchAccountByIndustry(driver, industry);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//a[.='" + accountName + "']"));
			fc.utobj().clickElement(driver, pobj.contactsTab);
			fc.utobj().clickElement(driver, pobj.addContact);

			String cFname = fc.utobj().generateTestData(dataSet.get("cFname"));
			String cLname = fc.utobj().generateTestData(dataSet.get("cLname"));
			String primaryCMethod = dataSet.get("primaryCMethod");
			String assignTo = "Corporate";
			String regionName = "";
			String franchiseId = "";
			String title = dataSet.get("title");
			//String email = "crmautomation@staffex.com";
			String email=fc.utobj().generateTestData("crmautomation")+"@staffex.com";
			contactsPage.addContactGenericWithAccount(driver, contactType, cFname, cLname, primaryCMethod, assignTo,
					corpUser.getuserFullName(), regionName, franchiseId, title, email, config, accountName);

			fc.utobj().printTestStep("Add Anothe Contact");
			fc.utobj().clickElement(driver, pobj.accountsLink);
			searchAccountByIndustry(driver, industry);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//a[.='" + accountName + "']"));
			fc.utobj().clickElement(driver, pobj.contactsTab);
			fc.utobj().clickElement(driver, pobj.addContact);

			fc.utobj().selectDropDown(driver, pobj.contactType, contactType);
			fc.utobj().selectDropDown(driver, pobj.title, title);
			String cFname1 = fc.utobj().generateTestData(dataSet.get("cFname"));
			String cLname1 = fc.utobj().generateTestData(dataSet.get("cLname"));
			fc.utobj().sendKeys(driver, pobj.contactFirstName, cFname1);
			fc.utobj().sendKeys(driver, pobj.contactLastName, cLname1);

			if (!fc.utobj().isSelected(driver, pobj.assignToCorporate)) {
				fc.utobj().clickElement(driver, pobj.assignToCorporate);
			}
			fc.utobj().selectDropDown(driver, pobj.selectCorporateUserContact, corpUser.getuserFullName());
			fc.utobj().sendKeys(driver, pobj.accountName, accountName);
			fc.utobj().clickElement(driver, pobj.saveBtn);

			fc.utobj().clickElement(driver, pobj.accountsLink);
			searchAccountByIndustry(driver, industry);

			fc.utobj().printTestStep("Merge Contact");
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//a[.='" + accountName + "']"));
			fc.utobj().clickElement(driver, pobj.contactsTab);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//input[@name='checkBox']"));
			fc.utobj().clickElement(driver, pobj.mergeContactsBtmBtn);
			if (!fc.utobj().isSelected(driver, pobj.secondNameSelect)) {
				fc.utobj().clickElement(driver, pobj.secondNameSelect);
			}

			String secondName = fc.utobj().getElementByXpath(driver, ".//*[@value='second' and @name='name']/..")
					.getText().trim();

			fc.utobj().sendKeys(driver, pobj.remarksAtMerge, "Merge Remarks");
			fc.utobj().clickElement(driver, pobj.mergeBtn);

			fc.utobj().clickElement(driver, pobj.accountsLink);
			searchAccountByIndustry(driver, industry);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//a[.='" + accountName + "']"));
			fc.utobj().clickElement(driver, pobj.contactsTab);

			fc.utobj().printTestStep("Verify Merge Contact");
			boolean isMergeContactNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondName + "')]");
			if (isMergeContactNamePresent == false) {
				fc.utobj().throwsException("was not able to verify contact Name");
			}

			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//a[contains(text () ,'" + secondName + "')]"));
			boolean isCNamePresentAtPrimaryInfo = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () ,'Name')]/ancestor::tr/td[.='" + title + " " + secondName + "']");
			if (isCNamePresentAtPrimaryInfo == false) {
				fc.utobj().throwsException("was not able to verify Contact Name At Primary Info Page");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "crm", "crmadssfa"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify Change Owner By Action Bottom Btn Options At CRM > Accounts > Contacts", testCaseId = "TC_139_Change_Owner")
	public void changeOwnerBottomBtn() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			CRMAccountsPage pobj = new CRMAccountsPage(driver);

			CRMContactsPageTest contactsPage = new CRMContactsPageTest();

			AdminUsersManageRegionalUsersAddRegionalUserPageTest regionalUserPage = new AdminUsersManageRegionalUsersAddRegionalUserPageTest();
			String userNameReg = fc.utobj().generateTestData(dataSet.get("userNameReg"));
			String region = fc.utobj().generateTestData(dataSet.get("regionName"));
			String emailId = "crmautomation@staffex.com";
			regionalUserPage.addRegionalUser(driver, userNameReg, region, emailId);

			fc.utobj().printTestStep("Navigate To Admin Users Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");

			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUser = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporateUser.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			fc.utobj().printTestStep("Navigating to Admin > CRM > ContactType Configuration");
			fc.utobj().printTestStep("Add Contact Type");
			AdminCRMContactTypeConfigurationPageTest contactTypePage = new AdminCRMContactTypeConfigurationPageTest();
			String contactType = fc.utobj().generateTestData(dataSet.get("contactType"));
			contactTypePage.addContactType(driver, contactType);

			AdminCRMConfigureIndustryPageTest industryPage = new AdminCRMConfigureIndustryPageTest();
			String industry = fc.utobj().generateTestData(dataSet.get("industry"));
			String description = fc.utobj().generateTestData(dataSet.get("description"));
			fc.utobj().printTestStep("Navigating to Admin > CRM >  Configure Industry Type");
			fc.utobj().printTestStep("Add Industry");
			industryPage.addIndustry(driver, industry, description);

			String accountName = fc.utobj().generateTestData(dataSet.get("accountName"));
			String visibility = dataSet.get("visibility");
			fc.utobj().printTestStep("Navigating to CRM >  Accounts > Accounts Summary");
			fc.utobj().printTestStep("Add Account");
			addAcocunt(driver, industry, accountName, visibility);

			fc.utobj().printTestStep("Navigating to CRM >  Accounts > Accounts Summary");
			fc.utobj().printTestStep("Add Contact");

			fc.utobj().clickElement(driver, pobj.accountsLink);
			searchAccountByIndustry(driver, industry);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//a[.='" + accountName + "']"));
			fc.utobj().clickElement(driver, pobj.contactsTab);
			fc.utobj().clickElement(driver, pobj.addContact);

			String cFname = fc.utobj().generateTestData(dataSet.get("cFname"));
			String cLname = fc.utobj().generateTestData(dataSet.get("cLname"));
			String primaryCMethod = dataSet.get("primaryCMethod");
			String assignTo = "Corporate";
			String regionName = "";
			String franchiseId = "";
			String title = dataSet.get("title");
			String email = "crmautomation@staffex.com";

			contactsPage.addContactGenericWithAccount(driver, contactType, cFname, cLname, primaryCMethod, assignTo,
					corpUser.getuserFullName(), regionName, franchiseId, title, email, config, accountName);

			fc.utobj().clickElement(driver, pobj.accountsLink);
			searchAccountByIndustry(driver, industry);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//a[.='" + accountName + "']"));
			fc.utobj().clickElement(driver, pobj.contactsTab);

			fc.utobj().printTestStep("Chnage Owner");
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//a[contains(text () ,'" + cFname + " " + cLname + "')]/ancestor::tr/td/input[@name='checkb']"));
			fc.utobj().clickElement(driver, pobj.changeOwnerBtmBtn);

			fc.commonMethods().switch_cboxIframe_frameId(driver);
			if (!fc.utobj().isSelected(driver, pobj.selectRegionalUser1)) {
				fc.utobj().clickElement(driver, pobj.selectRegionalUser1);
			}
			fc.utobj().selectDropDown(driver, pobj.selectAreaRegion, region);
			fc.utobj().selectDropDown(driver, pobj.selectRegionalUser, userNameReg + " " + userNameReg);
			fc.utobj().clickElement(driver, pobj.assignBtn);
			fc.utobj().clickElement(driver, pobj.closeBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Verify Change Owner");
			boolean isOwnerPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[contains(text () ,'" + cFname + " " + cLname
					+ "')]/ancestor::tr/td[contains(text () ,'" + userNameReg + " " + userNameReg + "')]");
			if (isOwnerPresent == false) {
				fc.utobj().throwsException("was not able to verify Owner");
			}
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//a[contains(text () ,'" + cFname + " " + cLname + "')]"));

			boolean isRegionNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () ,'Area / Region')]/ancestor::tr/td[.='" + region + "']");
			if (isRegionNamePresent == false) {
				fc.utobj().throwsException("was not able to verify Region name CRM > Contact > Primary Info");
			}

			boolean isRegionalUserPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () ,'Assign To')]/ancestor::tr/td[contains(text () ,'" + userNameReg + " "
							+ userNameReg + "')]");
			if (isRegionalUserPresent == false) {
				fc.utobj().throwsException("was not able to verify Regional User 'Owner' CRM > Contact > Primary Info");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = {"crm"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Change Status By Action Bottom Btn Options At CRM > Accounts > Contacts", testCaseId = "TC_140_Change_Status")
	public void changeStatusBottomBtn() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			CRMAccountsPage pobj = new CRMAccountsPage(driver);

			CRMContactsPageTest contactsPage = new CRMContactsPageTest();
			fc.utobj().printTestStep("Navigate To Admin Users Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			String emailId = fc.utobj().generateTestData("crmautomation")+"@staffex.com";//"crmautomation@staffex.com";//vipinS
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUser = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporateUser.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);
			fc.utobj().printTestStep("Navigating to Admin > CRM > ContactType Configuration");
			fc.utobj().printTestStep("Add Contact Type");
			AdminCRMContactTypeConfigurationPageTest contactTypePage = new AdminCRMContactTypeConfigurationPageTest();
			String contactType = fc.utobj().generateTestData(dataSet.get("contactType"));
			contactTypePage.addContactType(driver, contactType);

			AdminCRMConfigureIndustryPageTest industryPage = new AdminCRMConfigureIndustryPageTest();
			String industry = fc.utobj().generateTestData(dataSet.get("industry"));
			String description = fc.utobj().generateTestData(dataSet.get("description"));
			fc.utobj().printTestStep("Navigating to Admin > CRM >  Configure Industry Type");
			fc.utobj().printTestStep("Add Industry");
			industryPage.addIndustry(driver, industry, description);

			AdminCRMConfigureStatusPageTest statusPage = new AdminCRMConfigureStatusPageTest();
			String statusName = fc.utobj().generateTestData(dataSet.get("status"));
			statusPage.addStatus01(driver, statusName);

			String accountName = fc.utobj().generateTestData(dataSet.get("accountName"));
			String visibility = dataSet.get("visibility");
			fc.utobj().printTestStep("Navigating to CRM >  Accounts > Accounts Summary");
			fc.utobj().printTestStep("Add Account");
			addAcocunt(driver, industry, accountName, visibility);

			fc.utobj().printTestStep("Navigating to CRM >  Accounts > Accounts Summary");
			fc.utobj().printTestStep("Add Contact");

			fc.utobj().clickElement(driver, pobj.accountsLink);
			searchAccountByIndustry(driver, industry);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//a[.='" + accountName + "']"));
			fc.utobj().clickElement(driver, pobj.contactsTab);
			fc.utobj().clickElement(driver, pobj.addContact);

			String cFname = fc.utobj().generateTestData(dataSet.get("cFname"));
			String cLname = fc.utobj().generateTestData(dataSet.get("cLname"));
			String primaryCMethod = dataSet.get("primaryCMethod");
			String assignTo = "Corporate";
			String regionName = "";
			String franchiseId = "";
			String title = dataSet.get("title");
			String email = fc.utobj().generateTestData("crmautomation")+"@staffex.com";//"crmautomation@staffex.com";//vipinS

			contactsPage.addContactGenericWithAccount(driver, contactType, cFname, cLname, primaryCMethod, assignTo,
					corpUser.getuserFullName(), regionName, franchiseId, title, email, config, accountName);

			/*
			 * fc.utobj().sendKeys(driver, pobj.accountName, accountName);
			 * fc.utobj().clickElement(driver, pobj.saveBtn);
			 */

			fc.utobj().clickElement(driver, pobj.accountsLink);
			searchAccountByIndustry(driver, industry);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//a[.='" + accountName + "']"));
			fc.utobj().clickElement(driver, pobj.contactsTab);

			fc.utobj().printTestStep("Change Status");
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//a[contains(text () ,'" + cFname + " " + cLname + "')]/ancestor::tr/td/input[@name='checkb']"));
			fc.utobj().clickElement(driver, pobj.changeStatusBtmBtn);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().selectDropDown(driver, pobj.statusChangeTo, statusName);
			fc.utobj().sendKeys(driver, pobj.remarksAtMerge, "Test Remarks");
			fc.utobj().clickElement(driver, pobj.changeBtn);
			fc.utobj().clickElement(driver, pobj.closeBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Verify Change Status");
			boolean isStatusPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[contains(text () ,'" + cFname + " " + cLname
					+ "')]/ancestor::tr/td[contains(text () ,'" + statusName + "')]");
			if (isStatusPresent == false) {
				fc.utobj().throwsException("was not able to verify Status Name");
			}

			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//a[contains(text () ,'" + cFname + " " + cLname + "')]"));
			boolean isStatusPresentAtInfo = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//select[@id='cmLeadStatusID']/option[@selected and .='" + statusName + "']");
			if (isStatusPresentAtInfo == false) {
				fc.utobj().throwsException("was not able to verify Status Name at Info Page");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "crm" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Contact Associate With Campaign Action Btn Options At CRM > Accounts > Contacts", testCaseId = "TC_141_Associate_With_Email_Campaign")
	public void associateWithEmailCompaignBottomBtn() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			CRMAccountsPage pobj = new CRMAccountsPage(driver);

			fc.utobj().printTestStep("Navigate To CRM >Campaign Center");
			fc.utobj().printTestStep("Add Campaign");
			CRMCampaignCenterPageTestForCRM campaignCenterPage = new CRMCampaignCenterPageTestForCRM();
			String campaignName = fc.utobj().generateTestData(dataSet.get("campaignName"));
			String campaignDescription = fc.utobj().generateTestData(dataSet.get("campaignDescription"));
			String campaignAccessibility = dataSet.get("campaignAccessibility");
			String category = dataSet.get("category");
			String templateName = fc.utobj().generateTestData(dataSet.get("templateName"));
			String mailSubject = dataSet.get("mailSubject");
			String templateText = fc.utobj().generateTestData(dataSet.get("templateText"));
			campaignCenterPage.createCampaign(driver, campaignName, campaignDescription, campaignAccessibility,
					category, templateName, mailSubject, templateText, config);

			CRMContactsPageTest contactsPage = new CRMContactsPageTest();
			fc.utobj().printTestStep("Navigate To Admin Users Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			String emailId = "crmautomation@staffex.com";
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUser = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporateUser.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);
			fc.utobj().printTestStep("Navigating to Admin > CRM > ContactType Configuration");
			fc.utobj().printTestStep("Add Contact Type");
			AdminCRMContactTypeConfigurationPageTest contactTypePage = new AdminCRMContactTypeConfigurationPageTest();
			String contactType = fc.utobj().generateTestData(dataSet.get("contactType"));
			contactTypePage.addContactType(driver, contactType);

			AdminCRMConfigureIndustryPageTest industryPage = new AdminCRMConfigureIndustryPageTest();
			String industry = fc.utobj().generateTestData(dataSet.get("industry"));
			String description = fc.utobj().generateTestData(dataSet.get("description"));
			fc.utobj().printTestStep("Navigating to Admin > CRM >  Configure Industry Type");
			fc.utobj().printTestStep("Add Industry");
			industryPage.addIndustry(driver, industry, description);

			String accountName = fc.utobj().generateTestData(dataSet.get("accountName"));
			String visibility = dataSet.get("visibility");
			fc.utobj().printTestStep("Navigating to CRM >  Accounts > Accounts Summary");
			fc.utobj().printTestStep("Add Account");
			addAcocunt(driver, industry, accountName, visibility);

			fc.utobj().printTestStep("Navigating to CRM >  Accounts > Accounts Summary");
			fc.utobj().printTestStep("Add Contact");

			fc.utobj().clickElement(driver, pobj.accountsLink);
			searchAccountByIndustry(driver, industry);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//a[.='" + accountName + "']"));
			fc.utobj().clickElement(driver, pobj.contactsTab);
			fc.utobj().clickElement(driver, pobj.addContact);

			String cFname = fc.utobj().generateTestData(dataSet.get("cFname"));
			String cLname = fc.utobj().generateTestData(dataSet.get("cLname"));
			String primaryCMethod = dataSet.get("primaryCMethod");
			String assignTo = "Corporate";
			String regionName = "";
			String franchiseId = "";
			String title = dataSet.get("title");
			String email = "crmautomation@staffex.com";

			contactsPage.addContactGenericWithAccount(driver, contactType, cFname, cLname, primaryCMethod, assignTo,
					corpUser.getuserFullName(), regionName, franchiseId, title, email, config, accountName);

			fc.utobj().clickElement(driver, pobj.accountsLink);
			searchAccountByIndustry(driver, industry);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//a[.='" + accountName + "']"));
			fc.utobj().clickElement(driver, pobj.contactsTab);

			fc.utobj().printTestStep("Associate With Campaign");
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//a[contains(text () ,'" + cFname + " " + cLname + "')]/ancestor::tr/td/input[@name='checkb']"));
			fc.utobj().clickElement(driver, pobj.associatedWithEmailCampaignBtmBtn);

			fc.utobj().clickElement(driver, pobj.filterContainer);
			fc.utobj().sendKeys(driver, pobj.searchCampaign, campaignName);
			fc.utobj().clickElement(driver, pobj.applyFilter);

			fc.utobj().clickElement(driver, /* pobj.sendCampaignBtn */new CRMLeadsPage(driver).sendCampaignBtn);
			fc.utobj().clickElement(driver, pobj.confirmBtn);

			fc.utobj().clickElement(driver, pobj.accountsLink);
			searchAccountByIndustry(driver, industry);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//a[.='" + accountName + "']"));
			fc.utobj().clickElement(driver, pobj.contactsTab);

			fc.utobj().printTestStep("Verify Contact Associate With Campaign");
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//a[contains(text () ,'" + cFname + " " + cLname + "')]"));
			boolean isCampaignNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () ,'Promotional Campaign')]/following-sibling::td[.='" + campaignName + "']");
			if (isCampaignNamePresent == false) {
				fc.utobj().throwsException("was not able to verify Campign Name");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "crm"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Contact Add To Group By More Action Btn Options At CRM > Accounts > Contacts", testCaseId = "TC_142_Add_To_Group")
	public void addToGroupMoreAction() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());
		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			CRMAccountsPage pobj = new CRMAccountsPage(driver);
			CRMGroupsPageTest groupPage = new CRMGroupsPageTest();

			CRMContactsPageTest contactsPage = new CRMContactsPageTest();
			fc.utobj().printTestStep("Navigate To Admin Users Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			String emailId = "crmautomation@staffex.com";
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUser = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporateUser.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);
			fc.utobj().printTestStep("Navigating to Admin > CRM > ContactType Configuration");
			fc.utobj().printTestStep("Add Contact Type");
			AdminCRMContactTypeConfigurationPageTest contactTypePage = new AdminCRMContactTypeConfigurationPageTest();
			String contactType = fc.utobj().generateTestData(dataSet.get("contactType"));
			contactTypePage.addContactType(driver, contactType);

			AdminCRMConfigureIndustryPageTest industryPage = new AdminCRMConfigureIndustryPageTest();
			String industry = fc.utobj().generateTestData(dataSet.get("industry"));
			String description = fc.utobj().generateTestData(dataSet.get("description"));
			fc.utobj().printTestStep("Navigating to Admin > CRM >  Configure Industry Type");
			fc.utobj().printTestStep("Add Industry");
			industryPage.addIndustry(driver, industry, description);

			String accountName = fc.utobj().generateTestData(dataSet.get("accountName"));
			String visibility = dataSet.get("visibility");
			fc.utobj().printTestStep("Navigating to CRM >  Accounts > Accounts Summary");
			fc.utobj().printTestStep("Add Account");
			addAcocunt(driver, industry, accountName, visibility);

			fc.utobj().printTestStep("Navigating to CRM >  Accounts > Accounts Summary");
			fc.utobj().printTestStep("Add Contact");

			fc.utobj().clickElement(driver, pobj.accountsLink);
			searchAccountByIndustry(driver, industry);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//a[.='" + accountName + "']"));
			fc.utobj().clickElement(driver, pobj.contactsTab);
			fc.utobj().clickElement(driver, pobj.addContact);

			String cFname = fc.utobj().generateTestData(dataSet.get("cFname"));
			String cLname = fc.utobj().generateTestData(dataSet.get("cLname"));
			String primaryCMethod = dataSet.get("primaryCMethod");
			String assignTo = "Corporate";
			String regionName = "";
			String franchiseId = "";
			String title = dataSet.get("title");
			//String email = "crmautomation@staffex.com";
			String email= fc.utobj().generateTestData("crmautomation")+"@staffex.com";
			contactsPage.addContactGenericWithAccount(driver, contactType, cFname, cLname, primaryCMethod, assignTo,
					corpUser.getuserFullName(), regionName, franchiseId, title, email, config, accountName);

			fc.utobj().clickElement(driver, pobj.accountsLink);
			searchAccountByIndustry(driver, industry);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//a[.='" + accountName + "']"));
			fc.utobj().clickElement(driver, pobj.contactsTab);

			fc.utobj().printTestStep("Add To Group");
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//a[contains(text () ,'" + cFname + " " + cLname + "')]/ancestor::tr/td/input[@name='checkb']"));
			selectActionMenuItemsWithTagAMoreActions(driver, "Add To Group");

			fc.utobj().printTestStep("Create Group");
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().clickElement(driver, pobj.addToNewGroup);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Create New Group");
			String groupName = fc.utobj().generateTestData(dataSet.get("groupName"));
			String accessibility = "Public to all Users";
			groupPage.addToGroupRegular(driver, groupName, accessibility);

			/*
			 * fc.utobj().sendKeys(driver, pobj.searchAtGroup, groupName);
			 * fc.utobj().clickElement(driver, pobj.exactSearchGroup);
			 * fc.utobj().clickElement(driver, pobj.searchAccountBtn);
			 */

			new CRMGroupsPageTest().searchGroupsByFilter(driver, groupName, "Contact");

			String colunmName = "";
			int z = 0;

			// click over Recipients
			List<WebElement> listHead = driver
					.findElements(By.xpath(".//a[.='" + groupName + "']/ancestor::table/thead/tr/th"));
			System.out.println(listHead);
			for (int i = 0; i < listHead.size(); i++) {

				colunmName = listHead.get(i).getText().trim();
				if (colunmName.equalsIgnoreCase("Recipients")) {
					z = i;
					break;
				}
			}

			if (z != 0) {
				z = z + 1;
				fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
						".//a[.='" + groupName + "']/ancestor::tr/td[" + z + "]/a"));
			} else {
				fc.utobj().throwsException("was not able to find Recipients Column");
			}

			/*searchContactByContactType(driver, contactType);
			fc.utobj().printTestStep("Verify Contact In Group");
			boolean isContactNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + cFname + " " + cLname + "')]");
			if (isContactNamePresent == false) {
				fc.utobj().throwsException("was not able to add in Group");
			}*/

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = {"crm"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Archive Contacts By More Action Btn Options At CRM > Accounts > Contacts", testCaseId = "TC_143_Archive_Contact")
	public void acrhiveContactsMoreAction() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			CRMAccountsPage pobj = new CRMAccountsPage(driver);

			CRMContactsPageTest contactsPage = new CRMContactsPageTest();
			fc.utobj().printTestStep("Navigate To Admin Users Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			String emailId = "crmautomation@staffex.com";
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUser = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporateUser.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);
			fc.utobj().printTestStep("Navigating to Admin > CRM > ContactType Configuration");
			fc.utobj().printTestStep("Add Contact Type");
			AdminCRMContactTypeConfigurationPageTest contactTypePage = new AdminCRMContactTypeConfigurationPageTest();
			String contactType = fc.utobj().generateTestData(dataSet.get("contactType"));
			contactTypePage.addContactType(driver, contactType);

			AdminCRMConfigureIndustryPageTest industryPage = new AdminCRMConfigureIndustryPageTest();
			String industry = fc.utobj().generateTestData(dataSet.get("industry"));
			String description = fc.utobj().generateTestData(dataSet.get("description"));
			fc.utobj().printTestStep("Navigating to Admin > CRM >  Configure Industry Type");
			fc.utobj().printTestStep("Add Industry");
			industryPage.addIndustry(driver, industry, description);

			String accountName = fc.utobj().generateTestData(dataSet.get("accountName"));
			String visibility = dataSet.get("visibility");
			fc.utobj().printTestStep("Navigating to CRM >  Accounts > Accounts Summary");
			fc.utobj().printTestStep("Add Account");
			addAcocunt(driver, industry, accountName, visibility);

			fc.utobj().printTestStep("Navigating to CRM >  Accounts > Accounts Summary");
			fc.utobj().printTestStep("Add Contact");

			fc.utobj().clickElement(driver, pobj.accountsLink);
			searchAccountByIndustry(driver, industry);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//a[.='" + accountName + "']"));
			fc.utobj().clickElement(driver, pobj.contactsTab);
			fc.utobj().clickElement(driver, pobj.addContact);

			String cFname = fc.utobj().generateTestData(dataSet.get("cFname"));
			String cLname = fc.utobj().generateTestData(dataSet.get("cLname"));
			String primaryCMethod = dataSet.get("primaryCMethod");
			String assignTo = "Corporate";
			String regionName = "";
			String franchiseId = "";
			String title = dataSet.get("title");
			String email = "crmautomation@staffex.com";

			contactsPage.addContactGenericWithAccount(driver, contactType, cFname, cLname, primaryCMethod, assignTo,
					corpUser.getuserFullName(), regionName, franchiseId, title, email, config, accountName);

			fc.utobj().clickElement(driver, pobj.accountsLink);
			searchAccountByIndustry(driver, industry);

			fc.utobj().printTestStep("Archive Contact");
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//a[.='" + accountName + "']"));
			fc.utobj().clickElement(driver, pobj.contactsTab);

			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//a[contains(text () ,'" + cFname + " " + cLname + "')]/ancestor::tr/td/input[@name='checkb']"));
			selectActionMenuItemsWithTagAMoreActions(driver, "Archive Contacts");
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().clickElement(driver, ".//input[@value ='Archive']");
			fc.commonMethods().Click_Close_Input_ByValue(driver);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Verify Archive Contact At Accounts > Contacts");
			boolean isNoRecordPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//td[.='No records found.']");
			if (isNoRecordPresent == false) {
				fc.utobj().throwsException("was not able to Archive Contact");
			}
			fc.utobj().clickElement(driver, pobj.searchLink);
			fc.utobj().clickElement(driver, pobj.contactsTabAtSearch);
			if (!fc.utobj().isSelected(driver, pobj.archiveContacts)) {
				fc.utobj().clickElement(driver, pobj.archiveContacts);
			}
			/*fc.utobj().setToDefault(driver, pobj.assignTo);
			WebElement elementInput = fc.utobj().getElement(driver, pobj.assignTo)
					.findElement(By.xpath("./div/div/input"));
			fc.utobj().selectValFromMultiSelect(driver, pobj.assignTo, elementInput, corpUser.getuserFullName());*/
			fc.utobj().clickElement(driver, pobj.searchBtnAtSearch);

			fc.utobj().printTestStep("Verify Archive Contact At Search");
			boolean isNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + cFname + " " + cLname + "')]");
			if (isNamePresent == false) {
				fc.utobj().throwsException("was not able to verify Archive Contacts");
			}
			boolean isOwnerPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[contains(text () ,'" + cFname + " " + cLname
					+ "')]/ancestor::tr/td[contains(text () , '" + corpUser.getuserFullName() + "')]");
			if (isOwnerPresent == false) {
				fc.utobj().throwsException("was not able to verify Owner of Archive Contacts");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "crm")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Change Contact Type By More Action Btn Options At CRM > Accounts > Contacts", testCaseId = "TC_144_Change_Contact_Type")
	public void changeContactTypeMoreAction() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			CRMAccountsPage pobj = new CRMAccountsPage(driver);

			CRMContactsPageTest contactsPage = new CRMContactsPageTest();
			fc.utobj().printTestStep("Navigate To Admin Users Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			String emailId = "crmautomation@staffex.com";
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUser = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporateUser.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);
			fc.utobj().printTestStep("Navigating to Admin > CRM > ContactType Configuration");
			fc.utobj().printTestStep("Add Contact Type");
			AdminCRMContactTypeConfigurationPageTest contactTypePage = new AdminCRMContactTypeConfigurationPageTest();
			String contactType = fc.utobj().generateTestData(dataSet.get("contactType"));
			contactTypePage.addContactType(driver, contactType);

			AdminCRMConfigureIndustryPageTest industryPage = new AdminCRMConfigureIndustryPageTest();
			String industry = fc.utobj().generateTestData(dataSet.get("industry"));
			String description = fc.utobj().generateTestData(dataSet.get("description"));
			fc.utobj().printTestStep("Navigating to Admin > CRM >  Configure Industry Type");
			fc.utobj().printTestStep("Add Industry");
			industryPage.addIndustry(driver, industry, description);

			fc.utobj().printTestStep("Navigating to Admin > CRM > ContactType Configuration");
			fc.utobj().printTestStep("Add Contact Type");
			String contactType1 = fc.utobj().generateTestData(dataSet.get("contactType"));
			contactTypePage.addContactType(driver, contactType1);

			String accountName = fc.utobj().generateTestData(dataSet.get("accountName"));
			String visibility = dataSet.get("visibility");
			fc.utobj().printTestStep("Navigating to CRM >  Accounts > Accounts Summary");
			fc.utobj().printTestStep("Add Account");
			addAcocunt(driver, industry, accountName, visibility);

			fc.utobj().printTestStep("Navigating to CRM >  Accounts > Accounts Summary");
			fc.utobj().printTestStep("Add Contact");

			fc.utobj().clickElement(driver, pobj.accountsLink);
			searchAccountByIndustry(driver, industry);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//a[.='" + accountName + "']"));
			fc.utobj().clickElement(driver, pobj.contactsTab);
			fc.utobj().clickElement(driver, pobj.addContact);

			String cFname = fc.utobj().generateTestData(dataSet.get("cFname"));
			String cLname = fc.utobj().generateTestData(dataSet.get("cLname"));
			String primaryCMethod = dataSet.get("primaryCMethod");
			String assignTo = "Corporate";
			String regionName = "";
			String franchiseId = "";
			String title = dataSet.get("title");
			String email = "crmautomation@staffex.com";

			contactsPage.addContactGenericWithAccount(driver, contactType, cFname, cLname, primaryCMethod, assignTo,
					corpUser.getuserFullName(), regionName, franchiseId, title, email, config, accountName);

			fc.utobj().clickElement(driver, pobj.accountsLink);
			searchAccountByIndustry(driver, industry);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//a[.='" + accountName + "']"));
			fc.utobj().clickElement(driver, pobj.contactsTab);

			fc.utobj().printTestStep("Change Contact Type");
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//a[contains(text () ,'" + cFname + " " + cLname + "')]/ancestor::tr/td/input[@name='checkb']"));
			selectActionMenuItemsWithTagAMoreActions(driver, "Change Contact Type");

			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().selectDropDown(driver, pobj.contactType, contactType1);
			fc.utobj().clickElement(driver, pobj.changeBtn);
			fc.utobj().clickElement(driver, pobj.closeBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Verify Change Contact Type");
			boolean isStatusPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[contains(text () ,'" + cFname + " " + cLname
					+ "')]/ancestor::tr/td[contains(text () ,'" + contactType1 + "')]");
			if (isStatusPresent == false) {
				fc.utobj().throwsException("was not able to verify Contact Type");
			}

			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//a[contains(text () ,'" + cFname + " " + cLname + "')]"));
			boolean isStatusPresentAtInfo = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () ,'Contact Type')]/ancestor::tr/td[.='" + contactType1 + "']");
			if (isStatusPresentAtInfo == false) {
				fc.utobj().throwsException("was not able to verify Contact Type at Info Page");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = {"crm"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Delete Contacts By More Action Btn Options At CRM > Accounts > Contacts", testCaseId = "TC_145_Delete_Contact")
	public void deleteContactMoreAction() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			CRMAccountsPage pobj = new CRMAccountsPage(driver);

			CRMContactsPageTest contactsPage = new CRMContactsPageTest();
			fc.utobj().printTestStep("Navigate To Admin Users Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			String emailId = "crmautomation@staffex.com";
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUser = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporateUser.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);
			fc.utobj().printTestStep("Navigating to Admin > CRM > ContactType Configuration");
			fc.utobj().printTestStep("Add Contact Type");
			AdminCRMContactTypeConfigurationPageTest contactTypePage = new AdminCRMContactTypeConfigurationPageTest();
			String contactType = fc.utobj().generateTestData(dataSet.get("contactType"));
			contactTypePage.addContactType(driver, contactType);

			AdminCRMConfigureIndustryPageTest industryPage = new AdminCRMConfigureIndustryPageTest();
			String industry = fc.utobj().generateTestData(dataSet.get("industry"));
			String description = fc.utobj().generateTestData(dataSet.get("description"));
			fc.utobj().printTestStep("Navigating to Admin > CRM >  Configure Industry Type");
			fc.utobj().printTestStep("Add Industry");
			industryPage.addIndustry(driver, industry, description);

			String accountName = fc.utobj().generateTestData(dataSet.get("accountName"));
			String visibility = dataSet.get("visibility");
			fc.utobj().printTestStep("Navigating to CRM >  Accounts > Accounts Summary");
			fc.utobj().printTestStep("Add Account");
			addAcocunt(driver, industry, accountName, visibility);

			fc.utobj().printTestStep("Navigating to CRM >  Accounts > Accounts Summary");
			fc.utobj().printTestStep("Add Contact");

			fc.utobj().clickElement(driver, pobj.accountsLink);
			searchAccountByIndustry(driver, industry);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//a[.='" + accountName + "']"));
			fc.utobj().clickElement(driver, pobj.contactsTab);
			fc.utobj().clickElement(driver, pobj.addContact);

			String cFname = fc.utobj().generateTestData(dataSet.get("cFname"));
			String cLname = fc.utobj().generateTestData(dataSet.get("cLname"));
			String primaryCMethod = dataSet.get("primaryCMethod");
			String assignTo = "Corporate";
			String regionName = "";
			String franchiseId = "";
			String title = dataSet.get("title");
			String email = "crmautomation@staffex.com";

			contactsPage.addContactGenericWithAccount(driver, contactType, cFname, cLname, primaryCMethod, assignTo,
					corpUser.getuserFullName(), regionName, franchiseId, title, email, config, accountName);

			fc.utobj().clickElement(driver, pobj.accountsLink);
			searchAccountByIndustry(driver, industry);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//a[.='" + accountName + "']"));
			fc.utobj().clickElement(driver, pobj.contactsTab);

			fc.utobj().printTestStep("Detete Contact");
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//a[contains(text () ,'" + cFname + " " + cLname + "')]/ancestor::tr/td/input[@name='checkb']"));
			selectActionMenuItemsWithTagAMoreActions(driver, "Delete");
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().clickElement(driver, ".//input[@value = 'Delete']");
			fc.utobj().clickElement(driver, ".//input[@value = 'Close']");

			fc.utobj().printTestStep("Verify Delete Contact");
			boolean isNoRecordPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//td[.='No records found.']");
			if (isNoRecordPresent == false) {
				fc.utobj().throwsException("was not able to Delete Contact");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "crm")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Add Opportunity At CRM > Accounts > Opportunities", testCaseId = "TC_146_Add_Opportunity")
	public void addOpportunity() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			CRMAccountsPage pobj = new CRMAccountsPage(driver);

			CRMContactsPageTest contactsPage = new CRMContactsPageTest();
			fc.utobj().printTestStep("Navigate To Admin Users Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			String emailId = "crmautomation@staffex.com";
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUser = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporateUser.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);
			fc.utobj().printTestStep("Navigating to Admin > CRM > ContactType Configuration");
			fc.utobj().printTestStep("Add Contact Type");
			AdminCRMContactTypeConfigurationPageTest contactTypePage = new AdminCRMContactTypeConfigurationPageTest();
			String contactType = fc.utobj().generateTestData(dataSet.get("contactType"));
			contactTypePage.addContactType(driver, contactType);
			fc.utobj().printTestStep("Navigating to Admin > CRM >  Configure Opportunity Stages");
			fc.utobj().printTestStep("Add Stage");
			AdminCRMConfigureOpportunityStagesPageTest stagePage = new AdminCRMConfigureOpportunityStagesPageTest();
			String stage = fc.utobj().generateTestData(dataSet.get("stage"));
			stagePage.addStage(driver, stage);

			AdminCRMConfigureIndustryPageTest industryPage = new AdminCRMConfigureIndustryPageTest();
			String industry = fc.utobj().generateTestData(dataSet.get("industry"));
			String description = fc.utobj().generateTestData(dataSet.get("description"));
			fc.utobj().printTestStep("Navigating to Admin > CRM >  Configure Industry Type");
			fc.utobj().printTestStep("Add Industry");
			industryPage.addIndustry(driver, industry, description);
			String accountName = fc.utobj().generateTestData(dataSet.get("accountName"));
			String visibility = dataSet.get("visibility");
			fc.utobj().printTestStep("Navigating to CRM >  Accounts > Accounts Summary");
			fc.utobj().printTestStep("Add Account");
			addAcocunt(driver, industry, accountName, visibility);

			fc.utobj().printTestStep("Navigating to CRM >  Accounts > Accounts Summary");
			fc.utobj().printTestStep("Add Contact");

			fc.utobj().clickElement(driver, pobj.accountsLink);
			searchAccountByIndustry(driver, industry);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//a[.='" + accountName + "']"));
			fc.utobj().clickElement(driver, pobj.contactsTab);
			fc.utobj().clickElement(driver, pobj.addContact);

			String cFname = fc.utobj().generateTestData(dataSet.get("cFname"));
			String cLname = fc.utobj().generateTestData(dataSet.get("cLname"));
			String primaryCMethod = dataSet.get("primaryCMethod");
			String assignTo = "Corporate";
			String regionName = "";
			String franchiseId = "";
			String title = dataSet.get("title");
			String email = "crmautomation@staffex.com";

			contactsPage.addContactGenericWithAccount(driver, contactType, cFname, cLname, primaryCMethod, assignTo,
					corpUser.getuserFullName(), regionName, franchiseId, title, email, config, accountName);  //corpUser.getuserFullName()

			fc.utobj().clickElement(driver, pobj.accountsLink);
			searchAccountByIndustry(driver, industry);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//a[.='" + accountName + "']"));
			fc.utobj().clickElement(driver, pobj.opportunitiesTab);
			fc.utobj().printTestStep("Add Opportunity");

			String opportunityName = fc.utobj().generateTestData(dataSet.get("opportunityName"));
			String contactName = cFname + " " + cLname;
			String opportunityType = dataSet.get("opportunityType");
			String opportunitySource = dataSet.get("opportunitySource");
			String salesAmount = dataSet.get("salesAmount");
			fc.utobj().clickElement(driver, driver.findElement(By.linkText("Add Opportunity")));
			addOpportunity(driver, opportunityName, accountName, contactName, corpUser.getuserFullName(), stage,   //corpUser.getuserFullName()
					opportunityType, opportunitySource, salesAmount, config);

			fc.utobj().clickElement(driver, pobj.accountsLink);
			searchAccountByIndustry(driver, industry);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//a[.='" + accountName + "']"));
			fc.utobj().clickElement(driver, pobj.opportunitiesTab);

			fc.utobj().printTestStep("Verify Add Opportunity");
			boolean isOpportunityPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[.='" + opportunityName + "']");
			if (isOpportunityPresent == false) {
				fc.utobj().throwsException("was not able to verify Opportunity Name");
			}
			boolean isAssignToPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[.='" + opportunityName
					+ "']/ancestor::tr/td[contains(text () , '" + corpUser.getuserFullName() + "')]");
			if (isAssignToPresent == false) {
				fc.utobj().throwsException("was not able to verify Assign To");
			}
			boolean isAccountNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[.='" + opportunityName + "']/ancestor::tr/td/a[contains(text () , '" + accountName + "')]");
			if (isAccountNamePresent == false) {
				fc.utobj().throwsException("was not able to verify Account");
			}
			boolean isContactNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[.='" + opportunityName + "']/ancestor::tr/td/a[contains(text () , '" + contactName + "')]");
			if (isContactNamePresent == false) {
				fc.utobj().throwsException("was not able to verify Contact");
			}
			boolean isStagePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[.='" + opportunityName + "']/ancestor::tr/td[contains(text () , '" + stage + "')]");
			if (isStagePresent == false) {
				fc.utobj().throwsException("was not able to verify Stage");
			}
			boolean isSalesAmountPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[.='" + opportunityName
					+ "']/ancestor::tr/td[contains(text () , '" + salesAmount.concat(".00") + "')]");
			if (isSalesAmountPresent == false) {
				fc.utobj().throwsException("was not able to verify Sales Amount");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = {"crm"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Modify Opportunity At CRM > Accounts > Opportunities", testCaseId = "TC_147_Modify_Opportunity")
	public void modifyOpportunityActionImage() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			CRMAccountsPage pobj = new CRMAccountsPage(driver);

			CRMContactsPageTest contactsPage = new CRMContactsPageTest();
			fc.utobj().printTestStep("Navigate To Admin Users Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			String emailId = "crmautomation@staffex.com";
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUser = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporateUser.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);
			fc.utobj().printTestStep("Navigating to Admin > CRM > ContactType Configuration");
			fc.utobj().printTestStep("Add Contact Type");
			AdminCRMContactTypeConfigurationPageTest contactTypePage = new AdminCRMContactTypeConfigurationPageTest();
			String contactType = fc.utobj().generateTestData(dataSet.get("contactType"));
			contactTypePage.addContactType(driver, contactType);
			fc.utobj().printTestStep("Navigating to Admin > CRM >  Configure Opportunity Stages");
			fc.utobj().printTestStep("Add Stage");
			AdminCRMConfigureOpportunityStagesPageTest stagePage = new AdminCRMConfigureOpportunityStagesPageTest();
			String stage = fc.utobj().generateTestData(dataSet.get("stage"));
			stagePage.addStage(driver, stage);

			AdminCRMConfigureIndustryPageTest industryPage = new AdminCRMConfigureIndustryPageTest();
			String industry = fc.utobj().generateTestData(dataSet.get("industry"));
			String description = fc.utobj().generateTestData(dataSet.get("description"));
			fc.utobj().printTestStep("Navigating to Admin > CRM >  Configure Industry Type");
			fc.utobj().printTestStep("Add Industry");
			industryPage.addIndustry(driver, industry, description);

			String accountName = fc.utobj().generateTestData(dataSet.get("accountName"));
			String visibility = dataSet.get("visibility");
			fc.utobj().printTestStep("Navigating to CRM >  Accounts > Accounts Summary");
			fc.utobj().printTestStep("Add Account");
			addAcocunt(driver, industry, accountName, visibility);

			fc.utobj().printTestStep("Navigating to CRM >  Accounts > Accounts Summary");
			fc.utobj().printTestStep("Add Contact");

			fc.utobj().clickElement(driver, pobj.accountsLink);
			searchAccountByIndustry(driver, industry);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//a[.='" + accountName + "']"));
			fc.utobj().clickElement(driver, pobj.contactsTab);
			fc.utobj().clickElement(driver, pobj.addContact);

			String cFname = fc.utobj().generateTestData(dataSet.get("cFname"));
			String cLname = fc.utobj().generateTestData(dataSet.get("cLname"));
			String primaryCMethod = dataSet.get("primaryCMethod");
			String assignTo = "Corporate";
			String regionName = "";
			String franchiseId = "";
			String title = dataSet.get("title");
			String email = "crmautomation@staffex.com";

			contactsPage.addContactGenericWithAccount(driver, contactType, cFname, cLname, primaryCMethod, assignTo,
					corpUser.getuserFullName(), regionName, franchiseId, title, email, config, accountName);

			fc.utobj().printTestStep("Add Opportunity");
			fc.utobj().clickElement(driver, pobj.accountsLink);
			searchAccountByIndustry(driver, industry);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//a[.='" + accountName + "']"));
			fc.utobj().clickElement(driver, pobj.opportunitiesTab);
			String opportunityName = fc.utobj().generateTestData(dataSet.get("opportunityName"));
			String contactName = cFname + " " + cLname;
			String opportunityType = dataSet.get("opportunityType");
			String opportunitySource = dataSet.get("opportunitySource");
			String salesAmount = dataSet.get("salesAmount");
			fc.utobj().clickElement(driver, driver.findElement(By.xpath("//a[contains(text(),'Add Opportunity')]")));
			addOpportunity(driver, opportunityName, accountName, contactName, corpUser.getuserFullName(), stage,
					opportunityType, opportunitySource, salesAmount, config);

			fc.utobj().clickElement(driver, pobj.accountsLink);
			searchAccountByIndustry(driver, industry);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//a[.='" + accountName + "']"));
			fc.utobj().clickElement(driver, pobj.opportunitiesTab);

			// modify Opp
			fc.utobj().printTestStep("Modify Opportunity");
			fc.utobj().actionImgOption(driver, opportunityName, "Modify");
			opportunityName = fc.utobj().generateTestData(dataSet.get("opportunityName"));
			fc.utobj().sendKeys(driver, pobj.opportunityName, opportunityName);
			// fc.utobj().selectDropDown(driver, pobj.opportunityType,
			// opportunityType);
			fc.utobj().selectDropDown(driver, pobj.opportunitySource, opportunitySource);
			fc.utobj().clickElement(driver, pobj.saveBtn);

			fc.utobj().clickElement(driver, pobj.accountsLink);
			searchAccountByIndustry(driver, industry);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//a[.='" + accountName + "']"));
			fc.utobj().clickElement(driver, pobj.opportunitiesTab);

			fc.utobj().printTestStep("Verify Modify Opportunity");
			boolean isOpportunityPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[.='" + opportunityName + "']");
			if (isOpportunityPresent == false) {
				fc.utobj().throwsException("was not able to verify Opportunity Name");
			}
			boolean isAssignToPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[.='" + opportunityName
					+ "']/ancestor::tr/td[contains(text () , '" + corpUser.getuserFullName() + "')]");
			if (isAssignToPresent == false) {
				fc.utobj().throwsException("was not able to verify Assign To");
			}
			boolean isAccountNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[.='" + opportunityName + "']/ancestor::tr/td/a[contains(text () , '" + accountName + "')]");
			if (isAccountNamePresent == false) {
				fc.utobj().throwsException("was not able to verify Account");
			}
			boolean isContactNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[.='" + opportunityName + "']/ancestor::tr/td/a[contains(text () , '" + contactName + "')]");
			if (isContactNamePresent == false) {
				fc.utobj().throwsException("was not able to verify Contact");
			}
			boolean isStagePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[.='" + opportunityName + "']/ancestor::tr/td[contains(text () , '" + stage + "')]");
			if (isStagePresent == false) {
				fc.utobj().throwsException("was not able to verify Stage");
			}
			boolean isSalesAmountPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[.='" + opportunityName
					+ "']/ancestor::tr/td[contains(text () , '" + salesAmount.concat(".00") + "')]");
			if (isSalesAmountPresent == false) {
				fc.utobj().throwsException("was not able to verify Sales Amount");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "crm"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2017-08-09", testCaseDescription = "Verify The Add Transaction At CRM > Accounts > Opportunities", testCaseId = "TC_148_Add_Transaction")
	public void addTransactionActionImage() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			CRMAccountsPage pobj = new CRMAccountsPage(driver);

			CRMContactsPageTest contactsPage = new CRMContactsPageTest();

			String description = fc.utobj().generateTestData(dataSet.get("description"));
			AdminCRMManageProductServiceCategoryPageTest serviceProductPage = new AdminCRMManageProductServiceCategoryPageTest();
			String categoryName = fc.utobj().generateTestData(dataSet.get("categoryName"));
			String productName = fc.utobj().generateTestData(dataSet.get("productName"));
			String oneLineDescription = fc.utobj().generateTestData(dataSet.get("oneLineDescription"));
			String rate = dataSet.get("rate");
			fc.utobj().printTestStep("Navigating to Admin > CRM >  Manage Product / Service & Category");

			fc.utobj().printTestStep("Add Category");

			serviceProductPage.addCategory(driver, categoryName, description);
			fc.utobj().printTestStep("Add Product/Service");
			serviceProductPage.addProductService(driver, categoryName, productName, oneLineDescription, rate);

			fc.utobj().printTestStep("Navigate To Admin Users Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			String emailId = "crmautomation@staffex.com";
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUser = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporateUser.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);
			fc.utobj().printTestStep("Navigating to Admin > CRM > ContactType Configuration");
			fc.utobj().printTestStep("Add Contact Type");
			AdminCRMContactTypeConfigurationPageTest contactTypePage = new AdminCRMContactTypeConfigurationPageTest();
			String contactType = fc.utobj().generateTestData(dataSet.get("contactType"));
			contactTypePage.addContactType(driver, contactType);
			fc.utobj().printTestStep("Navigating to Admin > CRM >  Configure Opportunity Stages");
			fc.utobj().printTestStep("Add Stage");
			AdminCRMConfigureOpportunityStagesPageTest stagePage = new AdminCRMConfigureOpportunityStagesPageTest();
			String stage = fc.utobj().generateTestData(dataSet.get("stage"));
			stagePage.addStage(driver, stage);

			AdminCRMConfigureIndustryPageTest industryPage = new AdminCRMConfigureIndustryPageTest();
			String industry = fc.utobj().generateTestData(dataSet.get("industry"));
			description = fc.utobj().generateTestData(dataSet.get("description"));
			fc.utobj().printTestStep("Navigating to Admin > CRM >  Configure Industry Type");
			fc.utobj().printTestStep("Add Industry");
			industryPage.addIndustry(driver, industry, description);

			String accountName = fc.utobj().generateTestData(dataSet.get("accountName"));
			String visibility = dataSet.get("visibility");
			fc.utobj().printTestStep("Navigating to CRM >  Accounts > Accounts Summary");
			fc.utobj().printTestStep("Add Account");
			addAcocunt(driver, industry, accountName, visibility);

			fc.utobj().printTestStep("Navigating to CRM >  Accounts > Accounts Summary");
			fc.utobj().printTestStep("Add Contact");

			fc.utobj().clickElement(driver, pobj.accountsLink);
			searchAccountByIndustry(driver, industry);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//a[.='" + accountName + "']"));
			fc.utobj().clickElement(driver, pobj.contactsTab);
			fc.utobj().clickElement(driver, pobj.addContact);

			String cFname = fc.utobj().generateTestData(dataSet.get("cFname"));
			String cLname = fc.utobj().generateTestData(dataSet.get("cLname"));
			String primaryCMethod = dataSet.get("primaryCMethod");
			String assignTo = "Corporate";
			String regionName = "";
			String franchiseId = "";
			String title = dataSet.get("title");
			String email = "crmautomation@staffex.com";

			contactsPage.addContactGenericWithAccount(driver, contactType, cFname, cLname, primaryCMethod, assignTo,
					corpUser.getuserFullName(), regionName, franchiseId, title, email, config, accountName);

			fc.utobj().printTestStep("Add Opportunity");
			fc.utobj().clickElement(driver, pobj.accountsLink);
			searchAccountByIndustry(driver, industry);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//a[.='" + accountName + "']"));
			fc.utobj().clickElement(driver, pobj.opportunitiesTab);
			String opportunityName = fc.utobj().generateTestData(dataSet.get("opportunityName"));
			String contactName = cFname + " " + cLname;
			String opportunityType = dataSet.get("opportunityType");
			String opportunitySource = dataSet.get("opportunitySource");
			String salesAmount = dataSet.get("salesAmount");
			fc.utobj().clickElement(driver, driver.findElement(By.linkText("Add Opportunity")));
			addOpportunity(driver, opportunityName, accountName, contactName, corpUser.getuserFullName(), stage,
					opportunityType, opportunitySource, salesAmount, config);

			fc.utobj().clickElement(driver, pobj.accountsLink);
			searchAccountByIndustry(driver, industry);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//a[.='" + accountName + "']"));
			fc.utobj().clickElement(driver, pobj.opportunitiesTab);

			// Add Trasnsaction Opp
			fc.utobj().printTestStep("Add Transaction");
			fc.utobj().actionImgOption(driver, opportunityName, "Add Transaction");
			String invoiceDate = fc.utobj().getCurrentDateUSFormat();
			fc.utobj().sendKeys(driver, pobj.invoiceDate, invoiceDate);
			fc.utobj().clickElement(driver, pobj.selectProductAndService);
			fc.utobj().clickElement(driver, pobj.uncheckAll);
			fc.utobj().sendKeys(driver, pobj.searchProduct, productName);
			fc.utobj().clickElement(driver, pobj.checkAll);
			fc.utobj().clickElement(driver, pobj.okayBtn);
			fc.utobj().clickElement(driver, pobj.submitBtn);

			fc.utobj().clickElement(driver, pobj.accountsLink);
			searchAccountByIndustry(driver, industry);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//a[.='" + accountName + "']"));
			fc.utobj().clickElement(driver, pobj.transactionTab);

			fc.utobj().printTestStep("Verify Transaction");
			boolean isOppPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () , '" + opportunityName + "')]");
			if (isOppPresent == false) {
				fc.utobj().throwsException("was not able to verify Transaction");
			}

			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,(".//td[contains(text () , '" + opportunityName + "')]/preceding-sibling::td/a")));

			boolean isProductAvailable = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () ,'" + productName + "')]");
			if (isProductAvailable == false) {
				fc.utobj().throwsException("was not able to verify Product");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = {"crm","helloworld220"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Log A Task At CRM > Accounts > Opportunities", testCaseId = "TC_149_Log_A_Task")
	public void logATaskActionImageOpp() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			CRMAccountsPage pobj = new CRMAccountsPage(driver);

			CRMContactsPageTest contactsPage = new CRMContactsPageTest();
			fc.utobj().printTestStep("Navigate To Admin Users Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			String emailId = "crmautomation@staffex.com";
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUser = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporateUser.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);
			fc.utobj().printTestStep("Navigating to Admin > CRM > ContactType Configuration");
			fc.utobj().printTestStep("Add Contact Type");
			AdminCRMContactTypeConfigurationPageTest contactTypePage = new AdminCRMContactTypeConfigurationPageTest();
			String contactType = fc.utobj().generateTestData(dataSet.get("contactType"));
			contactTypePage.addContactType(driver, contactType);
			fc.utobj().printTestStep("Navigating to Admin > CRM >  Configure Opportunity Stages");
			fc.utobj().printTestStep("Add Stage");
			AdminCRMConfigureOpportunityStagesPageTest stagePage = new AdminCRMConfigureOpportunityStagesPageTest();
			String stage = fc.utobj().generateTestData(dataSet.get("stage"));
			stagePage.addStage(driver, stage);

			AdminCRMConfigureIndustryPageTest industryPage = new AdminCRMConfigureIndustryPageTest();
			String industry = fc.utobj().generateTestData(dataSet.get("industry"));
			String description = fc.utobj().generateTestData(dataSet.get("description"));
			fc.utobj().printTestStep("Navigating to Admin > CRM >  Configure Industry Type");
			fc.utobj().printTestStep("Add Industry");
			industryPage.addIndustry(driver, industry, description);

			String accountName = fc.utobj().generateTestData(dataSet.get("accountName"));
			String visibility = dataSet.get("visibility");
			fc.utobj().printTestStep("Navigating to CRM >  Accounts > Accounts Summary");
			fc.utobj().printTestStep("Add Account");
			addAcocunt(driver, industry, accountName, visibility);

			fc.utobj().printTestStep("Navigating to CRM >  Accounts > Accounts Summary");
			fc.utobj().printTestStep("Add Contact");

			fc.utobj().clickElement(driver, pobj.accountsLink);
			searchAccountByIndustry(driver, industry);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//a[.='" + accountName + "']"));
			fc.utobj().clickElement(driver, pobj.contactsTab);
			fc.utobj().clickElement(driver, pobj.addContact);

			String cFname = fc.utobj().generateTestData(dataSet.get("cFname"));
			String cLname = fc.utobj().generateTestData(dataSet.get("cLname"));
			String primaryCMethod = dataSet.get("primaryCMethod");
			String assignTo = "Corporate";
			String regionName = "";
			String franchiseId = "";
			String title = dataSet.get("title");
			//String email = "crmautomation@staffex.com";
			String email=fc.utobj().generateTestData("crmautomation")+"@staffex.com";
			contactsPage.addContactGenericWithAccount(driver, contactType, cFname, cLname, primaryCMethod, assignTo,
					corpUser.getuserFullName(), regionName, franchiseId, title, email, config, accountName);

			fc.utobj().printTestStep("Add Opportunity");
			fc.utobj().clickElement(driver, pobj.accountsLink);
			searchAccountByIndustry(driver, industry);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//a[.='" + accountName + "']"));
			fc.utobj().clickElement(driver, pobj.opportunitiesTab);
			String opportunityName = fc.utobj().generateTestData(dataSet.get("opportunityName"));
			String contactName = cFname + " " + cLname;
			String opportunityType = dataSet.get("opportunityType");
			String opportunitySource = dataSet.get("opportunitySource");
			String salesAmount = dataSet.get("salesAmount");
			fc.utobj().clickElement(driver, driver.findElement(By.linkText("Add Opportunity")));
			addOpportunity(driver, opportunityName, accountName, contactName, corpUser.getuserFullName(), stage,
					opportunityType, opportunitySource, salesAmount, config);

			fc.utobj().clickElement(driver, pobj.accountsLink);
			searchAccountByIndustry(driver, industry);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//a[.='" + accountName + "']"));
			fc.utobj().clickElement(driver, pobj.opportunitiesTab);

			// Log A Task
			fc.utobj().printTestStep("Log A Task");
			fc.utobj().actionImgOption(driver, opportunityName, "Log a Task");
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			if (!fc.utobj().isSelected(driver, pobj.radioOwner)) {
				fc.utobj().clickElement(driver, pobj.radioOwner);
			}
			String statusTask = dataSet.get("statusTask");
			fc.utobj().selectDropDown(driver, pobj.statusTask, statusTask);
			String subject = fc.utobj().generateTestData(dataSet.get("subject"));
			fc.utobj().sendKeys(driver, pobj.subject, subject);
			if (!fc.utobj().isSelected(driver, pobj.timeLessTask)) {
				fc.utobj().clickElement(driver, pobj.timeLessTask);
			}
			String priority = dataSet.get("priority");
			fc.utobj().selectDropDown(driver, pobj.priority, priority);
			fc.utobj().sendKeys(driver, pobj.comments, "Test Comment");
			fc.utobj().clickElement(driver, pobj.createBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//a[.='" + opportunityName + "']"));

			fc.utobj().printTestStep("Verify Log A Task");
			boolean isSubjectPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[contains(text () ,'" + subject + "')]");
			if (isSubjectPresent == false) {
				fc.utobj().throwsException("was not able to verify subject");
			}
			boolean isUserPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[contains(text () ,'" + subject
					+ "')]/ancestor::tr/td/div[.='" + corpUser.getuserFullName() + "']");
			if (isUserPresent == false) {
				fc.utobj().throwsException("was not able to verify userName at Lead Info Page");
			}
			boolean isStatusOfTaskPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[contains(text () ,'" + subject
					+ "')]/ancestor::tr/td[contains(text () ,'" + statusTask + "')]");
			if (isStatusOfTaskPresent == false) {
				fc.utobj().throwsException("was not able to verify status of task");
			}
			boolean isPriorityPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[contains(text () ,'" + subject
					+ "')]/ancestor::tr/td[contains(text () ,'" + priority + "')]");
			if (isPriorityPresent == false) {
				fc.utobj().throwsException("was not able to verify Priority of task");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "crm")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Change Stage By Action Image Options At CRM > Accounts > Opportunities", testCaseId = "TC_150_Change_Stage")
	public void changeStageActionImage() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			CRMAccountsPage pobj = new CRMAccountsPage(driver);

			CRMContactsPageTest contactsPage = new CRMContactsPageTest();
			fc.utobj().printTestStep("Navigate To Admin Users Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			String emailId = "crmautomation@staffex.com";
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUser = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporateUser.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);
			fc.utobj().printTestStep("Navigating to Admin > CRM > ContactType Configuration");
			fc.utobj().printTestStep("Add Contact Type");
			AdminCRMContactTypeConfigurationPageTest contactTypePage = new AdminCRMContactTypeConfigurationPageTest();
			String contactType = fc.utobj().generateTestData(dataSet.get("contactType"));
			contactTypePage.addContactType(driver, contactType);
			fc.utobj().printTestStep("Navigating to Admin > CRM >  Configure Opportunity Stages");
			fc.utobj().printTestStep("Add Stage");
			AdminCRMConfigureOpportunityStagesPageTest stagePage = new AdminCRMConfigureOpportunityStagesPageTest();
			String stage = fc.utobj().generateTestData(dataSet.get("stage"));
			stagePage.addStage(driver, stage);

			String stage1 = fc.utobj().generateTestData(dataSet.get("stage"));
			stagePage.addStage(driver, stage1);

			AdminCRMConfigureIndustryPageTest industryPage = new AdminCRMConfigureIndustryPageTest();
			String industry = fc.utobj().generateTestData(dataSet.get("industry"));
			String description = fc.utobj().generateTestData(dataSet.get("description"));
			fc.utobj().printTestStep("Navigating to Admin > CRM >  Configure Industry Type");
			fc.utobj().printTestStep("Add Industry");
			industryPage.addIndustry(driver, industry, description);

			String accountName = fc.utobj().generateTestData(dataSet.get("accountName"));
			String visibility = dataSet.get("visibility");
			fc.utobj().printTestStep("Navigating to CRM >  Accounts > Accounts Summary");
			fc.utobj().printTestStep("Add Account");
			addAcocunt(driver, industry, accountName, visibility);

			fc.utobj().printTestStep("Navigating to CRM >  Accounts > Accounts Summary");
			fc.utobj().printTestStep("Add Contact");

			fc.utobj().clickElement(driver, pobj.accountsLink);
			searchAccountByIndustry(driver, industry);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//a[.='" + accountName + "']"));
			fc.utobj().clickElement(driver, pobj.contactsTab);
			fc.utobj().clickElement(driver, pobj.addContact);

			String cFname = fc.utobj().generateTestData(dataSet.get("cFname"));
			String cLname = fc.utobj().generateTestData(dataSet.get("cLname"));
			String primaryCMethod = dataSet.get("primaryCMethod");
			String assignTo = "Corporate";
			String regionName = "";
			String franchiseId = "";
			String title = dataSet.get("title");
			String email = "crmautomation@staffex.com";

			contactsPage.addContactGenericWithAccount(driver, contactType, cFname, cLname, primaryCMethod, assignTo,
					corpUser.getuserFullName(), regionName, franchiseId, title, email, config, accountName);

			fc.utobj().printTestStep("Add Opportunity");
			fc.utobj().clickElement(driver, pobj.accountsLink);
			searchAccountByIndustry(driver, industry);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//a[.='" + accountName + "']"));
			fc.utobj().clickElement(driver, pobj.opportunitiesTab);
			String opportunityName = fc.utobj().generateTestData(dataSet.get("opportunityName"));
			String contactName = cFname + " " + cLname;
			String opportunityType = dataSet.get("opportunityType");
			String opportunitySource = dataSet.get("opportunitySource");
			String salesAmount = dataSet.get("salesAmount");
			fc.utobj().clickElement(driver, driver.findElement(By.linkText("Add Opportunity")));
			addOpportunity(driver, opportunityName, accountName, contactName, corpUser.getuserFullName(), stage,
					opportunityType, opportunitySource, salesAmount, config);

			fc.utobj().clickElement(driver, pobj.accountsLink);
			searchAccountByIndustry(driver, industry);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//a[.='" + accountName + "']"));
			fc.utobj().clickElement(driver, pobj.opportunitiesTab);

			// Change Stages
			fc.utobj().printTestStep("Change Stage");
			fc.utobj().actionImgOption(driver, opportunityName, "Change Stage");
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().selectDropDown(driver, pobj.stage, stage1);
			fc.utobj().clickElement(driver, pobj.changeBtn);
			fc.utobj().clickElement(driver, pobj.closeBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Verify Change Stage");
			boolean isOpportunityPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[.='" + opportunityName + "']");
			if (isOpportunityPresent == false) {
				fc.utobj().throwsException("was not able to verify Opportunity Name");
			}
			boolean isAssignToPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[.='" + opportunityName
					+ "']/ancestor::tr/td[contains(text () , '" + corpUser.getuserFullName() + "')]");
			if (isAssignToPresent == false) {
				fc.utobj().throwsException("was not able to verify Assign To");
			}
			boolean isAccountNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[.='" + opportunityName + "']/ancestor::tr/td/a[contains(text () , '" + accountName + "')]");
			if (isAccountNamePresent == false) {
				fc.utobj().throwsException("was not able to verify Account");
			}
			boolean isContactNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[.='" + opportunityName + "']/ancestor::tr/td/a[contains(text () , '" + contactName + "')]");
			if (isContactNamePresent == false) {
				fc.utobj().throwsException("was not able to verify Contact");
			}
			boolean isStagePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[.='" + opportunityName + "']/ancestor::tr/td[contains(text () ,'" + stage1 + "')]");
			if (isStagePresent == false) {
				fc.utobj().throwsException("was not able to verify Stage");
			}
			/*
			 * boolean isOpportunityTypePresent=fc.utobj().verifyCase(driver,
			 * ".//a[.='"+opportunityName+
			 * "']/ancestor::tr/td[contains(text () , '"+opportunityType+"')]");
			 * if (isOpportunityTypePresent==false) {
			 * fc.utobj().throwsException(
			 * "was not able to verify Opportunity Type"); }
			 * 
			 * boolean isOpportunitySourcePresent=fc.utobj().verifyCase(driver,
			 * ".//a[.='"+opportunityName+
			 * "']/ancestor::tr/td[contains(text () , '"
			 * +opportunitySource+"')]"); if (isOpportunitySourcePresent==false)
			 * { fc.utobj().throwsException(
			 * "was not able to verify Opportunity Source"); }
			 */
			boolean isSalesAmountPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[.='" + opportunityName
					+ "']/ancestor::tr/td[contains(text () , '" + salesAmount.concat(".00") + "')]");
			if (isSalesAmountPresent == false) {
				fc.utobj().throwsException("was not able to verify Sales Amount");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "crm")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Change Stage By Action Btn Options At CRM > Accounts > Opportunities", testCaseId = "TC_151_Change_Stage")
	public void changeStageActionBtn() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			CRMAccountsPage pobj = new CRMAccountsPage(driver);

			CRMContactsPageTest contactsPage = new CRMContactsPageTest();
			fc.utobj().printTestStep("Navigate To Admin Users Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			String emailId = "crmautomation@staffex.com";
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUser = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporateUser.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);
			fc.utobj().printTestStep("Navigating to Admin > CRM > ContactType Configuration");
			fc.utobj().printTestStep("Add Contact Type");
			AdminCRMContactTypeConfigurationPageTest contactTypePage = new AdminCRMContactTypeConfigurationPageTest();
			String contactType = fc.utobj().generateTestData(dataSet.get("contactType"));
			contactTypePage.addContactType(driver, contactType);
			fc.utobj().printTestStep("Navigating to Admin > CRM >  Configure Opportunity Stages");
			fc.utobj().printTestStep("Add Stage");
			AdminCRMConfigureOpportunityStagesPageTest stagePage = new AdminCRMConfigureOpportunityStagesPageTest();
			String stage = fc.utobj().generateTestData(dataSet.get("stage"));
			stagePage.addStage(driver, stage);

			String stage1 = fc.utobj().generateTestData(dataSet.get("stage"));
			stagePage.addStage(driver, stage1);

			AdminCRMConfigureIndustryPageTest industryPage = new AdminCRMConfigureIndustryPageTest();
			String industry = fc.utobj().generateTestData(dataSet.get("industry"));
			String description = fc.utobj().generateTestData(dataSet.get("description"));
			fc.utobj().printTestStep("Navigating to Admin > CRM >  Configure Industry Type");
			fc.utobj().printTestStep("Add Industry");
			industryPage.addIndustry(driver, industry, description);

			String accountName = fc.utobj().generateTestData(dataSet.get("accountName"));
			String visibility = dataSet.get("visibility");
			fc.utobj().printTestStep("Navigating to CRM >  Accounts > Accounts Summary");
			fc.utobj().printTestStep("Add Account");
			addAcocunt(driver, industry, accountName, visibility);

			fc.utobj().printTestStep("Navigating to CRM >  Accounts > Accounts Summary");
			fc.utobj().printTestStep("Add Contact");

			fc.utobj().clickElement(driver, pobj.accountsLink);
			searchAccountByIndustry(driver, industry);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//a[.='" + accountName + "']"));
			fc.utobj().clickElement(driver, pobj.contactsTab);
			fc.utobj().clickElement(driver, pobj.addContact);

			String cFname = fc.utobj().generateTestData(dataSet.get("cFname"));
			String cLname = fc.utobj().generateTestData(dataSet.get("cLname"));
			String primaryCMethod = dataSet.get("primaryCMethod");
			String assignTo = "Corporate";
			String regionName = "";
			String franchiseId = "";
			String title = dataSet.get("title");
			String email = "crmautomation@staffex.com";

			contactsPage.addContactGenericWithAccount(driver, contactType, cFname, cLname, primaryCMethod, assignTo,
					corpUser.getuserFullName(), regionName, franchiseId, title, email, config, accountName);
			fc.utobj().printTestStep("Add Opportunity");
			fc.utobj().clickElement(driver, pobj.accountsLink);
			searchAccountByIndustry(driver, industry);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//a[.='" + accountName + "']"));
			fc.utobj().clickElement(driver, pobj.opportunitiesTab);
			String opportunityName = fc.utobj().generateTestData(dataSet.get("opportunityName"));
			String contactName = cFname + " " + cLname;
			String opportunityType = dataSet.get("opportunityType");
			String opportunitySource = dataSet.get("opportunitySource");
			String salesAmount = dataSet.get("salesAmount");
			fc.utobj().clickElement(driver, driver.findElement(By.linkText("Add Opportunity")));
			addOpportunity(driver, opportunityName, accountName, contactName, corpUser.getuserFullName(), stage,
					opportunityType, opportunitySource, salesAmount, config);

			fc.utobj().clickElement(driver, pobj.accountsLink);
			searchAccountByIndustry(driver, industry);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//a[.='" + accountName + "']"));
			fc.utobj().clickElement(driver, pobj.opportunitiesTab);

			// Change Stages
			fc.utobj().printTestStep("Chnage Stage");
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//a[.='" + opportunityName + "']/ancestor::tr/td/input"));
			fc.utobj().selectActionMenuItemsWithTagA(driver, "Change Stage");
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().selectDropDown(driver, pobj.stage, stage1);
			fc.utobj().clickElement(driver, pobj.changeBtn);
			fc.utobj().clickElement(driver, pobj.closeBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Verify Change Stage");
			boolean isOpportunityPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[.='" + opportunityName + "']");
			if (isOpportunityPresent == false) {
				fc.utobj().throwsException("was not able to verify Opportunity Name");
			}
			boolean isAssignToPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[.='" + opportunityName
					+ "']/ancestor::tr/td[contains(text () , '" + corpUser.getuserFullName() + "')]");
			if (isAssignToPresent == false) {
				fc.utobj().throwsException("was not able to verify Assign To");
			}
			boolean isAccountNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[.='" + opportunityName + "']/ancestor::tr/td/a[contains(text () , '" + accountName + "')]");
			if (isAccountNamePresent == false) {
				fc.utobj().throwsException("was not able to verify Account");
			}
			boolean isContactNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[.='" + opportunityName + "']/ancestor::tr/td/a[contains(text () , '" + contactName + "')]");
			if (isContactNamePresent == false) {
				fc.utobj().throwsException("was not able to verify Contact");
			}
			boolean isStagePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[.='" + opportunityName + "']/ancestor::tr/td[contains(text () ,'" + stage1 + "')]");
			if (isStagePresent == false) {
				fc.utobj().throwsException("was not able to verify Stage");
			}
			boolean isSalesAmountPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[.='" + opportunityName
					+ "']/ancestor::tr/td[contains(text () , '" + salesAmount.concat(".00") + "')]");
			if (isSalesAmountPresent == false) {
				fc.utobj().throwsException("was not able to verify Sales Amount");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "crm")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Delete Opportunity By Action Button Option At CRM > Accounts > Opportunities", testCaseId = "TC_152_Delete_Opportunity")
	public void deleteOpportunityActionBtn() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			CRMAccountsPage pobj = new CRMAccountsPage(driver);

			CRMContactsPageTest contactsPage = new CRMContactsPageTest();
			fc.utobj().printTestStep("Navigate To Admin Users Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			String emailId = "crmautomation@staffex.com";
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUser = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporateUser.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);
			fc.utobj().printTestStep("Navigating to Admin > CRM > ContactType Configuration");
			fc.utobj().printTestStep("Add Contact Type");
			AdminCRMContactTypeConfigurationPageTest contactTypePage = new AdminCRMContactTypeConfigurationPageTest();
			String contactType = fc.utobj().generateTestData(dataSet.get("contactType"));
			contactTypePage.addContactType(driver, contactType);
			fc.utobj().printTestStep("Navigating to Admin > CRM >  Configure Opportunity Stages");
			fc.utobj().printTestStep("Add Stage");
			AdminCRMConfigureOpportunityStagesPageTest stagePage = new AdminCRMConfigureOpportunityStagesPageTest();
			String stage = fc.utobj().generateTestData(dataSet.get("stage"));
			stagePage.addStage(driver, stage);

			AdminCRMConfigureIndustryPageTest industryPage = new AdminCRMConfigureIndustryPageTest();
			String industry = fc.utobj().generateTestData(dataSet.get("industry"));
			String description = fc.utobj().generateTestData(dataSet.get("description"));
			fc.utobj().printTestStep("Navigating to Admin > CRM >  Configure Industry Type");
			fc.utobj().printTestStep("Add Industry");
			industryPage.addIndustry(driver, industry, description);

			String accountName = fc.utobj().generateTestData(dataSet.get("accountName"));
			String visibility = dataSet.get("visibility");
			fc.utobj().printTestStep("Navigating to CRM >  Accounts > Accounts Summary");
			fc.utobj().printTestStep("Add Account");
			addAcocunt(driver, industry, accountName, visibility);

			fc.utobj().printTestStep("Navigating to CRM >  Accounts > Accounts Summary");
			fc.utobj().printTestStep("Add Contact");

			fc.utobj().clickElement(driver, pobj.accountsLink);
			searchAccountByIndustry(driver, industry);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//a[.='" + accountName + "']"));
			fc.utobj().clickElement(driver, pobj.contactsTab);
			fc.utobj().clickElement(driver, pobj.addContact);

			String cFname = fc.utobj().generateTestData(dataSet.get("cFname"));
			String cLname = fc.utobj().generateTestData(dataSet.get("cLname"));
			String primaryCMethod = dataSet.get("primaryCMethod");
			String assignTo = "Corporate";
			String regionName = "";
			String franchiseId = "";
			String title = dataSet.get("title");
			String email = "crmautomation@staffex.com";

			contactsPage.addContactGenericWithAccount(driver, contactType, cFname, cLname, primaryCMethod, assignTo,
					corpUser.getuserFullName(), regionName, franchiseId, title, email, config, accountName);

			fc.utobj().printTestStep("Add Opportunity");
			fc.utobj().clickElement(driver, pobj.accountsLink);
			searchAccountByIndustry(driver, industry);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//a[.='" + accountName + "']"));
			fc.utobj().clickElement(driver, pobj.opportunitiesTab);
			String opportunityName = fc.utobj().generateTestData(dataSet.get("opportunityName"));
			String contactName = cFname + " " + cLname;
			String opportunityType = dataSet.get("opportunityType");
			String opportunitySource = dataSet.get("opportunitySource");
			String salesAmount = dataSet.get("salesAmount");
			fc.utobj().clickElement(driver, driver.findElement(By.linkText("Add Opportunity")));
			addOpportunity(driver, opportunityName, accountName, contactName, corpUser.getuserFullName(), stage,
					opportunityType, opportunitySource, salesAmount, config);

			fc.utobj().clickElement(driver, pobj.accountsLink);
			searchAccountByIndustry(driver, industry);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//a[.='" + accountName + "']"));
			fc.utobj().clickElement(driver, pobj.opportunitiesTab);

			// Delete Opportunity
			fc.utobj().printTestStep("Delete Opportunity");
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//a[.='" + opportunityName + "']/ancestor::tr/td/input"));
			fc.utobj().selectActionMenuItemsWithTagA(driver, "Delete");
			fc.utobj().acceptAlertBox(driver);

			fc.utobj().printTestStep("Verify Delete Opportunity");
			boolean isNoRecordPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//td[.='No records found.']");
			if (isNoRecordPresent == false) {
				fc.utobj().throwsException("was not able to Delete Opportunity");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "crm")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Change Stage By Action Bottom Btn Options At CRM > Accounts > Opportunities", testCaseId = "TC_153_Change_Stage")
	public void changeStageBottomBtn() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			CRMAccountsPage pobj = new CRMAccountsPage(driver);

			CRMContactsPageTest contactsPage = new CRMContactsPageTest();
			fc.utobj().printTestStep("Navigate To Admin Users Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			String emailId = "crmautomation@staffex.com";
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUser = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporateUser.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);
			fc.utobj().printTestStep("Navigating to Admin > CRM > ContactType Configuration");
			fc.utobj().printTestStep("Add Contact Type");
			AdminCRMContactTypeConfigurationPageTest contactTypePage = new AdminCRMContactTypeConfigurationPageTest();
			String contactType = fc.utobj().generateTestData(dataSet.get("contactType"));
			contactTypePage.addContactType(driver, contactType);
			fc.utobj().printTestStep("Navigating to Admin > CRM >  Configure Opportunity Stages");
			fc.utobj().printTestStep("Add Stage");
			AdminCRMConfigureOpportunityStagesPageTest stagePage = new AdminCRMConfigureOpportunityStagesPageTest();
			String stage = fc.utobj().generateTestData(dataSet.get("stage"));
			stagePage.addStage(driver, stage);

			String stage1 = fc.utobj().generateTestData(dataSet.get("stage"));
			stagePage.addStage(driver, stage1);

			AdminCRMConfigureIndustryPageTest industryPage = new AdminCRMConfigureIndustryPageTest();
			String industry = fc.utobj().generateTestData(dataSet.get("industry"));
			String description = fc.utobj().generateTestData(dataSet.get("description"));
			fc.utobj().printTestStep("Navigating to Admin > CRM >  Configure Industry Type");
			fc.utobj().printTestStep("Add Industry");
			industryPage.addIndustry(driver, industry, description);

			String accountName = fc.utobj().generateTestData(dataSet.get("accountName"));
			String visibility = dataSet.get("visibility");
			fc.utobj().printTestStep("Navigating to CRM >  Accounts > Accounts Summary");
			fc.utobj().printTestStep("Add Account");
			addAcocunt(driver, industry, accountName, visibility);

			fc.utobj().printTestStep("Navigating to CRM >  Accounts > Accounts Summary");
			fc.utobj().printTestStep("Add Contact");

			fc.utobj().clickElement(driver, pobj.accountsLink);
			searchAccountByIndustry(driver, industry);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//a[.='" + accountName + "']"));
			fc.utobj().clickElement(driver, pobj.contactsTab);
			fc.utobj().clickElement(driver, pobj.addContact);

			String cFname = fc.utobj().generateTestData(dataSet.get("cFname"));
			String cLname = fc.utobj().generateTestData(dataSet.get("cLname"));
			String primaryCMethod = dataSet.get("primaryCMethod");
			String assignTo = "Corporate";
			String regionName = "";
			String franchiseId = "";
			String title = dataSet.get("title");
			String email = "crmautomation@staffex.com";

			contactsPage.addContactGenericWithAccount(driver, contactType, cFname, cLname, primaryCMethod, assignTo,
					corpUser.getuserFullName(), regionName, franchiseId, title, email, config, accountName);

			fc.utobj().printTestStep("Add Opportunity");
			fc.utobj().clickElement(driver, pobj.accountsLink);
			searchAccountByIndustry(driver, industry);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//a[.='" + accountName + "']"));
			fc.utobj().clickElement(driver, pobj.opportunitiesTab);
			String opportunityName = fc.utobj().generateTestData(dataSet.get("opportunityName"));
			String contactName = cFname + " " + cLname;
			String opportunityType = dataSet.get("opportunityType");
			String opportunitySource = dataSet.get("opportunitySource");
			String salesAmount = dataSet.get("salesAmount");
			fc.utobj().clickElement(driver, driver.findElement(By.linkText("Add Opportunity")));
			addOpportunity(driver, opportunityName, accountName, contactName, corpUser.getuserFullName(), stage,
					opportunityType, opportunitySource, salesAmount, config);

			fc.utobj().clickElement(driver, pobj.accountsLink);
			searchAccountByIndustry(driver, industry);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//a[.='" + accountName + "']"));
			fc.utobj().clickElement(driver, pobj.opportunitiesTab);

			// Change Stages
			fc.utobj().printTestStep("Chnage Stage");
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//a[.='" + opportunityName + "']/ancestor::tr/td/input"));
			fc.utobj().clickElement(driver, pobj.changeStatusBottmBtn);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().selectDropDown(driver, pobj.stage, stage1);
			fc.utobj().clickElement(driver, pobj.changeBtn);
			fc.utobj().clickElement(driver, pobj.closeBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Verify Change Stage");
			boolean isOpportunityPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[.='" + opportunityName + "']");
			if (isOpportunityPresent == false) {
				fc.utobj().throwsException("was not able to verify Opportunity Name");
			}
			boolean isAssignToPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[.='" + opportunityName
					+ "']/ancestor::tr/td[contains(text () , '" + corpUser.getuserFullName() + "')]");
			if (isAssignToPresent == false) {
				fc.utobj().throwsException("was not able to verify Assign To");
			}
			boolean isAccountNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[.='" + opportunityName + "']/ancestor::tr/td/a[contains(text () , '" + accountName + "')]");
			if (isAccountNamePresent == false) {
				fc.utobj().throwsException("was not able to verify Account");
			}
			boolean isContactNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[.='" + opportunityName + "']/ancestor::tr/td/a[contains(text () , '" + contactName + "')]");
			if (isContactNamePresent == false) {
				fc.utobj().throwsException("was not able to verify Contact");
			}
			boolean isStagePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[.='" + opportunityName + "']/ancestor::tr/td[contains(text () ,'" + stage1 + "')]");
			if (isStagePresent == false) {
				fc.utobj().throwsException("was not able to verify Stage");
			}
			boolean isSalesAmountPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[.='" + opportunityName
					+ "']/ancestor::tr/td[contains(text () , '" + salesAmount.concat(".00") + "')]");
			if (isSalesAmountPresent == false) {
				fc.utobj().throwsException("was not able to verify Sales Amount");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "crm")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Delete Opportunity By Action Bottom Btn Options At CRM > Accounts > Opportunities", testCaseId = "TC_154_Delete_Opportunity")
	public void deleteOpportunityBottomBtn() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			CRMAccountsPage pobj = new CRMAccountsPage(driver);

			CRMContactsPageTest contactsPage = new CRMContactsPageTest();
			fc.utobj().printTestStep("Navigate To Admin Users Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			String emailId = "crmautomation@staffex.com";
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUser = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporateUser.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);
			fc.utobj().printTestStep("Navigating to Admin > CRM > ContactType Configuration");
			fc.utobj().printTestStep("Add Contact Type");
			AdminCRMContactTypeConfigurationPageTest contactTypePage = new AdminCRMContactTypeConfigurationPageTest();
			String contactType = fc.utobj().generateTestData(dataSet.get("contactType"));
			contactTypePage.addContactType(driver, contactType);
			fc.utobj().printTestStep("Navigating to Admin > CRM >  Configure Opportunity Stages");
			fc.utobj().printTestStep("Add Stage");
			AdminCRMConfigureOpportunityStagesPageTest stagePage = new AdminCRMConfigureOpportunityStagesPageTest();
			String stage = fc.utobj().generateTestData(dataSet.get("stage"));
			stagePage.addStage(driver, stage);

			AdminCRMConfigureIndustryPageTest industryPage = new AdminCRMConfigureIndustryPageTest();
			String industry = fc.utobj().generateTestData(dataSet.get("industry"));
			String description = fc.utobj().generateTestData(dataSet.get("description"));
			fc.utobj().printTestStep("Navigating to Admin > CRM >  Configure Industry Type");
			fc.utobj().printTestStep("Add Industry");
			industryPage.addIndustry(driver, industry, description);

			String accountName = fc.utobj().generateTestData(dataSet.get("accountName"));
			String visibility = dataSet.get("visibility");
			fc.utobj().printTestStep("Navigating to CRM >  Accounts > Accounts Summary");
			fc.utobj().printTestStep("Add Account");
			addAcocunt(driver, industry, accountName, visibility);

			fc.utobj().printTestStep("Navigating to CRM >  Accounts > Accounts Summary");
			fc.utobj().printTestStep("Add Contact");

			fc.utobj().clickElement(driver, pobj.accountsLink);
			searchAccountByIndustry(driver, industry);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//a[.='" + accountName + "']"));
			fc.utobj().clickElement(driver, pobj.contactsTab);
			fc.utobj().clickElement(driver, pobj.addContact);

			String cFname = fc.utobj().generateTestData(dataSet.get("cFname"));
			String cLname = fc.utobj().generateTestData(dataSet.get("cLname"));
			String primaryCMethod = dataSet.get("primaryCMethod");
			String assignTo = "Corporate";
			String regionName = "";
			String franchiseId = "";
			String title = dataSet.get("title");
			String email = "crmautomation@staffex.com";

			contactsPage.addContactGenericWithAccount(driver, contactType, cFname, cLname, primaryCMethod, assignTo,
					corpUser.getuserFullName(), regionName, franchiseId, title, email, config, accountName);

			fc.utobj().printTestStep("Add Opportunity");
			fc.utobj().clickElement(driver, pobj.accountsLink);
			searchAccountByIndustry(driver, industry);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//a[.='" + accountName + "']"));
			fc.utobj().clickElement(driver, pobj.opportunitiesTab);
			String opportunityName = fc.utobj().generateTestData(dataSet.get("opportunityName"));
			String contactName = cFname + " " + cLname;
			String opportunityType = dataSet.get("opportunityType");
			String opportunitySource = dataSet.get("opportunitySource");
			String salesAmount = dataSet.get("salesAmount");
			fc.utobj().clickElement(driver, driver.findElement(By.linkText("Add Opportunity")));
			addOpportunity(driver, opportunityName, accountName, contactName, corpUser.getuserFullName(), stage,
					opportunityType, opportunitySource, salesAmount, config);

			fc.utobj().clickElement(driver, pobj.accountsLink);
			searchAccountByIndustry(driver, industry);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//a[.='" + accountName + "']"));
			fc.utobj().clickElement(driver, pobj.opportunitiesTab);

			// Delete Opportunity
			fc.utobj().printTestStep("Delete Opportunity");
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//a[.='" + opportunityName + "']/ancestor::tr/td/input"));
			fc.utobj().clickElement(driver, pobj.deleteBtmBtn);
			fc.utobj().acceptAlertBox(driver);

			fc.utobj().printTestStep("Verify Delete Opportunity");
			boolean isNoRecordPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//td[.='No records found.']");
			if (isNoRecordPresent == false) {
				fc.utobj().throwsException("was not able to Delete Opportunity");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "crm", "crmcheking12","addTranasction"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2017-08-09", testCaseDescription = "Verify The Add Transaction At CRM > Accounts > Transaction", testCaseId = "TC_155_Add_Transaction")
	public void addNewTransaction() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			CRMAccountsPage pobj = new CRMAccountsPage(driver);

			String description = fc.utobj().generateTestData(dataSet.get("description"));
			AdminCRMManageProductServiceCategoryPageTest serviceProductPage = new AdminCRMManageProductServiceCategoryPageTest();
			String categoryName = fc.utobj().generateTestData(dataSet.get("categoryName"));
			String productName = fc.utobj().generateTestData(dataSet.get("productName"));
			String oneLineDescription = fc.utobj().generateTestData(dataSet.get("oneLineDescription"));
			String rate = dataSet.get("rate");
			fc.utobj().printTestStep("Navigating to Admin > CRM >  Manage Product / Service & Category");

			fc.utobj().printTestStep("Add Category");

			serviceProductPage.addCategory(driver, categoryName, description);
			fc.utobj().printTestStep("Add Product/Service");
			serviceProductPage.addProductService(driver, categoryName, productName, oneLineDescription, rate);

			CRMContactsPageTest contactsPage = new CRMContactsPageTest();
			fc.utobj().printTestStep("Navigate To Admin Users Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			String emailId = "crmautomation@staffex.com";
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUser = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporateUser.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);
			fc.utobj().printTestStep("Navigating to Admin > CRM > ContactType Configuration");
			fc.utobj().printTestStep("Add Contact Type");
			String contactType = "Contacts";
			fc.utobj().printTestStep("Navigating to Admin > CRM >  Configure Opportunity Stages");
			fc.utobj().printTestStep("Add Stage");
			AdminCRMConfigureOpportunityStagesPageTest stagePage = new AdminCRMConfigureOpportunityStagesPageTest();
			String stage = fc.utobj().generateTestData(dataSet.get("stage"));
			stagePage.addStage(driver, stage);

			AdminCRMConfigureIndustryPageTest industryPage = new AdminCRMConfigureIndustryPageTest();
			String industry = fc.utobj().generateTestData(dataSet.get("industry"));
			description = fc.utobj().generateTestData(dataSet.get("description"));
			fc.utobj().printTestStep("Navigating to Admin > CRM >  Configure Industry Type");
			fc.utobj().printTestStep("Add Industry");
			industryPage.addIndustry(driver, industry, description);

			String accountName = fc.utobj().generateTestData(dataSet.get("accountName"));
			String visibility = dataSet.get("visibility");
			fc.utobj().printTestStep("Navigating to CRM >  Accounts > Accounts Summary");
			fc.utobj().printTestStep("Add Account");
			addAcocunt(driver, industry, accountName, visibility);

			fc.utobj().printTestStep("Navigating to CRM >  Accounts > Accounts Summary");
			fc.utobj().printTestStep("Add Contact");

			fc.utobj().clickElement(driver, pobj.accountsLink);
			searchAccountByIndustry(driver, industry);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//a[.='" + accountName + "']"));
			fc.utobj().clickElement(driver, pobj.contactsTab);
			fc.utobj().clickElement(driver, pobj.addContact);

			String cFname = fc.utobj().generateTestData(dataSet.get("cFname"));
			String cLname = fc.utobj().generateTestData(dataSet.get("cLname"));
			String primaryCMethod = dataSet.get("primaryCMethod");
			String assignTo = "Corporate";
			String regionName = "";
			String franchiseId = "";
			String title = dataSet.get("title");
			String email = "crmautomation@staffex.com";

			contactsPage.addContactGenericWithAccount(driver, contactType, cFname, cLname, primaryCMethod, assignTo,
					corpUser.getuserFullName(), regionName, franchiseId, title, email, config, accountName);

			fc.utobj().printTestStep("Add Opportunity");
			fc.utobj().clickElement(driver, pobj.accountsLink);
			searchAccountByIndustry(driver, industry);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//a[.='" + accountName + "']"));
			fc.utobj().clickElement(driver, pobj.opportunitiesTab);
			String opportunityName = fc.utobj().generateTestData(dataSet.get("opportunityName"));
			String contactName = cFname + " " + cLname;
			String opportunityType = dataSet.get("opportunityType");
			String opportunitySource = dataSet.get("opportunitySource");
			String salesAmount = dataSet.get("salesAmount");
			fc.utobj().clickElement(driver, driver.findElement(By.linkText("Add Opportunity")));
			addOpportunity(driver, opportunityName, accountName, contactName, corpUser.getuserFullName(), stage,
					opportunityType, opportunitySource, salesAmount, config);

			fc.utobj().printTestStep("Add Transaction");
			fc.utobj().clickElement(driver, pobj.accountsLink);
			searchAccountByIndustry(driver, industry);
			fc.utobj().actionImgOption(driver, accountName, "Add Transaction");
			/*
			 * fc.utobj().clickElement(driver,
			 * fc.utobj().getElementByXpath(driver,".//a[.='"+accountName+"']"))
			 * ); fc.utobj().clickElement(driver, pobj.transactionTab);
			 * 
			 * fc.utobj().clickElement(driver, pobj.newTransactionLink);
			 */
			String invoiceDate = fc.utobj().getCurrentDateUSFormat();
			fc.utobj().sendKeys(driver, pobj.invoiceDate, invoiceDate);
			fc.utobj().sendKeys(driver, pobj.opportunityName, opportunityName);

			fc.utobj().clickElement(driver, pobj.ajaxResult);
			fc.utobj().clickElement(driver, pobj.selectProductAndService);
			fc.utobj().clickElement(driver, pobj.uncheckAll);
			fc.utobj().sendKeys(driver, pobj.searchProduct, productName);
			fc.utobj().clickElement(driver, pobj.checkAll);
			fc.utobj().clickElement(driver, pobj.okayBtn);
			fc.utobj().clickElement(driver, pobj.submitBtn);

			fc.utobj().printTestStep("Verify Add Transaction");
			/*
			 * boolean isProductServicePresent=fc.utobj().verifyCase(driver,
			 * ".//td[contains(text () , '"+productName+"')]"); if
			 * (isProductServicePresent==false) { fc.utobj().throwsException(
			 * "was not able to verify Transaction Name"); }
			 */

			fc.utobj().clickElement(driver, pobj.accountsLink);
			searchAccountByIndustry(driver, industry);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//a[.='" + accountName + "']"));
			fc.utobj().clickElement(driver, pobj.transactionTab);

			boolean isOppPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () , '" + opportunityName + "')]");
			if (isOppPresent == false) {
				fc.utobj().throwsException("was not able to verify Transaction");
			}

			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,(".//td[contains(text () , '" + opportunityName + "')]/preceding-sibling::td/a")));

			boolean isProductAvailable = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () ,'" + productName + "')]");
			if (isProductAvailable == false) {
				fc.utobj().throwsException("was not able to verify Product");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "crmasdf123")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2017-08-09", testCaseDescription = "Verify The Modify Transaction At CRM > Accounts > Transaction", testCaseId = "TC_156_Modify_Transaction")
	public void modifyTransactionActionImage() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			CRMAccountsPage pobj = new CRMAccountsPage(driver);

			String description = fc.utobj().generateTestData(dataSet.get("description"));
			AdminCRMManageProductServiceCategoryPageTest serviceProductPage = new AdminCRMManageProductServiceCategoryPageTest();
			String categoryName = fc.utobj().generateTestData(dataSet.get("categoryName"));
			String productName = fc.utobj().generateTestData(dataSet.get("productName"));
			String oneLineDescription = fc.utobj().generateTestData(dataSet.get("oneLineDescription"));
			String rate = dataSet.get("rate");
			fc.utobj().printTestStep("Navigating to Admin > CRM >  Manage Product / Service & Category");

			fc.utobj().printTestStep("Add Category");

			serviceProductPage.addCategory(driver, categoryName, description);
			fc.utobj().printTestStep("Add Product/Service");
			serviceProductPage.addProductService(driver, categoryName, productName, oneLineDescription, rate);

			String categoryName1 = fc.utobj().generateTestData(dataSet.get("categoryName"));
			String productName1 = fc.utobj().generateTestData(dataSet.get("productName"));
			String oneLineDescription1 = fc.utobj().generateTestData(dataSet.get("oneLineDescription"));
			serviceProductPage.addCategory(driver, categoryName1, description);
			serviceProductPage.addProductService(driver, categoryName1, productName1, oneLineDescription1, rate);

			CRMContactsPageTest contactsPage = new CRMContactsPageTest();
			fc.utobj().printTestStep("Navigate To Admin Users Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			String emailId = "crmautomation@staffex.com";
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUser = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporateUser.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);
			fc.utobj().printTestStep("Navigating to Admin > CRM > ContactType Configuration");
			fc.utobj().printTestStep("Add Contact Type");
			AdminCRMContactTypeConfigurationPageTest contactTypePage = new AdminCRMContactTypeConfigurationPageTest();
			String contactType = fc.utobj().generateTestData(dataSet.get("contactType"));
			contactTypePage.addContactType(driver, contactType);
			fc.utobj().printTestStep("Navigating to Admin > CRM >  Configure Opportunity Stages");
			fc.utobj().printTestStep("Add Stage");
			AdminCRMConfigureOpportunityStagesPageTest stagePage = new AdminCRMConfigureOpportunityStagesPageTest();
			String stage = fc.utobj().generateTestData(dataSet.get("stage"));
			stagePage.addStage(driver, stage);

			AdminCRMConfigureIndustryPageTest industryPage = new AdminCRMConfigureIndustryPageTest();
			String industry = fc.utobj().generateTestData(dataSet.get("industry"));
			description = fc.utobj().generateTestData(dataSet.get("description"));
			fc.utobj().printTestStep("Navigating to Admin > CRM >  Configure Industry Type");
			fc.utobj().printTestStep("Add Industry");
			industryPage.addIndustry(driver, industry, description);

			String accountName = fc.utobj().generateTestData(dataSet.get("accountName"));
			String visibility = dataSet.get("visibility");
			fc.utobj().printTestStep("Navigating to CRM >  Accounts > Accounts Summary");
			fc.utobj().printTestStep("Add Account");
			addAcocunt(driver, industry, accountName, visibility);

			fc.utobj().printTestStep("Navigating to CRM >  Accounts > Accounts Summary");
			fc.utobj().printTestStep("Add Contact");

			fc.utobj().clickElement(driver, pobj.accountsLink);
			searchAccountByIndustry(driver, industry);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//a[.='" + accountName + "']"));
			fc.utobj().clickElement(driver, pobj.contactsTab);
			fc.utobj().clickElement(driver, pobj.addContact);

			String cFname = fc.utobj().generateTestData(dataSet.get("cFname"));
			String cLname = fc.utobj().generateTestData(dataSet.get("cLname"));
			String primaryCMethod = dataSet.get("primaryCMethod");
			String assignTo = "Corporate";
			String regionName = "";
			String franchiseId = "";
			String title = dataSet.get("title");
			String email = "crmautomation@staffex.com";

			contactsPage.addContactGenericWithAccount(driver, contactType, cFname, cLname, primaryCMethod, assignTo,
					corpUser.getuserFullName(), regionName, franchiseId, title, email, config, accountName);

			fc.utobj().printTestStep("Add Opportunity");
			fc.utobj().clickElement(driver, pobj.accountsLink);
			searchAccountByIndustry(driver, industry);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//a[.='" + accountName + "']"));
			fc.utobj().clickElement(driver, pobj.opportunitiesTab);
			String opportunityName = fc.utobj().generateTestData(dataSet.get("opportunityName"));
			String contactName = cFname + " " + cLname;
			String opportunityType = dataSet.get("opportunityType");
			String opportunitySource = dataSet.get("opportunitySource");
			String salesAmount = dataSet.get("salesAmount");
			fc.utobj().clickElement(driver, driver.findElement(By.linkText("Add Opportunity")));
			addOpportunity(driver, opportunityName, accountName, contactName, corpUser.getuserFullName(), stage,
					opportunityType, opportunitySource, salesAmount, config);

			fc.utobj().printTestStep("Add Transaction");
			fc.utobj().clickElement(driver, pobj.accountsLink);
			searchAccountByIndustry(driver, industry);
			fc.utobj().actionImgOption(driver, accountName, "Add Transaction");
			/*
			 * fc.utobj().clickElement(driver,
			 * fc.utobj().getElementByXpath(driver,".//a[.='"+accountName+"']"))
			 * ); fc.utobj().clickElement(driver, pobj.transactionTab);
			 * fc.utobj().clickElement(driver, pobj.newTransactionLink);
			 */
			String invoiceDate = fc.utobj().getCurrentDateUSFormat();
			fc.utobj().sendKeys(driver, pobj.invoiceDate, invoiceDate);
			fc.utobj().sendKeys(driver, pobj.opportunityName, opportunityName);

			fc.utobj().clickElement(driver, pobj.ajaxResult);
			fc.utobj().clickElement(driver, pobj.selectProductAndService);
			fc.utobj().clickElement(driver, pobj.uncheckAll);
			fc.utobj().sendKeys(driver, pobj.searchProduct, productName);
			fc.utobj().clickElement(driver, pobj.checkAll);
			fc.utobj().clickElement(driver, pobj.okayBtn);
			fc.utobj().clickElement(driver, pobj.submitBtn);

			fc.utobj().clickElement(driver, pobj.accountsLink);
			searchAccountByIndustry(driver, industry);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//a[.='" + accountName + "']"));
			fc.utobj().clickElement(driver, pobj.transactionTab);

			// modify Transaction
			fc.utobj().printTestStep("Modify Transaction");
			fc.utobj().actionImgOption(driver, opportunityName, "Modify");
			fc.utobj().clickElement(driver, pobj.selectProductAndService);
			fc.utobj().clickElement(driver, pobj.uncheckAll);
			fc.utobj().sendKeys(driver, pobj.searchProduct, productName1);
			fc.utobj().clickElement(driver, pobj.checkAll);
			fc.utobj().clickElement(driver, pobj.okayBtn);
			fc.utobj().clickElement(driver, pobj.submitBtn);

			fc.utobj().printTestStep("Verify Modify Transaction");
			/*
			 * boolean isProductServicePresent=fc.utobj().verifyCase(driver,
			 * ".//td[contains(text () , '"+productName1+"')]"); if
			 * (isProductServicePresent==false) { fc.utobj().throwsException(
			 * "was not able to verify Transaction Name"); }
			 */

			fc.utobj().clickElement(driver, pobj.accountsLink);
			searchAccountByIndustry(driver, industry);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//a[.='" + accountName + "']"));
			fc.utobj().clickElement(driver, pobj.transactionTab);

			boolean isOppPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () , '" + opportunityName + "')]");
			if (isOppPresent == false) {
				fc.utobj().throwsException("was not able to verify Transaction");
			}

			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,(".//td[contains(text () , '" + opportunityName + "')]/preceding-sibling::td/a")));

			boolean isProductAvailable = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () ,'" + productName1 + "')]");
			if (isProductAvailable == false) {
				fc.utobj().throwsException("was not able to verify Product");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "crm")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2017-08-09", testCaseDescription = "Verify The Delete Transaction At CRM > Accounts > Transaction", testCaseId = "TC_157_Delete_Transaction")
	public void deleteTransactionActionImage() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			CRMAccountsPage pobj = new CRMAccountsPage(driver);

			String description = fc.utobj().generateTestData(dataSet.get("description"));
			AdminCRMManageProductServiceCategoryPageTest serviceProductPage = new AdminCRMManageProductServiceCategoryPageTest();
			String categoryName = fc.utobj().generateTestData(dataSet.get("categoryName"));
			String productName = fc.utobj().generateTestData(dataSet.get("productName"));
			String oneLineDescription = fc.utobj().generateTestData(dataSet.get("oneLineDescription"));
			String rate = dataSet.get("rate");
			fc.utobj().printTestStep("Navigating to Admin > CRM >  Manage Product / Service & Category");
			fc.utobj().printTestStep("Add Category");
			serviceProductPage.addCategory(driver, categoryName, description);
			fc.utobj().printTestStep("Add Product/Service");
			serviceProductPage.addProductService(driver, categoryName, productName, oneLineDescription, rate);

			CRMContactsPageTest contactsPage = new CRMContactsPageTest();
			fc.utobj().printTestStep("Navigate To Admin Users Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			String emailId = "crmautomation@staffex.com";
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUser = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporateUser.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);
			fc.utobj().printTestStep("Navigating to Admin > CRM > ContactType Configuration");
			fc.utobj().printTestStep("Add Contact Type");
			AdminCRMContactTypeConfigurationPageTest contactTypePage = new AdminCRMContactTypeConfigurationPageTest();
			String contactType = fc.utobj().generateTestData(dataSet.get("contactType"));
			contactTypePage.addContactType(driver, contactType);
			fc.utobj().printTestStep("Navigating to Admin > CRM >  Configure Opportunity Stages");
			fc.utobj().printTestStep("Add Stage");
			AdminCRMConfigureOpportunityStagesPageTest stagePage = new AdminCRMConfigureOpportunityStagesPageTest();
			String stage = fc.utobj().generateTestData(dataSet.get("stage"));
			stagePage.addStage(driver, stage);

			AdminCRMConfigureIndustryPageTest industryPage = new AdminCRMConfigureIndustryPageTest();
			String industry = fc.utobj().generateTestData(dataSet.get("industry"));
			description = fc.utobj().generateTestData(dataSet.get("description"));
			fc.utobj().printTestStep("Navigating to Admin > CRM >  Configure Industry Type");
			fc.utobj().printTestStep("Add Industry");
			industryPage.addIndustry(driver, industry, description);

			String accountName = fc.utobj().generateTestData(dataSet.get("accountName"));
			String visibility = dataSet.get("visibility");
			fc.utobj().printTestStep("Navigating to CRM >  Accounts > Accounts Summary");
			fc.utobj().printTestStep("Add Account");
			addAcocunt(driver, industry, accountName, visibility);

			fc.utobj().printTestStep("Navigating to CRM >  Accounts > Accounts Summary");
			fc.utobj().printTestStep("Add Contact");

			fc.utobj().clickElement(driver, pobj.accountsLink);
			searchAccountByIndustry(driver, industry);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//a[.='" + accountName + "']"));
			fc.utobj().clickElement(driver, pobj.contactsTab);
			fc.utobj().clickElement(driver, pobj.addContact);

			String cFname = fc.utobj().generateTestData(dataSet.get("cFname"));
			String cLname = fc.utobj().generateTestData(dataSet.get("cLname"));
			String primaryCMethod = dataSet.get("primaryCMethod");
			String assignTo = "Corporate";
			String regionName = "";
			String franchiseId = "";
			String title = dataSet.get("title");
			String email = "crmautomation@staffex.com";

			contactsPage.addContactGenericWithAccount(driver, contactType, cFname, cLname, primaryCMethod, assignTo,
					corpUser.getuserFullName(), regionName, franchiseId, title, email, config, accountName);

			fc.utobj().printTestStep("Add Opportunity");
			fc.utobj().clickElement(driver, pobj.accountsLink);
			searchAccountByIndustry(driver, industry);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//a[.='" + accountName + "']"));
			fc.utobj().clickElement(driver, pobj.opportunitiesTab);
			String opportunityName = fc.utobj().generateTestData(dataSet.get("opportunityName"));
			String contactName = cFname + " " + cLname;
			String opportunityType = dataSet.get("opportunityType");
			String opportunitySource = dataSet.get("opportunitySource");
			String salesAmount = dataSet.get("salesAmount");
			fc.utobj().clickElement(driver, driver.findElement(By.linkText("Add Opportunity")));
			addOpportunity(driver, opportunityName, accountName, contactName, corpUser.getuserFullName(), stage,
					opportunityType, opportunitySource, salesAmount, config);

			fc.utobj().printTestStep("Add Transaction");
			fc.utobj().clickElement(driver, pobj.accountsLink);
			searchAccountByIndustry(driver, industry);
			/*
			 * fc.utobj().clickElement(driver,
			 * fc.utobj().getElementByXpath(driver,".//a[.='"+accountName+"']"))
			 * ); fc.utobj().clickElement(driver, pobj.transactionTab);
			 * 
			 * fc.utobj().clickElement(driver, pobj.newTransactionLink);
			 */
			fc.utobj().actionImgOption(driver, accountName, "Add Transaction");
			String invoiceDate = fc.utobj().getCurrentDateUSFormat();
			fc.utobj().sendKeys(driver, pobj.invoiceDate, invoiceDate);
			fc.utobj().sendKeys(driver, pobj.opportunityName, opportunityName);

			fc.utobj().clickElement(driver, pobj.ajaxResult);
			fc.utobj().clickElement(driver, pobj.selectProductAndService);
			fc.utobj().clickElement(driver, pobj.uncheckAll);
			fc.utobj().sendKeys(driver, pobj.searchProduct, productName);
			fc.utobj().clickElement(driver, pobj.checkAll);
			fc.utobj().clickElement(driver, pobj.okayBtn);
			fc.utobj().clickElement(driver, pobj.submitBtn);

			fc.utobj().clickElement(driver, pobj.accountsLink);
			searchAccountByIndustry(driver, industry);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//a[.='" + accountName + "']"));
			fc.utobj().clickElement(driver, pobj.transactionTab);

			// Delete Transaction
			fc.utobj().printTestStep("Delete Transaction");
			fc.utobj().actionImgOption(driver, opportunityName, "Delete");
			fc.utobj().acceptAlertBox(driver);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().clickElement(driver, pobj.closeBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Verify Delete Transaction");
			boolean isNoRecordPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//td[.='No records found.']");
			if (isNoRecordPresent == false) {
				fc.utobj().throwsException("was not able to Archive Lead");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	/* Harish Dwivedi TC_280_Verify_Close_Button */

	@Test(groups = "crmmodule")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Add Opportunity With Corporate User By Action Image Option At CRM > Accounts > Accounts Summary to verify close button", testCaseId = "TC_280_Verify_Close_Button")
	public void addOpportunitywithCorporateUserToveryCloseButton() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			CRMAccountsPage pobj = new CRMAccountsPage(driver);

			CRMContactsPageTest contactsPage = new CRMContactsPageTest();

			AdminUsersManageCorporateUsersAddCorporateUserPageTest addCorporatePage = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			String userName = fc.utobj().generateTestData(dataSet.get("userName"));
			fc.utobj().printTestStep("Naviaget To Admin Users Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			String emailId = "crmautomation@staffex.com";
			CorporateUser corpUser = new CorporateUser();
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corpUser.setEmail(emailId);
			corpUser = addCorporatePage.createDefaultUser(driver, corpUser);

			fc.home_page().logout(driver);
			fc.loginpage().loginWithParameter(driver, userName, "T0n1ght1");

			fc.utobj().printTestStep("Navigating to Admin > CRM > ContactType Configuration");
			fc.utobj().printTestStep("Add Contact Type");
			AdminCRMContactTypeConfigurationPageTest contactTypePage = new AdminCRMContactTypeConfigurationPageTest();
			String contactType = fc.utobj().generateTestData(dataSet.get("contactType"));
			contactTypePage.addContactType(driver, contactType);
			fc.utobj().printTestStep("Navigating to Admin > CRM >  Configure Opportunity Stages");
			fc.utobj().printTestStep("Add Stage");
			AdminCRMConfigureOpportunityStagesPageTest stagePage = new AdminCRMConfigureOpportunityStagesPageTest();
			String stage = fc.utobj().generateTestData(dataSet.get("stage"));
			stagePage.addStage(driver, stage);

			AdminCRMConfigureIndustryPageTest industryPage = new AdminCRMConfigureIndustryPageTest();
			String industry = fc.utobj().generateTestData(dataSet.get("industry"));
			String description = fc.utobj().generateTestData(dataSet.get("description"));
			fc.utobj().printTestStep("Navigating to Admin > CRM >  Configure Industry Type");
			fc.utobj().printTestStep("Add Industry");
			industryPage.addIndustry(driver, industry, description);

			String accountName = fc.utobj().generateTestData(dataSet.get("accountName"));
			String visibility = dataSet.get("visibility");
			fc.utobj().printTestStep("Navigating to CRM >  Accounts > Accounts Summary");
			fc.utobj().printTestStep("Add Account");
			addAcocunt(driver, industry, accountName, visibility);

			fc.utobj().printTestStep("Navigating to CRM >  Accounts > Accounts Summary");
			fc.utobj().printTestStep("Add Contact");

			fc.utobj().clickElement(driver, pobj.accountsLink);
			searchAccountByIndustry(driver, industry);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//a[.='" + accountName + "']"));
			fc.utobj().clickElement(driver, pobj.contactsTab);
			fc.utobj().clickElement(driver, pobj.addContact);

			String cFname = fc.utobj().generateTestData(dataSet.get("cFname"));
			String cLname = fc.utobj().generateTestData(dataSet.get("cLname"));
			String primaryCMethod = dataSet.get("primaryCMethod");
			String assignTo = "Corporate";
			String regionName = "";
			String franchiseId = "";
			String title = dataSet.get("title");
			String email = "automation@franconnect.net";

			String userNameForCon = userName + " " + userName;

			contactsPage.addContactGenericWithAccount(driver, contactType, cFname, cLname, primaryCMethod, assignTo,
					userNameForCon, regionName, franchiseId, title, email, config, accountName);

			fc.utobj().printTestStep("Add Opportunity");
			fc.utobj().clickElement(driver, pobj.accountsLink);
			searchAccountByIndustry(driver, industry);

			fc.utobj().actionImgOption(driver, accountName, "Add Opportunity");

			String opportunityName = fc.utobj().generateTestData(dataSet.get("opportunityName"));
			String contactName = cFname + " " + cLname;
			String opportunityType = dataSet.get("opportunityType");
			String opportunitySource = dataSet.get("opportunitySource");
			String salesAmount = dataSet.get("salesAmount");
			addOpportunity(driver, opportunityName, accountName, contactName, userNameForCon, stage, opportunityType,
					opportunitySource, salesAmount, config);

			fc.utobj().clickElement(driver, pobj.accountsLink);
			searchAccountByIndustry(driver, industry);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//a[.='" + accountName + "']"));
			fc.utobj().clickElement(driver, pobj.opportunitiesTab);

			fc.utobj().printTestStep("Verify The Add Oportunity");
			boolean isOpportunityPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[.='" + opportunityName + "']");
			if (isOpportunityPresent == false) {
				fc.utobj().throwsException("was not able to verify Opportunity Name");
			}

			WebElement elementOpp = fc.utobj().getElementByXpath(driver, ".//*[.='" + opportunityName + "']");
			fc.utobj().clickElement(driver, elementOpp);
			fc.utobj().printTestStep("Verify The Close Button");
			fc.utobj().clickElement(driver, pobj.detailedHistoryLink);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().clickElement(driver, pobj.closeBtn);

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	public String addOpportunity(WebDriver driver, String opportunityName, String accountName, String contactName,
			String userName, String stage, String opportunityType, String opportunitySource, String salesAmount,
			Map<String, String> config) throws Exception {

		String testCaseId = "TC_Add_Opportunity_Account";

		if (fc.utobj().validate(testCaseId)) {
			try {
				CRMOpportunitiesPageUI pobj = new CRMOpportunitiesPageUI(driver);
				// fc.utobj().clickElement(driver,
				// driver.findElement(By.linkText("Add Opportunity")));
				fc.utobj().sendKeys(driver, pobj.opportunityName, opportunityName);

				// fc.utobj().sendKeys(driver, pobj.oppContactID, contactName);
				fc.utobj().clickElement(driver, pobj.searchContactBtn);
				fc.utobj().sleep(1000);
				fc.commonMethods().switch_cboxIframe_frameId(driver);
				fc.utobj().sleep(1000);
				fc.utobj().sendKeys(driver, pobj.oppContactID, contactName);
				fc.utobj().clickElement(driver, pobj.searchBtn);
				// fc.utobj().clickElement(driver,
				// fc.utobj().getElementByXpath(driver,".//a[contains(text () ,
				// '"+contactName+"')]")));
				fc.utobj().sleep(3000);
				fc.utobj().clickElement(driver, ".//a[contains(text(),'"+contactName+"')]");
				fc.utobj().switchFrameToDefault(driver);
				fc.utobj().sleep(1000);
				fc.utobj().sendKeys(driver, pobj.opportunityOwner, userName);
				fc.utobj().clickElement(driver, pobj.searchOppOwnerBtn);
				fc.utobj().sleep(1000);
				fc.commonMethods().switch_cboxIframe_frameId(driver);
				fc.utobj().sleep(1000);
				// fc.utobj().clickElement(driver,
				// fc.utobj().getElementByXpath(driver,".//a[contains(text () ,
				// '"+userName+"')]")));
				fc.utobj().clickLink(driver, userName);
				fc.utobj().switchFrameToDefault(driver);
				fc.utobj().sleep(1000);
				fc.utobj().selectDropDown(driver, pobj.stage, stage);
				fc.utobj().sendKeys(driver, pobj.salesAmount, salesAmount);

				fc.utobj().clickElement(driver, pobj.saveBtn);
				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException("was not able to add Opportunity In Accont :: CRM > Accounts > Opportunity");

		}
		return opportunityName;
	}

	public void searchAccountByIndustry(WebDriver driver, String industry) throws Exception {

		CRMAccountsPage pobj = new CRMAccountsPage(driver);
		fc.utobj().clickElement(driver, pobj.showFilter);
		fc.utobj().setToDefault(driver, pobj.selectVisibility);
		fc.utobj().selectDropDown(driver, pobj.addOnSelect, "All");
		fc.utobj().setToDefault(driver, pobj.selectIndustry);
		fc.utobj()
				.selectValFromMultiSelect(driver, pobj.selectIndustry, fc.utobj()
						.getElement(driver, pobj.selectIndustry).findElement(By.xpath("./div/div/input")),
						industry);
		if (!fc.utobj().isSelected(driver, pobj.showOnlyDuplicateNo)) {
			fc.utobj().clickElement(driver, pobj.showOnlyDuplicateNo);
		}
		fc.utobj().selectDropDown(driver, pobj.selectAccountType, "All Accounts");
		fc.utobj().selectDropDown(driver, pobj.selectShowAccounts, "All Accounts");

		fc.utobj().clickElement(driver, pobj.searchBtn);
		fc.utobj().clickElement(driver, pobj.hideFilter);
	}

	public void searchContactByContactType(WebDriver driver, String contactType) throws Exception {

		CRMAccountsPage pobj = new CRMAccountsPage(driver);
		fc.utobj().clickElement(driver, pobj.showFilter);
		fc.utobj().setToDefault(driver, pobj.contactTypeFilter);
		WebElement elementInput = fc.utobj().getElement(driver, pobj.contactTypeFilter)
				.findElement(By.xpath("./div/div/input"));
		fc.utobj().selectValFromMultiSelect(driver, pobj.contactTypeFilter, elementInput, contactType);
		fc.utobj().clickElement(driver, pobj.searchBtn);
		fc.utobj().clickElement(driver, pobj.hideFilter);
	}

	public void selectActionMenuItemsWithTagAMoreActions(WebDriver driver, String menuName) throws Exception {

		WebElement moreActionsLink = driver
				.findElement(By.xpath(".//*[@id='td1' or @id='leadsForm']/a[.='More Actions']"));
		fc.utobj().moveToElement(driver, moreActionsLink);
		// clickElement(driver,moreActionsLink);

		List<WebElement> menu = driver
				.findElements(By.xpath(".//*[@id='actionListButtons1']/table/tbody/tr[2]/td[2]/table/tbody//td"));
		fc.utobj().selectMoreActionMenu(driver, moreActionsLink, menu, menuName);
	}

	@Test(groups = { "ZcSanity", "ZcSanityLeadOff", "ZcSanityLeadOff36", "ZcSanity36" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Add Account At CRM > Accounts > Accounts Summary", testCaseId = "TC_Add_CRM_Account")
	public void addAccountTest() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			CRMAccountsPage pobj = new CRMAccountsPage(driver);

			Map<String, String> data2 = new HashMap<String, String>();
			data2.put("industry", "Test106Industry");
			data2.put("description", "Test106Desc");
			data2.put("accountName", "Test106Account");

			AdminCRMConfigureIndustryPageTest industryPage = new AdminCRMConfigureIndustryPageTest();
			String industry = fc.utobj().generateTestData(data2.get("industry"));
			String description = fc.utobj().generateTestData(data2.get("description"));
			fc.utobj().printTestStep("Navigating to Admin > CRM >  Configure Industry Type");
			fc.utobj().printTestStep("Add Industry");
			industryPage.addIndustry(driver, industry, description);

			fc.utobj().printTestStep("Navigate To CRM > Accouts > Accounts Summary");
			fc.utobj().printTestStep("Add Account");
			fc.crm().crm_common().CRMAccountsLnk(driver);
			fc.utobj().clickElement(driver, pobj.addNew);
			fc.utobj().clickElement(driver, pobj.accountLnk);
			String accountName = fc.utobj().generateTestData(data2.get("accountName"));
			fc.utobj().sendKeys(driver, pobj.accountName, accountName);
			if (!fc.utobj().isSelected(driver, pobj.B2BAccountType)) {
				fc.utobj().clickElement(driver, pobj.B2BAccountType);
			}
			fc.utobj().selectDropDown(driver, pobj.visibilityType, "All Network");
			if (fc.utobj().isSelected(driver, pobj.isParent)) {
				fc.utobj().clickElement(driver, pobj.isParent);
			}
			fc.utobj().sendKeys(driver, pobj.address, "AddressTest");
			fc.utobj().sendKeys(driver, pobj.city, "City");
			fc.utobj().selectDropDown(driver, pobj.country, "USA");
			fc.utobj().sendKeys(driver, pobj.zipcode, "123455");
			fc.utobj().selectDropDown(driver, pobj.state, "Alabama");
			fc.utobj().sendKeys(driver, pobj.phoneNumbers, "1236547896");
			fc.utobj().sendKeys(driver, pobj.extn, "123");
			fc.utobj().sendKeys(driver, pobj.faxNumbers, "1236654");
			fc.utobj().sendKeys(driver, pobj.mobileNumbers, "1478523698");
			fc.utobj().sendKeys(driver, pobj.emailIds, "crmautomation@staffex.com");
			fc.utobj().sendKeys(driver, pobj.noOfEmployee, "500");
			fc.utobj().selectDropDown(driver, pobj.industry, industry);
			fc.utobj().sendKeys(driver, pobj.sicCode, "SicCode");
			fc.utobj().sendKeys(driver, pobj.servicesProvided, "Service123");
			fc.utobj().sendKeys(driver, pobj.stockSymbol, "qwerty");

			if (!fc.utobj().isSelected(driver, pobj.addContactNo)) {
				fc.utobj().clickElement(driver, pobj.addContactNo);
			}
			fc.utobj().clickElement(driver, pobj.saveBtn);

			fc.utobj().clickElement(driver, pobj.accountsLink);
			searchAccountByIndustry(driver, industry);

			fc.utobj().printTestStep("Verify The Add Account");
			boolean isAccountNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[.='" + accountName + "']");
			if (isAccountNamePresent == false) {
				fc.utobj().throwsException("was not able to add Account");
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}
}
