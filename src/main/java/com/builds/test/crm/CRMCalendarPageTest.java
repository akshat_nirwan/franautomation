package com.builds.test.crm;

import java.util.Map;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Test;

import com.builds.uimaps.crm.CRMCalendarPage;
import com.builds.utilities.FranconnectUtil;
import com.builds.utilities.TestCase;

public class CRMCalendarPageTest {
	FranconnectUtil fc = new FranconnectUtil();

	
	@Test(groups = "crm")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify the Create Appointment At CRM > Calendar", testCaseId = "TC_270_Create_Appointment")
	public void createAppointment() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			CRMCalendarPage pobj = new CRMCalendarPage(driver);

			fc.utobj().printTestStep("Navigate To CRM > Calendar");
			fc.utobj().printTestStep("Create Appointment");
			fc.crm().crm_common().CRMCalendarLnk(driver);
			fc.utobj().selectDropDown(driver, pobj.calendarUserSelect, "FranConnect Administrator");
			fc.utobj().clickElement(driver, pobj.createLink);
			fc.utobj().selectDropDown(driver, pobj.scheduleSelect, "Appointment");
			String subject = fc.utobj().generateTestData(dataSet.get("subject"));
			fc.utobj().sendKeys(driver, pobj.subject, subject);
			String description = fc.utobj().generateTestData(dataSet.get("description"));
			fc.utobj().sendKeys(driver, pobj.description, description);
			fc.utobj().sendKeys(driver, pobj.startDatetime, fc.utobj().getCurrentDateUSFormat());
			fc.utobj().sendKeys(driver, pobj.endDatetime, fc.utobj().getFutureDateUSFormat(2));
			fc.utobj().sendKeys(driver, pobj.location, "TestLocation");
			fc.utobj().selectDropDown(driver, pobj.priority, "Low");
			fc.utobj().selectDropDown(driver, pobj.scheduleScope, "Yes");
			fc.utobj().clickElement(driver, pobj.createBtn);

			fc.utobj().clickElement(driver, pobj.searchBtn);
			fc.utobj().selectDropDown(driver, pobj.scheduleType, "Appointment");
			fc.utobj().sendKeys(driver, pobj.searchWords, subject);
			if (!fc.utobj().isSelected(driver, pobj.lookInSubCheck)) {
				fc.utobj().clickElement(driver, pobj.lookInSubCheck);
			}
			fc.utobj().clickElement(driver, pobj.searchBtnAtSearch);

			boolean isSubjectPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[.='" + subject + "']");
			if (isSubjectPresent == false) {
				fc.utobj().throwsException("was not able to verify Create Appointment");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	public void createAppointment(WebDriver driver, String subject, String description) throws Exception {

		String testCaseId = "TC_Create_Appointment_CRM";

		if (fc.utobj().validate(testCaseId)) {
			try {
				CRMCalendarPage pobj = new CRMCalendarPage(driver);
				fc.crm().crm_common().CRMCalendarLnk(driver);
				fc.utobj().selectDropDown(driver, pobj.calendarUserSelect, "FranConnect Administrator");
				fc.utobj().clickElement(driver, pobj.createLink);
				fc.utobj().selectDropDown(driver, pobj.scheduleSelect, "Appointment");
				fc.utobj().sendKeys(driver, pobj.subject, subject);
				fc.utobj().sendKeys(driver, pobj.description, description);
				fc.utobj().sendKeys(driver, pobj.startDatetime, fc.utobj().getCurrentDateUSFormat());
				fc.utobj().sendKeys(driver, pobj.endDatetime, fc.utobj().getFutureDateUSFormat(2));
				fc.utobj().sendKeys(driver, pobj.location, "TestLocation");
				fc.utobj().selectDropDown(driver, pobj.priority, "Low");
				fc.utobj().selectDropDown(driver, pobj.scheduleScope, "Yes");
				fc.utobj().clickElement(driver, pobj.createBtn);

				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException("Was not able to Create AppointMent :: CRM > Calender");

		}
	}

	@Test(groups = "crm")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify the Modify Appointment ::  CRM > Calender", testCaseId = "TC_271_Modify_Appointment")
	public void modifyAppoinment() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			CRMCalendarPage pobj = new CRMCalendarPage(driver);

			fc.utobj().printTestStep("Navigate To CRM > Calendar");
			fc.utobj().printTestStep("Create Appointment");
			String subject = fc.utobj().generateTestData(dataSet.get("subject"));
			String description = fc.utobj().generateTestData(dataSet.get("description"));
			createAppointment(driver, subject, description);

			fc.utobj().clickElement(driver, pobj.searchBtn);
			fc.utobj().selectDropDown(driver, pobj.scheduleType, "Appointment");
			fc.utobj().sendKeys(driver, pobj.searchWords, subject);
			if (!fc.utobj().isSelected(driver, pobj.lookInSubCheck)) {
				fc.utobj().clickElement(driver, pobj.lookInSubCheck);
			}
			fc.utobj().clickElement(driver, pobj.searchBtnAtSearch);

			fc.utobj().printTestStep("Modify The Appointment");
			fc.utobj().actionImgOption(driver, subject, "Modify");

			subject = fc.utobj().generateTestData(dataSet.get("subject"));
			fc.utobj().sendKeys(driver, pobj.subject, subject);
			description = fc.utobj().generateTestData(dataSet.get("description"));
			fc.utobj().sendKeys(driver, pobj.description, description);
			fc.utobj().sendKeys(driver, pobj.startDatetime, fc.utobj().getCurrentDateUSFormat());
			fc.utobj().sendKeys(driver, pobj.endDatetime, fc.utobj().getFutureDateUSFormat(3));
			fc.utobj().sendKeys(driver, pobj.location, "TestLocation");
			fc.utobj().selectDropDown(driver, pobj.priority, "Low");
			fc.utobj().selectDropDown(driver, pobj.scheduleScope, "Yes");
			fc.utobj().clickElement(driver, pobj.saveBtn);

			fc.utobj().printTestStep("Verify The Modify Appointment");

			fc.utobj().clickElement(driver, pobj.searchBtn);
			fc.utobj().selectDropDown(driver, pobj.scheduleType, "Appointment");
			fc.utobj().sendKeys(driver, pobj.searchWords, subject);
			if (!fc.utobj().isSelected(driver, pobj.lookInSubCheck)) {
				fc.utobj().clickElement(driver, pobj.lookInSubCheck);
			}
			fc.utobj().clickElement(driver, pobj.searchBtnAtSearch);

			boolean isSubjectPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[.='" + subject + "']");
			if (isSubjectPresent == false) {
				fc.utobj().throwsException("was not able to verify Modify Appointment");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "crm")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify the Create Meeting ::  CRM > Calendar", testCaseId = "TC_272_Create_Meeting")
	public void createMeeting() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			CRMCalendarPage pobj = new CRMCalendarPage(driver);

			fc.utobj().printTestStep("Navigate To CRM > Calendar");
			fc.utobj().printTestStep("Create Meeting");
			fc.crm().crm_common().CRMCalendarLnk(driver);
			fc.utobj().selectDropDown(driver, pobj.calendarUserSelect, "FranConnect Administrator");
			fc.utobj().clickElement(driver, pobj.createLink);
			fc.utobj().selectDropDown(driver, pobj.scheduleSelect, "Meeting");
			String subject = fc.utobj().generateTestData(dataSet.get("subject"));
			fc.utobj().sendKeys(driver, pobj.subject, subject);
			String description = fc.utobj().generateTestData(dataSet.get("description"));
			fc.utobj().sendKeys(driver, pobj.description, description);
			fc.utobj().selectDropDown(driver, pobj.scheduleScope, "Private");
			fc.utobj().sendKeys(driver, pobj.startDatetime, fc.utobj().getCurrentDateUSFormat());
			fc.utobj().sendKeys(driver, pobj.endDatetime, fc.utobj().getFutureDateUSFormat(2));
			fc.utobj().sendKeys(driver, pobj.location, "TestLocation");
			fc.utobj().selectDropDown(driver, pobj.priority, "Low");
			fc.utobj().clickElement(driver, pobj.createBtn);

			fc.utobj().printTestStep("Verify The Create Meeting");

			fc.utobj().clickElement(driver, pobj.searchBtn);
			fc.utobj().selectDropDown(driver, pobj.scheduleType, "Meeting");
			fc.utobj().sendKeys(driver, pobj.searchWords, subject);
			if (!fc.utobj().isSelected(driver, pobj.lookInSubCheck)) {
				fc.utobj().clickElement(driver, pobj.lookInSubCheck);
			}
			fc.utobj().clickElement(driver, pobj.searchBtnAtSearch);

			boolean isSubjectPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[.='" + subject + "']");
			if (isSubjectPresent == false) {
				fc.utobj().throwsException("was not able to verify Create Meeting");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	public void createMeeting(WebDriver driver, String subject, String description) throws Exception {

		String testCaseId = "TC_Create_Meeting_CRM";

		if (fc.utobj().validate(testCaseId)) {
			try {
				CRMCalendarPage pobj = new CRMCalendarPage(driver);
				fc.crm().crm_common().CRMCalendarLnk(driver);
				fc.utobj().selectDropDown(driver, pobj.calendarUserSelect, "FranConnect Administrator");
				fc.utobj().clickElement(driver, pobj.createLink);
				fc.utobj().selectDropDown(driver, pobj.scheduleSelect, "Meeting");
				fc.utobj().sendKeys(driver, pobj.subject, subject);
				fc.utobj().sendKeys(driver, pobj.description, description);
				fc.utobj().selectDropDown(driver, pobj.scheduleScope, "Private");
				fc.utobj().sendKeys(driver, pobj.startDatetime, fc.utobj().getCurrentDateUSFormat());
				fc.utobj().sendKeys(driver, pobj.endDatetime, fc.utobj().getFutureDateUSFormat(2));
				fc.utobj().sendKeys(driver, pobj.location, "TestLocation");
				fc.utobj().selectDropDown(driver, pobj.priority, "Low");
				fc.utobj().clickElement(driver, pobj.createBtn);

				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException("was not able to create Meeting :: CRM > Calender");

		}
	}

	@Test(groups = "crm")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify the Modify Meeting ::  CRM > Calendar", testCaseId = "TC_273_Modify_Meeting")
	public void modifyMeeting() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			CRMCalendarPage pobj = new CRMCalendarPage(driver);

			fc.utobj().printTestStep("Navigate To CRM > Calendar");
			fc.utobj().printTestStep("Create Meeting");
			String subject = fc.utobj().generateTestData(dataSet.get("subject"));
			String description = fc.utobj().generateTestData(dataSet.get("description"));
			createMeeting(driver, subject, description);

			fc.utobj().clickElement(driver, pobj.searchBtn);
			fc.utobj().selectDropDown(driver, pobj.scheduleType, "Meeting");
			fc.utobj().sendKeys(driver, pobj.searchWords, subject);
			if (!fc.utobj().isSelected(driver, pobj.lookInSubCheck)) {
				fc.utobj().clickElement(driver, pobj.lookInSubCheck);
			}
			fc.utobj().clickElement(driver, pobj.searchBtnAtSearch);

			fc.utobj().printTestStep("Modify Meeting");
			fc.utobj().actionImgOption(driver, subject, "Modify");

			subject = fc.utobj().generateTestData(dataSet.get("subject"));
			fc.utobj().sendKeys(driver, pobj.subject, subject);
			description = fc.utobj().generateTestData(dataSet.get("description"));
			fc.utobj().sendKeys(driver, pobj.description, description);
			fc.utobj().selectDropDown(driver, pobj.scheduleScope, "Private");
			fc.utobj().sendKeys(driver, pobj.startDatetime, fc.utobj().getCurrentDateUSFormat());
			fc.utobj().sendKeys(driver, pobj.endDatetime, fc.utobj().getFutureDateUSFormat(3));
			fc.utobj().sendKeys(driver, pobj.location, "TestLocation");
			fc.utobj().selectDropDown(driver, pobj.priority, "Low");
			fc.utobj().clickElement(driver, pobj.saveBtn);

			fc.utobj().printTestStep("Verify Modify The Meeting");

			fc.utobj().clickElement(driver, pobj.searchBtn);
			fc.utobj().selectDropDown(driver, pobj.scheduleType, "Meeting");
			fc.utobj().sendKeys(driver, pobj.searchWords, subject);
			if (!fc.utobj().isSelected(driver, pobj.lookInSubCheck)) {
				fc.utobj().clickElement(driver, pobj.lookInSubCheck);
			}
			fc.utobj().clickElement(driver, pobj.searchBtnAtSearch);

			boolean isSubjectPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[.='" + subject + "']");
			if (isSubjectPresent == false) {
				fc.utobj().throwsException("was not able to verify Modify Meeting");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "crm")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify the Create Event ::  CRM > Calendar", testCaseId = "TC_274_Create_Event")
	public void createEvent() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			CRMCalendarPage pobj = new CRMCalendarPage(driver);

			fc.utobj().printTestStep("Navigate To CRM > Calendar");
			fc.utobj().printTestStep("Create Event");
			String option = "Event";
			String subject = fc.utobj().generateTestData(dataSet.get("subject"));
			String description = fc.utobj().generateTestData(dataSet.get("description"));
			createEvent(driver, option, subject, description, dataSet);

			/*
			 * fc.utobj().printTestStep(testCaseId, "Verify The Create Event");
			 * WebElement element=driver.findElement(By.linkText(subject));
			 * fc.utobj().clickElement(driver, element);
			 * 
			 * fc.utobj().moveToElement(driver, element); JavascriptExecutor
			 * executor = (JavascriptExecutor)driver;
			 * executor.executeScript("arguments[0].click();", element);
			 * 
			 * if (!fc.utobj().getElementByXpath(driver,
			 * ".//*[contains(text () , 'Schedule :')]/following-sibling::td[.='Event']"
			 * )).isDisplayed()) { fc.utobj().throwsException(
			 * "was not able to verify Event"); }
			 * 
			 * if (!pobj.subject.getAttribute("value").trim().equalsIgnoreCase(
			 * subject)) { fc.utobj().throwsException(
			 * "was not able to verify Subject of Event"); }
			 * 
			 * if
			 * (!pobj.description.getAttribute("value").trim().equalsIgnoreCase(
			 * description)) { fc.utobj().throwsException(
			 * "was not able to verify description of Event"); }
			 * 
			 * if (!pobj.startDatetime.getAttribute("value").trim().
			 * equalsIgnoreCase(fc.utobj().getCurrentDateUSFormat())) {
			 * fc.utobj().throwsException(
			 * "was not able to verify Start Date of Event"); } if
			 * (!pobj.endDatetime.getAttribute("value").trim().equalsIgnoreCase(
			 * fc.utobj().getFutureDateUSFormat(2))) {
			 * fc.utobj().throwsException(
			 * "was not able to verify End Date of Event"); }
			 */

			fc.utobj().printTestStep("Verify The Create Event");

			fc.utobj().clickElement(driver, pobj.searchBtn);
			fc.utobj().selectDropDown(driver, pobj.scheduleType, "Event");
			fc.utobj().sendKeys(driver, pobj.searchWords, subject);
			if (!fc.utobj().isSelected(driver, pobj.lookInSubCheck)) {
				fc.utobj().clickElement(driver, pobj.lookInSubCheck);
			}
			fc.utobj().clickElement(driver, pobj.searchBtnAtSearch);

			boolean isSubjectPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[.='" + subject + "']");
			if (isSubjectPresent == false) {
				fc.utobj().throwsException("was not able to verify Create Event");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "crm")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify the Modify Event ::  CRM > Calendar", testCaseId = "TC_275_Modify_Event")
	public void modifyEvent() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			CRMCalendarPage pobj = new CRMCalendarPage(driver);

			fc.utobj().printTestStep("Navigate To CRM > Calendar");
			fc.utobj().printTestStep("Create Event");
			String option = "Event";
			String subject = fc.utobj().generateTestData(dataSet.get("subject"));
			String description = fc.utobj().generateTestData(dataSet.get("description"));
			createEvent(driver, option, subject, description, dataSet);

			/*
			 * fc.utobj().printTestStep(testCaseId, "Modify Event"); WebElement
			 * element=driver.findElement(By.linkText(subject));
			 * fc.utobj().clickElement(driver, element);
			 * 
			 * fc.utobj().moveToElement(driver, element); JavascriptExecutor
			 * executor = (JavascriptExecutor)driver;
			 * executor.executeScript("arguments[0].click();", element);
			 */

			fc.utobj().printTestStep("Modify Event");

			fc.utobj().clickElement(driver, pobj.searchBtn);
			fc.utobj().selectDropDown(driver, pobj.scheduleType, "Event");
			fc.utobj().sendKeys(driver, pobj.searchWords, subject);
			if (!fc.utobj().isSelected(driver, pobj.lookInSubCheck)) {
				fc.utobj().clickElement(driver, pobj.lookInSubCheck);
			}
			fc.utobj().clickElement(driver, pobj.searchBtnAtSearch);

			fc.utobj().actionImgOption(driver, subject, "Modify");

			fc.utobj().sendKeys(driver, pobj.subject, subject);
			fc.utobj().sendKeys(driver, pobj.description, description);

			if (option.equalsIgnoreCase("Event")) {
				fc.utobj().selectDropDown(driver, pobj.scheduleScope, dataSet.get("scheduleScope"));
				fc.utobj().sendKeys(driver, pobj.startDatetime, fc.utobj().getCurrentDateUSFormat());
				fc.utobj().sendKeys(driver, pobj.endDatetime, fc.utobj().getFutureDateUSFormat(2));
				fc.utobj().clickElement(driver, pobj.alldayEvent);
				fc.utobj().selectDropDown(driver, pobj.categorySelect, dataSet.get("category"));
				fc.utobj().selectDropDown(driver, pobj.priority, dataSet.get("priority"));
			}

			fc.utobj().clickElement(driver, pobj.saveBtn);

			/*
			 * fc.utobj().printTestStep(testCaseId, "Modify Event"); WebElement
			 * element1=driver.findElement(By.linkText(subject));
			 * fc.utobj().clickElement(driver, element);
			 * 
			 * fc.utobj().moveToElement(driver, element1);
			 * executor.executeScript("arguments[0].click();", element1);
			 * 
			 * if (!fc.utobj().getElementByXpath(driver,
			 * ".//*[contains(text () , 'Schedule :')]/following-sibling::td[.='Event']"
			 * )).isDisplayed()) { fc.utobj().throwsException(
			 * "was not able to verify Event"); }
			 * 
			 * if (!pobj.subject.getAttribute("value").trim().equalsIgnoreCase(
			 * subject)) { fc.utobj().throwsException(
			 * "was not able to verify Subject of Event"); }
			 * 
			 * if
			 * (!pobj.description.getAttribute("value").trim().equalsIgnoreCase(
			 * description)) { fc.utobj().throwsException(
			 * "was not able to verify description of Event"); }
			 * 
			 * if (!pobj.startDatetime.getAttribute("value").trim().
			 * equalsIgnoreCase(fc.utobj().getCurrentDateUSFormat())) {
			 * fc.utobj().throwsException(
			 * "was not able to verify Start Date of Event"); } if
			 * (!pobj.endDatetime.getAttribute("value").trim().equalsIgnoreCase(
			 * fc.utobj().getFutureDateUSFormat(2))) {
			 * fc.utobj().throwsException(
			 * "was not able to verify End Date of Event"); }
			 */

			fc.utobj().printTestStep("Verify The Modify Event");

			fc.utobj().clickElement(driver, pobj.searchBtn);
			fc.utobj().selectDropDown(driver, pobj.scheduleType, "Event");
			fc.utobj().sendKeys(driver, pobj.searchWords, subject);
			if (!fc.utobj().isSelected(driver, pobj.lookInSubCheck)) {
				fc.utobj().clickElement(driver, pobj.lookInSubCheck);
			}
			fc.utobj().clickElement(driver, pobj.searchBtnAtSearch);

			boolean isSubjectPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[.='" + subject + "']");
			if (isSubjectPresent == false) {
				fc.utobj().throwsException("was not able to verify Modify Event");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	public void createEvent(WebDriver driver, String option, String subject, String description,
			Map<String, String> dataSet) throws Exception {

		String testCaseId = "TC_Create_Event_CRM";

		if (fc.utobj().validate(testCaseId)) {
			try {
				CRMCalendarPage pobj = new CRMCalendarPage(driver);
				fc.crm().crm_common().CRMCalendarLnk(driver);
				fc.utobj().clickElement(driver, pobj.createLink);
				fc.utobj().selectDropDown(driver, pobj.scheduleSelect, option);
				fc.utobj().sendKeys(driver, pobj.subject, subject);
				fc.utobj().sendKeys(driver, pobj.description, description);

				if (option.equalsIgnoreCase("Event")) {
					fc.utobj().selectDropDown(driver, pobj.scheduleScope, dataSet.get("scheduleScope"));
					fc.utobj().sendKeys(driver, pobj.startDatetime, fc.utobj().getCurrentDateUSFormat());
					fc.utobj().sendKeys(driver, pobj.endDatetime, fc.utobj().getFutureDateUSFormat(2));
					fc.utobj().clickElement(driver, pobj.alldayEvent);
					fc.utobj().selectDropDown(driver, pobj.categorySelect, dataSet.get("category"));
					fc.utobj().selectDropDown(driver, pobj.priority, dataSet.get("priority"));
				}

				fc.utobj().clickElement(driver, pobj.createBtn);

				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException("was not able to verify Create Event :: CRM > Calender ");

		}
	}
}
