package com.builds.test.crm;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.xml.parsers.ParserConfigurationException;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.Test;
import org.xml.sax.SAXException;

import com.builds.test.admin.AdminAreaRegionAddAreaRegionPageTest;
import com.builds.test.admin.AdminFranchiseLocationAddFranchiseLocationPageTest;
import com.builds.test.admin.AdminUsersManageCorporateUsersAddCorporateUserPageTest;
import com.builds.test.admin.AdminUsersManageDivisionalUsersAddDivisionalUsersPageTest;
import com.builds.test.admin.AdminUsersManageRegionalUsersAddRegionalUserPageTest;
import com.builds.test.common.CorporateUser;
import com.builds.uimaps.campaigncentercrm.CRMCampaignCenterCampaignsPage;
import com.builds.uimaps.common.LoginPage;
import com.builds.uimaps.crm.CRMContactsPage;
import com.builds.uimaps.crm.CRMLeadsPage;
import com.builds.uimaps.fs.FSCampaignCenterPage;
import com.builds.utilities.FranconnectUtil;
import com.builds.utilities.TestCase;

public class CRMCampaignCenterCampaignsPageTest {

	FranconnectUtil fc = new FranconnectUtil();

	@Test(groups = { "crm", "crmRavi", "crmCampaignTest"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Creation Of Template in Campaign At CRM > Campaign Center > Template with Gloabl accessibility", testCaseId = "TC_Corporate_Template_Global_Accessibility")
	private void createCampaignTemplateGlobalAccessibility()
			throws IOException, Exception, ParserConfigurationException, SAXException {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			fc.utobj().printTestStep("CRM  > Campaign Center > Template with Gloabl accessibility");
			String templateName = fc.utobj().generateTestData(dataSet.get("templateName"));
			CRMCampaignCenterCampaignsPage pobj = new CRMCampaignCenterCampaignsPage(driver);
			fc.utobj().printTestStep("Navigate To CRM > Campaign Center");
			fc.crm().crm_common().CRMCampaignCenterLnk(driver);
			fc.utobj().clickElement(driver, pobj.dashBoradLnk);
			fc.utobj().clickElement(driver, pobj.templatesLink);
			fc.utobj().clickElement(driver, pobj.create);
			fc.utobj().clickElement(driver, pobj.createtemplatelist);
			fc.utobj().clickElement(driver, pobj.choosetemplate);
			fc.utobj().clickElement(driver, pobj.choosetemplate_custom);
			String emailSubject = fc.utobj().generateTestData(dataSet.get("emailSubject"));
			fc.utobj().sendKeys(driver, pobj.templateName, templateName);
			fc.utobj().sendKeys(driver, pobj.mailSubject, emailSubject);
			Select dropdown = new Select(fc.utobj().getElementByID(driver, "Accessibility"));

			dropdown.selectByValue("Global");
			fc.utobj().clickElement(driver, pobj.TemplateInfodoneBtn);
			fc.utobj().clickElement(driver, pobj.leafelementcenterimg);
			fc.utobj().clickElement(driver, pobj.uploadbutton);
			String fileName = fc.utobj().getFilePathFromTestData("pictureFile.jpg");
			fc.utobj().sendKeys(driver, pobj.uploadImage, fileName);
			fc.utobj().clickElement(driver, pobj.applyimage);
			JavascriptExecutor js = (JavascriptExecutor) driver;
			js.executeScript("arguments[0].innerHTML = arguments[1]", pobj.paragraph1,
					"This is a Lamborghini Aventador");
			fc.utobj().clickElement(driver, pobj.leafelementleftimg);
			fc.utobj().clickElement(driver, pobj.uploadbutton);
			fc.utobj().sendKeys(driver, pobj.uploadImage, fileName);
			fc.utobj().clickElement(driver, pobj.applyimage);
			fc.utobj().clickElement(driver, pobj.leafelementrightimg);
			fc.utobj().clickElement(driver, pobj.uploadbutton);
			fc.utobj().sendKeys(driver, pobj.uploadImage, fileName);
			fc.utobj().clickElement(driver, pobj.applyimage);
			js.executeScript("arguments[0].innerHTML = arguments[1]", pobj.paragraph2, "Now Available in India");
			fc.utobj().clickElement(driver, pobj.savetemplate);
			fc.utobj().clickElement(driver, pobj.dashBoradLnk);
			fc.utobj().clickElement(driver, pobj.templatesLink);
			fc.utobj().clickElement(driver, pobj.showFilter);
			fc.utobj().sendKeys(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='searchTemplate']"),
					templateName);
			fc.utobj().clickElement(driver, pobj.applyFilterAtGroup);
			fc.utobj().clickElement(driver, pobj.hideFilter);
			fc.utobj().printTestStep("checking for self");
			boolean foundforself = fc.utobj().isElementPresent(driver, ".//a[contains(text(),'"+templateName+"')]");
			boolean isLocationTemplate = false;
			fc.utobj().printTestStep("checking for other corp user");
			boolean foundwithcorpuser = CheckWithCorpUser(driver, config, dataSet, templateName, isLocationTemplate);
			fc.utobj().printTestStep("checking for divisional user");
			boolean foundwithdivuser = CheckWithDivUser(driver, config, dataSet, templateName, isLocationTemplate);
			fc.utobj().printTestStep("checking for regional user");
			String region = dataSet.get("regionName") + fc.utobj().generateRandomNumber();
			boolean foundwithreguser = CheckWithRegUser(driver, config, dataSet, region, templateName,
					isLocationTemplate);
			fc.utobj().printTestStep("checking for franchisee user");
			boolean foundwithfrauser = CheckWithFranUser(driver, config, dataSet, templateName, isLocationTemplate);
			if (!foundwithcorpuser || !foundwithdivuser || !foundwithreguser || !foundwithfrauser || !foundforself) {
				fc.utobj().throwsException("Template checked with All user but not found with someone");
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "crm", "crmRavi", "crmCampaignTest" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Creation Of Template in Campaign At CRM > Campaign Center > Template with public to all Corp User accessibility", testCaseId = "TC_Corporate_Template_Public_To_All_Corporate_Accessibility")
	private void createCampaignTemplatePublicToAll()
			throws IOException, Exception, ParserConfigurationException, SAXException {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			CRMCampaignCenterCampaignsPage pobj = new CRMCampaignCenterCampaignsPage(driver);
			fc.utobj().printTestStep("Navigate To CRM > Campaign Center");
			fc.crm().crm_common().CRMCampaignCenterLnk(driver);
			fc.utobj().clickElement(driver, pobj.dashBoradLnk);
			fc.utobj().clickElement(driver, pobj.templatesLink);
			fc.utobj().clickElement(driver, pobj.create);
			fc.utobj().clickElement(driver, pobj.createtemplatelist);
			fc.utobj().clickElement(driver, pobj.choosetemplate);
			fc.utobj().clickElement(driver, pobj.choosetemplate_custom);
			String templateName = fc.utobj().generateTestData(dataSet.get("templateName"));
			String emailSubject = fc.utobj().generateTestData(dataSet.get("emailSubject"));
			fc.utobj().sendKeys(driver, pobj.templateName, templateName);
			fc.utobj().sendKeys(driver, pobj.mailSubject, emailSubject);
			Select dropdown = new Select(fc.utobj().getElementByID(driver, "Accessibility"));
			dropdown.selectByValue("Public");
			fc.utobj().clickElement(driver, pobj.TemplateInfodoneBtn);
			fc.utobj().clickElement(driver, pobj.leafelementcenterimg);
			fc.utobj().clickElement(driver, pobj.uploadbutton);
			String fileName = fc.utobj().getFilePathFromTestData("pictureFile.jpg");
			fc.utobj().sendKeys(driver, pobj.uploadImage, fileName);
			fc.utobj().clickElement(driver, pobj.applyimage);
			JavascriptExecutor js = (JavascriptExecutor) driver;
			js.executeScript("arguments[0].innerHTML = arguments[1]", pobj.paragraph1,
					"This is a Lamborghini Aventador");
			fc.utobj().clickElement(driver, pobj.leafelementleftimg);
			fc.utobj().clickElement(driver, pobj.uploadbutton);
			fc.utobj().sendKeys(driver, pobj.uploadImage, fileName);
			fc.utobj().clickElement(driver, pobj.applyimage);
			fc.utobj().clickElement(driver, pobj.leafelementrightimg);
			fc.utobj().clickElement(driver, pobj.uploadbutton);
			fc.utobj().sendKeys(driver, pobj.uploadImage, fileName);
			fc.utobj().clickElement(driver, pobj.applyimage);
			js.executeScript("arguments[0].innerHTML = arguments[1]", pobj.paragraph2, "Now Available in India");
			fc.utobj().clickElement(driver, pobj.savetemplate);
			fc.utobj().clickElement(driver, pobj.dashBoradLnk);
			fc.utobj().clickElement(driver, pobj.templatesLink);
			fc.utobj().clickElement(driver, pobj.showFilter);
			fc.utobj().sendKeys(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='searchTemplate']"),
					templateName);
			fc.utobj().clickElement(driver, pobj.applyFilterAtGroup);
			fc.utobj().clickElement(driver, pobj.hideFilter);

			boolean foundforself = fc.utobj().assertPageSource(driver, templateName);
			boolean isLocationTemplate = false;
			fc.utobj().printTestStep("checking for other corp user");
			boolean foundwithcorpuser = CheckWithCorpUser(driver, config, dataSet, templateName, isLocationTemplate);
			fc.utobj().printTestStep("checking for divisional user");
			boolean foundwithdivuser = CheckWithDivUser(driver, config, dataSet, templateName, isLocationTemplate);
			fc.utobj().printTestStep("checking for regional user");
			String region = dataSet.get("regionName") + fc.utobj().generateRandomNumber();
			boolean foundwithreguser = CheckWithRegUser(driver, config, dataSet, region, templateName,
					isLocationTemplate);
			fc.utobj().printTestStep("checking for franchisee user");
			boolean foundwithfrauser = CheckWithFranUser(driver, config, dataSet, templateName, isLocationTemplate);

			if (!foundwithcorpuser || foundwithdivuser || foundwithreguser || foundwithfrauser || !foundforself) {
				fc.utobj().throwsException("Template checked with All user but not found with someone");
				}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "crm"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Creation Of Template in Campaign At CRM > Campaign Center > Template with Private accessibility for corp user", testCaseId = "TC_Corporate_Template_Private_Accessibility")
	private void createCampaignTemplatePrivate()
			throws IOException, Exception, ParserConfigurationException, SAXException {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			CRMCampaignCenterCampaignsPage pobj = new CRMCampaignCenterCampaignsPage(driver);
			fc.utobj().printTestStep("Navigate To CRM > Campaign Center");
			fc.crm().crm_common().CRMCampaignCenterLnk(driver);
			fc.utobj().clickElement(driver, pobj.dashBoradLnk);
			fc.utobj().clickElement(driver, pobj.templatesLink);
			fc.utobj().clickElement(driver, pobj.create);
			fc.utobj().clickElement(driver, pobj.createtemplatelist);
			fc.utobj().clickElement(driver, pobj.choosetemplate);
			fc.utobj().clickElement(driver, pobj.choosetemplate_custom);
			String templateName = fc.utobj().generateTestData(dataSet.get("templateName"));
			String emailSubject = fc.utobj().generateTestData(dataSet.get("emailSubject"));
			fc.utobj().sendKeys(driver, pobj.templateName, templateName);
			fc.utobj().sendKeys(driver, pobj.mailSubject, emailSubject);
			Select dropdown = new Select(fc.utobj().getElementByID(driver, "Accessibility"));
			dropdown.selectByValue("Private");
			fc.utobj().clickElement(driver, pobj.TemplateInfodoneBtn);
			fc.utobj().clickElement(driver, pobj.leafelementcenterimg);
			fc.utobj().clickElement(driver, pobj.uploadbutton);

			String fileName = fc.utobj().getFilePathFromTestData("pictureFile.jpg");
			fc.utobj().sendKeys(driver, pobj.uploadImage, fileName);
			fc.utobj().clickElement(driver, pobj.applyimage);
			JavascriptExecutor js = (JavascriptExecutor) driver;
			js.executeScript("arguments[0].innerHTML = arguments[1]", pobj.paragraph1,
					"This is a Lamborghini Aventador");

			fc.utobj().clickElement(driver, pobj.leafelementleftimg);
			fc.utobj().clickElement(driver, pobj.uploadbutton);
			fc.utobj().sendKeys(driver, pobj.uploadImage, fileName);
			fc.utobj().clickElement(driver, pobj.applyimage);

			fc.utobj().clickElement(driver, pobj.leafelementrightimg);
			fc.utobj().clickElement(driver, pobj.uploadbutton);
			fc.utobj().sendKeys(driver, pobj.uploadImage, fileName);
			fc.utobj().clickElement(driver, pobj.applyimage);

			js.executeScript("arguments[0].innerHTML = arguments[1]", pobj.paragraph2, "Now Available in India");

			fc.utobj().clickElement(driver, pobj.savetemplate);
			fc.utobj().clickElement(driver, pobj.dashBoradLnk);
			fc.utobj().clickElement(driver, pobj.templatesLink);
			fc.utobj().clickElement(driver, pobj.showFilter);
			fc.utobj().sendKeys(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='searchTemplate']"),
					templateName);
			fc.utobj().clickElement(driver, pobj.applyFilterAtGroup);
			fc.utobj().clickElement(driver, pobj.hideFilter);
			boolean foundforself = fc.utobj().assertPageSource(driver, templateName);
			// Checking for other corp user	
			boolean isLocationTemplate = false;
			fc.utobj().printTestStep("checking for other corp user");
			boolean foundwithcorpuser = CheckWithCorpUser(driver, config, dataSet, templateName, isLocationTemplate);
			fc.utobj().printTestStep("checking for divisional user");
			boolean foundwithdivuser = CheckWithDivUser(driver, config, dataSet, templateName, isLocationTemplate);
			fc.utobj().printTestStep("checking for regional user");
			String region = dataSet.get("regionName") + fc.utobj().generateRandomNumber();
			boolean foundwithreguser = CheckWithRegUser(driver, config, dataSet, region, templateName,
					isLocationTemplate);
			fc.utobj().printTestStep("checking for franchisee user");
			boolean foundwithfrauser = CheckWithFranUser(driver, config, dataSet, templateName, isLocationTemplate);
			if (!foundwithcorpuser || foundwithdivuser || foundwithreguser || foundwithfrauser || !foundforself)
			{
				fc.utobj().throwsException("Template checked with All user but not found with someone");
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "crm", "crmcheck"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2017-06-26", testCaseDescription = "Verify The Creation Of Template in Campaign At CRM > Campaign Center > Template with Public accessibility to all Franchisee", testCaseId = "TC_Franchise_Template_Accessibility_Public_To_All_My_Franchisees")
	private void createCampaignTemplatePublicToAllMyFranchisees()
			throws IOException, Exception, ParserConfigurationException, SAXException {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			String regionName = dataSet.get("regionName") + fc.utobj().generateRandomNumber();

			String storeType = dataSet.get("storeType");
			String franchiseName = dataSet.get("franchiseName");
			String centerName = dataSet.get("centerName");
			String openingDate = fc.utobj().generateCurrentDatewithformat("yyyy-MM-dd");
			String password = "T0n1ght123";
			String confirmPassword = "T0n1ght123";
			String loginId = dataSet.get("loginId") + fc.utobj().generateRandomNumber();
			String ownerFirstName = dataSet.get("ownerFirstName") + fc.utobj().generateRandomNumber6Digit();
			String ownerLastName = dataSet.get("ownerLastName") + fc.utobj().generateRandomNumber6Digit();
			String emailId = dataSet.get("emailId");
			AdminFranchiseLocationAddFranchiseLocationPageTest fran = new AdminFranchiseLocationAddFranchiseLocationPageTest();
			fran.addFranchiseLocationWithUserZC(driver, regionName, storeType, franchiseName, centerName, openingDate,
					password, confirmPassword, loginId, ownerFirstName, ownerLastName, config, emailId);

			fc.home_page().logout(driver);

			/*
			 * driver.get(config.get("buildUrl")); LoginPage npobj = new
			 * LoginPage(driver); fc.utobj().sendKeys(driver, npobj.userid,
			 * loginId); fc.utobj().sendKeys(driver, npobj.password, password);
			 * fc.utobj().clickElement(driver, npobj.login);
			 */

			fc.loginpage().loginWithParameter(driver, loginId, password);

			CRMCampaignCenterCampaignsPage pobj = new CRMCampaignCenterCampaignsPage(driver);
			fc.utobj().printTestStep("Navigate To CRM > Campaign Center");
			fc.crm().crm_common().CRMCampaignCenterLnk(driver);
			fc.utobj().clickElement(driver, pobj.dashBoradLnk);
			fc.utobj().clickElement(driver, pobj.templatesLink);
			fc.utobj().clickElement(driver, pobj.create);
			fc.utobj().clickElement(driver, pobj.createtemplatelist);
			fc.utobj().clickElement(driver, pobj.choosetemplate);
			fc.utobj().clickElement(driver, pobj.clickonchoosetemplate);
			String templateName = fc.utobj().generateTestData(dataSet.get("templateName"));
			String emailSubject = fc.utobj().generateTestData(dataSet.get("emailSubject"));    
			fc.utobj().sendKeys(driver, pobj.templateName, templateName);
			fc.utobj().sendKeys(driver, pobj.mailSubject, emailSubject);
			Select dropdown = new Select(fc.utobj().getElementByID(driver, "Accessibility"));
			dropdown.selectByValue("Public");
			fc.utobj().clickElement(driver, pobj.TemplateInfodoneBtn);
			fc.utobj().clickElement(driver, pobj.leafelementcenterimg);
			fc.utobj().clickElement(driver, pobj.uploadbutton);

			String fileName = fc.utobj().getFilePathFromTestData("pictureFile.jpg");
			fc.utobj().sendKeys(driver, pobj.uploadImage, fileName);
			fc.utobj().clickElement(driver, pobj.applyimage);
			JavascriptExecutor js = (JavascriptExecutor) driver;
			js.executeScript("arguments[0].innerHTML = arguments[1]", pobj.paragraph1,
					"This is a Lamborghini Aventador");

			fc.utobj().clickElement(driver, pobj.leafelementleftimg);
			fc.utobj().clickElement(driver, pobj.uploadbutton);
			fc.utobj().sendKeys(driver, pobj.uploadImage, fileName);
			fc.utobj().clickElement(driver, pobj.applyimage);

			fc.utobj().clickElement(driver, pobj.leafelementrightimg);
			fc.utobj().clickElement(driver, pobj.uploadbutton);
			fc.utobj().sendKeys(driver, pobj.uploadImage, fileName);
			fc.utobj().clickElement(driver, pobj.applyimage);

			js.executeScript("arguments[0].innerHTML = arguments[1]", pobj.paragraph2, "Now Available in India");

			fc.utobj().clickElement(driver, pobj.savetemplate);
			fc.utobj().clickElement(driver, pobj.dashBoradLnk);
			fc.utobj().clickElement(driver, pobj.templatesLink);
			fc.utobj().clickElement(driver, pobj.locationtemplatesLink);
			fc.utobj().clickElement(driver, pobj.showFilter);
			fc.utobj().sendKeys(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='searchTemplate']"),
					templateName);
			fc.utobj().clickElement(driver, pobj.applyFilterAtGroup);
			fc.utobj().clickElement(driver, pobj.hideFilter);
			boolean foundforself = fc.utobj().isElementPresent(driver, ".//a[contains(text(),'"+templateName+"')]");

			fc.home_page().logout(driver);
			fc.loginpage().loginWithParameter(driver, config.get("userName"), config.get("password"));

			// Checking for other corp user
			boolean isLocationTemplate = true;
			fc.utobj().printTestStep("checking for other corp user");
			boolean foundwithcorpuser = CheckWithCorpUser(driver, config, dataSet, templateName, isLocationTemplate);
			fc.utobj().printTestStep("checking for divisional user");
			boolean foundwithdivuser = CheckWithDivUser(driver, config, dataSet, templateName, isLocationTemplate);
																													
			fc.utobj().printTestStep("checking for regional user");
			String region = dataSet.get("regionName") + fc.utobj().generateRandomNumber();
			boolean foundwithreguser = CheckWithRegUser(driver, config, dataSet, region, templateName,
					isLocationTemplate);
			fc.utobj().printTestStep("checking for same region regional user");
			boolean foundwithSameRegionreguser = CheckWithRegUserWithRegionNameExist(driver, config, dataSet,
					templateName, regionName, isLocationTemplate);
			fc.utobj().printTestStep("checking for franchisee user");
			boolean foundwithfrauser = CheckWithFranUser(driver, config, dataSet, templateName, isLocationTemplate);
																													
			fc.utobj().printTestStep("checking for Same Region franchisee user");
			boolean foundwithSameRegionfrauser = CheckWithFranUserWithRegionNameExist(driver, config, dataSet,
					templateName, regionName, isLocationTemplate);//
			if (!foundwithcorpuser || foundwithdivuser || foundwithreguser || foundwithfrauser || !foundforself
					|| !foundwithSameRegionreguser || foundwithSameRegionfrauser) {
				fc.utobj().throwsException("Template checked with All user but not found with someone");
				}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "crmN")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Creation Of Template in Campaign At CRM > Campaign Center > Template with Private accessibility to fran user", testCaseId = "TC_Franchise_Template_Acceessibility_Private")
	private void createCampaignTemplatePrivateAccessbility()
			throws IOException, Exception, ParserConfigurationException, SAXException {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			String regionName = dataSet.get("regionName") + fc.utobj().generateRandomNumber();

			String storeType = dataSet.get("storeType");
			String franchiseName = dataSet.get("franchiseName");
			String centerName = dataSet.get("centerName");
			String openingDate = fc.utobj().generateCurrentDatewithformat("yyyy-MM-dd");
			String password = dataSet.get("password");
			String confirmPassword = dataSet.get("confirmPassword");
			String loginId = dataSet.get("loginId") + fc.utobj().generateRandomNumber();
			String ownerFirstName = dataSet.get("ownerFirstName") + fc.utobj().generateRandomNumber6Digit();
			String ownerLastName = dataSet.get("ownerLastName") + fc.utobj().generateRandomNumber6Digit();
			String emailId = dataSet.get("emailId");
			AdminFranchiseLocationAddFranchiseLocationPageTest fran = new AdminFranchiseLocationAddFranchiseLocationPageTest();
			fran.addFranchiseLocationWithUserZC(driver, regionName, storeType, franchiseName, centerName, openingDate,
					password, confirmPassword, loginId, ownerFirstName, ownerLastName, config, emailId);

			fc.home_page().logout(driver);

			/*
			 * driver.get(config.get("buildUrl")); LoginPage npobj = new
			 * LoginPage(driver); fc.utobj().sendKeys(driver, npobj.userid,
			 * loginId); fc.utobj().sendKeys(driver, npobj.password, password);
			 * fc.utobj().clickElement(driver, npobj.login);
			 */

			fc.loginpage().loginWithParameter(driver, loginId, password);

			CRMCampaignCenterCampaignsPage pobj = new CRMCampaignCenterCampaignsPage(driver);
			fc.utobj().printTestStep("Navigate To CRM > Campaign Center");
			fc.crm().crm_common().CRMCampaignCenterLnk(driver);
			fc.utobj().clickElement(driver, pobj.dashBoradLnk);
			fc.utobj().clickElement(driver, pobj.templatesLink);
			fc.utobj().clickElement(driver, pobj.create);
			fc.utobj().clickElement(driver, pobj.createtemplatelist);
			fc.utobj().clickElement(driver, pobj.choosetemplate);
			String templateName = fc.utobj().generateTestData(dataSet.get("templateName"));
			String emailSubject = fc.utobj().generateTestData(dataSet.get("emailSubject"));
			fc.utobj().sendKeys(driver, pobj.templateName, templateName);
			fc.utobj().sendKeys(driver, pobj.mailSubject, emailSubject);
			Select dropdown = new Select(fc.utobj().getElementByID(driver, "Accessibility"));
			dropdown.selectByValue("Private");
			fc.utobj().clickElement(driver, pobj.TemplateInfodoneBtn);
			fc.utobj().clickElement(driver, pobj.leafelementcenterimg);
			fc.utobj().clickElement(driver, pobj.uploadbutton);

			String fileName = fc.utobj().getFilePathFromTestData("pictureFile.jpg");
			fc.utobj().sendKeys(driver, pobj.uploadImage, fileName);
			fc.utobj().clickElement(driver, pobj.applyimage);
			JavascriptExecutor js = (JavascriptExecutor) driver;
			js.executeScript("arguments[0].innerHTML = arguments[1]", pobj.paragraph1,
					"This is a Lamborghini Aventador");

			fc.utobj().clickElement(driver, pobj.leafelementleftimg);
			fc.utobj().clickElement(driver, pobj.uploadbutton);
			fc.utobj().sendKeys(driver, pobj.uploadImage, fileName);
			fc.utobj().clickElement(driver, pobj.applyimage);

			fc.utobj().clickElement(driver, pobj.leafelementrightimg);
			fc.utobj().clickElement(driver, pobj.uploadbutton);
			fc.utobj().sendKeys(driver, pobj.uploadImage, fileName);
			fc.utobj().clickElement(driver, pobj.applyimage);

			js.executeScript("arguments[0].innerHTML = arguments[1]", pobj.paragraph2, "Now Available in India");

			fc.utobj().clickElement(driver, pobj.savetemplate);
			fc.utobj().clickElement(driver, pobj.dashBoradLnk);
			fc.utobj().clickElement(driver, pobj.templatesLink);
			fc.utobj().clickElement(driver, pobj.locationtemplatesLink);
			fc.utobj().clickElement(driver, pobj.showFilter);
			fc.utobj().sendKeys(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='searchTemplate']"),
					templateName);
			fc.utobj().clickElement(driver, pobj.applyFilterAtGroup);
			fc.utobj().clickElement(driver, pobj.hideFilter);
			boolean foundforself = fc.utobj().isElementPresent(driver, ".//a[contains(text(),'"+templateName+"')]");
			//boolean foundforself = fc.utobj().assertPageSource(driver, templateName);

			fc.home_page().logout(driver);
			fc.loginpage().loginWithParameter(driver, config.get("userName"), config.get("password"));

			// Checking for other corp user
			boolean isLocationTemplate = true;
			fc.utobj().printTestStep("checking for other corp user");
			boolean foundwithcorpuser = CheckWithCorpUser(driver, config, dataSet, templateName, isLocationTemplate);
			fc.utobj().printTestStep("checking for divisional user");
			boolean foundwithdivuser = CheckWithDivUser(driver, config, dataSet, templateName, isLocationTemplate);
			fc.utobj().printTestStep("checking for regional user");
			String region = dataSet.get("regionName") + fc.utobj().generateRandomNumber();
			boolean foundwithreguser = CheckWithRegUser(driver, config, dataSet, region, templateName,
					isLocationTemplate);
			fc.utobj().printTestStep("checking for same region regional user");
			boolean foundwithSameRegionreguser = CheckWithRegUserWithRegionNameExist(driver, config, dataSet,
					templateName, regionName, isLocationTemplate);
			fc.utobj().printTestStep("checking for franchisee user");
			boolean foundwithfrauser = CheckWithFranUser(driver, config, dataSet, templateName, isLocationTemplate);
			fc.utobj().printTestStep("checking for Same Region franchisee user");
			boolean foundwithSameRegionfrauser = CheckWithFranUserWithRegionNameExist(driver, config, dataSet,
					templateName, regionName, isLocationTemplate);
			if (!foundwithcorpuser || foundwithdivuser || foundwithreguser || foundwithfrauser || !foundforself
					|| !foundwithSameRegionreguser || !foundwithSameRegionfrauser)
			{
				fc.utobj().throwsException("Template checked with All user but not found with someone");
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "crm", "crmRavi", "crmCampaignTest"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Creation Of Template in Campaign At CRM > Campaign Center > Template with Public accessibility to fran user", testCaseId = "TC_Franchise_Template_Acessibility_Region_Public")
	private void createCampaignTemplatAccessbilityRegionPublic()
			throws IOException, Exception, ParserConfigurationException, SAXException {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			String regionName = dataSet.get("regionName") + fc.utobj().generateRandomNumber();
			String storeType = dataSet.get("storeType");
			String franchiseName = dataSet.get("franchiseName");
			String centerName = dataSet.get("centerName");
			String openingDate = fc.utobj().generateCurrentDatewithformat("yyyy-MM-dd");
			String password = dataSet.get("password");
			String confirmPassword = dataSet.get("confirmPassword");
			String loginId = dataSet.get("loginId") + fc.utobj().generateRandomNumber();
			String ownerFirstName = dataSet.get("ownerFirstName") + fc.utobj().generateRandomNumber6Digit();
			String ownerLastName = dataSet.get("ownerLastName") + fc.utobj().generateRandomNumber6Digit();
			String emailId = dataSet.get("emailId");
			AdminFranchiseLocationAddFranchiseLocationPageTest fran = new AdminFranchiseLocationAddFranchiseLocationPageTest();
			fran.addFranchiseLocationWithUserZC(driver, regionName, storeType, franchiseName, centerName, openingDate,
					password, confirmPassword, loginId, ownerFirstName, ownerLastName, config, emailId);

			fc.home_page().logout(driver);

			/*
			 * driver.get(config.get("buildUrl")); LoginPage npobj = new
			 * LoginPage(driver); fc.utobj().sendKeys(driver, npobj.userid,
			 * loginId); fc.utobj().sendKeys(driver, npobj.password, password);
			 * fc.utobj().clickElement(driver, npobj.login);
			 */

			fc.loginpage().loginWithParameter(driver, loginId, password);

			CRMCampaignCenterCampaignsPage pobj = new CRMCampaignCenterCampaignsPage(driver);
			fc.utobj().printTestStep("Navigate To CRM > Campaign Center");
			fc.crm().crm_common().CRMCampaignCenterLnk(driver);
			fc.utobj().clickElement(driver, pobj.dashBoradLnk);
			fc.utobj().clickElement(driver, pobj.templatesLink);
			fc.utobj().clickElement(driver, pobj.create);
			fc.utobj().clickElement(driver, pobj.createtemplatelist);
			fc.utobj().clickElement(driver, pobj.choosetemplate);
			
			fc.utobj().clickElement(driver,"//*[@id='LayoutSlider']/li[6]/div/div/div[2]/div[1]/p/a");
			String templateName = fc.utobj().generateTestData(dataSet.get("templateName"));
			String emailSubject = fc.utobj().generateTestData(dataSet.get("emailSubject"));
			fc.utobj().sendKeys(driver, pobj.templateName, templateName);
			fc.utobj().sendKeys(driver, pobj.mailSubject, emailSubject);
			Select dropdown = new Select(fc.utobj().getElementByID(driver, "Accessibility"));
			dropdown.selectByValue("Public");
			fc.utobj().clickElement(driver, pobj.TemplateInfodoneBtn);
			fc.utobj().clickElement(driver, pobj.leafelementcenterimg);
			fc.utobj().clickElement(driver, pobj.uploadbutton);

			String fileName = fc.utobj().getFilePathFromTestData("pictureFile.jpg");
			fc.utobj().sendKeys(driver, pobj.uploadImage, fileName);
			fc.utobj().clickElement(driver, pobj.applyimage);
			JavascriptExecutor js = (JavascriptExecutor) driver;
			js.executeScript("arguments[0].innerHTML = arguments[1]", pobj.paragraph1,
					"This is a Lamborghini Aventador");

			fc.utobj().clickElement(driver, pobj.leafelementleftimg);
			fc.utobj().clickElement(driver, pobj.uploadbutton);
			fc.utobj().sendKeys(driver, pobj.uploadImage, fileName);
			fc.utobj().clickElement(driver, pobj.applyimage);

			fc.utobj().clickElement(driver, pobj.leafelementrightimg);
			fc.utobj().clickElement(driver, pobj.uploadbutton);
			fc.utobj().sendKeys(driver, pobj.uploadImage, fileName);
			fc.utobj().clickElement(driver, pobj.applyimage);

			js.executeScript("arguments[0].innerHTML = arguments[1]", pobj.paragraph2, "Now Available in India");

			fc.utobj().clickElement(driver, pobj.savetemplate);
			fc.utobj().clickElement(driver, pobj.dashBoradLnk);
			fc.utobj().clickElement(driver, pobj.templatesLink);
			fc.utobj().clickElement(driver, pobj.locationtemplatesLink);
			fc.utobj().clickElement(driver, pobj.showFilter);
			fc.utobj().sendKeys(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='searchTemplate']"),
					templateName);
			fc.utobj().clickElement(driver, pobj.applyFilterAtGroup);
			fc.utobj().clickElement(driver, pobj.hideFilter);
			boolean foundforself = fc.utobj().isElementPresent(driver, ".//a[contains(text(),'"+templateName+"')]");
			//boolean foundforself = fc.utobj().assertPageSource(driver, templateName);

			fc.home_page().logout(driver);
			fc.loginpage().loginWithParameter(driver, config.get("userName"), config.get("password"));

			// Checking for other corp user
			boolean isLocationTemplate = true;
			fc.utobj().printTestStep("checking for other corp user");
			boolean foundwithcorpuser = CheckWithCorpUser(driver, config, dataSet, templateName, isLocationTemplate);
			fc.utobj().printTestStep("checking for divisional user");
			boolean foundwithdivuser = CheckWithDivUser(driver, config, dataSet, templateName, isLocationTemplate);
			fc.utobj().printTestStep("checking for regional user");
			String region = dataSet.get("regionName") + fc.utobj().generateRandomNumber();
			boolean foundwithreguser = CheckWithRegUser(driver, config, dataSet, region, templateName,
					isLocationTemplate);
			fc.utobj().printTestStep("checking for same region regional user");
			boolean foundwithSameRegionreguser = CheckWithRegUserWithRegionNameExist(driver, config, dataSet,
					templateName, regionName, isLocationTemplate);
			fc.utobj().printTestStep("checking for franchisee user");
			boolean foundwithfrauser = CheckWithFranUser(driver, config, dataSet, templateName, isLocationTemplate);
			fc.utobj().printTestStep("checking for Same Region franchisee user");
			boolean foundwithSameRegionfrauser = CheckWithFranUserWithRegionNameExist(driver, config, dataSet,
					templateName, regionName, isLocationTemplate);
			if (!foundwithcorpuser || foundwithdivuser || foundwithreguser || foundwithfrauser || !foundforself
					|| !foundwithSameRegionreguser || foundwithSameRegionfrauser)
			{
				fc.utobj().throwsException("Template checked with All user but not found with someone");
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "crm", "crmRavi", "crmCampaignTest"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Creation Of Template in Campaign At CRM > Campaign Center > Template with Public accessibility to reg user", testCaseId = "TC_Regional_Template_Accessibility_Public")
	private void createCampaignTemplatAccessbilitRegionalUserPublic()
			throws IOException, Exception, ParserConfigurationException, SAXException {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			boolean isLocationTemplate = false;
			driver = fc.loginpage().login(driver);
			String userName = dataSet.get("userName") + fc.utobj().generateRandomNumber();
			String emailId = dataSet.get("emailId");
			String regionName = dataSet.get("regionName") + fc.utobj().generateRandomNumber();
			AdminUsersManageRegionalUsersAddRegionalUserPageTest reguser = new AdminUsersManageRegionalUsersAddRegionalUserPageTest();
			reguser.addRegionalUserZC(driver, userName, regionName, config, emailId);

			fc.home_page().logout(driver);

			/*
			 * driver.get(config.get("buildUrl")); LoginPage npobj = new
			 * LoginPage(driver); fc.utobj().sendKeys(driver, npobj.userid,
			 * userName); fc.utobj().sendKeys(driver, npobj.password,
			 * "T0n1ght1"); fc.utobj().clickElement(driver, npobj.login);
			 */

			fc.loginpage().loginWithParameter(driver, userName, "T0n1ght1");

			CRMCampaignCenterCampaignsPage pobj = new CRMCampaignCenterCampaignsPage(driver);
			fc.utobj().printTestStep("Navigate To CRM > Campaign Center");
			fc.crm().crm_common().CRMCampaignCenterLnk(driver);
			fc.utobj().clickElement(driver, pobj.dashBoradLnk);
			fc.utobj().clickElement(driver, pobj.templatesLink);
			fc.utobj().clickElement(driver, pobj.create);
			fc.utobj().clickElement(driver, pobj.createtemplatelist);
			fc.utobj().clickElement(driver, pobj.choosetemplate);
			
			fc.utobj().clickElement(driver,"//*[@id='LayoutSlider']/li[6]/div/div/div[2]/div[1]/p/a");
			String templateName = fc.utobj().generateTestData(dataSet.get("templateName"));
			String emailSubject = fc.utobj().generateTestData(dataSet.get("emailSubject"));
			fc.utobj().sendKeys(driver, pobj.templateName, templateName);
			fc.utobj().sendKeys(driver, pobj.mailSubject, emailSubject);
			Select dropdown = new Select(fc.utobj().getElementByID(driver, "Accessibility"));
			dropdown.selectByValue("Public");
			fc.utobj().clickElement(driver, pobj.TemplateInfodoneBtn);
			fc.utobj().clickElement(driver, pobj.leafelementcenterimg);
			fc.utobj().clickElement(driver, pobj.uploadbutton);

			String fileName = fc.utobj().getFilePathFromTestData("pictureFile.jpg");
			fc.utobj().sendKeys(driver, pobj.uploadImage, fileName);
			fc.utobj().clickElement(driver, pobj.applyimage);
			JavascriptExecutor js = (JavascriptExecutor) driver;
			js.executeScript("arguments[0].innerHTML = arguments[1]", pobj.paragraph1,
					"This is a Lamborghini Aventador");

			fc.utobj().clickElement(driver, pobj.leafelementleftimg);
			fc.utobj().clickElement(driver, pobj.uploadbutton);
			fc.utobj().sendKeys(driver, pobj.uploadImage, fileName);
			fc.utobj().clickElement(driver, pobj.applyimage);

			fc.utobj().clickElement(driver, pobj.leafelementrightimg);
			fc.utobj().clickElement(driver, pobj.uploadbutton);
			fc.utobj().sendKeys(driver, pobj.uploadImage, fileName);
			fc.utobj().clickElement(driver, pobj.applyimage);
			js.executeScript("arguments[0].innerHTML = arguments[1]", pobj.paragraph2, "Now Available in India");

			fc.utobj().clickElement(driver, pobj.savetemplate);
			fc.utobj().clickElement(driver, pobj.dashBoradLnk);
			fc.utobj().clickElement(driver, pobj.templatesLink);
			if (isLocationTemplate)
				fc.utobj().clickElement(driver, pobj.locationtemplatesLink);
			fc.utobj().clickElement(driver, pobj.showFilter);
			fc.utobj().sendKeys(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='searchTemplate']"),
					templateName);
			fc.utobj().clickElement(driver, pobj.applyFilterAtGroup);
			fc.utobj().clickElement(driver, pobj.hideFilter);
			boolean foundforself = fc.utobj().isElementPresent(driver, ".//a[contains(text(),'"+templateName+"')]");
			//boolean foundforself = fc.utobj().assertPageSource(driver, templateName);

			fc.home_page().logout(driver);
			fc.loginpage().loginWithParameter(driver, config.get("userName"), config.get("password"));

			// Checking for other corp user

			fc.utobj().printTestStep("checking for other corp user");
			boolean foundwithcorpuser = CheckWithCorpUser(driver, config, dataSet, templateName, isLocationTemplate);
			fc.utobj().printTestStep("checking for divisional user");
			boolean foundwithdivuser = CheckWithDivUser(driver, config, dataSet, templateName, isLocationTemplate);
			fc.utobj().printTestStep("checking for regional user");
			String region = dataSet.get("regionName") + fc.utobj().generateRandomNumber();
			boolean foundwithreguser = CheckWithRegUser(driver, config, dataSet, region, templateName,
					isLocationTemplate);
			fc.utobj().printTestStep("checking for same region regional user");
			boolean foundwithSameRegionreguser = CheckWithRegUserWithRegionNameExist(driver, config, dataSet,
					templateName, regionName, isLocationTemplate);
			fc.utobj().printTestStep("checking for franchisee user");
			boolean foundwithfrauser = CheckWithFranUser(driver, config, dataSet, templateName, isLocationTemplate);
			fc.utobj().printTestStep("checking for Same Region franchisee user");
			boolean foundwithSameRegionfrauser = CheckWithFranUserWithRegionNameExist(driver, config, dataSet,
					templateName, regionName, isLocationTemplate);
			if (!foundwithcorpuser || foundwithdivuser || foundwithreguser || foundwithfrauser || !foundforself
					|| !foundwithSameRegionreguser || !foundwithSameRegionfrauser) {
				fc.utobj().throwsException("Template checked with All user but not found with someone");
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "crm", "crmRavi", "crmCampaignTest"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Creation Of Template in Campaign At CRM > Campaign Center > Template with Private accessibility to reg user", testCaseId = "TC_Regional_Template_Accessibility_Private")
	private void createCampaignTemplatAccessbilitRegionalUserPrivate()
			throws IOException, Exception, ParserConfigurationException, SAXException {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			String userName = dataSet.get("userName") + fc.utobj().generateRandomNumber();
			String emailId = dataSet.get("emailId");
			String regionName = dataSet.get("regionName") + fc.utobj().generateRandomNumber();
			AdminUsersManageRegionalUsersAddRegionalUserPageTest reguser = new AdminUsersManageRegionalUsersAddRegionalUserPageTest();
			reguser.addRegionalUserZC(driver, userName, regionName, config, emailId);

			fc.home_page().logout(driver);

			/*
			 * driver.get(config.get("buildUrl")); LoginPage npobj = new
			 * LoginPage(driver); fc.utobj().sendKeys(driver, npobj.userid,
			 * userName); fc.utobj().sendKeys(driver, npobj.password,
			 * "T0n1ght1"); fc.utobj().clickElement(driver, npobj.login);
			 */

			fc.loginpage().loginWithParameter(driver, userName, "T0n1ght1");

			CRMCampaignCenterCampaignsPage pobj = new CRMCampaignCenterCampaignsPage(driver);
			fc.utobj().printTestStep("Navigate To CRM > Campaign Center");
			fc.crm().crm_common().CRMCampaignCenterLnk(driver);
			fc.utobj().clickElement(driver, pobj.dashBoradLnk);
			fc.utobj().clickElement(driver, pobj.templatesLink);
			fc.utobj().clickElement(driver, pobj.create);
			fc.utobj().clickElement(driver, pobj.createtemplatelist);
			fc.utobj().clickElement(driver, pobj.choosetemplate);
			
			//Click on choose template
			fc.utobj().clickElement(driver,pobj.clickonchoosetemplate);
			
			
			String templateName = fc.utobj().generateTestData(dataSet.get("templateName"));
			String emailSubject = fc.utobj().generateTestData(dataSet.get("emailSubject"));
			fc.utobj().sendKeys(driver, pobj.templateName, templateName);
			fc.utobj().sendKeys(driver, pobj.mailSubject, emailSubject);
			Select dropdown = new Select(fc.utobj().getElementByID(driver, "Accessibility"));
			dropdown.selectByValue("Private");
			fc.utobj().clickElement(driver, pobj.TemplateInfodoneBtn);
			fc.utobj().clickElement(driver, pobj.leafelementcenterimg);
			fc.utobj().clickElement(driver, pobj.uploadbutton);

			String fileName = fc.utobj().getFilePathFromTestData("pictureFile.jpg");
			fc.utobj().sendKeys(driver, pobj.uploadImage, fileName);
			fc.utobj().clickElement(driver, pobj.applyimage);
			JavascriptExecutor js = (JavascriptExecutor) driver;
			js.executeScript("arguments[0].innerHTML = arguments[1]", pobj.paragraph1,
					"This is a Lamborghini Aventador");

			fc.utobj().clickElement(driver, pobj.leafelementleftimg);
			fc.utobj().clickElement(driver, pobj.uploadbutton);
			fc.utobj().sendKeys(driver, pobj.uploadImage, fileName);
			fc.utobj().clickElement(driver, pobj.applyimage);

			fc.utobj().clickElement(driver, pobj.leafelementrightimg);
			fc.utobj().clickElement(driver, pobj.uploadbutton);
			fc.utobj().sendKeys(driver, pobj.uploadImage, fileName);
			fc.utobj().clickElement(driver, pobj.applyimage);

			js.executeScript("arguments[0].innerHTML = arguments[1]", pobj.paragraph2, "Now Available in India");

			fc.utobj().clickElement(driver, pobj.savetemplate);
			fc.utobj().clickElement(driver, pobj.dashBoradLnk);
			fc.utobj().clickElement(driver, pobj.templatesLink);
			boolean isLocationTemplate = false;
			if (isLocationTemplate)
				fc.utobj().clickElement(driver, pobj.locationtemplatesLink);
			fc.utobj().clickElement(driver, pobj.showFilter);
			fc.utobj().sendKeys(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='searchTemplate']"),
					templateName);
			fc.utobj().clickElement(driver, pobj.applyFilterAtGroup);
			fc.utobj().clickElement(driver, pobj.hideFilter);
			boolean foundforself = fc.utobj().isElementPresent(driver, ".//a[contains(text(),'"+templateName+"')]");
			//boolean foundforself = fc.utobj().assertPageSource(driver, templateName);

			fc.home_page().logout(driver);

			fc.loginpage().loginWithParameter(driver, config.get("userName"), config.get("password"));

			// Checking for other corp user
			//
			// fc.utobj().printTestStep(testCaseId, "checking for other corp
			// user");
			// boolean foundwithcorpuser = CheckWithCorpUser(driver, config,
			// dataSet, templateName, isLocationTemplate);
			// fc.utobj().printTestStep(testCaseId, "checking for divisional
			// user");
			// boolean foundwithdivuser = CheckWithDivUser(driver, config,
			// dataSet, templateName, isLocationTemplate);
			// fc.utobj().printTestStep(testCaseId, "checking for regional
			// user");
			// boolean foundwithreguser = CheckWithRegUser(driver, config,
			// dataSet, templateName, isLocationTemplate);
			// fc.utobj().printTestStep(testCaseId, "checking for same region
			// regional user");
			// boolean foundwithSameRegionreguser =
			// CheckWithRegUserWithRegionNameExist(driver, config, dataSet,
			// templateName, regionName, isLocationTemplate);
			// fc.utobj().printTestStep(testCaseId, "checking for franchisee
			// user");
			// boolean foundwithfrauser = CheckWithFranUser(driver, config,
			// dataSet, templateName, isLocationTemplate);
			// fc.utobj().printTestStep(testCaseId, "checking for Same Region
			// franchisee user");
			boolean foundwithSameRegionfrauser = CheckWithFranUserWithRegionNameExist(driver, config, dataSet,
					templateName, regionName, isLocationTemplate);
			// if (!foundwithcorpuser || foundwithdivuser || foundwithreguser ||
			// foundwithfrauser || !foundforself
			// || !foundwithSameRegionreguser || foundwithSameRegionfrauser)
			// fc.utobj().throwsException("Template checked with All user but
			// not found with someone");
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "crm", "crmNew", "crmCampaignTest"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2017-06-23", testCaseDescription = "Verify The Creation Of Template in Campaign At CRM > Campaign Center > Template with Global accessibility reg user", testCaseId = "TC_Regional_Template_Accessibility_Global")
	private void createCampaignTemplatAccessbilitRegionalUserGlobal()
			throws IOException, Exception, ParserConfigurationException, SAXException {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			String userName = dataSet.get("userName") + fc.utobj().generateRandomNumber();
			String emailId = dataSet.get("emailId");
			String regionName = dataSet.get("regionName") + fc.utobj().generateRandomNumber();
			AdminUsersManageRegionalUsersAddRegionalUserPageTest reguser = new AdminUsersManageRegionalUsersAddRegionalUserPageTest();
			reguser.addRegionalUserZC(driver, userName, regionName, config, emailId);

			fc.home_page().logout(driver);

			/*
			 * driver.get(config.get("buildUrl")); LoginPage npobj = new
			 * LoginPage(driver); fc.utobj().sendKeys(driver, npobj.userid,
			 * userName); fc.utobj().sendKeys(driver, npobj.password,
			 * "T0n1ght1"); fc.utobj().clickElement(driver, npobj.login);
			 */

			fc.loginpage().loginWithParameter(driver, userName, "T0n1ght1");

			CRMCampaignCenterCampaignsPage pobj = new CRMCampaignCenterCampaignsPage(driver);
			fc.utobj().printTestStep("Navigate To CRM > Campaign Center");
			fc.crm().crm_common().CRMCampaignCenterLnk(driver);
			fc.utobj().clickElement(driver, pobj.dashBoradLnk);
			fc.utobj().clickElement(driver, pobj.templatesLink);
			fc.utobj().clickElement(driver, pobj.create);
			fc.utobj().clickElement(driver, pobj.createtemplatelist);
			fc.utobj().clickElement(driver, pobj.choosetemplate);
			//Click on choose template
			fc.utobj().clickElement(driver,pobj.clickonchoosetemplate);
			
			
			String templateName = fc.utobj().generateTestData(dataSet.get("templateName"));
			String emailSubject = fc.utobj().generateTestData(dataSet.get("emailSubject"));
			fc.utobj().sendKeys(driver, pobj.templateName, templateName);
			fc.utobj().sendKeys(driver, pobj.mailSubject, emailSubject);
			Select dropdown = new Select(fc.utobj().getElementByID(driver, "Accessibility"));
			dropdown.selectByValue("Public");
			fc.utobj().clickElement(driver, pobj.TemplateInfodoneBtn);
			fc.utobj().clickElement(driver, pobj.leafelementcenterimg);
			fc.utobj().clickElement(driver, pobj.uploadbutton);

			String fileName = fc.utobj().getFilePathFromTestData("pictureFile.jpg");
			fc.utobj().sendKeys(driver, pobj.uploadImage, fileName);
			fc.utobj().clickElement(driver, pobj.applyimage);
			JavascriptExecutor js = (JavascriptExecutor) driver;
			js.executeScript("arguments[0].innerHTML = arguments[1]", pobj.paragraph1,
					"This is a Lamborghini Aventador");

			fc.utobj().clickElement(driver, pobj.leafelementleftimg);
			fc.utobj().clickElement(driver, pobj.uploadbutton);
			fc.utobj().sendKeys(driver, pobj.uploadImage, fileName);
			fc.utobj().clickElement(driver, pobj.applyimage);

			fc.utobj().clickElement(driver, pobj.leafelementrightimg);
			fc.utobj().clickElement(driver, pobj.uploadbutton);
			fc.utobj().sendKeys(driver, pobj.uploadImage, fileName);
			fc.utobj().clickElement(driver, pobj.applyimage);

			js.executeScript("arguments[0].innerHTML = arguments[1]", pobj.paragraph2, "Now Available in India");

			fc.utobj().clickElement(driver, pobj.savetemplate);
			fc.utobj().clickElement(driver, pobj.dashBoradLnk);
			fc.utobj().clickElement(driver, pobj.templatesLink);
			boolean isLocationTemplate = false;
			if (isLocationTemplate)
				fc.utobj().clickElement(driver, pobj.locationtemplatesLink);
			fc.utobj().clickElement(driver, pobj.showFilter);
			fc.utobj().sendKeys(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='searchTemplate']"),
					templateName);
			fc.utobj().clickElement(driver, pobj.applyFilterAtGroup);
			fc.utobj().clickElement(driver, pobj.hideFilter);
			boolean foundforself = fc.utobj().isElementPresent(driver, ".//a[contains(text(),'"+templateName+"')]");
			//boolean foundforself = fc.utobj().assertPageSource(driver, templateName);

			fc.home_page().logout(driver);
			fc.loginpage().loginWithParameter(driver, config.get("userName"), config.get("password"));

			// Checking for other corp user

			fc.utobj().printTestStep("checking for other corp user");
			boolean foundwithcorpuser = CheckWithCorpUser(driver, config, dataSet, templateName, isLocationTemplate);
			fc.utobj().printTestStep("checking for divisional user");
			boolean foundwithdivuser = CheckWithDivUser(driver, config, dataSet, templateName, isLocationTemplate);
			fc.utobj().printTestStep("checking for regional user");
			String region = dataSet.get("regionName") + fc.utobj().generateRandomNumber();
			boolean foundwithreguser = CheckWithRegUser(driver, config, dataSet, region, templateName,
					isLocationTemplate);
			fc.utobj().printTestStep("checking for same region regional user");
			boolean foundwithSameRegionreguser = CheckWithRegUserWithRegionNameExist(driver, config, dataSet,
					templateName, regionName, isLocationTemplate);
			fc.utobj().printTestStep("checking for franchisee user");
			boolean foundwithfrauser = CheckWithFranUser(driver, config, dataSet, templateName, isLocationTemplate);
			fc.utobj().printTestStep("checking for Same Region franchisee user");
			boolean foundwithSameRegionfrauser = CheckWithFranUserWithRegionNameExist(driver, config, dataSet,
					templateName, regionName, isLocationTemplate);
			if (!foundwithcorpuser || foundwithdivuser || foundwithreguser || foundwithfrauser || !foundforself
					|| !foundwithSameRegionreguser || !foundwithSameRegionfrauser) {
				fc.utobj().throwsException("Template checked with All user but not found with someone");				
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "crm", "crmNewsdgvasdrfb"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Creation Of Template in Campaign At CRM > Campaign Center > Template with Private accessibility for div user", testCaseId = "TC_Divisional_Template_Accessibility_Private")
	private void createCampaignTemplatAccessbilitDivisionalUserPrivate()
			throws IOException, Exception, ParserConfigurationException, SAXException {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			String userName = fc.utobj().generateTestData(dataSet.get("userName"));
			String emailId = dataSet.get("emailId");
			String divisionName = fc.utobj().generateTestData(dataSet.get("divisionName"));
			String regionName = dataSet.get("regionName") + fc.utobj().generateRandomNumber();
			AdminUsersManageDivisionalUsersAddDivisionalUsersPageTest divuser = new AdminUsersManageDivisionalUsersAddDivisionalUsersPageTest();
			divuser.addDivisionalUser(driver, userName, divisionName, emailId);

			fc.home_page().logout(driver);

			/*
			 * driver.get(config.get("buildUrl")); LoginPage npobj = new
			 * LoginPage(driver); fc.utobj().sendKeys(driver, npobj.userid,
			 * userName); fc.utobj().sendKeys(driver, npobj.password,
			 * "T0n1ght1"); fc.utobj().clickElement(driver, npobj.login);
			 */

			fc.loginpage().loginWithParameter(driver, userName, "fran1234");

			CRMCampaignCenterCampaignsPage pobj = new CRMCampaignCenterCampaignsPage(driver);
			fc.utobj().printTestStep("Navigate To CRM > Campaign Center");
			fc.crm().crm_common().CRMCampaignCenterLnk(driver);
			fc.utobj().clickElement(driver, pobj.dashBoradLnk);
			fc.utobj().clickElement(driver, pobj.templatesLink);
			fc.utobj().clickElement(driver, pobj.create);
			fc.utobj().clickElement(driver, pobj.createtemplatelist);
			fc.utobj().clickElement(driver, pobj.choosetemplate);
			fc.utobj().clickElement(driver,pobj.customize);
			String templateName = fc.utobj().generateTestData(dataSet.get("templateName"));
			String emailSubject = fc.utobj().generateTestData(dataSet.get("emailSubject"));
			fc.utobj().sendKeys(driver, pobj.templateName, templateName);
			fc.utobj().sendKeys(driver, pobj.mailSubject, emailSubject);
			Select dropdown = new Select(fc.utobj().getElementByID(driver, "Accessibility"));
			dropdown.selectByValue("Private");
			fc.utobj().clickElement(driver, pobj.TemplateInfodoneBtn);
			fc.utobj().clickElement(driver, pobj.leafelementcenterimg);
			fc.utobj().clickElement(driver, pobj.uploadbutton);

			String fileName = fc.utobj().getFilePathFromTestData("pictureFile.jpg");
			fc.utobj().sendKeys(driver, pobj.uploadImage, fileName);
			fc.utobj().clickElement(driver, pobj.applyimage);
			JavascriptExecutor js = (JavascriptExecutor) driver;
			js.executeScript("arguments[0].innerHTML = arguments[1]", pobj.paragraph1,
					"This is a Lamborghini Aventador");

			fc.utobj().clickElement(driver, pobj.leafelementleftimg);
			fc.utobj().clickElement(driver, pobj.uploadbutton);
			fc.utobj().sendKeys(driver, pobj.uploadImage, fileName);
			fc.utobj().clickElement(driver, pobj.applyimage);

			fc.utobj().clickElement(driver, pobj.leafelementrightimg);
			fc.utobj().clickElement(driver, pobj.uploadbutton);
			fc.utobj().sendKeys(driver, pobj.uploadImage, fileName);
			fc.utobj().clickElement(driver, pobj.applyimage);

			js.executeScript("arguments[0].innerHTML = arguments[1]", pobj.paragraph2, "Now Available in India");

			fc.utobj().clickElement(driver, pobj.savetemplate);
			fc.utobj().clickElement(driver, pobj.dashBoradLnk);
			fc.utobj().clickElement(driver, pobj.templatesLink);
			boolean isLocationTemplate = false;
			if (isLocationTemplate)
				fc.utobj().clickElement(driver, pobj.locationtemplatesLink);
			fc.utobj().clickElement(driver, pobj.showFilter);
			fc.utobj().sendKeys(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='searchTemplate']"),
					templateName);
			fc.utobj().clickElement(driver, pobj.applyFilterAtGroup);
			fc.utobj().clickElement(driver, pobj.hideFilter);
			boolean foundforself = fc.utobj().isElementPresent(driver, ".//a[contains(text(),'"+templateName+"')]");
			//boolean foundforself = fc.utobj().assertPageSource(driver, templateName);

			fc.home_page().logout(driver);
			fc.loginpage().loginWithParameter(driver, config.get("userName"), config.get("password"));

			// Checking for other corp user

			
			fc.utobj().printTestStep("checking for franchisee user");
			boolean foundwithfrauser = CheckWithFranUser(driver, config, dataSet, templateName, isLocationTemplate);
			
			
			fc.utobj().printTestStep("checking for other corp user");
			boolean foundwithcorpuser = CheckWithCorpUser(driver, config, dataSet, templateName, isLocationTemplate);
			fc.utobj().printTestStep("checking for divisional user");
			boolean foundwithdivuser = CheckWithDivUser(driver, config, dataSet, templateName, isLocationTemplate);
			fc.utobj().printTestStep("checking for regional user");
			String region = dataSet.get("regionName") + fc.utobj().generateRandomNumber();
			boolean foundwithreguser = CheckWithRegUser(driver, config, dataSet, region, templateName,
					isLocationTemplate);
			/*fc.utobj().printTestStep("checking for franchisee user");
			boolean foundwithfrauser = CheckWithFranUser(driver, config, dataSet, templateName, isLocationTemplate);*/
			fc.utobj().printTestStep("checking for Same Division Divisional user");
			boolean foundwithSameDivisionaluser = CheckWithDivUserWithDivisionExist(driver, config, dataSet,
					templateName, divisionName, isLocationTemplate);
			if (!foundwithcorpuser || foundwithdivuser || foundwithreguser || foundwithfrauser || !foundforself
					|| !foundwithSameDivisionaluser)
			{
				fc.utobj().throwsException("Template checked with All user but not found with someone");
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

 			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "crm", "crmRavi"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2017-06-14", testCaseDescription = "Verify The Creation Of Template in Campaign At CRM > Campaign Center > Template with Modify/Delete accessibility ", testCaseId = "TC_Modify_Delete_Accessibility")
	private void modifyCampaignTemplatAccessbilitDivisionalUserPrivate()
			throws IOException, Exception, ParserConfigurationException, SAXException {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			boolean updateforcorpuser = false;
			boolean updateforfranuser = false;
			boolean updatefordivuser = false;
			boolean updateforreguser = false;
			driver = fc.loginpage().login(driver);
			CRMCampaignCenterCampaignsPage pobj = new CRMCampaignCenterCampaignsPage(driver);
			fc.utobj().printTestStep("Navigate To CRM > Campaign Center");
			fc.crm().crm_common().CRMCampaignCenterLnk(driver);
			fc.utobj().clickElement(driver, pobj.dashBoradLnk);
			fc.utobj().clickElement(driver, pobj.templatesLink);
			fc.utobj().clickElement(driver, pobj.create);
			fc.utobj().clickElement(driver, pobj.createtemplatelist);
			fc.utobj().clickElement(driver, pobj.choosetemplate);
			fc.utobj().clickElement(driver, pobj.choosetemplate_custom);
			String templateName = fc.utobj().generateTestData(dataSet.get("templateName"));
			String emailSubject = fc.utobj().generateTestData(dataSet.get("emailSubject"));
			fc.utobj().sendKeys(driver, pobj.templateName, templateName);
			fc.utobj().sendKeys(driver, pobj.mailSubject, emailSubject);
			Select dropdown = new Select(fc.utobj().getElementByID(driver, "Accessibility"));
			dropdown.selectByValue("Global");
			fc.utobj().clickElement(driver, pobj.TemplateInfodoneBtn);
			fc.utobj().clickElement(driver, pobj.leafelementcenterimg);
			fc.utobj().clickElement(driver, pobj.uploadbutton);

			String fileName = fc.utobj().getFilePathFromTestData("pictureFile.jpg");
			fc.utobj().sendKeys(driver, pobj.uploadImage, fileName);
			fc.utobj().clickElement(driver, pobj.applyimage);
			JavascriptExecutor js = (JavascriptExecutor) driver;
			js.executeScript("arguments[0].innerHTML = arguments[1]", pobj.paragraph1,
					"This is a Lamborghini Aventador");

			fc.utobj().clickElement(driver, pobj.leafelementleftimg);
			fc.utobj().clickElement(driver, pobj.uploadbutton);
			fc.utobj().sendKeys(driver, pobj.uploadImage, fileName);
			fc.utobj().clickElement(driver, pobj.applyimage);

			fc.utobj().clickElement(driver, pobj.leafelementrightimg);
			fc.utobj().clickElement(driver, pobj.uploadbutton);
			fc.utobj().sendKeys(driver, pobj.uploadImage, fileName);
			fc.utobj().clickElement(driver, pobj.applyimage);

			js.executeScript("arguments[0].innerHTML = arguments[1]", pobj.paragraph2, "Now Available in India");
			fc.utobj().clickElement(driver, pobj.savetemplate);
			fc.utobj().clickElement(driver, pobj.dashBoradLnk);
			fc.utobj().clickElement(driver, pobj.templatesLink);
			fc.utobj().clickElement(driver, pobj.showFilter);
			fc.utobj().sendKeys(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='searchTemplate']"),
					templateName);
			fc.utobj().clickElement(driver, pobj.applyFilterAtGroup);
			fc.utobj().clickElement(driver, pobj.hideFilter);

			boolean foundforself = fc.utobj().assertPageSource(driver, templateName);
			// check for corporate user
			AdminUsersManageCorporateUsersAddCorporateUserPageTest addcorpuser = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			String corpuseris = "TestCorpUserTemp" + fc.utobj().generateRandomNumber();
			CorporateUser corpUser = new CorporateUser();
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corpUser.setEmail("crmautomation@staffex.com");
			corpUser.setUserName(corpuseris);
			corpUser.setPassword("T0n1ght1");
			corpUser = addcorpuser.createDefaultUser(driver, corpUser);
			driver.get(config.get("buildUrl"));
			LoginPage npobj = new LoginPage(driver);
			
			fc.utobj().sendKeys(driver, npobj.userid, corpuseris);
			fc.utobj().sendKeys(driver, npobj.password, "T0n1ght1");
			fc.utobj().clickElement(driver, npobj.login);
			fc.crm().crm_common().CRMCampaignCenterLnk(driver);
			fc.utobj().clickElement(driver, pobj.dashBoradLnk);
			fc.utobj().clickElement(driver, pobj.templatesLink);
			fc.utobj().clickElement(driver, pobj.showFilter);
			fc.utobj().sendKeys(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='searchTemplate']"),
					templateName);
			fc.utobj().clickElement(driver, pobj.applyFilterAtGroup);
			fc.utobj().clickElement(driver, pobj.hideFilter);
			boolean foundforcorpuser = fc.utobj().assertPageSource(driver, templateName);
			if (foundforcorpuser) {
				try {
					fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
							".//a[contains(text(),'" + templateName + "')]/parent::*/following-sibling::td[5]/div"));
					fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//a[contains(text(),'"
							+ templateName + "')]/parent::*/following-sibling::td[5]//ul/li[1]"));
					js.executeScript("arguments[0].innerHTML = arguments[1]", pobj.paragraph1,
							"This is a Lamborghini Aventador Brand new Modified by Corp user");
					fc.utobj().clickElement(driver, pobj.savetemplate);
					updateforcorpuser = true;
				} catch (Exception e) {
					updateforcorpuser = false;
				}
			}
			// Checking with franchiseee user
			driver.get(config.get("buildUrl"));
			fc.utobj().sendKeys(driver, npobj.userid, "adm");
			fc.utobj().sendKeys(driver, npobj.password, "t0n1ght");
			fc.utobj().clickElement(driver, npobj.login);
			String regionName = dataSet.get("regionName") + fc.utobj().generateRandomNumber();
			String storeType = dataSet.get("storeType");
			String franchiseName = dataSet.get("franchiseName");
			String centerName = dataSet.get("centerName");
			String openingDate = fc.utobj().generateCurrentDatewithformat("yyyy-MM-dd");
			String password = dataSet.get("password");
			String confirmPassword = dataSet.get("confirmPassword");
			String loginId = dataSet.get("loginIdforfran") + fc.utobj().generateRandomNumber();
			String ownerFirstName = dataSet.get("ownerFirstName") + fc.utobj().generateRandomNumber6Digit();
			String ownerLastName = dataSet.get("ownerLastName") + fc.utobj().generateRandomNumber6Digit();
			String emailId = dataSet.get("emailId");
			AdminFranchiseLocationAddFranchiseLocationPageTest fran = new AdminFranchiseLocationAddFranchiseLocationPageTest();
			fran.addFranchiseLocationWithUserZC(driver, regionName, storeType, franchiseName, centerName, openingDate,
					password, confirmPassword, loginId, ownerFirstName, ownerLastName, config, emailId);
			driver.get(config.get("buildUrl"));
			fc.utobj().sendKeys(driver, npobj.userid, loginId);
			fc.utobj().sendKeys(driver, npobj.password, password);
			fc.utobj().clickElement(driver, npobj.login);
			fc.crm().crm_common().CRMCampaignCenterLnk(driver);
			fc.utobj().clickElement(driver, pobj.dashBoradLnk);
			fc.utobj().clickElement(driver, pobj.templatesLink);
			fc.utobj().clickElement(driver, pobj.showFilter);
			fc.utobj().sendKeys(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='searchTemplate']"),
					templateName);
			fc.utobj().clickElement(driver, pobj.applyFilterAtGroup);
			fc.utobj().clickElement(driver, pobj.hideFilter);
			boolean foundforFranuser = fc.utobj().assertPageSource(driver, templateName);
			if (foundforFranuser) {
				try {
					fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
							".//a[contains(text(),'" + templateName + "')]/parent::*/following-sibling::td[5]/div"));
					fc.utobj().clickElement(driver,
							fc.utobj().getElementByXpath(driver, ".//a[contains(text(),'" + templateName
									+ "')]/parent::*/following-sibling::td[5]//ul/li[contains(text(),'Modify')]"));
					js.executeScript("arguments[0].innerHTML = arguments[1]", pobj.paragraph1,
							"This is a Lamborghini Aventador Brand new Modified by Fran user");
					fc.utobj().clickElement(driver, pobj.savetemplate);
					updateforfranuser = true;
				} catch (Exception e) {
					updateforfranuser = false;
				}
			}
			// Checking with Regional user
			driver.get(config.get("buildUrl"));
			fc.utobj().sendKeys(driver, npobj.userid, "adm");
			fc.utobj().sendKeys(driver, npobj.password, "t0n1ght");
			fc.utobj().clickElement(driver, npobj.login);
			String userName = dataSet.get("userName") + fc.utobj().generateRandomNumber();
			regionName = dataSet.get("regionName") + fc.utobj().generateRandomNumber();
			AdminUsersManageRegionalUsersAddRegionalUserPageTest reguser = new AdminUsersManageRegionalUsersAddRegionalUserPageTest();
			reguser.addRegionalUserZC(driver, userName, regionName, config, emailId);
			driver.get(config.get("buildUrl"));
			fc.utobj().sendKeys(driver, npobj.userid, userName);
			fc.utobj().sendKeys(driver, npobj.password, "T0n1ght1");
			fc.utobj().clickElement(driver, npobj.login);
			fc.crm().crm_common().CRMCampaignCenterLnk(driver);
			fc.utobj().clickElement(driver, pobj.dashBoradLnk);
			fc.utobj().clickElement(driver, pobj.templatesLink);
			fc.utobj().clickElement(driver, pobj.showFilter);
			fc.utobj().sendKeys(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='searchTemplate']"),
					templateName);
			fc.utobj().clickElement(driver, pobj.applyFilterAtGroup);
			fc.utobj().clickElement(driver, pobj.hideFilter);
			boolean foundforRegUser = fc.utobj().assertPageSource(driver, templateName);
			if (foundforRegUser) {
				try {
					fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
							".//a[contains(text(),'" + templateName + "')]/parent::*/following-sibling::td[5]/div"));
					fc.utobj().clickElement(driver,
							fc.utobj().getElementByXpath(driver, ".//a[contains(text(),'" + templateName
									+ "')]/parent::*/following-sibling::td[5]//ul/li[contains(text(),'Modify')]"));
					js.executeScript("arguments[0].innerHTML = arguments[1]", pobj.paragraph1,
							"This is a Lamborghini Aventador Brand new Modified by Reg user");
					fc.utobj().clickElement(driver, pobj.savetemplate);
					updateforreguser = true;
				} catch (Exception e) {
					updateforreguser = false;
				}
			}
			// Checking with Divisional user
			driver.get(config.get("buildUrl"));
			fc.utobj().sendKeys(driver, npobj.userid, "adm");
			fc.utobj().sendKeys(driver, npobj.password, "t0n1ght");
			fc.utobj().clickElement(driver, npobj.login);
			userName = dataSet.get("userName") + fc.utobj().generateRandomNumber();
			emailId = dataSet.get("emailId");
			String divisionName = dataSet.get("divisionName") + fc.utobj().generateRandomNumber();
			regionName = dataSet.get("regionName") + fc.utobj().generateRandomNumber();
			AdminUsersManageDivisionalUsersAddDivisionalUsersPageTest divuser = new AdminUsersManageDivisionalUsersAddDivisionalUsersPageTest();
			divuser.addDivisionalUser(driver, userName, divisionName, emailId);
			driver.get(config.get("buildUrl"));
			fc.utobj().sendKeys(driver, npobj.userid, userName);
			fc.utobj().sendKeys(driver, npobj.password, "fran1234");
			fc.utobj().clickElement(driver, npobj.login);
			fc.crm().crm_common().CRMCampaignCenterLnk(driver);
			fc.utobj().clickElement(driver, pobj.dashBoradLnk);
			fc.utobj().clickElement(driver, pobj.templatesLink);
			fc.utobj().clickElement(driver, pobj.showFilter);
			fc.utobj().sendKeys(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='searchTemplate']"),
					templateName);
			fc.utobj().clickElement(driver, pobj.applyFilterAtGroup);
			fc.utobj().clickElement(driver, pobj.hideFilter);
			boolean foundforDivUser = fc.utobj().assertPageSource(driver, templateName);
			if (foundforDivUser) {
				try {
					fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
							".//a[contains(text(),'" + templateName + "')]/parent::*/following-sibling::td[5]/div"));
					fc.utobj().clickElement(driver,
							fc.utobj().getElementByXpath(driver, ".//a[contains(text(),'" + templateName
									+ "')]/parent::*/following-sibling::td[5]//ul/li[contains(text(),'Modify')]"));
					js.executeScript("arguments[0].innerHTML = arguments[1]", pobj.paragraph1,
							"This is a Lamborghini Aventador Brand new Modified by Reg user");
					fc.utobj().clickElement(driver, pobj.savetemplate);
					updatefordivuser = true;
				} catch (Exception e) {
					updatefordivuser = false;
				}
			}

			if (!updateforcorpuser || updatefordivuser || updateforfranuser || updateforreguser)
				fc.utobj()
						.throwsException("Template checked with All user but modified by unauthorised user pls check");
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "crm"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2017-06-14", testCaseDescription = "Verify The Creation Of Template in Campaign At CRM > Campaign Center > Template created on loaction not searchable at other location ", testCaseId = "TC_Search_Template_Onther_Location")
	private void createCampaignTemplateNotSearchableToOtherLocation()
			throws IOException, Exception, ParserConfigurationException, SAXException {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			String userName = dataSet.get("userName") + fc.utobj().generateRandomNumber();
			String emailId = dataSet.get("emailId");
			String divisionName = dataSet.get("divisionName") + fc.utobj().generateRandomNumber();
			String regionName = dataSet.get("regionName") + fc.utobj().generateRandomNumber();

			fc.utobj().printTestStep("Add Region");
			AdminAreaRegionAddAreaRegionPageTest regionPage = new AdminAreaRegionAddAreaRegionPageTest();
			regionPage.addAreaRegion(driver, regionName);

			AdminUsersManageDivisionalUsersAddDivisionalUsersPageTest divuser = new AdminUsersManageDivisionalUsersAddDivisionalUsersPageTest();
			divuser.addDivisionalUser(driver, userName, divisionName, emailId);

			fc.home_page().logout(driver);
			fc.loginpage().loginWithParameter(driver, userName, "fran1234");
			CRMCampaignCenterCampaignsPage pobj = new CRMCampaignCenterCampaignsPage(driver);
			fc.utobj().printTestStep("Navigate To CRM > Campaign Center");
			fc.crm().crm_common().CRMCampaignCenterLnk(driver);
			fc.utobj().clickElement(driver, pobj.dashBoradLnk);
			fc.utobj().clickElement(driver, pobj.templatesLink);
			fc.utobj().clickElement(driver, pobj.create);
			fc.utobj().clickElement(driver, pobj.createtemplatelist);
			fc.utobj().clickElement(driver, pobj.choosetemplate);
			fc.utobj().clickElement(driver, pobj.customize);
			
			String templateName = fc.utobj().generateTestData(dataSet.get("templateName"));
			String emailSubject = fc.utobj().generateTestData(dataSet.get("emailSubject"));
			fc.utobj().sendKeys(driver, pobj.templateName, templateName);
			fc.utobj().sendKeys(driver, pobj.mailSubject, emailSubject);
			Select dropdown = new Select(fc.utobj().getElementByID(driver, "Accessibility"));
			dropdown.selectByValue("Private");
			fc.utobj().clickElement(driver, pobj.TemplateInfodoneBtn);
			fc.utobj().clickElement(driver, pobj.leafelementcenterimg);
			fc.utobj().clickElement(driver, pobj.uploadbutton);

			String fileName = fc.utobj().getFilePathFromTestData("pictureFile.jpg");
			fc.utobj().sendKeys(driver, pobj.uploadImage, fileName);
			fc.utobj().clickElement(driver, pobj.applyimage);
			JavascriptExecutor js = (JavascriptExecutor) driver;
			js.executeScript("arguments[0].innerHTML = arguments[1]", pobj.paragraph1,
					"This is a Lamborghini Aventador");

			fc.utobj().clickElement(driver, pobj.leafelementleftimg);
			fc.utobj().clickElement(driver, pobj.uploadbutton);
			fc.utobj().sendKeys(driver, pobj.uploadImage, fileName);
			fc.utobj().clickElement(driver, pobj.applyimage);

			fc.utobj().clickElement(driver, pobj.leafelementrightimg);
			fc.utobj().clickElement(driver, pobj.uploadbutton);
			fc.utobj().sendKeys(driver, pobj.uploadImage, fileName);
			fc.utobj().clickElement(driver, pobj.applyimage);

			js.executeScript("arguments[0].innerHTML = arguments[1]", pobj.paragraph2, "Now Available in India");
			boolean isLocationTemplate = false;
			fc.utobj().clickElement(driver, pobj.savetemplate);
			fc.utobj().clickElement(driver, pobj.dashBoradLnk);
			fc.utobj().clickElement(driver, pobj.templatesLink);
			fc.utobj().clickElement(driver, pobj.showFilter);
			fc.utobj().sendKeys(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='searchTemplate']"),
					templateName);
			fc.utobj().clickElement(driver, pobj.applyFilterAtGroup);
			fc.utobj().clickElement(driver, pobj.hideFilter);
			boolean foundforself = fc.utobj().isElementPresent(driver, ".//a[contains(text(),'"+templateName+"')]");
			//boolean foundforself = fc.utobj().assertPageSource(driver, templateName);

			fc.home_page().logout(driver);
			fc.loginpage().loginWithParameter(driver, config.get("userName"), config.get("password"));
			fc.utobj().printTestStep("checking for other corp user");

			boolean foundwithcorpuser = CheckWithCorpUser(driver, config, dataSet, templateName, isLocationTemplate);
			fc.utobj().printTestStep("checking for divisional user");

			boolean foundwithdivuser = CheckWithDivUser(driver, config, dataSet, templateName, isLocationTemplate);
			fc.utobj().printTestStep("checking for regional user");

			String region = dataSet.get("regionName") + fc.utobj().generateRandomNumber();
			boolean foundwithreguser = CheckWithRegUser(driver, config, dataSet, region, templateName,
					isLocationTemplate);

			fc.utobj().printTestStep("checking for same region regional user");
			boolean foundwithSameRegionreguser = CheckWithRegUserWithRegionNameExist(driver, config, dataSet,
					templateName, regionName, isLocationTemplate);

			fc.utobj().printTestStep("checking for franchisee user");
			boolean foundwithfrauser = CheckWithFranUser(driver, config, dataSet, templateName, isLocationTemplate);
			fc.utobj().printTestStep("checking for Same Region franchisee user");

			boolean foundwithSameRegionfrauser = CheckWithFranUserWithRegionNameExist(driver, config, dataSet,
					templateName, regionName, isLocationTemplate);

			// Check With Ravi

			if (!foundwithcorpuser || foundwithdivuser || foundwithreguser || foundwithfrauser || !foundforself
					|| foundwithSameRegionreguser || foundwithSameRegionfrauser)
			{
				fc.utobj().throwsException("Template checked with All user but not found with someone");
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "crm", "crmRavi", "crmCampaignTest" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Creation Of Promotional Type Campaign At CRM > Campaign Center > Campaigns", testCaseId = "TC_05_Create_Campaign_Promotional_Type")
	private void createCampaignPromotionalType()
			throws IOException, Exception, ParserConfigurationException, SAXException {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			CRMCampaignCenterCampaignsPage pobj = new CRMCampaignCenterCampaignsPage(driver);

			CRMCampaignCenterPageTest campaignCenterPage = new CRMCampaignCenterPageTest();

			fc.utobj().printTestStep("Navigating to Admin > CRM > Configure Campaign Email Category");
			fc.utobj().printTestStep("Add Category");

			AdminCRMConfigureCampaignEmailCategoryPageTest campaignMailCategoryPage = new AdminCRMConfigureCampaignEmailCategoryPageTest();
			String categoryName = fc.utobj().generateTestData(dataSet.get("categoryName"));
			campaignMailCategoryPage.addCategory(driver, categoryName);

			fc.utobj().printTestStep("Navigate To CRM > Campaign Center");
			fc.crm().crm_common().CRMCampaignCenterLnk(driver);

			fc.utobj().printTestStep("Create Campaign At Campaign Center > Campaigns");

			fc.utobj().clickElement(driver, pobj.dashBoradLnk);
			fc.utobj().clickElement(driver, pobj.campaignLnk);

			fc.utobj().clickElement(driver, pobj.createCampaign);

			// WebElement frame
			// =fc.CommonMethods().switch_frameClass_newLayoutcboxIframe(driver);
			fc.commonMethods().switch_frameClass_newLayoutcboxIframe(driver);

			String campaignName = fc.utobj().generateTestData(dataSet.get("campaignName"));
			String accessibility = dataSet.get("accessibility");
			fc.utobj().sendKeys(driver, pobj.campaignTitle, campaignName);

			String camapignDescription = fc.utobj().generateTestData(dataSet.get("camapignDescription"));
			fc.utobj().clickElement(driver, pobj.campaignDescriptionLnk);
			fc.utobj().sendKeys(driver, pobj.campaignDescription, camapignDescription);
			fc.utobj().clickElement(driver, pobj.hideDescriptionLink);

			// fc.utobj().clickElement(driver, pobj.emailCampaign);
			fc.utobj().selectDropDown(driver, pobj.campaignAccessibility, accessibility);
			fc.utobj().selectDropDown(driver, pobj.selectCategory, categoryName);
			fc.utobj().clickElement(driver, pobj.promotionalTypeCampaign);
			fc.utobj().clickElement(driver, pobj.startBtn);
			fc.utobj().switchFrameToDefault(driver);

			// Code Your Template
			String templateName = fc.utobj().generateTestData(dataSet.get("templateName"));
			String emailSubject = fc.utobj().generateTestData(dataSet.get("emailSubject"));
			String plainTextEditor = fc.utobj().generateTestData(dataSet.get("plainTextEditor"));

			fc.utobj().printTestStep("Code Your Own Template");

			fc.utobj().clickElement(driver, pobj.codeYourOwn);
			fc.utobj().sendKeys(driver, pobj.templateName, templateName);
			fc.utobj().sendKeys(driver, pobj.mailSubject, emailSubject);

			fc.utobj().clickElement(driver, pobj.plainTextEmailType);
			fc.utobj().sendKeys(driver, pobj.textAreaPlainText, plainTextEditor);

			fc.utobj().clickElement(driver, pobj.saveAndContinue);
			fc.utobj().clickElement(driver, pobj.associateLater);
			fc.utobj().clickElement(driver, pobj.finalizeAndLaunchCampaignBtn);

			fc.utobj().printTestStep("Verify The Create Campaign");
			fc.utobj().clickElement(driver, pobj.viewAllButton);

			fc.utobj().printTestStep("Verify Campaign At All Tab");
			campaignCenterPage.searchCampaignByFilter(driver, campaignName);

			boolean isCampaignNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + campaignName + "')]");
			if (isCampaignNamePresent == false) {
				fc.utobj().throwsException("was not able to verify Campaign At Campaign Center All Tab");
			}

			/*
			 * fc.utobj().clickElement(driver, pobj.promotionalCampaignTab);
			 * campaignCenterPage.searchCampaignByFilter(driver, campaignName);
			 * 
			 * boolean isCampaignNamePresentAtPromotionalCampaignTab =
			 * fc.utobj().verifyCase(driver, ".//a[contains(text () ,'" +
			 * campaignName + "')]"); if
			 * (isCampaignNamePresentAtPromotionalCampaignTab == false) {
			 * fc.utobj() .throwsException(
			 * "was not able to verify Campaign At Campaign Center Promotional Campaign Tab"
			 * ); }
			 */

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "crm", "crmRavi", "crmCampaignTest" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Creation Of Status Driven Type Campaign At CRM > Campaign Center > Campaigns", testCaseId = "TC_06_Create_Campaign_Status_Driven_Type")
	private void createCampaignStatusDrivenType()
			throws IOException, Exception, ParserConfigurationException, SAXException {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			CRMCampaignCenterCampaignsPage pobj = new CRMCampaignCenterCampaignsPage(driver);
			CRMCampaignCenterPageTest campaignCenterPage = new CRMCampaignCenterPageTest();
			FSCampaignCenterPage campaignCenterObj = new FSCampaignCenterPage(driver);

			fc.utobj().printTestStep("Navigating to Admin > CRM > Configure Campaign Email Category");
			fc.utobj().printTestStep("Add Category");

			AdminCRMConfigureCampaignEmailCategoryPageTest campaignMailCategoryPage = new AdminCRMConfigureCampaignEmailCategoryPageTest();
			String categoryName = fc.utobj().generateTestData(dataSet.get("categoryName"));
			campaignMailCategoryPage.addCategory(driver, categoryName);

			fc.utobj().printTestStep("Navigate To CRM > Campaign Center");
			fc.crm().crm_common().CRMCampaignCenterLnk(driver);

			fc.utobj().printTestStep("Create Campaign At Campaign Center > Campaigns");

			fc.utobj().clickElement(driver, pobj.dashBoradLnk);
			fc.utobj().clickElement(driver, pobj.campaignLnk);

			fc.utobj().clickElement(driver, pobj.createCampaign);
			// WebElement frame
			// =fc.CommonMethods().switch_frameClass_newLayoutcboxIframe(driver);
			fc.commonMethods().switch_frameClass_newLayoutcboxIframe(driver);

			String campaignName = fc.utobj().generateTestData(dataSet.get("campaignName"));
			String accessibility = dataSet.get("accessibility");
			fc.utobj().sendKeys(driver, pobj.campaignTitle, campaignName);

			String camapignDescription = fc.utobj().generateTestData(dataSet.get("camapignDescription"));
			fc.utobj().clickElement(driver, pobj.campaignDescriptionLnk);
			fc.utobj().sendKeys(driver, pobj.campaignDescription, camapignDescription);
			fc.utobj().clickElement(driver, pobj.hideDescriptionLink);

			// fc.utobj().clickElement(driver, pobj.emailCampaign);
			fc.utobj().selectDropDown(driver, pobj.campaignAccessibility, accessibility);
			fc.utobj().selectDropDown(driver, pobj.selectCategory, categoryName);
			fc.utobj().clickElement(driver, pobj.statusDrivenTypeCampaign);
			fc.utobj().clickElement(driver, pobj.startBtn);
			fc.utobj().switchFrameToDefault(driver);

			// Code Your Template
			String templateName = fc.utobj().generateTestData(dataSet.get("templateName"));
			String emailSubject = fc.utobj().generateTestData(dataSet.get("emailSubject"));
			String plainTextEditor = fc.utobj().generateTestData(dataSet.get("plainTextEditor"));

			fc.utobj().printTestStep("Code Your Own Template");

			fc.utobj().clickElement(driver, pobj.codeYourOwn);
			fc.utobj().sendKeys(driver, pobj.templateName, templateName);
			fc.utobj().sendKeys(driver, pobj.mailSubject, emailSubject);

			fc.utobj().clickElement(driver, pobj.plainTextEmailType);
			fc.utobj().sendKeys(driver, pobj.textAreaPlainText, plainTextEditor);

			fc.utobj().clickElement(driver, pobj.saveAndContinue);
			fc.utobj().clickElement(driver, pobj.associateLater);
			fc.utobj().clickElement(driver, pobj.finalizeAndLaunchCampaignBtn);

			fc.utobj().printTestStep("Verify The Create Campaign");
			// fc.utobj().clickElement(driver, pobj.viewAllButton);
			// fc.utobj().clickElement(driver, campaignCenterObj.veiwCampaings);

			fc.utobj().clickElement(driver, campaignCenterObj.veiwCampaings);

			fc.utobj().printTestStep("Verify Campaign At All Tab");
			campaignCenterPage.searchCampaignByFilter(driver, campaignName);

			boolean isCampaignNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + campaignName + "')]");
			if (isCampaignNamePresent == false) {
				fc.utobj().throwsException("was not able to verify Campaign At Campaign Center All Tab");
			}

			/*
			 * fc.utobj().clickElement(driver, pobj.statusDrivenCampaignTab);
			 * campaignCenterPage.searchCampaignByFilter(driver, campaignName);
			 * 
			 * boolean isCampaignNamePresentAtPromotionalCampaignTab =
			 * fc.utobj().verifyCase(driver, ".//a[contains(text () ,'" +
			 * campaignName + "')]"); if
			 * (isCampaignNamePresentAtPromotionalCampaignTab == false) {
			 * fc.utobj().throwsException(
			 * "was not able to verify Campaign At Campaign Center Status Driven Campaign Tab"
			 * ); }
			 */

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "crm", "crmRavi", "crmCampaignTest" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Modify Of Promotional Type Campaign At CRM > Campaign Center > Campaigns", testCaseId = "TC_07_Modify_Campaign_At_All_Tab")
	private void modifyCampaignAtAllTab() throws IOException, Exception, ParserConfigurationException, SAXException {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			CRMCampaignCenterCampaignsPage pobj = new CRMCampaignCenterCampaignsPage(driver);
			CRMCampaignCenterPageTest campaignCenterPage = new CRMCampaignCenterPageTest();
			CRMGroupsPageTest groupPage = new CRMGroupsPageTest();

			fc.utobj().printTestStep("Navigate To CRM > Campaign Center");
			fc.crm().crm_common().CRMModule(driver);

			fc.utobj().printTestStep("Create Campaign At Campaign Center > Campaigns");
			String campaignName = fc.utobj().generateTestData(dataSet.get("campaignName"));
			String accessibility = dataSet.get("accessibility");
			String campaignType = dataSet.get("campaignType");
			String templateName = fc.utobj().generateTestData(dataSet.get("templateName"));
			String emailSubject = fc.utobj().generateTestData(dataSet.get("emailSubject"));
			String plainTextEditor = fc.utobj().generateTestData(dataSet.get("plainTextEditor"));
			campaignCenterPage.createCampaignGeneric(driver, config, campaignName, accessibility, campaignType,
					templateName, emailSubject, plainTextEditor);

			fc.utobj().printTestStep("Modify Campaign");
			fc.utobj().clickElement(driver, pobj.viewAllButton);

			campaignCenterPage.searchCampaignByFilter(driver, campaignName);
			groupPage.actionImgOption(driver, campaignName, "Modify");
			fc.utobj().clickElement(driver, pobj.modifyBtn);
			// WebElement frame
			// =fc.CommonMethods().switch_frameClass_newLayoutcboxIframe(driver);
			fc.commonMethods().switch_frameClass_newLayoutcboxIframe(driver);

			campaignName = fc.utobj().generateTestData(dataSet.get("campaignName"));
			fc.utobj().sendKeys(driver, pobj.campaignTitle, campaignName);
			fc.utobj().clickElement(driver, pobj.saveCampaignBtn);
			fc.utobj().clickElement(driver, pobj.closeBtn);
			fc.utobj().switchFrameToDefault(driver);
			fc.utobj().clickElement(driver, pobj.saveBtnAfterModify);

			fc.utobj().clickElement(driver, pobj.viewAllButton);
			campaignCenterPage.searchCampaignByFilter(driver, campaignName);

			boolean isCampaignNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + campaignName + "')]");
			if (isCampaignNamePresent == false) {
				fc.utobj().throwsException(
						"was not able to verify Campaign At Campaign Center All Tab After Modification");
			}

			/*
			 * fc.utobj().clickElement(driver, pobj.promotionalCampaignTab);
			 * campaignCenterPage.searchCampaignByFilter(driver, campaignName);
			 * 
			 * boolean isCampaignNamePresentAtPromotionalCampaignTab =
			 * fc.utobj().verifyCase(driver, ".//a[contains(text () ,'" +
			 * campaignName + "')]"); if
			 * (isCampaignNamePresentAtPromotionalCampaignTab == false) {
			 * fc.utobj().throwsException(
			 * "was not able to verify Campaign At Campaign Center Promotional Campaign Tab after Modification"
			 * ); }
			 */

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "crm", "TC_08_Copy_And_Customize_Campaign_At_All_Tab", "crmCampaignTest" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Copy And Customize Of Promotional Type Campaign At CRM > Campaign Center > Campaigns", testCaseId = "TC_08_Copy_And_Customize_Campaign_At_All_Tab")
	private void copyAndCustomizeCampaignAtAllTab()
			throws IOException, Exception, ParserConfigurationException, SAXException {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			CRMCampaignCenterCampaignsPage pobj = new CRMCampaignCenterCampaignsPage(driver);
			CRMCampaignCenterPageTest campaignCenterPage = new CRMCampaignCenterPageTest();
			CRMGroupsPageTest groupPage = new CRMGroupsPageTest();

			fc.utobj().printTestStep("Navigate To CRM > Campaign Center");
			fc.crm().crm_common().CRMModule(driver);

			fc.utobj().printTestStep("Create Campaign At Campaign Center > Campaigns");
			String campaignName = fc.utobj().generateTestData(dataSet.get("campaignName"));
			String accessibility = dataSet.get("accessibility");
			String campaignType = dataSet.get("campaignType");
			String templateName = fc.utobj().generateTestData(dataSet.get("templateName"));
			String emailSubject = fc.utobj().generateTestData(dataSet.get("emailSubject"));
			String plainTextEditor = fc.utobj().generateTestData(dataSet.get("plainTextEditor"));
			campaignCenterPage.createCampaignGeneric(driver, config, campaignName, accessibility, campaignType,
					templateName, emailSubject, plainTextEditor);

			fc.utobj().printTestStep("Copy And Customize Campaign");
			fc.utobj().clickElement(driver, pobj.viewAllButton);

			campaignCenterPage.searchCampaignByFilter(driver, campaignName);
			groupPage.actionImgOption(driver, campaignName, "Copy and Customize");
			fc.commonMethods().switch_frameClass_newLayoutcboxIframe(driver);
			String campaignTitleForCopy = fc.utobj().generateTestData(dataSet.get("campaignTitleForCopy"));
			fc.utobj().sendKeys(driver, pobj.campaignTitle, campaignTitleForCopy);
			String accessibilityForCopy = dataSet.get("accessibilityForCopy");
			fc.utobj().selectDropDown(driver, pobj.campaignAccessibility, accessibilityForCopy);
			fc.utobj().clickElement(driver, pobj.startProcessButton);
			fc.utobj().switchFrameToDefault(driver);
			fc.utobj().clickElement(driver, pobj.finalizeAndLaunchCampaignCopy);

			fc.utobj().printTestStep("Verify The Copy And Customize Campaign");
			fc.utobj().clickElement(driver, pobj.viewAllButton);

			fc.utobj().printTestStep("Verify Copy And Customize Campaign At All Tab");
			campaignCenterPage.searchCampaignByPromotionalFilter(driver, campaignName);

			boolean isCampaignNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + campaignName + "')]");
			if (isCampaignNamePresent == false) {
				fc.utobj().throwsException(
						"was not able to verify Copy And Customize Campaign At Campaign Center All Tab");
			}

			// Becuase now Tab are removed search on the basis of Drop down
			/*
			 * fc.utobj().clickElement(driver, pobj.promotionalCampaignTab);
			 * campaignCenterPage.searchCampaignByFilter(driver, campaignName);
			 * 
			 * boolean isCampaignNamePresentAtPromotionalCampaignTab =
			 * fc.utobj().verifyCase(driver, ".//a[contains(text () ,'" +
			 * campaignName + "')]"); if
			 * (isCampaignNamePresentAtPromotionalCampaignTab == false) {
			 * fc.utobj().throwsException(
			 * "was not able to verify Copy And Customize Campaign At Campaign Center Status Driven Campaign Tab"
			 * ); }
			 */

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "crm", "crmcheck21", "crmCampaignTest","errorpages1"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2017-08-09", testCaseDescription = "Verify The Stop Campaign For Lead At All Campaign Tab", testCaseId = "TC_09_Stop_Campaign_For_Lead")
	private void stopCampaignForLeadAtAllTab()
			throws IOException, Exception, ParserConfigurationException, SAXException {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			CRMLeadsPage pobj = new CRMLeadsPage(driver);
			CRMLeadsPageTest leadsPage = new CRMLeadsPageTest();
			CRMCampaignCenterPageTest campaignCenterPage = new CRMCampaignCenterPageTest();
			CRMGroupsPageTest groupPage = new CRMGroupsPageTest();
			CRMCampaignCenterCampaignsPage pobj1 = new CRMCampaignCenterCampaignsPage(driver);

			fc.utobj().printTestStep("Navigate To CRM >Campaign Center");
			fc.crm().crm_common().CRMModule(driver);

			fc.utobj().printTestStep("Create Campaign");

			String campaignName = fc.utobj().generateTestData(dataSet.get("campaignName"));
			String accessibility = dataSet.get("accessibility");
			String campaignType = dataSet.get("campaignType");
			String templateName = fc.utobj().generateTestData(dataSet.get("templateName"));
			String emailSubject = fc.utobj().generateTestData(dataSet.get("emailSubject"));
			String plainTextEditor = fc.utobj().generateTestData(dataSet.get("plainTextEditor"));
			campaignCenterPage.createCampaignGeneric(driver, config, campaignName, accessibility, campaignType,
					templateName, emailSubject, plainTextEditor);

			fc.utobj().printTestStep("Navigate To Admin Users Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			String emailId = "crmautomation@staffex.com";
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUser = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();

			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporateUser.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			String firstName = fc.utobj().generateTestData(dataSet.get("firstName"));
			String lastName = fc.utobj().generateTestData(dataSet.get("lastName"));
			String company = fc.utobj().generateTestData(dataSet.get("company"));
			String assignTo = "Corporate";
			String city = fc.utobj().generateTestData(dataSet.get("city"));
			String address = fc.utobj().generateTestData(dataSet.get("address"));
			String suffix = fc.utobj().generateTestData(dataSet.get("suffix"));
			String jobTitle = fc.utobj().generateTestData(dataSet.get("jobTitle"));
			String comment = fc.utobj().generateTestData(dataSet.get("comment"));
			String franchiseId = "";
			String franchiseUser = "";
			String userNameReg = "";
			String regionName = "";
			fc.utobj().printTestStep("Navigate To CRM > Leads > Leads Summary");
			fc.utobj().printTestStep("Add Lead");
			leadsPage.addLead(driver, dataSet, firstName, lastName, company, assignTo, city, address, suffix, jobTitle,
					comment, corpUser.getuserFullName(), regionName, userNameReg, franchiseId, franchiseUser, config);

			/*
			 * fc.utobj().clickElement(driver, pobj.leadsLink);
			 * leadsPage.searchLeadByOwner(driver, corpUser.getuserFullName(),
			 * "Open Leads");
			 * fc.utobj().clickElement(driver,fc.utobj().getElementByXpath(
			 * driver, ".//a[contains(text () ,'" + firstName + " " + lastName +
			 * "')]")));
			 */

			fc.utobj().printTestStep("Associate With Campaign");

			fc.utobj().clickElement(driver, pobj.AssociateWithRegularCampaignButton);
			fc.utobj().clickElement(driver, pobj.filterContainer);
			fc.utobj().sendKeys(driver, pobj.searchCampaign, campaignName);
			fc.utobj().clickElement(driver, pobj.applyFilter);

			fc.utobj().clickElement(driver, pobj.sendCampaignBtn);
			fc.utobj().clickElement(driver, pobj.confirmBtn);

			/*
			 * fc.utobj().clickElement(driver, pobj.leadsLink);
			 * leadsPage.searchLeadByOwner(driver, corpUser.getuserFullName(),
			 * "Open Leads");
			 * fc.utobj().clickElement(driver,fc.utobj().getElementByXpath(
			 * driver, ".//a[.='" + firstName + " " + lastName + "']")));
			 */

			/*fc.utobj().printTestStep("Verify Associate With Campaign");
			boolean isCampaignNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//td[contains(text () ,'" + campaignType
					+ "')]/following-sibling::td[contains(text () ,'" + campaignName + "')]");
			if (isCampaignNamePresent == false) {
				fc.utobj().throwsException("was not able to verify Campign Name");
			}*/

			
			fc.utobj().printTestStep("Navigate To CRM >Campaign Center");
			fc.utobj().printTestStep("Stop Campaign");

			fc.utobj().clickElement(driver, pobj1.campaignCenterLinks);
			fc.utobj().clickElement(driver, pobj1.viewAllButton);

			campaignCenterPage.searchCampaignByFilter(driver, campaignName);
			groupPage.actionImgOption(driver, campaignName, "Stop Campaign");
			fc.utobj().acceptAlertBox(driver);

			fc.utobj().printTestStep("Navigate To CRM > Leads > Lead Summary");
			/*
			 * fc.utobj().clickElement(driver, pobj.leadsLink);
			 * fc.utobj().clickElement(driver,
			 * fc.utobj().getElementByXpath(driver, ".//a[contains(text () ,'" +
			 * firstName + " " + lastName + "')]")));
			 */
			leadsPage.leadSearchFromSearchpage(driver, firstName, lastName);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//a[contains(text () ,'" + firstName + " " + lastName + "')]"));

			fc.utobj().printTestStep("Verify The Campaign Associated With Lead After Stop Campaign");
			boolean isCampaignPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[.//td[.='Promotional Campaign : ']]/following-sibling::td[.!='" + campaignName + "']");
			if (isCampaignPresent == false) {
				fc.utobj().throwsException("was not able to verify Campaign name gone from Promotional Campaign Field");
			}

			/*
			 * isCampaignPresent = fc.utobj().verifyCase(driver,
			 * ".//td[contains(text () ,'Older Promotional Campaign')]/following-sibling::td[.='"
			 * + campaignName + "']"); if (isCampaignPresent == false) {
			 * fc.utobj().throwsException(
			 * "was not able to verify Campaign name comming at Older Campaign Field"
			 * ); }
			 */

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "crm", "crmRavi", "crmCampaignTest"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2017-07-21", testCaseDescription = "Verify The Archive Campaign Of Status Driven Type Campaign At CRM > Campaign Center > Campaigns", testCaseId = "TC_10_Archive_Campaign_At_All_Tab")
	private void archiveCampaignAtAllTab() throws IOException, Exception, ParserConfigurationException, SAXException {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			CRMCampaignCenterCampaignsPage pobj = new CRMCampaignCenterCampaignsPage(driver);
			CRMCampaignCenterPageTest campaignCenterPage = new CRMCampaignCenterPageTest();
			CRMGroupsPageTest groupPage = new CRMGroupsPageTest();
			CRMCampaignCenterCampaignsPage pobj1 = new CRMCampaignCenterCampaignsPage(driver);

			fc.utobj().printTestStep("Navigate To CRM > Campaign Center");
			fc.crm().crm_common().CRMModule(driver);

			fc.utobj().printTestStep("Create Campaign At Campaign Center > Campaigns");
			String campaignName = fc.utobj().generateTestData(dataSet.get("campaignName"));
			String accessibility = dataSet.get("accessibility");
			String campaignType = dataSet.get("campaignType");
			String templateName = fc.utobj().generateTestData(dataSet.get("templateName"));
			String emailSubject = fc.utobj().generateTestData(dataSet.get("emailSubject"));
			String plainTextEditor = fc.utobj().generateTestData(dataSet.get("plainTextEditor"));
			campaignCenterPage.createCampaignGeneric(driver, config, campaignName, accessibility, campaignType,
					templateName, emailSubject, plainTextEditor);

			fc.utobj().clickElement(driver, pobj.viewAllButton);

			fc.utobj().printTestStep("Archive Campaign");

			campaignCenterPage.searchCampaignByFilter(driver, campaignName);
			groupPage.actionImgOption(driver, campaignName, "Archive");
			fc.utobj().acceptAlertBox(driver);
			fc.utobj().printTestStep("Verify Archive Campaign At All Tab");

			campaignCenterPage.searchCampaignByFilter(driver, campaignName);

			/*boolean isCampaignNamePresent = fc.utobj().assertPageSource(driver, campaignName);

			if (isCampaignNamePresent == true) {
				fc.utobj().throwsException("Not able to verify Campaign At Campaign Center All Tab After Archive");
			}*/
			boolean isCampaignNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () ,'No Record Found')]");
			if (isCampaignNamePresent == false) {
				fc.utobj()
						.throwsException("Not able to verify Campaign At Campaign Center All Tab After Archive");
			}

			// fc.utobj().clickElement(driver, pobj.statusDrivenCampaignTab);
			/*campaignCenterPage.searchCampaignByFilter(driver, campaignName);

			boolean isCampaignNamePresentAtStatusDrivenTab = fc.utobj().assertPageSource(driver, campaignName);

			if (isCampaignNamePresentAtStatusDrivenTab == true) {
				fc.utobj().throwsException(
						"was not able to verify Campaign At Campaign Center Status Driven Tab after Archive");
			}
*/
			fc.utobj().printTestStep("Verify The Archived Campaign At Archived Tab");
			fc.utobj().clickElement(driver, pobj1.archivedTab);
			campaignCenterPage.searchCampaignByFilter(driver, campaignName);

			boolean isCampaignNamePresentAtArchiveTab = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + campaignName + "')]");
			if (isCampaignNamePresentAtArchiveTab == false) {
				fc.utobj().throwsException(
						"was not able to verify Campaign At Campaign Center Status Driven Campaign Tab after Archive");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "crm", "crmemail"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2017-06-20", testCaseDescription = "Verify The Test Campaign Of Status Driven Type Campaign At CRM > Campaign Center > Campaigns", testCaseId = "TC_12_Test_Campaign_At_All_Tab")
	private void testCampaignAtAllTab() throws IOException, Exception, ParserConfigurationException, SAXException {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			CRMCampaignCenterCampaignsPage pobj = new CRMCampaignCenterCampaignsPage(driver);
			CRMCampaignCenterPageTest campaignCenterPage = new CRMCampaignCenterPageTest();
			CRMGroupsPageTest groupPage = new CRMGroupsPageTest();

			fc.utobj().printTestStep("Navigate To CRM > Campaign Center");
			fc.crm().crm_common().CRMModule(driver);

			fc.utobj().printTestStep("Create Campaign At Campaign Center > Campaigns");
			String campaignName = fc.utobj().generateTestData(dataSet.get("campaignName"));
			String accessibility = "Public to all Users";
			String campaignType = "Promotional";
			accessibility = dataSet.get("accessibility");
			campaignType = dataSet.get("campaignType");
			String templateName = fc.utobj().generateTestData(dataSet.get("templateName"));
			String emailSubject = fc.utobj().generateTestData(dataSet.get("emailSubject"));
			String plainTextEditor = fc.utobj().generateTestData(dataSet.get("plainTextEditor"));
			campaignCenterPage.createCampaignGeneric(driver, config, campaignName, accessibility, campaignType,
					templateName, emailSubject, plainTextEditor);
			fc.utobj().clickElement(driver, pobj.viewAllButton);

			fc.utobj().printTestStep("Test Campaign");

			campaignCenterPage.searchCampaignByFilter(driver, campaignName);
			groupPage.actionImgOption(driver, campaignName, "Test Campaign");

			String emailId = dataSet.get("emailId");
			fc.commonMethods().switch_frameClass_newLayoutcboxIframe(driver);
			fc.utobj().sleep(2000);
			fc.utobj().sendKeys(driver, pobj.testEmailField, emailId);
			fc.utobj().clickElement(driver, pobj.testButton);
			fc.utobj().clickElement(driver, pobj.closeBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Verify The Campaign Details In Email");

			Map<String, String> mailData = new HashMap<String, String>();

			mailData = fc.utobj().readMailBox(emailSubject, plainTextEditor, emailId, "sdg@1a@Hfs");

			if (mailData.size() == 0 || !mailData.get("mailBody").contains(plainTextEditor)) {
				fc.utobj().throwsException("was not able to verify Test Campaign Mail");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "crm", "crmRavi", "crmCampaignTest" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Delete Campaign Of Status Driven Type Campaign At CRM > Campaign Center > Campaigns", testCaseId = "TC_11_Delete_Campaign_At_All_Tab")
	private void deleteCampaignAtAllTab() throws IOException, Exception, ParserConfigurationException, SAXException {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			CRMCampaignCenterCampaignsPage pobj = new CRMCampaignCenterCampaignsPage(driver);
			CRMCampaignCenterPageTest campaignCenterPage = new CRMCampaignCenterPageTest();
			CRMGroupsPageTest groupPage = new CRMGroupsPageTest();

			fc.utobj().printTestStep("Navigate To CRM > Campaign Center");
			fc.crm().crm_common().CRMModule(driver);

			fc.utobj().printTestStep("Create Campaign At Campaign Center > Campaigns");
			String campaignName = fc.utobj().generateTestData(dataSet.get("campaignName"));
			String accessibility = dataSet.get("accessibility");
			String campaignType = dataSet.get("campaignType");
			String templateName = fc.utobj().generateTestData(dataSet.get("templateName"));
			String emailSubject = fc.utobj().generateTestData(dataSet.get("emailSubject"));
			String plainTextEditor = fc.utobj().generateTestData(dataSet.get("plainTextEditor"));
			campaignCenterPage.createCampaignGeneric(driver, config, campaignName, accessibility, campaignType,
					templateName, emailSubject, plainTextEditor);

			fc.utobj().clickElement(driver, pobj.viewAllButton);

			fc.utobj().printTestStep("Delete Campaign");

			campaignCenterPage.searchCampaignByFilter(driver, campaignName);
			groupPage.actionImgOption(driver, campaignName, "Delete");
			fc.utobj().acceptAlertBox(driver);

			fc.utobj().printTestStep("Verify Deleted Campaign At All Tab");

			campaignCenterPage.searchCampaignByStatusDrivenFilter(driver, campaignName);

			boolean isCampaignNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () ,'No Record Found')]");
			if (isCampaignNamePresent == false) {
				fc.utobj().throwsException("was not able to verify Campaign At Campaign Center All Tab After Deletion");
			}

			/*
			 * fc.utobj().clickElement(driver, pobj.statusDrivenCampaignTab);
			 * campaignCenterPage.searchCampaignByFilter(driver, campaignName);
			 * 
			 * boolean isCampaignNamePresentAtStatusDrivenTab =
			 * fc.utobj().verifyCase(driver,
			 * ".//td[contains(text () ,'No Record Found')]"); if
			 * (isCampaignNamePresentAtStatusDrivenTab == false) {
			 * fc.utobj().throwsException(
			 * "was not able to verify Campaign At Campaign Center Status Driven Tab after Deleteion"
			 * ); }
			 */

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "crm", "crmRavi", "crmCampaignTest" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Modify Of Promotional Type Campaign At CRM > Campaign Center > Campaigns [Promotional Campaign Tab]", testCaseId = "TC_14_Modify_Campaign_At_Promotional_Tab")
	private void modifyCampaignAtPromotionalTab()
			throws IOException, Exception, ParserConfigurationException, SAXException {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			CRMCampaignCenterCampaignsPage pobj = new CRMCampaignCenterCampaignsPage(driver);
			CRMCampaignCenterPageTest campaignCenterPage = new CRMCampaignCenterPageTest();
			CRMGroupsPageTest groupPage = new CRMGroupsPageTest();

			fc.utobj().printTestStep("Navigate To CRM > Campaign Center");
			fc.crm().crm_common().CRMModule(driver);

			fc.utobj().printTestStep("Create Campaign At Campaign Center > Campaigns");
			String campaignName = fc.utobj().generateTestData(dataSet.get("campaignName"));
			String accessibility = dataSet.get("accessibility");
			String campaignType = dataSet.get("campaignType");
			String templateName = fc.utobj().generateTestData(dataSet.get("templateName"));
			String emailSubject = fc.utobj().generateTestData(dataSet.get("emailSubject"));
			String plainTextEditor = fc.utobj().generateTestData(dataSet.get("plainTextEditor"));
			campaignCenterPage.createCampaignGeneric(driver, config, campaignName, accessibility, campaignType,
					templateName, emailSubject, plainTextEditor);

			fc.utobj().printTestStep("Modify Campaign At Promotional Campaign Tab");
			fc.utobj().clickElement(driver, pobj.viewAllButton);

			// fc.utobj().clickElement(driver, pobj.promotionalCampaignTab);

			campaignCenterPage.searchCampaignByFilter(driver, campaignName);
			groupPage.actionImgOption(driver, campaignName, "Modify");
			fc.utobj().clickElement(driver, pobj.modifyBtn);
			fc.commonMethods().switch_frameClass_newLayoutcboxIframe(driver);

			campaignName = fc.utobj().generateTestData(dataSet.get("campaignName"));
			fc.utobj().sendKeys(driver, pobj.campaignTitle, campaignName);
			fc.utobj().clickElement(driver, pobj.saveCampaignBtn);
			fc.utobj().clickElement(driver, pobj.closeBtn);
			fc.utobj().switchFrameToDefault(driver);
			fc.utobj().clickElement(driver, pobj.saveBtnAfterModify);

			fc.utobj().clickElement(driver, pobj.viewAllButton);

			fc.utobj().printTestStep("Verify The Modify Campaign At All Campaign Tab");

			campaignCenterPage.searchCampaignByPromotionalFilter(driver, campaignName);

			boolean isCampaignNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + campaignName + "')]");
			if (isCampaignNamePresent == false) {
				fc.utobj().throwsException(
						"was not able to verify Campaign At Campaign Center All Tab After Modification");
			}

			/*
			 * fc.utobj().printTestStep(testCaseId,
			 * "Verify The Modify Campaign At Promotional Campaign Tab");
			 * fc.utobj().clickElement(driver, pobj.promotionalCampaignTab);
			 * campaignCenterPage.searchCampaignByFilter(driver, campaignName);
			 * 
			 * boolean isCampaignNamePresentAtPromotionalCampaignTab =
			 * fc.utobj().verifyCase(driver, ".//a[contains(text () ,'" +
			 * campaignName + "')]"); if
			 * (isCampaignNamePresentAtPromotionalCampaignTab == false) {
			 * fc.utobj().throwsException(
			 * "was not able to verify Campaign At Campaign Center Promotional Campaign Tab after Modification"
			 * ); }
			 */

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "crm", "crmRavi"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Copy And Customize Of Promotional Type Campaign At CRM > Campaign Center > Campaigns", testCaseId = "TC_15_Copy_And_Customize_Campaign_At_Promotional_Tab")
	private void copyAndCustomizeCampaignAtPromotionalTab()
			throws IOException, Exception, ParserConfigurationException, SAXException {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			CRMCampaignCenterCampaignsPage pobj = new CRMCampaignCenterCampaignsPage(driver);
			CRMCampaignCenterPageTest campaignCenterPage = new CRMCampaignCenterPageTest();
			CRMGroupsPageTest groupPage = new CRMGroupsPageTest();

			fc.utobj().printTestStep("Navigate To CRM > Campaign Center");
			fc.crm().crm_common().CRMModule(driver);

			fc.utobj().printTestStep("Create Campaign At Campaign Center > Campaigns");
			String campaignName = fc.utobj().generateTestData(dataSet.get("campaignName"));
			String accessibility = dataSet.get("accessibility");
			String campaignType = dataSet.get("campaignType");
			String templateName = fc.utobj().generateTestData(dataSet.get("templateName"));
			String emailSubject = fc.utobj().generateTestData(dataSet.get("emailSubject"));
			String plainTextEditor = fc.utobj().generateTestData(dataSet.get("plainTextEditor"));
			campaignCenterPage.createCampaignGeneric(driver, config, campaignName, accessibility, campaignType,
					templateName, emailSubject, plainTextEditor);

			fc.utobj().printTestStep("Copy And Customize Campaign At Promotional Campaign Tab");
			fc.utobj().clickElement(driver, pobj.viewAllButton);

			// fc.utobj().clickElement(driver, pobj.promotionalCampaignTab);
			campaignCenterPage.searchCampaignByFilter(driver, campaignName);

			groupPage.actionImgOption(driver, campaignName, "Copy and Customize");
			fc.commonMethods().switch_frameClass_newLayoutcboxIframe(driver);
			String campaignTitleForCopy = fc.utobj().generateTestData(dataSet.get("campaignTitleForCopy"));
			fc.utobj().sendKeys(driver, pobj.campaignTitle, campaignTitleForCopy);
			String accessibilityForCopy = dataSet.get("accessibilityForCopy");
			fc.utobj().selectDropDown(driver, pobj.campaignAccessibility, accessibilityForCopy);
			fc.utobj().clickElement(driver, pobj.startProcessButton);
			fc.utobj().switchFrameToDefault(driver);
			fc.utobj().clickElement(driver, pobj.finalizeAndLaunchCampaignCopy);

			fc.utobj().printTestStep("Verify The Copy And Customize Campaign At All Campaign Tab");
			fc.utobj().clickElement(driver, pobj.viewAllButton);

			fc.utobj().printTestStep("Verify Copy And Customize Campaign At All Tab");
			campaignCenterPage.searchCampaignByFilter(driver, campaignName);

			boolean isCampaignNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + campaignName + "')]");
			if (isCampaignNamePresent == false) {
				fc.utobj().throwsException(
						"was not able to verify Copy And Customize Campaign At Campaign Center All Tab");
			}

			fc.utobj().printTestStep("Verify The Copy And Customize Campaign At Promotional Campaign Tab");

			// fc.utobj().clickElement(driver, pobj.promotionalCampaignTab);
			campaignCenterPage.searchCampaignByFilter(driver, campaignName);

			boolean isCampaignNamePresentAtPromotionalCampaignTab = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + campaignName + "')]");
			if (isCampaignNamePresentAtPromotionalCampaignTab == false) {
				fc.utobj().throwsException(
						"was not able to verify copy and customize campaign At Promotion Campaign Tab");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "crm", "crmmodulesx", "crmCampaignTest","errorpages1"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2017-06-14", testCaseDescription = "Verify The Stop Campaign For Contact At Promotional Campaign Tab", testCaseId = "TC_16_Stop_Campaign_For_Contact_At_Promotional_Tab")
	private void stopCampaignForContactPromotionalTab()
			throws IOException, Exception, ParserConfigurationException, SAXException {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			CRMContactsPage pobj = new CRMContactsPage(driver);
			CRMCampaignCenterPageTest campaignCenterPage = new CRMCampaignCenterPageTest();
			CRMContactsPageTest contactPageTest = new CRMContactsPageTest();
			CRMCampaignCenterCampaignsPage pobj1 = new CRMCampaignCenterCampaignsPage(driver);
			CRMGroupsPageTest groupPage = new CRMGroupsPageTest();

			fc.utobj().printTestStep("Navigate To CRM > Campaign Center");
			fc.crm().crm_common().CRMModule(driver);

			fc.utobj().printTestStep("Create Campaign At Campaign Center > Campaigns");

			String campaignName = fc.utobj().generateTestData(dataSet.get("campaignName"));
			String accessibility = dataSet.get("accessibility");
			String campaignType = dataSet.get("campaignType");
			String templateName = fc.utobj().generateTestData(dataSet.get("templateName"));
			String emailSubject = fc.utobj().generateTestData(dataSet.get("emailSubject"));
			String plainTextEditor = fc.utobj().generateTestData(dataSet.get("plainTextEditor"));
			campaignCenterPage.createCampaignGeneric(driver, config, campaignName, accessibility, campaignType,
					templateName, emailSubject, plainTextEditor);

			fc.utobj().printTestStep("Navigate To Admin Users Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			String emailId = "crmautomation@staffex.com";
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUser = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();

			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporateUser.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			fc.utobj().printTestStep("Navigate To Admin > CRM > Contact Type Configuration");
			fc.utobj().printTestStep("Add Contact Type");
			AdminCRMContactTypeConfigurationPageTest contatcTypePage = new AdminCRMContactTypeConfigurationPageTest();
			String contactType = fc.utobj().generateTestData(dataSet.get("contactType"));
			contatcTypePage.addContactType(driver, contactType);

			fc.utobj().printTestStep("Navigate To CRM > Contacts > Contacts Summary Page");
			fc.utobj().printTestStep("Add Contact");
			fc.crm().crm_common().CRMContactsLnk(driver);
			fc.utobj().clickElement(driver, pobj.addNew);
			fc.utobj().clickElement(driver, pobj.contactLnk);

			String firstName = fc.utobj().generateTestData(dataSet.get("firstName"));
			String lastName = fc.utobj().generateTestData(dataSet.get("lastName"));
			String primaryCMethod = dataSet.get("primaryCMethod");
			String assignTo = "Corporate";
			String regionName = "";
			String franchiseId = "";
			String title = dataSet.get("title");
			//String email = "crmautomation@staffex.com";
			String email=fc.utobj().generateTestData("crmautomation")+"@staffex.com";
			contactPageTest.addContactGeneric(driver, contactType, firstName, lastName, primaryCMethod, assignTo,
					corpUser.getuserFullName(), regionName, franchiseId, title, email, config);

			fc.utobj().clickElement(driver, pobj.contactsLinks);
			contactPageTest.contactsFilter(driver, contactType);
			fc.utobj().printTestStep("Associate With Email Campaign");
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//a[contains(text () ,'" + firstName
					+ " " + lastName + "')]/ancestor::tr/td/input[@name='checkb']"));
			fc.utobj().clickElement(driver, pobj.associateWithEmailCampaignBtmBtn);
			fc.utobj().clickElement(driver, pobj.filterContainer);
			fc.utobj().sendKeys(driver, pobj.searchCampaign, campaignName);
			fc.utobj().clickElement(driver, pobj.applyFilter);

			fc.utobj().clickElement(driver, new CRMLeadsPage(driver).sendCampaignBtn);
			fc.utobj().clickElement(driver, pobj.confirmBtn);

			fc.utobj().clickElement(driver, pobj.contactsLinks);
			contactPageTest.contactsFilter(driver, contactType);

			fc.utobj().printTestStep("Verify Contact Associate With Campaign At Contact Primary Info Page");
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//a[contains(text () ,'" + firstName + " " + lastName + "')]"));
			boolean isCampaignNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//td[contains(text () ,'" + campaignType
					+ "')]/following-sibling::td[contains(text () ,'" + campaignName + "')]");
			if (isCampaignNamePresent == false) {
				fc.utobj().throwsException("was not able to verify Campign Name");
			}

			fc.utobj().printTestStep("Navigate To CRM >Campaign Center");
			fc.utobj().printTestStep("Stop Campaign");

			fc.utobj().clickElement(driver, pobj1.campaignCenterLinks);
			fc.utobj().clickElement(driver, pobj1.viewAllButton);

			// fc.utobj().clickElement(driver, pobj1.promotionalCampaignTab);
			campaignCenterPage.searchCampaignByFilter(driver, campaignName);
			groupPage.actionImgOption(driver, campaignName, "Stop Campaign");
			fc.utobj().acceptAlertBox(driver);

			fc.utobj().clickElement(driver, pobj.contactsLinks);
			contactPageTest.contactsFilter(driver, contactType);

			fc.utobj().printTestStep(
					"Verify The Campaign Associate With Contact After Stop The Campaign At Contact Primary Info Page");
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//a[contains(text () ,'" + firstName + " " + lastName + "')]"));

			// boolean isCampaignNamePresent1=fc.utobj().verifyCase(driver,
			// ".//td[contains(text(),'Older Promotional
			// Campaign')]//following-sibling::td[contains(text(),'"+campaignName+"')]");

			boolean isCampaignNamePresent1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text(),'Promotional Campaign') and not (contains(text(),'Older Promotional Campaign'))]//following-sibling::td[contains(text(),'"
							+ campaignName + "')]");

			if (isCampaignNamePresent1 == true) {
				fc.utobj().throwsException("Was not able to verify the Campaign After Stop The Campaign");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "crm", "crmRavi" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Archive Campaign Of Promotional Type Campaign At CRM > Campaign Center > Campaigns [Promotional Campaign Tab] And Also Verify Unarchive The Campaign", testCaseId = "TC_17_Archive_Campaign_At_Promotional_Tab")
	private void archiveCampaignPromotionalTab()
			throws IOException, Exception, ParserConfigurationException, SAXException {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			CRMCampaignCenterCampaignsPage pobj = new CRMCampaignCenterCampaignsPage(driver);
			CRMCampaignCenterPageTest campaignCenterPage = new CRMCampaignCenterPageTest();
			CRMGroupsPageTest groupPage = new CRMGroupsPageTest();
			CRMCampaignCenterCampaignsPage pobj1 = new CRMCampaignCenterCampaignsPage(driver);

			fc.utobj().printTestStep("Navigate To CRM > Campaign Center");
			fc.crm().crm_common().CRMModule(driver);

			fc.utobj().printTestStep("Create Campaign At Campaign Center > Campaigns");
			String campaignName = fc.utobj().generateTestData(dataSet.get("campaignName"));
			String accessibility = dataSet.get("accessibility");
			String campaignType = dataSet.get("campaignType");
			String templateName = fc.utobj().generateTestData(dataSet.get("templateName"));
			String emailSubject = fc.utobj().generateTestData(dataSet.get("emailSubject"));
			String plainTextEditor = fc.utobj().generateTestData(dataSet.get("plainTextEditor"));
			campaignCenterPage.createCampaignGeneric(driver, config, campaignName, accessibility, campaignType,
					templateName, emailSubject, plainTextEditor);

			fc.utobj().clickElement(driver, pobj.viewAllButton);
			fc.utobj().printTestStep("Archive Campaign At Promotional Campaign Tab");

			// fc.utobj().clickElement(driver, pobj.promotionalCampaignTab);
			campaignCenterPage.searchCampaignByFilter(driver, campaignName);
			groupPage.actionImgOption(driver, campaignName, "Archive");
			fc.utobj().acceptAlertBox(driver);

			fc.utobj().printTestStep("Verify Archive Campaign At All Tab");
			campaignCenterPage.searchCampaignByFilter(driver, campaignName);

			boolean isCampaignNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () ,'No Record Found')]");
			if (isCampaignNamePresent == false) {
				fc.utobj().throwsException("was not able to verify Campaign At Campaign Center All Tab After Archive");
			}

			// fc.utobj().clickElement(driver, pobj.promotionalCampaignTab);
			campaignCenterPage.searchCampaignByFilter(driver, campaignName);

			boolean isCampaignNamePresentAtStatusDrivenTab = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () ,'No Record Found')]");
			if (isCampaignNamePresentAtStatusDrivenTab == false) {
				fc.utobj()
						.throwsException("was not able to verify Archive Promotional Type Campaign At Promotional Tab");
			}

			fc.utobj().printTestStep("Verify The Archived Promotional Campaign At Archived Tab");
			fc.utobj().clickElement(driver, pobj1.archivedTab);
			campaignCenterPage.searchCampaignByFilter(driver, campaignName);

			boolean isCampaignNamePresentAtArchiveTab = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + campaignName + "')]");
			if (isCampaignNamePresentAtArchiveTab == false) {
				fc.utobj().throwsException("was not able to verify Archived Promotional Campaign At Archived Tab");
			}

			fc.utobj().printTestStep("Verify The Unarchive Campaign");
			fc.utobj().clickElement(driver, pobj.archivedTab);
			campaignCenterPage.searchCampaignByFilter(driver, campaignName);

			fc.utobj().printTestStep("Unarchive The Campaign");
			groupPage.actionImgOption(driver, campaignName, "Unarchive");

			fc.utobj().printTestStep("Verify Unarchive Campaign At Promotional Campaign Tab");
			// fc.utobj().clickElement(driver, pobj.promotionalCampaignTab);
			fc.utobj().clickElement(driver,"//span[contains(text(),'Corporate Campaigns')]");
			campaignCenterPage.searchCampaignByFilter(driver, campaignName);

			boolean isCampaignNamePresentUnarchived = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + campaignName + "')]");
			if (isCampaignNamePresentUnarchived == false) {
				fc.utobj().throwsException(
						"was not able to verify Unarchived Promotional Type Campaign At Promotional Tab");
			}

			fc.utobj().printTestStep("Verify The Unarchived Promotional Campaign At Archived Tab");
			fc.utobj().clickElement(driver, pobj1.archivedTab);
			campaignCenterPage.searchCampaignByFilter(driver, campaignName);

			boolean isCampaignNamePresentArchivedTab = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () ,'No Record Found')]");
			if (isCampaignNamePresentArchivedTab == false) {
				fc.utobj().throwsException("was not able to verify unarchived Promotional Campaign At Archived Tab");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "crm", "crmRavi", "crmCampaignTest" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Delete Campaign Of Promotional Type Campaign At CRM > Campaign Center > Campaigns", testCaseId = "TC_18_Delete_Campaign_At_Promotional_Tab")
	private void deleteCampaignAtPromotionalTab()
			throws IOException, Exception, ParserConfigurationException, SAXException {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			CRMCampaignCenterCampaignsPage pobj = new CRMCampaignCenterCampaignsPage(driver);
			CRMCampaignCenterPageTest campaignCenterPage = new CRMCampaignCenterPageTest();
			CRMGroupsPageTest groupPage = new CRMGroupsPageTest();

			fc.utobj().printTestStep("Navigate To CRM > Campaign Center");
			fc.crm().crm_common().CRMModule(driver);

			fc.utobj().printTestStep("Create Campaign At Campaign Center > Campaigns");
			String campaignName = fc.utobj().generateTestData(dataSet.get("campaignName"));
			String accessibility = dataSet.get("accessibility");
			String campaignType = dataSet.get("campaignType");
			String templateName = fc.utobj().generateTestData(dataSet.get("templateName"));
			String emailSubject = fc.utobj().generateTestData(dataSet.get("emailSubject"));
			String plainTextEditor = fc.utobj().generateTestData(dataSet.get("plainTextEditor"));
			campaignCenterPage.createCampaignGeneric(driver, config, campaignName, accessibility, campaignType,
					templateName, emailSubject, plainTextEditor);

			fc.utobj().clickElement(driver, pobj.viewAllButton);

			fc.utobj().printTestStep("Delete Campaign At Promotional Campaign Tab");

			// fc.utobj().clickElement(driver, pobj.promotionalCampaignTab);
			campaignCenterPage.searchCampaignByFilter(driver, campaignName);
			groupPage.actionImgOption(driver, campaignName, "Delete");
			fc.utobj().acceptAlertBox(driver);

			fc.utobj().printTestStep("Verify Deleted Campaign At All Tab");
			campaignCenterPage.searchCampaignByFilter(driver, campaignName);

			boolean isCampaignNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () ,'No Record Found')]");
			if (isCampaignNamePresent == false) {
				fc.utobj().throwsException("was not able to verify Campaign At Campaign Center All Tab After Deletion");
			}

			// fc.utobj().clickElement(driver, pobj.promotionalCampaignTab);
			campaignCenterPage.searchCampaignByFilter(driver, campaignName);

			boolean isCampaignNamePresentAtStatusDrivenTab = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () ,'No Record Found')]");
			if (isCampaignNamePresentAtStatusDrivenTab == false) {
				fc.utobj().throwsException("was not able to verify the Deletion Of Promotional Type Campaign");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "crm", "crmRavi", "crmCampaignTest" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Modify Of Status Driven Type Campaign At CRM > Campaign Center > Campaigns [Status Driven Campaign Tab]", testCaseId = "TC_21_Modify_Campaign_At_Status_Driven_Tab")
	private void modifyCampaignAtStatusDrivenCampaignTab()
			throws IOException, Exception, ParserConfigurationException, SAXException {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			CRMCampaignCenterCampaignsPage pobj = new CRMCampaignCenterCampaignsPage(driver);
			CRMCampaignCenterPageTest campaignCenterPage = new CRMCampaignCenterPageTest();
			CRMGroupsPageTest groupPage = new CRMGroupsPageTest();

			fc.utobj().printTestStep("Navigate To CRM > Campaign Center");
			fc.crm().crm_common().CRMModule(driver);

			fc.utobj().printTestStep("Create Status Driven Type Campaign");
			String campaignName = fc.utobj().generateTestData(dataSet.get("campaignName"));
			String accessibility = dataSet.get("accessibility");
			String campaignType = dataSet.get("campaignType");
			String templateName = fc.utobj().generateTestData(dataSet.get("templateName"));
			String emailSubject = fc.utobj().generateTestData(dataSet.get("emailSubject"));
			String plainTextEditor = fc.utobj().generateTestData(dataSet.get("plainTextEditor"));
			campaignCenterPage.createCampaignGeneric(driver, config, campaignName, accessibility, campaignType,
					templateName, emailSubject, plainTextEditor);

			fc.utobj().printTestStep("Modify Campaign At Status Driven Campaign Tab");
			fc.utobj().clickElement(driver, pobj.viewAllButton);

			// fc.utobj().clickElement(driver, pobj.statusDrivenCampaignTab);

			campaignCenterPage.searchCampaignByFilter(driver, campaignName);
			groupPage.actionImgOption(driver, campaignName, "Modify");
			fc.utobj().clickElement(driver, pobj.modifyBtn);
			fc.commonMethods().switch_frameClass_newLayoutcboxIframe(driver);

			campaignName = fc.utobj().generateTestData(dataSet.get("campaignName"));
			fc.utobj().sendKeys(driver, pobj.campaignTitle, campaignName);
			fc.utobj().clickElement(driver, pobj.saveCampaignBtn);
			fc.utobj().clickElement(driver, pobj.closeBtn);
			fc.utobj().switchFrameToDefault(driver);
			fc.utobj().clickElement(driver, pobj.saveBtnAfterModify);

			fc.utobj().clickElement(driver, pobj.viewAllButton);

			fc.utobj().printTestStep("Verify The Modify Campaign At All Campaign Tab");
			campaignCenterPage.searchCampaignByStatusDrivenFilter(driver, campaignName);

			boolean isCampaignNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + campaignName + "')]");
			if (isCampaignNamePresent == false) {
				fc.utobj().throwsException(
						"was not able to verify the Status Driven Type Campaign At All Campaign Tab After Modification of campaign");
			}

			/*
			 * fc.utobj().printTestStep(testCaseId,
			 * "Verify The Modify Campaign At Status Driven Campaign Tab");
			 * fc.utobj().clickElement(driver, pobj.statusDrivenCampaignTab);
			 * campaignCenterPage.searchCampaignByFilter(driver, campaignName);
			 * 
			 * boolean isCampaignNamePresentAtPromotionalCampaignTab =
			 * fc.utobj().verifyCase(driver, ".//a[contains(text () ,'" +
			 * campaignName + "')]"); if
			 * (isCampaignNamePresentAtPromotionalCampaignTab == false) {
			 * fc.utobj().throwsException(
			 * "was not able to verify the campaign at Status Driven Campaign Tab After Modification Of Campaign"
			 * ); }
			 */

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "crm", "crmRavi", "crmCampaignTest" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Copy And Customize Of Status Driven Type Campaign At CRM > Campaign Center > Campaigns", testCaseId = "TC_22_Copy_And_Customize_Campaign_At_Status_Driven_Tab")
	private void copyAndCustomizeCampaignAtStatusDrivenCampaignTab()
			throws IOException, Exception, ParserConfigurationException, SAXException {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			CRMCampaignCenterCampaignsPage pobj = new CRMCampaignCenterCampaignsPage(driver);
			CRMCampaignCenterPageTest campaignCenterPage = new CRMCampaignCenterPageTest();
			CRMGroupsPageTest groupPage = new CRMGroupsPageTest();

			fc.utobj().printTestStep("Navigate To CRM > Campaign Center");
			fc.crm().crm_common().CRMModule(driver);

			fc.utobj().printTestStep("Create Status Driven Type Campaign");
			String campaignName = fc.utobj().generateTestData(dataSet.get("campaignName"));
			String accessibility = dataSet.get("accessibility");
			String campaignType = dataSet.get("campaignType");
			String templateName = fc.utobj().generateTestData(dataSet.get("templateName"));
			String emailSubject = fc.utobj().generateTestData(dataSet.get("emailSubject"));
			String plainTextEditor = fc.utobj().generateTestData(dataSet.get("plainTextEditor"));
			campaignCenterPage.createCampaignGeneric(driver, config, campaignName, accessibility, campaignType,
					templateName, emailSubject, plainTextEditor);

			fc.utobj().printTestStep("Copy And Customize Campaign At Status Driven Type Campaign Tab");
			fc.utobj().clickElement(driver, pobj.viewAllButton);
			// fc.utobj().clickElement(driver, pobj.statusDrivenCampaignTab);
			campaignCenterPage.searchCampaignByFilter(driver, campaignName);

			groupPage.actionImgOption(driver, campaignName, "Copy and Customize");
			fc.commonMethods().switch_frameClass_newLayoutcboxIframe(driver);
			String campaignTitleForCopy = fc.utobj().generateTestData(dataSet.get("campaignTitleForCopy"));
			fc.utobj().sendKeys(driver, pobj.campaignTitle, campaignTitleForCopy);
			String accessibilityForCopy = dataSet.get("accessibilityForCopy");
			fc.utobj().selectDropDown(driver, pobj.campaignAccessibility, accessibilityForCopy);
			fc.utobj().clickElement(driver, pobj.startProcessButton);
			fc.utobj().switchFrameToDefault(driver);
			fc.utobj().clickElement(driver, pobj.finalizeAndLaunchCampaignCopy);

			fc.utobj().printTestStep("Verify The Copy And Customize Campaign At All Campaign Tab");
			fc.utobj().clickElement(driver, pobj.viewAllButton);
			campaignCenterPage.searchCampaignByFilter(driver, campaignName);

			boolean isCampaignNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + campaignName + "')]");
			if (isCampaignNamePresent == false) {
				fc.utobj().throwsException("was not able to verify Copy And Customize Campaign At All Campaign Tab");
			}

			fc.utobj().printTestStep("Verify The Copy And Customize Campaign At Status Driven Type Campaign Tab");

			/*
			 * fc.utobj().clickElement(driver, pobj.statusDrivenCampaignTab);
			 * campaignCenterPage.searchCampaignByFilter(driver, campaignName);
			 * 
			 * boolean isCampaignNamePresentAtPromotionalCampaignTab =
			 * fc.utobj().verifyCase(driver, ".//a[contains(text () ,'" +
			 * campaignName + "')]"); if
			 * (isCampaignNamePresentAtPromotionalCampaignTab == false) {
			 * fc.utobj().throwsException(
			 * "was not able to verify copy and customize campaign At Status Driven Type Campaign Tab"
			 * ); }
			 */

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "crm", "crmmodcheck", "TC_23_Stop_Campaign_For_Contact_Status_Driven_Campaign_Tab",
			"crmCampaignTest","errorpages1" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2017-06-27", testCaseDescription = "Verify The Stop Campaign For Contact At Status Driven Campaign Tab And Also Verify The Activity Created at activity Tab", testCaseId = "TC_23_Stop_Campaign_For_Contact_Status_Driven_Campaign_Tab")
	private void stopCampaignForContactAtStatusDrivenCampaignTab()
			throws IOException, Exception, ParserConfigurationException, SAXException {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			CRMContactsPage pobj = new CRMContactsPage(driver);
			CRMCampaignCenterPageTest campaignCenterPage = new CRMCampaignCenterPageTest();
			CRMContactsPageTest contactPageTest = new CRMContactsPageTest();
			CRMCampaignCenterCampaignsPage pobj1 = new CRMCampaignCenterCampaignsPage(driver);
			CRMGroupsPageTest groupPage = new CRMGroupsPageTest();

			fc.utobj().printTestStep("Navigate To CRM > Campaign Center");
			fc.crm().crm_common().CRMModule(driver);

			fc.utobj().printTestStep("Create Status Driven Type Campaign");

			String campaignName = fc.utobj().generateTestData(dataSet.get("campaignName"));
			String accessibility = dataSet.get("accessibility");
			String campaignType = dataSet.get("campaignType");
			String templateName = fc.utobj().generateTestData(dataSet.get("templateName"));
			String emailSubject = fc.utobj().generateTestData(dataSet.get("emailSubject"));
			String plainTextEditor = fc.utobj().generateTestData(dataSet.get("plainTextEditor"));
			campaignCenterPage.createCampaignGeneric(driver, config, campaignName, accessibility, campaignType,
					templateName, emailSubject, plainTextEditor);

			fc.utobj().clickElement(driver, pobj1.viewAllButton);
			// fc.utobj().clickElement(driver, pobj1.statusDrivenCampaignTab);
			campaignCenterPage.searchCampaignByFilter(driver, campaignName);
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//a[contains(text () ,'" + campaignName + "')]"));

			String userText = driver
					.findElement(By.xpath(".//div[contains(text () , 'Added by')]/following-sibling::div[1]"))
					.getText();
			// fc.utobj().clickElement(driver, pobj1.backButton);

			fc.utobj().printTestStep("Verify The Activity Of Created Campaign At Activity Tab");
			// fc.utobj().clickElement(driver, pobj1.activityTab);

			// campaignCenterPage.searchCampaignByFilter(driver, campaignName);
			WebElement element = fc.utobj().getElementByXpath(driver, ".//*[@id='trackingData']/table/tbody/tr/td[2]");
			String textPresent = fc.utobj().getText(driver, element);

			if (!textPresent.contains(campaignName) && !textPresent.contains(userText)
					&& !textPresent.contains("has been added")) {
				fc.utobj().throwsException("was not able to verify Activity of Created Campaign");
			}

			fc.utobj().printTestStep("Navigate To Admin Users Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			String emailId = "crmautomation@staffex.com";
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUser = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();

			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporateUser.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			fc.utobj().printTestStep("Navigate To Admin > CRM > Contact Type Configuration");
			fc.utobj().printTestStep("Add Contact Type");
			AdminCRMContactTypeConfigurationPageTest contatcTypePage = new AdminCRMContactTypeConfigurationPageTest();
			String contactType = fc.utobj().generateTestData(dataSet.get("contactType"));
			contatcTypePage.addContactType(driver, contactType);

			fc.utobj().printTestStep("Navigate To CRM > Contacts > Contacts Summary Page");
			fc.utobj().printTestStep("Add Contact");
			fc.crm().crm_common().CRMContactsLnk(driver);
			fc.utobj().clickElement(driver, pobj.addNew);
			fc.utobj().clickElement(driver, pobj.contactLnk);

			String firstName = fc.utobj().generateTestData(dataSet.get("firstName"));
			String lastName = fc.utobj().generateTestData(dataSet.get("lastName"));
			String primaryCMethod = dataSet.get("primaryCMethod");
			String assignTo = "Corporate";
			String regionName = "";
			String franchiseId = "";
			String title = dataSet.get("title");
			String email = dataSet.get("emailId");

			contactPageTest.addContactGeneric(driver, contactType, firstName, lastName, primaryCMethod, assignTo,
					corpUser.getuserFullName(), regionName, franchiseId, title, email, config);

			fc.utobj().clickElement(driver, pobj.contactsLinks);
			contactPageTest.contactsFilter(driver, contactType, corpUser.getuserFullName());

			fc.utobj().printTestStep("Associate With Email Campaign");

			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//a[contains(text () ,'" + firstName
					+ " " + lastName + "')]/ancestor::tr/td/input[@name='checkb']"));

			fc.utobj().clickElement(driver, pobj.associateWithEmailCampaignBtmBtn);
			fc.utobj().clickElement(driver, pobj.filterContainer);
			fc.utobj().sendKeys(driver, pobj.searchCampaign, campaignName);
			fc.utobj().clickElement(driver, pobj.applyFilter);

			fc.utobj().clickElement(driver, new CRMLeadsPage(driver).sendCampaignBtn);
			fc.utobj().clickElement(driver, pobj.confirmBtn);

			fc.utobj().clickElement(driver, pobj.contactsLinks);
			contactPageTest.contactsFilter(driver, contactType);

			fc.utobj().printTestStep("Verify Contact Associate With Campaign At Contact Primary Info Page");
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//a[contains(text () ,'" + firstName + " " + lastName + "')]"));

			boolean isCampaignNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () ,'Status-Driven')]/following-sibling::td[contains(text () ,'" + campaignName
							+ "')]");
			if (isCampaignNamePresent == false) {
				fc.utobj().throwsException("was not able to verify Campign Name");
			}

			fc.utobj().printTestStep("Navigate To CRM >Campaign Center");
			fc.utobj().clickElement(driver, pobj1.campaignCenterLinks);
			fc.utobj().clickElement(driver, pobj1.viewAllButton);

			fc.utobj().printTestStep("Verify Activity Of Associate Campaign With Contact At Activity Tab");
			// fc.utobj().clickElement(driver, pobj1.activityTab);
			campaignCenterPage.searchCampaignByFilter(driver, campaignName);

			WebElement element1 = driver
					.findElement(By.xpath(".//a[contains(text () , '" + campaignName + "')]/ancestor::td"));
			String textPresent1 = fc.utobj().getText(driver, element1);

			if (!textPresent1.contains(campaignName) && !textPresent1.contains(userText)
					&& !textPresent1.contains("has been associated with") && !textPresent1.contains("contact(s)")) {
				fc.utobj().throwsException("was not able to verify Activity For Associated Campaign WIth Contact");
			}
			fc.utobj().printTestStep("Stop Campaign At Status Driven Tab");
			fc.utobj().printTestStep("Navigate To CRM >Campaign Center");
			fc.utobj().clickElement(driver, pobj1.campaignCenterLinks);
			fc.utobj().clickElement(driver, pobj1.viewAllButton);

			campaignCenterPage.searchCampaignByFilter(driver, campaignName);
			// fc.utobj().clickElement(driver,
			// fc.utobj().getElementByXpath(driver,".//a[contains(text () ,'" +
			// campaignName + "')]")));

			groupPage.actionImgOption(driver, campaignName, "Stop Campaign");
			fc.utobj().acceptAlertBox(driver);

			fc.utobj().printTestStep("Verify Activity Of Stop Campaign At Activity Tab");
			// fc.utobj().clickElement(driver, pobj1.activityTab);
			campaignCenterPage.searchCampaignByFilter(driver, campaignName);
			WebElement element2 = driver
					.findElement(By.xpath(".//a[contains(text () , '" + campaignName + "')]/ancestor::td"));
			String textPresent2 = fc.utobj().getText(driver, element2);

			if (!textPresent2.contains(campaignName) && !textPresent2.contains(userText)
					&& !textPresent2.contains("has been stopped")) {
				fc.utobj().throwsException("was not able to verify Activity For Stop Campaign");
			}

			fc.utobj().clickElement(driver, pobj.contactsLinks);
			contactPageTest.contactsFilter(driver, contactType, corpUser.getuserFullName());

			fc.utobj().printTestStep(
					"Verify The Campaign Associate With Contact After Stop The Campaign At Contact Primary Info Page");
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//a[contains(text () ,'" + firstName + " " + lastName + "')]"));

			boolean isCampaignNamePresent1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text(),'Promotional Campaign') and not (contains(text(),'Older Promotional Campaign'))]//following-sibling::td[contains(text(),'"
							+ campaignName + "')]");
			if (isCampaignNamePresent1 == true) {
				fc.utobj().throwsException(
						"Was not able to verify the Campaign After Stop The Campaign At Contact Primary Info Page");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "crm", "crmRavi", "crmCampaignTest"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Archive Campaign Of Status Driven Type Campaign At CRM > Campaign Center > Campaigns [Status Campaign Tab]", testCaseId = "TC_24_Archive_Campaign_At_Status_Driven_Tab")
	private void archiveCampaignAtStatusDrivenCampaignTab()
			throws IOException, Exception, ParserConfigurationException, SAXException {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			CRMCampaignCenterCampaignsPage pobj = new CRMCampaignCenterCampaignsPage(driver);
			CRMCampaignCenterPageTest campaignCenterPage = new CRMCampaignCenterPageTest();
			CRMGroupsPageTest groupPage = new CRMGroupsPageTest();
			CRMCampaignCenterCampaignsPage pobj1 = new CRMCampaignCenterCampaignsPage(driver);

			fc.utobj().printTestStep("Navigate To CRM > Campaign Center");
			fc.crm().crm_common().CRMModule(driver);

			fc.utobj().printTestStep("Create Status Driven Type Campaign");
			String campaignName = fc.utobj().generateTestData(dataSet.get("campaignName"));
			String accessibility = dataSet.get("accessibility");
			String campaignType = dataSet.get("campaignType");
			String templateName = fc.utobj().generateTestData(dataSet.get("templateName"));
			String emailSubject = fc.utobj().generateTestData(dataSet.get("emailSubject"));
			String plainTextEditor = fc.utobj().generateTestData(dataSet.get("plainTextEditor"));
			campaignCenterPage.createCampaignGeneric(driver, config, campaignName, accessibility, campaignType,
					templateName, emailSubject, plainTextEditor);

			fc.utobj().clickElement(driver, pobj.viewAllButton);
			fc.utobj().printTestStep("Archive Campaign At Status Driven Campaign Tab");

			// fc.utobj().clickElement(driver, pobj.statusDrivenCampaignTab);
			campaignCenterPage.searchCampaignByFilter(driver, campaignName);
			groupPage.actionImgOption(driver, campaignName, "Archive");
			fc.utobj().acceptAlertBox(driver);

			fc.utobj().printTestStep("Verify Archive Campaign At All Campaign Tab");
			campaignCenterPage.searchCampaignByFilter(driver, campaignName);

			boolean isCampaignNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () ,'No Record Found')]");
			if (isCampaignNamePresent == false) {
				fc.utobj()
						.throwsException("was not able to verify Campaign At All Campaign Tab After Archived Campaign");
			}

			//fc.utobj().clickElement(driver, pobj.statusDrivenCampaignTab);
			/*fc.utobj().clickElement(driver, pobj.archivedTab);
			campaignCenterPage.searchCampaignByFilter(driver, campaignName);

			boolean isCampaignNamePresentAtStatusDrivenTab = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () ,'No Record Found')]");
			if (isCampaignNamePresentAtStatusDrivenTab == false) {
				fc.utobj().throwsException(
						"was not able to verify Archive Campaign At Status Driven Campaign Tab After Archived Campaign");
			}*/

			fc.utobj().printTestStep("Verify The Archived Status Driven Campaign At Archived Tab");
			fc.utobj().clickElement(driver, pobj1.archivedTab);
			campaignCenterPage.searchCampaignByFilter(driver, campaignName);

			boolean isCampaignNamePresentAtArchiveTab = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + campaignName + "')]");
			if (isCampaignNamePresentAtArchiveTab == false) {
				fc.utobj().throwsException("was not able to verify Archived Status Driven Campaign At Archived Tab");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "crm", "crmRavi", "crmCampaignTest" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Delete Campaign Of Status Driven Type Campaign At CRM > Campaign Center > Campaigns", testCaseId = "TC_25_Delete_Campaign_At_Status_Driven_Tab")
	private void deleteCampaignAtStatusDrivenCampaignTab()
			throws IOException, Exception, ParserConfigurationException, SAXException {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			CRMCampaignCenterCampaignsPage pobj = new CRMCampaignCenterCampaignsPage(driver);
			CRMCampaignCenterPageTest campaignCenterPage = new CRMCampaignCenterPageTest();
			CRMGroupsPageTest groupPage = new CRMGroupsPageTest();

			fc.utobj().printTestStep("Navigate To CRM > Campaign Center");
			fc.crm().crm_common().CRMModule(driver);

			fc.utobj().printTestStep("Create Status Driven Type Campaign");
			String campaignName = fc.utobj().generateTestData(dataSet.get("campaignName"));
			String accessibility = dataSet.get("accessibility");
			String campaignType = dataSet.get("campaignType");
			String templateName = fc.utobj().generateTestData(dataSet.get("templateName"));
			String emailSubject = fc.utobj().generateTestData(dataSet.get("emailSubject"));
			String plainTextEditor = fc.utobj().generateTestData(dataSet.get("plainTextEditor"));
			campaignCenterPage.createCampaignGeneric(driver, config, campaignName, accessibility, campaignType,
					templateName, emailSubject, plainTextEditor);

			fc.utobj().clickElement(driver, pobj.viewAllButton);
			fc.utobj().printTestStep("Delete Campaign At Status Driven Campaign Tab");

			// fc.utobj().clickElement(driver, pobj.statusDrivenCampaignTab);
			campaignCenterPage.searchCampaignByFilter(driver, campaignName);
			groupPage.actionImgOption(driver, campaignName, "Delete");
			fc.utobj().acceptAlertBox(driver);

			fc.utobj().printTestStep("Verify Deleted Campaign At All Campaign Tab");
			campaignCenterPage.searchCampaignByFilter(driver, campaignName);

			boolean isCampaignNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () ,'No Record Found')]");
			if (isCampaignNamePresent == false) {
				fc.utobj().throwsException(
						"was not able to verify Status Driven Type Campaign At All Campaign Tab After Deletion Of Campaign");
			}

			fc.utobj().printTestStep("Verify Deleted Campaign At Status Driven Campaign Tab");
			// fc.utobj().clickElement(driver, pobj.statusDrivenCampaignTab);
			campaignCenterPage.searchCampaignByFilter(driver, campaignName);

			boolean isCampaignNamePresentAtStatusDrivenTab = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () ,'No Record Found')]");
			if (isCampaignNamePresentAtStatusDrivenTab == false) {
				fc.utobj().throwsException(
						"was not able to verify the staus driven campaign at status driven campaign tab after deletion of tab");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	private boolean CheckWithFranUser(WebDriver driver, Map<String, String> config, Map<String, String> dataSet,
			String templateName, boolean isLocationTemplate) throws Exception {
		try {
			/*
			 * driver.get(config.get("buildUrl")); LoginPage npobj = new
			 * LoginPage(driver); fc.utobj().sendKeys(driver, npobj.userid,
			 * "adm"); fc.utobj().sendKeys(driver, npobj.password, "t0n1ght");
			 * fc.utobj().clickElement(driver, npobj.login);
			 */

			String regionName = dataSet.get("regionName") + fc.utobj().generateRandomNumber();
			String storeType = "Default";
			String franchiseName = "franchiseName"+ fc.utobj().generateRandomNumber6Digit();
			String centerName = fc.utobj().generateTestData("0");
			String openingDate = fc.utobj().generateCurrentDatewithformat("yyyy-MM-dd");
			String password = "fran1234";
			String confirmPassword = "fran1234";
			String loginId = dataSet.get("loginIdforfran") + fc.utobj().generateRandomNumber();
			String ownerFirstName = "ownerFirstName"+ fc.utobj().generateRandomNumber6Digit();
			String ownerLastName = "ownerLastName" + fc.utobj().generateRandomNumber6Digit();
			String emailId = dataSet.get("emailId");
			AdminFranchiseLocationAddFranchiseLocationPageTest fran = new AdminFranchiseLocationAddFranchiseLocationPageTest();
			CRMCampaignCenterCampaignsPage pobj = new CRMCampaignCenterCampaignsPage(driver);
			fran.addFranchiseLocationWithUserZC(driver, regionName, storeType, franchiseName, centerName, openingDate,
					password, confirmPassword, loginId, ownerFirstName, ownerLastName, config, emailId);

			fc.home_page().logout(driver);

			/*
			 * driver.get(config.get("buildUrl")); fc.utobj().sendKeys(driver,
			 * npobj.userid, loginId); fc.utobj().sendKeys(driver,
			 * npobj.password, password); fc.utobj().clickElement(driver,
			 * npobj.login);
			 */

			fc.loginpage().loginWithParameter(driver, loginId, password);

			fc.crm().crm_common().CRMCampaignCenterLnk(driver);
			fc.utobj().clickElement(driver, pobj.dashBoradLnk);
			fc.utobj().clickElement(driver, pobj.templatesLink);
			if (isLocationTemplate)
				fc.utobj().clickElement(driver, pobj.locationtemplatesLink);
			fc.utobj().clickElement(driver, pobj.showFilter);
			fc.utobj().sendKeys(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='searchTemplate']"),
					templateName);
			fc.utobj().clickElement(driver, pobj.applyFilterAtGroup);
			fc.utobj().clickElement(driver, pobj.hideFilter);

			boolean foundforFranuser = fc.utobj().isElementPresent(driver, ".//a[contains(text(),'"+templateName+"')]");
			//boolean foundforFranuser = fc.utobj().assertPageSource(driver, templateName);

			fc.home_page().logout(driver);
			fc.loginpage().loginWithParameter(driver, config.get("userName"), config.get("password"));
			return foundforFranuser;
		} catch (Exception e) {
			fc.utobj().throwsException("Problem came while checking template with fran user ");
		}
		return false;
	}

	private boolean CheckWithFranUserWithRegionNameExist(WebDriver driver, Map<String, String> config,
			Map<String, String> dataSet, String templateName, String regionName, boolean isLocationTemplate)
			throws Exception {
		try {
			/*
			 * driver.get(config.get("buildUrl")); LoginPage npobj = new
			 * LoginPage(driver); fc.utobj().sendKeys(driver, npobj.userid,
			 * "adm"); fc.utobj().sendKeys(driver, npobj.password, "t0n1ght");
			 * fc.utobj().clickElement(driver, npobj.login);
			 */

			String storeType = "Default";
			String franchiseName = dataSet.get("franchiseName") + fc.utobj().generateRandomNumber6Digit();
			String centerName = dataSet.get("centerName");
			String openingDate = fc.utobj().generateCurrentDatewithformat("yyyy-MM-dd");
			String password = dataSet.get("password");
			String confirmPassword = dataSet.get("confirmPassword");
			String loginId = dataSet.get("loginIdforfran") + fc.utobj().generateRandomNumber();
			String ownerFirstName = dataSet.get("ownerFirstName") + fc.utobj().generateRandomNumber6Digit();
			String ownerLastName = dataSet.get("ownerLastName") + fc.utobj().generateRandomNumber6Digit();
			String emailId = dataSet.get("emailId");
			AdminFranchiseLocationAddFranchiseLocationPageTest fran = new AdminFranchiseLocationAddFranchiseLocationPageTest();
			CRMCampaignCenterCampaignsPage pobj = new CRMCampaignCenterCampaignsPage(driver);
			fran.addFranchiseLocationWithUserZCWithRegionNameExist(driver, regionName, storeType, franchiseName,
					centerName, openingDate, password, confirmPassword, loginId, ownerFirstName, ownerLastName, config,
					emailId);

			fc.home_page().logout(driver);

			/*
			 * driver.get(config.get("buildUrl")); fc.utobj().sendKeys(driver,
			 * npobj.userid, loginId); fc.utobj().sendKeys(driver,
			 * npobj.password, password); fc.utobj().clickElement(driver,
			 * npobj.login);
			 */

			fc.loginpage().loginWithParameter(driver, loginId, password);

			fc.crm().crm_common().CRMCampaignCenterLnk(driver);
			fc.utobj().clickElement(driver, pobj.dashBoradLnk);
			fc.utobj().clickElement(driver, pobj.templatesLink);
			if (isLocationTemplate)
				fc.utobj().clickElement(driver, pobj.locationtemplatesLink);
			fc.utobj().clickElement(driver, pobj.showFilter);
			fc.utobj().sendKeys(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='searchTemplate']"),
					templateName);
			fc.utobj().clickElement(driver, pobj.applyFilterAtGroup);
			fc.utobj().clickElement(driver, pobj.hideFilter);
			boolean foundforFranuser = fc.utobj().isElementPresent(driver, ".//a[contains(text(),'"+templateName+"')]");
			//boolean foundforFranuser = fc.utobj().assertPageSource(driver, templateName);

			fc.home_page().logout(driver);
			fc.loginpage().loginWithParameter(driver, config.get("userName"), config.get("password"));
			return foundforFranuser;
		} catch (Exception e) {
			fc.utobj().throwsException("Problem came while checking template with fran user ");
		}
		return false;
	}

	private boolean CheckWithRegUser(WebDriver driver, Map<String, String> config, Map<String, String> dataSet,
			String region, String templateName, boolean isLocationTemplate) throws Exception {

		try {
			/*
			 * driver.get(config.get("buildUrl")); LoginPage npobj = new
			 * LoginPage(driver); fc.utobj().sendKeys(driver, npobj.userid,
			 * "adm"); fc.utobj().sendKeys(driver, npobj.password, "t0n1ght");
			 * fc.utobj().clickElement(driver, npobj.login);
			 */

			String userName = dataSet.get("userName") + fc.utobj().generateRandomNumber();
			String emailId = dataSet.get("emailId");
			String regionName = region;
			AdminUsersManageRegionalUsersAddRegionalUserPageTest reguser = new AdminUsersManageRegionalUsersAddRegionalUserPageTest();
			CRMCampaignCenterCampaignsPage pobj = new CRMCampaignCenterCampaignsPage(driver);
			reguser.addRegionalUserZC(driver, userName, regionName, config, emailId);

			fc.home_page().logout(driver);

			/*
			 * driver.get(config.get("buildUrl")); fc.utobj().sendKeys(driver,
			 * npobj.userid, userName); fc.utobj().sendKeys(driver,
			 * npobj.password, "T0n1ght1"); fc.utobj().clickElement(driver,
			 * npobj.login);
			 */

			fc.loginpage().loginWithParameter(driver, userName, "T0n1ght1");

			fc.crm().crm_common().CRMCampaignCenterLnk(driver);
			fc.utobj().clickElement(driver, pobj.dashBoradLnk);
			fc.utobj().clickElement(driver, pobj.templatesLink);
			if (isLocationTemplate)
				fc.utobj().clickElement(driver, pobj.locationtemplatesLink);
			fc.utobj().clickElement(driver, pobj.showFilter);
			fc.utobj().sendKeys(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='searchTemplate']"),
					templateName);
			fc.utobj().clickElement(driver, pobj.applyFilterAtGroup);
			fc.utobj().clickElement(driver, pobj.hideFilter);
			boolean foundforRegUser = fc.utobj().isElementPresent(driver, ".//a[contains(text(),'"+templateName+"')]");
			//boolean foundforRegUser = fc.utobj().assertPageSource(driver, templateName);

			fc.home_page().logout(driver);
			fc.loginpage().loginWithParameter(driver, config.get("userName"), config.get("password"));

			return foundforRegUser;

		} catch (Exception e) {
			fc.utobj().throwsException("Problem came while checking template with reg user ");
		}
		return false;
	}

	private boolean CheckWithRegUserWithRegionNameExist(WebDriver driver, Map<String, String> config,
			Map<String, String> dataSet, String templateName, String regionName, boolean isLocationTemplate)
			throws Exception {
		try {
			/*
			 * driver.get(config.get("buildUrl")); LoginPage npobj = new
			 * LoginPage(driver); fc.utobj().sendKeys(driver, npobj.userid,
			 * "adm"); fc.utobj().sendKeys(driver, npobj.password, "t0n1ght");
			 * fc.utobj().clickElement(driver, npobj.login);
			 */

			String userName = dataSet.get("userName") + fc.utobj().generateRandomNumber();
			String emailId = dataSet.get("emailId");
			AdminUsersManageRegionalUsersAddRegionalUserPageTest reguser = new AdminUsersManageRegionalUsersAddRegionalUserPageTest();
			CRMCampaignCenterCampaignsPage pobj = new CRMCampaignCenterCampaignsPage(driver);
			reguser.addRegionalUserZCWithRegionNameExist(driver, userName, regionName, config, emailId);

			fc.home_page().logout(driver);

			/*
			 * driver.get(config.get("buildUrl")); fc.utobj().sendKeys(driver,
			 * npobj.userid, userName); fc.utobj().sendKeys(driver,
			 * npobj.password, "T0n1ght1"); fc.utobj().clickElement(driver,
			 * npobj.login);
			 */

			fc.loginpage().loginWithParameter(driver, userName, "T0n1ght1");
			fc.crm().crm_common().CRMCampaignCenterLnk(driver);
			fc.utobj().clickElement(driver, pobj.dashBoradLnk);
			fc.utobj().clickElement(driver, pobj.templatesLink);
			if (isLocationTemplate)
				fc.utobj().clickElement(driver, pobj.locationtemplatesLink);
			fc.utobj().clickElement(driver, pobj.showFilter);
			fc.utobj().sendKeys(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='searchTemplate']"),
					templateName);
			fc.utobj().clickElement(driver, pobj.applyFilterAtGroup);
			fc.utobj().clickElement(driver, pobj.hideFilter);
			boolean foundforRegUser = fc.utobj().isElementPresent(driver, ".//a[contains(text(),'"+templateName+"')]");
			//boolean foundforRegUser = fc.utobj().assertPageSource(driver, templateName);

			fc.home_page().logout(driver);
			fc.loginpage().loginWithParameter(driver, config.get("userName"), config.get("password"));

			return foundforRegUser;
		} catch (Exception e) {
			fc.utobj().throwsException("Problem came while checking template with reg user ");
		}
		return false;
	}

	private boolean CheckWithDivUser(WebDriver driver, Map<String, String> config, Map<String, String> dataSet,
			String templateName, boolean isLocationTemplate) throws Exception {
		try {
			/*
			 * driver.get(config.get("buildUrl")); LoginPage npobj = new
			 * LoginPage(driver); fc.utobj().sendKeys(driver, npobj.userid,
			 * "adm"); fc.utobj().sendKeys(driver, npobj.password, "t0n1ght");
			 * fc.utobj().clickElement(driver, npobj.login);
			 */

			String userName = dataSet.get("userName") + fc.utobj().generateRandomNumber();
			String emailId = dataSet.get("emailId");
			String divisionName = dataSet.get("divisionName") + fc.utobj().generateRandomNumber();
			String regionName = dataSet.get("regionName") + fc.utobj().generateRandomNumber();
			AdminUsersManageDivisionalUsersAddDivisionalUsersPageTest divuser = new AdminUsersManageDivisionalUsersAddDivisionalUsersPageTest();
			CRMCampaignCenterCampaignsPage pobj = new CRMCampaignCenterCampaignsPage(driver);
			divuser.addDivisionalUser(driver, userName, divisionName, emailId);

			fc.home_page().logout(driver);

			/*
			 * driver.get(config.get("buildUrl")); fc.utobj().sendKeys(driver,
			 * npobj.userid, userName); fc.utobj().sendKeys(driver,
			 * npobj.password, "T0n1ght1"); fc.utobj().clickElement(driver,
			 * npobj.login);
			 */

			fc.loginpage().loginWithParameter(driver, userName, "fran1234");

			fc.crm().crm_common().CRMCampaignCenterLnk(driver);
			fc.utobj().clickElement(driver, pobj.dashBoradLnk);
			fc.utobj().clickElement(driver, pobj.templatesLink);
			if (isLocationTemplate)
				fc.utobj().clickElement(driver, pobj.locationtemplatesLink);
			fc.utobj().clickElement(driver, pobj.showFilter);
			fc.utobj().sendKeys(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='searchTemplate']"),
					templateName);
			fc.utobj().clickElement(driver, pobj.applyFilterAtGroup);
			fc.utobj().clickElement(driver, pobj.hideFilter);
			boolean foundforDivUser = fc.utobj().isElementPresent(driver, ".//a[contains(text(),'"+templateName+"')]");
			//boolean foundforDivUser = fc.utobj().assertPageSource(driver, templateName);

			fc.home_page().logout(driver);
			fc.loginpage().loginWithParameter(driver, config.get("userName"), config.get("password"));

			return foundforDivUser;
		} catch (Exception e) {
			fc.utobj().throwsException("Problem came while checking template with div user ");
		}
		return false;
	}

	private boolean CheckWithDivUserWithDivisionExist(WebDriver driver, Map<String, String> config,
			Map<String, String> dataSet, String templateName, String divisionName, boolean isLocationTemplate)
			throws Exception {
		try {
			driver.get(config.get("buildUrl"));
			LoginPage npobj = new LoginPage(driver);
			fc.utobj().sendKeys(driver, npobj.userid, "adm");
			fc.utobj().sendKeys(driver, npobj.password, "t0n1ght");
			fc.utobj().clickElement(driver, npobj.login);
			String userName = dataSet.get("userName") + fc.utobj().generateRandomNumber();
			String emailId = dataSet.get("emailId");
			AdminUsersManageDivisionalUsersAddDivisionalUsersPageTest divuser = new AdminUsersManageDivisionalUsersAddDivisionalUsersPageTest();
			CRMCampaignCenterCampaignsPage pobj = new CRMCampaignCenterCampaignsPage(driver);
			divuser.addDivisionalUserWithDivisionNameExist(driver, userName, divisionName, config, emailId);
			driver.get(config.get("buildUrl"));
			fc.utobj().sendKeys(driver, npobj.userid, userName);
			fc.utobj().sendKeys(driver, npobj.password, "T0n1ght1");
			fc.utobj().clickElement(driver, npobj.login);
			fc.crm().crm_common().CRMCampaignCenterLnk(driver);
			fc.utobj().clickElement(driver, pobj.dashBoradLnk);
			fc.utobj().clickElement(driver, pobj.templatesLink);
			if (isLocationTemplate)
				fc.utobj().clickElement(driver, pobj.locationtemplatesLink);
			fc.utobj().clickElement(driver, pobj.showFilter);
			fc.utobj().sendKeys(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='searchTemplate']"),
					templateName);
			fc.utobj().clickElement(driver, pobj.applyFilterAtGroup);
			fc.utobj().clickElement(driver, pobj.hideFilter);
			boolean foundforDivUser = fc.utobj().assertPageSource(driver, templateName);
			return foundforDivUser;
		} catch (Exception e) {
			fc.utobj().throwsException("Problem came while checking template with div user ");
		}
		return false;
	}

	private boolean CheckWithCorpUser(WebDriver driver, Map<String, String> config, Map<String, String> dataSet,
			String templateName, boolean isLocationTemplate) throws Exception {
		try {
			/*
			 * driver.get(config.get("buildUrl")); LoginPage npobj = new
			 * LoginPage(driver); fc.utobj().sendKeys(driver, npobj.userid,
			 * "adm"); fc.utobj().sendKeys(driver, npobj.password, "t0n1ght");
			 * fc.utobj().clickElement(driver, npobj.login);
			 */

			AdminUsersManageCorporateUsersAddCorporateUserPageTest addcorpuser = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CRMCampaignCenterCampaignsPage pobj = new CRMCampaignCenterCampaignsPage(driver);
			String corpuseris = "TestCorpUserTemp" + fc.utobj().generateRandomNumber();
			CorporateUser corpUser = new CorporateUser();
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corpUser.setEmail("crmautomation@staffex.com");
			corpUser.setUserName(corpuseris);
			corpUser.setPassword("T0n1ght1");
			corpUser = addcorpuser.createDefaultUser(driver, corpUser);
			fc.home_page().logout(driver);

			/*
			 * driver.get(config.get("buildUrl")); fc.utobj().sendKeys(driver,
			 * npobj.userid, corpuseris); fc.utobj().sendKeys(driver,
			 * npobj.password, "T0n1ght1"); fc.utobj().clickElement(driver,
			 * npobj.login);
			 */

			fc.loginpage().loginWithParameter(driver, corpuseris, "T0n1ght1");

			fc.crm().crm_common().CRMCampaignCenterLnk(driver);
			fc.utobj().clickElement(driver, pobj.dashBoradLnk);
			fc.utobj().clickElement(driver, pobj.templatesLink);
			if (isLocationTemplate) {
				fc.utobj().clickElement(driver, pobj.locationtemplatesLink);
			}
			fc.utobj().clickElement(driver, pobj.showFilter);
			fc.utobj().sendKeys(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='searchTemplate']"),
					templateName);
			fc.utobj().clickElement(driver, pobj.applyFilterAtGroup);
			fc.utobj().clickElement(driver, pobj.hideFilter);
			boolean foundforcorpuser = fc.utobj().isElementPresent(driver, ".//a[contains(text(),'"+templateName+"')]");
			//boolean foundforcorpuser = fc.utobj().assertPageSource(driver, templateName);

			fc.home_page().logout(driver);
			fc.loginpage().loginWithParameter(driver, config.get("userName"), config.get("password"));

			return foundforcorpuser;
		} catch (Exception ex) {
			fc.utobj().throwsException("Problem came while checking template with corp user ");
		}
		return false;
	}

}
