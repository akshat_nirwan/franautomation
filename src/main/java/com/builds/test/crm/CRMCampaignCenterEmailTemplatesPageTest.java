package com.builds.test.crm;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.xml.parsers.ParserConfigurationException;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.Test;
import org.xml.sax.SAXException;

import com.builds.test.admin.AdminFranchiseLocationAddFranchiseLocationPageTest;
import com.builds.test.admin.AdminUsersManageManageFranchiseUsersPageTest;
import com.builds.uimaps.campaigncentercrm.CRMCampaignCenterEmailTemplatesPage;
import com.builds.utilities.FranconnectUtil;
import com.builds.utilities.TestCase;

public class CRMCampaignCenterEmailTemplatesPageTest {

	FranconnectUtil fc = new FranconnectUtil();

	@Test(groups = "crm")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Create Corporate Template At CRM > Campaign Center > Email Templates", testCaseId = "TC_28_Create_Corporate_Template")
	private void createCorporateTemplate() throws IOException, Exception, ParserConfigurationException, SAXException {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			CRMCampaignCenterEmailTemplatesPage pobj = new CRMCampaignCenterEmailTemplatesPage(driver);
			CRMCampaignCenterPageTest campaignCenterPage = new CRMCampaignCenterPageTest();

			fc.utobj().printTestStep("Navigate To CRM > Campaign Center > Email Template");
			fc.crm().crm_common().CRMCampaignCenterLnk(driver);
			fc.utobj().clickElement(driver, pobj.dashBoradLnk);
			fc.utobj().clickElement(driver, pobj.templateLnk);

			fc.utobj().printTestStep("Create Template");
			fc.utobj().clickElement(driver, pobj.createLink);
			fc.utobj().clickElement(driver, pobj.templateLink);

			fc.utobj().printTestStep("Code Your Own Template");

			// Code Your Template
			String templateName = fc.utobj().generateTestData(dataSet.get("templateName"));
			String emailSubject = fc.utobj().generateTestData(dataSet.get("emailSubject"));
			String plainTextEditor = fc.utobj().generateTestData(dataSet.get("plainTextEditor"));

			fc.utobj().clickElement(driver, pobj.codeYourOwn);
			fc.utobj().sendKeys(driver, pobj.templateName, templateName);
			fc.utobj().sendKeys(driver, pobj.mailSubject, emailSubject);
			fc.utobj().clickElement(driver, pobj.publicToAllUsersTemplate);
			fc.utobj().clickElement(driver, pobj.plainTextEmailType);
			fc.utobj().sendKeys(driver, pobj.textAreaPlainText, plainTextEditor);

			// Add Template In Folder
			fc.utobj().selectDropDownByValue(driver, pobj.addFolder, "addFolder");

			fc.commonMethods().switch_frameClass_newLayoutcboxIframe(driver);

			String folderName = fc.utobj().generateTestData(dataSet.get("folderName"));
			String folderAccessibility = dataSet.get("folderAccessibility");
			fc.utobj().sendKeys(driver, pobj.folderName, folderName);
			fc.utobj().selectDropDown(driver, pobj.folderAccessibility, folderAccessibility);

			fc.utobj().clickElement(driver, pobj.folderDescriptionLink);
			String folderDescription = fc.utobj().generateTestData(dataSet.get("folderDescription"));
			fc.utobj().sendKeys(driver, pobj.folderSummary, folderDescription);
			fc.utobj().clickElement(driver, pobj.saveBtn);
			fc.utobj().switchFrameToDefault(driver);
			fc.utobj().clickElement(driver, pobj.addAttchmentCircle);

			String fileName = dataSet.get("fileName");
			String filepath = fc.utobj().getFilePathFromTestData(fileName);
			fc.utobj().sendKeys(driver, pobj.attachmentName, filepath);
			fc.utobj().clickElement(driver, pobj.attachmentButton);
			fc.utobj().clickElement(driver, pobj.doneBtn);

			fc.utobj().printTestStep("Verify The Template At Corporate Templates Tab");
			fc.utobj().clickElement(driver, pobj.corporateTemplatesTab);
			campaignCenterPage.searchTemplateByFilter(driver, templateName);

			boolean isTemplatePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () , '" + templateName + "')]");
			if (isTemplatePresent == false) {
				fc.utobj().throwsException("was not able to verify Template At Corporate Template Tab");
			}

			boolean isFolderNamePresentAtCorporateTab = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[contains(text () , '"
					+ templateName + "')]/ancestor::tr/td[contains(text () , '" + folderName + "')]");
			if (isFolderNamePresentAtCorporateTab == false) {
				fc.utobj().throwsException("was not able to verify Folder At Corporate Template Tab");
			}

			fc.utobj().printTestStep("Verify The Template At Folders Tab");
			fc.utobj().clickElement(driver, pobj.foldersTab);
			campaignCenterPage.searchFoldersFilter(driver, folderName);

			boolean isFolderNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () , '" + folderName + "')]");
			if (isFolderNamePresent == false) {
				fc.utobj().throwsException("Was not able to verify Folder At Folders Tab");
			}

			fc.utobj().printTestStep("Verify The Template In Folder");
			fc.utobj().clickElement(driver, driver
					.findElement(By.xpath(".//td[contains(text () , '" + folderName + "')]/ancestor::tr/td/a[.='1']")));

			boolean isTemplatePresentAtCorporateTab = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () , '" + templateName + "')]");
			if (isTemplatePresentAtCorporateTab == false) {
				fc.utobj().throwsException(
						"was not able to verify Template At Corporate Template Tab Through Folder Tab");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "crm")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Modify Corporate Template At CRM > Campaign Center > Email Templates", testCaseId = "TC_29_Modify_Corporate_Template")
	private void modifyCorporateTemplate() throws IOException, Exception, ParserConfigurationException, SAXException {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			CRMCampaignCenterEmailTemplatesPage pobj = new CRMCampaignCenterEmailTemplatesPage(driver);
			CRMCampaignCenterPageTest campaignCenterPage = new CRMCampaignCenterPageTest();
			CRMGroupsPageTest groupPage = new CRMGroupsPageTest();

			fc.utobj().printTestStep("Navigate To CRM > Campaign Center > Email Template");

			String templateName = fc.utobj().generateTestData(dataSet.get("templateName"));
			String emailSubject = fc.utobj().generateTestData(dataSet.get("emailSubject"));
			String plainTextEditor = fc.utobj().generateTestData(dataSet.get("plainTextEditor"));
			String accessibility = dataSet.get("accessibility");
			String emailType = dataSet.get("emailType");

			fc.utobj().printTestStep("Create Corporate Template");
			createCodeYourTemplate(driver, config, dataSet, accessibility, emailType, templateName, emailSubject,
					plainTextEditor);

			fc.utobj().printTestStep("Modify The Corporate Template");
			fc.utobj().clickElement(driver, pobj.corporateTemplatesTab);
			campaignCenterPage.searchTemplateByFilter(driver, templateName);
			groupPage.actionImgOption(driver, templateName, "Modify");

			templateName = fc.utobj().generateTestData(dataSet.get("templateName"));
			emailSubject = fc.utobj().generateTestData(dataSet.get("emailSubject"));
			plainTextEditor = fc.utobj().generateTestData(dataSet.get("plainTextEditor"));
			fc.utobj().sendKeys(driver, pobj.templateName, templateName);
			fc.utobj().sendKeys(driver, pobj.mailSubject, emailSubject);
			fc.utobj().sendKeys(driver, pobj.textAreaPlainText, plainTextEditor);
			fc.utobj().clickElement(driver, pobj.addBtn);

			fc.utobj().printTestStep("Verify The Template At Corporate Templates Tab");
			fc.utobj().clickElement(driver, pobj.corporateTemplatesTab);
			campaignCenterPage.searchTemplateByFilter(driver, templateName);

			boolean isTemplatePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () , '" + templateName + "')]");
			if (isTemplatePresent == false) {
				fc.utobj().throwsException("was not able to verify Template At Corporate Template Tab");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "crm")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Delete Corporate Template At CRM > Campaign Center > Email Templates", testCaseId = "TC_30_Delete_Corporate_Template")
	private void deleteCorporateTemplate() throws IOException, Exception, ParserConfigurationException, SAXException {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			CRMCampaignCenterEmailTemplatesPage pobj = new CRMCampaignCenterEmailTemplatesPage(driver);
			CRMCampaignCenterPageTest campaignCenterPage = new CRMCampaignCenterPageTest();
			CRMGroupsPageTest groupPage = new CRMGroupsPageTest();

			fc.utobj().printTestStep("Navigate To CRM > Campaign Center > Email Template");

			String templateName = fc.utobj().generateTestData(dataSet.get("templateName"));
			String emailSubject = fc.utobj().generateTestData(dataSet.get("emailSubject"));
			String plainTextEditor = fc.utobj().generateTestData(dataSet.get("plainTextEditor"));
			String accessibility = dataSet.get("accessibility");
			String emailType = dataSet.get("emailType");

			fc.utobj().printTestStep("Create Corporate Template");
			createCodeYourTemplate(driver, config, dataSet, accessibility, emailType, templateName, emailSubject,
					plainTextEditor);

			fc.utobj().printTestStep("Delete The Corporate Template");
			fc.utobj().clickElement(driver, pobj.corporateTemplatesTab);
			campaignCenterPage.searchTemplateByFilter(driver, templateName);
			groupPage.actionImgOption(driver, templateName, "Delete");
			fc.utobj().acceptAlertBox(driver);

			fc.utobj().printTestStep("Verify The Template At Corporate Templates Tab");
			fc.utobj().clickElement(driver, pobj.corporateTemplatesTab);
			campaignCenterPage.searchTemplateByFilter(driver, templateName);

			boolean isTemplatePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//td[contains(text () , 'No Records found.')]");
			if (isTemplatePresent == false) {
				fc.utobj().throwsException("was not able to verify Template At Corporate Template Tab");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "crm", "crmtempdr" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Archive And Unarchive Corporate Template At CRM > Campaign Center > Email Templates", testCaseId = "TC_31_Archive_Corporate_Template")
	private void archiveCorporateTemplate() throws IOException, Exception, ParserConfigurationException, SAXException {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			CRMCampaignCenterEmailTemplatesPage pobj = new CRMCampaignCenterEmailTemplatesPage(driver);
			CRMCampaignCenterPageTest campaignCenterPage = new CRMCampaignCenterPageTest();
			CRMGroupsPageTest groupPage = new CRMGroupsPageTest();

			fc.utobj().printTestStep("Navigate To CRM > Campaign Center > Email Template");

			String templateName = fc.utobj().generateTestData(dataSet.get("templateName"));
			String emailSubject = fc.utobj().generateTestData(dataSet.get("emailSubject"));
			String plainTextEditor = fc.utobj().generateTestData(dataSet.get("plainTextEditor"));
			String accessibility = dataSet.get("accessibility");
			String emailType = dataSet.get("emailType");

			fc.utobj().printTestStep("Create Corporate Template");
			createCodeYourTemplate(driver, config, dataSet, accessibility, emailType, templateName, emailSubject,
					plainTextEditor);

			fc.utobj().printTestStep("Archive The Corporate Template");
			fc.utobj().clickElement(driver, pobj.corporateTemplatesTab);
			campaignCenterPage.searchTemplateByFilter(driver, templateName);
			groupPage.actionImgOption(driver, templateName, "Archive");
			fc.utobj().acceptAlertBox(driver);

			fc.commonMethods().switch_frameClass_newLayoutcboxIframe(driver);
			fc.utobj().clickElement(driver, pobj.closeBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Verify The Archive Template At Corporate Templates Tab");
			fc.utobj().clickElement(driver, pobj.corporateTemplatesTab);
			campaignCenterPage.searchTemplateByFilter(driver, templateName);

			boolean isTemplatePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//td[contains(text () , 'No Records found.')]");
			if (isTemplatePresent == false) {
				fc.utobj().throwsException("was not able to verify Template At Corporate Template Tab");
			}

			fc.utobj().printTestStep("Verify The Archived Template At Archived Tab");
			fc.utobj().clickElement(driver, pobj.archivedTab);

			campaignCenterPage.searchTemplateByFilter(driver, templateName);

			boolean isTemplatePresentArchived = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () , '" + templateName + "')]");
			if (isTemplatePresentArchived == false) {
				fc.utobj().throwsException("was not able to verify Template At Archived Tab");
			}

			fc.utobj().printTestStep("Unarchive The Corporate Template");
			groupPage.actionImgOption(driver, templateName, "Unarchive");
			fc.utobj().acceptAlertBox(driver);
			fc.commonMethods().switch_frameClass_newLayoutcboxIframe(driver);
			fc.utobj().clickElement(driver, pobj.closeBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Verify The UnArchive Template At Corporate Templates Tab");
			fc.utobj().clickElement(driver, pobj.corporateTemplatesTab);
			campaignCenterPage.searchTemplateByFilter(driver, templateName);

			boolean isTemplatePresentUnArchive = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () , '" + templateName + "')]");
			if (isTemplatePresentUnArchive == false) {
				fc.utobj().throwsException("was not able to verify Template At Corporate Template Tab");
			}

			fc.utobj().printTestStep("Verify The UnArchived Template At Archived Tab");
			fc.utobj().clickElement(driver, pobj.archivedTab);
			campaignCenterPage.searchTemplateByFilter(driver, templateName);

			boolean isTemplatePresentUnArchived = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () , 'No Records found.')]");
			if (isTemplatePresentUnArchived == false) {
				fc.utobj().throwsException("was not able to verify Template At Archived Tab");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "crm", "crmemail"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Test Corporate Template At CRM > Campaign Center > Email Templates", testCaseId = "TC_32_Test_Corporate_Template")
	private void testCorporateTemplate() throws IOException, Exception, ParserConfigurationException, SAXException {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			CRMCampaignCenterEmailTemplatesPage pobj = new CRMCampaignCenterEmailTemplatesPage(driver);
			CRMCampaignCenterPageTest campaignCenterPage = new CRMCampaignCenterPageTest();
			CRMGroupsPageTest groupPage = new CRMGroupsPageTest();

			fc.utobj().printTestStep("Navigate To CRM > Campaign Center > Email Template");

			String templateName = fc.utobj().generateTestData(dataSet.get("templateName"));
			String emailSubject = fc.utobj().generateTestData(dataSet.get("emailSubject"));
			String plainTextEditor = fc.utobj().generateTestData(dataSet.get("plainTextEditor"));
			String accessibility = dataSet.get("accessibility");
			String emailType = "Graphical"; //dataSet.get("Graphical");

			fc.utobj().printTestStep("Create Corporate Template");
			createCodeYourTemplate(driver, config, dataSet, accessibility, emailType, templateName, emailSubject,
					plainTextEditor);

			fc.utobj().printTestStep("Test The Corporate Template");
			fc.utobj().clickElement(driver, pobj.corporateTemplatesTab);
			campaignCenterPage.searchTemplateByFilter(driver, templateName);
			groupPage.actionImgOption(driver, templateName, "Test Template");
			String emailId = "crmautomation@staffex.com";
			fc.commonMethods().switch_frameClass_newLayoutcboxIframe(driver);
			fc.utobj().sleep(2000);
			fc.utobj().sendKeys(driver, pobj.testEmailField, emailId);
			fc.utobj().clickElement(driver, pobj.testButton);
			fc.utobj().clickElement(driver, pobj.closeBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Verify The Template Details In Email");

			Map<String, String> mailData = new HashMap<String, String>();

			mailData = fc.utobj().readMailBox(emailSubject, plainTextEditor, emailId, "sdg@1a@Hfs");

			if (mailData.size() == 0 || !mailData.get("mailBody").contains(plainTextEditor)) {

				fc.utobj().throwsException("was not able to verify Test Template Mail");

			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "crm")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Copy And Customize Corporate Template At CRM > Campaign Center > Email Templates", testCaseId = "TC_33_Copy_And_Customize_Corporate_Template")
	private void copyAndCustomizeCorporateTemplate()
			throws IOException, Exception, ParserConfigurationException, SAXException {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			CRMCampaignCenterEmailTemplatesPage pobj = new CRMCampaignCenterEmailTemplatesPage(driver);
			CRMCampaignCenterPageTest campaignCenterPage = new CRMCampaignCenterPageTest();
			CRMGroupsPageTest groupPage = new CRMGroupsPageTest();

			fc.utobj().printTestStep("Navigate To CRM > Campaign Center > Email Template");

			String templateName = fc.utobj().generateTestData(dataSet.get("templateName"));
			String emailSubject = fc.utobj().generateTestData(dataSet.get("emailSubject"));
			String plainTextEditor = fc.utobj().generateTestData(dataSet.get("plainTextEditor"));
			String accessibility = dataSet.get("accessibility");
			String emailType = dataSet.get("emailType");

			fc.utobj().printTestStep("Create Corporate Template");
			createCodeYourTemplate(driver, config, dataSet, accessibility, emailType, templateName, emailSubject,
					plainTextEditor);

			fc.utobj().printTestStep("Copy And Customize The Corporate Template");
			fc.utobj().clickElement(driver, pobj.corporateTemplatesTab);
			campaignCenterPage.searchTemplateByFilter(driver, templateName);
			groupPage.actionImgOption(driver, templateName, "Copy and Customize");

			templateName = fc.utobj().generateTestData(dataSet.get("templateName"));
			emailSubject = fc.utobj().generateTestData(dataSet.get("emailSubject"));
			fc.utobj().sendKeys(driver, pobj.templateName, templateName);
			fc.utobj().sendKeys(driver, pobj.mailSubject, emailSubject);
			fc.utobj().clickElement(driver, pobj.addBtn);

			fc.utobj().printTestStep("Verify The Template At Corporate Templates Tab");
			fc.utobj().clickElement(driver, pobj.corporateTemplatesTab);
			campaignCenterPage.searchTemplateByFilter(driver, templateName);

			boolean isTemplatePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () , '" + templateName + "')]");
			if (isTemplatePresent == false) {
				fc.utobj().throwsException("was not able to verify Template At Corporate Template Tab");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "crm")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Create Location Template At CRM > Campaign Center > Email Templates", testCaseId = "TC_34_Create_Location_Template")
	private void createLocationTemplate() throws IOException, Exception, ParserConfigurationException, SAXException {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			CRMCampaignCenterEmailTemplatesPage pobj = new CRMCampaignCenterEmailTemplatesPage(driver);
			CRMCampaignCenterPageTest campaignCenterPage = new CRMCampaignCenterPageTest();

			fc.utobj().printTestStep("Navigate To Admin > Franchise Location > Add Franchise Location");
			fc.utobj().printTestStep("Add Franchise Location");
			String franchiseId = fc.utobj().generateTestData(dataSet.get("franchiseId"));
			String regionName = fc.utobj().generateTestData(dataSet.get("regionName"));
			String firstName = fc.utobj().generateTestData(dataSet.get("firstName"));
			String lastName = fc.utobj().generateTestData(dataSet.get("lastName"));
			AdminFranchiseLocationAddFranchiseLocationPageTest franchiseLocationPage = new AdminFranchiseLocationAddFranchiseLocationPageTest();
			franchiseLocationPage.addFranchiseLocationForMkt(driver, franchiseId, regionName, firstName, lastName);

			fc.utobj().printTestStep("Add Franchise User");
			String userName = fc.utobj().generateTestData(dataSet.get("fuserName"));
			String password = dataSet.get("password");
			String roleName = dataSet.get("roleName");
			AdminUsersManageManageFranchiseUsersPageTest franchiseUserPage = new AdminUsersManageManageFranchiseUsersPageTest();
			String emailId = "crmautomation@staffex.com";
			franchiseUserPage.addFranchiseUser(driver, userName, password, franchiseId, roleName, emailId);

			fc.home_page().logout(driver);

			fc.utobj().printTestStep("Login With Franchise User");
			fc.loginpage().loginWithParameter(driver, userName, password);

			fc.utobj().printTestStep("Navigate To CRM > Campaign Center > Email Template");
			fc.crm().crm_common().CRMCampaignCenterLnk(driver);
			fc.utobj().clickElement(driver, pobj.dashBoradLnk);
			fc.utobj().clickElement(driver, pobj.templateLnk);

			fc.utobj().printTestStep("Create Template");
			fc.utobj().clickElement(driver, pobj.createLink);
			fc.utobj().clickElement(driver, pobj.templateLink);

			fc.utobj().printTestStep("Code Your Own Template");
			// Code Your Template
			String templateName = fc.utobj().generateTestData(dataSet.get("templateName"));
			String emailSubject = fc.utobj().generateTestData(dataSet.get("emailSubject"));
			String plainTextEditor = fc.utobj().generateTestData(dataSet.get("plainTextEditor"));

			fc.utobj().printTestStep("Code Your Own Template");
			fc.utobj().clickElement(driver, pobj.codeYourOwn);
			fc.utobj().sendKeys(driver, pobj.templateName, templateName);
			fc.utobj().sendKeys(driver, pobj.mailSubject, emailSubject);
			fc.utobj().clickElement(driver, pobj.publicToAllMyFranchiseLocation);
			fc.utobj().clickElement(driver, pobj.plainTextEmailType);
			fc.utobj().sendKeys(driver, pobj.textAreaPlainText, plainTextEditor);

			// Add Template In Folder
			fc.utobj().selectDropDownByValue(driver, pobj.addFolder, "addFolder");

			fc.commonMethods().switch_frameClass_newLayoutcboxIframe(driver);

			String folderName = fc.utobj().generateTestData(dataSet.get("folderName"));
			String folderAccessibility = dataSet.get("folderAccessibility");
			fc.utobj().sendKeys(driver, pobj.folderName, folderName);
			fc.utobj().selectDropDown(driver, pobj.folderAccessibility, folderAccessibility);
			fc.utobj().clickElement(driver, pobj.folderDescriptionLink);
			String folderDescription = fc.utobj().generateTestData(dataSet.get("folderDescription"));
			fc.utobj().sendKeys(driver, pobj.folderSummary, folderDescription);
			fc.utobj().clickElement(driver, pobj.saveBtn);
			fc.utobj().switchFrameToDefault(driver);
			fc.utobj().clickElement(driver, pobj.addAttchmentCircle);

			String fileName = dataSet.get("fileName");
			String filepath = fc.utobj().getFilePathFromTestData(fileName);
			fc.utobj().sendKeys(driver, pobj.attachmentName, filepath);
			fc.utobj().clickElement(driver, pobj.attachmentButton);
			fc.utobj().clickElement(driver, pobj.doneBtn);

			fc.utobj().printTestStep("Verify The Template At Location Tab");
			fc.utobj().clickElement(driver, pobj.locationTemplateTab);
			campaignCenterPage.searchTemplateByFilter(driver, templateName);

			boolean isTemplatePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () , '" + templateName + "')]");
			if (isTemplatePresent == false) {
				fc.utobj().throwsException("was not able to verify Template At Location Template Tab");
			}

			boolean isFolderNamePresentAtCorporateTab = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[contains(text () , '"
					+ templateName + "')]/ancestor::tr/td[contains(text () , '" + folderName + "')]");
			if (isFolderNamePresentAtCorporateTab == false) {
				fc.utobj().throwsException("was not able to verify Folder At Location Template Tab");
			}

			fc.utobj().printTestStep("Verify The Template At Folders Tab");
			fc.utobj().clickElement(driver, pobj.foldersTab);
			campaignCenterPage.searchFoldersFilter(driver, folderName);

			boolean isFolderNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () , '" + folderName + "')]");
			if (isFolderNamePresent == false) {
				fc.utobj().throwsException("Was not able to verify Folder At Folders Tab");
			}

			fc.utobj().printTestStep("Verify The Template In Folder");
			fc.utobj().clickElement(driver, driver
					.findElement(By.xpath(".//td[contains(text () , '" + folderName + "')]/ancestor::tr/td/a[.='1']")));

			boolean isTemplatePresentAtCorporateTab = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () , '" + templateName + "')]");
			if (isTemplatePresentAtCorporateTab == false) {
				fc.utobj().throwsException("was not able to verify Template In Folder Tab");
			}

			fc.home_page().logout(driver);

			fc.utobj().printTestStep("Login With Administrator User");
			fc.loginpage().loginWithParameter(driver, config.get("userName"), config.get("password"));

			fc.utobj().printTestStep("Navigate To CRM > Campaign Center > Email Template");
			fc.crm().crm_common().CRMCampaignCenterLnk(driver);
			fc.utobj().clickElement(driver, pobj.dashBoradLnk);
			fc.utobj().clickElement(driver, pobj.templateLnk);

			fc.utobj().printTestStep("Verify The Template At Location Tab");
			fc.utobj().clickElement(driver, pobj.locationTemplateTab);
			campaignCenterPage.searchTemplateByFilter(driver, templateName);

			boolean isTemplatePresent1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () , '" + templateName + "')]");
			if (isTemplatePresent1 == false) {
				fc.utobj().throwsException("was not able to verify Template At Location Template Tab");
			}

			boolean isFolderNamePresentAtCorporateTab1 = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[contains(text () , '"
					+ templateName + "')]/ancestor::tr/td[contains(text () , '" + folderName + "')]");
			if (isFolderNamePresentAtCorporateTab1 == false) {
				fc.utobj().throwsException("was not able to verify Folder At Location Template Tab");
			}

			fc.utobj().printTestStep("Verify The Template At Folders Tab");
			fc.utobj().clickElement(driver, pobj.foldersTab);
			campaignCenterPage.searchFoldersFilter(driver, folderName);

			boolean isFolderNamePresent1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () , '" + folderName + "')]");
			if (isFolderNamePresent1 == false) {
				fc.utobj().throwsException("Was not able to verify Folder At Folders Tab");
			}

			fc.utobj().printTestStep("Verify The Template In Folder");
			fc.utobj().clickElement(driver, driver
					.findElement(By.xpath(".//td[contains(text () , '" + folderName + "')]/ancestor::tr/td/a[.='1']")));

			boolean isTemplatePresentAtCorporateTab1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () , '" + templateName + "')]");
			if (isTemplatePresentAtCorporateTab1 == false) {
				fc.utobj().throwsException("was not able to verify Template In Folder");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "crm")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Modify Location Template At CRM > Campaign Center > Email Templates", testCaseId = "TC_35_Modify_Location_Template")
	private void modifyLocationTemplate() throws IOException, Exception, ParserConfigurationException, SAXException {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			CRMCampaignCenterEmailTemplatesPage pobj = new CRMCampaignCenterEmailTemplatesPage(driver);
			CRMCampaignCenterPageTest campaignCenterPage = new CRMCampaignCenterPageTest();
			CRMGroupsPageTest groupPage = new CRMGroupsPageTest();

			fc.utobj().printTestStep("Navigate To Admin > Franchise Location > Add Franchise Location");
			fc.utobj().printTestStep("Add Franchise Location");
			String franchiseId = fc.utobj().generateTestData(dataSet.get("franchiseId"));
			String regionName = fc.utobj().generateTestData(dataSet.get("regionName"));
			String firstName = fc.utobj().generateTestData(dataSet.get("firstName"));
			String lastName = fc.utobj().generateTestData(dataSet.get("lastName"));
			AdminFranchiseLocationAddFranchiseLocationPageTest franchiseLocationPage = new AdminFranchiseLocationAddFranchiseLocationPageTest();
			franchiseLocationPage.addFranchiseLocationForMkt(driver, franchiseId, regionName, firstName, lastName);

			fc.utobj().printTestStep("Add Franchise User");
			String userName = fc.utobj().generateTestData(dataSet.get("fuserName"));
			String password = dataSet.get("password");
			String roleName = dataSet.get("roleName");
			AdminUsersManageManageFranchiseUsersPageTest franchiseUserPage = new AdminUsersManageManageFranchiseUsersPageTest();
			String emailId = "crmautomation@staffex.com";
			franchiseUserPage.addFranchiseUser(driver, userName, password, franchiseId, roleName, emailId);

			fc.home_page().logout(driver);

			fc.utobj().printTestStep("Login With Franchise User");
			fc.loginpage().loginWithParameter(driver, userName, password);

			fc.utobj().printTestStep("Navigate To CRM > Campaign Center > Email Template");
			String templateName = fc.utobj().generateTestData(dataSet.get("templateName"));
			String emailSubject = fc.utobj().generateTestData(dataSet.get("emailSubject"));
			String plainTextEditor = fc.utobj().generateTestData(dataSet.get("plainTextEditor"));
			String accessibility = dataSet.get("accessibility");
			String emailType = dataSet.get("emailType");
			fc.utobj().printTestStep("Create Location Template");
			createCodeYourTemplate(driver, config, dataSet, accessibility, emailType, templateName, emailSubject,
					plainTextEditor);

			fc.home_page().logout(driver);

			fc.utobj().printTestStep("Login With Administrator User");
			fc.loginpage().loginWithParameter(driver, config.get("userName"), config.get("password"));

			fc.utobj().printTestStep("Navigate To CRM > Campaign Center > Email Template");
			fc.crm().crm_common().CRMCampaignCenterLnk(driver);
			fc.utobj().clickElement(driver, pobj.dashBoradLnk);
			fc.utobj().clickElement(driver, pobj.templateLnk);

			fc.utobj().printTestStep("Modify The Template");
			fc.utobj().clickElement(driver, pobj.locationTemplateTab);
			campaignCenterPage.searchTemplateByFilter(driver, templateName);
			groupPage.actionImgOption(driver, templateName, "Modify");

			templateName = fc.utobj().generateTestData(dataSet.get("templateName"));
			emailSubject = fc.utobj().generateTestData(dataSet.get("emailSubject"));
			plainTextEditor = fc.utobj().generateTestData(dataSet.get("plainTextEditor"));
			fc.utobj().sendKeys(driver, pobj.templateName, templateName);
			fc.utobj().sendKeys(driver, pobj.mailSubject, emailSubject);
			fc.utobj().sendKeys(driver, pobj.textAreaPlainText, plainTextEditor);
			fc.utobj().clickElement(driver, pobj.addBtn);

			fc.utobj().printTestStep("Verify The Template At Location Templates Tab");
			fc.utobj().clickElement(driver, pobj.locationTemplateTab);
			campaignCenterPage.searchTemplateByFilter(driver, templateName);

			boolean isTemplatePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () , '" + templateName + "')]");
			if (isTemplatePresent == false) {
				fc.utobj().throwsException("was not able to verify Template At Location Template Tab");
			}

			fc.home_page().logout(driver);
			fc.utobj().printTestStep("Login With Franchise User");
			fc.loginpage().loginWithParameter(driver, userName, password);

			fc.utobj().printTestStep("Verify The Template At Franchise User Side");
			fc.utobj().printTestStep("Navigate To CRM > Campaign Center > Email Template");
			fc.crm().crm_common().CRMCampaignCenterLnk(driver);
			fc.utobj().clickElement(driver, pobj.dashBoradLnk);
			fc.utobj().clickElement(driver, pobj.templateLnk);

			fc.utobj().clickElement(driver, pobj.locationTemplateTab);
			campaignCenterPage.searchTemplateByFilter(driver, templateName);

			boolean isTemplatePresentAtFranchise = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () , '" + templateName + "')]");
			if (isTemplatePresentAtFranchise == false) {
				fc.utobj().throwsException("was not able to verify Template At Location Template Tab");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = {"crm"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Delete Location Template At CRM > Campaign Center > Email Templates", testCaseId = "TC_36_Delete_Location_Template")
	private void deleteLocationTemplate() throws IOException, Exception, ParserConfigurationException, SAXException {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			CRMCampaignCenterEmailTemplatesPage pobj = new CRMCampaignCenterEmailTemplatesPage(driver);
			CRMCampaignCenterPageTest campaignCenterPage = new CRMCampaignCenterPageTest();
			CRMGroupsPageTest groupPage = new CRMGroupsPageTest();

			fc.utobj().printTestStep("Navigate To Admin > Franchise Location > Add Franchise Location");
			fc.utobj().printTestStep("Add Franchise Location");
			String franchiseId = fc.utobj().generateTestData(dataSet.get("franchiseId"));
			String regionName = fc.utobj().generateTestData(dataSet.get("regionName"));
			String firstName = fc.utobj().generateTestData(dataSet.get("firstName"));
			String lastName = fc.utobj().generateTestData(dataSet.get("lastName"));
			AdminFranchiseLocationAddFranchiseLocationPageTest franchiseLocationPage = new AdminFranchiseLocationAddFranchiseLocationPageTest();
			franchiseLocationPage.addFranchiseLocationForMkt(driver, franchiseId, regionName, firstName, lastName);

			fc.utobj().printTestStep("Add Franchise User");
			String userName = fc.utobj().generateTestData(dataSet.get("fuserName"));
			String password = dataSet.get("password");
			String roleName = dataSet.get("roleName");
			AdminUsersManageManageFranchiseUsersPageTest franchiseUserPage = new AdminUsersManageManageFranchiseUsersPageTest();
			String emailId = "crmautomation@staffex.com";
			franchiseUserPage.addFranchiseUser(driver, userName, password, franchiseId, roleName, emailId);

			fc.home_page().logout(driver);

			fc.utobj().printTestStep("Login With Franchise User");
			fc.loginpage().loginWithParameter(driver, userName, password);

			fc.utobj().printTestStep("Navigate To CRM > Campaign Center > Email Template");
			String templateName = fc.utobj().generateTestData(dataSet.get("templateName"));
			String emailSubject = fc.utobj().generateTestData(dataSet.get("emailSubject"));
			String plainTextEditor = fc.utobj().generateTestData(dataSet.get("plainTextEditor"));
			String accessibility = dataSet.get("accessibility");
			String emailType = dataSet.get("emailType");
			fc.utobj().printTestStep("Create Location Template");
			createCodeYourTemplate(driver, config, dataSet, accessibility, emailType, templateName, emailSubject,
					plainTextEditor);

			fc.home_page().logout(driver);

			fc.utobj().printTestStep("Login With Administrator User");
			fc.loginpage().loginWithParameter(driver, config.get("userName"), config.get("password"));

			fc.utobj().printTestStep("Navigate To CRM > Campaign Center > Email Template");
			fc.crm().crm_common().CRMCampaignCenterLnk(driver);
			fc.utobj().clickElement(driver, pobj.dashBoradLnk);
			fc.utobj().clickElement(driver, pobj.templateLnk);

			fc.utobj().printTestStep("Modify The Template");
			fc.utobj().clickElement(driver, pobj.locationTemplateTab);
			campaignCenterPage.searchTemplateByFilter(driver, templateName);
			groupPage.actionImgOption(driver, templateName, "Delete");
			fc.utobj().acceptAlertBox(driver);

			fc.utobj().printTestStep("Verify The Template At Location Templates Tab");
			fc.utobj().clickElement(driver, pobj.locationTemplateTab);
			campaignCenterPage.searchTemplateByFilter(driver, templateName);

			boolean isTemplatePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//td[contains(text () , 'No Records found.')]");
			if (isTemplatePresent == false) {
				fc.utobj().throwsException("was not able to verify Template At Location Template Tab");
			}

			fc.home_page().logout(driver);
			fc.utobj().printTestStep("Login With Franchise User");
			fc.loginpage().loginWithParameter(driver, userName, password);

			fc.utobj().printTestStep("Verify The Template At Franchise User Side");
			fc.utobj().printTestStep("Navigate To CRM > Campaign Center > Email Template");
			fc.crm().crm_common().CRMCampaignCenterLnk(driver);
			fc.utobj().clickElement(driver, pobj.dashBoradLnk);
			fc.utobj().clickElement(driver, pobj.templateLnk);

			fc.utobj().clickElement(driver, pobj.locationTemplateTab);
			campaignCenterPage.searchTemplateByFilter(driver, templateName);

			boolean isTemplatePresentAtFranchise = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () , 'No Records found.')]");
			if (isTemplatePresentAtFranchise == false) {
				fc.utobj().throwsException("was not able to verify Template At Location Template Tab");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "crm")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Archive Location Template At CRM > Campaign Center > Email Templates", testCaseId = "TC_37_Archive_Location_Template")
	private void archiveLocationTemplate() throws IOException, Exception, ParserConfigurationException, SAXException {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			CRMCampaignCenterEmailTemplatesPage pobj = new CRMCampaignCenterEmailTemplatesPage(driver);
			CRMCampaignCenterPageTest campaignCenterPage = new CRMCampaignCenterPageTest();
			CRMGroupsPageTest groupPage = new CRMGroupsPageTest();

			fc.utobj().printTestStep("Navigate To Admin > Franchise Location > Add Franchise Location");
			fc.utobj().printTestStep("Add Franchise Location");
			String franchiseId = fc.utobj().generateTestData(dataSet.get("franchiseId"));
			String regionName = fc.utobj().generateTestData(dataSet.get("regionName"));
			String firstName = fc.utobj().generateTestData(dataSet.get("firstName"));
			String lastName = fc.utobj().generateTestData(dataSet.get("lastName"));
			AdminFranchiseLocationAddFranchiseLocationPageTest franchiseLocationPage = new AdminFranchiseLocationAddFranchiseLocationPageTest();
			franchiseLocationPage.addFranchiseLocationForMkt(driver, franchiseId, regionName, firstName, lastName);

			fc.utobj().printTestStep("Add Franchise User");
			String userName = fc.utobj().generateTestData(dataSet.get("fuserName"));
			String password = dataSet.get("password");
			String roleName = dataSet.get("roleName");
			String emailId = "crmautomation@staffex.com";
			AdminUsersManageManageFranchiseUsersPageTest franchiseUserPage = new AdminUsersManageManageFranchiseUsersPageTest();
			franchiseUserPage.addFranchiseUser(driver, userName, password, franchiseId, roleName, emailId);

			fc.home_page().logout(driver);

			fc.utobj().printTestStep("Login With Franchise User");
			fc.loginpage().loginWithParameter(driver, userName, password);

			fc.utobj().printTestStep("Navigate To CRM > Campaign Center > Email Template");
			String templateName = fc.utobj().generateTestData(dataSet.get("templateName"));
			String emailSubject = fc.utobj().generateTestData(dataSet.get("emailSubject"));
			String plainTextEditor = fc.utobj().generateTestData(dataSet.get("plainTextEditor"));
			String accessibility = dataSet.get("accessibility");
			String emailType = dataSet.get("emailType");
			fc.utobj().printTestStep("Create Location Template");
			createCodeYourTemplate(driver, config, dataSet, accessibility, emailType, templateName, emailSubject,
					plainTextEditor);

			fc.home_page().logout(driver);

			fc.utobj().printTestStep("Login With Administrator User");
			fc.loginpage().loginWithParameter(driver, config.get("userName"), config.get("password"));

			fc.utobj().printTestStep("Navigate To CRM > Campaign Center > Email Template");
			fc.crm().crm_common().CRMCampaignCenterLnk(driver);
			fc.utobj().clickElement(driver, pobj.dashBoradLnk);
			fc.utobj().clickElement(driver, pobj.templateLnk);

			fc.utobj().printTestStep("Modify The Template");
			fc.utobj().clickElement(driver, pobj.locationTemplateTab);
			campaignCenterPage.searchTemplateByFilter(driver, templateName);
			groupPage.actionImgOption(driver, templateName, "Archive");
			fc.utobj().acceptAlertBox(driver);

			fc.commonMethods().switch_frameClass_newLayoutcboxIframe(driver);
			fc.utobj().clickElement(driver, pobj.closeBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Verify The Template At Location Templates Tab");
			fc.utobj().clickElement(driver, pobj.locationTemplateTab);
			campaignCenterPage.searchTemplateByFilter(driver, templateName);

			boolean isTemplatePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//td[contains(text () , 'No Records found.')]");
			if (isTemplatePresent == false) {
				fc.utobj().throwsException("was not able to verify Template At Location Template Tab");
			}

			fc.utobj().printTestStep("Verify The Template At Archived Templates Tab");
			fc.utobj().clickElement(driver, pobj.archivedTab);
			campaignCenterPage.searchTemplateByFilter(driver, templateName);

			boolean isTemplatePresentArchivedTab = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () , '" + templateName + "')]");
			if (isTemplatePresentArchivedTab == false) {
				fc.utobj().throwsException("was not able to verify Template At Location Template Tab");
			}

			fc.utobj().printTestStep("Unarchive The Location Template");
			groupPage.actionImgOption(driver, templateName, "Unarchive");
			fc.utobj().acceptAlertBox(driver);
			fc.commonMethods().switch_frameClass_newLayoutcboxIframe(driver);
			fc.utobj().clickElement(driver, pobj.closeBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Verify The UnArchive Template At Location Templates Tab");
			fc.utobj().clickElement(driver, pobj.locationTemplateTab);
			campaignCenterPage.searchTemplateByFilter(driver, templateName);

			boolean isTemplatePresentUnArchive = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () , '" + templateName + "')]");
			if (isTemplatePresentUnArchive == false) {
				fc.utobj().throwsException("was not able to verify Template At Location Template Tab");
			}

			fc.utobj().printTestStep("Verify The UnArchived Template At Archived Tab");
			fc.utobj().clickElement(driver, pobj.archivedTab);
			campaignCenterPage.searchTemplateByFilter(driver, templateName);

			boolean isTemplatePresentUnArchived = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () , 'No Records found.')]");
			if (isTemplatePresentUnArchived == false) {
				fc.utobj().throwsException("was not able to verify Template At Archived Tab");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "crmcampaigncenterTest1")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Test Location Template At CRM > Campaign Center > Email Templates", testCaseId = "TC_38_Test_Location_Template")
	private void testLocationTemplate() throws IOException, Exception, ParserConfigurationException, SAXException {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			CRMCampaignCenterEmailTemplatesPage pobj = new CRMCampaignCenterEmailTemplatesPage(driver);
			CRMCampaignCenterPageTest campaignCenterPage = new CRMCampaignCenterPageTest();
			CRMGroupsPageTest groupPage = new CRMGroupsPageTest();

			fc.utobj().printTestStep("Navigate To Admin > Franchise Location > Add Franchise Location");
			fc.utobj().printTestStep("Add Franchise Location");
			String franchiseId = fc.utobj().generateTestData(dataSet.get("franchiseId"));
			String regionName = fc.utobj().generateTestData(dataSet.get("regionName"));
			String firstName = fc.utobj().generateTestData(dataSet.get("firstName"));
			String lastName = fc.utobj().generateTestData(dataSet.get("lastName"));
			AdminFranchiseLocationAddFranchiseLocationPageTest franchiseLocationPage = new AdminFranchiseLocationAddFranchiseLocationPageTest();
			franchiseLocationPage.addFranchiseLocationForMkt(driver, franchiseId, regionName, firstName, lastName);

			fc.utobj().printTestStep("Add Franchise User");
			String userName = fc.utobj().generateTestData(dataSet.get("fuserName"));
			String password = dataSet.get("password");
			String roleName = dataSet.get("roleName");
			AdminUsersManageManageFranchiseUsersPageTest franchiseUserPage = new AdminUsersManageManageFranchiseUsersPageTest();
			String emailId = "crmautomation@staffex.com";
			franchiseUserPage.addFranchiseUser(driver, userName, password, franchiseId, roleName, emailId);

			fc.home_page().logout(driver);

			fc.utobj().printTestStep("Login With Franchise User");
			fc.loginpage().loginWithParameter(driver, userName, password);

			fc.utobj().printTestStep("Navigate To CRM > Campaign Center > Email Template");
			String templateName = fc.utobj().generateTestData(dataSet.get("templateName"));
			String emailSubject = fc.utobj().generateTestData(dataSet.get("emailSubject"));
			String plainTextEditor = fc.utobj().generateTestData(dataSet.get("plainTextEditor"));
			String accessibility = dataSet.get("accessibility");
			String emailType = dataSet.get("emailType");
			fc.utobj().printTestStep("Create Location Template");
			createCodeYourTemplate(driver, config, dataSet, accessibility, emailType, templateName, emailSubject,
					plainTextEditor);

			fc.home_page().logout(driver);

			fc.utobj().printTestStep("Login With Administrator User");
			fc.loginpage().loginWithParameter(driver, config.get("userName"), config.get("password"));

			fc.utobj().printTestStep("Navigate To CRM > Campaign Center > Email Template");
			fc.crm().crm_common().CRMCampaignCenterLnk(driver);
			fc.utobj().clickElement(driver, pobj.dashBoradLnk);
			fc.utobj().clickElement(driver, pobj.templateLnk);

			fc.utobj().printTestStep("Modify The Template");
			fc.utobj().clickElement(driver, pobj.locationTemplateTab);
			campaignCenterPage.searchTemplateByFilter(driver, templateName);
			groupPage.actionImgOption(driver, templateName, "Test Template");

			fc.commonMethods().switch_frameClass_newLayoutcboxIframe(driver);
			fc.utobj().sendKeys(driver, pobj.testEmailField, emailId);
			fc.utobj().clickElement(driver, pobj.testButton);
			fc.utobj().clickElement(driver, pobj.closeBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Verify The Template Details In Email");

			// Pending

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "crm")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Copy And Customize Location Template At CRM > Campaign Center > Email Templates", testCaseId = "TC_39_Copy_And_Customize_Location_Template")
	private void copyAndCustomizeLocationTemplate()
			throws IOException, Exception, ParserConfigurationException, SAXException {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			CRMCampaignCenterEmailTemplatesPage pobj = new CRMCampaignCenterEmailTemplatesPage(driver);
			CRMCampaignCenterPageTest campaignCenterPage = new CRMCampaignCenterPageTest();
			CRMGroupsPageTest groupPage = new CRMGroupsPageTest();

			fc.utobj().printTestStep("Navigate To Admin > Franchise Location > Add Franchise Location");
			fc.utobj().printTestStep("Add Franchise Location");
			String franchiseId = fc.utobj().generateTestData(dataSet.get("franchiseId"));
			String regionName = fc.utobj().generateTestData(dataSet.get("regionName"));
			String firstName = fc.utobj().generateTestData(dataSet.get("firstName"));
			String lastName = fc.utobj().generateTestData(dataSet.get("lastName"));
			AdminFranchiseLocationAddFranchiseLocationPageTest franchiseLocationPage = new AdminFranchiseLocationAddFranchiseLocationPageTest();
			franchiseLocationPage.addFranchiseLocationForMkt(driver, franchiseId, regionName, firstName, lastName);

			fc.utobj().printTestStep("Add Franchise User");
			String userName = fc.utobj().generateTestData(dataSet.get("fuserName"));
			String password = dataSet.get("password");
			String roleName = dataSet.get("roleName");
			AdminUsersManageManageFranchiseUsersPageTest franchiseUserPage = new AdminUsersManageManageFranchiseUsersPageTest();
			String emailId = "crmautomation@staffex.com";
			franchiseUserPage.addFranchiseUser(driver, userName, password, franchiseId, roleName, emailId);

			fc.home_page().logout(driver);

			fc.utobj().printTestStep("Login With Franchise User");
			fc.loginpage().loginWithParameter(driver, userName, password);

			fc.utobj().printTestStep("Navigate To CRM > Campaign Center > Email Template");
			String templateName = fc.utobj().generateTestData(dataSet.get("templateName"));
			String emailSubject = fc.utobj().generateTestData(dataSet.get("emailSubject"));
			String plainTextEditor = fc.utobj().generateTestData(dataSet.get("plainTextEditor"));
			String accessibility = dataSet.get("accessibility");
			String emailType = dataSet.get("emailType");
			fc.utobj().printTestStep("Create Location Template");
			createCodeYourTemplate(driver, config, dataSet, accessibility, emailType, templateName, emailSubject,
					plainTextEditor);

			fc.home_page().logout(driver);

			fc.utobj().printTestStep("Login With Administrator User");
			fc.loginpage().loginWithParameter(driver, config.get("userName"), config.get("password"));

			fc.utobj().printTestStep("Navigate To CRM > Campaign Center > Email Template");
			fc.crm().crm_common().CRMCampaignCenterLnk(driver);
			fc.utobj().clickElement(driver, pobj.dashBoradLnk);
			fc.utobj().clickElement(driver, pobj.templateLnk);

			fc.utobj().printTestStep("Modify The Template");
			fc.utobj().clickElement(driver, pobj.locationTemplateTab);
			campaignCenterPage.searchTemplateByFilter(driver, templateName);
			groupPage.actionImgOption(driver, templateName, "Copy and Customize");

			templateName = fc.utobj().generateTestData(dataSet.get("templateName"));
			emailSubject = fc.utobj().generateTestData(dataSet.get("emailSubject"));
			fc.utobj().sendKeys(driver, pobj.templateName, templateName);
			fc.utobj().sendKeys(driver, pobj.mailSubject, emailSubject);
			fc.utobj().clickElement(driver, pobj.addBtn);

			fc.utobj().printTestStep("Verify The Template At Location Templates Tab");
			fc.utobj().clickElement(driver, pobj.corporateTemplatesTab);
			campaignCenterPage.searchTemplateByFilter(driver, templateName);

			boolean isTemplatePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () , '" + templateName + "')]");
			if (isTemplatePresent == false) {
				fc.utobj().throwsException("was not able to verify Template At Location Template Tab");
			}

			fc.utobj().clickElement(driver, pobj.locationTemplateTab);
			campaignCenterPage.searchTemplateByFilter(driver, templateName);

			boolean isTemplatePresentAtFranchiseLocationAd = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () , 'No Records found.')]");
			if (isTemplatePresentAtFranchiseLocationAd == false) {
				fc.utobj().throwsException("was not able to verify Template At Location Template Tab");
			}

			fc.home_page().logout(driver);
			fc.utobj().printTestStep("Login With Franchise User");
			fc.loginpage().loginWithParameter(driver, userName, password);

			fc.utobj().printTestStep("Verify The Template At Franchise User Side");
			fc.utobj().printTestStep("Navigate To CRM > Campaign Center > Email Template");
			fc.crm().crm_common().CRMCampaignCenterLnk(driver);
			fc.utobj().clickElement(driver, pobj.dashBoradLnk);
			fc.utobj().clickElement(driver, pobj.templateLnk);

			fc.utobj().clickElement(driver, pobj.corporateTemplatesTab);
			campaignCenterPage.searchTemplateByFilter(driver, templateName);

			boolean isTemplatePresentAtFranchise = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () , '" + templateName + "')]");
			if (isTemplatePresentAtFranchise == false) {
				fc.utobj().throwsException("was not able to verify Template At Corporate Template Tab");
			}

			fc.utobj().clickElement(driver, pobj.locationTemplateTab);
			campaignCenterPage.searchTemplateByFilter(driver, templateName);

			boolean isTemplatePresentAtFranchiseLocation = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () , 'No Records found.')]");
			if (isTemplatePresentAtFranchiseLocation == false) {
				fc.utobj().throwsException("was not able to verify Template At Location Template Tab");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "crm")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Create Recommended Template At CRM > Campaign Center > Email Templates", testCaseId = "TC_40_Create_Recommended_Template")
	private void createRecommendedTemplate() throws IOException, Exception, ParserConfigurationException, SAXException {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			CRMCampaignCenterEmailTemplatesPage pobj = new CRMCampaignCenterEmailTemplatesPage(driver);
			CRMCampaignCenterPageTest campaignCenterPage = new CRMCampaignCenterPageTest();

			fc.utobj().printTestStep("Navigate To CRM > Campaign Center > Email Template");
			fc.crm().crm_common().CRMCampaignCenterLnk(driver);
			fc.utobj().clickElement(driver, pobj.dashBoradLnk);
			fc.utobj().clickElement(driver, pobj.templateLnk);

			fc.utobj().printTestStep("Create Template");
			fc.utobj().clickElement(driver, pobj.createLink);
			fc.utobj().clickElement(driver, pobj.templateLink);

			fc.utobj().printTestStep("Code Your Own Template");

			// Code Your Template
			String templateName = fc.utobj().generateTestData(dataSet.get("templateName"));
			String emailSubject = fc.utobj().generateTestData(dataSet.get("emailSubject"));
			String plainTextEditor = fc.utobj().generateTestData(dataSet.get("plainTextEditor"));

			fc.utobj().printTestStep("Code Your Own Template");

			fc.utobj().clickElement(driver, pobj.codeYourOwn);
			fc.utobj().sendKeys(driver, pobj.templateName, templateName);
			fc.utobj().sendKeys(driver, pobj.mailSubject, emailSubject);
			fc.utobj().clickElement(driver, pobj.publicToAllUsersTemplate);
			fc.utobj().clickElement(driver, pobj.plainTextEmailType);
			fc.utobj().sendKeys(driver, pobj.textAreaPlainText, plainTextEditor);

			// Add Template In Folder
			fc.utobj().selectDropDownByValue(driver, pobj.addFolder, "addFolder");

			fc.commonMethods().switch_frameClass_newLayoutcboxIframe(driver);

			String folderName = fc.utobj().generateTestData(dataSet.get("folderName"));
			String folderAccessibility = dataSet.get("folderAccessibility");
			fc.utobj().sendKeys(driver, pobj.folderName, folderName);
			fc.utobj().clickElement(driver, pobj.folderDescriptionLink);
			fc.utobj().selectDropDown(driver, pobj.folderAccessibility, folderAccessibility);
			String folderDescription = fc.utobj().generateTestData(dataSet.get("folderDescription"));
			fc.utobj().sendKeys(driver, pobj.folderSummary, folderDescription);
			fc.utobj().clickElement(driver, pobj.saveBtn);
			fc.utobj().switchFrameToDefault(driver);
			fc.utobj().clickElement(driver, pobj.addAttchmentCircle);

			String fileName = dataSet.get("fileName");
			String filepath = fc.utobj().getFilePathFromTestData(fileName);
			fc.utobj().sendKeys(driver, pobj.attachmentName, filepath);
			fc.utobj().clickElement(driver, pobj.attachmentButton);
			fc.utobj().clickElement(driver, pobj.recommendedTemplate);
			fc.utobj().clickElement(driver, pobj.doneBtn);

			fc.utobj().printTestStep("Verify The Template At Recommended Templates Tab");
			fc.utobj().clickElement(driver, pobj.recommendedTab);
			campaignCenterPage.searchTemplateByFilter(driver, templateName);

			boolean isTemplatePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () , '" + templateName + "')]");
			if (isTemplatePresent == false) {
				fc.utobj().throwsException("was not able to verify Template At Recommended Template Tab");
			}

			boolean isFolderNamePresentAtCorporateTab = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[contains(text () , '"
					+ templateName + "')]/ancestor::tr/td[contains(text () , '" + folderName + "')]");
			if (isFolderNamePresentAtCorporateTab == false) {
				fc.utobj().throwsException("was not able to verify Folder At Recommended Template Tab");
			}

			fc.utobj().printTestStep("Verify The Template At Folders Tab");
			fc.utobj().clickElement(driver, pobj.foldersTab);
			campaignCenterPage.searchFoldersFilter(driver, folderName);

			boolean isFolderNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () , '" + folderName + "')]");
			if (isFolderNamePresent == false) {
				fc.utobj().throwsException("Was not able to verify Folder At Folders Tab");
			}

			fc.utobj().printTestStep("Verify The Template In Folder");
			fc.utobj().clickElement(driver, driver
					.findElement(By.xpath(".//td[contains(text () , '" + folderName + "')]/ancestor::tr/td/a[.='1']")));

			boolean isTemplatePresentAtCorporateTab = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () , '" + templateName + "')]");
			if (isTemplatePresentAtCorporateTab == false) {
				fc.utobj().throwsException(
						"was not able to verify Template At Recommended Template Tab Through Folder Tab");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "crm")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Modify Recommended Template At CRM > Campaign Center > Email Templates", testCaseId = "TC_41_Modify_Recommended_Template")
	private void modifyRecommendedTemplate() throws IOException, Exception, ParserConfigurationException, SAXException {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			CRMCampaignCenterEmailTemplatesPage pobj = new CRMCampaignCenterEmailTemplatesPage(driver);
			CRMCampaignCenterPageTest campaignCenterPage = new CRMCampaignCenterPageTest();
			CRMGroupsPageTest groupPage = new CRMGroupsPageTest();

			fc.utobj().printTestStep("Navigate To CRM > Campaign Center > Email Template");

			String templateName = fc.utobj().generateTestData(dataSet.get("templateName"));
			String emailSubject = fc.utobj().generateTestData(dataSet.get("emailSubject"));
			String plainTextEditor = fc.utobj().generateTestData(dataSet.get("plainTextEditor"));
			String accessibility = dataSet.get("accessibility");
			String emailType = dataSet.get("emailType");

			fc.utobj().printTestStep("Create Recommended Template");
			createCodeYourTemplateRecomndedTemplate(driver, config, dataSet, accessibility, emailType, templateName,
					emailSubject, plainTextEditor);

			fc.utobj().printTestStep("Modify The Recommended Template");
			fc.utobj().clickElement(driver, pobj.corporateTemplatesTab);
			campaignCenterPage.searchTemplateByFilter(driver, templateName);
			groupPage.actionImgOption(driver, templateName, "Modify");

			templateName = fc.utobj().generateTestData(dataSet.get("templateName"));
			emailSubject = fc.utobj().generateTestData(dataSet.get("emailSubject"));
			plainTextEditor = fc.utobj().generateTestData(dataSet.get("plainTextEditor"));
			fc.utobj().sendKeys(driver, pobj.templateName, templateName);
			fc.utobj().sendKeys(driver, pobj.mailSubject, emailSubject);
			fc.utobj().sendKeys(driver, pobj.textAreaPlainText, plainTextEditor);
			fc.utobj().clickElement(driver, pobj.addBtn);

			fc.utobj().printTestStep("Verify The Template At Recommended Templates Tab");
			fc.utobj().clickElement(driver, pobj.recommendedTab);
			campaignCenterPage.searchTemplateByFilter(driver, templateName);

			boolean isTemplatePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () , '" + templateName + "')]");
			if (isTemplatePresent == false) {
				fc.utobj().throwsException("was not able to verify Template At Recommended Template Tab");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "crm")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Delete Recommended Template At CRM > Campaign Center > Email Templates", testCaseId = "TC_42_Delete_Recommended_Template")
	private void deleteRecommendedTemplate() throws IOException, Exception, ParserConfigurationException, SAXException {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			CRMCampaignCenterEmailTemplatesPage pobj = new CRMCampaignCenterEmailTemplatesPage(driver);
			CRMCampaignCenterPageTest campaignCenterPage = new CRMCampaignCenterPageTest();
			CRMGroupsPageTest groupPage = new CRMGroupsPageTest();

			fc.utobj().printTestStep("Navigate To CRM > Campaign Center > Email Template");

			String templateName = fc.utobj().generateTestData(dataSet.get("templateName"));
			String emailSubject = fc.utobj().generateTestData(dataSet.get("emailSubject"));
			String plainTextEditor = fc.utobj().generateTestData(dataSet.get("plainTextEditor"));
			String accessibility = dataSet.get("accessibility");
			String emailType = dataSet.get("emailType");

			fc.utobj().printTestStep("Create Recommended Template");
			createCodeYourTemplateRecomndedTemplate(driver, config, dataSet, accessibility, emailType, templateName,
					emailSubject, plainTextEditor);

			fc.utobj().printTestStep("Delete The Recommended Template");
			fc.utobj().clickElement(driver, pobj.recommendedTab);
			campaignCenterPage.searchTemplateByFilter(driver, templateName);
			groupPage.actionImgOption(driver, templateName, "Delete");
			fc.utobj().acceptAlertBox(driver);

			fc.utobj().printTestStep("Verify The Template At Recommended Templates Tab");
			fc.utobj().clickElement(driver, pobj.recommendedTab);
			campaignCenterPage.searchTemplateByFilter(driver, templateName);

			boolean isTemplatePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//td[contains(text () , 'No Records found.')]");
			if (isTemplatePresent == false) {
				fc.utobj().throwsException("was not able to verify Template At Recommended Template Tab");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "crm")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Archive And Unarchive Recommended Template At CRM > Campaign Center > Email Templates", testCaseId = "TC_43_Archive_Recommended_Template")
	private void archiveRecommendedTemplate()
			throws IOException, Exception, ParserConfigurationException, SAXException {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			CRMCampaignCenterEmailTemplatesPage pobj = new CRMCampaignCenterEmailTemplatesPage(driver);
			CRMCampaignCenterPageTest campaignCenterPage = new CRMCampaignCenterPageTest();
			CRMGroupsPageTest groupPage = new CRMGroupsPageTest();

			fc.utobj().printTestStep("Navigate To CRM > Campaign Center > Email Template");

			String templateName = fc.utobj().generateTestData(dataSet.get("templateName"));
			String emailSubject = fc.utobj().generateTestData(dataSet.get("emailSubject"));
			String plainTextEditor = fc.utobj().generateTestData(dataSet.get("plainTextEditor"));
			String accessibility = dataSet.get("accessibility");
			String emailType = dataSet.get("emailType");

			fc.utobj().printTestStep("Create Recommended Template");
			createCodeYourTemplateRecomndedTemplate(driver, config, dataSet, accessibility, emailType, templateName,
					emailSubject, plainTextEditor);

			fc.utobj().printTestStep("Archive The Recommended Template");
			fc.utobj().clickElement(driver, pobj.recommendedTab);
			campaignCenterPage.searchTemplateByFilter(driver, templateName);
			groupPage.actionImgOption(driver, templateName, "Archive");
			fc.utobj().acceptAlertBox(driver);

			fc.commonMethods().switch_frameClass_newLayoutcboxIframe(driver);
			fc.utobj().clickElement(driver, pobj.closeBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Verify The Archive Template At Recommended Templates Tab");
			fc.utobj().clickElement(driver, pobj.recommendedTab);
			campaignCenterPage.searchTemplateByFilter(driver, templateName);

			boolean isTemplatePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//td[contains(text () , 'No Records found.')]");
			if (isTemplatePresent == false) {
				fc.utobj().throwsException("was not able to verify Template At Corporate Template Tab");
			}

			fc.utobj().printTestStep("Verify The Archived Template At Archived Tab");
			fc.utobj().clickElement(driver, pobj.archivedTab);

			campaignCenterPage.searchTemplateByFilter(driver, templateName);

			boolean isTemplatePresentArchived = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () , '" + templateName + "')]");
			if (isTemplatePresentArchived == false) {
				fc.utobj().throwsException("was not able to verify Template At Archived Tab");
			}

			fc.utobj().printTestStep("Unarchive The Recommended Template");
			groupPage.actionImgOption(driver, templateName, "Unarchive");
			fc.utobj().acceptAlertBox(driver);
			fc.commonMethods().switch_frameClass_newLayoutcboxIframe(driver);
			fc.utobj().clickElement(driver, pobj.closeBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Verify The UnArchive Template At Recommended Templates Tab");
			fc.utobj().clickElement(driver, pobj.recommendedTab);
			campaignCenterPage.searchTemplateByFilter(driver, templateName);

			boolean isTemplatePresentUnArchive = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () , '" + templateName + "')]");
			if (isTemplatePresentUnArchive == false) {
				fc.utobj().throwsException("was not able to verify Template At Recommended Template Tab");
			}

			fc.utobj().printTestStep("Verify The UnArchived Template At Archived Tab");
			fc.utobj().clickElement(driver, pobj.archivedTab);
			campaignCenterPage.searchTemplateByFilter(driver, templateName);

			boolean isTemplatePresentUnArchived = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () , 'No Records found.')]");
			if (isTemplatePresentUnArchived == false) {
				fc.utobj().throwsException("was not able to verify Template At Archived Tab");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "crmcampaigncenterTest1")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Test Recommended Template At CRM > Campaign Center > Email Templates", testCaseId = "TC_44_Test_Recommended_Template")
	private void testRecommendedTemplate() throws IOException, Exception, ParserConfigurationException, SAXException {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			CRMCampaignCenterEmailTemplatesPage pobj = new CRMCampaignCenterEmailTemplatesPage(driver);
			CRMCampaignCenterPageTest campaignCenterPage = new CRMCampaignCenterPageTest();
			CRMGroupsPageTest groupPage = new CRMGroupsPageTest();

			fc.utobj().printTestStep("Navigate To CRM > Campaign Center > Email Template");

			String templateName = fc.utobj().generateTestData(dataSet.get("templateName"));
			String emailSubject = fc.utobj().generateTestData(dataSet.get("emailSubject"));
			String plainTextEditor = fc.utobj().generateTestData(dataSet.get("plainTextEditor"));
			String accessibility = dataSet.get("accessibility");
			String emailType = dataSet.get("emailType");

			fc.utobj().printTestStep("Create Recommended Template");
			createCodeYourTemplateRecomndedTemplate(driver, config, dataSet, accessibility, emailType, templateName,
					emailSubject, plainTextEditor);

			fc.utobj().printTestStep("Test The Recommended Template");
			fc.utobj().clickElement(driver, pobj.recommendedTab);
			campaignCenterPage.searchTemplateByFilter(driver, templateName);
			groupPage.actionImgOption(driver, templateName, "Test Template");

			String emailId = dataSet.get("emailId");

			fc.commonMethods().switch_frameClass_newLayoutcboxIframe(driver);
			fc.utobj().sendKeys(driver, pobj.testEmailField, emailId);
			fc.utobj().clickElement(driver, pobj.testButton);
			fc.utobj().clickElement(driver, pobj.closeBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Verify The Template Details In Email");

			// Pending

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "crm")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Copy And Customize Recommended Template At CRM > Campaign Center > Email Templates", testCaseId = "TC_45_Copy_And_Customize_Recommended_Template")
	private void copyAndCustomizeRecommendedTemplate()
			throws IOException, Exception, ParserConfigurationException, SAXException {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			CRMCampaignCenterEmailTemplatesPage pobj = new CRMCampaignCenterEmailTemplatesPage(driver);
			CRMCampaignCenterPageTest campaignCenterPage = new CRMCampaignCenterPageTest();
			CRMGroupsPageTest groupPage = new CRMGroupsPageTest();

			fc.utobj().printTestStep("Navigate To CRM > Campaign Center > Email Template");

			String templateName = fc.utobj().generateTestData(dataSet.get("templateName"));
			String emailSubject = fc.utobj().generateTestData(dataSet.get("emailSubject"));
			String plainTextEditor = fc.utobj().generateTestData(dataSet.get("plainTextEditor"));
			String accessibility = dataSet.get("accessibility");
			String emailType = dataSet.get("emailType");

			fc.utobj().printTestStep("Create Recommended Template");
			createCodeYourTemplateRecomndedTemplate(driver, config, dataSet, accessibility, emailType, templateName,
					emailSubject, plainTextEditor);

			fc.utobj().printTestStep("Copy And Customize The Recommended Template");
			fc.utobj().clickElement(driver, pobj.recommendedTab);
			campaignCenterPage.searchTemplateByFilter(driver, templateName);
			groupPage.actionImgOption(driver, templateName, "Copy and Customize");

			templateName = fc.utobj().generateTestData(dataSet.get("templateName"));
			emailSubject = fc.utobj().generateTestData(dataSet.get("emailSubject"));
			fc.utobj().sendKeys(driver, pobj.templateName, templateName);
			fc.utobj().sendKeys(driver, pobj.mailSubject, emailSubject);
			fc.utobj().clickElement(driver, pobj.addBtn);

			fc.utobj().printTestStep("Verify The Template At Recommended Templates Tab");
			fc.utobj().clickElement(driver, pobj.recommendedTab);
			campaignCenterPage.searchTemplateByFilter(driver, templateName);

			boolean isTemplatePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () , '" + templateName + "')]");
			if (isTemplatePresent == false) {
				fc.utobj().throwsException("was not able to verify Template At Recommended Template Tab");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "crm")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Delete Archived Corporate Template At CRM > Campaign Center > Email Templates", testCaseId = "TC_46_Delete_Archived_Template")
	private void deleteArchivedCorporateTemplate()
			throws IOException, Exception, ParserConfigurationException, SAXException {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			CRMCampaignCenterEmailTemplatesPage pobj = new CRMCampaignCenterEmailTemplatesPage(driver);
			CRMCampaignCenterPageTest campaignCenterPage = new CRMCampaignCenterPageTest();
			CRMGroupsPageTest groupPage = new CRMGroupsPageTest();

			fc.utobj().printTestStep("Navigate To CRM > Campaign Center > Email Template");

			String templateName = fc.utobj().generateTestData(dataSet.get("templateName"));
			String emailSubject = fc.utobj().generateTestData(dataSet.get("emailSubject"));
			String plainTextEditor = fc.utobj().generateTestData(dataSet.get("plainTextEditor"));
			String accessibility = dataSet.get("accessibility");
			String emailType = dataSet.get("emailType");

			fc.utobj().printTestStep("Create Corporate Template");
			createCodeYourTemplate(driver, config, dataSet, accessibility, emailType, templateName, emailSubject,
					plainTextEditor);

			fc.utobj().printTestStep("Archive The Corporate Template");
			fc.utobj().clickElement(driver, pobj.corporateTemplatesTab);
			campaignCenterPage.searchTemplateByFilter(driver, templateName);
			groupPage.actionImgOption(driver, templateName, "Archive");
			fc.utobj().acceptAlertBox(driver);

			fc.commonMethods().switch_frameClass_newLayoutcboxIframe(driver);
			fc.utobj().clickElement(driver, pobj.closeBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Navigate to Archived Tab");
			fc.utobj().clickElement(driver, pobj.archivedTab);

			campaignCenterPage.searchTemplateByFilter(driver, templateName);
			groupPage.actionImgOption(driver, templateName, "Delete");
			fc.utobj().acceptAlertBox(driver);

			fc.utobj().printTestStep("Verify The Template At Archived Templates Tab");
			campaignCenterPage.searchTemplateByFilter(driver, templateName);

			boolean isTemplatePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//td[contains(text () , 'No Records found.')]");
			if (isTemplatePresent == false) {
				fc.utobj().throwsException("was not able to verify Template At Archived Template Tab");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = {"crm","errorpages1"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Add Folder And Add Template In Folder At CRM > Campaign Center > Email Templates", testCaseId = "TC_47_Add_Folder")
	private void addFolder() throws IOException, Exception, ParserConfigurationException, SAXException {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			CRMCampaignCenterEmailTemplatesPage pobj = new CRMCampaignCenterEmailTemplatesPage(driver);
			CRMCampaignCenterPageTest campaignCenterPage = new CRMCampaignCenterPageTest();

			fc.utobj().printTestStep("Navigate To CRM > Campaign Center > Email Template");
			fc.crm().crm_common().CRMCampaignCenterLnk(driver);
			fc.utobj().clickElement(driver, pobj.dashBoradLnk);
			fc.utobj().clickElement(driver, pobj.templateLnk);

			fc.utobj().printTestStep("Add Folder");
			fc.utobj().clickElement(driver, pobj.createLink);
			fc.utobj().clickElement(driver, pobj.folderLnk);

			fc.commonMethods().switch_frameClass_newLayoutcboxIframe(driver);
			String folderName = fc.utobj().generateTestData(dataSet.get("folderName"));
			String folderAccessibility = dataSet.get("folderAccessibility");
			fc.utobj().sendKeys(driver, pobj.folderName, folderName);
			fc.utobj().selectDropDown(driver, pobj.folderAccessibility, folderAccessibility);
			fc.utobj().clickElement(driver, pobj.folderDescriptionLink);
			String folderDescription = fc.utobj().generateTestData(dataSet.get("folderDescription"));
			fc.utobj().sendKeys(driver, pobj.folderSummary, folderDescription);
			fc.utobj().clickElement(driver, pobj.saveAndAddTemplate);
			fc.utobj().switchFrameToDefault(driver);

			// Code Your Template
			String templateName = fc.utobj().generateTestData(dataSet.get("templateName"));
			String emailSubject = fc.utobj().generateTestData(dataSet.get("emailSubject"));
			String plainTextEditor = fc.utobj().generateTestData(dataSet.get("plainTextEditor"));

			fc.utobj().clickElement(driver, pobj.codeYourOwn);
			fc.utobj().sendKeys(driver, pobj.templateName, templateName);
			fc.utobj().sendKeys(driver, pobj.mailSubject, emailSubject);
			fc.utobj().clickElement(driver, pobj.publicToAllUsersTemplate);
			fc.utobj().clickElement(driver, pobj.plainTextEmailType);
			fc.utobj().sendKeys(driver, pobj.textAreaPlainText, plainTextEditor);
			fc.utobj().clickElement(driver, pobj.doneBtn);

			fc.utobj().printTestStep("Verify The Folder");
			fc.utobj().clickElement(driver, pobj.foldersTab);
			campaignCenterPage.searchFoldersFilter(driver, folderName);

			boolean isFolderNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () , '" + folderName + "')]");
			if (isFolderNamePresent == false) {
				fc.utobj().throwsException("Was not able to verify Folder At Folders Tab");
			}

			boolean isFolderAccessibility = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//td[contains(text () , '" + folderName
					+ "')]/ancestor::tr/td[contains(text () , '" + folderAccessibility + "')]");
			if (isFolderAccessibility == false) {
				fc.utobj().throwsException("Was not able to verify Folder Accesibility At Folders Tab");
			}

			fc.utobj().printTestStep("Verify The Template In Folder");
			fc.utobj().clickElement(driver, driver
					.findElement(By.xpath(".//td[contains(text () , '" + folderName + "')]/ancestor::tr/td/a[.='1']")));

			boolean isTemplatePresentAtCorporateTab = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () , '" + templateName + "')]");
			if (isTemplatePresentAtCorporateTab == false) {
				fc.utobj().throwsException(
						"was not able to verify Template At Corporate Template Tab Through Folder Tab");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "crm")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Modify Folder At CRM > Campaign Center > Email Templates", testCaseId = "TC_48_Modify_Folder")
	private void modifyFolder() throws IOException, Exception, ParserConfigurationException, SAXException {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			CRMCampaignCenterEmailTemplatesPage pobj = new CRMCampaignCenterEmailTemplatesPage(driver);
			CRMCampaignCenterPageTest campaignCenterPage = new CRMCampaignCenterPageTest();

			fc.utobj().printTestStep("Navigate To CRM > Campaign Center > Email Template");
			fc.utobj().printTestStep("Add Folder");
			String folderName = fc.utobj().generateTestData(dataSet.get("folderName"));
			String folderAccessibility = dataSet.get("folderAccessibility");
			String folderDescription = fc.utobj().generateTestData(dataSet.get("folderDescription"));
			addFolder(driver, config, folderName, folderAccessibility, folderDescription);

			fc.utobj().printTestStep("Modify The Folder");
			fc.utobj().clickElement(driver, pobj.foldersTab);
			campaignCenterPage.searchFoldersFilter(driver, folderName);
			actionImgOption(driver, folderName, "Modify");

			folderName = fc.utobj().generateTestData(dataSet.get("folderName"));
			String folderAccessibilityChanged = dataSet.get("folderAccessibilityChanged");
			folderDescription = fc.utobj().generateTestData(dataSet.get("folderDescription"));

			fc.commonMethods().switch_frameClass_newLayoutcboxIframe(driver);
			fc.utobj().sendKeys(driver, pobj.folderName, folderName);
			fc.utobj().selectDropDown(driver, pobj.folderAccessibility, folderAccessibilityChanged);
			fc.utobj().clickElement(driver, pobj.folderDescriptionLink);
			fc.utobj().sendKeys(driver, pobj.folderSummary, folderDescription);
			fc.utobj().clickElement(driver, pobj.saveFolder);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Verify The Modify Folder");
			fc.utobj().clickElement(driver, pobj.foldersTab);
			campaignCenterPage.searchFoldersFilter(driver, folderName);

			boolean isFolderNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () , '" + folderName + "')]");
			if (isFolderNamePresent == false) {
				fc.utobj().throwsException("Was not able to verify Folder At Folders Tab");
			}

			boolean isFolderAccessibility = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//td[contains(text () , '" + folderName
					+ "')]/ancestor::tr/td[contains(text () , '" + folderAccessibilityChanged + "')]");
			if (isFolderAccessibility == false) {
				fc.utobj().throwsException("Was not able to verify Folder Accesibility At Folders Tab");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "crm")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The delete Folder At CRM > Campaign Center > Email Templates", testCaseId = "TC_48_Delete_Folder")
	private void deleteFolder() throws IOException, Exception, ParserConfigurationException, SAXException {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			CRMCampaignCenterEmailTemplatesPage pobj = new CRMCampaignCenterEmailTemplatesPage(driver);
			CRMCampaignCenterPageTest campaignCenterPage = new CRMCampaignCenterPageTest();

			fc.utobj().printTestStep("Navigate To CRM > Campaign Center > Email Template");
			fc.utobj().printTestStep("Add Folder");
			String folderName = fc.utobj().generateTestData(dataSet.get("folderName"));
			String folderAccessibility = dataSet.get("folderAccessibility");
			String folderDescription = fc.utobj().generateTestData(dataSet.get("folderDescription"));
			addFolder(driver, config, folderName, folderAccessibility, folderDescription);

			fc.utobj().printTestStep("Delete The Folder");
			fc.utobj().clickElement(driver, pobj.foldersTab);
			campaignCenterPage.searchFoldersFilter(driver, folderName);
			actionImgOption(driver, folderName, "Delete");
			fc.utobj().acceptAlertBox(driver);

			fc.utobj().printTestStep("Verify The Delete Folder");
			fc.utobj().clickElement(driver, pobj.foldersTab);
			campaignCenterPage.searchFoldersFilter(driver, folderName);

			boolean isFolderNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () , 'No Records found.')]");
			if (isFolderNamePresent == false) {
				fc.utobj().throwsException("Was not able to verify Folder At Folders Tab");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	public void addFolder(WebDriver driver, Map<String, String> config, String folderName, String folderAccessibility,
			String folderDescription) throws Exception {

		String testCaseId = "TC_Add_Folder_At_Email_Template";

		if (fc.utobj().validate(testCaseId)) {

			try {

				CRMCampaignCenterEmailTemplatesPage pobj = new CRMCampaignCenterEmailTemplatesPage(driver);
				fc.crm().crm_common().CRMCampaignCenterLnk(driver);
				fc.utobj().clickElement(driver, pobj.dashBoradLnk);
				fc.utobj().clickElement(driver, pobj.templateLnk);
				fc.utobj().clickElement(driver, pobj.createLink);
				fc.utobj().clickElement(driver, pobj.folderLnk);
				fc.commonMethods().switch_frameClass_newLayoutcboxIframe(driver);
				fc.utobj().sendKeys(driver, pobj.folderName, folderName);
				fc.utobj().selectDropDown(driver, pobj.folderAccessibility, folderAccessibility);
				/*
				 * fc.utobj().clickElement(driver, pobj.folderDescriptionLink);
				 * fc.utobj().sendKeys(driver, pobj.folderSummary,
				 * folderDescription);
				 */
				fc.utobj().clickElement(driver, pobj.saveFolder);
				fc.utobj().switchFrameToDefault(driver);

			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException("Was not able to Create Folder");
		}
	}

	// create Template At CRM > Campaign Center > Email Template

	public void createCodeYourTemplate(WebDriver driver, Map<String, String> config, Map<String, String> dataSet,
			String accessibility, String emailType, String templateName, String emailSubject, String plainTextEditor)
			throws Exception {

		String testCaseId = "TC_Create_Template_At_Email_Template";

		if (fc.utobj().validate(testCaseId)) {
			try {
				CRMCampaignCenterEmailTemplatesPage pobj = new CRMCampaignCenterEmailTemplatesPage(driver);

				fc.crm().crm_common().CRMCampaignCenterLnk(driver);
				fc.utobj().clickElement(driver, pobj.dashBoradLnk);
				fc.utobj().clickElement(driver, pobj.templateLnk);

				fc.utobj().clickElement(driver, pobj.createLink);
				fc.utobj().clickElement(driver, pobj.templateLink);

				fc.utobj().clickElement(driver, pobj.codeYourOwn);
				fc.utobj().sendKeys(driver, pobj.templateName, templateName);
				fc.utobj().sendKeys(driver, pobj.mailSubject, emailSubject);

				if (accessibility.equalsIgnoreCase("Public to all Users")) {

					fc.utobj().clickElement(driver, pobj.publicToAllUsersTemplate);

				} else if (accessibility.equalsIgnoreCase("Public to all Corporate Users")) {

					fc.utobj().clickElement(driver, pobj.publicToAllCorporateUser);

				} else if (accessibility.equalsIgnoreCase("Private")) {

					fc.utobj().clickElement(driver, pobj.privateTemplate);

				} else if (accessibility.equalsIgnoreCase("Public to all My Franchise Users")) {

					fc.utobj().clickElement(driver, pobj.publicToAllMyFranchiseLocation);

				}

				if (emailType.equalsIgnoreCase("Graphical")) {

					fc.utobj().clickElement(driver, pobj.graphicalEmailType);
					fc.utobj().switchFrameById(driver, "ta_ifr");

					Actions actions = new Actions(driver);
					actions.moveToElement(fc.utobj().getElement(driver, pobj.editorTextArea));
					actions.click();
					actions.sendKeys("Grapgical template");
					actions.build().perform();
					fc.utobj().switchFrameToDefault(driver);

				} else if (emailType.equalsIgnoreCase("Plain Text")) {

					fc.utobj().clickElement(driver, pobj.plainTextEmailType);
					fc.utobj().sendKeys(driver, pobj.textAreaPlainText, plainTextEditor);

				} else if (emailType.equalsIgnoreCase("Upload HTML File")) {

					fc.utobj().clickElement(driver, pobj.uploadHTMLEmailType);
					// fc.utobj().clickElement(driver,
					// pobj.htmlFileAttachmenet);
					String fileName = dataSet.get("fileName");
					fc.utobj().getFilePathFromTestData(fileName);
					fc.utobj().sendKeys(driver, pobj.htmlFileAttachmenet, fileName);
				}

				fc.utobj().clickElement(driver, pobj.doneBtn);

			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException("Was not able to Create Template");
		}
	}

	// create Template At CRM > Campaign Center > Email Template [Recomnded
	// Template]

	public void createCodeYourTemplateRecomndedTemplate(WebDriver driver, Map<String, String> config,
			Map<String, String> dataSet, String accessibility, String emailType, String templateName,
			String emailSubject, String plainTextEditor) throws Exception {

		String testCaseId = "TC_Create_Recommended_Template_At_Email_Template";

		if (fc.utobj().validate(testCaseId)) {
			try {
				CRMCampaignCenterEmailTemplatesPage pobj = new CRMCampaignCenterEmailTemplatesPage(driver);

				fc.crm().crm_common().CRMCampaignCenterLnk(driver);
				fc.utobj().clickElement(driver, pobj.dashBoradLnk);
				fc.utobj().clickElement(driver, pobj.templateLnk);

				fc.utobj().clickElement(driver, pobj.createLink);
				fc.utobj().clickElement(driver, pobj.templateLink);

				fc.utobj().clickElement(driver, pobj.codeYourOwn);
				fc.utobj().sendKeys(driver, pobj.templateName, templateName);
				fc.utobj().sendKeys(driver, pobj.mailSubject, emailSubject);

				if (accessibility.equalsIgnoreCase("Public to all Users")) {

					fc.utobj().clickElement(driver, pobj.publicToAllUsersTemplate);

				} else if (accessibility.equalsIgnoreCase("Public to all Corporate Users")) {

					fc.utobj().clickElement(driver, pobj.publicToAllCorporateUser);

				} else if (accessibility.equalsIgnoreCase("Private")) {

					fc.utobj().clickElement(driver, pobj.privateTemplate);

				} else if (accessibility.equalsIgnoreCase("Public to all My Franchise Users")) {

					fc.utobj().clickElement(driver, pobj.publicToAllMyFranchiseLocation);

				}

				if (emailType.equalsIgnoreCase("Graphical")) {

					fc.utobj().clickElement(driver, pobj.graphicalEmailType);
					fc.utobj().switchFrameById(driver, "ta_ifr");

					Actions actions = new Actions(driver);
					actions.moveToElement(fc.utobj().getElement(driver, pobj.editorTextArea));
					actions.click();
					actions.sendKeys(dataSet.get("editorText"));
					actions.build().perform();
					fc.utobj().switchFrameToDefault(driver);

				} else if (emailType.equalsIgnoreCase("Plain Text")) {

					fc.utobj().clickElement(driver, pobj.plainTextEmailType);
					fc.utobj().sendKeys(driver, pobj.textAreaPlainText, plainTextEditor);

				} else if (emailType.equalsIgnoreCase("Upload HTML File")) {

					fc.utobj().clickElement(driver, pobj.uploadHTMLEmailType);
					// fc.utobj().clickElement(driver,
					// pobj.htmlFileAttachmenet);
					String fileName = dataSet.get("fileName");
					fc.utobj().getFilePathFromTestData(fileName);
					fc.utobj().sendKeys(driver, pobj.htmlFileAttachmenet, fileName);
				}

				fc.utobj().clickElement(driver, pobj.recommendedTemplate);
				fc.utobj().clickElement(driver, pobj.doneBtn);

			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException("Was not able to Create Template");
		}
	}

	public void createTemplate(WebDriver driver, Map<String, String> config, String templateNameForSendEmail,
			String emailSubjectForSendEmail, String plainTextEditorForSendEmail) throws Exception {

		String testCaseId = "TC_Create_Template_For_WorkFlow";

		if (fc.utobj().validate(testCaseId)) {

			try {

				CRMCampaignCenterEmailTemplatesPage pobj = new CRMCampaignCenterEmailTemplatesPage(driver);
				fc.utobj().clickElement(driver, pobj.campaignCenterLinks);

				fc.utobj().clickElement(driver, pobj.menuOptions);
				fc.utobj().clickElement(driver, pobj.templatesLink);
				fc.utobj().clickElement(driver, pobj.createLink);
				fc.utobj().clickElement(driver, pobj.templateLink);

				// create Template :: Code Your Own

				fc.utobj().clickElement(driver, pobj.codeYourOwn);
				fc.utobj().sendKeys(driver, pobj.templateName, templateNameForSendEmail);
				fc.utobj().sendKeys(driver, pobj.mailSubject, emailSubjectForSendEmail);
				fc.utobj().clickElement(driver, pobj.plainTextEmailType);
				fc.utobj().sendKeys(driver, pobj.textAreaPlainText, plainTextEditorForSendEmail);
				fc.utobj().clickElement(driver, pobj.addBtn);

				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);

			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException("Was not able to Create Template For WorkFlow");
		}
	}

	public void actionImgOption(WebDriver driver, String groupName, String option) throws Exception {
		fc.utobj().clickElement(driver, driver.findElement(By
				.xpath(".//*[contains(text () ,'" + groupName + "')]/following-sibling::td//div[@class='list-name']")));

		WebElement element = fc.utobj().getElementByXpath(driver,
				".//*[contains(text () ,'" + groupName
						+ "')]/following-sibling::td//div[@class='list-name']/following-sibling::ul/li/a[contains(text () ,'"
						+ option + "')]");
		fc.utobj().clickElement(driver, element);
	}
}
