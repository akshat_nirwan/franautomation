package com.builds.test.crm;

import java.io.IOException;
import java.util.Map;

import javax.xml.parsers.ParserConfigurationException;

import org.apache.poi.ss.formula.IStabilityClassifier;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.NoAlertPresentException;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.Test;
import org.xml.sax.SAXException;

import com.builds.test.common.CRMModule;
import com.builds.test.common.FCHomePageTest;
import com.builds.uimaps.campaigncentercrm.CRMCampaignCenterPage;
import com.builds.uimaps.crm.CRM_CampaignFinalandLunchUI;
import com.builds.uimaps.crm.CRM_CampaignSelectRecipientUI;
import com.builds.uimaps.fs.FSCampaignCenterPage;
import com.builds.utilities.FranconnectUtil;
import com.builds.utilities.TestCase;

public class CRMCampaignCenterPageTest {

	FranconnectUtil fc = new FranconnectUtil();

	@Test(groups = { "crm", "crmTestrun1234"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Creation Of Promotional Type Campaign At CRM > Campaign Center > DashBoard", testCaseId = "TC_01_Create_Campaign_Promotional_Type")
	private void createCampaignPromotionalType()
			throws IOException, Exception, ParserConfigurationException, SAXException {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			CRMCampaignCenterPage pobj = new CRMCampaignCenterPage(driver);

			fc.utobj().printTestStep("Navigating to Admin > CRM > Configure Campaign Email Category");
			fc.utobj().printTestStep("Add Category");

			AdminCRMConfigureCampaignEmailCategoryPageTest campaignMailCategoryPage = new AdminCRMConfigureCampaignEmailCategoryPageTest();
			FSCampaignCenterPage campaignCenterPage = new FSCampaignCenterPage(driver);

			String categoryName = fc.utobj().generateTestData(dataSet.get("categoryName"));
			campaignMailCategoryPage.addCategory(driver, categoryName);

			fc.utobj().printTestStep("Navigate To CRM > Campaign Center");
			fc.crm().crm_common().CRMCampaignCenterLnk(driver);

			fc.utobj().printTestStep("Create Campaign At Dashboard");

			fc.utobj().clickElement(driver, pobj.createLink);
			fc.utobj().clickElement(driver, pobj.createCampaignLink);
			fc.commonMethods().switch_frameClass_newLayoutcboxIframe(driver);

			String campaignName = fc.utobj().generateTestData(dataSet.get("campaignName"));
			String accessibility = dataSet.get("accessibility");
			fc.utobj().sendKeys(driver, pobj.campaignTitle, campaignName);

			String camapignDescription = fc.utobj().generateTestData(dataSet.get("camapignDescription"));
			fc.utobj().clickElement(driver, pobj.campaignDescriptionLnk);
			fc.utobj().sendKeys(driver, pobj.campaignDescription, camapignDescription);
			fc.utobj().clickElement(driver, pobj.hideDescriptionLink);

			// fc.utobj().clickElement(driver, pobj.emailCampaign);
			fc.utobj().selectDropDown(driver, pobj.campaignAccessibility, accessibility);
			fc.utobj().selectDropDown(driver, pobj.selectCategory, categoryName);
			fc.utobj().clickElement(driver, pobj.promotionalTypeCampaign);
			fc.utobj().clickElement(driver, pobj.startBtn);
			fc.utobj().switchFrameToDefault(driver);

			// Code Your Template
			String templateName = fc.utobj().generateTestData(dataSet.get("templateName"));
			String emailSubject = fc.utobj().generateTestData(dataSet.get("emailSubject"));
			String plainTextEditor = fc.utobj().generateTestData(dataSet.get("plainTextEditor"));

			fc.utobj().printTestStep("Code Your Own Template");

			fc.utobj().clickElement(driver, pobj.codeYourOwn);
			fc.utobj().sendKeys(driver, pobj.templateName, templateName);
			fc.utobj().sendKeys(driver, pobj.mailSubject, emailSubject);

			fc.utobj().clickElement(driver, pobj.plainTextEmailType);
			fc.utobj().sendKeys(driver, pobj.textAreaPlainText, plainTextEditor);

			fc.utobj().clickElement(driver, pobj.saveAndContinue);
			fc.utobj().clickElement(driver, pobj.associateLater);
			fc.utobj().clickElement(driver, pobj.finalizeAndLaunchCampaignBtn);

			fc.utobj().printTestStep("Verify The Create Campaign");
			// fc.utobj().clickElement(driver, pobj.viewAllButton);

			fc.utobj().clickElement(driver, campaignCenterPage.veiwCampaings);

			fc.utobj().printTestStep("Verify Campaign At All Tab");
			searchCampaignByFilter(driver, campaignName);

			boolean isCampaignNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[.='" + campaignName + "']");
			if (isCampaignNamePresent == false) {
				fc.utobj().throwsException("was not able to verify Campaign At Campaign Center All Tab");
			}

			/*
			 * fc.utobj().clickElement(driver, pobj.promotionalCampaignTab);
			 * searchCampaignByFilter(driver, campaignName);
			 * 
			 * boolean isCampaignNamePresentAtPromotionalCampaignTab=fc.utobj().
			 * verifyCase(driver, ".//a[.='"+campaignName+"']"); if
			 * (isCampaignNamePresentAtPromotionalCampaignTab==false) {
			 * fc.utobj().throwsException(
			 * "was not able to verify Campaign At Campaign Center Promotional Campaign Tab"
			 * ); }
			 */

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "crm")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Creation Of Status Driven Type Campaign At CRM > Campaign Center > DashBoard", testCaseId = "TC_02_Create_Campaign_Status_Driven_Type")
	private void createCampaignStatusDrivenType()
			throws IOException, Exception, ParserConfigurationException, SAXException {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			CRMCampaignCenterPage pobj = new CRMCampaignCenterPage(driver);
			FSCampaignCenterPage campaignCenterPage = new FSCampaignCenterPage(driver);

			fc.utobj().printTestStep("Navigating to Admin > CRM > Configure Campaign Email Category");
			fc.utobj().printTestStep("Add Category");

			AdminCRMConfigureCampaignEmailCategoryPageTest campaignMailCategoryPage = new AdminCRMConfigureCampaignEmailCategoryPageTest();
			String categoryName = fc.utobj().generateTestData(dataSet.get("categoryName"));
			campaignMailCategoryPage.addCategory(driver, categoryName);

			fc.utobj().printTestStep("Navigate To CRM > Campaign Center");
			fc.crm().crm_common().CRMCampaignCenterLnk(driver);

			fc.utobj().printTestStep("Create Campaign At Dashboard");

			fc.utobj().clickElement(driver, pobj.createLink);
			fc.utobj().clickElement(driver, pobj.createCampaignLink);
			fc.commonMethods().switch_frameClass_newLayoutcboxIframe(driver);

			String campaignName = fc.utobj().generateTestData(dataSet.get("campaignName"));
			String accessibility = dataSet.get("accessibility");
			fc.utobj().sendKeys(driver, pobj.campaignTitle, campaignName);

			String camapignDescription = fc.utobj().generateTestData(dataSet.get("camapignDescription"));
			fc.utobj().clickElement(driver, pobj.campaignDescriptionLnk);
			fc.utobj().sendKeys(driver, pobj.campaignDescription, camapignDescription);
			fc.utobj().clickElement(driver, pobj.hideDescriptionLink);

			// fc.utobj().clickElement(driver, pobj.emailCampaign);
			fc.utobj().selectDropDown(driver, pobj.campaignAccessibility, accessibility);
			fc.utobj().selectDropDown(driver, pobj.selectCategory, categoryName);
			fc.utobj().clickElement(driver, pobj.statusDrivenTypeCampaign);
			fc.utobj().clickElement(driver, pobj.startBtn);
			fc.utobj().switchFrameToDefault(driver);

			// Code Your Template
			String templateName = fc.utobj().generateTestData(dataSet.get("templateName"));
			String emailSubject = fc.utobj().generateTestData(dataSet.get("emailSubject"));
			String plainTextEditor = fc.utobj().generateTestData(dataSet.get("plainTextEditor"));

			fc.utobj().printTestStep("Code Your Own Template");

			fc.utobj().clickElement(driver, pobj.codeYourOwn);
			fc.utobj().sendKeys(driver, pobj.templateName, templateName);
			fc.utobj().sendKeys(driver, pobj.mailSubject, emailSubject);

			fc.utobj().clickElement(driver, pobj.plainTextEmailType);
			fc.utobj().sendKeys(driver, pobj.textAreaPlainText, plainTextEditor);

			fc.utobj().clickElement(driver, pobj.saveAndContinue);
			fc.utobj().clickElement(driver, pobj.associateLater);
			fc.utobj().clickElement(driver, pobj.finalizeAndLaunchCampaignBtn);

			fc.utobj().printTestStep("Verify The Create Campaign");
			// fc.utobj().clickElement(driver, pobj.viewAllButton);

			fc.utobj().clickElement(driver, campaignCenterPage.veiwCampaings);

			fc.utobj().printTestStep("Verify Campaign At All Tab");
			searchCampaignByFilter(driver, campaignName);

			boolean isCampaignNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[.='" + campaignName + "']");
			if (isCampaignNamePresent == false) {
				fc.utobj().throwsException("was not able to verify Campaign At Campaign Center All Tab");
			}

			/*
			 * fc.utobj().clickElement(driver, pobj.statusDrivenCampaignTab);
			 * searchCampaignByFilter(driver, campaignName);
			 * 
			 * boolean isCampaignNamePresentAtPromotionalCampaignTab=fc.utobj().
			 * verifyCase(driver, ".//a[.='"+campaignName+"']"); if
			 * (isCampaignNamePresentAtPromotionalCampaignTab==false) {
			 * fc.utobj().throwsException(
			 * "was not able to verify Campaign At Campaign Center Status Driven Campaign Tab"
			 * ); }
			 */

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "crm" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Creation Of Template At CRM > Campaign Center > DashBoard", testCaseId = "TC_03_Create_Template")
	private void createTemplate() throws IOException, Exception, ParserConfigurationException, SAXException {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			CRMCampaignCenterPage pobj = new CRMCampaignCenterPage(driver);

			fc.utobj().printTestStep("Navigating to CRM > Campaign Center");
			fc.crm().crm_common().CRMCampaignCenterLnk(driver);

			fc.utobj().printTestStep("Create Template");
			fc.utobj().clickElement(driver, pobj.createLink);
			fc.utobj().clickElement(driver, pobj.createTemplateLink);

			// Code Your Template
			String templateName = fc.utobj().generateTestData(dataSet.get("templateName"));
			String emailSubject = fc.utobj().generateTestData(dataSet.get("emailSubject"));
			String plainTextEditor = fc.utobj().generateTestData(dataSet.get("plainTextEditor"));

			fc.utobj().printTestStep("Code Your Own Template");

			fc.utobj().clickElement(driver, pobj.codeYourOwn);
			fc.utobj().sendKeys(driver, pobj.templateName, templateName);
			fc.utobj().sendKeys(driver, pobj.mailSubject, emailSubject);
			fc.utobj().clickElement(driver, pobj.publicToAllUsersTemplate);
			fc.utobj().clickElement(driver, pobj.plainTextEmailType);
			fc.utobj().sendKeys(driver, pobj.textAreaPlainText, plainTextEditor);

			// Add Template In Folder
			fc.utobj().selectDropDownByValue(driver, pobj.addFolder, "addFolder");

			fc.commonMethods().switch_frameClass_newLayoutcboxIframe(driver);

			String folderName = fc.utobj().generateTestData(dataSet.get("folderName"));
			String folderAccessibility = dataSet.get("folderAccessibility");
			fc.utobj().sendKeys(driver, pobj.folderName, folderName);
			fc.utobj().selectDropDown(driver, pobj.folderAccessibility, folderAccessibility);

			fc.utobj().clickElement(driver, pobj.folderDescriptionLink);
			String folderDescription = fc.utobj().generateTestData(dataSet.get("folderDescription"));
			fc.utobj().sendKeys(driver, pobj.folderSummary, folderDescription);
			fc.utobj().clickElement(driver, pobj.saveBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().clickElement(driver, pobj.addAttchmentCircle);

			String fileName = dataSet.get("fileName");
			String filepath = fc.utobj().getFilePathFromTestData(fileName);
			fc.utobj().sendKeys(driver, pobj.attachmentName, filepath);

			fc.utobj().clickElement(driver, pobj.attachmentButton);

			fc.utobj().clickElement(driver, pobj.doneBtn);

			fc.utobj().printTestStep("Verify The Template At Corporate Templates Tab");
			fc.utobj().clickElement(driver, pobj.manageTemplate);

			// Verify At Corporate Template Tab
			searchTemplateByFilter(driver, templateName);

			boolean isTemplatePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () , '" + templateName + "')]");
			if (isTemplatePresent == false) {
				fc.utobj().throwsException("was not able to verify Template At Corporate Template Tab");
			}

			boolean isFolderNamePresentAtCorporateTab = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[contains(text () , '"
					+ templateName + "')]/ancestor::tr/td[contains(text () , '" + folderName + "')]");
			if (isFolderNamePresentAtCorporateTab == false) {
				fc.utobj().throwsException("was not able to verify Folder At Corporate Template Tab");
			}

			fc.utobj().printTestStep("Verify The Template At Folders Tab");
			fc.utobj().clickElement(driver, pobj.foldersTab);
			searchFoldersFilter(driver, folderName);

			boolean isFolderNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () , '" + folderName + "')]");
			if (isFolderNamePresent == false) {
				fc.utobj().throwsException("Was not able to verify Folder At Folders Tab");
			}

			fc.utobj().printTestStep("Verify The Template In Folder");
			fc.utobj().clickElement(driver, driver
					.findElement(By.xpath(".//td[contains(text () , '" + folderName + "')]/ancestor::tr/td/a[.='1']")));

			boolean isTemplatePresentAtCorporateTab = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () , '" + templateName + "')]");
			if (isTemplatePresentAtCorporateTab == false) {
				fc.utobj().throwsException(
						"was not able to verify Template At Corporate Template Tab Through Folder Tab");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "crm"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Creation Of Recipients Group At CRM > Campaign Center > DashBoard", testCaseId = "TC_04_Create_Recipients_Group")
	private void createRecipientsGroup() throws IOException, Exception, ParserConfigurationException, SAXException {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			CRMCampaignCenterPage pobj = new CRMCampaignCenterPage(driver);

			fc.utobj().printTestStep("Navigating to CRM > Campaign Center");
			fc.crm().crm_common().CRMCampaignCenterLnk(driver);

			fc.utobj().printTestStep("Create Recipients Group");
			fc.utobj().clickElement(driver, pobj.createLink);
			fc.utobj().clickElement(driver, pobj.recipientsGroupLnk);
			fc.utobj().switchFrame(driver, fc.utobj().getElement(driver, pobj.frame));

			String groupName = fc.utobj().generateTestData(dataSet.get("groupName"));
			fc.utobj().sendKeys(driver, pobj.groupName, groupName);
			fc.utobj().clickElement(driver, pobj.groupDescriptionLink);
			String groupDescription = fc.utobj().generateTestData(dataSet.get("groupDescription"));
			fc.utobj().sendKeys(driver, pobj.groupDescription, groupDescription);
			fc.utobj().selectDropDown(driver, pobj.selectGroupType, "Lead");
			fc.utobj().selectDropDownByValue(driver, pobj.selectPublicToAllUsers, "Global");
			fc.utobj().clickElement(driver, pobj.saveAndContinueBtn);
			fc.utobj().switchFrameToDefault(driver);

			// select Criteria

			fc.utobj().clickElement(driver, pobj.fieldSelectBtn);
			fc.utobj().sendKeys(driver, pobj.searchField, "Assign To");
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//*[@id='fc-drop-parentselColList']//span[.='Assign To']"));
			fc.utobj().clickElement(driver, pobj.fieldSelectBtn);

			// configure Criteria
			fc.utobj().selectDropDown(driver, pobj.assignToCriteria1, "In");
			fc.utobj().clickElement(driver, pobj.selectAssignToUserBtn);
			fc.utobj().clickElement(driver, pobj.selectAssignToSelectAll);
			fc.utobj().clickElement(driver, pobj.selectAssignToUserBtn);
			fc.utobj().clickElement(driver, pobj.continueBtn);
			fc.utobj().clickElement(driver, pobj.associateWithGroupBtn);

			// fc.utobj().switchFrame(driver,
			// fc.utobj().getElementByXpath(driver,".//iframe[@class='newLayoutcboxIframe']")));
			fc.utobj().switchFrame(driver, fc.utobj().getElement(driver, pobj.frame));

			fc.utobj().clickElement(driver, pobj.okBtn);
			fc.utobj().switchFrameToDefault(driver);
			new CRMGroupsPageTest().searchGroupsByFilter(driver, groupName, "View All");

			fc.utobj().printTestStep("Verify Group");
			boolean isGroupNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[.='" + groupName + "']");
			if (isGroupNamePresent == false) {
				fc.utobj().throwsException("was not able to verify Group Name");
			}
			boolean isGroupTypePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[.='" + groupName + "']/ancestor::tr/td[contains(text () ,'Lead')]");
			if (isGroupTypePresent == false) {
				fc.utobj().throwsException("was not able to verify Group Type");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	public void searchCampaignByFilter(WebDriver driver, String campaignName) throws Exception {
		CRMCampaignCenterPage pobj = new CRMCampaignCenterPage(driver);
		fc.utobj().clickElement(driver, pobj.showFilter);
		fc.utobj().sendKeys(driver, pobj.searchCampaign, campaignName);
		fc.utobj().clickElement(driver, pobj.searchFilterBtn);
		fc.utobj().clickElement(driver, pobj.showFilter);
	}

	public void searchTemplateByFilter(WebDriver driver, String templateName) throws Exception {
		CRMCampaignCenterPage pobj = new CRMCampaignCenterPage(driver);
		fc.utobj().clickElement(driver, pobj.showFilter);
		fc.utobj().sendKeys(driver, pobj.searchTemplate, templateName);
		fc.utobj().clickElement(driver, pobj.applyFilterAtGroup);
		fc.utobj().clickElement(driver, pobj.hideFilter);
	}

	public void searchFoldersFilter(WebDriver driver, String folderName) throws Exception {
		CRMCampaignCenterPage pobj = new CRMCampaignCenterPage(driver);
		fc.utobj().clickElement(driver, pobj.showFilter);
		fc.utobj().sendKeys(driver, pobj.searchFolder, folderName);
		fc.utobj().clickElement(driver, pobj.applyFilterAtGroup);
		fc.utobj().clickElement(driver, pobj.hideFilter);
	}

	public void searchCampaignByPromotionalFilter(WebDriver driver, String campaignName) throws Exception {
		CRMCampaignCenterPage pobj = new CRMCampaignCenterPage(driver);
		fc.utobj().clickElement(driver, pobj.showFilter);
		fc.utobj().sendKeys(driver, pobj.searchCampaign, campaignName);
		fc.utobj().selectDropDown(driver, pobj.campStatus, "Promotional");
		fc.utobj().clickElement(driver, pobj.searchFilterBtn);
	}

	public void searchCampaignByStatusDrivenFilter(WebDriver driver, String campaignName) throws Exception {
		CRMCampaignCenterPage pobj = new CRMCampaignCenterPage(driver);
		fc.utobj().clickElement(driver, pobj.showFilter);
		fc.utobj().sendKeys(driver, pobj.searchCampaign, campaignName);
		fc.utobj().selectDropDown(driver, pobj.campStatus, "Status-Driven");
		fc.utobj().clickElement(driver, pobj.searchFilterBtn);
	}

	// Create Campaign For Check the Smoke OF campain Center

	public void createCampaignGeneric(WebDriver driver, Map<String, String> config, String campaignName,
			String accessibility, String campaignType, String templateName, String emailSubject, String plainTextEditor)
			throws Exception {

		String testCaseId = "TC_Create_Campaign_01";

		if (fc.utobj().validate(testCaseId)) {
			try {

				CRMCampaignCenterPage pobj = new CRMCampaignCenterPage(driver);
				fc.utobj().clickElement(driver, pobj.campaignCenterLinks);

				fc.utobj().clickElement(driver, pobj.menuOptions);
				fc.utobj().clickElement(driver, pobj.campaignLinks);
				fc.utobj().clickElement(driver, pobj.createCampaignButton);
				fc.utobj().sleep(1000);
				fc.commonMethods().switch_frameClass_newLayoutcboxIframe(driver);
				fc.utobj().sleep(1000);
				fc.utobj().sendKeys(driver, pobj.campaignTitle, campaignName);
				// fc.utobj().clickElement(driver, pobj.emailCampaign);
				fc.utobj().selectDropDown(driver, pobj.campaignAccessibility, accessibility);

				if (campaignType.equalsIgnoreCase("Promotional")) {
					fc.utobj().clickElement(driver, pobj.promotionalTypeCampaign);

				} else if (campaignType.equalsIgnoreCase("Status Driven")) {
					fc.utobj().clickElement(driver, pobj.statusDrivenTypeCampaign);

				}

				fc.utobj().clickElement(driver, pobj.startBtn);
				fc.utobj().switchFrameToDefault(driver);

				// create Template :: Code Your Own

				fc.utobj().clickElement(driver, pobj.codeYourOwn);
				fc.utobj().sendKeys(driver, pobj.templateName, templateName);
				fc.utobj().sendKeys(driver, pobj.mailSubject, emailSubject);

				fc.utobj().clickElement(driver, pobj.plainTextEmailType);
				fc.utobj().sendKeys(driver, pobj.textAreaPlainText, plainTextEditor);

				fc.utobj().clickElement(driver, pobj.saveAndContinue);
				fc.utobj().clickElement(driver, pobj.associateLater);

				fc.utobj().clickElement(driver, pobj.finalizeAndLaunchCampaignBtn);

				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException("Was not able to Create Campaign");
		}
	}

	// Create Campaign For WorkFlow

	public void createCampaign(WebDriver driver, Map<String, String> config, String campaignName, String accessibility,
			String campaignType, String templateName, String emailSubject, String plainTextEditor) throws Exception {

		String testCaseId = "TC_Create_Campaign_For_WorkFlow";

		if (fc.utobj().validate(testCaseId)) {
			try {

				CRMCampaignCenterPage pobj = new CRMCampaignCenterPage(driver);
				fc.utobj().clickElement(driver, pobj.campaignCenterLinks);

				fc.utobj().clickElement(driver, pobj.menuOptions);
				fc.utobj().clickElement(driver, pobj.campaignLinks);
				fc.utobj().clickElement(driver, pobj.createCampaignButton);
				fc.commonMethods().switch_frameClass_newLayoutcboxIframe(driver);

				fc.utobj().sendKeys(driver, pobj.campaignTitle, campaignName);
				// fc.utobj().clickElement(driver, pobj.emailCampaign);

				if (accessibility.equalsIgnoreCase("Public to all Users")) {

					fc.utobj().selectDropDownByValue(driver, pobj.campaignAccessibility, "Global");

				} else if (accessibility.equalsIgnoreCase("Public to all Corporate Users")) {

					fc.utobj().selectDropDownByValue(driver, pobj.campaignAccessibility, "Public");

				} else if (accessibility.equalsIgnoreCase("Private")) {

					fc.utobj().selectDropDownByValue(driver, pobj.campaignAccessibility, "Private");
				}

				/*
				 * fc.utobj().selectDropDown(driver, pobj.campaignAccessibility,
				 * accessibility);
				 */

				if (campaignType.equalsIgnoreCase("Promotional")) {
					fc.utobj().clickElement(driver, pobj.promotionalTypeCampaign);

				} else if (campaignType.equalsIgnoreCase("Status Driven")) {
					fc.utobj().clickElement(driver, pobj.statusDrivenTypeCampaign);

				}

				fc.utobj().clickElement(driver, pobj.startBtn);
				fc.utobj().switchFrameToDefault(driver);

				// create Template :: Code Your Own

				fc.utobj().clickElement(driver, pobj.codeYourOwn);
				fc.utobj().sendKeys(driver, pobj.templateName, templateName);
				fc.utobj().sendKeys(driver, pobj.mailSubject, emailSubject);

				fc.utobj().clickElement(driver, pobj.plainTextEmailType);
				fc.utobj().sendKeys(driver, pobj.textAreaPlainText, plainTextEditor);

				fc.utobj().clickElement(driver, pobj.saveAndContinue);
				fc.utobj().clickElement(driver, pobj.associateLater);

				fc.utobj().clickElement(driver, pobj.finalizeAndLaunchCampaignBtn);

				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException("Was not able to Create Campaign For WorkFlow");
		}
	}
	
	public void clickonemailtamplate(WebDriver driver) throws Exception {
		CRMCampaignCenterPage pobj = new CRMCampaignCenterPage(driver);
		fc.utobj().clickElement(driver, pobj.campaignCenterLinks);
		fc.utobj().clickElement(driver, pobj.menuOptions);
		fc.utobj().clickElement(driver, pobj.templatesLink);
	}
	
	public void clickoncampaigntemplate(WebDriver driver) throws Exception {
		CRMCampaignCenterPage pobj = new CRMCampaignCenterPage(driver);
		fc.utobj().clickElement(driver, pobj.campaignCenterLinks);
		fc.utobj().clickElement(driver, pobj.menuOptions);
		fc.utobj().clickElement(driver, pobj.campaignLinks);
	}
	
	public void createCampaignGenericwithSqlite(WebDriver driver, Map<String, String> config, String campaignName,
			String accessibility, String campaignType, String templateName, String emailSubject, String plainTextEditor)
			throws Exception {

		String testCaseId = "TC_Create_Campaign_01";

		if (fc.utobj().validate(testCaseId)) {
			try {

				CRMCampaignCenterPage pobj = new CRMCampaignCenterPage(driver);
				fc.utobj().clickElement(driver, pobj.campaignCenterLinks);

				fc.utobj().clickElement(driver, pobj.menuOptions);
				fc.utobj().clickElement(driver, pobj.campaignLinks);
				fc.utobj().clickElement(driver, pobj.createCampaignButton);
				fc.utobj().sleep(1000);
				fc.commonMethods().switch_frameClass_newLayoutcboxIframe(driver);
				fc.utobj().sleep(30000);
				fc.utobj().sendKeys(driver, pobj.campaignTitle, campaignName);
				// fc.utobj().clickElement(driver, pobj.emailCampaign);
				fc.utobj().selectDropDown(driver, pobj.campaignAccessibility, accessibility);

				if (campaignType.equalsIgnoreCase("Promotional")) {
					fc.utobj().clickElement(driver, pobj.promotionalTypeCampaign);

				} else if (campaignType.equalsIgnoreCase("Status Driven")) {
					fc.utobj().clickElement(driver, pobj.statusDrivenTypeCampaign);

				}

				fc.utobj().clickElement(driver, pobj.startBtn);
				
				while(isAlertPresent(driver)) {
					
						Alert alert = driver.switchTo().alert();
						String campaignalreadypresentalert=alert.getText();
						if(campaignalreadypresentalert.contains("Campaign Name already exists.")) {
							fc.utobj().acceptAlertBox(driver);
							String againcampaignnamegenerated = fc.utobj().generateTestData(config.get("CorpcampaignName"));
							fc.utobj().sendKeys(driver, pobj.campaignTitle, againcampaignnamegenerated);
							fc.utobj().clickElement(driver, pobj.startBtn);
						}
				
				}		
				
				fc.utobj().switchFrameToDefault(driver);

				// create Template :: Code Your Own

				fc.utobj().clickElement(driver, pobj.codeYourOwn);
				fc.utobj().sendKeys(driver, pobj.templateName, templateName);
				fc.utobj().sendKeys(driver, pobj.mailSubject, emailSubject);

				fc.utobj().clickElement(driver, pobj.plainTextEmailType);
				fc.utobj().sendKeys(driver, pobj.textAreaPlainText, plainTextEditor);

				fc.utobj().clickElement(driver, pobj.saveAndContinue);
				fc.utobj().clickElement(driver, pobj.associateLater);

				fc.utobj().clickElement(driver, pobj.finalizeAndLaunchCampaignBtn);

				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException("Was not able to Create Campaign");
		}
	}
	
	//vipinS
	public void Create_campaign(WebDriver driver,String Campaign_name,String Campaign_Accessibility ,String Tempalte_name) throws Exception
	{
		FCHomePageTest fcHomePageTest = fc.commonMethods().getModules();
		CRMModule crm = new CRMModule();
		crm.crm_common().openCRMCampaignCenterPage(fcHomePageTest, driver);
		CRM_CampaginCenterPagelink add = new CRM_CampaginCenterPagelink();
		add.CreateCampaign(driver);
		CRM_CampaignAddpage addfill = new CRM_CampaignAddpage();
		CRM_CampaignAddGetSet camAdd = new CRM_CampaignAddGetSet();
		camAdd = crm.crm_common().defualtCampaignfill(camAdd);
		addfill.filldetails(driver, camAdd);
		addfill.clickStartbtn(driver);
		Thread.sleep(500);
		
		CRM_SelectandCustomizeTemplatepage custom = new CRM_SelectandCustomizeTemplatepage();
	
		custom.firstTemplateclick(driver);
		Thread.sleep(5000);
		
		CRM_TemplateInfoPageGetSet teminfo = new CRM_TemplateInfoPageGetSet();
		CRM_TemplateInfoPage info = new CRM_TemplateInfoPage();
		teminfo = crm.crm_common().defualtTemplateinfofill(teminfo);
		
		info.filldetails(driver, teminfo);
		info.savebtn(driver);
	
		Thread.sleep(500);
		info.saveAndcontinue(driver);
		Thread.sleep(500);
		
		CRM_CampaignSelectRecipientpage recipient = new CRM_CampaignSelectRecipientpage();
		CRM_CampaignSelectRecipientUI recipientUI = new CRM_CampaignSelectRecipientUI(driver);
		recipient.AssociateLater(driver);
		Thread.sleep(500);
		
		CRM_CampaignFinalandLunchpage lunch = new CRM_CampaignFinalandLunchpage();
		CRM_CampaignFinalandLunchUI lunchUI = new CRM_CampaignFinalandLunchUI(driver);
		fc.utobj().selectDropDown(driver, lunchUI.TimeZone, "GMT +05:30 India");
	    String	CampaignName = lunchUI.campaignName.getText();
		lunch.clickFinalUpperbtn(driver);
		String confirm = lunchUI.confirmpagetext.getText();
		
		
		if(confirm.equalsIgnoreCase(CampaignName+" "+"has been scheduled successfully"))
		{
			System.out.println("Campaign Add Successfully ");
		}else {
			System.out.println("campaign not added");
		}
	}
	
	public boolean isAlertPresent(WebDriver driver) 
	{ 
	    try 
	    { 
			driver.switchTo().alert(); 
	        return true; 
	    }   // try 
	    catch (NoAlertPresentException Ex) 
	    { 
	        return false; 
	    }   // catch 
	}
	
	
}
