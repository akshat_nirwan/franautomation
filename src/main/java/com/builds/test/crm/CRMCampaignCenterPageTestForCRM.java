package com.builds.test.crm;

import java.util.Map;

import org.openqa.selenium.WebDriver;

import com.builds.uimaps.crm.CRMCampaignCenterPage;
import com.builds.utilities.FranconnectUtil;

public class CRMCampaignCenterPageTestForCRM {
	FranconnectUtil fc = new FranconnectUtil();

	public void createCampaign(WebDriver driver, String campaignName, String campaignDescription,
			String campaignAccessibility, String category, String templateName, String mailSubject, String templateText,
			Map<String, String> config) throws Exception {

		String testCaseId = "TC_Create_Campaign";

		if (fc.utobj().validate(testCaseId)) {
			try {
				CRMCampaignCenterPage pobj = new CRMCampaignCenterPage(driver);
				fc.crm().crm_common().CRMCampaignCenterLnk(driver);
				fc.utobj().clickElement(driver, pobj.createBtn);
				fc.utobj().clickElement(driver, pobj.campaignLnk);
				fc.utobj().switchFrame(driver,
						fc.utobj().getElementByXpath(driver, ".//iframe[@class='newLayoutcboxIframe']"));
				fc.utobj().sendKeys(driver, pobj.campaignName, campaignName);
				fc.utobj().clickElement(driver, pobj.addDescription);
				fc.utobj().sendKeys(driver, pobj.campaignDescription, campaignDescription);
				fc.utobj().selectDropDown(driver, pobj.campaignAccessibility, campaignAccessibility);
				fc.utobj().selectDropDown(driver, pobj.category, category);
				fc.utobj().clickElement(driver, pobj.campaignType);
				fc.utobj().clickElement(driver, pobj.startBtn);
				fc.utobj().switchFrameToDefault(driver);
				fc.utobj().clickElement(driver, pobj.codeYourOwn);
				fc.utobj().sendKeys(driver, pobj.templateName, templateName);
				fc.utobj().sendKeys(driver, pobj.mailSubject, mailSubject);
				fc.utobj().clickElement(driver, pobj.planeText);
				fc.utobj().sendKeys(driver, pobj.textArea, templateText);
				fc.utobj().clickElement(driver, pobj.saveAndContinue);
				fc.utobj().clickElement(driver, pobj.associateLater);
				fc.utobj().clickElement(driver, pobj.finalizeAndLaunchCampaignBtn);

				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException("was not able to create Campaign :: CRM > Campaign Center");

		}
	}
}
