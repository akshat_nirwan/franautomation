package com.builds.test.crm;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.xml.parsers.ParserConfigurationException;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;
import org.testng.Reporter;
import org.testng.annotations.Test;
import org.xml.sax.SAXException;

import com.builds.test.admin.AdminFranchiseLocationAddFranchiseLocationPageTest;
import com.builds.test.admin.AdminUsersManageCorporateUsersAddCorporateUserPageTest;
import com.builds.test.admin.AdminUsersManageDivisionalUsersAddDivisionalUsersPageTest;
import com.builds.test.admin.AdminUsersManageManageFranchiseUsersPageTest;
import com.builds.test.admin.AdminUsersManageRegionalUsersAddRegionalUserPageTest;
import com.builds.test.common.CorporateUser;
import com.builds.uimaps.campaigncentercrm.CRMCampaignCenterCampaignsPage;
import com.builds.uimaps.crm.CRMContactsPage;
import com.builds.uimaps.crm.CRMLeadsPage;
import com.builds.utilities.FranconnectUtil;
import com.builds.utilities.TestCase;

public class CRMContact_AreaMergeTestCase {

	FranconnectUtil fc = new FranconnectUtil();

	@Test(groups = { "crm", "crmcontactAreaMerge", "TC_Contact_CRM_MergeCase"})
	@TestCase(createdOn = "2018-01-16", updatedOn = "2018-01-16", testCaseId = "TC_Contact_CRM_MergeCase", testCaseDescription = "To Verify merging contacts behaviours.")
	private void crmInfoFillAreaMerge_1() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		Map<String, String> dataSetCustom = null;
		String testCaseIdInternal = "" + testCaseId;
		Map<String, String> printInfo = null;
		try {
			driver = fc.loginpage().login(driver);
			CRMContactsPage pobj = new CRMContactsPage(driver);

			fc.utobj().printTestStep("Navigate To Admin Users Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			String emailId = "crmautomation@staffex.com";
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUser = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporateUser.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			fc.utobj().printTestStep("Navigate To CRM > Contact > Contacts Summary");
			fc.utobj().printTestStep("Add First Contact");
			fc.crm().crm_common().CRMContactsLnk(driver);
			fc.utobj().clickElement(driver, pobj.addNew);
			fc.utobj().clickElement(driver, pobj.contactLnk);

			try {

				boolean isNewStatus = fc.utobj().verifyElementOnVisible_ByXpath(driver,
						".//td[contains(text () ,'Status')]/ancestor::tr/td[contains(text () ,'New')]");
				if (isNewStatus == false) {
					fc.utobj().throwsException("was not able to verify Default Status As New.");
				}
			} catch (Exception e) {

			}

			testCaseIdInternal = "TC_CRM_QA_Contact_Details";
			dataSetCustom = fc.utobj().readTestData("crm", testCaseIdInternal);
			String firstName = fc.utobj().generateTestData("Aa");
			String lastName = fc.utobj().generateTestData("Bb");
			dataSetCustom.put("contactFirstName", firstName);
			dataSetCustom.put("contactLastName", lastName);
			dataSetCustom.put("contactType", "Contacts");
			dataSetCustom.put("cmLeadTypeID", "Select");
			dataSetCustom.put("cmSource2ID", "Internet");
			dataSetCustom.put("cmSource3ID", "BISON");
			dataSetCustom.put("accountName", "");
			dataSetCustom.put("contactOwnerID2", corpUser.getuserFullName());

			printInfo = fillFormDataWithDataset(driver, config, testCaseIdInternal, dataSetCustom);
			// Reporter.log("Map == for ="+testCaseIdInternal+"==="+printInfo);

			if (printInfo != null && printInfo.size() > 0) {
			}

			fc.utobj().clickElement(driver, pobj.contactsLinks);

			systemExactSearch(driver, firstName + " " + lastName);

			fc.utobj().printTestStep("Verify Contact Name in Contact Summary.");
			boolean isLeadNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains( text(),'" + firstName + " " + lastName + "')]");
			if (isLeadNamePresent == false) {
				fc.utobj().throwsException("was not able to verify First Contact Name");
			}

			fc.utobj().printTestStep("Navigate To CRM > Contact > Contacts Summary");
			fc.utobj().printTestStep("Add Second Contact");
			fc.crm().crm_common().CRMContactsLnk(driver);
			fc.utobj().clickElement(driver, pobj.addNew);
			fc.utobj().clickElement(driver, pobj.contactLnk);

			testCaseIdInternal = "TC_CRM_QA_Contact_Details";
			dataSetCustom = fc.utobj().readTestData("crm", testCaseIdInternal);
			String firstNameSecond = fc.utobj().generateTestData("Cc");
			String lastNameSecond = fc.utobj().generateTestData("Dd");
			dataSetCustom.put("contactFirstName", firstNameSecond);
			dataSetCustom.put("contactLastName", lastNameSecond);
			dataSetCustom.put("contactType", "Contacts");
			dataSetCustom.put("cmLeadTypeID", "Select");
			dataSetCustom.put("cmSource2ID", "Internet");
			dataSetCustom.put("cmSource3ID", "BISON");
			dataSetCustom.put("accountName", "");
			dataSetCustom.put("contactOwnerID2", corpUser.getuserFullName());

			printInfo = fillFormDataWithDataset(driver, config, testCaseIdInternal, dataSetCustom);
			// Reporter.log("Map == for ="+testCaseIdInternal+"==="+printInfo);

			if (printInfo != null && printInfo.size() > 0) {
				// System.out.println("printInfo=Second="+printInfo);

				// System.out.println("printInfo=Second Email
				// ="+printInfo.get("emailIds"));
			}

			fc.utobj().clickElement(driver, pobj.contactsLinks);

			systemExactSearch(driver, firstNameSecond + " " + lastNameSecond);

			fc.utobj().printTestStep("Verify Second Contact Name in Contact Summary.");
			isLeadNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains( text(),'" + firstNameSecond + " " + lastNameSecond + "')]");
			if (isLeadNamePresent == false) {
				fc.utobj().throwsException("was not able to verify Second Contact Name.");
			}

			fc.utobj().clickElement(driver, pobj.contactsLinks);
			searchLeadByOwner(driver, corpUser.getuserFullName(), "Contacts");

			fc.utobj().printTestStep("Merge Contacts");
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//a[contains(text () ,'" + firstName
					+ " " + lastName + "')]/ancestor::tr/td/input[@name='checkb']"));
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//a[contains(text () ,'"
					+ firstNameSecond + " " + lastNameSecond + "')]/ancestor::tr/td/input[@name='checkb']"));
			fc.utobj().selectActionMenuItemsWithTagA(driver, "Merge Contacts");

			if (!fc.utobj().isSelected(driver, pobj.secondNameSelect)) {
				fc.utobj().clickElement(driver, pobj.secondNameSelect);
			}

			String secondName = fc.utobj().getElementByXpath(driver, ".//*[@value='second' and @name='name']/..")
					.getText().trim();
			String firstNameArchived = fc.utobj().getElementByXpath(driver, ".//*[@value='first' and @name='name']/..")
					.getText().trim();

			fc.utobj().sendKeys(driver, pobj.remarksAtMerge, "Merge Remarks of Contacts.");
			fc.utobj().clickElement(driver, pobj.mergeBtn);

			fc.utobj().clickElement(driver, pobj.contactsLinks);
			searchLeadByOwner(driver, corpUser.getuserFullName(), "Contacts");

			fc.utobj().webDriverWait(driver, ".//a[contains(text () ,'" + secondName + "')]");

			fc.utobj().printTestStep("Verify Merge Contacts");
			boolean isMergeContactNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondName + "')]");
			if (isMergeContactNamePresent == false) {
				fc.utobj().throwsException("was not able to verify Contact Name after Merge Contacts.");
			}

			boolean isMergeiconPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//*[contains(text () , '" + secondName
					+ "')]/ancestor::tr/td/img[contains(@src, 'lead_merge.gif')]");
			if (isMergeiconPresent == false) {
				fc.utobj().throwsException("was not able to verify Contact Merge icon with Contact Name.");
			}

			fc.utobj().printTestStep("Verify Merge Contacts ToolTip.");
			WebElement elementToolTip = fc.utobj().getElementByXpath(driver, ".//*[contains(text () , '" + secondName
					+ "')]/ancestor::tr/td/img[contains(@src, 'lead_merge.gif')]");
			Thread.sleep(2000);
			Actions ac1 = new Actions(driver);
			ac1.clickAndHold().moveToElement(elementToolTip);
			Thread.sleep(2000);
			ac1.moveToElement(elementToolTip).build().perform();
			Thread.sleep(1000);
			// ac1.moveToElement(elementToolTip).build().perform();

			String leadMergetoolTipText = fc.utobj().getElementByXpath(driver, ".//*[@id='contentDiv']").getText()
					.trim();

			if (leadMergetoolTipText.indexOf("Merging of Contacts") != -1) {
				fc.utobj().printTestStep("Verify ToolTip Indicates the Contacts is added through Merging of Leads.");
			} else {
				fc.utobj().throwsException("Was not able to verify ToolTip of Merging of Contacts");
			}

			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//a[contains(text () ,'" + secondName + "')]"));
			boolean isCNamePresentAtPrimaryInfo = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () ,'Name')]/ancestor::tr/td[contains(text () ,'" + firstNameSecond + "')]");
			if (isCNamePresentAtPrimaryInfo == false) {
				fc.utobj().throwsException("was not able to verify Contact Name at primary info page");
			}

			fc.utobj().printTestStep("Verify archive contacts at Contact Summary page");
			fc.utobj().clickElement(driver, pobj.searchLink);
			// fc.utobj().clickElement(driver, pobj.leadsTab);
			if (!fc.utobj().isSelected(driver, pobj.archiveLeads)) {
				fc.utobj().clickElement(driver, pobj.archiveLeads);
			}

			fc.utobj().setToDefault(driver, pobj.assignTo);
			WebElement elementInput = fc.utobj().getElement(driver, pobj.assignTo)
					.findElement(By.xpath("./div/div/input"));
			fc.utobj().selectValFromMultiSelect(driver, pobj.assignTo, elementInput, corpUser.getuserFullName());
			fc.utobj().clickElement(driver, pobj.searchBtnAtSearch);

			fc.utobj().printTestStep("Verify archive contact At Search Page");
			boolean isNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firstName + " " + lastName + "')]");
			if (isNamePresent == false) {
				fc.utobj().throwsException("was not able to verify archive contact");
			}
			isNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firstNameSecond + " " + lastNameSecond + "')]");
			if (isNamePresent == false) {
				fc.utobj().throwsException("was not able to verify archive contact");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "crm", "crmcontactAreaMerge"})
	@TestCase(createdOn = "2018-01-16", updatedOn = "2018-01-16", testCaseId = "TC_Contact_CRM_MergeCase_Bottom", testCaseDescription = "To Verify merging contacts behaviours from Bottom Button .")
	private void crmInfoFillAreaMerge_2() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		Map<String, String> dataSetCustom = null;
		String testCaseIdInternal = "" + testCaseId;
		Map<String, String> printInfo = null;
		try {
			driver = fc.loginpage().login(driver);
			CRMContactsPage pobj = new CRMContactsPage(driver);

			fc.utobj().printTestStep("Navigate To Admin Users Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			String emailId = "crmautomation@staffex.com";
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUser = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporateUser.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			fc.utobj().printTestStep("Navigate To CRM > Contact > Contacts Summary");
			fc.utobj().printTestStep("Add First Contact");
			fc.crm().crm_common().CRMContactsLnk(driver);
			fc.utobj().clickElement(driver, pobj.addNew);
			fc.utobj().clickElement(driver, pobj.contactLnk);

			testCaseIdInternal = "TC_CRM_QA_Contact_Details";
			dataSetCustom = fc.utobj().readTestData("crm", testCaseIdInternal);
			String firstName = fc.utobj().generateTestData("Aa");
			String lastName = fc.utobj().generateTestData("Bb");
			dataSetCustom.put("contactFirstName", firstName);
			dataSetCustom.put("contactLastName", lastName);
			dataSetCustom.put("contactType", "Contacts");
			dataSetCustom.put("cmLeadTypeID", "Select");
			dataSetCustom.put("cmSource2ID", "Internet");
			dataSetCustom.put("cmSource3ID", "BISON");
			dataSetCustom.put("accountName", "");
			dataSetCustom.put("contactOwnerID2", corpUser.getuserFullName());

			printInfo = fillFormDataWithDataset(driver, config, testCaseIdInternal, dataSetCustom);
			// Reporter.log("Map == for ="+testCaseIdInternal+"==="+printInfo);

			if (printInfo != null && printInfo.size() > 0) {
				// System.out.println("printInfo=First="+printInfo);

				// System.out.println("printInfo=First Email
				// ="+printInfo.get("emailIds"));
			}

			fc.utobj().clickElement(driver, pobj.contactsLinks);

			systemExactSearch(driver, firstName + " " + lastName);

			fc.utobj().printTestStep("Verify Contact Name in Contact Summary.");
			boolean isLeadNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains( text(),'" + firstName + " " + lastName + "')]");
			if (isLeadNamePresent == false) {
				fc.utobj().throwsException("was not able to verify First Contact Name");
			}

			fc.utobj().printTestStep("Navigate To CRM > Contact > Contacts Summary");
			fc.utobj().printTestStep("Add Second Contact");
			fc.crm().crm_common().CRMContactsLnk(driver);
			fc.utobj().clickElement(driver, pobj.addNew);
			fc.utobj().clickElement(driver, pobj.contactLnk);

			testCaseIdInternal = "TC_CRM_QA_Contact_Details";
			dataSetCustom = fc.utobj().readTestData("crm", testCaseIdInternal);
			String firstNameSecond = fc.utobj().generateTestData("Cc");
			String lastNameSecond = fc.utobj().generateTestData("Dd");
			dataSetCustom.put("contactFirstName", firstNameSecond);
			dataSetCustom.put("contactLastName", lastNameSecond);
			dataSetCustom.put("contactType", "Contacts");
			dataSetCustom.put("cmLeadTypeID", "Select");
			dataSetCustom.put("cmSource2ID", "Internet");
			dataSetCustom.put("cmSource3ID", "BISON");
			dataSetCustom.put("accountName", "");
			dataSetCustom.put("contactOwnerID2", corpUser.getuserFullName());

			printInfo = fillFormDataWithDataset(driver, config, testCaseIdInternal, dataSetCustom);
			// Reporter.log("Map == for ="+testCaseIdInternal+"==="+printInfo);

			if (printInfo != null && printInfo.size() > 0) {
				// System.out.println("printInfo=Second="+printInfo);

				// System.out.println("printInfo=Second Email
				// ="+printInfo.get("emailIds"));
			}

			fc.utobj().clickElement(driver, pobj.contactsLinks);

			systemExactSearch(driver, firstNameSecond + " " + lastNameSecond);

			fc.utobj().printTestStep("Verify Second Contact Name in Contact Summary.");
			isLeadNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains( text(),'" + firstNameSecond + " " + lastNameSecond + "')]");
			if (isLeadNamePresent == false) {
				fc.utobj().throwsException("was not able to verify Second Contact Name.");
			}

			fc.utobj().clickElement(driver, pobj.contactsLinks);
			searchLeadByOwner(driver, corpUser.getuserFullName(), "Contacts");

			fc.utobj().printTestStep("Merge Contacts from Bottom Button");
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//a[contains(text () ,'" + firstName
					+ " " + lastName + "')]/ancestor::tr/td/input[@name='checkb']"));
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//a[contains(text () ,'"
					+ firstNameSecond + " " + lastNameSecond + "')]/ancestor::tr/td/input[@name='checkb']"));

			// fc.utobj().selectActionMenuItemsWithTagA(driver, "Merge
			// Contacts");
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//input[@value='Merge Contacts']"));

			if (!fc.utobj().isSelected(driver, pobj.secondNameSelect)) {
				fc.utobj().clickElement(driver, pobj.secondNameSelect);
			}

			String secondName = fc.utobj().getElementByXpath(driver, ".//*[@value='second' and @name='name']/..")
					.getText().trim();
			String firstNameArchived = fc.utobj().getElementByXpath(driver, ".//*[@value='first' and @name='name']/..")
					.getText().trim();

			fc.utobj().sendKeys(driver, pobj.remarksAtMerge, "Merge Remarks of Contacts.");
			fc.utobj().clickElement(driver, pobj.mergeBtn);

			fc.utobj().clickElement(driver, pobj.contactsLinks);
			searchLeadByOwner(driver, corpUser.getuserFullName(), "Contacts");

			fc.utobj().webDriverWait(driver, ".//a[contains(text () ,'" + secondName + "')]");

			fc.utobj().printTestStep("Verify Merge Contacts");
			boolean isMergeContactNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondName + "')]");
			if (isMergeContactNamePresent == false) {
				fc.utobj().throwsException("was not able to verify Contact Name after Merge Contacts.");
			}

			boolean isMergeiconPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//*[contains(text () , '" + secondName
					+ "')]/ancestor::tr/td/img[contains(@src, 'lead_merge.gif')]");
			if (isMergeiconPresent == false) {
				fc.utobj().throwsException("was not able to verify Contact Merge icon with Contact Name.");
			}

			fc.utobj().printTestStep("Verify Merge Contacts ToolTip.");
			WebElement elementToolTip = fc.utobj().getElementByXpath(driver, ".//*[contains(text () , '" + secondName
					+ "')]/ancestor::tr/td/img[contains(@src, 'lead_merge.gif')]");
			Thread.sleep(2000);
			Actions ac1 = new Actions(driver);
			ac1.clickAndHold().moveToElement(elementToolTip);
			Thread.sleep(2000);
			ac1.moveToElement(elementToolTip).build().perform();
			Thread.sleep(1000);
			// ac1.moveToElement(elementToolTip).build().perform();

			String leadMergetoolTipText = fc.utobj().getElementByXpath(driver, ".//*[@id='contentDiv']").getText()
					.trim();

			if (leadMergetoolTipText.indexOf("Merging of Contacts") != -1) {
				fc.utobj().printTestStep("Verify ToolTip Indicates the Lead is added through Merging of Leads.");
			} else {
				fc.utobj().throwsException("Was not able to verify ToolTip of Merging of Leads");
			}

			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//a[contains(text () ,'" + secondName + "')]"));
			boolean isCNamePresentAtPrimaryInfo = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () ,'Name')]/ancestor::tr/td[contains(text () ,'" + firstNameSecond + "')]");
			if (isCNamePresentAtPrimaryInfo == false) {
				fc.utobj().throwsException("was not able to verify Contact Name at primary info page");
			}

			fc.utobj().printTestStep("Verify archive contacts at Contact Summary page");
			fc.utobj().clickElement(driver, pobj.searchLink);
			// fc.utobj().clickElement(driver, pobj.leadsTab);
			if (!fc.utobj().isSelected(driver, pobj.archiveLeads)) {
				fc.utobj().clickElement(driver, pobj.archiveLeads);
			}

			fc.utobj().setToDefault(driver, pobj.assignTo);
			WebElement elementInput = fc.utobj().getElement(driver, pobj.assignTo)
					.findElement(By.xpath("./div/div/input"));
			fc.utobj().selectValFromMultiSelect(driver, pobj.assignTo, elementInput, corpUser.getuserFullName());
			fc.utobj().clickElement(driver, pobj.searchBtnAtSearch);

			fc.utobj().printTestStep("Verify archive contact At Search Page");
			boolean isNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firstName + " " + lastName + "')]");
			if (isNamePresent == false) {
				fc.utobj().throwsException("was not able to verify archive contact");
			}
			isNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firstNameSecond + " " + lastNameSecond + "')]");
			if (isNamePresent == false) {
				fc.utobj().throwsException("was not able to verify archive contact");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "crm", "crmcontactAreaMerge" })
	@TestCase(createdOn = "2018-01-16", updatedOn = "2018-01-16", testCaseId = "TC_Contact_CRM_MergeCase_OnlyTwo", testCaseDescription = "To verify that User can merge only Two lead at a time.")
	private void crmInfoFillAreaMerge_3() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		Map<String, String> dataSetCustom = null;
		String testCaseIdInternal = "" + testCaseId;
		Map<String, String> printInfo = null;
		try {
			driver = fc.loginpage().login(driver);
			CRMContactsPage pobj = new CRMContactsPage(driver);
			;

			fc.utobj().printTestStep("Navigate To Admin Users Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			String emailId = "crmautomation@staffex.com";
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUser = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporateUser.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			fc.utobj().printTestStep("Navigate To CRM > Contact > Contacts Summary");
			fc.utobj().printTestStep("Add First Contact");
			fc.crm().crm_common().CRMContactsLnk(driver);
			fc.utobj().clickElement(driver, pobj.addNew);
			fc.utobj().clickElement(driver, pobj.contactLnk);

			testCaseIdInternal = "TC_CRM_QA_Contact_Details";
			dataSetCustom = fc.utobj().readTestData("crm", testCaseIdInternal);
			String firstName = fc.utobj().generateTestData("Aa");
			String lastName = fc.utobj().generateTestData("Bb");
			dataSetCustom.put("contactFirstName", firstName);
			dataSetCustom.put("contactLastName", lastName);
			dataSetCustom.put("contactType", "Contacts");
			dataSetCustom.put("cmLeadTypeID", "Select");
			dataSetCustom.put("cmSource2ID", "Internet");
			dataSetCustom.put("cmSource3ID", "BISON");
			dataSetCustom.put("accountName", "");
			dataSetCustom.put("contactOwnerID2", corpUser.getuserFullName());

			printInfo = fillFormDataWithDataset(driver, config, testCaseIdInternal, dataSetCustom);
			// Reporter.log("Map == for ="+testCaseIdInternal+"==="+printInfo);

			if (printInfo != null && printInfo.size() > 0) {
				// System.out.println("printInfo=First="+printInfo);

				// System.out.println("printInfo=First Email
				// ="+printInfo.get("emailIds"));
			}

			fc.utobj().clickElement(driver, pobj.contactsLinks);

			systemExactSearch(driver, firstName + " " + lastName);

			fc.utobj().printTestStep("Verify Contact Name in Contact Summary.");
			boolean isLeadNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains( text(),'" + firstName + " " + lastName + "')]");
			if (isLeadNamePresent == false) {
				fc.utobj().throwsException("was not able to verify First Contact Name");
			}

			fc.utobj().printTestStep("Navigate To CRM > Contact > Contacts Summary");
			fc.utobj().printTestStep("Add Second Contact");
			fc.crm().crm_common().CRMContactsLnk(driver);
			fc.utobj().clickElement(driver, pobj.addNew);
			fc.utobj().clickElement(driver, pobj.contactLnk);

			testCaseIdInternal = "TC_CRM_QA_Contact_Details";
			dataSetCustom = fc.utobj().readTestData("crm", testCaseIdInternal);
			String firstNameSecond = fc.utobj().generateTestData("Cc");
			String lastNameSecond = fc.utobj().generateTestData("Dd");
			dataSetCustom.put("contactFirstName", firstNameSecond);
			dataSetCustom.put("contactLastName", lastNameSecond);
			dataSetCustom.put("contactType", "Contacts");
			dataSetCustom.put("cmLeadTypeID", "Select");
			dataSetCustom.put("cmSource2ID", "Internet");
			dataSetCustom.put("cmSource3ID", "BISON");
			dataSetCustom.put("accountName", "");
			dataSetCustom.put("contactOwnerID2", corpUser.getuserFullName());

			printInfo = fillFormDataWithDataset(driver, config, testCaseIdInternal, dataSetCustom);
			// Reporter.log("Map == for ="+testCaseIdInternal+"==="+printInfo);

			if (printInfo != null && printInfo.size() > 0) {
				// System.out.println("printInfo=Second="+printInfo);

				// System.out.println("printInfo=Second Email
				// ="+printInfo.get("emailIds"));
			}

			fc.utobj().clickElement(driver, pobj.contactsLinks);

			systemExactSearch(driver, firstNameSecond + " " + lastNameSecond);

			fc.utobj().printTestStep("Verify Second Contact Name in Contact Summary.");
			isLeadNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains( text(),'" + firstNameSecond + " " + lastNameSecond + "')]");
			if (isLeadNamePresent == false) {
				fc.utobj().throwsException("was not able to verify Second Contact Name.");
			}

			fc.utobj().printTestStep("Navigate To CRM > Contact > Contacts Summary");
			fc.utobj().printTestStep("Add Third Lead");
			fc.crm().crm_common().CRMContactsLnk(driver);
			fc.utobj().clickElement(driver, pobj.addNew);
			fc.utobj().clickElement(driver, pobj.contactLnk);

			testCaseIdInternal = "TC_CRM_QA_Contact_Details";
			dataSetCustom = fc.utobj().readTestData("crm", testCaseIdInternal);
			String firstNameThird = fc.utobj().generateTestData("Ee");
			String lastNameThird = fc.utobj().generateTestData("Ff");
			dataSetCustom.put("contactFirstName", firstNameThird);
			dataSetCustom.put("contactLastName", lastNameThird);
			dataSetCustom.put("contactType", "Contacts");
			dataSetCustom.put("cmLeadTypeID", "Select");
			dataSetCustom.put("cmSource2ID", "Internet");
			dataSetCustom.put("cmSource3ID", "BISON");
			dataSetCustom.put("accountName", "");
			dataSetCustom.put("contactOwnerID2", corpUser.getuserFullName());

			printInfo = fillFormDataWithDataset(driver, config, testCaseIdInternal, dataSetCustom);
			// Reporter.log("Map == for ="+testCaseIdInternal+"==="+printInfo);

			if (printInfo != null && printInfo.size() > 0) {
				// System.out.println("printInfo=Second="+printInfo);

				// System.out.println("printInfo=Second Email
				// ="+printInfo.get("emailIds"));
			}
			fc.utobj().clickElement(driver, pobj.contactsLinks);

			systemExactSearch(driver, firstNameThird + " " + lastNameThird);

			fc.utobj().printTestStep("Verify Third Contact Name in Contact Summary.");
			isLeadNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains( text(),'" + firstNameThird + " " + lastNameThird + "')]");
			if (isLeadNamePresent == false) {
				fc.utobj().throwsException("was not able to verify Third Contact Name.");
			}

			fc.utobj().clickElement(driver, pobj.contactsLinks);
			searchLeadByOwner(driver, corpUser.getuserFullName(), "Contacts");

			fc.utobj().printTestStep("Merge Contacts from Bottom Button");
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//a[contains(text () ,'" + firstName
					+ " " + lastName + "')]/ancestor::tr/td/input[@name='checkb']"));
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//a[contains(text () ,'"
					+ firstNameSecond + " " + lastNameSecond + "')]/ancestor::tr/td/input[@name='checkb']"));
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//a[contains(text () ,'"
					+ firstNameThird + " " + lastNameThird + "')]/ancestor::tr/td/input[@name='checkb']"));

			// fc.utobj().selectActionMenuItemsWithTagA(driver, "Merge
			// Contacts");
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//input[@value='Merge Contacts']"));

			String alertText = fc.utobj().acceptAlertBox(driver);

			if (alertText.indexOf("two Contacts") != -1) {
				fc.utobj().printTestStep("Verify Alert Text Please select two Contact to Merge.");
			} else {
				fc.utobj().throwsException("was not able to verify Alert Text to select only two Contact to Merge.");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "crm", "crmcontactAreaMerge" })
	@TestCase(createdOn = "2018-01-16", updatedOn = "2018-01-16", testCaseId = "TC_Contact_CRM_MergeCase_Validation", testCaseDescription = "To verify validation message for Contact Merge option if user not select any Lead")
	private void crmInfoFillAreaMerge_4() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		Map<String, String> dataSetCustom = null;
		String testCaseIdInternal = "" + testCaseId;
		Map<String, String> printInfo = null;
		try {
			driver = fc.loginpage().login(driver);
			CRMContactsPage pobj = new CRMContactsPage(driver);

			fc.utobj().printTestStep("Navigate To Admin Users Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			String emailId = "crmautomation@staffex.com";
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUser = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporateUser.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			fc.utobj().printTestStep("Navigate To CRM > Contact > Contacts Summary");
			fc.utobj().printTestStep("Add First Contact");
			fc.crm().crm_common().CRMContactsLnk(driver);
			fc.utobj().clickElement(driver, pobj.addNew);
			fc.utobj().clickElement(driver, pobj.contactLnk);

			testCaseIdInternal = "TC_CRM_QA_Contact_Details";
			dataSetCustom = fc.utobj().readTestData("crm", testCaseIdInternal);
			String firstName = fc.utobj().generateTestData("Aa");
			String lastName = fc.utobj().generateTestData("Bb");
			dataSetCustom.put("contactFirstName", firstName);
			dataSetCustom.put("contactLastName", lastName);
			dataSetCustom.put("contactType", "Contacts");
			dataSetCustom.put("cmLeadTypeID", "Select");
			dataSetCustom.put("cmSource2ID", "Internet");
			dataSetCustom.put("cmSource3ID", "BISON");
			dataSetCustom.put("accountName", "");
			dataSetCustom.put("contactOwnerID2", corpUser.getuserFullName());

			printInfo = fillFormDataWithDataset(driver, config, testCaseIdInternal, dataSetCustom);
			// Reporter.log("Map == for ="+testCaseIdInternal+"==="+printInfo);

			if (printInfo != null && printInfo.size() > 0) {
				// System.out.println("printInfo=First="+printInfo);

				// System.out.println("printInfo=First Email
				// ="+printInfo.get("emailIds"));
			}

			fc.utobj().clickElement(driver, pobj.contactsLinks);

			systemExactSearch(driver, firstName + " " + lastName);

			fc.utobj().printTestStep("Verify Contact Name in Contact Summary.");
			boolean isLeadNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains( text(),'" + firstName + " " + lastName + "')]");
			if (isLeadNamePresent == false) {
				fc.utobj().throwsException("was not able to verify First Contact Name");
			}

			fc.utobj().printTestStep("Navigate To CRM > Contact > Contacts Summary");
			fc.utobj().printTestStep("Add Second Contact");
			fc.crm().crm_common().CRMContactsLnk(driver);
			fc.utobj().clickElement(driver, pobj.addNew);
			fc.utobj().clickElement(driver, pobj.contactLnk);

			testCaseIdInternal = "TC_CRM_QA_Contact_Details";

			dataSetCustom = fc.utobj().readTestData("crm", testCaseIdInternal);
			String firstNameSecond = fc.utobj().generateTestData("Cc");
			String lastNameSecond = fc.utobj().generateTestData("Dd");
			dataSetCustom.put("contactFirstName", firstNameSecond);
			dataSetCustom.put("contactLastName", lastNameSecond);
			dataSetCustom.put("contactType", "Contacts");
			dataSetCustom.put("cmLeadTypeID", "Select");
			dataSetCustom.put("cmSource2ID", "Internet");
			dataSetCustom.put("cmSource3ID", "BISON");
			dataSetCustom.put("accountName", "");
			dataSetCustom.put("contactOwnerID2", corpUser.getuserFullName());

			printInfo = fillFormDataWithDataset(driver, config, testCaseIdInternal, dataSetCustom);
			// Reporter.log("Map == for ="+testCaseIdInternal+"==="+printInfo);

			if (printInfo != null && printInfo.size() > 0) {
				// System.out.println("printInfo=Second="+printInfo);

				// System.out.println("printInfo=Second Email
				// ="+printInfo.get("emailIds"));
			}

			fc.utobj().clickElement(driver, pobj.contactsLinks);

			systemExactSearch(driver, firstNameSecond + " " + lastNameSecond);

			fc.utobj().printTestStep("Verify Second Contact Name in Contact Summary.");
			isLeadNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains( text(),'" + firstNameSecond + " " + lastNameSecond + "')]");
			if (isLeadNamePresent == false) {
				fc.utobj().throwsException("was not able to verify Second Contact Name.");
			}

			fc.utobj().clickElement(driver, pobj.contactsLinks);
			searchLeadByOwner(driver, corpUser.getuserFullName(), "Contacts");

			fc.utobj().printTestStep("Merge Contacts from Bottom Button");
			// fc.utobj().clickElement(driver,
			// fc.utobj().getElementByXpath(driver,".//a[contains(text ()
			// ,'"+firstName+"
			// "+lastName+"')]/ancestor::tr/td/input[@name='checkb']")));
			// fc.utobj().clickElement(driver,
			// fc.utobj().getElementByXpath(driver,".//a[contains(text ()
			// ,'"+firstNameSecond+"
			// "+lastNameSecond+"')]/ancestor::tr/td/input[@name='checkb']")));

			// fc.utobj().selectActionMenuItemsWithTagA(driver, "Merge
			// Contacts");
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//input[@value='Merge Contacts']"));

			String alertText = fc.utobj().acceptAlertBox(driver);

			if (alertText.indexOf("select") != -1) {
				fc.utobj().printTestStep("Verify Alert Text Please select Contacts(s) to Merge.");
			} else {
				fc.utobj().throwsException("was not able to verify Alert Text Please select Contacts(s) to Merge.");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "crm", "crmcontactAreaMerge", "TC_Contact_CRM_Merge_Task_Validation" })
	@TestCase(createdOn = "2018-01-17", updatedOn = "2018-01-17", testCaseId = "TC_Contact_CRM_Merge_Task_Validation", testCaseDescription = "To verify Contact Merge Cases If task is associated with any lead.")
	private void crmInfoFillAreaMerge_5() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		Map<String, String> dataSetCustom = null;
		String testCaseIdInternal = "" + testCaseId;
		Map<String, String> printInfo = null;
		try {
			driver = fc.loginpage().login(driver);
			CRMContactsPage pobj = new CRMContactsPage(driver);

			fc.utobj().printTestStep("Navigate To Admin Users Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			String emailId = "crmautomation@staffex.com";
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUser = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporateUser.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			fc.utobj().printTestStep("Navigate To CRM > Contact > Contacts Summary");
			fc.utobj().printTestStep("Add First Contact");
			fc.crm().crm_common().CRMContactsLnk(driver);
			fc.utobj().clickElement(driver, pobj.addNew);
			fc.utobj().clickElement(driver, pobj.contactLnk);

			testCaseIdInternal = "TC_CRM_QA_Contact_Details";
			dataSetCustom = fc.utobj().readTestData("crm", testCaseIdInternal);
			String firstName = fc.utobj().generateTestData("Aa");
			String lastName = fc.utobj().generateTestData("Bb");
			dataSetCustom.put("contactFirstName", firstName);
			dataSetCustom.put("contactLastName", lastName);
			dataSetCustom.put("contactType", "Contacts");
			dataSetCustom.put("cmLeadTypeID", "Select");
			dataSetCustom.put("cmSource2ID", "Internet");
			dataSetCustom.put("cmSource3ID", "BISON");
			dataSetCustom.put("accountName", "");
			dataSetCustom.put("contactOwnerID2", corpUser.getuserFullName());

			printInfo = fillFormDataWithDataset(driver, config, testCaseIdInternal, dataSetCustom);
			// Reporter.log("Map == for ="+testCaseIdInternal+"==="+printInfo);

			if (printInfo != null && printInfo.size() > 0) {
				// System.out.println("printInfo=First="+printInfo);

				// System.out.println("printInfo=First Email
				// ="+printInfo.get("emailIds"));
			}

			fc.utobj().clickElement(driver, pobj.contactsLinks);

			systemExactSearch(driver, firstName + " " + lastName);

			fc.utobj().printTestStep("Verify Contact Name in Contact Summary.");
			boolean isLeadNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains( text(),'" + firstName + " " + lastName + "')]");
			if (isLeadNamePresent == false) {
				fc.utobj().throwsException("was not able to verify First Contact Name");
			}

			fc.utobj().printTestStep("Navigate To CRM > Contact > Contacts Summary");
			fc.utobj().printTestStep("Add Second Contact");
			fc.crm().crm_common().CRMContactsLnk(driver);
			fc.utobj().clickElement(driver, pobj.addNew);
			fc.utobj().clickElement(driver, pobj.contactLnk);

			testCaseIdInternal = "TC_CRM_QA_Contact_Details";
			dataSetCustom = fc.utobj().readTestData("crm", testCaseIdInternal);
			String firstNameSecond = fc.utobj().generateTestData("Cc");
			String lastNameSecond = fc.utobj().generateTestData("Dd");
			dataSetCustom.put("contactFirstName", firstNameSecond);
			dataSetCustom.put("contactLastName", lastNameSecond);
			dataSetCustom.put("contactType", "Contacts");
			dataSetCustom.put("cmLeadTypeID", "Select");
			dataSetCustom.put("cmSource2ID", "Internet");
			dataSetCustom.put("cmSource3ID", "BISON");
			dataSetCustom.put("accountName", "");
			dataSetCustom.put("contactOwnerID2", corpUser.getuserFullName());

			printInfo = fillFormDataWithDataset(driver, config, testCaseIdInternal, dataSetCustom);
			// Reporter.log("Map == for ="+testCaseIdInternal+"==="+printInfo);

			if (printInfo != null && printInfo.size() > 0) {
				// System.out.println("printInfo=Second="+printInfo);

				// System.out.println("printInfo=Second Email
				// ="+printInfo.get("emailIds"));
			}

			fc.utobj().clickElement(driver, pobj.contactsLinks);

			systemExactSearch(driver, firstNameSecond + " " + lastNameSecond);

			fc.utobj().printTestStep("Verify Second Contact Name in Contact Summary.");
			isLeadNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains( text(),'" + firstNameSecond + " " + lastNameSecond + "')]");
			if (isLeadNamePresent == false) {
				fc.utobj().throwsException("was not able to verify Second Contact Name.");
			}

			fc.utobj().printTestStep("Log A Task");
			fc.utobj().actionImgOption(driver, firstNameSecond + " " + lastNameSecond, "Log a Task");

			fc.commonMethods().switch_cboxIframe_frameId(driver);
			if (!fc.utobj().isSelected(driver, pobj.radioOwner)) {
				fc.utobj().clickElement(driver, pobj.radioOwner);
			}
			String statusTask = "Not Started";
			fc.utobj().selectDropDown(driver, pobj.statusTask, statusTask);
			String subject = "Task for Merge TestCase";
			fc.utobj().sendKeys(driver, pobj.subject, subject);
			if (!fc.utobj().isSelected(driver, pobj.timeLessTask)) {
				fc.utobj().clickElement(driver, pobj.timeLessTask);
			}
			String priority = "Medium";
			fc.utobj().selectDropDown(driver, pobj.priority, priority);
			String comment = "To verify Contact Merge Cases If task is associated with any lead.";
			fc.utobj().sendKeys(driver, pobj.comments, comment);
			fc.utobj().clickElement(driver, pobj.createBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().clickElement(driver, pobj.contactsLinks);

			systemExactSearch(driver, firstNameSecond + " " + lastNameSecond);

			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,(".//a[contains(text () ,'" + firstNameSecond + " " + lastNameSecond + "')]")));

			fc.utobj().printTestStep("Verify Task");

			boolean isSubjectPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[contains(text () ,'" + subject + "')]");
			if (isSubjectPresent == false) {
				fc.utobj().throwsException("was not able to verify Task subject ");
			}

			fc.utobj().clickElement(driver, pobj.contactsLinks);
			searchLeadByOwner(driver, corpUser.getuserFullName(), "Contacts");

			fc.utobj().printTestStep("Merge Contacts from Bottom Button");
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//a[contains(text () ,'" + firstName
					+ " " + lastName + "')]/ancestor::tr/td/input[@name='checkb']"));
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//a[contains(text () ,'"
					+ firstNameSecond + " " + lastNameSecond + "')]/ancestor::tr/td/input[@name='checkb']"));

			// fc.utobj().selectActionMenuItemsWithTagA(driver, "Merge
			// Contacts");
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//input[@value='Merge Contacts']"));

			String alertText = fc.utobj().acceptAlertBox(driver);

			if (alertText.indexOf("cannot merge") != -1) {
				fc.utobj().printTestStep(
						"Verify Alert Text You cannot Merge Contacts as they have Task associated with them.");
			} else {
				fc.utobj().throwsException(
						"was not able to verify Alert Text You cannot Merge Contacts as they have Task associated with them.");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "crm", "crmcontactAreaMerge"},priority=19)
	@TestCase(createdOn = "2018-01-16", updatedOn = "2018-01-16", testCaseId = "TC_Contact_CRM_MergeCase_Assignee", testCaseDescription = "To verify that while merging contacts, if user change lead assignee.")
	private void crmInfoFillAreaMerge_6() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		Map<String, String> dataSetCustom = null;
		String testCaseIdInternal = "" + testCaseId;
		Map<String, String> printInfo = null;
		List<String> listInfo = new ArrayList<String>();

		try {
			driver = fc.loginpage().login(driver);
			CRMContactsPage pobj = new CRMContactsPage(driver);

			fc.utobj().printTestStep("Navigate To Admin Users Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			String emailId = "crmautomation@staffex.com";
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUser = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporateUser.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			fc.utobj().printTestStep("Navigate To Admin Users Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");

			CorporateUser corpUser2 = new CorporateUser();
			corpUser2.setEmail(emailId);
			corpUser2 = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser2);
			corporateUser.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser2);

			fc.utobj().printTestStep("Navigate To CRM > Contact > Contacts Summary");
			fc.utobj().printTestStep("Add First Contact");
			fc.crm().crm_common().CRMContactsLnk(driver);
			fc.utobj().clickElement(driver, pobj.addNew);
			fc.utobj().clickElement(driver, pobj.contactLnk);

			testCaseIdInternal = "TC_CRM_QA_Contact_Details";

			dataSetCustom = fc.utobj().readTestData("crm", testCaseIdInternal);
			String firstName = fc.utobj().generateTestData("Aa");
			String lastName = fc.utobj().generateTestData("Bb");
			dataSetCustom.put("contactFirstName", firstName);
			dataSetCustom.put("contactLastName", lastName);
			dataSetCustom.put("contactType", "Contacts");
			dataSetCustom.put("cmLeadTypeID", "Select");
			dataSetCustom.put("cmSource2ID", "Internet");
			dataSetCustom.put("cmSource3ID", "BISON");
			dataSetCustom.put("accountName", "");
			dataSetCustom.put("contactOwnerID2", corpUser.getuserFullName());

			listInfo.add(corpUser.getuserFullName());

			printInfo = fillFormDataWithDataset(driver, config, testCaseIdInternal, dataSetCustom);
			// Reporter.log("Map == for ="+testCaseIdInternal+"==="+printInfo);

			if (printInfo != null && printInfo.size() > 0) {
				// System.out.println("printInfo=First="+printInfo);

				// System.out.println("printInfo=First Email
				// ="+printInfo.get("emailIds"));
			}

			fc.utobj().clickElement(driver, pobj.contactsLinks);

			systemExactSearch(driver, firstName + " " + lastName);

			fc.utobj().printTestStep("Verify Contact Name in Contact Summary.");
			boolean isLeadNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains( text(),'" + firstName + " " + lastName + "')]");
			if (isLeadNamePresent == false) {
				fc.utobj().throwsException("was not able to verify First Contact Name");
			}

			fc.utobj().printTestStep("Navigate To CRM > Contact > Contacts Summary");
			fc.utobj().printTestStep("Add Second Contact");
			fc.crm().crm_common().CRMContactsLnk(driver);
			fc.utobj().clickElement(driver, pobj.addNew);
			fc.utobj().clickElement(driver, pobj.contactLnk);

			testCaseIdInternal = "TC_CRM_QA_Contact_Details";

			dataSetCustom = fc.utobj().readTestData("crm", testCaseIdInternal);
			String firstNameSecond = fc.utobj().generateTestData("Cc");
			String lastNameSecond = fc.utobj().generateTestData("Dd");
			dataSetCustom.put("contactFirstName", firstNameSecond);
			dataSetCustom.put("contactLastName", lastNameSecond);
			dataSetCustom.put("contactType", "Contacts");
			dataSetCustom.put("cmLeadTypeID", "Select");
			dataSetCustom.put("cmSource2ID", "Internet");
			dataSetCustom.put("cmSource3ID", "BISON");
			dataSetCustom.put("accountName", "");
			dataSetCustom.put("contactOwnerID2", corpUser2.getuserFullName());
			listInfo.add(corpUser2.getuserFullName());
			String secondOwnerName = corpUser2.getuserFullName();
			printInfo = fillFormDataWithDataset(driver, config, testCaseIdInternal, dataSetCustom);
			// Reporter.log("Map == for ="+testCaseIdInternal+"==="+printInfo);

			if (printInfo != null && printInfo.size() > 0) {
				// System.out.println("printInfo=Second="+printInfo);

				// System.out.println("printInfo=Second Email
				// ="+printInfo.get("emailIds"));
			}

			fc.utobj().clickElement(driver, pobj.contactsLinks);

			systemExactSearch(driver, firstNameSecond + " " + lastNameSecond);

			fc.utobj().printTestStep("Verify Second Contact Name in Contact Summary.");
			isLeadNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains( text(),'" + firstNameSecond + " " + lastNameSecond + "')]");
			if (isLeadNamePresent == false) {
				fc.utobj().throwsException("was not able to verify Second Contact Name.");
			}

			fc.utobj().clickElement(driver, pobj.contactsLinks);
			searchLeadByOwnerDiffrent(driver, corpUser.getuserFullName(), "Contacts", listInfo);

			fc.utobj().printTestStep("Merge Contacts");
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//a[contains(text () ,'" + firstName
					+ " " + lastName + "')]/ancestor::tr/td/input[@name='checkb']"));
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//a[contains(text () ,'"
					+ firstNameSecond + " " + lastNameSecond + "')]/ancestor::tr/td/input[@name='checkb']"));
			fc.utobj().selectActionMenuItemsWithTagA(driver, "Merge Contacts");

			if (!fc.utobj().isSelected(driver, pobj.secondassigntoSelect)) {
				fc.utobj().clickElement(driver, pobj.secondassigntoSelect);
			}

			String secondName = fc.utobj().getElementByXpath(driver, ".//*[@value='second' and @name='assignto']/..")
					.getText().trim();
			String firstNameArchived = fc.utobj()
					.getElementByXpath(driver, ".//*[@value='first' and @name='assignto']/..").getText().trim();

			fc.utobj().sendKeys(driver, pobj.remarksAtMerge, "Merge Remarks of Contacts.");
			fc.utobj().clickElement(driver, pobj.mergeBtn);

			fc.utobj().clickElement(driver, pobj.contactsLinks);
			searchLeadByOwner(driver, corpUser.getuserFullName(), "Contacts");

			try {
				fc.utobj().webDriverWait(driver, ".//a[contains(text () ,'" + firstName + " " + lastName + "')]");
			} catch (Exception e) {
				fc.utobj().printTestStep("First Owner record Lead not displaying verified.");
			}
			fc.utobj().clickElement(driver, pobj.contactsLinks);
			searchLeadByOwner(driver, corpUser2.getuserFullName(), "Contacts");

			fc.utobj().webDriverWait(driver, ".//a[contains(text () ,'" + firstName + " " + lastName + "')]");

			fc.utobj().printTestStep("Verify Merge Contacts");
			boolean isMergeContactNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firstName + " " + lastName + "')]");
			if (isMergeContactNamePresent == false) {
				fc.utobj().throwsException("was not able to verify Contact Name after Merge Contacts.");
			}

			boolean isOwnerPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//*[contains(text () ,'" + firstName + " "
					+ lastName + "')]/ancestor::tr/td[contains(text () ,'" + secondOwnerName + "')]");
			if (isOwnerPresent == false) {
				fc.utobj().throwsException("was not able to verify Merge Contact owner Contact Summary.");
			}

			boolean isMergeiconPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//*[contains(text () , '" + firstName + " "
					+ lastName + "')]/ancestor::tr/td/img[contains(@src, 'lead_merge.gif')]");
			if (isMergeiconPresent == false) {
				fc.utobj().throwsException("was not able to verify Contact Merge icon with Contact Name.");
			}

			fc.utobj().printTestStep("Verify Merge Contacts ToolTip.");
			WebElement elementToolTip = fc.utobj().getElementByXpath(driver, ".//*[contains(text () , '" + firstName
					+ " " + lastName + "')]/ancestor::tr/td/img[contains(@src, 'lead_merge.gif')]");
			Thread.sleep(2000);
			Actions ac1 = new Actions(driver);
			ac1.clickAndHold().moveToElement(elementToolTip);
			Thread.sleep(2000);
			ac1.moveToElement(elementToolTip).build().perform();
			Thread.sleep(1000);

			String leadMergetoolTipText = fc.utobj().getElementByXpath(driver, ".//*[@id='contentDiv']").getText()
					.trim();

			if (leadMergetoolTipText.indexOf("Merging of Contacts") != -1) {
				fc.utobj().printTestStep("Verify ToolTip Indicates the Contacts is added through Merging of Contacts.");
			} else {
				fc.utobj().throwsException("Was not able to verify ToolTip of Merging of Contacts");
			}

			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//a[contains(text () ,'" + firstName + " " + lastName + "')]"));
			boolean isCNamePresentAtPrimaryInfo = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () ,'Name')]/ancestor::tr/td[contains(text () ,'" + firstName + "')]");
			if (isCNamePresentAtPrimaryInfo == false) {
				fc.utobj().throwsException("was not able to verify Contact Name at primary info page");
			}

			fc.utobj().printTestStep("Verify archive contacts at Contact Summary page");
			fc.utobj().clickElement(driver, pobj.searchLink);
			// fc.utobj().clickElement(driver, pobj.leadsTab);
			if (!fc.utobj().isSelected(driver, pobj.archiveLeads)) {
				fc.utobj().clickElement(driver, pobj.archiveLeads);
			}

			fc.utobj().setToDefault(driver, pobj.assignTo);
			WebElement elementInput = fc.utobj().getElementByXpath(driver,
					".//*[@id='ms-parentC:CONTACT_OWNER_ID:COMBO']");
			fc.utobj().selectMultipleValFromMultiSelect(driver, elementInput, listInfo);

			fc.utobj().clickElement(driver, pobj.searchBtnAtSearch);

			fc.utobj().printTestStep("Verify archive contact At Search Page");
			boolean isNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firstName + " " + lastName + "')]");
			if (isNamePresent == false) {
				fc.utobj().throwsException("was not able to verify archive contact.");
			}
			isNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firstNameSecond + " " + lastNameSecond + "')]");
			if (isNamePresent == false) {
				fc.utobj().throwsException("was not able to verify archive contact.");
			}

			isOwnerPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//*[contains(text () ,'" + firstName + " " + lastName
					+ "')]/ancestor::tr/td[contains(text () ,'" + corpUser.getuserFullName() + "')]");
			if (isOwnerPresent == false) {
				fc.utobj().throwsException("was not able to verify First Contact owner Archive summary.");
			}

			isOwnerPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//*[contains(text () ,'" + firstNameSecond + " "
					+ lastNameSecond + "')]/ancestor::tr/td[contains(text () ,'" + secondOwnerName + "')]");
			if (isOwnerPresent == false) {
				fc.utobj().throwsException("was not able to verify Second Contact owner in Archive summary.");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "crmcontactAreaMerge1"})
	@TestCase(createdOn = "2018-01-16", updatedOn = "2018-01-16", testCaseId = "TC_Contact_CRM_MergeCase", testCaseDescription = "To Verify merging contacts behaviours.")
	private void crmInfoFilltest() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		try {
			driver = fc.loginpage().login(driver);
/*
			fc.utobj().printTestStep("Navigate To CRM > Contact > Contacts Summary");
			fc.crm().crm_common().CRMContactsLnk(driver);
			// fc.utobj().clickElement(driver, pobj.contactLnk);

			Map<String, String> userInfo = new HashMap<String, String>();
			userInfo.put("userFullName", "Cuz17122861 Cub17122820");
			List<String> listInfo = new ArrayList<String>();
			listInfo.add("Cud18104612 Cut18104612");
			listInfo.add("Cud18104612 Cut18104612");

			String secondName = "Ccx18104917 Ddz18104986";

			// searchLeadByOwner(driver, corpUser.getuserFullName(),
			// "Contacts");
			searchLeadByOwnerDiffrent(driver, userInfo.get("userFullName"), "Contacts", listInfo);

			//fc.utobj().webDriverWait(driver, ".//a[contains(text () ,'" + secondName + "')]");

			fc.utobj().printTestStep("Verify Merge Contacts");
			boolean isMergeContactNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondName + "')]");
			if (isMergeContactNamePresent == false) {
				fc.utobj().throwsException("was not able to verify Contact Name after Merge Contacts.");
			}

			boolean isMergeiconPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//*[contains(text () , '" + secondName
					+ "')]/ancestor::tr/td/img[contains(@src, 'lead_merge.gif')]");
			if (isMergeiconPresent == false) {
				fc.utobj().throwsException("was not able to verify Contact Merge icon with Contact Name.");
			}

			fc.utobj().printTestStep("Verify Merge Contacts ToolTip.");
			WebElement elementToolTip = fc.utobj().getElementByXpath(driver, ".//*[contains(text () , '" + secondName
					+ "')]/ancestor::tr/td/img[contains(@src, 'lead_merge.gif')]");
			Actions ac1 = new Actions(driver);
			ac1.clickAndHold().moveToElement(elementToolTip);
			ac1.moveToElement(elementToolTip).build().perform();
			// ac1.moveToElement(elementToolTip).build().perform();


			String leadMergetoolTipText = fc.utobj().getElementByXpath(driver, ".//*[@id='contentDiv']").getText()
					.trim();

			if (leadMergetoolTipText.indexOf("Merging of Leads") != -1) {
				fc.utobj().printTestStep("Verify ToolTip Indicates the Lead is added through Merging of Leads.");
			} else {
				fc.utobj().throwsException("Was not able to verify ToolTip of Merging of Leads");
			}*/

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "crm", "crmcontactAreaMerge"},priority=13)
	@TestCase(createdOn = "2017-05-01", updatedOn = "2017-06-27", testCaseDescription = "To verify If the Assignee is chosen as Divisional user then only Divisional Campaign is carried with Merge", testCaseId = "TC_Contact_CRM_A_CampaignCorpDiv")
	public void associateCampaignMerge() throws IOException, Exception, ParserConfigurationException, SAXException {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());
		String testCaseIdInternal = "TC_08_Copy_And_Customize_Campaign_At_All_Tab";
		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseIdInternal);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		List<String> listInfo = new ArrayList<String>();
		try {
			driver = fc.loginpage().login(driver);
			CRMContactsPage pobj = new CRMContactsPage(driver);
			CRMCampaignCenterPageTest campaignCenterPage = new CRMCampaignCenterPageTest();
			CRMGroupsPageTest groupPage = new CRMGroupsPageTest();
			CRMCampaignCenterCampaignsPage pobjcam = new CRMCampaignCenterCampaignsPage(driver);

			// First Cycle

			fc.utobj().printTestStep("Navigate To CRM > Campaign Center");
			fc.crm().crm_common().CRMModule(driver);

			fc.utobj().printTestStep("Create First Campaign At Campaign Center > Campaigns");
			String firstcampaignName = fc.utobj().generateTestData(dataSet.get("campaignName"));
			String firstaccessibility = dataSet.get("accessibility");
			String firstcampaignType = dataSet.get("campaignType");
			String firsttemplateName = fc.utobj().generateTestData(dataSet.get("templateName"));
			String firstemailSubject = fc.utobj().generateTestData(dataSet.get("emailSubject"));
			String firstplainTextEditor = fc.utobj().generateTestData(dataSet.get("plainTextEditor"));
			campaignCenterPage.createCampaignGeneric(driver, config, firstcampaignName, firstaccessibility,
					firstcampaignType, firsttemplateName, firstemailSubject, firstplainTextEditor);

			fc.utobj().printTestStep("Verify The First Campaign");
			fc.utobj().clickElement(driver, pobjcam.viewAllButton);

			campaignCenterPage.searchCampaignByFilter(driver, firstcampaignName);

			boolean isCampaignNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[.='" + firstcampaignName + "']");
			if (isCampaignNamePresent == false) {
				fc.utobj().throwsException("was not able to verify First Campaign");
			}

			dataSet = fc.utobj().readTestData("crm", testCaseId);

			fc.utobj().printTestStep("Navigate To Admin Users Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			
			//String emailId = "crmautomation@staffex.com";
			String emailId=fc.utobj().generateTestData("crmautomation")+"@staffex.com";
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUser = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();

			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporateUser.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			String firstName_1 = fc.utobj().generateTestData(dataSet.get("firstName"));
			String lastName_1 = fc.utobj().generateTestData(dataSet.get("lastName"));
			String company_1 = fc.utobj().generateTestData(dataSet.get("company"));
			String assignTo_1 = "Corporate";
			String city_1 = fc.utobj().generateTestData(dataSet.get("city"));
			String address_1 = fc.utobj().generateTestData(dataSet.get("address"));
			String suffix_1 = fc.utobj().generateTestData(dataSet.get("suffix"));
			String jobTitle_1 = fc.utobj().generateTestData(dataSet.get("jobTitle"));
			String comment_1 = fc.utobj().generateTestData(dataSet.get("comment"));
			String franchiseId_1 = "";
			String franchiseUser_1 = "";
			String userNameReg_1 = "";
			String regionName_1 = "";
			String divisionName = "";
			fc.utobj().printTestStep("Navigate To CRM > Contact > Contacts Summary");
			fc.utobj().printTestStep("Add Contact");
			addContact(driver, dataSet, firstName_1, lastName_1, company_1, assignTo_1, city_1, address_1, suffix_1,
					jobTitle_1, comment_1, corpUser.getuserFullName(), regionName_1, userNameReg_1, franchiseId_1,
					franchiseUser_1, divisionName, config);

			fc.utobj().clickElement(driver, pobj.contactsLinks);
			searchLeadByOwner(driver, corpUser.getuserFullName(), "Contacts");
			listInfo.add(corpUser.getuserFullName());
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//a[contains(text () ,'" + firstName_1 + " " + lastName_1 + "')]"));

			fc.utobj().printTestStep("Associate With Campaign");
			fc.utobj().clickElement(driver, pobj.campaignLinkAtLeadInfo);
			fc.utobj().clickElement(driver, pobj.filterContainer);
			fc.utobj().sendKeys(driver, pobj.searchCampaign, firstcampaignName);
			fc.utobj().clickElement(driver, pobj.applyFilter);

			fc.utobj().clickElement(driver, pobj.sendCampaignBtn);
			fc.utobj().clickElement(driver, pobj.confirmBtn);
			fc.utobj().clickElement(driver, pobj.contactsLinks);
			searchLeadByOwner(driver, corpUser.getuserFullName(), "Contacts");
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//a[contains(text () ,'" + firstName_1 + " " + lastName_1 + "')]"));

			fc.utobj().printTestStep("Verify Associate With Campaign");
			isCampaignNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () ,'Promotional Campaign')]/following-sibling::td[contains(text () ,'"
							+ firstcampaignName + "')]");
			if (isCampaignNamePresent == false) {
				fc.utobj().throwsException("was not able to verify Campign Name");
			}

			// Second Cycle
			dataSet = fc.utobj().readTestData("crm", testCaseIdInternal);
			fc.utobj().printTestStep("Navigate To CRM > Campaign Center");
			fc.crm().crm_common().CRMContactsLnk(driver);

			fc.utobj().printTestStep("Create Campaign At Campaign Center > Campaigns");
			String secondcampaignName = fc.utobj().generateTestData(dataSet.get("campaignName"));
			String secondaccessibility = dataSet.get("accessibility");
			String secondcampaignType = dataSet.get("campaignType");
			String secondtemplateName = fc.utobj().generateTestData(dataSet.get("templateName"));
			String secondemailSubject = fc.utobj().generateTestData(dataSet.get("emailSubject"));
			String thirdplainTextEditor = fc.utobj().generateTestData(dataSet.get("plainTextEditor"));
			campaignCenterPage.createCampaignGeneric(driver, config, secondcampaignName, secondaccessibility,
					secondcampaignType, secondtemplateName, secondemailSubject, thirdplainTextEditor);

			fc.utobj().printTestStep("Verify The Campaign");
			fc.utobj().clickElement(driver, pobjcam.viewAllButton);

			campaignCenterPage.searchCampaignByFilter(driver, secondcampaignName);

			isCampaignNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondcampaignName + "')]");
			if (isCampaignNamePresent == false) {
				fc.utobj().throwsException("was not able to verify second Campaign");
			}

			dataSet = fc.utobj().readTestData("crm", testCaseId);

			fc.utobj().printTestStep("Navigate To Admin Users Manage Divisonal User");
			fc.utobj().printTestStep("Add Divisonal User");
			//emailId = "crmautomation@staffex.com";
			emailId=fc.utobj().generateTestData("crmautomation")+"@staffex.com";
			AdminUsersManageDivisionalUsersAddDivisionalUsersPageTest divuser = new AdminUsersManageDivisionalUsersAddDivisionalUsersPageTest();
			String userName = "Div" + fc.utobj().generateRandomNumber();
			divisionName = "Name" + fc.utobj().generateRandomNumber();
			String regionName = "Reg" + fc.utobj().generateRandomNumber();
			String userFullName = userName + " " + userName;
			divuser.addDivisionalUser(driver, userName, divisionName, emailId);

			String firstName_2 = fc.utobj().generateTestData(dataSet.get("firstName"));
			String lastName_2 = fc.utobj().generateTestData(dataSet.get("lastName"));
			String company_2 = fc.utobj().generateTestData(dataSet.get("company"));
			String assignTo_2 = "Division";
			String city_2 = fc.utobj().generateTestData(dataSet.get("city"));
			String address_2 = fc.utobj().generateTestData(dataSet.get("address"));
			String suffix_2 = fc.utobj().generateTestData(dataSet.get("suffix"));
			String jobTitle_2 = fc.utobj().generateTestData(dataSet.get("jobTitle"));
			String comment_2 = fc.utobj().generateTestData(dataSet.get("comment"));
			String franchiseId_2 = "";
			String franchiseUser_2 = "";
			String userNameReg_2 = "";
			String regionName_2 = "";
			fc.utobj().printTestStep("Navigate To CRM > Contact > Contacts Summary");
			fc.utobj().printTestStep("Add First Contact");
			addContact(driver, dataSet, firstName_2, lastName_2, company_2, assignTo_2, city_2, address_2, suffix_2,
					jobTitle_2, comment_2, userFullName, regionName_2, userNameReg_2, franchiseId_2, franchiseUser_2,
					divisionName, config);

			fc.utobj().clickElement(driver, pobj.contactsLinks);
			searchLeadByOwner(driver, userFullName, "Contacts");
			listInfo.add(userFullName);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//a[contains(text () ,'" + firstName_2 + " " + lastName_2 + "')]"));

			fc.utobj().printTestStep("Associate With Campaign");
			fc.utobj().clickElement(driver, pobj.campaignLinkAtLeadInfo);
			fc.utobj().clickElement(driver, pobj.filterContainer);
			fc.utobj().sendKeys(driver, pobj.searchCampaign, secondcampaignName);
			fc.utobj().clickElement(driver, pobj.applyFilter);

			fc.utobj().clickElement(driver, pobj.sendCampaignBtn);
			fc.utobj().clickElement(driver, pobj.confirmBtn);
			fc.utobj().clickElement(driver, pobj.contactsLinks);
			searchLeadByOwner(driver, userFullName, "Contacts");
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//a[contains(text () ,'" + firstName_2 + " " + lastName_2 + "')]"));

			fc.utobj().printTestStep("Verify Associate With Campaign");
			isCampaignNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () ,'Promotional Campaign')]/following-sibling::td[contains(text () ,'"
							+ secondcampaignName + "')]");
			if (isCampaignNamePresent == false) {
				fc.utobj().throwsException("was not able to verify Campign Name");
			}

			// Merge

			fc.utobj().clickElement(driver, pobj.contactsLinks);
			searchLeadByOwnerDiffrent(driver, corpUser.getuserFullName(), "Contacts", listInfo);

			fc.utobj().printTestStep("Merge Contacts");
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//a[contains(text () ,'"
					+ firstName_1 + " " + lastName_1 + "')]/ancestor::tr/td/input[@name='checkb']"));
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//a[contains(text () ,'"
					+ firstName_2 + " " + lastName_2 + "')]/ancestor::tr/td/input[@name='checkb']"));
			fc.utobj().selectActionMenuItemsWithTagA(driver, "Merge Contacts");

			boolean furtherProceed = true;
			try {

				String alertText = fc.utobj().acceptAlertBox(driver);

				if (alertText.indexOf("cannot merge") != -1) {
					fc.utobj().printTestStep(
							"Verify Alert Text You cannot Merge Contacts as they have Task associated with them.");
					furtherProceed = false;
				} else {

				}

			} catch (Exception e) {

			}

			if (furtherProceed) {
				if (!fc.utobj().isSelected(driver, pobj.secondassigntoSelect)) {
					fc.utobj().clickElement(driver, pobj.secondassigntoSelect);
				}

				String secondName = fc.utobj()
						.getElementByXpath(driver, ".//*[@value='second' and @name='assignto']/..").getText().trim();
				String firstNameArchived = fc.utobj()
						.getElementByXpath(driver, ".//*[@value='first' and @name='assignto']/..").getText().trim();

				fc.utobj().sendKeys(driver, pobj.remarksAtMerge, "Merge Remarks of Contacts.");
				fc.utobj().clickElement(driver, pobj.mergeBtn);

				fc.utobj().clickElement(driver, pobj.contactsLinks);
				searchLeadByOwner(driver, corpUser.getuserFullName(), "Contacts");

				try {
					fc.utobj().webDriverWait(driver,
							".//a[contains(text () ,'" + firstName_1 + " " + lastName_1 + "')]");
				} catch (Exception e) {
					fc.utobj().printTestStep("First Owner record Lead not displaying verified.");
				}
				fc.utobj().clickElement(driver, pobj.contactsLinks);
				searchLeadByOwner(driver, userFullName, "Contacts");

				fc.utobj().webDriverWait(driver, ".//a[contains(text () ,'" + firstName_1 + " " + lastName_1 + "')]");

				fc.utobj().printTestStep("Verify Merge Contacts");
				boolean isMergeContactNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
						".//a[contains(text () ,'" + firstName_1 + " " + lastName_1 + "')]");
				if (isMergeContactNamePresent == false) {
					fc.utobj().throwsException("was not able to verify Contact Name after Merge Contacts.");
				}

				boolean isOwnerPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//*[contains(text () ,'" + firstName_1 + " "
						+ lastName_1 + "')]/ancestor::tr/td[contains(text () ,'" + userFullName + "')]");
				if (isOwnerPresent == false) {
					fc.utobj().throwsException("was not able to verify Merge Contact owner Contact Summary.");
				}

				boolean isMergeiconPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//*[contains(text () , '" + firstName_1
						+ " " + lastName_1 + "')]/ancestor::tr/td/img[contains(@src, 'lead_merge.gif')]");
				if (isMergeiconPresent == false) {
					fc.utobj().throwsException("was not able to verify Contact Merge icon with Contact Name.");
				}

				fc.utobj().clickElement(driver, driver
						.findElement(By.xpath(".//a[contains(text () ,'" + firstName_1 + " " + lastName_1 + "')]")));

				fc.utobj().printTestStep("Verify Associate With Campaign");
				boolean isCampaignNamePresentFirst = fc.utobj().verifyElementOnVisible_ByXpath(driver,
						".//td[contains(text () ,'Promotional Campaign')]/following-sibling::td[contains(text () ,'"
								+ firstcampaignName + "')]");
				boolean isCampaignNamePresentSecond = fc.utobj().verifyElementOnVisible_ByXpath(driver,
						".//td[contains(text () ,'Promotional Campaign')]/following-sibling::td[contains(text () ,'"
								+ secondcampaignName + "')]");

				if (isCampaignNamePresentSecond) {
					fc.utobj().printTestStep(
							"verified Campaigns in Contact Merge  If the Assignee is chosen as Divisional user then only Divisional Campaigns is carried.");

				} else {
					fc.utobj().throwsException(
							"was not able to verify Campaigns  Contact Merge  If the Assignee is chosen as Divisional user then only Divisional Campaigns is carried.");
				}

			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "crm", "TC_Contact_CRM_A_CampaignCorpReg", "crmcontactAreaMerge"},priority=15)
	@TestCase(createdOn = "2017-05-01", updatedOn = "2017-06-27", testCaseDescription = "To verify If the Assignee is chosen as Regional user then both Corporate and Regional Campaigns are carried with Merge", testCaseId = "TC_Contact_CRM_A_CampaignCorpReg")
	public void associateCampaignMergeCorpReg()
			throws IOException, Exception, ParserConfigurationException, SAXException {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());
		String testCaseIdInternal = "TC_08_Copy_And_Customize_Campaign_At_All_Tab";
		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseIdInternal);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		List<String> listInfo = new ArrayList<String>();
		try {
			driver = fc.loginpage().login(driver);
			CRMContactsPage pobj = new CRMContactsPage(driver);
			CRMCampaignCenterPageTest campaignCenterPage = new CRMCampaignCenterPageTest();
			CRMGroupsPageTest groupPage = new CRMGroupsPageTest();
			CRMCampaignCenterCampaignsPage pobjcam = new CRMCampaignCenterCampaignsPage(driver);

			// First Cycle

			fc.utobj().printTestStep("Navigate To CRM > Campaign Center");
			fc.crm().crm_common().CRMModule(driver);

			fc.utobj().printTestStep("Create First Campaign At Campaign Center > Campaigns");
			String firstcampaignName = fc.utobj().generateTestData(dataSet.get("campaignName"));
			String firstaccessibility = dataSet.get("accessibility");
			String firstcampaignType = dataSet.get("campaignType");
			String firsttemplateName = fc.utobj().generateTestData(dataSet.get("templateName"));
			String firstemailSubject = fc.utobj().generateTestData(dataSet.get("emailSubject"));
			String firstplainTextEditor = fc.utobj().generateTestData(dataSet.get("plainTextEditor"));
			campaignCenterPage.createCampaignGeneric(driver, config, firstcampaignName, firstaccessibility,
					firstcampaignType, firsttemplateName, firstemailSubject, firstplainTextEditor);

			fc.utobj().printTestStep("Verify The First Campaign");
			fc.utobj().clickElement(driver, pobjcam.viewAllButton);

			campaignCenterPage.searchCampaignByFilter(driver, firstcampaignName);

			boolean isCampaignNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firstcampaignName + "')]");
			if (isCampaignNamePresent == false) {
				fc.utobj().throwsException("was not able to verify First Campaign");
			}

			dataSet = fc.utobj().readTestData("crm", testCaseId);

			fc.utobj().printTestStep("Navigate To Admin Users Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			//String emailId = "crmautomation@staffex.com";
			String emailId=fc.utobj().generateTestData("crmautomation")+"@staffex.com";
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUser = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();

			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporateUser.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			String firstName_1 = fc.utobj().generateTestData(dataSet.get("firstName"));
			String lastName_1 = fc.utobj().generateTestData(dataSet.get("lastName"));
			String company_1 = fc.utobj().generateTestData(dataSet.get("company"));
			String assignTo_1 = "Corporate";
			String city_1 = fc.utobj().generateTestData(dataSet.get("city"));
			String address_1 = fc.utobj().generateTestData(dataSet.get("address"));
			String suffix_1 = fc.utobj().generateTestData(dataSet.get("suffix"));
			String jobTitle_1 = fc.utobj().generateTestData(dataSet.get("jobTitle"));
			String comment_1 = fc.utobj().generateTestData(dataSet.get("comment"));
			String franchiseId_1 = "";
			String franchiseUser_1 = "";
			String userNameReg_1 = "";
			String regionName_1 = "";
			String divisionName = "";
			fc.utobj().printTestStep("Navigate To CRM > Contact > Contacts Summary");
			fc.utobj().printTestStep("Add Contact");
			addContact(driver, dataSet, firstName_1, lastName_1, company_1, assignTo_1, city_1, address_1, suffix_1,
					jobTitle_1, comment_1, corpUser.getuserFullName(), regionName_1, userNameReg_1, franchiseId_1,
					franchiseUser_1, divisionName, config);

			fc.utobj().clickElement(driver, pobj.contactsLinks);
			searchLeadByOwner(driver, corpUser.getuserFullName(), "Contacts");
			listInfo.add(corpUser.getuserFullName());
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//a[contains(text () ,'" + firstName_1 + " " + lastName_1 + "')]"));

			fc.utobj().printTestStep("Associate With Campaign");
			fc.utobj().clickElement(driver, pobj.campaignLinkAtLeadInfo);
			fc.utobj().clickElement(driver, pobj.filterContainer);
			fc.utobj().sendKeys(driver, pobj.searchCampaign, firstcampaignName);
			fc.utobj().clickElement(driver, pobj.applyFilter);

			fc.utobj().clickElement(driver, pobj.sendCampaignBtn);
			fc.utobj().clickElement(driver, pobj.confirmBtn);
			fc.utobj().clickElement(driver, pobj.contactsLinks);
			searchLeadByOwner(driver, corpUser.getuserFullName(), "Contacts");
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//a[contains(text () ,'" + firstName_1 + " " + lastName_1 + "')]"));

			fc.utobj().printTestStep("Verify Associate With Campaign");
			isCampaignNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () ,'Promotional Campaign')]/following-sibling::td[contains(text () ,'"
							+ firstcampaignName + "')]");
			if (isCampaignNamePresent == false) {
				fc.utobj().throwsException("was not able to verify Campign Name");
			}

			// Second Cycle
			dataSet = fc.utobj().readTestData("crm", testCaseIdInternal);
			fc.utobj().printTestStep("Navigate To CRM > Campaign Center");
			fc.crm().crm_common().CRMContactsLnk(driver);

			fc.utobj().printTestStep("Create Campaign At Campaign Center > Campaigns");
			String secondcampaignName = fc.utobj().generateTestData(dataSet.get("campaignName"));
			String secondaccessibility = dataSet.get("accessibility");
			String secondcampaignType = dataSet.get("campaignType");
			String secondtemplateName = fc.utobj().generateTestData(dataSet.get("templateName"));
			String secondemailSubject = fc.utobj().generateTestData(dataSet.get("emailSubject"));
			String thirdplainTextEditor = fc.utobj().generateTestData(dataSet.get("plainTextEditor"));
			campaignCenterPage.createCampaignGeneric(driver, config, secondcampaignName, secondaccessibility,
					secondcampaignType, secondtemplateName, secondemailSubject, thirdplainTextEditor);

			fc.utobj().printTestStep("Verify The Campaign");
			fc.utobj().clickElement(driver, pobjcam.viewAllButton);

			campaignCenterPage.searchCampaignByFilter(driver, secondcampaignName);

			isCampaignNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondcampaignName + "')]");
			if (isCampaignNamePresent == false) {
				fc.utobj().throwsException("was not able to verify second Campaign");
			}

			dataSet = fc.utobj().readTestData("crm", testCaseId);

			fc.utobj().printTestStep("Navigate To Admin Users Manage Regional User");
			fc.utobj().printTestStep("Add Regional User");
			//emailId = "crmautomation@staffex.com";
			emailId=fc.utobj().generateTestData("crmautomation")+"@staffex.com";
			AdminUsersManageRegionalUsersAddRegionalUserPageTest divuser = new AdminUsersManageRegionalUsersAddRegionalUserPageTest();
			String userNameReg = fc.utobj().generateTestData("Reg");
			String regionName = fc.utobj().generateTestData("Area");
			String userFullName = userNameReg + " " + userNameReg;
			divuser.addRegionalUser(driver, userNameReg, regionName, emailId);

			String firstName_2 = fc.utobj().generateTestData(dataSet.get("firstName"));
			String lastName_2 = fc.utobj().generateTestData(dataSet.get("lastName"));
			String company_2 = fc.utobj().generateTestData(dataSet.get("company"));
			String assignTo_2 = "Regional";
			String city_2 = fc.utobj().generateTestData(dataSet.get("city"));
			String address_2 = fc.utobj().generateTestData(dataSet.get("address"));
			String suffix_2 = fc.utobj().generateTestData(dataSet.get("suffix"));
			String jobTitle_2 = fc.utobj().generateTestData(dataSet.get("jobTitle"));
			String comment_2 = fc.utobj().generateTestData(dataSet.get("comment"));
			String franchiseId_2 = "";
			String franchiseUser_2 = "";
			String userNameReg_2 = userFullName;
			String regionName_2 = regionName;
			fc.utobj().printTestStep("Navigate To CRM > Contact > Contacts Summary");
			fc.utobj().printTestStep("Add First Contact");
			addContact(driver, dataSet, firstName_2, lastName_2, company_2, assignTo_2, city_2, address_2, suffix_2,
					jobTitle_2, comment_2, userFullName, regionName_2, userNameReg_2, franchiseId_2, franchiseUser_2,
					divisionName, config);

			fc.utobj().clickElement(driver, pobj.contactsLinks);
			searchLeadByOwner(driver, userFullName, "Contacts");
			listInfo.add(userFullName);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//a[contains(text () ,'" + firstName_2 + " " + lastName_2 + "')]"));

			fc.utobj().printTestStep("Associate With Campaign");
			fc.utobj().clickElement(driver, pobj.campaignLinkAtLeadInfo);
			fc.utobj().clickElement(driver, pobj.filterContainer);
			fc.utobj().sendKeys(driver, pobj.searchCampaign, secondcampaignName);
			fc.utobj().clickElement(driver, pobj.applyFilter);

			fc.utobj().clickElement(driver, pobj.sendCampaignBtn);
			fc.utobj().clickElement(driver, pobj.confirmBtn);
			fc.utobj().clickElement(driver, pobj.contactsLinks);
			searchLeadByOwner(driver, userFullName, "Contacts");
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//a[contains(text () ,'" + firstName_2 + " " + lastName_2 + "')]"));

			fc.utobj().printTestStep("Verify Associate With Campaign");
			isCampaignNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () ,'Promotional Campaign')]/following-sibling::td[contains(text () ,'"
							+ secondcampaignName + "')]");
			if (isCampaignNamePresent == false) {
				fc.utobj().throwsException("was not able to verify Campign Name");
			}

			// Merge

			fc.utobj().clickElement(driver, pobj.contactsLinks);
			searchLeadByOwnerDiffrent(driver, corpUser.getuserFullName(), "Contacts", listInfo);

			fc.utobj().printTestStep("Merge Contacts");
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//a[contains(text () ,'"
					+ firstName_1 + " " + lastName_1 + "')]/ancestor::tr/td/input[@name='checkb']"));
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//a[contains(text () ,'"
					+ firstName_2 + " " + lastName_2 + "')]/ancestor::tr/td/input[@name='checkb']"));
			fc.utobj().selectActionMenuItemsWithTagA(driver, "Merge Contacts");

			boolean furtherProceed = true;
			try {

				String alertText = fc.utobj().acceptAlertBox(driver);

				if (alertText.indexOf("cannot merge") != -1) {
					fc.utobj().printTestStep(
							"Verify Alert Text You cannot Merge Contacts as they have Task associated with them.");
					furtherProceed = false;
				} else {

				}

			} catch (Exception e) {

			}

			if (furtherProceed) {
				if (!fc.utobj().isSelected(driver, pobj.secondassigntoSelect)) {
					fc.utobj().clickElement(driver, pobj.secondassigntoSelect);
				}

				String secondName = fc.utobj()
						.getElementByXpath(driver, ".//*[@value='second' and @name='assignto']/..").getText().trim();
				String firstNameArchived = fc.utobj()
						.getElementByXpath(driver, ".//*[@value='first' and @name='assignto']/..").getText().trim();

				fc.utobj().sendKeys(driver, pobj.remarksAtMerge, "Merge Remarks of Contacts.");
				fc.utobj().clickElement(driver, pobj.mergeBtn);

				fc.utobj().clickElement(driver, pobj.contactsLinks);
				searchLeadByOwner(driver, corpUser.getuserFullName(), "Contacts");

				try {
					fc.utobj().webDriverWait(driver,
							".//a[contains(text () ,'" + firstName_1 + " " + lastName_1 + "')]");
				} catch (Exception e) {
					fc.utobj().printTestStep("First Owner record Lead not displaying verified.");
				}
				fc.utobj().clickElement(driver, pobj.contactsLinks);
				searchLeadByOwner(driver, userFullName, "Contacts");

				fc.utobj().webDriverWait(driver, ".//a[contains(text () ,'" + firstName_1 + " " + lastName_1 + "')]");

				fc.utobj().printTestStep("Verify Merge Contacts");
				boolean isMergeContactNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
						".//a[contains(text () ,'" + firstName_1 + " " + lastName_1 + "')]");
				if (isMergeContactNamePresent == false) {
					fc.utobj().throwsException("was not able to verify Contact Name after Merge Contacts.");
				}

				boolean isOwnerPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//*[contains(text () ,'" + firstName_1 + " "
						+ lastName_1 + "')]/ancestor::tr/td[contains(text () ,'" + userFullName + "')]");
				if (isOwnerPresent == false) {
					fc.utobj().throwsException("was not able to verify Merge Contact owner Contact Summary.");
				}

				boolean isMergeiconPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//*[contains(text () , '" + firstName_1
						+ " " + lastName_1 + "')]/ancestor::tr/td/img[contains(@src, 'lead_merge.gif')]");
				if (isMergeiconPresent == false) {
					fc.utobj().throwsException("was not able to verify Contact Merge icon with Contact Name.");
				}

				fc.utobj().clickElement(driver, driver
						.findElement(By.xpath(".//a[contains(text () ,'" + firstName_1 + " " + lastName_1 + "')]")));

				fc.utobj().printTestStep("Verify Associate With Campaign");
				boolean isCampaignNamePresentFirst = fc.utobj().verifyElementOnVisible_ByXpath(driver,
						".//td[contains(text () ,'Promotional Campaign')]/following-sibling::td[contains(text () ,'"
								+ firstcampaignName + "')]");
				boolean isCampaignNamePresentSecond = fc.utobj().verifyElementOnVisible_ByXpath(driver,
						".//td[contains(text () ,'Promotional Campaign')]/following-sibling::td[contains(text () ,'"
								+ secondcampaignName + "')]");

				if (isCampaignNamePresentSecond) {
					fc.utobj().printTestStep(
							"verified Campaigns in Contact Merge  If the Assignee is chosen as Regional user then both Corporate and Divisional Campaigns are carried.");

				} else {
					fc.utobj().throwsException(
							"was not able to verify Campaigns  Contact Merge  If the Assignee is chosen as Regional user then both Corporate and Divisional Campaigns are carried.");
				}

			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "crm", "TC_Contact_CRM_A_CampaignCorpFran"},priority=14)
	@TestCase(createdOn = "2017-05-01", updatedOn = "2017-06-27", testCaseDescription = "To verify If the Assignee is chosen as Franchise  user then only  Franchise  Campaigns is carried with Merge", testCaseId = "TC_Contact_CRM_A_CampaignCorpFran"
			+ "")
	public void associateCampaignMergeCorpFran()
			throws IOException, Exception, ParserConfigurationException, SAXException {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());
		String testCaseIdInternal = "TC_08_Copy_And_Customize_Campaign_At_All_Tab";
		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseIdInternal);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		List<String> listInfo = new ArrayList<String>();
		try {
			driver = fc.loginpage().login(driver);
			CRMContactsPage pobj = new CRMContactsPage(driver);
			
			CRMCampaignCenterPageTest campaignCenterPage = new CRMCampaignCenterPageTest();
			CRMCampaignCenterCampaignsPage pobjcam = new CRMCampaignCenterCampaignsPage(driver);

			// First Cycle

			fc.utobj().printTestStep("Navigate To CRM > Campaign Center");
			fc.crm().crm_common().CRMModule(driver);

			fc.utobj().printTestStep("Create First Campaign At Campaign Center > Campaigns");
			String firstcampaignName = fc.utobj().generateTestData(dataSet.get("campaignName"));
			String firstaccessibility = dataSet.get("accessibility");
			String firstcampaignType = dataSet.get("campaignType");
			String firsttemplateName = fc.utobj().generateTestData(dataSet.get("templateName"));
			String firstemailSubject = fc.utobj().generateTestData(dataSet.get("emailSubject"));
			String firstplainTextEditor = fc.utobj().generateTestData(dataSet.get("plainTextEditor"));
			campaignCenterPage.createCampaignGeneric(driver, config, firstcampaignName, firstaccessibility,
					firstcampaignType, firsttemplateName, firstemailSubject, firstplainTextEditor);

			fc.utobj().printTestStep("Verify The First Campaign");
			fc.utobj().clickElement(driver, pobjcam.viewAllButton);

			campaignCenterPage.searchCampaignByFilter(driver, firstcampaignName);

			boolean isCampaignNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firstcampaignName + "')]");
			if (isCampaignNamePresent == false) {
				fc.utobj().throwsException("was not able to verify First Campaign");
			}

			dataSet = fc.utobj().readTestData("crm", testCaseId);

			fc.utobj().printTestStep("Navigate To Admin Users Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			//String emailId = "crmautomation@staffex.com";
			String emailId=fc.utobj().generateTestData("crmautomation")+"@staffex.com";
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUser = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();

			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporateUser.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			String firstName_1 = fc.utobj().generateTestData(dataSet.get("firstName"));
			String lastName_1 = fc.utobj().generateTestData(dataSet.get("lastName"));
			String company_1 = fc.utobj().generateTestData(dataSet.get("company"));
			String assignTo_1 = "Corporate";
			String city_1 = fc.utobj().generateTestData(dataSet.get("city"));
			String address_1 = fc.utobj().generateTestData(dataSet.get("address"));
			String suffix_1 = fc.utobj().generateTestData(dataSet.get("suffix"));
			String jobTitle_1 = fc.utobj().generateTestData(dataSet.get("jobTitle"));
			String comment_1 = fc.utobj().generateTestData(dataSet.get("comment"));
			String franchiseId_1 = "";
			String franchiseUser_1 = "";
			String userNameReg_1 = "";
			String regionName_1 = "";
			String divisionName = "";
			fc.utobj().printTestStep("Navigate To CRM > Contact > Contacts Summary");
			fc.utobj().printTestStep("Add Contact");
			addContact(driver, dataSet, firstName_1, lastName_1, company_1, assignTo_1, city_1, address_1, suffix_1,
					jobTitle_1, comment_1, corpUser.getuserFullName(), regionName_1, userNameReg_1, franchiseId_1,
					franchiseUser_1, divisionName, config);

			fc.utobj().clickElement(driver, pobj.contactsLinks);
			searchLeadByOwner(driver, corpUser.getuserFullName(), "Contacts");
			listInfo.add(corpUser.getuserFullName());
			fc.utobj().sleep(2000);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//a[contains(text () ,'" + firstName_1 + " " + lastName_1 + "')]"));

			fc.utobj().printTestStep("Associate With Campaign");
			fc.utobj().clickElement(driver, pobj.campaignLinkAtLeadInfo);
			fc.utobj().clickElement(driver, pobj.filterContainer);
			fc.utobj().sendKeys(driver, pobj.searchCampaign, firstcampaignName);
			fc.utobj().clickElement(driver, pobj.applyFilter);

			fc.utobj().clickElement(driver, pobj.sendCampaignBtn);
			fc.utobj().clickElement(driver, pobj.confirmBtn);
			fc.utobj().clickElement(driver, pobj.contactsLinks);
			searchLeadByOwner(driver, corpUser.getuserFullName(), "Contacts");
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//a[contains(text () ,'" + firstName_1 + " " + lastName_1 + "')]"));

			fc.utobj().printTestStep("Verify Associate With Campaign");
			isCampaignNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () ,'Promotional Campaign')]/following-sibling::td[contains(text () ,'"
							+ firstcampaignName + "')]");
			if (isCampaignNamePresent == false) {
				fc.utobj().throwsException("was not able to verify Campign Name");
			}

			// Second Cycle
			dataSet = fc.utobj().readTestData("crm", testCaseIdInternal);
			fc.utobj().printTestStep("Navigate To CRM > Campaign Center");
			fc.crm().crm_common().CRMContactsLnk(driver);

			fc.utobj().printTestStep("Create Campaign At Campaign Center > Campaigns");
			String secondcampaignName = fc.utobj().generateTestData(dataSet.get("campaignName"));
			String secondaccessibility = dataSet.get("accessibility");
			String secondcampaignType = dataSet.get("campaignType");
			String secondtemplateName = fc.utobj().generateTestData(dataSet.get("templateName"));
			String secondemailSubject = fc.utobj().generateTestData(dataSet.get("emailSubject"));
			String thirdplainTextEditor = fc.utobj().generateTestData(dataSet.get("plainTextEditor"));
			campaignCenterPage.createCampaignGeneric(driver, config, secondcampaignName, secondaccessibility,
					secondcampaignType, secondtemplateName, secondemailSubject, thirdplainTextEditor);

			fc.utobj().printTestStep("Verify The Campaign");
			fc.utobj().clickElement(driver, pobjcam.viewAllButton);

			campaignCenterPage.searchCampaignByFilter(driver, secondcampaignName);

			isCampaignNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondcampaignName + "')]");
			if (isCampaignNamePresent == false) {
				fc.utobj().throwsException("was not able to verify second Campaign");
			}

			dataSet = fc.utobj().readTestData("crm", testCaseId);

			//emailId = "crmautomation@staffex.com";
			emailId=fc.utobj().generateTestData("crmautomation")+"@staffex.com";
			String franchiseId1 = fc.utobj().generateTestData("Loc");
			String regionName = fc.utobj().generateTestData("TestRegion");
			String firstNameF = fc.utobj().generateTestData("First");
			String lastNameF = fc.utobj().generateTestData("Second");
			fc.utobj().printTestStep("Naviaget To Admin > Franchise Location > Add Franchise Location");
			AdminFranchiseLocationAddFranchiseLocationPageTest franchiseLocation = new AdminFranchiseLocationAddFranchiseLocationPageTest();
			franchiseLocation.addFranchiseLocationForMkt(driver, franchiseId1, regionName, firstNameF, lastNameF);

			fc.utobj().printTestStep("Admin > Users > Manage Franchise Users");
			fc.utobj().printTestStep("Add Franchise User");
			AdminUsersManageManageFranchiseUsersPageTest addFranUser = new AdminUsersManageManageFranchiseUsersPageTest();
			String userNameF = fc.utobj().generateTestData("Fran");
			String roleName = "Default Franchise Role";
			String password = "t0n1ght@123";
			String userFullName = firstNameF + " " + lastNameF;
			addFranUser.addFranchiseUser(driver, userNameF, password, franchiseId1, roleName, emailId);

			String firstName_2 = fc.utobj().generateTestData(dataSet.get("firstName"));
			String lastName_2 = fc.utobj().generateTestData(dataSet.get("lastName"));
			String company_2 = fc.utobj().generateTestData(dataSet.get("company"));
			String assignTo_2 = "Franchise";
			String city_2 = fc.utobj().generateTestData(dataSet.get("city"));
			String address_2 = fc.utobj().generateTestData(dataSet.get("address"));
			String suffix_2 = fc.utobj().generateTestData(dataSet.get("suffix"));
			String jobTitle_2 = fc.utobj().generateTestData(dataSet.get("jobTitle"));
			String comment_2 = fc.utobj().generateTestData(dataSet.get("comment"));
			String franchiseId_2 = franchiseId1;
			String franchiseUser_2 = userFullName;
			String userNameReg_2 = "";
			String regionName_2 = "";
			
			fc.utobj().printTestStep("Navigate To CRM > Contact > Contacts Summary");
			fc.utobj().printTestStep("Add First Contact");
			addContact(driver, dataSet, firstName_2, lastName_2, company_2, assignTo_2, city_2, address_2, suffix_2,
					jobTitle_2, comment_2, userFullName, regionName_2, userNameReg_2, franchiseId_2, franchiseUser_2,
					divisionName, config);

			fc.utobj().clickElement(driver, pobj.contactsLinks);
			searchLeadByOwner(driver, userFullName, "Contacts");
			listInfo.add(userFullName);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//a[contains(text () ,'" + firstName_2 + " " + lastName_2 + "')]"));

			fc.utobj().printTestStep("Associate With Campaign");
			fc.utobj().clickElement(driver, pobj.campaignLinkAtLeadInfo);
			fc.utobj().clickElement(driver, pobj.filterContainer);
			fc.utobj().sendKeys(driver, pobj.searchCampaign, secondcampaignName);
			fc.utobj().clickElement(driver, pobj.applyFilter);

			fc.utobj().clickElement(driver, pobj.sendCampaignBtn);
			fc.utobj().clickElement(driver, pobj.confirmBtn);
			fc.utobj().clickElement(driver, pobj.contactsLinks);
			searchLeadByOwner(driver, userFullName, "Contacts");
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//a[contains(text () ,'" + firstName_2 + " " + lastName_2 + "')]"));

			fc.utobj().printTestStep("Verify Associate With Campaign");
			isCampaignNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () ,'Promotional Campaign')]/following-sibling::td[contains(text () ,'"
							+ secondcampaignName + "')]");
			if (isCampaignNamePresent == false) {
				fc.utobj().throwsException("was not able to verify Campign Name");
			}

			// Merge

			fc.utobj().clickElement(driver, pobj.contactsLinks);
			searchLeadByOwnerDiffrent(driver, corpUser.getuserFullName(), "Contacts", listInfo);

			fc.utobj().printTestStep("Merge Contacts");
			fc.utobj().sleep(2000);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//a[contains(text () ,'"
					+ firstName_1 + " " + lastName_1 + "')]/ancestor::tr/td/input[@name='checkb']"));
			fc.utobj().sleep(2000);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//a[contains(text () ,'"
					+ firstName_2 + " " + lastName_2 + "')]/ancestor::tr/td/input[@name='checkb']"));
			fc.utobj().sleep(2000);
			fc.utobj().selectActionMenuItemsWithTagA(driver, "Merge Contacts");

			boolean furtherProceed = true;
			try {

				String alertText = fc.utobj().acceptAlertBox(driver);

				if (alertText.indexOf("cannot merge") != -1) {
					fc.utobj().printTestStep(
							"Verify Alert Text You cannot Merge Contacts as they have Task associated with them.");
					furtherProceed = false;
				} else {

				}

			} catch (Exception e) {

			}

			if (furtherProceed) {
				if (!fc.utobj().isSelected(driver, pobj.secondassigntoSelect)) {
					fc.utobj().clickElement(driver, pobj.secondassigntoSelect);
				}

				String secondName = fc.utobj()
						.getElementByXpath(driver, ".//*[@value='second' and @name='assignto']/..").getText().trim();
				String firstNameArchived = fc.utobj()
						.getElementByXpath(driver, ".//*[@value='first' and @name='assignto']/..").getText().trim();

				fc.utobj().sendKeys(driver, pobj.remarksAtMerge, "Merge Remarks of Contacts.");
				fc.utobj().clickElement(driver, pobj.mergeBtn);

				fc.utobj().clickElement(driver, pobj.contactsLinks);
				searchLeadByOwner(driver, corpUser.getuserFullName(), "Contacts");

				try {
					fc.utobj().webDriverWait(driver,
							".//a[contains(text () ,'" + firstName_1 + " " + lastName_1 + "')]");
				} catch (Exception e) {
					fc.utobj().printTestStep("First Owner record Lead not displaying verified.");
				}
				fc.utobj().clickElement(driver, pobj.contactsLinks);
				searchLeadByOwner(driver, userFullName, "Contacts");

				fc.utobj().webDriverWait(driver, ".//a[contains(text () ,'" + firstName_1 + " " + lastName_1 + "')]");

				fc.utobj().printTestStep("Verify Merge Contacts");
				boolean isMergeContactNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
						".//a[contains(text () ,'" + firstName_1 + " " + lastName_1 + "')]");
				if (isMergeContactNamePresent == false) {
					fc.utobj().throwsException("was not able to verify Contact Name after Merge Contacts.");
				}

				boolean isOwnerPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//*[contains(text () ,'" + firstName_1 + " "
						+ lastName_1 + "')]/ancestor::tr/td[contains(text () ,'" + userFullName + "')]");
				if (isOwnerPresent == false) {
					fc.utobj().throwsException("was not able to verify Merge Contact owner Contact Summary.");
				}

				boolean isMergeiconPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//*[contains(text () , '" + firstName_1
						+ " " + lastName_1 + "')]/ancestor::tr/td/img[contains(@src, 'lead_merge.gif')]");
				if (isMergeiconPresent == false) {
					fc.utobj().throwsException("was not able to verify Contact Merge icon with Contact Name.");
				}

				fc.utobj().clickElement(driver, driver
						.findElement(By.xpath(".//a[contains(text () ,'" + firstName_1 + " " + lastName_1 + "')]")));

				fc.utobj().printTestStep("Verify Associate With Campaign");
				boolean isCampaignNamePresentFirst = fc.utobj().verifyElementOnVisible_ByXpath(driver,
						".//td[contains(text () ,'Promotional Campaign')]/following-sibling::td[contains(text () ,'"
								+ firstcampaignName + "')]");
				boolean isCampaignNamePresentSecond = fc.utobj().verifyElementOnVisible_ByXpath(driver,
						".//td[contains(text () ,'Promotional Campaign')]/following-sibling::td[contains(text () ,'"
								+ secondcampaignName + "')]");

				if (isCampaignNamePresentSecond) {
					fc.utobj().printTestStep(
							"verified Campaigns in Contact Merge  If the Assignee is chosen as Franchise user then only Corporate Campaigns is carried.");

				} else {
					fc.utobj().throwsException(
							"was not able to verify Campaigns  Contact Merge  If the Assignee is chosen as Franchise user then both Corporate and Franchise Campaigns are carried.");
				}

			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "crm", "TC_Contact_CRM_A_CampaignDivCorp", "crmcontactAreaMerge"},priority=16)
	@TestCase(createdOn = "2017-05-01", updatedOn = "2017-06-27", testCaseDescription = "To verify If the Assignee is chosen as Corporate user then both Corporate campaign is carried with Merge", testCaseId = "TC_Contact_CRM_A_CampaignDivCorp")
	public void associateCampaignMerge_2() throws IOException, Exception, ParserConfigurationException, SAXException {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());
		String testCaseIdInternal = "TC_08_Copy_And_Customize_Campaign_At_All_Tab";
		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseIdInternal);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		List<String> listInfo = new ArrayList<String>();
		try {
			driver = fc.loginpage().login(driver);
			CRMContactsPage pobj = new CRMContactsPage(driver);
			;
			CRMCampaignCenterPageTest campaignCenterPage = new CRMCampaignCenterPageTest();
			CRMGroupsPageTest groupPage = new CRMGroupsPageTest();
			CRMCampaignCenterCampaignsPage pobjcam = new CRMCampaignCenterCampaignsPage(driver);

			// First Cycle

			fc.utobj().printTestStep("Navigate To CRM > Campaign Center");
			fc.crm().crm_common().CRMModule(driver);

			fc.utobj().printTestStep("Create First Campaign At Campaign Center > Campaigns");
			String firstcampaignName = fc.utobj().generateTestData(dataSet.get("campaignName"));
			String firstaccessibility = dataSet.get("accessibility");
			String firstcampaignType = dataSet.get("campaignType");
			String firsttemplateName = fc.utobj().generateTestData(dataSet.get("templateName"));
			String firstemailSubject = fc.utobj().generateTestData(dataSet.get("emailSubject"));
			String firstplainTextEditor = fc.utobj().generateTestData(dataSet.get("plainTextEditor"));
			campaignCenterPage.createCampaignGeneric(driver, config, firstcampaignName, firstaccessibility,
					firstcampaignType, firsttemplateName, firstemailSubject, firstplainTextEditor);

			fc.utobj().printTestStep("Verify The First Campaign");
			fc.utobj().clickElement(driver, pobjcam.viewAllButton);

			campaignCenterPage.searchCampaignByFilter(driver, firstcampaignName);

			boolean isCampaignNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firstcampaignName + "')]");
			if (isCampaignNamePresent == false) {
				fc.utobj().throwsException("was not able to verify First Campaign");
			}

			dataSet = fc.utobj().readTestData("crm", testCaseId);

			fc.utobj().printTestStep("Navigate To Admin Users Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			//String emailId = "crmautomation@staffex.com";
			String emailId=fc.utobj().generateTestData("crmautomation")+"@staffex.com";
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUser = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();

			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporateUser.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			String firstName_1 = fc.utobj().generateTestData(dataSet.get("firstName"));
			String lastName_1 = fc.utobj().generateTestData(dataSet.get("lastName"));
			String company_1 = fc.utobj().generateTestData(dataSet.get("company"));
			String assignTo_1 = "Corporate";
			String city_1 = fc.utobj().generateTestData(dataSet.get("city"));
			String address_1 = fc.utobj().generateTestData(dataSet.get("address"));
			String suffix_1 = fc.utobj().generateTestData(dataSet.get("suffix"));
			String jobTitle_1 = fc.utobj().generateTestData(dataSet.get("jobTitle"));
			String comment_1 = fc.utobj().generateTestData(dataSet.get("comment"));
			String franchiseId_1 = "";
			String franchiseUser_1 = "";
			String userNameReg_1 = "";
			String regionName_1 = "";
			String divisionName = "";
			fc.utobj().printTestStep("Navigate To CRM > Contact > Contacts Summary");
			fc.utobj().printTestStep("Add Contact");
			addContact(driver, dataSet, firstName_1, lastName_1, company_1, assignTo_1, city_1, address_1, suffix_1,
					jobTitle_1, comment_1, corpUser.getuserFullName(), regionName_1, userNameReg_1, franchiseId_1,
					franchiseUser_1, divisionName, config);

			fc.utobj().clickElement(driver, pobj.contactsLinks);
			searchLeadByOwner(driver, corpUser.getuserFullName(), "Contacts");
			listInfo.add(corpUser.getuserFullName());
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//a[contains(text () ,'" + firstName_1 + " " + lastName_1 + "')]"));

			fc.utobj().printTestStep("Associate With Campaign");
			fc.utobj().clickElement(driver, pobj.campaignLinkAtLeadInfo);
			fc.utobj().clickElement(driver, pobj.filterContainer);
			fc.utobj().sendKeys(driver, pobj.searchCampaign, firstcampaignName);
			fc.utobj().clickElement(driver, pobj.applyFilter);

			fc.utobj().clickElement(driver, pobj.sendCampaignBtn);
			fc.utobj().clickElement(driver, pobj.confirmBtn);
			fc.utobj().clickElement(driver, pobj.contactsLinks);
			searchLeadByOwner(driver, corpUser.getuserFullName(), "Contacts");
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//a[contains(text () ,'" + firstName_1 + " " + lastName_1 + "')]"));

			fc.utobj().printTestStep("Verify Associate With Campaign");
			isCampaignNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () ,'Promotional Campaign')]/following-sibling::td[contains(text () ,'"
							+ firstcampaignName + "')]");
			if (isCampaignNamePresent == false) {
				fc.utobj().throwsException("was not able to verify Campign Name");
			}

			// Second Cycle
			dataSet = fc.utobj().readTestData("crm", testCaseIdInternal);
			fc.utobj().printTestStep("Navigate To CRM > Campaign Center");
			fc.crm().crm_common().CRMContactsLnk(driver);

			fc.utobj().printTestStep("Create Campaign At Campaign Center > Campaigns");
			String secondcampaignName = fc.utobj().generateTestData(dataSet.get("campaignName"));
			String secondaccessibility = dataSet.get("accessibility");
			String secondcampaignType = dataSet.get("campaignType");
			String secondtemplateName = fc.utobj().generateTestData(dataSet.get("templateName"));
			String secondemailSubject = fc.utobj().generateTestData(dataSet.get("emailSubject"));
			String thirdplainTextEditor = fc.utobj().generateTestData(dataSet.get("plainTextEditor"));
			campaignCenterPage.createCampaignGeneric(driver, config, secondcampaignName, secondaccessibility,
					secondcampaignType, secondtemplateName, secondemailSubject, thirdplainTextEditor);

			fc.utobj().printTestStep("Verify The Campaign");
			fc.utobj().clickElement(driver, pobjcam.viewAllButton);

			campaignCenterPage.searchCampaignByFilter(driver, secondcampaignName);

			isCampaignNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondcampaignName + "')]");
			if (isCampaignNamePresent == false) {
				fc.utobj().throwsException("was not able to verify second Campaign");
			}

			dataSet = fc.utobj().readTestData("crm", testCaseId);

			fc.utobj().printTestStep("Navigate To Admin Users Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			//emailId = "crmautomation@staffex.com";
			emailId=fc.utobj().generateTestData("crmautomation")+"@staffex.com";
			AdminUsersManageDivisionalUsersAddDivisionalUsersPageTest divuser = new AdminUsersManageDivisionalUsersAddDivisionalUsersPageTest();
			String userName = "Div" + fc.utobj().generateRandomNumber();
			divisionName = "Name" + fc.utobj().generateRandomNumber();
			String regionName = "Reg" + fc.utobj().generateRandomNumber();
			String userFullName = userName + " " + userName;
			divuser.addDivisionalUser(driver, userName, divisionName, emailId);

			String firstName_2 = fc.utobj().generateTestData(dataSet.get("firstName"));
			String lastName_2 = fc.utobj().generateTestData(dataSet.get("lastName"));
			String company_2 = fc.utobj().generateTestData(dataSet.get("company"));
			String assignTo_2 = "Division";
			String city_2 = fc.utobj().generateTestData(dataSet.get("city"));
			String address_2 = fc.utobj().generateTestData(dataSet.get("address"));
			String suffix_2 = fc.utobj().generateTestData(dataSet.get("suffix"));
			String jobTitle_2 = fc.utobj().generateTestData(dataSet.get("jobTitle"));
			String comment_2 = fc.utobj().generateTestData(dataSet.get("comment"));
			String franchiseId_2 = "";
			String franchiseUser_2 = "";
			String userNameReg_2 = "";
			String regionName_2 = "";
			fc.utobj().printTestStep("Navigate To CRM > Contact > Contacts Summary");
			fc.utobj().printTestStep("Add First Contact");
			addContact(driver, dataSet, firstName_2, lastName_2, company_2, assignTo_2, city_2, address_2, suffix_2,
					jobTitle_2, comment_2, userFullName, regionName_2, userNameReg_2, franchiseId_2, franchiseUser_2,
					divisionName, config);

			fc.utobj().clickElement(driver, pobj.contactsLinks);
			searchLeadByOwner(driver, userFullName, "Contacts");
			listInfo.add(userFullName);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//a[contains(text () ,'" + firstName_2 + " " + lastName_2 + "')]"));

			fc.utobj().printTestStep("Associate With Campaign");
			fc.utobj().clickElement(driver, pobj.campaignLinkAtLeadInfo);
			fc.utobj().clickElement(driver, pobj.filterContainer);
			fc.utobj().sendKeys(driver, pobj.searchCampaign, secondcampaignName);
			fc.utobj().clickElement(driver, pobj.applyFilter);

			fc.utobj().clickElement(driver, pobj.sendCampaignBtn);
			fc.utobj().clickElement(driver, pobj.confirmBtn);
			fc.utobj().clickElement(driver, pobj.contactsLinks);
			searchLeadByOwner(driver, userFullName, "Contacts");
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//a[contains(text () ,'" + firstName_2 + " " + lastName_2 + "')]"));

			fc.utobj().printTestStep("Verify Associate With Campaign");
			isCampaignNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () ,'Promotional Campaign')]/following-sibling::td[contains(text () ,'"
							+ secondcampaignName + "')]");
			if (isCampaignNamePresent == false) {
				fc.utobj().throwsException("was not able to verify Campign Name");
			}

			// Merge

			fc.utobj().clickElement(driver, pobj.contactsLinks);

			
			searchLeadByOwnerDiffrent(driver, corpUser.getuserFullName(), "Contacts", listInfo);

			fc.utobj().printTestStep("Merge Contacts");
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//a[contains(text () ,'"
					+ firstName_1 + " " + lastName_1 + "')]/ancestor::tr/td/input[@name='checkb']"));
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//a[contains(text () ,'"
					+ firstName_2 + " " + lastName_2 + "')]/ancestor::tr/td/input[@name='checkb']"));
			fc.utobj().selectActionMenuItemsWithTagA(driver, "Merge Contacts");

			boolean furtherProceed = true;
			try {

				String alertText = fc.utobj().acceptAlertBox(driver);

				if (alertText.indexOf("cannot merge") != -1) {
					fc.utobj().printTestStep(
							"Verify Alert Text You cannot Merge Contacts as they have Task associated with them.");
					furtherProceed = false;
				} else {

				}

			} catch (Exception e) {

			}

			if (furtherProceed) {
				/*
				 * if (!fc.utobj().getElement(driver,
				 * pobj.secondassigntoSelect)) {
				 * fc.utobj().clickElement(driver, pobj.secondassigntoSelect); }
				 */

				String secondName = fc.utobj()
						.getElementByXpath(driver, ".//*[@value='second' and @name='assignto']/..").getText().trim();
				String firstNameArchived = fc.utobj()
						.getElementByXpath(driver, ".//*[@value='first' and @name='assignto']/..").getText().trim();

				fc.utobj().sendKeys(driver, pobj.remarksAtMerge, "Merge Remarks of Contacts.");
				fc.utobj().clickElement(driver, pobj.mergeBtn);

				fc.utobj().clickElement(driver, pobj.contactsLinks);
				searchLeadByOwner(driver, userFullName, "Contacts");

				try {
					fc.utobj().webDriverWait(driver,
							".//a[contains(text () ,'" + firstName_1 + " " + lastName_1 + "')]");
				} catch (Exception e) {
					fc.utobj().printTestStep("verified Second Owner record Lead not displaying .");
				}
				fc.utobj().clickElement(driver, pobj.contactsLinks);
				searchLeadByOwner(driver, corpUser.getuserFullName(), "Contacts");

				fc.utobj().webDriverWait(driver, ".//a[contains(text () ,'" + firstName_1 + " " + lastName_1 + "')]");

				fc.utobj().printTestStep("Verify Merge Contacts");
				boolean isMergeContactNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
						".//a[contains(text () ,'" + firstName_1 + " " + lastName_1 + "')]");
				if (isMergeContactNamePresent == false) {
					fc.utobj().throwsException("was not able to verify Contact Name after Merge Contacts.");
				}

				boolean isOwnerPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//*[contains(text () ,'" + firstName_1 + " "
						+ lastName_1 + "')]/ancestor::tr/td[contains(text () ,'" + corpUser.getuserFullName() + "')]");
				if (isOwnerPresent == false) {
					fc.utobj().throwsException("was not able to verify Merge Contact owner Contact Summary.");
				}

				boolean isMergeiconPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//*[contains(text () , '" + firstName_1
						+ " " + lastName_1 + "')]/ancestor::tr/td/img[contains(@src, 'lead_merge.gif')]");
				if (isMergeiconPresent == false) {
					fc.utobj().throwsException("was not able to verify Contact Merge icon with Contact Name.");
				}

				fc.utobj().clickElement(driver, driver
						.findElement(By.xpath(".//a[contains(text () ,'" + firstName_1 + " " + lastName_1 + "')]")));

				fc.utobj().printTestStep(
						"Verify Associate With Campaign If the Assignee is chosen as Corporate user then Only Corporate Campaigns");
				try {
					boolean isCampaignNamePresentFirst = fc.utobj().verifyElementOnVisible_ByXpath(driver,
							".//td[contains(text () ,'Promotional Campaign')]/following-sibling::td[contains(text () ,'"
									+ firstcampaignName + "')]");
					boolean isCampaignNamePresentSecond = fc.utobj().verifyElementOnVisible_ByXpath(driver,
							".//td[contains(text () ,'Promotional Campaign')]/following-sibling::td[contains(text () ,'"
									+ secondcampaignName + "')]");

					
					
					if (isCampaignNamePresentFirst==true) {
						
						fc.utobj().printTestStep(
								"verified Campaigns in Contact Merge  If the Assignee is chosen as Corporate user then both Corporate Campaigns and Divisional campaigns are carried.");
								
					} else {
						
						fc.utobj().throwsException("was not able to verify Campaigns Contact Merge  If the Assignee is chosen as Corporate user then both Corporate Campaigns are carried.");

					}
				} catch (Exception e) {
					fc.utobj().throwsException(
							"was not able to verify Campaigns Contact Merge  If the Assignee is chosen as Corporate user then both Corporate Campaigns are carried.");

				}

			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "crm", "TC_Contact_CRM_A_CampaignRegCorp", "crmcontactAreaMerge"},priority=18)
	@TestCase(createdOn = "2017-05-01", updatedOn = "2017-06-27", testCaseDescription = "To verify If the Assignee is chosen as Corporate user then Only Corporate Campaigns are carried with Merge", testCaseId = "TC_Contact_CRM_A_CampaignRegCorp")
	public void associateCampaignMerge_RegCorp()
			throws IOException, Exception, ParserConfigurationException, SAXException {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());
		String testCaseIdInternal = "TC_08_Copy_And_Customize_Campaign_At_All_Tab";
		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseIdInternal);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		List<String> listInfo = new ArrayList<String>();
		try {
			driver = fc.loginpage().login(driver);
			CRMContactsPage pobj = new CRMContactsPage(driver);
			;
			CRMCampaignCenterPageTest campaignCenterPage = new CRMCampaignCenterPageTest();
			CRMGroupsPageTest groupPage = new CRMGroupsPageTest();
			CRMCampaignCenterCampaignsPage pobjcam = new CRMCampaignCenterCampaignsPage(driver);

			// First Cycle

			fc.utobj().printTestStep("Navigate To CRM > Campaign Center");
			fc.crm().crm_common().CRMModule(driver);

			fc.utobj().printTestStep("Create First Campaign At Campaign Center > Campaigns");
			String firstcampaignName = fc.utobj().generateTestData(dataSet.get("campaignName"));
			String firstaccessibility = dataSet.get("accessibility");
			String firstcampaignType = dataSet.get("campaignType");
			String firsttemplateName = fc.utobj().generateTestData(dataSet.get("templateName"));
			String firstemailSubject = fc.utobj().generateTestData(dataSet.get("emailSubject"));
			String firstplainTextEditor = fc.utobj().generateTestData(dataSet.get("plainTextEditor"));
			campaignCenterPage.createCampaignGeneric(driver, config, firstcampaignName, firstaccessibility,
					firstcampaignType, firsttemplateName, firstemailSubject, firstplainTextEditor);

			fc.utobj().printTestStep("Verify The First Campaign");
			fc.utobj().clickElement(driver, pobjcam.viewAllButton);

			campaignCenterPage.searchCampaignByFilter(driver, firstcampaignName);

			boolean isCampaignNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firstcampaignName + "')]");
			if (isCampaignNamePresent == false) {
				fc.utobj().throwsException("was not able to verify First Campaign");
			}

			dataSet = fc.utobj().readTestData("crm", testCaseId);

			fc.utobj().printTestStep("Navigate To Admin Users Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			//String emailId = "crmautomation@staffex.com";
			String emailId=fc.utobj().generateTestData("crmautomation")+"@staffex.com";
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUser = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();

			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporateUser.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			String firstName_1 = fc.utobj().generateTestData(dataSet.get("firstName"));
			String lastName_1 = fc.utobj().generateTestData(dataSet.get("lastName"));
			String company_1 = fc.utobj().generateTestData(dataSet.get("company"));
			String assignTo_1 = "Corporate";
			String city_1 = fc.utobj().generateTestData(dataSet.get("city"));
			String address_1 = fc.utobj().generateTestData(dataSet.get("address"));
			String suffix_1 = fc.utobj().generateTestData(dataSet.get("suffix"));
			String jobTitle_1 = fc.utobj().generateTestData(dataSet.get("jobTitle"));
			String comment_1 = fc.utobj().generateTestData(dataSet.get("comment"));
			String franchiseId_1 = "";
			String franchiseUser_1 = "";
			String userNameReg_1 = "";
			String regionName_1 = "";
			String divisionName = "";
			fc.utobj().printTestStep("Navigate To CRM > Contact > Contacts Summary");
			fc.utobj().printTestStep("Add Contact");
			addContact(driver, dataSet, firstName_1, lastName_1, company_1, assignTo_1, city_1, address_1, suffix_1,
					jobTitle_1, comment_1, corpUser.getuserFullName(), regionName_1, userNameReg_1, franchiseId_1,
					franchiseUser_1, divisionName, config);

			fc.utobj().clickElement(driver, pobj.contactsLinks);
			searchLeadByOwner(driver, corpUser.getuserFullName(), "Contacts");
			listInfo.add(corpUser.getuserFullName());
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//a[contains(text () ,'" + firstName_1 + " " + lastName_1 + "')]"));

			fc.utobj().printTestStep("Associate With Campaign");
			fc.utobj().clickElement(driver, pobj.campaignLinkAtLeadInfo);
			fc.utobj().clickElement(driver, pobj.filterContainer);
			fc.utobj().sendKeys(driver, pobj.searchCampaign, firstcampaignName);
			fc.utobj().clickElement(driver, pobj.applyFilter);

			fc.utobj().clickElement(driver, pobj.sendCampaignBtn);
			fc.utobj().clickElement(driver, pobj.confirmBtn);
			fc.utobj().clickElement(driver, pobj.contactsLinks);
			searchLeadByOwner(driver, corpUser.getuserFullName(), "Contacts");
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//a[contains(text () ,'" + firstName_1 + " " + lastName_1 + "')]"));

			fc.utobj().printTestStep("Verify Associate With Campaign");
			isCampaignNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () ,'Promotional Campaign')]/following-sibling::td[contains(text () ,'"
							+ firstcampaignName + "')]");
			if (isCampaignNamePresent == false) {
				fc.utobj().throwsException("was not able to verify Campign Name");
			}

			// Second Cycle
			dataSet = fc.utobj().readTestData("crm", testCaseIdInternal);
			fc.utobj().printTestStep("Navigate To CRM > Campaign Center");
			fc.crm().crm_common().CRMContactsLnk(driver);

			fc.utobj().printTestStep("Create Campaign At Campaign Center > Campaigns");
			String secondcampaignName = fc.utobj().generateTestData(dataSet.get("campaignName"));
			String secondaccessibility = dataSet.get("accessibility");
			String secondcampaignType = dataSet.get("campaignType");
			String secondtemplateName = fc.utobj().generateTestData(dataSet.get("templateName"));
			String secondemailSubject = fc.utobj().generateTestData(dataSet.get("emailSubject"));
			String thirdplainTextEditor = fc.utobj().generateTestData(dataSet.get("plainTextEditor"));
			campaignCenterPage.createCampaignGeneric(driver, config, secondcampaignName, secondaccessibility,
					secondcampaignType, secondtemplateName, secondemailSubject, thirdplainTextEditor);

			fc.utobj().printTestStep("Verify The Campaign");
			fc.utobj().clickElement(driver, pobjcam.viewAllButton);

			campaignCenterPage.searchCampaignByFilter(driver, secondcampaignName);

			isCampaignNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondcampaignName + "')]");
			if (isCampaignNamePresent == false) {
				fc.utobj().throwsException("was not able to verify second Campaign");
			}

			dataSet = fc.utobj().readTestData("crm", testCaseId);

			fc.utobj().printTestStep("Navigate To Admin Users Manage Regional User");
			fc.utobj().printTestStep("Add Regional User");
			emailId = "crmautomation@staffex.com";
			AdminUsersManageRegionalUsersAddRegionalUserPageTest divuser = new AdminUsersManageRegionalUsersAddRegionalUserPageTest();
			String userNameReg = fc.utobj().generateTestData("Reg");
			String regionName = fc.utobj().generateTestData("Area");
			String userFullName = userNameReg + " " + userNameReg;
			divuser.addRegionalUser(driver, userNameReg, regionName, emailId);

			String firstName_2 = fc.utobj().generateTestData(dataSet.get("firstName"));
			String lastName_2 = fc.utobj().generateTestData(dataSet.get("lastName"));
			String company_2 = fc.utobj().generateTestData(dataSet.get("company"));
			String assignTo_2 = "Regional";
			String city_2 = fc.utobj().generateTestData(dataSet.get("city"));
			String address_2 = fc.utobj().generateTestData(dataSet.get("address"));
			String suffix_2 = fc.utobj().generateTestData(dataSet.get("suffix"));
			String jobTitle_2 = fc.utobj().generateTestData(dataSet.get("jobTitle"));
			String comment_2 = fc.utobj().generateTestData(dataSet.get("comment"));
			String franchiseId_2 = "";
			String franchiseUser_2 = "";
			String userNameReg_2 = userFullName;
			String regionName_2 = regionName;
			fc.utobj().printTestStep("Navigate To CRM > Contact > Contacts Summary");
			fc.utobj().printTestStep("Add First Contact");
			addContact(driver, dataSet, firstName_2, lastName_2, company_2, assignTo_2, city_2, address_2, suffix_2,
					jobTitle_2, comment_2, userFullName, regionName_2, userNameReg_2, franchiseId_2, franchiseUser_2,
					divisionName, config);

			fc.utobj().clickElement(driver, pobj.contactsLinks);
			searchLeadByOwner(driver, userFullName, "Contacts");
			listInfo.add(userFullName);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//a[contains(text () ,'" + firstName_2 + " " + lastName_2 + "')]"));

			fc.utobj().printTestStep("Associate With Campaign");
			fc.utobj().clickElement(driver, pobj.campaignLinkAtLeadInfo);
			fc.utobj().clickElement(driver, pobj.filterContainer);
			fc.utobj().sendKeys(driver, pobj.searchCampaign, secondcampaignName);
			fc.utobj().clickElement(driver, pobj.applyFilter);

			fc.utobj().clickElement(driver, pobj.sendCampaignBtn);
			fc.utobj().clickElement(driver, pobj.confirmBtn);
			fc.utobj().clickElement(driver, pobj.contactsLinks);
			searchLeadByOwner(driver, userFullName, "Contacts");
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//a[contains(text () ,'" + firstName_2 + " " + lastName_2 + "')]"));

			fc.utobj().printTestStep("Verify Associate With Campaign");
			isCampaignNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () ,'Promotional Campaign')]/following-sibling::td[contains(text () ,'"
							+ secondcampaignName + "')]");
			if (isCampaignNamePresent == false) {
				fc.utobj().throwsException("was not able to verify Campign Name");
			}

			// Merge

			fc.utobj().clickElement(driver, pobj.contactsLinks);
			searchLeadByOwnerDiffrent(driver, corpUser.getuserFullName(), "Contacts", listInfo);

			fc.utobj().printTestStep("Merge Contacts");
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//a[contains(text () ,'"
					+ firstName_1 + " " + lastName_1 + "')]/ancestor::tr/td/input[@name='checkb']"));
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//a[contains(text () ,'"
					+ firstName_2 + " " + lastName_2 + "')]/ancestor::tr/td/input[@name='checkb']"));
			fc.utobj().selectActionMenuItemsWithTagA(driver, "Merge Contacts");

			boolean furtherProceed = true;
			try {

				String alertText = fc.utobj().acceptAlertBox(driver);

				if (alertText.indexOf("cannot merge") != -1) {
					fc.utobj().printTestStep(
							"Verify Alert Text You cannot Merge Contacts as they have Task associated with them.");
					furtherProceed = false;
				} else {

				}

			} catch (Exception e) {

			}

			if (furtherProceed) {
				/*
				 * if (!fc.utobj().getElement(driver,
				 * pobj.secondassigntoSelect)) {
				 * fc.utobj().clickElement(driver, pobj.secondassigntoSelect); }
				 */

				String secondName = fc.utobj()
						.getElementByXpath(driver, ".//*[@value='second' and @name='assignto']/..").getText().trim();
				String firstNameArchived = fc.utobj()
						.getElementByXpath(driver, ".//*[@value='first' and @name='assignto']/..").getText().trim();

				fc.utobj().sendKeys(driver, pobj.remarksAtMerge, "Merge Remarks of Contacts.");
				fc.utobj().clickElement(driver, pobj.mergeBtn);

				fc.utobj().clickElement(driver, pobj.contactsLinks);
				searchLeadByOwner(driver, userFullName, "Contacts");

				try {
					fc.utobj().webDriverWait(driver,
							".//a[contains(text () ,'" + firstName_1 + " " + lastName_1 + "')]");
				} catch (Exception e) {
					fc.utobj().printTestStep("verified Second Owner record Lead not displaying .");
				}
				fc.utobj().clickElement(driver, pobj.contactsLinks);
				searchLeadByOwner(driver, corpUser.getuserFullName(), "Contacts");

				fc.utobj().webDriverWait(driver, ".//a[contains(text () ,'" + firstName_1 + " " + lastName_1 + "')]");

				fc.utobj().printTestStep("Verify Merge Contacts");
				boolean isMergeContactNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
						".//a[contains(text () ,'" + firstName_1 + " " + lastName_1 + "')]");
				if (isMergeContactNamePresent == false) {
					fc.utobj().throwsException("was not able to verify Contact Name after Merge Contacts.");
				}

				boolean isOwnerPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//*[contains(text () ,'" + firstName_1 + " "
						+ lastName_1 + "')]/ancestor::tr/td[contains(text () ,'" + corpUser.getuserFullName() + "')]");
				if (isOwnerPresent == false) {
					fc.utobj().throwsException("was not able to verify Merge Contact owner Contact Summary.");
				}

				boolean isMergeiconPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//*[contains(text () , '" + firstName_1
						+ " " + lastName_1 + "')]/ancestor::tr/td/img[contains(@src, 'lead_merge.gif')]");
				if (isMergeiconPresent == false) {
					fc.utobj().throwsException("was not able to verify Contact Merge icon with Contact Name.");
				}

				fc.utobj().clickElement(driver, driver
						.findElement(By.xpath(".//a[contains(text () ,'" + firstName_1 + " " + lastName_1 + "')]")));

				fc.utobj().printTestStep(
						"Verify Associate With Campaign If the Assignee is chosen as Corporate user then Only Corporate Campaigns");
				try {
					boolean isCampaignNamePresentFirst = fc.utobj().verifyElementOnVisible_ByXpath(driver,
							".//td[contains(text () ,'Promotional Campaign')]/following-sibling::td[contains(text () ,'"
									+ firstcampaignName + "')]");
					boolean isCampaignNamePresentSecond = fc.utobj().verifyElementOnVisible_ByXpath(driver,
							".//td[contains(text () ,'Promotional Campaign')]/following-sibling::td[contains(text () ,'"
									+ secondcampaignName + "')]");

					if (isCampaignNamePresentSecond) {
						fc.utobj().throwsException(
								"was not able to verify Campaigns  Contact Merge  If the Assignee is chosen as Corporate user then Only Corporate Campaigns are carried.");

					} else {
						fc.utobj().printTestStep(
								"verified Campaigns in Contact Merge  If the Assignee is chosen as Corporate user then Only Corporate Campaigns are carried.");

					}
				} catch (Exception e) {
					fc.utobj().throwsException(
							"was not able to verify Campaigns  Contact Merge  If the Assignee is chosen as Corporate user then Only Corporate Campaigns are carried.");

				}

			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "crm", "TC_Contact_CRM_A_CampaignFranCorp", "hidone" },priority=17)
	@TestCase(createdOn = "2017-05-01", updatedOn = "2017-06-27", testCaseDescription = "To verify If the Assignee is chosen as Corporate user then Only Corporate Campaigns are carried with Merge", testCaseId = "TC_Contact_CRM_A_CampaignFranCorp")
	public void associateCampaignMerge_FranCorp()
			throws IOException, Exception, ParserConfigurationException, SAXException {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());
		String testCaseIdInternal = "TC_08_Copy_And_Customize_Campaign_At_All_Tab";
		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseIdInternal);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		List<String> listInfo = new ArrayList<String>();
		try {
			driver = fc.loginpage().login(driver);
			CRMContactsPage pobj = new CRMContactsPage(driver);
			;
			CRMCampaignCenterPageTest campaignCenterPage = new CRMCampaignCenterPageTest();
			CRMGroupsPageTest groupPage = new CRMGroupsPageTest();
			CRMCampaignCenterCampaignsPage pobjcam = new CRMCampaignCenterCampaignsPage(driver);

			// First Cycle

			fc.utobj().printTestStep("Navigate To CRM > Campaign Center");
			fc.crm().crm_common().CRMModule(driver);

			fc.utobj().printTestStep("Create First Campaign At Campaign Center > Campaigns");
			String firstcampaignName = fc.utobj().generateTestData(dataSet.get("campaignName"));
			String firstaccessibility = dataSet.get("accessibility");
			String firstcampaignType = dataSet.get("campaignType");
			String firsttemplateName = fc.utobj().generateTestData(dataSet.get("templateName"));
			String firstemailSubject = fc.utobj().generateTestData(dataSet.get("emailSubject"));
			String firstplainTextEditor = fc.utobj().generateTestData(dataSet.get("plainTextEditor"));
			campaignCenterPage.createCampaignGeneric(driver, config, firstcampaignName, firstaccessibility,
					firstcampaignType, firsttemplateName, firstemailSubject, firstplainTextEditor);

			fc.utobj().printTestStep("Verify The First Campaign");
			fc.utobj().clickElement(driver, pobjcam.viewAllButton);

			campaignCenterPage.searchCampaignByFilter(driver, firstcampaignName);

			boolean isCampaignNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firstcampaignName + "')]");
			if (isCampaignNamePresent == false) {
				fc.utobj().throwsException("was not able to verify First Campaign");
			}

			dataSet = fc.utobj().readTestData("crm", testCaseId);

			fc.utobj().printTestStep("Navigate To Admin Users Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			//String emailId = "crmautomation@staffex.com";
			String emailId=fc.utobj().generateTestData("crmautomation")+"@staffex.com";
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUser = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();

			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporateUser.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			String firstName_1 = fc.utobj().generateTestData(dataSet.get("firstName"));
			String lastName_1 = fc.utobj().generateTestData(dataSet.get("lastName"));
			String company_1 = fc.utobj().generateTestData(dataSet.get("company"));
			String assignTo_1 = "Corporate";
			String city_1 = fc.utobj().generateTestData(dataSet.get("city"));
			String address_1 = fc.utobj().generateTestData(dataSet.get("address"));
			String suffix_1 = fc.utobj().generateTestData(dataSet.get("suffix"));
			String jobTitle_1 = fc.utobj().generateTestData(dataSet.get("jobTitle"));
			String comment_1 = fc.utobj().generateTestData(dataSet.get("comment"));
			String franchiseId_1 = "";
			String franchiseUser_1 = "";
			String userNameReg_1 = "";
			String regionName_1 = "";
			String divisionName = "";
			fc.utobj().printTestStep("Navigate To CRM > Contact > Contacts Summary");
			fc.utobj().printTestStep("Add Contact");
			addContact(driver, dataSet, firstName_1, lastName_1, company_1, assignTo_1, city_1, address_1, suffix_1,
					jobTitle_1, comment_1, corpUser.getuserFullName(), regionName_1, userNameReg_1, franchiseId_1,
					franchiseUser_1, divisionName, config);

			fc.utobj().clickElement(driver, pobj.contactsLinks);
			searchLeadByOwner(driver, corpUser.getuserFullName(), "Contacts");
			listInfo.add(corpUser.getuserFullName());
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//a[contains(text () ,'" + firstName_1 + " " + lastName_1 + "')]"));

			fc.utobj().printTestStep("Associate With Campaign");
			fc.utobj().clickElement(driver, pobj.campaignLinkAtLeadInfo);
			fc.utobj().clickElement(driver, pobj.filterContainer);
			fc.utobj().sendKeys(driver, pobj.searchCampaign, firstcampaignName);
			fc.utobj().clickElement(driver, pobj.applyFilter);

			fc.utobj().clickElement(driver, pobj.sendCampaignBtn);
			fc.utobj().clickElement(driver, pobj.confirmBtn);
			fc.utobj().clickElement(driver, pobj.contactsLinks);
			searchLeadByOwner(driver, corpUser.getuserFullName(), "Contacts");
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//a[contains(text () ,'" + firstName_1 + " " + lastName_1 + "')]"));

			fc.utobj().printTestStep("Verify Associate With Campaign");
			isCampaignNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () ,'Promotional Campaign')]/following-sibling::td[contains(text () ,'"
							+ firstcampaignName + "')]");
			if (isCampaignNamePresent == false) {
				fc.utobj().throwsException("was not able to verify Campign Name");
			}

			// Second Cycle
			dataSet = fc.utobj().readTestData("crm", testCaseIdInternal);
			fc.utobj().printTestStep("Navigate To CRM > Campaign Center");
			fc.crm().crm_common().CRMContactsLnk(driver);

			fc.utobj().printTestStep("Create Campaign At Campaign Center > Campaigns");
			String secondcampaignName = fc.utobj().generateTestData(dataSet.get("campaignName"));
			String secondaccessibility = dataSet.get("accessibility");
			String secondcampaignType = dataSet.get("campaignType");
			String secondtemplateName = fc.utobj().generateTestData(dataSet.get("templateName"));
			String secondemailSubject = fc.utobj().generateTestData(dataSet.get("emailSubject"));
			String thirdplainTextEditor = fc.utobj().generateTestData(dataSet.get("plainTextEditor"));
			campaignCenterPage.createCampaignGeneric(driver, config, secondcampaignName, secondaccessibility,
					secondcampaignType, secondtemplateName, secondemailSubject, thirdplainTextEditor);

			fc.utobj().printTestStep("Verify The Campaign");
			fc.utobj().clickElement(driver, pobjcam.viewAllButton);

			campaignCenterPage.searchCampaignByFilter(driver, secondcampaignName);

			isCampaignNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondcampaignName + "')]");
			if (isCampaignNamePresent == false) {
				fc.utobj().throwsException("was not able to verify second Campaign");
			}

			dataSet = fc.utobj().readTestData("crm", testCaseId);

			/*
			 * fc.utobj().printTestStep(testCaseId,
			 * "Navigate To Admin Users Manage Regional User");
			 * fc.utobj().printTestStep(testCaseId, "Add Regional User");
			 * emailId="crmautomation@staffex.com";
			 * AdminUsersManageRegionalUsersAddRegionalUserPageTest divuser =
			 * new AdminUsersManageRegionalUsersAddRegionalUserPageTest();
			 * String userNameReg = fc.utobj().generateTestData("Reg"); String
			 * regionName = fc.utobj().generateTestData("Area"); String
			 * userFullName = userNameReg+" "+userNameReg;
			 * divuser.addRegionalUser(driver, userNameReg, regionName, config ,
			 * emailId);
			 */

			//emailId = "crmautomation@staffex.com";
			emailId=fc.utobj().generateTestData("crmautomation")+"@staffex.com";
			String franchiseId1 = fc.utobj().generateTestData("Loc");
			String regionName = fc.utobj().generateTestData("TestRegion");
			String zipCode = fc.utobj().generateRandomNumber();
			String firstNameF = fc.utobj().generateTestData("First");
			String lastNameF = fc.utobj().generateTestData("Second");
			fc.utobj().printTestStep("Naviaget To Admin > Franchise Location > Add Franchise Location");
			AdminFranchiseLocationAddFranchiseLocationPageTest franchiseLocation = new AdminFranchiseLocationAddFranchiseLocationPageTest();
			franchiseLocation.addFranchiseLocationForMkt(driver, franchiseId1, regionName, firstNameF, lastNameF);

			fc.utobj().printTestStep("Admin > Users > Manage Franchise Users");
			fc.utobj().printTestStep("Add Franchise User");
			AdminUsersManageManageFranchiseUsersPageTest addFranUser = new AdminUsersManageManageFranchiseUsersPageTest();
			String userNameF = fc.utobj().generateTestData("Fran");
			String roleName = "Default Franchise Role";
			String password = "t0n1ght@123";
			String userFullName = firstNameF + " " + lastNameF;
			addFranUser.addFranchiseUser(driver, userNameF, password, franchiseId1, roleName, emailId);

			String firstName_2 = fc.utobj().generateTestData(dataSet.get("firstName"));
			String lastName_2 = fc.utobj().generateTestData(dataSet.get("lastName"));
			String company_2 = fc.utobj().generateTestData(dataSet.get("company"));
			String assignTo_2 = "Franchise";
			String city_2 = fc.utobj().generateTestData(dataSet.get("city"));
			String address_2 = fc.utobj().generateTestData(dataSet.get("address"));
			String suffix_2 = fc.utobj().generateTestData(dataSet.get("suffix"));
			String jobTitle_2 = fc.utobj().generateTestData(dataSet.get("jobTitle"));
			String comment_2 = fc.utobj().generateTestData(dataSet.get("comment"));
			String franchiseId_2 = franchiseId1;
			String franchiseUser_2 = userFullName;
			String userNameReg_2 = userFullName;
			String regionName_2 = "";
			fc.utobj().printTestStep("Navigate To CRM > Contact > Contacts Summary");
			fc.utobj().printTestStep("Add First Contact");
			addContact(driver, dataSet, firstName_2, lastName_2, company_2, assignTo_2, city_2, address_2, suffix_2,
					jobTitle_2, comment_2, userFullName, regionName_2, userNameReg_2, franchiseId_2, franchiseUser_2,
					divisionName, config);

			fc.utobj().clickElement(driver, pobj.contactsLinks);
			searchLeadByOwner(driver, userFullName, "Contacts");
			listInfo.add(userFullName);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//a[contains(text () ,'" + firstName_2 + " " + lastName_2 + "')]"));

			fc.utobj().printTestStep("Associate With Campaign");
			fc.utobj().clickElement(driver, pobj.campaignLinkAtLeadInfo);
			fc.utobj().clickElement(driver, pobj.filterContainer);
			fc.utobj().sendKeys(driver, pobj.searchCampaign, secondcampaignName);
			fc.utobj().clickElement(driver, pobj.applyFilter);

			fc.utobj().clickElement(driver, pobj.sendCampaignBtn);
			fc.utobj().clickElement(driver, pobj.confirmBtn);
			fc.utobj().clickElement(driver, pobj.contactsLinks);
			searchLeadByOwner(driver, userFullName, "Contacts");
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//a[contains(text () ,'" + firstName_2 + " " + lastName_2 + "')]"));

			fc.utobj().printTestStep("Verify Associate With Campaign");
			isCampaignNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () ,'Promotional Campaign')]/following-sibling::td[contains(text () ,'"
							+ secondcampaignName + "')]");
			if (isCampaignNamePresent == false) {
				fc.utobj().throwsException("was not able to verify Campign Name");
			}

			// Merge

			fc.utobj().clickElement(driver, pobj.contactsLinks);
			searchLeadByOwnerDiffrent(driver, corpUser.getuserFullName(), "Contacts", listInfo);

			fc.utobj().printTestStep("Merge Contacts");
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//a[contains(text () ,'"
					+ firstName_1 + " " + lastName_1 + "')]/ancestor::tr/td/input[@name='checkb']"));
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//a[contains(text () ,'"
					+ firstName_2 + " " + lastName_2 + "')]/ancestor::tr/td/input[@name='checkb']"));
			fc.utobj().selectActionMenuItemsWithTagA(driver, "Merge Contacts");

			boolean furtherProceed = true;
			try {

				String alertText = fc.utobj().acceptAlertBox(driver);

				if (alertText.indexOf("cannot merge") != -1) {
					fc.utobj().printTestStep(
							"Verify Alert Text You cannot Merge Contacts as they have Task associated with them.");
					furtherProceed = false;
				} else {

				}

			} catch (Exception e) {

			}

			if (furtherProceed) {
				/*
				 * if (!fc.utobj().getElement(driver,
				 * pobj.secondassigntoSelect)) {
				 * fc.utobj().clickElement(driver, pobj.secondassigntoSelect); }
				 */

				String secondName = fc.utobj()
						.getElementByXpath(driver, ".//*[@value='second' and @name='assignto']/..").getText().trim();
				String firstNameArchived = fc.utobj()
						.getElementByXpath(driver, ".//*[@value='first' and @name='assignto']/..").getText().trim();

				fc.utobj().sendKeys(driver, pobj.remarksAtMerge, "Merge Remarks of Contacts.");
				fc.utobj().clickElement(driver, pobj.mergeBtn);

				fc.utobj().clickElement(driver, pobj.contactsLinks);
				searchLeadByOwner(driver, userFullName, "Contacts");

				try {
					fc.utobj().webDriverWait(driver,
							".//a[contains(text () ,'" + firstName_1 + " " + lastName_1 + "')]");
				} catch (Exception e) {
					fc.utobj().printTestStep("verified Second Owner record Lead not displaying .");
				}
				fc.utobj().clickElement(driver, pobj.contactsLinks);
				searchLeadByOwner(driver, corpUser.getuserFullName(), "Contacts");

				fc.utobj().webDriverWait(driver, ".//a[contains(text () ,'" + firstName_1 + " " + lastName_1 + "')]");

				fc.utobj().printTestStep("Verify Merge Contacts");
				boolean isMergeContactNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
						".//a[contains(text () ,'" + firstName_1 + " " + lastName_1 + "')]");
				if (isMergeContactNamePresent == false) {
					fc.utobj().throwsException("was not able to verify Contact Name after Merge Contacts.");
				}

				boolean isOwnerPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//*[contains(text () ,'" + firstName_1 + " "
						+ lastName_1 + "')]/ancestor::tr/td[contains(text () ,'" + corpUser.getuserFullName() + "')]");
				if (isOwnerPresent == false) {
					fc.utobj().throwsException("was not able to verify Merge Contact owner Contact Summary.");
				}

				boolean isMergeiconPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//*[contains(text () , '" + firstName_1
						+ " " + lastName_1 + "')]/ancestor::tr/td/img[contains(@src, 'lead_merge.gif')]");
				if (isMergeiconPresent == false) {
					fc.utobj().throwsException("was not able to verify Contact Merge icon with Contact Name.");
				}

				fc.utobj().clickElement(driver, driver
						.findElement(By.xpath(".//a[contains(text () ,'" + firstName_1 + " " + lastName_1 + "')]")));

				fc.utobj().printTestStep(
						"Verify Associate With Campaign If the Assignee is chosen as Corporate user then Only Corporate Campaigns");
				try {
					boolean isCampaignNamePresentFirst = fc.utobj().verifyElementOnVisible_ByXpath(driver,
							".//td[contains(text () ,'Promotional Campaign')]/following-sibling::td[contains(text () ,'"
									+ firstcampaignName + "')]");
					boolean isCampaignNamePresentSecond = fc.utobj().verifyElementOnVisible_ByXpath(driver,
							".//td[contains(text () ,'Promotional Campaign')]/following-sibling::td[contains(text () ,'"
									+ secondcampaignName + "')]");

					if (isCampaignNamePresentSecond) {
						fc.utobj().throwsException(
								"was not able to verify Campaigns  Contact Merge  If the Assignee is chosen as Corporate user then Only Corporate Campaigns are carried.");

					} else {
						fc.utobj().printTestStep(
								"verified Campaigns in Contact Merge  If the Assignee is chosen as Corporate user then Only Corporate Campaigns are carried.");

					}
				} catch (Exception e) {
					fc.utobj().throwsException(
							"was not able to verify Campaigns  Contact Merge  If the Assignee is chosen as Corporate user then Only Corporate Campaigns are carried.");

				}

			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "crm", "TC_Contact_CRM_MediumTypeCase" })
	@TestCase(createdOn = "2018-01-16", updatedOn = "2018-01-16", testCaseId = "TC_Contact_CRM_MediumTypeCase", testCaseDescription = "To verify that Contact Medium are appearing properly in lead page.")
	private void crmInfoFillAreaConvert_contactType() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		Map<String, String> dataSetCustom = null;
		String testCaseIdInternal = "" + testCaseId;
		Map<String, String> printInfo = null;
		try {
			driver = fc.loginpage().login(driver);
			CRMContactsPage pobj = new CRMContactsPage(driver);

			fc.utobj().printTestStep("Navigating to Admin > CRM > Configure Medium");
			fc.utobj().printTestStep("Add Medium");
			AdminCRMConfigureMediumPageTest mediumPage = new AdminCRMConfigureMediumPageTest();
			String contactMedium = fc.utobj().generateTestData("medium");
			String description = fc.utobj().generateTestData("Dec");
			mediumPage.addMedium(driver, contactMedium, description);

			fc.utobj().printTestStep("Navigate To Admin Users Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			String emailId = "crmautomation@staffex.com";
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUser = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporateUser.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			fc.utobj().printTestStep("Navigate To CRM > Contact > Contacts Summary");
			fc.utobj().printTestStep("Add First Contact");
			fc.crm().crm_common().CRMContactsLnk(driver);
			fc.utobj().clickElement(driver, pobj.addNew);
			fc.utobj().clickElement(driver, pobj.contactLnk);

			testCaseIdInternal = "TC_CRM_QA_Contact_Details";
			dataSetCustom = fc.utobj().readTestData("crm", testCaseIdInternal);
			String firstName = fc.utobj().generateTestData("Aa");
			String lastName = fc.utobj().generateTestData("Bb");
			dataSetCustom.put("contactFirstName", firstName);
			dataSetCustom.put("contactLastName", lastName);
			dataSetCustom.put("contactType", "Contacts");
			dataSetCustom.put("cmLeadTypeID", "Select");
			dataSetCustom.put("cmSource2ID", "Internet");
			dataSetCustom.put("cmSource3ID", "BISON");
			dataSetCustom.put("accountName", "");
			dataSetCustom.put("contactOwnerID2", corpUser.getuserFullName());

			printInfo = fillFormDataWithDataset(driver, config, testCaseIdInternal, dataSetCustom);

			if (printInfo != null && printInfo.size() > 0) {
				// System.out.println("printInfo=First="+printInfo);
			}

			fc.utobj().clickElement(driver, pobj.contactsLinks);

			systemExactSearch(driver, firstName + " " + lastName);

			fc.utobj().printTestStep("Verify Contact Name in Contact Summary.");
			boolean isLeadNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains( text(),'" + firstName + " " + lastName + "')]");
			if (isLeadNamePresent == false) {
				fc.utobj().throwsException("was not able to verify First Contact Name");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "crm", "crmemail", "TC_LEAD_Send_EmailTab"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "To verify that Send Email button is appearing on the Emails Tab against the Contact Name.", testCaseId = "TC_LEAD_Send_EmailTab")
	public void sendEmailTab() throws IOException, Exception, ParserConfigurationException, SAXException {
		String testCaseIntrenal = "TC_84_Send_Email";

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());
		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseIntrenal);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			CRMContactsPage pobj = new CRMContactsPage(driver);

			fc.utobj().printTestStep("Navigate To Admin Users Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			String emailId = "crmautomation@staffex.com";
			//String emailId=fc.utobj().generateTestData("crmautomation")+"@staffex.com";
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUser = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporateUser.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			String firstName = fc.utobj().generateTestData(dataSet.get("firstName"));
			String lastName = fc.utobj().generateTestData(dataSet.get("lastName"));
			String company = fc.utobj().generateTestData(dataSet.get("company"));
			String assignTo = "Corporate";
			String city = fc.utobj().generateTestData(dataSet.get("city"));
			String address = fc.utobj().generateTestData(dataSet.get("address"));
			String suffix = fc.utobj().generateTestData(dataSet.get("suffix"));
			String jobTitle = fc.utobj().generateTestData(dataSet.get("jobTitle"));
			String comment = fc.utobj().generateTestData(dataSet.get("comment"));
			String regionName = "";
			String regionalUser = "";
			String franchiseId = "";
			String franchiseUser = "";

			fc.utobj().printTestStep("Navigate To CRM > Contact > Contacts Summary");
			fc.utobj().printTestStep("Add Contact");

			addContact(driver, dataSet, firstName, lastName, company, assignTo, city, address, suffix, jobTitle,
					comment, corpUser.getuserFullName(), regionName, regionalUser, franchiseId, franchiseUser, config);

			fc.utobj().clickElement(driver, pobj.contactsLinks);
			searchLeadByOwner(driver, corpUser.getuserFullName(), "Contacts");

			fc.utobj().printTestStep("Send Email");
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//a[contains(text () ,'" + firstName + " " + lastName + "')]"));
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//*[@qat_tabname='Emails']"));

			fc.utobj().printTestStep("Verify No Emails Found msg.");
			String norecord = fc.utobj().getText(driver,
					fc.utobj().getElementByXpath(driver, ".//*[@id='sentMailDiv']"));
			if (norecord != null && norecord.indexOf("No Emails") != -1) {

			} else {
				fc.utobj().throwsException("was not able to verify No Emails Found.");
			}

			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//*[contains (text(), 'Send Email')]"));

			if (!fc.utobj().isSelected(driver, pobj.loggedUserId)) {
				fc.utobj().clickElement(driver, pobj.loggedUserId);
			}

			String mailSentToId = fc.utobj().getText(driver, pobj.mailSentTo);
			if (!mailSentToId.equalsIgnoreCase(dataSet.get("emailId"))) {
				// fc.utobj().throwsException("was not able to verify
				// mailSentTo");
			}
			String mailCC = dataSet.get("mailCC");
			fc.utobj().sendKeys(driver, pobj.mailCC, mailCC);

			if (!fc.utobj().isSelected(driver,pobj.showBcc)) {
				fc.utobj().clickElement(driver, pobj.showBcc);
			}
			String mailBCC = dataSet.get("mailBCC");
			fc.utobj().sendKeys(driver, pobj.mailBCC, mailBCC);

			String subjectMail = fc.utobj().generateTestData(dataSet.get("subjectMail"));
			fc.utobj().sendKeys(driver, pobj.subjectMail, subjectMail);

			String editorText = fc.utobj().generateTestData(dataSet.get("mailText"));

			fc.utobj().switchFrameById(driver, "ta_ifr");

			Actions actions = new Actions(driver);
			actions.moveToElement(fc.utobj().getElement(driver, pobj.editorTextArea));
			actions.click();
			actions.sendKeys(editorText);
			fc.utobj().logReportMsg("Entered Text", editorText);
			actions.build().perform();
			fc.utobj().switchFrameToDefault(driver);
			fc.utobj().clickElement(driver, pobj.sendEmailBtn);
			fc.utobj().switchFrameToDefault(driver);
			fc.utobj().sleep(1000);
			fc.utobj().refresh(driver);
			fc.utobj().sleep(1000);

			fc.utobj().printTestStep("Verify Email subject.");
			String expectedSubject = subjectMail;
			String expectedMessageBody = editorText;

			boolean isexpectedSubject = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//*[contains(text(),'" + expectedSubject + "')]");
			if (isexpectedSubject == false) {
				fc.utobj().throwsException("was not able to verify Subject of mail.");
			}

			fc.utobj().refresh(driver);
			fc.utobj().printTestStep("Verify Email Reply .");
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, "//*[contains ( @id,'mailRow' )]/span/img"));

			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='sendButton1']"));
			fc.utobj().refresh(driver);
			fc.utobj().sleep(1000);
			isexpectedSubject = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//*[contains(text(),'" + expectedSubject + "')]");
			if (isexpectedSubject == false) {
				fc.utobj().throwsException("was not able to verify Subject of mail after reply.");
			}

			// Email Details
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//*[contains(text(),'" + expectedSubject + "')]"));
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			isexpectedSubject = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//*[contains(text(),'" + expectedSubject + "')]");
			if (isexpectedSubject == false) {
				fc.utobj().throwsException("was not able to verify Subject of mail after reply in popup details.");
			}

			try {
				fc.utobj().printTestStep("Verify Reply / Close  button in popup tab");
				fc.utobj().moveToElement(driver,
						fc.utobj().getElementByXpath(driver, ".//*[@type='button' and @value='Reply']"));
				fc.utobj().moveToElement(driver,
						fc.utobj().getElementByXpath(driver, ".//*[@type='button' and @value='Close']"));

			} catch (Exception e) {
				fc.utobj().throwsException("was not able to verify Reply / Close button in popup tab");
			}
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//*[@type='button' and @value='Close']"));
			fc.utobj().switchFrameToDefault(driver);

			// Email Details

			boolean isBackButton = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//*[@type='button' and @value='Back']");
			if (isexpectedSubject == false) {
				fc.utobj().throwsException("was not able to verify Back Button in Outbox Email.");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}


	@Test(groups = { "crm", "crmemail", "TC_LEAD_Send_CustomEmailTab"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "To verify that Send Email Custom Email ID is appearing on the Emails Tab against the Contact Name.", testCaseId = "TC_LEAD_Send_CustomEmailTab")
	public void sendEmailTab_custom() throws IOException, Exception, ParserConfigurationException, SAXException {
		String testCaseIntrenal = "TC_84_Send_Email";

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());
		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseIntrenal);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			CRMContactsPage pobj = new CRMContactsPage(driver);

			fc.utobj().printTestStep("Navigate To Admin Users Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			String emailId = "crmautomation@staffex.com";
			//String emailId=fc.utobj().generateTestData("crmautomation")+"@staffex.com";
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUser = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser); 
			corporateUser.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			String firstName = fc.utobj().generateTestData(dataSet.get("firstName"));
			String lastName = fc.utobj().generateTestData(dataSet.get("lastName"));
			String company = fc.utobj().generateTestData(dataSet.get("company"));
			String assignTo = "Corporate";
			String city = fc.utobj().generateTestData(dataSet.get("city"));
			String address = fc.utobj().generateTestData(dataSet.get("address"));
			String suffix = fc.utobj().generateTestData(dataSet.get("suffix"));
			String jobTitle = fc.utobj().generateTestData(dataSet.get("jobTitle"));
			String comment = fc.utobj().generateTestData(dataSet.get("comment"));
			String regionName = "";
			String regionalUser = "";
			String franchiseId = "";
			String franchiseUser = "";

			fc.utobj().printTestStep("Navigate To CRM > Contact > Contacts Summary");
			fc.utobj().printTestStep("Add Contact");

			addContact(driver, dataSet, firstName, lastName, company, assignTo, city, address, suffix, jobTitle,
					comment, corpUser.getuserFullName(), regionName, regionalUser, franchiseId, franchiseUser, config);

			fc.utobj().clickElement(driver, pobj.contactsLinks);
			searchLeadByOwner(driver, corpUser.getuserFullName(), "Contacts");

			fc.utobj().printTestStep("Send Email");
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//a[contains(text () ,'" + firstName + " " + lastName + "')]"));
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//*[@qat_tabname='Emails']"));

			fc.utobj().printTestStep("Verify No Emails Found msg.");
			String norecord = fc.utobj().getText(driver,
					fc.utobj().getElementByXpath(driver, ".//*[@id='sentMailDiv']"));
			if (norecord != null && norecord.indexOf("No Emails") != -1) {

			} else {
				fc.utobj().throwsException("was not able to verify No Emails Found.");
			}

			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//*[contains (text(), 'Send Email')]"));

			if (!fc.utobj().isSelected(driver, pobj.customId)) {
				fc.utobj().clickElement(driver, pobj.customId);
				fc.utobj().sendKeys(driver, pobj.customIdField, dataSet.get("emailId"));
			}

			String mailSentToId = fc.utobj().getText(driver, pobj.mailSentTo);
			if (!mailSentToId.equalsIgnoreCase(dataSet.get("emailId"))) {
				// fc.utobj().throwsException("was not able to verify
				// mailSentTo");
			}
			String mailCC = dataSet.get("mailCC");
			fc.utobj().sendKeys(driver, pobj.mailCC, mailCC);

			if (!fc.utobj().isSelected(driver,pobj.showBcc)) {
				fc.utobj().clickElement(driver, pobj.showBcc);
			}
			String mailBCC = dataSet.get("mailBCC");
			fc.utobj().sendKeys(driver, pobj.mailBCC, mailBCC);

			String subjectMail = fc.utobj().generateTestData(dataSet.get("subjectMail"));
			fc.utobj().sendKeys(driver, pobj.subjectMail, subjectMail);

			String editorText = fc.utobj().generateTestData(dataSet.get("mailText"));

			fc.utobj().switchFrameById(driver, "ta_ifr");

			Actions actions = new Actions(driver);
			actions.moveToElement(fc.utobj().getElement(driver, pobj.editorTextArea));
			actions.click();
			actions.sendKeys(editorText);
			fc.utobj().logReportMsg("Entered Text", editorText);
			actions.build().perform();
			fc.utobj().switchFrameToDefault(driver);
			fc.utobj().clickElement(driver, pobj.sendEmailBtn);
			fc.utobj().switchFrameToDefault(driver);
			fc.utobj().sleep(1000);
			fc.utobj().refresh(driver);
			fc.utobj().printTestStep("Verify Email subject.");
			String expectedSubject = subjectMail;
			String expectedMessageBody = editorText;

			fc.utobj().webDriverWait(driver, ".//*[contains(text(),'" + expectedSubject + "')]");

			boolean isexpectedSubject = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//*[contains(text(),'" + expectedSubject + "')]");
			if (isexpectedSubject == false) {
				fc.utobj().throwsException("was not able to verify Subject of mail.");
			}

			fc.utobj().printTestStep("Verify Email Reply .");
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, "//*[contains ( @id,'mailRow' )]/span/img"));

			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='sendButton1']"));
			fc.utobj().refresh(driver);
			isexpectedSubject = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//*[contains(text(),'" + expectedSubject + "')]");
			if (isexpectedSubject == false) {
				fc.utobj().throwsException("was not able to verify Subject of mail after reply.");
			}

			boolean isBackButton = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//*[@type='button' and @value='Back']");
			if (isexpectedSubject == false) {
				fc.utobj().throwsException("was not able to verify Back Button in Outbox Email.");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "crm", "TC_CRM_CONTACT_Reset_Remarks" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify the Rest Remarks At CRM > Lead > Lead Info", testCaseId = "TC_CRM_CONTACT_Reset_Remarks")
	public void addRemarksTab() throws IOException, Exception, ParserConfigurationException, SAXException {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());
		String testCaseInternal = "TC_90_Add_Remarks";
		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseInternal);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			CRMContactsPage pobj = new CRMContactsPage(driver);

			fc.utobj().printTestStep("Navigate To Admin Users Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			//String emailId = "crmautomation@staffex.com";

			String emailId=fc.utobj().generateTestData("crmautomation")+"@staffex.com";
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUser = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporateUser.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			String firstName = fc.utobj().generateTestData(dataSet.get("firstName"));
			String lastName = fc.utobj().generateTestData(dataSet.get("lastName"));
			String company = fc.utobj().generateTestData(dataSet.get("company"));
			String assignTo = "Corporate";
			String city = fc.utobj().generateTestData(dataSet.get("city"));
			String address = fc.utobj().generateTestData(dataSet.get("address"));
			String suffix = fc.utobj().generateTestData(dataSet.get("suffix"));
			String jobTitle = fc.utobj().generateTestData(dataSet.get("jobTitle"));
			String comment = fc.utobj().generateTestData(dataSet.get("comment"));
			String franchiseId = "";
			String franchiseUser = "";
			String userNameReg = "";
			String regionName = "";

			fc.utobj().printTestStep("Navigate To CRM > Contact > Contacts Summary");
			fc.utobj().printTestStep("Add Contact");

			addContact(driver, dataSet, firstName, lastName, company, assignTo, city, address, suffix, jobTitle,
					comment, corpUser.getuserFullName(), regionName, userNameReg, franchiseId, franchiseUser, config);

			fc.utobj().clickElement(driver, pobj.contactsLinks);
			searchLeadByOwner(driver, corpUser.getuserFullName(), "Contacts");

			fc.utobj().printTestStep("Add Remarks");
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//a[contains(text () ,'" + firstName + " " + lastName + "')]"));
			fc.utobj().clickElement(driver, pobj.addRemarks);

			String remarks = fc.utobj().generateTestData(dataSet.get("remarkField"));
			fc.utobj().sendKeys(driver, pobj.remarkField, remarks);

			try {
				fc.utobj().moveToElement(driver,
						fc.utobj().getElementByXpath(driver, ".//*[@type='reset' and @value='Reset']"));

				String remarksText = fc.utobj().getElementByXpath(driver, ".//*[@id='remark']").getText();

				// System.out.println("remarksText==="+remarksText);

				if (remarksText != null && !"".equals(remarksText) && !"null".equals(remarksText)) {
					fc.utobj().throwsException("was not able to verify Reset button");
				}

			} catch (Exception e) {
				fc.utobj().throwsException("was not able to verify Reset button");
			}
			fc.utobj().sendKeys(driver, pobj.remarkField, remarks);
			fc.utobj().clickElement(driver, pobj.saveBtn);

			fc.utobj().printTestStep("Verify Remarks");
			boolean isRemarksPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[@title='Remark']/span[.='" + remarks + "']");
			if (isRemarksPresent == false) {
				fc.utobj().throwsException("was not able to verify remarks");
			}

			fc.utobj().printTestStep("Verify Remarks At Detailed History Frame");
			fc.utobj().clickElement(driver, pobj.detailedHistoryLink);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().clickElement(driver, pobj.remarksTabAtCbox);
			boolean isRemarksAtCboxPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () ,'" + remarks + "')]");
			if (isRemarksAtCboxPresent == false) {
				fc.utobj().throwsException("was not able to verify Remarks At Detailed History Cbox");
			}
			fc.utobj().clickElement(driver, pobj.closeBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "crm", "TC_CRM_CONTACT_Add_AnotherLead" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify theSave and Add Another Lead At CRM > Lead > Lead Info", testCaseId = "TC_CRM_CONTACT_Add_AnotherLead")
	public void addAnotherLead() throws IOException, Exception, ParserConfigurationException, SAXException {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());
		String testCaseInternal = "TC_90_Add_Remarks";
		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseInternal);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			CRMContactsPage pobj = new CRMContactsPage(driver);

			fc.utobj().printTestStep("Navigate To Admin Users Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			//String emailId = "crmautomation@staffex.com";
			String emailId=fc.utobj().generateTestData("crmautomation")+"@staffex.com";
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUser = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporateUser.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			String firstName = fc.utobj().generateTestData(dataSet.get("firstName"));
			String lastName = fc.utobj().generateTestData(dataSet.get("lastName"));
			String company = fc.utobj().generateTestData(dataSet.get("company"));
			String assignTo = "Corporate";
			String city = fc.utobj().generateTestData(dataSet.get("city"));
			String address = fc.utobj().generateTestData(dataSet.get("address"));
			String suffix = fc.utobj().generateTestData(dataSet.get("suffix"));
			String jobTitle = fc.utobj().generateTestData(dataSet.get("jobTitle"));
			String comment = fc.utobj().generateTestData(dataSet.get("comment"));
			String franchiseId = "";
			String franchiseUser = "";
			String userNameReg = "";
			String regionName = "";

			fc.utobj().printTestStep("Navigate To CRM > Contact > Contacts Summary");
			fc.utobj().printTestStep("Add Contact");

			addContactAnother(driver, dataSet, firstName, lastName, company, assignTo, city, address, suffix, jobTitle,
					comment, corpUser.getuserFullName(), regionName, userNameReg, franchiseId, franchiseUser, config);

			fc.utobj().clickElement(driver, pobj.contactsLinks);
			searchLeadByOwner(driver, corpUser.getuserFullName(), "Contacts");

			fc.utobj().printTestStep("Add Remarks");
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//a[contains(text () ,'" + firstName + " " + lastName + "')]"));
			fc.utobj().clickElement(driver, pobj.addRemarks);

			String remarks = fc.utobj().generateTestData(dataSet.get("remarkField"));
			fc.utobj().sendKeys(driver, pobj.remarkField, remarks);

			try {
				fc.utobj().moveToElement(driver,
						fc.utobj().getElementByXpath(driver, ".//*[@type='reset' and @value='Reset']"));

				String remarksText = fc.utobj().getElementByXpath(driver, ".//*[@id='remark']").getText();

				// System.out.println("remarksText==="+remarksText);

				if (remarksText != null && !"".equals(remarksText) && !"null".equals(remarksText)) {
					fc.utobj().throwsException("was not able to verify Reset button");
				}

			} catch (Exception e) {
				fc.utobj().throwsException("was not able to verify Reset button");
			}
			fc.utobj().sendKeys(driver, pobj.remarkField, remarks);
			fc.utobj().clickElement(driver, pobj.saveBtn);

			fc.utobj().printTestStep("Verify Remarks");
			boolean isRemarksPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[@title='Remark']/span[.='" + remarks + "']");
			if (isRemarksPresent == false) {
				fc.utobj().throwsException("was not able to verify remarks");
			}

			fc.utobj().printTestStep("Verify Remarks At Detailed History Frame");
			fc.utobj().clickElement(driver, pobj.detailedHistoryLink);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().clickElement(driver, pobj.remarksTabAtCbox);
			boolean isRemarksAtCboxPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () ,'" + remarks + "')]");
			if (isRemarksAtCboxPresent == false) {
				fc.utobj().throwsException("was not able to verify Remarks At Detailed History Cbox");
			}
			fc.utobj().clickElement(driver, pobj.closeBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "crm", "TC_CRM_CONTACT_Add_ResetLead"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify Reset button Lead At CRM > Lead > Lead Info", testCaseId = "TC_CRM_CONTACT_Add_ResetLead")
	public void addRestLead() throws IOException, Exception, ParserConfigurationException, SAXException {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());
		String testCaseInternal = "TC_90_Add_Remarks";
		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseInternal);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			CRMContactsPage pobj = new CRMContactsPage(driver);

			fc.utobj().printTestStep("Navigate To Admin Users Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			//String emailId = "crmautomation@staffex.com";
			String emailId=fc.utobj().generateTestData("crmautomation")+"@staffex.com";
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUser = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporateUser.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);
             System.out.println(corpUser.getuserFullName());
			String firstName = fc.utobj().generateTestData(dataSet.get("firstName"));
			String lastName = fc.utobj().generateTestData(dataSet.get("lastName"));
			String company = fc.utobj().generateTestData(dataSet.get("company"));
			String assignTo = "Corporate";
			String city = fc.utobj().generateTestData(dataSet.get("city"));
			String address = fc.utobj().generateTestData(dataSet.get("address"));
			String suffix = fc.utobj().generateTestData(dataSet.get("suffix"));
			String jobTitle = fc.utobj().generateTestData(dataSet.get("jobTitle"));
			String comment = fc.utobj().generateTestData(dataSet.get("comment"));
			String franchiseId = "";
			String franchiseUser = "";
			String userNameReg = "";
			String regionName = "";

			fc.utobj().printTestStep("Navigate To CRM > Contact > Contacts Summary");
			fc.utobj().printTestStep("Add Contact");

			addContactReset(driver, dataSet, firstName, lastName, company, assignTo, city, address, suffix, jobTitle,
					comment, corpUser.getuserFullName(), regionName, userNameReg, franchiseId, franchiseUser, config);

			fc.utobj().clickElement(driver, pobj.contactsLinks);
			searchLeadByOwner(driver, corpUser.getuserFullName(), "Contacts");

			fc.utobj().printTestStep("Add Remarks");
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//a[contains(text () ,'" + firstName + " " + lastName + "')]"));
			fc.utobj().clickElement(driver, pobj.addRemarks);

			String remarks = fc.utobj().generateTestData(dataSet.get("remarkField"));
			fc.utobj().sendKeys(driver, pobj.remarkField, remarks);

			try {
				fc.utobj().moveToElement(driver,
						fc.utobj().getElementByXpath(driver, ".//*[@type='reset' and @value='Reset']"));

				String remarksText = fc.utobj().getElementByXpath(driver, ".//*[@id='remark']").getText();

				// System.out.println("remarksText==="+remarksText);

				if (remarksText != null && !"".equals(remarksText) && !"null".equals(remarksText)) {
					fc.utobj().throwsException("was not able to verify Reset button");
				}

			} catch (Exception e) {
				fc.utobj().throwsException("was not able to verify Reset button");
			}
			fc.utobj().sendKeys(driver, pobj.remarkField, remarks);
			fc.utobj().clickElement(driver, pobj.saveBtn);

			fc.utobj().printTestStep("Verify Remarks");
			boolean isRemarksPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[@title='Remark']/span[.='" + remarks + "']");
			if (isRemarksPresent == false) {
				fc.utobj().throwsException("was not able to verify remarks");
			}

			fc.utobj().printTestStep("Verify Remarks At Detailed History Frame");
			fc.utobj().clickElement(driver, pobj.detailedHistoryLink);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().clickElement(driver, pobj.remarksTabAtCbox);
			boolean isRemarksAtCboxPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () ,'" + remarks + "')]");
			if (isRemarksAtCboxPresent == false) {
				fc.utobj().throwsException("was not able to verify Remarks At Detailed History Cbox");
			}
			fc.utobj().clickElement(driver, pobj.closeBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "crm", "crmLeadAreaBatchPrint", "TC_Contact_CRM_BatchPrintCase","helloworld500"})
	@TestCase(createdOn = "2018-01-16", updatedOn = "2018-01-16", testCaseId = "TC_Contact_CRM_BatchPrintCase", testCaseDescription = "To Verify Batch Print contacts behaviours.")
	private void crmInfoFillAreaBatch_Print() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		Map<String, String> dataSetCustom = null;
		String testCaseIdInternal = "" + testCaseId;
		Map<String, String> printInfo = null;
		try {
			driver = fc.loginpage().login(driver);
			CRMContactsPage pobj = new CRMContactsPage(driver);
			;

			fc.utobj().printTestStep("Navigate To Admin Users Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			String emailId = "crmautomation@staffex.com";
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUser = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporateUser.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			fc.utobj().printTestStep("Navigate To CRM > Contact > Contacts Summary");
			fc.utobj().printTestStep("Add First Contact");
			fc.crm().crm_common().CRMContactsLnk(driver);
			fc.utobj().clickElement(driver, pobj.addNew);
			fc.utobj().clickElement(driver, pobj.contactLnk);

			try {

				boolean isNewStatus = fc.utobj().verifyElementOnVisible_ByXpath(driver,
						".//td[contains(text () ,'Status')]/ancestor::tr/td[contains(text () ,'New')]");
				if (isNewStatus == false) {
					fc.utobj().throwsException("was not able to verify Default Status As New.");
				}
			} catch (Exception e) {

			}

			testCaseIdInternal = "TC_CRM_QA_Contact_Details";
			dataSetCustom = fc.utobj().readTestData("crm", testCaseIdInternal);
			String firstName = "Harish";
			String lastName = "Dwivedi";
			dataSetCustom.put("contactFirstName", firstName);
			dataSetCustom.put("contactLastName", lastName);
			dataSetCustom.put("cmLeadTypeID", "Select");
			dataSetCustom.put("cmSource2ID", "Internet");
			dataSetCustom.put("cmSource3ID", "BISON");
			dataSetCustom.put("accountName", "");
			dataSetCustom.put("contactOwnerID2", corpUser.getuserFullName());
			
			printInfo = fillFormDataWithDataset(driver, config, testCaseIdInternal, dataSetCustom);
			// Reporter.log("Map == for ="+testCaseIdInternal+"==="+printInfo);

			if (printInfo != null && printInfo.size() > 0) {
				// System.out.println("printInfo=First="+printInfo);

				// System.out.println("printInfo=First Email
				// ="+printInfo.get("emailIds"));
			}

			fc.utobj().clickElement(driver, pobj.contactsLinks);

			systemExactSearch(driver, firstName + " " + lastName);

			fc.utobj().printTestStep("Verify Contact Name in Contact Summary.");
			boolean isLeadNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains( text(),'" + firstName + " " + lastName + "')]");
			if (isLeadNamePresent == false) {
				fc.utobj().throwsException("was not able to verify First Contact Name");
			}

			fc.utobj().printTestStep("Navigate To CRM > Contact > Contacts Summary");
			fc.utobj().printTestStep("Add Second Contact");
			fc.crm().crm_common().CRMContactsLnk(driver);
			fc.utobj().clickElement(driver, pobj.addNew);
			fc.utobj().clickElement(driver, pobj.contactLnk);

			testCaseIdInternal = "TC_CRM_QA_Contact_Details";
			dataSetCustom = fc.utobj().readTestData("crm", testCaseIdInternal);
			String firstNameSecond = firstName;
			String lastNameSecond = lastName;
			dataSetCustom.put("contactFirstName", firstNameSecond);
			dataSetCustom.put("contactLastName", lastNameSecond);
			dataSetCustom.put("cmLeadTypeID", "Select");
			dataSetCustom.put("cmSource2ID", "Internet");
			dataSetCustom.put("cmSource3ID", "BISON");
			dataSetCustom.put("accountName", "");
			dataSetCustom.put("contactOwnerID2", corpUser.getuserFullName());

			printInfo = fillFormDataWithDataset(driver, config, testCaseIdInternal, dataSetCustom);
			// Reporter.log("Map == for ="+testCaseIdInternal+"==="+printInfo);

			if (printInfo != null && printInfo.size() > 0) {
				// System.out.println("printInfo=Second="+printInfo);

				// System.out.println("printInfo=Second Email
				// ="+printInfo.get("emailIds"));
			}

			fc.utobj().clickElement(driver, pobj.contactsLinks);

			systemExactSearch(driver, firstNameSecond + " " + lastNameSecond);

			fc.utobj().printTestStep("Verify Second Contact Name in Contact Summary.");
			isLeadNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains( text(),'" + firstNameSecond + " " + lastNameSecond + "')]");
			if (isLeadNamePresent == false) {
				fc.utobj().throwsException("was not able to verify Second Contact Name.");
			}

			fc.utobj().clickElement(driver, pobj.contactsLinks);
			searchLeadByOwnerDuplcate(driver, corpUser.getuserFullName(), "Duplicate Contact");

			isLeadNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains( text(),'" + firstNameSecond + " " + lastNameSecond + "')]");
			if (isLeadNamePresent == false) {
				fc.utobj().throwsException("was not able to verify Duplicate Contact Name.");
			}

			/*try {
				selectActionMenuItemsWithTagAMoreActions(driver, "Print");
				VerifyPrintPreview(driver, firstNameSecond);

			} catch (Exception w) {
				fc.utobj().throwsException("was not able to verify Print functionality.");
			}*/

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "crm", "TC_CRM_CONTACT_Subcription_Lead"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify the Modify Contact By Action Image At CRM > Lead > Contact Summary", testCaseId = "TC_CRM_CONTACT_Subcription_Lead")
	public void modifyLeadActionImage() throws IOException, Exception, ParserConfigurationException, SAXException {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());
		String testCaseInternal = "TC_59_Modify_Lead";
		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseInternal);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		try {	
		
			driver = fc.loginpage().login(driver);
			CRMContactsPage pobj = new CRMContactsPage(driver);
			fc.utobj().printTestStep("Navigate To Admin Users Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			//String emailId = "crmautomation@staffex.com";
			String emailId=fc.utobj().generateTestData("crmautomation")+"@staffex.com";
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUser = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporateUser.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			fc.utobj().printTestStep("Navigating to Admin > CRM > Configure Status");
			fc.utobj().printTestStep("Add Status Lead Type");

			AdminCRMConfigureStatusPageTest statusPage = new AdminCRMConfigureStatusPageTest();
			String statusName = fc.utobj().generateTestData(dataSet.get("statusName"));
			statusPage.addStatus(driver, statusName);

			fc.utobj().printTestStep("Navigate To CRM > Contact > Contacts Summary");
			fc.utobj().printTestStep("Add Contact");

			String firstName = fc.utobj().generateTestData(dataSet.get("firstName"));
			String lastName = fc.utobj().generateTestData(dataSet.get("lastName"));
			String company = fc.utobj().generateTestData(dataSet.get("company"));
			String assignTo = "Corporate";
			String city = fc.utobj().generateTestData(dataSet.get("city"));
			String email = "xyz@franconnect.net";
			String address = fc.utobj().generateTestData(dataSet.get("address"));
			String suffix = fc.utobj().generateTestData(dataSet.get("suffix"));
			String jobTitle = fc.utobj().generateTestData(dataSet.get("jobTitle"));
			String comment = fc.utobj().generateTestData(dataSet.get("comment"));
			String regionName = "";
			String regionalUser = "";
			String franchiseId = "";
			String franchiseUser = "";

			addContact(driver, dataSet, firstName, lastName, company, assignTo, city, address, suffix, jobTitle,
					comment, corpUser.getuserFullName(), regionName, regionalUser, franchiseId, franchiseUser, config);

			fc.utobj().clickElement(driver, pobj.contactsLinks);
			searchLeadByOwner(driver, corpUser.getuserFullName(), "Contacts");

			fc.utobj().printTestStep("Modify Contact");

			fc.utobj().actionImgOption(driver, firstName + " " + lastName, "Modify Details");

			fc.utobj().selectDropDown(driver, pobj.title, dataSet.get("title"));
 			fc.utobj().selectDropDown(driver, pobj.contactType, statusName);
			firstName = fc.utobj().generateTestData(dataSet.get("firstName"));
			fc.utobj().sendKeys(driver, pobj.contactFirstName, firstName);
			lastName = fc.utobj().generateTestData(dataSet.get("lastName"));
			fc.utobj().sendKeys(driver, pobj.contactLastName, lastName);
			/*company = fc.utobj().generateTestData(dataSet.get("company"));
			fc.utobj().sendKeys(driver, pobj.companyName, company);
			fc.utobj().selectDropDown(driver, pobj.primaryContactMethodSelect, dataSet.get("primaryContactMethod"));*/
			address = fc.utobj().generateTestData(dataSet.get("address"));
			fc.utobj().sendKeys(driver, pobj.address, address);
			city = fc.utobj().generateTestData(dataSet.get("city"));
			fc.utobj().sendKeys(driver, pobj.city, city);
			fc.utobj().selectDropDown(driver, pobj.country, "USA");
			fc.utobj().selectDropDown(driver, pobj.state, "Alabama");
			fc.utobj().sendKeys(driver, pobj.zipcode, "12345");
			fc.utobj().sendKeys(driver, pobj.phoneNumbers, "1236547896");
			fc.utobj().sendKeys(driver, pobj.extn, "12");
			fc.utobj().sendKeys(driver, pobj.faxNumbers, "1263985487");
			fc.utobj().sendKeys(driver, pobj.mobileNumbers, "8896724441");
			fc.utobj().sendKeys(driver, pobj.emailIds, email);
			fc.utobj().sendKeys(driver, pobj.alternateEmail, "test@gmail.com");
			fc.utobj().generateTestData(dataSet.get("suffix"));
			fc.utobj().sendKeys(driver, pobj.suffix, suffix);
			fc.utobj().generateTestData(dataSet.get("jobTitle"));
			fc.utobj().sendKeys(driver, pobj.position, jobTitle);
			fc.utobj().sendKeys(driver, pobj.birthdate, "11/24/1980");
			fc.utobj().sendKeys(driver, pobj.anniversarydate, "11/24/1999");

			fc.utobj().selectDropDown(driver, pobj.rating, dataSet.get("rating"));

			fc.utobj().sendKeys(driver, pobj.bestTimeToContact, "12:00 PM EST");
			fc.utobj().generateTestData(dataSet.get("comment"));
			fc.utobj().sendKeys(driver, pobj.comments, comment);

			try {
				fc.utobj().clickElement(driver, driver
						.findElement(By.xpath(".//input[@type='radio' and  @name='inMailingList' and @value='1' ]")));
				String alertText = fc.utobj().acceptAlertBox(driver);
			} catch (Exception e) {

			}

			fc.utobj().clickElement(driver, pobj.saveBtn);

			// verify at Home Page
			fc.utobj().printTestStep("Verify Leads At Leads Page");
			fc.utobj().clickElement(driver, pobj.contactsLinks);
			searchLeadByOwner(driver, corpUser.getuserFullName(), "Contacts");
			

			
			
			if (!fc.utobj().getElementByXpath(driver, ".//a[contains(text () ,'" + firstName + " " + lastName + "')]")
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Name");
			}
			/*if (!driver
					.findElement(By.xpath(".//a[.='" + firstName + " " + lastName
							+ "']/ancestor::tr/td[contains(text () , '" + corpUser.getuserFullName() + "')]"))
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify owner of the lead");
			}*/
			/*if (!fc.utobj().getElementByXpath(driver, ".//a[.='" + firstName + " " + lastName
					+ "']/ancestor::tr/td[contains(text () , '" + statusName + "')]").isDisplayed()) {
				fc.utobj().throwsException("was not able to verify staus of the lead");
			}*/

			/*if (!fc.utobj().getElementByXpath(driver, ".//a[.='" + firstName + " " + lastName
					+ "']/ancestor::tr/td/a[contains(text () , '" + email + "')]").isDisplayed()) {
				fc.utobj().throwsException("was not able to verify email of the lead");
			}*/

			WebElement tooltip = fc.utobj().getElementByXpath(driver, ".//img[contains(@src, 'red.png')]");
			fc.utobj().printTestStep("Lead's Email Subscription image");
			boolean isleadsubscription = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//img[contains(@src, 'red.png')]");
			if (isleadsubscription == false) {
				fc.utobj().throwsException("was not able to verify Lead's Email Subscription image.");
			}

			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//a[contains(text () ,'" + firstName + " " + lastName + "')]"));

			fc.utobj().printTestStep("Verify Activity History");
			boolean isemailPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//*[contains(text () ,'Email has been changed')]");
			if (isemailPresent == false) {
				fc.utobj().throwsException("was not able to verify Text Email has been changed in Activity History.");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	public void VerifyPrintPreview(WebDriver driver, String textTobeVerified) throws Exception {
		Set<String> windows = new HashSet<String>();
		windows = driver.getWindowHandles();

		boolean isTextDisplayed = false;

		String currentWindow = driver.getWindowHandle();
		for (String window : windows) {
			if (!window.equals(currentWindow)) {

				driver.switchTo().window(window);
				if (driver.getCurrentUrl().contains("bPrint")) {
					isTextDisplayed = fc.utobj().assertPageSource(driver, textTobeVerified);
				}
			}
		}
		driver.close();

		if (isTextDisplayed) {
			Reporter.log(textTobeVerified + "  is displayed on the print preview page. !!! ");
		} else {
			fc.utobj().throwsException(textTobeVerified + "  is not displayed on the print preview page. !!! ");
		}

		driver.switchTo().window(currentWindow);
	}

	public void selectActionMenuItemsWithTagAMoreActions(WebDriver driver, String menuName) throws Exception {

		WebElement moreActionsLink = driver
				.findElement(By.xpath(".//*[@id='td1' or @id='leadsForm']/a[.='More Actions']"));
		fc.utobj().moveToElement(driver, moreActionsLink);
		// clickElement(driver,moreActionsLink);
		fc.utobj().sleep();
		List<WebElement> menu = driver
				.findElements(By.xpath(".//*[@id='actionListButtons1']/table/tbody/tr[2]/td[2]/table/tbody//td"));
		fc.utobj().selectMoreActionMenu(driver, moreActionsLink, menu, menuName);
	}

	public void addContact(WebDriver driver, Map<String, String> dataSet, String firstName, String lastName,
			String company, String assignTo, String city, String address, String suffix, String jobTitle,
			String comment, String userName, String regionName, String regionalUser, String franchiseId,
			String franchiseUser, String divisionName, Map<String, String> config) throws Exception {

		String contactType = "Contacts";
		String title = "Mr.";
		String primaryCMethod = "Mobile";
		//String emailId = "crmautomation@staffex.com";
		String emailId=fc.utobj().generateTestData("crmautomation")+"@staffex.com";
		String testCaseId = "TC_Add_Contact_Generic";

		if (fc.utobj().validate(testCaseId)) {
			try {
				CRMContactsPage pobj = new CRMContactsPage(driver);
				fc.crm().crm_common().CRMContactsLnk(driver);
				fc.utobj().clickElement(driver, pobj.addNew);
				fc.utobj().clickElement(driver, pobj.contactLnk);
				fc.utobj().selectDropDown(driver, pobj.contactType, contactType);
				fc.utobj().selectDropDown(driver, pobj.title, title);
				fc.utobj().sendKeys(driver, pobj.contactFirstName, firstName);
				fc.utobj().sendKeys(driver, pobj.contactLastName, lastName);
				fc.utobj().sendKeys(driver, pobj.suffix, "suffixTest");
				fc.utobj().sendKeys(driver, pobj.position, "TestJobTitle");
				fc.utobj().sendKeys(driver, pobj.address, "TestAddress");
				fc.utobj().sendKeys(driver, pobj.city, "TestCity");
				fc.utobj().selectDropDown(driver, pobj.country, "USA");
				fc.utobj().selectDropDown(driver, pobj.state, "Alabama");
				// fc.utobj().selectDropDown(driver,
				// pobj.primaryContactMethodSelect, primaryCMethod);
				fc.utobj().sendKeys(driver, pobj.bestTimeToContact, "12:00 PM EST");
				fc.utobj().sendKeys(driver, pobj.zipcode, "12345");
				fc.utobj().sendKeys(driver, pobj.phoneNumbers, "1236547896");
				fc.utobj().sendKeys(driver, pobj.extn, "12");
				fc.utobj().sendKeys(driver, pobj.faxNumbers, "1263985487");
				fc.utobj().sendKeys(driver, pobj.mobileNumbers, "8896724441");
				fc.utobj().sendKeys(driver, pobj.emailIds, emailId);
				fc.utobj().sendKeys(driver, pobj.alternateEmail, "test@gmail.com");
				fc.utobj().sendKeys(driver, pobj.birthdate, "11/24/1980");
				fc.utobj().sendKeys(driver, pobj.anniversarydate, "11/24/1999");

				if (assignTo.equalsIgnoreCase("Corporate")) {

					if (!fc.utobj().isSelected(driver, pobj.assignToCorporate)) {
						fc.utobj().clickElement(driver, pobj.assignToCorporate);
					}

					fc.utobj().selectDropDown(driver, pobj.selectCorporateUserContact, userName);

				} else if (assignTo.equalsIgnoreCase("Regional")) {

					if (!fc.utobj().isSelected(driver, pobj.assignToRegional)) {
						fc.utobj().clickElement(driver, pobj.assignToRegional);
					}

					fc.utobj().selectDropDown(driver, pobj.selectAreaRegion, regionName);
					fc.utobj().selectDropDown(driver, pobj.selectRegionalUser, userName);

				} else if (assignTo.equalsIgnoreCase("Franchise")) {

					try {
						if (!fc.utobj().isSelected(driver, pobj.assignToFranchise)) {
							fc.utobj().clickElement(driver, pobj.assignToFranchise);
						}
						fc.utobj().selectDropDown(driver, pobj.selectFranchiseId, franchiseId);
					} catch (Exception e) {
					}

					fc.utobj().selectDropDown(driver, pobj.selectFranchiseUser, userName);
					// fc.utobj().selectDropDownByPartialText(driver,
					// pobj.selectFranchiseUser, userName);
				} else if (assignTo.equalsIgnoreCase("Division")) {
					if (!fc.utobj().isSelected(driver, pobj.assignToDivision)) {
						fc.utobj().clickElement(driver, pobj.assignToDivision);
					}
					fc.utobj().selectDropDown(driver, pobj.selectDivisionId, divisionName);
					fc.utobj().selectDropDown(driver, pobj.selectDivisionUser, userName);
				}

				fc.utobj().sendKeys(driver, pobj.comments, "Test Comment");
				fc.utobj().clickElement(driver, pobj.saveBtn);

				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException("was not able to add Contact :: CRM > Contacts");

		}

	}

	public void addContact(WebDriver driver, Map<String, String> dataSet, String firstName, String lastName,
			String company, String assignTo, String city, String address, String suffix, String jobTitle,
			String comment, String userName, String regionName, String regionalUser, String franchiseId,
			String franchiseUser, Map<String, String> config) throws Exception {

		String contactType = "Contacts";
		String title = "Mr.";
		String primaryCMethod = "Mobile";
		String emailId = "crmautomation@staffex.com";
		//String emailId=fc.utobj().generateTestData("crmautomation")+"@staffex.com";
		String testCaseId = "TC_Add_Contact_Generic";

		if (fc.utobj().validate(testCaseId)) {
			try {
				CRMContactsPage pobj = new CRMContactsPage(driver);
				fc.crm().crm_common().CRMContactsLnk(driver);
				fc.utobj().clickElement(driver, pobj.addNew);
				fc.utobj().clickElement(driver, pobj.contactLnk);
				fc.utobj().selectDropDown(driver, pobj.contactType, contactType);
				fc.utobj().selectDropDown(driver, pobj.title, title);
				fc.utobj().sendKeys(driver, pobj.contactFirstName, firstName);
				fc.utobj().sendKeys(driver, pobj.contactLastName, lastName);
				fc.utobj().sendKeys(driver, pobj.suffix, "suffixTest");
				fc.utobj().sendKeys(driver, pobj.position, "TestJobTitle");
				fc.utobj().sendKeys(driver, pobj.address, "TestAddress");
				fc.utobj().sendKeys(driver, pobj.city, "TestCity");
				fc.utobj().selectDropDown(driver, pobj.country, "USA");
				fc.utobj().selectDropDown(driver, pobj.state, "Alabama");
				// fc.utobj().selectDropDown(driver,
				// pobj.primaryContactMethodSelect, primaryCMethod);
				fc.utobj().sendKeys(driver, pobj.bestTimeToContact, "12:00 PM EST");
				fc.utobj().sendKeys(driver, pobj.zipcode, "12345");
				fc.utobj().sendKeys(driver, pobj.phoneNumbers, "1236547896");
				fc.utobj().sendKeys(driver, pobj.extn, "12");
				fc.utobj().sendKeys(driver, pobj.faxNumbers, "1263985487");
				fc.utobj().sendKeys(driver, pobj.mobileNumbers, "8896724441");
				fc.utobj().sendKeys(driver, pobj.emailIds, emailId);
				fc.utobj().sendKeys(driver, pobj.alternateEmail, "test@gmail.com");
				fc.utobj().sendKeys(driver, pobj.birthdate, "11/24/1980");
				fc.utobj().sendKeys(driver, pobj.anniversarydate, "11/24/1999");

				if (assignTo.equalsIgnoreCase("Corporate")) {

					if (!fc.utobj().isSelected(driver, pobj.assignToCorporate)) {
						fc.utobj().clickElement(driver, pobj.assignToCorporate);
					}

					fc.utobj().selectDropDown(driver, pobj.selectCorporateUserContact, userName);

				} else if (assignTo.equalsIgnoreCase("Regional")) {

					if (!fc.utobj().isSelected(driver, pobj.assignToRegional)) {
						fc.utobj().clickElement(driver, pobj.assignToRegional);
					}

					fc.utobj().selectDropDown(driver, pobj.selectAreaRegion, regionName);
					fc.utobj().selectDropDown(driver, pobj.selectRegionalUser, userName);

				} else if (assignTo.equalsIgnoreCase("Franchise")) {

					try {
						if (!fc.utobj().isSelected(driver, pobj.assignToFranchise)) {
							fc.utobj().clickElement(driver, pobj.assignToFranchise);
						}
						fc.utobj().selectDropDown(driver, pobj.selectFranchiseId, franchiseId);
					} catch (Exception e) {
					}

					fc.utobj().selectDropDown(driver, pobj.selectFranchiseUser, userName);
					// fc.utobj().selectDropDownByPartialText(driver,
					// pobj.selectFranchiseUser, userName);
				}
				fc.utobj().sendKeys(driver, pobj.comments, "Test Comment");
				fc.utobj().clickElement(driver, pobj.saveBtn);

				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException("was not able to add Contact :: CRM > Contacts");

		}

	}

	public void addContactAnother(WebDriver driver, Map<String, String> dataSet, String firstName, String lastName,
			String company, String assignTo, String city, String address, String suffix, String jobTitle,
			String comment, String userName, String regionName, String regionalUser, String franchiseId,
			String franchiseUser, Map<String, String> config) throws Exception {

		String contactType = "Contacts";
		String title = "Mr.";
		String primaryCMethod = "Mobile";
		//String emailId = "crmautomation@staffex.com";
		String emailId=fc.utobj().generateTestData("crmautomation")+"@staffex.com";
		String testCaseId = "TC_Add_Contact_Generic";

		if (fc.utobj().validate(testCaseId)) {
			try {
				CRMContactsPage pobj = new CRMContactsPage(driver);
				fc.crm().crm_common().CRMContactsLnk(driver);
				fc.utobj().clickElement(driver, pobj.addNew);
				fc.utobj().clickElement(driver, pobj.contactLnk);
				fc.utobj().selectDropDown(driver, pobj.contactType, contactType);
				fc.utobj().selectDropDown(driver, pobj.title, title);
				fc.utobj().sendKeys(driver, pobj.contactFirstName, firstName);
				fc.utobj().sendKeys(driver, pobj.contactLastName, lastName);
				fc.utobj().sendKeys(driver, pobj.suffix, "suffixTest");
				fc.utobj().sendKeys(driver, pobj.position, "TestJobTitle");
				fc.utobj().sendKeys(driver, pobj.address, "TestAddress");
				fc.utobj().sendKeys(driver, pobj.city, "TestCity");
				fc.utobj().selectDropDown(driver, pobj.country, "USA");
				fc.utobj().selectDropDown(driver, pobj.state, "Alabama");
				// fc.utobj().selectDropDown(driver,
				// pobj.primaryContactMethodSelect, primaryCMethod);
				fc.utobj().sendKeys(driver, pobj.bestTimeToContact, "12:00 PM EST");
				fc.utobj().sendKeys(driver, pobj.zipcode, "12345");
				fc.utobj().sendKeys(driver, pobj.phoneNumbers, "1236547896");
				fc.utobj().sendKeys(driver, pobj.extn, "12");
				fc.utobj().sendKeys(driver, pobj.faxNumbers, "1263985487");
				fc.utobj().sendKeys(driver, pobj.mobileNumbers, "8896724441");
				fc.utobj().sendKeys(driver, pobj.emailIds, emailId);
				fc.utobj().sendKeys(driver, pobj.alternateEmail, "test@gmail.com");
				fc.utobj().sendKeys(driver, pobj.birthdate, "11/24/1980");
				fc.utobj().sendKeys(driver, pobj.anniversarydate, "11/24/1999");

				if (assignTo.equalsIgnoreCase("Corporate")) {

					if (!fc.utobj().isSelected(driver, pobj.assignToCorporate)) {
						fc.utobj().clickElement(driver, pobj.assignToCorporate);
					}

					fc.utobj().selectDropDown(driver, pobj.selectCorporateUserContact, userName);

				} else if (assignTo.equalsIgnoreCase("Regional")) {

					if (!fc.utobj().isSelected(driver, pobj.assignToRegional)) {
						fc.utobj().clickElement(driver, pobj.assignToRegional);
					}

					fc.utobj().selectDropDown(driver, pobj.selectAreaRegion, regionName);
					fc.utobj().selectDropDown(driver, pobj.selectRegionalUser, userName);

				} else if (assignTo.equalsIgnoreCase("Franchise")) {

					try {
						if (!fc.utobj().isSelected(driver, pobj.assignToFranchise)) {
							fc.utobj().clickElement(driver, pobj.assignToFranchise);
						}
						fc.utobj().selectDropDown(driver, pobj.selectFranchiseId, franchiseId);
					} catch (Exception e) {
					}

					fc.utobj().selectDropDown(driver, pobj.selectFranchiseUser, userName);
					// fc.utobj().selectDropDownByPartialText(driver,
					// pobj.selectFranchiseUser, userName);
				}
				fc.utobj().sendKeys(driver, pobj.comments, "Test Comment");
				fc.utobj().clickElement(driver, pobj.saveAddAnotherLead);

				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException("was not able to add Contact :: CRM > Contacts");

		}

	}

	public void addContactReset(WebDriver driver, Map<String, String> dataSet, String firstName, String lastName,
			String company, String assignTo, String city, String address, String suffix, String jobTitle,
			String comment, String userName, String regionName, String regionalUser, String franchiseId,
			String franchiseUser, Map<String, String> config) throws Exception {

		String contactType = "Contacts";
		String title = "Mr.";
		String primaryCMethod = "Mobile";
		//String emailId = "crmautomation@staffex.com";
		String emailId=fc.utobj().generateTestData("crmautomation")+"@staffex.com";
		String testCaseId = "TC_Add_Contact_Generic";

		if (fc.utobj().validate(testCaseId)) {
			try {
				CRMContactsPage pobj = new CRMContactsPage(driver);
				fc.crm().crm_common().CRMContactsLnk(driver);
				fc.utobj().clickElement(driver, pobj.addNew);
				fc.utobj().clickElement(driver, pobj.contactLnk);
				fc.utobj().selectDropDown(driver, pobj.contactType, contactType);
				fc.utobj().selectDropDown(driver, pobj.title, title);
				fc.utobj().sendKeys(driver, pobj.contactFirstName, firstName);
				fc.utobj().sendKeys(driver, pobj.contactLastName, lastName);
				fc.utobj().sendKeys(driver, pobj.suffix, "suffixTest");
				fc.utobj().sendKeys(driver, pobj.position, "TestJobTitle");
				fc.utobj().sendKeys(driver, pobj.address, "TestAddress");
				fc.utobj().sendKeys(driver, pobj.city, "TestCity");
				fc.utobj().selectDropDown(driver, pobj.country, "USA");
				fc.utobj().selectDropDown(driver, pobj.state, "Alabama");
				fc.utobj().selectDropDown(driver, pobj.primaryContactMethodSelect, "Email");

				try {
					fc.utobj().printTestStep("Primary Contact Method drop down changing to mandatory field.");

					boolean ismandaorySign = fc.utobj().verifyElementOnVisible_ByXpath(driver,
							"//*[@class='urgent_fields']/ancestor::td[contains (text(),'Email')]");
					if (ismandaorySign == false) {
						fc.utobj().throwsException(
								"was not able to verify Primary Contact Method drop down changing to mandatory field.");
					}

				} catch (Exception e) {

				}

				fc.utobj().sendKeys(driver, pobj.bestTimeToContact, "12:00 PM EST");
				fc.utobj().sendKeys(driver, pobj.zipcode, "12345");
				fc.utobj().sendKeys(driver, pobj.phoneNumbers, "1236547896");
				fc.utobj().sendKeys(driver, pobj.extn, "12");
				fc.utobj().sendKeys(driver, pobj.faxNumbers, "1263985487");
				fc.utobj().sendKeys(driver, pobj.mobileNumbers, "8896724441");
				fc.utobj().sendKeys(driver, pobj.emailIds, emailId);
				fc.utobj().sendKeys(driver, pobj.alternateEmail, "test@gmail.com");
				fc.utobj().sendKeys(driver, pobj.birthdate, "11/24/1980");
				fc.utobj().sendKeys(driver, pobj.anniversarydate, "11/24/1999");

				if (assignTo.equalsIgnoreCase("Corporate")) {

					if (!fc.utobj().isSelected(driver, pobj.assignToCorporate)) {
						fc.utobj().clickElement(driver, pobj.assignToCorporate);
					}

					fc.utobj().selectDropDown(driver, pobj.selectCorporateUserContact, userName);

				} else if (assignTo.equalsIgnoreCase("Regional")) {

					if (!fc.utobj().isSelected(driver, pobj.assignToRegional)) {
						fc.utobj().clickElement(driver, pobj.assignToRegional);
					}

					fc.utobj().selectDropDown(driver, pobj.selectAreaRegion, regionName);
					fc.utobj().selectDropDown(driver, pobj.selectRegionalUser, userName);

				} else if (assignTo.equalsIgnoreCase("Franchise")) {

					try {
						if (!fc.utobj().isSelected(driver, pobj.assignToFranchise)) {
							fc.utobj().clickElement(driver, pobj.assignToFranchise);
						}
						fc.utobj().selectDropDown(driver, pobj.selectFranchiseId, franchiseId);
					} catch (Exception e) {
					}

					fc.utobj().selectDropDown(driver, pobj.selectFranchiseUser, userName);
					// fc.utobj().selectDropDownByPartialText(driver,
					// pobj.selectFranchiseUser, userName);
				}
				fc.utobj().sendKeys(driver, pobj.comments, "Test Comment");

				try {
					fc.utobj().moveToElement(driver,
							fc.utobj().getElementByXpath(driver, ".//*[@id='resetButton' and @value='Reset']"));
					fc.utobj().clickElement(driver,
							fc.utobj().getElementByXpath(driver, ".//*[@id='resetButton' and @value='Reset']"));
					String leadFirstNameText = fc.utobj().getElementByXpath(driver, ".//*[@id='contactFirstName']")
							.getText();
					System.out.println("contactFirstName===" + leadFirstNameText);

					if (leadFirstNameText != null && !"".equals(leadFirstNameText)
							&& !"null".equals(leadFirstNameText)) {
						fc.utobj().throwsException("was not able to verify Reset button");
					}

					fc.utobj().selectDropDown(driver, pobj.contactType, contactType);
					fc.utobj().selectDropDown(driver, pobj.title, title);
					fc.utobj().sendKeys(driver, pobj.contactFirstName, firstName);
					fc.utobj().sendKeys(driver, pobj.contactLastName, lastName);
					fc.utobj().sendKeys(driver, pobj.suffix, "suffixTest");
					fc.utobj().sendKeys(driver, pobj.position, "TestJobTitle");
					fc.utobj().sendKeys(driver, pobj.address, "TestAddress");
					fc.utobj().sendKeys(driver, pobj.city, "TestCity");
					fc.utobj().selectDropDown(driver, pobj.country, "USA");
					fc.utobj().selectDropDown(driver, pobj.state, "Alabama");
					// fc.utobj().selectDropDown(driver,
					// pobj.primaryContactMethodSelect, primaryCMethod);
					fc.utobj().sendKeys(driver, pobj.bestTimeToContact, "12:00 PM EST");
					fc.utobj().sendKeys(driver, pobj.zipcode, "12345");
					fc.utobj().sendKeys(driver, pobj.phoneNumbers, "1236547896");
					fc.utobj().sendKeys(driver, pobj.extn, "12");
					fc.utobj().sendKeys(driver, pobj.faxNumbers, "1263985487");
					fc.utobj().sendKeys(driver, pobj.mobileNumbers, "8896724441");
					fc.utobj().sendKeys(driver, pobj.emailIds, emailId);
					fc.utobj().sendKeys(driver, pobj.alternateEmail, "test@gmail.com");
					fc.utobj().sendKeys(driver, pobj.birthdate, "11/24/1980");
					fc.utobj().sendKeys(driver, pobj.anniversarydate, "11/24/1999");

					if (assignTo.equalsIgnoreCase("Corporate")) {

						if (!fc.utobj().isSelected(driver, pobj.assignToCorporate)) {
							fc.utobj().clickElement(driver, pobj.assignToCorporate);
						}

						fc.utobj().selectDropDown(driver, pobj.selectCorporateUserContact, userName);

					} else if (assignTo.equalsIgnoreCase("Regional")) {

						if (!fc.utobj().isSelected(driver, pobj.assignToRegional)) {
							fc.utobj().clickElement(driver, pobj.assignToRegional);
						}

						fc.utobj().selectDropDown(driver, pobj.selectAreaRegion, regionName);
						fc.utobj().selectDropDown(driver, pobj.selectRegionalUser, userName);

					} else if (assignTo.equalsIgnoreCase("Franchise")) {

						try {
							if (!fc.utobj().isSelected(driver, pobj.assignToFranchise)) {
								fc.utobj().clickElement(driver, pobj.assignToFranchise);
							}
							fc.utobj().selectDropDown(driver, pobj.selectFranchiseId, franchiseId);
						} catch (Exception e) {
						}

						fc.utobj().selectDropDown(driver, pobj.selectFranchiseUser, userName);
					}
					fc.utobj().sendKeys(driver, pobj.comments, "Test Comment");

				} catch (Exception e) {
					fc.utobj().throwsException("was not able to verify Reset button");
				}

				fc.utobj().clickElement(driver, pobj.saveAddAnotherLead);

				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException("was not able to add Contact :: CRM > Contacts");

		}

	}

	public void searchLeadByOwner(WebDriver driver, String userName, String leadTypeView) throws Exception {

		CRMContactsPage pobj = new CRMContactsPage(driver);
		fc.utobj().clickElement(driver, pobj.showFilter);
		fc.utobj().setToDefault(driver, pobj.contactOwnerSelect);
		try {
			WebElement divsion = fc.utobj().getElementByXpath(driver, ".//*[@id='ms-parentdivisionID']");
			fc.utobj().setToDefault(driver, divsion);
		} catch (Exception e) {
		}
		fc.utobj()
				.selectValFromMultiSelect(driver, pobj.contactOwnerSelect, fc.utobj()
						.getElement(driver, pobj.contactOwnerSelect).findElement(By.xpath("./div/div/input")),
						userName);
		fc.utobj().setToDefault(driver, pobj.franchiseIdSelect);
		fc.utobj().setToDefault(driver, pobj.statusSelect);
		fc.utobj().selectDropDown(driver, pobj.addDateSelect, "All");
		fc.utobj().setToDefault(driver, pobj.emailStatusSelect);
		// fc.utobj().selectDropDown(driver, pobj.viewLeadsSelect,
		// leadTypeView);
		fc.utobj().clickElement(driver, pobj.searchBtn);
		
		fc.utobj().clickElement(driver, pobj.hideFilter);
	}

	public void searchLeadByOwnerDuplcate(WebDriver driver, String userName, String leadTypeView) throws Exception {

		CRMContactsPage pobj = new CRMContactsPage(driver);
		fc.utobj().clickElement(driver, pobj.showFilter);
		fc.utobj().setToDefault(driver, pobj.contactOwnerSelect);
		try {
			WebElement divsion = fc.utobj().getElementByXpath(driver, ".//*[@id='ms-parentdivisionID']");
			fc.utobj().setToDefault(driver, divsion);
		} catch (Exception e) {
		}
		fc.utobj()
				.selectValFromMultiSelect(driver, pobj.contactOwnerSelect, fc.utobj()
						.getElement(driver, pobj.contactOwnerSelect).findElement(By.xpath("./div/div/input")),
						userName);
		fc.utobj().setToDefault(driver, pobj.franchiseIdSelect);
		fc.utobj().setToDefault(driver, pobj.statusSelect);
		fc.utobj().selectDropDown(driver, pobj.addDateSelect, "All");
		fc.utobj().setToDefault(driver, pobj.emailStatusSelect);
		// fc.utobj().selectDropDown(driver, pobj.viewLeadsSelect,
		// leadTypeView);

		List<String> listInfo = new ArrayList<String>();
		listInfo.add("First Name");
		listInfo.add("Last Name");
		listInfo.add("Email");
		// fc.utobj().selectMultipleValFromMultiSelect(driver,
		// fc.utobj().getElementByXpath(driver,".//*[@id='ms-parentleadDuplicateCriteria']")),
		// listInfo);

		fc.utobj().clickElement(driver, pobj.searchBtn);
		;
		fc.utobj().clickElement(driver, pobj.hideFilter);
	}

	public void searchLeadByOwnerDiffrent(WebDriver driver, String userName, String leadTypeView, List<String> listInfo)
			throws Exception {

		CRMContactsPage pobj = new CRMContactsPage(driver);
		fc.utobj().clickElement(driver, pobj.showFilter);
		fc.utobj().setToDefault(driver, pobj.contactOwnerSelect);
		try {
			WebElement divsion = fc.utobj().getElementByXpath(driver, ".//*[@id='ms-parentdivisionID']");
			fc.utobj().setToDefault(driver, divsion);
		} catch (Exception e) {
		}
		
		
		fc.utobj().printTestStep("click on multiselect_dropdown option");
		fc.utobj().clickElement(driver, "//div[@id='ms-parentcontactOwner']/button/span[@class='placeholder']");
		fc.utobj().clickElement(driver,".//div[@id='ms-parentcontactOwner']/div[@class='ms-drop bottom']/ul/li/label/input[@id='selectAll']");
		
		/*fc.utobj()
		.selectValFromMultiSelect(driver, pobj.contactOwnerSelect, fc.utobj()
				.getElement(driver, pobj.contactOwnerSelect).findElement(By.xpath("./div/div/input")),
				userName);*/
		/*fc.utobj().selectMultipleValFromMultiSelect(driver,
				fc.utobj().getElementByXpath(driver, ".//*[@id='ms-parentcontactOwner']"), listInfo);*/
		/*fc.utobj().selectValFromMultiSelect(driver,
		pobj.leadOwnerSelect,fc.utobj().getElement(driver,
		pobj.leadOwnerSelect).findElement(By.xpath("./div/div/input")),
		userName);*/
		fc.utobj().setToDefault(driver, pobj.franchiseIdSelect);
		fc.utobj().setToDefault(driver, pobj.statusSelect);
		fc.utobj().selectDropDown(driver, pobj.addDateSelect, "All");
		fc.utobj().setToDefault(driver, pobj.emailStatusSelect);
		// fc.utobj().selectDropDown(driver, pobj.viewLeadsSelect,
		// leadTypeView);
		fc.utobj().clickElement(driver, pobj.searchBtn);
		
		fc.utobj().clickElement(driver, pobj.hideFilter);
	}

	// system search
	public void systemExactSearch(WebDriver driver, String leadName) throws Exception {
		// search By System Search Button
		CRMLeadsPage pobj = new CRMLeadsPage(driver);
		fc.utobj().sendKeys(driver, pobj.searchContact, leadName);
		// fc.utobj().clickElement(driver, pobj.exactSearch);
		fc.utobj().clickElement(driver, pobj.systemSearchButton);
	}

	public Map<String, String> fillFormDataWithDataset(WebDriver driver, Map<String, String> config, String testCaseId,
			Map<String, String> dataSet) throws Exception {
		Reporter.log("Filling TestCase Data = " + testCaseId);
		String id = "id";

		List<String> listItems = new ArrayList<String>();
		Map<String, String> printInfo = new HashMap<String, String>();

		List<WebElement> mandateBox = null;
		mandateBox = driver.findElements(By.xpath(
				".//td[@qat_tableid='addData']//*[@type='text' or @class='fTextBoxDate' or @class='fTextBox' or @class='multiList' or @type='radio' or @type='checkbox' or @type='checkBox' or @id='ms-parentsellerCombo' or @class='ms-choice' or @class='form-control']"));

		Map<String, String> alreadyitrateElement = new HashMap<>();
		Map<String, String> alreadyradio = new HashMap<>();
		Map<String, String> alreadycheckBox = new HashMap<>();

		try {
			for (WebElement webElement : mandateBox) {

				if (webElement.getAttribute("" + id) != null && !"".equals(webElement.getAttribute("" + id))
						&& !"null".equals(webElement.getAttribute("" + id))
						&& !":".equals(webElement.getAttribute("" + id))
						&& !"dateOfOpen".equals(webElement.getAttribute("" + id))
						&& !"fimCbOtherComplaintType".equals(webElement.getAttribute("" + id))) {
					/*
					 * try{
					 * System.out.println("Type==="+webElement.getAttribute(
					 * "type")+"====values==="+webElement.getAttribute("value")
					 * +"======"+dataSet.get(""+webElement.getAttribute(""+id))
					 * +"====="+""+webElement.getAttribute(""+id));
					 * }catch(Exception e) {
					 * 
					 * }
					 */

					if (alreadyitrateElement.containsKey(webElement.getAttribute("" + id))) {

					} else {
						if (dataSet.containsKey(webElement.getAttribute("" + id))) {
							WebElement elementmovepostion = null;
							if ("name".equals(id)) {
								elementmovepostion = driver.findElement(By.name("" + webElement.getAttribute("" + id)));

							} else {
								elementmovepostion = fc.utobj().getElementByID(driver,
										"" + webElement.getAttribute("" + id));

							}
							fc.utobj().moveToElement(driver, elementmovepostion);
							if (webElement.getAttribute("type").indexOf("select-one") != -1) {
								if (webElement.getAttribute("" + id) != null
										&& !"cmSource2ID".equals(webElement.getAttribute("" + id))
										&& !"cmSource3ID".equals(webElement.getAttribute("" + id)))

								{
									try {

										Select singledrop = null;
										List<WebElement> singleDropDown = null;
										try {
											singledrop = new Select(fc.utobj().getElementByID(driver,
													"" + webElement.getAttribute("" + id)));
											singleDropDown = driver
													.findElements(By.id("" + webElement.getAttribute("" + id)));
										} catch (Exception e) {
											singledrop = new Select(
													driver.findElement(By.name("" + webElement.getAttribute("" + id))));
											singleDropDown = driver
													.findElements(By.name("" + webElement.getAttribute("" + id)));
										}
										if (webElement.getAttribute("value") == null
												|| "".equals(webElement.getAttribute("value"))
												|| "null".equals(webElement.getAttribute("value"))
												|| "-1".equals(webElement.getAttribute("value"))) {

											int size = singleDropDown.size();
											if (dataSet.get("" + webElement.getAttribute("" + id)) != null
													&& !"".equals(dataSet.get("" + webElement.getAttribute("" + id)))) {
												if (size >= 1) {
													try {
														WebElement dropDownField = driver
																.findElement(By.id("" + webElement.getAttribute("id")));
														// fc.utobj().moveToElement(driver,
														// dropDownField);
														fc.utobj().selectDropDownByVisibleText(driver, dropDownField,
																dataSet.get("" + webElement.getAttribute("" + id)));

														if (!"birthMonth".equals(webElement.getAttribute("" + id))
																&& !"birthDate".equals(webElement.getAttribute("" + id))
																&& !"spouseBirthMonth"
																		.equals(webElement.getAttribute("" + id))
																&& !"spouseBirthDate"
																		.equals(webElement.getAttribute("" + id))) {
															listItems.add(
																	dataSet.get("" + webElement.getAttribute("" + id)));
														}
														printInfo.put("" + webElement.getAttribute("" + id),
																dataSet.get("" + webElement.getAttribute("" + id)));

													} catch (Exception eradio) {
														// Reporter.log("Problem
														// in selecting drop :
														// "+""+webElement.getAttribute("id")
														// +" Values getting
														// from Excel not match
														// in desired
														// fomrat."+""+dataSet.get(""+webElement.getAttribute("id")));
													}
												}
											}
										}

									} catch (Exception e3) {

									}
								}

							} else if (webElement.getAttribute("type").indexOf("select-multiple") != -1) {
								if (webElement.getAttribute("value") == null
										|| "".equals(webElement.getAttribute("value"))
										|| "null".equals(webElement.getAttribute("value"))
										|| "-1".equals(webElement.getAttribute("value"))) {
									try {
										WebElement element = driver
												.findElement(By.id("ms-parent" + webElement.getAttribute("" + id)));
										fc.utobj().clickElement(driver, element);
										fc.utobj().clickElement(driver,
												fc.utobj().getElementByXpath(driver, ".//*[@id='selectAll']"));
										fc.utobj().clickElement(driver, element);
									} catch (Exception emultipal) {
										// Reporter.log("Problem in multiple
										// value drop-down :
										// "+""+webElement.getAttribute("id"));
									}
								}
							} else if (webElement.getAttribute("type").indexOf("textarea") != -1) {
								if (webElement.getAttribute("value") == null
										|| "".equals(webElement.getAttribute("value"))
										|| "null".equals(webElement.getAttribute("value"))) {
									try {
										driver.findElement(By.name("" + webElement.getAttribute("" + id)))
												.sendKeys(dataSet.get("" + webElement.getAttribute("" + id)));
										listItems.add(dataSet.get("" + webElement.getAttribute("" + id)));
										printInfo.put("" + webElement.getAttribute("id"),
												dataSet.get("" + webElement.getAttribute("" + id)));
									} catch (Exception eText) {
										// Reporter.log("Problem in entering
										// TextArea in :
										// "+""+webElement.getAttribute("id"));
									}
								}
							} else if (webElement.getAttribute("type").indexOf("radio") != -1) {
								try {
									String sValue = "";
									List<WebElement> rdBtn_Sex = driver
											.findElements(By.id("" + webElement.getAttribute("" + id))); //
									if (alreadyradio.containsKey(webElement.getAttribute("" + id))) {

									} else {

										int size = rdBtn_Sex.size();
										if (size > 1) {
											sValue = dataSet.get("" + webElement.getAttribute("" + id));
										}
										try {
											List<WebElement> rdBtn_Field = null;
											WebElement element = null;
											try {
												rdBtn_Field = driver.findElements(By.xpath(
														".//*[@name='" + webElement.getAttribute("" + id) + "']")); // work
												element = fc.utobj().getElementByXpath(driver,
														".//*[@name='" + webElement.getAttribute("" + id) + "']");
											} catch (Exception eTxt) {
												rdBtn_Field = driver.findElements(
														By.xpath(".//*[@id='" + webElement.getAttribute("id") + "']")); // work
												element = driver.findElement(
														By.xpath(".//*[@id='" + webElement.getAttribute("id") + "']"));

											}

											fc.utobj().moveToElement(driver, element);
											fc.utobj().clickRadioButton(driver, rdBtn_Field, sValue); // work
											listItems.add(dataSet.get("" + webElement.getAttribute("" + id)));
											printInfo.put("" + webElement.getAttribute("id"),
													dataSet.get("" + webElement.getAttribute("" + id)));
											alreadyradio.put(webElement.getAttribute("" + id),
													webElement.getAttribute("" + id));
										} catch (Exception eText) {
											// Reporter.log("Problem in
											// selecting radio value in :
											// "+""+webElement.getAttribute("id"));
										}
									}
								} catch (Exception exp) {
									Reporter.log(exp.toString());
									exp.printStackTrace();
								}

							} else if (webElement.getAttribute("type").toLowerCase().indexOf("checkbox") != -1) {
								List<WebElement> oCheckBox = driver
										.findElements(By.name("" + webElement.getAttribute("" + id)));
								String sValue = "";
								if (alreadycheckBox.containsKey(webElement.getAttribute("" + id))) {

								} else {

									int size = oCheckBox.size();
									for (int i = 0; i < size; i++) {
										sValue = oCheckBox.get(i).getAttribute("value");
										// Thread.sleep(2000);
										if (i == (size - 1)) {
											// break;
										}

									}
									try {
										List<WebElement> rdBtn_Field = driver.findElements(By.xpath(
												"//*[contains(@id, '" + webElement.getAttribute("" + id) + "')]")); // work
										WebElement element = fc.utobj().getElementByXpath(driver,
												"//*[contains(@id, '" + webElement.getAttribute("" + id) + "')]");
										fc.utobj().moveToElement(driver, element);
										fc.utobj().clickRadioButton(driver, rdBtn_Field, sValue); // work
										alreadycheckBox.put(webElement.getAttribute("" + id),
												webElement.getAttribute("" + id));
									} catch (Exception eText) {

									}
								}
							} else if (webElement.getAttribute("type").indexOf("file") != -1) {
								String fileName = fc.utobj().getFilePathFromTestData("pictureFile");
								try {
									fc.utobj().sendKeys(driver,
											fc.utobj().getElementByID(driver, "" + webElement.getAttribute("" + id)),
											fileName);
									listItems.add(fileName);
									printInfo.put("" + webElement.getAttribute("" + id), fileName);
								} catch (Exception eFile) {
									// Reporter.log("Problem in uploading file
									// type value in :
									// "+""+webElement.getAttribute("id"));
								}
							} else if (webElement.getAttribute("type").indexOf("text") != -1) {
								if (webElement.getAttribute("value") == null
										|| "".equals(webElement.getAttribute("value"))
										|| "null".equals(webElement.getAttribute("value"))
										|| "0.00".equals(webElement.getAttribute("value"))) {
									if (webElement.getAttribute("class").indexOf("fTextBoxDate") != -1) {
										try {
											if (dataSet.get("" + webElement.getAttribute("" + id)) != null
													|| !"".equals(dataSet.get("" + webElement.getAttribute("" + id)))
													|| !"null".equals(
															dataSet.get("" + webElement.getAttribute("" + id)))) {
												fc.utobj().sendKeys(driver,
														driver.findElement(
																By.name("" + webElement.getAttribute("" + id))),
														dataSet.get("" + webElement.getAttribute("" + id))); // work
												listItems.add(dataSet.get("" + webElement.getAttribute("" + id)));
												printInfo.put("" + webElement.getAttribute("" + id),
														dataSet.get("" + webElement.getAttribute("" + id)));

											} else {

											}
										} catch (Exception eDate) {
											// Reporter.log("Problem in Entering
											// Date fields value in :
											// "+""+webElement.getAttribute("id"));
										}
									} else {
										try {
											if ("0.00".equals(webElement.getAttribute("value"))) {
												fc.utobj()
														.sendKeys(driver,
																driver.findElement(
																		By.name("" + webElement.getAttribute("" + id))),
																"");

												// System.out.println(webElement.getAttribute("value")
												// +"======"+dataSet.get(""+webElement.getAttribute(""+id))
												// +"====="+""+webElement.getAttribute(""+id));

											}
											if ("name".equals(id)) {
												fc.utobj().sendKeys(driver,
														driver.findElement(
																By.name("" + webElement.getAttribute("" + id))),
														dataSet.get("" + webElement.getAttribute("" + id)));
												if ("accountName".equals("" + webElement.getAttribute("" + id))
														|| "opportunityOwner"
																.equals("" + webElement.getAttribute("" + id))
														|| "oppContactID"
																.equals("" + webElement.getAttribute("" + id))) {
													;
													fc.utobj().clickElement(driver, driver.findElement(
															By.xpath(".//*[@id='customizedAjaxSearch']/div")));
													fc.utobj().sleep();
												}
											} else {
												fc.utobj().sendKeys(driver,
														driver.findElement(
																By.id("" + webElement.getAttribute("" + id))),
														dataSet.get("" + webElement.getAttribute("" + id)));
												if ("accountName".equals("" + webElement.getAttribute("" + id))
														|| "opportunityOwner"
																.equals("" + webElement.getAttribute("" + id))
														|| "oppContactID"
																.equals("" + webElement.getAttribute("" + id))) {
													;
													fc.utobj().clickElement(driver, driver.findElement(
															By.xpath(".//*[@id='customizedAjaxSearch']/div")));
													fc.utobj().sleep();
												}
											}

											if (!"ssn".equals("" + webElement.getAttribute("" + id))
													&& !"spouseSsn".equals("" + webElement.getAttribute("" + id))) {
												listItems.add(dataSet.get("" + webElement.getAttribute("" + id)));
											}
											printInfo.put("" + webElement.getAttribute("" + id),
													dataSet.get("" + webElement.getAttribute("" + id)));

										} catch (Exception eText) {
											// Reporter.log("Problem in Entering
											// Text fileds value in :
											// "+""+webElement.getAttribute("id"));
										}

									}
								}
							}
							alreadyitrateElement.put(webElement.getAttribute("" + id),
									webElement.getAttribute("" + id));
						}
					}
				}
			}

			
			if ("TC_CRM_QA_Contact_Details".equals(testCaseId)) {

				Select singledrop = null;
				List<WebElement> singleDropDown = null;
				try {
					singledrop = new Select(driver.findElement(By.name("contactOwnerID2")));
					singleDropDown = driver.findElements(By.name("contactOwnerID2"));
					WebElement dropDownField = driver.findElement(By.name("contactOwnerID2"));
					fc.utobj().sleep(3000);
					fc.utobj().selectDropDownByVisibleText(driver, dropDownField, dataSet.get("contactOwnerID2"));
					fc.utobj().sleep(3000);
					dropDownField = driver.findElement(By.name("cmSource2ID"));
					fc.utobj().selectDropDownByVisibleText(driver, dropDownField, dataSet.get("cmSource2ID"));
					
					fc.utobj().sleep(3000);// please do not remove this line 
					
					dropDownField = driver.findElement(By.name("cmSource3ID"));
					fc.utobj().selectDropDownByVisibleText(driver, dropDownField, dataSet.get("cmSource3ID"));
					fc.utobj().sleep(3000);

				} catch (Exception e) {

				}

			}

		} catch (Exception e) {
			// fc.utobj().throwsException("Fields data are not available!
			// "+e.getMessage());
		}
		try {
			// fc.utobj()+printTestStep(testCaseId, "Submiting the Tab ");
			WebElement elementbutton = fc.utobj().getElementByXpath(driver,
					".//*[@id='Submit' or @name='Submit' or @name='button' or @name='Submit1' or @name='add1'] ");
			fc.utobj().clickElement(driver, elementbutton);

			
			
		} catch (Exception ee) {
			fc.utobj().printBugStatus("Unable to submit page " + ee);
		}

		return printInfo;
	}

}