package com.builds.test.crm;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.testng.Reporter;
import org.testng.annotations.Test;

import com.builds.test.admin.AdminUsersManageCorporateUsersAddCorporateUserPageTest;
import com.builds.test.common.CorporateUser;
import com.builds.uimaps.crm.CRMContactsPage;
import com.builds.uimaps.crm.CRMLeadsPage;
import com.builds.uimaps.crm.CRMTasksPage;
import com.builds.utilities.FranconnectUtil;
import com.builds.utilities.TestCase;

public class CRMContact_AreaMoveContactTestCase {

	FranconnectUtil fc = new FranconnectUtil();

	@Test(groups = { "crm", "TC_Contact_CRM_MoveToContact_Case" })
	@TestCase(createdOn = "2018-01-17", updatedOn = "2018-01-17", testCaseId = "TC_Contact_CRM_MoveToContact_Case", testCaseDescription = "To verify that user is able to move Contact in to Lead.")
	private void crmInfoFillAreaMoveToContact_1() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		Map<String, String> dataSetCustom = null;
		String testCaseIdInternal = "" + testCaseId;
		Map<String, String> printInfo = null;
		try {
			driver = fc.loginpage().login(driver);
			CRMLeadsPage pobj = new CRMLeadsPage(driver);

			fc.utobj().printTestStep("Navigate To Admin Users Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			String emailId = "crmautomation@staffex.com";
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUser = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporateUser.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			fc.utobj().printTestStep("Navigate To CRM > Contact > Contact  Summary");
			fc.utobj().printTestStep("Add Contact");
			fc.crm().crm_common().CRMContactsLnk(driver);
			fc.utobj().clickElement(driver, pobj.addNew);
			fc.utobj().clickElement(driver, pobj.contactLnk);

			testCaseIdInternal = "TC_CRM_QA_Contact_Details";
			dataSetCustom = fc.utobj().readTestData("crm", testCaseIdInternal);
			String firstName = fc.utobj().generateTestData("Aa");
			String lastName = fc.utobj().generateTestData("Bb");
			dataSetCustom.put("contactFirstName", firstName);
			dataSetCustom.put("contactLastName", lastName);
			dataSetCustom.put("contactType", "Contacts");
			dataSetCustom.put("cmLeadTypeID", "Select");
			dataSetCustom.put("cmSource2ID", "Internet");
			dataSetCustom.put("cmSource3ID", "BISON");
			dataSetCustom.put("accountName", "");
			dataSetCustom.put("contactOwnerID2", corpUser.getuserFullName());

			printInfo = fillFormDataWithDataset(driver, testCaseIdInternal, dataSetCustom);
			// Reporter.log("Map == for ="+testCaseIdInternal+"==="+printInfo);

			if (printInfo != null && printInfo.size() > 0) {
				// System.out.println("printInfo=First="+printInfo);

				// System.out.println("printInfo=First Email
				// ="+printInfo.get("emailIds"));
			}

			fc.utobj().clickElement(driver, pobj.contactsLinks);

			systemExactSearch(driver, firstName + " " + lastName);

			fc.utobj().printTestStep("Verify Contact Name in Contact Summary.");
			boolean isLeadNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains( text(),'" + firstName + " " + lastName + "')]");
			if (isLeadNamePresent == false) {
				fc.utobj().throwsException("was not able to verify Contact Name in summary");
			}

			// Convert to Leads
			fc.utobj().printTestStep("Move Contact to Lead");
			fc.utobj().actionImgOption(driver, firstName + " " + lastName, "Move Contact to Lead");

			if (!fc.utobj().isSelected(driver, pobj.leadOwnerRadio)) {
				fc.utobj().clickElement(driver, pobj.leadOwnerRadio);
			}
			String contactFName = fc.utobj().generateTestData("Confirst");
			fc.utobj().sendKeys(driver, pobj.leadFirstName, contactFName);
			String contactLName = fc.utobj().generateTestData("Conlast");
			fc.utobj().sendKeys(driver, pobj.leadLastName, contactLName);
			try {
				fc.utobj().clickElement(driver, pobj.btnConvert);
			} catch (Exception e) {
				fc.utobj().throwsException("was not able to verify Move button on the Move Contact page.");
			}
			try {
				fc.utobj().clickElement(driver, pobj.goTosummaryBtn);
			} catch (Exception e) {
				fc.utobj().throwsException("was not able to verify Go to summary button on the Move Contact page");
			}

			fc.utobj().clickElement(driver, pobj.leadsLink);

			// verify at lead page
			fc.utobj().printTestStep("Verify Moved Lead on the Moved Lead page");
			searchLeadByOwnerLeadLink(driver, corpUser.getuserFullName(), "Converted Leads");

			boolean convertedLead = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text (),'" + contactFName + " " + contactLName + "')]");
			if (convertedLead == false) {
				fc.utobj().throwsException("was not able to verify Move Contact to Lead on Leads summary Page");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "crm", "TC_Contact_CRM_ConvertGroupCase" })
	@TestCase(createdOn = "2018-01-16", updatedOn = "2018-01-16", testCaseId = "TC_Contact_CRM_ConvertGroupCase", testCaseDescription = "To Verify leads Groups from multi select drop down on convert Lead Page.")
	private void crmInfoFillAreaConvert_Group() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		Map<String, String> dataSetCustom = null;
		String testCaseIdInternal = "" + testCaseId;
		Map<String, String> printInfo = null;
		try {
			driver = fc.loginpage().login(driver);
			CRMLeadsPage pobj = new CRMLeadsPage(driver);

			fc.utobj().printTestStep("Navigate To Admin > CRM > Configure Status Page");
			fc.utobj().printTestStep("Add Contact Status");
			String contactStatus = fc.utobj().generateTestData("Constat");
			AdminCRMConfigureStatusPageTest statusPage = new AdminCRMConfigureStatusPageTest();
			statusPage.addStatus(driver, contactStatus);

			fc.utobj().printTestStep("Navigate To CRM > Group");
			fc.utobj().printTestStep("Create Group");
			String typeGroup = "Lead";
			String accessibility = "Public To All User";
			String groupName = fc.utobj().generateTestData("group");
			CRMGroupsPageTest G1 = new CRMGroupsPageTest();
			G1.createGroup(driver, typeGroup, accessibility, groupName, config);

			fc.utobj().printTestStep("Navigate To Admin Users Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			String emailId = "crmautomation@staffex.com";
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUser = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporateUser.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			fc.utobj().printTestStep("Navigate To CRM > Contact > Contact  Summary");
			fc.utobj().printTestStep("Add Contact");
			fc.crm().crm_common().CRMContactsLnk(driver);
			fc.utobj().clickElement(driver, pobj.addNew);
			fc.utobj().clickElement(driver, pobj.contactLnk);

			testCaseIdInternal = "TC_CRM_QA_Contact_Details";
			dataSetCustom = fc.utobj().readTestData("crm", testCaseIdInternal);
			String firstName = fc.utobj().generateTestData("Aa");
			String lastName = fc.utobj().generateTestData("Bb");
			dataSetCustom.put("contactFirstName", firstName);
			dataSetCustom.put("contactLastName", lastName);
			dataSetCustom.put("contactType", "Contacts");
			dataSetCustom.put("cmLeadTypeID", "Select");
			dataSetCustom.put("cmSource2ID", "Internet");
			dataSetCustom.put("cmSource3ID", "BISON");
			dataSetCustom.put("accountName", "");
			dataSetCustom.put("contactOwnerID2", corpUser.getuserFullName());

			printInfo = fillFormDataWithDataset(driver, testCaseIdInternal, dataSetCustom);
			// Reporter.log("Map == for ="+testCaseIdInternal+"==="+printInfo);

			if (printInfo != null && printInfo.size() > 0) {
				// System.out.println("printInfo=First="+printInfo);

				// System.out.println("printInfo=First Email
				// ="+printInfo.get("emailIds"));
			}

			fc.utobj().clickElement(driver, pobj.contactsLinks);

			systemExactSearch(driver, firstName + " " + lastName);

			fc.utobj().printTestStep("Verify Contact Name in Contact Summary.");
			boolean isLeadNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains( text(),'" + firstName + " " + lastName + "')]");
			if (isLeadNamePresent == false) {
				fc.utobj().throwsException("was not able to verify Contact Name in summary");
			}

			// Convert to Leads
			fc.utobj().printTestStep("Move Contact to Lead");
			fc.utobj().actionImgOption(driver, firstName + " " + lastName, "Move Contact to Lead");

			if (!fc.utobj().isSelected(driver, pobj.leadOwnerRadio)) {
				fc.utobj().clickElement(driver, pobj.leadOwnerRadio);
			}
			String contactFName = fc.utobj().generateTestData("Confirst");
			fc.utobj().sendKeys(driver, pobj.leadFirstName, contactFName);
			String contactLName = fc.utobj().generateTestData("Conlast");
			fc.utobj().sendKeys(driver, pobj.leadLastName, contactLName);
			fc.utobj().selectDropDown(driver, fc.utobj().getElementByID(driver, "leadStatus"), contactStatus);
			fc.utobj().selectValFromMultiSelect(driver, pobj.groupSelect, groupName);

			try {
				fc.utobj().clickElement(driver, pobj.btnConvert);
			} catch (Exception e) {
				fc.utobj().throwsException("was not able to verify Move button on the Move Contact page.");
			}
			try {
				fc.utobj().clickElement(driver, pobj.goTosummaryBtn);
			} catch (Exception e) {
				fc.utobj().throwsException("was not able to verify Go to summary button on the Move Contact page");
			}

			fc.utobj().clickElement(driver, pobj.leadsLink);

			// verify at lead page
			fc.utobj().printTestStep("Verify Moved Lead on the Moved Lead page");
			searchLeadByOwnerLeadLink(driver, corpUser.getuserFullName(), "Converted Leads");

			boolean convertedLead = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text (),'" + contactFName + " " + contactLName + "')]");
			if (convertedLead == false) {
				fc.utobj().throwsException("was not able to verify Move Contact to Lead on Leads summary Page");
			}

			fc.utobj().clickElement(driver, driver
					.findElement(By.xpath(".//a[contains(text () ,'" + contactFName + " " + contactLName + "')]")));

			boolean isGroupPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () ,'Group')]/following-sibling::td[contains(text () ,'" + groupName + "')]");
			if (isGroupPresent == false) {
				fc.utobj().throwsException("was not able to verify Group at Contact Page");
			}

			fc.utobj().printTestStep("Verify Lead Status at Lead Page");

			WebElement staus = fc.utobj().getElementByID(driver, "cmLeadStatusID");
			String resulantstatts = fc.utobj().getSelectedDropDownValue(driver, staus);

			if (resulantstatts != null && resulantstatts.equals(contactStatus)) {

			} else {
				fc.utobj().throwsException("was not able to verify Status at Lead Page");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}

	}

	public void searchBySubjectFilter(WebDriver driver, String subject) throws Exception {
		CRMTasksPage pobj = new CRMTasksPage(driver);
		fc.utobj().clickElement(driver, pobj.showFilter);
		fc.utobj().sendKeys(driver, pobj.subjectFilter, subject);
		fc.utobj().setToDefault(driver, pobj.taskStatusFilter);
		fc.utobj().sendKeys(driver, pobj.startdateFilter, "");
		fc.utobj().sendKeys(driver, pobj.endDate, "");
		fc.utobj().selectDropDown(driver, pobj.reminderFilter, "Select");
		fc.utobj().setToDefault(driver, pobj.prioprityFilter);
		fc.utobj().setToDefault(driver, pobj.viewMineFilter);
		fc.utobj().setToDefault(driver, pobj.associatedWithFilter);
		fc.utobj().clickElement(driver, pobj.searchBtnFilter);
		fc.utobj().clickElement(driver, pobj.hideFilters);
	}

	public void actionImgOption(WebDriver driver, String task, String option) throws Exception {

		fc.utobj().moveToElement(driver,
				fc.utobj().getElementByXpath(driver, ".//*[contains(text () ,'" + task + "')]/ancestor::tr/td/layer"));
		String alterText = fc.utobj()
				.getElementByXpath(driver, ".//*[contains(text () ,'" + task + "')]/ancestor::tr/td/layer")
				.getAttribute("id").trim();
		alterText = alterText.replace("Actions_dynamicmenu", "");
		alterText = alterText.replace("Bar", "");

		fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
				".//*[contains(text () ,'" + task + "')]/ancestor::tr/td/layer/a/img"));
		// fc.utobj().clickElement(driver,
		// fc.utobj().getElementByXpath(driver,".//div[@id='Actions_dynamicmenu"+alterText+"Menu']/span[contains(text
		// () , '"+option+"')]")));
		fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
				".//div[@id='TaskActions_dynamicmenu" + alterText + "Menu']/*[contains(text () , '" + option + "')]"));
	}

	public void actionImgOptionOnlyShow(WebDriver driver, String task, String option) throws Exception {

		fc.utobj().moveToElement(driver,
				fc.utobj().getElementByXpath(driver, ".//*[contains(text () ,'" + task + "')]/ancestor::tr/td/layer"));
		String alterText = fc.utobj()
				.getElementByXpath(driver, ".//*[contains(text () ,'" + task + "')]/ancestor::tr/td/layer")
				.getAttribute("id").trim();
		alterText = alterText.replace("Actions_dynamicmenu", "");
		alterText = alterText.replace("Bar", "");

		fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
				".//*[contains(text () ,'" + task + "')]/ancestor::tr/td/layer/a/img"));
		// fc.utobj().clickElement(driver,
		// fc.utobj().getElementByXpath(driver,".//div[@id='Actions_dynamicmenu"+alterText+"Menu']/span[contains(text
		// () , '"+option+"')]")));
		// fc.utobj().clickElement(driver,
		// fc.utobj().getElementByXpath(driver,".//div[@id='TaskActions_dynamicmenu"+alterText+"Menu']/*[contains(text
		// () , '"+option+"')]")));
	}

	public void searchLeadByOwner(WebDriver driver, String userName, String leadTypeView) throws Exception {

		CRMContactsPage pobj = new CRMContactsPage(driver);
		fc.utobj().clickElement(driver, pobj.showFilter);
		fc.utobj().setToDefault(driver, pobj.contactOwnerSelect);
		try {
			WebElement divsion = fc.utobj().getElementByXpath(driver, ".//*[@id='ms-parentdivisionID']");
			fc.utobj().setToDefault(driver, divsion);
		} catch (Exception e) {
		}
		fc.utobj()
				.selectValFromMultiSelect(driver, pobj.contactOwnerSelect, fc.utobj()
						.getElement(driver, pobj.contactOwnerSelect).findElement(By.xpath("./div/div/input")),
						userName);
		fc.utobj().setToDefault(driver, pobj.franchiseIdSelect);
		fc.utobj().setToDefault(driver, pobj.statusSelect);
		fc.utobj().selectDropDown(driver, pobj.addDateSelect, "All");
		fc.utobj().setToDefault(driver, pobj.emailStatusSelect);
		// fc.utobj().selectDropDown(driver, pobj.viewLeadsSelect,
		// leadTypeView);
		fc.utobj().clickElement(driver, pobj.searchBtn);
		;
		fc.utobj().clickElement(driver, pobj.hideFilter);
	}

	public void searchLeadByOwnerLeadLink(WebDriver driver, String userName, String leadTypeView) throws Exception {

		CRMLeadsPage pobj = new CRMLeadsPage(driver);
		fc.utobj().clickElement(driver, pobj.showFilter);
		fc.utobj().setToDefault(driver, pobj.leadOwnerSelect);
		try {
			WebElement divsion = fc.utobj().getElementByXpath(driver, ".//*[@id='ms-parentdivisionID']");
			fc.utobj().setToDefault(driver, divsion);
		} catch (Exception e) {
		}
		fc.utobj()
				.selectValFromMultiSelect(driver, pobj.leadOwnerSelect, fc.utobj()
						.getElement(driver, pobj.leadOwnerSelect).findElement(By.xpath("./div/div/input")),
						userName);
		fc.utobj().setToDefault(driver, pobj.franchiseSelect);
		fc.utobj().setToDefault(driver, pobj.statusSelect);
		fc.utobj().selectDropDown(driver, pobj.addDateSelect, "All");
		fc.utobj().setToDefault(driver, pobj.emailStatusSelect);
		// fc.utobj().selectDropDown(driver, pobj.viewLeadsSelect,
		// leadTypeView);
		fc.utobj().clickElement(driver, pobj.searchBtn);
		;
		fc.utobj().clickElement(driver, pobj.hideFilter);
	}

	public void searchLeadByOwnerDiffrent(WebDriver driver, String userName, String leadTypeView, List<String> listInfo)
			throws Exception {

		CRMLeadsPage pobj = new CRMLeadsPage(driver);
		fc.utobj().clickElement(driver, pobj.showFilter);
		fc.utobj().setToDefault(driver, pobj.leadOwnerSelect);
		try {
			WebElement divsion = fc.utobj().getElementByXpath(driver, ".//*[@id='ms-parentdivisionID']");
			fc.utobj().setToDefault(driver, divsion);
		} catch (Exception e) {
		}
		fc.utobj().selectMultipleValFromMultiSelect(driver,
				fc.utobj().getElementByXpath(driver, ".//*[@id='ms-parentleadOwner']"), listInfo);
		// fc.utobj().selectValFromMultiSelect(driver,
		// pobj.leadOwnerSelect,fc.utobj().getElement(driver,
		// pobj.leadOwnerSelect).findElement(By.xpath("./div/div/input")),
		// userName);
		fc.utobj().setToDefault(driver, pobj.franchiseSelect);
		fc.utobj().setToDefault(driver, pobj.statusSelect);
		fc.utobj().selectDropDown(driver, pobj.addDateSelect, "All");
		fc.utobj().setToDefault(driver, pobj.emailStatusSelect);
		fc.utobj().selectDropDown(driver, pobj.viewLeadsSelect, leadTypeView);
		fc.utobj().clickElement(driver, pobj.searchBtn);
		;
		fc.utobj().clickElement(driver, pobj.hideFilter);
	}

	// system search
	public void systemExactSearch(WebDriver driver, String leadName) throws Exception {
		// search By System Search Button
		CRMLeadsPage pobj = new CRMLeadsPage(driver);
		fc.utobj().sendKeys(driver, pobj.searchContact, leadName);
		// fc.utobj().clickElement(driver, pobj.exactSearch);
		fc.utobj().clickElement(driver, pobj.systemSearchButton);
	}

	public Map<String, String> fillFormDataWithDataset(WebDriver driver, String testCaseId, Map<String, String> dataSet)
			throws Exception {
		Reporter.log("Filling TestCase Data = " + testCaseId);
		String id = "id";

		List<String> listItems = new ArrayList<String>();
		Map<String, String> printInfo = new HashMap<String, String>();

		List<WebElement> mandateBox = null;
		mandateBox = driver.findElements(By.xpath(
				".//td[@qat_tableid='addData']//*[@type='text' or @class='fTextBoxDate' or @class='fTextBox' or @class='multiList' or @type='radio' or @type='checkbox' or @type='checkBox' or @id='ms-parentsellerCombo' or @class='ms-choice' or @class='form-control']"));

		Map<String, String> alreadyitrateElement = new HashMap<>();
		Map<String, String> alreadyradio = new HashMap<>();
		Map<String, String> alreadycheckBox = new HashMap<>();

		try {
			for (WebElement webElement : mandateBox) {

				if (webElement.getAttribute("" + id) != null && !"".equals(webElement.getAttribute("" + id))
						&& !"null".equals(webElement.getAttribute("" + id))
						&& !":".equals(webElement.getAttribute("" + id))
						&& !"dateOfOpen".equals(webElement.getAttribute("" + id))
						&& !"fimCbOtherComplaintType".equals(webElement.getAttribute("" + id))) {
					/*
					 * try{
					 * System.out.println("Type==="+webElement.getAttribute(
					 * "type")+"====values==="+webElement.getAttribute("value")
					 * +"======"+dataSet.get(""+webElement.getAttribute(""+id))
					 * +"====="+""+webElement.getAttribute(""+id));
					 * }catch(Exception e) {
					 * 
					 * }
					 */

					if (alreadyitrateElement.containsKey(webElement.getAttribute("" + id))) {

					} else {
						if (dataSet.containsKey(webElement.getAttribute("" + id))) {
							WebElement elementmovepostion = null;
							if ("name".equals(id)) {
								elementmovepostion = driver.findElement(By.name("" + webElement.getAttribute("" + id)));

							} else {
								elementmovepostion = fc.utobj().getElementByID(driver,
										"" + webElement.getAttribute("" + id));

							}
							fc.utobj().moveToElement(driver, elementmovepostion);
							if (webElement.getAttribute("type").indexOf("select-one") != -1) {
								if (webElement.getAttribute("" + id) != null
										&& !"cmSource2ID".equals(webElement.getAttribute("" + id))
										&& !"cmSource3ID".equals(webElement.getAttribute("" + id)))

								{
									try {

										Select singledrop = null;
										List<WebElement> singleDropDown = null;
										try {
											singledrop = new Select(fc.utobj().getElementByID(driver,
													"" + webElement.getAttribute("" + id)));
											singleDropDown = driver
													.findElements(By.id("" + webElement.getAttribute("" + id)));
										} catch (Exception e) {
											singledrop = new Select(
													driver.findElement(By.name("" + webElement.getAttribute("" + id))));
											singleDropDown = driver
													.findElements(By.name("" + webElement.getAttribute("" + id)));
										}
										if (webElement.getAttribute("value") == null
												|| "".equals(webElement.getAttribute("value"))
												|| "null".equals(webElement.getAttribute("value"))
												|| "-1".equals(webElement.getAttribute("value"))) {

											int size = singleDropDown.size();
											if (dataSet.get("" + webElement.getAttribute("" + id)) != null
													&& !"".equals(dataSet.get("" + webElement.getAttribute("" + id)))) {
												if (size >= 1) {
													try {
														WebElement dropDownField = driver
																.findElement(By.id("" + webElement.getAttribute("id")));
														// fc.utobj().moveToElement(driver,
														// dropDownField);
														fc.utobj().selectDropDownByVisibleText(driver, dropDownField,
																dataSet.get("" + webElement.getAttribute("" + id)));

														if (!"birthMonth".equals(webElement.getAttribute("" + id))
																&& !"birthDate".equals(webElement.getAttribute("" + id))
																&& !"spouseBirthMonth"
																		.equals(webElement.getAttribute("" + id))
																&& !"spouseBirthDate"
																		.equals(webElement.getAttribute("" + id))) {
															listItems.add(
																	dataSet.get("" + webElement.getAttribute("" + id)));
														}
														printInfo.put("" + webElement.getAttribute("" + id),
																dataSet.get("" + webElement.getAttribute("" + id)));

													} catch (Exception eradio) {
														// Reporter.log("Problem
														// in selecting drop :
														// "+""+webElement.getAttribute("id")
														// +" Values getting
														// from Excel not match
														// in desired
														// fomrat."+""+dataSet.get(""+webElement.getAttribute("id")));
													}
												}
											}
										}

									} catch (Exception e3) {

									}
								}

							} else if (webElement.getAttribute("type").indexOf("select-multiple") != -1) {
								if (webElement.getAttribute("value") == null
										|| "".equals(webElement.getAttribute("value"))
										|| "null".equals(webElement.getAttribute("value"))
										|| "-1".equals(webElement.getAttribute("value"))) {
									try {
										WebElement element = driver
												.findElement(By.id("ms-parent" + webElement.getAttribute("" + id)));
										fc.utobj().clickElement(driver, element);
										fc.utobj().clickElement(driver,
												fc.utobj().getElementByXpath(driver, ".//*[@id='selectAll']"));
										fc.utobj().clickElement(driver, element);
									} catch (Exception emultipal) {
										// Reporter.log("Problem in multiple
										// value drop-down :
										// "+""+webElement.getAttribute("id"));
									}
								}
							} else if (webElement.getAttribute("type").indexOf("textarea") != -1) {
								if (webElement.getAttribute("value") == null
										|| "".equals(webElement.getAttribute("value"))
										|| "null".equals(webElement.getAttribute("value"))) {
									try {
										driver.findElement(By.name("" + webElement.getAttribute("" + id)))
												.sendKeys(dataSet.get("" + webElement.getAttribute("" + id)));
										listItems.add(dataSet.get("" + webElement.getAttribute("" + id)));
										printInfo.put("" + webElement.getAttribute("id"),
												dataSet.get("" + webElement.getAttribute("" + id)));
									} catch (Exception eText) {
										// Reporter.log("Problem in entering
										// TextArea in :
										// "+""+webElement.getAttribute("id"));
									}
								}
							} else if (webElement.getAttribute("type").indexOf("radio") != -1) {
								try {
									String sValue = "";
									List<WebElement> rdBtn_Sex = driver
											.findElements(By.id("" + webElement.getAttribute("" + id))); //
									if (alreadyradio.containsKey(webElement.getAttribute("" + id))) {

									} else {

										int size = rdBtn_Sex.size();
										if (size > 1) {
											sValue = dataSet.get("" + webElement.getAttribute("" + id));
										}
										try {
											List<WebElement> rdBtn_Field = null;
											WebElement element = null;
											try {
												rdBtn_Field = driver.findElements(By.xpath(
														".//*[@name='" + webElement.getAttribute("" + id) + "']")); // work
												element = fc.utobj().getElementByXpath(driver,
														".//*[@name='" + webElement.getAttribute("" + id) + "']");
											} catch (Exception eTxt) {
												rdBtn_Field = driver.findElements(
														By.xpath(".//*[@id='" + webElement.getAttribute("id") + "']")); // work
												element = driver.findElement(
														By.xpath(".//*[@id='" + webElement.getAttribute("id") + "']"));

											}

											fc.utobj().moveToElement(driver, element);
											fc.utobj().clickRadioButton(driver, rdBtn_Field, sValue); // work
											listItems.add(dataSet.get("" + webElement.getAttribute("" + id)));
											printInfo.put("" + webElement.getAttribute("id"),
													dataSet.get("" + webElement.getAttribute("" + id)));
											alreadyradio.put(webElement.getAttribute("" + id),
													webElement.getAttribute("" + id));
										} catch (Exception eText) {
											// Reporter.log("Problem in
											// selecting radio value in :
											// "+""+webElement.getAttribute("id"));
										}
									}
								} catch (Exception exp) {
									Reporter.log(exp.toString());
									exp.printStackTrace();
								}

							} else if (webElement.getAttribute("type").toLowerCase().indexOf("checkbox") != -1) {
								List<WebElement> oCheckBox = driver
										.findElements(By.name("" + webElement.getAttribute("" + id)));
								String sValue = "";
								if (alreadycheckBox.containsKey(webElement.getAttribute("" + id))) {

								} else {

									int size = oCheckBox.size();
									for (int i = 0; i < size; i++) {
										sValue = oCheckBox.get(i).getAttribute("value");
										// Thread.sleep(2000);
										if (i == (size - 1)) {
											// break;
										}

									}
									try {
										List<WebElement> rdBtn_Field = driver.findElements(By.xpath(
												"//*[contains(@id, '" + webElement.getAttribute("" + id) + "')]")); // work
										WebElement element = fc.utobj().getElementByXpath(driver,
												"//*[contains(@id, '" + webElement.getAttribute("" + id) + "')]");
										fc.utobj().moveToElement(driver, element);
										fc.utobj().clickRadioButton(driver, rdBtn_Field, sValue); // work
										alreadycheckBox.put(webElement.getAttribute("" + id),
												webElement.getAttribute("" + id));
									} catch (Exception eText) {

									}
								}
							} else if (webElement.getAttribute("type").indexOf("file") != -1) {
								String fileName = fc.utobj().getFilePathFromTestData("pictureFile");
								try {
									fc.utobj().sendKeys(driver,
											fc.utobj().getElementByID(driver, "" + webElement.getAttribute("" + id)),
											fileName);
									listItems.add(fileName);
									printInfo.put("" + webElement.getAttribute("" + id), fileName);
								} catch (Exception eFile) {
									// Reporter.log("Problem in uploading file
									// type value in :
									// "+""+webElement.getAttribute("id"));
								}
							} else if (webElement.getAttribute("type").indexOf("text") != -1) {
								if (webElement.getAttribute("value") == null
										|| "".equals(webElement.getAttribute("value"))
										|| "null".equals(webElement.getAttribute("value"))
										|| "0.00".equals(webElement.getAttribute("value"))) {
									if (webElement.getAttribute("class").indexOf("fTextBoxDate") != -1) {
										try {
											if (dataSet.get("" + webElement.getAttribute("" + id)) != null
													|| !"".equals(dataSet.get("" + webElement.getAttribute("" + id)))
													|| !"null".equals(
															dataSet.get("" + webElement.getAttribute("" + id)))) {
												fc.utobj().sendKeys(driver,
														driver.findElement(
																By.name("" + webElement.getAttribute("" + id))),
														dataSet.get("" + webElement.getAttribute("" + id))); // work
												listItems.add(dataSet.get("" + webElement.getAttribute("" + id)));
												printInfo.put("" + webElement.getAttribute("" + id),
														dataSet.get("" + webElement.getAttribute("" + id)));

											} else {

											}
										} catch (Exception eDate) {
											// Reporter.log("Problem in Entering
											// Date fields value in :
											// "+""+webElement.getAttribute("id"));
										}
									} else {
										try {
											if ("0.00".equals(webElement.getAttribute("value"))) {
												fc.utobj()
														.sendKeys(driver,
																driver.findElement(
																		By.name("" + webElement.getAttribute("" + id))),
																"");

												// System.out.println(webElement.getAttribute("value")
												// +"======"+dataSet.get(""+webElement.getAttribute(""+id))
												// +"====="+""+webElement.getAttribute(""+id));

											}
											if ("name".equals(id)) {
												fc.utobj().sendKeys(driver,
														driver.findElement(
																By.name("" + webElement.getAttribute("" + id))),
														dataSet.get("" + webElement.getAttribute("" + id)));
												if ("accountName".equals("" + webElement.getAttribute("" + id))
														|| "opportunityOwner"
																.equals("" + webElement.getAttribute("" + id))
														|| "oppContactID"
																.equals("" + webElement.getAttribute("" + id))) {
													;
													fc.utobj().clickElement(driver, driver.findElement(
															By.xpath(".//*[@id='customizedAjaxSearch']/div")));
													fc.utobj().sleep();
												}
											} else {
												fc.utobj().sendKeys(driver,
														driver.findElement(
																By.id("" + webElement.getAttribute("" + id))),
														dataSet.get("" + webElement.getAttribute("" + id)));
												if ("accountName".equals("" + webElement.getAttribute("" + id))
														|| "opportunityOwner"
																.equals("" + webElement.getAttribute("" + id))
														|| "oppContactID"
																.equals("" + webElement.getAttribute("" + id))) {
													;
													fc.utobj().clickElement(driver, driver.findElement(
															By.xpath(".//*[@id='customizedAjaxSearch']/div")));
													fc.utobj().sleep();
												}
											}

											if (!"ssn".equals("" + webElement.getAttribute("" + id))
													&& !"spouseSsn".equals("" + webElement.getAttribute("" + id))) {
												listItems.add(dataSet.get("" + webElement.getAttribute("" + id)));
											}
											printInfo.put("" + webElement.getAttribute("" + id),
													dataSet.get("" + webElement.getAttribute("" + id)));

										} catch (Exception eText) {
											// Reporter.log("Problem in Entering
											// Text fileds value in :
											// "+""+webElement.getAttribute("id"));
										}

									}
								}
							}
							alreadyitrateElement.put(webElement.getAttribute("" + id),
									webElement.getAttribute("" + id));
						}
					}
				}
			}

			if ("TC_CRM_QA_Contact_Details".equals(testCaseId)) {

				Select singledrop = null;
				List<WebElement> singleDropDown = null;
				try {
					singledrop = new Select(driver.findElement(By.name("contactOwnerID2")));
					singleDropDown = driver.findElements(By.name("contactOwnerID2"));
					WebElement dropDownField = driver.findElement(By.name("contactOwnerID2"));
					fc.utobj().selectDropDownByVisibleText(driver, dropDownField, dataSet.get("contactOwnerID2"));

					dropDownField = driver.findElement(By.name("cmSource2ID"));
					fc.utobj().selectDropDownByVisibleText(driver, dropDownField, dataSet.get("cmSource2ID"));
					;
					dropDownField = driver.findElement(By.name("cmSource3ID"));
					fc.utobj().selectDropDownByVisibleText(driver, dropDownField, dataSet.get("cmSource3ID"));

				} catch (Exception e) {

				}

			}

		} catch (Exception e) {
			// fc.utobj().throwsException("Fields data are not available!
			// "+e.getMessage());
		}
		try {
			// fc.utobj().printTestStep(testCaseId, "Submiting the Tab ");
			WebElement elementbutton = fc.utobj().getElementByXpath(driver,
					".//*[@id='Submit' or @name='Submit' or @name='button' or @name='Submit1' or @name='add1'] ");
			fc.utobj().clickElement(driver, elementbutton);

		} catch (Exception ee) {
			fc.utobj().printBugStatus("Unable to submit page " + ee);
		}

		return printInfo;
	}

}