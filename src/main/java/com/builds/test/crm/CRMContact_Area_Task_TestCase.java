package com.builds.test.crm;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;

import javax.xml.parsers.ParserConfigurationException;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.testng.Reporter;
import org.testng.annotations.Test;
import org.xml.sax.SAXException;

import com.builds.test.admin.AdminUsersManageCorporateUsersAddCorporateUserPageTest;
import com.builds.test.common.CorporateUser;
import com.builds.uimaps.crm.CRMContactsPage;
import com.builds.uimaps.crm.CRMLeadsPage;
import com.builds.uimaps.crm.CRMTasksPage;
import com.builds.utilities.FranconnectUtil;
import com.builds.utilities.TestCase;

public class CRMContact_Area_Task_TestCase {

	FranconnectUtil fc = new FranconnectUtil();

	@Test(groups = { "crm", "TC_Contact_CRM_NoTask_Case" })
	@TestCase(createdOn = "2018-01-17", updatedOn = "2018-01-17", testCaseId = "TC_Contact_CRM_NoTask_Case", testCaseDescription = "To verify that in case no task is associated with Contact then user is able to select the users.")
	private void crmInfoFillAreaNoTask_1() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		Map<String, String> dataSetCustom = null;
		String testCaseIdInternal = "" + testCaseId;
		Map<String, String> printInfo = null;
		try {
			driver = fc.loginpage().login(driver);
			CRMLeadsPage pobj = new CRMLeadsPage(driver);

			fc.utobj().printTestStep("Navigate To Admin Users Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			String emailId = "crmautomation@staffex.com";
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUser = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporateUser.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			fc.utobj().printTestStep("Navigate To CRM > Contact > Contact  Summary");
			fc.utobj().printTestStep("Add Contact");
			fc.crm().crm_common().CRMContactsLnk(driver);
			fc.utobj().clickElement(driver, pobj.addNew);
			fc.utobj().clickElement(driver, pobj.contactLnk);

			testCaseIdInternal = "TC_CRM_QA_Contact_Details";
			dataSetCustom = fc.utobj().readTestData("crm", testCaseIdInternal);
			String firstName = fc.utobj().generateTestData("Aa");
			String lastName = fc.utobj().generateTestData("Bb");
			dataSetCustom.put("contactFirstName", firstName);
			dataSetCustom.put("contactLastName", lastName);
			dataSetCustom.put("contactType", "Contacts");
			dataSetCustom.put("cmLeadTypeID", "Select");
			dataSetCustom.put("cmSource2ID", "Internet");
			dataSetCustom.put("cmSource3ID", "BISON");
			dataSetCustom.put("accountName", "");
			dataSetCustom.put("contactOwnerID2", corpUser.getuserFullName());

			printInfo = fillFormDataWithDataset(driver, testCaseIdInternal, dataSetCustom);
			// Reporter.log("Map == for ="+testCaseIdInternal+"==="+printInfo);

			if (printInfo != null && printInfo.size() > 0) {
				// System.out.println("printInfo=First="+printInfo);

				// System.out.println("printInfo=First Email
				// ="+printInfo.get("emailIds"));
			}

			fc.utobj().clickElement(driver, pobj.contactsLinks);

			systemExactSearch(driver, firstName + " " + lastName);

			fc.utobj().printTestStep("Verify Contact Name in Contact Summary.");
			boolean isLeadNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains( text(),'" + firstName + " " + lastName + "')]");
			if (isLeadNamePresent == false) {
				fc.utobj().throwsException("was not able to verify Contact Name in summary");
			}

			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//a[contains(text () ,'" + firstName + " " + lastName + "')]"));
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//a[contains ( text(),'Assign')]"));
			fc.utobj().printTestStep("Verify that user is able to select the users in Assign To.");
			fc.commonMethods().switch_cboxIframe_frameId(driver);

			try

			{
				fc.utobj().moveToElement(driver, fc.utobj().getElementByXpath(driver,
						".//input[@type='radio' and @name='ownerType' and @value='C']"));

				try {
					fc.utobj().moveToElement(driver,
							fc.utobj().getElementByXpath(driver, ".//select[@name='contactOwnerID2']"));
				} catch (NoSuchElementException e) {
					fc.utobj()
							.throwsException("was not able to verify that user is able to select the Corporate User.");
				}
				fc.utobj().moveToElement(driver, fc.utobj().getElementByXpath(driver,
						".//input[@type='radio' and @name='ownerType' and @value='R']"));
				try {
					fc.utobj().clickElement(driver, driver
							.findElement(By.xpath(".//input[@type='radio' and @name='ownerType' and @value='R']")));
					fc.utobj().moveToElement(driver, fc.utobj().getElementByXpath(driver, ".//select[@name='areaID']"));
				} catch (NoSuchElementException e) {
					fc.utobj().throwsException("was not able to verify that user is able to select the Regional User.");
				}
				fc.utobj().moveToElement(driver, fc.utobj().getElementByXpath(driver,
						".//input[@type='radio' and @name='ownerType' and @value='F']"));
				try {
					fc.utobj().clickElement(driver, driver
							.findElement(By.xpath(".//input[@type='radio' and @name='ownerType' and @value='F']")));
					fc.utobj().moveToElement(driver,
							fc.utobj().getElementByXpath(driver, ".//select[@name='franchiseeNo']"));

				} catch (NoSuchElementException e) {
					fc.utobj()
							.throwsException("was not able to verify that user is able to select the Franchise User.");
				}

				fc.utobj().clickElement(driver, pobj.closeBtn);
			} catch (Exception e) {
				fc.utobj().throwsException("was not able to verify that user is able to select the users.");
			}
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "crm", "TC_Contact_CRM_TaskAssignee_Case"})
	@TestCase(createdOn = "2018-01-17", updatedOn = "2018-01-17", testCaseId = "TC_Contact_CRM_TaskAssignee_Case", testCaseDescription = "To verify that after Log a Task all other user's Radio button disabled in Assign To.")
	private void crmInfoFillAreaTaskAssignee_2() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		Map<String, String> dataSetCustom = null;
		String testCaseIdInternal = "" + testCaseId;
		Map<String, String> printInfo = null;
		try {
			driver = fc.loginpage().login(driver);
			CRMLeadsPage pobj = new CRMLeadsPage(driver);

			fc.utobj().printTestStep("Navigate To Admin Users Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			String emailId = "crmautomation@staffex.com";
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUser = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporateUser.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			fc.utobj().printTestStep("Navigate To CRM > Contact > Contact  Summary");
			fc.utobj().printTestStep("Add Contact");
			fc.crm().crm_common().CRMContactsLnk(driver);
			fc.utobj().clickElement(driver, pobj.addNew);
			fc.utobj().clickElement(driver, pobj.contactLnk);

			testCaseIdInternal = "TC_CRM_QA_Contact_Details";
			dataSetCustom = fc.utobj().readTestData("crm", testCaseIdInternal);
			String firstName = fc.utobj().generateTestData("Aa");
			String lastName = fc.utobj().generateTestData("Bb");
			dataSetCustom.put("contactFirstName", firstName);
			dataSetCustom.put("contactLastName", lastName);
			dataSetCustom.put("contactType", "Contacts");
			dataSetCustom.put("cmLeadTypeID", "Select");
			dataSetCustom.put("cmSource2ID", "Internet");
			dataSetCustom.put("cmSource3ID", "BISON");
			dataSetCustom.put("accountName", "");
			dataSetCustom.put("contactOwnerID2", corpUser.getuserFullName());

			printInfo = fillFormDataWithDataset(driver, testCaseIdInternal, dataSetCustom);
			// Reporter.log("Map == for ="+testCaseIdInternal+"==="+printInfo);

			if (printInfo != null && printInfo.size() > 0) {
				// System.out.println("printInfo=First="+printInfo);

				// System.out.println("printInfo=First Email
				// ="+printInfo.get("emailIds"));
			}

			fc.utobj().clickElement(driver, pobj.contactsLinks);

			systemExactSearch(driver, firstName + " " + lastName);

			fc.utobj().printTestStep("Verify Contact Name in Contact Summary.");
			boolean isLeadNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains( text(),'" + firstName + " " + lastName + "')]");
			if (isLeadNamePresent == false) {
				fc.utobj().throwsException("was not able to verify First Contact Name");
			}

			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//a[contains(text () ,'" + firstName + " " + lastName + "')]"));

			try {
				fc.utobj().moveToElement(driver, fc.utobj().getElementByXpath(driver,
						".//*[@class='pvs_hdr2_new']/a[contains(text (),'Log a Task')]"));
			} catch (Exception e) {
				fc.utobj().throwsException(
						"was not able to verify the Log a Task button is under the Task history section.");
			}

			fc.utobj().printTestStep("Log A Task");
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//*[@class='pvs_hdr2_new']/a[contains(text (),'Log a Task')]"));
			// fc.utobj().actionImgOption(driver, firstName + " " + lastName,
			// "Log a Task");

			fc.commonMethods().switch_cboxIframe_frameId(driver);
			if (!fc.utobj().isSelected(driver,pobj.radioOwner)) {
				fc.utobj().clickElement(driver, pobj.radioOwner);
			}
			String statusTask = "Not Started";
			fc.utobj().selectDropDown(driver, pobj.statusTask, statusTask);
			String subject = "Task history section TestCase";
			fc.utobj().sendKeys(driver, pobj.subject, subject);
			if (!fc.utobj().isSelected(driver, pobj.timeLessTask)) {
				fc.utobj().clickElement(driver, pobj.timeLessTask);
			}
			String priority = "Medium";
			fc.utobj().selectDropDown(driver, pobj.priority, priority);
			String comment = "To verify Task history section.";
			fc.utobj().sendKeys(driver, pobj.comments, comment);
			fc.utobj().clickElement(driver, pobj.createBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().clickElement(driver, pobj.contactsLinks);

			systemExactSearch(driver, firstName + " " + lastName);

			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//a[contains(text () ,'" + firstName + " " + lastName + "')]"));

			fc.utobj().printTestStep("Verify Open Task");

			boolean isSubjectPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[contains(text () ,'" + subject + "')]");
			if (isSubjectPresent == false) {
				fc.utobj().throwsException("was not able to verify subject");
			}
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//a[contains(text () ,'" + subject + "')]"));
			fc.commonMethods().switch_cboxIframe_frameId(driver);

			boolean isCommentPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () ,'Comments')]/ancestor::tr/td[contains(text () ,'" + comment + "')]");
			if (isCommentPresent == false) {
				fc.utobj().throwsException("was not able to verify comments");
			}
			fc.utobj().clickElement(driver, pobj.closeBtn);
			fc.utobj().switchFrameToDefault(driver);

			// details History
			fc.utobj().printTestStep("Verify Task At Detailed History Frame");
			fc.utobj().clickElement(driver, pobj.detailedHistoryLink);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().clickElement(driver, pobj.openTaskTab);
			fc.utobj().isTextDisplayed(driver, subject,
					"was not able to verify subject of task at deatiled history pop up");
			fc.utobj().isTextDisplayed(driver, comment,
					"was not able to verify comments of task at deatiled history pop up");
			fc.utobj().clickElement(driver, pobj.closeBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().clickElement(driver, pobj.contactsLinks);
			systemExactSearch(driver, firstName + " " + lastName);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//a[contains(text () ,'" + firstName + " " + lastName + "')]"));
			isSubjectPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[contains(text () ,'" + subject + "')]");
			if (isSubjectPresent == false) {
				fc.utobj().throwsException("was not able to verify subject");
			}
			fc.utobj().printTestStep("Process Task");
			// fc.utobj().selectActionMenuItems(driver, "Process");
			actionImgOption(driver, subject, "Process");
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().selectDropDown(driver, pobj.statusTask, "Work In Progress");
			fc.utobj().sendKeys(driver, pobj.comments, "Test Comments Work In Progress");
			fc.utobj().clickElement(driver, pobj.processTask);
			fc.utobj().switchFrameToDefault(driver);

			isSubjectPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[contains(text () ,'" + subject + "')]");
			if (isSubjectPresent == false) {
				fc.utobj().throwsException("was not able to verify subject");
			}
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//a[contains(text () ,'" + subject + "')]"));
			fc.commonMethods().switch_cboxIframe_frameId(driver);

			boolean isStatsPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () ,'Status')]/ancestor::tr/td[contains(text () ,'Work In Progress')]");
			if (isStatsPresent == false) {
				fc.utobj().throwsException("was not able to verify change status");
			}

			isCommentPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () ,'Comments')]/ancestor::tr/td[contains(text () ,'Test Comments Work In Progress')]");
			if (isCommentPresent == false) {
				fc.utobj().throwsException("was not able to verify change comments");
			}
			fc.utobj().clickElement(driver, pobj.closeBtn);
			fc.utobj().switchFrameToDefault(driver);

			systemExactSearch(driver, firstName + " " + lastName);

			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//a[contains(text () ,'" + firstName + " " + lastName + "')]"));
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//a[contains ( text(),'Assign')]"));
			fc.utobj().printTestStep("Verify that user is able to select the users in Assign To.");
			fc.commonMethods().switch_cboxIframe_frameId(driver);

			try

			{
				fc.utobj().moveToElement(driver, fc.utobj().getElementByXpath(driver,
						".//input[@type='radio' and @name='ownerType' and @value='C']"));

				try {
					fc.utobj().moveToElement(driver,
							fc.utobj().getElementByXpath(driver, ".//select[@name='contactOwnerID2']"));
				} catch (NoSuchElementException e) {
					fc.utobj()
							.throwsException("was not able to verify that user is able to select the Corporate User.");
				}
				try {
					fc.utobj().moveToElement(driver, driver
							.findElement(By.xpath(".//input[@type='radio' and @name='ownerType' and @value='R']")));
					WebElement eradiol = driver
							.findElement(By.xpath(".//input[@type='radio' and @name='ownerType' and @value='R']"));
					boolean reg = eradiol.isEnabled();
					System.out.println("reg Case " + reg);
					if (reg) {
						fc.utobj().throwsException(
								"was not able to verify that user is able to select the Regional User.");
					} else {

					}
				} catch (NoSuchElementException e) {
					fc.utobj().throwsException("was not able to verify that user is able to select the Regional User.");
				}
				try {
					fc.utobj().moveToElement(driver, driver
							.findElement(By.xpath(".//input[@type='radio' and @name='ownerType' and @value='F']")));
					WebElement eradiol = driver
							.findElement(By.xpath(".//input[@type='radio' and @name='ownerType' and @value='F']"));
					boolean frn = eradiol.isEnabled();
					System.out.println("frn Case " + frn);
					if (frn) {
						fc.utobj().throwsException(
								"was not able to verify that user is able to select the Franchise User.");
					} else {

					}
				} catch (NoSuchElementException e) {
					fc.utobj()
							.throwsException("was not able to verify that user is able to select the Franchise User.");
				}

				fc.utobj().clickElement(driver, pobj.closeBtn);
			} catch (Exception e) {
				fc.utobj().throwsException("was not able to verify that user is able to select the users.");
			}
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "crm", "TC_Contact_CRM_CompletedTask_Case" })
	@TestCase(createdOn = "2018-01-17", updatedOn = "2018-01-17", testCaseId = "TC_Contact_CRM_CompletedTask_Case", testCaseDescription = "To verify that the completed tasks are not appearing on the Contact Info page.")
	private void crmInfoFillAreaTask_1() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		Map<String, String> dataSetCustom = null;
		String testCaseIdInternal = "" + testCaseId;
		Map<String, String> printInfo = null;
		try {
			driver = fc.loginpage().login(driver);
			CRMLeadsPage pobj = new CRMLeadsPage(driver);

			fc.utobj().printTestStep("Navigate To Admin Users Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			String emailId = "crmautomation@staffex.com";
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUser = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporateUser.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			fc.utobj().printTestStep("Navigate To CRM > Contact > Contact  Summary");
			fc.utobj().printTestStep("Add Contact");
			fc.crm().crm_common().CRMContactsLnk(driver);
			fc.utobj().clickElement(driver, pobj.addNew);
			fc.utobj().clickElement(driver, pobj.contactLnk);

			testCaseIdInternal = "TC_CRM_QA_Contact_Details";
			dataSetCustom = fc.utobj().readTestData("crm", testCaseIdInternal);
			String firstName = fc.utobj().generateTestData("Aa");
			String lastName = fc.utobj().generateTestData("Bb");
			dataSetCustom.put("contactFirstName", firstName);
			dataSetCustom.put("contactLastName", lastName);
			dataSetCustom.put("contactType", "Contacts");
			dataSetCustom.put("cmLeadTypeID", "Select");
			dataSetCustom.put("cmSource2ID", "Internet");
			dataSetCustom.put("cmSource3ID", "BISON");
			dataSetCustom.put("accountName", "");
			dataSetCustom.put("contactOwnerID2", corpUser.getuserFullName());

			printInfo = fillFormDataWithDataset(driver, testCaseIdInternal, dataSetCustom);
			// Reporter.log("Map == for ="+testCaseIdInternal+"==="+printInfo);

			if (printInfo != null && printInfo.size() > 0) {
				// System.out.println("printInfo=First="+printInfo);

				// System.out.println("printInfo=First Email
				// ="+printInfo.get("emailIds"));
			}

			fc.utobj().clickElement(driver, pobj.contactsLinks);

			systemExactSearch(driver, firstName + " " + lastName);

			fc.utobj().printTestStep("Verify Contact Name in Contact Summary.");
			boolean isLeadNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains( text(),'" + firstName + " " + lastName + "')]");
			if (isLeadNamePresent == false) {
				fc.utobj().throwsException("was not able to verify First Contact Name");
			}

			fc.utobj().printTestStep("Log A Task");
			fc.utobj().actionImgOption(driver, firstName + " " + lastName, "Log a Task");

			fc.commonMethods().switch_cboxIframe_frameId(driver);
			if (!fc.utobj().isSelected(driver,pobj.radioOwner)) {
				fc.utobj().clickElement(driver, pobj.radioOwner);
			}
			String statusTask = "Completed";
			fc.utobj().selectDropDown(driver, pobj.statusTask, statusTask);
			String subject = "Task for  TestCase";
			fc.utobj().sendKeys(driver, pobj.subject, subject);
			if (!fc.utobj().isSelected(driver, pobj.timeLessTask)) {
				fc.utobj().clickElement(driver, pobj.timeLessTask);
			}
			String priority = "Medium";
			fc.utobj().selectDropDown(driver, pobj.priority, priority);
			String comment = "To verify Change Assignee If task is associated with any contact.";
			fc.utobj().sendKeys(driver, pobj.comments, comment);
			fc.utobj().clickElement(driver, pobj.createBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().clickElement(driver, pobj.contactsLinks);

			systemExactSearch(driver, firstName + " " + lastName);

			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//a[contains(text () ,'" + firstName + " " + lastName + "')]"));

			fc.utobj().printTestStep("Verify Completed Task");

			boolean isSubjectPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[contains(text () ,'" + subject + "')]");
			if (isSubjectPresent == true) {
				fc.utobj()
						.throwsException("was not able to verify completed tasks are not appearing in Lead Info Page.");
			} else {
				fc.utobj().printTestStep("Verify completed tasks are not appearing in Lead Info Page.");
			}

			fc.utobj().clickElement(driver, pobj.contactsLinks);
			systemExactSearch(driver, firstName + " " + lastName);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//a[contains(text () ,'" + firstName + " " + lastName + "')]"));

			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//a[contains ( text(),'Assign')]"));
			fc.utobj().printTestStep("Verify that user is able to select the users in Assign To.");
			fc.commonMethods().switch_cboxIframe_frameId(driver);

			try

			{
				fc.utobj().moveToElement(driver, fc.utobj().getElementByXpath(driver,
						".//input[@type='radio' and @name='ownerType' and @value='C']"));

				try {
					fc.utobj().moveToElement(driver,
							fc.utobj().getElementByXpath(driver, ".//select[@name='contactOwnerID2']"));
				} catch (NoSuchElementException e) {
					fc.utobj()
							.throwsException("was not able to verify that user is able to select the Corporate User.");
				}
				fc.utobj().moveToElement(driver, fc.utobj().getElementByXpath(driver,
						".//input[@type='radio' and @name='ownerType' and @value='R']"));
				try {
					fc.utobj().clickElement(driver, driver
							.findElement(By.xpath(".//input[@type='radio' and @name='ownerType' and @value='R']")));
					fc.utobj().moveToElement(driver, fc.utobj().getElementByXpath(driver, ".//select[@name='areaID']"));
				} catch (NoSuchElementException e) {
					fc.utobj().throwsException("was not able to verify that user is able to select the Regional User.");
				}
				fc.utobj().moveToElement(driver, fc.utobj().getElementByXpath(driver,
						".//input[@type='radio' and @name='ownerType' and @value='F']"));
				try {
					fc.utobj().clickElement(driver, driver
							.findElement(By.xpath(".//input[@type='radio' and @name='ownerType' and @value='F']")));
					fc.utobj().moveToElement(driver,
							fc.utobj().getElementByXpath(driver, ".//select[@name='franchiseeNo']"));

				} catch (NoSuchElementException e) {
					fc.utobj()
							.throwsException("was not able to verify that user is able to select the Franchise User.");
				}

				fc.utobj().clickElement(driver, pobj.closeBtn);
			} catch (Exception e) {
				fc.utobj().throwsException("was not able to verify that user is able to select the users.");
			}
			fc.utobj().switchFrameToDefault(driver);

			// details History
			fc.utobj().printTestStep("Verify Task At Detailed History Frame");
			fc.utobj().clickElement(driver, pobj.detailedHistoryLink);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().clickElement(driver, pobj.openTaskTab);

			try {
				fc.utobj().isTextDisplayed(driver, subject,
						"was not able to verify subject of task at deatiled history pop up");
			} catch (Exception e) {
				fc.utobj().printTestStep("Verify completed tasks are not appearing in Detailed History open Task.");
			}

			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "crm", "crmContactAreaTask"})
	@TestCase(createdOn = "2018-01-17", updatedOn = "2018-01-17", testCaseId = "TC_Contact_CRM_Task_Case", testCaseDescription = "To verify that the Log a Task button is also appearing under the Task history section.")
	private void crmInfoFillAreaTask_2() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		Map<String, String> dataSetCustom = null;
		String testCaseIdInternal = "" + testCaseId;
		Map<String, String> printInfo = null;
		try {
			driver = fc.loginpage().login(driver);
			CRMLeadsPage pobj = new CRMLeadsPage(driver);

			fc.utobj().printTestStep("Navigate To Admin Users Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			String emailId = "crmautomation@staffex.com";
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUser = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporateUser.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			fc.utobj().printTestStep("Navigate To CRM > Contact > Contact  Summary");
			fc.utobj().printTestStep("Add Contact");
			fc.crm().crm_common().CRMContactsLnk(driver);
			fc.utobj().clickElement(driver, pobj.addNew);
			fc.utobj().clickElement(driver, pobj.contactLnk);

			testCaseIdInternal = "TC_CRM_QA_Contact_Details";
			dataSetCustom = fc.utobj().readTestData("crm", testCaseIdInternal);
			String firstName = fc.utobj().generateTestData("Aa");
			String lastName = fc.utobj().generateTestData("Bb");
			dataSetCustom.put("contactFirstName", firstName);
			dataSetCustom.put("contactLastName", lastName);
			dataSetCustom.put("contactType", "Contacts");
			dataSetCustom.put("cmLeadTypeID", "Select");
			dataSetCustom.put("cmSource2ID", "Internet");
			dataSetCustom.put("cmSource3ID", "BISON");
			dataSetCustom.put("accountName", "");
			dataSetCustom.put("contactOwnerID2", corpUser.getuserFullName());

			printInfo = fillFormDataWithDataset(driver, testCaseIdInternal, dataSetCustom);
			// Reporter.log("Map == for ="+testCaseIdInternal+"==="+printInfo);

			if (printInfo != null && printInfo.size() > 0) {
				// System.out.println("printInfo=First="+printInfo);

				// System.out.println("printInfo=First Email
				// ="+printInfo.get("emailIds"));
			}

			fc.utobj().clickElement(driver, pobj.contactsLinks);

			systemExactSearch(driver, firstName + " " + lastName);

			fc.utobj().printTestStep("Verify Contact Name in Contact Summary.");
			boolean isLeadNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains( text(),'" + firstName + " " + lastName + "')]");
			if (isLeadNamePresent == false) {
				fc.utobj().throwsException("was not able to verify First Contact Name");
			}

			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//a[contains(text () ,'" + firstName + " " + lastName + "')]"));

			try {
				fc.utobj().moveToElement(driver, fc.utobj().getElementByXpath(driver,
						".//*[@class='pvs_hdr2_new']/a[contains(text (),'Log a Task')]"));
			} catch (Exception e) {
				fc.utobj().throwsException(
						"was not able to verify the Log a Task button is under the Task history section.");
			}

			fc.utobj().printTestStep("Log A Task");
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//*[@class='pvs_hdr2_new']/a[contains(text (),'Log a Task')]"));
			// fc.utobj().actionImgOption(driver, firstName + " " + lastName,
			// "Log a Task");

			fc.commonMethods().switch_cboxIframe_frameId(driver);
			if (!fc.utobj().isSelected(driver,pobj.radioOwner)) {
				fc.utobj().clickElement(driver, pobj.radioOwner);
			}
			String statusTask = "Not Started";
			fc.utobj().selectDropDown(driver, pobj.statusTask, statusTask);
			String subject = "Task history section TestCase";
			fc.utobj().sendKeys(driver, pobj.subject, subject);
			if (!fc.utobj().isSelected(driver, pobj.timeLessTask)) {
				fc.utobj().clickElement(driver, pobj.timeLessTask);
			}
			String priority = "Medium";
			fc.utobj().selectDropDown(driver, pobj.priority, priority);
			String comment = "To verify Task history section.";
			fc.utobj().sendKeys(driver, pobj.comments, comment);
			fc.utobj().clickElement(driver, pobj.createBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().clickElement(driver, pobj.contactsLinks);

			systemExactSearch(driver, firstName + " " + lastName);

			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//a[contains(text () ,'" + firstName + " " + lastName + "')]"));

			fc.utobj().printTestStep("Verify Open Task");

			boolean isSubjectPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[contains(text () ,'" + subject + "')]");
			if (isSubjectPresent == false) {
				fc.utobj().throwsException("was not able to verify subject");
			}
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//a[contains(text () ,'" + subject + "')]"));
			fc.commonMethods().switch_cboxIframe_frameId(driver);

			boolean isCommentPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () ,'Comments')]/ancestor::tr/td[contains(text () ,'" + comment + "')]");
			if (isCommentPresent == false) {
				fc.utobj().throwsException("was not able to verify comments");
			}
			fc.utobj().clickElement(driver, pobj.closeBtn);
			fc.utobj().switchFrameToDefault(driver);

			// details History
			fc.utobj().printTestStep("Verify Task At Detailed History Frame");
			fc.utobj().clickElement(driver, pobj.detailedHistoryLink);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().clickElement(driver, pobj.openTaskTab);
			fc.utobj().isTextDisplayed(driver, subject,
					"was not able to verify subject of task at deatiled history pop up");
			fc.utobj().isTextDisplayed(driver, comment,
					"was not able to verify comments of task at deatiled history pop up");
			fc.utobj().clickElement(driver, pobj.closeBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().clickElement(driver, pobj.contactsLinks);
			systemExactSearch(driver, firstName + " " + lastName);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//a[contains(text () ,'" + firstName + " " + lastName + "')]"));
			isSubjectPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[contains(text () ,'" + subject + "')]");
			if (isSubjectPresent == false) {
				fc.utobj().throwsException("was not able to verify subject");
			}
			fc.utobj().printTestStep("Process Task");
			// fc.utobj().selectActionMenuItems(driver, "Process");
			actionImgOption(driver, subject, "Process");
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().selectDropDown(driver, pobj.statusTask, "Work In Progress");
			fc.utobj().sendKeys(driver, pobj.comments, "Test Comments Work In Progress");
			fc.utobj().clickElement(driver, pobj.processTask);
			fc.utobj().switchFrameToDefault(driver);

			isSubjectPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[contains(text () ,'" + subject + "')]");
			if (isSubjectPresent == false) {
				fc.utobj().throwsException("was not able to verify subject");
			}
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//a[contains(text () ,'" + subject + "')]"));
			fc.commonMethods().switch_cboxIframe_frameId(driver);

			boolean isStatsPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () ,'Status')]/ancestor::tr/td[contains(text () ,'Work In Progress')]");
			if (isStatsPresent == false) {
				fc.utobj().throwsException("was not able to verify change status");
			}

			isCommentPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () ,'Comments')]/ancestor::tr/td[contains(text () ,'Test Comments Work In Progress')]");
			if (isCommentPresent == false) {
				fc.utobj().throwsException("was not able to verify change comments");
			}
			fc.utobj().clickElement(driver, pobj.closeBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "crm", "TC_Contact_CRM_TaskButton" })
	@TestCase(createdOn = "2018-01-17", updatedOn = "2018-01-17", testCaseId = "TC_Contact_CRM_TaskButton", testCaseDescription = "To verify that the Modify, Delete and Process button.")
	private void crmInfoFillAreaTask_3() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		Map<String, String> dataSetCustom = null;
		String testCaseIdInternal = "" + testCaseId;
		Map<String, String> printInfo = null;
		try {
			driver = fc.loginpage().login(driver);
			CRMLeadsPage pobj = new CRMLeadsPage(driver);

			fc.utobj().printTestStep("Navigate To Admin Users Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			String emailId = "crmautomation@staffex.com";
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUser = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporateUser.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			fc.utobj().printTestStep("Navigate To CRM > Contact > Contact  Summary");
			fc.utobj().printTestStep("Add Contact");
			fc.crm().crm_common().CRMContactsLnk(driver);
			fc.utobj().clickElement(driver, pobj.addNew);
			fc.utobj().clickElement(driver, pobj.contactLnk);

			testCaseIdInternal = "TC_CRM_QA_Contact_Details";
			dataSetCustom = fc.utobj().readTestData("crm", testCaseIdInternal);
			String firstName = fc.utobj().generateTestData("Aa");
			String lastName = fc.utobj().generateTestData("Bb");
			dataSetCustom.put("contactFirstName", firstName);
			dataSetCustom.put("contactLastName", lastName);
			dataSetCustom.put("contactType", "Contacts");
			dataSetCustom.put("cmLeadTypeID", "Select");
			dataSetCustom.put("cmSource2ID", "Internet");
			dataSetCustom.put("cmSource3ID", "BISON");
			dataSetCustom.put("accountName", "");
			dataSetCustom.put("contactOwnerID2", corpUser.getuserFullName());

			printInfo = fillFormDataWithDataset(driver, testCaseIdInternal, dataSetCustom);
			// Reporter.log("Map == for ="+testCaseIdInternal+"==="+printInfo);

			if (printInfo != null && printInfo.size() > 0) {
				// System.out.println("printInfo=First="+printInfo);

				// System.out.println("printInfo=First Email
				// ="+printInfo.get("emailIds"));
			}

			fc.utobj().clickElement(driver, pobj.contactsLinks);

			systemExactSearch(driver, firstName + " " + lastName);

			fc.utobj().printTestStep("Verify Contact Name in Contact Summary.");
			boolean isLeadNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains( text(),'" + firstName + " " + lastName + "')]");
			if (isLeadNamePresent == false) {
				fc.utobj().throwsException("was not able to verify First Contact Name");
			}

			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//a[contains(text () ,'" + firstName + " " + lastName + "')]"));

			try {
				fc.utobj().moveToElement(driver, fc.utobj().getElementByXpath(driver,
						".//*[@class='pvs_hdr2_new']/a[contains(text (),'Log a Task')]"));
			} catch (Exception e) {
				fc.utobj().throwsException(
						"was not able to verify the Log a Task button is under the Task history section.");
			}

			fc.utobj().printTestStep("Log A Task");
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//*[@class='pvs_hdr2_new']/a[contains(text (),'Log a Task')]"));
			// fc.utobj().actionImgOption(driver, firstName + " " + lastName,
			// "Log a Task");

			fc.commonMethods().switch_cboxIframe_frameId(driver);
			if (!fc.utobj().isSelected(driver,pobj.radioOwner)) {
				fc.utobj().clickElement(driver, pobj.radioOwner);
			}
			String statusTask = "Not Started";
			fc.utobj().selectDropDown(driver, pobj.statusTask, statusTask);
			String subject = "Task history section TestCase";
			fc.utobj().sendKeys(driver, pobj.subject, subject);
			if (!fc.utobj().isSelected(driver, pobj.timeLessTask)) {
				fc.utobj().clickElement(driver, pobj.timeLessTask);
			}
			String priority = "Medium";
			fc.utobj().selectDropDown(driver, pobj.priority, priority);
			String comment = "To verify Task history section.";
			fc.utobj().sendKeys(driver, pobj.comments, comment);
			fc.utobj().clickElement(driver, pobj.createBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().clickElement(driver, pobj.contactsLinks);

			systemExactSearch(driver, firstName + " " + lastName);

			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//a[contains(text () ,'" + firstName + " " + lastName + "')]"));

			fc.utobj().printTestStep("Verify Open Task");

			boolean isSubjectPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[contains(text () ,'" + subject + "')]");
			if (isSubjectPresent == false) {
				fc.utobj().throwsException("was not able to verify subject");
			}
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//a[contains(text () ,'" + subject + "')]"));
			fc.commonMethods().switch_cboxIframe_frameId(driver);

			boolean isCommentPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () ,'Comments')]/ancestor::tr/td[contains(text () ,'" + comment + "')]");
			if (isCommentPresent == false) {
				fc.utobj().throwsException("was not able to verify comments");
			}
			fc.utobj().clickElement(driver, pobj.closeBtn);
			fc.utobj().switchFrameToDefault(driver);

			// details History
			fc.utobj().printTestStep("Verify Task At Detailed History Frame");
			fc.utobj().clickElement(driver, pobj.detailedHistoryLink);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().clickElement(driver, pobj.openTaskTab);
			fc.utobj().isTextDisplayed(driver, subject,
					"was not able to verify subject of task at deatiled history pop up");
			fc.utobj().isTextDisplayed(driver, comment,
					"was not able to verify comments of task at deatiled history pop up");
			fc.utobj().clickElement(driver, pobj.closeBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().clickElement(driver, pobj.contactsLinks);
			systemExactSearch(driver, firstName + " " + lastName);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//a[contains(text () ,'" + firstName + " " + lastName + "')]"));
			isSubjectPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[contains(text () ,'" + subject + "')]");
			if (isSubjectPresent == false) {
				fc.utobj().throwsException("was not able to verify subject");
			}
			fc.utobj().printTestStep("Verify To Modify, Delete and Process option");

			try {
				actionImgOptionOnlyShow(driver, subject, "Modify");
				fc.utobj().printTestStep("Verified Modify Action Menu");
			} catch (Exception e) {
				fc.utobj().throwsException("was not able to verify Modify option");
			}
			try {
				actionImgOptionOnlyShow(driver, subject, "Delete");
				fc.utobj().printTestStep("Verified Delete option");
			} catch (Exception e) {
				fc.utobj().throwsException("was not able to verify Delete option");
			}
			try {
				actionImgOptionOnlyShow(driver, subject, "Process");
				fc.utobj().printTestStep("Verified Process option");
			} catch (Exception e) {
				fc.utobj().throwsException("was not able to verify Process option");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "crm", "TC_Contact_CRM_TaskPrintClose" })
	@TestCase(createdOn = "2018-01-17", updatedOn = "2018-01-17", testCaseId = "TC_Contact_CRM_TaskPrintClose", testCaseDescription = "To verify that the Print and Close button on the Task Details pop-up.")
	private void crmInfoFillAreaTask_4() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		Map<String, String> dataSetCustom = null;
		String testCaseIdInternal = "" + testCaseId;
		Map<String, String> printInfo = null;
		try {
			driver = fc.loginpage().login(driver);
			CRMLeadsPage pobj = new CRMLeadsPage(driver);

			fc.utobj().printTestStep("Navigate To Admin Users Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			String emailId = "crmautomation@staffex.com";
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUser = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporateUser.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			fc.utobj().printTestStep("Navigate To CRM > Contact > Contact  Summary");
			fc.utobj().printTestStep("Add Contact");
			fc.crm().crm_common().CRMContactsLnk(driver);
			fc.utobj().clickElement(driver, pobj.addNew);
			fc.utobj().clickElement(driver, pobj.contactLnk);

			testCaseIdInternal = "TC_CRM_QA_Contact_Details";
			dataSetCustom = fc.utobj().readTestData("crm", testCaseIdInternal);
			String firstName = fc.utobj().generateTestData("Aa");
			String lastName = fc.utobj().generateTestData("Bb");
			dataSetCustom.put("contactFirstName", firstName);
			dataSetCustom.put("contactLastName", lastName);
			dataSetCustom.put("contactType", "Contacts");
			dataSetCustom.put("cmLeadTypeID", "Select");
			dataSetCustom.put("cmSource2ID", "Internet");
			dataSetCustom.put("cmSource3ID", "BISON");
			dataSetCustom.put("accountName", "");
			dataSetCustom.put("contactOwnerID2", corpUser.getuserFullName());

			printInfo = fillFormDataWithDataset(driver, testCaseIdInternal, dataSetCustom);

			if (printInfo != null && printInfo.size() > 0) {
			}

			fc.utobj().clickElement(driver, pobj.contactsLinks);

			systemExactSearch(driver, firstName + " " + lastName);

			fc.utobj().printTestStep("Verify Contact Name in Contact Summary.");
			boolean isLeadNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains( text(),'" + firstName + " " + lastName + "')]");
			if (isLeadNamePresent == false) {
				fc.utobj().throwsException("was not able to verify First Contact Name");
			}

			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//a[contains(text () ,'" + firstName + " " + lastName + "')]"));

			try {
				fc.utobj().moveToElement(driver, fc.utobj().getElementByXpath(driver,
						".//*[@class='pvs_hdr2_new']/a[contains(text (),'Log a Task')]"));
			} catch (Exception e) {
				fc.utobj().throwsException(
						"was not able to verify the Log a Task button is under the Task history section.");
			}

			fc.utobj().printTestStep("Log A Task");
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//*[@class='pvs_hdr2_new']/a[contains(text (),'Log a Task')]"));
			// fc.utobj().actionImgOption(driver, firstName + " " + lastName,
			// "Log a Task");

			fc.commonMethods().switch_cboxIframe_frameId(driver);
			if (!fc.utobj().isSelected(driver,pobj.radioOwner)) {
				fc.utobj().clickElement(driver, pobj.radioOwner);
			}
			String statusTask = "Not Started";
			fc.utobj().selectDropDown(driver, pobj.statusTask, statusTask);
			String subject = "Task history section TestCase";
			fc.utobj().sendKeys(driver, pobj.subject, subject);
			if (!fc.utobj().isSelected(driver, pobj.timeLessTask)) {
				fc.utobj().clickElement(driver, pobj.timeLessTask);
			}
			String priority = "Medium";
			fc.utobj().selectDropDown(driver, pobj.priority, priority);
			String comment = "To verify Task history section.";
			fc.utobj().sendKeys(driver, pobj.comments, comment);
			fc.utobj().clickElement(driver, pobj.createBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().clickElement(driver, pobj.contactsLinks);

			systemExactSearch(driver, firstName + " " + lastName);

			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//a[contains(text () ,'" + firstName + " " + lastName + "')]"));

			fc.utobj().printTestStep("Verify Open Task");

			boolean isSubjectPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[contains(text () ,'" + subject + "')]");
			if (isSubjectPresent == false) {
				fc.utobj().throwsException("was not able to verify subject");
			}
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//a[contains(text () ,'" + subject + "')]"));
			fc.commonMethods().switch_cboxIframe_frameId(driver);

			boolean isCommentPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () ,'Comments')]/ancestor::tr/td[contains(text () ,'" + comment + "')]");
			if (isCommentPresent == false) {
				fc.utobj().throwsException("was not able to verify comments");
			}

			fc.utobj().printTestStep("Verify To Print, Close Button.");

			try {
				fc.utobj().moveToElement(driver, pobj.processTask);
				fc.utobj().printTestStep("Verified Print Button at Pop-up");
			} catch (Exception e) {
				fc.utobj().throwsException("was not able to verify Print option");
			}
			try {
				fc.utobj().moveToElement(driver, pobj.closeBtn);
				fc.utobj().printTestStep("Verified Close Button at Pop-up");
			} catch (Exception e) {
				fc.utobj().throwsException("was not able to verify Close option");
			}

			fc.utobj().clickElement(driver, pobj.closeBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "crm", "TC_Contact_CRM_Delete_Task_Association"})
	@TestCase(createdOn = "2018-01-17", updatedOn = "2018-01-17", testCaseId = "TC_Contact_CRM_Delete_Task_Association", testCaseDescription = "To verify that the Contact associated with some task is not getting Deleted.")
	private void crmInfoFillAreaTask_5() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		Map<String, String> dataSetCustom = null;
		String testCaseIdInternal = "" + testCaseId;
		Map<String, String> printInfo = null;
		try {
			driver = fc.loginpage().login(driver);
			CRMLeadsPage pobj = new CRMLeadsPage(driver);

			fc.utobj().printTestStep("Navigate To Admin Users Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			String emailId = "crmautomation@staffex.com";
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUser = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporateUser.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			fc.utobj().printTestStep("Navigate To CRM > Contact > Contact  Summary");
			fc.utobj().printTestStep("Add First Lead");
			fc.crm().crm_common().CRMContactsLnk(driver);
			fc.utobj().clickElement(driver, pobj.addNew);
			fc.utobj().clickElement(driver, pobj.contactLnk);

			testCaseIdInternal = "TC_CRM_QA_Contact_Details";
			dataSetCustom = fc.utobj().readTestData("crm", testCaseIdInternal);
			String firstName = fc.utobj().generateTestData("Aa");
			String lastName = fc.utobj().generateTestData("Bb");
			dataSetCustom.put("contactFirstName", firstName);
			dataSetCustom.put("contactLastName", lastName);
			dataSetCustom.put("contactType", "Contacts");
			dataSetCustom.put("cmLeadTypeID", "Select");
			dataSetCustom.put("cmSource2ID", "Internet");
			dataSetCustom.put("cmSource3ID", "BISON");
			dataSetCustom.put("accountName", "");
			dataSetCustom.put("contactOwnerID2", corpUser.getuserFullName());

			printInfo = fillFormDataWithDataset(driver, testCaseIdInternal, dataSetCustom);
			// Reporter.log("Map == for ="+testCaseIdInternal+"==="+printInfo);

			if (printInfo != null && printInfo.size() > 0) {
				// System.out.println("printInfo=First="+printInfo);

				// System.out.println("printInfo=First Email
				// ="+printInfo.get("emailIds"));
			}

			fc.utobj().clickElement(driver, pobj.contactsLinks);

			systemExactSearch(driver, firstName + " " + lastName);

			fc.utobj().printTestStep("Verify Contact Name in Contact Summary.");
			boolean isLeadNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains( text(),'" + firstName + " " + lastName + "')]");
			if (isLeadNamePresent == false) {
				fc.utobj().throwsException("was not able to verify Contact Name");
			}

			fc.utobj().printTestStep("Log A Task");
			fc.utobj().actionImgOption(driver, firstName + " " + lastName, "Log a Task");

			fc.commonMethods().switch_cboxIframe_frameId(driver);
			if (!fc.utobj().isSelected(driver,pobj.radioOwner)) {
				fc.utobj().clickElement(driver, pobj.radioOwner);
			}
			String statusTask = "Not Started";
			fc.utobj().selectDropDown(driver, pobj.statusTask, statusTask);
			String subject = "Task for Delete TestCase";
			fc.utobj().sendKeys(driver, pobj.subject, subject);
			if (!fc.utobj().isSelected(driver, pobj.timeLessTask)) {
				fc.utobj().clickElement(driver, pobj.timeLessTask);
			}
			String priority = "Medium";
			fc.utobj().selectDropDown(driver, pobj.priority, priority);
			String comment = "To verify Lead Delete Cases If task is associated with any contact.";
			fc.utobj().sendKeys(driver, pobj.comments, comment);
			fc.utobj().clickElement(driver, pobj.createBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().clickElement(driver, pobj.contactsLinks);

			systemExactSearch(driver, firstName + " " + lastName);

			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//a[contains(text () ,'" + firstName + " " + lastName + "')]"));

			fc.utobj().printTestStep("Verify Task");

			boolean isSubjectPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[contains(text () ,'" + subject + "')]");
			if (isSubjectPresent == false) {
				fc.utobj().throwsException("was not able to verify Task subject ");
			}

			fc.utobj().clickElement(driver, pobj.contactsLinks);
			searchLeadByOwner(driver, corpUser.getuserFullName(), "Contacts");

			fc.utobj().printTestStep("Verify Delete Contact");
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//a[contains(text () ,'" + firstName
					+ " " + lastName + "')]/ancestor::tr/td/input[@name='checkb']"));
			fc.utobj().selectActionMenuItemsWithTagA(driver, "Delete");

			//String alertText = fc.utobj().acceptAlertBox(driver);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().sleep(2000);
			String closeframe=driver.findElement(By.xpath("//p[contains(text(),'Following contact(s) cannot be deleted as a Task / Opportunity is associated with it :')]")).getText();
			fc.commonMethods().Click_Close_Input_ByValue(driver);
			fc.utobj().sleep(2000);
			fc.utobj().switchFrameToDefault(driver);
			
			if (closeframe.contains("Following contact(s) cannot be deleted as a Task / Opportunity is associated with it :")) {
				fc.utobj().printTestStep(
						"Verify Alert Text You cannot Contact leads as they have Task associated with them.");
			} else {
				fc.utobj().throwsException(
						"was not able to verify Alert Text You cannot delete Contact as they have Task associated with them.");
			}
			
			/*if (alertText.indexOf("associated") != -1) {
				fc.utobj().printTestStep(
						"Verify Alert Text You cannot Contact leads as they have Task associated with them.");
			} else {
				fc.utobj().throwsException(
						"was not able to verify Alert Text You cannot delete Contact as they have Task associated with them.");
			}*/

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "crm", "TC_Contact_CRM_TaskMandatoryVal"})
	@TestCase(createdOn = "2018-01-17", updatedOn = "2018-01-17", testCaseId = "TC_Contact_CRM_TaskMandatoryVal", testCaseDescription = "To verify that the mandatory field and validation alerts are appearing properly on the Log a task page.")
	private void crmInfoFillAreaTask_7() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		Map<String, String> dataSetCustom = null;
		String testCaseIdInternal = "" + testCaseId;
		Map<String, String> printInfo = null;
		try {
			driver = fc.loginpage().login(driver);
			CRMLeadsPage pobj = new CRMLeadsPage(driver);

			fc.utobj().printTestStep("Navigate To Admin Users Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			String emailId = "crmautomation@staffex.com";
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUser = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporateUser.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			fc.utobj().printTestStep("Navigate To CRM > Contact > Contact  Summary");
			fc.utobj().printTestStep("Add Contact");
			fc.crm().crm_common().CRMContactsLnk(driver);
			fc.utobj().clickElement(driver, pobj.addNew);
			fc.utobj().clickElement(driver, pobj.contactLnk);

			testCaseIdInternal = "TC_CRM_QA_Contact_Details";
			dataSetCustom = fc.utobj().readTestData("crm", testCaseIdInternal);
			String firstName = fc.utobj().generateTestData("Aa");
			String lastName = fc.utobj().generateTestData("Bb");
			dataSetCustom.put("contactFirstName", firstName);
			dataSetCustom.put("contactLastName", lastName);
			dataSetCustom.put("contactType", "Contacts");
			dataSetCustom.put("cmLeadTypeID", "Select");
			dataSetCustom.put("cmSource2ID", "Internet");
			dataSetCustom.put("cmSource3ID", "BISON");
			dataSetCustom.put("accountName", "");
			dataSetCustom.put("contactOwnerID2", corpUser.getuserFullName());

			printInfo = fillFormDataWithDataset(driver, testCaseIdInternal, dataSetCustom);

			if (printInfo != null && printInfo.size() > 0) {
			}

			fc.utobj().clickElement(driver, pobj.contactsLinks);

			systemExactSearch(driver, firstName + " " + lastName);

			fc.utobj().printTestStep("Verify Contact Name in Contact Summary.");
			boolean isLeadNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains( text(),'" + firstName + " " + lastName + "')]");
			if (isLeadNamePresent == false) {
				fc.utobj().throwsException("was not able to verify First Contact Name");
			}

			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//a[contains(text () ,'" + firstName + " " + lastName + "')]"));

			try {
				fc.utobj().moveToElement(driver, fc.utobj().getElementByXpath(driver,
						".//*[@class='pvs_hdr2_new']/a[contains(text (),'Log a Task')]"));
			} catch (Exception e) {
				fc.utobj().throwsException(
						"was not able to verify the Log a Task button is under the Task history section.");
			}

			fc.utobj().printTestStep("Log A Task");
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//*[@class='pvs_hdr2_new']/a[contains(text (),'Log a Task')]"));
			// fc.utobj().actionImgOption(driver, firstName + " " + lastName,
			// "Log a Task");

			fc.commonMethods().switch_cboxIframe_frameId(driver);

			try {
				fc.utobj().printTestStep("verify that the mandatory field and validation alerts");
				fc.utobj().clickElement(driver, pobj.createBtn);
				fc.utobj().acceptAlertBox(driver);

			} catch (Exception e) {

			}

			if (!fc.utobj().isSelected(driver,pobj.radioOwner)) {
				fc.utobj().clickElement(driver, pobj.radioOwner);
			}
			String statusTask = "Not Started";
			fc.utobj().selectDropDown(driver, pobj.statusTask, statusTask);
			String subject = "Task history section TestCase";
			fc.utobj().sendKeys(driver, pobj.subject, subject);
			if (!fc.utobj().isSelected(driver, pobj.timeLessTask)) {
				fc.utobj().clickElement(driver, pobj.timeLessTask);
			}
			String priority = "Medium";
			fc.utobj().selectDropDown(driver, pobj.priority, priority);
			String comment = "To verify Task history section.";
			fc.utobj().sendKeys(driver, pobj.comments, comment);
			fc.utobj().clickElement(driver, pobj.createBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().clickElement(driver, pobj.contactsLinks);

			systemExactSearch(driver, firstName + " " + lastName);

			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//a[contains(text () ,'" + firstName + " " + lastName + "')]"));

			fc.utobj().printTestStep("Verify Open Task");

			boolean isSubjectPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[contains(text () ,'" + subject + "')]");
			if (isSubjectPresent == false) {
				fc.utobj().throwsException("was not able to verify subject");
			}
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//a[contains(text () ,'" + subject + "')]"));
			fc.commonMethods().switch_cboxIframe_frameId(driver);

			boolean isCommentPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () ,'Comments')]/ancestor::tr/td[contains(text () ,'" + comment + "')]");
			if (isCommentPresent == false) {
				fc.utobj().throwsException("was not able to verify comments");
			}

			fc.utobj().printTestStep("Verify To Print, Close Button.");

			try {
				fc.utobj().moveToElement(driver, pobj.processTask);
				fc.utobj().printTestStep("Verified Print Button at Pop-up");
			} catch (Exception e) {
				fc.utobj().throwsException("was not able to verify Print option");
			}
			try {
				fc.utobj().moveToElement(driver, pobj.closeBtn);
				fc.utobj().printTestStep("Verified Close Button at Pop-up");
			} catch (Exception e) {
				fc.utobj().throwsException("was not able to verify Close option");
			}

			fc.utobj().clickElement(driver, pobj.closeBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().clickElement(driver, pobj.contactsLinks);
			systemExactSearch(driver, firstName + " " + lastName);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//a[contains(text () ,'" + firstName + " " + lastName + "')]"));
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//a[contains(text (),'Modify')]"));

			try {
				fc.utobj().printTestStep(
						"verify that the There are some pending tasks associated with selected Lead(s), you cannot change the Assign To..");
				WebElement taskassocation = fc.utobj().getElementByXpath(driver, ".//*[@id='inddiv2']");
				String alertText = taskassocation.getText();

				if (alertText != null && alertText.indexOf("some pending tasks") != -1) {

				} else {
					fc.utobj().throwsException(
							"was not able to verify some pending tasks associated with selected Lead(s)");
				}
			} catch (Exception e) {

			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "crm", "TC_Contact_CRM_TaskCreateAnotherbutton" })
	@TestCase(createdOn = "2018-01-17", updatedOn = "2018-01-17", testCaseId = "TC_Contact_CRM_TaskCreateAnotherbutton", testCaseDescription = "To verify that upon clicking on Create and Create Another button a task is getting added.")
	private void crmInfoFillAreaTask_8() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		Map<String, String> dataSetCustom = null;
		String testCaseIdInternal = "" + testCaseId;
		Map<String, String> printInfo = null;
		try {
			driver = fc.loginpage().login(driver);
			CRMLeadsPage pobj = new CRMLeadsPage(driver);

			fc.utobj().printTestStep("Navigate To Admin Users Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			String emailId = "crmautomation@staffex.com";
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUser = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporateUser.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			fc.utobj().printTestStep("Navigate To CRM > Contact > Contact  Summary");
			fc.utobj().printTestStep("Add Contact");
			fc.crm().crm_common().CRMContactsLnk(driver);
			fc.utobj().clickElement(driver, pobj.addNew);
			fc.utobj().clickElement(driver, pobj.contactLnk);

			testCaseIdInternal = "TC_CRM_QA_Contact_Details";
			dataSetCustom = fc.utobj().readTestData("crm", testCaseIdInternal);
			String firstName = fc.utobj().generateTestData("Aa");
			String lastName = fc.utobj().generateTestData("Bb");
			dataSetCustom.put("contactFirstName", firstName);
			dataSetCustom.put("contactLastName", lastName);
			dataSetCustom.put("contactType", "Contacts");
			dataSetCustom.put("cmLeadTypeID", "Select");
			dataSetCustom.put("cmSource2ID", "Internet");
			dataSetCustom.put("cmSource3ID", "BISON");
			dataSetCustom.put("accountName", "");
			dataSetCustom.put("contactOwnerID2", corpUser.getuserFullName());

			printInfo = fillFormDataWithDataset(driver, testCaseIdInternal, dataSetCustom);

			if (printInfo != null && printInfo.size() > 0) {
			}

			fc.utobj().clickElement(driver, pobj.contactsLinks);

			systemExactSearch(driver, firstName + " " + lastName);

			fc.utobj().printTestStep("Verify Contact Name in Contact Summary.");
			boolean isLeadNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains( text(),'" + firstName + " " + lastName + "')]");
			if (isLeadNamePresent == false) {
				fc.utobj().throwsException("was not able to verify First Contact Name");
			}

			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//a[contains(text () ,'" + firstName + " " + lastName + "')]"));

			try {
				fc.utobj().moveToElement(driver, fc.utobj().getElementByXpath(driver,
						".//*[@class='pvs_hdr2_new']/a[contains(text (),'Log a Task')]"));
			} catch (Exception e) {
				fc.utobj().throwsException(
						"was not able to verify the Log a Task button is under the Task history section.");
			}

			fc.utobj().printTestStep("Create and Create Another Log A Task");
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//*[@class='pvs_hdr2_new']/a[contains(text (),'Log a Task')]"));
			// fc.utobj().actionImgOption(driver, firstName + " " + lastName,
			// "Log a Task");

			fc.commonMethods().switch_cboxIframe_frameId(driver);

			if (!fc.utobj().isSelected(driver,pobj.radioOwner)) {
				fc.utobj().clickElement(driver, pobj.radioOwner);
			}
			String statusTask = "Not Started";
			fc.utobj().selectDropDown(driver, pobj.statusTask, statusTask);
			String subject = "Task history section TestCase";
			fc.utobj().sendKeys(driver, pobj.subject, subject);
			if (!fc.utobj().isSelected(driver, pobj.timeLessTask)) {
				fc.utobj().clickElement(driver, pobj.timeLessTask);
			}
			String priority = "Medium";
			fc.utobj().selectDropDown(driver, pobj.priority, priority);
			String comment = "To verify Task history section.";
			fc.utobj().sendKeys(driver, pobj.comments, comment);
			fc.utobj().clickElement(driver, pobj.another);

			try {
				fc.utobj().clickElement(driver, pobj.closeBtn);
			} catch (Exception e) {
				fc.utobj().throwsException("was not able to verify the Create and Create Another.");
			}

			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().clickElement(driver, pobj.contactsLinks);

			systemExactSearch(driver, firstName + " " + lastName);

			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//a[contains(text () ,'" + firstName + " " + lastName + "')]"));

			fc.utobj().printTestStep("Verify Open Task");

			boolean isSubjectPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[contains(text () ,'" + subject + "')]");
			if (isSubjectPresent == false) {
				fc.utobj().throwsException("was not able to verify subject");
			}
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//a[contains(text () ,'" + subject + "')]"));
			fc.commonMethods().switch_cboxIframe_frameId(driver);

			boolean isCommentPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () ,'Comments')]/ancestor::tr/td[contains(text () ,'" + comment + "')]");
			if (isCommentPresent == false) {
				fc.utobj().throwsException("was not able to verify comments");
			}

			fc.utobj().printTestStep("Verify To Print, Close Button.");

			try {
				fc.utobj().moveToElement(driver, pobj.processTask);
				fc.utobj().printTestStep("Verified Print Button at Pop-up");
			} catch (Exception e) {
				fc.utobj().throwsException("was not able to verify Print option");
			}
			try {
				fc.utobj().moveToElement(driver, pobj.closeBtn);
				fc.utobj().printTestStep("Verified Close Button at Pop-up");
			} catch (Exception e) {
				fc.utobj().throwsException("was not able to verify Close option");
			}

			fc.utobj().clickElement(driver, pobj.closeBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "crm", "crmemail", "TC_Contact_CRM_Task_Email"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Add Task For Contact At CRM > Tasks And Verify Task Creation Mail", testCaseId = "TC_Contact_CRM_Task_Email")
	public void addTaskContact() throws IOException, Exception, ParserConfigurationException, SAXException {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		Map<String, String> dataSetCustom = null;
		String testCaseIdInternal = "" + testCaseId;
		Map<String, String> printInfo = null;
		try {
			driver = fc.loginpage().login(driver);
			CRMLeadsPageTest leadPage = new CRMLeadsPageTest();

			CRMTasksPage pobj = new CRMTasksPage(driver);

			fc.utobj().printTestStep("Navigate To Admin Users Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			String emailId = "crmautomation@staffex.com";
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUser = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporateUser.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			fc.utobj().printTestStep("Navigate To CRM > Contact > Contact  Summary");
			fc.utobj().printTestStep("Add Contact");
			fc.crm().crm_common().CRMContactsLnk(driver);
			fc.utobj().clickElement(driver, pobj.addNew);
			fc.utobj().clickElement(driver, pobj.contactLnk);

			testCaseIdInternal = "TC_CRM_QA_Contact_Details";
			dataSetCustom = fc.utobj().readTestData("crm", testCaseIdInternal);
			String firstName = fc.utobj().generateTestData("Aa");
			String lastName = fc.utobj().generateTestData("Bb");
			dataSetCustom.put("contactFirstName", firstName);
			dataSetCustom.put("contactLastName", lastName);
			dataSetCustom.put("contactType", "Contacts");
			dataSetCustom.put("cmLeadTypeID", "Select");
			dataSetCustom.put("cmSource2ID", "Internet");
			dataSetCustom.put("cmSource3ID", "BISON");
			dataSetCustom.put("accountName", "");
			dataSetCustom.put("contactOwnerID2", corpUser.getuserFullName());

			printInfo = fillFormDataWithDataset(driver, testCaseIdInternal, dataSetCustom);

			if (printInfo != null && printInfo.size() > 0) {
			}

			fc.utobj().printTestStep("Navigate To CRM > Tasks");
			fc.utobj().printTestStep("Add Task");
			fc.utobj().clickElement(driver, pobj.taskLinks);
			fc.utobj().clickElement(driver, pobj.addTaskLink);
			fc.utobj().sleep(1000);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().sleep(500);
			if (!fc.utobj().isSelected(driver, pobj.associateWithContact)) {
				fc.utobj().clickElement(driver, pobj.associateWithContact);
			}

			String leadName = firstName + " " + lastName;

			fc.utobj().sendKeys(driver, pobj.contactSearch, leadName);
			fc.utobj().sleep(1000);
			/*fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//span/u[contains(text () ,'" + leadName + "')]"));*/
			
			JavascriptExecutor js = (JavascriptExecutor)driver;
			 WebElement button =driver.findElement(By.xpath(".//span/u[.='" + leadName + "']"));
			 js.executeScript("arguments[0].click();", button);

			if (!fc.utobj().isSelected(driver,pobj.radioOwner)) {
				fc.utobj().clickElement(driver, pobj.radioOwner);
			}
			String statusTask = "Not Started";
			fc.utobj().selectDropDown(driver, pobj.statusTask, statusTask);
			String subject = fc.utobj().generateTestData("Tsk");
			fc.utobj().sendKeys(driver, pobj.subject, subject);
			if (!fc.utobj().isSelected(driver, pobj.timeLessTask)) {
				fc.utobj().clickElement(driver, pobj.timeLessTask);
			}
			String priority = "High";
			fc.utobj().selectDropDown(driver, pobj.priority, priority);
			fc.utobj().sendKeys(driver, pobj.comments, "Test Comment for High Priporty Task");
			fc.utobj().clickElement(driver, pobj.createBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Verify Task Detail");
			searchBySubjectFilter(driver, subject);

			boolean isSubjectPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[.='" + subject + "']");
			if (isSubjectPresent == false) {
				fc.utobj().throwsException("Was not able to verify Subject Of Task");
			}

			boolean isAssociatedWithPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[.='" + subject + "']/ancestor::tr/td/a[contains(text () ,'" + leadName + "')]");
			if (isAssociatedWithPresent == false) {
				fc.utobj().throwsException("was not able to verify Associated With");
			}

			boolean isAssignedTo = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[.='" + subject
					+ "']/ancestor::tr/td[contains(text () ,'" + corpUser.getuserFullName() + "')]");
			if (isAssignedTo == false) {
				fc.utobj().throwsException("was not able to verify Assign To");
			}

			fc.utobj().printTestStep("Verify The Task Detail At Contact Info Page");
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,(".//a[.='" + subject + "']/ancestor::tr/td/a[contains(text () ,'" + leadName + "')]")));

			boolean isAssignedToAtLeadInfo = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[.='" + subject
					+ "']/ancestor::tr/td/div[contains(text () ,'" + corpUser.getuserFullName() + "')]");
			if (isAssignedToAtLeadInfo == false) {
				fc.utobj().throwsException("was not able to verify Assign To At Contact Info Page");
			}

			fc.utobj().printTestStep("Verify Task Creation Mail");

			Map<String, String> mailData = new HashMap<String, String>();

			mailData = fc.utobj().readMailBox("New Task Added", subject, emailId, "sdg@1a@Hfs");

			if (mailData.size() == 0 || !mailData.get("mailBody").contains(firstName)) {

				fc.utobj().throwsException("was not able Verify Task Creation Mail");
			}

			if (mailData.size() == 0 || !mailData.get("mailBody").contains("Task Addition Notification")) {

				fc.utobj().throwsException("was not able Verify Task Creation Mail Info");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	public void searchBySubjectFilter(WebDriver driver, String subject) throws Exception {
		CRMTasksPage pobj = new CRMTasksPage(driver);
		fc.utobj().clickElement(driver, pobj.showFilter);
		fc.utobj().sendKeys(driver, pobj.subjectFilter, subject);
		fc.utobj().setToDefault(driver, pobj.taskStatusFilter);
		fc.utobj().sendKeys(driver, pobj.startdateFilter, "");
		fc.utobj().sendKeys(driver, pobj.endDate, "");
		fc.utobj().selectDropDown(driver, pobj.reminderFilter, "Select");
		fc.utobj().setToDefault(driver, pobj.prioprityFilter);
		fc.utobj().setToDefault(driver, pobj.viewMineFilter);
		fc.utobj().setToDefault(driver, pobj.associatedWithFilter);
		fc.utobj().clickElement(driver, pobj.searchBtnFilter);
		fc.utobj().clickElement(driver, pobj.hideFilters);
	}

	public void actionImgOption(WebDriver driver, String task, String option) throws Exception {

		fc.utobj().moveToElement(driver,
				fc.utobj().getElementByXpath(driver, ".//*[contains(text () ,'" + task + "')]/ancestor::tr/td/layer"));
		String alterText = fc.utobj()
				.getElementByXpath(driver, ".//*[contains(text () ,'" + task + "')]/ancestor::tr/td/layer")
				.getAttribute("id").trim();
		alterText = alterText.replace("Actions_dynamicmenu", "");
		alterText = alterText.replace("Bar", "");

		fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
				".//*[contains(text () ,'" + task + "')]/ancestor::tr/td/layer/a/img"));
		// fc.utobj().clickElement(driver,
		// fc.utobj().getElementByXpath(driver,".//div[@id='Actions_dynamicmenu"+alterText+"Menu']/span[contains(text
		// () , '"+option+"')]")));
		fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
				".//div[@id='TaskActions_dynamicmenu" + alterText + "Menu']/*[contains(text () , '" + option + "')]"));
	}

	public void actionImgOptionOnlyShow(WebDriver driver, String task, String option) throws Exception {

		fc.utobj().moveToElement(driver,
				fc.utobj().getElementByXpath(driver, ".//*[contains(text () ,'" + task + "')]/ancestor::tr/td/layer"));
		String alterText = fc.utobj()
				.getElementByXpath(driver, ".//*[contains(text () ,'" + task + "')]/ancestor::tr/td/layer")
				.getAttribute("id").trim();
		alterText = alterText.replace("Actions_dynamicmenu", "");
		alterText = alterText.replace("Bar", "");

		fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
				".//*[contains(text () ,'" + task + "')]/ancestor::tr/td/layer/a/img"));
		// fc.utobj().clickElement(driver,
		// fc.utobj().getElementByXpath(driver,".//div[@id='Actions_dynamicmenu"+alterText+"Menu']/span[contains(text
		// () , '"+option+"')]")));
		// fc.utobj().clickElement(driver,
		// fc.utobj().getElementByXpath(driver,".//div[@id='TaskActions_dynamicmenu"+alterText+"Menu']/*[contains(text
		// () , '"+option+"')]")));
	}

	public void searchLeadByOwner(WebDriver driver, String userName, String leadTypeView) throws Exception {

		CRMContactsPage pobj = new CRMContactsPage(driver);
		fc.utobj().clickElement(driver, pobj.showFilter);
		fc.utobj().setToDefault(driver, pobj.contactOwnerSelect);
		try {
			WebElement divsion = fc.utobj().getElementByXpath(driver, ".//*[@id='ms-parentdivisionID']");
			fc.utobj().setToDefault(driver, divsion);
		} catch (Exception e) {
		}
		fc.utobj()
				.selectValFromMultiSelect(driver, pobj.contactOwnerSelect, fc.utobj()
						.getElement(driver, pobj.contactOwnerSelect).findElement(By.xpath("./div/div/input")),
						userName);
		fc.utobj().setToDefault(driver, pobj.franchiseIdSelect);
		fc.utobj().setToDefault(driver, pobj.statusSelect);
		fc.utobj().selectDropDown(driver, pobj.addDateSelect, "All");
		fc.utobj().setToDefault(driver, pobj.emailStatusSelect);
		// fc.utobj().selectDropDown(driver, pobj.viewLeadsSelect,
		// leadTypeView);
		fc.utobj().clickElement(driver, pobj.searchBtn);
		;
		fc.utobj().clickElement(driver, pobj.hideFilter);
	}

	public void searchLeadByOwnerDiffrent(WebDriver driver, String userName, String leadTypeView, List<String> listInfo)
			throws Exception {

		CRMLeadsPage pobj = new CRMLeadsPage(driver);
		fc.utobj().clickElement(driver, pobj.showFilter);
		fc.utobj().setToDefault(driver, pobj.leadOwnerSelect);
		try {
			WebElement divsion = fc.utobj().getElementByXpath(driver, ".//*[@id='ms-parentdivisionID']");
			fc.utobj().setToDefault(driver, divsion);
		} catch (Exception e) {
		}
		fc.utobj().selectMultipleValFromMultiSelect(driver,
				fc.utobj().getElementByXpath(driver, ".//*[@id='ms-parentleadOwner']"), listInfo);
		// fc.utobj().selectValFromMultiSelect(driver,
		// pobj.leadOwnerSelect,fc.utobj().getElement(driver,
		// pobj.leadOwnerSelect).findElement(By.xpath("./div/div/input")),
		// userName);
		fc.utobj().setToDefault(driver, pobj.franchiseSelect);
		fc.utobj().setToDefault(driver, pobj.statusSelect);
		fc.utobj().selectDropDown(driver, pobj.addDateSelect, "All");
		fc.utobj().setToDefault(driver, pobj.emailStatusSelect);
		fc.utobj().selectDropDown(driver, pobj.viewLeadsSelect, leadTypeView);
		fc.utobj().clickElement(driver, pobj.searchBtn);
		;
		fc.utobj().clickElement(driver, pobj.hideFilter);
	}

	// system search
	public void systemExactSearch(WebDriver driver, String leadName) throws Exception {
		// search By System Search Button
		CRMLeadsPage pobj = new CRMLeadsPage(driver);
		fc.utobj().sendKeys(driver, pobj.searchContact, leadName);
		// fc.utobj().clickElement(driver, pobj.exactSearch);
		fc.utobj().clickElement(driver, pobj.systemSearchButton);
	}

	public Map<String, String> fillFormDataWithDataset(WebDriver driver, String testCaseId, Map<String, String> dataSet)
			throws Exception {
		Reporter.log("Filling TestCase Data = " + testCaseId);
		String id = "id";

		List<String> listItems = new ArrayList<String>();
		Map<String, String> printInfo = new HashMap<String, String>();

		List<WebElement> mandateBox = null;
		mandateBox = driver.findElements(By.xpath(
				".//td[@qat_tableid='addData']//*[@type='text' or @class='fTextBoxDate' or @class='fTextBox' or @class='multiList' or @type='radio' or @type='checkbox' or @type='checkBox' or @id='ms-parentsellerCombo' or @class='ms-choice' or @class='form-control']"));

		Map<String, String> alreadyitrateElement = new HashMap<>();
		Map<String, String> alreadyradio = new HashMap<>();
		Map<String, String> alreadycheckBox = new HashMap<>();

		try {
			for (WebElement webElement : mandateBox) {

				if (webElement.getAttribute("" + id) != null && !"".equals(webElement.getAttribute("" + id))
						&& !"null".equals(webElement.getAttribute("" + id))
						&& !":".equals(webElement.getAttribute("" + id))
						&& !"dateOfOpen".equals(webElement.getAttribute("" + id))
						&& !"fimCbOtherComplaintType".equals(webElement.getAttribute("" + id))) {
					/*
					 * try{
					 * System.out.println("Type==="+webElement.getAttribute(
					 * "type")+"====values==="+webElement.getAttribute("value")
					 * +"======"+dataSet.get(""+webElement.getAttribute(""+id))
					 * +"====="+""+webElement.getAttribute(""+id));
					 * }catch(Exception e) {
					 * 
					 * }
					 */

					if (alreadyitrateElement.containsKey(webElement.getAttribute("" + id))) {

					} else {
						if (dataSet.containsKey(webElement.getAttribute("" + id))) {
							WebElement elementmovepostion = null;
							if ("name".equals(id)) {
								elementmovepostion = driver.findElement(By.name("" + webElement.getAttribute("" + id)));

							} else {
								elementmovepostion = fc.utobj().getElementByID(driver,
										"" + webElement.getAttribute("" + id));

							}
							fc.utobj().moveToElement(driver, elementmovepostion);
							if (webElement.getAttribute("type").indexOf("select-one") != -1) {
								if (webElement.getAttribute("" + id) != null
										&& !"cmSource2ID".equals(webElement.getAttribute("" + id))
										&& !"cmSource3ID".equals(webElement.getAttribute("" + id)))

								{
									try {

										Select singledrop = null;
										List<WebElement> singleDropDown = null;
										try {
											singledrop = new Select(fc.utobj().getElementByID(driver,
													"" + webElement.getAttribute("" + id)));
											singleDropDown = driver
													.findElements(By.id("" + webElement.getAttribute("" + id)));
										} catch (Exception e) {
											singledrop = new Select(
													driver.findElement(By.name("" + webElement.getAttribute("" + id))));
											singleDropDown = driver
													.findElements(By.name("" + webElement.getAttribute("" + id)));
										}
										if (webElement.getAttribute("value") == null
												|| "".equals(webElement.getAttribute("value"))
												|| "null".equals(webElement.getAttribute("value"))
												|| "-1".equals(webElement.getAttribute("value"))) {

											int size = singleDropDown.size();
											if (dataSet.get("" + webElement.getAttribute("" + id)) != null
													&& !"".equals(dataSet.get("" + webElement.getAttribute("" + id)))) {
												if (size >= 1) {
													try {
														WebElement dropDownField = driver
																.findElement(By.id("" + webElement.getAttribute("id")));
														// fc.utobj().moveToElement(driver,
														// dropDownField);
														fc.utobj().selectDropDownByVisibleText(driver, dropDownField,
																dataSet.get("" + webElement.getAttribute("" + id)));

														if (!"birthMonth".equals(webElement.getAttribute("" + id))
																&& !"birthDate".equals(webElement.getAttribute("" + id))
																&& !"spouseBirthMonth"
																		.equals(webElement.getAttribute("" + id))
																&& !"spouseBirthDate"
																		.equals(webElement.getAttribute("" + id))) {
															listItems.add(
																	dataSet.get("" + webElement.getAttribute("" + id)));
														}
														printInfo.put("" + webElement.getAttribute("" + id),
																dataSet.get("" + webElement.getAttribute("" + id)));

													} catch (Exception eradio) {
														// Reporter.log("Problem
														// in selecting drop :
														// "+""+webElement.getAttribute("id")
														// +" Values getting
														// from Excel not match
														// in desired
														// fomrat."+""+dataSet.get(""+webElement.getAttribute("id")));
													}
												}
											}
										}

									} catch (Exception e3) {

									}
								}

							} else if (webElement.getAttribute("type").indexOf("select-multiple") != -1) {
								if (webElement.getAttribute("value") == null
										|| "".equals(webElement.getAttribute("value"))
										|| "null".equals(webElement.getAttribute("value"))
										|| "-1".equals(webElement.getAttribute("value"))) {
									try {
										WebElement element = driver
												.findElement(By.id("ms-parent" + webElement.getAttribute("" + id)));
										fc.utobj().clickElement(driver, element);
										fc.utobj().clickElement(driver,
												fc.utobj().getElementByXpath(driver, ".//*[@id='selectAll']"));
										fc.utobj().clickElement(driver, element);
									} catch (Exception emultipal) {
										// Reporter.log("Problem in multiple
										// value drop-down :
										// "+""+webElement.getAttribute("id"));
									}
								}
							} else if (webElement.getAttribute("type").indexOf("textarea") != -1) {
								if (webElement.getAttribute("value") == null
										|| "".equals(webElement.getAttribute("value"))
										|| "null".equals(webElement.getAttribute("value"))) {
									try {
										driver.findElement(By.name("" + webElement.getAttribute("" + id)))
												.sendKeys(dataSet.get("" + webElement.getAttribute("" + id)));
										listItems.add(dataSet.get("" + webElement.getAttribute("" + id)));
										printInfo.put("" + webElement.getAttribute("id"),
												dataSet.get("" + webElement.getAttribute("" + id)));
									} catch (Exception eText) {
										// Reporter.log("Problem in entering
										// TextArea in :
										// "+""+webElement.getAttribute("id"));
									}
								}
							} else if (webElement.getAttribute("type").indexOf("radio") != -1) {
								try {
									String sValue = "";
									List<WebElement> rdBtn_Sex = driver
											.findElements(By.id("" + webElement.getAttribute("" + id))); //
									if (alreadyradio.containsKey(webElement.getAttribute("" + id))) {

									} else {

										int size = rdBtn_Sex.size();
										if (size > 1) {
											sValue = dataSet.get("" + webElement.getAttribute("" + id));
										}
										try {
											List<WebElement> rdBtn_Field = null;
											WebElement element = null;
											try {
												rdBtn_Field = driver.findElements(By.xpath(
														".//*[@name='" + webElement.getAttribute("" + id) + "']")); // work
												element = fc.utobj().getElementByXpath(driver,
														".//*[@name='" + webElement.getAttribute("" + id) + "']");
											} catch (Exception eTxt) {
												rdBtn_Field = driver.findElements(
														By.xpath(".//*[@id='" + webElement.getAttribute("id") + "']")); // work
												element = driver.findElement(
														By.xpath(".//*[@id='" + webElement.getAttribute("id") + "']"));

											}

											fc.utobj().moveToElement(driver, element);
											fc.utobj().clickRadioButton(driver, rdBtn_Field, sValue); // work
											listItems.add(dataSet.get("" + webElement.getAttribute("" + id)));
											printInfo.put("" + webElement.getAttribute("id"),
													dataSet.get("" + webElement.getAttribute("" + id)));
											alreadyradio.put(webElement.getAttribute("" + id),
													webElement.getAttribute("" + id));
										} catch (Exception eText) {
											// Reporter.log("Problem in
											// selecting radio value in :
											// "+""+webElement.getAttribute("id"));
										}
									}
								} catch (Exception exp) {
									Reporter.log(exp.toString());
									exp.printStackTrace();
								}

							} else if (webElement.getAttribute("type").toLowerCase().indexOf("checkbox") != -1) {
								List<WebElement> oCheckBox = driver
										.findElements(By.name("" + webElement.getAttribute("" + id)));
								String sValue = "";
								if (alreadycheckBox.containsKey(webElement.getAttribute("" + id))) {

								} else {

									int size = oCheckBox.size();
									for (int i = 0; i < size; i++) {
										sValue = oCheckBox.get(i).getAttribute("value");
										// Thread.sleep(2000);
										if (i == (size - 1)) {
											// break;
										}

									}
									try {
										List<WebElement> rdBtn_Field = driver.findElements(By.xpath(
												"//*[contains(@id, '" + webElement.getAttribute("" + id) + "')]")); // work
										WebElement element = fc.utobj().getElementByXpath(driver,
												"//*[contains(@id, '" + webElement.getAttribute("" + id) + "')]");
										fc.utobj().moveToElement(driver, element);
										fc.utobj().clickRadioButton(driver, rdBtn_Field, sValue); // work
										alreadycheckBox.put(webElement.getAttribute("" + id),
												webElement.getAttribute("" + id));
									} catch (Exception eText) {

									}
								}
							} else if (webElement.getAttribute("type").indexOf("file") != -1) {
								String fileName = fc.utobj().getFilePathFromTestData("pictureFile");
								try {
									fc.utobj().sendKeys(driver,
											fc.utobj().getElementByID(driver, "" + webElement.getAttribute("" + id)),
											fileName);
									listItems.add(fileName);
									printInfo.put("" + webElement.getAttribute("" + id), fileName);
								} catch (Exception eFile) {
									// Reporter.log("Problem in uploading file
									// type value in :
									// "+""+webElement.getAttribute("id"));
								}
							} else if (webElement.getAttribute("type").indexOf("text") != -1) {
								if (webElement.getAttribute("value") == null
										|| "".equals(webElement.getAttribute("value"))
										|| "null".equals(webElement.getAttribute("value"))
										|| "0.00".equals(webElement.getAttribute("value"))) {
									if (webElement.getAttribute("class").indexOf("fTextBoxDate") != -1) {
										try {
											if (dataSet.get("" + webElement.getAttribute("" + id)) != null
													|| !"".equals(dataSet.get("" + webElement.getAttribute("" + id)))
													|| !"null".equals(
															dataSet.get("" + webElement.getAttribute("" + id)))) {
												fc.utobj().sendKeys(driver,
														driver.findElement(
																By.name("" + webElement.getAttribute("" + id))),
														dataSet.get("" + webElement.getAttribute("" + id))); // work
												listItems.add(dataSet.get("" + webElement.getAttribute("" + id)));
												printInfo.put("" + webElement.getAttribute("" + id),
														dataSet.get("" + webElement.getAttribute("" + id)));

											} else {

											}
										} catch (Exception eDate) {
											// Reporter.log("Problem in Entering
											// Date fields value in :
											// "+""+webElement.getAttribute("id"));
										}
									} else {
										try {
											if ("0.00".equals(webElement.getAttribute("value"))) {
												fc.utobj()
														.sendKeys(driver,
																driver.findElement(
																		By.name("" + webElement.getAttribute("" + id))),
																"");

												// System.out.println(webElement.getAttribute("value")
												// +"======"+dataSet.get(""+webElement.getAttribute(""+id))
												// +"====="+""+webElement.getAttribute(""+id));

											}
											if ("name".equals(id)) {
												fc.utobj().sendKeys(driver,
														driver.findElement(
																By.name("" + webElement.getAttribute("" + id))),
														dataSet.get("" + webElement.getAttribute("" + id)));
												if ("accountName".equals("" + webElement.getAttribute("" + id))
														|| "opportunityOwner"
																.equals("" + webElement.getAttribute("" + id))
														|| "oppContactID"
																.equals("" + webElement.getAttribute("" + id))) {
													;
													fc.utobj().clickElement(driver, driver.findElement(
															By.xpath(".//*[@id='customizedAjaxSearch']/div")));
													fc.utobj().sleep();
												}
											} else {
												fc.utobj().sendKeys(driver,
														driver.findElement(
																By.id("" + webElement.getAttribute("" + id))),
														dataSet.get("" + webElement.getAttribute("" + id)));
												if ("accountName".equals("" + webElement.getAttribute("" + id))
														|| "opportunityOwner"
																.equals("" + webElement.getAttribute("" + id))
														|| "oppContactID"
																.equals("" + webElement.getAttribute("" + id))) {
													;
													fc.utobj().clickElement(driver, driver.findElement(
															By.xpath(".//*[@id='customizedAjaxSearch']/div")));
													fc.utobj().sleep();
												}
											}

											if (!"ssn".equals("" + webElement.getAttribute("" + id))
													&& !"spouseSsn".equals("" + webElement.getAttribute("" + id))) {
												listItems.add(dataSet.get("" + webElement.getAttribute("" + id)));
											}
											printInfo.put("" + webElement.getAttribute("" + id),
													dataSet.get("" + webElement.getAttribute("" + id)));

										} catch (Exception eText) {
											// Reporter.log("Problem in Entering
											// Text fileds value in :
											// "+""+webElement.getAttribute("id"));
										}

									}
								}
							}
							alreadyitrateElement.put(webElement.getAttribute("" + id),
									webElement.getAttribute("" + id));
						}
					}
				}
			}

			if ("TC_CRM_QA_Contact_Details".equals(testCaseId)) {

				Select singledrop = null;
				List<WebElement> singleDropDown = null;
				try {
					singledrop = new Select(driver.findElement(By.name("contactOwnerID2")));
					singleDropDown = driver.findElements(By.name("contactOwnerID2"));
					WebElement dropDownField = driver.findElement(By.name("contactOwnerID2"));
					fc.utobj().selectDropDownByVisibleText(driver, dropDownField, dataSet.get("contactOwnerID2"));

					dropDownField = driver.findElement(By.name("cmSource2ID"));
					fc.utobj().selectDropDownByVisibleText(driver, dropDownField, dataSet.get("cmSource2ID"));
					fc.utobj().sleep(3000);
					dropDownField = driver.findElement(By.name("cmSource3ID"));
					fc.utobj().selectDropDownByVisibleText(driver, dropDownField, dataSet.get("cmSource3ID"));

				} catch (Exception e) {

				}

			}

		} catch (Exception e) {
			// fc.utobj().throwsException("Fields data are not available!
			// "+e.getMessage());
		}
		try {
			// fc.utobj().printTestStep(testCaseId, "Submiting the Tab ");
			WebElement elementbutton = fc.utobj().getElementByXpath(driver,
					".//*[@id='Submit' or @name='Submit' or @name='button' or @name='Submit1' or @name='add1'] ");
			fc.utobj().clickElement(driver, elementbutton);

		} catch (Exception ee) {
			fc.utobj().printBugStatus("Unable to submit page " + ee);
		}

		return printInfo;
	}

}