
package com.builds.test.crm;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.xml.parsers.ParserConfigurationException;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.Reporter;
import org.testng.annotations.Test;
import org.xml.sax.SAXException;

import com.builds.test.admin.AdminConfigurationConfigureCallStatusPageTest;
import com.builds.test.admin.AdminConfigurationConfigurePasswordSettingsPageTest;
import com.builds.test.admin.AdminFranchiseLocationAddFranchiseLocationPageTest;
import com.builds.test.admin.AdminUsersManageCorporateUsersAddCorporateUserPageTest;
import com.builds.test.admin.AdminUsersManageManageFranchiseUsersPageTest;
import com.builds.test.admin.AdminUsersManageRegionalUsersAddRegionalUserPageTest;
import com.builds.test.common.CorporateUser;
import com.builds.test.infomgr.AdminInfoMgrCommonMethods;
import com.builds.test.infomgr.FranchiseesCommonMethods;
import com.builds.test.infomgr.MultiUnitEntityCommonMethods;
import com.builds.uimaps.admin.AdminAreaRegionAddAreaRegionPage;
import com.builds.uimaps.admin.AdminConfigurationConfigureCallStatusPage;
import com.builds.uimaps.admin.AdminDivisionAddDivisionPage;
import com.builds.uimaps.admin.AdminFranchiseLocationAddFranchiseLocationPage;
import com.builds.uimaps.admin.AdminUsersManageManageFranchiseUsersPage;
import com.builds.uimaps.crm.AdminCRMConfigureIndustryPage;
import com.builds.uimaps.crm.AdminCRMConfigureMediumPage;
import com.builds.uimaps.crm.AdminCRMConfigureStatusPageUI;
import com.builds.uimaps.crm.AdminCRMContactTypeConfigurationPage;
import com.builds.uimaps.crm.AdminCRMManageProductServiceCategoryPage;
import com.builds.uimaps.crm.AdminCRMManageWebFormGeneratorPage;
import com.builds.uimaps.crm.AdminCRMSourceSummaryPage;
import com.builds.uimaps.crm.CRMAccountsPage;
import com.builds.uimaps.crm.CRMContactsPage;
import com.builds.uimaps.crm.CRMGroupsPage;
import com.builds.uimaps.crm.CRMLeadsPage;
import com.builds.uimaps.fs.SearchUI;
import com.builds.uimaps.infomgr.AdminInfoMgrManageFormGeneratorAddTabPage;
import com.builds.uimaps.infomgr.AdminInfoMgrManageFormGeneratorPage;
import com.builds.uimaps.infomgr.AdminInfoMgrManageFormGeneratorTabDetailsPage;
import com.builds.uimaps.infomgr.InfoMgrMultiUnitEntityPage;
import com.builds.uimaps.infomgr.InfoMgrMultiUnitInfoPage;
import com.builds.utilities.FranconnectUtil;
import com.builds.utilities.TestCase;

public class CRMContactsPageTest {
	FranconnectUtil fc = new FranconnectUtil();

	@Test(groups = { "crm","crm_new_Build"},invocationCount=10)
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify the Add Contact At CRM > Contact > Contact Summary", testCaseId = "TC_158_Add_Contact")
	public void addContact() throws IOException, Exception, ParserConfigurationException, SAXException {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);

		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			CRMContactsPage pobj = new CRMContactsPage(driver);
			SearchUI search_page = new SearchUI(driver);

			fc.utobj().printTestStep("Navigate To Admin Users Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			//String emailId = "crmautomation@staffex.com";
			String emailId=fc.utobj().generateTestData("crmautomation")+"@staffex.com";
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUser = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporateUser.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			fc.utobj().printTestStep("Navigate To Admin > CRM > Contact Type Configuration");
			fc.utobj().printTestStep("Add Contact Type");
			AdminCRMContactTypeConfigurationPageTest contatcTypePage = new AdminCRMContactTypeConfigurationPageTest();
			String contactType = fc.utobj().generateTestData(dataSet.get("contactType"));
			contatcTypePage.addContactType(driver, contactType);

			fc.utobj().printTestStep("Naviagte To CRM > Contacts > Contacts Summary");
			fc.crm().crm_common().CRMContactsLnk(driver);
			fc.utobj().printTestStep("Add Contacs");
			fc.utobj().clickElement(driver, pobj.addNew);
			fc.utobj().clickElement(driver, pobj.contactLnk);

			String firstName = fc.utobj().generateTestData(dataSet.get("firstName"));
			String lastName = fc.utobj().generateTestData(dataSet.get("lastName"));
			String primaryCMethod = dataSet.get("primaryContactMethod");
			String assignTo = "Corporate";
			String regionName = "";
			String franchiseId = "";
			String title = dataSet.get("title");

			addContactGeneric(driver, contactType, firstName, lastName, primaryCMethod, assignTo,
					corpUser.getuserFullName(), regionName, franchiseId, title, emailId, config);

			String contactName = firstName + " " + lastName;

			fc.utobj().printTestStep("Verify The Added Contact Search By Top Search");

			boolean isSearchTrue = false;
			for (int i = 0; i < 3; i++) {
				if (isSearchTrue == false) {
					fc.utobj().sendKeys(driver, search_page.topSearchField, contactName);
					Thread.sleep(2000);
					if (i == 2) {
						Thread.sleep(2000);
					}
					fc.utobj().clickEnterOnElement(driver, search_page.topSearchField);

					try {
						isSearchTrue = fc.utobj().isElementPresent(driver, fc.utobj().getElementByXpath(driver,
								".//custom[contains(text () , '" + contactName + "')]"));
					} catch (Exception e) {
						Reporter.log(e.getMessage());
						System.out.println(e.getMessage());
					}
				}
			}

			if (isSearchTrue == false) {
				fc.utobj().throwsException("not able to search contact By Top Search");
			}
			fc.utobj().refresh(driver);

			fc.utobj().printTestStep("Verify Contact At Primary Info Page");
			boolean isContactTypePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () ,'Contact Type')]/ancestor::tr/td[.='" + contactType + "']");
			if (isContactTypePresent == false) {
				fc.utobj().throwsException("was not able to verify Contact Type");
			}
			boolean isContactNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () ,'Name')]/ancestor::tr/td[.='Mr. " + firstName + " " + lastName + "']");
			if (isContactNamePresent == false) {
				fc.utobj().throwsException("was not able to verify Contact Name");
			}
			boolean isPrimaryContactTypePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () ,'Primary Contact Method')]/ancestor::tr/td[.='Email']");
			if (isPrimaryContactTypePresent == false) {
				fc.utobj().throwsException("was not able to verify Primary Contact Type");
			}
			boolean isUserPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () ,'Assign To ')]/ancestor::tr/td[contains(text () , '"
							+ corpUser.getuserFullName() + "')]");
			if (isUserPresent == false) {
				fc.utobj().throwsException("was not able to verify Assign To");
			}

			fc.utobj().clickElement(driver, pobj.contactsLinks);
			contactsFilter(driver, contactType);

			fc.utobj().printTestStep("Verify Contact At Contacts Summary Page");
			boolean isNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firstName + " " + lastName + "')]");
			if (isNamePresent == false) {
				fc.utobj().throwsException("was not able to verify Contact Name");
			}
			boolean isContactTypePresentAtHome = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[contains(text () ,'" + firstName
					+ " " + lastName + "')]/ancestor::tr/td[contains(text () ,'" + contactType + "')]");
			if (isContactTypePresentAtHome == false) {
				fc.utobj().throwsException("was not able to verify Contact Type");
			}
			boolean isOwnerPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[contains(text () ,'" + firstName + " "
					+ lastName + "')]/ancestor::tr/td[contains(text () ,'" + corpUser.getuserFullName() + "')]");
			if (isOwnerPresent == false) {
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "crm")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Add Lead At CRM > Contacts > Contacts Summary", testCaseId = "TC_159_Add_Lead")
	public void addLead() throws IOException, Exception, ParserConfigurationException, SAXException {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());
		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			CRMLeadsPage pobj = new CRMLeadsPage(driver);

			CRMLeadsPageTest leadPage = new CRMLeadsPageTest();
			/*
			 * fc.utobj().printTestStep(testCaseId,
			 * "Navigate To Admin Users Manage Corporate User");
			 * fc.utobj().printTestStep(testCaseId, "Add Corporate User");
			 * AdminUsersManageCorporateUsersAddCorporateUserPageTest corpTest =
			 * new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			 * String userName =
			 * fc.utobj().generateTestData(dataSet.get("userName"));
			 * 
			 * String emailId="crmautomation@staffex.com";
			 * 
			 * userName = corpTest.addCorporateUser(driver, userName, config ,
			 * emailId);
			 */

			fc.utobj().printTestStep("Navigate To Admin Users Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			String emailId = "crmautomation@staffex.com";
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUser = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporateUser.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			fc.utobj().printTestStep("Navigate To CRM > Contacts > Contacts Summary");
			fc.utobj().printTestStep("Add Lead");
			fc.crm().crm_common().CRMContactsLnk(driver);
			fc.utobj().clickElement(driver, pobj.addNew);
			fc.utobj().clickElement(driver, pobj.leadLnk);
			fc.utobj().selectDropDown(driver, pobj.title, dataSet.get("title"));
			String firstName = fc.utobj().generateTestData(dataSet.get("firstName"));
			fc.utobj().sendKeys(driver, pobj.leadFirstName, firstName);
			String lastName = fc.utobj().generateTestData(dataSet.get("lastName"));
			fc.utobj().sendKeys(driver, pobj.leadLastName, lastName);
			String company = fc.utobj().generateTestData(dataSet.get("company"));
			fc.utobj().sendKeys(driver, pobj.companyName, company);
			fc.utobj().selectDropDown(driver, pobj.primaryContactMethodSelect, dataSet.get("primaryContactMethod"));
			if (!fc.utobj().isSelected(driver, pobj.assignToCorporate)) {
				fc.utobj().clickElement(driver, pobj.assignToCorporate);
			}
			fc.utobj().selectDropDown(driver, pobj.selectCorporateUser, corpUser.getuserFullName());
			String address = fc.utobj().generateTestData(dataSet.get("address"));
			fc.utobj().sendKeys(driver, pobj.address, address);
			String city = fc.utobj().generateTestData(dataSet.get("city"));
			fc.utobj().sendKeys(driver, pobj.city, city);
			fc.utobj().selectDropDown(driver, pobj.country, "USA");
			fc.utobj().selectDropDown(driver, pobj.state, "Alabama");
			fc.utobj().sendKeys(driver, pobj.zipcode, "12345");
			fc.utobj().sendKeys(driver, pobj.phoneNumbers, "1236547896");
			fc.utobj().sendKeys(driver, pobj.extn, "12");
			fc.utobj().sendKeys(driver, pobj.faxNumbers, "1263985487");
			fc.utobj().sendKeys(driver, pobj.mobileNumbers, "8896724441");
			String email = dataSet.get("emailId");
			fc.utobj().sendKeys(driver, pobj.emailIds, email);
			fc.utobj().sendKeys(driver, pobj.alternateEmail, "test@gmail.com");
			String suffix = fc.utobj().generateTestData(dataSet.get("suffix"));
			fc.utobj().sendKeys(driver, pobj.suffix, suffix);
			String jobTitle = fc.utobj().generateTestData(dataSet.get("jobTitle"));
			fc.utobj().sendKeys(driver, pobj.position, jobTitle);
			fc.utobj().sendKeys(driver, pobj.birthdate, "11/24/1980");
			fc.utobj().sendKeys(driver, pobj.anniversarydate, "11/24/1999");
			fc.utobj().selectDropDown(driver, pobj.rating, dataSet.get("rating"));
			fc.utobj().sendKeys(driver, pobj.bestTimeToContact, "12:00 PM EST");
			String comment = fc.utobj().generateTestData(dataSet.get("comment"));
			fc.utobj().sendKeys(driver, pobj.comments, comment);
			fc.utobj().clickElement(driver, pobj.saveBtn);

			// verify at Home Page
			fc.utobj().printTestStep("Verify Lead At CRM > Leads Page");
			fc.utobj().clickElement(driver, pobj.leadsLink);
			leadPage.searchLeadByOwner(driver, corpUser.getuserFullName(), "Open Leads");

			boolean verifyName = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[.='" + firstName + " " + lastName + "']");
			if (verifyName == false) {
				fc.utobj().throwsException("was not able to verify Name");
			}
			boolean ownerOfLead = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[.='" + firstName + " " + lastName
					+ "']/ancestor::tr/td[contains(text () , '" + corpUser.getuserFullName() + "')]");
			if (ownerOfLead == false) {
				fc.utobj().throwsException("was not able to verify owner of the lead");
			}
			boolean statusOfLead = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[.='" + firstName + " " + lastName + "']/ancestor::tr/td[contains(text () , 'New')]");
			if (statusOfLead == false) {
				fc.utobj().throwsException("was not able to verify staus of the lead");
			}

			boolean emailOfLead = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[.='" + firstName + " " + lastName
					+ "']/ancestor::tr/td/a[contains(text () , '" + email + "')]");
			if (emailOfLead == false) {
				fc.utobj().throwsException("was not able to verify email of the lead");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "crm")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify the Add Account At CRM > Contact > Contact Summary", testCaseId = "TC_160_Add_Account")
	public void addAccount() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			CRMAccountsPage pobj = new CRMAccountsPage(driver);

			CRMAccountsPageTest accountPage = new CRMAccountsPageTest();

			AdminCRMConfigureIndustryPageTest industryPage = new AdminCRMConfigureIndustryPageTest();
			String industry = fc.utobj().generateTestData(dataSet.get("industry"));
			String description = fc.utobj().generateTestData(dataSet.get("description"));
			fc.utobj().printTestStep("Navigating to Admin > CRM >  Configure Industry Type");
			fc.utobj().printTestStep("Add Industry");
			industryPage.addIndustry(driver, industry, description);

			fc.utobj().printTestStep("Navigate To CRM > Contacts > Contacts Summary");
			fc.crm().crm_common().CRMContactsLnk(driver);
			fc.utobj().printTestStep("Add Account");
			fc.utobj().clickElement(driver, pobj.addNew);
			fc.utobj().clickElement(driver, pobj.accountLnk);
			String accountName = fc.utobj().generateTestData(dataSet.get("accountName"));
			fc.utobj().sendKeys(driver, pobj.accountName, accountName);
			if (!fc.utobj().isSelected(driver, pobj.B2BAccountType)) {
				fc.utobj().clickElement(driver, pobj.B2BAccountType);
			}
			fc.utobj().selectDropDown(driver, pobj.visibilityType, "All Network");
			if (fc.utobj().isSelected(driver,pobj.isParent)) {
				fc.utobj().clickElement(driver, pobj.isParent);
			}
			fc.utobj().sendKeys(driver, pobj.address, "AddressTest");
			fc.utobj().sendKeys(driver, pobj.city, "City");
			fc.utobj().selectDropDown(driver, pobj.country, "USA");
			fc.utobj().sendKeys(driver, pobj.zipcode, "123455");
			fc.utobj().selectDropDown(driver, pobj.state, "Alabama");
			fc.utobj().sendKeys(driver, pobj.phoneNumbers, "1236547896");
			fc.utobj().sendKeys(driver, pobj.extn, "123");
			fc.utobj().sendKeys(driver, pobj.faxNumbers, "1236654");
			fc.utobj().sendKeys(driver, pobj.mobileNumbers, "1478523698");
			fc.utobj().sendKeys(driver, pobj.emailIds, "crmautomation@staffex.com");
			fc.utobj().sendKeys(driver, pobj.noOfEmployee, "500");
			fc.utobj().selectDropDown(driver, pobj.industry, industry);
			fc.utobj().sendKeys(driver, pobj.sicCode, "SicCode");
			fc.utobj().sendKeys(driver, pobj.servicesProvided, "Service123");
			fc.utobj().sendKeys(driver, pobj.stockSymbol, "qwerty");

			if (!fc.utobj().isSelected(driver, pobj.addContactNo)) {
				fc.utobj().clickElement(driver, pobj.addContactNo);
			}
			fc.utobj().clickElement(driver, pobj.saveBtn);

			fc.utobj().clickElement(driver, pobj.accountsLink);
			accountPage.searchAccountByIndustry(driver, industry);

			fc.utobj().printTestStep("Verify Account At CRM > Account > Accounts Summary Page");
			boolean isAccountNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[.='" + accountName + "']");
			if (isAccountNamePresent == false) {
				fc.utobj().throwsException("was not able to add Account");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "crm")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify the Add Opportunity At CRM > Contacts > Contacts Summary", testCaseId = "TC_161_Add_Opportunity")
	public void addOpportunity() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			CRMAccountsPage pobj = new CRMAccountsPage(driver);
			CRMAccountsPageTest accountPage = new CRMAccountsPageTest();

			/*
			 * fc.utobj().printTestStep(testCaseId,
			 * "Navigate To Admin Users Manage Corporate User");
			 * fc.utobj().printTestStep(testCaseId, "Add Corporate User");
			 * 
			 * AdminUsersManageCorporateUsersAddCorporateUserPageTest corpTest =
			 * new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			 * String userName =
			 * fc.utobj().generateTestData(dataSet.get("userName")); String
			 * emailId="crmautomation@staffex.com"; userName =
			 * corpTest.addCorporateUser(driver, userName, config , emailId);
			 */

			fc.utobj().printTestStep("Navigate To Admin Users Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			String emailId = "crmautomation@staffex.com";
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUser = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporateUser.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			fc.utobj().printTestStep("Navigating to Admin > CRM > ContactType Configuration");
			fc.utobj().printTestStep("Add Contact Type");
			AdminCRMContactTypeConfigurationPageTest contactTypePage = new AdminCRMContactTypeConfigurationPageTest();
			String contactType = fc.utobj().generateTestData(dataSet.get("contactType"));
			contactTypePage.addContactType(driver, contactType);

			fc.utobj().printTestStep("Navigating to Admin > CRM >  Configure Opportunity Stages");
			fc.utobj().printTestStep("Add Stage");
			AdminCRMConfigureOpportunityStagesPageTest stagePage = new AdminCRMConfigureOpportunityStagesPageTest();
			String stage = fc.utobj().generateTestData(dataSet.get("stage"));
			stagePage.addStage(driver, stage);

			AdminCRMConfigureIndustryPageTest industryPage = new AdminCRMConfigureIndustryPageTest();
			String industry = fc.utobj().generateTestData(dataSet.get("industry"));
			String description = fc.utobj().generateTestData(dataSet.get("description"));
			fc.utobj().printTestStep("Navigating to Admin > CRM >  Configure Industry Type");
			fc.utobj().printTestStep("Add Industry");
			industryPage.addIndustry(driver, industry, description);

			String accountName = fc.utobj().generateTestData(dataSet.get("accountName"));
			String visibility = dataSet.get("visibility");
			fc.utobj().printTestStep("Navigating to CRM >  Accounts > Accounts Summary");
			fc.utobj().printTestStep("Add Account");
			accountPage.addAcocunt(driver, industry, accountName, visibility);

			fc.utobj().printTestStep("Navigate To CRM > Contacts > Contacts Summary Page");
			fc.utobj().printTestStep("Add Contact");
			fc.utobj().clickElement(driver, pobj.contactsLinks);
			fc.utobj().clickElement(driver, pobj.addNew);
			fc.utobj().clickElement(driver, pobj.contactLnk);

			String cFname = fc.utobj().generateTestData(dataSet.get("cFname"));
			String cLname = fc.utobj().generateTestData(dataSet.get("cLname"));
			String primaryCMethod = dataSet.get("primaryCMethod");
			String assignTo = "Corporate";
			String regionName = "";
			String franchiseId = "";
			String title = dataSet.get("title");

			addContactGenericWithAccount(driver, contactType, cFname, cLname, primaryCMethod, assignTo,
					corpUser.getuserFullName(), regionName, franchiseId, title, emailId, config, accountName);

			fc.utobj().printTestStep("Add Opportunity");
			fc.utobj().clickElement(driver, pobj.contactsLinks);
			fc.utobj().clickElement(driver, pobj.addNew);
			fc.utobj().clickElement(driver, pobj.OpportunityLnk);

			String opportunityName = fc.utobj().generateTestData(dataSet.get("opportunityName"));
			String contactName = cFname + " " + cLname;
			String opportunityType = dataSet.get("opportunityType");
			String opportunitySource = dataSet.get("opportunitySource");
			String salesAmount = dataSet.get("salesAmount");
			accountPage.addOpportunity(driver, opportunityName, accountName, contactName, corpUser.getuserFullName(),
					stage, opportunityType, opportunitySource, salesAmount, config);

			fc.utobj().clickElement(driver, pobj.contactsLinks);
			contactsFilter(driver, contactType);
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//a[contains(text () ,'" + contactName + "')]"));
			fc.utobj().clickElement(driver, pobj.opportunitiesTab);

			fc.utobj().printTestStep("Verify Opportunity");
			boolean isOpportunityPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[.='" + opportunityName + "']");
			if (isOpportunityPresent == false) {
				fc.utobj().throwsException("was not able to verify Opportunity Name");
			}
			boolean isAssignToPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[.='" + opportunityName
					+ "']/ancestor::tr/td[contains(text () , '" + corpUser.getuserFullName() + "')]");
			if (isAssignToPresent == false) {
				fc.utobj().throwsException("was not able to verify Assign To");
			}
			boolean isAccountNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[.='" + opportunityName + "']/ancestor::tr/td/a[contains(text () , '" + accountName + "')]");
			if (isAccountNamePresent == false) {
				fc.utobj().throwsException("was not able to verify Account");
			}
			boolean isContactNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[.='" + opportunityName + "']/ancestor::tr/td/a[contains(text () , '" + contactName + "')]");
			if (isContactNamePresent == false) {
				fc.utobj().throwsException("was not able to verify Contact");
			}
			boolean isStagePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[.='" + opportunityName + "']/ancestor::tr/td[contains(text () , '" + stage + "')]");
			if (isStagePresent == false) {
				fc.utobj().throwsException("was not able to verify Stage");
			}
			boolean isSalesAmountPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[.='" + opportunityName
					+ "']/ancestor::tr/td[contains(text () , '" + salesAmount.concat(".00") + "')]");
			if (isSalesAmountPresent == false) {
				fc.utobj().throwsException("was not able to verify Sales Amount");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "crm")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify the Modify Contact Details By Action Imgae Option At CRM > Contact > Contact Summary", testCaseId = "TC_162_Modify_Contact")
	public void modifyContactDetailsActionsImage()
			throws IOException, Exception, ParserConfigurationException, SAXException {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			CRMContactsPage pobj = new CRMContactsPage(driver);

			fc.utobj().printTestStep("Navigate To Admin > Users > Manage Regional User");
			fc.utobj().printTestStep("Add Regional User");
			AdminUsersManageRegionalUsersAddRegionalUserPageTest regionalUserPage = new AdminUsersManageRegionalUsersAddRegionalUserPageTest();
			String userNameReg = fc.utobj().generateTestData(dataSet.get("userNameReg"));
			String region = fc.utobj().generateTestData(dataSet.get("regionName"));
			String emailId = "crmautomation@staffex.com";
			regionalUserPage.addRegionalUser(driver, userNameReg, region, emailId);

			/*
			 * fc.utobj().printTestStep(testCaseId,
			 * "Navigate To Admin Users Manage Corporate User");
			 * fc.utobj().printTestStep(testCaseId, "Add Corporate User");
			 * AdminUsersManageCorporateUsersAddCorporateUserPageTest corpTest =
			 * new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			 * String userName =
			 * fc.utobj().generateTestData(dataSet.get("userName"));
			 * 
			 * userName = corpTest.addCorporateUser(driver, userName, config ,
			 * emailId);
			 */

			fc.utobj().printTestStep("Navigate To Admin Users Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");

			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUser = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporateUser.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			fc.utobj().printTestStep("Navigate To Admin > CRM > Contact Type Configuration");
			fc.utobj().printTestStep("Add Contact Type");

			AdminCRMContactTypeConfigurationPageTest contatcTypePage = new AdminCRMContactTypeConfigurationPageTest();
			String contactType = fc.utobj().generateTestData(dataSet.get("contactType"));
			contatcTypePage.addContactType(driver, contactType);

			fc.utobj().printTestStep("Navigate To CRM > Contacts > Contacts Summary Page");
			fc.utobj().printTestStep("Add Contact");
			fc.crm().crm_common().CRMContactsLnk(driver);
			fc.utobj().clickElement(driver, pobj.addNew);
			fc.utobj().clickElement(driver, pobj.contactLnk);

			String firstName = fc.utobj().generateTestData(dataSet.get("firstName"));
			String lastName = fc.utobj().generateTestData(dataSet.get("lastName"));
			String primaryCMethod = dataSet.get("primaryCMethod");
			String assignTo = "Corporate";
			String regionName = "";
			String franchiseId = "";
			String title = dataSet.get("title");

			addContactGeneric(driver, contactType, firstName, lastName, primaryCMethod, assignTo,
					corpUser.getuserFullName(), regionName, franchiseId, title, emailId, config);
			/* fc.utobj().clickElement(driver, pobj.saveBtn); */

			fc.utobj().clickElement(driver, pobj.contactsLinks);
			contactsFilter(driver, contactType);
			fc.utobj().printTestStep("Modify Details");
			fc.utobj().actionImgOption(driver, firstName + " " + lastName, "Modify Details");

			firstName = fc.utobj().generateTestData(dataSet.get("firstName"));
			lastName = fc.utobj().generateTestData(dataSet.get("lastName"));

			fc.utobj().sendKeys(driver, pobj.contactFirstName, firstName);
			fc.utobj().sendKeys(driver, pobj.contactLastName, lastName);

			if (!fc.utobj().isSelected(driver, pobj.assignToRegional)) {
				fc.utobj().clickElement(driver, pobj.assignToRegional);
			}

			fc.utobj().selectDropDown(driver, pobj.selectAreaRegion, region);
			fc.utobj().selectDropDown(driver, pobj.selectRegionalUser, userNameReg + " " + userNameReg);
			fc.utobj().clickElement(driver, pobj.saveBtn);

			fc.utobj().printTestStep("Verify Modify Details");
			boolean isContactTypePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () ,'Contact Type')]/ancestor::tr/td[.='" + contactType + "']");
			if (isContactTypePresent == false) {
				fc.utobj().throwsException("was not able to verify Contact Type");
			}
			boolean isContactNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () ,'Name')]/ancestor::tr/td[.='Mr. " + firstName + " " + lastName + "']");
			if (isContactNamePresent == false) {
				fc.utobj().throwsException("was not able to verify Contact Name");
			}
			boolean isPrimaryContactTypePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () ,'Primary Contact Method')]/ancestor::tr/td[.='Email']");
			if (isPrimaryContactTypePresent == false) {
				fc.utobj().throwsException("was not able to verify Primary Contact Type");
			}
			boolean isUserPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () ,'Assign To ')]/ancestor::tr/td[contains(text () , '" + userNameReg + " "
							+ userNameReg + "')]");
			if (isUserPresent == false) {
				fc.utobj().throwsException("was not able to verify Assign To");
			}

			fc.utobj().clickElement(driver, pobj.contactsLinks);
			contactsFilter(driver, contactType);

			boolean isNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firstName + " " + lastName + "')]");
			if (isNamePresent == false) {
				fc.utobj().throwsException("was not able to verify Contact Name");
			}
			boolean isContactTypePresentAtHome = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[contains(text () ,'" + firstName
					+ " " + lastName + "')]/ancestor::tr/td[contains(text () ,'" + contactType + "')]");
			if (isContactTypePresentAtHome == false) {
				fc.utobj().throwsException("was not able to verify Contact Type");
			}
			boolean isOwnerPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[contains(text () ,'" + firstName + " "
					+ lastName + "')]/ancestor::tr/td[contains(text () ,'" + userNameReg + " " + userNameReg + "')]");
			if (isOwnerPresent == false) {
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "crm")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify the Log A Task At CRM > Contact > Contact Summary", testCaseId = "TC_163_Lag_A_Task")
	public void logATaskActionImage() throws IOException, Exception, ParserConfigurationException, SAXException {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			CRMContactsPage pobj = new CRMContactsPage(driver);

			/*
			 * fc.utobj().printTestStep(testCaseId,
			 * "Navigate To Admin Users Manage Corporate User");
			 * fc.utobj().printTestStep(testCaseId, "Add Corporate User");
			 * 
			 * AdminUsersManageCorporateUsersAddCorporateUserPageTest corpTest =
			 * new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			 * String userName =
			 * fc.utobj().generateTestData(dataSet.get("userName")); String
			 * emailId="crmautomation@staffex.com"; userName =
			 * corpTest.addCorporateUser(driver, userName, config , emailId);
			 */

			fc.utobj().printTestStep("Navigate To Admin Users Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			String emailId = "crmautomation@staffex.com";
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUser = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporateUser.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			fc.utobj().printTestStep("Navigate To Admin > CRM > Contact Type Configuration");
			fc.utobj().printTestStep("Add Contact Type");
			AdminCRMContactTypeConfigurationPageTest contatcTypePage = new AdminCRMContactTypeConfigurationPageTest();
			String contactType = fc.utobj().generateTestData(dataSet.get("contactType"));
			contatcTypePage.addContactType(driver, contactType);

			fc.utobj().printTestStep("Navigate To CRM > Contacts > Contacts Summary Page");
			fc.utobj().printTestStep("Add Contact");
			fc.crm().crm_common().CRMContactsLnk(driver);
			fc.utobj().clickElement(driver, pobj.addNew);
			fc.utobj().clickElement(driver, pobj.contactLnk);

			String firstName = fc.utobj().generateTestData(dataSet.get("firstName"));
			String lastName = fc.utobj().generateTestData(dataSet.get("lastName"));
			String primaryCMethod = dataSet.get("primaryCMethod");
			String assignTo = "Corporate";
			String regionName = "";
			String franchiseId = "";
			String title = dataSet.get("title");

			addContactGeneric(driver, contactType, firstName, lastName, primaryCMethod, assignTo,
					corpUser.getuserFullName(), regionName, franchiseId, title, emailId, config);

			fc.utobj().clickElement(driver, pobj.contactsLinks);
			contactsFilter(driver, contactType);

			fc.utobj().printTestStep("Log A Task");

			fc.utobj().actionImgOption(driver, firstName + " " + lastName, "Log a Task");

			fc.commonMethods().switch_cboxIframe_frameId(driver);
			if (!fc.utobj().isSelected(driver,pobj.radioOwner)) {
				fc.utobj().clickElement(driver, pobj.radioOwner);
			}
			String statusTask = dataSet.get("statusTask");
			fc.utobj().selectDropDown(driver, pobj.statusTask, statusTask);
			String subject = fc.utobj().generateTestData(dataSet.get("subject"));
			fc.utobj().sendKeys(driver, pobj.subject, subject);
			if (!fc.utobj().isSelected(driver, pobj.timeLessTask)) {
				fc.utobj().clickElement(driver, pobj.timeLessTask);
			}
			String priority = dataSet.get("priority");
			fc.utobj().selectDropDown(driver, pobj.priority, priority);
			fc.utobj().sendKeys(driver, pobj.comments, "Test Comment");
			fc.utobj().clickElement(driver, pobj.createBtn);
			fc.utobj().switchFrameToDefault(driver);

			contactsFilter(driver, contactType);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//a[contains(text () ,'" + firstName + " " + lastName + "')]"));

			fc.utobj().printTestStep("Verify Task Contact Primary Info Page");
			boolean isSubjectPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[contains(text () ,'" + subject + "')]");
			if (isSubjectPresent == false) {
				fc.utobj().throwsException("was not able to verify subject");
			}
			boolean isUserPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[contains(text () ,'" + subject
					+ "')]/ancestor::tr/td/div[.='" + corpUser.getuserFullName() + "']");
			if (isUserPresent == false) {
				fc.utobj().throwsException("was not able to verify userName at Lead Info Page");
			}
			boolean isStatusOfTaskPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[contains(text () ,'" + subject
					+ "')]/ancestor::tr/td[contains(text () ,'" + statusTask + "')]");
			if (isStatusOfTaskPresent == false) {
				fc.utobj().throwsException("was not able to verify status of task");
			}
			boolean isPriorityPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[contains(text () ,'" + subject
					+ "')]/ancestor::tr/td[contains(text () ,'" + priority + "')]");
			if (isPriorityPresent == false) {
				fc.utobj().throwsException("was not able to verify Priority of task");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = {"crm"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify the Log A Call At CRM > Contact > Contact Summary", testCaseId = "TC_164_Log_A_Call")
	public void logACallActionImage() throws IOException, Exception, ParserConfigurationException, SAXException {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			CRMContactsPage pobj = new CRMContactsPage(driver);

			fc.utobj().printTestStep("Navigate To Admin Users Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			String emailId = "crmautomation@staffex.com";
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUser = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporateUser.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			fc.utobj().printTestStep("Navigate To Admin > CRM > Contact Type Configuration");
			fc.utobj().printTestStep("Add Contact Type");
			AdminCRMContactTypeConfigurationPageTest contatcTypePage = new AdminCRMContactTypeConfigurationPageTest();
			String contactType = fc.utobj().generateTestData(dataSet.get("contactType"));
			contatcTypePage.addContactType(driver, contactType);

			fc.utobj().printTestStep("Navigate To CRM > Contacts > Contacts Summary Page");
			fc.utobj().printTestStep("Add Contact");
			fc.crm().crm_common().CRMContactsLnk(driver);
			fc.utobj().clickElement(driver, pobj.addNew);
			fc.utobj().clickElement(driver, pobj.contactLnk);

			String firstName = fc.utobj().generateTestData(dataSet.get("firstName"));
			String lastName = fc.utobj().generateTestData(dataSet.get("lastName"));
			String primaryCMethod = dataSet.get("primaryCMethod");
			String assignTo = "Corporate";
			String regionName = "";
			String franchiseId = "";
			String title = dataSet.get("title");

			addContactGeneric(driver, contactType, firstName, lastName, primaryCMethod, assignTo,
					corpUser.getuserFullName(), regionName, franchiseId, title, emailId, config);

			fc.utobj().clickElement(driver, pobj.contactsLinks);
			contactsFilter(driver, contactType);
			fc.utobj().printTestStep("Log A Call");
			fc.utobj().actionImgOption(driver, firstName + " " + lastName, "Log a Call");
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			String callSubject = fc.utobj().generateTestData(dataSet.get("subject"));
			fc.utobj().sendKeys(driver, pobj.callSubject, callSubject);
			fc.utobj().getCurrentDateAndTime();
			String callDate = fc.utobj().getCurrentDateAndTime();//fc.utobj().getCurrentDateUSFormat();
			fc.utobj().sendKeys(driver, pobj.callDate, callDate);
			String callStatus = dataSet.get("callStatus");
			fc.utobj().selectDropDown(driver, pobj.callStatus, callStatus);
			String cType = dataSet.get("callType");
			fc.utobj().selectDropDown(driver, pobj.communicationType, cType);
			fc.utobj().sendKeys(driver, pobj.comments, "Test Comment");
			fc.utobj().clickElement(driver, pobj.saveBtn);
			fc.utobj().clickElement(driver, pobj.noBtn);
			fc.utobj().switchFrameToDefault(driver);

			contactsFilter(driver, contactType);
			fc.utobj().printTestStep("Verify Call At Detailed History Frame");
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//a[contains(text () ,'" + firstName + " " + lastName + "')]"));
			fc.utobj().clickElement(driver, pobj.detailedHistoryLink);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().clickElement(driver, pobj.callsTabDH);

			boolean isCallSubjectPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () ,'" + callSubject + "')]");
			if (isCallSubjectPresent == false) {
				fc.utobj().throwsException("was not able to verify Call Subject");
			}
			boolean isCommunicationTypePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () ,'" + cType + "')]");
			if (isCommunicationTypePresent == false) {
				fc.utobj().throwsException("was not able to verify communication type");
			}
			fc.utobj().clickElement(driver, pobj.closeBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "crm"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2017-08-10", testCaseDescription = "Verify The Add Transaction By Action Image Option At CRM > Contact > Contact Summary", testCaseId = "TC_165_Add_Transaction")
	public void addTransactionActionImage() throws IOException, Exception, ParserConfigurationException, SAXException {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			CRMAccountsPage pobj = new CRMAccountsPage(driver);

			String description = fc.utobj().generateTestData(dataSet.get("description"));
			AdminCRMManageProductServiceCategoryPageTest serviceProductPage = new AdminCRMManageProductServiceCategoryPageTest();
			String categoryName = fc.utobj().generateTestData(dataSet.get("categoryName"));
			String productName = fc.utobj().generateTestData(dataSet.get("productName"));
			String oneLineDescription = fc.utobj().generateTestData(dataSet.get("oneLineDescription"));
			String rate = dataSet.get("rate");
			fc.utobj().printTestStep("Navigating to Admin > CRM >  Manage Product / Service & Category");
			fc.utobj().printTestStep("Add Category");
			serviceProductPage.addCategory(driver, categoryName, description);

			fc.utobj().printTestStep("Add Product/Service");
			serviceProductPage.addProductService(driver, categoryName, productName, oneLineDescription, rate);

			CRMAccountsPageTest accountPage = new CRMAccountsPageTest();

			fc.utobj().printTestStep("Navigate To Admin Users Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			String emailId = "crmautomation@staffex.com";
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUser = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporateUser.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			fc.utobj().printTestStep("Navigating to Admin > CRM > ContactType Configuration");
			fc.utobj().printTestStep("Add Contact Type");
			AdminCRMContactTypeConfigurationPageTest contactTypePage = new AdminCRMContactTypeConfigurationPageTest();
			String contactType = fc.utobj().generateTestData(dataSet.get("contactType"));
			contactTypePage.addContactType(driver, contactType);

			fc.utobj().printTestStep("Navigating to Admin > CRM >  Configure Opportunity Stages");
			fc.utobj().printTestStep("Add Stage");
			AdminCRMConfigureOpportunityStagesPageTest stagePage = new AdminCRMConfigureOpportunityStagesPageTest();
			String stage = fc.utobj().generateTestData(dataSet.get("stage"));
			stagePage.addStage(driver, stage);

			AdminCRMConfigureIndustryPageTest industryPage = new AdminCRMConfigureIndustryPageTest();
			String industry = fc.utobj().generateTestData(dataSet.get("industry"));
			description = fc.utobj().generateTestData(dataSet.get("description"));
			fc.utobj().printTestStep("Navigating to Admin > CRM >  Configure Industry Type");
			fc.utobj().printTestStep("Add Industry");
			industryPage.addIndustry(driver, industry, description);

			String accountName = fc.utobj().generateTestData(dataSet.get("accountName"));
			String visibility = dataSet.get("visibility");
			fc.utobj().printTestStep("Navigating to CRM >  Accounts > Accounts Summary");
			fc.utobj().printTestStep("Add Account");
			accountPage.addAcocunt(driver, industry, accountName, visibility);

			fc.utobj().printTestStep("Navigate To CRM > Contacts > Contacts Summary Page");
			fc.utobj().printTestStep("Add Contact");
			fc.utobj().clickElement(driver, pobj.contactsLinks);
			fc.utobj().clickElement(driver, pobj.addNew);
			fc.utobj().clickElement(driver, pobj.contactLnk);

			String cFname = fc.utobj().generateTestData(dataSet.get("cFname"));
			String cLname = fc.utobj().generateTestData(dataSet.get("cLname"));
			String primaryCMethod = dataSet.get("primaryCMethod");
			String assignTo = "Corporate";
			String regionName = "";
			String franchiseId = "";
			String title = dataSet.get("title");

			addContactGenericWithAccount(driver, contactType, cFname, cLname, primaryCMethod, assignTo,
					corpUser.getuserFullName(), regionName, franchiseId, title, emailId, config, accountName);

			fc.utobj().printTestStep("Add Opportunity");
			fc.utobj().clickElement(driver, pobj.contactsLinks);
			fc.utobj().clickElement(driver, pobj.addNew);
			fc.utobj().clickElement(driver, pobj.OpportunityLnk);

			String opportunityName = fc.utobj().generateTestData(dataSet.get("opportunityName"));
			String contactName = cFname + " " + cLname;
			String opportunityType = dataSet.get("opportunityType");
			String opportunitySource = dataSet.get("opportunitySource");
			String salesAmount = dataSet.get("salesAmount");
			accountPage.addOpportunity(driver, opportunityName, accountName, contactName, corpUser.getuserFullName(),
					stage, opportunityType, opportunitySource, salesAmount, config);

			fc.utobj().clickElement(driver, pobj.contactsLinks);
			contactsFilter(driver, contactType);

			fc.utobj().printTestStep("Add Transaction");

			fc.utobj().actionImgOption(driver, contactName, "Add Transaction");
			fc.utobj().sendKeys(driver, pobj.invoiceDate, fc.utobj().getFutureDateUSFormat(-1));
			fc.utobj().sendKeys(driver, pobj.opportunityName, opportunityName);

			fc.utobj().clickElementWithoutMove(driver, fc.utobj().getElement(driver, pobj.ajaxResult));

			fc.utobj().clickElement(driver, pobj.selectProductAndService);
			fc.utobj().clickElement(driver, pobj.uncheckAll);
			fc.utobj().sendKeys(driver, pobj.searchProduct, productName);
			fc.utobj().clickElement(driver, pobj.checkAll);
			fc.utobj().clickElement(driver, pobj.okayBtn);
			fc.utobj().clickElement(driver, pobj.submitBtn);

			fc.utobj().clickElement(driver, pobj.contactsLinks);
			contactsFilter(driver, contactType);

			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//a[contains(text () , '" + contactName + "')]"));

			fc.utobj().clickElement(driver, pobj.transactionTab);
			fc.utobj().printTestStep("Verify Transaction");
			boolean isOppPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () , '" + opportunityName + "')]");
			if (isOppPresent == false) {
				fc.utobj().throwsException("was not able to verify Transaction");
			}

			boolean isContactPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () , '" + contactName + "')]");
			if (isContactPresent == false) {
				fc.utobj().throwsException("was not able to verify Contact Name");
			}

			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//td[contains(text () , '" + opportunityName + "')]/preceding-sibling::td/a"));

			boolean isProductAvailable = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () ,'" + productName + "')]");
			if (isProductAvailable == false) {
				fc.utobj().throwsException("was not able to verify Product");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "crm", "crmcheck" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2017-06-28", testCaseDescription = "Verify The Add Remarks At CRM > Contact > Contact Summary", testCaseId = "TC_166_Add_Remarks")
	public void addRemarksActionImage() throws IOException, Exception, ParserConfigurationException, SAXException {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			CRMContactsPage pobj = new CRMContactsPage(driver);

			fc.utobj().printTestStep("Navigate To Admin Users Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			String emailId = "crmautomation@staffex.com";
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUser = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporateUser.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			fc.utobj().printTestStep("Navigate To Admin > CRM > Contact Type Configuration");
			fc.utobj().printTestStep("Add Contact Type");
			AdminCRMContactTypeConfigurationPageTest contatcTypePage = new AdminCRMContactTypeConfigurationPageTest();
			String contactType = fc.utobj().generateTestData(dataSet.get("contactType"));
			contatcTypePage.addContactType(driver, contactType);

			fc.utobj().printTestStep("Navigate To CRM > Contacts > Contacts Summary Page");
			fc.utobj().printTestStep("Add Contact");
			fc.crm().crm_common().CRMContactsLnk(driver);
			fc.utobj().clickElement(driver, pobj.addNew);
			fc.utobj().clickElement(driver, pobj.contactLnk);

			String firstName = fc.utobj().generateTestData(dataSet.get("firstName"));
			String lastName = fc.utobj().generateTestData(dataSet.get("lastName"));
			String primaryCMethod = dataSet.get("primaryCMethod");
			String assignTo = "Corporate";
			String regionName = "";
			String franchiseId = "";
			String title = dataSet.get("title");

			addContactGeneric(driver, contactType, firstName, lastName, primaryCMethod, assignTo,
					corpUser.getuserFullName(), regionName, franchiseId, title, emailId, config);
			/* fc.utobj().clickElement(driver, pobj.saveBtn); */

			fc.utobj().clickElement(driver, pobj.contactsLinks);
			contactsFilter(driver, contactType);
			fc.utobj().printTestStep("Add Remarks");
			fc.utobj().actionImgOption(driver, firstName + " " + lastName, "Add Remarks");

			fc.commonMethods().switch_cboxIframe_frameId(driver);
			String remarks = fc.utobj().generateTestData(dataSet.get("remark"));
			fc.utobj().sendKeys(driver, pobj.remarks, remarks);
			fc.utobj().clickElement(driver, pobj.addBtn);
			fc.utobj().clickElement(driver, pobj.closeBtn);
			fc.utobj().switchFrameToDefault(driver);

			contactsFilter(driver, contactType);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//a[contains(text () ,'" + firstName + " " + lastName + "')]"));
			fc.utobj().printTestStep("Verify Remarks");
			boolean isRemarksPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[@title='Remark']/span[.='" + remarks + "']");
			if (isRemarksPresent == false) {
				fc.utobj().throwsException("was not able to verify remarks");
			}

			fc.utobj().printTestStep("Verify Remarks At Detailed History Page");
			fc.utobj().clickElement(driver, pobj.detailedHistoryLink);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().clickElement(driver, pobj.remarksTabAtCbox);

			boolean isRemarksAtCboxPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () ,'" + remarks + "')]");
			if (isRemarksAtCboxPresent == false) {
				fc.utobj().throwsException("was not able to verify Remarks At Detailed History Cbox");
			}
			fc.utobj().clickElement(driver, pobj.closeBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "crm")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Send Email Action Image Options At CRM > Contact > Contact Summary", testCaseId = "TC_167_Send_Email")
	public void sendEmailActionImage() throws IOException, Exception, ParserConfigurationException, SAXException {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());
		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			CRMContactsPage pobj = new CRMContactsPage(driver);

			String parentWindowsHandle = driver.getWindowHandle();

			fc.utobj().printTestStep("Navigate To Admin Users Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			String emailId = "crmautomation@staffex.com";
			//String emailId=fc.utobj().generateTestData("crmautomation")+"@staffex.com";
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUser = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporateUser.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			fc.utobj().printTestStep("Navigate To Admin > CRM > Contact Type Configuration");
			fc.utobj().printTestStep("Add Contact Type");
			AdminCRMContactTypeConfigurationPageTest contatcTypePage = new AdminCRMContactTypeConfigurationPageTest();
			String contactType = fc.utobj().generateTestData(dataSet.get("contactType"));
			contatcTypePage.addContactType(driver, contactType);

			fc.utobj().printTestStep("Navigate To CRM > Contacts > Contacts Summary Page");
			fc.utobj().printTestStep("Add Contact");
			fc.crm().crm_common().CRMContactsLnk(driver);
			fc.utobj().clickElement(driver, pobj.addNew);
			fc.utobj().clickElement(driver, pobj.contactLnk);

			String firstName = fc.utobj().generateTestData(dataSet.get("firstName"));
			String lastName = fc.utobj().generateTestData(dataSet.get("lastName"));
			String primaryCMethod = dataSet.get("primaryCMethod");
			String assignTo = "Corporate";
			String regionName = "";
			String franchiseId = "";
			String title = dataSet.get("title");

			addContactGeneric(driver, contactType, firstName, lastName, primaryCMethod, assignTo,
					corpUser.getuserFullName(), regionName, franchiseId, title, emailId, config);
			/* fc.utobj().clickElement(driver, pobj.saveBtn); */

			fc.utobj().clickElement(driver, pobj.contactsLinks);
			contactsFilter(driver, contactType);
			fc.utobj().printTestStep("Send Email");
			fc.utobj().actionImgOption(driver, firstName + " " + lastName, "Send Email");

			if (!fc.utobj().isSelected(driver, pobj.leadOwnerId)) {
				fc.utobj().clickElement(driver, pobj.leadOwnerId);
			}

			String mailCC = dataSet.get("mailCC");
			fc.utobj().sendKeys(driver, pobj.mailCC, mailCC);

			if (!fc.utobj().isSelected(driver,pobj.showBcc)) {
				fc.utobj().clickElement(driver, pobj.showBcc);
			}
			String mailBCC = dataSet.get("mailBCC");
			fc.utobj().sendKeys(driver, pobj.mailBCC, mailBCC);

			String subjectMail = fc.utobj().generateTestData(dataSet.get("subjectMail"));
			fc.utobj().sendKeys(driver, pobj.subjectMail, subjectMail);

			String editorText = fc.utobj().generateTestData(dataSet.get("mailText"));

			fc.utobj().switchFrameById(driver, "ta_ifr");

			Actions actions = new Actions(driver);
			actions.moveToElement(fc.utobj().getElement(driver, pobj.editorTextArea));
			actions.click();
			actions.sendKeys(editorText);
			fc.utobj().logReportMsg("Entered Text", editorText);
			actions.build().perform();
			fc.utobj().switchFrameToDefault(driver);
			fc.utobj().clickElement(driver, pobj.sendEmailBtn);
			fc.utobj().switchFrameToDefault(driver);

			contactsFilter(driver, contactType);
			fc.utobj().printTestStep("Verify Send Email At Detailed History Frame");
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//a[contains(text () , '" + firstName + " " + lastName + "')]"));
			fc.utobj().clickElement(driver, pobj.detailedHistoryLink);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().clickElement(driver, pobj.emailsTab);
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//*[contains(text () ,'" + firstName + "')]"));

			Set<String> allWindows = driver.getWindowHandles();
			for (String currentWindow : allWindows) {

				if (!currentWindow.equalsIgnoreCase(parentWindowsHandle)) {

					driver.switchTo().window(currentWindow);
					try {

						if (fc.utobj().getElementByXpath(driver, ".//*[.='Email Details']").isDisplayed()) {

							boolean isFromTextPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
									".//*[contains(text () ,'From')]/ancestor::tr/td[contains(text () , '"
											+ corpUser.getuserFullName() + "')]");
							if (isFromTextPresent == false) {
								fc.utobj().throwsException("was not able to From Text");
							}

							boolean isToTextPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
									".//*[contains(text () ,'To')]/ancestor::tr/td[contains(text () , '" + firstName
											+ " " + lastName + "')]");
							if (isToTextPresent == false) {
								fc.utobj().throwsException("was not able to To Text");
							}
							boolean isSubjectTextPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
									".//*[contains(text () ,'To')]/ancestor::tr/td[contains(text () , '" + subjectMail
											+ "')]");
							if (isSubjectTextPresent == false) {
								fc.utobj().throwsException("was not able to Subject");
							}
							boolean isTextPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//*[.='" + editorText + "']");
							if (isTextPresent == false) {
								fc.utobj().throwsException("was not able to verify Editor text");
							}
						}

					} catch (Exception e) {
						driver.close();
					}
				}
				driver.switchTo().window(parentWindowsHandle);
			}
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().clickElement(driver, pobj.closeBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "crm")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Log A Task By Action Btn At CRM > Contact > Contact Summary", testCaseId = "TC_168_Log_Task")
	public void logATaskActionBtn() throws IOException, Exception, ParserConfigurationException, SAXException {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			CRMContactsPage pobj = new CRMContactsPage(driver);

			/*
			 * fc.utobj().printTestStep(testCaseId,
			 * "Navigate To Admin Users Manage Corporate User");
			 * fc.utobj().printTestStep(testCaseId, "Add Corporate User");
			 * String emailId="crmautomation@staffex.com";
			 * AdminUsersManageCorporateUsersAddCorporateUserPageTest
			 * corporateUser=new
			 * AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			 * CorporateUser corpUser = new
			 * CorporateUser();corpUser.setEmail(emailId);corpUser =
			 * fc.CommonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			 * corporateUser.
			 * createDefaultCorporateUser_Through_WebService_AfterLogin(driver,
			 * corpUser);
			 */

			fc.utobj().printTestStep("Navigate To Admin Users Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			String emailId = "crmautomation@staffex.com";
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUser = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporateUser.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			fc.utobj().printTestStep("Navigate To Admin > CRM > Contact Type Configuration");
			fc.utobj().printTestStep("Add Contact Type");
			AdminCRMContactTypeConfigurationPageTest contatcTypePage = new AdminCRMContactTypeConfigurationPageTest();
			String contactType = fc.utobj().generateTestData(dataSet.get("contactType"));
			contatcTypePage.addContactType(driver, contactType);

			fc.utobj().printTestStep("Navigate To CRM > Contacts > Contacts Summary Page");
			fc.utobj().printTestStep("Add Contact");
			fc.crm().crm_common().CRMContactsLnk(driver);
			fc.utobj().clickElement(driver, pobj.addNew);
			fc.utobj().clickElement(driver, pobj.contactLnk);

			String firstName = fc.utobj().generateTestData(dataSet.get("firstName"));
			String lastName = fc.utobj().generateTestData(dataSet.get("lastName"));
			String primaryCMethod = dataSet.get("primaryCMethod");
			String assignTo = "Corporate";
			String regionName = "";
			String franchiseId = "";
			String title = dataSet.get("title");

			addContactGeneric(driver, contactType, firstName, lastName, primaryCMethod, assignTo,
					corpUser.getuserFullName(), regionName, franchiseId, title, emailId, config);
			/* fc.utobj().clickElement(driver, pobj.saveBtn); */

			fc.utobj().clickElement(driver, pobj.contactsLinks);
			contactsFilter(driver, contactType);
			fc.utobj().printTestStep("Log A Task");
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//a[contains(text () ,'" + firstName
					+ " " + lastName + "')]/ancestor::tr/td/input[@name='checkb']"));
			fc.utobj().selectActionMenuItemsWithTagA(driver, "Log a Task");

			fc.commonMethods().switch_cboxIframe_frameId(driver);
			if (!fc.utobj().isSelected(driver,pobj.radioOwner)) {
				fc.utobj().clickElement(driver, pobj.radioOwner);
			}
			String statusTask = dataSet.get("statusTask");
			fc.utobj().selectDropDown(driver, pobj.statusTask, statusTask);
			String subject = fc.utobj().generateTestData(dataSet.get("subject"));
			fc.utobj().sendKeys(driver, pobj.subject, subject);
			if (!fc.utobj().isSelected(driver, pobj.timeLessTask)) {
				fc.utobj().clickElement(driver, pobj.timeLessTask);
			}
			String priority = dataSet.get("priority");
			fc.utobj().selectDropDown(driver, pobj.priority, priority);
			fc.utobj().sendKeys(driver, pobj.comments, "Test Comment");
			fc.utobj().clickElement(driver, pobj.createBtn);
			fc.utobj().switchFrameToDefault(driver);

			contactsFilter(driver, contactType);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//a[contains(text () ,'" + firstName + " " + lastName + "')]"));

			fc.utobj().printTestStep("Verify Task");
			boolean isSubjectPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[contains(text () ,'" + subject + "')]");
			if (isSubjectPresent == false) {
				fc.utobj().throwsException("was not able to verify subject");
			}
			boolean isUserPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[contains(text () ,'" + subject
					+ "')]/ancestor::tr/td/div[.='" + corpUser.getuserFullName() + "']");
			if (isUserPresent == false) {
				fc.utobj().throwsException("was not able to verify userName at Lead Info Page");
			}
			boolean isStatusOfTaskPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[contains(text () ,'" + subject
					+ "')]/ancestor::tr/td[contains(text () ,'" + statusTask + "')]");
			if (isStatusOfTaskPresent == false) {
				fc.utobj().throwsException("was not able to verify status of task");
			}
			boolean isPriorityPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[contains(text () ,'" + subject
					+ "')]/ancestor::tr/td[contains(text () ,'" + priority + "')]");
			if (isPriorityPresent == false) {
				fc.utobj().throwsException("was not able to verify Priority of task");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "crm", "TC_169_Send_Email"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Send Email By Action Btn Options At CRM > Contact > Contact Summary", testCaseId = "TC_169_Send_Email")
	public void sendEmailActionBtn() throws IOException, Exception, ParserConfigurationException, SAXException {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			CRMContactsPage pobj = new CRMContactsPage(driver);

			String parentWindowsHandle = driver.getWindowHandle();
			/*
			 * fc.utobj().printTestStep(testCaseId,
			 * "Navigate To Admin Users Manage Corporate User");
			 * fc.utobj().printTestStep(testCaseId, "Add Corporate User");
			 * 
			 * AdminUsersManageCorporateUsersAddCorporateUserPageTest corpTest =
			 * new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			 * String userName =
			 * fc.utobj().generateTestData(dataSet.get("userName")); String
			 * emailId="crmautomation@staffex.com"; userName =
			 * corpTest.addCorporateUser(driver, userName, config , emailId);
			 */

			fc.utobj().printTestStep("Navigate To Admin Users Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			String emailId = "crmautomation@staffex.com";
			//String emailId=fc.utobj().generateTestData("crmautomation")+"@staffex.com";
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUser = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporateUser.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			fc.utobj().printTestStep("Navigate To Admin > CRM > Contact Type Configuration");
			fc.utobj().printTestStep("Add Contact Type");
			AdminCRMContactTypeConfigurationPageTest contatcTypePage = new AdminCRMContactTypeConfigurationPageTest();
			String contactType = fc.utobj().generateTestData(dataSet.get("contactType"));
			contatcTypePage.addContactType(driver, contactType);

			fc.utobj().printTestStep("Navigate To CRM > Contacts > Contacts Summary Page");
			fc.utobj().printTestStep("Add Contact");
			fc.crm().crm_common().CRMContactsLnk(driver);
			fc.utobj().clickElement(driver, pobj.addNew);
			fc.utobj().clickElement(driver, pobj.contactLnk);

			String firstName = fc.utobj().generateTestData(dataSet.get("firstName"));
			String lastName = fc.utobj().generateTestData(dataSet.get("lastName"));
			String primaryCMethod = dataSet.get("primaryCMethod");
			String assignTo = "Corporate";
			String regionName = "";
			String franchiseId = "";
			String title = dataSet.get("title");

			addContactGeneric(driver, contactType, firstName, lastName, primaryCMethod, assignTo,
					corpUser.getuserFullName(), regionName, franchiseId, title, emailId, config);

			fc.utobj().clickElement(driver, pobj.contactsLinks);
			contactsFilter(driver, contactType);
			fc.utobj().printTestStep("Send Email");
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//a[contains(text () ,'" + firstName
					+ " " + lastName + "')]/ancestor::tr/td/input[@name='checkb']"));
			fc.utobj().selectActionMenuItemsWithTagA(driver, "Send Email");

			if (!fc.utobj().isSelected(driver, pobj.leadOwnerId)) {
				fc.utobj().clickElement(driver, pobj.leadOwnerId);
			}

			String mailCC = dataSet.get("mailCC");
			fc.utobj().sendKeys(driver, pobj.mailCC, mailCC);

			if (!fc.utobj().isSelected(driver,pobj.showBcc)) {
				fc.utobj().clickElement(driver, pobj.showBcc);
			}
			String mailBCC = dataSet.get("mailBCC");
			fc.utobj().sendKeys(driver, pobj.mailBCC, mailBCC);

			String subjectMail = fc.utobj().generateTestData(dataSet.get("subjectMail"));
			fc.utobj().sendKeys(driver, pobj.subjectMail, subjectMail);

			String editorText = fc.utobj().generateTestData(dataSet.get("mailText"));

			fc.utobj().switchFrameById(driver, "ta_ifr");

			Actions actions = new Actions(driver);
			actions.moveToElement(fc.utobj().getElement(driver, pobj.editorTextArea));
			actions.click();
			actions.sendKeys(editorText);
			fc.utobj().logReportMsg("Entered Text", editorText);
			actions.build().perform();
			fc.utobj().switchFrameToDefault(driver);
			fc.utobj().clickElement(driver, pobj.sendEmailBtn);
			fc.utobj().switchFrameToDefault(driver);

			contactsFilter(driver, contactType);
			fc.utobj().printTestStep("Verify Send Email At Detailed History Frame");
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//a[contains(text () , '" + firstName + " " + lastName + "')]"));
			fc.utobj().clickElement(driver, pobj.detailedHistoryLink);

			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().clickElement(driver, pobj.emailsTab);
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//*[contains(text () ,'" + firstName + "')]"));

			Set<String> allWindows = driver.getWindowHandles();
			for (String currentWindow : allWindows) {

				if (!currentWindow.equalsIgnoreCase(parentWindowsHandle)) {

					driver.switchTo().window(currentWindow);
					try {

						if (fc.utobj().getElementByXpath(driver, ".//*[.='Email Details']").isDisplayed()) {

							boolean isFromTextPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, "");
							if (isFromTextPresent == false) {
								fc.utobj().throwsException("was not able to From Text");
							}

							boolean isToTextPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
									".//*[contains(text () ,'To')]/ancestor::tr/td[contains(text () , '" + firstName
											+ " " + lastName + "')]");
							if (isToTextPresent == false) {
								fc.utobj().throwsException("was not able to To Text");
							}
							boolean isSubjectTextPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
									".//*[contains(text () ,'To')]/ancestor::tr/td[contains(text () , '" + subjectMail
											+ "')]");
							if (isSubjectTextPresent == false) {
								fc.utobj().throwsException("was not able to Subject");
							}
							boolean isTextPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//*[.='" + editorText + "']");
							if (isTextPresent == false) {
								fc.utobj().throwsException("was not able to verify Editor text");
							}
						}

					} catch (Exception e) {
						driver.close();
					}
				}
				driver.switchTo().window(parentWindowsHandle);
			}
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().clickElement(driver, pobj.closeBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = {"crm"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Merge Contacts Action Btn Options AT CRM > Contacts > Contact Summary", testCaseId = "TC_170_Merge_Contact")
	public void mergeContactsActionBtn() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			CRMAccountsPage pobj = new CRMAccountsPage(driver);

			CRMAccountsPageTest accountPage = new CRMAccountsPageTest();

			/*
			 * fc.utobj().printTestStep(testCaseId,
			 * "Navigate To Admin Users Manage Corporate User");
			 * fc.utobj().printTestStep(testCaseId, "Add Corporate User");
			 * String emailId="crmautomation@staffex.com";
			 * AdminUsersManageCorporateUsersAddCorporateUserPageTest
			 * corporateUser=new
			 * AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			 * CorporateUser corpUser = new
			 * CorporateUser();corpUser.setEmail(emailId);corpUser =
			 * fc.CommonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			 * corporateUser.
			 * createDefaultCorporateUser_Through_WebService_AfterLogin(driver,
			 * corpUser);
			 */

			fc.utobj().printTestStep("Navigate To Admin Users Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			String emailId = "crmautomation@staffex.com";
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUser = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporateUser.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			fc.utobj().printTestStep("Navigating to Admin > CRM > ContactType Configuration");
			fc.utobj().printTestStep("Add Contact Type");
			AdminCRMContactTypeConfigurationPageTest contactTypePage = new AdminCRMContactTypeConfigurationPageTest();
			String contactType = fc.utobj().generateTestData(dataSet.get("contactType"));
			contactTypePage.addContactType(driver, contactType);

			AdminCRMConfigureIndustryPageTest industryPage = new AdminCRMConfigureIndustryPageTest();
			String industry = fc.utobj().generateTestData(dataSet.get("industry"));
			String description = fc.utobj().generateTestData(dataSet.get("description"));
			fc.utobj().printTestStep("Navigating to Admin > CRM >  Configure Industry Type");
			fc.utobj().printTestStep("Add Industry");
			industryPage.addIndustry(driver, industry, description);

			String accountName = fc.utobj().generateTestData(dataSet.get("accountName"));
			String visibility = dataSet.get("visibility");
			fc.utobj().printTestStep("Navigating to CRM >  Accounts > Accounts Summary");
			fc.utobj().printTestStep("Add Account");
			accountPage.addAcocunt(driver, industry, accountName, visibility);

			fc.utobj().printTestStep("Navigate To CRM > Contacts > Contacts Summary Page");
			fc.utobj().printTestStep("Add Contact");
			fc.utobj().clickElement(driver, pobj.contactsLinks);
			fc.utobj().clickElement(driver, pobj.addNew);
			fc.utobj().clickElement(driver, pobj.contactLnk);

			String cFname = fc.utobj().generateTestData(dataSet.get("cFname"));
			String cLname = fc.utobj().generateTestData(dataSet.get("cLname"));
			String primaryCMethod = dataSet.get("primaryCMethod");
			String assignTo = "Corporate";
			String regionName = "";
			String franchiseId = "";
			String title = dataSet.get("title");

			addContactGenericWithAccount(driver, contactType, cFname, cLname, primaryCMethod, assignTo,
					corpUser.getuserFullName(), regionName, franchiseId, title, emailId, config, accountName);

			fc.utobj().printTestStep("Add Another Contact For Merge");
			fc.utobj().clickElement(driver, pobj.contactsLinks);
			fc.utobj().clickElement(driver, pobj.addNew);
			fc.utobj().clickElement(driver, pobj.contactLnk);

			fc.utobj().selectDropDown(driver, pobj.contactType, contactType);
			fc.utobj().selectDropDown(driver, pobj.title, title);
			String cFname1 = fc.utobj().generateTestData(dataSet.get("cFname"));
			String cLname1 = fc.utobj().generateTestData(dataSet.get("cLname"));
			fc.utobj().sendKeys(driver, pobj.contactFirstName, cFname1);
			fc.utobj().sendKeys(driver, pobj.contactLastName, cLname1);

			if (!fc.utobj().isSelected(driver, pobj.assignToCorporate)) {
				fc.utobj().clickElement(driver, pobj.assignToCorporate);
			}
			fc.utobj().selectDropDown(driver, pobj.selectCorporateUserContact, corpUser.getuserFullName());
			fc.utobj().sendKeys(driver, pobj.accountName, accountName);

			fc.utobj().sleep(2000);
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//*[@id='customizedAjaxSearch']/div"));
			fc.utobj().clickElement(driver, pobj.saveBtn);

			fc.utobj().clickElement(driver, pobj.contactsLinks);
			contactsFilter(driver, contactType);

			fc.utobj().printTestStep("Merge Contacts");
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//a[contains(text () ,'" + cFname + " " + cLname + "')]/ancestor::tr/td/input[@name='checkb']"));
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//a[contains(text () ,'" + cFname1
					+ " " + cLname1 + "')]/ancestor::tr/td/input[@name='checkb']"));
			fc.utobj().selectActionMenuItemsWithTagA(driver, "Merge Contacts");

			if (!fc.utobj().isSelected(driver, pobj.secondNameSelect)) {
				fc.utobj().clickElement(driver, pobj.secondNameSelect);
			}

			String secondName = fc.utobj().getElementByXpath(driver, ".//*[@value='second' and @name='name']/..")
					.getText().trim();

			fc.utobj().sendKeys(driver, pobj.remarksAtMerge, "Merge Remarks");
			fc.utobj().clickElement(driver, pobj.mergeBtn);

			fc.utobj().clickElement(driver, pobj.contactsLinks);
			contactsFilter(driver, contactType);

			fc.utobj().webDriverWait(driver, ".//a[contains(text () ,'" + secondName + "')]");

			fc.utobj().printTestStep("Verify Merge Contacts");
			boolean isMergeContactNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondName + "')]");
			if (isMergeContactNamePresent == false) {
				fc.utobj().throwsException("was not able to verify contact Name");
			}

			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//a[contains(text () ,'" + secondName + "')]"));
			boolean isCNamePresentAtPrimaryInfo = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () ,'Name')]/ancestor::tr/td[.='" + title + " " + secondName + "']");
			if (isCNamePresentAtPrimaryInfo == false) {
				fc.utobj().throwsException("was not able to verify Contact Name At Primary Info Page");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "crm", "crmemail" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify the Change Owner Action Btn At CRM > Contact > Contact Summary", testCaseId = "TC_171_Change_Owner")
	public void changeOwnerActionBtn() throws IOException, Exception, ParserConfigurationException, SAXException {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			CRMContactsPage pobj = new CRMContactsPage(driver);

			fc.utobj().printTestStep("Navigate To Admin > Users > Manage Regional User");
			fc.utobj().printTestStep("Add Regional User");
			AdminUsersManageRegionalUsersAddRegionalUserPageTest regionalUserPage = new AdminUsersManageRegionalUsersAddRegionalUserPageTest();
			String userNameReg = fc.utobj().generateTestData(dataSet.get("userNameReg"));
			String region = fc.utobj().generateTestData(dataSet.get("regionName"));
			String emailId = "crmautomation@staffex.com";
			regionalUserPage.addRegionalUser(driver, userNameReg, region, emailId);

			fc.utobj().printTestStep("Navigate To Admin Users Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");

			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUser = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporateUser.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			fc.utobj().printTestStep("Navigate To Admin > CRM > Contact Type Configuration");
			fc.utobj().printTestStep("Add Contact Type");
			AdminCRMContactTypeConfigurationPageTest contatcTypePage = new AdminCRMContactTypeConfigurationPageTest();
			String contactType = fc.utobj().generateTestData(dataSet.get("contactType"));
			contatcTypePage.addContactType(driver, contactType);

			fc.utobj().printTestStep("Navigate To CRM > Contacts > Contacts Summary Page");
			fc.utobj().printTestStep("Add Contact");
			fc.crm().crm_common().CRMContactsLnk(driver);
			fc.utobj().clickElement(driver, pobj.addNew);
			fc.utobj().clickElement(driver, pobj.contactLnk);

			String firstName = fc.utobj().generateTestData(dataSet.get("firstName"));
			String lastName = fc.utobj().generateTestData(dataSet.get("lastName"));
			String primaryCMethod = dataSet.get("primaryCMethod");
			String assignTo = "Corporate";
			String regionName = "";
			String franchiseId = "";
			String title = dataSet.get("title");

			addContactGeneric(driver, contactType, firstName, lastName, primaryCMethod, assignTo,
					corpUser.getuserFullName(), regionName, franchiseId, title, emailId, config);

			fc.utobj().clickElement(driver, pobj.contactsLinks);
			contactsFilter(driver, contactType);
			fc.utobj().printTestStep("Change Owner");
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//a[contains(text () ,'" + firstName
					+ " " + lastName + "')]/ancestor::tr/td/input[@name='checkb']"));
			fc.utobj().selectActionMenuItemsWithTagA(driver, "Change Owner");
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			if (!fc.utobj().isSelected(driver, pobj.selectRegionalUser1)) {
				fc.utobj().clickElement(driver, pobj.selectRegionalUser1);
			}
			fc.utobj().selectDropDown(driver, pobj.selectAreaRegion, region);
			fc.utobj().selectDropDown(driver, pobj.selectRegionalUser, userNameReg + " " + userNameReg);
			fc.utobj().clickElement(driver, pobj.assignBtn);
			fc.utobj().clickElement(driver, pobj.closeBtn);
			fc.utobj().switchFrameToDefault(driver);

			contactsFilter(driver, contactType);
			fc.utobj().printTestStep("Verify Change Owner");
			boolean isOwnerPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[contains(text () ,'" + firstName + " "
					+ lastName + "')]/ancestor::tr/td[contains(text () ,'" + userNameReg + " " + userNameReg + "')]");
			if (isOwnerPresent == false) {
				fc.utobj().throwsException("was not able to verify Owner");
			}
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//a[contains(text () ,'" + firstName + " " + lastName + "')]"));

			boolean isRegionNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () ,'Area / Region')]/ancestor::tr/td[.='" + region + "']");
			if (isRegionNamePresent == false) {
				fc.utobj().throwsException("was not able to verify Region name CRM > Contact > Primary Info");
			}

			boolean isRegionalUserPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () ,'Assign To')]/ancestor::tr/td[contains(text () ,'" + userNameReg + " "
							+ userNameReg + "')]");
			if (isRegionalUserPresent == false) {
				fc.utobj().throwsException("was not able to verify Regional User 'Owner' CRM > Contact > Primary Info");
			}

			fc.utobj().printTestStep("Verify Contact Assignment");

			Map<String, String> mailData = new HashMap<String, String>();

			mailData = fc.utobj().readMailBox("Contact Assigned", firstName, emailId, "sdg@1a@Hfs");

			if (mailData.size() == 0 || !mailData.get("mailBody").contains("Contact Assigned")) {

				fc.utobj().throwsException("was not able to verify The Mail After Contact Assigned");
			}

			if (mailData.size() == 0 || !mailData.get("mailBody").contains(contactType)) {

				fc.utobj().throwsException("was not able to verify Contact Type In Contact Assigned Mail");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "crm")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify the Change Status Action Btn At CRM > Contact > Contact Summary", testCaseId = "TC_172_Change_Status")
	public void changeStatusActionBtn() throws IOException, Exception, ParserConfigurationException, SAXException {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			CRMContactsPage pobj = new CRMContactsPage(driver);

			/*
			 * fc.utobj().printTestStep(testCaseId,
			 * "Navigate To Admin Users Manage Corporate User");
			 * fc.utobj().printTestStep(testCaseId, "Add Corporate User");
			 * String emailId="crmautomation@staffex.com";
			 * AdminUsersManageCorporateUsersAddCorporateUserPageTest
			 * corporateUser=new
			 * AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			 * CorporateUser corpUser = new
			 * CorporateUser();corpUser.setEmail(emailId);corpUser =
			 * fc.CommonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			 * corporateUser.
			 * createDefaultCorporateUser_Through_WebService_AfterLogin(driver,
			 * corpUser);
			 */

			fc.utobj().printTestStep("Navigate To Admin Users Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			String emailId = "crmautomation@staffex.com";
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUser = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporateUser.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			fc.utobj().printTestStep("Navigate To Admin > CRM > Contact Type Configuration");
			fc.utobj().printTestStep("Add Contact Type");
			AdminCRMContactTypeConfigurationPageTest contatcTypePage = new AdminCRMContactTypeConfigurationPageTest();
			String contactType = fc.utobj().generateTestData(dataSet.get("contactType"));
			contatcTypePage.addContactType(driver, contactType);

			fc.utobj().printTestStep("Navigate To Admin > CRM > Configuration Status");
			fc.utobj().printTestStep("Add Status For Contact");
			AdminCRMConfigureStatusPageTest statusPage = new AdminCRMConfigureStatusPageTest();
			String statusName = fc.utobj().generateTestData(dataSet.get("status"));
			statusPage.addStatus01(driver, statusName);

			fc.utobj().printTestStep("Navigate To CRM > Contacts > Contacts Summary Page");
			fc.utobj().printTestStep("Add Contact");
			fc.crm().crm_common().CRMContactsLnk(driver);
			fc.utobj().clickElement(driver, pobj.addNew);
			fc.utobj().clickElement(driver, pobj.contactLnk);

			String firstName = fc.utobj().generateTestData(dataSet.get("firstName"));
			String lastName = fc.utobj().generateTestData(dataSet.get("lastName"));
			String primaryCMethod = dataSet.get("primaryCMethod");
			String assignTo = "Corporate";
			String regionName = "";
			String franchiseId = "";
			String title = dataSet.get("title");

			addContactGeneric(driver, contactType, firstName, lastName, primaryCMethod, assignTo,
					corpUser.getuserFullName(), regionName, franchiseId, title, emailId, config);
			/* fc.utobj().clickElement(driver, pobj.saveBtn); */

			fc.utobj().clickElement(driver, pobj.contactsLinks);
			contactsFilter(driver, contactType);
			fc.utobj().printTestStep("Change Status");
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//a[contains(text () ,'" + firstName
					+ " " + lastName + "')]/ancestor::tr/td/input[@name='checkb']"));
			fc.utobj().selectActionMenuItemsWithTagA(driver, "Change Status");

			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().selectDropDown(driver, pobj.statusChangeTo, statusName);
			fc.utobj().sendKeys(driver, pobj.remarksAtMerge, "Test Remarks");
			fc.utobj().clickElement(driver, pobj.changeBtn);
			fc.utobj().clickElement(driver, pobj.closeBtn);
			fc.utobj().switchFrameToDefault(driver);

			contactsFilter(driver, contactType);
			fc.utobj().printTestStep("Verify Change Status");
			boolean isStatusPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[contains(text () ,'" + firstName + " "
					+ lastName + "')]/ancestor::tr/td[contains(text () ,'" + statusName + "')]");
			if (isStatusPresent == false) {
				fc.utobj().throwsException("was not able to verify Status Name");
			}

			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//a[contains(text () ,'" + firstName + " " + lastName + "')]"));
			boolean isStatusPresentAtInfo = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//select[@id='cmLeadStatusID']/option[@selected and .='" + statusName + "']");
			if (isStatusPresentAtInfo == false) {
				fc.utobj().throwsException("was not able to verify Status Name at Info Page");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "crm" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify the Change Contact Type Action Btn At CRM > Contact > Contact Summary ", testCaseId = "TC_173_Change_Contact_Type")
	public void changeContactTypeActionBtn() throws IOException, Exception, ParserConfigurationException, SAXException {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			CRMContactsPage pobj = new CRMContactsPage(driver);

			fc.utobj().printTestStep("Navigate To Admin Users Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			String emailId = "crmautomation@staffex.com";
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUser = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporateUser.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			fc.utobj().printTestStep("Navigate To Admin > CRM > Contact Type Configuration");
			fc.utobj().printTestStep("Add Contact Type");
			AdminCRMContactTypeConfigurationPageTest contatcTypePage = new AdminCRMContactTypeConfigurationPageTest();
			String contactType = fc.utobj().generateTestData(dataSet.get("contactType"));
			contatcTypePage.addContactType(driver, contactType);

			String contactType1 = fc.utobj().generateTestData(dataSet.get("contactType"));
			contatcTypePage.addContactType(driver, contactType1);

			fc.utobj().printTestStep("Navigate To CRM > Contacts > Contacts Summary Page");
			fc.utobj().printTestStep("Add Contact");
			fc.crm().crm_common().CRMContactsLnk(driver);
			fc.utobj().clickElement(driver, pobj.addNew);
			fc.utobj().clickElement(driver, pobj.contactLnk);

			String firstName = fc.utobj().generateTestData(dataSet.get("firstName"));
			String lastName = fc.utobj().generateTestData(dataSet.get("lastName"));
			String primaryCMethod = dataSet.get("primaryCMethod");
			String assignTo = "Corporate";
			String regionName = "";
			String franchiseId = "";
			String title = dataSet.get("title");

			addContactGeneric(driver, contactType, firstName, lastName, primaryCMethod, assignTo,
					corpUser.getuserFullName(), regionName, franchiseId, title, emailId, config);

			fc.utobj().clickElement(driver, pobj.contactsLinks);
			contactsFilter(driver, contactType);
			fc.utobj().printTestStep("Change Contact Type");
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//a[contains(text () ,'" + firstName
					+ " " + lastName + "')]/ancestor::tr/td/input[@name='checkb']"));
			fc.utobj().selectActionMenuItemsWithTagA(driver, "Change Contact Type");

			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().selectDropDown(driver, pobj.contactType, contactType1);
			fc.utobj().clickElement(driver, pobj.changeBtn);
			fc.utobj().clickElement(driver, pobj.closeBtn);
			fc.utobj().switchFrameToDefault(driver);

			contactsFilter(driver, contactType1);
			fc.utobj().printTestStep("Verify Change Contact Type");
			boolean isStatusPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[contains(text () ,'" + firstName + " "
					+ lastName + "')]/ancestor::tr/td[contains(text () ,'" + contactType1 + "')]");
			if (isStatusPresent == false) {
				fc.utobj().throwsException("was not able to verify Contact Type");
			}

			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//a[contains(text () ,'" + firstName + " " + lastName + "')]"));
			boolean isStatusPresentAtInfo = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () ,'Contact Type')]/ancestor::tr/td[.='" + contactType1 + "']");
			if (isStatusPresentAtInfo == false) {
				fc.utobj().throwsException("was not able to verify Contact Type at Info Page");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "crm", "TC_174_Associate_With_Email_Campaign" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify the Contact Associated With Email Campaign Action Btn At CRM > Contact > Contact Summary", testCaseId = "TC_174_Associate_With_Email_Campaign")
	public void associatesWithEmailCampaignActionBtn()
			throws IOException, Exception, ParserConfigurationException, SAXException {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			CRMContactsPage pobj = new CRMContactsPage(driver);

			fc.utobj().printTestStep("Navigate To CRM >Campaign Center");
			fc.utobj().printTestStep("Add Campaign");
			CRMCampaignCenterPageTestForCRM campaignCenterPage = new CRMCampaignCenterPageTestForCRM();
			String campaignName = fc.utobj().generateTestData(dataSet.get("campaignName"));
			String campaignDescription = fc.utobj().generateTestData(dataSet.get("campaignDescription"));
			String campaignAccessibility = dataSet.get("campaignAccessibility");
			String category = dataSet.get("category");
			String templateName = fc.utobj().generateTestData(dataSet.get("templateName"));
			String mailSubject = dataSet.get("mailSubject");
			String templateText = fc.utobj().generateTestData(dataSet.get("templateText"));
			campaignCenterPage.createCampaign(driver, campaignName, campaignDescription, campaignAccessibility,
					category, templateName, mailSubject, templateText, config);

			/*
			 * fc.utobj().printTestStep(testCaseId,
			 * "Navigate To Admin Users Manage Corporate User");
			 * fc.utobj().printTestStep(testCaseId, "Add Corporate User");
			 * String emailId="crmautomation@staffex.com";
			 * AdminUsersManageCorporateUsersAddCorporateUserPageTest
			 * corporateUser=new
			 * AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			 * CorporateUser corpUser = new
			 * CorporateUser();corpUser.setEmail(emailId);corpUser =
			 * fc.CommonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			 * corporateUser.
			 * createDefaultCorporateUser_Through_WebService_AfterLogin(driver,
			 * corpUser);
			 */

			fc.utobj().printTestStep("Navigate To Admin Users Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			String emailId = "crmautomation@staffex.com";
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUser = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporateUser.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			fc.utobj().printTestStep("Navigate To Admin > CRM > Contact Type Configuration");
			fc.utobj().printTestStep("Add Contact Type");
			AdminCRMContactTypeConfigurationPageTest contatcTypePage = new AdminCRMContactTypeConfigurationPageTest();
			String contactType = fc.utobj().generateTestData(dataSet.get("contactType"));
			contatcTypePage.addContactType(driver, contactType);

			fc.utobj().printTestStep("Navigate To CRM > Contacts > Contacts Summary Page");
			fc.utobj().printTestStep("Add Contact");
			fc.crm().crm_common().CRMContactsLnk(driver);
			fc.utobj().clickElement(driver, pobj.addNew);
			fc.utobj().clickElement(driver, pobj.contactLnk);

			String firstName = fc.utobj().generateTestData(dataSet.get("firstName"));
			String lastName = fc.utobj().generateTestData(dataSet.get("lastName"));
			String primaryCMethod = dataSet.get("primaryCMethod");
			String assignTo = "Corporate";
			String regionName = "";
			String franchiseId = "";
			String title = dataSet.get("title");

			addContactGeneric(driver, contactType, firstName, lastName, primaryCMethod, assignTo,
					corpUser.getuserFullName(), regionName, franchiseId, title, emailId, config);

			fc.utobj().clickElement(driver, pobj.contactsLinks);
			contactsFilter(driver, contactType, corpUser.getuserFullName());
			fc.utobj().printTestStep("Associate With Campaign");
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//a[contains(text () ,'" + firstName
					+ " " + lastName + "')]/ancestor::tr/td/input[@name='checkb']"));
			fc.utobj().selectActionMenuItemsWithTagA(driver, "Associate with Campaign");

			fc.utobj().clickElement(driver, pobj.filterContainer);
			fc.utobj().sendKeys(driver, pobj.searchCampaign, campaignName);
			fc.utobj().clickElement(driver, pobj.applyFilter);

			fc.utobj().clickElement(driver, new CRMLeadsPage(driver).sendCampaignBtn);

			fc.utobj().printTestStep("Verify Back / Cancel Button in confrimation page. ");
			boolean isLeadBackButtonPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//*[@id='cancelConfirmation']");
			if (isLeadBackButtonPresent == false) {
				fc.utobj().throwsException("was not able to verify Cancel button is appearing properly.");
			} else {
				fc.utobj().printTestStep("verified that the Cancel button is appearing properly.");
			}
			isLeadBackButtonPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//*[@id='backConfirmation']");
			if (isLeadBackButtonPresent == false) {
				fc.utobj().throwsException("was not able to verify Back button is appearing properly.");
			} else {
				fc.utobj().printTestStep("verified that the Back button is appearing properly.");
			}

			fc.utobj().clickElement(driver, pobj.confirmBtn);

			fc.utobj().clickElement(driver, pobj.contactsLinks);
			contactsFilter(driver, contactType, corpUser.getuserFullName());

			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//a[contains(text () ,'" + firstName + " " + lastName + "')]"));

			fc.utobj().printTestStep("Verify Contact Associate With Campaign");
			boolean isCampaignNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () ,'Promotional Campaign')]/following-sibling::td[.='" + campaignName + "']");
			if (isCampaignNamePresent == false) {
				fc.utobj().throwsException("was not able to verify Campign Name");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "crm"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify the Contact Add To Group By Action Btn At CRM > Contact > Contact Summary", testCaseId = "TC_175_Add_To_Group")
	public void addToGroupActionBtn() throws IOException, Exception, ParserConfigurationException, SAXException {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			CRMContactsPage pobj = new CRMContactsPage(driver);
			CRMGroupsPageTest groupPage = new CRMGroupsPageTest();
			CRMAccountsPageTest accountPage = new CRMAccountsPageTest();

			fc.utobj().printTestStep("Navigate To Admin Users Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			String emailId = "crmautomation@staffex.com";
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUser = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporateUser.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			fc.utobj().printTestStep("Navigate To Admin > CRM > Contact Type Configuration");
			fc.utobj().printTestStep("Add Contact Type");
			AdminCRMContactTypeConfigurationPageTest contatcTypePage = new AdminCRMContactTypeConfigurationPageTest();
			String contactType = fc.utobj().generateTestData(dataSet.get("contactType"));
			contatcTypePage.addContactType(driver, contactType);

			fc.utobj().printTestStep("Navigate To CRM > Contacts > Contacts Summary Page");
			fc.utobj().printTestStep("Add Contact");
			fc.crm().crm_common().CRMContactsLnk(driver);
			fc.utobj().clickElement(driver, pobj.addNew);
			fc.utobj().clickElement(driver, pobj.contactLnk);

			String firstName = fc.utobj().generateTestData(dataSet.get("firstName"));
			String lastName = fc.utobj().generateTestData(dataSet.get("lastName"));
			String primaryCMethod = dataSet.get("primaryCMethod");
			String assignTo = "Corporate";
			String regionName = "";
			String franchiseId = "";
			String title = dataSet.get("title");
			addContactGeneric(driver, contactType, firstName, lastName, primaryCMethod, assignTo,
					corpUser.getuserFullName(), regionName, franchiseId, title, emailId, config);

			fc.utobj().clickElement(driver, pobj.contactsLinks);
			contactsFilter(driver, contactType);
			fc.utobj().printTestStep("Add To Group");
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//a[contains(text () ,'" + firstName
					+ " " + lastName + "')]/ancestor::tr/td/input[@name='checkb']"));

			fc.utobj().selectActionMenuItemsWithTagA(driver, "Add To Group");

			fc.utobj().printTestStep("Create Group");
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().clickElement(driver, pobj.addToNewGroup);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Create New Group");
			String groupName = fc.utobj().generateTestData(dataSet.get("groupName"));
			String accessibility = "Public to all Users";
			groupPage.addToGroupRegular(driver, groupName, accessibility);

			new CRMGroupsPageTest().searchGroupsByFilter(driver, groupName, "Contact");

			String colunmName = "";
			int z = 0;

			// click over Recipients
			List<WebElement> listHead = driver
					.findElements(By.xpath(".//a[.='" + groupName + "']/ancestor::table/thead/tr/th"));
			System.out.println(listHead);
			for (int i = 0; i < listHead.size(); i++) {

				colunmName = listHead.get(i).getText().trim();
				if (colunmName.equalsIgnoreCase("Recipients")) {
					z = i;
					break;
				}
			}

			if (z != 0) {
				z = z + 1;
				fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
						".//a[.='" + groupName + "']/ancestor::tr/td[" + z + "]/a"));
			} else {
				fc.utobj().throwsException("was not able to find Recipients Column");
			}

			//accountPage.searchContactByContactType(driver, contactType);
			contactsFilter(driver, contactType);
			fc.utobj().printTestStep("Verify Contacts Add To Group");
			boolean isContactNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firstName + " " + lastName + "')]");
			if (isContactNamePresent == false) {
				fc.utobj().throwsException("was not able to add in Group");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = {"crm"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify the Archive Contact By Action Btn At CRM > Contact > Contact Summary", testCaseId = "TC_176_Archive_Contact")
	public void archiveContactsActionBtn() throws IOException, Exception, ParserConfigurationException, SAXException {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			CRMContactsPage pobj = new CRMContactsPage(driver);
			fc.utobj().printTestStep("Navigate To Admin Users Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			//String emailId = "crmautomation@staffex.com";
			String emailId = fc.utobj().generateTestData("crmautomation")+"@staffex.com";
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUser = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporateUser.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			fc.utobj().printTestStep("Navigate To Admin > CRM > Contact Type Configuration");
			fc.utobj().printTestStep("Add Contact Type");
			AdminCRMContactTypeConfigurationPageTest contatcTypePage = new AdminCRMContactTypeConfigurationPageTest();
			String contactType = fc.utobj().generateTestData(dataSet.get("contactType"));
			contatcTypePage.addContactType(driver, contactType);

			fc.utobj().printTestStep("Navigate To CRM > Contacts > Contacts Summary Page");
			fc.utobj().printTestStep("Add Contact");
			fc.crm().crm_common().CRMContactsLnk(driver);
			fc.utobj().clickElement(driver, pobj.addNew);
			fc.utobj().clickElement(driver, pobj.contactLnk);

			String firstName = fc.utobj().generateTestData(dataSet.get("firstName"));
			String lastName = fc.utobj().generateTestData(dataSet.get("lastName"));
			String primaryCMethod = dataSet.get("primaryCMethod");
			String assignTo = "Corporate";
			String regionName = "";
			String franchiseId = "";
			String title = dataSet.get("title");

			addContactGeneric(driver, contactType, firstName, lastName, primaryCMethod, assignTo,
					corpUser.getuserFullName(), regionName, franchiseId, title, emailId, config);

			fc.utobj().clickElement(driver, pobj.contactsLinks);
			contactsFilter(driver, contactType);
			fc.utobj().printTestStep("Archive Contact");
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//a[contains(text () ,'" + firstName
					+ " " + lastName + "')]/ancestor::tr/td/input[@name='checkb']"));
			fc.utobj().selectActionMenuItemsWithTagA(driver, "Archive Contacts");
			//fc.utobj().acceptAlertBox(driver);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().clickElement(driver,pobj.archive);
			fc.utobj().switchFrameToDefault(driver);
			
			contactsFilter(driver, contactType);
			fc.utobj().printTestStep("Verify Archive Contact At Contact Summary Page");
			boolean isNoRecordPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//td[.='No records found.']");
			if (isNoRecordPresent == false) {
				fc.utobj().throwsException("was not able to Archive Contacts");
			}

			fc.utobj().clickElement(driver, pobj.searchLink);
			fc.utobj().clickElement(driver, pobj.contactsTabAtSearch);
			if (!fc.utobj().isSelected(driver, pobj.archiveContacts)) {
				fc.utobj().clickElement(driver, pobj.archiveContacts);
			}
			fc.utobj().setToDefault(driver, pobj.assignTo);
			WebElement elementInput = fc.utobj().getElement(driver, pobj.assignTo)
					.findElement(By.xpath("./div/div/input"));
			fc.utobj().selectValFromMultiSelect(driver, pobj.assignTo, elementInput, corpUser.getuserFullName());
			fc.utobj().clickElement(driver, pobj.searchBtnAtSearch);

			fc.utobj().printTestStep("Verify Archive Contact At Search Page");
			boolean isNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firstName + " " + lastName + "')]");
			if (isNamePresent == false) {
				fc.utobj().throwsException("was not able to verify Archive Contacts");
			}
			boolean isOwnerPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[contains(text () ,'" + firstName + " "
					+ lastName + "')]/ancestor::tr/td[contains(text () , '" + corpUser.getuserFullName() + "')]");
			if (isOwnerPresent == false) {
				fc.utobj().throwsException("was not able to verify Owner of Archive Contacts");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = {"crm"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify the Delete Contact By Action Btn At CRM > Contact > Contact Summary", testCaseId = "TC_177_Delete_Contact")
	public void deleteContactActionBtn() throws IOException, Exception, ParserConfigurationException, SAXException {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());
		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			CRMContactsPage pobj = new CRMContactsPage(driver);

			fc.utobj().printTestStep("Navigate To Admin Users Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			//String emailId = "crmautomation@staffex.com";
			String emailId = fc.utobj().generateTestData("crmautomation")+"@staffex.com";
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUser = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporateUser.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			fc.utobj().printTestStep("Navigate To Admin > CRM > Contact Type Configuration");
			fc.utobj().printTestStep("Add Contact Type");
			AdminCRMContactTypeConfigurationPageTest contatcTypePage = new AdminCRMContactTypeConfigurationPageTest();
			String contactType = fc.utobj().generateTestData(dataSet.get("contactType"));
			contatcTypePage.addContactType(driver, contactType);

			fc.utobj().printTestStep("Navigate To CRM > Contacts > Contacts Summary Page");
			fc.utobj().printTestStep("Add Contact");
			fc.crm().crm_common().CRMContactsLnk(driver);
			fc.utobj().clickElement(driver, pobj.addNew);
			fc.utobj().clickElement(driver, pobj.contactLnk);

			String firstName = fc.utobj().generateTestData(dataSet.get("firstName"));
			String lastName = fc.utobj().generateTestData(dataSet.get("lastName"));
			String primaryCMethod = dataSet.get("primaryCMethod");
			String assignTo = "Corporate";
			String regionName = "";
			String franchiseId = "";
			String title = dataSet.get("title");

			addContactGeneric(driver, contactType, firstName, lastName, primaryCMethod, assignTo,
					corpUser.getuserFullName(), regionName, franchiseId, title, emailId, config);

			fc.utobj().clickElement(driver, pobj.contactsLinks);
			contactsFilter(driver, contactType);
			fc.utobj().printTestStep("Delete Contact");
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//a[contains(text () ,'" + firstName
					+ " " + lastName + "')]/ancestor::tr/td/input[@name='checkb']"));
			fc.utobj().selectActionMenuItemsWithTagA(driver, "Delete");
			//fc.utobj().acceptAlertBox(driver);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().clickElement(driver,pobj.deletecontact);
			fc.utobj().switchFrameToDefault(driver);
			
			contactsFilter(driver, contactType);
			fc.utobj().printTestStep("Verify Delete Contact");
			boolean isNoRecordPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//td[.='No records found.']");
			if (isNoRecordPresent == false) {
				fc.utobj().throwsException("was not able to Delete Lead");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = {"crm"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify the Log A Task By Bottom Btn At CRM > Contact > Contact Summary", testCaseId = "TC_178_Log_A_Task")
	public void logATaskBottomBtn() throws IOException, Exception, ParserConfigurationException, SAXException {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			CRMContactsPage pobj = new CRMContactsPage(driver);

			/*
			 * fc.utobj().printTestStep(testCaseId,
			 * "Navigate To Admin Users Manage Corporate User");
			 * fc.utobj().printTestStep(testCaseId, "Add Corporate User");
			 * String emailId="crmautomation@staffex.com";
			 * AdminUsersManageCorporateUsersAddCorporateUserPageTest
			 * corporateUser=new
			 * AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			 * CorporateUser corpUser = new
			 * CorporateUser();corpUser.setEmail(emailId);corpUser =
			 * fc.CommonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			 * corporateUser.
			 * createDefaultCorporateUser_Through_WebService_AfterLogin(driver,
			 * corpUser);
			 */

			fc.utobj().printTestStep("Navigate To Admin Users Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			String emailId = "crmautomation@staffex.com";
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUser = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporateUser.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			fc.utobj().printTestStep("Navigate To Admin > CRM > Contact Type Configuration");
			fc.utobj().printTestStep("Add Contact Type");
			AdminCRMContactTypeConfigurationPageTest contatcTypePage = new AdminCRMContactTypeConfigurationPageTest();
			String contactType = fc.utobj().generateTestData(dataSet.get("contactType"));
			contatcTypePage.addContactType(driver, contactType);

			fc.utobj().printTestStep("Navigate To CRM > Contacts > Contacts Summary Page");
			fc.utobj().printTestStep("Add Contact");
			fc.crm().crm_common().CRMContactsLnk(driver);
			fc.utobj().clickElement(driver, pobj.addNew);
			fc.utobj().clickElement(driver, pobj.contactLnk);

			String firstName = fc.utobj().generateTestData(dataSet.get("firstName"));
			String lastName = fc.utobj().generateTestData(dataSet.get("lastName"));
			String primaryCMethod = dataSet.get("primaryCMethod");
			String assignTo = "Corporate";
			String regionName = "";
			String franchiseId = "";
			String title = dataSet.get("title");

			addContactGeneric(driver, contactType, firstName, lastName, primaryCMethod, assignTo,
					corpUser.getuserFullName(), regionName, franchiseId, title, emailId, config);

			fc.utobj().clickElement(driver, pobj.contactsLinks);
			contactsFilter(driver, contactType);
			fc.utobj().printTestStep("Log A Task");
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//a[contains(text () ,'" + firstName
					+ " " + lastName + "')]/ancestor::tr/td/input[@name='checkb']"));
			fc.utobj().clickElement(driver, pobj.logATaskBtmBtn);

			fc.commonMethods().switch_cboxIframe_frameId(driver);
			if (!fc.utobj().isSelected(driver,pobj.radioOwner)) {
				fc.utobj().clickElement(driver, pobj.radioOwner);
			}
			String statusTask = dataSet.get("statusTask");
			fc.utobj().selectDropDown(driver, pobj.statusTask, statusTask);
			String subject = fc.utobj().generateTestData(dataSet.get("subject"));
			fc.utobj().sendKeys(driver, pobj.subject, subject);
			if (!fc.utobj().isSelected(driver, pobj.timeLessTask)) {
				fc.utobj().clickElement(driver, pobj.timeLessTask);
			}
			String priority = dataSet.get("priority");
			fc.utobj().selectDropDown(driver, pobj.priority, priority);
			fc.utobj().sendKeys(driver, pobj.comments, "Test Comment");
			fc.utobj().clickElement(driver, pobj.createBtn);
			fc.utobj().switchFrameToDefault(driver);

			contactsFilter(driver, contactType);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//a[contains(text () ,'" + firstName + " " + lastName + "')]"));
			fc.utobj().printTestStep("Verify Task");
			boolean isSubjectPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[contains(text () ,'" + subject + "')]");
			if (isSubjectPresent == false) {
				fc.utobj().throwsException("was not able to verify subject");
			}
			boolean isUserPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[contains(text () ,'" + subject
					+ "')]/ancestor::tr/td/div[.='" + corpUser.getuserFullName() + "']");
			if (isUserPresent == false) {
				fc.utobj().throwsException("was not able to verify userName at Lead Info Page");
			}
			boolean isStatusOfTaskPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[contains(text () ,'" + subject
					+ "')]/ancestor::tr/td[contains(text () ,'" + statusTask + "')]");
			if (isStatusOfTaskPresent == false) {
				fc.utobj().throwsException("was not able to verify status of task");
			}
			boolean isPriorityPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[contains(text () ,'" + subject
					+ "')]/ancestor::tr/td[contains(text () ,'" + priority + "')]");
			if (isPriorityPresent == false) {
				fc.utobj().throwsException("was not able to verify Priority of task");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = {"crm"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify the Send Email By Bottom Btn Options At CRM > Contact > Contact Summary", testCaseId = "TC_179_Send_Email")
	public void sendEmailBottomBtn() throws IOException, Exception, ParserConfigurationException, SAXException {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			CRMContactsPage pobj = new CRMContactsPage(driver);

			String parentWindowsHandle = driver.getWindowHandle();

			/*
			 * fc.utobj().printTestStep(testCaseId,
			 * "Navigate To Admin Users Manage Corporate User");
			 * fc.utobj().printTestStep(testCaseId, "Add Corporate User");
			 * String emailId="crmautomation@staffex.com";
			 * AdminUsersManageCorporateUsersAddCorporateUserPageTest
			 * corporateUser=new
			 * AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			 * CorporateUser corpUser = new
			 * CorporateUser();corpUser.setEmail(emailId);corpUser =
			 * fc.CommonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			 * corporateUser.
			 * createDefaultCorporateUser_Through_WebService_AfterLogin(driver,
			 * corpUser);
			 */

			fc.utobj().printTestStep("Navigate To Admin Users Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			String emailId = "crmautomation@staffex.com";
			//String emailId=fc.utobj().generateTestData("crmautomation")+"@staffex.com";
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUser = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporateUser.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			fc.utobj().printTestStep("Navigate To Admin > CRM > Contact Type Configuration");
			fc.utobj().printTestStep("Add Contact Type");
			AdminCRMContactTypeConfigurationPageTest contatcTypePage = new AdminCRMContactTypeConfigurationPageTest();
			String contactType = fc.utobj().generateTestData(dataSet.get("contactType"));
			contatcTypePage.addContactType(driver, contactType);

			fc.utobj().printTestStep("Navigate To CRM > Contacts > Contacts Summary Page");
			fc.utobj().printTestStep("Add Contact");
			fc.crm().crm_common().CRMContactsLnk(driver);
			fc.utobj().clickElement(driver, pobj.addNew);
			fc.utobj().clickElement(driver, pobj.contactLnk);

			String firstName = fc.utobj().generateTestData(dataSet.get("firstName"));
			String lastName = fc.utobj().generateTestData(dataSet.get("lastName"));
			String primaryCMethod = dataSet.get("primaryCMethod");
			String assignTo = "Corporate";
			String regionName = "";
			String franchiseId = "";
			String title = dataSet.get("title");

			addContactGeneric(driver, contactType, firstName, lastName, primaryCMethod, assignTo,
					corpUser.getuserFullName(), regionName, franchiseId, title, emailId, config);

			fc.utobj().clickElement(driver, pobj.contactsLinks);
			contactsFilter(driver, contactType);
			fc.utobj().printTestStep("Send Email");
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//a[contains(text () ,'" + firstName
					+ " " + lastName + "')]/ancestor::tr/td/input[@name='checkb']"));
			fc.utobj().clickElement(driver, pobj.sendEmailBtmBtn);

			if (!fc.utobj().isSelected(driver, pobj.leadOwnerId)) {
				fc.utobj().clickElement(driver, pobj.leadOwnerId);
			}

			String mailCC = dataSet.get("mailCC");
			fc.utobj().sendKeys(driver, pobj.mailCC, mailCC);

			if (!fc.utobj().isSelected(driver,pobj.showBcc)) {
				fc.utobj().clickElement(driver, pobj.showBcc);
			}
			String mailBCC = dataSet.get("mailBCC");
			fc.utobj().sendKeys(driver, pobj.mailBCC, mailBCC);

			String subjectMail = fc.utobj().generateTestData(dataSet.get("subjectMail"));
			fc.utobj().sendKeys(driver, pobj.subjectMail, subjectMail);

			String editorText = fc.utobj().generateTestData(dataSet.get("mailText"));

			fc.utobj().switchFrameById(driver, "ta_ifr");

			Actions actions = new Actions(driver);
			actions.moveToElement(fc.utobj().getElement(driver, pobj.editorTextArea));
			actions.click();
			actions.sendKeys(editorText);
			fc.utobj().logReportMsg("Entered Text", editorText);
			actions.build().perform();
			fc.utobj().switchFrameToDefault(driver);
			fc.utobj().clickElement(driver, pobj.sendEmailBtn);
			fc.utobj().switchFrameToDefault(driver);

			contactsFilter(driver, contactType);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//a[contains(text () , '" + firstName + " " + lastName + "')]"));
			fc.utobj().clickElement(driver, pobj.detailedHistoryLink);

			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().clickElement(driver, pobj.emailsTab);
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//*[contains(text () ,'" + firstName + "')]"));

			fc.utobj().printTestStep("Verify Send Email At Detailed History Frame");
			Set<String> allWindows = driver.getWindowHandles();
			for (String currentWindow : allWindows) {

				if (!currentWindow.equalsIgnoreCase(parentWindowsHandle)) {

					driver.switchTo().window(currentWindow);
					try {

						if (fc.utobj().getElementByXpath(driver, ".//*[.='Email Details']").isDisplayed()) {

							boolean isFromTextPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, "");
							if (isFromTextPresent == false) {
								fc.utobj().throwsException("was not able to From Text");
							}

							boolean isToTextPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
									".//*[contains(text () ,'To')]/ancestor::tr/td[contains(text () , '" + firstName
											+ " " + lastName + "')]");
							if (isToTextPresent == false) {
								fc.utobj().throwsException("was not able to To Text");
							}
							boolean isSubjectTextPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
									".//*[contains(text () ,'To')]/ancestor::tr/td[contains(text () , '" + subjectMail
											+ "')]");
							if (isSubjectTextPresent == false) {
								fc.utobj().throwsException("was not able to Subject");
							}
							boolean isTextPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//*[.='" + editorText + "']");
							if (isTextPresent == false) {
								fc.utobj().throwsException("was not able to verify Editor text");
							}
						}

					} catch (Exception e) {
						driver.close();
					}
				}
				driver.switchTo().window(parentWindowsHandle);
			}
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().clickElement(driver, pobj.closeBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = {"crm"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Merge Contacts By Bottom Btn Options At CRM > Contacts > Contact Summary", testCaseId = "TC_180_Merge_Contact")
	public void mergeContactsBottomBtn() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			CRMAccountsPage pobj = new CRMAccountsPage(driver);
			CRMAccountsPageTest accountPage = new CRMAccountsPageTest();

			/*
			 * fc.utobj().printTestStep(testCaseId,
			 * "Navigate To Admin Users Manage Corporate User");
			 * fc.utobj().printTestStep(testCaseId, "Add Corporate User");
			 * String emailId="crmautomation@staffex.com";
			 * AdminUsersManageCorporateUsersAddCorporateUserPageTest
			 * corporateUser=new
			 * AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			 * CorporateUser corpUser = new
			 * CorporateUser();corpUser.setEmail(emailId);corpUser =
			 * fc.CommonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			 * corporateUser.
			 * createDefaultCorporateUser_Through_WebService_AfterLogin(driver,
			 * corpUser);
			 */

			fc.utobj().printTestStep("Navigate To Admin Users Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			String emailId = "crmautomation@staffex.com";
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUser = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporateUser.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			fc.utobj().printTestStep("Navigating to Admin > CRM > ContactType Configuration");
			fc.utobj().printTestStep("Add Contact Type");
			AdminCRMContactTypeConfigurationPageTest contactTypePage = new AdminCRMContactTypeConfigurationPageTest();
			String contactType = fc.utobj().generateTestData(dataSet.get("contactType"));
			contactTypePage.addContactType(driver, contactType);

			AdminCRMConfigureIndustryPageTest industryPage = new AdminCRMConfigureIndustryPageTest();
			String industry = fc.utobj().generateTestData(dataSet.get("industry"));
			String description = fc.utobj().generateTestData(dataSet.get("description"));
			fc.utobj().printTestStep("Navigating to Admin > CRM >  Configure Industry Type");
			fc.utobj().printTestStep("Add Industry");
			industryPage.addIndustry(driver, industry, description);

			String accountName = fc.utobj().generateTestData(dataSet.get("accountName"));
			String visibility = dataSet.get("visibility");
			fc.utobj().printTestStep("Navigating to CRM >  Accounts > Accounts Summary");
			fc.utobj().printTestStep("Add Account");
			accountPage.addAcocunt(driver, industry, accountName, visibility);

			fc.utobj().printTestStep("Navigate To CRM > Contacts > Contacts Summary Page");
			fc.utobj().printTestStep("Add Contact");
			fc.utobj().clickElement(driver, pobj.contactsLinks);
			fc.utobj().clickElement(driver, pobj.addNew);
			fc.utobj().clickElement(driver, pobj.contactLnk);

			String cFname = fc.utobj().generateTestData(dataSet.get("cFname"));
			String cLname = fc.utobj().generateTestData(dataSet.get("cLname"));
			String primaryCMethod = dataSet.get("primaryCMethod");
			String assignTo = "Corporate";
			String regionName = "";
			String franchiseId = "";
			String title = dataSet.get("title");

			addContactGenericWithAccount(driver, contactType, cFname, cLname, primaryCMethod, assignTo,
					corpUser.getuserFullName(), regionName, franchiseId, title, emailId, config, accountName);

			fc.utobj().printTestStep("Add Another Contacts");
			fc.utobj().clickElement(driver, pobj.contactsLinks);
			fc.utobj().clickElement(driver, pobj.addNew);
			fc.utobj().clickElement(driver, pobj.contactLnk);

			fc.utobj().selectDropDown(driver, pobj.contactType, contactType);
			fc.utobj().selectDropDown(driver, pobj.title, title);
			String cFname1 = fc.utobj().generateTestData(dataSet.get("cFname"));
			String cLname1 = fc.utobj().generateTestData(dataSet.get("cLname"));
			fc.utobj().sendKeys(driver, pobj.contactFirstName, cFname1);
			fc.utobj().sendKeys(driver, pobj.contactLastName, cLname1);

			if (!fc.utobj().isSelected(driver, pobj.assignToCorporate)) {
				fc.utobj().clickElement(driver, pobj.assignToCorporate);
			}
			fc.utobj().selectDropDown(driver, pobj.selectCorporateUserContact, corpUser.getuserFullName());
			fc.utobj().sendKeys(driver, pobj.accountName, accountName);

			fc.utobj().sleep(2000);
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//*[@id='customizedAjaxSearch']/div"));
			fc.utobj().clickElement(driver, pobj.saveBtn);

			fc.utobj().clickElement(driver, pobj.contactsLinks);
			contactsFilter(driver, contactType);

			fc.utobj().printTestStep("Merge Contacts");
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//a[contains(text () ,'" + cFname + " " + cLname + "')]/ancestor::tr/td/input[@name='checkb']"));
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//a[contains(text () ,'" + cFname1
					+ " " + cLname1 + "')]/ancestor::tr/td/input[@name='checkb']"));
			fc.utobj().clickElement(driver, pobj.mergeContactsBtmBtn);

			if (!fc.utobj().isSelected(driver, pobj.secondNameSelect)) {
				fc.utobj().clickElement(driver, pobj.secondNameSelect);
			}

			String secondName = fc.utobj().getElementByXpath(driver, ".//*[@value='second' and @name='name']/..")
					.getText().trim();

			fc.utobj().sendKeys(driver, pobj.remarksAtMerge, "Merge Remarks");
			fc.utobj().clickElement(driver, pobj.mergeBtn);

			fc.utobj().clickElement(driver, pobj.contactsLinks);
			contactsFilter(driver, contactType);

			fc.utobj().webDriverWait(driver, ".//a[contains(text () ,'" + secondName + "')]");

			fc.utobj().printTestStep("Verify Merge Contacts");
			boolean isMergeContactNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondName + "')]");
			if (isMergeContactNamePresent == false) {
				fc.utobj().throwsException("was not able to verify contact Name");
			}

			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//a[contains(text () ,'" + secondName + "')]"));
			boolean isCNamePresentAtPrimaryInfo = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () ,'Name')]/ancestor::tr/td[.='" + title + " " + secondName + "']");
			if (isCNamePresentAtPrimaryInfo == false) {
				fc.utobj().throwsException("was not able to verify Contact Name At Primary Info Page");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "crm")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Change Owner Of Contact By Bottom Button At CRM > Contact > Contact Summary ", testCaseId = "TC_181_Change_Owner")
	public void changeOwnerBottomBtn() throws IOException, Exception, ParserConfigurationException, SAXException {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			CRMContactsPage pobj = new CRMContactsPage(driver);

			fc.utobj().printTestStep("Navigate To Admin > Users > Manage Regional User");
			fc.utobj().printTestStep("Add Regional User");
			AdminUsersManageRegionalUsersAddRegionalUserPageTest regionalUserPage = new AdminUsersManageRegionalUsersAddRegionalUserPageTest();
			String userNameReg = fc.utobj().generateTestData(dataSet.get("userNameReg"));
			String region = fc.utobj().generateTestData(dataSet.get("regionName"));
			String emailId = "crmautomation@staffex.com";
			regionalUserPage.addRegionalUser(driver, userNameReg, region, emailId);

			/*
			 * fc.utobj().printTestStep(testCaseId,
			 * "Navigate To Admin Users Manage Corporate User");
			 * fc.utobj().printTestStep(testCaseId, "Add Corporate User");
			 * AdminUsersManageCorporateUsersAddCorporateUserPageTest corpTest =
			 * new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			 * String userName =
			 * fc.utobj().generateTestData(dataSet.get("userName"));
			 * 
			 * userName = corpTest.addCorporateUser(driver, userName, config ,
			 * emailId);
			 */

			fc.utobj().printTestStep("Navigate To Admin Users Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");

			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUser = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporateUser.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			fc.utobj().printTestStep("Navigate To Admin > CRM > Contact Type Configuration");
			fc.utobj().printTestStep("Add Contact Type");
			AdminCRMContactTypeConfigurationPageTest contatcTypePage = new AdminCRMContactTypeConfigurationPageTest();
			String contactType = fc.utobj().generateTestData(dataSet.get("contactType"));
			contatcTypePage.addContactType(driver, contactType);

			fc.utobj().printTestStep("Navigate To CRM > Contacts > Contacts Summary Page");
			fc.utobj().printTestStep("Add Contact");
			fc.crm().crm_common().CRMContactsLnk(driver);
			fc.utobj().clickElement(driver, pobj.addNew);
			fc.utobj().clickElement(driver, pobj.contactLnk);

			String firstName = fc.utobj().generateTestData(dataSet.get("firstName"));
			String lastName = fc.utobj().generateTestData(dataSet.get("lastName"));
			String primaryCMethod = dataSet.get("primaryCMethod");
			String assignTo = "Corporate";
			String regionName = "";
			String franchiseId = "";
			String title = dataSet.get("title");

			addContactGeneric(driver, contactType, firstName, lastName, primaryCMethod, assignTo,
					corpUser.getuserFullName(), regionName, franchiseId, title, emailId, config);

			fc.utobj().clickElement(driver, pobj.contactsLinks);
			contactsFilter(driver, contactType);
			fc.utobj().printTestStep("Change Owner");
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//a[contains(text () ,'" + firstName
					+ " " + lastName + "')]/ancestor::tr/td/input[@name='checkb']"));
			fc.utobj().clickElement(driver, pobj.changeOwnerBtmBtn);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			if (!fc.utobj().isSelected(driver, pobj.selectRegionalUser1)) {
				fc.utobj().clickElement(driver, pobj.selectRegionalUser1);
			}
			fc.utobj().selectDropDown(driver, pobj.selectAreaRegion, region);
			fc.utobj().selectDropDown(driver, pobj.selectRegionalUser, userNameReg + " " + userNameReg);
			fc.utobj().clickElement(driver, pobj.assignBtn);
			fc.utobj().clickElement(driver, pobj.closeBtn);
			fc.utobj().switchFrameToDefault(driver);

			contactsFilter(driver, contactType);
			fc.utobj().printTestStep("Verify Change Owner");
			boolean isOwnerPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[contains(text () ,'" + firstName + " "
					+ lastName + "')]/ancestor::tr/td[contains(text () ,'" + userNameReg + " " + userNameReg + "')]");
			if (isOwnerPresent == false) {
				fc.utobj().throwsException("was not able to verify Owner");
			}
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//a[contains(text () ,'" + firstName + " " + lastName + "')]"));

			boolean isRegionNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () ,'Area / Region')]/ancestor::tr/td[.='" + region + "']");
			if (isRegionNamePresent == false) {
				fc.utobj().throwsException("was not able to verify Region name CRM > Contact > Primary Info");
			}

			boolean isRegionalUserPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () ,'Assign To')]/ancestor::tr/td[contains(text () ,'" + userNameReg + " "
							+ userNameReg + "')]");
			if (isRegionalUserPresent == false) {
				fc.utobj().throwsException("was not able to verify Regional User 'Owner' CRM > Contact > Primary Info");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "crm")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify the Change Status of Contact By Bottom Button At CRM > Contact > Contact Summary", testCaseId = "TC_182_Change_Status")
	public void changeStatusBottomBtn() throws IOException, Exception, ParserConfigurationException, SAXException {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			CRMContactsPage pobj = new CRMContactsPage(driver);

			/*
			 * fc.utobj().printTestStep(testCaseId,
			 * "Navigate To Admin Users Manage Corporate User");
			 * fc.utobj().printTestStep(testCaseId, "Add Corporate User");
			 * String emailId="crmautomation@staffex.com";
			 * AdminUsersManageCorporateUsersAddCorporateUserPageTest
			 * corporateUser=new
			 * AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			 * CorporateUser corpUser = new
			 * CorporateUser();corpUser.setEmail(emailId);corpUser =
			 * fc.CommonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			 * corporateUser.
			 * createDefaultCorporateUser_Through_WebService_AfterLogin(driver,
			 * corpUser);
			 */

			fc.utobj().printTestStep("Navigate To Admin Users Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			String emailId = "crmautomation@staffex.com";
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUser = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporateUser.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			fc.utobj().printTestStep("Navigate To Admin > CRM > Contact Type Configuration");
			fc.utobj().printTestStep("Add Contact Type");
			AdminCRMContactTypeConfigurationPageTest contatcTypePage = new AdminCRMContactTypeConfigurationPageTest();
			String contactType = fc.utobj().generateTestData(dataSet.get("contactType"));
			contatcTypePage.addContactType(driver, contactType);

			fc.utobj().printTestStep("Navigate To Admin CRM > Configure Status Page");
			fc.utobj().printTestStep("Add Contact Type Status");
			AdminCRMConfigureStatusPageTest statusPage = new AdminCRMConfigureStatusPageTest();
			String statusName = fc.utobj().generateTestData(dataSet.get("status"));
			statusPage.addStatus01(driver, statusName);

			fc.utobj().printTestStep("Navigate To CRM > Contacts > Contacts Summary Page");
			fc.utobj().printTestStep("Add Contact");
			fc.crm().crm_common().CRMContactsLnk(driver);
			fc.utobj().clickElement(driver, pobj.addNew);
			fc.utobj().clickElement(driver, pobj.contactLnk);

			String firstName = fc.utobj().generateTestData(dataSet.get("firstName"));
			String lastName = fc.utobj().generateTestData(dataSet.get("lastName"));
			String primaryCMethod = dataSet.get("primaryCMethod");
			String assignTo = "Corporate";
			String regionName = "";
			String franchiseId = "";
			String title = dataSet.get("title");

			addContactGeneric(driver, contactType, firstName, lastName, primaryCMethod, assignTo,
					corpUser.getuserFullName(), regionName, franchiseId, title, emailId, config);

			fc.utobj().clickElement(driver, pobj.contactsLinks);
			contactsFilter(driver, contactType);
			fc.utobj().printTestStep("Change Status");
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//a[contains(text () ,'" + firstName
					+ " " + lastName + "')]/ancestor::tr/td/input[@name='checkb']"));
			fc.utobj().clickElement(driver, pobj.changeStatusBtmBtn);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().selectDropDown(driver, pobj.statusChangeTo, statusName);
			fc.utobj().sendKeys(driver, pobj.remarksAtMerge, "Test Remarks");
			fc.utobj().clickElement(driver, pobj.changeBtn);
			fc.utobj().clickElement(driver, pobj.closeBtn);
			fc.utobj().switchFrameToDefault(driver);

			contactsFilter(driver, contactType);
			fc.utobj().printTestStep("Verify Change Status");
			boolean isStatusPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[contains(text () ,'" + firstName + " "
					+ lastName + "')]/ancestor::tr/td[contains(text () ,'" + statusName + "')]");
			if (isStatusPresent == false) {
				fc.utobj().throwsException("was not able to verify Status Name");
			}

			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//a[contains(text () ,'" + firstName + " " + lastName + "')]"));
			boolean isStatusPresentAtInfo = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//select[@id='cmLeadStatusID']/option[@selected and .='" + statusName + "']");
			if (isStatusPresentAtInfo == false) {
				fc.utobj().throwsException("was not able to verify Status Name at Info Page");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "crm", "TC_183_Associate_With_Email_Campaign" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Contact Associated With Emai Campaign By Bottom Btn At CRM > Contact > Contact Summary", testCaseId = "TC_183_Associate_With_Email_Campaign")
	public void associatesWithEmailCampaignBottomBtn()
			throws IOException, Exception, ParserConfigurationException, SAXException {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			CRMContactsPage pobj = new CRMContactsPage(driver);

			fc.utobj().printTestStep("Navigate To CRM >Campaign Center");
			fc.utobj().printTestStep("Add Campaign");
			CRMCampaignCenterPageTestForCRM campaignCenterPage = new CRMCampaignCenterPageTestForCRM();
			String campaignName = fc.utobj().generateTestData(dataSet.get("campaignName"));
			String campaignDescription = fc.utobj().generateTestData(dataSet.get("campaignDescription"));
			String campaignAccessibility = dataSet.get("campaignAccessibility");
			String category = dataSet.get("category");
			String templateName = fc.utobj().generateTestData(dataSet.get("templateName"));
			String mailSubject = dataSet.get("mailSubject");
			String templateText = fc.utobj().generateTestData(dataSet.get("templateText"));
			campaignCenterPage.createCampaign(driver, campaignName, campaignDescription, campaignAccessibility,
					category, templateName, mailSubject, templateText, config);

			/*
			 * fc.utobj().printTestStep(testCaseId,
			 * "Navigate To Admin Users Manage Corporate User");
			 * fc.utobj().printTestStep(testCaseId, "Add Corporate User");
			 * String emailId="crmautomation@staffex.com";
			 * AdminUsersManageCorporateUsersAddCorporateUserPageTest
			 * corporateUser=new
			 * AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			 * CorporateUser corpUser = new
			 * CorporateUser();corpUser.setEmail(emailId);corpUser =
			 * fc.CommonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			 * corporateUser.
			 * createDefaultCorporateUser_Through_WebService_AfterLogin(driver,
			 * corpUser);
			 */

			fc.utobj().printTestStep("Navigate To Admin Users Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			String emailId = "crmautomation@staffex.com";
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUser = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporateUser.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			fc.utobj().printTestStep("Navigate To Admin > CRM > Contact Type Configuration");
			fc.utobj().printTestStep("Add Contact Type");
			AdminCRMContactTypeConfigurationPageTest contatcTypePage = new AdminCRMContactTypeConfigurationPageTest();
			String contactType = fc.utobj().generateTestData(dataSet.get("contactType"));
			contatcTypePage.addContactType(driver, contactType);

			fc.utobj().printTestStep("Navigate To CRM > Contacts > Contacts Summary Page");
			fc.utobj().printTestStep("Add Contact");
			fc.crm().crm_common().CRMContactsLnk(driver);
			fc.utobj().clickElement(driver, pobj.addNew);
			fc.utobj().clickElement(driver, pobj.contactLnk);

			String firstName = fc.utobj().generateTestData(dataSet.get("firstName"));
			String lastName = fc.utobj().generateTestData(dataSet.get("lastName"));
			String primaryCMethod = dataSet.get("primaryCMethod");
			String assignTo = "Corporate";
			String regionName = "";
			String franchiseId = "";
			String title = dataSet.get("title");

			addContactGeneric(driver, contactType, firstName, lastName, primaryCMethod, assignTo,
					corpUser.getuserFullName(), regionName, franchiseId, title, emailId, config);

			fc.utobj().clickElement(driver, pobj.contactsLinks);
			contactsFilter(driver, contactType);
			fc.utobj().printTestStep("Associate With Email Campaign");
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//a[contains(text () ,'" + firstName
					+ " " + lastName + "')]/ancestor::tr/td/input[@name='checkb']"));
			fc.utobj().clickElement(driver, pobj.associateWithEmailCampaignBtmBtn);
			fc.utobj().clickElement(driver, pobj.filterContainer);
			fc.utobj().sendKeys(driver, pobj.searchCampaign, campaignName);
			fc.utobj().clickElement(driver, pobj.applyFilter);

			fc.utobj().clickElement(driver, new CRMLeadsPage(driver).sendCampaignBtn);
			fc.utobj().clickElement(driver, pobj.confirmBtn);

			fc.utobj().clickElement(driver, pobj.contactsLinks);
			contactsFilter(driver, contactType);
			fc.utobj().printTestStep("Verify Contact Associate With Campaign");
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//a[contains(text () ,'" + firstName + " " + lastName + "')]"));
			boolean isCampaignNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () ,'Promotional Campaign')]/following-sibling::td[.='" + campaignName + "']");
			if (isCampaignNamePresent == false) {
				fc.utobj().throwsException("was not able to verify Campaign Name");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "crm"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Contact Add To Group By More Action Btn At CRM > Contact > Contact Summary", testCaseId = "TC_184_Add_To_Group")
	public void addToGroupMoreAction() throws IOException, Exception, ParserConfigurationException, SAXException {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			CRMContactsPage pobj = new CRMContactsPage(driver);
			CRMGroupsPageTest groupPage = new CRMGroupsPageTest();

			CRMAccountsPageTest accountPage = new CRMAccountsPageTest();
			fc.utobj().printTestStep("Navigate To Admin Users Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			String emailId = "crmautomation@staffex.com";
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUser = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporateUser.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			fc.utobj().printTestStep("Navigate To Admin > CRM > Contact Type Configuration");
			fc.utobj().printTestStep("Add Contact Type");
			AdminCRMContactTypeConfigurationPageTest contatcTypePage = new AdminCRMContactTypeConfigurationPageTest();
			String contactType = fc.utobj().generateTestData(dataSet.get("contactType"));
			contatcTypePage.addContactType(driver, contactType);

			fc.utobj().printTestStep("Navigate To CRM > Contacts > Contacts Summary Page");
			fc.utobj().printTestStep("Add Contact");
			fc.crm().crm_common().CRMContactsLnk(driver);
			fc.utobj().clickElement(driver, pobj.addNew);
			fc.utobj().clickElement(driver, pobj.contactLnk);

			String firstName = fc.utobj().generateTestData(dataSet.get("firstName"));
			String lastName = fc.utobj().generateTestData(dataSet.get("lastName"));
			String primaryCMethod = dataSet.get("primaryCMethod");
			String assignTo = "Corporate";
			String regionName = "";
			String franchiseId = "";
			String title = dataSet.get("title");
			addContactGeneric(driver, contactType, firstName, lastName, primaryCMethod, assignTo,
					corpUser.getuserFullName(), regionName, franchiseId, title, emailId, config);

			fc.utobj().clickElement(driver, pobj.contactsLinks);
			contactsFilter(driver, contactType);
			fc.utobj().printTestStep("Add To Group");
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//a[contains(text () ,'" + firstName
					+ " " + lastName + "')]/ancestor::tr/td/input[@name='checkb']"));
			accountPage.selectActionMenuItemsWithTagAMoreActions(driver, "Add To Group");
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().clickElement(driver, pobj.addToNewGroup);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Create New Group");
			String groupName = fc.utobj().generateTestData(dataSet.get("groupName"));
			String accessibility = "Public to all Users";
			groupPage.addToGroupRegular(driver, groupName, accessibility);

			/*
			 * fc.utobj().sendKeys(driver, pobj.searchAtGroup, groupName);
			 * fc.utobj().clickElement(driver, pobj.exactSearchGroup);
			 * fc.utobj().clickElement(driver, pobj.searchAccountBtn);
			 */

			new CRMGroupsPageTest().searchGroupsByFilter(driver, groupName, "Contact");

			String colunmName = "";
			int z = 0;

			// click over Recipients
			List<WebElement> listHead = driver
					.findElements(By.xpath(".//a[.='" + groupName + "']/ancestor::table/thead/tr/th"));
			System.out.println(listHead);
			for (int i = 0; i < listHead.size(); i++) {

				colunmName = listHead.get(i).getText().trim();
				if (colunmName.equalsIgnoreCase("Recipients")) {
					z = i;
					break;
				}
			}

			if (z != 0) {
				z = z + 1;
				fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
						".//a[.='" + groupName + "']/ancestor::tr/td[" + z + "]/a"));
			} else {
				fc.utobj().throwsException("was not able to find Recipients Column");
			}

			//accountPage.searchContactByContactType(driver, contactType);
			contactsFilter(driver, contactType);
			fc.utobj().printTestStep("Verify Contact In Group");
			boolean isContactNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firstName + " " + lastName + "')]");
			if (isContactNamePresent == false) {
				fc.utobj().throwsException("was not able to add in Group");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = {"crm"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify the Archive Contact By More Action Btn At CRM > Contact > Contact Summary", testCaseId = "TC_185_Archive_Contact")
	public void acrhiveContactsMoreActions() throws IOException, Exception, ParserConfigurationException, SAXException {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			CRMContactsPage pobj = new CRMContactsPage(driver);
			CRMAccountsPageTest accountPage = new CRMAccountsPageTest();

			/*
			 * fc.utobj().printTestStep(testCaseId,
			 * "Navigate To Admin Users Manage Corporate User");
			 * fc.utobj().printTestStep(testCaseId, "Add Corporate User");
			 * 
			 * AdminUsersManageCorporateUsersAddCorporateUserPageTest corpTest =
			 * new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			 * String userName =
			 * fc.utobj().generateTestData(dataSet.get("userName")); String
			 * emailId="crmautomation@staffex.com"; userName =
			 * corpTest.addCorporateUser(driver, userName, config , emailId);
			 */

			fc.utobj().printTestStep("Navigate To Admin Users Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			//String emailId = "crmautomation@staffex.com";
			String emailId = fc.utobj().generateTestData("crmautomation")+"@staffex.com";
			
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUser = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporateUser.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			fc.utobj().printTestStep("Navigate To Admin > CRM > Contact Type Configuration");
			fc.utobj().printTestStep("Add Contact Type");
			AdminCRMContactTypeConfigurationPageTest contatcTypePage = new AdminCRMContactTypeConfigurationPageTest();
			String contactType = fc.utobj().generateTestData(dataSet.get("contactType"));
			contatcTypePage.addContactType(driver, contactType);

			fc.utobj().printTestStep("Navigate To CRM > Contacts > Contacts Summary Page");
			fc.utobj().printTestStep("Add Contact");
			fc.crm().crm_common().CRMContactsLnk(driver);
			fc.utobj().clickElement(driver, pobj.addNew);
			fc.utobj().clickElement(driver, pobj.contactLnk);

			String firstName = fc.utobj().generateTestData(dataSet.get("firstName"));
			String lastName = fc.utobj().generateTestData(dataSet.get("lastName"));
			String primaryCMethod = dataSet.get("primaryCMethod");
			String assignTo = "Corporate";
			String regionName = "";
			String franchiseId = "";
			String title = dataSet.get("title");

			addContactGeneric(driver, contactType, firstName, lastName, primaryCMethod, assignTo,
					corpUser.getuserFullName(), regionName, franchiseId, title, emailId, config);

			fc.utobj().clickElement(driver, pobj.contactsLinks);
			contactsFilter(driver, contactType);
			fc.utobj().printTestStep("Archive Contact");
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//a[contains(text () ,'" + firstName
					+ " " + lastName + "')]/ancestor::tr/td/input[@name='checkb']"));
			accountPage.selectActionMenuItemsWithTagAMoreActions(driver, "Archive Contacts");
			//fc.utobj().acceptAlertBox(driver);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().clickElement(driver,pobj.archive);
			fc.utobj().switchFrameToDefault(driver);

			contactsFilter(driver, contactType);
			fc.utobj().printTestStep("Verify Archive Contact At Contacts Page");
			boolean isNoRecordPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//td[.='No records found.']");
			if (isNoRecordPresent == false) {
				fc.utobj().throwsException("was not able to Archive Lead");
			}

			fc.utobj().clickElement(driver, pobj.searchLink);
			fc.utobj().clickElement(driver, pobj.contactsTabAtSearch);
			if (!fc.utobj().isSelected(driver, pobj.archiveContacts)) {
				fc.utobj().clickElement(driver, pobj.archiveContacts);
			}
			fc.utobj().setToDefault(driver, pobj.assignTo);
			WebElement elementInput = fc.utobj().getElement(driver, pobj.assignTo)
					.findElement(By.xpath("./div/div/input"));
			fc.utobj().selectValFromMultiSelect(driver, pobj.assignTo, elementInput, corpUser.getuserFullName());
			fc.utobj().clickElement(driver, pobj.searchBtnAtSearch);

			fc.utobj().printTestStep("Verify Archive Contact At Search Page");
			boolean isNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firstName + " " + lastName + "')]");
			if (isNamePresent == false) {
				fc.utobj().throwsException("was not able to verify Archive Contacts");
			}
			boolean isOwnerPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[contains(text () ,'" + firstName + " "
					+ lastName + "')]/ancestor::tr/td[contains(text () , '" + corpUser.getuserFullName() + "')]");
			if (isOwnerPresent == false) {
				fc.utobj().throwsException("was not able to verify Owner of Archive Contacts");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "crm")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify the Change Contact Type Of Contact By Action Btn At CRM > Contact > Contact Summary", testCaseId = "TC_186_Change_Contact")
	public void changeContactTypeMoreActions()
			throws IOException, Exception, ParserConfigurationException, SAXException {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			CRMContactsPage pobj = new CRMContactsPage(driver);
			CRMAccountsPageTest accountPage = new CRMAccountsPageTest();

			/*
			 * fc.utobj().printTestStep(testCaseId,
			 * "Navigate To Admin Users Manage Corporate User");
			 * fc.utobj().printTestStep(testCaseId, "Add Corporate User");
			 * 
			 * AdminUsersManageCorporateUsersAddCorporateUserPageTest corpTest =
			 * new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			 * String userName =
			 * fc.utobj().generateTestData(dataSet.get("userName")); String
			 * emailId="crmautomation@staffex.com"; userName =
			 * corpTest.addCorporateUser(driver, userName, config , emailId);
			 */

			fc.utobj().printTestStep("Navigate To Admin Users Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			String emailId = "crmautomation@staffex.com";
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUser = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporateUser.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			fc.utobj().printTestStep("Navigate To Admin > CRM > Contact Type Configuration");
			fc.utobj().printTestStep("Add Contact Type");
			AdminCRMContactTypeConfigurationPageTest contatcTypePage = new AdminCRMContactTypeConfigurationPageTest();
			String contactType = fc.utobj().generateTestData(dataSet.get("contactType"));
			contatcTypePage.addContactType(driver, contactType);

			fc.utobj().printTestStep("Navigate To Admin > CRM > Contact Type Configuration");
			fc.utobj().printTestStep("Add Contact Type");
			String contactType1 = fc.utobj().generateTestData(dataSet.get("contactType"));
			contatcTypePage.addContactType(driver, contactType1);

			fc.utobj().printTestStep("Navigate To CRM > Contacts > Contacts Summary Page");
			fc.utobj().printTestStep("Add Contact");

			fc.crm().crm_common().CRMContactsLnk(driver);
			fc.utobj().clickElement(driver, pobj.addNew);
			fc.utobj().clickElement(driver, pobj.contactLnk);

			String firstName = fc.utobj().generateTestData(dataSet.get("firstName"));
			String lastName = fc.utobj().generateTestData(dataSet.get("lastName"));
			String primaryCMethod = dataSet.get("primaryCMethod");
			String assignTo = "Corporate";
			String regionName = "";
			String franchiseId = "";
			String title = dataSet.get("title");

			addContactGeneric(driver, contactType, firstName, lastName, primaryCMethod, assignTo,
					corpUser.getuserFullName(), regionName, franchiseId, title, emailId, config);

			fc.utobj().clickElement(driver, pobj.contactsLinks);
			contactsFilter(driver, contactType);
			fc.utobj().printTestStep("Change Contact Type");
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//a[contains(text () ,'" + firstName
					+ " " + lastName + "')]/ancestor::tr/td/input[@name='checkb']"));
			accountPage.selectActionMenuItemsWithTagAMoreActions(driver, "Change Contact Type");
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().selectDropDown(driver, pobj.contactType, contactType1);
			fc.utobj().clickElement(driver, pobj.changeBtn);
			fc.utobj().clickElement(driver, pobj.closeBtn);
			fc.utobj().switchFrameToDefault(driver);

			contactsFilter(driver, contactType1);
			fc.utobj().printTestStep("Verify Change Contact Type");
			boolean isStatusPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[contains(text () ,'" + firstName + " "
					+ lastName + "')]/ancestor::tr/td[contains(text () ,'" + contactType1 + "')]");
			if (isStatusPresent == false) {
				fc.utobj().throwsException("was not able to verify Contact Type");
			}

			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//a[contains(text () ,'" + firstName + " " + lastName + "')]"));
			boolean isStatusPresentAtInfo = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () ,'Contact Type')]/ancestor::tr/td[.='" + contactType1 + "']");
			if (isStatusPresentAtInfo == false) {
				fc.utobj().throwsException("was not able to verify Contact Type at Info Page");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = {"crm"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify the Delete Contact By More Action Btn At CRM > Contact > Contact Summary", testCaseId = "TC_187_Delete_Contact")
	public void deleteContactMoreActions() throws IOException, Exception, ParserConfigurationException, SAXException {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			CRMContactsPage pobj = new CRMContactsPage(driver);

			CRMAccountsPageTest accountPage = new CRMAccountsPageTest();
			fc.utobj().printTestStep("Navigate To Admin Users Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			//String emailId = "crmautomation@staffex.com";
			String emailId = fc.utobj().generateTestData("crmautomation")+"@staffex.com";
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUser = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporateUser.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			fc.utobj().printTestStep("Navigate To Admin > CRM > Contact Type Configuration");
			fc.utobj().printTestStep("Add Contact Type");
			AdminCRMContactTypeConfigurationPageTest contatcTypePage = new AdminCRMContactTypeConfigurationPageTest();
			String contactType = fc.utobj().generateTestData(dataSet.get("contactType"));
			contatcTypePage.addContactType(driver, contactType);

			fc.utobj().printTestStep("Navigate To CRM > Contacts > Contacts Summary Page");
			fc.utobj().printTestStep("Add Contact");
			fc.crm().crm_common().CRMContactsLnk(driver);
			fc.utobj().clickElement(driver, pobj.addNew);
			fc.utobj().clickElement(driver, pobj.contactLnk);

			String firstName = fc.utobj().generateTestData(dataSet.get("firstName"));
			String lastName = fc.utobj().generateTestData(dataSet.get("lastName"));
			String primaryCMethod = dataSet.get("primaryCMethod");
			String assignTo = "Corporate";
			String regionName = "";
			String franchiseId = "";
			String title = dataSet.get("title");

			addContactGeneric(driver, contactType, firstName, lastName, primaryCMethod, assignTo,
					corpUser.getuserFullName(), regionName, franchiseId, title, emailId, config);

			fc.utobj().clickElement(driver, pobj.contactsLinks);
			contactsFilter(driver, contactType);
			fc.utobj().printTestStep("Delete Contact");
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//a[contains(text () ,'" + firstName
					+ " " + lastName + "')]/ancestor::tr/td/input[@name='checkb']"));
			accountPage.selectActionMenuItemsWithTagAMoreActions(driver, "Delete");
			//fc.utobj().acceptAlertBox(driver);

			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().clickElement(driver,pobj.deletecontact);
			fc.utobj().switchFrameToDefault(driver);
			
			contactsFilter(driver, contactType);
			fc.utobj().printTestStep("Verify Delete Contact");
			boolean isNoRecordPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//td[.='No records found.']");
			if (isNoRecordPresent == false) {
				fc.utobj().throwsException("was not able to Archive Lead");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "crm")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify the Modify Contact by Modify Tab At CRM > Contact > Primary Info", testCaseId = "TC_188_Modify_Contact")
	public void modifyContactTab() throws IOException, Exception, ParserConfigurationException, SAXException {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			CRMContactsPage pobj = new CRMContactsPage(driver);

			fc.utobj().printTestStep("Navigate To Admin > Users > Manage Regional User");
			fc.utobj().printTestStep("Add Regional User");
			AdminUsersManageRegionalUsersAddRegionalUserPageTest regionalUserPage = new AdminUsersManageRegionalUsersAddRegionalUserPageTest();
			String userNameReg = fc.utobj().generateTestData(dataSet.get("userNameReg"));
			String region = fc.utobj().generateTestData(dataSet.get("regionName"));
			String emailId = "crmautomation@staffex.com";
			regionalUserPage.addRegionalUser(driver, userNameReg, region, emailId);

			fc.utobj().printTestStep("Navigate To Admin Users Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");

			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUser = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporateUser.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			fc.utobj().printTestStep("Navigate To Admin > CRM > Contact Type Configuration");
			fc.utobj().printTestStep("Add Contact Type");
			AdminCRMContactTypeConfigurationPageTest contatcTypePage = new AdminCRMContactTypeConfigurationPageTest();
			String contactType = fc.utobj().generateTestData(dataSet.get("contactType"));
			contatcTypePage.addContactType(driver, contactType);

			fc.utobj().printTestStep("Navigate To CRM > Contacts > Contacts Summary Page");
			fc.utobj().printTestStep("Add Contact");
			fc.crm().crm_common().CRMContactsLnk(driver);
			fc.utobj().clickElement(driver, pobj.addNew);
			fc.utobj().clickElement(driver, pobj.contactLnk);

			String firstName = fc.utobj().generateTestData(dataSet.get("firstName"));
			String lastName = fc.utobj().generateTestData(dataSet.get("lastName"));
			String primaryCMethod = dataSet.get("primaryCMethod");
			String assignTo = "Corporate";
			String regionName = "";
			String franchiseId = "";
			String title = dataSet.get("title");

			addContactGeneric(driver, contactType, firstName, lastName, primaryCMethod, assignTo,
					corpUser.getuserFullName(), regionName, franchiseId, title, emailId, config);

			fc.utobj().clickElement(driver, pobj.contactsLinks);
			contactsFilter(driver, contactType);
			fc.utobj().printTestStep("Modify Contact");
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//a[contains(text () , '" + firstName + " " + lastName + "')]"));

			fc.utobj().clickElement(driver, pobj.modifyContactTab);

			firstName = fc.utobj().generateTestData(dataSet.get("firstName"));
			lastName = fc.utobj().generateTestData(dataSet.get("lastName"));

			fc.utobj().sendKeys(driver, pobj.contactFirstName, firstName);
			fc.utobj().sendKeys(driver, pobj.contactLastName, lastName);

			if (!fc.utobj().isSelected(driver, pobj.assignToRegional)) {
				fc.utobj().clickElement(driver, pobj.assignToRegional);
			}

			fc.utobj().selectDropDown(driver, pobj.selectAreaRegion, region);
			fc.utobj().selectDropDown(driver, pobj.selectRegionalUser, userNameReg + " " + userNameReg);
			fc.utobj().clickElement(driver, pobj.saveBtn);

			fc.utobj().printTestStep("Verify Modify Contact");
			boolean isContactTypePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () ,'Contact Type')]/ancestor::tr/td[.='" + contactType + "']");
			if (isContactTypePresent == false) {
				fc.utobj().throwsException("was not able to verify Contact Type");
			}
			boolean isContactNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () ,'Name')]/ancestor::tr/td[.='Mr. " + firstName + " " + lastName + "']");
			if (isContactNamePresent == false) {
				fc.utobj().throwsException("was not able to verify Contact Name");
			}
			boolean isPrimaryContactTypePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () ,'Primary Contact Method')]/ancestor::tr/td[.='Email']");
			if (isPrimaryContactTypePresent == false) {
				fc.utobj().throwsException("was not able to verify Primary Contact Type");
			}
			boolean isUserPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () ,'Assign To ')]/ancestor::tr/td[contains(text () , '" + userNameReg + " "
							+ userNameReg + "')]");
			if (isUserPresent == false) {
				fc.utobj().throwsException("was not able to verify Assign To");
			}

			fc.utobj().clickElement(driver, pobj.contactsLinks);
			contactsFilter(driver, contactType);

			boolean isNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firstName + " " + lastName + "')]");
			if (isNamePresent == false) {
				fc.utobj().throwsException("was not able to verify Contact Name");
			}
			boolean isContactTypePresentAtHome = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[contains(text () ,'" + firstName
					+ " " + lastName + "')]/ancestor::tr/td[contains(text () ,'" + contactType + "')]");
			if (isContactTypePresentAtHome == false) {
				fc.utobj().throwsException("was not able to verify Contact Type");
			}
			boolean isOwnerPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[contains(text () ,'" + firstName + " "
					+ lastName + "')]/ancestor::tr/td[contains(text () ,'" + userNameReg + " " + userNameReg + "')]");
			if (isOwnerPresent == false) {
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "crm")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify the Log A Task At CRM > Contact > Primary Info", testCaseId = "TC_189_Log_A_Task")
	public void logATaskTab() throws IOException, Exception, ParserConfigurationException, SAXException {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			CRMContactsPage pobj = new CRMContactsPage(driver);

			fc.utobj().printTestStep("Navigate To Admin Users Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			String emailId = "crmautomation@staffex.com";
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUser = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporateUser.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);
			fc.utobj().printTestStep("Navigate To Admin > CRM > Contact Type Configuration");
			fc.utobj().printTestStep("Add Contact Type");
			AdminCRMContactTypeConfigurationPageTest contatcTypePage = new AdminCRMContactTypeConfigurationPageTest();
			String contactType = fc.utobj().generateTestData(dataSet.get("contactType"));
			contatcTypePage.addContactType(driver, contactType);

			fc.utobj().printTestStep("Navigate To CRM > Contacts > Contacts Summary Page");
			fc.utobj().printTestStep("Add Contact");
			fc.crm().crm_common().CRMContactsLnk(driver);
			fc.utobj().clickElement(driver, pobj.addNew);
			fc.utobj().clickElement(driver, pobj.contactLnk);

			String firstName = fc.utobj().generateTestData(dataSet.get("firstName"));
			String lastName = fc.utobj().generateTestData(dataSet.get("lastName"));
			String primaryCMethod = dataSet.get("primaryCMethod");
			String assignTo = "Corporate";
			String regionName = "";
			String franchiseId = "";
			String title = dataSet.get("title");

			addContactGeneric(driver, contactType, firstName, lastName, primaryCMethod, assignTo,
					corpUser.getuserFullName(), regionName, franchiseId, title, emailId, config);

			fc.utobj().clickElement(driver, pobj.contactsLinks);
			contactsFilter(driver, contactType);
			fc.utobj().printTestStep("Log A Task");
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//a[contains(text () , '" + firstName + " " + lastName + "')]"));
			fc.utobj().clickElement(driver, pobj.logATaskTab);

			fc.commonMethods().switch_cboxIframe_frameId(driver);
			if (!fc.utobj().isSelected(driver,pobj.radioOwner)) {
				fc.utobj().clickElement(driver, pobj.radioOwner);
			}
			String statusTask = dataSet.get("statusTask");
			fc.utobj().selectDropDown(driver, pobj.statusTask, statusTask);
			String subject = fc.utobj().generateTestData(dataSet.get("subject"));
			fc.utobj().sendKeys(driver, pobj.subject, subject);
			if (!fc.utobj().isSelected(driver, pobj.timeLessTask)) {
				fc.utobj().clickElement(driver, pobj.timeLessTask);
			}
			String priority = dataSet.get("priority");
			fc.utobj().selectDropDown(driver, pobj.priority, priority);
			fc.utobj().sendKeys(driver, pobj.comments, "Test Comment");
			fc.utobj().clickElement(driver, pobj.createBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Verify Task");
			boolean isSubjectPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[contains(text () ,'" + subject + "')]");
			if (isSubjectPresent == false) {
				fc.utobj().throwsException("was not able to verify subject");
			}
			boolean isUserPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[contains(text () ,'" + subject
					+ "')]/ancestor::tr/td/div[.='" + corpUser.getuserFullName() + "']");
			if (isUserPresent == false) {
				fc.utobj().throwsException("was not able to verify userName at Lead Info Page");
			}
			boolean isStatusOfTaskPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[contains(text () ,'" + subject
					+ "')]/ancestor::tr/td[contains(text () ,'" + statusTask + "')]");
			if (isStatusOfTaskPresent == false) {
				fc.utobj().throwsException("was not able to verify status of task");
			}
			boolean isPriorityPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[contains(text () ,'" + subject
					+ "')]/ancestor::tr/td[contains(text () ,'" + priority + "')]");
			if (isPriorityPresent == false) {
				fc.utobj().throwsException("was not able to verify Priority of task");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "crm")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify the Send Email By Send Email Tab At CRM > Contact > Primary Info", testCaseId = "TC_190_Send_Email")
	public void sendEmailTab() throws IOException, Exception, ParserConfigurationException, SAXException {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			CRMContactsPage pobj = new CRMContactsPage(driver);

			String parentWindowsHandle = driver.getWindowHandle();
			fc.utobj().printTestStep("Navigate To Admin Users Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			String emailId = "crmautomation@staffex.com";
			//String emailId=fc.utobj().generateTestData("crmautomation")+"@staffex.com";
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUser = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporateUser.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);
			fc.utobj().printTestStep("Navigate To Admin > CRM > Contact Type Configuration");
			fc.utobj().printTestStep("Add Contact Type");
			AdminCRMContactTypeConfigurationPageTest contatcTypePage = new AdminCRMContactTypeConfigurationPageTest();
			String contactType = fc.utobj().generateTestData(dataSet.get("contactType"));
			contatcTypePage.addContactType(driver, contactType);

			fc.utobj().printTestStep("Navigate To CRM > Contacts > Contacts Summary Page");
			fc.utobj().printTestStep("Add Contact");
			fc.crm().crm_common().CRMContactsLnk(driver);

			fc.utobj().clickElement(driver, pobj.addNew);
			fc.utobj().clickElement(driver, pobj.contactLnk);

			String firstName = fc.utobj().generateTestData(dataSet.get("firstName"));
			String lastName = fc.utobj().generateTestData(dataSet.get("lastName"));
			String primaryCMethod = dataSet.get("primaryCMethod");
			String assignTo = "Corporate";
			String regionName = "";
			String franchiseId = "";
			String title = dataSet.get("title");

			addContactGeneric(driver, contactType, firstName, lastName, primaryCMethod, assignTo,
					corpUser.getuserFullName(), regionName, franchiseId, title, emailId, config);

			fc.utobj().clickElement(driver, pobj.contactsLinks);
			contactsFilter(driver, contactType);
			fc.utobj().printTestStep("Send Email");
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//a[contains(text () , '" + firstName + " " + lastName + "')]"));
			fc.utobj().clickElement(driver, pobj.senEmailTab);

			if (!fc.utobj().isSelected(driver, pobj.leadOwnerId)) {
				fc.utobj().clickElement(driver, pobj.leadOwnerId);
			}

			String mailCC = dataSet.get("mailCC");
			fc.utobj().sendKeys(driver, pobj.mailCC, mailCC);

			if (!fc.utobj().isSelected(driver,pobj.showBcc)) {
				fc.utobj().clickElement(driver, pobj.showBcc);
			}
			String mailBCC = dataSet.get("mailBCC");
			fc.utobj().sendKeys(driver, pobj.mailBCC, mailBCC);

			String subjectMail = fc.utobj().generateTestData(dataSet.get("subjectMail"));
			fc.utobj().sendKeys(driver, pobj.subjectMail, subjectMail);

			String editorText = fc.utobj().generateTestData(dataSet.get("mailText"));

			fc.utobj().switchFrameById(driver, "ta_ifr");

			Actions actions = new Actions(driver);
			actions.moveToElement(fc.utobj().getElement(driver, pobj.editorTextArea));
			actions.click();
			actions.sendKeys(editorText);
			fc.utobj().logReportMsg("Entered Text", editorText);
			actions.build().perform();
			fc.utobj().switchFrameToDefault(driver);
			fc.utobj().clickElement(driver, pobj.sendEmailBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Verify At Detailed History Frame");
			fc.utobj().clickElement(driver, pobj.detailedHistoryLink);

			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().clickElement(driver, pobj.emailsTab);
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//*[contains(text () ,'" + firstName + "')]"));

			Set<String> allWindows = driver.getWindowHandles();
			for (String currentWindow : allWindows) {

				if (!currentWindow.equalsIgnoreCase(parentWindowsHandle)) {

					driver.switchTo().window(currentWindow);
					try {

						if (fc.utobj().getElementByXpath(driver, ".//*[.='Email Details']").isDisplayed()) {

							boolean isFromTextPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, "");
							if (isFromTextPresent == false) {
								fc.utobj().throwsException("was not able to From Text");
							}

							boolean isToTextPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
									".//*[contains(text () ,'To')]/ancestor::tr/td[contains(text () , '" + firstName
											+ " " + lastName + "')]");
							if (isToTextPresent == false) {
								fc.utobj().throwsException("was not able to To Text");
							}
							boolean isSubjectTextPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
									".//*[contains(text () ,'To')]/ancestor::tr/td[contains(text () , '" + subjectMail
											+ "')]");
							if (isSubjectTextPresent == false) {
								fc.utobj().throwsException("was not able to Subject");
							}
							boolean isTextPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//*[.='" + editorText + "']");
							if (isTextPresent == false) {
								fc.utobj().throwsException("was not able to verify Editor text");
							}
						}

					} catch (Exception e) {
						driver.close();
					}
				}
				driver.switchTo().window(parentWindowsHandle);
			}
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().clickElement(driver, pobj.closeBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "crm")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify the Change Owner Of Contact By More Action At CRM > Contact > Primary Info", testCaseId = "TC_191_Change_Owner")
	public void changeOwnerMoreActions() throws IOException, Exception, ParserConfigurationException, SAXException {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			CRMContactsPage pobj = new CRMContactsPage(driver);

			fc.utobj().printTestStep("Navigate To Admin > Users > Manage Regional User");
			fc.utobj().printTestStep("Add Regional User");
			AdminUsersManageRegionalUsersAddRegionalUserPageTest regionalUserPage = new AdminUsersManageRegionalUsersAddRegionalUserPageTest();
			String userNameReg = fc.utobj().generateTestData(dataSet.get("userNameReg"));
			String region = fc.utobj().generateTestData(dataSet.get("regionName"));
			String emailId = "crmautomation@staffex.com";
			regionalUserPage.addRegionalUser(driver, userNameReg, region, emailId);
			fc.utobj().printTestStep("Navigate To Admin Users Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");

			AdminUsersManageCorporateUsersAddCorporateUserPageTest corpTest = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser = new CorporateUser();
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corpUser.setEmail(emailId);
			corpUser = corpTest.createDefaultUser(driver, corpUser);
			fc.utobj().printTestStep("Navigate To Admin > CRM > Contact Type Configuration");
			fc.utobj().printTestStep("Add Contact Type");
			AdminCRMContactTypeConfigurationPageTest contatcTypePage = new AdminCRMContactTypeConfigurationPageTest();
			String contactType = fc.utobj().generateTestData(dataSet.get("contactType"));
			contatcTypePage.addContactType(driver, contactType);

			fc.utobj().printTestStep("Navigate To CRM > Contacts > Contacts Summary Page");
			fc.utobj().printTestStep("Add Contact");
			fc.crm().crm_common().CRMContactsLnk(driver);
			fc.utobj().clickElement(driver, pobj.addNew);
			fc.utobj().clickElement(driver, pobj.contactLnk);

			String firstName = fc.utobj().generateTestData(dataSet.get("firstName"));
			String lastName = fc.utobj().generateTestData(dataSet.get("lastName"));
			String primaryCMethod = dataSet.get("primaryCMethod");
			String assignTo = "Corporate";
			String regionName = "";
			String franchiseId = "";
			String title = dataSet.get("title");

			addContactGeneric(driver, contactType, firstName, lastName, primaryCMethod, assignTo,
					corpUser.getuserFullName(), regionName, franchiseId, title, emailId, config);

			fc.utobj().clickElement(driver, pobj.contactsLinks);
			contactsFilter(driver, contactType);
			fc.utobj().printTestStep("Change Owner");
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//a[contains(text () , '" + firstName + " " + lastName + "')]"));
			selectActionMenuItemsWithTagAMoreActions(driver, "Change Owner");

			fc.commonMethods().switch_cboxIframe_frameId(driver);
			if (!fc.utobj().isSelected(driver, pobj.selectRegionalUser1)) {
				fc.utobj().clickElement(driver, pobj.selectRegionalUser1);
			}
			fc.utobj().selectDropDown(driver, pobj.selectAreaRegion, region);
			fc.utobj().selectDropDown(driver, pobj.selectRegionalUser, userNameReg + " " + userNameReg);
			fc.utobj().clickElement(driver, pobj.assignBtn);
			fc.utobj().clickElement(driver, pobj.closeBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().clickElement(driver, pobj.contactsLinks);
			contactsFilter(driver, contactType);
			fc.utobj().printTestStep("Verify Change Owner Of Contact");
			boolean isOwnerPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[contains(text () ,'" + firstName + " "
					+ lastName + "')]/ancestor::tr/td[contains(text () ,'" + userNameReg + " " + userNameReg + "')]");
			if (isOwnerPresent == false) {
				fc.utobj().throwsException("was not able to verify Owner");
			}
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//a[contains(text () ,'" + firstName + " " + lastName + "')]"));

			boolean isRegionNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () ,'Area / Region')]/ancestor::tr/td[.='" + region + "']");
			if (isRegionNamePresent == false) {
				fc.utobj().throwsException("was not able to verify Region name CRM > Contact > Primary Info");
			}

			boolean isRegionalUserPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () ,'Assign To')]/ancestor::tr/td[contains(text () ,'" + userNameReg + " "
							+ userNameReg + "')]");
			if (isRegionalUserPresent == false) {
				fc.utobj().throwsException("was not able to verify Regional User 'Owner' CRM > Contact > Primary Info");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "crm"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Contact Add To Group By More Action Btn At CRM > Contact > Primary Info", testCaseId = "TC_192_Add_To_Group")
	public void addToGroupMoreActions() throws IOException, Exception, ParserConfigurationException, SAXException {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			CRMContactsPage pobj = new CRMContactsPage(driver);
			CRMGroupsPageTest groupPage = new CRMGroupsPageTest();
			CRMAccountsPageTest accountPage = new CRMAccountsPageTest();
			fc.utobj().printTestStep("Navigate To Admin Users Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			String emailId = "crmautomation@staffex.com";
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUser = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporateUser.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);
			fc.utobj().printTestStep("Navigate To Admin > CRM > Contact Type Configuration");
			fc.utobj().printTestStep("Add Contact Type");
			AdminCRMContactTypeConfigurationPageTest contatcTypePage = new AdminCRMContactTypeConfigurationPageTest();
			String contactType = fc.utobj().generateTestData(dataSet.get("contactType"));
			contatcTypePage.addContactType(driver, contactType);

			fc.utobj().printTestStep("Navigate To CRM > Contacts > Contacts Summary Page");
			fc.utobj().printTestStep("Add Contact");
			fc.crm().crm_common().CRMContactsLnk(driver);
			fc.utobj().clickElement(driver, pobj.addNew);
			fc.utobj().clickElement(driver, pobj.contactLnk);

			String firstName = fc.utobj().generateTestData(dataSet.get("firstName"));
			String lastName = fc.utobj().generateTestData(dataSet.get("lastName"));
			String primaryCMethod = dataSet.get("primaryCMethod");
			String assignTo = "Corporate";
			String regionName = "";
			String franchiseId = "";
			String title = dataSet.get("title");

			addContactGeneric(driver, contactType, firstName, lastName, primaryCMethod, assignTo,
					corpUser.getuserFullName(), regionName, franchiseId, title, emailId, config);

			fc.utobj().clickElement(driver, pobj.contactsLinks);
			contactsFilter(driver, contactType);
			fc.utobj().printTestStep("Add To Group");
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//a[contains(text () , '" + firstName + " " + lastName + "')]"));
			selectActionMenuItemsWithTagAMoreActions(driver, "Add To Group");
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().clickElement(driver, pobj.addToNewGroup);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Create New Group");
			String groupName = fc.utobj().generateTestData(dataSet.get("groupName"));
			String accessibility = "Public to all Users";
			groupPage.addToGroupRegular(driver, groupName, accessibility);

			/*
			 * fc.utobj().sendKeys(driver, pobj.searchAtGroup, groupName);
			 * fc.utobj().clickElement(driver, pobj.exactSearchGroup);
			 * fc.utobj().clickElement(driver, pobj.searchAccountBtn);
			 */

			new CRMGroupsPageTest().searchGroupsByFilter(driver, groupName, "Contact");

			String colunmName = "";
			int z = 0;

			// click over Recipients
			List<WebElement> listHead = driver
					.findElements(By.xpath(".//a[.='" + groupName + "']/ancestor::table/thead/tr/th"));
			System.out.println(listHead);
			for (int i = 0; i < listHead.size(); i++) {

				colunmName = listHead.get(i).getText().trim();
				if (colunmName.equalsIgnoreCase("Recipients")) {
					z = i;
					break;
				}
			}

			if (z != 0) {
				z = z + 1;
				fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
						".//a[.='" + groupName + "']/ancestor::tr/td[" + z + "]/a"));
			} else {
				fc.utobj().throwsException("was not able to find Recipients Column");
			}

			//accountPage.searchContactByContactType(driver, contactType);
			contactsFilter(driver, contactType);
			fc.utobj().printTestStep("Verify Contact In Group");
			boolean isContactNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firstName + " " + lastName + "')]");
			if (isContactNamePresent == false) {
				fc.utobj().throwsException("was not able to add in Group");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = {"crm"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Add Another Contacts By More Action Btn Options At CRM > Contacts > Primary Info", testCaseId = "TC_193_Add_Another_Contact")
	public void addAnotherContactMoreActions() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			CRMAccountsPage pobj = new CRMAccountsPage(driver);

			CRMAccountsPageTest accountPage = new CRMAccountsPageTest();
			fc.utobj().printTestStep("Navigate To Admin Users Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			String emailId = "crmautomation@staffex.com";
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUser = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporateUser.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);
			fc.utobj().printTestStep("Navigating to Admin > CRM > ContactType Configuration");
			fc.utobj().printTestStep("Add Contact Type");
			AdminCRMContactTypeConfigurationPageTest contactTypePage = new AdminCRMContactTypeConfigurationPageTest();
			String contactType = fc.utobj().generateTestData(dataSet.get("contactType"));
			contactTypePage.addContactType(driver, contactType);

			fc.utobj().printTestStep("Navigate To Admin > CRM > Contact Type Configuration");
			fc.utobj().printTestStep("Add Contact Type");
			String contactType1 = fc.utobj().generateTestData(dataSet.get("contactType"));
			contactTypePage.addContactType(driver, contactType1);

			AdminCRMConfigureIndustryPageTest industryPage = new AdminCRMConfigureIndustryPageTest();
			String industry = fc.utobj().generateTestData(dataSet.get("industry"));
			String description = fc.utobj().generateTestData(dataSet.get("description"));
			fc.utobj().printTestStep("Navigating to Admin > CRM >  Configure Industry Type");
			fc.utobj().printTestStep("Add Industry");
			industryPage.addIndustry(driver, industry, description);

			String accountName = fc.utobj().generateTestData(dataSet.get("accountName"));
			String visibility = dataSet.get("visibility");
			fc.utobj().printTestStep("Navigating to CRM >  Accounts > Accounts Summary");
			fc.utobj().printTestStep("Add Account");
			accountPage.addAcocunt(driver, industry, accountName, visibility);

			fc.utobj().printTestStep("Navigate To CRM > Contacts > Contacts Summary Page");
			fc.utobj().printTestStep("Add Contact");
			fc.utobj().clickElement(driver, pobj.contactsLinks);
			fc.utobj().clickElement(driver, pobj.addNew);
			fc.utobj().clickElement(driver, pobj.contactLnk);

			String cFname = fc.utobj().generateTestData(dataSet.get("cFname"));
			String cLname = fc.utobj().generateTestData(dataSet.get("cLname"));
			String primaryCMethod = dataSet.get("primaryCMethod");
			String assignTo = "Corporate";
			String regionName = "";
			String franchiseId = "";
			String title = dataSet.get("title");

			addContactGenericWithAccount(driver, contactType, cFname, cLname, primaryCMethod, assignTo,
					corpUser.getuserFullName(), regionName, franchiseId, title, emailId, config, accountName);

			fc.utobj().clickElement(driver, pobj.contactsLinks);
			contactsFilter(driver, contactType);

			fc.utobj().printTestStep("Add Another Contact");
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//a[contains(text () , '" + cFname + " " + cLname + "')]"));
			selectActionMenuItemsWithTagAMoreActions(driver, "Add Another Contact");

			fc.utobj().selectDropDown(driver, pobj.contactType, contactType1);
			fc.utobj().selectDropDown(driver, pobj.title, title);
			String cFname1 = fc.utobj().generateTestData(dataSet.get("cFname"));
			String cLname1 = fc.utobj().generateTestData(dataSet.get("cLname"));
			fc.utobj().sendKeys(driver, pobj.contactFirstName, cFname1);
			fc.utobj().sendKeys(driver, pobj.contactLastName, cLname1);

			if (!fc.utobj().isSelected(driver, pobj.assignToCorporate)) {
				fc.utobj().clickElement(driver, pobj.assignToCorporate);
			}
			fc.utobj().selectDropDown(driver, pobj.selectCorporateUserContact, corpUser.getuserFullName());
			fc.utobj().sendKeys(driver, pobj.accountName, accountName);

			/*fc.utobj().sleep(2000);
			fc.utobj().clickElement(driver, pobj.accountSearchBtn);*/
			fc.utobj().sleep(2000);
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//*[@id='customizedAjaxSearch']/div"));
			fc.utobj().clickElement(driver, pobj.saveBtn);

			fc.utobj().clickElement(driver, pobj.contactsLinks);
			contactsFilter(driver, contactType1);
			fc.utobj().printTestStep("Verify Another Contact");
			boolean isContactNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + cFname1 + " " + cLname1 + "')]");
			if (isContactNamePresent == false) {
				fc.utobj().throwsException("was not able to verify contact Name");
			}

			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//a[contains(text () ,'" + cFname1 + " " + cLname1 + "')]"));
			boolean isCNamePresentAtPrimaryInfo = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () ,'Name')]/ancestor::tr/td[.='" + title + " " + cFname1 + " " + cLname1
							+ "']");
			if (isCNamePresentAtPrimaryInfo == false) {
				fc.utobj().throwsException("was not able to verify Contact Name At Primary Info Page");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "crm" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify the Contact Associated With Email Campaign By More Action Btn At CRM > Contact > Primary Info ", testCaseId = "TC_194_Associate_With_Email_Campaign")
	public void associatesWithEmailCampaignMoreActions()
			throws IOException, Exception, ParserConfigurationException, SAXException {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			CRMContactsPage pobj = new CRMContactsPage(driver);

			fc.utobj().printTestStep("Navigate To CRM >Campaign Center");
			fc.utobj().printTestStep("Add Campaign");
			CRMCampaignCenterPageTestForCRM campaignCenterPage = new CRMCampaignCenterPageTestForCRM();
			String campaignName = fc.utobj().generateTestData(dataSet.get("campaignName"));
			String campaignDescription = fc.utobj().generateTestData(dataSet.get("campaignDescription"));
			String campaignAccessibility = dataSet.get("campaignAccessibility");
			String category = dataSet.get("category");
			String templateName = fc.utobj().generateTestData(dataSet.get("templateName"));
			String mailSubject = dataSet.get("mailSubject");
			String templateText = fc.utobj().generateTestData(dataSet.get("templateText"));
			campaignCenterPage.createCampaign(driver, campaignName, campaignDescription, campaignAccessibility,
					category, templateName, mailSubject, templateText, config);

			fc.utobj().printTestStep("Navigate To Admin Users Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			String emailId = "crmautomation@staffex.com";
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUser = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporateUser.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);
			fc.utobj().printTestStep("Navigate To Admin > CRM > Contact Type Configuration");
			fc.utobj().printTestStep("Add Contact Type");
			AdminCRMContactTypeConfigurationPageTest contatcTypePage = new AdminCRMContactTypeConfigurationPageTest();
			String contactType = fc.utobj().generateTestData(dataSet.get("contactType"));
			contatcTypePage.addContactType(driver, contactType);

			fc.utobj().printTestStep("Navigate To CRM > Contacts > Contacts Summary Page");
			fc.utobj().printTestStep("Add Contact");
			fc.crm().crm_common().CRMContactsLnk(driver);
			fc.utobj().clickElement(driver, pobj.addNew);
			fc.utobj().clickElement(driver, pobj.contactLnk);

			String firstName = fc.utobj().generateTestData(dataSet.get("firstName"));
			String lastName = fc.utobj().generateTestData(dataSet.get("lastName"));
			String primaryCMethod = dataSet.get("primaryCMethod");
			String assignTo = "Corporate";
			String regionName = "";
			String franchiseId = "";
			String title = dataSet.get("title");

			addContactGeneric(driver, contactType, firstName, lastName, primaryCMethod, assignTo,
					corpUser.getuserFullName(), regionName, franchiseId, title, emailId, config);
			// fc.utobj().clickElement(driver, pobj.saveBtn);

			fc.utobj().clickElement(driver, pobj.contactsLinks);
			contactsFilter(driver, contactType);
			fc.utobj().printTestStep("Associate With Campaign");
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//a[contains(text () , '" + firstName + " " + lastName + "')]"));
			selectActionMenuItemsWithTagAMoreActions(driver, "Associate with Campaign");
			fc.utobj().clickElement(driver, pobj.filterContainer);
			fc.utobj().sendKeys(driver, pobj.searchCampaign, campaignName);
			fc.utobj().clickElement(driver, pobj.applyFilter);

			fc.utobj().clickElement(driver, new CRMLeadsPage(driver).sendCampaignBtn);
			fc.utobj().clickElement(driver, pobj.confirmBtn);

			fc.utobj().clickElement(driver, pobj.contactsLinks);
			contactsFilter(driver, contactType);
			fc.utobj().printTestStep("Verify Contact Associate With Campaign");
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//a[contains(text () ,'" + firstName + " " + lastName + "')]"));
			boolean isCampaignNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () ,'Promotional Campaign')]/following-sibling::td[.='" + campaignName + "']");
			if (isCampaignNamePresent == false) {
				fc.utobj().throwsException("was not able to verify Campign Name");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "crm")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify the Change Owner By  More Action At CRM > Contact > Primary Info", testCaseId = "TC_195_View_Detailed_History")
	public void viewDetailHistoryMoreActions()
			throws IOException, Exception, ParserConfigurationException, SAXException {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			CRMContactsPage pobj = new CRMContactsPage(driver);

			fc.utobj().printTestStep("Navigate To Admin > Users > Manage Regional User");
			fc.utobj().printTestStep("Add Regional User");
			AdminUsersManageRegionalUsersAddRegionalUserPageTest regionalUserPage = new AdminUsersManageRegionalUsersAddRegionalUserPageTest();
			String userNameReg = fc.utobj().generateTestData(dataSet.get("userNameReg"));
			String region = fc.utobj().generateTestData(dataSet.get("regionName"));
			String emailId = "crmautomation@staffex.com";
			regionalUserPage.addRegionalUser(driver, userNameReg, region, emailId);
			fc.utobj().printTestStep("Navigate To Admin Users Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");

			AdminUsersManageCorporateUsersAddCorporateUserPageTest corpTest = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser = new CorporateUser();
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corpUser.setEmail(emailId);
			corpUser = corpTest.createDefaultUser(driver, corpUser);
			fc.utobj().printTestStep("Navigate To Admin > CRM > Contact Type Configuration");
			fc.utobj().printTestStep("Add Contact Type");
			AdminCRMContactTypeConfigurationPageTest contatcTypePage = new AdminCRMContactTypeConfigurationPageTest();
			String contactType = fc.utobj().generateTestData(dataSet.get("contactType"));
			contatcTypePage.addContactType(driver, contactType);

			fc.utobj().printTestStep("Navigate To CRM > Contacts > Contacts Summary Page");
			fc.utobj().printTestStep("Add Contact");
			fc.crm().crm_common().CRMContactsLnk(driver);
			fc.utobj().clickElement(driver, pobj.addNew);
			fc.utobj().clickElement(driver, pobj.contactLnk);

			String firstName = fc.utobj().generateTestData(dataSet.get("firstName"));
			String lastName = fc.utobj().generateTestData(dataSet.get("lastName"));
			String primaryCMethod = dataSet.get("primaryCMethod");
			String assignTo = "Corporate";
			String regionName = "";
			String franchiseId = "";
			String title = dataSet.get("title");

			addContactGeneric(driver, contactType, firstName, lastName, primaryCMethod, assignTo,
					corpUser.getuserFullName(), regionName, franchiseId, title, emailId, config);

			fc.utobj().clickElement(driver, pobj.contactsLinks);
			contactsFilter(driver, contactType);
			fc.utobj().printTestStep("Chnage Owner");
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//a[contains(text () , '" + firstName + " " + lastName + "')]"));
			selectActionMenuItemsWithTagAMoreActions(driver, "Change Owner");

			fc.commonMethods().switch_cboxIframe_frameId(driver);
			if (!fc.utobj().isSelected(driver, pobj.selectRegionalUser1)) {
				fc.utobj().clickElement(driver, pobj.selectRegionalUser1);
			}
			fc.utobj().selectDropDown(driver, pobj.selectAreaRegion, region);
			fc.utobj().selectDropDown(driver, pobj.selectRegionalUser, userNameReg + " " + userNameReg);
			fc.utobj().clickElement(driver, pobj.assignBtn);
			fc.utobj().clickElement(driver, pobj.closeBtn);
			fc.utobj().switchFrameToDefault(driver);

			selectActionMenuItemsWithTagAMoreActions(driver, "View Detailed History");
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().clickElement(driver, pobj.ownerChange);

			fc.utobj().printTestStep("Verify Change Owner");
			boolean isOwnerFromPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () , 'Owner From')]/../following-sibling::tr[1]/td[contains(text () , '"
							+ corpUser.getuserFullName() + "')]");
			if (isOwnerFromPresent == false) {
				fc.utobj().throwsException("was not able to verify Owner From");
			}
			boolean isOwnerToPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () , 'Owner To')]/../following-sibling::tr[1]/td[contains(text () , '"
							+ userNameReg + " " + userNameReg + "')]");
			if (isOwnerToPresent == false) {
				fc.utobj().throwsException("was not able to verify Owner To");
			}
			fc.utobj().clickElement(driver, pobj.closeBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "crm")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify the Archive Contact By More Action Btn At CRM > Contact > Primary Info", testCaseId = "TC_196_Archive_Contact")
	public void archiveContactsMoreActions() throws IOException, Exception, ParserConfigurationException, SAXException {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			CRMContactsPage pobj = new CRMContactsPage(driver);

			fc.utobj().printTestStep("Navigate To Admin Users Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corpTest = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			String emailId = "crmautomation@staffex.com";

			CorporateUser corpUser = new CorporateUser();
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corpUser.setEmail(emailId);
			corpUser = corpTest.createDefaultUser(driver, corpUser);
			fc.utobj().printTestStep("Navigate To Admin > CRM > Contact Type Configuration");
			fc.utobj().printTestStep("Add Contact Type");
			AdminCRMContactTypeConfigurationPageTest contatcTypePage = new AdminCRMContactTypeConfigurationPageTest();
			String contactType = fc.utobj().generateTestData(dataSet.get("contactType"));
			contatcTypePage.addContactType(driver, contactType);

			fc.utobj().printTestStep("Navigate To CRM > Contacts > Contacts Summary Page");
			fc.utobj().printTestStep("Add Contact");
			fc.crm().crm_common().CRMContactsLnk(driver);
			fc.utobj().clickElement(driver, pobj.addNew);
			fc.utobj().clickElement(driver, pobj.contactLnk);

			String firstName = fc.utobj().generateTestData(dataSet.get("firstName"));
			String lastName = fc.utobj().generateTestData(dataSet.get("lastName"));
			String primaryCMethod = dataSet.get("primaryCMethod");
			String assignTo = "Corporate";
			String regionName = "";
			String franchiseId = "";
			String title = dataSet.get("title");

			addContactGeneric(driver, contactType, firstName, lastName, primaryCMethod, assignTo,
					corpUser.getuserFullName(), regionName, franchiseId, title, emailId, config);

			fc.utobj().clickElement(driver, pobj.contactsLinks);
			contactsFilter(driver, contactType);
			fc.utobj().printTestStep("Archive Contact");
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//a[contains(text () , '" + firstName + " " + lastName + "')]"));
			selectActionMenuItemsWithTagAMoreActions(driver, "Archive Contact");
			fc.utobj().acceptAlertBox(driver);

			contactsFilter(driver, contactType);
			fc.utobj().printTestStep("Verify Archive Contact At Conact Summary Page");
			boolean isNoRecordPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//td[.='No records found.']");
			if (isNoRecordPresent == false) {
				fc.utobj().throwsException("was not able to Archive Contact");
			}

			fc.utobj().clickElement(driver, pobj.searchLink);
			fc.utobj().clickElement(driver, pobj.contactsTabAtSearch);
			if (!fc.utobj().isSelected(driver, pobj.archiveContacts)) {
				fc.utobj().clickElement(driver, pobj.archiveContacts);
			}
			fc.utobj().setToDefault(driver, pobj.assignTo);
			WebElement elementInput = fc.utobj().getElement(driver, pobj.assignTo)
					.findElement(By.xpath("./div/div/input"));
			fc.utobj().selectValFromMultiSelect(driver, pobj.assignTo, elementInput, corpUser.getuserFullName());
			fc.utobj().clickElement(driver, pobj.searchBtnAtSearch);
			fc.utobj().printTestStep("Verify Archive Contact At Search Page");
			boolean isNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firstName + " " + lastName + "')]");
			if (isNamePresent == false) {
				fc.utobj().throwsException("was not able to verify Archive Contacts");
			}
			boolean isOwnerPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[contains(text () ,'" + firstName + " "
					+ lastName + "')]/ancestor::tr/td[contains(text () , '" + corpUser.getuserFullName() + "')]");
			if (isOwnerPresent == false) {
				fc.utobj().throwsException("was not able to verify Owner of Archive Contacts");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "crm")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify the Delete Contact By More Action Btn At CRM > Contact > Primary Info", testCaseId = "TC_197_Delete_Contact")
	public void deleteContactMoreActions1() throws IOException, Exception, ParserConfigurationException, SAXException {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			CRMContactsPage pobj = new CRMContactsPage(driver);

			fc.utobj().printTestStep("Navigate To Admin Users Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			String emailId = "crmautomation@staffex.com";
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUser = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporateUser.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);
			fc.utobj().printTestStep("Navigate To Admin > CRM > Contact Type Configuration");
			fc.utobj().printTestStep("Add Contact Type");
			AdminCRMContactTypeConfigurationPageTest contatcTypePage = new AdminCRMContactTypeConfigurationPageTest();
			String contactType = fc.utobj().generateTestData(dataSet.get("contactType"));
			contatcTypePage.addContactType(driver, contactType);

			fc.utobj().printTestStep("Navigate To CRM > Contacts > Contacts Summary Page");
			fc.utobj().printTestStep("Add Contact");
			fc.crm().crm_common().CRMContactsLnk(driver);
			fc.utobj().clickElement(driver, pobj.addNew);
			fc.utobj().clickElement(driver, pobj.contactLnk);

			String firstName = fc.utobj().generateTestData(dataSet.get("firstName"));
			String lastName = fc.utobj().generateTestData(dataSet.get("lastName"));
			String primaryCMethod = dataSet.get("primaryCMethod");
			String assignTo = "Corporate";
			String regionName = "";
			String franchiseId = "";
			String title = dataSet.get("title");

			addContactGeneric(driver, contactType, firstName, lastName, primaryCMethod, assignTo,
					corpUser.getuserFullName(), regionName, franchiseId, title, emailId, config);

			fc.utobj().clickElement(driver, pobj.contactsLinks);
			contactsFilter(driver, contactType);
			fc.utobj().printTestStep("Delete Contact");
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//a[contains(text () , '" + firstName + " " + lastName + "')]"));
			selectActionMenuItemsWithTagAMoreActions(driver, "Delete Contact");
			fc.utobj().acceptAlertBox(driver);

			contactsFilter(driver, contactType);
			fc.utobj().printTestStep("Verify Delete Contact");
			boolean isNoRecordPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//td[.='No records found.']");
			if (isNoRecordPresent == false) {
				fc.utobj().throwsException("was not able to Archive Lead");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = {"crm"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify the Log A Call At CRM > Contact > Contact Summary", testCaseId = "TC_198_Log_A_Call")
	public void logACallTab() throws IOException, Exception, ParserConfigurationException, SAXException {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			CRMContactsPage pobj = new CRMContactsPage(driver);

			fc.utobj().printTestStep("Navigate To Admin Users Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			String emailId = "crmautomation@staffex.com";
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUser = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporateUser.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);
			fc.utobj().printTestStep("Navigate To Admin > CRM > Contact Type Configuration");
			fc.utobj().printTestStep("Add Contact Type");
			AdminCRMContactTypeConfigurationPageTest contatcTypePage = new AdminCRMContactTypeConfigurationPageTest();
			String contactType = fc.utobj().generateTestData(dataSet.get("contactType"));
			contatcTypePage.addContactType(driver, contactType);

			fc.utobj().printTestStep("Navigate To CRM > Contacts > Contacts Summary Page");
			fc.utobj().printTestStep("Add Contact");
			fc.crm().crm_common().CRMContactsLnk(driver);
			fc.utobj().clickElement(driver, pobj.addNew);
			fc.utobj().clickElement(driver, pobj.contactLnk);

			String firstName = fc.utobj().generateTestData(dataSet.get("firstName"));
			String lastName = fc.utobj().generateTestData(dataSet.get("lastName"));
			String primaryCMethod = dataSet.get("primaryCMethod");
			String assignTo = "Corporate";
			String regionName = "";
			String franchiseId = "";
			String title = dataSet.get("title");

			addContactGeneric(driver, contactType, firstName, lastName, primaryCMethod, assignTo,
					corpUser.getuserFullName(), regionName, franchiseId, title, emailId, config);

			fc.utobj().clickElement(driver, pobj.contactsLinks);
			contactsFilter(driver, contactType);
			fc.utobj().printTestStep("Log A Call");
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//a[contains(text () , '" + firstName + " " + lastName + "')]"));
			fc.utobj().clickElement(driver, pobj.logACallTab);

			String callSubject = fc.utobj().generateTestData(dataSet.get("subject"));
			fc.utobj().sendKeys(driver, pobj.callSubject, callSubject);
			/*String callDate = fc.utobj().getCurrentDateUSFormat();
			fc.utobj().sendKeys(driver, pobj.callDate, callDate);*/
			String callDate = fc.utobj().getCurrentDateAndTime();
			fc.utobj().sendKeys(driver, pobj.callDate, callDate);
			
			String callStatus = dataSet.get("callStatus");
			fc.utobj().selectDropDown(driver, pobj.callStatus, callStatus);
			String cType = dataSet.get("callType");
			fc.utobj().selectDropDown(driver, pobj.communicationType, cType);
			fc.utobj().clickElement(driver, pobj.saveBtn);

			fc.utobj().clickElement(driver, pobj.detailedHistoryLink);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().clickElement(driver, pobj.callsTabDH);

			fc.utobj().printTestStep("Verify Call At Detailed History Frame");
			boolean isCallSubjectPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () ,'" + callSubject + "')]");
			if (isCallSubjectPresent == false) {
				fc.utobj().throwsException("was not able to verify Call Subject");
			}
			boolean isCommunicationTypePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () ,'" + cType + "')]");
			if (isCommunicationTypePresent == false) {
				fc.utobj().throwsException("was not able to verify communication type");
			}
			fc.utobj().clickElement(driver, pobj.closeBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "crm")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify the Add Remarks By Add Remarks Tab At CRM > Contact > Primary Info", testCaseId = "TC_199_Add_Remarks")
	public void addRemarksTab() throws IOException, Exception, ParserConfigurationException, SAXException {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			CRMContactsPage pobj = new CRMContactsPage(driver);

			fc.utobj().printTestStep("Navigate To Admin Users Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			String emailId = "crmautomation@staffex.com";
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUser = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporateUser.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);
			fc.utobj().printTestStep("Navigate To Admin > CRM > Contact Type Configuration");
			fc.utobj().printTestStep("Add Contact Type");
			AdminCRMContactTypeConfigurationPageTest contatcTypePage = new AdminCRMContactTypeConfigurationPageTest();
			String contactType = fc.utobj().generateTestData(dataSet.get("contactType"));
			contatcTypePage.addContactType(driver, contactType);

			fc.utobj().printTestStep("Navigate To CRM > Contacts > Contacts Summary Page");
			fc.utobj().printTestStep("Add Contact");
			fc.crm().crm_common().CRMContactsLnk(driver);
			fc.utobj().clickElement(driver, pobj.addNew);
			fc.utobj().clickElement(driver, pobj.contactLnk);

			String firstName = fc.utobj().generateTestData(dataSet.get("firstName"));
			String lastName = fc.utobj().generateTestData(dataSet.get("lastName"));
			String primaryCMethod = dataSet.get("primaryCMethod");
			String assignTo = "Corporate";
			String regionName = "";
			String franchiseId = "";
			String title = dataSet.get("title");

			addContactGeneric(driver, contactType, firstName, lastName, primaryCMethod, assignTo,
					corpUser.getuserFullName(), regionName, franchiseId, title, emailId, config);

			fc.utobj().clickElement(driver, pobj.contactsLinks);
			contactsFilter(driver, contactType);
			fc.utobj().printTestStep("Add Remarks");
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//a[contains(text () , '" + firstName + " " + lastName + "')]"));
			fc.utobj().clickElement(driver, pobj.addRemraksTab);

			String remarks = fc.utobj().generateTestData(dataSet.get("remarkField"));
			fc.utobj().sendKeys(driver, pobj.remarkField, remarks);
			fc.utobj().clickElement(driver, pobj.saveBtn);
			fc.utobj().printTestStep("Verify Remarks");
			boolean isRemarksPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[@title='Remark']/span[.='" + remarks + "']");
			if (isRemarksPresent == false) {
				fc.utobj().throwsException("was not able to verify remarks");
			}

			fc.utobj().clickElement(driver, pobj.detailedHistoryLink);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().clickElement(driver, pobj.remarksTabAtCbox);
			boolean isRemarksAtCboxPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () ,'" + remarks + "')]");
			if (isRemarksAtCboxPresent == false) {
				fc.utobj().throwsException("was not able to verify Remarks At Detailed History Cbox");
			}
			fc.utobj().clickElement(driver, pobj.closeBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "crm")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify the Change Status Of Contact At CRM > Contact > Primary Info", testCaseId = "TC_200_Change_Status")
	public void changeStatusTab() throws IOException, Exception, ParserConfigurationException, SAXException {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			CRMContactsPage pobj = new CRMContactsPage(driver);

			fc.utobj().printTestStep("Navigate To Admin Users Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			String emailId = "crmautomation@staffex.com";
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUser = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporateUser.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);
			fc.utobj().printTestStep("Navigate To Admin > CRM > Contact Type Configuration");
			fc.utobj().printTestStep("Add Contact Type");
			AdminCRMContactTypeConfigurationPageTest contatcTypePage = new AdminCRMContactTypeConfigurationPageTest();
			String contactType = fc.utobj().generateTestData(dataSet.get("contactType"));
			contatcTypePage.addContactType(driver, contactType);

			fc.utobj().printTestStep("Navigate To Admin > CRM > Configure Status");
			fc.utobj().printTestStep("Add Status For Contact");
			AdminCRMConfigureStatusPageTest statusPage = new AdminCRMConfigureStatusPageTest();
			String statusName = fc.utobj().generateTestData(dataSet.get("status"));
			statusPage.addStatus01(driver, statusName);

			fc.utobj().printTestStep("Navigate To CRM > Contacts > Contacts Summary Page");
			fc.utobj().printTestStep("Add Contact");
			fc.crm().crm_common().CRMContactsLnk(driver);
			fc.utobj().clickElement(driver, pobj.addNew);
			fc.utobj().clickElement(driver, pobj.contactLnk);

			String firstName = fc.utobj().generateTestData(dataSet.get("firstName"));
			String lastName = fc.utobj().generateTestData(dataSet.get("lastName"));
			String primaryCMethod = dataSet.get("primaryCMethod");
			String assignTo = "Corporate";
			String regionName = "";
			String franchiseId = "";
			String title = dataSet.get("title");

			addContactGeneric(driver, contactType, firstName, lastName, primaryCMethod, assignTo,
					corpUser.getuserFullName(), regionName, franchiseId, title, emailId, config);

			fc.utobj().clickElement(driver, pobj.contactsLinks);
			contactsFilter(driver, contactType);
			fc.utobj().printTestStep("Change Status");
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//a[contains(text () , '" + firstName + " " + lastName + "')]"));
			fc.utobj().selectDropDown(driver, pobj.contactStatus, statusName);
			fc.utobj().clickElement(driver, pobj.changeStatus);

			fc.utobj().printTestStep("Verify Status");
			String statusText = fc.utobj().getText(driver,
					fc.utobj().getElementByXpath(driver, ".//*[@id='cmLeadStatusID']/option[@selected='']"));
			if (!statusText.equalsIgnoreCase(statusName)) {
				fc.utobj().throwsException("was not able to verify status Name");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "crm")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Contact Assign To Regional User By Link At CRM > Contacts > Primary Info", testCaseId = "TC_201_Assign_To")
	public void assignToLink() throws IOException, Exception, ParserConfigurationException, SAXException {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			CRMContactsPage pobj = new CRMContactsPage(driver);

			fc.utobj().printTestStep("Navigate To Admin > Users > Manage Regional User");
			fc.utobj().printTestStep("Add Regional User");
			AdminUsersManageRegionalUsersAddRegionalUserPageTest regionalUserPage = new AdminUsersManageRegionalUsersAddRegionalUserPageTest();
			String userNameReg = fc.utobj().generateTestData(dataSet.get("userNameReg"));
			String region = fc.utobj().generateTestData(dataSet.get("regionName"));
			String emailId = "crmautomation@staffex.com";
			regionalUserPage.addRegionalUser(driver, userNameReg, region, emailId);

			fc.utobj().printTestStep("Navigate To Admin Users Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corpTest = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();

			CorporateUser corpUser = new CorporateUser();
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corpUser.setEmail(emailId);
			corpUser = corpTest.createDefaultUser(driver, corpUser);
			fc.utobj().printTestStep("Navigate To Admin > CRM > Contact Type Configuration");
			fc.utobj().printTestStep("Add Contact Type");
			AdminCRMContactTypeConfigurationPageTest contatcTypePage = new AdminCRMContactTypeConfigurationPageTest();
			String contactType = fc.utobj().generateTestData(dataSet.get("contactType"));
			contatcTypePage.addContactType(driver, contactType);

			fc.utobj().printTestStep("Navigate To CRM > Contacts > Contacts Summary Page");
			fc.utobj().printTestStep("Add Contact");
			fc.crm().crm_common().CRMContactsLnk(driver);
			fc.utobj().clickElement(driver, pobj.addNew);
			fc.utobj().clickElement(driver, pobj.contactLnk);

			String firstName = fc.utobj().generateTestData(dataSet.get("firstName"));
			String lastName = fc.utobj().generateTestData(dataSet.get("lastName"));
			String primaryCMethod = dataSet.get("primaryCMethod");
			String assignTo = "Corporate";
			String regionName = "";
			String franchiseId = "";
			String title = dataSet.get("title");

			addContactGeneric(driver, contactType, firstName, lastName, primaryCMethod, assignTo,
					corpUser.getuserFullName(), regionName, franchiseId, title, emailId, config);

			fc.utobj().clickElement(driver, pobj.contactsLinks);
			contactsFilter(driver, contactType);
			fc.utobj().printTestStep("Assign To Regional User");
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//a[contains(text () , '" + firstName + " " + lastName + "')]"));
			fc.utobj().clickElement(driver, pobj.assignLink);

			fc.commonMethods().switch_cboxIframe_frameId(driver);
			if (!fc.utobj().isSelected(driver, pobj.selectRegionalUser1)) {
				fc.utobj().clickElement(driver, pobj.selectRegionalUser1);
			}
			fc.utobj().selectDropDown(driver, pobj.selectAreaRegion, region);
			fc.utobj().selectDropDown(driver, pobj.selectRegionalUser, userNameReg + " " + userNameReg);
			fc.utobj().clickElement(driver, pobj.assignBtn);
			fc.utobj().clickElement(driver, pobj.closeBtn);
			fc.utobj().switchFrameToDefault(driver);

			selectActionMenuItemsWithTagAMoreActions(driver, "View Detailed History");
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().clickElement(driver, pobj.ownerChange);

			fc.utobj().printTestStep("Verify Contact Assign To Regional User");
			boolean isOwnerFromPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () , 'Owner From')]/../following-sibling::tr[1]/td[contains(text () , '"
							+ corpUser.getuserFullName() + "')]");
			if (isOwnerFromPresent == false) {
				fc.utobj().throwsException("was not able to verify Owner From");
			}
			boolean isOwnerToPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () , 'Owner To')]/../following-sibling::tr[1]/td[contains(text () , '"
							+ userNameReg + " " + userNameReg + "')]");
			if (isOwnerToPresent == false) {
				fc.utobj().throwsException("was not able to verify Owner To");
			}
			fc.utobj().clickElement(driver, pobj.closeBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "crm" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Contact Associated With Campaign By MOre Action Btn At CRM > Contact > Primary Info", testCaseId = "TC_202_Associate_With_Email_Campaign")
	public void associatesWithEmailCampaignLink()
			throws IOException, Exception, ParserConfigurationException, SAXException {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			CRMContactsPage pobj = new CRMContactsPage(driver);

			fc.utobj().printTestStep("Navigate To CRM >Campaign Center");
			fc.utobj().printTestStep("Add Campaign");
			CRMCampaignCenterPageTestForCRM campaignCenterPage = new CRMCampaignCenterPageTestForCRM();
			String campaignName = fc.utobj().generateTestData(dataSet.get("campaignName"));
			String campaignDescription = fc.utobj().generateTestData(dataSet.get("campaignDescription"));
			String campaignAccessibility = dataSet.get("campaignAccessibility");
			String category = dataSet.get("category");
			String templateName = fc.utobj().generateTestData(dataSet.get("templateName"));
			String mailSubject = dataSet.get("mailSubject");
			String templateText = fc.utobj().generateTestData(dataSet.get("templateText"));
			campaignCenterPage.createCampaign(driver, campaignName, campaignDescription, campaignAccessibility,
					category, templateName, mailSubject, templateText, config);
			fc.utobj().printTestStep("Navigate To Admin Users Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			String emailId = "crmautomation@staffex.com";
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUser = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporateUser.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);
			fc.utobj().printTestStep("Navigate To Admin > CRM > Contact Type Configuration");
			fc.utobj().printTestStep("Add Contact Type");
			AdminCRMContactTypeConfigurationPageTest contatcTypePage = new AdminCRMContactTypeConfigurationPageTest();
			String contactType = fc.utobj().generateTestData(dataSet.get("contactType"));
			contatcTypePage.addContactType(driver, contactType);

			fc.utobj().printTestStep("Navigate To CRM > Contacts > Contacts Summary Page");
			fc.utobj().printTestStep("Add Contact");
			fc.crm().crm_common().CRMContactsLnk(driver);
			fc.utobj().clickElement(driver, pobj.addNew);
			fc.utobj().clickElement(driver, pobj.contactLnk);

			String firstName = fc.utobj().generateTestData(dataSet.get("firstName"));
			String lastName = fc.utobj().generateTestData(dataSet.get("lastName"));
			String primaryCMethod = dataSet.get("primaryCMethod");
			String assignTo = "Corporate";
			String regionName = "";
			String franchiseId = "";
			String title = dataSet.get("title");

			addContactGeneric(driver, contactType, firstName, lastName, primaryCMethod, assignTo,
					corpUser.getuserFullName(), regionName, franchiseId, title, emailId, config);

			fc.utobj().clickElement(driver, pobj.contactsLinks);
			contactsFilter(driver, contactType);
			fc.utobj().printTestStep("Associate With Campaign");
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//a[contains(text () , '" + firstName + " " + lastName + "')]"));

			/*
			 * fc.utobj().clickElement(driver, pobj.associateWithCampignLink);
			 */
			fc.utobj().selectActionMenuItemsWithTagACRM(driver, "Associate with Campaign");

			fc.utobj().clickElement(driver, pobj.filterContainer);
			fc.utobj().sendKeys(driver, pobj.searchCampaign, campaignName);
			fc.utobj().clickElement(driver, pobj.applyFilter);

			fc.utobj().clickElement(driver, new CRMLeadsPage(driver).sendCampaignBtn);
			fc.utobj().clickElement(driver, pobj.confirmBtn);

			fc.utobj().clickElement(driver, pobj.contactsLinks);
			contactsFilter(driver, contactType);
			fc.utobj().printTestStep("Verify Contact Associate With Campaign");
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//a[contains(text () ,'" + firstName + " " + lastName + "')]"));
			boolean isCampaignNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () ,'Promotional Campaign')]/following-sibling::td[.='" + campaignName + "']");
			if (isCampaignNamePresent == false) {
				fc.utobj().throwsException("was not able to verify Campign Name");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = {"crm"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify the Log A Task By Link At CRM > Contact > Primary Info", testCaseId = "TC_203_Log_A_Task")
	public void logATaskLink() throws IOException, Exception, ParserConfigurationException, SAXException {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			CRMContactsPage pobj = new CRMContactsPage(driver);

			fc.utobj().printTestStep("Navigate To Admin Users Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			String emailId = "crmautomation@staffex.com";
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUser = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporateUser.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);
			fc.utobj().printTestStep("Navigate To Admin > CRM > Contact Type Configuration");
			fc.utobj().printTestStep("Add Contact Type");
			AdminCRMContactTypeConfigurationPageTest contatcTypePage = new AdminCRMContactTypeConfigurationPageTest();
			String contactType = fc.utobj().generateTestData(dataSet.get("contactType"));
			contatcTypePage.addContactType(driver, contactType);

			fc.utobj().printTestStep("Navigate To CRM > Contacts > Contacts Summary Page");
			fc.utobj().printTestStep("Add Contact");
			fc.crm().crm_common().CRMContactsLnk(driver);
			fc.utobj().clickElement(driver, pobj.addNew);
			fc.utobj().clickElement(driver, pobj.contactLnk);

			String firstName = fc.utobj().generateTestData(dataSet.get("firstName"));
			String lastName = fc.utobj().generateTestData(dataSet.get("lastName"));
			String primaryCMethod = dataSet.get("primaryCMethod");
			String assignTo = "Corporate";
			String regionName = "";
			String franchiseId = "";
			String title = dataSet.get("title");

			addContactGeneric(driver, contactType, firstName, lastName, primaryCMethod, assignTo,
					corpUser.getuserFullName(), regionName, franchiseId, title, emailId, config);

			fc.utobj().clickElement(driver, pobj.contactsLinks);
			contactsFilter(driver, contactType);
			fc.utobj().printTestStep("Log A Task");
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//a[contains(text () , '" + firstName + " " + lastName + "')]"));

			fc.utobj().clickElement(driver, pobj.logATaskLink);

			fc.commonMethods().switch_cboxIframe_frameId(driver);
			if (!fc.utobj().isSelected(driver,pobj.radioOwner)) {
				fc.utobj().clickElement(driver, pobj.radioOwner);
			}
			String statusTask = dataSet.get("statusTask");
			fc.utobj().selectDropDown(driver, pobj.statusTask, statusTask);
			String subject = fc.utobj().generateTestData(dataSet.get("subject"));
			fc.utobj().sendKeys(driver, pobj.subject, subject);
			if (!fc.utobj().isSelected(driver, pobj.timeLessTask)) {
				fc.utobj().clickElement(driver, pobj.timeLessTask);
			}
			String priority = dataSet.get("priority");
			fc.utobj().selectDropDown(driver, pobj.priority, priority);
			fc.utobj().clickElement(driver, pobj.createBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Verify A Task");
			boolean isSubjectPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[contains(text () ,'" + subject + "')]");
			if (isSubjectPresent == false) {
				fc.utobj().throwsException("was not able to verify subject");
			}
			boolean isUserPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[contains(text () ,'" + subject
					+ "')]/ancestor::tr/td/div[.='" + corpUser.getuserFullName() + "']");
			if (isUserPresent == false) {
				fc.utobj().throwsException("was not able to verify userName at Lead Info Page");
			}
			boolean isStatusOfTaskPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[contains(text () ,'" + subject
					+ "')]/ancestor::tr/td[contains(text () ,'" + statusTask + "')]");
			if (isStatusOfTaskPresent == false) {
				fc.utobj().throwsException("was not able to verify status of task");
			}
			boolean isPriorityPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[contains(text () ,'" + subject
					+ "')]/ancestor::tr/td[contains(text () ,'" + priority + "')]");
			if (isPriorityPresent == false) {
				fc.utobj().throwsException("was not able to verify Priority of task");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "crm")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Modify A Task By Link At CRM > Contact > Primary Info", testCaseId = "TC_204_Modify_Task")
	public void modifyTaskActionImage() throws IOException, Exception, ParserConfigurationException, SAXException {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			CRMContactsPage pobj = new CRMContactsPage(driver);

			fc.utobj().printTestStep("Navigate To Admin Users Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			String emailId = "crmautomation@staffex.com";
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUser = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporateUser.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);
			fc.utobj().printTestStep("Navigate To Admin > CRM > Contact Type Configuration");
			fc.utobj().printTestStep("Add Contact Type");
			AdminCRMContactTypeConfigurationPageTest contatcTypePage = new AdminCRMContactTypeConfigurationPageTest();
			String contactType = fc.utobj().generateTestData(dataSet.get("contactType"));
			contatcTypePage.addContactType(driver, contactType);

			/*
			 * fc.utobj().printTestStep(testCaseId,
			 * "Navigating to Admin > CRM > Configure Lead Type");
			 * fc.utobj().printTestStep(testCaseId, "Add Lead Type");
			 * AdminCRMConfigureLeadTypePageTest leadTypePage = new
			 * AdminCRMConfigureLeadTypePageTest(); String leadType =
			 * fc.utobj().generateTestData(dataSet.get("leadType"));
			 * leadTypePage.addLeadType(driver, leadType, config);
			 * 
			 * AdminCRMSourceSummaryPageTest addSourcePage = new
			 * AdminCRMSourceSummaryPageTest(); String source =
			 * fc.utobj().generateTestData(dataSet.get("source")); String
			 * description =
			 * fc.utobj().generateTestData(dataSet.get("description")); String
			 * displayOnWebPageText =
			 * fc.utobj().generateTestData(dataSet.get("text"));
			 * fc.utobj().printTestStep(testCaseId,
			 * "Navigating to Admin > CRM > Source Summary");
			 * fc.utobj().printTestStep(testCaseId, "Add Source");
			 * addSourcePage.addSource(driver, source, description,
			 * displayOnWebPageText, config);
			 * fc.utobj().printTestStep(testCaseId, "Add Source Details");
			 * String sourceDetails =
			 * fc.utobj().generateTestData(dataSet.get("sourceDetails"));
			 * addSourcePage.addSourceDetails(driver, source, sourceDetails,
			 * displayOnWebPageText, description, config);
			 * fc.utobj().printTestStep(testCaseId,
			 * "Navigating to Admin > CRM > Configure Medium");
			 * fc.utobj().printTestStep(testCaseId, "Add Medium");
			 * AdminCRMConfigureMediumPageTest mediumPage = new
			 * AdminCRMConfigureMediumPageTest(); String medium =
			 * fc.utobj().generateTestData(dataSet.get("medium"));
			 * mediumPage.addMedium(driver, medium, description, config);
			 */

			fc.utobj().printTestStep("Navigate To CRM > Contacts > Contacts Summary Page");
			fc.utobj().printTestStep("Add Contact");
			fc.crm().crm_common().CRMContactsLnk(driver);
			fc.utobj().clickElement(driver, pobj.addNew);
			fc.utobj().clickElement(driver, pobj.contactLnk);

			String firstName = fc.utobj().generateTestData(dataSet.get("firstName"));
			String lastName = fc.utobj().generateTestData(dataSet.get("lastName"));
			String primaryCMethod = dataSet.get("primaryCMethod");
			String assignTo = "Corporate";
			String regionName = "";
			String franchiseId = "";
			String title = dataSet.get("title");

			addContactGeneric(driver, contactType, firstName, lastName, primaryCMethod, assignTo,
					corpUser.getuserFullName(), regionName, franchiseId, title, emailId, config);

			fc.utobj().clickElement(driver, pobj.contactsLinks);
			contactsFilter(driver, contactType);
			fc.utobj().printTestStep("Log A Task");
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//a[contains(text () , '" + firstName + " " + lastName + "')]"));

			fc.utobj().clickElement(driver, pobj.logATaskLink);

			fc.commonMethods().switch_cboxIframe_frameId(driver);
			if (!fc.utobj().isSelected(driver,pobj.radioOwner)) {
				fc.utobj().clickElement(driver, pobj.radioOwner);
			}
			String statusTask = dataSet.get("statusTask");
			fc.utobj().selectDropDown(driver, pobj.statusTask, statusTask);
			String subject = fc.utobj().generateTestData(dataSet.get("subject"));
			fc.utobj().sendKeys(driver, pobj.subject, subject);
			if (!fc.utobj().isSelected(driver, pobj.timeLessTask)) {
				fc.utobj().clickElement(driver, pobj.timeLessTask);
			}
			String priority = dataSet.get("priority");
			fc.utobj().selectDropDown(driver, pobj.priority, priority);
			fc.utobj().clickElement(driver, pobj.createBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Modify A Task");
			actionImgOption(driver, subject, "Modify");
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			subject = fc.utobj().generateTestData(dataSet.get("subject"));
			fc.utobj().sendKeys(driver, pobj.subject, subject);
			fc.utobj().clickElement(driver, pobj.saveBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Verify Modify Task");
			boolean isSubjectPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[contains(text () ,'" + subject + "')]");
			if (isSubjectPresent == false) {
				fc.utobj().throwsException("was not able to verify subject");
			}
			boolean isUserPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[contains(text () ,'" + subject
					+ "')]/ancestor::tr/td/div[.='" + corpUser.getuserFullName() + "']");
			if (isUserPresent == false) {
				fc.utobj().throwsException("was not able to verify userName at Lead Info Page");
			}
			boolean isStatusOfTaskPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[contains(text () ,'" + subject
					+ "')]/ancestor::tr/td[contains(text () ,'" + statusTask + "')]");
			if (isStatusOfTaskPresent == false) {
				fc.utobj().throwsException("was not able to verify status of task");
			}
			boolean isPriorityPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[contains(text () ,'" + subject
					+ "')]/ancestor::tr/td[contains(text () ,'" + priority + "')]");
			if (isPriorityPresent == false) {
				fc.utobj().throwsException("was not able to verify Priority of task");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "crm")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Delete A Task By Action Image At CRM > Contact > Primary Info", testCaseId = "TC_205_Delete_Task")
	public void deleteTaskActionImage() throws IOException, Exception, ParserConfigurationException, SAXException {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			CRMContactsPage pobj = new CRMContactsPage(driver);

			fc.utobj().printTestStep("Navigate To Admin Users Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			String emailId = "crmautomation@staffex.com";
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUser = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporateUser.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);
			fc.utobj().printTestStep("Navigate To Admin > CRM > Contact Type Configuration");
			fc.utobj().printTestStep("Add Contact Type");
			AdminCRMContactTypeConfigurationPageTest contatcTypePage = new AdminCRMContactTypeConfigurationPageTest();
			String contactType = fc.utobj().generateTestData(dataSet.get("contactType"));
			contatcTypePage.addContactType(driver, contactType);

			/*
			 * fc.utobj().printTestStep(testCaseId,
			 * "Navigating to Admin > CRM > Configure Lead Type");
			 * fc.utobj().printTestStep(testCaseId, "Add Lead Type");
			 * AdminCRMConfigureLeadTypePageTest leadTypePage = new
			 * AdminCRMConfigureLeadTypePageTest(); String leadType =
			 * fc.utobj().generateTestData(dataSet.get("leadType"));
			 * leadTypePage.addLeadType(driver, leadType, config);
			 * 
			 * AdminCRMSourceSummaryPageTest addSourcePage = new
			 * AdminCRMSourceSummaryPageTest(); String source =
			 * fc.utobj().generateTestData(dataSet.get("source")); String
			 * description =
			 * fc.utobj().generateTestData(dataSet.get("description")); String
			 * displayOnWebPageText =
			 * fc.utobj().generateTestData(dataSet.get("text"));
			 * fc.utobj().printTestStep(testCaseId,
			 * "Navigating to Admin > CRM > Source Summary");
			 * fc.utobj().printTestStep(testCaseId, "Add Source");
			 * addSourcePage.addSource(driver, source, description,
			 * displayOnWebPageText, config);
			 * fc.utobj().printTestStep(testCaseId, "Add Source Details");
			 * String sourceDetails =
			 * fc.utobj().generateTestData(dataSet.get("sourceDetails"));
			 * addSourcePage.addSourceDetails(driver, source, sourceDetails,
			 * displayOnWebPageText, description, config);
			 * fc.utobj().printTestStep(testCaseId,
			 * "Navigating to Admin > CRM > Configure Medium");
			 * fc.utobj().printTestStep(testCaseId, "Add Medium");
			 * AdminCRMConfigureMediumPageTest mediumPage = new
			 * AdminCRMConfigureMediumPageTest(); String medium =
			 * fc.utobj().generateTestData(dataSet.get("medium"));
			 * mediumPage.addMedium(driver, medium, description, config);
			 */

			fc.utobj().printTestStep("Navigate To CRM > Contacts > Contacts Summary Page");
			fc.utobj().printTestStep("Add Contact");
			fc.crm().crm_common().CRMContactsLnk(driver);
			fc.utobj().clickElement(driver, pobj.addNew);
			fc.utobj().clickElement(driver, pobj.contactLnk);

			String firstName = fc.utobj().generateTestData(dataSet.get("firstName"));
			String lastName = fc.utobj().generateTestData(dataSet.get("lastName"));
			String primaryCMethod = dataSet.get("primaryCMethod");
			String assignTo = "Corporate";
			String regionName = "";
			String franchiseId = "";
			String title = dataSet.get("title");

			addContactGeneric(driver, contactType, firstName, lastName, primaryCMethod, assignTo,
					corpUser.getuserFullName(), regionName, franchiseId, title, emailId, config);

			fc.utobj().clickElement(driver, pobj.contactsLinks);
			contactsFilter(driver, contactType);
			fc.utobj().printTestStep("Delete Task");
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//a[contains(text () , '" + firstName + " " + lastName + "')]"));
			fc.utobj().clickElement(driver, pobj.logATaskLink);

			fc.commonMethods().switch_cboxIframe_frameId(driver);
			if (!fc.utobj().isSelected(driver,pobj.radioOwner)) {
				fc.utobj().clickElement(driver, pobj.radioOwner);
			}
			String statusTask = dataSet.get("statusTask");
			fc.utobj().selectDropDown(driver, pobj.statusTask, statusTask);
			String subject = fc.utobj().generateTestData(dataSet.get("subject"));
			fc.utobj().sendKeys(driver, pobj.subject, subject);
			if (!fc.utobj().isSelected(driver, pobj.timeLessTask)) {
				fc.utobj().clickElement(driver, pobj.timeLessTask);
			}
			String priority = dataSet.get("priority");
			fc.utobj().selectDropDown(driver, pobj.priority, priority);
			fc.utobj().clickElement(driver, pobj.createBtn);
			fc.utobj().switchFrameToDefault(driver);

			actionImgOption(driver, subject, "Delete");
			fc.utobj().acceptAlertBox(driver);

			fc.utobj().printTestStep("Verify The Delete Task");
			boolean isNoRecordPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//td[.='No records found.']");
			if (isNoRecordPresent == false) {
				fc.utobj().throwsException("was not able to Delete Task");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "crm")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify the Process A Task By Link At CRM > Contact > Primary Info", testCaseId = "TC_206_Process_Task")
	public void processTaskActionImage() throws IOException, Exception, ParserConfigurationException, SAXException {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			CRMContactsPage pobj = new CRMContactsPage(driver);
			fc.utobj().printTestStep("Navigate To Admin Users Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");

			AdminUsersManageCorporateUsersAddCorporateUserPageTest corpTest = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			String emailId = "crmautomation@staffex.com";
			CorporateUser corpUser = new CorporateUser();
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corpUser.setEmail(emailId);
			corpUser = corpTest.createDefaultUser(driver, corpUser);
			fc.utobj().printTestStep("Navigate To Admin > CRM > Contact Type Configuration");
			fc.utobj().printTestStep("Add Contact Type");
			AdminCRMContactTypeConfigurationPageTest contatcTypePage = new AdminCRMContactTypeConfigurationPageTest();
			String contactType = fc.utobj().generateTestData(dataSet.get("contactType"));
			contatcTypePage.addContactType(driver, contactType);
			/*
			 * fc.utobj().printTestStep(testCaseId,
			 * "Navigating to Admin > CRM > Configure Lead Type");
			 * fc.utobj().printTestStep(testCaseId, "Add Lead Type");
			 * AdminCRMConfigureLeadTypePageTest leadTypePage = new
			 * AdminCRMConfigureLeadTypePageTest(); String leadType =
			 * fc.utobj().generateTestData(dataSet.get("leadType"));
			 * leadTypePage.addLeadType(driver, leadType, config);
			 * 
			 * AdminCRMSourceSummaryPageTest addSourcePage = new
			 * AdminCRMSourceSummaryPageTest(); String source =
			 * fc.utobj().generateTestData(dataSet.get("source")); String
			 * description =
			 * fc.utobj().generateTestData(dataSet.get("description")); String
			 * displayOnWebPageText =
			 * fc.utobj().generateTestData(dataSet.get("text"));
			 * fc.utobj().printTestStep(testCaseId,
			 * "Navigating to Admin > CRM > Source Summary");
			 * fc.utobj().printTestStep(testCaseId, "Add Source");
			 * addSourcePage.addSource(driver, source, description,
			 * displayOnWebPageText, config);
			 * fc.utobj().printTestStep(testCaseId, "Add Source Details");
			 * String sourceDetails =
			 * fc.utobj().generateTestData(dataSet.get("sourceDetails"));
			 * addSourcePage.addSourceDetails(driver, source, sourceDetails,
			 * displayOnWebPageText, description, config);
			 * fc.utobj().printTestStep(testCaseId,
			 * "Navigating to Admin > CRM > Configure Medium");
			 * fc.utobj().printTestStep(testCaseId, "Add Medium");
			 * AdminCRMConfigureMediumPageTest mediumPage = new
			 * AdminCRMConfigureMediumPageTest(); String medium =
			 * fc.utobj().generateTestData(dataSet.get("medium"));
			 * mediumPage.addMedium(driver, medium, description, config);
			 */

			fc.utobj().printTestStep("Navigate To CRM > Contacts > Contacts Summary Page");
			fc.utobj().printTestStep("Add Contact");
			fc.crm().crm_common().CRMContactsLnk(driver);
			fc.utobj().clickElement(driver, pobj.addNew);
			fc.utobj().clickElement(driver, pobj.contactLnk);

			String firstName = fc.utobj().generateTestData(dataSet.get("firstName"));
			String lastName = fc.utobj().generateTestData(dataSet.get("lastName"));
			String primaryCMethod = dataSet.get("primaryCMethod");
			String assignTo = "Corporate";
			String regionName = "";
			String franchiseId = "";
			String title = dataSet.get("title");

			addContactGeneric(driver, contactType, firstName, lastName, primaryCMethod, assignTo,
					corpUser.getuserFullName(), regionName, franchiseId, title, emailId, config);

			fc.utobj().clickElement(driver, pobj.contactsLinks);
			contactsFilter(driver, contactType);
			fc.utobj().printTestStep("Log A Task");
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//a[contains(text () , '" + firstName + " " + lastName + "')]"));

			fc.utobj().clickElement(driver, pobj.logATaskLink);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			if (!fc.utobj().isSelected(driver,pobj.radioOwner)) {
				fc.utobj().clickElement(driver, pobj.radioOwner);
			}
			String statusTask = dataSet.get("statusTask");
			fc.utobj().selectDropDown(driver, pobj.statusTask, statusTask);
			String subject = fc.utobj().generateTestData(dataSet.get("subject"));
			fc.utobj().sendKeys(driver, pobj.subject, subject);
			if (!fc.utobj().isSelected(driver, pobj.timeLessTask)) {
				fc.utobj().clickElement(driver, pobj.timeLessTask);
			}
			String priority = dataSet.get("priority");
			fc.utobj().selectDropDown(driver, pobj.priority, priority);
			fc.utobj().clickElement(driver, pobj.createBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Process A Task");

			actionImgOption(driver, subject, "Process");
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			String statusTaskM = dataSet.get("statusTaskM");
			fc.utobj().selectDropDown(driver, pobj.statusTask, statusTaskM);
			fc.utobj().clickElement(driver, pobj.processTask);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().refresh(driver);

			boolean isNoRecordPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//td[.='No records found.']");
			if (isNoRecordPresent == false) {
				fc.utobj().throwsException("was not able to Process The Task");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "crm")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Change Owner Of Contact By Bottom Button Option At CRM > Contacts > Primary Info", testCaseId = "TC_207_Change_Owner")
	public void changeOwnerBottomBtn1() throws IOException, Exception, ParserConfigurationException, SAXException {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			CRMContactsPage pobj = new CRMContactsPage(driver);

			fc.utobj().printTestStep("Navigate To Admin > Users > Manage Regional User");
			fc.utobj().printTestStep("Add Regional User");
			AdminUsersManageRegionalUsersAddRegionalUserPageTest regionalUserPage = new AdminUsersManageRegionalUsersAddRegionalUserPageTest();
			String userNameReg = fc.utobj().generateTestData(dataSet.get("userNameReg"));
			String region = fc.utobj().generateTestData(dataSet.get("regionName"));
			String emailId = "crmautomation@staffex.com";
			regionalUserPage.addRegionalUser(driver, userNameReg, region, emailId);
			fc.utobj().printTestStep("Navigate To Admin Users Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");

			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUser = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporateUser.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);
			fc.utobj().printTestStep("Navigate To Admin > CRM > Contact Type Configuration");
			fc.utobj().printTestStep("Add Contact Type");

			AdminCRMContactTypeConfigurationPageTest contatcTypePage = new AdminCRMContactTypeConfigurationPageTest();
			String contactType = fc.utobj().generateTestData(dataSet.get("contactType"));
			contatcTypePage.addContactType(driver, contactType);

			/*
			 * fc.utobj().printTestStep(testCaseId,
			 * "Navigating to Admin > CRM > Configure Lead Type");
			 * fc.utobj().printTestStep(testCaseId, "Add Lead Type");
			 * AdminCRMConfigureLeadTypePageTest leadTypePage = new
			 * AdminCRMConfigureLeadTypePageTest(); String leadType =
			 * fc.utobj().generateTestData(dataSet.get("leadType"));
			 * leadTypePage.addLeadType(driver, leadType, config);
			 * 
			 * AdminCRMSourceSummaryPageTest addSourcePage = new
			 * AdminCRMSourceSummaryPageTest(); String source =
			 * fc.utobj().generateTestData(dataSet.get("source")); String
			 * description =
			 * fc.utobj().generateTestData(dataSet.get("description")); String
			 * displayOnWebPageText =
			 * fc.utobj().generateTestData(dataSet.get("text"));
			 * fc.utobj().printTestStep(testCaseId,
			 * "Navigating to Admin > CRM > Source Summary");
			 * fc.utobj().printTestStep(testCaseId, "Add Source");
			 * addSourcePage.addSource(driver, source, description,
			 * displayOnWebPageText, config);
			 * fc.utobj().printTestStep(testCaseId, "Add Source Details");
			 * String sourceDetails =
			 * fc.utobj().generateTestData(dataSet.get("sourceDetails"));
			 * addSourcePage.addSourceDetails(driver, source, sourceDetails,
			 * displayOnWebPageText, description, config);
			 * fc.utobj().printTestStep(testCaseId,
			 * "Navigating to Admin > CRM > Configure Medium");
			 * fc.utobj().printTestStep(testCaseId, "Add Medium");
			 * AdminCRMConfigureMediumPageTest mediumPage = new
			 * AdminCRMConfigureMediumPageTest(); String medium =
			 * fc.utobj().generateTestData(dataSet.get("medium"));
			 * mediumPage.addMedium(driver, medium, description, config);
			 */
			fc.utobj().printTestStep("Navigate To CRM > Contacts > Contacts Summary Page");
			fc.utobj().printTestStep("Add Contact");
			fc.crm().crm_common().CRMContactsLnk(driver);
			fc.utobj().clickElement(driver, pobj.addNew);
			fc.utobj().clickElement(driver, pobj.contactLnk);

			String firstName = fc.utobj().generateTestData(dataSet.get("firstName"));
			String lastName = fc.utobj().generateTestData(dataSet.get("lastName"));
			String primaryCMethod = dataSet.get("primaryCMethod");
			String assignTo = "Corporate";
			String regionName = "";
			String franchiseId = "";
			String title = dataSet.get("title");

			addContactGeneric(driver, contactType, firstName, lastName, primaryCMethod, assignTo,
					corpUser.getuserFullName(), regionName, franchiseId, title, emailId, config);

			fc.utobj().clickElement(driver, pobj.contactsLinks);
			contactsFilter(driver, contactType);
			fc.utobj().printTestStep("Change Owner");
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//a[contains(text () , '" + firstName + " " + lastName + "')]"));
			fc.utobj().clickElement(driver, pobj.changeOwnerBtmBtn);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			if (!fc.utobj().isSelected(driver, pobj.selectRegionalUser1)) {
				fc.utobj().clickElement(driver, pobj.selectRegionalUser1);
			}
			fc.utobj().selectDropDown(driver, pobj.selectAreaRegion, region);
			fc.utobj().selectDropDown(driver, pobj.selectRegionalUser, userNameReg + " " + userNameReg);
			fc.utobj().clickElement(driver, pobj.assignBtn);
			fc.utobj().clickElement(driver, pobj.closeBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().clickElement(driver, pobj.contactsLinks);
			contactsFilter(driver, contactType);
			fc.utobj().printTestStep("Verify Chnge Owner");
			boolean isOwnerPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[contains(text () ,'" + firstName + " "
					+ lastName + "')]/ancestor::tr/td[contains(text () ,'" + userNameReg + " " + userNameReg + "')]");
			if (isOwnerPresent == false) {
				fc.utobj().throwsException("was not able to verify Owner");
			}
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//a[contains(text () ,'" + firstName + " " + lastName + "')]"));

			boolean isRegionNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () ,'Area / Region')]/ancestor::tr/td[.='" + region + "']");
			if (isRegionNamePresent == false) {
				fc.utobj().throwsException("was not able to verify Region name CRM > Contact > Primary Info");
			}

			boolean isRegionalUserPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () ,'Assign To')]/ancestor::tr/td[contains(text () ,'" + userNameReg + " "
							+ userNameReg + "')]");
			if (isRegionalUserPresent == false) {
				fc.utobj().throwsException("was not able to verify Regional User 'Owner' CRM > Contact > Primary Info");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "crm"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Contact Add To Group Bottom Btn At CRM > Contact > Primary Info", testCaseId = "TC_208_Add_To_Group")
	public void addToGroupBottomBtn1() throws IOException, Exception, ParserConfigurationException, SAXException {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			CRMContactsPage pobj = new CRMContactsPage(driver);
			CRMGroupsPageTest groupPage = new CRMGroupsPageTest();

			CRMAccountsPageTest accountPage = new CRMAccountsPageTest();

			fc.utobj().printTestStep("Navigate To Admin Users Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			String emailId = "crmautomation@staffex.com";
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUser = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporateUser.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			fc.utobj().printTestStep("Navigate To Admin > CRM > Contact Type Configuration");
			fc.utobj().printTestStep("Add Contact Type");
			AdminCRMContactTypeConfigurationPageTest contatcTypePage = new AdminCRMContactTypeConfigurationPageTest();
			String contactType = fc.utobj().generateTestData(dataSet.get("contactType"));
			contatcTypePage.addContactType(driver, contactType);

			fc.utobj().printTestStep("Navigate To CRM > Contacts > Contacts Summary Page");
			fc.utobj().printTestStep("Add Contact");
			fc.crm().crm_common().CRMContactsLnk(driver);
			fc.utobj().clickElement(driver, pobj.addNew);
			fc.utobj().clickElement(driver, pobj.contactLnk);

			String firstName = fc.utobj().generateTestData(dataSet.get("firstName"));
			String lastName = fc.utobj().generateTestData(dataSet.get("lastName"));
			String primaryCMethod = dataSet.get("primaryCMethod");
			String assignTo = "Corporate";
			String regionName = "";
			String franchiseId = "";
			String title = dataSet.get("title");

			addContactGeneric(driver, contactType, firstName, lastName, primaryCMethod, assignTo,
					corpUser.getuserFullName(), regionName, franchiseId, title, emailId, config);

			fc.utobj().clickElement(driver, pobj.contactsLinks);
			contactsFilter(driver, contactType);
			fc.utobj().printTestStep("Add To Group");
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//a[contains(text () , '" + firstName + " " + lastName + "')]"));

			fc.utobj().printTestStep("Create Group");
			fc.utobj().clickElement(driver, pobj.addtoGroupBtmBtn);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().clickElement(driver, pobj.addToNewGroup);
			fc.utobj().switchFrameToDefault(driver);

			/*
			 * String groupName =
			 * fc.utobj().generateTestData(dataSet.get("groupName"));
			 * fc.utobj().sendKeys(driver, pobj.groupName, groupName); String
			 * groupDescription =
			 * fc.utobj().generateTestData(dataSet.get("groupDescription"));
			 * fc.utobj().sendKeys(driver, pobj.groupDescription,
			 * groupDescription); fc.utobj().clickElement(driver,
			 * pobj.contactTypeTab); fc.utobj().clickElement(driver,
			 * pobj.accessibility); fc.utobj().clickElement(driver,
			 * pobj.saveAndContinueBtn);
			 * 
			 * // select Criteria fc.utobj().clickElement(driver,
			 * pobj.fieldSelectBtn); fc.utobj().sendKeys(driver,
			 * pobj.searchField, "Contact Type");
			 * fc.utobj().clickElement(driver,
			 * fc.utobj().getElementByXpath(driver,
			 * ".//*[@id='fc-drop-parentselColList']//span[.='Contact Type']"
			 * ))); fc.utobj().clickElement(driver, pobj.fieldSelectBtn);
			 * 
			 * // configure Criteria fc.utobj().selectDropDown(driver,
			 * pobj.assignToCriteria, "In"); fc.utobj().clickElement(driver,
			 * pobj.selectAllCriteria); fc.utobj().clickElement(driver,
			 * pobj.selectAllContactType); fc.utobj().clickElement(driver,
			 * pobj.saveAndContinueBtn); fc.utobj().clickElement(driver,
			 * pobj.associateWithGroupBtn); fc.utobj().switchFrame(driver,
			 * fc.utobj().getElementByXpath(driver,
			 * ".//iframe[@class='newLayoutcboxIframe']"));
			 * fc.utobj().clickElement(driver, pobj.okBtn);
			 * fc.utobj().switchFrameToDefault(driver);
			 */

			fc.utobj().printTestStep("Create New Group");
			String groupName = fc.utobj().generateTestData(dataSet.get("groupName"));
			String accessibility = "Public to all Users";
			groupPage.addToGroupRegular(driver, groupName, accessibility);

			/*
			 * fc.utobj().sendKeys(driver, pobj.searchAtGroup, groupName);
			 * fc.utobj().clickElement(driver, pobj.exactSearchGroup);
			 * fc.utobj().clickElement(driver, pobj.searchAccountBtn);
			 */

			new CRMGroupsPageTest().searchGroupsByFilter(driver, groupName, "Contact");

			String colunmName = "";
			int z = 0;

			// click over Recipients
			List<WebElement> listHead = driver
					.findElements(By.xpath(".//a[.='" + groupName + "']/ancestor::table/thead/tr/th"));
			System.out.println(listHead);
			for (int i = 0; i < listHead.size(); i++) {

				colunmName = listHead.get(i).getText().trim();
				if (colunmName.equalsIgnoreCase("Recipients")) {
					z = i;
					break;
				}
			}

			if (z != 0) {
				z = z + 1;
				fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
						".//a[.='" + groupName + "']/ancestor::tr/td[" + z + "]/a"));
			} else {
				fc.utobj().throwsException("was not able to find Recipients Column");
			}

			//accountPage.searchContactByContactType(driver, contactType);
			contactsFilter(driver, contactType);
			fc.utobj().printTestStep("Verify Contact Add To Group");
			boolean isContactNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firstName + " " + lastName + "')]");
			if (isContactNamePresent == false) {
				fc.utobj().throwsException("was not able to add in Group");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "crm", "crmmodule" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2017-08-10", testCaseDescription = "Verify The Contact Associated With Email Campaign By More Action Btn At CRM > Contact > Primary Info", testCaseId = "TC_209_Associate_Wit_Email_Campaign")
	public void associatesWithEmailCampaignBottomBtn1()
			throws IOException, Exception, ParserConfigurationException, SAXException {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			CRMContactsPage pobj = new CRMContactsPage(driver);

			fc.utobj().printTestStep("Navigate To CRM >Campaign Center");
			fc.utobj().printTestStep("Add Campaign");
			CRMCampaignCenterPageTestForCRM campaignCenterPage = new CRMCampaignCenterPageTestForCRM();
			String campaignName = fc.utobj().generateTestData(dataSet.get("campaignName"));
			String campaignDescription = fc.utobj().generateTestData(dataSet.get("campaignDescription"));
			String campaignAccessibility = dataSet.get("campaignAccessibility");
			String category = dataSet.get("category");
			String templateName = fc.utobj().generateTestData(dataSet.get("templateName"));
			String mailSubject = dataSet.get("mailSubject");
			String templateText = fc.utobj().generateTestData(dataSet.get("templateText"));
			campaignCenterPage.createCampaign(driver, campaignName, campaignDescription, campaignAccessibility,
					category, templateName, mailSubject, templateText, config);

			fc.utobj().printTestStep("Navigate To Admin Users Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			String emailId = "crmautomation@staffex.com";
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUser = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporateUser.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);
			fc.utobj().printTestStep("Navigate To Admin > CRM > Contact Type Configuration");
			fc.utobj().printTestStep("Add Contact Type");

			String contactType = "Contacts";
			fc.utobj().printTestStep("Navigate To CRM > Contacts > Contacts Summary Page");
			fc.utobj().printTestStep("Add Contact");
			fc.crm().crm_common().CRMContactsLnk(driver);
			fc.utobj().clickElement(driver, pobj.addNew);
			fc.utobj().clickElement(driver, pobj.contactLnk);

			String firstName = fc.utobj().generateTestData(dataSet.get("firstName"));
			String lastName = fc.utobj().generateTestData(dataSet.get("lastName"));
			String primaryCMethod = dataSet.get("primaryCMethod");
			String assignTo = "Corporate";
			String regionName = "";
			String franchiseId = "";
			String title = dataSet.get("title");

			addContactGeneric(driver, contactType, firstName, lastName, primaryCMethod, assignTo,
					corpUser.getuserFullName(), regionName, franchiseId, title, emailId, config);

			/*
			 * fc.utobj().sendKeys(driver, pobj.searchContacts, firstName+" "
			 * +lastName); fc.utobj().clickElement(driver,
			 * pobj.searchAccountBtn);
			 */

			/*
			 * fc.utobj().clickElement(driver, pobj.contactsLinks);
			 * contactsFilter(driver, contactType);
			 */
			fc.utobj().printTestStep("Associate With Campaign");
			// fc.utobj().clickElement(driver,
			// fc.utobj().getElementByXpath(driver,".//a[contains(text () ,
			// '"+firstName+" "+lastName+"')]"));
			fc.utobj().clickElement(driver, pobj.associateWithEmailCampaignBtmBtn);
			fc.utobj().clickElement(driver, pobj.filterContainer);
			fc.utobj().sendKeys(driver, pobj.searchCampaign, campaignName);
			fc.utobj().clickElement(driver, pobj.applyFilter);

			fc.utobj().clickElement(driver, new CRMLeadsPage(driver).sendCampaignBtn);
			fc.utobj().clickElement(driver, pobj.confirmBtn);

			fc.utobj().clickElement(driver, pobj.contactsLinks);
			// fc.utobj().sendKeys(driver, pobj.searchContacts, firstName+"
			// "+lastName);
			// fc.utobj().clickElement(driver, pobj.exactSearchGroup);
			// fc.utobj().clickElement(driver, pobj.searchAccountBtn );
			contactsFilter(driver, contactType);
			fc.utobj().printTestStep("Verify Contact Associate With Campaign");
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//a[contains(text () ,'" + firstName + " " + lastName + "')]"));
			boolean isCampaignNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () ,'Promotional Campaign')]/following-sibling::td[.='" + campaignName + "']");
			if (isCampaignNamePresent == false) {
				fc.utobj().throwsException("was not able to verify Campign Name");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "crm")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Archive Contact By Bottom Button At CRM > Contact > Primary Info", testCaseId = "TC_210_Archive_Contact")
	public void archiveContactBottomBtn1() throws IOException, Exception, ParserConfigurationException, SAXException {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			CRMContactsPage pobj = new CRMContactsPage(driver);

			fc.utobj().printTestStep("Navigate To Admin Users Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			String emailId = "crmautomation@staffex.com";
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUser = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporateUser.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);
			fc.utobj().printTestStep("Navigate To Admin > CRM > Contact Type Configuration");
			fc.utobj().printTestStep("Add Contact Type");
			AdminCRMContactTypeConfigurationPageTest contatcTypePage = new AdminCRMContactTypeConfigurationPageTest();
			String contactType = fc.utobj().generateTestData(dataSet.get("contactType"));
			contatcTypePage.addContactType(driver, contactType);

			fc.utobj().printTestStep("Navigate To CRM > Contacts > Contacts Summary Page");
			fc.utobj().printTestStep("Add Contact");
			fc.crm().crm_common().CRMContactsLnk(driver);
			fc.utobj().clickElement(driver, pobj.addNew);
			fc.utobj().clickElement(driver, pobj.contactLnk);

			String firstName = fc.utobj().generateTestData(dataSet.get("firstName"));
			String lastName = fc.utobj().generateTestData(dataSet.get("lastName"));
			String primaryCMethod = dataSet.get("primaryCMethod");
			String assignTo = "Corporate";
			String regionName = "";
			String franchiseId = "";
			String title = dataSet.get("title");

			addContactGeneric(driver, contactType, firstName, lastName, primaryCMethod, assignTo,
					corpUser.getuserFullName(), regionName, franchiseId, title, emailId, config);

			fc.utobj().clickElement(driver, pobj.contactsLinks);
			contactsFilter(driver, contactType);
			fc.utobj().printTestStep("Archive Contact");
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//a[contains(text () , '" + firstName + " " + lastName + "')]"));
			fc.utobj().clickElement(driver, pobj.archiveBtmBtn);
			fc.utobj().acceptAlertBox(driver);

			contactsFilter(driver, contactType);
			fc.utobj().printTestStep("Verify Archive Contact At Contacts Page");
			boolean isNoRecordPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//td[.='No records found.']");
			if (isNoRecordPresent == false) {
				fc.utobj().throwsException("was not able to Archive Lead");
			}

			fc.utobj().clickElement(driver, pobj.searchLink);
			fc.utobj().clickElement(driver, pobj.contactsTabAtSearch);
			if (!fc.utobj().isSelected(driver, pobj.archiveContacts)) {
				fc.utobj().clickElement(driver, pobj.archiveContacts);
			}
			fc.utobj().setToDefault(driver, pobj.assignTo);
			WebElement elementInput = fc.utobj().getElement(driver, pobj.assignTo)
					.findElement(By.xpath("./div/div/input"));
			fc.utobj().selectValFromMultiSelect(driver, pobj.assignTo, elementInput, corpUser.getuserFullName());
			fc.utobj().clickElement(driver, pobj.searchBtnAtSearch);

			fc.utobj().printTestStep("Verify Archive Contact At Search Page");
			boolean isNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firstName + " " + lastName + "')]");
			if (isNamePresent == false) {
				fc.utobj().throwsException("was not able to verify Archive Contacts");
			}
			boolean isOwnerPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[contains(text () ,'" + firstName + " "
					+ lastName + "')]/ancestor::tr/td[contains(text () , '" + corpUser.getuserFullName() + "')]");
			if (isOwnerPresent == false) {
				fc.utobj().throwsException("was not able to verify Owner of Archive Contacts");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = {"crm"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Remove Account At CRM > Contact > Account Info", testCaseId = "TC_211_Remove_Account")
	public void removeAccount() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			CRMAccountsPage pobj = new CRMAccountsPage(driver);
			fc.utobj().printTestStep("Navigate To Admin Users Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			CRMAccountsPageTest accountPage = new CRMAccountsPageTest();
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corpTest = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			//String emailId = "crmautomation@staffex.com";
			String emailId=fc.utobj().generateTestData("crmautomation")+"@staffex.com";
			CorporateUser corpUser = new CorporateUser();
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corpUser.setEmail(emailId);
			corpUser = corpTest.createDefaultUser(driver, corpUser);
			fc.utobj().printTestStep("Navigating to Admin > CRM > ContactType Configuration");
			fc.utobj().printTestStep("Add Contact Type");
			AdminCRMContactTypeConfigurationPageTest contactTypePage = new AdminCRMContactTypeConfigurationPageTest();
			String contactType = fc.utobj().generateTestData(dataSet.get("contactType"));
			contactTypePage.addContactType(driver, contactType);

			AdminCRMConfigureIndustryPageTest industryPage = new AdminCRMConfigureIndustryPageTest();
			String industry = fc.utobj().generateTestData(dataSet.get("industry"));
			String description = fc.utobj().generateTestData(dataSet.get("description"));
			fc.utobj().printTestStep("Navigating to Admin > CRM >  Configure Industry Type");
			fc.utobj().printTestStep("Add Industry");
			industryPage.addIndustry(driver, industry, description);

			String accountName = fc.utobj().generateTestData(dataSet.get("accountName"));
			String visibility = dataSet.get("visibility");
			fc.utobj().printTestStep("Navigating to CRM >  Accounts > Accounts Summary");
			fc.utobj().printTestStep("Add Account");
			accountPage.addAcocunt(driver, industry, accountName, visibility);

			fc.utobj().printTestStep("Navigate To CRM > Contacts > Contacts Summary Page");
			fc.utobj().printTestStep("Add Contact");
			fc.utobj().clickElement(driver, pobj.contactsLinks);
			fc.utobj().clickElement(driver, pobj.addNew);
			fc.utobj().clickElement(driver, pobj.contactLnk);

			String cFname = fc.utobj().generateTestData(dataSet.get("cFname"));
			String cLname = fc.utobj().generateTestData(dataSet.get("cLname"));
			String primaryCMethod = dataSet.get("primaryCMethod");
			String assignTo = "Corporate";
			String regionName = "";
			String franchiseId = "";
			String title = dataSet.get("title");

			addContactGenericWithAccount(driver, contactType, cFname, cLname, primaryCMethod, assignTo,
					corpUser.getuserFullName(), regionName, franchiseId, title, emailId, config, accountName);

			fc.utobj().printTestStep("Remove Account");
			fc.utobj().clickElement(driver, pobj.accountInfoTab);
			fc.utobj().clickElement(driver, pobj.removeAccountLink);
			fc.utobj().acceptAlertBox(driver);

			fc.utobj().printTestStep("Verify Remove Account");
			boolean isAssociateBtnPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//input[@value='Associate']");
			if (isAssociateBtnPresent == false) {
				fc.utobj().throwsException("was not able to verify remove account");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "crm"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Associate Account Into Contact At CRM > Contact > Account Info", testCaseId = "TC_212_Associate_Account")
	public void associateAccount() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			CRMAccountsPage pobj = new CRMAccountsPage(driver);
			fc.utobj().printTestStep("Navigate To Admin Users Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			CRMAccountsPageTest accountPage = new CRMAccountsPageTest();
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corpTest = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			/*String emailId = "crmautomation@staffex.com";*/
			String emailId=fc.utobj().generateTestData("crmautomation")+"@staffex.com";
			CorporateUser corpUser = new CorporateUser();
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corpUser.setEmail(emailId);
			corpUser = corpTest.createDefaultUser(driver, corpUser);
			fc.utobj().printTestStep("Navigating to Admin > CRM > ContactType Configuration");
			fc.utobj().printTestStep("Add Contact Type");
			AdminCRMContactTypeConfigurationPageTest contactTypePage = new AdminCRMContactTypeConfigurationPageTest();
			String contactType = fc.utobj().generateTestData(dataSet.get("contactType"));
			contactTypePage.addContactType(driver, contactType);

			AdminCRMConfigureIndustryPageTest industryPage = new AdminCRMConfigureIndustryPageTest();
			String industry = fc.utobj().generateTestData(dataSet.get("industry"));
			String description = fc.utobj().generateTestData(dataSet.get("description"));
			fc.utobj().printTestStep("Navigating to Admin > CRM >  Configure Industry Type");
			fc.utobj().printTestStep("Add Industry");
			industryPage.addIndustry(driver, industry, description);

			String accountName = fc.utobj().generateTestData(dataSet.get("accountName"));
			String visibility = dataSet.get("visibility");
			fc.utobj().printTestStep("Navigating to CRM >  Accounts > Accounts Summary");
			fc.utobj().printTestStep("Add Account");
			accountPage.addAcocunt(driver, industry, accountName, visibility);

			fc.utobj().printTestStep("Navigate To CRM > Contacts > Contacts Summary Page");
			fc.utobj().printTestStep("Add Contact");
			fc.utobj().clickElement(driver, pobj.contactsLinks);
			fc.utobj().clickElement(driver, pobj.addNew);
			fc.utobj().clickElement(driver, pobj.contactLnk);

			String cFname = fc.utobj().generateTestData(dataSet.get("cFname"));
			String cLname = fc.utobj().generateTestData(dataSet.get("cLname"));
			String primaryCMethod = dataSet.get("primaryCMethod");
			String assignTo = "Corporate";
			String regionName = "";
			String franchiseId = "";
			String title = dataSet.get("title");

			addContactGeneric(driver, contactType, cFname, cLname, primaryCMethod, assignTo, corpUser.getuserFullName(),
					regionName, franchiseId, title, emailId, config);

			fc.utobj().printTestStep("Associate Account");
			fc.utobj().clickElement(driver, pobj.accountInfoTab);
			fc.utobj().sendKeys(driver, pobj.accountNameinaccountinfopage, accountName);
			fc.utobj().sleep(2000);
			JavascriptExecutor js = (JavascriptExecutor)driver;
			WebElement button =driver.findElement(By.xpath(".//*[@id='customizedAjaxSearch']/div"));
			js.executeScript("arguments[0].click();", button);
			
			/*fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//*[@id='customizedAjaxSearch']/div"));*/
			fc.utobj().clickElement(driver, pobj.associateBtn);

			fc.utobj().printTestStep("Verify Associate Account With Contact");
			boolean isAccountNameIsPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () , 'Account Name')]/ancestor::tr/td[.='" + accountName + "']");
			if (isAccountNameIsPresent == false) {
				fc.utobj().throwsException("was not able to verify Account associate");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "crm", "TC_213_Add_Opportunity" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Add Opportunity At CRM > Contact > Opportunities", testCaseId = "TC_213_Add_Opportunity")
	public void addOpportunity1() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			CRMAccountsPage pobj = new CRMAccountsPage(driver);
			fc.utobj().printTestStep("Navigate To Admin Users Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			CRMAccountsPageTest accountPage = new CRMAccountsPageTest();
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corpTest = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			String emailId = "crmautomation@staffex.com";
			CorporateUser corpUser = new CorporateUser();
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corpUser.setEmail(emailId);
			corpUser = corpTest.createDefaultUser(driver, corpUser);
			fc.utobj().printTestStep("Navigating to Admin > CRM > ContactType Configuration");
			fc.utobj().printTestStep("Add Contact Type");
			AdminCRMContactTypeConfigurationPageTest contactTypePage = new AdminCRMContactTypeConfigurationPageTest();
			String contactType = fc.utobj().generateTestData(dataSet.get("contactType"));
			contactTypePage.addContactType(driver, contactType);
			fc.utobj().printTestStep("Navigating to Admin > CRM >  Configure Opportunity Stages");
			fc.utobj().printTestStep("Add Stage");
			AdminCRMConfigureOpportunityStagesPageTest stagePage = new AdminCRMConfigureOpportunityStagesPageTest();
			String stage = fc.utobj().generateTestData(dataSet.get("stage"));
			stagePage.addStage(driver, stage);

			AdminCRMConfigureIndustryPageTest industryPage = new AdminCRMConfigureIndustryPageTest();
			String industry = fc.utobj().generateTestData(dataSet.get("industry"));
			String description = fc.utobj().generateTestData(dataSet.get("description"));
			fc.utobj().printTestStep("Navigating to Admin > CRM >  Configure Industry Type");
			fc.utobj().printTestStep("Add Industry");
			industryPage.addIndustry(driver, industry, description);

			String accountName = fc.utobj().generateTestData(dataSet.get("accountName"));
			String visibility = dataSet.get("visibility");
			fc.utobj().printTestStep("Navigating to CRM >  Accounts > Accounts Summary");
			fc.utobj().printTestStep("Add Account");
			accountPage.addAcocunt(driver, industry, accountName, visibility);

			fc.utobj().printTestStep("Navigate To CRM > Contacts > Contacts Summary Page");
			fc.utobj().printTestStep("Add Contact");
			fc.utobj().clickElement(driver, pobj.contactsLinks);
			fc.utobj().clickElement(driver, pobj.addNew);
			fc.utobj().clickElement(driver, pobj.contactLnk);

			String cFname = fc.utobj().generateTestData(dataSet.get("cFname"));
			String cLname = fc.utobj().generateTestData(dataSet.get("cLname"));
			String primaryCMethod = dataSet.get("primaryCMethod");
			String assignTo = "Corporate";
			String regionName = "";
			String franchiseId = "";
			String title = dataSet.get("title");

			addContactGenericWithAccount(driver, contactType, cFname, cLname, primaryCMethod, assignTo,
					corpUser.getuserFullName(), regionName, franchiseId, title, emailId, config, accountName);

			fc.utobj().printTestStep("Add Opportunity");
			fc.utobj().clickElement(driver, pobj.opportunitiesTab);
			fc.utobj().clickElement(driver, pobj.opportunityLink);

			String opportunityName = fc.utobj().generateTestData(dataSet.get("opportunityName"));
			String salesAmount = dataSet.get("salesAmount");

			fc.utobj().sendKeys(driver, pobj.opportunityName, opportunityName);
			fc.utobj().sendKeys(driver, pobj.opportunityOwner, corpUser.getuserFullName());
			fc.utobj().clickElement(driver, pobj.searchOppOwnerBtn);

			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//a[contains(text () , '" + corpUser.getuserFullName() + "')]"));
			fc.utobj().switchFrameToDefault(driver);
			fc.utobj().selectDropDown(driver, pobj.stage, stage);
			fc.utobj().sendKeys(driver, pobj.salesAmount, salesAmount);
			fc.utobj().clickElement(driver, pobj.saveBtn);

			fc.utobj().clickElement(driver, pobj.contactsLinks);
			contactsFilter(driver, contactType);

			fc.utobj().printTestStep("Verify Opportunity");
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//a[contains(text () , '" + cFname + " " + cLname + "')]"));
			fc.utobj().clickElement(driver, pobj.opportunitiesTab);

			fc.utobj().webDriverWait(driver, ".//a[.='" + opportunityName + "']");

			boolean isOpportunityPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[.='" + opportunityName + "']");
			if (isOpportunityPresent == false) {
				fc.utobj().throwsException("was not able to verify Opportunity Name");
			}
			boolean isAssignToPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[.='" + opportunityName
					+ "']/ancestor::tr/td[contains(text () , '" + corpUser.getuserFullName() + "')]");
			if (isAssignToPresent == false) {
				fc.utobj().throwsException("was not able to verify Assign To");
			}
			boolean isAccountNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[.='" + opportunityName + "']/ancestor::tr/td/a[contains(text () , '" + accountName + "')]");
			if (isAccountNamePresent == false) {
				fc.utobj().throwsException("was not able to verify Account");
			}
			boolean isContactNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[.='" + opportunityName
					+ "']/ancestor::tr/td/a[contains(text () , '" + cFname + " " + cLname + "')]");
			if (isContactNamePresent == false) {
				fc.utobj().throwsException("was not able to verify Contact");
			}
			boolean isStagePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[.='" + opportunityName + "']/ancestor::tr/td[contains(text () , '" + stage + "')]");
			if (isStagePresent == false) {
				fc.utobj().throwsException("was not able to verify Stage");
			}
			boolean isSalesAmountPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[.='" + opportunityName
					+ "']/ancestor::tr/td[contains(text () , '" + salesAmount.concat(".00") + "')]");
			if (isSalesAmountPresent == false) {
				fc.utobj().throwsException("was not able to verify Sales Amount");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "crm")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Modify opportunity At CRM > Contact > Opportunities", testCaseId = "TC_214_Modify_Opportunity")
	public void modifyOpportunityActionImage() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());
		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			CRMAccountsPage pobj = new CRMAccountsPage(driver);
			fc.utobj().printTestStep("Navigate To Admin Users Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			CRMAccountsPageTest accountPage = new CRMAccountsPageTest();
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corpTest = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			String emailId = "crmautomation@staffex.com";
			CorporateUser corpUser = new CorporateUser();
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corpUser.setEmail(emailId);
			corpUser = corpTest.createDefaultUser(driver, corpUser);
			fc.utobj().printTestStep("Navigating to Admin > CRM > ContactType Configuration");
			fc.utobj().printTestStep("Add Contact Type");
			AdminCRMContactTypeConfigurationPageTest contactTypePage = new AdminCRMContactTypeConfigurationPageTest();
			String contactType = fc.utobj().generateTestData(dataSet.get("contactType"));
			contactTypePage.addContactType(driver, contactType);
			fc.utobj().printTestStep("Navigating to Admin > CRM >  Configure Opportunity Stages");
			fc.utobj().printTestStep("Add Stage");
			AdminCRMConfigureOpportunityStagesPageTest stagePage = new AdminCRMConfigureOpportunityStagesPageTest();
			String stage = fc.utobj().generateTestData(dataSet.get("stage"));
			stagePage.addStage(driver, stage);

			/*
			 * fc.utobj().printTestStep(testCaseId,
			 * "Navigating to Admin > CRM > Configure Lead Type");
			 * fc.utobj().printTestStep(testCaseId, "Add Lead Type");
			 * AdminCRMConfigureLeadTypePageTest leadTypePage=new
			 * AdminCRMConfigureLeadTypePageTest(); String
			 * leadType=fc.utobj().generateTestData(dataSet.get("leadType"));
			 * leadTypePage.addLeadType(driver, leadType, config);
			 * fc.utobj().printTestStep(testCaseId,
			 * "Navigating to Admin > CRM > Configure Medium");
			 * fc.utobj().printTestStep(testCaseId, "Add Medium");
			 * AdminCRMConfigureMediumPageTest mediumPage=new
			 * AdminCRMConfigureMediumPageTest(); String
			 * medium=fc.utobj().generateTestData(dataSet.get("medium")); String
			 * description=fc.utobj().generateTestData(dataSet.get("description"
			 * )); mediumPage.addMedium(driver, medium, description, config);
			 * 
			 * AdminCRMSourceSummaryPageTest addSourcePage = new
			 * AdminCRMSourceSummaryPageTest(); String source =
			 * fc.utobj().generateTestData(dataSet.get("source")); description =
			 * fc.utobj().generateTestData(dataSet.get("description")); String
			 * displayOnWebPageText =
			 * fc.utobj().generateTestData(dataSet.get("text"));
			 * fc.utobj().printTestStep(testCaseId,
			 * "Navigating to Admin > CRM > Source Summary");
			 * fc.utobj().printTestStep(testCaseId, "Add Source");
			 * addSourcePage.addSource(driver, source, description,
			 * displayOnWebPageText, config);
			 * fc.utobj().printTestStep(testCaseId, "Add Source Details");
			 * String sourceDetails =
			 * fc.utobj().generateTestData(dataSet.get("sourceDetails"));
			 * addSourcePage.addSourceDetails(driver, source, sourceDetails,
			 * displayOnWebPageText, description, config);
			 */

			AdminCRMConfigureIndustryPageTest industryPage = new AdminCRMConfigureIndustryPageTest();
			String industry = fc.utobj().generateTestData(dataSet.get("industry"));
			String description = fc.utobj().generateTestData(dataSet.get("description"));
			fc.utobj().printTestStep("Navigating to Admin > CRM >  Configure Industry Type");
			fc.utobj().printTestStep("Add Industry");
			industryPage.addIndustry(driver, industry, description);

			String accountName = fc.utobj().generateTestData(dataSet.get("accountName"));
			String visibility = dataSet.get("visibility");
			fc.utobj().printTestStep("Navigating to CRM >  Accounts > Accounts Summary");
			fc.utobj().printTestStep("Add Account");
			accountPage.addAcocunt(driver, industry, accountName, visibility);

			fc.utobj().printTestStep("Navigate To CRM > Contacts > Contacts Summary Page");
			fc.utobj().printTestStep("Add Contact");
			fc.utobj().clickElement(driver, pobj.contactsLinks);
			fc.utobj().clickElement(driver, pobj.addNew);
			fc.utobj().clickElement(driver, pobj.contactLnk);

			String cFname = fc.utobj().generateTestData(dataSet.get("cFname"));
			String cLname = fc.utobj().generateTestData(dataSet.get("cLname"));
			String primaryCMethod = dataSet.get("primaryCMethod");
			String assignTo = "Corporate";
			String regionName = "";
			String franchiseId = "";
			String title = dataSet.get("title");

			addContactGenericWithAccount(driver, contactType, cFname, cLname, primaryCMethod, assignTo,
					corpUser.getuserFullName(), regionName, franchiseId, title, emailId, config, accountName);

			fc.utobj().printTestStep("Add Opportunity");
			fc.utobj().clickElement(driver, pobj.opportunitiesTab);
			fc.utobj().clickElement(driver, pobj.opportunityLink);

			String opportunityName = fc.utobj().generateTestData(dataSet.get("opportunityName"));
			String salesAmount = dataSet.get("salesAmount");

			fc.utobj().sendKeys(driver, pobj.opportunityName, opportunityName);
			fc.utobj().sendKeys(driver, pobj.opportunityOwner, corpUser.getuserFullName());
			fc.utobj().clickElement(driver, pobj.searchOppOwnerBtn);

			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//a[contains(text () , '" + corpUser.getuserFullName() + "')]"));
			fc.utobj().switchFrameToDefault(driver);
			fc.utobj().selectDropDown(driver, pobj.stage, stage);
			fc.utobj().sendKeys(driver, pobj.salesAmount, salesAmount);
			fc.utobj().clickElement(driver, pobj.saveBtn);

			fc.utobj().clickElement(driver, pobj.contactsLinks);
			contactsFilter(driver, contactType);
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//a[contains(text () , '" + cFname + " " + cLname + "')]"));
			fc.utobj().clickElement(driver, pobj.opportunitiesTab);

			// modify Opportunity
			fc.utobj().printTestStep("Modify Opportunity");
			fc.utobj().actionImgOption(driver, opportunityName, "Modify");
			opportunityName = fc.utobj().generateTestData(dataSet.get("opportunityName"));
			fc.utobj().sendKeys(driver, pobj.opportunityName, opportunityName);
			fc.utobj().clickElement(driver, pobj.saveBtn);

			fc.utobj().clickElement(driver, pobj.contactsLinks);
			contactsFilter(driver, contactType);
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//a[contains(text () , '" + cFname + " " + cLname + "')]"));
			fc.utobj().clickElement(driver, pobj.opportunitiesTab);

			fc.utobj().printTestStep("Verify The Modify Opportunity");
			boolean isOpportunityPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[.='" + opportunityName + "']");
			if (isOpportunityPresent == false) {
				fc.utobj().throwsException("was not able to verify Opportunity Name");
			}
			boolean isAssignToPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[.='" + opportunityName
					+ "']/ancestor::tr/td[contains(text () , '" + corpUser.getuserFullName() + "')]");
			if (isAssignToPresent == false) {
				fc.utobj().throwsException("was not able to verify Assign To");
			}
			boolean isAccountNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[.='" + opportunityName + "']/ancestor::tr/td/a[contains(text () , '" + accountName + "')]");
			if (isAccountNamePresent == false) {
				fc.utobj().throwsException("was not able to verify Account");
			}
			boolean isContactNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[.='" + opportunityName
					+ "']/ancestor::tr/td/a[contains(text () , '" + cFname + " " + cLname + "')]");
			if (isContactNamePresent == false) {
				fc.utobj().throwsException("was not able to verify Contact");
			}
			boolean isStagePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[.='" + opportunityName + "']/ancestor::tr/td[contains(text () , '" + stage + "')]");
			if (isStagePresent == false) {
				fc.utobj().throwsException("was not able to verify Stage");
			}
			boolean isSalesAmountPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[.='" + opportunityName
					+ "']/ancestor::tr/td[contains(text () , '" + salesAmount.concat(".00") + "')]");
			if (isSalesAmountPresent == false) {
				fc.utobj().throwsException("was not able to verify Sales Amount");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "crm" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2017-08-10", testCaseDescription = "Verify the Add Transaction At CRM > Contact > Opportunities", testCaseId = "TC_215_Add_Transaction")
	public void addTransactionActionImage1() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			CRMAccountsPage pobj = new CRMAccountsPage(driver);

			String description = fc.utobj().generateTestData(dataSet.get("description"));
			AdminCRMManageProductServiceCategoryPageTest serviceProductPage = new AdminCRMManageProductServiceCategoryPageTest();
			String categoryName = fc.utobj().generateTestData(dataSet.get("categoryName"));
			String productName = fc.utobj().generateTestData(dataSet.get("productName"));
			String oneLineDescription = fc.utobj().generateTestData(dataSet.get("oneLineDescription"));
			String rate = dataSet.get("rate");
			fc.utobj().printTestStep("Navigating to Admin > CRM >  Manage Product / Service & Category");
			fc.utobj().printTestStep("Add Category");
			serviceProductPage.addCategory(driver, categoryName, description);
			fc.utobj().printTestStep("Add Product/Service");
			serviceProductPage.addProductService(driver, categoryName, productName, oneLineDescription, rate);
			fc.utobj().printTestStep("Navigate To Admin Users Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			CRMAccountsPageTest accountPage = new CRMAccountsPageTest();
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corpTest = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			String emailId = "crmautomation@staffex.com";
			CorporateUser corpUser = new CorporateUser();
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corpUser.setEmail(emailId);
			corpUser = corpTest.createDefaultUser(driver, corpUser);
			fc.utobj().printTestStep("Navigating to Admin > CRM > ContactType Configuration");
			fc.utobj().printTestStep("Add Contact Type");
			AdminCRMContactTypeConfigurationPageTest contactTypePage = new AdminCRMContactTypeConfigurationPageTest();
			String contactType = fc.utobj().generateTestData(dataSet.get("contactType"));
			contactTypePage.addContactType(driver, contactType);
			fc.utobj().printTestStep("Navigating to Admin > CRM >  Configure Opportunity Stages");
			fc.utobj().printTestStep("Add Stage");
			AdminCRMConfigureOpportunityStagesPageTest stagePage = new AdminCRMConfigureOpportunityStagesPageTest();
			String stage = fc.utobj().generateTestData(dataSet.get("stage"));
			stagePage.addStage(driver, stage);

			AdminCRMConfigureIndustryPageTest industryPage = new AdminCRMConfigureIndustryPageTest();
			String industry = fc.utobj().generateTestData(dataSet.get("industry"));
			description = fc.utobj().generateTestData(dataSet.get("description"));
			fc.utobj().printTestStep("Navigating to Admin > CRM >  Configure Industry Type");
			fc.utobj().printTestStep("Add Industry");
			industryPage.addIndustry(driver, industry, description);

			String accountName = fc.utobj().generateTestData(dataSet.get("accountName"));
			String visibility = dataSet.get("visibility");
			fc.utobj().printTestStep("Navigating to CRM >  Accounts > Accounts Summary");
			fc.utobj().printTestStep("Add Account");
			accountPage.addAcocunt(driver, industry, accountName, visibility);

			fc.utobj().printTestStep("Navigate To CRM > Contacts > Contacts Summary Page");
			fc.utobj().printTestStep("Add Contact");
			fc.utobj().clickElement(driver, pobj.contactsLinks);
			fc.utobj().clickElement(driver, pobj.addNew);
			fc.utobj().clickElement(driver, pobj.contactLnk);

			String cFname = fc.utobj().generateTestData(dataSet.get("cFname"));
			String cLname = fc.utobj().generateTestData(dataSet.get("cLname"));
			String primaryCMethod = dataSet.get("primaryCMethod");
			String assignTo = "Corporate";
			String regionName = "";
			String franchiseId = "";
			String title = dataSet.get("title");

			addContactGenericWithAccount(driver, contactType, cFname, cLname, primaryCMethod, assignTo,
					corpUser.getuserFullName(), regionName, franchiseId, title, emailId, config, accountName);

			fc.utobj().printTestStep("Add Opportunity");
			fc.utobj().clickElement(driver, pobj.opportunitiesTab);
			fc.utobj().clickElement(driver, pobj.opportunityLink);

			String opportunityName = fc.utobj().generateTestData(dataSet.get("opportunityName"));
			String salesAmount = dataSet.get("salesAmount");

			fc.utobj().sendKeys(driver, pobj.opportunityName, opportunityName);
			fc.utobj().sendKeys(driver, pobj.opportunityOwner, corpUser.getuserFullName());
			fc.utobj().clickElement(driver, pobj.searchOppOwnerBtn);

			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//a[contains(text () , '" + corpUser.getuserFullName() + "')]"));
			fc.utobj().switchFrameToDefault(driver);
			fc.utobj().selectDropDown(driver, pobj.stage, stage);
			fc.utobj().sendKeys(driver, pobj.salesAmount, salesAmount);
			fc.utobj().clickElement(driver, pobj.saveBtn);

			fc.utobj().clickElement(driver, pobj.contactsLinks);
			contactsFilter(driver, contactType);
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//a[contains(text () , '" + cFname + " " + cLname + "')]"));
			fc.utobj().clickElement(driver, pobj.opportunitiesTab);

			// Add Transaction
			fc.utobj().printTestStep("Add Transaction");
			fc.utobj().actionImgOption(driver, opportunityName, "Add Transaction");
			fc.utobj().sendKeys(driver, pobj.invoiceDate, fc.utobj().getFutureDateUSFormat(-1));
			fc.utobj().clickElement(driver, pobj.selectProductAndService);
			fc.utobj().clickElement(driver, pobj.uncheckAll);
			fc.utobj().sendKeys(driver, pobj.searchProduct, productName);
			fc.utobj().clickElement(driver, pobj.checkAll);
			fc.utobj().clickElement(driver, pobj.okayBtn);
			fc.utobj().clickElement(driver, pobj.submitBtn);

			fc.utobj().clickElement(driver, pobj.contactsLinks);
			contactsFilter(driver, contactType);
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//a[contains(text () , '" + cFname + " " + cLname + "')]"));
			fc.utobj().clickElement(driver, pobj.transactionTab);

			fc.utobj().printTestStep("Verify Transaction");
			boolean isOppPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () , '" + opportunityName + "')]");
			if (isOppPresent == false) {
				fc.utobj().throwsException("was not able to verify Transaction");
			}

			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//td[contains(text () , '" + opportunityName + "')]/preceding-sibling::td/a"));

			boolean isProductAvailable = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () ,'" + productName + "')]");
			if (isProductAvailable == false) {
				fc.utobj().throwsException("was not able to verify Product");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = {"crm"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Log A Task At CRM > Contacts > Opportunities", testCaseId = "TC_216_Log_A_Task")
	public void logATaskActionImageOpp() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			CRMAccountsPage pobj = new CRMAccountsPage(driver);

			CRMAccountsPageTest accountPage = new CRMAccountsPageTest();
			fc.utobj().printTestStep("Navigate To Admin Users Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			String emailId = "crmautomation@staffex.com";
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUser = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporateUser.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);
			fc.utobj().printTestStep("Navigating to Admin > CRM > ContactType Configuration");
			fc.utobj().printTestStep("Add Contact Type");
			AdminCRMContactTypeConfigurationPageTest contactTypePage = new AdminCRMContactTypeConfigurationPageTest();
			String contactType = fc.utobj().generateTestData(dataSet.get("contactType"));
			contactTypePage.addContactType(driver, contactType);
			fc.utobj().printTestStep("Navigating to Admin > CRM >  Configure Opportunity Stages");
			fc.utobj().printTestStep("Add Stage");
			AdminCRMConfigureOpportunityStagesPageTest stagePage = new AdminCRMConfigureOpportunityStagesPageTest();
			String stage = fc.utobj().generateTestData(dataSet.get("stage"));
			stagePage.addStage(driver, stage);

			/*
			 * fc.utobj().printTestStep(testCaseId,
			 * "Navigating to Admin > CRM > Configure Lead Type");
			 * fc.utobj().printTestStep(testCaseId, "Add Lead Type");
			 * AdminCRMConfigureLeadTypePageTest leadTypePage=new
			 * AdminCRMConfigureLeadTypePageTest(); String
			 * leadType=fc.utobj().generateTestData(dataSet.get("leadType"));
			 * leadTypePage.addLeadType(driver, leadType, config);
			 * fc.utobj().printTestStep(testCaseId,
			 * "Navigating to Admin > CRM > Configure Medium");
			 * fc.utobj().printTestStep(testCaseId, "Add Medium");
			 * AdminCRMConfigureMediumPageTest mediumPage=new
			 * AdminCRMConfigureMediumPageTest(); String
			 * medium=fc.utobj().generateTestData(dataSet.get("medium")); String
			 * description=fc.utobj().generateTestData(dataSet.get("description"
			 * )); mediumPage.addMedium(driver, medium, description, config);
			 * 
			 * AdminCRMSourceSummaryPageTest addSourcePage = new
			 * AdminCRMSourceSummaryPageTest(); String source =
			 * fc.utobj().generateTestData(dataSet.get("source")); description =
			 * fc.utobj().generateTestData(dataSet.get("description")); String
			 * displayOnWebPageText =
			 * fc.utobj().generateTestData(dataSet.get("text"));
			 * fc.utobj().printTestStep(testCaseId,
			 * "Navigating to Admin > CRM > Source Summary");
			 * fc.utobj().printTestStep(testCaseId, "Add Source");
			 * addSourcePage.addSource(driver, source, description,
			 * displayOnWebPageText, config);
			 * fc.utobj().printTestStep(testCaseId, "Add Source Details");
			 * String sourceDetails =
			 * fc.utobj().generateTestData(dataSet.get("sourceDetails"));
			 * addSourcePage.addSourceDetails(driver, source, sourceDetails,
			 * displayOnWebPageText, description, config);
			 */

			AdminCRMConfigureIndustryPageTest industryPage = new AdminCRMConfigureIndustryPageTest();
			String industry = fc.utobj().generateTestData(dataSet.get("industry"));
			String description = fc.utobj().generateTestData(dataSet.get("description"));
			fc.utobj().printTestStep("Navigating to Admin > CRM >  Configure Industry Type");
			fc.utobj().printTestStep("Add Industry");
			industryPage.addIndustry(driver, industry, description);

			String accountName = fc.utobj().generateTestData(dataSet.get("accountName"));
			String visibility = dataSet.get("visibility");
			fc.utobj().printTestStep("Navigating to CRM >  Accounts > Accounts Summary");
			fc.utobj().printTestStep("Add Account");
			accountPage.addAcocunt(driver, industry, accountName, visibility);

			fc.utobj().printTestStep("Navigate To CRM > Contacts > Contacts Summary Page");
			fc.utobj().printTestStep("Add Contact");
			fc.utobj().clickElement(driver, pobj.contactsLinks);
			fc.utobj().clickElement(driver, pobj.addNew);
			fc.utobj().clickElement(driver, pobj.contactLnk);

			String cFname = fc.utobj().generateTestData(dataSet.get("cFname"));
			String cLname = fc.utobj().generateTestData(dataSet.get("cLname"));
			String primaryCMethod = dataSet.get("primaryCMethod");
			String assignTo = "Corporate";
			String regionName = "";
			String franchiseId = "";
			String title = dataSet.get("title");

			addContactGenericWithAccount(driver, contactType, cFname, cLname, primaryCMethod, assignTo,
					corpUser.getuserFullName(), regionName, franchiseId, title, emailId, config, accountName);

			fc.utobj().printTestStep("Add Opportunity");
			fc.utobj().clickElement(driver, pobj.opportunitiesTab);
			fc.utobj().clickElement(driver, pobj.opportunityLink);

			String opportunityName = fc.utobj().generateTestData(dataSet.get("opportunityName"));
			String salesAmount = dataSet.get("salesAmount");

			fc.utobj().sendKeys(driver, pobj.opportunityName, opportunityName);
			fc.utobj().sendKeys(driver, pobj.opportunityOwner, corpUser.getuserFullName());
			fc.utobj().clickElement(driver, pobj.searchOppOwnerBtn);

			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//a[contains(text () , '" + corpUser.getuserFullName() + "')]"));
			fc.utobj().switchFrameToDefault(driver);
			fc.utobj().selectDropDown(driver, pobj.stage, stage);
			fc.utobj().sendKeys(driver, pobj.salesAmount, salesAmount);
			fc.utobj().clickElement(driver, pobj.saveBtn);

			fc.utobj().clickElement(driver, pobj.contactsLinks);
			contactsFilter(driver, contactType);
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//a[contains(text () , '" + cFname + " " + cLname + "')]"));
			fc.utobj().clickElement(driver, pobj.opportunitiesTab);

			// Log A Task
			fc.utobj().printTestStep("Log A Task");
			fc.utobj().actionImgOption(driver, opportunityName, "Log a Task");
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			if (!fc.utobj().isSelected(driver,pobj.radioOwner)) {
				fc.utobj().clickElement(driver, pobj.radioOwner);
			}
			String statusTask = dataSet.get("statusTask");
			fc.utobj().selectDropDown(driver, pobj.statusTask, statusTask);
			String subject = fc.utobj().generateTestData(dataSet.get("subject"));
			fc.utobj().sendKeys(driver, pobj.subject, subject);
			if (!fc.utobj().isSelected(driver, pobj.timeLessTask)) {
				fc.utobj().clickElement(driver, pobj.timeLessTask);
			}
			String priority = dataSet.get("priority");
			fc.utobj().selectDropDown(driver, pobj.priority, priority);
			fc.utobj().sendKeys(driver, pobj.comments, "Test Comment");
			fc.utobj().clickElement(driver, pobj.createBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//a[.='" + opportunityName + "']"));

			fc.utobj().printTestStep("Verify Task");
			boolean isSubjectPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[contains(text () ,'" + subject + "')]");
			if (isSubjectPresent == false) {
				fc.utobj().throwsException("was not able to verify subject");
			}
			boolean isUserPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[contains(text () ,'" + subject
					+ "')]/ancestor::tr/td/div[.='" + corpUser.getuserFullName() + "']");
			if (isUserPresent == false) {
				fc.utobj().throwsException("was not able to verify userName at Lead Info Page");
			}
			boolean isStatusOfTaskPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[contains(text () ,'" + subject
					+ "')]/ancestor::tr/td[contains(text () ,'" + statusTask + "')]");
			if (isStatusOfTaskPresent == false) {
				fc.utobj().throwsException("was not able to verify status of task");
			}
			boolean isPriorityPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[contains(text () ,'" + subject
					+ "')]/ancestor::tr/td[contains(text () ,'" + priority + "')]");
			if (isPriorityPresent == false) {
				fc.utobj().throwsException("was not able to verify Priority of task");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "crm")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify the Change Stage By Action Image Options At CRM > Contacts > Opportunities", testCaseId = "TC_217_Change_Stage")
	public void changeStageActionImage() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			CRMAccountsPage pobj = new CRMAccountsPage(driver);
			CRMAccountsPageTest accountPage = new CRMAccountsPageTest();

			CRMContactsPageTest contactsPage = new CRMContactsPageTest();
			fc.utobj().printTestStep("Navigate To Admin Users Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			String emailId = "crmautomation@staffex.com";
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUser = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporateUser.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);
			fc.utobj().printTestStep("Navigating to Admin > CRM > ContactType Configuration");
			fc.utobj().printTestStep("Add Contact Type");
			AdminCRMContactTypeConfigurationPageTest contactTypePage = new AdminCRMContactTypeConfigurationPageTest();
			String contactType = fc.utobj().generateTestData(dataSet.get("contactType"));
			contactTypePage.addContactType(driver, contactType);
			fc.utobj().printTestStep("Navigating to Admin > CRM >  Configure Opportunity Stages");
			fc.utobj().printTestStep("Add Stage");
			AdminCRMConfigureOpportunityStagesPageTest stagePage = new AdminCRMConfigureOpportunityStagesPageTest();
			String stage = fc.utobj().generateTestData(dataSet.get("stage"));
			stagePage.addStage(driver, stage);

			String stage1 = fc.utobj().generateTestData(dataSet.get("stage"));
			stagePage.addStage(driver, stage1);

			/*
			 * String stage1=fc.utobj().generateTestData(dataSet.get("stage"));
			 * stagePage.addStage(driver, stage1, config);
			 * fc.utobj().printTestStep(testCaseId,
			 * "Navigating to Admin > CRM > Configure Medium");
			 * fc.utobj().printTestStep(testCaseId, "Add Medium");
			 * AdminCRMConfigureMediumPageTest mediumPage=new
			 * AdminCRMConfigureMediumPageTest(); String
			 * medium=fc.utobj().generateTestData(dataSet.get("medium")); String
			 * description=fc.utobj().generateTestData(dataSet.get("description"
			 * )); mediumPage.addMedium(driver, medium, description, config);
			 * 
			 * AdminCRMSourceSummaryPageTest addSourcePage = new
			 * AdminCRMSourceSummaryPageTest(); String source =
			 * fc.utobj().generateTestData(dataSet.get("source")); description =
			 * fc.utobj().generateTestData(dataSet.get("description")); String
			 * displayOnWebPageText =
			 * fc.utobj().generateTestData(dataSet.get("text"));
			 * fc.utobj().printTestStep(testCaseId,
			 * "Navigating to Admin > CRM > Source Summary");
			 * fc.utobj().printTestStep(testCaseId, "Add Source");
			 * addSourcePage.addSource(driver, source, description,
			 * displayOnWebPageText, config);
			 * fc.utobj().printTestStep(testCaseId, "Add Source Details");
			 * String sourceDetails =
			 * fc.utobj().generateTestData(dataSet.get("sourceDetails"));
			 * addSourcePage.addSourceDetails(driver, source, sourceDetails,
			 * displayOnWebPageText, description, config);
			 */

			AdminCRMConfigureIndustryPageTest industryPage = new AdminCRMConfigureIndustryPageTest();
			String industry = fc.utobj().generateTestData(dataSet.get("industry"));
			String description = fc.utobj().generateTestData(dataSet.get("description"));
			fc.utobj().printTestStep("Navigating to Admin > CRM >  Configure Industry Type");
			fc.utobj().printTestStep("Add Industry");
			industryPage.addIndustry(driver, industry, description);

			String accountName = fc.utobj().generateTestData(dataSet.get("accountName"));
			String visibility = dataSet.get("visibility");
			fc.utobj().printTestStep("Navigating to CRM >  Accounts > Accounts Summary");
			fc.utobj().printTestStep("Add Account");
			accountPage.addAcocunt(driver, industry, accountName, visibility);

			fc.utobj().printTestStep("Navigating to CRM >  Contacts > Contacts Summary");
			fc.utobj().printTestStep("Add Contact");

			fc.utobj().clickElement(driver, pobj.contactsLinks);
			fc.utobj().clickElement(driver, pobj.addNew);
			fc.utobj().clickElement(driver, pobj.contactLnk);

			String cFname = fc.utobj().generateTestData(dataSet.get("cFname"));
			String cLname = fc.utobj().generateTestData(dataSet.get("cLname"));
			String primaryCMethod = dataSet.get("primaryCMethod");
			String assignTo = "Corporate";
			String regionName = "";
			String franchiseId = "";
			String title = dataSet.get("title");

			contactsPage.addContactGenericWithAccount(driver, contactType, cFname, cLname, primaryCMethod, assignTo,
					corpUser.getuserFullName(), regionName, franchiseId, title, emailId, config, accountName);

			fc.utobj().clickElement(driver, pobj.opportunitiesTab);
			fc.utobj().clickElement(driver, pobj.opportunityLink);

			String opportunityName = fc.utobj().generateTestData(dataSet.get("opportunityName"));
			String salesAmount = dataSet.get("salesAmount");

			fc.utobj().printTestStep("Add Opportunity");
			fc.utobj().sendKeys(driver, pobj.opportunityName, opportunityName);
			fc.utobj().sendKeys(driver, pobj.opportunityOwner, corpUser.getuserFullName());
			fc.utobj().clickElement(driver, pobj.searchOppOwnerBtn);

			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//a[contains(text () , '" + corpUser.getuserFullName() + "')]"));
			fc.utobj().switchFrameToDefault(driver);
			fc.utobj().selectDropDown(driver, pobj.stage, stage);
			fc.utobj().sendKeys(driver, pobj.salesAmount, salesAmount);
			fc.utobj().clickElement(driver, pobj.saveBtn);

			fc.utobj().clickElement(driver, pobj.contactsLinks);
			contactsFilter(driver, contactType);
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//a[contains(text () , '" + cFname + " " + cLname + "')]"));
			fc.utobj().clickElement(driver, pobj.opportunitiesTab);

			// Change Stages
			fc.utobj().printTestStep("Change Stage");
			fc.utobj().actionImgOption(driver, opportunityName, "Change Stage");
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().selectDropDown(driver, pobj.stage, stage1);
			fc.utobj().clickElement(driver, pobj.changeBtn);
			fc.utobj().clickElement(driver, pobj.closeBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Verify Change Stage");
			boolean isOpportunityPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[.='" + opportunityName + "']");
			if (isOpportunityPresent == false) {
				fc.utobj().throwsException("was not able to verify Opportunity Name");
			}
			boolean isAssignToPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[.='" + opportunityName
					+ "']/ancestor::tr/td[contains(text () , '" + corpUser.getuserFullName() + "')]");
			if (isAssignToPresent == false) {
				fc.utobj().throwsException("was not able to verify Assign To");
			}
			boolean isAccountNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[.='" + opportunityName + "']/ancestor::tr/td/a[contains(text () , '" + accountName + "')]");
			if (isAccountNamePresent == false) {
				fc.utobj().throwsException("was not able to verify Account");
			}
			boolean isContactNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[.='" + opportunityName
					+ "']/ancestor::tr/td/a[contains(text () , '" + cFname + " " + cLname + "')]");
			if (isContactNamePresent == false) {
				fc.utobj().throwsException("was not able to verify Contact");
			}
			boolean isStagePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[.='" + opportunityName + "']/ancestor::tr/td[contains(text () ,'" + stage1 + "')]");
			if (isStagePresent == false) {
				fc.utobj().throwsException("was not able to verify Stage");
			}
			boolean isSalesAmountPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[.='" + opportunityName
					+ "']/ancestor::tr/td[contains(text () , '" + salesAmount.concat(".00") + "')]");
			if (isSalesAmountPresent == false) {
				fc.utobj().throwsException("was not able to verify Sales Amount");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "crm" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Change Stage By Action Btn At CRM > Contacts > Opportunities", testCaseId = "TC_218_Change_Stage")
	public void changeStageActionBtn() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			CRMAccountsPage pobj = new CRMAccountsPage(driver);
			CRMAccountsPageTest accountPage = new CRMAccountsPageTest();

			CRMContactsPageTest contactsPage = new CRMContactsPageTest();
			fc.utobj().printTestStep("Navigate To Admin Users Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			String emailId = "crmautomation@staffex.com";
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUser = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporateUser.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);
			fc.utobj().printTestStep("Navigating to Admin > CRM > ContactType Configuration");
			fc.utobj().printTestStep("Add Contact Type");
			AdminCRMContactTypeConfigurationPageTest contactTypePage = new AdminCRMContactTypeConfigurationPageTest();
			String contactType = fc.utobj().generateTestData(dataSet.get("contactType"));
			contactTypePage.addContactType(driver, contactType);
			fc.utobj().printTestStep("Navigating to Admin > CRM >  Configure Opportunity Stages");
			fc.utobj().printTestStep("Add Stage");
			AdminCRMConfigureOpportunityStagesPageTest stagePage = new AdminCRMConfigureOpportunityStagesPageTest();
			String stage = fc.utobj().generateTestData(dataSet.get("stage"));
			stagePage.addStage(driver, stage);

			String stage1 = fc.utobj().generateTestData(dataSet.get("stage"));
			stagePage.addStage(driver, stage1);

			AdminCRMConfigureIndustryPageTest industryPage = new AdminCRMConfigureIndustryPageTest();
			String industry = fc.utobj().generateTestData(dataSet.get("industry"));
			String description = fc.utobj().generateTestData(dataSet.get("description"));
			fc.utobj().printTestStep("Navigating to Admin > CRM >  Configure Industry Type");
			fc.utobj().printTestStep("Add Industry");
			industryPage.addIndustry(driver, industry, description);

			String accountName = fc.utobj().generateTestData(dataSet.get("accountName"));
			String visibility = dataSet.get("visibility");
			fc.utobj().printTestStep("Navigating to CRM >  Accounts > Accounts Summary");
			fc.utobj().printTestStep("Add Account");
			accountPage.addAcocunt(driver, industry, accountName, visibility);

			fc.utobj().printTestStep("Navigating to CRM >  Contacts > Contacts Summary");
			fc.utobj().printTestStep("Add Contact");

			fc.utobj().clickElement(driver, pobj.contactsLinks);
			fc.utobj().clickElement(driver, pobj.addNew);
			fc.utobj().clickElement(driver, pobj.contactLnk);

			String cFname = fc.utobj().generateTestData(dataSet.get("cFname"));
			String cLname = fc.utobj().generateTestData(dataSet.get("cLname"));
			String primaryCMethod = dataSet.get("primaryCMethod");
			String assignTo = "Corporate";
			String regionName = "";
			String franchiseId = "";
			String title = dataSet.get("title");

			contactsPage.addContactGenericWithAccount(driver, contactType, cFname, cLname, primaryCMethod, assignTo,
					corpUser.getuserFullName(), regionName, franchiseId, title, emailId, config, accountName);

			fc.utobj().clickElement(driver, pobj.opportunitiesTab);
			fc.utobj().clickElement(driver, pobj.opportunityLink);

			fc.utobj().printTestStep("Add Opportunity");
			String opportunityName = fc.utobj().generateTestData(dataSet.get("opportunityName"));
			String salesAmount = dataSet.get("salesAmount");

			fc.utobj().sendKeys(driver, pobj.opportunityName, opportunityName);
			fc.utobj().sendKeys(driver, pobj.opportunityOwner, corpUser.getuserFullName());
			fc.utobj().clickElement(driver, pobj.searchOppOwnerBtn);

			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//a[contains(text () , '" + corpUser.getuserFullName() + "')]"));
			fc.utobj().switchFrameToDefault(driver);
			fc.utobj().selectDropDown(driver, pobj.stage, stage);
			fc.utobj().sendKeys(driver, pobj.salesAmount, salesAmount);
			fc.utobj().clickElement(driver, pobj.saveBtn);

			fc.utobj().clickElement(driver, pobj.contactsLinks);
			contactsFilter(driver, contactType);
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//a[contains(text () , '" + cFname + " " + cLname + "')]"));
			fc.utobj().clickElement(driver, pobj.opportunitiesTab);

			// Change Stages
			fc.utobj().printTestStep("Change Stage");
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//a[.='" + opportunityName + "']/ancestor::tr/td/input"));
			fc.utobj().selectActionMenuItemsWithTagA(driver, "Change Stage");
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().selectDropDown(driver, pobj.stage, stage1);
			fc.utobj().clickElement(driver, pobj.changeBtn);
			fc.utobj().clickElement(driver, pobj.closeBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Verify Change Stage");
			boolean isOpportunityPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[.='" + opportunityName + "']");
			if (isOpportunityPresent == false) {
				fc.utobj().throwsException("was not able to verify Opportunity Name");
			}
			boolean isAssignToPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[.='" + opportunityName
					+ "']/ancestor::tr/td[contains(text () , '" + corpUser.getuserFullName() + "')]");
			if (isAssignToPresent == false) {
				fc.utobj().throwsException("was not able to verify Assign To");
			}
			boolean isAccountNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[.='" + opportunityName + "']/ancestor::tr/td/a[contains(text () , '" + accountName + "')]");
			if (isAccountNamePresent == false) {
				fc.utobj().throwsException("was not able to verify Account");
			}
			boolean isContactNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[.='" + opportunityName
					+ "']/ancestor::tr/td/a[contains(text () , '" + cFname + " " + cLname + "')]");
			if (isContactNamePresent == false) {
				fc.utobj().throwsException("was not able to verify Contact");
			}
			boolean isStagePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[.='" + opportunityName + "']/ancestor::tr/td[contains(text () ,'" + stage1 + "')]");
			if (isStagePresent == false) {
				fc.utobj().throwsException("was not able to verify Stage");
			}
			boolean isSalesAmountPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[.='" + opportunityName
					+ "']/ancestor::tr/td[contains(text () , '" + salesAmount.concat(".00") + "')]");
			if (isSalesAmountPresent == false) {
				fc.utobj().throwsException("was not able to verify Sales Amount");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = {"crm"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify the Delete Opportunity By Action Btn Options At CRM > Contacts > Opportunities", testCaseId = "TC_219_Delete_Opportunity")
	public void deleteOpportunityActionBtn() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			CRMAccountsPage pobj = new CRMAccountsPage(driver);
			fc.utobj().printTestStep("Navigate To Admin Users Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			CRMAccountsPageTest accountPage = new CRMAccountsPageTest();
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corpTest = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			String emailId = "crmautomation@staffex.com";
			CorporateUser corpUser = new CorporateUser();
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corpUser.setEmail(emailId);
			corpUser = corpTest.createDefaultUser(driver, corpUser);
			fc.utobj().printTestStep("Navigating to Admin > CRM > ContactType Configuration");
			fc.utobj().printTestStep("Add Contact Type");
			AdminCRMContactTypeConfigurationPageTest contactTypePage = new AdminCRMContactTypeConfigurationPageTest();
			String contactType = fc.utobj().generateTestData(dataSet.get("contactType"));
			contactTypePage.addContactType(driver, contactType);
			fc.utobj().printTestStep("Navigating to Admin > CRM >  Configure Opportunity Stages");
			fc.utobj().printTestStep("Add Stage");
			AdminCRMConfigureOpportunityStagesPageTest stagePage = new AdminCRMConfigureOpportunityStagesPageTest();
			String stage = fc.utobj().generateTestData(dataSet.get("stage"));
			stagePage.addStage(driver, stage);

			/*
			 * fc.utobj().printTestStep(testCaseId,
			 * "Navigating to Admin > CRM > Configure Lead Type");
			 * fc.utobj().printTestStep(testCaseId, "Add Lead Type");
			 * AdminCRMConfigureLeadTypePageTest leadTypePage=new
			 * AdminCRMConfigureLeadTypePageTest(); String
			 * leadType=fc.utobj().generateTestData(dataSet.get("leadType"));
			 * leadTypePage.addLeadType(driver, leadType, config);
			 * fc.utobj().printTestStep(testCaseId,
			 * "Navigating to Admin > CRM > Configure Medium");
			 * fc.utobj().printTestStep(testCaseId, "Add Medium");
			 * AdminCRMConfigureMediumPageTest mediumPage=new
			 * AdminCRMConfigureMediumPageTest(); String
			 * medium=fc.utobj().generateTestData(dataSet.get("medium")); String
			 * description=fc.utobj().generateTestData(dataSet.get("description"
			 * )); mediumPage.addMedium(driver, medium, description, config);
			 * 
			 * AdminCRMSourceSummaryPageTest addSourcePage = new
			 * AdminCRMSourceSummaryPageTest(); String source =
			 * fc.utobj().generateTestData(dataSet.get("source")); description =
			 * fc.utobj().generateTestData(dataSet.get("description")); String
			 * displayOnWebPageText =
			 * fc.utobj().generateTestData(dataSet.get("text"));
			 * fc.utobj().printTestStep(testCaseId,
			 * "Navigating to Admin > CRM > Source Summary");
			 * fc.utobj().printTestStep(testCaseId, "Add Source");
			 * addSourcePage.addSource(driver, source, description,
			 * displayOnWebPageText, config);
			 * fc.utobj().printTestStep(testCaseId, "Add Source Details");
			 * String sourceDetails =
			 * fc.utobj().generateTestData(dataSet.get("sourceDetails"));
			 * addSourcePage.addSourceDetails(driver, source, sourceDetails,
			 * displayOnWebPageText, description, config);
			 */

			AdminCRMConfigureIndustryPageTest industryPage = new AdminCRMConfigureIndustryPageTest();
			String industry = fc.utobj().generateTestData(dataSet.get("industry"));
			String description = fc.utobj().generateTestData(dataSet.get("description"));
			fc.utobj().printTestStep("Navigating to Admin > CRM >  Configure Industry Type");
			fc.utobj().printTestStep("Add Industry");
			industryPage.addIndustry(driver, industry, description);

			String accountName = fc.utobj().generateTestData(dataSet.get("accountName"));
			String visibility = dataSet.get("visibility");
			fc.utobj().printTestStep("Navigating to CRM >  Accounts > Accounts Summary");
			fc.utobj().printTestStep("Add Account");
			accountPage.addAcocunt(driver, industry, accountName, visibility);

			fc.utobj().printTestStep("Navigate To CRM > Contacts > Contacts Summary Page");
			fc.utobj().printTestStep("Add Contact");
			fc.utobj().clickElement(driver, pobj.contactsLinks);
			fc.utobj().clickElement(driver, pobj.addNew);
			fc.utobj().clickElement(driver, pobj.contactLnk);

			String cFname = fc.utobj().generateTestData(dataSet.get("cFname"));
			String cLname = fc.utobj().generateTestData(dataSet.get("cLname"));
			String primaryCMethod = dataSet.get("primaryCMethod");
			String assignTo = "Corporate";
			String regionName = "";
			String franchiseId = "";
			String title = dataSet.get("title");

			addContactGenericWithAccount(driver, contactType, cFname, cLname, primaryCMethod, assignTo,
					corpUser.getuserFullName(), regionName, franchiseId, title, emailId, config, accountName);

			fc.utobj().printTestStep("Add Opportunity");
			fc.utobj().clickElement(driver, pobj.opportunitiesTab);
			fc.utobj().clickElement(driver, pobj.opportunityLink);

			String opportunityName = fc.utobj().generateTestData(dataSet.get("opportunityName"));
			String salesAmount = dataSet.get("salesAmount");

			fc.utobj().sendKeys(driver, pobj.opportunityName, opportunityName);
			fc.utobj().sendKeys(driver, pobj.opportunityOwner, corpUser.getuserFullName());
			fc.utobj().clickElement(driver, pobj.searchOppOwnerBtn);

			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//a[contains(text () , '" + corpUser.getuserFullName() + "')]"));
			fc.utobj().switchFrameToDefault(driver);
			fc.utobj().selectDropDown(driver, pobj.stage, stage);
			fc.utobj().sendKeys(driver, pobj.salesAmount, salesAmount);
			fc.utobj().clickElement(driver, pobj.saveBtn);

			fc.utobj().clickElement(driver, pobj.contactsLinks);
			contactsFilter(driver, contactType);
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//a[contains(text () , '" + cFname + " " + cLname + "')]"));
			fc.utobj().clickElement(driver, pobj.opportunitiesTab);

			// Delete Opportunity
			fc.utobj().printTestStep("Delete Opportunity");
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//a[.='" + opportunityName + "']/ancestor::tr/td/input"));
			fc.utobj().selectActionMenuItemsWithTagA(driver, "Delete");
			fc.utobj().acceptAlertBox(driver);

			fc.utobj().printTestStep("Verify Delete Opportunity");
			boolean isNoRecordPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//td[.='No records found.']");
			if (isNoRecordPresent == false) {
				fc.utobj().throwsException("was not able to Delete Opportunity");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "crm")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Change Stage of opportunity By Action Bottom Button Options At CRM > Contacts > Opportunities", testCaseId = "TC_220_Change_Stage")
	public void changeStageBottomBtn() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			CRMAccountsPage pobj = new CRMAccountsPage(driver);
			CRMAccountsPageTest accountPage = new CRMAccountsPageTest();
			fc.utobj().printTestStep("Navigate To Admin Users Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			String emailId = "crmautomation@staffex.com";
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUser = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporateUser.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);
			fc.utobj().printTestStep("Navigating to Admin > CRM > ContactType Configuration");
			fc.utobj().printTestStep("Add Contact Type");
			AdminCRMContactTypeConfigurationPageTest contactTypePage = new AdminCRMContactTypeConfigurationPageTest();
			String contactType = fc.utobj().generateTestData(dataSet.get("contactType"));
			contactTypePage.addContactType(driver, contactType);
			fc.utobj().printTestStep("Navigating to Admin > CRM >  Configure Opportunity Stages");
			fc.utobj().printTestStep("Add Stage");
			AdminCRMConfigureOpportunityStagesPageTest stagePage = new AdminCRMConfigureOpportunityStagesPageTest();
			String stage = fc.utobj().generateTestData(dataSet.get("stage"));
			stagePage.addStage(driver, stage);

			String stage1 = fc.utobj().generateTestData(dataSet.get("stage"));
			stagePage.addStage(driver, stage1);

			/*
			 * fc.utobj().printTestStep(testCaseId,
			 * "Navigating to Admin > CRM > Configure Medium");
			 * fc.utobj().printTestStep(testCaseId, "Add Medium");
			 * AdminCRMConfigureMediumPageTest mediumPage=new
			 * AdminCRMConfigureMediumPageTest(); String
			 * medium=fc.utobj().generateTestData(dataSet.get("medium")); String
			 * description=fc.utobj().generateTestData(dataSet.get("description"
			 * )); mediumPage.addMedium(driver, medium, description, config);
			 * 
			 * AdminCRMSourceSummaryPageTest addSourcePage = new
			 * AdminCRMSourceSummaryPageTest(); String source =
			 * fc.utobj().generateTestData(dataSet.get("source")); description =
			 * fc.utobj().generateTestData(dataSet.get("description")); String
			 * displayOnWebPageText =
			 * fc.utobj().generateTestData(dataSet.get("text"));
			 * fc.utobj().printTestStep(testCaseId,
			 * "Navigating to Admin > CRM > Source Summary");
			 * fc.utobj().printTestStep(testCaseId, "Add Source");
			 * addSourcePage.addSource(driver, source, description,
			 * displayOnWebPageText, config);
			 * fc.utobj().printTestStep(testCaseId, "Add Source Details");
			 * String sourceDetails =
			 * fc.utobj().generateTestData(dataSet.get("sourceDetails"));
			 * addSourcePage.addSourceDetails(driver, source, sourceDetails,
			 * displayOnWebPageText, description, config);
			 */

			AdminCRMConfigureIndustryPageTest industryPage = new AdminCRMConfigureIndustryPageTest();
			String industry = fc.utobj().generateTestData(dataSet.get("industry"));
			String description = fc.utobj().generateTestData(dataSet.get("description"));
			fc.utobj().printTestStep("Navigating to Admin > CRM >  Configure Industry Type");
			fc.utobj().printTestStep("Add Industry");
			industryPage.addIndustry(driver, industry, description);

			String accountName = fc.utobj().generateTestData(dataSet.get("accountName"));
			String visibility = dataSet.get("visibility");
			fc.utobj().printTestStep("Navigating to CRM >  Accounts > Accounts Summary");
			fc.utobj().printTestStep("Add Account");
			accountPage.addAcocunt(driver, industry, accountName, visibility);

			fc.utobj().printTestStep("Navigate To CRM > Contacts > Contacts Summary Page");
			fc.utobj().printTestStep("Add Contact");
			fc.utobj().clickElement(driver, pobj.contactsLinks);
			fc.utobj().clickElement(driver, pobj.addNew);
			fc.utobj().clickElement(driver, pobj.contactLnk);

			String cFname = fc.utobj().generateTestData(dataSet.get("cFname"));
			String cLname = fc.utobj().generateTestData(dataSet.get("cLname"));
			String primaryCMethod = dataSet.get("primaryCMethod");
			String assignTo = "Corporate";
			String regionName = "";
			String franchiseId = "";
			String title = dataSet.get("title");

			addContactGenericWithAccount(driver, contactType, cFname, cLname, primaryCMethod, assignTo,
					corpUser.getuserFullName(), regionName, franchiseId, title, emailId, config, accountName);

			fc.utobj().printTestStep("Add Opportunity");
			fc.utobj().clickElement(driver, pobj.opportunitiesTab);
			fc.utobj().clickElement(driver, pobj.opportunityLink);

			String opportunityName = fc.utobj().generateTestData(dataSet.get("opportunityName"));
			String salesAmount = dataSet.get("salesAmount");

			fc.utobj().sendKeys(driver, pobj.opportunityName, opportunityName);
			fc.utobj().sendKeys(driver, pobj.opportunityOwner, corpUser.getuserFullName());
			fc.utobj().clickElement(driver, pobj.searchOppOwnerBtn);

			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//a[contains(text () , '" + corpUser.getuserFullName() + "')]"));
			fc.utobj().switchFrameToDefault(driver);
			fc.utobj().selectDropDown(driver, pobj.stage, stage);
			fc.utobj().sendKeys(driver, pobj.salesAmount, salesAmount);
			fc.utobj().clickElement(driver, pobj.saveBtn);

			fc.utobj().clickElement(driver, pobj.contactsLinks);
			contactsFilter(driver, contactType);
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//a[contains(text () , '" + cFname + " " + cLname + "')]"));
			fc.utobj().clickElement(driver, pobj.opportunitiesTab);

			// Change Stages
			fc.utobj().printTestStep("Change Stage");
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//a[.='" + opportunityName + "']/ancestor::tr/td/input"));
			fc.utobj().clickElement(driver, pobj.changeStatusBottmBtn);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().selectDropDown(driver, pobj.stage, stage1);
			fc.utobj().clickElement(driver, pobj.changeBtn);
			fc.utobj().clickElement(driver, pobj.closeBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Verify Chage Stage");
			boolean isOpportunityPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[.='" + opportunityName + "']");
			if (isOpportunityPresent == false) {
				fc.utobj().throwsException("was not able to verify Opportunity Name");
			}
			boolean isAssignToPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[.='" + opportunityName
					+ "']/ancestor::tr/td[contains(text () , '" + corpUser.getuserFullName() + "')]");
			if (isAssignToPresent == false) {
				fc.utobj().throwsException("was not able to verify Assign To");
			}
			boolean isAccountNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[.='" + opportunityName + "']/ancestor::tr/td/a[contains(text () , '" + accountName + "')]");
			if (isAccountNamePresent == false) {
				fc.utobj().throwsException("was not able to verify Account");
			}
			boolean isContactNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[.='" + opportunityName
					+ "']/ancestor::tr/td/a[contains(text () , '" + cFname + " " + cLname + "')]");
			if (isContactNamePresent == false) {
				fc.utobj().throwsException("was not able to verify Contact");
			}
			boolean isStagePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[.='" + opportunityName + "']/ancestor::tr/td[contains(text () ,'" + stage1 + "')]");
			if (isStagePresent == false) {
				fc.utobj().throwsException("was not able to verify Stage");
			}
			boolean isSalesAmountPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[.='" + opportunityName
					+ "']/ancestor::tr/td[contains(text () , '" + salesAmount.concat(".00") + "')]");
			if (isSalesAmountPresent == false) {
				fc.utobj().throwsException("was not able to verify Sales Amount");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = {"crm"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Delete Opportunity By Action Bottom Btn Options At CRM > Contacts > Opportunities", testCaseId = "TC_221_Delete_Opportunity")
	public void deleteOpportunityBottomBtn() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			CRMAccountsPage pobj = new CRMAccountsPage(driver);
			CRMAccountsPageTest accountPage = new CRMAccountsPageTest();

			fc.utobj().printTestStep("Navigate To Admin Users Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			String emailId = "crmautomation@staffex.com";
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUser = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporateUser.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);
			fc.utobj().printTestStep("Navigating to Admin > CRM > ContactType Configuration");
			fc.utobj().printTestStep("Add Contact Type");
			AdminCRMContactTypeConfigurationPageTest contactTypePage = new AdminCRMContactTypeConfigurationPageTest();
			String contactType = fc.utobj().generateTestData(dataSet.get("contactType"));
			contactTypePage.addContactType(driver, contactType);
			fc.utobj().printTestStep("Navigating to Admin > CRM >  Configure Opportunity Stages");
			fc.utobj().printTestStep("Add Stage");
			AdminCRMConfigureOpportunityStagesPageTest stagePage = new AdminCRMConfigureOpportunityStagesPageTest();
			String stage = fc.utobj().generateTestData(dataSet.get("stage"));
			stagePage.addStage(driver, stage);

			/*
			 * fc.utobj().printTestStep(testCaseId,
			 * "Navigating to Admin > CRM > Configure Lead Type");
			 * fc.utobj().printTestStep(testCaseId, "Add Lead Type");
			 * AdminCRMConfigureLeadTypePageTest leadTypePage=new
			 * AdminCRMConfigureLeadTypePageTest(); String
			 * leadType=fc.utobj().generateTestData(dataSet.get("leadType"));
			 * leadTypePage.addLeadType(driver, leadType, config);
			 * fc.utobj().printTestStep(testCaseId,
			 * "Navigating to Admin > CRM > Configure Medium");
			 * fc.utobj().printTestStep(testCaseId, "Add Medium");
			 * AdminCRMConfigureMediumPageTest mediumPage=new
			 * AdminCRMConfigureMediumPageTest(); String
			 * medium=fc.utobj().generateTestData(dataSet.get("medium")); String
			 * description=fc.utobj().generateTestData(dataSet.get("description"
			 * )); mediumPage.addMedium(driver, medium, description, config);
			 * 
			 * AdminCRMSourceSummaryPageTest addSourcePage = new
			 * AdminCRMSourceSummaryPageTest(); String source =
			 * fc.utobj().generateTestData(dataSet.get("source")); description =
			 * fc.utobj().generateTestData(dataSet.get("description")); String
			 * displayOnWebPageText =
			 * fc.utobj().generateTestData(dataSet.get("text"));
			 * fc.utobj().printTestStep(testCaseId,
			 * "Navigating to Admin > CRM > Source Summary");
			 * fc.utobj().printTestStep(testCaseId, "Add Source");
			 * addSourcePage.addSource(driver, source, description,
			 * displayOnWebPageText, config);
			 * fc.utobj().printTestStep(testCaseId, "Add Source Details");
			 * String sourceDetails =
			 * fc.utobj().generateTestData(dataSet.get("sourceDetails"));
			 * addSourcePage.addSourceDetails(driver, source, sourceDetails,
			 * displayOnWebPageText, description, config);
			 */

			AdminCRMConfigureIndustryPageTest industryPage = new AdminCRMConfigureIndustryPageTest();
			String industry = fc.utobj().generateTestData(dataSet.get("industry"));
			String description = fc.utobj().generateTestData(dataSet.get("description"));
			fc.utobj().printTestStep("Navigating to Admin > CRM >  Configure Industry Type");
			fc.utobj().printTestStep("Add Industry");
			industryPage.addIndustry(driver, industry, description);

			String accountName = fc.utobj().generateTestData(dataSet.get("accountName"));
			String visibility = dataSet.get("visibility");
			fc.utobj().printTestStep("Navigating to CRM >  Accounts > Accounts Summary");
			fc.utobj().printTestStep("Add Account");
			accountPage.addAcocunt(driver, industry, accountName, visibility);

			fc.utobj().printTestStep("Navigate To CRM > Contacts > Contacts Summary Page");
			fc.utobj().printTestStep("Add Contact");
			fc.utobj().clickElement(driver, pobj.contactsLinks);
			fc.utobj().clickElement(driver, pobj.addNew);
			fc.utobj().clickElement(driver, pobj.contactLnk);

			String cFname = fc.utobj().generateTestData(dataSet.get("cFname"));
			String cLname = fc.utobj().generateTestData(dataSet.get("cLname"));
			String primaryCMethod = dataSet.get("primaryCMethod");
			String assignTo = "Corporate";
			String regionName = "";
			String franchiseId = "";
			String title = dataSet.get("title");

			addContactGenericWithAccount(driver, contactType, cFname, cLname, primaryCMethod, assignTo,
					corpUser.getuserFullName(), regionName, franchiseId, title, emailId, config, accountName);

			fc.utobj().printTestStep("Add Opportunity");
			fc.utobj().clickElement(driver, pobj.opportunitiesTab);
			fc.utobj().clickElement(driver, pobj.opportunityLink);

			String opportunityName = fc.utobj().generateTestData(dataSet.get("opportunityName"));
			String salesAmount = dataSet.get("salesAmount");

			fc.utobj().sendKeys(driver, pobj.opportunityName, opportunityName);
			fc.utobj().sendKeys(driver, pobj.opportunityOwner, corpUser.getuserFullName());
			fc.utobj().clickElement(driver, pobj.searchOppOwnerBtn);

			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//a[contains(text () , '" + corpUser.getuserFullName() + "')]"));
			fc.utobj().switchFrameToDefault(driver);
			fc.utobj().selectDropDown(driver, pobj.stage, stage);
			fc.utobj().sendKeys(driver, pobj.salesAmount, salesAmount);
			fc.utobj().clickElement(driver, pobj.saveBtn);

			fc.utobj().clickElement(driver, pobj.contactsLinks);
			contactsFilter(driver, contactType);
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//a[contains(text () , '" + cFname + " " + cLname + "')]"));
			fc.utobj().clickElement(driver, pobj.opportunitiesTab);

			// Delete Opportunity
			fc.utobj().printTestStep("Delete Opportunity");
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//a[.='" + opportunityName + "']/ancestor::tr/td/input"));
			fc.utobj().clickElement(driver, pobj.deleteBtmBtn);
			fc.utobj().acceptAlertBox(driver);

			boolean isNoRecordPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//td[.='No records found.']");
			if (isNoRecordPresent == false) {
				fc.utobj().throwsException("was not able to Delete Opportunity");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "crm", "crmemail" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Send Email At CRM > Contact > Emails", testCaseId = "TC_222_Send_Email")
	public void sendEmailTab1() throws IOException, Exception, ParserConfigurationException, SAXException {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			CRMContactsPage pobj = new CRMContactsPage(driver);

			fc.utobj().printTestStep("Navigate To Admin Users Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			String emailId = "crmautomation@staffex.com";
			//String emailId=fc.utobj().generateTestData("crmautomation")+"@staffex.com";
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUser = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporateUser.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			fc.utobj().printTestStep("Navigate To Admin > CRM > Contact Type Configuration");
			fc.utobj().printTestStep("Add Contact Type");
			AdminCRMContactTypeConfigurationPageTest contatcTypePage = new AdminCRMContactTypeConfigurationPageTest();
			String contactType = fc.utobj().generateTestData(dataSet.get("contactType"));
			contatcTypePage.addContactType(driver, contactType);

			fc.utobj().printTestStep("Navigate To CRM > Contacts > Contacts Summary Page");
			fc.utobj().printTestStep("Add Contact");
			fc.crm().crm_common().CRMContactsLnk(driver);
			fc.utobj().clickElement(driver, pobj.addNew);
			fc.utobj().clickElement(driver, pobj.contactLnk);

			String firstName = fc.utobj().generateTestData(dataSet.get("firstName"));
			String lastName = fc.utobj().generateTestData(dataSet.get("lastName"));
			String primaryCMethod = dataSet.get("primaryCMethod");
			String assignTo = "Corporate";
			String regionName = "";
			String franchiseId = "";
			String title = dataSet.get("title");

			addContactGeneric(driver, contactType, firstName, lastName, primaryCMethod, assignTo,
					corpUser.getuserFullName(), regionName, franchiseId, title, emailId, config);

			fc.utobj().clickElement(driver, pobj.contactsLinks);
			contactsFilter(driver, contactType);
			fc.utobj().printTestStep("Send Email");
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//a[contains(text () , '" + firstName + " " + lastName + "')]"));
			fc.utobj().clickElement(driver, pobj.emailsTab1);

			// send email
			fc.utobj().clickElement(driver, pobj.sendEmailAtEmails);

			if (!fc.utobj().isSelected(driver, pobj.loggedUserId)) {
				fc.utobj().clickElement(driver, pobj.loggedUserId);
			}
			String mailSentToId = fc.utobj().getText(driver, pobj.mailSentTo);
			if (!mailSentToId.equalsIgnoreCase(emailId)) {
				fc.utobj().throwsException("was not able to verify mailSentTo");
			}
			String mailCC = dataSet.get("mailCC");
			fc.utobj().sendKeys(driver, pobj.mailCC, mailCC);

			if (!fc.utobj().isSelected(driver,pobj.showBcc)) {
				fc.utobj().clickElement(driver, pobj.showBcc);
			}
			String mailBCC = dataSet.get("mailBCC");
			fc.utobj().sendKeys(driver, pobj.mailBCC, mailBCC);

			String subjectMail = fc.utobj().generateTestData(dataSet.get("subjectMail"));
			fc.utobj().sendKeys(driver, pobj.subjectMail, subjectMail);

			String editorText = fc.utobj().generateTestData(dataSet.get("mailText"));

			fc.utobj().switchFrameById(driver, "ta_ifr");

			Actions actions = new Actions(driver);
			actions.moveToElement(fc.utobj().getElement(driver, pobj.editorTextArea));
			actions.click();
			actions.sendKeys(editorText);
			actions.build().perform();
			fc.utobj().switchFrameToDefault(driver);
			fc.utobj().clickElement(driver, pobj.sendEmailBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Verify Send Email");
			String expectedSubject = subjectMail;
			String expectedMessageBody = editorText;
			Map<String, String> mailData = fc.utobj().readMailBox(expectedSubject, expectedMessageBody, emailId,
					"sdg@1a@Hfs");

			if (mailData.size() == 0 || !mailData.get("mailBody").contains(editorText)) {
				fc.utobj().throwsException("was not able to verify mail text");
			}
			if (mailData.size() == 0 || !mailData.get("subject").equalsIgnoreCase(subjectMail)) {
				fc.utobj().throwsException("was not able to verify subject mail");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "crm")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Add Document At CRM > Contact > Documents", testCaseId = "TC_223_Add_Document")
	public void addDocument() throws IOException, Exception, ParserConfigurationException, SAXException {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			CRMContactsPage pobj = new CRMContactsPage(driver);

			fc.utobj().printTestStep("Navigate To Admin Users Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			String emailId = "crmautomation@staffex.com";
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUser = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporateUser.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);
			fc.utobj().printTestStep("Navigate To Admin > CRM > Contact Type Configuration");
			fc.utobj().printTestStep("Add Contact Type");
			AdminCRMContactTypeConfigurationPageTest contatcTypePage = new AdminCRMContactTypeConfigurationPageTest();
			String contactType = fc.utobj().generateTestData(dataSet.get("contactType"));
			contatcTypePage.addContactType(driver, contactType);

			/*
			 * fc.utobj().printTestStep(testCaseId,
			 * "Navigating to Admin > CRM > Configure Lead Type");
			 * fc.utobj().printTestStep(testCaseId, "Add Lead Type");
			 * AdminCRMConfigureLeadTypePageTest leadTypePage = new
			 * AdminCRMConfigureLeadTypePageTest(); String leadType =
			 * fc.utobj().generateTestData(dataSet.get("leadType"));
			 * leadTypePage.addLeadType(driver, leadType, config);
			 * 
			 * AdminCRMSourceSummaryPageTest addSourcePage = new
			 * AdminCRMSourceSummaryPageTest(); String source =
			 * fc.utobj().generateTestData(dataSet.get("source")); String
			 * description =
			 * fc.utobj().generateTestData(dataSet.get("description")); String
			 * displayOnWebPageText =
			 * fc.utobj().generateTestData(dataSet.get("text"));
			 * fc.utobj().printTestStep(testCaseId,
			 * "Navigating to Admin > CRM > Source Summary");
			 * fc.utobj().printTestStep(testCaseId, "Add Source");
			 * addSourcePage.addSource(driver, source, description,
			 * displayOnWebPageText, config);
			 * fc.utobj().printTestStep(testCaseId, "Add Source Details");
			 * String sourceDetails =
			 * fc.utobj().generateTestData(dataSet.get("sourceDetails"));
			 * addSourcePage.addSourceDetails(driver, source, sourceDetails,
			 * displayOnWebPageText, description, config);
			 * fc.utobj().printTestStep(testCaseId,
			 * "Navigating to Admin > CRM > Configure Medium");
			 * fc.utobj().printTestStep(testCaseId, "Add Medium");
			 * AdminCRMConfigureMediumPageTest mediumPage = new
			 * AdminCRMConfigureMediumPageTest(); String medium =
			 * fc.utobj().generateTestData(dataSet.get("medium"));
			 * mediumPage.addMedium(driver, medium, description, config);
			 */

			fc.utobj().printTestStep("Navigate To CRM > Contacts > Contacts Summary Page");
			fc.utobj().printTestStep("Add Contact");
			fc.crm().crm_common().CRMContactsLnk(driver);
			fc.utobj().clickElement(driver, pobj.addNew);
			fc.utobj().clickElement(driver, pobj.contactLnk);

			String firstName = fc.utobj().generateTestData(dataSet.get("firstName"));
			String lastName = fc.utobj().generateTestData(dataSet.get("lastName"));
			String primaryCMethod = dataSet.get("primaryCMethod");
			String assignTo = "Corporate";
			String regionName = "";
			String franchiseId = "";
			String title = dataSet.get("title");

			addContactGeneric(driver, contactType, firstName, lastName, primaryCMethod, assignTo,
					corpUser.getuserFullName(), regionName, franchiseId, title, emailId, config);

			fc.utobj().clickElement(driver, pobj.contactsLinks);
			contactsFilter(driver, contactType);
			fc.utobj().printTestStep("Add Document");
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//a[contains(text () , '" + firstName + " " + lastName + "')]"));
			fc.utobj().clickElement(driver, pobj.documentsTab);

			String docTitle = fc.utobj().generateTestData(dataSet.get("docTitle"));
			fc.utobj().sendKeys(driver, pobj.documentTitle, docTitle);
			String fileName = fc.utobj().getFilePathFromTestData(dataSet.get("fileName"));
			fc.utobj().sendKeys(driver, pobj.cmDocumentAttachment, fileName);
			fc.utobj().clickElement(driver, pobj.saveBtn);

			fc.utobj().printTestStep("Verify The Add Document");
			boolean isTextPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//td[contains(text () , '" + docTitle + "')]");
			if (isTextPresent == false) {
				fc.utobj().throwsException("was not able to verify Document Title");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "crm")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Modify Document At CRM > Contact > Documents", testCaseId = "TC_224_Modify_Document")
	public void modifyDocument() throws IOException, Exception, ParserConfigurationException, SAXException {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			CRMContactsPage pobj = new CRMContactsPage(driver);

			fc.utobj().printTestStep("Navigate To Admin Users Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			String emailId = "crmautomation@staffex.com";
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUser = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporateUser.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);
			fc.utobj().printTestStep("Navigate To Admin > CRM > Contact Type Configuration");
			fc.utobj().printTestStep("Add Contact Type");
			AdminCRMContactTypeConfigurationPageTest contatcTypePage = new AdminCRMContactTypeConfigurationPageTest();
			String contactType = fc.utobj().generateTestData(dataSet.get("contactType"));
			contatcTypePage.addContactType(driver, contactType);

			/*
			 * fc.utobj().printTestStep(testCaseId,
			 * "Navigating to Admin > CRM > Configure Lead Type");
			 * fc.utobj().printTestStep(testCaseId, "Add Lead Type");
			 * AdminCRMConfigureLeadTypePageTest leadTypePage = new
			 * AdminCRMConfigureLeadTypePageTest(); String leadType =
			 * fc.utobj().generateTestData(dataSet.get("leadType"));
			 * leadTypePage.addLeadType(driver, leadType, config);
			 * 
			 * AdminCRMSourceSummaryPageTest addSourcePage = new
			 * AdminCRMSourceSummaryPageTest(); String source =
			 * fc.utobj().generateTestData(dataSet.get("source")); String
			 * description =
			 * fc.utobj().generateTestData(dataSet.get("description")); String
			 * displayOnWebPageText =
			 * fc.utobj().generateTestData(dataSet.get("text"));
			 * fc.utobj().printTestStep(testCaseId,
			 * "Navigating to Admin > CRM > Source Summary");
			 * fc.utobj().printTestStep(testCaseId, "Add Source");
			 * addSourcePage.addSource(driver, source, description,
			 * displayOnWebPageText, config);
			 * fc.utobj().printTestStep(testCaseId, "Add Source Details");
			 * String sourceDetails =
			 * fc.utobj().generateTestData(dataSet.get("sourceDetails"));
			 * addSourcePage.addSourceDetails(driver, source, sourceDetails,
			 * displayOnWebPageText, description, config);
			 * fc.utobj().printTestStep(testCaseId,
			 * "Navigating to Admin > CRM > Configure Medium");
			 * fc.utobj().printTestStep(testCaseId, "Add Medium");
			 * AdminCRMConfigureMediumPageTest mediumPage = new
			 * AdminCRMConfigureMediumPageTest(); String medium =
			 * fc.utobj().generateTestData(dataSet.get("medium"));
			 * mediumPage.addMedium(driver, medium, description, config);
			 */

			fc.utobj().printTestStep("Navigate To CRM > Contacts > Contacts Summary Page");
			fc.utobj().printTestStep("Add Contact");
			fc.crm().crm_common().CRMContactsLnk(driver);
			fc.utobj().clickElement(driver, pobj.addNew);
			fc.utobj().clickElement(driver, pobj.contactLnk);

			String firstName = fc.utobj().generateTestData(dataSet.get("firstName"));
			String lastName = fc.utobj().generateTestData(dataSet.get("lastName"));
			String primaryCMethod = dataSet.get("primaryCMethod");
			String assignTo = "Corporate";
			String regionName = "";
			String franchiseId = "";
			String title = dataSet.get("title");

			addContactGeneric(driver, contactType, firstName, lastName, primaryCMethod, assignTo,
					corpUser.getuserFullName(), regionName, franchiseId, title, emailId, config);

			fc.utobj().clickElement(driver, pobj.contactsLinks);
			contactsFilter(driver, contactType);
			fc.utobj().printTestStep("Add Document");
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//a[contains(text () , '" + firstName + " " + lastName + "')]"));
			fc.utobj().clickElement(driver, pobj.documentsTab);

			String docTitle = fc.utobj().generateTestData(dataSet.get("docTitle"));
			fc.utobj().sendKeys(driver, pobj.documentTitle, docTitle);
			String fileName = fc.utobj().getFilePathFromTestData(dataSet.get("fileName"));
			fc.utobj().sendKeys(driver, pobj.cmDocumentAttachment, fileName);
			fc.utobj().clickElement(driver, pobj.saveBtn);

			fc.utobj().printTestStep("Modify Document");
			fc.utobj().actionImgOption(driver, docTitle, "Modify");
			docTitle = fc.utobj().generateTestData(dataSet.get("docTitle"));
			fc.utobj().sendKeys(driver, pobj.documentTitle, docTitle);
			fc.utobj().clickElement(driver, pobj.saveBtn);

			fc.utobj().printTestStep("Verify The Modify Document");
			boolean isTextPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//td[contains(text () , '" + docTitle + "')]");
			if (isTextPresent == false) {
				fc.utobj().throwsException("was not able to verify Document Title");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = {"crm"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Delete Document At CRM > Contact > Documents", testCaseId = "TC_225_Delete_Document")
	public void deleteDocument() throws IOException, Exception, ParserConfigurationException, SAXException {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			CRMContactsPage pobj = new CRMContactsPage(driver);

			fc.utobj().printTestStep("Navigate To Admin Users Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			String emailId = "crmautomation@staffex.com";
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUser = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporateUser.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);
			fc.utobj().printTestStep("Navigate To Admin > CRM > Contact Type Configuration");
			fc.utobj().printTestStep("Add Contact Type");
			AdminCRMContactTypeConfigurationPageTest contatcTypePage = new AdminCRMContactTypeConfigurationPageTest();
			String contactType = fc.utobj().generateTestData(dataSet.get("contactType"));
			contatcTypePage.addContactType(driver, contactType);

			/*
			 * fc.utobj().printTestStep(testCaseId,
			 * "Navigating to Admin > CRM > Configure Lead Type");
			 * fc.utobj().printTestStep(testCaseId, "Add Lead Type");
			 * AdminCRMConfigureLeadTypePageTest leadTypePage = new
			 * AdminCRMConfigureLeadTypePageTest(); String leadType =
			 * fc.utobj().generateTestData(dataSet.get("leadType"));
			 * leadTypePage.addLeadType(driver, leadType, config);
			 * 
			 * AdminCRMSourceSummaryPageTest addSourcePage = new
			 * AdminCRMSourceSummaryPageTest(); String source =
			 * fc.utobj().generateTestData(dataSet.get("source")); String
			 * description =
			 * fc.utobj().generateTestData(dataSet.get("description")); String
			 * displayOnWebPageText =
			 * fc.utobj().generateTestData(dataSet.get("text"));
			 * fc.utobj().printTestStep(testCaseId,
			 * "Navigating to Admin > CRM > Source Summary");
			 * fc.utobj().printTestStep(testCaseId, "Add Source");
			 * addSourcePage.addSource(driver, source, description,
			 * displayOnWebPageText, config);
			 * fc.utobj().printTestStep(testCaseId, "Add Source Details");
			 * String sourceDetails =
			 * fc.utobj().generateTestData(dataSet.get("sourceDetails"));
			 * addSourcePage.addSourceDetails(driver, source, sourceDetails,
			 * displayOnWebPageText, description, config);
			 * fc.utobj().printTestStep(testCaseId,
			 * "Navigating to Admin > CRM > Configure Medium");
			 * fc.utobj().printTestStep(testCaseId, "Add Medium");
			 * AdminCRMConfigureMediumPageTest mediumPage = new
			 * AdminCRMConfigureMediumPageTest(); String medium =
			 * fc.utobj().generateTestData(dataSet.get("medium"));
			 * mediumPage.addMedium(driver, medium, description, config);
			 */

			fc.utobj().printTestStep("Navigate To CRM > Contacts > Contacts Summary Page");
			fc.utobj().printTestStep("Add Contact");
			fc.crm().crm_common().CRMContactsLnk(driver);
			fc.utobj().clickElement(driver, pobj.addNew);
			fc.utobj().clickElement(driver, pobj.contactLnk);

			String firstName = fc.utobj().generateTestData(dataSet.get("firstName"));
			String lastName = fc.utobj().generateTestData(dataSet.get("lastName"));
			String primaryCMethod = dataSet.get("primaryCMethod");
			String assignTo = "Corporate";
			String regionName = "";
			String franchiseId = "";
			String title = dataSet.get("title");

			addContactGeneric(driver, contactType, firstName, lastName, primaryCMethod, assignTo,
					corpUser.getuserFullName(), regionName, franchiseId, title, emailId, config);

			fc.utobj().clickElement(driver, pobj.contactsLinks);
			contactsFilter(driver, contactType);
			fc.utobj().printTestStep("Add Document");
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//a[contains(text () , '" + firstName + " " + lastName + "')]"));
			fc.utobj().clickElement(driver, pobj.documentsTab);

			String docTitle = fc.utobj().generateTestData(dataSet.get("docTitle"));
			fc.utobj().sendKeys(driver, pobj.documentTitle, docTitle);
			String fileName = fc.utobj().getFilePathFromTestData(dataSet.get("fileName"));
			fc.utobj().sendKeys(driver, pobj.cmDocumentAttachment, fileName);
			fc.utobj().clickElement(driver, pobj.saveBtn);

			fc.utobj().printTestStep("Delete Document");
			fc.utobj().actionImgOption(driver, docTitle, "Delete");
			fc.utobj().acceptAlertBox(driver);

			fc.utobj().printTestStep("Verify The Delete Document");
			boolean isTextPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//*[@id='cmDocumentTitle']");
			if (isTextPresent == false) {
				fc.utobj().throwsException("was not able to Delete Document");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "crm")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Add More Document At CRM > Contact > Documents", testCaseId = "TC_226_Add_More_Btn")
	public void addMoreBottmBtn() throws IOException, Exception, ParserConfigurationException, SAXException {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			CRMContactsPage pobj = new CRMContactsPage(driver);

			fc.utobj().printTestStep("Navigate To Admin Users Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			String emailId = "crmautomation@staffex.com";
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUser = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporateUser.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);
			fc.utobj().printTestStep("Navigate To Admin > CRM > Contact Type Configuration");
			fc.utobj().printTestStep("Add Contact Type");
			AdminCRMContactTypeConfigurationPageTest contatcTypePage = new AdminCRMContactTypeConfigurationPageTest();
			String contactType = fc.utobj().generateTestData(dataSet.get("contactType"));
			contatcTypePage.addContactType(driver, contactType);

			/*
			 * fc.utobj().printTestStep(testCaseId,
			 * "Navigating to Admin > CRM > Configure Lead Type");
			 * fc.utobj().printTestStep(testCaseId, "Add Lead Type");
			 * AdminCRMConfigureLeadTypePageTest leadTypePage = new
			 * AdminCRMConfigureLeadTypePageTest(); String leadType =
			 * fc.utobj().generateTestData(dataSet.get("leadType"));
			 * leadTypePage.addLeadType(driver, leadType, config);
			 * 
			 * AdminCRMSourceSummaryPageTest addSourcePage = new
			 * AdminCRMSourceSummaryPageTest(); String source =
			 * fc.utobj().generateTestData(dataSet.get("source")); String
			 * description =
			 * fc.utobj().generateTestData(dataSet.get("description")); String
			 * displayOnWebPageText =
			 * fc.utobj().generateTestData(dataSet.get("text"));
			 * fc.utobj().printTestStep(testCaseId,
			 * "Navigating to Admin > CRM > Source Summary");
			 * fc.utobj().printTestStep(testCaseId, "Add Source");
			 * addSourcePage.addSource(driver, source, description,
			 * displayOnWebPageText, config);
			 * fc.utobj().printTestStep(testCaseId, "Add Source Details");
			 * String sourceDetails =
			 * fc.utobj().generateTestData(dataSet.get("sourceDetails"));
			 * addSourcePage.addSourceDetails(driver, source, sourceDetails,
			 * displayOnWebPageText, description, config);
			 * fc.utobj().printTestStep(testCaseId,
			 * "Navigating to Admin > CRM > Configure Medium");
			 * fc.utobj().printTestStep(testCaseId, "Add Medium");
			 * AdminCRMConfigureMediumPageTest mediumPage = new
			 * AdminCRMConfigureMediumPageTest(); String medium =
			 * fc.utobj().generateTestData(dataSet.get("medium"));
			 * mediumPage.addMedium(driver, medium, description, config);
			 */

			fc.utobj().printTestStep("Navigate To CRM > Contacts > Contacts Summary Page");
			fc.utobj().printTestStep("Add Contact");
			fc.crm().crm_common().CRMContactsLnk(driver);
			fc.utobj().clickElement(driver, pobj.addNew);
			fc.utobj().clickElement(driver, pobj.contactLnk);

			String firstName = fc.utobj().generateTestData(dataSet.get("firstName"));
			String lastName = fc.utobj().generateTestData(dataSet.get("lastName"));
			String primaryCMethod = dataSet.get("primaryCMethod");
			String assignTo = "Corporate";
			String regionName = "";
			String franchiseId = "";
			String title = dataSet.get("title");

			addContactGeneric(driver, contactType, firstName, lastName, primaryCMethod, assignTo,
					corpUser.getuserFullName(), regionName, franchiseId, title, emailId, config);

			fc.utobj().clickElement(driver, pobj.contactsLinks);
			contactsFilter(driver, contactType);
			fc.utobj().printTestStep("Add Document");
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//a[contains(text () , '" + firstName + " " + lastName + "')]"));
			fc.utobj().clickElement(driver, pobj.documentsTab);

			String docTitle = fc.utobj().generateTestData(dataSet.get("docTitle"));
			fc.utobj().sendKeys(driver, pobj.documentTitle, docTitle);
			String fileName = fc.utobj().getFilePathFromTestData(dataSet.get("fileName"));
			fc.utobj().sendKeys(driver, pobj.cmDocumentAttachment, fileName);
			fc.utobj().clickElement(driver, pobj.saveBtn);

			fc.utobj().printTestStep("Add More Document");
			fc.utobj().clickElement(driver, pobj.addMOreBtn);
			String docTitle1 = fc.utobj().generateTestData(dataSet.get("docTitle"));
			fc.utobj().sendKeys(driver, pobj.documentTitle, docTitle1);
			fc.utobj().sendKeys(driver, pobj.cmDocumentAttachment, fileName);
			fc.utobj().clickElement(driver, pobj.saveBtn);

			fc.utobj().printTestStep("Verify The Add More Document");
			boolean isTextPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//td[contains(text () , '" + docTitle + "')]");
			if (isTextPresent == false) {
				fc.utobj().throwsException("was not able to verify Document Title 1");
			}

			boolean isTextPresent1 = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//td[contains(text () , '" + docTitle1 + "')]");
			if (isTextPresent1 == false) {
				fc.utobj().throwsException("was not able to verify Document Title 2");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "crm")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Add Other Address At CRM > Contact > Other Address", testCaseId = "TC_227_Add_Other_Address")
	public void addOtherAddress() throws IOException, Exception, ParserConfigurationException, SAXException {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			CRMContactsPage pobj = new CRMContactsPage(driver);

			fc.utobj().printTestStep("Navigate To Admin Users Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			String emailId = "crmautomation@staffex.com";
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUser = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporateUser.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);
			fc.utobj().printTestStep("Navigate To Admin > CRM > Contact Type Configuration");
			fc.utobj().printTestStep("Add Contact Type");
			AdminCRMContactTypeConfigurationPageTest contatcTypePage = new AdminCRMContactTypeConfigurationPageTest();
			String contactType = fc.utobj().generateTestData(dataSet.get("contactType"));
			contatcTypePage.addContactType(driver, contactType);

			/*
			 * fc.utobj().printTestStep(testCaseId,
			 * "Navigating to Admin > CRM > Configure Lead Type");
			 * fc.utobj().printTestStep(testCaseId, "Add Lead Type");
			 * AdminCRMConfigureLeadTypePageTest leadTypePage = new
			 * AdminCRMConfigureLeadTypePageTest(); String leadType =
			 * fc.utobj().generateTestData(dataSet.get("leadType"));
			 * leadTypePage.addLeadType(driver, leadType, config);
			 * 
			 * AdminCRMSourceSummaryPageTest addSourcePage = new
			 * AdminCRMSourceSummaryPageTest(); String source =
			 * fc.utobj().generateTestData(dataSet.get("source")); String
			 * description =
			 * fc.utobj().generateTestData(dataSet.get("description")); String
			 * displayOnWebPageText =
			 * fc.utobj().generateTestData(dataSet.get("text"));
			 * fc.utobj().printTestStep(testCaseId,
			 * "Navigating to Admin > CRM > Source Summary");
			 * fc.utobj().printTestStep(testCaseId, "Add Source");
			 * addSourcePage.addSource(driver, source, description,
			 * displayOnWebPageText, config);
			 * fc.utobj().printTestStep(testCaseId, "Add Source Details");
			 * String sourceDetails =
			 * fc.utobj().generateTestData(dataSet.get("sourceDetails"));
			 * addSourcePage.addSourceDetails(driver, source, sourceDetails,
			 * displayOnWebPageText, description, config);
			 * fc.utobj().printTestStep(testCaseId,
			 * "Navigating to Admin > CRM > Configure Medium");
			 * fc.utobj().printTestStep(testCaseId, "Add Medium");
			 * AdminCRMConfigureMediumPageTest mediumPage = new
			 * AdminCRMConfigureMediumPageTest(); String medium =
			 * fc.utobj().generateTestData(dataSet.get("medium"));
			 * mediumPage.addMedium(driver, medium, description, config);
			 */

			fc.utobj().printTestStep("Navigate To CRM > Contacts > Contacts Summary Page");
			fc.utobj().printTestStep("Add Contact");
			fc.crm().crm_common().CRMContactsLnk(driver);
			fc.utobj().clickElement(driver, pobj.addNew);
			fc.utobj().clickElement(driver, pobj.contactLnk);

			String firstName = fc.utobj().generateTestData(dataSet.get("firstName"));
			String lastName = fc.utobj().generateTestData(dataSet.get("lastName"));
			String primaryCMethod = dataSet.get("primaryCMethod");
			String assignTo = "Corporate";
			String regionName = "";
			String franchiseId = "";
			String title = dataSet.get("title");

			addContactGeneric(driver, contactType, firstName, lastName, primaryCMethod, assignTo,
					corpUser.getuserFullName(), regionName, franchiseId, title, emailId, config);

			fc.utobj().clickElement(driver, pobj.contactsLinks);
			contactsFilter(driver, contactType);
			fc.utobj().printTestStep("Add Other Address");
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//a[contains(text () , '" + firstName + " " + lastName + "')]"));
			fc.utobj().clickElement(driver, pobj.otherAddressTab);

			String titleAdd = fc.utobj().generateTestData(dataSet.get("titleAdd"));
			fc.utobj().sendKeys(driver, pobj.titleAddress, titleAdd);
			String address = fc.utobj().generateTestData(dataSet.get("address"));
			fc.utobj().sendKeys(driver, pobj.address, address);
			fc.utobj().clickElement(driver, pobj.saveBtn);

			fc.utobj().printTestStep("Verify The Add Other Address");
			boolean isAddressTitlePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//td/span[.='" + titleAdd + "']");
			if (isAddressTitlePresent == false) {
				fc.utobj().throwsException("was not able to verify add address");
			}

			boolean isAddressPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () , 'Address')]/ancestor::tr/td[.='" + address + "']");
			if (isAddressPresent == false) {
				fc.utobj().throwsException("was not able to verify address Name");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = {"crm"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify the Modify Other Address At CRM > Contact > Other Address", testCaseId = "TC_228_Modify_Address")
	public void modifyAddress() throws IOException, Exception, ParserConfigurationException, SAXException {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			CRMContactsPage pobj = new CRMContactsPage(driver);

			fc.utobj().printTestStep("Navigate To Admin Users Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			String emailId = "crmautomation@staffex.com";
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUser = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporateUser.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);
			fc.utobj().printTestStep("Navigate To Admin > CRM > Contact Type Configuration");
			fc.utobj().printTestStep("Add Contact Type");
			AdminCRMContactTypeConfigurationPageTest contatcTypePage = new AdminCRMContactTypeConfigurationPageTest();
			String contactType = fc.utobj().generateTestData(dataSet.get("contactType"));
			contatcTypePage.addContactType(driver, contactType);

			/*
			 * fc.utobj().printTestStep(testCaseId,
			 * "Navigating to Admin > CRM > Configure Lead Type");
			 * fc.utobj().printTestStep(testCaseId, "Add Lead Type");
			 * AdminCRMConfigureLeadTypePageTest leadTypePage = new
			 * AdminCRMConfigureLeadTypePageTest(); String leadType =
			 * fc.utobj().generateTestData(dataSet.get("leadType"));
			 * leadTypePage.addLeadType(driver, leadType, config);
			 * 
			 * AdminCRMSourceSummaryPageTest addSourcePage = new
			 * AdminCRMSourceSummaryPageTest(); String source =
			 * fc.utobj().generateTestData(dataSet.get("source")); String
			 * description =
			 * fc.utobj().generateTestData(dataSet.get("description")); String
			 * displayOnWebPageText =
			 * fc.utobj().generateTestData(dataSet.get("text"));
			 * fc.utobj().printTestStep(testCaseId,
			 * "Navigating to Admin > CRM > Source Summary");
			 * fc.utobj().printTestStep(testCaseId, "Add Source");
			 * addSourcePage.addSource(driver, source, description,
			 * displayOnWebPageText, config);
			 * fc.utobj().printTestStep(testCaseId, "Add Source Details");
			 * String sourceDetails =
			 * fc.utobj().generateTestData(dataSet.get("sourceDetails"));
			 * addSourcePage.addSourceDetails(driver, source, sourceDetails,
			 * displayOnWebPageText, description, config);
			 * fc.utobj().printTestStep(testCaseId,
			 * "Navigating to Admin > CRM > Configure Medium");
			 * fc.utobj().printTestStep(testCaseId, "Add Medium");
			 * AdminCRMConfigureMediumPageTest mediumPage = new
			 * AdminCRMConfigureMediumPageTest(); String medium =
			 * fc.utobj().generateTestData(dataSet.get("medium"));
			 * mediumPage.addMedium(driver, medium, description, config);
			 */

			fc.utobj().printTestStep("Navigate To CRM > Contacts > Contacts Summary Page");
			fc.utobj().printTestStep("Add Contact");
			fc.crm().crm_common().CRMContactsLnk(driver);
			fc.utobj().clickElement(driver, pobj.addNew);
			fc.utobj().clickElement(driver, pobj.contactLnk);

			String firstName = fc.utobj().generateTestData(dataSet.get("firstName"));
			String lastName = fc.utobj().generateTestData(dataSet.get("lastName"));
			String primaryCMethod = dataSet.get("primaryCMethod");
			String assignTo = "Corporate";
			String regionName = "";
			String franchiseId = "";
			String title = dataSet.get("title");

			addContactGeneric(driver, contactType, firstName, lastName, primaryCMethod, assignTo,
					corpUser.getuserFullName(), regionName, franchiseId, title, emailId, config);

			fc.utobj().clickElement(driver, pobj.contactsLinks);
			contactsFilter(driver, contactType);
			fc.utobj().printTestStep("Add Other Address");
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//a[contains(text () , '" + firstName + " " + lastName + "')]"));
			fc.utobj().clickElement(driver, pobj.otherAddressTab);

			String titleAdd = fc.utobj().generateTestData(dataSet.get("titleAdd"));
			fc.utobj().sendKeys(driver, pobj.titleAddress, titleAdd);
			String address = fc.utobj().generateTestData(dataSet.get("address"));
			fc.utobj().sendKeys(driver, pobj.address, address);
			fc.utobj().clickElement(driver, pobj.saveBtn);

			fc.utobj().printTestStep("Modify The Other Address");
			fc.utobj().clickElement(driver, pobj.modifyAddressBtn);
			titleAdd = fc.utobj().generateTestData(dataSet.get("titleAdd"));
			fc.utobj().sendKeys(driver, pobj.titleAddress, titleAdd);
			address = fc.utobj().generateTestData(dataSet.get("address"));
			fc.utobj().sendKeys(driver, pobj.address, address);
			fc.utobj().clickElement(driver, pobj.saveBtn);

			fc.utobj().printTestStep("Verify The Modify Other Address");
			boolean isAddressTitlePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//td/span[.='" + titleAdd + "']");
			if (isAddressTitlePresent == false) {
				fc.utobj().throwsException("was not able to verify add address");
			}

			boolean isAddressPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () , 'Address')]/ancestor::tr/td[.='" + address + "']");
			if (isAddressPresent == false) {
				fc.utobj().throwsException("was not able to verify address Name");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = {"crm"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Delete Address At CRM > Contact > Other Address", testCaseId = "TC_229_Delete_Address")
	public void deleteAddress() throws IOException, Exception, ParserConfigurationException, SAXException {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());
		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			CRMContactsPage pobj = new CRMContactsPage(driver);
			fc.utobj().printTestStep("Navigate To Admin Users Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");

			AdminUsersManageCorporateUsersAddCorporateUserPageTest corpTest = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			String emailId = "crmautomation@staffex.com";
			CorporateUser corpUser = new CorporateUser();
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corpUser.setEmail(emailId);
			corpUser = corpTest.createDefaultUser(driver, corpUser);
			fc.utobj().printTestStep("Navigate To Admin > CRM > Contact Type Configuration");
			fc.utobj().printTestStep("Add Contact Type");
			AdminCRMContactTypeConfigurationPageTest contatcTypePage = new AdminCRMContactTypeConfigurationPageTest();
			String contactType = fc.utobj().generateTestData(dataSet.get("contactType"));
			contatcTypePage.addContactType(driver, contactType);

			/*
			 * fc.utobj().printTestStep(testCaseId,
			 * "Navigating to Admin > CRM > Configure Lead Type");
			 * fc.utobj().printTestStep(testCaseId, "Add Lead Type");
			 * AdminCRMConfigureLeadTypePageTest leadTypePage = new
			 * AdminCRMConfigureLeadTypePageTest(); String leadType =
			 * fc.utobj().generateTestData(dataSet.get("leadType"));
			 * leadTypePage.addLeadType(driver, leadType, config);
			 * 
			 * AdminCRMSourceSummaryPageTest addSourcePage = new
			 * AdminCRMSourceSummaryPageTest(); String source =
			 * fc.utobj().generateTestData(dataSet.get("source")); String
			 * description =
			 * fc.utobj().generateTestData(dataSet.get("description")); String
			 * displayOnWebPageText =
			 * fc.utobj().generateTestData(dataSet.get("text"));
			 * fc.utobj().printTestStep(testCaseId,
			 * "Navigating to Admin > CRM > Source Summary");
			 * fc.utobj().printTestStep(testCaseId, "Add Source");
			 * addSourcePage.addSource(driver, source, description,
			 * displayOnWebPageText, config);
			 * fc.utobj().printTestStep(testCaseId, "Add Source Details");
			 * String sourceDetails =
			 * fc.utobj().generateTestData(dataSet.get("sourceDetails"));
			 * addSourcePage.addSourceDetails(driver, source, sourceDetails,
			 * displayOnWebPageText, description, config);
			 * fc.utobj().printTestStep(testCaseId,
			 * "Navigating to Admin > CRM > Configure Medium");
			 * fc.utobj().printTestStep(testCaseId, "Add Medium");
			 * AdminCRMConfigureMediumPageTest mediumPage = new
			 * AdminCRMConfigureMediumPageTest(); String medium =
			 * fc.utobj().generateTestData(dataSet.get("medium"));
			 * mediumPage.addMedium(driver, medium, description, config);
			 */

			fc.utobj().printTestStep("Navigate To CRM > Contacts > Contacts Summary Page");
			fc.utobj().printTestStep("Add Contact");
			fc.crm().crm_common().CRMContactsLnk(driver);
			fc.utobj().clickElement(driver, pobj.addNew);
			fc.utobj().clickElement(driver, pobj.contactLnk);

			String firstName = fc.utobj().generateTestData(dataSet.get("firstName"));
			String lastName = fc.utobj().generateTestData(dataSet.get("lastName"));
			String primaryCMethod = dataSet.get("primaryCMethod");
			String assignTo = "Corporate";
			String regionName = "";
			String franchiseId = "";
			String title = dataSet.get("title");

			addContactGeneric(driver, contactType, firstName, lastName, primaryCMethod, assignTo,
					corpUser.getuserFullName(), regionName, franchiseId, title, emailId, config);

			fc.utobj().clickElement(driver, pobj.contactsLinks);
			contactsFilter(driver, contactType);
			fc.utobj().printTestStep("Add Other Address");
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//a[contains(text () , '" + firstName + " " + lastName + "')]"));
			fc.utobj().clickElement(driver, pobj.otherAddressTab);

			String titleAdd = fc.utobj().generateTestData(dataSet.get("titleAdd"));
			fc.utobj().sendKeys(driver, pobj.titleAddress, titleAdd);
			String address = fc.utobj().generateTestData(dataSet.get("address"));
			fc.utobj().sendKeys(driver, pobj.address, address);
			fc.utobj().clickElement(driver, pobj.saveBtn);

			fc.utobj().printTestStep("Delete Other Address");
			fc.utobj().clickElement(driver, pobj.deleteAddress);
			fc.utobj().acceptAlertBox(driver);

			fc.utobj().printTestStep("Verify The Delete Other Address");
			boolean isAddressTitlePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//td/span[.='" + titleAdd + "']");
			if (isAddressTitlePresent == true) {
				fc.utobj().throwsException("was not able to delete address");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "crm")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Add More Address At CRM > Contact > Other Address", testCaseId = "TC_230_Add_More_Address")
	public void addMoreAddress() throws IOException, Exception, ParserConfigurationException, SAXException {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			CRMContactsPage pobj = new CRMContactsPage(driver);

			fc.utobj().printTestStep("Navigate To Admin Users Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			String emailId = "crmautomation@staffex.com";
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUser = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporateUser.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);
			fc.utobj().printTestStep("Navigate To Admin > CRM > Contact Type Configuration");
			fc.utobj().printTestStep("Add Contact Type");
			AdminCRMContactTypeConfigurationPageTest contatcTypePage = new AdminCRMContactTypeConfigurationPageTest();
			String contactType = fc.utobj().generateTestData(dataSet.get("contactType"));
			contatcTypePage.addContactType(driver, contactType);

			fc.utobj().printTestStep("Navigate To CRM > Contacts > Contacts Summary Page");
			fc.utobj().printTestStep("Add Contact");
			fc.crm().crm_common().CRMContactsLnk(driver);
			fc.utobj().clickElement(driver, pobj.addNew);
			fc.utobj().clickElement(driver, pobj.contactLnk);

			String firstName = fc.utobj().generateTestData(dataSet.get("firstName"));
			String lastName = fc.utobj().generateTestData(dataSet.get("lastName"));
			String primaryCMethod = dataSet.get("primaryCMethod");
			String assignTo = "Corporate";
			String regionName = "";
			String franchiseId = "";
			String title = dataSet.get("title");

			addContactGeneric(driver, contactType, firstName, lastName, primaryCMethod, assignTo,
					corpUser.getuserFullName(), regionName, franchiseId, title, emailId, config);

			fc.utobj().clickElement(driver, pobj.contactsLinks);
			contactsFilter(driver, contactType);
			fc.utobj().printTestStep("Add Other Address");
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//a[contains(text () , '" + firstName + " " + lastName + "')]"));

			fc.utobj().clickElement(driver, pobj.otherAddressTab);

			String titleAdd = fc.utobj().generateTestData(dataSet.get("titleAdd"));
			fc.utobj().sendKeys(driver, pobj.titleAddress, titleAdd);
			String address = fc.utobj().generateTestData(dataSet.get("address"));
			fc.utobj().sendKeys(driver, pobj.address, address);
			fc.utobj().clickElement(driver, pobj.saveBtn);

			fc.utobj().printTestStep("Add More Addeess");
			fc.utobj().clickElement(driver, pobj.addMoreAddress);
			String titleAdd1 = fc.utobj().generateTestData(dataSet.get("titleAdd"));
			fc.utobj().sendKeys(driver, pobj.titleAddress, titleAdd1);
			String address1 = fc.utobj().generateTestData(dataSet.get("address"));
			fc.utobj().sendKeys(driver, pobj.address, address1);

			fc.utobj().clickElement(driver, pobj.saveBtn);

			fc.utobj().printTestStep("Verify The Add More Address");
			boolean isAddressTitlePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//td/span[.='" + titleAdd1 + "']");
			if (isAddressTitlePresent == false) {
				fc.utobj().throwsException("was not able to verify add address");
			}

			boolean isAddressPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () , 'Address')]/ancestor::tr/td[.='" + address1 + "']");
			if (isAddressPresent == false) {
				fc.utobj().throwsException("was not able to verify address Name");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "crm", "crmDummay" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Add Transaction At CRM > Contacts > Transaction", testCaseId = "TC_231_Add_New_Transaction")
	public void addNewTransaction() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			CRMAccountsPage pobj = new CRMAccountsPage(driver);
			CRMAccountsPageTest accountPage = new CRMAccountsPageTest();

			String description = fc.utobj().generateTestData(dataSet.get("description"));
			AdminCRMManageProductServiceCategoryPageTest serviceProductPage = new AdminCRMManageProductServiceCategoryPageTest();
			String categoryName = fc.utobj().generateTestData(dataSet.get("categoryName"));
			String productName = fc.utobj().generateTestData(dataSet.get("productName"));
			String oneLineDescription = fc.utobj().generateTestData(dataSet.get("oneLineDescription"));
			String rate = dataSet.get("rate");
			fc.utobj().printTestStep("Navigating to Admin > CRM >  Manage Product / Service & Category");
			fc.utobj().printTestStep("Add Category");
			serviceProductPage.addCategory(driver, categoryName, description);
			fc.utobj().printTestStep("Add Product/Service");
			serviceProductPage.addProductService(driver, categoryName, productName, oneLineDescription, rate);
			fc.utobj().printTestStep("Navigate To Admin Users Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			String emailId = "crmautomation@staffex.com";
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUser = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporateUser.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);
			fc.utobj().printTestStep("Navigating to Admin > CRM > ContactType Configuration");
			fc.utobj().printTestStep("Add Contact Type");
			AdminCRMContactTypeConfigurationPageTest contactTypePage = new AdminCRMContactTypeConfigurationPageTest();
			String contactType = fc.utobj().generateTestData(dataSet.get("contactType"));
			contactTypePage.addContactType(driver, contactType);
			fc.utobj().printTestStep("Navigating to Admin > CRM >  Configure Opportunity Stages");
			fc.utobj().printTestStep("Add Stage");
			AdminCRMConfigureOpportunityStagesPageTest stagePage = new AdminCRMConfigureOpportunityStagesPageTest();
			String stage = fc.utobj().generateTestData(dataSet.get("stage"));
			stagePage.addStage(driver, stage);

			AdminCRMConfigureIndustryPageTest industryPage = new AdminCRMConfigureIndustryPageTest();
			String industry = fc.utobj().generateTestData(dataSet.get("industry"));
			description = fc.utobj().generateTestData(dataSet.get("description"));
			fc.utobj().printTestStep("Navigating to Admin > CRM >  Configure Industry Type");
			fc.utobj().printTestStep("Add Industry");
			industryPage.addIndustry(driver, industry, description);

			String accountName = fc.utobj().generateTestData(dataSet.get("accountName"));
			String visibility = dataSet.get("visibility");
			fc.utobj().printTestStep("Navigating to CRM >  Accounts > Accounts Summary");
			fc.utobj().printTestStep("Add Account");
			accountPage.addAcocunt(driver, industry, accountName, visibility);

			fc.utobj().printTestStep("Navigate To CRM > Contacts > Contacts Summary Page");
			fc.utobj().printTestStep("Add Contact");
			fc.utobj().clickElement(driver, pobj.contactsLinks);
			fc.utobj().clickElement(driver, pobj.addNew);
			fc.utobj().clickElement(driver, pobj.contactLnk);

			String cFname = fc.utobj().generateTestData(dataSet.get("cFname"));
			String cLname = fc.utobj().generateTestData(dataSet.get("cLname"));
			String primaryCMethod = dataSet.get("primaryCMethod");
			String assignTo = "Corporate";
			String regionName = "";
			String franchiseId = "";
			String title = dataSet.get("title");

			addContactGenericWithAccount(driver, contactType, cFname, cLname, primaryCMethod, assignTo,
					corpUser.getuserFullName(), regionName, franchiseId, title, emailId, config, accountName);

			fc.utobj().printTestStep("Add Opportunity");
			fc.utobj().clickElement(driver, pobj.opportunitiesTab);
			fc.utobj().clickElement(driver, pobj.opportunityLink);

			String opportunityName = fc.utobj().generateTestData(dataSet.get("opportunityName"));
			String salesAmount = dataSet.get("salesAmount");

			fc.utobj().sendKeys(driver, pobj.opportunityName, opportunityName);
			fc.utobj().sendKeys(driver, pobj.opportunityOwner, corpUser.getuserFullName());
			fc.utobj().clickElement(driver, pobj.searchOppOwnerBtn);

			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//a[contains(text () , '" + corpUser.getuserFullName() + "')]"));
			fc.utobj().switchFrameToDefault(driver);
			fc.utobj().selectDropDown(driver, pobj.stage, stage);
			fc.utobj().sendKeys(driver, pobj.salesAmount, salesAmount);
			fc.utobj().clickElement(driver, pobj.saveBtn);

			fc.utobj().clickElement(driver, pobj.contactsLinks);
			contactsFilter(driver, contactType);

			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//a[contains(text () , '" + cFname + " " + cLname + "')]"));
			fc.utobj().clickElement(driver, pobj.transactionTab);

			fc.utobj().printTestStep("Add Transaction");
			fc.utobj().clickElement(driver, pobj.newTransactionLink);

			fc.utobj().sendKeys(driver, pobj.invoiceDate, fc.utobj().getFutureDateUSFormat(-1));
			fc.utobj().sendKeys(driver, pobj.opportunityName, opportunityName);

			fc.utobj().clickElement(driver, pobj.ajaxResult);
			fc.utobj().sendKeys(driver, pobj.invoiceDate, fc.utobj().getFutureDateUSFormat(-1));
			fc.utobj().clickElement(driver, pobj.selectProductAndService);
			fc.utobj().clickElement(driver, pobj.uncheckAll);
			fc.utobj().sendKeys(driver, pobj.searchProduct, productName);
			fc.utobj().clickElement(driver, pobj.checkAll);
			fc.utobj().clickElement(driver, pobj.okayBtn);
			fc.utobj().clickElement(driver, pobj.submitBtn);

			/*
			 * fc.utobj().printTestStep(testCaseId, "Verify The Add Transaction"
			 * ); boolean isProductServicePresent=fc.utobj().verifyCase(driver,
			 * ".//td[contains(text () , '"+productName+"')]"); if
			 * (isProductServicePresent==false) { fc.utobj().throwsException(
			 * "was not able to verify Transaction Name"); }
			 */

			fc.utobj().clickElement(driver, pobj.contactsLinks);
			contactsFilter(driver, contactType);
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//a[contains(text () , '" + cFname + " " + cLname + "')]"));
			fc.utobj().clickElement(driver, pobj.transactionTab);

			boolean isOppPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () , '" + opportunityName + "')]");
			if (isOppPresent == false) {
				fc.utobj().throwsException("was not able to verify Transaction");
			}

			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//td[contains(text () , '" + opportunityName + "')]/preceding-sibling::td/a"));

			boolean isProductAvailable = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () ,'" + productName + "')]");
			if (isProductAvailable == false) {
				fc.utobj().throwsException("was not able to verify Product");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "crm" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify the Modify Transaction At CRM > Contacts > Transaction", testCaseId = "TC_232_Modify_Transaction")
	public void modifyTransactionActionImage() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			CRMAccountsPage pobj = new CRMAccountsPage(driver);
			CRMAccountsPageTest accountPage = new CRMAccountsPageTest();

			String description = fc.utobj().generateTestData(dataSet.get("description"));
			AdminCRMManageProductServiceCategoryPageTest serviceProductPage = new AdminCRMManageProductServiceCategoryPageTest();
			String categoryName = fc.utobj().generateTestData(dataSet.get("categoryName"));
			String productName = fc.utobj().generateTestData(dataSet.get("productName"));
			String oneLineDescription = fc.utobj().generateTestData(dataSet.get("oneLineDescription"));
			String rate = dataSet.get("rate");
			fc.utobj().printTestStep("Navigating to Admin > CRM >  Manage Product / Service & Category");
			fc.utobj().printTestStep("Add Category");
			serviceProductPage.addCategory(driver, categoryName, description);
			fc.utobj().printTestStep("Add Product/Service");
			serviceProductPage.addProductService(driver, categoryName, productName, oneLineDescription, rate);

			String categoryName1 = fc.utobj().generateTestData(dataSet.get("categoryName"));
			String productName1 = fc.utobj().generateTestData(dataSet.get("productName"));
			String oneLineDescription1 = fc.utobj().generateTestData(dataSet.get("oneLineDescription"));
			serviceProductPage.addCategory(driver, categoryName1, description);
			serviceProductPage.addProductService(driver, categoryName1, productName1, oneLineDescription1, rate);
			fc.utobj().printTestStep("Navigate To Admin Users Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			//String emailId = "crmautomation@staffex.com";
			String emailId=fc.utobj().generateTestData("crmautomation")+"@staffex.com";
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUser = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporateUser.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);
			fc.utobj().printTestStep("Navigating to Admin > CRM > ContactType Configuration");
			fc.utobj().printTestStep("Add Contact Type");
			AdminCRMContactTypeConfigurationPageTest contactTypePage = new AdminCRMContactTypeConfigurationPageTest();
			String contactType = fc.utobj().generateTestData(dataSet.get("contactType"));
			contactTypePage.addContactType(driver, contactType);
			fc.utobj().printTestStep("Navigating to Admin > CRM >  Configure Opportunity Stages");
			fc.utobj().printTestStep("Add Stage");
			AdminCRMConfigureOpportunityStagesPageTest stagePage = new AdminCRMConfigureOpportunityStagesPageTest();
			String stage = fc.utobj().generateTestData(dataSet.get("stage"));
			stagePage.addStage(driver, stage);

			AdminCRMConfigureIndustryPageTest industryPage = new AdminCRMConfigureIndustryPageTest();
			String industry = fc.utobj().generateTestData(dataSet.get("industry"));
			description = fc.utobj().generateTestData(dataSet.get("description"));
			fc.utobj().printTestStep("Navigating to Admin > CRM >  Configure Industry Type");
			fc.utobj().printTestStep("Add Industry");
			industryPage.addIndustry(driver, industry, description);

			String accountName = fc.utobj().generateTestData(dataSet.get("accountName"));
			String visibility = dataSet.get("visibility");
			fc.utobj().printTestStep("Navigating to CRM >  Accounts > Accounts Summary");
			fc.utobj().printTestStep("Add Account");
			accountPage.addAcocunt(driver, industry, accountName, visibility);

			fc.utobj().printTestStep("Navigate To CRM > Contacts > Contacts Summary Page");
			fc.utobj().printTestStep("Add Contact");
			fc.utobj().clickElement(driver, pobj.contactsLinks);
			fc.utobj().clickElement(driver, pobj.addNew);
			fc.utobj().clickElement(driver, pobj.contactLnk);

			String cFname = fc.utobj().generateTestData(dataSet.get("cFname"));
			String cLname = fc.utobj().generateTestData(dataSet.get("cLname"));
			String primaryCMethod = dataSet.get("primaryCMethod");
			String assignTo = "Corporate";
			String regionName = "";
			String franchiseId = "";
			String title = dataSet.get("title");

			addContactGenericWithAccount(driver, contactType, cFname, cLname, primaryCMethod, assignTo,
					corpUser.getuserFullName(), regionName, franchiseId, title, emailId, config, accountName);

			fc.utobj().printTestStep("Add Opportunity");
			fc.utobj().clickElement(driver, pobj.opportunitiesTab);
			fc.utobj().clickElement(driver, pobj.opportunityLink);

			String opportunityName = fc.utobj().generateTestData(dataSet.get("opportunityName"));
			String salesAmount = dataSet.get("salesAmount");

			fc.utobj().sendKeys(driver, pobj.opportunityName, opportunityName);
			fc.utobj().sendKeys(driver, pobj.opportunityOwner, corpUser.getuserFullName());
			fc.utobj().clickElement(driver, pobj.searchOppOwnerBtn);

			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//a[contains(text () , '" + corpUser.getuserFullName() + "')]"));
			fc.utobj().switchFrameToDefault(driver);
			fc.utobj().selectDropDown(driver, pobj.stage, stage);
			fc.utobj().sendKeys(driver, pobj.salesAmount, salesAmount);
			fc.utobj().clickElement(driver, pobj.saveBtn);

			fc.utobj().clickElement(driver, pobj.contactsLinks);
			contactsFilter(driver, contactType);
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//a[contains(text () , '" + cFname + " " + cLname + "')]"));

			fc.utobj().printTestStep("Add Transaction");
			fc.utobj().clickElement(driver, pobj.transactionTab);
			fc.utobj().clickElement(driver, pobj.newTransactionLink);
			fc.utobj().sendKeys(driver, pobj.opportunityName, opportunityName);
			fc.utobj().clickElement(driver, pobj.ajaxResult);
			fc.utobj().sendKeys(driver, pobj.invoiceDate, fc.utobj().getCurrentDateUSFormat());

			fc.utobj().clickElement(driver, pobj.selectProductAndService);
			fc.utobj().clickElement(driver, pobj.uncheckAll);
			fc.utobj().sendKeys(driver, pobj.searchProduct, productName);
			fc.utobj().clickElement(driver, pobj.checkAll);
			fc.utobj().clickElement(driver, pobj.okayBtn);
			fc.utobj().clickElement(driver, pobj.submitBtn);

			fc.utobj().clickElement(driver, pobj.contactsLinks);
			contactsFilter(driver, contactType);
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//a[contains(text () , '" + cFname + " " + cLname + "')]"));
			fc.utobj().clickElement(driver, pobj.transactionTab);

			fc.utobj().printTestStep("Modify Transaction");
			fc.utobj().actionImgOption(driver, opportunityName, "Modify");
			fc.utobj().clickElement(driver, pobj.selectProductAndService);
			fc.utobj().clickElement(driver, pobj.uncheckAll);
			fc.utobj().sendKeys(driver, pobj.searchProduct, productName1);
			fc.utobj().clickElement(driver, pobj.checkAll);
			fc.utobj().clickElement(driver, pobj.okayBtn);
			fc.utobj().clickElement(driver, pobj.submitBtn);

			fc.utobj().printTestStep("Verify The Modify Transaction");
			boolean isProductServicePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () , '" + opportunityName + "')]");
			if (isProductServicePresent == false) {
				fc.utobj().throwsException("was not able to verify Modified Transaction Name");
			}

			fc.utobj().printTestStep("Verify The Modified Transaction at transaction tab");

			fc.utobj().clickElement(driver, pobj.contactsLinks);
			contactsFilter(driver, contactType);
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//a[contains(text () , '" + cFname + " " + cLname + "')]"));
			fc.utobj().clickElement(driver, pobj.transactionTab);

			boolean isOppPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () , '" + opportunityName + "')]");
			if (isOppPresent == false) {
				fc.utobj().throwsException("was not able to verify Transaction at Tranaction tab");
			}

			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//td[contains(text () , '" + opportunityName + "')]/preceding-sibling::td/a"));

			boolean isProductAvailable = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () ,'" + productName1 + "')]");
			if (isProductAvailable == false) {
				fc.utobj().throwsException("was not able to verify Product");
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "crm"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify the Delete Transaction At CRM > Contacts > Transaction", testCaseId = "TC_233_Delete_Transaction")
	public void deleteTransactionActionImage() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());
		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			CRMAccountsPage pobj = new CRMAccountsPage(driver);
			CRMAccountsPageTest accountPage = new CRMAccountsPageTest();

			String description = fc.utobj().generateTestData(dataSet.get("description"));
			AdminCRMManageProductServiceCategoryPageTest serviceProductPage = new AdminCRMManageProductServiceCategoryPageTest();
			String categoryName = fc.utobj().generateTestData(dataSet.get("categoryName"));
			String productName = fc.utobj().generateTestData(dataSet.get("productName"));
			String oneLineDescription = fc.utobj().generateTestData(dataSet.get("oneLineDescription"));
			String rate = dataSet.get("rate");
			fc.utobj().printTestStep("Navigating to Admin > CRM >  Manage Product / Service & Category");
			fc.utobj().printTestStep("Add Category");
			serviceProductPage.addCategory(driver, categoryName, description);
			fc.utobj().printTestStep("Add Product/Service");
			serviceProductPage.addProductService(driver, categoryName, productName, oneLineDescription, rate);

			String categoryName1 = fc.utobj().generateTestData(dataSet.get("categoryName"));
			String productName1 = fc.utobj().generateTestData(dataSet.get("productName"));
			String oneLineDescription1 = fc.utobj().generateTestData(dataSet.get("oneLineDescription"));
			serviceProductPage.addCategory(driver, categoryName1, description);
			serviceProductPage.addProductService(driver, categoryName1, productName1, oneLineDescription1, rate);
			fc.utobj().printTestStep("Navigate To Admin Users Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			String emailId = "crmautomation@staffex.com";
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUser = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporateUser.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);
			fc.utobj().printTestStep("Navigating to Admin > CRM > ContactType Configuration");
			fc.utobj().printTestStep("Add Contact Type");
			AdminCRMContactTypeConfigurationPageTest contactTypePage = new AdminCRMContactTypeConfigurationPageTest();
			String contactType = fc.utobj().generateTestData(dataSet.get("contactType"));
			contactTypePage.addContactType(driver, contactType);
			fc.utobj().printTestStep("Navigating to Admin > CRM >  Configure Opportunity Stages");
			fc.utobj().printTestStep("Add Stage");
			AdminCRMConfigureOpportunityStagesPageTest stagePage = new AdminCRMConfigureOpportunityStagesPageTest();
			String stage = fc.utobj().generateTestData(dataSet.get("stage"));
			stagePage.addStage(driver, stage);

			AdminCRMConfigureIndustryPageTest industryPage = new AdminCRMConfigureIndustryPageTest();
			String industry = fc.utobj().generateTestData(dataSet.get("industry"));
			description = fc.utobj().generateTestData(dataSet.get("description"));
			fc.utobj().printTestStep("Navigating to Admin > CRM >  Configure Industry Type");
			fc.utobj().printTestStep("Add Industry");
			industryPage.addIndustry(driver, industry, description);

			String accountName = fc.utobj().generateTestData(dataSet.get("accountName"));
			String visibility = dataSet.get("visibility");
			fc.utobj().printTestStep("Navigating to CRM >  Accounts > Accounts Summary");
			fc.utobj().printTestStep("Add Account");
			accountPage.addAcocunt(driver, industry, accountName, visibility);

			fc.utobj().printTestStep("Navigate To CRM > Contacts > Contacts Summary Page");
			fc.utobj().printTestStep("Add Contact");
			fc.utobj().clickElement(driver, pobj.contactsLinks);
			fc.utobj().clickElement(driver, pobj.addNew);
			fc.utobj().clickElement(driver, pobj.contactLnk);

			String cFname = fc.utobj().generateTestData(dataSet.get("cFname"));
			String cLname = fc.utobj().generateTestData(dataSet.get("cLname"));
			String primaryCMethod = dataSet.get("primaryCMethod");
			String assignTo = "Corporate";
			String regionName = "";
			String franchiseId = "";
			String title = dataSet.get("title");

			addContactGenericWithAccount(driver, contactType, cFname, cLname, primaryCMethod, assignTo,
					corpUser.getuserFullName(), regionName, franchiseId, title, emailId, config, accountName);

			fc.utobj().printTestStep("Add Opportunity");
			fc.utobj().clickElement(driver, pobj.opportunitiesTab);
			fc.utobj().clickElement(driver, pobj.opportunityLink);

			String opportunityName = fc.utobj().generateTestData(dataSet.get("opportunityName"));
			String salesAmount = dataSet.get("salesAmount");

			fc.utobj().sendKeys(driver, pobj.opportunityName, opportunityName);
			fc.utobj().sendKeys(driver, pobj.opportunityOwner, corpUser.getuserFullName());
			fc.utobj().clickElement(driver, pobj.searchOppOwnerBtn);

			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//a[contains(text () , '" + corpUser.getuserFullName() + "')]"));
			fc.utobj().switchFrameToDefault(driver);
			fc.utobj().selectDropDown(driver, pobj.stage, stage);
			fc.utobj().sendKeys(driver, pobj.salesAmount, salesAmount);
			fc.utobj().clickElement(driver, pobj.saveBtn);

			fc.utobj().clickElement(driver, pobj.contactsLinks);
			contactsFilter(driver, contactType);
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//a[contains(text () , '" + cFname + " " + cLname + "')]"));

			fc.utobj().printTestStep("Add Transaction");
			fc.utobj().clickElement(driver, pobj.transactionTab);
			fc.utobj().clickElement(driver, pobj.newTransactionLink);
			fc.utobj().sendKeys(driver, pobj.opportunityName, opportunityName);
			fc.utobj().clickElement(driver, pobj.ajaxResult);

			fc.utobj().sendKeys(driver, pobj.invoiceDate, fc.utobj().getCurrentDateUSFormat());

			fc.utobj().clickElement(driver, pobj.selectProductAndService);
			fc.utobj().clickElement(driver, pobj.uncheckAll);
			fc.utobj().sendKeys(driver, pobj.searchProduct, productName);
			fc.utobj().clickElement(driver, pobj.checkAll);
			fc.utobj().clickElement(driver, pobj.okayBtn);
			fc.utobj().clickElement(driver, pobj.submitBtn);

			fc.utobj().clickElement(driver, pobj.contactsLinks);
			contactsFilter(driver, contactType);
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//a[contains(text () , '" + cFname + " " + cLname + "')]"));
			fc.utobj().clickElement(driver, pobj.transactionTab);

			fc.utobj().printTestStep("Delete Transaction");
			fc.utobj().actionImgOption(driver, opportunityName, "Delete");
			fc.utobj().acceptAlertBox(driver);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().clickElement(driver, pobj.closeBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Verify Deleted Transaction");
			boolean isNoRecordPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//td[.='No records found.']");
			if (isNoRecordPresent == false) {
				fc.utobj().throwsException("was not able to Archive Lead");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	/*
	 * Harish Dwivedi TC_279_Create_Task + TC_283_Verify_Filter_Associated_With
	 */

	@Test(groups = "crmNewtestCaseharish")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Create Task With Corporate User At Contacts > Contacts > Primary Info", testCaseId = "TC_279_Create_Task")
	public void cretaeTaskAtCorporateUser() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {

			driver = fc.loginpage().login(driver);
			CRMContactsPage pobj = new CRMContactsPage(driver);

			fc.utobj().printTestStep("Naviaget To Admin Users Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corpTest = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			String emailId = "crmautomation@staffex.com";
			CorporateUser corpUser = new CorporateUser();
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corpUser.setEmail(emailId);
			corpUser = corpTest.createDefaultUser(driver, corpUser);

			fc.utobj().printTestStep("Navigate To Admin > CRM > Contact Type Configuration");
			fc.utobj().printTestStep("Add Contact Type");
			AdminCRMContactTypeConfigurationPageTest contatcTypePage = new AdminCRMContactTypeConfigurationPageTest();
			String contactType = fc.utobj().generateTestData(dataSet.get("contactType"));
			contatcTypePage.addContactType(driver, contactType);

			fc.home_page().logout(driver);
			fc.loginpage().loginWithParameter(driver, corpUser.getUserName(), corpUser.getPassword());

			fc.utobj().printTestStep("Navigate To CRM > Contacts > Contacts Summary Page");
			fc.utobj().printTestStep("Add Contact");
			fc.crm().crm_common().CRMContactsLnk(driver);
			fc.utobj().clickElement(driver, pobj.addNew);
			fc.utobj().clickElement(driver, pobj.contactLnk);

			String firstName = fc.utobj().generateTestData(dataSet.get("firstName"));
			String lastName = fc.utobj().generateTestData(dataSet.get("lastName"));
			String primaryCMethod = dataSet.get("primaryCMethod");
			String assignTo = "Corporate";
			String regionName = "";
			String franchiseId = "";
			String title = dataSet.get("title");
			addContactGeneric(driver, contactType, firstName, lastName, primaryCMethod, assignTo,
					corpUser.getuserFullName(), regionName, franchiseId, title, emailId, config);

			fc.utobj().clickElement(driver, pobj.contactsLinks);
			contactsFilter(driver, contactType);
			fc.utobj().printTestStep("Log A Task");
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//a[contains(text () ,'" + firstName
					+ " " + lastName + "')]/ancestor::tr/td/input[@name='checkb']"));
			String statusTask = dataSet.get("statusTask");
			try {
				Thread.sleep(2000);
				fc.utobj().selectActionMenuItemsWithTagA(driver, "Log a Task");
				Thread.sleep(2000);
				fc.commonMethods().switch_cboxIframe_frameId(driver);
				if (!fc.utobj().isSelected(driver,pobj.radioOwner)) {
					fc.utobj().clickElement(driver, pobj.radioOwner);
				}

				fc.utobj().selectDropDown(driver, pobj.statusTask, statusTask);
			} catch (Exception ee) {
				ee.printStackTrace();
			}
			String subject = fc.utobj().generateTestData(dataSet.get("subject"));
			fc.utobj().sendKeys(driver, pobj.subject, subject);
			if (!fc.utobj().isSelected(driver, pobj.timeLessTask)) {
				fc.utobj().clickElement(driver, pobj.timeLessTask);
			}
			String priority = dataSet.get("priority");
			fc.utobj().selectDropDown(driver, pobj.priority, priority);
			fc.utobj().sendKeys(driver, pobj.comments, "Test Comment");
			fc.utobj().clickElement(driver, pobj.createBtn);
			fc.utobj().switchFrameToDefault(driver);

			contactsFilter(driver, contactType);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//a[contains(text () ,'" + firstName + " " + lastName + "')]"));

			fc.utobj().printTestStep("Verify Task");
			boolean isSubjectPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[contains(text () ,'" + subject + "')]");
			if (isSubjectPresent == false) {
				fc.utobj().throwsException("was not able to verify subject");
			}
			boolean isUserPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[contains(text () ,'" + subject
					+ "')]/ancestor::tr/td/div[.='" + corpUser.getuserFullName() + "']");
			if (isUserPresent == false) {
				fc.utobj().throwsException("was not able to verify userName at Lead Info Page");
			}
			boolean isStatusOfTaskPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[contains(text () ,'" + subject
					+ "')]/ancestor::tr/td[contains(text () ,'" + statusTask + "')]");
			if (isStatusOfTaskPresent == false) {
				fc.utobj().throwsException("was not able to verify status of task");
			}
			boolean isPriorityPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[contains(text () ,'" + subject
					+ "')]/ancestor::tr/td[contains(text () ,'" + priority + "')]");
			if (isPriorityPresent == false) {
				fc.utobj().throwsException("was not able to verify Priority of task");
			}

			/*
			 * Need to start TC_283_Verify_Filter_Associated_With //fot his tme
			 * only fc.crm().crm_common().CRMContactsLnk( driver);
			 * fc.crm().crm_common().CRMHomeLnk( driver);
			 * 
			 * fc.utobj().clickElement(driver, pobj.TaskLink);
			 * fc.utobj().clickElement(driver, pobj.showFilter);
			 * //fc.utobj().clickElement(driver, pobj.addTask);
			 */

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	/* Harish Dwivedi TC_290_Verify_Configure_Last_Contacted_Field_Log_Call */
	/* Anukaran Mishra */

	@Test(groups = { "crm"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2017-06-22", testCaseDescription = "Verify the Last Contacted Field at Log A Call At CRM > Contact > Contact Summary", testCaseId = "TC_290_Verify_Configure_Last_Contacted_Field_Log_Call")
	public void logACallVerifyConfigureLastContacted()
			throws IOException, Exception, ParserConfigurationException, SAXException {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			CRMContactsPage pobj = new CRMContactsPage(driver);

			fc.utobj().printTestStep("Naviaget To Admin Users Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corpTest = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			//String emailId = "crmautomation@staffex.com";
			String emailId=fc.utobj().generateTestData("crmautomation")+"@staffex.com";
			CorporateUser corpUser = new CorporateUser();
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corpUser.setEmail(emailId);
			corpUser = corpTest.createDefaultUser(driver, corpUser);

			fc.home_page().logout(driver);
			fc.loginpage().loginWithParameter(driver, corpUser.getUserName(), corpUser.getPassword());
			/*
			 * fc.utobj().printTestStep(testCaseId,
			 * "Navigate To Admin > CRM > Contact Type Configuration");
			 * fc.utobj().printTestStep(testCaseId, "Add Contact Type");
			 * AdminCRMContactTypeConfigurationPageTest contatcTypePage = new
			 * AdminCRMContactTypeConfigurationPageTest(); String contactType =
			 * fc.utobj().generateTestData(dataSet.get("contactType"));
			 * contatcTypePage.addContactType(driver, contactType, config);
			 */
			AdminCRMConfigureLastContactedFieldPageTest LastContract = new AdminCRMConfigureLastContactedFieldPageTest();
			fc.utobj().printTestStep("Navigating to  Admin > CRM > Configure Last Contacted Field");
			fc.utobj().printTestStep("Configure Last Contacted Field");
			LastContract.createConfigureLastContactedField(driver, config);

			fc.utobj().printTestStep("Navigate To CRM > Contacts > Contacts Summary Page");
			fc.utobj().printTestStep("Add Contact");
			fc.crm().crm_common().CRMContactsLnk(driver);
			fc.utobj().clickElement(driver, pobj.addNew);
			fc.utobj().clickElement(driver, pobj.contactLnk);

			String firstName = fc.utobj().generateTestData(dataSet.get("firstName"));
			String lastName = fc.utobj().generateTestData(dataSet.get("lastName"));
			String primaryCMethod = dataSet.get("primaryCMethod");
			String assignTo = "Corporate";
			String regionName = "";
			String franchiseId = "";
			String title = dataSet.get("title");
			String contactType = "Contacts";

			addContactGeneric(driver, contactType, firstName, lastName, primaryCMethod, assignTo,
					corpUser.getuserFullName(), regionName, franchiseId, title, emailId, config);

			fc.utobj().clickElement(driver, pobj.contactsLinks);
			// contactsFilter(driver, contactType);
			fc.utobj().printTestStep("Log A Call");
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//a[contains(text () , '" + firstName + " " + lastName + "')]"));
			fc.utobj().clickElement(driver, pobj.logACallTab);
			String callSubject = fc.utobj().generateTestData(dataSet.get("subject"));
			fc.utobj().sendKeys(driver, pobj.callSubject, callSubject);
			/*
			 * String callSubject =
			 * fc.utobj().generateTestData(dataSet.get("callSubject"));
			 * fc.utobj().sendKeys(driver, pobj.callSubject1, callSubject);
			 */
			/*String callDate = fc.utobj().getCurrentDateUSFormat();
			fc.utobj().sendKeys(driver, pobj.callDate, callDate);*/
			String currentdateAndTime=fc.utobj().getCurrentDateAndTime();
			fc.utobj().sendKeys(driver, pobj.callDate,currentdateAndTime);
			
			String arraydate[]=currentdateAndTime.split(" ");
			String currentdate=arraydate[0];
			
			String callStatus = dataSet.get("callStatus");
			fc.utobj().selectDropDown(driver, pobj.callStatus, callStatus);
			String cType = dataSet.get("callType");
			fc.utobj().selectDropDown(driver, pobj.communicationType, cType);
			fc.utobj().clickElement(driver, pobj.saveBtn);

			fc.utobj().clickElement(driver, pobj.detailedHistoryLink);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().clickElement(driver, pobj.callsTabDH);

			fc.utobj().printTestStep("Verify Call At Detailed History Frame");
			boolean isCallSubjectPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () ,'" + callSubject + "')]");
			if (isCallSubjectPresent == false) {
				fc.utobj().throwsException("was not able to verify Call Subject");
			}
			boolean isCommunicationTypePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () ,'" + cType + "')]");
			if (isCommunicationTypePresent == false) {
				fc.utobj().throwsException("was not able to verify communication type");
			}
			fc.utobj().clickElement(driver, pobj.closeBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Verify Last Contacted Field in Contact Detail page");
			boolean isCallLastContractPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//*[contains(text () ,'Last Contacted')]/ancestor::tr/td[contains(text () , '" + currentdate
							+ "')]");
			if (isCallLastContractPresent == false) {
				fc.utobj().throwsException("was not able to verify Last Contacted Field");
			}
			fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	/* Anukaran Mishra */

	@Test(groups = { "crmmodulewaitmail67", "crm","errorpages1"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2017-06-26", testCaseDescription = "Verify The Last Contacted Field after Send Email Action Image Options At CRM > Contact > Contact Summary", testCaseId = "TC_290_Verify_Configure_Last_Contacted_Field_Log_A_Send_Email")
	public void sendEmailActionImagetoverifyLastField()
			throws IOException, Exception, ParserConfigurationException, SAXException {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());
		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			CRMContactsPage pobj = new CRMContactsPage(driver);
			CRMAccountsPageTest accountPage = new CRMAccountsPageTest();

			String parentWindowsHandle = driver.getWindowHandle();

			fc.utobj().printTestStep("Naviaget To Admin Users Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corpTest = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			//String emailId = "crmautomation@staffex.com";
			String emailId=fc.utobj().generateTestData("crmautomation")+"@staffex.com";
			CorporateUser corpUser = new CorporateUser();
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corpUser.setEmail(emailId);
			corpUser = corpTest.createDefaultUser(driver, corpUser);

			fc.utobj().printTestStep("Navigate To Admin > CRM > Contact Type Configuration");
			fc.utobj().printTestStep("Add Contact Type");
			AdminCRMContactTypeConfigurationPageTest contatcTypePage = new AdminCRMContactTypeConfigurationPageTest();
			String contactType = fc.utobj().generateTestData(dataSet.get("contactType"));
			contatcTypePage.addContactType(driver, contactType);

			AdminCRMConfigureLastContactedFieldPageTest LastContract = new AdminCRMConfigureLastContactedFieldPageTest();
			fc.utobj().printTestStep("Navigating to  Admin > CRM > Configure Last Contacted Field");
			fc.utobj().printTestStep("Configure Last Contacted Field");
			LastContract.createConfigureLastContactedField(driver, config);

			fc.utobj().printTestStep("Navigate To CRM > Contacts > Contactsss Summary Page");
			fc.utobj().printTestStep("Add Contact");
			fc.crm().crm_common().CRMContactsLnk(driver);
			fc.utobj().clickElement(driver, pobj.addNew);
			fc.utobj().clickElement(driver, pobj.contactLnk);

			String firstName = fc.utobj().generateTestData(dataSet.get("firstName"));
			String lastName = fc.utobj().generateTestData(dataSet.get("lastName"));
			String primaryCMethod = dataSet.get("primaryCMethod");
			String assignTo = "Corporate";
			String regionName = "";
			String franchiseId = "";
			String title = dataSet.get("title");

			fc.utobj().printTestStep("Adding Contact");
			addContactGeneric(driver, contactType, firstName, lastName, primaryCMethod, assignTo,
					corpUser.getuserFullName(), regionName, franchiseId, title, emailId, config);

			fc.utobj().clickElement(driver, pobj.contactsLinks);
			contactsFilter(driver, contactType);
			fc.utobj().printTestStep("Send Email");
			fc.utobj().actionImgOption(driver, firstName + " " + lastName, "Send Email");

			if (!fc.utobj().isSelected(driver, pobj.leadOwnerId)) {
				fc.utobj().clickElement(driver, pobj.leadOwnerId);
			}

			String mailCC = dataSet.get("mailCC");
			fc.utobj().sendKeys(driver, pobj.mailCC, mailCC);

			if (!fc.utobj().isSelected(driver,pobj.showBcc)) {
				fc.utobj().clickElement(driver, pobj.showBcc);
			}
			String mailBCC = dataSet.get("mailBCC");
			fc.utobj().sendKeys(driver, pobj.mailBCC, mailBCC);

			String subjectMail = fc.utobj().generateTestData(dataSet.get("subjectMail"));
			fc.utobj().sendKeys(driver, pobj.subjectMail, subjectMail);

			String editorText = fc.utobj().generateTestData(dataSet.get("mailText"));

			fc.utobj().switchFrameById(driver, "ta_ifr");

			Actions actions = new Actions(driver);
			actions.moveToElement(fc.utobj().getElement(driver, pobj.editorTextArea));
			actions.click();
			actions.sendKeys(editorText);
			fc.utobj().logReportMsg("Entered Text", editorText);
			actions.build().perform();
			fc.utobj().switchFrameToDefault(driver);
			fc.utobj().clickElement(driver, pobj.sendEmailBtn);
			fc.utobj().switchFrameToDefault(driver);

			contactsFilter(driver, contactType);
			fc.utobj().printTestStep("Verify Send Email At Detailed History Frame");
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//a[contains(text () , '" + firstName + " " + lastName + "')]"));
			fc.utobj().clickElement(driver, pobj.detailedHistoryLink);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().clickElement(driver, pobj.emailsTab);
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//*[contains(text () ,'" + firstName + "')]"));

			Set<String> allWindows = driver.getWindowHandles();
			for (String currentWindow : allWindows) {

				if (!currentWindow.equalsIgnoreCase(parentWindowsHandle)) {

					driver.switchTo().window(currentWindow);
					try {

						if (fc.utobj().getElementByXpath(driver, ".//*[.='Email Details']").isDisplayed()) {

							boolean isFromTextPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
									".//*[contains(text () ,'From')]/ancestor::tr/td[contains(text () , '"
											+ corpUser.getuserFullName() + "')]");
							if (isFromTextPresent == false) {
								fc.utobj().throwsException("was not able to From Text");
							}

							boolean isToTextPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
									".//*[contains(text () ,'To')]/ancestor::tr/td[contains(text () , '" + firstName
											+ " " + lastName + "')]");
							if (isToTextPresent == false) {
								fc.utobj().throwsException("was not able to To Text");
							}
							boolean isSubjectTextPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
									".//*[contains(text () ,'Subject')]/ancestor::tr/td[contains(text () , '"
											+ subjectMail + "')]");
							if (isSubjectTextPresent == false) {
								fc.utobj().throwsException("was not able to Subject");
							}
							boolean isTextPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//*[.='" + editorText + "']");
							if (isTextPresent == false) {
								fc.utobj().throwsException("was not able to verify Editor text");
							}
							driver.close();
						}

					} catch (Exception e) {
						driver.close();
					}
				}
				driver.switchTo().window(parentWindowsHandle);
			}
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().clickElement(driver, pobj.closeBtn);
			fc.utobj().switchFrameToDefault(driver);

			String LastUpdatedlDate = fc.utobj().getCurrentDateUSFormat();
			fc.utobj().printTestStep("Verify Last Contacted Field in Contact Detail page");

			// Need to check becuase it picks the value of another attribute
			// boolean isSubjectTextPresent=fc.utobj().verifyCase(driver,
			// ".//*[contains(text () ,'Last
			// Contacted')]/ancestor::tr/td[contains(text () , '02/01/2017')]");
			boolean isCallLastContractPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//*[contains(text () ,'Last Contacted')]/ancestor::tr/td[contains(text () , '" + LastUpdatedlDate
							+ "')]");

			if (isCallLastContractPresent == false) {
				fc.utobj().throwsException("was not able to verify Last Contacted Field");
			}

			CRMAccountsPage npobj = new CRMAccountsPage(driver);

			String description = fc.utobj().generateTestData(dataSet.get("description"));
			AdminCRMManageProductServiceCategoryPageTest serviceProductPage = new AdminCRMManageProductServiceCategoryPageTest();
			String categoryName = fc.utobj().generateTestData(dataSet.get("categoryName"));
			String productName = fc.utobj().generateTestData(dataSet.get("productName"));
			String oneLineDescription = fc.utobj().generateTestData(dataSet.get("oneLineDescription"));
			String rate = "1000";
			fc.utobj().printTestStep("Navigating to Admin > CRM >  Manage Product / Service & Category");
			fc.utobj().printTestStep("Add Category");
			serviceProductPage.addCategory(driver, categoryName, description);

			fc.utobj().printTestStep("Add Product/Service");
			serviceProductPage.addProductService(driver, categoryName, productName, oneLineDescription, rate);

			/*
			 * fc.utobj().printTestStep(testCaseId,
			 * "Navigating to Admin > CRM > ContactType Configuration");
			 * fc.utobj().printTestStep(testCaseId, "Add Contact Type");
			 * AdminCRMContactTypeConfigurationPageTest contactTypePage=new
			 * AdminCRMContactTypeConfigurationPageTest(); String
			 * contactType=fc.utobj().generateTestData(dataSet.get("contactType"
			 * )); contactTypePage.addContactType(driver, contactType, config);
			 */

			String stage = "Prospecting";
			// stagePage.addStage(driver, stage, config);
			AdminCRMConfigureIndustryPageTest industryPage = new AdminCRMConfigureIndustryPageTest();
			String industry = fc.utobj().generateTestData("Industry");
			fc.utobj().printTestStep("Navigating to Admin > CRM >  Configure Industry Type");
			fc.utobj().printTestStep("Add Industry");
			industryPage.addIndustry(driver, industry, description);

			String accountName = fc.utobj().generateTestData(dataSet.get("Accountname"));
			String visibility = "Corporate";
			fc.utobj().printTestStep("Navigating to CRM >  Accounts > Accounts Summary");
			fc.utobj().printTestStep("Add Account");
			accountPage.addAcocunt(driver, industry, accountName, visibility);
			fc.utobj().printTestStep("Navigate To CRM > Contacts > Contacts Summary Page");
			fc.utobj().printTestStep("Add Contact");
			fc.utobj().clickElement(driver, npobj.contactsLinks);
			fc.utobj().clickElement(driver, npobj.addNew);
			fc.utobj().clickElement(driver, npobj.contactLnk);

			String cFname = fc.utobj().generateTestData(dataSet.get("firstName"));
			String cLname = fc.utobj().generateTestData(dataSet.get("LastName"));
			String primaryCMethod1 = dataSet.get("primaryCMethod");
			String assignTo1 = "Corporate";
			String regionName1 = "";
			String franchiseId1 = "";
			String title1 = dataSet.get("title");

			addContactGenericWithAccount(driver, contactType, cFname, cLname, primaryCMethod1, assignTo1,
					corpUser.getuserFullName(), regionName1, franchiseId1, title1, emailId, config, accountName);

			fc.utobj().printTestStep("Add Opportunity");
			fc.utobj().clickElement(driver, npobj.contactsLinks);
			fc.utobj().clickElement(driver, npobj.addNew);
			fc.utobj().clickElement(driver, npobj.OpportunityLnk);

			String opportunityName = fc.utobj().generateTestData(dataSet.get("opportunityName"));
			String contactName = cFname + " " + cLname;
			String opportunityType = dataSet.get("opportunityType");
			String opportunitySource = dataSet.get("opportunitySource");
			String salesAmount = "100";
			accountPage.addOpportunity(driver, opportunityName, accountName, contactName, corpUser.getuserFullName(),
					stage, opportunityType, opportunitySource, salesAmount, config);

			fc.utobj().clickElement(driver, npobj.contactsLinks);
			contactsFilter(driver, contactType);

			fc.utobj().printTestStep("Add Transaction");

			fc.utobj().actionImgOption(driver, contactName, "Add Transaction");
			fc.utobj().sendKeys(driver, npobj.opportunityName, opportunityName);
			// fc.utobj().clickElement(driver, npobj.searchOpportunityBtn);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='opportunityNameTD']/a"));
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//a[contains(text(),'" + opportunityName + "')]"));
			fc.utobj().switchFrameToDefault(driver);
			/*
			 * try{ fc.utobj().clickElement(driver,
			 * fc.utobj().getElementByXpath(driver,
			 * ".//*[@id='customizedAjaxSearch']/div")); } catch(Exception e){
			 * fc.utobj().clickElement(driver,
			 * fc.utobj().getElementByXpath(driver,
			 * ".//*[@id='customizedAjaxSearch']/div[1]")); }
			 */
			fc.utobj().sendKeys(driver, npobj.invoiceDate, fc.utobj().getFutureDateUSFormat(-1));
			fc.utobj().clickElement(driver, npobj.selectProductAndService);
			fc.utobj().clickElement(driver, npobj.uncheckAll);
			fc.utobj().sendKeys(driver, npobj.searchProduct, productName);
			fc.utobj().clickElement(driver, npobj.checkAll);
			fc.utobj().clickElement(driver, npobj.okayBtn);
			fc.utobj().clickElement(driver, npobj.submitBtn);

			fc.utobj().clickElement(driver, npobj.contactsLinks);
			contactsFilter(driver, contactType);

			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//a[contains(text () , '" + contactName + "')]"));

			LastUpdatedlDate = fc.utobj().getCurrentDateUSFormat();
			fc.utobj().printTestStep("Verify Last Contacted Field in Contact Detail page");

			// Need to check becuase it picks the value of another attribute
			// boolean isSubjectTextPresent=fc.utobj().verifyCase(driver,
			// ".//*[contains(text () ,'Last
			// Contacted')]/ancestor::tr/td[contains(text () , '02/01/2017')]");
			boolean isLastContractPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//*[contains(text () ,'Last Contacted')]/ancestor::tr/td[contains(text () , '" + LastUpdatedlDate
							+ "')]");

			if (isLastContractPresent == false) {
				fc.utobj().throwsException("was not able to verify Last Contacted Field");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	/* Harish Dwivedi Regarding TC_282_Verify_Search */
	public void addContactGenericWithAdditionalParameter(WebDriver driver, String contactType, String firstName,
			String lastName, String primaryCMethod, String assignTo, String userName, String regionName,
			String franchiseId, String title, String email, String address, Map<String, String> config)
			throws Exception {

		String testCaseId = "TC_Add_Contact_Generic";

		if (fc.utobj().validate(testCaseId)) {
			try {
				CRMContactsPage pobj = new CRMContactsPage(driver);
				fc.utobj().selectDropDown(driver, pobj.contactType, contactType);
				fc.utobj().selectDropDown(driver, pobj.title, title);
				fc.utobj().sendKeys(driver, pobj.contactFirstName, firstName);
				fc.utobj().sendKeys(driver, pobj.contactLastName, lastName);
				fc.utobj().sendKeys(driver, pobj.suffix, "suffixTest");
				fc.utobj().sendKeys(driver, pobj.position, "TestJobTitle");
				fc.utobj().sendKeys(driver, pobj.address, "" + address);
				fc.utobj().sendKeys(driver, pobj.city, "TestCity");
				fc.utobj().selectDropDown(driver, pobj.country, "USA");
				fc.utobj().selectDropDown(driver, pobj.state, "Alabama");
				fc.utobj().selectDropDown(driver, pobj.primaryContactMethodSelect, primaryCMethod);
				fc.utobj().sendKeys(driver, pobj.bestTimeToContact, "12:00 PM EST");
				fc.utobj().sendKeys(driver, pobj.zipcode, "12345");
				fc.utobj().sendKeys(driver, pobj.phoneNumbers, "1236547896");
				fc.utobj().sendKeys(driver, pobj.extn, "12");
				fc.utobj().sendKeys(driver, pobj.faxNumbers, "1263985487");
				fc.utobj().sendKeys(driver, pobj.mobileNumbers, "8896724441");
				fc.utobj().sendKeys(driver, pobj.emailIds, email);
				fc.utobj().sendKeys(driver, pobj.alternateEmail, "test@gmail.com");
				fc.utobj().sendKeys(driver, pobj.birthdate, "11/24/1980");
				fc.utobj().sendKeys(driver, pobj.anniversarydate, "11/24/1999");

				fc.utobj().selectDropDown(driver, pobj.rating, "Cold");

				if (assignTo.equalsIgnoreCase("Corporate")) {

					if (!fc.utobj().isSelected(driver, pobj.assignToCorporate)) {
						fc.utobj().clickElement(driver, pobj.assignToCorporate);
					}

					fc.utobj().selectDropDown(driver, pobj.selectCorporateUserContact, userName);

				} else if (assignTo.equalsIgnoreCase("Regional")) {

					if (!fc.utobj().isSelected(driver, pobj.assignToRegional)) {
						fc.utobj().clickElement(driver, pobj.assignToRegional);
					}

					fc.utobj().selectDropDown(driver, pobj.selectAreaRegion, regionName);
					fc.utobj().selectDropDown(driver, pobj.selectRegionalUser, userName);

				} else if (assignTo.equalsIgnoreCase("Franchise")) {

					if (!fc.utobj().isSelected(driver, pobj.assignToFranchise)) {
						fc.utobj().clickElement(driver, pobj.assignToFranchise);
					}

					fc.utobj().selectDropDown(driver, pobj.selectFranchiseId, franchiseId);
					fc.utobj().selectDropDown(driver, pobj.selectFranchiseUser, userName);

				} else if (assignTo.equalsIgnoreCase("Only Franchise")) {
					fc.utobj().selectDropDown(driver, pobj.selectFranchiseUser, userName);

				}
				fc.utobj().sendKeys(driver, pobj.comments, "Test Comment");
				fc.utobj().clickElement(driver, pobj.saveBtn);

				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException("was not able to add Contact :: CRM > Contacts");

		}
	}

	public void actionImgOption(WebDriver driver, String task, String option) throws Exception {

		fc.utobj().moveToElement(driver,
				fc.utobj().getElementByXpath(driver, ".//*[contains(text () ,'" + task + "')]/ancestor::tr/td/layer"));
		String alterText = fc.utobj()
				.getElementByXpath(driver, ".//*[contains(text () ,'" + task + "')]/ancestor::tr/td/layer")
				.getAttribute("id").trim();
		alterText = alterText.replace("Actions_dynamicmenu", "");
		alterText = alterText.replace("Bar", "");

		fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
				".//*[contains(text () ,'" + task + "')]/ancestor::tr/td/layer/a/img"));
		// fc.utobj().clickElement(driver,
		// fc.utobj().getElementByXpath(driver,".//div[@id='Actions_dynamicmenu"+alterText+"Menu']/span[contains(text
		// () , '"+option+"')]"));
		fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
				".//div[@id='TaskActions_dynamicmenu" + alterText + "Menu']/*[contains(text () , '" + option + "')]"));
	}

	public void addContactGeneric(WebDriver driver, String contactType, String firstName, String lastName,
			String primaryCMethod, String assignTo, String userName, String regionName, String franchiseId,
			String title, String email, Map<String, String> config) throws Exception {

		String testCaseId = "TC_Add_Contact_Generic";

		if (fc.utobj().validate(testCaseId)) {
			try {
				CRMContactsPage pobj = new CRMContactsPage(driver);
				fc.utobj().selectDropDown(driver, pobj.contactType, contactType);
				fc.utobj().selectDropDown(driver, pobj.title, title);
				fc.utobj().sendKeys(driver, pobj.contactFirstName, firstName);
				fc.utobj().sendKeys(driver, pobj.contactLastName, lastName);
				fc.utobj().sendKeys(driver, pobj.suffix, "suffixTest");
				fc.utobj().sendKeys(driver, pobj.position, "TestJobTitle");
				fc.utobj().sendKeys(driver, pobj.address, "TestAddress");
				fc.utobj().sendKeys(driver, pobj.city, "TestCity");
				fc.utobj().selectDropDown(driver, pobj.country, "USA");
				fc.utobj().selectDropDown(driver, pobj.state, "Alabama");
				fc.utobj().selectDropDown(driver, pobj.primaryContactMethodSelect, primaryCMethod);
				fc.utobj().sendKeys(driver, pobj.bestTimeToContact, "12:00 PM EST");
				fc.utobj().sendKeys(driver, pobj.zipcode, "12345");
				fc.utobj().sendKeys(driver, pobj.phoneNumbers, "1236547896");
				fc.utobj().sendKeys(driver, pobj.extn, "12");
				fc.utobj().sendKeys(driver, pobj.faxNumbers, "1263985487");
				fc.utobj().sendKeys(driver, pobj.mobileNumbers, "8896724441");
				fc.utobj().sendKeys(driver, pobj.emailIds, email);
				fc.utobj().sendKeys(driver, pobj.alternateEmail, "test@gmail.com");
				fc.utobj().sendKeys(driver, pobj.birthdate, "11/24/1980");
				fc.utobj().sendKeys(driver, pobj.anniversarydate, "11/24/1999");

				if (assignTo.equalsIgnoreCase("Corporate")) {

					if (!fc.utobj().isSelected(driver, pobj.assignToCorporate)) {
						fc.utobj().clickElement(driver, pobj.assignToCorporate);
					}

					fc.utobj().selectDropDown(driver, pobj.selectCorporateUserContact, userName);

				} else if (assignTo.equalsIgnoreCase("Regional")) {

					if (!fc.utobj().isSelected(driver, pobj.assignToRegional)) {
						fc.utobj().clickElement(driver, pobj.assignToRegional);
					}

					fc.utobj().selectDropDown(driver, pobj.selectAreaRegion, regionName);
					fc.utobj().selectDropDown(driver, pobj.selectRegionalUser, userName);

				} else if (assignTo.equalsIgnoreCase("Franchise")) {

					try {
						if (!fc.utobj().isSelected(driver, pobj.assignToFranchise)) {
							fc.utobj().clickElement(driver, pobj.assignToFranchise);
						}
						fc.utobj().selectDropDown(driver, pobj.selectFranchiseId, franchiseId);
					} catch (Exception e) {
					}

					fc.utobj().selectDropDown(driver, pobj.selectFranchiseUser, userName);
					// fc.utobj().selectDropDownByPartialText(driver,
					// pobj.selectFranchiseUser, userName);
				}
				fc.utobj().sendKeys(driver, pobj.comments, "Test Comment");
				fc.utobj().clickElement(driver, pobj.saveBtn);

				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException("was not able to add Contact :: CRM > Contacts");

		}
	}

	public void addContactGenericWithAccount(WebDriver driver, String contactType, String firstName, String lastName,
			String primaryCMethod, String assignTo, String userName, String regionName, String franchiseId,
			String title, String email, Map<String, String> config, String accountName) throws Exception {

		String testCaseId = "TC_Add_Contact_Generic_With_Account";

		if (fc.utobj().validate(testCaseId)) {
			try {
				CRMContactsPage pobj = new CRMContactsPage(driver);
				fc.utobj().selectDropDown(driver, pobj.contactType, contactType);
				fc.utobj().selectDropDown(driver, pobj.title, title);
				fc.utobj().sendKeys(driver, pobj.contactFirstName, firstName);
				fc.utobj().sendKeys(driver, pobj.contactLastName, lastName);
				fc.utobj().sendKeys(driver, pobj.suffix, "suffixTest");
				fc.utobj().sendKeys(driver, pobj.position, "TestJobTitle");
				fc.utobj().sendKeys(driver, pobj.address, "TestAddress");
				fc.utobj().sendKeys(driver, pobj.city, "TestCity");
				fc.utobj().selectDropDown(driver, pobj.country, "USA");
				fc.utobj().selectDropDown(driver, pobj.state, "Alabama");
				fc.utobj().selectDropDown(driver, pobj.primaryContactMethodSelect, primaryCMethod);
				fc.utobj().sendKeys(driver, pobj.bestTimeToContact, "12:00 PM EST");
				fc.utobj().sendKeys(driver, pobj.zipcode, "12345");
				fc.utobj().sendKeys(driver, pobj.phoneNumbers, "1236547896");
				fc.utobj().sendKeys(driver, pobj.extn, "12");
				fc.utobj().sendKeys(driver, pobj.faxNumbers, "1263985487");
				fc.utobj().sendKeys(driver, pobj.mobileNumbers, "8896724441");
				fc.utobj().sendKeys(driver, pobj.emailIds, email);
				fc.utobj().sendKeys(driver, pobj.alternateEmail, "test@gmail.com");
				fc.utobj().sendKeys(driver, pobj.birthdate, "11/24/1980");
				fc.utobj().sendKeys(driver, pobj.anniversarydate, "11/24/1999");
				fc.utobj().selectDropDown(driver, pobj.rating, "Cold");

				if (assignTo.equalsIgnoreCase("Corporate")) {

					if (!fc.utobj().isSelected(driver, pobj.assignToCorporate)) {
						fc.utobj().clickElement(driver, pobj.assignToCorporate);
					}

					fc.utobj().selectDropDown(driver, pobj.selectCorporateUserContact, userName);

				} else if (assignTo.equalsIgnoreCase("Regional")) {

					if (!fc.utobj().isSelected(driver, pobj.assignToRegional)) {
						fc.utobj().clickElement(driver, pobj.assignToRegional);
					}

					fc.utobj().selectDropDown(driver, pobj.selectAreaRegion, regionName);
					fc.utobj().selectDropDown(driver, pobj.selectRegionalUser, userName);

				} else if (assignTo.equalsIgnoreCase("Franchise")) {

					if (!fc.utobj().isSelected(driver, pobj.assignToFranchise)) {
						fc.utobj().clickElement(driver, pobj.assignToFranchise);
					}

					fc.utobj().selectDropDown(driver, pobj.selectFranchiseId, franchiseId);
					fc.utobj().selectDropDown(driver, pobj.selectFranchiseUser, userName);

				}
				fc.utobj().sendKeys(driver, pobj.comments, "Test Comment");
				fc.utobj().sleep(3000);
				fc.utobj().sendKeys(driver, pobj.accountName, accountName);

				// fc.utobj().clickElement(driver,
				// fc.utobj().getElementByXpath(driver,".//*[@id='customizedAjaxSearch']/div"));
				fc.utobj().sleep(2000);
				fc.utobj().clickElement(driver, pobj.accountSearchBtn);
				fc.commonMethods().switch_cboxIframe_frameId(driver);
				fc.utobj().clickLink(driver, accountName);
				fc.utobj().switchFrameToDefault(driver);
				fc.utobj().clickElement(driver, pobj.saveBtn);

				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException("Not able to Add Contact At CRM > Contacts");

		}
	}

	/* Anukaran Mishra292 */

	@Test(groups = { "crmmodule", "crm" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Add Contact Type At Admin> Modify Contact Type At Admin > Verify the Contact Type Visibility at Contact Primary Info", testCaseId = "TC_292_Verify_Contact_Type_Modification")
	public void verifyAddModifyContactType() throws IOException, Exception, ParserConfigurationException, SAXException {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			CRMContactsPage pobj = new CRMContactsPage(driver);

			fc.utobj().printTestStep("Naviaget To Admin Users Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corpTest = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			String emailId = "crmautomation@staffex.com";
			CorporateUser corpUser = new CorporateUser();
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corpUser.setEmail(emailId);
			corpUser = corpTest.createDefaultUser(driver, corpUser);

			fc.home_page().logout(driver);
			fc.loginpage().loginWithParameter(driver, corpUser.getUserName(), corpUser.getPassword());
			fc.utobj().printTestStep("Navigate To Admin > CRM > Contact Type Configuration");
			fc.utobj().printTestStep("Add Contact Type");
			AdminCRMContactTypeConfigurationPageTest contatcTypePage = new AdminCRMContactTypeConfigurationPageTest();
			String contactType = fc.utobj().generateTestData(dataSet.get("contactType"));
			contatcTypePage.addContactType(driver, contactType);

			fc.utobj().actionImgOption(driver, contactType, "Modify");
			fc.commonMethods().switch_cboxIframe_frameId(driver);

			fc.utobj().sendKeys(driver, pobj.ContactTypeName, contactType);
			fc.utobj().clickElement(driver, pobj.ModifyBtn);
			Thread.sleep(5000);
			fc.utobj().switchFrameToDefault(driver);
			// fc.utobj().printTestStep(testCaseId, "Navigate To Admin > CRM >
			// Contact Type Configuration");
			// fc.utobj().printTestStep(testCaseId, "Add Contact Type");
			// AdminCRMContactTypeConfigurationPageTest contatcTypePage = new
			// AdminCRMContactTypeConfigurationPageTest();
			// String contactType =
			// fc.utobj().generateTestData(dataSet.get("contactType"));

			fc.utobj().printTestStep("Verify Add Contact Type with same name");
			AdminCRMContactTypeConfigurationPage npobj = new AdminCRMContactTypeConfigurationPage(driver);
			fc.crm().crm_common().adminCRMContactTypeConfigurationLnk(driver);
			fc.utobj().clickElement(driver, npobj.addContactType);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().sendKeys(driver, pobj.AddcontactType, contactType);
			fc.utobj().clickElement(driver, pobj.addBtn);
			fc.utobj().acceptAlertBox(driver);
			fc.utobj().printTestStep("Verified Add Contact Type with same name");
			//
			fc.utobj().clickElement(driver, pobj.closeBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Navigate To Admin > CRM > Contact Type Configuration");
			fc.utobj().printTestStep("Add Contact Type");
			String contactType1 = fc.utobj().generateTestData(dataSet.get("contactType"));
			contatcTypePage.addContactType(driver, contactType1);
			Thread.sleep(2000);
			fc.utobj().actionImgOption(driver, contactType, "Modify");
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().sendKeys(driver, pobj.ContactTypeName, contactType1);
			fc.utobj().clickElement(driver, pobj.ModifyBtn);
			fc.utobj().acceptAlertBox(driver);
			fc.utobj().printTestStep("Verified Modify Contact Type with same name");
			fc.utobj().clickElement(driver, pobj.closeBtn);

			String firstName = fc.utobj().generateTestData(dataSet.get("firstName"));
			String lastName = fc.utobj().generateTestData(dataSet.get("lastName"));
			String primaryCMethod = dataSet.get("primaryContactMethod");
			String assignTo = "Corporate";
			String regionName = "";
			String franchiseId = "";
			String title = dataSet.get("title");

			fc.utobj().printTestStep("Navigate To CRM > Contacts > Contactsss Summary Page");
			fc.utobj().printTestStep("Add Contact");
			fc.crm().crm_common().CRMContactsLnk(driver);
			fc.utobj().clickElement(driver, pobj.addNew);
			fc.utobj().clickElement(driver, pobj.contactLnk);
			// fc.utobj().printTestStep(testCaseId,"Adding Contact");
			addContactGeneric(driver, contactType, firstName, lastName, primaryCMethod, assignTo,
					corpUser.getuserFullName(), regionName, franchiseId, title, emailId, config);

			fc.utobj().clickElement(driver, pobj.contactsLinks);
			contactsFilter(driver, contactType);
			fc.utobj().printTestStep("Verifying Contact Type Visibility");
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//a[contains(text () , '" + firstName + " " + lastName + "')]"));

			boolean isContactTypePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//*[contains(text () ,'Contact Type')]/ancestor::tr/td[contains(text () , '" + contactType
							+ "')]");

			if (isContactTypePresent == false) {
				fc.utobj().throwsException("was not able to verify Contact Type");
			}

			fc.utobj().printTestStep("Verify delete for contact type associated with contact name");
			fc.crm().crm_common().adminCRMContactTypeConfigurationLnk(driver);
			try {
				fc.utobj().throwsSkipException(
						"Failed!! Contact type was getting deleted, even though it is associated with a contact");
				fc.utobj().actionImgOption(driver, contactType, "Delete");
			} catch (Exception ee) {
			}
			/*
			 * fc.crm().crm_common().adminCRMContactTypeConfigurationLnk(
			 * driver); fc.utobj().actionImgOption(driver, contactType,
			 * "Delete"); fc.utobj().acceptAlertBox(driver);
			 */
			fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	/* Anukaran Mishra293 */

	@Test(groups = { "crmmodule", "crm" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Add Contact Status At Admin> Modify Contact Status At Admin > Verify the Contact Ststus at Contact Primary Info", testCaseId = "TC_293_Verify_Contact_Status_Modification")
	public void verifyAddModifyContactStatus()
			throws IOException, Exception, ParserConfigurationException, SAXException {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			CRMContactsPage pobj = new CRMContactsPage(driver);
			AdminCRMConfigureStatusPageTest nnpoj = new AdminCRMConfigureStatusPageTest();

			fc.utobj().printTestStep("Naviaget To Admin Users Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corpTest = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			//String emailId = "crmautomation@staffex.com";
			String emailId=fc.utobj().generateTestData("crmautomation")+"@staffex.com";
			CorporateUser corpUser = new CorporateUser();
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corpUser.setEmail(emailId);
			corpUser = corpTest.createDefaultUser(driver, corpUser);
			fc.home_page().logout(driver);
			fc.loginpage().loginWithParameter(driver, corpUser.getUserName(), corpUser.getPassword());
			fc.utobj().printTestStep("Navigate To Admin CRM > Configure Status Page");
			fc.utobj().printTestStep("Add Contact Status");
			AdminCRMConfigureStatusPageTest statusPage = new AdminCRMConfigureStatusPageTest();
			String statusName = fc.utobj().generateTestData(dataSet.get("status"));
			statusPage.addStatus01(driver, statusName);
			nnpoj.actionImgOption(driver, statusName, "Modify");
			fc.commonMethods().switch_cboxIframe_frameId(driver);

			fc.utobj().sendKeys(driver, pobj.ContactStatusName, statusName);
			fc.utobj().clickElement(driver, pobj.ModifyStatusBtn);
			Thread.sleep(5000);

			AdminCRMConfigureStatusPageUI npobj = new AdminCRMConfigureStatusPageUI(driver);
			fc.crm().crm_common().adminCRMConfigureStatusLnk(driver);
			fc.utobj().clickElement(driver, npobj.contacts);
			fc.utobj().clickElement(driver, npobj.addStatus);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().sendKeys(driver, npobj.statusTxBx, statusName);
			fc.utobj().clickElement(driver, pobj.addBtn);
			fc.utobj().acceptAlertBox(driver);
			fc.utobj().printTestStep("Verified Add Contact Status with same name");
			//
			fc.utobj().clickElement(driver, npobj.CancelStatusBtn);
			fc.utobj().switchFrameToDefault(driver);
			// fc.utobj().switchFrameToDefault(driver);

			// statusPage.addStatus01(driver, statusName, config);

			fc.utobj().printTestStep("Navigate To Admin > CRM > Contact Type Configuration");
			fc.utobj().printTestStep("Add Contact Status");
			String statusName1 = fc.utobj().generateTestData(dataSet.get("status"));
			statusPage.addStatus01(driver, statusName1);
			nnpoj.actionImgOption(driver, statusName1, "Modify");
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().sendKeys(driver, pobj.ContactStatusName, statusName);
			fc.utobj().clickElement(driver, pobj.ModifyStatusBtn);
			fc.utobj().acceptAlertBox(driver);
			fc.utobj().printTestStep("Verified Modify Contact Status with same name");
			fc.utobj().clickElement(driver, npobj.CancelStatusBtn);

			fc.utobj().printTestStep("Navigate To Admin > CRM > Contact Type Configuration");
			fc.utobj().printTestStep("Add Contact Type");
			AdminCRMContactTypeConfigurationPageTest contatcTypePage = new AdminCRMContactTypeConfigurationPageTest();
			String contactType = fc.utobj().generateTestData(dataSet.get("contactType"));
			contatcTypePage.addContactType(driver, contactType);

			String firstName = fc.utobj().generateTestData(dataSet.get("firstName"));
			String lastName = fc.utobj().generateTestData(dataSet.get("lastName"));
			String primaryCMethod = dataSet.get("primaryContactMethod");
			String assignTo = "Corporate";
			String regionName = "";
			String franchiseId = "";
			String title = dataSet.get("title");

			fc.utobj().printTestStep("Navigate To CRM > Contacts > Contactsss Summary Page");
			fc.utobj().printTestStep("Add Contact");
			fc.crm().crm_common().CRMContactsLnk(driver);
			fc.utobj().clickElement(driver, pobj.addNew);
			fc.utobj().clickElement(driver, pobj.contactLnk);
			// fc.utobj().printTestStep(testCaseId,"Adding Contact");
			addContactGeneric(driver, contactType, firstName, lastName, primaryCMethod, assignTo,
					corpUser.getuserFullName(), regionName, franchiseId, title, emailId, config);

			fc.utobj().clickElement(driver, pobj.contactsLinks);
			contactsFilter(driver, contactType);
			fc.utobj().printTestStep("Change Status");
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//a[contains(text () ,'" + firstName
					+ " " + lastName + "')]/ancestor::tr/td/input[@name='checkb']"));
			fc.utobj().clickElement(driver, pobj.changeStatusBtmBtn);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().selectDropDown(driver, pobj.statusChangeTo, statusName);
			fc.utobj().sendKeys(driver, pobj.remarksAtMerge, "Test Remarks");
			fc.utobj().clickElement(driver, pobj.changeBtn);
			fc.utobj().clickElement(driver, pobj.closeBtn);

			// Delete functionality not available
			fc.crm().crm_common().adminCRMConfigureStatusLnk(driver);
			fc.utobj().clickElement(driver, npobj.contacts);
			try {
				fc.utobj().actionImgOption(driver, statusName1, "Delete");
				fc.utobj().throwsSkipException(
						"Failed!! Status was getting deleted, even though it is associated with a contact");
			} catch (Exception ec) {
				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	/* Anukaran Mishra */

	@Test(groups = { "crmmodule", "crm" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Add Contact Medium At Admin> Modify Contact Medium At Admin > Verify the Contact Medium at Contact Primary Info", testCaseId = "TC_294_Verify_Contact_Medium_Modification")
	public void verifyAddModifyContactMedium()
			throws IOException, Exception, ParserConfigurationException, SAXException {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			CRMContactsPage npobj = new CRMContactsPage(driver);
			AdminCRMConfigureMediumPage pobj = new AdminCRMConfigureMediumPage(driver);
			AdminCRMConfigureMediumPageTest nnpoj = new AdminCRMConfigureMediumPageTest();

			fc.utobj().printTestStep("Naviaget To Admin Users Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corpTest = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			//String emailId = "crmautomation@staffex.com";
			String emailId=fc.utobj().generateTestData("crmautomation")+"@staffex.com";
			CorporateUser corpUser = new CorporateUser();
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corpUser.setEmail(emailId);
			corpUser = corpTest.createDefaultUser(driver, corpUser);
			fc.home_page().logout(driver);
			fc.loginpage().loginWithParameter(driver, corpUser.getUserName(), corpUser.getPassword());

			String medium = fc.utobj().generateTestData(dataSet.get("medium"));
			String description = fc.utobj().generateTestData(dataSet.get("description"));
			fc.utobj().printTestStep("Navigating to Admin > CRM > Configure Medium");
			fc.utobj().printTestStep("Add Medium");
			nnpoj.addMedium(driver, medium, description);

			fc.utobj().printTestStep("Modify Medium");
			//
			fc.utobj().actionImgOption(driver, medium, "Modify");
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().sendKeys(driver, pobj.medium, medium);
			fc.utobj().sendKeys(driver, pobj.description, description);
			fc.utobj().clickElement(driver, pobj.saveBtn);
			fc.utobj().clickElement(driver, pobj.okBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().clickElement(driver, pobj.addMedium);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().sendKeys(driver, pobj.medium, medium);
			fc.utobj().sendKeys(driver, pobj.description, description);
			fc.utobj().clickElement(driver, pobj.addBtn);
			fc.utobj().acceptAlertBox(driver);
			fc.utobj().printTestStep("Verified Add Contact Medium with same name");
			//
			fc.utobj().clickElement(driver, pobj.CloseBtn);
			//
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Navigating to Admin > CRM > Configure Medium");
			fc.utobj().printTestStep("Add Medium");
			String medium1 = fc.utobj().generateTestData(dataSet.get("medium"));
			String description1 = fc.utobj().generateTestData(dataSet.get("description"));
			nnpoj.addMedium(driver, medium1, description1);
			fc.utobj().actionImgOption(driver, medium, "Modify");
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().sendKeys(driver, pobj.medium, medium1);
			fc.utobj().sendKeys(driver, pobj.description, description1);
			fc.utobj().clickElement(driver, pobj.saveBtn);
			fc.utobj().acceptAlertBox(driver);
			fc.utobj().printTestStep("Verified Add Contact Medium with same name");
			//
			fc.utobj().clickElement(driver, pobj.CloseBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Navigate To Admin > CRM > Contact Type Configuration");
			fc.utobj().printTestStep("Add Contact Type");
			AdminCRMContactTypeConfigurationPageTest contatcTypePage = new AdminCRMContactTypeConfigurationPageTest();
			String contactType = fc.utobj().generateTestData(dataSet.get("contactType"));
			contatcTypePage.addContactType(driver, contactType);
			String firstName = fc.utobj().generateTestData(dataSet.get("firstName"));
			String lastName = fc.utobj().generateTestData(dataSet.get("lastName"));
			// String userName=firstName+" "+lastName;
			String primaryCMethod = dataSet.get("primaryContactMethod");
			String assignTo = "Corporate";
			String regionName = "";
			String franchiseId = "";
			String title = dataSet.get("title");

			fc.utobj().printTestStep("Navigate To CRM > Contacts > Contactsss Summary Page");
			fc.utobj().printTestStep("Add Contact");
			fc.crm().crm_common().CRMContactsLnk(driver);
			fc.utobj().clickElement(driver, npobj.addNew);
			fc.utobj().clickElement(driver, npobj.contactLnk);
			// fc.utobj().printTestStep(testCaseId,"Adding Contact");
			String contactName = firstName + " " + lastName;
			/*
			 * addContactGeneric(driver, "Contacts", firstName, lastName,
			 * primaryCMethod,assignTo, userName, regionName, franchiseId,
			 * title, emailId, config);
			 */

			fc.utobj().selectDropDown(driver, npobj.contactType, contactType);
			fc.utobj().selectDropDown(driver, npobj.title, title);
			fc.utobj().sendKeys(driver, npobj.contactFirstName, firstName);
			fc.utobj().sendKeys(driver, npobj.contactLastName, lastName);
			fc.utobj().sendKeys(driver, npobj.suffix, "suffixTest");
			fc.utobj().sendKeys(driver, npobj.position, "TestJobTitle");
			fc.utobj().sendKeys(driver, npobj.address, "TestAddress");
			fc.utobj().sendKeys(driver, npobj.city, "TestCity");
			fc.utobj().selectDropDown(driver, npobj.country, "USA");
			fc.utobj().selectDropDown(driver, npobj.state, "Alabama");
			fc.utobj().selectDropDown(driver, npobj.primaryContactMethodSelect, primaryCMethod);
			fc.utobj().sendKeys(driver, npobj.bestTimeToContact, "12:00 PM EST");
			fc.utobj().sendKeys(driver, npobj.zipcode, "12345");
			fc.utobj().sendKeys(driver, npobj.phoneNumbers, "1236547896");
			fc.utobj().sendKeys(driver, npobj.extn, "12");
			fc.utobj().sendKeys(driver, npobj.faxNumbers, "1263985487");
			fc.utobj().sendKeys(driver, npobj.mobileNumbers, "8896724441");
			fc.utobj().sendKeys(driver, npobj.emailIds, emailId);
			fc.utobj().sendKeys(driver, npobj.alternateEmail, "test@gmail.com");
			fc.utobj().sendKeys(driver, npobj.birthdate, "11/24/1980");
			fc.utobj().sendKeys(driver, npobj.anniversarydate, "11/24/1999");
			fc.utobj().selectDropDown(driver, npobj.contactMedium, medium);
			if (assignTo.equalsIgnoreCase("Corporate")) {

				if (!fc.utobj().isSelected(driver, npobj.assignToCorporate)) {
					fc.utobj().clickElement(driver, npobj.assignToCorporate);
				}

				fc.utobj().selectDropDown(driver, npobj.selectCorporateUserContact, corpUser.getuserFullName());

			} else if (assignTo.equalsIgnoreCase("Regional")) {

				if (!fc.utobj().isSelected(driver, npobj.assignToRegional)) {
					fc.utobj().clickElement(driver, npobj.assignToRegional);
				}

				fc.utobj().selectDropDown(driver, npobj.selectAreaRegion, regionName);
				fc.utobj().selectDropDown(driver, npobj.selectRegionalUser, corpUser.getuserFullName());

			} else if (assignTo.equalsIgnoreCase("Franchise")) {

				if (!fc.utobj().isSelected(driver, npobj.assignToFranchise)) {
					fc.utobj().clickElement(driver, npobj.assignToFranchise);
				}

				fc.utobj().selectDropDown(driver, npobj.selectFranchiseId, franchiseId);
				fc.utobj().selectDropDown(driver, npobj.selectFranchiseUser, corpUser.getuserFullName());

			}
			fc.utobj().sendKeys(driver, npobj.comments, "Test Comment");
			fc.utobj().clickElement(driver, npobj.saveBtn);

			fc.utobj().clickElement(driver, npobj.contactsLinks);
			contactsFilter(driver, contactType);
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//a[contains(text () , '" + contactName + "')]"));
			boolean isContactMediumPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//*[contains(text () ,'Contact Medium')]/ancestor::tr/td[contains(text () , '" + medium + "')]");
			if (isContactMediumPresent == false) {
				fc.utobj().throwsException("was not able to verify Contact Medium Field");
			}
			try {
				fc.utobj().actionImgOption(driver, medium, "Delete");
				fc.utobj()
						.throwsSkipException("Failed Contact medium cannot be deleted , if associated with a contact");
			} catch (Exception ee) {
			}

			fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	/* Anukaran Mishra */

	@Test(groups = { "crmmodule", "crm"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Add Contact Source At Admin> Modify Contact Source At Admin > Verify the Contact Source at Contact Primary Info", testCaseId = "TC_295_Verify_Contact_Source_Modification")
	public void verifyAddModifyContactSource()
			throws IOException, Exception, ParserConfigurationException, SAXException {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			CRMContactsPage npobj = new CRMContactsPage(driver);
			AdminCRMSourceSummaryPageTest nnpobj = new AdminCRMSourceSummaryPageTest();
			AdminCRMSourceSummaryPage pobj = new AdminCRMSourceSummaryPage(driver);

			fc.utobj().printTestStep("Naviaget To Admin Users Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corpTest = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			String emailId = "crmautomation@staffex.com";
			CorporateUser corpUser = new CorporateUser();
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corpUser.setEmail(emailId);
			corpUser = corpTest.createDefaultUser(driver, corpUser);
			fc.home_page().logout(driver);
			fc.loginpage().loginWithParameter(driver, corpUser.getUserName(), corpUser.getPassword());
			fc.utobj().printTestStep("Navigate To Admin > CRM > Source Summary");
			fc.utobj().printTestStep("Add Source");
			String description = fc.utobj().generateTestData(dataSet.get("description"));
			String source = fc.utobj().generateTestData(dataSet.get("source"));
			String displayOnWebPageText = fc.utobj().generateTestData(dataSet.get("text"));
			String sourceDetails = fc.utobj().generateTestData(dataSet.get("sourceDetails"));
			nnpobj.addSource(driver, source, description, displayOnWebPageText);

			fc.utobj().webDriverWait(driver, ".//*[contains(text () ,'" + source + "')]");

			fc.utobj().printTestStep("Modify Source");
			fc.utobj().actionImgOption(driver, source, "Modify");
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			String source1 = fc.utobj().generateTestData(dataSet.get("source"));
			fc.utobj().sendKeys(driver, pobj.source, source1);
			String description1 = fc.utobj().generateTestData(dataSet.get("description"));
			fc.utobj().sendKeys(driver, pobj.description, description1);
			if (!fc.utobj().isSelected(driver,pobj.displayOnWebPage)) {
				fc.utobj().clickElement(driver, pobj.displayOnWebPage);
			}
			displayOnWebPageText = fc.utobj().generateTestData(dataSet.get("text"));
			fc.utobj().sendKeys(driver, pobj.displayOnWebPageTxBx, displayOnWebPageText);
			fc.utobj().clickElement(driver, pobj.saveBtn);

			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().webDriverWait(driver, ".//*[contains(text () ,'" + source1 + "')]");
			fc.utobj().printTestStep("Verify Modify Source");
			WebElement element = fc.utobj().getElementByXpath(driver, ".//*[contains(text () ,'" + source1 + "')]");
			fc.utobj().moveToElement(driver, element);
			WebElement element1 = fc.utobj().getElementByXpath(driver, ".//*[contains(text () , '" + source1
					+ "')]/ancestor::tr/td[contains(text () ,'" + description1 + "')]");
			if (!element.isDisplayed() && !element1.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify modify Source and source description");
			}

			fc.utobj().clickElement(driver, pobj.sourceSummaryTab);
			fc.utobj().clickElement(driver, pobj.addSource);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			// String source=fc.utobj().generateTestData(dataSet.get("source"));
			fc.utobj().sendKeys(driver, pobj.source, source1);
			// String
			// description=fc.utobj().generateTestData(dataSet.get("description"));
			fc.utobj().sendKeys(driver, pobj.description, description1);
			if (!fc.utobj().isSelected(driver,pobj.displayOnWebPage)) {
				fc.utobj().clickElement(driver, pobj.displayOnWebPage);
			}
			// String
			// displayOnWebPageText=fc.utobj().generateTestData(dataSet.get("text"));
			fc.utobj().sendKeys(driver, pobj.displayOnWebPageTxBx, displayOnWebPageText);
			fc.utobj().clickElement(driver, pobj.addBtn);
			fc.utobj().acceptAlertBox(driver);
			fc.utobj().clickElement(driver, pobj.closeBtn);
			fc.utobj().switchFrameToDefault(driver);
			fc.utobj().printTestStep("Verified Add Contact Source with same name");

			nnpobj.addSourceDetails(driver, source1, sourceDetails, displayOnWebPageText, description1);

			fc.utobj().printTestStep("Modify Source Details");
			fc.utobj().refresh(driver);
			fc.utobj().sleep(5000);
			fc.utobj().actionImgOption(driver, sourceDetails, "Modify");
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			String sourceDetails1 = fc.utobj().generateTestData(dataSet.get("sourceDetails"));
			fc.utobj().sendKeys(driver, pobj.sourceDetails, sourceDetails1);

			if (!fc.utobj().isSelected(driver,pobj.includePage)) {
				fc.utobj().clickElement(driver, pobj.includePage);
			}
			displayOnWebPageText = fc.utobj().generateTestData(dataSet.get("text"));
			fc.utobj().sendKeys(driver, pobj.cmSource3NameDisplay, displayOnWebPageText);
			String description2 = fc.utobj().generateTestData(dataSet.get("description"));
			fc.utobj().sendKeys(driver, pobj.descriptionRemarks, description2);
			fc.utobj().clickElement(driver, pobj.saveBtn);

			fc.utobj().webDriverWait(driver, ".//*[contains(text () ,'" + sourceDetails1 + "')]");
			fc.utobj().printTestStep("Verify Modify Source Details");
			boolean isTextPresent = fc.utobj().assertPageSource(driver, sourceDetails1);
			if (isTextPresent == false) {
				fc.utobj().throwsException("was not able to Modify Source details");
			}

			//
			fc.utobj().webDriverWait(driver, ".//*[contains(text () ,'" + source1 + "')]");
			fc.utobj().clickElement(driver, pobj.addSourceDetails);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().selectDropDown(driver, pobj.sourceSelect, source1);
			fc.utobj().sendKeys(driver, pobj.sourceDetails, sourceDetails1);
			if (!fc.utobj().isSelected(driver,pobj.includePage)) {
				fc.utobj().clickElement(driver, pobj.includePage);
			}
			fc.utobj().sendKeys(driver, pobj.cmSource3NameDisplay, displayOnWebPageText);
			fc.utobj().sendKeys(driver, pobj.descriptionRemarks, description1);
			fc.utobj().clickElement(driver, pobj.addBtn);
			fc.utobj().acceptAlertBox(driver);
			fc.utobj().clickElement(driver, pobj.closeBtn);
			fc.utobj().switchFrameToDefault(driver);
			fc.utobj().printTestStep("Verified Add Contact Source with same name");
			fc.utobj().sleep(5000);
			fc.utobj().refresh(driver);
			fc.utobj().printTestStep("Navigate To Admin > CRM > Contact Type Configuration");
			fc.utobj().printTestStep("Add Contact Type");
			AdminCRMContactTypeConfigurationPageTest contatcTypePage = new AdminCRMContactTypeConfigurationPageTest();
			String contactType = fc.utobj().generateTestData(dataSet.get("contactType"));
			contatcTypePage.addContactType(driver, contactType);
			String firstName = fc.utobj().generateTestData(dataSet.get("firstName"));
			String lastName = fc.utobj().generateTestData(dataSet.get("lastName"));
			// String userName=firstName+" "+lastName;
			String primaryCMethod = dataSet.get("primaryContactMethod");
			String assignTo = "Corporate";
			String regionName = "";
			String franchiseId = "";
			String title = dataSet.get("title");

			fc.utobj().printTestStep("Navigate To CRM > Contacts > Contactsss Summary Page");
			fc.utobj().printTestStep("Add Contact");
			fc.crm().crm_common().CRMContactsLnk(driver);
			fc.utobj().clickElement(driver, npobj.addNew);
			fc.utobj().clickElement(driver, npobj.contactLnk);
			// fc.utobj().printTestStep(testCaseId,"Adding Contact");
			String contactName = firstName + " " + lastName;

			fc.utobj().selectDropDown(driver, npobj.contactType, contactType);
			fc.utobj().selectDropDown(driver, npobj.title, title);
			fc.utobj().sendKeys(driver, npobj.contactFirstName, firstName);
			fc.utobj().sendKeys(driver, npobj.contactLastName, lastName);
			fc.utobj().sendKeys(driver, npobj.suffix, "suffixTest");
			fc.utobj().sendKeys(driver, npobj.position, "TestJobTitle");
			fc.utobj().sendKeys(driver, npobj.address, "TestAddress");
			fc.utobj().sendKeys(driver, npobj.city, "TestCity");
			fc.utobj().selectDropDown(driver, npobj.country, "USA");
			fc.utobj().selectDropDown(driver, npobj.state, "Alabama");
			fc.utobj().selectDropDown(driver, npobj.primaryContactMethodSelect, primaryCMethod);
			fc.utobj().sendKeys(driver, npobj.bestTimeToContact, "12:00 PM EST");
			fc.utobj().sendKeys(driver, npobj.zipcode, "12345");
			fc.utobj().sendKeys(driver, npobj.phoneNumbers, "1236547896");
			fc.utobj().sendKeys(driver, npobj.extn, "12");
			fc.utobj().sendKeys(driver, npobj.faxNumbers, "1263985487");
			fc.utobj().sendKeys(driver, npobj.mobileNumbers, "8896724441");
			fc.utobj().sendKeys(driver, npobj.emailIds, emailId);
			fc.utobj().sendKeys(driver, npobj.alternateEmail, "test@gmail.com");
			fc.utobj().sendKeys(driver, npobj.birthdate, "11/24/1980");
			fc.utobj().sendKeys(driver, npobj.anniversarydate, "11/24/1999");
			fc.utobj().selectDropDown(driver, npobj.leadSource, source1);
			fc.utobj().selectDropDown(driver, npobj.leadSourceDetails, sourceDetails1);
			if (assignTo.equalsIgnoreCase("Corporate")) {

				if (!fc.utobj().isSelected(driver, npobj.assignToCorporate)) {
					fc.utobj().clickElement(driver, npobj.assignToCorporate);
				}

				fc.utobj().selectDropDown(driver, npobj.selectCorporateUserContact, corpUser.getuserFullName());

			} else if (assignTo.equalsIgnoreCase("Regional")) {

				if (!fc.utobj().isSelected(driver, npobj.assignToRegional)) {
					fc.utobj().clickElement(driver, npobj.assignToRegional);
				}

				fc.utobj().selectDropDown(driver, npobj.selectAreaRegion, regionName);
				fc.utobj().selectDropDown(driver, npobj.selectRegionalUser, corpUser.getuserFullName());

			} else if (assignTo.equalsIgnoreCase("Franchise")) {

				if (!fc.utobj().isSelected(driver, npobj.assignToFranchise)) {
					fc.utobj().clickElement(driver, npobj.assignToFranchise);
				}

				fc.utobj().selectDropDown(driver, npobj.selectFranchiseId, franchiseId);
				fc.utobj().selectDropDown(driver, npobj.selectFranchiseUser, corpUser.getuserFullName());

			}
			fc.utobj().sendKeys(driver, npobj.comments, "Test Comment");
			fc.utobj().clickElement(driver, npobj.saveBtn);

			fc.utobj().clickElement(driver, npobj.contactsLinks);
			contactsFilter(driver, contactType);
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//a[contains(text () , '" + contactName + "')]"));
			/*boolean isContactSourcePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//*[contains(text () ,'How did you hear about us?(Contact Source)')]/ancestor::tr/td[contains(text () , '"
							+ source1 + "')]");*/
			
			boolean isContactSourcePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,".//*[contains(text () ,'How did you hear about us ? (Contact Source) : ')]/ancestor::tr/td[contains(text(),'"+source1+"')]");
			
			if (isContactSourcePresent == false) {
				fc.utobj().throwsException("was not able to verify Contact Source Field");
			}
			boolean isContactSourceDetailsPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//*[contains(text () ,'" + sourceDetails1 + "')]");
			if (isContactSourceDetailsPresent == false) {
				fc.utobj().throwsException("was not able to verify Contact Source Field");
			}

			fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}

	}

	@Test(groups = "crmmodule")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Log a task against the contact>Verify the task at calendor", testCaseId = "TC_298_Verify_LogATask_At_Calendor")
	public void verifyLogATaskAtCalendor() throws IOException, Exception, ParserConfigurationException, SAXException {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			CRMContactsPage pobj = new CRMContactsPage(driver);
			fc.utobj().printTestStep("Naviaget To Admin Users Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corpTest = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			String emailId = "crmautomation@staffex.com";
			CorporateUser corpUser = new CorporateUser();
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corpUser.setEmail(emailId);
			corpUser = corpTest.createDefaultUser(driver, corpUser);
			fc.home_page().logout(driver);
			fc.loginpage().loginWithParameter(driver, corpUser.getUserName(), corpUser.getPassword());

			fc.utobj().printTestStep("Navigate To Admin > CRM > Contact Type Configuration");
			fc.utobj().printTestStep("Add Contact Type");
			AdminCRMContactTypeConfigurationPageTest contatcTypePage = new AdminCRMContactTypeConfigurationPageTest();
			String contactType = fc.utobj().generateTestData(dataSet.get("contactType"));
			contatcTypePage.addContactType(driver, contactType);
			String firstName = fc.utobj().generateTestData(dataSet.get("firstName"));
			String lastName = fc.utobj().generateTestData(dataSet.get("lastName"));
			String primaryCMethod = dataSet.get("primaryContactMethod");
			String assignTo = "Corporate";
			String regionName = "";
			String franchiseId = "";
			String title = dataSet.get("title");

			String contactName = firstName + " " + lastName;

			fc.utobj().printTestStep("Navigate To CRM > Contacts > Contactsss Summary Page");
			fc.utobj().printTestStep("Add Contact");
			fc.crm().crm_common().CRMContactsLnk(driver);
			fc.utobj().clickElement(driver, pobj.addNew);
			fc.utobj().clickElement(driver, pobj.contactLnk);
			// fc.utobj().printTestStep(testCaseId,"Adding Contact");
			addContactGeneric(driver, contactType, firstName, lastName, primaryCMethod, assignTo,
					corpUser.getuserFullName(), regionName, franchiseId, title, emailId, config);

			fc.utobj().clickElement(driver, pobj.contactsLinks);
			contactsFilter(driver, contactType);
			// log a task
			String taskName = fc.utobj().generateTestData(dataSet.get("TaskName"));
			fc.utobj().actionImgOption(driver, contactName, "Log a Task");
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			// fc.commonMethods().switch_cboxIframe_frameId(driver);
			if (!fc.utobj().isSelected(driver,pobj.radioOwner)) {
				fc.utobj().clickElement(driver, pobj.radioOwner);
			}
			fc.utobj().selectDropDown(driver, pobj.LogATaskStatus, "Work In Progress");
			fc.utobj().sendKeys(driver, pobj.LogATaskStatusName, taskName);
			if (!fc.utobj().isSelected(driver,pobj.calendarTaskCheckBox)) {
				fc.utobj().clickElement(driver, pobj.calendarTaskCheckBox);
			}
			fc.utobj().clickElement(driver, pobj.AddTaskBtn);

			// verify in calendor
			fc.utobj().clickElement(driver, pobj.calendarLink);
			boolean isTextPresent = fc.utobj().assertPageSource(driver, taskName);
			if (isTextPresent == false) {
				fc.utobj().throwsException("was not able to verify task at Calendor field");
			}
			fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	// Ravi Kumar

	@Test(groups = "crmmodulewaitFormGen")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Document At CRM > Mangage Form Generator >New Tabular Tab", testCaseId = "TC_303_Verify_Document_Tab_At_CRM")
	public void verifyNewDocumentTabCRM() throws IOException, Exception, ParserConfigurationException, SAXException {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		AdminInfoMgrCommonMethods adminCommonMethods = fc.adminpage().adminInfoMgr();
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		// CRM_CorporateUser crmCU= new CRM_CorporateUser();

		try {
			driver = fc.loginpage().login(driver);
			CRMLeadsPage pobj = new CRMLeadsPage(driver);

			fc.utobj().printTestStep("Navigate To Admin Users Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corpTest = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			String CuserName = fc.utobj().generateTestData(dataSet.get("userName"));

			String emailId = "crmautomation@staffex.com";

			CorporateUser corpUser = new CorporateUser();
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corpUser.setEmail(emailId);
			corpUser = corpTest.createDefaultUser(driver, corpUser);
			fc.home_page().logout(driver);
			fc.loginpage().loginWithParameter(driver, CuserName, corpUser.getPassword());

			fc.utobj().printTestStep("Navigate To Admin > CRM > Manage Form Generator");
			fc.utobj().printTestStep("Add New Tab");
			// fc.crm().crm_common().CRMLeadsLnk( driver);
			fc.infomgr().infomgr_common().AdminFIMManageFormGenerator(driver);
			AdminInfoMgrManageFormGeneratorPage manageFormGeneratorPage = new AdminInfoMgrManageFormGeneratorPage(
					driver);
			adminCommonMethods.NavigateToManageFormGeneratorTabs(driver, "Contact");
			fc.utobj().clickElement(driver, manageFormGeneratorPage.btnAddNewTab);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			AdminInfoMgrManageFormGeneratorAddTabPage objAddNewTabPage = new AdminInfoMgrManageFormGeneratorAddTabPage(
					driver);
			String newTabName = fc.utobj().generateTestData("New Tab for Testing");
			fc.utobj().sendKeys(driver, objAddNewTabPage.txtDisplayName, newTabName);
			fc.utobj().clickElement(driver, objAddNewTabPage.btnSubmit);
			fc.utobj().clickLink(driver, newTabName);

			AdminInfoMgrManageFormGeneratorTabDetailsPage ADM_form_gen = manageFormGeneratorPage.getAddTabDetailsPage();
			fc.utobj().clickElement(driver, ADM_form_gen.btnAddSection);

			fc.commonMethods().switch_cboxIframe_frameId(driver);
			String SectionName = "Customized Data View";
			SectionName = fc.utobj().generateTestData(SectionName);

			fc.utobj().sendKeys(driver, ADM_form_gen.txtSectionName, SectionName);
			fc.utobj().clickElement(driver, ADM_form_gen.btnAddSectionName);
			fc.utobj().switchFrameToDefault(driver);
			fc.utobj().clickElement(driver, ADM_form_gen.btnAddField);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			String FieldName = "TestRA Doc";
			fc.utobj().sendKeys(driver, ADM_form_gen.txtFieldName, FieldName);
			fc.utobj().clickElement(driver, ADM_form_gen.documentopt);
			fc.utobj().clickElement(driver, ADM_form_gen.btnSumbitField);
			fc.utobj().switchFrameToDefault(driver);
			fc.utobj().printTestStep("Navigate To Admin > CRM > Contact Type Configuration");
			fc.utobj().printTestStep("Add Contact Type");
			AdminCRMContactTypeConfigurationPageTest contatcTypePage = new AdminCRMContactTypeConfigurationPageTest();
			String contactType = fc.utobj().generateTestData(dataSet.get("contactType"));
			contatcTypePage.addContactType(driver, contactType);

			fc.utobj().printTestStep("Naviagte To CRM > Contacts > Contacts Summary");
			fc.crm().crm_common().CRMContactsLnk(driver);
			fc.utobj().printTestStep("Add Contacts");
			fc.utobj().clickElement(driver, pobj.addNew);
			fc.utobj().clickElement(driver, pobj.contactLnk);

			String firstName = fc.utobj().generateTestData(dataSet.get("firstName"));
			String lastName = fc.utobj().generateTestData(dataSet.get("lastName"));
			String primaryCMethod = dataSet.get("primaryContactMethod");
			String assignTo = "Corporate";
			String regionName = "";
			String franchiseId = "";
			String title = dataSet.get("title");

			CRMContactsPageTest crmCPtest = new CRMContactsPageTest();
			crmCPtest.addContactGeneric(driver, contactType, firstName, lastName, primaryCMethod, assignTo,
					corpUser.getuserFullName(), regionName, franchiseId, title, emailId, config);
			fc.utobj().clickLink(driver, newTabName);
			String docTitle = dataSet.get("docTitle");
			// String
			// fileName=config.get("inputDirectory")+"\\testData\\CoursePdf.pdf";
			// fc.utobj().getFilePathFromTestData(config,
			// dataSet.get("fileName"));
			String fileName = fc.utobj().getFilePathFromTestData(dataSet.get("fileName"));
			fc.utobj().sendKeys(driver, fc.utobj().getElementByXpath(driver, "//input[@type='file']"), fileName);
			fc.utobj().switchFrameToDefault(driver);
			fc.utobj().clickElement(driver,
					driver.findElement((By.xpath(".//*[@id='siteMainTable']//input[@name='Submit']"))));
			fc.utobj().printTestStep("Verify The Add Document");
			boolean isTextPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[contains(text () , '" + docTitle + "')]");
			if (isTextPresent == false) {
				fc.utobj().throwsException("was not able to verify Document Title");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

			System.out.println("End of your program");

		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "crmmoduleWait" })

	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "TC_308_Verify_Deactivating_MUser", testCaseDescription = "Deactivate M-user associated with Contact,task,Lead,Lead task & Default location owner>Verify The Owner assignment , contact Task , Lead task option")
	public void VerifyMultiUnitReportsOnReportsPage() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		FranchiseesCommonMethods franchisee = fc.infomgr().franchisees();
		AdminInfoMgrCommonMethods adminInfoMgr = fc.adminpage().adminInfoMgr();
		MultiUnitEntityCommonMethods multiUnit = fc.infomgr().multiUnitEntity();

		try {
			driver = fc.loginpage().login(driver);

			// Add Region
			fc.utobj().printTestStep("Add area region");
			// String regionName1 = adminInfoMgr.addAreaRegion(driver , config);
			String regionName = fc.utobj().generateTestData("TestRegion");
			fc.adminpage().adminAreaRegionAddAreaRegionPage(driver);
			AdminAreaRegionAddAreaRegionPage pobjReg = new AdminAreaRegionAddAreaRegionPage(driver);
			fc.utobj().selectDropDown(driver, pobjReg.category, "Domestic");
			fc.utobj().sendKeys(driver, pobjReg.aregRegionName, regionName);
			fc.utobj().selectDropDown(driver, pobjReg.groupBy, "Zip Codes");
			fc.utobj().clickElement(driver, pobjReg.all);
			fc.utobj().clickElement(driver, pobjReg.commaSeparated);
			String zipCode = fc.utobj().generateRandomNumber();
			fc.utobj().sendKeys(driver, pobjReg.ziplist, zipCode);
			fc.utobj().clickElement(driver, pobjReg.Submit);

			// Add Division
			fc.utobj().printTestStep("Add division name");
			// String divName1 = adminInfoMgr.addDivisionName(driver , config);
			AdminDivisionAddDivisionPage pobjDiv = new AdminDivisionAddDivisionPage(driver);
			fc.adminpage().adminDivisionAddDivision(driver);
			//
			String divisionName = fc.utobj().generateTestData("Division 01");
			fc.utobj().sendKeys(driver, pobjDiv.DivisionName, divisionName);
			fc.utobj().clickElement(driver, pobjDiv.submit);

			// Add Franchisee Location with division and region name
			// String divName1="Division 01k21125489";
			// String regionName1="TestRegiont21125140";

			fc.utobj().printTestStep("Add first franchise location");
			// String franchiseID1 =
			// adminInfoMgr.addFranchiseLocationWithDivisionRegion(driver,
			// config , divName1, regionName1);
			// Add Franchise Location
			fc.adminpage().adminAddNewFranchiseLocationLnk(driver);
			AdminFranchiseLocationAddFranchiseLocationPage pobj = new AdminFranchiseLocationAddFranchiseLocationPage(
					driver);
			String franchiseId = fc.utobj().generateTestData("TestfranchiseId");
			fc.utobj().sendKeys(driver, pobj.franchiseeID, franchiseId);
			fc.utobj().sendKeys(driver, pobj.centerName, "0000");
			fc.utobj().selectDropDownByVisibleText(driver, pobj.drpDivisionBrand, divisionName);
			//
			fc.utobj().selectDropDownByVisibleText(driver, pobj.areaRegion, regionName);
			//
			// fc.utobj().selectDropDownByVisibleText(driver, pobj.storeTypeId,
			// storeType);
			fc.utobj().sendKeys(driver, pobj.openingDate, fc.utobj().getFutureDateUSFormat(10));
			fc.utobj().sendKeys(driver, pobj.city, "test city");

			fc.utobj().selectDropDownByVisibleText(driver, pobj.stateID, "Alaska");

			fc.utobj().sendKeys(driver, pobj.storePhone, "0001");
			String firstname = fc.utobj().generateTestData("firstname");
			String lastname = fc.utobj().generateTestData("lastname");
			fc.utobj().sendKeys(driver, pobj.ownerFirstName, firstname);
			fc.utobj().sendKeys(driver, pobj.ownerLastName, lastname);
			fc.utobj().clickElement(driver, pobj.submit);
			String userid = fc.utobj().generateTestData("userid");
			fc.utobj().sendKeys(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='userName']"), userid);
			fc.utobj().sendKeys(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='password']']"), "T0n1ght123");
			fc.utobj().sendKeys(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='email']"),
					"testfran@gmail.com");
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='Submit']"));

			// add location2

			fc.utobj().printTestStep("Add second franchise location");
			// String franchiseID1 =
			// adminInfoMgr.addFranchiseLocationWithDivisionRegion(driver,
			// config , divName1, regionName1);
			// Add Franchise Location
			fc.adminpage().adminAddNewFranchiseLocationLnk(driver);
			String franchiseId2 = fc.utobj().generateTestData("TestfranchiseId");
			fc.utobj().sendKeys(driver, pobj.franchiseeID, franchiseId2);
			fc.utobj().sendKeys(driver, pobj.centerName, "0000");
			fc.utobj().selectDropDownByVisibleText(driver, pobj.drpDivisionBrand, divisionName);
			;
			//
			fc.utobj().selectDropDownByVisibleText(driver, pobj.areaRegion, regionName);
			//
			// fc.utobj().selectDropDownByVisibleText(driver, pobj.storeTypeId,
			// storeType);
			fc.utobj().sendKeys(driver, pobj.openingDate, fc.utobj().getFutureDateUSFormat(10));
			fc.utobj().sendKeys(driver, pobj.city, "test city");

			fc.utobj().selectDropDownByVisibleText(driver, pobj.stateID, "Alaska");
			String firstname1 = fc.utobj().generateTestData("firstname");
			String lastname1 = fc.utobj().generateTestData("lastname");

			fc.utobj().sendKeys(driver, pobj.storePhone, "0001");
			fc.utobj().sendKeys(driver, pobj.ownerFirstName, firstname1);
			fc.utobj().sendKeys(driver, pobj.ownerLastName, lastname1);
			fc.utobj().clickElement(driver, pobj.submit);
			userid = fc.utobj().generateTestData("userid");
			fc.utobj().sendKeys(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='userName']"), userid);
			fc.utobj().sendKeys(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='password']']"), "T0n1ght123");
			fc.utobj().sendKeys(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='email']"),
					"testfran@gmail.com");
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='Submit']"));

			// add another user franchisee 1
			fc.utobj().actionImgOption(driver, franchiseId, "Users");
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//*[@id='siteMainTable']//input[@value='Add User']"));
			fc.utobj().sendKeys(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='userName']"), userid);
			fc.utobj().sendKeys(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='password']']"), "T0n1ght123");
			fc.utobj().selectDropDownByVisibleText(driver, pobj.drpDivisionBrand, divisionName);
			fc.utobj().sendKeys(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='ms-parentroleID']/button"),
					"Default Franchise Role");
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='selectAll']"));
			String firstnameU1 = fc.utobj().generateTestData("firstname");
			String lastnameU1 = fc.utobj().generateTestData("lastname");
			fc.utobj().sendKeys(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='firstName']"), firstnameU1);
			fc.utobj().sendKeys(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='lastName']"), lastnameU1);

			fc.utobj().sendKeys(driver, pobj.openingDate, fc.utobj().getFutureDateUSFormat(10));
			fc.utobj().sendKeys(driver, pobj.city, "test city");

			fc.utobj().selectDropDownByVisibleText(driver, pobj.stateID, "Alaska");

			fc.utobj().sendKeys(driver, pobj.storePhone, "0001");
			fc.utobj().sendKeys(driver, pobj.ownerFirstName, firstnameU1);
			fc.utobj().sendKeys(driver, pobj.ownerLastName, lastnameU1);
			fc.utobj().sendKeys(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='email']"),
					"testfran@gmail.com");
			fc.utobj().clickElement(driver, pobj.submit);

			// add another user franchisee 2
			fc.utobj().actionImgOption(driver, franchiseId2, "Users");
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//*[@id='siteMainTable']//input[@value='Add User']"));
			fc.utobj().sendKeys(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='userName']"), userid);
			fc.utobj().sendKeys(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='password']']"), "T0n1ght123");
			fc.utobj().selectDropDownByVisibleText(driver, pobj.drpDivisionBrand, divisionName);
			fc.utobj().sendKeys(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='ms-parentroleID']/button"),
					"Default Franchise Role");
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='selectAll']"));
			String firstnameu2 = fc.utobj().generateTestData("firstname");
			String lastnameu2 = fc.utobj().generateTestData("lastname");
			fc.utobj().sendKeys(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='firstName']"), firstnameu2);
			fc.utobj().sendKeys(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='lastName']"), lastnameu2);

			fc.utobj().sendKeys(driver, pobj.openingDate, fc.utobj().getFutureDateUSFormat(10));
			fc.utobj().sendKeys(driver, pobj.city, "test city");

			fc.utobj().selectDropDownByVisibleText(driver, pobj.stateID, "Alaska");

			fc.utobj().sendKeys(driver, pobj.storePhone, "0001");
			fc.utobj().sendKeys(driver, pobj.ownerFirstName, firstnameu2);
			fc.utobj().sendKeys(driver, pobj.ownerLastName, lastnameu2);
			fc.utobj().sendKeys(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='email']"),
					"testfran@gmail.com");
			fc.utobj().clickElement(driver, pobj.submit);

			String primaryCMethod = "Email";
			String assignTo = "Franchise";
			String title = "Mr.";
			String emailId = "crmautomation@staffex.com";

			CRMLeadsPage pobjleads = new CRMLeadsPage(driver);
			fc.utobj().printTestStep("Navigate To CRM > Contacts > Contactsss Summary Page");
			fc.utobj().printTestStep("Add Contact");
			fc.crm().crm_common().CRMContactsLnk(driver);
			fc.utobj().clickElement(driver, pobjleads.addNew);
			fc.utobj().clickElement(driver, pobjleads.contactLnk);
			// fc.utobj().printTestStep(testCaseId,"Adding Contact");
			addContactGeneric(driver, "Contacts", fc.utobj().generateTestData("firstName"),
					fc.utobj().generateTestData("lastName"), primaryCMethod, assignTo, firstname + " " + lastname,
					regionName, franchiseId, title, emailId, config);
			addContactGeneric(driver, "Contacts", fc.utobj().generateTestData("firstName"),
					fc.utobj().generateTestData("lastName"), primaryCMethod, assignTo, firstname1 + " " + lastname1,
					regionName, franchiseId, title, emailId, config);

			/*
			 * fc.utobj().printTestStep(testCaseId, "Search franchise and click"
			 * ); franchisee.searchFranchiseAndClick(driver, franchiseId);
			 */
			// add user

			// Add Owner
			fc.utobj().printTestStep("Add owner");
			InfoMgrMultiUnitInfoPage entityInfoPage = new InfoMgrMultiUnitEntityPage(driver).getMultiUnitInfoPage();
			fc.utobj().clickElement(driver, entityInfoPage.tabOwners);
			Map<String, String> lstOwnerDetails = franchisee.addOwner(driver);

			fc.utobj().printTestStep("Add second franchise location");
			String franchiseID2 = adminInfoMgr.addFranchiseLocationWithDivisionRegion(driver, divisionName, regionName);

			fc.utobj().printTestStep("Search franchise and click");
			franchisee.searchFranchiseAndClick(driver, franchiseID2);

			fc.utobj().printTestStep("Add owners");
			fc.utobj().clickElement(driver, entityInfoPage.tabOwners);
			String multiUnitID = franchisee.addExistingOwner(driver, lstOwnerDetails.get("FirstName"));

			fc.utobj().printTestStep("Search multi unit and verify");
			multiUnit.searchMultiUnit(driver, lstOwnerDetails.get("FirstName"));

			boolean ismultiUnitIDDisplayed = fc.utobj().assertPageSource(driver, multiUnitID);

			if (ismultiUnitIDDisplayed) {
				Reporter.log("Multi Unit " + multiUnitID
						+ " is displayed on the page. Multi Unit creation is successfull !!!");
			} else {
				fc.utobj().throwsException("Multi Unit Creation has failed !!!");
			}

		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	// Anukaran Mishra 306

	@Test(groups = { "crmFormGenerator", "crm"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Create a Tabular section>Add All type of Fields to the same with PII supervisior as yes>Verify The Fields data is visible", testCaseId = "TC_306_Verify_PII_Secured_Fields")
	public void verifyPIIsupervisiorFields() throws IOException, Exception, ParserConfigurationException, SAXException {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		AdminInfoMgrCommonMethods adminCommonMethods = fc.adminpage().adminInfoMgr();
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			CRMLeadsPage pobj = new CRMLeadsPage(driver);

			fc.utobj().printTestStep("Navigate To Admin Users Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corpTest = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			String CuserName = fc.utobj().generateTestData(dataSet.get("userName"));

			String emailId = "crmautomation@staffex.com";
			CorporateUser corpUser = new CorporateUser();
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corpUser.setEmail(emailId);
			corpUser.setUserName(CuserName);
			corpUser = corpTest.createDefaultUser(driver, corpUser);
			fc.home_page().logout(driver);
			fc.loginpage().loginWithParameter(driver, CuserName, corpUser.getPassword());

			fc.utobj().printTestStep("Navigate To Admin > CRM > Manage Form Generator");
			fc.utobj().printTestStep("Add New Tab");

			fc.crm().crm_common().adminCRMManageFormGeneratorLnk(driver);
			AdminInfoMgrManageFormGeneratorPage manageFormGeneratorPage = new AdminInfoMgrManageFormGeneratorPage(
					driver);
			adminCommonMethods.NavigateToManageFormGeneratorTabs(driver, "Lead");
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//*[@id='tabdetails']//a[contains(text(),'Lead Info')]"));

			AdminInfoMgrManageFormGeneratorTabDetailsPage ADM_form_gen = manageFormGeneratorPage.getAddTabDetailsPage();
			fc.utobj().clickElement(driver, ADM_form_gen.btnAddSection);

			fc.commonMethods().switch_cboxIframe_frameId(driver);
			String SectionName = "New Sction";
			SectionName = fc.utobj().generateTestData(SectionName);

			fc.utobj().sendKeys(driver, ADM_form_gen.txtSectionName, SectionName);

			if (!fc.utobj().isSelected(driver, ADM_form_gen.tabularSection)) {
				fc.utobj().clickElement(driver, ADM_form_gen.tabularSection);
			}
			fc.utobj().clickElement(driver, ADM_form_gen.btnAddSectionName);
			fc.utobj().switchFrameToDefault(driver);
			fc.utobj().sleep(5000);
			JavascriptExecutor js = (JavascriptExecutor)driver;
			WebElement button =driver.findElement(By.xpath(".//*[@id='siteMainTable']//tr/td[contains(text(),'" 
					+ SectionName + "')]/following-sibling::td/a[contains(text(),'Add New Field')]"));
			js.executeScript("arguments[0].click();", button);

			/*fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//*[@id='siteMainTable']//tr/td[contains(text(),'"
							+ SectionName + "')]/following-sibling::td/a[contains(text(),'Add New Field')]"));*/
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			String FieldName1 = "Text";
			FieldName1 = fc.utobj().generateTestData(FieldName1);
			fc.utobj().sendKeys(driver, ADM_form_gen.txtFieldName, FieldName1);
			fc.utobj().selectDropDown(driver, ADM_form_gen.FieldType, "Text");
			fc.utobj().clickElement(driver, ADM_form_gen.btnSumbitField);
			fc.utobj().switchFrameToDefault(driver);
			fc.utobj().sleep(10000);
			fc.utobj().refresh(driver);
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//*[@id='siteMainTable']//tr/td[contains(text(),'"
							+ FieldName1 + "')]/following-sibling::td[5]/a/img"));

			
			WebElement button1 =driver.findElement(By.xpath(".//*[@id='siteMainTable']//tr/td[contains(text(),'" 
					+ SectionName + "')]/following-sibling::td/a[contains(text(),'Add New Field')]"));
			js.executeScript("arguments[0].click();", button1);
			
			
/*			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//*[@id='siteMainTable']//tr/td[contains(text(),'"
							+ SectionName + "')]/following-sibling::td/a[contains(text(),'Add New Field')]"));*/
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			String FieldName2 = "Test Field2";
			FieldName2 = fc.utobj().generateTestData(FieldName2);
			fc.utobj().sendKeys(driver, ADM_form_gen.txtFieldName, FieldName2);

			fc.utobj().selectDropDown(driver, ADM_form_gen.FieldType, "Date");
			fc.utobj().clickElement(driver, ADM_form_gen.btnSumbitField);
			fc.utobj().switchFrameToDefault(driver);
			fc.utobj().sleep(10000);
			fc.utobj().refresh(driver);
			
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//*[@id='siteMainTable']//tr/td[contains(text(),'"
							+ FieldName2 + "')]/following-sibling::td[5]/a/img"));
			WebElement button2 =driver.findElement(By.xpath(".//*[@id='siteMainTable']//tr/td[contains(text(),'" 
					+ SectionName + "')]/following-sibling::td/a[contains(text(),'Add New Field')]"));
			js.executeScript("arguments[0].click();", button2);
			/*fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//*[@id='siteMainTable']//tr/td[contains(text(),'"
							+ SectionName + "')]/following-sibling::td/a[contains(text(),'Add New Field')]"));*/
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			String FieldName3 = "Test Field3";
			FieldName3 = fc.utobj().generateTestData(FieldName3);
			fc.utobj().sendKeys(driver, ADM_form_gen.txtFieldName, FieldName3);

			fc.utobj().selectDropDown(driver, ADM_form_gen.FieldType, "Numeric");
			fc.utobj().clickElement(driver, ADM_form_gen.btnSumbitField);
			fc.utobj().switchFrameToDefault(driver);
			fc.utobj().sleep(10000);
			fc.utobj().refresh(driver);
			
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//*[@id='siteMainTable']//tr/td[contains(text(),'"
							+ FieldName3 + "')]/following-sibling::td[5]/a/img"));

			fc.utobj().printTestStep("Navigate To CRM > Contacts > Contacts Summary");
			fc.utobj().printTestStep("Add Lead");
			fc.crm().crm_common().CRMContactsLnk(driver);
			fc.utobj().clickElement(driver, pobj.addNew);
			fc.utobj().clickElement(driver, pobj.leadLnk);
			fc.utobj().selectDropDown(driver, pobj.title, dataSet.get("title"));
			String firstName = fc.utobj().generateTestData(dataSet.get("firstName"));
			fc.utobj().sendKeys(driver, pobj.leadFirstName, firstName);
			String lastName = fc.utobj().generateTestData(dataSet.get("lastName"));
			fc.utobj().sendKeys(driver, pobj.leadLastName, lastName);
			String company = fc.utobj().generateTestData(dataSet.get("company"));
			fc.utobj().sendKeys(driver, pobj.companyName, company);
			fc.utobj().selectDropDown(driver, pobj.primaryContactMethodSelect, dataSet.get("primaryContactMethod"));
			if (!fc.utobj().isSelected(driver, pobj.assignToCorporate)) {
				fc.utobj().clickElement(driver, pobj.assignToCorporate);
			}
			fc.utobj().selectDropDown(driver, pobj.selectCorporateUser, corpUser.getuserFullName());
			String address = fc.utobj().generateTestData(dataSet.get("address"));
			fc.utobj().sendKeys(driver, pobj.address, address);
			String city = fc.utobj().generateTestData(dataSet.get("city"));
			fc.utobj().sendKeys(driver, pobj.city, city);
			fc.utobj().selectDropDown(driver, pobj.country, "USA");
			fc.utobj().selectDropDown(driver, pobj.state, "Alabama");
			fc.utobj().sendKeys(driver, pobj.zipcode, "12345");
			fc.utobj().sendKeys(driver, pobj.phoneNumbers, "1236547896");
			fc.utobj().sendKeys(driver, pobj.extn, "12");
			fc.utobj().sendKeys(driver, pobj.faxNumbers, "1263985487");
			fc.utobj().sendKeys(driver, pobj.mobileNumbers, "8896724441");

			fc.utobj().sendKeys(driver, pobj.emailIds, emailId);
			fc.utobj().sendKeys(driver, pobj.alternateEmail, "test@gmail.com");
			String suffix = fc.utobj().generateTestData(dataSet.get("suffix"));
			fc.utobj().sendKeys(driver, pobj.suffix, suffix);
			String jobTitle = fc.utobj().generateTestData(dataSet.get("jobTitle"));
			fc.utobj().sendKeys(driver, pobj.position, jobTitle);
			fc.utobj().sendKeys(driver, pobj.birthdate, "11/24/1980");
			fc.utobj().sendKeys(driver, pobj.anniversarydate, "11/24/1999");
			fc.utobj().selectDropDown(driver, pobj.rating, dataSet.get("rating"));
			fc.utobj().sendKeys(driver, pobj.bestTimeToContact, "12:00 PM EST");
			String comment = fc.utobj().generateTestData(dataSet.get("comment"));
			fc.utobj().sendKeys(driver, pobj.comments, comment);
			fc.utobj().clickElement(driver, pobj.saveBtn);

			fc.utobj().moveToElement(driver,
					fc.utobj().getElementByXpath(driver, ".//*[contains(text(),'" + SectionName + "')]"));
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//td[contains(text(),'" + SectionName + "')]/ancestor::tr/td/a[contains(text(),'Add')]"));

			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().sendKeys(driver,
					driver.findElement(
							By.xpath(".//td[contains(text(),'" + FieldName1 + " :')]/following-sibling::td/input")),
					"textEncValue");
			fc.utobj().sendKeys(driver, fc.utobj().getElementByXpath(driver,
					".//td[contains(text(),'" + FieldName2 + "')]/following-sibling::td/table/tbody/tr/td/input"),
					"02/21/2017");
			fc.utobj().sendKeys(driver,
					driver.findElement(
							By.xpath(".//td[contains(text(),'" + FieldName3 + " :')]/following-sibling::td/input")),
					"25");

			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='Submit']"));
			fc.commonMethods().Click_Close_Input_ByValue(driver);
			fc.utobj().switchFrameToDefault(driver);
			
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//div[contains(text(),'******alue')]/ancestor::td[1]/following-sibling::td[3]/div/layer/a/img"));
			fc.utobj().clickElement(driver,"//div/span[contains(text(),'View')]");
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//td[contains(text(),'" + FieldName1 + " :')]/following-sibling::td[1]/div/a/img"));
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().sendKeys(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='password']"), "piisupervisior");
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='Submit']"));
			fc.utobj().switchFrameToDefault(driver);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//td[contains(text(),'" + FieldName2 + " :')]/following-sibling::td/div/a/img"));
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().sendKeys(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='password']"), "piisupervisior");
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='Submit']"));
			fc.utobj().switchFrameToDefault(driver);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//td[contains(text(),'" + FieldName3 + " :')]/following-sibling::td/div/a/img"));
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().sendKeys(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='password']"), "piisupervisior");
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='Submit']"));

			fc.utobj().switchFrameToDefault(driver);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			boolean isTextPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//*[contains(text () , 'textEncValue')]");
			if (isTextPresent == false) {
				fc.utobj().throwsException("was not able to verify text");
			}
			boolean isTextPresent2 = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//*[contains(text () , '02/21/2017')]");
			if (isTextPresent2 == false) {
				fc.utobj().throwsException("was not able to verify Date");
			}
			boolean isTextPresent3 = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//*[contains(text () , '25')]");
			if (isTextPresent3 == false) {
				fc.utobj().throwsException("was not able to verify Numeric text");
			}

			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//input[@value='Close']"));

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	// Anukaran Mishra 305

	@Test(groups = { "crmmodule", "crm", "crmenckin3"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Login as Franchise user>Archive some contacts>Verify The Archive Contact", testCaseId = "TC_305_Verify_Archive_Contact")
	public void verifiArchiveContact() throws IOException, Exception, ParserConfigurationException, SAXException {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			CRMContactsPage pobj = new CRMContactsPage(driver);
			CRMAccountsPageTest accountPage = new CRMAccountsPageTest();

			/*
			 * //Add region String regionName =
			 * fc.utobj().generateTestData(dataSet.get("regionName"));
			 * AdminAreaRegionAddAreaRegionPageTest p1= new
			 * AdminAreaRegionAddAreaRegionPageTest(); regionName =
			 * p1.addAreaRegion(driver, regionName, config);
			 */
			// add Franchise user

			String zipCode = fc.utobj().generateRandomNumber();
			String firstNameF = fc.utobj().generateTestData(dataSet.get("firstNameFid"));
			String lastNameF = fc.utobj().generateTestData(dataSet.get("lastNameFid"));
			String regionName = fc.utobj().generateTestData("TestRegion");
			/*
			 * // Add Region fc.utobj().printTestStep(testCaseId,
			 * "Add area region");
			 * 
			 * fc.adminpage().adminAreaRegionAddAreaRegionPage( driver);
			 * AdminAreaRegionAddAreaRegionPage pobjReg = new
			 * AdminAreaRegionAddAreaRegionPage(driver);
			 * fc.utobj().selectDropDown(driver,pobjReg.category, "Domestic");
			 * fc.utobj().sendKeys(driver,pobjReg.aregRegionName , regionName);
			 * fc.utobj().selectDropDown(driver,pobjReg.groupBy, "Zip Codes");
			 * fc.utobj().clickElement(driver,pobjReg.all);
			 * fc.utobj().clickElement(driver,pobjReg.commaSeparated);
			 * fc.utobj().sendKeys(driver,pobjReg.ziplist,zipCode);
			 * fc.utobj().clickElement(driver,pobjReg.Submit);
			 */
			fc.utobj().printTestStep("Naviaget To Admin > Franchise Location > Add Franchise Location");
			AdminFranchiseLocationAddFranchiseLocationPageTest franchiseLocation = new AdminFranchiseLocationAddFranchiseLocationPageTest();
			String franchiseId = fc.utobj().generateTestData(dataSet.get("franchiseId"));
			franchiseLocation.addFranchiseLocationForMkt(driver, franchiseId, regionName, firstNameF, lastNameF);

			/*
			 * // add Franchise Location fc.utobj().printTestStep(testCaseId,
			 * "Navigate To Admin > Franchise Location > Add Franchise Location"
			 * ); AdminFranchiseLocationAddFranchiseLocationPageTest
			 * franchiseLocation = new
			 * AdminFranchiseLocationAddFranchiseLocationPageTest(); String
			 * franchiseId1 =
			 * fc.utobj().generateTestData(dataSet.get("franchiseId")); //String
			 * regionName1 =
			 * fc.utobj().generateTestData(dataSet.get("regionName")); String
			 * firstNameF =
			 * fc.utobj().generateTestData(dataSet.get("firstNameFid")); String
			 * lastNameF =
			 * fc.utobj().generateTestData(dataSet.get("lastNameFid"));
			 * franchiseLocation.addFranchiseLocationForMkt(driver,
			 * franchiseId1, regionName, firstNameF, lastNameF, config);
			 */

			
			// Add Franchise User
			fc.utobj().printTestStep("Admin > Users > Manage Franchise Users");
			fc.utobj().printTestStep("Add Franchise User");
			AdminUsersManageManageFranchiseUsersPageTest addFranUser = new AdminUsersManageManageFranchiseUsersPageTest();
			String userNameF = fc.utobj().generateTestData(dataSet.get("userNameF"));
			String roleName = "Default Franchise Role";
			String password = "t0n1ght@123";

			//String emailId = "crmautomation@staffex.com";
			String emailId = fc.utobj().generateTestData("crmautomation")+"@staffex.com";
			addFranUser.addFranchiseUser(driver, userNameF, password, franchiseId, roleName, emailId);

			// Add Contact Type
			fc.utobj().printTestStep("Navigate To Admin > CRM > Contact Type Configuration");
			fc.utobj().printTestStep("Add Contact Type");
			AdminCRMContactTypeConfigurationPageTest contatcTypePage = new AdminCRMContactTypeConfigurationPageTest();
			String contactType = fc.utobj().generateTestData(dataSet.get("contactType"));
			contatcTypePage.addContactType(driver, contactType);

			// addContact

			String firstName = fc.utobj().generateTestData(dataSet.get("firstName"));
			String lastName = fc.utobj().generateTestData(dataSet.get("lastName"));
			String primaryCMethod = dataSet.get("primaryContactMethod");
			String assignTo = "Franchise";
			String userName = firstNameF + " " + lastNameF;
			// String franchiseId="";
			String title = dataSet.get("title");

			String contactName = firstName + " " + lastName;

			fc.utobj().printTestStep("Navigate To CRM > Contacts > Contactsss Summary Page");
			fc.utobj().printTestStep("Add Contact");
			fc.crm().crm_common().CRMContactsLnk(driver);
			fc.utobj().clickElement(driver, pobj.addNew);
			fc.utobj().clickElement(driver, pobj.contactLnk);
			// fc.utobj().printTestStep(testCaseId,"Adding Contact");
			addContactGeneric(driver, contactType, firstName, lastName, primaryCMethod, assignTo, userName, regionName,
					franchiseId, title, emailId, config);

			fc.home_page().logout(driver);
			fc.loginpage().loginWithParameter(driver, userNameF, "t0n1ght@123");
			fc.crm().crm_common().CRMContactsLnk(driver);
			// contactsFilter(driver, contactType);
			/*
			 * WebElement elementInput=fc.utobj().getElement(driver,
			 * pobj.contactTypeSelect).findElement(By.xpath("./div/div/input"));
			 * fc.utobj().selectValFromMultiSelect(driver,
			 * pobj.contactTypeSelect, elementInput, contactType);
			 * fc.utobj().clickElement(driver, pobj.searchBtn);
			 * 
			 * fc.utobj().clickElement(driver, pobj.hideFilter);
			 */
			fc.utobj().printTestStep("Archive Contact");
			try {
				fc.utobj().clickElement(driver,
						fc.utobj().getElementByXpath(driver, ".//*[@id='result']//td/a[contains(text(),'" + contactName
								+ "')]/ancestor::tr/td/input[@name='checkb']"));
			} catch (Exception e) {
				fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
						".//*[@id='result']/table/tbody/tr[2]/td/table/tbody/tr[2]/td[1]/input[1]"));
			}
			accountPage.selectActionMenuItemsWithTagAMoreActions(driver, "Archive Contacts");
			//fc.utobj().acceptAlertBox(driver);
			
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().clickElement(driver,pobj.archive);
			fc.commonMethods().Click_Close_Input_ByValue(driver);
			fc.utobj().switchFrameToDefault(driver);

			/*
			 * fc.utobj().clickElement(driver,
			 * fc.utobj().getElementByXpath(driver, ".//a[contains(text () , '"
			 * +contactName+"')]"));
			 * 
			 * fc.utobj().clickElement(driver, driver.findElement((By.xpath(
			 * ".//*[@id='siteMainTable']//input[@value='Archive Contact' and @name='archive2']"
			 * )))); fc.utobj().acceptAlertBox(driver);
			 * fc.utobj().clickElement(driver, driver.findElement((By.xpath(
			 * ".//*[@id='result']//a[@class='showAction link-btn' and contains(text(),'Actions')]"
			 * ))));
			 */
			// contactsFilter(driver, contactType);
			fc.utobj().printTestStep("Verify Archive Contact At Contact Summary Page");
			boolean isNoRecordPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//td[.='No records found.']");
			if (isNoRecordPresent == false) {
				fc.utobj().throwsException("was not able to Archive Contacts");
			}

			fc.utobj().clickElement(driver, pobj.searchLink);
			fc.utobj().clickElement(driver, pobj.contactsTabAtSearch);
			if (!fc.utobj().isSelected(driver, pobj.archiveContacts)) {
				fc.utobj().clickElement(driver, pobj.archiveContacts);
			}
			/*
			 * fc.utobj().setToDefault(driver, pobj.assignTo); WebElement
			 * elementInput = fc.utobj().getElement(driver,
			 * pobj.assignTo).findElement(By.xpath("./div/div/input"));
			 * fc.utobj().selectValFromMultiSelect(driver, pobj.assignTo,
			 * elementInput, userName); fc.utobj().clickElement(driver,
			 * pobj.searchBtnAtSearch);
			 */
			/*
			 * try { WebElement elementInput =
			 * pobj.selectSearchContactType.findElement(By.xpath(
			 * "./div/div/input")); fc.utobj().selectValFromMultiSelect(driver,
			 * pobj.selectSearchContactType, elementInput, contactType);
			 * 
			 * } catch (Exception e) { // TODO: handle exception }
			 */
			fc.utobj().sendKeys(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='C:CONTACT_FIRST_NAME:TEXT']"),
					firstName);
			fc.utobj().sendKeys(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='C:CONTACT_LAST_NAME:TEXT']"),
					lastName);
			fc.utobj().clickElement(driver,
					driver.findElement((By.xpath(".//*[@id='siteMainTable']//input[@name='Submit']"))));

			fc.utobj().printTestStep("Verify Archive Contact At Search Page");
			boolean isNamePresent = fc.utobj().assertLinkPartialText(driver, contactName);
			if (isNamePresent == false) {
				fc.utobj().throwsException("was not able to verify Archive Contacts");
			}
			/*
			 * boolean isOwnerPresent = fc.utobj().verifyCase(driver,
			 * ".//*[contains(text () ,'"+firstName+" "+lastName+
			 * "')]/ancestor::tr/td[contains(text () , '" + userNameF + "')]");
			 * if (isOwnerPresent == false) { fc.utobj().throwsException(
			 * "was not able to verify Owner of Archive Contacts"); }
			 */
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	/* Anukaran Mishra */

	@Test(groups = { "crmmodule1aa", "crm" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2017-06-20", testCaseDescription = "Archive a private group>Verify The Archived group with private accessibility under Archived", testCaseId = "TC_312_Private_Gruop_Archive")
	public void verifyArchivedGroupPrivateAccessibility()
			throws IOException, Exception, ParserConfigurationException, SAXException {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			CRMContactsPage pobj = new CRMContactsPage(driver);
			CRMGroupsPageTest npoj = new CRMGroupsPageTest();
			CRMGroupsPage npobj = new CRMGroupsPage(driver);

			fc.utobj().printTestStep("Navigate To Admin Users Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corpTest = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			String emailId = "crmautomation@staffex.com";
			CorporateUser corpUser = new CorporateUser();
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corpUser.setEmail(emailId);
			corpUser = corpTest.createDefaultUser(driver, corpUser);
			fc.home_page().logout(driver);
			fc.loginpage().loginWithParameter(driver, corpUser.getUserName(), corpUser.getPassword());
			fc.utobj().printTestStep("Navigate To CRM > Group");
			fc.utobj().printTestStep("Create Group");
			String typeGroup = "Contact";
			String accessibility = "Private";
			String groupName = fc.utobj().generateTestData(dataSet.get("groupName"));
			String groupDescription = fc.utobj().generateTestData(dataSet.get("groupDescription"));
			fc.crm().crm_common().CRMGroupsLnk(driver);
			createGroup2(driver, typeGroup, accessibility, groupName, groupDescription, config);

			fc.utobj().printTestStep("Archive Group");
			npoj.actionImgOption(driver, groupName, "Archive");
			fc.utobj().acceptAlertBox(driver);

			fc.utobj().clickElement(driver, npobj.archivedTab);

			fc.utobj().clickElement(driver, npobj.showFilter);
			fc.utobj().sendKeys(driver, npobj.groupNameSearch, groupName);
			/*
			 * fc.utobj().clickElement(driver, pobj.creationDate);
			 * fc.utobj().getElementByXpath(driver,
			 * ".//span[.='All']/ancestor::label/input"));
			 */
			fc.utobj().clickElement(driver, npobj.fieldSelectGrpTypeBtn);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//*[@id='fc-drop-parentgroupType']/div/ul/li[1]/label/span"));

			fc.utobj().sendKeys(driver, npobj.searchGrpTypeField, "Private");
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//*[@id='fc-drop-parentgroupType']/div/ul/li[4]/label/span[.='Private']"));
			fc.utobj().clickElement(driver, npobj.fieldSelectGrpTypeBtn);
			/*
			 * WebElement elementInput =
			 * npobj.accessabilityTypeBtn.findElement(By.xpath("./div/div/input"
			 * )); fc.utobj().selectValFromMultiSelect(driver,
			 * npobj.accessabilityTypeBtn, elementInput, "Private");
			 */
			// fc.utobj().selectDropDown(driver, npobj.accessabilityTypeBtn,
			// "Private");
			fc.utobj().clickElement(driver, npobj.applyFilterAtGroup);
			boolean isGroupNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[.='" + groupName + "']");
			if (isGroupNamePresent == false) {
				fc.utobj().throwsException("Group was not found in Archived group with private accesibility");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	// Anukaran
	public String createGroup2(WebDriver driver, String typeGroup, String accessibility, String groupName,
			String groupDescription, Map<String, String> config) throws Exception {

		String testCaseId = "TC_Create_Group_CRM";

		if (fc.utobj().validate(testCaseId)) {
			try {
				CRMGroupsPage pobj = new CRMGroupsPage(driver);
				// sync
				List<WebElement> elemntList = driver.findElements(By.xpath(".//*[@id='module_cm']"));

				if (elemntList.size() > 0) {
					fc.utobj().clickElement(driver, pobj.groupsLink);
				} else {
					fc.crm().crm_common().CRMGroupsLnk(driver);
				}
				fc.utobj().clickElement(driver, pobj.createGroupBtn);

				fc.utobj().switchFrame(driver,
						fc.utobj().getElementByXpath(driver, ".//iframe[@class='newLayoutcboxIframe']"));

				fc.utobj().sendKeys(driver, pobj.groupName, groupName);
				// fc.utobj().sendKeys(driver, pobj.groupDescription,
				// groupDescription);

				if (typeGroup.equalsIgnoreCase("Lead")) {
					fc.utobj().selectDropDown(driver, pobj.groupTypeAddGroup, "Lead");

				} else if (typeGroup.equalsIgnoreCase("Contact")) {
					fc.utobj().selectDropDown(driver, pobj.groupTypeAddGroup, "Contact");
				}

				if (accessibility.equalsIgnoreCase("Public To All User")) {
					fc.utobj().selectDropDown(driver, pobj.selectAccessibility, "Public to all Users");

				} else if (accessibility.equalsIgnoreCase("Public To All Corporate User")) {

					fc.utobj().selectDropDown(driver, pobj.selectAccessibility, "Public to all Corporate Users");

				} else if (accessibility.equalsIgnoreCase("Private")) {

					fc.utobj().selectDropDown(driver, pobj.selectAccessibility, "Private");
				}
				fc.utobj().clickElement(driver, pobj.saveAndAssociateBtn);
				fc.utobj().switchFrameToDefault(driver);

				if (typeGroup.equalsIgnoreCase("Lead")) {

					fc.utobj().clickElement(driver, pobj.fieldSelectBtn);
					fc.utobj().sendKeys(driver, pobj.searchField, "Assign To");
					fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
							".//*[@id='fc-drop-parentselColList']//span[.='Assign To']"));
					fc.utobj().clickElement(driver, pobj.fieldSelectBtn);

					// configure Criteria
					fc.utobj().selectDropDown(driver, pobj.assignToCriteria1, "In");
					fc.utobj().clickElement(driver, pobj.selectAssignToUserBtn);
					fc.utobj().clickElement(driver, pobj.selectAssignToSelectAll);

				} else if (typeGroup.equalsIgnoreCase("Contact")) {
					fc.utobj().clickElement(driver, pobj.fieldSelectBtn);
					fc.utobj().sendKeys(driver, pobj.searchField, "First Name");
					fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
							".//*[@id='fc-drop-parentselColList']//span[.='First Name']"));
					fc.utobj().clickElement(driver, pobj.fieldSelectBtn);

					// configure Criteria
					fc.utobj().selectDropDown(driver, pobj.containsCriteria, "Contains");
					// fc.utobj().clickElement(driver, pobj.selectAllCriteria);
					fc.utobj().sendKeys(driver, pobj.containsFirstName, groupName);
				}

				fc.utobj().clickElement(driver, pobj.saveAndContinueBtn);
				// fc.utobj().clickElement(driver, pobj.associateWithGroupBtn);
				// fc.utobj().switchFrame(driver,
				// fc.utobj().getElementByXpath(driver,".//iframe[@class='newLayoutcboxIframe']"));
				// fc.utobj().clickElement(driver, pobj.okBtn);
				// fc.utobj().switchFrameToDefault(driver);
				fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
						".//*[@id='siteMainTable']/tbody/tr/td/div/div[5]/div/button[1]"));

				/*
				 * fc.utobj().sendKeys(driver, pobj.searchAtGroup, groupName);
				 * fc.utobj().clickElement(driver, pobj.exactSearchGroup);
				 * fc.utobj().clickElement(driver, pobj.searchAccountBtn);
				 */

				new CRMGroupsPageTest().searchGroupsByFilter(driver, groupName, "ViewAll");

				fc.utobj().clickElement(driver,
						fc.utobj().getElementByXpath(driver, ".//a[contains(text () ,'" + groupName + "')]"));
				fc.utobj().switchFrame(driver,
						fc.utobj().getElementByXpath(driver, ".//iframe[@class='newLayoutcboxIframe']"));

				boolean isGroupNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
						".//td[contains(text(),'" + groupName + "')]");
				if (isGroupNamePresent == false) {
					fc.utobj().throwsException("was not able to verify Group Name");
				}
				boolean isGroupTypePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//td[contains(text(),'Private')]");
				if (isGroupTypePresent == false) {
					fc.utobj().throwsException("was not able to verify Group Accessibility ");
				}
				fc.utobj().clickElement(driver, pobj.closeNewCbox);
				fc.utobj().switchFrameToDefault(driver);
			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException("Was not able to create Group :: CRM > Groups");

		}
		return groupName;
	}

	/* Anukaran Mishra */

	@Test(groups = { "crmmodule", "crm"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Create Industry at Admin> Modify Industry At Admin > Verify the Industry at Contact Account Info", testCaseId = "TC_313_Verify_Configure_Industry")
	public void verifyConfigureIndustry() throws IOException, Exception, ParserConfigurationException, SAXException {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			CRMContactsPage npobj = new CRMContactsPage(driver);
			CRMAccountsPageTest accountPage = new CRMAccountsPageTest();

			fc.utobj().printTestStep("Naviaget To Admin Users Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corpTest = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			String emailId = "crmautomation@staffex.com";
			CorporateUser corpUser = new CorporateUser();
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corpUser.setEmail(emailId);
			corpUser = corpTest.createDefaultUser(driver, corpUser);
			fc.home_page().logout(driver);
			fc.loginpage().loginWithParameter(driver, corpUser.getUserName(), corpUser.getPassword());

			AdminCRMConfigureIndustryPage pobj = new AdminCRMConfigureIndustryPage(driver);
			AdminCRMConfigureIndustryPageTest industryPage = new AdminCRMConfigureIndustryPageTest();
			String industry = fc.utobj().generateTestData(dataSet.get("industry"));
			String description = fc.utobj().generateTestData(dataSet.get("description"));
			fc.utobj().printTestStep("Navigating to Admin > CRM >  Configure Industry Type");
			fc.utobj().printTestStep("Add Industry");
			industryPage.addIndustry(driver, industry, description);

			fc.utobj().printTestStep("Modify Industry");

			boolean isIndustryPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//td[contains(text () , '" + industry + "')]");

			if (isIndustryPresent == false) {

				fc.utobj().clickElement(driver, pobj.industryTab);

			}

			fc.utobj().actionImgOption(driver, industry, "Modify");
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			industry = fc.utobj().generateTestData(dataSet.get("industry"));
			fc.utobj().sendKeys(driver, pobj.indusrty, industry);
			description = fc.utobj().generateTestData(dataSet.get("description"));
			fc.utobj().sendKeys(driver, pobj.description, description);
			fc.utobj().clickElement(driver, pobj.saveBtn);
			fc.utobj().switchFrameToDefault(driver);
			//This code is commented because there is no need of adding new industry
			/*fc.crm().crm_common().adminCRMConfigureIndustryLnk(driver);
			fc.utobj().clickElement(driver, pobj.addIndustry);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().sendKeys(driver, pobj.indusrty, industry);
			fc.utobj().sendKeys(driver, pobj.description, description);
			fc.utobj().clickElement(driver, pobj.addBtn);
			fc.utobj().clickElement(driver, pobj.closeBtn);

			fc.utobj().switchFrameToDefault(driver);*/

			String accountName = fc.utobj().generateTestData(dataSet.get("accountName"));
			String visibility = "Corporate";
			fc.utobj().printTestStep("Navigating to CRM >  Accounts > Accounts Summary");
			fc.utobj().printTestStep("Add Account");
			accountPage.addAcocunt(driver, industry, accountName, visibility);

			// fc.crm().crm_common().CRMAccountsLnk( driver);
			fc.utobj().clickElement(driver, npobj.AccountsLinks);
			accountPage.searchAccountByIndustry(driver, industry);
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//a[contains(text () , '" + accountName + "')]"));
			boolean isAccountNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () ,'Account Name')]/ancestor::tr/td[.='" + accountName + "']");
			if (isAccountNamePresent == false) {
				fc.utobj().throwsException("was not able to modify Account");
			}

			boolean isVisibilityPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () ,'Industry ')]/ancestor::tr/td[.='" + industry + "']");
			if (isVisibilityPresent == false) {
				fc.utobj().throwsException("was not able to verify visibility");
			}

			fc.utobj().printTestStep("Navigating to CRM >  Accounts > Accounts Summary");
			fc.utobj().printTestStep("Add Contact");

			// fc.utobj().clickElement(driver, npobj.contactsLinks);
			fc.utobj().clickElement(driver, npobj.AccountsLinks);
			accountPage.searchAccountByIndustry(driver, industry);
			fc.utobj().actionImgOption(driver, accountName, "Add Contact");

			String cFname = fc.utobj().generateTestData(dataSet.get("cFname"));
			String cLname = fc.utobj().generateTestData(dataSet.get("cLname"));
			String contactName = cFname + " " + cLname;
			String primaryCMethod = "Email";
			String assignTo = "Corporate";
			String regionName = "";
			String franchiseId = "";
			String title = "Mr.";

			addContactGeneric(driver, "Contacts", cFname, cLname, primaryCMethod, assignTo, corpUser.getuserFullName(),
					regionName, franchiseId, title, emailId, config);

			/*
			 * fc.utobj().clickElement(driver, npobj.contactsLinks);
			 * fc.utobj().clickElement(driver,
			 * fc.utobj().getElementByXpath(driver, ".//a[contains(text () , '"
			 * +contactName+"')]"));
			 */
			fc.utobj().clickElement(driver, npobj.accountInfoTab);

			isVisibilityPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () ,'Industry ')]/ancestor::tr/td[.='" + industry + "']");
			if (isVisibilityPresent == false) {
				fc.utobj().throwsException("was not able to verify visibility");
			}

			isAccountNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () ,'Account Name')]/ancestor::tr/td[.='" + accountName + "']");
			if (isAccountNamePresent == false) {
				fc.utobj().throwsException("was not able to modify Account");
			}
			fc.crm().crm_common().adminCRMConfigureIndustryLnk(driver);
			try {
				fc.utobj().actionImgOption(driver, industry, "Delete");
				fc.utobj().throwsSkipException("FAILED !! Industry type associated with an account can not be deleted");
			} catch (Exception ec) {
				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
				fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
			}
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	/* Anukaran Mishra */

	@Test(groups = { "crmmodule", "crm" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Create Industry at Admin> Modify Industry At Admin > Verify the Industry at Contact Account Info", testCaseId = "TC_314_Verify_Call_Status")
	public void verifyConfigureCallStatus() throws IOException, Exception, ParserConfigurationException, SAXException {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			CRMContactsPage npobj = new CRMContactsPage(driver);
			AdminConfigurationConfigureCallStatusPageTest callStatusPage = new AdminConfigurationConfigureCallStatusPageTest();
			AdminConfigurationConfigureCallStatusPage pobj = new AdminConfigurationConfigureCallStatusPage(driver);

			fc.utobj().printTestStep("Naviaget To Admin Users Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corpTest = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			//String emailId = "crmautomation@staffex.com";
			String emailId=fc.utobj().generateTestData("crmautomation")+"@staffex.com";
			CorporateUser corpUser = new CorporateUser();
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corpUser.setEmail(emailId);
			corpUser = corpTest.createDefaultUser(driver, corpUser);
			fc.home_page().logout(driver);
			fc.loginpage().loginWithParameter(driver, corpUser.getUserName(), corpUser.getPassword());

			String callStatusName = fc.utobj().generateTestData(dataSet.get("callStatusName"));
			fc.utobj().printTestStep("Navigating to Admin > CRM >  Call Status");
			fc.utobj().printTestStep("Add Call Status");
			callStatusPage.addCallStatus(driver, callStatusName);

			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//*[@id='siteMainTable']//select//option[contains(text(),'" + callStatusName + "')]"));
			fc.utobj().clickElement(driver, pobj.modifyBtn);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			String updatedName = fc.utobj().generateTestData(dataSet.get("callStatusName"));
			fc.utobj().sendKeys(driver, pobj.callStatusName, updatedName);
			fc.utobj().clickElement(driver, pobj.modifyBtn);
			//
			fc.utobj().clickElement(driver, pobj.closeBtn);

			fc.utobj().clickElement(driver, pobj.addCallStatus);
			String windowHandle = driver.getWindowHandle();
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().sendKeys(driver, pobj.callStatusName, updatedName);
			fc.utobj().clickElement(driver, pobj.addBtn);
			fc.utobj().acceptAlertBox(driver);
			fc.utobj().printTestStep("Verified Add Call Status with same name");
			fc.utobj().clickElement(driver, pobj.closeBtn);
			driver.switchTo().window(windowHandle);

			String callStatusName1 = fc.utobj().generateTestData(dataSet.get("callStatusName"));
			callStatusPage.addCallStatus(driver, callStatusName1);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//*[@id='siteMainTable']//select//option[contains(text(),'" + callStatusName1 + "')]"));
			fc.utobj().clickElement(driver, pobj.modifyBtn);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().sendKeys(driver, pobj.callStatusName, updatedName);
			fc.utobj().clickElement(driver, pobj.modifyBtn);
			fc.utobj().acceptAlertBox(driver);
			fc.utobj().printTestStep("Verified Modify Call Status with same name");
			fc.utobj().clickElement(driver, pobj.closeBtn);
			driver.switchTo().window(windowHandle);

			fc.utobj().printTestStep("Navigate To Admin > CRM > Contact Type Configuration");
			fc.utobj().printTestStep("Add Contact Type");
			AdminCRMContactTypeConfigurationPageTest contatcTypePage = new AdminCRMContactTypeConfigurationPageTest();
			String contactType = fc.utobj().generateTestData(dataSet.get("contactType"));
			contatcTypePage.addContactType(driver, contactType);

			fc.utobj().printTestStep("Navigate To CRM > Contacts > Contacts Summary Page");
			fc.utobj().printTestStep("Add Contact");
			fc.crm().crm_common().CRMContactsLnk(driver);
			fc.utobj().clickElement(driver, npobj.addNew);
			fc.utobj().clickElement(driver, npobj.contactLnk);

			String firstName = fc.utobj().generateTestData(dataSet.get("firstName"));
			String lastName = fc.utobj().generateTestData(dataSet.get("lastName"));
			String primaryCMethod = dataSet.get("primaryCMethod");
			String assignTo = "Corporate";
			String regionName = "";
			String franchiseId = "";
			String title = dataSet.get("title");

			addContactGeneric(driver, contactType, firstName, lastName, primaryCMethod, assignTo,
					corpUser.getuserFullName(), regionName, franchiseId, title, emailId, config);

			fc.utobj().clickElement(driver, npobj.contactsLinks);
			contactsFilter(driver, contactType);
			fc.utobj().printTestStep("Log A Call");
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//a[contains(text () , '" + firstName + " " + lastName + "')]"));
			fc.utobj().clickElement(driver, npobj.logACallTab);
			String callSubject = fc.utobj().generateTestData(dataSet.get("callSubject"));
			fc.utobj().sendKeys(driver, npobj.callSubject, callSubject);
			/*
			 * String callSubject =
			 * fc.utobj().generateTestData(dataSet.get("callSubject"));
			 * fc.utobj().sendKeys(driver, pobj.callSubject1, callSubject);
			 */
			String callDate = fc.utobj().getCurrentDateUSFormat();
			fc.utobj().sendKeys(driver, npobj.callDate, callDate);
			fc.utobj().selectDropDown(driver, npobj.callStatus, updatedName);
			String cType = dataSet.get("callType");
			fc.utobj().selectDropDown(driver, npobj.communicationType, cType);
			fc.utobj().clickElement(driver, npobj.saveBtn);

			fc.utobj().printTestStep("Verify Remark Activity History");
			try {

				fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
						".//*[@id='ActivityHis']//*[contains(text(),'" + callSubject + "')]"));

			} catch (Exception e) {
				// TODO: handle exception
			}
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			boolean isStatusPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () ,'Call Status')]/ancestor::tr/td[.='" + updatedName + "']");

			if (isStatusPresent == false) {
				fc.utobj().throwsException("was not able to verify Call Status");
			}
			fc.utobj().clickElement(driver, pobj.closeBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.adminpage().adminConfigureCallStatus(driver);
			try {
				fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
						".//*[@id='siteMainTable']//select//option[contains(text(),'" + updatedName + "')]"));
				fc.utobj().clickElement(driver, pobj.deleteBtn);
				fc.utobj().acceptAlertBox(driver);
				fc.utobj().throwsSkipException("FAILED !! Call Status associated with an conatact can not be deleted");
			} catch (Exception ee) {
				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
				fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
			}

		}

		catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	/* Anukaran Mishra */

	@Test(groups = { "crmmodule", "crm" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2017-06-23", testCaseDescription = "Create Industry at Admin> Modify Industry At Admin > Verify the Industry at Contact Account Info", testCaseId = "TC_316_Verify_Duplicate_Contact_Account")
	public void verifyDuplicateConatactAccount()
			throws IOException, Exception, ParserConfigurationException, SAXException {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			fc.utobj().printTestStep("Naviaget To Admin Users Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corpTest = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			String emailId = "crmautomation@staffex.com";
			CorporateUser corpUser = new CorporateUser();
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corpUser.setEmail(emailId);
			corpUser = corpTest.createDefaultUser(driver, corpUser);
			fc.home_page().logout(driver);
			fc.loginpage().loginWithParameter(driver, corpUser.getUserName(), corpUser.getPassword());

			AdminCRMConfigureIndustryPageTest industryPage = new AdminCRMConfigureIndustryPageTest();
			String industry = fc.utobj().generateTestData(dataSet.get("industry"));
			String description = fc.utobj().generateTestData(dataSet.get("description"));
			fc.utobj().printTestStep("Navigating to Admin > CRM >  Configure Industry Type");
			fc.utobj().printTestStep("Add Industry");
			industryPage.addIndustry(driver, industry, description);

			CRMAccountsPageTest accountPage = new CRMAccountsPageTest();
			String accountName = fc.utobj().generateTestData(dataSet.get("accountName"));
			String visibility = "Corporate";
			fc.utobj().printTestStep("Navigating to CRM >  Accounts > Accounts Summary");
			fc.utobj().printTestStep("Add Account");
			accountPage.addAcocunt(driver, industry, accountName, visibility);

			String industry1 = fc.utobj().generateTestData(dataSet.get("industry"));
			String description1 = fc.utobj().generateTestData(dataSet.get("description"));
			fc.utobj().printTestStep("Navigating to Admin > CRM >  Configure Industry Type");
			fc.utobj().printTestStep("Add Industry");
			industryPage.addIndustry(driver, industry1, description1);

			fc.utobj().printTestStep("Navigating to CRM >  Accounts > Accounts Summary");
			fc.utobj().printTestStep("Add Account");

			CRMAccountsPage accountTestPage = new CRMAccountsPage(driver);
			fc.crm().crm_common().CRMAccountsLnk(driver);
			fc.utobj().clickElement(driver, accountTestPage.addNew);
			fc.utobj().clickElement(driver, accountTestPage.accountLnk);
			fc.utobj().sendKeys(driver, accountTestPage.accountName, accountName);
			if (!fc.utobj().isSelected(driver, accountTestPage.B2BAccountType)) {
				fc.utobj().clickElement(driver, accountTestPage.B2BAccountType);
			}
			fc.utobj().selectDropDown(driver, accountTestPage.visibilityType, visibility);
			if (fc.utobj().isSelected(driver,accountTestPage.isParent)) {
				fc.utobj().clickElement(driver, accountTestPage.isParent);
			}
			fc.utobj().sendKeys(driver, accountTestPage.address, "AddressTest");
			fc.utobj().sendKeys(driver, accountTestPage.city, "City");
			fc.utobj().selectDropDown(driver, accountTestPage.country, "USA");
			fc.utobj().sendKeys(driver, accountTestPage.zipcode, "123455");
			fc.utobj().selectDropDown(driver, accountTestPage.state, "Alabama");
			fc.utobj().sendKeys(driver, accountTestPage.phoneNumbers, "1236547896");
			fc.utobj().sendKeys(driver, accountTestPage.extn, "123");
			fc.utobj().sendKeys(driver, accountTestPage.faxNumbers, "1236654");
			fc.utobj().sendKeys(driver, accountTestPage.mobileNumbers, "1478523698");
			fc.utobj().sendKeys(driver, accountTestPage.emailIds, "crmautomation@staffex.com");
			fc.utobj().sendKeys(driver, accountTestPage.noOfEmployee, "500");
			if (industry != null) {
				fc.utobj().selectDropDown(driver, accountTestPage.industry, industry);
			}
			fc.utobj().sendKeys(driver, accountTestPage.sicCode, "SicCode");
			fc.utobj().sendKeys(driver, accountTestPage.servicesProvided, "Service123");
			fc.utobj().sendKeys(driver, accountTestPage.stockSymbol, "qwerty");

			if (!fc.utobj().isSelected(driver, accountTestPage.addContactNo)) {
				fc.utobj().clickElement(driver, accountTestPage.addContactNo);
			}
			fc.utobj().clickElement(driver, accountTestPage.saveBtn);
			fc.utobj().acceptAlertBox(driver);

			fc.utobj().clickElement(driver, accountTestPage.accountsLink);

			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//*[contains(text () , '" + visibility + "')]/ancestor::tr/td/img[@class='appnetixToolTip']"));

			boolean isVisibilityTypePresent1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//*[@id='siteMainTable']/tbody/tr/td/form[2]/table/tbody/tr[7]/td[2]/table/tbody/tr[2]/td/table/tbody/tr[2]/td[2]/a");
			if (isVisibilityTypePresent1 == false) {
				fc.utobj().throwsException("was not able to verify first duplicate contact");
			}

			boolean isVisibilityTypePresent2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//*[@id='siteMainTable']/tbody/tr/td/form[2]/table/tbody/tr[7]/td[2]/table/tbody/tr[2]/td/table/tbody/tr[3]/td[2]/a");
			if (isVisibilityTypePresent2 == false) {
				fc.utobj().throwsException("was not able to verify second duplicate contact");
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "crmmodule", "crm" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Add Contact Source At Admin> Modify Contact Source At Admin > Verify the Contact Source at Contact Primary Info", testCaseId = "TC_317_LeadOwnerAssignment_WebForm")
	public void LeadOwnerAssignmentZipCode() throws IOException, Exception, ParserConfigurationException, SAXException {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			CRMContactsPage npobj = new CRMContactsPage(driver);
			AdminCRMManageWebFormGeneratorPageTest webFormPage = new AdminCRMManageWebFormGeneratorPageTest();

			fc.utobj().printTestStep("Naviaget To Admin Users Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corpTest = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			String emailId = "crmautomation@staffex.com";
			CorporateUser corpUser = new CorporateUser();
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corpUser.setEmail(emailId);
			corpUser = corpTest.createDefaultUser(driver, corpUser);
			fc.home_page().logout(driver);
			fc.loginpage().loginWithParameter(driver, corpUser.getUserName(), corpUser.getPassword());

			String regionName = fc.utobj().generateTestData("TestRegion");
			String zipCode = fc.utobj().generateRandomNumber();
			String firstNameF = fc.utobj().generateTestData(dataSet.get("firstNameFid"));
			String lastNameF = fc.utobj().generateTestData(dataSet.get("lastNameFid"));

			fc.utobj().printTestStep("Naviaget To Admin > Franchise Location > Add Franchise Location");
			AdminFranchiseLocationAddFranchiseLocationPageTest franchiseLocation = new AdminFranchiseLocationAddFranchiseLocationPageTest();
			String franchiseId = fc.utobj().generateTestData(dataSet.get("franchiseId"));
			franchiseLocation.addFranchiseLocationForMkt(driver, franchiseId, regionName, firstNameF, lastNameF);

			fc.utobj().printTestStep("Admin > Users > Manage Franchise Users");
			fc.utobj().printTestStep("Add Franchise User");
			AdminUsersManageManageFranchiseUsersPageTest addFranUser = new AdminUsersManageManageFranchiseUsersPageTest();
			String userNameF = fc.utobj().generateTestData(dataSet.get("userNameF"));
			String roleName = "Default Franchise Role";
			String password = "t0n1ght@123";
			addFranUser.addFranchiseUser(driver, userNameF, password, franchiseId, roleName, emailId);

			String userNameF1 = fc.utobj().generateTestData(dataSet.get("userNameF"));
			AdminConfigurationConfigurePasswordSettingsPageTest configurePassword = new AdminConfigurationConfigurePasswordSettingsPageTest();
			AdminUsersManageManageFranchiseUsersPage adminManageFranUsers = new AdminUsersManageManageFranchiseUsersPage(
					driver);
			configurePassword.ConfigurePasswordSettings(driver);

			fc.adminpage().adminUsersManageFranchiseUsersPage(driver);

			fc.utobj().clickElement(driver, adminManageFranUsers.addFranchiseUserBtn);

			List<WebElement> list = adminManageFranUsers.franchiseIdMuid;
			try {
				if (fc.utobj().isSelected(driver, list.get(0))) {
				} else {
					fc.utobj().clickElement(driver, list.get(0));
				}
			} catch (Exception e) {

			}
			fc.utobj().selectDropDown(driver, adminManageFranUsers.franchiseeNo, franchiseId);
			fc.utobj().clickElement(driver, adminManageFranUsers.addUserBtn);

			fc.utobj().sendKeys(driver, adminManageFranUsers.userName, userNameF1);
			fc.utobj().sendKeys(driver, adminManageFranUsers.password, password);
			fc.utobj().sendKeys(driver, adminManageFranUsers.confirmPassword, password);
			
			/*fc.utobj().clickElement(driver, adminManageFranUsers.rolesBtn);
			fc.utobj().sendKeys(driver, adminManageFranUsers.searchRoles, roleName);
			fc.utobj().clickElement(driver, adminManageFranUsers.selectAll);*/
			
			
			fc.utobj().selectValFromMultiSelect(driver, adminManageFranUsers.selectRole, roleName);
			
			fc.utobj().selectDropDown(driver, adminManageFranUsers.timeZone, "GMT -06:00 US/Canada/Central");
			fc.utobj().selectDropDown(driver, adminManageFranUsers.userType, "New Employee");
			fc.utobj().selectDropDown(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='salutation']"), "Mr.");
			fc.utobj().sendKeys(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='firstName']"), userNameF1);
			fc.utobj().sendKeys(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='lastName']"), userNameF1);
			fc.utobj().sendKeys(driver, adminManageFranUsers.city, "11");
			fc.utobj().selectDropDown(driver, adminManageFranUsers.country, "USA");
			fc.utobj().selectDropDown(driver, adminManageFranUsers.state, "Alabama");

			fc.utobj().sendKeys(driver, adminManageFranUsers.phone1, "1234567890");
			fc.utobj().sendKeys(driver, adminManageFranUsers.phoneExt1, "2");
			fc.utobj().sendKeys(driver, adminManageFranUsers.email, emailId);
			fc.utobj().clickElement(driver, adminManageFranUsers.submit);

			String userNameF2 = fc.utobj().generateTestData(dataSet.get("userNameF"));
			configurePassword.ConfigurePasswordSettings(driver);

			fc.adminpage().adminUsersManageFranchiseUsersPage(driver);

			fc.utobj().clickElement(driver, adminManageFranUsers.addFranchiseUserBtn);

			try {
				if (fc.utobj().isSelected(driver, list.get(0))) {
				} else {
					fc.utobj().clickElement(driver, list.get(0));
				}
			} catch (Exception e) {

			}
			fc.utobj().selectDropDown(driver, adminManageFranUsers.franchiseeNo, franchiseId);
			fc.utobj().clickElement(driver, adminManageFranUsers.addUserBtn);

			fc.utobj().sendKeys(driver, adminManageFranUsers.userName, userNameF2);
			fc.utobj().sendKeys(driver, adminManageFranUsers.password, password);
			fc.utobj().sendKeys(driver, adminManageFranUsers.confirmPassword, password);
			
			/*fc.utobj().clickElement(driver, adminManageFranUsers.rolesBtn);
			fc.utobj().sendKeys(driver, adminManageFranUsers.searchRoles, roleName);
			fc.utobj().clickElement(driver, adminManageFranUsers.selectAll);*/
			
			fc.utobj().selectValFromMultiSelect(driver, adminManageFranUsers.selectRole, roleName);
			
			fc.utobj().selectDropDown(driver, adminManageFranUsers.timeZone, "GMT -06:00 US/Canada/Central");
			fc.utobj().selectDropDown(driver, adminManageFranUsers.userType, "New Employee");
			fc.utobj().selectDropDown(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='salutation']"), "Mr.");
			fc.utobj().sendKeys(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='firstName']"), userNameF2);
			fc.utobj().sendKeys(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='lastName']"), userNameF2);
			fc.utobj().sendKeys(driver, adminManageFranUsers.city, "11");
			fc.utobj().selectDropDown(driver, adminManageFranUsers.country, "USA");
			fc.utobj().selectDropDown(driver, adminManageFranUsers.state, "Alabama");
			fc.utobj().sendKeys(driver, adminManageFranUsers.phone1, "1234567890");
			fc.utobj().sendKeys(driver, adminManageFranUsers.phoneExt1, "2");
			fc.utobj().sendKeys(driver, adminManageFranUsers.email, emailId);
			fc.utobj().clickElement(driver, adminManageFranUsers.submit);

			fc.utobj().printTestStep("Navigate To Admin > Zip / Postal Code Locator > Add New Zip / Postal Codes");
			fc.adminpage().openAdminZipPostalCodeLocatorAddNewLnk(driver);

			zipCode = fc.utobj().generateRandomNumber6Digit();
			fc.utobj().selectDropDown(driver, npobj.selectStateDropDown, "Alabama");
			fc.utobj().sendKeys(driver,
					fc.utobj().getElementByXpath(driver, ".//*[@id='siteMainTable']//textarea[@name='ziplist']"),
					zipCode);
			try {
				fc.utobj().acceptAlertBox(driver);
				fc.utobj().clickElement(driver, npobj.CancelBtn);
			} catch (Exception ex) {
				fc.utobj().clickElement(driver, npobj.submitBtn);
				fc.utobj().clickElement(driver,
						fc.utobj().getElementByXpath(driver, ".//*[@id='siteMainTable']//input[@value='OK']"));
			}

			fc.utobj().printTestStep(
					"Navigate To Admin > Zip / Postal Code Locator > Associate Zip / Postal Codes with Franchise Locations");
			fc.adminpage().openAdminZipPostalCodeLocatorAssociateLnk(driver);
			fc.utobj().selectDropDown(driver, npobj.selectFranID, franchiseId);
			fc.utobj().selectDropDown(driver, npobj.selectStateAtAssociateZip, "Alabama");
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver,
							".//*[@id='siteMainTable']//select[@name='availablezipcode']//option[contains(text(),'"
									+ zipCode + "')]"));
			fc.utobj().clickElement(driver, npobj.moveToOwned);
			fc.utobj().clickElement(driver, npobj.UpdateBtn);

			fc.utobj().printTestStep("Navigate To Admin > CRM > Assign Zip / Postal Codes to Franchise Users");
			fc.crm().crm_common().adminCRMAssignZipToFranUserLnk(driver);
			fc.utobj().selectDropDown(driver, npobj.selectFranID, franchiseId);
			String name = firstNameF + " " + lastNameF + " ";
			try {
				fc.utobj().selectDropDown(driver, npobj.selectAssignTo, name);
			} catch (Exception e) {
				fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
						".//*[@id='siteMainTable']/tbody/tr/td/form[2]/table[1]/tbody/tr[10]/td[2]/table/tbody/tr/td/table/tbody/tr[2]/td/table/tbody/tr[1]/td[4]/select"));
				fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
						".//*[@id='siteMainTable']/tbody/tr/td/form[2]/table[1]/tbody/tr[10]/td[2]/table/tbody/tr/td/table/tbody/tr[2]/td/table/tbody/tr[1]/td[4]/select/option[2]"));
			}

			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver,
							".//*[@id='siteMainTable']//select[@name='ownedzipcode']//option[contains(text(),'"
									+ zipCode + "')]"));
			fc.utobj().clickElement(driver, npobj.moveToAssigned);
			fc.utobj().clickElement(driver, npobj.UpdateBtn);

			String status = "New";
			String contactMedium = "Email";
			String leadSource = "Import";
			String sourceDetails = "Import";
			fc.utobj().printTestStep("Navigate To Admin > CRM > Manage Web Form Generator");
			fc.utobj().printTestStep("Create New Lead Form");
			String formFormat = "Single Page";
			String header = fc.utobj().generateTestData(dataSet.get("header"));
			String textLabel = fc.utobj().generateTestData(dataSet.get("textLabel"));
			String fieldLabel = fc.utobj().generateTestData(dataSet.get("fieldLabel"));
			String footer = fc.utobj().generateTestData(dataSet.get("footer"));
			String assignTo = "Default Owner";
			String submissionType = dataSet.get("submissionType");
			String formName = fc.utobj().generateTestData(dataSet.get("formName"));
			String formTitle = fc.utobj().generateTestData(dataSet.get("formTitle"));
			String description = fc.utobj().generateTestData(dataSet.get("description"));
			String formUrl = fc.utobj().generateTestData(dataSet.get("formUrl"));
			String sectionName = fc.utobj().generateTestData("sectionName");
			String userNameReg = "";
			String userNameFranchise = "";
			String afterSubmission = dataSet.get("afterSubmission");
			String tabName = fc.utobj().generateTestData(dataSet.get("tabName"));
			String redirectedUrl = dataSet.get("reUrl");
			AdminCRMManageWebFormGeneratorPage CrmWebFormPage = new AdminCRMManageWebFormGeneratorPage(driver);

			webFormPage.createNewFormLeads(driver, formFormat, header, textLabel, fieldLabel, footer, assignTo,
					submissionType, formName, formTitle, description, formUrl, sectionName, corpUser.getuserFullName(),
					regionName, userNameReg, franchiseId, userNameFranchise, status, contactMedium, leadSource,
					sourceDetails, afterSubmission, redirectedUrl, tabName, config);

			String LName = fc.utobj().generateTestData(dataSet.get("Lastname"));
			String FName = fc.utobj().generateTestData(dataSet.get("Firstname"));
			String parentWindow = driver.getWindowHandle();
			fc.utobj().sendKeys(driver, CrmWebFormPage.searchMyForm, formName);
			fc.utobj().clickElement(driver, CrmWebFormPage.searchMyFormBtn);
			fc.utobj().actionImgOption(driver, formName, "Launch & Test");

			fc.utobj().printTestStep("Launch And Test Form");
			Set<String> allWindows2 = driver.getWindowHandles();
			for (String currentWindow : allWindows2) {
				if (!currentWindow.equalsIgnoreCase(parentWindow)) {
					driver.switchTo().window(currentWindow);
					String titleTextCurrent = driver.getTitle();
					if (titleTextCurrent.equalsIgnoreCase(formTitle)) {

						fc.utobj().isTextDisplayed(driver, header, "was not able to verify Header");
						fc.utobj().isTextDisplayed(driver, formTitle, "was not able to verify Form Title");
						fc.utobj().isTextDisplayed(driver, textLabel, "was not able to verify Text Label");
						fc.utobj().isTextDisplayed(driver, footer, "was not able to verify Footer");

						fc.utobj().sendKeys(driver, CrmWebFormPage.leadLastName, LName);
						fc.utobj().sendKeys(driver, CrmWebFormPage.leadFirstName, FName);
						fc.utobj().selectDropDownByPartialText(driver, CrmWebFormPage.title, "Dr.");
						fc.utobj().sendKeys(driver, CrmWebFormPage.companyName, "companyName");
						fc.utobj().sendKeys(driver, CrmWebFormPage.address, "address");
						fc.utobj().sendKeys(driver, CrmWebFormPage.city, "city");
						fc.utobj().selectDropDownByPartialText(driver, CrmWebFormPage.country, "USA");
						fc.utobj().sendKeys(driver, CrmWebFormPage.zipcode, zipCode);
						fc.utobj().selectDropDownByPartialText(driver, CrmWebFormPage.state, "Alabama");
						fc.utobj().sendKeys(driver, CrmWebFormPage.phoneNumbers, "1236547896");
						fc.utobj().sendKeys(driver, CrmWebFormPage.extn, "12");
						fc.utobj().sendKeys(driver, CrmWebFormPage.faxNumbers, "1236577896");
						fc.utobj().sendKeys(driver, CrmWebFormPage.mobileNumbers, "9874563212");
						fc.utobj().sendKeys(driver, CrmWebFormPage.emailIds, "jhsdhfs@gmail.com");
						fc.utobj().sendKeys(driver, CrmWebFormPage.alternateEmail, "jhsdhdasdfs@gmail.com");
						fc.utobj().selectDropDownByPartialText(driver, CrmWebFormPage.leadSourceLaunch, leadSource);

						fc.utobj().selectDropDownByPartialText(driver, CrmWebFormPage.leadSourceDetailsLaunch,
								sourceDetails);
						// fc.utobj().clickElement(driver,
						// CrmWebFormPage.nextBtn);

						fc.utobj().isTextDisplayed(driver, header, "was not able to verify Header");
						fc.utobj().isTextDisplayed(driver, formTitle, "was not able to verify Form Title");
						// fc.utobj().isTextDisplayed(driver, tabName, "was not
						// able to verify Tab Name");
						// fc.utobj().isTextDisplayed(driver, fieldLabel, "was
						// not able to verify Fiedl Label");
						fc.utobj().isTextDisplayed(driver, footer, "was not able to verify Footer");
						fc.utobj().clickElement(driver, CrmWebFormPage.submitBtn);
						// fc.utobj().isTextDisplayed(driver, "Thank you for
						// submitting your information, we will get back to you
						// shortly.", "was not able to verify confirmation
						// Msg");

						driver.close();
					} else {
						driver.close();
						driver.switchTo().window(parentWindow);
					}
					driver.switchTo().window(parentWindow);
				}
			}
			CRMLeadsPageTest leadPageTest = new CRMLeadsPageTest();
			fc.crm().crm_common().CRMLeadsLnk(driver);
			leadPageTest.searchLeadByOwner(driver, name.trim(), "Open Leads");
			try {
				boolean verifyOwner = fc.utobj().verifyElementOnVisible_ByXpath(driver,
						".//*[@id='leadsForm']//td/a[contains(text(),'" + FName + " " + LName
								+ "')]/ancestor::td[@class='botBorder colPadding']/following-sibling::td[contains(text(),'"
								+ name.trim() + "')]");
				if (verifyOwner == false) {
					fc.utobj().throwsException("was not able to verify Name");
				}
			} catch (Exception ee) {
			}

			try {
				AdminCRMContactTypeConfigurationPageTest contatcTypePage = new AdminCRMContactTypeConfigurationPageTest();
				formUrl = fc.utobj().generateTestData(dataSet.get("formUrl"));
				String contactType = fc.utobj().generateTestData(dataSet.get("contactType"));
				contatcTypePage.addContactType(driver, contactType);
				webFormPage.createNewFormContactType(driver, formName, formTitle, description, formFormat,
						submissionType, formUrl, sectionName, tabName, header, textLabel, fieldLabel, footer,
						contactType, status, contactMedium, leadSource, sourceDetails, assignTo,
						corpUser.getuserFullName(), regionName, userNameReg, franchiseId, userNameFranchise,
						afterSubmission, redirectedUrl, config);
				parentWindow = driver.getWindowHandle();
				fc.utobj().sendKeys(driver, CrmWebFormPage.searchMyForm, formName);
				fc.utobj().clickElement(driver, CrmWebFormPage.searchMyFormBtn);
				fc.utobj().actionImgOption(driver, formName, "Launch & Test");

				fc.utobj().printTestStep("Launc And Test Form");
				allWindows2 = driver.getWindowHandles();
				for (String currentWindow : allWindows2) {
					if (!currentWindow.equalsIgnoreCase(parentWindow)) {
						driver.switchTo().window(currentWindow);
						String titleTextCurrent = driver.getTitle();
						if (titleTextCurrent.equalsIgnoreCase(formTitle)) {
							// fill the form with data

							fc.utobj().isTextDisplayed(driver, header, "was not able to verify Header");
							fc.utobj().isTextDisplayed(driver, formTitle, "was not able to verify Form Title");
							fc.utobj().isTextDisplayed(driver, textLabel, "was not able to verify Text Label");
							fc.utobj().isTextDisplayed(driver, footer, "was not able to verify Footer");

							fc.utobj().sendKeys(driver, CrmWebFormPage.contactFirstName, FName);
							fc.utobj().sendKeys(driver, CrmWebFormPage.contactLastName, LName);
							fc.utobj().selectDropDownByPartialText(driver, CrmWebFormPage.title, "Dr.");
							// fc.utobj().sendKeys(driver,
							// CrmWebFormPage.companyName, "companyName");
							fc.utobj().sendKeys(driver, CrmWebFormPage.address, "address");
							fc.utobj().sendKeys(driver, CrmWebFormPage.city, "city");
							fc.utobj().selectDropDownByPartialText(driver, CrmWebFormPage.country, "USA");
							fc.utobj().sendKeys(driver, CrmWebFormPage.zipcode, zipCode);
							fc.utobj().selectDropDownByPartialText(driver, CrmWebFormPage.state, "Alabama");
							fc.utobj().sendKeys(driver, CrmWebFormPage.phoneNumbers, "1236547896");
							fc.utobj().sendKeys(driver, CrmWebFormPage.extn, "12");
							fc.utobj().sendKeys(driver, CrmWebFormPage.faxNumbers, "1236577896");
							fc.utobj().sendKeys(driver, CrmWebFormPage.mobileNumbers, "9874563212");
							fc.utobj().sendKeys(driver, CrmWebFormPage.emailIds, "jhsdhfs@gmail.com");
							fc.utobj().sendKeys(driver, CrmWebFormPage.alternateEmail, "jhsdhdasdfs@gmail.com");
							fc.utobj().selectDropDownByPartialText(driver, CrmWebFormPage.cmSourceDetails, leadSource);

							fc.utobj().selectDropDownByPartialText(driver, CrmWebFormPage.howDidYouHearAboutUs,
									sourceDetails);
							// fc.utobj().clickElement(driver,
							// CrmWebFormPage.nextBtn);

							fc.utobj().isTextDisplayed(driver, header, "was not able to verify Header");
							fc.utobj().isTextDisplayed(driver, formTitle, "was not able to verify Form Title");
							// fc.utobj().isTextDisplayed(driver, tabName, "was
							// not able to verify Tab Name");
							// fc.utobj().isTextDisplayed(driver, fieldLabel,
							// "was not able to verify Fiedl Label");
							fc.utobj().isTextDisplayed(driver, footer, "was not able to verify Footer");
							fc.utobj().clickElement(driver, CrmWebFormPage.submitBtn);
							// fc.utobj().isTextDisplayed(driver, "Thank you for
							// submitting your information, we will get back to
							// you shortly.", "was not able to verify
							// confirmation Msg");

							driver.close();
						} else {
							driver.close();
							driver.switchTo().window(parentWindow);
						}
						driver.switchTo().window(parentWindow);
					}
				}
				try {

					fc.crm().crm_common().CRMContactsLnk(driver);
				} catch (Exception exx) {
					fc.utobj().clickElement(driver, npobj.contactsLinks);
				}
				contactsFilter(driver, contactType);
				boolean verifyOwner = fc.utobj().verifyElementOnVisible_ByXpath(driver,
						".//*[@id='result']//td/a[contains(text(),'" + FName + " " + LName
								+ "')]/ancestor::td[@class='botBorder colPadding']/following-sibling::td[contains(text(),'"
								+ name.trim() + "')]");
				if (verifyOwner == false) {
					fc.utobj().throwsException("was not able to verify Name");
				}
			} catch (Exception ee) {
			}
			fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}

	}

	/* Anukaran Mishra */

	@Test(groups = { "crmmodule", "crm"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Assign a Default Owner to the created location >Lead should get assign to location with same user who has been Configured as default owner of the location ", testCaseId = "TC_317_LeadOwnerAssignment_Default_Owner")
	public void verifyLeadOwnerAssignmentDefaultOwner()
			throws IOException, Exception, ParserConfigurationException, SAXException {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			CRMContactsPage npobj = new CRMContactsPage(driver);
			// AdminCRMSourceSummaryPageTest nnpobj=new
			// AdminCRMSourceSummaryPageTest();
			// AdminCRMSourceSummaryPage pobj=new
			// AdminCRMSourceSummaryPage(driver);
			AdminCRMManageWebFormGeneratorPageTest webFormPage = new AdminCRMManageWebFormGeneratorPageTest();

			fc.utobj().printTestStep("Naviaget To Admin Users Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corpTest = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			String emailId = "crmautomation@staffex.com";
			CorporateUser corpUser = new CorporateUser();
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corpUser.setEmail(emailId);
			corpUser = corpTest.createDefaultUser(driver, corpUser);
			fc.home_page().logout(driver);
			fc.loginpage().loginWithParameter(driver, corpUser.getUserName(), corpUser.getPassword());

			// Add Region
			fc.utobj().printTestStep("Add area region");
			String regionName = fc.utobj().generateTestData("TestRegion");
			String zipCode = fc.utobj().generateRandomNumber();
			/*
			 * fc.adminpage().adminAreaRegionAddAreaRegionPage( driver);
			 * AdminAreaRegionAddAreaRegionPage pobjReg = new
			 * AdminAreaRegionAddAreaRegionPage(driver);
			 * fc.utobj().selectDropDown(driver,pobjReg.category, "Domestic");
			 * fc.utobj().sendKeys(driver,pobjReg.aregRegionName , regionName);
			 * fc.utobj().selectDropDown(driver,pobjReg.groupBy, "Zip Codes");
			 * fc.utobj().clickElement(driver,pobjReg.all);
			 * fc.utobj().clickElement(driver,pobjReg.commaSeparated);
			 * fc.utobj().sendKeys(driver,pobjReg.ziplist,zipCode);
			 * fc.utobj().clickElement(driver,pobjReg.Submit);
			 */ String firstNameF = fc.utobj().generateTestData(dataSet.get("firstNameFid"));
			String lastNameF = fc.utobj().generateTestData(dataSet.get("lastNameFid"));
			String OwnerName = lastNameF + " " + firstNameF;
			fc.utobj().printTestStep("Naviaget To Admin > Franchise Location > Add Franchise Location");
			AdminFranchiseLocationAddFranchiseLocationPageTest franchiseLocation = new AdminFranchiseLocationAddFranchiseLocationPageTest();
			String franchiseId = fc.utobj().generateTestData(dataSet.get("franchiseId"));
			franchiseLocation.addFranchiseLocationForMkt(driver, franchiseId, regionName, firstNameF, lastNameF);

			// Add Franchise User
			fc.utobj().printTestStep("Admin > Users > Manage Franchise Users");
			fc.utobj().printTestStep("Add Franchise User");
			AdminUsersManageManageFranchiseUsersPageTest addFranUser = new AdminUsersManageManageFranchiseUsersPageTest();
			String userNameF = fc.utobj().generateTestData(dataSet.get("userNameF"));
			String roleName = "Default Franchise Role";
			String password = "t0n1ght@123";

			addFranUser.addFranchiseUser(driver, userNameF, password, franchiseId, roleName, emailId);

			String userNameF1 = fc.utobj().generateTestData(dataSet.get("userNameF"));
			/*
			 * password = "t0n1ght@123"; addFranUser.addFranchiseUser(driver,
			 * userNameF1, password, franchiseId, roleName, config);
			 */

			AdminConfigurationConfigurePasswordSettingsPageTest configurePassword = new AdminConfigurationConfigurePasswordSettingsPageTest();
			AdminUsersManageManageFranchiseUsersPage adminManageFranUsers = new AdminUsersManageManageFranchiseUsersPage(
					driver);
			configurePassword.ConfigurePasswordSettings(driver);

			fc.adminpage().adminUsersManageFranchiseUsersPage(driver);

			// Add Franchise User Btn
			fc.utobj().clickElement(driver, adminManageFranUsers.addFranchiseUserBtn);

			List<WebElement> list = adminManageFranUsers.franchiseIdMuid;
			try {
				if (fc.utobj().isSelected(driver, list.get(0))) {
					// do nothing
				} else {
					fc.utobj().clickElement(driver, list.get(0));
				}
			} catch (Exception e) {

			}
			fc.utobj().selectDropDown(driver, adminManageFranUsers.franchiseeNo, franchiseId);
			fc.utobj().clickElement(driver, adminManageFranUsers.addUserBtn);

			// User Details

			fc.utobj().sendKeys(driver, adminManageFranUsers.userName, userNameF1);
			fc.utobj().sendKeys(driver, adminManageFranUsers.password, password);
			fc.utobj().sendKeys(driver, adminManageFranUsers.confirmPassword, password);
			// fc.utobj().selectDropDown(driver,adminManageFranUsers.type,
			// "Normal User");
			// fc.utobj().selectDropDown(driver,adminManageFranUsers.expiryDays,
			// "Not Applicable");
			
			/*fc.utobj().clickElement(driver, adminManageFranUsers.rolesBtn);
			fc.utobj().sendKeys(driver, adminManageFranUsers.searchRoles, roleName);
			fc.utobj().clickElement(driver, adminManageFranUsers.selectAll);*/
			
			fc.utobj().selectValFromMultiSelect(driver, adminManageFranUsers.selectRole, roleName);
			
			fc.utobj().selectDropDown(driver, adminManageFranUsers.timeZone, "GMT -06:00 US/Canada/Central");
			fc.utobj().selectDropDown(driver, adminManageFranUsers.userType, "New Employee");
			fc.utobj().selectDropDown(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='salutation']"), "Mr.");
			fc.utobj().sendKeys(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='firstName']"), userNameF1);
			fc.utobj().sendKeys(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='lastName']"), userNameF1);
			// fc.utobj().sendKeys(driver,adminManageFranUsers.jobTitle,"11");
			// fc.utobj().sendKeys(driver,adminManageFranUsers.address,"11");
			fc.utobj().sendKeys(driver, adminManageFranUsers.city, "11");
			fc.utobj().selectDropDown(driver, adminManageFranUsers.country, "USA");

			// fc.utobj().sendKeys(driver,adminManageFranUsers.zipcode,"11");

			fc.utobj().selectDropDown(driver, adminManageFranUsers.state, "Alabama");

			fc.utobj().sendKeys(driver, adminManageFranUsers.phone1, "1234567890");
			fc.utobj().sendKeys(driver, adminManageFranUsers.phoneExt1, "2");
			// fc.utobj().sendKeys(driver,adminManageFranUsers.phone2,"4444455555");
			// fc.utobj().sendKeys(driver,adminManageFranUsers.phoneExt2,"3");
			// fc.utobj().sendKeys(driver,adminManageFranUsers.fax,"33333344444");
			// fc.utobj().sendKeys(driver,adminManageFranUsers.mobile,"3333344444");
			fc.utobj().sendKeys(driver, adminManageFranUsers.email, emailId);
			// fc.utobj().clickElement(driver,adminManageFranUsers.isBillable);
			fc.utobj().clickElement(driver, adminManageFranUsers.submit);

			String userNameF2 = fc.utobj().generateTestData(dataSet.get("userNameF"));
			/*
			 * password = "t0n1ght@123"; addFranUser.addFranchiseUser(driver,
			 * userNameF2, password, franchiseId, roleName, config);
			 */

			configurePassword.ConfigurePasswordSettings(driver);

			fc.adminpage().adminUsersManageFranchiseUsersPage(driver);

			// Add Franchise User Btn
			fc.utobj().clickElement(driver, adminManageFranUsers.addFranchiseUserBtn);

			try {
				if (fc.utobj().isSelected(driver, list.get(0))) {
					// do nothing
				} else {
					fc.utobj().clickElement(driver, list.get(0));
				}
			} catch (Exception e) {

			}
			fc.utobj().selectDropDown(driver, adminManageFranUsers.franchiseeNo, franchiseId);
			fc.utobj().clickElement(driver, adminManageFranUsers.addUserBtn);

			// User Details

			fc.utobj().sendKeys(driver, adminManageFranUsers.userName, userNameF2);
			fc.utobj().sendKeys(driver, adminManageFranUsers.password, password);
			fc.utobj().sendKeys(driver, adminManageFranUsers.confirmPassword, password);
			// fc.utobj().selectDropDown(driver,adminManageFranUsers.type,
			// "Normal User");
			// fc.utobj().selectDropDown(driver,adminManageFranUsers.expiryDays,
			// "Not Applicable");
			
			/*fc.utobj().clickElement(driver, adminManageFranUsers.rolesBtn);
			fc.utobj().sendKeys(driver, adminManageFranUsers.searchRoles, roleName);
			fc.utobj().clickElement(driver, adminManageFranUsers.selectAll);*/
			
			fc.utobj().selectValFromMultiSelect(driver, adminManageFranUsers.selectRole, roleName);
			
			
			fc.utobj().selectDropDown(driver, adminManageFranUsers.timeZone, "GMT -06:00 US/Canada/Central");
			fc.utobj().selectDropDown(driver, adminManageFranUsers.userType, "New Employee");
			fc.utobj().selectDropDown(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='salutation']"), "Mr.");
			fc.utobj().sendKeys(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='firstName']"), userNameF2);
			fc.utobj().sendKeys(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='lastName']"), userNameF2);
			// fc.utobj().sendKeys(driver,adminManageFranUsers.jobTitle,"11");
			// fc.utobj().sendKeys(driver,adminManageFranUsers.address,"11");
			fc.utobj().sendKeys(driver, adminManageFranUsers.city, "11");
			fc.utobj().selectDropDown(driver, adminManageFranUsers.country, "USA");

			// fc.utobj().sendKeys(driver,adminManageFranUsers.zipcode,"11");

			fc.utobj().selectDropDown(driver, adminManageFranUsers.state, "Alabama");

			fc.utobj().sendKeys(driver, adminManageFranUsers.phone1, "1234567890");
			fc.utobj().sendKeys(driver, adminManageFranUsers.phoneExt1, "2");
			// fc.utobj().sendKeys(driver,adminManageFranUsers.phone2,"4444455555");
			// fc.utobj().sendKeys(driver,adminManageFranUsers.phoneExt2,"3");
			// fc.utobj().sendKeys(driver,adminManageFranUsers.fax,"33333344444");
			// fc.utobj().sendKeys(driver,adminManageFranUsers.mobile,"3333344444");
			fc.utobj().sendKeys(driver, adminManageFranUsers.email, emailId);
			// fc.utobj().clickElement(driver,adminManageFranUsers.isBillable);
			fc.utobj().clickElement(driver, adminManageFranUsers.submit);

			fc.adminpage().openAdminZipPostalCodeLocatorAddNewLnk(driver);
			// int zip = 100000 + random_float() * 900000;

			zipCode = fc.utobj().generateRandomNumber6Digit();
			fc.utobj().selectDropDown(driver, npobj.selectStateDropDown, "Alabama");
			fc.utobj().sendKeys(driver,
					fc.utobj().getElementByXpath(driver, ".//*[@id='siteMainTable']//textarea[@name='ziplist']"),
					zipCode);
			try {
				fc.utobj().acceptAlertBox(driver);
				fc.utobj().clickElement(driver, npobj.CancelBtn);
			} catch (Exception ex) {
				fc.utobj().clickElement(driver, npobj.submitBtn);
				fc.utobj().clickElement(driver,
						fc.utobj().getElementByXpath(driver, ".//*[@id='siteMainTable']//input[@value='OK']"));
			}
			fc.adminpage().openAdminZipPostalCodeLocatorAssociateLnk(driver);
			fc.utobj().selectDropDown(driver, npobj.selectFranID, franchiseId);
			// fc.utobj().clickElement(driver,npobj.selectFranID);
			// WebElement
			// elementInput=fc.utobj().getElementByXpath(driver,".//*[@id='siteMainTable']//select[@name='availablezipcode']//option[contains(text(),'35007')]"));
			// fc.utobj().selectValFromMultiSelect(driver,
			// pobj.contactTypeSelect, elementInput, "35006");
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver,
							".//*[@id='siteMainTable']//select[@name='availablezipcode']//option[contains(text(),'"
									+ zipCode + "')]"));
			fc.utobj().clickElement(driver, npobj.moveToOwned);
			fc.utobj().clickElement(driver, npobj.UpdateBtn);

			fc.crm().crm_common().openAdminSetupDefaultOwner(driver);
			fc.utobj().clickElement(driver, npobj.defaultlocationOwnerTab);
			fc.utobj().selectDropDown(driver, npobj.franchiseMenu, franchiseId);
			fc.utobj().selectDropDown(driver, npobj.franchiseUser, userNameF1 + " " + userNameF1);
			fc.utobj().clickElement(driver, npobj.saveBtn);

			// create Web form (Lead)
			String status = "New";
			String contactMedium = "Email";
			String leadSource = "Import";
			String sourceDetails = "Import";
			fc.utobj().printTestStep("Navigate To Admin > CRM > Manage Web Form Generator");
			fc.utobj().printTestStep("Create New Lead Form");
			String formFormat = "Single Page";
			String header = fc.utobj().generateTestData(dataSet.get("header"));
			String textLabel = fc.utobj().generateTestData(dataSet.get("textLabel"));
			String fieldLabel = fc.utobj().generateTestData(dataSet.get("fieldLabel"));
			String footer = fc.utobj().generateTestData(dataSet.get("footer"));
			String assignTo = "Default Owner";
			String submissionType = dataSet.get("submissionType");
			String formName = fc.utobj().generateTestData(dataSet.get("formName"));
			String formTitle = fc.utobj().generateTestData(dataSet.get("formTitle"));
			String description = fc.utobj().generateTestData(dataSet.get("description"));
			String formUrl = fc.utobj().generateTestData(dataSet.get("formUrl"));
			String sectionName = fc.utobj().generateTestData("sectionName");
			String userNameReg = "";
			String userNameFranchise = "";
			String afterSubmission = dataSet.get("afterSubmission");
			String tabName = fc.utobj().generateTestData(dataSet.get("tabName"));
			String redirectedUrl = dataSet.get("reUrl");
			AdminCRMManageWebFormGeneratorPage CrmWebFormPage = new AdminCRMManageWebFormGeneratorPage(driver);
			// leadSource=sourceDetails;

			webFormPage.createNewFormLeads(driver, formFormat, header, textLabel, fieldLabel, footer, assignTo,
					submissionType, formName, formTitle, description, formUrl, sectionName, corpUser.getuserFullName(),
					regionName, userNameReg, franchiseId, userNameFranchise, status, contactMedium, leadSource,
					sourceDetails, afterSubmission, redirectedUrl, tabName, config);

			String LName = fc.utobj().generateTestData(dataSet.get("Lastname"));
			String FName = fc.utobj().generateTestData(dataSet.get("Firstname"));
			String parentWindow = driver.getWindowHandle();
			fc.utobj().sendKeys(driver, CrmWebFormPage.searchMyForm, formName);
			fc.utobj().clickElement(driver, CrmWebFormPage.searchMyFormBtn);
			fc.utobj().actionImgOption(driver, formName, "Launch & Test");

			fc.utobj().printTestStep("Launc And Test Form");
			Set<String> allWindows2 = driver.getWindowHandles();
			for (String currentWindow : allWindows2) {
				if (!currentWindow.equalsIgnoreCase(parentWindow)) {
					driver.switchTo().window(currentWindow);
					String titleTextCurrent = driver.getTitle();
					if (titleTextCurrent.equalsIgnoreCase(formTitle)) {
						// fill the form with data

						fc.utobj().isTextDisplayed(driver, header, "was not able to verify Header");
						fc.utobj().isTextDisplayed(driver, formTitle, "was not able to verify Form Title");
						fc.utobj().isTextDisplayed(driver, textLabel, "was not able to verify Text Label");
						fc.utobj().isTextDisplayed(driver, footer, "was not able to verify Footer");

						fc.utobj().sendKeys(driver, CrmWebFormPage.leadLastName, LName);
						fc.utobj().sendKeys(driver, CrmWebFormPage.leadFirstName, FName);
						fc.utobj().selectDropDownByPartialText(driver, CrmWebFormPage.title, "Dr.");
						fc.utobj().sendKeys(driver, CrmWebFormPage.companyName, "companyName");
						fc.utobj().sendKeys(driver, CrmWebFormPage.address, "address");
						fc.utobj().sendKeys(driver, CrmWebFormPage.city, "city");
						fc.utobj().selectDropDownByPartialText(driver, CrmWebFormPage.country, "USA");
						fc.utobj().sendKeys(driver, CrmWebFormPage.zipcode, zipCode);
						fc.utobj().selectDropDownByPartialText(driver, CrmWebFormPage.state, "Alabama");
						fc.utobj().sendKeys(driver, CrmWebFormPage.phoneNumbers, "1236547896");
						fc.utobj().sendKeys(driver, CrmWebFormPage.extn, "12");
						fc.utobj().sendKeys(driver, CrmWebFormPage.faxNumbers, "1236577896");
						fc.utobj().sendKeys(driver, CrmWebFormPage.mobileNumbers, "9874563212");
						fc.utobj().sendKeys(driver, CrmWebFormPage.emailIds, "jhsdhfs@gmail.com");
						fc.utobj().sendKeys(driver, CrmWebFormPage.alternateEmail, "jhsdhdasdfs@gmail.com");
						try {
							fc.utobj().selectDropDownByPartialText(driver, CrmWebFormPage.leadSourceLaunch, leadSource);

							fc.utobj().selectDropDownByPartialText(driver, CrmWebFormPage.leadSourceDetailsLaunch,
									sourceDetails);
							// fc.utobj().clickElement(driver,
							// CrmWebFormPage.nextBtn);
						} catch (Exception ew) {
						}
						fc.utobj().isTextDisplayed(driver, header, "was not able to verify Header");
						fc.utobj().isTextDisplayed(driver, formTitle, "was not able to verify Form Title");
						// fc.utobj().isTextDisplayed(driver, tabName, "was not
						// able to verify Tab Name");
						// fc.utobj().isTextDisplayed(driver, fieldLabel, "was
						// not able to verify Fiedl Label");
						fc.utobj().isTextDisplayed(driver, footer, "was not able to verify Footer");
						fc.utobj().clickElement(driver, CrmWebFormPage.submitBtn);
						// fc.utobj().isTextDisplayed(driver, "Thank you for
						// submitting your information, we will get back to you
						// shortly.", "was not able to verify confirmation
						// Msg");

						driver.close();
					} else {
						driver.close();
						driver.switchTo().window(parentWindow);
					}
					driver.switchTo().window(parentWindow);
				}
			}

			CRMLeadsPageTest leadPageTest = new CRMLeadsPageTest();
			CRMLeadsPage leadsPage = new CRMLeadsPage(driver);
			fc.crm().crm_common().CRMLeadsLnk(driver);
			leadPageTest.searchLeadByOwner(driver, userNameF1 + " " + userNameF1, "Open Leads");

			boolean verifyOwner = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//*[@id='leadsForm']//td/a[contains(text(),'" + FName + " " + LName
							+ "')]/ancestor::td[@class='botBorder colPadding']/following-sibling::td[contains(text(),'"
							+ userNameF1 + " " + userNameF1 + "')]");
			if (verifyOwner == false) {
				fc.utobj().throwsException("was not able to verify default owner");
			}
			fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}

	}

	/* Anukaran Mishra */

	@Test(groups = { "crmmodule", "crm"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Assign a Default Owner to the created location >Contact should get assign to location with same user who has been Configured as default owner of the location ", testCaseId = "TC_318_ContactOwnerAssignment_Default_Owner")
	public void verifyContactOwnerAssignmentDefaultOwner()
			throws IOException, Exception, ParserConfigurationException, SAXException {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			CRMContactsPage npobj = new CRMContactsPage(driver);
			AdminCRMManageWebFormGeneratorPageTest webFormPage = new AdminCRMManageWebFormGeneratorPageTest();

			fc.utobj().printTestStep("Naviaget To Admin Users Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corpTest = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			String emailId = "crmautomation@staffex.com";
			CorporateUser corpUser = new CorporateUser();
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corpUser.setEmail(emailId);
			corpUser = corpTest.createDefaultUser(driver, corpUser);
			fc.home_page().logout(driver);
			fc.loginpage().loginWithParameter(driver, corpUser.getUserName(), corpUser.getPassword());

			// Add Region
			fc.utobj().printTestStep("Add area region");
			String regionName = fc.utobj().generateTestData("TestRegion");
			String zipCode = fc.utobj().generateRandomNumber();
			String firstNameF = fc.utobj().generateTestData(dataSet.get("firstNameFid"));
			String lastNameF = fc.utobj().generateTestData(dataSet.get("lastNameFid"));
			String OwnerName = lastNameF + " " + firstNameF;
			fc.utobj().printTestStep("Naviaget To Admin > Franchise Location > Add Franchise Location");
			AdminFranchiseLocationAddFranchiseLocationPageTest franchiseLocation = new AdminFranchiseLocationAddFranchiseLocationPageTest();
			String franchiseId = fc.utobj().generateTestData(dataSet.get("franchiseId"));
			franchiseLocation.addFranchiseLocationForMkt(driver, franchiseId, regionName, firstNameF, lastNameF);

			// Add Franchise User
			fc.utobj().printTestStep("Admin > Users > Manage Franchise Users");
			fc.utobj().printTestStep("Add Franchise User");
			AdminUsersManageManageFranchiseUsersPageTest addFranUser = new AdminUsersManageManageFranchiseUsersPageTest();
			String userNameF = fc.utobj().generateTestData(dataSet.get("userNameF"));
			String roleName = "Default Franchise Role";
			String password = "t0n1ght@123";

			addFranUser.addFranchiseUser(driver, userNameF, password, franchiseId, roleName, emailId);

			String userNameF1 = fc.utobj().generateTestData(dataSet.get("userNameF"));
			/*
			 * password = "t0n1ght@123"; addFranUser.addFranchiseUser(driver,
			 * userNameF1, password, franchiseId, roleName, config);
			 */

			AdminConfigurationConfigurePasswordSettingsPageTest configurePassword = new AdminConfigurationConfigurePasswordSettingsPageTest();
			AdminUsersManageManageFranchiseUsersPage adminManageFranUsers = new AdminUsersManageManageFranchiseUsersPage(
					driver);
			// configurePassword.ConfigurePasswordSettings(driver, config);

			Thread.sleep(3000);
			fc.adminpage().adminUsersManageFranchiseUsersPage(driver);

			// Add Franchise User Btn
			fc.utobj().clickElement(driver, adminManageFranUsers.addFranchiseUserBtn);

			List<WebElement> list = adminManageFranUsers.franchiseIdMuid;
			try {
				if (fc.utobj().isSelected(driver, list.get(0))) {
				} else {
					fc.utobj().clickElement(driver, list.get(0));
				}
			} catch (Exception e) {

			}
			fc.utobj().selectDropDown(driver, adminManageFranUsers.franchiseeNo, franchiseId);
			fc.utobj().clickElement(driver, adminManageFranUsers.addUserBtn);

			// User Details

			fc.utobj().sendKeys(driver, adminManageFranUsers.userName, userNameF1);
			fc.utobj().sendKeys(driver, adminManageFranUsers.password, password);
			fc.utobj().sendKeys(driver, adminManageFranUsers.confirmPassword, password);
			// fc.utobj().selectDropDown(driver,adminManageFranUsers.type,
			// "Normal User");
			// fc.utobj().selectDropDown(driver,adminManageFranUsers.expiryDays,
			// "Not Applicable");
			
			/*fc.utobj().clickElement(driver, adminManageFranUsers.rolesBtn);
			fc.utobj().sendKeys(driver, adminManageFranUsers.searchRoles, roleName);
			fc.utobj().clickElement(driver, adminManageFranUsers.selectAll);*/
			
			fc.utobj().selectValFromMultiSelect(driver, adminManageFranUsers.selectRole, roleName);
			
			
			fc.utobj().selectDropDown(driver, adminManageFranUsers.timeZone, "GMT -06:00 US/Canada/Central");
			fc.utobj().selectDropDown(driver, adminManageFranUsers.userType, "New Employee");
			fc.utobj().selectDropDown(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='salutation']"), "Mr.");
			fc.utobj().sendKeys(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='firstName']"), userNameF1);
			fc.utobj().sendKeys(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='lastName']"), userNameF1);
			// fc.utobj().sendKeys(driver,adminManageFranUsers.jobTitle,"11");
			// fc.utobj().sendKeys(driver,adminManageFranUsers.address,"11");
			fc.utobj().sendKeys(driver, adminManageFranUsers.city, "11");
			fc.utobj().selectDropDown(driver, adminManageFranUsers.country, "USA");

			// fc.utobj().sendKeys(driver,adminManageFranUsers.zipcode,"11");

			fc.utobj().selectDropDown(driver, adminManageFranUsers.state, "Alabama");

			fc.utobj().sendKeys(driver, adminManageFranUsers.phone1, "1234567890");
			fc.utobj().sendKeys(driver, adminManageFranUsers.phoneExt1, "2");
			// fc.utobj().sendKeys(driver,adminManageFranUsers.phone2,"4444455555");
			// fc.utobj().sendKeys(driver,adminManageFranUsers.phoneExt2,"3");
			// fc.utobj().sendKeys(driver,adminManageFranUsers.fax,"33333344444");
			// fc.utobj().sendKeys(driver,adminManageFranUsers.mobile,"3333344444");
			fc.utobj().sendKeys(driver, adminManageFranUsers.email, emailId);
			// fc.utobj().clickElement(driver,adminManageFranUsers.isBillable);
			fc.utobj().clickElement(driver, adminManageFranUsers.submit);

			String userNameF2 = fc.utobj().generateTestData(dataSet.get("userNameF"));
			/*
			 * password = "t0n1ght@123"; addFranUser.addFranchiseUser(driver,
			 * userNameF2, password, franchiseId, roleName, config);
			 */

			// configurePassword.ConfigurePasswordSettings(driver, config);

			fc.adminpage().adminUsersManageFranchiseUsersPage(driver);

			// Add Franchise User Btn
			fc.utobj().clickElement(driver, adminManageFranUsers.addFranchiseUserBtn);

			try {
				if (fc.utobj().isSelected(driver, list.get(0))) {
					// do nothing
				} else {
					fc.utobj().clickElement(driver, list.get(0));
				}
			} catch (Exception e) {

			}
			fc.utobj().selectDropDown(driver, adminManageFranUsers.franchiseeNo, franchiseId);
			fc.utobj().clickElement(driver, adminManageFranUsers.addUserBtn);

			// User Details

			fc.utobj().sendKeys(driver, adminManageFranUsers.userName, userNameF2);
			fc.utobj().sendKeys(driver, adminManageFranUsers.password, password);
			fc.utobj().sendKeys(driver, adminManageFranUsers.confirmPassword, password);
			// fc.utobj().selectDropDown(driver,adminManageFranUsers.type,
			// "Normal User");
			// fc.utobj().selectDropDown(driver,adminManageFranUsers.expiryDays,
			// "Not Applicable");
			
			/*fc.utobj().clickElement(driver, adminManageFranUsers.rolesBtn);
			fc.utobj().sendKeys(driver, adminManageFranUsers.searchRoles, roleName);
			fc.utobj().clickElement(driver, adminManageFranUsers.selectAll);*/
			
			fc.utobj().selectValFromMultiSelect(driver, adminManageFranUsers.selectRole, roleName);
			
			fc.utobj().selectDropDown(driver, adminManageFranUsers.timeZone, "GMT -06:00 US/Canada/Central");
			fc.utobj().selectDropDown(driver, adminManageFranUsers.userType, "New Employee");
			fc.utobj().selectDropDown(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='salutation']"), "Mr.");
			fc.utobj().sendKeys(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='firstName']"), userNameF2);
			fc.utobj().sendKeys(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='lastName']"), userNameF2);
			// fc.utobj().sendKeys(driver,adminManageFranUsers.jobTitle,"11");
			// fc.utobj().sendKeys(driver,adminManageFranUsers.address,"11");
			fc.utobj().sendKeys(driver, adminManageFranUsers.city, "11");
			fc.utobj().selectDropDown(driver, adminManageFranUsers.country, "USA");

			// fc.utobj().sendKeys(driver,adminManageFranUsers.zipcode,"11");

			fc.utobj().selectDropDown(driver, adminManageFranUsers.state, "Alabama");

			fc.utobj().sendKeys(driver, adminManageFranUsers.phone1, "1234567890");
			fc.utobj().sendKeys(driver, adminManageFranUsers.phoneExt1, "2");
			// fc.utobj().sendKeys(driver,adminManageFranUsers.phone2,"4444455555");
			// fc.utobj().sendKeys(driver,adminManageFranUsers.phoneExt2,"3");
			// fc.utobj().sendKeys(driver,adminManageFranUsers.fax,"33333344444");
			// fc.utobj().sendKeys(driver,adminManageFranUsers.mobile,"3333344444");
			fc.utobj().sendKeys(driver, adminManageFranUsers.email, emailId);
			// fc.utobj().clickElement(driver,adminManageFranUsers.isBillable);
			fc.utobj().clickElement(driver, adminManageFranUsers.submit);

			fc.adminpage().openAdminZipPostalCodeLocatorAddNewLnk(driver);
			// int zip = 100000 + random_float() * 900000;

			zipCode = fc.utobj().generateRandomNumber6Digit();
			fc.utobj().selectDropDown(driver, npobj.selectStateDropDown, "Alabama");
			fc.utobj().sendKeys(driver,
					fc.utobj().getElementByXpath(driver, ".//*[@id='siteMainTable']//textarea[@name='ziplist']"),
					zipCode);
			try {
				fc.utobj().acceptAlertBox(driver);
				fc.utobj().clickElement(driver, npobj.CancelBtn);
			} catch (Exception ex) {
				fc.utobj().clickElement(driver, npobj.submitBtn);
				fc.utobj().clickElement(driver,
						fc.utobj().getElementByXpath(driver, ".//*[@id='siteMainTable']//input[@value='OK']"));
			}
			fc.adminpage().openAdminZipPostalCodeLocatorAssociateLnk(driver);
			fc.utobj().selectDropDown(driver, npobj.selectFranID, franchiseId);
			// fc.utobj().clickElement(driver,npobj.selectFranID);
			// WebElement
			// elementInput=fc.utobj().getElementByXpath(driver,".//*[@id='siteMainTable']//select[@name='availablezipcode']//option[contains(text(),'35007')]"));
			// fc.utobj().selectValFromMultiSelect(driver,
			// pobj.contactTypeSelect, elementInput, "35006");
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver,
							".//*[@id='siteMainTable']//select[@name='availablezipcode']//option[contains(text(),'"
									+ zipCode + "')]"));
			fc.utobj().clickElement(driver, npobj.moveToOwned);
			fc.utobj().clickElement(driver, npobj.UpdateBtn);

			fc.crm().crm_common().openAdminSetupDefaultOwner(driver);
			fc.utobj().clickElement(driver, npobj.defaultlocationOwnerTab);
			fc.utobj().selectDropDown(driver, npobj.franchiseMenu, franchiseId);
			fc.utobj().selectDropDown(driver, npobj.franchiseUser, userNameF1 + " " + userNameF1);
			fc.utobj().clickElement(driver, npobj.saveBtn);

			// create Web form (Lead)
			String status = "New";
			String contactMedium = "Email";
			String leadSource = "Import";
			String sourceDetails = "Import";
			fc.utobj().printTestStep("Navigate To Admin > CRM > Manage Web Form Generator");
			fc.utobj().printTestStep("Create New Lead Form");
			String formFormat = "Single Page";
			String header = fc.utobj().generateTestData(dataSet.get("header"));
			String textLabel = fc.utobj().generateTestData(dataSet.get("textLabel"));
			String fieldLabel = fc.utobj().generateTestData(dataSet.get("fieldLabel"));
			String footer = fc.utobj().generateTestData(dataSet.get("footer"));
			String assignTo = "Default Owner";
			String submissionType = dataSet.get("submissionType");
			String formName = fc.utobj().generateTestData(dataSet.get("formName"));
			String formTitle = fc.utobj().generateTestData(dataSet.get("formTitle"));
			String description = fc.utobj().generateTestData(dataSet.get("description"));
			String formUrl = fc.utobj().generateTestData(dataSet.get("formUrl"));
			String sectionName = fc.utobj().generateTestData("sectionName");
			String userNameReg = "";
			String userNameFranchise = "";
			String afterSubmission = dataSet.get("afterSubmission");
			String tabName = fc.utobj().generateTestData(dataSet.get("tabName"));
			String redirectedUrl = dataSet.get("reUrl");
			AdminCRMManageWebFormGeneratorPage CrmWebFormPage = new AdminCRMManageWebFormGeneratorPage(driver);
			String LName = fc.utobj().generateTestData(dataSet.get("Lastname"));
			String FName = fc.utobj().generateTestData(dataSet.get("Firstname"));
			// leadSource=sourceDetails;

			AdminCRMContactTypeConfigurationPageTest contatcTypePage = new AdminCRMContactTypeConfigurationPageTest();
			formUrl = fc.utobj().generateTestData(dataSet.get("formUrl"));
			String contactType = fc.utobj().generateTestData(dataSet.get("contactType"));
			contatcTypePage.addContactType(driver, contactType);
			webFormPage.createNewFormContactType(driver, formName, formTitle, description, formFormat, submissionType,
					formUrl, sectionName, tabName, header, textLabel, fieldLabel, footer, contactType, status,
					contactMedium, leadSource, sourceDetails, assignTo, corpUser.getuserFullName(), regionName,
					userNameReg, franchiseId, userNameFranchise, afterSubmission, redirectedUrl, config);
			String parentWindow = driver.getWindowHandle();
			fc.utobj().sendKeys(driver, CrmWebFormPage.searchMyForm, formName);
			fc.utobj().clickElement(driver, CrmWebFormPage.searchMyFormBtn);
			fc.utobj().actionImgOption(driver, formName, "Launch & Test");

			fc.utobj().printTestStep("Launc And Test Form");
			Set<String> allWindows2 = driver.getWindowHandles();
			for (String currentWindow : allWindows2) {
				if (!currentWindow.equalsIgnoreCase(parentWindow)) {
					driver.switchTo().window(currentWindow);
					String titleTextCurrent = driver.getTitle();
					if (titleTextCurrent.equalsIgnoreCase(formTitle)) {
						// fill the form with data

						fc.utobj().isTextDisplayed(driver, header, "was not able to verify Header");
						fc.utobj().isTextDisplayed(driver, formTitle, "was not able to verify Form Title");
						fc.utobj().isTextDisplayed(driver, textLabel, "was not able to verify Text Label");
						fc.utobj().isTextDisplayed(driver, footer, "was not able to verify Footer");

						fc.utobj().sendKeys(driver, CrmWebFormPage.contactFirstName, FName);
						fc.utobj().sendKeys(driver, CrmWebFormPage.contactLastName, LName);
						fc.utobj().selectDropDownByPartialText(driver, CrmWebFormPage.title, "Dr.");
						// fc.utobj().sendKeys(driver,
						// CrmWebFormPage.companyName, "companyName");
						fc.utobj().sendKeys(driver, CrmWebFormPage.address, "address");
						fc.utobj().sendKeys(driver, CrmWebFormPage.city, "city");
						fc.utobj().selectDropDownByPartialText(driver, CrmWebFormPage.country, "USA");
						fc.utobj().sendKeys(driver, CrmWebFormPage.zipcode, zipCode);
						fc.utobj().selectDropDownByPartialText(driver, CrmWebFormPage.state, "Alabama");
						fc.utobj().sendKeys(driver, CrmWebFormPage.phoneNumbers, "1236547896");
						fc.utobj().sendKeys(driver, CrmWebFormPage.extn, "12");
						fc.utobj().sendKeys(driver, CrmWebFormPage.faxNumbers, "1236577896");
						fc.utobj().sendKeys(driver, CrmWebFormPage.mobileNumbers, "9874563212");
						fc.utobj().sendKeys(driver, CrmWebFormPage.emailIds, "crmautomation@staffex.com");
						fc.utobj().sendKeys(driver, CrmWebFormPage.alternateEmail, "crmautomation@staffex.com");
						fc.utobj().selectDropDownByPartialText(driver, CrmWebFormPage.cmSourceDetails, leadSource);

						fc.utobj().selectDropDownByPartialText(driver, CrmWebFormPage.howDidYouHearAboutUs,
								sourceDetails);
						// fc.utobj().clickElement(driver,
						// CrmWebFormPage.nextBtn);

						fc.utobj().isTextDisplayed(driver, header, "was not able to verify Header");
						fc.utobj().isTextDisplayed(driver, formTitle, "was not able to verify Form Title");
						// fc.utobj().isTextDisplayed(driver, tabName, "was not
						// able to verify Tab Name");
						// fc.utobj().isTextDisplayed(driver, fieldLabel, "was
						// not able to verify Fiedl Label");
						fc.utobj().isTextDisplayed(driver, footer, "was not able to verify Footer");
						fc.utobj().clickElement(driver, CrmWebFormPage.submitBtn);
						// fc.utobj().isTextDisplayed(driver, "Thank you for
						// submitting your information, we will get back to you
						// shortly.", "was not able to verify confirmation
						// Msg");

						driver.close();
					} else {
						driver.close();
						driver.switchTo().window(parentWindow);
					}
					driver.switchTo().window(parentWindow);
				}
			}
			try {

				fc.crm().crm_common().CRMContactsLnk(driver);
			} catch (Exception exx) {
				fc.utobj().clickElement(driver, npobj.contactsLinks);
			}
			contactsFilter(driver, contactType);
			WebElement wb = fc.utobj().getElementByXpath(driver, ".//span[contains(text(),'" + FName + " " + LName
					+ "')]/ancestor::tr/td/span[contains(text(),'" + userNameF1 + "')]");
			boolean isOwnerDisplayed = wb.isEnabled();
			if (isOwnerDisplayed == false) {
				fc.utobj().throwsException("was not able to verify Name");
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}

	}

	/* Anukaran Mishra */

	/*
	 * @Test(groups = {"crmmoduledefcon" , "crm"})
	 * 
	 * @TestCase(createdOn="2017-05-01",updatedOn="2017-06-27",
	 * testCaseDescription =
	 * "Configure Default Contact Setting >Verify all the configured Settings against added Contact/Lead"
	 * , testCaseId = "TC_320_Verify_Default_Contact_Setting") public void
	 * verifyDefaultContactSetting()throws IOException, Exception,
	 * ParserConfigurationException, SAXException {
	 * 
	 * String testCaseId =
	 * fc.utobj().readTestCaseInfo(this.getClass().getName()+"."+new
	 * Object(){}.getClass().getEnclosingMethod().getName());
	 * 
	 * Map<String, String> config =
	 * fc.utobj().readConfigurationFile(FranconnectUtil.path);
	 * Map<String,String> dataSet = fc.utobj().readTestData("crm", testCaseId);
	 * WebDriver driver = fc.utobj().openDriver(config);
	 * 
	 * try { driver = fc.loginpage().login(driver, config); CRMContactsPage
	 * npobj = new CRMContactsPage(driver);
	 * 
	 * fc.utobj().printTestStep(testCaseId,
	 * "Navigating to Admin > CRM > Configure Medium");
	 * fc.utobj().printTestStep(testCaseId, "Add Medium");
	 * AdminCRMConfigureMediumPageTest mediumPage=new
	 * AdminCRMConfigureMediumPageTest(); String
	 * medium=fc.utobj().generateTestData(dataSet.get("medium")); String
	 * description=fc.utobj().generateTestData(dataSet.get("description"));
	 * mediumPage.addMedium(driver, medium, description, config);
	 * AdminCRMSourceSummaryPageTest addSourcePage = new
	 * AdminCRMSourceSummaryPageTest(); String source =
	 * fc.utobj().generateTestData(dataSet.get("source")); description =
	 * fc.utobj().generateTestData(dataSet.get("description")); String
	 * displayOnWebPageText = fc.utobj().generateTestData(dataSet.get("text"));
	 * 
	 * fc.utobj().printTestStep(testCaseId,
	 * "Navigating to Admin > CRM > Source Summary");
	 * fc.utobj().printTestStep(testCaseId, "Add Source");
	 * addSourcePage.addSource(driver, source, description,
	 * displayOnWebPageText, config);
	 * 
	 * fc.utobj().printTestStep(testCaseId, "Add Source Details"); String
	 * sourceDetails =
	 * fc.utobj().generateTestData(dataSet.get("sourceDetails"));
	 * addSourcePage.addSourceDetails(driver, source, sourceDetails,
	 * displayOnWebPageText, description, config);
	 * 
	 * fc.utobj().printTestStep(testCaseId,
	 * "Navigate To Admin > CRM > Contact Type Configuration");
	 * fc.utobj().printTestStep(testCaseId, "Add Contact Type");
	 * AdminCRMContactTypeConfigurationPageTest contatcTypePage = new
	 * AdminCRMContactTypeConfigurationPageTest(); String contactType =
	 * fc.utobj().generateTestData(dataSet.get("contactType"));
	 * contatcTypePage.addContactType(driver, contactType, config);
	 * 
	 * 
	 * fc.utobj().printTestStep(testCaseId,
	 * "Navigate To Admin > CRM > Configure Status");
	 * fc.utobj().printTestStep(testCaseId, "Add Status"); String
	 * status=fc.utobj().generateTestData(dataSet.get("status"));
	 * AdminCRMConfigureStatusPageTest statusPage=new
	 * AdminCRMConfigureStatusPageTest(); statusPage.addStatus01(driver, status,
	 * config);
	 * 
	 * 
	 * String campaignName =
	 * fc.utobj().generateTestData(dataSet.get("campaignName")); String
	 * campaignDescription =
	 * fc.utobj().generateTestData(dataSet.get("campaignDescription")); String
	 * campaignAccessibility = dataSet.get("campaignAccessibility"); String
	 * category = dataSet.get("category"); String templateName =
	 * fc.utobj().generateTestData(dataSet.get("templateName")); String
	 * templateName1 = fc.utobj().generateTestData(dataSet.get("templateName"));
	 * String mailSubject = dataSet.get("mailSubject"); String templateText =
	 * fc.utobj().generateTestData(dataSet.get("templateText"));
	 * 
	 * fc.utobj().printTestStep(testCaseId,
	 * "Navigate to CRM > Campaigns Center Page");
	 * 
	 * CRMCampaignCenterPage pobj=new CRMCampaignCenterPage(driver);
	 * fc.crm().crm_common().CRMCampaignCenterLnk( driver);
	 * 
	 * fc.utobj().printTestStep(testCaseId,
	 * "Create Campaign with code your own template");
	 * fc.utobj().clickElement(driver, pobj.createBtn);
	 * fc.utobj().clickElement(driver, pobj.campaignLnk);
	 * fc.utobj().switchFrame(driver, fc.utobj().getElementByXpath(driver,
	 * ".//iframe[@class='newLayoutcboxIframe']")); fc.utobj().sendKeys(driver,
	 * pobj.campaignName, campaignName); fc.utobj().clickElement(driver,
	 * pobj.addDescription); fc.utobj().sendKeys(driver,
	 * pobj.campaignDescription, campaignDescription);
	 * fc.utobj().selectDropDown(driver, pobj.campaignAccessibility,
	 * campaignAccessibility); fc.utobj().selectDropDown(driver, pobj.category,
	 * category); fc.utobj().clickElement(driver, pobj.campaignType);
	 * fc.utobj().clickElement(driver, pobj.startBtn);
	 * fc.utobj().switchFrameToDefault(driver); fc.utobj().clickElement(driver,
	 * pobj.codeYourOwn); fc.utobj().sendKeys(driver, pobj.templateName,
	 * templateName); fc.utobj().sendKeys(driver, pobj.mailSubject,
	 * mailSubject); fc.utobj().clickElement(driver, pobj.planeText);
	 * fc.utobj().sendKeys(driver, pobj.textArea, templateText);
	 * fc.utobj().clickElement(driver, pobj.saveAndContinue);
	 * fc.utobj().clickElement(driver, pobj.associateLater);
	 * fc.utobj().clickElement(driver, pobj.finalizeAndLaunchCampaignBtn);
	 * 
	 * fc.utobj().printTestStep(testCaseId,
	 * "Create Another Campaign with code your own template"); String
	 * campaignName1 = fc.utobj().generateTestData(dataSet.get("campaignName"));
	 * String campaignDescription1 =
	 * fc.utobj().generateTestData(dataSet.get("campaignDescription"));
	 * 
	 * fc.utobj().clickElement(driver, pobj.createAnotherCampaign);
	 * 
	 * fc.utobj().switchFrame(driver, fc.utobj().getElementByXpath(driver,
	 * ".//iframe[@class='newLayoutcboxIframe']")); fc.utobj().sendKeys(driver,
	 * pobj.campaignName, campaignName1); fc.utobj().clickElement(driver,
	 * pobj.addDescription); fc.utobj().sendKeys(driver,
	 * pobj.campaignDescription, campaignDescription1);
	 * fc.utobj().selectDropDown(driver, pobj.campaignAccessibility,
	 * campaignAccessibility); fc.utobj().selectDropDown(driver, pobj.category,
	 * category); fc.utobj().clickElement(driver, pobj.campaignType2);
	 * fc.utobj().clickElement(driver, pobj.startBtn);
	 * fc.utobj().switchFrameToDefault(driver); fc.utobj().clickElement(driver,
	 * pobj.codeYourOwn); fc.utobj().sendKeys(driver, pobj.templateName,
	 * templateName1); fc.utobj().sendKeys(driver, pobj.mailSubject,
	 * mailSubject); fc.utobj().clickElement(driver, pobj.planeText);
	 * fc.utobj().sendKeys(driver, pobj.textArea, templateText);
	 * fc.utobj().clickElement(driver, pobj.saveAndContinue);
	 * fc.utobj().clickElement(driver, pobj.associateLater);
	 * fc.utobj().clickElement(driver, pobj.finalizeAndLaunchCampaignBtn);
	 * 
	 * fc.crm().crm_common().openAdminSetupDefaultContact(driver);
	 * fc.utobj().selectDropDown(driver, npobj.formContactType, contactType);
	 * fc.utobj().selectDropDown(driver, npobj.contactStatusId, status);
	 * fc.utobj().selectDropDown(driver, npobj.contactSource1Id, medium);
	 * fc.utobj().selectDropDown(driver, npobj.contactSource2Id, source);
	 * fc.utobj().selectDropDown(driver, npobj.contactSource3Id, sourceDetails);
	 * fc.utobj().selectDropDown(driver, npobj.contactRegularCampaign,
	 * campaignName1); fc.utobj().selectDropDown(driver,
	 * npobj.contactPromotionalCampaign, campaignName);
	 * fc.utobj().clickElement(driver, npobj.saveBtn);
	 * 
	 * String firstName=fc.utobj().generateTestData(dataSet.get("firstName"));
	 * String lastName=fc.utobj().generateTestData(dataSet.get("LastName"));
	 * String phone="1230456789"; String comments="comments"; String
	 * emailId="crmautomation@staffex.com";
	 * 
	 * fc.crm().crm_common().CRMHomeLnk( driver);
	 * fc.utobj().clickElement(driver,
	 * fc.utobj().getElementByXpath(driver,".//*[@id='taskAlert']"));
	 * fc.commonMethods().switch_cboxIframe_frameId(driver);
	 * fc.utobj().sendKeys(driver, npobj.hContactFirstName, firstName);
	 * fc.utobj().sendKeys(driver, npobj.hContactLastName, lastName);
	 * fc.utobj().sendKeys(driver, npobj.hContactEmail, emailId);
	 * fc.utobj().sendKeys(driver, npobj.hContactPhone, phone);
	 * fc.utobj().sendKeys(driver, npobj.hContactComment, comments);
	 * fc.utobj().clickElement(driver, npobj.hContactSubmit);
	 * fc.commonMethods().switch_cboxIframe_frameId(driver);
	 * fc.utobj().clickElement(driver, npobj.hContactVeiwDetails);
	 * fc.utobj().switchFrameToDefault(driver);
	 * 
	 * boolean isContactPresent=fc.utobj().verifyCase(driver,
	 * ".//td[contains(text () ,'Name ')]/ancestor::tr/td[.='"+firstName+" "
	 * +lastName+"']"); if (isContactPresent==false) {
	 * fc.utobj().throwsException("was not able to verify visibility"); }
	 * boolean isContactTypePresent=fc.utobj().verifyCase(driver,
	 * ".//td[contains(text () ,'Contact Type ')]/ancestor::tr/td[.='"
	 * +contactType+"']"); if (isContactTypePresent==false) {
	 * fc.utobj().throwsException("was not able to verify visibility"); }
	 * boolean isContactStatusPresent=fc.utobj().verifyCase(driver,
	 * ".//td[contains(text () ,'Status ')]/ancestor::tr/td[.='New']"); if
	 * (isContactStatusPresent==false) { fc.utobj().throwsException(
	 * "was not able to verify visibility"); } boolean
	 * isContactMediumPresent=fc.utobj().verifyCase(driver,
	 * ".//td[contains(text () ,'Contact Medium ')]/ancestor::tr/td[.='"
	 * +medium+"']"); if (isContactMediumPresent==false) {
	 * fc.utobj().throwsException("was not able to verify visibility"); }
	 * boolean isContactSourcePresent=fc.utobj().verifyCase(driver,
	 * ".//td[contains(text () ,'How did you hear about us?(Contact Source) ')]/ancestor::tr/td[.='"
	 * +medium+"']"); if (isContactSourcePresent==false) {
	 * fc.utobj().throwsException("was not able to verify visibility"); }
	 * boolean isContactSourceDetailsPresent=fc.utobj().verifyCase(driver,
	 * ".//td[contains(text () ,'Please Specify(Contact Source Details) ')]/ancestor::tr/td[.='"
	 * +medium+"']"); if (isContactSourceDetailsPresent==false) {
	 * fc.utobj().throwsException("was not able to verify visibility"); }
	 * boolean isStatusDrivenCampaignPresent=fc.utobj().verifyCase(driver,
	 * ".//td[contains(text () ,'Status-Driven Campaign ')]/ancestor::tr/td[.='"
	 * +campaignName+"']"); if (isStatusDrivenCampaignPresent==false) {
	 * fc.utobj().throwsException("was not able to verify visibility"); }
	 * boolean isPromotionalCampaignPresent=fc.utobj().verifyCase(driver,
	 * ".//td[contains(text () ,'Promotional Campaign ')]/ancestor::tr/td[.='"
	 * +campaignName1+"']"); if (isPromotionalCampaignPresent==false) {
	 * fc.utobj().throwsException("was not able to verify visibility"); }
	 * fc.utobj().logoutAndQuitBrowser(driver, config, testCaseId); } catch
	 * (Exception e){ fc.utobj().quitBrowserOnCatch(driver, config, e,
	 * testCaseId); } }
	 */

	@Test(groups = { "crmmodule4", "crm"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2017-06-28", testCaseDescription = "Product/Service and Category Modification and deactivation Check", testCaseId = "TC_319_Verify_Product_Service_Category")
	public void verifyAddModifyContactS() throws IOException, Exception, ParserConfigurationException, SAXException {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			CRMContactsPage npobj = new CRMContactsPage(driver);
			CRMAccountsPage accountTestPage = new CRMAccountsPage(driver);
			CRMAccountsPageTest accountPage = new CRMAccountsPageTest();

			String description = fc.utobj().generateTestData(dataSet.get("description"));
			AdminCRMManageProductServiceCategoryPageTest serviceProductPage = new AdminCRMManageProductServiceCategoryPageTest();
			String categoryName = fc.utobj().generateTestData(dataSet.get("categoryName"));
			String productName = fc.utobj().generateTestData(dataSet.get("productName"));
			String oneLineDescription = fc.utobj().generateTestData(dataSet.get("oneLineDescription"));
			String rate = dataSet.get("rate");
			fc.utobj().printTestStep("Navigating to Admin > CRM >  Manage Product / Service & Category");
			fc.utobj().printTestStep("Add Category");
			serviceProductPage.addCategory(driver, categoryName, description);

			fc.utobj().printTestStep("Navigate To Admin Users Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			String emailId = "crmautomation@staffex.com";
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUser = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporateUser.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			AdminCRMManageProductServiceCategoryPage pobj = new AdminCRMManageProductServiceCategoryPage(driver);

			fc.utobj().printTestStep("Navigating to Admin > CRM >  Manage Product / Service & Category");
			fc.utobj().printTestStep("Add Category");
			fc.crm().crm_common().adminCRMManageProductServiceCategoryLnk(driver);
			fc.utobj().clickElement(driver, pobj.manageCategoryTab);
			fc.utobj().clickElement(driver, pobj.addCategoryBtn);
			fc.utobj().sendKeys(driver, pobj.categoryName, categoryName);
			fc.utobj().sendKeys(driver, pobj.description, description);
			fc.utobj().clickElement(driver, pobj.addBtn);
			fc.utobj().acceptAlertBox(driver);
			fc.utobj().clickElement(driver, pobj.cancelBtn);

			String categoryName1 = fc.utobj().generateTestData(dataSet.get("categoryName"));
			serviceProductPage.addCategory(driver, categoryName1, description);
			fc.utobj().sendKeys(driver, pobj.categorySearch, categoryName);
			fc.utobj().refresh(driver);
			fc.utobj().sleep(5000);
			fc.utobj().sendKeys(driver, pobj.categorySearch, categoryName);
			fc.utobj().printTestStep("Modify The Category");
			/*fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//*[@id='ajaxDiv']//td[contains(text(),'" + categoryName
							+ "')]/following-sibling::td/div/layer/a/img"));
			*/
			fc.utobj().sleep(5000);
			
			JavascriptExecutor js = (JavascriptExecutor)driver;
			WebElement button =driver.findElement(By.xpath(".//td[@id='ajaxDiv']//div[@id='print123Div']//tr/td[contains(text(),'" + categoryName + "')]/following-sibling::td/div/layer/a/img"));
			js.executeScript("arguments[0].click();", button);
			
			/*fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver,".//td[@id='ajaxDiv']//div[@id='print123Div']//tr/td[contains(text(),'" + categoryName + "')]/following-sibling::td/div/layer/a/img"));*/
			
			
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//div[contains(text(),'Modify Details')]"));
			fc.utobj().sendKeys(driver, pobj.categoryName, categoryName);
			description = fc.utobj().generateTestData(dataSet.get("description"));
			fc.utobj().sendKeys(driver, pobj.description, description);
			fc.utobj().sleep(2000);
			fc.utobj().clickElement(driver, pobj.saveBtn);
			fc.utobj().printTestStep("Add Product/Service");
			serviceProductPage.addProductService(driver, categoryName, productName, oneLineDescription, rate);

			fc.utobj().clickElement(driver, pobj.manageProductServiceTab);
			fc.utobj().clickElement(driver, pobj.addProductServiceBtn);
			fc.utobj().sendKeys(driver, pobj.productName, productName);
			fc.utobj().sendKeys(driver, pobj.oneLineItemDescription, oneLineDescription);
			fc.utobj().sendKeys(driver, pobj.rate, rate);
			fc.utobj().selectDropDown(driver, pobj.categoryIDSelect, categoryName);
			if (!fc.utobj().isSelected(driver,pobj.activeCheck)) {
				fc.utobj().clickElement(driver, pobj.activeCheck);
			}
			fc.utobj().clickElement(driver, pobj.addBtn);
			fc.utobj().acceptAlertBox(driver);
			fc.utobj().clickElement(driver, pobj.cancelBtn);

			String productName1 = fc.utobj().generateTestData(dataSet.get("productName"));
			serviceProductPage.addProductService(driver, categoryName, productName1, oneLineDescription, rate);

			fc.utobj().sendKeys(driver, pobj.productServiceSearch, productName);
			fc.utobj().sleep(3000);
			fc.utobj().sendKeys(driver, pobj.categorySearch, categoryName);
			fc.utobj().sleep(3000);
			fc.utobj().printTestStep("Modify Details");
			fc.utobj().actionImgOption(driver, productName, "Modify Details");

			fc.utobj().sendKeys(driver, pobj.productName, productName1);
			oneLineDescription = fc.utobj().generateTestData(dataSet.get("oneLineDescription"));
			fc.utobj().sendKeys(driver, pobj.oneLineItemDescription, oneLineDescription);
			rate = dataSet.get("rate");
			fc.utobj().sendKeys(driver, pobj.rate, rate);
			fc.utobj().selectDropDown(driver, pobj.categoryIDSelect, categoryName);
			if (!fc.utobj().isSelected(driver,pobj.activeCheck)) {
				fc.utobj().clickElement(driver, pobj.activeCheck);
			}
			fc.utobj().clickElement(driver, pobj.saveBtn);
			fc.utobj().acceptAlertBox(driver);
			fc.utobj().clickElement(driver, pobj.cancelBtn);

			String stage = "Prospecting";
			// stagePage.addStage(driver, stage, config);
			AdminCRMConfigureIndustryPageTest industryPage = new AdminCRMConfigureIndustryPageTest();
			String industry = fc.utobj().generateTestData(dataSet.get("industry"));
			
			fc.utobj().printTestStep("Navigating to Admin > CRM >  Configure Industry Type");
			fc.utobj().printTestStep("Add Industry");
			industryPage.addIndustry(driver, industry, description);


			
			AdminCRMContactTypeConfigurationPageTest contatcTypePage = new AdminCRMContactTypeConfigurationPageTest();
			String contactType = fc.utobj().generateTestData(dataSet.get("contactType"));
			contatcTypePage.addContactType(driver, contactType);
			String accountName = fc.utobj().generateTestData(dataSet.get("Accountname"));
			String visibility = "Corporate";
			fc.utobj().printTestStep("Navigating to CRM >  Accounts > Accounts Summary");
			fc.utobj().printTestStep("Add Account");
			accountPage.addAcocunt(driver, industry, accountName, visibility);
			fc.utobj().printTestStep("Navigate To CRM > Contacts > Contacts Summary Page");
			fc.utobj().printTestStep("Add Contact");
			fc.utobj().clickElement(driver, npobj.contactsLinks);
			fc.utobj().clickElement(driver, npobj.addNew);
			fc.utobj().clickElement(driver, npobj.contactLnk);

			String cFname = fc.utobj().generateTestData(dataSet.get("firstName"));
			String cLname = fc.utobj().generateTestData(dataSet.get("LastName"));
			String primaryCMethod1 = dataSet.get("primaryCMethod");
			String assignTo1 = "Corporate";
			String regionName1 = "";
			String franchiseId1 = "";
			String title1 = dataSet.get("title");

			addContactGenericWithAccount(driver, contactType, cFname, cLname, primaryCMethod1, assignTo1,
					corpUser.getuserFullName(), regionName1, franchiseId1, title1, emailId, config, accountName);

			fc.utobj().printTestStep("Add Opportunity");
			fc.utobj().clickElement(driver, npobj.contactsLinks);
			fc.utobj().clickElement(driver, npobj.addNew);
			fc.utobj().clickElement(driver, npobj.OpportunityLnk);

			String opportunityName = fc.utobj().generateTestData(dataSet.get("opportunityName"));
			String contactName = cFname + " " + cLname;
			String opportunityType = dataSet.get("opportunityType");
			String opportunitySource = dataSet.get("opportunitySource");
			String salesAmount = "100";
			accountPage.addOpportunity(driver, opportunityName, accountName, contactName, corpUser.getuserFullName(),
					stage, opportunityType, opportunitySource, salesAmount, config);

			fc.utobj().clickElement(driver, npobj.contactsLinks);
			contactsFilter(driver, contactType);

			fc.utobj().printTestStep("Add Transaction");

			fc.utobj().actionImgOption(driver, contactName, "Add Transaction");
			fc.utobj().sendKeys(driver, accountTestPage.opportunityName, opportunityName);
			fc.utobj().sleep(2000);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='opportunityNameTD']/a"));
			fc.utobj().sleep(2000);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().sleep(2000);
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//a[contains(text(),'" + opportunityName + "')]"));
			fc.utobj().sleep(2000);
			fc.utobj().switchFrameToDefault(driver);
			fc.utobj().clickElement(driver, npobj.selectProductAndService);
			fc.utobj().clickElement(driver, npobj.uncheckAll);
			fc.utobj().sendKeys(driver, npobj.searchProduct, productName);
			fc.utobj().clickElement(driver, npobj.checkAll);
			fc.utobj().clickElement(driver, npobj.okayBtn);
			fc.utobj().clickElement(driver, npobj.submitBtn);
			// driver.findElement(By.linkText("FDD Email Template
			// summary")).click();
			// driver.findElement(By.linkText(opportunityName));
			fc.crm().crm_common().adminCRMManageProductServiceCategoryLnk(driver);
			fc.utobj().clickElement(driver, pobj.manageCategoryTab);
			try {
				fc.utobj().actionImgOption(driver, categoryName, "Delete");
				fc.utobj().throwsException("Failed!!Catogory associated with a transaction can not be deleted ");
				fc.utobj().acceptAlertBox(driver);
			} catch (Exception ee) {
			}
			// fc.crm().crm_common().adminCRMManageProductServiceCategoryLnk(
			// driver);
			fc.utobj().clickElement(driver, pobj.manageProductServiceTab);
			try {

				fc.utobj().actionImgOption(driver, productName, "Deactivate");

			} catch (Exception e) {
				// TODO: handle exception
			}
			fc.utobj().acceptAlertBox(driver);

			fc.crm().crm_common().CRMContactsLnk(driver);
			fc.utobj().clickElement(driver, npobj.transactionsLinks);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//td/a[contains(text(),'"
					+ opportunityName + "')]/ancestor::tr[@class='bText12']/td[1]/a[contains(text(),'INV')]"));

			boolean isProductServicePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () , '" + productName + "')]");
			if (isProductServicePresent == false) {
				fc.utobj().throwsException("was not able to verify Transaction Name");
			}
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//*[@value='Back']"));

			fc.utobj().clickElement(driver, accountTestPage.addTransactionLink);
			fc.utobj().sendKeys(driver, accountTestPage.opportunityName, opportunityName);

			// fc.utobj().clickElement(driver, accountTestPage.ajaxResult);
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//*[@id='opportunityName']/following-sibling::a/img"));
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//a[contains(text(),'" + opportunityName + "')]"));
			fc.utobj().switchFrameToDefault(driver);
			fc.utobj().clickElement(driver, accountTestPage.selectProductAndService);
			fc.utobj().clickElement(driver, accountTestPage.uncheckAll);
			fc.utobj().sendKeys(driver, accountTestPage.searchProduct, productName);
			fc.utobj().clickElement(driver, accountTestPage.checkAll);
			fc.utobj().clickElement(driver, npobj.okayBtn);
			fc.utobj().clickElement(driver, npobj.submitBtn);
			boolean isProductServicePresentAfterDeactivate = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//*[contains(text () , '" + productName + "')]");
			if (isProductServicePresentAfterDeactivate == false) {
				fc.utobj().throwsException("Product/Service still present after deactivatation");
			}
			fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}

	}

	// Anukaran

	@Test(groups = { "crmmodule", "crm"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2017-06-14", testCaseDescription = "Log a task against the contact>Verify the task at calendar", testCaseId = "TC_329_Verify_Group_Meeting_At_Calendor")
	public void verifyCreateGroupMeetingAtCalendor()
			throws IOException, Exception, ParserConfigurationException, SAXException {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			CRMContactsPage pobj = new CRMContactsPage(driver);

			fc.utobj().printTestStep("Navigate To Admin Users Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User1");
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corpTest = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			// String userName =
			// fc.utobj().generateTestData(dataSet.get("userName"));
			// String userNameCorp = userName;

			String emailId = "crmautomation@staffex.com";
			CorporateUser corpUser0 = new CorporateUser();
			corpUser0 = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser0);
			corpUser0.setEmail(emailId);
			corpUser0 = corpTest.createDefaultUser(driver, corpUser0);

			fc.utobj().printTestStep("Add Corporate User2");
			// String userName1 =
			// fc.utobj().generateTestData(dataSet.get("userName"));
			// String userNameCorp1 = userName1;
			CorporateUser corpUser1 = new CorporateUser();
			corpUser1 = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser1);
			corpUser1.setEmail(emailId);
			corpUser1 = corpTest.createDefaultUser(driver, corpUser1);

			fc.utobj().printTestStep("Add Corporate User3");
			// String userName2 =
			// fc.utobj().generateTestData(dataSet.get("userName"));
			// String userNameCorp2 = userName2;
			CorporateUser corpUser2 = new CorporateUser();
			corpUser2 = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser2);
			corpUser2.setEmail(emailId);
			corpUser2 = corpTest.createDefaultUser(driver, corpUser2);

			fc.home_page().logout(driver);
			fc.loginpage().loginWithParameter(driver, corpUser0.getUserName(), corpUser0.getPassword());
			fc.crm().crm_common().CRMContactsLnk(driver);

			String meetingSubject = fc.utobj().generateTestData(dataSet.get("meetingSubject"));
			fc.utobj().clickElement(driver, pobj.calendarLink);
			fc.utobj().clickElement(driver, pobj.createCalendarEvent);
			fc.utobj().selectDropDown(driver, pobj.schedule, "Meeting");
			fc.utobj().sendKeys(driver, pobj.meetingSubject, meetingSubject);
			fc.utobj().selectDropDown(driver, pobj.meetingScheduleScope, "Group");
			fc.utobj().clickElement(driver, pobj.participantsLnk);
			fc.commonMethods().switch_cboxIframe_frameId(driver);

			fc.utobj().sendKeys(driver, pobj.searchCorpUsers, corpUser0.getuserFullName());
			fc.utobj().clickElement(driver, pobj.searchKey);
			fc.utobj().sleep(2000);
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//*[@id='corpDiv']//td[contains(text(),'"
							+ corpUser0.getuserFullName() + "')]/ancestor::tr/td/input[@name='userToCorp']"));
			fc.utobj().sendKeys(driver, pobj.searchCorpUsers, corpUser1.getuserFullName());
			fc.utobj().clickElement(driver, pobj.searchKey);
			fc.utobj().sleep(2000);
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//*[@id='corpDiv']//td[contains(text(),'"
							+ corpUser1.getuserFullName() + "')]/ancestor::tr/td/input[@name='userToCorp']"));
			fc.utobj().sendKeys(driver, pobj.searchCorpUsers, corpUser2.getuserFullName());
			fc.utobj().clickElement(driver, pobj.searchKey);
			fc.utobj().sleep(2000);
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//*[@id='corpDiv']//td[contains(text(),'"
							+ corpUser2.getuserFullName() + "')]/ancestor::tr/td/input[@name='userToCorp']"));

			fc.utobj().clickElement(driver, pobj.addBtn);
			fc.utobj().switchFrameToDefault(driver);
			fc.utobj().selectDropDown(driver,
					fc.utobj().getElementByXpath(driver, ".//*[@id='form2']//select[@name='APM']"), "AM");
			fc.utobj().clickElement(driver, pobj.hContactSubmit);

			boolean isTextPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//*[contains(text () , '" + meetingSubject + "')]");
			if (isTextPresent == false) {
				fc.utobj().throwsException("was not able to verify meeting");
			}

			fc.home_page().logout(driver);
			fc.loginpage().loginWithParameter(driver, corpUser1.getUserName(), corpUser1.getPassword());

			fc.crm().crm_common().CRMContactsLnk(driver);
			fc.utobj().clickElement(driver, pobj.calendarLink);
			boolean isTextPresent2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//*[contains(text () , '" + meetingSubject + "')]");
			if (isTextPresent2 == false) {
				fc.utobj().throwsException("was not able to verify meeting");
			}

			fc.home_page().logout(driver);
			fc.loginpage().loginWithParameter(driver, corpUser2.getUserName(), corpUser2.getPassword());

			fc.crm().crm_common().CRMContactsLnk(driver);
			fc.utobj().clickElement(driver, pobj.calendarLink);
			boolean isTextPresent3 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//*[contains(text () , '" + meetingSubject + "')]");
			if (isTextPresent3 == false) {
				fc.utobj().throwsException("was not able to verify meeting");
			}

			fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}

	}

	// Anukaran

	@Test(groups = { "crmmodule", "crm"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Log a task against the contact>Verify the task at calendor", testCaseId = "TC_328_Verify_Group_Event_At_Calendor")
	public void verifyCreateGroupEventAtCalendor()
			throws IOException, Exception, ParserConfigurationException, SAXException {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			CRMContactsPage pobj = new CRMContactsPage(driver);

			fc.utobj().printTestStep("Naviaget To Admin Users Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User1");
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corpTest = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			// String userName =
			// fc.utobj().generateTestData(dataSet.get("userName"));
			// String userNameCorp = userName;
			//String emailId = "crmautomation@staffex.com";
			String emailId=fc.utobj().generateTestData("crmautomation")+"@staffex.com";
			CorporateUser corpUser0 = new CorporateUser();
			corpUser0 = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser0);
			corpUser0.setEmail(emailId);
			corpUser0 = corpTest.createDefaultUser(driver, corpUser0);

			fc.utobj().printTestStep("Add Corporate User2");
			// String userName1 =
			// fc.utobj().generateTestData(dataSet.get("userName"));
			// String userNameCorp1 = userName1;
			CorporateUser corpUser1 = new CorporateUser();
			corpUser1 = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser1);
			corpUser1.setEmail(emailId);
			corpUser1 = corpTest.createDefaultUser(driver, corpUser1);

			fc.utobj().printTestStep("Add Corporate User3");
			// String userName2 =
			// fc.utobj().generateTestData(dataSet.get("userName"));
			// String userNameCorp2 = userName2;
			CorporateUser corpUser2 = new CorporateUser();
			corpUser2 = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser2);
			corpUser2.setEmail(emailId);
			corpUser2 = corpTest.createDefaultUser(driver, corpUser2);

			fc.home_page().logout(driver);
			fc.loginpage().loginWithParameter(driver, corpUser0.getUserName(), corpUser2.getPassword());

			fc.crm().crm_common().CRMContactsLnk(driver);

			String meetingSubject = fc.utobj().generateTestData(dataSet.get("meetingSubject"));
			fc.utobj().clickElement(driver, pobj.calendarLink);
			fc.utobj().clickElement(driver, pobj.createCalendarEvent);
			fc.utobj().selectDropDown(driver, pobj.schedule, "Event");
			fc.utobj().sendKeys(driver, pobj.meetingSubject, meetingSubject);
			fc.utobj().selectDropDown(driver, pobj.meetingScheduleScope, "Group");
			fc.utobj().clickElement(driver, pobj.participantsLnk);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().sendKeys(driver, pobj.searchCorpUsers, corpUser0.getuserFullName());
			fc.utobj().clickElement(driver, pobj.searchKey);
			fc.utobj().sleep(3000);
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//*[@id='corpDiv']//td[contains(text(),'"
							+ corpUser0.getuserFullName() + "')]/ancestor::tr/td/input[@name='userToCorp']"));
			fc.utobj().sendKeys(driver, pobj.searchCorpUsers, corpUser1.getuserFullName());
			fc.utobj().clickElement(driver, pobj.searchKey);
			fc.utobj().sleep(3000);
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//*[@id='corpDiv']//td[contains(text(),'"
							+ corpUser1.getuserFullName() + "')]/ancestor::tr/td/input[@name='userToCorp']"));
			fc.utobj().sendKeys(driver, pobj.searchCorpUsers, corpUser2.getuserFullName());
			fc.utobj().clickElement(driver, pobj.searchKey);
			fc.utobj().sleep(3000);
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//*[@id='corpDiv']//td[contains(text(),'"
							+ corpUser2.getuserFullName() + "')]/ancestor::tr/td/input[@name='userToCorp']"));
			fc.utobj().clickElement(driver, pobj.addBtn);
			fc.utobj().switchFrameToDefault(driver);
			fc.utobj().selectDropDown(driver,
					fc.utobj().getElementByXpath(driver, ".//*[@id='form2']//select[@name='APM']"), "AM");
			fc.utobj().clickElement(driver, pobj.hContactSubmit);

			fc.utobj().clickElement(driver, pobj.calendarLink);
			boolean isTextPresent1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//*[contains(text () , '" + meetingSubject + "')]");
			if (isTextPresent1 == false) {
				fc.utobj().throwsException("was not able to verify meeting");
			}

			fc.home_page().logout(driver);
			fc.loginpage().loginWithParameter(driver, corpUser1.getUserName(), corpUser1.getPassword());

			fc.crm().crm_common().CRMContactsLnk(driver);
			fc.utobj().clickElement(driver, pobj.calendarLink);
			boolean isTextPresent2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//*[contains(text () , '" + meetingSubject + "')]");
			if (isTextPresent2 == false) {
				fc.utobj().throwsException("was not able to verify meeting");
			}

			fc.home_page().logout(driver);
			fc.loginpage().loginWithParameter(driver, corpUser2.getUserName(), corpUser2.getPassword());

			fc.crm().crm_common().CRMContactsLnk(driver);
			fc.utobj().clickElement(driver, pobj.calendarLink);
			boolean isTextPresent3 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//*[contains(text () , '" + meetingSubject + "')]");
			if (isTextPresent3 == false) {
				fc.utobj().throwsException("was not able to verify meeting");
			}

			fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}

	}

	// Anukaran Mishra

	@Test(groups = { "crmmoduleformgen" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Add Section with all types of custom fields under the new tab > add a contact through External Form", testCaseId = "TC_324_Verify_New_Conatact_Tab")
	public void verifyNewContactTab() throws IOException, Exception, ParserConfigurationException, SAXException {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		AdminInfoMgrCommonMethods adminCommonMethods = fc.adminpage().adminInfoMgr();
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		// CRM_CorporateUser crmCU= new CRM_CorporateUser();

		try {
			driver = fc.loginpage().login(driver);
			CRMLeadsPage pobj = new CRMLeadsPage(driver);

			/*
			 * fc.utobj().printTestStep(testCaseId,
			 * "Navigate To Admin Users Manage Corporate User");
			 * fc.utobj().printTestStep(testCaseId, "Add Corporate User");
			 * AdminUsersManageCorporateUsersAddCorporateUserPageTest corpTest =
			 * new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			 * String CuserName =
			 * fc.utobj().generateTestData(dataSet.get("userName")); String
			 * userName = corpTest.addCorporateUser(driver, CuserName, config);
			 * fc.home_page().logout(driver, config);
			 * fc.loginpage().loginWithParameter(driver, CuserName, corpUser.getPassword());
			 */
			fc.utobj().printTestStep("Navigate To Admin > CRM > Manage Form Generator");
			fc.utobj().printTestStep("Add New Tab");
			// fc.crm().crm_common().CRMLeadsLnk( driver);

			fc.crm().crm_common().adminCRMManageFormGeneratorLnk(driver);
			fc.adminpage().adminInfoMgr();
			AdminInfoMgrManageFormGeneratorPage manageFormGeneratorPage = new AdminInfoMgrManageFormGeneratorPage(
					driver);
			AdminInfoMgrManageFormGeneratorTabDetailsPage ADM_form_gen = manageFormGeneratorPage.getAddTabDetailsPage();
			adminCommonMethods.NavigateToManageFormGeneratorTabs(driver, "Contact");

			fc.utobj().clickElement(driver, manageFormGeneratorPage.btnAddNewTab);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			AdminInfoMgrManageFormGeneratorAddTabPage objAddNewTabPage = new AdminInfoMgrManageFormGeneratorAddTabPage(
					driver);
			String newTabName = fc.utobj().generateTestData("New Tab");
			fc.utobj().sendKeys(driver, objAddNewTabPage.txtDisplayName, newTabName);
			fc.utobj().clickElement(driver, objAddNewTabPage.multipleInputNo);
			fc.utobj().clickElement(driver, objAddNewTabPage.btnSubmit);
			fc.utobj().switchFrameToDefault(driver);
			fc.utobj().clickLink(driver, newTabName);

			fc.utobj().clickElement(driver, ADM_form_gen.btnAddSection);

			fc.commonMethods().switch_cboxIframe_frameId(driver);
			String SectionName = "New Section";
			SectionName = fc.utobj().generateTestData(SectionName);

			fc.utobj().sendKeys(driver, ADM_form_gen.txtSectionName, SectionName);
			fc.utobj().clickElement(driver, ADM_form_gen.btnAddSectionName);
			fc.utobj().switchFrameToDefault(driver);
			fc.utobj().clickElement(driver, ADM_form_gen.btnAddField);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			String FieldName2 = "Text Area";
			FieldName2 = fc.utobj().generateTestData(FieldName2);
			fc.utobj().sendKeys(driver, ADM_form_gen.txtFieldName, FieldName2);
			// fc.utobj().clickElement(driver, ADM_form_gen.documentopt);
			fc.utobj().selectDropDown(driver, ADM_form_gen.FieldType, "Text Area");
			fc.utobj().clickElement(driver, ADM_form_gen.btnSumbitField);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().clickElement(driver, ADM_form_gen.btnAddField);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			String FieldName1 = "Text";
			FieldName1 = fc.utobj().generateTestData(FieldName1);
			fc.utobj().sendKeys(driver, ADM_form_gen.txtFieldName, FieldName1);
			// fc.utobj().clickElement(driver, ADM_form_gen.documentopt);
			fc.utobj().selectDropDown(driver, ADM_form_gen.FieldType, "Text");
			fc.utobj().clickElement(driver, ADM_form_gen.btnSumbitField);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().clickElement(driver, ADM_form_gen.btnAddField);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			String FieldName3 = "Date";
			FieldName3 = fc.utobj().generateTestData(FieldName3);
			fc.utobj().sendKeys(driver, ADM_form_gen.txtFieldName, FieldName3);
			// fc.utobj().clickElement(driver, ADM_form_gen.documentopt);
			fc.utobj().selectDropDown(driver, ADM_form_gen.FieldType, "Date");
			fc.utobj().clickElement(driver, ADM_form_gen.btnSumbitField);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().clickElement(driver, ADM_form_gen.btnAddField);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			String FieldName4 = "DropDown";
			FieldName4 = fc.utobj().generateTestData(FieldName4);
			fc.utobj().sendKeys(driver, ADM_form_gen.txtFieldName, FieldName4);
			// fc.utobj().clickElement(driver, ADM_form_gen.documentopt);
			fc.utobj().selectDropDown(driver, ADM_form_gen.FieldType, "Drop-down");
			fc.utobj().sendKeys(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='row_1']/td[2]/input[1]"),
					"Opt" + FieldName4);

			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//*[@id='removeTDForAddMore']//*[contains(text(),'Add Option')]"));
			fc.utobj().sendKeys(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='row_2']/td[2]/input[1]"),
					"Opt2");
			fc.utobj().clickElement(driver, ADM_form_gen.btnSumbitField);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().clickElement(driver, ADM_form_gen.btnAddField);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			String FieldName5 = "Radio";
			FieldName5 = fc.utobj().generateTestData(FieldName5);
			fc.utobj().sendKeys(driver, ADM_form_gen.txtFieldName, FieldName5);
			// fc.utobj().clickElement(driver, ADM_form_gen.documentopt);
			fc.utobj().selectDropDown(driver, ADM_form_gen.FieldType, "Radio");
			fc.utobj().sendKeys(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='row_1']/td[2]/input[1]"),
					"Opt" + FieldName5);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//*[@id='removeTDForAddMore']//*[contains(text(),'Add Option')]"));
			fc.utobj().sendKeys(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='row_2']/td[2]/input[1]"),
					"Opt2" + FieldName5);
			fc.utobj().clickElement(driver, ADM_form_gen.btnSumbitField);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().clickElement(driver, ADM_form_gen.btnAddField);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			String FieldName6 = "Checkbox";
			FieldName6 = fc.utobj().generateTestData(FieldName6);
			fc.utobj().sendKeys(driver, ADM_form_gen.txtFieldName, FieldName6);
			// fc.utobj().clickElement(driver, ADM_form_gen.documentopt);
			fc.utobj().selectDropDown(driver, ADM_form_gen.FieldType, "Checkbox");
			fc.utobj().sendKeys(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='row_1']/td[2]/input[1]"),
					"Opt1");
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//*[@id='removeTDForAddMore']//*[contains(text(),'Add Option')]"));
			fc.utobj().sendKeys(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='row_2']/td[2]/input[1]"),
					"Opt2");
			fc.utobj().clickElement(driver, ADM_form_gen.btnSumbitField);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().clickElement(driver, ADM_form_gen.btnAddField);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			String FieldName7 = "Test Field3";
			FieldName7 = fc.utobj().generateTestData(FieldName7);
			fc.utobj().sendKeys(driver, ADM_form_gen.txtFieldName, FieldName7);
			// fc.utobj().clickElement(driver, ADM_form_gen.documentopt);
			fc.utobj().selectDropDown(driver, ADM_form_gen.FieldType, "Numeric");
			fc.utobj().clickElement(driver, ADM_form_gen.btnSumbitField);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().clickElement(driver, ADM_form_gen.btnAddField);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			String FieldName8 = "Multislect";
			FieldName8 = fc.utobj().generateTestData(FieldName8);
			fc.utobj().sendKeys(driver, ADM_form_gen.txtFieldName, FieldName8);
			// fc.utobj().clickElement(driver, ADM_form_gen.documentopt);
			fc.utobj().selectDropDown(driver, ADM_form_gen.FieldType, "Multi Select Drop-down");
			fc.utobj().sendKeys(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='row_1']/td[2]/input[1]"),
					"Mul" + FieldName8);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//*[@id='removeTDForAddMore']//*[contains(text(),'Add Option')]"));
			fc.utobj().sendKeys(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='row_2']/td[2]/input[1]"),
					"Opt2");
			fc.utobj().clickElement(driver, ADM_form_gen.btnSumbitField);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().clickElement(driver, ADM_form_gen.btnAddField);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			String FieldName9 = "Test Field3";
			FieldName9 = fc.utobj().generateTestData(FieldName9);
			fc.utobj().sendKeys(driver, ADM_form_gen.txtFieldName, FieldName9);
			// fc.utobj().clickElement(driver, ADM_form_gen.documentopt);
			fc.utobj().selectDropDown(driver, ADM_form_gen.FieldType, "Document");
			fc.utobj().clickElement(driver, ADM_form_gen.btnSumbitField);
			fc.utobj().switchFrameToDefault(driver);

			AdminCRMManageWebFormGeneratorPageTest webFormPage = new AdminCRMManageWebFormGeneratorPageTest();
			// create Web form (Lead)
			String status = "New";
			String contactMedium = "Email";
			String leadSource = "Import";
			String sourceDetails = "Import";
			fc.utobj().printTestStep("Navigate To Admin > CRM > Manage Web Form Generator");
			fc.utobj().printTestStep("Create New Lead Form");
			String formFormat = "Single Page";
			String header = fc.utobj().generateTestData(dataSet.get("header"));
			String textLabel = fc.utobj().generateTestData(dataSet.get("textLabel"));
			String fieldLabel = fc.utobj().generateTestData(dataSet.get("fieldLabel"));
			String footer = fc.utobj().generateTestData(dataSet.get("footer"));
			String assignTo = "Default Owner";
			String submissionType = dataSet.get("submissionType");
			String formName = fc.utobj().generateTestData(dataSet.get("formName"));
			String formTitle = fc.utobj().generateTestData(dataSet.get("formTitle"));
			String description = fc.utobj().generateTestData(dataSet.get("description"));
			String formUrl = fc.utobj().generateTestData(dataSet.get("formUrl"));
			String sectionName = fc.utobj().generateTestData("sectionName");
			String userNameReg = "";
			String userNameFranchise = "";
			String afterSubmission = dataSet.get("afterSubmission");
			String tabName = fc.utobj().generateTestData(dataSet.get("tabName"));
			String redirectedUrl = dataSet.get("reUrl");
			AdminCRMManageWebFormGeneratorPage CrmWebFormPage = new AdminCRMManageWebFormGeneratorPage(driver);
			String LName = fc.utobj().generateTestData(dataSet.get("Lastname"));
			String FName = fc.utobj().generateTestData(dataSet.get("Firstname"));
			// leadSource=sourceDetails;
			String userNameCorp = "FranConnect Administrator";
			String regionName = "";
			String franchiseId = "";

			AdminCRMContactTypeConfigurationPageTest contatcTypePage = new AdminCRMContactTypeConfigurationPageTest();
			formUrl = fc.utobj().generateTestData(dataSet.get("formUrl"));
			String contactType = fc.utobj().generateTestData(dataSet.get("contactType"));
			contatcTypePage.addContactType(driver, contactType);
			// webFormPage.createNewFormContactType(driver, formName, formTitle,
			// description, formFormat, submissionType, formUrl, sectionName,
			// tabName, header, textLabel, fieldLabel, footer, contactType,
			// status, contactMedium, leadSource, sourceDetails, assignTo,
			// userNameCorp, regionName, userNameReg, franchiseId,
			// userNameFranchise, afterSubmission, redirectedUrl, config);

			String urlText = null;
			try {
				AdminCRMManageWebFormGeneratorPage WebForm, webForm = new AdminCRMManageWebFormGeneratorPage(driver);
				fc.crm().crm_common().adminCRMManageWebFormGeneratorLnk(driver);
				fc.utobj().clickElement(driver, webForm.createNewForm);
				fc.utobj().sendKeys(driver, webForm.formName, formName);
				fc.utobj().sendKeys(driver, webForm.formDisplayTitle, formTitle);
				if (!fc.utobj().isSelected(driver, webForm.displayFormTitleCheckBox)) {
					fc.utobj().clickElement(driver, webForm.displayFormTitleCheckBox);
				}
				fc.utobj().sendKeys(driver, webForm.formDescription, description);

				if (!fc.utobj().isSelected(driver, webForm.formTypeContact)) {
					fc.utobj().clickElement(driver, webForm.formTypeContact);
				}
				if (formFormat.equalsIgnoreCase("Single Page")) {
					fc.utobj().selectDropDown(driver, webForm.formFormat, "Single Page");

				} else if (formFormat.equalsIgnoreCase("Multi Page")) {
					fc.utobj().selectDropDown(driver, webForm.formFormat, "Multi Page");
					fc.utobj().selectDropDown(driver, webForm.submissionType, submissionType);
				}
				fc.utobj().selectDropDown(driver, webForm.columnCount, "1");
				fc.utobj().selectDropDown(driver, webForm.filedLabelAlignment, "Left");
				try {
					fc.utobj().selectDropDown(driver, webForm.formLanguage, "English");
				} catch (Exception e) {

				}
				fc.utobj().sendKeys(driver, webForm.iframeWidth, "100%");
				fc.utobj().sendKeys(driver, webForm.iframeHeight, "500");
				fc.utobj().sendKeys(driver, webForm.formUrl, formUrl);
				fc.utobj().clickElement(driver, webForm.saveNextBtn);

				// 1.Details Page

				if (formFormat.equalsIgnoreCase("Single Page")) {
					// Add Section
					fc.utobj().clickElement(driver, webForm.addSection);
					fc.commonMethods().switch_cboxIframe_frameId(driver);
					fc.utobj().sendKeys(driver, webForm.sectionName, sectionName);
					fc.utobj().selectDropDown(driver, webForm.noOfCols, "1");
					fc.utobj().selectDropDown(driver, webForm.fieldLabelAlignmentSection, "Left");
					fc.utobj().clickElement(driver, webForm.addBtn);
					fc.utobj().switchFrameToDefault(driver);
				} else if (formFormat.equalsIgnoreCase("Multi Page")) {
					fc.utobj().clickElement(driver, webForm.addTab);
					fc.commonMethods().switch_cboxIframe_frameId(driver);
					fc.utobj().sendKeys(driver, webForm.sectionName, tabName);
					fc.utobj().clickElement(driver, webForm.addBtn);
					fc.utobj().switchFrameToDefault(driver);
				}
				// add Header
				fc.utobj().doubleClickElement(driver, webForm.addHeader);
				fc.commonMethods().switch_cboxIframe_frameId(driver);
				fc.utobj().switchFrameById(driver, "textBlock_ifr");
				WebElement editorTextArea = fc.utobj().getElementByXpath(driver, ".//*[@id='tinymce']/p");
				Actions actions = new Actions(driver);
				actions.moveToElement(editorTextArea);
				actions.click();
				actions.sendKeys(header);
				fc.utobj().logReport("Entering Value in Text Field", editorTextArea);
				fc.utobj().logReportMsg("Entered Text", header);
				actions.build().perform();
				fc.utobj().switchFrameToDefault(driver);
				fc.commonMethods().switch_cboxIframe_frameId(driver);
				fc.utobj().clickElement(driver, webForm.saveBtn);
				fc.utobj().switchFrameToDefault(driver);

				if (formFormat.equalsIgnoreCase("Multi Page")) {
					fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
							".//*[contains(@id , 'defaultTab') and contains(@id , 'Tab') and contains(text () ,'Default Tab')]"));
				}
				// Add Text
				fc.utobj().doubleClickElement(driver, webForm.defaultSectionHeaderAddText);
				fc.commonMethods().switch_cboxIframe_frameId(driver);
				fc.utobj().sendKeys(driver, webForm.labelAddText, textLabel);
				fc.utobj().selectDropDown(driver, webForm.headerLabelAlignment, "Left");
				fc.utobj().clickElement(driver, webForm.saveBtn);
				fc.utobj().switchFrameToDefault(driver);
				// FormTool
				fc.utobj().clickElement(driver, webForm.availableFieldsTab);
				fc.utobj().clickElement(driver, webForm.formTools);

				fc.utobj().dragAndDropElement(driver, webForm.formToolText, webForm.defaultDropHere);

				/*
				 * fc.utobj().dragAndDropElement(driver,
				 * webForm.formToolCaptcha, webForm.defaultDropHere);
				 * fc.utobj().dragAndDropElement(driver,
				 * webForm.formToolIAgreeCheckBox, webForm.defaultDropHere);
				 */

				// Contact Info
				fc.utobj().clickElement(driver, webForm.primaryInfo);
				fc.utobj().sendKeys(driver, webForm.searchField, "Title");

				fc.utobj().dragAndDropElement(driver, fc.utobj().getElementByXpath(driver, ".//a[.='Title']"),
						webForm.defaultDropHere);

				fc.utobj().sendKeys(driver, webForm.searchField, "Address");

				fc.utobj().dragAndDropElement(driver, fc.utobj().getElementByXpath(driver, ".//a[.='Address']"),
						webForm.defaultDropHere);

				fc.utobj().sendKeys(driver, webForm.searchField, "City");

				fc.utobj().dragAndDropElement(driver, fc.utobj().getElementByXpath(driver, ".//a[.='City']"),
						webForm.defaultDropHere);

				fc.utobj().sendKeys(driver, webForm.searchField, "Country");

				fc.utobj().dragAndDropElement(driver, fc.utobj().getElementByXpath(driver, ".//a[.='Country']"),
						webForm.defaultDropHere);

				fc.utobj().sendKeys(driver, webForm.searchField, "Zip / Postal Code");

				fc.utobj().dragAndDropElement(driver,
						fc.utobj().getElementByXpath(driver, ".//a[.='Zip / Postal Code']"), webForm.defaultDropHere);

				fc.utobj().sendKeys(driver, webForm.searchField, "State / Province");

				fc.utobj().dragAndDropElement(driver,
						fc.utobj().getElementByXpath(driver, ".//a[.='State / Province']"), webForm.defaultDropHere);

				fc.utobj().sendKeys(driver, webForm.searchField, "Phone");

				fc.utobj().dragAndDropElement(driver, fc.utobj().getElementByXpath(driver, ".//a[.='Phone']"),
						webForm.defaultDropHere);

				fc.utobj().sendKeys(driver, webForm.searchField, "Phone Extension");

				fc.utobj().dragAndDropElement(driver, fc.utobj().getElementByXpath(driver, ".//a[.='Phone Extension']"),
						webForm.defaultDropHere);

				fc.utobj().sendKeys(driver, webForm.searchField, "Fax");

				fc.utobj().dragAndDropElement(driver, fc.utobj().getElementByXpath(driver, ".//a[.='Fax']"),
						webForm.defaultDropHere);

				fc.utobj().sendKeys(driver, webForm.searchField, "Mobile");

				fc.utobj().dragAndDropElement(driver, fc.utobj().getElementByXpath(driver, ".//a[.='Mobile']"),
						webForm.defaultDropHere);

				fc.utobj().sendKeys(driver, webForm.searchField, "Alternate Email");

				fc.utobj().dragAndDropElement(driver, fc.utobj().getElementByXpath(driver, ".//a[.='Alternate Email']"),
						webForm.defaultDropHere);

				fc.utobj().sendKeys(driver, webForm.searchField, "Job Title");

				fc.utobj().dragAndDropElement(driver, fc.utobj().getElementByXpath(driver, ".//a[.='Job Title']"),
						webForm.defaultDropHere);

				fc.utobj().sendKeys(driver, webForm.searchField, "Suffix");

				fc.utobj().dragAndDropElement(driver, fc.utobj().getElementByXpath(driver, ".//a[.='Suffix']"),
						webForm.defaultDropHere);

				fc.utobj().sendKeys(driver, webForm.searchField, "Birthdate");

				fc.utobj().dragAndDropElement(driver, fc.utobj().getElementByXpath(driver, ".//a[.='Birthdate']"),
						webForm.defaultDropHere);

				fc.utobj().sendKeys(driver, webForm.searchField, "Anniversary Date");

				fc.utobj().dragAndDropElement(driver,
						fc.utobj().getElementByXpath(driver, ".//a[.='Anniversary Date']"), webForm.defaultDropHere);

				fc.utobj().sendKeys(driver, webForm.searchField, "How did you hear about us?(Contact Source)");

				fc.utobj().dragAndDropElement(driver,
						driver.findElement(
								By.xpath(".//a[contains(text () ,'How did you hear about us?(Contact Source)')]")),
						webForm.defaultDropHere);

				fc.utobj().sendKeys(driver, webForm.searchField, "Best Time to Contact");

				fc.utobj().dragAndDropElement(driver,
						fc.utobj().getElementByXpath(driver, ".//a[.='Best Time to Contact']"),
						webForm.defaultDropHere);

				fc.utobj().sendKeys(driver, webForm.searchField, "Comments");

				fc.utobj().dragAndDropElement(driver, fc.utobj().getElementByXpath(driver, ".//a[.='Comments']"),
						webForm.defaultDropHere);

				fc.utobj().sendKeys(driver, webForm.searchField, "Facebook");

				fc.utobj().dragAndDropElement(driver, fc.utobj().getElementByXpath(driver, ".//a[.='Facebook']"),
						webForm.defaultDropHere);

				fc.utobj().sendKeys(driver, webForm.searchField, "Twitter");

				fc.utobj().clickElement(driver,
						fc.utobj().getElementByXpath(driver, ".//a[contains(text () ,'" + newTabName + "')]"));
				fc.utobj().dragAndDropElement(driver, fc.utobj().getElementByXpath(driver, ".//a[.='Twitter']"),
						webForm.defaultDropHere);

				fc.utobj().dragAndDropElement(driver,
						fc.utobj().getElementByXpath(driver, ".//a[contains(text(),'" + FieldName1 + "')]"),
						webForm.defaultDropHere);

				fc.utobj().dragAndDropElement(driver,
						fc.utobj().getElementByXpath(driver, ".//a[contains(text(),'" + FieldName2 + "')]"),
						webForm.defaultDropHere);

				fc.utobj().dragAndDropElement(driver,
						fc.utobj().getElementByXpath(driver, ".//a[contains(text(),'" + FieldName3 + "')]"),
						webForm.defaultDropHere);

				fc.utobj().dragAndDropElement(driver,
						fc.utobj().getElementByXpath(driver, ".//a[contains(text(),'" + FieldName4 + "')]"),
						webForm.defaultDropHere);

				fc.utobj().dragAndDropElement(driver,
						fc.utobj().getElementByXpath(driver, ".//a[contains(text(),'" + FieldName5 + "')]"),
						webForm.defaultDropHere);

				fc.utobj().dragAndDropElement(driver,
						fc.utobj().getElementByXpath(driver, ".//a[contains(text(),'" + FieldName6 + "')]"),
						webForm.defaultDropHere);

				fc.utobj().dragAndDropElement(driver,
						fc.utobj().getElementByXpath(driver, ".//a[contains(text(),'" + FieldName7 + "')]"),
						webForm.defaultDropHere);

				fc.utobj().dragAndDropElement(driver,
						fc.utobj().getElementByXpath(driver, ".//a[contains(text(),'" + FieldName8 + "')]"),
						webForm.defaultDropHere);

				if (formFormat.equalsIgnoreCase("Single Page")) {
					// add Text in custom section
					fc.utobj().doubleClickElement(driver, fc.utobj().getElementByXpath(driver,
							".//*[.='" + sectionName + "']/following-sibling::table//ul/li[@class='emptyList']"));
					fc.commonMethods().switch_cboxIframe_frameId(driver);
					fc.utobj().sendKeys(driver, webForm.labelAddText, fieldLabel);
					fc.utobj().selectDropDown(driver, webForm.headerLabelAlignment, "Left");
					fc.utobj().clickElement(driver, webForm.saveBtn);
					fc.utobj().switchFrameToDefault(driver);

					// add Text Drop Here Custom
					fc.utobj().clickElement(driver, webForm.formTools);
					fc.utobj().dragAndDropElement(driver, webForm.formToolText,
							fc.utobj().getElementByXpath(driver, ".//*[.='" + sectionName
									+ "']/following-sibling::table//li[@class='emptyList']//p[.='Drop here.']"));

				} else if (formFormat.equalsIgnoreCase("Multi Page")) {
					fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//li[.='" + tabName + "']"));

					// add Text in custom section
					// click over double click
					String text = fc.utobj().getElementByXpath(driver, ".//li[.='" + tabName + "']").getAttribute("id");

					String tabName1 = tabName.toLowerCase();
					text = text.replace("_" + tabName1 + "_", "");
					text = text.replace("_Tab", "");

					String text1 = driver
							.findElement(By.xpath(
									".//div[contains(@id , '" + text + "')]/ul[contains(@id , '" + text + "')]/li"))
							.getAttribute("id");
					text1 = text1.replace("defaultSection_", "");

					fc.utobj().doubleClickElement(driver, fc.utobj().getElementByXpath(driver, ".//ul[contains(@id , '"
							+ text1 + "')]/li[@class='emptyList']//p[.='Double click or drag & drop to add text.']"));

					fc.commonMethods().switch_cboxIframe_frameId(driver);
					fc.utobj().sendKeys(driver, webForm.labelAddText, fieldLabel);
					fc.utobj().selectDropDown(driver, webForm.headerLabelAlignment, "Left");
					fc.utobj().clickElement(driver, webForm.saveBtn);
					fc.utobj().switchFrameToDefault(driver);

					// add Text Drop Here Custom
					fc.utobj().clickElement(driver, webForm.formTools);

					fc.utobj().dragAndDropElement(driver, webForm.formToolText, driver
							.findElement(By.xpath(".//ul[contains(@id , '" + text + "')]/li//p[.='Drop here.']")));

				}
				// add Footer
				fc.utobj().doubleClickElement(driver, webForm.addFooterDefault);
				fc.commonMethods().switch_cboxIframe_frameId(driver);
				fc.utobj().switchFrameById(driver, "textBlock_ifr");

				WebElement editorTextArea1 = fc.utobj().getElementByXpath(driver, ".//*[@id='tinymce']/p");
				Actions actions1 = new Actions(driver);
				actions1.moveToElement(editorTextArea1);
				actions1.click();
				actions1.sendKeys(footer);
				fc.utobj().logReport("Entering Value in Text Field", editorTextArea1);
				fc.utobj().logReportMsg("Entered Text", footer);
				actions1.build().perform();
				fc.utobj().switchFrameToDefault(driver);
				fc.commonMethods().switch_cboxIframe_frameId(driver);
				fc.utobj().clickElement(driver, webForm.saveBtn);
				fc.utobj().switchFrameToDefault(driver);
				fc.utobj().clickElement(driver, webForm.saveAndNextBtnDesign);

				// Setting
				fc.utobj().selectDropDown(driver, webForm.contactType, contactType);
				fc.utobj().selectDropDown(driver, webForm.cmLeadStatusID, status);
				fc.utobj().selectDropDown(driver, webForm.contactMediumSelect, contactMedium);
				fc.utobj().selectDropDown(driver, webForm.leadSource, leadSource);
				fc.utobj().selectDropDown(driver, webForm.leadSourceDetailsSelect, sourceDetails);
				if (assignTo.equalsIgnoreCase("default")) {
					if (!fc.utobj().isSelected(driver, webForm.assignToDefault)) {
						fc.utobj().clickElement(driver, webForm.assignToDefault);
					}
				} else if (assignTo.equalsIgnoreCase("Corporate")) {
					if (!fc.utobj().isSelected(driver, webForm.assignToCorporate)) {
						fc.utobj().clickElement(driver, webForm.assignToCorporate);
					}
					fc.utobj().selectDropDown(driver, webForm.selectCorporateUser, userNameCorp);

				} else if (assignTo.equalsIgnoreCase("Regional")) {
					if (!fc.utobj().isSelected(driver, webForm.assignToRegional)) {
						fc.utobj().clickElement(driver, webForm.assignToRegional);
					}
					fc.utobj().selectDropDown(driver, webForm.selectAreaRegion, regionName);
					fc.utobj().selectDropDown(driver, webForm.selectRegionalUser, userNameReg);

				} else if (assignTo.equalsIgnoreCase("Franchisee")) {

					if (!fc.utobj().isSelected(driver, webForm.assignToFranchise)) {
						fc.utobj().clickElement(driver, webForm.assignToFranchise);
					}
					fc.utobj().selectDropDown(driver, webForm.selectFranchiseID, franchiseId);
					fc.utobj().selectDropDown(driver, webForm.selectFranchiseUser, userNameFranchise);

				}
				if (afterSubmission.equalsIgnoreCase("message")) {
					if (!fc.utobj().isSelected(driver, webForm.afterSubmissionMsg)) {
						fc.utobj().clickElement(driver, webForm.afterSubmissionMsg);
					}
				} else if (afterSubmission.equalsIgnoreCase("url")) {
					if (!fc.utobj().isSelected(driver, webForm.afterSubmissionUrl)) {
						fc.utobj().clickElement(driver, webForm.afterSubmissionUrl);
					}
					fc.utobj().sendKeys(driver, webForm.redirectedUrl, redirectedUrl);
				}

				fc.utobj().clickElement(driver, webForm.finishBtn);

				if (!fc.utobj().isSelected(driver, webForm.hostURL)) {
					fc.utobj().clickElement(driver, webForm.hostURL);
				}

				urlText = webForm.hostCodeBox.getText().trim();
				fc.utobj().clickElement(driver, webForm.okBtn);

			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}

			String parentWindow = driver.getWindowHandle();
			fc.utobj().sendKeys(driver, CrmWebFormPage.searchMyForm, formName);
			fc.utobj().clickElement(driver, CrmWebFormPage.searchMyFormBtn);
			fc.utobj().actionImgOption(driver, formName, "Launch & Test");

			fc.utobj().printTestStep("Launc And Test Form");
			Set<String> allWindows2 = driver.getWindowHandles();
			for (String currentWindow : allWindows2) {
				if (!currentWindow.equalsIgnoreCase(parentWindow)) {
					driver.switchTo().window(currentWindow);
					String titleTextCurrent = driver.getTitle();
					if (titleTextCurrent.equalsIgnoreCase(formTitle)) {
						// fill the form with data

						fc.utobj().isTextDisplayed(driver, header, "was not able to verify Header");
						fc.utobj().isTextDisplayed(driver, formTitle, "was not able to verify Form Title");
						// fc.utobj().isTextDisplayed(driver, textLabel, "was
						// not able to verify Text Label");
						fc.utobj().isTextDisplayed(driver, footer, "was not able to verify Footer");

						fc.utobj().sendKeys(driver, CrmWebFormPage.contactFirstName, FName);
						fc.utobj().sendKeys(driver, CrmWebFormPage.contactLastName, LName);
						fc.utobj().selectDropDownByPartialText(driver, CrmWebFormPage.title, "Dr.");
						// fc.utobj().sendKeys(driver,
						// CrmWebFormPage.companyName, "companyName");
						fc.utobj().sendKeys(driver, CrmWebFormPage.address, "address");
						fc.utobj().sendKeys(driver, CrmWebFormPage.city, "city");
						fc.utobj().selectDropDownByPartialText(driver, CrmWebFormPage.country, "USA");
						// fc.utobj().sendKeys(driver, CrmWebFormPage.zipcode,
						// zipCode);
						fc.utobj().selectDropDownByPartialText(driver, CrmWebFormPage.state, "Alabama");
						fc.utobj().sendKeys(driver, CrmWebFormPage.phoneNumbers, "1236547896");
						fc.utobj().sendKeys(driver, CrmWebFormPage.extn, "12");
						fc.utobj().sendKeys(driver, CrmWebFormPage.faxNumbers, "1236577896");
						fc.utobj().sendKeys(driver, CrmWebFormPage.mobileNumbers, "9874563212");
						fc.utobj().sendKeys(driver, CrmWebFormPage.emailIds, "jhsdhfs@gmail.com");
						fc.utobj().sendKeys(driver, CrmWebFormPage.alternateEmail, "jhsdhdasdfs@gmail.com");
						fc.utobj().selectDropDownByPartialText(driver, CrmWebFormPage.cmSourceDetails, leadSource);

						fc.utobj().selectDropDownByPartialText(driver, CrmWebFormPage.howDidYouHearAboutUs,
								sourceDetails);

						fc.utobj().sendKeys(driver,
								fc.utobj().getElementByXpath(driver,
										".//td[contains(text () , '" + FieldName1 + "')]/following-sibling::td/input"),
								FieldName1);
						fc.utobj().sendKeys(driver, fc.utobj().getElementByXpath(driver,
								".//td[contains(text () , '" + FieldName2 + "')]/following-sibling::td/label/textarea"),
								FieldName2);
						fc.utobj()
								.sendKeys(driver,
										fc.utobj().getElementByXpath(driver,
												".//td[contains(text () , '" + FieldName3
														+ "')]/following-sibling::td/table/tbody/tr/td/input"),
										"02/02/2017");
						fc.utobj().clickElement(driver,
								fc.utobj().getElementByXpath(driver,
										".//td[contains(text () , '" + FieldName4
												+ "')]/following-sibling::td/select/option[contains(text(),'Opt"
												+ FieldName4 + "')]"));
						fc.utobj().clickElement(driver,
								fc.utobj().getElementByXpath(driver, ".//td[contains(text () , '" + FieldName5
										+ "')]/following-sibling::td/input[@type='radio' and @value='1']"));
						fc.utobj().sendKeys(driver,
								fc.utobj().getElementByXpath(driver,
										".//td[contains(text () , '" + FieldName7 + "')]/following-sibling::td/input"),
								"99");
						fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
								".//td[contains(text () , '" + FieldName6 + "')]/following-sibling::td/input"));
						try {
							fc.utobj().clickElement(driver,
									fc.utobj().getElementByXpath(driver,
											".//td[contains(text () , '" + FieldName8
													+ "')]/following-sibling::td/div/div/ul/li/label[contains(text(),'Mul"
													+ FieldName8 + "')]/input"));
						} catch (Exception e) {
						}
						String fileName = fc.utobj().getFilePathFromTestData(dataSet.get("fileName"));
						fc.utobj()
								.sendKeys(driver,
										fc.utobj()
												.getElementByXpath(driver,
														".//td[contains(text () , '" + FieldName9
																+ "')]/following-sibling::td/input[@type='file']"),
										fileName);

						// fc.utobj().clickElement(driver,
						// CrmWebFormPage.nextBtn);

						fc.utobj().isTextDisplayed(driver, header, "was not able to verify Header");
						fc.utobj().isTextDisplayed(driver, formTitle, "was not able to verify Form Title");
						// fc.utobj().isTextDisplayed(driver, tabName, "was not
						// able to verify Tab Name");
						// fc.utobj().isTextDisplayed(driver, fieldLabel, "was
						// not able to verify Fiedl Label");
						fc.utobj().isTextDisplayed(driver, footer, "was not able to verify Footer");
						fc.utobj().clickElement(driver, CrmWebFormPage.submitBtn);
						// fc.utobj().isTextDisplayed(driver, "Thank you for
						// submitting your information, we will get back to you
						// shortly.", "was not able to verify confirmation
						// Msg");

						driver.close();
					} else {
						driver.close();
						driver.switchTo().window(parentWindow);
					}
					driver.switchTo().window(parentWindow);
				}
			}

			fc.crm().crm_common().CRMContactsLnk(driver);
			fc.utobj().clickElement(driver, pobj.addNew);
			fc.utobj().clickElement(driver, pobj.contactLnk);
			fc.utobj().clickElement(driver, pobj.contactsLinks);
			contactsFilter(driver, contactType);
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//a[contains(text () , '" + FName + " " + LName + "')]"));

			boolean isVisibilityPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//*[contains(text () ,'" + FieldName1 + "')]");
			if (isVisibilityPresent == false) {
				fc.utobj().throwsException("was not able to " + FieldName1);
			}
			boolean isVisibilityPresent2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//*[contains(text () ,'" + FieldName2 + "')]");
			if (isVisibilityPresent2 == false) {
				fc.utobj().throwsException("was not able to " + FieldName2);
			}
			boolean isVisibilityPresent3 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//*[contains(text () ,'" + FieldName3 + "')]");
			if (isVisibilityPresent3 == false) {
				fc.utobj().throwsException("was not able to " + FieldName3);
			}
			try {
				boolean isVisibilityPresent4 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
						".//*[contains(text () ,'" + FieldName4 + "')]");
				if (isVisibilityPresent4 == false) {
					fc.utobj().throwsException("was not able to " + FieldName4);
				}
			} catch (Exception e) {
			}
			try {
				boolean isVisibilityPresent8 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
						".//*[contains(text () ,'" + FieldName8 + "')]");
				if (isVisibilityPresent8 == false) {
					fc.utobj().throwsException("was not able to " + FieldName4);
				}
			} catch (Exception e) {
			}
			boolean isVisibilityPresent5 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//*[contains(text () ,'" + FieldName7 + "')]");
			if (isVisibilityPresent5 == false) {
				fc.utobj().throwsException("was not able to " + FieldName6);
			}
			boolean isVisibilityPresent6 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//*[contains(text () ,'" + FieldName6 + "')]");
			if (isVisibilityPresent6 == false) {
				fc.utobj().throwsException("was not able to " + FieldName6);
			}
			fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	// Anukaran

	@Test(groups = { "crmmodule2a", "crm" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2017-06-21", testCaseDescription = "composing the email click on keywords and add location keywords >The keywords for owner should be displayed and replaced in the sent email. ", testCaseId = "TC_329_Verfy_email_Keyword_Contact")
	public void verifiEmailKeywordContact() throws IOException, Exception, ParserConfigurationException, SAXException {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			CRMContactsPage pobj = new CRMContactsPage(driver);
			String regionName = fc.utobj().generateTestData("TestRegion");
			String firstNameF = fc.utobj().generateTestData(dataSet.get("firstNameFid"));
			String lastNameF = fc.utobj().generateTestData(dataSet.get("lastNameFid"));
			String OwnerName = firstNameF + " " + lastNameF;
			fc.utobj().printTestStep("Naviaget To Admin > Franchise Location > Add Franchise Location");
			AdminFranchiseLocationAddFranchiseLocationPageTest franchiseLocation = new AdminFranchiseLocationAddFranchiseLocationPageTest();
			String franchiseId = fc.utobj().generateTestData(dataSet.get("franchiseId"));
			franchiseLocation.addFranchiseLocationForMkt(driver, franchiseId, regionName, firstNameF, lastNameF);

			// login as franchise user
			fc.utobj().printTestStep("Admin > Users > Manage Franchise Users");
			fc.utobj().printTestStep("Add Franchise User");
			AdminUsersManageManageFranchiseUsersPageTest addFranUser = new AdminUsersManageManageFranchiseUsersPageTest();
			String userNameF = fc.utobj().generateTestData(dataSet.get("userNameF"));
			String roleName = "Default Franchise Role";
			String password = "t0n1ght@123";
			String emailId = "crmautomation@staffex.com";
			addFranUser.addFranchiseUser(driver, userNameF, password, franchiseId, roleName, emailId);

			// fc.utobj().printTestStep(testCaseId, "Navigate To Admin > CRM >
			// Contact Type Configuration");
			// fc.utobj().printTestStep(testCaseId, "Add Contact Type");
			// AdminCRMContactTypeConfigurationPageTest contatcTypePage = new
			// AdminCRMContactTypeConfigurationPageTest();
			// String contactType =
			// fc.utobj().generateTestData(dataSet.get("contactType"));
			// contatcTypePage.addContactType(driver, contactType, config);
			fc.home_page().logout(driver);
			fc.loginpage().loginWithParameter(driver, userNameF, "t0n1ght@123");
			String contactType = "Contacts";

			fc.utobj().printTestStep("Navigate To CRM > Contacts > Contacts Summary Page");
			fc.utobj().printTestStep("Add Contact");
			fc.crm().crm_common().CRMContactsLnk(driver);
			fc.utobj().clickElement(driver, pobj.addNew);
			fc.utobj().clickElement(driver, pobj.contactLnk);

			String firstName = fc.utobj().generateTestData(dataSet.get("firstName"));
			String lastName = fc.utobj().generateTestData(dataSet.get("lastName"));
			String primaryCMethod = dataSet.get("primaryCMethod");
			String assignTo = "Franchise";
			franchiseId = "";
			String title = "Mr.";

			addContactGeneric(driver, contactType, firstName, lastName, primaryCMethod, assignTo, OwnerName, regionName,
					franchiseId, title, emailId, config);
			CRMLeadsPageTest leadsPage = new CRMLeadsPageTest();
			// fc.utobj().clickElement(driver, pobj.contactsLinks);
			leadsPage.contactSearchFromSearchpage(driver, firstName, lastName);
			// fc.utobj().sendKeys(driver, pobj.searchContacts, firstName);
			// fc.utobj().clickElement(driver, pobj.searchAccountBtn);
			// contactsFilter(driver, contactType);
			fc.utobj().printTestStep("Send Email with location keywords");
			fc.utobj().actionImgOption(driver, firstName + " " + lastName, "Send Email");

			String emailSubject = fc.utobj().generateTestData("SsubjectMail");
			fc.utobj().sendKeys(driver, pobj.subjectMail, emailSubject);
			// fc.utobj().clickElement(driver, pobj.insertKeyword);
			// fc.utobj().selectDropDown(driver, pobj.selectKeyword, "Location
			// City");
			String editorText = "LOCATION_CITY: $LOCATION_CITY$ \n"
					+ "LOCATION_STREET_ADDRESS1: $LOCATION_STREET_ADDRESS1$ \n"
					+ "LOCATION_STREET_ADDRESS2:  $LOCATION_STREET_ADDRESS2$ \n"
					+ "LOCATION_STATE : $LOCATION_STATE$ \n" + "LOCATION_ZIPCODE: $LOCATION_ZIPCODE$ \n"
					+ "LOCATION_PHONE: $LOCATION_PHONE$ \n" + "LOCATION_FAX: $LOCATION_FAX$ \n"
					+ "LOCATION_EMAIL: $LOCATION_EMAIL$ \n" + "LOCATION_WEBSITE: $LOCATION_WEBSITE$ \n";

			fc.utobj().switchFrameById(driver, "ta_ifr");

			Actions actions = new Actions(driver);
			actions.moveToElement(fc.utobj().getElement(driver, pobj.editorTextArea));
			actions.click();
			actions.sendKeys(editorText);
			fc.utobj().logReportMsg("Entered Text", editorText);
			actions.build().perform();
			fc.utobj().switchFrameToDefault(driver);
			fc.utobj().clickElement(driver, pobj.sendEmailBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Verify Send Email");

			String mailBody = "LOCATION_CITY:";
			Map<String, String> mailData = new HashMap<String, String>();
			mailData = fc.utobj().readMailBox(emailSubject, mailBody, emailId, "sdg@1a@Hfs");

			if (mailData.size() == 0 || !mailData.get("mailBody").contains("Street Address")) {
				fc.utobj().throwsException("was not able to verify keyword for Address Test Campaign Mail");
			}
			if (mailData.size() == 0 || !mailData.get("mailBody").contains("224122")) {
				fc.utobj().throwsException("was not able to verify keyword for zipcode Test Campaign Mail");
			}
			if (mailData.size() == 0 || !mailData.get("mailBody").contains("test city")) {
				fc.utobj().throwsException("was not able to verify keyword for City Test Campaign Mail");
			}
			if (mailData.size() == 0 || !mailData.get("mailBody").contains("11111")) {
				fc.utobj().throwsException("was not able to verify keyword for Fax Test Campaign Mail");
			}
			if (mailData.size() == 0 || !mailData.get("mailBody").contains("0001")) {
				fc.utobj().throwsException("was not able to verify keyword for phone Test Campaign Mail");
			}
			if (mailData.size() == 0 || !mailData.get("mailBody").contains("crmautomation@staffex.com")) {
				fc.utobj().throwsException("was not able to verify keyword for email Test Campaign Mail");
			}
			if (mailData.size() == 0 || !mailData.get("mailBody").contains("www.google.com")) {
				fc.utobj().throwsException("was not able to verify keyword for website Test Campaign Mail");
			}
			if (mailData.size() == 0 || !mailData.get("mailBody").contains("Alabama")) {
				fc.utobj().throwsException("was not able to verify keyword for state Test Campaign Mail");
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	public void contactsFilter(WebDriver driver, String contactType) throws Exception {

		CRMContactsPage pobj = new CRMContactsPage(driver);
		try {
			fc.utobj().clickElement(driver, pobj.showFilter);
		} catch (Exception e) {
		}
		fc.utobj().setToDefault(driver, pobj.contactOwnerSelect);
		fc.utobj().setToDefault(driver, pobj.franchiseIdSelect);
		try {
			fc.utobj().setToDefault(driver, pobj.contactTypeSelect);
			WebElement elementInput = fc.utobj().getElement(driver, pobj.contactTypeSelect)
					.findElement(By.xpath("./div/div/input"));
			fc.utobj().selectValFromMultiSelect(driver, pobj.contactTypeSelect, elementInput, contactType);
		} catch (Exception e) {
		}
		fc.utobj().setToDefault(driver, pobj.contactSourceSelect);
		fc.utobj().setToDefault(driver, pobj.statusSelect);
		fc.utobj().selectDropDown(driver, pobj.addDateSelect, "All");
		fc.utobj().setToDefault(driver, pobj.emailStatusSelect);

		if (fc.utobj().isSelected(driver, pobj.shoOnlyDuplicateNo)) {
			fc.utobj().clickElement(driver, pobj.shoOnlyDuplicateNo);
		}

		JavascriptExecutor executor = (JavascriptExecutor) driver;
	    executor.executeScript("arguments[0].click();", pobj.searchBtn);
	        
		//fc.utobj().clickElement(driver, pobj.searchBtn);
		fc.utobj().clickElement(driver, pobj.hideFilter);
	}

	public void contactsFilter(WebDriver driver, String contactType, String owner) throws Exception {

		CRMContactsPage pobj = new CRMContactsPage(driver);
		try {
			fc.utobj().clickElement(driver, pobj.showFilter);
		} catch (Exception e) {
		}
		fc.utobj().setToDefault(driver, pobj.contactOwnerSelect);
		try {
			WebElement divsion = fc.utobj().getElementByXpath(driver, ".//*[@id='ms-parentdivisionID']");
			fc.utobj().setToDefault(driver, divsion);
		} catch (Exception e) {
		}
		fc.utobj()
				.selectValFromMultiSelect(driver, pobj.contactOwnerSelect, fc.utobj()
						.getElement(driver, pobj.contactOwnerSelect).findElement(By.xpath("./div/div/input")),
						owner);
		fc.utobj().setToDefault(driver, pobj.franchiseIdSelect);
		try {
			fc.utobj().setToDefault(driver, pobj.contactTypeSelect);
			WebElement elementInput = fc.utobj().getElement(driver, pobj.contactTypeSelect)
					.findElement(By.xpath("./div/div/input"));
			fc.utobj().selectValFromMultiSelect(driver, pobj.contactTypeSelect, elementInput, contactType);
		} catch (Exception e) {
		}
		fc.utobj().setToDefault(driver, pobj.contactSourceSelect);
		fc.utobj().setToDefault(driver, pobj.statusSelect);
		fc.utobj().selectDropDown(driver, pobj.addDateSelect, "All");
		fc.utobj().setToDefault(driver, pobj.emailStatusSelect);

		if (fc.utobj().isSelected(driver, pobj.shoOnlyDuplicateNo)) {
			fc.utobj().clickElement(driver, pobj.shoOnlyDuplicateNo);
		}

		fc.utobj().clickElement(driver, pobj.searchBtn);
		fc.utobj().clickElement(driver, pobj.hideFilter);
	}

	public void selectActionMenuItemsWithTagAMoreActions(WebDriver driver, String menuName) throws Exception {

		WebElement moreActionsLink = driver
				.findElement(By.xpath(".//a[@class='showAction_cdetails link-btn' and .='More Actions']"));
		fc.utobj().moveToElement(driver, moreActionsLink);
		// clickElement(driver,moreActionsLink);

		List<WebElement> menu = driver
				.findElements(By.xpath(".//*[@id='actionListButtonsForCM']/table/tbody/tr[2]/td[2]/table/tbody//td"));
		fc.utobj().selectMoreActionMenu(driver, moreActionsLink, menu, menuName);
	}

	@Test(groups = { "ZcSanity", "ZcSanityLeadOff", "ZcSanityLeadOff36", "ZcSanity36" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify the Add Contact At CRM > Contact > Contact Summary", testCaseId = "TC_Add_CRM_Contact")
	public void addContactTest() throws IOException, Exception, ParserConfigurationException, SAXException {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		// Map<String,String> dataSet = fc.utobj().readTestData("crm",
		// testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			Map<String, String> data2 = new HashMap<String, String>();
			data2.put("userName", "Test158Cor");
			data2.put("contactType", "Test158ContactType");
			data2.put("leadType", "Test158LeadType");
			data2.put("source", "Test158Source");
			data2.put("description", "Test158Desc");
			data2.put("text", "Test158Display");
			data2.put("sourceDetails", "Test158SDetails");
			data2.put("medium", "Test158Medium");
			data2.put("firstName", "Test158fname");
			data2.put("lastName", "Test158lname");
			data2.put("primaryContactMethod", "Email");
			data2.put("title", "Mr.");

			CRMContactsPage pobj = new CRMContactsPage(driver);
			SearchUI search_page = new SearchUI(driver);

			fc.utobj().printTestStep("Navigate To Admin Users Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			String emailId = "crmautomation@staffex.com";
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUser = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporateUser.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			fc.utobj().printTestStep("Navigate To Admin > CRM > Contact Type Configuration");
			fc.utobj().printTestStep("Add Contact Type");
			AdminCRMContactTypeConfigurationPageTest contatcTypePage = new AdminCRMContactTypeConfigurationPageTest();
			String contactType = fc.utobj().generateTestData(data2.get("contactType"));
			contatcTypePage.addContactType(driver, contactType);

			fc.utobj().printTestStep("Naviagte To CRM > Contacts > Contacts Summary");
			fc.crm().crm_common().CRMContactsLnk(driver);
			fc.utobj().printTestStep("Add Contacs");
			fc.utobj().clickElement(driver, pobj.addNew);
			fc.utobj().clickElement(driver, pobj.contactLnk);

			String firstName = fc.utobj().generateTestData(data2.get("firstName"));
			String lastName = fc.utobj().generateTestData(data2.get("lastName"));
			String primaryCMethod = data2.get("primaryContactMethod");
			String assignTo = "Corporate";
			String regionName = "";
			String franchiseId = "";
			String title = data2.get("title");

			addContactGeneric(driver, contactType, firstName, lastName, primaryCMethod, assignTo,
					corpUser.getuserFullName(), regionName, franchiseId, title, emailId, config);
			String contactName = firstName + " " + lastName;

			if (!config.get("moduleList").equalsIgnoreCase("ZcSanityLeadOff36")
					&& !config.get("moduleList").equalsIgnoreCase("ZcSanity36")) {
				fc.utobj().printTestStep("Verify The Added Contact Search By Top Search");

				boolean isSearchTrue = false;
				for (int i = 0; i < 3; i++) {
					if (isSearchTrue == false) {
						fc.utobj().sendKeys(driver, search_page.topSearchField, contactName);
						Thread.sleep(5000);
						if (i == 2) {
							Thread.sleep(2000);
						}
						fc.utobj().clickEnterOnElement(driver, search_page.topSearchField);

						try {
							isSearchTrue = fc.utobj().isElementPresent(driver
									.findElements(By.xpath(".//custom[contains(text () , '" + contactName + "')]")));
						} catch (Exception e) {
							Reporter.log(e.getMessage());
							System.out.println(e.getMessage());
						}
					}
				}

				if (isSearchTrue == false) {
					fc.utobj().throwsException("not able to search contact By Top Search");
				}
				fc.utobj().refresh(driver);
			}

			fc.utobj().printTestStep("Verify Contact At Primary Info Page");
			boolean isContactTypePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () ,'Contact Type')]/ancestor::tr/td[.='" + contactType + "']");
			if (isContactTypePresent == false) {
				fc.utobj().throwsException("was not able to verify Contact Type");
			}
			boolean isContactNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () ,'Name')]/ancestor::tr/td[.='Mr. " + firstName + " " + lastName + "']");
			if (isContactNamePresent == false) {
				fc.utobj().throwsException("was not able to verify Contact Name");
			}
			boolean isPrimaryContactTypePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () ,'Primary Contact Method')]/ancestor::tr/td[.='Email']");
			if (isPrimaryContactTypePresent == false) {
				fc.utobj().throwsException("was not able to verify Primary Contact Type");
			}
			boolean isUserPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () ,'Assign To ')]/ancestor::tr/td[contains(text () , '"
							+ corpUser.getuserFullName() + "')]");
			if (isUserPresent == false) {
				fc.utobj().throwsException("was not able to verify Assign To");
			}

			fc.utobj().printTestStep("Verify Contact At Contacts Summary Page");
			fc.utobj().clickElement(driver, pobj.contactsLinks);
			contactsFilter(driver, contactType);

			boolean isNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firstName + " " + lastName + "')]");
			if (isNamePresent == false) {
				fc.utobj().throwsException("was not able to verify Contact Name");
			}
			boolean isContactTypePresentAtHome = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[contains(text () ,'" + firstName
					+ " " + lastName + "')]/ancestor::tr/td[contains(text () ,'" + contactType + "')]");
			if (isContactTypePresentAtHome == false) {
				fc.utobj().throwsException("was not able to verify Contact Type");
			}
			boolean isOwnerPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[contains(text () ,'" + firstName + " "
					+ lastName + "')]/ancestor::tr/td[contains(text () ,'" + corpUser.getuserFullName() + "')]");
			if (isOwnerPresent == false) {
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}
}
