package com.builds.test.crm;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import javax.xml.parsers.ParserConfigurationException;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;
import org.xml.sax.SAXException;

import com.builds.test.admin.AdminUsersManageCorporateUsersAddCorporateUserPageTest;
import com.builds.test.common.CRMModule;
import com.builds.test.common.CorporateUser;
import com.builds.uimaps.crm.CRMContactsPage;
import com.builds.uimaps.crm.CRMGroupsPage;
import com.builds.uimaps.crm.CRMLeadsPage;
import com.builds.utilities.FranconnectUtil;
import com.builds.utilities.TestCase;

public class CRMGroupsPageTest {
	FranconnectUtil fc = new FranconnectUtil();

	@Test(groups = "crm")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify the Create Group Of Contact Type At CRM > Groups > Active", testCaseId = "TC_259_Create_Group")
	public void createGroupContactType() throws IOException, Exception, ParserConfigurationException, SAXException {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());
		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		try {

			driver = fc.loginpage().login(driver);
			CRMGroupsPage pobj = new CRMGroupsPage(driver);
			CRMContactsPageTest contactsPage = new CRMContactsPageTest();

			fc.utobj().printTestStep("Navigate To Admin Users Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			String emailId = "crmautomation@staffex.com";
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUser = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporateUser.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			fc.utobj().printTestStep("Navigate To Admin > CRM > Contact Type Configuration");
			fc.utobj().printTestStep("Add Contact Type");

			AdminCRMContactTypeConfigurationPageTest contatcTypePage = new AdminCRMContactTypeConfigurationPageTest();
			String contactType = fc.utobj().generateTestData(dataSet.get("contactType"));
			contatcTypePage.addContactType(driver, contactType);

			// Create Contact
			fc.utobj().printTestStep("Navigate To CRM > Contacts > Contacts Summary Page");
			fc.utobj().printTestStep("Add Contact");
			fc.crm().crm_common().CRMContactsLnk(driver);
			fc.utobj().clickElement(driver, pobj.addNew);
			fc.utobj().clickElement(driver, pobj.contactLnk);

			String firstName = fc.utobj().generateTestData(dataSet.get("firstName"));
			String lastName = fc.utobj().generateTestData(dataSet.get("lastName"));
			String primaryCMethod = dataSet.get("primaryCMethod");
			String assignTo = "Corporate";
			String regionName = "";
			String franchiseId = "";
			String title = dataSet.get("title");
			String email = "automation@gmail.com";

			contactsPage.addContactGeneric(driver, contactType, firstName, lastName, primaryCMethod, assignTo,
					corpUser.getuserFullName(), regionName, franchiseId, title, email, config);

			fc.utobj().printTestStep("Navigate To CRM > Groups");
			/* fc.crm().crm_common().CRMGroupsLnk(driver); */
			fc.utobj().clickElement(driver, pobj.groupsLink);

			fc.utobj().printTestStep("Create Group");
			fc.utobj().clickElement(driver, pobj.createGroupBtn);
			fc.utobj().switchFrame(driver,
					fc.utobj().getElementByXpath(driver, ".//iframe[@class='newLayoutcboxIframe']"));
			String groupName = fc.utobj().generateTestData(dataSet.get("groupName"));
			fc.utobj().sendKeys(driver, pobj.groupName, groupName);
			fc.utobj().clickElement(driver, pobj.groupDecLnk);
			String groupDescription = fc.utobj().generateTestData(dataSet.get("groupDescription"));
			fc.utobj().sendKeys(driver, pobj.groupDescription, groupDescription);
			fc.utobj().selectDropDown(driver, pobj.groupTypeAddGroup, "Contact");
			fc.utobj().selectDropDown(driver, pobj.selectAccessibility, "Public to all Users");

			fc.utobj().clickElement(driver, pobj.saveAndContinueBtn);
			fc.utobj().switchFrameToDefault(driver);

			// select Criteria
			fc.utobj().clickElement(driver, pobj.fieldSelectBtn);
			fc.utobj().sendKeys(driver, pobj.searchField, "Contact Type");
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//*[@id='fc-drop-parentselColList']//span[.='Contact Type']"));
			fc.utobj().clickElement(driver, pobj.fieldSelectBtn);

			// configure Criteria
			fc.utobj().selectDropDown(driver, pobj.assignToCriteria, "In");
			fc.utobj().clickElement(driver, pobj.selectAllCriteria);
			fc.utobj().clickElement(driver, pobj.selectAllContactType);
			fc.utobj().clickElement(driver, pobj.selectAllCriteria);
			fc.utobj().clickElement(driver, pobj.saveAndContinueBtn);
			fc.utobj().clickElement(driver, pobj.associateWithGroupBtn);
			fc.utobj().switchFrame(driver,
					fc.utobj().getElementByXpath(driver, ".//iframe[@class='newLayoutcboxIframe']"));
			fc.utobj().clickElement(driver, pobj.okBtn);
			fc.utobj().switchFrameToDefault(driver);

			/*
			 * fc.utobj().sendKeys(driver, pobj.searchAtGroup, groupName);
			 * fc.utobj().clickElement(driver, pobj.exactSearchGroup);
			 * fc.utobj().clickElement(driver, pobj.searchAccountBtn);
			 */

			searchGroupsByFilter(driver, groupName, "Contact");

			fc.utobj().printTestStep("Verify Group");
			boolean isGroupNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[.='" + groupName + "']");
			if (isGroupNamePresent == false) {
				fc.utobj().throwsException("was not able to verify Group NAme");
			}
			boolean isGroupTypePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[.='" + groupName + "']/ancestor::tr/td[contains(text () ,'Contact')]");
			if (isGroupTypePresent == false) {
				fc.utobj().throwsException("was not able to verify Group Type");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "crm" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify the Create Group Of Lead Type At CRM > Groups > Active", testCaseId = "TC_260_Create_Group")
	public void createGroupLeadType() throws IOException, Exception, ParserConfigurationException, SAXException {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {

			driver = fc.loginpage().login(driver);
			CRMGroupsPage pobj = new CRMGroupsPage(driver);

			fc.utobj().printTestStep("Navigate To CRM > Group");

			fc.crm().crm_common().CRMGroupsLnk(driver);
			fc.utobj().printTestStep("Create Group");
			fc.utobj().clickElement(driver, pobj.createGroupBtn);

			fc.utobj().switchFrame(driver,
					fc.utobj().getElementByXpath(driver, ".//iframe[@class='newLayoutcboxIframe']"));

			String groupName = fc.utobj().generateTestData(dataSet.get("groupName"));
			fc.utobj().sendKeys(driver, pobj.groupName, groupName);

			fc.utobj().clickElement(driver, pobj.groupDecLnk);
			String groupDescription = fc.utobj().generateTestData(dataSet.get("groupDescription"));
			fc.utobj().sendKeys(driver, pobj.groupDescription, groupDescription);
			fc.utobj().selectDropDown(driver, pobj.groupTypeAddGroup, "Lead");
			fc.utobj().selectDropDown(driver, pobj.selectAccessibility, "Public to all Users");

			// add To On SmartGroup
			try {
				fc.utobj().getElement(driver, pobj.saveAndContinueCheck).isDisplayed();
			} catch (Exception e) {
				fc.utobj().clickElement(driver, pobj.isSmartGroup);
			}

			fc.utobj().clickElement(driver, pobj.saveAndContinueBtn);
			fc.utobj().switchFrameToDefault(driver);

			// select Criteria

			fc.utobj().clickElement(driver, pobj.fieldSelectBtn);
			fc.utobj().sendKeys(driver, pobj.searchField, "Assign To");
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//*[@id='fc-drop-parentselColList']//span[.='Assign To']"));
			fc.utobj().clickElement(driver, pobj.fieldSelectBtn);

			// configure Criteria
			fc.utobj().selectDropDown(driver, pobj.assignToCriteria1, "In");
			fc.utobj().clickElement(driver, pobj.selectAssignToUserBtn);
			fc.utobj().clickElement(driver, pobj.selectAssignToSelectAll);
			fc.utobj().clickElement(driver, pobj.selectAssignToUserBtn);
			fc.utobj().clickElement(driver, pobj.saveAndContinueBtn);
			fc.utobj().clickElement(driver, pobj.associateWithGroupBtn);
			fc.utobj().switchFrame(driver,
					fc.utobj().getElementByXpath(driver, ".//iframe[@class='newLayoutcboxIframe']"));
			fc.utobj().clickElement(driver, pobj.okBtn);
			fc.utobj().switchFrameToDefault(driver);

			/*
			 * fc.utobj().sendKeys(driver, pobj.searchAtGroup, groupName);
			 * fc.utobj().clickElement(driver, pobj.exactSearchGroup);
			 * fc.utobj().clickElement(driver, pobj.searchAccountBtn);
			 */

			new CRMGroupsPageTest().searchGroupsByFilter(driver, groupName, "ViewAll");

			fc.utobj().printTestStep("Verify Group");
			boolean isGroupNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[.='" + groupName + "']");
			if (isGroupNamePresent == false) {
				fc.utobj().throwsException("was not able to verify Group Name");
			}
			boolean isGroupTypePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[.='" + groupName + "']/ancestor::tr/td[contains(text () ,'Lead')]");
			if (isGroupTypePresent == false) {
				fc.utobj().throwsException("was not able to verify Group Type");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	public String createGroup(WebDriver driver, String typeGroup, String accessibility, String groupName,
			Map<String, String> config) throws Exception {

		String testCaseId = "TC_Create_Group_CRM";

		if (fc.utobj().validate(testCaseId)) {
			try {
				CRMGroupsPage pobj = new CRMGroupsPage(driver);
				// sync
				List<WebElement> elemntList = driver.findElements(By.xpath(".//*[@id='module_cm']"));

				if (elemntList.size() > 0) {
					fc.utobj().clickElement(driver, pobj.groupsLink);
				} else {
					fc.crm().crm_common().CRMGroupsLnk(driver);
				}
				fc.utobj().clickElement(driver, pobj.createGroupBtn);

				fc.utobj().switchFrame(driver,
						fc.utobj().getElementByXpath(driver, ".//iframe[@class='newLayoutcboxIframe']"));

				fc.utobj().sendKeys(driver, pobj.groupName, groupName);
				// fc.utobj().sendKeys(driver, pobj.groupDescription,
				// groupDescription);

				if (typeGroup.equalsIgnoreCase("Lead")) {
					fc.utobj().selectDropDown(driver, pobj.groupTypeAddGroup, "Lead");

				} else if (typeGroup.equalsIgnoreCase("Contact")) {
					fc.utobj().selectDropDown(driver, pobj.groupTypeAddGroup, "Contact");
				}

				if (accessibility.equalsIgnoreCase("Public To All User")) {
					fc.utobj().selectDropDown(driver, pobj.selectAccessibility, "Public to all Users");

				} else if (accessibility.equalsIgnoreCase("Public To All Corporate User")) {

					fc.utobj().selectDropDown(driver, pobj.selectAccessibility, "Public to all Corporate Users");

				} else if (accessibility.equalsIgnoreCase("Private")) {

					fc.utobj().selectDropDown(driver, pobj.selectAccessibility, "Private");
				}

				fc.utobj().clickElement(driver, pobj.saveAndContinueBtn);
				fc.utobj().switchFrameToDefault(driver);

				if (typeGroup.equalsIgnoreCase("Lead")) {

					fc.utobj().clickElement(driver, pobj.fieldSelectBtn);
					fc.utobj().sendKeys(driver, pobj.searchField, "Assign To");
					fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
							".//*[@id='fc-drop-parentselColList']//span[.='Assign To']"));
					fc.utobj().clickElement(driver, pobj.fieldSelectBtn);

					// configure Criteria
					fc.utobj().selectDropDown(driver, pobj.assignToCriteria1, "In");
					fc.utobj().clickElement(driver, pobj.selectAssignToUserBtn);
					fc.utobj().clickElement(driver, pobj.selectAssignToSelectAll);

				} else if (typeGroup.equalsIgnoreCase("Contact")) {
					fc.utobj().clickElement(driver, pobj.fieldSelectBtn);
					fc.utobj().sendKeys(driver, pobj.searchField, "Contact Type");
					fc.utobj().clickElement(driver, driver
							.findElement(By.xpath(".//*[@id='fc-drop-parentselColList']//span[.='Contact Type']")));
					fc.utobj().clickElement(driver, pobj.fieldSelectBtn);

					// configure Criteria
					fc.utobj().selectDropDown(driver, pobj.assignToCriteria, "In");
					fc.utobj().clickElement(driver, pobj.selectAllCriteria);
					fc.utobj().clickElement(driver, pobj.selectAllContactType);
				}

				fc.utobj().clickElement(driver, pobj.saveAndContinueBtn);

				
				
				fc.utobj().clickElement(driver, pobj.associateWithGroupBtn);
				fc.utobj().switchFrame(driver,
						fc.utobj().getElementByXpath(driver, ".//iframe[@class='newLayoutcboxIframe']"));
				fc.utobj().clickElement(driver, pobj.okBtn);
				fc.utobj().switchFrameToDefault(driver);

				searchGroupsByFilter(driver, groupName, typeGroup);

				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException("Was not able to create Group :: CRM > Groups");

		}
		return groupName;
	}

	// create Smart Group
	public String createGroupSmartGroup(WebDriver driver, String typeGroup, String accessibility, String groupName,
			Map<String, String> config) throws Exception {

		String testCaseId = "TC_Create_Smart_Group_CRM";

		if (fc.utobj().validate(testCaseId)) {
			try {
				CRMGroupsPage pobj = new CRMGroupsPage(driver);
				// sync
				List<WebElement> elemntList = driver.findElements(By.xpath(".//*[@id='module_cm']"));

				if (elemntList.size() > 0) {
					fc.utobj().clickElement(driver, pobj.groupsLink);
				} else {
					fc.crm().crm_common().CRMGroupsLnk(driver);
				}
				fc.utobj().clickElement(driver, pobj.createGroupBtn);

				fc.utobj().switchFrame(driver,
						fc.utobj().getElementByXpath(driver, ".//iframe[@class='newLayoutcboxIframe']"));

				fc.utobj().sendKeys(driver, pobj.groupName, groupName);

				if (typeGroup.equalsIgnoreCase("Lead")) {
					fc.utobj().selectDropDown(driver, pobj.groupTypeAddGroup, "Lead");

				} else if (typeGroup.equalsIgnoreCase("Contact")) {
					fc.utobj().selectDropDown(driver, pobj.groupTypeAddGroup, "Contact");
				}

				if (accessibility.equalsIgnoreCase("Public To All User")) {
					fc.utobj().selectDropDown(driver, pobj.selectAccessibility, "Public to all Users");

				} else if (accessibility.equalsIgnoreCase("Public To All Corporate User")) {

					fc.utobj().selectDropDown(driver, pobj.selectAccessibility, "Public to all Corporate Users");

				} else if (accessibility.equalsIgnoreCase("Private")) {

					fc.utobj().selectDropDown(driver, pobj.selectAccessibility, "Private");
				}

				fc.utobj().clickElement(driver, pobj.isSmartGroup);
				fc.utobj().clickElement(driver, pobj.saveAndContinueBtn);
				fc.utobj().switchFrameToDefault(driver);

				if (typeGroup.equalsIgnoreCase("Lead")) {

					fc.utobj().clickElement(driver, pobj.fieldSelectBtn);
					fc.utobj().sendKeys(driver, pobj.searchField, "Assign To");
					fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
							".//*[@id='fc-drop-parentselColList']//span[.='Assign To']"));
					fc.utobj().clickElement(driver, pobj.fieldSelectBtn);

					// configure Criteria
					fc.utobj().selectDropDown(driver, pobj.assignToCriteria1, "In");
					fc.utobj().clickElement(driver, pobj.selectAssignToUserBtn);
					fc.utobj().clickElement(driver, pobj.selectAssignToSelectAll);

				} else if (typeGroup.equalsIgnoreCase("Contact")) {
					fc.utobj().clickElement(driver, pobj.fieldSelectBtn);
					fc.utobj().sendKeys(driver, pobj.searchField, "Contact Type");
					fc.utobj().clickElement(driver, driver
							.findElement(By.xpath(".//*[@id='fc-drop-parentselColList']//span[.='Contact Type']")));
					fc.utobj().clickElement(driver, pobj.fieldSelectBtn);

					// configure Criteria
					fc.utobj().selectDropDown(driver, pobj.assignToCriteria, "In");
					fc.utobj().clickElement(driver, pobj.selectAllCriteria);
					fc.utobj().clickElement(driver, pobj.selectAllContactType);
				}

				fc.utobj().clickElement(driver, pobj.saveAndContinueBtn);
				fc.utobj().clickElement(driver, pobj.associateWithGroupBtn);
				fc.utobj().switchFrame(driver,
						fc.utobj().getElementByXpath(driver, ".//iframe[@class='newLayoutcboxIframe']"));
				fc.utobj().clickElement(driver, pobj.okBtn);
				fc.utobj().switchFrameToDefault(driver);

				/*
				 * fc.utobj().sendKeys(driver, pobj.searchAtGroup, groupName);
				 * fc.utobj().clickElement(driver, pobj.exactSearchGroup);
				 * fc.utobj().clickElement(driver, pobj.searchAccountBtn);
				 */

				searchGroupsByFilter(driver, groupName, "ViewAll");

				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException("Was not able to create Group :: CRM > Groups");

		}
		return groupName;
	}

	@Test(groups = {"crm"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify the Modify Group At CRM > Groups > Active", testCaseId = "TC_261_Modify_Group")
	public void modifyGroup() throws IOException, Exception, ParserConfigurationException, SAXException {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			CRMGroupsPage pobj = new CRMGroupsPage(driver);

			fc.utobj().printTestStep("Navigate To CRM > Group");
			fc.utobj().printTestStep("Create Group");
			String typeGroup = "Contact";
			String accessibility = "Public To All User";
			String groupName = fc.utobj().generateTestData(dataSet.get("groupName"));
			createGroup(driver, typeGroup, accessibility, groupName, config);

			fc.utobj().printTestStep("Modify Group");
			actionImgOption(driver, groupName, "Modify");

			fc.utobj().switchFrame(driver,
					fc.utobj().getElementByXpath(driver, ".//iframe[@class='newLayoutcboxIframe']"));

			groupName = fc.utobj().generateTestData(dataSet.get("groupName"));
			fc.utobj().sendKeys(driver, pobj.groupName, groupName);

			fc.utobj().clickElement(driver, pobj.modifySaveGroup);
			fc.utobj().switchFrameToDefault(driver);

			/*
			 * // select Criteria fc.utobj().clickElement(driver,
			 * pobj.fieldSelectBtn); fc.utobj().sendKeys(driver,
			 * pobj.searchField, "Assign To");
			 * fc.utobj().clickElement(driver,fc.utobj().getElementByXpath(
			 * driver,
			 * ".//*[@id='fc-drop-parentselColList']//span[.='Assign To']")));
			 * fc.utobj().clickElement(driver, pobj.fieldSelectBtn);
			 * 
			 * //configure Criteria fc.utobj().selectDropDown(driver,
			 * pobj.configCreteria, "In"); fc.utobj().clickElement(driver,
			 * pobj.selectUserTypeConfigCriteria);
			 * fc.utobj().clickElement(driver,
			 * pobj.selectAllUserTypeConfigCriteria);
			 * fc.utobj().clickElement(driver,
			 * pobj.selectUserTypeConfigCriteria);
			 * 
			 * fc.utobj().clickElement(driver, pobj.saveAndContinueBtn);
			 * fc.utobj().clickElement(driver, pobj.associateWithGroupBtn);
			 * fc.utobj().switchFrame(driver,
			 * fc.utobj().getElementByXpath(driver,
			 * ".//iframe[@class='newLayoutcboxIframe']")));
			 * fc.utobj().clickElement(driver, pobj.okBtn);
			 * fc.utobj().switchFrameToDefault(driver);
			 */

			/*
			 * fc.utobj().sendKeys(driver, pobj.searchAtGroup, groupName);
			 * fc.utobj().clickElement(driver, pobj.exactSearchGroup);
			 * fc.utobj().clickElement(driver, pobj.searchAccountBtn);
			 */

			searchGroupsByFilter(driver, groupName, "ViewAll");

			fc.utobj().printTestStep("Verify Modify Group");
			boolean isGroupNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[.='" + groupName + "']");
			if (isGroupNamePresent == false) {
				fc.utobj().throwsException("was not able to verify Group NAme");
			}
			boolean isGroupTypePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[.='" + groupName + "']/ancestor::tr/td[contains(text () ,'Contact')]");
			if (isGroupTypePresent == false) {
				fc.utobj().throwsException("was not able to verify Group Type");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "crm"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2017-06-29", testCaseDescription = "Verify the Group Associate With Campaign At CRM > Groups > Active", testCaseId = "TC_262_Associate_With_Campaign")
	public void associateWithCampaign() throws IOException, Exception, ParserConfigurationException, SAXException {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			CRMGroupsPage pobj = new CRMGroupsPage(driver);
			CRMContactsPageTest contactPage = new CRMContactsPageTest();

			fc.utobj().printTestStep("Navigate To Admin Users Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			String emailId = "crmautomation@staffex.com";
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUser = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporateUser.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			fc.utobj().printTestStep("Navigating to Admin > CRM > Contact Type Configuration");
			fc.utobj().printTestStep("Add Contact Type");
			AdminCRMContactTypeConfigurationPageTest contactTypePage = new AdminCRMContactTypeConfigurationPageTest();
			String contactType = fc.utobj().generateTestData(dataSet.get("contactType"));
			contactTypePage.addContactType(driver, contactType);

			fc.utobj().printTestStep("Navigating to CRM > Contacts > Contacts Summary");
			fc.utobj().printTestStep("Add Contact");
			fc.crm().crm_common().CRMContactsLnk(driver);
			fc.utobj().clickElement(driver, pobj.addNew);
			fc.utobj().clickElement(driver, pobj.contactLnk);

			String cFname = fc.utobj().generateTestData(dataSet.get("cFname"));
			String cLname = fc.utobj().generateTestData(dataSet.get("cLname"));
			String primaryCMethod = dataSet.get("primaryCMethod");
			String assignTo = "Corporate";
			String regionName = "";
			String franchiseId = "";
			String title = dataSet.get("title");
			//String email = "crmautomation@staffex.com";
			String email=fc.utobj().generateTestData("crmautomation")+"@staffex.com";
			contactPage.addContactGeneric(driver, contactType, cFname, cLname, primaryCMethod, assignTo,
					corpUser.getuserFullName(), regionName, franchiseId, title, email, config);

			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//div[@class='header-logo']"));
			fc.utobj().printTestStep("Navigate To CRM >Campaign Center");
			fc.utobj().printTestStep("Add Campaign");
			CRMCampaignCenterPageTestForCRM campaignCenterPage = new CRMCampaignCenterPageTestForCRM();
			String campaignName = fc.utobj().generateTestData(dataSet.get("campaignName"));
			String campaignDescription = fc.utobj().generateTestData(dataSet.get("campaignDescription"));
			String campaignAccessibility = dataSet.get("campaignAccessibility");
			String category = dataSet.get("category");
			String templateName = fc.utobj().generateTestData(dataSet.get("templateName"));
			String mailSubject = dataSet.get("mailSubject");
			String templateText = fc.utobj().generateTestData(dataSet.get("templateText"));
			campaignCenterPage.createCampaign(driver, campaignName, campaignDescription, campaignAccessibility,
					category, templateName, mailSubject, templateText, config);

			fc.utobj().printTestStep("Navigate To CRM > Group");
			fc.utobj().printTestStep("Create Group");
			String typeGroup = "Contact";
			String accessibility = "Public to all Users";
			String groupName = fc.utobj().generateTestData(dataSet.get("groupName"));
			createGroupSmartGroup(driver, typeGroup, accessibility, groupName, config);

			fc.utobj().printTestStep("Associate With Campaign");
			actionImgOption(driver, groupName, "Associate with Campaign");
			fc.utobj().clickElement(driver, pobj.filterContainer);
			fc.utobj().sendKeys(driver, pobj.searchCampaign, campaignName);
			fc.utobj().clickElement(driver, pobj.applyFilter);

			fc.utobj().clickElement(driver, new CRMLeadsPage(driver).sendCampaignBtn);
			// fc.utobj().clickElement(driver, pobj.confirmBtn);

			/*
			 * fc.utobj().sendKeys(driver, pobj.searchAtGroup, groupName);
			 * fc.utobj().clickElement(driver, pobj.exactSearchGroup);
			 * fc.utobj().clickElement(driver, pobj.searchAccountBtn);
			 */

			//searchGroupsByFilter(driver, groupName, "Contact");

			/*fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//a[.='" + groupName + "']"));
			fc.utobj().switchFrame(driver,
					fc.utobj().getElementByXpath(driver, ".//iframe[@class='newLayoutcboxIframe']"));*/
			fc.utobj().clickElement(driver, ".//button[@id='confirm']");
			searchGroupsByFilter(driver, groupName, "Contact");
			
			fc.utobj().printTestStep("Verify Associate With Campaign");
			fc.utobj().clickElement(driver, ".//a[.='"+groupName+"']/ancestor::tr[1]//td[6]/a");
			
			fc.utobj().switchFrameByClass(driver, "newLayoutcboxIframe");
			if(fc.utobj().assertPageSource(driver, campaignName))
			{
				System.out.println("campaign name present");
				
				fc.utobj().clickElement(driver, ".//button[@onclick = 'closeWindow();']");
			}else {
				fc.utobj().throwsException("was not able to verify campaign Name");
			}
			
			
			
			
			
			
			/*boolean isCampaignTitlePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[.='" + campaignName + "']");
			if (isCampaignTitlePresent == false) {
				fc.utobj().throwsException("was not able to verify campaign Name");
			}

			boolean isCampaignStatusPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[.='" + campaignName + "']/ancestor::tr/td[.='Active']");
			if (isCampaignStatusPresent == false) {
				fc.utobj().throwsException("was not able to verify campaign Status");
			}
			fc.utobj().switchFrameToDefault(driver);*/

			/*
			 * fc.utobj().printTestStep(testCaseId,
			 * "Verify Mail After Campaign mail against Smart Group Association"
			 * );
			 * 
			 * Map<String, String> mailData=new HashMap<String, String>();
			 * mailData=fc.utobj().readMailBox(mailSubject, templateText,
			 * emailId, "sdg@1a@Hfs");
			 * 
			 * if (mailData.size()==0 ||
			 * !mailData.get("mailBody").contains(templateText)) {
			 * fc.utobj().throwsException(
			 * "was not able to Verify Mail After Campaign mail against Smart Group Association"
			 * ); }
			 */

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = {"crm"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify the Delete Group At CRM > Groups > Active", testCaseId = "TC_263_Delete_Group")
	public void deleteGroup() throws IOException, Exception, ParserConfigurationException, SAXException {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			CRMGroupsPage pobj = new CRMGroupsPage(driver);

			fc.utobj().printTestStep("Navigate To CRM > Group");
			fc.utobj().printTestStep("Create Group");
			String typeGroup = "Contact";
			String accessibility = "Public To All User";
			String groupName = fc.utobj().generateTestData(dataSet.get("groupName"));
			createGroup(driver, typeGroup, accessibility, groupName, config);

			fc.utobj().printTestStep("Delete Group");
			actionImgOption(driver, groupName, "Delete");
			fc.utobj().acceptAlertBox(driver);

			/*
			 * fc.utobj().sendKeys(driver, pobj.searchAtGroup, groupName);
			 * fc.utobj().clickElement(driver, pobj.exactSearchGroup);
			 * fc.utobj().clickElement(driver, pobj.searchAccountBtn);
			 */

			searchGroupsByFilter(driver, groupName, "ViewAll");

			fc.utobj().printTestStep("Verify Delete Group");
			boolean isNoRecordPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//td[.='No Records found.']");
			if (isNoRecordPresent == false) {
				fc.utobj().throwsException("was not able to Delete Group");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = {"crm"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify the Archive Group  At CRM > Groups > Active", testCaseId = "TC_264_Archive_Group")
	public void archiveGroup() throws IOException, Exception, ParserConfigurationException, SAXException {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			CRMGroupsPage pobj = new CRMGroupsPage(driver);
			CRMContactsPageTest contactPage = new CRMContactsPageTest();

			/*
			 * fc.utobj().printTestStep(testCaseId,
			 * "Navigate To Admin > Users > Manage Corporate Users");
			 * fc.utobj().printTestStep(testCaseId, "Add Corporate User");
			 * AdminUsersManageCorporateUsersAddCorporateUserPageTest
			 * addCorporatePage = new
			 * AdminUsersManageCorporateUsersAddCorporateUserPageTest(); String
			 * userName = fc.utobj().generateTestData(dataSet.get("userName"));
			 * String emailId="crmautomation@staffex.com"; userName =
			 * addCorporatePage.addCorporateUser(driver, userName , config ,
			 * emailId);
			 */

			/*fc.utobj().printTestStep("Navigate To Admin Users Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			String emailId = "crmautomation@staffex.com";
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUser = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporateUser.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			fc.utobj().printTestStep("Navigating to Admin > CRM > Contact Type Configuration");
			fc.utobj().printTestStep("Add Contact Type");
			AdminCRMContactTypeConfigurationPageTest contactTypePage = new AdminCRMContactTypeConfigurationPageTest();
			String contactType = fc.utobj().generateTestData(dataSet.get("contactType"));
			contactTypePage.addContactType(driver, contactType);

			fc.utobj().printTestStep("Navigating to CRM > Contacts > Contacts Summary");
			fc.utobj().printTestStep("Add Contact");
			fc.crm().crm_common().CRMContactsLnk(driver);
			fc.utobj().clickElement(driver, pobj.addNew);
			fc.utobj().clickElement(driver, pobj.contactLnk);

			String cFname = fc.utobj().generateTestData(dataSet.get("cFname"));
			String cLname = fc.utobj().generateTestData(dataSet.get("cLname"));
			String primaryCMethod = dataSet.get("primaryCMethod");
			String assignTo = "Corporate";
			String regionName = "";
			String franchiseId = "";
			String title = dataSet.get("title");
			String email = "crmautomation@staffex.com";

			contactPage.addContactGeneric(driver, contactType, cFname, cLname, primaryCMethod, assignTo,
					corpUser.getuserFullName(), regionName, franchiseId, title, email, config);*/
			CRMModule crm = new CRMModule();
			crm.crm_common().CRMGroupsLnk(driver);
			//fc.utobj().clickElement(driver, pobj.groupsLink);
			fc.utobj().clickElement(driver, pobj.createGroupBtn);

			fc.utobj().printTestStep("Navigating to CRM > Groups");
			fc.utobj().printTestStep("Create Group");

			fc.utobj().switchFrame(driver,
					fc.utobj().getElementByXpath(driver, ".//iframe[@class='newLayoutcboxIframe']"));

			String groupName = fc.utobj().generateTestData(dataSet.get("groupName"));
			fc.utobj().sendKeys(driver, pobj.groupName, groupName);

			fc.utobj().selectDropDown(driver, pobj.groupTypeAddGroup, "Contact");
			fc.utobj().selectDropDown(driver, pobj.selectAccessibility, "Public to all Corporate Users");

			fc.utobj().clickElement(driver, pobj.modifySaveGroup);

			fc.utobj().switchFrameToDefault(driver);

			// select Criteria
			/*fc.utobj().clickElement(driver, pobj.fieldSelectBtn);
			fc.utobj().sendKeys(driver, pobj.searchField, "Contact Type");
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//*[@id='fc-drop-parentselColList']//span[.='Contact Type']"));
			fc.utobj().clickElement(driver, pobj.fieldSelectBtn);

			// configure Criteria
			fc.utobj().selectDropDown(driver, pobj.assignToCriteria, "In");
			fc.utobj().clickElement(driver, pobj.selectAllCriteria);
			fc.utobj().sendKeys(driver, pobj.searchAreaCriteria, contactType);
			fc.utobj().clickElement(driver, pobj.selectAllContactType);
			fc.utobj().clickElement(driver, pobj.saveAndContinueBtn);
			fc.utobj().clickElement(driver, pobj.associateWithGroupBtn);
			fc.utobj().switchFrame(driver,
					fc.utobj().getElementByXpath(driver, ".//iframe[@class='newLayoutcboxIframe']"));
			fc.utobj().clickElement(driver, pobj.okBtn);
			fc.utobj().switchFrameToDefault(driver);
*/
			/*
			 * fc.utobj().sendKeys(driver, pobj.searchAtGroup, groupName);
			 * fc.utobj().clickElement(driver, pobj.exactSearchGroup);
			 * fc.utobj().clickElement(driver, pobj.searchAccountBtn);
			 */

			/*searchGroupsByFilter(driver, groupName, "Contact");

			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//a[.='" + groupName + "']/ancestor::tr/td/a[.='1']"));
			//fc.utobj().clickElement(driver,
					//fc.utobj().getElementByXpath(driver, ".//a[contains(text () ,'" + cFname + " " + cLname + "')]"));
			fc.utobj().clickElement(driver, pobj.modifyBtn);
			fc.utobj().setToDefault(driver, pobj.groupsNameSelect);
			fc.utobj().clickElement(driver, pobj.saveBtn);

			fc.utobj().clickElement(driver, pobj.groupsLink);
*/
			/*
			 * fc.utobj().sendKeys(driver, pobj.searchAtGroup, groupName);
			 * fc.utobj().clickElement(driver, pobj.exactSearchGroup);
			 * fc.utobj().clickElement(driver, pobj.searchAccountBtn);
			 */

			searchGroupsByFilter(driver, groupName, "Contact");

			fc.utobj().printTestStep("Archive Group");
			actionImgOption(driver, groupName, "Archive");
			fc.utobj().acceptAlertBox(driver);
			searchGroupsByFilter(driver, groupName, "Contact");
			boolean isNoRecordPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//td[.='No Records found.']");
			if (isNoRecordPresent == false) {
				fc.utobj().throwsException("was not able to Archive Group");
			}

			fc.utobj().clickElement(driver, pobj.archivedTab);
			fc.utobj().printTestStep("Verify Archive Group");
			
			boolean isRecordPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[.='"+groupName+"']");
			if(isRecordPresent == true) {
				fc.utobj().printTestStep("group present on the archived page");
			}else {
				fc.utobj().throwsException("group not present on archived page");
			}

			/*
			 * fc.utobj().sendKeys(driver, pobj.searchAtGroup, groupName);
			 * fc.utobj().clickElement(driver, pobj.exactSearchGroup);
			 * fc.utobj().clickElement(driver, pobj.searchAccountBtn);
			 */

			/*searchGroupsByFilter(driver, groupName, "Contact");

			boolean isNoRecordPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//td[.='No Records found.']");
			if (isNoRecordPresent == false) {
				fc.utobj().throwsException("was not able to Archive Group");
			}*/

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = {"crm"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify the Group Associate With Contact At CRM > Groups > Active", testCaseId = "TC_265_Associate_With_Contact")
	public void associateWithContact() throws IOException, Exception, ParserConfigurationException, SAXException {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			CRMGroupsPage pobj = new CRMGroupsPage(driver);
			CRMContactsPageTest contactPage = new CRMContactsPageTest();

			fc.utobj().printTestStep("Navigate To Admin Users Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			String emailId = "crmautomation@staffex.com";
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUser = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporateUser.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			fc.utobj().printTestStep("Navigating to Admin > CRM > Contact Type Configuration");
			fc.utobj().printTestStep("Add Contact Type");
			AdminCRMContactTypeConfigurationPageTest contactTypePage = new AdminCRMContactTypeConfigurationPageTest();
			String contactType = fc.utobj().generateTestData(dataSet.get("contactType"));
			contactTypePage.addContactType(driver, contactType);

			fc.utobj().printTestStep("Navigate To CRM > Contacts > Contacts Summary Page");
			fc.utobj().printTestStep("Add Contacts");
			fc.crm().crm_common().CRMContactsLnk(driver);
			fc.utobj().clickElement(driver, pobj.addNew);
			fc.utobj().clickElement(driver, pobj.contactLnk);

			String cFname = fc.utobj().generateTestData(dataSet.get("cFname"));
			String cLname = fc.utobj().generateTestData(dataSet.get("cLname"));
			String primaryCMethod = dataSet.get("primaryCMethod");
			String assignTo = "Corporate";
			String regionName = "";
			String franchiseId = "";
			String title = dataSet.get("title");
			//String email = "crmautomation@staffex.com";
			String email=fc.utobj().generateTestData("crmautomation")+"@staffex.com";
			contactPage.addContactGeneric(driver, contactType, cFname, cLname, primaryCMethod, assignTo,
					corpUser.getuserFullName(), regionName, franchiseId, title, email, config);

			fc.utobj().printTestStep("Navigate To CRM > Group");
			fc.utobj().printTestStep("Create Group");
			String typeGroup = "Contact";
			String accessibility = "Public to all Users";
			String groupName = fc.utobj().generateTestData(dataSet.get("groupName"));
			createGroup(driver, typeGroup, accessibility, groupName, config);

			fc.utobj().printTestStep("Associate With Contact");
			actionImgOption(driver, groupName, "Associate with Contacts");

			fc.utobj().selectDropDown(driver, pobj.assignToCriteria, "In");
			fc.utobj().clickElement(driver, pobj.selectAllCriteria);
			fc.utobj().clickElement(driver, pobj.selectAllContactType);
			fc.utobj().sendKeys(driver, pobj.searchAreaCriteria, contactType);
			fc.utobj().clickElement(driver, pobj.selectAllContactType);

			fc.utobj().clickElement(driver, pobj.saveAndContinueBtn);
			fc.utobj().clickElement(driver, pobj.associateWithGroupBtn);
			fc.utobj().switchFrame(driver,
					fc.utobj().getElementByXpath(driver, ".//iframe[@class='newLayoutcboxIframe']"));
			fc.utobj().clickElement(driver, pobj.okBtn);
			fc.utobj().switchFrameToDefault(driver);

			searchGroupsByFilter(driver, groupName, "Contact");

			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//a[.='" + groupName
					+ "']/ancestor::tr/td[contains(text () , 'Contact')]/following-sibling::td[1]/a"));
			contactPage.contactsFilter(driver, contactType);
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//a[contains(text () ,'" + cFname + " " + cLname + "')]"));

			fc.utobj().printTestStep("Verify Associate With Contact");
			boolean isContactNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () ,'Name')]/ancestor::tr/td[.='" + title + " " + cFname + " " + cLname
							+ "']");
			if (isContactNamePresent == false) {
				fc.utobj().throwsException("was not able to Associate Contact");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "crm")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify the Group Associate With Lead At CRM > Groups > Active", testCaseId = "TC_266_Associate_With_Lead")
	public void associateWithLead() throws IOException, Exception, ParserConfigurationException, SAXException {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			CRMGroupsPage pobj = new CRMGroupsPage(driver);
			CRMLeadsPageTest leadPage = new CRMLeadsPageTest();

			fc.utobj().printTestStep("Navigate To Admin Users Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			String emailId = "crmautomation@staffex.com";
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUser = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporateUser.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			fc.utobj().printTestStep("Navigating to Admin > CRM > Configure Status");
			fc.utobj().printTestStep("Add Status Lead Type");
			AdminCRMConfigureStatusPageTest statusPage = new AdminCRMConfigureStatusPageTest();
			String statusName = fc.utobj().generateTestData(dataSet.get("statusName"));
			statusPage.addStatus(driver, statusName);

			fc.utobj().printTestStep("Navigate To CRM > Leads > Leads Summary");
			fc.utobj().printTestStep("Add Lead");

			String firstName = fc.utobj().generateTestData(dataSet.get("firstName"));
			String lastName = fc.utobj().generateTestData(dataSet.get("lastName"));
			String company = fc.utobj().generateTestData(dataSet.get("company"));
			String assignTo = "Corporate";
			String city = fc.utobj().generateTestData(dataSet.get("city"));
			String address = fc.utobj().generateTestData(dataSet.get("address"));
			String suffix = fc.utobj().generateTestData(dataSet.get("suffix"));
			String jobTitle = fc.utobj().generateTestData(dataSet.get("jobTitle"));
			String comment = fc.utobj().generateTestData(dataSet.get("comment"));
			String regionName = "";
			String regionalUser = "";
			String franchiseId = "";
			String franchiseUser = "";

			leadPage.addLead(driver, dataSet, firstName, lastName, company, assignTo, city, address, suffix, jobTitle,
					comment, corpUser.getuserFullName(), regionName, regionalUser, franchiseId, franchiseUser, config);

			fc.utobj().printTestStep("Navigate To CRM > Group");
			fc.utobj().printTestStep("Create Group");
			String typeGroup = "Lead";
			String accessibility = "Public To All User";
			String groupName = fc.utobj().generateTestData(dataSet.get("groupName"));
			createGroup(driver, typeGroup, accessibility, groupName, config);

			fc.utobj().printTestStep("Associate With Lead");
			actionImgOption(driver, groupName, "Associate with Lead");

			fc.utobj().clickElement(driver, pobj.saveAndContinueBtn);
			fc.utobj().clickElement(driver, pobj.associateWithGroupBtn);
			fc.utobj().switchFrame(driver,
					fc.utobj().getElementByXpath(driver, ".//iframe[@class='newLayoutcboxIframe']"));
			fc.utobj().clickElement(driver, pobj.okBtn);
			fc.utobj().switchFrameToDefault(driver);

			searchGroupsByFilter(driver, groupName, "Lead");

			String colunmName = "";
			int z = 0;

			List<WebElement> listHead = driver
					.findElements(By.xpath(".//a[.='" + groupName + "']/ancestor::table/thead/tr/th"));
			System.out.println(listHead);
			for (int i = 0; i < listHead.size(); i++) {

				colunmName = listHead.get(i).getText().trim();
				if (colunmName.equalsIgnoreCase("Recipients")) {
					z = i;
					break;
				}
			}

			if (z != 0) {
				z = z + 1;
				fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
						".//a[.='" + groupName + "']/ancestor::tr/td[" + z + "]/a"));
			} else {
				fc.utobj().throwsException("was not able to find Recipients Column");
			}

			leadPage.searchLeadByOwner(driver, corpUser.getuserFullName(), "All Leads");

			fc.utobj().printTestStep("Verify Group Associate With Lead");
			boolean isLeadNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[.='" + firstName + " " + lastName + "']");
			if (isLeadNamePresent == false) {
				fc.utobj().throwsException("was not able to verify Associate Lead");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "crm" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify the Removes Lead From Group At CRM > Groups > Active", testCaseId = "TC_267_Remove_Lead_From_Group")
	public void removeLeadsFromGroup() throws IOException, Exception, ParserConfigurationException, SAXException {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			CRMGroupsPage pobj = new CRMGroupsPage(driver);

			CRMLeadsPageTest leadPage = new CRMLeadsPageTest();

			fc.utobj().printTestStep("Navigate To Admin > Users > Manage Corporate Users");
			fc.utobj().printTestStep("Add Corporate User");
			AdminUsersManageCorporateUsersAddCorporateUserPageTest addCorporatePage = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			String emailId = "crmautomation@staffex.com";
			CorporateUser corpUser = new CorporateUser();
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corpUser.setEmail(emailId);
			corpUser = addCorporatePage.createDefaultUser(driver, corpUser);

			fc.utobj().printTestStep("Navigating to Admin > CRM > Configure Status");
			fc.utobj().printTestStep("Add Status Lead Type");
			AdminCRMConfigureStatusPageTest statusPage = new AdminCRMConfigureStatusPageTest();
			String statusName = fc.utobj().generateTestData(dataSet.get("statusName"));
			statusPage.addStatus(driver, statusName);

			fc.utobj().printTestStep("Navigate To CRM > Leads > Leads Summary");
			fc.utobj().printTestStep("Add Lead");

			String firstName = fc.utobj().generateTestData(dataSet.get("firstName"));
			String lastName = fc.utobj().generateTestData(dataSet.get("lastName"));
			String company = fc.utobj().generateTestData(dataSet.get("company"));
			String assignTo = "Corporate";
			String city = fc.utobj().generateTestData(dataSet.get("city"));
			String address = fc.utobj().generateTestData(dataSet.get("address"));
			String suffix = fc.utobj().generateTestData(dataSet.get("suffix"));
			String jobTitle = fc.utobj().generateTestData(dataSet.get("jobTitle"));
			String comment = fc.utobj().generateTestData(dataSet.get("comment"));
			String regionName = "";
			String regionalUser = "";
			String franchiseId = "";
			String franchiseUser = "";

			leadPage.addLead(driver, dataSet, firstName, lastName, company, assignTo, city, address, suffix, jobTitle,
					comment, corpUser.getuserFullName(), regionName, regionalUser, franchiseId, franchiseUser, config);

			fc.utobj().printTestStep("Navigate To CRM > Group");
			fc.utobj().printTestStep("Create Group");
			String typeGroup = "Lead";
			String accessibility = "Public To All User";
			String groupName = fc.utobj().generateTestData(dataSet.get("groupName"));
			createGroup(driver, typeGroup, accessibility, groupName, config);

			fc.utobj().printTestStep("Remove Leads From Group");
			actionImgOption(driver, groupName, "Remove Leads from Group");
			leadPage.searchLeadByOwner(driver, corpUser.getuserFullName(), "All Leads");

			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,(".//a[.='" + firstName + " " + lastName + "']/ancestor::tr/td/input[@name='checkBox']")));
			fc.utobj().clickElement(driver, pobj.removeFromGroupBtn);

			leadPage.searchLeadByOwner(driver, corpUser.getuserFullName(), "All Leads");

			fc.utobj().printTestStep("Verify Remove Leads Group");
			boolean isNoRecordPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//td[.='No records found.']");
			if (isNoRecordPresent == false) {
				fc.utobj().throwsException("was not able to Removes Leads From Group");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = {"crm"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify the Remove Contact From Group  At CRM > Groups > Active", testCaseId = "TC_268_Remove_Contact_From_Group")
	public void removeContactsFromGroup() throws IOException, Exception, ParserConfigurationException, SAXException {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			CRMGroupsPage pobj = new CRMGroupsPage(driver);
			CRMContactsPageTest contactPage = new CRMContactsPageTest();

			fc.utobj().printTestStep("Navigate To Admin > Users > Manage Corporate Users");
			fc.utobj().printTestStep("Add Corporate User");
			AdminUsersManageCorporateUsersAddCorporateUserPageTest addCorporatePage = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			String emailId = "crmautomation@staffex.com";
			CorporateUser corpUser = new CorporateUser();
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corpUser.setEmail(emailId);
			corpUser = addCorporatePage.createDefaultUser(driver, corpUser);

			fc.utobj().printTestStep("Navigating to Admin > CRM > Contact Type Configuration");
			fc.utobj().printTestStep("Add Contact Type");
			AdminCRMContactTypeConfigurationPageTest contactTypePage = new AdminCRMContactTypeConfigurationPageTest();
			String contactType = fc.utobj().generateTestData(dataSet.get("contactType"));
			contactTypePage.addContactType(driver, contactType);

			fc.utobj().printTestStep("Navigate To CRM > Contacts > Contacts Summary ");
			fc.utobj().printTestStep("Add Contact");
			fc.crm().crm_common().CRMContactsLnk(driver);
			fc.utobj().clickElement(driver, pobj.addNew);
			fc.utobj().clickElement(driver, pobj.contactLnk);

			String cFname = fc.utobj().generateTestData(dataSet.get("cFname"));
			String cLname = fc.utobj().generateTestData(dataSet.get("cLname"));
			String primaryCMethod = dataSet.get("primaryCMethod");
			String assignTo = "Corporate";
			String regionName = "";
			String franchiseId = "";
			String title = dataSet.get("title");
			//String email = "crmautomation@staffex.com";
			String email=fc.utobj().generateTestData("crmautomation")+"@staffex.com";
			contactPage.addContactGeneric(driver, contactType, cFname, cLname, primaryCMethod, assignTo,
					corpUser.getuserFullName(), regionName, franchiseId, title, email, config);

			fc.utobj().printTestStep("Navigate To CRM > Group");
			fc.utobj().printTestStep("Create Group");
			String typeGroup = "Contact";
			String accessibility = "Public to all Users";
			String groupName = fc.utobj().generateTestData(dataSet.get("groupName"));
			createGroup(driver, typeGroup, accessibility, groupName, config);

			fc.utobj().printTestStep("Remove Contact From Group");
			actionImgOption(driver, groupName, "Remove Contacts From Group");

			contactPage.contactsFilter(driver, contactType);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//a[contains(text () , '" + cFname + " " + cLname + "')]/ancestor::tr/td/input[@name='checkb']"));

			fc.utobj().clickElement(driver, pobj.removeFromGroupBtn);

			contactPage.contactsFilter(driver, contactType);
			fc.utobj().printTestStep("Verify Remove Contact From Group");
			boolean isNoRecordPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//td[.='No records found.']");
			if (isNoRecordPresent == false) {
				fc.utobj().throwsException("was not able to Remove Contacts From Group");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "crm" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify the Restore Group  At CRM > Groups > Active", testCaseId = "TC_269_Restore_Group")
	public void restoreGroup() throws IOException, Exception, ParserConfigurationException, SAXException {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			CRMGroupsPage pobj = new CRMGroupsPage(driver);

			/*CRMContactsPageTest contactPage = new CRMContactsPageTest();

			fc.utobj().printTestStep("Navigate To Admin > Users > Manage Corporate Users");
			fc.utobj().printTestStep("Add Corporate User");
			AdminUsersManageCorporateUsersAddCorporateUserPageTest addCorporatePage = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			String emailId = "crmautomation@staffex.com";
			CorporateUser corpUser = new CorporateUser();
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corpUser.setEmail(emailId);
			corpUser = addCorporatePage.createDefaultUser(driver, corpUser);

			fc.utobj().printTestStep("Navigating to Admin > CRM > Contact Type Configuration");
			fc.utobj().printTestStep("Add Contact Type");
			AdminCRMContactTypeConfigurationPageTest contactTypePage = new AdminCRMContactTypeConfigurationPageTest();
			String contactType = fc.utobj().generateTestData(dataSet.get("contactType"));
			contactTypePage.addContactType(driver, contactType);

			fc.utobj().printTestStep("Navigate To CRM > Contacts > Contacts Summary");
			fc.crm().crm_common().CRMContactsLnk(driver);

			fc.utobj().printTestStep("Add Contact");
			fc.utobj().clickElement(driver, pobj.addNew);
			fc.utobj().clickElement(driver, pobj.contactLnk);

			String cFname = fc.utobj().generateTestData(dataSet.get("cFname"));
			String cLname = fc.utobj().generateTestData(dataSet.get("cLname"));
			String primaryCMethod = dataSet.get("primaryCMethod");
			String assignTo = "Corporate";
			String regionName = "";
			String franchiseId = "";
			String title = dataSet.get("title");
			String email = "crmautomation@staffex.com";

			contactPage.addContactGeneric(driver, contactType, cFname, cLname, primaryCMethod, assignTo,
					corpUser.getuserFullName(), regionName, franchiseId, title, email, config);*/

			fc.utobj().printTestStep("Navigate To CRM > Groups");
			CRMModule crm = new CRMModule();
			crm.crm_common().CRMGroupsLnk(driver);
			//fc.utobj().clickElement(driver, pobj.groupsLink);

			fc.utobj().printTestStep("Create Group");
			fc.utobj().clickElement(driver, pobj.createGroupBtn);

			fc.utobj().switchFrame(driver,
					fc.utobj().getElementByXpath(driver, ".//iframe[@class='newLayoutcboxIframe']"));

			String groupName = fc.utobj().generateTestData(dataSet.get("groupName"));
			fc.utobj().sendKeys(driver, pobj.groupName, groupName);
			fc.utobj().selectDropDown(driver, pobj.groupTypeAddGroup, "Contact");
			fc.utobj().selectDropDown(driver, pobj.selectAccessibility, "Public to all Corporate Users");

			/*
			 * fc.utobj().clickElement(driver, pobj.contactTypeTab);
			 * fc.utobj().clickElement(driver,
			 * pobj.accessibilityPAllCorporateUser);
			 */

			/*
			 * //add To On SmartGroup try {
			 * fc.utobj().getElement(driver,pobj.saveAndContinueCheck).
			 * isDisplayed(); } catch (Exception e) {
			 * fc.utobj().clickElement(driver, pobj.isSmartGroup); }
			 */

			fc.utobj().clickElement(driver, pobj.modifySaveGroup);

			fc.utobj().switchFrameToDefault(driver);

			/*// select Criteria
			fc.utobj().clickElement(driver, pobj.fieldSelectBtn);
			fc.utobj().sendKeys(driver, pobj.searchField, "Contact Type");
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//*[@id='fc-drop-parentselColList']//span[.='Contact Type']"));
			fc.utobj().clickElement(driver, pobj.fieldSelectBtn);

			// configure Criteria
			fc.utobj().selectDropDown(driver, pobj.assignToCriteria, "In");
			fc.utobj().clickElement(driver, pobj.selectAllCriteria);
			//fc.utobj().sendKeys(driver, pobj.searchAreaCriteria, contactType);
			fc.utobj().clickElement(driver, pobj.selectAllContactType);
			fc.utobj().clickElement(driver, pobj.saveAndContinueBtn);
			fc.utobj().clickElement(driver, pobj.associateWithGroupBtn);
			fc.utobj().switchFrame(driver,
					fc.utobj().getElementByXpath(driver, ".//iframe[@class='newLayoutcboxIframe']"));
			fc.utobj().clickElement(driver, pobj.okBtn);
			fc.utobj().switchFrameToDefault(driver);
*/
			/*
			 * fc.utobj().sendKeys(driver, pobj.searchAtGroup, groupName);
			 * fc.utobj().clickElement(driver, pobj.exactSearchGroup);
			 * fc.utobj().clickElement(driver, pobj.searchAccountBtn);
			 */

			searchGroupsByFilter(driver, groupName, "Contact");

			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//a[.='"+groupName+"']/ancestor::tr[1]//label[@class = 'checkbox-lbl']"));
			//fc.utobj().clickElement(driver,
					//fc.utobj().getElementByXpath(driver, ".//a[contains(text () ,'" + cFname + " " + cLname + "')]"));
			/*fc.utobj().clickElement(driver, pobj.modifyBtn);
			fc.utobj().setToDefault(driver, pobj.groupsNameSelect);
			fc.utobj().clickElement(driver, pobj.saveBtn);

			fc.utobj().clickElement(driver, pobj.groupsLink);
*/
			/*
			 * fc.utobj().sendKeys(driver, pobj.searchAtGroup, groupName);
			 * fc.utobj().clickElement(driver, pobj.exactSearchGroup);
			 * fc.utobj().clickElement(driver, pobj.searchAccountBtn);
			 */

			//searchGroupsByFilter(driver, groupName, "Contact");

			fc.utobj().printTestStep("Archive Group");
			actionImgOption(driver, groupName, "Archive");
			fc.utobj().acceptAlertBox(driver);
			fc.utobj().clickElement(driver, pobj.archivedTab);

			boolean isPresentG = false;
			boolean isCountA = false;

			for (int i = 0; i < 100; i++) {

				try {
					WebElement elementGroup = fc.utobj().getElementByXpath(driver, ".//a[.='" + groupName + "']");
					elementGroup.isDisplayed();
					isPresentG = true;
					break;
				} catch (Exception e) {
					isPresentG = false;
				}

				try {

					if (isPresentG == false) {
						fc.utobj().clickElement(driver,
								fc.utobj().getElementByXpath(driver, ".//li[@class='count']/a[.='" + i + "']"));
					}

					isCountA = true;

				} catch (Exception e) {
					isCountA = false;
					break;
				}
			}

			if (isCountA == false && isPresentG == false) {

				fc.utobj().throwsException("Was not able to find group name in Archive");
			}

			fc.utobj().printTestStep("Restore Group");
			actionImgOption(driver, groupName, "Restore");
			fc.utobj().acceptAlertBox(driver);

			searchGroupsByFilter(driver, groupName, "Contact");

			fc.utobj().printTestStep("Verify Group At Archive Tab");
			boolean isNoRecordPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//td[.='No Records found.']");
			if (isNoRecordPresent == false) {
				fc.utobj().throwsException("was not able to Restore Leads From Group");
			}

			fc.utobj().clickElement(driver, pobj.activeTab);

			/*
			 * fc.utobj().sendKeys(driver, pobj.searchAtGroup, groupName);
			 * fc.utobj().clickElement(driver, pobj.exactSearchGroup);
			 * fc.utobj().clickElement(driver, pobj.searchAccountBtn);
			 */

			searchGroupsByFilter(driver, groupName, "Contact");

			fc.utobj().printTestStep("Verify Group At Active Group Tab");
			boolean isGroupNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[.='" + groupName + "']");
			if (isGroupNamePresent == false) {
				fc.utobj().throwsException("was not able to verify Group NAme");
			}
			boolean isGroupTypePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[.='" + groupName + "']/ancestor::tr/td[contains(text () ,'Contact')]");
			if (isGroupTypePresent == false) {
				fc.utobj().throwsException("was not able to verify Group Type");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	/* Harish Dwivedi TC_286_Verify_The_Create_Group starts */

	@Test(groups = "crmmodule")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Login through corporate >Verify the Create Group Of Contact Type At CRM > Groups > Active", testCaseId = "TC_286_Verify_The_Create_Group")
	public void verifyTheCreateGroupCreatedByCorporate()
			throws IOException, Exception, ParserConfigurationException, SAXException {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());
		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		try {

			driver = fc.loginpage().login(driver);
			CRMGroupsPage pobj = new CRMGroupsPage(driver);

			fc.utobj().printTestStep("Naviagate To Users > Manage Corporate Users");
			AdminUsersManageCorporateUsersAddCorporateUserPageTest addCorporatePage = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			String emailId = "crmautomation@staffex.com";
			CorporateUser corpUser = new CorporateUser();
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corpUser.setEmail(emailId);
			corpUser = addCorporatePage.createDefaultUser(driver, corpUser);

			fc.home_page().logout(driver);

			fc.utobj().printTestStep("Login With Corporate User");
			fc.loginpage().loginWithParameter(driver, corpUser.getUserName(), corpUser.getPassword());

			fc.utobj().printTestStep("Naviaget To CRM > Groups");
			fc.crm().crm_common().CRMGroupsLnk(driver);
			fc.utobj().printTestStep("Create Group");

			fc.utobj().clickElement(driver, pobj.createGroupBtn);

			fc.utobj().switchFrame(driver,
					fc.utobj().getElementByXpath(driver, ".//iframe[@class='newLayoutcboxIframe']"));

			String groupName = fc.utobj().generateTestData(dataSet.get("groupName"));
			fc.utobj().sendKeys(driver, pobj.groupName, groupName);
			String groupDescription = fc.utobj().generateTestData(dataSet.get("groupDescription"));
			// fc.utobj().sendKeys(driver, pobj.groupDescription,
			// groupDescription);

			/*
			 * fc.utobj().clickElement(driver, pobj.contactTypeTab);
			 * fc.utobj().clickElement(driver,
			 * pobj.accessibilityPAllCorporateUser);
			 */

			fc.utobj().selectDropDown(driver, pobj.groupTypeAddGroup, "Contact");
			fc.utobj().selectDropDown(driver, pobj.selectAccessibility, "Public to all Corporate Users");

			fc.utobj().clickElement(driver, pobj.saveAndAssociateBtn);

			fc.utobj().switchFrameToDefault(driver);

			// select Criteria
			fc.utobj().clickElement(driver, pobj.fieldSelectBtn);
			fc.utobj().sendKeys(driver, pobj.searchField, "Contact Type");
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//*[@id='fc-drop-parentselColList']//span[.='Contact Type']"));
			fc.utobj().clickElement(driver, pobj.fieldSelectBtn);

			// configure Criteria
			fc.utobj().selectDropDown(driver, pobj.assignToCriteria, "In");
			fc.utobj().clickElement(driver, pobj.selectAllCriteria);
			fc.utobj().clickElement(driver, pobj.selectAllContactType);
			fc.utobj().clickElement(driver, pobj.selectAllCriteria);
			fc.utobj().clickElement(driver, pobj.saveAndContinueBtn);
			fc.utobj().clickElement(driver, pobj.associateWithGroupBtn);
			fc.utobj().switchFrame(driver,
					fc.utobj().getElementByXpath(driver, ".//iframe[@class='newLayoutcboxIframe']"));
			fc.utobj().clickElement(driver, pobj.okBtn);
			fc.utobj().switchFrameToDefault(driver);

			/*
			 * fc.utobj().sendKeys(driver, pobj.searchAtGroup, groupName);
			 * fc.utobj().clickElement(driver, pobj.exactSearchGroup);
			 * fc.utobj().clickElement(driver, pobj.searchAccountBtn);
			 */

			searchGroupsByFilter(driver, groupName, "Contact");

			fc.utobj().printTestStep("Verify Group");
			boolean isGroupNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[.='" + groupName + "']");
			if (isGroupNamePresent == false) {
				fc.utobj().throwsException("was not able to verify Group NAme");
			}
			boolean isGroupTypePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[.='" + groupName + "']/ancestor::tr/td[contains(text () ,'Contact')]");
			if (isGroupTypePresent == false) {
				fc.utobj().throwsException("was not able to verify Group Type");
			}
			fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	public void addToGroupRegular(WebDriver driver, String groupName, String accessibility) throws Exception {

		CRMLeadsPage pobj = new CRMLeadsPage(driver);
		fc.commonMethods().switch_frameClass_newLayoutcboxIframe(driver);

		fc.utobj().sendKeys(driver, pobj.groupName, groupName);

		if (accessibility.equalsIgnoreCase("Public to all Users")) {

			fc.utobj().selectDropDownByValue(driver, pobj.selectAccessibility, "Global");

		} else if (accessibility.equalsIgnoreCase("Public to all Corporate Users")) {

			fc.utobj().selectDropDownByValue(driver, pobj.selectAccessibility, "Public");

		} else if (accessibility.equalsIgnoreCase("Private")) {

			fc.utobj().selectDropDownByValue(driver, pobj.selectAccessibility, "Private");
		}

		fc.utobj().clickElement(driver, pobj.saveGroupButton);
		fc.utobj().switchFrameToDefault(driver);
	}

	// Anukaran

	@Test(groups = {"crm"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2017-06-20", testCaseDescription = "Set Add Date between with contact source and contact source details as filters criteria > Verify the contact count associated with the group", testCaseId = "TC_326_Verify_Contact_Count_In_Group")
	public void verifyContactCountWithGroup()
			throws IOException, Exception, ParserConfigurationException, SAXException {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			CRMGroupsPage pobj = new CRMGroupsPage(driver);
			AdminCRMSourceSummaryPageTest nnpobj = new AdminCRMSourceSummaryPageTest();

			fc.utobj().printTestStep("Navigate To Admin > CRM > Source Summary");
			fc.utobj().printTestStep("Add Source");
			String description = fc.utobj().generateTestData(dataSet.get("description"));
			String source = fc.utobj().generateTestData(dataSet.get("source"));
			String displayOnWebPageText = fc.utobj().generateTestData(dataSet.get("text"));
			String sourceDetails = fc.utobj().generateTestData(dataSet.get("sourceDetails"));
			nnpobj.addSource(driver, source, description, displayOnWebPageText);

			nnpobj.addSourceDetails(driver, source, sourceDetails, displayOnWebPageText, description);

			fc.utobj().printTestStep("Navigate To Admin > CRM > Contact Type Configuration");
			fc.utobj().printTestStep("Add Contact Type");
			AdminCRMContactTypeConfigurationPageTest contatcTypePage = new AdminCRMContactTypeConfigurationPageTest();
			String contactType = fc.utobj().generateTestData(dataSet.get("contactType"));
			contatcTypePage.addContactType(driver, contactType);
			String firstName = fc.utobj().generateTestData(dataSet.get("firstName"));
			String lastName = fc.utobj().generateTestData(dataSet.get("lastName"));
			String primaryCMethod = dataSet.get("primaryContactMethod");
			String assignTo = "Corporate";
			String regionName = "";
			String franchiseId = "";
			String title = dataSet.get("title");
			//String email = "crmautomation@staffex.com";
			String email = fc.utobj().generateTestData("crmautomation")+"@staffex.com";
			String userName = "FranConnect Administrator";

			CRMLeadsPage leadPage = new CRMLeadsPage(driver);
			fc.crm().crm_common().CRMLeadsLnk(driver);
			fc.utobj().clickElement(driver, leadPage.addNew);
			fc.utobj().clickElement(driver, leadPage.leadLnk);
			fc.utobj().selectDropDown(driver, leadPage.title, dataSet.get("title"));
			fc.utobj().sendKeys(driver, leadPage.leadFirstName, firstName);
			fc.utobj().sendKeys(driver, leadPage.leadLastName, lastName);
			fc.utobj().selectDropDown(driver, leadPage.primaryContactMethodSelect, dataSet.get("primaryContactMethod"));

			if (assignTo.equalsIgnoreCase("Corporate")) {
				if (!fc.utobj().isSelected(driver, leadPage.assignToCorporate)) {
					fc.utobj().clickElement(driver, leadPage.assignToCorporate);
				}
				fc.utobj().selectDropDown(driver, leadPage.selectCorporateUser, userName);
			}
			fc.utobj().sendKeys(driver, leadPage.address, "address");
			fc.utobj().sendKeys(driver, leadPage.city, "city");
			fc.utobj().selectDropDown(driver, leadPage.country, "USA");
			fc.utobj().selectDropDown(driver, leadPage.state, "Alabama");
			fc.utobj().sendKeys(driver, leadPage.zipcode, "12345");
			fc.utobj().sendKeys(driver, leadPage.phoneNumbers, "1236547896");
			fc.utobj().sendKeys(driver, leadPage.extn, "12");
			fc.utobj().sendKeys(driver, leadPage.faxNumbers, "1263985487");
			fc.utobj().sendKeys(driver, leadPage.mobileNumbers, "8896724441");
			fc.utobj().sendKeys(driver, leadPage.emailIds, email);
			fc.utobj().sendKeys(driver, leadPage.alternateEmail, "test@gmail.com");
			fc.utobj().sendKeys(driver, leadPage.birthdate, "11/24/1980");
			fc.utobj().sendKeys(driver, leadPage.anniversarydate, "11/24/1999");
			fc.utobj().selectDropDown(driver, leadPage.rating, "Hot");
			fc.utobj().sendKeys(driver, leadPage.bestTimeToContact, "12:00 PM EST");
			fc.utobj().sendKeys(driver, leadPage.comments, "comment");
			fc.utobj().selectDropDown(driver, leadPage.leadSource, source);
			fc.utobj().selectDropDown(driver, leadPage.leadSourceDetails, sourceDetails);

			fc.utobj().clickElement(driver, leadPage.saveBtn);
			CRMContactsPage npobj = new CRMContactsPage(driver);
			fc.utobj().printTestStep("Navigate To CRM > Contacts > Contacts Summary Page");
			fc.utobj().printTestStep("Add Contact");
			fc.utobj().clickElement(driver, npobj.contactsLinks);
			fc.utobj().clickElement(driver, npobj.addNew);
			fc.utobj().clickElement(driver, npobj.contactLnk);
			fc.utobj().selectDropDown(driver, npobj.contactType, contactType);
			fc.utobj().selectDropDown(driver, npobj.title, title);
			fc.utobj().sendKeys(driver, npobj.contactFirstName, firstName);
			fc.utobj().sendKeys(driver, npobj.contactLastName, lastName);
			fc.utobj().sendKeys(driver, npobj.suffix, "suffixTest");
			fc.utobj().sendKeys(driver, npobj.position, "TestJobTitle");
			fc.utobj().sendKeys(driver, npobj.address, "TestAddress");
			fc.utobj().sendKeys(driver, npobj.city, "TestCity");
			fc.utobj().selectDropDown(driver, npobj.country, "USA");
			fc.utobj().selectDropDown(driver, npobj.state, "Alabama");
			fc.utobj().selectDropDown(driver, npobj.primaryContactMethodSelect, primaryCMethod);
			fc.utobj().sendKeys(driver, npobj.bestTimeToContact, "12:00 PM EST");
			fc.utobj().sendKeys(driver, npobj.zipcode, "12345");
			fc.utobj().sendKeys(driver, npobj.phoneNumbers, "1236547896");
			fc.utobj().sendKeys(driver, npobj.extn, "12");
			fc.utobj().sendKeys(driver, npobj.faxNumbers, "1263985487");
			fc.utobj().sendKeys(driver, npobj.mobileNumbers, "8896724441");
			fc.utobj().sendKeys(driver, npobj.emailIds, email);
			fc.utobj().sendKeys(driver, npobj.alternateEmail, "test@gmail.com");
			fc.utobj().sendKeys(driver, npobj.birthdate, "11/24/1980");
			fc.utobj().sendKeys(driver, npobj.anniversarydate, "11/24/1999");
			fc.utobj().selectDropDown(driver, npobj.leadSource, source);
			fc.utobj().selectDropDown(driver, npobj.leadSourceDetails, sourceDetails);
			if (assignTo.equalsIgnoreCase("Corporate")) {

				if (!fc.utobj().isSelected(driver, npobj.assignToCorporate)) {
					fc.utobj().clickElement(driver, npobj.assignToCorporate);
				}

				fc.utobj().selectDropDown(driver, npobj.selectCorporateUserContact, userName);

			} else if (assignTo.equalsIgnoreCase("Regional")) {

				if (!fc.utobj().isSelected(driver, npobj.assignToRegional)) {
					fc.utobj().clickElement(driver, npobj.assignToRegional);
				}

				fc.utobj().selectDropDown(driver, npobj.selectAreaRegion, regionName);
				fc.utobj().selectDropDown(driver, npobj.selectRegionalUser, userName);

			} else if (assignTo.equalsIgnoreCase("Franchise")) {

				if (!fc.utobj().isSelected(driver, npobj.assignToFranchise)) {
					fc.utobj().clickElement(driver, npobj.assignToFranchise);
				}

				fc.utobj().selectDropDown(driver, npobj.selectFranchiseId, franchiseId);
				fc.utobj().selectDropDown(driver, npobj.selectFranchiseUser, userName);

			}
			fc.utobj().sendKeys(driver, npobj.comments, "Test Comment");
			fc.utobj().clickElement(driver, npobj.saveBtn);

			fc.utobj().printTestStep("Navigate To CRM > Group");
			fc.utobj().printTestStep("Create Group");
			String typeGroup = "Contact";
			String accessibility = "Public to all Users";
			String groupName = fc.utobj().generateTestData(dataSet.get("groupName"));
			List<WebElement> elemntList = driver.findElements(By.xpath(".//*[@id='module_cm']"));

			if (elemntList.size() > 0) {
				fc.utobj().clickElement(driver, pobj.groupsLink);
			} else {
				fc.crm().crm_common().CRMGroupsLnk(driver);
			}
			fc.utobj().clickElement(driver, pobj.createGroupBtn);

			fc.utobj().switchFrame(driver,
					fc.utobj().getElementByXpath(driver, ".//iframe[@class='newLayoutcboxIframe']"));

			fc.utobj().sendKeys(driver, pobj.groupName, groupName);

			if (typeGroup.equalsIgnoreCase("Lead")) {
				fc.utobj().selectDropDown(driver, pobj.groupTypeAddGroup, "Lead");

			} else if (typeGroup.equalsIgnoreCase("Contact")) {
				fc.utobj().selectDropDown(driver, pobj.groupTypeAddGroup, "Contact");
			}

			if (accessibility.equalsIgnoreCase("Public to all Users")) {
				fc.utobj().selectDropDown(driver, pobj.selectAccessibility, "Public to all Users");

			} else if (accessibility.equalsIgnoreCase("Public To All Corporate User")) {

				fc.utobj().selectDropDown(driver, pobj.selectAccessibility, "Public to all Corporate Users");

			} else if (accessibility.equalsIgnoreCase("Private")) {

				fc.utobj().selectDropDown(driver, pobj.selectAccessibility, "Private");
			}
			fc.utobj().clickElement(driver, pobj.saveAndContinueBtn);
			fc.utobj().switchFrameToDefault(driver);
			if (typeGroup.equalsIgnoreCase("Lead")) {

				fc.utobj().clickElement(driver, pobj.fieldSelectBtn);
				fc.utobj().sendKeys(driver, pobj.searchField, "Add Date");
				fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
						".//*[@id='fc-drop-parentselColList']//span[.='Add Date']"));
				Thread.sleep(1000);
				fc.utobj().sendKeys(driver, pobj.searchField, "How did you hear about us ? (Contact Source)");
				fc.utobj().clickElement(driver, ".//input[@class='search-btn on']");
				fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
						".//*[@id='fc-drop-parentselColList']//span[.='How did you hear about us ? (Contact Source)']"));
				Thread.sleep(1000);
				fc.utobj().sendKeys(driver, pobj.searchField, "Please Specify (Contact Source Details)");
				fc.utobj().clickElement(driver, ".//input[@class='search-btn on']");
				fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
						".//*[@id='fc-drop-parentselColList']//span[.='Please Specify (Contact Source Details)']"));

				fc.utobj().clickElement(driver, pobj.fieldSelectBtn);

				fc.utobj().selectDropDown(driver, pobj.assignToAddDate, "Is Between");
				String fromDate = fc.utobj().getCurrentDateUSFormat();
				String toDate = fc.utobj().getFutureDateUSFormat(1);
				fc.utobj().sendKeys(driver, pobj.fromAddDate, fromDate);
				fc.utobj().sendKeys(driver, pobj.toAddDate, toDate);

				fc.utobj().selectDropDown(driver, pobj.assignToContactSource, "In");
				WebElement elementInput = fc.utobj().getElement(driver, pobj.contactSource)
						.findElement(By.xpath("./div/div/input"));
				fc.utobj().selectValFromMultiSelect(driver, pobj.contactSource, elementInput, source);

				fc.utobj().selectDropDown(driver, pobj.assignToContactSrcDetails, "In");
				WebElement selectAllCheck = driver
						.findElement(By.xpath(".//*[@id='selectAll']/following-sibling::span"));
				fc.utobj().clickElement(driver, driver
						.findElement(By.xpath(".//*[@id='fc-drop-parentCM_CONTACT_DETAILS_cmSource3ID']/button")));
				Thread.sleep(1000);
				WebElement searchTextBox = driver.findElement(
						By.xpath(".//*[@id='fc-drop-parentCM_CONTACT_DETAILS_cmSource3ID']/div/div/input"));
				searchTextBox.sendKeys(sourceDetails);
				Thread.sleep(500);
				fc.utobj().clickElement(driver, selectAllCheck);

			} else if (typeGroup.equalsIgnoreCase("Contact")) {

				fc.utobj().clickElement(driver, pobj.fieldSelectBtn);
				fc.utobj().sendKeys(driver, pobj.searchField, "Add Date");
				fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
						".//*[@id='fc-drop-parentselColList']//span[.='Add Date']"));
				fc.utobj().sendKeys(driver, pobj.searchField, "How did you hear about us ? (Contact Source)");
				Thread.sleep(1000);
				fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
						".//*[@id='fc-drop-parentselColList']//span[.='How did you hear about us ? (Contact Source)']"));
				Thread.sleep(1000);
				fc.utobj().sendKeys(driver, pobj.searchField, "Please Specify (Contact Source Details)");
				fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
						".//*[@id='fc-drop-parentselColList']//span[.='Please Specify (Contact Source Details)']"));

				fc.utobj().clickElement(driver, pobj.fieldSelectBtn);

				fc.utobj().selectDropDown(driver, pobj.assignToAddDate, "Is Between");
				String fromDate = fc.utobj().getCurrentDateUSFormat();
				String toDate = fc.utobj().getFutureDateUSFormat(1);
				fc.utobj().sendKeys(driver, pobj.fromAddDate, fromDate);
				fc.utobj().sendKeys(driver, pobj.toAddDate, toDate);
				fc.utobj().selectDropDown(driver, pobj.assignToContactSource, "In");
				fc.utobj().selectDropDown(driver, pobj.contactSource, source);
				try {
					WebElement selectAllCheck = driver
							.findElement(By.xpath(".//*[@id='selectAll']/following-sibling::span"));
					fc.utobj().clickElement(driver, driver
							.findElement(By.xpath(".//*[@id='fc-drop-parentCM_CONTACT_DETAILS_cmSource3ID']/button")));
					Thread.sleep(1000);
					WebElement searchTextBox = driver.findElement(
							By.xpath(".//*[@id='fc-drop-parentCM_CONTACT_DETAILS_cmSource3ID']/div/div/input"));
					searchTextBox.sendKeys(sourceDetails);
					Thread.sleep(500);
					fc.utobj().clickElement(driver, selectAllCheck);
				} catch (Exception e) {
					fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
							".//*[@id='fc-drop-parentCM_CONTACT_DETAILS_cmSource3ID']/div/ul/li[1]/label/span"));
				}
			}
			fc.utobj().clickElement(driver, pobj.saveAndContinueBtn);
			fc.utobj().switchFrameToDefault(driver);

			try {
				boolean verifyContactCount = fc.utobj().verifyElementOnVisible_ByXpath(driver,
						".//*[@id='siteMainTable']//p[contains(text(),'1 Contacts filtered based on defined criteria')]");
				if (verifyContactCount == false) {
					fc.utobj().throwsException("was not able to verify contact Count");
				}
			} catch (Exception e) {
				// TODO: handle exception
			}
			fc.utobj().clickElement(driver, pobj.associateWithGroupBtn);
			fc.utobj().switchFrame(driver,
					fc.utobj().getElementByXpath(driver, ".//iframe[@class='newLayoutcboxIframe']"));
			fc.utobj().clickElement(driver, pobj.okBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Navigate To CRM > Group");
			fc.utobj().printTestStep("Create Group");
			typeGroup = "Lead";
			accessibility = "Public to all Users";
			groupName = fc.utobj().generateTestData(dataSet.get("groupName"));
			// createGroup(driver, typeGroup, accessibility, groupName, config);

			// sync
			elemntList = driver.findElements(By.xpath(".//*[@id='module_cm']"));

			if (elemntList.size() > 0) {
				fc.utobj().clickElement(driver, pobj.groupsLink);
			} else {
				fc.crm().crm_common().CRMGroupsLnk(driver);
			}
			fc.utobj().clickElement(driver, pobj.createGroupBtn);

			fc.utobj().switchFrame(driver,
					fc.utobj().getElementByXpath(driver, ".//iframe[@class='newLayoutcboxIframe']"));

			fc.utobj().sendKeys(driver, pobj.groupName, groupName);
			// fc.utobj().sendKeys(driver, pobj.groupDescription,
			// groupDescription);

			if (typeGroup.equalsIgnoreCase("Lead")) {
				fc.utobj().selectDropDown(driver, pobj.groupTypeAddGroup, "Lead");

			} else if (typeGroup.equalsIgnoreCase("Contact")) {
				fc.utobj().selectDropDown(driver, pobj.groupTypeAddGroup, "Contact");
			}

			if (accessibility.equalsIgnoreCase("Public to all Users")) {
				fc.utobj().selectDropDown(driver, pobj.selectAccessibility, "Public to all Users");

			} else if (accessibility.equalsIgnoreCase("Public To All Corporate User")) {

				fc.utobj().selectDropDown(driver, pobj.selectAccessibility, "Public to all Corporate Users");

			} else if (accessibility.equalsIgnoreCase("Private")) {

				fc.utobj().selectDropDown(driver, pobj.selectAccessibility, "Private");
			}
			fc.utobj().clickElement(driver, pobj.saveAndContinueBtn);
			fc.utobj().switchFrameToDefault(driver);
			if (typeGroup.equalsIgnoreCase("Lead")) {

				fc.utobj().clickElement(driver, pobj.fieldSelectBtn);
				fc.utobj().sendKeys(driver, pobj.searchField, "Add Date");
				fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
						".//*[@id='fc-drop-parentselColList']//span[.='Add Date']"));
				Thread.sleep(1000);
				fc.utobj().sendKeys(driver, pobj.searchField, "How did you hear about us ? (Lead Source)");
				fc.utobj().clickElement(driver, ".//input[@class='search-btn on']");
				fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
						".//*[@id='fc-drop-parentselColList']//span[.='How did you hear about us ? (Lead Source)']"));
				fc.utobj().sendKeys(driver, pobj.searchField, "Please Specify (Lead Source Details)");
				fc.utobj().clickElement(driver, ".//input[@class='search-btn on']");
				fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
						".//*[@id='fc-drop-parentselColList']//span[.='Please Specify (Lead Source Details)']"));

				fc.utobj().clickElement(driver, pobj.fieldSelectBtn);

				// configure Criteria
				fc.utobj().selectDropDown(driver, pobj.assignToAddDateLead, "Is Between");
				String fromDate = fc.utobj().getCurrentDateUSFormat();
				String toDate = fc.utobj().getFutureDateUSFormat(1);
				fc.utobj().sendKeys(driver, pobj.fromAddDateLead, fromDate);
				fc.utobj().sendKeys(driver, pobj.toAddDateLead, toDate);

				fc.utobj().selectDropDown(driver, pobj.assignToLeadSource, "In");
				// fc.utobj().selectDropDown(driver, pobj.contactSource,
				// contactSource);
				fc.utobj().selectDropDown(driver, pobj.leadSource, source);
				fc.utobj().selectDropDown(driver, pobj.assignToLeadSrcDetails, "In");

				WebElement selectAllCheck = driver
						.findElement(By.xpath(".//*[@id='selectAll']/following-sibling::span"));
				fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
						".//*[@id='fc-drop-parentCM_LEAD_DETAILS_cmSource3ID']/button"));
				WebElement searchTextBox = driver
						.findElement(By.xpath(".//*[@id='fc-drop-parentCM_LEAD_DETAILS_cmSource3ID']/div/div/input"));
				Thread.sleep(1000);
				searchTextBox.sendKeys(sourceDetails);
				Thread.sleep(500);
				fc.utobj().clickElement(driver, selectAllCheck);

				/*
				 * WebElement elementInput =
				 * pobj.leadSrcDetailsSlctAll.findElement(By.xpath(
				 * "./div/div/input"));
				 * fc.utobj().selectValFromMultiSelect(driver,
				 * pobj.leadSrcDetailsSlctAll, elementInput, sourceDetails);
				 */
			} else if (typeGroup.equalsIgnoreCase("Contact")) {

				fc.utobj().clickElement(driver, pobj.fieldSelectBtn);
				fc.utobj().sendKeys(driver, pobj.searchField, "Add Date");
				fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
						".//*[@id='fc-drop-parentselColList']//span[.='Add Date']"));
				fc.utobj().sendKeys(driver, pobj.searchField, "How did you hear about us ? (Contact Source)");
				fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
						".//*[@id='fc-drop-parentselColList']//span[.='How did you hear about us ? (Contact Source)']"));
				fc.utobj().sendKeys(driver, pobj.searchField, "Please Specify (Contact Source Details)");
				fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
						".//*[@id='fc-drop-parentselColList']//span[.='Please Specify (Contact Source Details)']"));

				fc.utobj().clickElement(driver, pobj.fieldSelectBtn);

				// configure Criteria
				fc.utobj().selectDropDown(driver, pobj.assignToAddDateLead, "Is Between");
				String fromDate = fc.utobj().getCurrentDateUSFormat();
				String toDate = fc.utobj().getFutureDateUSFormat(1);
				fc.utobj().sendKeys(driver, pobj.fromAddDateLead, fromDate);
				fc.utobj().sendKeys(driver, pobj.toAddDateLead, toDate);

				fc.utobj().selectDropDown(driver, pobj.assignToLeadSource, "In");
				// fc.utobj().selectDropDown(driver, pobj.contactSource,
				// contactSource);
				// WebElement elementInput =
				// pobj.contactSource.findElement(By.xpath("./div/div/input"));
				// fc.utobj().selectValFromMultiSelect(driver,
				// pobj.contactSource, elementInput, source);
				fc.utobj().selectDropDown(driver, pobj.leadSource, source);

				fc.utobj().selectDropDown(driver, pobj.assignToLeadSrcDetails, "In");
				// fc.utobj().selectDropDown(driver,
				// pobj.contactSrcDetailsSlctAll, "Select All");
				// fc.utobj().clickElement(driver,
				// fc.utobj().getElementByXpath(driver,".//*[@id='fc-drop-parentCM_LEAD_DETAILS_cmSource3ID']/div/ul/li[1]/label/input")));
				// WebElement elementInput =
				// pobj.leadSrcDetailsSlctAll.findElement(By.xpath("./div/div/input"));
				// fc.utobj().selectValFromMultiSelect(driver,
				// pobj.leadSrcDetailsSlctAll, elementInput, sourceDetails);

				WebElement selectAllCheck = driver
						.findElement(By.xpath(".//*[@id='selectAll']/following-sibling::span"));
				fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
						".//*[@id='fc-drop-parentCM_LEAD_DETAILS_cmSource3ID']/button"));
				WebElement searchTextBox = driver
						.findElement(By.xpath(".//*[@id='fc-drop-parentCM_LEAD_DETAILS_cmSource3ID']/div/div/input"));
				searchTextBox.sendKeys(sourceDetails);
				Thread.sleep(500);
				fc.utobj().clickElement(driver, selectAllCheck);

			}

			fc.utobj().clickElement(driver, pobj.saveAndContinueBtn);
			fc.utobj().switchFrameToDefault(driver);

			boolean verifyLeadCount = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//*[@id='siteMainTable']//p[contains(text(),'1 Leads filtered based on defined criteria')]");
			if (verifyLeadCount == false) {
				fc.utobj().throwsException("was not able to verify contact Count");
			}
			fc.utobj().clickElement(driver, pobj.associateWithGroupBtn);
			fc.utobj().switchFrame(driver,
					fc.utobj().getElementByXpath(driver, ".//iframe[@class='newLayoutcboxIframe']"));
			fc.utobj().clickElement(driver, pobj.okBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	public void actionImgOption(WebDriver driver, String groupName, String option) throws Exception {
		fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
				".//*[contains(text () ,'" + groupName + "')]/../following-sibling::td//div[@class='list-name']"));

		WebElement element = fc.utobj().getElementByXpath(driver,
				".//*[contains(text () ,'" + groupName
						+ "')]/../following-sibling::td//div[@class='list-name']/following-sibling::ul/li/a[contains(text () ,'"
						+ option + "')]");
		fc.utobj().clickElement(driver, element);
	}

	public void searchGroupsByFilter(WebDriver driver, String groupName, String typeOfGroup) throws Exception {
		CRMGroupsPage pobj = new CRMGroupsPage(driver);
		fc.utobj().clickElement(driver, pobj.showFilter);
		fc.utobj().sendKeys(driver, pobj.groupNameSearch, groupName);
		fc.utobj().clickElement(driver, pobj.creationDate);
		fc.utobj().getElementByXpath(driver, ".//span[.='All']/ancestor::label/input");

		if (typeOfGroup.equalsIgnoreCase("Lead")) {

			fc.utobj().selectDropDownByValue(driver, pobj.groupType, "L");

		} else if (typeOfGroup.equalsIgnoreCase("Contact")) {

			fc.utobj().selectDropDownByValue(driver, pobj.groupType, "C");

		} else if (typeOfGroup.equalsIgnoreCase("ViewAll")) {
			fc.utobj().selectDropDownByValue(driver, pobj.groupType, "none");
		}

		// fc.utobj().selectDropDown(driver, pobj.groupType, typeOfGroup);
		fc.utobj().clickElement(driver, pobj.applyFilterAtGroup);
	}
}
