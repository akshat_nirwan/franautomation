package com.builds.test.crm;

import java.util.HashMap;
import java.util.Map;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import com.builds.test.admin.AdminUsersManageCorporateUsersAddCorporateUserPageTest;
import com.builds.test.common.CorporateUser;
import com.builds.uimaps.crm.CRMHomePage;
import com.builds.utilities.FranconnectUtil;
import com.builds.utilities.TestCase;

public class CRMHomePageTest {
	FranconnectUtil fc = new FranconnectUtil();

	@Test(groups = { "crm", "crmmail324"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify the Add Lead At CRM > Home And Verify Lead Creation Mail", testCaseId = "TC_56_Add_Lead")
	public void addLead() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());
		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			CRMHomePage pobj = new CRMHomePage(driver);
			CRMLeadsPageTest leadsPage = new CRMLeadsPageTest();

			fc.utobj().printTestStep("Navigate To Admin Users Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			String emailId = "crmautomation@staffex.com";
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUser = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporateUser.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			AdminCRMSourceSummaryPageTest addSourcePage = new AdminCRMSourceSummaryPageTest();
			String source = fc.utobj().generateTestData(dataSet.get("source"));
			String description = fc.utobj().generateTestData(dataSet.get("description"));
			String displayOnWebPageText = fc.utobj().generateTestData(dataSet.get("text"));
			fc.utobj().printTestStep("Navigating to Admin > CRM > Source Summary");
			fc.utobj().printTestStep("Add Source");
			addSourcePage.addSource(driver, source, description, displayOnWebPageText);
			fc.utobj().printTestStep("Add Source Details");

			String sourceDetails = fc.utobj().generateTestData(dataSet.get("sourceDetails"));
			addSourcePage.addSourceDetails(driver, source, sourceDetails, displayOnWebPageText, description);

			fc.utobj().printTestStep("Configure Email Content for lead");
			AdminCRMConfigureEmailContentPageTest configureMail = new AdminCRMConfigureEmailContentPageTest();
			configureMail.configureAllEmailContentLead(driver);

			fc.utobj().printTestStep("Navigate To CRM > Home Page");
			fc.utobj().printTestStep("Add Lead");
			fc.crm().crm_common().CRMHomeLnk(driver);
			fc.utobj().clickElement(driver, pobj.addNew);
			fc.utobj().clickElement(driver, pobj.addLeadLnk);
			WebElement frame = fc.utobj().getElementByXpath(driver, ".//*[@class='newLayoutcboxIframe']");
			fc.utobj().switchFrame(driver, frame);
			String firstName = fc.utobj().generateTestData(dataSet.get("firstName"));
			fc.utobj().sendKeys(driver, pobj.leadFirstName, firstName);
			String lastName = fc.utobj().generateTestData(dataSet.get("lastName"));
			fc.utobj().sendKeys(driver, pobj.leadLastName, lastName);
			String address = fc.utobj().generateTestData(dataSet.get("address"));
			fc.utobj().sendKeys(driver, pobj.address, address);
			String city = fc.utobj().generateTestData(dataSet.get("city"));
			fc.utobj().sendKeys(driver, pobj.city, city);
			fc.utobj().selectDropDown(driver, pobj.country, "USA");
			fc.utobj().sendKeys(driver, pobj.zipcode, "123456");
			fc.utobj().selectDropDown(driver, pobj.state, "Alabama");
			fc.utobj().selectDropDown(driver, pobj.primaryContactMethod, "Email");
			fc.utobj().sendKeys(driver, pobj.phoneNumbers, "1236547896");
			fc.utobj().sendKeys(driver, pobj.faxNumbers, "1245789654");
			fc.utobj().sendKeys(driver, pobj.emailIds, "crmautomation@staffex.com");
			fc.utobj().sendKeys(driver, pobj.mobileNumbers, "8896724441");
			fc.utobj().selectDropDown(driver, pobj.leadSource, source);
			fc.utobj().sendKeys(driver, pobj.leadSourceDetails, sourceDetails);
			if (!fc.utobj().isSelected(driver, pobj.assignToCorporate)) {
				fc.utobj().clickElement(driver, pobj.assignToCorporate);
			}
			fc.utobj().selectDropDown(driver, pobj.selectCorporateUser, corpUser.getuserFullName());
			String comments = fc.utobj().generateTestData(dataSet.get("comments"));
			fc.utobj().sendKeys(driver, pobj.comments, comments);
			fc.utobj().clickElement(driver, pobj.saveBtn);
			fc.utobj().clickElement(driver, pobj.closeBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Verify Lead At CRM > Leads Summary Page");
			fc.utobj().clickElement(driver, pobj.leadsLink);
			leadsPage.searchLeadByOwner(driver, corpUser.getuserFullName(), "Open Leads");

			boolean verifyName = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[.='" + firstName + " " + lastName + "']");
			if (verifyName == false) {
				fc.utobj().throwsException("was not able to verify Name");
			}
			boolean ownerOfLead = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[.='" + firstName + " " + lastName
					+ "']/ancestor::tr/td[contains(text () , '" + corpUser.getuserFullName() + "')]");
			if (ownerOfLead == false) {
				fc.utobj().throwsException("was not able to verify owner of the lead");
			}
			boolean statusOfLead = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[.='" + firstName + " " + lastName + "']/ancestor::tr/td[contains(text () , 'New')]");
			if (statusOfLead == false) {
				fc.utobj().throwsException("was not able to verify staus of the lead");
			}

			fc.utobj().printTestStep("Verify Lead At CRM > Home Page");
			fc.utobj().clickElement(driver, pobj.homeLnk);

			boolean statusT = false;
			fc.utobj().clickElement(driver, pobj.manageBtn);
			if (!fc.utobj().isSelected(driver, pobj.leadBySource)) {
				fc.utobj().clickElement(driver, pobj.leadBySource);
				fc.utobj().clickElement(driver, pobj.applyBtn);
				statusT = true;
			}
			if (statusT == false) {
				fc.utobj().clickElement(driver, pobj.applyBtn);
			}

			fc.utobj().clickElement(driver, pobj.leadBySourceLeads);

			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//*[@id='leadSourceDiv']//*[contains(text () , '" + source + "')]/following-sibling::td/a"));
			fc.utobj().isTextDisplayed(driver, firstName + " " + lastName,
					"was not able to verify first and last name");

			if (!driver
					.findElement(By.xpath(".//*[.='" + firstName + " " + lastName
							+ "']/ancestor::tr/td[contains(text () ,'" + corpUser.getuserFullName() + "')]"))
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify user Name");
			}
			if (!driver
					.findElement(By.xpath(".//*[.='" + firstName + " " + lastName
							+ "']/ancestor::tr/td/a[contains(text () ,'crmautomation@staffex.com')]"))
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Email");
			}

			fc.utobj().printTestStep("Verify User Creation Mail");

			Map<String, String> mailInfo = fc.utobj().readMailBox("Your Account has been created",
					corpUser.getuserFullName(), emailId, "sdg@1a@Hfs");

			if (mailInfo.size() == 0 || !mailInfo.get("mailBody").contains(corpUser.getuserFullName())) {
				fc.utobj().throwsException("Was not able to verify Mail After User Creation");
			}

			fc.utobj().printTestStep("Verify Lead Creation Mail");

			Map<String, String> mailInfo1 = fc.utobj().readMailBox("New Lead Added", firstName, emailId, "sdg@1a@Hfs");

			if (mailInfo1.size() == 0 || !mailInfo1.get("mailBody").contains(lastName)) {
				fc.utobj().throwsException("Was not able to verify Lead Creation Mail");
			}

			if (mailInfo1.size() == 0 || !mailInfo1.get("mailBody").contains(comments)) {
				fc.utobj().throwsException("Was not able to verify Comments In Email");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "crm", "crmemailwe3"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2017-06-28", testCaseDescription = "Verify The Add Contact At CRM > Home And Verify Contact Creation Mail", testCaseId = "TC_57_Add_Contact")
	public void addContact() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			CRMHomePage pobj = new CRMHomePage(driver);
			CRMContactsPageTest contactsPage = new CRMContactsPageTest();

			fc.utobj().printTestStep("Navigate To Admin Users Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			String emailId = "crmautomation@staffex.com";
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUser = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporateUser.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			AdminCRMSourceSummaryPageTest addSourcePage = new AdminCRMSourceSummaryPageTest();
			String source = fc.utobj().generateTestData(dataSet.get("source"));
			String description = fc.utobj().generateTestData(dataSet.get("description"));
			String displayOnWebPageText = fc.utobj().generateTestData(dataSet.get("text"));
			fc.utobj().printTestStep("Navigating to Admin > CRM > Source Summary");
			fc.utobj().printTestStep("Add Source");
			addSourcePage.addSource(driver, source, description, displayOnWebPageText);
			fc.utobj().printTestStep("Add Source Details");
			String sourceDetails = fc.utobj().generateTestData(dataSet.get("sourceDetails"));
			addSourcePage.addSourceDetails(driver, source, sourceDetails, displayOnWebPageText, description);

			fc.utobj().printTestStep("Navigating to Admin > CRM > ContactType Configuration");
			fc.utobj().printTestStep("Add Contact Type");
			AdminCRMContactTypeConfigurationPageTest contactTypePage = new AdminCRMContactTypeConfigurationPageTest();
			String contactType = fc.utobj().generateTestData(dataSet.get("contactType"));
			contactTypePage.addContactType(driver, contactType);

			AdminCRMConfigureEmailContentPageTest configureMail = new AdminCRMConfigureEmailContentPageTest();
			configureMail.configureAllEmailContentContact(driver);

			fc.utobj().printTestStep("Navigate To CRM > Home Page");
			fc.utobj().printTestStep("Add Contact");
			fc.crm().crm_common().CRMHomeLnk(driver);
			fc.utobj().clickElement(driver, pobj.addNew);
			fc.utobj().clickElement(driver, pobj.addContactLnk);
			WebElement frame = fc.utobj().getElementByXpath(driver, ".//*[@class='newLayoutcboxIframe']");
			fc.utobj().switchFrame(driver, frame);
			fc.utobj().selectDropDown(driver, pobj.selectContactType, contactType);
			String firstName = fc.utobj().generateTestData(dataSet.get("firstName"));
			fc.utobj().sendKeys(driver, pobj.contactFirstName, firstName);
			String lastName = fc.utobj().generateTestData(dataSet.get("lastName"));
			fc.utobj().sendKeys(driver, pobj.contactLastName, lastName);
			String address = fc.utobj().generateTestData(dataSet.get("address"));
			fc.utobj().sendKeys(driver, pobj.address, address);
			String city = fc.utobj().generateTestData(dataSet.get("city"));
			fc.utobj().sendKeys(driver, pobj.city, city);
			fc.utobj().selectDropDown(driver, pobj.country, "USA");
			fc.utobj().sendKeys(driver, pobj.zipcode, "123456");
			fc.utobj().selectDropDown(driver, pobj.state, "Alabama");
			fc.utobj().selectDropDown(driver, pobj.primaryContactMethod, "Email");
			fc.utobj().sendKeys(driver, pobj.phoneNumbers, "1236547896");
			fc.utobj().sendKeys(driver, pobj.faxNumbers, "1245789654");
			fc.utobj().sendKeys(driver, pobj.emailIds, "crmautomation@staffex.com");
			fc.utobj().sendKeys(driver, pobj.mobileNumbers, "8896724441");
			fc.utobj().selectDropDown(driver, pobj.leadSource, source);
			fc.utobj().sendKeys(driver, pobj.leadSourceDetails, sourceDetails);
			if (!fc.utobj().isSelected(driver, pobj.assignToCorporate)) {
				fc.utobj().clickElement(driver, pobj.assignToCorporate);
			}
			fc.utobj().selectDropDown(driver, pobj.selectCorporateUserContact, corpUser.getuserFullName());
			fc.utobj().clickElement(driver, pobj.saveBtn);
			fc.utobj().clickElement(driver, pobj.closeBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Verify Contact At CRM > Contact Summary Page");
			fc.utobj().clickElement(driver, pobj.contactsLink);

			contactsPage.contactsFilter(driver, contactType);

			fc.utobj().printTestStep("Verify Contact At Contacts Summary Page");
			boolean isNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firstName + " " + lastName + "')]");
			if (isNamePresent == false) {
				fc.utobj().throwsException("was not able to verify Contact Name");
			}
			boolean isContactTypePresentAtHome = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[contains(text () ,'" + firstName
					+ " " + lastName + "')]/ancestor::tr/td[contains(text () ,'" + contactType + "')]");
			if (isContactTypePresentAtHome == false) {
				fc.utobj().throwsException("was not able to verify Contact Type");
			}
			boolean isOwnerPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[contains(text () ,'" + firstName + " "
					+ lastName + "')]/ancestor::tr/td[contains(text () ,'" + corpUser.getuserFullName() + "')]");
			if (isOwnerPresent == false) {
				fc.utobj().throwsException("was not able to verify owner");
			}

			fc.utobj().printTestStep("Verify Contact At CRM > Home Page");
			fc.utobj().clickElement(driver, pobj.homeLnk);

			boolean statusT = false;
			fc.utobj().clickElement(driver, pobj.manageBtn);
			if (!fc.utobj().isSelected(driver, pobj.leadBySource)) {
				fc.utobj().clickElement(driver, pobj.leadBySource);
				fc.utobj().clickElement(driver, pobj.applyBtn);
				statusT = true;
			}
			if (statusT == false) {
				fc.utobj().clickElement(driver, pobj.applyBtn);
			}

			fc.utobj().clickElement(driver, pobj.leadBySourceContact);

			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//*[@id='contactSourceDiv']//*[contains(text () , '" + source + "')]/following-sibling::td/a"));
			fc.utobj().isTextDisplayed(driver, firstName + " " + lastName,
					"was not able to verify first and last name");

			if (!fc.utobj().getElementByXpath(driver, ".//a[contains(text () ,'" + firstName + " " + lastName
					+ "')]/ancestor::tr/td[contains(text () ,'" + contactType + "')]").isDisplayed()) {
				fc.utobj().throwsException("was not able to verify contact Type");
			}
			if (!driver
					.findElement(By.xpath(".//a[contains(text () ,'" + firstName + " " + lastName
							+ "')]/ancestor::tr/td[contains(text () ,'" + corpUser.getuserFullName() + "')]"))
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify User Name");
			}
			if (!driver
					.findElement(By.xpath(".//a[contains(text () ,'" + firstName + " " + lastName
							+ "')]/ancestor::tr/td/a[contains(text () ,'crmautomation@staffex.com')]"))
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify User Name");
			}
			if (!fc.utobj().getElementByXpath(driver, ".//a[contains(text () ,'" + firstName + " " + lastName
					+ "')]/ancestor::tr/td[contains(text () ,'" + city + "')]").isDisplayed()) {
				fc.utobj().throwsException("was not able to verify User Name");
			}

			Thread.sleep(1000);
			fc.utobj().printTestStep("Verify Contact Creation Mail");

			Map<String, String> mailData = new HashMap<String, String>();

			mailData = fc.utobj().readMailBox("New Contact Added", firstName, emailId, "sdg@1a@Hfs");

			if (mailData.size() == 0 || !mailData.get("mailBody").contains("New Contact Added")) {

				fc.utobj().throwsException("was not able to verify Mail After Contact Creation");

			}

			if (mailData.size() == 0 || !mailData.get("mailBody").contains(contactType)) {

				fc.utobj().throwsException("was not able to verify contact type in Mail After Contact Creation");

			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}
}
