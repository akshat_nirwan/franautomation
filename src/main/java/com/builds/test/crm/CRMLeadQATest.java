package com.builds.test.crm;

import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.testng.Reporter;
import org.testng.annotations.Test;

import com.builds.uimaps.crm.CRMAccountsPage;
import com.builds.uimaps.crm.CRMLeadsPage;
import com.builds.uimaps.fs.FSLeadSummaryAddLeadPage;
import com.builds.utilities.FranconnectUtil;
import com.builds.utilities.TestCase;

public class CRMLeadQATest {

	FranconnectUtil fc = new FranconnectUtil();

	@Test(groups = { "crm", "crmQA"})
	@TestCase(createdOn = "2017-07-21", updatedOn = "2017-08-01", testCaseId = "TC_BasicData_CRM_QAData", testCaseDescription = "Verify CRM Lead/Account/contact QA Data")
	private void crmInfoFill() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		String testCaseIdInternal = "" + testCaseId;
		Map<String, String> printInfo = null;
		try {
			driver = fc.loginpage().login(driver);
			CRMLeadsPage pobj = new CRMLeadsPage(driver);

			fc.utobj().printTestStep("Navigate To CRM > Leads > Leads Summary");
			fc.utobj().printTestStep("Add Lead");
			fc.crm().crm_common().CRMLeadsLnk(driver);
			fc.utobj().clickElement(driver, pobj.addNew);
			fc.utobj().clickElement(driver, pobj.leadLnk);

			testCaseIdInternal = "TC_CRM_QA_Lead_Details";
			printInfo = fillFormDataInfo(driver, testCaseIdInternal);
			Reporter.log("Map == for =" + testCaseIdInternal + "===" + printInfo);

			fc.utobj().printTestStep("Navigate To CRM > Account > Account Summary");
			fc.utobj().printTestStep("Add Lead");
			fc.crm().crm_common().CRMLeadsLnk(driver);
			fc.utobj().clickElement(driver, pobj.addNew);
			fc.utobj().clickElement(driver, pobj.accountLnk);

			testCaseIdInternal = "TC_CRM_QA_Account_Details";
			printInfo = fillFormDataInfo(driver, testCaseIdInternal);
			Reporter.log("Map == for =" + testCaseIdInternal + "===" + printInfo);
			CRMAccountsPage accountPage = new CRMAccountsPage(driver);
			if (printInfo != null && printInfo.size() > 0) {
				fc.utobj().printTestStep("Navigate To CRM > Child Account");
				fc.utobj().clickElement(driver, accountPage.childAccounts);
				fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//a[.='Add Child Account']"));

				testCaseIdInternal = "TC_CRM_QA_childAccount_Details";
				printInfo = fillFormDataInfo(driver, testCaseIdInternal);
				Reporter.log("Map == for =" + testCaseIdInternal + "===" + printInfo);
			}

			if (printInfo != null && printInfo.size() > 0) {
				fc.utobj().printTestStep("Navigate To CRM > Contact");
				fc.utobj().clickElement(driver, accountPage.contactsTab);
				fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//a[.='Add Contact']"));
				
			
				testCaseIdInternal = "TC_CRM_QA_Contact_Details";
				printInfo = fillFormDataInfo(driver, testCaseIdInternal);
				Reporter.log("Map == for =" + testCaseIdInternal + "===" + printInfo);

			}
			if (printInfo != null && printInfo.size() > 0) {
				fc.utobj().printTestStep("Navigate To CRM > Contact Documents Tab");
				fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//span[.='Documents']"));
				testCaseIdInternal = "TC_CRM_QA_Document_Details";
				printInfo = fillFormDataInfo(driver, testCaseIdInternal);
				Reporter.log("Map == for =" + testCaseIdInternal + "===" + printInfo);

			}

			if (printInfo != null && printInfo.size() > 0) {
				fc.utobj().printTestStep("Navigate To CRM > Contact Other Addresses");
				fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//span[.='Other Addresses']"));
				testCaseIdInternal = "TC_CRM_QA_OtherAddresses_Details";
				printInfo = fillFormDataInfo(driver, testCaseIdInternal);
				Reporter.log("Map == for =" + testCaseIdInternal + "===" + printInfo);

			}

			if (printInfo != null && printInfo.size() > 0) {
				fc.utobj().printTestStep("Navigate To CRM >  Opportunity");
				fc.utobj().clickElement(driver, pobj.opportunityTab);
				fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//a[.='Add Opportunity']"));

				testCaseIdInternal = "TC_CRM_QA_Opportunity_Details";
				printInfo = fillFormDataInfo(driver, testCaseIdInternal);
				Reporter.log("Map == for =" + testCaseIdInternal + "===" + printInfo);

			}

			/*
			 * fc.utobj().printTestStep(testCaseId,
			 * "Navigate To CRM > Leads > Leads Summary");
			 * fc.utobj().printTestStep(testCaseId, "Add Lead");
			 * fc.crm().crm_common().CRMLeadsLnk( driver);
			 * fc.utobj().clickElement(driver, pobj.addNew);
			 * fc.utobj().clickElement(driver, pobj.contactLnk);
			 * 
			 * testCaseIdInternal = "TC_CRM_QA_Contact_Details"; printInfo =
			 * fillFormDataInfo(driver,config,testCaseIdInternal); Reporter.log(
			 * "Map == for ="+testCaseIdInternal+"==="+printInfo);
			 * 
			 * if(printInfo!=null && printInfo.size()>0) {
			 * fc.utobj().clickElement(driver,
			 * fc.utobj().getElementByXpath(driver,".//span[.='Documents']")));
			 * testCaseIdInternal = "TC_CRM_QA_Document_Details"; printInfo =
			 * fillFormDataInfo(driver,config,testCaseIdInternal); Reporter.log(
			 * "Map == for ="+testCaseIdInternal+"==="+printInfo);
			 * 
			 * }
			 * 
			 * if(printInfo!=null && printInfo.size()>0) {
			 * fc.utobj().clickElement(driver,
			 * fc.utobj().getElementByXpath(driver,
			 * ".//span[.='Other Addresses']"))); testCaseIdInternal =
			 * "TC_CRM_QA_OtherAddresses_Details"; printInfo =
			 * fillFormDataInfo(driver,config,testCaseIdInternal); Reporter.log(
			 * "Map == for ="+testCaseIdInternal+"==="+printInfo);
			 * 
			 * }
			 */

			/*
			 * if(printInfo!=null && printInfo.size()>0) {
			 * fc.utobj().clickElement(driver, pobj.acountInfoTab);
			 * fc.utobj().clickElement(driver, pobj.opportunityTab);
			 * fc.utobj().clickElement(driver,
			 * fc.utobj().getElementByXpath(driver, ".//a[.='Add Opportunity']"
			 * ))) ;
			 * 
			 * testCaseIdInternal = "TC_CRM_QA_Opportunity_Details"; printInfo =
			 * fillFormDataInfo(driver,config,testCaseIdInternal); Reporter.log(
			 * "Map == for ="+testCaseIdInternal+"==="+printInfo);
			 * 
			 * 
			 * }
			 */

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	public Map<String, String> fillFormDataInfo(WebDriver driver, String testCaseId) throws Exception {
		Reporter.log("Filling TestCase Data = " + testCaseId);
		// fc.utobj().printTestStep(testCaseId, "Filling TestCase Data");
		String id = "id";

		Map<String, String> dataSet = new HashMap<String, String>();
		dataSet = fc.utobj().readTestData("crm", testCaseId);

		List<String> listItems = new ArrayList<String>();
		Map<String, String> printInfo = new HashMap<String, String>();

		List<WebElement> mandateBox = null;
		mandateBox = driver.findElements(By.xpath(
				".//td[@qat_tableid='addData']//*[@type='text' or @class='fTextBoxDate' or @class='fTextBox' or @class='multiList' or @type='radio' or @type='checkbox' or @type='checkBox' or @id='ms-parentsellerCombo' or @class='ms-choice' or @class='form-control' or @type='file']"));

		Map<String, String> alreadyitrateElement = new HashMap<>();
		Map<String, String> alreadyradio = new HashMap<>();
		Map<String, String> alreadycheckBox = new HashMap<>();

		try {
			for (WebElement webElement : mandateBox) {

				try {
					if (webElement.getAttribute("" + id) != null && !"".equals(webElement.getAttribute("" + id))
							&& !"null".equals(webElement.getAttribute("" + id))
							&& !":".equals(webElement.getAttribute("" + id))
							&& !"cmSource2ID".equals(webElement.getAttribute("" + id))
							&& !"cmSource3ID".equals(webElement.getAttribute("" + id))) {

						// System.out.println("Key==>>"+webElement.getAttribute("id")+"===values==>>"+webElement.getAttribute("value")+"=====Type===>>>"+webElement.getAttribute("type"));
						if (alreadyitrateElement.containsKey(webElement.getAttribute("" + id))) {

						} else {
							if (dataSet.containsKey(webElement.getAttribute("" + id))) {
								WebElement elementmovepostion = null;
								if ("name".equals(id)) {
									elementmovepostion = driver
											.findElement(By.name("" + webElement.getAttribute("" + id)));

								} else {
									elementmovepostion = driver
											.findElement(By.id("" + webElement.getAttribute("" + id)));

								}
								fc.utobj().moveToElement(driver, elementmovepostion);
								if (webElement.getAttribute("type").indexOf("select-one") != -1) {
									Select singledrop = null;
									List<WebElement> singleDropDown = null;
									try {
										singledrop = new Select(fc.utobj().getElementByID(driver,
												"" + webElement.getAttribute("" + id)));
										singleDropDown = driver
												.findElements(By.id("" + webElement.getAttribute("" + id)));
									} catch (Exception e) {
										singledrop = new Select(
												driver.findElement(By.name("" + webElement.getAttribute("" + id))));
										singleDropDown = driver
												.findElements(By.name("" + webElement.getAttribute("" + id)));
									}
									if (webElement.getAttribute("value") == null
											|| "".equals(webElement.getAttribute("value"))
											|| "null".equals(webElement.getAttribute("value"))
											|| "-1".equals(webElement.getAttribute("value"))) {

										int size = singleDropDown.size();
										if (dataSet.get("" + webElement.getAttribute("" + id)) != null
												&& !"".equals(dataSet.get("" + webElement.getAttribute("" + id)))) {
											if (size >= 1) {
												try {
													WebElement dropDownField = driver
															.findElement(By.id("" + webElement.getAttribute("id")));
													fc.utobj().selectDropDownByVisibleText(driver, dropDownField,
															dataSet.get("" + webElement.getAttribute("" + id)));

													if (!"birthMonth".equals(webElement.getAttribute("" + id))
															&& !"birthDate".equals(webElement.getAttribute("" + id))
															&& !"spouseBirthMonth"
																	.equals(webElement.getAttribute("" + id))
															&& !"spouseBirthDate"
																	.equals(webElement.getAttribute("" + id))) {
														listItems.add(
																dataSet.get("" + webElement.getAttribute("" + id)));
													}

													printInfo.put("" + webElement.getAttribute("" + id),
															dataSet.get("" + webElement.getAttribute("" + id)));

												} catch (Exception eradio) {
													// Reporter.log("Problem in
													// selecting drop :
													// "+""+webElement.getAttribute("id")
													// +" Values getting from
													// Excel not match in
													// desired
													// fomrat."+""+dataSet.get(""+webElement.getAttribute("id")));
												}
											}
										}
									}
								} else if (webElement.getAttribute("type").indexOf("select-multiple") != -1) {
									if (webElement.getAttribute("value") == null
											|| "".equals(webElement.getAttribute("value"))
											|| "null".equals(webElement.getAttribute("value"))
											|| "-1".equals(webElement.getAttribute("value"))) {
										try {
											WebElement element = driver
													.findElement(By.id("ms-parent" + webElement.getAttribute("" + id)));
											fc.utobj().clickElement(driver, element);
											fc.utobj().clickElement(driver,
													fc.utobj().getElementByXpath(driver, ".//*[@id='selectAll']"));
											fc.utobj().clickElement(driver, element);

											fc.utobj().selectValFromMultiSelect(driver, element,
													dataSet.get("" + webElement.getAttribute("" + id)));

										} catch (Exception emultipal) {
											// Reporter.log("Problem in multiple
											// value drop-down :
											// "+""+webElement.getAttribute("id"));
										}
									}
								} else if (webElement.getAttribute("type").indexOf("textarea") != -1) {
									if (webElement.getAttribute("value") == null
											|| "".equals(webElement.getAttribute("value"))
											|| "null".equals(webElement.getAttribute("value"))) {
										try {
											driver.findElement(By.name("" + webElement.getAttribute("" + id)))
													.sendKeys(dataSet.get("" + webElement.getAttribute("" + id)));
											listItems.add(dataSet.get("" + webElement.getAttribute("" + id)));
											printInfo.put("" + webElement.getAttribute("id"),
													dataSet.get("" + webElement.getAttribute("" + id)));
										} catch (Exception eText) {
											// Reporter.log("Problem in entering
											// TextArea in :
											// "+""+webElement.getAttribute("id"));
										}
									}
								} else if (webElement.getAttribute("type").indexOf("radio") != -1) {
									try {
										String sValue = "";
										List<WebElement> rdBtn_Sex = driver
												.findElements(By.id("" + webElement.getAttribute("" + id))); //
										if (alreadyradio.containsKey(webElement.getAttribute("" + id))) {

										} else {

											int size = rdBtn_Sex.size();
											if (size > 1) {
												sValue = dataSet.get("" + webElement.getAttribute("" + id));
											}
											try {
												List<WebElement> rdBtn_Field = null;
												WebElement element = null;
												try {
													rdBtn_Field = driver.findElements(By.xpath(
															".//*[@name='" + webElement.getAttribute("" + id) + "']")); // work
													element = fc.utobj().getElementByXpath(driver,
															".//*[@name='" + webElement.getAttribute("" + id) + "']");
												} catch (Exception eTxt) {
													rdBtn_Field = driver.findElements(By.xpath(
															".//*[@id='" + webElement.getAttribute("id") + "']")); // work
													element = fc.utobj().getElementByXpath(driver,
															".//*[@id='" + webElement.getAttribute("id") + "']");

												}

												fc.utobj().moveToElement(driver, element);
												fc.utobj().clickRadioButton(driver, rdBtn_Field, sValue); // work
												listItems.add(dataSet.get("" + webElement.getAttribute("" + id)));
												printInfo.put("" + webElement.getAttribute("id"),
														dataSet.get("" + webElement.getAttribute("" + id)));
												alreadyradio.put(webElement.getAttribute("" + id),
														webElement.getAttribute("" + id));
											} catch (Exception eText) {
												// Reporter.log("Problem in
												// selecting radio value in :
												// "+""+webElement.getAttribute("id"));
											}
										}
									} catch (Exception exp) {
										Reporter.log(exp.toString());
										exp.printStackTrace();
									}

								} else if (webElement.getAttribute("type").toLowerCase().indexOf("checkbox") != -1) {
									List<WebElement> oCheckBox = driver
											.findElements(By.name("" + webElement.getAttribute("" + id)));
									String sValue = "";
									if (alreadycheckBox.containsKey(webElement.getAttribute("" + id))) {

									} else {

										int size = oCheckBox.size();
										for (int i = 0; i < size; i++) {
											sValue = oCheckBox.get(i).getAttribute("value");
											// Thread.sleep(2000);
											if (i == (size - 1)) {
												// break;
											}

										}
										sValue = dataSet.get("" + webElement.getAttribute("" + id));
										try {
											List<WebElement> rdBtn_Field = driver.findElements(By.xpath(
													"//*[contains(@id, '" + webElement.getAttribute("" + id) + "')]")); // work
											WebElement element = fc.utobj().getElementByXpath(driver,
													"//*[contains(@id, '" + webElement.getAttribute("" + id) + "')]");
											fc.utobj().moveToElement(driver, element);
											fc.utobj().clickRadioButton(driver, rdBtn_Field, sValue); // work
											alreadycheckBox.put(webElement.getAttribute("" + id),
													webElement.getAttribute("" + id));
										} catch (Exception eText) {
											// Reporter.log("Problem in
											// selecting CheckBox value in :
											// "+""+webElement.getAttribute("id"));
										}
									}
								} else if (webElement.getAttribute("type").indexOf("file") != -1) {
									String fileName = fc.utobj().getFilePathFromTestData("pictureFile");
									try {
										fc.utobj().sendKeys(driver, fc.utobj().getElementByID(driver,
												"" + webElement.getAttribute("" + id)), fileName);
										listItems.add(fileName);
										printInfo.put("" + webElement.getAttribute("" + id), fileName);
									} catch (Exception eFile) {
										// Reporter.log("Problem in uploading
										// file type value in :
										// "+""+webElement.getAttribute("id"));
									}
								} else if (webElement.getAttribute("type").indexOf("text") != -1) {
									if (webElement.getAttribute("value") == null
											|| "".equals(webElement.getAttribute("value"))
											|| "null".equals(webElement.getAttribute("value"))
											|| "0.00".equals(webElement.getAttribute("value"))) {
										if (webElement.getAttribute("class").indexOf("fTextBoxDate") != -1) {
											try {
												if (dataSet.get("" + webElement.getAttribute("" + id)) != null
														|| !"".equals(
																dataSet.get("" + webElement.getAttribute("" + id)))
														|| !"null".equals(
																dataSet.get("" + webElement.getAttribute("" + id)))) {
													fc.utobj().sendKeys(driver,
															driver.findElement(
																	By.name("" + webElement.getAttribute("" + id))),
															dataSet.get("" + webElement.getAttribute("" + id))); // work
													listItems.add(dataSet.get("" + webElement.getAttribute("" + id)));
													printInfo.put("" + webElement.getAttribute("" + id),
															dataSet.get("" + webElement.getAttribute("" + id)));

												}
											} catch (Exception eDate) {
												// Reporter.log("Problem in
												// Entering Date fields value in
												// :
												// "+""+webElement.getAttribute("id"));
											}
										} else {
											// System.out.println(webElement.getAttribute("value")
											// +"======"+dataSet.get(""+webElement.getAttribute(""+id))
											// +"====="+""+webElement.getAttribute(""+id));
											try {
												if ("0.00".equals(webElement.getAttribute("value"))) {
													fc.utobj().sendKeys(driver,
															driver.findElement(
																	By.name("" + webElement.getAttribute("" + id))),
															"");
												}
												if ("name".equals(id)) {
													fc.utobj().sendKeys(driver,
															driver.findElement(
																	By.name("" + webElement.getAttribute("" + id))),
															dataSet.get("" + webElement.getAttribute("" + id)));
													if ("accountName".equals("" + webElement.getAttribute("" + id))
															|| "opportunityOwner"
																	.equals("" + webElement.getAttribute("" + id))
															|| "oppContactID"
																	.equals("" + webElement.getAttribute("" + id))) {

														fc.utobj().clickElement(driver, driver.findElement(
																By.xpath(".//*[@id='customizedAjaxSearch']/div")));

													}
												} else {
													fc.utobj().sendKeys(driver,
															driver.findElement(
																	By.id("" + webElement.getAttribute("" + id))),
															dataSet.get("" + webElement.getAttribute("" + id)));
													if ("accountName".equals("" + webElement.getAttribute("" + id))
															|| "opportunityOwner"
																	.equals("" + webElement.getAttribute("" + id))
															|| "oppContactID"
																	.equals("" + webElement.getAttribute("" + id))) {

														fc.utobj().clickElement(driver, driver.findElement(
																By.xpath(".//*[@id='customizedAjaxSearch']/div")));

													}
												}

												if (!"ssn".equals("" + webElement.getAttribute("" + id))
														&& !"spouseSsn".equals("" + webElement.getAttribute("" + id))) {
													listItems.add(dataSet.get("" + webElement.getAttribute("" + id)));
												}
												printInfo.put("" + webElement.getAttribute("" + id),
														dataSet.get("" + webElement.getAttribute("" + id)));

											} catch (Exception eText) {
												// Reporter.log("Problem in
												// Entering Text fileds value in
												// :
												// "+""+webElement.getAttribute("id"));
											}

										}
									}
								}
								alreadyitrateElement.put(webElement.getAttribute("" + id),
										webElement.getAttribute("" + id));
							}
						}
					}
				} catch (Exception ee) {
					ee.printStackTrace();
				}
			}

			if ("TC_CRM_QA_Lead_Details".equals(testCaseId)) {

				Select singledrop = null;
				List<WebElement> singleDropDown = null;
				try {
					singledrop = new Select(driver.findElement(By.name("leadOwnerID2")));
					singleDropDown = driver.findElements(By.name("leadOwnerID2"));
					WebElement dropDownField = driver.findElement(By.name("leadOwnerID2"));
					fc.utobj().selectDropDownByVisibleText(driver, dropDownField, dataSet.get("leadOwnerID2"));

					dropDownField = driver.findElement(By.name("cmSource2ID"));
					fc.utobj().selectDropDownByVisibleText(driver, dropDownField, dataSet.get("cmSource2ID"));

					dropDownField = driver.findElement(By.name("cmSource3ID"));
					fc.utobj().selectDropDownByVisibleText(driver, dropDownField, dataSet.get("cmSource3ID"));

				} catch (Exception e) {

				}

			}
			if ("TC_CRM_QA_Contact_Details".equals(testCaseId)) {

				Select singledrop = null;
				List<WebElement> singleDropDown = null;
				try {
					singledrop = new Select(driver.findElement(By.name("contactOwnerID2")));
					singleDropDown = driver.findElements(By.name("contactOwnerID2"));
					WebElement dropDownField = driver.findElement(By.name("contactOwnerID2"));
					fc.utobj().selectDropDownByVisibleText(driver, dropDownField, dataSet.get("contactOwnerID2"));
					
					/*dropDownField = driver.findElement(By.name("cmSource2ID"));
					fc.utobj().selectDropDownByVisibleText(driver, dropDownField, dataSet.get("cmSource2ID"));

					dropDownField = driver.findElement(By.name("cmSource3ID"));
					fc.utobj().selectDropDownByVisibleText(driver, dropDownField, dataSet.get("cmSource3ID"));*/

				} catch (Exception e) {

				}

			}

		} catch (Exception e) {
			e.printStackTrace();
			fc.utobj().throwsException("Fields data are not available! " + e.getMessage());
		}
		try {
			fc.utobj().printTestStep("Submiting the Tab ");
			WebElement elementbutton = fc.utobj().getElementByXpath(driver,
					".//*[@id='Submit' or @name='Submit' or @name='button' or @name='Submit1' or @name='add1'] ");
			fc.utobj().clickElement(driver, elementbutton);
			try {
				fc.utobj().acceptAlertBox(driver);
			} catch (Exception e) {
			}
		} catch (Exception ee) {
			fc.utobj().printBugStatus("Unable to submit page " + ee);
		}
		fc.utobj().printTestStep("verifying The fields in page soruce. ");
		boolean isLeadInfoFound = fc.utobj().assertPageSourceWithMultipleRecords(driver, listItems);
		if (isLeadInfoFound == false) {
			fc.utobj().printBugStatus("Either some fields not matched in page source.");
		}
		return printInfo;
	}

	public Map<String, String> fillFormDataExcel(WebDriver driver, Map<String, String> config, String testCaseId)
			throws Exception {
		Reporter.log("Filling TestCase Data = " + testCaseId);

		String id = "id";
		Map<String, String> dataSet = new HashMap<String, String>();
		dataSet = fc.utobj().readTestData("crm", testCaseId);

		if ("TC_InfoMgr_Franchisees_Add_Employees".equals(testCaseId)
				|| "TC_InfoMgr_Franchisees_Add_EntityDetails".equals(testCaseId)
				|| "TC_InFoMgr_Franchisees_Add_Transfer".equals(testCaseId)) {
			id = "name";
		}

		List<String> listItems = new ArrayList<String>();
		Map<String, String> leadInfo = new HashMap<String, String>();

		List<WebElement> mandateBox = null;
		mandateBox = driver.findElements(By.xpath(
				".//*[@qat_tableid='addData']//*[@type='text' or @class='fTextBoxDate' or @class='fTextBox' or @class='multiList' or @type='radio' or @type='checkbox' or @type='checkBox' or @id='ms-parentsellerCombo' or @class='ms-choice' or @class='form-control']"));

		Map<String, String> alreadyitrateElement = new HashMap<>();
		Map<String, String> alreadyradio = new HashMap<>();
		Map<String, String> alreadycheckBox = new HashMap<>();
		String filePath = "C:\\Users\\admin\\Downloads\\" + testCaseId + ".xls";
		try {

			FileOutputStream fout = new FileOutputStream(filePath);
			Workbook wb = new HSSFWorkbook();
			Sheet sh = wb.createSheet();

			CellStyle style = wb.createCellStyle();
			style.setWrapText(true);

			Row r = sh.createRow(0);
			r.setRowStyle(style);

			Cell c1 = r.createCell(0);
			c1.setCellStyle(style);

			c1.setCellValue(testCaseId);

			Row r2 = sh.createRow(1);
			r2.setRowStyle(style);

			int counter = 0;
			for (WebElement webElement : mandateBox) {

				try {
					System.out.println("ID==>>" + webElement.getAttribute("id"));

					// System.out.println("ID==>>"+webElement.getAttribute("id")+"===Name==>>"+webElement.getAttribute("name")+"=====Type===>>>"+webElement.getAttribute("type"));

					if (webElement.getAttribute("" + id) != null && !"".equals(webElement.getAttribute("" + id))
							&& !"null".equals(webElement.getAttribute("" + id))
							&& !":".equals(webElement.getAttribute("" + id))) {

						// System.out.println("Key==>>"+webElement.getAttribute("id")+"===values==>>"+webElement.getAttribute("value")+"=====Type===>>>"+webElement.getAttribute("type"));
						if (alreadyitrateElement.containsKey(webElement.getAttribute("" + id))) {

						} else {
							Cell c2 = r2.createCell(counter);
							c2.setCellStyle(style);
							sh.setColumnWidth(counter, 10000);
							// System.out.println(""+webElement.getAttribute("id"));
							alreadyitrateElement.put(webElement.getAttribute("" + id),
									webElement.getAttribute("" + id));
							c2.setCellValue("" + webElement.getAttribute("" + id));
							counter++;
						}
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			}

			wb.write(fout);
			wb.close();

		} catch (Exception e) {
			fc.utobj().throwsException("Fields data are not available! " + e.getMessage());
		}

		return leadInfo;
	}

	public Map<String, String> fillFormData(WebDriver driver, Map<String, String> config, String testCaseId)
			throws Exception {
		Reporter.log("Filling " + testCaseId + " Info");

		Map<String, String> dataSet = new HashMap<String, String>();
		dataSet = fc.utobj().readTestData("crm", testCaseId);

		List<String> listItems = new ArrayList<String>();
		Map<String, String> leadInfo = new HashMap<String, String>();

		List<WebElement> mandateBox = null;
		mandateBox = driver.findElements(By.xpath(
				".//*[@qat_tableid='addData']//*[@type='text' or @class='fTextBoxDate' or @class='fTextBox' or @class='multiList' or @type='radio' or @type='checkbox' or @type='checkBox' or @id='ms-parentsellerCombo' or @class='ms-choice' or @class='form-control']"));

		Map<String, String> alreadyitrateElement = new HashMap<>();
		Map<String, String> alreadyradio = new HashMap<>();
		Map<String, String> alreadycheckBox = new HashMap<>();

		try {
			for (WebElement webElement : mandateBox) {

				if (webElement.getAttribute("id") != null && !"".equals(webElement.getAttribute("id"))
						&& !"null".equals(webElement.getAttribute("id")) && !":".equals(webElement.getAttribute("id"))
						&& !"dateOfOpen".equals(webElement.getAttribute("id"))
						&& !"fimCbOtherComplaintType".equals(webElement.getAttribute("id"))) {
					// System.out.println("Key==>>"+webElement.getAttribute("id")+"===values==>>"+webElement.getAttribute("value")+"=====Type===>>>"+webElement.getAttribute("type"));
					if (alreadyitrateElement.containsKey(webElement.getAttribute("id"))) {

					} else {
						if (dataSet.containsKey(webElement.getAttribute("id"))) {
							WebElement elementmovepostion = driver
									.findElement(By.id("" + webElement.getAttribute("id")));
							fc.utobj().moveToElement(driver, elementmovepostion);

							// System.out.println("Key==Matched>>"+webElement.getAttribute("id")+"===Inside
							// ==>>>>"+dataSet.get(""+webElement.getAttribute("id")));

							// System.out.println(webElement.getAttribute("id")+"=====>>"+webElement.getAttribute("value")+"=====Type="+webElement.getAttribute("type"));
							if (webElement.getAttribute("type").indexOf("select-one") != -1) {
								Select singledrop = new Select(
										fc.utobj().getElementByID(driver, "" + webElement.getAttribute("id")));
								if (webElement.getAttribute("value") == null
										|| "".equals(webElement.getAttribute("value"))
										|| "null".equals(webElement.getAttribute("value"))
										|| "-1".equals(webElement.getAttribute("value"))) {
									List<WebElement> singleDropDown = driver
											.findElements(By.name("" + webElement.getAttribute("id")));

									int size = singleDropDown.size();
									// System.out.println("singile drop down
									// size =="+size);
									if (size > 1) {
										// singledrop.selectByIndex(1);
										WebElement dropDownField = driver
												.findElement(By.id("" + webElement.getAttribute("id")));
										fc.utobj().selectDropDownByVisibleText(driver, dropDownField,
												dataSet.get("" + webElement.getAttribute("id")));

									} else {
										// singledrop.selectByIndex(-1);
									}
								}

							} else if (webElement.getAttribute("type").indexOf("select-multiple") != -1) {
								if (webElement.getAttribute("value") == null
										|| "".equals(webElement.getAttribute("value"))
										|| "null".equals(webElement.getAttribute("value"))
										|| "-1".equals(webElement.getAttribute("value"))) {
									WebElement element = driver
											.findElement(By.id("ms-parent" + webElement.getAttribute("id")));
									fc.utobj().clickElement(driver, element);
									fc.utobj().clickElement(driver,
											fc.utobj().getElementByXpath(driver, ".//*[@id='selectAll']"));
									fc.utobj().clickElement(driver, element);
								}
							} else if (webElement.getAttribute("type").indexOf("textarea") != -1) {
								if (webElement.getAttribute("value") == null
										|| "".equals(webElement.getAttribute("value"))
										|| "null".equals(webElement.getAttribute("value"))) {
									driver.findElement(By.name("" + webElement.getAttribute("id")))
											.sendKeys(dataSet.get("" + webElement.getAttribute("id")));
								}
							} else if (webElement.getAttribute("type").indexOf("radio") != -1) {
								try {
									String sValue = "";
									List<WebElement> rdBtn_Sex = driver
											.findElements(By.id("" + webElement.getAttribute("id"))); //
									// Thread.sleep(5000);
									if (alreadyradio.containsKey(webElement.getAttribute("id"))) {

									} else {

										int size = rdBtn_Sex.size();
										for (int i = 0; i < size; i++) {
											// Thread.sleep(1000);
											sValue = rdBtn_Sex.get(i).getAttribute("value");
											if (i == (size - 1)) {
												// Thread.sleep(1000);
												// rdBtn_Sex.get(0).click();

											}

										}
										// Thread.sleep(1000);
										List<WebElement> rdBtn_Field = driver.findElements(By.xpath(
												"//*[contains(@name, '" + webElement.getAttribute("id") + "')]")); // work
										WebElement element = fc.utobj().getElementByXpath(driver,
												"//*[contains(@name, '" + webElement.getAttribute("id") + "')]");
										fc.utobj().moveToElement(driver, element);
										fc.utobj().clickRadioButton(driver, rdBtn_Field, sValue); // work
										alreadyradio.put(webElement.getAttribute("id"), webElement.getAttribute("id"));
									}
								} catch (Exception exp) {
									Reporter.log(exp.toString());
									exp.printStackTrace();
								}

							} else if (webElement.getAttribute("type").toLowerCase().indexOf("checkbox") != -1) {
								List<WebElement> oCheckBox = driver
										.findElements(By.name("" + webElement.getAttribute("id")));
								String sValue = "";
								if (alreadycheckBox.containsKey(webElement.getAttribute("id"))) {

								} else {

									int size = oCheckBox.size();
									for (int i = 0; i < size; i++) {
										sValue = oCheckBox.get(i).getAttribute("value");
										// Thread.sleep(2000);
										if (i == (size - 1)) {

											// break;
										}

									}
									List<WebElement> rdBtn_Field = driver.findElements(
											By.xpath("//*[contains(@id, '" + webElement.getAttribute("id") + "')]")); // work
									WebElement element = driver.findElement(
											By.xpath("//*[contains(@id, '" + webElement.getAttribute("id") + "')]"));
									fc.utobj().moveToElement(driver, element);
									fc.utobj().clickRadioButton(driver, rdBtn_Field, sValue); // work
									alreadycheckBox.put(webElement.getAttribute("id"), webElement.getAttribute("id"));
								}

							} else if (webElement.getAttribute("type").indexOf("file") != -1) {
								String fileName = fc.utobj().getFilePathFromTestData("pictureFile");
								fc.utobj().sendKeys(driver,
										fc.utobj().getElementByID(driver, "" + webElement.getAttribute("id")),
										fileName);
							} else if (webElement.getAttribute("type").indexOf("text") != -1) {
								if (webElement.getAttribute("value") == null
										|| "".equals(webElement.getAttribute("value"))
										|| "null".equals(webElement.getAttribute("value"))) {
									if (webElement.getAttribute("class").indexOf("fTextBoxDate") != -1) {
										fc.utobj().sendKeys(driver,
												driver.findElement(By.name("" + webElement.getAttribute("id"))),
												"02/08/2018"); // work
									} else {
										driver.findElement(By.name("" + webElement.getAttribute("id")))
												.sendKeys(dataSet.get("" + webElement.getAttribute("id")));
										listItems.add(dataSet.get("" + webElement.getAttribute("id")));
										leadInfo.put("" + webElement.getAttribute("id"),
												dataSet.get("" + webElement.getAttribute("id")));

									}
								}
							}
							alreadyitrateElement.put(webElement.getAttribute("id"), webElement.getAttribute("id"));

						} else {
							WebElement elementmovepostion = driver
									.findElement(By.id("" + webElement.getAttribute("id")));
							fc.utobj().moveToElement(driver, elementmovepostion);

							// System.out.println("Key==NotMached>>"+webElement.getAttribute("id")+"===values==>>"+webElement.getAttribute("value")+"=====Type===>>>"+webElement.getAttribute("type"));

							// System.out.println(webElement.getAttribute("id")+"=====>>"+webElement.getAttribute("value")+"=====Type="+webElement.getAttribute("type"));
							if (webElement.getAttribute("type").indexOf("select-one") != -1) {
								Select singledrop = new Select(
										fc.utobj().getElementByID(driver, "" + webElement.getAttribute("id")));
								if (webElement.getAttribute("value") == null
										|| "".equals(webElement.getAttribute("value"))
										|| "null".equals(webElement.getAttribute("value"))
										|| "-1".equals(webElement.getAttribute("value"))) {
									List<WebElement> singleDropDown = driver
											.findElements(By.name("" + webElement.getAttribute("id")));

									int size = singleDropDown.size();
									// System.out.println("singile drop down
									// size =="+size);
									if (size >= 1) {
										try {
											singledrop.selectByIndex(1);
										} catch (Exception e) {

										}
									} else {
										// singledrop.selectByIndex(-1);
									}

								}

							} else if (webElement.getAttribute("type").indexOf("select-multiple") != -1) {
								if (webElement.getAttribute("value") == null
										|| "".equals(webElement.getAttribute("value"))
										|| "null".equals(webElement.getAttribute("value"))
										|| "-1".equals(webElement.getAttribute("value"))) {
									WebElement element = driver
											.findElement(By.id("ms-parent" + webElement.getAttribute("id")));
									fc.utobj().clickElement(driver, element);
									fc.utobj().clickElement(driver,
											fc.utobj().getElementByXpath(driver, ".//*[@id='selectAll']"));
									fc.utobj().clickElement(driver, element);
								}
							} else if (webElement.getAttribute("type").indexOf("textarea") != -1) {
								if (webElement.getAttribute("value") == null
										|| "".equals(webElement.getAttribute("value"))
										|| "null".equals(webElement.getAttribute("value"))) {
									driver.findElement(By.name("" + webElement.getAttribute("id")))
											.sendKeys("CustomTaxt_" + webElement.getAttribute("type"));
								}
							} else if (webElement.getAttribute("type").indexOf("radio") != -1) {
								try {
									String sValue = "";
									List<WebElement> rdBtn_Sex = driver
											.findElements(By.name("" + webElement.getAttribute("id"))); //
									// Thread.sleep(5000);
									if (alreadyradio.containsKey(webElement.getAttribute("id"))) {

									} else {

										int size = rdBtn_Sex.size();
										for (int i = 0; i < size; i++) {
											// Thread.sleep(1000);
											sValue = rdBtn_Sex.get(i).getAttribute("value");
											if (i == (size - 1)) {
												// Thread.sleep(1000);
												// rdBtn_Sex.get(0).click();

											}

										}
										// Thread.sleep(1000);
										List<WebElement> rdBtn_Field = driver.findElements(By
												.xpath("//*[contains(@id, '" + webElement.getAttribute("id") + "')]")); // work
										WebElement element = driver.findElement(By
												.xpath("//*[contains(@id, '" + webElement.getAttribute("id") + "')]"));
										fc.utobj().moveToElement(driver, element);
										fc.utobj().clickRadioButton(driver, rdBtn_Field, sValue); // work
										alreadyradio.put(webElement.getAttribute("id"), webElement.getAttribute("id"));
									}
								} catch (Exception exp) {
									Reporter.log(exp.toString());
									exp.printStackTrace();
								}

							} else if (webElement.getAttribute("type").toLowerCase().indexOf("checkbox") != -1) {
								List<WebElement> oCheckBox = driver
										.findElements(By.name("" + webElement.getAttribute("id")));
								String sValue = "";
								if (alreadycheckBox.containsKey(webElement.getAttribute("id"))) {

								} else {

									int size = oCheckBox.size();
									for (int i = 0; i < size; i++) {
										sValue = oCheckBox.get(i).getAttribute("value");
										// Thread.sleep(2000);
										if (i == (size - 1)) {

											// break;
										}

									}
									List<WebElement> rdBtn_Field = driver.findElements(
											By.xpath("//*[contains(@id, '" + webElement.getAttribute("id") + "')]")); // work
									WebElement element = driver.findElement(
											By.xpath("//*[contains(@id, '" + webElement.getAttribute("id") + "')]"));
									fc.utobj().moveToElement(driver, element);
									fc.utobj().clickRadioButton(driver, rdBtn_Field, sValue); // work
									alreadycheckBox.put(webElement.getAttribute("id"), webElement.getAttribute("id"));
								}

							} else if (webElement.getAttribute("type").indexOf("file") != -1) {
								String fileName = fc.utobj().getFilePathFromTestData("pictureFile");
								fc.utobj().sendKeys(driver,
										fc.utobj().getElementByID(driver, "" + webElement.getAttribute("id")),
										fileName);
							} else if (webElement.getAttribute("type").indexOf("text") != -1) {
								if (webElement.getAttribute("value") == null
										|| "".equals(webElement.getAttribute("value"))
										|| "null".equals(webElement.getAttribute("value"))) {
									if (webElement.getAttribute("class").indexOf("fTextBoxDate") != -1) {
										fc.utobj().sendKeys(driver,
												driver.findElement(By.name("" + webElement.getAttribute("id"))),
												"02/08/2018"); // work
									} else {

										if (webElement.getAttribute("id").indexOf("email") != -1) {
											driver.findElement(By.name("" + webElement.getAttribute("id")))
													.sendKeys("harish.dwivedi@franconnect.net");
										} else {
											driver.findElement(By.name("" + webElement.getAttribute("id")))
													.sendKeys("21");
											listItems.add("22");
											leadInfo.put("" + webElement.getAttribute("id"), "22");
										}
									}
								}
							}
							alreadyitrateElement.put(webElement.getAttribute("id"), webElement.getAttribute("id"));

						}

					}
				}

			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		FSLeadSummaryAddLeadPage pobj = new FSLeadSummaryAddLeadPage(driver);

		try {
			fc.utobj().clickElement(driver, pobj.save);
		} catch (Exception e) {
			try {
				WebElement elementbutton = fc.utobj().getElementByXpath(driver, ".//*[@name='button']");
				fc.utobj().clickElement(driver, elementbutton);
			} catch (Exception ee) {
				WebElement elementbutton = fc.utobj().getElementByXpath(driver, ".//*[@name='Submit']");
				fc.utobj().clickElement(driver, elementbutton);
			}

			// fc.utobj().throwsException("Basic Fields are not available!");
		}
		
		boolean isLeadInfoFound = fc.utobj().assertPageSourceWithMultipleRecords(driver, listItems);
		if (isLeadInfoFound == false) {
			fc.utobj().printBugStatus("Either Lead Not Added or lead info missing in lead!");
		}
		return leadInfo;
	}

}