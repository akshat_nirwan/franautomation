package com.builds.test.crm;


import java.util.List;
import java.util.Map;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.Test;

import com.builds.test.admin.AdminUsersManageCorporateUsersAddCorporateUserPageTest;
import com.builds.test.common.CRMModule;
import com.builds.test.common.CorporateUser;
import com.builds.test.common.FCHomePageTest;
import com.builds.uimaps.common.FCHomePage;
import com.builds.uimaps.crm.CRMContactsPage;
import com.builds.uimaps.crm.CRMTasksPage;
import com.builds.uimaps.crm.CRM_CampaignAddUI;
import com.builds.uimaps.crm.CRM_CampaignFinalandLunchUI;
import com.builds.uimaps.crm.CRM_CampaignSelectRecipientUI;
import com.builds.uimaps.crm.CRM_CreateWorkFlowPageUI;
import com.builds.uimaps.crm.CRM_LeadinfopageUI;
import com.builds.uimaps.crm.CRM_SendEmailUI;
import com.builds.uimaps.crm.CRM_TemplateInfoPageUI;
import com.builds.uimaps.crm.ConvertLeadPageUI;
import com.builds.uimaps.crm.LeadUI;
import com.builds.uimaps.crm.LeadsummarypageUI;
import com.builds.utilities.FranconnectUtil;
import com.builds.utilities.TestCase;

public class CRMLeadSummary {
	FranconnectUtil fc = new FranconnectUtil();
    
	
	@Test(groups = "crm")
	@TestCase(createdOn = "2018-05-07", updatedOn = "2018-05-07", testCaseDescription = "CRM > Lead Summary > Add Lead > verify same email already present in opt-in sub and add,Modify and Reset lead", testCaseId = "TC_28_Add,modify, Reset and verify email sub")
	public void leadInfopage() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			// This will open the CRM Module
			FCHomePageTest fcHomePageTest = fc.commonMethods().getModules();
			Leadgetset lead = new Leadgetset();
			CRMModule crm = new CRMModule();
			// Go to Lead in CRM

			crm.crm_common().openCRMLeadsPage(fcHomePageTest, driver);
			boolean actionbtn = driver.findElement(By.xpath(".//td/a[contains(text(),'Actions')]")).isDisplayed();
			if(actionbtn ==true) {
				System.out.println("action button present");
				fc.utobj().printTestStep("action button present on lead summary page");
			}else{
				System.out.println("action button not present");
				fc.utobj().printTestStep("action button not present on lead summary page");
			}
			LeadSummaryPage addleadlink = new LeadSummaryPage();
			addleadlink.clickAddLead(driver);

			lead = crm.crm_common().getDefaultDataForLead(lead);

			Addleadpage addlead = new Addleadpage();
			addlead.fillLeadDetails(driver, lead);
			addlead.save(driver);
			Thread.sleep(1000);
			

			CRM_leadInfoPageOptions leadinfo = new CRM_leadInfoPageOptions();
			leadinfo.modifyLeadOnInfopage(driver);

			addlead.fillLeadDetails(driver, lead);
			addlead.reset(driver);
			lead = crm.crm_common().getDefaultDataForLead(lead);
			addlead.fillLeadDetails(driver, lead);
			addlead.save(driver);
 			
            String activehst = driver.findElement(By.id("ActivityHis")).getText();
			String newlead = "Lead is added through FranConnect Application.";
			String leadsub = "Lead has been updated with Opted-In subscription status as another Contact / Lead with same Email is present in the system with this Subscription status.";
			
			if (activehst.contains(newlead))  {   
				
				fc.utobj().printTestStep("add new lead remark present in the activity history");
			}
			if(activehst.contains(leadsub)) {
				
				fc.utobj().printTestStep("same email already present so opted in subscription remark present in the activity history");
			}
			
			
			System.out.println("lead modify done");

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = {"crm"})
	@TestCase(createdOn = "2018-05-07", updatedOn = "2018-05-07", testCaseDescription = "CRM > Lead Summary > Add Lead > log a call", testCaseId = "TC_28_log a Call against Lead and lead filter button working or not")
	public void leadInfopage1() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			// This will open the CRM Module
			FCHomePageTest fcHomePageTest = fc.commonMethods().getModules();
			Leadgetset lead = new Leadgetset();
			CRMModule crm = new CRMModule();
			// Go to Lead in CRM

			crm.crm_common().openCRMLeadsPage(fcHomePageTest, driver);
			LeadsummarypageUI ui = new LeadsummarypageUI(driver);
			if(ui.ShowFilter.isDisplayed()) {
				fc.utobj().printTestStep("Hidefilter button not display so click on show filter");
			fc.utobj().clickElement(driver, ui.ShowFilter);
			}
			if(ui.HideFilter.isDisplayed()) {
				fc.utobj().printTestStep("click on hide filter button ");
			fc.utobj().clickElement(driver, ui.HideFilter);
			}
			LeadSummaryPage summ = new LeadSummaryPage();
			summ.clickAddLead(driver);

			lead = crm.crm_common().getDefaultDataForLead(lead);

			Addleadpage addlead = new Addleadpage();
			addlead.fillLeadDetails(driver, lead);
			addlead.save(driver);

			CRM_leadInfoPageOptions leadinfo = new CRM_leadInfoPageOptions();
			leadinfo.logACallomOnInfoPage(driver);
			CRM_LogaCallfillTest callfill = new CRM_LogaCallfillTest();
			CRM_LogaCallgetsetTest call = new CRM_LogaCallgetsetTest();
			call = crm.crm_common().defualtdataForCallRemark(call);

			Thread.sleep(2000);
			callfill.fillcalremarkpage(driver, call);
			callfill.clickreset(driver);
			callfill.fillcalremarkpage(driver, call);
			callfill.clickSave(driver);
			
			String call_Subject = call.getSubject();
			fc.utobj().printTestStep("log a call done");
			
			
			
            String activehst = driver.findElement(By.id("ActivityHis")).getText();
			
			
			if (activehst.contains(call_Subject))  {
				fc.utobj().printTestStep("call found in activity history");
				System.out.println("call found in activity history");
				WebElement call_active = driver.findElement(By.xpath("//span[@class = 'callfrom'][contains(text(),'"+call_Subject+"')]"));
				fc.utobj().clickElement(driver, call_active);
				
			}else {
				fc.utobj().printTestStep("call not found");
				
			}
			fc.utobj().sleep(1000);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			
			String verify = driver.findElement(By.xpath(".//td[contains(text(),'Subject :')]/ancestor::tr[1]/td[2]")).getText();
			if(verify.equalsIgnoreCase(call_Subject) ) {
				System.out.println("log a call subject name match");
				fc.utobj().printTestStep("Remark verify of log a call");
				System.out.println("click on close button");
				fc.utobj().clickElement(driver, ".//input[@value = 'Close']");
				
			}
			fc.utobj().printTestStep("log a call full test run");
			
			
			

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "crm")
	@TestCase(createdOn = "2018-05-07", updatedOn = "2018-05-07", testCaseDescription = "CRM > Lead Summary > Add Lead > Add Remak", testCaseId = "TC_28_Add_Remark against Lead")
	public void leadInfopage2() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			// This will open the CRM Module
			FCHomePageTest fcHomePageTest = fc.commonMethods().getModules();
			Leadgetset lead = new Leadgetset();
			CRMModule crm = new CRMModule();
			// Go to Lead in CRM

			crm.crm_common().openCRMLeadsPage(fcHomePageTest, driver).clickAddLead(driver);

			lead = crm.crm_common().getDefaultDataForLead(lead);

			Addleadpage addlead = new Addleadpage();
			addlead.fillLeadDetails(driver, lead);
			addlead.save(driver);
			
			

			CRM_leadInfoPageOptions leadinfo = new CRM_leadInfoPageOptions();

			CRM_AddRemarkgetsetTest re = new CRM_AddRemarkgetsetTest();
			re = crm.crm_common().defualtaddreamrk(re);
			CRM_AddRemarkfill addre = new CRM_AddRemarkfill();
			CRM_LogaCallfillTest callfill = new CRM_LogaCallfillTest();
			leadinfo.remarkOnInfoPage(driver);
			addre.fillremark(driver, re);
			callfill.clickreset(driver);
			addre.fillremark(driver, re);
			callfill.clickSave(driver);
			System.out.println("remark done");

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "crm")
	@TestCase(createdOn = "2018-05-07", updatedOn = "2018-05-07", testCaseDescription = "CRM > Lead Summary > Add Lead > status changed", testCaseId = "TC-12995_Add_Status then add lead and change status")
	public void leadInfopage3() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			// This will open the CRM Module
			FCHomePageTest fcHomePageTest = fc.commonMethods().getModules();
			Leadgetset lead = new Leadgetset();
			CRMModule crm = new CRMModule();
			// Go to Lead in CRM
            crm.crm_common().AdminCRMStatus_change(driver, "Selenium", "Leads");
			crm.crm_common().openCRMLeadsPage(fcHomePageTest, driver).clickAddLead(driver);

			lead = crm.crm_common().getDefaultDataForLead(lead);

			Addleadpage addlead = new Addleadpage();
			addlead.fillLeadDetails(driver, lead);
			addlead.save(driver);

			CRM_leadInfoPageOptions leadinfo = new CRM_leadInfoPageOptions();

			WebElement status = driver.findElement(By.id("cmLeadStatusID"));
			status.click();
			Select choosestatus = new Select(status);
			choosestatus.selectByVisibleText("Selenium");
			leadinfo.changestatusOnInfoPage(driver);

			System.out.println("status changed");

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "crm")
	@TestCase(createdOn = "2018-05-07", updatedOn = "2018-05-07", testCaseDescription = "CRM > Lead Summary > Add Lead > log a task of upper link on lead info page", testCaseId = "TC_28_Add_Lead and log a task from Upper link")
	public void leadInfopage4() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			// This will open the CRM Module
			FCHomePageTest fcHomePageTest = fc.commonMethods().getModules();
			Leadgetset lead = new Leadgetset();
			CRMModule crm = new CRMModule();
			// Go to Lead in CRM

			crm.crm_common().openCRMLeadsPage(fcHomePageTest, driver).clickAddLead(driver);

			lead = crm.crm_common().getDefaultDataForLead(lead);

			Addleadpage addlead = new Addleadpage();
			addlead.fillLeadDetails(driver, lead);
			addlead.save(driver);

			CRM_leadInfoPageOptions leadinfo = new CRM_leadInfoPageOptions();

			leadinfo.logtaskOnInfoPage(driver);
			CRM_AddTaskgetsetTest fill = new CRM_AddTaskgetsetTest();
			CRM_AddTAskDetailsFill taskfill = new CRM_AddTAskDetailsFill();
			fill = crm.crm_common().defualtTaskdata(fill);

			fc.utobj().switchFrameById(driver, "cboxIframe");
			taskfill.detailsoftask(driver, fill);
			taskfill.clickclosebtn(driver);

			leadinfo.logtaskOnInfoPage(driver);

			fc.utobj().switchFrameById(driver, "cboxIframe");
			taskfill.detailsoftask(driver, fill);
			taskfill.clicksavebtn(driver);

			leadinfo.logtaskOnInfoPage(driver);

			fc.commonMethods().switch_cboxIframe_frameId(driver);
			taskfill.detailsoftask(driver, fill);
			taskfill.clickCreateAnother(driver);
			Thread.sleep(2000);
			taskfill.detailsoftask(driver, fill);
			taskfill.clicksavebtn(driver);
			System.out.println("log a task done by Upper link");

			driver.navigate().refresh();
			Thread.sleep(2000);

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "crm")
	@TestCase(createdOn = "2018-05-07", updatedOn = "2018-05-07", testCaseDescription = "CRM > Lead Summary > Add Lead > log a task of Bottom link on lead info page", testCaseId = "TC_28_Add_Lead and log a task from bottom link")
	public void leadInfopage5() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			// This will open the CRM Module
			FCHomePageTest fcHomePageTest = fc.commonMethods().getModules();
			Leadgetset lead = new Leadgetset();
			CRMModule crm = new CRMModule();
			// Go to Lead in CRM

			crm.crm_common().openCRMLeadsPage(fcHomePageTest, driver).clickAddLead(driver);

			lead = crm.crm_common().getDefaultDataForLead(lead);

			Addleadpage addlead = new Addleadpage();
			addlead.fillLeadDetails(driver, lead);
			addlead.save(driver);

			CRM_leadInfoPageOptions leadinfo = new CRM_leadInfoPageOptions();

			leadinfo.logtaskbottomOnInfoPage(driver);
			fc.commonMethods().switch_cboxIframe_frameId(driver);

			CRM_AddTaskgetsetTest fill = new CRM_AddTaskgetsetTest();
			CRM_AddTAskDetailsFill taskfill = new CRM_AddTAskDetailsFill();
			fill = crm.crm_common().defualtTaskdata(fill);
			taskfill.clickclosebtn(driver);
			Thread.sleep(1000);
			leadinfo.logtaskbottomOnInfoPage(driver);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			taskfill.detailsoftask(driver, fill);
			Thread.sleep(500);
			taskfill.clicksavebtn(driver);

			Thread.sleep(1500);

			leadinfo.logtaskbottomOnInfoPage(driver);

			fc.commonMethods().switch_cboxIframe_frameId(driver);
			taskfill.detailsoftask(driver, fill);
			taskfill.clickCreateAnother(driver);
			Thread.sleep(2000);
			taskfill.detailsoftask(driver, fill);
			taskfill.clicksavebtn(driver);
			Thread.sleep(1000);
			System.out.println("Log a task bottom button done");

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	

	@Test(groups = "crm")
	@TestCase(createdOn = "2018-05-07", updatedOn = "2018-05-07", testCaseDescription = "CRM > Lead Summary > Add Lead > Assign link on lead info page", testCaseId = "TC_28_Add_Lead and Owner Change by Assign link")
	public void leadInfopage6() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			// This will open the CRM Module
			FCHomePageTest fcHomePageTest = fc.commonMethods().getModules();
			Leadgetset lead = new Leadgetset();
			CRMModule crm = new CRMModule();
			// Go to Lead in CRM

			crm.crm_common().openCRMLeadsPage(fcHomePageTest, driver).clickAddLead(driver);

			lead = crm.crm_common().getDefaultDataForLead(lead);

			Addleadpage addlead = new Addleadpage();
			addlead.fillLeadDetails(driver, lead);
			addlead.save(driver);

			CRM_leadInfoPageOptions leadinfo = new CRM_leadInfoPageOptions();

			leadinfo.assignOnInfoPage(driver);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			WebElement close = driver.findElement(By.xpath(".//*[@value = 'Close']"));
			close.click();

			leadinfo.assignOnInfoPage(driver);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			WebElement assign = driver.findElement(By.name("contactOwnerID2"));
			assign.sendKeys("vipin corporate user");
			WebElement ass = driver.findElement(By.xpath(".//*[@value = 'Assign']"));
			ass.click();
			Thread.sleep(5000);
			WebElement read = driver.findElement(By.className("TextLbl"));
			String text = read.getText();
			System.out.println(text);

			if (text.equalsIgnoreCase("Your request has been processed successfully.")) {
				WebElement inn = driver.findElement(By.xpath(".//*[@value = 'Close']"));
				inn.click();
				System.out.println("Assign link done");
			} else {
				System.out.println("Assign link done not done");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "crm")
	@TestCase(createdOn = "2018-05-07", updatedOn = "2018-05-07", testCaseDescription = "CRM > Lead Summary > Add Lead > click on Owner change button and check remark", testCaseId = "TC-13002_Owner change and verify remark for leads")
	public void leadInfopage7() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			
			
			fc.utobj().printTestStep("Navigate To Admin Users Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			String emailId = "crmautomation@staffex.com";
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUser = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporateUser.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);
			String userName = corpUser.getFirstName()+" "+corpUser.getLastName();
			System.out.println(userName);
			
			FCHomePageTest fcHomePageTest = fc.commonMethods().getModules();
			CRMModule crm = new CRMModule();
			crm.crm_common().openCRMLeadsPage(fcHomePageTest, driver).clickAddLead(driver);
			
			Leadgetset lead = new Leadgetset();
			lead = crm.crm_common().getDefaultDataForLead(lead);
			Addleadpage addlead = new Addleadpage();
			String owner =lead.getOwner();
			System.out.println(owner);
			addlead.fillLeadDetails(driver, lead);
			addlead.save(driver);
			 
			CRM_leadInfoPageOptions leadinfo = new CRM_leadInfoPageOptions();
			CRM_LeadinfopageUI mod = new CRM_LeadinfopageUI(driver);
			leadinfo.ownerChangeOnInfoPage(driver);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
		
			fc.utobj().selectDropDown(driver, mod.Assignpage_Onwerfield, userName);
			fc.utobj().clickElement(driver, mod.Assignpage_Assignbutton );
			fc.utobj().clickElement(driver, mod.Assignpage_closebtn);
			
			fc.utobj().clickElement(driver, mod.DetailedHistory);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			
			fc.utobj().clickElement(driver, mod.DetailedHistory_Ownerchange);
			String Oldowner = driver.findElement(By.xpath(".//div[@id = 'div5']//tr[2]/td[2]")).getText();
			String NewOwner = driver .findElement(By.xpath(".//div[@id = 'div5']//tr[2]/td[3]")).getText();
			System.out.println(Oldowner +" " +NewOwner   );
			if(Oldowner.equalsIgnoreCase(owner)&& NewOwner.equalsIgnoreCase(userName)) {
				System.out.println("Owner changed");
			}
			
			fc.utobj().clickElement(driver, mod.Assignpage_closebtn);
			
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "crm")
	@TestCase(createdOn = "2018-05-07", updatedOn = "2018-05-07", testCaseDescription = "CRM > Lead Summary > Add Lead > Send Email", testCaseId = "TC_28_Send Email against lead")
	public void leadInfopage8() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			// This will open the CRM Module
			FCHomePageTest fcHomePageTest = fc.commonMethods().getModules();
			Leadgetset lead = new Leadgetset();
			CRMModule crm = new CRMModule();
			// Go to Lead in CRM

			crm.crm_common().openCRMLeadsPage(fcHomePageTest, driver).clickAddLead(driver);

			lead = crm.crm_common().getDefaultDataForLead(lead);

			Addleadpage addlead = new Addleadpage();
			addlead.fillLeadDetails(driver, lead);
			addlead.save(driver);

			CRM_leadInfoPageOptions leadinfo = new CRM_leadInfoPageOptions();

			leadinfo.SendEmailOnInfoPage(driver);
			CRM_SendEmailGetSet Semail = new CRM_SendEmailGetSet();
			CRM_SendEmailFillTest emailfill = new CRM_SendEmailFillTest();
			Semail = crm.crm_common().defualtemailfill(Semail);
			emailfill.cancelbtn(driver);

			Thread.sleep(2000);
			leadinfo.SendEmailOnInfoPage(driver);
			emailfill.filldetails(driver, Semail);
			fc.utobj().switchFrameToDefault(driver);
			emailfill.resetbtn(driver);
			emailfill.filldetails(driver, Semail);
			fc.utobj().switchFrameToDefault(driver);
			emailfill.savebtn(driver);
           System.out.println("Send Email Done");
           
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "crm")
	@TestCase(createdOn = "2018-05-07", updatedOn = "2018-05-07", testCaseDescription = "CRM > Lead Summary > Add Lead > Lead convert > Change lead name > change status > contact Type > add company name as account name > add opportunity and verify all ", testCaseId = "TC_28_Convert lead and perform all action")
	public void leadInfopage9() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			FCHomePageTest fcHomePageTest = fc.commonMethods().getModules();
			CRMModule crm = new CRMModule();

			crm.crm_common().CRMGroupsLnk(driver);
			CRM_CreateGroupPage Group = new CRM_CreateGroupPage();
			CRM_CreateGroupGetSet ginfo = new CRM_CreateGroupGetSet();
			ginfo = crm.crm_common().DefualtGroupinfo(ginfo);
			
			Group.Create_groupbtn(driver);
			ginfo.setGroupType("Contact");
			Group.fillGroup(driver, ginfo);
			
			Group.savebtn(driver);
			fc.utobj().printTestStep("Group Created");
			System.out.println(ginfo.getGroupName());
			String groupName = ginfo.getGroupName();
			crm.crm_common().AdminCRMStatus_change(driver, "selenium", "Contacts");
			fc.utobj().printTestStep("Status Created");
			crm.crm_common().openCRMLeadsPage(fcHomePageTest, driver).clickAddLead(driver);
			Leadgetset lead = new Leadgetset();
			lead = crm.crm_common().getDefaultDataForLead(lead);
			Addleadpage addlead = new Addleadpage();
			addlead.fillLeadDetails(driver, lead);
			addlead.save(driver);
			fc.utobj().printTestStep("Lead Created");
			String companyName = lead.getCompnayname();
			Thread.sleep(2000);
			
			String Fname = driver.findElement(By.xpath(".//td[contains(text(),'First Name')]/ancestor::tr[1]/td[2]"))
					.getText();
			String Lname = driver.findElement(By.xpath(".//td[contains(text(),'Last Name')]/ancestor::tr[1]/td[4]"))
					.getText();
			System.out.println(Fname + " " + Lname);
			
			CRM_leadInfoPageOptions leadinfo = new CRM_leadInfoPageOptions();
			leadinfo.convertOnInfoPage(driver);
			lead = crm.crm_common().getDefaultDataForLead(lead);
			ConvertLeadPageUI con = new ConvertLeadPageUI(driver);
			fc.utobj().sendKeys(driver, con.FirstName, lead.getFirstname());
			fc.utobj().sendKeys(driver, con.LastName, lead.getLastname());
			fc.utobj().printTestStep("change lead name");
			String new_contactName = lead.getFirstname()+" "+lead.getLastname();
			System.out.println(new_contactName);
			fc.utobj().selectDropDown(driver, con.contactType, "Customers");
			fc.utobj().printTestStep("change contact type");
			fc.utobj().selectDropDown(driver, con.Contactstatus, "selenium");
			fc.utobj().printTestStep("Status change");
			addlead.Groupoption_onAddlead(driver, groupName);
			fc.utobj().printTestStep("Group selected");
			fc.utobj().clickElement(driver, con.opportunityCreate);
			CRM_OpportunityGetSet opp = new CRM_OpportunityGetSet();
			opp= crm.crm_common().Defualtopportunity(opp);
			fc.utobj().sendKeys(driver, con.OpportunityName, opp.getOpportunityName());
			String opportunity = opp.getOpportunityName();
			fc.utobj().selectDropDown(driver, con.Stage, opp.getStage());
			fc.utobj().sendKeys(driver, con.SalesAmaount, opp.getSales_Amount());
			fc.utobj().selectDropDown(driver, con.OpportunitySource, opp.getOpportunity_Source());
			fc.utobj().printTestStep("opportunity filled");
			
			ConvertLeadpageTest change = new ConvertLeadpageTest();
			change.converbtn(driver);
			Thread.sleep(2000);
			String succ = driver.findElement(By.xpath("//div[@class='textGreen vp']")).getText();
			

			String companyName_convert = driver.findElement(By.xpath(".//td/a[contains(text(),'"+companyName+"')]")).getText();
			String contact_convert = driver.findElement(By.xpath(".//td/a[contains(text(),'"+new_contactName+"')]")).getText();
			String opportunity_convert = driver.findElement(By.xpath(".//td/a[contains(text(),'"+opportunity+"')]")).getText();
			if(companyName_convert.equalsIgnoreCase(companyName)&& contact_convert.equalsIgnoreCase(new_contactName) && opportunity_convert.equalsIgnoreCase(opportunity) && succ.equalsIgnoreCase(
					"Lead" + " \"" + Fname + " " + Lname + "\"" + " has been successfully converted.")) {
				System.out.println("Lead convert properly ");
			}else {
				System.out.println("not converted");
			}

		
			change.converbtn(driver);
			fc.utobj().printBugStatus("convert lead done");
			
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	
	@Test(groups = "crm")
	@TestCase(createdOn = "2018-05-07", updatedOn = "2018-05-07", testCaseDescription = "CRM > Lead Summary > Add Lead > Add campaign and associate with lead > verify campagin associate or not and click on Stop campagin button ", testCaseId = "TC_28_Verify campaign and Stop campaign against lead")
	public void leadInfopage10() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		
		
		try {
			driver = fc.loginpage().login(driver);

			FCHomePageTest fcHomePageTest = fc.commonMethods().getModules();
			CRMModule crm = new CRMModule();
			crm.crm_common().openCRMCampaignCenterPage(fcHomePageTest, driver);
			CRM_CampaginCenterPagelink add = new CRM_CampaginCenterPagelink();
			add.CreateCampaign(driver);
			CRM_CampaignAddpage addfill = new CRM_CampaignAddpage();
			CRM_CampaignAddGetSet camAdd = new CRM_CampaignAddGetSet();
			camAdd = crm.crm_common().defualtCampaignfill(camAdd);
			addfill.filldetails(driver, camAdd);
			addfill.Cancelbtnclick(driver);
			Thread.sleep(1000);
			add.CreateCampaign(driver);
			addfill.filldetails(driver, camAdd);
			addfill.clickStartbtn(driver);
			Thread.sleep(500);
			
			CRM_SelectandCustomizeTemplatepage custom = new CRM_SelectandCustomizeTemplatepage();
			custom.EditCampaignName(driver);
			addfill.filldetails(driver, camAdd);
			addfill.Savebtnclick(driver);
			Thread.sleep(500);
			addfill.closebtnclick(driver);
			
			
			custom.firstTemplateclick(driver);
			Thread.sleep(5000);
			
			
			CRM_TemplateInfoPageGetSet teminfo = new CRM_TemplateInfoPageGetSet();
			CRM_TemplateInfoPage info = new CRM_TemplateInfoPage();
			teminfo = crm.crm_common().defualtTemplateinfofill(teminfo);
			
			info.filldetails(driver, teminfo);
			info.savebtn(driver);
		
			Thread.sleep(5000);
			info.saveAndcontinue(driver);
			Thread.sleep(500);
			
			CRM_CampaignSelectRecipientpage recipient = new CRM_CampaignSelectRecipientpage();
			CRM_CampaignSelectRecipientUI recipientUI = new CRM_CampaignSelectRecipientUI(driver);
			recipient.AssociateLater(driver);
			Thread.sleep(500);
			
			CRM_CampaignFinalandLunchpage lunch = new CRM_CampaignFinalandLunchpage();
			CRM_CampaignFinalandLunchUI lunchUI = new CRM_CampaignFinalandLunchUI(driver);
			fc.utobj().selectDropDown(driver, lunchUI.TimeZone, "GMT +05:30 India");
		    String	CampaignName = lunchUI.campaignName.getText();
			System.out.println(CampaignName);
			lunch.clickFinalUpperbtn(driver);
			String confirm = lunchUI.confirmpagetext.getText();
			System.out.println(confirm);
			
			
			if(confirm.equalsIgnoreCase(CampaignName+" "+"has been scheduled successfully"))
			{
				System.out.println("Campaign Add Successfully ");
			}else {
				System.out.println("campaign not added");
			}
			
			crm.crm_common().openCRMLeadsPage(fcHomePageTest, driver).clickAddLead(driver);
			Leadgetset lead = new Leadgetset();
			lead = crm.crm_common().getDefaultDataForLead(lead);
			Addleadpage addlead = new Addleadpage();
			addlead.fillLeadDetails(driver, lead);
			addlead.save(driver);
			 CRM_leadInfoPageOptions leadinfo = new CRM_leadInfoPageOptions();
			 leadinfo.AssociatecampOnInfoPage(driver);
			 fc.utobj().sleep(1000);
			 fc.utobj().clickElement(driver, ".//button[@id='backConfirmation' or contains(text(),'Back')]");
			 fc.utobj().sleep(1000);
			 
			 leadinfo.AssociatecampOnInfoPage(driver);
			 fc.utobj().sleep(1000);
			fc.utobj().clickElement(driver, recipientUI.getXpathOfCampaignAction(CampaignName));
			 fc.utobj().sleep(1000);
			 fc.utobj().clickElement(driver, ".//button[@id='backConfirmation' or contains(text(),'Back')]");
			 
			 fc.utobj().clickElement(driver, ".//a[.='"+CampaignName+"']");
			 
			 
			 fc.utobj().clickElement(driver, ".//*[contains(text(),'Back')]");
			 
			 
			 fc.utobj().clickElement(driver, recipientUI.getXpathOfCampaignAction(CampaignName));
			fc.utobj().clickElement(driver, recipientUI.confirmOfAssociateWithCampagin);
	
			String camName = driver.findElement(By.xpath(".//td[contains(text(),'"+CampaignName+"')]")).getText();
			if(camName.equalsIgnoreCase(CampaignName)) {
				System.out.println("campaign present on lead info page");
				WebElement stopcam = driver.findElement(By.xpath("//a[contains(text(),'Stop Campaign')]"));
				if(stopcam.isDisplayed()) {
					System.out.println("Stop campaign link display");
					fc.utobj().printBugStatus("click on stop campagin link");
					fc.utobj().clickElement(driver, stopcam);
					fc.utobj().acceptAlertBox(driver);
					fc.commonMethods().switch_cboxIframe_frameId(driver);
					
					fc.utobj().clickElement(driver, ".//input[@value='Close']");
				}
			}else {
				System.out.println("campaign not present on lead info page");
			}
			
			
			
			
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}
	
	
	@Test(groups = "crm")
	@TestCase(createdOn = "2018-05-07", updatedOn = "2018-05-07", testCaseDescription = "CRM > Lead Summary > Add Lead > Add Group from lead info page", testCaseId = "TC_28_Add Group from lead info page")
	public void leadInfopage11() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			FCHomePageTest fcHomePageTest = fc.commonMethods().getModules();
			Leadgetset lead = new Leadgetset();
			CRMModule crm = new CRMModule();
			// Go to Lead in CRM

			crm.crm_common().openCRMLeadsPage(fcHomePageTest, driver).clickAddLead(driver);

			lead = crm.crm_common().getDefaultDataForLead(lead);

			Addleadpage addlead = new Addleadpage();
			addlead.fillLeadDetails(driver, lead);
			addlead.save(driver);

			CRM_leadInfoPageOptions leadinfo = new CRM_leadInfoPageOptions();
			leadinfo.addToGroupOnInfoPage(driver);
			leadinfo.addToNewGroupbtnOnIframe(driver);
			CRM_CreateGroupPage Group = new CRM_CreateGroupPage();
			CRM_CreateGroupGetSet ginfo = new CRM_CreateGroupGetSet();
			ginfo = crm.crm_common().DefualtGroupinfo(ginfo);
			Group.fillGroup(driver, ginfo);
			Group.associatebtn_AddGroup(driver);

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}
	
	@Test(groups = "crm")
	@TestCase(createdOn = "2018-05-07", updatedOn = "2018-05-07", testCaseDescription = "CRM > Lead Summary > Add Lead > Save and Add Another Lead ", testCaseId = "TC_28_save and Add Another lead")
	public void leadInfopage12() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		
		
		try {
			driver = fc.loginpage().login(driver);

			FCHomePageTest fcHomePageTest = fc.commonMethods().getModules();
			CRMModule crm = new CRMModule();
			
			
			crm.crm_common().openCRMLeadsPage(fcHomePageTest, driver).clickAddLead(driver);
			Leadgetset lead = new Leadgetset();
			lead = crm.crm_common().getDefaultDataForLead(lead);
			Addleadpage addlead = new Addleadpage();
			addlead.fillLeadDetails(driver, lead);
			addlead.saveAndAnother(driver);
			addlead.fillLeadDetails(driver, lead);
			addlead.save(driver);

			   fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
			    
			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
			}
		}

	
	@Test(groups = "crm")
	@TestCase(createdOn = "2018-05-07", updatedOn = "2018-05-07", testCaseDescription = "CRM > Lead Summary > Add Lead > save and convert lead ", testCaseId = "TC_28_Save and Convert Lead")
	public void leadInfopage13() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		
		
		try {
			driver = fc.loginpage().login(driver);

			FCHomePageTest fcHomePageTest = fc.commonMethods().getModules();
			CRMModule crm = new CRMModule();
			
			crm.crm_common().openCRMLeadsPage(fcHomePageTest, driver).clickAddLead(driver);
			Leadgetset lead = new Leadgetset();
			lead = crm.crm_common().getDefaultDataForLead(lead);
			Addleadpage addlead = new Addleadpage();
			addlead.fillLeadDetails(driver, lead);
			
			addlead.saveconvert(driver);
			
			ConvertLeadpageTest change = new ConvertLeadpageTest();
			change.converbtn(driver);
			 
			String succ = driver.findElement(By.xpath("//div[@class='textGreen vp']")).getText();
			System.out.println(succ);
	
	
			 fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
			    
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}
	
	
	@Test(groups = "crm")
	@TestCase(createdOn = "2018-05-07", updatedOn = "2018-05-07", testCaseDescription = "CRM > Lead Summary > Add Lead > Owner Change of lead from more Action button ", testCaseId = "TC_28_Owner change from More Action Button for Lead")
	public void leadInfopage14() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		
		
		try {
			driver = fc.loginpage().login(driver);

			FCHomePageTest fcHomePageTest = fc.commonMethods().getModules();
			CRMModule crm = new CRMModule();
	
			crm.crm_common().openCRMLeadsPage(fcHomePageTest, driver).clickAddLead(driver);
			Leadgetset lead = new Leadgetset();
			lead = crm.crm_common().getDefaultDataForLead(lead);
			Addleadpage addlead = new Addleadpage();
			addlead.fillLeadDetails(driver, lead);
			addlead.save(driver);
			Thread.sleep(2000);
			
			
			CRM_LeadinfopageUI ui = new CRM_LeadinfopageUI(driver);
			Actions action = new Actions(driver);
			fc.utobj().printTestStep("click Owner change button in more action menu");
			action.moveToElement(ui.MoreActionMenu).build().perform();
			Thread.sleep(500);
			action.click(ui.MoreActionMenu_changeOwner).build().perform();
			
		
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			WebElement assign = driver.findElement(By.name("contactOwnerID2"));
			assign.sendKeys("vipin corporate user");
			WebElement ass = driver.findElement(By.xpath(".//*[@value = 'Assign']"));
			ass.click();
			Thread.sleep(5000);
			WebElement read = driver.findElement(By.className("TextLbl"));
			String text = read.getText();
			

			if (text.equalsIgnoreCase("Your request has been processed successfully.")) {
				WebElement inn = driver.findElement(By.xpath(".//*[@value = 'Close']"));
				inn.click();
				System.out.println("Assign link done");
			} else {
				System.out.println("Assign link done not done");
			}
			Thread.sleep(1000);
	
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		    
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}
	
	@Test(groups = "crm")
	@TestCase(createdOn = "2018-05-07", updatedOn = "2018-05-07", testCaseDescription = "CRM > Lead Summary > Add Lead > Delete lead and restore it again ", testCaseId = "TC_28_Delete_Leadand Restore it Again")
	public void leadInfopage15() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			FCHomePageTest fcHomePageTest = fc.commonMethods().getModules();
			CRMModule crm = new CRMModule();

			crm.crm_common().openCRMLeadsPage(fcHomePageTest, driver).clickAddLead(driver);
			Leadgetset lead = new Leadgetset();
			lead = crm.crm_common().getDefaultDataForLead(lead);
			Addleadpage addlead = new Addleadpage();
			addlead.fillLeadDetails(driver, lead);
			addlead.save(driver);
			Thread.sleep(2000);
			String Fname = driver.findElement(By.xpath(".//td[contains(text(),'First Name')]/ancestor::tr[1]/td[2]"))
					.getText();
			String Lname = driver.findElement(By.xpath(".//td[contains(text(),'Last Name')]/ancestor::tr[1]/td[4]"))
					.getText();
			System.out.println(Fname + " " + Lname);

			CRM_LeadinfopageUI ui = new CRM_LeadinfopageUI(driver);
			Actions action = new Actions(driver);

			action.moveToElement(ui.MoreActionMenu).build().perform();
			Thread.sleep(100);
			action.click(ui.MoreActionMenu_DeleteLead).build().perform();
			fc.utobj().acceptAlertBox(driver);
			fc.utobj().printTestStep("click on delete lead option in more action menu");
			Thread.sleep(1000);
			
			fc.utobj().refresh(driver);
			crm.crm_common().search(driver, Fname+" "+Lname);
			if(fc.utobj().assertPageSource(driver, Fname+" "+Lname)) {
				System.out.println("lead not deleted");
			}else {
				System.out.println("Lead not found on lead summary page");
			}
	
			crm.crm_common().AdminPage(driver);
			/*WebElement admin = driver.findElement(By.xpath("//span[@class='selecter-selected bText12']//a[@href='javascript:void(0);']"));
			fc.utobj().clickElement(driver, admin);
			Thread.sleep(500);
			WebElement adminbtn = driver.findElement(By.xpath("//a[@class='bText12'][contains(text(),'Admin')]"));
			fc.utobj().clickElement(driver, adminbtn);
			Thread.sleep(1000);*/
			WebElement deletelog = driver.findElement(By.xpath(".//*[contains(text(),'Deleted Logs')]"));
			fc.utobj().clickElement(driver, deletelog);
			fc.utobj().printTestStep("click on deleted log in admin");
			Thread.sleep(1000);
			fc.utobj().showAll(driver);
			

			Thread.sleep(5000);
			fc.utobj().switchFrameById(driver, "reportiframe");
			if (fc.utobj().assertPageSource(driver, Fname+" "+Lname)) {
				System.out.println("lead found in Deleted logs");
				fc.utobj().printTestStep("lead found in Deleted logs");
				fc.utobj().actionImgOption(driver, Fname+" "+Lname, "Restore");
				fc.utobj().acceptAlertBox(driver);
				fc.utobj().printTestStep("click on restored button");
			} else {
				System.out.println("Lead not present in the deleted logs");
			}
			driver.navigate().refresh();
			Thread.sleep(2000);
			fc.utobj().switchFrameById(driver, "reportiframe");
			if (fc.utobj().assertPageSource(driver, Fname+" "+Lname)) {
				System.out.println("Lead not restored");
				
			} else {
				System.out.println("Lead Restored");
				fc.utobj().printTestStep("Lead Restored");
			}
			

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	
	@Test(groups = {"crm"})
	@TestCase(createdOn = "2018-05-07", updatedOn = "2018-05-07", testCaseDescription = "CRM > Lead Summary > Add Lead > Associate campaign with lead from More Action menu ", testCaseId = "TC_28_Associate Campaign against Lead from More action button")
	public void leadInfopage16() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			
			FCHomePageTest fcHomePageTest = fc.commonMethods().getModules();
			CRMModule crm = new CRMModule();
			crm.crm_common().openCRMCampaignCenterPage(fcHomePageTest, driver);
			CRM_CampaginCenterPagelink add = new CRM_CampaginCenterPagelink();
			add.CreateCampaign(driver);
			CRM_CampaignAddpage addfill = new CRM_CampaignAddpage();
			CRM_CampaignAddGetSet camAdd = new CRM_CampaignAddGetSet();
			camAdd = crm.crm_common().defualtCampaignfill(camAdd);
			addfill.filldetails(driver, camAdd);
			addfill.Cancelbtnclick(driver);
			Thread.sleep(1000);
			add.CreateCampaign(driver);
			addfill.filldetails(driver, camAdd);
			addfill.clickStartbtn(driver);
			Thread.sleep(500);
			
			CRM_SelectandCustomizeTemplatepage custom = new CRM_SelectandCustomizeTemplatepage();
			custom.EditCampaignName(driver);
			addfill.filldetails(driver, camAdd);
			addfill.Savebtnclick(driver);
			Thread.sleep(500);
			addfill.closebtnclick(driver);
			
			
			custom.firstTemplateclick(driver);
			Thread.sleep(5000);
			
			
			CRM_TemplateInfoPageGetSet teminfo = new CRM_TemplateInfoPageGetSet();
			CRM_TemplateInfoPage info = new CRM_TemplateInfoPage();
			teminfo = crm.crm_common().defualtTemplateinfofill(teminfo);
			
			info.filldetails(driver, teminfo);
			info.savebtn(driver);
			Thread.sleep(5000);
			info.saveAndcontinue(driver);
			Thread.sleep(500);
			
			CRM_CampaignSelectRecipientpage recipient = new CRM_CampaignSelectRecipientpage();
			CRM_CampaignSelectRecipientUI recipientUI = new CRM_CampaignSelectRecipientUI(driver);
			recipient.AssociateLater(driver);
			Thread.sleep(500);
			
			CRM_CampaignFinalandLunchpage lunch = new CRM_CampaignFinalandLunchpage();
			CRM_CampaignFinalandLunchUI lunchUI = new CRM_CampaignFinalandLunchUI(driver);
			fc.utobj().selectDropDown(driver, lunchUI.TimeZone, "GMT +05:30 India");
		    String	CampaignName = lunchUI.campaignName.getText();
			System.out.println(CampaignName);
			lunch.clickFinalUpperbtn(driver);
			String confirm = lunchUI.confirmpagetext.getText();
			System.out.println(confirm);
			
			
			if(confirm.equalsIgnoreCase(CampaignName+" "+"has been scheduled successfully"))
			{
				System.out.println("Campaign Add Successfully ");
			}else {
				System.out.println("campaign not added");
			}

			

			crm.crm_common().openCRMLeadsPage(fcHomePageTest, driver).clickAddLead(driver);
			Leadgetset lead = new Leadgetset();
			lead = crm.crm_common().getDefaultDataForLead(lead);
			Addleadpage addlead = new Addleadpage();
			addlead.fillLeadDetails(driver, lead);
			addlead.save(driver);
			Thread.sleep(2000);
			String Fname = driver.findElement(By.xpath(".//td[contains(text(),'First Name')]/ancestor::tr[1]/td[2]"))
					.getText();
			String Lname = driver.findElement(By.xpath(".//td[contains(text(),'Last Name')]/ancestor::tr[1]/td[4]"))
					.getText();
			System.out.println(Fname + " " + Lname);

			CRM_LeadinfopageUI ui = new CRM_LeadinfopageUI(driver);
			Actions action = new Actions(driver);

			action.moveToElement(ui.MoreActionMenu).build().perform();
			Thread.sleep(100);
			action.click(ui.MoreActionMenu_Associatewithcampaign).build().perform();
			//There is no accept present there so,commenting it
			//fc.utobj().acceptAlertBox(driver);
			Thread.sleep(1000);
			
			WebElement statusdriven_tab = driver.findElement(By.xpath("//span[contains(text(),'Status-Driven')]"));
			fc.utobj().printBugStatus("switch to status driven campaign tab");
			statusdriven_tab.click();

			WebElement promotion_tab = driver.findElement(By.xpath("//span[contains(text(),'Promotional')]"));
			fc.utobj().printBugStatus("switch to promotion campaign tab");
			promotion_tab.click();
			
			fc.utobj().clickElement(driver, recipientUI.getXpathOfCampaignAction(CampaignName));
			fc.utobj().clickElement(driver, recipientUI.confirmOfAssociateWithCampagin);
			

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}
	
	@Test(groups = "crm")
	@TestCase(createdOn = "2018-05-07", updatedOn = "2018-05-07", testCaseDescription = "CRM > Lead Summary > Add Lead > Add to group from More Action Menu", testCaseId = "TC_28_Add Group_Lead From More Action")
	public void leadInfopage17() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			
			FCHomePageTest fcHomePageTest = fc.commonMethods().getModules();
			CRMModule crm = new CRMModule();
			

			crm.crm_common().openCRMLeadsPage(fcHomePageTest, driver).clickAddLead(driver);
			Leadgetset lead = new Leadgetset();
			lead = crm.crm_common().getDefaultDataForLead(lead);
			Addleadpage addlead = new Addleadpage();
			addlead.fillLeadDetails(driver, lead);
			addlead.save(driver);
			Thread.sleep(2000);
			String Fname = driver.findElement(By.xpath(".//td[contains(text(),'First Name')]/ancestor::tr[1]/td[2]"))
					.getText();
			String Lname = driver.findElement(By.xpath(".//td[contains(text(),'Last Name')]/ancestor::tr[1]/td[4]"))
					.getText();
			System.out.println(Fname + " " + Lname);

			CRM_LeadinfopageUI ui = new CRM_LeadinfopageUI(driver);
			Actions action = new Actions(driver);

			action.moveToElement(ui.MoreActionMenu).build().perform();
			Thread.sleep(100);
			action.click(ui.MoreActionMenu_AddtoGroup).build().perform();
			
			Thread.sleep(1000);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().clickElement(driver, ".//span[contains(text(),'Add to New Group')]");
			
			CRM_CreateGroupPage Group = new CRM_CreateGroupPage();
			CRM_CreateGroupGetSet ginfo = new CRM_CreateGroupGetSet();
			ginfo = crm.crm_common().DefualtGroupinfo(ginfo);
			Group.fillGroup(driver, ginfo);
			Group.associatebtn_AddGroup(driver);
			Thread.sleep(1000);
			fc.utobj().printTestStep("Add to group from More Action menu done");
			
			
			
	//for click on group corresponding lead numbers		//a[contains(text(), 'test group')]/ancestor::tr[1]//a[contains(@onclick,'showLeads')]

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	
	@Test(groups = "crm")
	@TestCase(createdOn = "2018-05-07", updatedOn = "2018-05-07", testCaseDescription = "CRM > Lead Summary > Add Lead > Archived lead from More action menu button and restore it from archived lead page  ", testCaseId = "TC_28_Archived_Lead and restored It")
	public void leadInfopage18() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			
			FCHomePageTest fcHomePageTest = fc.commonMethods().getModules();
			CRMModule crm = new CRMModule();
			

			crm.crm_common().openCRMLeadsPage(fcHomePageTest, driver).clickAddLead(driver);
			Leadgetset lead = new Leadgetset();
			lead = crm.crm_common().getDefaultDataForLead(lead);
			Addleadpage addlead = new Addleadpage();
			addlead.fillLeadDetails(driver, lead);
			addlead.save(driver);
			Thread.sleep(2000);
			String Fname = driver.findElement(By.xpath(".//td[contains(text(),'First Name')]/ancestor::tr[1]/td[2]"))
					.getText();
			String Lname = driver.findElement(By.xpath(".//td[contains(text(),'Last Name')]/ancestor::tr[1]/td[4]"))
					.getText();
			System.out.println(Fname + " " + Lname);

			CRM_LeadinfopageUI ui = new CRM_LeadinfopageUI(driver);
			Actions action = new Actions(driver);
			fc.utobj().printTestStep("click on archived lead option in More Action button");
			action.moveToElement(ui.MoreActionMenu).build().perform();
			Thread.sleep(100);
			action.click(ui.MoreActionMenu_ArchivedLead).build().perform();
			Thread.sleep(1000);
			driver.navigate().refresh();
			
			WebElement search = driver.findElement(By.id("searchSystem"));
			fc.utobj().sendKeys(driver, search, Fname+" "+Lname);
			WebElement searchbtn = driver.findElement(By.id("systemSearchBtn"));
			searchbtn.click();
			
			if(fc.utobj().assertPageSource(driver, Fname+" "+Lname))
			{
				System.out.println("Lead no Archived");
			}else {
				System.out.println("Lead archived");
			}
			
			crm.crm_common().openCRMsearchpage(fcHomePageTest, driver);
			CRM_SearchPageLinks searchlink = new CRM_SearchPageLinks();
			searchlink.leadtab(driver);
			searchlink.ArchivedRadiobtn(driver);
			searchlink.submitbtn(driver);
			
			if(fc.utobj().assertPageSource(driver, Fname+" "+Lname))
			{
				System.out.println("Lead present on archived page");
			}else {
				System.out.println("Lead archived");
			}
			
			WebElement leadclick = driver.findElement(By.xpath("//a[contains(text(),'"+Fname+" "+Lname+"')]/ancestor::tr[1]/td//input[@type = 'checkbox']"));
			leadclick.click();
			WebElement move = driver.findElement(By.xpath(".//input[@value = 'Move to Active Leads(s)']"));
			move.click();
			
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			WebElement Ok = driver.findElement(By.xpath(".//*[@value = 'Ok']"));
			Ok.click();
			
			WebElement close = driver.findElement(By.xpath(".//*[@value = 'Close']"));
			close.click();
			
			driver.navigate().refresh();
			
			if(fc.utobj().assertPageSource(driver, Fname+" "+Lname)) {
				System.out.println("Lead not restore");
			}else {
				System.out.println("Lead restored");
			}
			
			fc.utobj().printTestStep("Archived lead done ");
	
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	
	@Test(groups = "crm")
	@TestCase(createdOn = "2018-05-07", updatedOn = "2018-05-07", testCaseDescription = "CRM > Lead Summary > Add Lead > on lead info page next and previous button", testCaseId = "TC_28_Add_Lead or Next and previous button")
	public void leadInfopage19() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			
			FCHomePageTest fcHomePageTest = fc.commonMethods().getModules();
			CRMModule crm = new CRMModule();
			

			crm.crm_common().openCRMLeadsPage(fcHomePageTest, driver).clickAddLead(driver);
			Leadgetset lead = new Leadgetset();
			lead = crm.crm_common().getDefaultDataForLead(lead);
			Addleadpage addlead = new Addleadpage();
			addlead.fillLeadDetails(driver, lead);
			addlead.save(driver);
			Thread.sleep(2000);
			driver.navigate().refresh();
			
			String Fname = driver.findElement(By.xpath(".//td[contains(text(),'First Name')]/ancestor::tr[1]/td[2]"))
					.getText();
			String Lname = driver.findElement(By.xpath(".//td[contains(text(),'Last Name')]/ancestor::tr[1]/td[4]"))
					.getText();
			System.out.println(Fname + " " + Lname);
			CRM_leadInfoPageOptions leadinfo = new CRM_leadInfoPageOptions();
			driver.navigate().refresh();
			
			
			//boolean next = driver.findElement(By.xpath("//a[@href='javascript:leadNextId()']")). ;
			if(fc.utobj().assertPageSource(driver, ".//a[.='Next']")) {
				 leadinfo.NextbtnOnInfoPage(driver); Thread.sleep(2000);
			}else {
				System.out.println("Next button not present on the lead info page");
			}
			 
			//boolean prev = driver.findElement(By.xpath("//a[@href='javascript:leadPrevId()']")).isDisplayed() ;
			 if(fc.utobj().assertPageSource(driver, ".//a[.='Prev']")) { leadinfo.PrevbtnOnInfoPage(driver);
			 
			 }else {
				 System.out.println("Previous button not present");
			 }
			 
				
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	
	@Test(groups = {"crm"})
	@TestCase(createdOn = "2018-05-07", updatedOn = "2018-05-07", testCaseDescription = "CRM > Lead Summary > Add Lead > On Email Tab send Email and reply ", testCaseId = "TC_28_Send Email From Email Tab and reply against Lead")
	public void leadInfopage20() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			
			FCHomePageTest fcHomePageTest = fc.commonMethods().getModules();
			CRMModule crm = new CRMModule();
			

			crm.crm_common().openCRMLeadsPage(fcHomePageTest, driver).clickAddLead(driver);
			Leadgetset lead = new Leadgetset();
			lead = crm.crm_common().getDefaultDataForLead(lead);
			Addleadpage addlead = new Addleadpage();
			addlead.fillLeadDetails(driver, lead);
			addlead.save(driver);
	        Thread.sleep(1000);
	        fc.utobj().refresh(driver);
	        
	        CRM_leadInfoPageOptions leadinfo = new CRM_leadInfoPageOptions();
	        leadinfo.Emails_TabOnInfoPage(driver);
	        
	        String Emailinbox = driver.findElement(By.id("sentMailDiv")).getText();
		       System.out.println(Emailinbox);
		       if(Emailinbox.equalsIgnoreCase("  No Emails Found.")) {
		    	   System.out.println("No Email found on email page");
		    	   fc.utobj().printTestStep("No Emails found ");
		       }
		       
	        leadinfo.Backbtn_Emails_Tab(driver);
	        Thread.sleep(1000);
	        leadinfo.Emails_TabOnInfoPage(driver);
	        leadinfo.SendEmail_Emails_Tab(driver);
	        
	        CRM_SendEmailGetSet Semail = new CRM_SendEmailGetSet();
			CRM_SendEmailFillTest emailfill = new CRM_SendEmailFillTest();
			Semail = crm.crm_common().defualtemailfill(Semail);
			emailfill.filldetails(driver, Semail);
			fc.utobj().switchFrameToDefault(driver);
			emailfill.savebtn(driver);
	        System.out.println(Semail.getSubject());
	        Thread.sleep(1000);
	        fc.utobj().refresh(driver);
	        CRM_LeadinfopageUI mod = new CRM_LeadinfopageUI(driver);
	        fc.utobj().sendKeys(driver, mod.Searchbar_Emails_tab, Semail.getSubject()); 
	        leadinfo.Searchbtn_Emails_Tab(driver);
	        Thread.sleep(1000);
	        
	        boolean result = fc.utobj().assertPageSource(driver, Semail.getSubject());
	        if(result == true) {
	        	System.out.println("send Email found on the Email page");
	        	leadinfo.Reply_Emails_Tab(driver);
	        	
	        }else {
	        	System.out.println("Email not found");
	        }
	        Thread.sleep(1000);
	       emailfill.CustomID_onEmail(driver, "customId@gmail.com");
	        
	        emailfill.filldetails(driver, Semail);
			fc.utobj().switchFrameToDefault(driver);
			emailfill.savebtn(driver);
			System.out.println("Reply done");
			
			
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = {"crm"})
	@TestCase(createdOn = "2018-05-07", updatedOn = "2018-05-07", testCaseDescription = "CRM > Lead Summary > fill > Add task > modfiy task and delete ", testCaseId = "TC_28_Add task modify, delete task against lead")
	public void leadInfopage21() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			FCHomePageTest fcHomePageTest = fc.commonMethods().getModules();
			CRMModule crm = new CRMModule();

			crm.crm_common().openCRMLeadsPage(fcHomePageTest, driver).clickAddLead(driver);
			Leadgetset lead = new Leadgetset();
			lead = crm.crm_common().getDefaultDataForLead(lead);
			Addleadpage addlead = new Addleadpage();
			addlead.fillLeadDetails(driver, lead);
			addlead.save(driver);
			Thread.sleep(2000);
			driver.navigate().refresh();

			CRM_leadInfoPageOptions leadinfo = new CRM_leadInfoPageOptions();
			CRM_AddTAskDetailsFill taskfill = new CRM_AddTAskDetailsFill();
			CRM_AddTaskgetsetTest fill = new CRM_AddTaskgetsetTest();

			fill = crm.crm_common().defualtTaskdata(fill);
			leadinfo.logtaskbottomOnInfoPage(driver);
			fc.utobj().sleep(1000);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().sleep(1000);
			taskfill.detailsoftask(driver, fill);
			taskfill.clicksavebtn(driver);
			Thread.sleep(1000);
			String subjectoftask = fill.getSubject();
			System.out.println(fill.getSubject());
			boolean taskname = fc.utobj().assertPageSource(driver, fill.getSubject());
			if (taskname == true) {
				System.out.println("Task present on the lead info page");
				crm.crm_common().clickOn_taskOption_onleadinfopage(driver, subjectoftask, "Modify");
				fc.commonMethods().switch_cboxIframe_frameId(driver);
				fill = crm.crm_common().defualtTaskdata(fill);
				taskfill.detailsoftask(driver, fill);
				taskfill.clicksavebtn(driver);
				
				System.out.println("task modify done");
			}else {
				System.out.println("Task not present on the Lead info page");
			}
			fc.utobj().refresh(driver);
			String newtasksubject = fill.getSubject();
			System.out.println(fill.getSubject());
			boolean New_taskname = fc.utobj().assertPageSource(driver, fill.getSubject());
			if (New_taskname == true) {
				System.out.println("Task present on the lead info page");
				crm.crm_common().clickOn_taskOption_onleadinfopage(driver, newtasksubject , "Delete");
				fc.utobj().acceptAlertBox(driver);
			}
			if (fc.utobj().assertPageSource(driver, fill.getSubject())) {
				System.out.println("task not deleted");
			} else {
				System.out.println("Task deleted");
			}
			
			
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}
		
	@Test(groups = "crm")
	@TestCase(createdOn = "2018-05-07", updatedOn = "2018-05-07", testCaseDescription = "CRM > Lead Summary > Add Lead > Status change > work in process > verify > again status change to complete > verify in active history page", testCaseId = "TC_28_Change task Status verify remark for Lead")
	public void leadInfopage22() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			FCHomePageTest fcHomePageTest = fc.commonMethods().getModules();
			CRMModule crm = new CRMModule();

			crm.crm_common().openCRMLeadsPage(fcHomePageTest, driver).clickAddLead(driver);
			Leadgetset lead = new Leadgetset();
			lead = crm.crm_common().getDefaultDataForLead(lead);
			Addleadpage addlead = new Addleadpage();
			addlead.fillLeadDetails(driver, lead);
			addlead.save(driver);
			Thread.sleep(2000);
			driver.navigate().refresh();

			CRM_leadInfoPageOptions leadinfo = new CRM_leadInfoPageOptions();
			CRM_AddTAskDetailsFill taskfill = new CRM_AddTAskDetailsFill();
			CRM_AddTaskgetsetTest fill = new CRM_AddTaskgetsetTest();
			fill = crm.crm_common().defualtTaskdata(fill);
			leadinfo.logtaskbottomOnInfoPage(driver);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			taskfill.detailsoftask(driver, fill);
			taskfill.clicksavebtn(driver);
			Thread.sleep(1000);

			String subjectoftask = fill.getSubject();
			
			driver.navigate().refresh();

			boolean taskname = fc.utobj().assertPageSource(driver, fill.getSubject());
			if (taskname == true) {
				System.out.println("Task present on the lead info page");
				
				crm.crm_common().clickOn_taskOption_onleadinfopage(driver, subjectoftask, "Process");
				fc.commonMethods().switch_cboxIframe_frameId(driver);
				WebElement close = driver.findElement(By.xpath(".//*[@value = 'Close']"));
				close.click();
				Thread.sleep(1000);
				
			}
			
			crm.crm_common().clickOn_taskOption_onleadinfopage(driver, subjectoftask, "Process");
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			CRMTasksPage taskUI = new CRMTasksPage(driver);
			fc.utobj().selectDropDown(driver, taskUI.statusTask, "Work In Progress");
			WebElement proces = driver.findElement(By.xpath(".//*[@value = 'Process']"));
			proces.click();
			
			String check_status = driver.findElement(By.xpath("//a[contains(text(),'"+subjectoftask+"')]/ancestor::td[1]/following-sibling::td[2]")).getText();
			
			if (check_status.equalsIgnoreCase("Work In Progress")) {
				System.out.println("task Status change through process button");
			} else {
				System.out.println("Task Status not change ");
			}

			crm.crm_common().clickOn_taskOption_onleadinfopage(driver, subjectoftask, "Process");
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().selectDropDown(driver, taskUI.statusTask, "Completed");
			WebElement proces1 = driver.findElement(By.xpath(".//*[@value = 'Process']"));
			proces1.click();
			
			String activehst = driver.findElement(By.id("ActivityHis")).getText();
			
			
			if (activehst.contains(subjectoftask))  {
				System.out.println("task found in activity history");
				String title = driver.findElement(By.xpath(".//span[@class = 'subText'][contains(text(),'"+subjectoftask+"')]/ancestor::a")).getAttribute("title");
				
				if(title.equalsIgnoreCase("Task (Completed)")){
					System.out.println("task completed");
				}
			
			}else {
				System.out.println("task not completed");
			}
			fc.utobj().refresh(driver);
			

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	
	@Test(groups = "crm")
	@TestCase(createdOn = "2018-05-07", updatedOn = "2018-05-07", testCaseDescription = "CRM > Lead Summary > Add Lead > add task > try to delete lead if task is associated ", testCaseId = "TC_28_try to delele lead if task is associated")
	public void leadInfopage23() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			FCHomePageTest fcHomePageTest = fc.commonMethods().getModules();
			CRMModule crm = new CRMModule();

			crm.crm_common().openCRMLeadsPage(fcHomePageTest, driver).clickAddLead(driver);
			Leadgetset lead = new Leadgetset();
			lead = crm.crm_common().getDefaultDataForLead(lead);
			Addleadpage addlead = new Addleadpage();
			addlead.fillLeadDetails(driver, lead);
			addlead.save(driver);
			Thread.sleep(2000);
			driver.navigate().refresh();

			CRM_leadInfoPageOptions leadinfo = new CRM_leadInfoPageOptions();
			CRM_AddTAskDetailsFill taskfill = new CRM_AddTAskDetailsFill();
			CRM_AddTaskgetsetTest fill = new CRM_AddTaskgetsetTest();
			fill = crm.crm_common().defualtTaskdata(fill);
			leadinfo.logtaskbottomOnInfoPage(driver);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			taskfill.detailsoftask(driver, fill);
			taskfill.clicksavebtn(driver);
			Thread.sleep(1000);

			CRM_LeadinfopageUI ui = new CRM_LeadinfopageUI(driver);
			Actions action = new Actions(driver);

			action.moveToElement(ui.MoreActionMenu).build().perform();
			Thread.sleep(100);
			action.click(ui.MoreActionMenu_DeleteLead).build().perform();


			String alertmsg = driver.switchTo().alert().getText();
			System.out.println(alertmsg);
			
			if(alertmsg.equalsIgnoreCase("Lead can not be deleted as a Task is associated with it."))
			{
				System.out.println("lead having a task so unable to delete it");
				fc.utobj().acceptAlertBox(driver);
			}if(alertmsg.equalsIgnoreCase("Are you sure you want to delete the lead?")){
				System.out.println("Lead can be deleted with task");
			}else {
				System.out.println("lead can not be deleted");
			}
	

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}
	
	@Test(groups = "crm")
	@TestCase(createdOn = "2018-05-07", updatedOn = "2018-05-07", testCaseDescription = "CRM > Lead Summary > Add Lead > Associate lead with already created group and verify ", testCaseId = "TC_28_Associate_Lead With Already created Group")
	public void leadInfopage24() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			FCHomePageTest fcHomePageTest = fc.commonMethods().getModules();
			CRMModule crm = new CRMModule();

			crm.crm_common().CRMGroupsLnk(driver);
			CRM_CreateGroupPage Group = new CRM_CreateGroupPage();
			CRM_CreateGroupGetSet ginfo = new CRM_CreateGroupGetSet();
			ginfo = crm.crm_common().DefualtGroupinfo(ginfo);
			
			Group.Create_groupbtn(driver);
			Group.fillGroup(driver, ginfo);
			Group.savebtn(driver);
			System.out.println(ginfo.getGroupName());
			String groupName = ginfo.getGroupName();
			
			crm.crm_common().openCRMLeadsPage(fcHomePageTest, driver).clickAddLead(driver);
			Leadgetset lead = new Leadgetset();
			lead = crm.crm_common().getDefaultDataForLead(lead);
			Addleadpage addlead = new Addleadpage();
			addlead.fillLeadDetails(driver, lead);
			addlead.save(driver);
			Thread.sleep(2000);
			
			CRM_leadInfoPageOptions leadinfo = new CRM_leadInfoPageOptions();
			leadinfo.addToGroupOnInfoPage(driver);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			if(fc.utobj().assertPageSource(driver, groupName)) {
				System.out.println("Group Name present on the Iframe");
				System.out.println("click against created group name");
				fc.utobj().clickElement(driver, ".//td[contains(text(),'"+groupName+"')]/ancestor::tr[1]/td[1]/input");
				fc.utobj().clickElement(driver, ".//input[@type = 'Submit']");
				String success = driver.findElement(By.xpath(".//td[contains(text(),'Lead(s) has been added to Group(s) successfully..')]")).getText();
				if(success.equalsIgnoreCase("Lead(s) has been added to Group(s) successfully..")) {
					fc.utobj().printTestStep("lead added with group ");
					fc.utobj().clickElement(driver, ".//input[@value = 'Close']");
				}
			}else {
				System.out.println("group name not found on the frame");
			}
			
			String group_associate_ornot = driver.findElement(By.xpath(".//td[contains(text(),'Group(s) Name : ')]/ancestor::tr[1]/td[2]")).getText();
			System.out.println(group_associate_ornot);
			if(group_associate_ornot.equalsIgnoreCase(groupName)) {
				System.out.println("Group Properly associated with lead");
			}else {
				System.out.println("Group name not fround on lead info page against group name tag");
			}
		
			fc.utobj().printTestStep("Group association done");
			
			
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	
	@Test(groups = {"crm"})
	@TestCase(createdOn = "2018-05-07", updatedOn = "2018-05-07", testCaseDescription = "CRM > Lead Summary > Add Lead > add group at the time of add lead and verify", testCaseId = "TC_28_Add_Group_at the time of add lead")
	public void leadInfopage25() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			FCHomePageTest fcHomePageTest = fc.commonMethods().getModules();
			CRMModule crm = new CRMModule();

			crm.crm_common().CRMGroupsLnk(driver);
			CRM_CreateGroupPage Group = new CRM_CreateGroupPage();
			CRM_CreateGroupGetSet ginfo = new CRM_CreateGroupGetSet();
			ginfo = crm.crm_common().DefualtGroupinfo(ginfo);
			
			Group.Create_groupbtn(driver);
			Group.fillGroup(driver, ginfo);
			Group.savebtn(driver);
			System.out.println(ginfo.getGroupName());
			String groupName = ginfo.getGroupName();
			
			crm.crm_common().openCRMLeadsPage(fcHomePageTest, driver).clickAddLead(driver);
			Leadgetset lead = new Leadgetset();
			lead = crm.crm_common().getDefaultDataForLead(lead);
			Addleadpage addlead = new Addleadpage();
			addlead.fillLeadDetails(driver, lead);
			addlead.Groupoption_onAddlead(driver, groupName);
			
			addlead.save(driver);
			Thread.sleep(2000);
			String group_associate_ornot = driver.findElement(By.xpath(".//td[contains(text(),'Group(s) Name : ')]/ancestor::tr[1]/td[2]")).getText();
			System.out.println(group_associate_ornot);
			if(group_associate_ornot.equalsIgnoreCase(groupName)) {
				System.out.println("Group Properly associated with lead");
			}else {
				System.out.println("Group name not found on lead info page against group name tag");
			}
			fc.utobj().printTestStep("Group association done");
	
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}
	
	
	@Test(groups = "crm")
	@TestCase(createdOn = "2018-05-07", updatedOn = "2018-05-07", testCaseDescription = "CRM > Lead Summary > Add Lead > convert lead > add new account if compnay name is blank ", testCaseId = "TC_28_Add new account_on convert Lead")
	public void leadInfopage26() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			FCHomePageTest fcHomePageTest = fc.commonMethods().getModules();
			CRMModule crm = new CRMModule();

			
			
			crm.crm_common().openCRMLeadsPage(fcHomePageTest, driver).clickAddLead(driver);
			Leadgetset lead = new Leadgetset();
			lead = crm.crm_common().getDefaultDataForLead(lead);
			lead.setCompnayname("");
			Addleadpage addlead = new Addleadpage();
			addlead.fillLeadDetails(driver, lead);
			addlead.save(driver);
			fc.utobj().printTestStep("lead added with blank company name");
		
			Thread.sleep(2000);
			
			String Fname = driver.findElement(By.xpath(".//td[contains(text(),'First Name')]/ancestor::tr[1]/td[2]"))
					.getText();
			String Lname = driver.findElement(By.xpath(".//td[contains(text(),'Last Name')]/ancestor::tr[1]/td[4]"))
					.getText();
			System.out.println(Fname + " " + Lname);
			
			CRM_leadInfoPageOptions leadinfo = new CRM_leadInfoPageOptions();
			leadinfo.convertOnInfoPage(driver);
			
			ConvertLeadPageUI con = new ConvertLeadPageUI(driver);
			
			String accountName = "testaccount" ;
			fc.utobj().sendKeys(driver, con.AccountName, accountName);
			
			ConvertLeadpageTest change = new ConvertLeadpageTest();
			change.converbtn(driver);
			Thread.sleep(2000);
			String succ = driver.findElement(By.xpath("//div[@class='textGreen vp']")).getText();
			

			String companyName_convert = driver.findElement(By.xpath(".//td/a[contains(text(),'"+accountName+"')]")).getText();
			
		
			if(companyName_convert.equalsIgnoreCase(accountName)&& succ.equalsIgnoreCase(
					"Lead" + " \"" + Fname + " " + Lname + "\"" + " has been successfully converted.")) {
				System.out.println("Lead convert properly ");
			}else {
				System.out.println("not converted");
			}

		
			change.converbtn(driver);
			fc.utobj().printBugStatus("convert lead done");
			
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}
	
	@Test(groups = {"crm"})
	@TestCase(createdOn = "2018-05-07", updatedOn = "2018-05-07", testCaseDescription = "CRM > Lead Summary > Add Lead > add existing account at the time of convert lead and change owner of newly created contact", testCaseId = "TC_28_Add_existing account while convert lead")
	public void leadInfopage27() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			fc.utobj().printTestStep("Navigate To Admin Users Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			String emailId = "crmautomation@staffex.com";
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUser = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporateUser.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);
			String userName = corpUser.getFirstName()+" "+corpUser.getLastName();
			System.out.println(userName);

			FCHomePageTest fcHomePageTest = fc.commonMethods().getModules();
			CRMModule crm = new CRMModule();
			CRMAccountsPageTest account = new CRMAccountsPageTest();
			String AccountName = fc.utobj().generateTestData("Selenium");
			account.addAcocunt(driver, "industry", AccountName, "All Network");
		
			crm.crm_common().openCRMLeadsPage(fcHomePageTest, driver).clickAddLead(driver);
			Leadgetset lead = new Leadgetset();
			lead = crm.crm_common().getDefaultDataForLead(lead);
			lead.setCompnayname("");
			Addleadpage addlead = new Addleadpage();
			addlead.fillLeadDetails(driver, lead);
			addlead.save(driver);
			fc.utobj().printTestStep("lead added with blank company name");
		
			Thread.sleep(2000);
			
			String Fname = driver.findElement(By.xpath(".//td[contains(text(),'First Name')]/ancestor::tr[1]/td[2]"))
					.getText();
			String Lname = driver.findElement(By.xpath(".//td[contains(text(),'Last Name')]/ancestor::tr[1]/td[4]"))
					.getText();
			System.out.println(Fname + " " + Lname);
			
			CRM_leadInfoPageOptions leadinfo = new CRM_leadInfoPageOptions();
			leadinfo.convertOnInfoPage(driver);
			
			ConvertLeadPageUI con = new ConvertLeadPageUI(driver);
			fc.utobj().clickElement(driver, con.ExistingAccount);
			fc.utobj().sendKeys(driver, con.Accountsearch, AccountName);
			fc.utobj().sleep(1000);
			fc.utobj().clickElement(driver, "//div[@class='divrow hlight']");
			fc.utobj().clickElement(driver, con.OtherUser);
			
			fc.utobj().selectDropDown(driver, con.AssignTo, userName);
			
		
			ConvertLeadpageTest change = new ConvertLeadpageTest();
			change.converbtn(driver);
			Thread.sleep(2000);
			String succ = driver.findElement(By.xpath("//div[@class='textGreen vp']")).getText();
			
			String companyName_convert = driver.findElement(By.xpath(".//td/a[contains(text(),'"+AccountName+"')]")).getText();
			
			
			if(companyName_convert.equalsIgnoreCase(AccountName)&& succ.equalsIgnoreCase(
					"Lead" + " \"" + Fname + " " + Lname + "\"" + " has been successfully converted.")) {
				System.out.println("Lead convert properly ");
			}else {
				System.out.println("not converted");
			}

		
			change.converbtn(driver);
			fc.utobj().printBugStatus("convert lead done");
			
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}
	
	@Test(groups = "crm")
	@TestCase(createdOn = "2018-05-07", updatedOn = "2018-05-07", testCaseDescription = "CRM > Lead Summary > Add Lead > convert lead and merge with existing contact of same name  ", testCaseId = "TC_402_Covert Lead with existing contact")
	public void leadInfopage28() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			

			FCHomePageTest fcHomePageTest = fc.commonMethods().getModules();
			CRMModule crm = new CRMModule();
			Leadgetset lead = new Leadgetset();
			lead = crm.crm_common().getDefaultDataForLead(lead);
			crm.crm_common().openCRMContactsPage(fcHomePageTest, driver);
			fc.utobj().clickElement(driver, ".//*[@id = 'add_new']");
			fc.utobj().clickElement(driver, "//a[@href='#'][contains(text(),'Contact')]");
			CRMContactsPage conUI = new CRMContactsPage(driver);
			fc.utobj().selectDropDown(driver,conUI.contactType , "Contacts");
			fc.utobj().sendKeys(driver, conUI.contactFirstName, lead.getFirstname());
			fc.utobj().sendKeys(driver, conUI.contactLastName, lead.getLastname());
			fc.utobj().sendKeys(driver, conUI.emailIds, lead.getEmail());
			fc.utobj().selectDropDown(driver, conUI.selectCorporateUserContact, lead.getOwner());
			fc.utobj().clickElement(driver, conUI.submitContactBtn);
			System.out.println("contact created");
			String contactName = lead.getFirstname()+" "+lead.getLastname();
			
			crm.crm_common().openCRMLeadsPage(fcHomePageTest, driver).clickAddLead(driver);
		
			lead.setCompnayname("");
			Addleadpage addlead = new Addleadpage();
			addlead.fillLeadDetails(driver, lead);
			addlead.save(driver);
			fc.utobj().printTestStep("lead added with blank company name");
		
			Thread.sleep(2000);
			
			String Fname = driver.findElement(By.xpath(".//td[contains(text(),'First Name')]/ancestor::tr[1]/td[2]"))
					.getText();
			String Lname = driver.findElement(By.xpath(".//td[contains(text(),'Last Name')]/ancestor::tr[1]/td[4]"))
					.getText();
			System.out.println(Fname + " " + Lname);
			
			CRM_leadInfoPageOptions leadinfo = new CRM_leadInfoPageOptions();
			leadinfo.convertOnInfoPage(driver);
			ConvertLeadPageUI con = new ConvertLeadPageUI(driver);
			fc.utobj().clickElement(driver, con.AssociateWithMatchingcontact);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			if(fc.utobj().assertPageSource(driver, contactName)) {
				System.out.println("contact name found ");
				fc.utobj().clickElement(driver, ".//a[contains(text(),'"+contactName+"')]");
			}
			fc.utobj().switchFrameToDefault(driver);
			
		
			ConvertLeadpageTest change = new ConvertLeadpageTest();
			change.converbtn(driver);
			
			
			
			Thread.sleep(2000);
			String succ = driver.findElement(By.xpath("//div[@class='textGreen vp']")).getText();
			String contact_convert = driver.findElement(By.xpath(".//a[contains(text(),'"+contactName+"')]")).getText();
		
			
			
			if(succ.equalsIgnoreCase(
					"Lead" + " \"" + Fname + " " + Lname + "\"" + " has been successfully converted.")
					&& contact_convert.equalsIgnoreCase(contactName) ) {
				System.out.println("Lead convert properly ");
			}else {
				System.out.println("not converted");
			}

		
			change.converbtn(driver);
			fc.utobj().printBugStatus("convert lead done");
			
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}
	
	
	
	@Test(groups = {"crm"})
	@TestCase(createdOn = "2018-05-07", updatedOn = "2018-05-07", testCaseDescription = "CRM > Lead Summary > Add Lead > send Email from Email Tab > click on email to check email details and reply from them", testCaseId = "TC_13142_click on email to check details")
	public void leadInfopage29() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			
			FCHomePageTest fcHomePageTest = fc.commonMethods().getModules();
			CRMModule crm = new CRMModule();
			

			crm.crm_common().openCRMLeadsPage(fcHomePageTest, driver).clickAddLead(driver);
			Leadgetset lead = new Leadgetset();
			lead = crm.crm_common().getDefaultDataForLead(lead);
			Addleadpage addlead = new Addleadpage();
			addlead.fillLeadDetails(driver, lead);
			addlead.save(driver);
	        Thread.sleep(1000);
	        fc.utobj().refresh(driver);
	        
	        CRM_leadInfoPageOptions leadinfo = new CRM_leadInfoPageOptions();
	        leadinfo.Emails_TabOnInfoPage(driver);
	        leadinfo.Backbtn_Emails_Tab(driver);
	        Thread.sleep(1000);
	        leadinfo.Emails_TabOnInfoPage(driver);
	        leadinfo.SendEmail_Emails_Tab(driver);
	        
	        CRM_SendEmailGetSet Semail = new CRM_SendEmailGetSet();
			CRM_SendEmailFillTest emailfill = new CRM_SendEmailFillTest();
			Semail = crm.crm_common().defualtemailfill(Semail);
			emailfill.filldetails(driver, Semail);
			fc.utobj().switchFrameToDefault(driver);
			emailfill.savebtn(driver);
	        System.out.println(Semail.getSubject());
	        String email_sub = Semail.getSubject();
	        Thread.sleep(1000);
	        fc.utobj().refresh(driver);
	        CRM_LeadinfopageUI mod = new CRM_LeadinfopageUI(driver);
	        fc.utobj().sendKeys(driver, mod.Searchbar_Emails_tab, email_sub); 
	        leadinfo.Searchbtn_Emails_Tab(driver);
	        Thread.sleep(1000);
	        
	        boolean result = fc.utobj().assertPageSource(driver, Semail.getSubject());
	        if(result == true) {
	        	System.out.println("send Email found on the Email page");
	        	 fc.utobj().clickElement(driver, ".//span[contains(text(),'"+email_sub+"')]");
	 	        fc.commonMethods().switch_cboxIframe_frameId(driver);
	 	        fc.utobj().clickElement(driver, ".//input[@value = 'Close']");
	 	        fc.utobj().switchFrameToDefault(driver);
	 	       fc.utobj().clickElement(driver, ".//span[contains(text(),'"+email_sub+"')]");
	 	      fc.commonMethods().switch_cboxIframe_frameId(driver);
	 	     fc.utobj().clickElement(driver, ".//input[@value = 'Reply']");
	        	
	        }else {
	        	System.out.println("Email not found");
	        }
	        Thread.sleep(1000);
	      
	        Semail = crm.crm_common().defualtemailfill(Semail);
	       emailfill.filldetails(driver, Semail);
			fc.utobj().switchFrameToDefault(driver);
			emailfill.savebtn(driver);
			System.out.println("Reply done");
			
			
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}
	
	@Test(groups = {"crm"})
	@TestCase(createdOn = "2018-05-07", updatedOn = "2018-05-07", testCaseDescription = "CRM > Lead Summary > Add campagin > add lead > associate campagin page >filter campagin > move to campaign detail page and associate with lead ", testCaseId = "TC_13142_Add_Lead")
	public void leadInfopage30() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			
			FCHomePageTest fcHomePageTest = fc.commonMethods().getModules();
			CRMModule crm = new CRMModule();
			

			crm.crm_common().openCRMCampaignCenterPage(fcHomePageTest, driver);
			CRM_CampaginCenterPagelink add = new CRM_CampaginCenterPagelink();
			add.CreateCampaign(driver);
			CRM_CampaignAddpage addfill = new CRM_CampaignAddpage();
			CRM_CampaignAddGetSet camAdd = new CRM_CampaignAddGetSet();
			camAdd = crm.crm_common().defualtCampaignfill(camAdd);
			addfill.filldetails(driver, camAdd);
			addfill.Cancelbtnclick(driver);
			Thread.sleep(1000);
			add.CreateCampaign(driver);
			addfill.filldetails(driver, camAdd);
			addfill.clickStartbtn(driver);
			Thread.sleep(500);
			
			CRM_SelectandCustomizeTemplatepage custom = new CRM_SelectandCustomizeTemplatepage();
			custom.EditCampaignName(driver);
			addfill.filldetails(driver, camAdd);
			addfill.Savebtnclick(driver);
			Thread.sleep(500);
			addfill.closebtnclick(driver);
			
			
			custom.firstTemplateclick(driver);
			Thread.sleep(5000);
			
			
			CRM_TemplateInfoPageGetSet teminfo = new CRM_TemplateInfoPageGetSet();
			CRM_TemplateInfoPage info = new CRM_TemplateInfoPage();
			teminfo = crm.crm_common().defualtTemplateinfofill(teminfo);
			String TemNAme = teminfo.getTemaplateName();
			System.out.println(TemNAme);
			info.filldetails(driver, teminfo);
			info.savebtn(driver);
		
			Thread.sleep(5000);
			info.saveAndcontinue(driver);
			Thread.sleep(500);
			
			CRM_CampaignSelectRecipientpage recipient = new CRM_CampaignSelectRecipientpage();
			CRM_CampaignSelectRecipientUI recipientUI = new CRM_CampaignSelectRecipientUI(driver);
			recipient.AssociateLater(driver);
			Thread.sleep(500);
			
			CRM_CampaignFinalandLunchpage lunch = new CRM_CampaignFinalandLunchpage();
			CRM_CampaignFinalandLunchUI lunchUI = new CRM_CampaignFinalandLunchUI(driver);
			fc.utobj().selectDropDown(driver, lunchUI.TimeZone, "GMT +05:30 India");
		    String	CampaignName = lunchUI.campaignName.getText();
			System.out.println(CampaignName);
			lunch.clickFinalUpperbtn(driver);
			String confirm = lunchUI.confirmpagetext.getText();
			System.out.println(confirm);
			
			
			if(confirm.equalsIgnoreCase(CampaignName+" "+"has been scheduled successfully"))
			{
				System.out.println("Campaign Add Successfully ");
			}else {
				System.out.println("campaign not added");
			}
			
			crm.crm_common().openCRMLeadsPage(fcHomePageTest, driver).clickAddLead(driver);
			Leadgetset lead = new Leadgetset();
			lead = crm.crm_common().getDefaultDataForLead(lead);
			Addleadpage addlead = new Addleadpage();
			addlead.fillLeadDetails(driver, lead);
			addlead.save(driver);
			 CRM_leadInfoPageOptions leadinfo = new CRM_leadInfoPageOptions();
			 leadinfo.AssociatecampOnInfoPage(driver);
			 fc.utobj().printTestStep("click filter button");
			 fc.utobj().clickElement(driver, lunchUI.FilterBtn);
			 fc.utobj().printTestStep("search campaign by name");
			 fc.utobj().sendKeys(driver,lunchUI.SearchCampaignfield , CampaignName);
			 fc.utobj().clickElement(driver, "//button[@id='search']");
			 
			 fc.utobj().printBugStatus("click on campagin name to open campagin detail page");
			 fc.utobj().clickElement(driver, ".//a[contains (text(),'"+CampaignName+"')]");
			 fc.utobj().clickElement(driver, "//tr[@class='ContactsType']//a[@id='ccptem2']");
			 fc.utobj().switchFrameByClass(driver, "newLayoutcboxIframe");
			 fc.utobj().sleep(2000);
			 //fc.utobj().clickElement(driver, ".//button[contains(text(),'Close')]");//.//button[.='Close' or @name='Close' or @value='Close']")
			 fc.commonMethods().Click_Close_Button_ByValue(driver);
			fc.utobj().switchFrameToDefault(driver);
			fc.utobj().sleep(2000);
			/*fc.utobj().clickElement(driver, ".//div[@id = 'a1']//span");
	
			 fc.utobj().switchFrameByClass(driver, "newLayoutcboxIframe");      this code is only for preview campaign 
			 fc.utobj().sleep(2000);
			 fc.utobj().clickElement(driver, ".//*[@id = 'BackToPage']");*/
			 
			 
			 fc.utobj().clickElement(driver, ".//button[contains(text(),'Back')]");
			 
			 fc.utobj().clickElement(driver, recipientUI.getXpathOfCampaignAction(CampaignName));
			 fc.utobj().clickElement(driver, recipientUI.confirmOfAssociateWithCampagin);
			 
			
			
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}
	
	@Test(groups = {"crm"})
	@TestCase(createdOn = "2018-05-07", updatedOn = "2018-05-07", testCaseDescription = "CRM > Lead Summary > Add Template > add lead > add campaign and modify", testCaseId = "TC_13142_Add and modify campagin")
	public void leadInfopage31() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			
			FCHomePageTest fcHomePageTest = fc.commonMethods().getModules();
			CRMModule crm = new CRMModule();
			

			crm.crm_common().openCRMCampaignCenterPage(fcHomePageTest, driver);
			CRM_CampaginCenterPagelink add = new CRM_CampaginCenterPagelink();
			CRM_CampaignAddpage addfill = new CRM_CampaignAddpage();
			CRM_CampaignAddGetSet camAdd = new CRM_CampaignAddGetSet();
			camAdd = crm.crm_common().defualtCampaignfill(camAdd);
			add.CreateCampaign(driver);
			addfill.filldetails(driver, camAdd);
			addfill.Cancelbtnclick(driver);
			Thread.sleep(1000);
			add.CreateCampaign(driver);
			addfill.filldetails(driver, camAdd);
			addfill.clickStartbtn(driver);
			Thread.sleep(500);
			
			CRM_SelectandCustomizeTemplatepage custom = new CRM_SelectandCustomizeTemplatepage();
			custom.EditCampaignName(driver);
			addfill.filldetails(driver, camAdd);
			addfill.Savebtnclick(driver);
			Thread.sleep(500);
			addfill.closebtnclick(driver);
			
			
			custom.firstTemplateclick(driver);
			Thread.sleep(5000);
			
			
			CRM_TemplateInfoPageGetSet teminfo = new CRM_TemplateInfoPageGetSet();
			CRM_TemplateInfoPage info = new CRM_TemplateInfoPage();
			teminfo = crm.crm_common().defualtTemplateinfofill(teminfo);
			String TemNAme = teminfo.getTemaplateName();
			System.out.println(TemNAme);
			info.filldetails(driver, teminfo);
			info.savebtn(driver);
		
			Thread.sleep(5000);
			info.saveAndcontinue(driver);
			Thread.sleep(500);
			
			CRM_CampaignSelectRecipientpage recipient = new CRM_CampaignSelectRecipientpage();
			
			recipient.AssociateLater(driver);
			Thread.sleep(500);
			
			CRM_CampaignFinalandLunchpage lunch = new CRM_CampaignFinalandLunchpage();
			CRM_CampaignFinalandLunchUI lunchUI = new CRM_CampaignFinalandLunchUI(driver);
			fc.utobj().selectDropDown(driver, lunchUI.TimeZone, "GMT +05:30 India");
		    String	CampaignName = lunchUI.campaignName.getText();
			System.out.println(CampaignName);
			lunch.clickFinalUpperbtn(driver);
			String confirm = lunchUI.confirmpagetext.getText();
			System.out.println(confirm);
			
			
			if(confirm.equalsIgnoreCase(CampaignName+" "+"has been scheduled successfully"))
			{
				System.out.println("Campaign Add Successfully ");
			}else {
				System.out.println("campaign not added");
			}
			
			fc.utobj().clickElement(driver, lunchUI.Viewcampaign);
			fc.utobj().clickElement(driver, ".//a[.='"+CampaignName+"']/ancestor::td[1]/following-sibling::td/div[@class= 'dropdown-action no-arrow three-dot-nav']");
			fc.utobj().clickElement(driver, ".//ul[@class='dropdown-list direction open-menu']/li[1]/a[1]");
			Thread.sleep(1000);
			fc.utobj().clickElement(driver, ".//button[contains(text(),'Save')]");
			
			 
			
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}
	
	@Test(groups = {"crm"})
	@TestCase(createdOn = "2018-05-07", updatedOn = "2018-05-07", testCaseDescription = "CRM > Lead Summary > add campaign > add lead >  modify campaign while associate with lead", testCaseId = "TC-13180_Add_Lead")
	public void leadInfopage32() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			
			FCHomePageTest fcHomePageTest = fc.commonMethods().getModules();
			CRMModule crm = new CRMModule();
			

			crm.crm_common().openCRMCampaignCenterPage(fcHomePageTest, driver);
			CRM_CampaginCenterPagelink add = new CRM_CampaginCenterPagelink();
			add.CreateCampaign(driver);
			CRM_CampaignAddpage addfill = new CRM_CampaignAddpage();
			CRM_CampaignAddGetSet camAdd = new CRM_CampaignAddGetSet();
			camAdd = crm.crm_common().defualtCampaignfill(camAdd);
			addfill.filldetails(driver, camAdd);
			addfill.Cancelbtnclick(driver);
			Thread.sleep(1000);
			add.CreateCampaign(driver);
			addfill.filldetails(driver, camAdd);
			addfill.clickStartbtn(driver);
			Thread.sleep(500);
			
			CRM_SelectandCustomizeTemplatepage custom = new CRM_SelectandCustomizeTemplatepage();
			custom.EditCampaignName(driver);
			addfill.filldetails(driver, camAdd);
			addfill.Savebtnclick(driver);
			Thread.sleep(500);
			addfill.closebtnclick(driver);
			
			
			custom.firstTemplateclick(driver);
			Thread.sleep(5000);
			
			
			CRM_TemplateInfoPageGetSet teminfo = new CRM_TemplateInfoPageGetSet();
			CRM_TemplateInfoPage info = new CRM_TemplateInfoPage();
			teminfo = crm.crm_common().defualtTemplateinfofill(teminfo);
			String TemNAme = teminfo.getTemaplateName();
			System.out.println(TemNAme);
			info.filldetails(driver, teminfo);
			info.savebtn(driver);
		
			Thread.sleep(5000);
			info.saveAndcontinue(driver);
			Thread.sleep(500);
			
			CRM_CampaignSelectRecipientpage recipient = new CRM_CampaignSelectRecipientpage();
			
			recipient.AssociateLater(driver);
			Thread.sleep(500);
			
			CRM_CampaignFinalandLunchpage lunch = new CRM_CampaignFinalandLunchpage();
			CRM_CampaignFinalandLunchUI lunchUI = new CRM_CampaignFinalandLunchUI(driver);
			fc.utobj().selectDropDown(driver, lunchUI.TimeZone, "GMT +05:30 India");
		    String	CampaignName = lunchUI.campaignName.getText();
			System.out.println(CampaignName);
			lunch.clickFinalUpperbtn(driver);
			String confirm = lunchUI.confirmpagetext.getText();
			System.out.println(confirm);
			
			
			if(confirm.equalsIgnoreCase(CampaignName+" "+"has been scheduled successfully"))
			{
				System.out.println("Campaign Add Successfully ");
			}else {
				System.out.println("campaign not added");
			}
			
			crm.crm_common().openCRMLeadsPage(fcHomePageTest, driver).clickAddLead(driver);
			Leadgetset lead = new Leadgetset();
			lead = crm.crm_common().getDefaultDataForLead(lead);
			Addleadpage addlead = new Addleadpage();
			addlead.fillLeadDetails(driver, lead);
			addlead.save(driver);
			 CRM_leadInfoPageOptions leadinfo = new CRM_leadInfoPageOptions();
			 leadinfo.AssociatecampOnInfoPage(driver);
			 
			 fc.utobj().printBugStatus("click on campaign name to open campaign detail page");
			 fc.utobj().clickElement(driver, ".//a[contains (text(),'"+CampaignName+"')]");
			 
			 fc.utobj().clickElement(driver, "//span[@id='campaignActions']");
			 fc.utobj().clickElement(driver, "//a[@href='#'][contains(text(),'Modify')]");
			 fc.utobj().clickElement(driver, "//div[@id='siteMainDiv']//button[2]");
			 
			 
				fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
			}
	}
	
	
	@Test(groups = {"crm"})
	@TestCase(createdOn = "2018-05-07", updatedOn = "2018-05-07", testCaseDescription = "CRM > Lead Summary > add campaign > add lead >  delete campaign while associate with lead", testCaseId = "TC-13082_Add_Lead")
	public void leadInfopage33() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			
			FCHomePageTest fcHomePageTest = fc.commonMethods().getModules();
			CRMModule crm = new CRMModule();
			

			crm.crm_common().openCRMCampaignCenterPage(fcHomePageTest, driver);
			CRM_CampaginCenterPagelink add = new CRM_CampaginCenterPagelink();
			add.CreateCampaign(driver);
			CRM_CampaignAddpage addfill = new CRM_CampaignAddpage();
			CRM_CampaignAddGetSet camAdd = new CRM_CampaignAddGetSet();
			camAdd = crm.crm_common().defualtCampaignfill(camAdd);
			addfill.filldetails(driver, camAdd);
			addfill.Cancelbtnclick(driver);
			Thread.sleep(1000);
			add.CreateCampaign(driver);
			addfill.filldetails(driver, camAdd);
			addfill.clickStartbtn(driver);
			Thread.sleep(500);
			
			CRM_SelectandCustomizeTemplatepage custom = new CRM_SelectandCustomizeTemplatepage();
			custom.EditCampaignName(driver);
			addfill.filldetails(driver, camAdd);
			addfill.Savebtnclick(driver);
			Thread.sleep(500);
			addfill.closebtnclick(driver);
			
			
			custom.firstTemplateclick(driver);
			Thread.sleep(5000);
			
			
			CRM_TemplateInfoPageGetSet teminfo = new CRM_TemplateInfoPageGetSet();
			CRM_TemplateInfoPage info = new CRM_TemplateInfoPage();
			teminfo = crm.crm_common().defualtTemplateinfofill(teminfo);
			String TemNAme = teminfo.getTemaplateName();
			System.out.println(TemNAme);
			info.filldetails(driver, teminfo);
			info.savebtn(driver);
		
			Thread.sleep(5000);
			info.saveAndcontinue(driver);
			Thread.sleep(500);
			
			CRM_CampaignSelectRecipientpage recipient = new CRM_CampaignSelectRecipientpage();
			
			recipient.AssociateLater(driver);
			Thread.sleep(500);
			
			CRM_CampaignFinalandLunchpage lunch = new CRM_CampaignFinalandLunchpage();
			CRM_CampaignFinalandLunchUI lunchUI = new CRM_CampaignFinalandLunchUI(driver);
			fc.utobj().selectDropDown(driver, lunchUI.TimeZone, "GMT +05:30 India");
		    String	CampaignName = lunchUI.campaignName.getText();
			System.out.println(CampaignName);
			lunch.clickFinalUpperbtn(driver);
			String confirm = lunchUI.confirmpagetext.getText();
			System.out.println(confirm);
			
			
			if(confirm.equalsIgnoreCase(CampaignName+" "+"has been scheduled successfully"))
			{
				System.out.println("Campaign Add Successfully ");
			}else {
				System.out.println("campaign not added");
			}
			
			crm.crm_common().openCRMLeadsPage(fcHomePageTest, driver).clickAddLead(driver);
			Leadgetset lead = new Leadgetset();
			lead = crm.crm_common().getDefaultDataForLead(lead);
			Addleadpage addlead = new Addleadpage();
			addlead.fillLeadDetails(driver, lead);
			addlead.save(driver);
			 CRM_leadInfoPageOptions leadinfo = new CRM_leadInfoPageOptions();
			 leadinfo.AssociatecampOnInfoPage(driver);
			 
			 fc.utobj().printBugStatus("click on campaign name to open campaign detail page");
			 fc.utobj().clickElement(driver, ".//a[contains (text(),'"+CampaignName+"')]");
			 
			 fc.utobj().clickElement(driver, "//span[@id='campaignActions']");
			
			 fc.utobj().clickElement(driver, "//a[@href='#'][contains(text(),'Delete')]");
			 fc.utobj().acceptAlertBox(driver);
			 
			
				fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
			}
	}
	
	@Test(groups = "crm")
	@TestCase(createdOn = "2018-05-07", updatedOn = "2018-05-07", testCaseDescription = "CRM > Lead Summary >add template > add lead > send email > attach template > signature > email CC and custom ID ", testCaseId = "TC-13082_TC-13028_send Email With Keyword,template and all")
	public void leadInfopage34() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			
			FCHomePageTest fcHomePageTest = fc.commonMethods().getModules();
			CRMModule crm = new CRMModule();
			

			crm.crm_common().openCRMCampaignCenterPage(fcHomePageTest, driver);
			CRM_CampaginCenterPagelink add = new CRM_CampaginCenterPagelink();
			add.CreateTemplate(driver);
			
			 CRM_SelectandCustomizeTemplatepage custom = new CRM_SelectandCustomizeTemplatepage();
			 custom.firstTemplateclick(driver); 
			
			 CRM_TemplateInfoPageGetSet teminfo = new CRM_TemplateInfoPageGetSet();
				CRM_TemplateInfoPage info = new CRM_TemplateInfoPage();
				teminfo = crm.crm_common().defualtTemplateinfofill(teminfo);
				String TemNAme = teminfo.getTemaplateName();
				System.out.println(TemNAme);
				info.filldetails(driver, teminfo);
				info.savebtn(driver);
				
				
				
				CRM_TemplateInfoPageUI Teminfop = new CRM_TemplateInfoPageUI(driver);
				fc.utobj().clickElement(driver, Teminfop.Save);
			 
				crm.crm_common().openCRMLeadsPage(fcHomePageTest, driver).clickAddLead(driver);
				Leadgetset lead = new Leadgetset();
				lead = crm.crm_common().getDefaultDataForLead(lead);
				Addleadpage addlead = new Addleadpage();
				addlead.fillLeadDetails(driver, lead);
				addlead.save(driver);
				
				CRM_leadInfoPageOptions leadinfo = new CRM_leadInfoPageOptions();
				leadinfo.SendEmailOnInfoPage(driver);
				CRM_SendEmailUI em = new CRM_SendEmailUI(driver);
				CRM_SendEmailFillTest emailfill = new CRM_SendEmailFillTest();
				emailfill.CustomID_onEmail(driver, "vipin.sain@franconnect.com");
			
				fc.utobj().selectDropDown(driver, em.TemplateEmail, TemNAme);
				CRM_SendEmailGetSet Semail = new CRM_SendEmailGetSet();
				Semail= crm.crm_common().defualtemailfill(Semail);
				fc.utobj().sendKeys(driver, em.CCbox, Semail.getCC());
				fc.utobj().sendKeys(driver, em.EmailSubject, Semail.getSubject());
				fc.utobj().clickElement(driver, em.SignatureRadio);
				/*fc.utobj().clickElement(driver, em.add_signature_btn);
				fc.commonMethods().switch_cboxIframe_frameId(driver);
				fc.utobj().sendKeys(driver, em.Signature_Name, "signature vip");
				fc.utobj().switchFrame(driver, em.Signature_textAreaframe);
				fc.utobj().sendKeys(driver, em.Signature_textArea,"vipin");
				fc.utobj().switchFrameToDefault(driver);
				fc.utobj().clickElement(driver, em.SetAsDefualt);
				fc.utobj().clickElement(driver, em.Add_signaturebtn);*/
			fc.utobj().clickElement(driver, em.keyWord);
			fc.utobj().clickElement(driver, em.Availabe_keyword);
			fc.utobj().clickElement(driver, em.firstname_keyword);
			
				emailfill.savebtn(driver);
                String emailsubject = Semail.getSubject();
				String activehst = driver.findElement(By.id("ActivityHis")).getText();
				
				
				if (activehst.contains(emailsubject))  {	
					System.out.println("Email subject remark present");
					fc.utobj().printTestStep("email remark created");
				}
			 
			 
			 

				fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
			}
	}
	
	@Test(groups = {"crm","hello789"})
	@TestCase(createdOn = "2018-05-07", updatedOn = "2018-05-07", testCaseDescription = "CRM > Lead Summary >add lead > email id field blank > click associate campagin button > to check without email campaign can associate or not  ", testCaseId = "TC-13142_add lead without email and try to associate campaign")
	public void leadInfopage35() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			
			FCHomePageTest fcHomePageTest = fc.commonMethods().getModules();
			CRMModule crm = new CRMModule();
			
				crm.crm_common().openCRMLeadsPage(fcHomePageTest, driver).clickAddLead(driver);
				Leadgetset lead = new Leadgetset();
				lead = crm.crm_common().getDefaultDataForLead(lead);
				Addleadpage addlead = new Addleadpage();
				lead.setPrimarycontact("");
				lead.setEmail("");
				lead.setAlternarteemail("");
				addlead.fillLeadDetails(driver, lead);
				addlead.save(driver);
				
				CRM_leadInfoPageOptions leadinfo = new CRM_leadInfoPageOptions();
				leadinfo.AssociatecampOnInfoPage(driver);
				Alert alert = driver.switchTo().alert();
				String war = alert.getText();
				fc.utobj().acceptAlertBox(driver);
		
				String warningMSG = "No Email is associated with this Lead.";
				if(war.equalsIgnoreCase(warningMSG)) {
					System.out.println("unable to associate");
				}
				CRM_LeadinfopageUI ui = new CRM_LeadinfopageUI(driver);
				Actions action = new Actions(driver);
				action.moveToElement(ui.MoreActionMenu).build().perform();
				Thread.sleep(100);
				fc.utobj().clickElement(driver, ui.MoreActionMenu);
				fc.utobj().clickElement(driver, ui.MoreActionMenu_Associatewithcampaign);
				action.click(ui.MoreActionMenu_Associatewithcampaign).build().perform();
				
				String warning1 = "No Email/Mobile is associated with this Lead.";
				
				Alert alert1 = driver.switchTo().alert();
				String morewar = alert1.getText();
				fc.utobj().acceptAlertBox(driver);
				if(morewar.equalsIgnoreCase(warning1)) {
					System.out.println("unable to associate from more action button too");
				}
			 
			 
				fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
			}
	}
	
	@Test(groups = {"crm"})
	@TestCase(createdOn = "2018-05-07", updatedOn = "2018-05-07", testCaseDescription = "CRM > Lead Summary >add campaign > add lead > associate with lead > copy and customize campagin ", testCaseId = "TC-13255_add lead without email and try to associate campaign")
	public void leadInfopage36() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		
		try {
			driver = fc.loginpage().login(driver);
			
			FCHomePageTest fcHomePageTest = fc.commonMethods().getModules();
			CRMModule crm = new CRMModule();
			

			crm.crm_common().openCRMCampaignCenterPage(fcHomePageTest, driver);
			CRM_CampaginCenterPagelink add = new CRM_CampaginCenterPagelink();
			add.CreateCampaign(driver);
			CRM_CampaignAddpage addfill = new CRM_CampaignAddpage();
			CRM_CampaignAddGetSet camAdd = new CRM_CampaignAddGetSet();
			camAdd = crm.crm_common().defualtCampaignfill(camAdd);
			addfill.filldetails(driver, camAdd);
			addfill.Cancelbtnclick(driver);
			Thread.sleep(1000);
			add.CreateCampaign(driver);
			addfill.filldetails(driver, camAdd);
			addfill.clickStartbtn(driver);
			Thread.sleep(500);
			
			CRM_SelectandCustomizeTemplatepage custom = new CRM_SelectandCustomizeTemplatepage();
			custom.EditCampaignName(driver);
			addfill.filldetails(driver, camAdd);
			addfill.Savebtnclick(driver);
			Thread.sleep(500);
			addfill.closebtnclick(driver);
			
			
			custom.firstTemplateclick(driver);
			Thread.sleep(5000);
			
			
			CRM_TemplateInfoPageGetSet teminfo = new CRM_TemplateInfoPageGetSet();
			CRM_TemplateInfoPage info = new CRM_TemplateInfoPage();
			teminfo = crm.crm_common().defualtTemplateinfofill(teminfo);
			String TemNAme = teminfo.getTemaplateName();
			System.out.println(TemNAme);
			info.filldetails(driver, teminfo);
			info.savebtn(driver);
		
			Thread.sleep(5000);
			info.saveAndcontinue(driver);
			Thread.sleep(500);
			
			CRM_CampaignSelectRecipientpage recipient = new CRM_CampaignSelectRecipientpage();
			
			recipient.AssociateLater(driver);
			Thread.sleep(500);
			
			CRM_CampaignFinalandLunchpage lunch = new CRM_CampaignFinalandLunchpage();
			CRM_CampaignFinalandLunchUI lunchUI = new CRM_CampaignFinalandLunchUI(driver);
			fc.utobj().selectDropDown(driver, lunchUI.TimeZone, "GMT +05:30 India");
		    String	CampaignName = lunchUI.campaignName.getText();
			System.out.println(CampaignName);
			lunch.clickFinalUpperbtn(driver);
			String confirm = lunchUI.confirmpagetext.getText();
			System.out.println(confirm);
			
			
			if(confirm.equalsIgnoreCase(CampaignName+" "+"has been scheduled successfully"))
			{
				System.out.println("Campaign Add Successfully ");
			}else {
				System.out.println("campaign not added");
			}
			
			crm.crm_common().openCRMLeadsPage(fcHomePageTest, driver).clickAddLead(driver);
			Leadgetset lead = new Leadgetset();
			lead = crm.crm_common().getDefaultDataForLead(lead);
			Addleadpage addlead = new Addleadpage();
			addlead.fillLeadDetails(driver, lead);
			addlead.save(driver);
			 CRM_leadInfoPageOptions leadinfo = new CRM_leadInfoPageOptions();
			 leadinfo.AssociatecampOnInfoPage(driver);
			 
			 fc.utobj().printBugStatus("click on campaign name to open campaign detail page");
			 fc.utobj().clickElement(driver, ".//a[contains (text(),'"+CampaignName+"')]");
			 
			 fc.utobj().clickElement(driver, "//span[@id='campaignActions']");
			
			 fc.utobj().clickElement(driver, "//a[@href='#'][contains(text(),'Copy and Customize')]");
			fc.utobj().switchFrameByClass(driver, "newLayoutcboxIframe");
			camAdd = crm.crm_common().defualtCampaignfill(camAdd);
			CRM_CampaignAddUI camui = new CRM_CampaignAddUI(driver);
			fc.utobj().sendKeys(driver, camui.campaignName, camAdd.getCampaignName());
			System.out.println(camAdd.getCampaignName());
			String newcamNAME = camAdd.getCampaignName();
			fc.utobj().clickElement(driver, camui.Start);
			Thread.sleep(1000);
			fc.utobj().clickElement(driver, lunchUI.finalizebottombtn);
			
			String Again_confirm = lunchUI.confirmpagetext.getText();
			System.out.println(confirm);
			
			
			if(Again_confirm.equalsIgnoreCase(newcamNAME+" "+"has been scheduled successfully"))
			{
				System.out.println("Campaign Add Successfully ");
			}else {
				System.out.println("campaign not added");
			}
			
			 fc.utobj().clickElement(driver, ".//button[.='View Campaigns']");
			
			 
				fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
			}
	}
	
	@Test(groups = "crm")
	@TestCase(createdOn = "2018-05-07", updatedOn = "2018-05-07", testCaseDescription = "CRM > Lead Summary >add lead > verify email tab page contents ", testCaseId = "TC-13015_Verify Email tab contents")
	public void leadInfopage37() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		
		try {
			driver = fc.loginpage().login(driver);
			
			FCHomePageTest fcHomePageTest = fc.commonMethods().getModules();
			CRMModule crm = new CRMModule();
			crm.crm_common().openCRMLeadsPage(fcHomePageTest, driver).clickAddLead(driver);
			
			Leadgetset lead = new Leadgetset();
			lead = crm.crm_common().getDefaultDataForLead(lead);
			Addleadpage addlead = new Addleadpage();
			addlead.fillLeadDetails(driver, lead);
			addlead.save(driver);
			
			  CRM_leadInfoPageOptions leadinfo = new CRM_leadInfoPageOptions();
		        leadinfo.Emails_TabOnInfoPage(driver);
		        String outbox = driver.findElement(By.xpath(".//td[contains(text(),' Outbox for ')]")).getText();
		        if(outbox.contains("Outbox for")) {
		        	System.out.println("Your r on outbox page");
		        }
		       String Emailinbox = driver.findElement(By.id("sentMailDiv")).getText();
		       System.out.println(Emailinbox);
		       if(Emailinbox.equalsIgnoreCase("  No Emails Found.")) {
		    	   System.out.println("No Email found on email page");
		       }
		       leadinfo.SendEmail_Emails_Tab(driver);
		        
		        CRM_SendEmailGetSet Semail = new CRM_SendEmailGetSet();
				CRM_SendEmailFillTest emailfill = new CRM_SendEmailFillTest();
				Semail = crm.crm_common().defualtemailfill(Semail);
				
				emailfill.filldetails(driver, Semail);
				fc.utobj().switchFrameToDefault(driver);
				emailfill.savebtn(driver);
		       Thread.sleep(1000);
				String Emailinbox1 = driver.findElement(By.id("sentMailDiv")).getText();
			       System.out.println(Emailinbox1);
			if(Emailinbox1.contains(Semail.getSubject()))
			{
				System.out.println("Match");
			}
			
			
				fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
				fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
			}
	}
	
	@Test(groups = {"crm"})
	@TestCase(createdOn = "2018-05-07", updatedOn = "2018-05-07", testCaseDescription = "CRM > Lead Summary >add lead > change subscription status and verify remarks  ", testCaseId = "TC-13103_TC-13139 verify subscription chnage in lead")
	public void leadInfopage38() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		
		try {
			driver = fc.loginpage().login(driver);
			
			FCHomePageTest fcHomePageTest = fc.commonMethods().getModules();
			CRMModule crm = new CRMModule();
			crm.crm_common().openCRMLeadsPage(fcHomePageTest, driver).clickAddLead(driver);
			
			Leadgetset lead = new Leadgetset();
			lead = crm.crm_common().getDefaultDataForLead(lead);
			Addleadpage addlead = new Addleadpage();
			addlead.fillLeadDetails(driver, lead);
			addlead.save(driver);
			CRM_LeadinfopageUI mod = new CRM_LeadinfopageUI(driver);
			String Newly_Add = mod.Email_Subscription_Status.getText();
			if(Newly_Add.equalsIgnoreCase("Opted In")) {
				System.out.println("Newly Add lead email subscription status is in Opted-in Status");
				fc.utobj().printTestStep("Newly Add lead email subscription status is in Opted-in Status");
			}
			
			Thread.sleep(1000);
			 CRM_leadInfoPageOptions leadinfo = new CRM_leadInfoPageOptions();
			 leadinfo.modifyLeadOnInfopage(driver);
			 
			 LeadUI leadui = new LeadUI(driver);
			 fc.utobj().printTestStep("change subscription status into opted-out state");
			 fc.utobj().clickElement(driver, leadui.opt_out);
			 fc.utobj().acceptAlertBox(driver);
			 addlead.save(driver);
			 Thread.sleep(1000);
			 
			 String activity = mod.Activity_History.getText();
			 String Current_Status = mod.Email_Subscription_Status.getText();
			 if(activity.contains("Lead has been Opted-Out manually.")&& Current_Status.equalsIgnoreCase("Opted Out")) {
				 System.out.println("Subscription status change into opt-out Status ");
				 fc.utobj().printTestStep("Subscription status change into opt-out Status");
			 }
			 
			 
			 leadinfo.modifyLeadOnInfopage(driver);
			 fc.utobj().printTestStep("change subscription status into opted-in state");
			 fc.utobj().clickElement(driver, leadui.opt_in);
			 addlead.save(driver);
			 Thread.sleep(1000);
			 String New_Status = mod.Email_Subscription_Status.getText();
			 String activity1 = mod.Activity_History.getText();
			 if(activity1.contains("Lead has been Opted-In manually.") && New_Status.equalsIgnoreCase("Opted In")) {
				 System.out.println("Subscription status change into opt-in Status ");
				 fc.utobj().printTestStep("Subscription status change into opt-in Status");
			 }
			 
			 System.out.println("Subscription Status Done");
			 
		     
			
				fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
				fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
			}
	}
	
	@Test(groups = "crm")
	@TestCase(createdOn = "2018-05-07", updatedOn = "2018-05-07", testCaseDescription = "CRM > Lead Summary >add campagin >add lead > associate with lead > move to campagin detail page and archived campaign ", testCaseId = "TC-13160_Archived campaign")
	public void leadInfopage39() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		
		try {
			driver = fc.loginpage().login(driver);
			
			FCHomePageTest fcHomePageTest = fc.commonMethods().getModules();
			CRMModule crm = new CRMModule();
			

			crm.crm_common().openCRMCampaignCenterPage(fcHomePageTest, driver);
			CRM_CampaginCenterPagelink add = new CRM_CampaginCenterPagelink();
			add.CreateCampaign(driver);
			CRM_CampaignAddpage addfill = new CRM_CampaignAddpage();
			CRM_CampaignAddGetSet camAdd = new CRM_CampaignAddGetSet();
			camAdd = crm.crm_common().defualtCampaignfill(camAdd);
			addfill.filldetails(driver, camAdd);
			addfill.Cancelbtnclick(driver);
			Thread.sleep(1000);
			add.CreateCampaign(driver);
			addfill.filldetails(driver, camAdd);
			addfill.clickStartbtn(driver);
			Thread.sleep(500);
			
			CRM_SelectandCustomizeTemplatepage custom = new CRM_SelectandCustomizeTemplatepage();
			custom.EditCampaignName(driver);
			addfill.filldetails(driver, camAdd);
			addfill.Savebtnclick(driver);
			Thread.sleep(500);
			addfill.closebtnclick(driver);
			
			
			custom.firstTemplateclick(driver);
			Thread.sleep(5000);
			
			
			CRM_TemplateInfoPageGetSet teminfo = new CRM_TemplateInfoPageGetSet();
			CRM_TemplateInfoPage info = new CRM_TemplateInfoPage();
			teminfo = crm.crm_common().defualtTemplateinfofill(teminfo);
			String TemNAme = teminfo.getTemaplateName();
			System.out.println(TemNAme);
			info.filldetails(driver, teminfo);
			info.savebtn(driver);
		
			Thread.sleep(5000);
			info.saveAndcontinue(driver);
			Thread.sleep(500);
			
			CRM_CampaignSelectRecipientpage recipient = new CRM_CampaignSelectRecipientpage();
			
			recipient.AssociateLater(driver);
			Thread.sleep(500);
			
			CRM_CampaignFinalandLunchpage lunch = new CRM_CampaignFinalandLunchpage();
			CRM_CampaignFinalandLunchUI lunchUI = new CRM_CampaignFinalandLunchUI(driver);
			fc.utobj().selectDropDown(driver, lunchUI.TimeZone, "GMT +05:30 India");
		    String	CampaignName = lunchUI.campaignName.getText();
			System.out.println(CampaignName);
			lunch.clickFinalUpperbtn(driver);
			String confirm = lunchUI.confirmpagetext.getText();
			System.out.println(confirm);
			
			
			if(confirm.equalsIgnoreCase(CampaignName+" "+"has been scheduled successfully"))
			{
				System.out.println("Campaign Add Successfully ");
			}else {
				System.out.println("campaign not added");
			}
			
			crm.crm_common().openCRMLeadsPage(fcHomePageTest, driver).clickAddLead(driver);
			Leadgetset lead = new Leadgetset();
			lead = crm.crm_common().getDefaultDataForLead(lead);
			Addleadpage addlead = new Addleadpage();
			addlead.fillLeadDetails(driver, lead);
			addlead.save(driver);
			 CRM_leadInfoPageOptions leadinfo = new CRM_leadInfoPageOptions();
			 leadinfo.AssociatecampOnInfoPage(driver);
			 
			 fc.utobj().printBugStatus("click on campaign name to open campaign detail page");
			 fc.utobj().clickElement(driver, ".//a[contains (text(),'"+CampaignName+"')]");
			 
			 fc.utobj().clickElement(driver, "//span[@id='campaignActions']");
			fc.utobj().printTestStep("Click on archive button in action menu");
			 fc.utobj().clickElement(driver, "//a[@href='#'][contains(text(),'Archive')]");
			 fc.utobj().acceptAlertBox(driver);
			 
			 Thread.sleep(1000);
			 fc.utobj().printTestStep("click on archived tab");
			 fc.utobj().clickElement(driver, ".//ul[@id='FilterTabShowHide']/li[4]");
			 
			 if(fc.utobj().assertPageSource(driver, CampaignName))
			 {
				 System.out.println("campagin present in the archive page");
			 }
			 
			 
			 fc.utobj().printTestStep("campaign archived done");
			 
			 
				fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
				fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
			}
	}
	
	@Test(groups = { "crm"})
	@TestCase(createdOn = "2018-05-07", updatedOn = "2018-05-07", testCaseDescription = "CRM > Lead Summary >add lead > add group > add campaign > add group at recipient page in campaign finalize", testCaseId = "TC-13165_recipient page")
	public void leadInfopage40() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		
			
			try {
				driver = fc.loginpage().login(driver);

				FCHomePageTest fcHomePageTest = fc.commonMethods().getModules();
				Leadgetset lead = new Leadgetset();
				CRMModule crm = new CRMModule();
				// Go to Lead in CRM
				
			

				crm.crm_common().openCRMLeadsPage(fcHomePageTest, driver).clickAddLead(driver);

				lead = crm.crm_common().getDefaultDataForLead(lead);

				Addleadpage addlead = new Addleadpage();
				addlead.fillLeadDetails(driver, lead);
				addlead.save(driver);

				CRM_leadInfoPageOptions leadinfo = new CRM_leadInfoPageOptions();
				leadinfo.addToGroupOnInfoPage(driver);
				leadinfo.addToNewGroupbtnOnIframe(driver);
				CRM_CreateGroupPage Group = new CRM_CreateGroupPage();
				CRM_CreateGroupGetSet ginfo = new CRM_CreateGroupGetSet();
				ginfo = crm.crm_common().DefualtGroupinfo(ginfo);
				Group.fillGroup(driver, ginfo);
				String groupName = ginfo.getGroupName(); 
				Group.associatebtn_AddGroup(driver);
			
		
			 crm.crm_common().openCRMCampaignCenterPage(fcHomePageTest, driver);
			CRM_CampaginCenterPagelink add = new CRM_CampaginCenterPagelink();
			add.CreateCampaign(driver);
			CRM_CampaignAddpage addfill = new CRM_CampaignAddpage();
			CRM_CampaignAddGetSet camAdd = new CRM_CampaignAddGetSet();
			camAdd = crm.crm_common().defualtCampaignfill(camAdd);
			
			addfill.filldetails(driver, camAdd);
			addfill.clickStartbtn(driver);
			Thread.sleep(500);
			
			CRM_SelectandCustomizeTemplatepage custom = new CRM_SelectandCustomizeTemplatepage();
			
			
			custom.firstTemplateclick(driver);
			Thread.sleep(500);
			
			
			CRM_TemplateInfoPageGetSet teminfo = new CRM_TemplateInfoPageGetSet();
			CRM_TemplateInfoPage info = new CRM_TemplateInfoPage();
			teminfo = crm.crm_common().defualtTemplateinfofill(teminfo);
			String TemNAme = teminfo.getTemaplateName();
			System.out.println(TemNAme);
			info.filldetails(driver, teminfo);
			info.savebtn(driver);
		
			Thread.sleep(5000);
			info.saveAndcontinue(driver);
			Thread.sleep(500);
			
			
			CRM_CampaignSelectRecipientUI SRUI = new CRM_CampaignSelectRecipientUI(driver);
			
			if(fc.utobj().assertPageSource(driver, groupName)) {
				System.out.println("group name found on the page");
				fc.utobj().clickElement(driver, ".//td[contains(text(),'"+groupName+"')]/ancestor::tr[1]//div");
				
			}
			fc.utobj().clickElement(driver, SRUI.Continuebtn);
			
			
			
			Thread.sleep(500);
			
			CRM_CampaignFinalandLunchpage lunch = new CRM_CampaignFinalandLunchpage();
			CRM_CampaignFinalandLunchUI lunchUI = new CRM_CampaignFinalandLunchUI(driver);
			fc.utobj().selectDropDown(driver, lunchUI.TimeZone, "GMT +05:30 India");
		    String	CampaignName = lunchUI.campaignName.getText();
			System.out.println(CampaignName);
			lunch.clickFinalUpperbtn(driver);
			String confirm = lunchUI.confirmpagetext.getText();
			System.out.println(confirm);
			
			
			if(confirm.equalsIgnoreCase(CampaignName+" "+"has been scheduled successfully"))
			{
				System.out.println("Campaign Add Successfully ");
			}else {
				System.out.println("campaign not added");
			}
			
			
			
				fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
				fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
			}
	}
	
	@Test(groups = "crm")
	@TestCase(createdOn = "2018-05-07", updatedOn = "2018-05-07", testCaseDescription = "CRM > Lead Summary >add lead > select all leads > click on send email button ", testCaseId = "TC-12938_Send Email in Batch for leads")
	public void leadInfopage41() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		
			
		try {
			driver = fc.loginpage().login(driver);
			
			FCHomePageTest fcHomePageTest = fc.commonMethods().getModules();
			CRMModule crm = new CRMModule();
			crm.crm_common().openCRMLeadsPage(fcHomePageTest, driver).clickAddLead(driver);
			
			Leadgetset lead = new Leadgetset();
			lead = crm.crm_common().getDefaultDataForLead(lead);
			Addleadpage addlead = new Addleadpage();
			addlead.fillLeadDetails(driver, lead);
			addlead.save(driver);
			
			FCHomePage Lhome = new FCHomePage(driver);
			fc.utobj().clickElement(driver, Lhome.leadsLink);
			LeadSummaryPage add = new LeadSummaryPage();
			add.clickAddLead(driver);
			lead = crm.crm_common().getDefaultDataForLead(lead);
			addlead.fillLeadDetails(driver, lead);
			addlead.save(driver);
			
			fc.utobj().clickElement(driver, Lhome.leadsLink);
			add.clickAddLead(driver);
			lead = crm.crm_common().getDefaultDataForLead(lead);
			addlead.fillLeadDetails(driver, lead);
			addlead.save(driver);
			
			fc.utobj().clickElement(driver, Lhome.leadsLink);
			add.clickAddLead(driver);
			lead = crm.crm_common().getDefaultDataForLead(lead);
			addlead.fillLeadDetails(driver, lead);
			addlead.save(driver);
			
			
			fc.utobj().clickElement(driver, Lhome.leadsLink);
			
			fc.utobj().clickElement(driver, ".//input[@name = 'allSelect']");
			fc.utobj().printTestStep("Click on Send Email button in Action Menu");
			fc.utobj().selectActionMenuItemsWithTagA(driver, "Send Email");
			CRM_SendEmailGetSet Semail = new CRM_SendEmailGetSet();
			CRM_SendEmailUI em = new CRM_SendEmailUI(driver);
			Semail = crm.crm_common().defualtemailfill(Semail);
			fc.utobj().sendKeys(driver, em.EmailSubject, Semail.getSubject());
			fc.utobj().clickElement(driver, ".//input[@id = 'sendButton2']");
			
			fc.utobj().printTestStep("Send Email Batch operation perform");
			
				fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
				fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
			}
	}
	
	
	@Test(groups = "crm")
	@TestCase(createdOn = "2018-05-07", updatedOn = "2018-05-07", testCaseDescription = "CRM > Lead Summary >add lead > click on export to excel in batch ", testCaseId = "TC-12945_Export to Excel in Batch for leads")
	public void leadInfopage42() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		
			
		try {
			driver = fc.loginpage().login(driver);
			
			FCHomePageTest fcHomePageTest = fc.commonMethods().getModules();
			CRMModule crm = new CRMModule();
			crm.crm_common().openCRMLeadsPage(fcHomePageTest, driver).clickAddLead(driver);
			
			Leadgetset lead = new Leadgetset();
			lead = crm.crm_common().getDefaultDataForLead(lead);
			Addleadpage addlead = new Addleadpage();
			addlead.fillLeadDetails(driver, lead);
			addlead.save(driver);
			
			FCHomePage Lhome = new FCHomePage(driver);
			fc.utobj().clickElement(driver, Lhome.leadsLink);
			LeadSummaryPage add = new LeadSummaryPage();
			add.clickAddLead(driver);
			lead = crm.crm_common().getDefaultDataForLead(lead);
			addlead.fillLeadDetails(driver, lead);
			addlead.save(driver);
			
			fc.utobj().clickElement(driver, Lhome.leadsLink);
			add.clickAddLead(driver);
			lead = crm.crm_common().getDefaultDataForLead(lead);
			addlead.fillLeadDetails(driver, lead);
			addlead.save(driver);
			
			fc.utobj().clickElement(driver, Lhome.leadsLink);
			add.clickAddLead(driver);
			lead = crm.crm_common().getDefaultDataForLead(lead);
			addlead.fillLeadDetails(driver, lead);
			addlead.save(driver);
			
			
			fc.utobj().clickElement(driver, Lhome.leadsLink);
			
			fc.utobj().clickElement(driver, ".//input[@name = 'allSelect']");
			fc.utobj().printTestStep("Click on export to excel button in Action Menu");
			fc.utobj().selectActionMenuItemsWithTagA(driver, "Export As Excel");
			
			fc.utobj().printTestStep("Export to excel Batch operation perform");
			
				fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
				fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
			}
	}
	
	@Test(groups = "crm")
	@TestCase(createdOn = "2018-05-07", updatedOn = "2018-05-07", testCaseDescription = "CRM > Lead Summary >add campagin >add lead > remove from bounce list ", testCaseId = "TC-12949_Remove from bounce in Batch for leads")
	public void leadInfopage43() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		
			
		try {
			driver = fc.loginpage().login(driver);
			
			FCHomePageTest fcHomePageTest = fc.commonMethods().getModules();
			CRMModule crm = new CRMModule();
			crm.crm_common().openCRMLeadsPage(fcHomePageTest, driver).clickAddLead(driver);
			
			Leadgetset lead = new Leadgetset();
			lead = crm.crm_common().getDefaultDataForLead(lead);
			Addleadpage addlead = new Addleadpage();
			addlead.fillLeadDetails(driver, lead);
			addlead.save(driver);
			
			FCHomePage Lhome = new FCHomePage(driver);
			fc.utobj().clickElement(driver, Lhome.leadsLink);
			LeadSummaryPage add = new LeadSummaryPage();
			add.clickAddLead(driver);
			lead = crm.crm_common().getDefaultDataForLead(lead);
			addlead.fillLeadDetails(driver, lead);
			addlead.save(driver);
			
			fc.utobj().clickElement(driver, Lhome.leadsLink);
			add.clickAddLead(driver);
			lead = crm.crm_common().getDefaultDataForLead(lead);
			addlead.fillLeadDetails(driver, lead);
			addlead.save(driver);
			
			fc.utobj().clickElement(driver, Lhome.leadsLink);
			add.clickAddLead(driver);
			lead = crm.crm_common().getDefaultDataForLead(lead);
			addlead.fillLeadDetails(driver, lead);
			addlead.save(driver);
			
			
			fc.utobj().clickElement(driver, Lhome.leadsLink);
			
			fc.utobj().clickElement(driver, ".//input[@name = 'allSelect']");
			fc.utobj().printTestStep("Click on remove from bounce list button in Action Menu");
			fc.utobj().selectActionMenuItemsWithTagA(driver, "Remove From Bounce List");
			
			fc.utobj().printTestStep("Remove from Bouncelist in Batch operation perform");
			
				fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
				fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
			}
	}
	
	@Test(groups = {"crm"})
	@TestCase(createdOn = "2018-05-07", updatedOn = "2018-05-07", testCaseDescription = "CRM > Lead Summary >add campaign > add lead > associate campaign > check remark > click on detail button  ", testCaseId = "TC-13168_campagin association remark and detail check")
	public void leadInfopage44() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		
			
		try {
			driver = fc.loginpage().login(driver);

			FCHomePageTest fcHomePageTest = fc.commonMethods().getModules();
			CRMModule crm = new CRMModule();
			crm.crm_common().openCRMCampaignCenterPage(fcHomePageTest, driver);
			CRM_CampaginCenterPagelink add = new CRM_CampaginCenterPagelink();
			add.CreateCampaign(driver);
			CRM_CampaignAddpage addfill = new CRM_CampaignAddpage();
			CRM_CampaignAddGetSet camAdd = new CRM_CampaignAddGetSet();
			camAdd = crm.crm_common().defualtCampaignfill(camAdd);
			
			addfill.filldetails(driver, camAdd);
			addfill.clickStartbtn(driver);
			Thread.sleep(500);
			
			CRM_SelectandCustomizeTemplatepage custom = new CRM_SelectandCustomizeTemplatepage();
					
			
			custom.firstTemplateclick(driver);
			Thread.sleep(500);
			
			
			CRM_TemplateInfoPageGetSet teminfo = new CRM_TemplateInfoPageGetSet();
			CRM_TemplateInfoPage info = new CRM_TemplateInfoPage();
			teminfo = crm.crm_common().defualtTemplateinfofill(teminfo);
			String TemplateName = teminfo.getTemaplateName();
			info.filldetails(driver, teminfo);
			info.savebtn(driver);
		
			Thread.sleep(500);
			info.saveAndcontinue(driver);
			Thread.sleep(500);
			
			CRM_CampaignSelectRecipientpage recipient = new CRM_CampaignSelectRecipientpage();
			CRM_CampaignSelectRecipientUI recipientUI = new CRM_CampaignSelectRecipientUI(driver);
			recipient.AssociateLater(driver);
			Thread.sleep(500);
			
			CRM_CampaignFinalandLunchpage lunch = new CRM_CampaignFinalandLunchpage();
			CRM_CampaignFinalandLunchUI lunchUI = new CRM_CampaignFinalandLunchUI(driver);
			fc.utobj().selectDropDown(driver, lunchUI.TimeZone, "GMT +05:30 India");
		    String	CampaignName = lunchUI.campaignName.getText();
			
			lunch.clickFinalUpperbtn(driver);
			String confirm = lunchUI.confirmpagetext.getText();
			
			
			
			if(confirm.equalsIgnoreCase(CampaignName+" "+"has been scheduled successfully"))
			{
				System.out.println("Campaign Add Successfully ");
			}else {
				System.out.println("campaign not added");
			}
			
			crm.crm_common().openCRMLeadsPage(fcHomePageTest, driver).clickAddLead(driver);
			Leadgetset lead = new Leadgetset();
			lead = crm.crm_common().getDefaultDataForLead(lead);
			Addleadpage addlead = new Addleadpage();
			addlead.fillLeadDetails(driver, lead);
			addlead.save(driver);
			 CRM_leadInfoPageOptions leadinfo = new CRM_leadInfoPageOptions();
			 leadinfo.AssociatecampOnInfoPage(driver);
			 
			 fc.utobj().printTestStep("click on associate campaign button in fornt of campaign name");
			 fc.utobj().clickElement(driver, recipientUI.getXpathOfCampaignAction(CampaignName));
				fc.utobj().clickElement(driver, recipientUI.confirmOfAssociateWithCampagin);
				String camName = driver.findElement(By.xpath(".//td[contains(text(),'"+CampaignName+"')]")).getText();
				if(camName.equalsIgnoreCase(CampaignName)) {
					System.out.println("campaign present on lead info page");
					fc.utobj().printTestStep("Campaign associated with lead");
			          Thread.sleep(6000);
			          fc.utobj().refresh(driver);
			          fc.utobj().refresh(driver);
			          System.out.println("after 4 min wait");
			          String Total_mailsent = driver.findElement(By.xpath("//td[contains(text(),'Total Mails Sent :')]/ancestor::tr[1]/td[4]")).getText();
			          System.out.println(Total_mailsent);
			          if(Total_mailsent.length()>0) {
			        	  fc.utobj().printTestStep("total sent email count increase");
			        	  System.out.println("total sent email count increase");
			        	  fc.utobj().clickElement(driver, "//td[contains(text(),'Total Mails Sent :')]/ancestor::td[1]//td[4]/a or //font[@color='#004899']");   //font[@color='#004899']
			        	  fc.commonMethods().switch_cboxIframe_frameId(driver);
			        	  fc.utobj().printTestStep("Switch to Email detail page");
			        	  System.out.println("swtich");
			        	  CRM_LeadinfopageUI mod = new CRM_LeadinfopageUI(driver);
			        	  fc.utobj().clickElement(driver, mod.Assignpage_closebtn);
			        	  
			        	  String Older = driver.findElement(By.xpath(".//td[contains(text(),'Older')]/ancestor::tr[1]/td[2]")).getText();
			        	  if(Older!=null) {
			        		  System.out.println("campaign switch to older promtion field");
			        	  }
			        	  
			          }
			 
				}else {
					System.out.println("campaign not present on lead info page");
				}
				CRM_LeadinfopageUI mod = new CRM_LeadinfopageUI(driver);
				String activehst = mod.Activity_History.getText();
				if(activehst.contains(TemplateName)) {
					System.out.println("Remark generated");
				}
			 
			 
			 
				fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
				fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
			}
	}
	
	@Test(groups = {"crm"})
	@TestCase(createdOn = "2018-05-07", updatedOn = "2018-05-07", testCaseDescription = "CRM > Lead Summary >add workflow > trigger workflow at the time of lead add ", testCaseId = "TC-13002_associate standard workflow")
	public void leadInfopage45() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

			
		try {
			driver = fc.loginpage().login(driver);

			FCHomePageTest fcHomePageTest = fc.commonMethods().getModules();
			CRMModule crm = new CRMModule();
			
			String StatusName = fc.utobj().generateTestData("selenium");
			crm.crm_common().AdminCRMStatus_change(driver, StatusName, "Leads");
			crm.crm_common().openCRMWorkflowsPage(fcHomePageTest, driver);
			CRM_CreateWorkFlowPage flow = new CRM_CreateWorkFlowPage();
			flow.Create_Workflowbtn(driver);
			flow.filldetails(driver, "Lead", "Standard");
			flow.Nextbtn_onWorkflow(driver);
			CRM_CreateWorkFlowPageUI workUI= new CRM_CreateWorkFlowPageUI(driver);
			fc.utobj().clickElement(driver, workUI.Nextbtn_trigger);
			fc.utobj().clickElement(driver, workUI.Nextbtn_condition);
			fc.utobj().clickElement(driver, workUI.Condition_Actionbtn);
			fc.utobj().clickElement(driver, workUI.Change_Status);
			fc.utobj().sendKeys(driver, workUI.Status_field, StatusName);
			fc.utobj().clickElement(driver, workUI.Save_onStatus);   
			fc.utobj().clickElement(driver, workUI.Workflow_addbtn);
			
			crm.crm_common().openCRMLeadsPage(fcHomePageTest, driver).clickAddLead(driver);
			Leadgetset lead = new Leadgetset();
			lead = crm.crm_common().getDefaultDataForLead(lead);

			Addleadpage addlead = new Addleadpage();
			addlead.fillLeadDetails(driver, lead);
			addlead.save(driver);
			
			Thread.sleep(2000);
			fc.utobj().refresh(driver);
			
			CRM_LeadinfopageUI mod = new CRM_LeadinfopageUI(driver);
			fc.utobj().clickElement(driver, mod.DetailedHistory);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().clickElement(driver, mod.DetailedHistory_StatusChange);
			String Remark = driver.findElement(By.id("div4")).getText();
			if(Remark.contains(StatusName)) {
				System.out.println("change workflow trigger");
				fc.utobj().clickElement(driver, ".//input[@value = 'Close']");
			}
			
				fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
				fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
			}
	}
	
	
	@Test(groups = {"crm"})
	@TestCase(createdOn = "2018-05-07", updatedOn = "2018-05-07", testCaseDescription = "CRM > Lead Summary >add campagin >add lead and associate campaign > add another lead without email > merge both of them ", testCaseId = "TC-13245_Merge lead and lead owner dont have emails ")
	public void leadInfopage46() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		
			
		try {
			driver = fc.loginpage().login(driver);

			FCHomePageTest fcHomePageTest = fc.commonMethods().getModules();
			CRMModule crm = new CRMModule();
			
			crm.crm_common().openCRMCampaignCenterPage(fcHomePageTest, driver);
			CRM_CampaginCenterPagelink add = new CRM_CampaginCenterPagelink();
			add.CreateCampaign(driver);
			CRM_CampaignAddpage addfill = new CRM_CampaignAddpage();
			CRM_CampaignAddGetSet camAdd = new CRM_CampaignAddGetSet();
			camAdd = crm.crm_common().defualtCampaignfill(camAdd);
			
			addfill.filldetails(driver, camAdd);
			addfill.clickStartbtn(driver);
			Thread.sleep(1000);
			
			CRM_SelectandCustomizeTemplatepage custom = new CRM_SelectandCustomizeTemplatepage();
			
			custom.firstTemplateclick(driver);
			Thread.sleep(2000);
			
			
			CRM_TemplateInfoPageGetSet teminfo = new CRM_TemplateInfoPageGetSet();
			CRM_TemplateInfoPage info = new CRM_TemplateInfoPage();
			teminfo = crm.crm_common().defualtTemplateinfofill(teminfo);
			
			info.filldetails(driver, teminfo);
			info.savebtn(driver);
		
			Thread.sleep(2000);
			info.saveAndcontinue(driver);
			Thread.sleep(1000);
			
			CRM_CampaignSelectRecipientpage recipient = new CRM_CampaignSelectRecipientpage();
			CRM_CampaignSelectRecipientUI recipientUI = new CRM_CampaignSelectRecipientUI(driver);
			recipient.AssociateLater(driver);
			Thread.sleep(500);
			
			CRM_CampaignFinalandLunchpage lunch = new CRM_CampaignFinalandLunchpage();
			CRM_CampaignFinalandLunchUI lunchUI = new CRM_CampaignFinalandLunchUI(driver);
			fc.utobj().selectDropDown(driver, lunchUI.TimeZone, "GMT +05:30 India");
		    String	CampaignName = lunchUI.campaignName.getText();
			System.out.println(CampaignName);
			lunch.clickFinalUpperbtn(driver);
			String confirm = lunchUI.confirmpagetext.getText();
			System.out.println(confirm);
			
			
			if(confirm.equalsIgnoreCase(CampaignName+" "+"has been scheduled successfully"))
			{
				System.out.println("Campaign Add Successfully ");
			}else {
				System.out.println("campaign not added");
			}
			
			crm.crm_common().openCRMLeadsPage(fcHomePageTest, driver).clickAddLead(driver);
			Leadgetset lead = new Leadgetset();
			lead = crm.crm_common().getDefaultDataForLead(lead);
			Addleadpage addlead = new Addleadpage();
			addlead.fillLeadDetails(driver, lead);
			addlead.save(driver);
			String FirstLead = lead.getFirstname()+" "+lead.getLastname();
			 CRM_leadInfoPageOptions leadinfo = new CRM_leadInfoPageOptions();
			 leadinfo.AssociatecampOnInfoPage(driver);
			 Thread.sleep(1000);


			 fc.utobj().clickElement(driver, ".//button[@id='backConfirmation' or contains(text(),'Back')]");

			 Thread.sleep(1000);
			 
			 leadinfo.AssociatecampOnInfoPage(driver);


			fc.utobj().sleep(1000);

			fc.utobj().clickElement(driver, recipientUI.getXpathOfCampaignAction(CampaignName));
			fc.utobj().clickElement(driver, recipientUI.confirmOfAssociateWithCampagin);
			
			FCHomePage Lhome = new FCHomePage(driver);
			fc.utobj().clickElement(driver, Lhome.leadsLink);
			LeadSummaryPage sum = new LeadSummaryPage();
			sum.clickAddLead(driver);
			lead = crm.crm_common().getDefaultDataForLead(lead);
			lead.setFirstname("Harsh");
			String SecondLead = lead.getFirstname()+" "+lead.getLastname();
			lead.setPrimarycontact("");
			lead.setEmail("");
			lead.setAlternarteemail("");
			addlead.fillLeadDetails(driver, lead);
			addlead.save(driver);
			
			Thread.sleep(4000);
			 
			fc.utobj().clickElement(driver, Lhome.leadsLink);
			fc.utobj().clickElement(driver, ".//a[contains(text(),'"+FirstLead+"')]/ancestor::tr[1]//input[@type='checkbox']");
			fc.utobj().clickElement(driver, ".//a[contains(text(),'"+SecondLead+"')]/ancestor::tr[1]//input[@type='checkbox']");
			CRM_LeadinfopageUI mod = new CRM_LeadinfopageUI(driver);
			fc.utobj().selectActionMenuItemsWithTagA(driver, "Merge Leads");
			fc.utobj().clickElement(driver, ".//span/input[@value = 'Merge']");
			
			String activehst = mod.Activity_History.getText();
			
			if(activehst.contains("Leads added through merging of two Leads"))
			{
				System.out.println("Merge Remark created");
			}
			 String older = driver.findElement(By.xpath(".//td[contains(text(),'Older')]/ancestor::tr[1]/td[2]")).getText();
			 if(older.contains(CampaignName)) {
				 System.out.println("campaign carryed");
			 }else {
				 System.out.println("Campagin not carried ");
			 }
			
			
			 
				fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
				fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
			}
	}
	

	@Test(groups = {"crm"})
	@TestCase(createdOn = "2018-05-07", updatedOn = "2018-05-07", testCaseDescription = "CRM > Lead Summary >add campaign >add lead and associate campagin >add lead again make it opt-out and merge lead to check campaign    ", testCaseId = "TC-13246_Merge lead and lead owner is optout emails ")
	public void leadInfopage47() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		
			
		try {
			driver = fc.loginpage().login(driver);

			FCHomePageTest fcHomePageTest = fc.commonMethods().getModules();
			CRMModule crm = new CRMModule();
			
			crm.crm_common().openCRMCampaignCenterPage(fcHomePageTest, driver);
			CRM_CampaginCenterPagelink add = new CRM_CampaginCenterPagelink();
			add.CreateCampaign(driver);
			CRM_CampaignAddpage addfill = new CRM_CampaignAddpage();
			CRM_CampaignAddGetSet camAdd = new CRM_CampaignAddGetSet();
			camAdd = crm.crm_common().defualtCampaignfill(camAdd);
			
			addfill.filldetails(driver, camAdd);
			addfill.clickStartbtn(driver);
			Thread.sleep(500);
			
			CRM_SelectandCustomizeTemplatepage custom = new CRM_SelectandCustomizeTemplatepage();
			
			custom.firstTemplateclick(driver);
			Thread.sleep(5000);
			
			
			CRM_TemplateInfoPageGetSet teminfo = new CRM_TemplateInfoPageGetSet();
			CRM_TemplateInfoPage info = new CRM_TemplateInfoPage();
			teminfo = crm.crm_common().defualtTemplateinfofill(teminfo);
			
			info.filldetails(driver, teminfo);
			info.savebtn(driver);
		
			Thread.sleep(5000);
			info.saveAndcontinue(driver);
			Thread.sleep(500);
			
			CRM_CampaignSelectRecipientpage recipient = new CRM_CampaignSelectRecipientpage();
			CRM_CampaignSelectRecipientUI recipientUI = new CRM_CampaignSelectRecipientUI(driver);
			recipient.AssociateLater(driver);
			Thread.sleep(500);
			
			CRM_CampaignFinalandLunchpage lunch = new CRM_CampaignFinalandLunchpage();
			CRM_CampaignFinalandLunchUI lunchUI = new CRM_CampaignFinalandLunchUI(driver);
			fc.utobj().selectDropDown(driver, lunchUI.TimeZone, "GMT +05:30 India");
		    String	CampaignName = lunchUI.campaignName.getText();
			System.out.println(CampaignName);
			lunch.clickFinalUpperbtn(driver);
			String confirm = lunchUI.confirmpagetext.getText();
			System.out.println(confirm);
			
			
			if(confirm.equalsIgnoreCase(CampaignName+" "+"has been scheduled successfully"))
			{
				System.out.println("Campaign Add Successfully ");
			}else {
				System.out.println("campaign not added");
			}
			
			crm.crm_common().openCRMLeadsPage(fcHomePageTest, driver).clickAddLead(driver);
			Leadgetset lead = new Leadgetset();
			lead = crm.crm_common().getDefaultDataForLead(lead);
			Addleadpage addlead = new Addleadpage();
			addlead.fillLeadDetails(driver, lead);
			addlead.save(driver);
			String FirstLead = lead.getFirstname()+" "+lead.getLastname();
			 CRM_leadInfoPageOptions leadinfo = new CRM_leadInfoPageOptions();
			 leadinfo.AssociatecampOnInfoPage(driver);

			 Thread.sleep(1000);
	
			 fc.utobj().clickElement(driver, ".//button[@id='backConfirmation' or contains(text(),'Back')]");
			 fc.utobj().sleep(1000);

			 
			 leadinfo.AssociatecampOnInfoPage(driver);


			fc.utobj().sleep(1000);

			 fc.utobj().sleep(1000);


			fc.utobj().clickElement(driver, recipientUI.getXpathOfCampaignAction(CampaignName));
			fc.utobj().clickElement(driver, recipientUI.confirmOfAssociateWithCampagin);
			
			FCHomePage Lhome = new FCHomePage(driver);
			fc.utobj().clickElement(driver, Lhome.leadsLink);
			LeadSummaryPage sum = new LeadSummaryPage();
			sum.clickAddLead(driver);
			lead = crm.crm_common().getDefaultDataForLead(lead);
			lead.setFirstname("Harsh");
			String SecondLead = lead.getFirstname()+" "+lead.getLastname();
			lead.setPrimarycontact("");
			lead.setEmail("akshit.batra@franconnect.com");
			lead.setAlternarteemail("rohit@franconnect.com");
			addlead.fillLeadDetails(driver, lead);
			addlead.save(driver);
			 LeadUI leadui = new LeadUI(driver);
			 leadinfo.modifyLeadOnInfopage(driver);
			 fc.utobj().clickElement(driver, leadui.opt_out);
			 fc.utobj().acceptAlertBox(driver);
			 addlead.save(driver);
			
			Thread.sleep(4000);
			 
			fc.utobj().clickElement(driver, Lhome.leadsLink);
			fc.utobj().clickElement(driver, ".//a[contains(text(),'"+FirstLead+"')]/ancestor::tr[1]//input[@type='checkbox']");
			fc.utobj().clickElement(driver, ".//a[contains(text(),'"+SecondLead+"')]/ancestor::tr[1]//input[@type='checkbox']");
			CRM_LeadinfopageUI mod = new CRM_LeadinfopageUI(driver);
			fc.utobj().selectActionMenuItemsWithTagA(driver, "Merge Leads");
			fc.utobj().clickElement(driver, ".//span/input[@value = 'Merge']");
			
			String activehst = mod.Activity_History.getText();
			
			if(activehst.contains("Leads added through merging of two Leads"))
			{
				System.out.println("Merge Remark created");
			}
			 String older = driver.findElement(By.xpath(".//td[contains(text(),'Older')]/ancestor::tr[1]/td[2]")).getText();
			 if(older.contains(CampaignName)) {
				 System.out.println("campaign carryed");
			 }else {
				 System.out.println("Campagin not carried ");
			 }
			
			
			 
				fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
				fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
			}
	}
	
	
	@Test(groups = {"crm"})
	@TestCase(createdOn = "2018-05-07", updatedOn = "2018-05-07", testCaseDescription = "CRM > Lead Summary >add duplicates leads > filter leads on the basis of first name duplicate and verify ", testCaseId = "TC-12958_Filter_duplicate_leads ")
	public void leadInfopage48() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		
			
		try {
			driver = fc.loginpage().login(driver);

			FCHomePageTest fcHomePageTest = fc.commonMethods().getModules();
			CRMModule crm = new CRMModule();
			
			crm.crm_common().openCRMLeadsPage(fcHomePageTest, driver).clickAddLead(driver);
			Leadgetset lead = new Leadgetset();
			lead = crm.crm_common().getDefaultDataForLead(lead);
			String leadfirst = lead.getFirstname()+" "+lead.getLastname();
			Addleadpage addlead = new Addleadpage();
			addlead.fillLeadDetails(driver, lead);
			addlead.save(driver);
			FCHomePage pobj = new FCHomePage(driver);
			fcHomePageTest.utobj().clickElement(driver, pobj.leadsLink);
			LeadSummaryPage summ = new LeadSummaryPage();
			summ.clickAddLead(driver);
			addlead.fillLeadDetails(driver, lead);
			addlead.save(driver);
			
			fcHomePageTest.utobj().clickElement(driver, pobj.leadsLink);
			summ.clickAddLead(driver);
			lead = crm.crm_common().getDefaultDataForLead(lead);
			addlead.fillLeadDetails(driver, lead);
			addlead.save(driver);
			fcHomePageTest.utobj().clickElement(driver, pobj.leadsLink);
			LeadsummarypageUI ui = new LeadsummarypageUI(driver);
			fc.utobj().clickElement(driver, ui.ShowFilter);
			fc.utobj().selectDropDown(driver, ui.View_leads, "Duplicate Leads");
			fc.utobj().clickElement(driver, ui.Duplicate_criteria);
			fc.utobj().clickElement(driver, ui.Duplicate_criteria_firstName);
			fc.utobj().clickElement(driver, ui.Search_btn);
			fc.utobj().clickElement(driver, ui.Save_View);
			
			List<WebElement> elements = driver.findElements(By.xpath(".//a[text() = '" + leadfirst + "']"));
			int count = elements.size();
			System.out.println(count);
			if(count > 1) {
				System.out.println("Duplicate leads present on the lead summary page");
			}
			
			fcHomePageTest.utobj().clickElement(driver, pobj.leadsLink);
			
			
				fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
				fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
			}
	}
	
	
	@Test(groups = {"crm"})
	@TestCase(createdOn = "2018-05-07", updatedOn = "2018-05-07", testCaseDescription = "CRM > Lead Summary >add lead type when add lead ", testCaseId = "TC-12968_Add_Lead type when add lead ")
	public void leadInfopage49() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		
			
		try {
			driver = fc.loginpage().login(driver);

			FCHomePageTest fcHomePageTest = fc.commonMethods().getModules();
			CRMModule crm = new CRMModule();
			
			AdminCRMConfigureLeadTypePageTest type = new AdminCRMConfigureLeadTypePageTest();
			String LeadType = fc.utobj().generateTestData("SeleType");
			type.addLeadType(driver, LeadType);
			crm.crm_common().openCRMLeadsPage(fcHomePageTest, driver).clickAddLead(driver);
			Leadgetset lead = new Leadgetset();
			lead = crm.crm_common().getDefaultDataForLead(lead);
			Addleadpage addlead = new Addleadpage();
			addlead.fillLeadDetails(driver, lead);
			LeadUI leadui = new LeadUI(driver);
			fc.utobj().selectDropDown(driver, leadui.LeadType, LeadType);
			addlead.save(driver);
			
			
				fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
				fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
			}
	}
	
}
