package com.builds.test.crm;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.xml.parsers.ParserConfigurationException;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.testng.Reporter;
import org.testng.annotations.Test;
import org.xml.sax.SAXException;

import com.builds.test.admin.AdminUsersManageCorporateUsersAddCorporateUserPageTest;
import com.builds.test.common.CorporateUser;
import com.builds.uimaps.campaigncentercrm.CRMCampaignCenterCampaignsPage;
import com.builds.uimaps.crm.CRMLeadsPage;
import com.builds.utilities.FranconnectUtil;
import com.builds.utilities.TestCase;

public class CRMLead_AreaCampaignTestCase {

	FranconnectUtil fc = new FranconnectUtil();

	@Test(groups = { "crm", "crmCheck112"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2017-06-26", testCaseDescription = "Verify The Lead Associate with email campaign By Action Button Option At CRM > Lead > Lead Summary", testCaseId = "TC_73_Associate_With_Email_Campaign")
	public void AssociateWithEmailCompaignActionBtn()
			throws IOException, Exception, ParserConfigurationException, SAXException {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			CRMLeadsPage pobj = new CRMLeadsPage(driver);

			fc.utobj().printTestStep("Navigate To CRM >Campaign Center");
			fc.utobj().printTestStep("Add Campaign");
			CRMCampaignCenterPageTestForCRM campaignCenterPage = new CRMCampaignCenterPageTestForCRM();
			String campaignName = fc.utobj().generateTestData(dataSet.get("campaignName"));
			String campaignDescription = fc.utobj().generateTestData(dataSet.get("campaignDescription"));
			String campaignAccessibility = dataSet.get("campaignAccessibility");
			String category = dataSet.get("category");
			String templateName = fc.utobj().generateTestData(dataSet.get("templateName"));
			String mailSubject = dataSet.get("mailSubject");
			String templateText = fc.utobj().generateTestData(dataSet.get("templateText"));
			campaignCenterPage.createCampaign(driver, campaignName, campaignDescription, campaignAccessibility,
					category, templateName, mailSubject, templateText, config);

			fc.utobj().printTestStep("Navigate To Admin Users Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			String emailId = "crmautomation@staffex.com";
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUser = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporateUser.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			String firstName = fc.utobj().generateTestData(dataSet.get("firstName"));
			String lastName = fc.utobj().generateTestData(dataSet.get("lastName"));
			String company = fc.utobj().generateTestData(dataSet.get("company"));
			String assignTo = "Corporate";
			String city = fc.utobj().generateTestData(dataSet.get("city"));
			String address = fc.utobj().generateTestData(dataSet.get("address"));
			String suffix = fc.utobj().generateTestData(dataSet.get("suffix"));
			String jobTitle = fc.utobj().generateTestData(dataSet.get("jobTitle"));
			String comment = fc.utobj().generateTestData(dataSet.get("comment"));
			String franchiseId = "";
			String franchiseUser = "";
			String userNameReg = "";
			String regionName = "";
			fc.utobj().printTestStep("Navigate To CRM > Leads > Leads Summary");
			fc.utobj().printTestStep("Add Lead");
			addLead(driver, dataSet, firstName, lastName, company, assignTo, city, address, suffix, jobTitle, comment,
					corpUser.getuserFullName(), regionName, userNameReg, franchiseId, franchiseUser, config);

			fc.utobj().clickElement(driver, pobj.leadsLink);
			searchLeadByOwner(driver, corpUser.getuserFullName(), "Open Leads");
			fc.utobj().printTestStep("Associate With Campaign");
			fc.utobj().sleep(2000);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,(".//a[contains(text () ,'" + firstName + " " + lastName + "')]/ancestor::tr/td/input")));
			try {
				fc.utobj().selectActionMenuItemsWithTagA(driver, "Associate With Campaign");
			} catch (Exception e) {
				fc.utobj().selectActionMenuItemsWithTagA(driver, "Associate with Campaign");
			}
			fc.utobj().clickElement(driver, pobj.filterContainer);
			fc.utobj().sendKeys(driver, pobj.searchCampaign, campaignName);
			fc.utobj().clickElement(driver, pobj.applyFilter);

			fc.utobj().clickElement(driver, pobj.sendCampaignBtn);

			fc.utobj().printTestStep("Verify Back / Cancel Button in confrimation page. ");
			boolean isLeadBackButtonPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//*[@id='cancelConfirmation']");
			if (isLeadBackButtonPresent == false) {
				fc.utobj().throwsException("was not able to verify Cancel button is appearing properly.");
			} else {
				fc.utobj().printTestStep("verified that the Cancel button is appearing properly.");
			}
			isLeadBackButtonPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//*[@id='backConfirmation']");
			if (isLeadBackButtonPresent == false) {
				fc.utobj().throwsException("was not able to verify Back button is appearing properly.");
			} else {
				fc.utobj().printTestStep("verified that the Back button is appearing properly.");
			}

			fc.utobj().clickElement(driver, pobj.confirmBtn);
			fc.utobj().clickElement(driver, pobj.leadsLink);

			/*
			 * fc.utobj().sendKeys(driver, pobj.searchSystem, firstName);
			 * fc.utobj().clickElement(driver, pobj.systemSearchButton);
			 */
			fc.utobj().sleep(2000);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//a[contains(text () ,'" + firstName + " " + lastName + "')]"));

			fc.utobj().printTestStep("Verify Lead Assocaite With Campaign");
			boolean isCampaignNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () ,'Promotional Campaign')]/following-sibling::td[contains(text () ,'"
							+ campaignName + "')]");
			if (isCampaignNamePresent == false) {
				fc.utobj().throwsException("was not able to verify Campign Name");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "crm", "crmcheck22" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2017-06-27", testCaseDescription = "Verify the  Associate with email campaign At CRM > Lead > Lead Info", testCaseId = "TC_94_Associate_With_Email_Campaign")
	public void associateWithEmailCampaignLink()
			throws IOException, Exception, ParserConfigurationException, SAXException {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			CRMLeadsPage pobj = new CRMLeadsPage(driver);

			fc.utobj().printTestStep("Navigate To CRM >Campaign Center");
			fc.utobj().printTestStep("Add Campaign");
			CRMCampaignCenterPageTestForCRM campaignCenterPage = new CRMCampaignCenterPageTestForCRM();
			String campaignName = fc.utobj().generateTestData(dataSet.get("campaignName"));
			String campaignDescription = fc.utobj().generateTestData(dataSet.get("campaignDescription"));
			String campaignAccessibility = dataSet.get("campaignAccessibility");
			String category = dataSet.get("category");
			String templateName = fc.utobj().generateTestData(dataSet.get("templateName"));
			String mailSubject = dataSet.get("mailSubject");
			String templateText = fc.utobj().generateTestData(dataSet.get("templateText"));
			campaignCenterPage.createCampaign(driver, campaignName, campaignDescription, campaignAccessibility,
					category, templateName, mailSubject, templateText, config);

			fc.utobj().printTestStep("Navigate To Admin Users Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			String emailId = "crmautomation@staffex.com";
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUser = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporateUser.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			String firstName = fc.utobj().generateTestData(dataSet.get("firstName"));
			String lastName = fc.utobj().generateTestData(dataSet.get("lastName"));
			String company = fc.utobj().generateTestData(dataSet.get("company"));
			String assignTo = "Corporate";
			String city = fc.utobj().generateTestData(dataSet.get("city"));
			String address = fc.utobj().generateTestData(dataSet.get("address"));
			String suffix = fc.utobj().generateTestData(dataSet.get("suffix"));
			String jobTitle = fc.utobj().generateTestData(dataSet.get("jobTitle"));
			String comment = fc.utobj().generateTestData(dataSet.get("comment"));
			String franchiseId = "";
			String franchiseUser = "";
			String userNameReg = "";
			String regionName = "";
			fc.utobj().printTestStep("Navigate To CRM > Leads > Leads Summary");
			fc.utobj().printTestStep("Add Lead");
			addLead(driver, dataSet, firstName, lastName, company, assignTo, city, address, suffix, jobTitle, comment,
					corpUser.getuserFullName(), regionName, userNameReg, franchiseId, franchiseUser, config);

			fc.utobj().clickElement(driver, pobj.leadsLink);
			searchLeadByOwner(driver, corpUser.getuserFullName(), "Open Leads");
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//a[contains(text () ,'" + firstName + " " + lastName + "')]"));

			fc.utobj().printTestStep("Associate With Campaign");
			fc.utobj().clickElement(driver, pobj.campaignLinkAtLeadInfo);
			fc.utobj().clickElement(driver, pobj.filterContainer);
			fc.utobj().sendKeys(driver, pobj.searchCampaign, campaignName);
			fc.utobj().clickElement(driver, pobj.applyFilter);

			fc.utobj().clickElement(driver, pobj.sendCampaignBtn);
			fc.utobj().clickElement(driver, pobj.confirmBtn);
			fc.utobj().clickElement(driver, pobj.leadsLink);
			searchLeadByOwner(driver, corpUser.getuserFullName(), "Open Leads");
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//a[contains(text () ,'" + firstName + " " + lastName + "')]"));

			fc.utobj().printTestStep("Verify Associate With Campaign");
			boolean isCampaignNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () ,'Promotional Campaign')]/following-sibling::td[contains(text () ,'"
							+ campaignName + "')]");
			if (isCampaignNamePresent == false) {
				fc.utobj().throwsException("was not able to verify Campign Name");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "crm"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2017-06-16", testCaseDescription = "Verify the Associate with email campaign At CRM > Lead > Lead Info", testCaseId = "TC_97_Associate_With_Email_Campaign")
	public void associateWithCampaignBottomBtnLI()
			throws IOException, Exception, ParserConfigurationException, SAXException {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			CRMLeadsPage pobj = new CRMLeadsPage(driver);

			fc.utobj().printTestStep("Navigate To CRM >Campaign Center");
			fc.utobj().printTestStep("Add Campaign");
			CRMCampaignCenterPageTestForCRM campaignCenterPage = new CRMCampaignCenterPageTestForCRM();
			String campaignName = fc.utobj().generateTestData(dataSet.get("campaignName"));
			String campaignDescription = fc.utobj().generateTestData(dataSet.get("campaignDescription"));
			String campaignAccessibility = dataSet.get("campaignAccessibility");
			String category = dataSet.get("category");
			String templateName = fc.utobj().generateTestData(dataSet.get("templateName"));
			String mailSubject = dataSet.get("mailSubject");
			String templateText = fc.utobj().generateTestData(dataSet.get("templateText"));
			campaignCenterPage.createCampaign(driver, campaignName, campaignDescription, campaignAccessibility,
					category, templateName, mailSubject, templateText, config);

			fc.utobj().printTestStep("Navigate To Admin Users Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			String emailId = "crmautomation@staffex.com";
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUser = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporateUser.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			String firstName = fc.utobj().generateTestData(dataSet.get("firstName"));
			String lastName = fc.utobj().generateTestData(dataSet.get("lastName"));
			String company = fc.utobj().generateTestData(dataSet.get("company"));
			String assignTo = "Corporate";
			String city = fc.utobj().generateTestData(dataSet.get("city"));
			String address = fc.utobj().generateTestData(dataSet.get("address"));
			String suffix = fc.utobj().generateTestData(dataSet.get("suffix"));
			String jobTitle = fc.utobj().generateTestData(dataSet.get("jobTitle"));
			String comment = fc.utobj().generateTestData(dataSet.get("comment"));
			String franchiseId = "";
			String franchiseUser = "";
			String userNameReg = "";
			String regionName = "";
			fc.utobj().printTestStep("Navigate To CRM > Leads > Leads Summary");
			fc.utobj().printTestStep("Add Lead");

			addLead(driver, dataSet, firstName, lastName, company, assignTo, city, address, suffix, jobTitle, comment,
					corpUser.getuserFullName(), regionName, userNameReg, franchiseId, franchiseUser, config);

			fc.utobj().clickElement(driver, pobj.leadsLink);
			searchLeadByOwner(driver, corpUser.getuserFullName(), "Open Leads");
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//a[contains(text () ,'" + firstName + " " + lastName + "')]"));

			fc.utobj().printTestStep("Associate With Campaign");
			fc.utobj().clickElement(driver, pobj.AssociateWithRegularCampaignButton);
			fc.utobj().clickElement(driver, pobj.filterContainer);
			fc.utobj().sendKeys(driver, pobj.searchCampaign, campaignName);
			fc.utobj().clickElement(driver, pobj.applyFilter);

			fc.utobj().clickElement(driver, pobj.sendCampaignBtn);
			fc.utobj().clickElement(driver, pobj.confirmBtn);
			fc.utobj().clickElement(driver, pobj.leadsLink);
			searchLeadByOwner(driver, corpUser.getuserFullName(), "Open Leads");
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//a[contains(text () ,'" + firstName + " " + lastName + "')]"));

			fc.utobj().printTestStep("Verify Associate With Campaign");
			boolean isCampaignNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () ,'Promotional Campaign')]/following-sibling::td[contains(text () ,'"
							+ campaignName + "')]");
			if (isCampaignNamePresent == false) {
				fc.utobj().throwsException("was not able to verify Campign Name");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "crm" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify the Associate with email campaign by More Action Btn :: CRM > Lead > Lead Info", testCaseId = "TC_86_Associate_With_Email_Campaign")
	public void associateWithEmailCampaignMoreActionBtn()
			throws IOException, Exception, ParserConfigurationException, SAXException {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			CRMLeadsPage pobj = new CRMLeadsPage(driver);

			fc.utobj().printTestStep("Navigate To CRM >Campaign Center");
			fc.utobj().printTestStep("Add Campaign");
			CRMCampaignCenterPageTestForCRM campaignCenterPage = new CRMCampaignCenterPageTestForCRM();
			String campaignName = fc.utobj().generateTestData(dataSet.get("campaignName"));
			String campaignDescription = fc.utobj().generateTestData(dataSet.get("campaignDescription"));
			String campaignAccessibility = dataSet.get("campaignAccessibility");
			String category = dataSet.get("category");
			String templateName = fc.utobj().generateTestData(dataSet.get("templateName"));
			String mailSubject = dataSet.get("mailSubject");
			String templateText = fc.utobj().generateTestData(dataSet.get("templateText"));
			campaignCenterPage.createCampaign(driver, campaignName, campaignDescription, campaignAccessibility,
					category, templateName, mailSubject, templateText, config);

			fc.utobj().printTestStep("Navigate To Admin Users Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			String emailId = "crmautomation@staffex.com";
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUser = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporateUser.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			String firstName = fc.utobj().generateTestData(dataSet.get("firstName"));
			String lastName = fc.utobj().generateTestData(dataSet.get("lastName"));
			String company = fc.utobj().generateTestData(dataSet.get("company"));
			String assignTo = "Corporate";
			String city = fc.utobj().generateTestData(dataSet.get("city"));
			String address = fc.utobj().generateTestData(dataSet.get("address"));
			String suffix = fc.utobj().generateTestData(dataSet.get("suffix"));
			String jobTitle = fc.utobj().generateTestData(dataSet.get("jobTitle"));
			String comment = fc.utobj().generateTestData(dataSet.get("comment"));
			String franchiseId = "";
			String franchiseUser = "";
			String userNameReg = "";
			String regionName = "";

			fc.utobj().printTestStep("Navigate To CRM > Leads > Leads Summary");
			fc.utobj().printTestStep("Add Lead");

			addLead(driver, dataSet, firstName, lastName, company, assignTo, city, address, suffix, jobTitle, comment,
					corpUser.getuserFullName(), regionName, userNameReg, franchiseId, franchiseUser, config);

			fc.utobj().clickElement(driver, pobj.leadsLink);
			searchLeadByOwner(driver, corpUser.getuserFullName(), "Open Leads");
			fc.utobj().printTestStep("Associate With Campaign");

			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//a[contains(text () ,'" + firstName + " " + lastName + "')]"));

			fc.utobj().selectActionMenuItemsWithTagAMoreActions(driver, "Associate with Campaign");
			fc.utobj().clickElement(driver, pobj.filterContainer);
			fc.utobj().sendKeys(driver, pobj.searchCampaign, campaignName);
			fc.utobj().clickElement(driver, pobj.applyFilter);

			fc.utobj().clickElement(driver, pobj.sendCampaignBtn);
			fc.utobj().clickElement(driver, pobj.confirmBtn);
			fc.utobj().clickElement(driver, pobj.leadsLink);
			searchLeadByOwner(driver, corpUser.getuserFullName(), "Open Leads");
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//a[contains(text () ,'" + firstName + " " + lastName + "')]"));

			fc.utobj().printTestStep("Verify Associate With Campaign");
			boolean isCampaignNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () ,'Promotional Campaign')]/following-sibling::td[contains(text () ,'"
							+ campaignName + "')]");
			if (isCampaignNamePresent == false) {
				fc.utobj().throwsException("was not able to verify Campign Name");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "crm", "TC_Lead_CRM_EmailCampaign" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2017-06-27", testCaseDescription = "To verify that user is able to Copy and Customize the campaign while associating lead", testCaseId = "TC_Lead_CRM_EmailCampaign")
	public void associateWithCopyCustomCampaignLink()
			throws IOException, Exception, ParserConfigurationException, SAXException {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());
		String testCaseIdInternal = "TC_08_Copy_And_Customize_Campaign_At_All_Tab";
		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseIdInternal);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			CRMLeadsPage pobj = new CRMLeadsPage(driver);
			CRMCampaignCenterPageTest campaignCenterPage = new CRMCampaignCenterPageTest();
			CRMGroupsPageTest groupPage = new CRMGroupsPageTest();
			CRMCampaignCenterCampaignsPage pobjcam = new CRMCampaignCenterCampaignsPage(driver);

			fc.utobj().printTestStep("Navigate To CRM > Campaign Center");
			fc.crm().crm_common().CRMModule(driver);

			fc.utobj().printTestStep("Create Campaign At Campaign Center > Campaigns");
			String campaignName = fc.utobj().generateTestData(dataSet.get("campaignName"));
			String accessibility = dataSet.get("accessibility");
			String campaignType = dataSet.get("campaignType");
			String templateName = fc.utobj().generateTestData(dataSet.get("templateName"));
			String emailSubject = fc.utobj().generateTestData(dataSet.get("emailSubject"));
			String plainTextEditor = fc.utobj().generateTestData(dataSet.get("plainTextEditor"));
			campaignCenterPage.createCampaignGeneric(driver, config, campaignName, accessibility, campaignType,
					templateName, emailSubject, plainTextEditor);

			fc.utobj().printTestStep("Copy And Customize Campaign");
			fc.utobj().clickElement(driver, pobjcam.viewAllButton);

			campaignCenterPage.searchCampaignByFilter(driver, campaignName);
			groupPage.actionImgOption(driver, campaignName, "Copy and Customize");
			fc.commonMethods().switch_frameClass_newLayoutcboxIframe(driver);
			String campaignTitleForCopy = fc.utobj().generateTestData(dataSet.get("campaignTitleForCopy"));
			campaignName = campaignTitleForCopy;
			fc.utobj().sendKeys(driver, pobjcam.campaignTitle, campaignTitleForCopy);
			String accessibilityForCopy = dataSet.get("accessibilityForCopy");
			fc.utobj().selectDropDown(driver, pobjcam.campaignAccessibility, accessibilityForCopy);
			fc.utobj().clickElement(driver, pobjcam.startProcessButton);
			fc.utobj().switchFrameToDefault(driver);
			fc.utobj().clickElement(driver, pobjcam.finalizeAndLaunchCampaignCopy);

			fc.utobj().printTestStep("Verify The Copy And Customize Campaign");
			fc.utobj().clickElement(driver, pobjcam.viewAllButton);

			fc.utobj().printTestStep("Verify Copy And Customize Campaign At All Tab");
			campaignCenterPage.searchCampaignByPromotionalFilter(driver, campaignName);

			boolean isCampaignNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[.='" + campaignName + "']");
			if (isCampaignNamePresent == false) {
				fc.utobj().throwsException(
						"was not able to verify Copy And Customize Campaign At Campaign Center All Tab");
			}

			dataSet = fc.utobj().readTestData("crm", testCaseId);

			fc.utobj().printTestStep("Navigate To Admin Users Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			String emailId = "crmautomation@staffex.com";
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUser = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporateUser.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			String firstName = fc.utobj().generateTestData(dataSet.get("firstName"));
			String lastName = fc.utobj().generateTestData(dataSet.get("lastName"));
			String company = fc.utobj().generateTestData(dataSet.get("company"));
			String assignTo = "Corporate";
			String city = fc.utobj().generateTestData(dataSet.get("city"));
			String address = fc.utobj().generateTestData(dataSet.get("address"));
			String suffix = fc.utobj().generateTestData(dataSet.get("suffix"));
			String jobTitle = fc.utobj().generateTestData(dataSet.get("jobTitle"));
			String comment = fc.utobj().generateTestData(dataSet.get("comment"));
			String franchiseId = "";
			String franchiseUser = "";
			String userNameReg = "";
			String regionName = "";
			fc.utobj().printTestStep("Navigate To CRM > Leads > Leads Summary");
			fc.utobj().printTestStep("Add Lead");
			addLead(driver, dataSet, firstName, lastName, company, assignTo, city, address, suffix, jobTitle, comment,
					corpUser.getuserFullName(), regionName, userNameReg, franchiseId, franchiseUser, config);

			fc.utobj().clickElement(driver, pobj.leadsLink);
			searchLeadByOwner(driver, corpUser.getuserFullName(), "Open Leads");
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//a[contains(text () ,'" + firstName + " " + lastName + "')]"));

			fc.utobj().printTestStep("Associate With Campaign");
			fc.utobj().clickElement(driver, pobj.campaignLinkAtLeadInfo);
			fc.utobj().clickElement(driver, pobj.filterContainer);
			fc.utobj().sendKeys(driver, pobj.searchCampaign, campaignName);
			fc.utobj().clickElement(driver, pobj.applyFilter);

			fc.utobj().clickElement(driver, pobj.sendCampaignBtn);
			fc.utobj().clickElement(driver, pobj.confirmBtn);
			fc.utobj().clickElement(driver, pobj.leadsLink);
			searchLeadByOwner(driver, corpUser.getuserFullName(), "Open Leads");
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//a[contains(text () ,'" + firstName + " " + lastName + "')]"));

			fc.utobj().printTestStep("Verify Associate With Campaign");
			isCampaignNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () ,'Promotional Campaign')]/following-sibling::td[contains(text () ,'"
							+ campaignName + "')]");
			if (isCampaignNamePresent == false) {
				fc.utobj().throwsException("was not able to verify Campign Name");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "crm", "TC_Lead_CRM_Email_Campaign_EmailValidation" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "To verify user is not able to associate campaign when email address is not provided", testCaseId = "TC_Lead_CRM_Email_Campaign_EmailValidation")
	public void associateWithEmailCampaignifEmailNot()
			throws IOException, Exception, ParserConfigurationException, SAXException {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		Map<String, String> dataSetCustom = null;
		String testCaseIdInternal = "" + testCaseId;
		Map<String, String> printInfo = null;
		try {
			driver = fc.loginpage().login(driver);
			CRMLeadsPage pobj = new CRMLeadsPage(driver);

			fc.utobj().printTestStep("Navigate To Admin Users Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			String emailId = "crmautomation@staffex.com";
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUser = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporateUser.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			String firstName = fc.utobj().generateTestData(dataSet.get("firstName"));
			String lastName = fc.utobj().generateTestData(dataSet.get("lastName"));
			String company = fc.utobj().generateTestData(dataSet.get("company"));
			String assignTo = "Corporate";
			String city = fc.utobj().generateTestData(dataSet.get("city"));
			String address = fc.utobj().generateTestData(dataSet.get("address"));
			String suffix = fc.utobj().generateTestData(dataSet.get("suffix"));
			String jobTitle = fc.utobj().generateTestData(dataSet.get("jobTitle"));
			String comment = fc.utobj().generateTestData(dataSet.get("comment"));
			String franchiseId = "";
			String franchiseUser = "";
			String userNameReg = "";
			String regionName = "";

			fc.utobj().printTestStep("Navigate To CRM > Leads > Leads Summary");
			fc.utobj().printTestStep("Add Lead");
			fc.crm().crm_common().CRMLeadsLnk(driver);
			fc.utobj().clickElement(driver, pobj.addNew);
			fc.utobj().clickElement(driver, pobj.leadLnk);

			try {
				fc.utobj().sendKeys(driver, pobj.leadFirstName, firstName);
				fc.utobj().sendKeys(driver, pobj.leadLastName, lastName);

				WebElement dropDownField = driver.findElement(By.name("leadOwnerID2"));
				fc.utobj().selectDropDownByVisibleText(driver, dropDownField, corpUser.getuserFullName());

				fc.utobj().clickElement(driver, pobj.saveBtn);

			} catch (Exception e) {
				fc.utobj().throwsException(
						"was not able to verify associate campaign when email address is not provided.");
			}

			fc.utobj().clickElement(driver, pobj.leadsLink);
			searchLeadByOwner(driver, corpUser.getuserFullName(), "Open Leads");
			fc.utobj().printTestStep("Associate With Campaign");

			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//a[contains(text () ,'" + firstName + " " + lastName + "')]"));

			fc.utobj().selectActionMenuItemsWithTagAMoreActions(driver, "Associate with Campaign");

			fc.utobj()
					.printTestStep("verify user is not able to associate campaign when email address is not provided");
			try {
				fc.utobj().acceptAlertBox(driver);
			} catch (Exception e) {
				fc.utobj().throwsException(
						"was not able to verify associate campaign when email address is not provided.");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "crm", "TC_Lead_CRM_EmailCampaign_Archive" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2017-06-27", testCaseDescription = "To verify that user is able to perform Archive actions under the Actions button on the Campaign Details page while associating lead from Lead summary page ", testCaseId = "TC_Lead_CRM_EmailCampaign_Archive")
	public void associateWithCampaign_Archive()
			throws IOException, Exception, ParserConfigurationException, SAXException {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());
		String testCaseIdInternal = "TC_08_Copy_And_Customize_Campaign_At_All_Tab";
		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseIdInternal);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			CRMLeadsPage pobj = new CRMLeadsPage(driver);
			CRMCampaignCenterPageTest campaignCenterPage = new CRMCampaignCenterPageTest();
			CRMGroupsPageTest groupPage = new CRMGroupsPageTest();
			CRMCampaignCenterCampaignsPage pobjcam = new CRMCampaignCenterCampaignsPage(driver);

			fc.utobj().printTestStep("Navigate To CRM > Campaign Center");
			fc.crm().crm_common().CRMModule(driver);

			fc.utobj().printTestStep("Create Campaign At Campaign Center > Campaigns");
			String campaignName = fc.utobj().generateTestData(dataSet.get("campaignName"));
			String accessibility = dataSet.get("accessibility");
			String campaignType = dataSet.get("campaignType");
			String templateName = fc.utobj().generateTestData(dataSet.get("templateName"));
			String emailSubject = fc.utobj().generateTestData(dataSet.get("emailSubject"));
			String plainTextEditor = fc.utobj().generateTestData(dataSet.get("plainTextEditor"));
			campaignCenterPage.createCampaignGeneric(driver, config, campaignName, accessibility, campaignType,
					templateName, emailSubject, plainTextEditor);

			fc.utobj().printTestStep("Archive Campaign");
			fc.utobj().clickElement(driver, pobjcam.viewAllButton);

			campaignCenterPage.searchCampaignByFilter(driver, campaignName);
			groupPage.actionImgOption(driver, campaignName, "Archive");
			fc.utobj().acceptAlertBox(driver);

			fc.utobj().printTestStep("Verify Archive Campaign At All Campaign Tab");
			campaignCenterPage.searchCampaignByFilter(driver, campaignName);

			boolean isCampaignNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () ,'No Record Found')]");
			if (isCampaignNamePresent == false) {
				fc.utobj()
						.throwsException("was not able to verify Campaign At All Campaign Tab After Archived Campaign");
			}

			dataSet = fc.utobj().readTestData("crm", testCaseId);

			fc.utobj().printTestStep("Navigate To Admin Users Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			String emailId = "crmautomation@staffex.com";
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUser = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporateUser.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			String firstName = fc.utobj().generateTestData(dataSet.get("firstName"));
			String lastName = fc.utobj().generateTestData(dataSet.get("lastName"));
			String company = fc.utobj().generateTestData(dataSet.get("company"));
			String assignTo = "Corporate";
			String city = fc.utobj().generateTestData(dataSet.get("city"));
			String address = fc.utobj().generateTestData(dataSet.get("address"));
			String suffix = fc.utobj().generateTestData(dataSet.get("suffix"));
			String jobTitle = fc.utobj().generateTestData(dataSet.get("jobTitle"));
			String comment = fc.utobj().generateTestData(dataSet.get("comment"));
			String franchiseId = "";
			String franchiseUser = "";
			String userNameReg = "";
			String regionName = "";
			fc.utobj().printTestStep("Navigate To CRM > Leads > Leads Summary");
			fc.utobj().printTestStep("Add Lead");
			addLead(driver, dataSet, firstName, lastName, company, assignTo, city, address, suffix, jobTitle, comment,
					corpUser.getuserFullName(), regionName, userNameReg, franchiseId, franchiseUser, config);

			fc.utobj().clickElement(driver, pobj.leadsLink);
			searchLeadByOwner(driver, corpUser.getuserFullName(), "Open Leads");
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//a[contains(text () ,'" + firstName + " " + lastName + "')]"));

			fc.utobj().printTestStep("Associate With Campaign");
			fc.utobj().clickElement(driver, pobj.campaignLinkAtLeadInfo);
			fc.utobj().clickElement(driver, pobj.filterContainer);
			fc.utobj().sendKeys(driver, pobj.searchCampaign, campaignName);
			fc.utobj().clickElement(driver, pobj.applyFilter);

			fc.utobj().printTestStep("Verify Associate With Campaign");
			isCampaignNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//td[contains(text () ,'No Record Found')]");
			if (isCampaignNamePresent == false) {
				fc.utobj()
						.throwsException("was not able to verify Campaign At All Campaign Tab After Archived Campaign");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "crm", "TC_Lead_CRM_EmailCampaign_Delete" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2017-06-27", testCaseDescription = "To verify that user is able to perform Delete actions under the Actions button on the Campaign Details page while associating lead from Lead summary page ", testCaseId = "TC_Lead_CRM_EmailCampaign_Delete")
	public void associateWithCampaign_Delete()
			throws IOException, Exception, ParserConfigurationException, SAXException {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());
		String testCaseIdInternal = "TC_08_Copy_And_Customize_Campaign_At_All_Tab";
		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseIdInternal);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			CRMLeadsPage pobj = new CRMLeadsPage(driver);
			CRMCampaignCenterPageTest campaignCenterPage = new CRMCampaignCenterPageTest();
			CRMGroupsPageTest groupPage = new CRMGroupsPageTest();
			CRMCampaignCenterCampaignsPage pobjcam = new CRMCampaignCenterCampaignsPage(driver);

			fc.utobj().printTestStep("Navigate To CRM > Campaign Center");
			fc.crm().crm_common().CRMModule(driver);

			fc.utobj().printTestStep("Create Campaign At Campaign Center > Campaigns");
			String campaignName = fc.utobj().generateTestData(dataSet.get("campaignName"));
			String accessibility = dataSet.get("accessibility");
			String campaignType = dataSet.get("campaignType");
			String templateName = fc.utobj().generateTestData(dataSet.get("templateName"));
			String emailSubject = fc.utobj().generateTestData(dataSet.get("emailSubject"));
			String plainTextEditor = fc.utobj().generateTestData(dataSet.get("plainTextEditor"));
			campaignCenterPage.createCampaignGeneric(driver, config, campaignName, accessibility, campaignType,
					templateName, emailSubject, plainTextEditor);

			fc.utobj().printTestStep("Delete Campaign");
			fc.utobj().clickElement(driver, pobjcam.viewAllButton);

			campaignCenterPage.searchCampaignByFilter(driver, campaignName);
			groupPage.actionImgOption(driver, campaignName, "Delete");
			fc.utobj().acceptAlertBox(driver);

			fc.utobj().printTestStep("Verify Delete Campaign At All Campaign Tab");
			campaignCenterPage.searchCampaignByFilter(driver, campaignName);

			boolean isCampaignNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () ,'No Record Found')]");
			if (isCampaignNamePresent == false) {
				fc.utobj().throwsException("was not able to verify Campaign At All Campaign Tab After Delete Campaign");
			}

			dataSet = fc.utobj().readTestData("crm", testCaseId);

			fc.utobj().printTestStep("Navigate To Admin Users Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			String emailId = "crmautomation@staffex.com";
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUser = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporateUser.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			String firstName = fc.utobj().generateTestData(dataSet.get("firstName"));
			String lastName = fc.utobj().generateTestData(dataSet.get("lastName"));
			String company = fc.utobj().generateTestData(dataSet.get("company"));
			String assignTo = "Corporate";
			String city = fc.utobj().generateTestData(dataSet.get("city"));
			String address = fc.utobj().generateTestData(dataSet.get("address"));
			String suffix = fc.utobj().generateTestData(dataSet.get("suffix"));
			String jobTitle = fc.utobj().generateTestData(dataSet.get("jobTitle"));
			String comment = fc.utobj().generateTestData(dataSet.get("comment"));
			String franchiseId = "";
			String franchiseUser = "";
			String userNameReg = "";
			String regionName = "";
			fc.utobj().printTestStep("Navigate To CRM > Leads > Leads Summary");
			fc.utobj().printTestStep("Add Lead");
			addLead(driver, dataSet, firstName, lastName, company, assignTo, city, address, suffix, jobTitle, comment,
					corpUser.getuserFullName(), regionName, userNameReg, franchiseId, franchiseUser, config);

			fc.utobj().clickElement(driver, pobj.leadsLink);
			searchLeadByOwner(driver, corpUser.getuserFullName(), "Open Leads");
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//a[contains(text () ,'" + firstName + " " + lastName + "')]"));

			fc.utobj().printTestStep("Associate With Campaign");
			fc.utobj().clickElement(driver, pobj.campaignLinkAtLeadInfo);
			fc.utobj().clickElement(driver, pobj.filterContainer);
			fc.utobj().sendKeys(driver, pobj.searchCampaign, campaignName);
			fc.utobj().clickElement(driver, pobj.applyFilter);

			fc.utobj().printTestStep("Verify Associate With Campaign with Deleted Campaign");
			isCampaignNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//td[contains(text () ,'No Record Found')]");
			if (isCampaignNamePresent == false) {
				fc.utobj().throwsException("was not able to verify Campaign At All Campaign Tab After Delete Campaign");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	public void addLead(WebDriver driver, Map<String, String> dataSet, String firstName, String lastName,
			String company, String assignTo, String city, String address, String suffix, String jobTitle,
			String comment, String userName, String regionName, String regionalUser, String franchiseId,
			String franchiseUser, Map<String, String> config) throws Exception {

		String testCaseId = "TC_Add_Lead_At_Lead_Page";

		if (fc.utobj().validate(testCaseId)) {
			try {
				CRMLeadsPage pobj = new CRMLeadsPage(driver);
				fc.crm().crm_common().CRMLeadsLnk(driver);
				fc.utobj().clickElement(driver, pobj.addNew);
				fc.utobj().clickElement(driver, pobj.leadLnk);
				fc.utobj().selectDropDown(driver, pobj.title, "Mr.");
				fc.utobj().sendKeys(driver, pobj.leadFirstName, firstName);
				fc.utobj().sendKeys(driver, pobj.leadLastName, lastName);
				fc.utobj().sendKeys(driver, pobj.companyName, company);
				fc.utobj().selectDropDown(driver, pobj.primaryContactMethodSelect, "Email");

				if (assignTo.equalsIgnoreCase("Corporate")) {
					if (!fc.utobj().isSelected(driver, pobj.assignToCorporate)) {
						fc.utobj().clickElement(driver, pobj.assignToCorporate);
					}
					fc.utobj().selectDropDown(driver, pobj.selectCorporateUser, userName);
				} else if (assignTo.equalsIgnoreCase("Regional")) {
					if (!fc.utobj().isSelected(driver, pobj.assignToRegional)) {
						fc.utobj().clickElement(driver, pobj.assignToRegional);
					}
					fc.utobj().selectDropDown(driver, pobj.selectAreaRegion, regionName);
					;
					fc.utobj().selectDropDown(driver, pobj.selectRegionalUser, regionalUser);

				} else if (assignTo.equalsIgnoreCase("Franchise")) {
					if (!fc.utobj().isSelected(driver, pobj.assignToFranchise)) {
						fc.utobj().clickElement(driver, pobj.assignToFranchise);
					}
					fc.utobj().selectDropDown(driver, pobj.selectFranchiseId, franchiseId);
					fc.utobj().selectDropDown(driver, pobj.selectFranchiseUser, franchiseUser);
				} else if (assignTo.equalsIgnoreCase("Only Franchise")) {
					fc.utobj().selectDropDown(driver, pobj.selectFranchiseUser, franchiseUser);

				}
				fc.utobj().sendKeys(driver, pobj.address, address);
				fc.utobj().sendKeys(driver, pobj.city, city);
				fc.utobj().selectDropDown(driver, pobj.country, "USA");
				fc.utobj().selectDropDown(driver, pobj.state, "Alabama");
				fc.utobj().sendKeys(driver, pobj.zipcode, "12345");
				fc.utobj().sendKeys(driver, pobj.phoneNumbers, "1236547896");
				fc.utobj().sendKeys(driver, pobj.extn, "12");
				fc.utobj().sendKeys(driver, pobj.faxNumbers, "1263985487");
				fc.utobj().sendKeys(driver, pobj.mobileNumbers, "8896724441");
				
				String email = fc.utobj().generateTestData("crmautomation")+"@staffex.com";
				//String email = "crmautomation@staffex.com";
				fc.utobj().sendKeys(driver, pobj.emailIds, email);
				fc.utobj().sendKeys(driver, pobj.alternateEmail, "testautoamtion@franqa.com");
				fc.utobj().sendKeys(driver, pobj.suffix, suffix);
				fc.utobj().sendKeys(driver, pobj.position, jobTitle);
				fc.utobj().sendKeys(driver, pobj.birthdate, "11/24/1980");
				fc.utobj().sendKeys(driver, pobj.anniversarydate, "11/24/1999");
				fc.utobj().selectDropDown(driver, pobj.rating, "Hot");
				fc.utobj().sendKeys(driver, pobj.bestTimeToContact, "12:00 PM EST");
				fc.utobj().sendKeys(driver, pobj.comments, comment);
				fc.utobj().clickElement(driver, pobj.saveBtn);

				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException("Was not able to Add Lead :: CRM > Leads");
		}
	}

	
	
	public void addLead_fromOwnUser(WebDriver driver, Map<String, String> dataSet, String firstName, String lastName,
			String company, String assignTo, String city, String address, String suffix, String jobTitle,
			String comment, String userName, String regionName, String regionalUser, String franchiseId,
			String franchiseUser, Map<String, String> config) throws Exception {

		String testCaseId = "TC_Add_Lead_At_Lead_Page";

		if (fc.utobj().validate(testCaseId)) {
			try {
				CRMLeadsPage pobj = new CRMLeadsPage(driver);
				fc.crm().crm_common().CRMLeadsLnk(driver);
				fc.utobj().clickElement(driver, pobj.addNew);
				fc.utobj().clickElement(driver, pobj.leadLnk);
				fc.utobj().selectDropDown(driver, pobj.title, "Mr.");
				fc.utobj().sendKeys(driver, pobj.leadFirstName, firstName);
				fc.utobj().sendKeys(driver, pobj.leadLastName, lastName);
				fc.utobj().sendKeys(driver, pobj.companyName, company);
				fc.utobj().selectDropDown(driver, pobj.primaryContactMethodSelect, "Email");

				if (assignTo.equalsIgnoreCase("Corporate")) {
					if (!fc.utobj().isSelected(driver, pobj.assignToCorporate)) {
						fc.utobj().clickElement(driver, pobj.assignToCorporate);
					}
					fc.utobj().selectDropDown(driver, pobj.selectCorporateUser, userName);
				} else if (assignTo.equalsIgnoreCase("Regional")) {
					if (!fc.utobj().isSelected(driver, pobj.assignToRegional)) {
						fc.utobj().clickElement(driver, pobj.assignToRegional);
					}
					fc.utobj().selectDropDown(driver, pobj.selectRegionalUser, regionalUser);

				} else if (assignTo.equalsIgnoreCase("Franchise")) {
					if (!fc.utobj().isSelected(driver, pobj.assignToFranchise)) {
						fc.utobj().clickElement(driver, pobj.assignToFranchise);
					}
					
					fc.utobj().selectDropDown(driver, pobj.selectFranchiseUser, franchiseUser);
				} else if (assignTo.equalsIgnoreCase("Only Franchise")) {
					fc.utobj().selectDropDown(driver, pobj.selectFranchiseUser, franchiseUser);

				}
				fc.utobj().sendKeys(driver, pobj.address, address);
				fc.utobj().sendKeys(driver, pobj.city, city);
				fc.utobj().selectDropDown(driver, pobj.country, "USA");
				fc.utobj().selectDropDown(driver, pobj.state, "Alabama");
				fc.utobj().sendKeys(driver, pobj.zipcode, "12345");
				fc.utobj().sendKeys(driver, pobj.phoneNumbers, "1236547896");
				fc.utobj().sendKeys(driver, pobj.extn, "12");
				fc.utobj().sendKeys(driver, pobj.faxNumbers, "1263985487");
				fc.utobj().sendKeys(driver, pobj.mobileNumbers, "8896724441");
				
				String email = fc.utobj().generateTestData("crmautomation")+"@staffex.com";
				//String email = "crmautomation@staffex.com";
				fc.utobj().sendKeys(driver, pobj.emailIds, email);
				fc.utobj().sendKeys(driver, pobj.alternateEmail, "testautoamtion@franqa.com");
				fc.utobj().sendKeys(driver, pobj.suffix, suffix);
				fc.utobj().sendKeys(driver, pobj.position, jobTitle);
				fc.utobj().sendKeys(driver, pobj.birthdate, "11/24/1980");
				fc.utobj().sendKeys(driver, pobj.anniversarydate, "11/24/1999");
				fc.utobj().selectDropDown(driver, pobj.rating, "Hot");
				fc.utobj().sendKeys(driver, pobj.bestTimeToContact, "12:00 PM EST");
				fc.utobj().sendKeys(driver, pobj.comments, comment);
				fc.utobj().clickElement(driver, pobj.saveBtn);

				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException("Was not able to Add Lead :: CRM > Leads");
		}
	}
	
	
	
	
	public void VerifyPrintPreview(WebDriver driver, String textTobeVerified) throws Exception {
		Set<String> windows = new HashSet<String>();
		windows = driver.getWindowHandles();

		boolean isTextDisplayed = false;

		String currentWindow = driver.getWindowHandle();
		for (String window : windows) {
			if (!window.equals(currentWindow)) {

				driver.switchTo().window(window);
				if (driver.getCurrentUrl().contains("bPrint")) {
					isTextDisplayed = fc.utobj().assertPageSource(driver, textTobeVerified);
				}
			}
		}
		driver.close();

		if (isTextDisplayed) {
			Reporter.log(textTobeVerified + "  is displayed on the print preview page. !!! ");
		} else {
			fc.utobj().throwsException(textTobeVerified + "  is not displayed on the print preview page. !!! ");
		}

		driver.switchTo().window(currentWindow);
	}

	public void searchLeadByOwner(WebDriver driver, String userName, String leadTypeView) throws Exception {

		CRMLeadsPage pobj = new CRMLeadsPage(driver);
		fc.utobj().clickElement(driver, pobj.showFilter);
		fc.utobj().setToDefault(driver, pobj.leadOwnerSelect);
		try {
			WebElement divsion = fc.utobj().getElementByXpath(driver, ".//*[@id='ms-parentdivisionID']");
			fc.utobj().setToDefault(driver, divsion);
		} catch (Exception e) {
		}
		fc.utobj()
				.selectValFromMultiSelect(driver, pobj.leadOwnerSelect, fc.utobj()
						.getElement(driver, pobj.leadOwnerSelect).findElement(By.xpath("./div/div/input")),
						userName);
		fc.utobj().setToDefault(driver, pobj.franchiseSelect);
		fc.utobj().setToDefault(driver, pobj.statusSelect);
		fc.utobj().selectDropDown(driver, pobj.addDateSelect, "All");
		fc.utobj().setToDefault(driver, pobj.emailStatusSelect);
		fc.utobj().selectDropDown(driver, pobj.viewLeadsSelect, leadTypeView);
		fc.utobj().clickElement(driver, pobj.searchBtn);
		;
		fc.utobj().clickElement(driver, pobj.hideFilter);
	}

	public void searchLeadByOwnerDiffrent(WebDriver driver, String userName, String leadTypeView, List<String> listInfo)
			throws Exception {

		CRMLeadsPage pobj = new CRMLeadsPage(driver);
		fc.utobj().clickElement(driver, pobj.showFilter);
		fc.utobj().setToDefault(driver, pobj.leadOwnerSelect);
		try {
			WebElement divsion = fc.utobj().getElementByXpath(driver, ".//*[@id='ms-parentdivisionID']");
			fc.utobj().setToDefault(driver, divsion);
		} catch (Exception e) {
		}
		fc.utobj().selectMultipleValFromMultiSelect(driver,
				fc.utobj().getElementByXpath(driver, ".//*[@id='ms-parentleadOwner']"), listInfo);
		// fc.utobj().selectValFromMultiSelect(driver,
		// pobj.leadOwnerSelect,fc.utobj().getElement(driver,
		// pobj.leadOwnerSelect).findElement(By.xpath("./div/div/input")),
		// userName);
		fc.utobj().setToDefault(driver, pobj.franchiseSelect);
		fc.utobj().setToDefault(driver, pobj.statusSelect);
		fc.utobj().selectDropDown(driver, pobj.addDateSelect, "All");
		fc.utobj().setToDefault(driver, pobj.emailStatusSelect);
		fc.utobj().selectDropDown(driver, pobj.viewLeadsSelect, leadTypeView);
		fc.utobj().clickElement(driver, pobj.searchBtn);
		;
		fc.utobj().clickElement(driver, pobj.hideFilter);
	}

	// system search
	public void systemExactSearch(WebDriver driver, String leadName) throws Exception {
		// search By System Search Button
		CRMLeadsPage pobj = new CRMLeadsPage(driver);
		fc.utobj().sendKeys(driver, pobj.searchLeads, leadName);
		// fc.utobj().clickElement(driver, pobj.exactSearch);
		fc.utobj().clickElement(driver, pobj.systemSearchButton);
	}

	public Map<String, String> fillFormDataWithDataset(WebDriver driver, Map<String, String> config, String testCaseId,
			Map<String, String> dataSet) throws Exception {
		Reporter.log("Filling TestCase Data = " + testCaseId);
		String id = "id";

		List<String> listItems = new ArrayList<String>();
		Map<String, String> printInfo = new HashMap<String, String>();

		List<WebElement> mandateBox = null;
		mandateBox = driver.findElements(By.xpath(
				".//td[@qat_tableid='addData']//*[@type='text' or @class='fTextBoxDate' or @class='fTextBox' or @class='multiList' or @type='radio' or @type='checkbox' or @type='checkBox' or @id='ms-parentsellerCombo' or @class='ms-choice' or @class='form-control']"));

		Map<String, String> alreadyitrateElement = new HashMap<>();
		Map<String, String> alreadyradio = new HashMap<>();
		Map<String, String> alreadycheckBox = new HashMap<>();

		try {
			for (WebElement webElement : mandateBox) {

				if (webElement.getAttribute("" + id) != null && !"".equals(webElement.getAttribute("" + id))
						&& !"null".equals(webElement.getAttribute("" + id))
						&& !":".equals(webElement.getAttribute("" + id))
						&& !"dateOfOpen".equals(webElement.getAttribute("" + id))
						&& !"fimCbOtherComplaintType".equals(webElement.getAttribute("" + id))) {
					/*
					 * try{
					 * System.out.println("Type==="+webElement.getAttribute(
					 * "type")+"====values==="+webElement.getAttribute("value")
					 * +"======"+dataSet.get(""+webElement.getAttribute(""+id))
					 * +"====="+""+webElement.getAttribute(""+id));
					 * }catch(Exception e) {
					 * 
					 * }
					 */

					if (alreadyitrateElement.containsKey(webElement.getAttribute("" + id))) {

					} else {
						if (dataSet.containsKey(webElement.getAttribute("" + id))) {
							WebElement elementmovepostion = null;
							if ("name".equals(id)) {
								elementmovepostion = driver.findElement(By.name("" + webElement.getAttribute("" + id)));

							} else {
								elementmovepostion = fc.utobj().getElementByID(driver,
										"" + webElement.getAttribute("" + id));

							}
							fc.utobj().moveToElement(driver, elementmovepostion);
							if (webElement.getAttribute("type").indexOf("select-one") != -1) {
								if (webElement.getAttribute("" + id) != null
										&& !"cmSource2ID".equals(webElement.getAttribute("" + id))
										&& !"cmSource3ID".equals(webElement.getAttribute("" + id)))

								{
									try {

										Select singledrop = null;
										List<WebElement> singleDropDown = null;
										try {
											singledrop = new Select(fc.utobj().getElementByID(driver,
													"" + webElement.getAttribute("" + id)));
											singleDropDown = driver
													.findElements(By.id("" + webElement.getAttribute("" + id)));
										} catch (Exception e) {
											singledrop = new Select(
													driver.findElement(By.name("" + webElement.getAttribute("" + id))));
											singleDropDown = driver
													.findElements(By.name("" + webElement.getAttribute("" + id)));
										}
										if (webElement.getAttribute("value") == null
												|| "".equals(webElement.getAttribute("value"))
												|| "null".equals(webElement.getAttribute("value"))
												|| "-1".equals(webElement.getAttribute("value"))) {

											int size = singleDropDown.size();
											if (dataSet.get("" + webElement.getAttribute("" + id)) != null
													&& !"".equals(dataSet.get("" + webElement.getAttribute("" + id)))) {
												if (size >= 1) {
													try {
														WebElement dropDownField = driver
																.findElement(By.id("" + webElement.getAttribute("id")));
														// fc.utobj().moveToElement(driver,
														// dropDownField);
														fc.utobj().selectDropDownByVisibleText(driver, dropDownField,
																dataSet.get("" + webElement.getAttribute("" + id)));

														if (!"birthMonth".equals(webElement.getAttribute("" + id))
																&& !"birthDate".equals(webElement.getAttribute("" + id))
																&& !"spouseBirthMonth"
																		.equals(webElement.getAttribute("" + id))
																&& !"spouseBirthDate"
																		.equals(webElement.getAttribute("" + id))) {
															listItems.add(
																	dataSet.get("" + webElement.getAttribute("" + id)));
														}
														printInfo.put("" + webElement.getAttribute("" + id),
																dataSet.get("" + webElement.getAttribute("" + id)));

													} catch (Exception eradio) {
														// Reporter.log("Problem
														// in selecting drop :
														// "+""+webElement.getAttribute("id")
														// +" Values getting
														// from Excel not match
														// in desired
														// fomrat."+""+dataSet.get(""+webElement.getAttribute("id")));
													}
												}
											}
										}

									} catch (Exception e3) {

									}
								}

							} else if (webElement.getAttribute("type").indexOf("select-multiple") != -1) {
								if (webElement.getAttribute("value") == null
										|| "".equals(webElement.getAttribute("value"))
										|| "null".equals(webElement.getAttribute("value"))
										|| "-1".equals(webElement.getAttribute("value"))) {
									try {
										WebElement element = driver
												.findElement(By.id("ms-parent" + webElement.getAttribute("" + id)));
										fc.utobj().clickElement(driver, element);
										fc.utobj().clickElement(driver,
												fc.utobj().getElementByXpath(driver, ".//*[@id='selectAll']"));
										fc.utobj().clickElement(driver, element);
									} catch (Exception emultipal) {
										// Reporter.log("Problem in multiple
										// value drop-down :
										// "+""+webElement.getAttribute("id"));
									}
								}
							} else if (webElement.getAttribute("type").indexOf("textarea") != -1) {
								if (webElement.getAttribute("value") == null
										|| "".equals(webElement.getAttribute("value"))
										|| "null".equals(webElement.getAttribute("value"))) {
									try {
										driver.findElement(By.name("" + webElement.getAttribute("" + id)))
												.sendKeys(dataSet.get("" + webElement.getAttribute("" + id)));
										listItems.add(dataSet.get("" + webElement.getAttribute("" + id)));
										printInfo.put("" + webElement.getAttribute("id"),
												dataSet.get("" + webElement.getAttribute("" + id)));
									} catch (Exception eText) {
										// Reporter.log("Problem in entering
										// TextArea in :
										// "+""+webElement.getAttribute("id"));
									}
								}
							} else if (webElement.getAttribute("type").indexOf("radio") != -1) {
								try {
									String sValue = "";
									List<WebElement> rdBtn_Sex = driver
											.findElements(By.id("" + webElement.getAttribute("" + id))); //
									if (alreadyradio.containsKey(webElement.getAttribute("" + id))) {

									} else {

										int size = rdBtn_Sex.size();
										if (size > 1) {
											sValue = dataSet.get("" + webElement.getAttribute("" + id));
										}
										try {
											List<WebElement> rdBtn_Field = null;
											WebElement element = null;
											try {
												rdBtn_Field = driver.findElements(By.xpath(
														".//*[@name='" + webElement.getAttribute("" + id) + "']")); // work
												element = fc.utobj().getElementByXpath(driver,
														".//*[@name='" + webElement.getAttribute("" + id) + "']");
											} catch (Exception eTxt) {
												rdBtn_Field = driver.findElements(
														By.xpath(".//*[@id='" + webElement.getAttribute("id") + "']")); // work
												element = driver.findElement(
														By.xpath(".//*[@id='" + webElement.getAttribute("id") + "']"));

											}

											fc.utobj().moveToElement(driver, element);
											fc.utobj().clickRadioButton(driver, rdBtn_Field, sValue); // work
											listItems.add(dataSet.get("" + webElement.getAttribute("" + id)));
											printInfo.put("" + webElement.getAttribute("id"),
													dataSet.get("" + webElement.getAttribute("" + id)));
											alreadyradio.put(webElement.getAttribute("" + id),
													webElement.getAttribute("" + id));
										} catch (Exception eText) {
											// Reporter.log("Problem in
											// selecting radio value in :
											// "+""+webElement.getAttribute("id"));
										}
									}
								} catch (Exception exp) {
									Reporter.log(exp.toString());
									exp.printStackTrace();
								}

							} else if (webElement.getAttribute("type").toLowerCase().indexOf("checkbox") != -1) {
								List<WebElement> oCheckBox = driver
										.findElements(By.name("" + webElement.getAttribute("" + id)));
								String sValue = "";
								if (alreadycheckBox.containsKey(webElement.getAttribute("" + id))) {

								} else {

									int size = oCheckBox.size();
									for (int i = 0; i < size; i++) {
										sValue = oCheckBox.get(i).getAttribute("value");
										// Thread.sleep(2000);
										if (i == (size - 1)) {

											// break;
										}

									}
									try {
										List<WebElement> rdBtn_Field = driver.findElements(By.xpath(
												"//*[contains(@id, '" + webElement.getAttribute("" + id) + "')]")); // work
										WebElement element = fc.utobj().getElementByXpath(driver,
												"//*[contains(@id, '" + webElement.getAttribute("" + id) + "')]");
										fc.utobj().moveToElement(driver, element);
										fc.utobj().clickRadioButton(driver, rdBtn_Field, sValue); // work
										alreadycheckBox.put(webElement.getAttribute("" + id),
												webElement.getAttribute("" + id));
									} catch (Exception eText) {

									}
								}
							} else if (webElement.getAttribute("type").indexOf("file") != -1) {
								String fileName = fc.utobj().getFilePathFromTestData("pictureFile");
								try {
									fc.utobj().sendKeys(driver,
											fc.utobj().getElementByID(driver, "" + webElement.getAttribute("" + id)),
											fileName);
									listItems.add(fileName);
									printInfo.put("" + webElement.getAttribute("" + id), fileName);
								} catch (Exception eFile) {
									// Reporter.log("Problem in uploading file
									// type value in :
									// "+""+webElement.getAttribute("id"));
								}
							} else if (webElement.getAttribute("type").indexOf("text") != -1) {
								if (webElement.getAttribute("value") == null
										|| "".equals(webElement.getAttribute("value"))
										|| "null".equals(webElement.getAttribute("value"))
										|| "0.00".equals(webElement.getAttribute("value"))) {
									if (webElement.getAttribute("class").indexOf("fTextBoxDate") != -1) {
										try {
											if (dataSet.get("" + webElement.getAttribute("" + id)) != null
													|| !"".equals(dataSet.get("" + webElement.getAttribute("" + id)))
													|| !"null".equals(
															dataSet.get("" + webElement.getAttribute("" + id)))) {
												fc.utobj().sendKeys(driver,
														driver.findElement(
																By.name("" + webElement.getAttribute("" + id))),
														dataSet.get("" + webElement.getAttribute("" + id))); // work
												listItems.add(dataSet.get("" + webElement.getAttribute("" + id)));
												printInfo.put("" + webElement.getAttribute("" + id),
														dataSet.get("" + webElement.getAttribute("" + id)));

											} else {

											}
										} catch (Exception eDate) {
											// Reporter.log("Problem in Entering
											// Date fields value in :
											// "+""+webElement.getAttribute("id"));
										}
									} else {
										try {
											if ("0.00".equals(webElement.getAttribute("value"))) {
												fc.utobj()
														.sendKeys(driver,
																driver.findElement(
																		By.name("" + webElement.getAttribute("" + id))),
																"");

												// System.out.println(webElement.getAttribute("value")
												// +"======"+dataSet.get(""+webElement.getAttribute(""+id))
												// +"====="+""+webElement.getAttribute(""+id));

											}
											if ("name".equals(id)) {
												fc.utobj().sendKeys(driver,
														driver.findElement(
																By.name("" + webElement.getAttribute("" + id))),
														dataSet.get("" + webElement.getAttribute("" + id)));
												if ("accountName".equals("" + webElement.getAttribute("" + id))
														|| "opportunityOwner"
																.equals("" + webElement.getAttribute("" + id))
														|| "oppContactID"
																.equals("" + webElement.getAttribute("" + id))) {
													;
													fc.utobj().clickElement(driver, driver.findElement(
															By.xpath(".//*[@id='customizedAjaxSearch']/div")));
													fc.utobj().sleep();
												}
											} else {
												fc.utobj().sendKeys(driver,
														driver.findElement(
																By.id("" + webElement.getAttribute("" + id))),
														dataSet.get("" + webElement.getAttribute("" + id)));
												if ("accountName".equals("" + webElement.getAttribute("" + id))
														|| "opportunityOwner"
																.equals("" + webElement.getAttribute("" + id))
														|| "oppContactID"
																.equals("" + webElement.getAttribute("" + id))) {
													;
													fc.utobj().clickElement(driver, driver.findElement(
															By.xpath(".//*[@id='customizedAjaxSearch']/div")));
													fc.utobj().sleep();
												}
											}

											if (!"ssn".equals("" + webElement.getAttribute("" + id))
													&& !"spouseSsn".equals("" + webElement.getAttribute("" + id))) {
												listItems.add(dataSet.get("" + webElement.getAttribute("" + id)));
											}
											printInfo.put("" + webElement.getAttribute("" + id),
													dataSet.get("" + webElement.getAttribute("" + id)));

										} catch (Exception eText) {
											// Reporter.log("Problem in Entering
											// Text fileds value in :
											// "+""+webElement.getAttribute("id"));
										}

									}
								}
							}
							alreadyitrateElement.put(webElement.getAttribute("" + id),
									webElement.getAttribute("" + id));
						}
					}
				}
			}

			if ("TC_CRM_QA_Lead_Details".equals(testCaseId)) {

				Select singledrop = null;
				List<WebElement> singleDropDown = null;
				try {
					singledrop = new Select(driver.findElement(By.name("leadOwnerID2")));
					singleDropDown = driver.findElements(By.name("leadOwnerID2"));
					WebElement dropDownField = driver.findElement(By.name("leadOwnerID2"));
					fc.utobj().selectDropDownByVisibleText(driver, dropDownField, dataSet.get("leadOwnerID2"));

					dropDownField = driver.findElement(By.name("cmSource2ID"));
					fc.utobj().selectDropDownByVisibleText(driver, dropDownField, dataSet.get("cmSource2ID"));
					;
					dropDownField = driver.findElement(By.name("cmSource3ID"));
					fc.utobj().selectDropDownByVisibleText(driver, dropDownField, dataSet.get("cmSource3ID"));

				} catch (Exception e) {

				}

			}
			if ("TC_CRM_QA_Contact_Details".equals(testCaseId)) {

				Select singledrop = null;
				List<WebElement> singleDropDown = null;
				try {
					singledrop = new Select(driver.findElement(By.name("contactOwnerID2")));
					singleDropDown = driver.findElements(By.name("contactOwnerID2"));
					WebElement dropDownField = driver.findElement(By.name("contactOwnerID2"));
					fc.utobj().selectDropDownByVisibleText(driver, dropDownField, dataSet.get("contactOwnerID2"));

					dropDownField = driver.findElement(By.name("cmSource2ID"));
					fc.utobj().selectDropDownByVisibleText(driver, dropDownField, dataSet.get("cmSource2ID"));
					;
					dropDownField = driver.findElement(By.name("cmSource3ID"));
					fc.utobj().selectDropDownByVisibleText(driver, dropDownField, dataSet.get("cmSource3ID"));

				} catch (Exception e) {

				}

			}

		} catch (Exception e) {
			// fc.utobj().throwsException("Fields data are not available!
			// "+e.getMessage());
		}
		try {
			// fc.utobj().printTestStep(testCaseId, "Submiting the Tab ");
			WebElement elementbutton = fc.utobj().getElementByXpath(driver,
					".//*[@id='Submit' or @name='Submit' or @name='button' or @name='Submit1' or @name='add1'] ");
			fc.utobj().clickElement(driver, elementbutton);

		} catch (Exception ee) {
			fc.utobj().printBugStatus("Unable to submit page " + ee);
		}

		return printInfo;
	}

}