package com.builds.test.crm;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.parsers.ParserConfigurationException;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;
import org.testng.Reporter;
import org.testng.annotations.Test;
import org.xml.sax.SAXException;

import com.builds.test.admin.AdminUsersManageCorporateUsersAddCorporateUserPageTest;
import com.builds.test.common.CorporateUser;
import com.builds.uimaps.crm.AdminCRMContactTypeConfigurationPage;
import com.builds.uimaps.crm.CRMLeadsPage;
import com.builds.utilities.FranconnectUtil;
import com.builds.utilities.TestCase;

public class CRMLead_AreaConvertTestCase {

	FranconnectUtil fc = new FranconnectUtil();

	@Test(groups = { "crm", "crmLeadAreaConvert" })
	@TestCase(createdOn = "2018-01-16", updatedOn = "2018-01-16", testCaseId = "TC_Lead_CRM_ConvertCase", testCaseDescription = "To Verify Convert leads behaviours.")
	private void crmInfoFillAreaConvert_1() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		Map<String, String> dataSetCustom = null;
		String testCaseIdInternal = "" + testCaseId;
		Map<String, String> printInfo = null;
		try {
			driver = fc.loginpage().login(driver);
			CRMLeadsPage pobj = new CRMLeadsPage(driver);

			fc.utobj().printTestStep("Navigate To Admin Users Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			String emailId = "crmautomation@staffex.com";
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUser = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporateUser.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			fc.utobj().printTestStep("Navigate To CRM > Leads > Leads Summary");
			fc.utobj().printTestStep("Add First Lead");
			fc.crm().crm_common().CRMLeadsLnk(driver);
			fc.utobj().clickElement(driver, pobj.addNew);
			fc.utobj().clickElement(driver, pobj.leadLnk);

			testCaseIdInternal = "TC_CRM_QA_Lead_Details";
			dataSetCustom = fc.utobj().readTestData("crm", testCaseIdInternal);
			String firstName = fc.utobj().generateTestData("Aa");
			String lastName = fc.utobj().generateTestData("Bb");
			dataSetCustom.put("leadFirstName", firstName);
			dataSetCustom.put("leadLastName", lastName);
			dataSetCustom.put("cmLeadTypeID", "Select");
			dataSetCustom.put("cmSource2ID", "Internet");
			dataSetCustom.put("cmSource3ID", "BISON");
			dataSetCustom.put("leadOwnerID2", corpUser.getuserFullName());

			printInfo = fillFormDataWithDataset(driver, config, testCaseIdInternal, dataSetCustom);

			if (printInfo != null && printInfo.size() > 0) {
				// System.out.println("printInfo=First="+printInfo);
			}

			fc.utobj().clickElement(driver, pobj.leadsLink);

			systemExactSearch(driver, firstName + " " + lastName);

			fc.utobj().printTestStep("Verify Lead Name in Lead Summary.");
			boolean isLeadNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//*[contains(text () ,'" + firstName + " " + lastName + "')]");
			if (isLeadNamePresent == false) {
				fc.utobj().throwsException("was not able to verify First Lead Name");
			}

			// Convert Leads
			fc.utobj().printTestStep("Convert Lead");
			String regionName = fc.utobj().generateTestData("Conregion");
			fc.utobj().actionImgOption(driver, firstName + " " + lastName, "Convert");
			if (!fc.utobj().isSelected(driver, pobj.leadOwnerRadio)) {
				fc.utobj().clickElement(driver, pobj.leadOwnerRadio);
			}
			String contactFName = fc.utobj().generateTestData("Confirst");
			fc.utobj().sendKeys(driver, pobj.contactFirstName, contactFName);
			String contactLName = fc.utobj().generateTestData("Conlast");
			fc.utobj().sendKeys(driver, pobj.contactLastName, contactLName);
			String accountName = fc.utobj().generateTestData("Conaccount");
			fc.utobj().sendKeys(driver, pobj.accountName, accountName);
			fc.utobj().clickElement(driver, pobj.convertBtn);
			fc.utobj().clickElement(driver, pobj.goToLeadBtn);

			// verify at lead page
			fc.utobj().printTestStep("Verify Convert Lead At Lead Page");
			searchLeadByOwner(driver, corpUser.getuserFullName(), "Converted Leads");

			boolean convertedLead = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firstName + " " + lastName + "')]");
			if (convertedLead == false) {
				fc.utobj().throwsException("was not able to verify Converted Lead At Leads Home Page");
			}
			boolean owneOfLead = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[contains(text () ,'" + firstName + " " + lastName
					+ "')]/ancestor::tr/td[contains(text () ,'" + corpUser.getuserFullName() + "')]");
			if (owneOfLead == false) {
				fc.utobj().throwsException("was not able to verify Owner of lead After Conversion");
			}

			fc.utobj().clickElement(driver, driver
					.findElement(By.xpath(".//a[contains(text () ,'" + firstName + " " + lastName + "')]/../img")));

			boolean nameOfContact = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[.='Mr. " + contactFName + " " + contactLName + "']");
			if (nameOfContact == false) {
				fc.utobj().throwsException("was not able to verify name of Contatct");
			}
			fc.utobj().clickElement(driver, pobj.acountInfoTab);
			boolean accountNameText = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//td[contains(text () ,'" + accountName + "')]");
			if (accountNameText == false) {
				fc.utobj().throwsException("was not able to verify Account Name");
			}
			fc.utobj().clickElement(driver, pobj.accountsLink);

			fc.utobj().sendKeys(driver, pobj.searchAccounts, accountName);

			fc.utobj().clickElement(driver, pobj.systemSearchButton);
			boolean isAccountNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + accountName + "')]");
			if (isAccountNamePresent == false) {
				fc.utobj().throwsException("was not able to verify account name at Accounts page");
			}

			// verify number of contact
			fc.utobj().clickElement(driver, driver
					.findElement(By.xpath(".//a[contains(text () ,'" + accountName + "')]/ancestor::tr/td/a[.='1']")));

			// verify Contact Info
			boolean isContactNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + contactFName + " " + contactLName + "')]");
			if (isContactNamePresent == false) {
				fc.utobj().throwsException("was not able to verify contact Name at Contact Page");
			}
			boolean isOwnerPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[contains(text () ,'" + contactFName + " "
					+ contactLName + "')]/ancestor::tr/td[contains(text () ,'" + corpUser.getuserFullName() + "')]");
			if (isOwnerPresent == false) {
				fc.utobj().throwsException("was not able to verify owner at Contact Page");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "crm", "crmLeadAreaConvert" })
	@TestCase(createdOn = "2018-01-16", updatedOn = "2018-01-16", testCaseId = "TC_Lead_CRM_ConvertCycle", testCaseDescription = "To verify that after filling all the details in the form if user clicks on the Save and Convert button.")
	private void crmInfoFillAreaConvert_2() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		Map<String, String> dataSetCustom = null;
		String testCaseIdInternal = "" + testCaseId;
		Map<String, String> printInfo = null;
		try {
			driver = fc.loginpage().login(driver);
			CRMLeadsPage pobj = new CRMLeadsPage(driver);

			fc.utobj().printTestStep("Navigate To Admin Users Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			String emailId = "crmautomation@staffex.com";
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUser = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporateUser.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			fc.utobj().printTestStep("Navigating to Admin > CRM >  Configure Opportunity Stages");
			fc.utobj().printTestStep("Add Stage");
			AdminCRMConfigureOpportunityStagesPageTest stagePage = new AdminCRMConfigureOpportunityStagesPageTest();
			String stage = fc.utobj().generateTestData("Convstage");
			stagePage.addStage(driver, stage);

			fc.utobj().printTestStep("Navigate To CRM > Leads > Leads Summary");
			fc.utobj().printTestStep("Add First Lead");
			fc.crm().crm_common().CRMLeadsLnk(driver);
			fc.utobj().clickElement(driver, pobj.addNew);
			fc.utobj().clickElement(driver, pobj.leadLnk);

			testCaseIdInternal = "TC_CRM_QA_Lead_Details";
			dataSetCustom = fc.utobj().readTestData("crm", testCaseIdInternal);
			String firstName = fc.utobj().generateTestData("Aa");
			String lastName = fc.utobj().generateTestData("Bb");
			dataSetCustom.put("leadFirstName", firstName);
			dataSetCustom.put("leadLastName", lastName);
			dataSetCustom.put("cmLeadTypeID", "Select");
			dataSetCustom.put("cmSource2ID", "Internet");
			dataSetCustom.put("cmSource3ID", "BISON");
			dataSetCustom.put("leadOwnerID2", corpUser.getuserFullName());

			printInfo = fillFormDataWithDataset(driver, config, testCaseIdInternal, dataSetCustom);

			if (printInfo != null && printInfo.size() > 0) {
				// System.out.println("printInfo=First="+printInfo);
			}

			fc.utobj().clickElement(driver, pobj.leadsLink);

			systemExactSearch(driver, firstName + " " + lastName);

			fc.utobj().printTestStep("Verify Lead Name in Lead Summary.");
			boolean isLeadNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//*[contains(text () ,'" + firstName + " " + lastName + "')]");
			if (isLeadNamePresent == false) {
				fc.utobj().throwsException("was not able to verify First Lead Name");
			}

			// Convert Leads
			fc.utobj().printTestStep("Convert Lead");
			String regionName = fc.utobj().generateTestData("Conregion");
			fc.utobj().actionImgOption(driver, firstName + " " + lastName, "Convert");
			if (!fc.utobj().isSelected(driver, pobj.leadOwnerRadio)) {
				fc.utobj().clickElement(driver, pobj.leadOwnerRadio);
			}
			String accountName = fc.utobj().generateTestData("Conaccount");
			fc.utobj().sendKeys(driver, pobj.accountName, accountName);

			// create New Opportunity
			if (!fc.utobj().isSelected(driver,pobj.isOpportunity)) {
				fc.utobj().clickElement(driver, pobj.isOpportunity);
			}

			String opportunityName = fc.utobj().generateTestData("Convopp");
			fc.utobj().sendKeys(driver, pobj.opportunityName, opportunityName);
			fc.utobj().selectDropDown(driver, pobj.stage, stage);
			String salesAmount = "100";
			fc.utobj().sendKeys(driver, pobj.salesAmount, salesAmount);
			String closureDate = fc.utobj().getFutureDateUSFormat(4);
			fc.utobj().sendKeys(driver, pobj.closerDate, closureDate);

			fc.utobj().clickElement(driver, pobj.convertBtn);
			fc.utobj().clickElement(driver, pobj.goToLeadBtn);

			// check in open leads

			fc.utobj().printTestStep("Verify At Lead Page With Filter Open Leads");

			searchLeadByOwner(driver, corpUser.getuserFullName(), "Open Leads");
			boolean isNoRecordPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//td[.='No records found.']");
			if (isNoRecordPresent == false) {
				fc.utobj().throwsException("was not able to convert Lead");
			}

			// verify at lead page
			fc.utobj().printTestStep("Verify Convert Lead At Lead Page");
			searchLeadByOwner(driver, corpUser.getuserFullName(), "Converted Leads");

			boolean convertedLead = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firstName + " " + lastName + "')]");
			if (convertedLead == false) {
				fc.utobj().throwsException("was not able to verify Converted Lead At Leads Home Page");
			}

			WebElement elementToolTip = fc.utobj().getElementByXpath(driver, ".//*[contains(text () , '" + firstName
					+ " " + lastName + "')]/ancestor::tr/td/img[contains(@src, 'contactadded.png')]");
			Thread.sleep(2000);
			Actions ac1 = new Actions(driver);
			ac1.clickAndHold().moveToElement(elementToolTip);
			Thread.sleep(2000);
			ac1.moveToElement(elementToolTip).build().perform();
			Thread.sleep(1000);
			// ac1.moveToElement(elementToolTip).build().perform();

			String leadTipText = elementToolTip.getAttribute("title");

			if (leadTipText.indexOf("View Contact Details") != -1) {
				fc.utobj().printTestStep("Verify ToolTip Indicates the Lead is added through Converting of Leads.");
			} else {
				fc.utobj().throwsException("Was not able to verify ToolTip of Converting of Leads");
			}

			boolean owneOfLead = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[contains(text () ,'" + firstName + " " + lastName
					+ "')]/ancestor::tr/td[contains(text () ,'" + corpUser.getuserFullName() + "')]");
			if (owneOfLead == false) {
				fc.utobj().throwsException("was not able to verify Owner of lead After Conversion");
			}

			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//a[contains(text () ,'" + firstName + " " + lastName + "')]"));

			boolean isAccountPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[contains(text () ,'" + accountName + "')]");
			if (isAccountPresent == false) {
				fc.utobj().throwsException("was not able to verify account");
			}
			boolean isContactPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firstName + " " + lastName + "')]");
			if (isContactPresent == false) {
				fc.utobj().throwsException("was not able to verify Contact");
			}
			boolean isOpportunityPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + opportunityName + "')]");
			if (isOpportunityPresent == false) {
				fc.utobj().throwsException("was not able to verify Opportunity");
			}

			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//a[contains(text () ,'View Lead Details')]"));
			fc.commonMethods().switch_cboxIframe_frameId(driver);

			boolean isFirstNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () ,'First Name')]/following-sibling::td[.='" + firstName + "']");
			if (isFirstNamePresent == false) {
				fc.utobj().throwsException("was not able to verify First Name");
			}
			boolean isLastNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () ,'Last Name')]/following-sibling::td[.='" + lastName + "']");
			if (isLastNamePresent == false) {
				fc.utobj().throwsException("was not able to verify Last Name");
			}

			fc.utobj().clickElement(driver, pobj.closeBtn);
			fc.utobj().switchFrameToDefault(driver);
			fc.utobj().clickElement(driver, pobj.goToLeadBtn);

			// verify account and opportunity and contact information
			searchLeadByOwner(driver, corpUser.getuserFullName(), "Converted Leads");
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,(".//*[contains(text () ,'" + firstName + " " + lastName + "')]/ancestor::tr/td/img")));

			boolean isContatcInfoPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[.='Mr. " + firstName + " " + lastName + "']");
			if (isContatcInfoPresent == false) {
				fc.utobj().throwsException("was not able to verify Contact information");
			}

			fc.utobj().clickElement(driver, pobj.accountInfoTab);
			boolean isAccountNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () ,'Account Name')]/following-sibling::td[contains(text () ,'" + accountName
							+ "')]");
			if (isAccountNamePresent == false) {
				fc.utobj().throwsException("was not able to verify Account Name");
			}

			fc.utobj().clickElement(driver, pobj.opportunityTab);

			boolean isOpportunityNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + opportunityName + "')]");
			if (isOpportunityNamePresent == false) {
				fc.utobj().throwsException("was not able to verify Opportunity At Opprtunities Info Page");
			}
			boolean isAssignToOppPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[contains(text () ,'" + opportunityName
					+ "')]/ancestor::tr/td[contains(text () ,'" + corpUser.getuserFullName() + "')]");
			if (isAssignToOppPresent == false) {
				fc.utobj().throwsException("was not able to verify Assign TO At Opprtunities Info Page");
			}
			boolean isContactOppPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[contains(text () ,'" + opportunityName
					+ "')]/ancestor::tr/td/a[contains(text () ,'" + firstName + " " + lastName + "')]");
			if (isContactOppPresent == false) {
				fc.utobj().throwsException("was not able to verify Contact At Opprtunities Info Page");
			}
			boolean isStageOppPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[contains(text () ,'" + opportunityName
					+ "')]/ancestor::tr/td[contains(text () ,'" + stage + "')]");
			if (isStageOppPresent == false) {
				fc.utobj().throwsException("was not able to verify Stage At Opprtunities Info Page");
			}
			boolean isSalesAmountOppPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + opportunityName + "')]/ancestor::tr/td[contains(text () ,'100.00')]");
			if (isSalesAmountOppPresent == false) {
				fc.utobj().throwsException("was not able to verify Sales Amount At Opprtunities Info Page");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "crm", "crmLeadAreaConvert" })
	@TestCase(createdOn = "2018-01-16", updatedOn = "2018-01-16", testCaseId = "TC_Lead_CRM_ConvertButton", testCaseDescription = "To verify that the Convert button is appearing.")
	private void crmInfoFillAreaConvert_3() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		Map<String, String> dataSetCustom = null;
		String testCaseIdInternal = "" + testCaseId;
		Map<String, String> printInfo = null;
		try {
			driver = fc.loginpage().login(driver);
			CRMLeadsPage pobj = new CRMLeadsPage(driver);

			fc.utobj().printTestStep("Navigate To Admin Users Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			String emailId = "crmautomation@staffex.com";
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUser = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporateUser.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			fc.utobj().printTestStep("Navigate To CRM > Leads > Leads Summary");
			fc.utobj().printTestStep("Add First Lead");
			fc.crm().crm_common().CRMLeadsLnk(driver);
			fc.utobj().clickElement(driver, pobj.addNew);
			fc.utobj().clickElement(driver, pobj.leadLnk);

			testCaseIdInternal = "TC_CRM_QA_Lead_Details";
			dataSetCustom = fc.utobj().readTestData("crm", testCaseIdInternal);
			String firstName = fc.utobj().generateTestData("Aa");
			String lastName = fc.utobj().generateTestData("Bb");
			dataSetCustom.put("leadFirstName", firstName);
			dataSetCustom.put("leadLastName", lastName);
			dataSetCustom.put("cmLeadTypeID", "Select");
			dataSetCustom.put("cmSource2ID", "Internet");
			dataSetCustom.put("cmSource3ID", "BISON");
			dataSetCustom.put("leadOwnerID2", corpUser.getuserFullName());

			printInfo = fillFormDataWithDataset(driver, config, testCaseIdInternal, dataSetCustom);

			if (printInfo != null && printInfo.size() > 0) {
				// System.out.println("printInfo=First="+printInfo);
			}

			fc.utobj().clickElement(driver, pobj.leadsLink);

			systemExactSearch(driver, firstName + " " + lastName);

			fc.utobj().printTestStep("Verify Lead Name in Lead Summary.");
			boolean isLeadNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//*[contains(text () ,'" + firstName + " " + lastName + "')]");
			if (isLeadNamePresent == false) {
				fc.utobj().throwsException("was not able to verify First Lead Name");
			}
			fc.utobj().printTestStep(
					"To verify that the Convert button is appearing properly on the Lead Info Page beside the Change Status button.");
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//a[contains(text () ,'" + firstName + " " + lastName + "')]"));

			boolean isLeadConvertButtonPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//*[@name='convertLead']");
			if (isLeadConvertButtonPresent == false) {
				fc.utobj().throwsException("was not able to verify vert button is appearing properly.");
			} else {
				fc.utobj()
						.printTestStep("verified that the Convert button is appearing properly on the Lead Info Page.");
			}

			// Convert Leads
			fc.utobj().printTestStep("Verify Convert Lead from Button on Lead page");
			String regionName = fc.utobj().generateTestData("Conregion");
			// fc.utobj().actionImgOption(driver, firstName + " " + lastName,
			// "Convert");
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//*[@name='convertLead']"));
			if (!fc.utobj().isSelected(driver, pobj.leadOwnerRadio)) {
				fc.utobj().clickElement(driver, pobj.leadOwnerRadio);
			}
			String contactFName = fc.utobj().generateTestData("Confirst");
			fc.utobj().sendKeys(driver, pobj.contactFirstName, contactFName);
			String contactLName = fc.utobj().generateTestData("Conlast");
			fc.utobj().sendKeys(driver, pobj.contactLastName, contactLName);
			String accountName = fc.utobj().generateTestData("Conaccount");
			fc.utobj().sendKeys(driver, pobj.accountName, accountName);
			fc.utobj().clickElement(driver, pobj.convertBtn);
			fc.utobj().clickElement(driver, pobj.goToLeadBtn);

			// verify at lead page
			fc.utobj().printTestStep("Verify Convert Lead At Lead Page");
			searchLeadByOwner(driver, corpUser.getuserFullName(), "Converted Leads");

			boolean convertedLead = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firstName + " " + lastName + "')]");
			if (convertedLead == false) {
				fc.utobj().throwsException("was not able to verify Converted Lead At Leads Home Page");
			}
			boolean owneOfLead = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[contains(text () ,'" + firstName + " " + lastName
					+ "')]/ancestor::tr/td[contains(text () ,'" + corpUser.getuserFullName() + "')]");
			if (owneOfLead == false) {
				fc.utobj().throwsException("was not able to verify Owner of lead After Conversion");
			}

			fc.utobj().clickElement(driver, driver
					.findElement(By.xpath(".//a[contains(text () ,'" + firstName + " " + lastName + "')]/../img")));

			boolean nameOfContact = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[.='Mr. " + contactFName + " " + contactLName + "']");
			if (nameOfContact == false) {
				fc.utobj().throwsException("was not able to verify name of Contatct");
			}
			fc.utobj().clickElement(driver, pobj.acountInfoTab);
			boolean accountNameText = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//td[contains(text () ,'" + accountName + "')]");
			if (accountNameText == false) {
				fc.utobj().throwsException("was not able to verify Account Name");
			}
			fc.utobj().clickElement(driver, pobj.accountsLink);

			fc.utobj().sendKeys(driver, pobj.searchAccounts, accountName);

			fc.utobj().clickElement(driver, pobj.systemSearchButton);
			boolean isAccountNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + accountName + "')]");
			if (isAccountNamePresent == false) {
				fc.utobj().throwsException("was not able to verify account name at Accounts page");
			}

			// verify number of contact
			fc.utobj().clickElement(driver, driver
					.findElement(By.xpath(".//a[contains(text () ,'" + accountName + "')]/ancestor::tr/td/a[.='1']")));

			// verify Contact Info
			boolean isContactNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + contactFName + " " + contactLName + "')]");
			if (isContactNamePresent == false) {
				fc.utobj().throwsException("was not able to verify contact Name at Contact Page");
			}
			boolean isOwnerPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[contains(text () ,'" + contactFName + " "
					+ contactLName + "')]/ancestor::tr/td[contains(text () ,'" + corpUser.getuserFullName() + "')]");
			if (isOwnerPresent == false) {
				fc.utobj().throwsException("was not able to verify owner at Contact Page");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "crmLeadAreaConvert", "crm" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2017-08-10", testCaseDescription = "Verify the Conevert Lead Into Contact from Lead Info page to verify Count At CRM > Account > Account Summary ", testCaseId = "TC_291_Verify_Account_Summary")
	public void convertLeadtoContacteWithNewaccountAndOpportunity()
			throws IOException, Exception, ParserConfigurationException, SAXException {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			CRMLeadsPage pobj = new CRMLeadsPage(driver);

			fc.utobj().printTestStep("Naviaget To Admin Users Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			AdminUsersManageCorporateUsersAddCorporateUserPageTest addCorporatePage = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			String emailId = "crmautomation@staffex.com";
			CorporateUser corpUser = new CorporateUser();
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corpUser.setEmail(emailId);
			corpUser = addCorporatePage.createDefaultUser(driver, corpUser);
			String regionName = fc.utobj().generateTestData(dataSet.get("regionName"));

			String firstName = fc.utobj().generateTestData(dataSet.get("firstName"));
			String lastName = fc.utobj().generateTestData(dataSet.get("lastName"));
			String company = fc.utobj().generateTestData(dataSet.get("company"));
			String assignTo = "Corporate";
			String city = fc.utobj().generateTestData(dataSet.get("city"));
			String address = fc.utobj().generateTestData(dataSet.get("address"));
			String suffix = fc.utobj().generateTestData(dataSet.get("suffix"));
			String jobTitle = fc.utobj().generateTestData(dataSet.get("jobTitle"));
			String comment = fc.utobj().generateTestData(dataSet.get("comment"));
			String userNameReg = fc.utobj().generateTestData(dataSet.get("userNameReg"));
			String franchiseId = "";
			String franchiseUser = "";

			fc.utobj().printTestStep("Navigate To CRM > Leads > Leads Summary");
			fc.utobj().printTestStep("Add Lead");

			addLead(driver, dataSet, firstName, lastName, company, assignTo, city, address, suffix, jobTitle, comment,
					corpUser.getuserFullName(), regionName, userNameReg, franchiseId, franchiseUser, config);

			fc.utobj().printTestStep("Convert Lead At LeadInfo Page");
			fc.utobj().clickElement(driver, pobj.ConvertLead);
			String contactFName = fc.utobj().generateTestData(dataSet.get("contactFName"));
			fc.utobj().sendKeys(driver, pobj.contactFirstName, contactFName);
			String contactLName = fc.utobj().generateTestData(dataSet.get("contactLName"));
			fc.utobj().sendKeys(driver, pobj.contactLastName, contactLName);
			String accountName = fc.utobj().generateTestData(dataSet.get("accountName"));
			fc.utobj().sendKeys(driver, pobj.accountName, accountName);
			fc.utobj().clickElement(driver, pobj.isOpportunity);
			String opportunityName = fc.utobj().generateTestData(dataSet.get("opportunityName"));
			fc.utobj().sendKeys(driver, pobj.opportunityName, opportunityName);
			String stage = "Prospecting";
			fc.utobj().selectDropDown(driver, pobj.stage, stage);

			String salesAmount = "1000";
			fc.utobj().printTestStep(salesAmount);
			fc.utobj().sendKeys(driver, pobj.salesAmount, salesAmount);
			String closureDate = fc.utobj().getFutureDateUSFormat(4);
			fc.utobj().sendKeys(driver, pobj.closerDate, closureDate);
			String opportunitySource = "Web";
			fc.utobj().selectDropDown(driver, pobj.opportunitySource, opportunitySource);
			fc.utobj().clickElement(driver, pobj.btnConvert);

			fc.utobj().printTestStep("Navigate To CRM > Contacts > Contacts Summary Page");
			String contactname = contactFName + " " + contactLName;
			contactSearchFromSearchpage(driver, contactFName, contactLName);
			fc.utobj().clickLink(driver, contactname);
			Thread.sleep(1000);
			boolean isContactNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () ,'Name')]/ancestor::tr/td[.='Mr. " + contactFName + " " + contactLName
							+ "']");
			if (isContactNamePresent == false) {
				fc.utobj().throwsException("was not able to verify Contact Name");
			}
			boolean isPrimaryContactTypePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () ,'Primary Contact Method')]/ancestor::tr/td[.='Email']");
			if (isPrimaryContactTypePresent == false) {
				fc.utobj().throwsException("was not able to verify Primary Contact Type");
			}
			boolean isUserPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () ,'Assign To ')]/ancestor::tr/td[contains(text () , '"
							+ corpUser.getuserFullName() + "')]");
			if (isUserPresent == false) {
				fc.utobj().throwsException("was not able to verify Assign To");
			}

			fc.utobj().clickElement(driver, pobj.accountInfoTab);
			boolean isAccountNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () ,'Account Name')]/following-sibling::td[contains(text () ,'" + accountName
							+ "')]");
			if (isAccountNamePresent == false) {
				fc.utobj().throwsException("was not able to verify Account Name");
			}

			// repeating add lead

			String firstName1 = fc.utobj().generateTestData(dataSet.get("firstName"));
			String lastName1 = fc.utobj().generateTestData(dataSet.get("lastName"));
			String company1 = fc.utobj().generateTestData(dataSet.get("company"));
			String city1 = fc.utobj().generateTestData(dataSet.get("city"));
			String address1 = fc.utobj().generateTestData(dataSet.get("address"));
			String suffix1 = fc.utobj().generateTestData(dataSet.get("suffix"));
			String jobTitle1 = fc.utobj().generateTestData(dataSet.get("jobTitle"));
			String comment1 = fc.utobj().generateTestData(dataSet.get("comment"));
			String userNameReg1 = fc.utobj().generateTestData(dataSet.get("userNameReg"));
			String franchiseId1 = "";
			String franchiseUser1 = "";

			fc.utobj().printTestStep("Navigate To CRM > Leads > Leads Summary");
			fc.utobj().printTestStep("Add Lead");

			addLead(driver, dataSet, firstName1, lastName1, company1, assignTo, city1, address1, suffix1, jobTitle1,
					comment1, corpUser.getuserFullName(), regionName, userNameReg1, franchiseId1, franchiseUser1,
					config);

			fc.utobj().printTestStep("Convert Lead At LeadInfo Page");
			fc.utobj().clickElement(driver, pobj.ConvertLead);
			String contactFName1 = fc.utobj().generateTestData(dataSet.get("contactFName"));
			fc.utobj().sendKeys(driver, pobj.contactFirstName, contactFName1);
			String contactLName1 = fc.utobj().generateTestData(dataSet.get("contactLName"));
			fc.utobj().sendKeys(driver, pobj.contactLastName, contactLName1);

			fc.utobj().printTestStep("Associate with Existing Account");
			fc.utobj().clickElement(driver, pobj.AssociateExistingAccount);
			fc.utobj().sendKeys(driver, pobj.accountNameSearch, accountName);
			try {
				fc.utobj().clickElement(driver,
						fc.utobj().getElementByXpath(driver, ".//*[@id='customizedAjaxSearch']/div"));
			} catch (Exception e) {
				fc.utobj().clickElement(driver,
						fc.utobj().getElementByXpath(driver, ".//*[@id='customizedAjaxSearch']/div[1]"));
			}

			fc.utobj().clickElement(driver, pobj.isOpportunity);
			String opportunityName1 = fc.utobj().generateTestData(dataSet.get("opportunityName"));
			fc.utobj().sendKeys(driver, pobj.opportunityName, opportunityName1);
			fc.utobj().selectDropDown(driver, pobj.stage, stage);
			String salesAmount1 = "1000";
			fc.utobj().sendKeys(driver, pobj.salesAmount, salesAmount1);
			String closerDate1 = fc.utobj().getCurrentDateUSFormat();
			fc.utobj().sendKeys(driver, pobj.closerDate, closerDate1);
			String opportunitySource1 = "Web";
			fc.utobj().selectDropDown(driver, pobj.opportunitySource, opportunitySource1);
			fc.utobj().clickElement(driver, pobj.btnConvert);

			fc.utobj().printTestStep("Navigate To CRM > Contacts > Contacts Summary Page");
			// fc.utobj().clickElement(driver, pobj.contactsLinks);
			// pobjcontact.contactsFilter(driver, contactType);
			String contactname1 = contactFName1 + " " + contactLName1;
			contactSearchFromSearchpage(driver, contactFName1, contactLName1);
			/*
			 * fc.utobj().sendKeys(driver, pobj.searchSystem, contactname1);
			 * fc.utobj().clickElement(driver, pobj.searchAccountBtn);
			 */
			// fc.utobj().clickElement(driver,
			// fc.utobj().getElementByXpath(driver,".//*[@id='result']/table/tbody/tr[2]/td/table/tbody/tr[2]/td[2]/a")));
			fc.utobj().clickLink(driver, contactname1);
			boolean isContatcInfoPresent1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[.='" + "Mr." + " " + contactFName1 + " " + contactLName1 + "']");
			if (isContatcInfoPresent1 == false) {
				fc.utobj().throwsException("was not able to verify Contact information");
			}

			fc.utobj().clickElement(driver, pobj.accountInfoTab);
			boolean isAccountNamePresent1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () ,'Account Name')]/following-sibling::td[contains(text () ,'" + accountName
							+ "')]");
			if (isAccountNamePresent1 == false) {
				fc.utobj().throwsException("was not able to verify Account Name");
			}

			// fc.utobj().clickElement(driver, pobj.opportunityTab);
			fc.utobj().clickElement(driver, pobj.accountsLink);
			// fc.utobj().sendKeys(driver, pobj.searchAccounts, accountName);
			// fc.utobj().clickElement(driver, pobj.searchAccountBtn);

			isAccountNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[contains(text () ,'" + accountName + "')]");
			if (isAccountNamePresent == false) {
				fc.utobj().throwsException("was not able to verify account name at Accounts page");
			}
			// verify number of contact
			fc.utobj().clickElement(driver, driver
					.findElement(By.xpath(".//a[contains(text () ,'" + accountName + "')]/ancestor::tr/td/a[.='2']")));

			// verify Contact Info
			boolean isContactNamePresent1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + contactFName1 + " " + contactLName1 + "')]");
			if (isContactNamePresent1 == false) {
				fc.utobj().throwsException("was not able to verify contact Name at Contact Page");
			}
			boolean isOwnerPresent1 = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[contains(text () ,'" + contactFName + " "
					+ contactLName + "')]/ancestor::tr/td[contains(text () ,'" + corpUser.getuserFullName() + "')]");
			if (isOwnerPresent1 == false) {
				fc.utobj().throwsException("was not able to verify Owner");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "TC_Lead_CRM_ConvertaccountSummary", "crm" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2017-08-10", testCaseId = "TC_Lead_CRM_ConvertAccountSummary", testCaseDescription = "To verify that the user is able to select account on the account Summary pop-up")
	public void crmInfoFillAreaConvert_5() throws IOException, Exception, ParserConfigurationException, SAXException {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			CRMLeadsPage pobj = new CRMLeadsPage(driver);

			fc.utobj().printTestStep("Naviaget To Admin Users Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			AdminUsersManageCorporateUsersAddCorporateUserPageTest addCorporatePage = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			String emailId = "crmautomation@staffex.com";
			CorporateUser corpUser = new CorporateUser();
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corpUser.setEmail(emailId);
			corpUser = addCorporatePage.createDefaultUser(driver, corpUser);
			String regionName = fc.utobj().generateTestData(dataSet.get("regionName"));
			String firstName = fc.utobj().generateTestData(dataSet.get("firstName"));
			String lastName = fc.utobj().generateTestData(dataSet.get("lastName"));
			String company = fc.utobj().generateTestData(dataSet.get("company"));
			String assignTo = "Corporate";
			String city = fc.utobj().generateTestData(dataSet.get("city"));
			String address = fc.utobj().generateTestData(dataSet.get("address"));
			String suffix = fc.utobj().generateTestData(dataSet.get("suffix"));
			String jobTitle = fc.utobj().generateTestData(dataSet.get("jobTitle"));
			String comment = fc.utobj().generateTestData(dataSet.get("comment"));
			String userNameReg = fc.utobj().generateTestData(dataSet.get("userNameReg"));
			String franchiseId = "";
			String franchiseUser = "";

			fc.utobj().printTestStep("Navigate To CRM > Leads > Leads Summary");
			fc.utobj().printTestStep("Add Lead");

			addLead(driver, dataSet, firstName, lastName, company, assignTo, city, address, suffix, jobTitle, comment,
					corpUser.getuserFullName(), regionName, userNameReg, franchiseId, franchiseUser, config);

			fc.utobj().printTestStep("Convert Lead At LeadInfo Page");
			fc.utobj().clickElement(driver, pobj.ConvertLead);
			String contactFName = fc.utobj().generateTestData(dataSet.get("contactFName"));
			fc.utobj().sendKeys(driver, pobj.contactFirstName, contactFName);
			String contactLName = fc.utobj().generateTestData(dataSet.get("contactLName"));
			fc.utobj().sendKeys(driver, pobj.contactLastName, contactLName);
			String accountName = fc.utobj().generateTestData(dataSet.get("accountName"));
			fc.utobj().sendKeys(driver, pobj.accountName, accountName);
			// fc.utobj().clickElement(driver,
			// fc.utobj().getElementByXpath(driver,".//*[@id='customizedAjaxSearch']/div")));
			fc.utobj().clickElement(driver, pobj.isOpportunity);
			String opportunityName = fc.utobj().generateTestData(dataSet.get("opportunityName"));
			fc.utobj().sendKeys(driver, pobj.opportunityName, opportunityName);
			String stage = "Prospecting";
			fc.utobj().selectDropDown(driver, pobj.stage, stage);

			String salesAmount = "1000";
			fc.utobj().printTestStep(salesAmount);
			fc.utobj().sendKeys(driver, pobj.salesAmount, salesAmount);
			String closureDate = fc.utobj().getFutureDateUSFormat(4);
			fc.utobj().sendKeys(driver, pobj.closerDate, closureDate);
			String opportunitySource = "Web";
			fc.utobj().selectDropDown(driver, pobj.opportunitySource, opportunitySource);
			fc.utobj().clickElement(driver, pobj.btnConvert);

			fc.utobj().printTestStep("Navigate To CRM > Contacts > Contacts Summary Page");
			// fc.utobj().clickElement(driver, pobj.searchLink);
			// pobjcontact.contactsFilter(driver, contactType);
			String contactname = contactFName + " " + contactLName;
			contactSearchFromSearchpage(driver, contactFName, contactLName);
			fc.utobj().clickLink(driver, contactname);
			Thread.sleep(1000);
			boolean isContactNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () ,'Name')]/ancestor::tr/td[.='Mr. " + contactFName + " " + contactLName
							+ "']");
			if (isContactNamePresent == false) {
				fc.utobj().throwsException("was not able to verify Contact Name");
			}
			boolean isPrimaryContactTypePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () ,'Primary Contact Method')]/ancestor::tr/td[.='Email']");
			if (isPrimaryContactTypePresent == false) {
				fc.utobj().throwsException("was not able to verify Primary Contact Type");
			}
			boolean isUserPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () ,'Assign To ')]/ancestor::tr/td[contains(text () , '"
							+ corpUser.getuserFullName() + "')]");
			if (isUserPresent == false) {
				fc.utobj().throwsException("was not able to verify Assign To");
			}

			fc.utobj().clickElement(driver, pobj.accountInfoTab);
			boolean isAccountNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () ,'Account Name')]/following-sibling::td[contains(text () ,'" + accountName
							+ "')]");
			if (isAccountNamePresent == false) {
				fc.utobj().throwsException("was not able to verify Account Name");
			}

			// repeating add lead

			String firstName1 = fc.utobj().generateTestData(dataSet.get("firstName"));
			String lastName1 = fc.utobj().generateTestData(dataSet.get("lastName"));
			String company1 = fc.utobj().generateTestData(dataSet.get("company"));
			String city1 = fc.utobj().generateTestData(dataSet.get("city"));
			String address1 = fc.utobj().generateTestData(dataSet.get("address"));
			String suffix1 = fc.utobj().generateTestData(dataSet.get("suffix"));
			String jobTitle1 = fc.utobj().generateTestData(dataSet.get("jobTitle"));
			String comment1 = fc.utobj().generateTestData(dataSet.get("comment"));
			String userNameReg1 = fc.utobj().generateTestData(dataSet.get("userNameReg"));
			String franchiseId1 = "";
			String franchiseUser1 = "";

			fc.utobj().printTestStep("Navigate To CRM > Leads > Leads Summary");
			fc.utobj().printTestStep("Add Lead");

			addLead(driver, dataSet, firstName1, lastName1, company1, assignTo, city1, address1, suffix1, jobTitle1,
					comment1, corpUser.getuserFullName(), regionName, userNameReg1, franchiseId1, franchiseUser1,
					config);

			fc.utobj().printTestStep("Convert Lead At LeadInfo Page");
			fc.utobj().clickElement(driver, pobj.ConvertLead);
			String contactFName1 = fc.utobj().generateTestData(dataSet.get("contactFName"));
			fc.utobj().sendKeys(driver, pobj.contactFirstName, contactFName1);
			String contactLName1 = fc.utobj().generateTestData(dataSet.get("contactLName"));
			fc.utobj().sendKeys(driver, pobj.contactLastName, contactLName1);

			fc.utobj().printTestStep("Associate with Existing Account");
			fc.utobj().clickElement(driver, pobj.AssociateExistingAccount);
			fc.utobj().sendKeys(driver, pobj.accountNameSearch, accountName);
			try {
				fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//*[@class='searchvalue']"));
				fc.commonMethods().switch_cboxIframe_frameId(driver);
				fc.utobj().clickLink(driver, accountName);
				fc.utobj().switchFrameToDefault(driver);
			} catch (Exception e) {
				fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//*[@class='searchvalue']"));
				fc.commonMethods().switch_cboxIframe_frameId(driver);
				fc.utobj().clickLink(driver, accountName);
				fc.utobj().switchFrameToDefault(driver);
			}

			fc.utobj().clickElement(driver, pobj.isOpportunity);
			String opportunityName1 = fc.utobj().generateTestData(dataSet.get("opportunityName"));
			fc.utobj().sendKeys(driver, pobj.opportunityName, opportunityName1);
			fc.utobj().selectDropDown(driver, pobj.stage, stage);
			String salesAmount1 = "1000";
			fc.utobj().sendKeys(driver, pobj.salesAmount, salesAmount1);
			String closerDate1 = fc.utobj().getCurrentDateUSFormat();
			fc.utobj().sendKeys(driver, pobj.closerDate, closerDate1);
			String opportunitySource1 = "Web";
			fc.utobj().selectDropDown(driver, pobj.opportunitySource, opportunitySource1);
			fc.utobj().clickElement(driver, pobj.btnConvert);

			fc.utobj().printTestStep("Navigate To CRM > Contacts > Contacts Summary Page");
			// fc.utobj().clickElement(driver, pobj.contactsLinks);
			// pobjcontact.contactsFilter(driver, contactType);
			String contactname1 = contactFName1 + " " + contactLName1;
			contactSearchFromSearchpage(driver, contactFName1, contactLName1);
			/*
			 * fc.utobj().sendKeys(driver, pobj.searchSystem, contactname1);
			 * fc.utobj().clickElement(driver, pobj.searchAccountBtn);
			 */
			// fc.utobj().clickElement(driver,
			// fc.utobj().getElementByXpath(driver,".//*[@id='result']/table/tbody/tr[2]/td/table/tbody/tr[2]/td[2]/a")));
			fc.utobj().clickLink(driver, contactname1);
			boolean isContatcInfoPresent1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[.='" + "Mr." + " " + contactFName1 + " " + contactLName1 + "']");
			if (isContatcInfoPresent1 == false) {
				fc.utobj().throwsException("was not able to verify Contact information");
			}

			fc.utobj().clickElement(driver, pobj.accountInfoTab);
			boolean isAccountNamePresent1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () ,'Account Name')]/following-sibling::td[contains(text () ,'" + accountName
							+ "')]");
			if (isAccountNamePresent1 == false) {
				fc.utobj().throwsException("was not able to verify Account Name");
			}

			// fc.utobj().clickElement(driver, pobj.opportunityTab);
			fc.utobj().clickElement(driver, pobj.accountsLink);
			// fc.utobj().sendKeys(driver, pobj.searchAccounts, accountName);
			// fc.utobj().clickElement(driver, pobj.searchAccountBtn);

			isAccountNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[contains(text () ,'" + accountName + "')]");
			if (isAccountNamePresent == false) {
				fc.utobj().throwsException("was not able to verify account name at Accounts page");
			}
			// verify number of contact
			fc.utobj().clickElement(driver, driver
					.findElement(By.xpath(".//a[contains(text () ,'" + accountName + "')]/ancestor::tr/td/a[.='2']")));

			// verify Contact Info
			boolean isContactNamePresent1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + contactFName1 + " " + contactLName1 + "')]");
			if (isContactNamePresent1 == false) {
				fc.utobj().throwsException("was not able to verify contact Name at Contact Page");
			}
			boolean isOwnerPresent1 = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[contains(text () ,'" + contactFName + " "
					+ contactLName + "')]/ancestor::tr/td[contains(text () ,'" + corpUser.getuserFullName() + "')]");
			if (isOwnerPresent1 == false) {
				fc.utobj().throwsException("was not able to verify Owner");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "TC_Lead_CRM_ConvertContactSummary", "crm" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2017-08-10", testCaseId = "TC_Lead_CRM_ConvertContactSummary", testCaseDescription = "To verify that the user is able to select contact on the contact Summary pop-up")
	public void crmInfoFillAreaConvert_6() throws IOException, Exception, ParserConfigurationException, SAXException {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			CRMLeadsPage pobj = new CRMLeadsPage(driver);

			fc.utobj().printTestStep("Naviaget To Admin Users Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			AdminUsersManageCorporateUsersAddCorporateUserPageTest addCorporatePage = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			String emailId = "crmautomation@staffex.com";
			CorporateUser corpUser = new CorporateUser();
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corpUser.setEmail(emailId);
			corpUser = addCorporatePage.createDefaultUser(driver, corpUser);
			String regionName = fc.utobj().generateTestData(dataSet.get("regionName"));
			// userNameCorpLogin = userNameCorp;

			String firstName = "Same10";// fc.utobj().generateTestData(dataSet.get("firstName"));
			String lastName = "Name10";// fc.utobj().generateTestData(dataSet.get("lastName"));
			String company = fc.utobj().generateTestData(dataSet.get("company"));
			String assignTo = "Corporate";
			String city = fc.utobj().generateTestData(dataSet.get("city"));
			String address = fc.utobj().generateTestData(dataSet.get("address"));
			String suffix = fc.utobj().generateTestData(dataSet.get("suffix"));
			String jobTitle = fc.utobj().generateTestData(dataSet.get("jobTitle"));
			String comment = fc.utobj().generateTestData(dataSet.get("comment"));
			String userNameReg = fc.utobj().generateTestData(dataSet.get("userNameReg"));
			String franchiseId = "";
			String franchiseUser = "";
			String userName = corpUser.getuserFullName();

			fc.utobj().printTestStep("Navigate To CRM > Leads > Leads Summary");
			fc.utobj().printTestStep("Add Lead");

			addLead(driver, dataSet, firstName, lastName, company, assignTo, city, address, suffix, jobTitle, comment,
					userName, regionName, userNameReg, franchiseId, franchiseUser, config);

			fc.utobj().printTestStep("Convert Lead At LeadInfo Page");
			fc.utobj().clickElement(driver, pobj.ConvertLead);
			String contactFName = "Same10";// fc.utobj().generateTestData(dataSet.get("contactFName"));
			fc.utobj().sendKeys(driver, pobj.contactFirstName, contactFName);
			String contactLName = "Name10";// fc.utobj().generateTestData(dataSet.get("contactLName"));
			fc.utobj().sendKeys(driver, pobj.contactLastName, contactLName);
			String accountName = fc.utobj().generateTestData(dataSet.get("accountName"));
			fc.utobj().sendKeys(driver, pobj.accountName, accountName);
			// fc.utobj().clickElement(driver,
			// fc.utobj().getElementByXpath(driver,".//*[@id='customizedAjaxSearch']/div")));
			fc.utobj().clickElement(driver, pobj.isOpportunity);
			String opportunityName = fc.utobj().generateTestData(dataSet.get("opportunityName"));
			fc.utobj().sendKeys(driver, pobj.opportunityName, opportunityName);
			String stage = "Prospecting";
			fc.utobj().selectDropDown(driver, pobj.stage, stage);

			String salesAmount = "1000";
			fc.utobj().printTestStep(salesAmount);
			fc.utobj().sendKeys(driver, pobj.salesAmount, salesAmount);
			String closureDate = fc.utobj().getFutureDateUSFormat(4);
			fc.utobj().sendKeys(driver, pobj.closerDate, closureDate);
			String opportunitySource = "Web";
			fc.utobj().selectDropDown(driver, pobj.opportunitySource, opportunitySource);
			fc.utobj().clickElement(driver, pobj.btnConvert);

			fc.utobj().printTestStep("Navigate To CRM > Contacts > Contacts Summary Page");
			// fc.utobj().clickElement(driver, pobj.searchLink);
			// pobjcontact.contactsFilter(driver, contactType);
			String contactname = contactFName + " " + contactLName;
			contactSearchFromSearchpage(driver, contactFName, contactLName);
			fc.utobj().clickLink(driver, contactname);
			Thread.sleep(1000);
			boolean isContactNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () ,'Name')]/ancestor::tr/td[.='Mr. " + contactFName + " " + contactLName
							+ "']");
			if (isContactNamePresent == false) {
				fc.utobj().throwsException("was not able to verify Contact Name");
			}
			boolean isPrimaryContactTypePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () ,'Primary Contact Method')]/ancestor::tr/td[.='Email']");
			if (isPrimaryContactTypePresent == false) {
				fc.utobj().throwsException("was not able to verify Primary Contact Type");
			}
			boolean isUserPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () ,'Assign To ')]/ancestor::tr/td[contains(text () , '" + userName + "')]");
			if (isUserPresent == false) {
				fc.utobj().throwsException("was not able to verify Assign To");
			}

			fc.utobj().clickElement(driver, pobj.accountInfoTab);
			boolean isAccountNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () ,'Account Name')]/following-sibling::td[contains(text () ,'" + accountName
							+ "')]");
			if (isAccountNamePresent == false) {
				fc.utobj().throwsException("was not able to verify Account Name");
			}

			// repeating add lead

			String firstName1 = "Same10";// fc.utobj().generateTestData(dataSet.get("firstName"));
			String lastName1 = "Name10";// fc.utobj().generateTestData(dataSet.get("lastName"));
			String company1 = fc.utobj().generateTestData(dataSet.get("company"));
			String city1 = fc.utobj().generateTestData(dataSet.get("city"));
			String address1 = fc.utobj().generateTestData(dataSet.get("address"));
			String suffix1 = fc.utobj().generateTestData(dataSet.get("suffix"));
			String jobTitle1 = fc.utobj().generateTestData(dataSet.get("jobTitle"));
			String comment1 = fc.utobj().generateTestData(dataSet.get("comment"));
			String userNameReg1 = fc.utobj().generateTestData(dataSet.get("userNameReg"));
			String franchiseId1 = "";
			String franchiseUser1 = "";
			// String userName1 = "";
			// String contactType1 =
			// fc.utobj().generateTestData(dataSet.get("contactType"));

			fc.utobj().printTestStep("Navigate To CRM > Leads > Leads Summary");
			fc.utobj().printTestStep("Add Lead");

			addLead(driver, dataSet, firstName1, lastName1, company1, assignTo, city1, address1, suffix1, jobTitle1,
					comment1, userName, regionName, userNameReg1, franchiseId1, franchiseUser1, config);

			fc.utobj().printTestStep("Convert Lead At LeadInfo Page");
			fc.utobj().clickElement(driver, pobj.ConvertLead);

			try {
				fc.utobj().clickElement(driver,
						fc.utobj().getElementByXpath(driver, ".//*[@name='contactOption' and @value='M']"));
				fc.commonMethods().switch_cboxIframe_frameId(driver);
				fc.utobj().clickElement(driver,
						fc.utobj().getElementByXpath(driver, ".//a[.='" + firstName1 + " " + lastName1 + "']"));
				fc.utobj().switchFrameToDefault(driver);

			} catch (Exception e) {
				fc.utobj().clickElement(driver,
						fc.utobj().getElementByXpath(driver, ".//*[@name='contactOption' and @value='M']"));
				fc.commonMethods().switch_cboxIframe_frameId(driver);
				fc.utobj().clickElement(driver,
						fc.utobj().getElementByXpath(driver, ".//a[.='" + firstName1 + " " + lastName1 + "']"));
				fc.utobj().switchFrameToDefault(driver);

			}

			String contactFName1 = "Same10"; // fc.utobj().generateTestData(dataSet.get("contactFName"));
			// fc.utobj().sendKeys(driver, pobj.contactFirstName,
			// contactFName1);
			String contactLName1 = "Name10";// fc.utobj().generateTestData(dataSet.get("contactLName"));
			// fc.utobj().sendKeys(driver, pobj.contactLastName, contactLName1);

			// fc.utobj().printTestStep(testCaseId, "Associate with Existing
			// Account");
			// fc.utobj().clickElement(driver, pobj.AssociateExistingAccount);
			// fc.utobj().sendKeys(driver, pobj.accountNameSearch, accountName);
			/*
			 * try{ fc.utobj().clickElement(driver,
			 * fc.utobj().getElementByXpath(driver,".//*[@class='searchvalue']")
			 * )); fc.commonMethods().switch_cboxIframe_frameId(driver);
			 * fc.utobj().clickLink(driver, accountName);
			 * fc.utobj().switchFrameToDefault(driver); } catch(Exception e){
			 * fc.utobj().clickElement(driver,
			 * fc.utobj().getElementByXpath(driver,".//*[@class='searchvalue']")
			 * )); fc.commonMethods().switch_cboxIframe_frameId(driver);
			 * fc.utobj().clickLink(driver, accountName);
			 * fc.utobj().switchFrameToDefault(driver); }
			 */

			fc.utobj().clickElement(driver, pobj.isOpportunity);
			String opportunityName1 = fc.utobj().generateTestData(dataSet.get("opportunityName"));
			fc.utobj().sendKeys(driver, pobj.opportunityName, opportunityName1);
			fc.utobj().selectDropDown(driver, pobj.stage, stage);
			String salesAmount1 = "1000";
			fc.utobj().sendKeys(driver, pobj.salesAmount, salesAmount1);
			String closerDate1 = fc.utobj().getCurrentDateUSFormat();
			fc.utobj().sendKeys(driver, pobj.closerDate, closerDate1);
			String opportunitySource1 = "Web";
			fc.utobj().selectDropDown(driver, pobj.opportunitySource, opportunitySource1);
			fc.utobj().clickElement(driver, pobj.btnConvert);

			fc.utobj().printTestStep("Navigate To CRM > Contacts > Contacts Summary Page");
			// fc.utobj().clickElement(driver, pobj.contactsLinks);
			// pobjcontact.contactsFilter(driver, contactType);
			String contactname1 = contactFName1 + " " + contactLName1;
			contactSearchFromSearchpage(driver, contactFName1, contactLName1);
			/*
			 * fc.utobj().sendKeys(driver, pobj.searchSystem, contactname1);
			 * fc.utobj().clickElement(driver, pobj.searchAccountBtn);
			 */
			// fc.utobj().clickElement(driver,
			// fc.utobj().getElementByXpath(driver,".//*[@id='result']/table/tbody/tr[2]/td/table/tbody/tr[2]/td[2]/a")));
			fc.utobj().clickLink(driver, contactname1);
			boolean isContatcInfoPresent1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[.='" + "Mr." + " " + contactFName1 + " " + contactLName1 + "']");
			if (isContatcInfoPresent1 == false) {
				fc.utobj().throwsException("was not able to verify Contact information");
			}

			fc.utobj().clickElement(driver, pobj.accountInfoTab);
			boolean isAccountNamePresent1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () ,'Account Name')]/following-sibling::td[contains(text () ,'" + accountName
							+ "')]");
			if (isAccountNamePresent1 == false) {
				fc.utobj().throwsException("was not able to verify Account Name");
			}

			// fc.utobj().clickElement(driver, pobj.opportunityTab);
			fc.utobj().clickElement(driver, pobj.accountsLink);
			// fc.utobj().sendKeys(driver, pobj.searchAccounts, accountName);
			// fc.utobj().clickElement(driver, pobj.searchAccountBtn);

			isAccountNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[contains(text () ,'" + accountName + "')]");
			if (isAccountNamePresent == false) {
				fc.utobj().throwsException("was not able to verify account name at Accounts page");
			}
			// verify number of contact
			fc.utobj().clickElement(driver, driver
					.findElement(By.xpath(".//a[contains(text () ,'" + accountName + "')]/ancestor::tr/td/a[.='1']")));

			// verify Contact Info
			boolean isContactNamePresent1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + contactFName1 + " " + contactLName1 + "')]");
			if (isContactNamePresent1 == false) {
				fc.utobj().throwsException("was not able to verify contact Name at Contact Page");
			}
			boolean isOwnerPresent1 = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[contains(text () ,'" + contactFName + " "
					+ contactLName + "')]/ancestor::tr/td[contains(text () ,'" + userName + "')]");
			if (isOwnerPresent1 == false) {
				fc.utobj().throwsException("was not able to verify Owner");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "crm", "TC_Lead_CRM_ConvertGroupCase" })
	@TestCase(createdOn = "2018-01-16", updatedOn = "2018-01-16", testCaseId = "TC_Lead_CRM_ConvertGroupCase", testCaseDescription = "To Verify leads Groups from multi select drop down on convert Lead Page.")
	private void crmInfoFillAreaConvert_Group() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		Map<String, String> dataSetCustom = null;
		String testCaseIdInternal = "" + testCaseId;
		Map<String, String> printInfo = null;
		try {
			driver = fc.loginpage().login(driver);
			CRMLeadsPage pobj = new CRMLeadsPage(driver);

			fc.utobj().printTestStep("Navigate To CRM > Group");
			fc.utobj().printTestStep("Create Group");
			String typeGroup = "Contact";
			String accessibility = "Public To All User";
			String groupName = fc.utobj().generateTestData("group");
			CRMGroupsPageTest G1 = new CRMGroupsPageTest();
			G1.createGroup(driver, typeGroup, accessibility, groupName, config);

			fc.utobj().printTestStep("Navigate To Admin Users Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			String emailId = "crmautomation@staffex.com";
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUser = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporateUser.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			fc.utobj().printTestStep("Navigate To CRM > Leads > Leads Summary");
			fc.utobj().printTestStep("Add First Lead");
			fc.crm().crm_common().CRMLeadsLnk(driver);
			fc.utobj().clickElement(driver, pobj.addNew);
			fc.utobj().clickElement(driver, pobj.leadLnk);

			testCaseIdInternal = "TC_CRM_QA_Lead_Details";
			dataSetCustom = fc.utobj().readTestData("crm", testCaseIdInternal);
			String firstName = fc.utobj().generateTestData("Aa");
			String lastName = fc.utobj().generateTestData("Bb");
			dataSetCustom.put("leadFirstName", firstName);
			dataSetCustom.put("leadLastName", lastName);
			dataSetCustom.put("cmLeadTypeID", "Select");
			dataSetCustom.put("cmSource2ID", "Internet");
			dataSetCustom.put("cmSource3ID", "BISON");
			dataSetCustom.put("leadOwnerID2", corpUser.getuserFullName());

			printInfo = fillFormDataWithDataset(driver, config, testCaseIdInternal, dataSetCustom);

			if (printInfo != null && printInfo.size() > 0) {
				// System.out.println("printInfo=First="+printInfo);
			}

			fc.utobj().clickElement(driver, pobj.leadsLink);

			systemExactSearch(driver, firstName + " " + lastName);

			fc.utobj().printTestStep("Verify Lead Name in Lead Summary.");
			boolean isLeadNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//*[contains(text () ,'" + firstName + " " + lastName + "')]");
			if (isLeadNamePresent == false) {
				fc.utobj().throwsException("was not able to verify First Lead Name");
			}

			// Convert Leads
			fc.utobj().printTestStep("Convert Lead");
			String regionName = fc.utobj().generateTestData("Conregion");
			fc.utobj().actionImgOption(driver, firstName + " " + lastName, "Convert");
			if (!fc.utobj().isSelected(driver, pobj.leadOwnerRadio)) {
				fc.utobj().clickElement(driver, pobj.leadOwnerRadio);
			}
			String contactFName = fc.utobj().generateTestData("Confirst");
			fc.utobj().sendKeys(driver, pobj.contactFirstName, contactFName);
			String contactLName = fc.utobj().generateTestData("Conlast");
			fc.utobj().sendKeys(driver, pobj.contactLastName, contactLName);
			String accountName = fc.utobj().generateTestData("Conaccount");
			fc.utobj().sendKeys(driver, pobj.accountName, accountName);
			fc.utobj().selectValFromMultiSelect(driver, pobj.groupSelect, groupName);
			fc.utobj().clickElement(driver, pobj.convertBtn);
			fc.utobj().clickElement(driver, pobj.goToLeadBtn);

			// verify at lead page
			fc.utobj().printTestStep("Verify Convert Lead At Lead Page");
			searchLeadByOwner(driver, corpUser.getuserFullName(), "Converted Leads");

			boolean convertedLead = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firstName + " " + lastName + "')]");
			if (convertedLead == false) {
				fc.utobj().throwsException("was not able to verify Converted Lead At Leads Home Page");
			}
			boolean owneOfLead = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[.='" + firstName + " " + lastName
					+ "']/ancestor::tr/td[contains(text () ,'" + corpUser.getuserFullName() + "')]");
			if (owneOfLead == false) {
				fc.utobj().throwsException("was not able to verify Owner of lead After Conversion");
			}

			fc.utobj().clickElement(driver, driver
					.findElement(By.xpath(".//a[contains(text () ,'" + firstName + " " + lastName + "')]/../img")));

			boolean nameOfContact = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[.='Mr. " + contactFName + " " + contactLName + "']");
			if (nameOfContact == false) {
				fc.utobj().throwsException("was not able to verify name of Contatct");
			}
			fc.utobj().clickElement(driver, pobj.acountInfoTab);
			boolean accountNameText = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//td[contains(text () ,'" + accountName + "')]");
			if (accountNameText == false) {
				fc.utobj().throwsException("was not able to verify Account Name");
			}
			fc.utobj().clickElement(driver, pobj.accountsLink);

			fc.utobj().sendKeys(driver, pobj.searchAccounts, accountName);

			fc.utobj().clickElement(driver, pobj.systemSearchButton);
			boolean isAccountNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + accountName + "')]");
			if (isAccountNamePresent == false) {
				fc.utobj().throwsException("was not able to verify account name at Accounts page");
			}

			// verify number of contact
			fc.utobj().clickElement(driver, driver
					.findElement(By.xpath(".//a[contains(text () ,'" + accountName + "')]/ancestor::tr/td/a[.='1']")));

			// verify Contact Info
			boolean isContactNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + contactFName + " " + contactLName + "')]");
			if (isContactNamePresent == false) {
				fc.utobj().throwsException("was not able to verify contact Name at Contact Page");
			}
			boolean isOwnerPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[contains(text () ,'" + contactFName + " "
					+ contactLName + "')]/ancestor::tr/td[contains(text () ,'" + corpUser.getuserFullName() + "')]");
			if (isOwnerPresent == false) {
				fc.utobj().throwsException("was not able to verify owner at Contact Page");
			}

			fc.utobj().clickElement(driver, driver
					.findElement(By.xpath(".//a[contains(text () ,'" + contactFName + " " + contactLName + "')]")));
			fc.utobj().printTestStep("Verify Group at Contact Page");
			boolean isGroupPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () ,'Group')]/following-sibling::td[contains(text () ,'" + groupName + "')]");
			if (isGroupPresent == false) {
				fc.utobj().throwsException("was not able to verify Group at Contact Page");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "crm", "TC_Lead_CRM_ConvertStatusCase" })
	@TestCase(createdOn = "2018-01-16", updatedOn = "2018-01-16", testCaseId = "TC_Lead_CRM_ConvertStatusCase", testCaseDescription = "To Verify leads status from drop down on convert Lead Page.")
	private void crmInfoFillAreaConvert_status() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		Map<String, String> dataSetCustom = null;
		String testCaseIdInternal = "" + testCaseId;
		Map<String, String> printInfo = null;
		try {
			driver = fc.loginpage().login(driver);
			CRMLeadsPage pobj = new CRMLeadsPage(driver);

			fc.utobj().printTestStep("Navigate To Admin > CRM > Configure Status Page");
			fc.utobj().printTestStep("Add Contact Status");
			String contactStatus = fc.utobj().generateTestData("Constat");
			AdminCRMConfigureStatusPageTest statusPage = new AdminCRMConfigureStatusPageTest();
			statusPage.addStatus01(driver, contactStatus);

			fc.utobj().printTestStep("Navigate To Admin Users Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			String emailId = "crmautomation@staffex.com";
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUser = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporateUser.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			fc.utobj().printTestStep("Navigate To CRM > Leads > Leads Summary");
			fc.utobj().printTestStep("Add First Lead");
			fc.crm().crm_common().CRMLeadsLnk(driver);
			fc.utobj().clickElement(driver, pobj.addNew);
			fc.utobj().clickElement(driver, pobj.leadLnk);

			testCaseIdInternal = "TC_CRM_QA_Lead_Details";
			dataSetCustom = fc.utobj().readTestData("crm", testCaseIdInternal);
			String firstName = fc.utobj().generateTestData("Aa");
			String lastName = fc.utobj().generateTestData("Bb");
			dataSetCustom.put("leadFirstName", firstName);
			dataSetCustom.put("leadLastName", lastName);
			dataSetCustom.put("cmLeadTypeID", "Select");
			dataSetCustom.put("cmSource2ID", "Internet");
			dataSetCustom.put("cmSource3ID", "BISON");
			dataSetCustom.put("leadOwnerID2", corpUser.getuserFullName());

			printInfo = fillFormDataWithDataset(driver, config, testCaseIdInternal, dataSetCustom);

			if (printInfo != null && printInfo.size() > 0) {
				// System.out.println("printInfo=First="+printInfo);
			}

			fc.utobj().clickElement(driver, pobj.leadsLink);

			systemExactSearch(driver, firstName + " " + lastName);

			fc.utobj().printTestStep("Verify Lead Name in Lead Summary.");
			boolean isLeadNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//*[contains(text () ,'" + firstName + " " + lastName + "')]");
			if (isLeadNamePresent == false) {
				fc.utobj().throwsException("was not able to verify First Lead Name");
			}

			// Convert Leads
			fc.utobj().printTestStep("Convert Lead");
			String regionName = fc.utobj().generateTestData("Conregion");
			fc.utobj().actionImgOption(driver, firstName + " " + lastName, "Convert");
			if (!fc.utobj().isSelected(driver, pobj.leadOwnerRadio)) {
				fc.utobj().clickElement(driver, pobj.leadOwnerRadio);
			}
			String contactFName = fc.utobj().generateTestData("Confirst");
			fc.utobj().sendKeys(driver, pobj.contactFirstName, contactFName);
			String contactLName = fc.utobj().generateTestData("Conlast");
			fc.utobj().sendKeys(driver, pobj.contactLastName, contactLName);
			String accountName = fc.utobj().generateTestData("Conaccount");
			fc.utobj().sendKeys(driver, pobj.accountName, accountName);
			fc.utobj().selectDropDown(driver, fc.utobj().getElementByID(driver, "leadStatus"), contactStatus);
			fc.utobj().clickElement(driver, pobj.convertBtn);
			fc.utobj().clickElement(driver, pobj.goToLeadBtn);

			// verify at lead page
			fc.utobj().printTestStep("Verify Convert Lead At Lead Page");
			searchLeadByOwner(driver, corpUser.getuserFullName(), "Converted Leads");

			boolean convertedLead = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firstName + " " + lastName + "')]");
			if (convertedLead == false) {
				fc.utobj().throwsException("was not able to verify Converted Lead At Leads Home Page");
			}
			boolean owneOfLead = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[.='" + firstName + " " + lastName
					+ "']/ancestor::tr/td[contains(text () ,'" + corpUser.getuserFullName() + "')]");
			if (owneOfLead == false) {
				fc.utobj().throwsException("was not able to verify Owner of lead After Conversion");
			}

			fc.utobj().clickElement(driver, driver
					.findElement(By.xpath(".//a[contains(text () ,'" + firstName + " " + lastName + "')]/../img")));

			boolean nameOfContact = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[.='Mr. " + contactFName + " " + contactLName + "']");
			if (nameOfContact == false) {
				fc.utobj().throwsException("was not able to verify name of Contatct");
			}
			fc.utobj().clickElement(driver, pobj.acountInfoTab);
			boolean accountNameText = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//td[contains(text () ,'" + accountName + "')]");
			if (accountNameText == false) {
				fc.utobj().throwsException("was not able to verify Account Name");
			}
			fc.utobj().clickElement(driver, pobj.accountsLink);

			fc.utobj().sendKeys(driver, pobj.searchAccounts, accountName);

			fc.utobj().clickElement(driver, pobj.systemSearchButton);
			boolean isAccountNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + accountName + "')]");
			if (isAccountNamePresent == false) {
				fc.utobj().throwsException("was not able to verify account name at Accounts page");
			}

			// verify number of contact
			fc.utobj().clickElement(driver, driver
					.findElement(By.xpath(".//a[contains(text () ,'" + accountName + "')]/ancestor::tr/td/a[.='1']")));

			// verify Contact Info
			boolean isContactNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + contactFName + " " + contactLName + "')]");
			if (isContactNamePresent == false) {
				fc.utobj().throwsException("was not able to verify contact Name at Contact Page");
			}
			boolean isOwnerPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[contains(text () ,'" + contactFName + " "
					+ contactLName + "')]/ancestor::tr/td[contains(text () ,'" + corpUser.getuserFullName() + "')]");
			if (isOwnerPresent == false) {
				fc.utobj().throwsException("was not able to verify owner at Contact Page");
			}

			fc.utobj().clickElement(driver, driver
					.findElement(By.xpath(".//a[contains(text () ,'" + contactFName + " " + contactLName + "')]")));
			fc.utobj().printTestStep("Verify Status at Contact Page");

			WebElement staus = fc.utobj().getElementByID(driver, "cmLeadStatusID");
			String resulantstatts = fc.utobj().getSelectedDropDownValue(driver, staus);

			if (resulantstatts != null && resulantstatts.equals(contactStatus)) {

			} else {
				fc.utobj().throwsException("was not able to verify Status at Contact Page");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "crm", "TC_Lead_CRM_ConvertContactTyprCase" })
	@TestCase(createdOn = "2018-01-16", updatedOn = "2018-01-16", testCaseId = "TC_Lead_CRM_ConvertContactTyprCase", testCaseDescription = "To Verify leads Contact Type from drop down on convert Lead Page.")
	private void crmInfoFillAreaConvert_contactType() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		Map<String, String> dataSetCustom = null;
		String testCaseIdInternal = "" + testCaseId;
		Map<String, String> printInfo = null;
		try {
			driver = fc.loginpage().login(driver);
			CRMLeadsPage pobj = new CRMLeadsPage(driver);

			AdminCRMContactTypeConfigurationPage npobj = new AdminCRMContactTypeConfigurationPage(driver);
			fc.utobj().printTestStep("Navigating to Admin > CRM > ContactTypeConfiguration");
			fc.utobj().printTestStep("Add Contact Type");
			fc.crm().crm_common().adminCRMContactTypeConfigurationLnk(driver);
			fc.utobj().clickElement(driver, npobj.addContactType);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			String contactType = fc.utobj().generateTestData("contype");
			fc.utobj().sendKeys(driver, driver.findElement(By.name("contactTypeName")), contactType);
			fc.utobj().clickElement(driver, npobj.addBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Verify Contact Type");
			boolean isTextPresent = fc.utobj().assertPageSource(driver, contactType);
			if (isTextPresent == false) {
				fc.utobj().throwsException("was not able to add Contact Type");
			}

			fc.utobj().printTestStep("Navigate To Admin Users Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			String emailId = "crmautomation@staffex.com";
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUser = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporateUser.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			fc.utobj().printTestStep("Navigate To CRM > Leads > Leads Summary");
			fc.utobj().printTestStep("Add First Lead");
			fc.crm().crm_common().CRMLeadsLnk(driver);
			fc.utobj().clickElement(driver, pobj.addNew);
			fc.utobj().clickElement(driver, pobj.leadLnk);

			testCaseIdInternal = "TC_CRM_QA_Lead_Details";
			dataSetCustom = fc.utobj().readTestData("crm", testCaseIdInternal);
			String firstName = fc.utobj().generateTestData("Aa");
			String lastName = fc.utobj().generateTestData("Bb");
			dataSetCustom.put("leadFirstName", firstName);
			dataSetCustom.put("leadLastName", lastName);
			dataSetCustom.put("cmLeadTypeID", "Select");
			dataSetCustom.put("cmSource2ID", "Internet");
			dataSetCustom.put("cmSource3ID", "BISON");
			dataSetCustom.put("leadOwnerID2", corpUser.getuserFullName());

			printInfo = fillFormDataWithDataset(driver, config, testCaseIdInternal, dataSetCustom);

			if (printInfo != null && printInfo.size() > 0) {
				// System.out.println("printInfo=First="+printInfo);
			}

			fc.utobj().clickElement(driver, pobj.leadsLink);

			systemExactSearch(driver, firstName + " " + lastName);

			fc.utobj().printTestStep("Verify Lead Name in Lead Summary.");
			boolean isLeadNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//*[contains(text () ,'" + firstName + " " + lastName + "')]");
			if (isLeadNamePresent == false) {
				fc.utobj().throwsException("was not able to verify First Lead Name");
			}

			// Convert Leads
			fc.utobj().printTestStep("Convert Lead");
			String regionName = fc.utobj().generateTestData("Conregion");
			fc.utobj().actionImgOption(driver, firstName + " " + lastName, "Convert");
			if (!fc.utobj().isSelected(driver, pobj.leadOwnerRadio)) {
				fc.utobj().clickElement(driver, pobj.leadOwnerRadio);
			}
			String contactFName = fc.utobj().generateTestData("Confirst");
			fc.utobj().sendKeys(driver, pobj.contactFirstName, contactFName);
			String contactLName = fc.utobj().generateTestData("Conlast");
			fc.utobj().sendKeys(driver, pobj.contactLastName, contactLName);
			String accountName = fc.utobj().generateTestData("Conaccount");
			fc.utobj().sendKeys(driver, pobj.accountName, accountName);
			fc.utobj().selectDropDown(driver, fc.utobj().getElementByID(driver, "contactType"), contactType);
			fc.utobj().clickElement(driver, pobj.convertBtn);
			fc.utobj().clickElement(driver, pobj.goToLeadBtn);

			// verify at lead page
			fc.utobj().printTestStep("Verify Convert Lead At Lead Page");
			searchLeadByOwner(driver, corpUser.getuserFullName(), "Converted Leads");

			boolean convertedLead = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firstName + " " + lastName + "')]");
			if (convertedLead == false) {
				fc.utobj().throwsException("was not able to verify Converted Lead At Leads Home Page");
			}
			boolean owneOfLead = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[.='" + firstName + " " + lastName
					+ "']/ancestor::tr/td[contains(text () ,'" + corpUser.getuserFullName() + "')]");
			if (owneOfLead == false) {
				fc.utobj().throwsException("was not able to verify Owner of lead After Conversion");
			}

			fc.utobj().clickElement(driver, driver
					.findElement(By.xpath(".//a[contains(text () ,'" + firstName + " " + lastName + "')]/../img")));

			boolean nameOfContact = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[.='Mr. " + contactFName + " " + contactLName + "']");
			if (nameOfContact == false) {
				fc.utobj().throwsException("was not able to verify name of Contatct");
			}
			fc.utobj().clickElement(driver, pobj.acountInfoTab);
			boolean accountNameText = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//td[contains(text () ,'" + accountName + "')]");
			if (accountNameText == false) {
				fc.utobj().throwsException("was not able to verify Account Name");
			}
			fc.utobj().clickElement(driver, pobj.accountsLink);

			fc.utobj().sendKeys(driver, pobj.searchAccounts, accountName);

			fc.utobj().clickElement(driver, pobj.systemSearchButton);
			boolean isAccountNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + accountName + "')]");
			if (isAccountNamePresent == false) {
				fc.utobj().throwsException("was not able to verify account name at Accounts page");
			}

			// verify number of contact
			fc.utobj().clickElement(driver, driver
					.findElement(By.xpath(".//a[contains(text () ,'" + accountName + "')]/ancestor::tr/td/a[.='1']")));

			// verify Contact Info
			boolean isContactNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + contactFName + " " + contactLName + "')]");
			if (isContactNamePresent == false) {
				fc.utobj().throwsException("was not able to verify contact Name at Contact Page");
			}
			boolean isOwnerPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[contains(text () ,'" + contactFName + " "
					+ contactLName + "')]/ancestor::tr/td[contains(text () ,'" + corpUser.getuserFullName() + "')]");
			if (isOwnerPresent == false) {
				fc.utobj().throwsException("was not able to verify owner at Contact Page");
			}

			fc.utobj().clickElement(driver, driver
					.findElement(By.xpath(".//a[contains(text () ,'" + contactFName + " " + contactLName + "')]")));
			fc.utobj().printTestStep("Verify Contact Type at Contact Page");

			boolean isContactTYpePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () ,'Contact Type')]/following-sibling::td[contains(text () ,'" + contactType
							+ "')]");

			if (isContactTYpePresent == false) {
				fc.utobj().throwsException("was not able to Verify Contact Type at Contact Page");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "crm", "TC_Lead_CRM_ConvertCompnayCase" })
	@TestCase(createdOn = "2018-01-16", updatedOn = "2018-01-16", testCaseId = "TC_Lead_CRM_ConvertCompnayCase", testCaseDescription = "To Verify Convert leads behaviours if Company name is not entered.")
	private void crmInfoFillAreaConvert_compnayName() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		Map<String, String> dataSetCustom = null;
		String testCaseIdInternal = "" + testCaseId;
		Map<String, String> printInfo = null;
		try {
			driver = fc.loginpage().login(driver);
			CRMLeadsPage pobj = new CRMLeadsPage(driver);

			fc.utobj().printTestStep("Navigate To Admin Users Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			String emailId = "crmautomation@staffex.com";
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUser = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporateUser.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			fc.utobj().printTestStep("Navigate To CRM > Leads > Leads Summary");
			fc.utobj().printTestStep("Add First Lead");
			fc.crm().crm_common().CRMLeadsLnk(driver);
			fc.utobj().clickElement(driver, pobj.addNew);
			fc.utobj().clickElement(driver, pobj.leadLnk);

			testCaseIdInternal = "TC_CRM_QA_Lead_Details";
			dataSetCustom = fc.utobj().readTestData("crm", testCaseIdInternal);
			String firstName = fc.utobj().generateTestData("Aa");
			String lastName = fc.utobj().generateTestData("Bb");
			dataSetCustom.put("leadFirstName", firstName);
			dataSetCustom.put("leadLastName", lastName);
			dataSetCustom.put("companyName", "");
			dataSetCustom.put("cmLeadTypeID", "Select");
			dataSetCustom.put("cmSource2ID", "Internet");
			dataSetCustom.put("cmSource3ID", "BISON");
			dataSetCustom.put("leadOwnerID2", corpUser.getuserFullName());

			printInfo = fillFormDataWithDataset(driver, config, testCaseIdInternal, dataSetCustom);

			if (printInfo != null && printInfo.size() > 0) {
				// System.out.println("printInfo=First="+printInfo);
			}

			fc.utobj().clickElement(driver, pobj.leadsLink);

			systemExactSearch(driver, firstName + " " + lastName);

			fc.utobj().printTestStep("Verify Lead Name in Lead Summary.");
			boolean isLeadNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//*[contains(text () ,'" + firstName + " " + lastName + "')]");
			if (isLeadNamePresent == false) {
				fc.utobj().throwsException("was not able to verify First Lead Name");
			}

			// Convert Leads
			fc.utobj().printTestStep("Convert Lead");
			String regionName = fc.utobj().generateTestData("Conregion");
			fc.utobj().actionImgOption(driver, firstName + " " + lastName, "Convert");
			if (!fc.utobj().isSelected(driver, pobj.leadOwnerRadio)) {
				fc.utobj().clickElement(driver, pobj.leadOwnerRadio);
			}
			String contactFName = fc.utobj().generateTestData("Confirst");
			fc.utobj().sendKeys(driver, pobj.contactFirstName, contactFName);
			String contactLName = fc.utobj().generateTestData("Conlast");
			fc.utobj().sendKeys(driver, pobj.contactLastName, contactLName);
			// String accountName = fc.utobj().generateTestData("Conaccount");
			// fc.utobj().sendKeys(driver, pobj.accountName, accountName);
			fc.utobj().clickElement(driver, pobj.convertBtn);
			fc.utobj().clickElement(driver, pobj.goToLeadBtn);

			// verify at lead page
			fc.utobj().printTestStep("Verify Convert Lead At Lead Page");
			searchLeadByOwner(driver, corpUser.getuserFullName(), "Converted Leads");

			boolean convertedLead = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firstName + " " + lastName + "')]");
			if (convertedLead == false) {
				fc.utobj().throwsException("was not able to verify Converted Lead At Leads Home Page");
			}
			boolean owneOfLead = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[contains(text () ,'" + firstName + " " + lastName
					+ "')]/ancestor::tr/td[contains(text () ,'" + corpUser.getuserFullName() + "')]");
			if (owneOfLead == false) {
				fc.utobj().throwsException("was not able to verify Owner of lead After Conversion");
			}

			fc.utobj().clickElement(driver, driver
					.findElement(By.xpath(".//a[contains(text () ,'" + firstName + " " + lastName + "')]/../img")));

			boolean nameOfContact = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[.='Mr. " + contactFName + " " + contactLName + "']");
			if (nameOfContact == false) {
				fc.utobj().throwsException("was not able to verify name of Contatct");
			}
			fc.utobj().clickElement(driver, pobj.acountInfoTab);
			fc.utobj().printTestStep("Verify Account if Company name is not entered no account is getting created.");
			boolean accountNameText = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//*[@id='accountName']");
			if (accountNameText == false) {
				fc.utobj().throwsException("was not able to verify Account if Company name is not entered.");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	public void searchLeadByOwner(WebDriver driver, String userName, String leadTypeView) throws Exception {

		CRMLeadsPage pobj = new CRMLeadsPage(driver);
		fc.utobj().clickElement(driver, pobj.showFilter);
		fc.utobj().setToDefault(driver, pobj.leadOwnerSelect);
		try {
			WebElement divsion = fc.utobj().getElementByXpath(driver, ".//*[@id='ms-parentdivisionID']");
			fc.utobj().setToDefault(driver, divsion);
		} catch (Exception e) {
		}
		fc.utobj()
				.selectValFromMultiSelect(driver, pobj.leadOwnerSelect, fc.utobj()
						.getElement(driver, pobj.leadOwnerSelect).findElement(By.xpath("./div/div/input")),
						userName);
		fc.utobj().setToDefault(driver, pobj.franchiseSelect);
		fc.utobj().setToDefault(driver, pobj.statusSelect);
		fc.utobj().selectDropDown(driver, pobj.addDateSelect, "All");
		fc.utobj().setToDefault(driver, pobj.emailStatusSelect);
		fc.utobj().selectDropDown(driver, pobj.viewLeadsSelect, leadTypeView);
		fc.utobj().clickElement(driver, pobj.searchBtn);
		;
		fc.utobj().clickElement(driver, pobj.hideFilter);
	}

	public void searchLeadByOwnerDiffrent(WebDriver driver, String userName, String leadTypeView, List<String> listInfo)
			throws Exception {

		CRMLeadsPage pobj = new CRMLeadsPage(driver);
		fc.utobj().clickElement(driver, pobj.showFilter);
		fc.utobj().setToDefault(driver, pobj.leadOwnerSelect);
		try {
			WebElement divsion = fc.utobj().getElementByXpath(driver, ".//*[@id='ms-parentdivisionID']");
			fc.utobj().setToDefault(driver, divsion);
		} catch (Exception e) {
		}
		fc.utobj().selectMultipleValFromMultiSelect(driver,
				fc.utobj().getElementByXpath(driver, ".//*[@id='ms-parentleadOwner']"), listInfo);
		// fc.utobj().selectValFromMultiSelect(driver,
		// pobj.leadOwnerSelect,fc.utobj().getElement(driver,
		// pobj.leadOwnerSelect).findElement(By.xpath("./div/div/input")),
		// userName);
		fc.utobj().setToDefault(driver, pobj.franchiseSelect);
		fc.utobj().setToDefault(driver, pobj.statusSelect);
		fc.utobj().selectDropDown(driver, pobj.addDateSelect, "All");
		fc.utobj().setToDefault(driver, pobj.emailStatusSelect);
		fc.utobj().selectDropDown(driver, pobj.viewLeadsSelect, leadTypeView);
		fc.utobj().clickElement(driver, pobj.searchBtn);
		;
		fc.utobj().clickElement(driver, pobj.hideFilter);
	}

	// system search
	public void systemExactSearch(WebDriver driver, String leadName) throws Exception {
		// search By System Search Button
		CRMLeadsPage pobj = new CRMLeadsPage(driver);
		fc.utobj().sendKeys(driver, pobj.searchLeads, leadName);
		// fc.utobj().clickElement(driver, pobj.exactSearch);
		fc.utobj().clickElement(driver, pobj.systemSearchButton);
	}

	public Map<String, String> fillFormDataWithDataset(WebDriver driver, Map<String, String> config, String testCaseId,
			Map<String, String> dataSet) throws Exception {
		Reporter.log("Filling TestCase Data = " + testCaseId);
		String id = "id";

		List<String> listItems = new ArrayList<String>();
		Map<String, String> printInfo = new HashMap<String, String>();

		List<WebElement> mandateBox = null;
		mandateBox = driver.findElements(By.xpath(
				".//td[@qat_tableid='addData']//*[@type='text' or @class='fTextBoxDate' or @class='fTextBox' or @class='multiList' or @type='radio' or @type='checkbox' or @type='checkBox' or @id='ms-parentsellerCombo' or @class='ms-choice' or @class='form-control']"));

		Map<String, String> alreadyitrateElement = new HashMap<>();
		Map<String, String> alreadyradio = new HashMap<>();
		Map<String, String> alreadycheckBox = new HashMap<>();

		try {
			for (WebElement webElement : mandateBox) {

				if (webElement.getAttribute("" + id) != null && !"".equals(webElement.getAttribute("" + id))
						&& !"null".equals(webElement.getAttribute("" + id))
						&& !":".equals(webElement.getAttribute("" + id))
						&& !"dateOfOpen".equals(webElement.getAttribute("" + id))
						&& !"fimCbOtherComplaintType".equals(webElement.getAttribute("" + id))) {
					/*
					 * try{
					 * System.out.println("Type==="+webElement.getAttribute(
					 * "type")+"====values==="+webElement.getAttribute("value")
					 * +"======"+dataSet.get(""+webElement.getAttribute(""+id))
					 * +"====="+""+webElement.getAttribute(""+id));
					 * }catch(Exception e) {
					 * 
					 * }
					 */

					if (alreadyitrateElement.containsKey(webElement.getAttribute("" + id))) {

					} else {
						if (dataSet.containsKey(webElement.getAttribute("" + id))) {
							WebElement elementmovepostion = null;
							if ("name".equals(id)) {
								elementmovepostion = driver.findElement(By.name("" + webElement.getAttribute("" + id)));

							} else {
								elementmovepostion = fc.utobj().getElementByID(driver,
										"" + webElement.getAttribute("" + id));

							}
							fc.utobj().moveToElement(driver, elementmovepostion);
							if (webElement.getAttribute("type").indexOf("select-one") != -1) {
								if (webElement.getAttribute("" + id) != null
										&& !"cmSource2ID".equals(webElement.getAttribute("" + id))
										&& !"cmSource3ID".equals(webElement.getAttribute("" + id)))

								{
									try {

										Select singledrop = null;
										List<WebElement> singleDropDown = null;
										try {
											singledrop = new Select(fc.utobj().getElementByID(driver,
													"" + webElement.getAttribute("" + id)));
											singleDropDown = driver
													.findElements(By.id("" + webElement.getAttribute("" + id)));
										} catch (Exception e) {
											singledrop = new Select(
													driver.findElement(By.name("" + webElement.getAttribute("" + id))));
											singleDropDown = driver
													.findElements(By.name("" + webElement.getAttribute("" + id)));
										}
										if (webElement.getAttribute("value") == null
												|| "".equals(webElement.getAttribute("value"))
												|| "null".equals(webElement.getAttribute("value"))
												|| "-1".equals(webElement.getAttribute("value"))) {

											int size = singleDropDown.size();
											if (dataSet.get("" + webElement.getAttribute("" + id)) != null
													&& !"".equals(dataSet.get("" + webElement.getAttribute("" + id)))) {
												if (size >= 1) {
													try {
														WebElement dropDownField = driver
																.findElement(By.id("" + webElement.getAttribute("id")));
														// fc.utobj().moveToElement(driver,
														// dropDownField);
														fc.utobj().selectDropDownByVisibleText(driver, dropDownField,
																dataSet.get("" + webElement.getAttribute("" + id)));

														if (!"birthMonth".equals(webElement.getAttribute("" + id))
																&& !"birthDate".equals(webElement.getAttribute("" + id))
																&& !"spouseBirthMonth"
																		.equals(webElement.getAttribute("" + id))
																&& !"spouseBirthDate"
																		.equals(webElement.getAttribute("" + id))) {
															listItems.add(
																	dataSet.get("" + webElement.getAttribute("" + id)));
														}
														printInfo.put("" + webElement.getAttribute("" + id),
																dataSet.get("" + webElement.getAttribute("" + id)));

													} catch (Exception eradio) {
														// Reporter.log("Problem
														// in selecting drop :
														// "+""+webElement.getAttribute("id")
														// +" Values getting
														// from Excel not match
														// in desired
														// fomrat."+""+dataSet.get(""+webElement.getAttribute("id")));
													}
												}
											}
										}

									} catch (Exception e3) {

									}
								}

							} else if (webElement.getAttribute("type").indexOf("select-multiple") != -1) {
								if (webElement.getAttribute("value") == null
										|| "".equals(webElement.getAttribute("value"))
										|| "null".equals(webElement.getAttribute("value"))
										|| "-1".equals(webElement.getAttribute("value"))) {
									try {
										WebElement element = driver
												.findElement(By.id("ms-parent" + webElement.getAttribute("" + id)));
										fc.utobj().clickElement(driver, element);
										fc.utobj().clickElement(driver,
												fc.utobj().getElementByXpath(driver, ".//*[@id='selectAll']"));
										fc.utobj().clickElement(driver, element);
									} catch (Exception emultipal) {
										// Reporter.log("Problem in multiple
										// value drop-down :
										// "+""+webElement.getAttribute("id"));
									}
								}
							} else if (webElement.getAttribute("type").indexOf("textarea") != -1) {
								if (webElement.getAttribute("value") == null
										|| "".equals(webElement.getAttribute("value"))
										|| "null".equals(webElement.getAttribute("value"))) {
									try {
										driver.findElement(By.name("" + webElement.getAttribute("" + id)))
												.sendKeys(dataSet.get("" + webElement.getAttribute("" + id)));
										listItems.add(dataSet.get("" + webElement.getAttribute("" + id)));
										printInfo.put("" + webElement.getAttribute("id"),
												dataSet.get("" + webElement.getAttribute("" + id)));
									} catch (Exception eText) {
										// Reporter.log("Problem in entering
										// TextArea in :
										// "+""+webElement.getAttribute("id"));
									}
								}
							} else if (webElement.getAttribute("type").indexOf("radio") != -1) {
								try {
									String sValue = "";
									List<WebElement> rdBtn_Sex = driver
											.findElements(By.id("" + webElement.getAttribute("" + id))); //
									if (alreadyradio.containsKey(webElement.getAttribute("" + id))) {

									} else {

										int size = rdBtn_Sex.size();
										if (size > 1) {
											sValue = dataSet.get("" + webElement.getAttribute("" + id));
										}
										try {
											List<WebElement> rdBtn_Field = null;
											WebElement element = null;
											try {
												rdBtn_Field = driver.findElements(By.xpath(
														".//*[@name='" + webElement.getAttribute("" + id) + "']")); // work
												element = fc.utobj().getElementByXpath(driver,
														".//*[@name='" + webElement.getAttribute("" + id) + "']");
											} catch (Exception eTxt) {
												rdBtn_Field = driver.findElements(
														By.xpath(".//*[@id='" + webElement.getAttribute("id") + "']")); // work
												element = driver.findElement(
														By.xpath(".//*[@id='" + webElement.getAttribute("id") + "']"));

											}

											fc.utobj().moveToElement(driver, element);
											fc.utobj().clickRadioButton(driver, rdBtn_Field, sValue); // work
											listItems.add(dataSet.get("" + webElement.getAttribute("" + id)));
											printInfo.put("" + webElement.getAttribute("id"),
													dataSet.get("" + webElement.getAttribute("" + id)));
											alreadyradio.put(webElement.getAttribute("" + id),
													webElement.getAttribute("" + id));
										} catch (Exception eText) {
											// Reporter.log("Problem in
											// selecting radio value in :
											// "+""+webElement.getAttribute("id"));
										}
									}
								} catch (Exception exp) {
									Reporter.log(exp.toString());
									exp.printStackTrace();
								}

							} else if (webElement.getAttribute("type").toLowerCase().indexOf("checkbox") != -1) {
								List<WebElement> oCheckBox = driver
										.findElements(By.name("" + webElement.getAttribute("" + id)));
								String sValue = "";
								if (alreadycheckBox.containsKey(webElement.getAttribute("" + id))) {

								} else {

									int size = oCheckBox.size();
									for (int i = 0; i < size; i++) {
										sValue = oCheckBox.get(i).getAttribute("value");
										// Thread.sleep(2000);
										if (i == (size - 1)) {

											// break;
										}

									}
									try {
										List<WebElement> rdBtn_Field = driver.findElements(By.xpath(
												"//*[contains(@id, '" + webElement.getAttribute("" + id) + "')]")); // work
										WebElement element = fc.utobj().getElementByXpath(driver,
												"//*[contains(@id, '" + webElement.getAttribute("" + id) + "')]");
										fc.utobj().moveToElement(driver, element);
										fc.utobj().clickRadioButton(driver, rdBtn_Field, sValue); // work
										alreadycheckBox.put(webElement.getAttribute("" + id),
												webElement.getAttribute("" + id));
									} catch (Exception eText) {

									}
								}
							} else if (webElement.getAttribute("type").indexOf("file") != -1) {
								String fileName = fc.utobj().getFilePathFromTestData("pictureFile");
								try {
									fc.utobj().sendKeys(driver,
											fc.utobj().getElementByID(driver, "" + webElement.getAttribute("" + id)),
											fileName);
									listItems.add(fileName);
									printInfo.put("" + webElement.getAttribute("" + id), fileName);
								} catch (Exception eFile) {
									// Reporter.log("Problem in uploading file
									// type value in :
									// "+""+webElement.getAttribute("id"));
								}
							} else if (webElement.getAttribute("type").indexOf("text") != -1) {
								if (webElement.getAttribute("value") == null
										|| "".equals(webElement.getAttribute("value"))
										|| "null".equals(webElement.getAttribute("value"))
										|| "0.00".equals(webElement.getAttribute("value"))) {
									if (webElement.getAttribute("class").indexOf("fTextBoxDate") != -1) {
										try {
											if (dataSet.get("" + webElement.getAttribute("" + id)) != null
													|| !"".equals(dataSet.get("" + webElement.getAttribute("" + id)))
													|| !"null".equals(
															dataSet.get("" + webElement.getAttribute("" + id)))) {
												fc.utobj().sendKeys(driver,
														driver.findElement(
																By.name("" + webElement.getAttribute("" + id))),
														dataSet.get("" + webElement.getAttribute("" + id))); // work
												listItems.add(dataSet.get("" + webElement.getAttribute("" + id)));
												printInfo.put("" + webElement.getAttribute("" + id),
														dataSet.get("" + webElement.getAttribute("" + id)));

											} else {

											}
										} catch (Exception eDate) {
											// Reporter.log("Problem in Entering
											// Date fields value in :
											// "+""+webElement.getAttribute("id"));
										}
									} else {
										try {
											if ("0.00".equals(webElement.getAttribute("value"))) {
												fc.utobj()
														.sendKeys(driver,
																driver.findElement(
																		By.name("" + webElement.getAttribute("" + id))),
																"");

												// System.out.println(webElement.getAttribute("value")
												// +"======"+dataSet.get(""+webElement.getAttribute(""+id))
												// +"====="+""+webElement.getAttribute(""+id));

											}
											if ("name".equals(id)) {
												fc.utobj().sendKeys(driver,
														driver.findElement(
																By.name("" + webElement.getAttribute("" + id))),
														dataSet.get("" + webElement.getAttribute("" + id)));
												if ("accountName".equals("" + webElement.getAttribute("" + id))
														|| "opportunityOwner"
																.equals("" + webElement.getAttribute("" + id))
														|| "oppContactID"
																.equals("" + webElement.getAttribute("" + id))) {
													;
													fc.utobj().clickElement(driver, driver.findElement(
															By.xpath(".//*[@id='customizedAjaxSearch']/div")));
													fc.utobj().sleep();
												}
											} else {
												fc.utobj().sendKeys(driver,
														driver.findElement(
																By.id("" + webElement.getAttribute("" + id))),
														dataSet.get("" + webElement.getAttribute("" + id)));
												if ("accountName".equals("" + webElement.getAttribute("" + id))
														|| "opportunityOwner"
																.equals("" + webElement.getAttribute("" + id))
														|| "oppContactID"
																.equals("" + webElement.getAttribute("" + id))) {
													;
													fc.utobj().clickElement(driver, driver.findElement(
															By.xpath(".//*[@id='customizedAjaxSearch']/div")));
													fc.utobj().sleep();
												}
											}

											if (!"ssn".equals("" + webElement.getAttribute("" + id))
													&& !"spouseSsn".equals("" + webElement.getAttribute("" + id))) {
												listItems.add(dataSet.get("" + webElement.getAttribute("" + id)));
											}
											printInfo.put("" + webElement.getAttribute("" + id),
													dataSet.get("" + webElement.getAttribute("" + id)));

										} catch (Exception eText) {
											// Reporter.log("Problem in Entering
											// Text fileds value in :
											// "+""+webElement.getAttribute("id"));
										}

									}
								}
							}
							alreadyitrateElement.put(webElement.getAttribute("" + id),
									webElement.getAttribute("" + id));
						}
					}
				}
			}

			if ("TC_CRM_QA_Lead_Details".equals(testCaseId)) {

				Select singledrop = null;
				List<WebElement> singleDropDown = null;
				try {
					singledrop = new Select(driver.findElement(By.name("leadOwnerID2")));
					singleDropDown = driver.findElements(By.name("leadOwnerID2"));
					WebElement dropDownField = driver.findElement(By.name("leadOwnerID2"));
					fc.utobj().selectDropDownByVisibleText(driver, dropDownField, dataSet.get("leadOwnerID2"));

					dropDownField = driver.findElement(By.name("cmSource2ID"));
					fc.utobj().selectDropDownByVisibleText(driver, dropDownField, dataSet.get("cmSource2ID"));
					;
					dropDownField = driver.findElement(By.name("cmSource3ID"));
					fc.utobj().selectDropDownByVisibleText(driver, dropDownField, dataSet.get("cmSource3ID"));

				} catch (Exception e) {

				}

			}
			if ("TC_CRM_QA_Contact_Details".equals(testCaseId)) {

				Select singledrop = null;
				List<WebElement> singleDropDown = null;
				try {
					singledrop = new Select(driver.findElement(By.name("contactOwnerID2")));
					singleDropDown = driver.findElements(By.name("contactOwnerID2"));
					WebElement dropDownField = driver.findElement(By.name("contactOwnerID2"));
					fc.utobj().selectDropDownByVisibleText(driver, dropDownField, dataSet.get("contactOwnerID2"));

					dropDownField = driver.findElement(By.name("cmSource2ID"));
					fc.utobj().selectDropDownByVisibleText(driver, dropDownField, dataSet.get("cmSource2ID"));
					;
					dropDownField = driver.findElement(By.name("cmSource3ID"));
					fc.utobj().selectDropDownByVisibleText(driver, dropDownField, dataSet.get("cmSource3ID"));

				} catch (Exception e) {

				}

			}

		} catch (Exception e) {
			// fc.utobj().throwsException("Fields data are not available!
			// "+e.getMessage());
		}
		try {
			// fc.utobj().printTestStep(testCaseId, "Submiting the Tab ");
			WebElement elementbutton = fc.utobj().getElementByXpath(driver,
					".//*[@id='Submit' or @name='Submit' or @name='button' or @name='Submit1' or @name='add1'] ");
			fc.utobj().clickElement(driver, elementbutton);

		} catch (Exception ee) {
			fc.utobj().printBugStatus("Unable to submit page " + ee);
		}

		return printInfo;
	}

	public void contactSearchFromSearchpage(WebDriver driver, String Firstname, String LastName) throws Exception {
		// search By System Search Button
		CRMLeadsPage pobj = new CRMLeadsPage(driver);
		fc.utobj().clickElement(driver, pobj.searchLink);
		fc.utobj().sendKeys(driver, pobj.contactSearchFirstName, Firstname);
		fc.utobj().sendKeys(driver, pobj.contactSearchLastName, LastName);
		fc.utobj().clickElement(driver, pobj.searchBtnAtSearch);
	}

	public void addLead(WebDriver driver, Map<String, String> dataSet, String firstName, String lastName,
			String company, String assignTo, String city, String address, String suffix, String jobTitle,
			String comment, String userName, String regionName, String regionalUser, String franchiseId,
			String franchiseUser, Map<String, String> config) throws Exception {

		String testCaseId = "TC_Add_Lead_At_Lead_Page";

		if (fc.utobj().validate(testCaseId)) {
			try {
				CRMLeadsPage pobj = new CRMLeadsPage(driver);
				fc.crm().crm_common().CRMLeadsLnk(driver);
				fc.utobj().clickElement(driver, pobj.addNew);
				fc.utobj().clickElement(driver, pobj.leadLnk);
				fc.utobj().selectDropDown(driver, pobj.title, "Mr.");
				fc.utobj().sendKeys(driver, pobj.leadFirstName, firstName);
				fc.utobj().sendKeys(driver, pobj.leadLastName, lastName);
				fc.utobj().sendKeys(driver, pobj.companyName, company);
				fc.utobj().selectDropDown(driver, pobj.primaryContactMethodSelect, "Email");

				if (assignTo.equalsIgnoreCase("Corporate")) {
					if (!fc.utobj().isSelected(driver, pobj.assignToCorporate)) {
						fc.utobj().clickElement(driver, pobj.assignToCorporate);
					}
					fc.utobj().selectDropDown(driver, pobj.selectCorporateUser, userName);
				} else if (assignTo.equalsIgnoreCase("Regional")) {
					if (!fc.utobj().isSelected(driver, pobj.assignToRegional)) {
						fc.utobj().clickElement(driver, pobj.assignToRegional);
					}
					fc.utobj().selectDropDown(driver, pobj.selectAreaRegion, regionName);
					;
					fc.utobj().selectDropDown(driver, pobj.selectRegionalUser, regionalUser);

				} else if (assignTo.equalsIgnoreCase("Franchise")) {
					if (!fc.utobj().isSelected(driver, pobj.assignToFranchise)) {
						fc.utobj().clickElement(driver, pobj.assignToFranchise);
					}
					fc.utobj().selectDropDown(driver, pobj.selectFranchiseId, franchiseId);
					fc.utobj().selectDropDown(driver, pobj.selectFranchiseUser, franchiseUser);
				} else if (assignTo.equalsIgnoreCase("Only Franchise")) {
					fc.utobj().selectDropDown(driver, pobj.selectFranchiseUser, franchiseUser);

				}
				fc.utobj().sendKeys(driver, pobj.address, address);
				fc.utobj().sendKeys(driver, pobj.city, city);
				fc.utobj().selectDropDown(driver, pobj.country, "USA");
				fc.utobj().selectDropDown(driver, pobj.state, "Alabama");
				fc.utobj().sendKeys(driver, pobj.zipcode, "12345");
				fc.utobj().sendKeys(driver, pobj.phoneNumbers, "1236547896");
				fc.utobj().sendKeys(driver, pobj.extn, "12");
				fc.utobj().sendKeys(driver, pobj.faxNumbers, "1263985487");
				fc.utobj().sendKeys(driver, pobj.mobileNumbers, "8896724441");
				
				String email = fc.utobj().generateTestData("crmautomation")+"@staffex.com";
				//String email = "crmautomation@staffex.com";
				fc.utobj().sendKeys(driver, pobj.emailIds, email);
				fc.utobj().sendKeys(driver, pobj.alternateEmail, "testautoamtion@franqa.com");
				fc.utobj().sendKeys(driver, pobj.suffix, suffix);
				fc.utobj().sendKeys(driver, pobj.position, jobTitle);
				fc.utobj().sendKeys(driver, pobj.birthdate, "11/24/1980");
				fc.utobj().sendKeys(driver, pobj.anniversarydate, "11/24/1999");
				fc.utobj().selectDropDown(driver, pobj.rating, "Hot");
				fc.utobj().sendKeys(driver, pobj.bestTimeToContact, "12:00 PM EST");
				fc.utobj().sendKeys(driver, pobj.comments, comment);
				fc.utobj().clickElement(driver, pobj.saveBtn);

				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException("Was not able to Add Lead :: CRM > Leads");
		}
	}

}