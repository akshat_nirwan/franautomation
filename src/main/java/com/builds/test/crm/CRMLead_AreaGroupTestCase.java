package com.builds.test.crm;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.xml.parsers.ParserConfigurationException;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.testng.Reporter;
import org.testng.annotations.Test;
import org.xml.sax.SAXException;

import com.builds.test.admin.AdminUsersManageCorporateUsersAddCorporateUserPageTest;
import com.builds.test.common.CorporateUser;
import com.builds.uimaps.crm.CRMLeadsPage;
import com.builds.utilities.FranconnectUtil;
import com.builds.utilities.TestCase;

public class CRMLead_AreaGroupTestCase {

	FranconnectUtil fc = new FranconnectUtil();

	@Test(groups = { "crm"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Lead Add To Group By Action Button Option At CRM > Lead > Lead Summary", testCaseId = "TC_70_Add_To_Group")
	public void addToGroupActionBtn() throws IOException, Exception, ParserConfigurationException, SAXException {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			CRMLeadsPage pobj = new CRMLeadsPage(driver);
			CRMGroupsPageTest groupPage = new CRMGroupsPageTest();

			/*
			 * fc.utobj().printTestStep(testCaseId,
			 * "Navigate To Admin Users Manage Corporate User");
			 * fc.utobj().printTestStep(testCaseId, "Add Corporate User");
			 * String emailId="crmautomation@staffex.com";
			 * AdminUsersManageCorporateUsersAddCorporateUserPageTest
			 * corporateUser=new
			 * AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			 * CorporateUser corpUser = new
			 * CorporateUser();corpUser.setEmail(emailId);corpUser =
			 * fc.CommonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			 * corporateUser.
			 * createDefaultCorporateUser_Through_WebService_AfterLogin(driver,
			 * corpUser);
			 */

			fc.utobj().printTestStep("Navigate To Admin Users Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			String emailId = "crmautomation@staffex.com";
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUser = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporateUser.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			String firstName = fc.utobj().generateTestData(dataSet.get("firstName"));
			String lastName = fc.utobj().generateTestData(dataSet.get("lastName"));
			String company = fc.utobj().generateTestData(dataSet.get("company"));
			String assignTo = "Corporate";
			String city = fc.utobj().generateTestData(dataSet.get("city"));
			String address = fc.utobj().generateTestData(dataSet.get("address"));
			String suffix = fc.utobj().generateTestData(dataSet.get("suffix"));
			String jobTitle = fc.utobj().generateTestData(dataSet.get("jobTitle"));
			String comment = fc.utobj().generateTestData(dataSet.get("comment"));
			String franchiseId = "";
			String franchiseUser = "";
			String userNameReg = "";
			String regionName = "";
			fc.utobj().printTestStep("Navigate To CRM > Leads > Leads Summary");
			fc.utobj().printTestStep("Add Lead");
			addLead(driver, dataSet, firstName, lastName, company, assignTo, city, address, suffix, jobTitle, comment,
					corpUser.getuserFullName(), regionName, userNameReg, franchiseId, franchiseUser, config);

			fc.utobj().clickElement(driver, pobj.leadsLink);
			searchLeadByOwner(driver, corpUser.getuserFullName(), "Open Leads");
			fc.utobj().printTestStep("Add Lead To Group");
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,(".//a[contains(text () ,'" + firstName + " " + lastName + "')]/ancestor::tr/td/input")));
			fc.utobj().selectActionMenuItemsWithTagA(driver, "Add To Group");

			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().clickElement(driver, pobj.addToNewGroup);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Create New Group");
			String groupName = fc.utobj().generateTestData(dataSet.get("groupName"));
			String accessibility = "Public to all Users";
			groupPage.addToGroupRegular(driver, groupName, accessibility);

			/*
			 * fc.utobj().sendKeys(driver, pobj.searchAtGroup, groupName);
			 * ; fc.utobj().clickElement(driver,
			 * pobj.exactSearch); fc.utobj().clickElement(driver,
			 * pobj.systemSearchButton);
			 */
			new CRMGroupsPageTest().searchGroupsByFilter(driver, groupName, "Lead");
			String colunmName = "";
			int z = 0;

			// click over Recipients
			List<WebElement> listHead = driver
					.findElements(By.xpath(".//a[.='" + groupName + "']/ancestor::table/thead/tr/th"));
			System.out.println(listHead);
			for (int i = 0; i < listHead.size(); i++) {

				colunmName = listHead.get(i).getText().trim();
				if (colunmName.equalsIgnoreCase("Recipients")) {
					z = i;
					break;
				}
			}

			if (z != 0) {
				z = z + 1;
				fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
						".//a[.='" + groupName + "']/ancestor::tr/td[" + z + "]/a"));
			} else {
				fc.utobj().throwsException("was not able to find Recipients Column");
			}
			searchLeadByOwner(driver, corpUser.getuserFullName(), "All Leads");

			fc.utobj().printTestStep("Verify Lead In Group");

			boolean isLeadNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firstName + " " + lastName + "')]");
			if (isLeadNamePresent == false) {
				fc.utobj().throwsException("was not able to add lead in group");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "crm", "crmLeadAreaConvert_Button"})
	@TestCase(createdOn = "2018-01-16", updatedOn = "2018-01-16", testCaseId = "TC_Lead_Area_CRM_GroupTestCase", testCaseDescription = "To verify that the crossponding buttons are appearing or not.")
	private void crmInfoFillAreaConvert_3() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		Map<String, String> dataSetCustom = null;
		String testCaseIdInternal = "" + testCaseId;
		Map<String, String> printInfo = null;
		try {
			driver = fc.loginpage().login(driver);
			CRMLeadsPage pobj = new CRMLeadsPage(driver);

			fc.utobj().printTestStep("Navigate To Admin Users Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			String emailId = "crmautomation@staffex.com";
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUser = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporateUser.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			fc.utobj().printTestStep("Navigate To CRM > Leads > Leads Summary");
			fc.utobj().printTestStep("Add First Lead");
			fc.crm().crm_common().CRMLeadsLnk(driver);
			fc.utobj().clickElement(driver, pobj.addNew);
			fc.utobj().clickElement(driver, pobj.leadLnk);

			testCaseIdInternal = "TC_CRM_QA_Lead_Details";
			dataSetCustom = fc.utobj().readTestData("crm", testCaseIdInternal);
			String firstName = fc.utobj().generateTestData("Aa");
			String lastName = fc.utobj().generateTestData("Bb");
			dataSetCustom.put("leadFirstName", firstName);
			dataSetCustom.put("leadLastName", lastName);
			dataSetCustom.put("cmLeadTypeID", "Select");
			dataSetCustom.put("cmSource2ID", "Internet");
			dataSetCustom.put("cmSource3ID", "BISON");
			dataSetCustom.put("leadOwnerID2", corpUser.getuserFullName());

			printInfo = fillFormDataWithDataset(driver, config, testCaseIdInternal, dataSetCustom);

			if (printInfo != null && printInfo.size() > 0) {
				// System.out.println("printInfo=First="+printInfo);
			}

			fc.utobj().clickElement(driver, pobj.leadsLink);

			systemExactSearch(driver, firstName + " " + lastName);

			fc.utobj().printTestStep("Verify Lead Name in Lead Summary.");
			boolean isLeadNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//*[contains(text () ,'" + firstName + " " + lastName + "')]");
			if (isLeadNamePresent == false) {
				fc.utobj().throwsException("was not able to verify First Lead Name");
			}

			fc.utobj().printTestStep(
					"To verify that the Convert button is appearing properly on the Lead Info Page beside the Change Status button.");
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//a[contains(text () ,'" + firstName + " " + lastName + "')]"));

			boolean isLeadConvertButtonPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//*[@name='convertLead']");
			if (isLeadConvertButtonPresent == false) {
				fc.utobj().throwsException("was not able to verify vert button is appearing properly.");
			} else {
				fc.utobj()
						.printTestStep("verified that the Convert button is appearing properly on the Lead Info Page.");
			}

			fc.utobj().printTestStep("To verify that the Print button is appearing properly on the Lead Info Page.");
			boolean isLeadPrintButtonPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//*[@type='button' and @value=' Print ']");
			if (isLeadPrintButtonPresent == false) {
				fc.utobj().throwsException("was not able to verify Print button is appearing properly.");
			} else {
				fc.utobj().printTestStep("verified that the Print button is appearing properly on the Lead Info Page.");
			}

			fc.utobj().printTestStep("To verify that the Print page is opening properly on the Lead Info Page.");

			try {
				fc.utobj().clickElement(driver,
						fc.utobj().getElementByXpath(driver, ".//*[@type='button' and @value=' Print ']"));
				VerifyPrintPreview(driver, firstName);

			} catch (Exception w) {
				fc.utobj().throwsException("was not able to verify Print functionality.");
			}

			fc.utobj().printTestStep(
					"To verify that the Associate With Campaign button is appearing properly on the Lead Info Page.");
			boolean isLeadAssociateCampaginButtonPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//*[@name='AssociateWithRegularCampaignButton']");
			if (isLeadAssociateCampaginButtonPresent == false) {
				fc.utobj().throwsException(
						"was not able to verify Associate With Campaign button is appearing properly.");
			} else {
				fc.utobj().printTestStep(
						"verified that the Associate With Campaign button is appearing properly on the Lead Info Page.");
			}

			fc.utobj().printTestStep(
					"To verify that the Associate With Campaign page is opening properly on the Lead Info Page.");

			try {
				fc.utobj().clickElement(driver,
						fc.utobj().getElementByXpath(driver, ".//*[@name='AssociateWithRegularCampaignButton']"));
				WebElement CampaignText = fc.utobj().getElementByXpath(driver, ".//*[@class='heading page-heading']");
				
				String TextVerify = CampaignText.getText().trim();
				if (TextVerify.indexOf("CAMPAIGNS") != -1) {

				} else {
					fc.utobj().throwsException("was not able to verify Associate With Campaign page Text.");
				}

			} catch (Exception w) {
				fc.utobj().throwsException("was not able to verify Associate With Campaign page.");
			}
			fc.utobj().clickElement(driver, pobj.leadsLink);

			systemExactSearch(driver, firstName + " " + lastName);

			fc.utobj().printTestStep("Verify Lead Name in Lead Summary.");
			isLeadNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//*[contains(text () ,'" + firstName + " " + lastName + "')]");
			if (isLeadNamePresent == false) {
				fc.utobj().throwsException("was not able to verify First Lead Name");
			}
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//a[contains(text () ,'" + firstName + " " + lastName + "')]"));

			fc.utobj().printTestStep(
					"To verify that the Add To Group button is appearing properly on the Lead Info Page.");
			boolean isLeadAddtoGroupButtonPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//*[@name='addToGroup']");
			if (isLeadAddtoGroupButtonPresent == false) {
				fc.utobj().throwsException("was not able to verify Add To Group button is appearing properly.");
			} else {
				fc.utobj().printTestStep(
						"verified that the Add To Group button is appearing properly on the Lead Info Page.");
			}

			fc.utobj().clickPartialLinkText(driver, "Modify");

			fc.utobj().printTestStep(
					"To verify that the Reset button is appearing properly on the Modify Lead Info Page.");
			boolean isLeadresetButtonPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//*[@id='resetButton']");
			if (isLeadresetButtonPresent == false) {
				fc.utobj().throwsException("was not able to verify Reset is appearing properly in Modify Lead.");
			} else {
				fc.utobj().printTestStep(
						"verified that the Reset button is appearing properly on Modify the Lead Info Page.");
			}
			fc.utobj().printTestStep(
					"To verify that the Back button is appearing properly on the Modify Lead Info Page.");
			boolean isLeadBackButtonPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//*[@name='back']");
			if (isLeadBackButtonPresent == false) {
				fc.utobj().throwsException("was not able to verify Back is appearing properly in Modify Lead.");
			} else {
				fc.utobj().printTestStep(
						"verified that the Back button is appearing properly on Modify the Lead Info Page.");
			}

			fc.utobj().printTestStep("To verify that if user clicks on the Back button on the Add Lead Page");
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//*[@name='back']"));
			systemExactSearch(driver, firstName + " " + lastName);
			fc.utobj().printTestStep("Verify Lead Name in Lead Summary.");
			isLeadNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//*[contains(text () ,'" + firstName + " " + lastName + "')]");
			if (isLeadNamePresent == false) {
				fc.utobj().throwsException("was not able to verify Lead Name");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	public void addLead(WebDriver driver, Map<String, String> dataSet, String firstName, String lastName,
			String company, String assignTo, String city, String address, String suffix, String jobTitle,
			String comment, String userName, String regionName, String regionalUser, String franchiseId,
			String franchiseUser, Map<String, String> config) throws Exception {

		String testCaseId = "TC_Add_Lead_At_Lead_Page";

		if (fc.utobj().validate(testCaseId)) {
			try {
				CRMLeadsPage pobj = new CRMLeadsPage(driver);
				fc.crm().crm_common().CRMLeadsLnk(driver);
				fc.utobj().clickElement(driver, pobj.addNew);
				fc.utobj().clickElement(driver, pobj.leadLnk);
				fc.utobj().selectDropDown(driver, pobj.title, "Mr.");
				fc.utobj().sendKeys(driver, pobj.leadFirstName, firstName);
				fc.utobj().sendKeys(driver, pobj.leadLastName, lastName);
				fc.utobj().sendKeys(driver, pobj.companyName, company);
				fc.utobj().selectDropDown(driver, pobj.primaryContactMethodSelect, "Email");

				if (assignTo.equalsIgnoreCase("Corporate")) {
					if (!fc.utobj().isSelected(driver, pobj.assignToCorporate)) {
						fc.utobj().clickElement(driver, pobj.assignToCorporate);
					}
					fc.utobj().selectDropDown(driver, pobj.selectCorporateUser, userName);
				} else if (assignTo.equalsIgnoreCase("Regional")) {
					if (!fc.utobj().isSelected(driver, pobj.assignToRegional)) {
						fc.utobj().clickElement(driver, pobj.assignToRegional);
					}
					fc.utobj().selectDropDown(driver, pobj.selectAreaRegion, regionName);
					;
					fc.utobj().selectDropDown(driver, pobj.selectRegionalUser, regionalUser);

				} else if (assignTo.equalsIgnoreCase("Franchise")) {
					if (!fc.utobj().isSelected(driver, pobj.assignToFranchise)) {
						fc.utobj().clickElement(driver, pobj.assignToFranchise);
					}
					fc.utobj().selectDropDown(driver, pobj.selectFranchiseId, franchiseId);
					fc.utobj().selectDropDown(driver, pobj.selectFranchiseUser, franchiseUser);
				} else if (assignTo.equalsIgnoreCase("Only Franchise")) {
					fc.utobj().selectDropDown(driver, pobj.selectFranchiseUser, franchiseUser);

				}
				fc.utobj().sendKeys(driver, pobj.address, address);
				fc.utobj().sendKeys(driver, pobj.city, city);
				fc.utobj().selectDropDown(driver, pobj.country, "USA");
				fc.utobj().selectDropDown(driver, pobj.state, "Alabama");
				fc.utobj().sendKeys(driver, pobj.zipcode, "12345");
				fc.utobj().sendKeys(driver, pobj.phoneNumbers, "1236547896");
				fc.utobj().sendKeys(driver, pobj.extn, "12");
				fc.utobj().sendKeys(driver, pobj.faxNumbers, "1263985487");
				fc.utobj().sendKeys(driver, pobj.mobileNumbers, "8896724441");
				
				String email = fc.utobj().generateTestData("vipingrover")+"@staffex.com";
				//String email = "crmautomation@staffex.com";
				fc.utobj().sendKeys(driver, pobj.emailIds, email);
				fc.utobj().sendKeys(driver, pobj.alternateEmail, "testautoamtion@franqa.com");
				fc.utobj().sendKeys(driver, pobj.suffix, suffix);
				fc.utobj().sendKeys(driver, pobj.position, jobTitle);
				fc.utobj().sendKeys(driver, pobj.birthdate, "11/24/1980");
				fc.utobj().sendKeys(driver, pobj.anniversarydate, "11/24/1999");
				fc.utobj().selectDropDown(driver, pobj.rating, "Hot");
				fc.utobj().sendKeys(driver, pobj.bestTimeToContact, "12:00 PM EST");
				fc.utobj().sendKeys(driver, pobj.comments, comment);
				fc.utobj().clickElement(driver, pobj.saveBtn);

				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException("Was not able to Add Lead :: CRM > Leads");
		}
	}

	public void VerifyPrintPreview(WebDriver driver, String textTobeVerified) throws Exception {
		Set<String> windows = new HashSet<String>();
		windows = driver.getWindowHandles();

		boolean isTextDisplayed = false;

		String currentWindow = driver.getWindowHandle();
		for (String window : windows) {
			if (!window.equals(currentWindow)) {

				driver.switchTo().window(window);
				if (driver.getCurrentUrl().contains("bPrint")) {
					isTextDisplayed = fc.utobj().assertPageSource(driver, textTobeVerified);
				}
			}
		}
		driver.close();

		if (isTextDisplayed) {
			Reporter.log(textTobeVerified + "  is displayed on the print preview page. !!! ");
		} else {
			fc.utobj().throwsException(textTobeVerified + "  is not displayed on the print preview page. !!! ");
		}

		driver.switchTo().window(currentWindow);
	}

	public void searchLeadByOwner(WebDriver driver, String userName, String leadTypeView) throws Exception {

		CRMLeadsPage pobj = new CRMLeadsPage(driver);
		fc.utobj().clickElement(driver, pobj.showFilter);
		fc.utobj().setToDefault(driver, pobj.leadOwnerSelect);
		try {
			WebElement divsion = fc.utobj().getElementByXpath(driver, ".//*[@id='ms-parentdivisionID']");
			fc.utobj().setToDefault(driver, divsion);
		} catch (Exception e) {
		}
		fc.utobj()
				.selectValFromMultiSelect(driver, pobj.leadOwnerSelect, fc.utobj()
						.getElement(driver, pobj.leadOwnerSelect).findElement(By.xpath("./div/div/input")),
						userName);
		fc.utobj().setToDefault(driver, pobj.franchiseSelect);
		fc.utobj().setToDefault(driver, pobj.statusSelect);
		fc.utobj().selectDropDown(driver, pobj.addDateSelect, "All");
		fc.utobj().setToDefault(driver, pobj.emailStatusSelect);
		fc.utobj().selectDropDown(driver, pobj.viewLeadsSelect, leadTypeView);
		fc.utobj().clickElement(driver, pobj.searchBtn);
		;
		fc.utobj().clickElement(driver, pobj.hideFilter);
	}

	public void searchLeadByOwnerDiffrent(WebDriver driver, String userName, String leadTypeView, List<String> listInfo)
			throws Exception {

		CRMLeadsPage pobj = new CRMLeadsPage(driver);
		fc.utobj().clickElement(driver, pobj.showFilter);
		fc.utobj().setToDefault(driver, pobj.leadOwnerSelect);
		try {
			WebElement divsion = fc.utobj().getElementByXpath(driver, ".//*[@id='ms-parentdivisionID']");
			fc.utobj().setToDefault(driver, divsion);
		} catch (Exception e) {
		}
		fc.utobj().selectMultipleValFromMultiSelect(driver,
				fc.utobj().getElementByXpath(driver, ".//*[@id='ms-parentleadOwner']"), listInfo);
		// fc.utobj().selectValFromMultiSelect(driver,
		// pobj.leadOwnerSelect,fc.utobj().getElement(driver,
		// pobj.leadOwnerSelect).findElement(By.xpath("./div/div/input")),
		// userName);
		fc.utobj().setToDefault(driver, pobj.franchiseSelect);
		fc.utobj().setToDefault(driver, pobj.statusSelect);
		fc.utobj().selectDropDown(driver, pobj.addDateSelect, "All");
		fc.utobj().setToDefault(driver, pobj.emailStatusSelect);
		fc.utobj().selectDropDown(driver, pobj.viewLeadsSelect, leadTypeView);
		fc.utobj().clickElement(driver, pobj.searchBtn);
		;
		fc.utobj().clickElement(driver, pobj.hideFilter);
	}

	// system search
	public void systemExactSearch(WebDriver driver, String leadName) throws Exception {
		// search By System Search Button
		CRMLeadsPage pobj = new CRMLeadsPage(driver);
		fc.utobj().sendKeys(driver, pobj.searchLeads, leadName);
		// fc.utobj().clickElement(driver, pobj.exactSearch);
		fc.utobj().clickElement(driver, pobj.systemSearchButton);
	}

	public Map<String, String> fillFormDataWithDataset(WebDriver driver, Map<String, String> config, String testCaseId,
			Map<String, String> dataSet) throws Exception {
		Reporter.log("Filling TestCase Data = " + testCaseId);
		String id = "id";

		List<String> listItems = new ArrayList<String>();
		Map<String, String> printInfo = new HashMap<String, String>();

		List<WebElement> mandateBox = null;
		mandateBox = driver.findElements(By.xpath(
				".//td[@qat_tableid='addData']//*[@type='text' or @class='fTextBoxDate' or @class='fTextBox' or @class='multiList' or @type='radio' or @type='checkbox' or @type='checkBox' or @id='ms-parentsellerCombo' or @class='ms-choice' or @class='form-control']"));

		Map<String, String> alreadyitrateElement = new HashMap<>();
		Map<String, String> alreadyradio = new HashMap<>();
		Map<String, String> alreadycheckBox = new HashMap<>();

		try {
			for (WebElement webElement : mandateBox) {

				if (webElement.getAttribute("" + id) != null && !"".equals(webElement.getAttribute("" + id))
						&& !"null".equals(webElement.getAttribute("" + id))
						&& !":".equals(webElement.getAttribute("" + id))
						&& !"dateOfOpen".equals(webElement.getAttribute("" + id))
						&& !"fimCbOtherComplaintType".equals(webElement.getAttribute("" + id))) {
					/*
					 * try{
					 * System.out.println("Type==="+webElement.getAttribute(
					 * "type")+"====values==="+webElement.getAttribute("value")
					 * +"======"+dataSet.get(""+webElement.getAttribute(""+id))
					 * +"====="+""+webElement.getAttribute(""+id));
					 * }catch(Exception e) {
					 * 
					 * }
					 */

					if (alreadyitrateElement.containsKey(webElement.getAttribute("" + id))) {

					} else {
						if (dataSet.containsKey(webElement.getAttribute("" + id))) {
							WebElement elementmovepostion = null;
							if ("name".equals(id)) {
								elementmovepostion = driver.findElement(By.name("" + webElement.getAttribute("" + id)));

							} else {
								elementmovepostion = fc.utobj().getElementByID(driver,
										"" + webElement.getAttribute("" + id));

							}
							fc.utobj().moveToElement(driver, elementmovepostion);
							if (webElement.getAttribute("type").indexOf("select-one") != -1) {
								if (webElement.getAttribute("" + id) != null
										&& !"cmSource2ID".equals(webElement.getAttribute("" + id))
										&& !"cmSource3ID".equals(webElement.getAttribute("" + id)))

								{
									try {

										Select singledrop = null;
										List<WebElement> singleDropDown = null;
										try {
											singledrop = new Select(fc.utobj().getElementByID(driver,
													"" + webElement.getAttribute("" + id)));
											singleDropDown = driver
													.findElements(By.id("" + webElement.getAttribute("" + id)));
										} catch (Exception e) {
											singledrop = new Select(
													driver.findElement(By.name("" + webElement.getAttribute("" + id))));
											singleDropDown = driver
													.findElements(By.name("" + webElement.getAttribute("" + id)));
										}
										if (webElement.getAttribute("value") == null
												|| "".equals(webElement.getAttribute("value"))
												|| "null".equals(webElement.getAttribute("value"))
												|| "-1".equals(webElement.getAttribute("value"))) {

											int size = singleDropDown.size();
											if (dataSet.get("" + webElement.getAttribute("" + id)) != null
													&& !"".equals(dataSet.get("" + webElement.getAttribute("" + id)))) {
												if (size >= 1) {
													try {
														WebElement dropDownField = driver
																.findElement(By.id("" + webElement.getAttribute("id")));
														// fc.utobj().moveToElement(driver,
														// dropDownField);
														fc.utobj().selectDropDownByVisibleText(driver, dropDownField,
																dataSet.get("" + webElement.getAttribute("" + id)));

														if (!"birthMonth".equals(webElement.getAttribute("" + id))
																&& !"birthDate".equals(webElement.getAttribute("" + id))
																&& !"spouseBirthMonth"
																		.equals(webElement.getAttribute("" + id))
																&& !"spouseBirthDate"
																		.equals(webElement.getAttribute("" + id))) {
															listItems.add(
																	dataSet.get("" + webElement.getAttribute("" + id)));
														}
														printInfo.put("" + webElement.getAttribute("" + id),
																dataSet.get("" + webElement.getAttribute("" + id)));

													} catch (Exception eradio) {
														// Reporter.log("Problem
														// in selecting drop :
														// "+""+webElement.getAttribute("id")
														// +" Values getting
														// from Excel not match
														// in desired
														// fomrat."+""+dataSet.get(""+webElement.getAttribute("id")));
													}
												}
											}
										}

									} catch (Exception e3) {

									}
								}

							} else if (webElement.getAttribute("type").indexOf("select-multiple") != -1) {
								if (webElement.getAttribute("value") == null
										|| "".equals(webElement.getAttribute("value"))
										|| "null".equals(webElement.getAttribute("value"))
										|| "-1".equals(webElement.getAttribute("value"))) {
									try {
										WebElement element = driver
												.findElement(By.id("ms-parent" + webElement.getAttribute("" + id)));
										fc.utobj().clickElement(driver, element);
										fc.utobj().clickElement(driver,
												fc.utobj().getElementByXpath(driver, ".//*[@id='selectAll']"));
										fc.utobj().clickElement(driver, element);
									} catch (Exception emultipal) {
										// Reporter.log("Problem in multiple
										// value drop-down :
										// "+""+webElement.getAttribute("id"));
									}
								}
							} else if (webElement.getAttribute("type").indexOf("textarea") != -1) {
								if (webElement.getAttribute("value") == null
										|| "".equals(webElement.getAttribute("value"))
										|| "null".equals(webElement.getAttribute("value"))) {
									try {
										driver.findElement(By.name("" + webElement.getAttribute("" + id)))
												.sendKeys(dataSet.get("" + webElement.getAttribute("" + id)));
										listItems.add(dataSet.get("" + webElement.getAttribute("" + id)));
										printInfo.put("" + webElement.getAttribute("id"),
												dataSet.get("" + webElement.getAttribute("" + id)));
									} catch (Exception eText) {
										// Reporter.log("Problem in entering
										// TextArea in :
										// "+""+webElement.getAttribute("id"));
									}
								}
							} else if (webElement.getAttribute("type").indexOf("radio") != -1) {
								try {
									String sValue = "";
									List<WebElement> rdBtn_Sex = driver
											.findElements(By.id("" + webElement.getAttribute("" + id))); //
									if (alreadyradio.containsKey(webElement.getAttribute("" + id))) {

									} else {

										int size = rdBtn_Sex.size();
										if (size > 1) {
											sValue = dataSet.get("" + webElement.getAttribute("" + id));
										}
										try {
											List<WebElement> rdBtn_Field = null;
											WebElement element = null;
											try {
												rdBtn_Field = driver.findElements(By.xpath(
														".//*[@name='" + webElement.getAttribute("" + id) + "']")); // work
												element = fc.utobj().getElementByXpath(driver,
														".//*[@name='" + webElement.getAttribute("" + id) + "']");
											} catch (Exception eTxt) {
												rdBtn_Field = driver.findElements(
														By.xpath(".//*[@id='" + webElement.getAttribute("id") + "']")); // work
												element = driver.findElement(
														By.xpath(".//*[@id='" + webElement.getAttribute("id") + "']"));

											}

											fc.utobj().moveToElement(driver, element);
											fc.utobj().clickRadioButton(driver, rdBtn_Field, sValue); // work
											listItems.add(dataSet.get("" + webElement.getAttribute("" + id)));
											printInfo.put("" + webElement.getAttribute("id"),
													dataSet.get("" + webElement.getAttribute("" + id)));
											alreadyradio.put(webElement.getAttribute("" + id),
													webElement.getAttribute("" + id));
										} catch (Exception eText) {
											// Reporter.log("Problem in
											// selecting radio value in :
											// "+""+webElement.getAttribute("id"));
										}
									}
								} catch (Exception exp) {
									Reporter.log(exp.toString());
									exp.printStackTrace();
								}

							} else if (webElement.getAttribute("type").toLowerCase().indexOf("checkbox") != -1) {
								List<WebElement> oCheckBox = driver
										.findElements(By.name("" + webElement.getAttribute("" + id)));
								String sValue = "";
								if (alreadycheckBox.containsKey(webElement.getAttribute("" + id))) {

								} else {

									int size = oCheckBox.size();
									for (int i = 0; i < size; i++) {
										sValue = oCheckBox.get(i).getAttribute("value");
										// Thread.sleep(2000);
										if (i == (size - 1)) {

											// break;
										}

									}
									try {
										List<WebElement> rdBtn_Field = driver.findElements(By.xpath(
												"//*[contains(@id, '" + webElement.getAttribute("" + id) + "')]")); // work
										WebElement element = fc.utobj().getElementByXpath(driver,
												"//*[contains(@id, '" + webElement.getAttribute("" + id) + "')]");
										fc.utobj().moveToElement(driver, element);
										fc.utobj().clickRadioButton(driver, rdBtn_Field, sValue); // work
										alreadycheckBox.put(webElement.getAttribute("" + id),
												webElement.getAttribute("" + id));
									} catch (Exception eText) {

									}
								}
							} else if (webElement.getAttribute("type").indexOf("file") != -1) {
								String fileName = fc.utobj().getFilePathFromTestData("pictureFile");
								try {
									fc.utobj().sendKeys(driver,
											fc.utobj().getElementByID(driver, "" + webElement.getAttribute("" + id)),
											fileName);
									listItems.add(fileName);
									printInfo.put("" + webElement.getAttribute("" + id), fileName);
								} catch (Exception eFile) {
									// Reporter.log("Problem in uploading file
									// type value in :
									// "+""+webElement.getAttribute("id"));
								}
							} else if (webElement.getAttribute("type").indexOf("text") != -1) {
								if (webElement.getAttribute("value") == null
										|| "".equals(webElement.getAttribute("value"))
										|| "null".equals(webElement.getAttribute("value"))
										|| "0.00".equals(webElement.getAttribute("value"))) {
									if (webElement.getAttribute("class").indexOf("fTextBoxDate") != -1) {
										try {
											if (dataSet.get("" + webElement.getAttribute("" + id)) != null
													|| !"".equals(dataSet.get("" + webElement.getAttribute("" + id)))
													|| !"null".equals(
															dataSet.get("" + webElement.getAttribute("" + id)))) {
												fc.utobj().sendKeys(driver,
														driver.findElement(
																By.name("" + webElement.getAttribute("" + id))),
														dataSet.get("" + webElement.getAttribute("" + id))); // work
												listItems.add(dataSet.get("" + webElement.getAttribute("" + id)));
												printInfo.put("" + webElement.getAttribute("" + id),
														dataSet.get("" + webElement.getAttribute("" + id)));

											} else {

											}
										} catch (Exception eDate) {
											// Reporter.log("Problem in Entering
											// Date fields value in :
											// "+""+webElement.getAttribute("id"));
										}
									} else {
										try {
											if ("0.00".equals(webElement.getAttribute("value"))) {
												fc.utobj()
														.sendKeys(driver,
																driver.findElement(
																		By.name("" + webElement.getAttribute("" + id))),
																"");

												// System.out.println(webElement.getAttribute("value")
												// +"======"+dataSet.get(""+webElement.getAttribute(""+id))
												// +"====="+""+webElement.getAttribute(""+id));

											}
											if ("name".equals(id)) {
												fc.utobj().sendKeys(driver,
														driver.findElement(
																By.name("" + webElement.getAttribute("" + id))),
														dataSet.get("" + webElement.getAttribute("" + id)));
												if ("accountName".equals("" + webElement.getAttribute("" + id))
														|| "opportunityOwner"
																.equals("" + webElement.getAttribute("" + id))
														|| "oppContactID"
																.equals("" + webElement.getAttribute("" + id))) {
													;
													fc.utobj().clickElement(driver, driver.findElement(
															By.xpath(".//*[@id='customizedAjaxSearch']/div")));
													fc.utobj().sleep();
												}
											} else {
												fc.utobj().sendKeys(driver,
														driver.findElement(
																By.id("" + webElement.getAttribute("" + id))),
														dataSet.get("" + webElement.getAttribute("" + id)));
												if ("accountName".equals("" + webElement.getAttribute("" + id))
														|| "opportunityOwner"
																.equals("" + webElement.getAttribute("" + id))
														|| "oppContactID"
																.equals("" + webElement.getAttribute("" + id))) {
													;
													fc.utobj().clickElement(driver, driver.findElement(
															By.xpath(".//*[@id='customizedAjaxSearch']/div")));
													fc.utobj().sleep();
												}
											}

											if (!"ssn".equals("" + webElement.getAttribute("" + id))
													&& !"spouseSsn".equals("" + webElement.getAttribute("" + id))) {
												listItems.add(dataSet.get("" + webElement.getAttribute("" + id)));
											}
											printInfo.put("" + webElement.getAttribute("" + id),
													dataSet.get("" + webElement.getAttribute("" + id)));

										} catch (Exception eText) {
											// Reporter.log("Problem in Entering
											// Text fileds value in :
											// "+""+webElement.getAttribute("id"));
										}

									}
								}
							}
							alreadyitrateElement.put(webElement.getAttribute("" + id),
									webElement.getAttribute("" + id));
						}
					}
				}
			}

			if ("TC_CRM_QA_Lead_Details".equals(testCaseId)) {

				Select singledrop = null;
				List<WebElement> singleDropDown = null;
				try {
					singledrop = new Select(driver.findElement(By.name("leadOwnerID2")));
					singleDropDown = driver.findElements(By.name("leadOwnerID2"));
					WebElement dropDownField = driver.findElement(By.name("leadOwnerID2"));
					fc.utobj().selectDropDownByVisibleText(driver, dropDownField, dataSet.get("leadOwnerID2"));

					dropDownField = driver.findElement(By.name("cmSource2ID"));
					fc.utobj().selectDropDownByVisibleText(driver, dropDownField, dataSet.get("cmSource2ID"));
					;
					dropDownField = driver.findElement(By.name("cmSource3ID"));
					fc.utobj().selectDropDownByVisibleText(driver, dropDownField, dataSet.get("cmSource3ID"));

				} catch (Exception e) {

				}

			}
			if ("TC_CRM_QA_Contact_Details".equals(testCaseId)) {

				Select singledrop = null;
				List<WebElement> singleDropDown = null;
				try {
					singledrop = new Select(driver.findElement(By.name("contactOwnerID2")));
					singleDropDown = driver.findElements(By.name("contactOwnerID2"));
					WebElement dropDownField = driver.findElement(By.name("contactOwnerID2"));
					fc.utobj().selectDropDownByVisibleText(driver, dropDownField, dataSet.get("contactOwnerID2"));

					dropDownField = driver.findElement(By.name("cmSource2ID"));
					fc.utobj().selectDropDownByVisibleText(driver, dropDownField, dataSet.get("cmSource2ID"));
					;
					dropDownField = driver.findElement(By.name("cmSource3ID"));
					fc.utobj().selectDropDownByVisibleText(driver, dropDownField, dataSet.get("cmSource3ID"));

				} catch (Exception e) {

				}

			}

		} catch (Exception e) {
			// fc.utobj().throwsException("Fields data are not available!
			// "+e.getMessage());
		}
		try {
			// fc.utobj().printTestStep(testCaseId, "Submiting the Tab ");
			WebElement elementbutton = fc.utobj().getElementByXpath(driver,
					".//*[@id='Submit' or @name='Submit' or @name='button' or @name='Submit1' or @name='add1'] ");
			fc.utobj().clickElement(driver, elementbutton);

		} catch (Exception ee) {
			fc.utobj().printBugStatus("Unable to submit page " + ee);
		}

		return printInfo;
	}

}