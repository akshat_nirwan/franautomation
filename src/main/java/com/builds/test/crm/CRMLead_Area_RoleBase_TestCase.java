package com.builds.test.crm;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.testng.Reporter;
import org.testng.annotations.Test;

import com.builds.test.admin.AdminUsersManageCorporateUsersAddCorporateUserPageTest;
import com.builds.test.admin.AdminUsersRolesAddNewRolePageTest;
import com.builds.test.admin.AdminUsersRolesAddRoleAddUsersforRolePageTest;
import com.builds.test.common.CorporateUser;
import com.builds.uimaps.admin.AdminUsersRolesAddNewRolePage;
import com.builds.uimaps.crm.CRMImportPage;
import com.builds.uimaps.crm.CRMLeadsPage;
import com.builds.utilities.FranconnectUtil;
import com.builds.utilities.TestCase;

import au.com.bytecode.opencsv.CSVReader;
import au.com.bytecode.opencsv.CSVWriter;

public class CRMLead_Area_RoleBase_TestCase {

	FranconnectUtil fc = new FranconnectUtil();

	@Test(groups = { "crm", "TC_Lead_CRM_DeleteLeadsRole_Case"})
	@TestCase(createdOn = "2018-01-17", updatedOn = "2018-01-17", testCaseId = "TC_Lead_CRM_DeleteLeadsRole_Case", testCaseDescription = "To verify that the that does not have privilege Can Delete Leads, is not able to Delete Lead.")
	private void crmInfoFillAreaDeleteLeadsRole_1() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		Map<String, String> dataSetCustom = null;
		String testCaseIdInternal = "" + testCaseId;
		Map<String, String> printInfo = null;
		try {
			driver = fc.loginpage().login(driver);
			CRMLeadsPage pobj = new CRMLeadsPage(driver);

			fc.utobj().printTestStep("Add New Corporate Role Without CRM Can Delete Leads Privilege");
			AdminUsersRolesAddNewRolePageTest roles_page = new AdminUsersRolesAddNewRolePageTest();
			String roleType = "Corporate Role";
			String roleName = fc.utobj().generateTestData("crmcorplead");
			String moduleName = "CRM";
			String emailId = "crmautomation@staffex.com";

			Map<String, String> privileges = new HashMap<String, String>();
			privileges.put("Can Delete Leads", "No");
			addRolesWithModulesPrivileges(driver, roleType, roleName, moduleName, privileges, testCaseId);

			fc.utobj().printTestStep("Navigate To Admin > Users > Manage Corporate Users > Add  Corporate User");
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporate_user = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser = new CorporateUser();
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corpUser.setEmail(emailId);
			corpUser.setRole(roleName);
			corpUser = corporate_user.createDefaultUser(driver, corpUser);

			/*
			 * System.out.println("fullUserName"+fullUserName);
			 * System.out.println("userName=="+userName +"== password = "
			 * +password);
			 */

			fc.utobj().printTestStep("Navigate To CRM > Leads > Leads Summary");
			fc.utobj().printTestStep("Add Lead");
			fc.crm().crm_common().CRMLeadsLnk(driver);
			fc.utobj().clickElement(driver, pobj.addNew);
			fc.utobj().clickElement(driver, pobj.leadLnk);

			testCaseIdInternal = "TC_CRM_QA_Lead_Details";
			dataSetCustom = fc.utobj().readTestData("crm", testCaseIdInternal);
			String firstName = fc.utobj().generateTestData("Aa");
			String lastName = fc.utobj().generateTestData("Bb");
			dataSetCustom.put("leadFirstName", firstName);
			dataSetCustom.put("leadLastName", lastName);
			dataSetCustom.put("cmLeadTypeID", "Select");
			dataSetCustom.put("cmSource2ID", "Internet");
			dataSetCustom.put("cmSource3ID", "BISON");
			dataSetCustom.put("leadOwnerID2", corpUser.getuserFullName());

			printInfo = fillFormDataWithDataset(driver, config, testCaseIdInternal, dataSetCustom);
			if (printInfo != null && printInfo.size() > 0) {
			}

			fc.utobj().clickElement(driver, pobj.leadsLink);

			systemExactSearch(driver, firstName + " " + lastName);

			fc.utobj().printTestStep("Verify Lead Name in Lead Summary.");
			boolean isLeadNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//*[.='" + firstName + " " + lastName + "']");
			if (isLeadNamePresent == false) {
				fc.utobj().throwsException("was not able to verify First Lead Name");
			}

			fc.home_page().logout(driver);
			fc.utobj().printTestStep("Login With Corporate User");
			fc.loginpage().loginWithParameter(driver, corpUser.getUserName(), corpUser.getPassword());

			fc.crm().crm_common().CRMLeadsLnk(driver);
			fc.utobj().clickElement(driver, pobj.leadsLink);
			systemExactSearch(driver, firstName + " " + lastName);

			fc.utobj().printTestStep("Verify Lead Name in Lead Summary.");
			isLeadNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//*[.='" + firstName + " " + lastName + "']");
			if (isLeadNamePresent == false) {
				fc.utobj().throwsException("was not able to verify First Lead Name");
			}
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//a[contains(text () ,'" + firstName + " " + lastName + "')]"));
			fc.utobj().printTestStep("Verify Delete Lead");

			try {
				fc.utobj().selectActionMenuItemsWithTagAMoreActions(driver, "Delete Lead");
			} catch (Exception e) {
				fc.utobj().printTestStep("Verified not able to Delete Lead");

			}
			fc.utobj().clickElement(driver, pobj.leadsLink);
			systemExactSearch(driver, firstName + " " + lastName);
			boolean isNoRecordPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//td[.='No records found.']");
			if (isNoRecordPresent == true) {
				fc.utobj().throwsException("was not able to verify that privilege Can Delete Leads");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "crm"})
	@TestCase(createdOn = "2018-01-17", updatedOn = "2018-01-17", testCaseId = "TC_Lead_CRM_SaveViewLeads_Case", testCaseDescription = "To verify that the Save View button CRM Lead.")
	private void crmInfoFillAreaSaveView_1() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		Map<String, String> dataSetCustom = null;
		String testCaseIdInternal = "" + testCaseId;
		Map<String, String> printInfo = null;
		try {
			driver = fc.loginpage().login(driver);
			CRMLeadsPage pobj = new CRMLeadsPage(driver);

			fc.utobj().printTestStep("Add New Corporate Role ");
			AdminUsersRolesAddNewRolePageTest roles_page = new AdminUsersRolesAddNewRolePageTest();
			String roleType = "Corporate Role";
			String roleName = fc.utobj().generateTestData("crmcorplead");
			String moduleName = "CRM";
			String emailId = "crmautomation@staffex.com";

			Map<String, String> privileges = new HashMap<String, String>();
			privileges.put("Can Delete Leads", "No");
			addRolesWithModulesPrivileges(driver, roleType, roleName, moduleName, privileges, testCaseId);

			fc.utobj().printTestStep("Navigate To Admin > Users > Manage Corporate Users > Add  Corporate User");
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporate_user = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser = new CorporateUser();
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corpUser.setEmail(emailId);
			corpUser.setRole(roleName);
			corpUser = corporate_user.createDefaultUser(driver, corpUser);

			/*
			 * System.out.println("fullUserName"+fullUserName);
			 * System.out.println("userName=="+userName +"== password = "
			 * +password);
			 */

			fc.utobj().printTestStep("Navigate To CRM > Leads > Leads Summary");
			fc.utobj().printTestStep("Add Lead");
			fc.crm().crm_common().CRMLeadsLnk(driver);
			fc.utobj().clickElement(driver, pobj.addNew);
			fc.utobj().clickElement(driver, pobj.leadLnk);

			testCaseIdInternal = "TC_CRM_QA_Lead_Details";
			dataSetCustom = fc.utobj().readTestData("crm", testCaseIdInternal);
			String firstName = fc.utobj().generateTestData("Aa");
			String lastName = fc.utobj().generateTestData("Bb");
			dataSetCustom.put("leadFirstName", firstName);
			dataSetCustom.put("leadLastName", lastName);
			dataSetCustom.put("cmLeadTypeID", "Select");
			dataSetCustom.put("cmSource2ID", "Internet");
			dataSetCustom.put("cmSource3ID", "BISON");
			dataSetCustom.put("leadOwnerID2", corpUser.getuserFullName());

			printInfo = fillFormDataWithDataset(driver, config, testCaseIdInternal, dataSetCustom);
			if (printInfo != null && printInfo.size() > 0) {
			}

			fc.utobj().clickElement(driver, pobj.leadsLink);

			systemExactSearch(driver, firstName + " " + lastName);

			fc.utobj().printTestStep("Verify Lead Name in Lead Summary.");
			boolean isLeadNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//*[.='" + firstName + " " + lastName + "']");
			if (isLeadNamePresent == false) {
				fc.utobj().throwsException("was not able to verify First Lead Name");
			}

			fc.home_page().logout(driver);
			fc.utobj().printTestStep("Login With Corporate User");
			fc.loginpage().loginWithParameter(driver, corpUser.getUserName(), corpUser.getPassword());

			fc.crm().crm_common().CRMLeadsLnk(driver);
			fc.utobj().clickElement(driver, pobj.leadsLink);
			systemExactSearch(driver, firstName + " " + lastName);

			fc.utobj().printTestStep("Verify Lead Name in Lead Summary.");
			isLeadNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//*[.='" + firstName + " " + lastName + "']");
			if (isLeadNamePresent == false) {
				fc.utobj().throwsException("was not able to verify First Lead Name");
			}

			fc.utobj().printTestStep("Verify Save view in Lead Summary.");
			fc.utobj().clickElement(driver, pobj.showFilter);
			fc.utobj().setToDefault(driver, pobj.leadOwnerSelect);
			try {
				WebElement divsion = fc.utobj().getElementByXpath(driver, ".//*[@id='ms-parentdivisionID']");
				fc.utobj().setToDefault(driver, divsion);
			} catch (Exception e) {
			}
			fc.utobj()
					.selectValFromMultiSelect(driver, pobj.leadOwnerSelect, fc.utobj()
							.getElement(driver, pobj.leadOwnerSelect).findElement(By.xpath("./div/div/input")),
							corpUser.getuserFullName());
			fc.utobj().setToDefault(driver, pobj.franchiseSelect);
			fc.utobj().setToDefault(driver, pobj.statusSelect);
			fc.utobj().selectDropDown(driver, pobj.addDateSelect, "All");
			fc.utobj().setToDefault(driver, pobj.emailStatusSelect);
			fc.utobj().selectDropDown(driver, pobj.viewLeadsSelect, "Open Leads");

			fc.utobj().clickElement(driver, pobj.saveViewBtn);
			
			fc.utobj().clickElement(driver, pobj.hideFilter);

			fc.crm().crm_common().CRMLeadsLnk(driver);
			fc.utobj().clickElement(driver, pobj.leadsLink);
			systemExactSearch(driver, firstName + " " + lastName);

			fc.utobj().printTestStep("Verify Lead Name in Lead Summary.");
			isLeadNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//*[.='" + firstName + " " + lastName + "']");
			if (isLeadNamePresent == false) {
				fc.utobj().throwsException("was not able to verify First Lead Name");
			}

			fc.utobj().clickElement(driver, pobj.showFilter);
			fc.utobj().clickElement(driver,".//*[@id='ms-parentleadOwner']");
			fc.utobj().sendKeys(driver,pobj.searchText, corpUser.getuserFullName());
			fc.utobj().clickElement(driver,pobj.seacrhicon);
			fc.utobj().clickElement(driver,pobj.check);	
			fc.utobj().clickElement(driver,"//*[@id='ms-parentdivisionID']");
			String ownerName = fc.utobj().getElementByXpath(driver, ".//*[@id='ms-parentleadOwner']").getText();
			fc.utobj().clickElement(driver, pobj.searchBtn);
			
			
			fc.utobj().clickElement(driver, pobj.hideFilter);

			// System.out.println("ownerName======="+ownerName);
			fc.utobj().printTestStep("Verify Lead Name in Lead Summary.");
			System.out.println(corpUser.getuserFullName());
			if (ownerName != null && ownerName.equals(corpUser.getuserFullName())) {
				fc.utobj().printTestStep("Verify Lead owner in Lead Summary save view." + ownerName);
			} else {
				fc.utobj().throwsException("was not able to verify Lead Owner Name in save view ");
			}

			isLeadNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//*[.='" + firstName + " " + lastName + "']");
			if (isLeadNamePresent == false) {
				fc.utobj().throwsException("was not able to verify First Lead Name");
			}
			String header;
			int totalRecordCount = 0;
			int totalrecordPerPage = 0;
			try {
				header = fc.utobj().getElementByXpath(driver, ".//*[@id='pageid']").getText();
				String options[] = header.split(" ");
				if (options != null) {
					totalRecordCount = Integer.valueOf(options[5]);
					totalrecordPerPage = Integer.valueOf(options[3]);
				}
				fc.utobj().printTestStep(" ToTal Record " + totalRecordCount);

				if (totalRecordCount == 1) {
					fc.utobj().printTestStep("Verify Lead Name in Lead Summary.");
				} else {
					fc.utobj().throwsException("was not able to verify Lead Name in save view");
				}

			} catch (Exception E) {
				fc.utobj().throwsException("was not able to verify Lead Name in save view");
				// System.out.println("Pagination Not Found");
				totalRecordCount = 0;
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "crm"})
	@TestCase(createdOn = "2018-01-17", updatedOn = "2018-01-17", testCaseId = "TC_Lead_CRM_CanViewAllLeadsRole_Case", testCaseDescription = "To verify that the that does not have privilege Can View All Leads, is not able to view other Lead.")
	private void crmInfoFillAreaDeleteLeadsRole_2() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		Map<String, String> dataSetCustom = null;
		String testCaseIdInternal = "" + testCaseId;
		Map<String, String> printInfo = null;
		try {
			driver = fc.loginpage().login(driver);
			CRMLeadsPage pobj = new CRMLeadsPage(driver);

			fc.utobj().printTestStep("Add New Corporate Role Without The Hub > Can View All Leads Privilege");
			AdminUsersRolesAddNewRolePageTest roles_page = new AdminUsersRolesAddNewRolePageTest();
			String roleType = "Corporate Role";
			String roleName = fc.utobj().generateTestData("crmcorplead");
			String moduleName = "CRM";
			String emailId = "crmautomation@staffex.com";

			Map<String, String> privileges = new HashMap<String, String>();
			privileges.put("Can View All Leads", "No");
			addRolesWithModulesPrivileges(driver, roleType, roleName, moduleName, privileges, testCaseId);

			fc.utobj().printTestStep("Navigate To Admin > Users > Manage Corporate Users > Add  Corporate User");
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporate_user = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser = new CorporateUser();
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corpUser.setEmail(emailId);
			corpUser.setRole(roleName);
			corpUser = corporate_user.createDefaultUser(driver, corpUser);

			fc.utobj().printTestStep("Navigate To Admin Users Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			emailId = "crmautomation@staffex.com";
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUser = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser1 = new CorporateUser();
			corpUser1.setEmail(emailId);
			corpUser1 = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser1);
			corporateUser.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser1);

			/*
			 * System.out.println("fullUserName"+fullUserName);
			 * System.out.println("userName=="+userName +"== password = "
			 * +password);
			 */

			fc.utobj().printTestStep("Navigate To CRM > Leads > Leads Summary");
			fc.utobj().printTestStep("Add Lead");
			fc.crm().crm_common().CRMLeadsLnk(driver);
			fc.utobj().clickElement(driver, pobj.addNew);
			fc.utobj().clickElement(driver, pobj.leadLnk);

			testCaseIdInternal = "TC_CRM_QA_Lead_Details";
			dataSetCustom = fc.utobj().readTestData("crm", testCaseIdInternal);
			String firstName = fc.utobj().generateTestData("Aa");
			String lastName = fc.utobj().generateTestData("Bb");
			dataSetCustom.put("leadFirstName", firstName);
			dataSetCustom.put("leadLastName", lastName);
			dataSetCustom.put("cmLeadTypeID", "Select");
			dataSetCustom.put("cmSource2ID", "Internet");
			dataSetCustom.put("cmSource3ID", "BISON");
			dataSetCustom.put("leadOwnerID2", "FranConnect Administrator"); //corpUser.getuserFullName()

			printInfo = fillFormDataWithDataset(driver, config, testCaseIdInternal, dataSetCustom);
			if (printInfo != null && printInfo.size() > 0) {
			}

			fc.utobj().clickElement(driver, pobj.leadsLink);

			systemExactSearch(driver, firstName + " " + lastName);

			fc.utobj().printTestStep("Verify Lead Name in Lead Summary.");
			boolean isLeadNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//*[.='" + firstName + " " + lastName + "']");
			if (isLeadNamePresent == false) {
				fc.utobj().throwsException("was not able to verify First Lead Name");
			}

			fc.utobj().printTestStep("Navigate To CRM > Leads > Leads Summary");
			fc.utobj().printTestStep("Add Lead");
			fc.crm().crm_common().CRMLeadsLnk(driver);
			fc.utobj().clickElement(driver, pobj.addNew);
			fc.utobj().clickElement(driver, pobj.leadLnk);

			testCaseIdInternal = "TC_CRM_QA_Lead_Details";
			dataSetCustom = fc.utobj().readTestData("crm", testCaseIdInternal);
			String secondfirstName = fc.utobj().generateTestData("Aa");
			String secondlastName = fc.utobj().generateTestData("Bb");
			dataSetCustom.put("leadFirstName", secondfirstName);
			dataSetCustom.put("leadLastName", secondlastName);
			dataSetCustom.put("cmLeadTypeID", "Select");
			dataSetCustom.put("cmSource2ID", "Internet");
			dataSetCustom.put("cmSource3ID", "BISON");
			dataSetCustom.put("leadOwnerID2", corpUser.getuserFullName());

			printInfo = fillFormDataWithDataset(driver, config, testCaseIdInternal, dataSetCustom);
			if (printInfo != null && printInfo.size() > 0) {
			}

			fc.utobj().clickElement(driver, pobj.leadsLink);

			systemExactSearch(driver, secondfirstName + " " + secondlastName);

			fc.utobj().printTestStep("Verify Lead Name in Lead Summary.");
			isLeadNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//*[.='" + secondfirstName + " " + secondlastName + "']");
			if (isLeadNamePresent == false) {
				fc.utobj().throwsException("was not able to verify First Lead Name");
			}

			fc.home_page().logout(driver);
			fc.utobj().printTestStep("Login With Corporate User");
			fc.loginpage().loginWithParameter(driver, corpUser.getUserName(), corpUser.getPassword());

			fc.crm().crm_common().CRMLeadsLnk(driver);
			fc.utobj().clickElement(driver, pobj.leadsLink);
			//systemExactSearch(driver, firstName + " " + lastName);
			systemExactSearch(driver, secondfirstName + " " + secondlastName);
			fc.utobj().printTestStep("Verify Lead Name in Lead Summary.");
			isLeadNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//*[.='" +secondfirstName + " " + secondlastName+ "']");
			if (isLeadNamePresent == false) {
				fc.utobj().throwsException("was not able to verify First Lead Name");
			}

			fc.utobj().printTestStep("Verify Can View All Leads");

			fc.utobj().clickElement(driver, pobj.leadsLink);
			//systemExactSearch(driver, secondfirstName + " " + secondlastName);
			systemExactSearch(driver, firstName + " " + lastName);
			boolean isNoRecordPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//td[.='No records found.']");
			if (isNoRecordPresent == false) {
				fc.utobj().throwsException("was not able to verify that privilege Can View All Leads");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "crm", "TC_Lead_CRM_LeadImport_Case"})
	@TestCase(createdOn = "2018-01-17", updatedOn = "2018-01-17", testCaseId = "TC_Lead_CRM_LeadImport_Case", testCaseDescription = "To verify that the Lead added from Import.")
	private void crmInfoFillAreaTask_1() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		Map<String, String> dataSetCustom = null;
		String testCaseIdInternal = "" + testCaseId;
		Map<String, String> printInfo = null;
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		Map<String, String> leadInfoafteradded = null;
		try {
			driver = fc.loginpage().login(driver);
			CRMLeadsPage pobj = new CRMLeadsPage(driver);

			/*
			 * fc.utobj().printTestStep(testCaseId,
			 * "Navigate To Admin Users Manage Corporate User");
			 * fc.utobj().printTestStep(testCaseId, "Add Corporate User");
			 * String emailId="crmautomation@staffex.com";
			 * AdminUsersManageCorporateUsersAddCorporateUserPageTest
			 * corporateUser=new
			 * AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			 * CorporateUser corpUser = new
			 * CorporateUser();corpUser.setEmail(emailId);corpUser =
			 * fc.CommonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			 * corporateUser.
			 * createDefaultCorporateUser_Through_WebService_AfterLogin(driver,
			 * corpUser);
			 */

			testCaseIdInternal = "TC_FC_QA_Sale_Lead_Primary_Owner_Assignment";
			dataSet = fc.utobj().readTestData("crm", testCaseIdInternal);
			fc.utobj().printTestStep("Adding lead from Web Form");
			Map<String, String> leadInfo = new HashMap<String, String>();
			String firstName = fc.utobj().generateTestData("Wk");
			String lastName = fc.utobj().generateTestData("Wk");
			String formName = fc.utobj().generateTestData("FORM");
			leadInfo.put("firstName", firstName);
			leadInfo.put("lastName", lastName);
			leadInfo.put("country", dataSet.get("country"));
			// leadInfo.put("state", dataSet.get("stateID"));
			leadInfo.put("email", dataSet.get("emailID"));
			// leadInfo.put("leadOwner", "FranConnect Administrator");
			leadInfo.put("leadSourceCategory", "Internet");
			leadInfo.put("leadSourceDetails", "BISON");
			leadInfo.put("zipCode", "201009");
			// leadInfo.put("state", "");
			// leadInfo.put("brand", divisionName);
			leadInfo.put("owner", "Based on Assignment Rules");
			leadInfo.put("ownerName", "FranConnect Administrator");
			String fileName = "OneLeadImportFile.csv";
			fc.utobj().printTestStep("Adding Lead from Import");
			fileName = fc.utobj().getFilePathFromTestData(fileName);
			System.out.println("fileName === > " + fileName);

			try {
				leadInfoafteradded = addLeadFromImport(driver, config, fileName, leadInfo);
			} catch (Exception e) {

			}
			System.out.println("leadInfoafteradded === > " + leadInfoafteradded);

			fc.utobj().clickElement(driver, pobj.leadsLink);

			systemExactSearch(driver, firstName);

			fc.utobj().printTestStep("Verify Lead Name in Lead Summary.");
			//Add First name and last name
			String fullName=firstName+" "+lastName;
			
			//boolean isLeadNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//*[.='" + fullName + "']");
			String isLeadNamePresent = driver.findElement(By.xpath("//tbody/tr[2]//td[2]/a[@class='bText12lnk']")).getText();
			if (isLeadNamePresent.equalsIgnoreCase(fullName)) {
				fc.utobj().printTestStep("able to verify Lead Name");
			}
			else {
				fc.utobj().throwsException("was not able to verify Lead Name");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	public Map addLeadFromImport(WebDriver driver, Map<String, String> config, String fileName,
			Map<String, String> leadInfo) {

		// Map<String,String> leadInfo = new HashMap<String, String>();

		try {
			File inputFile = new File(fileName);

			// Read existing file
			CSVReader reader = new CSVReader(new FileReader(inputFile), ',');
			List<String[]> csvBody = reader.readAll();
			// get CSV row column and replace with by using row and column
			if (leadInfo.get("InquiryDate") != null) {
				csvBody.get(1)[0] = leadInfo.get("InquiryDate");
			}

			csvBody.get(1)[1] = leadInfo.get("firstName");
			csvBody.get(1)[2] = leadInfo.get("lastName");
			if (leadInfo.get("email") != null) {
				csvBody.get(1)[3] = leadInfo.get("email");
			}
			if (leadInfo.get("companyName") != null) {
				csvBody.get(1)[4] = leadInfo.get("companyName");
			}
			if (leadInfo.get("address1") != null) {
				csvBody.get(1)[5] = leadInfo.get("address1");
			}
			/*
			 * if (leadInfo.get("address2")!=null) { csvBody.get(1)[6] =
			 * leadInfo.get("address2"); }
			 */
			if (leadInfo.get("city") != null) {
				csvBody.get(1)[7] = leadInfo.get("city");
			}
			if (leadInfo.get("state") != null) {
				csvBody.get(1)[8] = leadInfo.get("state");
			}
			if (leadInfo.get("country") != null) {
				csvBody.get(1)[9] = leadInfo.get("country");
			}
			if (leadInfo.get("zipCode") != null) {
				csvBody.get(1)[10] = leadInfo.get("zipCode");
			}
			if (leadInfo.get("workPhone") != null) {
				csvBody.get(1)[11] = leadInfo.get("workPhone");
			}
			if (leadInfo.get("workPhoneExtension") != null) {
				csvBody.get(1)[12] = leadInfo.get("workPhoneExtension");
			}
			/*
			 * if (leadInfo.get("homePhone")!=null) { csvBody.get(1)[13] =
			 * leadInfo.get("homePhone"); } if
			 * (leadInfo.get("homePhoneExtension")!=null) { csvBody.get(1)[14] =
			 * leadInfo.get("homePhoneExtension"); }
			 */
			if (leadInfo.get("fax") != null) {
				csvBody.get(1)[15] = leadInfo.get("fax");
			}
			if (leadInfo.get("comments") != null) {
				csvBody.get(1)[16] = leadInfo.get("comments");
			}
			if (leadInfo.get("mobile") != null) {
				csvBody.get(1)[17] = leadInfo.get("mobile");
			}
			if (leadInfo.get("bestTimeToContact") != null) {
				csvBody.get(1)[18] = leadInfo.get("bestTimeToContact");
			}
			if (leadInfo.get("status") != null) {
				csvBody.get(1)[19] = leadInfo.get("status");
			} else {
				leadInfo.put("status", "New");
			}
			/*
			 * if (leadInfo.get("leadSourceCategory")!=null) {
			 * csvBody.get(1)[20] = leadInfo.get("leadSourceCategory"); } if
			 * (leadInfo.get("leadSourceDetails")!=null) { csvBody.get(1)[21] =
			 * leadInfo.get("leadSourceDetails"); }
			 */
			if (leadInfo.get("assignTo") != null) {
				csvBody.get(1)[22] = leadInfo.get("assignTo");
			}
			/*
			 * if (leadInfo.get("preferedModeOfContact")!=null) {
			 * csvBody.get(1)[23] = leadInfo.get("preferedModeOfContact"); } if
			 * (leadInfo.get("currentNetWorth")!=null) { csvBody.get(1)[24] =
			 * leadInfo.get("currentNetWorth"); } if
			 * (leadInfo.get("cashAvailableForInvestment")!=null) {
			 * csvBody.get(1)[25] = leadInfo.get("cashAvailableForInvestment");
			 * } if (leadInfo.get("investmentTimeframe")!=null) {
			 * csvBody.get(1)[26] = leadInfo.get("investmentTimeframe"); } if
			 * (leadInfo.get("sourceOfInvestment")!=null) { csvBody.get(1)[27] =
			 * leadInfo.get("sourceOfInvestment"); } if
			 * (leadInfo.get("background")!=null) { csvBody.get(1)[28] =
			 * leadInfo.get("background"); }
			 */
			/*
			 * if (leadInfo.get("division")!=null) { csvBody.get(1)[29] =
			 * leadInfo.get("background"); }
			 */
			/*
			 * if (leadInfo.get("brand")!=null) { csvBody.get(1)[30] =
			 * leadInfo.get("brand"); }
			 */

			reader.close();
			Thread.sleep(3000);
			// Write to CSV file which is open
			CSVWriter writer = new CSVWriter(new FileWriter(inputFile), ',');
			writer.writeAll(csvBody);
			writer.flush();
			writer.close();

			CRM_Common fsmod = new CRM_Common();
			Thread.sleep(3000);
			/*
			 * if(leadInfo.get("salutation")!=null){
			 * fc.utobj().selectDropDown(driver, leadAddPage.salutation,
			 * leadInfo.get("salutation")); }
			 */
			CRMImportPage importPage = new CRMImportPage(driver);
			fsmod.CRMImporttLnk(driver);
			fileName = fc.utobj().getFilePathFromTestData(fileName);
			fc.utobj().sendKeys(driver, importPage.importFile, fileName);
			fc.utobj().clickElement(driver, importPage.continueBtn);

			fc.utobj().selectDropDownByPartialText(driver, importPage.firstName, "First Name");
			fc.utobj().selectDropDownByPartialText(driver, importPage.lastName, "Last Name");
			fc.utobj().selectDropDownByPartialText(driver, importPage.email, "Email");
			fc.utobj().selectDropDownByPartialText(driver, importPage.companyName, "Company Name");
			fc.utobj().selectDropDownByPartialText(driver, importPage.address1, "Address");
			// fc.utobj().selectDropDownByPartialText(driver,
			// importPage.address2, "Address 2");
			fc.utobj().selectDropDownByPartialText(driver, importPage.city, "City");
			fc.utobj().selectDropDownByPartialText(driver, importPage.country, "Country");
			fc.utobj().selectDropDownByPartialText(driver, importPage.stateProvince, "State / Province");
			fc.utobj().selectDropDownByPartialText(driver, importPage.zipPostalCode, "Zip / Postal Code");
			fc.utobj().selectDropDownByPartialText(driver, importPage.workPhone, "Wrok Phone");
			fc.utobj().selectDropDownByPartialText(driver, importPage.workPhoneExt, "Work Phone Extension");
			/*
			 * fc.utobj().selectDropDownByPartialText(driver,
			 * importPage.homePhone, "Home Phone");
			 * fc.utobj().selectDropDownByPartialText(driver,
			 * importPage.homePhoneExt, "Home Phone Extension");
			 */
			fc.utobj().selectDropDownByPartialText(driver, importPage.fax, "Fax");
			fc.utobj().selectDropDownByPartialText(driver, importPage.comments, "Comments");
			fc.utobj().selectDropDownByPartialText(driver, importPage.mobile, "Mobile");
			fc.utobj().selectDropDownByPartialText(driver, importPage.bestTimetoContact, "Best Time To Contact");
			fc.utobj().selectDropDownByPartialText(driver, importPage.leadStatus, "Lead Status");
			fc.utobj().selectDropDownByPartialText(driver, importPage.leadSourceCatagory, "Lead Source Category");
			fc.utobj().selectDropDownByPartialText(driver, importPage.leadSourceDetails, "Lead Source Details");
			fc.utobj().selectDropDownByPartialText(driver, importPage.assignTo, "Assign To");
			/*
			 * fc.utobj().selectDropDownByPartialText(driver,
			 * importPage.preferedModeOfContact, "Preferred Mode of Contact");
			 * fc.utobj().selectDropDownByPartialText(driver,
			 * importPage.currentNetWorth, "Current Net Worth");
			 * fc.utobj().selectDropDownByPartialText(driver,
			 * importPage.cashAvailableForInves, "Cash Available for Investment"
			 * ); fc.utobj().selectDropDownByPartialText(driver,
			 * importPage.investmentTimeframe, "Investment Timeframe");
			 * fc.utobj().selectDropDownByPartialText(driver,
			 * importPage.sourceOfInvestment, "Source Of Investment");
			 * fc.utobj().selectDropDownByPartialText(driver,
			 * importPage.background, "Background");
			 */
			// fc.utobj().selectDropDownByPartialText(driver,
			// importPage.division, "Division");

			String inquiryDate = fc.utobj().getCurrentDateUSFormat();
			fc.utobj().sendKeys(driver, importPage.inquiryDate, inquiryDate);
			if (leadInfo.get("owner") != null) {
				if (leadInfo.get("owner").contentEquals("Based on Assignment Rules")) {
					// fc.utobj().clickElement(driver,
					// fc.utobj().getElementByXpath(driver,".//*[@id='contactOwnerID2']")));
					fc.utobj().selectDropDownByPartialText(driver, importPage.defaultLeadOwner,
							leadInfo.get("ownerName"));
				} else {
					fc.utobj().selectDropDownByPartialText(driver, importPage.defaultLeadOwner, leadInfo.get("owner"));
				}
			} else {
				fc.utobj().selectDropDownByPartialText(driver, importPage.defaultLeadOwner,
						"FranConnect Administrator");
			}
			fc.utobj().selectDropDownByPartialText(driver, importPage.defaultLeadStatus, "New");
			fc.utobj().sendKeys(driver, importPage.defaultLeadLastName, leadInfo.get("lastName"));
			fc.utobj().selectDropDownByPartialText(driver, importPage.defaultLeadSourceCatagory, "Import");
			fc.utobj().selectDropDownByPartialText(driver, importPage.defaultLeadSourceDetails, "Import");
			fc.utobj().clickElement(driver, importPage.caanadianLeadDisclaimer);
			fc.utobj().clickElement(driver, importPage.continueBtn);
			fc.utobj().clickElement(driver, importPage.continueBtn);
			Thread.sleep(6000);
			fc.utobj().clickElement(driver, importPage.backBtn);
			fc.utobj().acceptAlertBox(driver);
		} catch (Exception e) {
			System.out.println(e);
		}
		return leadInfo;
	}

	public void actionImgOption(WebDriver driver, String task, String option) throws Exception {

		fc.utobj().moveToElement(driver,
				fc.utobj().getElementByXpath(driver, ".//*[contains(text () ,'" + task + "')]/ancestor::tr/td/layer"));
		String alterText = fc.utobj()
				.getElementByXpath(driver, ".//*[contains(text () ,'" + task + "')]/ancestor::tr/td/layer")
				.getAttribute("id").trim();
		alterText = alterText.replace("Actions_dynamicmenu", "");
		alterText = alterText.replace("Bar", "");

		fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
				".//*[contains(text () ,'" + task + "')]/ancestor::tr/td/layer/a/img"));
		// fc.utobj().clickElement(driver,
		// fc.utobj().getElementByXpath(driver,".//div[@id='Actions_dynamicmenu"+alterText+"Menu']/span[contains(text
		// () , '"+option+"')]")));
		fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
				".//div[@id='TaskActions_dynamicmenu" + alterText + "Menu']/*[contains(text () , '" + option + "')]"));
	}

	public void actionImgOptionOnlyShow(WebDriver driver, String task, String option) throws Exception {

		fc.utobj().moveToElement(driver,
				fc.utobj().getElementByXpath(driver, ".//*[contains(text () ,'" + task + "')]/ancestor::tr/td/layer"));
		String alterText = fc.utobj()
				.getElementByXpath(driver, ".//*[contains(text () ,'" + task + "')]/ancestor::tr/td/layer")
				.getAttribute("id").trim();
		alterText = alterText.replace("Actions_dynamicmenu", "");
		alterText = alterText.replace("Bar", "");

		fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
				".//*[contains(text () ,'" + task + "')]/ancestor::tr/td/layer/a/img"));
		// fc.utobj().clickElement(driver,
		// fc.utobj().getElementByXpath(driver,".//div[@id='Actions_dynamicmenu"+alterText+"Menu']/span[contains(text
		// () , '"+option+"')]")));
		// fc.utobj().clickElement(driver,
		// fc.utobj().getElementByXpath(driver,".//div[@id='TaskActions_dynamicmenu"+alterText+"Menu']/*[contains(text
		// () , '"+option+"')]")));
	}

	public void searchLeadByOwner(WebDriver driver, String userName, String leadTypeView) throws Exception {

		CRMLeadsPage pobj = new CRMLeadsPage(driver);
		fc.utobj().clickElement(driver, pobj.showFilter);
		fc.utobj().setToDefault(driver, pobj.leadOwnerSelect);
		try {
			WebElement divsion = fc.utobj().getElementByXpath(driver, ".//*[@id='ms-parentdivisionID']");
			fc.utobj().setToDefault(driver, divsion);
		} catch (Exception e) {
		}
		fc.utobj()
				.selectValFromMultiSelect(driver, pobj.leadOwnerSelect, fc.utobj()
						.getElement(driver, pobj.leadOwnerSelect).findElement(By.xpath("./div/div/input")),
						userName);
		fc.utobj().setToDefault(driver, pobj.franchiseSelect);
		fc.utobj().setToDefault(driver, pobj.statusSelect);
		fc.utobj().selectDropDown(driver, pobj.addDateSelect, "All");
		fc.utobj().setToDefault(driver, pobj.emailStatusSelect);
		fc.utobj().selectDropDown(driver, pobj.viewLeadsSelect, leadTypeView);
		fc.utobj().clickElement(driver, pobj.searchBtn);
		;
		fc.utobj().clickElement(driver, pobj.hideFilter);
	}

	public void searchLeadByOwnerDiffrent(WebDriver driver, String userName, String leadTypeView, List<String> listInfo)
			throws Exception {

		CRMLeadsPage pobj = new CRMLeadsPage(driver);
		fc.utobj().clickElement(driver, pobj.showFilter);
		fc.utobj().setToDefault(driver, pobj.leadOwnerSelect);
		try {
			WebElement divsion = fc.utobj().getElementByXpath(driver, ".//*[@id='ms-parentdivisionID']");
			fc.utobj().setToDefault(driver, divsion);
		} catch (Exception e) {
		}
		fc.utobj().selectMultipleValFromMultiSelect(driver,
				fc.utobj().getElementByXpath(driver, ".//*[@id='ms-parentleadOwner']"), listInfo);
		// fc.utobj().selectValFromMultiSelect(driver,
		// pobj.leadOwnerSelect,fc.utobj().getElement(driver,
		// pobj.leadOwnerSelect).findElement(By.xpath("./div/div/input")),
		// userName);
		fc.utobj().setToDefault(driver, pobj.franchiseSelect);
		fc.utobj().setToDefault(driver, pobj.statusSelect);
		fc.utobj().selectDropDown(driver, pobj.addDateSelect, "All");
		fc.utobj().setToDefault(driver, pobj.emailStatusSelect);
		fc.utobj().selectDropDown(driver, pobj.viewLeadsSelect, leadTypeView);
		fc.utobj().clickElement(driver, pobj.searchBtn);
		;
		fc.utobj().clickElement(driver, pobj.hideFilter);
	}

	// system search
	public void systemExactSearch(WebDriver driver, String leadName) throws Exception {
		// search By System Search Button
		CRMLeadsPage pobj = new CRMLeadsPage(driver);
		fc.utobj().sendKeys(driver, pobj.searchLeads, leadName);
		// fc.utobj().clickElement(driver, pobj.exactSearch);
		fc.utobj().clickElement(driver, pobj.systemSearchButton);
	}

	public Map<String, String> fillFormDataWithDataset(WebDriver driver, Map<String, String> config, String testCaseId,
			Map<String, String> dataSet) throws Exception {
		Reporter.log("Filling TestCase Data = " + testCaseId);
		String id = "id";

		List<String> listItems = new ArrayList<String>();
		Map<String, String> printInfo = new HashMap<String, String>();

		List<WebElement> mandateBox = null;
		mandateBox = driver.findElements(By.xpath(
				".//td[@qat_tableid='addData']//*[@type='text' or @class='fTextBoxDate' or @class='fTextBox' or @class='multiList' or @type='radio' or @type='checkbox' or @type='checkBox' or @id='ms-parentsellerCombo' or @class='ms-choice' or @class='form-control']"));

		Map<String, String> alreadyitrateElement = new HashMap<>();
		Map<String, String> alreadyradio = new HashMap<>();
		Map<String, String> alreadycheckBox = new HashMap<>();

		try {
			for (WebElement webElement : mandateBox) {

				if (webElement.getAttribute("" + id) != null && !"".equals(webElement.getAttribute("" + id))
						&& !"null".equals(webElement.getAttribute("" + id))
						&& !":".equals(webElement.getAttribute("" + id))
						&& !"dateOfOpen".equals(webElement.getAttribute("" + id))
						&& !"fimCbOtherComplaintType".equals(webElement.getAttribute("" + id))) {
					/*
					 * try{
					 * System.out.println("Type==="+webElement.getAttribute(
					 * "type")+"====values==="+webElement.getAttribute("value")
					 * +"======"+dataSet.get(""+webElement.getAttribute(""+id))
					 * +"====="+""+webElement.getAttribute(""+id));
					 * }catch(Exception e) {
					 * 
					 * }
					 */

					if (alreadyitrateElement.containsKey(webElement.getAttribute("" + id))) {

					} else {
						if (dataSet.containsKey(webElement.getAttribute("" + id))) {
							WebElement elementmovepostion = null;
							if ("name".equals(id)) {
								elementmovepostion = driver.findElement(By.name("" + webElement.getAttribute("" + id)));

							} else {
								elementmovepostion = fc.utobj().getElementByID(driver,
										"" + webElement.getAttribute("" + id));

							}
							fc.utobj().moveToElement(driver, elementmovepostion);
							if (webElement.getAttribute("type").indexOf("select-one") != -1) {
								if (webElement.getAttribute("" + id) != null
										&& !"cmSource2ID".equals(webElement.getAttribute("" + id))
										&& !"cmSource3ID".equals(webElement.getAttribute("" + id)))

								{
									try {

										Select singledrop = null;
										List<WebElement> singleDropDown = null;
										try {
											singledrop = new Select(fc.utobj().getElementByID(driver,
													"" + webElement.getAttribute("" + id)));
											singleDropDown = driver
													.findElements(By.id("" + webElement.getAttribute("" + id)));
										} catch (Exception e) {
											singledrop = new Select(
													driver.findElement(By.name("" + webElement.getAttribute("" + id))));
											singleDropDown = driver
													.findElements(By.name("" + webElement.getAttribute("" + id)));
										}
										if (webElement.getAttribute("value") == null
												|| "".equals(webElement.getAttribute("value"))
												|| "null".equals(webElement.getAttribute("value"))
												|| "-1".equals(webElement.getAttribute("value"))) {

											int size = singleDropDown.size();
											if (dataSet.get("" + webElement.getAttribute("" + id)) != null
													&& !"".equals(dataSet.get("" + webElement.getAttribute("" + id)))) {
												if (size >= 1) {
													try {
														WebElement dropDownField = driver
																.findElement(By.id("" + webElement.getAttribute("id")));
														// fc.utobj().moveToElement(driver,
														// dropDownField);
														fc.utobj().selectDropDownByVisibleText(driver, dropDownField,
																dataSet.get("" + webElement.getAttribute("" + id)));

														if (!"birthMonth".equals(webElement.getAttribute("" + id))
																&& !"birthDate".equals(webElement.getAttribute("" + id))
																&& !"spouseBirthMonth"
																		.equals(webElement.getAttribute("" + id))
																&& !"spouseBirthDate"
																		.equals(webElement.getAttribute("" + id))) {
															listItems.add(
																	dataSet.get("" + webElement.getAttribute("" + id)));
														}
														printInfo.put("" + webElement.getAttribute("" + id),
																dataSet.get("" + webElement.getAttribute("" + id)));

													} catch (Exception eradio) {
														// Reporter.log("Problem
														// in selecting drop :
														// "+""+webElement.getAttribute("id")
														// +" Values getting
														// from Excel not match
														// in desired
														// fomrat."+""+dataSet.get(""+webElement.getAttribute("id")));
													}
												}
											}
										}

									} catch (Exception e3) {

									}
								}

							} else if (webElement.getAttribute("type").indexOf("select-multiple") != -1) {
								if (webElement.getAttribute("value") == null
										|| "".equals(webElement.getAttribute("value"))
										|| "null".equals(webElement.getAttribute("value"))
										|| "-1".equals(webElement.getAttribute("value"))) {
									try {
										WebElement element = driver
												.findElement(By.id("ms-parent" + webElement.getAttribute("" + id)));
										fc.utobj().clickElement(driver, element);
										fc.utobj().clickElement(driver,
												fc.utobj().getElementByXpath(driver, ".//*[@id='selectAll']"));
										fc.utobj().clickElement(driver, element);
									} catch (Exception emultipal) {
										// Reporter.log("Problem in multiple
										// value drop-down :
										// "+""+webElement.getAttribute("id"));
									}
								}
							} else if (webElement.getAttribute("type").indexOf("textarea") != -1) {
								if (webElement.getAttribute("value") == null
										|| "".equals(webElement.getAttribute("value"))
										|| "null".equals(webElement.getAttribute("value"))) {
									try {
										driver.findElement(By.name("" + webElement.getAttribute("" + id)))
												.sendKeys(dataSet.get("" + webElement.getAttribute("" + id)));
										listItems.add(dataSet.get("" + webElement.getAttribute("" + id)));
										printInfo.put("" + webElement.getAttribute("id"),
												dataSet.get("" + webElement.getAttribute("" + id)));
									} catch (Exception eText) {
										// Reporter.log("Problem in entering
										// TextArea in :
										// "+""+webElement.getAttribute("id"));
									}
								}
							} else if (webElement.getAttribute("type").indexOf("radio") != -1) {
								try {
									String sValue = "";
									List<WebElement> rdBtn_Sex = driver
											.findElements(By.id("" + webElement.getAttribute("" + id))); //
									if (alreadyradio.containsKey(webElement.getAttribute("" + id))) {

									} else {

										int size = rdBtn_Sex.size();
										if (size > 1) {
											sValue = dataSet.get("" + webElement.getAttribute("" + id));
										}
										try {
											List<WebElement> rdBtn_Field = null;
											WebElement element = null;
											try {
												rdBtn_Field = driver.findElements(By.xpath(
														".//*[@name='" + webElement.getAttribute("" + id) + "']")); // work
												element = fc.utobj().getElementByXpath(driver,
														".//*[@name='" + webElement.getAttribute("" + id) + "']");
											} catch (Exception eTxt) {
												rdBtn_Field = driver.findElements(
														By.xpath(".//*[@id='" + webElement.getAttribute("id") + "']")); // work
												element = driver.findElement(
														By.xpath(".//*[@id='" + webElement.getAttribute("id") + "']"));

											}

											fc.utobj().moveToElement(driver, element);
											fc.utobj().clickRadioButton(driver, rdBtn_Field, sValue); // work
											listItems.add(dataSet.get("" + webElement.getAttribute("" + id)));
											printInfo.put("" + webElement.getAttribute("id"),
													dataSet.get("" + webElement.getAttribute("" + id)));
											alreadyradio.put(webElement.getAttribute("" + id),
													webElement.getAttribute("" + id));
										} catch (Exception eText) {
											// Reporter.log("Problem in
											// selecting radio value in :
											// "+""+webElement.getAttribute("id"));
										}
									}
								} catch (Exception exp) {
									Reporter.log(exp.toString());
									exp.printStackTrace();
								}

							} else if (webElement.getAttribute("type").toLowerCase().indexOf("checkbox") != -1) {
								List<WebElement> oCheckBox = driver
										.findElements(By.name("" + webElement.getAttribute("" + id)));
								String sValue = "";
								if (alreadycheckBox.containsKey(webElement.getAttribute("" + id))) {

								} else {

									int size = oCheckBox.size();
									for (int i = 0; i < size; i++) {
										sValue = oCheckBox.get(i).getAttribute("value");
										// Thread.sleep(2000);
										if (i == (size - 1)) {

											// break;
										}

									}
									try {
										List<WebElement> rdBtn_Field = driver.findElements(By.xpath(
												"//*[contains(@id, '" + webElement.getAttribute("" + id) + "')]")); // work
										WebElement element = fc.utobj().getElementByXpath(driver,
												"//*[contains(@id, '" + webElement.getAttribute("" + id) + "')]");
										fc.utobj().moveToElement(driver, element);
										fc.utobj().clickRadioButton(driver, rdBtn_Field, sValue); // work
										alreadycheckBox.put(webElement.getAttribute("" + id),
												webElement.getAttribute("" + id));
									} catch (Exception eText) {

									}
								}
							} else if (webElement.getAttribute("type").indexOf("file") != -1) {
								String fileName = fc.utobj().getFilePathFromTestData("pictureFile");
								try {
									fc.utobj().sendKeys(driver,
											fc.utobj().getElementByID(driver, "" + webElement.getAttribute("" + id)),
											fileName);
									listItems.add(fileName);
									printInfo.put("" + webElement.getAttribute("" + id), fileName);
								} catch (Exception eFile) {
									// Reporter.log("Problem in uploading file
									// type value in :
									// "+""+webElement.getAttribute("id"));
								}
							} else if (webElement.getAttribute("type").indexOf("text") != -1) {
								if (webElement.getAttribute("value") == null
										|| "".equals(webElement.getAttribute("value"))
										|| "null".equals(webElement.getAttribute("value"))
										|| "0.00".equals(webElement.getAttribute("value"))) {
									if (webElement.getAttribute("class").indexOf("fTextBoxDate") != -1) {
										try {
											if (dataSet.get("" + webElement.getAttribute("" + id)) != null
													|| !"".equals(dataSet.get("" + webElement.getAttribute("" + id)))
													|| !"null".equals(
															dataSet.get("" + webElement.getAttribute("" + id)))) {
												fc.utobj().sendKeys(driver,
														driver.findElement(
																By.name("" + webElement.getAttribute("" + id))),
														dataSet.get("" + webElement.getAttribute("" + id))); // work
												listItems.add(dataSet.get("" + webElement.getAttribute("" + id)));
												printInfo.put("" + webElement.getAttribute("" + id),
														dataSet.get("" + webElement.getAttribute("" + id)));

											} else {

											}
										} catch (Exception eDate) {
											// Reporter.log("Problem in Entering
											// Date fields value in :
											// "+""+webElement.getAttribute("id"));
										}
									} else {
										try {
											if ("0.00".equals(webElement.getAttribute("value"))) {
												fc.utobj()
														.sendKeys(driver,
																driver.findElement(
																		By.name("" + webElement.getAttribute("" + id))),
																"");

												// System.out.println(webElement.getAttribute("value")
												// +"======"+dataSet.get(""+webElement.getAttribute(""+id))
												// +"====="+""+webElement.getAttribute(""+id));

											}
											if ("name".equals(id)) {
												fc.utobj().sendKeys(driver,
														driver.findElement(
																By.name("" + webElement.getAttribute("" + id))),
														dataSet.get("" + webElement.getAttribute("" + id)));
												if ("accountName".equals("" + webElement.getAttribute("" + id))
														|| "opportunityOwner"
																.equals("" + webElement.getAttribute("" + id))
														|| "oppContactID"
																.equals("" + webElement.getAttribute("" + id))) {
													;
													fc.utobj().clickElement(driver, driver.findElement(
															By.xpath(".//*[@id='customizedAjaxSearch']/div")));
													fc.utobj().sleep();
												}
											} else {
												fc.utobj().sendKeys(driver,
														driver.findElement(
																By.id("" + webElement.getAttribute("" + id))),
														dataSet.get("" + webElement.getAttribute("" + id)));
												if ("accountName".equals("" + webElement.getAttribute("" + id))
														|| "opportunityOwner"
																.equals("" + webElement.getAttribute("" + id))
														|| "oppContactID"
																.equals("" + webElement.getAttribute("" + id))) {
													;
													fc.utobj().clickElement(driver, driver.findElement(
															By.xpath(".//*[@id='customizedAjaxSearch']/div")));
													fc.utobj().sleep();
												}
											}

											if (!"ssn".equals("" + webElement.getAttribute("" + id))
													&& !"spouseSsn".equals("" + webElement.getAttribute("" + id))) {
												listItems.add(dataSet.get("" + webElement.getAttribute("" + id)));
											}
											printInfo.put("" + webElement.getAttribute("" + id),
													dataSet.get("" + webElement.getAttribute("" + id)));

										} catch (Exception eText) {
											// Reporter.log("Problem in Entering
											// Text fileds value in :
											// "+""+webElement.getAttribute("id"));
										}

									}
								}
							}
							alreadyitrateElement.put(webElement.getAttribute("" + id),
									webElement.getAttribute("" + id));
						}
					}
				}
			}

			if ("TC_CRM_QA_Lead_Details".equals(testCaseId)) {

				Select singledrop = null;
				List<WebElement> singleDropDown = null;
				try {
					singledrop = new Select(driver.findElement(By.name("leadOwnerID2")));
					singleDropDown = driver.findElements(By.name("leadOwnerID2"));
					fc.utobj().sleep(2000);//please do not remove this line
					WebElement dropDownField = driver.findElement(By.name("leadOwnerID2"));
					fc.utobj().selectDropDownByVisibleText(driver, dropDownField, dataSet.get("leadOwnerID2"));
					fc.utobj().sleep(2000);//please do not remove this line
					dropDownField = driver.findElement(By.name("cmSource2ID"));
					fc.utobj().selectDropDownByVisibleText(driver, dropDownField, dataSet.get("cmSource2ID"));
					fc.utobj().sleep(3000);//please do not remove this line
					dropDownField = driver.findElement(By.name("cmSource3ID"));
					fc.utobj().selectDropDownByVisibleText(driver, dropDownField, dataSet.get("cmSource3ID"));

				} catch (Exception e) {

				}

			}
			if ("TC_CRM_QA_Contact_Details".equals(testCaseId)) {

				Select singledrop = null;
				List<WebElement> singleDropDown = null;
				try {
					singledrop = new Select(driver.findElement(By.name("contactOwnerID2")));
					singleDropDown = driver.findElements(By.name("contactOwnerID2"));
					fc.utobj().sleep(2000);//please do not remove this line
					WebElement dropDownField = driver.findElement(By.name("contactOwnerID2"));
					fc.utobj().selectDropDownByVisibleText(driver, dropDownField, dataSet.get("contactOwnerID2"));
					fc.utobj().sleep(2000);//please do not remove this line
					dropDownField = driver.findElement(By.name("cmSource2ID"));
					fc.utobj().selectDropDownByVisibleText(driver, dropDownField, dataSet.get("cmSource2ID"));
					fc.utobj().sleep(3000);//please do not remove this line
					dropDownField = driver.findElement(By.name("cmSource3ID"));
					fc.utobj().selectDropDownByVisibleText(driver, dropDownField, dataSet.get("cmSource3ID"));

				} catch (Exception e) {

				}

			}

		} catch (Exception e) {
			// fc.utobj().throwsException("Fields data are not available!
			// "+e.getMessage());
		}
		try {
			// fc.utobj().printTestStep(testCaseId, "Submiting the Tab ");
			WebElement elementbutton = fc.utobj().getElementByXpath(driver,
					".//*[@id='Submit' or @name='Submit' or @name='button' or @name='Submit1' or @name='add1'] ");
			fc.utobj().clickElement(driver, elementbutton);

		} catch (Exception ee) {
			fc.utobj().printBugStatus("Unable to submit page " + ee);
		}

		return printInfo;
	}

	public void addRolesWithModulesPrivileges(WebDriver driver, String roleType, String roleName, String moduleName,
			Map<String, String> privileges, String testCaseId) throws Exception {

		fc.utobj().printTestStep("Admin > Users > Roles > Add New Role");
		fc.adminpage().adminUsersRolesAddNewRolePage(driver);
		AdminUsersRolesAddNewRolePage pobj = new AdminUsersRolesAddNewRolePage(driver);

		// webdriver wait
		fc.utobj().webDriverWait(driver, ".//select[@name='roleType']");

		if (roleType.equalsIgnoreCase("Corporate Role")) {
			fc.utobj().selectDropDownByValue(driver, pobj.roleType, "1");

		} else if (roleType.equalsIgnoreCase("Division Level")) {
			fc.utobj().selectDropDownByValue(driver, pobj.roleType, "5");

		} else if (roleType.equalsIgnoreCase("Regional Level")) {
			fc.utobj().selectDropDownByValue(driver, pobj.roleType, "2");

		} else if (roleType.equalsIgnoreCase("Franchise Level")) {
			fc.utobj().selectDropDownByValue(driver, pobj.roleType, "0");
		}

		fc.utobj().sendKeys(driver, pobj.roleName, roleName);

		if (moduleName.equalsIgnoreCase("CRM")) {

			Set<String> keySet = privileges.keySet();

			for (String s : keySet) {
				if (s.contains("Can Delete Leads")) {
					if (privileges.get("Can Delete Leads").equalsIgnoreCase("Yes")) {

						if (!fc.utobj().isSelected(driver,fc.utobj().getElementByXpath(driver , ".//td[contains(text(),'Can Delete Leads')]/ancestor::tr/td/input[@id='204' or @id='5' or @id='4']"))) {
							fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
									".//td[contains(text(),'Can Delete Leads')]/ancestor::tr/td/input[@id='204' or @id='5' or @id='4']"));
						}

					} else if (privileges.get("Can Delete Leads").equalsIgnoreCase("No")) {

						if (fc.utobj().isSelected(driver,fc.utobj().getElementByXpath(driver ,".//td[contains(text(),'Can Delete Leads')]/ancestor::tr/td/input[@id='204' or @id='5' or @id='4']"))
								) {
							fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
									".//td[contains(text(),'Can Delete Leads')]/ancestor::tr/td/input[@id='204' or @id='5' or @id='4']"));
						}
					}

				} else if (s.contains("Can View All Leads")) {
					if (privileges.get("Can View All Leads").equalsIgnoreCase("Yes")) {

						if (!fc.utobj().isSelected(driver,fc.utobj().getElementByXpath(driver ,".//td[contains(text(),'Can View All Leads')]/ancestor::tr/td/input[@id='204' or @id='5' or @id='4']"))
								) {
							fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
									".//td[contains(text(),'Can View All Leads')]/ancestor::tr/td/input[@id='204' or @id='5' or @id='4']"));
						}

					} else if (privileges.get("Can View All Leads").equalsIgnoreCase("No")) {

						if (fc.utobj().isSelected(driver,fc.utobj().getElementByXpath(driver ,".//td[contains(text(),'Can View All Leads')]/ancestor::tr/td/input[@id='204' or @id='5' or @id='4']"))
								) {
							fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
									".//td[contains(text(),'Can View All Leads')]/ancestor::tr/td/input[@id='204' or @id='5' or @id='4']"));
						}
					}

				}

			}
		}

		fc.utobj().clickElement(driver, pobj.submit);
		AdminUsersRolesAddRoleAddUsersforRolePageTest p2 = new AdminUsersRolesAddRoleAddUsersforRolePageTest();
		p2.assignLater(driver);

		fc.utobj().printTestStep("Verify The Added Role");
		boolean isTextPresent = fc.utobj().assertPageSource(driver, roleName);
		if (isTextPresent == false) {
			fc.utobj().throwsException("Added Corporate Role is not present at Admin > Users > Roles Page");
		}
	}

	public void addRolesPrivilegetoseepriavatecampaign(WebDriver driver, String roleType, String roleName, String moduleName,
			Map<String, String> privileges, String testCaseId) throws Exception {

		fc.utobj().printTestStep("Admin > Users > Roles > Add New Role");
		fc.adminpage().adminUsersRolesAddNewRolePage(driver);
		AdminUsersRolesAddNewRolePage pobj = new AdminUsersRolesAddNewRolePage(driver);

		// webdriver wait
		fc.utobj().webDriverWait(driver, ".//select[@name='roleType']");

		if (roleType.equalsIgnoreCase("Corporate Role")) {
			fc.utobj().selectDropDownByValue(driver, pobj.roleType, "1");

		} else if (roleType.equalsIgnoreCase("Division Level")) {
			fc.utobj().selectDropDownByValue(driver, pobj.roleType, "5");

		} else if (roleType.equalsIgnoreCase("Regional Level")) {
			fc.utobj().selectDropDownByValue(driver, pobj.roleType, "2");

		} else if (roleType.equalsIgnoreCase("Franchise Level")) {
			fc.utobj().selectDropDownByValue(driver, pobj.roleType, "0");
		}

		fc.utobj().sendKeys(driver, pobj.roleName, roleName);

		if (moduleName.equalsIgnoreCase("CRM")) {

			Set<String> keySet = privileges.keySet();

			for (String s : keySet) {
				if (s.contains("Can View Private Folders / Templates of Other Users ")) {
					if (privileges.get("Can View Private Folders / Templates of Other Users ").equalsIgnoreCase("Yes")) {

						if (!fc.utobj().isSelected(driver,fc.utobj().getElementByXpath(driver , ".//td[contains(text(),'Can View Private Folders / Templates of Other Users ')]/ancestor::tr/td/input[@id='204' or @id='5' or @id='4']"))) {
							fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
									".//td[contains(text(),'Can View Private Folders / Templates of Other Users ')]/ancestor::tr/td/input[@id='204' or @id='5' or @id='4']"));
						}

					} else if (privileges.get("Can View Private Folders / Templates of Other Users ").equalsIgnoreCase("No")) {

						if (fc.utobj().isSelected(driver,fc.utobj().getElementByXpath(driver ,".//td[contains(text(),'Can View Private Folders / Templates of Other Users ')]/ancestor::tr/td/input[@id='204' or @id='5' or @id='4']"))
								) {
							fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
									".//td[contains(text(),'Can View Private Folders / Templates of Other Users ')]/ancestor::tr/td/input[@id='204' or @id='5' or @id='4']"));
						}
					}

				} 
			}
		}

		fc.utobj().clickElement(driver, pobj.submit);
		AdminUsersRolesAddRoleAddUsersforRolePageTest p2 = new AdminUsersRolesAddRoleAddUsersforRolePageTest();
		p2.assignLater(driver);

		fc.utobj().printTestStep("Verify The Added Role");
		boolean isTextPresent = fc.utobj().assertPageSource(driver, roleName);
		if (isTextPresent == false) {
			fc.utobj().throwsException("Added Corporate Role is not present at Admin > Users > Roles Page");
		}
	}	
}