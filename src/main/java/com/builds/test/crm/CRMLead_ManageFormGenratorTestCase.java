package com.builds.test.crm;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.testng.Reporter;
import org.testng.annotations.Test;

import com.builds.test.admin.AdminUsersManageCorporateUsersAddCorporateUserPageTest;
import com.builds.test.common.CorporateUser;
import com.builds.test.infomgr.AdminInfoMgrCommonMethods;
import com.builds.uimaps.crm.CRMLeadsPage;
import com.builds.uimaps.infomgr.AdminInfoMgrManageFormGeneratorAddTabPage;
import com.builds.uimaps.infomgr.AdminInfoMgrManageFormGeneratorPage;
import com.builds.uimaps.infomgr.AdminInfoMgrManageFormGeneratorTabDetailsPage;
import com.builds.utilities.FranconnectUtil;
import com.builds.utilities.TestCase;

public class CRMLead_ManageFormGenratorTestCase {

	FranconnectUtil fc = new FranconnectUtil();

	@Test(groups = { "crm", "TC_Lead_CRM_FormGenCase" })
	@TestCase(createdOn = "2018-01-16", updatedOn = "2018-01-16", testCaseId = "TC_Lead_CRM_FormGenCase", testCaseDescription = "To Verify custom fields in leads.")
	private void crmInfoFillAreaFormGen_1() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		Map<String, String> dataSetCustom = null;
		String testCaseIdInternal = "" + testCaseId;
		Map<String, String> printInfo = null;
		AdminInfoMgrCommonMethods adminCommonMethods = fc.adminpage().adminInfoMgr();
		Map<String, String> dataSet = fc.utobj().readTestData("crm", "TC_FS_FormGenerator_006");
		try {
			driver = fc.loginpage().login(driver);
			CRMLeadsPage pobj = new CRMLeadsPage(driver);

			fc.utobj().printTestStep("Navigate To Admin > CRM > Manage Form Generator");
			fc.utobj().printTestStep("Add New Tab");

			fc.crm().crm_common().adminCRMManageFormGeneratorLnk(driver);
			fc.adminpage().adminInfoMgr();
			AdminInfoMgrManageFormGeneratorPage manageFormGeneratorPage = new AdminInfoMgrManageFormGeneratorPage(
					driver);
			AdminInfoMgrManageFormGeneratorTabDetailsPage ADM_form_gen = manageFormGeneratorPage.getAddTabDetailsPage();
			adminCommonMethods.NavigateToManageFormGeneratorTabs(driver, "Lead");

			fc.utobj().clickElement(driver, manageFormGeneratorPage.btnAddNewTab);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			AdminInfoMgrManageFormGeneratorAddTabPage objAddNewTabPage = new AdminInfoMgrManageFormGeneratorAddTabPage(
					driver);
			String tabName = fc.utobj().generateTestData("Tab");
			fc.utobj().sendKeys(driver, objAddNewTabPage.txtDisplayName, tabName);
			fc.utobj().clickElement(driver, objAddNewTabPage.multipleInputNo);
			fc.utobj().clickElement(driver, objAddNewTabPage.btnSubmit);
			fc.utobj().switchFrameToDefault(driver);
			fc.utobj().clickLink(driver, tabName);

			fc.utobj().clickElement(driver, ADM_form_gen.btnAddSection);

			fc.commonMethods().switch_cboxIframe_frameId(driver);
			String SectionName = "Sec";
			SectionName = fc.utobj().generateTestData(SectionName);
			fc.utobj().sendKeys(driver, ADM_form_gen.txtSectionName, SectionName);
			fc.utobj().clickElement(driver, ADM_form_gen.btnAddSectionName);
			fc.utobj().switchFrameToDefault(driver);
			fc.utobj().clickElement(driver, ADM_form_gen.btnAddField);
			fc.commonMethods().switch_cboxIframe_frameId(driver);

			String fieldNameTextArea = "TextArea";
			fieldNameTextArea = fc.utobj().generateTestData(fieldNameTextArea);
			fc.utobj().sendKeys(driver, ADM_form_gen.txtFieldName, fieldNameTextArea);
			// fc.utobj().clickElement(driver, ADM_form_gen.documentopt);
			fc.utobj().selectDropDown(driver, ADM_form_gen.FieldType, "Text Area");
			fc.utobj().clickElement(driver, ADM_form_gen.btnSumbitField);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().clickElement(driver, ADM_form_gen.btnAddField);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			String fieldNameText = "Text";
			fieldNameText = fc.utobj().generateTestData(fieldNameText);
			fc.utobj().sendKeys(driver, ADM_form_gen.txtFieldName, fieldNameText);
			// fc.utobj().clickElement(driver, ADM_form_gen.documentopt);
			fc.utobj().selectDropDown(driver, ADM_form_gen.FieldType, "Text");
			fc.utobj().clickElement(driver, ADM_form_gen.btnSumbitField);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().clickElement(driver, ADM_form_gen.btnAddField);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			String fieldNameDate = "Date";
			fieldNameDate = fc.utobj().generateTestData(fieldNameDate);
			fc.utobj().sendKeys(driver, ADM_form_gen.txtFieldName, fieldNameDate);
			// fc.utobj().clickElement(driver, ADM_form_gen.documentopt);
			fc.utobj().selectDropDown(driver, ADM_form_gen.FieldType, "Date");
			fc.utobj().clickElement(driver, ADM_form_gen.btnSumbitField);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().clickElement(driver, ADM_form_gen.btnAddField);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			String fieldNameDropdown = "DropDown";
			fieldNameDropdown = fc.utobj().generateTestData(fieldNameDropdown);
			fc.utobj().sendKeys(driver, ADM_form_gen.txtFieldName, fieldNameDropdown);
			// fc.utobj().clickElement(driver, ADM_form_gen.documentopt);
			fc.utobj().selectDropDown(driver, ADM_form_gen.FieldType, "Drop-down");
			fc.utobj().sendKeys(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='row_1']/td[2]/input[1]"),
					"Opt" + fieldNameDropdown);

			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//*[@id='removeTDForAddMore']//*[contains(text(),'Add Option')]"));
			fc.utobj().sendKeys(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='row_2']/td[2]/input[1]"),
					"DropValue1");
			fc.utobj().clickElement(driver, ADM_form_gen.btnSumbitField);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().clickElement(driver, ADM_form_gen.btnAddField);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			String fieldNameRadio = "Radio";
			fieldNameRadio = fc.utobj().generateTestData(fieldNameRadio);
			fc.utobj().sendKeys(driver, ADM_form_gen.txtFieldName, fieldNameRadio);
			// fc.utobj().clickElement(driver, ADM_form_gen.documentopt);
			fc.utobj().selectDropDown(driver, ADM_form_gen.FieldType, "Radio");
			fc.utobj().sendKeys(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='row_1']/td[2]/input[1]"),
					"Radio1");
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//*[@id='removeTDForAddMore']//*[contains(text(),'Add Option')]"));
			fc.utobj().sendKeys(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='row_2']/td[2]/input[1]"),
					"Opt2" + fieldNameRadio);
			fc.utobj().clickElement(driver, ADM_form_gen.btnSumbitField);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().clickElement(driver, ADM_form_gen.btnAddField);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			String fieldNameCheckbox = "Checkbox";
			fieldNameCheckbox = fc.utobj().generateTestData(fieldNameCheckbox);
			fc.utobj().sendKeys(driver, ADM_form_gen.txtFieldName, fieldNameCheckbox);
			// fc.utobj().clickElement(driver, ADM_form_gen.documentopt);
			fc.utobj().selectDropDown(driver, ADM_form_gen.FieldType, "Checkbox");
			fc.utobj().sendKeys(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='row_1']/td[2]/input[1]"),
					"Checkbox1");
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//*[@id='removeTDForAddMore']//*[contains(text(),'Add Option')]"));
			fc.utobj().sendKeys(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='row_2']/td[2]/input[1]"),
					"Opt2");
			fc.utobj().clickElement(driver, ADM_form_gen.btnSumbitField);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().clickElement(driver, ADM_form_gen.btnAddField);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			String fieldNameNumeric = "Numeric";
			fieldNameNumeric = fc.utobj().generateTestData(fieldNameNumeric);
			fc.utobj().sendKeys(driver, ADM_form_gen.txtFieldName, fieldNameNumeric);
			// fc.utobj().clickElement(driver, ADM_form_gen.documentopt);
			fc.utobj().selectDropDown(driver, ADM_form_gen.FieldType, "Numeric");
			fc.utobj().clickElement(driver, ADM_form_gen.btnSumbitField);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().clickElement(driver, ADM_form_gen.btnAddField);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			String fieldNameMultiSelectDropdown = "Multislect";
			fieldNameMultiSelectDropdown = fc.utobj().generateTestData(fieldNameMultiSelectDropdown);
			fc.utobj().sendKeys(driver, ADM_form_gen.txtFieldName, fieldNameMultiSelectDropdown);
			// fc.utobj().clickElement(driver, ADM_form_gen.documentopt);
			fc.utobj().selectDropDown(driver, ADM_form_gen.FieldType, "Multi Select Drop-down");
			fc.utobj().sendKeys(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='row_1']/td[2]/input[1]"),
					"Mul" + fieldNameMultiSelectDropdown);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//*[@id='removeTDForAddMore']//*[contains(text(),'Add Option')]"));
			fc.utobj().sendKeys(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='row_2']/td[2]/input[1]"),
					"Multi1");
			fc.utobj().clickElement(driver, ADM_form_gen.btnSumbitField);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().clickElement(driver, ADM_form_gen.btnAddField);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			String fieldNameDocument = "Doc";
			fieldNameDocument = fc.utobj().generateTestData(fieldNameDocument);
			fc.utobj().sendKeys(driver, ADM_form_gen.txtFieldName, fieldNameDocument);
			// fc.utobj().clickElement(driver, ADM_form_gen.documentopt);
			fc.utobj().selectDropDown(driver, ADM_form_gen.FieldType, "Document");
			fc.utobj().clickElement(driver, ADM_form_gen.btnSumbitField);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Navigate To Admin Users Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			String emailId = "crmautomation@staffex.com";
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUser = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporateUser.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			fc.utobj().printTestStep("Navigate To CRM > Leads > Leads Summary");
			fc.utobj().printTestStep("Add First Lead");
			fc.crm().crm_common().CRMLeadsLnk(driver);
			fc.utobj().clickElement(driver, pobj.addNew);
			fc.utobj().clickElement(driver, pobj.leadLnk);

			testCaseIdInternal = "TC_CRM_QA_Lead_Details";
			dataSetCustom = fc.utobj().readTestData("crm", testCaseIdInternal);
			String firstName = fc.utobj().generateTestData("Aa");
			String lastName = fc.utobj().generateTestData("Bb");
			dataSetCustom.put("leadFirstName", firstName);
			dataSetCustom.put("leadLastName", lastName);
			dataSetCustom.put("cmLeadTypeID", "Select");
			dataSetCustom.put("cmSource2ID", "Internet");
			dataSetCustom.put("cmSource3ID", "BISON");
			dataSetCustom.put("leadOwnerID2", corpUser.getuserFullName());

			printInfo = fillFormDataWithDataset(driver, config, testCaseIdInternal, dataSetCustom);
			// Reporter.log("Map == for ="+testCaseIdInternal+"==="+printInfo);

			if (printInfo != null && printInfo.size() > 0) {
				// System.out.println("printInfo=First="+printInfo);

				// System.out.println("printInfo=First Email
				// ="+printInfo.get("emailIds"));
			}

			fc.utobj().clickElement(driver, pobj.leadsLink);

			systemExactSearch(driver, firstName + " " + lastName);

			fc.utobj().printTestStep("Verify Lead Name in Lead Summary.");
			boolean isLeadNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//*[.='" + firstName + " " + lastName + "']");
			if (isLeadNamePresent == false) {
				fc.utobj().throwsException("was not able to verify First Lead Name");
			}

			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//a[contains(text () ,'" + firstName + " " + lastName + "')]"));

			fieldNameText = fieldNameText.toLowerCase();
			fieldNameTextArea = fieldNameTextArea.toLowerCase();
			fieldNameDate = fieldNameDate.toLowerCase();
			fieldNameDropdown = fieldNameDropdown.toLowerCase();
			fieldNameRadio = fieldNameRadio.toLowerCase();
			fieldNameNumeric = fieldNameNumeric.toLowerCase();
			fieldNameCheckbox = fieldNameCheckbox.toLowerCase();
			fieldNameMultiSelectDropdown = fieldNameMultiSelectDropdown.toLowerCase();
			fieldNameDocument = fieldNameDocument.toLowerCase();

			String fieldNameAction = fieldNameText;
			fieldNameText = fieldNameText.toLowerCase();

			fc.utobj().printTestStep("Navigate to newly added Tab at Lead Info");
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//*[@qat_tabname='" + tabName + "']"));

			Thread.sleep(2000);
			fc.utobj().sendKeys(driver,
					fc.utobj().getElementByXpath(driver, "//input[contains(@name, '_" + fieldNameText + "')]"),
					fieldNameText); // Work
			fc.utobj().sendKeys(driver,
					fc.utobj().getElementByXpath(driver, "//*[contains(@id, '_" + fieldNameTextArea + "')]"),
					fieldNameTextArea); // work
			fc.utobj().sendKeys(driver,
					fc.utobj().getElementByXpath(driver, "//input[contains(@name, '_" + fieldNameDate + "')]"),
					"02/08/2018"); // work
			fc.utobj().selectDropDownByVisibleText(driver,
					fc.utobj().getElementByXpath(driver, "//*[contains(@name, '_" + fieldNameDropdown + "')]"),
					"DropValue1"); // work
			List<WebElement> rdBtn_Field = driver
					.findElements(By.xpath("//*[contains(@name, '_" + fieldNameRadio + "')]")); // work
			fc.utobj().clickRadioButton(driver, rdBtn_Field, "1"); // work
			fc.utobj().sendKeys(driver,
					fc.utobj().getElementByXpath(driver, "//input[contains(@name, '_" + fieldNameNumeric + "')]"),
					"100"); // work
			List<WebElement> checkbox = driver
					.findElements(By.xpath("//*[contains(@name, '_" + fieldNameCheckbox + "')]"));
			((WebElement) checkbox.get(0)).click();
			WebElement element = driver
					.findElement(By.xpath(".//div[contains(@id , '" + fieldNameMultiSelectDropdown + "')]"));
			fc.utobj().setToDefault(driver, element);
			WebElement elementInput = element.findElement(By.xpath("./div/div/input"));
			fc.utobj().selectValFromMultiSelect(driver, element, elementInput, "Multi1");

			String fileName = fc.utobj().getFilePathFromTestData(dataSet.get("fileName")); // "C:\\images.jpg"
			// ;//
			fc.utobj().sendKeys(driver, fc.utobj().getElementByXpath(driver, "//input[@type='file']"), fileName);

			Thread.sleep(1000);
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//*[@type='button' and @name='Submit' and @value='Add']"));

			fc.utobj().printTestStep("Verify The Added Field inb Lead Tab");
			boolean isfieldNameText = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () , '" + fieldNameText + "')]");
			if (isfieldNameText == false) {
				fc.utobj().throwsException("was not able to verify Text Field");
			}
			boolean isfieldNameTextArea = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () , '" + fieldNameTextArea + "')]");
			if (isfieldNameTextArea == false) {
				fc.utobj().throwsException("was not able to verify TextArea Field");
			}
			boolean isfieldNameDate = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//td[contains(text () , '02/08/2018')]");
			if (isfieldNameDate == false) {
				fc.utobj().throwsException("was not able to verify Date Field");
			}
			boolean isfieldNameDropdown = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//td[contains(text () , 'DropValue1')]");
			if (isfieldNameDropdown == false) {
				fc.utobj().throwsException("was not able to verify DropDown Field");
			}

			boolean isfieldNameRadio = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//td[contains(text () , 'Radio1')]");
			if (isfieldNameRadio == false) {
				fc.utobj().throwsException("was not able to verify Radio Field");
			}
			boolean isfieldNameNumeric = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//td[contains(text () , '100')]");
			if (isfieldNameNumeric == false) {
				fc.utobj().throwsException("was not able to verify numric Field");
			}
			boolean isfieldNameCheckbox = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//td[contains(text () , 'Checkbox1')]");
			if (isfieldNameCheckbox == false) {
				fc.utobj().throwsException("was not able to verify checkBox Field");
			}
			boolean isfieldNameMultiSelectDropdown = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () , 'Multi1')]");
			if (isfieldNameMultiSelectDropdown == false) {
				fc.utobj().throwsException("was not able to verify Multi Select Field");
			}
			boolean isfileName = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[contains(text () , '.jpg')]");
			if (isfileName == false) {
				fc.utobj().throwsException("was not able to verify File Type Field");
			}

			fc.utobj().printTestStep("Navigate to Print Tab at Lead tab Info");
			Thread.sleep(1000);
			try {
				fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
						".//*[@type='button' and @name='Button0' and @value='Print']"));
				VerifyPrintPreview(driver, fieldNameText);

			} catch (Exception w) {
				fc.utobj().throwsException("was not able to verify Print functionality.");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "crm","testingpurpose" })
	@TestCase(createdOn = "2018-01-16", updatedOn = "2018-01-16", testCaseId = "TC_Lead_CRM_FormMultiGenCase", testCaseDescription = "To Verify custom fields in leads.")
	private void crmInfoFillAreaFormGen_2() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		Map<String, String> dataSetCustom = null;
		String testCaseIdInternal = "" + testCaseId;
		Map<String, String> printInfo = null;
		AdminInfoMgrCommonMethods adminCommonMethods = fc.adminpage().adminInfoMgr();
		Map<String, String> dataSet = fc.utobj().readTestData("crm", "TC_FS_FormGenerator_006");
		try {
			driver = fc.loginpage().login(driver);
			CRMLeadsPage pobj = new CRMLeadsPage(driver);

			fc.utobj().printTestStep("Navigate To Admin > CRM > Manage Form Generator");
			fc.utobj().printTestStep("Add New Tab");

			fc.crm().crm_common().adminCRMManageFormGeneratorLnk(driver);
			fc.adminpage().adminInfoMgr();
			AdminInfoMgrManageFormGeneratorPage manageFormGeneratorPage = new AdminInfoMgrManageFormGeneratorPage(
					driver);
			AdminInfoMgrManageFormGeneratorTabDetailsPage ADM_form_gen = manageFormGeneratorPage.getAddTabDetailsPage();
			adminCommonMethods.NavigateToManageFormGeneratorTabs(driver, "Lead");

			fc.utobj().clickElement(driver, manageFormGeneratorPage.btnAddNewTab);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			AdminInfoMgrManageFormGeneratorAddTabPage objAddNewTabPage = new AdminInfoMgrManageFormGeneratorAddTabPage(
					driver);
			String tabName = fc.utobj().generateTestData("Tab");
			fc.utobj().sendKeys(driver, objAddNewTabPage.txtDisplayName, tabName);
			// fc.utobj().clickElement(driver,
			// objAddNewTabPage.multipleInputNo);
			fc.utobj().clickElement(driver, objAddNewTabPage.btnSubmit);
			fc.utobj().switchFrameToDefault(driver);
			fc.utobj().clickLink(driver, tabName);

			fc.utobj().clickElement(driver, ADM_form_gen.btnAddSection);

			fc.commonMethods().switch_cboxIframe_frameId(driver);
			String SectionName = "Sec";
			SectionName = fc.utobj().generateTestData(SectionName);
			fc.utobj().sendKeys(driver, ADM_form_gen.txtSectionName, SectionName);
			fc.utobj().clickElement(driver, ADM_form_gen.btnAddSectionName);
			fc.utobj().switchFrameToDefault(driver);
			fc.utobj().clickElement(driver, ADM_form_gen.btnAddField);
			fc.commonMethods().switch_cboxIframe_frameId(driver);

			String fieldNameTextArea = "TextArea";
			fieldNameTextArea = fc.utobj().generateTestData(fieldNameTextArea);
			fc.utobj().sendKeys(driver, ADM_form_gen.txtFieldName, fieldNameTextArea);
			// fc.utobj().clickElement(driver, ADM_form_gen.documentopt);
			fc.utobj().selectDropDown(driver, ADM_form_gen.FieldType, "Text Area");
			fc.utobj().clickElement(driver, ADM_form_gen.btnSumbitField);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().clickElement(driver, ADM_form_gen.btnAddField);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			String fieldNameText = "Text";
			fieldNameText = fc.utobj().generateTestData(fieldNameText);
			fc.utobj().sendKeys(driver, ADM_form_gen.txtFieldName, fieldNameText);
			// fc.utobj().clickElement(driver, ADM_form_gen.documentopt);
			fc.utobj().selectDropDown(driver, ADM_form_gen.FieldType, "Text");
			fc.utobj().clickElement(driver, ADM_form_gen.btnSumbitField);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().clickElement(driver, ADM_form_gen.btnAddField);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			String fieldNameDate = "Date";
			fieldNameDate = fc.utobj().generateTestData(fieldNameDate);
			fc.utobj().sendKeys(driver, ADM_form_gen.txtFieldName, fieldNameDate);
			// fc.utobj().clickElement(driver, ADM_form_gen.documentopt);
			fc.utobj().selectDropDown(driver, ADM_form_gen.FieldType, "Date");
			fc.utobj().clickElement(driver, ADM_form_gen.btnSumbitField);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().clickElement(driver, ADM_form_gen.btnAddField);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			String fieldNameDropdown = "DropDown";
			fieldNameDropdown = fc.utobj().generateTestData(fieldNameDropdown);
			fc.utobj().sendKeys(driver, ADM_form_gen.txtFieldName, fieldNameDropdown);
			// fc.utobj().clickElement(driver, ADM_form_gen.documentopt);
			fc.utobj().selectDropDown(driver, ADM_form_gen.FieldType, "Drop-down");
			fc.utobj().sendKeys(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='row_1']/td[2]/input[1]"),
					"Opt" + fieldNameDropdown);

			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//*[@id='removeTDForAddMore']//*[contains(text(),'Add Option')]"));
			fc.utobj().sendKeys(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='row_2']/td[2]/input[1]"),
					"DropValue1");
			fc.utobj().clickElement(driver, ADM_form_gen.btnSumbitField);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().clickElement(driver, ADM_form_gen.btnAddField);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			String fieldNameRadio = "Radio";
			fieldNameRadio = fc.utobj().generateTestData(fieldNameRadio);
			fc.utobj().sendKeys(driver, ADM_form_gen.txtFieldName, fieldNameRadio);
			// fc.utobj().clickElement(driver, ADM_form_gen.documentopt);
			fc.utobj().selectDropDown(driver, ADM_form_gen.FieldType, "Radio");
			fc.utobj().sendKeys(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='row_1']/td[2]/input[1]"),
					"Radio1");
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//*[@id='removeTDForAddMore']//*[contains(text(),'Add Option')]"));
			fc.utobj().sendKeys(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='row_2']/td[2]/input[1]"),
					"Opt2" + fieldNameRadio);
			fc.utobj().clickElement(driver, ADM_form_gen.btnSumbitField);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().clickElement(driver, ADM_form_gen.btnAddField);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			String fieldNameCheckbox = "Checkbox";
			fieldNameCheckbox = fc.utobj().generateTestData(fieldNameCheckbox);
			fc.utobj().sendKeys(driver, ADM_form_gen.txtFieldName, fieldNameCheckbox);
			// fc.utobj().clickElement(driver, ADM_form_gen.documentopt);
			fc.utobj().selectDropDown(driver, ADM_form_gen.FieldType, "Checkbox");
			fc.utobj().sendKeys(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='row_1']/td[2]/input[1]"),
					"Checkbox1");
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//*[@id='removeTDForAddMore']//*[contains(text(),'Add Option')]"));
			fc.utobj().sendKeys(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='row_2']/td[2]/input[1]"),
					"Opt2");
			fc.utobj().clickElement(driver, ADM_form_gen.btnSumbitField);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().clickElement(driver, ADM_form_gen.btnAddField);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			String fieldNameNumeric = "Numeric";
			fieldNameNumeric = fc.utobj().generateTestData(fieldNameNumeric);
			fc.utobj().sendKeys(driver, ADM_form_gen.txtFieldName, fieldNameNumeric);
			// fc.utobj().clickElement(driver, ADM_form_gen.documentopt);
			fc.utobj().selectDropDown(driver, ADM_form_gen.FieldType, "Numeric");
			fc.utobj().clickElement(driver, ADM_form_gen.btnSumbitField);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().clickElement(driver, ADM_form_gen.btnAddField);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			String fieldNameMultiSelectDropdown = "Multislect";
			fieldNameMultiSelectDropdown = fc.utobj().generateTestData(fieldNameMultiSelectDropdown);
			fc.utobj().sendKeys(driver, ADM_form_gen.txtFieldName, fieldNameMultiSelectDropdown);
			// fc.utobj().clickElement(driver, ADM_form_gen.documentopt);
			fc.utobj().selectDropDown(driver, ADM_form_gen.FieldType, "Multi Select Drop-down");
			fc.utobj().sendKeys(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='row_1']/td[2]/input[1]"),
					"Mul" + fieldNameMultiSelectDropdown);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//*[@id='removeTDForAddMore']//*[contains(text(),'Add Option')]"));
			fc.utobj().sendKeys(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='row_2']/td[2]/input[1]"),
					"Multi1");
			fc.utobj().clickElement(driver, ADM_form_gen.btnSumbitField);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().clickElement(driver, ADM_form_gen.btnAddField);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			String fieldNameDocument = "Doc";
			fieldNameDocument = fc.utobj().generateTestData(fieldNameDocument);
			fc.utobj().sendKeys(driver, ADM_form_gen.txtFieldName, fieldNameDocument);
			// fc.utobj().clickElement(driver, ADM_form_gen.documentopt);
			fc.utobj().selectDropDown(driver, ADM_form_gen.FieldType, "Document");
			fc.utobj().clickElement(driver, ADM_form_gen.btnSumbitField);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Navigate To Admin Users Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			String emailId = "crmautomation@staffex.com";
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUser = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporateUser.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			fc.utobj().printTestStep("Navigate To CRM > Leads > Leads Summary");
			fc.utobj().printTestStep("Add First Lead");
			fc.crm().crm_common().CRMLeadsLnk(driver);
			fc.utobj().clickElement(driver, pobj.addNew);
			fc.utobj().clickElement(driver, pobj.leadLnk);

			testCaseIdInternal = "TC_CRM_QA_Lead_Details";
			dataSetCustom = fc.utobj().readTestData("crm", testCaseIdInternal);
			String firstName = fc.utobj().generateTestData("Aa");
			String lastName = fc.utobj().generateTestData("Bb");
			dataSetCustom.put("leadFirstName", firstName);
			dataSetCustom.put("leadLastName", lastName);
			dataSetCustom.put("cmLeadTypeID", "Select");
			dataSetCustom.put("cmSource2ID", "Internet");
			dataSetCustom.put("cmSource3ID", "BISON");
			dataSetCustom.put("leadOwnerID2", corpUser.getuserFullName());

			printInfo = fillFormDataWithDataset(driver, config, testCaseIdInternal, dataSetCustom);
			// Reporter.log("Map == for ="+testCaseIdInternal+"==="+printInfo);

			if (printInfo != null && printInfo.size() > 0) {
				// System.out.println("printInfo=First="+printInfo);

				// System.out.println("printInfo=First Email
				// ="+printInfo.get("emailIds"));
			}

			fc.utobj().clickElement(driver, pobj.leadsLink);

			systemExactSearch(driver, firstName + " " + lastName);

			fc.utobj().printTestStep("Verify Lead Name in Lead Summary.");
			boolean isLeadNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//*[.='" + firstName + " " + lastName + "']");
			if (isLeadNamePresent == false) {
				fc.utobj().throwsException("was not able to verify First Lead Name");
			}

			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//a[contains(text () ,'" + firstName + " " + lastName + "')]"));

			fieldNameText = fieldNameText.toLowerCase();
			fieldNameTextArea = fieldNameTextArea.toLowerCase();
			fieldNameDate = fieldNameDate.toLowerCase();
			fieldNameDropdown = fieldNameDropdown.toLowerCase();
			fieldNameRadio = fieldNameRadio.toLowerCase();
			fieldNameNumeric = fieldNameNumeric.toLowerCase();
			fieldNameCheckbox = fieldNameCheckbox.toLowerCase();
			fieldNameMultiSelectDropdown = fieldNameMultiSelectDropdown.toLowerCase();
			fieldNameDocument = fieldNameDocument.toLowerCase();

			String fieldNameAction = fieldNameText;
			fieldNameText = fieldNameText.toLowerCase();

			fc.utobj().printTestStep("Navigate to newly added Tab at Lead Info");
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//*[@qat_tabname='" + tabName + "']"));

			Thread.sleep(2000);
			fc.utobj().sendKeys(driver,
					fc.utobj().getElementByXpath(driver, "//input[contains(@name, '_" + fieldNameText + "')]"),
					fieldNameText); // Work
			fc.utobj().sendKeys(driver,
					fc.utobj().getElementByXpath(driver, "//*[contains(@id, '_" + fieldNameTextArea + "')]"),
					fieldNameTextArea); // work
			fc.utobj().sendKeys(driver,
					fc.utobj().getElementByXpath(driver, "//input[contains(@name, '_" + fieldNameDate + "')]"),
					"02/08/2018"); // work
			fc.utobj().selectDropDownByVisibleText(driver,
					fc.utobj().getElementByXpath(driver, "//*[contains(@name, '_" + fieldNameDropdown + "')]"),
					"DropValue1"); // work
			List<WebElement> rdBtn_Field = driver
					.findElements(By.xpath("//*[contains(@name, '_" + fieldNameRadio + "')]")); // work
			fc.utobj().clickRadioButton(driver, rdBtn_Field, "1"); // work
			fc.utobj().sendKeys(driver,
					fc.utobj().getElementByXpath(driver, "//input[contains(@name, '_" + fieldNameNumeric + "')]"),
					"100"); // work
			List<WebElement> checkbox = driver
					.findElements(By.xpath("//*[contains(@name, '_" + fieldNameCheckbox + "')]"));
			((WebElement) checkbox.get(0)).click();
			WebElement element = driver
					.findElement(By.xpath(".//div[contains(@id , '" + fieldNameMultiSelectDropdown + "')]"));
			fc.utobj().setToDefault(driver, element);
			WebElement elementInput = element.findElement(By.xpath("./div/div/input"));
			fc.utobj().selectValFromMultiSelect(driver, element, elementInput, "Multi1");

			String fileName = fc.utobj().getFilePathFromTestData(dataSet.get("fileName")); // "C:\\images.jpg"
			// ;//
			fc.utobj().sendKeys(driver, fc.utobj().getElementByXpath(driver, "//input[@type='file']"), fileName);

			Thread.sleep(1000);
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//*[@type='button' and @name='Submit' and @value='Add']"));

			fc.utobj().printTestStep("Verify The Added Field inb Lead Tab");
			boolean isfieldNameText = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () , '" + fieldNameText + "')]");
			if (isfieldNameText == false) {
				fc.utobj().throwsException("was not able to verify Text Field");
			}
			boolean isfieldNameTextArea = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () , '" + fieldNameTextArea + "')]");
			if (isfieldNameTextArea == false) {
				fc.utobj().throwsException("was not able to verify TextArea Field");
			}
			boolean isfieldNameDate = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//td[contains(text () , '02/08/2018')]");
			if (isfieldNameDate == false) {
				fc.utobj().throwsException("was not able to verify Date Field");
			}
			boolean isfieldNameDropdown = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//td[contains(text () , 'DropValue1')]");
			if (isfieldNameDropdown == false) {
				fc.utobj().throwsException("was not able to verify DropDown Field");
			}

			boolean isfieldNameRadio = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//td[contains(text () , 'Radio1')]");
			if (isfieldNameRadio == false) {
				fc.utobj().throwsException("was not able to verify Radio Field");
			}
			boolean isfieldNameNumeric = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//td[contains(text () , '100')]");
			if (isfieldNameNumeric == false) {
				fc.utobj().throwsException("was not able to verify numric Field");
			}
			boolean isfieldNameCheckbox = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//td[contains(text () , 'Checkbox1')]");
			if (isfieldNameCheckbox == false) {
				fc.utobj().throwsException("was not able to verify checkBox Field");
			}
			boolean isfieldNameMultiSelectDropdown = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () , 'Multi1')]");
			if (isfieldNameMultiSelectDropdown == false) {
				fc.utobj().throwsException("was not able to verify Multi Select Field");
			}
			boolean isfileName = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[contains(text () , '.jpg')]");
			if (isfileName == false) {
				fc.utobj().throwsException("was not able to verify File Type Field");
			}

			fc.utobj().printTestStep("Navigate to Print Tab at Lead tab Info");
			Thread.sleep(1000);
			try {
				fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
						".//*[@type='button' and @name='Button0' and @value='Print']"));
				VerifyPrintPreview(driver, fieldNameText);

			} catch (Exception w) {
				fc.utobj().throwsException("was not able to verify Print functionality.");
			}

			fc.utobj().printTestStep("Verify to Add More at Lead tab Info");
			Thread.sleep(1000);
			try {
				fc.utobj().moveToElement(driver, fc.utobj().getElementByXpath(driver,
						".//*[@type='button' and @name='Button0' and @value='Add More']"));

			} catch (Exception w) {
				fc.utobj().throwsException("was not able to verify Add More button.");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "crm", "TC_Lead_CRM_FormMultiAddMoreGenCase"})
	@TestCase(createdOn = "2018-01-16", updatedOn = "2018-01-16", testCaseId = "TC_Lead_CRM_FormMultiAddMoreGenCase", testCaseDescription = "To Verify custom fields in leads.")
	private void crmInfoFillAreaFormGen_3() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		Map<String, String> dataSetCustom = null;
		String testCaseIdInternal = "" + testCaseId;
		Map<String, String> printInfo = null;
		AdminInfoMgrCommonMethods adminCommonMethods = fc.adminpage().adminInfoMgr();
		Map<String, String> dataSet = fc.utobj().readTestData("crm", "TC_FS_FormGenerator_006");
		try {
			driver = fc.loginpage().login(driver);
			CRMLeadsPage pobj = new CRMLeadsPage(driver);

			fc.utobj().printTestStep("Navigate To Admin > CRM > Manage Form Generator");
			fc.utobj().printTestStep("Add New Tab");

			fc.crm().crm_common().adminCRMManageFormGeneratorLnk(driver);
			fc.adminpage().adminInfoMgr();
			AdminInfoMgrManageFormGeneratorPage manageFormGeneratorPage = new AdminInfoMgrManageFormGeneratorPage(
					driver);
			AdminInfoMgrManageFormGeneratorTabDetailsPage ADM_form_gen = manageFormGeneratorPage.getAddTabDetailsPage();
			adminCommonMethods.NavigateToManageFormGeneratorTabs(driver, "Lead");

			fc.utobj().clickElement(driver, manageFormGeneratorPage.btnAddNewTab);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			AdminInfoMgrManageFormGeneratorAddTabPage objAddNewTabPage = new AdminInfoMgrManageFormGeneratorAddTabPage(
					driver);
			String tabName = fc.utobj().generateTestData("Tab");
			fc.utobj().sendKeys(driver, objAddNewTabPage.txtDisplayName, tabName);
			// fc.utobj().clickElement(driver,
			// objAddNewTabPage.multipleInputNo);
			fc.utobj().clickElement(driver, objAddNewTabPage.btnSubmit);
			fc.utobj().switchFrameToDefault(driver);
			fc.utobj().clickLink(driver, tabName);

			fc.utobj().clickElement(driver, ADM_form_gen.btnAddSection);

			fc.commonMethods().switch_cboxIframe_frameId(driver);
			String SectionName = "Sec";
			SectionName = fc.utobj().generateTestData(SectionName);
			fc.utobj().sendKeys(driver, ADM_form_gen.txtSectionName, SectionName);
			fc.utobj().clickElement(driver, ADM_form_gen.btnAddSectionName);
			fc.utobj().switchFrameToDefault(driver);
			fc.utobj().clickElement(driver, ADM_form_gen.btnAddField);
			fc.commonMethods().switch_cboxIframe_frameId(driver);

			String fieldNameText = "Text";
			fieldNameText = fc.utobj().generateTestData(fieldNameText);
			fc.utobj().sendKeys(driver, ADM_form_gen.txtFieldName, fieldNameText);
			// fc.utobj().clickElement(driver, ADM_form_gen.documentopt);
			fc.utobj().selectDropDown(driver, ADM_form_gen.FieldType, "Text");
			fc.utobj().clickElement(driver, ADM_form_gen.btnSumbitField);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Navigate To Admin Users Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			String emailId = "crmautomation@staffex.com";
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUser = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporateUser.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			fc.utobj().printTestStep("Navigate To CRM > Leads > Leads Summary");
			fc.utobj().printTestStep("Add First Lead");
			fc.crm().crm_common().CRMLeadsLnk(driver);
			fc.utobj().clickElement(driver, pobj.addNew);
			fc.utobj().clickElement(driver, pobj.leadLnk);

			testCaseIdInternal = "TC_CRM_QA_Lead_Details";
			dataSetCustom = fc.utobj().readTestData("crm", testCaseIdInternal);
			String firstName = fc.utobj().generateTestData("Aa");
			String lastName = fc.utobj().generateTestData("Bb");
			dataSetCustom.put("leadFirstName", firstName);
			dataSetCustom.put("leadLastName", lastName);
			dataSetCustom.put("cmLeadTypeID", "Select");
			dataSetCustom.put("cmSource2ID", "Internet");
			dataSetCustom.put("cmSource3ID", "BISON");
			dataSetCustom.put("leadOwnerID2", corpUser.getuserFullName());

			printInfo = fillFormDataWithDataset(driver, config, testCaseIdInternal, dataSetCustom);
			// Reporter.log("Map == for ="+testCaseIdInternal+"==="+printInfo);

			if (printInfo != null && printInfo.size() > 0) {
				// System.out.println("printInfo=First="+printInfo);

				// System.out.println("printInfo=First Email
				// ="+printInfo.get("emailIds"));
			}

			fc.utobj().clickElement(driver, pobj.leadsLink);

			systemExactSearch(driver, firstName + " " + lastName);

			fc.utobj().printTestStep("Verify Lead Name in Lead Summary.");
			boolean isLeadNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//*[.='" + firstName + " " + lastName + "']");
			if (isLeadNamePresent == false) {
				fc.utobj().throwsException("was not able to verify First Lead Name");
			}

			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//a[contains(text () ,'" + firstName + " " + lastName + "')]"));

			fieldNameText = fieldNameText.toLowerCase();

			String fieldNameAction = fieldNameText;
			fieldNameText = fieldNameText.toLowerCase();

			fc.utobj().printTestStep("Navigate to newly added Tab at Lead Info");
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//*[@qat_tabname='" + tabName + "']"));

			Thread.sleep(2000);
			fc.utobj().sendKeys(driver,
					fc.utobj().getElementByXpath(driver, "//input[contains(@name, '_" + fieldNameText + "')]"),
					fieldNameText); // Work

			Thread.sleep(1000);
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//*[@type='button' and @name='Submit' and @value='Add']"));

			fc.utobj().printTestStep("Verify The Added Field inb Lead Tab");
			boolean isfieldNameText = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () , '" + fieldNameText + "')]");
			if (isfieldNameText == false) {
				fc.utobj().throwsException("was not able to verify Text Field");
			}

			fc.utobj().printTestStep("Verify to Add More at Lead tab Info");
			Thread.sleep(1000);
			try {
				fc.utobj().moveToElement(driver, fc.utobj().getElementByXpath(driver,
						".//*[@type='button' and @name='Button0' and @value='Add More']"));
				fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
						".//*[@type='button' and @name='Button0' and @value='Add More']"));
				Thread.sleep(2000);
				fc.utobj().sendKeys(driver,
						fc.utobj().getElementByXpath(driver, "//input[contains(@name, '_" + fieldNameText + "')]"),
						"Harish"); // Work

				Thread.sleep(1000);
				fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
						".//*[@type='button' and @name='Submit' and @value='Add']"));

				fc.utobj().printTestStep("Verify The Added Field inb Lead Tab");
				isfieldNameText = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//td[contains(text () , 'Harish')]");
				if (isfieldNameText == false) {
					fc.utobj().throwsException("was not able to verify Text Field");
				}

			} catch (Exception w) {
				fc.utobj().throwsException("was not able to verify Add More button.");
			}
			try {
				Thread.sleep(1000);
				fc.utobj().clickElement(driver,
						fc.utobj().getElementByXpath(driver, ".//span[contains (text(),'Modify')]"));
				fc.utobj().sendKeys(driver,
						fc.utobj().getElementByXpath(driver, "//input[contains(@name, '_" + fieldNameText + "')]"),
						"HarishModify"); // Work
				Thread.sleep(1000);
				fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
						".//*[@type='button' and @name='Submit' and @value='Save']"));

				fc.utobj().printTestStep("Verify The Modified Field in Lead Tab");
				isfieldNameText = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//td[contains(text () , 'HarishModify')]");
				if (isfieldNameText == false) {
					fc.utobj().throwsException("was not able to verify Modify Text Field");
				}

			} catch (Exception w) {
				fc.utobj().throwsException("was not able to verify Modify after More button.");
			}

			try {
				Thread.sleep(1000);
				fc.utobj().clickElement(driver,
						fc.utobj().getElementByXpath(driver, ".//span[contains (text(),'Delete')]"));
				String alertText = fc.utobj().acceptAlertBox(driver);

				fc.utobj().printTestStep("Verify The Deleted Field in Lead Tab");
				isfieldNameText = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//td[contains(text () , 'HarishModify')]");
				if (isfieldNameText == true) {
					fc.utobj().throwsException("was not able to verify Delete Text Field");
				}

			} catch (Exception w) {
				fc.utobj().throwsException("was not able to verify Delete after More button.");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "crm", "TC_Lead_CRM_FormLogCallGenCase" })
	@TestCase(createdOn = "2018-01-16", updatedOn = "2018-01-16", testCaseId = "TC_Lead_CRM_FormLogCallGenCase", testCaseDescription = "To Verify custom fields in leads.")
	private void crmInfoFillAreaFormGen_4() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		Map<String, String> dataSetCustom = null;
		String testCaseIdInternal = "" + testCaseId;
		Map<String, String> printInfo = null;
		AdminInfoMgrCommonMethods adminCommonMethods = fc.adminpage().adminInfoMgr();
		Map<String, String> dataSet = fc.utobj().readTestData("crm", "TC_FS_FormGenerator_006");
		try {
			driver = fc.loginpage().login(driver);
			CRMLeadsPage pobj = new CRMLeadsPage(driver);

			fc.utobj().printTestStep("Navigate To Admin > CRM > Manage Form Generator");
			fc.utobj().printTestStep("Add New Tab");

			fc.crm().crm_common().adminCRMManageFormGeneratorLnk(driver);
			fc.adminpage().adminInfoMgr();
			AdminInfoMgrManageFormGeneratorPage manageFormGeneratorPage = new AdminInfoMgrManageFormGeneratorPage(
					driver);
			AdminInfoMgrManageFormGeneratorTabDetailsPage ADM_form_gen = manageFormGeneratorPage.getAddTabDetailsPage();
			adminCommonMethods.NavigateToManageFormGeneratorTabs(driver, "Lead");

			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//a[contains (text(),'Call')]"));
			fc.utobj().clickElement(driver, ADM_form_gen.btnAddField);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			String fieldNameText = "Text";
			fieldNameText = fc.utobj().generateTestData(fieldNameText);
			fc.utobj().sendKeys(driver, ADM_form_gen.txtFieldName, fieldNameText);
			// fc.utobj().clickElement(driver, ADM_form_gen.documentopt);
			fc.utobj().selectDropDown(driver, ADM_form_gen.FieldType, "Text");
			fc.utobj().clickElement(driver, ADM_form_gen.btnSumbitField);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Navigate To Admin Users Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			String emailId = "crmautomation@staffex.com";
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUser = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporateUser.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			fc.utobj().printTestStep("Navigate To CRM > Leads > Leads Summary");
			fc.utobj().printTestStep("Add First Lead");
			fc.crm().crm_common().CRMLeadsLnk(driver);
			fc.utobj().clickElement(driver, pobj.addNew);
			fc.utobj().clickElement(driver, pobj.leadLnk);

			testCaseIdInternal = "TC_CRM_QA_Lead_Details";
			dataSetCustom = fc.utobj().readTestData("crm", testCaseIdInternal);
			String firstName = fc.utobj().generateTestData("Aa");
			String lastName = fc.utobj().generateTestData("Bb");
			dataSetCustom.put("leadFirstName", firstName);
			dataSetCustom.put("leadLastName", lastName);
			dataSetCustom.put("cmLeadTypeID", "Select");
			dataSetCustom.put("cmSource2ID", "Internet");
			dataSetCustom.put("cmSource3ID", "BISON");
			dataSetCustom.put("leadOwnerID2", corpUser.getuserFullName());

			printInfo = fillFormDataWithDataset(driver, config, testCaseIdInternal, dataSetCustom);
			// Reporter.log("Map == for ="+testCaseIdInternal+"==="+printInfo);

			if (printInfo != null && printInfo.size() > 0) {
				// System.out.println("printInfo=First="+printInfo);

				// System.out.println("printInfo=First Email
				// ="+printInfo.get("emailIds"));
			}

			fc.utobj().clickElement(driver, pobj.leadsLink);

			systemExactSearch(driver, firstName + " " + lastName);

			fc.utobj().printTestStep("Verify Lead Name in Lead Summary.");
			boolean isLeadNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//*[.='" + firstName + " " + lastName + "']");
			if (isLeadNamePresent == false) {
				fc.utobj().throwsException("was not able to verify First Lead Name");
			}

			// fc.utobj().clickElement(driver,fc.utobj().getElementByXpath(driver,".//a[contains(text
			// () ,'" + firstName + " " + lastName + "')]")));
			fc.utobj().printTestStep("Log A Call");
			fc.utobj().actionImgOption(driver, firstName + " " + lastName, "Log a Call");

			fc.commonMethods().switch_cboxIframe_frameId(driver);
			String subject = fc.utobj().generateTestData("subject");
			fc.utobj().sendKeys(driver, pobj.callSubject, subject);
			String currentDate = fc.utobj().getCurrentDateUSFormat();
			fc.utobj().sendKeys(driver, pobj.callDate, currentDate);
			String comment = fc.utobj().generateTestData("comment");
			fc.utobj().sendKeys(driver, pobj.comments, comment);
			fieldNameText = fieldNameText.toLowerCase();
			fieldNameText = fieldNameText.toLowerCase();
			fc.utobj().sendKeys(driver,
					fc.utobj().getElementByXpath(driver, "//input[contains(@name, '_" + fieldNameText + "')]"),
					fieldNameText); // Work
			fc.utobj().clickElement(driver, pobj.saveBtn);
			fc.utobj().clickElement(driver, pobj.noBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//a[contains(text () , '" + firstName + " " + lastName + "')]"));

			// verify Activity History
			fc.utobj().printTestStep("Verify At Activity History Frame");
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//*[@id='ActivityHis']//a/span[.='" + subject + "']"));
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			// call Details
			fc.utobj().isTextDisplayed(driver, subject, "was not able to verify subject");
			fc.utobj().isTextDisplayed(driver, comment, "was not able to verify comment");
			fc.utobj().isTextDisplayed(driver, fieldNameText, "was not able to verify Form Generator Field");
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "crm"})
	@TestCase(createdOn = "2018-01-16", updatedOn = "2018-01-16", testCaseId = "TC_Lead_CRM_FormTabularGenCase", testCaseDescription = "To Verify custom fields in leads.")
	private void crmInfoFillAreaFormGen_Tabular() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		Map<String, String> dataSetCustom = null;
		String testCaseIdInternal = "" + testCaseId;
		Map<String, String> printInfo = null;
		AdminInfoMgrCommonMethods adminCommonMethods = fc.adminpage().adminInfoMgr();
		Map<String, String> dataSet = fc.utobj().readTestData("crm", "TC_FS_FormGenerator_006");
		try {
			driver = fc.loginpage().login(driver);
			CRMLeadsPage pobj = new CRMLeadsPage(driver);

			fc.utobj().printTestStep("Navigate To Admin > CRM > Manage Form Generator");
			fc.utobj().printTestStep("Add New Tab");

			fc.crm().crm_common().adminCRMManageFormGeneratorLnk(driver);
			fc.adminpage().adminInfoMgr();
			AdminInfoMgrManageFormGeneratorPage manageFormGeneratorPage = new AdminInfoMgrManageFormGeneratorPage(
					driver);
			AdminInfoMgrManageFormGeneratorTabDetailsPage ADM_form_gen = manageFormGeneratorPage.getAddTabDetailsPage();
			adminCommonMethods.NavigateToManageFormGeneratorTabs(driver, "Lead");

			fc.utobj().clickElement(driver, manageFormGeneratorPage.btnAddNewTab);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			AdminInfoMgrManageFormGeneratorAddTabPage objAddNewTabPage = new AdminInfoMgrManageFormGeneratorAddTabPage(
					driver);
			String tabName = fc.utobj().generateTestData("Tab");
			fc.utobj().sendKeys(driver, objAddNewTabPage.txtDisplayName, tabName);
			fc.utobj().clickElement(driver, objAddNewTabPage.multipleInputNo);
			fc.utobj().clickElement(driver, objAddNewTabPage.btnSubmit);
			fc.utobj().switchFrameToDefault(driver);
			fc.utobj().clickLink(driver, tabName);

			fc.utobj().clickElement(driver, ADM_form_gen.btnAddSection);

			fc.commonMethods().switch_cboxIframe_frameId(driver);
			String SectionName = "Sec";
			SectionName = fc.utobj().generateTestData(SectionName);
			fc.utobj().sendKeys(driver, ADM_form_gen.txtSectionName, SectionName);

			if (!fc.utobj().isSelected(driver, ADM_form_gen.tabularSection)) {
				fc.utobj().clickElement(driver, ADM_form_gen.tabularSection);
			}
			fc.utobj().clickElement(driver, ADM_form_gen.btnAddSectionName);
			fc.utobj().switchFrameToDefault(driver);
			fc.utobj().clickElement(driver, ADM_form_gen.btnAddField);
			fc.commonMethods().switch_cboxIframe_frameId(driver);

			String fieldNameTextArea = "TextArea";
			fieldNameTextArea = fc.utobj().generateTestData(fieldNameTextArea);
			fc.utobj().sendKeys(driver, ADM_form_gen.txtFieldName, fieldNameTextArea);
			// fc.utobj().clickElement(driver, ADM_form_gen.documentopt);
			fc.utobj().selectDropDown(driver, ADM_form_gen.FieldType, "Text Area");
			fc.utobj().clickElement(driver, ADM_form_gen.btnSumbitField);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().clickElement(driver, ADM_form_gen.btnAddField);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			String fieldNameText = "Text";
			fieldNameText = fc.utobj().generateTestData(fieldNameText);
			fc.utobj().sendKeys(driver, ADM_form_gen.txtFieldName, fieldNameText);
			// fc.utobj().clickElement(driver, ADM_form_gen.documentopt);
			fc.utobj().selectDropDown(driver, ADM_form_gen.FieldType, "Text");
			fc.utobj().clickElement(driver, ADM_form_gen.btnSumbitField);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().clickElement(driver, ADM_form_gen.btnAddField);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			String fieldNameDate = "Date";
			fieldNameDate = fc.utobj().generateTestData(fieldNameDate);
			fc.utobj().sendKeys(driver, ADM_form_gen.txtFieldName, fieldNameDate);
			// fc.utobj().clickElement(driver, ADM_form_gen.documentopt);
			fc.utobj().selectDropDown(driver, ADM_form_gen.FieldType, "Date");
			fc.utobj().clickElement(driver, ADM_form_gen.btnSumbitField);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().clickElement(driver, ADM_form_gen.btnAddField);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			String fieldNameNumeric = "Numeric";
			fieldNameNumeric = fc.utobj().generateTestData(fieldNameNumeric);
			fc.utobj().sendKeys(driver, ADM_form_gen.txtFieldName, fieldNameNumeric);
			// fc.utobj().clickElement(driver, ADM_form_gen.documentopt);
			fc.utobj().selectDropDown(driver, ADM_form_gen.FieldType, "Numeric");
			fc.utobj().clickElement(driver, ADM_form_gen.btnSumbitField);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Navigate To Admin Users Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			String emailId = "crmautomation@staffex.com";
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUser = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporateUser.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			fc.utobj().printTestStep("Navigate To CRM > Leads > Leads Summary");
			fc.utobj().printTestStep("Add First Lead");
			fc.crm().crm_common().CRMLeadsLnk(driver);
			fc.utobj().clickElement(driver, pobj.addNew);
			fc.utobj().clickElement(driver, pobj.leadLnk);

			testCaseIdInternal = "TC_CRM_QA_Lead_Details";
			dataSetCustom = fc.utobj().readTestData("crm", testCaseIdInternal);
			String firstName = fc.utobj().generateTestData("Aa");
			String lastName = fc.utobj().generateTestData("Bb");
			dataSetCustom.put("leadFirstName", firstName);
			dataSetCustom.put("leadLastName", lastName);
			dataSetCustom.put("cmLeadTypeID", "Select");
			dataSetCustom.put("cmSource2ID", "Internet");
			dataSetCustom.put("cmSource3ID", "BISON");
			dataSetCustom.put("leadOwnerID2", corpUser.getuserFullName());
			printInfo = fillFormDataWithDataset(driver, config, testCaseIdInternal, dataSetCustom);

			fc.utobj().clickElement(driver, pobj.leadsLink);

			systemExactSearch(driver, firstName + " " + lastName);

			fc.utobj().printTestStep("Verify Lead Name in Lead Summary.");
			boolean isLeadNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//*[.='" + firstName + " " + lastName + "']");
			if (isLeadNamePresent == false) {
				fc.utobj().throwsException("was not able to verify First Lead Name");
			}

			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//a[contains(text () ,'" + firstName + " " + lastName + "')]"));

			fieldNameText = fieldNameText.toLowerCase();
			fieldNameTextArea = fieldNameTextArea.toLowerCase();
			fieldNameDate = fieldNameDate.toLowerCase();
			fieldNameNumeric = fieldNameNumeric.toLowerCase();

			fieldNameText = fieldNameText.toLowerCase();

			fc.utobj().printTestStep("Navigate to newly added Tab at Lead Info");
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//*[@qat_tabname='" + tabName + "']"));
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//a[contains (text(),'Add')]"));
			fc.commonMethods().switch_cboxIframe_frameId(driver);

			Thread.sleep(5000);
			fc.utobj().sendKeys(driver,
					fc.utobj().getElementByXpath(driver, "//input[contains(@name, '_" + fieldNameText + "')]"),
					fieldNameText); // Work
			fc.utobj().sendKeys(driver,
					fc.utobj().getElementByXpath(driver, "//*[contains(@id, '_" + fieldNameTextArea + "')]"),
					fieldNameTextArea); // work
			fc.utobj().sendKeys(driver,
					fc.utobj().getElementByXpath(driver, "//input[contains(@name, '_" + fieldNameDate + "')]"),
					"02/08/2018"); // work
			fc.utobj().sendKeys(driver,
					fc.utobj().getElementByXpath(driver, "//input[contains(@name, '_" + fieldNameNumeric + "')]"),
					"100"); // work

			Thread.sleep(1000);

			try {
				fc.utobj().printTestStep("Verify Save / Reset and close button in new tab");
				fc.utobj().moveToElement(driver,
						fc.utobj().getElementByXpath(driver, ".//*[@type='RESET' and @value='Reset']"));
				fc.utobj().moveToElement(driver, fc.utobj().getElementByXpath(driver,
						".//*[@type='button' and @name='Button' and @value='Close']"));

			} catch (Exception e) {
				fc.utobj().throwsException("was not able to verify Save / Reset and close button in new tab");
			}

			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//*[@type='submit' and @name='Submit' and @value='Save']"));
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//*[@type='button' and @name='closeButton' and @value='Close']"));

			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Verify The Added Field inb Lead Tab");
			boolean isfieldNameText = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () , '" + fieldNameText + "')]");
			if (isfieldNameText == false) {
				fc.utobj().throwsException("was not able to verify Text Field");
			}
			boolean isfieldNameTextArea = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () , '" + fieldNameTextArea + "')]");
			if (isfieldNameTextArea == false) {
				fc.utobj().throwsException("was not able to verify TextArea Field");
			}
			boolean isfieldNameDate = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//td[contains(text () , '02/08/2018')]");
			if (isfieldNameDate == false) {
				fc.utobj().throwsException("was not able to verify Date Field");
			}

			fc.utobj().printTestStep("Navigate to Print Tab at Lead tab Info");
			Thread.sleep(1000);
			try {
				fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
						".//*[@type='button' and @name='Button0' and @value='Print']"));
				VerifyPrintPreview(driver, fieldNameText);

			} catch (Exception w) {
				fc.utobj().throwsException("was not able to verify Print functionality.");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "crm"})
	@TestCase(createdOn = "2018-01-16", updatedOn = "2018-01-16", testCaseId = "TC_Lead_CRM_FormTabularDisabledFieldGenCase", testCaseDescription = "To Verify disabled  custom fields in leads.")
	private void crmInfoFillAreaFormGen_Tabular_DisabledField() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		Map<String, String> dataSetCustom = null;
		String testCaseIdInternal = "" + testCaseId;
		Map<String, String> printInfo = null;
		AdminInfoMgrCommonMethods adminCommonMethods = fc.adminpage().adminInfoMgr();
		Map<String, String> dataSet = fc.utobj().readTestData("crm", "TC_FS_FormGenerator_006");
		try {
			driver = fc.loginpage().login(driver);
			CRMLeadsPage pobj = new CRMLeadsPage(driver);

			fc.utobj().printTestStep("Navigate To Admin > CRM > Manage Form Generator");
			fc.utobj().printTestStep("Add New Tab");

			fc.crm().crm_common().adminCRMManageFormGeneratorLnk(driver);
			fc.adminpage().adminInfoMgr();
			AdminInfoMgrManageFormGeneratorPage manageFormGeneratorPage = new AdminInfoMgrManageFormGeneratorPage(
					driver);
			AdminInfoMgrManageFormGeneratorTabDetailsPage ADM_form_gen = manageFormGeneratorPage.getAddTabDetailsPage();
			adminCommonMethods.NavigateToManageFormGeneratorTabs(driver, "Lead");

			fc.utobj().clickElement(driver, manageFormGeneratorPage.btnAddNewTab);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			AdminInfoMgrManageFormGeneratorAddTabPage objAddNewTabPage = new AdminInfoMgrManageFormGeneratorAddTabPage(
					driver);
			String tabName = fc.utobj().generateTestData("Tab");
			fc.utobj().sendKeys(driver, objAddNewTabPage.txtDisplayName, tabName);
			fc.utobj().clickElement(driver, objAddNewTabPage.multipleInputNo);
			fc.utobj().clickElement(driver, objAddNewTabPage.btnSubmit);
			fc.utobj().switchFrameToDefault(driver);
			fc.utobj().clickLink(driver, tabName);

			fc.utobj().clickElement(driver, ADM_form_gen.btnAddSection);

			fc.commonMethods().switch_cboxIframe_frameId(driver);
			String SectionName = "Sec";
			SectionName = fc.utobj().generateTestData(SectionName);
			fc.utobj().sendKeys(driver, ADM_form_gen.txtSectionName, SectionName);

			if (!fc.utobj().isSelected(driver, ADM_form_gen.tabularSection)) {
				fc.utobj().clickElement(driver, ADM_form_gen.tabularSection);
			}
			fc.utobj().clickElement(driver, ADM_form_gen.btnAddSectionName);
			fc.utobj().switchFrameToDefault(driver);
			fc.utobj().clickElement(driver, ADM_form_gen.btnAddField);
			fc.commonMethods().switch_cboxIframe_frameId(driver);

			String fieldNameTextArea = "TextArea";
			fieldNameTextArea = fc.utobj().generateTestData(fieldNameTextArea);
			fc.utobj().sendKeys(driver, ADM_form_gen.txtFieldName, fieldNameTextArea);
			// fc.utobj().clickElement(driver, ADM_form_gen.documentopt);
			fc.utobj().selectDropDown(driver, ADM_form_gen.FieldType, "Text Area");
			fc.utobj().clickElement(driver, ADM_form_gen.btnSumbitField);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Click to De-Activate this Field");
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//img[@title='Click to De-Activate this Field']"));

			fc.utobj().clickElement(driver, ADM_form_gen.btnAddField);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			String fieldNameText = "Text";
			fieldNameText = fc.utobj().generateTestData(fieldNameText);
			fc.utobj().sendKeys(driver, ADM_form_gen.txtFieldName, fieldNameText);
			// fc.utobj().clickElement(driver, ADM_form_gen.documentopt);
			fc.utobj().selectDropDown(driver, ADM_form_gen.FieldType, "Text");
			fc.utobj().clickElement(driver, ADM_form_gen.btnSumbitField);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().clickElement(driver, ADM_form_gen.btnAddField);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			String fieldNameDate = "Date";
			fieldNameDate = fc.utobj().generateTestData(fieldNameDate);
			fc.utobj().sendKeys(driver, ADM_form_gen.txtFieldName, fieldNameDate);
			// fc.utobj().clickElement(driver, ADM_form_gen.documentopt);
			fc.utobj().selectDropDown(driver, ADM_form_gen.FieldType, "Date");
			fc.utobj().clickElement(driver, ADM_form_gen.btnSumbitField);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().clickElement(driver, ADM_form_gen.btnAddField);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			String fieldNameNumeric = "Numeric";
			fieldNameNumeric = fc.utobj().generateTestData(fieldNameNumeric);
			fc.utobj().sendKeys(driver, ADM_form_gen.txtFieldName, fieldNameNumeric);
			// fc.utobj().clickElement(driver, ADM_form_gen.documentopt);
			fc.utobj().selectDropDown(driver, ADM_form_gen.FieldType, "Numeric");
			fc.utobj().clickElement(driver, ADM_form_gen.btnSumbitField);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Navigate To Admin Users Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			String emailId = "crmautomation@staffex.com";
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUser = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporateUser.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			fc.utobj().printTestStep("Navigate To CRM > Leads > Leads Summary");
			fc.utobj().printTestStep("Add First Lead");
			fc.crm().crm_common().CRMLeadsLnk(driver);
			fc.utobj().clickElement(driver, pobj.addNew);
			fc.utobj().clickElement(driver, pobj.leadLnk);

			testCaseIdInternal = "TC_CRM_QA_Lead_Details";
			dataSetCustom = fc.utobj().readTestData("crm", testCaseIdInternal);
			String firstName = fc.utobj().generateTestData("Aa");
			String lastName = fc.utobj().generateTestData("Bb");
			dataSetCustom.put("leadFirstName", firstName);
			dataSetCustom.put("leadLastName", lastName);
			dataSetCustom.put("cmLeadTypeID", "Select");
			dataSetCustom.put("cmSource2ID", "Internet");
			dataSetCustom.put("cmSource3ID", "BISON");
			dataSetCustom.put("leadOwnerID2", corpUser.getuserFullName());
			printInfo = fillFormDataWithDataset(driver, config, testCaseIdInternal, dataSetCustom);

			fc.utobj().clickElement(driver, pobj.leadsLink);

			systemExactSearch(driver, firstName + " " + lastName);

			fc.utobj().printTestStep("Verify Lead Name in Lead Summary.");
			boolean isLeadNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//*[.='" + firstName + " " + lastName + "']");
			if (isLeadNamePresent == false) {
				fc.utobj().throwsException("was not able to verify First Lead Name");
			}

			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//a[contains(text () ,'" + firstName + " " + lastName + "')]"));

			fieldNameText = fieldNameText.toLowerCase();
			fieldNameTextArea = fieldNameTextArea.toLowerCase();
			fieldNameDate = fieldNameDate.toLowerCase();
			fieldNameNumeric = fieldNameNumeric.toLowerCase();

			fieldNameText = fieldNameText.toLowerCase();

			fc.utobj().printTestStep("Navigate to newly added Tab at Lead Info");
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//*[@qat_tabname='" + tabName + "']"));
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//a[contains (text(),'Add')]"));
			fc.commonMethods().switch_cboxIframe_frameId(driver);

			Thread.sleep(5000);
			fc.utobj().sendKeys(driver,
					fc.utobj().getElementByXpath(driver, "//input[contains(@name, '_" + fieldNameText + "')]"),
					fieldNameText); // Work
			// fc.utobj().sendKeys(driver,fc.utobj().getElementByXpath(driver,"//*[contains(@id,
			// '_"+fieldNameTextArea+"')]")),fieldNameTextArea); // work
			fc.utobj().sendKeys(driver,
					fc.utobj().getElementByXpath(driver, "//input[contains(@name, '_" + fieldNameDate + "')]"),
					"02/08/2018"); // work
			fc.utobj().sendKeys(driver,
					fc.utobj().getElementByXpath(driver, "//input[contains(@name, '_" + fieldNameNumeric + "')]"),
					"100"); // work

			Thread.sleep(1000);

			try {
				fc.utobj().printTestStep("Verify Save / Reset and close button in new tab");
				fc.utobj().moveToElement(driver,
						fc.utobj().getElementByXpath(driver, ".//*[@type='RESET' and @value='Reset']"));
				fc.utobj().moveToElement(driver, fc.utobj().getElementByXpath(driver,
						".//*[@type='button' and @name='Button' and @value='Close']"));

			} catch (Exception e) {
				fc.utobj().throwsException("was not able to verify Save / Reset and close button in new tab");
			}

			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//*[@type='submit' and @name='Submit' and @value='Save']"));
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//*[@type='button' and @name='closeButton' and @value='Close']"));

			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Verify The Added Field inb Lead Tab");
			boolean isfieldNameText = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () , '" + fieldNameText + "')]");
			if (isfieldNameText == false) {
				fc.utobj().throwsException("was not able to verify Text Field");
			}
			boolean isfieldNameTextArea = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () , '" + fieldNameTextArea + "')]");
			if (isfieldNameTextArea == true) {
				fc.utobj().throwsException("was not able to verify TextArea Field");
			}
			boolean isfieldNameDate = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//td[contains(text () , '02/08/2018')]");
			if (isfieldNameDate == false) {
				fc.utobj().throwsException("was not able to verify Date Field");
			}

			fc.utobj().printTestStep("Navigate to Print Tab at Lead tab Info");
			Thread.sleep(1000);
			try {
				fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
						".//*[@type='button' and @name='Button0' and @value='Print']"));
				VerifyPrintPreview(driver, fieldNameText);

			} catch (Exception w) {
				fc.utobj().throwsException("was not able to verify Print functionality.");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "crm"})
	@TestCase(createdOn = "2018-01-16", updatedOn = "2018-01-16", testCaseId = "TC_Lead_CRM_FormTabularValdationFieldGenCase", testCaseDescription = "To Verify Validation  custom fields in leads.")
	private void crmInfoFillAreaFormGen_Tabular_ValdationField() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());
		

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		Map<String, String> dataSetCustom = null;
		String testCaseIdInternal = "" + testCaseId;
		Map<String, String> printInfo = null;
		AdminInfoMgrCommonMethods adminCommonMethods = fc.adminpage().adminInfoMgr();
		Map<String, String> dataSet = fc.utobj().readTestData("crm", "TC_FS_FormGenerator_006");
		try {
			driver = fc.loginpage().login(driver);
			CRMLeadsPage pobj = new CRMLeadsPage(driver);

			fc.utobj().printTestStep("Navigate To Admin > CRM > Manage Form Generator");
			fc.utobj().printTestStep("Add New Tab");

			fc.crm().crm_common().adminCRMManageFormGeneratorLnk(driver);
			fc.adminpage().adminInfoMgr();
			AdminInfoMgrManageFormGeneratorPage manageFormGeneratorPage = new AdminInfoMgrManageFormGeneratorPage(
					driver);
			AdminInfoMgrManageFormGeneratorTabDetailsPage ADM_form_gen = manageFormGeneratorPage.getAddTabDetailsPage();
			adminCommonMethods.NavigateToManageFormGeneratorTabs(driver, "Lead");

			fc.utobj().clickElement(driver, manageFormGeneratorPage.btnAddNewTab);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			AdminInfoMgrManageFormGeneratorAddTabPage objAddNewTabPage = new AdminInfoMgrManageFormGeneratorAddTabPage(
					driver);
			String tabName = fc.utobj().generateTestData("Tab");
			fc.utobj().sendKeys(driver, objAddNewTabPage.txtDisplayName, tabName);
			fc.utobj().clickElement(driver, objAddNewTabPage.multipleInputNo);
			fc.utobj().clickElement(driver, objAddNewTabPage.btnSubmit);
			fc.utobj().switchFrameToDefault(driver);
			fc.utobj().clickLink(driver, tabName);

			fc.utobj().clickElement(driver, ADM_form_gen.btnAddSection);

			fc.commonMethods().switch_cboxIframe_frameId(driver);
			String SectionName = "Sec";
			SectionName = fc.utobj().generateTestData(SectionName);
			fc.utobj().sendKeys(driver, ADM_form_gen.txtSectionName, SectionName);

			if (!fc.utobj().isSelected(driver, ADM_form_gen.tabularSection)) {
				fc.utobj().clickElement(driver, ADM_form_gen.tabularSection);
			}
			fc.utobj().clickElement(driver, ADM_form_gen.btnAddSectionName);
			fc.utobj().switchFrameToDefault(driver);
			fc.utobj().clickElement(driver, ADM_form_gen.btnAddField);
			fc.commonMethods().switch_cboxIframe_frameId(driver);

			String fieldNameTextArea = "TextArea";
			fieldNameTextArea = fc.utobj().generateTestData(fieldNameTextArea);
			fc.utobj().sendKeys(driver, ADM_form_gen.txtFieldName, fieldNameTextArea);
			// fc.utobj().clickElement(driver, ADM_form_gen.documentopt);
			fc.utobj().selectDropDown(driver, ADM_form_gen.FieldType, "Text Area");
			fc.utobj().clickElement(driver, ADM_form_gen.btnSumbitField);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Click to make Mandatory");
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//img[@title='Click to make Mandatory']"));

			fc.utobj().clickElement(driver, ADM_form_gen.btnAddField);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			String fieldNameText = "Text";
			fieldNameText = fc.utobj().generateTestData(fieldNameText);
			fc.utobj().sendKeys(driver, ADM_form_gen.txtFieldName, fieldNameText);
			// fc.utobj().clickElement(driver, ADM_form_gen.documentopt);
			fc.utobj().selectDropDown(driver, ADM_form_gen.FieldType, "Text");
			fc.utobj().clickElement(driver, ADM_form_gen.btnSumbitField);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().clickElement(driver, ADM_form_gen.btnAddField);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			String fieldNameDate = "Date";
			fieldNameDate = fc.utobj().generateTestData(fieldNameDate);
			fc.utobj().sendKeys(driver, ADM_form_gen.txtFieldName, fieldNameDate);
			// fc.utobj().clickElement(driver, ADM_form_gen.documentopt);
			fc.utobj().selectDropDown(driver, ADM_form_gen.FieldType, "Date");
			fc.utobj().clickElement(driver, ADM_form_gen.btnSumbitField);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().clickElement(driver, ADM_form_gen.btnAddField);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			String fieldNameNumeric = "Numeric";
			fieldNameNumeric = fc.utobj().generateTestData(fieldNameNumeric);
			fc.utobj().sendKeys(driver, ADM_form_gen.txtFieldName, fieldNameNumeric);
			// fc.utobj().clickElement(driver, ADM_form_gen.documentopt);
			fc.utobj().selectDropDown(driver, ADM_form_gen.FieldType, "Numeric");
			fc.utobj().clickElement(driver, ADM_form_gen.btnSumbitField);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Navigate To Admin Users Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			String emailId = "crmautomation@staffex.com";
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUser = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporateUser.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			fc.utobj().printTestStep("Navigate To CRM > Leads > Leads Summary");
			fc.utobj().printTestStep("Add First Lead");
			fc.crm().crm_common().CRMLeadsLnk(driver);
			fc.utobj().clickElement(driver, pobj.addNew);
			fc.utobj().clickElement(driver, pobj.leadLnk);

			testCaseIdInternal = "TC_CRM_QA_Lead_Details";
			dataSetCustom = fc.utobj().readTestData("crm", testCaseIdInternal);
			String firstName = fc.utobj().generateTestData("Aa");
			String lastName = fc.utobj().generateTestData("Bb");
			dataSetCustom.put("leadFirstName", firstName);
			dataSetCustom.put("leadLastName", lastName);
			dataSetCustom.put("cmLeadTypeID", "Select");
			dataSetCustom.put("cmSource2ID", "Internet");
			dataSetCustom.put("cmSource3ID", "BISON");
			dataSetCustom.put("leadOwnerID2", corpUser.getuserFullName());
			printInfo = fillFormDataWithDataset(driver, config, testCaseIdInternal, dataSetCustom);

			fc.utobj().clickElement(driver, pobj.leadsLink);

			systemExactSearch(driver, firstName + " " + lastName);

			fc.utobj().printTestStep("Verify Lead Name in Lead Summary.");
			boolean isLeadNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//*[.='" + firstName + " " + lastName + "']");
			if (isLeadNamePresent == false) {
				fc.utobj().throwsException("was not able to verify First Lead Name");
			}

			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//a[contains(text () ,'" + firstName + " " + lastName + "')]"));

			fieldNameText = fieldNameText.toLowerCase();
			fieldNameTextArea = fieldNameTextArea.toLowerCase();
			fieldNameDate = fieldNameDate.toLowerCase();
			fieldNameNumeric = fieldNameNumeric.toLowerCase();

			fieldNameText = fieldNameText.toLowerCase();

			fc.utobj().printTestStep("Navigate to newly added Tab at Lead Info");
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//*[@qat_tabname='" + tabName + "']"));
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//a[contains (text(),'Add')]"));
			fc.commonMethods().switch_cboxIframe_frameId(driver);

			Thread.sleep(5000);
			fc.utobj().sendKeys(driver,
					fc.utobj().getElementByXpath(driver, "//input[contains(@name, '_" + fieldNameText + "')]"),
					fieldNameText); // Work
			// fc.utobj().sendKeys(driver,fc.utobj().getElementByXpath(driver,"//*[contains(@id,
			// '_"+fieldNameTextArea+"')]")),fieldNameTextArea); // work
			fc.utobj().sendKeys(driver,
					fc.utobj().getElementByXpath(driver, "//input[contains(@name, '_" + fieldNameDate + "')]"),
					"02/08/2018"); // work
			fc.utobj().sendKeys(driver,
					fc.utobj().getElementByXpath(driver, "//input[contains(@name, '_" + fieldNameNumeric + "')]"),
					"XYZ"); // work

			try {
				String alertText = fc.utobj().acceptAlertBox(driver);
				fc.utobj().sendKeys(driver,
						fc.utobj().getElementByXpath(driver, "//input[contains(@name, '_" + fieldNameNumeric + "')]"),
						"100"); // work
				
				fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
						".//*[@type='submit' and @name='Submit' and @value='Save']"));
				

			} catch (Exception e) {

			}
			/*try {
				//FieldNameTextArea is not a mandatory field and there is no allert to handle this. 
				String alertText = fc.utobj().acceptAlertBox(driver);
				fc.utobj().sendKeys(driver,
						fc.utobj().getElementByXpath(driver, "//*[contains(@id, '_" + fieldNameTextArea + "')]"),
						fieldNameTextArea);
				fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
						".//*[@type='submit' and @name='Submit' and @value='Save']"));

			} catch (Exception e) {
				fc.utobj().throwsException("was not able to verify Validation Field");
			}*/

			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//*[@type='button' and @name='closeButton' and @value='Close']"));

			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Verify The Added Field inb Lead Tab");
			boolean isfieldNameText = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () , '" + fieldNameText + "')]");
			if (isfieldNameText == false) {
				fc.utobj().throwsException("was not able to verify Text Field");
			}
			boolean isfieldNameTextArea = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () , '" + fieldNameTextArea + "')]");
			if (isfieldNameTextArea == true) {
				fc.utobj().throwsException("was not able to verify TextArea Field");
			}
			boolean isfieldNameDate = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//td[contains(text () , '02/08/2018')]");
			if (isfieldNameDate == false) {
				fc.utobj().throwsException("was not able to verify Date Field");
			}

			fc.utobj().printTestStep("Navigate to Print Tab at Lead tab Info");
			Thread.sleep(1000);
			try {
				fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
						".//*[@type='button' and @name='Button0' and @value='Print']"));
				VerifyPrintPreview(driver, fieldNameText);

			} catch (Exception w) {
				fc.utobj().throwsException("was not able to verify Print functionality.");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	public void VerifyPrintPreview(WebDriver driver, String textTobeVerified) throws Exception {
		Set<String> windows = new HashSet<String>();
		windows = driver.getWindowHandles();

		boolean isTextDisplayed = false;

		String currentWindow = driver.getWindowHandle();
		for (String window : windows) {
			if (!window.equals(currentWindow)) {

				driver.switchTo().window(window);
				if (driver.getCurrentUrl().contains("bPrint")) {
					isTextDisplayed = fc.utobj().assertPageSource(driver, textTobeVerified);
				}
			}
		}
		driver.close();

		if (isTextDisplayed) {
			Reporter.log(textTobeVerified + "  is displayed on the print preview page. !!! ");
		} else {
			fc.utobj().throwsException(textTobeVerified + "  is not displayed on the print preview page. !!! ");
		}

		driver.switchTo().window(currentWindow);
	}

	public void addLead(WebDriver driver, Map<String, String> dataSet, String firstName, String lastName,
			String company, String assignTo, String city, String address, String suffix, String jobTitle,
			String comment, String userName, String regionName, String regionalUser, String franchiseId,
			String franchiseUser, String divisionName, Map<String, String> config) throws Exception {

		String testCaseId = "TC_Add_Lead_At_Lead_Page";

		if (fc.utobj().validate(testCaseId)) {
			try {
				CRMLeadsPage pobj = new CRMLeadsPage(driver);
				fc.crm().crm_common().CRMLeadsLnk(driver);
				fc.utobj().clickElement(driver, pobj.addNew);
				fc.utobj().clickElement(driver, pobj.leadLnk);
				fc.utobj().selectDropDown(driver, pobj.title, "Mr.");
				fc.utobj().sendKeys(driver, pobj.leadFirstName, firstName);
				fc.utobj().sendKeys(driver, pobj.leadLastName, lastName);
				fc.utobj().sendKeys(driver, pobj.companyName, company);
				fc.utobj().selectDropDown(driver, pobj.primaryContactMethodSelect, "Email");

				if (assignTo.equalsIgnoreCase("Corporate")) {
					if (!fc.utobj().isSelected(driver, pobj.assignToCorporate)) {
						fc.utobj().clickElement(driver, pobj.assignToCorporate);
					}
					fc.utobj().selectDropDown(driver, pobj.selectCorporateUser, userName);
				} else if (assignTo.equalsIgnoreCase("Regional")) {
					if (!fc.utobj().isSelected(driver, pobj.assignToRegional)) {
						fc.utobj().clickElement(driver, pobj.assignToRegional);
					}
					fc.utobj().selectDropDown(driver, pobj.selectAreaRegion, regionName);
					;
					fc.utobj().selectDropDown(driver, pobj.selectRegionalUser, regionalUser);

				} else if (assignTo.equalsIgnoreCase("Franchise")) {
					if (!fc.utobj().isSelected(driver, pobj.assignToFranchise)) {
						fc.utobj().clickElement(driver, pobj.assignToFranchise);
					}
					fc.utobj().selectDropDown(driver, pobj.selectFranchiseId, franchiseId);
					fc.utobj().selectDropDown(driver, pobj.selectFranchiseUser, franchiseUser);
				} else if (assignTo.equalsIgnoreCase("Only Franchise")) {
					fc.utobj().selectDropDown(driver, pobj.selectFranchiseUser, franchiseUser);

				} else if (assignTo.equalsIgnoreCase("Division")) {
					if (!fc.utobj().isSelected(driver, pobj.assignToDivision)) {
						fc.utobj().clickElement(driver, pobj.assignToDivision);
					}
					fc.utobj().selectDropDown(driver, pobj.leadDivision, divisionName);
					fc.utobj().selectDropDown(driver, pobj.leadDivisionUser, userName);
				}

				fc.utobj().sendKeys(driver, pobj.address, address);
				fc.utobj().sendKeys(driver, pobj.city, city);
				fc.utobj().selectDropDown(driver, pobj.country, "USA");
				fc.utobj().selectDropDown(driver, pobj.state, "Alabama");
				fc.utobj().sendKeys(driver, pobj.zipcode, "12345");
				fc.utobj().sendKeys(driver, pobj.phoneNumbers, "1236547896");
				fc.utobj().sendKeys(driver, pobj.extn, "12");
				fc.utobj().sendKeys(driver, pobj.faxNumbers, "1263985487");
				fc.utobj().sendKeys(driver, pobj.mobileNumbers, "8896724441");
				String email = "crmautomation@staffex.com";
				fc.utobj().sendKeys(driver, pobj.emailIds, email);
				fc.utobj().sendKeys(driver, pobj.alternateEmail, "testautoamtion@franqa.com");
				fc.utobj().sendKeys(driver, pobj.suffix, suffix);
				fc.utobj().sendKeys(driver, pobj.position, jobTitle);
				fc.utobj().sendKeys(driver, pobj.birthdate, "11/24/1980");
				fc.utobj().sendKeys(driver, pobj.anniversarydate, "11/24/1999");
				fc.utobj().selectDropDown(driver, pobj.rating, "Hot");
				fc.utobj().sendKeys(driver, pobj.bestTimeToContact, "12:00 PM EST");
				fc.utobj().sendKeys(driver, pobj.comments, comment);
				fc.utobj().clickElement(driver, pobj.saveBtn);

				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException("Was not able to Add Lead :: CRM > Leads");
		}
	}

	public void addLead(WebDriver driver, Map<String, String> dataSet, String firstName, String lastName,
			String company, String assignTo, String city, String address, String suffix, String jobTitle,
			String comment, String userName, String regionName, String regionalUser, String franchiseId,
			String franchiseUser, Map<String, String> config) throws Exception {

		String testCaseId = "TC_Add_Lead_At_Lead_Page";

		if (fc.utobj().validate(testCaseId)) {
			try {
				CRMLeadsPage pobj = new CRMLeadsPage(driver);
				fc.crm().crm_common().CRMLeadsLnk(driver);
				fc.utobj().clickElement(driver, pobj.addNew);
				fc.utobj().clickElement(driver, pobj.leadLnk);
				fc.utobj().selectDropDown(driver, pobj.title, "Mr.");
				fc.utobj().sendKeys(driver, pobj.leadFirstName, firstName);
				fc.utobj().sendKeys(driver, pobj.leadLastName, lastName);
				fc.utobj().sendKeys(driver, pobj.companyName, company);
				fc.utobj().selectDropDown(driver, pobj.primaryContactMethodSelect, "Email");

				if (assignTo.equalsIgnoreCase("Corporate")) {
					if (!fc.utobj().isSelected(driver, pobj.assignToCorporate)) {
						fc.utobj().clickElement(driver, pobj.assignToCorporate);
					}
					fc.utobj().selectDropDown(driver, pobj.selectCorporateUser, userName);
				} else if (assignTo.equalsIgnoreCase("Regional")) {
					if (!fc.utobj().isSelected(driver, pobj.assignToRegional)) {
						fc.utobj().clickElement(driver, pobj.assignToRegional);
					}
					fc.utobj().selectDropDown(driver, pobj.selectAreaRegion, regionName);
					;
					fc.utobj().selectDropDown(driver, pobj.selectRegionalUser, regionalUser);

				} else if (assignTo.equalsIgnoreCase("Franchise")) {
					if (!fc.utobj().isSelected(driver, pobj.assignToFranchise)) {
						fc.utobj().clickElement(driver, pobj.assignToFranchise);
					}
					fc.utobj().selectDropDown(driver, pobj.selectFranchiseId, franchiseId);
					fc.utobj().selectDropDown(driver, pobj.selectFranchiseUser, franchiseUser);
				} else if (assignTo.equalsIgnoreCase("Only Franchise")) {
					fc.utobj().selectDropDown(driver, pobj.selectFranchiseUser, franchiseUser);

				}
				fc.utobj().sendKeys(driver, pobj.address, address);
				fc.utobj().sendKeys(driver, pobj.city, city);
				fc.utobj().selectDropDown(driver, pobj.country, "USA");
				fc.utobj().selectDropDown(driver, pobj.state, "Alabama");
				fc.utobj().sendKeys(driver, pobj.zipcode, "12345");
				fc.utobj().sendKeys(driver, pobj.phoneNumbers, "1236547896");
				fc.utobj().sendKeys(driver, pobj.extn, "12");
				fc.utobj().sendKeys(driver, pobj.faxNumbers, "1263985487");
				fc.utobj().sendKeys(driver, pobj.mobileNumbers, "8896724441");
				String email = "crmautomation@staffex.com";
				fc.utobj().sendKeys(driver, pobj.emailIds, email);
				fc.utobj().sendKeys(driver, pobj.alternateEmail, "testautoamtion@franqa.com");
				fc.utobj().sendKeys(driver, pobj.suffix, suffix);
				fc.utobj().sendKeys(driver, pobj.position, jobTitle);
				fc.utobj().sendKeys(driver, pobj.birthdate, "11/24/1980");
				fc.utobj().sendKeys(driver, pobj.anniversarydate, "11/24/1999");
				fc.utobj().selectDropDown(driver, pobj.rating, "Hot");
				fc.utobj().sendKeys(driver, pobj.bestTimeToContact, "12:00 PM EST");
				fc.utobj().sendKeys(driver, pobj.comments, comment);
				fc.utobj().clickElement(driver, pobj.saveBtn);

				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException("Was not able to Add Lead :: CRM > Leads");
		}
	}

	public void searchLeadByOwner(WebDriver driver, String userName, String leadTypeView) throws Exception {

		CRMLeadsPage pobj = new CRMLeadsPage(driver);
		fc.utobj().clickElement(driver, pobj.showFilter);
		fc.utobj().setToDefault(driver, pobj.leadOwnerSelect);
		try {
			WebElement divsion = fc.utobj().getElementByXpath(driver, ".//*[@id='ms-parentdivisionID']");
			fc.utobj().setToDefault(driver, divsion);
		} catch (Exception e) {
		}
		fc.utobj()
				.selectValFromMultiSelect(driver, pobj.leadOwnerSelect, fc.utobj()
						.getElement(driver, pobj.leadOwnerSelect).findElement(By.xpath("./div/div/input")),
						userName);
		fc.utobj().setToDefault(driver, pobj.franchiseSelect);
		fc.utobj().setToDefault(driver, pobj.statusSelect);
		fc.utobj().selectDropDown(driver, pobj.addDateSelect, "All");
		fc.utobj().setToDefault(driver, pobj.emailStatusSelect);
		fc.utobj().selectDropDown(driver, pobj.viewLeadsSelect, leadTypeView);
		fc.utobj().clickElement(driver, pobj.searchBtn);
		;
		fc.utobj().clickElement(driver, pobj.hideFilter);
	}

	public void searchLeadByOwnerDiffrent(WebDriver driver, String userName, String leadTypeView, List<String> listInfo)
			throws Exception {

		CRMLeadsPage pobj = new CRMLeadsPage(driver);
		fc.utobj().clickElement(driver, pobj.showFilter);
		fc.utobj().setToDefault(driver, pobj.leadOwnerSelect);
		try {
			WebElement divsion = fc.utobj().getElementByXpath(driver, ".//*[@id='ms-parentdivisionID']");
			fc.utobj().setToDefault(driver, divsion);
		} catch (Exception e) {
		}
		fc.utobj().selectMultipleValFromMultiSelect(driver,
				fc.utobj().getElementByXpath(driver, ".//*[@id='ms-parentleadOwner']"), listInfo);
		// fc.utobj().selectValFromMultiSelect(driver,
		// pobj.leadOwnerSelect,fc.utobj().getElement(driver,
		// pobj.leadOwnerSelect).findElement(By.xpath("./div/div/input")),
		// userName);
		fc.utobj().setToDefault(driver, pobj.franchiseSelect);
		fc.utobj().setToDefault(driver, pobj.statusSelect);
		fc.utobj().selectDropDown(driver, pobj.addDateSelect, "All");
		fc.utobj().setToDefault(driver, pobj.emailStatusSelect);
		fc.utobj().selectDropDown(driver, pobj.viewLeadsSelect, leadTypeView);
		fc.utobj().clickElement(driver, pobj.searchBtn);
		;
		fc.utobj().clickElement(driver, pobj.hideFilter);
	}

	// system search
	public void systemExactSearch(WebDriver driver, String leadName) throws Exception {
		// search By System Search Button
		CRMLeadsPage pobj = new CRMLeadsPage(driver);
		fc.utobj().sendKeys(driver, pobj.searchLeads, leadName);
		// fc.utobj().clickElement(driver, pobj.exactSearch);
		fc.utobj().clickElement(driver, pobj.systemSearchButton);
	}

	public Map<String, String> fillFormDataWithDataset(WebDriver driver, Map<String, String> config, String testCaseId,
			Map<String, String> dataSet) throws Exception {
		Reporter.log("Filling TestCase Data = " + testCaseId);
		String id = "id";

		List<String> listItems = new ArrayList<String>();
		Map<String, String> printInfo = new HashMap<String, String>();

		List<WebElement> mandateBox = null;
		mandateBox = driver.findElements(By.xpath(
				".//td[@qat_tableid='addData']//*[@type='text' or @class='fTextBoxDate' or @class='fTextBox' or @class='multiList' or @type='radio' or @type='checkbox' or @type='checkBox' or @id='ms-parentsellerCombo' or @class='ms-choice' or @class='form-control']"));

		Map<String, String> alreadyitrateElement = new HashMap<>();
		Map<String, String> alreadyradio = new HashMap<>();
		Map<String, String> alreadycheckBox = new HashMap<>();

		try {
			for (WebElement webElement : mandateBox) {

				if (webElement.getAttribute("" + id) != null && !"".equals(webElement.getAttribute("" + id))
						&& !"null".equals(webElement.getAttribute("" + id))
						&& !":".equals(webElement.getAttribute("" + id))
						&& !"dateOfOpen".equals(webElement.getAttribute("" + id))
						&& !"fimCbOtherComplaintType".equals(webElement.getAttribute("" + id))) {
					/*
					 * try{
					 * System.out.println("Type==="+webElement.getAttribute(
					 * "type")+"====values==="+webElement.getAttribute("value")
					 * +"======"+dataSet.get(""+webElement.getAttribute(""+id))
					 * +"====="+""+webElement.getAttribute(""+id));
					 * }catch(Exception e) {
					 * 
					 * }
					 */

					if (alreadyitrateElement.containsKey(webElement.getAttribute("" + id))) {

					} else {
						if (dataSet.containsKey(webElement.getAttribute("" + id))) {
							WebElement elementmovepostion = null;
							if ("name".equals(id)) {
								elementmovepostion = driver.findElement(By.name("" + webElement.getAttribute("" + id)));

							} else {
								elementmovepostion = fc.utobj().getElementByID(driver,
										"" + webElement.getAttribute("" + id));

							}
							fc.utobj().moveToElement(driver, elementmovepostion);
							if (webElement.getAttribute("type").indexOf("select-one") != -1) {
								if (webElement.getAttribute("" + id) != null
										&& !"cmSource2ID".equals(webElement.getAttribute("" + id))
										&& !"cmSource3ID".equals(webElement.getAttribute("" + id)))

								{
									try {

										Select singledrop = null;
										List<WebElement> singleDropDown = null;
										try {
											singledrop = new Select(fc.utobj().getElementByID(driver,
													"" + webElement.getAttribute("" + id)));
											singleDropDown = driver
													.findElements(By.id("" + webElement.getAttribute("" + id)));
										} catch (Exception e) {
											singledrop = new Select(
													driver.findElement(By.name("" + webElement.getAttribute("" + id))));
											singleDropDown = driver
													.findElements(By.name("" + webElement.getAttribute("" + id)));
										}
										if (webElement.getAttribute("value") == null
												|| "".equals(webElement.getAttribute("value"))
												|| "null".equals(webElement.getAttribute("value"))
												|| "-1".equals(webElement.getAttribute("value"))) {

											int size = singleDropDown.size();
											if (dataSet.get("" + webElement.getAttribute("" + id)) != null
													&& !"".equals(dataSet.get("" + webElement.getAttribute("" + id)))) {
												if (size >= 1) {
													try {
														WebElement dropDownField = driver
																.findElement(By.id("" + webElement.getAttribute("id")));
														// fc.utobj().moveToElement(driver,
														// dropDownField);
														fc.utobj().selectDropDownByVisibleText(driver, dropDownField,
																dataSet.get("" + webElement.getAttribute("" + id)));

														if (!"birthMonth".equals(webElement.getAttribute("" + id))
																&& !"birthDate".equals(webElement.getAttribute("" + id))
																&& !"spouseBirthMonth"
																		.equals(webElement.getAttribute("" + id))
																&& !"spouseBirthDate"
																		.equals(webElement.getAttribute("" + id))) {
															listItems.add(
																	dataSet.get("" + webElement.getAttribute("" + id)));
														}
														printInfo.put("" + webElement.getAttribute("" + id),
																dataSet.get("" + webElement.getAttribute("" + id)));

													} catch (Exception eradio) {
														// Reporter.log("Problem
														// in selecting drop :
														// "+""+webElement.getAttribute("id")
														// +" Values getting
														// from Excel not match
														// in desired
														// fomrat."+""+dataSet.get(""+webElement.getAttribute("id")));
													}
												}
											}
										}

									} catch (Exception e3) {

									}
								}

							} else if (webElement.getAttribute("type").indexOf("select-multiple") != -1) {
								if (webElement.getAttribute("value") == null
										|| "".equals(webElement.getAttribute("value"))
										|| "null".equals(webElement.getAttribute("value"))
										|| "-1".equals(webElement.getAttribute("value"))) {
									try {
										WebElement element = driver
												.findElement(By.id("ms-parent" + webElement.getAttribute("" + id)));
										fc.utobj().clickElement(driver, element);
										fc.utobj().clickElement(driver,
												fc.utobj().getElementByXpath(driver, ".//*[@id='selectAll']"));
										fc.utobj().clickElement(driver, element);
									} catch (Exception emultipal) {
										// Reporter.log("Problem in multiple
										// value drop-down :
										// "+""+webElement.getAttribute("id"));
									}
								}
							} else if (webElement.getAttribute("type").indexOf("textarea") != -1) {
								if (webElement.getAttribute("value") == null
										|| "".equals(webElement.getAttribute("value"))
										|| "null".equals(webElement.getAttribute("value"))) {
									try {
										driver.findElement(By.name("" + webElement.getAttribute("" + id)))
												.sendKeys(dataSet.get("" + webElement.getAttribute("" + id)));
										listItems.add(dataSet.get("" + webElement.getAttribute("" + id)));
										printInfo.put("" + webElement.getAttribute("id"),
												dataSet.get("" + webElement.getAttribute("" + id)));
									} catch (Exception eText) {
										// Reporter.log("Problem in entering
										// TextArea in :
										// "+""+webElement.getAttribute("id"));
									}
								}
							} else if (webElement.getAttribute("type").indexOf("radio") != -1) {
								try {
									String sValue = "";
									List<WebElement> rdBtn_Sex = driver
											.findElements(By.id("" + webElement.getAttribute("" + id))); //
									if (alreadyradio.containsKey(webElement.getAttribute("" + id))) {

									} else {

										int size = rdBtn_Sex.size();
										if (size > 1) {
											sValue = dataSet.get("" + webElement.getAttribute("" + id));
										}
										try {
											List<WebElement> rdBtn_Field = null;
											WebElement element = null;
											try {
												rdBtn_Field = driver.findElements(By.xpath(
														".//*[@name='" + webElement.getAttribute("" + id) + "']")); // work
												element = fc.utobj().getElementByXpath(driver,
														".//*[@name='" + webElement.getAttribute("" + id) + "']");
											} catch (Exception eTxt) {
												rdBtn_Field = driver.findElements(
														By.xpath(".//*[@id='" + webElement.getAttribute("id") + "']")); // work
												element = driver.findElement(
														By.xpath(".//*[@id='" + webElement.getAttribute("id") + "']"));

											}

											fc.utobj().moveToElement(driver, element);
											fc.utobj().clickRadioButton(driver, rdBtn_Field, sValue); // work
											listItems.add(dataSet.get("" + webElement.getAttribute("" + id)));
											printInfo.put("" + webElement.getAttribute("id"),
													dataSet.get("" + webElement.getAttribute("" + id)));
											alreadyradio.put(webElement.getAttribute("" + id),
													webElement.getAttribute("" + id));
										} catch (Exception eText) {
											// Reporter.log("Problem in
											// selecting radio value in :
											// "+""+webElement.getAttribute("id"));
										}
									}
								} catch (Exception exp) {
									Reporter.log(exp.toString());
									exp.printStackTrace();
								}

							} else if (webElement.getAttribute("type").toLowerCase().indexOf("checkbox") != -1) {
								List<WebElement> oCheckBox = driver
										.findElements(By.name("" + webElement.getAttribute("" + id)));
								String sValue = "";
								if (alreadycheckBox.containsKey(webElement.getAttribute("" + id))) {

								} else {

									int size = oCheckBox.size();
									for (int i = 0; i < size; i++) {
										sValue = oCheckBox.get(i).getAttribute("value");
										// Thread.sleep(2000);
										if (i == (size - 1)) {

											// break;
										}

									}
									try {
										List<WebElement> rdBtn_Field = driver.findElements(By.xpath(
												"//*[contains(@id, '" + webElement.getAttribute("" + id) + "')]")); // work
										WebElement element = fc.utobj().getElementByXpath(driver,
												"//*[contains(@id, '" + webElement.getAttribute("" + id) + "')]");
										fc.utobj().moveToElement(driver, element);
										fc.utobj().clickRadioButton(driver, rdBtn_Field, sValue); // work
										alreadycheckBox.put(webElement.getAttribute("" + id),
												webElement.getAttribute("" + id));
									} catch (Exception eText) {

									}
								}
							} else if (webElement.getAttribute("type").indexOf("file") != -1) {
								String fileName = fc.utobj().getFilePathFromTestData("pictureFile");
								try {
									fc.utobj().sendKeys(driver,
											fc.utobj().getElementByID(driver, "" + webElement.getAttribute("" + id)),
											fileName);
									listItems.add(fileName);
									printInfo.put("" + webElement.getAttribute("" + id), fileName);
								} catch (Exception eFile) {
									// Reporter.log("Problem in uploading file
									// type value in :
									// "+""+webElement.getAttribute("id"));
								}
							} else if (webElement.getAttribute("type").indexOf("text") != -1) {
								if (webElement.getAttribute("value") == null
										|| "".equals(webElement.getAttribute("value"))
										|| "null".equals(webElement.getAttribute("value"))
										|| "0.00".equals(webElement.getAttribute("value"))) {
									if (webElement.getAttribute("class").indexOf("fTextBoxDate") != -1) {
										try {
											if (dataSet.get("" + webElement.getAttribute("" + id)) != null
													|| !"".equals(dataSet.get("" + webElement.getAttribute("" + id)))
													|| !"null".equals(
															dataSet.get("" + webElement.getAttribute("" + id)))) {
												fc.utobj().sendKeys(driver,
														driver.findElement(
																By.name("" + webElement.getAttribute("" + id))),
														dataSet.get("" + webElement.getAttribute("" + id))); // work
												listItems.add(dataSet.get("" + webElement.getAttribute("" + id)));
												printInfo.put("" + webElement.getAttribute("" + id),
														dataSet.get("" + webElement.getAttribute("" + id)));

											} else {

											}
										} catch (Exception eDate) {
											// Reporter.log("Problem in Entering
											// Date fields value in :
											// "+""+webElement.getAttribute("id"));
										}
									} else {
										try {
											if ("0.00".equals(webElement.getAttribute("value"))) {
												fc.utobj()
														.sendKeys(driver,
																driver.findElement(
																		By.name("" + webElement.getAttribute("" + id))),
																"");

												// System.out.println(webElement.getAttribute("value")
												// +"======"+dataSet.get(""+webElement.getAttribute(""+id))
												// +"====="+""+webElement.getAttribute(""+id));

											}
											if ("name".equals(id)) {
												fc.utobj().sendKeys(driver,
														driver.findElement(
																By.name("" + webElement.getAttribute("" + id))),
														dataSet.get("" + webElement.getAttribute("" + id)));
												if ("accountName".equals("" + webElement.getAttribute("" + id))
														|| "opportunityOwner"
																.equals("" + webElement.getAttribute("" + id))
														|| "oppContactID"
																.equals("" + webElement.getAttribute("" + id))) {
													;
													fc.utobj().clickElement(driver, driver.findElement(
															By.xpath(".//*[@id='customizedAjaxSearch']/div")));
													fc.utobj().sleep();
												}
											} else {
												fc.utobj().sendKeys(driver,
														driver.findElement(
																By.id("" + webElement.getAttribute("" + id))),
														dataSet.get("" + webElement.getAttribute("" + id)));
												if ("accountName".equals("" + webElement.getAttribute("" + id))
														|| "opportunityOwner"
																.equals("" + webElement.getAttribute("" + id))
														|| "oppContactID"
																.equals("" + webElement.getAttribute("" + id))) {
													;
													fc.utobj().clickElement(driver, driver.findElement(
															By.xpath(".//*[@id='customizedAjaxSearch']/div")));
													fc.utobj().sleep();
												}
											}

											if (!"ssn".equals("" + webElement.getAttribute("" + id))
													&& !"spouseSsn".equals("" + webElement.getAttribute("" + id))) {
												listItems.add(dataSet.get("" + webElement.getAttribute("" + id)));
											}
											printInfo.put("" + webElement.getAttribute("" + id),
													dataSet.get("" + webElement.getAttribute("" + id)));

										} catch (Exception eText) {
											// Reporter.log("Problem in Entering
											// Text fileds value in :
											// "+""+webElement.getAttribute("id"));
										}

									}
								}
							}
							alreadyitrateElement.put(webElement.getAttribute("" + id),
									webElement.getAttribute("" + id));
						}
					}
				}
			}

			if ("TC_CRM_QA_Lead_Details".equals(testCaseId)) {

				Select singledrop = null;
				List<WebElement> singleDropDown = null;
				try {
					singledrop = new Select(driver.findElement(By.name("leadOwnerID2")));
					singleDropDown = driver.findElements(By.name("leadOwnerID2"));
					WebElement dropDownField = driver.findElement(By.name("leadOwnerID2"));
					fc.utobj().selectDropDownByVisibleText(driver, dropDownField, dataSet.get("leadOwnerID2"));

					dropDownField = driver.findElement(By.name("cmSource2ID"));
					fc.utobj().selectDropDownByVisibleText(driver, dropDownField, dataSet.get("cmSource2ID"));
					;
					dropDownField = driver.findElement(By.name("cmSource3ID"));
					fc.utobj().selectDropDownByVisibleText(driver, dropDownField, dataSet.get("cmSource3ID"));

				} catch (Exception e) {

				}

			}
			if ("TC_CRM_QA_Contact_Details".equals(testCaseId)) {

				Select singledrop = null;
				List<WebElement> singleDropDown = null;
				try {
					singledrop = new Select(driver.findElement(By.name("contactOwnerID2")));
					singleDropDown = driver.findElements(By.name("contactOwnerID2"));
					WebElement dropDownField = driver.findElement(By.name("contactOwnerID2"));
					fc.utobj().selectDropDownByVisibleText(driver, dropDownField, dataSet.get("contactOwnerID2"));

					dropDownField = driver.findElement(By.name("cmSource2ID"));
					fc.utobj().selectDropDownByVisibleText(driver, dropDownField, dataSet.get("cmSource2ID"));
					;
					dropDownField = driver.findElement(By.name("cmSource3ID"));
					fc.utobj().selectDropDownByVisibleText(driver, dropDownField, dataSet.get("cmSource3ID"));

				} catch (Exception e) {

				}

			}

		} catch (Exception e) {
			// fc.utobj().throwsException("Fields data are not available!
			// "+e.getMessage());
		}
		try {
			// fc.utobj().printTestStep(testCaseId, "Submiting the Tab ");
			WebElement elementbutton = fc.utobj().getElementByXpath(driver,
					".//*[@id='Submit' or @name='Submit' or @name='button' or @name='Submit1' or @name='add1'] ");
			fc.utobj().clickElement(driver, elementbutton);

		} catch (Exception ee) {
			fc.utobj().printBugStatus("Unable to submit page " + ee);
		}

		return printInfo;
	}

}