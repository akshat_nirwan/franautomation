package com.builds.test.crm;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.xml.parsers.ParserConfigurationException;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.Reporter;
import org.testng.annotations.Test;
import org.xml.sax.SAXException;

import com.builds.test.admin.AdminFranchiseLocationAddFranchiseLocationPageTest;
import com.builds.test.admin.AdminUsersManageCorporateUsersAddCorporateUserPageTest;
import com.builds.test.admin.AdminUsersManageDivisionalUsersAddDivisionalUsersPageTest;
import com.builds.test.admin.AdminUsersManageManageFranchiseUsersPageTest;
import com.builds.test.admin.AdminUsersManageRegionalUsersAddRegionalUserPageTest;
import com.builds.test.common.CorporateUser;
import com.builds.test.infomgr.AdminInfoMgrCommonMethods;
import com.builds.uimaps.crm.AdminCRMConfigureLeadTypePage;
import com.builds.uimaps.crm.AdminCRMConfigureStatusPageUI;
import com.builds.uimaps.crm.AdminCRMManageWebFormGeneratorPage;
import com.builds.uimaps.crm.CRMContactsPage;
import com.builds.uimaps.crm.CRMLeadsPage;
import com.builds.uimaps.fs.SearchUI;
import com.builds.uimaps.infomgr.AdminInfoMgrManageFormGeneratorAddTabPage;
import com.builds.uimaps.infomgr.AdminInfoMgrManageFormGeneratorPage;
import com.builds.uimaps.infomgr.AdminInfoMgrManageFormGeneratorTabDetailsPage;
import com.builds.utilities.FranconnectUtil;
import com.builds.utilities.TestCase;

public class CRMLeadsPageTest {
	FranconnectUtil fc = new FranconnectUtil();

	@Test(groups = { "crm","testadminnewgui"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Add Lead At CRM > Lead > Lead Summary", testCaseId = "TC_58_Add_Lead")
	public void addLead() throws IOException, Exception, ParserConfigurationException, SAXException {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			CRMLeadsPage pobj = new CRMLeadsPage(driver);
			SearchUI search_page = new SearchUI(driver);
			fc.utobj().printTestStep("Navigate To Admin Users Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			String emailId = "crmautomation@staffex.com";
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUser = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporateUser.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			fc.utobj().printTestStep("Navigate To CRM > Leads > Leads Summary");
			fc.utobj().printTestStep("Add Lead");
			fc.crm().crm_common().CRMLeadsLnk(driver);

			fc.utobj().clickElement(driver, pobj.addNew);
			fc.utobj().clickElement(driver, pobj.leadLnk);
			fc.utobj().selectDropDown(driver, pobj.title, dataSet.get("title"));
			String firstName = fc.utobj().generateTestData(dataSet.get("firstName"));
			fc.utobj().sendKeys(driver, pobj.leadFirstName, firstName);
			String lastName = fc.utobj().generateTestData(dataSet.get("lastName"));
			fc.utobj().sendKeys(driver, pobj.leadLastName, lastName);
			String company = fc.utobj().generateTestData(dataSet.get("company"));
			fc.utobj().sendKeys(driver, pobj.companyName, company);
			fc.utobj().selectDropDown(driver, pobj.primaryContactMethodSelect, dataSet.get("primaryContactMethod"));
			if (!fc.utobj().isSelected(driver, pobj.assignToCorporate)) {
				fc.utobj().clickElement(driver, pobj.assignToCorporate);
			}
			fc.utobj().selectDropDown(driver, pobj.selectCorporateUser, corpUser.getuserFullName());
			String address = fc.utobj().generateTestData(dataSet.get("address"));
			fc.utobj().sendKeys(driver, pobj.address, address);
			String city = fc.utobj().generateTestData(dataSet.get("city"));
			fc.utobj().sendKeys(driver, pobj.city, city);
			fc.utobj().selectDropDown(driver, pobj.country, "USA");
			fc.utobj().selectDropDown(driver, pobj.state, "Alabama");
			fc.utobj().sendKeys(driver, pobj.zipcode, "12345");
			fc.utobj().sendKeys(driver, pobj.phoneNumbers, "1236547896");
			fc.utobj().sendKeys(driver, pobj.extn, "12");
			fc.utobj().sendKeys(driver, pobj.faxNumbers, "1263985487");
			fc.utobj().sendKeys(driver, pobj.mobileNumbers, "8896724441");
			//String email = dataSet.get("emailId");
			String email=fc.utobj().generateTestData("crmautomation")+"@staffex.com";
			fc.utobj().sendKeys(driver, pobj.emailIds, email);
			fc.utobj().sendKeys(driver, pobj.alternateEmail, "test@gmail.com");
			String suffix = fc.utobj().generateTestData(dataSet.get("suffix"));
			fc.utobj().sendKeys(driver, pobj.suffix, suffix);
			String jobTitle = fc.utobj().generateTestData(dataSet.get("jobTitle"));
			fc.utobj().sendKeys(driver, pobj.position, jobTitle);
			fc.utobj().sendKeys(driver, pobj.birthdate, "11/24/1980");
			fc.utobj().sendKeys(driver, pobj.anniversarydate, "11/24/1999");
			fc.utobj().selectDropDown(driver, pobj.rating, dataSet.get("rating"));
			fc.utobj().sendKeys(driver, pobj.bestTimeToContact, "12:00 PM EST");
			String comment = fc.utobj().generateTestData(dataSet.get("comment"));
			fc.utobj().sendKeys(driver, pobj.comments, comment);
			fc.utobj().clickElement(driver, pobj.saveBtn);

			fc.utobj().printTestStep("Verify The Added Lead By Top Search");

			String leadName = firstName + " " + lastName;

			boolean isSearchTrue = false;
			for (int i = 0; i < 3; i++) {
				if (isSearchTrue == false) {
					fc.utobj().sendKeys(driver, search_page.topSearchField, leadName);
					Thread.sleep(2000);
					if (i == 2) {
						Thread.sleep(2000);
					}
					fc.utobj().clickEnterOnElement(driver, search_page.topSearchField);

					try {
						isSearchTrue = fc.utobj().isElementPresent(
								driver.findElements(By.xpath(".//custom[contains(text () , '" + leadName + "')]")));
					} catch (Exception e) {
						Reporter.log(e.getMessage());
						System.out.println(e.getMessage());
					}
				}
			}

			if (isSearchTrue == false) {
				fc.utobj().throwsException("Not able to search Lead By Top Search");
			}

			fc.utobj().refresh(driver);

			// verify at Home Page
			fc.utobj().printTestStep("Verify Lead At Lead Home Page");
			fc.utobj().clickElement(driver, pobj.leadsLink);
			searchLeadByOwner(driver, corpUser.getuserFullName(), "Open Leads");

			boolean verifyName = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[.='" + firstName + " " + lastName + "']");
			if (verifyName == false) {
				fc.utobj().throwsException("was not able to verify Name");
			}
			boolean ownerOfLead = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[.='" + firstName + " " + lastName
					+ "']/ancestor::tr/td[contains(text () , '" + corpUser.getuserFullName() + "')]");
			if (ownerOfLead == false) {
				fc.utobj().throwsException("was not able to verify owner of the lead");
			}
			boolean statusOfLead = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[.='" + firstName + " " + lastName + "']/ancestor::tr/td[contains(text () , 'New')]");
			if (statusOfLead == false) {
				fc.utobj().throwsException("was not able to verify staus of the lead");
			}
			boolean emailOfLead = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[.='" + firstName + " " + lastName
					+ "']/ancestor::tr/td/a[contains(text () , '" + email + "')]");
			if (emailOfLead == false) {
				fc.utobj().throwsException("was not able to verify email of the lead");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	public void addLead(WebDriver driver, Map<String, String> dataSet, String firstName, String lastName,
			String company, String assignTo, String city, String address, String suffix, String jobTitle,
			String comment, String userName, String regionName, String regionalUser, String franchiseId,
			String franchiseUser, Map<String, String> config) throws Exception {

		String testCaseId = "TC_Add_Lead_At_Lead_Page";

		if (fc.utobj().validate(testCaseId)) {
			try {
				CRMLeadsPage pobj = new CRMLeadsPage(driver);
				fc.crm().crm_common().CRMLeadsLnk(driver);
				fc.utobj().clickElement(driver, pobj.addNew);
				fc.utobj().clickElement(driver, pobj.leadLnk);
				fc.utobj().selectDropDown(driver, pobj.title, "Mr.");
				fc.utobj().sendKeys(driver, pobj.leadFirstName, firstName);
				fc.utobj().sendKeys(driver, pobj.leadLastName, lastName);
				fc.utobj().sendKeys(driver, pobj.companyName, company);
				fc.utobj().selectDropDown(driver, pobj.primaryContactMethodSelect, "Email");

				if (assignTo.equalsIgnoreCase("Corporate")) {
					if (!fc.utobj().isSelected(driver, pobj.assignToCorporate)) {
						fc.utobj().clickElement(driver, pobj.assignToCorporate);
					}
					fc.utobj().selectDropDown(driver, pobj.selectCorporateUser, userName);
				} else if (assignTo.equalsIgnoreCase("Regional")) {
					if (!fc.utobj().isSelected(driver, pobj.assignToRegional)) {
						fc.utobj().clickElement(driver, pobj.assignToRegional);
					}
					fc.utobj().selectDropDown(driver, pobj.selectAreaRegion, regionName);

					fc.utobj().selectDropDown(driver, pobj.selectRegionalUser, regionalUser);

				} else if (assignTo.equalsIgnoreCase("Franchise")) {
					if (!fc.utobj().isSelected(driver, pobj.assignToFranchise)) {
						fc.utobj().clickElement(driver, pobj.assignToFranchise);
					}
					fc.utobj().selectDropDown(driver, pobj.selectFranchiseId, franchiseId);
					fc.utobj().selectDropDown(driver, pobj.selectFranchiseUser, franchiseUser);
				} else if (assignTo.equalsIgnoreCase("Only Franchise")) {
					fc.utobj().selectDropDown(driver, pobj.selectFranchiseUser, franchiseUser);

				}
				fc.utobj().sendKeys(driver, pobj.address, address);
				fc.utobj().sendKeys(driver, pobj.city, city);
				fc.utobj().selectDropDown(driver, pobj.country, "USA");
				fc.utobj().selectDropDown(driver, pobj.state, "Alabama");
				fc.utobj().sendKeys(driver, pobj.zipcode, "12345");
				fc.utobj().sendKeys(driver, pobj.phoneNumbers, "1236547896");
				fc.utobj().sendKeys(driver, pobj.extn, "12");
				fc.utobj().sendKeys(driver, pobj.faxNumbers, "1263985487");
				fc.utobj().sendKeys(driver, pobj.mobileNumbers, "8896724441");
				String email=fc.utobj().generateTestData("custom")+"@gmail.com";
				fc.utobj().sendKeys(driver, pobj.emailIds, email);
				fc.utobj().sendKeys(driver, pobj.alternateEmail, "testautoamtion@franqa.com");
				fc.utobj().sendKeys(driver, pobj.suffix, suffix);
				fc.utobj().sendKeys(driver, pobj.position, jobTitle);
				fc.utobj().sendKeys(driver, pobj.birthdate, "11/24/1980");
				fc.utobj().sendKeys(driver, pobj.anniversarydate, "11/24/1999");
				fc.utobj().selectDropDown(driver, pobj.rating, "Hot");
				fc.utobj().sendKeys(driver, pobj.bestTimeToContact, "12:00 PM EST");
				fc.utobj().sendKeys(driver, pobj.comments, comment);
				fc.utobj().clickElement(driver, pobj.saveBtn);

				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException("Was not able to Add Lead :: CRM > Leads");
		}
	}
	
	//VipinG
	public void addLeadwithSqlite(WebDriver driver, Map<String, String> dataSet, String firstName, String lastName,
			String company, String assignTo, String city, String address, String suffix, String jobTitle,
			String comment, String userName, String regionName, String regionalUser, String franchiseId,
			String franchiseUser, Map<String, String> config) throws Exception {

		String testCaseId = "Task_Creation_CorporateUser";

		if (fc.utobj().validate(testCaseId)) {
			try {
				CRMLeadsPage pobj = new CRMLeadsPage(driver);
				fc.crm().crm_common().CRMLeadsLnk(driver);
				fc.utobj().clickElement(driver, pobj.addNew);
				fc.utobj().clickElement(driver, pobj.leadLnk);
				fc.utobj().selectDropDown(driver, pobj.title, dataSet.get("title"));
				fc.utobj().sendKeys(driver, pobj.leadFirstName, firstName);
				fc.utobj().sendKeys(driver, pobj.leadLastName, lastName);
				fc.utobj().sendKeys(driver, pobj.companyName, company);
				fc.utobj().selectDropDown(driver, pobj.primaryContactMethodSelect, dataSet.get("primaryContactMethod"));

				if (assignTo.equalsIgnoreCase("Corporate")) {
					if (!fc.utobj().isSelected(driver, pobj.assignToCorporate)) {
						fc.utobj().clickElement(driver, pobj.assignToCorporate);
					}
					fc.utobj().selectDropDown(driver, pobj.selectCorporateUser, userName);
				} else if (assignTo.equalsIgnoreCase("Regional")) {
					if (!fc.utobj().isSelected(driver, pobj.assignToRegional)) {
						fc.utobj().clickElement(driver, pobj.assignToRegional);
					}
					//Commenting bcus we have login with Regional uname and regPassword
					/*fc.utobj().selectDropDown(driver, pobj.selectAreaRegion, regionName)*/;

					fc.utobj().selectDropDown(driver, pobj.selectRegionalUser, regionalUser);

				} else if (assignTo.equalsIgnoreCase("Franchise")) {
					//Commenting because login with Franchise user
					/*if (!fc.utobj().isSelected(driver, pobj.assignToFranchise)) {
						fc.utobj().clickElement(driver, pobj.assignToFranchise);
					}*/
					/*fc.utobj().selectDropDown(driver, pobj.selectFranchiseId, franchiseId);*/
					fc.utobj().selectDropDown(driver, pobj.selectFranchiseUser, franchiseUser);
				} else if (assignTo.equalsIgnoreCase("Only Franchise")) {
					fc.utobj().selectDropDown(driver, pobj.selectFranchiseUser, franchiseUser);

				}
				fc.utobj().sendKeys(driver, pobj.address, address);
				fc.utobj().sendKeys(driver, pobj.city, city);
				fc.utobj().selectDropDown(driver, pobj.country,dataSet.get("country"));
				fc.utobj().selectDropDown(driver, pobj.state,dataSet.get("state"));
				fc.utobj().sendKeys(driver, pobj.zipcode,dataSet.get("zipcode"));
				fc.utobj().sendKeys(driver, pobj.phoneNumbers, dataSet.get("phoneNumbers"));
				fc.utobj().sendKeys(driver, pobj.extn, dataSet.get("extn"));
				fc.utobj().sendKeys(driver, pobj.faxNumbers,dataSet.get("faxNumbers"));
				fc.utobj().sendKeys(driver, pobj.mobileNumbers, dataSet.get("mobileNumbers"));
				String email=fc.utobj().generateTestData(dataSet.get("custom"))+"@gmail.com";
				fc.utobj().sendKeys(driver, pobj.emailIds, email);
				fc.utobj().sendKeys(driver, pobj.alternateEmail, dataSet.get("alternateEmail"));
				fc.utobj().sendKeys(driver, pobj.suffix, suffix);
				fc.utobj().sendKeys(driver, pobj.position, jobTitle);
				fc.utobj().sendKeys(driver, pobj.birthdate, dataSet.get("birthdate"));
				fc.utobj().sendKeys(driver, pobj.anniversarydate, dataSet.get("anniversarydate"));
				fc.utobj().selectDropDown(driver, pobj.rating, dataSet.get("rating"));
				fc.utobj().sendKeys(driver, pobj.bestTimeToContact,dataSet.get("bestTimeToContact"));
				fc.utobj().sendKeys(driver, pobj.comments, comment);
				fc.utobj().clickElement(driver, pobj.saveBtn);

				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsException("Was not able to Add Lead :: CRM > Leads");
		}
	}

	@Test(groups = { "crm", "TC_59_Modify_Lead" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify the Modify Lead By Action Image At CRM > Lead > Lead Summary", testCaseId = "TC_59_Modify_Lead")
	public void modifyLeadActionImage() throws IOException, Exception, ParserConfigurationException, SAXException {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			CRMLeadsPage pobj = new CRMLeadsPage(driver);

			fc.utobj().printTestStep("Navigate To Admin Users Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			String emailId = "crmautomation@staffex.com";
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUser = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporateUser.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			fc.utobj().printTestStep("Navigating to Admin > CRM > Configure Status");
			fc.utobj().printTestStep("Add Status Lead Type");

			AdminCRMConfigureStatusPageTest statusPage = new AdminCRMConfigureStatusPageTest();
			String statusName = fc.utobj().generateTestData(dataSet.get("statusName"));
			statusPage.addStatus(driver, statusName);

			fc.utobj().printTestStep("Navigate To CRM > Leads > Leads Summary");
			fc.utobj().printTestStep("Add Lead");

			String firstName = fc.utobj().generateTestData(dataSet.get("firstName"));
			String lastName = fc.utobj().generateTestData(dataSet.get("lastName"));
			String company = fc.utobj().generateTestData(dataSet.get("company"));
			String assignTo = "Corporate";
			String city = fc.utobj().generateTestData(dataSet.get("city"));
			//String email = dataSet.get("emailId");
			String email=fc.utobj().generateTestData("crmautomation")+"@staffex.com";
			String address = fc.utobj().generateTestData(dataSet.get("address"));
			String suffix = fc.utobj().generateTestData(dataSet.get("suffix"));
			String jobTitle = fc.utobj().generateTestData(dataSet.get("jobTitle"));
			String comment = fc.utobj().generateTestData(dataSet.get("comment"));
			String regionName = "";
			String regionalUser = "";
			String franchiseId = "";
			String franchiseUser = "";

			addLead(driver, dataSet, firstName, lastName, company, assignTo, city, address, suffix, jobTitle, comment,
					corpUser.getuserFullName(), regionName, regionalUser, franchiseId, franchiseUser, config);

			fc.utobj().clickElement(driver, pobj.leadsLink);
			searchLeadByOwner(driver, corpUser.getuserFullName(), "Open Leads");

			fc.utobj().printTestStep("Modify Lead");

			fc.utobj().actionImgOption(driver, firstName + " " + lastName, "Modify Details");

			fc.utobj().selectDropDown(driver, pobj.title, dataSet.get("title"));
			fc.utobj().selectDropDown(driver, pobj.leadStatus, statusName);
			firstName = fc.utobj().generateTestData(dataSet.get("firstName"));
			fc.utobj().sendKeys(driver, pobj.leadFirstName, firstName);
			lastName = fc.utobj().generateTestData(dataSet.get("lastName"));
			fc.utobj().sendKeys(driver, pobj.leadLastName, lastName);
			company = fc.utobj().generateTestData(dataSet.get("company"));
			fc.utobj().sendKeys(driver, pobj.companyName, company);
			fc.utobj().selectDropDown(driver, pobj.primaryContactMethodSelect, dataSet.get("primaryContactMethod"));
			address = fc.utobj().generateTestData(dataSet.get("address"));
			fc.utobj().sendKeys(driver, pobj.address, address);
			city = fc.utobj().generateTestData(dataSet.get("city"));
			fc.utobj().sendKeys(driver, pobj.city, city);
			fc.utobj().selectDropDown(driver, pobj.country, "USA");
			fc.utobj().selectDropDown(driver, pobj.state, "Alabama");
			fc.utobj().sendKeys(driver, pobj.zipcode, "12345");
			fc.utobj().sendKeys(driver, pobj.phoneNumbers, "1236547896");
			fc.utobj().sendKeys(driver, pobj.extn, "12");
			fc.utobj().sendKeys(driver, pobj.faxNumbers, "1263985487");
			fc.utobj().sendKeys(driver, pobj.mobileNumbers, "8896724441");
			//email = dataSet.get("emailId");
			email=fc.utobj().generateTestData("crmautomation")+"@staffex.com";
			fc.utobj().sendKeys(driver, pobj.emailIds, email);
			fc.utobj().sendKeys(driver, pobj.alternateEmail, "test@gmail.com");
			fc.utobj().generateTestData(dataSet.get("suffix"));
			fc.utobj().sendKeys(driver, pobj.suffix, suffix);
			fc.utobj().generateTestData(dataSet.get("jobTitle"));
			fc.utobj().sendKeys(driver, pobj.position, jobTitle);
			fc.utobj().sendKeys(driver, pobj.birthdate, "11/24/1980");
			fc.utobj().sendKeys(driver, pobj.anniversarydate, "11/24/1999");

			fc.utobj().selectDropDown(driver, pobj.rating, dataSet.get("rating"));

			fc.utobj().sendKeys(driver, pobj.bestTimeToContact, "12:00 PM EST");
			fc.utobj().generateTestData(dataSet.get("comment"));
			fc.utobj().sendKeys(driver, pobj.comments, comment);
			fc.utobj().clickElement(driver, pobj.saveBtn);

			// verify at Home Page
			fc.utobj().printTestStep("Verify Leads At Leads Page");
			fc.utobj().clickElement(driver, pobj.leadsLink);
			searchLeadByOwner(driver, corpUser.getuserFullName(), "Open Leads");

			if (!fc.utobj().getElementByXpath(driver, ".//a[.='" + firstName + " " + lastName + "']").isDisplayed()) {
				fc.utobj().throwsException("was not able to verify Name");
			}
			if (!driver
					.findElement(By.xpath(".//a[.='" + firstName + " " + lastName
							+ "']/ancestor::tr/td[contains(text () , '" + corpUser.getuserFullName() + "')]"))
					.isDisplayed()) {
				fc.utobj().throwsException("was not able to verify owner of the lead");
			}
			if (!fc.utobj().getElementByXpath(driver, ".//a[.='" + firstName + " " + lastName
					+ "']/ancestor::tr/td[contains(text () , '" + statusName + "')]").isDisplayed()) {
				fc.utobj().throwsException("was not able to verify staus of the lead");
			}

			if (!fc.utobj().getElementByXpath(driver, ".//a[.='" + firstName + " " + lastName
					+ "']/ancestor::tr/td/a[contains(text () , '" + email + "')]").isDisplayed()) {
				fc.utobj().throwsException("was not able to verify email of the lead");
			}

			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//a[.='" + firstName + " " + lastName + "']"));

			fc.utobj().printTestStep("Verify Activity History");
			boolean isemailPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//*[contains(text () ,'Email has been changed')]");
			if (isemailPresent == false) {
				fc.utobj().throwsException("was not able to verify Text Email has been changed in Activity History.");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "crm")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify the Convert Lead Into Contact By Action Image Option At CRM > Lead > Lead Summary ", testCaseId = "TC_60_Convert_Lead")
	public void convertLeadActionImage() throws IOException, Exception, ParserConfigurationException, SAXException {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			CRMLeadsPage pobj = new CRMLeadsPage(driver);

			fc.utobj().printTestStep("Admin > Users > Manage Regional Users");
			fc.utobj().printTestStep("Add Regional User");
			AdminUsersManageRegionalUsersAddRegionalUserPageTest regionalUserPage = new AdminUsersManageRegionalUsersAddRegionalUserPageTest();
			String userNameReg = fc.utobj().generateTestData(dataSet.get("userNameReg"));
			String regionName = fc.utobj().generateTestData(dataSet.get("regionName"));
			String emailId = "crmautomation@staffex.com";
			regionalUserPage.addRegionalUser(driver, userNameReg, regionName, emailId);

			String firstName = fc.utobj().generateTestData(dataSet.get("firstName"));
			String lastName = fc.utobj().generateTestData(dataSet.get("lastName"));
			String company = fc.utobj().generateTestData(dataSet.get("company"));
			String assignTo = "Regional";
			String city = fc.utobj().generateTestData(dataSet.get("city"));
			String address = fc.utobj().generateTestData(dataSet.get("address"));
			String suffix = fc.utobj().generateTestData(dataSet.get("suffix"));
			String jobTitle = fc.utobj().generateTestData(dataSet.get("jobTitle"));
			String comment = fc.utobj().generateTestData(dataSet.get("comment"));
			String franchiseId = "";
			String franchiseUser = "";
			String userName = "";
			userNameReg = userNameReg + " " + userNameReg;

			fc.utobj().printTestStep("Navigate To CRM > Leads > Leads Summary");
			fc.utobj().printTestStep("Add Lead");

			addLead(driver, dataSet, firstName, lastName, company, assignTo, city, address, suffix, jobTitle, comment,
					userName, regionName, userNameReg, franchiseId, franchiseUser, config);

			fc.utobj().clickElement(driver, pobj.leadsLink);
			searchLeadByOwner(driver, userNameReg, "Open Leads");
			fc.utobj().printTestStep("Convert Lead");
			fc.utobj().actionImgOption(driver, firstName + " " + lastName, "Convert");
			if (!fc.utobj().isSelected(driver, pobj.leadOwnerRadio)) {
				fc.utobj().clickElement(driver, pobj.leadOwnerRadio);
			}
			String contactFName = fc.utobj().generateTestData(dataSet.get("contactFName"));
			fc.utobj().sendKeys(driver, pobj.contactFirstName, contactFName);
			String contactLName = fc.utobj().generateTestData(dataSet.get("contactLName"));
			fc.utobj().sendKeys(driver, pobj.contactLastName, contactLName);
			String accountName = fc.utobj().generateTestData(dataSet.get("accountName"));
			fc.utobj().sendKeys(driver, pobj.accountName, accountName);
			fc.utobj().clickElement(driver, pobj.convertBtn);
			fc.utobj().clickElement(driver, pobj.goToLeadBtn);

			// verify at lead page
			fc.utobj().printTestStep("Verify Convert Lead At Lead Page");
			fc.utobj().clickElement(driver, pobj.showFilter);
			fc.utobj().selectDropDown(driver, pobj.viewLeadsSelect, "Converted Leads");
			fc.utobj().clickElement(driver, pobj.searchBtn);

			boolean convertedLead = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[.='" + firstName + " " + lastName + "']");
			if (convertedLead == false) {
				fc.utobj().throwsException("was not able to verify Converted Lead At Leads Home Page");
			}
			boolean owneOfLead = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[.='" + firstName + " " + lastName
					+ "']/ancestor::tr/td[contains(text () ,'" + userNameReg + "')]");
			if (owneOfLead == false) {
				fc.utobj().throwsException("was not able to verify Owner of lead After Conversion");
			}

			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//a[.='" + firstName + " " + lastName + "']/../img"));

			boolean nameOfContact = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[.='" + dataSet.get("title") + " " + contactFName + " " + contactLName + "']");
			if (nameOfContact == false) {
				fc.utobj().throwsException("was not able to verify name of Contatct");
			}
			fc.utobj().clickElement(driver, pobj.acountInfoTab);
			boolean accountNameText = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//td[.='" + accountName + "']");
			if (accountNameText == false) {
				fc.utobj().throwsException("was not able to verify Account Name");
			}
			boolean verifyRegionName = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//td[.='" + regionName + "']");
			if (verifyRegionName == false) {
				fc.utobj().throwsException("was not able to verify Region Name at account page");
			}

			fc.utobj().clickElement(driver, pobj.accountsLink);

			fc.utobj().sendKeys(driver, pobj.searchAccounts, accountName);

			fc.utobj().clickElement(driver, pobj.systemSearchButton);
			boolean isAccountNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[.='" + accountName + "']");
			if (isAccountNamePresent == false) {
				fc.utobj().throwsException("was not able to verify account name at Accounts page");
			}
			boolean isVisibilityPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[.='" + accountName + "']/ancestor::tr/td[contains(text () ,'" + regionName + "')]");
			if (isVisibilityPresent == false) {
				fc.utobj().throwsException("was not able to verify Visibility Text at Account Page");
			}
			// verify number of contact
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//a[.='" + accountName + "']/ancestor::tr/td/a[.='1']"));

			// verify Contact Info
			boolean isContactNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + contactFName + " " + contactLName + "')]");
			if (isContactNamePresent == false) {
				fc.utobj().throwsException("was not able to verify contact Name at Contact Page");
			}
			boolean isOwnerPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[contains(text () ,'" + contactFName + " "
					+ contactLName + "')]/ancestor::tr/td[contains(text () ,'" + userNameReg + "')]");
			if (isOwnerPresent == false) {
				fc.utobj().throwsException("was not able to verify owner at Contact Page");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = {"crm"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify the Log a call By Action Image Option At CRM > Lead > Lead Summary", testCaseId = "TC_61_Log_A_Call")
	public void logACallActionImage() throws IOException, Exception, ParserConfigurationException, SAXException {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			CRMLeadsPage pobj = new CRMLeadsPage(driver);

			/*
			 * fc.utobj().printTestStep(testCaseId,
			 * "Navigate To Admin Users Manage Corporate User");
			 * fc.utobj().printTestStep(testCaseId, "Add Corporate User");
			 * String emailId="crmautomation@staffex.com";
			 * AdminUsersManageCorporateUsersAddCorporateUserPageTest
			 * corporateUser=new
			 * AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			 * CorporateUser corpUser = new
			 * CorporateUser();corpUser.setEmail(emailId);corpUser =
			 * fc.CommonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			 * corporateUser.
			 * createDefaultCorporateUser_Through_WebService_AfterLogin(driver,
			 * corpUser);
			 */

			fc.utobj().printTestStep("Navigate To Admin Users Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			String emailId = "crmautomation@staffex.com";
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUser = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporateUser.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			String firstName = fc.utobj().generateTestData(dataSet.get("firstName"));
			String lastName = fc.utobj().generateTestData(dataSet.get("lastName"));
			String company = fc.utobj().generateTestData(dataSet.get("company"));
			String assignTo = "Corporate";
			String city = fc.utobj().generateTestData(dataSet.get("city"));
			String address = fc.utobj().generateTestData(dataSet.get("address"));
			String suffix = fc.utobj().generateTestData(dataSet.get("suffix"));
			String jobTitle = fc.utobj().generateTestData(dataSet.get("jobTitle"));
			String comment = fc.utobj().generateTestData(dataSet.get("comment"));
			String regionName = "";
			String regionalUser = "";
			String franchiseId = "";
			String franchiseUser = "";

			fc.utobj().printTestStep("Navigate To CRM > Leads > Leads Summary");
			fc.utobj().printTestStep("Add Lead");

			addLead(driver, dataSet, firstName, lastName, company, assignTo, city, address, suffix, jobTitle, comment,
					corpUser.getuserFullName(), regionName, regionalUser, franchiseId, franchiseUser, config);

			fc.utobj().clickElement(driver, pobj.leadsLink);
			searchLeadByOwner(driver, corpUser.getuserFullName(), "Open Leads");
			fc.utobj().printTestStep("Log A Call");
			fc.utobj().actionImgOption(driver, firstName + " " + lastName, "Log a Call");
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			String subject = fc.utobj().generateTestData(dataSet.get("subject"));
			fc.utobj().sendKeys(driver, pobj.callSubject, subject);
			String currentDate = fc.utobj().getCurrentDateUSFormat();
			fc.utobj().sendKeys(driver, pobj.callDate, currentDate);
			String callStatus = dataSet.get("callStatus");
			fc.utobj().selectDropDown(driver, pobj.callStatus, callStatus);
			String callType = dataSet.get("callType");
			fc.utobj().selectDropDown(driver, pobj.communicationType, callType);
			comment = fc.utobj().generateTestData(dataSet.get("comment"));
			fc.utobj().sendKeys(driver, pobj.comments, comment);
			fc.utobj().clickElement(driver, pobj.saveBtn);
			fc.utobj().clickElement(driver, pobj.noBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//a[contains(text () , '" + firstName + " " + lastName + "')]"));

			// verify Activity History
			fc.utobj().printTestStep("Verify At Activity History Frame");
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//*[@id='ActivityHis']//a/span[.='" + subject + "']"));
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			// call Details
			fc.utobj().isTextDisplayed(driver, subject, "was not able to verify subject");
			fc.utobj().isTextDisplayed(driver, callStatus, "was not able to verify callStatus");
			fc.utobj().isTextDisplayed(driver, callType, "was not able to verify callType");
			fc.utobj().isTextDisplayed(driver, comment, "was not able to verify comment");
			// lead info
			fc.utobj().isTextDisplayed(driver, firstName + " " + lastName, "was not able to verify subject");
			fc.utobj().clickElement(driver, pobj.closeBtn);
			fc.utobj().switchFrameToDefault(driver);

			// detail history
			/*fc.utobj().clickElement(driver, pobj.detailedHistoryLink);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().clickElement(driver, pobj.callsTabDH);
			fc.utobj().isTextDisplayed(driver, subject, "was not able to verify call subject at Activity History CBOx");
			fc.utobj().isTextDisplayed(driver, callType, "was not able to verify call Type at Activity History CBOx");
			fc.utobj().clickElement(driver, pobj.closeBtn);*/

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "crm")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify the Log A Task By Action Image Option At CRM > Lead > Lead Summary ", testCaseId = "TC_62_Log_A_Task")
	public void logATaskActiomImage() throws IOException, Exception, ParserConfigurationException, SAXException {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			CRMLeadsPage pobj = new CRMLeadsPage(driver);

			/*
			 * fc.utobj().printTestStep(testCaseId,
			 * "Navigate To Admin Users Manage Corporate User");
			 * fc.utobj().printTestStep(testCaseId, "Add Corporate User");
			 * String emailId="crmautomation@staffex.com";
			 * AdminUsersManageCorporateUsersAddCorporateUserPageTest
			 * corporateUser=new
			 * AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			 * CorporateUser corpUser = new
			 * CorporateUser();corpUser.setEmail(emailId);corpUser =
			 * fc.CommonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			 * corporateUser.
			 * createDefaultCorporateUser_Through_WebService_AfterLogin(driver,
			 * corpUser);
			 */

			fc.utobj().printTestStep("Navigate To Admin Users Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			String emailId = "crmautomation@staffex.com";
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUser = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporateUser.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			String firstName = fc.utobj().generateTestData(dataSet.get("firstName"));
			String lastName = fc.utobj().generateTestData(dataSet.get("lastName"));
			String company = fc.utobj().generateTestData(dataSet.get("company"));
			String assignTo = "Corporate";
			String city = fc.utobj().generateTestData(dataSet.get("city"));
			String address = fc.utobj().generateTestData(dataSet.get("address"));
			String suffix = fc.utobj().generateTestData(dataSet.get("suffix"));
			String jobTitle = fc.utobj().generateTestData(dataSet.get("jobTitle"));
			String comment = fc.utobj().generateTestData(dataSet.get("comment"));
			String regionName = "";
			String regionalUser = "";
			String franchiseId = "";
			String franchiseUser = "";

			fc.utobj().printTestStep("Navigate To CRM > Leads > Leads Summary");
			fc.utobj().printTestStep("Add Lead");

			addLead(driver, dataSet, firstName, lastName, company, assignTo, city, address, suffix, jobTitle, comment,
					corpUser.getuserFullName(), regionName, regionalUser, franchiseId, franchiseUser, config);

			fc.utobj().clickElement(driver, pobj.leadsLink);

			// search By System Search Button
			systemExactSearch(driver, firstName + " " + lastName);

			fc.utobj().printTestStep("Log A Task");

			fc.utobj().actionImgOption(driver, firstName + " " + lastName, "Log a Task");
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			if (!fc.utobj().isSelected(driver,pobj.radioOwner)) {
				fc.utobj().clickElement(driver, pobj.radioOwner);
			}
			String statusTask = dataSet.get("statusTask");
			fc.utobj().selectDropDown(driver, pobj.statusTask, statusTask);
			String subject = fc.utobj().generateTestData(dataSet.get("subject"));
			fc.utobj().sendKeys(driver, pobj.subject, subject);
			if (!fc.utobj().isSelected(driver, pobj.timeLessTask)) {
				fc.utobj().clickElement(driver, pobj.timeLessTask);
			}
			String priority = dataSet.get("priority");
			fc.utobj().selectDropDown(driver, pobj.priority, priority);
			comment = fc.utobj().generateTestData(dataSet.get("comment"));
			fc.utobj().sendKeys(driver, pobj.comments, comment);
			fc.utobj().clickElement(driver, pobj.createBtn);
			fc.utobj().switchFrameToDefault(driver);

			systemExactSearch(driver, firstName + " " + lastName);

			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//a[contains(text () ,'" + firstName + " " + lastName + "')]"));

			fc.utobj().printTestStep("Verify Task");

			boolean isSubjectPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[contains(text () ,'" + subject + "')]");
			if (isSubjectPresent == false) {
				fc.utobj().throwsException("was not able to verify subject");
			}
			boolean isUserPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[contains(text () ,'" + subject
					+ "')]/ancestor::tr/td/div[.='" + corpUser.getuserFullName() + "']");
			if (isUserPresent == false) {
				fc.utobj().throwsException("was not able to verify userName at Lead Info Page");
			}
			boolean isStatusOfTaskPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[contains(text () ,'" + subject
					+ "')]/ancestor::tr/td[contains(text () ,'" + statusTask + "')]");
			if (isStatusOfTaskPresent == false) {
				fc.utobj().throwsException("was not able to verify status of task");
			}
			/*
			 * boolean isCommentPresent = fc.utobj().verifyCase(driver,
			 * ".//a[contains(text () ,'" + subject +
			 * "')]/ancestor::tr/td[contains(text () ,'" + comment + "')]"); if
			 * (isCommentPresent == false) { fc.utobj().throwsException(
			 * "was not able to verify Comment of task"); }
			 */
			boolean isPriorityPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[contains(text () ,'" + subject
					+ "')]/ancestor::tr/td[contains(text () ,'" + priority + "')]");
			if (isPriorityPresent == false) {
				fc.utobj().throwsException("was not able to verify Priority of task");
			}

			// details History
			fc.utobj().printTestStep("Verify Task At Detailed History Frame");
			fc.utobj().clickElement(driver, pobj.detailedHistoryLink);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().clickElement(driver, pobj.openTaskTab);
			fc.utobj().isTextDisplayed(driver, subject,
					"was not able to verify subject of task at deatiled history pop up");
			fc.utobj().isTextDisplayed(driver, corpUser.getuserFullName(),
					"was not able to verify userName of task at deatiled history pop up");
			fc.utobj().isTextDisplayed(driver, statusTask,
					"was not able to verify status of task at deatiled history pop up");
			fc.utobj().isTextDisplayed(driver, comment,
					"was not able to verify comments of task at deatiled history pop up");
			fc.utobj().isTextDisplayed(driver, priority,
					"was not able to verify priority of task at deatiled history pop up");
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "crm")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Log A Task By Action Button Option At CRM > Lead > Lead Summary", testCaseId = "TC_63_Log_A_Task")
	public void logATaskActionBtn() throws IOException, Exception, ParserConfigurationException, SAXException {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			CRMLeadsPage pobj = new CRMLeadsPage(driver);

			/*
			 * fc.utobj().printTestStep(testCaseId,
			 * "Navigate To Admin Users Manage Corporate User");
			 * fc.utobj().printTestStep(testCaseId, "Add Corporate User");
			 * String emailId="crmautomation@staffex.com";
			 * AdminUsersManageCorporateUsersAddCorporateUserPageTest
			 * corporateUser=new
			 * AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			 * CorporateUser corpUser = new
			 * CorporateUser();corpUser.setEmail(emailId);corpUser =
			 * fc.CommonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			 * corporateUser.
			 * createDefaultCorporateUser_Through_WebService_AfterLogin(driver,
			 * corpUser);
			 */

			fc.utobj().printTestStep("Navigate To Admin Users Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			String emailId = "crmautomation@staffex.com";
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUser = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporateUser.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			String firstName = fc.utobj().generateTestData(dataSet.get("firstName"));
			String lastName = fc.utobj().generateTestData(dataSet.get("lastName"));
			String company = fc.utobj().generateTestData(dataSet.get("company"));
			String assignTo = "Corporate";
			String city = fc.utobj().generateTestData(dataSet.get("city"));
			String address = fc.utobj().generateTestData(dataSet.get("address"));
			String suffix = fc.utobj().generateTestData(dataSet.get("suffix"));
			String jobTitle = fc.utobj().generateTestData(dataSet.get("jobTitle"));
			String comment = fc.utobj().generateTestData(dataSet.get("comment"));
			String regionName = "";
			String regionalUser = "";
			String franchiseId = "";
			String franchiseUser = "";

			fc.utobj().printTestStep("Navigate To CRM > Leads > Leads Summary");
			fc.utobj().printTestStep("Add Lead");

			addLead(driver, dataSet, firstName, lastName, company, assignTo, city, address, suffix, jobTitle, comment,
					corpUser.getuserFullName(), regionName, regionalUser, franchiseId, franchiseUser, config);

			fc.utobj().clickElement(driver, pobj.leadsLink);

			// searchLeadByOwner(driver, userName, "Open Leads");
			systemExactSearch(driver, firstName + " " + lastName);

			fc.utobj().printTestStep("Log A Task");

			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,(".//a[contains(text () ,'" + firstName + " " + lastName + "')]/ancestor::tr/td/input")));
			fc.utobj().selectActionMenuItemsWithTagA(driver, "Log a Task");

			fc.commonMethods().switch_cboxIframe_frameId(driver);
			if (!fc.utobj().isSelected(driver,pobj.radioOwner)) {
				fc.utobj().clickElement(driver, pobj.radioOwner);
			}
			String statusTask = dataSet.get("statusTask");
			fc.utobj().selectDropDown(driver, pobj.statusTask, statusTask);
			String subject = fc.utobj().generateTestData(dataSet.get("subject"));
			fc.utobj().sendKeys(driver, pobj.subject, subject);
			if (!fc.utobj().isSelected(driver, pobj.timeLessTask)) {
				fc.utobj().clickElement(driver, pobj.timeLessTask);
			}
			String priority = dataSet.get("priority");
			fc.utobj().selectDropDown(driver, pobj.priority, priority);
			comment = fc.utobj().generateTestData(dataSet.get("comment"));
			fc.utobj().sendKeys(driver, pobj.comments, comment);
			fc.utobj().clickElement(driver, pobj.createBtn);
			fc.utobj().switchFrameToDefault(driver);

			// searchLeadByOwner(driver, userName, "Open Leads");
			systemExactSearch(driver, firstName + " " + lastName);

			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//a[contains(text () ,'" + firstName + " " + lastName + "')]"));

			fc.utobj().printTestStep("Verify Task");

			boolean isSubjectPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[contains(text () ,'" + subject + "')]");
			if (isSubjectPresent == false) {
				fc.utobj().throwsException("was not able to verify subject");
			}
			boolean isUserPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[contains(text () ,'" + subject
					+ "')]/ancestor::tr/td/div[.='" + corpUser.getuserFullName() + "']");
			if (isUserPresent == false) {
				fc.utobj().throwsException("was not able to verify userName at Lead Info Page");
			}
			boolean isStatusOfTaskPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[contains(text () ,'" + subject
					+ "')]/ancestor::tr/td[contains(text () ,'" + statusTask + "')]");
			if (isStatusOfTaskPresent == false) {
				fc.utobj().throwsException("was not able to verify status of task");
			}
			/*
			 * boolean isCommentPresent = fc.utobj().verifyCase(driver,
			 * ".//a[contains(text () ,'" + subject +
			 * "')]/ancestor::tr/td[contains(text () ,'" + comment + "')]"); if
			 * (isCommentPresent == false) { fc.utobj().throwsException(
			 * "was not able to verify Comment of task"); }
			 */
			boolean isPriorityPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[contains(text () ,'" + subject
					+ "')]/ancestor::tr/td[contains(text () ,'" + priority + "')]");
			if (isPriorityPresent == false) {
				fc.utobj().throwsException("was not able to verify Priority of task");
			}

			// details History
			fc.utobj().printTestStep("Verify Task At Detailed History Frame");
			fc.utobj().clickElement(driver, pobj.detailedHistoryLink);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().clickElement(driver, pobj.openTaskTab);
			fc.utobj().isTextDisplayed(driver, subject,
					"was not able to verify subject of task at deatiled history pop up");
			fc.utobj().isTextDisplayed(driver, corpUser.getuserFullName(),
					"was not able to verify userName of task at deatiled history pop up");
			fc.utobj().isTextDisplayed(driver, statusTask,
					"was not able to verify status of task at deatiled history pop up");
			fc.utobj().isTextDisplayed(driver, comment,
					"was not able to verify comments of task at deatiled history pop up");
			fc.utobj().isTextDisplayed(driver, priority,
					"was not able to verify priority of task at deatiled history pop up");

			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "crm","helloworld78"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2017-06-30", testCaseDescription = "Verify the Send  Email At CRM > Lead > Lead Summary", testCaseId = "TC_64_Send_Email")
	public void sendEmailActionBtn() throws IOException, Exception, ParserConfigurationException, SAXException {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			CRMLeadsPage pobj = new CRMLeadsPage(driver);

			fc.utobj().printTestStep("Navigate To Admin Users Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			String emailId = "crmautomation@staffex.com";
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUser = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporateUser.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			String firstName = fc.utobj().generateTestData(dataSet.get("firstName"));
			String lastName = fc.utobj().generateTestData(dataSet.get("lastName"));
			String company = fc.utobj().generateTestData(dataSet.get("company"));
			String assignTo = "Corporate";
			String city = fc.utobj().generateTestData(dataSet.get("city"));
			String address = fc.utobj().generateTestData(dataSet.get("address"));
			String suffix = fc.utobj().generateTestData(dataSet.get("suffix"));
			String jobTitle = fc.utobj().generateTestData(dataSet.get("jobTitle"));
			String comment = fc.utobj().generateTestData(dataSet.get("comment"));
			String regionName = "";
			String regionalUser = "";
			String franchiseId = "";
			String franchiseUser = "";

			fc.utobj().printTestStep("Navigate To CRM > Leads > Leads Summary");
			fc.utobj().printTestStep("Add Lead");

			addLead(driver, dataSet, firstName, lastName, company, assignTo, city, address, suffix, jobTitle, comment,
					corpUser.getuserFullName(), regionName, regionalUser, franchiseId, franchiseUser, config);

			fc.utobj().clickElement(driver, pobj.leadsLink);
			searchLeadByOwner(driver, corpUser.getuserFullName(), "Open Leads");

			fc.utobj().printTestStep("Send Email");

			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,(".//a[contains(text () ,'" + firstName + " " + lastName + "')]/ancestor::tr/td/input")));
			fc.utobj().selectActionMenuItemsWithTagA(driver, "Send Email");

			if (!fc.utobj().isSelected(driver, pobj.loggedUserId)) {
				fc.utobj().clickElement(driver, pobj.loggedUserId);
			}

			/*
			 * String mailSentToId = fc.utobj().getText(driver,
			 * pobj.mailSentTo); if
			 * (!mailSentToId.equalsIgnoreCase(dataSet.get("emailId"))) {
			 * fc.utobj().throwsException("was not able to verify mailSentTo");
			 * } String mailCC = dataSet.get("mailCC");
			 * fc.utobj().sendKeys(driver, pobj.mailCC, mailCC);
			 * 
			 * if (!pobj.showBcc) { fc.utobj().clickElement(driver,
			 * pobj.showBcc); } String mailBCC = dataSet.get("mailBCC");
			 * fc.utobj().sendKeys(driver, pobj.mailBCC, mailBCC);
			 */
			String subjectMail = fc.utobj().generateTestData(dataSet.get("subjectMail"));
			fc.utobj().sendKeys(driver, pobj.subjectMail, subjectMail);

			String editorText = fc.utobj().generateTestData(dataSet.get("mailText"));

			fc.utobj().switchFrameById(driver, "ta_ifr");

			Actions actions = new Actions(driver);
			actions.moveToElement(fc.utobj().getElement(driver, pobj.editorTextArea));
			actions.click();
			actions.sendKeys(editorText);
			fc.utobj().logReportMsg("Entered Text", editorText);
			actions.build().perform();
			fc.utobj().switchFrameToDefault(driver);
			fc.utobj().clickElement(driver, pobj.sendEmailBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Verify Send Email");

			String expectedSubject = subjectMail;
			String expectedMessageBody = editorText;
			Map<String, String> mailData = fc.utobj().readMailBox(expectedSubject, expectedMessageBody, emailId,
					"sdg@1a@Hfs");

			if (mailData.size() == 0 || !mailData.get("mailBody").contains(editorText)) {
				fc.utobj().throwsException("was not able to verify mail text");
			}
			if (mailData.size() == 0 || !mailData.get("subject").equalsIgnoreCase(subjectMail)) {
				fc.utobj().throwsException("was not able to verify subject mail");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = {"crm"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify the Change Status of Lead By Action Button Option At CRM > Lead > Lead Summary", testCaseId = "TC_65_Change_Status")
	public void changeStatusActionBtn() throws IOException, Exception, ParserConfigurationException, SAXException {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			CRMLeadsPage pobj = new CRMLeadsPage(driver);

			fc.utobj().printTestStep("Admin > Users > Manage Regional Users");
			fc.utobj().printTestStep("Add Regional User");
			AdminUsersManageRegionalUsersAddRegionalUserPageTest regionalUserPage = new AdminUsersManageRegionalUsersAddRegionalUserPageTest();
			String userNameReg = fc.utobj().generateTestData(dataSet.get("userNameReg"));
			String regionName = fc.utobj().generateTestData(dataSet.get("regionName"));
			String emailId = "crmautomation@staffex.com";
			regionalUserPage.addRegionalUser(driver, userNameReg, regionName, emailId);

			fc.utobj().printTestStep("Navigating to Admin > CRM > Configure Status");
			fc.utobj().printTestStep("Add Status Lead Type");
			AdminCRMConfigureStatusPageTest statusPage = new AdminCRMConfigureStatusPageTest();

			String statusName = fc.utobj().generateTestData(dataSet.get("statusName"));
			statusPage.addStatus(driver, statusName);

			String firstName = fc.utobj().generateTestData(dataSet.get("firstName"));
			String lastName = fc.utobj().generateTestData(dataSet.get("lastName"));
			String company = fc.utobj().generateTestData(dataSet.get("company"));
			String assignTo = "Regional";
			String city = fc.utobj().generateTestData(dataSet.get("city"));
			String address = fc.utobj().generateTestData(dataSet.get("address"));
			String suffix = fc.utobj().generateTestData(dataSet.get("suffix"));
			String jobTitle = fc.utobj().generateTestData(dataSet.get("jobTitle"));
			String comment = fc.utobj().generateTestData(dataSet.get("comment"));
			String franchiseId = "";
			String franchiseUser = "";
			String userName = "";
			userNameReg = userNameReg + " " + userNameReg;

			fc.utobj().printTestStep("Navigate To CRM > Leads > Leads Summary");
			fc.utobj().printTestStep("Add Lead");

			addLead(driver, dataSet, firstName, lastName, company, assignTo, city, address, suffix, jobTitle, comment,
					userName, regionName, userNameReg, franchiseId, franchiseUser, config);

			fc.utobj().clickElement(driver, pobj.leadsLink);
			searchLeadByOwner(driver, userNameReg, "Open Leads");

			fc.utobj().printTestStep("Change Status");

			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,(".//a[contains(text () ,'" + firstName + " " + lastName + "')]/ancestor::tr/td/input")));
			fc.utobj().selectActionMenuItemsWithTagA(driver, "Change Status");
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			// verify status
			String selectedVal = fc.utobj().getElementByXpath(driver, ".//*[@id='statusChangeTo']/option[@selected='']")
					.getText();
			if (!selectedVal.equalsIgnoreCase("New")) {
				fc.utobj().throwsException("was not able to verify selected status");
			}

			fc.utobj().selectDropDown(driver, pobj.statusChangeTo, statusName);
			String remarks = fc.utobj().generateTestData(dataSet.get("remarks"));
			fc.utobj().sendKeys(driver, pobj.remarks, remarks);
			fc.utobj().clickElement(driver, pobj.changeBtn);
			fc.utobj().clickElement(driver, pobj.closeBtn);
			fc.utobj().switchFrameToDefault(driver);
			searchLeadByOwner(driver, userNameReg, "Open Leads");

			fc.utobj().printTestStep("Verify Status");

			boolean isStatusPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[.='" + firstName + " " + lastName
					+ "']/ancestor::tr/td[contains(text() , '" + statusName + "')]");
			if (isStatusPresent == false) {
				fc.utobj().throwsException("was not able to verify status at Lead summary Page");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "crm", "crmemail" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify the Change Owner of Lead By Action Button Option At CRM > Lead > Lead Summary And Verify Lead Assignment Mail", testCaseId = "TC_66_Change_Owner")
	public void changeOwnerActionBtn() throws IOException, Exception, ParserConfigurationException, SAXException {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			CRMLeadsPage pobj = new CRMLeadsPage(driver);

			fc.utobj().printTestStep("Navigate To Admin Users Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			String emailId = "crmautomation@staffex.com";
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUser = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporateUser.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			// add Franchise user
			fc.utobj().printTestStep("Navigate To Admin > Franchise Location > Add Franchise Location");
			AdminFranchiseLocationAddFranchiseLocationPageTest franchiseLocation = new AdminFranchiseLocationAddFranchiseLocationPageTest();
			String franchiseId1 = fc.utobj().generateTestData(dataSet.get("franchiseId"));
			String regionName1 = fc.utobj().generateTestData(dataSet.get("regionName"));
			String firstNameF = fc.utobj().generateTestData(dataSet.get("firstNameFid"));
			String lastNameF = fc.utobj().generateTestData(dataSet.get("lastNameFid"));
			franchiseLocation.addFranchiseLocationForMkt(driver, franchiseId1, regionName1, firstNameF, lastNameF);

			// Add Franchise User
			fc.utobj().printTestStep("Admin > Users > Manage Franchise Users");
			fc.utobj().printTestStep("Add Franchise User");
			AdminUsersManageManageFranchiseUsersPageTest addFranUser = new AdminUsersManageManageFranchiseUsersPageTest();
			String userNameF = fc.utobj().generateTestData(dataSet.get("userNameF"));
			String roleName = "Default Franchise Role";
			String password = "t0n1ght@123";

			addFranUser.addFranchiseUser(driver, userNameF, password, franchiseId1, roleName, emailId);

			String firstName = fc.utobj().generateTestData(dataSet.get("firstName"));
			String lastName = fc.utobj().generateTestData(dataSet.get("lastName"));
			String company = fc.utobj().generateTestData(dataSet.get("company"));
			String assignTo = "Corporate";
			String city = fc.utobj().generateTestData(dataSet.get("city"));
			String address = fc.utobj().generateTestData(dataSet.get("address"));
			String suffix = fc.utobj().generateTestData(dataSet.get("suffix"));
			String jobTitle = fc.utobj().generateTestData(dataSet.get("jobTitle"));
			String comment = fc.utobj().generateTestData(dataSet.get("comment"));
			String franchiseId = "";
			String franchiseUser = "";
			String regionName = "";
			String userNameReg = "";

			fc.utobj().printTestStep("Navigate To CRM > Leads > Leads Summary");
			fc.utobj().printTestStep("Add Lead");

			addLead(driver, dataSet, firstName, lastName, company, assignTo, city, address, suffix, jobTitle, comment,
					corpUser.getuserFullName(), regionName, userNameReg, franchiseId, franchiseUser, config);

			fc.utobj().clickElement(driver, pobj.leadsLink);
			searchLeadByOwner(driver, corpUser.getuserFullName(), "Open Leads");

			fc.utobj().printTestStep("Change Owner");

			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,(".//a[contains(text () ,'" + firstName + " " + lastName + "')]/ancestor::tr/td/input")));
			fc.utobj().selectActionMenuItemsWithTagA(driver, "Change Owner");
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			if (!fc.utobj().isSelected(driver, pobj.selectFranchiseAtCbox)) {
				fc.utobj().clickElement(driver, pobj.selectFranchiseAtCbox);
			}
			fc.utobj().selectDropDown(driver, pobj.selectFranchiseId, franchiseId1);
			fc.utobj().selectDropDown(driver, pobj.franchiseUserSelect, firstNameF + " " + lastNameF);
			fc.utobj().clickElement(driver, pobj.assignBtn);
			fc.utobj().clickElement(driver, pobj.closeBtn);
			fc.utobj().switchFrameToDefault(driver);

			searchLeadByOwner(driver, firstNameF + " " + lastNameF, "Open Leads");
			fc.utobj().printTestStep("Verify Change Owner");
			boolean isLeadNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//*[.='" + firstName + " " + lastName + "']");
			if (isLeadNamePresent == false) {
				fc.utobj().throwsException("was not able to verify Lead Name");
			}
			boolean isFranchiseIdPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//*[.='" + firstName + " " + lastName
					+ "']/ancestor::tr/td[contains(text () ,'" + franchiseId1 + "')]");
			if (isFranchiseIdPresent == false) {
				fc.utobj().throwsException("was not able to verify Franchise Id");
			}
			boolean isOwnerPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//*[.='" + firstName + " " + lastName
					+ "']/ancestor::tr/td[contains(text () ,'" + firstNameF + " " + lastNameF + "')]");
			if (isOwnerPresent == false) {
				fc.utobj().throwsException("was not able to verify owner");
			}

			fc.utobj().printTestStep("Verify Mail For Lead Assignment");

			Map<String, String> mailData = fc.utobj().readMailBox("Lead Assigned", firstName, emailId, "sdg@1a@Hfs");

			if (mailData.size() == 0 || !mailData.get("mailBody").contains("Lead Assigned")) {
				fc.utobj().throwsException("was not able to verify Lead Assigned");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "crm")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Convert Lead Into Contact By Action Button Option At CRM > Lead > Lead Summary", testCaseId = "TC_67_Convert_Lead")
	public void convertLeadActionBtn() throws IOException, Exception, ParserConfigurationException, SAXException {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			CRMLeadsPage pobj = new CRMLeadsPage(driver);

			fc.utobj().printTestStep("Admin > Users > Manage Regional Users");
			fc.utobj().printTestStep("Add Regional User");
			AdminUsersManageRegionalUsersAddRegionalUserPageTest regionalUserPage = new AdminUsersManageRegionalUsersAddRegionalUserPageTest();
			String userNameReg = fc.utobj().generateTestData(dataSet.get("userNameReg"));
			String regionName = fc.utobj().generateTestData(dataSet.get("regionName"));
			String emailId = "crmautomation@staffex.com";
			regionalUserPage.addRegionalUser(driver, userNameReg, regionName, emailId);

			fc.utobj().printTestStep("Navigating to Admin > CRM >  Configure Opportunity Stages");
			fc.utobj().printTestStep("Add Stage");
			AdminCRMConfigureOpportunityStagesPageTest stagePage = new AdminCRMConfigureOpportunityStagesPageTest();
			String stage = fc.utobj().generateTestData(dataSet.get("stage"));
			stagePage.addStage(driver, stage);

			String firstName = fc.utobj().generateTestData(dataSet.get("firstName"));
			String lastName = fc.utobj().generateTestData(dataSet.get("lastName"));
			String company = fc.utobj().generateTestData(dataSet.get("company"));
			String assignTo = "Regional";
			String city = fc.utobj().generateTestData(dataSet.get("city"));
			String address = fc.utobj().generateTestData(dataSet.get("address"));
			String suffix = fc.utobj().generateTestData(dataSet.get("suffix"));
			String jobTitle = fc.utobj().generateTestData(dataSet.get("jobTitle"));
			String comment = fc.utobj().generateTestData(dataSet.get("comment"));
			String franchiseId = "";
			String franchiseUser = "";
			String userName = "";
			userNameReg = userNameReg + " " + userNameReg;

			fc.utobj().printTestStep("Navigate To CRM > Leads > Leads Summary");
			fc.utobj().printTestStep("Add Lead");

			addLead(driver, dataSet, firstName, lastName, company, assignTo, city, address, suffix, jobTitle, comment,
					userName, regionName, userNameReg, franchiseId, franchiseUser, config);

			fc.utobj().clickElement(driver, pobj.leadsLink);
			searchLeadByOwner(driver, userNameReg, "Open Leads");

			fc.utobj().printTestStep("Convert Lead");

			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,(".//a[contains(text () ,'" + firstName + " " + lastName + "')]/ancestor::tr/td/input")));
			fc.utobj().selectActionMenuItemsWithTagA(driver, "Convert");

			if (!fc.utobj().isSelected(driver, pobj.leadOwnerRadio)) {
				fc.utobj().clickElement(driver, pobj.leadOwnerRadio);
			}
			if (!fc.utobj().isSelected(driver, pobj.createNewAccount)) {
				fc.utobj().clickElement(driver, pobj.createNewAccount);
			}
			String accountName = fc.utobj().generateTestData(dataSet.get("accountName"));
			fc.utobj().sendKeys(driver, pobj.accountName, accountName);

			// create New Opportunity
			if (!fc.utobj().isSelected(driver,pobj.isOpportunity)) {
				fc.utobj().clickElement(driver, pobj.isOpportunity);
			}

			String opportunityName = fc.utobj().generateTestData(dataSet.get("opportunityName"));
			fc.utobj().sendKeys(driver, pobj.opportunityName, opportunityName);
			fc.utobj().selectDropDown(driver, pobj.stage, stage);
			String salesAmount = dataSet.get("salesAmount");
			fc.utobj().sendKeys(driver, pobj.salesAmount, salesAmount);
			String closureDate = fc.utobj().getFutureDateUSFormat(4);
			fc.utobj().sendKeys(driver, pobj.closerDate, closureDate);
			fc.utobj().clickElement(driver, pobj.convertBtn);
			fc.utobj().clickElement(driver, pobj.goToLeadBtn);

			// check in open leads

			fc.utobj().printTestStep("Verify At Lead Page With Filter Open Leads");

			searchLeadByOwner(driver, userNameReg, "Open Leads");
			boolean isNoRecordPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//td[.='No records found.']");
			if (isNoRecordPresent == false) {
				fc.utobj().throwsException("was not able to convert Lead");
			}

			searchLeadByOwner(driver, userNameReg, "Converted Leads");

			fc.utobj().printTestStep("Verify At Lead Page With Filter Converted Leads");

			boolean isLeadNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//*[.='" + firstName + " " + lastName + "']");
			if (isLeadNamePresent == false) {
				fc.utobj().throwsException("was not able to verify Lead Name");
			}
			boolean isFranchiseIdPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//*[.='" + firstName + " " + lastName
					+ "']/ancestor::tr/td[contains(text () ,'" + userNameReg + "')]");
			if (isFranchiseIdPresent == false) {
				fc.utobj().throwsException("was not able to verify Owner");
			}

			// verify Lead Information
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//*[.='" + firstName + " " + lastName + "']"));

			boolean isAccountPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[.='" + accountName + "']");
			if (isAccountPresent == false) {
				fc.utobj().throwsException("was not able to verify account");
			}
			boolean isContactPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[.='" + firstName + " " + lastName + "']");
			if (isContactPresent == false) {
				fc.utobj().throwsException("was not able to verify Contact");
			}
			boolean isOpportunityPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + opportunityName + "')]");
			if (isOpportunityPresent == false) {
				fc.utobj().throwsException("was not able to verify Opportunity");
			}

			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//a[contains(text () ,'View Lead Details')]"));
			fc.commonMethods().switch_cboxIframe_frameId(driver);

			boolean isFirstNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () ,'First Name')]/following-sibling::td[.='" + firstName + "']");
			if (isFirstNamePresent == false) {
				fc.utobj().throwsException("was not able to verify First Name");
			}
			boolean isLastNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () ,'Last Name')]/following-sibling::td[.='" + lastName + "']");
			if (isLastNamePresent == false) {
				fc.utobj().throwsException("was not able to verify Last Name");
			}
			boolean isCompanyPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () ,'Company')]/following-sibling::td[.='" + company + "']");
			if (isCompanyPresent == false) {
				fc.utobj().throwsException("was not able to verify Company Name");
			}
			boolean isPrimaryContactMethodPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () ,'Primary Contact Method')]/following-sibling::td[.='"
							+ dataSet.get("primaryContactMethod") + "']");
			if (isPrimaryContactMethodPresent == false) {
				fc.utobj().throwsException("was not able to verify Primary Contact Method");
			}
			boolean isAssignToPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () ,'Assign To')]/following-sibling::td[.='" + userNameReg + "']");
			if (isAssignToPresent == false) {
				fc.utobj().throwsException("was not able to verify AssignTo");
			}
			boolean isAreaPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () ,'Area / Region')]/following-sibling::td[.='" + regionName + "']");
			if (isAreaPresent == false) {
				fc.utobj().throwsException("was not able to verify Area Region");
			}
			fc.utobj().clickElement(driver, pobj.closeBtn);

			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().clickElement(driver, pobj.goToLeadBtn);

			// verify account and opportunity and contact information
			searchLeadByOwner(driver, userNameReg, "Converted Leads");
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//*[.='" + firstName + " " + lastName + "']/ancestor::tr/td/img"));

			boolean isContatcInfoPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[.='" + dataSet.get("title") + " " + firstName + " " + lastName + "']");
			if (isContatcInfoPresent == false) {
				fc.utobj().throwsException("was not able to verify Contact information");
			}

			fc.utobj().clickElement(driver, pobj.accountInfoTab);
			boolean isAccountNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () ,'Account Name')]/following-sibling::td[.='" + accountName + "']");
			if (isAccountNamePresent == false) {
				fc.utobj().throwsException("was not able to verify Account Name");
			}

			fc.utobj().clickElement(driver, pobj.opportunityTab);

			boolean isOpportunityNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[.='" + firstName + " " + lastName + "-" + opportunityName + "']");
			if (isOpportunityNamePresent == false) {
				fc.utobj().throwsException("was not able to verify Opportunity At Opprtunities Info Page");
			}
			boolean isAssignToOppPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[.='" + firstName + " " + lastName + "-"
					+ opportunityName + "']/ancestor::tr/td[contains(text () ,'" + userNameReg + "')]");
			if (isAssignToOppPresent == false) {
				fc.utobj().throwsException("was not able to verify Assign TO At Opprtunities Info Page");
			}
			boolean isContactOppPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[.='" + firstName + " " + lastName + "-" + opportunityName
							+ "']/ancestor::tr/td/a[contains(text () ,'" + firstName + " " + lastName + "')]");
			if (isContactOppPresent == false) {
				fc.utobj().throwsException("was not able to verify Contact At Opprtunities Info Page");
			}
			boolean isStageOppPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[.='" + firstName + " " + lastName + "-"
					+ opportunityName + "']/ancestor::tr/td[contains(text () ,'" + stage + "')]");
			if (isStageOppPresent == false) {
				fc.utobj().throwsException("was not able to verify Stage At Opprtunities Info Page");
			}
			boolean isSalesAmountOppPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[.='" + firstName + " " + lastName + "-" + opportunityName
							+ "']/ancestor::tr/td[contains(text () ,'" + dataSet.get("salesAmount").concat(".00")
							+ "')]");
			if (isSalesAmountOppPresent == false) {
				fc.utobj().throwsException("was not able to verify Sales Amount At Opprtunities Info Page");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = {"crm"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Delete Lead By Action Button Option CRM > Lead > Lead Summary", testCaseId = "TC_68_Delete_Lead")
	public void deleteLeadActionBtn() throws IOException, Exception, ParserConfigurationException, SAXException {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			CRMLeadsPage pobj = new CRMLeadsPage(driver);

			fc.utobj().printTestStep("Navigate To Admin Users Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			//String emailId = "crmautomation@staffex.com";
			String emailId=fc.utobj().generateTestData("crmautomation")+"@staffex.com";
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUser = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporateUser.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			String firstName = fc.utobj().generateTestData(dataSet.get("firstName"));
			String lastName = fc.utobj().generateTestData(dataSet.get("lastName"));
			String company = fc.utobj().generateTestData(dataSet.get("company"));
			String assignTo = "Corporate";
			String city = fc.utobj().generateTestData(dataSet.get("city"));
			String address = fc.utobj().generateTestData(dataSet.get("address"));
			String suffix = fc.utobj().generateTestData(dataSet.get("suffix"));
			String jobTitle = fc.utobj().generateTestData(dataSet.get("jobTitle"));
			String comment = fc.utobj().generateTestData(dataSet.get("comment"));
			String franchiseId = "";
			String franchiseUser = "";
			String userNameReg = "";
			String regionName = "";

			fc.utobj().printTestStep("Navigate To CRM > Leads > Leads Summary");
			fc.utobj().printTestStep("Add Lead");

			addLead(driver, dataSet, firstName, lastName, company, assignTo, city, address, suffix, jobTitle, comment,
					corpUser.getuserFullName(), regionName, userNameReg, franchiseId, franchiseUser, config);

			fc.utobj().clickElement(driver, pobj.leadsLink);
			searchLeadByOwner(driver, corpUser.getuserFullName(), "Open Leads");

			fc.utobj().printTestStep("Delete Lead");

			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,(".//a[contains(text () ,'" + firstName + " " + lastName + "')]/ancestor::tr/td/input")));
			fc.utobj().selectActionMenuItemsWithTagA(driver, "Delete");
			//fc.utobj().acceptAlertBox(driver);

			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().clickElement(driver,pobj.deleteLead);
			fc.utobj().sleep(3000);
			fc.commonMethods().Click_Close_Input_ByValue(driver);
			fc.utobj().switchFrameToDefault(driver);
			
			fc.utobj().printTestStep("Verify Lead");

			searchLeadByOwner(driver, corpUser.getuserFullName(), "All Leads");
			boolean isNoRecordPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//td[.='No records found.']");
			if (isNoRecordPresent == false) {
				fc.utobj().throwsException("was not able to Delete Lead");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "crm", "crmcheckk"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2017-06-29", testCaseDescription = "Verify the Archive Lead Action Button Options At CRM > Lead > Lead Summary", testCaseId = "TC_69_Archive_Lead")
	public void archiveLeadActionBtn() throws IOException, Exception, ParserConfigurationException, SAXException {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			CRMLeadsPage pobj = new CRMLeadsPage(driver);

			fc.utobj().printTestStep("Navigate To Admin Users Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			//String emailId = "crmautomation@staffex.com";
			String emailId=fc.utobj().generateTestData("crmautomation")+"@staffex.com";
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUser = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporateUser.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			String firstName = fc.utobj().generateTestData(dataSet.get("firstName"));
			String lastName = fc.utobj().generateTestData(dataSet.get("lastName"));
			String company = fc.utobj().generateTestData(dataSet.get("company"));
			String assignTo = "Corporate";
			String city = fc.utobj().generateTestData(dataSet.get("city"));
			String address = fc.utobj().generateTestData(dataSet.get("address"));
			String suffix = fc.utobj().generateTestData(dataSet.get("suffix"));
			String jobTitle = fc.utobj().generateTestData(dataSet.get("jobTitle"));
			String comment = fc.utobj().generateTestData(dataSet.get("comment"));
			String franchiseId = "";
			String franchiseUser = "";
			String userNameReg = "";
			String regionName = "";

			fc.utobj().printTestStep("Navigate To CRM > Leads > Leads Summary");
			fc.utobj().printTestStep("Add Lead");

			addLead(driver, dataSet, firstName, lastName, company, assignTo, city, address, suffix, jobTitle, comment,
					corpUser.getuserFullName(), regionName, userNameReg, franchiseId, franchiseUser, config);

			fc.utobj().clickElement(driver, pobj.leadsLink);
			searchLeadByOwner(driver, corpUser.getuserFullName(), "Open Leads");
			fc.utobj().printTestStep("Archive Lead");
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,(".//a[contains(text () ,'" + firstName + " " + lastName + "')]/ancestor::tr/td/input")));
			fc.utobj().selectActionMenuItemsWithTagA(driver, "Archive Leads");
			//fc.utobj().acceptAlertBox(driver);
			
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().clickElement(driver,pobj.archive);
			fc.commonMethods().Click_Close_Input_ByValue(driver);
			fc.utobj().switchFrameToDefault(driver);
			
			
			searchLeadByOwner(driver, corpUser.getuserFullName(), "All Leads");

			fc.utobj().printTestStep("Verify At Lead Page");

			boolean isNoRecordPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//td[.='No records found.']");
			if (isNoRecordPresent == false) {
				fc.utobj().throwsException("was not able to Archive Lead");
			}

			fc.utobj().printTestStep("Verify Archive At Search Page");

			fc.utobj().clickElement(driver, pobj.searchLink);
			fc.utobj().clickElement(driver, pobj.leadsTab);
			if (!fc.utobj().isSelected(driver, pobj.archiveLeads)) {
				fc.utobj().clickElement(driver, pobj.archiveLeads);
			}
			fc.utobj().setToDefault(driver, pobj.selectAssignTo);
			WebElement elementInput = fc.utobj().getElement(driver, pobj.selectAssignTo)
					.findElement(By.xpath("./div/div/input"));
			fc.utobj().selectValFromMultiSelect(driver, pobj.selectAssignTo, elementInput, corpUser.getuserFullName());
			fc.utobj().clickElement(driver, pobj.searchBtnAtSearch);

			boolean isNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[.='" + firstName + " " + lastName + "']");
			if (isNamePresent == false) {
				fc.utobj().throwsException("was not able to verify Archive Leads");
			}
			boolean isOwnerPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[.='" + firstName + " " + lastName
					+ "']/ancestor::tr/td[contains(text () , '" + corpUser.getuserFullName() + "')]");
			if (isOwnerPresent == false) {
				fc.utobj().throwsException("was not able to verify Owner of Archive Lead");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "crm", "crmgrpouyp" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Lead Add To Group By Action Button Option At CRM > Lead > Lead Summary", testCaseId = "TC_70_Add_To_Group")
	public void addToGroupActionBtn() throws IOException, Exception, ParserConfigurationException, SAXException {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			CRMLeadsPage pobj = new CRMLeadsPage(driver);
			CRMGroupsPageTest groupPage = new CRMGroupsPageTest();

			/*
			 * fc.utobj().printTestStep(testCaseId,
			 * "Navigate To Admin Users Manage Corporate User");
			 * fc.utobj().printTestStep(testCaseId, "Add Corporate User");
			 * String emailId="crmautomation@staffex.com";
			 * AdminUsersManageCorporateUsersAddCorporateUserPageTest
			 * corporateUser=new
			 * AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			 * CorporateUser corpUser = new
			 * CorporateUser();corpUser.setEmail(emailId);corpUser =
			 * fc.CommonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			 * corporateUser.
			 * createDefaultCorporateUser_Through_WebService_AfterLogin(driver,
			 * corpUser);
			 */

			fc.utobj().printTestStep("Navigate To Admin Users Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			String emailId = "crmautomation@staffex.com";
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUser = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporateUser.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			String firstName = fc.utobj().generateTestData(dataSet.get("firstName"));
			String lastName = fc.utobj().generateTestData(dataSet.get("lastName"));
			String company = fc.utobj().generateTestData(dataSet.get("company"));
			String assignTo = "Corporate";
			String city = fc.utobj().generateTestData(dataSet.get("city"));
			String address = fc.utobj().generateTestData(dataSet.get("address"));
			String suffix = fc.utobj().generateTestData(dataSet.get("suffix"));
			String jobTitle = fc.utobj().generateTestData(dataSet.get("jobTitle"));
			String comment = fc.utobj().generateTestData(dataSet.get("comment"));
			String franchiseId = "";
			String franchiseUser = "";
			String userNameReg = "";
			String regionName = "";
			fc.utobj().printTestStep("Navigate To CRM > Leads > Leads Summary");
			fc.utobj().printTestStep("Add Lead");
			addLead(driver, dataSet, firstName, lastName, company, assignTo, city, address, suffix, jobTitle, comment,
					corpUser.getuserFullName(), regionName, userNameReg, franchiseId, franchiseUser, config);

			fc.utobj().clickElement(driver, pobj.leadsLink);
			searchLeadByOwner(driver, corpUser.getuserFullName(), "Open Leads");
			fc.utobj().printTestStep("Add Lead To Group");
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,(".//a[contains(text () ,'" + firstName + " " + lastName + "')]/ancestor::tr/td/input")));
			fc.utobj().selectActionMenuItemsWithTagA(driver, "Add To Group");

			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().clickElement(driver, pobj.addToNewGroup);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Create New Group");
			String groupName = fc.utobj().generateTestData(dataSet.get("groupName"));
			String accessibility = "Public to all Users";
			groupPage.addToGroupRegular(driver, groupName, accessibility);

			/*
			 * fc.utobj().sendKeys(driver, pobj.searchAtGroup, groupName);
			 * fc.utobj().clickElement(driver, pobj.exactSearch);
			 * fc.utobj().clickElement(driver, pobj.systemSearchButton);
			 */
			new CRMGroupsPageTest().searchGroupsByFilter(driver, groupName, "Lead");
			String colunmName = "";
			int z = 0;

			// click over Recipients
			List<WebElement> listHead = driver
					.findElements(By.xpath(".//a[.='" + groupName + "']/ancestor::table/thead/tr/th"));
			System.out.println(listHead);
			for (int i = 0; i < listHead.size(); i++) {

				colunmName = listHead.get(i).getText().trim();
				if (colunmName.equalsIgnoreCase("Recipients")) {
					z = i;
					break;
				}
			}

			if (z != 0) {
				z = z + 1;
				fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
						".//a[.='" + groupName + "']/ancestor::tr/td[" + z + "]/a"));
			} else {
				fc.utobj().throwsException("was not able to find Recipients Column");
			}
			searchLeadByOwner(driver, corpUser.getuserFullName(), "All Leads");

			fc.utobj().printTestStep("Verify Lead In Group");

			boolean isLeadNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[.='" + firstName + " " + lastName + "']");
			if (isLeadNamePresent == false) {
				fc.utobj().throwsException("was not able to add lead in group");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "crm")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Log Task By Bottom Button Option By CRM > Lead > Lead Summary", testCaseId = "TC_71_Log_A_Task")
	public void logATaskBottomBtn() throws IOException, Exception, ParserConfigurationException, SAXException {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			CRMLeadsPage pobj = new CRMLeadsPage(driver);

			/*
			 * fc.utobj().printTestStep(testCaseId,
			 * "Navigate To Admin Users Manage Corporate User");
			 * fc.utobj().printTestStep(testCaseId, "Add Corporate User");
			 * String emailId="crmautomation@staffex.com";
			 * AdminUsersManageCorporateUsersAddCorporateUserPageTest
			 * corporateUser=new
			 * AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			 * CorporateUser corpUser = new
			 * CorporateUser();corpUser.setEmail(emailId);corpUser =
			 * fc.CommonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			 * corporateUser.
			 * createDefaultCorporateUser_Through_WebService_AfterLogin(driver,
			 * corpUser);
			 */

			fc.utobj().printTestStep("Navigate To Admin Users Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			String emailId = "crmautomation@staffex.com";
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUser = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporateUser.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			String firstName = fc.utobj().generateTestData(dataSet.get("firstName"));
			String lastName = fc.utobj().generateTestData(dataSet.get("lastName"));
			String company = fc.utobj().generateTestData(dataSet.get("company"));
			String assignTo = "Corporate";
			String city = fc.utobj().generateTestData(dataSet.get("city"));
			String address = fc.utobj().generateTestData(dataSet.get("address"));
			String suffix = fc.utobj().generateTestData(dataSet.get("suffix"));
			String jobTitle = fc.utobj().generateTestData(dataSet.get("jobTitle"));
			String comment = fc.utobj().generateTestData(dataSet.get("comment"));
			String regionName = "";
			String regionalUser = "";
			String franchiseId = "";
			String franchiseUser = "";
			fc.utobj().printTestStep("Navigate To CRM > Leads > Leads Summary");
			fc.utobj().printTestStep("Add Lead");
			addLead(driver, dataSet, firstName, lastName, company, assignTo, city, address, suffix, jobTitle, comment,
					corpUser.getuserFullName(), regionName, regionalUser, franchiseId, franchiseUser, config);

			fc.utobj().clickElement(driver, pobj.leadsLink);

			// searchLeadByOwner(driver, userName, "Open Leads");
			systemExactSearch(driver, firstName + " " + lastName);

			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,(".//a[contains(text () ,'" + firstName + " " + lastName + "')]/ancestor::tr/td/input")));

			fc.utobj().printTestStep("Log Task");

			fc.utobj().clickElement(driver, pobj.logTaskBtmBtn);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			if (!fc.utobj().isSelected(driver,pobj.radioOwner)) {
				fc.utobj().clickElement(driver, pobj.radioOwner);
			}
			String statusTask = dataSet.get("statusTask");
			fc.utobj().selectDropDown(driver, pobj.statusTask, statusTask);
			String subject = fc.utobj().generateTestData(dataSet.get("subject"));
			fc.utobj().sendKeys(driver, pobj.subject, subject);
			if (!fc.utobj().isSelected(driver, pobj.timeLessTask)) {
				fc.utobj().clickElement(driver, pobj.timeLessTask);
			}
			String priority = dataSet.get("priority");
			fc.utobj().selectDropDown(driver, pobj.priority, priority);
			comment = fc.utobj().generateTestData(dataSet.get("comment"));
			fc.utobj().sendKeys(driver, pobj.comments, comment);
			fc.utobj().clickElement(driver, pobj.createBtn);
			fc.utobj().switchFrameToDefault(driver);

			// searchLeadByOwner(driver, userName, "Open Leads");
			systemExactSearch(driver, firstName + " " + lastName);

			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//a[contains(text () ,'" + firstName + " " + lastName + "')]"));

			fc.utobj().printTestStep("Verify Log Task");
			boolean isSubjectPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[contains(text () ,'" + subject + "')]");
			if (isSubjectPresent == false) {
				fc.utobj().throwsException("was not able to verify subject");
			}
			boolean isUserPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[contains(text () ,'" + subject
					+ "')]/ancestor::tr/td/div[.='" + corpUser.getuserFullName() + "']");
			if (isUserPresent == false) {
				fc.utobj().throwsException("was not able to verify userName at Lead Info Page");
			}
			boolean isStatusOfTaskPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[contains(text () ,'" + subject
					+ "')]/ancestor::tr/td[contains(text () ,'" + statusTask + "')]");
			if (isStatusOfTaskPresent == false) {
				fc.utobj().throwsException("was not able to verify status of task");
			}
			/*
			 * boolean isCommentPresent = fc.utobj().verifyCase(driver,
			 * ".//a[contains(text () ,'" + subject +
			 * "')]/ancestor::tr/td[contains(text () ,'" + comment + "')]"); if
			 * (isCommentPresent == false) { fc.utobj().throwsException(
			 * "was not able to verify Comment of task"); }
			 */
			boolean isPriorityPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[contains(text () ,'" + subject
					+ "')]/ancestor::tr/td[contains(text () ,'" + priority + "')]");
			if (isPriorityPresent == false) {
				fc.utobj().throwsException("was not able to verify Priority of task");
			}

			// details History

			fc.utobj().printTestStep("Verify Task At Detailed History Frame");
			fc.utobj().clickElement(driver, pobj.detailedHistoryLink);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().clickElement(driver, pobj.openTaskTab);
			fc.utobj().isTextDisplayed(driver, subject,
					"was not able to verify subject of task at deatiled history pop up");
			fc.utobj().isTextDisplayed(driver, corpUser.getuserFullName(),
					"was not able to verify userName of task at deatiled history pop up");
			fc.utobj().isTextDisplayed(driver, statusTask,
					"was not able to verify status of task at deatiled history pop up");
			fc.utobj().isTextDisplayed(driver, comment,
					"was not able to verify comments of task at deatiled history pop up");
			fc.utobj().isTextDisplayed(driver, priority,
					"was not able to verify priority of task at deatiled history pop up");
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "crm", "crmemail" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2017-06-27", testCaseDescription = "Verify the Send Email With Existing Email Template By Button At CRM > Lead > Lead Summary", testCaseId = "TC_72_Send_Email")
	public void sendEmailBottomBtn() throws IOException, Exception, ParserConfigurationException, SAXException {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			CRMLeadsPage pobj = new CRMLeadsPage(driver);
			CRMCampaignCenterEmailTemplatesPageTest emailTemplate = new CRMCampaignCenterEmailTemplatesPageTest();

			fc.utobj().printTestStep("Navigate To CRM > Campaign Center > Email Template");
			String templateName = fc.utobj().generateTestData(dataSet.get("templateName"));
			String emailSubject = fc.utobj().generateTestData(dataSet.get("emailSubject"));
			String plainTextEditor = fc.utobj().generateTestData(dataSet.get("plainTextEditor"));
			String accessibility = dataSet.get("accessibility");
			String emailType = dataSet.get("emailType");

			fc.utobj().printTestStep("Create Corporate Template");
			emailTemplate.createCodeYourTemplate(driver, config, dataSet, accessibility, emailType, templateName,
					emailSubject, plainTextEditor);

			fc.utobj().printTestStep("Navigate To Admin Users Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			String emailId = "crmautomation@staffex.com";
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUser = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporateUser.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			String firstName = fc.utobj().generateTestData(dataSet.get("firstName"));
			String lastName = fc.utobj().generateTestData(dataSet.get("lastName"));
			String company = fc.utobj().generateTestData(dataSet.get("company"));
			String assignTo = "Corporate";
			String city = fc.utobj().generateTestData(dataSet.get("city"));
			String address = fc.utobj().generateTestData(dataSet.get("address"));
			String suffix = fc.utobj().generateTestData(dataSet.get("suffix"));
			String jobTitle = fc.utobj().generateTestData(dataSet.get("jobTitle"));
			String comment = fc.utobj().generateTestData(dataSet.get("comment"));
			String regionName = "";
			String regionalUser = "";
			String franchiseId = "";
			String franchiseUser = "";
			fc.utobj().printTestStep("Navigate To CRM > Leads > Leads Summary");
			fc.utobj().printTestStep("Add Lead");
			addLead(driver, dataSet, firstName, lastName, company, assignTo, city, address, suffix, jobTitle, comment,
					corpUser.getuserFullName(), regionName, regionalUser, franchiseId, franchiseUser, config);

			fc.utobj().clickElement(driver, pobj.leadsLink);
			systemExactSearch(driver, firstName + " " + lastName);

			fc.utobj().printTestStep("Send Email");
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,(".//a[contains(text () ,'" + firstName + " " + lastName + "')]/ancestor::tr/td/input")));
			fc.utobj().clickElement(driver, pobj.sendEmailBtmBtn);

			if (!fc.utobj().isSelected(driver, pobj.loggedUserId)) {
				fc.utobj().clickElement(driver, pobj.loggedUserId);
			}

			fc.utobj().selectDropDown(driver, pobj.mailTemplateID, templateName);

			String mailSentToId = fc.utobj().getText(driver, pobj.mailSentTo);
			/*if (!mailSentToId.equalsIgnoreCase(dataSet.get("emailId"))) {
				fc.utobj().throwsException("was not able to verify mailSentTo");
			}*/

			fc.utobj().clickElement(driver, pobj.sendEmailBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Verify Existing Email Templates Mail For Lead");

			Map<String, String> mailData = fc.utobj().readMailBox(emailSubject, plainTextEditor, emailId, "sdg@1a@Hfs");

			if (mailData.size() == 0 || !mailData.get("mailBody").contains(plainTextEditor)) {
				fc.utobj().throwsException("was not able to verify mail text");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "crm", "crmCheck112" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2017-06-26", testCaseDescription = "Verify The Lead Associate with email campaign By Action Button Option At CRM > Lead > Lead Summary", testCaseId = "TC_73_Associate_With_Email_Campaign")
	public void AssociateWithEmailCompaignActionBtn()
			throws IOException, Exception, ParserConfigurationException, SAXException {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			CRMLeadsPage pobj = new CRMLeadsPage(driver);

			fc.utobj().printTestStep("Navigate To CRM >Campaign Center");
			fc.utobj().printTestStep("Add Campaign");
			CRMCampaignCenterPageTestForCRM campaignCenterPage = new CRMCampaignCenterPageTestForCRM();
			String campaignName = fc.utobj().generateTestData(dataSet.get("campaignName"));
			String campaignDescription = fc.utobj().generateTestData(dataSet.get("campaignDescription"));
			String campaignAccessibility = dataSet.get("campaignAccessibility");
			String category = dataSet.get("category");
			String templateName = fc.utobj().generateTestData(dataSet.get("templateName"));
			String mailSubject = dataSet.get("mailSubject");
			String templateText = fc.utobj().generateTestData(dataSet.get("templateText"));
			campaignCenterPage.createCampaign(driver, campaignName, campaignDescription, campaignAccessibility,
					category, templateName, mailSubject, templateText, config);

			fc.utobj().printTestStep("Navigate To Admin Users Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			String emailId = "crmautomation@staffex.com";
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUser = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporateUser.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			String firstName = fc.utobj().generateTestData(dataSet.get("firstName"));
			String lastName = fc.utobj().generateTestData(dataSet.get("lastName"));
			String company = fc.utobj().generateTestData(dataSet.get("company"));
			String assignTo = "Corporate";
			String city = fc.utobj().generateTestData(dataSet.get("city"));
			String address = fc.utobj().generateTestData(dataSet.get("address"));
			String suffix = fc.utobj().generateTestData(dataSet.get("suffix"));
			String jobTitle = fc.utobj().generateTestData(dataSet.get("jobTitle"));
			String comment = fc.utobj().generateTestData(dataSet.get("comment"));
			String franchiseId = "";
			String franchiseUser = "";
			String userNameReg = "";
			String regionName = "";
			fc.utobj().printTestStep("Navigate To CRM > Leads > Leads Summary");
			fc.utobj().printTestStep("Add Lead");
			addLead(driver, dataSet, firstName, lastName, company, assignTo, city, address, suffix, jobTitle, comment,
					corpUser.getuserFullName(), regionName, userNameReg, franchiseId, franchiseUser, config);

			fc.utobj().clickElement(driver, pobj.leadsLink);
			searchLeadByOwner(driver, corpUser.getuserFullName(), "Open Leads");
			fc.utobj().printTestStep("Associate With Campaign");
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,(".//a[contains(text () ,'" + firstName + " " + lastName + "')]/ancestor::tr/td/input")));
			try {
				fc.utobj().selectActionMenuItemsWithTagA(driver, "Associate With Campaign");
			} catch (Exception e) {
				fc.utobj().selectActionMenuItemsWithTagA(driver, "Associate with Campaign");
			}
			fc.utobj().clickElement(driver, pobj.filterContainer);
			fc.utobj().sendKeys(driver, pobj.searchCampaign, campaignName);
			fc.utobj().clickElement(driver, pobj.applyFilter);

			fc.utobj().clickElement(driver, pobj.sendCampaignBtn);
			fc.utobj().clickElement(driver, pobj.confirmBtn);
			fc.utobj().clickElement(driver, pobj.leadsLink);

			/*
			 * fc.utobj().sendKeys(driver, pobj.searchSystem, firstName);
			 * fc.utobj().clickElement(driver, pobj.systemSearchButton);
			 */

			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//a[.='" + firstName + " " + lastName + "']"));

			fc.utobj().printTestStep("Verify Lead Assocaite With Campaign");
			boolean isCampaignNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () ,'Promotional Campaign')]/following-sibling::td[.='" + campaignName + "']");
			if (isCampaignNamePresent == false) {
				fc.utobj().throwsException("was not able to verify Campign Name");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "crm")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Change Status of Lead By Bottom Button Option At CRM > Lead > Lead Summary", testCaseId = "TC_74_Change_Status")
	public void changeStatusBottomBtn() throws IOException, Exception, ParserConfigurationException, SAXException {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			CRMLeadsPage pobj = new CRMLeadsPage(driver);

			fc.utobj().printTestStep("Admin > Users > Manage Regional Users");
			fc.utobj().printTestStep("Add Regional User");

			AdminUsersManageRegionalUsersAddRegionalUserPageTest regionalUserPage = new AdminUsersManageRegionalUsersAddRegionalUserPageTest();
			String userNameReg = fc.utobj().generateTestData(dataSet.get("userNameReg"));
			String regionName = fc.utobj().generateTestData(dataSet.get("regionName"));
			String emailId = "crmautomation@staffex.com";
			regionalUserPage.addRegionalUser(driver, userNameReg, regionName, emailId);

			fc.utobj().printTestStep("Navigating to Admin > CRM > Configure Status");
			fc.utobj().printTestStep("Add Status Lead Type");
			AdminCRMConfigureStatusPageTest statusPage = new AdminCRMConfigureStatusPageTest();
			String statusName = fc.utobj().generateTestData(dataSet.get("statusName"));
			statusPage.addStatus(driver, statusName);

			String firstName = fc.utobj().generateTestData(dataSet.get("firstName"));
			String lastName = fc.utobj().generateTestData(dataSet.get("lastName"));
			String company = fc.utobj().generateTestData(dataSet.get("company"));
			String assignTo = "Regional";
			String city = fc.utobj().generateTestData(dataSet.get("city"));
			String address = fc.utobj().generateTestData(dataSet.get("address"));
			String suffix = fc.utobj().generateTestData(dataSet.get("suffix"));
			String jobTitle = fc.utobj().generateTestData(dataSet.get("jobTitle"));
			String comment = fc.utobj().generateTestData(dataSet.get("comment"));
			String franchiseId = "";
			String franchiseUser = "";
			String userName = "";
			userNameReg = userNameReg + " " + userNameReg;
			fc.utobj().printTestStep("Navigate To CRM > Leads > Leads Summary");
			fc.utobj().printTestStep("Add Lead");
			addLead(driver, dataSet, firstName, lastName, company, assignTo, city, address, suffix, jobTitle, comment,
					userName, regionName, userNameReg, franchiseId, franchiseUser, config);

			fc.utobj().clickElement(driver, pobj.leadsLink);
			searchLeadByOwner(driver, userNameReg, "Open Leads");

			fc.utobj().printTestStep("Change Status");

			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,(".//a[contains(text () ,'" + firstName + " " + lastName + "')]/ancestor::tr/td/input")));
			fc.utobj().clickElement(driver, pobj.changeStatus);

			fc.commonMethods().switch_cboxIframe_frameId(driver);
			// verify status
			fc.utobj().printTestStep("Verify Status");
			String selectedVal = fc.utobj().getElementByXpath(driver, ".//*[@id='statusChangeTo']/option[@selected='']")
					.getText();

			if (!selectedVal.equalsIgnoreCase("New")) {
				fc.utobj().throwsException("was not able to verify selected status");
			}

			fc.utobj().selectDropDown(driver, pobj.statusChangeTo, statusName);
			String remarks = fc.utobj().generateTestData(dataSet.get("remarks"));
			fc.utobj().sendKeys(driver, pobj.remarks, remarks);
			fc.utobj().clickElement(driver, pobj.changeBtn);
			fc.utobj().clickElement(driver, pobj.closeBtn);
			fc.utobj().switchFrameToDefault(driver);
			searchLeadByOwner(driver, userNameReg, "Open Leads");

			fc.utobj().printTestStep("Verify Change Status");
			boolean isStatusPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[.='" + firstName + " " + lastName
					+ "']/ancestor::tr/td[contains(text() , '" + statusName + "')]");
			if (isStatusPresent == false) {
				fc.utobj().throwsException("was not able to verify status at Lead summary Page");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "crm" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Lead Associate With email campaign By Bottom Button At CRM > Lead > Lead Summary ", testCaseId = "TC_75_Associate_Email_Campaign")
	public void associateWithEmailCampaignBottomBtn()
			throws IOException, Exception, ParserConfigurationException, SAXException {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			CRMLeadsPage pobj = new CRMLeadsPage(driver);

			fc.utobj().printTestStep("Navigate To CRM >Campaign Center");
			fc.utobj().printTestStep("Add Campaign");
			CRMCampaignCenterPageTestForCRM campaignCenterPage = new CRMCampaignCenterPageTestForCRM();
			String campaignName = fc.utobj().generateTestData(dataSet.get("campaignName"));
			String campaignDescription = fc.utobj().generateTestData(dataSet.get("campaignDescription"));
			String campaignAccessibility = dataSet.get("campaignAccessibility");
			String category = dataSet.get("category");
			String templateName = fc.utobj().generateTestData(dataSet.get("templateName"));
			String mailSubject = dataSet.get("mailSubject");
			String templateText = fc.utobj().generateTestData(dataSet.get("templateText"));
			campaignCenterPage.createCampaign(driver, campaignName, campaignDescription, campaignAccessibility,
					category, templateName, mailSubject, templateText, config);

			/*
			 * fc.utobj().printTestStep(testCaseId,
			 * "Navigate To Admin Users Manage Corporate User");
			 * fc.utobj().printTestStep(testCaseId, "Add Corporate User");
			 * String emailId="crmautomation@staffex.com";
			 * AdminUsersManageCorporateUsersAddCorporateUserPageTest
			 * corporateUser=new
			 * AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			 * CorporateUser corpUser = new
			 * CorporateUser();corpUser.setEmail(emailId);corpUser =
			 * fc.CommonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			 * corporateUser.
			 * createDefaultCorporateUser_Through_WebService_AfterLogin(driver,
			 * corpUser);
			 */

			fc.utobj().printTestStep("Navigate To Admin Users Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			String emailId = "crmautomation@staffex.com";
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUser = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporateUser.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			String firstName = fc.utobj().generateTestData(dataSet.get("firstName"));
			String lastName = fc.utobj().generateTestData(dataSet.get("lastName"));
			String company = fc.utobj().generateTestData(dataSet.get("company"));
			String assignTo = "Corporate";
			String city = fc.utobj().generateTestData(dataSet.get("city"));
			String address = fc.utobj().generateTestData(dataSet.get("address"));
			String suffix = fc.utobj().generateTestData(dataSet.get("suffix"));
			String jobTitle = fc.utobj().generateTestData(dataSet.get("jobTitle"));
			String comment = fc.utobj().generateTestData(dataSet.get("comment"));
			String franchiseId = "";
			String franchiseUser = "";
			String userNameReg = "";
			String regionName = "";
			fc.utobj().printTestStep("Navigate To CRM > Leads > Leads Summary");
			fc.utobj().printTestStep("Add Lead");
			addLead(driver, dataSet, firstName, lastName, company, assignTo, city, address, suffix, jobTitle, comment,
					corpUser.getuserFullName(), regionName, userNameReg, franchiseId, franchiseUser, config);

			fc.utobj().clickElement(driver, pobj.leadsLink);
			searchLeadByOwner(driver, corpUser.getuserFullName(), "Open Leads");

			fc.utobj().printTestStep("Associate Lead With Campaign");

			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,(".//a[contains(text () ,'" + firstName + " " + lastName + "')]/ancestor::tr/td/input")));
			fc.utobj().clickElement(driver, pobj.associateWitCampaignBottomBtn);
			fc.utobj().clickElement(driver, pobj.filterContainer);
			fc.utobj().sendKeys(driver, pobj.searchCampaign, campaignName);
			fc.utobj().clickElement(driver, pobj.applyFilter);

			fc.utobj().clickElement(driver, pobj.sendCampaignBtn);
			fc.utobj().clickElement(driver, pobj.confirmBtn);
			fc.utobj().clickElement(driver, pobj.leadsLink);
			searchLeadByOwner(driver, corpUser.getuserFullName(), "Open Leads");
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//a[.='" + firstName + " " + lastName + "']"));

			fc.utobj().printTestStep("Verify Lead");

			boolean isCampaignNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () ,'Promotional Campaign')]/following-sibling::td[.='" + campaignName + "']");
			if (isCampaignNamePresent == false) {
				fc.utobj().throwsException("was not able to verify Campign Name");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "crm")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Change Owner of Lead By Bottom More Action Button Option At CRM > Lead > Lead Summary > Add New Contact", testCaseId = "TC_76_Change_Owner")
	public void changeOwnerBottomMoreActionBtn()
			throws IOException, Exception, ParserConfigurationException, SAXException {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			CRMLeadsPage pobj = new CRMLeadsPage(driver);

			/*
			 * fc.utobj().printTestStep(testCaseId,
			 * "Navigate To Admin Users Manage Corporate User");
			 * fc.utobj().printTestStep(testCaseId, "Add Corporate User");
			 * String emailId="crmautomation@staffex.com";
			 * AdminUsersManageCorporateUsersAddCorporateUserPageTest
			 * corporateUser=new
			 * AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			 * CorporateUser corpUser = new
			 * CorporateUser();corpUser.setEmail(emailId);corpUser =
			 * fc.CommonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			 * corporateUser.
			 * createDefaultCorporateUser_Through_WebService_AfterLogin(driver,
			 * corpUser);
			 */

			fc.utobj().printTestStep("Navigate To Admin Users Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			String emailId = "crmautomation@staffex.com";
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUser = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporateUser.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			// add Franchise user
			fc.utobj().printTestStep("Navigate To Admin > Franchise Location > Add Franchise Location");
			AdminFranchiseLocationAddFranchiseLocationPageTest franchiseLocation = new AdminFranchiseLocationAddFranchiseLocationPageTest();
			String franchiseId1 = fc.utobj().generateTestData(dataSet.get("franchiseId"));
			String regionName1 = fc.utobj().generateTestData(dataSet.get("regionName"));
			String firstNameF = fc.utobj().generateTestData(dataSet.get("firstNameFid"));
			String lastNameF = fc.utobj().generateTestData(dataSet.get("lastNameFid"));
			franchiseLocation.addFranchiseLocationForMkt(driver, franchiseId1, regionName1, firstNameF, lastNameF);

			// Add Franchise User
			fc.utobj().printTestStep("Admin > Users > Manage Franchise Users");
			fc.utobj().printTestStep("Add Franchise User");
			AdminUsersManageManageFranchiseUsersPageTest addFranUser = new AdminUsersManageManageFranchiseUsersPageTest();
			String userNameF = fc.utobj().generateTestData(dataSet.get("userNameF"));
			String roleName = "Default Franchise Role";
			String password = "t0n1ght@123";

			addFranUser.addFranchiseUser(driver, userNameF, password, franchiseId1, roleName, emailId);

			String firstName = fc.utobj().generateTestData(dataSet.get("firstName"));
			String lastName = fc.utobj().generateTestData(dataSet.get("lastName"));
			String company = fc.utobj().generateTestData(dataSet.get("company"));
			String assignTo = "Corporate";
			String city = fc.utobj().generateTestData(dataSet.get("city"));
			String address = fc.utobj().generateTestData(dataSet.get("address"));
			String suffix = fc.utobj().generateTestData(dataSet.get("suffix"));
			String jobTitle = fc.utobj().generateTestData(dataSet.get("jobTitle"));
			String comment = fc.utobj().generateTestData(dataSet.get("comment"));
			String franchiseId = "";
			String franchiseUser = "";
			String regionName = "";
			String userNameReg = "";
			fc.utobj().printTestStep("Navigate To CRM > Leads > Leads Summary");
			fc.utobj().printTestStep("Add Lead");
			addLead(driver, dataSet, firstName, lastName, company, assignTo, city, address, suffix, jobTitle, comment,
					corpUser.getuserFullName(), regionName, userNameReg, franchiseId, franchiseUser, config);

			fc.utobj().clickElement(driver, pobj.leadsLink);
			searchLeadByOwner(driver, corpUser.getuserFullName(), "Open Leads");

			fc.utobj().printTestStep("Chane Owner");

			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,(".//a[contains(text () ,'" + firstName + " " + lastName + "')]/ancestor::tr/td/input")));

			fc.utobj().selectMoreActionMenuItemsWithTagA(driver, "Change Owner");

			fc.commonMethods().switch_cboxIframe_frameId(driver);
			if (!fc.utobj().isSelected(driver, pobj.selectFranchiseAtCbox)) {
				fc.utobj().clickElement(driver, pobj.selectFranchiseAtCbox);
			}
			fc.utobj().selectDropDown(driver, pobj.selectFranchiseId, franchiseId1);
			fc.utobj().selectDropDown(driver, pobj.franchiseUserSelect, firstNameF + " " + lastNameF);
			fc.utobj().clickElement(driver, pobj.assignBtn);
			fc.utobj().clickElement(driver, pobj.closeBtn);
			fc.utobj().switchFrameToDefault(driver);

			searchLeadByOwner(driver, firstNameF + " " + lastNameF, "Open Leads");

			fc.utobj().printTestStep("Verify Change Owner");
			boolean isLeadNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//*[.='" + firstName + " " + lastName + "']");
			if (isLeadNamePresent == false) {
				fc.utobj().throwsException("was not able to verify Lead Name");
			}
			boolean isFranchiseIdPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//*[.='" + firstName + " " + lastName
					+ "']/ancestor::tr/td[contains(text () ,'" + franchiseId1 + "')]");
			if (isFranchiseIdPresent == false) {
				fc.utobj().throwsException("was not able to verify Franchise Id");
			}
			boolean isOwnerPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//*[.='" + firstName + " " + lastName
					+ "']/ancestor::tr/td[contains(text () ,'" + firstNameF + " " + lastNameF + "')]");
			if (isOwnerPresent == false) {
				fc.utobj().throwsException("was not able to verify owner");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "crm")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify the Lead Add To Group At CRM > Lead > Lead Summary", testCaseId = "TC_78_Add_To_Group")
	public void addToGroupBottomMoreActionBtn()
			throws IOException, Exception, ParserConfigurationException, SAXException {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			CRMLeadsPage pobj = new CRMLeadsPage(driver);
			CRMGroupsPageTest groupPage = new CRMGroupsPageTest();

			fc.utobj().printTestStep("Navigate To Admin Users Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			String emailId = "crmautomation@staffex.com";
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUser = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporateUser.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			String firstName = fc.utobj().generateTestData(dataSet.get("firstName"));
			String lastName = fc.utobj().generateTestData(dataSet.get("lastName"));
			String company = fc.utobj().generateTestData(dataSet.get("company"));
			String assignTo = "Corporate";
			String city = fc.utobj().generateTestData(dataSet.get("city"));
			String address = fc.utobj().generateTestData(dataSet.get("address"));
			String suffix = fc.utobj().generateTestData(dataSet.get("suffix"));
			String jobTitle = fc.utobj().generateTestData(dataSet.get("jobTitle"));
			String comment = fc.utobj().generateTestData(dataSet.get("comment"));
			String franchiseId = "";
			String franchiseUser = "";
			String userNameReg = "";
			String regionName = "";
			fc.utobj().printTestStep("Navigate To CRM > Leads > Leads Summary");
			fc.utobj().printTestStep("Add Lead");
			addLead(driver, dataSet, firstName, lastName, company, assignTo, city, address, suffix, jobTitle, comment,
					corpUser.getuserFullName(), regionName, userNameReg, franchiseId, franchiseUser, config);

			fc.utobj().clickElement(driver, pobj.leadsLink);
			searchLeadByOwner(driver, corpUser.getuserFullName(), "Open Leads");
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,(".//a[contains(text () ,'" + firstName + " " + lastName + "')]/ancestor::tr/td/input")));

			fc.utobj().printTestStep("Add To Group");

			fc.utobj().selectMoreActionMenuItemsWithTagA(driver, "Add To Group");
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().clickElement(driver, pobj.addToNewGroup);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Create New Group");
			String groupName = fc.utobj().generateTestData(dataSet.get("groupName"));
			String accessibility = "Public to all Users";
			groupPage.addToGroupRegular(driver, groupName, accessibility);

			new CRMGroupsPageTest().searchGroupsByFilter(driver, groupName, "Lead");
			/*
			 * fc.utobj().sendKeys(driver, pobj.searchAtGroup, groupName);
			 * fc.utobj().clickElement(driver, pobj.exactSearch);
			 * fc.utobj().clickElement(driver, pobj.systemSearchButton);
			 */

			String colunmName = "";
			int z = 0;

			// click over Recipients
			List<WebElement> listHead = driver
					.findElements(By.xpath(".//a[.='" + groupName + "']/ancestor::table/thead/tr/th"));
			System.out.println(listHead);
			for (int i = 0; i < listHead.size(); i++) {

				colunmName = listHead.get(i).getText().trim();
				if (colunmName.equalsIgnoreCase("Recipients")) {
					z = i;
					break;
				}
			}

			if (z != 0) {
				z = z + 1;
				fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
						".//a[.='" + groupName + "']/ancestor::tr/td[" + z + "]/a"));
			} else {
				fc.utobj().throwsException("was not able to find Recipients Column");
			}
			searchLeadByOwner(driver, corpUser.getuserFullName(), "All Leads");

			fc.utobj().printTestStep("Verify Lead In Group");

			boolean isLeadNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[.='" + firstName + " " + lastName + "']");
			if (isLeadNamePresent == false) {
				fc.utobj().throwsException("was not able to add lead in group");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "crm")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify the Add Account At CRM > Lead > Lead Summary", testCaseId = "TC_79_Add_Account")
	public void addAccount() throws IOException, Exception, ParserConfigurationException, SAXException {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			CRMLeadsPage pobj = new CRMLeadsPage(driver);

			AdminCRMConfigureIndustryPageTest industryPage = new AdminCRMConfigureIndustryPageTest();
			String industry = fc.utobj().generateTestData(dataSet.get("industry"));
			String description = fc.utobj().generateTestData(dataSet.get("description"));
			fc.utobj().printTestStep("Navigating to Admin > CRM >  Configure Industry Type");
			fc.utobj().printTestStep("Add Industry");
			industryPage.addIndustry(driver, industry, description);

			fc.utobj().printTestStep("Navigate To CRM >Lead > Lead Summary");

			fc.crm().crm_common().CRMLeadsLnk(driver);
			fc.utobj().clickElement(driver, pobj.addNew);
			fc.utobj().clickElement(driver, pobj.accountLnk);
			String accountName = fc.utobj().generateTestData(dataSet.get("accountName"));
			fc.utobj().sendKeys(driver, pobj.accountName, accountName);
			if (!fc.utobj().isSelected(driver, pobj.B2BAccountType)) {
				fc.utobj().clickElement(driver, pobj.B2BAccountType);
			}
			fc.utobj().selectDropDown(driver, pobj.visibilityType, "All Network");
			if (fc.utobj().isSelected(driver, pobj.isParent)) {
				fc.utobj().clickElement(driver, pobj.isParent);
			}
			fc.utobj().sendKeys(driver, pobj.address, "AddressTest");
			fc.utobj().sendKeys(driver, pobj.city, "City");
			fc.utobj().selectDropDown(driver, pobj.country, "USA");
			fc.utobj().sendKeys(driver, pobj.zipcode, "123455");
			fc.utobj().selectDropDown(driver, pobj.state, "Alabama");
			fc.utobj().sendKeys(driver, pobj.phoneNumbers, "1236547896");
			fc.utobj().sendKeys(driver, pobj.extn, "123");
			fc.utobj().sendKeys(driver, pobj.faxNumbers, "1236654");
			fc.utobj().sendKeys(driver, pobj.mobileNumbers, "1478523698");
			fc.utobj().sendKeys(driver, pobj.emailIds, "crmautomation@staffex.com");
			fc.utobj().sendKeys(driver, pobj.noOfEmployee, "500");
			fc.utobj().selectDropDown(driver, pobj.industry, industry);
			fc.utobj().sendKeys(driver, pobj.sicCode, "SicCode");
			fc.utobj().sendKeys(driver, pobj.servicesProvided, "Service123");
			fc.utobj().sendKeys(driver, pobj.stockSymbol, "qwerty");

			if (!fc.utobj().isSelected(driver, pobj.addContactNo)) {
				fc.utobj().clickElement(driver, pobj.addContactNo);
			}
			fc.utobj().clickElement(driver, pobj.saveBtn);

			fc.utobj().printTestStep("Verify Lead At Lead Page");

			boolean isAccountNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () ,'Account Name')]/ancestor::tr/td[.='" + accountName + "']");
			if (isAccountNamePresent == false) {
				fc.utobj().throwsException("was not able to add Account");
			}

			boolean isIndustyPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () ,'Industry')]/ancestor::tr/td[.='" + industry + "']");
			if (isIndustyPresent == false) {
				fc.utobj().throwsException("was not able to Verify Industry");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "crm")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify the Add Contact At CRM > Lead > Lead Summary", testCaseId = "TC_80_Add_Contact")
	public void addContact() throws IOException, Exception, ParserConfigurationException, SAXException {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			CRMLeadsPage pobj = new CRMLeadsPage(driver);

			CRMContactsPageTest contactsPage = new CRMContactsPageTest();

			/*
			 * fc.utobj().printTestStep(testCaseId,
			 * "Navigate To Admin Users Manage Corporate User");
			 * fc.utobj().printTestStep(testCaseId, "Add Corporate User");
			 * String emailId="crmautomation@staffex.com";
			 * AdminUsersManageCorporateUsersAddCorporateUserPageTest
			 * corporateUser=new
			 * AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			 * CorporateUser corpUser = new
			 * CorporateUser();corpUser.setEmail(emailId);corpUser =
			 * fc.CommonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			 * corporateUser.
			 * createDefaultCorporateUser_Through_WebService_AfterLogin(driver,
			 * corpUser);
			 */

			fc.utobj().printTestStep("Navigate To Admin Users Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			String emailId = "crmautomation@staffex.com";
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUser = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporateUser.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			AdminCRMContactTypeConfigurationPageTest contatcTypePage = new AdminCRMContactTypeConfigurationPageTest();
			String contactType = fc.utobj().generateTestData(dataSet.get("contactType"));
			contatcTypePage.addContactType(driver, contactType);

			fc.utobj().printTestStep("Navigating to CRM >  Leads > Leads Summary");
			fc.utobj().printTestStep("Add Contact");

			fc.crm().crm_common().CRMLeadsLnk(driver);
			fc.utobj().clickElement(driver, pobj.addNew);
			fc.utobj().clickElement(driver, pobj.contactLnk);

			String firstName = fc.utobj().generateTestData(dataSet.get("firstName"));
			String lastName = fc.utobj().generateTestData(dataSet.get("lastName"));
			String primaryCMethod = dataSet.get("primaryCMethod");
			String assignTo = "Corporate";
			String regionName = "";
			String franchiseId = "";
			String title = dataSet.get("title");
			String email = "automation@gmail.com";

			contactsPage.addContactGeneric(driver, contactType, firstName, lastName, primaryCMethod, assignTo,
					corpUser.getuserFullName(), regionName, franchiseId, title, email, config);
			// fc.utobj().clickElement(driver, pobj.saveBtn);

			fc.utobj().printTestStep("Verify Contatct");

			boolean isContactTypePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () ,'Contact Type')]/ancestor::tr/td[.='" + contactType + "']");
			if (isContactTypePresent == false) {
				fc.utobj().throwsException("was not able to verify Contact Type");
			}
			boolean isContactNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () ,'Name')]/ancestor::tr/td[.='Mr. " + firstName + " " + lastName + "']");
			if (isContactNamePresent == false) {
				fc.utobj().throwsException("was not able to verify Contact Name");
			}
			boolean isPrimaryContactTypePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () ,'Primary Contact Method')]/ancestor::tr/td[.='Email']");
			if (isPrimaryContactTypePresent == false) {
				fc.utobj().throwsException("was not able to verify Primary Contact Type");
			}
			boolean isUserPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () ,'Assign To ')]/ancestor::tr/td[contains(text () , '"
							+ corpUser.getuserFullName() + "')]");
			if (isUserPresent == false) {
				fc.utobj().throwsException("was not able to verify Assign To");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = {"crm"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify the Add Opportunity At CRM > Lead > Lead Summary", testCaseId = "TC_81_Add_Opportunity")
	public void addOpportunity() throws IOException, Exception, ParserConfigurationException, SAXException {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			CRMLeadsPage pobj = new CRMLeadsPage(driver);

			/*
			 * fc.utobj().printTestStep(testCaseId,
			 * "Navigate To Admin Users Manage Corporate User");
			 * fc.utobj().printTestStep(testCaseId, "Add Corporate User");
			 * 
			 * AdminUsersManageCorporateUsersAddCorporateUserPageTest
			 * addCorporatePage = new
			 * AdminUsersManageCorporateUsersAddCorporateUserPageTest(); String
			 * userName = fc.utobj().generateTestData(dataSet.get("userName"));
			 * String emailId="crmautomation@staffex.com"; userName =
			 * addCorporatePage.addCorporateUser(driver, userName, config ,
			 * emailId);
			 */

			fc.utobj().printTestStep("Navigate To Admin Users Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			String emailId = "crmautomation@staffex.com";
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUser = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporateUser.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			AdminCRMContactTypeConfigurationPageTest contatcTypePage = new AdminCRMContactTypeConfigurationPageTest();
			String contactType = fc.utobj().generateTestData(dataSet.get("contactType"));
			contatcTypePage.addContactType(driver, contactType);

			fc.utobj().printTestStep("Navigate To CRM > Leads > Leads Summary");
			fc.utobj().printTestStep("Add Contact");
			fc.crm().crm_common().CRMLeadsLnk(driver);
			fc.utobj().clickElement(driver, pobj.addNew);
			fc.utobj().clickElement(driver, pobj.contactLnk);
			fc.utobj().selectDropDown(driver, pobj.contactType, contactType);
			fc.utobj().selectDropDown(driver, pobj.title, "Mr.");
			String firstName = fc.utobj().generateTestData(dataSet.get("firstName"));
			fc.utobj().sendKeys(driver, pobj.contactFirstName, firstName);
			String lastName = fc.utobj().generateTestData(dataSet.get("lastName"));
			fc.utobj().sendKeys(driver, pobj.contactLastName, lastName);
			fc.utobj().sendKeys(driver, pobj.suffix, "suffixTest");
			fc.utobj().sendKeys(driver, pobj.position, "TestJobTitle");
			fc.utobj().sendKeys(driver, pobj.address, "TestAddress");
			fc.utobj().sendKeys(driver, pobj.city, "TestCity");
			fc.utobj().selectDropDown(driver, pobj.country, "USA");
			fc.utobj().selectDropDown(driver, pobj.state, "Alabama");
			fc.utobj().selectDropDown(driver, pobj.primaryContactMethodSelect, "Email");
			fc.utobj().sendKeys(driver, pobj.bestTimeToContact, "12:00 PM EST");
			fc.utobj().sendKeys(driver, pobj.zipcode, "12345");
			fc.utobj().sendKeys(driver, pobj.phoneNumbers, "1236547896");
			fc.utobj().sendKeys(driver, pobj.extn, "12");
			fc.utobj().sendKeys(driver, pobj.faxNumbers, "1263985487");
			fc.utobj().sendKeys(driver, pobj.mobileNumbers, "8896724441");
			fc.utobj().sendKeys(driver, pobj.emailIds, "crmautomation@staffex.com");
			fc.utobj().sendKeys(driver, pobj.alternateEmail, "test@gmail.com");
			fc.utobj().sendKeys(driver, pobj.birthdate, "11/24/1980");
			fc.utobj().sendKeys(driver, pobj.anniversarydate, "11/24/1999");

			fc.utobj().selectDropDown(driver, pobj.rating, "Cold");

			if (!fc.utobj().isSelected(driver, pobj.assignToCorporate)) {
				fc.utobj().clickElement(driver, pobj.assignToCorporate);
			}
			fc.utobj().selectDropDown(driver, pobj.selectCorporateUserContact, corpUser.getuserFullName());
			String comment = fc.utobj().generateTestData(dataSet.get("comment"));
			fc.utobj().sendKeys(driver, pobj.comments, comment);
			fc.utobj().clickElement(driver, pobj.saveBtn);

			boolean isContactTypePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () ,'Contact Type')]/ancestor::tr/td[.='" + contactType + "']");
			if (isContactTypePresent == false) {
				fc.utobj().throwsException("was not able to verify Contact Type");
			}
			boolean isContactNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () ,'Name')]/ancestor::tr/td[.='Mr. " + firstName + " " + lastName + "']");
			if (isContactNamePresent == false) {
				fc.utobj().throwsException("was not able to verify Contact Name");
			}
			boolean isPrimaryContactTypePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () ,'Primary Contact Method')]/ancestor::tr/td[.='Email']");
			if (isPrimaryContactTypePresent == false) {
				fc.utobj().throwsException("was not able to verify Primary Contact Type");
			}
			boolean isUserPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () ,'Assign To ')]/ancestor::tr/td[contains(text () , '"
							+ corpUser.getuserFullName() + "')]");
			if (isUserPresent == false) {
				fc.utobj().throwsException("was not able to verify Assign To");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "crm")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify the Modify Lead At CRM > Lead > Lead Info", testCaseId = "TC_82_Modify_Lead")
	public void modifyLeadTab() throws IOException, Exception, ParserConfigurationException, SAXException {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());
		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			CRMLeadsPage pobj = new CRMLeadsPage(driver);

			/*
			 * fc.utobj().printTestStep(testCaseId,
			 * "Navigate To Admin Users Manage Corporate User");
			 * fc.utobj().printTestStep(testCaseId, "Add Corporate User");
			 * String emailId="crmautomation@staffex.com";
			 * AdminUsersManageCorporateUsersAddCorporateUserPageTest
			 * corporateUser=new
			 * AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			 * CorporateUser corpUser = new
			 * CorporateUser();corpUser.setEmail(emailId);corpUser =
			 * fc.CommonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			 * corporateUser.
			 * createDefaultCorporateUser_Through_WebService_AfterLogin(driver,
			 * corpUser);
			 */

			fc.utobj().printTestStep("Navigate To Admin Users Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			String emailId = "crmautomation@staffex.com";
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUser = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporateUser.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			fc.utobj().printTestStep("Navigating to Admin > CRM > Configure Status");
			fc.utobj().printTestStep("Add Status Lead Type");
			AdminCRMConfigureStatusPageTest statusPage = new AdminCRMConfigureStatusPageTest();
			String statusName = fc.utobj().generateTestData(dataSet.get("statusName"));
			statusPage.addStatus(driver, statusName);

			String firstName = fc.utobj().generateTestData(dataSet.get("firstName"));
			String lastName = fc.utobj().generateTestData(dataSet.get("lastName"));
			String company = fc.utobj().generateTestData(dataSet.get("company"));
			String assignTo = "Corporate";
			String city = fc.utobj().generateTestData(dataSet.get("city"));
			//String email = dataSet.get("emailId");
			String email=fc.utobj().generateTestData("crmautomation")+"@staffex.com";
			String address = fc.utobj().generateTestData(dataSet.get("address"));
			String suffix = fc.utobj().generateTestData(dataSet.get("suffix"));
			String jobTitle = fc.utobj().generateTestData(dataSet.get("jobTitle"));
			String comment = fc.utobj().generateTestData(dataSet.get("comment"));
			String regionName = "";
			String regionalUser = "";
			String franchiseId = "";
			String franchiseUser = "";

			fc.utobj().printTestStep("Navigate To CRM > Leads > Leads Summary");
			fc.utobj().printTestStep("Add Lead");

			addLead(driver, dataSet, firstName, lastName, company, assignTo, city, address, suffix, jobTitle, comment,
					corpUser.getuserFullName(), regionName, regionalUser, franchiseId, franchiseUser, config);

			fc.utobj().clickElement(driver, pobj.leadsLink);
			searchLeadByOwner(driver, corpUser.getuserFullName(), "Open Leads");
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//a[.='" + firstName + " " + lastName + "']"));

			fc.utobj().printTestStep("Modify Lead");

			fc.utobj().clickElement(driver, pobj.modifyTab);

			fc.utobj().selectDropDown(driver, pobj.title, dataSet.get("title"));
			fc.utobj().selectDropDown(driver, pobj.leadStatus, statusName);
			firstName = fc.utobj().generateTestData(dataSet.get("firstName"));
			fc.utobj().sendKeys(driver, pobj.leadFirstName, firstName);
			lastName = fc.utobj().generateTestData(dataSet.get("lastName"));
			fc.utobj().sendKeys(driver, pobj.leadLastName, lastName);
			company = fc.utobj().generateTestData(dataSet.get("company"));
			fc.utobj().sendKeys(driver, pobj.companyName, company);
			fc.utobj().selectDropDown(driver, pobj.primaryContactMethodSelect, dataSet.get("primaryContactMethod"));
			address = fc.utobj().generateTestData(dataSet.get("address"));
			fc.utobj().sendKeys(driver, pobj.address, address);
			city = fc.utobj().generateTestData(dataSet.get("city"));
			fc.utobj().sendKeys(driver, pobj.city, city);
			fc.utobj().selectDropDown(driver, pobj.country, "USA");
			fc.utobj().selectDropDown(driver, pobj.state, "Alabama");
			fc.utobj().sendKeys(driver, pobj.zipcode, "12345");
			fc.utobj().sendKeys(driver, pobj.phoneNumbers, "1236547896");
			fc.utobj().sendKeys(driver, pobj.extn, "12");
			fc.utobj().sendKeys(driver, pobj.faxNumbers, "1263985487");
			fc.utobj().sendKeys(driver, pobj.mobileNumbers, "8896724441");
			//email = dataSet.get("emailId");
			email=fc.utobj().generateTestData("crmautomation")+"@staffex.com";
			fc.utobj().sendKeys(driver, pobj.emailIds, email);
			fc.utobj().sendKeys(driver, pobj.alternateEmail, "test@gmail.com");
			fc.utobj().generateTestData(dataSet.get("suffix"));
			fc.utobj().sendKeys(driver, pobj.suffix, suffix);
			fc.utobj().generateTestData(dataSet.get("jobTitle"));
			fc.utobj().sendKeys(driver, pobj.position, jobTitle);
			fc.utobj().sendKeys(driver, pobj.birthdate, "11/24/1980");
			fc.utobj().sendKeys(driver, pobj.anniversarydate, "11/24/1999");

			fc.utobj().selectDropDown(driver, pobj.rating, dataSet.get("rating"));
			fc.utobj().sendKeys(driver, pobj.bestTimeToContact, "12:00 PM EST");
			fc.utobj().generateTestData(dataSet.get("comment"));
			fc.utobj().sendKeys(driver, pobj.comments, comment);
			fc.utobj().clickElement(driver, pobj.saveBtn);

			// verify
			fc.utobj().printTestStep("Verify Modify Lead");
			String statusText = fc.utobj().getText(driver,
					fc.utobj().getElementByXpath(driver, ".//*[@id='cmLeadStatusID']/option[@selected='']"));
			if (!statusText.equalsIgnoreCase(statusName)) {
				fc.utobj().throwsException("was not able to verify status");
			}
			boolean isFirstNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () ,'First Name')]/ancestor::tr/td[.='" + firstName + "']");
			if (isFirstNamePresent == false) {
				fc.utobj().throwsException("was not able to verify First Name of Contact");
			}
			boolean isLatsNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () ,'Last Name')]/ancestor::tr/td[.='" + lastName + "']");
			if (isLatsNamePresent == false) {
				fc.utobj().throwsException("was not able to verify Last Name of Contact");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "crm")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Log A Task At CRM > Lead > Lead Info", testCaseId = "TC_83_Log_A_Task")
	public void logATaskTab() throws IOException, Exception, ParserConfigurationException, SAXException {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			CRMLeadsPage pobj = new CRMLeadsPage(driver);

			/*
			 * fc.utobj().printTestStep(testCaseId,
			 * "Navigate To Admin Users Manage Corporate User");
			 * fc.utobj().printTestStep(testCaseId, "Add Corporate User");
			 * String emailId="crmautomation@staffex.com";
			 * AdminUsersManageCorporateUsersAddCorporateUserPageTest
			 * corporateUser=new
			 * AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			 * CorporateUser corpUser = new
			 * CorporateUser();corpUser.setEmail(emailId);corpUser =
			 * fc.CommonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			 * corporateUser.
			 * createDefaultCorporateUser_Through_WebService_AfterLogin(driver,
			 * corpUser);
			 */

			fc.utobj().printTestStep("Navigate To Admin Users Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			String emailId = "crmautomation@staffex.com";
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUser = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporateUser.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			String firstName = fc.utobj().generateTestData(dataSet.get("firstName"));
			String lastName = fc.utobj().generateTestData(dataSet.get("lastName"));
			String company = fc.utobj().generateTestData(dataSet.get("company"));
			String assignTo = "Corporate";
			String city = fc.utobj().generateTestData(dataSet.get("city"));
			String address = fc.utobj().generateTestData(dataSet.get("address"));
			String suffix = fc.utobj().generateTestData(dataSet.get("suffix"));
			String jobTitle = fc.utobj().generateTestData(dataSet.get("jobTitle"));
			String comment = fc.utobj().generateTestData(dataSet.get("comment"));
			String regionName = "";
			String regionalUser = "";
			String franchiseId = "";
			String franchiseUser = "";

			fc.utobj().printTestStep("Navigate To CRM > Leads > Leads Summary");
			fc.utobj().printTestStep("Add Lead");

			addLead(driver, dataSet, firstName, lastName, company, assignTo, city, address, suffix, jobTitle, comment,
					corpUser.getuserFullName(), regionName, regionalUser, franchiseId, franchiseUser, config);

			fc.utobj().clickElement(driver, pobj.leadsLink);
			searchLeadByOwner(driver, corpUser.getuserFullName(), "Open Leads");
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//a[.='" + firstName + " " + lastName + "']"));
			fc.utobj().printTestStep("Log A Task");

			fc.utobj().clickElement(driver, pobj.logATaskTab);

			fc.commonMethods().switch_cboxIframe_frameId(driver);
			if (!fc.utobj().isSelected(driver,pobj.radioOwner)) {
				fc.utobj().clickElement(driver, pobj.radioOwner);
			}
			String statusTask = dataSet.get("statusTask");
			fc.utobj().selectDropDown(driver, pobj.statusTask, statusTask);
			String subject = fc.utobj().generateTestData(dataSet.get("subject"));
			fc.utobj().sendKeys(driver, pobj.subject, subject);
			if (!fc.utobj().isSelected(driver, pobj.timeLessTask)) {
				fc.utobj().clickElement(driver, pobj.timeLessTask);
			}
			String priority = dataSet.get("priority");
			fc.utobj().selectDropDown(driver, pobj.priority, priority);
			comment = fc.utobj().generateTestData(dataSet.get("comment"));
			fc.utobj().sendKeys(driver, pobj.comments, comment);
			fc.utobj().clickElement(driver, pobj.createBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Verify Log A Task");
			boolean isSubjectPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[contains(text () ,'" + subject + "')]");
			if (isSubjectPresent == false) {
				fc.utobj().throwsException("was not able to verify subject");
			}
			boolean isUserPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[contains(text () ,'" + subject
					+ "')]/ancestor::tr/td/div[.='" + corpUser.getuserFullName() + "']");
			if (isUserPresent == false) {
				fc.utobj().throwsException("was not able to verify userName at Lead Info Page");
			}
			boolean isStatusOfTaskPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[contains(text () ,'" + subject
					+ "')]/ancestor::tr/td[contains(text () ,'" + statusTask + "')]");
			if (isStatusOfTaskPresent == false) {
				fc.utobj().throwsException("was not able to verify status of task");
			}

			boolean isPriorityPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[contains(text () ,'" + subject
					+ "')]/ancestor::tr/td[contains(text () ,'" + priority + "')]");
			if (isPriorityPresent == false) {
				fc.utobj().throwsException("was not able to verify Priority of task");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "crm", "crmemail" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify the Send Email By Send Email Tab At CRM > Lead > Lead Info", testCaseId = "TC_84_Send_Email")
	public void sendEmailTab() throws IOException, Exception, ParserConfigurationException, SAXException {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());
		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			CRMLeadsPage pobj = new CRMLeadsPage(driver);

			fc.utobj().printTestStep("Navigate To Admin Users Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			String emailId = "crmautomation@staffex.com";
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUser = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporateUser.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			String firstName = fc.utobj().generateTestData(dataSet.get("firstName"));
			String lastName = fc.utobj().generateTestData(dataSet.get("lastName"));
			String company = fc.utobj().generateTestData(dataSet.get("company"));
			String assignTo = "Corporate";
			String city = fc.utobj().generateTestData(dataSet.get("city"));
			String address = fc.utobj().generateTestData(dataSet.get("address"));
			String suffix = fc.utobj().generateTestData(dataSet.get("suffix"));
			String jobTitle = fc.utobj().generateTestData(dataSet.get("jobTitle"));
			String comment = fc.utobj().generateTestData(dataSet.get("comment"));
			String regionName = "";
			String regionalUser = "";
			String franchiseId = "";
			String franchiseUser = "";

			fc.utobj().printTestStep("Navigate To CRM > Leads > Leads Summary");
			fc.utobj().printTestStep("Add Lead");

			addLead(driver, dataSet, firstName, lastName, company, assignTo, city, address, suffix, jobTitle, comment,
					corpUser.getuserFullName(), regionName, regionalUser, franchiseId, franchiseUser, config);

			fc.utobj().clickElement(driver, pobj.leadsLink);
			searchLeadByOwner(driver, corpUser.getuserFullName(), "Open Leads");

			fc.utobj().printTestStep("Send Email");
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//a[.='" + firstName + " " + lastName + "']"));
			fc.utobj().clickElement(driver, pobj.sendEmailTab);

			if (!fc.utobj().isSelected(driver, pobj.loggedUserId)) {
				fc.utobj().clickElement(driver, pobj.loggedUserId);
			}

			/*String mailSentToId = fc.utobj().getText(driver, pobj.mailSentTo);
			if (!mailSentToId.equalsIgnoreCase(dataSet.get("emailId"))) {
				fc.utobj().throwsException("was not able to verify mailSentTo");
			}*/
			String mailCC = dataSet.get("mailCC");
			fc.utobj().sendKeys(driver, pobj.mailCC, mailCC);

			if (!fc.utobj().isSelected(driver,pobj.showBcc)) {
				fc.utobj().clickElement(driver, pobj.showBcc);
			}
			String mailBCC = dataSet.get("mailBCC");
			fc.utobj().sendKeys(driver, pobj.mailBCC, mailBCC);

			String subjectMail = fc.utobj().generateTestData(dataSet.get("subjectMail"));
			fc.utobj().sendKeys(driver, pobj.subjectMail, subjectMail);

			String editorText = fc.utobj().generateTestData(dataSet.get("mailText"));

			fc.utobj().switchFrameById(driver, "ta_ifr");

			Actions actions = new Actions(driver);
			actions.moveToElement(fc.utobj().getElement(driver, pobj.editorTextArea));
			actions.click();
			actions.sendKeys(editorText);
			fc.utobj().logReportMsg("Entered Text", editorText);
			actions.build().perform();
			fc.utobj().switchFrameToDefault(driver);
			fc.utobj().clickElement(driver, pobj.sendEmailBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Verify Email");
			String expectedSubject = subjectMail;
			String expectedMessageBody = editorText;
			Map<String, String> mailData = fc.utobj().readMailBox(expectedSubject, expectedMessageBody, emailId,
					"sdg@1a@Hfs");

			if (mailData.size() == 0 || !mailData.get("mailBody").contains(editorText)) {
				fc.utobj().throwsException("was not able to verify mail text");
			}
			if (mailData.size() == 0 || !mailData.get("subject").equalsIgnoreCase(subjectMail)) {
				fc.utobj().throwsException("was not able to verify subject mail");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "crm")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify the Change Owner of Lead At CRM > Lead > Lead Info", testCaseId = "TC_85_Change_Owner")
	public void changeOwnerMoreActionBtn() throws IOException, Exception, ParserConfigurationException, SAXException {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			CRMLeadsPage pobj = new CRMLeadsPage(driver);

			/*
			 * fc.utobj().printTestStep(testCaseId,
			 * "Navigate To Admin Users Manage Corporate User");
			 * fc.utobj().printTestStep(testCaseId, "Add Corporate User");
			 * String emailId="crmautomation@staffex.com";
			 * AdminUsersManageCorporateUsersAddCorporateUserPageTest
			 * corporateUser=new
			 * AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			 * CorporateUser corpUser = new
			 * CorporateUser();corpUser.setEmail(emailId);corpUser =
			 * fc.CommonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			 * corporateUser.
			 * createDefaultCorporateUser_Through_WebService_AfterLogin(driver,
			 * corpUser);
			 */

			fc.utobj().printTestStep("Navigate To Admin Users Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			String emailId = "crmautomation@staffex.com";
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUser = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporateUser.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			fc.utobj().printTestStep("Navigate To Admin > Franchise Location > Add Franchise Location");
			AdminFranchiseLocationAddFranchiseLocationPageTest franchiseLocation = new AdminFranchiseLocationAddFranchiseLocationPageTest();
			String franchiseId1 = fc.utobj().generateTestData(dataSet.get("franchiseId"));
			String regionName1 = fc.utobj().generateTestData(dataSet.get("regionName"));
			String firstNameF = fc.utobj().generateTestData(dataSet.get("firstNameFid"));
			String lastNameF = fc.utobj().generateTestData(dataSet.get("lastNameFid"));
			franchiseLocation.addFranchiseLocationForMkt(driver, franchiseId1, regionName1, firstNameF, lastNameF);

			fc.utobj().printTestStep("Admin > Users > Manage Franchise Users");
			fc.utobj().printTestStep("Add Franchise User");
			AdminUsersManageManageFranchiseUsersPageTest addFranUser = new AdminUsersManageManageFranchiseUsersPageTest();
			String userNameF = fc.utobj().generateTestData(dataSet.get("userNameF"));
			String roleName = "Default Franchise Role";
			String password = "t0n1ght@123";

			addFranUser.addFranchiseUser(driver, userNameF, password, franchiseId1, roleName, emailId);

			String firstName = fc.utobj().generateTestData(dataSet.get("firstName"));
			String lastName = fc.utobj().generateTestData(dataSet.get("lastName"));
			String company = fc.utobj().generateTestData(dataSet.get("company"));
			String assignTo = "Corporate";
			String city = fc.utobj().generateTestData(dataSet.get("city"));
			String address = fc.utobj().generateTestData(dataSet.get("address"));
			String suffix = fc.utobj().generateTestData(dataSet.get("suffix"));
			String jobTitle = fc.utobj().generateTestData(dataSet.get("jobTitle"));
			String comment = fc.utobj().generateTestData(dataSet.get("comment"));
			String franchiseId = "";
			String franchiseUser = "";
			String regionName = "";
			String userNameReg = "";

			fc.utobj().printTestStep("Navigate To CRM > Leads > Leads Summary");
			fc.utobj().printTestStep("Add Lead");

			addLead(driver, dataSet, firstName, lastName, company, assignTo, city, address, suffix, jobTitle, comment,
					corpUser.getuserFullName(), regionName, userNameReg, franchiseId, franchiseUser, config);

			fc.utobj().clickElement(driver, pobj.leadsLink);
			searchLeadByOwner(driver, corpUser.getuserFullName(), "Open Leads");

			fc.utobj().printTestStep("Change Owner");
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//a[.='" + firstName + " " + lastName + "']"));
			fc.utobj().selectActionMenuItemsWithTagAMoreActions(driver, "Change Owner");

			fc.commonMethods().switch_cboxIframe_frameId(driver);
			if (!fc.utobj().isSelected(driver, pobj.selectFranchiseAtCbox)) {
				fc.utobj().clickElement(driver, pobj.selectFranchiseAtCbox);
			}
			fc.utobj().selectDropDown(driver, pobj.selectFranchiseId, franchiseId1);
			fc.utobj().selectDropDown(driver, pobj.franchiseUserSelect, firstNameF + " " + lastNameF);
			fc.utobj().clickElement(driver, pobj.assignBtn);
			fc.utobj().clickElement(driver, pobj.closeBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Verify Change Owner");

			boolean isUserNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () ,'Assign To')]/ancestor::tr/td[contains(text () ,'" + firstNameF + " "
							+ lastNameF + "')]");
			if (isUserNamePresent == false) {
				fc.utobj().throwsException("was not able to verify user name");
			}

			fc.utobj().printTestStep("Verify Chnaege Owner At Detailed History Frame");
			fc.utobj().selectActionMenuItemsWithTagAMoreActions(driver, "View Detailed History");
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().clickElement(driver, pobj.ownerChange);

			boolean isOwnerFromPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () , 'Owner From')]/../following-sibling::tr[1]/td[contains(text () , '"
							+ corpUser.getuserFullName() + "')]");
			if (isOwnerFromPresent == false) {
				fc.utobj().throwsException("was not able to verify Owner From");
			}
			boolean isOwnerToPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () , 'Owner To')]/../following-sibling::tr[1]/td[contains(text () , '"
							+ firstNameF + " " + lastNameF + "')]");
			if (isOwnerToPresent == false) {
				fc.utobj().throwsException("was not able to verify Owner To");
			}
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "crm" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify the Associate with email campaign by More Action Btn :: CRM > Lead > Lead Info", testCaseId = "TC_86_Associate_With_Email_Campaign")
	public void associateWithEmailCampaignMoreActionBtn()
			throws IOException, Exception, ParserConfigurationException, SAXException {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			CRMLeadsPage pobj = new CRMLeadsPage(driver);

			fc.utobj().printTestStep("Navigate To CRM >Campaign Center");
			fc.utobj().printTestStep("Add Campaign");
			CRMCampaignCenterPageTestForCRM campaignCenterPage = new CRMCampaignCenterPageTestForCRM();
			String campaignName = fc.utobj().generateTestData(dataSet.get("campaignName"));
			String campaignDescription = fc.utobj().generateTestData(dataSet.get("campaignDescription"));
			String campaignAccessibility = dataSet.get("campaignAccessibility");
			String category = dataSet.get("category");
			String templateName = fc.utobj().generateTestData(dataSet.get("templateName"));
			String mailSubject = dataSet.get("mailSubject");
			String templateText = fc.utobj().generateTestData(dataSet.get("templateText"));
			campaignCenterPage.createCampaign(driver, campaignName, campaignDescription, campaignAccessibility,
					category, templateName, mailSubject, templateText, config);

			/*
			 * fc.utobj().printTestStep(testCaseId,
			 * "Navigate To Admin Users Manage Corporate User");
			 * fc.utobj().printTestStep(testCaseId, "Add Corporate User");
			 * String emailId="crmautomation@staffex.com";
			 * AdminUsersManageCorporateUsersAddCorporateUserPageTest
			 * corporateUser=new
			 * AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			 * CorporateUser corpUser = new
			 * CorporateUser();corpUser.setEmail(emailId);corpUser =
			 * fc.CommonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			 * corporateUser.
			 * createDefaultCorporateUser_Through_WebService_AfterLogin(driver,
			 * corpUser);
			 */

			fc.utobj().printTestStep("Navigate To Admin Users Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			String emailId = "crmautomation@staffex.com";
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUser = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporateUser.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			String firstName = fc.utobj().generateTestData(dataSet.get("firstName"));
			String lastName = fc.utobj().generateTestData(dataSet.get("lastName"));
			String company = fc.utobj().generateTestData(dataSet.get("company"));
			String assignTo = "Corporate";
			String city = fc.utobj().generateTestData(dataSet.get("city"));
			String address = fc.utobj().generateTestData(dataSet.get("address"));
			String suffix = fc.utobj().generateTestData(dataSet.get("suffix"));
			String jobTitle = fc.utobj().generateTestData(dataSet.get("jobTitle"));
			String comment = fc.utobj().generateTestData(dataSet.get("comment"));
			String franchiseId = "";
			String franchiseUser = "";
			String userNameReg = "";
			String regionName = "";

			fc.utobj().printTestStep("Navigate To CRM > Leads > Leads Summary");
			fc.utobj().printTestStep("Add Lead");

			addLead(driver, dataSet, firstName, lastName, company, assignTo, city, address, suffix, jobTitle, comment,
					corpUser.getuserFullName(), regionName, userNameReg, franchiseId, franchiseUser, config);

			fc.utobj().clickElement(driver, pobj.leadsLink);
			searchLeadByOwner(driver, corpUser.getuserFullName(), "Open Leads");
			fc.utobj().printTestStep("Associate With Campaign");

			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//a[.='" + firstName + " " + lastName + "']"));

			fc.utobj().selectActionMenuItemsWithTagAMoreActions(driver, "Associate with Campaign");
			fc.utobj().clickElement(driver, pobj.filterContainer);
			fc.utobj().sendKeys(driver, pobj.searchCampaign, campaignName);
			fc.utobj().clickElement(driver, pobj.applyFilter);

			fc.utobj().clickElement(driver, pobj.sendCampaignBtn);
			fc.utobj().clickElement(driver, pobj.confirmBtn);
			fc.utobj().clickElement(driver, pobj.leadsLink);
			searchLeadByOwner(driver, corpUser.getuserFullName(), "Open Leads");
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//a[.='" + firstName + " " + lastName + "']"));

			fc.utobj().printTestStep("Verify Associate With Campaign");
			boolean isCampaignNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () ,'Promotional Campaign')]/following-sibling::td[.='" + campaignName + "']");
			if (isCampaignNamePresent == false) {
				fc.utobj().throwsException("was not able to verify Campign Name");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "crm")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2017-07-04", testCaseDescription = "Verify the Archive Lead By More Action Btn At CRM > Lead > Lead Info", testCaseId = "TC_87_Archive_Lead")
	public void archiveLeadMoreActionBtn() throws IOException, Exception, ParserConfigurationException, SAXException {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			CRMLeadsPage pobj = new CRMLeadsPage(driver);

			fc.utobj().printTestStep("Navigate To Admin Users Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			String emailId = "crmautomation@staffex.com";
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUser = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporateUser.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			String firstName = fc.utobj().generateTestData(dataSet.get("firstName"));
			String lastName = fc.utobj().generateTestData(dataSet.get("lastName"));
			String company = fc.utobj().generateTestData(dataSet.get("company"));
			String assignTo = "Corporate";
			String city = fc.utobj().generateTestData(dataSet.get("city"));
			String address = fc.utobj().generateTestData(dataSet.get("address"));
			String suffix = fc.utobj().generateTestData(dataSet.get("suffix"));
			String jobTitle = fc.utobj().generateTestData(dataSet.get("jobTitle"));
			String comment = fc.utobj().generateTestData(dataSet.get("comment"));
			String franchiseId = "";
			String franchiseUser = "";
			String userNameReg = "";
			String regionName = "";

			fc.utobj().printTestStep("Navigate To CRM > Leads > Leads Summary");
			fc.utobj().printTestStep("Add Lead");

			addLead(driver, dataSet, firstName, lastName, company, assignTo, city, address, suffix, jobTitle, comment,
					corpUser.getuserFullName(), regionName, userNameReg, franchiseId, franchiseUser, config);

			fc.utobj().clickElement(driver, pobj.leadsLink);
			searchLeadByOwner(driver, corpUser.getuserFullName(), "Open Leads");
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//a[contains(text () ,'" + firstName + " " + lastName + "')]"));

			fc.utobj().printTestStep("Archive Lead");
			fc.utobj().selectActionMenuItemsWithTagAMoreActions(driver, "Archive Lead");

			searchLeadByOwner(driver, corpUser.getuserFullName(), "All Leads");

			fc.utobj().printTestStep("Verify Archive Lead At Leads Page");

			// boolean isNoRecordPresent = fc.utobj().verifyCase(driver,
			// ".//td[.='No records found.']");

			boolean isNoRecordPresent = fc.utobj().assertPageSource(driver, firstName);

			if (isNoRecordPresent == true) {
				fc.utobj().throwsException("Archive Lead Present At Lead Summary Page");
			}

			fc.utobj().clickElement(driver, pobj.searchLink);
			fc.utobj().clickElement(driver, pobj.leadsTab);
			if (!fc.utobj().isSelected(driver, pobj.archiveLeads)) {
				fc.utobj().clickElement(driver, pobj.archiveLeads);
			}
			fc.utobj().setToDefault(driver, pobj.selectAssignTo);
			WebElement elementInput = fc.utobj().getElement(driver, pobj.selectAssignTo)
					.findElement(By.xpath("./div/div/input"));
			fc.utobj().selectValFromMultiSelect(driver, pobj.selectAssignTo, elementInput, corpUser.getuserFullName());
			fc.utobj().clickElement(driver, pobj.searchBtnAtSearch);

			fc.utobj().printTestStep("Verify Archive Lead Search Page");

			boolean isNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[.='" + firstName + " " + lastName + "']");

			if (isNamePresent == false) {
				fc.utobj().throwsException("was not able to verify Archive Leads");
			}

			boolean isOwnerPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[.='" + firstName + " " + lastName
					+ "']/ancestor::tr/td[contains(text () , '" + corpUser.getuserFullName() + "')]");

			if (isOwnerPresent == false) {
				fc.utobj().throwsException("was not able to verify Owner of Archive Lead");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "crm", "crmodulewsd" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2017-07-03", testCaseDescription = "Verify the Delete Lead By More Action Btn At CRM > Lead > Lead Info", testCaseId = "TC_88_Delete_Lead")
	public void deleteLeadMoreActionBtn() throws IOException, Exception, ParserConfigurationException, SAXException {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());
		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			CRMLeadsPage pobj = new CRMLeadsPage(driver);

			fc.utobj().printTestStep("Navigate To Admin Users Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			String emailId = "crmautomation@staffex.com";
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUser = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporateUser.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			String firstName = fc.utobj().generateTestData(dataSet.get("firstName"));
			String lastName = fc.utobj().generateTestData(dataSet.get("lastName"));
			String company = fc.utobj().generateTestData(dataSet.get("company"));
			String assignTo = "Corporate";
			String city = fc.utobj().generateTestData(dataSet.get("city"));
			String address = fc.utobj().generateTestData(dataSet.get("address"));
			String suffix = fc.utobj().generateTestData(dataSet.get("suffix"));
			String jobTitle = fc.utobj().generateTestData(dataSet.get("jobTitle"));
			String comment = fc.utobj().generateTestData(dataSet.get("comment"));
			String franchiseId = "";
			String franchiseUser = "";
			String userNameReg = "";
			String regionName = "";

			fc.utobj().printTestStep("Navigate To CRM > Leads > Leads Summary");
			fc.utobj().printTestStep("Add Lead");

			addLead(driver, dataSet, firstName, lastName, company, assignTo, city, address, suffix, jobTitle, comment,
					corpUser.getuserFullName(), regionName, userNameReg, franchiseId, franchiseUser, config);

			fc.utobj().clickElement(driver, pobj.leadsLink);
			fc.utobj().sendKeys(driver, pobj.searchLeads, firstName);
			fc.utobj().clickElement(driver, pobj.systemSearchButton);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//a[contains(text () ,'" + firstName + " " + lastName + "')]"));

			fc.utobj().printTestStep("Verify Delete Lead");
			fc.utobj().selectActionMenuItemsWithTagAMoreActions(driver, "Delete Lead");
			fc.utobj().acceptAlertBox(driver);

			fc.utobj().sendKeys(driver, pobj.searchLeads, firstName);
			fc.utobj().clickElement(driver, pobj.systemSearchButton);
			boolean isNoRecordPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//td[.='No records found.']");
			if (isNoRecordPresent == false) {
				fc.utobj().throwsException("was not able to Delete Lead");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "crm")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify the Add To Group By More Action Btn At CRM > Lead > Lead Info", testCaseId = "TC_89_Add_To_Group")
	public void addToGroupMoreActionBtn() throws IOException, Exception, ParserConfigurationException, SAXException {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			CRMLeadsPage pobj = new CRMLeadsPage(driver);
			CRMGroupsPageTest groupPage = new CRMGroupsPageTest();

			fc.utobj().printTestStep("Navigate To Admin Users Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			String emailId = "crmautomation@staffex.com";
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUser = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporateUser.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			String firstName = fc.utobj().generateTestData(dataSet.get("firstName"));
			String lastName = fc.utobj().generateTestData(dataSet.get("lastName"));
			String company = fc.utobj().generateTestData(dataSet.get("company"));
			String assignTo = "Corporate";
			String city = fc.utobj().generateTestData(dataSet.get("city"));
			String address = fc.utobj().generateTestData(dataSet.get("address"));
			String suffix = fc.utobj().generateTestData(dataSet.get("suffix"));
			String jobTitle = fc.utobj().generateTestData(dataSet.get("jobTitle"));
			String comment = fc.utobj().generateTestData(dataSet.get("comment"));
			String franchiseId = "";
			String franchiseUser = "";
			String userNameReg = "";
			String regionName = "";

			fc.utobj().printTestStep("Navigate To CRM > Leads > Leads Summary");
			fc.utobj().printTestStep("Add Lead");
			addLead(driver, dataSet, firstName, lastName, company, assignTo, city, address, suffix, jobTitle, comment,
					corpUser.getuserFullName(), regionName, userNameReg, franchiseId, franchiseUser, config);

			fc.utobj().clickElement(driver, pobj.leadsLink);
			searchLeadByOwner(driver, corpUser.getuserFullName(), "Open Leads");
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//a[contains(text () ,'" + firstName + " " + lastName + "')]"));

			fc.utobj().printTestStep("Add TO Group");
			fc.utobj().selectActionMenuItemsWithTagAMoreActions(driver, "Add To Group");

			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().clickElement(driver, pobj.addToNewGroup);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Create New Group");
			String groupName = fc.utobj().generateTestData(dataSet.get("groupName"));
			String accessibility = "Public to all Users";
			groupPage.addToGroupRegular(driver, groupName, accessibility);

			/*
			 * fc.utobj().sendKeys(driver, pobj.searchAtGroup, groupName);
			 * fc.utobj().clickElement(driver, pobj.exactSearch);
			 * fc.utobj().clickElement(driver, pobj.systemSearchButton);
			 */
			new CRMGroupsPageTest().searchGroupsByFilter(driver, groupName, "Lead");
			String colunmName = "";
			int z = 0;

			// click over Recipients
			List<WebElement> listHead = driver
					.findElements(By.xpath(".//a[.='" + groupName + "']/ancestor::table/thead/tr/th"));
			System.out.println(listHead);
			for (int i = 0; i < listHead.size(); i++) {

				colunmName = listHead.get(i).getText().trim();
				if (colunmName.equalsIgnoreCase("Recipients")) {
					z = i;
					break;
				}
			}

			if (z != 0) {
				z = z + 1;
				fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
						".//a[.='" + groupName + "']/ancestor::tr/td[" + z + "]/a"));
			} else {
				fc.utobj().throwsException("was not able to find Recipients Column");
			}

			searchLeadByOwner(driver, corpUser.getuserFullName(), "All Leads");
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//a[contains(text () ,'" + firstName + " " + lastName + "')]"));

			fc.utobj().printTestStep("Verify Lead At Group");
			boolean isGroupNamePresent = fc.utobj().assertPageSource(driver, groupName);
			if (isGroupNamePresent == false) {
				fc.utobj().throwsException("was not able to add in group");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "crm")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify the Add Remarks At CRM > Lead > Lead Info", testCaseId = "TC_90_Add_Remarks")
	public void addRemarksTab() throws IOException, Exception, ParserConfigurationException, SAXException {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			CRMLeadsPage pobj = new CRMLeadsPage(driver);

			fc.utobj().printTestStep("Navigate To Admin Users Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			String emailId = "crmautomation@staffex.com";
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUser = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporateUser.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			String firstName = fc.utobj().generateTestData(dataSet.get("firstName"));
			String lastName = fc.utobj().generateTestData(dataSet.get("lastName"));
			String company = fc.utobj().generateTestData(dataSet.get("company"));
			String assignTo = "Corporate";
			String city = fc.utobj().generateTestData(dataSet.get("city"));
			String address = fc.utobj().generateTestData(dataSet.get("address"));
			String suffix = fc.utobj().generateTestData(dataSet.get("suffix"));
			String jobTitle = fc.utobj().generateTestData(dataSet.get("jobTitle"));
			String comment = fc.utobj().generateTestData(dataSet.get("comment"));
			String franchiseId = "";
			String franchiseUser = "";
			String userNameReg = "";
			String regionName = "";

			fc.utobj().printTestStep("Navigate To CRM > Leads > Leads Summary");
			fc.utobj().printTestStep("Add Lead");

			addLead(driver, dataSet, firstName, lastName, company, assignTo, city, address, suffix, jobTitle, comment,
					corpUser.getuserFullName(), regionName, userNameReg, franchiseId, franchiseUser, config);

			fc.utobj().clickElement(driver, pobj.leadsLink);
			searchLeadByOwner(driver, corpUser.getuserFullName(), "Open Leads");

			fc.utobj().printTestStep("Add Remarks");
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//a[contains(text () ,'" + firstName + " " + lastName + "')]"));
			fc.utobj().clickElement(driver, pobj.addRemarks);

			String remarks = fc.utobj().generateTestData(dataSet.get("remarkField"));
			fc.utobj().sendKeys(driver, pobj.remarkField, remarks);
			fc.utobj().clickElement(driver, pobj.saveBtn);

			fc.utobj().printTestStep("Verify Remarks");
			boolean isRemarksPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[@title='Remark']/span[.='" + remarks + "']");
			if (isRemarksPresent == false) {
				fc.utobj().throwsException("was not able to verify remarks");
			}

			fc.utobj().printTestStep("Verify Remarks At Detailed History Frame");
			fc.utobj().clickElement(driver, pobj.detailedHistoryLink);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().clickElement(driver, pobj.remarksTabAtCbox);
			boolean isRemarksAtCboxPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () ,'" + remarks + "')]");
			if (isRemarksAtCboxPresent == false) {
				fc.utobj().throwsException("was not able to verify Remarks At Detailed History Cbox");
			}
			fc.utobj().clickElement(driver, pobj.closeBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = {"crm" ,"crmDateTest"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify the Log a Call At CRM > Lead > Lead Info", testCaseId = "TC_91_Log_A_Call")
	public void logACallTab() throws IOException, Exception, ParserConfigurationException, SAXException {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			CRMLeadsPage pobj = new CRMLeadsPage(driver);
			
			fc.utobj().printTestStep("Navigate To Admin Users Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			String emailId = "crmautomation@staffex.com";
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUser = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporateUser.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			String firstName = fc.utobj().generateTestData(dataSet.get("firstName"));
			String lastName = fc.utobj().generateTestData(dataSet.get("lastName"));
			String company = fc.utobj().generateTestData(dataSet.get("company"));
			String assignTo = "Corporate";
			String city = fc.utobj().generateTestData(dataSet.get("city"));
			String address = fc.utobj().generateTestData(dataSet.get("address"));
			String suffix = fc.utobj().generateTestData(dataSet.get("suffix"));
			String jobTitle = fc.utobj().generateTestData(dataSet.get("jobTitle"));
			String comment = fc.utobj().generateTestData(dataSet.get("comment"));
			String franchiseId = "";
			String franchiseUser = "";
			String userNameReg = "";
			String regionName = "";

			fc.utobj().printTestStep("Navigate To CRM > Leads > Leads Summary");
			fc.utobj().printTestStep("Add Lead");
			addLead(driver, dataSet, firstName, lastName, company, assignTo, city, address, suffix, jobTitle, comment,
					corpUser.getuserFullName(), regionName, userNameReg, franchiseId, franchiseUser, config);

			fc.utobj().clickElement(driver, pobj.leadsLink);
			searchLeadByOwner(driver, corpUser.getuserFullName(), "Open Leads");
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//a[contains(text () ,'" + firstName + " " + lastName + "')]"));

			fc.utobj().printTestStep("Log A Call");
			fc.utobj().clickElement(driver, pobj.logACall);
			
			String callSubject = fc.utobj().generateTestData(dataSet.get("subject"));
			fc.utobj().sendKeys(driver, pobj.callSubject, callSubject);
			
			String callDate=fc.utobj().getCurrentDateAndTime();
			fc.utobj().sendKeys(driver, pobj.callDate, callDate);
			
			String callStatus = dataSet.get("callStatus");
			fc.utobj().selectDropDown(driver, pobj.callStatus, callStatus);
			String cType = dataSet.get("callType");
			fc.utobj().selectDropDown(driver, pobj.communicationType, cType);
			comment = fc.utobj().generateTestData(dataSet.get("comment"));
			fc.utobj().sendKeys(driver, pobj.comments, comment);
			fc.utobj().clickElement(driver, pobj.saveBtn);

			fc.utobj().clickElement(driver, pobj.detailedHistoryLink);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().clickElement(driver, pobj.callsTabDH);

			fc.utobj().printTestStep("Verify Log A Call At Detailed History Tab");
			boolean isCallSubjectPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () ,'" + callSubject + "')]");
			if (isCallSubjectPresent == false) {
				fc.utobj().throwsException("was not able to verify Call Subject");
			}
			boolean isCommunicationTypePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () ,'" + cType + "')]");
			if (isCommunicationTypePresent == false) {
				fc.utobj().throwsException("was not able to verify communication type");
			}
			fc.utobj().clickElement(driver, pobj.closeBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "crm")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify the Change Status Of Lead At CRM > Lead > Lead Info", testCaseId = "TC_92_Change_Status")
	public void changeStatusTab() throws IOException, Exception, ParserConfigurationException, SAXException {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			CRMLeadsPage pobj = new CRMLeadsPage(driver);

			/*
			 * fc.utobj().printTestStep(testCaseId,
			 * "Navigate To Admin Users Manage Corporate User");
			 * fc.utobj().printTestStep(testCaseId, "Add Corporate User");
			 * String emailId="crmautomation@staffex.com";
			 * AdminUsersManageCorporateUsersAddCorporateUserPageTest
			 * corporateUser=new
			 * AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			 * CorporateUser corpUser = new
			 * CorporateUser();corpUser.setEmail(emailId);corpUser =
			 * fc.CommonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			 * corporateUser.
			 * createDefaultCorporateUser_Through_WebService_AfterLogin(driver,
			 * corpUser);
			 */

			fc.utobj().printTestStep("Navigate To Admin Users Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			String emailId = "crmautomation@staffex.com";
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUser = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporateUser.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			fc.utobj().printTestStep("Navigating to Admin > CRM > Configure Status");
			fc.utobj().printTestStep("Add Status Lead Type");
			AdminCRMConfigureStatusPageTest statusPage = new AdminCRMConfigureStatusPageTest();
			String statusName = fc.utobj().generateTestData(dataSet.get("status"));
			statusPage.addStatus(driver, statusName);

			String firstName = fc.utobj().generateTestData(dataSet.get("firstName"));
			String lastName = fc.utobj().generateTestData(dataSet.get("lastName"));
			String company = fc.utobj().generateTestData(dataSet.get("company"));
			String assignTo = "Corporate";
			String city = fc.utobj().generateTestData(dataSet.get("city"));
			String address = fc.utobj().generateTestData(dataSet.get("address"));
			String suffix = fc.utobj().generateTestData(dataSet.get("suffix"));
			String jobTitle = fc.utobj().generateTestData(dataSet.get("jobTitle"));
			String comment = fc.utobj().generateTestData(dataSet.get("comment"));
			String franchiseId = "";
			String franchiseUser = "";
			String userNameReg = "";
			String regionName = "";

			fc.utobj().printTestStep("Navigate To CRM > Leads > Leads Summary");
			fc.utobj().printTestStep("Add Lead");
			addLead(driver, dataSet, firstName, lastName, company, assignTo, city, address, suffix, jobTitle, comment,
					corpUser.getuserFullName(), regionName, userNameReg, franchiseId, franchiseUser, config);

			fc.utobj().clickElement(driver, pobj.leadsLink);
			searchLeadByOwner(driver, corpUser.getuserFullName(), "Open Leads");
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//a[contains(text () ,'" + firstName + " " + lastName + "')]"));

			fc.utobj().printTestStep("Change Status");
			fc.utobj().selectDropDown(driver, pobj.leadStatus, statusName);
			fc.utobj().clickElement(driver, pobj.changeStatus);

			fc.utobj().printTestStep("Verify Change Status");
			String statusText = fc.utobj().getText(driver,
					fc.utobj().getElementByXpath(driver, ".//*[@id='cmLeadStatusID']/option[@selected='']"));
			if (!statusText.equalsIgnoreCase(statusName)) {
				fc.utobj().throwsException("was not able to verify status Name");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "crm")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify the Log A Task At CRM > Lead > Lead Info", testCaseId = "TC_93_Log_A_Task")
	public void logATaskLink() throws IOException, Exception, ParserConfigurationException, SAXException {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			CRMLeadsPage pobj = new CRMLeadsPage(driver);

			/*
			 * fc.utobj().printTestStep(testCaseId,
			 * "Navigate To Admin Users Manage Corporate User");
			 * fc.utobj().printTestStep(testCaseId, "Add Corporate User");
			 * String emailId="crmautomation@staffex.com";
			 * AdminUsersManageCorporateUsersAddCorporateUserPageTest
			 * corporateUser=new
			 * AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			 * CorporateUser corpUser = new
			 * CorporateUser();corpUser.setEmail(emailId);corpUser =
			 * fc.CommonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			 * corporateUser.
			 * createDefaultCorporateUser_Through_WebService_AfterLogin(driver,
			 * corpUser);
			 */

			fc.utobj().printTestStep("Navigate To Admin Users Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			String emailId = "crmautomation@staffex.com";
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUser = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporateUser.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			String firstName = fc.utobj().generateTestData(dataSet.get("firstName"));
			String lastName = fc.utobj().generateTestData(dataSet.get("lastName"));
			String company = fc.utobj().generateTestData(dataSet.get("company"));
			String assignTo = "Corporate";
			String city = fc.utobj().generateTestData(dataSet.get("city"));
			String address = fc.utobj().generateTestData(dataSet.get("address"));
			String suffix = fc.utobj().generateTestData(dataSet.get("suffix"));
			String jobTitle = fc.utobj().generateTestData(dataSet.get("jobTitle"));
			String comment = fc.utobj().generateTestData(dataSet.get("comment"));
			String regionName = "";
			String regionalUser = "";
			String franchiseId = "";
			String franchiseUser = "";
			fc.utobj().printTestStep("Navigate To CRM > Leads > Leads Summary");
			fc.utobj().printTestStep("Add Lead");
			addLead(driver, dataSet, firstName, lastName, company, assignTo, city, address, suffix, jobTitle, comment,
					corpUser.getuserFullName(), regionName, regionalUser, franchiseId, franchiseUser, config);

			fc.utobj().clickElement(driver, pobj.leadsLink);
			searchLeadByOwner(driver, corpUser.getuserFullName(), "Open Leads");

			fc.utobj().printTestStep("Log A Task");
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//a[contains(text () ,'" + firstName + " " + lastName + "')]"));
			fc.utobj().clickElement(driver, pobj.logATaskLink);

			fc.commonMethods().switch_cboxIframe_frameId(driver);
			if (!fc.utobj().isSelected(driver,pobj.radioOwner)) {
				fc.utobj().clickElement(driver, pobj.radioOwner);
			}
			String statusTask = dataSet.get("statusTask");
			fc.utobj().selectDropDown(driver, pobj.statusTask, statusTask);
			String subject = fc.utobj().generateTestData(dataSet.get("subject"));
			fc.utobj().sendKeys(driver, pobj.subject, subject);
			if (!fc.utobj().isSelected(driver, pobj.timeLessTask)) {
				fc.utobj().clickElement(driver, pobj.timeLessTask);
			}
			String priority = dataSet.get("priority");
			fc.utobj().selectDropDown(driver, pobj.priority, priority);
			comment = fc.utobj().generateTestData(dataSet.get("comment"));
			fc.utobj().sendKeys(driver, pobj.comments, comment);
			fc.utobj().clickElement(driver, pobj.createBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Verify Log A Task");
			boolean isSubjectPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[contains(text () ,'" + subject + "')]");
			if (isSubjectPresent == false) {
				fc.utobj().throwsException("was not able to verify subject");
			}
			boolean isUserPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[contains(text () ,'" + subject
					+ "')]/ancestor::tr/td/div[.='" + corpUser.getuserFullName() + "']");
			if (isUserPresent == false) {
				fc.utobj().throwsException("was not able to verify userName at Lead Info Page");
			}
			boolean isStatusOfTaskPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[contains(text () ,'" + subject
					+ "')]/ancestor::tr/td[contains(text () ,'" + statusTask + "')]");
			if (isStatusOfTaskPresent == false) {
				fc.utobj().throwsException("was not able to verify status of task");
			}
			/*
			 * boolean isCommentPresent = fc.utobj().verifyCase(driver,
			 * ".//a[contains(text () ,'" + subject +
			 * "')]/ancestor::tr/td[contains(text () ,'" + comment + "')]"); if
			 * (isCommentPresent == false) { fc.utobj().throwsException(
			 * "was not able to verify Comment of task"); }
			 */
			boolean isPriorityPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[contains(text () ,'" + subject
					+ "')]/ancestor::tr/td[contains(text () ,'" + priority + "')]");
			if (isPriorityPresent == false) {
				fc.utobj().throwsException("was not able to verify Priority of task");
			}

			fc.utobj().printTestStep("Verify A Task At Detailed History Frame");
			fc.utobj().clickElement(driver, pobj.detailedHistoryLink);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().clickElement(driver, pobj.openTaskTab);
			fc.utobj().isTextDisplayed(driver, subject,
					"was not able to verify subject of task at deatiled history pop up");
			fc.utobj().isTextDisplayed(driver, corpUser.getuserFullName(),
					"was not able to verify userName of task at deatiled history pop up");
			fc.utobj().isTextDisplayed(driver, statusTask,
					"was not able to verify status of task at deatiled history pop up");
			fc.utobj().isTextDisplayed(driver, comment,
					"was not able to verify comments of task at deatiled history pop up");
			fc.utobj().isTextDisplayed(driver, priority,
					"was not able to verify priority of task at deatiled history pop up");
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "crm", "crmcheck22" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2017-06-27", testCaseDescription = "Verify the  Associate with email campaign At CRM > Lead > Lead Info", testCaseId = "TC_94_Associate_With_Email_Campaign")
	public void associateWithEmailCampaignLink()
			throws IOException, Exception, ParserConfigurationException, SAXException {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			CRMLeadsPage pobj = new CRMLeadsPage(driver);

			fc.utobj().printTestStep("Navigate To CRM >Campaign Center");
			fc.utobj().printTestStep("Add Campaign");
			CRMCampaignCenterPageTestForCRM campaignCenterPage = new CRMCampaignCenterPageTestForCRM();
			String campaignName = fc.utobj().generateTestData(dataSet.get("campaignName"));
			String campaignDescription = fc.utobj().generateTestData(dataSet.get("campaignDescription"));
			String campaignAccessibility = dataSet.get("campaignAccessibility");
			String category = dataSet.get("category");
			String templateName = fc.utobj().generateTestData(dataSet.get("templateName"));
			String mailSubject = dataSet.get("mailSubject");
			String templateText = fc.utobj().generateTestData(dataSet.get("templateText"));
			campaignCenterPage.createCampaign(driver, campaignName, campaignDescription, campaignAccessibility,
					category, templateName, mailSubject, templateText, config);

			/*
			 * fc.utobj().printTestStep(testCaseId,
			 * "Navigate To Admin Users Manage Corporate User");
			 * fc.utobj().printTestStep(testCaseId, "Add Corporate User");
			 * String emailId="crmautomation@staffex.com";
			 * AdminUsersManageCorporateUsersAddCorporateUserPageTest
			 * corporateUser=new
			 * AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			 * CorporateUser corpUser = new
			 * CorporateUser();corpUser.setEmail(emailId);corpUser =
			 * fc.CommonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			 * corporateUser.
			 * createDefaultCorporateUser_Through_WebService_AfterLogin(driver,
			 * corpUser);
			 */

			fc.utobj().printTestStep("Navigate To Admin Users Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			String emailId = "crmautomation@staffex.com";
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUser = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporateUser.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			String firstName = fc.utobj().generateTestData(dataSet.get("firstName"));
			String lastName = fc.utobj().generateTestData(dataSet.get("lastName"));
			String company = fc.utobj().generateTestData(dataSet.get("company"));
			String assignTo = "Corporate";
			String city = fc.utobj().generateTestData(dataSet.get("city"));
			String address = fc.utobj().generateTestData(dataSet.get("address"));
			String suffix = fc.utobj().generateTestData(dataSet.get("suffix"));
			String jobTitle = fc.utobj().generateTestData(dataSet.get("jobTitle"));
			String comment = fc.utobj().generateTestData(dataSet.get("comment"));
			String franchiseId = "";
			String franchiseUser = "";
			String userNameReg = "";
			String regionName = "";
			fc.utobj().printTestStep("Navigate To CRM > Leads > Leads Summary");
			fc.utobj().printTestStep("Add Lead");
			addLead(driver, dataSet, firstName, lastName, company, assignTo, city, address, suffix, jobTitle, comment,
					corpUser.getuserFullName(), regionName, userNameReg, franchiseId, franchiseUser, config);

			fc.utobj().clickElement(driver, pobj.leadsLink);
			searchLeadByOwner(driver, corpUser.getuserFullName(), "Open Leads");
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//a[contains(text () ,'" + firstName + " " + lastName + "')]"));

			fc.utobj().printTestStep("Associate With Campaign");
			fc.utobj().clickElement(driver, pobj.campaignLinkAtLeadInfo);
			fc.utobj().clickElement(driver, pobj.filterContainer);
			fc.utobj().sendKeys(driver, pobj.searchCampaign, campaignName);
			fc.utobj().clickElement(driver, pobj.applyFilter);

			fc.utobj().clickElement(driver, pobj.sendCampaignBtn);
			fc.utobj().clickElement(driver, pobj.confirmBtn);
			fc.utobj().clickElement(driver, pobj.leadsLink);
			searchLeadByOwner(driver, corpUser.getuserFullName(), "Open Leads");
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//a[.='" + firstName + " " + lastName + "']"));

			fc.utobj().printTestStep("Verify Associate With Campaign");
			boolean isCampaignNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () ,'Promotional Campaign')]/following-sibling::td[.='" + campaignName + "']");
			if (isCampaignNamePresent == false) {
				fc.utobj().throwsException("was not able to verify Campign Name");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "crm")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify the Change Owner of Lead At CRM > Lead > Lead Info", testCaseId = "TC_95_Change_Owner")
	public void changeOwnerBottomBtnLI() throws IOException, Exception, ParserConfigurationException, SAXException {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			CRMLeadsPage pobj = new CRMLeadsPage(driver);

			/*
			 * fc.utobj().printTestStep(testCaseId,
			 * "Navigate To Admin Users Manage Corporate User");
			 * fc.utobj().printTestStep(testCaseId, "Add Corporate User");
			 * String emailId="crmautomation@staffex.com";
			 * AdminUsersManageCorporateUsersAddCorporateUserPageTest
			 * corporateUser=new
			 * AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			 * CorporateUser corpUser = new
			 * CorporateUser();corpUser.setEmail(emailId);corpUser =
			 * fc.CommonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			 * corporateUser.
			 * createDefaultCorporateUser_Through_WebService_AfterLogin(driver,
			 * corpUser);
			 */

			fc.utobj().printTestStep("Navigate To Admin Users Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			String emailId = "crmautomation@staffex.com";
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUser = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporateUser.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			fc.utobj().printTestStep("Admin > Users > Manage Regional Users");
			fc.utobj().printTestStep("Add Regional User");
			AdminUsersManageRegionalUsersAddRegionalUserPageTest regionalUserPage = new AdminUsersManageRegionalUsersAddRegionalUserPageTest();
			String userNameReg1 = fc.utobj().generateTestData(dataSet.get("userNameReg"));
			String regionName1 = fc.utobj().generateTestData(dataSet.get("regionName"));

			regionalUserPage.addRegionalUser(driver, userNameReg1, regionName1, emailId);

			String firstName = fc.utobj().generateTestData(dataSet.get("firstName"));
			String lastName = fc.utobj().generateTestData(dataSet.get("lastName"));
			String company = fc.utobj().generateTestData(dataSet.get("company"));
			String assignTo = "Corporate";
			String city = fc.utobj().generateTestData(dataSet.get("city"));
			String address = fc.utobj().generateTestData(dataSet.get("address"));
			String suffix = fc.utobj().generateTestData(dataSet.get("suffix"));
			String jobTitle = fc.utobj().generateTestData(dataSet.get("jobTitle"));
			String comment = fc.utobj().generateTestData(dataSet.get("comment"));
			String franchiseId = "";
			String franchiseUser = "";
			String regionName = "";
			String userNameReg = "";
			fc.utobj().printTestStep("Navigate To CRM > Leads > Leads Summary");
			fc.utobj().printTestStep("Add Lead");
			addLead(driver, dataSet, firstName, lastName, company, assignTo, city, address, suffix, jobTitle, comment,
					corpUser.getuserFullName(), regionName, userNameReg, franchiseId, franchiseUser, config);

			userNameReg1 = userNameReg1 + " " + userNameReg1;

			fc.utobj().clickElement(driver, pobj.leadsLink);
			searchLeadByOwner(driver, corpUser.getuserFullName(), "Open Leads");

			fc.utobj().printTestStep("Change Owner");
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//a[.='" + firstName + " " + lastName + "']"));
			fc.utobj().clickElement(driver, pobj.changeOwnerBtmBtn);

			fc.commonMethods().switch_cboxIframe_frameId(driver);
			if (!fc.utobj().isSelected(driver, pobj.selectRegional)) {
				fc.utobj().clickElement(driver, pobj.selectRegional);
			}
			fc.utobj().selectDropDown(driver, pobj.selectAreaRegion, regionName1);
			fc.utobj().selectDropDown(driver, pobj.selectRegionalUserR, userNameReg1);
			fc.utobj().clickElement(driver, pobj.assignBtn);
			fc.utobj().clickElement(driver, pobj.closeBtn);
			fc.utobj().switchFrameToDefault(driver);

			boolean isUserNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () ,'Assign To')]/ancestor::tr/td[contains(text () ,'" + userNameReg1 + "')]");
			if (isUserNamePresent == false) {
				fc.utobj().throwsException("was not able to verify user name");
			}

			// verify Detailed History More Action
			fc.utobj().printTestStep("Verify Change Owner At Detailed History Frame");
			fc.utobj().selectActionMenuItemsWithTagAMoreActions(driver, "View Detailed History");
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().clickElement(driver, pobj.ownerChange);

			boolean isOwnerFromPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () , 'Owner From')]/../following-sibling::tr[1]/td[contains(text () , '"
							+ corpUser.getuserFullName() + "')]");
			if (isOwnerFromPresent == false) {
				fc.utobj().throwsException("was not able to verify Owner From");
			}
			boolean isOwnerToPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () , 'Owner To')]/../following-sibling::tr[1]/td[contains(text () , '"
							+ userNameReg1 + "')]");
			if (isOwnerToPresent == false) {
				fc.utobj().throwsException("was not able to verify Owner To");
			}
			fc.utobj().clickElement(driver, pobj.closeBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "crm")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify the Add To Group Bottom Btn AT Lead Info At CRM > Lead > Lead Info", testCaseId = "TC_96_Add_To_Group")
	public void addToGroupBottomBtnLI() throws IOException, Exception, ParserConfigurationException, SAXException {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			CRMLeadsPage pobj = new CRMLeadsPage(driver);
			CRMGroupsPageTest groupPage = new CRMGroupsPageTest();

			fc.utobj().printTestStep("Navigate To Admin Users Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			String emailId = "crmautomation@staffex.com";
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUser = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporateUser.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			String firstName = fc.utobj().generateTestData(dataSet.get("firstName"));
			String lastName = fc.utobj().generateTestData(dataSet.get("lastName"));
			String company = fc.utobj().generateTestData(dataSet.get("company"));
			String assignTo = "Corporate";
			String city = fc.utobj().generateTestData(dataSet.get("city"));
			String address = fc.utobj().generateTestData(dataSet.get("address"));
			String suffix = fc.utobj().generateTestData(dataSet.get("suffix"));
			String jobTitle = fc.utobj().generateTestData(dataSet.get("jobTitle"));
			String comment = fc.utobj().generateTestData(dataSet.get("comment"));
			String franchiseId = "";
			String franchiseUser = "";
			String userNameReg = "";
			String regionName = "";
			fc.utobj().printTestStep("Navigate To CRM > Leads > Leads Summary");
			fc.utobj().printTestStep("Add Lead");
			addLead(driver, dataSet, firstName, lastName, company, assignTo, city, address, suffix, jobTitle, comment,
					corpUser.getuserFullName(), regionName, userNameReg, franchiseId, franchiseUser, config);

			fc.utobj().clickElement(driver, pobj.leadsLink);
			searchLeadByOwner(driver, corpUser.getuserFullName(), "Open Leads");
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//a[contains(text () ,'" + firstName + " " + lastName + "')]"));
			fc.utobj().clickElement(driver, pobj.addToGroupLinkBtmBtnAtLeadInfo);

			fc.utobj().printTestStep("Add To Group");
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().clickElement(driver, pobj.addToNewGroup);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Create New Group");
			String groupName = fc.utobj().generateTestData(dataSet.get("groupName"));
			String accessibility = "Public to all Users";
			groupPage.addToGroupRegular(driver, groupName, accessibility);

			/*
			 * fc.utobj().sendKeys(driver, pobj.searchAtGroup, groupName);
			 * fc.utobj().clickElement(driver, pobj.exactSearch);
			 * fc.utobj().clickElement(driver, pobj.systemSearchButton);
			 */

			new CRMGroupsPageTest().searchGroupsByFilter(driver, groupName, "Lead");

			String colunmName = "";
			int z = 0;

			// click over Recipients
			List<WebElement> listHead = driver
					.findElements(By.xpath(".//a[.='" + groupName + "']/ancestor::table/thead/tr/th"));
			System.out.println(listHead);
			for (int i = 0; i < listHead.size(); i++) {

				colunmName = listHead.get(i).getText().trim();
				if (colunmName.equalsIgnoreCase("Recipients")) {
					z = i;
					break;
				}
			}

			if (z != 0) {
				z = z + 1;
				fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
						".//a[.='" + groupName + "']/ancestor::tr/td[" + z + "]/a"));
			} else {
				fc.utobj().throwsException("was not able to find Recipients Column");
			}

			searchLeadByOwner(driver, corpUser.getuserFullName(), "All Leads");
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//a[contains(text () ,'" + firstName + " " + lastName + "')]"));

			fc.utobj().printTestStep("Verify Add To Group");
			boolean isGroupNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () ,'Group(s) Name')]/ancestor::tr/td[contains(text () , '" + groupName
							+ "')]");
			if (isGroupNamePresent == false) {
				fc.utobj().throwsException("was not able to add in group");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "crm" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2017-06-16", testCaseDescription = "Verify the Associate with email campaign At CRM > Lead > Lead Info", testCaseId = "TC_97_Associate_With_Email_Campaign")
	public void associateWithCampaignBottomBtnLI()
			throws IOException, Exception, ParserConfigurationException, SAXException {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			CRMLeadsPage pobj = new CRMLeadsPage(driver);

			fc.utobj().printTestStep("Navigate To CRM >Campaign Center");
			fc.utobj().printTestStep("Add Campaign");
			CRMCampaignCenterPageTestForCRM campaignCenterPage = new CRMCampaignCenterPageTestForCRM();
			String campaignName = fc.utobj().generateTestData(dataSet.get("campaignName"));
			String campaignDescription = fc.utobj().generateTestData(dataSet.get("campaignDescription"));
			String campaignAccessibility = dataSet.get("campaignAccessibility");
			String category = dataSet.get("category");
			String templateName = fc.utobj().generateTestData(dataSet.get("templateName"));
			String mailSubject = dataSet.get("mailSubject");
			String templateText = fc.utobj().generateTestData(dataSet.get("templateText"));
			campaignCenterPage.createCampaign(driver, campaignName, campaignDescription, campaignAccessibility,
					category, templateName, mailSubject, templateText, config);

			fc.utobj().printTestStep("Navigate To Admin Users Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			String emailId = "crmautomation@staffex.com";
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUser = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporateUser.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			String firstName = fc.utobj().generateTestData(dataSet.get("firstName"));
			String lastName = fc.utobj().generateTestData(dataSet.get("lastName"));
			String company = fc.utobj().generateTestData(dataSet.get("company"));
			String assignTo = "Corporate";
			String city = fc.utobj().generateTestData(dataSet.get("city"));
			String address = fc.utobj().generateTestData(dataSet.get("address"));
			String suffix = fc.utobj().generateTestData(dataSet.get("suffix"));
			String jobTitle = fc.utobj().generateTestData(dataSet.get("jobTitle"));
			String comment = fc.utobj().generateTestData(dataSet.get("comment"));
			String franchiseId = "";
			String franchiseUser = "";
			String userNameReg = "";
			String regionName = "";
			fc.utobj().printTestStep("Navigate To CRM > Leads > Leads Summary");
			fc.utobj().printTestStep("Add Lead");

			addLead(driver, dataSet, firstName, lastName, company, assignTo, city, address, suffix, jobTitle, comment,
					corpUser.getuserFullName(), regionName, userNameReg, franchiseId, franchiseUser, config);

			fc.utobj().clickElement(driver, pobj.leadsLink);
			searchLeadByOwner(driver, corpUser.getuserFullName(), "Open Leads");
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//a[contains(text () ,'" + firstName + " " + lastName + "')]"));

			fc.utobj().printTestStep("Associate With Campaign");
			fc.utobj().clickElement(driver, pobj.AssociateWithRegularCampaignButton);
			fc.utobj().clickElement(driver, pobj.filterContainer);
			fc.utobj().sendKeys(driver, pobj.searchCampaign, campaignName);
			fc.utobj().clickElement(driver, pobj.applyFilter);

			fc.utobj().clickElement(driver, pobj.sendCampaignBtn);
			fc.utobj().clickElement(driver, pobj.confirmBtn);
			fc.utobj().clickElement(driver, pobj.leadsLink);
			searchLeadByOwner(driver, corpUser.getuserFullName(), "Open Leads");
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//a[.='" + firstName + " " + lastName + "']"));

			fc.utobj().printTestStep("Verify Associate With Campaign");
			boolean isCampaignNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () ,'Promotional Campaign')]/following-sibling::td[.='" + campaignName + "']");
			if (isCampaignNamePresent == false) {
				fc.utobj().throwsException("was not able to verify Campign Name");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "crm", "crmmodule1q2"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify the Send  Email At CRM > Leads > Emails", testCaseId = "TC_98_Send_Email")
	public void sendEmailTabAtLeadInfo() throws IOException, Exception, ParserConfigurationException, SAXException {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			CRMLeadsPage pobj = new CRMLeadsPage(driver);

			/*
			 * fc.utobj().printTestStep(testCaseId,
			 * "Navigate To Admin Users Manage Corporate User");
			 * fc.utobj().printTestStep(testCaseId, "Add Corporate User");
			 * String emailId="crmautomation@staffex.com";
			 * AdminUsersManageCorporateUsersAddCorporateUserPageTest
			 * corporateUser=new
			 * AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			 * CorporateUser corpUser = new
			 * CorporateUser();corpUser.setEmail(emailId);corpUser =
			 * fc.CommonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			 * corporateUser.
			 * createDefaultCorporateUser_Through_WebService_AfterLogin(driver,
			 * corpUser);
			 */

			fc.utobj().printTestStep("Navigate To Admin Users Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			String emailId = "crmautomation@staffex.com";
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUser = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporateUser.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			AdminCRMConfigureEmailContentPageTest configureMail = new AdminCRMConfigureEmailContentPageTest();
			configureMail.configureAllEmailContentLead(driver);

			String firstName = fc.utobj().generateTestData(dataSet.get("firstName"));
			String lastName = fc.utobj().generateTestData(dataSet.get("lastName"));
			String company = fc.utobj().generateTestData(dataSet.get("company"));
			String assignTo = "Corporate";
			String city = fc.utobj().generateTestData(dataSet.get("city"));
			String address = fc.utobj().generateTestData(dataSet.get("address"));
			String suffix = fc.utobj().generateTestData(dataSet.get("suffix"));
			String jobTitle = fc.utobj().generateTestData(dataSet.get("jobTitle"));
			String comment = fc.utobj().generateTestData(dataSet.get("comment"));
			String regionName = "";
			String regionalUser = "";
			String franchiseId = "";
			String franchiseUser = "";
			fc.utobj().printTestStep("Navigate To CRM > Leads > Leads Summary");
			fc.utobj().printTestStep("Add Lead");
			addLead(driver, dataSet, firstName, lastName, company, assignTo, city, address, suffix, jobTitle, comment,
					corpUser.getuserFullName(), regionName, regionalUser, franchiseId, franchiseUser, config);

			fc.utobj().clickElement(driver, pobj.leadsLink);
			searchLeadByOwner(driver, corpUser.getuserFullName(), "Open Leads");

			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//a[contains(text () ,'" + firstName + " " + lastName + "')]"));
			fc.utobj().clickElement(driver, pobj.emailsTab);
			fc.utobj().printTestStep("Send Email");

			fc.utobj().clickElement(driver, pobj.sendEmailAtEmails);

			if (!fc.utobj().isSelected(driver, pobj.loggedUserId)) {
				fc.utobj().clickElement(driver, pobj.loggedUserId);
			}

			/*
			 * String mailSentToId = fc.utobj().getText(driver,
			 * pobj.mailSentTo); if
			 * (!mailSentToId.equalsIgnoreCase("crmautomation@staffex.com"))
			 * { fc.utobj().throwsException("was not able to verify mailSentTo"
			 * ); } String mailCC = dataSet.get("mailCC");
			 * fc.utobj().sendKeys(driver, pobj.mailCC, mailCC);
			 * 
			 * if (!pobj.showBcc) { fc.utobj().clickElement(driver,
			 * pobj.showBcc); } String mailBCC = dataSet.get("mailBCC");
			 * fc.utobj().sendKeys(driver, pobj.mailBCC, mailBCC);
			 */
			String subjectMail = fc.utobj().generateTestData(dataSet.get("subjectMail"));
			fc.utobj().sendKeys(driver, pobj.subjectMail, subjectMail);

			String editorText = fc.utobj().generateTestData(dataSet.get("mailText"));

			fc.utobj().switchFrameById(driver, "ta_ifr");

			Actions actions = new Actions(driver);
			actions.moveToElement(fc.utobj().getElement(driver, pobj.editorTextArea));
			actions.click();
			actions.sendKeys(editorText);
			fc.utobj().logReportMsg("Entered Text", editorText);
			actions.build().perform();
			fc.utobj().switchFrameToDefault(driver);
			fc.utobj().clickElement(driver, pobj.sendEmailBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Verify Email");

			String expectedSubject = subjectMail;
			String expectedMessageBody = editorText;
			Map<String, String> mailData = fc.utobj().readMailBox(expectedSubject, expectedMessageBody, emailId,
					"sdg@1a@Hfs");

			if (mailData.size() == 0 || !mailData.get("mailBody").contains(editorText)) {
				fc.utobj().throwsException("was not able to verify mail text");
			}
			if (mailData.size() == 0 || !mailData.get("subject").equalsIgnoreCase(subjectMail)) {
				fc.utobj().throwsException("was not able to verify subject mail");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "crm"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2017-06-29", testCaseDescription = "Verify the Add Lead From External Form At CRM > Lead > Lead Summary", testCaseId = "TC_99_Verify_Lead_External_Form")
	public void verifyLeadExternalForm() throws IOException, Exception, ParserConfigurationException, SAXException {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			CRMLeadsPage pobj = new CRMLeadsPage(driver);

			AdminCRMManageWebFormGeneratorPageTest webFormPage = new AdminCRMManageWebFormGeneratorPageTest();

			/*
			 * fc.utobj().printTestStep(testCaseId,
			 * "Navigate To Admin Users Manage Corporate User");
			 * fc.utobj().printTestStep(testCaseId, "Add Corporate User");
			 * AdminUsersManageCorporateUsersAddCorporateUserPageTest
			 * corporatePage = new
			 * AdminUsersManageCorporateUsersAddCorporateUserPageTest(); String
			 * userName = fc.utobj().generateTestData(dataSet.get("userName"));
			 * String emailId="crmautomation@staffex.com"; String
			 * userNameCor = corporatePage.addCorporateUser(driver, userName,
			 * config , emailId);
			 */

			fc.utobj().printTestStep("Navigate To Admin Users Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			String emailId = "crmautomation@staffex.com";
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUser = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporateUser.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			fc.utobj().printTestStep("Navigate To Admin > CRM > Configure Status");
			fc.utobj().printTestStep("Add Status");
			AdminCRMConfigureStatusPageTest statusPage = new AdminCRMConfigureStatusPageTest();
			String status = fc.utobj().generateTestData(dataSet.get("status"));
			statusPage.addStatus(driver, status);

			fc.utobj().printTestStep("Navigating to Admin > CRM > Configure Medium");
			fc.utobj().printTestStep("Add Medium");
			AdminCRMConfigureMediumPageTest mediumPage = new AdminCRMConfigureMediumPageTest();
			String contactMedium = fc.utobj().generateTestData(dataSet.get("medium"));
			String description = fc.utobj().generateTestData(dataSet.get("description"));
			mediumPage.addMedium(driver, contactMedium, description);

			fc.utobj().printTestStep("Navigating to Admin > CRM > Source Summary");
			fc.utobj().printTestStep("Add Source");
			AdminCRMSourceSummaryPageTest leadSourcePage = new AdminCRMSourceSummaryPageTest();
			String leadSource = fc.utobj().generateTestData(dataSet.get("source"));
			String displayOnWebPageText = fc.utobj().generateTestData(dataSet.get("text"));
			leadSourcePage.addSource(driver, leadSource, description, displayOnWebPageText);
			String sourceDetails = fc.utobj().generateTestData(dataSet.get("sourceDetails"));
			fc.utobj().printTestStep("Add Source Details");
			leadSourcePage.addSourceDetails(driver, leadSource, sourceDetails, displayOnWebPageText, description);

			String formFormat = dataSet.get("formFormat");
			String header = fc.utobj().generateTestData(dataSet.get("header"));
			String textLabel = fc.utobj().generateTestData(dataSet.get("textLabel"));
			String fieldLabel = fc.utobj().generateTestData(dataSet.get("fieldLabel"));
			String footer = fc.utobj().generateTestData(dataSet.get("footer"));
			String assignTo = dataSet.get("assignTo");
			String submissionType = dataSet.get("submissionType");
			String formName = fc.utobj().generateTestData(dataSet.get("formName"));
			String formTitle = fc.utobj().generateTestData(dataSet.get("formTitle"));
			description = fc.utobj().generateTestData(dataSet.get("description"));
			String formUrl = fc.utobj().generateTestData(dataSet.get("formUrl"));
			String sectionName = "";
			String regionName = "";
			String userNameReg = "";
			String franchiseId = "";
			String userNameFranchise = "";
			String afterSubmission = dataSet.get("afterSubmission");
			String tabName = fc.utobj().generateTestData(dataSet.get("tabName"));
			String redirectedUrl = "www.google.co.in";

			fc.utobj().printTestStep("Navigate To Admin > CRM > Manage Web Form Generator");
			fc.utobj().printTestStep("Create New Form Lead");
			leadSource = sourceDetails = displayOnWebPageText;
			webFormPage.createNewFormLeads(driver, formFormat, header, textLabel, fieldLabel, footer, assignTo,
					submissionType, formName, formTitle, description, formUrl, sectionName, corpUser.getuserFullName(),
					regionName, userNameReg, franchiseId, userNameFranchise, status, contactMedium, leadSource,
					sourceDetails, afterSubmission, redirectedUrl, tabName, config);

			String parentWindow = driver.getWindowHandle();
			String firstName = fc.utobj().generateTestData(dataSet.get("firstName"));
			String lastName = fc.utobj().generateTestData(dataSet.get("lastName"));

			fc.utobj().sendKeys(driver, pobj.searchMyForm, formName);
			fc.utobj().sleep(3000);
			fc.utobj().clickElement(driver, pobj.searchMyFormBtn);
			fc.utobj().sleep(5000);
			fc.utobj().actionImgOption(driver, formName, "Launch & Test");
			fc.utobj().sleep(5000);
			fc.utobj().printTestStep("Add Lead");
			Set<String> allWindows2 = driver.getWindowHandles();
			for (String currentWindow : allWindows2) {
				if (!currentWindow.equalsIgnoreCase(parentWindow)) {
					driver.switchTo().window(currentWindow);
					String titleTextCurrent = driver.getTitle();
					if (titleTextCurrent.equalsIgnoreCase(formTitle)) {
			
						fc.utobj().sendKeys(driver, pobj.leadLastName, firstName);
			
						fc.utobj().sendKeys(driver, pobj.leadFirstName, lastName);
					
						fc.utobj().selectDropDownByPartialText(driver, pobj.title, "Dr.");
					
						fc.utobj().sendKeys(driver, pobj.companyName, "companyName");
				
						fc.utobj().sendKeys(driver, pobj.address, "address");
				
						fc.utobj().sendKeys(driver, pobj.city, "city");
					
						fc.utobj().selectDropDownByPartialText(driver, pobj.country, "USA");
			
						fc.utobj().sendKeys(driver, pobj.zipcode, "124578");
				
						fc.utobj().selectDropDownByPartialText(driver, pobj.state, "Alabama");
				
						fc.utobj().sendKeys(driver, pobj.phoneNumbers, "1236547896");
				
						fc.utobj().sendKeys(driver, pobj.extn, "12");
				
						fc.utobj().sendKeys(driver, pobj.faxNumbers, "1236577896");
					
						fc.utobj().sendKeys(driver, pobj.mobileNumbers, "9874563212");
					
						fc.utobj().sendKeys(driver, pobj.emailIds, emailId);
				
						/*fc.utobj().sendKeys(driver, pobj.alternateEmail, "jhsdhdasdfs@gmail.com");
						fc.utobj().selectDropDownByPartialText(driver, pobj.leadSourceLaunch, leadSource);

						fc.utobj().selectDropDownByPartialText(driver, pobj.leadSourceDetailsLaunch, sourceDetails);*/
						fc.utobj().clickElement(driver, pobj.nextBtn);
						fc.utobj().clickElement(driver, pobj.submitBtn);
						driver.close();
					} else {
						driver.close();
						driver.switchTo().window(parentWindow);
					}
					driver.switchTo().window(parentWindow);
				}
			}
		
			fc.utobj().printTestStep("Navigate To CRM > Leads > Leads Summary");
		
			fc.crm().crm_common().CRMLeadsLnk(driver);
		
			searchLeadByOwner(driver, corpUser.getuserFullName(), "Open Leads");

			fc.utobj().printTestStep("Verify Lead");
			boolean isLeadNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[.='" + lastName + " " + firstName + "']");
			if (isLeadNamePresent == false) {
				fc.utobj().throwsException("was not able to verify Lead Name");
			}
			boolean isOwnerPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[.='" + lastName + " " + firstName
					+ "']/ancestor::tr/td[contains(text () ,'" + corpUser.getuserFullName() + "')]");
			if (isOwnerPresent == false) {
				fc.utobj().throwsException("was not able to verify Owner of the Lead");
			}
			/*boolean isStatusPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[.='" + lastName + " " + firstName
					+ "']/ancestor::tr/td[contains(text () ,'" + status + "')]");
			if (isStatusPresent == false) {
				fc.utobj().throwsException("was not able to verify Status of the Lead");
			}
*/
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	/*
	 * @Test(groups = "crm")
	 * 
	 * @TestCase(createdOn="2017-05-01",updatedOn="2018-06-14",
	 * testCaseDescription =
	 * "Verify the Add Lead From Social Media Form At CRM > Lead > Lead Summary"
	 * , testCaseId = "TC_100_Verify_Lead_Social_Media") public void
	 * verifyLeadSocialMedia() throws IOException, Exception,
	 * ParserConfigurationException, SAXException {
	 * 
	 * String testCaseId =
	 * fc.utobj().readTestCaseInfo(this.getClass().getName()+"."+new
	 * Object(){}.getClass().getEnclosingMethod().getName());
	 * 
	 * Map<String, String> config =
	 * fc.utobj().readConfigurationFile(FranconnectUtil.path);
	 * Map<String,String> dataSet = fc.utobj().readTestData("crm", testCaseId);
	 * WebDriver driver = fc.utobj().openDriver(config);
	 * 
	 * 
	 * 
	 * try { driver = fc.loginpage().login(driver, config); CRMLeadsPage pobj =
	 * new CRMLeadsPage(driver);
	 * 
	 * 
	 * fc.utobj().printTestStep(testCaseId,
	 * "Naviagate To Admin CRM > Manage Web Form Generator");
	 * fc.crm().crm_common().adminCRMManageWebFormGeneratorLnk( driver); String
	 * parentWindow = driver.getWindowHandle(); String firstName =
	 * fc.utobj().generateTestData(dataSet.get("firstName")); String lastName =
	 * fc.utobj().generateTestData(dataSet.get("lastName")); String emailId =
	 * dataSet.get("emailId"); fc.utobj().sendKeys(driver, pobj.searchMyForm,
	 * "Social Lead Form"); fc.utobj().clickElement(driver,
	 * pobj.searchMyFormBtn); fc.utobj().actionImgOption(driver,
	 * "Social Lead Form", "Launch & Test");
	 * 
	 * fc.utobj().printTestStep(testCaseId, "Add Lead By Social Medium Form");
	 * Set<String> allWindows2 = driver.getWindowHandles(); for (String
	 * currentWindow : allWindows2) { if
	 * (!currentWindow.equalsIgnoreCase(parentWindow)) {
	 * 
	 * driver.switchTo().window(currentWindow); String titleTextCurrent =
	 * driver.getTitle(); if (titleTextCurrent.equalsIgnoreCase("Contact Us")) {
	 * 
	 * fc.utobj().sendKeys(driver, pobj.leadLastName, firstName);
	 * fc.utobj().sendKeys(driver, pobj.leadFirstName, lastName);
	 * fc.utobj().sendKeys(driver, pobj.emailIds, emailId);
	 * fc.utobj().sendKeys(driver, pobj.zipcode, "124578");
	 * fc.utobj().sendKeys(driver, pobj.phoneNumbers, "1236547896");
	 * fc.utobj().clickElement(driver, pobj.submitBtn); driver.close(); } else {
	 * driver.close(); driver.switchTo().window(parentWindow); } }
	 * 
	 * driver.switchTo().window(parentWindow); }
	 * 
	 * fc.utobj().printTestStep(testCaseId, "Navigate To CRM > Leads Page");
	 * fc.crm().crm_common().CRMLeadsLnk( driver); searchLeadByOwner(driver,
	 * "FranConnect Administrator", "Open Leads"); try {
	 * fc.utobj().selectDropDown(driver, pobj.resultsPerPage, "100"); } catch
	 * (Exception e) { }
	 * 
	 * fc.utobj().printTestStep(testCaseId, "Verify Lead"); boolean
	 * isLeadNamePresent = fc.utobj().verifyCase(driver, ".//a[.='" + lastName +
	 * " " + firstName + "']"); if (isLeadNamePresent == false) {
	 * fc.utobj().throwsException("was not able to verify Lead Name"); } boolean
	 * isOwnerPresent = fc.utobj().verifyCase(driver, ".//a[.='" + lastName +
	 * " " + firstName +
	 * "']/ancestor::tr/td[contains(text () ,'FranConnect Administrator')]"); if
	 * (isOwnerPresent == false) { fc.utobj().throwsException(
	 * "was not able to verify Owner of the Lead"); } boolean isStatusPresent =
	 * fc.utobj().verifyCase(driver, ".//a[.='" + lastName + " " + firstName +
	 * "']/ancestor::tr/td[contains(text () ,'New')]"); if (isStatusPresent ==
	 * false) { fc.utobj().throwsException(
	 * "was not able to verify Status of the Lead"); }
	 * 
	 * 
	 * fc.utobj().logoutAndQuitBrowser(driver, config, testCaseId); } catch
	 * (Exception e) {
	 * 
	 * fc.utobj().quitBrowserOnCatch(driver, config, e, testCaseId);} }
	 */

	/*
	 * @Test(groups = "crmAbs")
	 * 
	 * @TestCase(createdOn="2017-05-01",updatedOn="2018-06-14",
	 * testCaseDescription =
	 * "Verify the Add Lead From Google PPC Form At CRM > Lead > Lead Summary",
	 * testCaseId = "TC_101_Verify_Lead_Google_PPC") public void
	 * verifyLeadGooglePPC() throws IOException, Exception,
	 * ParserConfigurationException, SAXException {
	 * 
	 * String testCaseId =
	 * fc.utobj().readTestCaseInfo(this.getClass().getName()+"."+new
	 * Object(){}.getClass().getEnclosingMethod().getName());
	 * 
	 * Map<String, String> config =
	 * fc.utobj().readConfigurationFile(FranconnectUtil.path);
	 * Map<String,String> dataSet = fc.utobj().readTestData("crm", testCaseId);
	 * WebDriver driver = fc.utobj().openDriver(config);
	 * 
	 * 
	 * 
	 * try { driver = fc.loginpage().login(driver, config); CRMLeadsPage pobj =
	 * new CRMLeadsPage(driver);
	 * 
	 * 
	 * fc.utobj().printTestStep(testCaseId,
	 * "Navigate To Admin > CRM > Manage Web Form Generator");
	 * fc.crm().crm_common().adminCRMManageWebFormGeneratorLnk( driver); String
	 * parentWindow = driver.getWindowHandle(); String firstName =
	 * fc.utobj().generateTestData(dataSet.get("firstName")); String lastName =
	 * fc.utobj().generateTestData(dataSet.get("lastName")); String emailId =
	 * dataSet.get("emailId"); fc.utobj().sendKeys(driver, pobj.searchMyForm,
	 * "Online Ads Lead Form"); fc.utobj().clickElement(driver,
	 * pobj.searchMyFormBtn);
	 * 
	 * fc.utobj().printTestStep(testCaseId, "Add Lead By Online Ads Lead Form");
	 * fc.utobj().actionImgOption(driver, "Online Ads Lead Form",
	 * "Launch & Test");
	 * 
	 * Set<String> allWindows2 = driver.getWindowHandles(); for (String
	 * currentWindow : allWindows2) { if
	 * (!currentWindow.equalsIgnoreCase(parentWindow)) {
	 * 
	 * driver.switchTo().window(currentWindow); String titleTextCurrent =
	 * driver.getTitle(); if (titleTextCurrent.equalsIgnoreCase(
	 * "Online Ads Lead Form")) {
	 * 
	 * fc.utobj().sendKeys(driver, pobj.leadLastName, firstName);
	 * fc.utobj().sendKeys(driver, pobj.leadFirstName, lastName);
	 * fc.utobj().sendKeys(driver, pobj.emailIds, emailId);
	 * fc.utobj().sendKeys(driver, pobj.zipcode, "124578");
	 * fc.utobj().sendKeys(driver, pobj.phoneNumbers, "1236547896");
	 * fc.utobj().clickElement(driver, pobj.submitBtn); driver.close(); } else {
	 * driver.close(); driver.switchTo().window(parentWindow); } } }
	 * 
	 * fc.utobj().printTestStep(testCaseId, "Navigate To CRM Lead Page");
	 * fc.crm().crm_common().CRMLeadsLnk( driver); searchLeadByOwner(driver,
	 * "FranConnect Administrator", "Open Leads"); try {
	 * fc.utobj().selectDropDown(driver, pobj.resultsPerPage, "100"); } catch
	 * (Exception e) { }
	 * 
	 * fc.utobj().printTestStep(testCaseId, "Verify Lead"); boolean
	 * isLeadNamePresent = fc.utobj().verifyCase(driver, ".//a[.='" + firstName
	 * + " " + lastName + "']"); if (isLeadNamePresent == false) {
	 * fc.utobj().throwsException("was not able to verify Lead Name"); } boolean
	 * isOwnerPresent = fc.utobj().verifyCase(driver, ".//a[.='" + firstName +
	 * " " + lastName +
	 * "']/ancestor::tr/td[contains(text () ,'FranConnect Administrator')]"); if
	 * (isOwnerPresent == false) { fc.utobj().throwsException(
	 * "was not able to verify Owner of the Lead"); } boolean isStatusPresent =
	 * fc.utobj().verifyCase(driver, ".//a[.='" + firstName + " " + lastName +
	 * "']/ancestor::tr/td[contains(text () ,'New')]"); if (isStatusPresent ==
	 * false) { fc.utobj().throwsException(
	 * "was not able to verify Status of the Lead"); }
	 * fc.utobj().logoutAndQuitBrowser(driver, config, testCaseId); } catch
	 * (Exception e) {
	 * 
	 * fc.utobj().quitBrowserOnCatch(driver, config, e, testCaseId);} }
	 */

	@Test(groups = {"crm"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify the Add Lead From Corporate User At CRM > Lead > Lead Summary", testCaseId = "TC_102_Add_Lead_Corporate_User")
	public void addLeadCorporateUser() throws IOException, Exception, ParserConfigurationException, SAXException {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			CRMLeadsPage pobj = new CRMLeadsPage(driver);

			fc.utobj().printTestStep("Naviagate To Users > Manage Corporate Users");
			AdminUsersManageCorporateUsersAddCorporateUserPageTest addCorporatePage = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			String emailId = "crmautomation@staffex.com";
			CorporateUser corpUser = new CorporateUser();
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corpUser.setEmail(emailId);
			corpUser = addCorporatePage.createDefaultUser(driver, corpUser);

			String firstName = fc.utobj().generateTestData(dataSet.get("firstName"));
			String lastName = fc.utobj().generateTestData(dataSet.get("lastName"));
			String company = fc.utobj().generateTestData(dataSet.get("company"));
			String assignTo = "Corporate";
			String email = dataSet.get("emailId");
			String city = fc.utobj().generateTestData(dataSet.get("city"));
			String address = fc.utobj().generateTestData(dataSet.get("address"));
			String suffix = fc.utobj().generateTestData(dataSet.get("suffix"));
			String jobTitle = fc.utobj().generateTestData(dataSet.get("jobTitle"));
			String comment = fc.utobj().generateTestData(dataSet.get("comment"));
			String regionName = "";
			String regionalUser = "";
			String franchiseId = "";
			String franchiseUser = "";

			fc.home_page().logout(driver);

			fc.utobj().printTestStep("Login With Corporate User");
			fc.loginpage().loginWithParameter(driver, corpUser.getUserName(), corpUser.getPassword());

			fc.utobj().printTestStep("Navigate To CRM > Leads > Leads Summary");
			fc.utobj().printTestStep("Add Lead");
			addLead(driver, dataSet, firstName, lastName, company, assignTo, city, address, suffix, jobTitle, comment,
					corpUser.getuserFullName(), regionName, regionalUser, franchiseId, franchiseUser, config);

			fc.utobj().printTestStep("Verify Lead At Corporate Lead Page");
			fc.utobj().clickElement(driver, pobj.leadsLink);
			searchLeadByOwner(driver, corpUser.getuserFullName(), "Open Leads");

			boolean verifyName = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[.='" + firstName + " " + lastName + "']");
			if (verifyName == false) {
				fc.utobj().throwsException("was not able to verify Name");
			}
			boolean ownerOfLead = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[.='" + firstName + " " + lastName
					+ "']/ancestor::tr/td[contains(text () , '" + corpUser.getuserFullName() + "')]");
			if (ownerOfLead == false) {
				fc.utobj().throwsException("was not able to verify owner of the lead");
			}
			boolean statusOfLead = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[.='" + firstName + " " + lastName + "']/ancestor::tr/td[contains(text () , 'New')]");
			if (statusOfLead == false) {
				fc.utobj().throwsException("was not able to verify staus of the lead");
			}
			/*boolean emailOfLead = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[.='" + firstName + " " + lastName
					+ "']/ancestor::tr/td/a[contains(text () , '" + email + "')]");
			if (emailOfLead == false) {
				fc.utobj().throwsException("was not able to verify email of the lead");
			}*/

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "crm"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify the Add Lead From Divisional User At CRM > Lead > Lead Summary", testCaseId = "TC_103_Add_Lead_Divisional_User")
	public void addLeadDivisionalUser() throws IOException, Exception, ParserConfigurationException, SAXException {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			CRMLeadsPage pobj = new CRMLeadsPage(driver);

			String password = "T0n1ght1";

			fc.utobj().printTestStep("Admin > Users > Manage Regional Users");
			fc.utobj().printTestStep("Add Regional User");

			String emailId = "crmautomation@staffex.com";
			fc.utobj().printTestStep("Navigate To Admin Users > Manage Divisional User");
			fc.utobj().printTestStep("Add Divisional User");
			AdminUsersManageDivisionalUsersAddDivisionalUsersPageTest divisionalUserPage = new AdminUsersManageDivisionalUsersAddDivisionalUsersPageTest();
			String divisionName = fc.utobj().generateTestData(dataSet.get("division"));
			String divUser = fc.utobj().generateTestData(dataSet.get("divUser"));
			String userName = divisionalUserPage.addDivisionalUser(driver, divUser, divisionName, emailId);

			String firstName = fc.utobj().generateTestData(dataSet.get("firstName"));
			String lastName = fc.utobj().generateTestData(dataSet.get("lastName"));
			String company = fc.utobj().generateTestData(dataSet.get("company"));
			//String email = dataSet.get("emailId");
			String email=fc.utobj().generateTestData("crmautomation")+"@staffex.com";
			String city = fc.utobj().generateTestData(dataSet.get("city"));
			String address = fc.utobj().generateTestData(dataSet.get("address"));
			String suffix = fc.utobj().generateTestData(dataSet.get("suffix"));
			String jobTitle = fc.utobj().generateTestData(dataSet.get("jobTitle"));
			String comment = fc.utobj().generateTestData(dataSet.get("comment"));
			fc.home_page().logout(driver);

			fc.utobj().printTestStep("Login With Divisional User");
			fc.loginpage().loginWithParameter(driver, divUser, "fran1234");
			fc.utobj().printTestStep("Navigate To CRM > Leads > Leads Summary");
			fc.utobj().printTestStep("Add Lead");
			fc.crm().crm_common().CRMLeadsLnk(driver);
			fc.utobj().clickElement(driver, pobj.addNew);
			fc.utobj().clickElement(driver, pobj.leadLnk);
			fc.utobj().selectDropDown(driver, pobj.title, dataSet.get("title"));
			fc.utobj().sendKeys(driver, pobj.leadFirstName, firstName);
			fc.utobj().sendKeys(driver, pobj.leadLastName, lastName);
			fc.utobj().sendKeys(driver, pobj.companyName, company);
			fc.utobj().selectDropDown(driver, pobj.primaryContactMethodSelect, dataSet.get("primaryContactMethod"));
			fc.utobj().selectDropDown(driver, pobj.leadDivision, divisionName);
			fc.utobj().selectDropDown(driver, pobj.leadDivisionUser, userName + " " + userName);
			fc.utobj().sendKeys(driver, pobj.address, address);
			fc.utobj().sendKeys(driver, pobj.city, city);
			fc.utobj().selectDropDown(driver, pobj.country, "USA");
			fc.utobj().selectDropDown(driver, pobj.state, "Alabama");
			fc.utobj().sendKeys(driver, pobj.zipcode, "12345");
			fc.utobj().sendKeys(driver, pobj.phoneNumbers, "1236547896");
			fc.utobj().sendKeys(driver, pobj.extn, "12");
			fc.utobj().sendKeys(driver, pobj.faxNumbers, "1263985487");
			fc.utobj().sendKeys(driver, pobj.mobileNumbers, "8896724441");
			fc.utobj().sendKeys(driver, pobj.emailIds, email);
			fc.utobj().sendKeys(driver, pobj.alternateEmail, "test@gmail.com");
			fc.utobj().sendKeys(driver, pobj.suffix, suffix);
			fc.utobj().sendKeys(driver, pobj.position, jobTitle);
			fc.utobj().sendKeys(driver, pobj.birthdate, "11/24/1980");
			fc.utobj().sendKeys(driver, pobj.anniversarydate, "11/24/1999");
			fc.utobj().selectDropDown(driver, pobj.rating, dataSet.get("rating"));
			fc.utobj().sendKeys(driver, pobj.bestTimeToContact, "12:00 PM EST");
			fc.utobj().sendKeys(driver, pobj.comments, comment);
			fc.utobj().clickElement(driver, pobj.saveBtn);

			fc.utobj().printTestStep("Verify Lead At Divisional User Page");
			fc.utobj().clickElement(driver, pobj.leadsLink);

			/* Start TC_281_Verify_Filter_All_Leads_Type */

			fc.utobj().clickElement(driver, pobj.showFilter);
			fc.utobj().selectDropDown(driver, pobj.viewLeadsSelect, "All Leads");

			fc.utobj().clickElement(driver, pobj.searchBtn);

			/* End TC_281_Verify_Filter_All_Leads_Type */

			try {
				fc.utobj().selectDropDown(driver, pobj.resultsPerPage, "100");
			} catch (Exception e) {
			}

			boolean verifyName = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[.='" + firstName + " " + lastName + "']");
			if (verifyName == false) {
				fc.utobj().throwsException("was not able to verify Name");
			}
			boolean ownerOfLead = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[.='" + firstName + " " + lastName
					+ "']/ancestor::tr/td[contains(text () , '" + userName + " " + userName + "')]");
			if (ownerOfLead == false) {
				fc.utobj().throwsException("was not able to verify owner of the lead");
			}
			boolean statusOfLead = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[.='" + firstName + " " + lastName + "']/ancestor::tr/td[contains(text () , 'New')]");
			if (statusOfLead == false) {
				fc.utobj().throwsException("was not able to verify staus of the lead");
			}
			/*boolean emailOfLead = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[.='" + firstName + " " + lastName
					+ "']/ancestor::tr/td/a[contains(text () , '" + email + "')]");
			if (emailOfLead == false) {
				fc.utobj().throwsException("was not able to verify email of the lead");
			}*/
			/*
			 * fc.home_page().logout(driver, config);
			 * 
			 * fc.utobj().printTestStep(testCaseId,
			 * "Login WIth Regional User Page");
			 * fc.loginpage().loginWithParameter(driver, userNameReg1,
			 * password);
			 * 
			 * fc.utobj().printTestStep(testCaseId,
			 * "Verify At Regional User Leads Page");
			 * fc.crm().crm_common().CRMLeadsLnk( driver);
			 * searchLeadByOwner(driver, userName+" "+userName, "Open Leads");
			 * /*try { fc.utobj().selectDropDown(driver, pobj.resultsPerPage,
			 * "100"); } catch (Exception e) { }
			 * 
			 * boolean verifyNameAtR = fc.utobj().verifyCase(driver, ".//a[.='"
			 * + firstName + " " + lastName + "']"); if (verifyNameAtR == false)
			 * { fc.utobj().throwsException("was not able to verify Name"); }
			 * boolean ownerOfLeadAtR = fc.utobj().verifyCase(driver, ".//a[.='"
			 * + firstName + " " + lastName +
			 * "']/ancestor::tr/td[contains(text () , '" + userName+" "+userName
			 * + "')]"); if (ownerOfLeadAtR == false) {
			 * fc.utobj().throwsException(
			 * "was not able to verify owner of the lead"); } boolean
			 * statusOfLeadAtR = fc.utobj().verifyCase(driver, ".//a[.='" +
			 * firstName + " " + lastName +
			 * "']/ancestor::tr/td[contains(text () , 'New')]"); if
			 * (statusOfLeadAtR == false) { fc.utobj().throwsException(
			 * "was not able to verify staus of the lead"); } boolean
			 * emailOfLeadAtR = fc.utobj().verifyCase(driver, ".//a[.='" +
			 * firstName + " " + lastName +
			 * "']/ancestor::tr/td/a[contains(text () , '" + email + "')]"); if
			 * (emailOfLeadAtR == false) { fc.utobj().throwsException(
			 * "was not able to verify email of the lead"); }
			 * 
			 */
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "crm"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify the Add Lead From Regional User At CRM > Lead > Lead Summary", testCaseId = "TC_104_Add_Lead_Regional_User")
	public void addLeadRegionalUser() throws IOException, Exception, ParserConfigurationException, SAXException {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			CRMLeadsPage pobj = new CRMLeadsPage(driver);

			String password = "T0n1ght1";

			// add Franchise user
			fc.utobj().printTestStep("Navigate To Admin > Franchise Location > Add Franchise Location");
			AdminFranchiseLocationAddFranchiseLocationPageTest franchiseLocation = new AdminFranchiseLocationAddFranchiseLocationPageTest();
			String franchiseId = fc.utobj().generateTestData(dataSet.get("franchiseId"));
			String regionName = fc.utobj().generateTestData(dataSet.get("regionName"));
			String firstNameF = fc.utobj().generateTestData(dataSet.get("firstNameFid"));
			String lastNameF = fc.utobj().generateTestData(dataSet.get("lastNameFid"));
			franchiseLocation.addFranchiseLocationForMkt(driver, franchiseId, regionName, firstNameF, lastNameF);

			// Add Franchise User
			fc.utobj().printTestStep("Admin > Users > Manage Franchise Users");
			fc.utobj().printTestStep("Add Franchise User");
			AdminUsersManageManageFranchiseUsersPageTest addFranUser = new AdminUsersManageManageFranchiseUsersPageTest();
			String userNameF = fc.utobj().generateTestData(dataSet.get("userNameF"));
			String roleName = "Default Franchise Role";
			String emailId = "crmautomation@staffex.com";
			addFranUser.addFranchiseUser(driver, userNameF, password, franchiseId, roleName, emailId);

			// Add Regional User
			fc.utobj().printTestStep("Admin > Users > Manage Regional Users");
			fc.utobj().printTestStep("Add Regional User");
			AdminUsersManageRegionalUsersAddRegionalUserPageTest regionalUserPage = new AdminUsersManageRegionalUsersAddRegionalUserPageTest();
			String userNameReg = fc.utobj().generateTestData(dataSet.get("userNameReg"));
			regionalUserPage.addRegionalUser(driver, userNameReg, password, regionName, emailId);

			String firstName = fc.utobj().generateTestData(dataSet.get("firstName"));
			String lastName = fc.utobj().generateTestData(dataSet.get("lastName"));
			String company = fc.utobj().generateTestData(dataSet.get("company"));
			String assignTo = "Franchise";
			// String email = dataSet.get("emailId");
			String city = fc.utobj().generateTestData(dataSet.get("city"));
			String address = fc.utobj().generateTestData(dataSet.get("address"));
			String suffix = fc.utobj().generateTestData(dataSet.get("suffix"));
			String jobTitle = fc.utobj().generateTestData(dataSet.get("jobTitle"));
			String comment = fc.utobj().generateTestData(dataSet.get("comment"));
			String userName = "";

			fc.home_page().logout(driver);

			fc.utobj().printTestStep("Login With Regional User");

			fc.loginpage().loginWithParameter(driver, userNameReg, password);

			String franchiseUser = firstNameF + " " + lastNameF;
			fc.utobj().printTestStep("Navigate To CRM > Leads > Leads Summary");
			fc.utobj().printTestStep("Add Lead");
			addLead(driver, dataSet, firstName, lastName, company, assignTo, city, address, suffix, jobTitle, comment,
					userName, regionName, userNameReg, franchiseId, franchiseUser, config);

			fc.utobj().printTestStep("Verify Lead At Leads Page");
			fc.utobj().clickElement(driver, pobj.leadsLink);
			searchLeadByOwner(driver, franchiseUser, "Open Leads");
			try {
				fc.utobj().selectDropDown(driver, pobj.resultsPerPage, "100");
			} catch (Exception e) {
			}

			boolean verifyName = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[.='" + firstName + " " + lastName + "']");
			if (verifyName == false) {
				fc.utobj().throwsException("was not able to verify Name");
			}
			boolean ownerOfLead = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[.='" + firstName + " " + lastName
					+ "']/ancestor::tr/td[contains(text () , '" + franchiseUser + "')]");
			if (ownerOfLead == false) {
				fc.utobj().throwsException("was not able to verify owner of the lead");
			}
			boolean statusOfLead = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[.='" + firstName + " " + lastName + "']/ancestor::tr/td[contains(text () , 'New')]");
			if (statusOfLead == false) {
				fc.utobj().throwsException("was not able to verify staus of the lead");
			}
			/*boolean emailOfLead = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[.='" + firstName + " " + lastName
					+ "']/ancestor::tr/td/a[contains(text () , '" + emailId + "')]");
			if (emailOfLead == false) {
				fc.utobj().throwsException("was not able to verify email of the lead");
			}*/

			fc.home_page().logout(driver);
			fc.utobj().printTestStep("Login With Franchise User");
			fc.loginpage().loginWithParameter(driver, userNameF, password);

			fc.crm().crm_common().CRMLeadsLnk(driver);
			fc.utobj().printTestStep("Verify Lead At Franchise Lead Page");
			try {
				fc.utobj().selectDropDown(driver, pobj.resultsPerPage, "100");
			} catch (Exception e) {
			}

			boolean verifyNameAtF = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[.='" + firstName + " " + lastName + "']");
			if (verifyNameAtF == false) {
				fc.utobj().throwsException("was not able to verify Name");
			}
			boolean isFranchiseIdPresentAtF = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[.='" + firstName + " " + lastName
					+ "']/ancestor::tr/td[contains(text () , '" + franchiseId + "')]");
			if (isFranchiseIdPresentAtF == false) {
				fc.utobj().throwsException("was not able to verify Franchise Id");
			}
			boolean ownerOfLeadAtF = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[.='" + firstName + " " + lastName
					+ "']/ancestor::tr/td[contains(text () , '" + franchiseUser + "')]");
			if (ownerOfLeadAtF == false) {
				fc.utobj().throwsException("was not able to verify owner of the lead");
			}
			boolean statusOfLeadAtF = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[.='" + firstName + " " + lastName + "']/ancestor::tr/td[contains(text () , 'New')]");
			if (statusOfLeadAtF == false) {
				fc.utobj().throwsException("was not able to verify staus of the lead");
			}
			/*boolean emailOfLeadAtF = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[.='" + firstName + " " + lastName
					+ "']/ancestor::tr/td/a[contains(text () , '" + emailId + "')]");
			if (emailOfLeadAtF == false) {
				fc.utobj().throwsException("was not able to verify email of the lead");
			}*/

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = {"crm"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2017-06-30", testCaseDescription = "Verify the Add Lead From Franchise User At CRM > Lead > Lead Summary", testCaseId = "TC_105_Add_Lead_Franchise_User")
	public void addLeadFranchiseUser() throws IOException, Exception, ParserConfigurationException, SAXException {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			CRMLeadsPage pobj = new CRMLeadsPage(driver);
			String password = "T0n1ght1";

			// add Franchise user
			fc.utobj().printTestStep("Navigate To Admin > Franchise Location > Add Franchise Location");
			AdminFranchiseLocationAddFranchiseLocationPageTest franchiseLocation = new AdminFranchiseLocationAddFranchiseLocationPageTest();
			String franchiseId = fc.utobj().generateTestData(dataSet.get("franchiseId"));
			String regionName = fc.utobj().generateTestData(dataSet.get("regionName"));
			String firstNameF = fc.utobj().generateTestData(dataSet.get("firstNameFid"));
			String lastNameF = fc.utobj().generateTestData(dataSet.get("lastNameFid"));
			franchiseLocation.addFranchiseLocationForMkt(driver, franchiseId, regionName, firstNameF, lastNameF);

			// Add Franchise User
			fc.utobj().printTestStep("Admin > Users > Manage Franchise Users");
			fc.utobj().printTestStep("Add Franchise User");
			AdminUsersManageManageFranchiseUsersPageTest addFranUser = new AdminUsersManageManageFranchiseUsersPageTest();
			String userNameF = fc.utobj().generateTestData(dataSet.get("userNameF"));
			String roleName = "Default Franchise Role";
			String emailId = "crmautomation@staffex.com";
			addFranUser.addFranchiseUser(driver, userNameF, password, franchiseId, roleName, emailId);

			String firstName = fc.utobj().generateTestData(dataSet.get("firstName"));
			String lastName = fc.utobj().generateTestData(dataSet.get("lastName"));
			String company = fc.utobj().generateTestData(dataSet.get("company"));
			String assignTo = "Only Franchise";
			String email = dataSet.get("emailId");
			String city = fc.utobj().generateTestData(dataSet.get("city"));
			String address = fc.utobj().generateTestData(dataSet.get("address"));
			String suffix = fc.utobj().generateTestData(dataSet.get("suffix"));
			String jobTitle = fc.utobj().generateTestData(dataSet.get("jobTitle"));
			String comment = fc.utobj().generateTestData(dataSet.get("comment"));
			String userName = "";
			String userNameReg = "";

			fc.home_page().logout(driver);

			fc.utobj().printTestStep("Login With Franchise User");

			fc.loginpage().loginWithParameter(driver, userNameF, password);

			String franchiseUser = firstNameF + " " + lastNameF;
			fc.utobj().printTestStep("Navigate To CRM > Leads > Leads Summary");
			fc.utobj().printTestStep("Add Lead");
			addLead(driver, dataSet, firstName, lastName, company, assignTo, city, address, suffix, jobTitle, comment,
					userName, regionName, userNameReg, franchiseId, franchiseUser, config);

			// verify at Home Page
			fc.utobj().clickElement(driver, pobj.leadsLink);
			fc.utobj().printTestStep("Verify At Leads Page");
			try {
				fc.utobj().selectDropDown(driver, pobj.resultsPerPage, "100");
			} catch (Exception e) {
			}

			boolean verifyName = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[.='" + firstName + " " + lastName + "']");
			if (verifyName == false) {
				fc.utobj().throwsException("was not able to verify Name");
			}
			boolean isFranchiseIdPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[.='" + firstName + " " + lastName
					+ "']/ancestor::tr/td[contains(text () , '" + franchiseId + "')]");
			if (isFranchiseIdPresent == false) {
				fc.utobj().throwsException("was not able to verify Franchise Id");
			}
			boolean ownerOfLead = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[.='" + firstName + " " + lastName
					+ "']/ancestor::tr/td[contains(text () , '" + franchiseUser + "')]");
			if (ownerOfLead == false) {
				fc.utobj().throwsException("was not able to verify owner of the lead");
			}
			boolean statusOfLead = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[.='" + firstName + " " + lastName + "']/ancestor::tr/td[contains(text () , 'New')]");
			if (statusOfLead == false) {
				fc.utobj().throwsException("was not able to verify staus of the lead");
			}
			/*boolean emailOfLead = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[.='" + firstName + " " + lastName
					+ "']/ancestor::tr/td/a[contains(text () , '" + email + "')]");
			if (emailOfLead == false) {
				fc.utobj().throwsException("was not able to verify email of the lead");
			}*/

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	/* Harish Dwivedi TC_282_Verify_Search */

	@Test(groups = "crmmodule")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify the Add Contact At CRM > Lead > Lead Summary with corporate User", testCaseId = "TC_282_Verify_Search")
	public void addContactWithCorporateUsertoVerifyAdressSerach()
			throws IOException, Exception, ParserConfigurationException, SAXException {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			CRMLeadsPage pobj = new CRMLeadsPage(driver);

			CRMContactsPageTest contactsPage = new CRMContactsPageTest();
			fc.utobj().printTestStep("Naviaget To Admin Users Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			AdminUsersManageCorporateUsersAddCorporateUserPageTest addCorporatePage = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			String emailId = "crmautomation@staffex.com";
			CorporateUser corpUser = new CorporateUser();
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corpUser.setEmail(emailId);
			corpUser = addCorporatePage.createDefaultUser(driver, corpUser);

			String contactType = "Contacts";
			fc.home_page().logout(driver);
			fc.utobj().printTestStep("Login With Corporate User");
			fc.loginpage().loginWithParameter(driver, corpUser.getUserName(), corpUser.getPassword());

			fc.utobj().printTestStep("Navigating to CRM >  Leads > Leads Summary");
			fc.utobj().printTestStep("Add Contact");

			fc.crm().crm_common().CRMLeadsLnk(driver);
			fc.utobj().clickElement(driver, pobj.addNew);
			fc.utobj().clickElement(driver, pobj.contactLnk);

			String firstName = fc.utobj().generateTestData(dataSet.get("firstName"));
			String lastName = fc.utobj().generateTestData(dataSet.get("lastName"));
			String address = fc.utobj().generateTestData(dataSet.get("address"));

			String primaryCMethod = dataSet.get("primaryCMethod");
			String assignTo = "Corporate";
			String regionName = "";
			String franchiseId = "";
			String title = dataSet.get("title");
			String email = "automation@gmail.com";

			contactsPage.addContactGenericWithAdditionalParameter(driver, contactType, firstName, lastName,
					primaryCMethod, assignTo, corpUser.getuserFullName(), regionName, franchiseId, title, email,
					address, config);
			// fc.utobj().clickElement(driver, pobj.saveBtn);

			fc.utobj().printTestStep("Verify Contatct in details page");

			boolean isContactTypePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () ,'Contact Type')]/ancestor::tr/td[.='" + contactType + "']");
			if (isContactTypePresent == false) {
				fc.utobj().throwsException("was not able to verify Contact Type");
			}
			boolean isContactNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () ,'Name')]/ancestor::tr/td[.='Mr. " + firstName + " " + lastName + "']");
			if (isContactNamePresent == false) {
				fc.utobj().throwsException("was not able to verify Contact Name");
			}
			boolean isPrimaryContactTypePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () ,'Primary Contact Method')]/ancestor::tr/td[.='Email']");
			if (isPrimaryContactTypePresent == false) {
				fc.utobj().throwsException("was not able to verify Primary Contact Type");
			}
			boolean isUserPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () ,'Assign To ')]/ancestor::tr/td[contains(text () , '"
							+ corpUser.getuserFullName() + "')]");
			if (isUserPresent == false) {
				fc.utobj().throwsException("was not able to verify Assign To");
			}

			fc.utobj().printTestStep("Verify Contatct in Serach page");

			fc.utobj().clickElement(driver, pobj.searchLink);

			fc.utobj().getElementByID(driver, "C:ADDRESS:TEXT").sendKeys("" + address);

			fc.utobj().clickElement(driver, pobj.searchBtnAtSearch);

			isContactNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firstName + " " + lastName + "')]");
			if (isContactNamePresent == false) {
				fc.utobj().throwsException("was not able to verify contact Name at Contact Page");
			}
			isUserPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[contains(text () ,'" + firstName + " " + lastName
					+ "')]/ancestor::tr/td[contains(text () , '" + corpUser.getuserFullName() + "')]");
			if (isUserPresent == false) {
				fc.utobj().throwsException("was not able to verify Owner");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	/*
	 * Harish Dwivedi TC_284_Verify_Status_Contact_Deatiled_History +
	 * TC_289_Verify_Assign_To
	 */

	@Test(groups = "crmmodule")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify the Add Contact At CRM > Lead > Lead Summary with Franchisee User", testCaseId = "TC_284_Verify_Status_Contact_Deatiled_History")
	public void addContactWithFranchiseeUserWithDetailedHistory()
			throws IOException, Exception, ParserConfigurationException, SAXException {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			CRMLeadsPage pobj = new CRMLeadsPage(driver);

			CRMContactsPageTest contactsPage = new CRMContactsPageTest();
			String password = "T0n1ght1";

			// add Franchise user
			fc.utobj().printTestStep("Naviaget To Admin > Franchise Location > Add Franchise Location");
			AdminFranchiseLocationAddFranchiseLocationPageTest franchiseLocation = new AdminFranchiseLocationAddFranchiseLocationPageTest();
			String franchiseId = fc.utobj().generateTestData(dataSet.get("franchiseId"));

			// Add Region
			fc.utobj().printTestStep("Add area region");
			String regionName = fc.utobj().generateTestData("TestRegion");
			String zipCode = fc.utobj().generateRandomNumber();
			/*
			 * fc.adminpage().adminAreaRegionAddAreaRegionPage( driver);
			 * AdminAreaRegionAddAreaRegionPage pobjReg = new
			 * AdminAreaRegionAddAreaRegionPage(driver);
			 * fc.utobj().selectDropDown(driver,pobjReg.category, "Domestic");
			 * fc.utobj().sendKeys(driver,pobjReg.aregRegionName , regionName);
			 * fc.utobj().selectDropDown(driver,pobjReg.groupBy, "Zip Codes");
			 * fc.utobj().clickElement(driver,pobjReg.all);
			 * fc.utobj().clickElement(driver,pobjReg.commaSeparated);
			 * fc.utobj().sendKeys(driver,pobjReg.ziplist,zipCode);
			 * fc.utobj().clickElement(driver,pobjReg.Submit);
			 */ String firstNameF = fc.utobj().generateTestData(dataSet.get("firstNameFid"));
			String lastNameF = fc.utobj().generateTestData(dataSet.get("lastNameFid"));

			franchiseLocation.addFranchiseLocationForMkt(driver, franchiseId, regionName, firstNameF, lastNameF);

			// Add Franchise User
			fc.utobj().printTestStep("Admin > Users > Manage Franchise Users");
			fc.utobj().printTestStep("Add Franchise User");
			AdminUsersManageManageFranchiseUsersPageTest addFranUser = new AdminUsersManageManageFranchiseUsersPageTest();
			String userNameF = fc.utobj().generateTestData(dataSet.get("userNameF"));
			String roleName = "Default Franchise Role";
			String emailId = "crmautomation@staffex.com";
			addFranUser.addFranchiseUser(driver, userNameF, password, franchiseId, roleName, emailId);

			AdminCRMContactTypeConfigurationPageTest contatcTypePage = new AdminCRMContactTypeConfigurationPageTest();
			String contactType = fc.utobj().generateTestData(dataSet.get("contactType"));
			contatcTypePage.addContactType(driver, contactType);

			fc.home_page().logout(driver);
			fc.utobj().printTestStep("Login With Franchise User");
			fc.loginpage().loginWithParameter(driver, userNameF, password);
			String franchiseUser = firstNameF + " " + lastNameF;

			fc.utobj().printTestStep("Navigating to CRM >  Leads > Leads Summary");
			fc.utobj().printTestStep("Add Contact");

			fc.crm().crm_common().CRMLeadsLnk(driver);
			fc.utobj().clickElement(driver, pobj.addNew);
			fc.utobj().clickElement(driver, pobj.contactLnk);

			String firstName = fc.utobj().generateTestData(dataSet.get("firstName"));
			String lastName = fc.utobj().generateTestData(dataSet.get("lastName"));
			String address = fc.utobj().generateTestData(dataSet.get("address"));
			// String userName =
			String primaryCMethod = dataSet.get("primaryCMethod");
			String assignTo = "Only Franchise";
			String title = dataSet.get("title");
			String email = "automation@gmail.com";

			contactsPage.addContactGenericWithAdditionalParameter(driver, contactType, firstName, lastName,
					primaryCMethod, assignTo, franchiseUser, regionName, franchiseId, title, email, address, config);

			fc.utobj().printTestStep("Verify Contatct");

			boolean isContactTypePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () ,'Contact Type')]/ancestor::tr/td[.='" + contactType + "']");
			if (isContactTypePresent == false) {
				fc.utobj().throwsException("was not able to verify Contact Type");
			}
			boolean isContactNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () ,'Name')]/ancestor::tr/td[.='Mr. " + firstName + " " + lastName + "']");
			if (isContactNamePresent == false) {
				fc.utobj().throwsException("was not able to verify Contact Name");
			}
			boolean isPrimaryContactTypePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () ,'Primary Contact Method')]/ancestor::tr/td[.='Email']");
			if (isPrimaryContactTypePresent == false) {
				fc.utobj().throwsException("was not able to verify Primary Contact Type");
			}
			boolean isUserPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () ,'Assign To ')]/ancestor::tr/td[contains(text () , '" + franchiseUser
							+ "')]");
			if (isUserPresent == false) {
				fc.utobj().throwsException("was not able to verify Assign To");
			}

			// details History
			fc.utobj().printTestStep("Verify Status Change Activity History At Detailed History Frame");
			fc.utobj().clickElement(driver, pobj.detailedHistoryLink);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().clickElement(driver, pobj.StatusChange);
			fc.utobj().isTextDisplayed(driver, "New",
					"was not able to verify Status Changed To at deatiled history pop up");
			fc.utobj().isTextDisplayed(driver, franchiseUser,
					"was not able to verify  Status Changed By of contact at deatiled history pop up");
			fc.utobj().switchFrameToDefault(driver);
			// end details history

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "crmmodule")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Add Lead Status At Admin> Modify Lead Status At Admin > Verify the Lead Ststus at Contact Primary Info", testCaseId = "TC_296_Verify_Lead_Status_Modification")
	public void verifyAddModifyLeadStatus() throws IOException, Exception, ParserConfigurationException, SAXException {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			CRMLeadsPage pobj = new CRMLeadsPage(driver);
			AdminCRMConfigureStatusPageTest nnpoj = new AdminCRMConfigureStatusPageTest();

			fc.utobj().printTestStep("Naviaget To Admin Users Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			AdminUsersManageCorporateUsersAddCorporateUserPageTest addCorporatePage = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			String emailId = "crmautomation@staffex.com";
			CorporateUser corpUser = new CorporateUser();
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corpUser.setEmail(emailId);
			corpUser = addCorporatePage.createDefaultUser(driver, corpUser);

			fc.home_page().logout(driver);
			fc.loginpage().loginWithParameter(driver, corpUser.getUserName(), corpUser.getPassword());
			fc.utobj().printTestStep("Navigate To Admin CRM > Configure Status Page");
			fc.utobj().printTestStep("Add Lead Status");
			AdminCRMConfigureStatusPageTest statusPage = new AdminCRMConfigureStatusPageTest();
			String statusName = fc.utobj().generateTestData(dataSet.get("status"));
			statusPage.addStatus(driver, statusName);
			nnpoj.actionImgOption(driver, statusName, "Modify");
			fc.commonMethods().switch_cboxIframe_frameId(driver);

			fc.utobj().sendKeys(driver, pobj.LeadStatusName, statusName);
			fc.utobj().clickElement(driver, pobj.ModifyStatusBtn);
			Thread.sleep(5000);

			AdminCRMConfigureStatusPageUI npobj = new AdminCRMConfigureStatusPageUI(driver);
			/* fc.crm().crm_common().adminCRMConfigureStatusLnk( driver); */
			fc.utobj().clickElement(driver, npobj.leads);
			fc.utobj().clickElement(driver, npobj.addStatus);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().sendKeys(driver, npobj.statusTxBx, statusName);
			fc.utobj().clickElement(driver, npobj.addBtn);
			fc.utobj().acceptAlertBox(driver);
			fc.utobj().printTestStep("Verified Add Contact Status with same name");
			//
			fc.utobj().clickElement(driver, npobj.CancelStatusBtn);
			fc.utobj().switchFrameToDefault(driver);
			// fc.utobj().switchFrameToDefault(driver);

			// statusPage.addStatus01(driver, statusName, config);

			fc.utobj().printTestStep("Navigate To Admin > CRM > Contact Type Configuration");
			fc.utobj().printTestStep("Add Contact Status");
			String statusName1 = fc.utobj().generateTestData(dataSet.get("status"));
			statusPage.addStatus(driver, statusName1);
			nnpoj.actionImgOption(driver, statusName1, "Modify");
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().sendKeys(driver, pobj.LeadStatusName, statusName);
			fc.utobj().clickElement(driver, pobj.ModifyStatusBtn);
			fc.utobj().acceptAlertBox(driver);
			fc.utobj().printTestStep("Verified Modify Contact Status with same name");
			fc.utobj().clickElement(driver, npobj.CancelStatusBtn);

			fc.utobj().printTestStep("Navigate To Admin > CRM > Contact Type Configuration");
			fc.utobj().printTestStep("Add Lead Type");

			String firstName = fc.utobj().generateTestData(dataSet.get("firstName"));
			String lastName = fc.utobj().generateTestData(dataSet.get("lastName"));
			String primaryCMethod = dataSet.get("primaryContactMethod");
			String assignTo = "Corporate";
			String regionName = "";
			String regionalUser = "";
			String franchiseId = "";
			String city = "new city";
			String address = "";
			String suffix = "";
			String jobTitle = "";
			String comment = "";
			String franchiseUser = "";
			String title = dataSet.get("title");
			String company = "";
			String email = "automation@gmail.com";

			fc.utobj().printTestStep("Navigate To CRM > Leadd > Contactsss Summary Page");
			fc.utobj().printTestStep("Add Lead");
			addLead(driver, dataSet, firstName, lastName, company, assignTo, city, address, suffix, jobTitle, comment,
					corpUser.getuserFullName(), regionName, regionalUser, franchiseId, franchiseUser, config);
			fc.utobj().printTestStep("Change Status");
			fc.utobj().selectDropDown(driver, pobj.leadStatus, statusName);
			fc.utobj().clickElement(driver, pobj.ChangeStatusBtn);

			// Delete functionality not available

			boolean isTextPresent = fc.utobj().assertPageSource(driver, statusName);
			if (isTextPresent == false) {
				fc.utobj().throwsException("was not able to verify Contact Source Field");
			}
			fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "crmmodule")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Add Lead Type At Admin> Modify Lead Type At Admin > Verify the Lead Type at Contact Primary Info", testCaseId = "TC_297_Verify_Lead_Type_Modification")
	public void verifyAddModifyLeadType() throws IOException, Exception, ParserConfigurationException, SAXException {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			CRMLeadsPage nnpobj = new CRMLeadsPage(driver);
			AdminCRMConfigureLeadTypePageTest npobj = new AdminCRMConfigureLeadTypePageTest();
			AdminCRMConfigureLeadTypePage pobj = new AdminCRMConfigureLeadTypePage(driver);
			fc.utobj().printTestStep("Naviaget To Admin Users Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			AdminUsersManageCorporateUsersAddCorporateUserPageTest addCorporatePage = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			String emailId = "crmautomation@staffex.com";
			CorporateUser corpUser = new CorporateUser();
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corpUser.setEmail(emailId);
			corpUser = addCorporatePage.createDefaultUser(driver, corpUser);
			// String userName="FranConnect Adminstrator";
			/*
			 * fc.home_page().logout(driver, config);
			 * fc.loginpage().loginWithParameter(driver, userNameCorp,
			 * "T0n1ght1");
			 */
			fc.utobj().printTestStep("Navigate To Admin > CRM > Lead Type Configuration");
			fc.utobj().printTestStep("Add Lead Type");
			String leadType = fc.utobj().generateTestData(dataSet.get("leadType"));
			npobj.addLeadType(driver, leadType);

			fc.utobj().actionImgOption(driver, leadType, "Modify");
			fc.commonMethods().switch_cboxIframe_frameId(driver);

			fc.utobj().sendKeys(driver, pobj.leadType, leadType);
			fc.utobj().clickElement(driver, pobj.leadTypeModify);
			// Thread.sleep(5000);
			fc.utobj().clickElement(driver, pobj.Close);
			fc.utobj().switchFrameToDefault(driver);

			fc.crm().crm_common().adminCRMConfigureLeadTypeLnk(driver);
			fc.utobj().clickElement(driver, pobj.addLead);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().sendKeys(driver, pobj.leadType, leadType);
			fc.utobj().clickElement(driver, pobj.addBtn);
			fc.utobj().acceptAlertBox(driver);
			fc.utobj().clickElement(driver, pobj.closeBtn);
			fc.utobj().printTestStep("Verified Add Lead Type with same name");

			fc.utobj().printTestStep("Navigate To Admin > CRM > Lead Type Configuration");
			fc.utobj().printTestStep("Add Contact Type");
			String leadType1 = fc.utobj().generateTestData(dataSet.get("leadType"));
			npobj.addLeadType(driver, leadType1);
			// fc.utobj().clickElement(driver, pobj.Close);
			fc.utobj().printTestStep("Modify Lead Type");
			fc.utobj().actionImgOption(driver, leadType1, "Modify");
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().sendKeys(driver, pobj.leadModifyName, leadType);
			fc.utobj().clickElement(driver, pobj.saveBtn);
			fc.utobj().acceptAlertBox(driver);
			fc.utobj().printTestStep("Verified Modify Contact Status with same name");
			fc.utobj().clickElement(driver, pobj.closeBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Navigate To Admin > CRM > Contact Type Configuration");
			fc.utobj().printTestStep("Add Lead Type");

			String firstName = fc.utobj().generateTestData(dataSet.get("firstName"));
			String lastName = fc.utobj().generateTestData(dataSet.get("lastName"));
			String primaryCMethod = dataSet.get("primaryContactMethod");
			String assignTo = "Corporate";
			String regionName = "";
			String regionalUser = "";
			String franchiseId = "";
			String city = "new city";
			String address = "";
			String suffix = "";
			String jobTitle = "";
			String comment = "";
			String franchiseUser = "";
			String title = dataSet.get("title");
			String company = "";
			String email = "automation@gmail.com";

			fc.utobj().printTestStep("Navigate To CRM > Leadd > Contactsss Summary Page");
			fc.utobj().printTestStep("Add Lead");
			// addLead(driver, dataSet, firstName, lastName, company, assignTo,
			// city, address, suffix, jobTitle, comment, userName, regionName,
			// regionalUser, franchiseId, franchiseUser, config);

			fc.crm().crm_common().CRMLeadsLnk(driver);
			fc.utobj().clickElement(driver, nnpobj.addNew);
			fc.utobj().clickElement(driver, nnpobj.leadLnk);
			fc.utobj().selectDropDown(driver, nnpobj.title, dataSet.get("title"));
			fc.utobj().sendKeys(driver, nnpobj.leadFirstName, firstName);
			fc.utobj().sendKeys(driver, nnpobj.leadLastName, lastName);
			fc.utobj().sendKeys(driver, nnpobj.companyName, company);
			fc.utobj().selectDropDown(driver, nnpobj.primaryContactMethodSelect, dataSet.get("primaryContactMethod"));

			if (assignTo.equalsIgnoreCase("Corporate")) {
				if (!fc.utobj().isSelected(driver, nnpobj.assignToCorporate)) {
					fc.utobj().clickElement(driver, nnpobj.assignToCorporate);
				}
				fc.utobj().selectDropDown(driver, nnpobj.selectCorporateUser, corpUser.getuserFullName());
			} else if (assignTo.equalsIgnoreCase("Regional")) {
				if (!fc.utobj().isSelected(driver, nnpobj.assignToRegional)) {
					fc.utobj().clickElement(driver, nnpobj.assignToRegional);
				}
				fc.utobj().selectDropDown(driver, nnpobj.selectAreaRegion, regionName);

				fc.utobj().selectDropDown(driver, nnpobj.selectRegionalUser, regionalUser);

			} else if (assignTo.equalsIgnoreCase("Franchise")) {
				if (!fc.utobj().isSelected(driver, nnpobj.assignToFranchise)) {
					fc.utobj().clickElement(driver, nnpobj.assignToFranchise);
				}
				fc.utobj().selectDropDown(driver, nnpobj.selectFranchiseId, franchiseId);
				fc.utobj().selectDropDown(driver, nnpobj.selectFranchiseUser, franchiseUser);
			} else if (assignTo.equalsIgnoreCase("Only Franchise")) {
				fc.utobj().selectDropDown(driver, nnpobj.selectFranchiseUser, franchiseUser);

			}
			fc.utobj().sendKeys(driver, nnpobj.address, address);
			fc.utobj().sendKeys(driver, nnpobj.city, city);
			fc.utobj().selectDropDown(driver, nnpobj.country, "USA");
			fc.utobj().selectDropDown(driver, nnpobj.state, "Alabama");
			fc.utobj().sendKeys(driver, nnpobj.zipcode, "12345");
			fc.utobj().sendKeys(driver, nnpobj.phoneNumbers, "1236547896");
			fc.utobj().sendKeys(driver, nnpobj.extn, "12");
			fc.utobj().sendKeys(driver, nnpobj.faxNumbers, "1263985487");
			fc.utobj().sendKeys(driver, nnpobj.mobileNumbers, "8896724441");
			fc.utobj().sendKeys(driver, nnpobj.emailIds, email);
			fc.utobj().sendKeys(driver, nnpobj.alternateEmail, "test@gmail.com");
			fc.utobj().sendKeys(driver, nnpobj.suffix, suffix);
			fc.utobj().sendKeys(driver, nnpobj.position, jobTitle);
			fc.utobj().sendKeys(driver, nnpobj.birthdate, "11/24/1980");
			fc.utobj().sendKeys(driver, nnpobj.anniversarydate, "11/24/1999");
			fc.utobj().selectDropDown(driver, nnpobj.leadType, leadType);
			fc.utobj().selectDropDown(driver, nnpobj.rating, dataSet.get("rating"));
			fc.utobj().sendKeys(driver, nnpobj.bestTimeToContact, "12:00 PM EST");
			fc.utobj().sendKeys(driver, nnpobj.comments, comment);
			fc.utobj().clickElement(driver, nnpobj.saveBtn);

			// Delete functionality not available

			boolean isTextPresent = fc.utobj().assertPageSource(driver, leadType);
			if (isTextPresent == false) {
				fc.utobj().throwsException("was not able to verify Contact Source Field");
			}
			fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	// Anukaran

	@Test(groups = { "crmmodule", "crm" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "composing the email click on keywords and add location keywords >The keywords for owner should be displayed and replaced in the sent email. ", testCaseId = "TC_330_Verfy_email_Keyword_Lead")
	public void verifiEmailKeywordLead() throws IOException, Exception, ParserConfigurationException, SAXException {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			CRMContactsPage npobj = new CRMContactsPage(driver);
			CRMLeadsPage pobj = new CRMLeadsPage(driver);
			String regionName = fc.utobj().generateTestData("TestRegion");
			String firstNameF = fc.utobj().generateTestData(dataSet.get("firstNameFid"));
			String lastNameF = fc.utobj().generateTestData(dataSet.get("lastNameFid"));
			String OwnerName = lastNameF + " " + firstNameF;
			fc.utobj().printTestStep("Naviaget To Admin > Franchise Location > Add Franchise Location");
			AdminFranchiseLocationAddFranchiseLocationPageTest franchiseLocation = new AdminFranchiseLocationAddFranchiseLocationPageTest();
			String franchiseId = fc.utobj().generateTestData(dataSet.get("franchiseId"));
			franchiseLocation.addFranchiseLocationForMkt(driver, franchiseId, regionName, firstNameF, lastNameF);

			// login as franchise user
			fc.utobj().printTestStep("Admin > Users > Manage Franchise Users");
			fc.utobj().printTestStep("Add Franchise User");
			AdminUsersManageManageFranchiseUsersPageTest addFranUser = new AdminUsersManageManageFranchiseUsersPageTest();
			String userNameF = fc.utobj().generateTestData(dataSet.get("userNameF"));
			String userName = "";
			String roleName = "Default Franchise Role";
			String password = "t0n1ght@123";
			String emailId = "crmautomation@staffex.com";
			userNameF = addFranUser.addFranchiseUser(driver, userNameF, password, franchiseId, roleName, emailId);
			fc.utobj().printTestStep("Logging as franchisee user");
			fc.home_page().logout(driver);
			fc.loginpage().loginWithParameter(driver, userNameF, "t0n1ght@123");

			String firstName = fc.utobj().generateTestData(dataSet.get("firstName"));
			String lastName = fc.utobj().generateTestData(dataSet.get("lastName"));
			String company = fc.utobj().generateTestData(dataSet.get("company"));
			String assignTo = "Only Franchise";
			String city = fc.utobj().generateTestData(dataSet.get("city"));
			String address = fc.utobj().generateTestData(dataSet.get("address"));
			String suffix = fc.utobj().generateTestData(dataSet.get("suffix"));
			String jobTitle = fc.utobj().generateTestData(dataSet.get("jobTitle"));
			String comment = fc.utobj().generateTestData(dataSet.get("comment"));
			String regionalUser = "";
			String franchiseUser = firstNameF + " " + lastNameF;
			
			fc.utobj().printTestStep("Navigate To CRM > Leads > Leads Summary");
			fc.utobj().printTestStep("Add Lead");

			addLead(driver, dataSet, firstName, lastName, company, assignTo, city, address, suffix, jobTitle, comment,
					userName, regionName, regionalUser, franchiseId, franchiseUser, config);

			fc.utobj().clickElement(driver, pobj.leadsLink);
			fc.utobj().sendKeys(driver, pobj.searchLeads, firstName);
			fc.utobj().clickElement(driver, pobj.systemSearchButton);
			fc.utobj().printTestStep("Send Email");
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,(".//a[contains(text () ,'" + firstName + " " + lastName + "')]/ancestor::tr/td/input")));
			fc.utobj().selectActionMenuItemsWithTagA(driver, "Send Email");

			if (!fc.utobj().isSelected(driver, pobj.loggedUserId)) {
				fc.utobj().clickElement(driver, pobj.loggedUserId);
			}

			/*String mailSentToId = fc.utobj().getText(driver, pobj.mailSentTo);
			if (!mailSentToId.equalsIgnoreCase(dataSet.get("emailId"))) {
				fc.utobj().throwsException("was not able to verify mailSentTo");
			}*/
			String subjectMail = fc.utobj().generateTestData(dataSet.get("subjectMail"));
			fc.utobj().sendKeys(driver, pobj.subjectMail, subjectMail);
			String editorText = "Location City: $LOCATION_CITY$";

			fc.utobj().switchFrameById(driver, "ta_ifr");
			Actions actions = new Actions(driver);
			actions.moveToElement(fc.utobj().getElement(driver, pobj.editorTextArea));
			actions.click();
			actions.sendKeys(editorText);
			actions.build().perform();
			fc.utobj().switchFrameToDefault(driver);
			fc.utobj().clickElement(driver, pobj.sendEmailBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Verify Send Email");

			// verify Email Sent to user
			String expectedSubject = subjectMail;
			String expectedMessageBody = "Location City:";

			Map<String, String> mailData = fc.utobj().readMailBox(expectedSubject, expectedMessageBody, emailId,
					"sdg@1a@Hfs");

			if (mailData.size() == 0 || !mailData.get("mailBody").contains("test city")) {
				fc.utobj().throwsException("was not able to verify mail text");
			}
			if (mailData.size() == 0 || !mailData.get("subject").equalsIgnoreCase(subjectMail)) {
				fc.utobj().throwsException("was not able to verify firstname mail");
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	// Anukaran Mishra

	@Test(groups = { "crmmodulewaitformGen" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Add Section with all types of custom fields under the new tab > add a Lead through External Form", testCaseId = "TC_325_Verify_New_Lead_Tab")
	public void verifyNewContactTab() throws IOException, Exception, ParserConfigurationException, SAXException {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		AdminInfoMgrCommonMethods adminCommonMethods = fc.adminpage().adminInfoMgr();
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		// CRM_CorporateUser crmCU= new CRM_CorporateUser();

		try {
			driver = fc.loginpage().login(driver);
			CRMLeadsPage pobj = new CRMLeadsPage(driver);

			/*
			 * fc.utobj().printTestStep(testCaseId,
			 * "Navigate To Admin Users Manage Corporate User");
			 * fc.utobj().printTestStep(testCaseId, "Add Corporate User");
			 * AdminUsersManageCorporateUsersAddCorporateUserPageTest
			 * addCorporatePage = new
			 * AdminUsersManageCorporateUsersAddCorporateUserPageTest(); String
			 * CuserName = fc.utobj().generateTestData(dataSet.get("userName"));
			 * String userName = addCorporatePage.addCorporateUser(driver,
			 * CuserName, config); fc.home_page().logout(driver, config);
			 * fc.loginpage().loginWithParameter(driver, CuserName, "T0n1ght1");
			 */
			fc.utobj().printTestStep("Navigate To Admin > CRM > Manage Form Generator");
			fc.utobj().printTestStep("Add New Tab");
			// fc.crm().crm_common().CRMLeadsLnk( driver);

			fc.crm().crm_common().adminCRMManageFormGeneratorLnk(driver);
			fc.adminpage().adminInfoMgr();
			AdminInfoMgrManageFormGeneratorPage manageFormGeneratorPage = new AdminInfoMgrManageFormGeneratorPage(
					driver);
			AdminInfoMgrManageFormGeneratorTabDetailsPage ADM_form_gen = manageFormGeneratorPage.getAddTabDetailsPage();
			adminCommonMethods.NavigateToManageFormGeneratorTabs(driver, "Lead");

			fc.utobj().clickElement(driver, manageFormGeneratorPage.btnAddNewTab);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			AdminInfoMgrManageFormGeneratorAddTabPage objAddNewTabPage = new AdminInfoMgrManageFormGeneratorAddTabPage(
					driver);
			String newTabName = fc.utobj().generateTestData("New Tab");
			fc.utobj().sendKeys(driver, objAddNewTabPage.txtDisplayName, newTabName);
			fc.utobj().clickElement(driver, objAddNewTabPage.multipleInputNo);
			fc.utobj().clickElement(driver, objAddNewTabPage.btnSubmit);
			fc.utobj().switchFrameToDefault(driver);
			fc.utobj().clickLink(driver, newTabName);

			fc.utobj().clickElement(driver, ADM_form_gen.btnAddSection);

			fc.commonMethods().switch_cboxIframe_frameId(driver);
			String SectionName = "New Section";
			SectionName = fc.utobj().generateTestData(SectionName);

			fc.utobj().sendKeys(driver, ADM_form_gen.txtSectionName, SectionName);
			fc.utobj().clickElement(driver, ADM_form_gen.btnAddSectionName);
			fc.utobj().switchFrameToDefault(driver);
			fc.utobj().clickElement(driver, ADM_form_gen.btnAddField);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			String FieldName2 = "Text Area";
			FieldName2 = fc.utobj().generateTestData(FieldName2);
			fc.utobj().sendKeys(driver, ADM_form_gen.txtFieldName, FieldName2);
			// fc.utobj().clickElement(driver, ADM_form_gen.documentopt);
			fc.utobj().selectDropDown(driver, ADM_form_gen.FieldType, "Text Area");
			fc.utobj().clickElement(driver, ADM_form_gen.btnSumbitField);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().clickElement(driver, ADM_form_gen.btnAddField);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			String FieldName1 = "Text";
			FieldName1 = fc.utobj().generateTestData(FieldName1);
			fc.utobj().sendKeys(driver, ADM_form_gen.txtFieldName, FieldName1);
			// fc.utobj().clickElement(driver, ADM_form_gen.documentopt);
			fc.utobj().selectDropDown(driver, ADM_form_gen.FieldType, "Text");
			fc.utobj().clickElement(driver, ADM_form_gen.btnSumbitField);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().clickElement(driver, ADM_form_gen.btnAddField);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			String FieldName3 = "Date";
			FieldName3 = fc.utobj().generateTestData(FieldName3);
			fc.utobj().sendKeys(driver, ADM_form_gen.txtFieldName, FieldName3);
			// fc.utobj().clickElement(driver, ADM_form_gen.documentopt);
			fc.utobj().selectDropDown(driver, ADM_form_gen.FieldType, "Date");
			fc.utobj().clickElement(driver, ADM_form_gen.btnSumbitField);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().clickElement(driver, ADM_form_gen.btnAddField);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			String FieldName4 = "DropDown";
			FieldName4 = fc.utobj().generateTestData(FieldName4);
			fc.utobj().sendKeys(driver, ADM_form_gen.txtFieldName, FieldName4);
			// fc.utobj().clickElement(driver, ADM_form_gen.documentopt);
			fc.utobj().selectDropDown(driver, ADM_form_gen.FieldType, "Drop-down");
			fc.utobj().sendKeys(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='row_1']/td[2]/input[1]"),
					"Opt" + FieldName4);

			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//*[@id='removeTDForAddMore']//*[contains(text(),'Add Option')]"));
			fc.utobj().sendKeys(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='row_2']/td[2]/input[1]"),
					"Opt2");
			fc.utobj().clickElement(driver, ADM_form_gen.btnSumbitField);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().clickElement(driver, ADM_form_gen.btnAddField);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			String FieldName5 = "Radio";
			FieldName5 = fc.utobj().generateTestData(FieldName5);
			fc.utobj().sendKeys(driver, ADM_form_gen.txtFieldName, FieldName5);
			// fc.utobj().clickElement(driver, ADM_form_gen.documentopt);
			fc.utobj().selectDropDown(driver, ADM_form_gen.FieldType, "Radio");
			fc.utobj().sendKeys(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='row_1']/td[2]/input[1]"),
					"Opt" + FieldName5);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//*[@id='removeTDForAddMore']//*[contains(text(),'Add Option')]"));
			fc.utobj().sendKeys(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='row_2']/td[2]/input[1]"),
					"Opt2" + FieldName5);
			fc.utobj().clickElement(driver, ADM_form_gen.btnSumbitField);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().clickElement(driver, ADM_form_gen.btnAddField);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			String FieldName6 = "Checkbox";
			FieldName6 = fc.utobj().generateTestData(FieldName6);
			fc.utobj().sendKeys(driver, ADM_form_gen.txtFieldName, FieldName6);
			// fc.utobj().clickElement(driver, ADM_form_gen.documentopt);
			fc.utobj().selectDropDown(driver, ADM_form_gen.FieldType, "Checkbox");
			fc.utobj().sendKeys(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='row_1']/td[2]/input[1]"),
					"Opt1");
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//*[@id='removeTDForAddMore']//*[contains(text(),'Add Option')]"));
			fc.utobj().sendKeys(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='row_2']/td[2]/input[1]"),
					"Opt2");
			fc.utobj().clickElement(driver, ADM_form_gen.btnSumbitField);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().clickElement(driver, ADM_form_gen.btnAddField);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			String FieldName7 = "Test Field3";
			FieldName7 = fc.utobj().generateTestData(FieldName7);
			fc.utobj().sendKeys(driver, ADM_form_gen.txtFieldName, FieldName7);
			// fc.utobj().clickElement(driver, ADM_form_gen.documentopt);
			fc.utobj().selectDropDown(driver, ADM_form_gen.FieldType, "Numeric");
			fc.utobj().clickElement(driver, ADM_form_gen.btnSumbitField);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().clickElement(driver, ADM_form_gen.btnAddField);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			String FieldName8 = "Multislect";
			FieldName8 = fc.utobj().generateTestData(FieldName8);
			fc.utobj().sendKeys(driver, ADM_form_gen.txtFieldName, FieldName8);
			// fc.utobj().clickElement(driver, ADM_form_gen.documentopt);
			fc.utobj().selectDropDown(driver, ADM_form_gen.FieldType, "Multi Select Drop-down");
			fc.utobj().sendKeys(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='row_1']/td[2]/input[1]"),
					"Mul" + FieldName8);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//*[@id='removeTDForAddMore']//*[contains(text(),'Add Option')]"));
			fc.utobj().sendKeys(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='row_2']/td[2]/input[1]"),
					"Opt2");
			fc.utobj().clickElement(driver, ADM_form_gen.btnSumbitField);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().clickElement(driver, ADM_form_gen.btnAddField);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			String FieldName9 = "Test Field3";
			FieldName9 = fc.utobj().generateTestData(FieldName9);
			fc.utobj().sendKeys(driver, ADM_form_gen.txtFieldName, FieldName9);
			// fc.utobj().clickElement(driver, ADM_form_gen.documentopt);
			fc.utobj().selectDropDown(driver, ADM_form_gen.FieldType, "Document");
			fc.utobj().clickElement(driver, ADM_form_gen.btnSumbitField);
			fc.utobj().switchFrameToDefault(driver);

			AdminCRMManageWebFormGeneratorPageTest webFormPage = new AdminCRMManageWebFormGeneratorPageTest();
			// create Web form (Lead)

			String status = "New";
			String contactMedium = "Email";
			String leadSource = "Import";
			String sourceDetails = "Import";
			fc.utobj().printTestStep("Navigate To Admin > CRM > Manage Web Form Generator");
			fc.utobj().printTestStep("Create New Lead Form");
			String formFormat = "Single Page";
			String header = fc.utobj().generateTestData(dataSet.get("header"));
			String textLabel = fc.utobj().generateTestData(dataSet.get("textLabel"));
			String fieldLabel = fc.utobj().generateTestData(dataSet.get("fieldLabel"));
			String footer = fc.utobj().generateTestData(dataSet.get("footer"));
			String assignTo = "Default Owner";
			String submissionType = dataSet.get("submissionType");
			String formName = fc.utobj().generateTestData(dataSet.get("formName"));
			String formTitle = fc.utobj().generateTestData(dataSet.get("formTitle"));
			String description = fc.utobj().generateTestData(dataSet.get("description"));
			String formUrl = fc.utobj().generateTestData(dataSet.get("formUrl"));
			String sectionName = fc.utobj().generateTestData("sectionName");
			String userNameReg = "";
			String userNameFranchise = "";
			String afterSubmission = dataSet.get("afterSubmission");
			String tabName = fc.utobj().generateTestData(dataSet.get("tabName"));
			String redirectedUrl = dataSet.get("reUrl");

			String userNameCorp = "FranConnect Administrator";
			String regionName = "";
			String franchiseId = "";
			AdminCRMManageWebFormGeneratorPage CrmWebFormPage = new AdminCRMManageWebFormGeneratorPage(driver);
			// leadSource=sourceDetails;

			webFormPage.createNewFormLeads(driver, formFormat, header, textLabel, fieldLabel, footer, assignTo,
					submissionType, formName, formTitle, description, formUrl, sectionName, userNameCorp, regionName,
					userNameReg, franchiseId, userNameFranchise, status, contactMedium, leadSource, sourceDetails,
					afterSubmission, redirectedUrl, tabName, config);

			String urlText = null;
			AdminCRMManageWebFormGeneratorPage webFormObj = new AdminCRMManageWebFormGeneratorPage(driver);
			fc.crm().crm_common().adminCRMManageWebFormGeneratorLnk(driver);
			fc.utobj().clickElement(driver, webFormObj.createNewForm);
			fc.utobj().sendKeys(driver, webFormObj.formName, formName);
			fc.utobj().sendKeys(driver, webFormObj.formDisplayTitle, formTitle);
			if (!fc.utobj().isSelected(driver,webFormObj.displayFormTitleCheckBox)) {
				fc.utobj().clickElement(driver, webFormObj.displayFormTitleCheckBox);
			}
			fc.utobj().sendKeys(driver, webFormObj.formDescription, description);

			if (!fc.utobj().isSelected(driver, webFormObj.formTypeLeads)) {
				fc.utobj().clickElement(driver, webFormObj.formTypeLeads);
			}
			if (formFormat.equalsIgnoreCase("Single Page")) {
				fc.utobj().selectDropDown(driver, webFormObj.formFormat, "Single Page");

			} else if (formFormat.equalsIgnoreCase("Multi Page")) {
				fc.utobj().selectDropDown(driver, webFormObj.formFormat, "Multi Page");
				fc.utobj().selectDropDown(driver, webFormObj.submissionType, submissionType);
			}
			fc.utobj().selectDropDown(driver, webFormObj.columnCount, "1");
			fc.utobj().selectDropDown(driver, webFormObj.filedLabelAlignment, "Left");
			fc.utobj().selectDropDown(driver, webFormObj.formLanguage, "English");
			fc.utobj().sendKeys(driver, webFormObj.iframeWidth, "100%");
			fc.utobj().sendKeys(driver, webFormObj.iframeHeight, "500");
			fc.utobj().sendKeys(driver, webFormObj.formUrl, formUrl);
			fc.utobj().clickElement(driver, webFormObj.saveNextBtn);

			// 1.Details Page

			if (formFormat.equalsIgnoreCase("Single Page")) {
				// Add Section
				fc.utobj().clickElement(driver, webFormObj.addSection);
				fc.commonMethods().switch_cboxIframe_frameId(driver);
				fc.utobj().sendKeys(driver, webFormObj.sectionName, sectionName);
				fc.utobj().selectDropDown(driver, webFormObj.noOfCols, "1");
				fc.utobj().selectDropDown(driver, webFormObj.fieldLabelAlignmentSection, "Left");
				fc.utobj().clickElement(driver, webFormObj.addBtn);
				fc.utobj().switchFrameToDefault(driver);
			} else if (formFormat.equalsIgnoreCase("Multi Page")) {
				fc.utobj().clickElement(driver, webFormObj.addTab);
				fc.commonMethods().switch_cboxIframe_frameId(driver);
				fc.utobj().sendKeys(driver, webFormObj.sectionName, tabName);
				fc.utobj().clickElement(driver, webFormObj.addBtn);
				fc.utobj().switchFrameToDefault(driver);
			}
			// add Header
			fc.utobj().doubleClickElement(driver, webFormObj.addHeader);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().switchFrameById(driver, "textBlock_ifr");
			WebElement editorTextArea = fc.utobj().getElementByXpath(driver, ".//*[@id='tinymce']/p");
			Actions actions = new Actions(driver);
			actions.moveToElement(editorTextArea);
			actions.click();
			actions.sendKeys(header);
			fc.utobj().logReport("Entering Value in Text Field", editorTextArea);
			fc.utobj().logReportMsg("Entered Text", header);
			actions.build().perform();
			fc.utobj().switchFrameToDefault(driver);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().clickElement(driver, webFormObj.saveBtn);
			fc.utobj().switchFrameToDefault(driver);

			if (formFormat.equalsIgnoreCase("Multi Page")) {
				fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
						".//*[contains(@id , 'defaultTab') and contains(@id , 'Tab') and contains(text () ,'Default Tab')]"));
			}

			// Add Text
			fc.utobj().doubleClickElement(driver, webFormObj.defaultSectionHeaderAddText);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().sendKeys(driver, webFormObj.labelAddText, textLabel);
			fc.utobj().selectDropDown(driver, webFormObj.headerLabelAlignment, "Left");
			fc.utobj().clickElement(driver, webFormObj.saveBtn);
			fc.utobj().switchFrameToDefault(driver);

			// FormTool
			fc.utobj().clickElement(driver, webFormObj.availableFieldsTab);
			fc.utobj().clickElement(driver, webFormObj.formTools);

			fc.utobj().dragAndDropElement(driver, webFormObj.formToolText, webFormObj.defaultDropHere);

			/*
			 * fc.utobj().dragAndDropElement(driver, webFormObj.formToolCaptcha,
			 * webFormObj.defaultDropHere);
			 */
			/*
			 * fc.utobj().dragAndDropElement(driver,
			 * webFormObj.formToolIAgreeCheckBox, webFormObj.defaultDropHere);
			 */

			// Lead Info
			fc.utobj().clickElement(driver, webFormObj.leadInfo);
			fc.utobj().sendKeys(driver, webFormObj.searchField, "First Name");

			fc.utobj().dragAndDropElement(driver, fc.utobj().getElementByXpath(driver, ".//a[.='First Name']"),
					webFormObj.defaultDropHere);

			fc.utobj().sendKeys(driver, webFormObj.searchField, "Title");

			fc.utobj().dragAndDropElement(driver, fc.utobj().getElementByXpath(driver, ".//a[.='Title']"),
					webFormObj.defaultDropHere);

			fc.utobj().sendKeys(driver, webFormObj.searchField, "Company");

			fc.utobj().dragAndDropElement(driver, fc.utobj().getElementByXpath(driver, ".//a[.='Company']"),
					webFormObj.defaultDropHere);

			fc.utobj().sendKeys(driver, webFormObj.searchField, "Address");

			fc.utobj().dragAndDropElement(driver, fc.utobj().getElementByXpath(driver, ".//a[.='Address']"),
					webFormObj.defaultDropHere);

			fc.utobj().sendKeys(driver, webFormObj.searchField, "City");

			fc.utobj().dragAndDropElement(driver, fc.utobj().getElementByXpath(driver, ".//a[.='City']"),
					webFormObj.defaultDropHere);

			fc.utobj().sendKeys(driver, webFormObj.searchField, "Country");

			fc.utobj().dragAndDropElement(driver, fc.utobj().getElementByXpath(driver, ".//a[.='Country']"),
					webFormObj.defaultDropHere);

			fc.utobj().sendKeys(driver, webFormObj.searchField, "Zip / Postal Code");

			fc.utobj().dragAndDropElement(driver, fc.utobj().getElementByXpath(driver, ".//a[.='Zip / Postal Code']"),
					webFormObj.defaultDropHere);

			fc.utobj().sendKeys(driver, webFormObj.searchField, "State / Province");

			fc.utobj().dragAndDropElement(driver, fc.utobj().getElementByXpath(driver, ".//a[.='State / Province']"),
					webFormObj.defaultDropHere);

			fc.utobj().sendKeys(driver, webFormObj.searchField, "Phone");

			fc.utobj().dragAndDropElement(driver, fc.utobj().getElementByXpath(driver, ".//a[.='Phone']"),
					webFormObj.defaultDropHere);

			fc.utobj().sendKeys(driver, webFormObj.searchField, "Phone Extension");

			fc.utobj().dragAndDropElement(driver, fc.utobj().getElementByXpath(driver, ".//a[.='Phone Extension']"),
					webFormObj.defaultDropHere);

			fc.utobj().sendKeys(driver, webFormObj.searchField, "Fax");

			fc.utobj().dragAndDropElement(driver, fc.utobj().getElementByXpath(driver, ".//a[.='Fax']"),
					webFormObj.defaultDropHere);

			fc.utobj().sendKeys(driver, webFormObj.searchField, "Mobile");

			fc.utobj().dragAndDropElement(driver, fc.utobj().getElementByXpath(driver, ".//a[.='Mobile']"),
					webFormObj.defaultDropHere);

			fc.utobj().sendKeys(driver, webFormObj.searchField, "Email");

			fc.utobj().dragAndDropElement(driver, fc.utobj().getElementByXpath(driver, ".//a[.='Email']"),
					webFormObj.defaultDropHere);

			fc.utobj().sendKeys(driver, webFormObj.searchField, "Alternate Email");

			fc.utobj().dragAndDropElement(driver, fc.utobj().getElementByXpath(driver, ".//a[.='Alternate Email']"),
					webFormObj.defaultDropHere);

			fc.utobj().sendKeys(driver, webFormObj.searchField, "Job Title");

			fc.utobj().dragAndDropElement(driver, fc.utobj().getElementByXpath(driver, ".//a[.='Job Title']"),
					webFormObj.defaultDropHere);

			fc.utobj().sendKeys(driver, webFormObj.searchField, "Suffix");

			fc.utobj().dragAndDropElement(driver, fc.utobj().getElementByXpath(driver, ".//a[.='Suffix']"),
					webFormObj.defaultDropHere);

			fc.utobj().sendKeys(driver, webFormObj.searchField, "Birthdate");

			fc.utobj().dragAndDropElement(driver, fc.utobj().getElementByXpath(driver, ".//a[.='Birthdate']"),
					webFormObj.defaultDropHere);

			fc.utobj().sendKeys(driver, webFormObj.searchField, "Anniversary Date");

			fc.utobj().dragAndDropElement(driver, fc.utobj().getElementByXpath(driver, ".//a[.='Anniversary Date']"),
					webFormObj.defaultDropHere);

			fc.utobj().sendKeys(driver, webFormObj.searchField, "How did you hear about us?(Lead Source)");

			fc.utobj().dragAndDropElement(driver,
					fc.utobj().getElementByXpath(driver, ".//a[.='How did you hear about us?(Lead Source)']"),
					webFormObj.defaultDropHere);

			fc.utobj().sendKeys(driver, webFormObj.searchField, "Best Time to Contact");

			fc.utobj().dragAndDropElement(driver,
					fc.utobj().getElementByXpath(driver, ".//a[.='Best Time to Contact']"), webFormObj.defaultDropHere);

			fc.utobj().sendKeys(driver, webFormObj.searchField, "Comments");

			fc.utobj().dragAndDropElement(driver, fc.utobj().getElementByXpath(driver, ".//a[.='Comments']"),
					webFormObj.defaultDropHere);

			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//a[contains(text () ,'" + newTabName + "')]"));
			fc.utobj().dragAndDropElement(driver, fc.utobj().getElementByXpath(driver, ".//a[.='Twitter']"),
					webFormObj.defaultDropHere);

			fc.utobj().dragAndDropElement(driver,
					fc.utobj().getElementByXpath(driver, ".//a[contains(text(),'" + FieldName1 + "')]"),
					webFormObj.defaultDropHere);

			fc.utobj().dragAndDropElement(driver,
					fc.utobj().getElementByXpath(driver, ".//a[contains(text(),'" + FieldName2 + "')]"),
					webFormObj.defaultDropHere);

			fc.utobj().dragAndDropElement(driver,
					fc.utobj().getElementByXpath(driver, ".//a[contains(text(),'" + FieldName3 + "')]"),
					webFormObj.defaultDropHere);

			fc.utobj().dragAndDropElement(driver,
					fc.utobj().getElementByXpath(driver, ".//a[contains(text(),'" + FieldName4 + "')]"),
					webFormObj.defaultDropHere);

			fc.utobj().dragAndDropElement(driver,
					fc.utobj().getElementByXpath(driver, ".//a[contains(text(),'" + FieldName5 + "')]"),
					webFormObj.defaultDropHere);

			fc.utobj().dragAndDropElement(driver,
					fc.utobj().getElementByXpath(driver, ".//a[contains(text(),'" + FieldName6 + "')]"),
					webFormObj.defaultDropHere);

			fc.utobj().dragAndDropElement(driver,
					fc.utobj().getElementByXpath(driver, ".//a[contains(text(),'" + FieldName7 + "')]"),
					webFormObj.defaultDropHere);

			fc.utobj().dragAndDropElement(driver,
					fc.utobj().getElementByXpath(driver, ".//a[contains(text(),'" + FieldName8 + "')]"),
					webFormObj.defaultDropHere);

			if (formFormat.equalsIgnoreCase("Single Page")) {
				// add Text in custom section
				fc.utobj().doubleClickElement(driver, driver.findElement(
						By.xpath(".//*[.='" + sectionName + "']/following-sibling::table//ul/li[@class='emptyList']")));
				fc.commonMethods().switch_cboxIframe_frameId(driver);
				fc.utobj().sendKeys(driver, webFormObj.labelAddText, fieldLabel);
				fc.utobj().selectDropDown(driver, webFormObj.headerLabelAlignment, "Left");
				fc.utobj().clickElement(driver, webFormObj.saveBtn);
				fc.utobj().switchFrameToDefault(driver);

				// add Text Drop Here Custom
				fc.utobj().clickElement(driver, webFormObj.formTools);
				fc.utobj().dragAndDropElement(driver, webFormObj.formToolText,
						fc.utobj().getElementByXpath(driver, ".//*[.='" + sectionName
								+ "']/following-sibling::table//li[@class='emptyList']//p[.='Drop here.']"));

			} else if (formFormat.equalsIgnoreCase("Multi Page")) {
				fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//li[.='" + tabName + "']"));

				// add Text in custom section
				// click over double click
				String text = fc.utobj().getElementByXpath(driver, ".//li[.='" + tabName + "']").getAttribute("id");

				String tabName1 = tabName.toLowerCase();
				text = text.replace("_" + tabName1 + "_", "");
				text = text.replace("_Tab", "");

				String text1 = driver
						.findElement(By
								.xpath(".//div[contains(@id , '" + text + "')]/ul[contains(@id , '" + text + "')]/li"))
						.getAttribute("id");
				text1 = text1.replace("defaultSection_", "");

				fc.utobj().doubleClickElement(driver, fc.utobj().getElementByXpath(driver, ".//ul[contains(@id , '"
						+ text1 + "')]/li[@class='emptyList']//p[.='Double click or drag & drop to add text.']"));

				fc.commonMethods().switch_cboxIframe_frameId(driver);
				fc.utobj().sendKeys(driver, webFormObj.labelAddText, fieldLabel);
				fc.utobj().selectDropDown(driver, webFormObj.headerLabelAlignment, "Left");
				fc.utobj().clickElement(driver, webFormObj.saveBtn);
				fc.utobj().switchFrameToDefault(driver);

				// add Text Drop Here Custom
				fc.utobj().clickElement(driver, webFormObj.formTools);

				fc.utobj().dragAndDropElement(driver, webFormObj.formToolText, fc.utobj().getElementByXpath(driver,
						".//ul[contains(@id , '" + text + "')]/li//p[.='Drop here.']"));

			}
			// add Footer
			fc.utobj().doubleClickElement(driver, webFormObj.addFooterDefault);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().switchFrameById(driver, "textBlock_ifr");

			WebElement editorTextArea1 = fc.utobj().getElementByXpath(driver, ".//*[@id='tinymce']/p");
			Actions actions1 = new Actions(driver);
			actions1.moveToElement(editorTextArea1);
			actions1.click();
			actions1.sendKeys(footer);
			fc.utobj().logReport("Entering Value in Text Field", editorTextArea1);
			fc.utobj().logReportMsg("Entered Text", footer);
			actions1.build().perform();
			fc.utobj().switchFrameToDefault(driver);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().clickElement(driver, webFormObj.saveBtn);
			fc.utobj().switchFrameToDefault(driver);
			fc.utobj().clickElement(driver, webFormObj.saveAndNextBtnDesign);

			// settings page
			fc.utobj().selectDropDown(driver, webFormObj.cmLeadStatusID, status);
			fc.utobj().selectDropDown(driver, webFormObj.contactMediumSelect, contactMedium);
			fc.utobj().selectDropDown(driver, webFormObj.leadSource, leadSource);
			fc.utobj().selectDropDown(driver, webFormObj.leadSourceDetailsSelect, sourceDetails);
			if (assignTo.equalsIgnoreCase("default")) {
				if (!fc.utobj().isSelected(driver, webFormObj.assignToDefault)) {
					fc.utobj().clickElement(driver, webFormObj.assignToDefault);
				}
			} else if (assignTo.equalsIgnoreCase("Corporate")) {
				if (!fc.utobj().isSelected(driver, webFormObj.assignToCorporate)) {
					fc.utobj().clickElement(driver, webFormObj.assignToCorporate);
				}
				fc.utobj().selectDropDown(driver, webFormObj.selectCorporateUser, userNameCorp);

			} else if (assignTo.equalsIgnoreCase("Regional")) {
				if (!fc.utobj().isSelected(driver, webFormObj.assignToRegional)) {
					fc.utobj().clickElement(driver, webFormObj.assignToCorporate);
				}
				fc.utobj().selectDropDown(driver, webFormObj.selectAreaRegion, regionName);
				fc.utobj().selectDropDown(driver, webFormObj.selectAreaRegion, userNameReg);

			} else if (assignTo.equalsIgnoreCase("Franchisee")) {

				if (!fc.utobj().isSelected(driver, webFormObj.assignToFranchise)) {
					fc.utobj().clickElement(driver, webFormObj.assignToFranchise);
				}
				fc.utobj().selectDropDown(driver, webFormObj.selectFranchiseID, franchiseId);
				fc.utobj().selectDropDown(driver, webFormObj.selectFranchiseUser, userNameFranchise);

			}
			if (afterSubmission.equalsIgnoreCase("message")) {
				if (!fc.utobj().isSelected(driver, webFormObj.afterSubmissionMsg)) {
					fc.utobj().clickElement(driver, webFormObj.afterSubmissionMsg);
				}
			} else if (afterSubmission.equalsIgnoreCase("url")) {
				if (!fc.utobj().isSelected(driver, webFormObj.afterSubmissionUrl)) {
					fc.utobj().clickElement(driver, webFormObj.afterSubmissionUrl);
				}
				fc.utobj().sendKeys(driver, webFormObj.redirectedUrl, redirectedUrl);
			}

			fc.utobj().clickElement(driver, webFormObj.finishBtn);

			if (!fc.utobj().isSelected(driver,webFormObj.hostURL)){
				fc.utobj().clickElement(driver, webFormObj.hostURL);
			}

			urlText = webFormObj.hostCodeBox.getText().trim();
			fc.utobj().clickElement(driver, webFormObj.okBtn);

			String LName = fc.utobj().generateTestData(dataSet.get("Lastname"));
			String FName = fc.utobj().generateTestData(dataSet.get("Firstname"));
			String parentWindow = driver.getWindowHandle();
			fc.utobj().sendKeys(driver, CrmWebFormPage.searchMyForm, formName);
			fc.utobj().clickElement(driver, CrmWebFormPage.searchMyFormBtn);
			fc.utobj().actionImgOption(driver, formName, "Launch & Test");

			fc.utobj().printTestStep("Launc And Test Form");
			Set<String> allWindows2 = driver.getWindowHandles();
			for (String currentWindow : allWindows2) {
				if (!currentWindow.equalsIgnoreCase(parentWindow)) {
					driver.switchTo().window(currentWindow);
					String titleTextCurrent = driver.getTitle();
					if (titleTextCurrent.equalsIgnoreCase(formTitle)) {
						// fill the form with data

						fc.utobj().isTextDisplayed(driver, header, "was not able to verify Header");
						fc.utobj().isTextDisplayed(driver, formTitle, "was not able to verify Form Title");
						fc.utobj().isTextDisplayed(driver, textLabel, "was not able to verify Text Label");
						fc.utobj().isTextDisplayed(driver, footer, "was not able to verify Footer");

						fc.utobj().sendKeys(driver, CrmWebFormPage.leadLastName, LName);
						fc.utobj().sendKeys(driver, CrmWebFormPage.leadFirstName, FName);
						fc.utobj().selectDropDownByPartialText(driver, CrmWebFormPage.title, "Dr.");
						fc.utobj().sendKeys(driver, CrmWebFormPage.companyName, "companyName");
						fc.utobj().sendKeys(driver, CrmWebFormPage.address, "address");
						fc.utobj().sendKeys(driver, CrmWebFormPage.city, "city");
						fc.utobj().selectDropDownByPartialText(driver, CrmWebFormPage.country, "USA");
						// fc.utobj().sendKeys(driver, CrmWebFormPage.zipcode,
						// zipCode);
						fc.utobj().selectDropDownByPartialText(driver, CrmWebFormPage.state, "Alabama");
						fc.utobj().sendKeys(driver, CrmWebFormPage.phoneNumbers, "1236547896");
						fc.utobj().sendKeys(driver, CrmWebFormPage.extn, "12");
						fc.utobj().sendKeys(driver, CrmWebFormPage.faxNumbers, "1236577896");
						fc.utobj().sendKeys(driver, CrmWebFormPage.mobileNumbers, "9874563212");
						fc.utobj().sendKeys(driver, CrmWebFormPage.emailIds, "jhsdhfs@gmail.com");
						fc.utobj().sendKeys(driver, CrmWebFormPage.alternateEmail, "jhsdhdasdfs@gmail.com");

						fc.utobj().sendKeys(driver,
								fc.utobj().getElementByXpath(driver,
										".//td[contains(text () , '" + FieldName1 + "')]/following-sibling::td/input"),
								FieldName1);
						fc.utobj().sendKeys(driver, fc.utobj().getElementByXpath(driver,
								".//td[contains(text () , '" + FieldName2 + "')]/following-sibling::td/label/textarea"),
								FieldName2);
						fc.utobj()
								.sendKeys(driver,
										fc.utobj().getElementByXpath(driver,
												".//td[contains(text () , '" + FieldName3
														+ "')]/following-sibling::td/table/tbody/tr/td/input"),
										"02/02/2017");
						fc.utobj().clickElement(driver,
								fc.utobj().getElementByXpath(driver,
										".//td[contains(text () , '" + FieldName4
												+ "')]/following-sibling::td/select/option[contains(text(),'Opt"
												+ FieldName4 + "')]"));
						fc.utobj().clickElement(driver,
								fc.utobj().getElementByXpath(driver, ".//td[contains(text () , '" + FieldName5
										+ "')]/following-sibling::td/input[@type='radio' and @value='1']"));
						fc.utobj().sendKeys(driver,
								fc.utobj().getElementByXpath(driver,
										".//td[contains(text () , '" + FieldName7 + "')]/following-sibling::td/input"),
								"99");
						fc.utobj().clickElement(driver, driver.findElement(By
								.xpath(".//td[contains(text () , '" + FieldName6 + "')]/following-sibling::td/input")));
						try {
							fc.utobj().clickElement(driver,
									fc.utobj().getElementByXpath(driver,
											".//td[contains(text () , '" + FieldName8
													+ "')]/following-sibling::td/div/div/ul/li/label[contains(text(),'Mul"
													+ FieldName8 + "')]/input"));
						} catch (Exception e) {
							// TODO: handle exception
						}
						String fileName = fc.utobj().getFilePathFromTestData(dataSet.get("fileName"));
						fc.utobj()
								.sendKeys(driver,
										fc.utobj()
												.getElementByXpath(driver,
														".//td[contains(text () , '" + FieldName9
																+ "')]/following-sibling::td/input[@type='file']"),
										fileName);
						try {
							fc.utobj().selectDropDownByPartialText(driver, CrmWebFormPage.leadSourceLaunch, leadSource);

							fc.utobj().selectDropDownByPartialText(driver, CrmWebFormPage.leadSourceDetailsLaunch,
									sourceDetails);
							// fc.utobj().clickElement(driver,
							// CrmWebFormPage.nextBtn);
						} catch (Exception ew) {
						}
						fc.utobj().isTextDisplayed(driver, header, "was not able to verify Header");
						fc.utobj().isTextDisplayed(driver, formTitle, "was not able to verify Form Title");
						// fc.utobj().isTextDisplayed(driver, tabName, "was not
						// able to verify Tab Name");
						// fc.utobj().isTextDisplayed(driver, fieldLabel, "was
						// not able to verify Fiedl Label");
						fc.utobj().isTextDisplayed(driver, footer, "was not able to verify Footer");
						fc.utobj().clickElement(driver, CrmWebFormPage.submitBtn);
						// fc.utobj().isTextDisplayed(driver, "Thank you for
						// submitting your information, we will get back to you
						// shortly.", "was not able to verify confirmation
						// Msg");

						driver.close();
					} else {
						driver.close();
						driver.switchTo().window(parentWindow);
					}
					driver.switchTo().window(parentWindow);
				}
			}

			CRMLeadsPageTest leadPageTest = new CRMLeadsPageTest();
			CRMLeadsPage leadsPage = new CRMLeadsPage(driver);
			fc.crm().crm_common().CRMLeadsLnk(driver);
			// leadPageTest.searchLeadByOwner(driver, userNameF1+" "+userNameF1,
			// "Open Leads");
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//a[contains(text () , '" + FName + " " + LName + "')]"));

			boolean isVisibilityPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//*[contains(text () ,'" + FieldName1 + "')]");
			if (isVisibilityPresent == false) {
				fc.utobj().throwsException("was not able to " + FieldName1);
			}
			boolean isVisibilityPresent2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//*[contains(text () ,'" + FieldName2 + "')]");
			if (isVisibilityPresent2 == false) {
				fc.utobj().throwsException("was not able to " + FieldName2);
			}
			boolean isVisibilityPresent3 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//*[contains(text () ,'" + FieldName3 + "')]");
			if (isVisibilityPresent3 == false) {
				fc.utobj().throwsException("was not able to " + FieldName3);
			}
			try {
				boolean isVisibilityPresent4 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
						".//*[contains(text () ,'" + FieldName4 + "')]");
				if (isVisibilityPresent4 == false) {
					fc.utobj().throwsException("was not able to " + FieldName4);
				}
			} catch (Exception e) {
			}
			try {
				boolean isVisibilityPresent8 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
						".//*[contains(text () ,'" + FieldName8 + "')]");
				if (isVisibilityPresent8 == false) {
					fc.utobj().throwsException("was not able to " + FieldName4);
				}
			} catch (Exception e) {
			}
			boolean isVisibilityPresent5 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//*[contains(text () ,'" + FieldName7 + "')]");
			if (isVisibilityPresent5 == false) {
				fc.utobj().throwsException("was not able to " + FieldName6);
			}
			boolean isVisibilityPresent6 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//*[contains(text () ,'" + FieldName6 + "')]");
			if (isVisibilityPresent6 == false) {
				fc.utobj().throwsException("was not able to " + FieldName6);
			}

			fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	/* Anukaran Mishra */

	@Test(groups = { "crmmoduleyt6", "crm"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2017-06-23", testCaseDescription = "Product/Service and Category Modification and deactivation Check", testCaseId = "TC_331_Verify_Task_Creation_Lead")
	public void verifyTaskCreationLead() throws IOException, Exception, ParserConfigurationException, SAXException {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			CRMContactsPage pobj = new CRMContactsPage(driver);

			/*
			 * fc.utobj().printTestStep(testCaseId,
			 * "Navigate To Admin > CRM > Contact Type Configuration");
			 * fc.utobj().printTestStep(testCaseId, "Add Contact Type");
			 * AdminCRMContactTypeConfigurationPageTest contatcTypePage = new
			 * AdminCRMContactTypeConfigurationPageTest(); String contactType =
			 * fc.utobj().generateTestData(dataSet.get("contactType"));
			 * contatcTypePage.addContactType(driver, contactType, config);
			 */

			/*
			 * fc.utobj().printTestStep(testCaseId,
			 * "Naviagte To CRM > Contacts > Contacts Summary");
			 * fc.crm().crm_common().CRMContactsLnk( driver);
			 * fc.utobj().printTestStep(testCaseId, "Add Contacs");
			 * fc.utobj().clickElement(driver, pobj.addNew);
			 * fc.utobj().clickElement(driver, pobj.leadLnk);
			 */
			String emailId = "commonautomation@staffex.com";
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUser = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporateUser.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			AdminCRMConfigureEmailContentPageTest configureMail = new AdminCRMConfigureEmailContentPageTest();
			configureMail.configureAllEmailContentLead(driver);
			String firstName = fc.utobj().generateTestData(dataSet.get("firstName"));
			String lastName = fc.utobj().generateTestData(dataSet.get("lastName"));
			String primaryCMethod = dataSet.get("primaryContactMethod");
			String assignTo = "Corporate";
			String regionName = "";
			String franchiseId = "";
			String title = dataSet.get("title");
			String email = "automation@gmail.com";
			// String userName="FranConnect Administrator";
			String leadName = firstName + " " + lastName;
			String regionalUser = "";
			String city = "city";
			String comment = "comment";
			String company = "Company";
			String address = "address";
			String suffix = "";
			String jobTitle = "";
			String franchiseUser = "";
			addLead(driver, dataSet, firstName, lastName, company, assignTo, city, address, suffix, jobTitle, comment,
					corpUser.getuserFullName(), regionName, regionalUser, franchiseId, franchiseUser, config);

			fc.utobj().clickElement(driver, pobj.tasksLinks);
			// fc.utobj().clickElement(driver, pobj.showFilter);
			fc.utobj().clickElement(driver, pobj.taskLnk);

			fc.commonMethods().switch_cboxIframe_frameId(driver);
			// fc.utobj().clickElement(driver, pobj.contactTaskRadio);
			fc.utobj().sendKeys(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='cmLeadName']"), leadName);
			/*
			 * fc.utobj().clickElement(driver,
			 * fc.utobj().getElementByXpath(driver,
			 * ".//*[@id='leadSearch']/tbody/tr/td[2]/a/img")));
			 * fc.utobj().clickElement(driver,
			 * fc.utobj().getElementByXpath(driver,".//input[@name='checkb']")))
			 * ;
			 */
			fc.utobj().sleep(3000);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//*[@id='contentdivTopSearch']/table/tbody/tr[1]/td/a/span/u[1]"));
			// fc.utobj().clickElement(driver, pobj.createTaskBtn);
			// fc.utobj().clickElement(driver,
			// fc.utobj().getElementByXpath(driver,".//*[@id='contentdivTopSearch']/table/tbody/tr[1]/td/a/span/u")));
			// fc.commonMethods().switch_cboxIframe_frameId(driver);

			if (!fc.utobj().isSelected(driver,pobj.radioOwner)) {
				fc.utobj().clickElement(driver, pobj.radioOwner);
			}
			/*
			 * WebElement elementInput=pobj.otherUserTask.findElement(By.xpath(
			 * "./div/div/input")); fc.utobj().selectValFromMultiSelect(driver,
			 * pobj.otherUserTask, elementInput, userName);
			 */

			String statusTask = "Work In Progress";
			fc.utobj().selectDropDown(driver, pobj.statusTask, statusTask);
			String subject = fc.utobj().generateTestData(dataSet.get("subject"));
			fc.utobj().sendKeys(driver, pobj.subject, subject);
			/*
			 * if (!fc.utobj().getElement(driver,
			 * pobj.timeLessTask)) {
			 * fc.utobj().clickElement(driver, pobj.timeLessTask); }
			 */
			String priority = dataSet.get("priority");
			fc.utobj().selectDropDown(driver, pobj.priority, priority);
			// fc.utobj().sendKeys(driver, pobj.LogATaskStatusName, taskName);
			if (!fc.utobj().isSelected(driver,pobj.calendarTaskCheckBox)) {
				fc.utobj().clickElement(driver, pobj.calendarTaskCheckBox);
			}
			// fc.utobj().clickElement(driver, pobj.AddTaskBtn);

			fc.utobj().sendKeys(driver, pobj.comments, "Test Comment");
			fc.utobj().clickElement(driver, pobj.createBtn);
			fc.utobj().switchFrameToDefault(driver);

			String parentWindowsHandle = driver.getWindowHandle();
			Set<String> allWindows = driver.getWindowHandles();
			for (String currentWindow : allWindows) {

				if (!currentWindow.equalsIgnoreCase(parentWindowsHandle)) {

					driver.switchTo().window(currentWindow);
					try {

						if (fc.utobj().getElementByXpath(driver, ".//*[.='Alert Details']").isDisplayed()) {

							boolean isUserPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
									".//td[contains(text () ,'Assign To ')]/ancestor::tr/td[contains(text () , '"
											+ corpUser.getuserFullName() + "')]");
							if (isUserPresent == false) {
								fc.utobj().throwsException("was not able to verify Assign To");
							}

							boolean isSubjectPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
									".//td[contains(text () ,'Subject ')]/ancestor::tr/td[contains(text () , '"
											+ subject + "')]");
							if (isSubjectPresent == false) {
								fc.utobj().throwsException("was not able to verify Assign To");
							}
							fc.utobj().printTestStep("Reminder pop-up verified ");
						}
						fc.utobj().clickElement(driver, pobj.complete);
						driver.close();
					} catch (Exception e) {
						driver.close();
					}
				}
				driver.switchTo().window(parentWindowsHandle);
			}

			fc.utobj().printTestStep("Verify Send Email");
			String editorText = subject;
			// String emailId="crmautomation@staffex.com";
			// verify Email Sent to user
			String expectedSubject = "New Task Added";
			String expectedMessageBody = editorText;
			Map<String, String> mailData = fc.utobj().readMailBox(expectedSubject, expectedMessageBody, emailId,
					"sdg@1a@Hfs");

			if (mailData.size() == 0 || !mailData.get("mailBody").contains(editorText)) {
				fc.utobj().throwsException("was not able to verify mail text");
			}
			if (mailData.size() == 0 || !mailData.get("mailBody").contains(firstName)) {
				fc.utobj().throwsException("was not able to verify firstname mail");
			}
			if (mailData.size() == 0 || !mailData.get("mailBody").contains(lastName)) {
				fc.utobj().throwsException("was not able to verify lastname mail");
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}

	}

	public void searchLeadByOwner(WebDriver driver, String userName, String leadTypeView) throws Exception {

		CRMLeadsPage pobj = new CRMLeadsPage(driver);
		fc.utobj().clickElement(driver, pobj.showFilter);
		
		/*fc.utobj().setToDefault(driver, pobj.leadOwnerSelect);
		fc.utobj()
				.selectValFromMultiSelect(driver, pobj.leadOwnerSelect, fc.utobj()
						.getElement(driver, pobj.leadOwnerSelect).findElement(By.xpath("./div/div/input")),
						userName);*/
		fc.utobj().selectValFromMultiSelect(driver, pobj.selectleadOwner, userName);
		
		fc.utobj().setToDefault(driver, pobj.franchiseSelect);
		fc.utobj().setToDefault(driver, pobj.statusSelect);
		fc.utobj().selectDropDown(driver, pobj.addDateSelect, "All");
		fc.utobj().setToDefault(driver, pobj.emailStatusSelect);
		fc.utobj().selectDropDown(driver, pobj.viewLeadsSelect, leadTypeView);
		fc.utobj().clickElement(driver, pobj.searchBtn);

		fc.utobj().clickElement(driver, pobj.hideFilter);
	}

	// system search
	public void systemExactSearch(WebDriver driver, String leadName) throws Exception {
		// search By System Search Button
		CRMLeadsPage pobj = new CRMLeadsPage(driver);
		fc.utobj().sendKeys(driver, pobj.searchLeads, leadName);
		fc.utobj().clickElement(driver, pobj.exactSearch);
		fc.utobj().clickElement(driver, pobj.systemSearchButton);
	}

	public void leadSearchFromSearchpage(WebDriver driver, String leadFirstname, String leadLastName) throws Exception {
		// search By System Search Button
		CRMLeadsPage pobj = new CRMLeadsPage(driver);
		fc.utobj().clickElement(driver, pobj.searchLink);
		fc.utobj().clickElement(driver, pobj.leadsTab);
		fc.utobj().sendKeys(driver, pobj.leadSearchFirstName, leadFirstname);
		fc.utobj().sendKeys(driver, pobj.leadSearchLastName, leadLastName);
		fc.utobj().clickElement(driver, pobj.searchBtnAtSearch);
	}

	public void contactSearchFromSearchpage(WebDriver driver, String Firstname, String LastName) throws Exception {
		// search By System Search Button
		CRMLeadsPage pobj = new CRMLeadsPage(driver);
		fc.utobj().clickElement(driver, pobj.searchLink);
		fc.utobj().sendKeys(driver, pobj.contactSearchFirstName, Firstname);
		fc.utobj().sendKeys(driver, pobj.contactSearchLastName, LastName);
		fc.utobj().clickElement(driver, pobj.searchBtnAtSearch);
	}

	@Test(groups = { "ZcSanity", "ZcSanity36" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Add Lead At CRM > Lead > Lead Summary", testCaseId = "TC_Add_CRM_Lead")
	public void addLeadTest() throws IOException, Exception, ParserConfigurationException, SAXException {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		// Map<String,String> dataSet = fc.utobj().readTestData("crm",
		// testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			Map<String, String> data2 = new HashMap<String, String>();

			data2.put("userName", "Test58CorUser");
			data2.put("source", "Test58Source");
			data2.put("description", "Test58Description");
			data2.put("text", "Test58Display");
			data2.put("sourceDetails", "Test58SourceDetails");
			data2.put("medium", "Test58Medium");
			data2.put("leadType", "Test58LeadType");
			data2.put("title", "Mr.");
			data2.put("firstName", "Test58fname");
			data2.put("lastName", "Test58lname");
			data2.put("company", "Test58CompanyName");
			data2.put("primaryContactMethod", "Email");
			data2.put("address", "Test58Address");
			data2.put("city", "Test58City");
			data2.put("emailId", "testautomation@franqa.net");
			data2.put("suffix", "Test58Suffix");
			data2.put("jobTitle", "Test58JobTitle");
			data2.put("rating", "Hot");
			data2.put("comment", "Test58Comment");

			CRMLeadsPage pobj = new CRMLeadsPage(driver);
			SearchUI search_page = new SearchUI(driver);
			fc.utobj().printTestStep("Navigate To Admin Users Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			String emailId = "crmautomation@staffex.com";
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUser = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporateUser.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			fc.utobj().printTestStep("Navigate To CRM > Leads > Leads Summary");
			fc.utobj().printTestStep("Add Lead");
			fc.crm().crm_common().CRMLeadsLnk(driver);

			fc.utobj().clickElement(driver, pobj.addNew);
			fc.utobj().clickElement(driver, pobj.leadLnk);
			fc.utobj().selectDropDown(driver, pobj.title, data2.get("title"));
			String firstName = fc.utobj().generateTestData(data2.get("firstName"));
			fc.utobj().sendKeys(driver, pobj.leadFirstName, firstName);
			String lastName = fc.utobj().generateTestData(data2.get("lastName"));
			fc.utobj().sendKeys(driver, pobj.leadLastName, lastName);
			String company = fc.utobj().generateTestData(data2.get("company"));
			fc.utobj().sendKeys(driver, pobj.companyName, company);
			fc.utobj().selectDropDown(driver, pobj.primaryContactMethodSelect, data2.get("primaryContactMethod"));
			if (!fc.utobj().isSelected(driver, pobj.assignToCorporate)) {
				fc.utobj().clickElement(driver, pobj.assignToCorporate);
			}
			fc.utobj().selectDropDown(driver, pobj.selectCorporateUser, corpUser.getuserFullName());
			String address = fc.utobj().generateTestData(data2.get("address"));
			fc.utobj().sendKeys(driver, pobj.address, address);
			String city = fc.utobj().generateTestData(data2.get("city"));
			fc.utobj().sendKeys(driver, pobj.city, city);
			fc.utobj().selectDropDown(driver, pobj.country, "USA");
			fc.utobj().selectDropDown(driver, pobj.state, "Alabama");
			fc.utobj().sendKeys(driver, pobj.zipcode, "12345");
			fc.utobj().sendKeys(driver, pobj.phoneNumbers, "1236547896");
			fc.utobj().sendKeys(driver, pobj.extn, "12");
			fc.utobj().sendKeys(driver, pobj.faxNumbers, "1263985487");
			fc.utobj().sendKeys(driver, pobj.mobileNumbers, "8896724441");
			String email = data2.get("emailId");
			fc.utobj().sendKeys(driver, pobj.emailIds, email);
			fc.utobj().sendKeys(driver, pobj.alternateEmail, "test@gmail.com");
			String suffix = fc.utobj().generateTestData(data2.get("suffix"));
			fc.utobj().sendKeys(driver, pobj.suffix, suffix);
			String jobTitle = fc.utobj().generateTestData(data2.get("jobTitle"));
			fc.utobj().sendKeys(driver, pobj.position, jobTitle);
			fc.utobj().sendKeys(driver, pobj.birthdate, "11/24/1980");
			fc.utobj().sendKeys(driver, pobj.anniversarydate, "11/24/1999");
			fc.utobj().selectDropDown(driver, pobj.rating, data2.get("rating"));
			fc.utobj().sendKeys(driver, pobj.bestTimeToContact, "12:00 PM EST");
			String comment = fc.utobj().generateTestData(data2.get("comment"));
			fc.utobj().sendKeys(driver, pobj.comments, comment);
			fc.utobj().clickElement(driver, pobj.saveBtn);

			fc.utobj().printTestStep("Verify The Added Lead By Top Search");
			String leadName = firstName + " " + lastName;

			if (!config.get("moduleList").equalsIgnoreCase("ZcSanity36")) {
				boolean isSearchTrue = false;
				for (int i = 0; i < 3; i++) {
					if (isSearchTrue == false) {
						fc.utobj().sendKeys(driver, search_page.topSearchField, leadName);
						Thread.sleep(5000);
						if (i == 2) {
							Thread.sleep(2000);
						}
						fc.utobj().clickEnterOnElement(driver, search_page.topSearchField);

						try {
							isSearchTrue = fc.utobj().isElementPresent(
									driver.findElements(By.xpath(".//custom[contains(text () , '" + leadName + "')]")));
						} catch (Exception e) {
							Reporter.log(e.getMessage());
							System.out.println(e.getMessage());
						}
					}
				}

				if (isSearchTrue == false) {
					fc.utobj().throwsException("Not able to search Lead By Top Search");
				}

				fc.utobj().refresh(driver);
			}

			// verify at Home Page
			fc.utobj().printTestStep("Verify Lead At Lead Home Page");
			fc.utobj().clickElement(driver, pobj.leadsLink);
			searchLeadByOwner(driver, corpUser.getuserFullName(), "Open Leads");

			boolean verifyName = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[.='" + firstName + " " + lastName + "']");
			if (verifyName == false) {
				fc.utobj().throwsException("was not able to verify Name");
			}
			boolean ownerOfLead = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[.='" + firstName + " " + lastName
					+ "']/ancestor::tr/td[contains(text () , '" + corpUser.getuserFullName() + "')]");
			if (ownerOfLead == false) {
				fc.utobj().throwsException("was not able to verify owner of the lead");
			}
			/*
			 * boolean statusOfLead = fc.utobj().verifyCase(driver,".//a[.='" +
			 * firstName + " " + lastName +
			 * "']/ancestor::tr/td[contains(text () , 'New')]"); if
			 * (statusOfLead == false) { fc.utobj().throwsException(
			 * "was not able to verify staus of the lead"); }
			 */
			boolean emailOfLead = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[.='" + firstName + " " + lastName
					+ "']/ancestor::tr/td/a[contains(text () , '" + email + "')]");
			if (emailOfLead == false) {
				fc.utobj().throwsException("was not able to verify email of the lead");
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}
}
