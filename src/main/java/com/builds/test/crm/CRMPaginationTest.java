package com.builds.test.crm;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.xml.parsers.ParserConfigurationException;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;
import org.xml.sax.SAXException;

import com.builds.uimaps.common.FCHomePage;
import com.builds.uimaps.infomgr.InfoMgrPagination;
import com.builds.utilities.FranconnectUtil;
import com.builds.utilities.TestCase;

public class CRMPaginationTest {

	FranconnectUtil fc = new FranconnectUtil();

	// Lead Summary
	@Test(groups = { "crmpaging", "crm" })

	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "TC_CRM_Paging_Report_Filter_leadSummary", testCaseDescription = "This test case will verify Paging all the summary of CRM Module ", reference = {
			"" })
	public void VerifySalesSummaryPaging_leadSummary() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		List<String> listItems = new ArrayList<String>();
		FCHomePage pobj = new FCHomePage(driver);
		try {
			driver = fc.loginpage().login(driver);
			List<WebElement> elementofSummaryPage = null;

			fc.utobj().printTestStep("verify Paging for Lead Summary ");
			showPagingCRMModule(config, driver, "LeadSummary", 20);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception ex) {
			fc.utobj().quitBrowserOnCatch(driver, ex, testCaseId);
		}
	}

	// AccountsSummary
	@Test(groups = { "crmpaging" })

	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "TC_CRM_Paging_Report_Filter_AccountsSummary", testCaseDescription = "This test case will verify Paging all the summary of CRM Module ", reference = {
			"" })
	public void VerifySalesSummaryPaging_AccountsSummary() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		List<String> listItems = new ArrayList<String>();
		FCHomePage pobj = new FCHomePage(driver);
		try {
			driver = fc.loginpage().login(driver);
			List<WebElement> elementofSummaryPage = null;

			fc.utobj().printTestStep("verify Paging for Accounts Summary ");
			showPagingCRMModule(config, driver, "AccountsSummary", 20);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception ex) {
			fc.utobj().quitBrowserOnCatch(driver, ex, testCaseId);
		}
	}

	// ContactSummary
	@Test(groups = { "crmpaging", "crm" })

	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "TC_CRM_Paging_Report_Filter_ContactSummary", testCaseDescription = "This test case will verify ContactSummary Paging  ", reference = {
			"" })
	public void VerifySalesSummaryPaging_ContactSummary() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		List<String> listItems = new ArrayList<String>();
		FCHomePage pobj = new FCHomePage(driver);
		try {
			driver = fc.loginpage().login(driver);
			List<WebElement> elementofSummaryPage = null;

			fc.utobj().printTestStep("verify Paging for ContactSummary ");
			showPagingCRMModule(config, driver, "ContactSummary", 20);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception ex) {
			fc.utobj().quitBrowserOnCatch(driver, ex, testCaseId);
		}
	}

	// OpportunitySummary
	@Test(groups = { "crmpaging" })

	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "TC_CRM_Paging_Report_Filter_OpportunitySummary", testCaseDescription = "This test case will verify Opportunity Summary Paging  ", reference = {
			"" })
	public void VerifySalesSummaryPaging_OpportunitySummary() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		List<String> listItems = new ArrayList<String>();
		FCHomePage pobj = new FCHomePage(driver);
		try {
			driver = fc.loginpage().login(driver);
			List<WebElement> elementofSummaryPage = null;

			fc.utobj().printTestStep("verify Paging for  Opportunity Summary ");
			showPagingCRMModule(config, driver, "OpportunitySummary", 20);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception ex) {
			fc.utobj().quitBrowserOnCatch(driver, ex, testCaseId);
		}
	}

	// Groups
	@Test(groups = { "crmpaging" })

	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "TC_CRM_Paging_Report_Filter_Groups", testCaseDescription = "This test case will verify Groups Paging  ", reference = {
			"" })
	public void VerifySalesSummaryPaging_Groups() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		List<String> listItems = new ArrayList<String>();
		FCHomePage pobj = new FCHomePage(driver);
		try {
			driver = fc.loginpage().login(driver);
			List<WebElement> elementofSummaryPage = null;

			fc.utobj().printTestStep("verify Paging for Groups Summary ");
			showPagingCRMModule(config, driver, "Groups", 20);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception ex) {
			fc.utobj().quitBrowserOnCatch(driver, ex, testCaseId);
		}
	}

	// Templates
	@Test(groups = { "crmpaging" })

	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "TC_CRM_Paging_Report_Filter_Templates", testCaseDescription = "This test case will verify Templates Paging  ", reference = {
			"" })
	public void VerifySalesSummaryPaging_Templates() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		List<String> listItems = new ArrayList<String>();
		FCHomePage pobj = new FCHomePage(driver);
		try {
			driver = fc.loginpage().login(driver);
			List<WebElement> elementofSummaryPage = null;

			fc.utobj().printTestStep("verify Paging for Templates Summary ");
			showPagingCRMModule(config, driver, "Templates", 20);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception ex) {
			fc.utobj().quitBrowserOnCatch(driver, ex, testCaseId);
		}
	}

	// Campaigns
	@Test(groups = { "crm", "TC_CRM_Paging_Report_Filter_Templates" })

	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "TC_CRM_Paging_Report_Filter_Templates", testCaseDescription = "This test case will verify Campaigns Paging  ", reference = {
			"" })
	public void VerifySalesSummaryPaging_Campaigns() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		List<String> listItems = new ArrayList<String>();
		FCHomePage pobj = new FCHomePage(driver);
		try {
			driver = fc.loginpage().login(driver);
			List<WebElement> elementofSummaryPage = null;

			fc.utobj().printTestStep("verify Paging for Campaigns Summary ");
			showPagingCRMModule(config, driver, "Campaigns", 10);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception ex) {
			fc.utobj().quitBrowserOnCatch(driver, ex, testCaseId);
		}
	}

	// Workflows
	@Test(groups = { "crmpaging" })

	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "TC_CRM_Paging_Report_Filter_Workflows", testCaseDescription = "This test case will verify Workflows Paging  ", reference = {
			"" })
	public void VerifySalesSummaryPaging_Workflows() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		List<String> listItems = new ArrayList<String>();
		FCHomePage pobj = new FCHomePage(driver);
		try {
			driver = fc.loginpage().login(driver);
			List<WebElement> elementofSummaryPage = null;

			fc.utobj().printTestStep("verify Paging for Workflows Summary ");
			showPagingCRMModule(config, driver, "Workflows", 20);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception ex) {
			fc.utobj().quitBrowserOnCatch(driver, ex, testCaseId);
		}
	}

	// Tasks
	@Test(groups = { "crmpaging" })

	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "TC_CRM_Paging_Report_Filter_Tasks", testCaseDescription = "This test case will verify Tasks Paging  ", reference = {
			"" })
	public void VerifySalesSummaryPaging_Tasks() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		List<String> listItems = new ArrayList<String>();
		FCHomePage pobj = new FCHomePage(driver);
		try {
			driver = fc.loginpage().login(driver);
			List<WebElement> elementofSummaryPage = null;

			fc.utobj().printTestStep("verify Paging for Task Summary ");
			showPagingCRMModule(config, driver, "Tasks", 20);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception ex) {
			fc.utobj().quitBrowserOnCatch(driver, ex, testCaseId);
		}
	}

	@Test(groups = { "crmpaging" })

	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "TC_CRM_Paging_Report_Filter_MailMerge", testCaseDescription = "This test case will verify MailMerge Paging ", reference = {
			"" })
	public void VerifySalesSummaryPaging_MailMerge() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		List<String> listItems = new ArrayList<String>();
		FCHomePage pobj = new FCHomePage(driver);
		try {
			driver = fc.loginpage().login(driver);
			List<WebElement> elementofSummaryPage = null;

			// New UI Paging

			fc.utobj().printTestStep("verify Paging for Mail Merge ");
			showPagingCRMModule(config, driver, "MailMerge", 20);

			// fc.utobj().printTestStep(testCaseId, "verify Paging
			// for Brokers Summary ");
			// showPagingCRMModule(config,driver,"BrokersSummary",20);
			// fc.utobj().printTestStep(testCaseId, "verify Paging
			// for Sites ");
			// showPagingCRMModule(config,driver,"Sites",20);
			// fc.utobj().printTestStep(testCaseId, "verify Paging
			// for FDD ");
			// showPagingCRMModule(config,driver,"FDD",20);

			// End Lead Summary paging

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	public void showPagingCRMModule(Map<String, String> config, WebDriver driver, String subModuleName, int viewPerPage)
			throws ParserConfigurationException, SAXException, IOException, InterruptedException, Exception

	{
		String testCaseId = "TC_CRM_Paging_Report_Filter";
		InfoMgrPagination InfoMgrPagination = new InfoMgrPagination(driver);

		if (subModuleName == null)
			subModuleName = "LeadSummary";
		List<WebElement> records = null;
		CRM_Common fsmod = new CRM_Common();

		fc.utobj().printTestStep("Search " + subModuleName + " and click");

		if (subModuleName != null && "LeadSummary".equals(subModuleName)) {
			fsmod.CRMLeadsLnk(driver);

			try {
				records = driver.findElements(By.xpath(".//*[@id='siteMainTable']/tbody/tr/td/div"));
				// Thread.sleep(1000);
			} catch (Exception e) {
				e.printStackTrace();
			}
		} else if (subModuleName != null && "AccountsSummary".equals(subModuleName)) {
			fsmod.CRMHomeLnk(driver);
			fsmod.CRMAccountsLnk(driver);
			try {
				records = driver.findElements(By.xpath(".//*[@id='siteMainTable']/tbody/tr/td/form[2]"));
				// Thread.sleep(1000);
			} catch (Exception e) {
				e.printStackTrace();
			}
		} else if (subModuleName != null && "ContactSummary".equals(subModuleName)) {
			fsmod.CRMHomeLnk(driver);
			fsmod.CRMContactsLnk(driver);
			try {
				records = driver.findElements(By.xpath(".//*[@id='siteMainTable']/tbody/tr/td/form[8]"));
				// Thread.sleep(1000);
			} catch (Exception e) {
				e.printStackTrace();
			}
		} else if (subModuleName != null && "OpportunitySummary".equals(subModuleName)) {
			fsmod.CRMHomeLnk(driver);
			fsmod.CRMOpportunitiesLnk(driver);
			try {
				records = driver.findElements(By.xpath(".//*[@id='mainTable']/tbody/tr[2]/td[2]/table/tbody/tr[8]"));
				// Thread.sleep(1000);
			} catch (Exception e) {
				e.printStackTrace();
			}
		} else if (subModuleName != null && "Tasks".equals(subModuleName)) {
			fsmod.CRMHomeLnk(driver);
			fsmod.CRMLeadsLnk(driver);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//a[@original-title='View and manage tasks associated with leads, contacts and opportunities']"));

			try {
				records = driver.findElements(
						By.xpath(".//*[@id='siteMainTable']/tbody/tr/td/table/tbody/tr[6]/td/table/tbody/tr[1]/td"));
				// Thread.sleep(1000);
			} catch (Exception e) {
				e.printStackTrace();
			}
		} else if (subModuleName != null && "BrokersSummary".equals(subModuleName)) {
			fsmod.CRMHomeLnk(driver);
			fsmod.CRMLeadsLnk(driver);
			try {
				records = driver.findElements(
						By.xpath(".//*[@id='siteMainTable']/tbody/tr/td/form[11]/table/tbody/tr[3]/td/table/tbody"));
				// Thread.sleep(1000);
			} catch (Exception e) {
				e.printStackTrace();
			}
		} else if (subModuleName != null && "Sites".equals(subModuleName)) {

			try {
				fsmod.CRMHomeLnk(driver);
				fsmod.CRMLeadsLnk(driver);
				records = driver.findElements(By.xpath(".//*[@id='siteMainTable']/tbody/tr/td/table[3]"));
				// Thread.sleep(1000);
			} catch (Exception e) {
				// e.printStackTrace();
				fc.utobj().printTestStep(subModuleName + " Not clickable or opreational.");
			}
		} else if (subModuleName != null && "FDD".equals(subModuleName)) {
			fsmod.CRMHomeLnk(driver);
			fsmod.CRMLeadsLnk(driver);
			try {
				records = driver.findElements(
						By.xpath(".//*[@id='siteMainTable']/tbody/tr/td/table/tbody/tr[5]/td[2]/table/tbody"));
				// Thread.sleep(1000);
			} catch (Exception e) {
				e.printStackTrace();
			}
		} else if (subModuleName != null && "MailMerge".equals(subModuleName)) {
			fsmod.CRMHomeLnk(driver);
			fsmod.CRMLeadsLnk(driver);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,(".//a[@original-title='Send personalized document to multiple recipients']")));

			try {
				records = driver.findElements(By.xpath(
						".//*[@id='siteMainTable']/tbody/tr/td/table[2]/tbody/tr[1]/td[2]/div/table/tbody/tr/td/table/tbody"));
				// Thread.sleep(1000);
			} catch (Exception e) {
				e.printStackTrace();
			}
		} else if (subModuleName != null && ("Groups".equals(subModuleName) || "Templates".equals(subModuleName)
				|| "Workflows".equals(subModuleName) || "Campaigns".equals(subModuleName))) {

			if ("Groups".equals(subModuleName)) {
				fsmod.CRMHomeLnk(driver);
				fsmod.CRMGroupsLnk(driver);

			} else if ("Templates".equals(subModuleName)) {
				fsmod.CRMHomeLnk(driver);
				fsmod.CRMCampaignCenterLnk(driver);
				fc.utobj().printTestStep("Navigate to Manage Templates");
				fc.utobj().clickElement(driver, driver.findElement(
						By.xpath(".//*[@class='bottom-fixed-btn']//*[contains(text () ,'Manage Templates')]")));

			} else if ("Campaigns".equals(subModuleName)) {
				fsmod.CRMHomeLnk(driver);
				fsmod.CRMCampaignCenterLnk(driver);
				fc.utobj().printTestStep("Navigate to Manage Campaigns");
				fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
						".//*[@class='bottom-fixed-btn exbtn-style']//*[contains(text () ,'Manage Campaigns')]"));

			} else if ("Workflows".equals(subModuleName)) {
				fsmod.CRMHomeLnk(driver);
				fsmod.CRMCampaignCenterLnk(driver);
				fc.utobj().printTestStep("Navigate to Manage Workflows");
				try {
					fc.utobj().clickElement(driver, driver.findElement(
							By.xpath(".//*[@class='bottom-fixed-btn']//*[contains(text () ,'Manage Workflows')]")));
				} catch (Exception e) {
					fc.utobj().printTestStep("You don't have any active workflows");
				}

			}

			try {
				records = driver.findElements(By.xpath(".//*[@id='siteMainTable']/tbody/tr/td/div"));
				// Thread.sleep(1000);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		String header;
		int totalRecordCount = 0;
		int totalrecordPerPage = 0;
		try {
			header = fc.utobj().getElementByXpath(driver, ".//*[@id='pageid']").getText();
			if (subModuleName != null && ("Groups".equals(subModuleName) || "Templates".equals(subModuleName)
					|| "Workflows".equals(subModuleName) || "Campaigns".equals(subModuleName))) {
				header = fc.utobj().getElementByXpath(driver, ".//*[@class='caption white-space']").getText();
			}

			String options[] = header.split(" ");
			if (options != null) {
				totalRecordCount = Integer.valueOf(options[5]);
				totalrecordPerPage = Integer.valueOf(options[3]);
			}
			fc.utobj().printTestStep(subModuleName + " ToTal Record " + totalRecordCount);

		} catch (Exception E) {
			fc.utobj().printTestStep("Pagination not found in " + subModuleName);
			// System.out.println("Pagination Not Found");
			totalRecordCount = 0;
		}

		if (totalRecordCount > 0 && totalRecordCount > viewPerPage) {
			fc.utobj().printTestStep("Pagination Exists in " + subModuleName);

			int clickCount = totalRecordCount / viewPerPage;
			String optionsShoud[] = new String[clickCount + 1];
			;
			int inLastRerord = totalRecordCount - (viewPerPage * clickCount);
			for (int cnt = 0; cnt <= clickCount; cnt++) {

				optionsShoud[cnt] = viewPerPage + "";
				if (cnt == clickCount) {
					optionsShoud[cnt] = inLastRerord + "";
				}

			}

			int foundRecordCount = 0;
			int counter = 1;
			try {
				if (optionsShoud != null && optionsShoud.length > 0) {
					boolean breakLoop = false;
					// while (!breakLoop) {
					for (int i = 0; i < (optionsShoud.length); i++) {
						// System.out.println("===Val=="+lstOptions.get(i)
						// +"==New=="+lstOptions.get(i).getText()
						// +"===clickCount=="+clickCount+"optionsShoud=="+optionsShoud[i]);
						if (true) {
							// driver.manage().timeouts().implicitlyWait(1000,
							// TimeUnit.MILLISECONDS);
							if (i == 1 && i != 0) {

								if (subModuleName != null && ("Groups".equals(subModuleName)
										|| "Templates".equals(subModuleName) || "Campaigns".equals(subModuleName)
										|| "Workflows".equals(subModuleName))) {
									fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
											".//*[@class='count']/a[" + (i) + "]"));
									counter = i + 1;
								} else {
									fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
											".//*[@id='pageid']/a[" + (i) + "]/u"));
									counter = i + 1;
								}
							} else if (i > 1 && i != 0) {
								if (subModuleName != null && ("Groups".equals(subModuleName)
										|| "Templates".equals(subModuleName) || "Campaigns".equals(subModuleName)
										|| "Workflows".equals(subModuleName))) {
									fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
											".//*[@class='count']/a[" + (i + 1) + "]"));
									counter = i + 2;
								} else {
									fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
											".//*[@id='pageid']/a[" + (i) + "]/u"));
									counter = i + 1;
								}
							}
							foundRecordCount = pagingRecordsCRM(driver, records, subModuleName).size();
							if (foundRecordCount > 0) {

								if (foundRecordCount == Integer.parseInt(optionsShoud[i])) {
									// System.out.println("found record
									// "+foundRecordCount+"= Test Case Pass for
									// the page "+(i));
									fc.utobj().printTestStep("" + foundRecordCount + " records for page = " + counter);
								} else {

									if (foundRecordCount <= Integer.parseInt(optionsShoud[i])) {
										// System.out.println("found record
										// "+foundRecordCount+"= Test Case Pass
										// for the Last page "+(i));
										// fc.utobj().printTestStep(testCaseId,
										// "found record "+foundRecordCount+"=
										// Test Case Pass for the Last page
										// "+counter);
										fc.utobj().printTestStep(
												"" + foundRecordCount + " records for page = " + counter);
									} else {
										fc.utobj().throwsException("found record " + foundRecordCount
												+ "= Test Case Fail for the page Almost Last Page " + counter);
									}
								}
							} else {
								fc.utobj().throwsException(
										"" + foundRecordCount + " records for page " + counter + " Test case fails");
							}
							// Check for paging after sort function
							if (subModuleName != null && !"Groups".equals(subModuleName)
									&& !"Templates".equals(subModuleName) && !"Campaigns".equals(subModuleName)
									&& !"Workflows".equals(subModuleName)) {
								fc.utobj().clickElement(driver, driver
										.findElement(By.xpath(".//table[@class='summaryTblex']/tbody/tr[1]/td[2]/a")));
								foundRecordCount = pagingRecordsCRM(driver, records, subModuleName).size();
								if (foundRecordCount > 0) {

									if (foundRecordCount == Integer.parseInt(optionsShoud[i])) {
										// System.out.println("found record
										// "+foundRecordCount+"= Test Case Pass
										// for the page "+(i));
										fc.utobj().printTestStep(
												"After Sort record " + foundRecordCount + " for the page " + counter);
									} else {

										if (foundRecordCount <= Integer.parseInt(optionsShoud[i])) {
											// System.out.println("found record
											// "+foundRecordCount+"= Test Case
											// Pass for the Last page "+(i));
											// fc.utobj().printTestStep(testCaseId,
											// "After Sort record
											// "+foundRecordCount+"= Test Case
											// Pass for the Last page
											// "+counter);
											fc.utobj().printTestStep("After Sort record " + foundRecordCount
													+ " for the page " + counter);
										} else {
											fc.utobj().throwsException("After Sort record " + foundRecordCount
													+ "= Test Case Fail for the page Almost Last Page " + counter);
										}

									}
								} else {
									fc.utobj().throwsException("" + foundRecordCount + " records for page " + counter
											+ " Test case fails on click sort");
								}
								//
							}
						}
						if (totalRecordCount > 500) {
							if (i == 15) {
								// breakLoop = true;
							}
						}
					}
					// }
				}

			} catch (Exception e) {
				e.printStackTrace();
			}

			try {
				// fc.utobj().printTestStep(testCaseId, "Reset our page to the
				// page 1 to check the results Per Page verification.");

				if (subModuleName != null && ("Groups".equals(subModuleName) || "Templates".equals(subModuleName)
						|| "Campaigns".equals(subModuleName) || "Workflows".equals(subModuleName))) {
					fc.utobj().clickElement(driver,
							fc.utobj().getElementByXpath(driver, ".//*[@class='print-active-name']/a[1]"));
					fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
							".//*[@id='siteMainTable']//*[@class='full-width']/div/div[3]//*[@class='dropdown-list direction' or @class='dropdown-list direction open-menu']/li[3]/a[1]"));

				} else {
					fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='pageid']/a[1]/u"));
					// fc.utobj().selectDropDown(driver,
					// fc.utobj().getElementByID(driver,"resultsPerPage")),
					// "100");
					fc.utobj().selectDropDown(driver,
							fc.utobj().getElementByXpath(driver, ".//*[@id='resultsPerPage']"), "100");
				}

				foundRecordCount = pagingRecordsCRM(driver, records, subModuleName).size();
				if (foundRecordCount > 0) {

				}
				if (foundRecordCount <= 100) {
					// System.out.println("found record in Result Per page all
					// "+foundRecordCount+" test case passed at View Per Page");
					fc.utobj().printTestStep("View Per Page " + foundRecordCount + " records.");

				} else {
					if (foundRecordCount > 100) {
						fc.utobj().throwsException("View Per Page " + foundRecordCount + " records.");
					}
				}
				// Check for paging after click on Sort function

				if (subModuleName != null && ("Groups".equals(subModuleName) || "Templates".equals(subModuleName)
						|| "Campaigns".equals(subModuleName) || "Workflows".equals(subModuleName))) {
					fc.utobj().clickElement(driver, driver.findElement(
							By.xpath(".//table[@class='tbl-style tbl-hover tbl-sticky-header']/thead/tr/th[2]/a")));
				} else {
					fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
							".//table[@class='summaryTblex']/tbody/tr[1]/td[2]/a"));
				}
				foundRecordCount = pagingRecordsCRM(driver, records, subModuleName).size();
				if (foundRecordCount > 0) {

				}
				if (foundRecordCount <= 100) {
					// System.out.println("found record in Result Per page all
					// "+foundRecordCount+" test case passed at View Per Page");
					fc.utobj().printTestStep("After Sort found record in Result Per page  " + foundRecordCount
							+ " test case passed at View Per Page");

				} else {
					if (foundRecordCount > 100) {
						fc.utobj().throwsException("View Per Page " + foundRecordCount + " records.");
					}
				}

			} catch (Exception E) {
				// System.out.println("resultsPerPage Field Not Found!");
				fc.utobj().printTestStep("View Per Page  Field Not Found! in " + subModuleName);

				// totalRecordCount = 0;
			}

			// if(subModuleName!=null && "FDD".equals(subModuleName))
			// {
			try {
				fc.utobj().clickLink(driver, "Show All");
				// System.out.println("totalRecordCount===in show
				// all"+totalRecordCount);
				fc.utobj().printTestStep("Total in show all " + subModuleName + " = " + totalRecordCount);
				foundRecordCount = pagingRecordsCRM(driver, records, subModuleName).size();
				if (foundRecordCount > 0) {

				}
				if (totalRecordCount == foundRecordCount) {
					fc.utobj().printTestStep("found record in show all " + foundRecordCount + " test case passed");
				} else {
					if (foundRecordCount > 0) {
						if (totalRecordCount != foundRecordCount) {
							fc.utobj()
									.throwsException("Show All " + foundRecordCount + " records not working correct.");
						}
					}
				}
				// System.out.println("found record "+foundRecordCount+" in
				// "+lstOptions.get(i).getText()+" th Page");

			} catch (Exception E) {
				fc.utobj().printTestStep("Show All field not found!");
			}
			// }

		} else {
			// System.out.println("Pagination Not Exists");
			fc.utobj().printTestStep("Pagination not exists in " + subModuleName);
		}

		// sortedList
		try {
			boolean isSorttable = sortedList(driver, config, records, subModuleName);
			if (isSorttable == true) {
				fc.utobj().printTestStep("Sorting function working for  " + subModuleName);
			} else {
				fc.utobj().printTestStep("Sorting function not Working OR not applicable for  " + subModuleName + " ");
			}

		} catch (Exception e) {
			// e.printStackTrace();
			fc.utobj().printTestStep("Eception in Sorting function for " + subModuleName);
		}

		// End sortedList

		fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
	}

	public boolean sortedList(WebDriver driver, Map<String, String> config, List<WebElement> records,
			String subModuleName) {

		boolean isSorttable = false;
		try {
			records = pagingRecordsCRM(driver, records, subModuleName);
			ArrayList<String> obtainedList = new ArrayList<>();
			ArrayList<String> sortedList = new ArrayList<>();
			List<WebElement> elementList = records; // driver.findElements(By.xpath(YourLocator));
			for (WebElement we : elementList) {
				obtainedList.add(we.getText());
			}

			if (subModuleName != null && !"Groups".equals(subModuleName) && !"Templates".equals(subModuleName)
					&& !"Campaigns".equals(subModuleName) && !"Workflows".equals(subModuleName)) {
				if (subModuleName != null && "AccountsSummary".equals(subModuleName)) {
					fc.utobj().clickElement(driver, driver
							.findElement(By.xpath(".//table[@class='summaryTblex']/tbody/tr[1]/td[1]//a[not(*)]")));
				} else {
					fc.utobj().clickElement(driver, driver
							.findElement(By.xpath(".//table[@class='summaryTblex']/tbody/tr[1]/td[2]//a[not(*)]")));
				}

				records = pagingRecordsCRM(driver, records, subModuleName);
				for (WebElement we : records) {
					sortedList.add(we.getText());
				}
			}
			try {
				if (sortedList != null) {
					for (int i = 0; i < sortedList.size(); i++) {
						if (sortedList.get(0) != obtainedList.get(0)) {
							isSorttable = true;
							break;
						}
					}
				}

			} catch (Exception e) {
				isSorttable = false;

			}

		} catch (Exception e) {
			isSorttable = false;
			// e.printStackTrace();
		}
		return isSorttable;
	}

	public List<WebElement> pagingRecordsCRM(WebDriver driver, List<WebElement> records, String subModuleName) {

		if (subModuleName == null)
			subModuleName = "LeadSummary";
		List<WebElement> listofElements = null;
		int rc = 0;
		if (subModuleName != null && "LeadSummary".equals(subModuleName)) {
			listofElements = driver.findElements(
					By.xpath(".//table[@class='summaryTblex']/tbody/tr/td[@class='botBorder colPadding'][2]/a"));
		} else if (subModuleName != null && "AccountsSummary".equals(subModuleName)) {
			listofElements = driver.findElements(By.xpath(
					".//table[@class='summaryTblex']/tbody/tr[@class='bText12']/td[@class='botBorder colPadding'][1]/a[@class='bText12lnk']"));
		} else if (subModuleName != null && "ContactSummary".equals(subModuleName)) {
			listofElements = driver.findElements(
					By.xpath(".//table[@class='summaryTblex']/tbody/tr/td[@class='botBorder colPadding'][2]/a"));
		} else if (subModuleName != null && "OpportunitySummary".equals(subModuleName)) {
			listofElements = driver.findElements(
					By.xpath(".//table[@class='summaryTblex']/tbody/tr/td[@class='botBorder colPadding'][2]/a"));
		} else if (subModuleName != null && ("Groups".equals(subModuleName))) {
			listofElements = driver.findElements(By.xpath(".//div[@class='full-width']/div/table/tbody/tr/td[2]/a"));
		} else if (subModuleName != null && ("Templates".equals(subModuleName))) {
			listofElements = driver.findElements(
					By.xpath(".//div[@class='full-width']/div/table/tbody/tr/td[@class='ellipsis'][1]/a"));
		} else if (subModuleName != null && ("Campaigns".equals(subModuleName))) {
			listofElements = driver.findElements(By.xpath(".//div[@class='full-width']/div[1]/table/tbody/tr/td[2]/a"));
		} else if (subModuleName != null && ("Workflows".equals(subModuleName))) {
			listofElements = driver.findElements(
					By.xpath(".//div[@class='full-width']/div[2]/table/tbody/tr/td[@class='ellipsis'][1]/a"));
		} else if (subModuleName != null && "Tasks".equals(subModuleName)) {
			listofElements = driver.findElements(
					By.xpath(".//table[@class='summaryTblex']/tbody/tr/td[@class='botBorder colPadding'][2]/a"));
		} else if (subModuleName != null && "BrokersSummary".equals(subModuleName)) {
			listofElements = driver.findElements(
					By.xpath(".//table[@class='summaryTblex']/tbody/tr/td[@class='botBorder colPadding'][2]/a"));
		} else if (subModuleName != null && "Sites".equals(subModuleName)) {
			listofElements = driver.findElements(By.xpath(
					".//*[@id='siteMainTable']/tbody/tr/td/table[3]/tbody/tr[1]/td/form/table/tbody/tr[2]/td/table/tbody/tr/td[@class='botBorder colPadding']/a"));
		} else if (subModuleName != null && "FDD".equals(subModuleName)) {
			listofElements = driver.findElements(By.xpath(
					".//*[@id='siteMainTable']/tbody/tr/td/table/tbody/tr[5]/td[2]/table/tbody/tr/td/table[1]/tbody/tr/td[1]/a"));
		} else if (subModuleName != null && "MailMerge".equals(subModuleName)) {
			listofElements = driver.findElements(By.xpath(
					".//*[@id='printDiv']//table[@class='summaryTblex']/tbody/tr/td[@class='botBorder colPadding'][1]/a"));
		}

		rc = listofElements.size();
		return listofElements;

	}

}
