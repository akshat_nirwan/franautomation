package com.builds.test.crm;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.xml.parsers.ParserConfigurationException;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;
import org.xml.sax.SAXException;

import com.builds.test.admin.AdminUsersManageCorporateUsersAddCorporateUserPageTest;
import com.builds.test.common.CorporateUser;
import com.builds.uimaps.crm.CRMAccountsPage;
import com.builds.uimaps.crm.CRMTasksPage;
import com.builds.utilities.FranconnectUtil;
import com.builds.utilities.TestCase;

public class CRMTasksPageTest {
	FranconnectUtil fc = new FranconnectUtil();

	@Test(groups = { "crm", "crmemail","crmemail543"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Add Task For Lead At CRM > Tasks And Verify Task Creation Mail", testCaseId = "TC_279_Add_Task_Lead")
	public void addTaskLead() throws IOException, Exception, ParserConfigurationException, SAXException {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			CRMLeadsPageTest leadPage = new CRMLeadsPageTest();

			CRMTasksPage pobj = new CRMTasksPage(driver);

			fc.utobj().printTestStep("Navigate To Admin Users Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			String emailId = "crmautomation@staffex.com";
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUser = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporateUser.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			fc.utobj().printTestStep("Navigating to Admin > CRM > Configure Status");
			fc.utobj().printTestStep("Add Status Lead Type");

			AdminCRMConfigureStatusPageTest statusPage = new AdminCRMConfigureStatusPageTest();
			String statusName = fc.utobj().generateTestData(dataSet.get("statusName"));
			statusPage.addStatus(driver, statusName);

			fc.utobj().printTestStep("Navigate To CRM > Leads > Leads Summary");
			fc.utobj().printTestStep("Add Lead");

			String firstName = fc.utobj().generateTestData(dataSet.get("firstName"));
			String lastName = fc.utobj().generateTestData(dataSet.get("lastName"));
			String company = fc.utobj().generateTestData(dataSet.get("company"));
			String assignTo = "Corporate";
			String city = fc.utobj().generateTestData(dataSet.get("city"));
			String address = fc.utobj().generateTestData(dataSet.get("address"));
			String suffix = fc.utobj().generateTestData(dataSet.get("suffix"));
			String jobTitle = fc.utobj().generateTestData(dataSet.get("jobTitle"));
			String comment = fc.utobj().generateTestData(dataSet.get("comment"));
			String regionName = "";
			String regionalUser = "";
			String franchiseId = "";
			String franchiseUser = "";

			leadPage.addLead(driver, dataSet, firstName, lastName, company, assignTo, city, address, suffix, jobTitle,
					comment, corpUser.getuserFullName(), regionName, regionalUser, franchiseId, franchiseUser, config);

			pobj = new CRMTasksPage(driver);
			
			fc.utobj().printTestStep("Navigate To CRM > Tasks");
			fc.utobj().printTestStep("Add Task");
			fc.utobj().clickElement(driver, pobj.taskLinks);
			fc.utobj().clickElement(driver, pobj.addTaskLink);
			fc.commonMethods().switch_cboxIframe_frameId(driver);

			
			if (!fc.utobj().isSelected(driver, pobj.associatedWithLead)) {
				fc.utobj().clickElement(driver, pobj.associatedWithLead);
			}

			String leadName = firstName + " " + lastName;

			fc.utobj().sendKeys(driver, pobj.leadSearch, leadName);
			Thread.sleep(1000);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//span/u[.='" + leadName + "']"));

			if (!fc.utobj().isSelected(driver,pobj.radioOwner)) {
				fc.utobj().clickElement(driver, pobj.radioOwner);
			}
			String statusTask = dataSet.get("statusTask");
			fc.utobj().selectDropDown(driver, pobj.statusTask, statusTask);
			String subject = fc.utobj().generateTestData(dataSet.get("subject"));
			fc.utobj().sendKeys(driver, pobj.subject, subject);
			if (!fc.utobj().isSelected(driver, pobj.timeLessTask)) {
				fc.utobj().clickElement(driver, pobj.timeLessTask);
			}
			String priority = dataSet.get("priority");
			fc.utobj().selectDropDown(driver, pobj.priority, priority);
			fc.utobj().sendKeys(driver, pobj.comments, "Test Comment");
			fc.utobj().clickElement(driver, pobj.createBtn);
			fc.utobj().sleep(2000);
			fc.utobj().switchFrameToDefault(driver);
			Thread.sleep(5000);
			fc.utobj().printTestStep("Verify Task Detail");
			searchBySubjectFilter(driver, subject);

			boolean isSubjectPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[.='" + subject + "']");
			if (isSubjectPresent == false) {
				fc.utobj().throwsException("Was not able to verify Subject Of Task");
			}

			boolean isAssociatedWithPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[.='" + subject + "']/ancestor::tr/td/a[contains(text () ,'" + leadName + "')]");
			if (isAssociatedWithPresent == false) {
				fc.utobj().throwsException("was not able to verify Associated With");
			}

			boolean isTaskStatusPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[.='" + subject + "']/ancestor::tr/td[contains(text () ,'" + statusTask + "')]");
			if (isTaskStatusPresent == false) {
				fc.utobj().throwsException("was not able to verify Status Of Task");
			}

			boolean isAssignedTo = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[.='" + subject
					+ "']/ancestor::tr/td[contains(text () ,'" + corpUser.getuserFullName() + "')]");
			if (isAssignedTo == false) {
				fc.utobj().throwsException("was not able to verify Assign To");
			}

			fc.utobj().printTestStep("Verify The Task Detail At Lead Info Page");
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,(".//a[.='" + subject + "']/ancestor::tr/td/a[contains(text () ,'" + leadName + "')]")));

			boolean isAssignedToAtLeadInfo = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[.='" + subject
					+ "']/ancestor::tr/td/div[contains(text () ,'" + corpUser.getuserFullName() + "')]");
			if (isAssignedToAtLeadInfo == false) {
				fc.utobj().throwsException("was not able to verify Assign To At Lead Info Page");
			}

			boolean isTaskStatusPresentAtLeadInfo = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[.='" + subject + "']/ancestor::tr/td[contains(text () ,'" + statusTask + "')]");
			if (isTaskStatusPresentAtLeadInfo == false) {
				fc.utobj().throwsException("was not able to verify Status Of Task At Lead Info Page");
			}

			fc.utobj().printTestStep("Verify that if any task is associated with Any Lead so entry will not be delete");

			fc.utobj().selectActionMenuItemsWithTagAMoreActions(driver, "Delete Lead");

			String alertText = fc.utobj().acceptAlertBox(driver);

			if (!alertText.equalsIgnoreCase(
					fc.utobj().translateString("Lead can not be deleted as a Task is associated with it."))) {
				fc.utobj().throwsException(
						"Not able to verify that if any task is associated with Any Lead so entry will not be delete");
			}

			fc.utobj().printTestStep("Verify Task Creation Mail");

			Map<String, String> mailData = new HashMap<String, String>();

			mailData = fc.utobj().readMailBox("New Task Added", subject, emailId, "sdg@1a@Hfs");

			if (mailData.size() == 0 || !mailData.get("mailBody").contains(firstName)) {

				fc.utobj().throwsException("was not able Verify Task Creation Mail");
			}

			if (mailData.size() == 0 || !mailData.get("mailBody").contains("Task Addition Notification")) {

				fc.utobj().throwsException("was not able Verify Task Creation Mail Info");
			}

			fc.utobj().printTestStep("Complete The Added Task");
			fc.crm().crm_common().CRMTasksLnk(driver);
			searchBySubjectFilter(driver, subject);

			fc.utobj().printTestStep("Process Task");
			fc.utobj().actionImgOption(driver, subject, "Process");

			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().selectDropDown(driver, pobj.statusTask, fc.utobj().translateString("Completed"));
			fc.utobj().clickElement(driver, pobj.processTask);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Verify that after completing task , lead will be deleted");

			fc.crm().crm_common().CRMLeadsLnk(driver);
			leadPage.searchLeadByOwner(driver, corpUser.getuserFullName(), "Open Leads");

			fc.utobj().printTestStep("Delete Lead");

			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,(".//a[contains(text () ,'" + firstName + " " + lastName + "')]/ancestor::tr/td/input")));
			fc.utobj().selectActionMenuItemsWithTagA(driver, "Delete");
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().clickElement(driver, ".//input[@value='Delete']");
			fc.utobj().sleep(1000);
			fc.utobj().clickElement(driver, ".//input[@value='Close']");
			fc.utobj().switchFrameToDefault(driver);
			fc.utobj().sleep(2000);
			//fc.utobj().acceptAlertBox(driver);

			fc.utobj().printTestStep("Verify Deleted Lead");

			leadPage.searchLeadByOwner(driver, corpUser.getuserFullName(), "All Leads");
			boolean isNoRecordPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//td[.='No records found.']");
			if (isNoRecordPresent == false) {
				fc.utobj().throwsException("was not able to Delete Lead");
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "crm", "crmTest22221"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Add Task For Contact At CRM > Tasks", testCaseId = "TC_280_Add_Task_Contact")
	public void addTaskContact() throws IOException, Exception, ParserConfigurationException, SAXException {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			CRMTasksPage pobj = new CRMTasksPage(driver);
			CRMContactsPageTest contactPage = new CRMContactsPageTest();

			fc.utobj().printTestStep("Navigate To Admin Users Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			String emailId = "crmautomation@staffex.com";
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUser = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporateUser.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			fc.utobj().printTestStep("Navigate To Admin > CRM > Contact Type Configuration");
			fc.utobj().printTestStep("Add Contact Type");
			AdminCRMContactTypeConfigurationPageTest contatcTypePage = new AdminCRMContactTypeConfigurationPageTest();
			String contactType = fc.utobj().generateTestData(dataSet.get("contactType"));
			contatcTypePage.addContactType(driver, contactType);

			fc.utobj().printTestStep("Naviagte To CRM > Contacts > Contacts Summary");
			fc.crm().crm_common().CRMContactsLnk(driver);
			fc.utobj().printTestStep("Add Contacs");
			fc.utobj().clickElement(driver, pobj.addNew);
			fc.utobj().clickElement(driver, pobj.contactLnk);

			String firstName = fc.utobj().generateTestData(dataSet.get("firstName"));
			String lastName = fc.utobj().generateTestData(dataSet.get("lastName"));
			String primaryCMethod = dataSet.get("primaryContactMethod");
			String assignTo = "Corporate";
			String regionName = "";
			String franchiseId = "";
			String title = dataSet.get("title");
			String email = "automation@gmail.com";

			contactPage.addContactGeneric(driver, contactType, firstName, lastName, primaryCMethod, assignTo,
					corpUser.getuserFullName(), regionName, franchiseId, title, email, config);

			fc.utobj().printTestStep("Navigate To CRM > Tasks");
			fc.utobj().printTestStep("Add Task");
			fc.utobj().clickElement(driver, pobj.taskLinks);
			fc.utobj().clickElement(driver, pobj.addTaskLink);
			fc.commonMethods().switch_cboxIframe_frameId(driver);

			if (!fc.utobj().isSelected(driver, pobj.associateWithContact)) {
				fc.utobj().clickElement(driver, pobj.associateWithContact);
			}

			String contactName = firstName +" "+ lastName;

			fc.utobj().sendKeys(driver, pobj.contactSearch, contactName);
			Thread.sleep(1000);
			fc.utobj().clickElement(driver, ".//td[@class='thead']//u[contains(text(),'"+contactName+"')]");
			//fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//td[@class='thead']//u[contains(text(),'"+contactName+"')]"));
			//fc.utobj().clickEnterOnElement(driver, pobj.contactSearch);

			if (!fc.utobj().isSelected(driver,pobj.radioOwner)) {
				fc.utobj().clickElement(driver, pobj.radioOwner);
			}
			String statusTask = dataSet.get("statusTask");
			fc.utobj().selectDropDown(driver, pobj.statusTask, statusTask);
			String subject = fc.utobj().generateTestData(dataSet.get("subject"));
			fc.utobj().sendKeys(driver, pobj.subject, subject);
			if (!fc.utobj().isSelected(driver, pobj.timeLessTask)) {
				fc.utobj().clickElement(driver, pobj.timeLessTask);
			}
			String priority = dataSet.get("priority");
			fc.utobj().selectDropDown(driver, pobj.priority, priority);
			fc.utobj().sendKeys(driver, pobj.comments, "Test Comment");
			fc.utobj().clickElement(driver, pobj.createBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Verify Task Detail");
			searchBySubjectFilter(driver, subject);

			boolean isSubjectPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[.='" + subject + "']");
			if (isSubjectPresent == false) {
				fc.utobj().throwsException("Was not able to verify Subject Of Task");
			}

			boolean isAssociatedWithPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[.='" + subject + "']/ancestor::tr/td/a[contains(text () ,'" + contactName + "')]");
			if (isAssociatedWithPresent == false) {
				fc.utobj().throwsException("was not able to verify Associated With");
			}

			boolean isTaskStatusPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[.='" + subject + "']/ancestor::tr/td[contains(text () ,'" + statusTask + "')]");
			if (isTaskStatusPresent == false) {
				fc.utobj().throwsException("was not able to verify Status Of Task");
			}

			boolean isAssignedTo = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[.='" + subject
					+ "']/ancestor::tr/td[contains(text () ,'" + corpUser.getuserFullName() + "')]");
			if (isAssignedTo == false) {
				fc.utobj().throwsException("was not able to verify Assign To");
			}

			fc.utobj().printTestStep("Verify The Task Detail At Contacts Info Page");

			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,(".//a[.='" + subject + "']/ancestor::tr/td/a[contains(text () ,'" + contactName + "')]")));

			boolean isAssignedToAtLeadInfo = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[.='" + subject
					+ "']/ancestor::tr/td/div[contains(text () ,'" + corpUser.getuserFullName() + "')]");
			if (isAssignedToAtLeadInfo == false) {
				fc.utobj().throwsException("was not able to verify Assign To At Contacts Info Page");
			}

			boolean isTaskStatusPresentAtLeadInfo = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[.='" + subject + "']/ancestor::tr/td[contains(text () ,'" + statusTask + "')]");
			if (isTaskStatusPresentAtLeadInfo == false) {
				fc.utobj().throwsException("was not able to verify Status Of Task At Contacts Info Page");
			}

			fc.utobj().printTestStep(
					"Verify that if any task is associated with Any Contact so entry will not be deleted");
			contactPage.selectActionMenuItemsWithTagAMoreActions(driver, "Delete Contact");

			
			String alertText = fc.utobj().acceptAlertBox(driver);

			if (!alertText.equalsIgnoreCase(fc.utobj()
					.translateString("Contact cannot be deleted as a Task / Opportunity is associated with it"))) {
				fc.utobj().throwsException(
						"Not able to verify that if any task is associated with Any Contact so entry will not be delete");
			}

			fc.utobj().printTestStep("Complete The Added Task");
			fc.crm().crm_common().CRMTasksLnk(driver);
			searchBySubjectFilter(driver, subject);

			fc.utobj().printTestStep("Process Task");
			fc.utobj().actionImgOption(driver, subject, "Process");

			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().selectDropDown(driver, pobj.statusTask, fc.utobj().translateString("Completed"));
			fc.utobj().clickElement(driver, pobj.processTask);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Verify that after completing task , Contact will be deleted");

			fc.crm().crm_common().CRMContactsLnk(driver);
			contactPage.contactsFilter(driver, contactType);
			fc.utobj().printTestStep("Delete Contact");
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//a[contains(text () ,'" + firstName
					+ " " + lastName + "')]/ancestor::tr/td/input[@name='checkb']"));
			fc.utobj().selectActionMenuItemsWithTagA(driver, "Delete");
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().clickElement(driver,".//input[@value='Delete']");
			fc.utobj().switchFrameToDefault(driver);
			//fc.utobj().clickElement(driver,".//input[@value='Close']");
			//fc.utobj().acceptAlertBox(driver);

			contactPage.contactsFilter(driver, contactType);
			fc.utobj().printTestStep("Verify Delete Contact");
			boolean isNoRecordPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//td[.='No records found.']");
			if (isNoRecordPresent == false) {
				fc.utobj().throwsException("was not able to Delete Lead");
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "crm"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Add Task For Opportunity At CRM > Tasks", testCaseId = "TC_281_Add_Task_Opportunity")
	public void addTaskOpportunity() throws IOException, Exception, ParserConfigurationException, SAXException {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			CRMTasksPage pobj = new CRMTasksPage(driver);

			CRMContactsPageTest contactsPage = new CRMContactsPageTest();
			CRMAccountsPageTest accountPage = new CRMAccountsPageTest();
			CRM_OpportunitiesPageTest opportunityPage = new CRM_OpportunitiesPageTest();

			String description = fc.utobj().generateTestData(dataSet.get("description"));

			fc.utobj().printTestStep("Navigate To Admin Users Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			String emailId = "crmautomation@staffex.com";
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUser = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporateUser.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			fc.utobj().printTestStep("Navigating to Admin > CRM > ContactType Configuration");
			fc.utobj().printTestStep("Add Contact Type");
			AdminCRMContactTypeConfigurationPageTest contactTypePage = new AdminCRMContactTypeConfigurationPageTest();
			String contactType = fc.utobj().generateTestData(dataSet.get("contactType"));
			contactTypePage.addContactType(driver, contactType);

			fc.utobj().printTestStep("Navigating to Admin > CRM >  Configure Opportunity Stages");
			fc.utobj().printTestStep("Add Stage");
			AdminCRMConfigureOpportunityStagesPageTest stagePage = new AdminCRMConfigureOpportunityStagesPageTest();
			String stage = fc.utobj().generateTestData(dataSet.get("stage"));
			stagePage.addStage(driver, stage);

			AdminCRMConfigureIndustryPageTest industryPage = new AdminCRMConfigureIndustryPageTest();
			String industry = fc.utobj().generateTestData(dataSet.get("industry"));
			description = fc.utobj().generateTestData(dataSet.get("description"));
			fc.utobj().printTestStep("Navigating to Admin > CRM >  Configure Industry Type");
			fc.utobj().printTestStep("Add Industry");
			industryPage.addIndustry(driver, industry, description);

			String accountName = fc.utobj().generateTestData(dataSet.get("accountName"));
			String visibility = dataSet.get("visibility");
			fc.utobj().printTestStep("Navigating to CRM >  Accounts > Accounts Summary");
			fc.utobj().printTestStep("Add Account");
			accountPage.addAcocunt(driver, industry, accountName, visibility);

			fc.utobj().printTestStep("Navigating to CRM >  Opportunities > Opportunities Summary");
			fc.utobj().printTestStep("Add Contact");

			fc.utobj().clickElement(driver, pobj.opportunitiesLinks);
			fc.utobj().clickElement(driver, pobj.addNew);
			fc.utobj().clickElement(driver, pobj.contactLnk);

			String cFname = fc.utobj().generateTestData(dataSet.get("cFname"));
			String cLname = fc.utobj().generateTestData(dataSet.get("cLname"));
			String primaryCMethod = dataSet.get("primaryCMethod");
			String assignTo = "Corporate";
			String regionName = "";
			String franchiseId = "";
			String title = dataSet.get("title");
			//String email = "crmautomation@staffex.com";
			String email = fc.utobj().generateTestData("crmautomation")+"@staffex.com";
			contactsPage.addContactGenericWithAccount(driver, contactType, cFname, cLname, primaryCMethod, assignTo,
					corpUser.getuserFullName(), regionName, franchiseId, title, email, config, accountName);

			fc.utobj().printTestStep("Navigating to CRM >  Opportunities > Opportunities Summary");
			fc.utobj().printTestStep("Add Opportunity");

			fc.utobj().clickElement(driver, pobj.opportunitiesLinks);
			fc.utobj().clickElement(driver, pobj.addNew);
			fc.utobj().clickElement(driver, pobj.OpportunityLnk);

			String opportunityName = fc.utobj().generateTestData(dataSet.get("opportunityName"));
			String contactName = cFname + " " + cLname;
			String salesAmount = dataSet.get("salesAmount");
			opportunityPage.addOpportunityGenric(driver, opportunityName, accountName, contactName,
					corpUser.getuserFullName(), stage, salesAmount, config);

			pobj = new CRMTasksPage(driver);
			
			fc.utobj().printTestStep("Navigate To CRM > Tasks");
			fc.utobj().printTestStep("Add Task");
			fc.utobj().clickElement(driver, pobj.taskLinks);
			fc.utobj().clickElement(driver, pobj.addTaskLink);
			fc.utobj().sleep(2000);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().sleep(2000);
			if (!fc.utobj().isSelected(driver, pobj.associatedWithOpportunity)) {
				fc.utobj().clickElement(driver, pobj.associatedWithOpportunity);
			}

			fc.utobj().sendKeys(driver, pobj.opportunitySearch, opportunityName);
			fc.utobj().sleep(3000);
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//span/u[.='" + opportunityName + "']"));

			if (!fc.utobj().isSelected(driver,pobj.radioOwner)) {
				fc.utobj().clickElement(driver, pobj.radioOwner);
			}
			String statusTask = dataSet.get("statusTask");
			fc.utobj().selectDropDown(driver, pobj.statusTask, statusTask);
			String subject = fc.utobj().generateTestData(dataSet.get("subject"));
			fc.utobj().sendKeys(driver, pobj.subject, subject);
			if (!fc.utobj().isSelected(driver, pobj.timeLessTask)) {
				fc.utobj().clickElement(driver, pobj.timeLessTask);
			}
			String priority = dataSet.get("priority");
			fc.utobj().selectDropDown(driver, pobj.priority, priority);
			fc.utobj().sendKeys(driver, pobj.comments, "Test Comment");
			fc.utobj().clickElement(driver, pobj.createBtn);
			fc.utobj().sleep(2000);
			fc.utobj().switchFrameToDefault(driver);
			fc.utobj().sleep(2000);
			fc.utobj().printTestStep("Verify Task Detail");
			searchBySubjectFilter(driver, subject);
			fc.utobj().sleep(2000);
			boolean isSubjectPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[.='" + subject + "']");
			if (isSubjectPresent == false) {
				fc.utobj().throwsException("Was not able to verify Subject Of Task");
			}

			boolean isAssociatedWithPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[.='" + subject + "']/ancestor::tr/td/a[contains(text () ,'" + opportunityName + "')]");
			if (isAssociatedWithPresent == false) {
				fc.utobj().throwsException("was not able to verify Associated With");
			}

			boolean isTaskStatusPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[.='" + subject + "']/ancestor::tr/td[contains(text () ,'" + statusTask + "')]");
			if (isTaskStatusPresent == false) {
				fc.utobj().throwsException("was not able to verify Status Of Task");
			}

			boolean isAssignedTo = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[.='" + subject
					+ "']/ancestor::tr/td[contains(text () ,'" + corpUser.getuserFullName() + "')]");
			if (isAssignedTo == false) {
				fc.utobj().throwsException("was not able to verify Assign To");
			}

			fc.utobj().printTestStep("Verify The Task Detail At Opportunity Info Page");

			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//a[.='" + subject + "']/ancestor::tr/td/a[contains(text () ,'" + opportunityName + "')]"));

			boolean isAssignedToAtLeadInfo = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[.='" + subject
					+ "']/ancestor::tr/td/div[contains(text () ,'" + corpUser.getuserFullName() + "')]");
			if (isAssignedToAtLeadInfo == false) {
				fc.utobj().throwsException("was not able to verify Assign To At Opportunity Info Page");
			}

			boolean isTaskStatusPresentAtLeadInfo = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[.='" + subject + "']/ancestor::tr/td[contains(text () ,'" + statusTask + "')]");
			if (isTaskStatusPresentAtLeadInfo == false) {
				fc.utobj().throwsException("was not able to verify Status Of Task At Opportunity Info Page");
			}

			fc.utobj().printTestStep(
					"Verify that if any task is associated with Any Opportunity so entry will not be delete");

			fc.utobj().clickElement(driver, new CRMAccountsPage(driver).deleteTab);
			
			String alertText = fc.utobj().acceptAlertBox(driver);

			if (!alertText.equalsIgnoreCase(
					"Opportunity cannot be deleted as there are some Transaction(s)/Task(s) associated with this Opportunity.")) 
			{
				fc.utobj().throwsException(
						"Not able to verify that if any task is associated with Any Opportunity so entry will not be delete");
			}

			fc.utobj().printTestStep("Complete The Added Task");
			fc.crm().crm_common().CRMTasksLnk(driver);
			searchBySubjectFilter(driver, subject);
			fc.utobj().sleep(2000);
			fc.utobj().printTestStep("Process Task");
			fc.utobj().actionImgOption(driver, subject, "Process");
			fc.utobj().sleep(2000);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().sleep(2000);
			fc.utobj().selectDropDown(driver, pobj.statusTask, fc.utobj().translateString("Completed"));
			fc.utobj().clickElement(driver, pobj.processTask);
			fc.utobj().sleep(2000);
			fc.utobj().switchFrameToDefault(driver);
			fc.utobj().sleep(2000);
			fc.utobj().printTestStep("Verify that after completing task , Opportunity will be deleted");

			fc.utobj().clickElement(driver, pobj.opportunitiesLinks);
			fc.utobj().sleep(2000);
			opportunityPage.opportunityFilter(driver, stage);
			fc.utobj().sleep(2000);
			fc.utobj().printTestStep("Delete Opportinity");
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//a[.='" + opportunityName + "']/ancestor::tr/td/input"));
			fc.utobj().selectActionMenuItemsWithTagA(driver, "Delete");
			fc.utobj().acceptAlertBox(driver);
			fc.utobj().sleep(2000);
			opportunityPage.opportunityFilter(driver, stage);
			fc.utobj().sleep(2000);
			fc.utobj().printTestStep("Verify Delete Opportunity");
			boolean isNoRecordPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//td[.='No records found.']");
			if (isNoRecordPresent == false) {
				fc.utobj().throwsException("was not able to Delete Opportunity");
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "crm")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Modify Task For Lead By Action Image Option At CRM > Tasks", testCaseId = "TC_282_Modify_Task_Lead")
	public void modifyTask() throws IOException, Exception, ParserConfigurationException, SAXException {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			CRMLeadsPageTest leadPage = new CRMLeadsPageTest();

			CRMTasksPage pobj = new CRMTasksPage(driver);

			fc.utobj().printTestStep("Navigate To Admin Users Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			String emailId = "crmautomation@staffex.com";
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUser = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporateUser.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			fc.utobj().printTestStep("Navigating to Admin > CRM > Configure Status");
			fc.utobj().printTestStep("Add Status Lead Type");

			AdminCRMConfigureStatusPageTest statusPage = new AdminCRMConfigureStatusPageTest();
			String statusName = fc.utobj().generateTestData(dataSet.get("statusName"));
			statusPage.addStatus(driver, statusName);

			fc.utobj().printTestStep("Navigate To CRM > Leads > Leads Summary");
			fc.utobj().printTestStep("Add Lead");

			String firstName = fc.utobj().generateTestData(dataSet.get("firstName"));
			String lastName = fc.utobj().generateTestData(dataSet.get("lastName"));
			String company = fc.utobj().generateTestData(dataSet.get("company"));
			String assignTo = "Corporate";
			String city = fc.utobj().generateTestData(dataSet.get("city"));
			String address = fc.utobj().generateTestData(dataSet.get("address"));
			String suffix = fc.utobj().generateTestData(dataSet.get("suffix"));
			String jobTitle = fc.utobj().generateTestData(dataSet.get("jobTitle"));
			String comment = fc.utobj().generateTestData(dataSet.get("comment"));
			String regionName = "";
			String regionalUser = "";
			String franchiseId = "";
			String franchiseUser = "";

			leadPage.addLead(driver, dataSet, firstName, lastName, company, assignTo, city, address, suffix, jobTitle,
					comment, corpUser.getuserFullName(), regionName, regionalUser, franchiseId, franchiseUser, config);

			pobj = new CRMTasksPage(driver);
			
			fc.utobj().printTestStep("Navigate To CRM > Tasks");
			fc.utobj().printTestStep("Add Task");
			fc.utobj().clickElement(driver, pobj.taskLinks);
			fc.utobj().clickElement(driver, pobj.addTaskLink);
			fc.commonMethods().switch_cboxIframe_frameId(driver);

			if (!fc.utobj().isSelected(driver, pobj.associatedWithLead)) {
				fc.utobj().clickElement(driver, pobj.associatedWithLead);
			}

			String leadName = firstName + " " + lastName;

			fc.utobj().sendKeys(driver, pobj.leadSearch, leadName);
			fc.utobj().sleep(3000);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//span/u[.='" + leadName + "']"));

			if (!fc.utobj().isSelected(driver,pobj.radioOwner)) {
				fc.utobj().clickElement(driver, pobj.radioOwner);
			}
			String statusTask = dataSet.get("statusTask");
			fc.utobj().selectDropDown(driver, pobj.statusTask, statusTask);
			String subject = fc.utobj().generateTestData(dataSet.get("subject"));
			fc.utobj().sendKeys(driver, pobj.subject, subject);
			if (!fc.utobj().isSelected(driver, pobj.timeLessTask)) {
				fc.utobj().clickElement(driver, pobj.timeLessTask);
			}
			String priority = dataSet.get("priority");
			fc.utobj().selectDropDown(driver, pobj.priority, priority);
			fc.utobj().sendKeys(driver, pobj.comments, "Test Comment");
			fc.utobj().clickElement(driver, pobj.createBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Modify Task Detail");
			searchBySubjectFilter(driver, subject);

			fc.utobj().actionImgOption(driver, subject, "Modify");
			fc.commonMethods().switch_cboxIframe_frameId(driver);

			if (!fc.utobj().isSelected(driver,pobj.radioOwner)) {
				fc.utobj().clickElement(driver, pobj.radioOwner);
			}

			subject = fc.utobj().generateTestData(dataSet.get("subject"));
			fc.utobj().sendKeys(driver, pobj.subject, subject);
			if (!fc.utobj().isSelected(driver, pobj.timeLessTask)) {
				fc.utobj().clickElement(driver, pobj.timeLessTask);
			}

			fc.utobj().clickElement(driver, pobj.saveBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Verify The Modify Task Detail");
			searchBySubjectFilter(driver, subject);

			boolean isSubjectPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[.='" + subject + "']");
			if (isSubjectPresent == false) {
				fc.utobj().throwsException("Was not able to verify Subject Of Task");
			}

			boolean isAssociatedWithPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[.='" + subject + "']/ancestor::tr/td/a[contains(text () ,'" + leadName + "')]");
			if (isAssociatedWithPresent == false) {
				fc.utobj().throwsException("was not able to verify Associated With");
			}

			boolean isTaskStatusPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[.='" + subject + "']/ancestor::tr/td[contains(text () ,'" + statusTask + "')]");
			if (isTaskStatusPresent == false) {
				fc.utobj().throwsException("was not able to verify Status Of Task");
			}

			boolean isAssignedTo = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[.='" + subject
					+ "']/ancestor::tr/td[contains(text () ,'" + corpUser.getuserFullName() + "')]");
			if (isAssignedTo == false) {
				fc.utobj().throwsException("was not able to verify Assign To");
			}

			fc.utobj().printTestStep("Verify The Task Detail At Lead Info Page");

			// Verify At Lead Info Page
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,(".//a[.='" + subject + "']/ancestor::tr/td/a[contains(text () ,'" + leadName + "')]")));

			boolean isAssignedToAtLeadInfo = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[.='" + subject
					+ "']/ancestor::tr/td/div[contains(text () ,'" + corpUser.getuserFullName() + "')]");
			if (isAssignedToAtLeadInfo == false) {
				fc.utobj().throwsException("was not able to verify Assign To At Lead Info Page");
			}

			boolean isTaskStatusPresentAtLeadInfo = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[.='" + subject + "']/ancestor::tr/td[contains(text () ,'" + statusTask + "')]");
			if (isTaskStatusPresentAtLeadInfo == false) {
				fc.utobj().throwsException("was not able to verify Status Of Task At Lead Info Page");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "crm")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The View Task For Lead By Action Image At CRM > Tasks", testCaseId = "TC_283_View_Task_Lead")
	public void viewTask() throws IOException, Exception, ParserConfigurationException, SAXException {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			CRMLeadsPageTest leadPage = new CRMLeadsPageTest();

			CRMTasksPage pobj = new CRMTasksPage(driver);

			fc.utobj().printTestStep("Navigate To Admin Users Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			String emailId = "crmautomation@staffex.com";
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUser = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporateUser.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			fc.utobj().printTestStep("Navigating to Admin > CRM > Configure Status");
			fc.utobj().printTestStep("Add Status Lead Type");

			AdminCRMConfigureStatusPageTest statusPage = new AdminCRMConfigureStatusPageTest();
			String statusName = fc.utobj().generateTestData(dataSet.get("statusName"));
			statusPage.addStatus(driver, statusName);

			fc.utobj().printTestStep("Navigate To CRM > Leads > Leads Summary");
			fc.utobj().printTestStep("Add Lead");

			String firstName = fc.utobj().generateTestData(dataSet.get("firstName"));
			String lastName = fc.utobj().generateTestData(dataSet.get("lastName"));
			String company = fc.utobj().generateTestData(dataSet.get("company"));
			String assignTo = "Corporate";
			String city = fc.utobj().generateTestData(dataSet.get("city"));
			String address = fc.utobj().generateTestData(dataSet.get("address"));
			String suffix = fc.utobj().generateTestData(dataSet.get("suffix"));
			String jobTitle = fc.utobj().generateTestData(dataSet.get("jobTitle"));
			String comment = fc.utobj().generateTestData(dataSet.get("comment"));
			String regionName = "";
			String regionalUser = "";
			String franchiseId = "";
			String franchiseUser = "";

			leadPage.addLead(driver, dataSet, firstName, lastName, company, assignTo, city, address, suffix, jobTitle,
					comment, corpUser.getuserFullName(), regionName, regionalUser, franchiseId, franchiseUser, config);

			pobj = new CRMTasksPage(driver);
			
			fc.utobj().printTestStep("Navigate To CRM > Tasks");
			fc.utobj().printTestStep("Add Task");
			fc.utobj().clickElement(driver, pobj.taskLinks);
			fc.utobj().clickElement(driver, pobj.addTaskLink);
			fc.commonMethods().switch_cboxIframe_frameId(driver);

			if (!fc.utobj().isSelected(driver, pobj.associatedWithLead)) {
				fc.utobj().clickElement(driver, pobj.associatedWithLead);
			}

			String leadName = firstName + " " + lastName;

			fc.utobj().sendKeys(driver, pobj.leadSearch, leadName);
			fc.utobj().sleep(3000);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//span/u[.='" + leadName + "']"));

			if (!fc.utobj().isSelected(driver,pobj.radioOwner)) {
				fc.utobj().clickElement(driver, pobj.radioOwner);
			}
			String statusTask = dataSet.get("statusTask");
			fc.utobj().selectDropDown(driver, pobj.statusTask, statusTask);
			String subject = fc.utobj().generateTestData(dataSet.get("subject"));
			fc.utobj().sendKeys(driver, pobj.subject, subject);
			if (!fc.utobj().isSelected(driver, pobj.timeLessTask)) {
				fc.utobj().clickElement(driver, pobj.timeLessTask);
			}
			String priority = dataSet.get("priority");
			fc.utobj().selectDropDown(driver, pobj.priority, priority);
			fc.utobj().sendKeys(driver, pobj.comments, "Test Comment");
			fc.utobj().clickElement(driver, pobj.createBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("View Task Detail");
			searchBySubjectFilter(driver, subject);

			fc.utobj().actionImgOption(driver, subject, "View");
			fc.commonMethods().switch_cboxIframe_frameId(driver);

			boolean isLeadNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[contains(text () ,'" + leadName + "')]");
			if (isLeadNamePresent == false) {
				fc.utobj().throwsException("was not able to verify Lead Name");
			}

			boolean isAssignedToAtViewPage = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//*[contains(text () ,'" + corpUser.getuserFullName() + "')]");
			if (isAssignedToAtViewPage == false) {
				fc.utobj().throwsException("was not able to verify Assign To At view Page");
			}

			boolean isTaskStatusPresentAtLeadInfo = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//*[.='" + statusTask + "']");
			if (isTaskStatusPresentAtLeadInfo == false) {
				fc.utobj().throwsException("was not able to verify Status Of Task At View Page");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "crm")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Delete Task For Lead By Action Image At CRM > Tasks", testCaseId = "TC_284_Delete_Task_Lead")
	public void deleteTask() throws IOException, Exception, ParserConfigurationException, SAXException {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			CRMLeadsPageTest leadPage = new CRMLeadsPageTest();

			CRMTasksPage pobj = new CRMTasksPage(driver);

			fc.utobj().printTestStep("Navigate To Admin Users Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			String emailId = "crmautomation@staffex.com";
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUser = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporateUser.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			fc.utobj().printTestStep("Navigating to Admin > CRM > Configure Status");
			fc.utobj().printTestStep("Add Status Lead Type");

			AdminCRMConfigureStatusPageTest statusPage = new AdminCRMConfigureStatusPageTest();
			String statusName = fc.utobj().generateTestData(dataSet.get("statusName"));
			statusPage.addStatus(driver, statusName);

			fc.utobj().printTestStep("Navigate To CRM > Leads > Leads Summary");
			fc.utobj().printTestStep("Add Lead");

			String firstName = fc.utobj().generateTestData(dataSet.get("firstName"));
			String lastName = fc.utobj().generateTestData(dataSet.get("lastName"));
			String company = fc.utobj().generateTestData(dataSet.get("company"));
			String assignTo = "Corporate";
			String city = fc.utobj().generateTestData(dataSet.get("city"));
			String address = fc.utobj().generateTestData(dataSet.get("address"));
			String suffix = fc.utobj().generateTestData(dataSet.get("suffix"));
			String jobTitle = fc.utobj().generateTestData(dataSet.get("jobTitle"));
			String comment = fc.utobj().generateTestData(dataSet.get("comment"));
			String regionName = "";
			String regionalUser = "";
			String franchiseId = "";
			String franchiseUser = "";

			leadPage.addLead(driver, dataSet, firstName, lastName, company, assignTo, city, address, suffix, jobTitle,
					comment, corpUser.getuserFullName(), regionName, regionalUser, franchiseId, franchiseUser, config);

			pobj = new CRMTasksPage(driver);
			
			fc.utobj().printTestStep("Navigate To CRM > Tasks");
			fc.utobj().printTestStep("Add Task");
			fc.utobj().clickElement(driver, pobj.taskLinks);
			fc.utobj().clickElement(driver, pobj.addTaskLink);
			fc.commonMethods().switch_cboxIframe_frameId(driver);

			if (!fc.utobj().isSelected(driver, pobj.associatedWithLead)) {
				fc.utobj().clickElement(driver, pobj.associatedWithLead);
			}

			String leadName = firstName + " " + lastName;

			fc.utobj().sendKeys(driver, pobj.leadSearch, leadName);
			fc.utobj().sleep(3000);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//span/u[.='" + leadName + "']"));

			if (!fc.utobj().isSelected(driver,pobj.radioOwner)) {
				fc.utobj().clickElement(driver, pobj.radioOwner);
			}
			String statusTask = dataSet.get("statusTask");
			fc.utobj().selectDropDown(driver, pobj.statusTask, statusTask);
			String subject = fc.utobj().generateTestData(dataSet.get("subject"));
			fc.utobj().sendKeys(driver, pobj.subject, subject);
			if (!fc.utobj().isSelected(driver, pobj.timeLessTask)) {
				fc.utobj().clickElement(driver, pobj.timeLessTask);
			}
			String priority = dataSet.get("priority");
			fc.utobj().selectDropDown(driver, pobj.priority, priority);
			fc.utobj().sendKeys(driver, pobj.comments, "Test Comment");
			fc.utobj().clickElement(driver, pobj.createBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Delete Task");
			searchBySubjectFilter(driver, subject);
			fc.utobj().actionImgOption(driver, subject, "Delete");
			fc.utobj().acceptAlertBox(driver);

			fc.utobj().printTestStep("Verify The Delete Task");
			searchBySubjectFilter(driver, subject);

			boolean isTaskPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//td[contains(text () ,'No records found.')]");
			if (isTaskPresent == false) {
				fc.utobj().throwsException("was not able to Verify The Delete Task");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "crm")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Process Task For Lead By Action Image At CRM > Tasks", testCaseId = "TC_285_Process_Task_Lead")
	public void processTask() throws IOException, Exception, ParserConfigurationException, SAXException {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			CRMLeadsPageTest leadPage = new CRMLeadsPageTest();

			CRMTasksPage pobj = new CRMTasksPage(driver);

			fc.utobj().printTestStep("Navigate To Admin Users Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			String emailId = "crmautomation@staffex.com";
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUser = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporateUser.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			fc.utobj().printTestStep("Navigating to Admin > CRM > Configure Status");
			fc.utobj().printTestStep("Add Status Lead Type");

			AdminCRMConfigureStatusPageTest statusPage = new AdminCRMConfigureStatusPageTest();
			String statusName = fc.utobj().generateTestData(dataSet.get("statusName"));
			statusPage.addStatus(driver, statusName);

			fc.utobj().printTestStep("Navigate To CRM > Leads > Leads Summary");
			fc.utobj().printTestStep("Add Lead");

			String firstName = fc.utobj().generateTestData(dataSet.get("firstName"));
			String lastName = fc.utobj().generateTestData(dataSet.get("lastName"));
			String company = fc.utobj().generateTestData(dataSet.get("company"));
			String assignTo = "Corporate";
			String city = fc.utobj().generateTestData(dataSet.get("city"));
			String address = fc.utobj().generateTestData(dataSet.get("address"));
			String suffix = fc.utobj().generateTestData(dataSet.get("suffix"));
			String jobTitle = fc.utobj().generateTestData(dataSet.get("jobTitle"));
			String comment = fc.utobj().generateTestData(dataSet.get("comment"));
			String regionName = "";
			String regionalUser = "";
			String franchiseId = "";
			String franchiseUser = "";

			leadPage.addLead(driver, dataSet, firstName, lastName, company, assignTo, city, address, suffix, jobTitle,
					comment, corpUser.getuserFullName(), regionName, regionalUser, franchiseId, franchiseUser, config);

			pobj = new CRMTasksPage(driver);
			
			fc.utobj().printTestStep("Navigate To CRM > Tasks");
			fc.utobj().printTestStep("Add Task");
			fc.utobj().clickElement(driver, pobj.taskLinks);
			fc.utobj().clickElement(driver, pobj.addTaskLink);
			fc.commonMethods().switch_cboxIframe_frameId(driver);

			if (!fc.utobj().isSelected(driver, pobj.associatedWithLead)) {
				fc.utobj().clickElement(driver, pobj.associatedWithLead);
			}

			String leadName = firstName + " " + lastName;

			fc.utobj().sendKeys(driver, pobj.leadSearch, leadName);
			fc.utobj().sleep(3000);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//span/u[.='" + leadName + "']"));

			if (!fc.utobj().isSelected(driver,pobj.radioOwner)) {
				fc.utobj().clickElement(driver, pobj.radioOwner);
			}
			String statusTask = dataSet.get("statusTask");
			fc.utobj().selectDropDown(driver, pobj.statusTask, statusTask);
			String subject = fc.utobj().generateTestData(dataSet.get("subject"));
			fc.utobj().sendKeys(driver, pobj.subject, subject);
			if (!fc.utobj().isSelected(driver, pobj.timeLessTask)) {
				fc.utobj().clickElement(driver, pobj.timeLessTask);
			}
			String priority = dataSet.get("priority");
			fc.utobj().selectDropDown(driver, pobj.priority, priority);
			fc.utobj().sendKeys(driver, pobj.comments, "Test Comment");
			fc.utobj().clickElement(driver, pobj.createBtn);
			fc.utobj().switchFrameToDefault(driver);

			String taskStatusProcess = dataSet.get("taskStatusProcess");

			fc.utobj().printTestStep("Process Task");
			searchBySubjectFilter(driver, subject);
			fc.utobj().actionImgOption(driver, subject, "Process");

			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().selectDropDown(driver, pobj.statusTask, taskStatusProcess);
			fc.utobj().clickElement(driver, pobj.processTask);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Verify The Process Task Detail");
			searchBySubjectFilter(driver, subject);

			boolean isSubjectPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[.='" + subject + "']");
			if (isSubjectPresent == false) {
				fc.utobj().throwsException("Was not able to verify Subject Of Task");
			}

			boolean isAssociatedWithPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[.='" + subject + "']/ancestor::tr/td/a[contains(text () ,'" + leadName + "')]");
			if (isAssociatedWithPresent == false) {
				fc.utobj().throwsException("was not able to verify Associated With");
			}

			boolean isTaskStatusPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[.='" + subject + "']/ancestor::tr/td[contains(text () ,'" + taskStatusProcess + "')]");
			if (isTaskStatusPresent == false) {
				fc.utobj().throwsException("was not able to verify Status Of Task");
			}

			boolean isAssignedTo = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[.='" + subject
					+ "']/ancestor::tr/td[contains(text () ,'" + corpUser.getuserFullName() + "')]");
			if (isAssignedTo == false) {
				fc.utobj().throwsException("was not able to verify Assign To");
			}

			fc.utobj().printTestStep("Verify The Task Detail At Lead Info Page");

			// Verify At Lead Info Page
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,(".//a[.='" + subject + "']/ancestor::tr/td/a[contains(text () ,'" + leadName + "')]")));

			boolean isAssignedToAtLeadInfo = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[.='" + subject
					+ "']/ancestor::tr/td/div[contains(text () ,'" + corpUser.getuserFullName() + "')]");
			if (isAssignedToAtLeadInfo == false) {
				fc.utobj().throwsException("was not able to verify Assign To At Lead Info Page");
			}

			boolean isTaskStatusPresentAtLeadInfo = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[.='" + subject + "']/ancestor::tr/td[contains(text () ,'" + taskStatusProcess + "')]");
			if (isTaskStatusPresentAtLeadInfo == false) {
				fc.utobj().throwsException("was not able to verify Status Of Task At Lead Info Page");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = {"crm"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Change Task Status For Lead By Action Button At CRM > Tasks", testCaseId = "TC_286_Change_Task_Status_Lead")
	public void changeStatusTask() throws IOException, Exception, ParserConfigurationException, SAXException {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			CRMLeadsPageTest leadPage = new CRMLeadsPageTest();

			CRMTasksPage pobj = new CRMTasksPage(driver);

			fc.utobj().printTestStep("Navigate To Admin Users Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			String emailId = "crmautomation@staffex.com";
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUser = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporateUser.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			fc.utobj().printTestStep("Navigating to Admin > CRM > Configure Status");
			fc.utobj().printTestStep("Add Status Lead Type");

			AdminCRMConfigureStatusPageTest statusPage = new AdminCRMConfigureStatusPageTest();
			String statusName = fc.utobj().generateTestData(dataSet.get("statusName"));
			statusPage.addStatus(driver, statusName);

			fc.utobj().printTestStep("Navigate To CRM > Leads > Leads Summary");
			fc.utobj().printTestStep("Add Lead");

			String firstName = fc.utobj().generateTestData(dataSet.get("firstName"));
			String lastName = fc.utobj().generateTestData(dataSet.get("lastName"));
			String company = fc.utobj().generateTestData(dataSet.get("company"));
			String assignTo = "Corporate";
			String city = fc.utobj().generateTestData(dataSet.get("city"));
			String address = fc.utobj().generateTestData(dataSet.get("address"));
			String suffix = fc.utobj().generateTestData(dataSet.get("suffix"));
			String jobTitle = fc.utobj().generateTestData(dataSet.get("jobTitle"));
			String comment = fc.utobj().generateTestData(dataSet.get("comment"));
			String regionName = "";
			String regionalUser = "";
			String franchiseId = "";
			String franchiseUser = "";

			leadPage.addLead(driver, dataSet, firstName, lastName, company, assignTo, city, address, suffix, jobTitle,
					comment, corpUser.getuserFullName(), regionName, regionalUser, franchiseId, franchiseUser, config);

			pobj = new CRMTasksPage(driver);
			
			fc.utobj().printTestStep("Navigate To CRM > Tasks");
			fc.utobj().printTestStep("Add Task");
			fc.utobj().clickElement(driver, pobj.taskLinks);
			fc.utobj().clickElement(driver, pobj.addTaskLink);
			fc.commonMethods().switch_cboxIframe_frameId(driver);

			if (!fc.utobj().isSelected(driver, pobj.associatedWithLead)) {
				fc.utobj().clickElement(driver, pobj.associatedWithLead);
			}

			String leadName = firstName + " " + lastName;

			fc.utobj().sendKeys(driver, pobj.leadSearch, leadName);
			fc.utobj().sleep(3000);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//span/u[.='" + leadName + "']"));

			if (!fc.utobj().isSelected(driver,pobj.radioOwner)) {
				fc.utobj().clickElement(driver, pobj.radioOwner);
			}
			String statusTask = dataSet.get("statusTask");
			fc.utobj().selectDropDown(driver, pobj.statusTask, statusTask);
			String subject = fc.utobj().generateTestData(dataSet.get("subject"));
			fc.utobj().sendKeys(driver, pobj.subject, subject);
			if (!fc.utobj().isSelected(driver, pobj.timeLessTask)) {
				fc.utobj().clickElement(driver, pobj.timeLessTask);
			}
			String priority = dataSet.get("priority");
			fc.utobj().selectDropDown(driver, pobj.priority, priority);
			fc.utobj().sendKeys(driver, pobj.comments, "Test Comment");
			fc.utobj().clickElement(driver, pobj.createBtn);
			fc.utobj().switchFrameToDefault(driver);

			String statusChange = dataSet.get("statusChange");

			fc.utobj().printTestStep("Change Status Of Task");
			searchBySubjectFilter(driver, subject);

			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//a[.='" + subject + "']/ancestor::tr/td/input[@name='checkb']"));
			fc.utobj().selectActionMenuItems(driver, "Change Status");
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().selectDropDown(driver, pobj.statusTask, statusChange);
			fc.utobj().sendKeys(driver, pobj.comments, "Test Comments");
			fc.utobj().clickElement(driver, pobj.changeButton);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Verify The Change Task Status");
			searchBySubjectFilter(driver, subject);

			boolean isSubjectPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[.='" + subject + "']");
			if (isSubjectPresent == false) {
				fc.utobj().throwsException("Was not able to verify Subject Of Task");
			}

			boolean isAssociatedWithPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[.='" + subject + "']/ancestor::tr/td/a[contains(text () ,'" + leadName + "')]");
			if (isAssociatedWithPresent == false) {
				fc.utobj().throwsException("was not able to verify Associated With");
			}

			boolean isTaskStatusPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[.='" + subject + "']/ancestor::tr/td[contains(text () ,'" + statusChange + "')]");
			if (isTaskStatusPresent == false) {
				fc.utobj().throwsException("was not able to verify Status Of Task");
			}

			boolean isAssignedTo = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[.='" + subject
					+ "']/ancestor::tr/td[contains(text () ,'" + corpUser.getuserFullName() + "')]");
			if (isAssignedTo == false) {
				fc.utobj().throwsException("was not able to verify Assign To");
			}

			fc.utobj().printTestStep("Verify The Task Detail At Lead Info Page");

			// Verify At Lead Info Page
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,(".//a[.='" + subject + "']/ancestor::tr/td/a[contains(text () ,'" + leadName + "')]")));

			boolean isAssignedToAtLeadInfo = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[.='" + subject
					+ "']/ancestor::tr/td/div[contains(text () ,'" + corpUser.getuserFullName() + "')]");
			if (isAssignedToAtLeadInfo == false) {
				fc.utobj().throwsException("was not able to verify Assign To At Lead Info Page");
			}

			boolean isTaskStatusPresentAtLeadInfo = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[.='" + subject + "']/ancestor::tr/td[contains(text () ,'" + statusChange + "')]");
			if (isTaskStatusPresentAtLeadInfo == false) {
				fc.utobj().throwsException("was not able to verify Status Of Task At Lead Info Page");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "crm")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Delete Task For Lead By Action Button At CRM > Tasks", testCaseId = "TC_287_Delete_Task_Lead")
	public void deleteTaskByActionButton() throws IOException, Exception, ParserConfigurationException, SAXException {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			CRMLeadsPageTest leadPage = new CRMLeadsPageTest();

			CRMTasksPage pobj = new CRMTasksPage(driver);

			fc.utobj().printTestStep("Navigate To Admin Users Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			String emailId = "crmautomation@staffex.com";
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUser = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporateUser.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			fc.utobj().printTestStep("Navigating to Admin > CRM > Configure Status");
			fc.utobj().printTestStep("Add Status Lead Type");

			AdminCRMConfigureStatusPageTest statusPage = new AdminCRMConfigureStatusPageTest();
			String statusName = fc.utobj().generateTestData(dataSet.get("statusName"));
			statusPage.addStatus(driver, statusName);

			fc.utobj().printTestStep("Navigate To CRM > Leads > Leads Summary");
			fc.utobj().printTestStep("Add Lead");

			String firstName = fc.utobj().generateTestData(dataSet.get("firstName"));
			String lastName = fc.utobj().generateTestData(dataSet.get("lastName"));
			String company = fc.utobj().generateTestData(dataSet.get("company"));
			String assignTo = "Corporate";
			String city = fc.utobj().generateTestData(dataSet.get("city"));
			String address = fc.utobj().generateTestData(dataSet.get("address"));
			String suffix = fc.utobj().generateTestData(dataSet.get("suffix"));
			String jobTitle = fc.utobj().generateTestData(dataSet.get("jobTitle"));
			String comment = fc.utobj().generateTestData(dataSet.get("comment"));
			String regionName = "";
			String regionalUser = "";
			String franchiseId = "";
			String franchiseUser = "";

			leadPage.addLead(driver, dataSet, firstName, lastName, company, assignTo, city, address, suffix, jobTitle,
					comment, corpUser.getuserFullName(), regionName, regionalUser, franchiseId, franchiseUser, config);

			pobj = new CRMTasksPage(driver);
			
			fc.utobj().printTestStep("Navigate To CRM > Tasks");
			fc.utobj().printTestStep("Add Task");
			fc.utobj().clickElement(driver, pobj.taskLinks);
			fc.utobj().clickElement(driver, pobj.addTaskLink);
			fc.commonMethods().switch_cboxIframe_frameId(driver);

			if (!fc.utobj().isSelected(driver, pobj.associatedWithLead)) {
				fc.utobj().clickElement(driver, pobj.associatedWithLead);
			}

			String leadName = firstName + " " + lastName;

			fc.utobj().sendKeys(driver, pobj.leadSearch, leadName);
			fc.utobj().sleep(3000);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//span/u[.='" + leadName + "']"));

			if (!fc.utobj().isSelected(driver,pobj.radioOwner)) {
				fc.utobj().clickElement(driver, pobj.radioOwner);
			}
			String statusTask = dataSet.get("statusTask");
			fc.utobj().selectDropDown(driver, pobj.statusTask, statusTask);
			String subject = fc.utobj().generateTestData(dataSet.get("subject"));
			fc.utobj().sendKeys(driver, pobj.subject, subject);
			if (!fc.utobj().isSelected(driver, pobj.timeLessTask)) {
				fc.utobj().clickElement(driver, pobj.timeLessTask);
			}
			String priority = dataSet.get("priority");
			fc.utobj().selectDropDown(driver, pobj.priority, priority);
			fc.utobj().sendKeys(driver, pobj.comments, "Test Comment");
			fc.utobj().clickElement(driver, pobj.createBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Delete Task");
			searchBySubjectFilter(driver, subject);

			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//a[.='" + subject + "']/ancestor::tr/td/input[@name='checkb']"));
			fc.utobj().selectActionMenuItems(driver, "Delete");
			fc.utobj().acceptAlertBox(driver);

			fc.utobj().printTestStep("Verify The Delete Task");
			searchBySubjectFilter(driver, subject);

			boolean isTaskPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//td[contains(text () ,'No records found.')]");
			if (isTaskPresent == false) {
				fc.utobj().throwsException("was not able to Verify The Delete Task");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "crm")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Change Task Status For Lead By Bottom Button At CRM > Tasks", testCaseId = "TC_288_Change_Task_Status_Lead")
	public void changeStatusTaskByBottomButton()
			throws IOException, Exception, ParserConfigurationException, SAXException {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			CRMLeadsPageTest leadPage = new CRMLeadsPageTest();

			CRMTasksPage pobj = new CRMTasksPage(driver);

			fc.utobj().printTestStep("Navigate To Admin Users Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			String emailId = "crmautomation@staffex.com";
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUser = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporateUser.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			fc.utobj().printTestStep("Navigating to Admin > CRM > Configure Status");
			fc.utobj().printTestStep("Add Status Lead Type");

			AdminCRMConfigureStatusPageTest statusPage = new AdminCRMConfigureStatusPageTest();
			String statusName = fc.utobj().generateTestData(dataSet.get("statusName"));
			statusPage.addStatus(driver, statusName);

			fc.utobj().printTestStep("Navigate To CRM > Leads > Leads Summary");
			fc.utobj().printTestStep("Add Lead");

			String firstName = fc.utobj().generateTestData(dataSet.get("firstName"));
			String lastName = fc.utobj().generateTestData(dataSet.get("lastName"));
			String company = fc.utobj().generateTestData(dataSet.get("company"));
			String assignTo = "Corporate";
			String city = fc.utobj().generateTestData(dataSet.get("city"));
			String address = fc.utobj().generateTestData(dataSet.get("address"));
			String suffix = fc.utobj().generateTestData(dataSet.get("suffix"));
			String jobTitle = fc.utobj().generateTestData(dataSet.get("jobTitle"));
			String comment = fc.utobj().generateTestData(dataSet.get("comment"));
			String regionName = "";
			String regionalUser = "";
			String franchiseId = "";
			String franchiseUser = "";

			leadPage.addLead(driver, dataSet, firstName, lastName, company, assignTo, city, address, suffix, jobTitle,
					comment, corpUser.getuserFullName(), regionName, regionalUser, franchiseId, franchiseUser, config);

			pobj = new CRMTasksPage(driver);
			
			fc.utobj().printTestStep("Navigate To CRM > Tasks");
			fc.utobj().printTestStep("Add Task");
			fc.utobj().clickElement(driver, pobj.taskLinks);
			fc.utobj().clickElement(driver, pobj.addTaskLink);
			fc.commonMethods().switch_cboxIframe_frameId(driver);

			if (!fc.utobj().isSelected(driver, pobj.associatedWithLead)) {
				fc.utobj().clickElement(driver, pobj.associatedWithLead);
			}

			String leadName = firstName + " " + lastName;

			fc.utobj().sendKeys(driver, pobj.leadSearch, leadName);
			fc.utobj().sleep(3000);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//span/u[.='" + leadName + "']"));

			if (!fc.utobj().isSelected(driver,pobj.radioOwner)) {
				fc.utobj().clickElement(driver, pobj.radioOwner);
			}
			String statusTask = dataSet.get("statusTask");
			fc.utobj().selectDropDown(driver, pobj.statusTask, statusTask);
			String subject = fc.utobj().generateTestData(dataSet.get("subject"));
			fc.utobj().sendKeys(driver, pobj.subject, subject);
			if (!fc.utobj().isSelected(driver, pobj.timeLessTask)) {
				fc.utobj().clickElement(driver, pobj.timeLessTask);
			}
			String priority = dataSet.get("priority");
			fc.utobj().selectDropDown(driver, pobj.priority, priority);
			fc.utobj().sendKeys(driver, pobj.comments, "Test Comment");
			fc.utobj().clickElement(driver, pobj.createBtn);
			fc.utobj().switchFrameToDefault(driver);

			String statusChange = dataSet.get("statusChange");

			fc.utobj().printTestStep("Change Status Of Task");
			searchBySubjectFilter(driver, subject);

			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//a[.='" + subject + "']/ancestor::tr/td/input[@name='checkb']"));

			fc.utobj().clickElement(driver, pobj.chnageBottomBtn);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().selectDropDown(driver, pobj.statusTask, statusChange);
			fc.utobj().sendKeys(driver, pobj.comments, "Test Comments");
			fc.utobj().clickElement(driver, pobj.changeButton);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Verify The Change Task Status");
			searchBySubjectFilter(driver, subject);

			boolean isSubjectPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[.='" + subject + "']");
			if (isSubjectPresent == false) {
				fc.utobj().throwsException("Was not able to verify Subject Of Task");
			}

			boolean isAssociatedWithPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[.='" + subject + "']/ancestor::tr/td/a[contains(text () ,'" + leadName + "')]");
			if (isAssociatedWithPresent == false) {
				fc.utobj().throwsException("was not able to verify Associated With");
			}

			boolean isTaskStatusPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[.='" + subject + "']/ancestor::tr/td[contains(text () ,'" + statusChange + "')]");
			if (isTaskStatusPresent == false) {
				fc.utobj().throwsException("was not able to verify Status Of Task");
			}

			boolean isAssignedTo = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[.='" + subject
					+ "']/ancestor::tr/td[contains(text () ,'" + corpUser.getuserFullName() + "')]");
			if (isAssignedTo == false) {
				fc.utobj().throwsException("was not able to verify Assign To");
			}

			fc.utobj().printTestStep("Verify The Task Detail At Lead Info Page");

			// Verify At Lead Info Page
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,(".//a[.='" + subject + "']/ancestor::tr/td/a[contains(text () ,'" + leadName + "')]")));

			boolean isAssignedToAtLeadInfo = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[.='" + subject
					+ "']/ancestor::tr/td/div[contains(text () ,'" + corpUser.getuserFullName() + "')]");
			if (isAssignedToAtLeadInfo == false) {
				fc.utobj().throwsException("was not able to verify Assign To At Lead Info Page");
			}

			boolean isTaskStatusPresentAtLeadInfo = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[.='" + subject + "']/ancestor::tr/td[contains(text () ,'" + statusChange + "')]");
			if (isTaskStatusPresentAtLeadInfo == false) {
				fc.utobj().throwsException("was not able to verify Status Of Task At Lead Info Page");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "crm")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Delete Task For Lead By Bottom Button At CRM > Tasks", testCaseId = "TC_289_Delete_Task_Lead")
	public void deleteTaskByBottomButton() throws IOException, Exception, ParserConfigurationException, SAXException {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			CRMLeadsPageTest leadPage = new CRMLeadsPageTest();

			CRMTasksPage pobj = new CRMTasksPage(driver);

			fc.utobj().printTestStep("Navigate To Admin Users Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			String emailId = "crmautomation@staffex.com";
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUser = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporateUser.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			fc.utobj().printTestStep("Navigating to Admin > CRM > Configure Status");
			fc.utobj().printTestStep("Add Status Lead Type");

			AdminCRMConfigureStatusPageTest statusPage = new AdminCRMConfigureStatusPageTest();
			String statusName = fc.utobj().generateTestData(dataSet.get("statusName"));
			statusPage.addStatus(driver, statusName);

			fc.utobj().printTestStep("Navigate To CRM > Leads > Leads Summary");
			fc.utobj().printTestStep("Add Lead");

			String firstName = fc.utobj().generateTestData(dataSet.get("firstName"));
			String lastName = fc.utobj().generateTestData(dataSet.get("lastName"));
			String company = fc.utobj().generateTestData(dataSet.get("company"));
			String assignTo = "Corporate";
			String city = fc.utobj().generateTestData(dataSet.get("city"));
			String address = fc.utobj().generateTestData(dataSet.get("address"));
			String suffix = fc.utobj().generateTestData(dataSet.get("suffix"));
			String jobTitle = fc.utobj().generateTestData(dataSet.get("jobTitle"));
			String comment = fc.utobj().generateTestData(dataSet.get("comment"));
			String regionName = "";
			String regionalUser = "";
			String franchiseId = "";
			String franchiseUser = "";

			leadPage.addLead(driver, dataSet, firstName, lastName, company, assignTo, city, address, suffix, jobTitle,
					comment, corpUser.getuserFullName(), regionName, regionalUser, franchiseId, franchiseUser, config);

			pobj = new CRMTasksPage(driver);
			
			fc.utobj().printTestStep("Navigate To CRM > Tasks");
			fc.utobj().printTestStep("Add Task");
			fc.utobj().clickElement(driver, pobj.taskLinks);
			fc.utobj().clickElement(driver, pobj.addTaskLink);
			fc.commonMethods().switch_cboxIframe_frameId(driver);

			if (!fc.utobj().isSelected(driver, pobj.associatedWithLead)) {
				fc.utobj().clickElement(driver, pobj.associatedWithLead);
			}

			String leadName = firstName + " " + lastName;

			fc.utobj().sendKeys(driver, pobj.leadSearch, leadName);
			fc.utobj().sleep(3000);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//span/u[.='" + leadName + "']"));

			if (!fc.utobj().isSelected(driver,pobj.radioOwner)) {
				fc.utobj().clickElement(driver, pobj.radioOwner);
			}
			String statusTask = dataSet.get("statusTask");
			fc.utobj().selectDropDown(driver, pobj.statusTask, statusTask);
			String subject = fc.utobj().generateTestData(dataSet.get("subject"));
			fc.utobj().sendKeys(driver, pobj.subject, subject);
			if (!fc.utobj().isSelected(driver, pobj.timeLessTask)) {
				fc.utobj().clickElement(driver, pobj.timeLessTask);
			}
			String priority = dataSet.get("priority");
			fc.utobj().selectDropDown(driver, pobj.priority, priority);
			fc.utobj().sendKeys(driver, pobj.comments, "Test Comment");
			fc.utobj().clickElement(driver, pobj.createBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Delete Task");
			searchBySubjectFilter(driver, subject);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//a[.='" + subject + "']/ancestor::tr/td/input[@name='checkb']"));

			fc.utobj().clickElement(driver, pobj.deleteBottomButton);
			fc.utobj().acceptAlertBox(driver);

			fc.utobj().printTestStep("Verify The Delete Task");
			searchBySubjectFilter(driver, subject);

			boolean isTaskPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//td[contains(text () ,'No records found.')]");
			if (isTaskPresent == false) {
				fc.utobj().throwsException("was not able to Verify The Delete Task");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = {"crm"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Modify Task For Lead At View Frame At CRM > Tasks", testCaseId = "TC_290_Modify_Task_Lead")
	public void modifyTaskAtViewFrame() throws IOException, Exception, ParserConfigurationException, SAXException {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			CRMLeadsPageTest leadPage = new CRMLeadsPageTest();

			CRMTasksPage pobj = new CRMTasksPage(driver);

			fc.utobj().printTestStep("Navigate To Admin Users Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			String emailId = "crmautomation@staffex.com";
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUser = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporateUser.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			fc.utobj().printTestStep("Navigating to Admin > CRM > Configure Status");
			fc.utobj().printTestStep("Add Status Lead Type");

			AdminCRMConfigureStatusPageTest statusPage = new AdminCRMConfigureStatusPageTest();
			String statusName = fc.utobj().generateTestData(dataSet.get("statusName"));
			statusPage.addStatus(driver, statusName);

			fc.utobj().printTestStep("Navigate To CRM > Leads > Leads Summary");
			fc.utobj().printTestStep("Add Lead");

			String firstName = fc.utobj().generateTestData(dataSet.get("firstName"));
			String lastName = fc.utobj().generateTestData(dataSet.get("lastName"));
			String company = fc.utobj().generateTestData(dataSet.get("company"));
			String assignTo = "Corporate";
			String city = fc.utobj().generateTestData(dataSet.get("city"));
			String address = fc.utobj().generateTestData(dataSet.get("address"));
			String suffix = fc.utobj().generateTestData(dataSet.get("suffix"));
			String jobTitle = fc.utobj().generateTestData(dataSet.get("jobTitle"));
			String comment = fc.utobj().generateTestData(dataSet.get("comment"));
			String regionName = "";
			String regionalUser = "";
			String franchiseId = "";
			String franchiseUser = "";

			leadPage.addLead(driver, dataSet, firstName, lastName, company, assignTo, city, address, suffix, jobTitle,
					comment, corpUser.getuserFullName(), regionName, regionalUser, franchiseId, franchiseUser, config);

			pobj = new CRMTasksPage(driver);
			
			fc.utobj().printTestStep("Navigate To CRM > Tasks");
			fc.utobj().printTestStep("Add Task");
			fc.utobj().clickElement(driver, pobj.taskLinks);
			fc.utobj().clickElement(driver, pobj.addTaskLink);
			fc.commonMethods().switch_cboxIframe_frameId(driver);

			if (!fc.utobj().isSelected(driver, pobj.associatedWithLead)) {
				fc.utobj().clickElement(driver, pobj.associatedWithLead);
			}

			String leadName = firstName + " " + lastName;

			fc.utobj().sendKeys(driver, pobj.leadSearch, leadName);

			//fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//span/u[.='" + leadName + "']"));
			JavascriptExecutor js = (JavascriptExecutor)driver;
			WebElement button =driver.findElement(By.xpath(".//span/u[.='" + leadName + "']"));
			js.executeScript("arguments[0].click();", button);
			
			
			if (!fc.utobj().isSelected(driver,pobj.radioOwner)) {
				fc.utobj().clickElement(driver, pobj.radioOwner);
			}
			String statusTask = dataSet.get("statusTask");
			fc.utobj().selectDropDown(driver, pobj.statusTask, statusTask);
			String subject = fc.utobj().generateTestData(dataSet.get("subject"));
			fc.utobj().sendKeys(driver, pobj.subject, subject);
			if (!fc.utobj().isSelected(driver, pobj.timeLessTask)) {
				fc.utobj().clickElement(driver, pobj.timeLessTask);
			}
			String priority = dataSet.get("priority");
			fc.utobj().selectDropDown(driver, pobj.priority, priority);
			fc.utobj().sendKeys(driver, pobj.comments, "Test Comment");
			fc.utobj().clickElement(driver, pobj.createBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("View Task Detail");
			fc.utobj().sleep(2000);
			searchBySubjectFilter(driver, subject);
			fc.utobj().sleep(2000);
			fc.utobj().actionImgOption(driver, subject, "View");
			fc.commonMethods().switch_cboxIframe_frameId(driver);

			fc.utobj().printTestStep("Modify Task Detail");
			fc.utobj().clickElement(driver, pobj.modifyTask);

			if (!fc.utobj().isSelected(driver,pobj.radioOwner)) {
				fc.utobj().clickElement(driver, pobj.radioOwner);
			}

			subject = fc.utobj().generateTestData(dataSet.get("subject"));
			fc.utobj().sendKeys(driver, pobj.subject, subject);
			if (!fc.utobj().isSelected(driver, pobj.timeLessTask)) {
				fc.utobj().clickElement(driver, pobj.timeLessTask);
			}

			fc.utobj().clickElement(driver, pobj.saveBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Verify The Modify Task Detail");
			searchBySubjectFilter(driver, subject);

			boolean isSubjectPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[.='" + subject + "']");
			if (isSubjectPresent == false) {
				fc.utobj().throwsException("Was not able to verify Subject Of Task");
			}

			boolean isAssociatedWithPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[.='" + subject + "']/ancestor::tr/td/a[contains(text () ,'" + leadName + "')]");
			if (isAssociatedWithPresent == false) {
				fc.utobj().throwsException("was not able to verify Associated With");
			}

			boolean isTaskStatusPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[.='" + subject + "']/ancestor::tr/td[contains(text () ,'" + statusTask + "')]");
			if (isTaskStatusPresent == false) {
				fc.utobj().throwsException("was not able to verify Status Of Task");
			}

			boolean isAssignedTo = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[.='" + subject
					+ "']/ancestor::tr/td[contains(text () ,'" + corpUser.getuserFullName() + "')]");
			if (isAssignedTo == false) {
				fc.utobj().throwsException("was not able to verify Assign To");
			}

			fc.utobj().printTestStep("Verify The Task Detail At Lead Info Page");

			// Verify At Lead Info Page
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,(".//a[.='" + subject + "']/ancestor::tr/td/a[contains(text () ,'" + leadName + "')]")));

			boolean isAssignedToAtLeadInfo = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[.='" + subject
					+ "']/ancestor::tr/td/div[contains(text () ,'" + corpUser.getuserFullName() + "')]");
			if (isAssignedToAtLeadInfo == false) {
				fc.utobj().throwsException("was not able to verify Assign To At Lead Info Page");
			}

			boolean isTaskStatusPresentAtLeadInfo = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[.='" + subject + "']/ancestor::tr/td[contains(text () ,'" + statusTask + "')]");
			if (isTaskStatusPresentAtLeadInfo == false) {
				fc.utobj().throwsException("was not able to verify Status Of Task At Lead Info Page");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "crm"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Complete Task For Lead At View Frame At CRM > Tasks And Verify Task Creation Mail", testCaseId = "TC_291_Complete_Task_Lead")
	public void completeTaskAtViewFrame() throws IOException, Exception, ParserConfigurationException, SAXException {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			CRMLeadsPageTest leadPage = new CRMLeadsPageTest();
			CRMTasksPage pobj = new CRMTasksPage(driver);

			fc.utobj().printTestStep("Configure Task Emails");
			fc.adminpage().openConfigureTaskEmailsPage(driver);
			fc.utobj().clickElement(driver, pobj.configureCompletionMail);
			String mailSubject = "Task Completion Notification";
			fc.utobj().sendKeys(driver, pobj.subjectField, mailSubject);
			fc.utobj().clickElement(driver, pobj.configureBtn);

			fc.utobj().printTestStep("Navigate To Admin Users Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			String emailId = "crmautomation@staffex.com";
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUser = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporateUser.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			fc.utobj().printTestStep("Navigating to Admin > CRM > Configure Status");
			fc.utobj().printTestStep("Add Status Lead Type");

			AdminCRMConfigureStatusPageTest statusPage = new AdminCRMConfigureStatusPageTest();
			String statusName = fc.utobj().generateTestData(dataSet.get("statusName"));
			statusPage.addStatus(driver, statusName);

			fc.utobj().printTestStep("Navigate To CRM > Leads > Leads Summary");
			fc.utobj().printTestStep("Add Lead");

			String firstName = fc.utobj().generateTestData(dataSet.get("firstName"));
			String lastName = fc.utobj().generateTestData(dataSet.get("lastName"));
			String company = fc.utobj().generateTestData(dataSet.get("company"));
			String assignTo = "Corporate";
			String city = fc.utobj().generateTestData(dataSet.get("city"));
			String address = fc.utobj().generateTestData(dataSet.get("address"));
			String suffix = fc.utobj().generateTestData(dataSet.get("suffix"));
			String jobTitle = fc.utobj().generateTestData(dataSet.get("jobTitle"));
			String comment = fc.utobj().generateTestData(dataSet.get("comment"));
			String regionName = "";
			String regionalUser = "";
			String franchiseId = "";
			String franchiseUser = "";

			leadPage.addLead(driver, dataSet, firstName, lastName, company, assignTo, city, address, suffix, jobTitle,
					comment, corpUser.getuserFullName(), regionName, regionalUser, franchiseId, franchiseUser, config);

			pobj = new CRMTasksPage(driver);
			
			fc.utobj().printTestStep("Navigate To CRM > Tasks");
			fc.utobj().printTestStep("Add Task");
			fc.utobj().clickElement(driver, pobj.taskLinks);
			fc.utobj().clickElement(driver, pobj.addTaskLink);
			fc.utobj().sleep(2000);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().sleep(2000);
			
			if (!fc.utobj().isSelected(driver, pobj.associatedWithLead)) {
				fc.utobj().clickElement(driver, pobj.associatedWithLead);
			}

			String leadName = firstName + " " + lastName;

			fc.utobj().sendKeys(driver, pobj.leadSearch, leadName);
			
			fc.utobj().sleep(2000);
			//fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//span/u[.='" + leadName + "']"));
			 JavascriptExecutor js = (JavascriptExecutor)driver;
			 WebElement button =driver.findElement(By.xpath(".//span/u[.='" + leadName + "']"));
			 js.executeScript("arguments[0].click();", button);
			
			
			if (!fc.utobj().isSelected(driver,pobj.radioOwner)) {
				fc.utobj().clickElement(driver, pobj.radioOwner);
			}
			String statusTask = dataSet.get("statusTask");
			fc.utobj().selectDropDown(driver, pobj.statusTask, statusTask);
			String subject = fc.utobj().generateTestData(dataSet.get("subject"));
			fc.utobj().sendKeys(driver, pobj.subject, subject);
			if (!fc.utobj().isSelected(driver, pobj.timeLessTask)) {
				fc.utobj().clickElement(driver, pobj.timeLessTask);
			}
			String priority = dataSet.get("priority");
			fc.utobj().selectDropDown(driver, pobj.priority, priority);

			String comments = "Test Comment";

			fc.utobj().sendKeys(driver, pobj.comments, comments);
			fc.utobj().clickElement(driver, pobj.createBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("View Task Detail");
			fc.utobj().sleep(2000);
			searchBySubjectFilter(driver, subject);
			fc.utobj().sleep(2000);
			fc.utobj().actionImgOption(driver, subject, "View");
			fc.utobj().sleep(2000);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().sleep(2000);
			fc.utobj().printTestStep("Complete Task Detail");
			fc.utobj().clickElement(driver, pobj.completeTask);

			fc.utobj().clickElement(driver, pobj.processTask);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Verify The Complete Task Detail");
			searchBySubjectFilter(driver, subject);

			boolean isSubjectPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[.='" + subject + "']");
			if (isSubjectPresent == false) {
				fc.utobj().throwsException("Was not able to verify Subject Of Task");
			}
			
			

			boolean isAssociatedWithPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[.='" + subject + "']/ancestor::tr/td/a[contains(text () ,'" + leadName + "')]");
			if (isAssociatedWithPresent == false) {
				fc.utobj().throwsException("was not able to verify Associated With");
			}

			boolean isTaskStatusPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[.='" + subject + "']/ancestor::tr/td[contains(text () ,'Completed')]");
			if (isTaskStatusPresent == false) {
				fc.utobj().throwsException("was not able to verify Status Of Task");
			}

			boolean isAssignedTo = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[.='" + subject
					+ "']/ancestor::tr/td[contains(text () ,'" + corpUser.getuserFullName() + "')]");
			if (isAssignedTo == false) {
				fc.utobj().throwsException("was not able to verify Assign To");
			}

			fc.utobj().printTestStep("Verify The Task Detail At Lead Info Page");

			// Verify At Lead Info Page
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,(".//a[.='" + subject + "']/ancestor::tr/td/a[contains(text () ,'" + leadName + "')]")));

			boolean isAssignedToAtLeadInfo = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//*[contains(text () ,'No records found.')]");
			if (isAssignedToAtLeadInfo == false) {
				fc.utobj().throwsException("Was not able to Verify The Completed Task At Lead Info Page");
			}

			fc.utobj().printTestStep("Verify The Task Completion Mail");
			Map<String, String> mailData = new HashMap<String, String>();
			mailData = fc.utobj().readMailBox(mailSubject, subject, emailId, "sdg@1a@Hfs");

			if (mailData.size() == 0 || !mailData.get("mailBody").contains(firstName)) {

				fc.utobj().throwsException("was not able to verify Lead In Task Completion Mail");
			}

			if (mailData.size() == 0 || !mailData.get("mailBody").contains(comments)) {

				fc.utobj().throwsException("was not able to verify Comment in Task Completion Mail");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	public void searchBySubjectFilter(WebDriver driver, String subject) throws Exception {
		CRMTasksPage pobj = new CRMTasksPage(driver);
		fc.utobj().sleep(3000);
		fc.utobj().clickElement(driver, pobj.showFilter);
		fc.utobj().sendKeys(driver, pobj.subjectFilter, subject);
		fc.utobj().setToDefault(driver, pobj.taskStatusFilter);
		fc.utobj().sendKeys(driver, pobj.startdateFilter, "");
		fc.utobj().sendKeys(driver, pobj.endDate, "");
		fc.utobj().selectDropDown(driver, pobj.reminderFilter, "Select");
		fc.utobj().setToDefault(driver, pobj.prioprityFilter);
		fc.utobj().setToDefault(driver, pobj.viewMineFilter);
		fc.utobj().setToDefault(driver, pobj.associatedWithFilter);
		fc.utobj().clickElement(driver, pobj.searchBtnFilter);
		fc.utobj().clickElement(driver, pobj.hideFilters);
	}
}
