package com.builds.test.crm;

import java.util.Map;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import com.builds.test.admin.AdminUsersManageCorporateUsersAddCorporateUserPageTest;
import com.builds.test.common.CorporateUser;
import com.builds.uimaps.crm.CRMAccountsPage;
import com.builds.uimaps.crm.CRMTransactionsPage;
import com.builds.utilities.FranconnectUtil;
import com.builds.utilities.TestCase;

public class CRMTransactionsPageTest {
	FranconnectUtil fc = new FranconnectUtil();

	@Test(groups = "crm")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2017-08-10", testCaseDescription = "Verify The Add Transaction At CRM Transaction Page", testCaseId = "TC_276_Add_Transaction")
	public void addNewTransaction() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			CRMAccountsPage pobj = new CRMAccountsPage(driver);
			CRMAccountsPageTest accountPage = new CRMAccountsPageTest();
			CRMContactsPageTest contactsPage = new CRMContactsPageTest();

			String description = fc.utobj().generateTestData(dataSet.get("description"));
			AdminCRMManageProductServiceCategoryPageTest serviceProductPage = new AdminCRMManageProductServiceCategoryPageTest();
			String categoryName = fc.utobj().generateTestData(dataSet.get("categoryName"));
			String productName = fc.utobj().generateTestData(dataSet.get("productName"));
			String oneLineDescription = fc.utobj().generateTestData(dataSet.get("oneLineDescription"));
			String rate = dataSet.get("rate");
			fc.utobj().printTestStep("Navigating to Admin > CRM >  Manage Product / Service & Category");

			fc.utobj().printTestStep("Add Category");

			serviceProductPage.addCategory(driver, categoryName, description);
			fc.utobj().printTestStep("Add Product/Service");
			serviceProductPage.addProductService(driver, categoryName, productName, oneLineDescription, rate);

			fc.utobj().printTestStep("Navigate To Admin Users Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			String emailId = "crmautomation@staffex.com";
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUser = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporateUser.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			AdminCRMContactTypeConfigurationPageTest contactTypePage = new AdminCRMContactTypeConfigurationPageTest();
			String contactType = fc.utobj().generateTestData(dataSet.get("contactType"));
			fc.utobj().printTestStep("Navigating to Admin > CRM > ContactType Configuration");
			fc.utobj().printTestStep("Add Contact Type");
			contactTypePage.addContactType(driver, contactType);

			AdminCRMConfigureOpportunityStagesPageTest stagePage = new AdminCRMConfigureOpportunityStagesPageTest();
			String stage = fc.utobj().generateTestData(dataSet.get("stage"));
			fc.utobj().printTestStep("Navigating to Admin > CRM >  Configure Opportunity Stages");
			fc.utobj().printTestStep("Add Stage");
			stagePage.addStage(driver, stage);

			AdminCRMConfigureIndustryPageTest industryPage = new AdminCRMConfigureIndustryPageTest();
			String industry = fc.utobj().generateTestData(dataSet.get("industry"));
			description = fc.utobj().generateTestData(dataSet.get("description"));
			fc.utobj().printTestStep("Navigating to Admin > CRM >  Configure Industry Type");
			fc.utobj().printTestStep("Add Industry");
			fc.utobj().printTestStep("Navigating to Admin > CRM >  Configure Industry Type");
			fc.utobj().printTestStep("Add Industry");
			industryPage.addIndustry(driver, industry, description);

			String accountName = fc.utobj().generateTestData(dataSet.get("accountName"));
			String visibility = dataSet.get("visibility");
			fc.utobj().printTestStep("Navigating to CRM >  Accounts > Accounts Summary");
			fc.utobj().printTestStep("Add Account");
			fc.utobj().printTestStep("Navigating to CRM >  Accounts > Accounts Summary");
			fc.utobj().printTestStep("Add Account");
			accountPage.addAcocunt(driver, industry, accountName, visibility);

			fc.utobj().printTestStep("Navigating to CRM >  Contacts > Contacts Summary");
			fc.utobj().printTestStep("Add Contact");

			fc.utobj().printTestStep("Navigating to CRM >  Contacts > Contacts Summary");
			fc.utobj().printTestStep("Add Contact");

			fc.utobj().clickElement(driver, pobj.contactsLinks);
			fc.utobj().clickElement(driver, pobj.addNew);
			fc.utobj().clickElement(driver, pobj.contactLnk);

			String cFname = fc.utobj().generateTestData(dataSet.get("cFname"));
			String cLname = fc.utobj().generateTestData(dataSet.get("cLname"));
			String primaryCMethod = dataSet.get("primaryCMethod");
			String assignTo = "Corporate";
			String regionName = "";
			String franchiseId = "";
			String title = dataSet.get("title");
			//String email = "crmautomation@staffex.com";
			String email=fc.utobj().generateTestData("crmautomation")+"@staffex.com";
			contactsPage.addContactGenericWithAccount(driver, contactType, cFname, cLname, primaryCMethod, assignTo,
					corpUser.getuserFullName(), regionName, franchiseId, title, email, config, accountName);

			// add Opportunity
			fc.utobj().printTestStep("Add Opportunity");
			fc.utobj().clickElement(driver, pobj.opportunitiesTab);
			fc.utobj().clickElement(driver, pobj.opportunityLink);

			String opportunityName = fc.utobj().generateTestData(dataSet.get("opportunityName"));
			String salesAmount = dataSet.get("salesAmount");

			fc.utobj().sendKeys(driver, pobj.opportunityName, opportunityName);
			fc.utobj().sendKeys(driver, pobj.opportunityOwner, corpUser.getuserFullName());
			fc.utobj().clickElement(driver, pobj.searchOppOwnerBtn);

			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//a[contains(text () , '" + corpUser.getuserFullName() + "')]"));
			fc.utobj().switchFrameToDefault(driver);
			fc.utobj().selectDropDown(driver, pobj.stage, stage);
			fc.utobj().sendKeys(driver, pobj.salesAmount, salesAmount);
			fc.utobj().clickElement(driver, pobj.saveBtn);

			fc.utobj().clickElement(driver, pobj.transactionLink);
			fc.utobj().printTestStep("Add Transaction");

			// add Transaction
			addTransaction(driver, opportunityName, productName, config);

			fc.utobj().printTestStep("Verify Transaction");
			/*
			 * boolean isProductServicePresent=fc.utobj().verifyCase(driver,
			 * ".//td[contains(text () , '"+productName+"')]"); if
			 * (isProductServicePresent==false) { fc.utobj().throwsException(
			 * "was not able to verify Transaction Name"); }
			 */
			//
			fc.utobj().clickElement(driver, pobj.transactionLink);
			transactionFilter(driver, corpUser.getuserFullName());

			boolean isOppPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td/a[contains(text () , '" + opportunityName + "')]");
			if (isOppPresent == false) {
				fc.utobj().throwsException("was not able to verify Transaction");
			}

			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,(".//td/a[contains(text () , '" + opportunityName + "')]/../preceding-sibling::td/a")));

			boolean isProductAvailable = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () ,'" + productName + "')]");
			if (isProductAvailable == false) {
				fc.utobj().throwsException("was not able to verify Product");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	public void addTransaction(WebDriver driver, String opportunityName, String productName, Map<String, String> config)
			throws Exception {

		String testCaseId = "TC_Add_Transaction_AT_Transaction_Page";

		if (fc.utobj().validate(testCaseId)) {
			try {
				CRMAccountsPage pobj = new CRMAccountsPage(driver);
				fc.utobj().clickElement(driver, pobj.addTransactionLink);
				fc.utobj().sendKeys(driver, pobj.opportunityName, opportunityName);

				fc.utobj().clickElement(driver, pobj.ajaxResult);
				fc.utobj().sendKeys(driver, pobj.invoiceDate, fc.utobj().getFutureDateUSFormat(-1));
				fc.utobj().clickElement(driver, pobj.selectProductAndService);
				fc.utobj().clickElement(driver, pobj.uncheckAll);
				fc.utobj().sendKeys(driver, pobj.searchProduct, productName);
				fc.utobj().clickElement(driver, pobj.checkAll);
				fc.utobj().clickElement(driver, pobj.okayBtn);
				fc.utobj().clickElement(driver, pobj.submitBtn);

				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException("was not able to Add Transaction :: CRM > Transaction Page");
		}
	}

	@Test(groups = {"crm"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify the Modify Transaction At CRM > Transactions Page", testCaseId = "TC_277_Modify_Transaction")
	public void modifyTransactionActionImage() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			CRMAccountsPage pobj = new CRMAccountsPage(driver);
			CRMAccountsPageTest accountPage = new CRMAccountsPageTest();
			CRMContactsPageTest contactsPage = new CRMContactsPageTest();

			String description = fc.utobj().generateTestData(dataSet.get("description"));
			AdminCRMManageProductServiceCategoryPageTest serviceProductPage = new AdminCRMManageProductServiceCategoryPageTest();
			String categoryName = fc.utobj().generateTestData(dataSet.get("categoryName"));
			String productName = fc.utobj().generateTestData(dataSet.get("productName"));
			String oneLineDescription = fc.utobj().generateTestData(dataSet.get("oneLineDescription"));
			String rate = dataSet.get("rate");
			fc.utobj().printTestStep("Navigating to Admin > CRM >  Manage Product / Service & Category");

			fc.utobj().printTestStep("Add Category");
			serviceProductPage.addCategory(driver, categoryName, description);
			serviceProductPage.addProductService(driver, categoryName, productName, oneLineDescription, rate);

			String categoryName1 = fc.utobj().generateTestData(dataSet.get("categoryName"));
			String productName1 = fc.utobj().generateTestData(dataSet.get("productName"));
			String oneLineDescription1 = fc.utobj().generateTestData(dataSet.get("oneLineDescription"));
			serviceProductPage.addCategory(driver, categoryName1, description);
			serviceProductPage.addProductService(driver, categoryName1, productName1, oneLineDescription1, rate);

			fc.utobj().printTestStep("Navigate To Admin Users Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			String emailId = "crmautomation@staffex.com";
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUser = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporateUser.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			fc.utobj().printTestStep("Navigating to Admin > CRM > ContactType Configuration");
			fc.utobj().printTestStep("Add Contact Type");
			AdminCRMContactTypeConfigurationPageTest contactTypePage = new AdminCRMContactTypeConfigurationPageTest();
			String contactType = fc.utobj().generateTestData(dataSet.get("contactType"));
			contactTypePage.addContactType(driver, contactType);
			fc.utobj().printTestStep("Navigating to Admin > CRM >  Configure Opportunity Stages");
			fc.utobj().printTestStep("Add Stage");
			AdminCRMConfigureOpportunityStagesPageTest stagePage = new AdminCRMConfigureOpportunityStagesPageTest();
			String stage = fc.utobj().generateTestData(dataSet.get("stage"));
			stagePage.addStage(driver, stage);

			AdminCRMConfigureIndustryPageTest industryPage = new AdminCRMConfigureIndustryPageTest();
			String industry = fc.utobj().generateTestData(dataSet.get("industry"));
			description = fc.utobj().generateTestData(dataSet.get("description"));
			fc.utobj().printTestStep("Navigating to Admin > CRM >  Configure Industry Type");
			fc.utobj().printTestStep("Add Industry");
			industryPage.addIndustry(driver, industry, description);

			String accountName = fc.utobj().generateTestData(dataSet.get("accountName"));
			String visibility = dataSet.get("visibility");
			fc.utobj().printTestStep("Navigating to CRM >  Accounts > Accounts Summary");
			fc.utobj().printTestStep("Add Account");
			accountPage.addAcocunt(driver, industry, accountName, visibility);

			fc.utobj().printTestStep("Navigating to CRM >  Contacts > Contacts Summary");
			fc.utobj().printTestStep("Add Contact");

			fc.utobj().clickElement(driver, pobj.contactsLinks);
			fc.utobj().clickElement(driver, pobj.addNew);
			fc.utobj().clickElement(driver, pobj.contactLnk);

			String cFname = fc.utobj().generateTestData(dataSet.get("cFname"));
			String cLname = fc.utobj().generateTestData(dataSet.get("cLname"));
			String primaryCMethod = dataSet.get("primaryCMethod");
			String assignTo = "Corporate";
			String regionName = "";
			String franchiseId = "";
			String title = dataSet.get("title");
			//String email = "crmautomation@staffex.com";
			String email=fc.utobj().generateTestData("crmautomation")+"@staffex.com";
			contactsPage.addContactGenericWithAccount(driver, contactType, cFname, cLname, primaryCMethod, assignTo,
					corpUser.getuserFullName(), regionName, franchiseId, title, email, config, accountName);

			// add Opportunity
			fc.utobj().printTestStep("Add Opportunity");
			fc.utobj().clickElement(driver, pobj.opportunitiesTab);
			fc.utobj().clickElement(driver, pobj.opportunityLink);

			String opportunityName = fc.utobj().generateTestData(dataSet.get("opportunityName"));
			String salesAmount = dataSet.get("salesAmount");

			fc.utobj().sendKeys(driver, pobj.opportunityName, opportunityName);
			fc.utobj().sendKeys(driver, pobj.opportunityOwner, corpUser.getuserFullName());
			fc.utobj().clickElement(driver, pobj.searchOppOwnerBtn);

			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//a[contains(text () , '" + corpUser.getuserFullName() + "')]"));
			fc.utobj().switchFrameToDefault(driver);
			fc.utobj().selectDropDown(driver, pobj.stage, stage);
			fc.utobj().sendKeys(driver, pobj.salesAmount, salesAmount);
			fc.utobj().clickElement(driver, pobj.saveBtn);

			// add Transaction
			fc.utobj().printTestStep("Add Transaction");
			fc.utobj().clickElement(driver, pobj.transactionLink);
			addTransaction(driver, opportunityName, productName1, config);

			fc.utobj().clickElement(driver, pobj.transactionLink);
			transactionFilter(driver, corpUser.getuserFullName());

			// modify Transaction
			fc.utobj().printTestStep("Modify Transaction");
			fc.utobj().actionImgOption(driver, opportunityName, "Modify");
			fc.utobj().clickElement(driver, pobj.selectProductAndService);
			fc.utobj().clickElement(driver, pobj.uncheckAll);
			fc.utobj().sendKeys(driver, pobj.searchProduct, productName1);
			fc.utobj().clickElement(driver, pobj.checkAll);
			fc.utobj().clickElement(driver, pobj.okayBtn);
			fc.utobj().clickElement(driver, pobj.submitBtn);

			fc.utobj().printTestStep("Verify Transaction");

			boolean isProductServicePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () , '" + productName1 + "')]");
			if (isProductServicePresent == false) {
				fc.utobj().throwsException("was not able to verify Transaction Name");
			}

			fc.utobj().clickElement(driver, pobj.transactionLink);
			transactionFilter(driver, corpUser.getuserFullName());

			boolean isOppPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td/a[contains(text () , '" + opportunityName + "')]");
			if (isOppPresent == false) {
				fc.utobj().throwsException("was not able to verify Transaction");
			}

			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,(".//td/a[contains(text () , '" + opportunityName + "')]/../preceding-sibling::td/a")));

			boolean isProductAvailable = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () ,'" + productName1 + "')]");
			if (isProductAvailable == false) {
				fc.utobj().throwsException("was not able to verify Product");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "crm", "checkcrmdel"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2017-07-04", testCaseDescription = "Verify the Delete Transaction At CRM > Transactions Page", testCaseId = "TC_278_Delete_Transaction")
	public void deleteTransactionActionImage() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		
		try {
			driver = fc.loginpage().login(driver);
			CRMAccountsPage pobj = new CRMAccountsPage(driver);
			CRMAccountsPageTest accountPage = new CRMAccountsPageTest();
			CRMContactsPageTest contactsPage = new CRMContactsPageTest();

			String description = fc.utobj().generateTestData(dataSet.get("description"));
			AdminCRMManageProductServiceCategoryPageTest serviceProductPage = new AdminCRMManageProductServiceCategoryPageTest();
			String categoryName = fc.utobj().generateTestData(dataSet.get("categoryName"));
			String productName = fc.utobj().generateTestData(dataSet.get("productName"));
			String oneLineDescription = fc.utobj().generateTestData(dataSet.get("oneLineDescription"));
			String rate = dataSet.get("rate");
			fc.utobj().printTestStep("Navigating to Admin > CRM >  Manage Product / Service & Category");
			fc.utobj().printTestStep("Add Category");
			serviceProductPage.addCategory(driver, categoryName, description);
			fc.utobj().printTestStep("Add Product/Service");
			serviceProductPage.addProductService(driver, categoryName, productName, oneLineDescription, rate);

			String categoryName1 = fc.utobj().generateTestData(dataSet.get("categoryName"));
			String productName1 = fc.utobj().generateTestData(dataSet.get("productName"));
			String oneLineDescription1 = fc.utobj().generateTestData(dataSet.get("oneLineDescription"));
			serviceProductPage.addCategory(driver, categoryName1, description);
			serviceProductPage.addProductService(driver, categoryName1, productName1, oneLineDescription1, rate);

			fc.utobj().printTestStep("Navigate To Admin Users Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			String emailId = "crmautomation@staffex.com";
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUser = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporateUser.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			fc.utobj().printTestStep("Navigating to Admin > CRM > ContactType Configuration");
			fc.utobj().printTestStep("Add Contact Type");
			AdminCRMContactTypeConfigurationPageTest contactTypePage = new AdminCRMContactTypeConfigurationPageTest();
			String contactType = fc.utobj().generateTestData(dataSet.get("contactType"));
			contactTypePage.addContactType(driver, contactType);

			fc.utobj().printTestStep("Navigating to Admin > CRM >  Configure Opportunity Stages");
			fc.utobj().printTestStep("Add Stage");
			AdminCRMConfigureOpportunityStagesPageTest stagePage = new AdminCRMConfigureOpportunityStagesPageTest();
			String stage = fc.utobj().generateTestData(dataSet.get("stage"));
			stagePage.addStage(driver, stage);

			AdminCRMConfigureIndustryPageTest industryPage = new AdminCRMConfigureIndustryPageTest();
			String industry = fc.utobj().generateTestData(dataSet.get("industry"));
			description = fc.utobj().generateTestData(dataSet.get("description"));
			fc.utobj().printTestStep("Navigating to Admin > CRM >  Configure Industry Type");
			fc.utobj().printTestStep("Add Industry");
			industryPage.addIndustry(driver, industry, description);

			String accountName = fc.utobj().generateTestData(dataSet.get("accountName"));
			String visibility = dataSet.get("visibility");
			fc.utobj().printTestStep("Navigating to CRM >  Accounts > Accounts Summary");
			fc.utobj().printTestStep("Add Account");
			accountPage.addAcocunt(driver, industry, accountName, visibility);

			fc.utobj().printTestStep("Navigating to CRM >  Contacts > Contacts Summary");
			fc.utobj().printTestStep("Add Contact");

			fc.utobj().clickElement(driver, pobj.contactsLinks);
			fc.utobj().clickElement(driver, pobj.addNew);
			fc.utobj().clickElement(driver, pobj.contactLnk);

			String cFname = fc.utobj().generateTestData(dataSet.get("cFname"));
			String cLname = fc.utobj().generateTestData(dataSet.get("cLname"));
			String primaryCMethod = dataSet.get("primaryCMethod");
			String assignTo = "Corporate";
			String regionName = "";
			String franchiseId = "";
			String title = dataSet.get("title");
			//String email = "crmautomation@staffex.com";
			String email=fc.utobj().generateTestData("crmautomation")+"@staffex.com";
			contactsPage.addContactGenericWithAccount(driver, contactType, cFname, cLname, primaryCMethod, assignTo,
					corpUser.getuserFullName(), regionName, franchiseId, title, email, config, accountName);

			// add Opportunity
			fc.utobj().printTestStep("Add Opportunity");
			fc.utobj().clickElement(driver, pobj.opportunitiesTab);
			fc.utobj().clickElement(driver, pobj.opportunityLink);

			String opportunityName = fc.utobj().generateTestData(dataSet.get("opportunityName"));
			String salesAmount = dataSet.get("salesAmount");

			fc.utobj().sendKeys(driver, pobj.opportunityName, opportunityName);
			fc.utobj().sendKeys(driver, pobj.opportunityOwner, corpUser.getuserFullName());
			fc.utobj().clickElement(driver, pobj.searchOppOwnerBtn);

			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//a[contains(text () , '" + corpUser.getuserFullName() + "')]"));
			fc.utobj().switchFrameToDefault(driver);
			fc.utobj().selectDropDown(driver, pobj.stage, stage);
			fc.utobj().sendKeys(driver, pobj.salesAmount, salesAmount);
			fc.utobj().clickElement(driver, pobj.saveBtn);

			// add Transaction
			fc.utobj().printTestStep("Add Transaction");
			fc.utobj().clickElement(driver, pobj.transactionLink);
			addTransaction(driver, opportunityName, productName1, config);

			fc.utobj().clickElement(driver, pobj.transactionLink);
			transactionFilter(driver, corpUser.getuserFullName());

			// Delete Transaction

			fc.utobj().printTestStep("Delete Transaction");
			fc.utobj().actionImgOption(driver, opportunityName, "Delete");
			fc.utobj().acceptAlertBox(driver);
			/*
			 * fc.commonMethods().switch_cboxIframe_frameId(driver);
			 * fc.utobj().clickElement(driver, pobj.closeBtn);
			 * fc.utobj().switchFrameToDefault(driver);
			 */

			fc.utobj().printTestStep("Verify Transaction");

			// boolean isNoRecordPresent = fc.utobj().verifyCase(driver,
			// ".//td[.='No records found.']");
			boolean isNoRecordPresent = fc.utobj().assertPageSource(driver, opportunityName);

			if (isNoRecordPresent == true) {
				fc.utobj().throwsException("was not able to Delete Transaction");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	public void transactionFilter(WebDriver driver, String userName) throws Exception {

		CRMTransactionsPage pobj = new CRMTransactionsPage(driver);
		fc.utobj().clickElement(driver, pobj.showFilter);
		fc.utobj().setToDefault(driver, pobj.opportunityOwner);
		WebElement elementInput = fc.utobj().getElement(driver, pobj.opportunityOwner)
				.findElement(By.xpath("./div/div/input"));
		fc.utobj().selectValFromMultiSelect(driver, pobj.opportunityOwner, elementInput, userName);
		fc.utobj().setToDefault(driver, pobj.franchiseIdSelect);
		fc.utobj().selectDropDown(driver, pobj.transactionDate, "All");
		fc.utobj().clickElement(driver, pobj.searchBtn);
		fc.utobj().clickElement(driver, pobj.hideFilter);
	}

}
