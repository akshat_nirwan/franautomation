package com.builds.test.crm;

import org.openqa.selenium.WebDriver;

import com.builds.uimaps.crm.CRMTasksPage;
import com.builds.utilities.FranconnectUtil;

public class CRM_AddTAskDetailsFill {

	FranconnectUtil fc = new FranconnectUtil();
	
	public CRM_AddTAskDetailsFill detailsoftask(WebDriver driver ,CRM_AddTaskgetsetTest fill) throws Exception
	{
		
		CRMTasksPage taskUI = new CRMTasksPage(driver);
		
		
		if(fill.getSubject()!=null)
		{

			fc.utobj().sendKeys(driver, taskUI.subject, fill.getSubject());
		}
		
		if(fill.getStatus()!=null)
		{

			fc.utobj().selectDropDown(driver, taskUI.statusTask, fill.getStatus());
		}
		
		if(!fc.utobj().isSelected(driver, taskUI.timeLessTask)) 
		{
		fc.utobj().clickElement(driver, taskUI.timeLessTask);
		}
		
		if(fill.getStartDAte()!=null)
		{

			fc.utobj().sendKeys(driver, taskUI.startdateFilter, fill.getStartDAte());
		}
		
		if(fill.getTimelesstask()!=null)
		{

			fc.utobj().sendKeys(driver, taskUI.timeLessTask, fill.getTimelesstask());
		}
		
		if(fill.getPriority()!=null)
		{

			fc.utobj().selectDropDown(driver, taskUI.priority, fill.getPriority());
		}
		
				
		if(fill.getComment()!=null)
		{

			fc.utobj().sendKeys(driver, taskUI.comments, fill.getComment());
		}
		
	
		
			
		return this ;
		
	}
	 public void clicksavebtn(WebDriver driver) throws Exception
	 {
		 CRMTasksPage taskUI = new CRMTasksPage(driver);
		 fc.utobj().clickElement(driver, taskUI.saveBtn);
		
	 }
	
     public void clickclosebtn(WebDriver driver) throws Exception 
     {
    	 CRMTasksPage taskUI = new CRMTasksPage(driver);
		 fc.utobj().clickElement(driver, taskUI.closebtn);
     }
	
     public void clickCreateAnother(WebDriver driver)throws Exception 
     {
    	 CRMTasksPage taskUI = new CRMTasksPage(driver);
		 fc.utobj().clickElement(driver, taskUI.Createanother); 
     }
}
