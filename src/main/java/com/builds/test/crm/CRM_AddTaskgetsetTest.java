package com.builds.test.crm;

public class CRM_AddTaskgetsetTest {

	private String status;
	private String Subject;
	private String Priority;
	private String StartDAte;
	private String EndTime;
	private String StartTime;
	private String Comment;
	private String AddtoCalender;
	private String Timelesstask;
	private String Save;
	private String close;
	private String Createanother;
	
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getSubject() {
		return Subject;
	}
	public void setSubject(String subject) {
		Subject = subject;
	}
	public String getPriority() {
		return Priority;
	}
	public void setPriority(String priority) {
		Priority = priority;
	}
	public String getStartDAte() {
		return StartDAte;
	}
	public void setStartDAte(String startDAte) {
		StartDAte = startDAte;
	}
	public String getEndTime() {
		return EndTime;
	}
	public void setEndTime(String endTime) {
		EndTime = endTime;
	}
	public String getStartTime() {
		return StartTime;
	}
	public void setStartTime(String startTime) {
		StartTime = startTime;
	}
	public String getComment() {
		return Comment;
	}
	public void setComment(String comment) {
		Comment = comment;
	}
	public String getAddtoCalender() {
		return AddtoCalender;
	}
	public void setAddtoCalender(String addtoCalender) {
		AddtoCalender = addtoCalender;
	}
	public String getTimelesstask() {
		return Timelesstask;
	}
	public void setTimelesstask(String timelesstask) {
		Timelesstask = timelesstask;
	}
	public String getSave() {
		return Save;
	}
	public void setSave(String save) {
		Save = save;
	}
	public String getClose() {
		return close;
	}
	public void setClose(String close) {
		this.close = close;
	}
	public String getCreateanother() {
		return Createanother;
	}
	public void setCreateanother(String createanother) {
		Createanother = createanother;
	}
	
	
	
	
	
}
