package com.builds.test.crm;

import org.openqa.selenium.WebDriver;

import com.builds.uimaps.crm.CRM_AssociateWithCampaignConfirmationPageUI;
import com.builds.utilities.FranconnectUtil;

public class CRM_AssociateWithCampaignConfirmationPage {

	FranconnectUtil fc = new FranconnectUtil();
	
	public void confirmbtn(WebDriver driver) throws Exception
	{
		CRM_AssociateWithCampaignConfirmationPageUI conUI = new CRM_AssociateWithCampaignConfirmationPageUI(driver);
		fc.utobj().printTestStep("click on confirm button");
		fc.utobj().clickElement(driver, conUI.Confirmbtn);
		
	}
	
	public void Cancelbtn(WebDriver driver) throws Exception
	{
		CRM_AssociateWithCampaignConfirmationPageUI conUI = new CRM_AssociateWithCampaignConfirmationPageUI(driver);
		fc.utobj().printTestStep("click on cancel button");
		fc.utobj().clickElement(driver, conUI.Cancelconfirm);
		
	}
	
	public void Backbtn(WebDriver driver) throws Exception
	{
		CRM_AssociateWithCampaignConfirmationPageUI conUI = new CRM_AssociateWithCampaignConfirmationPageUI(driver);
		fc.utobj().printTestStep("click on Back button");
		fc.utobj().clickElement(driver, conUI.Backconfirm);
		
	}
		
}
