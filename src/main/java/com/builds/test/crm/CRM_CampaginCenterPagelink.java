package com.builds.test.crm;

import org.openqa.selenium.WebDriver;

import com.builds.uimaps.crm.CRM_CampaignCenterUI;
import com.builds.utilities.FranconnectUtil;

public class CRM_CampaginCenterPagelink {
	
	FranconnectUtil fc = new FranconnectUtil();
	
	public void clickCreate(WebDriver driver) throws Exception
	{
		CRM_CampaignCenterUI centerUI = new CRM_CampaignCenterUI(driver);
		fc.utobj().printTestStep("Click Create");
		fc.utobj().clickElement(driver, centerUI.clickCreatebtn);
	}
	
	public void CreateCampaign(WebDriver driver) throws Exception
	{
		CRM_CampaignCenterUI centerUI = new CRM_CampaignCenterUI(driver);
		clickCreate(driver);
		fc.utobj().printTestStep("Click Create Campaign");
		fc.utobj().clickElement(driver, centerUI.CreateCampaign_Link);
		fc.utobj().printTestStep("Switch to Add campaign frame");
		Thread.sleep(2000);
		fc.utobj().switchFrame(driver, centerUI.CreateCampaign_frame);
	}
	
	public void CreateTemplate(WebDriver driver) throws Exception
	{
		CRM_CampaignCenterUI centerUI = new CRM_CampaignCenterUI(driver);
		clickCreate(driver);
		fc.utobj().printTestStep("Click Create Template");
		fc.utobj().clickElement(driver, centerUI.CreateTemplate_Link);
	}
	
	public void CreateRecipientGroup(WebDriver driver) throws Exception
	{
		CRM_CampaignCenterUI centerUI = new CRM_CampaignCenterUI(driver);
		clickCreate(driver);
		fc.utobj().printTestStep("Click Create RecipientsGroup");
		fc.utobj().clickElement(driver, centerUI.CreateRecipientsGroup_Link);
		fc.utobj().printTestStep("Switch to Add campaign frame");
		Thread.sleep(2000);
		fc.utobj().switchFrame(driver, centerUI.CreateCampaign_frame);
	}
	
	public void CreateWorkFlow(WebDriver driver) throws Exception
	{
		CRM_CampaignCenterUI centerUI = new CRM_CampaignCenterUI(driver);
		clickCreate(driver);
		fc.utobj().printTestStep("Click Create Workflow");
		fc.utobj().clickElement(driver, centerUI.CreateWorkflow_Link);
		fc.utobj().printTestStep("Switch to Add Workflow frame");
		Thread.sleep(2000);
		fc.utobj().switchFrame(driver, centerUI.CreateWorkflow_frame);
	}
	
	public void Dashboardsidebtn(WebDriver driver) throws Exception
	{
		CRM_CampaignCenterUI centerUI = new CRM_CampaignCenterUI(driver);
		fc.utobj().printTestStep("Click on Dashborad Three line button");
		fc.utobj().clickElement(driver, centerUI.DashboradSidelink);
	}
	
	public void Dashbordbtn(WebDriver driver) throws Exception
	{
		CRM_CampaignCenterUI centerUI = new CRM_CampaignCenterUI(driver);
		Dashboardsidebtn(driver);
		fc.utobj().printTestStep("Click on Dashborad in Three line button");
		fc.utobj().clickElement(driver, centerUI.Dashboard_link);
		Thread.sleep(2000);
	}
	
	public void Campaignbtn(WebDriver driver) throws Exception
	{
		CRM_CampaignCenterUI centerUI = new CRM_CampaignCenterUI(driver);
		Dashboardsidebtn(driver);
		fc.utobj().printTestStep("Click on campaign in Three line button");
		fc.utobj().clickElement(driver, centerUI.Campaign_Link);
		Thread.sleep(2000);
	}
	
	public void Templatebtn(WebDriver driver) throws Exception
	{
		CRM_CampaignCenterUI centerUI = new CRM_CampaignCenterUI(driver);
		Dashboardsidebtn(driver);
		fc.utobj().printTestStep("Click on Template in Three line button");
		fc.utobj().clickElement(driver, centerUI.Template_Link);
		Thread.sleep(2000);
	}
	
	public void ReciientGroupbtn(WebDriver driver) throws Exception
	{
		CRM_CampaignCenterUI centerUI = new CRM_CampaignCenterUI(driver);
		Dashboardsidebtn(driver);
		fc.utobj().printTestStep("Click on ReciientGroup in Three line button");
		fc.utobj().clickElement(driver, centerUI.RecipientGroup_Link);
		Thread.sleep(2000);
	}
	
	public void Workflowbtn(WebDriver driver) throws Exception
	{
		CRM_CampaignCenterUI centerUI = new CRM_CampaignCenterUI(driver);
		Dashboardsidebtn(driver);
		fc.utobj().printTestStep("Click on Workflow in Three line button");
		fc.utobj().clickElement(driver, centerUI.Workflow_Link);
		Thread.sleep(2000);
	}
	
	
	public void Mediafolderbtn(WebDriver driver) throws Exception
	{
		CRM_CampaignCenterUI centerUI = new CRM_CampaignCenterUI(driver);
		Dashboardsidebtn(driver);
		fc.utobj().printTestStep("Click on Mediafilder in Three line button");
		fc.utobj().clickElement(driver, centerUI.Media_Link);
		Thread.sleep(2000);
	}
	
	
	
	
	
}