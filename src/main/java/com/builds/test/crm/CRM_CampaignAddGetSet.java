package com.builds.test.crm;

public class CRM_CampaignAddGetSet {
	
	private String CampaignName;
	private String descriptionlink;
	private String description;
	private String Accessibility;
	private String PromotionalRadio;
	private String StatusDrivenRadio;
	private String Start;
	private String Cancel;
	
	
	public String getCampaignName() {
		return CampaignName;
	}
	public void setCampaignName(String campaignName) {
		CampaignName = campaignName;
	}
	public String getDescriptionlink() {
		return descriptionlink;
	}
	public void setDescriptionlink(String descriptionlink) {
		this.descriptionlink = descriptionlink;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getAccessibility() {
		return Accessibility;
	}
	public void setAccessibility(String accessibility) {
		Accessibility = accessibility;
	}
	public String getPromotionalRadio() {
		return PromotionalRadio;
	}
	public void setPromotionalRadio(String promotionalRadio) {
		PromotionalRadio = promotionalRadio;
	}
	public String getStatusDrivenRadio() {
		return StatusDrivenRadio;
	}
	public void setStatusDrivenRadio(String statusDrivenRadio) {
		StatusDrivenRadio = statusDrivenRadio;
	}
	public String getStart() {
		return Start;
	}
	public void setStart(String start) {
		Start = start;
	}
	public String getCancel() {
		return Cancel;
	}
	public void setCancel(String cancel) {
		Cancel = cancel;
	}
	
	
	
	
	
	

}
