package com.builds.test.crm;

import org.openqa.selenium.WebDriver;

import com.builds.uimaps.crm.CRM_CampaignAddUI;

import com.builds.utilities.FranconnectUtil;

public class CRM_CampaignAddpage {
    
	FranconnectUtil fc = new FranconnectUtil();
	
	public CRM_CampaignAddpage filldetails(WebDriver driver, CRM_CampaignAddGetSet camAdd) throws Exception
	{
		CRM_CampaignAddUI camui = new CRM_CampaignAddUI(driver);
		fc.utobj().sleep(1000);
		if(camAdd.getCampaignName()!= null)
		{
			fc.utobj().sendKeys(driver, camui.campaignName, camAdd.getCampaignName());
		}
		fc.utobj().clickElement(driver, camui.Descriptionlink);
		
		if(camAdd.getDescription()!=null)
		{
			fc.utobj().sendKeys(driver, camui.Description, camAdd.getDescription());
		}
		if(camAdd.getAccessibility()!=null)
		{
			fc.utobj().selectDropDown(driver, camui.Accessibility, camAdd.getAccessibility());
		}
		
		return this;
		
	}
	
	public void PromotionalRadioclick(WebDriver driver) throws Exception
	{
		CRM_CampaignAddUI camui = new CRM_CampaignAddUI(driver);
		fc.utobj().printTestStep("Click promotional Radio button");
		fc.utobj().clickElement(driver, camui.PromotionalRadio);
	}
	
	public void StatusDrivenRadioclick(WebDriver driver) throws Exception
	{
		CRM_CampaignAddUI camui = new CRM_CampaignAddUI(driver);
		fc.utobj().printTestStep("Click Status Driven Radio button");
		fc.utobj().clickElement(driver, camui.StatusDrivenRadio);
	}
	public void clickStartbtn(WebDriver driver) throws Exception
	{
		CRM_CampaignAddUI camui = new CRM_CampaignAddUI(driver);
		fc.utobj().printTestStep("Click Start button");
		fc.utobj().clickElement(driver, camui.Start);
	}
	
	public void Cancelbtnclick(WebDriver driver) throws Exception
	{
		CRM_CampaignAddUI camui = new CRM_CampaignAddUI(driver);
		fc.utobj().printTestStep("Click cancel button");
		fc.utobj().clickElement(driver, camui.Cancel);
	}
	
	public void Savebtnclick(WebDriver driver) throws Exception
	{
		CRM_CampaignAddUI camui = new CRM_CampaignAddUI(driver);
		fc.utobj().printTestStep("Click save button on modify page");
		fc.utobj().clickElement(driver, camui.Save);
	}
	public void closebtnclick(WebDriver driver) throws Exception
	{
		CRM_CampaignAddUI camui = new CRM_CampaignAddUI(driver);
		fc.utobj().printTestStep("Click close button on modify page");
		fc.utobj().clickElement(driver, camui.close);
	}
	

	
}
