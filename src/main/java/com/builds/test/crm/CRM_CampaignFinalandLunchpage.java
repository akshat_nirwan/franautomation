package com.builds.test.crm;

import org.openqa.selenium.WebDriver;

import com.builds.uimaps.crm.CRM_CampaignFinalandLunchUI;
import com.builds.utilities.FranconnectUtil;

public class CRM_CampaignFinalandLunchpage {

	FranconnectUtil fc = new FranconnectUtil();
	
	public void clickFinalUpperbtn(WebDriver driver) throws Exception
	{
		CRM_CampaignFinalandLunchUI flUI = new CRM_CampaignFinalandLunchUI(driver);
		fc.utobj().printTestStep("click on final upper button");
		fc.utobj().clickElement(driver, flUI.finalizeupperbtn);
	}
	
	public void clickFinalbottombtn(WebDriver driver) throws Exception
	{
		CRM_CampaignFinalandLunchUI flUI = new CRM_CampaignFinalandLunchUI(driver);
		fc.utobj().printTestStep("click on final bottom button");
		fc.utobj().clickElement(driver, flUI.finalizebottombtn);
	}
	
	public void Backbtn(WebDriver driver) throws Exception
	{
		CRM_CampaignFinalandLunchUI flUI = new CRM_CampaignFinalandLunchUI(driver);
		fc.utobj().printTestStep("click on Back button");
		fc.utobj().clickElement(driver, flUI.Backbottombtn);
	}
}
