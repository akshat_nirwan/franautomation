package com.builds.test.crm;

import org.openqa.selenium.WebDriver;

import com.builds.uimaps.crm.CRM_CampaignSelectRecipientUI;
import com.builds.utilities.FranconnectUtil;

public class CRM_CampaignSelectRecipientpage {
	
	FranconnectUtil fc = new FranconnectUtil();
	
	public void continuebtn(WebDriver driver) throws Exception
	{
		CRM_CampaignSelectRecipientUI SRUI = new CRM_CampaignSelectRecipientUI(driver);
		fc.utobj().printBugStatus("Click Continue button on select recipient page ");
		fc.utobj().clickElement(driver, SRUI.Continuebtn);
	}

	public void Backbtn(WebDriver driver) throws Exception
	{
		CRM_CampaignSelectRecipientUI SRUI = new CRM_CampaignSelectRecipientUI(driver);
		fc.utobj().printBugStatus("Click Cancel button on select recipient page ");
		fc.utobj().clickElement(driver, SRUI.Backbtn);
	}
	
	public void AssociateLater(WebDriver driver) throws Exception
	{
		CRM_CampaignSelectRecipientUI SRUI = new CRM_CampaignSelectRecipientUI(driver);
		fc.utobj().printBugStatus("Click Associate later button on select recipient page ");
		fc.utobj().clickElement(driver, SRUI.AssociateLater);
	}
	
}
