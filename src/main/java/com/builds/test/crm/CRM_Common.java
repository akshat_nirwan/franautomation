package com.builds.test.crm;

import java.util.List;
import java.util.Map;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.Reporter;

import com.builds.test.admin.AdminPageTest;
import com.builds.test.common.FCHomePageTest;
import com.builds.uimaps.admin.AdminPage;
import com.builds.uimaps.common.FCHomePage;
import com.builds.uimaps.crm.AdminCRMConfigureStatusPageUI;
import com.builds.uimaps.crm.LeadUI;
import com.builds.utilities.FranconnectUtil;

public class CRM_Common extends FranconnectUtil {

	FranconnectUtil fc = new FranconnectUtil();

	public void CRMCalendarLnk(WebDriver driver) throws Exception {
		Reporter.log("Navigating to CRM  > Calendar...");
		crm().crm_common().openCRMCalendarPage(home_page(), driver);
	}

	public void CRMGroupsLnk(WebDriver driver) throws Exception {
		Reporter.log("Navigating to CRM  > Groups.....");
		crm().crm_common().openCRMGroupssPage(home_page(), driver);
	}

	public void CRMContactsLnk(WebDriver driver) throws Exception {
		Reporter.log("Navigating to CRM  > Contacts...");
		crm().crm_common().openCRMContactsPage(home_page(), driver);
	}

	public void CRMOpportunitiesLnk(WebDriver driver) throws Exception {
		Reporter.log("Navigating to CRM  > Opportunities...");
		crm().crm_common().openCRMOpportunitiesPage(home_page(), driver);
	}

	public void CRMCampaignCenterLnk(WebDriver driver) throws Exception {
		Reporter.log("Navigating to CRM  > Campaign Center...");

		crm().crm_common().openCRMCampaignCenterPage(home_page(), driver);
	}

	public void CRMWorkflowsLnk(WebDriver driver) throws Exception {
		Reporter.log("Navigating to CRM  > Workflows...");

		crm().crm_common().openCRMWorkflowsPage(home_page(), driver);
	}

	public void CRMAccountsLnk(WebDriver driver) throws Exception {
		Reporter.log("Navigating to CRM  > Accounts...");

		crm().crm_common().openCRMAccountsPage(home_page(), driver);
	}

	public void CRMTasksLnk(WebDriver driver) throws Exception {
		Reporter.log("Navigating to CRM  > Tasks...");

		crm().crm_common().openCRMTasksPage(home_page(), driver);
	}

	public void CRMLeadsLnk(WebDriver driver) throws Exception {
		Reporter.log("Navigating to CRM  > Leads...");

		crm().crm_common().openCRMLeadsPage(home_page(), driver);
	}

	public void CRMHomeLnk(WebDriver driver) throws Exception {
		Reporter.log("Navigating to CRM  > Home...");

		crm().crm_common().openCRMHomePage(home_page(), driver);
	}

	public void CRMExportLnk(WebDriver driver) throws Exception {
		Reporter.log("Navigating to CRM  > Export...");

		crm().crm_common().openCRMExportPage(home_page(), driver);
	}

	public void CRMModule(WebDriver driver) throws Exception {
		Reporter.log("Navigating to CRM  Module...");
		crm().crm_common().openCRMModule(home_page(), driver);
	}

	// Anukaran
	public void CRMHomeModule(WebDriver driver) throws Exception {
		Reporter.log("Navigating to CRM  > Home...");

		crm().crm_common().openMarketingHomeModule(home_page(), driver);
	}

	/* Harish Dwivedi adminCRMConfigureLastContactedFieldLnk */

	public void adminCRMConfigureLastContactedFieldLnk(WebDriver driver) throws Exception {
		Reporter.log("Navigating to Admin > CRM > Last Contacted Field...");
		adminpage().adminPage(driver);
		AdminPageTest p2 = new AdminPageTest();
		p2.marketing(driver);
		p2.openAdminCRMCRMConfigureLastContactedFieldLnk(driver);
	}

	public void adminCRMManageWebFormGeneratorLnk(WebDriver driver) throws Exception {
		Reporter.log("Navigating to Admin > CRM >  Manage Web Form Generator...");
		adminpage().adminPage(driver);
		AdminPageTest p2 = new AdminPageTest();
		p2.marketing(driver);
		p2.openAdminCRMManageWebFormGeneratorLnk(driver);
	}

	public void adminCRMManageProductServiceCategoryLnk(WebDriver driver) throws Exception {
		Reporter.log("Navigating to Admin > CRM >  Manage Product / Service & Category...");
		adminpage().adminPage(driver);
		AdminPageTest p2 = new AdminPageTest();
		p2.marketing(driver);
		p2.openAdminCRMManageProductServiceCategoryLnk(driver);
	}

	public void adminCRMConfigureCampaignEmailCategoryLnk(WebDriver driver) throws Exception {
		Reporter.log("Navigating to Admin > CRM >  Configure Campaign Email Category...");
		adminpage().adminPage(driver);
		AdminPageTest p2 = new AdminPageTest();
		p2.marketing(driver);
		p2.openAdminCRMConfigureCampaignEmailCategoryLnk(driver);
	}

	public void adminCRMConfigureEmailContentLnk(WebDriver driver) throws Exception {
		Reporter.log("Navigating to Admin > CRM >  Configure Email Content...");
		adminpage().adminPage(driver);
		AdminPageTest p2 = new AdminPageTest();
		p2.marketing(driver);
		p2.openAdminCRMConfigureEmailContentLnk(driver);
	}

	public void adminCRMConfigureEmailContactOwnerLnk(WebDriver driver) throws Exception {
		Reporter.log("Navigating to Admin > CRM >  Configure Email Content...");
		adminpage().adminPage(driver);
		AdminPageTest p2 = new AdminPageTest();
		p2.marketing(driver);
		p2.openAdminCRMConfigureEmailContentSenttoContactOwner(driver);
	}

	public void adminCRMConfigureOpportunityStagesLnk(WebDriver driver) throws Exception {
		Reporter.log("Navigating to Admin > CRM >  Configure Opportunity Stages...");
		adminpage().adminPage(driver);
		AdminPageTest p2 = new AdminPageTest();
		p2.marketing(driver);
		p2.openAdminCRMConfigureOpportunityStagesLnk(driver);
	}

	public void adminCRMConfigureIndustryLnk(WebDriver driver) throws Exception {
		Reporter.log("Navigating to Admin > CRM > Configure Industry Type...");
		adminpage().adminPage(driver);
		AdminPageTest p2 = new AdminPageTest();
		p2.marketing(driver);
		p2.openAdminCRMConfigureIndustryLnk(driver);
	}

	public void adminCRMConfigureLeadTypeLnk(WebDriver driver) throws Exception {
		Reporter.log("Navigating to Admin > CRM > Configure Lead Type...");
		adminpage().adminPage(driver);
		AdminPageTest p2 = new AdminPageTest();
		p2.marketing(driver);
		p2.openAdminCRMConfigureLeadTypeLnk(driver);
	}

	public void adminCRMSourceSummaryLnk(WebDriver driver) throws Exception {
		Reporter.log("Navigating to Admin > CRM > Source Summary...");
		adminpage().adminPage(driver);
		AdminPageTest p2 = new AdminPageTest();
		p2.marketing(driver);
		p2.openAdminCRMSourceSummaryLnk(driver);
	}

	public void adminCRMConfigureMediumLnk(WebDriver driver) throws Exception {
		Reporter.log("Navigating to Admin > CRM > Configure Medium...");
		adminpage().adminPage(driver);
		AdminPageTest p2 = new AdminPageTest();
		p2.marketing(driver);
		p2.openAdminCRMConfigureMediumLnk(driver);
	}

	public void adminCRMConfigureStatusLnk(WebDriver driver) throws Exception {
		Reporter.log("Navigating to Admin > CRM > ConfigureStatus...");
		adminpage().adminPage(driver);
		AdminPageTest p2 = new AdminPageTest();
		p2.marketing(driver);
		p2.openAdminCRMConfigureStatusLnk(driver);
	}

	public void adminCRMContactTypeConfigurationLnk(WebDriver driver) throws Exception {
		Reporter.log("Navigating to Admin > CRM > ContactTypeConfiguration...");
		adminpage().adminPage(driver);
		AdminPageTest p2 = new AdminPageTest();
		p2.marketing(driver);
		p2.openAdminCRMContactTypeConfigurationLnk(driver);
	}

	// Anukaran Mishra
	public void adminCRMManageFormGeneratorLnk(WebDriver driver) throws Exception {
		Reporter.log("Navigating to Admin > CRM >  Manage Form Generator...");
		adminpage().adminPage(driver);
		AdminPageTest p2 = new AdminPageTest();
		p2.marketing(driver);
		p2.openAdminCRMManageFormGeneratorLnk(driver);
	}

	// Anukaran Mishra
	public void adminCRMAssignZipToFranUserLnk(WebDriver driver) throws Exception {
		Reporter.log("Navigating to Admin > CRM >  Assign Zip / Postal Codes to Franchise Users...");
		adminpage().adminPage(driver);
		AdminPageTest p2 = new AdminPageTest();
		p2.marketing(driver);
		p2.openAdminCRMAssignZip(driver);
	}

	// Anukaran
	public void openAdminSetupDefaultOwner(WebDriver driver) throws Exception {
		Reporter.log("Navigating to Admin > CRM >  Setup Default Owner...");
		adminpage().adminPage(driver);
		AdminPageTest p2 = new AdminPageTest();
		p2.marketing(driver);
		p2.openAdminCRMDefaultOwner(driver);
	}

	// Anukaran
	public void openAdminSetupDefaultContact(WebDriver driver) throws Exception {
		Reporter.log("Navigating to Admin > CRM >  Setup Default Contact...");
		adminpage().adminPage(driver);
		AdminPageTest p2 = new AdminPageTest();
		p2.marketing(driver);
		p2.openAdminCRMDefaultContact(driver);
	}

	public void openCRMCampaignCenterPage(FCHomePageTest fcHomePageTest, WebDriver driver) throws Exception {

		FCHomePage pobj = new FCHomePage(driver);
		FCHomePageTest obj = new FCHomePageTest();
		obj.openCRMModule(driver);
		fcHomePageTest.utobj().sleep(1000);
		fcHomePageTest.utobj().clickElement(driver, pobj.campaignCenterLink);
	}

	public void openCRMWorkflowsPage(FCHomePageTest fcHomePageTest, WebDriver driver) throws Exception {

		FCHomePage pobj = new FCHomePage(driver);
		FCHomePageTest obj = new FCHomePageTest();
		obj.openCRMModule(driver);
		fcHomePageTest.utobj().sleep(1000);
		fcHomePageTest.utobj().clickElement(driver, pobj.workflowsLink);
	}

	public void openCRMCalendarPage(FCHomePageTest fcHomePageTest, WebDriver driver) throws Exception {

		FCHomePage pobj = new FCHomePage(driver);
		FCHomePageTest obj = new FCHomePageTest();
		obj.openCRMModule(driver);
		fcHomePageTest.utobj().sleep(1000);
		fcHomePageTest.utobj().clickElement(driver, pobj.calendarLink);
	}

	public void openCRMGroupssPage(FCHomePageTest fcHomePageTest, WebDriver driver) throws Exception {

		FCHomePage pobj = new FCHomePage(driver);
		FCHomePageTest obj = new FCHomePageTest();
		obj.openCRMModule(driver);
		fcHomePageTest.utobj().sleep(1000);
		fcHomePageTest.utobj().clickElement(driver, pobj.groupsLink);
	}

	public void openCRMContactsPage(FCHomePageTest fcHomePageTest, WebDriver driver) throws Exception {

		FCHomePage pobj = new FCHomePage(driver);
		FCHomePageTest obj = new FCHomePageTest();
		obj.openCRMModule(driver);
		fcHomePageTest.utobj().sleep(1000);
		fcHomePageTest.utobj().clickElement(driver, pobj.contactsLink);
	}

	public void openCRMAccountsPage(FCHomePageTest fcHomePageTest, WebDriver driver) throws Exception {

		FCHomePage pobj = new FCHomePage(driver);
		FCHomePageTest obj = new FCHomePageTest();
		obj.openCRMModule(driver);
		fcHomePageTest.utobj().sleep(1000);
		fc.utobj().sleep(500);
		fcHomePageTest.utobj().clickElement(driver, pobj.accountsLink);
	}

	public void openCRMTasksPage(FCHomePageTest fcHomePageTest, WebDriver driver) throws Exception {

		FCHomePage pobj = new FCHomePage(driver);
		FCHomePageTest obj = new FCHomePageTest();
		obj.openCRMModule(driver);
		fcHomePageTest.utobj().sleep(3000);
		fcHomePageTest.utobj().clickElement(driver, pobj.tasksLink);
	}

	public void openCRMOpportunitiesPage(FCHomePageTest fcHomePageTest, WebDriver driver) throws Exception {

		FCHomePage pobj = new FCHomePage(driver);
		FCHomePageTest obj = new FCHomePageTest();
		obj.openCRMModule(driver);
		fcHomePageTest.utobj().sleep(1000);
		fcHomePageTest.utobj().clickElement(driver, pobj.opportunitiesLink);
	}

	public LeadSummaryPage openCRMLeadsPage(FCHomePageTest fcHomePageTest, WebDriver driver) throws Exception {

		FCHomePage pobj = new FCHomePage(driver);
		FCHomePageTest obj = new FCHomePageTest();
		obj.openCRMModule(driver);
		
		fcHomePageTest.utobj().sleep(1000);
		fcHomePageTest.utobj().clickElement(driver, pobj.leadsLink);

		return new LeadSummaryPage();
	}

	public void openCRMHomePage(FCHomePageTest fcHomePageTest, WebDriver driver) throws Exception {

		FCHomePage pobj = new FCHomePage(driver);
		FCHomePageTest obj = new FCHomePageTest();
		obj.openCRMModule(driver);
		fcHomePageTest.utobj().sleep(1000);
		fcHomePageTest.utobj().clickElement(driver, pobj.homeLnk);
	}

	public void openCRMExportPage(FCHomePageTest fcHomePageTest, WebDriver driver) throws Exception {

		FCHomePage pobj = new FCHomePage(driver);
		FCHomePageTest obj = new FCHomePageTest();
		obj.openCRMModule(driver);
		fcHomePageTest.utobj().sleep(1000);
		fcHomePageTest.utobj().clickElement(driver, pobj.exportLink);
	}

	public FCHomePageTest openCRMModule(FCHomePageTest fcHomePageTest, WebDriver driver) throws Exception {

		/*FCHomePage pobj = new FCHomePage(driver);
		fcHomePageTest.utobj().clickElement(driver, pobj.productLink);
		fcHomePageTest.utobj().clickElement(driver, pobj.crmLink);*/
		
		FCHomePage pobj = new FCHomePage(driver);
		boolean isModuleClickable=false;
		
		//New Change CRM : Navigation : 11:01:2018
		if (pobj.adminGearIcon.size() > 0) {
			//Click over marketing module
			try {
				
				if (utobj().isElementPresent(driver, ".//li[contains(@class ,'leftnav-first-li mktOn') and not(contains(@class ,'leftnav-first-li mktOn active'))]")) {
					utobj().clickElement(driver, driver.findElement(By.id("mktSubmenu")));
				}
				List<WebElement> listOfModules3 = driver
						.findElements(By.xpath(".//li[contains(@class ,'leftnav-first-li mktOn')]/ul/li"));
				for (WebElement w : listOfModules3) {
					if (w.getAttribute("id").contentEquals("module_cm")) {
						utobj().clickElement(driver, w);
						isModuleClickable=true;
					}
				}
			} catch (Exception e) {
				// TODO: handle exception
			}
			if (!isModuleClickable) {
				utobj().throwsException("Unable to Click on CRM module");
			}
		}else {
			try {
				WebElement marketinglabel = driver.findElement(By.xpath(".//a[@class = 'marketinglabel']"));
				marketinglabel.click();
				Thread.sleep(1000);
				WebElement marketinglist = driver.findElement(By.id("moreMenu-options2"));
				Thread.sleep(1000);
				List<WebElement> marketing_list = marketinglist.findElements(By.tagName("li"));

				boolean isCRMClicked = false;
				if (marketing_list.size() > 0) {
					for (WebElement SelectLi : marketing_list) {
						if (SelectLi.getText().equals("CRM")) {
							SelectLi.click();
							isCRMClicked = true;
							break;
						}
					}
				}
				if (isCRMClicked == false) {
					WebElement crm = driver.findElement(By.xpath(".//a[contains(text(),'CRM')]"));
					crm.click();
				}
			} catch (Exception e) {
				utobj().clickElement(driver, pobj.crmTab);
			}
		}
		
		utobj().printTestStep("Navigating to CRM module");
		return new FCHomePageTest();
	}

	// Anukaran
	public void openMarketingHomeModule(FCHomePageTest fcHomePageTest, WebDriver driver) throws Exception {

		FCHomePage pobj = new FCHomePage(driver);
		fcHomePageTest.utobj().clickElement(driver, pobj.productLink);
		fcHomePageTest.utobj().clickElement(driver, pobj.HomeLink);
	}

	public void openAdminCRMManageFormGeneratorLnk(WebDriver driver) throws Exception {
		Reporter.log("Navigating to openAdminCRMManageFormGeneratorLnk...");

		AdminPage pobj = new AdminPage(driver);
		utobj().clickElement(driver, pobj.infoMgrManageWebFormGeneratorLink);
	}

	public void openCRMImportPage(FCHomePageTest fcHomePageTest, WebDriver driver) throws Exception {

		FCHomePage pobj = new FCHomePage(driver);
		FCHomePageTest obj = new FCHomePageTest();
		obj.openCRMModule(driver);
		fcHomePageTest.utobj().sleep(1000);
		fcHomePageTest.utobj().clickElement(driver, pobj.importpage);

	}

	public void CRMImporttLnk(WebDriver driver) throws Exception {
		Reporter.log("Navigating to CRM  > Import...");

		crm().crm_common().openCRMImportPage(home_page(), driver);
	}
	
	public void openCRMsearchpage(FCHomePageTest fcHomePageTest, WebDriver driver) throws Exception
	{
		//vipin
		FCHomePage pobj = new FCHomePage(driver);
		FCHomePageTest obj = new FCHomePageTest();
		obj.openCRMModule(driver);
		fcHomePageTest.utobj().sleep(1000);
		fcHomePageTest.utobj().clickElement(driver, pobj.crmSearch);
	}

	public Leadgetset getDefaultDataForLead(Leadgetset lead) throws InterruptedException {
		// vipin
		lead.setTitle("Mr.");
		lead.setFirstname(fc.utobj().generateTestData("Test"));
		lead.setLastname(fc.utobj().generateTestData("Lead"));
		lead.setPrimarycontact("Email");
		lead.setCompnayname(fc.utobj().generateTestData("franconnect"));
		lead.setAddress(fc.utobj().generateTestData("Noida sector 8 c-94"));
		lead.setCity(fc.utobj().generateTestData("Delhi"));
		lead.setEmail(fc.utobj().generateTestData("Seleniumvip")+"@gmail.com");
		lead.setAlternarteemail("Alternate@gmail.com");
		lead.setPhonenumber(fc.utobj().generateRandomNumber());
		lead.setExtension(fc.utobj().generateRandomNumber6Digit());
		lead.setFax(fc.utobj().generateRandomNumber());
		lead.setMobile(fc.utobj().generateRandomNumber());
		lead.setCountry("USA");
		lead.setState("Virginia");
		lead.setAssign("C");
		lead.setOwner("FranConnect Administrator");
		lead.setSuffix(fc.utobj().generateTestData("Test"));
		lead.setJobtitle(fc.utobj().generateTestData("Owner"));
		lead.setBirthday(fc.utobj().generateCurrentDatewithformat("05/04/1975"));
		lead.setAnniversary(fc.utobj().generateCurrentDatewithformat("05/04/1990"));
		lead.setRating("Hot");
		lead.setSource("Import");
		lead.setSource1("Import");
		lead.setContactMedium("Email");
		lead.setBesttime(fc.utobj().generateTestData("Anytime"));
		lead.setComment(fc.utobj().generateTestData("commnet by selenium"));
		lead.setZipcode(fc.utobj().generateRandomNumber6Digit());
		
		
		
		lead.setWeb_Title("Mr.");
		lead.setWeb_firstname(fc.utobj().generateTestData("Rest"));
		lead.setWeb_lastname(fc.utobj().generateTestData("API"));
		lead.setWeb_Primarycontact("Email");
		lead.setWeb_companyname(fc.utobj().generateTestData("franconnect"));
		lead.setWeb_Adress(fc.utobj().generateTestData("noida sector 8"));
		lead.setWeb_city("delhi");
		lead.setWeb_country("India");
		lead.setWeb_State("Delhi");
		lead.setWeb_Email("RestAPI@gmail.com");
		lead.setWeb_alternateEmail("restalternate@gmail.com");
		lead.setWeb_phone(fc.utobj().generateRandomNumber());
		lead.setWeb_phoneExt(fc.utobj().generateRandomNumber());
		lead.setWeb_fax(fc.utobj().generateRandomNumber());
		lead.setWeb_zipcode(fc.utobj().generateRandomNumber());
		lead.setWeb_Suffix("API");
		lead.setWeb_jobtitle("Owner");
		lead.setWeb_rating("Hot");
		lead.setWeb_source("import");
		lead.setWeb_source1("import");
		lead.setWebservice_AssignTo("CORP");
	    lead.setWebService_LeadOwner("FranConnect Administrator");
	    lead.setWeb_besttime("Anytime");
	    
		return lead;
	}
	
	//VipinG
	public Leadgetset getDefaultDataForLeadwithSqlite(Leadgetset lead) throws Exception {
		
		String testCaseId="Add_Lead_CRM_DashBoard";
		String area="Lead_Visibility";
		Map<String, String> dataSet = fc.utobj().readTestDatawithsqllite("crm", testCaseId, area);
		
		lead.setTitle(dataSet.get("title"));
		lead.setFirstname(fc.utobj().generateTestData(dataSet.get("firstName")));
		lead.setLastname(fc.utobj().generateTestData(dataSet.get("lastName")));
		lead.setPrimarycontact(dataSet.get("primaryContactMethod"));
		lead.setCompnayname(fc.utobj().generateTestData(dataSet.get("company")));
		lead.setAddress(fc.utobj().generateTestData(dataSet.get("address")));
		lead.setCity(fc.utobj().generateTestData(dataSet.get("city")));
		lead.setEmail(fc.utobj().generateTestData(dataSet.get("emailIds"))+"@gmail.com");
		lead.setAlternarteemail(dataSet.get("alternateEmail"));
		lead.setPhonenumber(dataSet.get("phoneNumbers"));
		lead.setExtension(dataSet.get("extn"));
		lead.setFax(dataSet.get("faxNumbers"));
		lead.setMobile(dataSet.get("mobileNumbers"));
		lead.setCountry(dataSet.get("country"));
		lead.setState(dataSet.get("state"));
		lead.setAssign(dataSet.get("Assignadd"));
		lead.setOwner(dataSet.get("corpusername"));
		lead.setSuffix(dataSet.get("Suffixadd"));
		lead.setJobtitle(dataSet.get("JobTtileadd"));
		lead.setBirthday(dataSet.get("BirthDateadd"));
		lead.setAnniversary(dataSet.get("Anniversaydateadd"));
		lead.setRating(dataSet.get("rating"));
		lead.setSource(dataSet.get("Sourceadd"));
		lead.setSource1(dataSet.get("Sourceadd"));
		lead.setContactMedium(dataSet.get("contactMedium"));
		lead.setBesttime(fc.utobj().generateTestData("Anytime"));
		lead.setComment(dataSet.get("comments"));
		lead.setZipcode(dataSet.get("zipcode"));
		
		
		
		lead.setWeb_Title(dataSet.get("title"));
		lead.setWeb_firstname(fc.utobj().generateTestData(dataSet.get("FirstNameRest")));
		lead.setWeb_lastname(fc.utobj().generateTestData(dataSet.get("LastNameRest")));
		lead.setWeb_Primarycontact(dataSet.get("primaryContactMethod"));
		lead.setWeb_companyname(fc.utobj().generateTestData(dataSet.get("company")));
		lead.setWeb_Adress(fc.utobj().generateTestData(dataSet.get("address")));
		lead.setWeb_city(fc.utobj().generateTestData(dataSet.get("city")));
		lead.setWeb_country(dataSet.get("country"));
		lead.setWeb_State(dataSet.get("state"));
		lead.setWeb_Email(fc.utobj().generateTestData(dataSet.get("emailIds"))+"@gmail.com");
		lead.setWeb_alternateEmail(dataSet.get("alternateEmail"));
		lead.setWeb_phone(dataSet.get("phoneNumbers"));
		lead.setWeb_phoneExt(dataSet.get("extn"));
		lead.setWeb_fax(dataSet.get("faxNumbers"));
		lead.setWeb_zipcode(dataSet.get("zipcode"));
		lead.setWeb_Suffix(dataSet.get("Suffixadd"));
		lead.setWeb_jobtitle(dataSet.get("JobTtileadd"));
		lead.setWeb_rating(dataSet.get("rating"));
		lead.setWeb_source(dataSet.get("Sourceadd"));
		lead.setWeb_source1(dataSet.get("Sourceadd"));
		lead.setWebservice_AssignTo(dataSet.get("AssignRest"));
	    lead.setWebService_LeadOwner(dataSet.get("corpusername"));
	    lead.setWeb_besttime(dataSet.get("besttime"));
	    
		return lead;
	}

	public CRM_LogaCallgetsetTest defualtdataForCallRemark(CRM_LogaCallgetsetTest call) throws InterruptedException {
		//vipin
		call.setSubject(fc.utobj().generateTestData("subject"));
		call.setCallTime(fc.utobj().getCurrentDateAndTime());
		call.setStatus("Busy");
		call.setType("Inbound Call");
		call.setComment(fc.utobj().generateTestData("log a call comment"));
		
		return call;
	}
	
	public CRM_AddRemarkgetsetTest defualtaddreamrk(CRM_AddRemarkgetsetTest re) throws InterruptedException
	{
		re.setRemarkcomment(fc.utobj().generateTestData("remark"));
		return re;
	}

     
	public CRM_AddTaskgetsetTest defualtTaskdata(CRM_AddTaskgetsetTest fill) throws InterruptedException
	{
		fill.setStatus("Not Started");
		fill.setSubject(fc.utobj().generateTestData("task selenium"));
		fill.setStartDAte(fc.utobj().getCurrentDateUSFormat());
		fill.setComment(fc.utobj().generateTestData("this task is added from the automation "));		
		
		return fill;
	}
    
	public CRM_SendEmailGetSet defualtemailfill(CRM_SendEmailGetSet Semail) throws InterruptedException
	{
		Semail.setSubject(fc.utobj().generateTestData("emialselenium"));
		Semail.setCC("vipin.sain@franconnect.com");
		Semail.setTextfield(fc.utobj().generateTestData("email content from selenium "));
		
		return Semail;
	}
	
	public CRM_CampaignAddGetSet defualtCampaignfill(CRM_CampaignAddGetSet camAdd) throws InterruptedException
	{
		camAdd.setCampaignName(fc.utobj().generateTestData("SeleniumVip"));
		camAdd.setDescription(fc.utobj().generateTestData("Campaign description vip"));
		camAdd.setAccessibility("Public to all Users");
		return camAdd;
	}
	
	public CRM_TemplateInfoPageGetSet defualtTemplateinfofill(CRM_TemplateInfoPageGetSet teminfo) throws InterruptedException
	{
		teminfo.setTemaplateName(fc.utobj().generateTestData("TemInfo Vip"));
		teminfo.setTemaplatesubject(fc.utobj().generateTestData("TemSubject"));
		teminfo.setTempAccess("Public to all Users");
		return teminfo;
	}
	
	public CRM_CreateGroupGetSet DefualtGroupinfo(CRM_CreateGroupGetSet ginfo) throws InterruptedException
	{
		ginfo.setGroupName(fc.utobj().generateTestData("Groupvip"));
		ginfo.setGroupType("Lead");
		ginfo.setDescription(fc.utobj().generateTestData("Desccription"));
		ginfo.setAccessibility("Public to all Users");
		return ginfo;
	}
	
	public CRM_CreateGroupGetSetSQLite DefualtGroupinfoSQLite(CRM_CreateGroupGetSetSQLite ginfo1, Map<String, String> dataSet) throws InterruptedException
	{
		ginfo1.setGroupName(fc.utobj().generateTestData(dataSet.get("GroupName")));
		ginfo1.setGroupType("Lead");
		ginfo1.setDescription(fc.utobj().generateTestData(dataSet.get("GroupDescription")));
		ginfo1.setAccessibility("Public to all Users");
		return ginfo1;
	}
	
	public void clickOn_taskOption_onleadinfopage(WebDriver driver,String subject,String option) throws Exception // Subject = Task subject , option = which option use want to click like modify/delete/process
	{
		
		WebElement imgbtn = driver.findElement(By.xpath("//a[contains(text(),'"+subject+"')]/ancestor::td[1]/following-sibling::td[4]/layer/a/img"));
		fc.utobj().clickElement(driver, imgbtn);;
		Thread.sleep(200);
		WebElement del = driver.findElement(By.xpath(".//div[contains(text(),'"+option+"')]"));
		fc.utobj().clickElement(driver, del);;
	}
	
	public void search(WebDriver driver , String value) throws Exception // value = which lead/contact etc you want to search provide that name
	{
		fc.utobj().printBugStatus("Search value");
		WebElement search_bar = driver.findElement(By.id("searchSystem"));
		fc.utobj().sendKeys(driver, search_bar, value);
		WebElement click_searchbtn = driver.findElement(By.id("systemSearchBtn"));
		fc.utobj().clickElement(driver, click_searchbtn);
	}
	
	public CRM_OpportunityGetSet Defualtopportunity(CRM_OpportunityGetSet opp) throws InterruptedException
	{
		opp.setOpportunityName(fc.utobj().generateTestData("opportunity"));
		opp.setAccountName("");
		opp.setContactName("");
		opp.setAssignTo("");
		opp.setStage("Prospecting");
		opp.setOpportunityType("New Business");
		opp.setProbability("50");
		opp.setOpportunity_Source("Web");
		opp.setExpected_Closure_Date(fc.utobj().generateCurrentDate());
		opp.setSales_Amount("100");
		return opp;
	}
	
	public void AdminCRMStatus_change(WebDriver driver,String StatusName,String Statusfor) throws Exception  //Status for = Leads/Contacts
	{
    	fc.crm().crm_common().adminCRMConfigureStatusLnk(driver);
    	AdminCRMConfigureStatusPageUI npobj = new AdminCRMConfigureStatusPageUI(driver);
    	 if(Statusfor.equalsIgnoreCase("Leads")) {
    		 fc.utobj().printTestStep("click on lead tab");
    		 fc.utobj().clickElement(driver, npobj.leads);
    	 }
    	if(Statusfor.equalsIgnoreCase("Contacts")) {
    		fc.utobj().printTestStep("click on contact tab");
    		fc.utobj().clickElement(driver, npobj.contacts);
    	}
		fc.utobj().clickElement(driver, npobj.addStatus);
    	
		fc.commonMethods().switch_cboxIframe_frameId(driver);
		fc.utobj().sendKeys(driver, npobj.statusTxBx, StatusName);
		fc.utobj().clickElement(driver, npobj.addBtn);
		fc.utobj().printTestStep("Status created");	
}
	
	public void AdminPage(WebDriver driver) throws Exception
	{
		fc.utobj().printTestStep("Move to admin page");
		WebElement admin = driver.findElement(By.xpath("//span[@class='selecter-selected bText12']//a[@href='javascript:void(0);']"));	
		fc.utobj().clickElement(driver, admin);
		Thread.sleep(500);
		WebElement adminbtn = driver.findElement(By.xpath("//a[@class='bText12'][contains(text(),'Admin')]"));
		fc.utobj().clickElement(driver, adminbtn);
	}
	
	
	public void MultiSelect_DropDown(WebDriver driver,String value) throws Exception
	{
		fc.utobj().printTestStep("click on multiselect_dropdown option");
		LeadUI leadui = new LeadUI(driver);
		fc.utobj().clickElement(driver, "//span[@class='placeholder']");
		fc.utobj().printTestStep("search value name");
		fc.utobj().sendKeys(driver, leadui.Searchtab_insideGroup, value);
		Thread.sleep(500);
		fc.utobj().clickElement(driver, leadui.Searchbtn_insideGroup);
		Thread.sleep(1000);
		fc.utobj().clickElement(driver, ".//input[@id='selectAll']");
		
	}
	
	//VipinG
	public void CRMReportsLnk(WebDriver driver) throws Exception {
		Reporter.log("Navigating to CRM  > Reports");
		crm().crm_common().openCRMReportsPage(home_page(), driver);
	}
	
	public void openCRMReportsPage(FCHomePageTest fcHomePageTest, WebDriver driver) throws Exception {
		FCHomePage pobj = new FCHomePage(driver);
		FCHomePageTest obj = new FCHomePageTest();
		obj.openCRMModule(driver);
		fcHomePageTest.utobj().sleep(1000);
		fcHomePageTest.utobj().clickElement(driver, pobj.reportLink);
	}
}
