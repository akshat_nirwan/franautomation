package com.builds.test.crm;

public class CRM_CreateGroupGetSetSQLite {

	private String GroupName;
	private String GroupType;
	private String Accessibility;
	private String Description;
	
	
	
	public String getGroupName() {
		return GroupName;
	}
	public void setGroupName(String groupName) {
		GroupName = groupName;
	}
	public String getGroupType() {
		return GroupType;
	}
	public void setGroupType(String groupType) {
		GroupType = groupType;
	}
	public String getAccessibility() {
		return Accessibility;
	}
	public void setAccessibility(String accessibility) {
		Accessibility = accessibility;
	}
	public String getDescription() {
		return Description;
	}
	public void setDescription(String description) {
		Description = description;
	}
	
	
	
}
