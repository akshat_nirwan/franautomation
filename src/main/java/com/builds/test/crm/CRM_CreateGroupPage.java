package com.builds.test.crm;

import org.openqa.selenium.WebDriver;

import com.builds.uimaps.crm.CRMGroupsPage;
import com.builds.uimaps.crm.CRM_CreateGroupPageUI;
import com.builds.utilities.FranconnectUtil;

public class CRM_CreateGroupPage {
	
	FranconnectUtil fc = new FranconnectUtil();
	
	public CRM_CreateGroupPage fillGroup(WebDriver driver, CRM_CreateGroupGetSet ginfo) throws Exception
	{
		CRM_CreateGroupPageUI gUI = new CRM_CreateGroupPageUI(driver);
		fc.utobj().printTestStep("Info Add Group");
		fc.utobj().switchFrameByClass(driver, "newLayoutcboxIframe");
		fc.utobj().sleep(3000);
		if (ginfo.getGroupName()!=null)
		{
			fc.utobj().sendKeys(driver, gUI.GroupName, ginfo.getGroupName());
		}
		
		/*fc.utobj().clickElement(driver, gUI.GroupDescriptionLink);
		if(ginfo.getDescription()!=null)
		{
			fc.utobj().sendKeys(driver, gUI.Description, ginfo.getDescription());
		}*/
		
		if(ginfo.getGroupType()!=null)
		{
			fc.utobj().selectDropDown(driver, gUI.GroupType, ginfo.getGroupType());
		}
		
		if(ginfo.getAccessibility()!=null)
		{
		fc.utobj().selectDropDown(driver, gUI.GroupAccess, ginfo.getAccessibility());
		}

		return this;
		
	}
	
	public void descriptionbtn(WebDriver driver) throws Exception
	{
		CRM_CreateGroupPageUI gUI = new CRM_CreateGroupPageUI(driver);
		fc.utobj().printTestStep("Click on decription button");
		fc.utobj().clickElement(driver, gUI.GroupDescriptionLink);
	}

	public void Cancelbtn(WebDriver driver) throws Exception
	{
		CRM_CreateGroupPageUI gUI = new CRM_CreateGroupPageUI(driver);
		fc.utobj().printTestStep("Click on cancel button");
		fc.utobj().clickElement(driver, gUI.cancelbtn);
	}
	
	public void savebtn(WebDriver driver) throws Exception
	{
		CRM_CreateGroupPageUI gUI = new CRM_CreateGroupPageUI(driver);
		fc.utobj().printTestStep("Click on save button");
		fc.utobj().clickElement(driver, gUI.savebtn);
	}
	public void associatebtn_AddGroup(WebDriver driver) throws Exception
	{
		CRM_CreateGroupPageUI gUI = new CRM_CreateGroupPageUI(driver);
		fc.utobj().printTestStep("Click on Associate button");
		fc.utobj().clickElement(driver, gUI.Associatebtn_AddGroup);
	}
	public void Create_groupbtn(WebDriver driver) throws Exception
	{
		CRMGroupsPage GUI = new CRMGroupsPage(driver);
		fc.utobj().printTestStep("click on Create Group Button");
		fc.utobj().clickElement(driver, GUI.createGroupBtn);
	}
	
}
