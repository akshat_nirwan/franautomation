package com.builds.test.crm;

import org.openqa.selenium.WebDriver;

import com.builds.uimaps.crm.CRM_CreateGroupPageUI;
import com.builds.utilities.FranconnectUtil;

public class CRM_CreateGroupPageSQLite {
	
FranconnectUtil fc = new FranconnectUtil();
	
	public CRM_CreateGroupPageSQLite fillGroupfromSQLite(WebDriver driver, CRM_CreateGroupGetSetSQLite ginfo1) throws Exception
	{
		
		
		CRM_CreateGroupPageUI gUI = new CRM_CreateGroupPageUI(driver);
		fc.utobj().printTestStep("Info Add Group");
		fc.utobj().switchFrameByClass(driver, "newLayoutcboxIframe");
		fc.utobj().sleep(2000);
		if (ginfo1.getGroupName()!=null)
		{
			fc.utobj().sendKeys(driver, gUI.GroupName, ginfo1.getGroupName());
		}
		
		/*fc.utobj().clickElement(driver, gUI.GroupDescriptionLink);
		if(ginfo1.getDescription()!=null)
		{
			fc.utobj().sendKeys(driver, gUI.Description, ginfo1.getDescription());
		}*/
		
		if(ginfo1.getGroupType()!=null)
		{
			fc.utobj().selectDropDown(driver, gUI.GroupType, ginfo1.getGroupType());
		}
		
		if(ginfo1.getAccessibility()!=null)
		{
		fc.utobj().selectDropDown(driver, gUI.GroupAccess, ginfo1.getAccessibility());
		}

		return this;
		
	}

}
