package com.builds.test.crm;

import org.openqa.selenium.WebDriver;

import com.builds.uimaps.crm.CRM_CreateWorkFlowPageUI;
import com.builds.utilities.FranconnectUtil;

public class CRM_CreateWorkFlowPage {
	FranconnectUtil fc = new FranconnectUtil();
	
	public void Create_Workflowbtn(WebDriver driver) throws Exception
	{
		CRM_CreateWorkFlowPageUI workUI= new CRM_CreateWorkFlowPageUI(driver);
		fc.utobj().printTestStep("click on Create workflow Button ");
		fc.utobj().clickElement(driver, workUI.Create_WorkFlowbtn);
	}
	
	public void Nextbtn_onWorkflow(WebDriver driver) throws Exception
	{
		CRM_CreateWorkFlowPageUI workUI= new CRM_CreateWorkFlowPageUI(driver);
		fc.utobj().printTestStep("click on Next workflow Button ");
		fc.utobj().clickElement(driver, workUI.Next_onCreatePage);
	}
	
	public void Cancel_onWorkflow(WebDriver driver) throws Exception
	{
		CRM_CreateWorkFlowPageUI workUI= new CRM_CreateWorkFlowPageUI(driver);
		fc.utobj().printTestStep("click on cancel workflow Button ");
		fc.utobj().clickElement(driver, workUI.cancelbtn_onCreatePage);
	}
	
	public CRM_CreateWorkFlowPage filldetails(WebDriver driver, String Record_type , String workflow_type ) throws Exception
	{
		CRM_CreateWorkFlowPageUI workUI= new CRM_CreateWorkFlowPageUI(driver);
		fc.utobj().printTestStep("click on fill workflow details ");
		
		fc.utobj().sendKeys(driver, workUI.WorkFlow_Name, fc.utobj().generateTestData("Workflow"));
		if(Record_type.equalsIgnoreCase("Lead"))
		{
			fc.utobj().clickElement(driver, workUI.WorkFlow_Lead);
		}
		if(Record_type.equalsIgnoreCase("Contact"))
		{
			fc.utobj().clickElement(driver, workUI.WorkFlow_Contact);
		}
		if(workflow_type.equalsIgnoreCase("Event")) {
			fc.utobj().clickElement(driver, workUI.Event_Radio);
		}
		if(workflow_type.equalsIgnoreCase("Standard")) {
			fc.utobj().clickElement(driver, workUI.Standard_radio);
		}
		if(workflow_type.equalsIgnoreCase("Date")) {
			fc.utobj().clickElement(driver, workUI.Date_Radio);
		}
		
		return this;
	}
	
	
	
	

}
