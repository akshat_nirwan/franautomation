package com.builds.test.crm;

import java.util.Map;

import org.apache.xmlbeans.soap.SOAPArrayType;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.Test;
import org.testng.xml.ISuiteParser;

import com.builds.test.admin.AdminAreaRegionAddAreaRegionPageTest;
import com.builds.test.admin.AdminFranchiseLocationAddFranchiseLocationPageTest;
import com.builds.test.admin.AdminUsersManageCorporateUsersAddCorporateUserPageTest;
import com.builds.test.admin.AdminUsersManageRegionalUsersAddRegionalUserPageTest;
import com.builds.test.common.CRMModule;
import com.builds.test.common.CorporateUser;
import com.builds.test.common.FCHomePageTest;
import com.builds.uimaps.campaigncentercrm.CRMCampaignCenterCampaignsPage;
import com.builds.utilities.FranconnectUtil;
import com.builds.utilities.TestCase;

public class CRM_Group_VisibilityTest {
	
	FranconnectUtil fc = new FranconnectUtil();
	
	@Test(groups = "crm")
	@TestCase(createdOn = "2018-05-07", updatedOn = "2018-05-07", testCaseDescription = "CRM > Lead Summary > fill > Add task > modfiy task and delete ", testCaseId = "PTAU_Group_from_corporate_user")
	public void leadInfopage21() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());
        String area = "Group_Visibility";
		Map<String, String> dataSet = fc.utobj().readTestDatawithsqllite("crm", testCaseId,area);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			
			driver = fc.loginpage().login(driver);
			fc.utobj().printTestStep("Navigate To Admin Users Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			String emailId ="Groupvisibility@gmail.com";
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUser = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporateUser.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);
			String firstCorpuser = corpUser.getUserName();
			String firstCorpuser_pass = corpUser.getPassword();
			fc.loginpage().loginWithParameter(driver, firstCorpuser, firstCorpuser_pass);
			fc.utobj().printTestStep("Login with corporate user whose having corp admin privilage"+ corpUser.getFirstName() +" "+ corpUser.getLastName());
			
			FCHomePageTest fcHomePageTest = fc.commonMethods().getModules();
			CRMModule crm = new CRMModule();
			
			crm.crm_common().CRMGroupsLnk(driver);
			CRM_CreateGroupPage Group = new CRM_CreateGroupPage();
			CRM_CreateGroupPageSQLite Group1 = new CRM_CreateGroupPageSQLite();
			CRM_CreateGroupGetSetSQLite ginfo1 = new CRM_CreateGroupGetSetSQLite();
			ginfo1 = crm.crm_common().DefualtGroupinfoSQLite(ginfo1,dataSet);
			Group.Create_groupbtn(driver);
			Group1.fillGroupfromSQLite(driver, ginfo1);
			Group.savebtn(driver);
			
			String creategroup = ".//a[@class='textblue'][contains(text(),'"+ ginfo1.getGroupName()+"')]";
			fc.utobj().printTestStep("Group 1 for lead Created with Public to all user assisibility");

			Group.Create_groupbtn(driver);
			ginfo1 = crm.crm_common().DefualtGroupinfoSQLite(ginfo1,dataSet);
			ginfo1.setAccessibility("Public to all Corporate Users");
			Group1.fillGroupfromSQLite(driver, ginfo1);
			Group.savebtn(driver);
			String creategroup2 = ".//a[@class='textblue'][contains(text(),'"+ ginfo1.getGroupName()+"')]";
		
			fc.utobj().printTestStep("Group 2 for lead  Created with Public to all corporate user assisibility");
			
			Group.Create_groupbtn(driver);
			ginfo1 = crm.crm_common().DefualtGroupinfoSQLite(ginfo1,dataSet);
			ginfo1.setAccessibility("Private");
			Group1.fillGroupfromSQLite(driver, ginfo1);
			Group.savebtn(driver);
			String creategroup3 = ".//a[@class='textblue'][contains(text(),'"+ ginfo1.getGroupName()+"')]";
			
			fc.utobj().printTestStep("Group 3 for lead  Created with priavte assisibility");
			
			Group.Create_groupbtn(driver);
			ginfo1 = crm.crm_common().DefualtGroupinfoSQLite(ginfo1,dataSet);
			ginfo1.setGroupType("Contact");
			Group1.fillGroupfromSQLite(driver, ginfo1);
			Group.savebtn(driver);
			String creategroup4 = ".//a[@class='textblue'][contains(text(),'"+ ginfo1.getGroupName()+"')]";
			
			fc.utobj().printTestStep("Group 1 for contact Created with Public to all user assisibility");
			
			
			Group.Create_groupbtn(driver);
			ginfo1 = crm.crm_common().DefualtGroupinfoSQLite(ginfo1,dataSet);
			ginfo1.setAccessibility("Public to all Corporate Users");
			ginfo1.setGroupType("Contact");
			Group1.fillGroupfromSQLite(driver, ginfo1);
			Group.savebtn(driver);
			String creategroup5 = ".//a[@class='textblue'][contains(text(),'"+ ginfo1.getGroupName()+"')]";
			
			fc.utobj().printTestStep("Group 2 for contact  Created with Public to all corporate user assisibility");
			
			
			Group.Create_groupbtn(driver);
			ginfo1 = crm.crm_common().DefualtGroupinfoSQLite(ginfo1,dataSet);
			ginfo1.setAccessibility("Private");
			ginfo1.setGroupType("Contact");
			Group1.fillGroupfromSQLite(driver, ginfo1);
			Group.savebtn(driver);
			String creategroup6 = ".//a[@class='textblue'][contains(text(),'"+ ginfo1.getGroupName()+"')]";
			
			fc.utobj().printTestStep("Group 3 for contact  Created with private assisibility");
			
			fc.utobj().printTestStep("verify group name" );
			if(fc.utobj().isElementPresent(driver, creategroup) && fc.utobj().isElementPresent(driver, creategroup2)&& fc.utobj().isElementPresent(driver, creategroup3) 
					&& fc.utobj().isElementPresent(driver, creategroup4)
					&& fc.utobj().isElementPresent(driver, creategroup5) && fc.utobj().isElementPresent(driver, creategroup6))
            {
				fc.utobj().printTestStep("Group name present on the group page" );
			}else {
				fc.utobj().throwsException("Group names are not present on the group info page of corporate user");
			}
			
			CRM_Roles role = new CRM_Roles();
			String RoleName =  fc.utobj().generateTestData("Corprate Role");
			role.Addroles_with_All_privileges(driver, "Corporate Level", RoleName);
			fc.home_page().logout(driver);
			
			fc.utobj().printTestStep("Navigate To Admin Users and second Corporate User with corporate role");
			fc.utobj().printTestStep("Add Corporate User");
			String emailId1 =fc.utobj().generateTestData("Groupvisibility")+"@gmail.com";
			//AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUser = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			//CorporateUser corpUser = new CorporateUser();
			corpUser.setUserName(fc.utobj().generateTestData("vipin"));
			corpUser.setFirstName(fc.utobj().generateTestData("Second corp"));
			corpUser.setLastName(fc.utobj().generateTestData("User"));
			corpUser.setEmail(emailId1);
			corpUser.setRole(RoleName);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			fc.utobj().sleep(1000);
			corporateUser.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);
		
			fc.utobj().printTestStep("Login with Second corporate user");
			String secondcorpUser = corpUser.getUserName() ;
			String secondcorpUser_pass = corpUser.getPassword();
			fc.loginpage().loginWithParameter(driver, secondcorpUser, secondcorpUser_pass);
			
            //CRMModule crm = new CRMModule();
			crm.crm_common().CRMGroupsLnk(driver);
			
			fc.utobj().printTestStep("Verify Public to all user group with Second corporate user");
			if(fc.utobj().isElementPresent(driver, creategroup)&& fc.utobj().isElementPresent(driver, creategroup4))
			{
				fc.utobj().printTestStep("Public to all user group present");
			}else {
				fc.utobj().throwsException("Public to all user group are not present on second corp user");
			}
			
			fc.utobj().printTestStep("Verify Public to all corporate user with Second corporate user");
			if(fc.utobj().isElementPresent(driver, creategroup2) && fc.utobj().isElementPresent(driver, creategroup5))
			{
				fc.utobj().printTestStep("Public to all corporate user group present");
			}else {
				fc.utobj().throwsException("Public to all user group are not present on second corp user");
				
			}
			
			fc.utobj().printTestStep("Verify private group with Second corporate user");
			if(!fc.utobj().isElementPresent(driver, creategroup3) && !fc.utobj().isElementPresent(driver, creategroup6))
			{
				fc.utobj().printTestStep("private group not present and are working fine");
			}else {
				
				fc.utobj().throwsException("Public to all user group are not present on second corp user");
			}
			//corporate user work done 
			
			
			
			AdminAreaRegionAddAreaRegionPageTest p1 = new AdminAreaRegionAddAreaRegionPageTest();
			String regionName =  "regionName"+ fc.utobj().generateRandomNumber();
			p1.addAreaRegion(driver, regionName);
			
			String userName = "RegiuserName" + fc.utobj().generateRandomNumber();
			String password = "fran1234";
			String emailId2 = fc.utobj().generateTestData("Selenium")+"@gmail.com";
			AdminUsersManageRegionalUsersAddRegionalUserPageTest reguser = new AdminUsersManageRegionalUsersAddRegionalUserPageTest();
			reguser.addRegionalUser(driver, userName, password, regionName, emailId2);
			
			fc.home_page().logout(driver);
			
			fc.utobj().printTestStep("Login with regional user ");
			fc.loginpage().loginWithParameter(driver, userName, password);
			//CRMModule crm = new CRMModule();
			crm.crm_common().CRMGroupsLnk(driver);
			
			//verify corporate group in region
			fc.utobj().printTestStep("Verify Public to all user group with regional user");
			if(fc.utobj().isElementPresent(driver, creategroup)&& fc.utobj().isElementPresent(driver, creategroup4))
			{
				fc.utobj().printTestStep("Public to all user group present");
			}else {
				fc.utobj().throwsException("Public to all user group are not present on regional user");
			}
			
			fc.utobj().printTestStep("Verify Public to all corporate user with regional user");
			if(!fc.utobj().isElementPresent(driver, creategroup2) && !fc.utobj().isElementPresent(driver, creategroup5))
			{
				fc.utobj().printTestStep("Public to all corporate user group not present");
			}else {
				
				fc.utobj().throwsException("Public to all corporate user group  present regional user");
			}
			
			fc.utobj().printTestStep("Verify private group with regional user");
			if(!fc.utobj().isElementPresent(driver, creategroup3) && !fc.utobj().isElementPresent(driver, creategroup6))
			{
				fc.utobj().printTestStep("private group group of corporate not present");
			}else {
			
				fc.utobj().throwsException("Private group are present on regional user");
			}
			
			// Now Adding REgional groups
			Group.Create_groupbtn(driver);
			ginfo1 = crm.crm_common().DefualtGroupinfoSQLite(ginfo1,dataSet);
			ginfo1.setAccessibility("Public to all Users of my Region");
			ginfo1.setGroupType("Contact");
			Group1.fillGroupfromSQLite(driver, ginfo1);
			Group.savebtn(driver);
			String creategroup7 = ".//a[@class='textblue'][contains(text(),'"+ ginfo1.getGroupName()+"')]";
			
			fc.utobj().printTestStep("Group 2 for contact  Created with Public to all Users of my Region user assisibility");
			
			
			Group.Create_groupbtn(driver);
			ginfo1 = crm.crm_common().DefualtGroupinfoSQLite(ginfo1,dataSet);
			ginfo1.setAccessibility("Private");
			ginfo1.setGroupType("Contact");
			Group1.fillGroupfromSQLite(driver, ginfo1);
			Group.savebtn(driver);
			String creategroup8 = ".//a[@class='textblue'][contains(text(),'"+ ginfo1.getGroupName()+"')]";
			
			fc.utobj().printTestStep("Group 2 for contact  Created with Private assisibility");
			
			Group.Create_groupbtn(driver);
			ginfo1 = crm.crm_common().DefualtGroupinfoSQLite(ginfo1,dataSet);
			ginfo1.setAccessibility("Public to all Users of my Region");
			ginfo1.setGroupType("Lead");
			Group1.fillGroupfromSQLite(driver, ginfo1);
			Group.savebtn(driver);
			String creategroup9 = ".//a[@class='textblue'][contains(text(),'"+ ginfo1.getGroupName()+"')]";
		
			fc.utobj().printTestStep("Group 2 for contact  Created with Public to all Users of my Region user assisibility");
			
			
			Group.Create_groupbtn(driver);
			ginfo1 = crm.crm_common().DefualtGroupinfoSQLite(ginfo1,dataSet);
			ginfo1.setAccessibility("Private");
			ginfo1.setGroupType("Lead");
			Group1.fillGroupfromSQLite(driver, ginfo1);
			Group.savebtn(driver);
			String creategroup10 = ".//a[@class='textblue'][contains(text(),'"+ ginfo1.getGroupName()+"')]";
			
			fc.utobj().printTestStep("Group 2 for contact  Created with Private assisibility");
			
			
			if(fc.utobj().isElementPresent(driver, creategroup7) && fc.utobj().isElementPresent(driver, creategroup8)&& fc.utobj().isElementPresent(driver, creategroup9) 
					&& fc.utobj().isElementPresent(driver, creategroup10))
            {
				fc.utobj().printTestStep("Groups of regional are present on the group page of region user" );
			}else {
				fc.utobj().throwsException("reginoal created group are not present on regional user");
			}
			
			fc.home_page().logout(driver);
			
			fc.loginpage().loginWithParameter(driver, "adm", "t0n1ght");
			fc.utobj().printTestStep("creating franchise all user same region MU, employee, and diff region franchise");
			AdminFranchiseLocationAddFranchiseLocationPageTest fran = new AdminFranchiseLocationAddFranchiseLocationPageTest();
			
			String franID = fc.utobj().generateTestData("Vipinfran");
			String firstName = fc.utobj().generateTestData("Groupvisibility");
			String lastName = fc.utobj().generateTestData("franchise");
			String firstandlastName = firstName+" "+lastName;
			String loginId =fc.utobj().generateTestData("franMU");
			String Role = "Default Franchise Role";
			String userType ="Owner";
			fran.AddfranchiseuserwithlocationforMKT_existingregion(driver, franID,regionName, firstName, lastName, loginId, Role, userType, "New", "");
			
			String franID1 = fc.utobj().generateTestData("Vipinfran");
			String firstName1 = fc.utobj().generateTestData("Groupvisibility");
			String lastName1 = fc.utobj().generateTestData("franchise");
			String loginId1 =fc.utobj().generateTestData("franMU");;
			String Role1 = "Default Franchise Role";
			String userType1 ="New Employee";
			
			fran.AddfranchiseuserwithlocationforMKT_existingregion(driver, franID1, regionName, firstName1, lastName1, loginId1, Role1, userType1, "Existing", firstandlastName);
			
			String franID2 = fc.utobj().generateTestData("Vipinfran");
			String firstName2 = fc.utobj().generateTestData("Groupvisibility");
			String lastName2 = fc.utobj().generateTestData("franchise");
			String loginId2 =fc.utobj().generateTestData("franMU");;
			String Role2 = "Default Franchise Role";
			String userType2 ="Owner";
			String region = fc.utobj().generateTestData("regionName");
			fran.AddfranchiseuserwithlocationforMKT(driver, franID2, region, firstName2, lastName2, loginId2, Role2, userType2, "New", "");

			
			fc.home_page().logout(driver);
			fc.utobj().printTestStep("login with franchise MU user");
			fc.loginpage().loginWithParameter(driver, loginId, "fran1234");
			//CRMModule crm = new CRMModule();
			crm.crm_common().CRMGroupsLnk(driver);
			
			//corporate group verify 
			fc.utobj().printTestStep("Verify Public to all user group of corp user in MU franchise");
			if(fc.utobj().isElementPresent(driver, creategroup)&& fc.utobj().isElementPresent(driver, creategroup4))
			{
				fc.utobj().printTestStep("Public to all user of corp group present");
			}else {
				fc.utobj().throwsException("Public to all user group are not present on franchise user");
			}
			
			fc.utobj().printTestStep("Verify Public to all corporate user in corp user MU franchise");
			if(!fc.utobj().isElementPresent(driver, creategroup2) && !fc.utobj().isElementPresent(driver, creategroup5))
			{
				fc.utobj().printTestStep("Public to all corporate user group not present in franchise user");
			}else {
				fc.utobj().throwsException("Public to all corporate user group present in franchise user");
			}
			
			fc.utobj().printTestStep("Verify private group of corp user in mu franchise");
			if(!fc.utobj().isElementPresent(driver, creategroup3) && !fc.utobj().isElementPresent(driver, creategroup6))
			{
				fc.utobj().printTestStep("Private group present");
			}else {
				fc.utobj().throwsException("Public to all corporate user group not present in franchise user");
			}
			
			//regional groups verify 
			if(fc.utobj().isElementPresent(driver, creategroup7) && fc.utobj().isElementPresent(driver, creategroup9))
			{
				fc.utobj().printTestStep("Public to all Users of my Region are present");
			}else {
				fc.utobj().throwsException("Public to all Users of my Region not present in regional user");
			}
			
			
			if(!fc.utobj().isElementPresent(driver, creategroup8) && !fc.utobj().isElementPresent(driver, creategroup10))
			{
				fc.utobj().printTestStep("private group of region user are not present ");
			}else {
				fc.utobj().throwsException("Priavte user of region user present in regional user");
			}
			
			
			//creating Mu user groups
			Group.Create_groupbtn(driver);
			ginfo1 = crm.crm_common().DefualtGroupinfoSQLite(ginfo1,dataSet);
			ginfo1.setAccessibility("Public to all Users of my Franchise");
			ginfo1.setGroupType("Lead");
			Group1.fillGroupfromSQLite(driver, ginfo1);
			Group.savebtn(driver);
			String creategroup11 = ".//a[@class='textblue'][contains(text(),'"+ ginfo1.getGroupName()+"')]";
		
			fc.utobj().printTestStep("Group 2 for contact  Created with Private Region user assisibility");
			
			
			Group.Create_groupbtn(driver);
			ginfo1 = crm.crm_common().DefualtGroupinfoSQLite(ginfo1,dataSet);
			ginfo1.setAccessibility("Private");
			ginfo1.setGroupType("Lead");
			Group1.fillGroupfromSQLite(driver, ginfo1);
			Group.savebtn(driver);
			String creategroup12 = ".//a[@class='textblue'][contains(text(),'"+ ginfo1.getGroupName()+"')]";
			
			fc.utobj().printTestStep("Group 2 for contact  Created with Private franchise user assisibility");
			
			
			Group.Create_groupbtn(driver);
			ginfo1 = crm.crm_common().DefualtGroupinfoSQLite(ginfo1,dataSet);
			ginfo1.setAccessibility("Public to all Users of my Franchise");
			ginfo1.setGroupType("Contact");
			Group1.fillGroupfromSQLite(driver, ginfo1);
			Group.savebtn(driver);
			String creategroup13 = ".//a[@class='textblue'][contains(text(),'"+ ginfo1.getGroupName()+"')]";
			
			fc.utobj().printTestStep("Group 2 for contact  Created with Public to all Users of my franchise user assisibility");
			
			
			Group.Create_groupbtn(driver);
			ginfo1 = crm.crm_common().DefualtGroupinfoSQLite(ginfo1,dataSet);
			ginfo1.setAccessibility("Private");
			ginfo1.setGroupType("Contact");
			Group1.fillGroupfromSQLite(driver, ginfo1);
			Group.savebtn(driver);
			String creategroup14 = ".//a[@class='textblue'][contains(text(),'"+ ginfo1.getGroupName()+"')]";
			
			fc.utobj().printTestStep("Group 2 for contact  Created with priavte franchise user assisibility");
			
			
			if(fc.utobj().isElementPresent(driver, creategroup11) && fc.utobj().isElementPresent(driver, creategroup12)&& fc.utobj().isElementPresent(driver, creategroup13) 
					&& fc.utobj().isElementPresent(driver, creategroup14))
            {
				fc.utobj().printTestStep("Group name present on the group page" );
			}else {
				fc.utobj().throwsException("Mu user Group name are not present on the group page");
			}
			
			
			fc.home_page().logout(driver);
			fc.utobj().printTestStep("login with employe franchise user of diffrent location");
			fc.loginpage().loginWithParameter(driver, loginId1, "fran1234");
			
			//CRMModule crm = new CRMModule();
			crm.crm_common().CRMGroupsLnk(driver);
			
			fc.utobj().printTestStep("corporate group verify"); 
			fc.utobj().printTestStep("Verify Public to all user group with corp user");
			if(fc.utobj().isElementPresent(driver, creategroup)&& fc.utobj().isElementPresent(driver, creategroup4))
			{
				fc.utobj().printTestStep("Public to all user group present");
			}else {
				fc.utobj().throwsException("Public to all user group are not present on franchise user");
			}
			
			fc.utobj().printTestStep("Verify Public to all corporate user with corp user");
			if(!fc.utobj().isElementPresent(driver, creategroup2) && !fc.utobj().isElementPresent(driver, creategroup5))
			{
				fc.utobj().printTestStep("Public to all corporate user groupn not present in franchise user");
			}else {
				fc.utobj().throwsException("Public to all corporate user group present in franchise user");
			}
			
			fc.utobj().printTestStep("Verify private group with corp user");
			if(!fc.utobj().isElementPresent(driver, creategroup3) && !fc.utobj().isElementPresent(driver, creategroup6))
			{
				fc.utobj().printTestStep("Private group of corp user are not present");
			}else {
				fc.utobj().throwsException("Public to all corporate user group not present in franchise user");
			}
			
			//regional groups verify 
			if(fc.utobj().isElementPresent(driver, creategroup7) && fc.utobj().isElementPresent(driver, creategroup9))
			{
				fc.utobj().printTestStep("Public to all Users of my Region are present");
			}else {
				fc.utobj().throwsException("Public to all Users of my Region not present in regional user");
			}
			
			
			if(!fc.utobj().isElementPresent(driver, creategroup8) && !fc.utobj().isElementPresent(driver, creategroup10))
			{
				fc.utobj().printTestStep("private group are not present ");
			}else {
				fc.utobj().throwsException("Priavte user are not present in regional user");
			}
			
			//verify MU user group
			if(fc.utobj().isElementPresent(driver, creategroup11) && fc.utobj().isElementPresent(driver, creategroup13))
			{
				fc.utobj().printTestStep("Public to all Users of my Franchise");
			}else {
				fc.utobj().throwsException("Public to all Users of my Franchise are not present in regional user");
			}
			
			
			if(!fc.utobj().isElementPresent(driver, creategroup12) && !fc.utobj().isElementPresent(driver, creategroup14))
			{
				fc.utobj().printTestStep("private group of MU are not present ");
			}else {
				fc.utobj().throwsException("Priavte user are present in regional user");
			}
			
			
			//Creating employee franchise groups
			
			Group.Create_groupbtn(driver);
			ginfo1 = crm.crm_common().DefualtGroupinfoSQLite(ginfo1,dataSet);
			ginfo1.setAccessibility("Public to all Users of my Franchise");
			ginfo1.setGroupType("Lead");
			Group1.fillGroupfromSQLite(driver, ginfo1);
			Group.savebtn(driver);
			String creategroup15 = ".//a[@class='textblue'][contains(text(),'"+ ginfo1.getGroupName()+"')]";
			
			fc.utobj().printTestStep("Group 2 for contact  Created with priavte franchise user assisibility");
			
			Group.Create_groupbtn(driver);
			ginfo1 = crm.crm_common().DefualtGroupinfoSQLite(ginfo1,dataSet);
			ginfo1.setAccessibility("Private");
			ginfo1.setGroupType("Lead");
			Group1.fillGroupfromSQLite(driver, ginfo1);
			Group.savebtn(driver);
			String creategroup16 = ".//a[@class='textblue'][contains(text(),'"+ ginfo1.getGroupName()+"')]";
			
			fc.utobj().printTestStep("Group 2 for contact  Created with priavte franchise user assisibility");
			
			Group.Create_groupbtn(driver);
			ginfo1 = crm.crm_common().DefualtGroupinfoSQLite(ginfo1,dataSet);
			ginfo1.setAccessibility("Public to all Users of my Franchise");
			ginfo1.setGroupType("Contact");
			Group1.fillGroupfromSQLite(driver, ginfo1);
			Group.savebtn(driver);
			String creategroup17 = ".//a[@class='textblue'][contains(text(),'"+ ginfo1.getGroupName()+"')]";
			
			fc.utobj().printTestStep("Group 2 for contact  Created with priavte franchise user assisibility");
			
			Group.Create_groupbtn(driver);
			ginfo1 = crm.crm_common().DefualtGroupinfoSQLite(ginfo1,dataSet);
			ginfo1.setAccessibility("Private");
			ginfo1.setGroupType("Contact");
			Group1.fillGroupfromSQLite(driver, ginfo1);
			Group.savebtn(driver);
			String creategroup18 = ".//a[@class='textblue'][contains(text(),'"+ ginfo1.getGroupName()+"')]";
			
			fc.utobj().printTestStep("Group 2 for contact  Created with priavte franchise user assisibility");
			
			if(fc.utobj().isElementPresent(driver, creategroup15) && fc.utobj().isElementPresent(driver, creategroup16)&& fc.utobj().isElementPresent(driver, creategroup17) 
					&& fc.utobj().isElementPresent(driver, creategroup18))
            {
				fc.utobj().printTestStep("Group name present on the group page" );
			}else {
				fc.utobj().throwsException("franchise user Group name are not present on the group page");
			}
			
			
			fc.home_page().logout(driver);
			
			fc.utobj().printTestStep("login with different region franchise user");
			
			fc.loginpage().loginWithParameter(driver, loginId2, "fran1234");
			
			
			//CRMModule crm = new CRMModule();
			crm.crm_common().CRMGroupsLnk(driver);
			
			fc.utobj().printTestStep("corporate group verify"); 
			fc.utobj().printTestStep("Verify Public to all user group with corp user");
			if(fc.utobj().isElementPresent(driver, creategroup)&& fc.utobj().isElementPresent(driver, creategroup4))
			{
				fc.utobj().printTestStep("Public to all user group present");
			}else {
				fc.utobj().throwsException("Public to all user group are not present on franchise user");
			}
			
			fc.utobj().printTestStep("Verify Public to all corporate user with corp user");
			if(!fc.utobj().isElementPresent(driver, creategroup2) && !fc.utobj().isElementPresent(driver, creategroup5))
			{
				fc.utobj().printTestStep("Public to all corporate user groupn not present in franchise user");
			}else {
				fc.utobj().throwsException("Public to all corporate user group present in franchise user");
			}
			
			fc.utobj().printTestStep("Verify private group with corp user");
			if(!fc.utobj().isElementPresent(driver, creategroup3) && !fc.utobj().isElementPresent(driver, creategroup6))
			{
				fc.utobj().printTestStep("Private group present");
			}else {
				fc.utobj().throwsException("Public to all corporate user group not present in franchise user");
			}
			
			//regional groups verify 
			if(!fc.utobj().isElementPresent(driver, creategroup7) && !fc.utobj().isElementPresent(driver, creategroup9))
			{
				fc.utobj().printTestStep("Public to all Users of my Region are present");
			}else {
				fc.utobj().throwsException("Public to all Users of my Region not present in regional user");
			}
			
			
			if(!fc.utobj().isElementPresent(driver, creategroup8) && !fc.utobj().isElementPresent(driver, creategroup10))
			{
				fc.utobj().printTestStep("private group are not present ");
			}else {
				fc.utobj().throwsException("Priavte user are not present in regional user");
			}
			
			//verify MU user group
			if(!fc.utobj().isElementPresent(driver, creategroup11) && !fc.utobj().isElementPresent(driver, creategroup13))
			{
				fc.utobj().printTestStep("Public to all Users of my Franchise");
			}else {
				fc.utobj().throwsException("Public to all Users of my Franchise are not present in regional user");
			}
			
			
			if(!fc.utobj().isElementPresent(driver, creategroup12) && !fc.utobj().isElementPresent(driver, creategroup14))
			{
				fc.utobj().printTestStep("private group of MU are not present ");
			}else {
				fc.utobj().throwsException("Priavte user are present in regional user");
			}
			
			
			Group.Create_groupbtn(driver);
			ginfo1 = crm.crm_common().DefualtGroupinfoSQLite(ginfo1,dataSet);
			ginfo1.setAccessibility("Public to all Users of my Franchise");
			ginfo1.setGroupType("Lead");
			Group1.fillGroupfromSQLite(driver, ginfo1);
			Group.savebtn(driver);
			String creategroup19 = ".//a[@class='textblue'][contains(text(),'"+ ginfo1.getGroupName()+"')]";
			
			fc.utobj().printTestStep("Group 2 for contact  Created with priavte franchise user assisibility");
			
			
			
			Group.Create_groupbtn(driver);
			ginfo1 = crm.crm_common().DefualtGroupinfoSQLite(ginfo1,dataSet);
			ginfo1.setAccessibility("Private");
			ginfo1.setGroupType("Lead");
			Group1.fillGroupfromSQLite(driver, ginfo1);
			Group.savebtn(driver);
			String creategroup20 = ".//a[@class='textblue'][contains(text(),'"+ ginfo1.getGroupName()+"')]";
			
			fc.utobj().printTestStep("Group 2 for contact  Created with priavte franchise user assisibility");
			
			
			Group.Create_groupbtn(driver);
			ginfo1 = crm.crm_common().DefualtGroupinfoSQLite(ginfo1,dataSet);
			ginfo1.setAccessibility("Public to all Users of my Franchise");
			ginfo1.setGroupType("Contact");
			Group1.fillGroupfromSQLite(driver, ginfo1);
			Group.savebtn(driver);
			String creategroup21 = ".//a[@class='textblue'][contains(text(),'"+ ginfo1.getGroupName()+"')]";
			
			fc.utobj().printTestStep("Group 2 for contact  Created with priavte franchise user assisibility");
			
			
			
			Group.Create_groupbtn(driver);
			ginfo1 = crm.crm_common().DefualtGroupinfoSQLite(ginfo1,dataSet);
			ginfo1.setAccessibility("Private");
			ginfo1.setGroupType("Contact");
			Group1.fillGroupfromSQLite(driver, ginfo1);
			Group.savebtn(driver);
			String creategroup22 = ".//a[@class='textblue'][contains(text(),'"+ ginfo1.getGroupName()+"')]";
			
			fc.utobj().printTestStep("Group 2 for contact  Created with priavte franchise user assisibility");
			
			
			if(fc.utobj().isElementPresent(driver, creategroup19) && fc.utobj().isElementPresent(driver, creategroup20)&& fc.utobj().isElementPresent(driver, creategroup21) 
					&& fc.utobj().isElementPresent(driver, creategroup22))
            {
				fc.utobj().printTestStep("Group name present on the group page" );
			}else {
				fc.utobj().throwsException("franchise user Group name are not present on the group page");
			}
			
			
			fc.home_page().logout(driver);
			fc.utobj().printTestStep("Again login with MU franchise user");
			fc.loginpage().loginWithParameter(driver, loginId,"fran1234");
			
			crm.crm_common().CRMGroupsLnk(driver);
			
			fc.utobj().printTestStep("different region franchise user group verify again with MU user");
			
			
			if(!fc.utobj().isElementPresent(driver, creategroup19) && !fc.utobj().isElementPresent(driver, creategroup21))
			{
				fc.utobj().printTestStep("Public to all Users of my Franchise");
			}else {
				fc.utobj().throwsException("Public to all Users of my Franchise are not present in MU user of different franchise");
			}
			
			
			if(!fc.utobj().isElementPresent(driver, creategroup20) && !fc.utobj().isElementPresent(driver, creategroup22))
			{
				fc.utobj().printTestStep("private group of MU are not present ");
			}else {
				fc.utobj().throwsException("Priavte user are present in in MU user of different franchise");
			}
			
			
			fc.utobj().printTestStep("verify employee franchisee user groups ");
			
			if(fc.utobj().isElementPresent(driver, creategroup15) && fc.utobj().isElementPresent(driver, creategroup17))
			{
				fc.utobj().printTestStep("Public to all Users of my Franchise");
			}else {
				fc.utobj().throwsException("Public to all Users of my Franchise are not present in regional user");
			}
			
			
			if(!fc.utobj().isElementPresent(driver, creategroup16) && !fc.utobj().isElementPresent(driver, creategroup18))
			{
				fc.utobj().printTestStep("private group of emp franchise are not present ");
			}else {
				fc.utobj().throwsException("Priavte user are present of emp franchise user");
			}
			
			fc.home_page().logout(driver);
			
			fc.utobj().printTestStep("Again login with regional user");
			fc.loginpage().loginWithParameter(driver, userName, "fran1234");
			
			crm.crm_common().CRMGroupsLnk(driver);
			
			fc.utobj().printTestStep("verify MU and employee franchisee user groups on regional level ");
			
			if(fc.utobj().isElementPresent(driver, creategroup15) && fc.utobj().isElementPresent(driver, creategroup17) && fc.utobj().isElementPresent(driver, creategroup11) && fc.utobj().isElementPresent(driver, creategroup13))
			{
				fc.utobj().printTestStep("Public to all Users of my Franchise");
			}else {
				fc.utobj().throwsException("Public to all Users of my Franchise are not present in regional user");
			}
			
			
			if(!fc.utobj().isElementPresent(driver, creategroup16) && !fc.utobj().isElementPresent(driver, creategroup18) && !fc.utobj().isElementPresent(driver, creategroup12) && !fc.utobj().isElementPresent(driver, creategroup14))
			{
				fc.utobj().printTestStep("private group of MU are not present ");
			}else {
				fc.utobj().throwsException("Priavte user are present in regional user");
			}
			
			
			fc.utobj().printTestStep("verify another region franchisee user groups on regional level ");
			
			if(!fc.utobj().isElementPresent(driver, creategroup19) && !fc.utobj().isElementPresent(driver, creategroup20) && !fc.utobj().isElementPresent(driver, creategroup21) && !fc.utobj().isElementPresent(driver, creategroup22))
			{
				fc.utobj().printTestStep("different region franchise user groups are not visible");
			}else {
				fc.utobj().throwsException("different region franchise user groups are visible");
			}
			
			
			fc.home_page().logout(driver);
			fc.utobj().printBugStatus("login with second corporate user");
			fc.loginpage().loginWithParameter(driver,secondcorpUser, secondcorpUser_pass);
			
			crm.crm_common().CRMGroupsLnk(driver);
			
			fc.utobj().printTestStep("verify MU and employee franchisee user groups on regional level ");
			
			if(fc.utobj().isElementPresent(driver, creategroup15) && fc.utobj().isElementPresent(driver, creategroup17) && fc.utobj().isElementPresent(driver, creategroup11) && fc.utobj().isElementPresent(driver, creategroup13))
			{
				fc.utobj().printTestStep("Public to all Users of my Franchise");
			}else {
				fc.utobj().throwsException("Public to all Users of my Franchise are not present in regional user");
			}
			
			
			if(!fc.utobj().isElementPresent(driver, creategroup16) && !fc.utobj().isElementPresent(driver, creategroup18) && !fc.utobj().isElementPresent(driver, creategroup12) && !fc.utobj().isElementPresent(driver, creategroup14))
			{
				fc.utobj().printTestStep("private group of MU are not present ");
			}else {
				fc.utobj().throwsException("Priavte user are present in regional user");
			}
			
			
			fc.utobj().printTestStep("verify another region franchisee user groups on regional level ");
			
			if(fc.utobj().isElementPresent(driver, creategroup19) && fc.utobj().isElementPresent(driver, creategroup21))
			{
				fc.utobj().printTestStep("different region franchise user  public to all groups are  visible");
			}else {
				fc.utobj().throwsException("different region franchise user  public to all groups are not visible");
			}
			
			if(!fc.utobj().isElementPresent(driver, creategroup20) && !fc.utobj().isElementPresent(driver, creategroup22))
			{
				fc.utobj().printTestStep("different region franchise user private groups are not visible");
			}else {
				fc.utobj().throwsException("different region franchise user private groups are visible");
			}
			
			
			fc.utobj().printTestStep("Verify the regional groups");
			
			if(fc.utobj().isElementPresent(driver, creategroup7) && fc.utobj().isElementPresent(driver, creategroup9))
			{
				fc.utobj().printTestStep("Public to all Users of my Region are present");
			}else {
				fc.utobj().throwsException("Public to all Users of my Region not present in regional user");
			}
			
			
			if(!fc.utobj().isElementPresent(driver, creategroup8) && !fc.utobj().isElementPresent(driver, creategroup10))
			{
				fc.utobj().printTestStep("private group are not present ");
			}else {
				fc.utobj().throwsException("Priavte user are not present in regional user");
			}
			
			
			
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
			
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}
	
	
	
	

}
