package com.builds.test.crm;

import org.openqa.selenium.WebDriver;
import com.builds.uimaps.crm.CRM_LogACall_UI;
import com.builds.utilities.FranconnectUtil;

public class CRM_LogaCallfillTest {

	FranconnectUtil fc = new FranconnectUtil();

	public CRM_LogaCallfillTest fillcalremarkpage (WebDriver driver,CRM_LogaCallgetsetTest call) throws Exception 
	{

		
		CRM_LogACall_UI logcall = new CRM_LogACall_UI(driver);

		if(call.getSubject()!=null)
		{

			fc.utobj().sendKeys(driver, logcall.callsubject, call.getSubject());
		}
		fc.utobj().sleep(1000);
		if(call.getCallTime()!=null)
		{

			fc.utobj().sendKeys(driver, logcall.Calltime, call.getCallTime());
		}
		fc.utobj().sleep(1000);
		if(call.getStatus()!=null)
		{

			fc.utobj().sendKeys(driver, logcall.Status, call.getStatus());
		}
		if(call.getType()!=null)
		{

			fc.utobj().sendKeys(driver, logcall.Type, call.getType());
		}
		if(call.getComment()!=null)
		{

			fc.utobj().sendKeys(driver, logcall.commnet, call.getComment());
		}
		fc.utobj().printTestStep("log a call filled");
		return this;
	}
		
	     public void clickreset (WebDriver driver) throws Exception
		{
	    	 fc.utobj().printTestStep("click on reset button");
			CRM_LogACall_UI logcall = new CRM_LogACall_UI(driver);
			fc.utobj().clickElement(driver, logcall.ResetButton);

		}
		
		public void clickSave (WebDriver driver) throws Exception
		{
			fc.utobj().printTestStep("click on save button");
			CRM_LogACall_UI logcall = new CRM_LogACall_UI(driver);
			fc.utobj().clickElement(driver, logcall.save);
  
		}
		
		
   }
		
	

