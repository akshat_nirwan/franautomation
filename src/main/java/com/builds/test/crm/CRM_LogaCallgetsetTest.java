package com.builds.test.crm;

public class CRM_LogaCallgetsetTest {
	
	
	private String subject;
	private String callTime;
	private String Status;
	private String Type;
	private String comment;
	private String savebutton;
	private String Resetbutton;
	
	
	
	
	public String getSubject() {
		return subject;
	}
	public void setSubject(String subject) {
		this.subject = subject;
	}
	public String getCallTime() {
		return callTime;
	}
	public void setCallTime(String callTime) {
		this.callTime = callTime;
	}
	public String getStatus() {
		return Status;
	}
	public void setStatus(String status) {
		Status = status;
	}
	public String getType() {
		return Type;
	}
	public void setType(String type) {
		Type = type;
	}
	public String getComment() {
		return comment;
	}
	public void setComment(String comment) {
		this.comment = comment;
	}
	public String getSavebutton() {
		return savebutton;
	}
	public void setSavebutton(String savebutton) {
		this.savebutton = savebutton;
	}
	public String getResetbutton() {
		return Resetbutton;
	}
	public void setResetbutton(String resetbutton) {
		Resetbutton = resetbutton;
	}
	
	
 
}
