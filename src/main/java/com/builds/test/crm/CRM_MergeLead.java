package com.builds.test.crm;

import java.util.Map;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import com.builds.test.admin.AdminAreaRegionAddAreaRegionPageTest;
import com.builds.test.admin.AdminUsersManageCorporateUsersAddCorporateUserPageTest;
import com.builds.test.admin.AdminUsersManageRegionalUsersAddRegionalUserPageTest;
import com.builds.test.common.CRMModule;
import com.builds.test.common.CorporateUser;
import com.builds.test.common.FCHomePageTest;
import com.builds.uimaps.crm.CRMLeadsPage;
import com.builds.uimaps.crm.CRM_CampaignFinalandLunchUI;
import com.builds.uimaps.crm.CRM_CampaignSelectRecipientUI;
import com.builds.uimaps.crm.CRM_LeadinfopageUI;
import com.builds.uimaps.crm.CRM_LogACall_UI;
import com.builds.utilities.FranconnectUtil;
import com.builds.utilities.TestCase;

public class CRM_MergeLead {

	FranconnectUtil fc = new FranconnectUtil();

	@Test(groups = "crm")
	@TestCase(createdOn = "2018-05-07", updatedOn = "2018-05-07", testCaseDescription = "CRM > Lead Summary > fill > Add task > modfiy task and delete ", testCaseId = "Lead_merge_corporate")
	public void MergeLead() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());
		String area = "Lead_Merge";
		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestDatawithsqllite("crm", testCaseId, area);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			fc.utobj().printTestStep("Navigate To Admin Users Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			String emailId = "Groupvisibility@gmail.com";
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUser = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporateUser.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);
			fc.utobj().printTestStep("login with created corp user");
			fc.loginpage().loginWithParameter(driver, corpUser.getUserName(), corpUser.getPassword());

			FCHomePageTest fcHomePageTest = fc.commonMethods().getModules();
			CRMModule crm = new CRMModule();
			crm.crm_common().openCRMCampaignCenterPage(fcHomePageTest, driver);
			CRM_CampaginCenterPagelink add = new CRM_CampaginCenterPagelink();
			add.CreateCampaign(driver);
			CRM_CampaignAddpage addfill = new CRM_CampaignAddpage();
			CRM_CampaignAddGetSet camAdd = new CRM_CampaignAddGetSet();
			camAdd = crm.crm_common().defualtCampaignfill(camAdd);
			addfill.filldetails(driver, camAdd);
			addfill.clickStartbtn(driver);
			Thread.sleep(500);

			CRM_SelectandCustomizeTemplatepage custom = new CRM_SelectandCustomizeTemplatepage();

			custom.firstTemplateclick(driver);
			Thread.sleep(500);

			CRM_TemplateInfoPageGetSet teminfo = new CRM_TemplateInfoPageGetSet();
			CRM_TemplateInfoPage info = new CRM_TemplateInfoPage();
			teminfo = crm.crm_common().defualtTemplateinfofill(teminfo);

			info.filldetails(driver, teminfo);
			info.savebtn(driver);

			Thread.sleep(500);
			info.saveAndcontinue(driver);
			Thread.sleep(500);

			CRM_CampaignSelectRecipientpage recipient = new CRM_CampaignSelectRecipientpage();
			CRM_CampaignSelectRecipientUI recipientUI = new CRM_CampaignSelectRecipientUI(driver);
			recipient.AssociateLater(driver);
			Thread.sleep(500);

			CRM_CampaignFinalandLunchpage lunch = new CRM_CampaignFinalandLunchpage();
			CRM_CampaignFinalandLunchUI lunchUI = new CRM_CampaignFinalandLunchUI(driver);
			fc.utobj().selectDropDown(driver, lunchUI.TimeZone, "GMT +05:30 India");
			String CampaignName = lunchUI.campaignName.getText();
			lunch.clickFinalUpperbtn(driver);
			String confirm = lunchUI.confirmpagetext.getText();

			if (confirm.equalsIgnoreCase(CampaignName + " " + "has been scheduled successfully")) {
				System.out.println("Campaign Add Successfully ");
			} else {
				System.out.println("campaign not added");
			}

			// second campaign

			crm.crm_common().openCRMCampaignCenterPage(fcHomePageTest, driver);
			// CRM_CampaginCenterPagelink add = new CRM_CampaginCenterPagelink();
			add.CreateCampaign(driver);
			// CRM_CampaignAddpage addfill = new CRM_CampaignAddpage();
			// CRM_CampaignAddGetSet camAdd = new CRM_CampaignAddGetSet();
			camAdd = crm.crm_common().defualtCampaignfill(camAdd);
			addfill.filldetails(driver, camAdd);
			addfill.clickStartbtn(driver);
			Thread.sleep(500);

			// CRM_SelectandCustomizeTemplatepage custom = new
			// CRM_SelectandCustomizeTemplatepage();

			custom.firstTemplateclick(driver);
			Thread.sleep(500);

			// CRM_TemplateInfoPageGetSet teminfo = new CRM_TemplateInfoPageGetSet();
			// CRM_TemplateInfoPage info = new CRM_TemplateInfoPage();
			teminfo = crm.crm_common().defualtTemplateinfofill(teminfo);

			info.filldetails(driver, teminfo);
			info.savebtn(driver);

			Thread.sleep(500);
			info.saveAndcontinue(driver);
			Thread.sleep(500);

			// CRM_CampaignSelectRecipientpage recipient = new
			// CRM_CampaignSelectRecipientpage();
			// CRM_CampaignSelectRecipientUI recipientUI = new
			// CRM_CampaignSelectRecipientUI(driver);
			recipient.AssociateLater(driver);
			Thread.sleep(500);

			// CRM_CampaignFinalandLunchpage lunch = new CRM_CampaignFinalandLunchpage();
			// CRM_CampaignFinalandLunchUI lunchUI = new
			// CRM_CampaignFinalandLunchUI(driver);
			fc.utobj().selectDropDown(driver, lunchUI.TimeZone, "GMT +05:30 India");
			String CampaignName1 = lunchUI.campaignName.getText();
			lunch.clickFinalUpperbtn(driver);
			String confirm1 = lunchUI.confirmpagetext.getText();

			if (confirm.equalsIgnoreCase(CampaignName1 + " " + "has been scheduled successfully")) {
				System.out.println("Campaign Add Successfully ");
			} else {
				System.out.println("campaign not added");
			}

			String Corporate_user_name = corpUser.getFirstName() + " " + corpUser.getLastName();
			String firstName = fc.utobj().generateTestData(dataSet.get("FirstName"));
			String lastName = fc.utobj().generateTestData(dataSet.get("LastName"));
			String company = fc.utobj().generateTestData(dataSet.get("company"));
			String assignTo = "Corporate";
			String city = fc.utobj().generateTestData(dataSet.get("City"));
			String address = fc.utobj().generateTestData(dataSet.get("Address"));
			String suffix = fc.utobj().generateTestData(dataSet.get("Suffix"));
			String jobTitle = fc.utobj().generateTestData(dataSet.get("JobTitle"));
			String comment = fc.utobj().generateTestData(dataSet.get("comment"));
			String franchiseId = "";
			String franchiseUser = "";
			String userNameReg = "";
			String regionName = "";

			fc.utobj().printTestStep("Navigate To CRM > Leads > Leads Summary");
			fc.utobj().printTestStep("Add Lead");

			CRMLead_AreaCampaignTestCase add1 = new CRMLead_AreaCampaignTestCase();
			add1.addLead(driver, dataSet, firstName, lastName, company, assignTo, city, address, suffix, jobTitle,
					comment, Corporate_user_name, regionName, userNameReg, franchiseId, franchiseUser, config);

			CRM_leadInfoPageOptions leadinfo = new CRM_leadInfoPageOptions();
			leadinfo.logACallomOnInfoPage(driver);
			CRM_LogaCallfillTest callfill = new CRM_LogaCallfillTest();
			CRM_LogaCallgetsetTest call = new CRM_LogaCallgetsetTest();
			call = crm.crm_common().defualtdataForCallRemark(call);

			Thread.sleep(2000);
			callfill.fillcalremarkpage(driver, call);
			callfill.clickSave(driver);
			String call_Subject = call.getSubject();
			leadinfo.AssociatecampOnInfoPage(driver);

			fc.utobj().sleep(1000);

			fc.utobj().clickElement(driver, recipientUI.getXpathOfCampaignAction(CampaignName));
			fc.utobj().clickElement(driver, recipientUI.confirmOfAssociateWithCampagin);
			
			fc.utobj().sleep(1000);
			
			
			String Corporate_user_name1 = corpUser.getFirstName() + " " + corpUser.getLastName();
			String firstName1 = fc.utobj().generateTestData(dataSet.get("FirstName"));
			String lastName1 = fc.utobj().generateTestData(dataSet.get("LastName"));
			String company1 = fc.utobj().generateTestData(dataSet.get("company"));
			String assignTo1 = "Corporate";
			String city1 = fc.utobj().generateTestData(dataSet.get("City"));
			String address1 = fc.utobj().generateTestData(dataSet.get("Address"));
			String suffix1 = fc.utobj().generateTestData(dataSet.get("Suffix"));
			String jobTitle1 = fc.utobj().generateTestData(dataSet.get("JobTitle"));
			String comment1 = fc.utobj().generateTestData(dataSet.get("comment"));
			String franchiseId1 = "";
			String franchiseUser1 = "";
			String userNameReg1 = "";
			String regionName1 = "";

			fc.utobj().printTestStep("Navigate To CRM > Leads > Leads Summary");
			fc.utobj().printTestStep("Add Lead");
			
			
			add1.addLead(driver, dataSet, firstName1, lastName1, company1, assignTo1, city1, address1, suffix1, jobTitle1,
					comment1, Corporate_user_name1, regionName1, userNameReg1, franchiseId1, franchiseUser1, config);
			
			leadinfo.AssociatecampOnInfoPage(driver);

			fc.utobj().sleep(1000);

			fc.utobj().clickElement(driver, recipientUI.getXpathOfCampaignAction(CampaignName1));
			fc.utobj().clickElement(driver, recipientUI.confirmOfAssociateWithCampagin);
			CRMLeadsPage pobj = new CRMLeadsPage(driver);
			fc.utobj().clickElement(driver, pobj.leadsLink);
			CRMLead_AreaTestCase search = new CRMLead_AreaTestCase();
			
			search.searchLeadByOwner(driver, corpUser.getuserFullName(), "Open Leads");
			
			
			fc.utobj().printTestStep("Merge Leads");
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//a[contains(text () ,'" + firstName
					+ " " + lastName + "')]/ancestor::tr/td/input[@name='checkBox']"));
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//a[contains(text () ,'"
					+ firstName1 + " " + lastName1 + "')]/ancestor::tr/td/input[@name='checkBox']"));
			fc.utobj().selectActionMenuItemsWithTagA(driver, "Merge Leads");	
			
			
			WebElement Secondradio = driver.findElement(By.xpath(".//input[@name='sel' and @onclick='selSecond()']"));
			
			if(!fc.utobj().isSelected(driver, Secondradio)) {
				fc.utobj().clickElement(driver, Secondradio);
			}
			
			fc.utobj().sendKeys(driver, pobj.remarksAtMerge, "Merge Remarks of Leads.");
			fc.utobj().clickElement(driver, pobj.mergeBtn);
			CRM_LeadinfopageUI mod = new CRM_LeadinfopageUI(driver);
			String activehst = mod.Activity_History.getText();
			
			if(activehst.contains("Leads added through merging of two Leads"))
			{
				fc.utobj().printTestStep("Merge Remark created");
				
			}else {
				fc.utobj().printTestStep("Merge Remark not created");
			}
			
			if (activehst.contains(call_Subject))  {
				fc.utobj().printTestStep("call found in activity history");
			
				/*WebElement call_active = driver.findElement(By.xpath("//span[@class = 'callfrom'][contains(text(),'"+call_Subject+"')]"));
				fc.utobj().clickElement(driver, call_active);*/
				
			}else {
				fc.utobj().printTestStep("call not found");
			
			}
			
			if(activehst.contains(CampaignName)&&activehst.contains(CampaignName1)) {
				fc.utobj().printTestStep("both campaign present in the activity history");
				
			}else {
				fc.utobj().printTestStep("both campaign are not present in the activity history");
			}
			

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}
	
	
	
	
	
	@Test(groups = "MI10")
	@TestCase(createdOn = "2018-05-07", updatedOn = "2018-05-07", testCaseDescription = "CRM > Lead Summary > fill > Add task > modfiy task and delete ", testCaseId = "Lead_merge_Regional")
	public void MergeLead1() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());
		String area = "Lead_Merge";
		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestDatawithsqllite("crm", testCaseId, area);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			
			
			AdminAreaRegionAddAreaRegionPageTest p1 = new AdminAreaRegionAddAreaRegionPageTest();
			String regionName =  "regionName"+ fc.utobj().generateRandomNumber();
			p1.addAreaRegion(driver, regionName);
			
			String userName = "RegiuserName" + fc.utobj().generateRandomNumber();
			String password = "fran1234";
			String emailId2 = fc.utobj().generateTestData("Selenium")+"@gmail.com";
			AdminUsersManageRegionalUsersAddRegionalUserPageTest reguser = new AdminUsersManageRegionalUsersAddRegionalUserPageTest();
			reguser.addRegionalUser(driver, userName, password, regionName, emailId2);
			
			fc.home_page().logout(driver);
			
			fc.utobj().printTestStep("Login with regional user ");
			fc.loginpage().loginWithParameter(driver, userName, password);
			
			FCHomePageTest fcHomePageTest = fc.commonMethods().getModules();
			CRMModule crm = new CRMModule();
			crm.crm_common().openCRMCampaignCenterPage(fcHomePageTest, driver);
			CRM_CampaginCenterPagelink add = new CRM_CampaginCenterPagelink();
			add.CreateCampaign(driver);
			CRM_CampaignAddpage addfill = new CRM_CampaignAddpage();
			CRM_CampaignAddGetSet camAdd = new CRM_CampaignAddGetSet();
			camAdd = crm.crm_common().defualtCampaignfill(camAdd);
			addfill.filldetails(driver, camAdd);
			addfill.clickStartbtn(driver);
			Thread.sleep(500);

			CRM_SelectandCustomizeTemplatepage custom = new CRM_SelectandCustomizeTemplatepage();

			custom.firstTemplateclick(driver);
			Thread.sleep(500);

			CRM_TemplateInfoPageGetSet teminfo = new CRM_TemplateInfoPageGetSet();
			CRM_TemplateInfoPage info = new CRM_TemplateInfoPage();
			teminfo = crm.crm_common().defualtTemplateinfofill(teminfo);

			info.filldetails(driver, teminfo);
			info.savebtn(driver);

			Thread.sleep(500);
			info.saveAndcontinue(driver);
			Thread.sleep(500);

			CRM_CampaignSelectRecipientpage recipient = new CRM_CampaignSelectRecipientpage();
			CRM_CampaignSelectRecipientUI recipientUI = new CRM_CampaignSelectRecipientUI(driver);
			recipient.AssociateLater(driver);
			Thread.sleep(500);

			CRM_CampaignFinalandLunchpage lunch = new CRM_CampaignFinalandLunchpage();
			CRM_CampaignFinalandLunchUI lunchUI = new CRM_CampaignFinalandLunchUI(driver);
			fc.utobj().selectDropDown(driver, lunchUI.TimeZone, "GMT +05:30 India");
			String CampaignName = lunchUI.campaignName.getText();
			lunch.clickFinalUpperbtn(driver);
			String confirm = lunchUI.confirmpagetext.getText();

			if (confirm.equalsIgnoreCase(CampaignName + " " + "has been scheduled successfully")) {
				System.out.println("Campaign Add Successfully ");
			} else {
				System.out.println("campaign not added");
			}

			// second campaign

			crm.crm_common().openCRMCampaignCenterPage(fcHomePageTest, driver);
			// CRM_CampaginCenterPagelink add = new CRM_CampaginCenterPagelink();
			add.CreateCampaign(driver);
			// CRM_CampaignAddpage addfill = new CRM_CampaignAddpage();
			// CRM_CampaignAddGetSet camAdd = new CRM_CampaignAddGetSet();
			camAdd = crm.crm_common().defualtCampaignfill(camAdd);
			addfill.filldetails(driver, camAdd);
			addfill.clickStartbtn(driver);
			Thread.sleep(500);

			// CRM_SelectandCustomizeTemplatepage custom = new
			// CRM_SelectandCustomizeTemplatepage();

			custom.firstTemplateclick(driver);
			Thread.sleep(500);

			// CRM_TemplateInfoPageGetSet teminfo = new CRM_TemplateInfoPageGetSet();
			// CRM_TemplateInfoPage info = new CRM_TemplateInfoPage();
			teminfo = crm.crm_common().defualtTemplateinfofill(teminfo);

			info.filldetails(driver, teminfo);
			info.savebtn(driver);

			Thread.sleep(500);
			info.saveAndcontinue(driver);
			Thread.sleep(500);

			// CRM_CampaignSelectRecipientpage recipient = new
			// CRM_CampaignSelectRecipientpage();
			// CRM_CampaignSelectRecipientUI recipientUI = new
			// CRM_CampaignSelectRecipientUI(driver);
			recipient.AssociateLater(driver);
			Thread.sleep(500);

			// CRM_CampaignFinalandLunchpage lunch = new CRM_CampaignFinalandLunchpage();
			// CRM_CampaignFinalandLunchUI lunchUI = new
			// CRM_CampaignFinalandLunchUI(driver);
			fc.utobj().selectDropDown(driver, lunchUI.TimeZone, "GMT +05:30 India");
			String CampaignName1 = lunchUI.campaignName.getText();
			lunch.clickFinalUpperbtn(driver);
			String confirm1 = lunchUI.confirmpagetext.getText();

			if (confirm.equalsIgnoreCase(CampaignName1 + " " + "has been scheduled successfully")) {
				System.out.println("Campaign Add Successfully ");
			} else {
				System.out.println("campaign not added");
			}

			
			String region_user_name = userName + " " + userName;
			String firstName = fc.utobj().generateTestData(dataSet.get("FirstName"));
			String lastName = fc.utobj().generateTestData(dataSet.get("LastName"));
			String company = fc.utobj().generateTestData(dataSet.get("company"));
			String assignTo = "Regional";
			String city = fc.utobj().generateTestData(dataSet.get("City"));
			String address = fc.utobj().generateTestData(dataSet.get("Address"));
			String suffix = fc.utobj().generateTestData(dataSet.get("Suffix"));
			String jobTitle = fc.utobj().generateTestData(dataSet.get("JobTitle"));
			String comment = fc.utobj().generateTestData(dataSet.get("comment"));
			String franchiseId = "";
			String franchiseUser = "";
			String userNameReg = "";
			String region = "";

			fc.utobj().printTestStep("Navigate To CRM > Leads > Leads Summary");
			fc.utobj().printTestStep("Add Lead");

			CRMLead_AreaCampaignTestCase add1 = new CRMLead_AreaCampaignTestCase();
			add1.addLead_fromOwnUser(driver, dataSet, firstName, lastName, company, assignTo, city, address, suffix, jobTitle,
				comment, userName, regionName, region_user_name, franchiseId, franchiseUser, config);

			CRM_leadInfoPageOptions leadinfo = new CRM_leadInfoPageOptions();
			leadinfo.logACallomOnInfoPage(driver);
			CRM_LogaCallfillTest callfill = new CRM_LogaCallfillTest();
			CRM_LogaCallgetsetTest call = new CRM_LogaCallgetsetTest();
			call = crm.crm_common().defualtdataForCallRemark(call);

			Thread.sleep(2000);
			callfill.fillcalremarkpage(driver, call);
			callfill.clickSave(driver);
			String call_Subject = call.getSubject();
			leadinfo.AssociatecampOnInfoPage(driver);

			fc.utobj().sleep(1000);

			fc.utobj().clickElement(driver, recipientUI.getXpathOfCampaignAction(CampaignName));
			fc.utobj().clickElement(driver, recipientUI.confirmOfAssociateWithCampagin);
			
			fc.utobj().sleep(1000);
			
			
			String region_user_name1 = userName + " " + userName;
			String firstName1 = fc.utobj().generateTestData(dataSet.get("FirstName"));
			String lastName1 = fc.utobj().generateTestData(dataSet.get("LastName"));
			String company1 = fc.utobj().generateTestData(dataSet.get("company"));
			String assignTo1 = "Regional";
			String city1 = fc.utobj().generateTestData(dataSet.get("City"));
			String address1 = fc.utobj().generateTestData(dataSet.get("Address"));
			String suffix1 = fc.utobj().generateTestData(dataSet.get("Suffix"));
			String jobTitle1 = fc.utobj().generateTestData(dataSet.get("JobTitle"));
			String comment1 = fc.utobj().generateTestData(dataSet.get("comment"));
			String franchiseId1 = "";
			String franchiseUser1 = "";
			String userNameReg1 = "";
			String regionName1 = "";

			fc.utobj().printTestStep("Navigate To CRM > Leads > Leads Summary");
			fc.utobj().printTestStep("Add Lead");
			
			
			add1.addLead_fromOwnUser(driver, dataSet, firstName1, lastName1, company1, assignTo1, city1, address1, suffix1, jobTitle1,
					comment1, userName, regionName1, region_user_name1, franchiseId1, franchiseUser1, config);
			
			leadinfo.AssociatecampOnInfoPage(driver);

			fc.utobj().sleep(1000);

			fc.utobj().clickElement(driver, recipientUI.getXpathOfCampaignAction(CampaignName1));
			fc.utobj().clickElement(driver, recipientUI.confirmOfAssociateWithCampagin);
			CRMLeadsPage pobj = new CRMLeadsPage(driver);
			fc.utobj().clickElement(driver, pobj.leadsLink);
			CRMLead_AreaTestCase search = new CRMLead_AreaTestCase();
			
			search.searchLeadByOwner(driver, region_user_name, "Open Leads");
			
			
			fc.utobj().printTestStep("Merge Leads");
			
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//a[contains(text () ,'" + firstName
					+ " " + lastName + "')]/ancestor::tr/td/input[@name='checkBox']"));
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//a[contains(text () ,'"
					+ firstName1 + " " + lastName1 + "')]/ancestor::tr/td/input[@name='checkBox']"));
			fc.utobj().selectActionMenuItemsWithTagA(driver, "Merge Leads");	
			
			
			WebElement Secondradio = driver.findElement(By.xpath(".//input[@name='sel' and @onclick='selSecond()']"));
			
			if(!fc.utobj().isSelected(driver, Secondradio)) {
				fc.utobj().clickElement(driver, Secondradio);
			}
			
			fc.utobj().sendKeys(driver, pobj.remarksAtMerge, "Merge Remarks of Leads.");
			fc.utobj().clickElement(driver, pobj.mergeBtn);
			CRM_LeadinfopageUI mod = new CRM_LeadinfopageUI(driver);
			String activehst = mod.Activity_History.getText();
			
			if(activehst.contains("Leads added through merging of two Leads"))
			{
				fc.utobj().printTestStep("Merge Remark created");
				
			}else {
				fc.utobj().printTestStep("Merge Remark not created");
			}
			
			if (activehst.contains(call_Subject))  {
				fc.utobj().printTestStep("call found in activity history");
			
				/*WebElement call_active = driver.findElement(By.xpath("//span[@class = 'callfrom'][contains(text(),'"+call_Subject+"')]"));
				fc.utobj().clickElement(driver, call_active);*/
				
			}else {
				fc.utobj().printTestStep("call not found");
			
			}
			
			if(activehst.contains(CampaignName)&&activehst.contains(CampaignName1)) {
				fc.utobj().printTestStep("both campaign present in the activity history");
				
			}else {
				fc.utobj().printTestStep("both campaign are not present in the activity history");
			}
			
			
			

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}
	
}
	

	
	
