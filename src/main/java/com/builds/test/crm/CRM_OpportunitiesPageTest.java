package com.builds.test.crm;

import java.io.IOException;
import java.util.Map;
import java.util.Set;

import javax.xml.parsers.ParserConfigurationException;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;
import org.xml.sax.SAXException;

import com.builds.test.admin.AdminDivisionAddDivisionPageTest;
import com.builds.test.admin.AdminUsersManageCorporateUsersAddCorporateUserPageTest;
import com.builds.test.admin.AdminUsersManageDivisionalUsersAddDivisionalUsersPageTest;
import com.builds.test.common.CorporateUser;
import com.builds.uimaps.crm.AdminCRMManageWebFormGeneratorPage;
import com.builds.uimaps.crm.CRMAccountsPage;
import com.builds.uimaps.crm.CRMContactsPage;
import com.builds.uimaps.crm.CRMLeadsPage;
import com.builds.uimaps.crm.CRMOpportunitiesPageUI;
import com.builds.utilities.FranconnectUtil;
import com.builds.utilities.ReadLocalEmail;
import com.builds.utilities.TestCase;

public class CRM_OpportunitiesPageTest {
	FranconnectUtil fc = new FranconnectUtil();

	@Test(groups = {"crm"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify the Add Opportunity At CRM > Opportunities > Opportunity Summary Page", testCaseId = "TC_234_Add_Opportunity")
	public void addOpportunity() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			CRMAccountsPage pobj = new CRMAccountsPage(driver);
			CRMAccountsPageTest accountPage = new CRMAccountsPageTest();
			CRMContactsPageTest contactsPage = new CRMContactsPageTest();

			fc.utobj().printTestStep("Navigate To Admin Users Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			String emailId = "crmautomation@staffex.com";
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUser = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporateUser.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			fc.utobj().printTestStep("Navigating to Admin > CRM > ContactType Configuration");
			fc.utobj().printTestStep("Add Contact Type");
			AdminCRMContactTypeConfigurationPageTest contactTypePage = new AdminCRMContactTypeConfigurationPageTest();
			String contactType = fc.utobj().generateTestData(dataSet.get("contactType"));
			contactTypePage.addContactType(driver, contactType);
			fc.utobj().printTestStep("Navigating to Admin > CRM >  Configure Opportunity Stages");
			fc.utobj().printTestStep("Add Stage");
			AdminCRMConfigureOpportunityStagesPageTest stagePage = new AdminCRMConfigureOpportunityStagesPageTest();
			String stage = fc.utobj().generateTestData(dataSet.get("stage"));
			stagePage.addStage(driver, stage);

			AdminCRMConfigureIndustryPageTest industryPage = new AdminCRMConfigureIndustryPageTest();
			String industry = fc.utobj().generateTestData(dataSet.get("industry"));
			String description = fc.utobj().generateTestData(dataSet.get("description"));
			fc.utobj().printTestStep("Navigating to Admin > CRM >  Configure Industry Type");
			fc.utobj().printTestStep("Add Industry");
			industryPage.addIndustry(driver, industry, description);

			String accountName = fc.utobj().generateTestData(dataSet.get("accountName"));
			String visibility = dataSet.get("visibility");
			fc.utobj().printTestStep("Navigating to CRM >  Accounts > Accounts Summary");
			fc.utobj().printTestStep("Add Account");
			accountPage.addAcocunt(driver, industry, accountName, visibility);

			fc.utobj().printTestStep("Navigating to CRM >  Opportunities > Opportunities Summary");
			fc.utobj().printTestStep("Add Contact");

			fc.utobj().clickElement(driver, pobj.opportunitiesLinks);
			fc.utobj().clickElement(driver, pobj.addNew);
			fc.utobj().clickElement(driver, pobj.contactLnk);

			String cFname = fc.utobj().generateTestData(dataSet.get("cFname"));
			String cLname = fc.utobj().generateTestData(dataSet.get("cLname"));
			String primaryCMethod = dataSet.get("primaryCMethod");
			String assignTo = "Corporate";
			String regionName = "";
			String franchiseId = "";
			String title = dataSet.get("title");
			
			String email = fc.utobj().generateTestData("crmautomation")+"@staffex.com";
			//String email = "crmautomation@staffex.com";

			contactsPage.addContactGenericWithAccount(driver, contactType, cFname, cLname, primaryCMethod, assignTo,
					corpUser.getuserFullName(), regionName, franchiseId, title, email, config, accountName);

			fc.utobj().printTestStep("Navigating to CRM >  Opportunities > Opportunities Summary");
			fc.utobj().printTestStep("Add Opportunity");

			fc.utobj().clickElement(driver, pobj.opportunitiesLinks);
			fc.utobj().clickElement(driver, pobj.addNew);
			fc.utobj().clickElement(driver, pobj.OpportunityLnk);

			String opportunityName = fc.utobj().generateTestData(dataSet.get("opportunityName"));
			String contactName = cFname + " " + cLname;
			String salesAmount = dataSet.get("salesAmount");
			addOpportunityGenric(driver, opportunityName, accountName, contactName, corpUser.getuserFullName(), stage,
					salesAmount, config);

			fc.utobj().clickElement(driver, pobj.opportunitiesLinks);
			opportunityFilter(driver, stage);

			fc.utobj().printTestStep("Verify Opportunity");
			boolean isOpportunityPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[.='" + opportunityName + "']");
			if (isOpportunityPresent == false) {
				fc.utobj().throwsException("was not able to verify Opportunity Name");
			}
			boolean isAssignToPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[.='" + opportunityName
					+ "']/ancestor::tr/td[contains(text () , '" + corpUser.getuserFullName() + "')]");
			if (isAssignToPresent == false) {
				fc.utobj().throwsException("was not able to verify Assign To");
			}
			boolean isAccountNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[.='" + opportunityName + "']/ancestor::tr/td/a[contains(text () , '" + accountName + "')]");
			if (isAccountNamePresent == false) {
				fc.utobj().throwsException("was not able to verify Account");
			}
			boolean isContactNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[.='" + opportunityName + "']/ancestor::tr/td/a[contains(text () , '" + contactName + "')]");
			if (isContactNamePresent == false) {
				fc.utobj().throwsException("was not able to verify Contact");
			}
			boolean isStagePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[.='" + opportunityName + "']/ancestor::tr/td[contains(text () , '" + stage + "')]");
			if (isStagePresent == false) {
				fc.utobj().throwsException("was not able to verify Stage");
			}
			boolean isSalesAmountPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[.='" + opportunityName
					+ "']/ancestor::tr/td[contains(text () , '" + salesAmount.concat(".00") + "')]");
			if (isSalesAmountPresent == false) {
				fc.utobj().throwsException("was not able to verify Sales Amount");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = {"crm"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Add Lead At CRM > Opportunities > Opportunity Summary", testCaseId = "TC_235_Add_Lead")
	public void addLead() throws IOException, Exception, ParserConfigurationException, SAXException {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			CRMLeadsPage pobj = new CRMLeadsPage(driver);

			CRMLeadsPageTest leadPage = new CRMLeadsPageTest();

			/*
			 * fc.utobj().printTestStep(testCaseId,
			 * "Navigate To Admin Users Manage Corporate User");
			 * fc.utobj().printTestStep(testCaseId, "Add Corporate User");
			 * String emailId="crmautomation@staffex.com";
			 * AdminUsersManageCorporateUsersAddCorporateUserPageTest
			 * corporateUser=new
			 * AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			 * CorporateUser corpUser = new
			 * CorporateUser();corpUser.setEmail(emailId);corpUser =
			 * fc.CommonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			 * corporateUser.
			 * createDefaultCorporateUser_Through_WebService_AfterLogin(driver,
			 * corpUser);
			 */

			fc.utobj().printTestStep("Navigate To Admin Users Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			String emailId = "crmautomation@staffex.com";
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUser = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporateUser.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			AdminCRMSourceSummaryPageTest addSourcePage = new AdminCRMSourceSummaryPageTest();
			String source = fc.utobj().generateTestData(dataSet.get("source"));
			String description = fc.utobj().generateTestData(dataSet.get("description"));
			String displayOnWebPageText = fc.utobj().generateTestData(dataSet.get("text"));
			fc.utobj().printTestStep("Navigating to Admin > CRM > Source Summary");
			fc.utobj().printTestStep("Add Source");
			addSourcePage.addSource(driver, source, description, displayOnWebPageText);
			fc.utobj().printTestStep("Add Source Details");
			String sourceDetails = fc.utobj().generateTestData(dataSet.get("sourceDetails"));
			addSourcePage.addSourceDetails(driver, source, sourceDetails, displayOnWebPageText, description);
			fc.utobj().printTestStep("Navigating to Admin > CRM > Configure Medium");
			fc.utobj().printTestStep("Add Medium");
			AdminCRMConfigureMediumPageTest mediumPage = new AdminCRMConfigureMediumPageTest();
			String medium = fc.utobj().generateTestData(dataSet.get("medium"));
			mediumPage.addMedium(driver, medium, description);
			fc.utobj().printTestStep("Navigating to Admin > CRM > Configure Lead Type");
			fc.utobj().printTestStep("Add Lead Type");
			AdminCRMConfigureLeadTypePageTest leadTypePage = new AdminCRMConfigureLeadTypePageTest();
			String leadType = fc.utobj().generateTestData(dataSet.get("leadType"));
			leadTypePage.addLeadType(driver, leadType);

			fc.utobj().printTestStep("Navigating to CRM >  Opportunities > Opportunities Summary");
			fc.crm().crm_common().CRMOpportunitiesLnk(driver);

			fc.utobj().printTestStep("Add Lead");

			fc.utobj().clickElement(driver, pobj.addNew);
			fc.utobj().clickElement(driver, pobj.leadLnk);
			fc.utobj().selectDropDown(driver, pobj.title, dataSet.get("title"));
			String firstName = fc.utobj().generateTestData(dataSet.get("firstName"));
			fc.utobj().sendKeys(driver, pobj.leadFirstName, firstName);
			String lastName = fc.utobj().generateTestData(dataSet.get("lastName"));
			fc.utobj().sendKeys(driver, pobj.leadLastName, lastName);
			String company = fc.utobj().generateTestData(dataSet.get("company"));
			fc.utobj().sendKeys(driver, pobj.companyName, company);
			fc.utobj().selectDropDown(driver, pobj.primaryContactMethodSelect, dataSet.get("primaryContactMethod"));
			if (!fc.utobj().isSelected(driver, pobj.assignToCorporate)) {
				fc.utobj().clickElement(driver, pobj.assignToCorporate);
			}
			fc.utobj().selectDropDown(driver, pobj.selectCorporateUser, corpUser.getuserFullName());
			String address = fc.utobj().generateTestData(dataSet.get("address"));
			fc.utobj().sendKeys(driver, pobj.address, address);
			String city = fc.utobj().generateTestData(dataSet.get("city"));
			fc.utobj().sendKeys(driver, pobj.city, city);
			fc.utobj().selectDropDown(driver, pobj.country, "USA");
			fc.utobj().selectDropDown(driver, pobj.state, "Alabama");
			fc.utobj().sendKeys(driver, pobj.zipcode, "12345");
			fc.utobj().sendKeys(driver, pobj.phoneNumbers, "1236547896");
			fc.utobj().sendKeys(driver, pobj.extn, "12");
			fc.utobj().sendKeys(driver, pobj.faxNumbers, "1263985487");
			fc.utobj().sendKeys(driver, pobj.mobileNumbers, "8896724441");
			//String email = dataSet.get("emailId");
			String email=fc.utobj().generateTestData("crmautomtion")+"@staffex.com";
			fc.utobj().sendKeys(driver, pobj.emailIds, email);
			fc.utobj().sendKeys(driver, pobj.alternateEmail, "test@gmail.com");
			String suffix = fc.utobj().generateTestData(dataSet.get("suffix"));
			fc.utobj().sendKeys(driver, pobj.suffix, suffix);
			String jobTitle = fc.utobj().generateTestData(dataSet.get("jobTitle"));
			fc.utobj().sendKeys(driver, pobj.position, jobTitle);
			fc.utobj().sendKeys(driver, pobj.birthdate, "11/24/1980");
			fc.utobj().sendKeys(driver, pobj.anniversarydate, "11/24/1999");
			fc.utobj().selectDropDown(driver, pobj.leadType, leadType);
			fc.utobj().selectDropDown(driver, pobj.rating, dataSet.get("rating"));
			fc.utobj().selectDropDown(driver, pobj.leadSource, source);
			fc.utobj().selectDropDown(driver, pobj.leadSourceDetails, sourceDetails);
			fc.utobj().selectDropDown(driver, pobj.contactMedium, medium);
			fc.utobj().sendKeys(driver, pobj.bestTimeToContact, "12:00 PM EST");
			String comment = fc.utobj().generateTestData(dataSet.get("comment"));
			fc.utobj().sendKeys(driver, pobj.comments, comment);
			fc.utobj().clickElement(driver, pobj.saveBtn);

			// verify at Home Page
			fc.utobj().printTestStep("Navigate To Lead Home Page");
			fc.utobj().clickElement(driver, pobj.leadsLink);
			leadPage.searchLeadByOwner(driver, corpUser.getuserFullName(), "Open Leads");

			fc.utobj().printTestStep("Verify Lead");

			boolean verifyName = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[.='" + firstName + " " + lastName + "']");
			if (verifyName == false) {
				fc.utobj().throwsException("was not able to verify Name");
			}
			boolean ownerOfLead = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[.='" + firstName + " " + lastName
					+ "']/ancestor::tr/td[contains(text () , '" + corpUser.getuserFullName() + "')]");
			if (ownerOfLead == false) {
				fc.utobj().throwsException("was not able to verify owner of the lead");
			}
			/*boolean statusOfLead = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[.='" + firstName + " " + lastName + "']/ancestor::tr/td[contains(text () , 'New')]");
			if (statusOfLead == false) {
				fc.utobj().throwsException("was not able to verify staus of the lead");
			}*/
			boolean emailOfLead = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[.='" + firstName + " " + lastName
					+ "']/ancestor::tr/td/a[contains(text () , '" + email + "')]");
			if (emailOfLead == false) {
				fc.utobj().throwsException("was not able to verify email of the lead");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "crm")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify the Add Account At CRM > Opportunities > Opportunity Summary", testCaseId = "TC_236_Add_Account")
	public void addAccount() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			CRMAccountsPage pobj = new CRMAccountsPage(driver);

			CRMAccountsPageTest accountPage = new CRMAccountsPageTest();

			AdminCRMConfigureIndustryPageTest industryPage = new AdminCRMConfigureIndustryPageTest();

			String industry = fc.utobj().generateTestData(dataSet.get("industry"));
			String description = fc.utobj().generateTestData(dataSet.get("description"));
			fc.utobj().printTestStep("Navigating to Admin > CRM >  Configure Industry Type");
			fc.utobj().printTestStep("Add Industry");
			industryPage.addIndustry(driver, industry, description);

			fc.utobj().printTestStep("Navigating to CRM >  Opportunities > Opportunities Summary");
			fc.utobj().printTestStep("Add Account");
			fc.crm().crm_common().CRMOpportunitiesLnk(driver);
			fc.utobj().clickElement(driver, pobj.addNew);
			fc.utobj().clickElement(driver, pobj.accountLnk);
			String accountName = fc.utobj().generateTestData(dataSet.get("accountName"));
			fc.utobj().sendKeys(driver, pobj.accountName, accountName);
			if (!fc.utobj().isSelected(driver, pobj.B2BAccountType)) {
				fc.utobj().clickElement(driver, pobj.B2BAccountType);
			}
			fc.utobj().selectDropDown(driver, pobj.visibilityType, "All Network");
			if (fc.utobj().isSelected(driver,pobj.isParent)) {
				fc.utobj().clickElement(driver, pobj.isParent);
			}
			fc.utobj().sendKeys(driver, pobj.address, "AddressTest");
			fc.utobj().sendKeys(driver, pobj.city, "City");
			fc.utobj().selectDropDown(driver, pobj.country, "USA");
			fc.utobj().sendKeys(driver, pobj.zipcode, "123455");
			fc.utobj().selectDropDown(driver, pobj.state, "Alabama");
			fc.utobj().sendKeys(driver, pobj.phoneNumbers, "1236547896");
			fc.utobj().sendKeys(driver, pobj.extn, "123");
			fc.utobj().sendKeys(driver, pobj.faxNumbers, "1236654");
			fc.utobj().sendKeys(driver, pobj.mobileNumbers, "1478523698");
			fc.utobj().sendKeys(driver, pobj.emailIds, "crmautomation@staffex.com");
			fc.utobj().sendKeys(driver, pobj.noOfEmployee, "500");
			fc.utobj().selectDropDown(driver, pobj.industry, industry);
			fc.utobj().sendKeys(driver, pobj.sicCode, "SicCode");
			fc.utobj().sendKeys(driver, pobj.servicesProvided, "Service123");
			fc.utobj().sendKeys(driver, pobj.stockSymbol, "qwerty");

			if (!fc.utobj().isSelected(driver, pobj.addContactNo)) {
				fc.utobj().clickElement(driver, pobj.addContactNo);
			}
			fc.utobj().clickElement(driver, pobj.saveBtn);

			fc.utobj().printTestStep("Navigating to CRM >  Accounts > Accounts Summary");

			fc.utobj().clickElement(driver, pobj.accountsLink);
			accountPage.searchAccountByIndustry(driver, industry);

			fc.utobj().printTestStep("Verify Account");

			boolean isAccountNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[.='" + accountName + "']");
			if (isAccountNamePresent == false) {
				fc.utobj().throwsException("was not able to add Account");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = {"crm"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify the Add Contact At CRM > Opportunities > Opportunity Summary", testCaseId = "TC_237_Add_Contact")
	public void addContact() throws IOException, Exception, ParserConfigurationException, SAXException {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());
		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			CRMLeadsPage pobj = new CRMLeadsPage(driver);

			CRMContactsPageTest contactsPage = new CRMContactsPageTest();

			/*
			 * fc.utobj().printTestStep(testCaseId,
			 * "Navigate To Admin Users Manage Corporate User");
			 * fc.utobj().printTestStep(testCaseId, "Add Corporate User");
			 * String emailId="crmautomation@staffex.com";
			 * AdminUsersManageCorporateUsersAddCorporateUserPageTest
			 * corporateUser=new
			 * AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			 * CorporateUser corpUser = new
			 * CorporateUser();corpUser.setEmail(emailId);corpUser =
			 * fc.CommonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			 * corporateUser.
			 * createDefaultCorporateUser_Through_WebService_AfterLogin(driver,
			 * corpUser);
			 */

			fc.utobj().printTestStep("Navigate To Admin Users Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			String emailId = "crmautomation@staffex.com";
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUser = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporateUser.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			AdminCRMContactTypeConfigurationPageTest contatcTypePage = new AdminCRMContactTypeConfigurationPageTest();
			String contactType = fc.utobj().generateTestData(dataSet.get("contactType"));
			contatcTypePage.addContactType(driver, contactType);

			fc.utobj().printTestStep("Navigating to CRM >  Opportunities > Opportunities Summary");
			fc.utobj().printTestStep("Add Contact");

			fc.crm().crm_common().CRMOpportunitiesLnk(driver);
			fc.utobj().clickElement(driver, pobj.addNew);
			fc.utobj().clickElement(driver, pobj.contactLnk);

			String firstName = fc.utobj().generateTestData(dataSet.get("firstName"));
			String lastName = fc.utobj().generateTestData(dataSet.get("lastName"));
			String primaryCMethod = dataSet.get("primaryCMethod");
			String assignTo = "Corporate";
			String regionName = "";
			String franchiseId = "";
			String title = dataSet.get("title");
			String email = fc.utobj().generateTestData("crmautomation")+"@staffex.com";   //crmautomation@staffex.com";

			contactsPage.addContactGeneric(driver, contactType, firstName, lastName, primaryCMethod, assignTo,
					corpUser.getuserFullName(), regionName, franchiseId, title, email, config);

			fc.utobj().printTestStep("Verify Contact");

			boolean isContactTypePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () ,'Contact Type')]/ancestor::tr/td[.='" + contactType + "']");
			if (isContactTypePresent == false) {
				fc.utobj().throwsException("was not able to verify Contact Type");
			}
			boolean isContactNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () ,'Name')]/ancestor::tr/td[.='Mr. " + firstName + " " + lastName + "']");
			if (isContactNamePresent == false) {
				fc.utobj().throwsException("was not able to verify Contact Name");
			}
			boolean isPrimaryContactTypePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () ,'Primary Contact Method')]/ancestor::tr/td[.='Email']");
			if (isPrimaryContactTypePresent == false) {
				fc.utobj().throwsException("was not able to verify Primary Contact Type");
			}
			boolean isUserPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () ,'Assign To ')]/ancestor::tr/td[contains(text () , '"
							+ corpUser.getuserFullName() + "')]");
			if (isUserPresent == false) {
				fc.utobj().throwsException("was not able to verify Assign To");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = {"crm"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify the Modify Opportunity At CRM > Opportunities > Opportunity Summary", testCaseId = "TC_238_Modify_Opportunity")
	public void modifyOpportunityActionImage() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());
		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			CRMAccountsPage pobj = new CRMAccountsPage(driver);
			CRMAccountsPageTest accountPage = new CRMAccountsPageTest();
			CRMContactsPageTest contactsPage = new CRMContactsPageTest();

			/*
			 * fc.utobj().printTestStep(testCaseId,
			 * "Navigate To Admin Users Manage Corporate User");
			 * fc.utobj().printTestStep(testCaseId, "Add Corporate User");
			 * String emailId="crmautomation@staffex.com";
			 * AdminUsersManageCorporateUsersAddCorporateUserPageTest
			 * corporateUser=new
			 * AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			 * CorporateUser corpUser = new
			 * CorporateUser();corpUser.setEmail(emailId);corpUser =
			 * fc.CommonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			 * corporateUser.
			 * createDefaultCorporateUser_Through_WebService_AfterLogin(driver,
			 * corpUser);
			 */

			fc.utobj().printTestStep("Navigate To Admin Users Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			String emailId = fc.utobj().generateTestData("crmautomation")+"@staffex.com"; //"crmautomation@staffex.com";
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUser = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporateUser.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			fc.utobj().printTestStep("Navigating to Admin > CRM > ContactType Configuration");
			fc.utobj().printTestStep("Add Contact Type");
			AdminCRMContactTypeConfigurationPageTest contactTypePage = new AdminCRMContactTypeConfigurationPageTest();
			String contactType = fc.utobj().generateTestData(dataSet.get("contactType"));
			contactTypePage.addContactType(driver, contactType);
			fc.utobj().printTestStep("Navigating to Admin > CRM >  Configure Opportunity Stages");
			fc.utobj().printTestStep("Add Stage");
			AdminCRMConfigureOpportunityStagesPageTest stagePage = new AdminCRMConfigureOpportunityStagesPageTest();
			String stage = fc.utobj().generateTestData(dataSet.get("stage"));
			stagePage.addStage(driver, stage);

			AdminCRMConfigureIndustryPageTest industryPage = new AdminCRMConfigureIndustryPageTest();
			String industry = fc.utobj().generateTestData(dataSet.get("industry"));
			String description = fc.utobj().generateTestData(dataSet.get("description"));
			fc.utobj().printTestStep("Navigating to Admin > CRM >  Configure Industry Type");
			fc.utobj().printTestStep("Add Industry");
			industryPage.addIndustry(driver, industry, description);

			String accountName = fc.utobj().generateTestData(dataSet.get("accountName"));
			String visibility = dataSet.get("visibility");
			fc.utobj().printTestStep("Navigating to CRM >  Accounts > Accounts Summary");
			fc.utobj().printTestStep("Add Account");
			accountPage.addAcocunt(driver, industry, accountName, visibility);

			fc.utobj().printTestStep("Navigating to CRM >  Opportunities > Opportunities Summary");
			fc.utobj().printTestStep("Add Contact");

			fc.utobj().clickElement(driver, pobj.opportunitiesLinks);
			fc.utobj().clickElement(driver, pobj.addNew);
			fc.utobj().clickElement(driver, pobj.contactLnk);

			String cFname = fc.utobj().generateTestData(dataSet.get("cFname"));
			String cLname = fc.utobj().generateTestData(dataSet.get("cLname"));
			String primaryCMethod = dataSet.get("primaryCMethod");
			String assignTo = "Corporate";
			String regionName = "";
			String franchiseId = "";
			String title = dataSet.get("title");
			//String email = "crmautomation@staffex.com";
			String email = fc.utobj().generateTestData("crmautomation")+"@staffex.com";
			contactsPage.addContactGenericWithAccount(driver, contactType, cFname, cLname, primaryCMethod, assignTo,
					corpUser.getuserFullName(), regionName, franchiseId, title, email, config, accountName);

			fc.utobj().printTestStep("Navigating to CRM >  Opportunities > Opportunities Summary");
			fc.utobj().printTestStep("Add Opportunity");

			fc.utobj().clickElement(driver, pobj.opportunitiesLinks);
			fc.utobj().clickElement(driver, pobj.addNew);
			fc.utobj().clickElement(driver, pobj.OpportunityLnk);

			String opportunityName = fc.utobj().generateTestData(dataSet.get("opportunityName"));
			String contactName = cFname + " " + cLname;
			String salesAmount = dataSet.get("salesAmount");
			addOpportunityGenric(driver, opportunityName, accountName, contactName, corpUser.getuserFullName(), stage,
					salesAmount, config);

			fc.utobj().clickElement(driver, pobj.opportunitiesLinks);
			opportunityFilter(driver, stage);

			fc.utobj().printTestStep("Modify Opportunity");

			fc.utobj().actionImgOption(driver, opportunityName, "Modify");
			opportunityName = fc.utobj().generateTestData(dataSet.get("opportunityName"));
			fc.utobj().sendKeys(driver, pobj.opportunityName, opportunityName);
			fc.utobj().clickElement(driver, pobj.saveBtn);

			fc.utobj().clickElement(driver, pobj.opportunitiesLinks);

			opportunityFilter(driver, stage);

			fc.utobj().printTestStep("Verify Opportunity");
			boolean isOpportunityPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[.='" + opportunityName + "']");
			if (isOpportunityPresent == false) {
				fc.utobj().throwsException("was not able to verify Opportunity Name");
			}
			boolean isAssignToPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[.='" + opportunityName
					+ "']/ancestor::tr/td[contains(text () , '" + corpUser.getuserFullName() + "')]");
			if (isAssignToPresent == false) {
				fc.utobj().throwsException("was not able to verify Assign To");
			}
			boolean isAccountNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[.='" + opportunityName + "']/ancestor::tr/td/a[contains(text () , '" + accountName + "')]");
			if (isAccountNamePresent == false) {
				fc.utobj().throwsException("was not able to verify Account");
			}
			boolean isContactNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[.='" + opportunityName + "']/ancestor::tr/td/a[contains(text () , '" + contactName + "')]");
			if (isContactNamePresent == false) {
				fc.utobj().throwsException("was not able to verify Contact");
			}
			boolean isStagePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[.='" + opportunityName + "']/ancestor::tr/td[contains(text () , '" + stage + "')]");
			if (isStagePresent == false) {
				fc.utobj().throwsException("was not able to verify Stage");
			}
			boolean isSalesAmountPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[.='" + opportunityName
					+ "']/ancestor::tr/td[contains(text () , '" + salesAmount.concat(".00") + "')]");
			if (isSalesAmountPresent == false) {
				fc.utobj().throwsException("was not able to verify Sales Amount");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "crm" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify the Add Transaction At CRM > Opportunities > Opportunity Summary", testCaseId = "TC_239_Add_Transaction")
	public void addTransactionActionImage() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());
		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			CRMAccountsPage pobj = new CRMAccountsPage(driver);
			CRMContactsPageTest contactsPage = new CRMContactsPageTest();
			CRMAccountsPageTest accountPage = new CRMAccountsPageTest();

			String description = fc.utobj().generateTestData(dataSet.get("description"));
			AdminCRMManageProductServiceCategoryPageTest serviceProductPage = new AdminCRMManageProductServiceCategoryPageTest();
			String categoryName = fc.utobj().generateTestData(dataSet.get("categoryName"));
			String productName = fc.utobj().generateTestData(dataSet.get("productName"));
			String oneLineDescription = fc.utobj().generateTestData(dataSet.get("oneLineDescription"));
			String rate = dataSet.get("rate");
			fc.utobj().printTestStep("Navigating to Admin > CRM >  Manage Product / Service & Category");
			fc.utobj().printTestStep("Add Category");
			serviceProductPage.addCategory(driver, categoryName, description);
			fc.utobj().printTestStep("Add Product/Service");
			serviceProductPage.addProductService(driver, categoryName, productName, oneLineDescription, rate);

			fc.utobj().printTestStep("Navigate To Admin Users Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			String emailId = "crmautomation@staffex.com";
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUser = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporateUser.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			fc.utobj().printTestStep("Navigating to Admin > CRM > ContactType Configuration");
			fc.utobj().printTestStep("Add Contact Type");
			AdminCRMContactTypeConfigurationPageTest contactTypePage = new AdminCRMContactTypeConfigurationPageTest();
			String contactType = fc.utobj().generateTestData(dataSet.get("contactType"));
			contactTypePage.addContactType(driver, contactType);

			fc.utobj().printTestStep("Navigating to Admin > CRM >  Configure Opportunity Stages");
			fc.utobj().printTestStep("Add Stage");
			AdminCRMConfigureOpportunityStagesPageTest stagePage = new AdminCRMConfigureOpportunityStagesPageTest();
			String stage = fc.utobj().generateTestData(dataSet.get("stage"));
			stagePage.addStage(driver, stage);

			AdminCRMConfigureIndustryPageTest industryPage = new AdminCRMConfigureIndustryPageTest();
			String industry = fc.utobj().generateTestData(dataSet.get("industry"));
			description = fc.utobj().generateTestData(dataSet.get("description"));
			fc.utobj().printTestStep("Navigating to Admin > CRM >  Configure Industry Type");
			fc.utobj().printTestStep("Add Industry");
			industryPage.addIndustry(driver, industry, description);

			String accountName = fc.utobj().generateTestData(dataSet.get("accountName"));
			String visibility = dataSet.get("visibility");
			fc.utobj().printTestStep("Navigating to CRM >  Accounts > Accounts Summary");
			fc.utobj().printTestStep("Add Account");
			accountPage.addAcocunt(driver, industry, accountName, visibility);

			fc.utobj().printTestStep("Navigating to CRM >  Opportunities > Opportunities Summary");
			fc.utobj().printTestStep("Add Contact");

			fc.utobj().clickElement(driver, pobj.opportunitiesLinks);
			fc.utobj().clickElement(driver, pobj.addNew);
			fc.utobj().clickElement(driver, pobj.contactLnk);

			String cFname = fc.utobj().generateTestData(dataSet.get("cFname"));
			String cLname = fc.utobj().generateTestData(dataSet.get("cLname"));
			String primaryCMethod = dataSet.get("primaryCMethod");
			String assignTo = "Corporate";
			String regionName = "";
			String franchiseId = "";
			String title = dataSet.get("title");
			//String email = "crmautomation@staffex.com";
			String email = fc.utobj().generateTestData("crmautomation")+"@staffex.com";
			contactsPage.addContactGenericWithAccount(driver, contactType, cFname, cLname, primaryCMethod, assignTo,
					corpUser.getuserFullName(), regionName, franchiseId, title, email, config, accountName);

			fc.utobj().printTestStep("Navigating to CRM >  Opportunities > Opportunities Summary");
			fc.utobj().printTestStep("Add Opportunity");

			fc.utobj().clickElement(driver, pobj.opportunitiesLinks);
			fc.utobj().clickElement(driver, pobj.addNew);
			fc.utobj().clickElement(driver, pobj.OpportunityLnk);

			String opportunityName = fc.utobj().generateTestData(dataSet.get("opportunityName"));
			String contactName = cFname + " " + cLname;

			String salesAmount = dataSet.get("salesAmount");
			addOpportunityGenric(driver, opportunityName, accountName, contactName, corpUser.getuserFullName(), stage,
					salesAmount, config);

			fc.utobj().clickElement(driver, pobj.opportunitiesLinks);
			opportunityFilter(driver, stage);

			fc.utobj().printTestStep("Add Transaction");
			fc.utobj().actionImgOption(driver, opportunityName, "Add Transaction");

			fc.utobj().sendKeys(driver, pobj.invoiceDate, fc.utobj().getFutureDateUSFormat(-1));
			fc.utobj().clickElement(driver, pobj.selectProductAndService);
			fc.utobj().clickElement(driver, pobj.uncheckAll);
			fc.utobj().sendKeys(driver, pobj.searchProduct, productName);
			fc.utobj().clickElement(driver, pobj.checkAll);
			fc.utobj().clickElement(driver, pobj.okayBtn);
			fc.utobj().clickElement(driver, pobj.submitBtn);

			fc.utobj().clickElement(driver, pobj.opportunitiesLinks);
			opportunityFilter(driver, stage);

			fc.utobj().printTestStep("Verify Transaction");

			fc.utobj().clickLink(driver, opportunityName);
			fc.utobj().clickElement(driver, pobj.transactionTab);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,(".//td[contains(text () , '" + opportunityName + "')]/preceding-sibling::td/a")));

			boolean isProductAvailable = fc.utobj().assertPageSource(driver, productName);
			if (isProductAvailable == false) {
				fc.utobj().throwsException("Not able to Verify Product At CRM > Transactions > Transaction Info Page");
			}

			boolean isCategoryPresent = fc.utobj().assertPageSource(driver, categoryName);
			if (isCategoryPresent == false) {
				fc.utobj().throwsException("Not able to Verify Category At CRM > Transactions > Transaction Info Page");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "crm" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify the Log A Task At CRM > Opportunities > Opportunity Summary", testCaseId = "TC_240_Log_Task")
	public void logATaskActionImage() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());
		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			CRMAccountsPage pobj = new CRMAccountsPage(driver);

			CRMContactsPageTest contactsPage = new CRMContactsPageTest();
			CRMAccountsPageTest accountPage = new CRMAccountsPageTest();

			String description = fc.utobj().generateTestData(dataSet.get("description"));
			fc.utobj().printTestStep("Navigate To Admin Users Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			String emailId = fc.utobj().generateTestData("crmautomation")+"@staffex.com"; //"crmautomation@staffex.com";
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUser = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporateUser.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			fc.utobj().printTestStep("Navigating to Admin > CRM > ContactType Configuration");
			fc.utobj().printTestStep("Add Contact Type");
			AdminCRMContactTypeConfigurationPageTest contactTypePage = new AdminCRMContactTypeConfigurationPageTest();
			String contactType = fc.utobj().generateTestData(dataSet.get("contactType"));
			contactTypePage.addContactType(driver, contactType);
			fc.utobj().printTestStep("Navigating to Admin > CRM >  Configure Opportunity Stages");
			fc.utobj().printTestStep("Add Stage");
			AdminCRMConfigureOpportunityStagesPageTest stagePage = new AdminCRMConfigureOpportunityStagesPageTest();
			String stage = fc.utobj().generateTestData(dataSet.get("stage"));
			stagePage.addStage(driver, stage);

			AdminCRMConfigureIndustryPageTest industryPage = new AdminCRMConfigureIndustryPageTest();
			String industry = fc.utobj().generateTestData(dataSet.get("industry"));
			description = fc.utobj().generateTestData(dataSet.get("description"));
			fc.utobj().printTestStep("Navigating to Admin > CRM >  Configure Industry Type");
			fc.utobj().printTestStep("Add Industry");
			industryPage.addIndustry(driver, industry, description);

			String accountName = fc.utobj().generateTestData(dataSet.get("accountName"));
			String visibility = dataSet.get("visibility");
			fc.utobj().printTestStep("Navigating to CRM >  Accounts > Accounts Summary");
			fc.utobj().printTestStep("Add Account");
			accountPage.addAcocunt(driver, industry, accountName, visibility);

			fc.utobj().printTestStep("Navigating to CRM >  Opportunities > Opportunities Summary");
			fc.utobj().printTestStep("Add Contact");

			fc.utobj().clickElement(driver, pobj.opportunitiesLinks);
			fc.utobj().clickElement(driver, pobj.addNew);
			fc.utobj().clickElement(driver, pobj.contactLnk);

			String cFname = fc.utobj().generateTestData(dataSet.get("cFname"));
			String cLname = fc.utobj().generateTestData(dataSet.get("cLname"));
			String primaryCMethod = dataSet.get("primaryCMethod");
			String assignTo = "Corporate";
			String regionName = "";
			String franchiseId = "";
			String title = dataSet.get("title");
			//String email = "crmautomation@staffex.com";
			String email = fc.utobj().generateTestData("crmautomation")+"@staffex.com";
			contactsPage.addContactGenericWithAccount(driver, contactType, cFname, cLname, primaryCMethod, assignTo,
					corpUser.getuserFullName(), regionName, franchiseId, title, email, config, accountName);

			fc.utobj().printTestStep("Navigating to CRM >  Opportunities > Opportunities Summary");
			fc.utobj().printTestStep("Add Opportunity");

			fc.utobj().clickElement(driver, pobj.opportunitiesLinks);
			fc.utobj().clickElement(driver, pobj.addNew);
			fc.utobj().clickElement(driver, pobj.OpportunityLnk);

			String opportunityName = fc.utobj().generateTestData(dataSet.get("opportunityName"));
			String contactName = cFname + " " + cLname;
			String salesAmount = dataSet.get("salesAmount");
			addOpportunityGenric(driver, opportunityName, accountName, contactName, corpUser.getuserFullName(), stage,
					salesAmount, config);

			fc.utobj().clickElement(driver, pobj.opportunitiesLinks);
			opportunityFilter(driver, stage);

			fc.utobj().printTestStep("Log A Task");
			fc.utobj().actionImgOption(driver, opportunityName, "Log a Task");
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			if (!fc.utobj().isSelected(driver,pobj.radioOwner)) {
				fc.utobj().clickElement(driver, pobj.radioOwner);
			}
			String statusTask = dataSet.get("statusTask");
			fc.utobj().selectDropDown(driver, pobj.statusTask, statusTask);
			String subject = fc.utobj().generateTestData(dataSet.get("subject"));
			fc.utobj().sendKeys(driver, pobj.subject, subject);
			if (!fc.utobj().isSelected(driver, pobj.timeLessTask)) {
				fc.utobj().clickElement(driver, pobj.timeLessTask);
			}
			String priority = dataSet.get("priority");
			fc.utobj().selectDropDown(driver, pobj.priority, priority);
			fc.utobj().sendKeys(driver, pobj.comments, "Test Comment");
			fc.utobj().clickElement(driver, pobj.createBtn);
			fc.utobj().switchFrameToDefault(driver);

			// opportunityFilter(driver, stage);
			/*
			 * fc.utobj().clickElement(driver,
			 * fc.utobj().getElementByXpath(driver,".//a[.='"+opportunityName+
			 * "']")));
			 */
			fc.utobj().clickLink(driver, opportunityName);

			fc.utobj().printTestStep("Verify Task");

			boolean isSubjectPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[contains(text () ,'" + subject + "')]");
			if (isSubjectPresent == false) {
				fc.utobj().throwsException("was not able to verify subject");
			}
			boolean isUserPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[contains(text () ,'" + subject
					+ "')]/ancestor::tr/td/div[.='" + corpUser.getuserFullName() + "']");
			if (isUserPresent == false) {
				fc.utobj().throwsException("was not able to verify userName at Lead Info Page");
			}
			boolean isStatusOfTaskPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[contains(text () ,'" + subject
					+ "')]/ancestor::tr/td[contains(text () ,'" + statusTask + "')]");
			if (isStatusOfTaskPresent == false) {
				fc.utobj().throwsException("was not able to verify status of task");
			}
			boolean isPriorityPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[contains(text () ,'" + subject
					+ "')]/ancestor::tr/td[contains(text () ,'" + priority + "')]");
			if (isPriorityPresent == false) {
				fc.utobj().throwsException("was not able to verify Priority of task");
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "crm1" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify the Change Stage By Action Image At CRM > Opportunities > Opportunity Summary", testCaseId = "TC_241_Change_Stage")
	public void changeStageActionImage() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());
		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			CRMAccountsPage pobj = new CRMAccountsPage(driver);

			CRMContactsPageTest contactsPage = new CRMContactsPageTest();
			CRMAccountsPageTest accountPage = new CRMAccountsPageTest();

			String description = fc.utobj().generateTestData(dataSet.get("description"));

			fc.utobj().printTestStep("Navigate To Admin Users Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			String emailId = fc.utobj().generateTestData("crmautomation")+"@staffex.com";//"crmautomation@staffex.com";
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUser = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporateUser.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			fc.utobj().printTestStep("Navigating to Admin > CRM > ContactType Configuration");
			fc.utobj().printTestStep("Add Contact Type");
			AdminCRMContactTypeConfigurationPageTest contactTypePage = new AdminCRMContactTypeConfigurationPageTest();
			String contactType = fc.utobj().generateTestData(dataSet.get("contactType"));
			contactTypePage.addContactType(driver, contactType);
			fc.utobj().printTestStep("Navigating to Admin > CRM >  Configure Opportunity Stages");
			fc.utobj().printTestStep("Add Stage");
			AdminCRMConfigureOpportunityStagesPageTest stagePage = new AdminCRMConfigureOpportunityStagesPageTest();
			String stage = fc.utobj().generateTestData(dataSet.get("stage"));
			stagePage.addStage(driver, stage);

			String stage1 = fc.utobj().generateTestData(dataSet.get("stage"));
			stagePage.addStage(driver, stage1);

			AdminCRMConfigureIndustryPageTest industryPage = new AdminCRMConfigureIndustryPageTest();
			String industry = fc.utobj().generateTestData(dataSet.get("industry"));
			description = fc.utobj().generateTestData(dataSet.get("description"));
			fc.utobj().printTestStep("Navigating to Admin > CRM >  Configure Industry Type");
			fc.utobj().printTestStep("Add Industry");
			industryPage.addIndustry(driver, industry, description);

			String accountName = fc.utobj().generateTestData(dataSet.get("accountName"));
			String visibility = dataSet.get("visibility");
			fc.utobj().printTestStep("Navigating to CRM >  Accounts > Accounts Summary");
			fc.utobj().printTestStep("Add Account");
			accountPage.addAcocunt(driver, industry, accountName, visibility);

			fc.utobj().printTestStep("Navigating to CRM >  Opportunities > Opportunities Summary");
			fc.utobj().printTestStep("Add Contact");

			fc.utobj().clickElement(driver, pobj.opportunitiesLinks);
			fc.utobj().clickElement(driver, pobj.addNew);
			fc.utobj().clickElement(driver, pobj.contactLnk);

			String cFname = fc.utobj().generateTestData(dataSet.get("cFname"));
			String cLname = fc.utobj().generateTestData(dataSet.get("cLname"));
			String primaryCMethod = dataSet.get("primaryCMethod");
			String assignTo = "Corporate";
			String regionName = "";
			String franchiseId = "";
			String title = dataSet.get("title");
			String email = fc.utobj().generateTestData("crmautomation")+"@staffex.com";//"crmautomation@staffex.com";

			contactsPage.addContactGenericWithAccount(driver, contactType, cFname, cLname, primaryCMethod, assignTo,
					corpUser.getuserFullName(), regionName, franchiseId, title, email, config, accountName);

			fc.utobj().printTestStep("Navigating to CRM >  Opportunities > Opportunities Summary");
			fc.utobj().printTestStep("Add Opportunity");

			fc.utobj().clickElement(driver, pobj.opportunitiesLinks);
			fc.utobj().clickElement(driver, pobj.addNew);
			fc.utobj().clickElement(driver, pobj.OpportunityLnk);

			String opportunityName = fc.utobj().generateTestData(dataSet.get("opportunityName"));
			String contactName = cFname + " " + cLname;
			String salesAmount = dataSet.get("salesAmount");
			addOpportunityGenric(driver, opportunityName, accountName, contactName, corpUser.getuserFullName(), stage,
					salesAmount, config);

			fc.utobj().clickElement(driver, pobj.opportunitiesLinks);
			opportunityFilter(driver, stage);

			fc.utobj().printTestStep("Change Stage");
			fc.utobj().actionImgOption(driver, opportunityName, "Change Stage");
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().selectDropDown(driver, pobj.stage, stage1);
			fc.utobj().clickElement(driver, pobj.changeBtn);
			fc.utobj().clickElement(driver, pobj.closeBtn);
			fc.utobj().switchFrameToDefault(driver);

			opportunityFilter(driver, stage);

			fc.utobj().printTestStep("Verify Stage");

			boolean isOpportunityPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[.='" + opportunityName + "']");
			if (isOpportunityPresent == false) {
				fc.utobj().throwsException("was not able to verify Opportunity Name");
			}
			boolean isAssignToPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[.='" + opportunityName
					+ "']/ancestor::tr/td[contains(text () , '" + corpUser.getuserFullName() + "')]");
			if (isAssignToPresent == false) {
				fc.utobj().throwsException("was not able to verify Assign To");
			}
			boolean isAccountNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[.='" + opportunityName + "']/ancestor::tr/td/a[contains(text () , '" + accountName + "')]");
			if (isAccountNamePresent == false) {
				fc.utobj().throwsException("was not able to verify Account");
			}
			boolean isContactNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[.='" + opportunityName + "']/ancestor::tr/td/a[contains(text () , '" + contactName + "')]");
			if (isContactNamePresent == false) {
				fc.utobj().throwsException("was not able to verify Contact");
			}
			boolean isStagePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[.='" + opportunityName + "']/ancestor::tr/td[contains(text () ,'" + stage1 + "')]");
			if (isStagePresent == false) {
				fc.utobj().throwsException("was not able to verify Stage");
			}
			boolean isSalesAmountPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[.='" + opportunityName
					+ "']/ancestor::tr/td[contains(text () , '" + salesAmount.concat(".00") + "')]");
			if (isSalesAmountPresent == false) {
				fc.utobj().throwsException("was not able to verify Sales Amount");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "crm1")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify the Change Stage By Action Button Option At CRM > Opportunities > Opportunity Summary", testCaseId = "TC_242_Change_Stage")
	public void changeStageActionBtn() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());
		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			CRMAccountsPage pobj = new CRMAccountsPage(driver);

			CRMContactsPageTest contactsPage = new CRMContactsPageTest();
			CRMAccountsPageTest accountPage = new CRMAccountsPageTest();

			String description = fc.utobj().generateTestData(dataSet.get("description"));

			/*
			 * fc.utobj().printTestStep(testCaseId,
			 * "Navigate To Admin Users Manage Corporate User");
			 * fc.utobj().printTestStep(testCaseId, "Add Corporate User");
			 * String emailId="crmautomation@staffex.com";
			 * AdminUsersManageCorporateUsersAddCorporateUserPageTest
			 * corporateUser=new
			 * AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			 * CorporateUser corpUser = new
			 * CorporateUser();corpUser.setEmail(emailId);corpUser =
			 * fc.CommonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			 * corporateUser.
			 * createDefaultCorporateUser_Through_WebService_AfterLogin(driver,
			 * corpUser);
			 */

			fc.utobj().printTestStep("Navigate To Admin Users Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			String emailId = fc.utobj().generateTestData("crmautomation")+"@staffex.com"; //"crmautomation@staffex.com";
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUser = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporateUser.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			fc.utobj().printTestStep("Navigating to Admin > CRM > ContactType Configuration");
			fc.utobj().printTestStep("Add Contact Type");
			AdminCRMContactTypeConfigurationPageTest contactTypePage = new AdminCRMContactTypeConfigurationPageTest();
			String contactType = fc.utobj().generateTestData(dataSet.get("contactType"));
			contactTypePage.addContactType(driver, contactType);
			fc.utobj().printTestStep("Navigating to Admin > CRM >  Configure Opportunity Stages");
			fc.utobj().printTestStep("Add Stage");
			AdminCRMConfigureOpportunityStagesPageTest stagePage = new AdminCRMConfigureOpportunityStagesPageTest();
			String stage = fc.utobj().generateTestData(dataSet.get("stage"));
			stagePage.addStage(driver, stage);

			String stage1 = fc.utobj().generateTestData(dataSet.get("stage"));
			stagePage.addStage(driver, stage1);

			AdminCRMConfigureIndustryPageTest industryPage = new AdminCRMConfigureIndustryPageTest();
			String industry = fc.utobj().generateTestData(dataSet.get("industry"));
			description = fc.utobj().generateTestData(dataSet.get("description"));
			fc.utobj().printTestStep("Navigating to Admin > CRM >  Configure Industry Type");
			fc.utobj().printTestStep("Add Industry");
			industryPage.addIndustry(driver, industry, description);

			String accountName = fc.utobj().generateTestData(dataSet.get("accountName"));
			String visibility = dataSet.get("visibility");
			fc.utobj().printTestStep("Navigating to CRM >  Accounts > Accounts Summary");
			fc.utobj().printTestStep("Add Account");
			accountPage.addAcocunt(driver, industry, accountName, visibility);

			fc.utobj().printTestStep("Navigating to CRM >  Opportunities > Opportunities Summary");
			fc.utobj().printTestStep("Add Contact");

			fc.utobj().clickElement(driver, pobj.opportunitiesLinks);
			fc.utobj().clickElement(driver, pobj.addNew);
			fc.utobj().clickElement(driver, pobj.contactLnk);

			String cFname = fc.utobj().generateTestData(dataSet.get("cFname"));
			String cLname = fc.utobj().generateTestData(dataSet.get("cLname"));
			String primaryCMethod = dataSet.get("primaryCMethod");
			String assignTo = "Corporate";
			String regionName = "";
			String franchiseId = "";
			String title = dataSet.get("title");
			String email = fc.utobj().generateTestData("crmautomation")+"@staffex.com";//"crmautomation@staffex.com";

			contactsPage.addContactGenericWithAccount(driver, contactType, cFname, cLname, primaryCMethod, assignTo,
					corpUser.getuserFullName(), regionName, franchiseId, title, email, config, accountName);
			fc.utobj().printTestStep("Navigating to CRM >  Opportunities > Opportunities Summary");
			fc.utobj().printTestStep("Add Opportunity");

			fc.utobj().clickElement(driver, pobj.opportunitiesLinks);
			fc.utobj().clickElement(driver, pobj.addNew);
			fc.utobj().clickElement(driver, pobj.OpportunityLnk);

			String opportunityName = fc.utobj().generateTestData(dataSet.get("opportunityName"));
			String contactName = cFname + " " + cLname;
			String salesAmount = dataSet.get("salesAmount");
			addOpportunityGenric(driver, opportunityName, accountName, contactName, corpUser.getuserFullName(), stage,
					salesAmount, config);

			fc.utobj().clickElement(driver, pobj.opportunitiesLinks);
			opportunityFilter(driver, stage);

			// change stage
			fc.utobj().printTestStep("Change Stage");
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//a[.='" + opportunityName + "']/ancestor::tr/td/input"));
			fc.utobj().selectActionMenuItemsWithTagA(driver, "Change Stage");

			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().selectDropDown(driver, pobj.stage, stage1);
			fc.utobj().clickElement(driver, pobj.changeBtn);
			fc.utobj().clickElement(driver, pobj.closeBtn);
			fc.utobj().switchFrameToDefault(driver);

			opportunityFilter(driver, stage);
			fc.utobj().printTestStep("Verify Stage");
			boolean isOpportunityPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[.='" + opportunityName + "']");
			if (isOpportunityPresent == false) {
				fc.utobj().throwsException("was not able to verify Opportunity Name");
			}
			boolean isAssignToPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[.='" + opportunityName
					+ "']/ancestor::tr/td[contains(text () , '" + corpUser.getuserFullName() + "')]");
			if (isAssignToPresent == false) {
				fc.utobj().throwsException("was not able to verify Assign To");
			}
			boolean isAccountNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[.='" + opportunityName + "']/ancestor::tr/td/a[contains(text () , '" + accountName + "')]");
			if (isAccountNamePresent == false) {
				fc.utobj().throwsException("was not able to verify Account");
			}
			boolean isContactNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[.='" + opportunityName + "']/ancestor::tr/td/a[contains(text () , '" + contactName + "')]");
			if (isContactNamePresent == false) {
				fc.utobj().throwsException("was not able to verify Contact");
			}
			boolean isStagePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[.='" + opportunityName + "']/ancestor::tr/td[contains(text () ,'" + stage1 + "')]");
			if (isStagePresent == false) {
				fc.utobj().throwsException("was not able to verify Stage");
			}
			boolean isSalesAmountPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[.='" + opportunityName
					+ "']/ancestor::tr/td[contains(text () , '" + salesAmount.concat(".00") + "')]");
			if (isSalesAmountPresent == false) {
				fc.utobj().throwsException("was not able to verify Sales Amount");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = {"crm"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify the Delete Opportunity By Action Btn Option At CRM > Opportunities > Opportunity Summary", testCaseId = "TC_243_Delete_Opportunity")
	public void deleteOpportunityActionBtn() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());
		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			CRMAccountsPage pobj = new CRMAccountsPage(driver);

			CRMContactsPageTest contactsPage = new CRMContactsPageTest();
			CRMAccountsPageTest accountPage = new CRMAccountsPageTest();

			String description = fc.utobj().generateTestData(dataSet.get("description"));

			fc.utobj().printTestStep("Navigate To Admin Users Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			String emailId = fc.utobj().generateTestData("crmautomation")+"@staffex.com";//"crmautomation@staffex.com";
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUser = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporateUser.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			fc.utobj().printTestStep("Navigating to Admin > CRM > ContactType Configuration");
			fc.utobj().printTestStep("Add Contact Type");
			AdminCRMContactTypeConfigurationPageTest contactTypePage = new AdminCRMContactTypeConfigurationPageTest();
			String contactType = fc.utobj().generateTestData(dataSet.get("contactType"));
			contactTypePage.addContactType(driver, contactType);
			fc.utobj().printTestStep("Navigating to Admin > CRM >  Configure Opportunity Stages");
			fc.utobj().printTestStep("Add Stage");
			AdminCRMConfigureOpportunityStagesPageTest stagePage = new AdminCRMConfigureOpportunityStagesPageTest();
			String stage = fc.utobj().generateTestData(dataSet.get("stage"));
			stagePage.addStage(driver, stage);

			AdminCRMConfigureIndustryPageTest industryPage = new AdminCRMConfigureIndustryPageTest();
			String industry = fc.utobj().generateTestData(dataSet.get("industry"));
			description = fc.utobj().generateTestData(dataSet.get("description"));
			fc.utobj().printTestStep("Navigating to Admin > CRM >  Configure Industry Type");
			fc.utobj().printTestStep("Add Industry");
			industryPage.addIndustry(driver, industry, description);

			String accountName = fc.utobj().generateTestData(dataSet.get("accountName"));
			String visibility = dataSet.get("visibility");
			fc.utobj().printTestStep("Navigating to CRM >  Accounts > Accounts Summary");
			fc.utobj().printTestStep("Add Account");
			accountPage.addAcocunt(driver, industry, accountName, visibility);

			fc.utobj().printTestStep("Navigating to CRM >  Opportunities > Opportunities Summary");
			fc.utobj().printTestStep("Add Contact");

			fc.utobj().clickElement(driver, pobj.opportunitiesLinks);
			fc.utobj().clickElement(driver, pobj.addNew);
			fc.utobj().clickElement(driver, pobj.contactLnk);

			String cFname = fc.utobj().generateTestData(dataSet.get("cFname"));
			String cLname = fc.utobj().generateTestData(dataSet.get("cLname"));
			String primaryCMethod = dataSet.get("primaryCMethod");
			String assignTo = "Corporate";
			String regionName = "";
			String franchiseId = "";
			String title = dataSet.get("title");
			//String email = "crmautomation@staffex.com";
			String email = fc.utobj().generateTestData("crmautomation")+"@staffex.com";
			contactsPage.addContactGenericWithAccount(driver, contactType, cFname, cLname, primaryCMethod, assignTo,
					corpUser.getuserFullName(), regionName, franchiseId, title, email, config, accountName);

			fc.utobj().printTestStep("Navigating to CRM >  Opportunities > Opportunities Summary");
			fc.utobj().printTestStep("Add Opportunity");

			fc.utobj().clickElement(driver, pobj.opportunitiesLinks);
			fc.utobj().clickElement(driver, pobj.addNew);
			fc.utobj().clickElement(driver, pobj.OpportunityLnk);

			String opportunityName = fc.utobj().generateTestData(dataSet.get("opportunityName"));
			String contactName = cFname + " " + cLname;
			String salesAmount = dataSet.get("salesAmount");
			addOpportunityGenric(driver, opportunityName, accountName, contactName, corpUser.getuserFullName(), stage,
					salesAmount, config);

			fc.utobj().clickElement(driver, pobj.opportunitiesLinks);
			opportunityFilter(driver, stage);

			// Delete Opportunity
			fc.utobj().printTestStep("Delete Opportinity");
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//a[.='" + opportunityName + "']/ancestor::tr/td/input"));
			fc.utobj().selectActionMenuItemsWithTagA(driver, "Delete");
			fc.utobj().acceptAlertBox(driver);

			opportunityFilter(driver, stage);
			fc.utobj().printTestStep("Verify Delete Opportunity");
			boolean isNoRecordPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//td[.='No records found.']");
			if (isNoRecordPresent == false) {
				fc.utobj().throwsException("was not able to Delete Opportunity");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "crm1")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify the Change Stage By Bottom Btn Option At CRM > Opportunities > Opportunity Summary", testCaseId = "TC_244_Change_Stage")
	public void changeStageBottomBtn() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());
		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			CRMAccountsPage pobj = new CRMAccountsPage(driver);

			CRMContactsPageTest contactsPage = new CRMContactsPageTest();
			CRMAccountsPageTest accountPage = new CRMAccountsPageTest();

			String description = fc.utobj().generateTestData(dataSet.get("description"));

			/*
			 * fc.utobj().printTestStep(testCaseId,
			 * "Navigate To Admin Users Manage Corporate User");
			 * fc.utobj().printTestStep(testCaseId, "Add Corporate User");
			 * String emailId="crmautomation@staffex.com";
			 * AdminUsersManageCorporateUsersAddCorporateUserPageTest
			 * corporateUser=new
			 * AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			 * CorporateUser corpUser = new
			 * CorporateUser();corpUser.setEmail(emailId);corpUser =
			 * fc.CommonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			 * corporateUser.
			 * createDefaultCorporateUser_Through_WebService_AfterLogin(driver,
			 * corpUser);
			 */

			fc.utobj().printTestStep("Navigate To Admin Users Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			String emailId = fc.utobj().generateTestData("crmautomation")+"@staffex.com";   //"crmautomation@staffex.com";
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUser = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporateUser.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			fc.utobj().printTestStep("Navigating to Admin > CRM > ContactType Configuration");
			fc.utobj().printTestStep("Add Contact Type");
			AdminCRMContactTypeConfigurationPageTest contactTypePage = new AdminCRMContactTypeConfigurationPageTest();
			String contactType = fc.utobj().generateTestData(dataSet.get("contactType"));
			contactTypePage.addContactType(driver, contactType);
			fc.utobj().printTestStep("Navigating to Admin > CRM >  Configure Opportunity Stages");
			fc.utobj().printTestStep("Add Stage");
			AdminCRMConfigureOpportunityStagesPageTest stagePage = new AdminCRMConfigureOpportunityStagesPageTest();
			String stage = fc.utobj().generateTestData(dataSet.get("stage"));
			stagePage.addStage(driver, stage);

			String stage1 = fc.utobj().generateTestData(dataSet.get("stage"));
			stagePage.addStage(driver, stage1);

			AdminCRMConfigureIndustryPageTest industryPage = new AdminCRMConfigureIndustryPageTest();
			String industry = fc.utobj().generateTestData(dataSet.get("industry"));
			description = fc.utobj().generateTestData(dataSet.get("description"));
			fc.utobj().printTestStep("Navigating to Admin > CRM >  Configure Industry Type");
			fc.utobj().printTestStep("Add Industry");
			industryPage.addIndustry(driver, industry, description);

			String accountName = fc.utobj().generateTestData(dataSet.get("accountName"));
			String visibility = dataSet.get("visibility");
			fc.utobj().printTestStep("Navigating to CRM >  Accounts > Accounts Summary");
			fc.utobj().printTestStep("Add Account");
			accountPage.addAcocunt(driver, industry, accountName, visibility);

			fc.utobj().printTestStep("Navigating to CRM >  Opportunities > Opportunities Summary");
			fc.utobj().printTestStep("Add Contact");

			fc.utobj().clickElement(driver, pobj.opportunitiesLinks);
			fc.utobj().clickElement(driver, pobj.addNew);
			fc.utobj().clickElement(driver, pobj.contactLnk);

			String cFname = fc.utobj().generateTestData(dataSet.get("cFname"));
			String cLname = fc.utobj().generateTestData(dataSet.get("cLname"));
			String primaryCMethod = dataSet.get("primaryCMethod");
			String assignTo = "Corporate";
			String regionName = "";
			String franchiseId = "";
			String title = dataSet.get("title");
			String email = fc.utobj().generateTestData("crmautomation")+"@staffex.com";   //"crmautomation@staffex.com";

			contactsPage.addContactGenericWithAccount(driver, contactType, cFname, cLname, primaryCMethod, assignTo,
					corpUser.getuserFullName(), regionName, franchiseId, title, email, config, accountName);

			fc.utobj().printTestStep("Navigating to CRM >  Opportunities > Opportunities Summary");
			fc.utobj().printTestStep("Add Opportunity");

			fc.utobj().clickElement(driver, pobj.opportunitiesLinks);
			fc.utobj().clickElement(driver, pobj.addNew);
			fc.utobj().clickElement(driver, pobj.OpportunityLnk);

			String opportunityName = fc.utobj().generateTestData(dataSet.get("opportunityName"));
			String contactName = cFname + " " + cLname;
			String salesAmount = dataSet.get("salesAmount");
			addOpportunityGenric(driver, opportunityName, accountName, contactName, corpUser.getuserFullName(), stage,
					salesAmount, config);

			fc.utobj().clickElement(driver, pobj.opportunitiesLinks);
			opportunityFilter(driver, stage);

			// change stage
			fc.utobj().printTestStep("Change Stage");
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//a[.='" + opportunityName + "']/ancestor::tr/td/input"));
			fc.utobj().clickElement(driver, pobj.cahngeStageBtmBtn);

			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().selectDropDown(driver, pobj.stage, stage1);
			fc.utobj().clickElement(driver, pobj.changeBtn);
			fc.utobj().clickElement(driver, pobj.closeBtn);
			fc.utobj().switchFrameToDefault(driver);

			opportunityFilter(driver, stage);
			fc.utobj().printTestStep("Verify Change Stage");
			boolean isOpportunityPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[.='" + opportunityName + "']");
			if (isOpportunityPresent == false) {
				fc.utobj().throwsException("was not able to verify Opportunity Name");
			}
			boolean isAssignToPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[.='" + opportunityName
					+ "']/ancestor::tr/td[contains(text () , '" + corpUser.getuserFullName() + "')]");
			if (isAssignToPresent == false) {
				fc.utobj().throwsException("was not able to verify Assign To");
			}
			boolean isAccountNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[.='" + opportunityName + "']/ancestor::tr/td/a[contains(text () , '" + accountName + "')]");
			if (isAccountNamePresent == false) {
				fc.utobj().throwsException("was not able to verify Account");
			}
			boolean isContactNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[.='" + opportunityName + "']/ancestor::tr/td/a[contains(text () , '" + contactName + "')]");
			if (isContactNamePresent == false) {
				fc.utobj().throwsException("was not able to verify Contact");
			}
			boolean isStagePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[.='" + opportunityName + "']/ancestor::tr/td[contains(text () ,'" + stage1 + "')]");
			if (isStagePresent == false) {
				fc.utobj().throwsException("was not able to verify Stage");
			}
			boolean isSalesAmountPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[.='" + opportunityName
					+ "']/ancestor::tr/td[contains(text () , '" + salesAmount.concat(".00") + "')]");
			if (isSalesAmountPresent == false) {
				fc.utobj().throwsException("was not able to verify Sales Amount");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = {"crm"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify the Delete Opportunity By Action button Option At CRM > Opportunities > Opportunity Summary ", testCaseId = "TC_245_Delete_Opportunity")
	public void deleteOpportunityBottomBtn() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			CRMAccountsPage pobj = new CRMAccountsPage(driver);

			CRMContactsPageTest contactsPage = new CRMContactsPageTest();
			CRMAccountsPageTest accountPage = new CRMAccountsPageTest();

			String description = fc.utobj().generateTestData(dataSet.get("description"));

			/*
			 * fc.utobj().printTestStep(testCaseId,
			 * "Navigate To Admin Users Manage Corporate User");
			 * fc.utobj().printTestStep(testCaseId, "Add Corporate User");
			 * String emailId="crmautomation@staffex.com";
			 * AdminUsersManageCorporateUsersAddCorporateUserPageTest
			 * corporateUser=new
			 * AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			 * CorporateUser corpUser = new
			 * CorporateUser();corpUser.setEmail(emailId);corpUser =
			 * fc.CommonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			 * corporateUser.
			 * createDefaultCorporateUser_Through_WebService_AfterLogin(driver,
			 * corpUser);
			 */

			fc.utobj().printTestStep("Navigate To Admin Users Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			String emailId = fc.utobj().generateTestData("crmautomation")+"@staffex.com"; //"crmautomation@staffex.com";
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUser = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporateUser.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			fc.utobj().printTestStep("Navigating to Admin > CRM > ContactType Configuration");
			fc.utobj().printTestStep("Add Contact Type");
			AdminCRMContactTypeConfigurationPageTest contactTypePage = new AdminCRMContactTypeConfigurationPageTest();
			String contactType = fc.utobj().generateTestData(dataSet.get("contactType"));
			contactTypePage.addContactType(driver, contactType);
			fc.utobj().printTestStep("Navigating to Admin > CRM >  Configure Opportunity Stages");
			fc.utobj().printTestStep("Add Stage");
			AdminCRMConfigureOpportunityStagesPageTest stagePage = new AdminCRMConfigureOpportunityStagesPageTest();
			String stage = fc.utobj().generateTestData(dataSet.get("stage"));
			stagePage.addStage(driver, stage);

			AdminCRMConfigureIndustryPageTest industryPage = new AdminCRMConfigureIndustryPageTest();
			String industry = fc.utobj().generateTestData(dataSet.get("industry"));
			description = fc.utobj().generateTestData(dataSet.get("description"));
			fc.utobj().printTestStep("Navigating to Admin > CRM >  Configure Industry Type");
			fc.utobj().printTestStep("Add Industry");
			industryPage.addIndustry(driver, industry, description);

			String accountName = fc.utobj().generateTestData(dataSet.get("accountName"));
			String visibility = dataSet.get("visibility");
			fc.utobj().printTestStep("Navigating to CRM >  Accounts > Accounts Summary");
			fc.utobj().printTestStep("Add Account");
			accountPage.addAcocunt(driver, industry, accountName, visibility);

			fc.utobj().printTestStep("Navigating to CRM >  Opportunities > Opportunities Summary");
			fc.utobj().printTestStep("Add Contact");

			fc.utobj().clickElement(driver, pobj.opportunitiesLinks);
			fc.utobj().clickElement(driver, pobj.addNew);
			fc.utobj().clickElement(driver, pobj.contactLnk);

			String cFname = fc.utobj().generateTestData(dataSet.get("cFname"));
			String cLname = fc.utobj().generateTestData(dataSet.get("cLname"));
			String primaryCMethod = dataSet.get("primaryCMethod");
			String assignTo = "Corporate";
			String regionName = "";
			String franchiseId = "";
			String title = dataSet.get("title");
			//String email = "crmautomation@staffex.com";
			String email = fc.utobj().generateTestData("crmautomation")+"@staffex.com";
			contactsPage.addContactGenericWithAccount(driver, contactType, cFname, cLname, primaryCMethod, assignTo,
					corpUser.getuserFullName(), regionName, franchiseId, title, email, config, accountName);

			fc.utobj().printTestStep("Navigating to CRM >  Opportunities > Opportunities Summary");
			fc.utobj().printTestStep("Add Opportunity");

			fc.utobj().clickElement(driver, pobj.opportunitiesLinks);
			fc.utobj().clickElement(driver, pobj.addNew);
			fc.utobj().clickElement(driver, pobj.OpportunityLnk);

			String opportunityName = fc.utobj().generateTestData(dataSet.get("opportunityName"));
			String contactName = cFname + " " + cLname;
			String salesAmount = dataSet.get("salesAmount");
			addOpportunityGenric(driver, opportunityName, accountName, contactName, corpUser.getuserFullName(), stage,
					salesAmount, config);

			fc.utobj().clickElement(driver, pobj.opportunitiesLinks);
			opportunityFilter(driver, stage);
			fc.utobj().sleep(1000);
			// Delete Opportunity
			fc.utobj().printTestStep("Delete Opportunity");
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//a[.='" + opportunityName + "']/ancestor::tr/td/input"));
			fc.utobj().clickElement(driver, pobj.deleteBtmBtn);
			fc.utobj().acceptAlertBox(driver);
			fc.utobj().sleep(1000);
			opportunityFilter(driver, stage);
			fc.utobj().printTestStep("Verify Delete Opprtunity");
			boolean isNoRecordPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//td[.='No records found.']");
			if (isNoRecordPresent == false) {
				fc.utobj().throwsException("was not able to Delete Opportunity");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "crm")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify the Modify Opportunity By Modify Tab At CRM > Opportunities > Opportunity Info", testCaseId = "TC_246_Modify_Opportunity")
	public void modifyOpportunityTab() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());
		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			CRMAccountsPage pobj = new CRMAccountsPage(driver);
			CRMAccountsPageTest accountPage = new CRMAccountsPageTest();
			CRMContactsPageTest contactsPage = new CRMContactsPageTest();

			/*
			 * fc.utobj().printTestStep(testCaseId,
			 * "Navigate To Admin Users Manage Corporate User");
			 * fc.utobj().printTestStep(testCaseId, "Add Corporate User");
			 * String emailId="crmautomation@staffex.com";
			 * AdminUsersManageCorporateUsersAddCorporateUserPageTest
			 * corporateUser=new
			 * AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			 * CorporateUser corpUser = new
			 * CorporateUser();corpUser.setEmail(emailId);corpUser =
			 * fc.CommonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			 * corporateUser.
			 * createDefaultCorporateUser_Through_WebService_AfterLogin(driver,
			 * corpUser);
			 */

			fc.utobj().printTestStep("Navigate To Admin Users Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			String emailId = "crmautomation@staffex.com";
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUser = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporateUser.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			fc.utobj().printTestStep("Navigating to Admin > CRM > ContactType Configuration");
			fc.utobj().printTestStep("Add Contact Type");
			AdminCRMContactTypeConfigurationPageTest contactTypePage = new AdminCRMContactTypeConfigurationPageTest();
			String contactType = fc.utobj().generateTestData(dataSet.get("contactType"));
			contactTypePage.addContactType(driver, contactType);
			fc.utobj().printTestStep("Navigating to Admin > CRM >  Configure Opportunity Stages");
			fc.utobj().printTestStep("Add Stage");
			AdminCRMConfigureOpportunityStagesPageTest stagePage = new AdminCRMConfigureOpportunityStagesPageTest();
			String stage = fc.utobj().generateTestData(dataSet.get("stage"));
			stagePage.addStage(driver, stage);

			AdminCRMConfigureIndustryPageTest industryPage = new AdminCRMConfigureIndustryPageTest();
			String industry = fc.utobj().generateTestData(dataSet.get("industry"));
			String description = fc.utobj().generateTestData(dataSet.get("description"));
			fc.utobj().printTestStep("Navigating to Admin > CRM >  Configure Industry Type");
			fc.utobj().printTestStep("Add Industry");
			industryPage.addIndustry(driver, industry, description);

			String accountName = fc.utobj().generateTestData(dataSet.get("accountName"));
			String visibility = dataSet.get("visibility");
			fc.utobj().printTestStep("Navigating to CRM >  Accounts > Accounts Summary");
			fc.utobj().printTestStep("Add Account");
			accountPage.addAcocunt(driver, industry, accountName, visibility);

			fc.utobj().printTestStep("Navigating to CRM >  Opportunities > Opportunities Summary");
			fc.utobj().printTestStep("Add Contact");

			fc.utobj().clickElement(driver, pobj.opportunitiesLinks);
			fc.utobj().clickElement(driver, pobj.addNew);
			fc.utobj().clickElement(driver, pobj.contactLnk);

			String cFname = fc.utobj().generateTestData(dataSet.get("cFname"));
			String cLname = fc.utobj().generateTestData(dataSet.get("cLname"));
			String primaryCMethod = dataSet.get("primaryCMethod");
			String assignTo = "Corporate";
			String regionName = "";
			String franchiseId = "";
			String title = dataSet.get("title");
			String email = fc.utobj().generateTestData("crmautomation")+"@staffex.com";//"crmautomation@staffex.com";

			contactsPage.addContactGenericWithAccount(driver, contactType, cFname, cLname, primaryCMethod, assignTo,
					corpUser.getuserFullName(), regionName, franchiseId, title, email, config, accountName);

			fc.utobj().printTestStep("Navigating to CRM >  Opportunities > Opportunities Summary");
			fc.utobj().printTestStep("Add Opportunity");

			fc.utobj().clickElement(driver, pobj.opportunitiesLinks);
			fc.utobj().clickElement(driver, pobj.addNew);
			fc.utobj().clickElement(driver, pobj.OpportunityLnk);

			String opportunityName = fc.utobj().generateTestData(dataSet.get("opportunityName"));
			String contactName = cFname + " " + cLname;
			String salesAmount = dataSet.get("salesAmount");
			addOpportunityGenric(driver, opportunityName, accountName, contactName, corpUser.getuserFullName(), stage,
					salesAmount, config);

			fc.utobj().clickElement(driver, pobj.opportunitiesLinks);
			opportunityFilter(driver, stage);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//a[.='" + opportunityName + "']"));

			fc.utobj().printTestStep("Modify Opportunity");

			fc.utobj().clickElement(driver, pobj.modifyTab);
			opportunityName = fc.utobj().generateTestData(dataSet.get("opportunityName"));
			fc.utobj().clickElement(driver, pobj.saveBtn);

			fc.utobj().printTestStep("Verify Modify Opprtunity");

			boolean isoppPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () , 'Opportunity Name')]/ancestor::tr/td[.='" + opportunityName + "']");
			if (isoppPresent == false) {
				fc.utobj().throwsException("was not able to verify Opportunity");
			}

			boolean isAccountPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () , 'Account')]/ancestor::tr/td/a[.='" + accountName + "']");
			if (isAccountPresent == false) {
				fc.utobj().throwsException("was not able to verify Account");
			}

			boolean isContactPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () , './/td[contains(text () , 'Contact ')]/ancestor::tr/td/a[.='" + cFname
							+ " " + cLname + "']");
			if (isContactPresent == false) {
				fc.utobj().throwsException("was not able to verify Contact");
			}

			boolean isAssignToPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () , 'Assign To')]/ancestor::tr/td[.='" + corpUser.getuserFullName() + "']");
			if (isAssignToPresent == false) {
				fc.utobj().throwsException("was not able to verify Assign");
			}

			boolean isStagePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () , './/td[contains(text () , 'Stage ')]/ancestor::tr/td[.='" + stage + "']");
			if (isStagePresent == false) {
				fc.utobj().throwsException("was not able to verify Stage");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = {"crm"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify the Delete Opportunity By Delete Tab At CRM > Opportunities > Opportunity Info", testCaseId = "TC_247_Delete_Opportunity")
	public void deleteOpportunityTab() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());
		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			CRMAccountsPage pobj = new CRMAccountsPage(driver);
			CRMAccountsPageTest accountPage = new CRMAccountsPageTest();
			CRMContactsPageTest contactsPage = new CRMContactsPageTest();

			fc.utobj().printTestStep("Navigate To Admin Users Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			String emailId = fc.utobj().generateTestData("crmautomation")+"@staffex.com";//"crmautomation@staffex.com";
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUser = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporateUser.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			fc.utobj().printTestStep("Navigating to Admin > CRM > ContactType Configuration");
			fc.utobj().printTestStep("Add Contact Type");
			AdminCRMContactTypeConfigurationPageTest contactTypePage = new AdminCRMContactTypeConfigurationPageTest();
			String contactType = fc.utobj().generateTestData(dataSet.get("contactType"));
			contactTypePage.addContactType(driver, contactType);
			fc.utobj().printTestStep("Navigating to Admin > CRM >  Configure Opportunity Stages");
			fc.utobj().printTestStep("Add Stage");
			AdminCRMConfigureOpportunityStagesPageTest stagePage = new AdminCRMConfigureOpportunityStagesPageTest();
			String stage = fc.utobj().generateTestData(dataSet.get("stage"));
			stagePage.addStage(driver, stage);

			AdminCRMConfigureIndustryPageTest industryPage = new AdminCRMConfigureIndustryPageTest();
			String industry = fc.utobj().generateTestData(dataSet.get("industry"));
			String description = fc.utobj().generateTestData(dataSet.get("description"));
			fc.utobj().printTestStep("Navigating to Admin > CRM >  Configure Industry Type");
			fc.utobj().printTestStep("Add Industry");
			industryPage.addIndustry(driver, industry, description);

			String accountName = fc.utobj().generateTestData(dataSet.get("accountName"));
			String visibility = dataSet.get("visibility");
			fc.utobj().printTestStep("Navigating to CRM >  Accounts > Accounts Summary");
			fc.utobj().printTestStep("Add Account");
			accountPage.addAcocunt(driver, industry, accountName, visibility);

			fc.utobj().printTestStep("Navigating to CRM >  Opportunities > Opportunities Summary");
			fc.utobj().printTestStep("Add Contact");

			fc.utobj().clickElement(driver, pobj.opportunitiesLinks);
			fc.utobj().clickElement(driver, pobj.addNew);
			fc.utobj().clickElement(driver, pobj.contactLnk);

			String cFname = fc.utobj().generateTestData(dataSet.get("cFname"));
			String cLname = fc.utobj().generateTestData(dataSet.get("cLname"));
			String primaryCMethod = dataSet.get("primaryCMethod");
			String assignTo = "Corporate";
			String regionName = "";
			String franchiseId = "";
			String title = dataSet.get("title");
			String email = fc.utobj().generateTestData("crmautomation")+"@staffex.com";  //"crmautomation@staffex.com";

			contactsPage.addContactGenericWithAccount(driver, contactType, cFname, cLname, primaryCMethod, assignTo,
					corpUser.getuserFullName(), regionName, franchiseId, title, email, config, accountName);

			fc.utobj().printTestStep("Navigating to CRM >  Opportunities > Opportunities Summary");
			fc.utobj().printTestStep("Add Opportunity");

			fc.utobj().clickElement(driver, pobj.opportunitiesLinks);
			fc.utobj().clickElement(driver, pobj.addNew);
			fc.utobj().clickElement(driver, pobj.OpportunityLnk);

			String opportunityName = fc.utobj().generateTestData(dataSet.get("opportunityName"));
			String contactName = cFname + " " + cLname;
			String salesAmount = dataSet.get("salesAmount");
			addOpportunityGenric(driver, opportunityName, accountName, contactName, corpUser.getuserFullName(), stage,
					salesAmount, config);

			fc.utobj().clickElement(driver, pobj.opportunitiesLinks);
			opportunityFilter(driver, stage);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//a[.='" + opportunityName + "']"));

			fc.utobj().printTestStep("Delete Opportunity");

			fc.utobj().clickElement(driver, pobj.deleteTab);
			fc.utobj().acceptAlertBox(driver);
			opportunityFilter(driver, stage);
			fc.utobj().printTestStep("Verify Delete Opportunity");

			boolean isNoRecordPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//td[.='No records found.']");
			if (isNoRecordPresent == false) {
				fc.utobj().throwsException("was not able to Delete Opportunity");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "crm")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify the Log A Task By Log A Task Tab At CRM > Opportunities > Opportunity Info", testCaseId = "TC_248_Log_A_Task")
	public void logATaskTab() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			CRMAccountsPage pobj = new CRMAccountsPage(driver);
			CRMAccountsPageTest accountPage = new CRMAccountsPageTest();
			CRMContactsPageTest contactsPage = new CRMContactsPageTest();

			/*
			 * fc.utobj().printTestStep(testCaseId,
			 * "Navigate To Admin Users Manage Corporate User");
			 * fc.utobj().printTestStep(testCaseId, "Add Corporate User");
			 * String emailId="crmautomation@staffex.com";
			 * AdminUsersManageCorporateUsersAddCorporateUserPageTest
			 * corporateUser=new
			 * AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			 * CorporateUser corpUser = new
			 * CorporateUser();corpUser.setEmail(emailId);corpUser =
			 * fc.CommonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			 * corporateUser.
			 * createDefaultCorporateUser_Through_WebService_AfterLogin(driver,
			 * corpUser);
			 */

			fc.utobj().printTestStep("Navigate To Admin Users Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			String emailId = fc.utobj().generateTestData("crmautomation")+"@staffex.com";  //"crmautomation@staffex.com";
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUser = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporateUser.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			fc.utobj().printTestStep("Navigating to Admin > CRM > ContactType Configuration");
			fc.utobj().printTestStep("Add Contact Type");
			AdminCRMContactTypeConfigurationPageTest contactTypePage = new AdminCRMContactTypeConfigurationPageTest();
			String contactType = fc.utobj().generateTestData(dataSet.get("contactType"));
			contactTypePage.addContactType(driver, contactType);
			fc.utobj().printTestStep("Navigating to Admin > CRM >  Configure Opportunity Stages");
			fc.utobj().printTestStep("Add Stage");
			AdminCRMConfigureOpportunityStagesPageTest stagePage = new AdminCRMConfigureOpportunityStagesPageTest();
			String stage = fc.utobj().generateTestData(dataSet.get("stage"));
			stagePage.addStage(driver, stage);

			AdminCRMConfigureIndustryPageTest industryPage = new AdminCRMConfigureIndustryPageTest();
			String industry = fc.utobj().generateTestData(dataSet.get("industry"));
			String description = fc.utobj().generateTestData(dataSet.get("description"));
			fc.utobj().printTestStep("Navigating to Admin > CRM >  Configure Industry Type");
			fc.utobj().printTestStep("Add Industry");
			industryPage.addIndustry(driver, industry, description);

			String accountName = fc.utobj().generateTestData(dataSet.get("accountName"));
			String visibility = dataSet.get("visibility");
			fc.utobj().printTestStep("Navigating to CRM >  Accounts > Accounts Summary");
			fc.utobj().printTestStep("Add Account");
			accountPage.addAcocunt(driver, industry, accountName, visibility);

			fc.utobj().printTestStep("Navigating to CRM >  Opportunities > Opportunities Summary");
			fc.utobj().printTestStep("Add Contact");

			fc.utobj().clickElement(driver, pobj.opportunitiesLinks);
			fc.utobj().clickElement(driver, pobj.addNew);
			fc.utobj().clickElement(driver, pobj.contactLnk);

			String cFname = fc.utobj().generateTestData(dataSet.get("cFname"));
			String cLname = fc.utobj().generateTestData(dataSet.get("cLname"));
			String primaryCMethod = dataSet.get("primaryCMethod");
			String assignTo = "Corporate";
			String regionName = "";
			String franchiseId = "";
			String title = dataSet.get("title");
			//String email = "crmautomation@staffex.com";
			String email = fc.utobj().generateTestData("crmautomation")+"@staffex.com";
			contactsPage.addContactGenericWithAccount(driver, contactType, cFname, cLname, primaryCMethod, assignTo,
					corpUser.getuserFullName(), regionName, franchiseId, title, email, config, accountName);

			fc.utobj().printTestStep("Navigating to CRM >  Opportunities > Opportunities Summary");
			fc.utobj().printTestStep("Add Opportunity");

			fc.utobj().clickElement(driver, pobj.opportunitiesLinks);
			fc.utobj().clickElement(driver, pobj.addNew);
			fc.utobj().clickElement(driver, pobj.OpportunityLnk);

			String opportunityName = fc.utobj().generateTestData(dataSet.get("opportunityName"));
			String contactName = cFname + " " + cLname;
			String salesAmount = dataSet.get("salesAmount");
			addOpportunityGenric(driver, opportunityName, accountName, contactName, corpUser.getuserFullName(), stage,
					salesAmount, config);

			fc.utobj().clickElement(driver, pobj.opportunitiesLinks);
			opportunityFilter(driver, stage);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//a[.='" + opportunityName + "']"));

			// Log A Task
			fc.utobj().printTestStep("Log A Task");
			fc.utobj().clickElement(driver, pobj.logATaskTab);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			if (!fc.utobj().isSelected(driver,pobj.radioOwner)) {
				fc.utobj().clickElement(driver, pobj.radioOwner);
			}
			String statusTask = dataSet.get("statusTask");
			fc.utobj().selectDropDown(driver, pobj.statusTask, statusTask);
			String subject = fc.utobj().generateTestData(dataSet.get("subject"));
			fc.utobj().sendKeys(driver, pobj.subject, subject);
			if (!fc.utobj().isSelected(driver, pobj.timeLessTask)) {
				fc.utobj().clickElement(driver, pobj.timeLessTask);
			}
			String priority = dataSet.get("priority");
			fc.utobj().selectDropDown(driver, pobj.priority, priority);
			fc.utobj().sendKeys(driver, pobj.comments, "Test Comment");
			fc.utobj().clickElement(driver, pobj.createBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Verify A Task");

			boolean isSubjectPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[contains(text () ,'" + subject + "')]");
			if (isSubjectPresent == false) {
				fc.utobj().throwsException("was not able to verify subject");
			}
			boolean isUserPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[contains(text () ,'" + subject
					+ "')]/ancestor::tr/td/div[.='" + corpUser.getuserFullName() + "']");
			if (isUserPresent == false) {
				fc.utobj().throwsException("was not able to verify userName at Lead Info Page");
			}
			boolean isStatusOfTaskPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[contains(text () ,'" + subject
					+ "')]/ancestor::tr/td[contains(text () ,'" + statusTask + "')]");
			if (isStatusOfTaskPresent == false) {
				fc.utobj().throwsException("was not able to verify status of task");
			}
			boolean isPriorityPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[contains(text () ,'" + subject
					+ "')]/ancestor::tr/td[contains(text () ,'" + priority + "')]");
			if (isPriorityPresent == false) {
				fc.utobj().throwsException("was not able to verify Priority of task");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "crm1")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify the Log A Call By Log A Call Tab At CRM > Opportunities > Opportunity Info", testCaseId = "TC_249_Log_Call")
	public void logACallTab() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());
		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			CRMAccountsPage pobj = new CRMAccountsPage(driver);

			CRMAccountsPageTest accountPage = new CRMAccountsPageTest();
			CRMContactsPageTest contactsPage = new CRMContactsPageTest();

			/*
			 * fc.utobj().printTestStep(testCaseId,
			 * "Navigate To Admin Users Manage Corporate User");
			 * fc.utobj().printTestStep(testCaseId, "Add Corporate User");
			 * String emailId="crmautomation@staffex.com";
			 * AdminUsersManageCorporateUsersAddCorporateUserPageTest
			 * corporateUser=new
			 * AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			 * CorporateUser corpUser = new
			 * CorporateUser();corpUser.setEmail(emailId);corpUser =
			 * fc.CommonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			 * corporateUser.
			 * createDefaultCorporateUser_Through_WebService_AfterLogin(driver,
			 * corpUser);
			 */

			fc.utobj().printTestStep("Navigate To Admin Users Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			String emailId = fc.utobj().generateTestData("crmautomation")+"@staffex.com"; //"crmautomation@staffex.com";
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUser = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporateUser.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			fc.utobj().printTestStep("Navigating to Admin > CRM > ContactType Configuration");
			fc.utobj().printTestStep("Add Contact Type");
			AdminCRMContactTypeConfigurationPageTest contactTypePage = new AdminCRMContactTypeConfigurationPageTest();
			String contactType = fc.utobj().generateTestData(dataSet.get("contactType"));
			contactTypePage.addContactType(driver, contactType);

			fc.utobj().printTestStep("Navigating to Admin > CRM >  Configure Opportunity Stages");
			fc.utobj().printTestStep("Add Stage");
			AdminCRMConfigureOpportunityStagesPageTest stagePage = new AdminCRMConfigureOpportunityStagesPageTest();
			String stage = fc.utobj().generateTestData(dataSet.get("stage"));
			stagePage.addStage(driver, stage);

			AdminCRMConfigureIndustryPageTest industryPage = new AdminCRMConfigureIndustryPageTest();
			String industry = fc.utobj().generateTestData(dataSet.get("industry"));
			String description = fc.utobj().generateTestData(dataSet.get("description"));
			fc.utobj().printTestStep("Navigating to Admin > CRM >  Configure Industry Type");
			fc.utobj().printTestStep("Add Industry");
			industryPage.addIndustry(driver, industry, description);

			String accountName = fc.utobj().generateTestData(dataSet.get("accountName"));
			String visibility = dataSet.get("visibility");
			fc.utobj().printTestStep("Navigating to CRM >  Accounts > Accounts Summary");
			fc.utobj().printTestStep("Add Account");
			accountPage.addAcocunt(driver, industry, accountName, visibility);

			fc.utobj().printTestStep("Navigating to CRM >  Opportunities > Opportunities Summary");
			fc.utobj().printTestStep("Add Contact");

			fc.utobj().clickElement(driver, pobj.opportunitiesLinks);
			fc.utobj().clickElement(driver, pobj.addNew);
			fc.utobj().clickElement(driver, pobj.contactLnk);

			String cFname = fc.utobj().generateTestData(dataSet.get("cFname"));
			String cLname = fc.utobj().generateTestData(dataSet.get("cLname"));
			String primaryCMethod = dataSet.get("primaryCMethod");
			String assignTo = "Corporate";
			String regionName = "";
			String franchiseId = "";
			String title = dataSet.get("title");
			String email = fc.utobj().generateTestData("crmautomation")+"@staffex.com";   //"crmautomation@staffex.com";

			contactsPage.addContactGenericWithAccount(driver, contactType, cFname, cLname, primaryCMethod, assignTo,
					corpUser.getuserFullName(), regionName, franchiseId, title, email, config, accountName);

			fc.utobj().printTestStep("Navigating to CRM >  Opportunities > Opportunities Summary");
			fc.utobj().printTestStep("Add Opportunity");

			fc.utobj().clickElement(driver, pobj.opportunitiesLinks);
			fc.utobj().clickElement(driver, pobj.addNew);
			fc.utobj().clickElement(driver, pobj.OpportunityLnk);

			String opportunityName = fc.utobj().generateTestData(dataSet.get("opportunityName"));
			String contactName = cFname + " " + cLname;
			String salesAmount = dataSet.get("salesAmount");
			addOpportunityGenric(driver, opportunityName, accountName, contactName, corpUser.getuserFullName(), stage,
					salesAmount, config);

			fc.utobj().clickElement(driver, pobj.opportunitiesLinks);
			opportunityFilter(driver, stage);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//a[.='" + opportunityName + "']"));

			// Log A Call
			fc.utobj().printTestStep("Log A Call");
			fc.utobj().clickElement(driver, pobj.logACallTab);

			String callSubject = fc.utobj().generateTestData(dataSet.get("subject"));
			fc.utobj().sendKeys(driver, pobj.callSubject, callSubject);
			String callDate = fc.utobj().getCurrentDateUSFormat();
			fc.utobj().sendKeys(driver, pobj.callDate, callDate);
			String callStatus = dataSet.get("callStatus");
			fc.utobj().selectDropDown(driver, pobj.callStatus, callStatus);
			String cType = dataSet.get("callType");
			fc.utobj().selectDropDown(driver, pobj.communicationType, cType);
			fc.utobj().clickElement(driver, pobj.saveBtn);

			fc.utobj().clickElement(driver, pobj.detailedHistoryLink);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().clickElement(driver, pobj.callsTabDH);

			fc.utobj().printTestStep("Verify A Call Detailed History Frame");

			boolean isCallSubjectPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () ,'" + callSubject + "')]");
			if (isCallSubjectPresent == false) {
				fc.utobj().throwsException("was not able to verify Call Subject");
			}
			boolean isCommunicationTypePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () ,'" + cType + "')]");
			if (isCommunicationTypePresent == false) {
				fc.utobj().throwsException("was not able to verify communication type");
			}
			fc.utobj().clickElement(driver, pobj.closeBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = {"crm"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify the Add Remarks By Remarks Tab At CRM > Opportunities > Opportunity Info", testCaseId = "TC_250_Add_Remarks")
	public void addRemarksTab() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			CRMAccountsPage pobj = new CRMAccountsPage(driver);
			CRMAccountsPageTest accountPage = new CRMAccountsPageTest();
			CRMContactsPageTest contactsPage = new CRMContactsPageTest();

			/*
			 * fc.utobj().printTestStep(testCaseId,
			 * "Navigate To Admin Users Manage Corporate User");
			 * fc.utobj().printTestStep(testCaseId, "Add Corporate User");
			 * String emailId="crmautomation@staffex.com";
			 * AdminUsersManageCorporateUsersAddCorporateUserPageTest
			 * corporateUser=new
			 * AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			 * CorporateUser corpUser = new
			 * CorporateUser();corpUser.setEmail(emailId);corpUser =
			 * fc.CommonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			 * corporateUser.
			 * createDefaultCorporateUser_Through_WebService_AfterLogin(driver,
			 * corpUser);
			 */

			fc.utobj().printTestStep("Navigate To Admin Users Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			String emailId = fc.utobj().generateTestData("crmautomation")+"@staffex.com";  // "crmautomation@staffex.com";
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUser = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporateUser.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			fc.utobj().printTestStep("Navigating to Admin > CRM > ContactType Configuration");
			fc.utobj().printTestStep("Add Contact Type");
			AdminCRMContactTypeConfigurationPageTest contactTypePage = new AdminCRMContactTypeConfigurationPageTest();
			String contactType = fc.utobj().generateTestData(dataSet.get("contactType"));
			contactTypePage.addContactType(driver, contactType);
			fc.utobj().printTestStep("Navigating to Admin > CRM >  Configure Opportunity Stages");
			fc.utobj().printTestStep("Add Stage");
			AdminCRMConfigureOpportunityStagesPageTest stagePage = new AdminCRMConfigureOpportunityStagesPageTest();
			String stage = fc.utobj().generateTestData(dataSet.get("stage"));
			stagePage.addStage(driver, stage);

			AdminCRMConfigureIndustryPageTest industryPage = new AdminCRMConfigureIndustryPageTest();
			String industry = fc.utobj().generateTestData(dataSet.get("industry"));
			String description = fc.utobj().generateTestData(dataSet.get("description"));
			fc.utobj().printTestStep("Navigating to Admin > CRM >  Configure Industry Type");
			fc.utobj().printTestStep("Add Industry");
			industryPage.addIndustry(driver, industry, description);

			String accountName = fc.utobj().generateTestData(dataSet.get("accountName"));
			String visibility = dataSet.get("visibility");
			fc.utobj().printTestStep("Navigating to CRM >  Accounts > Accounts Summary");
			fc.utobj().printTestStep("Add Account");
			accountPage.addAcocunt(driver, industry, accountName, visibility);

			fc.utobj().printTestStep("Navigating to CRM >  Opportunities > Opportunities Summary");
			fc.utobj().printTestStep("Add Contact");

			fc.utobj().clickElement(driver, pobj.opportunitiesLinks);
			fc.utobj().clickElement(driver, pobj.addNew);
			fc.utobj().clickElement(driver, pobj.contactLnk);

			String cFname = fc.utobj().generateTestData(dataSet.get("cFname"));
			String cLname = fc.utobj().generateTestData(dataSet.get("cLname"));
			String primaryCMethod = dataSet.get("primaryCMethod");
			String assignTo = "Corporate";
			String regionName = "";
			String franchiseId = "";
			String title = dataSet.get("title");
			//String email = "crmautomation@staffex.com";
			String email = fc.utobj().generateTestData("crmautomation")+"@staffex.com";
			contactsPage.addContactGenericWithAccount(driver, contactType, cFname, cLname, primaryCMethod, assignTo,
					corpUser.getuserFullName(), regionName, franchiseId, title, email, config, accountName);

			fc.utobj().printTestStep("Navigating to CRM >  Opportunities > Opportunities Summary");
			fc.utobj().printTestStep("Add Opportunity");

			fc.utobj().clickElement(driver, pobj.opportunitiesLinks);
			fc.utobj().clickElement(driver, pobj.addNew);
			fc.utobj().clickElement(driver, pobj.OpportunityLnk);

			String opportunityName = fc.utobj().generateTestData(dataSet.get("opportunityName"));
			String contactName = cFname + " " + cLname;
			String salesAmount = dataSet.get("salesAmount");
			addOpportunityGenric(driver, opportunityName, accountName, contactName, corpUser.getuserFullName(), stage,
					salesAmount, config);

			fc.utobj().clickElement(driver, pobj.opportunitiesLinks);
			opportunityFilter(driver, stage);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//a[.='" + opportunityName + "']"));

			// Add Remarks
			fc.utobj().printTestStep("Add Remarks");
			fc.utobj().clickElement(driver, pobj.addRemraksTab);

			String remarks = fc.utobj().generateTestData(dataSet.get("remarkField"));
			fc.utobj().sendKeys(driver, pobj.remarkField, remarks);
			fc.utobj().clickElement(driver, pobj.saveBtn);

			fc.utobj().printTestStep("Verify Remarks");

			boolean isRemarksPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[@title='Remark']/span[.='" + remarks + "']");
			if (isRemarksPresent == false) {
				fc.utobj().throwsException("was not able to verify remarks");
			}

			fc.utobj().printTestStep("Verify Remarks At Detailed History Frame");

			fc.utobj().clickElement(driver, pobj.detailedHistoryLink);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().clickElement(driver, pobj.remarksTabAtCbox);
			boolean isRemarksAtCboxPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () ,'" + remarks + "')]");
			if (isRemarksAtCboxPresent == false) {
				fc.utobj().throwsException("was not able to verify Remarks At Detailed History Cbox");
			}
			fc.utobj().clickElement(driver, pobj.closeBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "crm")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify the Log A Task By Link At CRM > Opportunities > Opportunity Info", testCaseId = "TC_251_Log_A_Task")
	public void logATaskLink() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			CRMAccountsPage pobj = new CRMAccountsPage(driver);
			CRMAccountsPageTest accountPage = new CRMAccountsPageTest();
			CRMContactsPageTest contactsPage = new CRMContactsPageTest();

			/*
			 * fc.utobj().printTestStep(testCaseId,
			 * "Navigate To Admin Users Manage Corporate User");
			 * fc.utobj().printTestStep(testCaseId, "Add Corporate User");
			 * String emailId="crmautomation@staffex.com";
			 * AdminUsersManageCorporateUsersAddCorporateUserPageTest
			 * corporateUser=new
			 * AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			 * CorporateUser corpUser = new
			 * CorporateUser();corpUser.setEmail(emailId);corpUser =
			 * fc.CommonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			 * corporateUser.
			 * createDefaultCorporateUser_Through_WebService_AfterLogin(driver,
			 * corpUser);
			 */

			fc.utobj().printTestStep("Navigate To Admin Users Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			String emailId = fc.utobj().generateTestData("crmautomation")+"@staffex.com";  //"crmautomation@staffex.com";
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUser = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporateUser.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			fc.utobj().printTestStep("Navigating to Admin > CRM > ContactType Configuration");
			fc.utobj().printTestStep("Add Contact Type");
			AdminCRMContactTypeConfigurationPageTest contactTypePage = new AdminCRMContactTypeConfigurationPageTest();
			String contactType = fc.utobj().generateTestData(dataSet.get("contactType"));
			contactTypePage.addContactType(driver, contactType);
			fc.utobj().printTestStep("Navigating to Admin > CRM >  Configure Opportunity Stages");
			fc.utobj().printTestStep("Add Stage");
			AdminCRMConfigureOpportunityStagesPageTest stagePage = new AdminCRMConfigureOpportunityStagesPageTest();
			String stage = fc.utobj().generateTestData(dataSet.get("stage"));
			stagePage.addStage(driver, stage);

			AdminCRMConfigureIndustryPageTest industryPage = new AdminCRMConfigureIndustryPageTest();
			String industry = fc.utobj().generateTestData(dataSet.get("industry"));
			String description = fc.utobj().generateTestData(dataSet.get("description"));
			fc.utobj().printTestStep("Navigating to Admin > CRM >  Configure Industry Type");
			fc.utobj().printTestStep("Add Industry");
			industryPage.addIndustry(driver, industry, description);

			String accountName = fc.utobj().generateTestData(dataSet.get("accountName"));
			String visibility = dataSet.get("visibility");
			fc.utobj().printTestStep("Navigating to CRM >  Accounts > Accounts Summary");
			fc.utobj().printTestStep("Add Account");
			accountPage.addAcocunt(driver, industry, accountName, visibility);

			fc.utobj().printTestStep("Navigating to CRM >  Opportunities > Opportunities Summary");
			fc.utobj().printTestStep("Add Contact");

			fc.utobj().clickElement(driver, pobj.opportunitiesLinks);
			fc.utobj().clickElement(driver, pobj.addNew);
			fc.utobj().clickElement(driver, pobj.contactLnk);

			String cFname = fc.utobj().generateTestData(dataSet.get("cFname"));
			String cLname = fc.utobj().generateTestData(dataSet.get("cLname"));
			String primaryCMethod = dataSet.get("primaryCMethod");
			String assignTo = "Corporate";
			String regionName = "";
			String franchiseId = "";
			String title = dataSet.get("title");
			//String email = "crmautomation@staffex.com";
			String email = fc.utobj().generateTestData("crmautomation")+"@staffex.com";
			contactsPage.addContactGenericWithAccount(driver, contactType, cFname, cLname, primaryCMethod, assignTo,
					corpUser.getuserFullName(), regionName, franchiseId, title, email, config, accountName);

			fc.utobj().printTestStep("Navigating to CRM >  Opportunities > Opportunities Summary");
			fc.utobj().printTestStep("Add Opportunity");

			fc.utobj().clickElement(driver, pobj.opportunitiesLinks);
			fc.utobj().clickElement(driver, pobj.addNew);
			fc.utobj().clickElement(driver, pobj.OpportunityLnk);

			String opportunityName = fc.utobj().generateTestData(dataSet.get("opportunityName"));
			String contactName = cFname + " " + cLname;
			String salesAmount = dataSet.get("salesAmount");
			addOpportunityGenric(driver, opportunityName, accountName, contactName, corpUser.getuserFullName(), stage,
					salesAmount, config);

			fc.utobj().clickElement(driver, pobj.opportunitiesLinks);
			opportunityFilter(driver, stage);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//a[.='" + opportunityName + "']"));

			// Log A Task
			fc.utobj().printTestStep("Log A Task");
			fc.utobj().clickElement(driver, pobj.logATaskLinks);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			if (!fc.utobj().isSelected(driver,pobj.radioOwner)) {
				fc.utobj().clickElement(driver, pobj.radioOwner);
			}
			String statusTask = dataSet.get("statusTask");
			fc.utobj().selectDropDown(driver, pobj.statusTask, statusTask);
			String subject = fc.utobj().generateTestData(dataSet.get("subject"));
			fc.utobj().sendKeys(driver, pobj.subject, subject);
			if (!fc.utobj().isSelected(driver, pobj.timeLessTask)) {
				fc.utobj().clickElement(driver, pobj.timeLessTask);
			}
			String priority = dataSet.get("priority");
			fc.utobj().selectDropDown(driver, pobj.priority, priority);
			fc.utobj().sendKeys(driver, pobj.comments, "Test Comment");
			fc.utobj().clickElement(driver, pobj.createBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Verify A Task");

			boolean isSubjectPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[contains(text () ,'" + subject + "')]");
			if (isSubjectPresent == false) {
				fc.utobj().throwsException("was not able to verify subject");
			}
			boolean isUserPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[contains(text () ,'" + subject
					+ "')]/ancestor::tr/td/div[.='" + corpUser.getuserFullName() + "']");
			if (isUserPresent == false) {
				fc.utobj().throwsException("was not able to verify userName at Lead Info Page");
			}
			boolean isStatusOfTaskPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[contains(text () ,'" + subject
					+ "')]/ancestor::tr/td[contains(text () ,'" + statusTask + "')]");
			if (isStatusOfTaskPresent == false) {
				fc.utobj().throwsException("was not able to verify status of task");
			}
			boolean isPriorityPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[contains(text () ,'" + subject
					+ "')]/ancestor::tr/td[contains(text () ,'" + priority + "')]");
			if (isPriorityPresent == false) {
				fc.utobj().throwsException("was not able to verify Priority of task");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "crm")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify the Modify A Task By Link At CRM > Opportunities > Opportunity Info", testCaseId = "TC_252_Modify_Task")
	public void modifyTaskActionImage() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			CRMAccountsPage pobj = new CRMAccountsPage(driver);
			CRMAccountsPageTest accountPage = new CRMAccountsPageTest();
			CRMContactsPageTest contactsPage = new CRMContactsPageTest();

			/*
			 * fc.utobj().printTestStep(testCaseId,
			 * "Navigate To Admin Users Manage Corporate User");
			 * fc.utobj().printTestStep(testCaseId, "Add Corporate User");
			 * String emailId="crmautomation@staffex.com";
			 * AdminUsersManageCorporateUsersAddCorporateUserPageTest
			 * corporateUser=new
			 * AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			 * CorporateUser corpUser = new
			 * CorporateUser();corpUser.setEmail(emailId);corpUser =
			 * fc.CommonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			 * corporateUser.
			 * createDefaultCorporateUser_Through_WebService_AfterLogin(driver,
			 * corpUser);
			 */

			fc.utobj().printTestStep("Navigate To Admin Users Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			String emailId = fc.utobj().generateTestData("crmautomation")+"@staffex.com";  // "crmautomation@staffex.com";
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUser = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporateUser.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			fc.utobj().printTestStep("Navigating to Admin > CRM > ContactType Configuration");
			fc.utobj().printTestStep("Add Contact Type");
			AdminCRMContactTypeConfigurationPageTest contactTypePage = new AdminCRMContactTypeConfigurationPageTest();
			String contactType = fc.utobj().generateTestData(dataSet.get("contactType"));
			contactTypePage.addContactType(driver, contactType);
			fc.utobj().printTestStep("Navigating to Admin > CRM >  Configure Opportunity Stages");
			fc.utobj().printTestStep("Add Stage");
			AdminCRMConfigureOpportunityStagesPageTest stagePage = new AdminCRMConfigureOpportunityStagesPageTest();
			String stage = fc.utobj().generateTestData(dataSet.get("stage"));
			stagePage.addStage(driver, stage);

			AdminCRMConfigureIndustryPageTest industryPage = new AdminCRMConfigureIndustryPageTest();
			String industry = fc.utobj().generateTestData(dataSet.get("industry"));
			String description = fc.utobj().generateTestData(dataSet.get("description"));
			fc.utobj().printTestStep("Navigating to Admin > CRM >  Configure Industry Type");
			fc.utobj().printTestStep("Add Industry");
			industryPage.addIndustry(driver, industry, description);

			String accountName = fc.utobj().generateTestData(dataSet.get("accountName"));
			String visibility = dataSet.get("visibility");
			fc.utobj().printTestStep("Navigating to CRM >  Accounts > Accounts Summary");
			fc.utobj().printTestStep("Add Account");
			accountPage.addAcocunt(driver, industry, accountName, visibility);

			fc.utobj().printTestStep("Navigating to CRM >  Opportunities > Opportunities Summary");
			fc.utobj().printTestStep("Add Contact");

			fc.utobj().clickElement(driver, pobj.opportunitiesLinks);
			fc.utobj().clickElement(driver, pobj.addNew);
			fc.utobj().clickElement(driver, pobj.contactLnk);

			String cFname = fc.utobj().generateTestData(dataSet.get("cFname"));
			String cLname = fc.utobj().generateTestData(dataSet.get("cLname"));
			String primaryCMethod = dataSet.get("primaryCMethod");
			String assignTo = "Corporate";
			String regionName = "";
			String franchiseId = "";
			String title = dataSet.get("title");
			//String email = "crmautomation@staffex.com";
			String email = fc.utobj().generateTestData("crmautomation")+"@staffex.com";
			contactsPage.addContactGenericWithAccount(driver, contactType, cFname, cLname, primaryCMethod, assignTo,
					corpUser.getuserFullName(), regionName, franchiseId, title, email, config, accountName);

			fc.utobj().clickElement(driver, pobj.opportunitiesLinks);
			fc.utobj().clickElement(driver, pobj.addNew);
			fc.utobj().clickElement(driver, pobj.OpportunityLnk);

			String opportunityName = fc.utobj().generateTestData(dataSet.get("opportunityName"));
			String contactName = cFname + " " + cLname;
			String salesAmount = dataSet.get("salesAmount");
			addOpportunityGenric(driver, opportunityName, accountName, contactName, corpUser.getuserFullName(), stage,
					salesAmount, config);

			fc.utobj().clickElement(driver, pobj.opportunitiesLinks);
			opportunityFilter(driver, stage);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//a[.='" + opportunityName + "']"));

			// Log A Task
			fc.utobj().printTestStep("Log A Task");
			fc.utobj().clickElement(driver, pobj.logATaskLinks);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			if (!fc.utobj().isSelected(driver,pobj.radioOwner)) {
				fc.utobj().clickElement(driver, pobj.radioOwner);
			}
			String statusTask = dataSet.get("statusTask");
			fc.utobj().selectDropDown(driver, pobj.statusTask, statusTask);
			String subject = fc.utobj().generateTestData(dataSet.get("subject"));
			fc.utobj().sendKeys(driver, pobj.subject, subject);
			if (!fc.utobj().isSelected(driver, pobj.timeLessTask)) {
				fc.utobj().clickElement(driver, pobj.timeLessTask);
			}
			String priority = dataSet.get("priority");
			fc.utobj().selectDropDown(driver, pobj.priority, priority);
			fc.utobj().sendKeys(driver, pobj.comments, "Test Comment");
			fc.utobj().clickElement(driver, pobj.createBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Modify A Task");

			contactsPage.actionImgOption(driver, subject, "Modify");
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			subject = fc.utobj().generateTestData(dataSet.get("subject"));
			fc.utobj().sendKeys(driver, pobj.subject, subject);
			fc.utobj().clickElement(driver, pobj.saveBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Verify Task");

			boolean isSubjectPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[contains(text () ,'" + subject + "')]");
			if (isSubjectPresent == false) {
				fc.utobj().throwsException("was not able to verify subject");
			}
			boolean isUserPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[contains(text () ,'" + subject
					+ "')]/ancestor::tr/td/div[.='" + corpUser.getuserFullName() + "']");
			if (isUserPresent == false) {
				fc.utobj().throwsException("was not able to verify userName at Lead Info Page");
			}
			boolean isStatusOfTaskPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[contains(text () ,'" + subject
					+ "')]/ancestor::tr/td[contains(text () ,'" + statusTask + "')]");
			if (isStatusOfTaskPresent == false) {
				fc.utobj().throwsException("was not able to verify status of task");
			}
			boolean isPriorityPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[contains(text () ,'" + subject
					+ "')]/ancestor::tr/td[contains(text () ,'" + priority + "')]");
			if (isPriorityPresent == false) {
				fc.utobj().throwsException("was not able to verify Priority of task");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "crm")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify the Delete A Task By Link At CRM > Opportunities > Opportunity Info", testCaseId = "TC_253_Delete_Task")
	public void deleteTaskActionImage() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			CRMAccountsPage pobj = new CRMAccountsPage(driver);
			CRMAccountsPageTest accountPage = new CRMAccountsPageTest();
			CRMContactsPageTest contactsPage = new CRMContactsPageTest();

			/*
			 * fc.utobj().printTestStep(testCaseId,
			 * "Navigate To Admin Users Manage Corporate User");
			 * fc.utobj().printTestStep(testCaseId, "Add Corporate User");
			 * String emailId="crmautomation@staffex.com";
			 * AdminUsersManageCorporateUsersAddCorporateUserPageTest
			 * corporateUser=new
			 * AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			 * CorporateUser corpUser = new
			 * CorporateUser();corpUser.setEmail(emailId);corpUser =
			 * fc.CommonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			 * corporateUser.
			 * createDefaultCorporateUser_Through_WebService_AfterLogin(driver,
			 * corpUser);
			 */

			fc.utobj().printTestStep("Navigate To Admin Users Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			String emailId = fc.utobj().generateTestData("crmautomation")+"@staffex.com";  //"crmautomation@staffex.com";
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUser = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporateUser.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			fc.utobj().printTestStep("Navigating to Admin > CRM > ContactType Configuration");
			fc.utobj().printTestStep("Add Contact Type");
			AdminCRMContactTypeConfigurationPageTest contactTypePage = new AdminCRMContactTypeConfigurationPageTest();
			String contactType = fc.utobj().generateTestData(dataSet.get("contactType"));
			contactTypePage.addContactType(driver, contactType);
			fc.utobj().printTestStep("Navigating to Admin > CRM >  Configure Opportunity Stages");
			fc.utobj().printTestStep("Add Stage");
			AdminCRMConfigureOpportunityStagesPageTest stagePage = new AdminCRMConfigureOpportunityStagesPageTest();
			String stage = fc.utobj().generateTestData(dataSet.get("stage"));
			stagePage.addStage(driver, stage);

			AdminCRMConfigureIndustryPageTest industryPage = new AdminCRMConfigureIndustryPageTest();
			String industry = fc.utobj().generateTestData(dataSet.get("industry"));
			String description = fc.utobj().generateTestData(dataSet.get("description"));
			fc.utobj().printTestStep("Navigating to Admin > CRM >  Configure Industry Type");
			fc.utobj().printTestStep("Add Industry");
			industryPage.addIndustry(driver, industry, description);

			String accountName = fc.utobj().generateTestData(dataSet.get("accountName"));
			String visibility = dataSet.get("visibility");
			fc.utobj().printTestStep("Navigating to CRM >  Accounts > Accounts Summary");
			fc.utobj().printTestStep("Add Account");
			accountPage.addAcocunt(driver, industry, accountName, visibility);

			fc.utobj().printTestStep("Navigating to CRM >  Opportunities > Opportunities Summary");
			fc.utobj().printTestStep("Add Contact");

			fc.utobj().clickElement(driver, pobj.opportunitiesLinks);
			fc.utobj().clickElement(driver, pobj.addNew);
			fc.utobj().clickElement(driver, pobj.contactLnk);

			String cFname = fc.utobj().generateTestData(dataSet.get("cFname"));
			String cLname = fc.utobj().generateTestData(dataSet.get("cLname"));
			String primaryCMethod = dataSet.get("primaryCMethod");
			String assignTo = "Corporate";
			String regionName = "";
			String franchiseId = "";
			String title = dataSet.get("title");
			String email = fc.utobj().generateTestData("crmautomation")+"@staffex.com";
			//String email = "crmautomation@staffex.com";

			contactsPage.addContactGenericWithAccount(driver, contactType, cFname, cLname, primaryCMethod, assignTo,
					corpUser.getuserFullName(), regionName, franchiseId, title, email, config, accountName);

			fc.utobj().printTestStep("Navigating to CRM >  Opportunities > Opportunities Summary");
			fc.utobj().printTestStep("Add Opportunity");

			fc.utobj().clickElement(driver, pobj.opportunitiesLinks);
			fc.utobj().clickElement(driver, pobj.addNew);
			fc.utobj().clickElement(driver, pobj.OpportunityLnk);

			String opportunityName = fc.utobj().generateTestData(dataSet.get("opportunityName"));
			String contactName = cFname + " " + cLname;
			String salesAmount = dataSet.get("salesAmount");
			addOpportunityGenric(driver, opportunityName, accountName, contactName, corpUser.getuserFullName(), stage,
					salesAmount, config);

			fc.utobj().clickElement(driver, pobj.opportunitiesLinks);
			opportunityFilter(driver, stage);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//a[.='" + opportunityName + "']"));

			// Log A Task
			fc.utobj().printTestStep("Log A Task");
			fc.utobj().clickElement(driver, pobj.logATaskLinks);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			if (!fc.utobj().isSelected(driver,pobj.radioOwner)) {
				fc.utobj().clickElement(driver, pobj.radioOwner);
			}
			String statusTask = dataSet.get("statusTask");
			fc.utobj().selectDropDown(driver, pobj.statusTask, statusTask);
			String subject = fc.utobj().generateTestData(dataSet.get("subject"));
			fc.utobj().sendKeys(driver, pobj.subject, subject);
			if (!fc.utobj().isSelected(driver, pobj.timeLessTask)) {
				fc.utobj().clickElement(driver, pobj.timeLessTask);
			}
			String priority = dataSet.get("priority");
			fc.utobj().selectDropDown(driver, pobj.priority, priority);
			fc.utobj().sendKeys(driver, pobj.comments, "Test Comment");
			fc.utobj().clickElement(driver, pobj.createBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Delete A Task");

			contactsPage.actionImgOption(driver, subject, "Delete");
			fc.utobj().acceptAlertBox(driver);

			boolean isNoRecordPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//td[.='No records found.']");
			if (isNoRecordPresent == false) {
				fc.utobj().throwsException("was not able to Delete Task");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "crm1")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify the Process A Task By Link At CRM > Opportunities > Opportunity Info", testCaseId = "TC_254_Process_Task")
	public void processTaskActionImage() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			CRMAccountsPage pobj = new CRMAccountsPage(driver);
			CRMAccountsPageTest accountPage = new CRMAccountsPageTest();
			CRMContactsPageTest contactsPage = new CRMContactsPageTest();

			/*
			 * fc.utobj().printTestStep(testCaseId,
			 * "Navigate To Admin Users Manage Corporate User");
			 * fc.utobj().printTestStep(testCaseId, "Add Corporate User");
			 * String emailId="crmautomation@staffex.com";
			 * AdminUsersManageCorporateUsersAddCorporateUserPageTest
			 * corporateUser=new
			 * AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			 * CorporateUser corpUser = new
			 * CorporateUser();corpUser.setEmail(emailId);corpUser =
			 * fc.CommonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			 * corporateUser.
			 * createDefaultCorporateUser_Through_WebService_AfterLogin(driver,
			 * corpUser);
			 */

			fc.utobj().printTestStep("Navigate To Admin Users Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			String emailId = fc.utobj().generateTestData("crmautomation")+"@staffex.com";  //"crmautomation@staffex.com";
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUser = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporateUser.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			fc.utobj().printTestStep("Navigating to Admin > CRM > ContactType Configuration");
			fc.utobj().printTestStep("Add Contact Type");
			AdminCRMContactTypeConfigurationPageTest contactTypePage = new AdminCRMContactTypeConfigurationPageTest();
			String contactType = fc.utobj().generateTestData(dataSet.get("contactType"));
			contactTypePage.addContactType(driver, contactType);
			fc.utobj().printTestStep("Navigating to Admin > CRM >  Configure Opportunity Stages");
			fc.utobj().printTestStep("Add Stage");
			AdminCRMConfigureOpportunityStagesPageTest stagePage = new AdminCRMConfigureOpportunityStagesPageTest();
			String stage = fc.utobj().generateTestData(dataSet.get("stage"));
			stagePage.addStage(driver, stage);

			AdminCRMConfigureIndustryPageTest industryPage = new AdminCRMConfigureIndustryPageTest();
			String industry = fc.utobj().generateTestData(dataSet.get("industry"));
			String description = fc.utobj().generateTestData(dataSet.get("description"));
			fc.utobj().printTestStep("Navigating to Admin > CRM >  Configure Industry Type");
			fc.utobj().printTestStep("Add Industry");
			industryPage.addIndustry(driver, industry, description);

			String accountName = fc.utobj().generateTestData(dataSet.get("accountName"));
			String visibility = dataSet.get("visibility");
			fc.utobj().printTestStep("Navigating to CRM >  Accounts > Accounts Summary");
			fc.utobj().printTestStep("Add Account");
			accountPage.addAcocunt(driver, industry, accountName, visibility);

			fc.utobj().printTestStep("Navigating to CRM >  Opportunities > Opportunities Summary");
			fc.utobj().printTestStep("Add Contact");

			fc.utobj().clickElement(driver, pobj.opportunitiesLinks);
			fc.utobj().clickElement(driver, pobj.addNew);
			fc.utobj().clickElement(driver, pobj.contactLnk);

			String cFname = fc.utobj().generateTestData(dataSet.get("cFname"));
			String cLname = fc.utobj().generateTestData(dataSet.get("cLname"));
			String primaryCMethod = dataSet.get("primaryCMethod");
			String assignTo = "Corporate";
			String regionName = "";
			String franchiseId = "";
			String title = dataSet.get("title");
			String email = "crmautomation@staffex.com";

			contactsPage.addContactGenericWithAccount(driver, contactType, cFname, cLname, primaryCMethod, assignTo,
					corpUser.getuserFullName(), regionName, franchiseId, title, email, config, accountName);

			fc.utobj().printTestStep("Navigating to CRM >  Opportunities > Opportunities Summary");
			fc.utobj().printTestStep("Add Opportunity");

			fc.utobj().clickElement(driver, pobj.opportunitiesLinks);
			fc.utobj().clickElement(driver, pobj.addNew);
			fc.utobj().clickElement(driver, pobj.OpportunityLnk);

			String opportunityName = fc.utobj().generateTestData(dataSet.get("opportunityName"));
			String contactName = cFname + " " + cLname;
			String salesAmount = dataSet.get("salesAmount");
			addOpportunityGenric(driver, opportunityName, accountName, contactName, corpUser.getuserFullName(), stage,
					salesAmount, config);

			fc.utobj().clickElement(driver, pobj.opportunitiesLinks);
			opportunityFilter(driver, stage);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//a[.='" + opportunityName + "']"));

			// Log A Task
			fc.utobj().printTestStep("Log A Task");
			fc.utobj().clickElement(driver, pobj.logATaskLinks);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			if (!fc.utobj().isSelected(driver,pobj.radioOwner)) {
				fc.utobj().clickElement(driver, pobj.radioOwner);
			}
			String statusTask = dataSet.get("statusTask");
			fc.utobj().selectDropDown(driver, pobj.statusTask, statusTask);
			String subject = fc.utobj().generateTestData(dataSet.get("subject"));
			fc.utobj().sendKeys(driver, pobj.subject, subject);
			if (!fc.utobj().isSelected(driver, pobj.timeLessTask)) {
				fc.utobj().clickElement(driver, pobj.timeLessTask);
			}
			String priority = dataSet.get("priority");
			fc.utobj().selectDropDown(driver, pobj.priority, priority);
			fc.utobj().sendKeys(driver, pobj.comments, "Test Comment");
			fc.utobj().clickElement(driver, pobj.createBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Process A Task");

			contactsPage.actionImgOption(driver, subject, "Process");
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			String statusTaskM = dataSet.get("statusTaskM");
			fc.utobj().selectDropDown(driver, pobj.statusTask, statusTaskM);
			fc.utobj().clickElement(driver, pobj.processTask);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Verify Process Task");

			boolean isSubjectPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[contains(text () ,'" + subject + "')]");
			if (isSubjectPresent == false) {
				fc.utobj().throwsException("was not able to verify subject");
			}
			boolean isUserPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[contains(text () ,'" + subject
					+ "')]/ancestor::tr/td/div[.='" + corpUser.getuserFullName() + "']");
			if (isUserPresent == false) {
				fc.utobj().throwsException("was not able to verify userName at Lead Info Page");
			}
			boolean isStatusOfTaskPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[contains(text () ,'" + subject
					+ "')]/ancestor::tr/td[contains(text () ,'" + statusTaskM + "')]");
			if (isStatusOfTaskPresent == false) {
				fc.utobj().throwsException("was not able to verify status of task");
			}
			boolean isPriorityPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[contains(text () ,'" + subject
					+ "')]/ancestor::tr/td[contains(text () ,'" + priority + "')]");
			if (isPriorityPresent == false) {
				fc.utobj().throwsException("was not able to verify Priority of task");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "crm1")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Modify Task By Bottom Button At CRM > Opportunities > Opportunity Info", testCaseId = "TC_255_Modify_Task")
	public void modifyTaskBottomBtn() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			CRMAccountsPage pobj = new CRMAccountsPage(driver);
			CRMAccountsPageTest accountPage = new CRMAccountsPageTest();
			CRMContactsPageTest contactsPage = new CRMContactsPageTest();

			/*
			 * fc.utobj().printTestStep(testCaseId,
			 * "Navigate To Admin Users Manage Corporate User");
			 * fc.utobj().printTestStep(testCaseId, "Add Corporate User");
			 * String emailId="crmautomation@staffex.com";
			 * AdminUsersManageCorporateUsersAddCorporateUserPageTest
			 * corporateUser=new
			 * AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			 * CorporateUser corpUser = new
			 * CorporateUser();corpUser.setEmail(emailId);corpUser =
			 * fc.CommonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			 * corporateUser.
			 * createDefaultCorporateUser_Through_WebService_AfterLogin(driver,
			 * corpUser);
			 */

			fc.utobj().printTestStep("Navigate To Admin Users Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			String emailId = fc.utobj().generateTestData("crmautomation")+"@staffex.com";  //"crmautomation@staffex.com";
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUser = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporateUser.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			fc.utobj().printTestStep("Navigating to Admin > CRM > ContactType Configuration");
			fc.utobj().printTestStep("Add Contact Type");
			AdminCRMContactTypeConfigurationPageTest contactTypePage = new AdminCRMContactTypeConfigurationPageTest();
			String contactType = fc.utobj().generateTestData(dataSet.get("contactType"));
			contactTypePage.addContactType(driver, contactType);
			fc.utobj().printTestStep("Navigating to Admin > CRM >  Configure Opportunity Stages");
			fc.utobj().printTestStep("Add Stage");
			AdminCRMConfigureOpportunityStagesPageTest stagePage = new AdminCRMConfigureOpportunityStagesPageTest();
			String stage = fc.utobj().generateTestData(dataSet.get("stage"));
			stagePage.addStage(driver, stage);

			AdminCRMConfigureIndustryPageTest industryPage = new AdminCRMConfigureIndustryPageTest();
			String industry = fc.utobj().generateTestData(dataSet.get("industry"));
			String description = fc.utobj().generateTestData(dataSet.get("description"));
			fc.utobj().printTestStep("Navigating to Admin > CRM >  Configure Industry Type");
			fc.utobj().printTestStep("Add Industry");
			industryPage.addIndustry(driver, industry, description);

			String accountName = fc.utobj().generateTestData(dataSet.get("accountName"));
			String visibility = dataSet.get("visibility");
			fc.utobj().printTestStep("Navigating to CRM >  Accounts > Accounts Summary");
			fc.utobj().printTestStep("Add Account");
			accountPage.addAcocunt(driver, industry, accountName, visibility);

			fc.utobj().printTestStep("Navigating to CRM >  Opportunities > Opportunities Summary");
			fc.utobj().printTestStep("Add Contact");

			fc.utobj().clickElement(driver, pobj.opportunitiesLinks);
			fc.utobj().clickElement(driver, pobj.addNew);
			fc.utobj().clickElement(driver, pobj.contactLnk);

			String cFname = fc.utobj().generateTestData(dataSet.get("cFname"));
			String cLname = fc.utobj().generateTestData(dataSet.get("cLname"));
			String primaryCMethod = dataSet.get("primaryCMethod");
			String assignTo = "Corporate";
			String regionName = "";
			String franchiseId = "";
			String title = dataSet.get("title");
			String email = fc.utobj().generateTestData("crmautomation")+"@staffex.com"; //"crmautomation@staffex.com";

			contactsPage.addContactGenericWithAccount(driver, contactType, cFname, cLname, primaryCMethod, assignTo,
					corpUser.getuserFullName(), regionName, franchiseId, title, email, config, accountName);

			fc.utobj().printTestStep("Navigating to CRM >  Opportunities > Opportunities Summary");
			fc.utobj().printTestStep("Add Opportunity");

			fc.utobj().clickElement(driver, pobj.opportunitiesLinks);
			fc.utobj().clickElement(driver, pobj.addNew);
			fc.utobj().clickElement(driver, pobj.OpportunityLnk);

			String opportunityName = fc.utobj().generateTestData(dataSet.get("opportunityName"));
			String contactName = cFname + " " + cLname;
			String salesAmount = dataSet.get("salesAmount");
			addOpportunityGenric(driver, opportunityName, accountName, contactName, corpUser.getuserFullName(), stage,
					salesAmount, config);

			fc.utobj().clickElement(driver, pobj.opportunitiesLinks);
			opportunityFilter(driver, stage);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//a[.='" + opportunityName + "']"));

			// Log A Task
			fc.utobj().printTestStep("Log A Task");
			fc.utobj().clickElement(driver, pobj.logATaskLinks);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			if (!fc.utobj().isSelected(driver,pobj.radioOwner)) {
				fc.utobj().clickElement(driver, pobj.radioOwner);
			}
			String statusTask = dataSet.get("statusTask");
			fc.utobj().selectDropDown(driver, pobj.statusTask, statusTask);
			String subject = fc.utobj().generateTestData(dataSet.get("subject"));
			fc.utobj().sendKeys(driver, pobj.subject, subject);
			if (!fc.utobj().isSelected(driver, pobj.timeLessTask)) {
				fc.utobj().clickElement(driver, pobj.timeLessTask);
			}
			String priority = dataSet.get("priority");
			fc.utobj().selectDropDown(driver, pobj.priority, priority);
			fc.utobj().sendKeys(driver, pobj.comments, "Test Comment");
			fc.utobj().clickElement(driver, pobj.createBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Modify Task");

			fc.utobj().clickElement(driver, pobj.modifyTabBtm);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			subject = fc.utobj().generateTestData(dataSet.get("subject"));
			fc.utobj().sendKeys(driver, pobj.subject, subject);
			fc.utobj().clickElement(driver, pobj.saveBtn);
			fc.utobj().switchFrameToDefault(driver);

			boolean isSubjectPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[contains(text () ,'" + subject + "')]");
			if (isSubjectPresent == false) {
				fc.utobj().throwsException("was not able to verify subject");
			}
			boolean isUserPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[contains(text () ,'" + subject
					+ "')]/ancestor::tr/td/div[.='" + corpUser.getuserFullName() + "']");
			if (isUserPresent == false) {
				fc.utobj().throwsException("was not able to verify userName at Lead Info Page");
			}
			boolean isStatusOfTaskPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[contains(text () ,'" + subject
					+ "')]/ancestor::tr/td[contains(text () ,'" + statusTask + "')]");
			if (isStatusOfTaskPresent == false) {
				fc.utobj().throwsException("was not able to verify status of task");
			}
			boolean isPriorityPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[contains(text () ,'" + subject
					+ "')]/ancestor::tr/td[contains(text () ,'" + priority + "')]");
			if (isPriorityPresent == false) {
				fc.utobj().throwsException("was not able to verify Priority of task");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "crm", "crmtranc"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Add Transaction At CRM > Opportunities > Opportunity Info", testCaseId = "TC_256_Add_New_Transaction")
	public void addNewTransaction() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			CRMAccountsPage pobj = new CRMAccountsPage(driver);
			CRMAccountsPageTest accountPage = new CRMAccountsPageTest();
			CRMContactsPageTest contactsPage = new CRMContactsPageTest();

			String description = fc.utobj().generateTestData(dataSet.get("description"));

			AdminCRMManageProductServiceCategoryPageTest serviceProductPage = new AdminCRMManageProductServiceCategoryPageTest();
			String categoryName = fc.utobj().generateTestData(dataSet.get("categoryName"));
			String productName = fc.utobj().generateTestData(dataSet.get("productName"));
			String oneLineDescription = fc.utobj().generateTestData(dataSet.get("oneLineDescription"));
			String rate = dataSet.get("rate");
			fc.utobj().printTestStep("Navigating to Admin > CRM >  Manage Product / Service & Category");
			fc.utobj().printTestStep("Add Category");
			serviceProductPage.addCategory(driver, categoryName, description);
			fc.utobj().printTestStep("Add Product/Service");
			serviceProductPage.addProductService(driver, categoryName, productName, oneLineDescription, rate);

			/*
			 * fc.utobj().printTestStep(testCaseId,
			 * "Navigate To Admin Users Manage Corporate User");
			 * fc.utobj().printTestStep(testCaseId, "Add Corporate User");
			 * AdminUsersManageCorporateUsersAddCorporateUserPageTest
			 * addCorporatePage = new
			 * AdminUsersManageCorporateUsersAddCorporateUserPageTest(); String
			 * userName = fc.utobj().generateTestData(dataSet.get("userName"));
			 * String emailId="crmautomation@staffex.com"; userName =
			 * addCorporatePage.addCorporateUser(driver, userName,
			 * config,emailId);
			 */

			fc.utobj().printTestStep("Navigate To Admin Users Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			String emailId = fc.utobj().generateTestData("crmautomation")+"@staffex.com";  // "crmautomation@staffex.com";
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUser = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporateUser.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			fc.utobj().printTestStep("Navigating to Admin > CRM > ContactType Configuration");
			fc.utobj().printTestStep("Add Contact Type");
			AdminCRMContactTypeConfigurationPageTest contactTypePage = new AdminCRMContactTypeConfigurationPageTest();
			String contactType = fc.utobj().generateTestData(dataSet.get("contactType"));
			contactTypePage.addContactType(driver, contactType);
			fc.utobj().printTestStep("Navigating to Admin > CRM >  Configure Opportunity Stages");
			fc.utobj().printTestStep("Add Stage");
			AdminCRMConfigureOpportunityStagesPageTest stagePage = new AdminCRMConfigureOpportunityStagesPageTest();
			String stage = fc.utobj().generateTestData(dataSet.get("stage"));
			stagePage.addStage(driver, stage);

			AdminCRMConfigureIndustryPageTest industryPage = new AdminCRMConfigureIndustryPageTest();
			String industry = fc.utobj().generateTestData(dataSet.get("industry"));
			description = fc.utobj().generateTestData(dataSet.get("description"));
			fc.utobj().printTestStep("Navigating to Admin > CRM >  Configure Industry Type");
			fc.utobj().printTestStep("Add Industry");
			industryPage.addIndustry(driver, industry, description);

			String accountName = fc.utobj().generateTestData(dataSet.get("accountName"));
			String visibility = dataSet.get("visibility");
			fc.utobj().printTestStep("Navigating to CRM >  Accounts > Accounts Summary");
			fc.utobj().printTestStep("Add Account");
			accountPage.addAcocunt(driver, industry, accountName, visibility);

			fc.utobj().printTestStep("Navigating to CRM >  Opportunities > Opportunities Summary");
			fc.utobj().printTestStep("Add Contact");

			fc.utobj().clickElement(driver, pobj.opportunitiesLinks);
			fc.utobj().clickElement(driver, pobj.addNew);
			fc.utobj().clickElement(driver, pobj.contactLnk);

			String cFname = fc.utobj().generateTestData(dataSet.get("cFname"));
			String cLname = fc.utobj().generateTestData(dataSet.get("cLname"));
			String primaryCMethod = dataSet.get("primaryCMethod");
			String assignTo = "Corporate";
			String regionName = "";
			String franchiseId = "";
			String title = dataSet.get("title");
			//String email = "crmautomation@staffex.com";
			String email = fc.utobj().generateTestData("crmautomation")+"@staffex.com";
			contactsPage.addContactGenericWithAccount(driver, contactType, cFname, cLname, primaryCMethod, assignTo,
					corpUser.getuserFullName(), regionName, franchiseId, title, email, config, accountName);

			fc.utobj().printTestStep("Navigating to CRM >  Opportunities > Opportunities Summary");
			fc.utobj().printTestStep("Add Opportunity");

			fc.utobj().clickElement(driver, pobj.opportunitiesLinks);
			fc.utobj().clickElement(driver, pobj.addNew);
			fc.utobj().clickElement(driver, pobj.OpportunityLnk);

			String opportunityName = fc.utobj().generateTestData(dataSet.get("opportunityName"));
			String contactName = cFname + " " + cLname;
			String salesAmount = dataSet.get("salesAmount");
			addOpportunityGenric(driver, opportunityName, accountName, contactName, corpUser.getuserFullName(), stage,
					salesAmount, config);

			fc.utobj().clickElement(driver, pobj.opportunitiesLinks);
			opportunityFilter(driver, stage);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//a[.='" + opportunityName + "']"));

			// add Transaction
			fc.utobj().printTestStep("At Transaction");
			fc.utobj().clickElement(driver, pobj.transactionTab);
			fc.utobj().clickElement(driver, pobj.newTransactionLink);
			/*
			 * fc.utobj().sendKeys(driver, pobj.opportunityName,
			 * opportunityName); fc.utobj().clickElement(driver,
			 * pobj.ajaxResult);
			 */
			fc.utobj().sendKeys(driver, pobj.invoiceDate, fc.utobj().getFutureDateUSFormat(-1));
			fc.utobj().clickElement(driver, pobj.selectProductAndService);
			fc.utobj().clickElement(driver, pobj.uncheckAll);
			fc.utobj().sendKeys(driver, pobj.searchProduct, productName);
			fc.utobj().clickElement(driver, pobj.checkAll);
			fc.utobj().clickElement(driver, pobj.okayBtn);
			fc.utobj().clickElement(driver, pobj.submitBtn);
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//td[contains(text(),'" + opportunityName
							+ "')]/ancestor::tr[@qat_maintable='withoutheader']/td/a[contains(text(),'INV')]"));

			fc.utobj().printTestStep("Verify Transaction");

			boolean isProductServicePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () , '" + productName + "')]");
			if (isProductServicePresent == false) {
				fc.utobj().throwsException("was not able to verify Transaction Name");
			}

			fc.utobj().clickElement(driver, pobj.opportunitiesLinks);
			opportunityFilter(driver, stage);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//a[.='" + opportunityName + "']"));
			fc.utobj().clickElement(driver, pobj.transactionTab);

			boolean isOppPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () , '" + opportunityName + "')]");
			if (isOppPresent == false) {
				fc.utobj().throwsException("was not able to verify Transaction");
			}

			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,(".//td[contains(text () , '" + opportunityName + "')]/preceding-sibling::td/a")));

			boolean isProductAvailable = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () ,'" + productName + "')]");
			if (isProductAvailable == false) {
				fc.utobj().throwsException("was not able to verify Product");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "crm1")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify the Modify Transaction At CRM > Opportunities > Opportunity Info", testCaseId = "TC_257_Modify_Transaction")
	public void modifyTransactionActionImage() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			CRMAccountsPage pobj = new CRMAccountsPage(driver);
			CRMAccountsPageTest accountPage = new CRMAccountsPageTest();
			CRMContactsPageTest contactsPage = new CRMContactsPageTest();

			String description = fc.utobj().generateTestData(dataSet.get("description"));
			AdminCRMManageProductServiceCategoryPageTest serviceProductPage = new AdminCRMManageProductServiceCategoryPageTest();
			String categoryName = fc.utobj().generateTestData(dataSet.get("categoryName"));
			String productName = fc.utobj().generateTestData(dataSet.get("productName"));
			String oneLineDescription = fc.utobj().generateTestData(dataSet.get("oneLineDescription"));
			String rate = dataSet.get("rate");
			fc.utobj().printTestStep("Navigating to Admin > CRM >  Manage Product / Service & Category");
			fc.utobj().printTestStep("Add Category");
			serviceProductPage.addCategory(driver, categoryName, description);
			fc.utobj().printTestStep("Add Product/Service");
			serviceProductPage.addProductService(driver, categoryName, productName, oneLineDescription, rate);

			String categoryName1 = fc.utobj().generateTestData(dataSet.get("categoryName"));
			String productName1 = fc.utobj().generateTestData(dataSet.get("productName"));
			String oneLineDescription1 = fc.utobj().generateTestData(dataSet.get("oneLineDescription"));
			serviceProductPage.addCategory(driver, categoryName1, description);
			serviceProductPage.addProductService(driver, categoryName1, productName1, oneLineDescription1, rate);

			/*
			 * fc.utobj().printTestStep(testCaseId,
			 * "Navigate To Admin Users Manage Corporate User");
			 * fc.utobj().printTestStep(testCaseId, "Add Corporate User");
			 * String emailId="crmautomation@staffex.com";
			 * AdminUsersManageCorporateUsersAddCorporateUserPageTest
			 * corporateUser=new
			 * AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			 * CorporateUser corpUser = new
			 * CorporateUser();corpUser.setEmail(emailId);corpUser =
			 * fc.CommonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			 * corporateUser.
			 * createDefaultCorporateUser_Through_WebService_AfterLogin(driver,
			 * corpUser);
			 */

			fc.utobj().printTestStep("Navigate To Admin Users Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			String emailId = fc.utobj().generateTestData("crmautomation")+"@staffex.com"; //"crmautomation@staffex.com";
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUser = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporateUser.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			fc.utobj().printTestStep("Navigating to Admin > CRM > ContactType Configuration");
			fc.utobj().printTestStep("Add Contact Type");
			AdminCRMContactTypeConfigurationPageTest contactTypePage = new AdminCRMContactTypeConfigurationPageTest();
			String contactType = fc.utobj().generateTestData(dataSet.get("contactType"));
			contactTypePage.addContactType(driver, contactType);
			fc.utobj().printTestStep("Navigating to Admin > CRM >  Configure Opportunity Stages");
			fc.utobj().printTestStep("Add Stage");
			AdminCRMConfigureOpportunityStagesPageTest stagePage = new AdminCRMConfigureOpportunityStagesPageTest();
			String stage = fc.utobj().generateTestData(dataSet.get("stage"));
			stagePage.addStage(driver, stage);

			AdminCRMConfigureIndustryPageTest industryPage = new AdminCRMConfigureIndustryPageTest();
			String industry = fc.utobj().generateTestData(dataSet.get("industry"));
			description = fc.utobj().generateTestData(dataSet.get("description"));
			fc.utobj().printTestStep("Navigating to Admin > CRM >  Configure Industry Type");
			fc.utobj().printTestStep("Add Industry");
			industryPage.addIndustry(driver, industry, description);

			String accountName = fc.utobj().generateTestData(dataSet.get("accountName"));
			String visibility = dataSet.get("visibility");
			fc.utobj().printTestStep("Navigating to CRM >  Accounts > Accounts Summary");
			fc.utobj().printTestStep("Add Account");
			accountPage.addAcocunt(driver, industry, accountName, visibility);

			fc.utobj().printTestStep("Navigating to CRM >  Opportunities > Opportunities Summary");
			fc.utobj().printTestStep("Add Contact");

			fc.utobj().clickElement(driver, pobj.opportunitiesLinks);
			fc.utobj().clickElement(driver, pobj.addNew);
			fc.utobj().clickElement(driver, pobj.contactLnk);

			String cFname = fc.utobj().generateTestData(dataSet.get("cFname"));
			String cLname = fc.utobj().generateTestData(dataSet.get("cLname"));
			String primaryCMethod = dataSet.get("primaryCMethod");
			String assignTo = "Corporate";
			String regionName = "";
			String franchiseId = "";
			String title = dataSet.get("title");
			String email = fc.utobj().generateTestData("crmautomation")+"@staffex.com"; //"crmautomation@staffex.com";

			contactsPage.addContactGenericWithAccount(driver, contactType, cFname, cLname, primaryCMethod, assignTo,
					corpUser.getuserFullName(), regionName, franchiseId, title, email, config, accountName);

			fc.utobj().printTestStep("Navigating to CRM >  Opportunities > Opportunities Summary");
			fc.utobj().printTestStep("Add Opportunity");

			fc.utobj().clickElement(driver, pobj.opportunitiesLinks);
			fc.utobj().clickElement(driver, pobj.addNew);
			fc.utobj().clickElement(driver, pobj.OpportunityLnk);

			String opportunityName = fc.utobj().generateTestData(dataSet.get("opportunityName"));
			String contactName = cFname + " " + cLname;
			String salesAmount = dataSet.get("salesAmount");
			addOpportunityGenric(driver, opportunityName, accountName, contactName, corpUser.getuserFullName(), stage,
					salesAmount, config);

			fc.utobj().clickElement(driver, pobj.opportunitiesLinks);
			opportunityFilter(driver, stage);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//a[.='" + opportunityName + "']"));

			// add Transaction
			fc.utobj().printTestStep("Add Transaction");
			fc.utobj().clickElement(driver, pobj.transactionTab);
			fc.utobj().clickElement(driver, pobj.newTransactionLink);
			/*
			 * fc.utobj().sendKeys(driver, pobj.opportunityName,
			 * opportunityName); fc.utobj().clickElement(driver,
			 * pobj.ajaxResult);
			 */
			fc.utobj().sendKeys(driver, pobj.invoiceDate, fc.utobj().getFutureDateUSFormat(-1));
			fc.utobj().clickElement(driver, pobj.selectProductAndService);
			fc.utobj().clickElement(driver, pobj.uncheckAll);
			fc.utobj().sendKeys(driver, pobj.searchProduct, productName);
			fc.utobj().clickElement(driver, pobj.checkAll);
			fc.utobj().clickElement(driver, pobj.okayBtn);
			fc.utobj().clickElement(driver, pobj.submitBtn);

			fc.utobj().clickElement(driver, pobj.transactionTab);

			// modify Transaction
			fc.utobj().printTestStep("Modify Transaction");
			fc.utobj().actionImgOption(driver, opportunityName, "Modify");
			fc.utobj().clickElement(driver, pobj.selectProductAndService);
			fc.utobj().clickElement(driver, pobj.uncheckAll);
			fc.utobj().sendKeys(driver, pobj.searchProduct, productName1);
			fc.utobj().clickElement(driver, pobj.checkAll);
			fc.utobj().clickElement(driver, pobj.okayBtn);
			fc.utobj().clickElement(driver, pobj.submitBtn);

			fc.utobj().printTestStep("Verify Modify Transaction");
			boolean isProductServicePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () , '" + productName1 + "')]");
			if (isProductServicePresent == false) {
				fc.utobj().throwsException("was not able to verify Transaction Name");
			}

			fc.utobj().clickElement(driver, pobj.opportunitiesLinks);
			opportunityFilter(driver, stage);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//a[.='" + opportunityName + "']"));
			fc.utobj().clickElement(driver, pobj.transactionTab);

			boolean isOppPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () , '" + opportunityName + "')]");
			if (isOppPresent == false) {
				fc.utobj().throwsException("was not able to verify Transaction");
			}

			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,(".//td[contains(text () , '" + opportunityName + "')]/preceding-sibling::td/a")));

			boolean isProductAvailable = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () ,'" + productName1 + "')]");
			if (isProductAvailable == false) {
				fc.utobj().throwsException("was not able to verify Product");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "crm")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify the Delete Transaction At CRM > Opportunities > Opportunity Info", testCaseId = "TC_258_Delete_Transaction")
	public void deleteTransactionActionImage() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			CRMAccountsPage pobj = new CRMAccountsPage(driver);
			CRMAccountsPageTest accountPage = new CRMAccountsPageTest();
			CRMContactsPageTest contactsPage = new CRMContactsPageTest();

			String description = fc.utobj().generateTestData(dataSet.get("description"));
			AdminCRMManageProductServiceCategoryPageTest serviceProductPage = new AdminCRMManageProductServiceCategoryPageTest();
			String categoryName = fc.utobj().generateTestData(dataSet.get("categoryName"));
			String productName = fc.utobj().generateTestData(dataSet.get("productName"));
			String oneLineDescription = fc.utobj().generateTestData(dataSet.get("oneLineDescription"));
			String rate = dataSet.get("rate");
			fc.utobj().printTestStep("Navigating to Admin > CRM >  Manage Product / Service & Category");
			fc.utobj().printTestStep("Add Category");
			serviceProductPage.addCategory(driver, categoryName, description);
			fc.utobj().printTestStep("Add Product/Service");
			serviceProductPage.addProductService(driver, categoryName, productName, oneLineDescription, rate);

			/*
			 * fc.utobj().printTestStep(testCaseId,
			 * "Navigate To Admin Users Manage Corporate User");
			 * fc.utobj().printTestStep(testCaseId, "Add Corporate User");
			 * String emailId="crmautomation@staffex.com";
			 * AdminUsersManageCorporateUsersAddCorporateUserPageTest
			 * corporateUser=new
			 * AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			 * CorporateUser corpUser = new
			 * CorporateUser();corpUser.setEmail(emailId);corpUser =
			 * fc.CommonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			 * corporateUser.
			 * createDefaultCorporateUser_Through_WebService_AfterLogin(driver,
			 * corpUser);
			 */

			fc.utobj().printTestStep("Navigate To Admin Users Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			String emailId = fc.utobj().generateTestData("crmautomation")+"@staffex.com";  //"crmautomation@staffex.com";
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUser = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporateUser.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			fc.utobj().printTestStep("Navigating to Admin > CRM > ContactType Configuration");
			fc.utobj().printTestStep("Add Contact Type");
			AdminCRMContactTypeConfigurationPageTest contactTypePage = new AdminCRMContactTypeConfigurationPageTest();
			String contactType = fc.utobj().generateTestData(dataSet.get("contactType"));
			contactTypePage.addContactType(driver, contactType);
			fc.utobj().printTestStep("Navigating to Admin > CRM >  Configure Opportunity Stages");
			fc.utobj().printTestStep("Add Stage");
			AdminCRMConfigureOpportunityStagesPageTest stagePage = new AdminCRMConfigureOpportunityStagesPageTest();
			String stage = fc.utobj().generateTestData(dataSet.get("stage"));
			stagePage.addStage(driver, stage);

			AdminCRMConfigureIndustryPageTest industryPage = new AdminCRMConfigureIndustryPageTest();
			String industry = fc.utobj().generateTestData(dataSet.get("industry"));
			description = fc.utobj().generateTestData(dataSet.get("description"));
			fc.utobj().printTestStep("Navigating to Admin > CRM >  Configure Industry Type");
			fc.utobj().printTestStep("Add Industry");
			industryPage.addIndustry(driver, industry, description);

			String accountName = fc.utobj().generateTestData(dataSet.get("accountName"));
			String visibility = dataSet.get("visibility");
			fc.utobj().printTestStep("Navigating to CRM >  Accounts > Accounts Summary");
			fc.utobj().printTestStep("Add Account");
			accountPage.addAcocunt(driver, industry, accountName, visibility);

			fc.utobj().printTestStep("Navigating to CRM >  Opportunities > Opportunities Summary");
			fc.utobj().printTestStep("Add Contact");

			fc.utobj().clickElement(driver, pobj.opportunitiesLinks);
			fc.utobj().clickElement(driver, pobj.addNew);
			fc.utobj().clickElement(driver, pobj.contactLnk);

			String cFname = fc.utobj().generateTestData(dataSet.get("cFname"));
			String cLname = fc.utobj().generateTestData(dataSet.get("cLname"));
			String primaryCMethod = dataSet.get("primaryCMethod");
			String assignTo = "Corporate";
			String regionName = "";
			String franchiseId = "";
			String title = dataSet.get("title");
			//String email = "crmautomation@staffex.com";
			String email = fc.utobj().generateTestData("crmautomation")+"@staffex.com";
			contactsPage.addContactGenericWithAccount(driver, contactType, cFname, cLname, primaryCMethod, assignTo,
					corpUser.getuserFullName(), regionName, franchiseId, title, email, config, accountName);

			fc.utobj().printTestStep("Navigating to CRM >  Opportunities > Opportunities Summary");
			fc.utobj().printTestStep("Add Opportunity");

			fc.utobj().clickElement(driver, pobj.opportunitiesLinks);
			fc.utobj().clickElement(driver, pobj.addNew);
			fc.utobj().clickElement(driver, pobj.OpportunityLnk);

			String opportunityName = fc.utobj().generateTestData(dataSet.get("opportunityName"));
			String contactName = cFname + " " + cLname;
			String salesAmount = dataSet.get("salesAmount");
			addOpportunityGenric(driver, opportunityName, accountName, contactName, corpUser.getuserFullName(), stage,
					salesAmount, config);

			fc.utobj().clickElement(driver, pobj.opportunitiesLinks);
			opportunityFilter(driver, stage);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//a[.='" + opportunityName + "']"));

			// add Transaction
			fc.utobj().printTestStep("Add Transaction");
			fc.utobj().clickElement(driver, pobj.transactionTab);
			fc.utobj().clickElement(driver, pobj.newTransactionLink);
			/*
			 * fc.utobj().sendKeys(driver, pobj.opportunityName,
			 * opportunityName); fc.utobj().clickElement(driver,
			 * pobj.ajaxResult);
			 */
			fc.utobj().sendKeys(driver, pobj.invoiceDate, fc.utobj().getFutureDateUSFormat(-1));
			fc.utobj().clickElement(driver, pobj.selectProductAndService);
			fc.utobj().clickElement(driver, pobj.uncheckAll);
			fc.utobj().sendKeys(driver, pobj.searchProduct, productName);
			fc.utobj().clickElement(driver, pobj.checkAll);
			fc.utobj().clickElement(driver, pobj.okayBtn);
			fc.utobj().clickElement(driver, pobj.submitBtn);

			fc.utobj().printTestStep("Verify Transaction");

			/*
			 * boolean isProductServicePresent=fc.utobj().verifyCase(driver,
			 * ".//td[contains(text () , '"+productName+"')]"); if
			 * (isProductServicePresent==false) { fc.utobj().throwsException(
			 * "was not able to verify Transaction Name"); }
			 */
			fc.utobj().clickElement(driver, pobj.opportunitiesLinks);
			opportunityFilter(driver, stage);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//a[.='" + opportunityName + "']"));
			fc.utobj().clickElement(driver, pobj.transactionTab);

			boolean isOppPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () , '" + opportunityName + "')]");
			if (isOppPresent == false) {
				fc.utobj().throwsException("was not able to verify Transaction");
			}

			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,(".//td[contains(text () , '" + opportunityName + "')]/preceding-sibling::td/a")));

			boolean isProductAvailable = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () ,'" + productName + "')]");
			if (isProductAvailable == false) {
				fc.utobj().throwsException("was not able to verify Product");
			}

			fc.utobj().clickElement(driver, pobj.opportunitiesLinks);
			opportunityFilter(driver, stage);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//a[.='" + opportunityName + "']"));
			fc.utobj().clickElement(driver, pobj.transactionTab);

			// Delete Transaction
			fc.utobj().printTestStep("Delete Transaction");
			fc.utobj().actionImgOption(driver, opportunityName, "Delete");
			fc.utobj().acceptAlertBox(driver);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().clickElement(driver, pobj.closeBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Verify Delete Transaction");

			boolean isNoRecordPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//td[.='No records found.']");
			if (isNoRecordPresent == false) {
				fc.utobj().throwsException("was not able to Archive Lead");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	// Anukaran Mishra

	@Test(groups = { "crmmodule", "crm"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Add a remark against the same opportunity>Attach a document in the remarks> Verify Document At Remark Activity History", testCaseId = "TC_301_Opportunity_Remark_Document ")
	public void verifyOpportunityRemarkDocument() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			CRMAccountsPage pobj = new CRMAccountsPage(driver);
			CRMAccountsPageTest accountPage = new CRMAccountsPageTest();
			CRMContactsPageTest contactsPage = new CRMContactsPageTest();

			fc.utobj().printTestStep("Naviaget To Admin Users Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			AdminUsersManageCorporateUsersAddCorporateUserPageTest addCorporatePage = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			String emailId = fc.utobj().generateTestData("crmautomation")+"@staffex.com";  //crmautomation@staffex.com";
			CorporateUser corpUser = new CorporateUser();
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corpUser.setEmail(emailId);
			corpUser = addCorporatePage.createDefaultUser(driver, corpUser);
			fc.home_page().logout(driver);
			fc.loginpage().loginWithParameter(driver, corpUser.getUserName(), corpUser.getPassword());

			// add customize remark document
			AdminCRMManageWebFormGeneratorPage npobj = new AdminCRMManageWebFormGeneratorPage(driver);
			fc.crm().crm_common().adminCRMManageFormGeneratorLnk(driver);
			fc.utobj().clickElement(driver, npobj.formContiue);
			String docSubjectName = fc.utobj().generateTestData(dataSet.get("docSubjectName"));
			String docLabel = fc.utobj().generateTestData(dataSet.get("docLabel"));
			fc.utobj().clickElement(driver, npobj.formTypeOpportunity);
			fc.utobj().clickElement(driver, npobj.oppremarks);
			fc.utobj().clickElement(driver, npobj.addDoc);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().sendKeys(driver, npobj.docSubject, docSubjectName);
			fc.utobj().sendKeys(driver, npobj.docLabel, docLabel);
			fc.utobj().clickElement(driver, npobj.submitBtn);

			fc.utobj().switchFrameToDefault(driver);

			// add opportnity
			String cFname = fc.utobj().generateTestData(dataSet.get("cFname"));
			String cLname = fc.utobj().generateTestData(dataSet.get("cLname"));
			String contactName = cFname + " " + cLname;
			String primaryCMethod = dataSet.get("primaryCMethod");
			String assignTo = "Corporate";
			String regionName = "";
			String franchiseId = "";
			String title = dataSet.get("title");
			String email = fc.utobj().generateTestData("crmautomation")+"@staffex.com";  //"automation@franconnect.net";
			String opportunityName = fc.utobj().generateTestData(dataSet.get("opportunityName"));
			String salesAmount = "100";

			fc.utobj().printTestStep("Navigating to Admin > CRM > ContactType Configuration");
			fc.utobj().printTestStep("Add Contact Type");
			AdminCRMContactTypeConfigurationPageTest contactTypePage = new AdminCRMContactTypeConfigurationPageTest();
			String contactType = fc.utobj().generateTestData(dataSet.get("contactType"));
			contactTypePage.addContactType(driver, contactType);
			Thread.sleep(1000);
			fc.utobj().printTestStep("Navigating to Admin > CRM >  Configure Opportunity Stages");
			fc.utobj().printTestStep("Add Stage");
			AdminCRMConfigureOpportunityStagesPageTest stagePage = new AdminCRMConfigureOpportunityStagesPageTest();
			String stage = fc.utobj().generateTestData(dataSet.get("stage"));
			stagePage.addStage(driver, stage);

			AdminCRMConfigureIndustryPageTest industryPage = new AdminCRMConfigureIndustryPageTest();
			String industry = fc.utobj().generateTestData(dataSet.get("industry"));
			String description = fc.utobj().generateTestData(dataSet.get("description"));
			fc.utobj().printTestStep("Navigating to Admin > CRM >  Configure Industry Type");
			fc.utobj().printTestStep("Add Industry");
			industryPage.addIndustry(driver, industry, description);

			String accountName = fc.utobj().generateTestData(dataSet.get("AccountName"));
			String visibility = dataSet.get("visibility");
			fc.utobj().printTestStep("Navigating to CRM >  Accounts > Accounts Summary");
			fc.utobj().printTestStep("Add Account");
			accountPage.addAcocunt(driver, industry, accountName, visibility);

			fc.utobj().clickElement(driver, pobj.contactsLinks);
			fc.utobj().clickElement(driver, pobj.addNew);
			fc.utobj().clickElement(driver, pobj.contactLnk);
			contactsPage.addContactGenericWithAccount(driver, contactType, cFname, cLname, primaryCMethod, assignTo,
					corpUser.getuserFullName(), regionName, franchiseId, title, email, config, accountName);
			fc.utobj().printTestStep("Navigating to CRM >  Opportunities > Opportunities Summary");
			fc.utobj().printTestStep("Add Opportunity");
			fc.utobj().clickElement(driver, pobj.opportunitiesLinks);
			fc.utobj().clickElement(driver, pobj.addNew);
			fc.utobj().clickElement(driver, pobj.OpportunityLnk);

			/*
			 * String opportunityName="defaultOpportunitya2211147"; String
			 * accountName="AccountNamex22111790"; String contactName=
			 * "Harishg22111477 Harishc2211145"; String userNameCorp=""; String
			 * stage="Prospecting"; String salesAmount="12";
			 */
			addOpportunityGenric(driver, opportunityName, accountName, contactName, corpUser.getuserFullName(), stage,
					salesAmount, config);

			/*
			 * fc.utobj().clickElement(driver, pobj.opportunitiesLinks);
			 * opportunityFilter(driver, stage); fc.utobj().clickElement(driver,
			 * fc.utobj().getElementByXpath(driver,".//a[.='"+opportunityName+
			 * "']")));
			 */
			fc.utobj().clickElement(driver, pobj.opportunitiesLinks);

			fc.utobj().printTestStep("Add Remarks");
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//a[contains(text () , '" + opportunityName + "')]"));
			fc.utobj().clickElement(driver, pobj.addRemraksTab);

			String remarks = fc.utobj().generateTestData(dataSet.get("remarkField"));
			String subjectRemark = fc.utobj().generateTestData(dataSet.get("subjectRemark"));
			fc.utobj().sendKeys(driver, pobj.remarkField, remarks);
			fc.utobj().sendKeys(driver, fc.utobj().getElementByXpath(driver,
					".//label[contains(text () , '" + docSubjectName + " :')]/following-sibling::input[@type='text']"),
					subjectRemark);
			String fileName = fc.utobj().getFilePathFromTestData(dataSet.get("fileName"));
			fc.utobj().sendKeys(driver, fc.utobj().getElementByXpath(driver,
					".//label[contains(text () , '" + docLabel + " :')]/following-sibling::input[@type='file']"),
					fileName);
			fc.utobj().clickElement(driver, pobj.saveBtn);

			// Remark Activity History
			// fc.utobj().clickElement(driver,
			// fc.utobj().getElementByXpath(driver,".//a[contains(text () ,
			// '"+opportunityName+"')]")));
			fc.utobj().printTestStep("Verify Remark Activity History");
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//*[@id='ActivityHis']//*[contains(text(),'" + remarks + "')]"));
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().sleep(1000);
			boolean isRemarksPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () ,'Remarks')]/ancestor::tr/td[.='" + remarks + "']");
			boolean isDocumentPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () ,'" + docSubjectName + "')]/ancestor::tr/td[.='" + subjectRemark + "']");

			if (isRemarksPresent == false || isDocumentPresent == false) {
				fc.utobj().throwsException("was not able to verify remarks");
			}
			fc.utobj().clickElement(driver, pobj.closeBtn);
			fc.utobj().switchFrameToDefault(driver);
			fc.utobj().sleep(1000);
			// TC_307_Verify_Delete_Opportnity start
			fc.utobj().printTestStep("Delete Opportunity Test Case TC_307_Verify_Delete_Opportnity start");
			fc.utobj().clickElement(driver, pobj.opportunitiesLinks);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//a[contains(text () ,'" + opportunityName + "')]/ancestor::tr/td/input[@name='checkb']"));
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//input[@value='Delete']"));
			fc.utobj().acceptAlertBox(driver);
			fc.utobj().sleep(1000);
			opportunityFilter(driver, stage);
			fc.utobj().printTestStep("Verify Archive Contact At Contacts Page");
			boolean isNoRecordPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//td[.='No records found.']");

			if (isNoRecordPresent == false) {
				fc.utobj().printTestStep("Delete Opportunity Test Case TC_307_Verify_Delete_Opportnity FAILED");
				fc.utobj().throwsException("was not able to verify delete opportunity");
			}
			fc.utobj().printTestStep("Delete Opportunity Test Case TC_307_Verify_Delete_Opportnity PASSED");

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	// Anukaran Mishra 304

	@Test(groups = { "crmmodule", "crm" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Login as divisional user >Create and Verify Opportunity under Opportunity Summary", testCaseId = "TC_304_Verify_Opportunity_DivisionalUser")
	public void verifyOpportunityByDivisionalLogin() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			CRMAccountsPage pobj = new CRMAccountsPage(driver);
			// CRMAccountsPageTest accountPage=new CRMAccountsPageTest();
			// CRMContactsPageTest contactsPage=new CRMContactsPageTest();

			AdminDivisionAddDivisionPageTest addDivPage = new AdminDivisionAddDivisionPageTest();
			String divisionName = fc.utobj().generateTestData(dataSet.get("divisionName"));
			addDivPage.addDivision(driver, divisionName);

			AdminUsersManageDivisionalUsersAddDivisionalUsersPageTest addDivUserPage = new AdminUsersManageDivisionalUsersAddDivisionalUsersPageTest();
			String userName = fc.utobj().generateTestData(dataSet.get("userName"));
			String password = "t0n1ght123";
			String userNameCorp = userName;
			String emailId = fc.utobj().generateTestData("crmautomation")+"@staffex.com"; //"crmautomation@staffex.com";
			addDivUserPage.addDivisionalUser(driver, userName, password, divisionName, emailId);
			userName = userName + " " + userName;

			fc.home_page().logout(driver);

			fc.utobj().printTestStep("Login With Divisional User");
			fc.loginpage().loginWithParameter(driver, userNameCorp, password);

			// add opportnity
			String cFname = fc.utobj().generateTestData(dataSet.get("cFname"));
			String cLname = fc.utobj().generateTestData(dataSet.get("cLname"));
			String contactName = cFname + " " + cLname;
			String primaryCMethod = dataSet.get("primaryCMethod");
			String assignTo = "Division";
			String regionName = "";
			String franchiseId = "";
			String title = dataSet.get("title");
			String email = fc.utobj().generateTestData("crmautomation")+"@staffex.com"; //"automation@franconnect.net";
			String opportunityName = fc.utobj().generateTestData(dataSet.get("opportunityName"));
			String salesAmount = dataSet.get("salesAmount");

			/*
			 * fc.utobj().printTestStep(testCaseId,
			 * "Navigating to Admin > CRM > ContactType Configuration");
			 * fc.utobj().printTestStep(testCaseId, "Add Contact Type");
			 * AdminCRMContactTypeConfigurationPageTest contactTypePage=new
			 * AdminCRMContactTypeConfigurationPageTest(); String
			 * contactType=fc.utobj().generateTestData(dataSet.get("contactType"
			 * )); contactTypePage.addContactType(driver, contactType, config);
			 */
			Thread.sleep(1000);
			String contactType = "Contacts";
			/*
			 * fc.utobj().printTestStep(testCaseId,
			 * "Navigating to Admin > CRM >  Configure Opportunity Stages");
			 * fc.utobj().printTestStep(testCaseId, "Add Stage");
			 * AdminCRMConfigureOpportunityStagesPageTest stagePage=new
			 * AdminCRMConfigureOpportunityStagesPageTest(); String
			 * stage=fc.utobj().generateTestData(dataSet.get("stage"));
			 * stagePage.addStage(driver, stage, config);
			 */
			String stage = "Prospecting";
			/*
			 * AdminCRMConfigureIndustryPageTest industryPage=new
			 * AdminCRMConfigureIndustryPageTest(); String
			 * industry=fc.utobj().generateTestData(dataSet.get("industry"));
			 * String
			 * description=fc.utobj().generateTestData(dataSet.get("description"
			 * )); fc.utobj().printTestStep(testCaseId,
			 * "Navigating to Admin > CRM >  Configure Industry Type");
			 * fc.utobj().printTestStep(testCaseId, "Add Industry");
			 * industryPage.addIndustry(driver, industry, description, config);
			 */
			String industry = null;
			String accountName = fc.utobj().generateTestData(dataSet.get("accountName"));
			String visibility = "Division";
			fc.utobj().printTestStep("Navigating to CRM >  Accounts > Accounts Summary");
			fc.utobj().printTestStep("Add Account");
			// accountPage.addAcocunt(driver, industry, accountName, visibility,
			// config);

			CRMAccountsPage accPage = new CRMAccountsPage(driver);
			fc.crm().crm_common().CRMAccountsLnk(driver);
			fc.utobj().clickElement(driver, accPage.addNew);
			fc.utobj().clickElement(driver, accPage.accountLnk);
			fc.utobj().sendKeys(driver, accPage.accountName, accountName);
			if (!fc.utobj().isSelected(driver, accPage.B2BAccountType)) {
				fc.utobj().clickElement(driver, accPage.B2BAccountType);
			}
			fc.utobj().selectDropDown(driver, accPage.visibilityType, visibility);
			if (divisionName != null) {
				fc.utobj().sendKeys(driver, accPage.divisionType, divisionName);
			}
			if (fc.utobj().isSelected(driver,accPage.isParent)) {
				fc.utobj().clickElement(driver, accPage.isParent);
			}
			fc.utobj().sendKeys(driver, accPage.address, "AddressTest");
			fc.utobj().sendKeys(driver, accPage.city, "City");
			fc.utobj().selectDropDown(driver, accPage.country, "USA");
			fc.utobj().sendKeys(driver, accPage.zipcode, "123455");
			fc.utobj().selectDropDown(driver, accPage.state, "Alabama");
			fc.utobj().sendKeys(driver, accPage.phoneNumbers, "1236547896");
			fc.utobj().sendKeys(driver, accPage.extn, "123");
			fc.utobj().sendKeys(driver, accPage.faxNumbers, "1236654");
			fc.utobj().sendKeys(driver, accPage.mobileNumbers, "1478523698");
			fc.utobj().sendKeys(driver, accPage.emailIds, "automation@franconnect.net");
			fc.utobj().sendKeys(driver, accPage.noOfEmployee, "500");
			fc.utobj().sendKeys(driver, accPage.sicCode, "SicCode");
			fc.utobj().sendKeys(driver, accPage.servicesProvided, "Service123");
			fc.utobj().sendKeys(driver, accPage.stockSymbol, "qwerty");

			if (!fc.utobj().isSelected(driver, accPage.addContactNo)) {
				fc.utobj().clickElement(driver, accPage.addContactNo);
			}
			fc.utobj().clickElement(driver, accPage.saveBtn);

			fc.utobj().clickElement(driver, pobj.contactsLinks);
			fc.utobj().clickElement(driver, pobj.addNew);
			fc.utobj().clickElement(driver, pobj.contactLnk);
			/*
			 * contactsPage.addContactGenericWithAccount(driver, contactType,
			 * cFname, cLname, primaryCMethod, assignTo, userName, regionName,
			 * franchiseId, title, email, config , accountName);
			 */

			CRMContactsPage CRMContactsPage = new CRMContactsPage(driver);
			fc.utobj().selectDropDown(driver, CRMContactsPage.contactType, contactType);
			fc.utobj().selectDropDown(driver, CRMContactsPage.title, title);
			fc.utobj().sendKeys(driver, CRMContactsPage.contactFirstName, cFname);
			fc.utobj().sendKeys(driver, CRMContactsPage.contactLastName, cLname);
			fc.utobj().sendKeys(driver, CRMContactsPage.suffix, "suffixTest");
			fc.utobj().sendKeys(driver, CRMContactsPage.position, "TestJobTitle");
			fc.utobj().sendKeys(driver, CRMContactsPage.address, "TestAddress");
			fc.utobj().sendKeys(driver, CRMContactsPage.city, "TestCity");
			fc.utobj().selectDropDown(driver, CRMContactsPage.country, "USA");
			fc.utobj().selectDropDown(driver, CRMContactsPage.state, "Alabama");
			fc.utobj().selectDropDown(driver, CRMContactsPage.primaryContactMethodSelect, primaryCMethod);
			fc.utobj().sendKeys(driver, CRMContactsPage.bestTimeToContact, "12:00 PM EST");
			fc.utobj().sendKeys(driver, CRMContactsPage.zipcode, "12345");
			fc.utobj().sendKeys(driver, CRMContactsPage.phoneNumbers, "1236547896");
			fc.utobj().sendKeys(driver, CRMContactsPage.extn, "12");
			fc.utobj().sendKeys(driver, CRMContactsPage.faxNumbers, "1263985487");
			fc.utobj().sendKeys(driver, CRMContactsPage.mobileNumbers, "8896724441");
			fc.utobj().sendKeys(driver, CRMContactsPage.emailIds, email);
			fc.utobj().sendKeys(driver, CRMContactsPage.alternateEmail, "test@gmail.com");
			fc.utobj().sendKeys(driver, CRMContactsPage.birthdate, "11/24/1980");
			fc.utobj().sendKeys(driver, CRMContactsPage.anniversarydate, "11/24/1999");
			fc.utobj().selectDropDown(driver, CRMContactsPage.rating, "Cold");

			if (assignTo.equalsIgnoreCase("Division")) {

				if (!fc.utobj().isSelected(driver, CRMContactsPage.assignToDivision)) {
					fc.utobj().clickElement(driver, CRMContactsPage.assignToDivision);
				}

				fc.utobj().selectDropDown(driver, CRMContactsPage.selectDivisionId, divisionName);
				fc.utobj().selectDropDown(driver, CRMContactsPage.selectDivisionUser, userName);

			}

			fc.utobj().sendKeys(driver, CRMContactsPage.comments, "Test Comment");

			fc.utobj().clickElement(driver, CRMContactsPage.saveBtn);

			fc.utobj().printTestStep("Navigating to CRM >  Opportunities > Opportunities Summary");
			fc.utobj().printTestStep("Add Opportunity");
			fc.utobj().clickElement(driver, pobj.opportunitiesLinks);
			fc.utobj().clickElement(driver, pobj.addNew);
			fc.utobj().clickElement(driver, pobj.OpportunityLnk);

			// addOpportunityGenric(driver, opportunityName, accountName,
			// contactName, userName, stage, salesAmount, config);

			CRMOpportunitiesPageUI opportunityPage = new CRMOpportunitiesPageUI(driver);
			fc.utobj().sendKeys(driver, opportunityPage.opportunityName, opportunityName);
			fc.utobj().sendKeys(driver, opportunityPage.oppAccountID, accountName);
			fc.utobj().clickElement(driver, opportunityPage.searchAccountBtn);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			// Anukaran
			try {
				fc.utobj().clickElement(driver,
						fc.utobj().getElementByXpath(driver, ".//*[contains(text () , '" + accountName + "')]"));
			} catch (Exception ee) {
				fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
						".//*[@id='companyform1Table']/tbody/tr/td/table/tbody/tr[2]/td/table/tbody/tr[2]/td/table/tbody/tr[2]/td[1]/a"));
			}
			fc.utobj().switchFrameToDefault(driver);

			/*
			 * fc.utobj().sendKeys(driver, opportunityPage.oppContactID,
			 * contactName); fc.utobj().clickElement(driver,
			 * opportunityPage.searchContactBtn);
			 * fc.commonMethods().switch_cboxIframe_frameId(driver);
			 * fc.utobj().clickElement(driver,
			 * fc.utobj().getElementByXpath(driver, ".//a[contains(text () , '"
			 * +contactName+"')]"))); fc.utobj().switchFrameToDefault(driver);
			 */

			fc.utobj().sendKeys(driver, opportunityPage.opportunityOwner, userName);
			fc.utobj().clickElement(driver, opportunityPage.searchOppOwnerBtn);

			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//a[contains(text () , '" + userName + "')]"));
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().selectDropDown(driver, opportunityPage.stage, stage);
			fc.utobj().sendKeys(driver, opportunityPage.salesAmount, "10");
			fc.utobj().clickElement(driver, opportunityPage.saveBtn);

			fc.utobj().clickElement(driver, pobj.opportunitiesLinks);
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//a[contains(text () , '" + opportunityName + "')]"));

			boolean isOpportunityPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () ,'Opportunity Name')]/ancestor::tr/td[.='" + opportunityName + "']");
			boolean isAccountPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () ,'Account')]/ancestor::tr/td[.='" + accountName + "']");
			if (isOpportunityPresent == false || isAccountPresent == false) {
				fc.utobj().throwsException("was not able to verify remarks");
			}

			fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "crmmodule", "crm"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Add a remark against the same opportunity>Attach a document in the remarks> Verify Document At Remark Activity History", testCaseId = "TC_315_Verify_Opportunity_Stage ")
	public void verifyConfigureOpportunityStage() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			CRMAccountsPage pobj = new CRMAccountsPage(driver);
			CRMAccountsPageTest accountPage = new CRMAccountsPageTest();
			CRMContactsPageTest contactsPage = new CRMContactsPageTest();

			fc.utobj().printTestStep("Naviaget To Admin Users Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			AdminUsersManageCorporateUsersAddCorporateUserPageTest addCorporatePage = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			String emailId = fc.utobj().generateTestData("crmautomation")+"@staffex.com"; //"crmautomation@staffex.com";
			CorporateUser corpUser = new CorporateUser();
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corpUser.setEmail(emailId);
			corpUser = addCorporatePage.createDefaultUser(driver, corpUser);
			fc.home_page().logout(driver);
			fc.loginpage().loginWithParameter(driver, corpUser.getUserName(), corpUser.getPassword());

			String cFname = fc.utobj().generateTestData(dataSet.get("cFname"));
			String cLname = fc.utobj().generateTestData(dataSet.get("cLname"));
			String contactName = cFname + " " + cLname;
			String primaryCMethod = dataSet.get("primaryCMethod");
			String assignTo = "Corporate";
			String regionName = "";
			String franchiseId = "";
			String title = dataSet.get("title");
			String email = "automation@franconnect.net";
			String opportunityName = fc.utobj().generateTestData(dataSet.get("opportunityName"));
			String salesAmount = "10";

			fc.utobj().printTestStep("Navigating to Admin > CRM > ContactType Configuration");
			fc.utobj().printTestStep("Add Contact Type");

			String contactType = "Contacts";
			fc.utobj().printTestStep("Navigating to Admin > CRM >  Configure Opportunity Stages");
			fc.utobj().printTestStep("Add Stage");
			AdminCRMConfigureOpportunityStagesPageTest stagePage = new AdminCRMConfigureOpportunityStagesPageTest();

			String stage = fc.utobj().generateTestData(dataSet.get("stage"));
			stagePage.addStage(driver, stage);

			AdminCRMConfigureIndustryPageTest industryPage = new AdminCRMConfigureIndustryPageTest();
			String industry = fc.utobj().generateTestData(dataSet.get("industry"));
			String description = fc.utobj().generateTestData(dataSet.get("description"));
			fc.utobj().printTestStep("Navigating to Admin > CRM >  Configure Industry Type");
			fc.utobj().printTestStep("Add Industry");
			industryPage.addIndustry(driver, industry, description);

			String accountName = fc.utobj().generateTestData(dataSet.get("accountName"));
			String visibility = dataSet.get("visibility");
			fc.utobj().printTestStep("Navigating to CRM >  Accounts > Accounts Summary");
			fc.utobj().printTestStep("Add Account");
			accountPage.addAcocunt(driver, industry, accountName, visibility);

			fc.utobj().clickElement(driver, pobj.contactsLinks);
			fc.utobj().clickElement(driver, pobj.addNew);
			fc.utobj().clickElement(driver, pobj.contactLnk);
			contactsPage.addContactGenericWithAccount(driver, contactType, cFname, cLname, primaryCMethod, assignTo,
					corpUser.getuserFullName(), regionName, franchiseId, title, email, config, accountName);

			fc.utobj().printTestStep("Navigating to CRM >  Opportunities > Opportunities Summary");
			fc.utobj().printTestStep("Add Opportunity");
			fc.utobj().clickElement(driver, pobj.opportunitiesLinks);
			fc.utobj().sleep(1000);
			fc.utobj().clickElement(driver, pobj.addNew);
			fc.utobj().sleep(1000);
			fc.utobj().clickElement(driver, pobj.OpportunityLnk);

			addOpportunityGenric(driver, opportunityName, accountName, contactName, corpUser.getuserFullName(), stage,
					salesAmount, config);
			fc.utobj().clickElement(driver, pobj.opportunitiesLinks);
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//a[contains(text () , '" + opportunityName + "')]"));

			boolean isOpportunityPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () ,'Opportunity Name')]/ancestor::tr/td[.='" + opportunityName + "']");
			boolean isAccountPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () ,'Account')]/ancestor::tr/td[.='" + accountName + "']");
			if (isOpportunityPresent == false || isAccountPresent == false) {
				fc.utobj().throwsException("was not able to verify remarks");
			}
			boolean isOpportunityStagePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () ,'Stage')]/ancestor::tr/td[.='" + stage + "']");

			if (isOpportunityStagePresent == false) {
				fc.utobj().throwsException("was not able to verify Opportunity Stage");
			}

			CRMLeadsPageTest leadTestPage = new CRMLeadsPageTest();
			CRMLeadsPage leadPage = new CRMLeadsPage(driver);

			fc.utobj().printTestStep("Navigate To CRM > Leads > Leads Summary");
			fc.utobj().printTestStep("Add Lead");

			String firstName = fc.utobj().generateTestData(dataSet.get("firstName"));
			String lastName = fc.utobj().generateTestData(dataSet.get("lastName"));
			String company = fc.utobj().generateTestData(dataSet.get("company"));
			String city = fc.utobj().generateTestData(dataSet.get("city"));
			String address = fc.utobj().generateTestData(dataSet.get("address"));
			String suffix = fc.utobj().generateTestData(dataSet.get("suffix"));
			String jobTitle = fc.utobj().generateTestData(dataSet.get("jobTitle"));
			String comment = fc.utobj().generateTestData(dataSet.get("comment"));
			String userNameReg = fc.utobj().generateTestData(dataSet.get("userNameReg"));
			String franchiseUser = "";

			leadTestPage.addLead(driver, dataSet, firstName, lastName, company, assignTo, city, address, suffix,
					jobTitle, comment, corpUser.getuserFullName(), regionName, userNameReg, franchiseId, franchiseUser,
					config);

			fc.utobj().printTestStep("Convert Lead At Lead Info Page");
			fc.utobj().clickElement(driver, leadPage.ConvertLead);
			String contactFName = fc.utobj().generateTestData(dataSet.get("contactFName"));
			fc.utobj().sendKeys(driver, leadPage.contactFirstName, contactFName);
			String contactLName = fc.utobj().generateTestData(dataSet.get("contactLName"));
			fc.utobj().sendKeys(driver, leadPage.contactLastName, contactLName);

			if (!fc.utobj().isSelected(driver, leadPage.radioAssociateWithExistingAcc)) {
				fc.utobj().clickElement(driver, leadPage.radioAssociateWithExistingAcc);
			}
			fc.utobj().sendKeys(driver, leadPage.accountNameSearch, accountName);
			fc.utobj().clickElement(driver, pobj.searchExistingAccountBtn);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().sleep(1000);
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//a[contains(text(),'" + accountName + "')]"));
			fc.utobj().switchFrameToDefault(driver);
			fc.utobj().clickElement(driver, leadPage.isOpportunity);
			opportunityName = fc.utobj().generateTestData(dataSet.get("opportunityName"));
			fc.utobj().sendKeys(driver, leadPage.opportunityName, opportunityName);
			fc.utobj().selectDropDown(driver, leadPage.stage, stage);

			fc.utobj().printTestStep(salesAmount);
			fc.utobj().sendKeys(driver, leadPage.salesAmount, salesAmount);
			String closureDate = fc.utobj().getFutureDateUSFormat(4);
			fc.utobj().sendKeys(driver, leadPage.closerDate, closureDate);
			String opportunitySource = "Web";
			fc.utobj().selectDropDown(driver, leadPage.opportunitySource, opportunitySource);
			fc.utobj().clickElement(driver, leadPage.btnConvert);

			fc.utobj().printTestStep("Navigate To CRM > Contacts > Contacts Summary Page");

			fc.utobj().clickElement(driver, pobj.opportunitiesLinks);
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//a[contains(text () , '" + opportunityName + "')]"));

			isOpportunityPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () ,'Opportunity Name')]/ancestor::tr/td[.='" + opportunityName + "']");
			isAccountPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () ,'Account')]/ancestor::tr/td[.='" + accountName + "']");

			if (isOpportunityPresent == false || isAccountPresent == false) {
				fc.utobj().throwsException("was not able to verify remarks");
			}

			isOpportunityStagePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () ,'Stage')]/ancestor::tr/td[.='" + stage + "']");

			if (isOpportunityStagePresent == false) {
				fc.utobj().throwsException("was not able to verify Opportunity Stage");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	public void addOpportunityGenric(WebDriver driver, String opportunityName, String accountName, String contactName,
			String userName, String stage, String salesAmount, Map<String, String> config) throws Exception {

		String testCaseId = "TC_Add_Opportunity_Generic";

		if (fc.utobj().validate(testCaseId)) {
			try {
				CRMOpportunitiesPageUI pobj = new CRMOpportunitiesPageUI(driver);
				fc.utobj().sendKeys(driver, pobj.opportunityName, opportunityName);
				fc.utobj().sendKeys(driver, pobj.oppAccountID, accountName);
				/*
				 * fc.utobj().clickElement(driver,
				 * fc.utobj().getElementByXpath(driver,
				 * ".//*[@id='customizedAjaxSearch']/div[contains(text () , '"
				 * +accountName+"')]")));
				 */

				fc.utobj().clickElement(driver, pobj.searchAccountBtn);
				fc.commonMethods().switch_cboxIframe_frameId(driver);
				fc.utobj().sleep(1000);
				fc.utobj().clickElement(driver,
						fc.utobj().getElementByXpath(driver, ".//a[contains(text () , '" + accountName + "')]"));
				fc.utobj().switchFrameToDefault(driver);
				fc.utobj().sleep(1000);
				fc.utobj().sendKeys(driver, pobj.oppContactID, contactName);
				/*
				 * fc.utobj().clickElement(driver,
				 * fc.utobj().getElementByXpath(driver,
				 * ".//*[@id='customizedAjaxSearch']/div[contains(text () , '"
				 * +contactName+"')]")));
				 */

				fc.utobj().clickElement(driver, pobj.searchContactBtn);

				fc.commonMethods().switch_cboxIframe_frameId(driver);
				fc.utobj().sleep(1000);
				fc.utobj().clickElement(driver,
						fc.utobj().getElementByXpath(driver, ".//a[contains(text () , '" + contactName + "')]"));
				fc.utobj().switchFrameToDefault(driver);

				fc.utobj().sendKeys(driver, pobj.opportunityOwner, userName);
				/*
				 * fc.utobj().clickElement(driver,
				 * fc.utobj().getElementByXpath(driver,
				 * ".//*[@id='customizedAjaxSearch']/div[contains(text () , '"
				 * +userName+"')]")));
				 */

				fc.utobj().clickElement(driver, pobj.searchOppOwnerBtn);

				fc.commonMethods().switch_cboxIframe_frameId(driver);
				fc.utobj().sleep(1000);
				fc.utobj().clickElement(driver,
						fc.utobj().getElementByXpath(driver, ".//a[contains(text () , '" + userName + "')]"));
				fc.utobj().switchFrameToDefault(driver);

				fc.utobj().selectDropDown(driver, pobj.stage, stage);
				fc.utobj().sendKeys(driver, pobj.salesAmount, salesAmount);
				fc.utobj().clickElement(driver, pobj.saveBtn);

				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException("Was not able to add Opportunity At CRM > Opportunities");

		}
	}

	/* Anukaran Mishra */

	@Test(groups = { "crmmodulewaitmail", "crm"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Create a task against opportunity with reminder as enable and add to calendar as yes> Verify task creation mail>Verify task reminder pop up and visibility of task at CRM Home", testCaseId = "TC_332_Verify_Task_Creation_Opportunity")
	public void verifyTaskCreationOpportunity()
			throws IOException, Exception, ParserConfigurationException, SAXException {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			/*CRMContactsPage pobj = new CRMContactsPage(driver);
			CRMContactsPageTest contactTestPage = new CRMContactsPageTest();

			fc.utobj().printTestStep("Naviaget To Admin Users Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			AdminUsersManageCorporateUsersAddCorporateUserPageTest addCorporatePage = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			String emailId = "crmautomation@staffex.com";
			CorporateUser corpUser = new CorporateUser();
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corpUser.setEmail(emailId);
			corpUser = addCorporatePage.createDefaultUser(driver, corpUser);
			fc.home_page().logout(driver);
			fc.loginpage().loginWithParameter(driver, corpUser.getUserName(), corpUser.getPassword());

			fc.utobj().printTestStep("Navigate To Admin > CRM > Contact Type Configuration");
			fc.utobj().printTestStep("Add Contact Type");
			AdminCRMContactTypeConfigurationPageTest contatcTypePage = new AdminCRMContactTypeConfigurationPageTest();
			String contactType = fc.utobj().generateTestData(dataSet.get("contactType"));
			contatcTypePage.addContactType(driver, contactType);

			fc.utobj().printTestStep("Naviagte To CRM > Contacts > Contacts Summary");
			fc.crm().crm_common().CRMContactsLnk(driver);
			fc.utobj().printTestStep("Add Contacts");
			fc.utobj().clickElement(driver, pobj.addNew);
			fc.utobj().clickElement(driver, pobj.contactLnk);

			String firstName = fc.utobj().generateTestData(dataSet.get("firstName"));
			String lastName = fc.utobj().generateTestData(dataSet.get("lastName"));
			String primaryCMethod = dataSet.get("primaryContactMethod");
			String assignTo = "Corporate";
			String regionName = "";
			String franchiseId = "";
			String title = "Mr.";
			String email = "automation@gmail.com";
			String contactName = firstName + " " + lastName;
			contactTestPage.addContactGeneric(driver, contactType, firstName, lastName, primaryCMethod, assignTo,
					corpUser.getuserFullName(), regionName, franchiseId, title, email, config);

			fc.utobj().clickElement(driver, pobj.tasksLinks);
			fc.utobj().clickElement(driver, pobj.taskLnk);

			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().sleep(1000);
			fc.utobj().clickElement(driver, pobj.contactTaskRadio);
			fc.utobj().sendKeys(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='name']"), contactName);
			fc.utobj().sleep(1000);
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//*[@id='contactSearch']/tbody/tr/td[2]/a/img"));
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//input[@name='checkb']"));
			fc.utobj().clickElement(driver, pobj.createTaskBtn);
			if (!fc.utobj().isSelected(driver,pobj.radioOwner)) {
				fc.utobj().clickElement(driver, pobj.radioOwner);
			}
			String statusTask = "Work In Progress";
			fc.utobj().selectDropDown(driver, pobj.statusTask, statusTask);
			String subject = fc.utobj().generateTestData(dataSet.get("subject"));
			fc.utobj().sendKeys(driver, pobj.subject, subject);
			String priority = dataSet.get("priority");
			fc.utobj().selectDropDown(driver, pobj.priority, priority);
			if (!fc.utobj().isSelected(driver,pobj.calendarTaskCheckBox)) {
				fc.utobj().clickElement(driver, pobj.calendarTaskCheckBox);
			}
			fc.utobj().sendKeys(driver, pobj.comments, "Test Comment");
			fc.utobj().clickElement(driver, pobj.createBtn);
			fc.utobj().switchFrameToDefault(driver);

			String parentWindowsHandle = driver.getWindowHandle();
			Set<String> allWindows = driver.getWindowHandles();
			for (String currentWindow : allWindows) {

				if (!currentWindow.equalsIgnoreCase(parentWindowsHandle)) {

					driver.switchTo().window(currentWindow);
					try {

						if (fc.utobj().getElementByXpath(driver, ".//*[.='Alert Details']").isDisplayed()) {

							boolean isUserPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
									".//td[contains(text () ,'Assign To ')]/ancestor::tr/td[contains(text () , '"
											+ corpUser.getuserFullName() + "')]");
							if (isUserPresent == false) {
								fc.utobj().throwsException("was not able to verify Assign To");
							}

							try {
								boolean isLeadPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
										".//*[contains(text () , '" + contactName + "')]");
								if (isLeadPresent == false) {
									fc.utobj().throwsException("was not able to verify Assign To");
								}
							} catch (Exception e) {
							}
							boolean isSubjectPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
									".//td[contains(text () ,'Subject ')]/ancestor::tr/td[contains(text () , '"
											+ subject + "')]");
							if (isSubjectPresent == false) {
								fc.utobj().throwsException("was not able to verify Assign To");
							}
							fc.utobj().printTestStep("Reminder pop-up verified ");
						}

					} catch (Exception e) {
						driver.close();
					}
				}
				driver.switchTo().window(parentWindowsHandle);
			}

			fc.utobj().printTestStep("Verify Send Email");
			String editorText = subject;

			String host = "192.168.9.2";// change accordingly
			String mailStoreType = "pop3";
			String user = "crmautomation@staffex.com";// change accordingly
			String password = "sdg@1a@Hfs";// change accordingly
			String subjectMail = "New Task Added";
			String fromEmail = "support@franconnect.com";
			String returnPath = "anukaran.mishra@franconnect.net";
			String[] emailRecepient = { "crmautomation@staffex.com", "automation101@franconnect.net" };
			subject = subjectMail;
			String mailBody = editorText;
			String[] attachmentName = { "attachment1.txt" };

			ReadLocalEmail readEmail = new ReadLocalEmail();
			Map<String, String> mailData = readEmail.emailReceivedValidator(host, mailStoreType, user, password,
					fromEmail, returnPath, emailRecepient, subject, mailBody, attachmentName);

			System.out.println(mailData.get("EmailBody"));
			System.out.println(mailData.get("EmailSubject"));
			System.out.println(mailData.get("EmailContentType"));
			System.out.println(mailData.get("EmailReceipients"));
			System.out.println(mailData.get("receivedMailSentDate"));
			System.out.println(mailData.get("EmailReplyTo"));
			System.out.println(mailData.get("EmailGetFrom"));

			if (!mailData.get("EmailBody").contains(editorText)) {
				fc.utobj().throwsException("was not able to verify mail text");
			}
			if (!mailData.get("EmailSubject").equalsIgnoreCase(subjectMail)) {
				fc.utobj().throwsException("was not able to verify subject mail");
			}*/

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}

	}

	public void opportunityFilter(WebDriver driver, String stage) throws Exception {

		CRMOpportunitiesPageUI pobj = new CRMOpportunitiesPageUI(driver);
		fc.utobj().clickElement(driver, pobj.showFilter);
		pobj.oppAccountID.clear();
		pobj.oppContactID.clear();
		fc.utobj().setToDefault(driver, pobj.assignToSelect);
		fc.utobj().setToDefault(driver, pobj.stageSelect);
		WebElement elementInput = fc.utobj().getElement(driver, pobj.stageSelect)
				.findElement(By.xpath("./div/div/input"));
		fc.utobj().selectValFromMultiSelect(driver, pobj.stageSelect, elementInput, stage);
		fc.utobj().selectDropDown(driver, pobj.addOn, "All");
		fc.utobj().clickElement(driver, pobj.searchBtn);
		fc.utobj().clickElement(driver, pobj.hideFilter);
	}
}
