package com.builds.test.crm;

import org.openqa.selenium.WebDriver;

import com.builds.uimaps.crm.CRMOpportunitiesPageUI;
import com.builds.utilities.FranconnectUtil;

public class CRM_OpportunityFill {
	
	FranconnectUtil fc = new FranconnectUtil();
	
	public CRM_OpportunityFill fillopportunity(WebDriver driver,CRM_OpportunityGetSet opp) throws Exception
	{
		CRMOpportunitiesPageUI oppUI = new CRMOpportunitiesPageUI(driver);
		
		if(opp.getOpportunityName()!=null)
		{
			fc.utobj().sendKeys(driver, oppUI.opportunityName, opp.getOpportunityName());
		}
		if(opp.getStage()!=null)
		{
			fc.utobj().selectDropDown(driver, oppUI.stage, opp.getStage());
		}
		if(opp.getOpportunityType()!=null)
		{
			fc.utobj().selectDropDown(driver, oppUI.opportunityType, opp.getOpportunityType());
		}
		if(opp.getProbability()!=null)
		{
			fc.utobj().selectDropDown(driver, oppUI.probability, opp.getProbability());
		}
		if(opp.getOpportunity_Source()!=null)
		{
			fc.utobj().selectDropDown(driver, oppUI.opportunitySource, opp.getOpportunity_Source());
		}
		if(opp.getExpected_Closure_Date()!=null)
		{
			fc.utobj().selectDropDown(driver, oppUI.closerDate, opp.getExpected_Closure_Date());
		}
		if(opp.getSales_Amount()!=null)
		{
			fc.utobj().selectDropDown(driver, oppUI.salesAmount, opp.getSales_Amount());
		}
		
		
		 
		return this;
	}

}
