package com.builds.test.crm;

public class CRM_OpportunityGetSet {
	
	
	private String OpportunityName;
	private String AccountName;
	private String ContactName;
	private String AssignTo;
	private String Stage;
	private String opportunityType;
	private String Probability;
	private String Opportunity_Source;
	private String Expected_Closure_Date;
	private String Sales_Amount;
	public String getOpportunityName() {
		return OpportunityName;
	}
	public void setOpportunityName(String opportunityName) {
		OpportunityName = opportunityName;
	}
	public String getAccountName() {
		return AccountName;
	}
	public void setAccountName(String accountName) {
		AccountName = accountName;
	}
	public String getContactName() {
		return ContactName;
	}
	public void setContactName(String contactName) {
		ContactName = contactName;
	}
	public String getAssignTo() {
		return AssignTo;
	}
	public void setAssignTo(String assignTo) {
		AssignTo = assignTo;
	}
	public String getStage() {
		return Stage;
	}
	public void setStage(String stage) {
		Stage = stage;
	}
	public String getOpportunityType() {
		return opportunityType;
	}
	public void setOpportunityType(String opportunityType) {
		this.opportunityType = opportunityType;
	}
	public String getProbability() {
		return Probability;
	}
	public void setProbability(String probability) {
		Probability = probability;
	}
	public String getOpportunity_Source() {
		return Opportunity_Source;
	}
	public void setOpportunity_Source(String opportunity_Source) {
		Opportunity_Source = opportunity_Source;
	}
	public String getExpected_Closure_Date() {
		return Expected_Closure_Date;
	}
	public void setExpected_Closure_Date(String expected_Closure_Date) {
		Expected_Closure_Date = expected_Closure_Date;
	}
	public String getSales_Amount() {
		return Sales_Amount;
	}
	public void setSales_Amount(String sales_Amount) {
		Sales_Amount = sales_Amount;
	}
	
	
	
	
	
}
