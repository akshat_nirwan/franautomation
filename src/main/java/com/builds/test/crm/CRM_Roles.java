package com.builds.test.crm;

import org.openqa.selenium.WebDriver;

import com.builds.test.admin.AdminUsersRolesAddRoleAddUsersforRolePageTest;
import com.builds.uimaps.admin.AdminUsersRolesAddNewRolePage;
import com.builds.utilities.FranconnectUtil;

public class CRM_Roles {
	FranconnectUtil fc = new FranconnectUtil();

	public void Addroles_with_All_privileges(WebDriver driver,String roleType, String roleName) throws Exception
	{
		AdminUsersRolesAddNewRolePage pobj = new AdminUsersRolesAddNewRolePage(driver);
		
		fc.utobj().printTestStep("Admin > Users > Roles > Add New Role");
		fc.adminpage().adminUsersRolesAddNewRolePage(driver);
		
		if (roleType.equalsIgnoreCase("Corporate Level")) {
			fc.utobj().selectDropDownByValue(driver, pobj.roleType, "1");

		} else if (roleType.equalsIgnoreCase("Division Level")) {
			fc.utobj().selectDropDownByValue(driver, pobj.roleType, "5");

		} else if (roleType.equalsIgnoreCase("Regional Level")) {
			fc.utobj().selectDropDownByValue(driver, pobj.roleType, "2");

		} else if (roleType.equalsIgnoreCase("Franchise Level")) {
			fc.utobj().selectDropDownByValue(driver, pobj.roleType, "0");
		}

		fc.utobj().sendKeys(driver, pobj.roleName, roleName);
		

		fc.utobj().clickElement(driver, pobj.submit);
		AdminUsersRolesAddRoleAddUsersforRolePageTest p2 = new AdminUsersRolesAddRoleAddUsersforRolePageTest();
		p2.assignLater(driver);

		fc.utobj().printTestStep("Verify The Added Role");
		boolean isTextPresent = fc.utobj().assertPageSource(driver, roleName);
		if (isTextPresent == false) {
			fc.utobj().throwsException( roleType + "Added Role is not present at Admin > Users > Roles Page");
		}
	}
		
	
	
	
	
	
	}


