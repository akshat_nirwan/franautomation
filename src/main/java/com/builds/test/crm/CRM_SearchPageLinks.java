package com.builds.test.crm;

import org.openqa.selenium.WebDriver;

import com.builds.uimaps.crm.CRM_SearchPageUI;
import com.builds.utilities.FranconnectUtil;

public class CRM_SearchPageLinks {
	FranconnectUtil fc = new FranconnectUtil();
	
	public void leadtab(WebDriver driver) throws Exception
	{
		CRM_SearchPageUI Search = new CRM_SearchPageUI(driver);
		fc.utobj().printTestStep("Click on the Lead tab in search");
		fc.utobj().clickElement(driver, Search.LeadTab);
	}

	public void ArchivedRadiobtn(WebDriver driver) throws Exception
	{
		CRM_SearchPageUI Search = new CRM_SearchPageUI(driver);
		fc.utobj().printTestStep("Click on the Archived Radio Lead in search");
		fc.utobj().clickElement(driver, Search.ArchivedLeadRadio);
	}
	
	public void submitbtn(WebDriver driver) throws Exception
	{
		CRM_SearchPageUI Search = new CRM_SearchPageUI(driver);
		fc.utobj().printTestStep("Click on the Submit btn in search");
		fc.utobj().clickElement(driver, Search.SubmitBtn);
	}
	
}
