package com.builds.test.crm;

import org.openqa.selenium.WebDriver;

import com.builds.uimaps.crm.CRM_SelectaAndCustomizeTemplateUI;
import com.builds.utilities.FranconnectUtil;

public class CRM_SelectandCustomizeTemplatepage {

	FranconnectUtil fc = new FranconnectUtil();
	
	public void EditCampaignName(WebDriver driver)throws Exception
	{
		CRM_SelectaAndCustomizeTemplateUI temUI = new CRM_SelectaAndCustomizeTemplateUI(driver);
		fc.utobj().printTestStep("Edit Campaign Name");
		fc.utobj().clickElement(driver, temUI.Editcampaign_name);
		Thread.sleep(2000);
		fc.utobj().switchFrame(driver, temUI.CreateCampaign_frame);
	}
	
	public void firstTemplateclick(WebDriver driver) throws Exception
	{
		CRM_SelectaAndCustomizeTemplateUI temUI = new CRM_SelectaAndCustomizeTemplateUI(driver);
		fc.utobj().printTestStep("Click on First Template of list");
		fc.utobj().clickElement(driver, temUI.firstTemplate);
		Thread.sleep(500);
		fc.utobj().printTestStep("Click Customize button of template");
		fc.utobj().clickElement(driver, temUI.firstTemplate_customizebtn);
	}
	
	
	
	
	
}
