package com.builds.test.crm;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.interactions.Actions;

import com.builds.uimaps.crm.CRM_SendEmailUI;
import com.builds.utilities.FranconnectUtil;

public class CRM_SendEmailFillTest {

	FranconnectUtil fc = new FranconnectUtil();

	public CRM_SendEmailFillTest filldetails(WebDriver driver, CRM_SendEmailGetSet Semail) throws Exception {
		CRM_SendEmailUI em = new CRM_SendEmailUI(driver);

		
		if(Semail.getCC() !=null) {
			fc.utobj().sendKeys(driver, em.CCbox, Semail.getCC());
		}
			
		if (Semail.getSubject() != null) {

			fc.utobj().sendKeys(driver, em.EmailSubject, Semail.getSubject());
		}
		
		fc.utobj().clickElement(driver, em.SignatureRadio);

		fc.utobj().switchFrame(driver, em.frame);
		Actions actions = new Actions(driver);
		actions.moveToElement(fc.utobj().getElement(driver, em.textfield));
		actions.click();
		actions.sendKeys("test from selenium");
		actions.build().perform();

		return this;
	}

	public void cancelbtn(WebDriver driver) throws Exception {
		CRM_SendEmailUI em = new CRM_SendEmailUI(driver);
		fc.utobj().clickElement(driver, em.cancelbtn);
	}

	public void savebtn(WebDriver driver) throws Exception {
		CRM_SendEmailUI em = new CRM_SendEmailUI(driver);
		fc.utobj().clickElement(driver, em.sendbtn);
	}

	public void resetbtn(WebDriver driver) throws Exception {
		CRM_SendEmailUI em = new CRM_SendEmailUI(driver);
		fc.utobj().clickElement(driver, em.Resetbtn);
	}

	public void CustomID_onEmail(WebDriver driver, String value) throws Exception // Value = provide custom email id 
	{
		fc.utobj().printTestStep("click on custom radio button and provide id");
		CRM_SendEmailUI em = new CRM_SendEmailUI(driver);
		fc.utobj().clickElement(driver, em.CustomRadio);
		Thread.sleep(500);
		fc.utobj().sendKeys(driver, em.Customtext, value);
		
	}
	public void addTemplate_inEmail(WebDriver driver, String TemName) throws Exception
	{
		CRM_SendEmailUI em = new CRM_SendEmailUI(driver);
		fc.utobj().printBugStatus("add template in the Email");
		fc.utobj().selectDropDown(driver, em.TemplateEmail, TemName);
	}
	public void Attachmenton_emailpage(WebDriver driver) throws Exception
	{
		
		CRM_SendEmailUI em = new CRM_SendEmailUI(driver);
		fc.utobj().printTestStep("Click on Attachment button on Send Email page");
		fc.utobj().clickElement(driver, em.AttachmentBtn);
		Thread.sleep(500);
		fc.commonMethods().switch_cboxIframe_frameId(driver);
		 fc.utobj().sendKeys(driver, em.choosefile,  fc.utobj().getFilePathFromTestData("importData"));
		 fc.utobj().clickElement(driver, em.Attachbtn);
		 fc.utobj().clickElement(driver, em.Done);
	
		
	}
	
}
