package com.builds.test.crm;

public class CRM_SendEmailGetSet {

	private String subject;
	private String CC;
	private String resetbtn;
	private String savebtn;
	private String cancelbtn;
	private String frame;
	private String textfield;
	
	
	public String getTextfield() {
		return textfield;
	}
	public void setTextfield(String textfield) {
		this.textfield = textfield;
	}
	public String getSubject() {
		return subject;
	}
	public void setSubject(String subject) {
		this.subject = subject;
	}
	public String getResetbtn() {
		return resetbtn;
	}
	public void setResetbtn(String resetbtn) {
		this.resetbtn = resetbtn;
	}
	public String getSavebtn() {
		return savebtn;
	}
	public void setSavebtn(String savebtn) {
		this.savebtn = savebtn;
	}
	public String getCancelbtn() {
		return cancelbtn;
	}
	public void setCancelbtn(String cancelbtn) {
		this.cancelbtn = cancelbtn;
	}
	public String getFrame() {
		return frame;
	}
	public void setFrame(String frame) {
		this.frame = frame;
	}
	public String getCC() {
		return CC;
	}
	public void setCC(String cC) {
		CC = cC;
	}
	
	
	
	
	
}
