package com.builds.test.crm;

import org.openqa.selenium.WebDriver;

import com.builds.uimaps.crm.CRM_TemplateInfoPageUI;
import com.builds.utilities.FranconnectUtil;

public class CRM_TemplateInfoPage {
	
	FranconnectUtil fc = new FranconnectUtil();
	
	public CRM_TemplateInfoPage filldetails(WebDriver driver, CRM_TemplateInfoPageGetSet teminfo) throws Exception
	{
		CRM_TemplateInfoPageUI Teminfop = new CRM_TemplateInfoPageUI(driver);
		fc.utobj().printTestStep("Fill Template info");
		
		
		if(teminfo.getTemaplateName()!= null)
		{
			fc.utobj().sendKeys(driver, Teminfop.TemaplateName, teminfo.getTemaplateName());
		}
		if(teminfo.getTemaplatesubject()!=null)
		{
			fc.utobj().sendKeys(driver, Teminfop.Templatesubject, teminfo.getTemaplatesubject());
		}
		/*if(teminfo.getTempAccess()!=null)
		{
			fc.utobj().selectDropDown(driver, Teminfop.TemplateAccess, teminfo.getTempAccess());
		}
		*/
		
		return this;
	}
	
	public void cancelbtn(WebDriver driver) throws Exception
	{
		CRM_TemplateInfoPageUI Teminfop = new CRM_TemplateInfoPageUI(driver);
		fc.utobj().printTestStep("Click cancel button");
		fc.utobj().clickElement(driver, Teminfop.Cancelbtn);
	}

	public void savebtn(WebDriver driver) throws Exception
	{
		CRM_TemplateInfoPageUI Teminfop = new CRM_TemplateInfoPageUI(driver);
		fc.utobj().printTestStep("Click Save button");
		fc.utobj().clickElement(driver, Teminfop.Savebtn);
	}
	
	public void MakeasFavorite(WebDriver driver) throws Exception
	{
		CRM_TemplateInfoPageUI Teminfop = new CRM_TemplateInfoPageUI(driver);
		fc.utobj().printTestStep("Click Make as Favorite toggel");
		fc.utobj().clickElement(driver, Teminfop.MakeasFavorite);
	}
	
	public void MarkasRecomended(WebDriver driver) throws Exception
	{
		CRM_TemplateInfoPageUI Teminfop = new CRM_TemplateInfoPageUI(driver);
		fc.utobj().printTestStep("Click Mark as Recomended toggel");
		fc.utobj().clickElement(driver, Teminfop.MarkasRecomended);
	}
	public void saveAndcontinue(WebDriver driver) throws Exception
	{
		CRM_TemplateInfoPageUI Teminfop = new CRM_TemplateInfoPageUI(driver);
		fc.utobj().printTestStep("Click Save and Continue button to Associate template");
		fc.utobj().clickElement(driver, Teminfop.SaveAndcontinue);
	}
	
	
}
