package com.builds.test.crm;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Test;
import com.builds.utilities.FranconnectUtil;
import com.builds.utilities.TestCase;

public class CRM_TestLeadAddedThroughCRMDashboard {
	FranconnectUtil fc = new FranconnectUtil();
	
	
	@Test(groups = {"crm_add_lead","crm"})
	@TestCase(createdOn = "2018-11-06", updatedOn = "2018-11-06", testCaseDescription = "Verify Add Lead from crm dashboard", testCaseId = "Add_Lead_CRM_DashBoard")//TC_Add_Lead_From_CRM_Dashboard
	public void VerifyAddLead() throws Exception{
		String testCaseId= fc.utobj().readTestCaseInfo(this.getClass(
				).getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());;
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		driver = fc.loginpage().login(driver);
		try {
			CRM_Verify_Lead verifylead=new CRM_Verify_Lead();
			verifylead.addLead(driver,"LeatThroughRestAPI");
			verifylead.addLead(driver,"LeadThroughWebFormGenerator");
			verifylead.addLead(driver,"LeadThroughHome");
			verifylead.addLead(driver,"LeadThroughLeadPage");	
			verifylead.addLead(driver,"LeadThroughAccountsPage");
			verifylead.addLead(driver,"LeadThroughContactPage");
			verifylead.addLead(driver,"LeadThroughOpportunityPage");
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
			
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
		
		
	}
}
