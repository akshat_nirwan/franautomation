package com.builds.test.crm;

import java.util.Map;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import com.builds.test.crm.RestAPI.CRM_AddLeadFrom_Webservice;
import com.builds.uimaps.crm.CRMHomePage;
import com.builds.uimaps.crm.CRMLeadsPage;
import com.builds.utilities.FranconnectUtil;

public class CRM_Verify_Lead {

	FranconnectUtil fc = new FranconnectUtil();
	
	public void addLead(WebDriver driver,String leadaddedthroghCRMdashboard) throws Exception{
		String testCaseId="Add_Lead_CRM_DashBoard";
		String area="Lead_Visibility";
		Map<String, String> dataSet = fc.utobj().readTestDatawithsqllite("crm", testCaseId, area);
		CRMHomePage pobj = new CRMHomePage(driver);
		CRMLeadsPage crmleadpage=new CRMLeadsPage(driver);
	
		
		try {
			if(leadaddedthroghCRMdashboard.equals("LeatThroughRestAPI")) {
				//Add Lead Through REST API
				fc.utobj().printTestStep("Add Lead Through REST API");
				CRM_AddLeadFrom_Webservice addleadthroughwebservice=new CRM_AddLeadFrom_Webservice();
				addleadthroughwebservice.Addleadfrom_Webservices_Verify(driver);
				fc.utobj().printTestStep("Lead added through RESt API");		
			}
			
			else if(leadaddedthroghCRMdashboard.equalsIgnoreCase("LeadThroughWebFormGenerator")) {
				//Add Lead Through Web Form Generator
				fc.utobj().printTestStep("Add Lead Through Web Form Generator");
				AdminCRMManageWebFormGeneratorPageTest addedthroughwebformgenerator=new AdminCRMManageWebFormGeneratorPageTest();
				addedthroughwebformgenerator.launchTestFormlead(driver);
			}
			
			else if(leadaddedthroghCRMdashboard.equalsIgnoreCase("LeadThroughHome")) {
				
				//Add Lead through Home Page
				fc.utobj().printTestStep("Navigate To CRM > Home Page");
				fc.utobj().printTestStep("Add Lead");
				fc.crm().crm_common().CRMHomeLnk(driver);
				fc.utobj().clickElement(driver, pobj.addNew);
				fc.utobj().clickElement(driver, pobj.addLeadLnk);
				WebElement frame = fc.utobj().getElementByXpath(driver, ".//*[@class='newLayoutcboxIframe']");
				fc.utobj().switchFrame(driver, frame);
				String firstName = fc.utobj().generateTestData(dataSet.get("firstName"));
				fc.utobj().sendKeys(driver, pobj.leadFirstName, firstName);
				String lastName = fc.utobj().generateTestData(dataSet.get("lastName"));
				fc.utobj().sendKeys(driver, pobj.leadLastName, lastName);
				String fullName=firstName+" "+lastName;
				String address = fc.utobj().generateTestData(dataSet.get("address"));
				fc.utobj().sendKeys(driver, pobj.address, address);
				String city = fc.utobj().generateTestData(dataSet.get("city"));
				fc.utobj().sendKeys(driver, pobj.city, city);
				fc.utobj().selectDropDown(driver, pobj.country, dataSet.get("country"));
				fc.utobj().sendKeys(driver, pobj.zipcode,dataSet.get("zipcode"));
				fc.utobj().selectDropDown(driver, pobj.state,dataSet.get("state"));
				fc.utobj().selectDropDown(driver, pobj.primaryContactMethod,dataSet.get("primaryContactMethod"));
				fc.utobj().sendKeys(driver, pobj.phoneNumbers,dataSet.get("phoneNumbers"));
				fc.utobj().sendKeys(driver, pobj.faxNumbers, dataSet.get("faxNumbers"));
				
				String email=fc.utobj().generateTestData(dataSet.get("emailIds"))+"@staffex.com";
				fc.utobj().sendKeys(driver, pobj.emailIds, email);
				fc.utobj().sendKeys(driver, pobj.mobileNumbers,dataSet.get("mobileNumbers"));
				
				if (!fc.utobj().isSelected(driver, pobj.assignToCorporate)) {
					fc.utobj().clickElement(driver, pobj.assignToCorporate);
				}
				fc.utobj().selectDropDown(driver, pobj.selectCorporateUser,dataSet.get("userName"));
				String comments = fc.utobj().generateTestData(dataSet.get("comments"));
				fc.utobj().sendKeys(driver, pobj.comments, comments);
				fc.utobj().clickElement(driver, pobj.saveBtn);
				fc.utobj().clickElement(driver, pobj.closeBtn);
				fc.utobj().switchFrameToDefault(driver);

				fc.utobj().printTestStep("Verify Lead At CRM > Leads Summary Page");
				fc.utobj().clickElement(driver, pobj.leadsLink);
				
				fc.utobj().sendKeys(driver, crmleadpage.searchLeads, fullName);
				fc.utobj().clickElement(driver,"//*[@id='systemSearchBtn']");
				
				boolean verifyName = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[.='" + firstName + " " + lastName + "']");
				if (verifyName == false) {
					fc.utobj().throwsException("was not able to verify Name");
				}
				boolean ownerOfLead = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[.='" + firstName + " " + lastName
						+ "']/ancestor::tr/td[contains(text () , 'FranConnect Administrator')]");
				
				if (ownerOfLead == false) {
					fc.utobj().throwsException("was not able to verify owner of the lead");
				}
				boolean statusOfLead = fc.utobj().verifyElementOnVisible_ByXpath(driver,
						".//a[.='" + firstName + " " + lastName + "']/ancestor::tr/td[contains(text () , 'New')]");
				if (statusOfLead == false) {
					fc.utobj().throwsException("was not able to verify staus of the lead");
				}
			}
			
			
			else if(leadaddedthroghCRMdashboard.equalsIgnoreCase("LeadThroughLeadPage")) {
				//Add lead through CRM>Lead
				
				fc.utobj().printTestStep("Navigate To CRM > Leads > Leads Summary");
				fc.utobj().printTestStep("Add Lead");
				fc.crm().crm_common().CRMLeadsLnk(driver);

				fc.utobj().clickElement(driver, crmleadpage.addNew);
				fc.utobj().clickElement(driver, crmleadpage.leadLnk);
				fc.utobj().selectDropDown(driver, crmleadpage.title, dataSet.get("title"));
				String firstNameaddedthroughcrmlead = fc.utobj().generateTestData(dataSet.get("firstName"));
				fc.utobj().sendKeys(driver, crmleadpage.leadFirstName, firstNameaddedthroughcrmlead);
				String lastNameaddedthroughcrmlead = fc.utobj().generateTestData(dataSet.get("lastName"));
				fc.utobj().sendKeys(driver, crmleadpage.leadLastName, lastNameaddedthroughcrmlead);
				String fullnameaddedthroughcrmlead=firstNameaddedthroughcrmlead+" "+lastNameaddedthroughcrmlead;
				String company = fc.utobj().generateTestData(dataSet.get("company"));
				fc.utobj().sendKeys(driver, crmleadpage.companyName, company);
				fc.utobj().selectDropDown(driver, crmleadpage.primaryContactMethodSelect, dataSet.get("primaryContactMethod"));
				if (!fc.utobj().isSelected(driver, crmleadpage.assignToCorporate)) {
					fc.utobj().clickElement(driver, crmleadpage.assignToCorporate);
				}
				fc.utobj().selectDropDown(driver, crmleadpage.selectCorporateUser,dataSet.get("userName"));
				String addressthroughcrmlead = fc.utobj().generateTestData(dataSet.get("address"));
				fc.utobj().sendKeys(driver, crmleadpage.address, addressthroughcrmlead);
				String citythroughcrmlead = fc.utobj().generateTestData(dataSet.get("city"));
				fc.utobj().sendKeys(driver, crmleadpage.city, citythroughcrmlead);
				fc.utobj().selectDropDown(driver, crmleadpage.country, dataSet.get("country"));
				fc.utobj().selectDropDown(driver, crmleadpage.state, dataSet.get("state"));
				fc.utobj().sendKeys(driver, crmleadpage.zipcode, dataSet.get("zipcode"));
				fc.utobj().sendKeys(driver, crmleadpage.phoneNumbers, dataSet.get("phoneNumbers"));
				fc.utobj().sendKeys(driver, crmleadpage.extn, dataSet.get("extn"));
				fc.utobj().sendKeys(driver, crmleadpage.faxNumbers, dataSet.get("faxNumbers"));
				fc.utobj().sendKeys(driver, crmleadpage.mobileNumbers, dataSet.get("mobileNumbers"));
				
				String email=fc.utobj().generateTestData(dataSet.get("emailIds"))+"@staffex.com";
				fc.utobj().sendKeys(driver, crmleadpage.emailIds, email);
				fc.utobj().sendKeys(driver, crmleadpage.alternateEmail, dataSet.get("alternateEmail"));
				String suffix = fc.utobj().generateTestData(dataSet.get("suffix"));
				fc.utobj().sendKeys(driver, crmleadpage.suffix, suffix);
				String jobTitle = fc.utobj().generateTestData(dataSet.get("jobTitle"));
				fc.utobj().sendKeys(driver, crmleadpage.position, jobTitle);
				fc.utobj().sendKeys(driver, crmleadpage.birthdate, dataSet.get("birthdate"));
				fc.utobj().sendKeys(driver, crmleadpage.anniversarydate, dataSet.get("anniversarydate"));
				fc.utobj().selectDropDown(driver, crmleadpage.rating, dataSet.get("rating"));
				fc.utobj().sendKeys(driver, crmleadpage.bestTimeToContact, dataSet.get("bestTimeToContact"));
				String comment = fc.utobj().generateTestData(dataSet.get("comment"));
				fc.utobj().sendKeys(driver, crmleadpage.comments, comment);
				fc.utobj().clickElement(driver, crmleadpage.saveBtn);
				
				
				fc.utobj().printTestStep("Verify Lead At CRM > Leads Summary Page");
				fc.utobj().clickElement(driver, pobj.leadsLink);
				fc.utobj().sendKeys(driver, crmleadpage.searchLeads, fullnameaddedthroughcrmlead);
				fc.utobj().clickElement(driver,"//*[@id='systemSearchBtn']");
				
				boolean verifyNamethroughcrmlead = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[.='" + firstNameaddedthroughcrmlead + " " + lastNameaddedthroughcrmlead + "']");
				if (verifyNamethroughcrmlead == false) {
					fc.utobj().throwsException("was not able to verify Name");
				}
				boolean ownerOfLeadaddedthroughcrmlead = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[.='" + firstNameaddedthroughcrmlead + " " + lastNameaddedthroughcrmlead
						+ "']/ancestor::tr/td[contains(text () , 'FranConnect Administrator')]");
				
				if (ownerOfLeadaddedthroughcrmlead == false) {
					fc.utobj().throwsException("was not able to verify owner of the lead");
				}
				boolean statusOfLeadownerOfLeadaddedthroughcrmlead = fc.utobj().verifyElementOnVisible_ByXpath(driver,
						".//a[.='" + firstNameaddedthroughcrmlead + " " + lastNameaddedthroughcrmlead + "']/ancestor::tr/td[contains(text () , 'New')]");
				if (statusOfLeadownerOfLeadaddedthroughcrmlead == false) {
					fc.utobj().throwsException("was not able to verify staus of the lead");
				}
			}
			
			else if(leadaddedthroghCRMdashboard.equalsIgnoreCase("LeadThroughAccountsPage")) {
				
				//Add lead through CRM>Accounts
				fc.utobj().printTestStep("Navigate To Accounts > Accounts Summary");
				fc.utobj().printTestStep("Add Lead");
				fc.crm().crm_common().CRMAccountsLnk(driver);

				fc.utobj().clickElement(driver, crmleadpage.addNew);
				fc.utobj().clickElement(driver, crmleadpage.leadLnk);
				fc.utobj().selectDropDown(driver, crmleadpage.title, dataSet.get("title"));
				String firstName = fc.utobj().generateTestData(dataSet.get("firstName"));
				fc.utobj().sendKeys(driver, crmleadpage.leadFirstName, firstName);
				String lastName = fc.utobj().generateTestData(dataSet.get("lastName"));
				fc.utobj().sendKeys(driver, crmleadpage.leadLastName, lastName);
				String fullname=firstName+" "+lastName;
				String company = fc.utobj().generateTestData(dataSet.get("company"));
				fc.utobj().sendKeys(driver, crmleadpage.companyName, company);
				fc.utobj().selectDropDown(driver, crmleadpage.primaryContactMethodSelect, dataSet.get("primaryContactMethod"));
				if (!fc.utobj().isSelected(driver, crmleadpage.assignToCorporate)) {
					fc.utobj().clickElement(driver, crmleadpage.assignToCorporate);
				}
				fc.utobj().selectDropDown(driver, crmleadpage.selectCorporateUser,dataSet.get("userName"));
				String addressthroughcrmlead = fc.utobj().generateTestData(dataSet.get("address"));
				fc.utobj().sendKeys(driver, crmleadpage.address, addressthroughcrmlead);
				String citythroughcrmlead = fc.utobj().generateTestData(dataSet.get("city"));
				fc.utobj().sendKeys(driver, crmleadpage.city, citythroughcrmlead);
				fc.utobj().selectDropDown(driver, crmleadpage.country, dataSet.get("country"));
				fc.utobj().selectDropDown(driver, crmleadpage.state, dataSet.get("state"));
				fc.utobj().sendKeys(driver, crmleadpage.zipcode, dataSet.get("zipcode"));
				fc.utobj().sendKeys(driver, crmleadpage.phoneNumbers, dataSet.get("phoneNumbers"));
				fc.utobj().sendKeys(driver, crmleadpage.extn, dataSet.get("extn"));
				fc.utobj().sendKeys(driver, crmleadpage.faxNumbers, dataSet.get("faxNumbers"));
				fc.utobj().sendKeys(driver, crmleadpage.mobileNumbers, dataSet.get("mobileNumbers"));
				
				String email=fc.utobj().generateTestData(dataSet.get("emailIds"))+"@staffex.com";
				fc.utobj().sendKeys(driver, crmleadpage.emailIds, email);
				fc.utobj().sendKeys(driver, crmleadpage.alternateEmail, dataSet.get("alternateEmail"));
				String suffix = fc.utobj().generateTestData(dataSet.get("suffix"));
				fc.utobj().sendKeys(driver, crmleadpage.suffix, suffix);
				String jobTitle = fc.utobj().generateTestData(dataSet.get("jobTitle"));
				fc.utobj().sendKeys(driver, crmleadpage.position, jobTitle);
				fc.utobj().sendKeys(driver, crmleadpage.birthdate, dataSet.get("birthdate"));
				fc.utobj().sendKeys(driver, crmleadpage.anniversarydate, dataSet.get("anniversarydate"));
				fc.utobj().selectDropDown(driver, crmleadpage.rating, dataSet.get("rating"));
				fc.utobj().sendKeys(driver, crmleadpage.bestTimeToContact, dataSet.get("bestTimeToContact"));
				String comment = fc.utobj().generateTestData(dataSet.get("comment"));
				fc.utobj().sendKeys(driver, crmleadpage.comments, comment);
				fc.utobj().clickElement(driver, crmleadpage.saveBtn);
				
				
				fc.utobj().printTestStep("Verify Lead At CRM > Leads Summary Page");
				fc.utobj().clickElement(driver, pobj.leadsLink);
				fc.utobj().sendKeys(driver, crmleadpage.searchLeads, fullname);
				fc.utobj().clickElement(driver,"//*[@id='systemSearchBtn']");
				
				boolean verifyNamethroughcrmaccount = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[.='" + firstName + " " + lastName + "']");
				if (verifyNamethroughcrmaccount == false) {
					fc.utobj().throwsException("was not able to verify Name");
				}
				boolean ownerOfLeadaddedthroughcrmaccount = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[.='" + firstName + " " + lastName
						+ "']/ancestor::tr/td[contains(text () , 'FranConnect Administrator')]");
				
				if (ownerOfLeadaddedthroughcrmaccount == false) {
					fc.utobj().throwsException("was not able to verify owner of the lead");
				}
				boolean statusOfLeadownerOfLeadaddedthroughcrmaccount = fc.utobj().verifyElementOnVisible_ByXpath(driver,
						".//a[.='" + firstName + " " + lastName + "']/ancestor::tr/td[contains(text () , 'New')]");
				if (statusOfLeadownerOfLeadaddedthroughcrmaccount == false) {
					fc.utobj().throwsException("was not able to verify staus of the lead");
				}
			
				
			}
		
			else if(leadaddedthroghCRMdashboard.equalsIgnoreCase("LeadThroughContactPage")) {
				//Add lead through CRM>Contact
				fc.utobj().printTestStep("Navigate To Contacts > Contacts Summary");
				fc.utobj().printTestStep("Add Lead");
				fc.crm().crm_common().CRMContactsLnk(driver);

				fc.utobj().clickElement(driver, crmleadpage.addNew);
				fc.utobj().clickElement(driver, crmleadpage.leadLnk);
				fc.utobj().selectDropDown(driver, crmleadpage.title, dataSet.get("title"));
				String firstName = fc.utobj().generateTestData(dataSet.get("firstName"));
				fc.utobj().sendKeys(driver, crmleadpage.leadFirstName, firstName);
				String lastName = fc.utobj().generateTestData(dataSet.get("lastName"));
				fc.utobj().sendKeys(driver, crmleadpage.leadLastName, lastName);
				String fullname=firstName+" "+lastName;
				String company = fc.utobj().generateTestData(dataSet.get("company"));
				fc.utobj().sendKeys(driver, crmleadpage.companyName, company);
				fc.utobj().selectDropDown(driver, crmleadpage.primaryContactMethodSelect, dataSet.get("primaryContactMethod"));
				if (!fc.utobj().isSelected(driver, crmleadpage.assignToCorporate)) {
					fc.utobj().clickElement(driver, crmleadpage.assignToCorporate);
				}
				fc.utobj().selectDropDown(driver, crmleadpage.selectCorporateUser,dataSet.get("userName"));
				String addressthroughcrmlead = fc.utobj().generateTestData(dataSet.get("address"));
				fc.utobj().sendKeys(driver, crmleadpage.address, addressthroughcrmlead);
				String citythroughcrmlead = fc.utobj().generateTestData(dataSet.get("city"));
				fc.utobj().sendKeys(driver, crmleadpage.city, citythroughcrmlead);
				fc.utobj().selectDropDown(driver, crmleadpage.country, dataSet.get("country"));
				fc.utobj().selectDropDown(driver, crmleadpage.state, dataSet.get("state"));
				fc.utobj().sendKeys(driver, crmleadpage.zipcode, dataSet.get("zipcode"));
				fc.utobj().sendKeys(driver, crmleadpage.phoneNumbers, dataSet.get("phoneNumbers"));
				fc.utobj().sendKeys(driver, crmleadpage.extn, dataSet.get("extn"));
				fc.utobj().sendKeys(driver, crmleadpage.faxNumbers, dataSet.get("faxNumbers"));
				fc.utobj().sendKeys(driver, crmleadpage.mobileNumbers, dataSet.get("mobileNumbers"));
				
				String email=fc.utobj().generateTestData(dataSet.get("emailIds"))+"@staffex.com";
				fc.utobj().sendKeys(driver, crmleadpage.emailIds, email);
				fc.utobj().sendKeys(driver, crmleadpage.alternateEmail, dataSet.get("alternateEmail"));
				String suffix = fc.utobj().generateTestData(dataSet.get("suffix"));
				fc.utobj().sendKeys(driver, crmleadpage.suffix, suffix);
				String jobTitle = fc.utobj().generateTestData(dataSet.get("jobTitle"));
				fc.utobj().sendKeys(driver, crmleadpage.position, jobTitle);
				fc.utobj().sendKeys(driver, crmleadpage.birthdate, dataSet.get("birthdate"));
				fc.utobj().sendKeys(driver, crmleadpage.anniversarydate, dataSet.get("anniversarydate"));
				fc.utobj().selectDropDown(driver, crmleadpage.rating, dataSet.get("rating"));
				fc.utobj().sendKeys(driver, crmleadpage.bestTimeToContact, dataSet.get("bestTimeToContact"));
				String comment = fc.utobj().generateTestData(dataSet.get("comment"));
				fc.utobj().sendKeys(driver, crmleadpage.comments, comment);
				fc.utobj().clickElement(driver, crmleadpage.saveBtn);
				
				
				fc.utobj().printTestStep("Verify Lead At CRM > Leads Summary Page");
				fc.utobj().clickElement(driver, pobj.leadsLink);
				fc.utobj().sendKeys(driver, crmleadpage.searchLeads, fullname);
				fc.utobj().clickElement(driver,"//*[@id='systemSearchBtn']");
				
				boolean verifyNamethroughcrmcontact = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[.='" + firstName + " " + lastName + "']");
				if (verifyNamethroughcrmcontact == false) {
					fc.utobj().throwsException("was not able to verify Name");
				}
				boolean ownerOfLeadaddedthroughcrmcontact = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[.='" + firstName + " " + lastName
						+ "']/ancestor::tr/td[contains(text () , 'FranConnect Administrator')]");
				
				if (ownerOfLeadaddedthroughcrmcontact == false) {
					fc.utobj().throwsException("was not able to verify owner of the lead");
				}
				boolean statusOfLeadownerOfLeadaddedthroughcrmcontact = fc.utobj().verifyElementOnVisible_ByXpath(driver,
						".//a[.='" + firstName + " " + lastName + "']/ancestor::tr/td[contains(text () , 'New')]");
				if (statusOfLeadownerOfLeadaddedthroughcrmcontact == false) {
					fc.utobj().throwsException("was not able to verify staus of the lead");
				}
			}
			
			else if(leadaddedthroghCRMdashboard.equalsIgnoreCase("LeadThroughOpportunityPage")) {
				
				//Add lead through CRM>Opprtunity
				fc.utobj().printTestStep("Navigate To Opprtunity>Opprtunity Summary");
				fc.utobj().printTestStep("Add Lead");
				fc.crm().crm_common().CRMOpportunitiesLnk(driver);

				fc.utobj().clickElement(driver, crmleadpage.addNew);
				fc.utobj().clickElement(driver, crmleadpage.leadLnk);
				fc.utobj().selectDropDown(driver, crmleadpage.title, dataSet.get("title"));
				String firstName = fc.utobj().generateTestData(dataSet.get("firstName"));
				fc.utobj().sendKeys(driver, crmleadpage.leadFirstName, firstName);
				String lastName = fc.utobj().generateTestData(dataSet.get("lastName"));
				fc.utobj().sendKeys(driver, crmleadpage.leadLastName, lastName);
				String fullname=firstName+" "+lastName;
				String company = fc.utobj().generateTestData(dataSet.get("company"));
				fc.utobj().sendKeys(driver, crmleadpage.companyName, company);
				fc.utobj().selectDropDown(driver, crmleadpage.primaryContactMethodSelect, dataSet.get("primaryContactMethod"));
				if (!fc.utobj().isSelected(driver, crmleadpage.assignToCorporate)) {
					fc.utobj().clickElement(driver, crmleadpage.assignToCorporate);
				}
				fc.utobj().selectDropDown(driver, crmleadpage.selectCorporateUser,dataSet.get("userName"));
				String addressthroughcrmlead = fc.utobj().generateTestData(dataSet.get("address"));
				fc.utobj().sendKeys(driver, crmleadpage.address, addressthroughcrmlead);
				String citythroughcrmlead = fc.utobj().generateTestData(dataSet.get("city"));
				fc.utobj().sendKeys(driver, crmleadpage.city, citythroughcrmlead);
				fc.utobj().selectDropDown(driver, crmleadpage.country, dataSet.get("country"));
				fc.utobj().selectDropDown(driver, crmleadpage.state, dataSet.get("state"));
				fc.utobj().sendKeys(driver, crmleadpage.zipcode, dataSet.get("zipcode"));
				fc.utobj().sendKeys(driver, crmleadpage.phoneNumbers, dataSet.get("phoneNumbers"));
				fc.utobj().sendKeys(driver, crmleadpage.extn, dataSet.get("extn"));
				fc.utobj().sendKeys(driver, crmleadpage.faxNumbers, dataSet.get("faxNumbers"));
				fc.utobj().sendKeys(driver, crmleadpage.mobileNumbers, dataSet.get("mobileNumbers"));
				
				String email=fc.utobj().generateTestData(dataSet.get("emailIds"))+"@staffex.com";
				fc.utobj().sendKeys(driver, crmleadpage.emailIds, email);
				fc.utobj().sendKeys(driver, crmleadpage.alternateEmail, dataSet.get("alternateEmail"));
				String suffix = fc.utobj().generateTestData(dataSet.get("suffix"));
				fc.utobj().sendKeys(driver, crmleadpage.suffix, suffix);
				String jobTitle = fc.utobj().generateTestData(dataSet.get("jobTitle"));
				fc.utobj().sendKeys(driver, crmleadpage.position, jobTitle);
				fc.utobj().sendKeys(driver, crmleadpage.birthdate, dataSet.get("birthdate"));
				fc.utobj().sendKeys(driver, crmleadpage.anniversarydate, dataSet.get("anniversarydate"));
				fc.utobj().selectDropDown(driver, crmleadpage.rating, dataSet.get("rating"));
				fc.utobj().sendKeys(driver, crmleadpage.bestTimeToContact, dataSet.get("bestTimeToContact"));
				String comment = fc.utobj().generateTestData(dataSet.get("comment"));
				fc.utobj().sendKeys(driver, crmleadpage.comments, comment);
				fc.utobj().clickElement(driver, crmleadpage.saveBtn);
				
				
				fc.utobj().printTestStep("Verify Lead At CRM > Leads Summary Page");
				fc.utobj().clickElement(driver, pobj.leadsLink);
				fc.utobj().sendKeys(driver, crmleadpage.searchLeads, fullname);
				fc.utobj().clickElement(driver,"//*[@id='systemSearchBtn']");
				
				boolean verifyNamethroughcrmopportunity = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[.='" + firstName + " " + lastName + "']");
				if (verifyNamethroughcrmopportunity == false) {
					fc.utobj().throwsException("was not able to verify Name");
				}
				boolean ownerOfLeadaddedthroughcrmopportunity = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[.='" + firstName + " " + lastName
						+ "']/ancestor::tr/td[contains(text () , 'FranConnect Administrator')]");
				
				if (ownerOfLeadaddedthroughcrmopportunity == false) {
					fc.utobj().throwsException("was not able to verify owner of the lead");
				}
				boolean statusOfLeadownerOfLeadaddedthroughcrmopportunity = fc.utobj().verifyElementOnVisible_ByXpath(driver,
						".//a[.='" + firstName + " " + lastName + "']/ancestor::tr/td[contains(text () , 'New')]");
				if (statusOfLeadownerOfLeadaddedthroughcrmopportunity == false) {
					fc.utobj().throwsException("was not able to verify staus of the lead");
				}
				
			}
			fc.utobj().printTestStep("Lead Added through CRM dahsboard");	
		}
		catch(Exception e) {
			fc.utobj().throwsException("Lead was not added through CRM dashboard");
		}
		
		
		
	}
}
