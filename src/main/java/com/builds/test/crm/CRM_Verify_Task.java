package com.builds.test.crm;

import java.io.IOException;
import java.util.Map;

import javax.xml.parsers.ParserConfigurationException;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;
import org.xml.sax.SAXException;

import com.builds.test.admin.AdminAreaRegionAddAreaRegionPageTest;
import com.builds.test.admin.AdminFranchiseLocationAddFranchiseLocationPageTest;
import com.builds.test.admin.AdminUsersManageCorporateUsersAddCorporateUserPageTest;
import com.builds.test.admin.AdminUsersManageManageFranchiseUsersPageTest;
import com.builds.test.admin.AdminUsersManageRegionalUsersAddRegionalUserPageTest;
import com.builds.test.common.CorporateUser;
import com.builds.uimaps.crm.CRMLeadsPage;
import com.builds.uimaps.crm.CRMTasksPage;
import com.builds.uimaps.crm.CRM_Verify_TaskUI;
import com.builds.utilities.FranconnectUtil;
import com.builds.utilities.TestCase;

public class CRM_Verify_Task {
	FranconnectUtil fc = new FranconnectUtil();

	@Test(groups = { "crm","crmtaskfinall"})
	@TestCase(createdOn = "2018-11-19", updatedOn = "2018-11-19", testCaseDescription = "Create task through corporate user and verify on different places", testCaseId = "Task_Creation_CorporateUser")
	public void addTaskLead() throws IOException, Exception, ParserConfigurationException, SAXException {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());
		String area = "Task";
		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestDatawithsqllite("crm", testCaseId, area);

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		CRMLeadsPage crmleadpage = new CRMLeadsPage(driver);
		CRMTasksPage pobj = new CRMTasksPage(driver);
		try {
			driver = fc.loginpage().login(driver);
			CRMLeadsPageTest leadPage = new CRMLeadsPageTest();

			fc.utobj().printTestStep("Navigate To Admin Users Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			String emailId = dataSet.get("emailId");
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUser = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser.setUserName(fc.utobj().generateTestData(dataSet.get("userName")));
			corpUser.setPassword(dataSet.get("Password"));
			String username = corpUser.getUserName();
			String password = corpUser.getPassword();
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			String corporateusername = corpUser.getuserFullName();
			corporateUser.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);
			fc.loginpage().loginWithParameter(driver, username, password);

			CRM_Verify_TaskUI taskui = new CRM_Verify_TaskUI(driver);

			fc.utobj().clickElement(driver, taskui.User);
			fc.utobj().clickElement(driver, taskui.Options);
			fc.utobj().moveToElement(driver, taskui.More);
			fc.utobj().clickElement(driver, taskui.More);
			fc.utobj().clickElement(driver, taskui.More);
			fc.utobj().clickElement(driver, taskui.Notifications);
			if (!fc.utobj().isSelected(driver, taskui.testcreationcheckbox)) {
				fc.utobj().clickElement(driver, taskui.testcreationcheckbox);
			}
			fc.utobj().clickElement(driver, taskui.save);

			fc.utobj().printTestStep("Navigate To CRM > Leads > Leads Summary");
			fc.utobj().printTestStep("Add Lead");

			String firstName = fc.utobj().generateTestData(dataSet.get("firstName"));
			String lastName = fc.utobj().generateTestData(dataSet.get("lastName"));
			String company = fc.utobj().generateTestData(dataSet.get("company"));
			String assignTo = dataSet.get("assignTo");
			String city = fc.utobj().generateTestData(dataSet.get("city"));
			String address = fc.utobj().generateTestData(dataSet.get("address"));
			String suffix = fc.utobj().generateTestData(dataSet.get("suffix"));
			String jobTitle = fc.utobj().generateTestData(dataSet.get("jobTitle"));
			String comment = fc.utobj().generateTestData(dataSet.get("comment"));
			String regionName = "";
			String regionalUser = "";
			String franchiseId = "";
			String franchiseUser = "";
			String fullName = corpUser.getuserFullName();
			String fullnameaddedthroughcrmlead = firstName + " " + lastName;

			leadPage.addLeadwithSqlite(driver, dataSet, firstName, lastName, company, assignTo, city, address, suffix,
					jobTitle, comment, fullName, regionName, regionalUser, franchiseId, franchiseUser, config);

			CRM_Verify_Task_Method verifytaskmethod = new CRM_Verify_Task_Method();
			String subject1 = fc.utobj().generateTestData(dataSet.get("subject"));
			String subject2 = fc.utobj().generateTestData(dataSet.get("subject"));
			String subject3 = fc.utobj().generateTestData(dataSet.get("subject"));
			String subject4 = fc.utobj().generateTestData(dataSet.get("subject"));
			String subject5 = fc.utobj().generateTestData(dataSet.get("subject"));
			String subject6 = fc.utobj().generateTestData(dataSet.get("subject"));

			String parameter_name_Lead_Actions_Log_A_Task = "AddTaskThroughActionsLogATask";
			verifytaskmethod.addTaskLeadMethod(driver, firstName, lastName, fullName, emailId,
					parameter_name_Lead_Actions_Log_A_Task, subject1);
			String parameter_name_Click_Lead_Name_Log_Task = "AddTaskbyclickingonLeadName";
			verifytaskmethod.addTaskLeadMethod(driver, firstName, lastName, fullName, emailId,
					parameter_name_Click_Lead_Name_Log_Task, subject2);
			String parameter_name_Action_Image_Lead_Summary = "AddTaskThroughActionImageOnLeadSummaryPage";
			verifytaskmethod.addTaskLeadMethod(driver, firstName, lastName, fullName, emailId,
					parameter_name_Action_Image_Lead_Summary, subject3);
			String parameter_name_Lead_Bottom = "AddTaskThroughLeadBottom";
			verifytaskmethod.addTaskLeadMethod(driver, firstName, lastName, fullName, emailId,
					parameter_name_Lead_Bottom, subject4);
			String parameter_name = "AddTaskThroughTaskLink";
			verifytaskmethod.addTaskLeadMethod(driver, firstName, lastName, fullName, emailId, parameter_name,
					subject5);
			String parameter_name_without_Calendar = "AddTaskThroughTaskLinkwithoutCalendar";
			verifytaskmethod.addTaskLeadMethod(driver, firstName, lastName, fullName, emailId,
					parameter_name_without_Calendar, subject6);

			// Verification of tasks on calendar
			fc.utobj().printTestStep("Verification of tasks on calendar Day Link");
			fc.crm().crm_common().CRMCalendarLnk(driver);

			fc.utobj().clickElement(driver, taskui.Day);
			boolean isSubjectPresent1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text(),'" + subject1 + "')]");
			if (isSubjectPresent1 == false) {
				fc.utobj().throwsException("was not able to verify subject1 on calendar page");
			}

			boolean isSubjectPresent2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text(),'" + subject2 + "')]");
			if (isSubjectPresent2 == false) {
				fc.utobj().throwsException("was not able to verify subject2 on calendar page");
			}

			boolean isSubjectPresent3 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text(),'" + subject3 + "')]");
			if (isSubjectPresent3 == false) {
				fc.utobj().throwsException("was not able to verify subject3 on calendar page");
			}

			boolean isSubjectPresent4 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text(),'" + subject4 + "')]");
			if (isSubjectPresent4 == false) {
				fc.utobj().throwsException("was not able to verify subject4 on calendar page");
			}

			boolean isSubjectPresent5 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text(),'" + subject5 + "')]");
			if (isSubjectPresent5 == false) {
				fc.utobj().throwsException("was not able to verify subject5 on calendar page");
			}

			// Verification of tasks is not present on calendar
			fc.utobj().printTestStep("Verification of tasks is not present on calendar");
			boolean isSubjectPresent6 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text(),'" + subject6 + "')]");
			if (!isSubjectPresent6 == false) {
				fc.utobj().throwsException("was not able to verify subject6 on calendar page");
			}

			fc.utobj().printTestStep("Verification of tasks on calendar Week Link");
			fc.utobj().clickElement(driver, taskui.Week);

			// is
			boolean isSubjectPresentweek1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text(),'" + subject1 + "')]");
			if (isSubjectPresentweek1 == false) {
				fc.utobj().throwsException("was not able to verify subject1 on calendar page");
			}

			boolean isSubjectPresentweek2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text(),'" + subject2 + "')]");
			if (isSubjectPresentweek2 == false) {
				fc.utobj().throwsException("was not able to verify subject2 on calendar page");
			}

			boolean isSubjectPresentweek3 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text(),'" + subject3 + "')]");
			if (isSubjectPresentweek3 == false) {
				fc.utobj().throwsException("was not able to verify subject3 on calendar page");
			}

			boolean isSubjectPresentweek4 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text(),'" + subject4 + "')]");
			if (isSubjectPresentweek4 == false) {
				fc.utobj().throwsException("was not able to verify subject4 on calendar page");
			}

			boolean isSubjectPresentweek5 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text(),'" + subject5 + "')]");
			if (isSubjectPresentweek5 == false) {
				fc.utobj().throwsException("was not able to verify subject5 on calendar page");
			}

			// Verification of tasks is not present on calendar
			fc.utobj().printTestStep("Verification of tasks is not present on calendar");
			boolean isSubjectPresentweek6 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text(),'" + subject6 + "')]");
			if (!isSubjectPresentweek6 == false) {
				fc.utobj().throwsException("was not able to verify subject6 on calendar page");
			}

			fc.utobj().sleep(4000);
			fc.utobj().printTestStep("Verification of tasks on calendar Month Link");
			fc.utobj().refresh(driver);
			WebElement element = driver.findElement(By.xpath("//a//span[contains(text(),'Month')]"));
			boolean monthElement = element.isDisplayed();
			if (monthElement) {
				JavascriptExecutor executor = (JavascriptExecutor) driver;
				executor.executeScript("arguments[0].click();", element);
			}

			else {
				fc.utobj().refresh(driver);
				WebElement element1 = driver.findElement(By.xpath("//a//span[contains(text(),'Month')]"));
				JavascriptExecutor executor1 = (JavascriptExecutor) driver;
				executor1.executeScript("arguments[0].click();", element1);
			}

			/*
			 * flag = "true";
			 * fc.utobj().printTestStep("Verification of tasks on calendar Month Link");
			 * while (flag = "true") { fc.utobj().refresh(driver); fc.utobj().sleep(4000);
			 * WebElement element =
			 * driver.findElement(By.xpath("//a//span[contains(text(),'Month')]")); boolean
			 * monthElement = element.isDisplayed(); if (monthElement) { JavascriptExecutor
			 * executor = (JavascriptExecutor) driver;
			 * executor.executeScript("arguments[0].click();", element); flag = false; }
			 * 
			 * }
			 */

			boolean isSubjectPresentMonth1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text(),'" + subject1 + "')]");
			if (isSubjectPresentMonth1 == false) {
				fc.utobj().throwsException("was not able to verify subject1 on calendar page");
			}

			boolean isSubjectPresentMonth2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text(),'" + subject2 + "')]");
			if (isSubjectPresentMonth2 == false) {
				fc.utobj().throwsException("was not able to verify subject2 on calendar page");
			}

			boolean isSubjectPresentMonth3 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text(),'" + subject3 + "')]");
			if (isSubjectPresentMonth3 == false) {
				fc.utobj().throwsException("was not able to verify subject3 on calendar page");
			}

			boolean isSubjectPresentMonth4 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text(),'" + subject4 + "')]");
			if (isSubjectPresentMonth4 == false) {
				fc.utobj().throwsException("was not able to verify subject4 on calendar page");
			}

			boolean isSubjectPresentMonth5 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text(),'" + subject5 + "')]");
			if (isSubjectPresentMonth5 == false) {
				fc.utobj().throwsException("was not able to verify subject5 on calendar page");
			}

			// Verification of tasks is not present on calendar
			fc.utobj().printTestStep("Verification of tasks is not present on calendar");
			boolean isSubjectPresentMonth6 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text(),'" + subject6 + "')]");
			if (!isSubjectPresentMonth6 == false) {
				fc.utobj().throwsException("was not able to verify subject6 on calendar page");
			}

			// Verification of Task Notification on Notifications Bar
			fc.utobj().printTestStep("Verification of Task Notification on Notifications Bar");

			fc.crm().crm_common().CRMTasksLnk(driver);

			fc.utobj().clickElement(driver, taskui.arrow);
			fc.utobj().clickElement(driver, "//*[@id='showNotification']");
			fc.utobj().clickElement(driver, "//a[@href='viewAllNotifications?viewCriteria=16']");

			boolean Subject1 = fc.utobj().isElementPresent(driver, "//b[contains(text(),'" + subject1 + "')]");
			if (Subject1 == false) {
				fc.utobj().throwsException("was not able to verify sunject1");
			}

			boolean Subject2 = fc.utobj().isElementPresent(driver, "//b[contains(text(),'" + subject2 + "')]");
			if (Subject2 == false) {
				fc.utobj().throwsException("was not able to verify sunject2");
			}
			boolean Subject3 = fc.utobj().isElementPresent(driver, "//b[contains(text(),'" + subject3 + "')]");
			if (Subject3 == false) {
				fc.utobj().throwsException("was not able to verify sunject3");
			}
			boolean Subject4 = fc.utobj().isElementPresent(driver, "//b[contains(text(),'" + subject4 + "')]");
			if (Subject4 == false) {
				fc.utobj().throwsException("was not able to verify sunject4");
			}
			boolean Subject5 = fc.utobj().isElementPresent(driver, "//b[contains(text(),'" + subject5 + "')]");
			if (Subject5 == false) {
				fc.utobj().throwsException("was not able to verify sunject5");
			}
			boolean Subject6 = fc.utobj().isElementPresent(driver, "//b[contains(text(),'" + subject6 + "')]");
			if (Subject6 == false) {
				fc.utobj().throwsException("was not able to verify sunject5");
			}

			// Verification of Tasks on task reports
			fc.utobj().printTestStep("Verification of Tasks on task reports");
			CRM_Verify_Task_Common_Method commonmetohod = new CRM_Verify_Task_Common_Method();
			commonmetohod.openTaskHistoryOnTaskReportandsearchforaowner(driver, corporateusername);
			fc.utobj().switchFrameById(driver, "reportiframe");
			boolean isSubjectPresentReportinfo1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//*[contains(text(),'" + subject1 + "')]");
			if (isSubjectPresentReportinfo1 == false) {

				fc.utobj().throwsException("was not able to verify subject1 on Report info page");
			}

			boolean isSubjectPresentReportinfo2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//*[contains(text(),'" + subject2 + "')]");
			if (isSubjectPresentReportinfo2 == false) {
				fc.utobj().throwsException("was not able to verify subject2 on Report info page");
			}

			boolean isSubjectPresentReportinfo3 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//*[contains(text(),'" + subject3 + "')]");
			if (isSubjectPresentReportinfo3 == false) {
				fc.utobj().throwsException("was not able to verify subject3 on Report info page");
			}

			boolean isSubjectPresentReportinfo4 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//*[contains(text(),'" + subject4 + "')]");
			if (isSubjectPresentReportinfo4 == false) {
				fc.utobj().throwsException("was not able to verify subject4 on Report info page");
			}

			boolean isSubjectPresentReportinfo5 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//*[contains(text(),'" + subject5 + "')]");
			if (isSubjectPresentReportinfo5 == false) {
				fc.utobj().throwsException("was not able to verify subject5 on Report info page");
			}

			boolean isSubjectPresentReportinfo6 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//*[contains(text(),'" + subject6 + "')]");
			if (isSubjectPresentReportinfo6 == false) {
				fc.utobj().throwsException("was not able to verify subject6 on Report info page");
			}
			System.out.println("Verification of Tasks on task reports done");
			fc.utobj().switchFrameToDefault(driver);

			// Verification of Added Task is search out through module top search at Task
			// Summary page
			fc.utobj().printTestStep(
					"Verification of Added Task is search out through module top search at Task Summary page");

			fc.crm().crm_common().CRMTasksLnk(driver);
			System.out.println(
					"Verification of Added Task is search out through module top search at Task Summary page is going to start");
			fc.utobj().sendKeys(driver, taskui.taskSearch, subject1);
			fc.utobj().clickElement(driver, "//*[@id='systemSearchBtn']");

			boolean isSubjectPresentTaskSummaryPage1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text(),'" + subject1 + "')]");
			if (isSubjectPresentTaskSummaryPage1 == false) {
				fc.utobj().throwsException("was not able to verify subject1 on Task Summary page");
			}

			fc.utobj().sendKeys(driver, taskui.taskSearch, subject2);
			fc.utobj().clickElement(driver, "//*[@id='systemSearchBtn']");

			boolean isSubjectPresentTaskSummaryPage2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text(),'" + subject2 + "')]");
			if (isSubjectPresentTaskSummaryPage2 == false) {
				fc.utobj().throwsException("was not able to verify subject2 on Task Summary page");
			}

			fc.utobj().sendKeys(driver, taskui.taskSearch, subject3);
			fc.utobj().clickElement(driver, "//*[@id='systemSearchBtn']");

			boolean isSubjectPresentTaskSummaryPage3 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text(),'" + subject3 + "')]");
			if (isSubjectPresentTaskSummaryPage3 == false) {
				fc.utobj().throwsException("was not able to verify subject3 on Task Summary page");
			}

			fc.utobj().sendKeys(driver, taskui.taskSearch, subject4);
			fc.utobj().clickElement(driver, "//*[@id='systemSearchBtn']");

			boolean isSubjectPresentTaskSummaryPage4 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text(),'" + subject4 + "')]");
			if (isSubjectPresentTaskSummaryPage4 == false) {
				fc.utobj().throwsException("was not able to verify subject4 on Task Summary page");
			}

			fc.utobj().sendKeys(driver, taskui.taskSearch, subject5);
			fc.utobj().clickElement(driver, "//*[@id='systemSearchBtn']");

			boolean isSubjectPresentTaskSummaryPage5 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text(),'" + subject5 + "')]");
			if (isSubjectPresentTaskSummaryPage5 == false) {
				fc.utobj().throwsException("was not able to verify subject5 on Task Summary page");
			}

			fc.utobj().sendKeys(driver, taskui.taskSearch, subject6);
			fc.utobj().clickElement(driver, "//*[@id='systemSearchBtn']");

			boolean isSubjectPresentTaskSummaryPage6 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text(),'" + subject6 + "')]");
			if (isSubjectPresentTaskSummaryPage6 == false) {
				fc.utobj().throwsException("was not able to verify subject6 on Task Summary page");
			}

			// Verification of tasks at Lead Primary info page
			fc.utobj().printTestStep("Verification of tasks at Lead Primary info page");
			fc.crm().crm_common().CRMLeadsLnk(driver);
			fc.utobj().sendKeys(driver, crmleadpage.searchLeads, fullnameaddedthroughcrmlead);
			fc.utobj().clickElement(driver, "//*[@id='systemSearchBtn']");
			fc.utobj().clickElement(driver, "//a[contains(text(),'" + fullnameaddedthroughcrmlead + "')]");

			fc.utobj().printTestStep("Navigate to Lead Info Page");
			boolean isSubjectPresentLeadinfo1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text(),'" + subject1 + "')]");

			if (isSubjectPresentLeadinfo1 == false) {

				fc.utobj().throwsException("was not able to verify subject1 on Lead info page");
			}

			boolean isSubjectPresentLeadinfo2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text(),'" + subject2 + "')]");
			if (isSubjectPresentLeadinfo2 == false) {
				fc.utobj().throwsException("was not able to verify subject2 on Lead info page");
			}

			boolean isSubjectPresentLeadinfo3 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text(),'" + subject3 + "')]");
			if (isSubjectPresentLeadinfo3 == false) {
				fc.utobj().throwsException("was not able to verify subject3 on Lead info page");
			}

			boolean isSubjectPresentLeadinfo4 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text(),'" + subject4 + "')]");
			if (isSubjectPresentLeadinfo4 == false) {
				fc.utobj().throwsException("was not able to verify subject4 on Lead info page");
			}

			boolean isSubjectPresentLeadinfo5 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text(),'" + subject5 + "')]");
			if (isSubjectPresentLeadinfo5 == false) {
				fc.utobj().throwsException("was not able to verify subject5 on Lead info page");
			}

			boolean isSubjectPresentLeadinfo6 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text(),'" + subject6 + "')]");
			if (isSubjectPresentLeadinfo6 == false) {
				fc.utobj().throwsException("was not able to verify subject6 on Lead info page");
			}

			// Verification of task details is shown properly on View/modify Details page of
			// task
			fc.utobj().printTestStep(
					"Verification of task details is shown properly on View/modify Details page of task");
			fc.crm().crm_common().CRMTasksLnk(driver);
			fc.utobj().printTestStep("Navigated to CRM_Task");
			fc.utobj().sendKeys(driver, taskui.taskSearch, subject1);
			fc.utobj().clickElement(driver, "//*[@id='systemSearchBtn']");

			boolean isSubjectPresentTaskSummaryPagemodifydetails = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text(),'" + subject1 + "')]");
			if (isSubjectPresentTaskSummaryPagemodifydetails == false) {
				fc.utobj().throwsException(
						"was not able to verify subject1 on Task Summary page when doing modification");
			}

			fc.utobj().clickElement(driver, ".//a[contains(text(),'" + subject1 + "')]");

			fc.commonMethods().switch_cboxIframe_frameId(driver);

			fc.utobj().printTestStep("Verifying view of the task by clicking on close button");
			fc.utobj().clickElement(driver, taskui.close);

			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().clickElement(driver, pobj.clickonActionMenuLeadSummary);
			fc.utobj().sleep(2000);
			fc.utobj().clickElement(driver, pobj.modify);

			fc.commonMethods().switch_cboxIframe_frameId(driver);

			fc.utobj().printTestStep("Modifying the task by changing the status");
			fc.utobj().selectDropDown(driver, pobj.statusTask, "Waiting For Some One");
			fc.utobj().clickElement(driver, pobj.createBtn);
			fc.utobj().sleep(2000);
			fc.utobj().switchFrameToDefault(driver);
			Thread.sleep(5000);
			String taskstatus = taskui.taskStatus.getText();
			if (taskstatus.equalsIgnoreCase("Waiting For Some One")) {
				fc.utobj().printTestStep("Verified modified status of Task");
			} else {
				fc.utobj().throwsException("was not able to modify status of task");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}

	}

	@Test(groups = { "crm", "crmemailRegional","crmtaskfinall" })
	@TestCase(createdOn = "2018-04-12", updatedOn = "2018-04-12", testCaseDescription = "Create task through regional user and verify on different places", testCaseId = "Task_Creation_RegionalUser")
	public void addTaskLeadRegional() throws IOException, Exception, ParserConfigurationException, SAXException {

		fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		String testCaseId = "Task_Creation_CorporateUser";
		String area = "Task";
		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestDatawithsqllite("crm", testCaseId, area);

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		CRMLeadsPage crmleadpage = new CRMLeadsPage(driver);
		CRMTasksPage pobj = new CRMTasksPage(driver);
		try {
			driver = fc.loginpage().login(driver);
			CRMLeadsPageTest leadPage = new CRMLeadsPageTest();

			// Adding a region and creating a regional user
			fc.utobj().printTestStep("Adding a region and creating a regional user");
			AdminUsersManageRegionalUsersAddRegionalUserPageTest regionalUserPage = new AdminUsersManageRegionalUsersAddRegionalUserPageTest();
			String testCaseId1 = "PTA_Campaign_From_Corporate_User";
			String area1 = "Campaign_Visibility";
			Map<String, String> dataSet1 = fc.utobj().readTestDatawithsqllite("crm", testCaseId1, area1);
			String regionName1 = fc.utobj().generateTestData(dataSet1.get("Regionname"));
			// username and password for regional user
			String Reguname = fc.utobj().generateTestData(dataSet1.get("Reguname"));// AreaRegion
			String Regpassword = dataSet1.get("Regpassword");
			regionalUserPage.addRegionalUserwithunamepass(driver, Reguname, regionName1, Regpassword, dataSet1,
					"Default Regional Role");
			System.out.println("Regional(R1) user id and password" + Reguname + " " + Regpassword);
			fc.loginpage().loginWithParameter(driver, Reguname, Regpassword);

			CRM_Verify_TaskUI taskui = new CRM_Verify_TaskUI(driver);
			fc.utobj().clickElement(driver, taskui.User);
			fc.utobj().clickElement(driver, taskui.Options);
			fc.utobj().moveToElement(driver, taskui.More);
			fc.utobj().clickElement(driver, taskui.More);
			fc.utobj().clickElement(driver, taskui.More);
			fc.utobj().clickElement(driver, taskui.Notifications);
			if (!fc.utobj().isSelected(driver, taskui.testcreationcheckbox)) {
				fc.utobj().clickElement(driver, taskui.testcreationcheckbox);
			}
			fc.utobj().clickElement(driver, taskui.save);

			fc.utobj().printTestStep("Navigate To CRM > Leads > Leads Summary");
			fc.utobj().printTestStep("Add Lead");

			String firstName = fc.utobj().generateTestData(dataSet.get("firstName"));
			String lastName = fc.utobj().generateTestData(dataSet.get("lastName"));
			String company = fc.utobj().generateTestData(dataSet.get("company"));
			String assignTo = "Regional";
			String city = fc.utobj().generateTestData(dataSet.get("city"));
			String address = fc.utobj().generateTestData(dataSet.get("address"));
			String suffix = fc.utobj().generateTestData(dataSet.get("suffix"));
			String jobTitle = fc.utobj().generateTestData(dataSet.get("jobTitle"));
			String comment = fc.utobj().generateTestData(dataSet.get("comment"));
			String regionName = regionName1;
			String regionalUser = Reguname + " " + Reguname;
			String franchiseId = "";
			String franchiseUser = "";
			String fullName = "";// corpUser.getuserFullName();
			String fullnameaddedthroughcrmlead = firstName + " " + lastName;

			leadPage.addLeadwithSqlite(driver, dataSet, firstName, lastName, company, assignTo, city, address, suffix,
					jobTitle, comment, fullName, regionName, regionalUser, franchiseId, franchiseUser, config);

			CRM_Verify_Task_Method verifytaskmethod = new CRM_Verify_Task_Method();
			String subject1 = fc.utobj().generateTestData(dataSet.get("subject"));
			String subject2 = fc.utobj().generateTestData(dataSet.get("subject"));
			String subject3 = fc.utobj().generateTestData(dataSet.get("subject"));
			String subject4 = fc.utobj().generateTestData(dataSet.get("subject"));
			String subject5 = fc.utobj().generateTestData(dataSet.get("subject"));
			String subject6 = fc.utobj().generateTestData(dataSet.get("subject"));
			String emailId = dataSet.get("emailId");

			String parameter_name_Lead_Actions_Log_A_Task = "AddTaskThroughActionsLogATask";
			verifytaskmethod.addTaskLeadMethod(driver, firstName, lastName, fullName, emailId,
					parameter_name_Lead_Actions_Log_A_Task, subject1);
			String parameter_name_Click_Lead_Name_Log_Task = "AddTaskbyclickingonLeadName";
			verifytaskmethod.addTaskLeadMethod(driver, firstName, lastName, fullName, emailId,
					parameter_name_Click_Lead_Name_Log_Task, subject2);
			String parameter_name_Action_Image_Lead_Summary = "AddTaskThroughActionImageOnLeadSummaryPage";
			verifytaskmethod.addTaskLeadMethod(driver, firstName, lastName, fullName, emailId,
					parameter_name_Action_Image_Lead_Summary, subject3);
			String parameter_name_Lead_Bottom = "AddTaskThroughLeadBottom";
			verifytaskmethod.addTaskLeadMethod(driver, firstName, lastName, fullName, emailId,
					parameter_name_Lead_Bottom, subject4);
			String parameter_name = "AddTaskThroughTaskLink";
			verifytaskmethod.addTaskLeadMethod(driver, firstName, lastName, fullName, emailId, parameter_name,
					subject5);
			String parameter_name_without_Calendar = "AddTaskThroughTaskLinkwithoutCalendar";
			verifytaskmethod.addTaskLeadMethod(driver, firstName, lastName, fullName, emailId,
					parameter_name_without_Calendar, subject6);

			// Verification of tasks on calendar
			fc.utobj().printTestStep("Verification of tasks on calendar Day Link");
			fc.crm().crm_common().CRMCalendarLnk(driver);

			fc.utobj().clickElement(driver, taskui.Day);
			boolean isSubjectPresent1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text(),'" + subject1 + "')]");
			if (isSubjectPresent1 == false) {
				fc.utobj().throwsException("was not able to verify subject1 on calendar page");
			}

			boolean isSubjectPresent2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text(),'" + subject2 + "')]");
			if (isSubjectPresent2 == false) {
				fc.utobj().throwsException("was not able to verify subject2 on calendar page");
			}

			boolean isSubjectPresent3 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text(),'" + subject3 + "')]");
			if (isSubjectPresent3 == false) {
				fc.utobj().throwsException("was not able to verify subject3 on calendar page");
			}

			boolean isSubjectPresent4 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text(),'" + subject4 + "')]");
			if (isSubjectPresent4 == false) {
				fc.utobj().throwsException("was not able to verify subject4 on calendar page");
			}

			boolean isSubjectPresent5 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text(),'" + subject5 + "')]");
			if (isSubjectPresent5 == false) {
				fc.utobj().throwsException("was not able to verify subject5 on calendar page");
			}

			// Verification of tasks is not present on calendar
			fc.utobj().printTestStep("Verification of tasks is not present on calendar");
			boolean isSubjectPresent6 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text(),'" + subject6 + "')]");
			if (!isSubjectPresent6 == false) {
				fc.utobj().throwsException("was not able to verify subject6 on calendar page");
			}

			fc.utobj().printTestStep("Verification of tasks on calendar Week Link");
			fc.utobj().clickElement(driver, taskui.Week);

			// is
			boolean isSubjectPresentweek1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text(),'" + subject1 + "')]");
			if (isSubjectPresentweek1 == false) {
				fc.utobj().throwsException("was not able to verify subject1 on calendar page");
			}

			boolean isSubjectPresentweek2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text(),'" + subject2 + "')]");
			if (isSubjectPresentweek2 == false) {
				fc.utobj().throwsException("was not able to verify subject2 on calendar page");
			}

			boolean isSubjectPresentweek3 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text(),'" + subject3 + "')]");
			if (isSubjectPresentweek3 == false) {
				fc.utobj().throwsException("was not able to verify subject3 on calendar page");
			}

			boolean isSubjectPresentweek4 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text(),'" + subject4 + "')]");
			if (isSubjectPresentweek4 == false) {
				fc.utobj().throwsException("was not able to verify subject4 on calendar page");
			}

			boolean isSubjectPresentweek5 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text(),'" + subject5 + "')]");
			if (isSubjectPresentweek5 == false) {
				fc.utobj().throwsException("was not able to verify subject5 on calendar page");
			}

			// Verification of tasks is not present on calendar
			fc.utobj().printTestStep("Verification of tasks is not present on calendar");
			boolean isSubjectPresentweek6 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text(),'" + subject6 + "')]");
			if (!isSubjectPresentweek6 == false) {
				fc.utobj().throwsException("was not able to verify subject6 on calendar page");
			}

			fc.utobj().sleep(4000);
			fc.utobj().printTestStep("Verification of tasks on calendar Month Link");
			fc.utobj().refresh(driver);
			WebElement element = driver.findElement(By.xpath("//a//span[contains(text(),'Month')]"));
			boolean monthElement = element.isDisplayed();
			if (monthElement) {
				JavascriptExecutor executor = (JavascriptExecutor) driver;
				executor.executeScript("arguments[0].click();", element);
			}

			else {
				fc.utobj().refresh(driver);
				WebElement element1 = driver.findElement(By.xpath("//a//span[contains(text(),'Month')]"));
				JavascriptExecutor executor1 = (JavascriptExecutor) driver;
				executor1.executeScript("arguments[0].click();", element1);
			}

			boolean isSubjectPresentMonth1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text(),'" + subject1 + "')]");
			if (isSubjectPresentMonth1 == false) {
				fc.utobj().throwsException("was not able to verify subject1 on calendar page");
			}

			boolean isSubjectPresentMonth2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text(),'" + subject2 + "')]");
			if (isSubjectPresentMonth2 == false) {
				fc.utobj().throwsException("was not able to verify subject2 on calendar page");
			}

			boolean isSubjectPresentMonth3 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text(),'" + subject3 + "')]");
			if (isSubjectPresentMonth3 == false) {
				fc.utobj().throwsException("was not able to verify subject3 on calendar page");
			}

			boolean isSubjectPresentMonth4 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text(),'" + subject4 + "')]");
			if (isSubjectPresentMonth4 == false) {
				fc.utobj().throwsException("was not able to verify subject4 on calendar page");
			}

			boolean isSubjectPresentMonth5 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text(),'" + subject5 + "')]");
			if (isSubjectPresentMonth5 == false) {
				fc.utobj().throwsException("was not able to verify subject5 on calendar page");
			}

			// Verification of tasks is not present on calendar
			fc.utobj().printTestStep("Verification of tasks is not present on calendar");
			boolean isSubjectPresentMonth6 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text(),'" + subject6 + "')]");
			if (!isSubjectPresentMonth6 == false) {
				fc.utobj().throwsException("was not able to verify subject6 on calendar page");
			}

			// Verification of Task Notification on Notifications Bar
			fc.utobj().printTestStep("Verification of Task Notification on Notifications Bar");

			fc.crm().crm_common().CRMTasksLnk(driver);

			fc.utobj().clickElement(driver, taskui.arrow);
			fc.utobj().clickElement(driver, "//*[@id='showNotification']");
			fc.utobj().clickElement(driver, "//a[@href='viewAllNotifications?viewCriteria=16']");

			boolean Subject1 = fc.utobj().isElementPresent(driver, "//b[contains(text(),'" + subject1 + "')]");
			if (Subject1 == false) {
				fc.utobj().throwsException("was not able to verify sunject1");
			}

			boolean Subject2 = fc.utobj().isElementPresent(driver, "//b[contains(text(),'" + subject2 + "')]");
			if (Subject2 == false) {
				fc.utobj().throwsException("was not able to verify sunject2");
			}
			boolean Subject3 = fc.utobj().isElementPresent(driver, "//b[contains(text(),'" + subject3 + "')]");
			if (Subject3 == false) {
				fc.utobj().throwsException("was not able to verify sunject3");
			}
			boolean Subject4 = fc.utobj().isElementPresent(driver, "//b[contains(text(),'" + subject4 + "')]");
			if (Subject4 == false) {
				fc.utobj().throwsException("was not able to verify sunject4");
			}
			boolean Subject5 = fc.utobj().isElementPresent(driver, "//b[contains(text(),'" + subject5 + "')]");
			if (Subject5 == false) {
				fc.utobj().throwsException("was not able to verify sunject5");
			}
			boolean Subject6 = fc.utobj().isElementPresent(driver, "//b[contains(text(),'" + subject6 + "')]");
			if (Subject6 == false) {
				fc.utobj().throwsException("was not able to verify sunject5");
			}

			// Verification of Tasks on task reports
			fc.utobj().printTestStep("Verification of Tasks on task reports");
			CRM_Verify_Task_Common_Method commonmetohod = new CRM_Verify_Task_Common_Method();
			commonmetohod.openTaskHistoryOnTaskReportandsearchforaowner(driver, regionalUser);
			fc.utobj().switchFrameById(driver, "reportiframe");
			boolean isSubjectPresentReportinfo1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//*[contains(text(),'" + subject1 + "')]");
			if (isSubjectPresentReportinfo1 == false) {

				fc.utobj().throwsException("was not able to verify subject1 on Report info page");
			}

			boolean isSubjectPresentReportinfo2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//*[contains(text(),'" + subject2 + "')]");
			if (isSubjectPresentReportinfo2 == false) {
				fc.utobj().throwsException("was not able to verify subject2 on Report info page");
			}

			boolean isSubjectPresentReportinfo3 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//*[contains(text(),'" + subject3 + "')]");
			if (isSubjectPresentReportinfo3 == false) {
				fc.utobj().throwsException("was not able to verify subject3 on Report info page");
			}

			boolean isSubjectPresentReportinfo4 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//*[contains(text(),'" + subject4 + "')]");
			if (isSubjectPresentReportinfo4 == false) {
				fc.utobj().throwsException("was not able to verify subject4 on Report info page");
			}

			boolean isSubjectPresentReportinfo5 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//*[contains(text(),'" + subject5 + "')]");
			if (isSubjectPresentReportinfo5 == false) {
				fc.utobj().throwsException("was not able to verify subject5 on Report info page");
			}

			boolean isSubjectPresentReportinfo6 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//*[contains(text(),'" + subject6 + "')]");
			if (isSubjectPresentReportinfo6 == false) {
				fc.utobj().throwsException("was not able to verify subject6 on Report info page");
			}
			System.out.println("Verification of Tasks on task reports done");
			fc.utobj().switchFrameToDefault(driver);

			// Verification of Added Task is search out through module top search at Task
			// Summary page
			fc.utobj().printTestStep(
					"Verification of Added Task is search out through module top search at Task Summary page");

			fc.crm().crm_common().CRMTasksLnk(driver);
			System.out.println(
					"Verification of Added Task is search out through module top search at Task Summary page is going to start");
			fc.utobj().sendKeys(driver, taskui.taskSearch, subject1);
			fc.utobj().clickElement(driver, "//*[@id='systemSearchBtn']");

			boolean isSubjectPresentTaskSummaryPage1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text(),'" + subject1 + "')]");
			if (isSubjectPresentTaskSummaryPage1 == false) {
				fc.utobj().throwsException("was not able to verify subject1 on Task Summary page");
			}

			fc.utobj().sendKeys(driver, taskui.taskSearch, subject2);
			fc.utobj().clickElement(driver, "//*[@id='systemSearchBtn']");

			boolean isSubjectPresentTaskSummaryPage2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text(),'" + subject2 + "')]");
			if (isSubjectPresentTaskSummaryPage2 == false) {
				fc.utobj().throwsException("was not able to verify subject2 on Task Summary page");
			}

			fc.utobj().sendKeys(driver, taskui.taskSearch, subject3);
			fc.utobj().clickElement(driver, "//*[@id='systemSearchBtn']");

			boolean isSubjectPresentTaskSummaryPage3 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text(),'" + subject3 + "')]");
			if (isSubjectPresentTaskSummaryPage3 == false) {
				fc.utobj().throwsException("was not able to verify subject3 on Task Summary page");
			}

			fc.utobj().sendKeys(driver, taskui.taskSearch, subject4);
			fc.utobj().clickElement(driver, "//*[@id='systemSearchBtn']");

			boolean isSubjectPresentTaskSummaryPage4 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text(),'" + subject4 + "')]");
			if (isSubjectPresentTaskSummaryPage4 == false) {
				fc.utobj().throwsException("was not able to verify subject4 on Task Summary page");
			}

			fc.utobj().sendKeys(driver, taskui.taskSearch, subject5);
			fc.utobj().clickElement(driver, "//*[@id='systemSearchBtn']");

			boolean isSubjectPresentTaskSummaryPage5 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text(),'" + subject5 + "')]");
			if (isSubjectPresentTaskSummaryPage5 == false) {
				fc.utobj().throwsException("was not able to verify subject5 on Task Summary page");
			}

			fc.utobj().sendKeys(driver, taskui.taskSearch, subject6);
			fc.utobj().clickElement(driver, "//*[@id='systemSearchBtn']");

			boolean isSubjectPresentTaskSummaryPage6 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text(),'" + subject6 + "')]");
			if (isSubjectPresentTaskSummaryPage6 == false) {
				fc.utobj().throwsException("was not able to verify subject6 on Task Summary page");
			}

			// Verification of tasks at Lead Primary info page
			fc.utobj().printTestStep("Verification of tasks at Lead Primary info page");
			fc.crm().crm_common().CRMLeadsLnk(driver);
			fc.utobj().sendKeys(driver, crmleadpage.searchLeads, fullnameaddedthroughcrmlead);
			fc.utobj().clickElement(driver, "//*[@id='systemSearchBtn']");
			fc.utobj().clickElement(driver, "//a[contains(text(),'" + fullnameaddedthroughcrmlead + "')]");

			fc.utobj().printTestStep("Navigate to Lead Info Page");
			boolean isSubjectPresentLeadinfo1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text(),'" + subject1 + "')]");

			if (isSubjectPresentLeadinfo1 == false) {

				fc.utobj().throwsException("was not able to verify subject1 on Lead info page");
			}

			boolean isSubjectPresentLeadinfo2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text(),'" + subject2 + "')]");
			if (isSubjectPresentLeadinfo2 == false) {
				fc.utobj().throwsException("was not able to verify subject2 on Lead info page");
			}

			boolean isSubjectPresentLeadinfo3 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text(),'" + subject3 + "')]");
			if (isSubjectPresentLeadinfo3 == false) {
				fc.utobj().throwsException("was not able to verify subject3 on Lead info page");
			}

			boolean isSubjectPresentLeadinfo4 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text(),'" + subject4 + "')]");
			if (isSubjectPresentLeadinfo4 == false) {
				fc.utobj().throwsException("was not able to verify subject4 on Lead info page");
			}

			boolean isSubjectPresentLeadinfo5 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text(),'" + subject5 + "')]");
			if (isSubjectPresentLeadinfo5 == false) {
				fc.utobj().throwsException("was not able to verify subject5 on Lead info page");
			}

			boolean isSubjectPresentLeadinfo6 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text(),'" + subject6 + "')]");
			if (isSubjectPresentLeadinfo6 == false) {
				fc.utobj().throwsException("was not able to verify subject6 on Lead info page");
			}

			// Verification of task details is shown properly on View/modify Details page of
			// task
			fc.utobj().printTestStep(
					"Verification of task details is shown properly on View/modify Details page of task");
			fc.crm().crm_common().CRMTasksLnk(driver);
			fc.utobj().printTestStep("Navigated to CRM_Task");
			fc.utobj().sendKeys(driver, taskui.taskSearch, subject1);
			fc.utobj().clickElement(driver, "//*[@id='systemSearchBtn']");

			boolean isSubjectPresentTaskSummaryPagemodifydetails = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text(),'" + subject1 + "')]");
			if (isSubjectPresentTaskSummaryPagemodifydetails == false) {
				fc.utobj().throwsException(
						"was not able to verify subject1 on Task Summary page when doing modification");
			}

			fc.utobj().clickElement(driver, ".//a[contains(text(),'" + subject1 + "')]");

			fc.commonMethods().switch_cboxIframe_frameId(driver);

			fc.utobj().printTestStep("Verifying view of the task by clicking on close button");
			fc.utobj().clickElement(driver, taskui.close);

			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().clickElement(driver, pobj.clickonActionMenuLeadSummary);
			fc.utobj().sleep(2000);
			fc.utobj().clickElement(driver, pobj.modify);

			fc.commonMethods().switch_cboxIframe_frameId(driver);

			fc.utobj().printTestStep("Modifying the task by changing the status");
			fc.utobj().selectDropDown(driver, pobj.statusTask, "Waiting For Some One");
			fc.utobj().clickElement(driver, pobj.createBtn);
			fc.utobj().sleep(2000);
			fc.utobj().switchFrameToDefault(driver);
			Thread.sleep(5000);
			String taskstatus = taskui.taskStatus.getText();
			if (taskstatus.equalsIgnoreCase("Waiting For Some One")) {
				fc.utobj().printTestStep("Verified modified status of Task");
			} else {
				fc.utobj().throwsException("was not able to modify status of task");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "crm", "crmLeadFranchisefinal","crmtaskfinall" })
	@TestCase(createdOn = "2018-05-12", updatedOn = "2018-05-12", testCaseDescription = "Create task through Franchise owner user and verify on different places", testCaseId = "Task_Creation_FranchiseOwnerUser")
	public void addTaskLeadFranchiseeOwner() throws IOException, Exception, ParserConfigurationException, SAXException {

		fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		String testCaseId = "Task_Creation_CorporateUser";
		String area = "Task";
		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestDatawithsqllite("crm", testCaseId, area);

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		CRMLeadsPage crmleadpage = new CRMLeadsPage(driver);
		CRMTasksPage pobj = new CRMTasksPage(driver);
		try {
			driver = fc.loginpage().login(driver);
			CRMLeadsPageTest leadPage = new CRMLeadsPageTest();

			String testCaseId1 = "PTA_Campaign_From_Corporate_User";
			String area1 = "Campaign_Visibility";
			Map<String, String> dataSet1 = fc.utobj().readTestDatawithsqllite("crm", testCaseId1, area1);

			// Adding a region and creating a regional user who has role to view private
			// campaign(Region and R1)
			// R1
			fc.utobj().printTestStep(
					"Adding a region and creating a regional user who has role to view private campaign(Region and R1)");
			String regionName1 = fc.utobj().generateTestData(dataSet1.get("Regionname"));
			AdminAreaRegionAddAreaRegionPageTest p1 = new AdminAreaRegionAddAreaRegionPageTest();
			p1.addAreaRegionwithSQlite(driver, regionName1, dataSet1);

			// Mu user
			// Add a franchise location with same region
			// L6
			fc.utobj().printTestStep("Add a franchise location with same region");
			String firstNameF5 = fc.utobj().generateTestData(dataSet1.get("Franlocfirstname"));
			String lastNameF5 = fc.utobj().generateTestData(dataSet1.get("Franloclasttname"));
			fc.utobj().printTestStep("Naviaget To Admin > Franchise Location > Add Franchise Location");
			AdminFranchiseLocationAddFranchiseLocationPageTest franchiseLocation5 = new AdminFranchiseLocationAddFranchiseLocationPageTest();
			String franchiseId5 = fc.utobj().generateTestData(dataSet1.get("Franchiseid"));
			franchiseLocation5.addFranchiseLocationForMktwithoutaddingarearegion(driver, franchiseId5, regionName1,
					firstNameF5, lastNameF5, dataSet1);

			// Add Franchise Mu User
			// V1
			// Add Franchise owner mu User who has role to see private campaign
			fc.utobj().printTestStep("Add Franchise owner User who has role to see private campaign");
			AdminUsersManageManageFranchiseUsersPageTest addFranUsermu = new AdminUsersManageManageFranchiseUsersPageTest();
			// username and password for Owner type franchise user
			String userNameFOmu = fc.utobj().generateTestData(dataSet1.get("Franowneruname"));
			String roleNameFOmu = "Default Franchise Role";// dataSet.get("Franownerrolename");
			String passwordFOmu = dataSet1.get("Franownerpassword");
			String emailIdFOmu = dataSet1.get("Franowneremail") + "@staffex.com";
			userNameFOmu = addFranUsermu.addFranchiseUserwithoutchangeinsettings(driver, userNameFOmu, passwordFOmu,
					franchiseId5, roleNameFOmu, emailIdFOmu, dataSet1);
			System.out
					.println("(V1)Username and password for franchise mu user is" + userNameFOmu + " " + passwordFOmu);
			fc.loginpage().loginWithParameter(driver, userNameFOmu, passwordFOmu);

			CRM_Verify_TaskUI taskui = new CRM_Verify_TaskUI(driver);
			fc.utobj().clickElement(driver, taskui.User);
			fc.utobj().clickElement(driver, taskui.Options);
			fc.utobj().moveToElement(driver, taskui.More);
			fc.utobj().clickElement(driver, taskui.More);
			fc.utobj().clickElement(driver, taskui.More);
			fc.utobj().clickElement(driver, taskui.Notifications);
			if (!fc.utobj().isSelected(driver, taskui.testcreationcheckbox)) {
				fc.utobj().clickElement(driver, taskui.testcreationcheckbox);
			}
			fc.utobj().clickElement(driver, taskui.save);

			fc.utobj().printTestStep("Navigate To CRM > Leads > Leads Summary");
			fc.utobj().printTestStep("Add Lead");

			String firstName = fc.utobj().generateTestData(dataSet.get("firstName"));
			String lastName = fc.utobj().generateTestData(dataSet.get("lastName"));
			String company = fc.utobj().generateTestData(dataSet.get("company"));
			String assignTo = "Franchise";
			String city = fc.utobj().generateTestData(dataSet.get("city"));
			String address = fc.utobj().generateTestData(dataSet.get("address"));
			String suffix = fc.utobj().generateTestData(dataSet.get("suffix"));
			String jobTitle = fc.utobj().generateTestData(dataSet.get("jobTitle"));
			String comment = fc.utobj().generateTestData(dataSet.get("comment"));
			String regionName = regionName1;
			String regionalUser = "";// Reguname+" "+Reguname;
			String franchiseId = franchiseId5;
			String franchiseUser = firstNameF5 + " " + lastNameF5;
			String fullName = "";// corpUser.getuserFullName();
			String fullnameaddedthroughcrmlead = firstName + " " + lastName;

			leadPage.addLeadwithSqlite(driver, dataSet, firstName, lastName, company, assignTo, city, address, suffix,
					jobTitle, comment, fullName, regionName, regionalUser, franchiseId, franchiseUser, config);

			CRM_Verify_Task_Method verifytaskmethod = new CRM_Verify_Task_Method();
			String subject1 = fc.utobj().generateTestData(dataSet.get("subject"));
			String subject2 = fc.utobj().generateTestData(dataSet.get("subject"));
			String subject3 = fc.utobj().generateTestData(dataSet.get("subject"));
			String subject4 = fc.utobj().generateTestData(dataSet.get("subject"));
			String subject5 = fc.utobj().generateTestData(dataSet.get("subject"));
			String subject6 = fc.utobj().generateTestData(dataSet.get("subject"));
			String emailId = dataSet.get("emailId");

			String parameter_name_Lead_Actions_Log_A_Task = "AddTaskThroughActionsLogATask";
			verifytaskmethod.addTaskLeadMethod(driver, firstName, lastName, fullName, emailId,
					parameter_name_Lead_Actions_Log_A_Task, subject1);
			String parameter_name_Click_Lead_Name_Log_Task = "AddTaskbyclickingonLeadName";
			verifytaskmethod.addTaskLeadMethod(driver, firstName, lastName, fullName, emailId,
					parameter_name_Click_Lead_Name_Log_Task, subject2);
			String parameter_name_Action_Image_Lead_Summary = "AddTaskThroughActionImageOnLeadSummaryPage";
			verifytaskmethod.addTaskLeadMethod(driver, firstName, lastName, fullName, emailId,
					parameter_name_Action_Image_Lead_Summary, subject3);
			String parameter_name_Lead_Bottom = "AddTaskThroughLeadBottom";
			verifytaskmethod.addTaskLeadMethod(driver, firstName, lastName, fullName, emailId,
					parameter_name_Lead_Bottom, subject4);
			String parameter_name = "AddTaskThroughTaskLink";
			verifytaskmethod.addTaskLeadMethod(driver, firstName, lastName, fullName, emailId, parameter_name,
					subject5);
			String parameter_name_without_Calendar = "AddTaskThroughTaskLinkwithoutCalendar";
			verifytaskmethod.addTaskLeadMethod(driver, firstName, lastName, fullName, emailId,
					parameter_name_without_Calendar, subject6);

			// Verification of tasks on calendar
			fc.utobj().printTestStep("Verification of tasks on calendar Day Link");
			fc.crm().crm_common().CRMCalendarLnk(driver);

			fc.utobj().clickElement(driver, taskui.Day);
			boolean isSubjectPresent1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text(),'" + subject1 + "')]");
			if (isSubjectPresent1 == false) {
				fc.utobj().throwsException("was not able to verify subject1 on calendar page");
			}

			boolean isSubjectPresent2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text(),'" + subject2 + "')]");
			if (isSubjectPresent2 == false) {
				fc.utobj().throwsException("was not able to verify subject2 on calendar page");
			}

			boolean isSubjectPresent3 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text(),'" + subject3 + "')]");
			if (isSubjectPresent3 == false) {
				fc.utobj().throwsException("was not able to verify subject3 on calendar page");
			}

			boolean isSubjectPresent4 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text(),'" + subject4 + "')]");
			if (isSubjectPresent4 == false) {
				fc.utobj().throwsException("was not able to verify subject4 on calendar page");
			}

			boolean isSubjectPresent5 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text(),'" + subject5 + "')]");
			if (isSubjectPresent5 == false) {
				fc.utobj().throwsException("was not able to verify subject5 on calendar page");
			}

			// Verification of tasks is not present on calendar
			fc.utobj().printTestStep("Verification of tasks is not present on calendar");
			boolean isSubjectPresent6 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text(),'" + subject6 + "')]");
			if (!isSubjectPresent6 == false) {
				fc.utobj().throwsException("was not able to verify subject6 on calendar page");
			}

			fc.utobj().printTestStep("Verification of tasks on calendar Week Link");
			fc.utobj().clickElement(driver, taskui.Week);

			// is
			boolean isSubjectPresentweek1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text(),'" + subject1 + "')]");
			if (isSubjectPresentweek1 == false) {
				fc.utobj().throwsException("was not able to verify subject1 on calendar page");
			}

			boolean isSubjectPresentweek2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text(),'" + subject2 + "')]");
			if (isSubjectPresentweek2 == false) {
				fc.utobj().throwsException("was not able to verify subject2 on calendar page");
			}

			boolean isSubjectPresentweek3 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text(),'" + subject3 + "')]");
			if (isSubjectPresentweek3 == false) {
				fc.utobj().throwsException("was not able to verify subject3 on calendar page");
			}

			boolean isSubjectPresentweek4 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text(),'" + subject4 + "')]");
			if (isSubjectPresentweek4 == false) {
				fc.utobj().throwsException("was not able to verify subject4 on calendar page");
			}

			boolean isSubjectPresentweek5 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text(),'" + subject5 + "')]");
			if (isSubjectPresentweek5 == false) {
				fc.utobj().throwsException("was not able to verify subject5 on calendar page");
			}

			// Verification of tasks is not present on calendar
			fc.utobj().printTestStep("Verification of tasks is not present on calendar");
			boolean isSubjectPresentweek6 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text(),'" + subject6 + "')]");
			if (!isSubjectPresentweek6 == false) {
				fc.utobj().throwsException("was not able to verify subject6 on calendar page");
			}

			fc.utobj().sleep(4000);
			fc.utobj().printTestStep("Verification of tasks on calendar Month Link");
			fc.utobj().refresh(driver);
			WebElement element = driver.findElement(By.xpath("//a//span[contains(text(),'Month')]"));
			boolean monthElement = element.isDisplayed();
			if (monthElement) {
				JavascriptExecutor executor = (JavascriptExecutor) driver;
				executor.executeScript("arguments[0].click();", element);
			}

			else {
				fc.utobj().refresh(driver);
				WebElement element1 = driver.findElement(By.xpath("//a//span[contains(text(),'Month')]"));
				JavascriptExecutor executor1 = (JavascriptExecutor) driver;
				executor1.executeScript("arguments[0].click();", element1);
			}

			boolean isSubjectPresentMonth1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text(),'" + subject1 + "')]");
			if (isSubjectPresentMonth1 == false) {
				fc.utobj().throwsException("was not able to verify subject1 on calendar page");
			}

			boolean isSubjectPresentMonth2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text(),'" + subject2 + "')]");
			if (isSubjectPresentMonth2 == false) {
				fc.utobj().throwsException("was not able to verify subject2 on calendar page");
			}

			boolean isSubjectPresentMonth3 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text(),'" + subject3 + "')]");
			if (isSubjectPresentMonth3 == false) {
				fc.utobj().throwsException("was not able to verify subject3 on calendar page");
			}

			boolean isSubjectPresentMonth4 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text(),'" + subject4 + "')]");
			if (isSubjectPresentMonth4 == false) {
				fc.utobj().throwsException("was not able to verify subject4 on calendar page");
			}

			boolean isSubjectPresentMonth5 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text(),'" + subject5 + "')]");
			if (isSubjectPresentMonth5 == false) {
				fc.utobj().throwsException("was not able to verify subject5 on calendar page");
			}

			// Verification of tasks is not present on calendar
			fc.utobj().printTestStep("Verification of tasks is not present on calendar");
			boolean isSubjectPresentMonth6 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text(),'" + subject6 + "')]");
			if (!isSubjectPresentMonth6 == false) {
				fc.utobj().throwsException("was not able to verify subject6 on calendar page");
			}

			// Verification of Task Notification on Notifications Bar
			fc.utobj().printTestStep("Verification of Task Notification on Notifications Bar");

			fc.crm().crm_common().CRMTasksLnk(driver);

			fc.utobj().clickElement(driver, taskui.arrow);
			fc.utobj().clickElement(driver, "//*[@id='showNotification']");
			fc.utobj().clickElement(driver, "//a[@href='viewAllNotifications?viewCriteria=16']");

			boolean Subject1 = fc.utobj().isElementPresent(driver, "//b[contains(text(),'" + subject1 + "')]");
			if (Subject1 == false) {
				fc.utobj().throwsException("was not able to verify sunject1");
			}

			boolean Subject2 = fc.utobj().isElementPresent(driver, "//b[contains(text(),'" + subject2 + "')]");
			if (Subject2 == false) {
				fc.utobj().throwsException("was not able to verify sunject2");
			}
			boolean Subject3 = fc.utobj().isElementPresent(driver, "//b[contains(text(),'" + subject3 + "')]");
			if (Subject3 == false) {
				fc.utobj().throwsException("was not able to verify sunject3");
			}
			boolean Subject4 = fc.utobj().isElementPresent(driver, "//b[contains(text(),'" + subject4 + "')]");
			if (Subject4 == false) {
				fc.utobj().throwsException("was not able to verify sunject4");
			}
			boolean Subject5 = fc.utobj().isElementPresent(driver, "//b[contains(text(),'" + subject5 + "')]");
			if (Subject5 == false) {
				fc.utobj().throwsException("was not able to verify sunject5");
			}
			boolean Subject6 = fc.utobj().isElementPresent(driver, "//b[contains(text(),'" + subject6 + "')]");
			if (Subject6 == false) {
				fc.utobj().throwsException("was not able to verify sunject5");
			}

			// Verification of Tasks on task reports
			fc.utobj().printTestStep("Verification of Tasks on task reports");
			CRM_Verify_Task_Common_Method commonmetohod = new CRM_Verify_Task_Common_Method();
			commonmetohod.openTaskHistoryOnTaskReportandsearchforaowner(driver, franchiseUser);
			fc.utobj().switchFrameById(driver, "reportiframe");
			boolean isSubjectPresentReportinfo1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//*[contains(text(),'" + subject1 + "')]");
			if (isSubjectPresentReportinfo1 == false) {

				fc.utobj().throwsException("was not able to verify subject1 on Report info page");
			}

			boolean isSubjectPresentReportinfo2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//*[contains(text(),'" + subject2 + "')]");
			if (isSubjectPresentReportinfo2 == false) {
				fc.utobj().throwsException("was not able to verify subject2 on Report info page");
			}

			boolean isSubjectPresentReportinfo3 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//*[contains(text(),'" + subject3 + "')]");
			if (isSubjectPresentReportinfo3 == false) {
				fc.utobj().throwsException("was not able to verify subject3 on Report info page");
			}

			boolean isSubjectPresentReportinfo4 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//*[contains(text(),'" + subject4 + "')]");
			if (isSubjectPresentReportinfo4 == false) {
				fc.utobj().throwsException("was not able to verify subject4 on Report info page");
			}

			boolean isSubjectPresentReportinfo5 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//*[contains(text(),'" + subject5 + "')]");
			if (isSubjectPresentReportinfo5 == false) {
				fc.utobj().throwsException("was not able to verify subject5 on Report info page");
			}

			boolean isSubjectPresentReportinfo6 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//*[contains(text(),'" + subject6 + "')]");
			if (isSubjectPresentReportinfo6 == false) {
				fc.utobj().throwsException("was not able to verify subject6 on Report info page");
			}
			System.out.println("Verification of Tasks on task reports done");
			fc.utobj().switchFrameToDefault(driver);

			// Verification of Added Task is search out through module top search at Task
			// Summary page
			fc.utobj().printTestStep(
					"Verification of Added Task is search out through module top search at Task Summary page");

			fc.crm().crm_common().CRMTasksLnk(driver);
			System.out.println(
					"Verification of Added Task is search out through module top search at Task Summary page is going to start");
			fc.utobj().sendKeys(driver, taskui.taskSearch, subject1);
			fc.utobj().clickElement(driver, "//*[@id='systemSearchBtn']");

			boolean isSubjectPresentTaskSummaryPage1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text(),'" + subject1 + "')]");
			if (isSubjectPresentTaskSummaryPage1 == false) {
				fc.utobj().throwsException("was not able to verify subject1 on Task Summary page");
			}

			fc.utobj().sendKeys(driver, taskui.taskSearch, subject2);
			fc.utobj().clickElement(driver, "//*[@id='systemSearchBtn']");

			boolean isSubjectPresentTaskSummaryPage2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text(),'" + subject2 + "')]");
			if (isSubjectPresentTaskSummaryPage2 == false) {
				fc.utobj().throwsException("was not able to verify subject2 on Task Summary page");
			}

			fc.utobj().sendKeys(driver, taskui.taskSearch, subject3);
			fc.utobj().clickElement(driver, "//*[@id='systemSearchBtn']");

			boolean isSubjectPresentTaskSummaryPage3 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text(),'" + subject3 + "')]");
			if (isSubjectPresentTaskSummaryPage3 == false) {
				fc.utobj().throwsException("was not able to verify subject3 on Task Summary page");
			}

			fc.utobj().sendKeys(driver, taskui.taskSearch, subject4);
			fc.utobj().clickElement(driver, "//*[@id='systemSearchBtn']");

			boolean isSubjectPresentTaskSummaryPage4 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text(),'" + subject4 + "')]");
			if (isSubjectPresentTaskSummaryPage4 == false) {
				fc.utobj().throwsException("was not able to verify subject4 on Task Summary page");
			}

			fc.utobj().sendKeys(driver, taskui.taskSearch, subject5);
			fc.utobj().clickElement(driver, "//*[@id='systemSearchBtn']");

			boolean isSubjectPresentTaskSummaryPage5 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text(),'" + subject5 + "')]");
			if (isSubjectPresentTaskSummaryPage5 == false) {
				fc.utobj().throwsException("was not able to verify subject5 on Task Summary page");
			}

			fc.utobj().sendKeys(driver, taskui.taskSearch, subject6);
			fc.utobj().clickElement(driver, "//*[@id='systemSearchBtn']");

			boolean isSubjectPresentTaskSummaryPage6 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text(),'" + subject6 + "')]");
			if (isSubjectPresentTaskSummaryPage6 == false) {
				fc.utobj().throwsException("was not able to verify subject6 on Task Summary page");
			}

			// Verification of tasks at Lead Primary info page
			fc.utobj().printTestStep("Verification of tasks at Lead Primary info page");
			fc.crm().crm_common().CRMLeadsLnk(driver);
			fc.utobj().sendKeys(driver, crmleadpage.searchLeads, fullnameaddedthroughcrmlead);
			fc.utobj().clickElement(driver, "//*[@id='systemSearchBtn']");
			fc.utobj().clickElement(driver, "//a[contains(text(),'" + fullnameaddedthroughcrmlead + "')]");

			fc.utobj().printTestStep("Navigate to Lead Info Page");
			boolean isSubjectPresentLeadinfo1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text(),'" + subject1 + "')]");

			if (isSubjectPresentLeadinfo1 == false) {

				fc.utobj().throwsException("was not able to verify subject1 on Lead info page");
			}

			boolean isSubjectPresentLeadinfo2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text(),'" + subject2 + "')]");
			if (isSubjectPresentLeadinfo2 == false) {
				fc.utobj().throwsException("was not able to verify subject2 on Lead info page");
			}

			boolean isSubjectPresentLeadinfo3 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text(),'" + subject3 + "')]");
			if (isSubjectPresentLeadinfo3 == false) {
				fc.utobj().throwsException("was not able to verify subject3 on Lead info page");
			}

			boolean isSubjectPresentLeadinfo4 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text(),'" + subject4 + "')]");
			if (isSubjectPresentLeadinfo4 == false) {
				fc.utobj().throwsException("was not able to verify subject4 on Lead info page");
			}

			boolean isSubjectPresentLeadinfo5 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text(),'" + subject5 + "')]");
			if (isSubjectPresentLeadinfo5 == false) {
				fc.utobj().throwsException("was not able to verify subject5 on Lead info page");
			}

			boolean isSubjectPresentLeadinfo6 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text(),'" + subject6 + "')]");
			if (isSubjectPresentLeadinfo6 == false) {
				fc.utobj().throwsException("was not able to verify subject6 on Lead info page");
			}

			// Verification of task details is shown properly on View/modify Details page of
			// task
			fc.utobj().printTestStep(
					"Verification of task details is shown properly on View/modify Details page of task");
			fc.crm().crm_common().CRMTasksLnk(driver);
			fc.utobj().printTestStep("Navigated to CRM_Task");
			fc.utobj().sendKeys(driver, taskui.taskSearch, subject1);
			fc.utobj().clickElement(driver, "//*[@id='systemSearchBtn']");

			boolean isSubjectPresentTaskSummaryPagemodifydetails = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text(),'" + subject1 + "')]");
			if (isSubjectPresentTaskSummaryPagemodifydetails == false) {
				fc.utobj().throwsException(
						"was not able to verify subject1 on Task Summary page when doing modification");
			}

			fc.utobj().clickElement(driver, ".//a[contains(text(),'" + subject1 + "')]");

			fc.commonMethods().switch_cboxIframe_frameId(driver);

			fc.utobj().printTestStep("Verifying view of the task by clicking on close button");
			fc.utobj().clickElement(driver, taskui.close);

			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().clickElement(driver, pobj.clickonActionMenuLeadSummary);
			fc.utobj().sleep(2000);
			fc.utobj().clickElement(driver, pobj.modify);

			fc.commonMethods().switch_cboxIframe_frameId(driver);

			fc.utobj().printTestStep("Modifying the task by changing the status");
			fc.utobj().selectDropDown(driver, pobj.statusTask, "Waiting For Some One");
			fc.utobj().clickElement(driver, pobj.createBtn);
			fc.utobj().sleep(2000);
			fc.utobj().switchFrameToDefault(driver);
			Thread.sleep(5000);
			String taskstatus = taskui.taskStatus.getText();
			if (taskstatus.equalsIgnoreCase("Waiting For Some One")) {
				fc.utobj().printTestStep("Verified modified status of Task");
			} else {
				fc.utobj().throwsException("was not able to modify status of task");
			}
			System.out.println("DONE");
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "crm", "crmLeadFranchiseMu","crmtaskfinall" })
	@TestCase(createdOn = "2018-05-12", updatedOn = "2018-05-12", testCaseDescription = "Create task through Franchise Mu user and verify on different places", testCaseId = "Task_Creation_FranchiseMuUser")
	public void addTaskLeadFranchiseeMu() throws IOException, Exception, ParserConfigurationException, SAXException {

		fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		String testCaseId = "Task_Creation_CorporateUser";
		String area = "Task";
		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestDatawithsqllite("crm", testCaseId, area);

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		CRMLeadsPage crmleadpage = new CRMLeadsPage(driver);
		CRMTasksPage pobj = new CRMTasksPage(driver);
		try {
			driver = fc.loginpage().login(driver);
			CRMLeadsPageTest leadPage = new CRMLeadsPageTest();

			String testCaseId1 = "PTA_Campaign_From_Corporate_User";
			String area1 = "Campaign_Visibility";
			Map<String, String> dataSet1 = fc.utobj().readTestDatawithsqllite("crm", testCaseId1, area1);

			// Adding a region and creating a regional user who has role to view private
			// campaign(Region and R1)
			// R1
			fc.utobj().printTestStep(
					"Adding a region and creating a regional user who has role to view private campaign(Region and R1)");
			String regionName1 = fc.utobj().generateTestData(dataSet1.get("Regionname"));
			AdminAreaRegionAddAreaRegionPageTest p1 = new AdminAreaRegionAddAreaRegionPageTest();
			p1.addAreaRegionwithSQlite(driver, regionName1, dataSet1);

			// Mu user
			// Add a franchise location with same region
			// L6
			fc.utobj().printTestStep("Add a franchise location with same region");
			String firstNameF5 = fc.utobj().generateTestData(dataSet1.get("Franlocfirstname"));
			String lastNameF5 = fc.utobj().generateTestData(dataSet1.get("Franloclasttname"));
			fc.utobj().printTestStep("Naviaget To Admin > Franchise Location > Add Franchise Location");
			AdminFranchiseLocationAddFranchiseLocationPageTest franchiseLocation5 = new AdminFranchiseLocationAddFranchiseLocationPageTest();
			String franchiseId5 = fc.utobj().generateTestData(dataSet1.get("Franchiseid"));
			franchiseLocation5.addFranchiseLocationForMktwithoutaddingarearegion(driver, franchiseId5, regionName1,
					firstNameF5, lastNameF5, dataSet1);

			// Add Franchise Mu User
			// V1
			// Add Franchise owner mu User who has role to see private campaign
			fc.utobj().printTestStep("Add Franchise owner mu User who has role to see private campaign");
			AdminUsersManageManageFranchiseUsersPageTest addFranUsermu = new AdminUsersManageManageFranchiseUsersPageTest();
			// username and password for Owner type franchise user
			String userNameFOmu = fc.utobj().generateTestData(dataSet1.get("Franowneruname"));
			String roleNameFOmu = "Default Franchise Role";// dataSet.get("Franownerrolename");
			String passwordFOmu = dataSet1.get("Franownerpassword");
			String emailIdFOmu = dataSet1.get("Franowneremail") + "@staffex.com";
			userNameFOmu = addFranUsermu.addFranchiseUserwithoutchangeinsettings(driver, userNameFOmu, passwordFOmu,
					franchiseId5, roleNameFOmu, emailIdFOmu, dataSet1);
			System.out
					.println("(V1)Username and password for franchise mu user is" + userNameFOmu + " " + passwordFOmu);

			// Add a new Franchise location with same region but owner as existing owner
			// L7
			fc.utobj().printTestStep("Add a new Franchise location with same region but owner as existing owner");
			String franchiseId6 = fc.utobj().generateTestData(dataSet1.get("Franchiseid"));
			AdminFranchiseLocationAddFranchiseLocationPageTest franchiseLocation6 = new AdminFranchiseLocationAddFranchiseLocationPageTest();
			franchiseLocation6.addFranchiseLocationForMktwithoutaddingarearegionwithexistingowner(driver, franchiseId6,
					regionName1, firstNameF5, lastNameF5, dataSet1);

			// Add Franchise employee user
			// V2
			fc.utobj().printTestStep("Add Franchise employee User who has role to see private campaign");
			AdminUsersManageManageFranchiseUsersPageTest addFranEmpUsermu = new AdminUsersManageManageFranchiseUsersPageTest();
			String userNameFEmu = fc.utobj().generateTestData(dataSet1.get("Franempuname"));
			String roleNameFEmu = "Default Franchise Role";// dataSet.get("Franownerrolename");
			String passwordFEmu = dataSet1.get("Franemppassword");
			String emailIdFEmu = fc.utobj().generateTestData(dataSet1.get("Franempemail")) + "@staffex.com";
			userNameFEmu = addFranEmpUsermu.addFranchiseEmployeeUserwithoutchangeinsettings(driver, userNameFEmu,
					passwordFEmu, franchiseId6, roleNameFEmu, emailIdFEmu, dataSet1);
			System.out.println("(V2)Username and password for franchise employee existing mu user is" + userNameFEmu
					+ " " + passwordFEmu);

			fc.loginpage().loginWithParameter(driver, userNameFOmu, passwordFOmu);

			CRM_Verify_TaskUI taskui = new CRM_Verify_TaskUI(driver);
			fc.utobj().clickElement(driver, taskui.User);
			fc.utobj().clickElement(driver, taskui.Options);
			fc.utobj().moveToElement(driver, taskui.More);
			fc.utobj().clickElement(driver, taskui.More);
			fc.utobj().clickElement(driver, taskui.More);
			fc.utobj().clickElement(driver, taskui.Notifications);
			if (!fc.utobj().isSelected(driver, taskui.testcreationcheckbox)) {
				fc.utobj().clickElement(driver, taskui.testcreationcheckbox);
			}
			fc.utobj().clickElement(driver, taskui.save);

			fc.utobj().printTestStep("Navigate To CRM > Leads > Leads Summary");
			fc.utobj().printTestStep("Add Lead");

			String firstName = fc.utobj().generateTestData(dataSet.get("firstName"));
			String lastName = fc.utobj().generateTestData(dataSet.get("lastName"));
			String company = fc.utobj().generateTestData(dataSet.get("company"));
			String assignTo = "Franchise";
			String city = fc.utobj().generateTestData(dataSet.get("city"));
			String address = fc.utobj().generateTestData(dataSet.get("address"));
			String suffix = fc.utobj().generateTestData(dataSet.get("suffix"));
			String jobTitle = fc.utobj().generateTestData(dataSet.get("jobTitle"));
			String comment = fc.utobj().generateTestData(dataSet.get("comment"));
			String regionName = regionName1;
			String regionalUser = "";// Reguname+" "+Reguname;
			String franchiseId = franchiseId5;
			String franchiseUser = firstNameF5 + " " + lastNameF5;
			String fullName = "";// corpUser.getuserFullName();
			String fullnameaddedthroughcrmlead = firstName + " " + lastName;

			leadPage.addLeadwithSqlite(driver, dataSet, firstName, lastName, company, assignTo, city, address, suffix,
					jobTitle, comment, fullName, regionName, regionalUser, franchiseId, franchiseUser, config);

			CRM_Verify_Task_Method verifytaskmethod = new CRM_Verify_Task_Method();
			String subject1 = fc.utobj().generateTestData(dataSet.get("subject"));
			String subject2 = fc.utobj().generateTestData(dataSet.get("subject"));
			String subject3 = fc.utobj().generateTestData(dataSet.get("subject"));
			String subject4 = fc.utobj().generateTestData(dataSet.get("subject"));
			String subject5 = fc.utobj().generateTestData(dataSet.get("subject"));
			String subject6 = fc.utobj().generateTestData(dataSet.get("subject"));
			String emailId = dataSet.get("emailId");

			String parameter_name_Lead_Actions_Log_A_Task = "AddTaskThroughActionsLogATask";
			verifytaskmethod.addTaskLeadMethod(driver, firstName, lastName, fullName, emailId,
					parameter_name_Lead_Actions_Log_A_Task, subject1);
			String parameter_name_Click_Lead_Name_Log_Task = "AddTaskbyclickingonLeadName";
			verifytaskmethod.addTaskLeadMethod(driver, firstName, lastName, fullName, emailId,
					parameter_name_Click_Lead_Name_Log_Task, subject2);
			String parameter_name_Action_Image_Lead_Summary = "AddTaskThroughActionImageOnLeadSummaryPage";
			verifytaskmethod.addTaskLeadMethod(driver, firstName, lastName, fullName, emailId,
					parameter_name_Action_Image_Lead_Summary, subject3);
			String parameter_name_Lead_Bottom = "AddTaskThroughLeadBottom";
			verifytaskmethod.addTaskLeadMethod(driver, firstName, lastName, fullName, emailId,
					parameter_name_Lead_Bottom, subject4);
			String parameter_name = "AddTaskThroughTaskLink";
			verifytaskmethod.addTaskLeadMethod(driver, firstName, lastName, fullName, emailId, parameter_name,
					subject5);
			String parameter_name_without_Calendar = "AddTaskThroughTaskLinkwithoutCalendar";
			verifytaskmethod.addTaskLeadMethod(driver, firstName, lastName, fullName, emailId,
					parameter_name_without_Calendar, subject6);

			// Verification of tasks on calendar
			fc.utobj().printTestStep("Verification of tasks on calendar Day Link");
			fc.crm().crm_common().CRMCalendarLnk(driver);

			fc.utobj().clickElement(driver, taskui.Day);
			boolean isSubjectPresent1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text(),'" + subject1 + "')]");
			if (isSubjectPresent1 == false) {
				fc.utobj().throwsException("was not able to verify subject1 on calendar page");
			}

			boolean isSubjectPresent2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text(),'" + subject2 + "')]");
			if (isSubjectPresent2 == false) {
				fc.utobj().throwsException("was not able to verify subject2 on calendar page");
			}

			boolean isSubjectPresent3 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text(),'" + subject3 + "')]");
			if (isSubjectPresent3 == false) {
				fc.utobj().throwsException("was not able to verify subject3 on calendar page");
			}

			boolean isSubjectPresent4 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text(),'" + subject4 + "')]");
			if (isSubjectPresent4 == false) {
				fc.utobj().throwsException("was not able to verify subject4 on calendar page");
			}

			boolean isSubjectPresent5 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text(),'" + subject5 + "')]");
			if (isSubjectPresent5 == false) {
				fc.utobj().throwsException("was not able to verify subject5 on calendar page");
			}

			// Verification of tasks is not present on calendar
			fc.utobj().printTestStep("Verification of tasks is not present on calendar");
			boolean isSubjectPresent6 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text(),'" + subject6 + "')]");
			if (!isSubjectPresent6 == false) {
				fc.utobj().throwsException("was not able to verify subject6 on calendar page");
			}

			fc.utobj().printTestStep("Verification of tasks on calendar Week Link");
			fc.utobj().clickElement(driver, taskui.Week);

			// is
			boolean isSubjectPresentweek1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text(),'" + subject1 + "')]");
			if (isSubjectPresentweek1 == false) {
				fc.utobj().throwsException("was not able to verify subject1 on calendar page");
			}

			boolean isSubjectPresentweek2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text(),'" + subject2 + "')]");
			if (isSubjectPresentweek2 == false) {
				fc.utobj().throwsException("was not able to verify subject2 on calendar page");
			}

			boolean isSubjectPresentweek3 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text(),'" + subject3 + "')]");
			if (isSubjectPresentweek3 == false) {
				fc.utobj().throwsException("was not able to verify subject3 on calendar page");
			}

			boolean isSubjectPresentweek4 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text(),'" + subject4 + "')]");
			if (isSubjectPresentweek4 == false) {
				fc.utobj().throwsException("was not able to verify subject4 on calendar page");
			}

			boolean isSubjectPresentweek5 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text(),'" + subject5 + "')]");
			if (isSubjectPresentweek5 == false) {
				fc.utobj().throwsException("was not able to verify subject5 on calendar page");
			}

			// Verification of tasks is not present on calendar
			fc.utobj().printTestStep("Verification of tasks is not present on calendar");
			boolean isSubjectPresentweek6 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text(),'" + subject6 + "')]");
			if (!isSubjectPresentweek6 == false) {
				fc.utobj().throwsException("was not able to verify subject6 on calendar page");
			}

			fc.utobj().sleep(4000);
			fc.utobj().printTestStep("Verification of tasks on calendar Month Link");
			fc.utobj().refresh(driver);
			WebElement element = driver.findElement(By.xpath("//a//span[contains(text(),'Month')]"));
			boolean monthElement = element.isDisplayed();
			if (monthElement) {
				JavascriptExecutor executor = (JavascriptExecutor) driver;
				executor.executeScript("arguments[0].click();", element);
			}

			else {
				fc.utobj().refresh(driver);
				WebElement element1 = driver.findElement(By.xpath("//a//span[contains(text(),'Month')]"));
				JavascriptExecutor executor1 = (JavascriptExecutor) driver;
				executor1.executeScript("arguments[0].click();", element1);
			}

			boolean isSubjectPresentMonth1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text(),'" + subject1 + "')]");
			if (isSubjectPresentMonth1 == false) {
				fc.utobj().throwsException("was not able to verify subject1 on calendar page");
			}

			boolean isSubjectPresentMonth2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text(),'" + subject2 + "')]");
			if (isSubjectPresentMonth2 == false) {
				fc.utobj().throwsException("was not able to verify subject2 on calendar page");
			}

			boolean isSubjectPresentMonth3 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text(),'" + subject3 + "')]");
			if (isSubjectPresentMonth3 == false) {
				fc.utobj().throwsException("was not able to verify subject3 on calendar page");
			}

			boolean isSubjectPresentMonth4 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text(),'" + subject4 + "')]");
			if (isSubjectPresentMonth4 == false) {
				fc.utobj().throwsException("was not able to verify subject4 on calendar page");
			}

			boolean isSubjectPresentMonth5 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text(),'" + subject5 + "')]");
			if (isSubjectPresentMonth5 == false) {
				fc.utobj().throwsException("was not able to verify subject5 on calendar page");
			}

			// Verification of tasks is not present on calendar
			fc.utobj().printTestStep("Verification of tasks is not present on calendar");
			boolean isSubjectPresentMonth6 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text(),'" + subject6 + "')]");
			if (!isSubjectPresentMonth6 == false) {
				fc.utobj().throwsException("was not able to verify subject6 on calendar page");
			}

			// Verification of Task Notification on Notifications Bar
			fc.utobj().printTestStep("Verification of Task Notification on Notifications Bar");

			fc.crm().crm_common().CRMTasksLnk(driver);

			fc.utobj().clickElement(driver, taskui.arrow);
			fc.utobj().clickElement(driver, "//*[@id='showNotification']");
			fc.utobj().clickElement(driver, "//a[@href='viewAllNotifications?viewCriteria=16']");

			boolean Subject1 = fc.utobj().isElementPresent(driver, "//b[contains(text(),'" + subject1 + "')]");
			if (Subject1 == false) {
				fc.utobj().throwsException("was not able to verify sunject1");
			}

			boolean Subject2 = fc.utobj().isElementPresent(driver, "//b[contains(text(),'" + subject2 + "')]");
			if (Subject2 == false) {
				fc.utobj().throwsException("was not able to verify sunject2");
			}
			boolean Subject3 = fc.utobj().isElementPresent(driver, "//b[contains(text(),'" + subject3 + "')]");
			if (Subject3 == false) {
				fc.utobj().throwsException("was not able to verify sunject3");
			}
			boolean Subject4 = fc.utobj().isElementPresent(driver, "//b[contains(text(),'" + subject4 + "')]");
			if (Subject4 == false) {
				fc.utobj().throwsException("was not able to verify sunject4");
			}
			boolean Subject5 = fc.utobj().isElementPresent(driver, "//b[contains(text(),'" + subject5 + "')]");
			if (Subject5 == false) {
				fc.utobj().throwsException("was not able to verify sunject5");
			}
			boolean Subject6 = fc.utobj().isElementPresent(driver, "//b[contains(text(),'" + subject6 + "')]");
			if (Subject6 == false) {
				fc.utobj().throwsException("was not able to verify sunject5");
			}

			// Verification of Tasks on task reports
			fc.utobj().printTestStep("Verification of Tasks on task reports");
			CRM_Verify_Task_Common_Method commonmetohod = new CRM_Verify_Task_Common_Method();
			commonmetohod.openTaskHistoryOnTaskReportandsearchforaowner(driver, franchiseUser);
			fc.utobj().switchFrameById(driver, "reportiframe");
			boolean isSubjectPresentReportinfo1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//*[contains(text(),'" + subject1 + "')]");
			if (isSubjectPresentReportinfo1 == false) {

				fc.utobj().throwsException("was not able to verify subject1 on Report info page");
			}

			boolean isSubjectPresentReportinfo2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//*[contains(text(),'" + subject2 + "')]");
			if (isSubjectPresentReportinfo2 == false) {
				fc.utobj().throwsException("was not able to verify subject2 on Report info page");
			}

			boolean isSubjectPresentReportinfo3 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//*[contains(text(),'" + subject3 + "')]");
			if (isSubjectPresentReportinfo3 == false) {
				fc.utobj().throwsException("was not able to verify subject3 on Report info page");
			}

			boolean isSubjectPresentReportinfo4 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//*[contains(text(),'" + subject4 + "')]");
			if (isSubjectPresentReportinfo4 == false) {
				fc.utobj().throwsException("was not able to verify subject4 on Report info page");
			}

			boolean isSubjectPresentReportinfo5 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//*[contains(text(),'" + subject5 + "')]");
			if (isSubjectPresentReportinfo5 == false) {
				fc.utobj().throwsException("was not able to verify subject5 on Report info page");
			}

			boolean isSubjectPresentReportinfo6 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//*[contains(text(),'" + subject6 + "')]");
			if (isSubjectPresentReportinfo6 == false) {
				fc.utobj().throwsException("was not able to verify subject6 on Report info page");
			}
			System.out.println("Verification of Tasks on task reports done");
			fc.utobj().switchFrameToDefault(driver);

			// Verification of Added Task is search out through module top search at Task
			// Summary page
			fc.utobj().printTestStep(
					"Verification of Added Task is search out through module top search at Task Summary page");

			fc.crm().crm_common().CRMTasksLnk(driver);
			System.out.println(
					"Verification of Added Task is search out through module top search at Task Summary page is going to start");
			fc.utobj().sendKeys(driver, taskui.taskSearch, subject1);
			fc.utobj().clickElement(driver, "//*[@id='systemSearchBtn']");

			boolean isSubjectPresentTaskSummaryPage1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text(),'" + subject1 + "')]");
			if (isSubjectPresentTaskSummaryPage1 == false) {
				fc.utobj().throwsException("was not able to verify subject1 on Task Summary page");
			}

			fc.utobj().sendKeys(driver, taskui.taskSearch, subject2);
			fc.utobj().clickElement(driver, "//*[@id='systemSearchBtn']");

			boolean isSubjectPresentTaskSummaryPage2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text(),'" + subject2 + "')]");
			if (isSubjectPresentTaskSummaryPage2 == false) {
				fc.utobj().throwsException("was not able to verify subject2 on Task Summary page");
			}

			fc.utobj().sendKeys(driver, taskui.taskSearch, subject3);
			fc.utobj().clickElement(driver, "//*[@id='systemSearchBtn']");

			boolean isSubjectPresentTaskSummaryPage3 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text(),'" + subject3 + "')]");
			if (isSubjectPresentTaskSummaryPage3 == false) {
				fc.utobj().throwsException("was not able to verify subject3 on Task Summary page");
			}

			fc.utobj().sendKeys(driver, taskui.taskSearch, subject4);
			fc.utobj().clickElement(driver, "//*[@id='systemSearchBtn']");

			boolean isSubjectPresentTaskSummaryPage4 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text(),'" + subject4 + "')]");
			if (isSubjectPresentTaskSummaryPage4 == false) {
				fc.utobj().throwsException("was not able to verify subject4 on Task Summary page");
			}

			fc.utobj().sendKeys(driver, taskui.taskSearch, subject5);
			fc.utobj().clickElement(driver, "//*[@id='systemSearchBtn']");

			boolean isSubjectPresentTaskSummaryPage5 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text(),'" + subject5 + "')]");
			if (isSubjectPresentTaskSummaryPage5 == false) {
				fc.utobj().throwsException("was not able to verify subject5 on Task Summary page");
			}

			fc.utobj().sendKeys(driver, taskui.taskSearch, subject6);
			fc.utobj().clickElement(driver, "//*[@id='systemSearchBtn']");

			boolean isSubjectPresentTaskSummaryPage6 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text(),'" + subject6 + "')]");
			if (isSubjectPresentTaskSummaryPage6 == false) {
				fc.utobj().throwsException("was not able to verify subject6 on Task Summary page");
			}

			// Verification of tasks at Lead Primary info page
			fc.utobj().printTestStep("Verification of tasks at Lead Primary info page");
			fc.crm().crm_common().CRMLeadsLnk(driver);
			fc.utobj().sendKeys(driver, crmleadpage.searchLeads, fullnameaddedthroughcrmlead);
			fc.utobj().clickElement(driver, "//*[@id='systemSearchBtn']");
			fc.utobj().clickElement(driver, "//a[contains(text(),'" + fullnameaddedthroughcrmlead + "')]");

			fc.utobj().printTestStep("Navigate to Lead Info Page");
			boolean isSubjectPresentLeadinfo1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text(),'" + subject1 + "')]");

			if (isSubjectPresentLeadinfo1 == false) {

				fc.utobj().throwsException("was not able to verify subject1 on Lead info page");
			}

			boolean isSubjectPresentLeadinfo2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text(),'" + subject2 + "')]");
			if (isSubjectPresentLeadinfo2 == false) {
				fc.utobj().throwsException("was not able to verify subject2 on Lead info page");
			}

			boolean isSubjectPresentLeadinfo3 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text(),'" + subject3 + "')]");
			if (isSubjectPresentLeadinfo3 == false) {
				fc.utobj().throwsException("was not able to verify subject3 on Lead info page");
			}

			boolean isSubjectPresentLeadinfo4 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text(),'" + subject4 + "')]");
			if (isSubjectPresentLeadinfo4 == false) {
				fc.utobj().throwsException("was not able to verify subject4 on Lead info page");
			}

			boolean isSubjectPresentLeadinfo5 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text(),'" + subject5 + "')]");
			if (isSubjectPresentLeadinfo5 == false) {
				fc.utobj().throwsException("was not able to verify subject5 on Lead info page");
			}

			boolean isSubjectPresentLeadinfo6 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text(),'" + subject6 + "')]");
			if (isSubjectPresentLeadinfo6 == false) {
				fc.utobj().throwsException("was not able to verify subject6 on Lead info page");
			}

			// Verification of task details is shown properly on View/modify Details page of
			// task
			fc.utobj().printTestStep(
					"Verification of task details is shown properly on View/modify Details page of task");
			fc.crm().crm_common().CRMTasksLnk(driver);
			fc.utobj().printTestStep("Navigated to CRM_Task");
			fc.utobj().sendKeys(driver, taskui.taskSearch, subject1);
			fc.utobj().clickElement(driver, "//*[@id='systemSearchBtn']");

			boolean isSubjectPresentTaskSummaryPagemodifydetails = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text(),'" + subject1 + "')]");
			if (isSubjectPresentTaskSummaryPagemodifydetails == false) {
				fc.utobj().throwsException(
						"was not able to verify subject1 on Task Summary page when doing modification");
			}

			fc.utobj().clickElement(driver, ".//a[contains(text(),'" + subject1 + "')]");

			fc.commonMethods().switch_cboxIframe_frameId(driver);

			fc.utobj().printTestStep("Verifying view of the task by clicking on close button");
			fc.utobj().clickElement(driver, taskui.close);

			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().clickElement(driver, pobj.clickonActionMenuLeadSummary);
			fc.utobj().sleep(2000);
			fc.utobj().clickElement(driver, pobj.modify);

			fc.commonMethods().switch_cboxIframe_frameId(driver);

			fc.utobj().printTestStep("Modifying the task by changing the status");
			fc.utobj().selectDropDown(driver, pobj.statusTask, "Waiting For Some One");
			fc.utobj().clickElement(driver, pobj.createBtn);
			fc.utobj().sleep(2000);
			fc.utobj().switchFrameToDefault(driver);
			Thread.sleep(5000);
			String taskstatus = taskui.taskStatus.getText();
			if (taskstatus.equalsIgnoreCase("Waiting For Some One")) {
				fc.utobj().printTestStep("Verified modified status of Task");
			} else {
				fc.utobj().throwsException("was not able to modify status of task");
			}
			System.out.println("DONE");
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "crm", "crmLeadFranchiseEmployee","crmtaskfinall"})
	@TestCase(createdOn = "2018-06-12", updatedOn = "2018-06-12", testCaseDescription = "Create task through Franchise Employee user and verify on different places", testCaseId = "Task_Creation_FranchiseEmployeeUser")
	public void addTaskLeadFranchiseeEmployee()
			throws IOException, Exception, ParserConfigurationException, SAXException {

		fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		String testCaseId = "Task_Creation_CorporateUser";
		String area = "Task";
		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestDatawithsqllite("crm", testCaseId, area);

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		CRMLeadsPage crmleadpage = new CRMLeadsPage(driver);
		CRMTasksPage pobj = new CRMTasksPage(driver);
		try {
			driver = fc.loginpage().login(driver);
			CRMLeadsPageTest leadPage = new CRMLeadsPageTest();

			String testCaseId1 = "PTA_Campaign_From_Corporate_User";
			String area1 = "Campaign_Visibility";
			Map<String, String> dataSet1 = fc.utobj().readTestDatawithsqllite("crm", testCaseId1, area1);

			// Adding a region and creating a regional user who has role to view private
			// campaign(Region and R1)
			// R1
			fc.utobj().printTestStep(
					"Adding a region and creating a regional user who has role to view private campaign(Region and R1)");
			String regionName1 = fc.utobj().generateTestData(dataSet1.get("Regionname"));
			AdminAreaRegionAddAreaRegionPageTest p1 = new AdminAreaRegionAddAreaRegionPageTest();
			p1.addAreaRegionwithSQlite(driver, regionName1, dataSet1);

			// Mu user
			// Add a franchise location with same region
			// L6
			fc.utobj().printTestStep("Add a franchise location with same region");
			String firstNameF5 = fc.utobj().generateTestData(dataSet1.get("Franlocfirstname"));
			String lastNameF5 = fc.utobj().generateTestData(dataSet1.get("Franloclasttname"));
			fc.utobj().printTestStep("Naviaget To Admin > Franchise Location > Add Franchise Location");
			AdminFranchiseLocationAddFranchiseLocationPageTest franchiseLocation5 = new AdminFranchiseLocationAddFranchiseLocationPageTest();
			String franchiseId5 = fc.utobj().generateTestData(dataSet1.get("Franchiseid"));
			franchiseLocation5.addFranchiseLocationForMktwithoutaddingarearegion(driver, franchiseId5, regionName1,
					firstNameF5, lastNameF5, dataSet1);

			// Add Franchise Mu User
			// V1
			// Add Franchise owner mu User who has role to see private campaign
			fc.utobj().printTestStep("Add Franchise owner mu User who has role to see private campaign");
			AdminUsersManageManageFranchiseUsersPageTest addFranUsermu = new AdminUsersManageManageFranchiseUsersPageTest();
			// username and password for Owner type franchise user
			String userNameFOmu = fc.utobj().generateTestData(dataSet1.get("Franowneruname"));
			String roleNameFOmu = "Default Franchise Role";// dataSet.get("Franownerrolename");
			String passwordFOmu = dataSet1.get("Franownerpassword");
			String emailIdFOmu = dataSet1.get("Franowneremail") + "@staffex.com";
			userNameFOmu = addFranUsermu.addFranchiseUserwithoutchangeinsettings(driver, userNameFOmu, passwordFOmu,
					franchiseId5, roleNameFOmu, emailIdFOmu, dataSet1);
			System.out
					.println("(V1)Username and password for franchise mu user is" + userNameFOmu + " " + passwordFOmu);

			// Add a new Franchise location with same region but owner as existing owner
			// L7
			fc.utobj().printTestStep("Add a new Franchise location with same region but owner as existing owner");
			String franchiseId6 = fc.utobj().generateTestData(dataSet1.get("Franchiseid"));
			AdminFranchiseLocationAddFranchiseLocationPageTest franchiseLocation6 = new AdminFranchiseLocationAddFranchiseLocationPageTest();
			franchiseLocation6.addFranchiseLocationForMktwithoutaddingarearegionwithexistingowner(driver, franchiseId6,
					regionName1, firstNameF5, lastNameF5, dataSet1);

			// Add Franchise employee user
			// V2
			fc.utobj().printTestStep("Add Franchise employee User who has role to see private campaign");
			AdminUsersManageManageFranchiseUsersPageTest addFranEmpUsermu = new AdminUsersManageManageFranchiseUsersPageTest();
			String userNameFEmu = fc.utobj().generateTestData(dataSet1.get("Franempuname"));
			String roleNameFEmu = "Default Franchise Role";// dataSet.get("Franownerrolename");
			String passwordFEmu = dataSet1.get("Franemppassword");
			String emailIdFEmu = "crmautomation@staffex.com";
			userNameFEmu = addFranEmpUsermu.addFranchiseEmployeeUserwithoutchangeinsettingswithfirstnamelastname(driver,
					userNameFEmu, passwordFEmu, franchiseId6, roleNameFEmu, emailIdFEmu, dataSet1);
			System.out.println("(V2)Username and password for franchise employee existing mu user is" + userNameFEmu
					+ " " + passwordFEmu);

			fc.loginpage().loginWithParameter(driver, userNameFEmu, passwordFEmu);

			CRM_Verify_TaskUI taskui = new CRM_Verify_TaskUI(driver);
			fc.utobj().clickElement(driver, taskui.User);
			fc.utobj().clickElement(driver, taskui.Options);
			fc.utobj().moveToElement(driver, taskui.More);
			fc.utobj().clickElement(driver, taskui.More);
			fc.utobj().clickElement(driver, taskui.More);
			fc.utobj().clickElement(driver, taskui.Notifications);
			if (!fc.utobj().isSelected(driver, taskui.testcreationcheckbox)) {
				fc.utobj().clickElement(driver, taskui.testcreationcheckbox);
			}
			fc.utobj().clickElement(driver, taskui.save);

			fc.utobj().printTestStep("Navigate To CRM > Leads > Leads Summary");
			fc.utobj().printTestStep("Add Lead");

			String firstName = fc.utobj().generateTestData(dataSet.get("firstName"));
			String lastName = fc.utobj().generateTestData(dataSet.get("lastName"));
			String company = fc.utobj().generateTestData(dataSet.get("company"));
			String assignTo = "Franchise";
			String city = fc.utobj().generateTestData(dataSet.get("city"));
			String address = fc.utobj().generateTestData(dataSet.get("address"));
			String suffix = fc.utobj().generateTestData(dataSet.get("suffix"));
			String jobTitle = fc.utobj().generateTestData(dataSet.get("jobTitle"));
			String comment = fc.utobj().generateTestData(dataSet.get("comment"));
			String regionName = regionName1;
			String regionalUser = "";// Reguname+" "+Reguname;
			String franchiseId = franchiseId5;
			String franchiseUser = userNameFEmu + " " + userNameFEmu;
			String fullName = "";// corpUser.getuserFullName();
			String fullnameaddedthroughcrmlead = firstName + " " + lastName;

			leadPage.addLeadwithSqlite(driver, dataSet, firstName, lastName, company, assignTo, city, address, suffix,
					jobTitle, comment, fullName, regionName, regionalUser, franchiseId, franchiseUser, config);

			CRM_Verify_Task_Method verifytaskmethod = new CRM_Verify_Task_Method();
			String subject1 = fc.utobj().generateTestData(dataSet.get("subject"));
			String subject2 = fc.utobj().generateTestData(dataSet.get("subject"));
			String subject3 = fc.utobj().generateTestData(dataSet.get("subject"));
			String subject4 = fc.utobj().generateTestData(dataSet.get("subject"));
			String subject5 = fc.utobj().generateTestData(dataSet.get("subject"));
			String subject6 = fc.utobj().generateTestData(dataSet.get("subject"));
			String emailId = dataSet.get("emailId");

			String parameter_name_Lead_Actions_Log_A_Task = "AddTaskThroughActionsLogATask";
			verifytaskmethod.addTaskLeadMethod(driver, firstName, lastName, fullName, emailId,
					parameter_name_Lead_Actions_Log_A_Task, subject1);
			String parameter_name_Click_Lead_Name_Log_Task = "AddTaskbyclickingonLeadName";
			verifytaskmethod.addTaskLeadMethod(driver, firstName, lastName, fullName, emailId,
					parameter_name_Click_Lead_Name_Log_Task, subject2);
			String parameter_name_Action_Image_Lead_Summary = "AddTaskThroughActionImageOnLeadSummaryPage";
			verifytaskmethod.addTaskLeadMethod(driver, firstName, lastName, fullName, emailId,
					parameter_name_Action_Image_Lead_Summary, subject3);
			String parameter_name_Lead_Bottom = "AddTaskThroughLeadBottom";
			verifytaskmethod.addTaskLeadMethod(driver, firstName, lastName, fullName, emailId,
					parameter_name_Lead_Bottom, subject4);
			String parameter_name = "AddTaskThroughTaskLink";
			verifytaskmethod.addTaskLeadMethod(driver, firstName, lastName, fullName, emailId, parameter_name,
					subject5);
			String parameter_name_without_Calendar = "AddTaskThroughTaskLinkwithoutCalendar";
			verifytaskmethod.addTaskLeadMethod(driver, firstName, lastName, fullName, emailId,
					parameter_name_without_Calendar, subject6);

			// Verification of tasks on calendar
			fc.utobj().printTestStep("Verification of tasks on calendar Day Link");
			fc.crm().crm_common().CRMCalendarLnk(driver);

			fc.utobj().selectDropDown(driver, taskui.calendarmultiselectbox, franchiseUser);
			// fc.utobj().selectDropDownByValue(driver,taskui.calendarmultiselectbox,
			// franchiseUser);

			fc.utobj().clickElement(driver, taskui.Day);
			boolean isSubjectPresent1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text(),'" + subject1 + "')]");
			if (isSubjectPresent1 == false) {
				fc.utobj().throwsException("was not able to verify subject1 on calendar page");
			}

			boolean isSubjectPresent2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text(),'" + subject2 + "')]");
			if (isSubjectPresent2 == false) {
				fc.utobj().throwsException("was not able to verify subject2 on calendar page");
			}

			boolean isSubjectPresent3 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text(),'" + subject3 + "')]");
			if (isSubjectPresent3 == false) {
				fc.utobj().throwsException("was not able to verify subject3 on calendar page");
			}

			boolean isSubjectPresent4 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text(),'" + subject4 + "')]");
			if (isSubjectPresent4 == false) {
				fc.utobj().throwsException("was not able to verify subject4 on calendar page");
			}

			boolean isSubjectPresent5 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text(),'" + subject5 + "')]");
			if (isSubjectPresent5 == false) {
				fc.utobj().throwsException("was not able to verify subject5 on calendar page");
			}

			// Verification of tasks is not present on calendar
			fc.utobj().printTestStep("Verification of tasks is not present on calendar");
			boolean isSubjectPresent6 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text(),'" + subject6 + "')]");
			if (!isSubjectPresent6 == false) {
				fc.utobj().throwsException("was not able to verify subject6 on calendar page");
			}

			fc.utobj().printTestStep("Verification of tasks on calendar Week Link");
			fc.utobj().clickElement(driver, taskui.Week);

			// is
			boolean isSubjectPresentweek1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text(),'" + subject1 + "')]");
			if (isSubjectPresentweek1 == false) {
				fc.utobj().throwsException("was not able to verify subject1 on calendar page");
			}

			boolean isSubjectPresentweek2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text(),'" + subject2 + "')]");
			if (isSubjectPresentweek2 == false) {
				fc.utobj().throwsException("was not able to verify subject2 on calendar page");
			}

			boolean isSubjectPresentweek3 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text(),'" + subject3 + "')]");
			if (isSubjectPresentweek3 == false) {
				fc.utobj().throwsException("was not able to verify subject3 on calendar page");
			}

			boolean isSubjectPresentweek4 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text(),'" + subject4 + "')]");
			if (isSubjectPresentweek4 == false) {
				fc.utobj().throwsException("was not able to verify subject4 on calendar page");
			}

			boolean isSubjectPresentweek5 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text(),'" + subject5 + "')]");
			if (isSubjectPresentweek5 == false) {
				fc.utobj().throwsException("was not able to verify subject5 on calendar page");
			}

			// Verification of tasks is not present on calendar
			fc.utobj().printTestStep("Verification of tasks is not present on calendar");
			boolean isSubjectPresentweek6 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text(),'" + subject6 + "')]");
			if (!isSubjectPresentweek6 == false) {
				fc.utobj().throwsException("was not able to verify subject6 on calendar page");
			}

			fc.utobj().sleep(4000);
			fc.utobj().printTestStep("Verification of tasks on calendar Month Link");
			fc.utobj().refresh(driver);
			WebElement element = driver.findElement(By.xpath("//a//span[contains(text(),'Month')]"));
			boolean monthElement = element.isDisplayed();
			if (monthElement) {
				JavascriptExecutor executor = (JavascriptExecutor) driver;
				executor.executeScript("arguments[0].click();", element);
			}

			else {
				fc.utobj().refresh(driver);
				WebElement element1 = driver.findElement(By.xpath("//a//span[contains(text(),'Month')]"));
				JavascriptExecutor executor1 = (JavascriptExecutor) driver;
				executor1.executeScript("arguments[0].click();", element1);
			}

			boolean isSubjectPresentMonth1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text(),'" + subject1 + "')]");
			if (isSubjectPresentMonth1 == false) {
				fc.utobj().throwsException("was not able to verify subject1 on calendar page");
			}

			boolean isSubjectPresentMonth2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text(),'" + subject2 + "')]");
			if (isSubjectPresentMonth2 == false) {
				fc.utobj().throwsException("was not able to verify subject2 on calendar page");
			}

			boolean isSubjectPresentMonth3 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text(),'" + subject3 + "')]");
			if (isSubjectPresentMonth3 == false) {
				fc.utobj().throwsException("was not able to verify subject3 on calendar page");
			}

			boolean isSubjectPresentMonth4 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text(),'" + subject4 + "')]");
			if (isSubjectPresentMonth4 == false) {
				fc.utobj().throwsException("was not able to verify subject4 on calendar page");
			}

			boolean isSubjectPresentMonth5 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text(),'" + subject5 + "')]");
			if (isSubjectPresentMonth5 == false) {
				fc.utobj().throwsException("was not able to verify subject5 on calendar page");
			}

			// Verification of tasks is not present on calendar
			fc.utobj().printTestStep("Verification of tasks is not present on calendar");
			boolean isSubjectPresentMonth6 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text(),'" + subject6 + "')]");
			if (!isSubjectPresentMonth6 == false) {
				fc.utobj().throwsException("was not able to verify subject6 on calendar page");
			}

			// Verification of Task Notification on Notifications Bar
			fc.utobj().printTestStep("Verification of Task Notification on Notifications Bar");

			fc.crm().crm_common().CRMTasksLnk(driver);

			fc.utobj().clickElement(driver, taskui.arrow);
			fc.utobj().clickElement(driver, "//*[@id='showNotification']");
			fc.utobj().clickElement(driver, "//a[@href='viewAllNotifications?viewCriteria=16']");

			boolean Subject1 = fc.utobj().isElementPresent(driver, "//b[contains(text(),'" + subject1 + "')]");
			if (Subject1 == false) {
				fc.utobj().throwsException("was not able to verify sunject1");
			}

			boolean Subject2 = fc.utobj().isElementPresent(driver, "//b[contains(text(),'" + subject2 + "')]");
			if (Subject2 == false) {
				fc.utobj().throwsException("was not able to verify sunject2");
			}
			boolean Subject3 = fc.utobj().isElementPresent(driver, "//b[contains(text(),'" + subject3 + "')]");
			if (Subject3 == false) {
				fc.utobj().throwsException("was not able to verify sunject3");
			}
			boolean Subject4 = fc.utobj().isElementPresent(driver, "//b[contains(text(),'" + subject4 + "')]");
			if (Subject4 == false) {
				fc.utobj().throwsException("was not able to verify sunject4");
			}
			boolean Subject5 = fc.utobj().isElementPresent(driver, "//b[contains(text(),'" + subject5 + "')]");
			if (Subject5 == false) {
				fc.utobj().throwsException("was not able to verify sunject5");
			}
			boolean Subject6 = fc.utobj().isElementPresent(driver, "//b[contains(text(),'" + subject6 + "')]");
			if (Subject6 == false) {
				fc.utobj().throwsException("was not able to verify sunject5");
			}

			// Verification of Tasks on task reports
			fc.utobj().printTestStep("Verification of Tasks on task reports");
			CRM_Verify_Task_Common_Method commonmetohod = new CRM_Verify_Task_Common_Method();
			commonmetohod.openTaskHistoryOnTaskReportandsearchforaowner(driver, franchiseUser);
			fc.utobj().switchFrameById(driver, "reportiframe");
			boolean isSubjectPresentReportinfo1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//*[contains(text(),'" + subject1 + "')]");
			if (isSubjectPresentReportinfo1 == false) {

				fc.utobj().throwsException("was not able to verify subject1 on Report info page");
			}

			boolean isSubjectPresentReportinfo2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//*[contains(text(),'" + subject2 + "')]");
			if (isSubjectPresentReportinfo2 == false) {
				fc.utobj().throwsException("was not able to verify subject2 on Report info page");
			}

			boolean isSubjectPresentReportinfo3 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//*[contains(text(),'" + subject3 + "')]");
			if (isSubjectPresentReportinfo3 == false) {
				fc.utobj().throwsException("was not able to verify subject3 on Report info page");
			}

			boolean isSubjectPresentReportinfo4 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//*[contains(text(),'" + subject4 + "')]");
			if (isSubjectPresentReportinfo4 == false) {
				fc.utobj().throwsException("was not able to verify subject4 on Report info page");
			}

			boolean isSubjectPresentReportinfo5 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//*[contains(text(),'" + subject5 + "')]");
			if (isSubjectPresentReportinfo5 == false) {
				fc.utobj().throwsException("was not able to verify subject5 on Report info page");
			}

			boolean isSubjectPresentReportinfo6 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//*[contains(text(),'" + subject6 + "')]");
			if (isSubjectPresentReportinfo6 == false) {
				fc.utobj().throwsException("was not able to verify subject6 on Report info page");
			}
			System.out.println("Verification of Tasks on task reports done");
			fc.utobj().switchFrameToDefault(driver);

			// Verification of Added Task is search out through module top search at Task
			// Summary page
			fc.utobj().printTestStep(
					"Verification of Added Task is search out through module top search at Task Summary page");

			fc.crm().crm_common().CRMTasksLnk(driver);
			System.out.println(
					"Verification of Added Task is search out through module top search at Task Summary page is going to start");
			fc.utobj().sendKeys(driver, taskui.taskSearch, subject1);
			fc.utobj().clickElement(driver, "//*[@id='systemSearchBtn']");

			boolean isSubjectPresentTaskSummaryPage1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text(),'" + subject1 + "')]");
			if (isSubjectPresentTaskSummaryPage1 == false) {
				fc.utobj().throwsException("was not able to verify subject1 on Task Summary page");
			}

			fc.utobj().sendKeys(driver, taskui.taskSearch, subject2);
			fc.utobj().clickElement(driver, "//*[@id='systemSearchBtn']");

			boolean isSubjectPresentTaskSummaryPage2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text(),'" + subject2 + "')]");
			if (isSubjectPresentTaskSummaryPage2 == false) {
				fc.utobj().throwsException("was not able to verify subject2 on Task Summary page");
			}

			fc.utobj().sendKeys(driver, taskui.taskSearch, subject3);
			fc.utobj().clickElement(driver, "//*[@id='systemSearchBtn']");

			boolean isSubjectPresentTaskSummaryPage3 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text(),'" + subject3 + "')]");
			if (isSubjectPresentTaskSummaryPage3 == false) {
				fc.utobj().throwsException("was not able to verify subject3 on Task Summary page");
			}

			fc.utobj().sendKeys(driver, taskui.taskSearch, subject4);
			fc.utobj().clickElement(driver, "//*[@id='systemSearchBtn']");

			boolean isSubjectPresentTaskSummaryPage4 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text(),'" + subject4 + "')]");
			if (isSubjectPresentTaskSummaryPage4 == false) {
				fc.utobj().throwsException("was not able to verify subject4 on Task Summary page");
			}

			fc.utobj().sendKeys(driver, taskui.taskSearch, subject5);
			fc.utobj().clickElement(driver, "//*[@id='systemSearchBtn']");

			boolean isSubjectPresentTaskSummaryPage5 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text(),'" + subject5 + "')]");
			if (isSubjectPresentTaskSummaryPage5 == false) {
				fc.utobj().throwsException("was not able to verify subject5 on Task Summary page");
			}

			fc.utobj().sendKeys(driver, taskui.taskSearch, subject6);
			fc.utobj().clickElement(driver, "//*[@id='systemSearchBtn']");

			boolean isSubjectPresentTaskSummaryPage6 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text(),'" + subject6 + "')]");
			if (isSubjectPresentTaskSummaryPage6 == false) {
				fc.utobj().throwsException("was not able to verify subject6 on Task Summary page");
			}

			// Verification of tasks at Lead Primary info page
			fc.utobj().printTestStep("Verification of tasks at Lead Primary info page");
			fc.crm().crm_common().CRMLeadsLnk(driver);
			fc.utobj().sendKeys(driver, crmleadpage.searchLeads, fullnameaddedthroughcrmlead);
			fc.utobj().clickElement(driver, "//*[@id='systemSearchBtn']");
			fc.utobj().clickElement(driver, "//a[contains(text(),'" + fullnameaddedthroughcrmlead + "')]");

			fc.utobj().printTestStep("Navigate to Lead Info Page");
			boolean isSubjectPresentLeadinfo1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text(),'" + subject1 + "')]");

			if (isSubjectPresentLeadinfo1 == false) {

				fc.utobj().throwsException("was not able to verify subject1 on Lead info page");
			}

			boolean isSubjectPresentLeadinfo2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text(),'" + subject2 + "')]");
			if (isSubjectPresentLeadinfo2 == false) {
				fc.utobj().throwsException("was not able to verify subject2 on Lead info page");
			}

			boolean isSubjectPresentLeadinfo3 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text(),'" + subject3 + "')]");
			if (isSubjectPresentLeadinfo3 == false) {
				fc.utobj().throwsException("was not able to verify subject3 on Lead info page");
			}

			boolean isSubjectPresentLeadinfo4 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text(),'" + subject4 + "')]");
			if (isSubjectPresentLeadinfo4 == false) {
				fc.utobj().throwsException("was not able to verify subject4 on Lead info page");
			}

			boolean isSubjectPresentLeadinfo5 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text(),'" + subject5 + "')]");
			if (isSubjectPresentLeadinfo5 == false) {
				fc.utobj().throwsException("was not able to verify subject5 on Lead info page");
			}

			boolean isSubjectPresentLeadinfo6 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text(),'" + subject6 + "')]");
			if (isSubjectPresentLeadinfo6 == false) {
				fc.utobj().throwsException("was not able to verify subject6 on Lead info page");
			}

			// Verification of task details is shown properly on View/modify Details page of
			// task
			fc.utobj().printTestStep(
					"Verification of task details is shown properly on View/modify Details page of task");
			fc.crm().crm_common().CRMTasksLnk(driver);
			fc.utobj().printTestStep("Navigated to CRM_Task");
			fc.utobj().sendKeys(driver, taskui.taskSearch, subject1);
			fc.utobj().clickElement(driver, "//*[@id='systemSearchBtn']");

			boolean isSubjectPresentTaskSummaryPagemodifydetails = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text(),'" + subject1 + "')]");
			if (isSubjectPresentTaskSummaryPagemodifydetails == false) {
				fc.utobj().throwsException(
						"was not able to verify subject1 on Task Summary page when doing modification");
			}

			fc.utobj().clickElement(driver, ".//a[contains(text(),'" + subject1 + "')]");

			fc.commonMethods().switch_cboxIframe_frameId(driver);

			fc.utobj().printTestStep("Verifying view of the task by clicking on close button");
			fc.utobj().clickElement(driver, taskui.close);

			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().clickElement(driver, pobj.clickonActionMenuLeadSummary);
			fc.utobj().sleep(2000);
			fc.utobj().clickElement(driver, pobj.modify);

			fc.commonMethods().switch_cboxIframe_frameId(driver);

			fc.utobj().printTestStep("Modifying the task by changing the status");
			fc.utobj().selectDropDown(driver, pobj.statusTask, "Waiting For Some One");
			fc.utobj().clickElement(driver, pobj.createBtn);
			fc.utobj().sleep(2000);
			fc.utobj().switchFrameToDefault(driver);
			Thread.sleep(5000);
			String taskstatus = taskui.taskStatus.getText();
			if (taskstatus.equalsIgnoreCase("Waiting For Some One")) {
				fc.utobj().printTestStep("Verified modified status of Task");
			} else {
				fc.utobj().throwsException("was not able to modify status of task");
			}
			System.out.println("DONE");
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

}