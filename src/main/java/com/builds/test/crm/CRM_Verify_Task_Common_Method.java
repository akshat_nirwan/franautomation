package com.builds.test.crm;

import org.openqa.selenium.WebDriver;

import com.builds.uimaps.crm.CRM_Verify_TaskUI;
import com.builds.utilities.FranconnectUtil;

public class CRM_Verify_Task_Common_Method {
	FranconnectUtil fc = new FranconnectUtil();
	public void openTaskHistoryOnTaskReportandsearchforaowner(WebDriver driver,String corporateusername) throws Exception {
		try {
			CRM_Verify_TaskUI taskui=new CRM_Verify_TaskUI(driver);
			 fc.crm().crm_common().CRMReportsLnk(driver);
			 fc.utobj().clickElement(driver, taskui.activityReports);
			 fc.utobj().clickElement(driver, taskui.taskHistory);
			 fc.utobj().printTestStep("Navigate to Task History Page");
			 /*fc.utobj().selectValFromMultiSelect(driver, taskui.assignedToReports, corporateusername);*/
			 fc.utobj().clickElement(driver, taskui.assignedToReports);
			 fc.utobj().sleep(2000);
			 fc.utobj().clickElement(driver,taskui.typeandEnter);
			 fc.utobj().sleep(2000);
			 fc.utobj().sendKeys(driver, taskui.typeandEnter,corporateusername);
			 fc.utobj().sleep(2000);
			 fc.utobj().clickElement(driver,taskui.typeandEnterSearch);
			 fc.utobj().clickElement(driver, taskui.selectAll);
			 fc.utobj().clickElement(driver,taskui.viewReport);
		}
		catch(Exception e) {
			fc.utobj().throwsException("was not able to open task history on task report or not able to search for a owner");
		}
		
	}
	
}
