package com.builds.test.crm;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.builds.test.common.FCHomePageTest;
import com.builds.uimaps.crm.CRMTasksPage;
import com.builds.utilities.FranconnectUtil;

public class CRM_Verify_Task_Method {
	
	FranconnectUtil fc = new FranconnectUtil();
	FCHomePageTest fcHomePageTest=new FCHomePageTest();
	public void addTaskLeadMethod(WebDriver driver,String firstName,String lastName,String fullName, String emailId,String parameter,String subject) throws Exception{
		String area="Task";
		String testCaseId="Task_Creation_CorporateUser";
		Map<String, String> dataSet = fc.utobj().readTestDatawithsqllite("crm", testCaseId,area);
		CRMTasksPage pobj = new CRMTasksPage(driver);
		pobj = new CRMTasksPage(driver);
		
		if(parameter.equalsIgnoreCase("AddTaskThroughTaskLink")) {
			fc.utobj().printTestStep("Navigate To CRM > Tasks");
			fc.utobj().printTestStep("Add Task");
			fc.utobj().clickElement(driver, pobj.taskLinks);
			fc.utobj().clickElement(driver, pobj.addTaskLink);
			fc.commonMethods().switch_cboxIframe_frameId(driver);

			if (!fc.utobj().isSelected(driver, pobj.associatedWithLead)) {
				fc.utobj().clickElement(driver, pobj.associatedWithLead);
			}
			String leadName = firstName + " " + lastName;

			fc.utobj().sendKeys(driver, pobj.leadSearch, leadName);
			Thread.sleep(4000);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//span/u[.='" + leadName + "']"));
		}
		
		else if(parameter.equalsIgnoreCase("AddTaskThroughTaskLinkwithoutCalendar")) {
			fc.utobj().printTestStep("Navigate To CRM > Tasks");
			fc.utobj().printTestStep("Add Task");
			fc.utobj().clickElement(driver, pobj.taskLinks);
			fc.utobj().clickElement(driver, pobj.addTaskLink);
			fc.commonMethods().switch_cboxIframe_frameId(driver);

			if (!fc.utobj().isSelected(driver, pobj.associatedWithLead)) {
				fc.utobj().clickElement(driver, pobj.associatedWithLead);
			}
			String leadName = firstName + " " + lastName;

			fc.utobj().sendKeys(driver, pobj.leadSearch, leadName);
			Thread.sleep(4000);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//span/u[.='" + leadName + "']"));
		}
		
		else if(parameter.equalsIgnoreCase("AddTaskThroughLeadBottom")) {
			fc.utobj().printTestStep("Navigate To CRM > Leads");
			fc.crm().crm_common().openCRMLeadsPage(fcHomePageTest, driver);
			fc.utobj().printTestStep("Search for lead");
			String leadName = firstName + " " + lastName;
			fc.utobj().sendKeys(driver, pobj.searchLeadOnSystem, leadName);
			fc.utobj().clickElement(driver, pobj.searchbtnonLeadSystem);
			fc.utobj().clickElement(driver, pobj.clickonleadcheckbox);
			fc.commonMethods().Click_LogATask_Input_ByValue(driver);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
		}
		
		else if(parameter.equalsIgnoreCase("AddTaskThroughActionImageOnLeadSummaryPage")) {
			fc.utobj().printTestStep("Navigate To CRM > Leads");
			fc.crm().crm_common().openCRMLeadsPage(fcHomePageTest, driver);
			fc.utobj().printTestStep("Search for lead");
			String leadName = firstName + " " + lastName;
			fc.utobj().sendKeys(driver, pobj.searchLeadOnSystem, leadName);
			fc.utobj().clickElement(driver, pobj.searchbtnonLeadSystem);
			fc.utobj().clickElement(driver, pobj.clickonleadcheckbox);
			fc.utobj().clickElement(driver, pobj.clickonActionMenuLeadSummary);
			fc.utobj().sleep(2000);
			fc.utobj().clickElement(driver,pobj.clickonLogATask);
			fc.commonMethods().switch_cboxIframe_frameId(driver);	
		}
		else if(parameter.equalsIgnoreCase("AddTaskbyclickingonLeadName")) {
			fc.utobj().printTestStep("Navigate To CRM > Leads");
			fc.crm().crm_common().openCRMLeadsPage(fcHomePageTest, driver);
			fc.utobj().printTestStep("Search for lead");
			String leadName = firstName + " " + lastName;
			fc.utobj().sendKeys(driver, pobj.searchLeadOnSystem, leadName);
			fc.utobj().clickElement(driver, pobj.searchbtnonLeadSystem);
			fc.utobj().clickElement(driver, "//*[contains(text(),'"+leadName+"')]");
			fc.utobj().clickElement(driver, "//*[contains(text(),'Log a Task')]");
			fc.commonMethods().switch_cboxIframe_frameId(driver);
		}
		else if(parameter.equalsIgnoreCase("AddTaskThroughActionsLogATask")) {
			fc.utobj().printTestStep("Navigate To CRM > Leads");
			fc.crm().crm_common().openCRMLeadsPage(fcHomePageTest, driver);
			fc.utobj().printTestStep("Search for lead");
			String leadName = firstName + " " + lastName;
			fc.utobj().sendKeys(driver, pobj.searchLeadOnSystem, leadName);
			fc.utobj().clickElement(driver, pobj.searchbtnonLeadSystem);
			fc.utobj().clickElement(driver, pobj.clickonleadcheckbox);
			/*fc.utobj().selectActionMenuItemsWithTagA(driver, "Log a Task");*/
			fc.utobj().clickElement(driver,"//td/a[contains(text(),'Actions')]");
			List<WebElement> menu = driver.findElements(By.xpath(".//*[@id='actionListButtons']/table/tbody/tr[2]/td[2]/table/tbody//td"));
			fc.utobj().clickElement(driver,menu.get(0));
			fc.commonMethods().switch_cboxIframe_frameId(driver);
		}
		
		try {
				String leadName = firstName + " " + lastName;
				if (!fc.utobj().isSelected(driver,pobj.radioOwner)) {
					fc.utobj().clickElement(driver, pobj.radioOwner);
				}
				String statusTask = dataSet.get("statusTask");
				fc.utobj().selectDropDown(driver, pobj.statusTask, statusTask);
				fc.utobj().sendKeys(driver, pobj.subject, subject);
				
				if(parameter.equalsIgnoreCase("AddTaskThroughTaskLinkwithoutCalendar")){
					if (!fc.utobj().isSelected(driver, pobj.timeLessTask)) {
					fc.utobj().clickElement(driver, pobj.timeLessTask);
					}
				}
				
				String priority = dataSet.get("priority");
				fc.utobj().selectDropDown(driver, pobj.priority, priority);
				fc.utobj().sendKeys(driver, pobj.comments,dataSet.get("comment"));
				
				if(!parameter.equalsIgnoreCase("AddTaskThroughTaskLinkwithoutCalendar")){
					fc.utobj().selectDropDown(driver, pobj.Starttimehour,dataSet.get("Starttimehour"));
					fc.utobj().selectDropDown(driver, pobj.Starttimeminute,dataSet.get("Starttimeminute"));
					fc.utobj().selectDropDown(driver, pobj.Startimeapm,dataSet.get("Startimeapm"));
					fc.utobj().selectDropDown(driver, pobj.Endtimehour,dataSet.get("Endtimehour"));
					fc.utobj().selectDropDown(driver, pobj.Endtimeminute,dataSet.get("Endtimeminute"));
					fc.utobj().selectDropDown(driver, pobj.Endtimeapm,dataSet.get("Endtimeapm"));
				}
				
				
				
				fc.utobj().clickElement(driver, pobj.createBtn);
				fc.utobj().sleep(2000);
				fc.utobj().switchFrameToDefault(driver);
				Thread.sleep(5000);
				
				fc.utobj().printTestStep("Navigate To CRM > Tasks");
				fc.utobj().clickElement(driver, pobj.taskLinks);
				fc.utobj().printTestStep("Verify Task Detail");
				searchBySubjectFilter(driver, subject);

				boolean isSubjectPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[.='" + subject + "']");
				if (isSubjectPresent == false) {
					fc.utobj().throwsException("Was not able to verify Subject Of Task");
				}

				boolean isAssociatedWithPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
						".//a[.='" + subject + "']/ancestor::tr/td/a[contains(text () ,'" + leadName + "')]");
				if (isAssociatedWithPresent == false) {
					fc.utobj().throwsException("was not able to verify Associated With");
				}

				boolean isTaskStatusPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
						".//a[.='" + subject + "']/ancestor::tr/td[contains(text () ,'" + statusTask + "')]");
				if (isTaskStatusPresent == false) {
					fc.utobj().throwsException("was not able to verify Status Of Task");
				}

				boolean isAssignedTo = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[.='" + subject
						+ "']/ancestor::tr/td[contains(text () ,'" + fullName + "')]");//corpUser.getuserFullName()
				if (isAssignedTo == false) {
					fc.utobj().throwsException("was not able to verify Assign To");
				}

				fc.utobj().printTestStep("Verify The Task Detail At Lead Info Page");
				fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,(".//a[.='" + subject + "']/ancestor::tr/td/a[contains(text () ,'" + leadName + "')]")));

				boolean isAssignedToAtLeadInfo = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[.='" + subject
						+ "']/ancestor::tr/td/div[contains(text () ,'" + fullName + "')]");//corpUser.getuserFullName()
				if (isAssignedToAtLeadInfo == false) {
					fc.utobj().throwsException("was not able to verify Assign To At Lead Info Page");
				}

				boolean isTaskStatusPresentAtLeadInfo = fc.utobj().verifyElementOnVisible_ByXpath(driver,
						".//a[.='" + subject + "']/ancestor::tr/td[contains(text () ,'" + statusTask + "')]");
				if (isTaskStatusPresentAtLeadInfo == false) {
					fc.utobj().throwsException("was not able to verify Status Of Task At Lead Info Page");
				}

				fc.utobj().printTestStep("Verify that if any task is associated with Any Lead so entry will not be delete");

				fc.utobj().selectActionMenuItemsWithTagAMoreActions(driver, "Delete Lead");

				String alertText = fc.utobj().acceptAlertBox(driver);

				if (!alertText.equalsIgnoreCase(
						fc.utobj().translateString("Lead can not be deleted as a Task is associated with it."))) {
					fc.utobj().throwsException(
							"Not able to verify that if any task is associated with Any Lead so entry will not be delete");
				}

				//Verification of Task Notification mailsent to Assigned User with Proper details
				fc.utobj().printTestStep("Verification of Task Notification mailsent to Assigned User with Proper details");

				Map<String, String> mailData = new HashMap<String, String>();

				mailData = fc.utobj().readMailBox("New Task Added", subject, emailId, "sdg@1a@Hfs");

				if (mailData.size() == 0 || !mailData.get("mailBody").contains(firstName)) {

					fc.utobj().throwsException("was not able Verify Task Creation Mail");
				}

				if (mailData.size() == 0 || !mailData.get("mailBody").contains("Task Addition Notification")) {

					fc.utobj().throwsException("was not able Verify Task Creation Mail Info");
				}
		}
			
		catch(Exception e) {
			fc.utobj().throwsException("Was not able to add task");
		}
		
		
	}
	public void searchBySubjectFilter(WebDriver driver, String subject) throws Exception {
		CRMTasksPage pobj = new CRMTasksPage(driver);
		fc.utobj().sleep(3000);
		fc.utobj().clickElement(driver, pobj.showFilter);
		fc.utobj().sendKeys(driver, pobj.subjectFilter, subject);
		fc.utobj().setToDefault(driver, pobj.taskStatusFilter);
		fc.utobj().sendKeys(driver, pobj.startdateFilter, "");
		fc.utobj().sendKeys(driver, pobj.endDate, "");
		fc.utobj().selectDropDown(driver, pobj.reminderFilter, "Select");
		fc.utobj().setToDefault(driver, pobj.prioprityFilter);
		fc.utobj().setToDefault(driver, pobj.viewMineFilter);
		fc.utobj().setToDefault(driver, pobj.associatedWithFilter);
		fc.utobj().clickElement(driver, pobj.searchBtnFilter);
		fc.utobj().clickElement(driver, pobj.hideFilters);
	}
}
