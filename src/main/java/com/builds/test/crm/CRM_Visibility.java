package com.builds.test.crm;

import java.util.HashMap;
import java.util.Map;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Test;

import com.builds.test.admin.AdminAreaRegionAddAreaRegionPageTest;
import com.builds.test.admin.AdminFranchiseLocationAddFranchiseLocationPageTest;
import com.builds.test.admin.AdminPageTest;
import com.builds.test.admin.AdminUsersManageCorporateUsersAddCorporateUserPageTest;
import com.builds.test.admin.AdminUsersManageManageFranchiseUsersPageTest;
import com.builds.test.admin.AdminUsersManageRegionalUsersAddRegionalUserPageTest;
import com.builds.test.admin.AdminUsersRolesAddNewRolePageTest;
import com.builds.test.common.CorporateUser;
import com.builds.test.common.FCHomePageTest;
import com.builds.uimaps.admin.AdminPage;
import com.builds.uimaps.admin.AdminUsersManageManageFranchiseUsersPage;
import com.builds.uimaps.campaigncentercrm.CRMCampaignCenterCampaignsPage;
import com.builds.uimaps.campaigncentercrm.CRMCampaignCenterPage;
import com.builds.utilities.FranconnectUtil;
import com.builds.utilities.TestCase;

public class CRM_Visibility {

	
	FranconnectUtil fc = new FranconnectUtil();
	
	@Test(groups = {"Crm_Campaign_Visibility","crmcampaignfinal"})
	@TestCase(createdOn = "2018-10-18", updatedOn = "2018-10-18", testCaseDescription = "To verify the visibility of all types of campaign/template (Code your Own and layout and Theme)created by all types of users through corporate,regional and franchisee users", testCaseId = "PTA_Campaign_From_Corporate_User")
	public void visibilityofcampaign() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());
		String area="Campaign_Visibility";
		Map<String, String> dataSet = fc.utobj().readTestDatawithsqllite("crm", testCaseId,area);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		AdminUsersManageManageFranchiseUsersPage pobj = new AdminUsersManageManageFranchiseUsersPage(driver);
		try {
			driver = fc.loginpage().login(driver);
			FCHomePageTest fcHomePageTest= new FCHomePageTest();
			CRMCampaignCenterPage crmcenter=new CRMCampaignCenterPage(driver);
			//Add New Corporate Role which Can not View Private Folders / Templates of Other Users(CR2)
			fc.utobj().printTestStep("Add New Corporate Role which Can not View Private Folders / Templates of Other Users(CR2)");
			String roleType = dataSet.get("CorproleType");
			String roleNamewithoutpri = fc.utobj().generateTestData(dataSet.get("CorproleNamewithoutpri"));//CR2
			String moduleName = dataSet.get("CorpmoduleName");
			Map<String, String> withoutprivileges = new HashMap<String, String>();
			withoutprivileges.put(dataSet.get("Privilege"),dataSet.get("PrivilegeNo"));
			AdminUsersRolesAddNewRolePageTest setpriviledge=new AdminUsersRolesAddNewRolePageTest();
			setpriviledge.addRolesWithModulesPrivileges(driver, roleType, roleNamewithoutpri, moduleName, withoutprivileges, testCaseId);
			
			//Add New Corporate Role which Can View Private Folders / Templates of Other Users(CR1)
			fc.utobj().printTestStep("Add New Corporate Role which Can View Private Folders / Templates of Other Users(CR1)");
			String roleNamewithpri = fc.utobj().generateTestData(dataSet.get("CorproleNamewithpri"));//CR1
			Map<String, String> withprivileges = new HashMap<String, String>();
			withprivileges.put(dataSet.get("Privilege"), dataSet.get("PrivilegeYes"));
			setpriviledge.addRolesWithModulesPrivileges(driver, roleType, roleNamewithpri, moduleName, withprivileges, testCaseId);
			
			//Add New Regional Role which Can not View Private Folders / Templates of Other Users Leads Privilege(RR2)
			fc.utobj().printTestStep("Add New Regional Role which Can not View Private Folders / Templates of Other Users Leads Privilege(RR2)");
			String roleType1 = dataSet.get("RegroleType1");
			String roleNamewithoutpri1 = fc.utobj().generateTestData(dataSet.get("RegroleNamewithoutpri1"));//RR2
			Map<String, String> withoutprivileges1 = new HashMap<String, String>();
			withoutprivileges1.put(dataSet.get("Privilege"),dataSet.get("PrivilegeNo"));
			setpriviledge.addRolesWithModulesPrivileges(driver, roleType1, roleNamewithoutpri1, moduleName, withoutprivileges1, testCaseId);
			
			//Add New Regional Role which Can View Private Folders / Templates of Other Users Leads Privilege(RR1)
			fc.utobj().printTestStep("Add New Regional Role which Can View Private Folders / Templates of Other Users Leads Privilege(RR1)");
			String roleNamewithpri1 = fc.utobj().generateTestData(dataSet.get("RegroleNamewithpri1"));//RR1
			Map<String, String> withprivileges1 = new HashMap<String, String>();
			withprivileges1.put(dataSet.get("Privilege"), dataSet.get("PrivilegeYes"));
			setpriviledge.addRolesWithModulesPrivileges(driver, roleType1, roleNamewithpri1, moduleName, withprivileges1, testCaseId);
			
			//Add New Franchise Role which Can View Private Folders / Templates of Other Users Leads Privilege(FR1)
			fc.utobj().printTestStep("Add New Franchise Role which Can View Private Folders / Templates of Other Users Leads Privilege(FR1)");
			String roleNamewithpri2 = fc.utobj().generateTestData(dataSet.get("FranroleNamewithpri2"));//FR1
			String roleType2 = dataSet.get("FranroleType2");
			Map<String, String> withprivileges2 = new HashMap<String, String>();
			withprivileges2.put(dataSet.get("Privilege"), dataSet.get("PrivilegeYes"));
			setpriviledge.addRolesWithModulesPrivileges(driver, roleType2, roleNamewithpri2, moduleName, withprivileges2, testCaseId);
			
			//Adding a corporate user who has role to view private campaign
			//C1
			fc.utobj().printTestStep("Adding a corporate user who has role to view private campaign(C1)");
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporate_user = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser = new CorporateUser();
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUserwithSQLite(corpUser,dataSet);
			corpUser.setRole(roleNamewithpri);
			corpUser = corporate_user.createDefaultUser(driver, corpUser);
			//Getting the username and password for corporate user
			fc.utobj().printTestStep("Getting the username and password for corporate user");
			String CorpUserName=corpUser.getUserName();
			String CorpUserPassword=corpUser.getPassword();
			System.out.println("Corporate(C1) userid and password"+CorpUserName+CorpUserPassword);
			
			//Creating a corporate user who does not have role to view private campaign(C2)
			//C2
			fc.utobj().printTestStep("Creating a corporate user who does not have role to view private campaign(C2)");
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporate_user1 = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser1 = new CorporateUser();
			corpUser1 = fc.commonMethods().fillDefaultDataFor_CorporateUserwithSQLite(corpUser1,dataSet);
			corpUser1.setRole(roleNamewithoutpri);
			corpUser1 = corporate_user1.createDefaultUser(driver, corpUser1);
			//Getting the username and password for corporate user
			fc.utobj().printTestStep("Getting the username and password for another corporate user");
			String CorpUserName1=corpUser1.getUserName();
			String CorpUserPassword1=corpUser1.getPassword();
			System.out.println("Corporate(C2) userid and password"+CorpUserName1+CorpUserPassword1);
			
			//Adding a region and creating a regional user who has role to view private campaign(Region and R1)
			//R1
			fc.utobj().printTestStep("Adding a region and creating a regional user who has role to view private campaign(Region and R1)");
			AdminUsersManageRegionalUsersAddRegionalUserPageTest regionalUserPage = new AdminUsersManageRegionalUsersAddRegionalUserPageTest();
			String regionName = fc.utobj().generateTestData(dataSet.get("Regionname"));
			//username and password for regional user
			String Reguname = fc.utobj().generateTestData(dataSet.get("Reguname"));//AreaRegion
			String Regpassword=dataSet.get("Regpassword");
			regionalUserPage.addRegionalUserwithunamepass(driver, Reguname, regionName,Regpassword,dataSet,roleNamewithpri1);
			System.out.println("Regional(R1) user id and password"+Reguname+" "+Regpassword);
			
			//Adding the same region area and creating a regional user who does not have role to view private campaign(R2)
			//R2
			fc.utobj().printTestStep("Adding the same region area and creating a regional user who does not have role to view private campaign(R2)");
			//username and password for regional user
			String Reguname1 = fc.utobj().generateTestData(dataSet.get("Reguname"));
			String Regpassword1=dataSet.get("Regpassword");
			regionalUserPage.addRegionalUserwithunamepasswithoutaddingarea(driver, Reguname1, regionName,Regpassword1,dataSet,roleNamewithoutpri1);
			fc.utobj().printTestStep("R2 added");
			System.out.println("Regional(R2) user id and password"+Reguname1+" "+Regpassword1);
			
			//Mu user		
			//Add a franchise location with same region
			//L6
			fc.utobj().printTestStep("Add a franchise location with same region");
			String firstNameF5 = fc.utobj().generateTestData(dataSet.get("Franlocfirstname"));
			String lastNameF5 = fc.utobj().generateTestData(dataSet.get("Franloclasttname"));
			fc.utobj().printTestStep("Naviaget To Admin > Franchise Location > Add Franchise Location");
			AdminFranchiseLocationAddFranchiseLocationPageTest franchiseLocation5 = new AdminFranchiseLocationAddFranchiseLocationPageTest();
			String franchiseId5 = fc.utobj().generateTestData(dataSet.get("Franchiseid"));
			franchiseLocation5.addFranchiseLocationForMktwithoutaddingarearegion(driver, franchiseId5, regionName, firstNameF5, lastNameF5,dataSet);
			
			//Add Franchise Mu User
			//V1
			//Add Franchise owner mu User who has role to see private campaign
			fc.utobj().printTestStep("Add Franchise owner mu User who has role to see private campaign");
			AdminUsersManageManageFranchiseUsersPageTest addFranUsermu = new AdminUsersManageManageFranchiseUsersPageTest();
			//username and password for Owner type franchise user
			String userNameFOmu = fc.utobj().generateTestData(dataSet.get("Franowneruname"));
			String roleNameFOmu = roleNamewithpri2;//dataSet.get("Franownerrolename");
			String passwordFOmu = dataSet.get("Franownerpassword");
			String emailIdFOmu =fc.utobj().generateTestData(dataSet.get("Franowneremail"))+"@staffex.com";
			userNameFOmu = addFranUsermu.addFranchiseUserwithoutchangeinsettings(driver, userNameFOmu, passwordFOmu, franchiseId5, roleNameFOmu, emailIdFOmu,dataSet);
			System.out.println("(V1)Username and password for franchise mu user is"+userNameFOmu+" "+passwordFOmu);
			
			//Add a new Franchise location with same region but owner as existing owner
			//L7
			fc.utobj().printTestStep("Add a new Franchise location with same region but owner as existing owner");
			String franchiseId6 = fc.utobj().generateTestData(dataSet.get("Franchiseid"));
			AdminFranchiseLocationAddFranchiseLocationPageTest franchiseLocation6 = new AdminFranchiseLocationAddFranchiseLocationPageTest();
			franchiseLocation6.addFranchiseLocationForMktwithoutaddingarearegionwithexistingowner(driver, franchiseId6, regionName, firstNameF5, lastNameF5, dataSet);
			
			//Add Franchise employee user
			//V2
			fc.utobj().printTestStep("Add Franchise employee User who has role to see private campaign");
			AdminUsersManageManageFranchiseUsersPageTest addFranEmpUsermu = new AdminUsersManageManageFranchiseUsersPageTest();
			String userNameFEmu = fc.utobj().generateTestData(dataSet.get("Franempuname"));
			String roleNameFEmu =roleNamewithpri2;//dataSet.get("Franownerrolename");
			String passwordFEmu = dataSet.get("Franemppassword");
			String emailIdFEmu =fc.utobj().generateTestData(dataSet.get("Franempemail"))+"@staffex.com";
			userNameFEmu = addFranEmpUsermu.addFranchiseEmployeeUserwithoutchangeinsettings(driver, userNameFEmu, passwordFEmu, franchiseId6, roleNameFEmu, emailIdFEmu,dataSet);
			System.out.println("(V2)Username and password for franchise employee existing mu user is"+userNameFEmu+" "+passwordFEmu);
			
			//Add a new Franchisee owner user with location same as existing location of V2
			AdminPageTest admninpage=new AdminPageTest();
			admninpage.manageFranchiseLocationLnk(driver);
			AdminPage adminpage=new AdminPage(driver);
			fc.utobj().selectValFromMultiSelect(driver, adminpage.franchiseid,franchiseId6);//franchiseId6//Locationjain2
			fc.utobj().clickElement(driver,adminpage.searchbuttonoffranchisee);
			fc.utobj().clickElement(driver, adminpage.actionmenuoffranchisee);
			fc.utobj().clickElement(driver,adminpage.usersoffranchisee);
			fc.utobj().clickElement(driver,adminpage.addowner);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			String first=fc.utobj().generateTestData(dataSet.get("FranNewowner"));
			String last=fc.utobj().generateTestData(dataSet.get("FranNewownersurname"));
			String fullname=first+" "+last;
			fc.utobj().sendKeys(driver,adminpage.firstName,first);
			fc.utobj().sendKeys(driver,adminpage.lastName,last);
			fc.utobj().sleep(3000);
			fc.utobj().clickElement(driver, adminpage.save);
			fc.utobj().sleep(3000);
			fc.commonMethods().Click_Close_Input_ByValue(driver);
			fc.utobj().sleep(3000);
			fc.utobj().switchFrameToDefault(driver);
			fc.utobj().clickElement(driver, ".//*[contains(text(),'"+fullname+"')]/ancestor::tr[1]/td[4]/div/layer/a/img");
			fc.utobj().clickElement(driver,"//span[contains(text(),'Generate Login')]");
			/*fc.utobj().actionImgOption(driver, fullname,dataSet.get("Frangeneratlogin"));*/
			
		
			//Add New Franchise owner User who has role to see private campaign
			fc.utobj().printTestStep("Add New Franchise owner User who has role to see private campaign");
			//username and password for Owner type franchise user
			String userNameFONew = fc.utobj().generateTestData(dataSet.get("Franowneruname"));
			String roleNameFONew = roleNamewithpri2;//dataSet.get("Franownerrolename");
			String passwordFONew = dataSet.get("Franownerpassword");
			fc.utobj().sendKeys(driver, pobj.userName, userNameFONew);
			fc.utobj().sendKeys(driver, pobj.password, passwordFONew);
			fc.utobj().sendKeys(driver, pobj.confirmPassword, passwordFONew);
			fc.utobj().selectValFromMultiSelect(driver, pobj.selectRole, roleNameFONew);
			fc.utobj().clickElement(driver, pobj.submit);
			System.out.println("Franchisee owner(FONew) userName and Password are"+userNameFONew+" "+passwordFONew);
			
			
			
			
			CRMCampaignCenterPageTest campaignCenterPage = new CRMCampaignCenterPageTest();
			CRMCampaignCenterCampaignsPage pobjcam = new CRMCampaignCenterCampaignsPage(driver);
			CRMCampaignCenterPageTest emailtemplate=new CRMCampaignCenterPageTest();
			
			//Login with Franchise Owner user(FONew) who has role to see private campaign
			fc.utobj().printTestStep("Login with Franchise Owner user(FONew) who has role to see private campaign");
			fc.loginpage().loginWithParameter(driver, userNameFONew, passwordFONew);
			fc.crm().crm_common().openCRMCampaignCenterPage(fcHomePageTest, driver);
			
			//Create Campaign At Campaign Center > Campaigns
			fc.utobj().printTestStep("Create Campaign At Campaign Center > Campaigns");
			String firstcampaignNamefran = fc.utobj().generateTestData(dataSet.get("CorpcampaignName"));
			String firstaccessibilityfran = dataSet.get("FranaccessibilityPublictoallUsersofmyFranchise");
			String firstcampaignTypefran = dataSet.get("CorpcampaignType");
			String firsttemplateNamefran = fc.utobj().generateTestData(dataSet.get("CorptemplateName"));
			String firstemailSubjectfran = fc.utobj().generateTestData(dataSet.get("CorpemailSubject"));
			String firstplainTextEditorfran = fc.utobj().generateTestData(dataSet.get("CorpplainTextEditor"));
			campaignCenterPage.createCampaignGenericwithSqlite(driver, dataSet, firstcampaignNamefran, firstaccessibilityfran,
					firstcampaignTypefran, firsttemplateNamefran, firstemailSubjectfran, firstplainTextEditorfran);
			
			//Create another campaign at Campaign Center > Campaigns
			fc.utobj().printTestStep("Create another campaign at Campaign Center > Campaigns");
			String secondcampaignNamefran = fc.utobj().generateTestData(dataSet.get("CorpcampaignName"));
			String privateaccessibilityfran = dataSet.get("Franaccessibilityprivate");
			String secondcampaignTypefran = dataSet.get("CorpcampaignType");
			String secondtemplateNamefran = fc.utobj().generateTestData(dataSet.get("CorptemplateName"));
			String secondemailSubjectfran = fc.utobj().generateTestData(dataSet.get("CorpemailSubject"));
			String secondplainTextEditorfran = fc.utobj().generateTestData(dataSet.get("CorpplainTextEditor"));
			campaignCenterPage.createCampaignGenericwithSqlite(driver, dataSet, secondcampaignNamefran, privateaccessibilityfran,
					secondcampaignTypefran, secondtemplateNamefran, secondemailSubjectfran, secondplainTextEditorfran);
			
			//Create Status Driven campaign At Campaign Center > Campaigns
			fc.utobj().printTestStep("Create Status Driven campaign At Campaign Center > Campaigns");
			String firstcampaignNamefranstatus = fc.utobj().generateTestData(dataSet.get("CorpcampaignName"));
			String firstaccessibilityfranstatus = dataSet.get("FranaccessibilityPublictoallUsersofmyFranchise");
			String firstcampaignTypefranstatus = dataSet.get("StatusCampaigntype");
			String firsttemplateNamefranstatus = fc.utobj().generateTestData(dataSet.get("CorptemplateName"));
			String firstemailSubjectfranstatus = fc.utobj().generateTestData(dataSet.get("CorpemailSubject"));
			String firstplainTextEditorfranstatus = fc.utobj().generateTestData(dataSet.get("CorpplainTextEditor"));
			campaignCenterPage.createCampaignGenericwithSqlite(driver, dataSet, firstcampaignNamefranstatus, firstaccessibilityfranstatus,
					firstcampaignTypefranstatus, firsttemplateNamefranstatus, firstemailSubjectfranstatus, firstplainTextEditorfranstatus);
			
			//Create Status Driven campaign 1 at Campaign Center > Campaigns
			fc.utobj().printTestStep("Create Status Driven campaign 1 at Campaign Center > Campaigns");
			String secondcampaignNamefranstatus = fc.utobj().generateTestData(dataSet.get("CorpcampaignName"));
			String privateaccessibilityfranstatus = dataSet.get("Franaccessibilityprivate");
			String secondcampaignTypefranstatus = dataSet.get("StatusCampaigntype");
			String secondtemplateNamefranstatus = fc.utobj().generateTestData(dataSet.get("CorptemplateName"));
			String secondemailSubjectfranstatus = fc.utobj().generateTestData(dataSet.get("CorpemailSubject"));
			String secondplainTextEditorfranstatus = fc.utobj().generateTestData(dataSet.get("CorpplainTextEditor"));
			campaignCenterPage.createCampaignGenericwithSqlite(driver, dataSet, secondcampaignNamefranstatus, privateaccessibilityfranstatus,
					secondcampaignTypefranstatus, secondtemplateNamefranstatus, secondemailSubjectfranstatus, secondplainTextEditorfranstatus);
			
			fc.utobj().clickElement(driver, pobjcam.viewAllButton);
			
			//Verify Public to all users of my Franchise Campaign created by Franchise user who has privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Public to all users of my Franchise Campaign created by Franchise user who has privilege to see private campaign by itself");
			campaignCenterPage.searchCampaignByFilter(driver, firstcampaignNamefran);

			boolean isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchise = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firstcampaignNamefran + "')]");
			if (isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchise == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my Franchise Campaign created by Franchise user who has privilege to see private campaign by itself");
			}
			
			//Verify Private Campaign created by Franchise user who has privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Private Campaign created by Franchise user who has privilege to see private campaign by itself");
			campaignCenterPage.searchCampaignByFilter(driver, secondcampaignNamefran);
			
			boolean isCampaignNamePresentFranpaccessibilityprivate = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondcampaignNamefran + "')]");
			if (isCampaignNamePresentFranpaccessibilityprivate == false) {
				fc.utobj().throwsException("was not able to verify Private Campaign created by Franchise user who has privilege to see private campaign by itself");
			}
			
			
			//Verify Public to all users of my Franchise (Status Driven)Campaign created by Franchise user who has privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Public to all users of my Franchise (Status Driven)Campaign created by Franchise user who has privilege to see private campaign by itself");
			campaignCenterPage.searchCampaignByFilter(driver, firstcampaignNamefranstatus);

			boolean isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchisestatus = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firstcampaignNamefranstatus + "')]");
			if (isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchisestatus == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my Franchise (Status Driven)Campaign created by Franchise user who has privilege to see private campaign by itself");
			}
			
			//Verify Private Campaign created by Franchise user who has privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Private (Status Driven)Campaign created by Franchise user who has privilege to see private campaign by itself");
			campaignCenterPage.searchCampaignByFilter(driver, secondcampaignNamefranstatus);
			
			boolean isCampaignNamePresentFranpaccessibilityprivatestatus = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondcampaignNamefranstatus + "')]");
			if (isCampaignNamePresentFranpaccessibilityprivatestatus == false) {
				fc.utobj().throwsException("was not able to verify Private (Status Driven)Campaign created by Franchise user who has privilege to see private campaign by itself");
			}
			
			fc.utobj().printTestStep("Navigate to CRM>Campaign Center>email template");
			emailtemplate.clickonemailtamplate(driver);
			fc.utobj().clickElement(driver, crmcenter.locationtemplate);
			//Verify Public to all users template created by Corporate user who has privilege to see private template by itself
			fc.utobj().printTestStep("Verify Public to all users of my franchise template created by franchise user who has privilege to see private template by itself");
			campaignCenterPage.searchTemplateByFilter(driver, firsttemplateNamefran);

			boolean isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchise = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firsttemplateNamefran + "')]");
			if (isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchise == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my franchise template created by franchise user who has privilege to see private template by itself");
			}
			
			//Verify Private template created by franchise user who has privilege to see private template by itself
			fc.utobj().printTestStep("Verify Private template created by franchise user who has privilege to see private template by itself");
			campaignCenterPage.searchTemplateByFilter(driver, secondtemplateNamefran);
			
			boolean isTemplateNamePresentfranaccessibilityprivate = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondtemplateNamefran + "')]");
			if (isTemplateNamePresentfranaccessibilityprivate == false) {
				fc.utobj().throwsException("was not able to verify private template created by franchise user who has privilege to see private template by itself");
			}
			
			//Verify Public to all users (Status Driven)template created by Corporate user who has privilege to see private template by itself
			fc.utobj().printTestStep("Verify Public to all users of my franchise (Status Driven)template created by franchise user who has privilege to see private template by itself");
			campaignCenterPage.searchTemplateByFilter(driver, firsttemplateNamefranstatus);

			boolean isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchisestatus = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firsttemplateNamefranstatus + "')]");
			if (isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchisestatus == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my franchise (Status Driven)template created by franchise user who has privilege to see private template by itself");
			}
			
			//Verify Private template created by franchise user who has privilege to see private template by itself
			fc.utobj().printTestStep("Verify Private (Status Driven)template created by franchise user who has privilege to see private template by itself");
			campaignCenterPage.searchTemplateByFilter(driver, secondtemplateNamefranstatus);
			
			boolean isTemplateNamePresentfranaccessibilityprivatestatus = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondtemplateNamefranstatus + "')]");
			if (isTemplateNamePresentfranaccessibilityprivatestatus == false) {
				fc.utobj().throwsException("was not able to verify private (Status Driven)template created by franchise user who has privilege to see private template by itself");
			}
			
			
			
			//Login with Corporate user who has role to see private campaign(C1)
			fc.utobj().printTestStep("Login with Corporate user");
			fc.loginpage().loginWithParameter(driver, CorpUserName, CorpUserPassword);
			fc.crm().crm_common().openCRMCampaignCenterPage(fcHomePageTest, driver);
			
			//Create Campaign At Campaign Center > Campaigns
			/*CRMCampaignCenterPageTest campaignCenterPage = new CRMCampaignCenterPageTest();
			CRMCampaignCenterCampaignsPage pobjcam = new CRMCampaignCenterCampaignsPage(driver);*/
			fc.utobj().printTestStep("Create Campaign At Campaign Center > Campaigns");
			String firstcampaignName = fc.utobj().generateTestData(dataSet.get("CorpcampaignName"));
			String firstaccessibility = dataSet.get("Corpaccessibilitypublictoallusers");
			String firstcampaignType = dataSet.get("CorpcampaignType");
			String firsttemplateName = fc.utobj().generateTestData(dataSet.get("CorptemplateName"));
			String firstemailSubject = fc.utobj().generateTestData(dataSet.get("CorpemailSubject"));
			String firstplainTextEditor = fc.utobj().generateTestData(dataSet.get("CorpplainTextEditor"));
			campaignCenterPage.createCampaignGenericwithSqlite(driver, dataSet, firstcampaignName, firstaccessibility,
					firstcampaignType, firsttemplateName, firstemailSubject, firstplainTextEditor);
			
			//Create another campaign at Campaign Center > Campaigns
			fc.utobj().printTestStep("Create another campaign at Campaign Center > Campaigns");
			String secondcampaignName = fc.utobj().generateTestData(dataSet.get("CorpcampaignName"));
			String publictocorporateusersaccessibility = dataSet.get("Corpaccessibilitypublictoallcorporateusers");
			String secondcampaignType = dataSet.get("CorpcampaignType");
			String secondtemplateName = fc.utobj().generateTestData(dataSet.get("CorptemplateName"));
			String secondemailSubject = fc.utobj().generateTestData(dataSet.get("CorpemailSubject"));
			String secondplainTextEditor = fc.utobj().generateTestData(dataSet.get("CorpplainTextEditor"));
			campaignCenterPage.createCampaignGenericwithSqlite(driver, dataSet, secondcampaignName, publictocorporateusersaccessibility,
					secondcampaignType, secondtemplateName, secondemailSubject, secondplainTextEditor);
			
			//Create another campaign1 at Campaign Center > Campaigns
			fc.utobj().printTestStep("Create another campaign at Campaign Center > Campaigns");
			String thirdcampaignName = fc.utobj().generateTestData(dataSet.get("CorpcampaignName"));
			String privatecorpaccessibility = dataSet.get("Corpaccessibilityprivate");
			String thirdcampaignType = dataSet.get("CorpcampaignType");
			String thirdtemplateName = fc.utobj().generateTestData(dataSet.get("CorptemplateName"));
			String thirdemailSubject = fc.utobj().generateTestData(dataSet.get("CorpemailSubject"));
			String thirdplainTextEditor = fc.utobj().generateTestData(dataSet.get("CorpplainTextEditor"));
			campaignCenterPage.createCampaignGenericwithSqlite(driver, dataSet, thirdcampaignName, privatecorpaccessibility,
					thirdcampaignType, thirdtemplateName, thirdemailSubject, thirdplainTextEditor);
			
			//Create Status Driven campaign At Campaign Center > Campaigns
			fc.utobj().printTestStep("Create Status Driven campaign At Campaign Center > Campaigns");
			String firstcampaignNamestatus = fc.utobj().generateTestData(dataSet.get("CorpcampaignName"));
			String firstaccessibilitystatus = dataSet.get("Corpaccessibilitypublictoallusers");
			String firstcampaignTypestatus = dataSet.get("StatusCampaigntype");
			String firsttemplateNamestatus = fc.utobj().generateTestData(dataSet.get("CorptemplateName"));
			String firstemailSubjectstatus = fc.utobj().generateTestData(dataSet.get("CorpemailSubject"));
			String firstplainTextEditorstatus = fc.utobj().generateTestData(dataSet.get("CorpplainTextEditor"));
			campaignCenterPage.createCampaignGenericwithSqlite(driver, dataSet, firstcampaignNamestatus, firstaccessibilitystatus,
					firstcampaignTypestatus, firsttemplateNamestatus, firstemailSubjectstatus, firstplainTextEditorstatus);
			
			//Create Status Driven campaign 1 at Campaign Center > Campaigns
			fc.utobj().printTestStep("Create Status Driven campaign 1 at Campaign Center > Campaigns");
			String secondcampaignNamestatus = fc.utobj().generateTestData(dataSet.get("CorpcampaignName"));
			String publictocorporateusersaccessibilitystatus = dataSet.get("Corpaccessibilitypublictoallcorporateusers");
			String secondcampaignTypestatus = dataSet.get("StatusCampaigntype");
			String secondtemplateNamestatus = fc.utobj().generateTestData(dataSet.get("CorptemplateName"));
			String secondemailSubjectstatus = fc.utobj().generateTestData(dataSet.get("CorpemailSubject"));
			String secondplainTextEditorstatus = fc.utobj().generateTestData(dataSet.get("CorpplainTextEditor"));
			campaignCenterPage.createCampaignGenericwithSqlite(driver, dataSet, secondcampaignNamestatus, publictocorporateusersaccessibilitystatus,
					secondcampaignTypestatus, secondtemplateNamestatus, secondemailSubjectstatus, secondplainTextEditorstatus);
			
			//Create another Status Driven campaign 2 at Campaign Center > Campaigns
			fc.utobj().printTestStep("Create another Status Driven campaign 2 at Campaign Center > Campaigns");
			String thirdcampaignNamestatus = fc.utobj().generateTestData(dataSet.get("CorpcampaignName"));
			String privatecorpaccessibilitystatus = dataSet.get("Corpaccessibilityprivate");
			String thirdcampaignTypestatus = dataSet.get("StatusCampaigntype");
			String thirdtemplateNamestatus = fc.utobj().generateTestData(dataSet.get("CorptemplateName"));
			String thirdemailSubjectstatus = fc.utobj().generateTestData(dataSet.get("CorpemailSubject"));
			String thirdplainTextEditorstatus = fc.utobj().generateTestData(dataSet.get("CorpplainTextEditor"));
			campaignCenterPage.createCampaignGenericwithSqlite(driver, dataSet, thirdcampaignNamestatus, privatecorpaccessibilitystatus,
					thirdcampaignTypestatus, thirdtemplateNamestatus, thirdemailSubjectstatus, thirdplainTextEditorstatus);
			
			fc.utobj().clickElement(driver, pobjcam.viewAllButton);
			
			//Verify Public to all users Campaign created by Corporate user who has privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Public to all users Campaign created by Corporate user who has privilege to see private campaign by itself");
			campaignCenterPage.searchCampaignByFilter(driver, firstcampaignName);

			boolean isCampaignNamePresentCorpaccessibilitypublictoallusers = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firstcampaignName + "')]");
			if (isCampaignNamePresentCorpaccessibilitypublictoallusers == false) {
				fc.utobj().throwsException("was not able to verify Public to all users Campaign created by Corporate user who has privilege to see private campaign by itself");
			}
			//Verify Public to all corporate users Campaign created by Corporate user who has privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Public to all corporate users Campaign created by Corporate user who has privilege to see private campaign");
			campaignCenterPage.searchCampaignByFilter(driver, secondcampaignName);
			
			boolean isCampaignNamePresentCorpaccessibilitypublictoallcorporateusers = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondcampaignName + "')]");
			if (isCampaignNamePresentCorpaccessibilitypublictoallcorporateusers == false) {
				fc.utobj().throwsException("was not able to verify Public to all corporate users Campaign created by Corporate user who has privilege to see private campaign by itself");
			}
			
			//Verify Private Campaign created by Corporate user who has privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Private Campaign created by Corporate user who has privilege to see private campaign by itself");
			campaignCenterPage.searchCampaignByFilter(driver, thirdcampaignName);
			
			boolean isCampaignNamePresentCorpaccessibilityprivate = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + thirdcampaignName + "')]");
			if (isCampaignNamePresentCorpaccessibilityprivate == false) {
				fc.utobj().throwsException("was not able to Private Campaign created by Corporate user who has privilege to see private campaign by itself");
			}
			
			//Verify Public to all users(Status Driven) Campaign created by Corporate user who has privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Public to all users(Status Driven) Campaign created by Corporate user who has privilege to see private campaign by itself");
			campaignCenterPage.searchCampaignByFilter(driver, firstcampaignNamestatus);

			boolean isCampaignNamePresentCorpaccessibilitypublictoallusersstatus = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firstcampaignNamestatus + "')]");
			if (isCampaignNamePresentCorpaccessibilitypublictoallusersstatus == false) {
				fc.utobj().throwsException("was not able to verify Public to all users(Status Driven) Campaign created by Corporate user who has privilege to see private campaign by itself");
			}
			//Verify Public to all corporate users(Status Driven) Campaign created by Corporate user who has privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Public to all corporate users(Status Driven) Campaign created by Corporate user who has privilege to see private campaign");
			campaignCenterPage.searchCampaignByFilter(driver, secondcampaignNamestatus);
			
			boolean isCampaignNamePresentCorpaccessibilitypublictoallcorporateusersstatus = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondcampaignNamestatus + "')]");
			if (isCampaignNamePresentCorpaccessibilitypublictoallcorporateusersstatus == false) {
				fc.utobj().throwsException("was not able to verify Public to all corporate users(Status Driven) Campaign created by Corporate user who has privilege to see private campaign by itself");
			}
			
			//Verify Private Campaign created by Corporate user(Status Driven) who has privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Private Campaign(Status Driven) created by Corporate user who has privilege to see private campaign by itself");
			campaignCenterPage.searchCampaignByFilter(driver, thirdcampaignNamestatus);
			
			boolean isCampaignNamePresentCorpaccessibilityprivatestatus = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + thirdcampaignNamestatus + "')]");
			if (isCampaignNamePresentCorpaccessibilityprivatestatus == false) {
				fc.utobj().throwsException("was not able to Private Campaign(Status Driven) created by Corporate user who has privilege to see private campaign by itself");
			}
		
			fc.utobj().printTestStep("Navigate to CRM>Campaign Center>email template");
			
			emailtemplate.clickonemailtamplate(driver);
			
			//Verify Public to all users template created by Corporate user who has privilege to see private template by itself
			fc.utobj().printTestStep("Verify Public to all users template created by Corporate user who has privilege to see private template by itself");
			campaignCenterPage.searchTemplateByFilter(driver, firsttemplateName);

			boolean isTemplateNamePresentCorpaccessibilitypublictoallusers = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firsttemplateName + "')]");
			if (isTemplateNamePresentCorpaccessibilitypublictoallusers == false) {
				fc.utobj().throwsException("was not able to verify Public to all users template created by Corporate user who has privilege to see private template by itself");
			}
			//Verify Public to all corporate users template created by Corporate user who has privilege to see private template by itself
			fc.utobj().printTestStep("Verify Public to all corporate users template created by Corporate user who has privilege to see private template by itself");
			campaignCenterPage.searchTemplateByFilter(driver, secondtemplateName);
			
			boolean isTemplateNamePresentCorpaccessibilitypublictoallcorporateusers = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondtemplateName + "')]");
			if (isTemplateNamePresentCorpaccessibilitypublictoallcorporateusers == false) {
				fc.utobj().throwsException("was not able to verify Public to all users template created by Corporate user who has privilege to see private template by itself");
			}
			
			//Verify Private template created by Corporate user who has privilege to see private template by itself
			fc.utobj().printTestStep("Verify Privare template created by Corporate user who has privilege to see private template by itself");
			campaignCenterPage.searchTemplateByFilter(driver, thirdtemplateName);
			
			boolean isTemplateNamePresentCorpaccessibilityprivate = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + thirdtemplateName + "')]");
			if (isTemplateNamePresentCorpaccessibilityprivate == false) {
				fc.utobj().throwsException("was not able to verify private template created by Corporate user who has privilege to see private template by itself");
			}
				
			//Verify Public to all users template(Status Driven) created by Corporate user who has privilege to see private template by itself
			fc.utobj().printTestStep("Verify Public to all users template(Status Driven) created by Corporate user who has privilege to see private template by itself");
			campaignCenterPage.searchTemplateByFilter(driver, firsttemplateNamestatus);

			boolean isTemplateNamePresentCorpaccessibilitypublictoallusersstatus = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firsttemplateNamestatus + "')]");
			if (isTemplateNamePresentCorpaccessibilitypublictoallusersstatus == false) {
				fc.utobj().throwsException("was not able to verify Public to all users template(Status Driven) created by Corporate user who has privilege to see private template by itself");
			}
			//Verify Public to all corporate users template(Status Driven) created by Corporate user who has privilege to see private template by itself
			fc.utobj().printTestStep("Verify Public to all corporate users template(Status Driven) created by Corporate user who has privilege to see private template by itself");
			campaignCenterPage.searchTemplateByFilter(driver, secondtemplateNamestatus);
			
			boolean isTemplateNamePresentCorpaccessibilitypublictoallcorporateusersstatus = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondtemplateNamestatus + "')]");
			if (isTemplateNamePresentCorpaccessibilitypublictoallcorporateusersstatus == false) {
				fc.utobj().throwsException("was not able to verify Public to all users template(Status Driven) created by Corporate user who has privilege to see private template by itself");
			}
			
			//Verify Private template(Status Driven) created by Corporate user who has privilege to see private template by itself
			fc.utobj().printTestStep("Verify Privare template(Status Driven) created by Corporate user who has privilege to see private template by itself");
			campaignCenterPage.searchTemplateByFilter(driver, thirdtemplateNamestatus);
			
			boolean isTemplateNamePresentCorpaccessibilityprivatestatus = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + thirdtemplateNamestatus + "')]");
			if (isTemplateNamePresentCorpaccessibilityprivatestatus == false) {
				fc.utobj().throwsException("was not able to verify private template(Status Driven) created by Corporate user who has privilege to see private template by itself");
			}
			
			
			
			//Login with Corporate user who does not have role to see private campaign(C2)
			fc.utobj().printTestStep("Login with Corporate user who does not have role to see private campaign(C2)");
			fc.loginpage().loginWithParameter(driver, CorpUserName1, CorpUserPassword1);
			fc.crm().crm_common().openCRMCampaignCenterPage(fcHomePageTest, driver);
		
			//Create Campaign At Campaign Center > Campaigns
			fc.utobj().printTestStep("Create Campaign At Campaign Center > Campaigns");
			String firstcampaignNamecorp = fc.utobj().generateTestData(dataSet.get("CorpcampaignName"));
			String firstaccessibilitycorp = dataSet.get("Corpaccessibilitypublictoallusers");
			String firstcampaignTypecorp = dataSet.get("CorpcampaignType");
			String firsttemplateNamecorp = fc.utobj().generateTestData(dataSet.get("CorptemplateName"));
			String firstemailSubjectcorp = fc.utobj().generateTestData(dataSet.get("CorpemailSubject"));
			String firstplainTextEditorcorp = fc.utobj().generateTestData(dataSet.get("CorpplainTextEditor"));
			campaignCenterPage.createCampaignGenericwithSqlite(driver, dataSet, firstcampaignNamecorp, firstaccessibilitycorp,
					firstcampaignTypecorp, firsttemplateNamecorp, firstemailSubjectcorp, firstplainTextEditorcorp);
			
			//Create another campaign at Campaign Center > Campaigns
			fc.utobj().printTestStep("Create another campaign at Campaign Center > Campaigns");
			String secondcampaignNamecorp = fc.utobj().generateTestData(dataSet.get("CorpcampaignName"));
			String publictocorporateusersaccessibilitycorp = dataSet.get("Corpaccessibilitypublictoallcorporateusers");
			String secondcampaignTypecorp = dataSet.get("CorpcampaignType");
			String secondtemplateNamecorp = fc.utobj().generateTestData(dataSet.get("CorptemplateName"));
			String secondemailSubjectcorp = fc.utobj().generateTestData(dataSet.get("CorpemailSubject"));
			String secondplainTextEditorcorp = fc.utobj().generateTestData(dataSet.get("CorpplainTextEditor"));
			campaignCenterPage.createCampaignGenericwithSqlite(driver, dataSet, secondcampaignNamecorp, publictocorporateusersaccessibilitycorp,
					secondcampaignTypecorp, secondtemplateNamecorp, secondemailSubjectcorp, secondplainTextEditorcorp);
			
			//Create another campaign1 at Campaign Center > Campaigns
			fc.utobj().printTestStep("Create another campaign at Campaign Center > Campaigns");
			String thirdcampaignNamecorp = fc.utobj().generateTestData(dataSet.get("CorpcampaignName"));
			String privatecorpaccessibilitycorp = dataSet.get("Corpaccessibilityprivate");
			String thirdcampaignTypecorp = dataSet.get("CorpcampaignType");
			String thirdtemplateNamecorp = fc.utobj().generateTestData(dataSet.get("CorptemplateName"));
			String thirdemailSubjectcorp = fc.utobj().generateTestData(dataSet.get("CorpemailSubject"));
			String thirdplainTextEditorcorp = fc.utobj().generateTestData(dataSet.get("CorpplainTextEditor"));
			campaignCenterPage.createCampaignGenericwithSqlite(driver, dataSet, thirdcampaignNamecorp, privatecorpaccessibilitycorp,
					thirdcampaignTypecorp, thirdtemplateNamecorp, thirdemailSubjectcorp, thirdplainTextEditorcorp);
			
			
			
			//Create Status Driven Campaign At Campaign Center > Campaigns
			fc.utobj().printTestStep("Create Status Driven Campaign At Campaign Center > Campaigns");
			String firstcampaignNamecorpstatus = fc.utobj().generateTestData(dataSet.get("CorpcampaignName"));
			String firstaccessibilitycorpstatus = dataSet.get("Corpaccessibilitypublictoallusers");
			String firstcampaignTypecorpstatus = dataSet.get("StatusCampaigntype");
			String firsttemplateNamecorpstatus = fc.utobj().generateTestData(dataSet.get("CorptemplateName"));
			String firstemailSubjectcorpstatus = fc.utobj().generateTestData(dataSet.get("CorpemailSubject"));
			String firstplainTextEditorcorpstatus = fc.utobj().generateTestData(dataSet.get("CorpplainTextEditor"));
			campaignCenterPage.createCampaignGenericwithSqlite(driver, dataSet, firstcampaignNamecorpstatus, firstaccessibilitycorpstatus,
					firstcampaignTypecorpstatus, firsttemplateNamecorpstatus, firstemailSubjectcorpstatus, firstplainTextEditorcorpstatus);
			
			//Create Status Driven campaign 1 at Campaign Center > Campaigns
			fc.utobj().printTestStep("Create Status Driven campaign 1 at Campaign Center > Campaigns");
			String secondcampaignNamecorpstatus = fc.utobj().generateTestData(dataSet.get("CorpcampaignName"));
			String publictocorporateusersaccessibilitycorpstatus = dataSet.get("Corpaccessibilitypublictoallcorporateusers");
			String secondcampaignTypecorpstatus = dataSet.get("StatusCampaigntype");
			String secondtemplateNamecorpstatus = fc.utobj().generateTestData(dataSet.get("CorptemplateName"));
			String secondemailSubjectcorpstatus = fc.utobj().generateTestData(dataSet.get("CorpemailSubject"));
			String secondplainTextEditorcorpstatus = fc.utobj().generateTestData(dataSet.get("CorpplainTextEditor"));
			campaignCenterPage.createCampaignGenericwithSqlite(driver, dataSet, secondcampaignNamecorpstatus, publictocorporateusersaccessibilitycorpstatus,
					secondcampaignTypecorpstatus, secondtemplateNamecorpstatus, secondemailSubjectcorpstatus, secondplainTextEditorcorpstatus);
			
			//Create Status Driver campaign 2 at Campaign Center > Campaigns
			fc.utobj().printTestStep("Create Status Driver campaign 2 at Campaign Center > Campaigns");
			String thirdcampaignNamecorpstatus = fc.utobj().generateTestData(dataSet.get("CorpcampaignName"));
			String privatecorpaccessibilitycorpstatus = dataSet.get("Corpaccessibilityprivate");
			String thirdcampaignTypecorpstatus = dataSet.get("StatusCampaigntype");
			String thirdtemplateNamecorpstatus = fc.utobj().generateTestData(dataSet.get("CorptemplateName"));
			String thirdemailSubjectcorpstatus = fc.utobj().generateTestData(dataSet.get("CorpemailSubject"));
			String thirdplainTextEditorcorpstatus = fc.utobj().generateTestData(dataSet.get("CorpplainTextEditor"));
			campaignCenterPage.createCampaignGenericwithSqlite(driver, dataSet, thirdcampaignNamecorpstatus, privatecorpaccessibilitycorpstatus,
					thirdcampaignTypecorpstatus, thirdtemplateNamecorpstatus, thirdemailSubjectcorpstatus, thirdplainTextEditorcorpstatus);
		
			fc.utobj().clickElement(driver, pobjcam.viewAllButton);
			
			//Verify Public to all users Campaign created by Corporate user who does not have privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Public to all users Campaign created by Corporate user who does not have privilege to see private campaign by itself");
			campaignCenterPage.searchCampaignByFilter(driver, firstcampaignNamecorp);

			boolean isCampaignNamePresentCorpaccessibilitypublictoalluserscorp = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firstcampaignNamecorp + "')]");
			if (isCampaignNamePresentCorpaccessibilitypublictoalluserscorp == false) {
				fc.utobj().throwsException("was not able to verify Public to all users Campaign created by Corporate user who does not have privilege to see private campaign by itself");
			}
			//Verify Public to all corporate users Campaign created by Corporate user who has privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Public to all corporate users Campaign created by Corporate user who does not have privilege to see private campaign");
			campaignCenterPage.searchCampaignByFilter(driver, secondcampaignNamecorp);
			
			boolean isCampaignNamePresentCorpaccessibilitypublictoallcorporateuserscorp = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondcampaignNamecorp + "')]");
			if (isCampaignNamePresentCorpaccessibilitypublictoallcorporateuserscorp == false) {
				fc.utobj().throwsException("was not able to verify Public to all corporate users Campaign created by Corporate user who does not privilege to see private campaign by itself");
			}
			
			//Verify Private Campaign created by Corporate user who has privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Private Campaign created by Corporate user who does not have privilege to see private campaign by itself");
			campaignCenterPage.searchCampaignByFilter(driver, thirdcampaignNamecorp);
			
			boolean isCampaignNamePresentCorpaccessibilityprivatecorp = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + thirdcampaignNamecorp + "')]");
			if (isCampaignNamePresentCorpaccessibilityprivatecorp == false) {
				fc.utobj().throwsException("was not able to Private Campaign created by Corporate user who does not have privilege to see private campaign by itself");
			}
			
			
			//Verify Public to all users (Status Driven)Campaign created by Corporate user who does not have privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Public to all users(Status Driven) Campaign created by Corporate user who does not have privilege to see private campaign by itself");
			campaignCenterPage.searchCampaignByFilter(driver, firstcampaignNamecorpstatus);

			boolean isCampaignNamePresentCorpaccessibilitypublictoalluserscorpstatus = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firstcampaignNamecorpstatus + "')]");
			if (isCampaignNamePresentCorpaccessibilitypublictoalluserscorpstatus == false) {
				fc.utobj().throwsException("was not able to verify Public to all users(Status Driven) Campaign created by Corporate user who does not have privilege to see private campaign by itself");
			}
			//Verify Public to all corporate users Campaign created by Corporate user who has privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Public to all corporate users(Status Driven) Campaign created by Corporate user who does not have privilege to see private campaign");
			campaignCenterPage.searchCampaignByFilter(driver, secondcampaignNamecorpstatus);
			
			boolean isCampaignNamePresentCorpaccessibilitypublictoallcorporateuserscorpstatus = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondcampaignNamecorpstatus + "')]");
			if (isCampaignNamePresentCorpaccessibilitypublictoallcorporateuserscorpstatus == false) {
				fc.utobj().throwsException("was not able to verify Public to all corporate users(Status Driven) Campaign created by Corporate user who does not privilege to see private campaign by itself");
			}
			
			//Verify Private Campaign created by Corporate user who has privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Private Campaign created by Corporate user(Status Driven) who does not have privilege to see private campaign by itself");
			campaignCenterPage.searchCampaignByFilter(driver, thirdcampaignNamecorpstatus);
			
			boolean isCampaignNamePresentCorpaccessibilityprivatecorpstatus = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + thirdcampaignNamecorpstatus + "')]");
			if (isCampaignNamePresentCorpaccessibilityprivatecorpstatus == false) {
				fc.utobj().throwsException("was not able to Private Campaign created by Corporate user(Status Driven) who does not have privilege to see private campaign by itself");
			}
				
			fc.utobj().printTestStep("Navigate to CRM>Campaign Center>email template");
			emailtemplate.clickonemailtamplate(driver);
			
			//Verify Public to all users template created by Corporate user who does not have privilege to see private template by itself
			fc.utobj().printTestStep("Verify Public to all users template created by Corporate user who does not have privilege to see private template by itself");
			campaignCenterPage.searchTemplateByFilter(driver, firsttemplateNamecorp);

			boolean isTemplateNamePresentCorpaccessibilitypublictoalluserscorp = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firsttemplateNamecorp + "')]");
			if (isTemplateNamePresentCorpaccessibilitypublictoalluserscorp == false) {
				fc.utobj().throwsException("was not able to verify Public to all users template created by Corporate user who has privilege to see private template by itself");
			}
			//Verify Public to all corporate users template created by Corporate user who has privilege to see private template by itself
			fc.utobj().printTestStep("Verify Public to all corporate users template created by Corporate user who does not have privilege to see private template by itself");
			campaignCenterPage.searchTemplateByFilter(driver, secondtemplateNamecorp);
			
			boolean isTemplateNamePresentCorpaccessibilitypublictoallcorporateuserscorp = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondtemplateNamecorp + "')]");
			if (isTemplateNamePresentCorpaccessibilitypublictoallcorporateuserscorp == false) {
				fc.utobj().throwsException("was not able to verify Public to all users template created by Corporate user who has privilege to see private template by itself");
			}
			
			//Verify Private template created by Corporate user who has privilege to see private template by itself
			fc.utobj().printTestStep("Verify Privare template created by Corporate user who does not have privilege to see private template by itself");
			campaignCenterPage.searchTemplateByFilter(driver, thirdtemplateNamecorp);
			
			boolean isTemplateNamePresentCorpaccessibilityprivatecorp = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + thirdtemplateNamecorp + "')]");
			if (isTemplateNamePresentCorpaccessibilityprivatecorp == false) {
				fc.utobj().throwsException("was not able to verify private template created by Corporate user who has privilege to see private template by itself");
			}
			
			
			//Verify Public to all users (Status driven)template created by Corporate user who does not have privilege to see private template by itself
			fc.utobj().printTestStep("Verify Public to all users (Status driven)template created by Corporate user who does not have privilege to see private template by itself");
			campaignCenterPage.searchTemplateByFilter(driver, firsttemplateNamecorpstatus);

			boolean isTemplateNamePresentCorpaccessibilitypublictoalluserscorpstatus = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firsttemplateNamecorpstatus + "')]");
			if (isTemplateNamePresentCorpaccessibilitypublictoalluserscorpstatus == false) {
				fc.utobj().throwsException("was not able to verify Public to all users (Status driven)template created by Corporate user who has privilege to see private template by itself");
			}
			//Verify Public to all corporate users template created by Corporate user who has privilege to see private template by itself
			fc.utobj().printTestStep("Verify Public to all corporate users (Status driven)template created by Corporate user who does not have privilege to see private template by itself");
			campaignCenterPage.searchTemplateByFilter(driver, secondtemplateNamecorpstatus);
			
			boolean isTemplateNamePresentCorpaccessibilitypublictoallcorporateuserscorpstatus = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondtemplateNamecorpstatus + "')]");
			if (isTemplateNamePresentCorpaccessibilitypublictoallcorporateuserscorpstatus == false) {
				fc.utobj().throwsException("was not able to verify Public to all users (Status driven)template created by Corporate user who has privilege to see private template by itself");
			}
			
			//Verify Private template created by Corporate user who has privilege to see private template by itself
			fc.utobj().printTestStep("Verify Privare (Status driven)template created by Corporate user who does not have privilege to see private template by itself");
			campaignCenterPage.searchTemplateByFilter(driver, thirdtemplateNamecorpstatus);
			
			boolean isTemplateNamePresentCorpaccessibilityprivatecorpstatus = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + thirdtemplateNamecorpstatus + "')]");
			if (isTemplateNamePresentCorpaccessibilityprivatecorpstatus == false) {
				fc.utobj().throwsException("was not able to verify private (Status driven)template created by Corporate user who has privilege to see private template by itself");
			}
			
			
			//Login with Regional user who has role to see private campaign(R1)
			fc.utobj().printTestStep("Login with Regional user who has role to see private campaign(R1)");
			fc.loginpage().loginWithParameter(driver, Reguname, Regpassword);
			fc.crm().crm_common().openCRMCampaignCenterPage(fcHomePageTest, driver);
			
			//Create Campaign At Campaign Center > Campaigns
			fc.utobj().printTestStep("Create Campaign At Campaign Center > Campaigns");
			String firstcampaignNamereg = fc.utobj().generateTestData(dataSet.get("CorpcampaignName"));
			String firstaccessibilityreg = dataSet.get("Regaccessibilitypublictoallusersofmyregion");
			String firstcampaignTypereg = dataSet.get("CorpcampaignType");
			String firsttemplateNamereg = fc.utobj().generateTestData(dataSet.get("CorptemplateName"));
			String firstemailSubjectreg = fc.utobj().generateTestData(dataSet.get("CorpemailSubject"));
			String firstplainTextEditorreg = fc.utobj().generateTestData(dataSet.get("CorpplainTextEditor"));
			campaignCenterPage.createCampaignGenericwithSqlite(driver, dataSet, firstcampaignNamereg, firstaccessibilityreg,
					firstcampaignTypereg, firsttemplateNamereg, firstemailSubjectreg, firstplainTextEditorreg);
			
			//Create another campaign at Campaign Center > Campaigns
			fc.utobj().printTestStep("Create another campaign at Campaign Center > Campaigns");
			String secondcampaignNamereg = fc.utobj().generateTestData(dataSet.get("CorpcampaignName"));
			String privateaccessibilityreg = dataSet.get("Regaccessibilityprivate");
			String secondcampaignTypereg = dataSet.get("CorpcampaignType");
			String secondtemplateNamereg = fc.utobj().generateTestData(dataSet.get("CorptemplateName"));
			String secondemailSubjectreg = fc.utobj().generateTestData(dataSet.get("CorpemailSubject"));
			String secondplainTextEditorreg = fc.utobj().generateTestData(dataSet.get("CorpplainTextEditor"));
			campaignCenterPage.createCampaignGenericwithSqlite(driver, dataSet, secondcampaignNamereg, privateaccessibilityreg,
					secondcampaignTypereg, secondtemplateNamereg, secondemailSubjectreg, secondplainTextEditorreg);
			
			//Create Status Driven Campaign At Campaign Center > Campaigns
			fc.utobj().printTestStep("Create Status Driven Campaign At Campaign Center > Campaigns");
			String firstcampaignNameregstatus = fc.utobj().generateTestData(dataSet.get("CorpcampaignName"));
			String firstaccessibilityregstatus = dataSet.get("Regaccessibilitypublictoallusersofmyregion");
			String firstcampaignTyperegstatus = dataSet.get("StatusCampaigntype");
			String firsttemplateNameregstatus = fc.utobj().generateTestData(dataSet.get("CorptemplateName"));
			String firstemailSubjectregstatus = fc.utobj().generateTestData(dataSet.get("CorpemailSubject"));
			String firstplainTextEditorregstatus = fc.utobj().generateTestData(dataSet.get("CorpplainTextEditor"));
			campaignCenterPage.createCampaignGenericwithSqlite(driver, dataSet, firstcampaignNameregstatus, firstaccessibilityregstatus,
					firstcampaignTyperegstatus, firsttemplateNameregstatus, firstemailSubjectregstatus, firstplainTextEditorregstatus);
			
			//Create Status driven campaign 1 at Campaign Center > Campaigns
			fc.utobj().printTestStep("Create Status Driven campaign 1 at Campaign Center > Campaigns");
			String secondcampaignNameregstatus = fc.utobj().generateTestData(dataSet.get("CorpcampaignName"));
			String privateaccessibilityregstatus = dataSet.get("Regaccessibilityprivate");
			String secondcampaignTyperegstatus = dataSet.get("StatusCampaigntype");
			String secondtemplateNameregstatus = fc.utobj().generateTestData(dataSet.get("CorptemplateName"));
			String secondemailSubjectregstatus = fc.utobj().generateTestData(dataSet.get("CorpemailSubject"));
			String secondplainTextEditorregstatus = fc.utobj().generateTestData(dataSet.get("CorpplainTextEditor"));
			campaignCenterPage.createCampaignGenericwithSqlite(driver, dataSet, secondcampaignNameregstatus, privateaccessibilityregstatus,
					secondcampaignTyperegstatus, secondtemplateNameregstatus, secondemailSubjectregstatus, secondplainTextEditorregstatus);
			
			fc.utobj().clickElement(driver, pobjcam.viewAllButton);
			
			//Verify Public to all users of my region Campaign created by Regional user who has privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Public to all users of my region Campaign created by Regional user who has privilege to see private campaign by itself");
			campaignCenterPage.searchCampaignByFilter(driver, firstcampaignNamereg);

			boolean isCampaignNamePresentRegpaccessibilitypublictoallusersofmyregion = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firstcampaignNamereg + "')]");
			if (isCampaignNamePresentRegpaccessibilitypublictoallusersofmyregion == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my region Campaign created by Regional user who has privilege to see private campaign by itself");
			}
			
			//Verify Private Campaign created by Regional user who has privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Private Campaign created by Regional user who has privilege to see private campaign by itself");
			campaignCenterPage.searchCampaignByFilter(driver, secondcampaignNamereg);
			
			boolean isCampaignNamePresentRegpaccessibilityprivate = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondcampaignNamereg + "')]");
			if (isCampaignNamePresentRegpaccessibilityprivate == false) {
				fc.utobj().throwsException("was not able to verify Private Campaign created by Regional user who has privilege to see private campaign by itself");
			}
			
			//Verify Public to all users of my region (Status Driven)Campaign created by Regional user who has privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Public to all users of my region (Status Driven)Campaign created by Regional user who has privilege to see private campaign by itself");
			campaignCenterPage.searchCampaignByFilter(driver, firstcampaignNameregstatus);

			boolean isCampaignNamePresentRegpaccessibilitypublictoallusersofmyregionstatus = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firstcampaignNameregstatus + "')]");
			if (isCampaignNamePresentRegpaccessibilitypublictoallusersofmyregionstatus == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my region (Status Driven)Campaign created by Regional user who has privilege to see private campaign by itself");
			}
			
			//Verify Private Campaign created by Regional user who has privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Private (Status Driven)Campaign created by Regional user who has privilege to see private campaign by itself");
			campaignCenterPage.searchCampaignByFilter(driver, secondcampaignNameregstatus);
			
			boolean isCampaignNamePresentRegpaccessibilityprivatestatus = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondcampaignNameregstatus + "')]");
			if (isCampaignNamePresentRegpaccessibilityprivatestatus == false) {
				fc.utobj().throwsException("was not able to verify Private (Status Driven)Campaign created by Regional user who has privilege to see private campaign by itself");
			}
			
			fc.utobj().printTestStep("Navigate to CRM>Campaign Center>email template");
			emailtemplate.clickonemailtamplate(driver);
			
			//Verify Public to all users template created by Corporate user who has privilege to see private template by itself
			fc.utobj().printTestStep("Verify Public to all users of my region template created by Regional user who has privilege to see private template by itself");
			campaignCenterPage.searchTemplateByFilter(driver, firsttemplateNamereg);

			boolean isTemplateNamePresentRegaccessibilitypublictoallusersofmyregion = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firsttemplateNamereg + "')]");
			if (isTemplateNamePresentRegaccessibilitypublictoallusersofmyregion == false) {
				fc.utobj().throwsException("was not able to verify Public to all users template created by Regional user who has privilege to see private template by itself");
			}
			
			//Verify Private template created by Regional user who has privilege to see private template by itself
			fc.utobj().printTestStep("Verify Private template created by Regional user who has privilege to see private template by itself");
			campaignCenterPage.searchTemplateByFilter(driver, secondtemplateNamereg);
			
			boolean isTemplateNamePresentRegaccessibilityprivate = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondtemplateNamereg + "')]");
			if (isTemplateNamePresentRegaccessibilityprivate == false) {
				fc.utobj().throwsException("was not able to verify private template created by Regional user who has privilege to see private template by itself");
			}
			
			//Verify Public to all users(Status Driven) template created by Regional user who has privilege to see private template by itself
			fc.utobj().printTestStep("Verify Public to all users of my region (Status Driven)template created by Regional user who has privilege to see private template by itself");
			campaignCenterPage.searchTemplateByFilter(driver, firsttemplateNameregstatus);

			boolean isTemplateNamePresentRegaccessibilitypublictoallusersofmyregionstatus = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firsttemplateNameregstatus + "')]");
			if (isTemplateNamePresentRegaccessibilitypublictoallusersofmyregionstatus == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my region (Status Driven)template created by Regional user who has privilege to see private template by itself");
			}
			
			//Verify Private template created by Regional user who has privilege to see private template by itself
			fc.utobj().printTestStep("Verify Private (Status Driven)template created by Regional user who has privilege to see private template by itself");
			campaignCenterPage.searchTemplateByFilter(driver, secondtemplateNameregstatus);
			
			boolean isTemplateNamePresentRegaccessibilityprivatestatus = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondtemplateNameregstatus + "')]");
			if (isTemplateNamePresentRegaccessibilityprivatestatus == false) {
				fc.utobj().throwsException("was not able to verify private (Status Driven)template created by Regional user who has privilege to see private template by itself");
			}
			
			
			//Login with Regional user who does not have role to see private campaign(R2)
			fc.utobj().printTestStep("Login with Regional user who does not have role to see private campaign(R2)");
			fc.loginpage().loginWithParameter(driver, Reguname1, Regpassword1);
			fc.crm().crm_common().openCRMCampaignCenterPage(fcHomePageTest, driver);
			
			//Create Campaign At Campaign Center > Campaigns
			fc.utobj().printTestStep("Create Campaign At Campaign Center > Campaigns");
			String firstcampaignNamereg1 = fc.utobj().generateTestData(dataSet.get("CorpcampaignName"));
			String firstaccessibilityreg1 = dataSet.get("Regaccessibilitypublictoallusersofmyregion");
			String firstcampaignTypereg1 = dataSet.get("CorpcampaignType");
			String firsttemplateNamereg1 = fc.utobj().generateTestData(dataSet.get("CorptemplateName"));
			String firstemailSubjectreg1= fc.utobj().generateTestData(dataSet.get("CorpemailSubject"));
			String firstplainTextEditorreg1 = fc.utobj().generateTestData(dataSet.get("CorpplainTextEditor"));
			campaignCenterPage.createCampaignGenericwithSqlite(driver, dataSet, firstcampaignNamereg1, firstaccessibilityreg1,
					firstcampaignTypereg1, firsttemplateNamereg1, firstemailSubjectreg1, firstplainTextEditorreg1);
			
			//Create another campaign at Campaign Center > Campaigns
			fc.utobj().printTestStep("Create another campaign at Campaign Center > Campaigns");
			String secondcampaignNamereg1 = fc.utobj().generateTestData(dataSet.get("CorpcampaignName"));
			String privateaccessibilityreg1 = dataSet.get("Regaccessibilityprivate");
			String secondcampaignTypereg1 = dataSet.get("CorpcampaignType");
			String secondtemplateNamereg1 = fc.utobj().generateTestData(dataSet.get("CorptemplateName"));
			String secondemailSubjectreg1 = fc.utobj().generateTestData(dataSet.get("CorpemailSubject"));
			String secondplainTextEditorreg1 = fc.utobj().generateTestData(dataSet.get("CorpplainTextEditor"));
			campaignCenterPage.createCampaignGenericwithSqlite(driver, dataSet, secondcampaignNamereg1, privateaccessibilityreg1,
					secondcampaignTypereg1, secondtemplateNamereg1, secondemailSubjectreg1, secondplainTextEditorreg1);
			
			//Create Status Driven Campaign At Campaign Center > Campaigns
			fc.utobj().printTestStep("Create Status Driven Campaign At Campaign Center > Campaigns");
			String firstcampaignNamereg1status = fc.utobj().generateTestData(dataSet.get("CorpcampaignName"));
			String firstaccessibilityreg1status = dataSet.get("Regaccessibilitypublictoallusersofmyregion");
			String firstcampaignTypereg1status = dataSet.get("StatusCampaigntype");
			String firsttemplateNamereg1status = fc.utobj().generateTestData(dataSet.get("CorptemplateName"));
			String firstemailSubjectreg1status= fc.utobj().generateTestData(dataSet.get("CorpemailSubject"));
			String firstplainTextEditorreg1status = fc.utobj().generateTestData(dataSet.get("CorpplainTextEditor"));
			campaignCenterPage.createCampaignGenericwithSqlite(driver, dataSet, firstcampaignNamereg1status, firstaccessibilityreg1status,
					firstcampaignTypereg1status, firsttemplateNamereg1status, firstemailSubjectreg1status, firstplainTextEditorreg1status);
			
			//Create Status Driven campaign at Campaign Center > Campaigns
			fc.utobj().printTestStep("Create Status Driven campaign at Campaign Center > Campaigns");
			String secondcampaignNamereg1status = fc.utobj().generateTestData(dataSet.get("CorpcampaignName"));
			String privateaccessibilityreg1status = dataSet.get("Regaccessibilityprivate");
			String secondcampaignTypereg1status = dataSet.get("StatusCampaigntype");
			String secondtemplateNamereg1status = fc.utobj().generateTestData(dataSet.get("CorptemplateName"));
			String secondemailSubjectreg1status = fc.utobj().generateTestData(dataSet.get("CorpemailSubject"));
			String secondplainTextEditorreg1status = fc.utobj().generateTestData(dataSet.get("CorpplainTextEditor"));
			campaignCenterPage.createCampaignGenericwithSqlite(driver, dataSet, secondcampaignNamereg1status, privateaccessibilityreg1status,
					secondcampaignTypereg1status, secondtemplateNamereg1status, secondemailSubjectreg1status, secondplainTextEditorreg1status);
			
			fc.utobj().clickElement(driver, pobjcam.viewAllButton);
			
			//Verify Public to all users of my region Campaign created by Regional user who does not have privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Public to all users of my region Campaign created by Regional user who does not have privilege to see private campaign by itself");
			campaignCenterPage.searchCampaignByFilter(driver, firstcampaignNamereg1);

			boolean isCampaignNamePresentRegpaccessibilitypublictoallusersofmyregion1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firstcampaignNamereg1 + "')]");
			if (isCampaignNamePresentRegpaccessibilitypublictoallusersofmyregion1 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my region Campaign created by Regional user who does not have privilege to see private campaign by itself");
			}
			
			//Verify Private Campaign created by Regional user who does not have privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Private Campaign created by Regional user who does not have privilege to see private campaign by itself");
			campaignCenterPage.searchCampaignByFilter(driver, secondcampaignNamereg1);
			
			boolean isCampaignNamePresentRegpaccessibilityprivate1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondcampaignNamereg1 + "')]");
			if (isCampaignNamePresentRegpaccessibilityprivate1 == false) {
				fc.utobj().throwsException("was not able to verify Private Campaign created by Regional user who does not have privilege to see private campaign by itself");
			}
			
			//Verify Public to all users of my region (Status Driven)Campaign created by Regional user who does not have privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Public to all users of my region (Status Driven)Campaign created by Regional user who does not have privilege to see private campaign by itself");
			campaignCenterPage.searchCampaignByFilter(driver, firstcampaignNamereg1status);

			boolean isCampaignNamePresentRegpaccessibilitypublictoallusersofmyregion1status = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firstcampaignNamereg1status + "')]");
			if (isCampaignNamePresentRegpaccessibilitypublictoallusersofmyregion1status == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my region (Status Driven)Campaign created by Regional user who does not have privilege to see private campaign by itself");
			}
			
			//Verify Private (Status Driven)Campaign created by Regional user who does not have privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Private (Status Driven)Campaign created by Regional user who does not have privilege to see private campaign by itself");
			campaignCenterPage.searchCampaignByFilter(driver, secondcampaignNamereg1status);
			
			boolean isCampaignNamePresentRegpaccessibilityprivate1status = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondcampaignNamereg1status + "')]");
			if (isCampaignNamePresentRegpaccessibilityprivate1status == false) {
				fc.utobj().throwsException("was not able to verify Private (Status Driven)Campaign created by Regional user who does not have privilege to see private campaign by itself");
			}
		
			fc.utobj().printTestStep("Navigate to CRM>Campaign Center>email template");
			emailtemplate.clickonemailtamplate(driver);
			
			//Verify Public to all users template created by Corporate user who does not have privilege to see private template by itself
			fc.utobj().printTestStep("Verify Public to all users of my region template created by Regional user who does not have privilege to see private template by itself");
			campaignCenterPage.searchTemplateByFilter(driver, firsttemplateNamereg1);

			boolean isTemplateNamePresentRegaccessibilitypublictoallusersofmyregion1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firsttemplateNamereg1 + "')]");
			if (isTemplateNamePresentRegaccessibilitypublictoallusersofmyregion1 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my region template created by Regional user who does not have privilege to see private template by itself");
			}
			
			//Verify Private template created by Regional user who does not have privilege to see private template by itself
			fc.utobj().printTestStep("Verify Private template created by Regional user does not have privilege to see private template by itself");
			campaignCenterPage.searchTemplateByFilter(driver, secondtemplateNamereg1);
			
			boolean isTemplateNamePresentRegaccessibilityprivate1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondtemplateNamereg1 + "')]");
			if (isTemplateNamePresentRegaccessibilityprivate1 == false) {
				fc.utobj().throwsException("was not able to verify Private template created by Regional user does not have privilege to see private template by itself");
			}
			
			//Verify Public to all users (Status Driven)template created by Regional user who does not have privilege to see private template by itself
			fc.utobj().printTestStep("Verify Public to all users of my region (Status Driven)template created by Regional user who does not have privilege to see private template by itself");
			campaignCenterPage.searchTemplateByFilter(driver, firsttemplateNamereg1status);

			boolean isTemplateNamePresentRegaccessibilitypublictoallusersofmyregion1status = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firsttemplateNamereg1status + "')]");
			if (isTemplateNamePresentRegaccessibilitypublictoallusersofmyregion1status == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my region (Status Driven)template created by Regional user who does not have privilege to see private template by itself");
			}
			
			//Verify Private template created by Regional user who does not have privilege to see private template by itself
			fc.utobj().printTestStep("Verify Private (Status Driven)template created by Regional user does not have privilege to see private template by itself");
			campaignCenterPage.searchTemplateByFilter(driver, secondtemplateNamereg1status);
			
			boolean isTemplateNamePresentRegaccessibilityprivate1status = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondtemplateNamereg1status + "')]");
			if (isTemplateNamePresentRegaccessibilityprivate1status == false) {
				fc.utobj().throwsException("was not able to verify Private (Status Driven)template created by Regional user does not have privilege to see private template by itself");
			}
			
			//Login with Franchise Owner MU user(V1) who has role to see private campaign
			fc.utobj().printTestStep("Login with Franchise Owner MU user(V1) who has role to see private campaign");
			fc.loginpage().loginWithParameter(driver, userNameFOmu, passwordFOmu);
			fc.crm().crm_common().openCRMCampaignCenterPage(fcHomePageTest, driver);
			
			//Create Campaign At Campaign Center > Campaigns
			fc.utobj().printTestStep("Create Campaign At Campaign Center > Campaigns");
			String firstcampaignNamefranmu = fc.utobj().generateTestData(dataSet.get("CorpcampaignName"));
			String firstaccessibilityfranmu = dataSet.get("FranaccessibilityPublictoallUsersofmyFranchise");
			String firstcampaignTypefranmu = dataSet.get("CorpcampaignType");
			String firsttemplateNamefranmu = fc.utobj().generateTestData(dataSet.get("CorptemplateName"));
			String firstemailSubjectfranmu = fc.utobj().generateTestData(dataSet.get("CorpemailSubject"));
			String firstplainTextEditorfranmu = fc.utobj().generateTestData(dataSet.get("CorpplainTextEditor"));
			campaignCenterPage.createCampaignGenericwithSqlite(driver, dataSet, firstcampaignNamefranmu, firstaccessibilityfranmu,
					firstcampaignTypefranmu, firsttemplateNamefranmu, firstemailSubjectfranmu, firstplainTextEditorfranmu);
			
			//Create another campaign at Campaign Center > Campaigns
			fc.utobj().printTestStep("Create another campaign at Campaign Center > Campaigns");
			String secondcampaignNamefranmu = fc.utobj().generateTestData(dataSet.get("CorpcampaignName"));
			String privateaccessibilityfranmu = dataSet.get("Franaccessibilityprivate");
			String secondcampaignTypefranmu = dataSet.get("CorpcampaignType");
			String secondtemplateNamefranmu = fc.utobj().generateTestData(dataSet.get("CorptemplateName"));
			String secondemailSubjectfranmu = fc.utobj().generateTestData(dataSet.get("CorpemailSubject"));
			String secondplainTextEditorfranmu = fc.utobj().generateTestData(dataSet.get("CorpplainTextEditor"));
			campaignCenterPage.createCampaignGenericwithSqlite(driver, dataSet, secondcampaignNamefranmu, privateaccessibilityfranmu,
					secondcampaignTypefranmu, secondtemplateNamefranmu, secondemailSubjectfranmu, secondplainTextEditorfranmu);
			
			//Create Status driven campaign at At Campaign Center > Campaigns
			fc.utobj().printTestStep("Create Status driven campaign At Campaign Center > Campaigns");
			String firstcampaignNamefranmustatus = fc.utobj().generateTestData(dataSet.get("CorpcampaignName"));
			String firstaccessibilityfranmustatus = dataSet.get("FranaccessibilityPublictoallUsersofmyFranchise");
			String firstcampaignTypefranmustatus = dataSet.get("StatusCampaigntype");
			String firsttemplateNamefranmustatus = fc.utobj().generateTestData(dataSet.get("CorptemplateName"));
			String firstemailSubjectfranmustatus = fc.utobj().generateTestData(dataSet.get("CorpemailSubject"));
			String firstplainTextEditorfranmustatus = fc.utobj().generateTestData(dataSet.get("CorpplainTextEditor"));
			campaignCenterPage.createCampaignGenericwithSqlite(driver, dataSet, firstcampaignNamefranmustatus, firstaccessibilityfranmustatus,
					firstcampaignTypefranmustatus, firsttemplateNamefranmustatus, firstemailSubjectfranmustatus, firstplainTextEditorfranmustatus);
			
			//Create Status Driven campaign at Campaign Center > Campaigns
			fc.utobj().printTestStep("Create Status Driven campaign at Campaign Center > Campaigns");
			String secondcampaignNamefranmustatus = fc.utobj().generateTestData(dataSet.get("CorpcampaignName"));
			String privateaccessibilityfranmustatus = dataSet.get("Franaccessibilityprivate");
			String secondcampaignTypefranmustatus = dataSet.get("StatusCampaigntype");
			String secondtemplateNamefranmustatus = fc.utobj().generateTestData(dataSet.get("CorptemplateName"));
			String secondemailSubjectfranmustatus = fc.utobj().generateTestData(dataSet.get("CorpemailSubject"));
			String secondplainTextEditorfranmustatus = fc.utobj().generateTestData(dataSet.get("CorpplainTextEditor"));
			campaignCenterPage.createCampaignGenericwithSqlite(driver, dataSet, secondcampaignNamefranmustatus, privateaccessibilityfranmustatus,
					secondcampaignTypefranmustatus, secondtemplateNamefranmustatus, secondemailSubjectfranmustatus, secondplainTextEditorfranmustatus);
			
			fc.utobj().clickElement(driver, pobjcam.viewAllButton);
			
			//Verify Public to all users of my Franchise Campaign created by Franchise user who has privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Public to all users of my Franchise Campaign created by Franchise MU user(V1) who has privilege to see private campaign by itself");
			campaignCenterPage.searchCampaignByFilter(driver, firstcampaignNamefranmu);

			boolean isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchisemu = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firstcampaignNamefranmu + "')]");
			if (isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchisemu == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my Franchise Campaign created by Franchise MU user(V1) who has privilege to see private campaign by itself");
			}
			
			//Verify Private Campaign created by Franchise user who has privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Private Campaign created by Franchise MU user(V1) who has privilege to see private campaign by itself");
			campaignCenterPage.searchCampaignByFilter(driver, secondcampaignNamefranmu);
			
			boolean isCampaignNamePresentFranpaccessibilityprivatemu = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondcampaignNamefranmu + "')]");
			if (isCampaignNamePresentFranpaccessibilityprivatemu == false) {
				fc.utobj().throwsException("was not able to verify Private Campaign created by Franchise MU user(V1) who has privilege to see private campaign by itself");
			}
			
			
			//Verify Public to all users of my Franchise (Status Driven)Campaign created by Franchise user who has privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Public to all users of my Franchise (Status Driven)Campaign created by Franchise MU user(V1) who has privilege to see private campaign by itself");
			campaignCenterPage.searchCampaignByFilter(driver, firstcampaignNamefranmustatus);

			boolean isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchisemustatus = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firstcampaignNamefranmustatus + "')]");
			if (isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchisemustatus == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my Franchise (Status Driven)Campaign created by Franchise MU user(V1) who has privilege to see private campaign by itself");
			}
			
			//Verify Private Campaign created by Franchise user who has privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Private (Status Driven)Campaign created by Franchise MU user(V1) who has privilege to see private campaign by itself");
			campaignCenterPage.searchCampaignByFilter(driver, secondcampaignNamefranmustatus);
			
			boolean isCampaignNamePresentFranpaccessibilityprivatemustatus = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondcampaignNamefranmustatus + "')]");
			if (isCampaignNamePresentFranpaccessibilityprivatemustatus == false) {
				fc.utobj().throwsException("was not able to verify Private (Status Driven)Campaign created by Franchise MU user(V1) who has privilege to see private campaign by itself");
			}
			
			fc.utobj().printTestStep("Navigate to CRM>Campaign Center>email template");
			emailtemplate.clickonemailtamplate(driver);
			fc.utobj().clickElement(driver, crmcenter.locationtemplate);
			//Verify Public to all users template created by Corporate user who has privilege to see private template by itself
			fc.utobj().printTestStep("Verify Public to all users of my franchise template created by franchise MU user(V1) who has privilege to see private template by itself");
			campaignCenterPage.searchTemplateByFilter(driver, firsttemplateNamefranmu);

			boolean isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchisemu = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firsttemplateNamefranmu + "')]");
			if (isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchisemu == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my franchise template created by franchise MU user(V1) who has privilege to see private template by itself");
			}
			
			//Verify Private template created by franchise user who has privilege to see private template by itself
			fc.utobj().printTestStep("Verify Private template created by franchise MU user(V1) who has privilege to see private template by itself");
			campaignCenterPage.searchTemplateByFilter(driver, secondtemplateNamefranmu);
			
			boolean isTemplateNamePresentfranaccessibilityprivatemu = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondtemplateNamefranmu + "')]");
			if (isTemplateNamePresentfranaccessibilityprivatemu == false) {
				fc.utobj().throwsException("was not able to verify private template created by franchise MU user(V1) who has privilege to see private template by itself");
			}
			
			//Verify Public to all users template created by Corporate user who has privilege to see private template by itself
			fc.utobj().printTestStep("Verify Public to all users of my franchise (Status Driven)template created by franchise MU user(V1) who has privilege to see private template by itself");
			campaignCenterPage.searchTemplateByFilter(driver, firsttemplateNamefranmustatus);

			boolean isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchisemustatus = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firsttemplateNamefranmustatus + "')]");
			if (isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchisemustatus == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my franchise (Status Driven)template created by franchise MU user(V1) who has privilege to see private template by itself");
			}
			
			//Verify Private template created by franchise user who has privilege to see private template by itself
			fc.utobj().printTestStep("Verify Private (Status Driven)template created by franchise MU user(V1) who has privilege to see private template by itself");
			campaignCenterPage.searchTemplateByFilter(driver, secondtemplateNamefranmustatus);
			
			boolean isTemplateNamePresentfranaccessibilityprivatemustatus = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondtemplateNamefranmustatus + "')]");
			if (isTemplateNamePresentfranaccessibilityprivatemustatus == false) {
				fc.utobj().throwsException("was not able to verify private (Status Driven)template created by franchise MU user(V1) who has privilege to see private template by itself");
			}
			
			
			//Login with Franchise Employee user(V2) who has role to see private campaign
			fc.utobj().printTestStep("Login with Franchise Employee user(V2) who has role to see private campaign");
			fc.loginpage().loginWithParameter(driver, userNameFEmu, passwordFEmu);
			fc.crm().crm_common().openCRMCampaignCenterPage(fcHomePageTest, driver);
			
			//Create Campaign At Campaign Center > Campaigns
			fc.utobj().printTestStep("Create Campaign At Campaign Center > Campaigns");
			String firstcampaignNamefranmue2 = fc.utobj().generateTestData(dataSet.get("CorpcampaignName"));
			String firstaccessibilityfranmue2 = dataSet.get("FranaccessibilityPublictoallUsersofmyFranchise");
			String firstcampaignTypefranmue2 = dataSet.get("CorpcampaignType");
			String firsttemplateNamefranmue2 = fc.utobj().generateTestData(dataSet.get("CorptemplateName"));
			String firstemailSubjectfranmue2 = fc.utobj().generateTestData(dataSet.get("CorpemailSubject"));
			String firstplainTextEditorfranmue2 = fc.utobj().generateTestData(dataSet.get("CorpplainTextEditor"));
			campaignCenterPage.createCampaignGenericwithSqlite(driver, dataSet, firstcampaignNamefranmue2, firstaccessibilityfranmue2,
					firstcampaignTypefranmue2, firsttemplateNamefranmue2, firstemailSubjectfranmue2, firstplainTextEditorfranmue2);
			
			//Create another campaign at Campaign Center > Campaigns
			fc.utobj().printTestStep("Create another campaign at Campaign Center > Campaigns");
			String secondcampaignNamefranmue2 = fc.utobj().generateTestData(dataSet.get("CorpcampaignName"));
			String privateaccessibilityfranmue2 = dataSet.get("Franaccessibilityprivate");
			String secondcampaignTypefranmue2 = dataSet.get("CorpcampaignType");
			String secondtemplateNamefranmue2 = fc.utobj().generateTestData(dataSet.get("CorptemplateName"));
			String secondemailSubjectfranmue2 = fc.utobj().generateTestData(dataSet.get("CorpemailSubject"));
			String secondplainTextEditorfranmue2 = fc.utobj().generateTestData(dataSet.get("CorpplainTextEditor"));
			campaignCenterPage.createCampaignGenericwithSqlite(driver, dataSet, secondcampaignNamefranmue2, privateaccessibilityfranmue2,
					secondcampaignTypefranmue2, secondtemplateNamefranmue2, secondemailSubjectfranmue2, secondplainTextEditorfranmue2);
			
			
			//Create Status Driven campaign At Campaign Center > Campaigns
			fc.utobj().printTestStep("Create Status Driven campaign At Campaign Center > Campaigns");
			String firstcampaignNamefranmue2status = fc.utobj().generateTestData(dataSet.get("CorpcampaignName"));
			String firstaccessibilityfranmue2status = dataSet.get("FranaccessibilityPublictoallUsersofmyFranchise");
			String firstcampaignTypefranmue2status = dataSet.get("StatusCampaigntype");
			String firsttemplateNamefranmue2status = fc.utobj().generateTestData(dataSet.get("CorptemplateName"));
			String firstemailSubjectfranmue2status = fc.utobj().generateTestData(dataSet.get("CorpemailSubject"));
			String firstplainTextEditorfranmue2status = fc.utobj().generateTestData(dataSet.get("CorpplainTextEditor"));
			campaignCenterPage.createCampaignGenericwithSqlite(driver, dataSet, firstcampaignNamefranmue2status, firstaccessibilityfranmue2status,
					firstcampaignTypefranmue2status, firsttemplateNamefranmue2status, firstemailSubjectfranmue2status, firstplainTextEditorfranmue2status);
			
			//Create another campaign at Campaign Center > Campaigns
			fc.utobj().printTestStep("Create another campaign at Campaign Center > Campaigns");
			String secondcampaignNamefranmue2status = fc.utobj().generateTestData(dataSet.get("CorpcampaignName"));
			String privateaccessibilityfranmue2status = dataSet.get("Franaccessibilityprivate");
			String secondcampaignTypefranmue2status = dataSet.get("StatusCampaigntype");
			String secondtemplateNamefranmue2status = fc.utobj().generateTestData(dataSet.get("CorptemplateName"));
			String secondemailSubjectfranmue2status = fc.utobj().generateTestData(dataSet.get("CorpemailSubject"));
			String secondplainTextEditorfranmue2status = fc.utobj().generateTestData(dataSet.get("CorpplainTextEditor"));
			campaignCenterPage.createCampaignGenericwithSqlite(driver, dataSet, secondcampaignNamefranmue2status, privateaccessibilityfranmue2status,
					secondcampaignTypefranmue2status, secondtemplateNamefranmue2status, secondemailSubjectfranmue2status, secondplainTextEditorfranmue2status);
			
			
			fc.utobj().clickElement(driver, pobjcam.viewAllButton);
			
			//Verify Public to all users of my Franchise Campaign created by Franchise Employee user(V2) who has privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Public to all users of my Franchise Campaign created by Franchise Employee user(V2) who has privilege to see private campaign by itself");
			campaignCenterPage.searchCampaignByFilter(driver, firstcampaignNamefranmue2);

			boolean isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchisemue2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firstcampaignNamefranmue2 + "')]");
			if (isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchisemue2 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my Franchise Campaign created by Franchise Employee user(V2) who has privilege to see private campaign by itself");
			}
			
			//Verify Private Campaign created by Franchise user who has privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Private Campaign created by Franchise Employee user(V2) who has privilege to see private campaign by itself");
			campaignCenterPage.searchCampaignByFilter(driver, secondcampaignNamefranmue2);
			
			boolean isCampaignNamePresentFranpaccessibilityprivatemue2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondcampaignNamefranmue2 + "')]");
			if (isCampaignNamePresentFranpaccessibilityprivatemue2 == false) {
				fc.utobj().throwsException("was not able to verify Private Campaign created by Franchise Employee user(V2) who has privilege to see private campaign by itself");
			}
			
			
			//Verify Public to all users of my Franchise Campaign created by Franchise Employee user(V2) who has privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Public to all users of my Franchise (Status Driven)Campaign created by Franchise Employee user(V2) who has privilege to see private campaign by itself");
			campaignCenterPage.searchCampaignByFilter(driver, firstcampaignNamefranmue2status);

			boolean isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchisemue2status = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firstcampaignNamefranmue2status + "')]");
			if (isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchisemue2status == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my Franchise (Status Driven)Campaign created by Franchise Employee user(V2) who has privilege to see private campaign by itself");
			}
			
			//Verify Private Campaign created by Franchise user who has privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Private (Status Driven)Campaign created by Franchise Employee user(V2) who has privilege to see private campaign by itself");
			campaignCenterPage.searchCampaignByFilter(driver, secondcampaignNamefranmue2status);
			
			boolean isCampaignNamePresentFranpaccessibilityprivatemue2status = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondcampaignNamefranmue2status + "')]");
			if (isCampaignNamePresentFranpaccessibilityprivatemue2status == false) {
				fc.utobj().throwsException("was not able to verify Private (Status Driven)Campaign created by Franchise Employee user(V2) who has privilege to see private campaign by itself");
			}
			
			fc.utobj().printTestStep("Navigate to CRM>Campaign Center>email template");
			emailtemplate.clickonemailtamplate(driver);
			fc.utobj().clickElement(driver, crmcenter.locationtemplate);
			//Verify Public to all users template created by Franchise user who has privilege to see private template by itself
			fc.utobj().printTestStep("Verify Public to all users of my franchise template created by franchise Employee user(V2) who has privilege to see private template by itself");
			campaignCenterPage.searchTemplateByFilter(driver, firsttemplateNamefranmue2);

			boolean isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchisemue2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firsttemplateNamefranmue2 + "')]");
			if (isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchisemue2 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my franchise template created by franchise Employee user(V2) who has privilege to see private template by itself");
			}
			
			//Verify Private template created by franchise user who has privilege to see private template by itself
			fc.utobj().printTestStep("Verify Private template created by franchise Employee user(V2) who has privilege to see private template by itself");
			campaignCenterPage.searchTemplateByFilter(driver, secondtemplateNamefranmue2);
			
			boolean isTemplateNamePresentfranaccessibilityprivatemue2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondtemplateNamefranmue2 + "')]");
			if (isTemplateNamePresentfranaccessibilityprivatemue2 == false) {
				fc.utobj().throwsException("was not able to verify private template created by franchise Employee user(V2) who has privilege to see private template by itself");
			}
			
			//Verify Public to all users template created by Franchise user who has privilege to see private template by itself
			fc.utobj().printTestStep("Verify Public to all users of my franchise (Status Driven)template created by franchise Employee user(V2) who has privilege to see private template by itself");
			campaignCenterPage.searchTemplateByFilter(driver, firsttemplateNamefranmue2status);

			boolean isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchisemue2status = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firsttemplateNamefranmue2status + "')]");
			if (isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchisemue2status == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my franchise (Status Driven)template created by franchise Employee user(V2) who has privilege to see private template by itself");
			}
			
			//Verify Private template created by franchise user who has privilege to see private template by itself
			fc.utobj().printTestStep("Verify Private (Status Driven)template created by franchise Employee user(V2) who has privilege to see private template by itself");
			campaignCenterPage.searchTemplateByFilter(driver, secondtemplateNamefranmue2status);
			
			boolean isTemplateNamePresentfranaccessibilityprivatemue2status = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondtemplateNamefranmue2status + "')]");
			if (isTemplateNamePresentfranaccessibilityprivatemue2status == false) {
				fc.utobj().throwsException("was not able to verify private (Status Driven)template created by franchise Employee user(V2) who has privilege to see private template by itself");
			}
			
		
			CRMCampaignCenterPage pobj1 = new CRMCampaignCenterPage(driver);
			
			//Verification of ALL campaigns with different different users	
			//Login with Corporate user(C1) who has role to see private campaign
			fc.utobj().printTestStep("Login with Corporate user(C1) who has role to see private campaign");
			fc.loginpage().loginWithParameter(driver, CorpUserName, CorpUserPassword);
			fc.crm().crm_common().openCRMCampaignCenterPage(fcHomePageTest, driver);
			fc.utobj().printTestStep("Click on the menu option and select campaign");
			/*CRMCampaignCenterPage pobj1 = new CRMCampaignCenterPage(driver);*/
			fc.utobj().clickElement(driver, pobj1.menuOptions);
			fc.utobj().clickElement(driver, pobj1.campaignLinks);
			
			//Verifying Campaigns and templates of corporate user (C2) after logging with Corporate user (C1)
			fc.utobj().printTestStep("Verifying Campaigns and templates of corporate user (C2) after logging with Corporate user (C1)");
			fc.utobj().printTestStep("Verify Public to all users Campaign created by Corporate user(C2) who does not have privilege to see private campaign by Corporate user(C1)");
			campaignCenterPage.searchCampaignByFilter(driver, firstcampaignNamecorp);

			boolean isCampaignNamePresentCorpaccessibilitypublictoalluserscorpC1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firstcampaignNamecorp + "')]");
			if (isCampaignNamePresentCorpaccessibilitypublictoalluserscorpC1 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users Campaign created by Corporate user who does not have privilege to see private campaign by another corporate user(C1)");
			}
			//Verify Public to all corporate users Campaign created by Corporate user who has privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Public to all corporate users Campaign created by Corporate user who does not have privilege to see private campaign by another corporate user(C1)");
			campaignCenterPage.searchCampaignByFilter(driver, secondcampaignNamecorp);
			
			boolean isCampaignNamePresentCorpaccessibilitypublictoallcorporateuserscorpC1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondcampaignNamecorp + "')]");
			if (isCampaignNamePresentCorpaccessibilitypublictoallcorporateuserscorpC1 == false) {
				fc.utobj().throwsException("was not able to verify Public to all corporate users Campaign created by Corporate user who does not privilege to see private campaign by another corporate user(C1)");
			}
			
			//Verify Private Campaign created by Corporate user who has privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Private Campaign created by Corporate user who does not have privilege to see private campaign by another corporate user(C1)");
			campaignCenterPage.searchCampaignByFilter(driver, thirdcampaignNamecorp);
			
			boolean isCampaignNamePresentCorpaccessibilityprivatecorpC1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + thirdcampaignNamecorp + "')]");
			if (isCampaignNamePresentCorpaccessibilityprivatecorpC1 == false) {
				fc.utobj().throwsException("was not able to Private Campaign created by Corporate user who does not have privilege to see private campaign by another corporate user(C1)");
			}
		
			//Verify Public to all users (Status Driven)Campaign created by Corporate user who does not have privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Public to all users(Status Driven) Campaign created by Corporate user who does not have privilege to see private campaign by another corporate user(C1)");
			campaignCenterPage.searchCampaignByFilter(driver, firstcampaignNamecorpstatus);

			boolean isCampaignNamePresentCorpaccessibilitypublictoalluserscorpstatusC1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firstcampaignNamecorpstatus + "')]");
			if (isCampaignNamePresentCorpaccessibilitypublictoalluserscorpstatusC1 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users(Status Driven) Campaign created by Corporate user who does not have privilege to see private campaign by another corporate user(C1)");
			}
			//Verify Public to all corporate users Campaign created by Corporate user who has privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Public to all corporate users(Status Driven) Campaign created by Corporate user who does not have privilege to see private campaign by another corporate user(C1)");
			campaignCenterPage.searchCampaignByFilter(driver, secondcampaignNamecorpstatus);
			
			boolean isCampaignNamePresentCorpaccessibilitypublictoallcorporateuserscorpstatusC1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondcampaignNamecorpstatus + "')]");
			if (isCampaignNamePresentCorpaccessibilitypublictoallcorporateuserscorpstatusC1 == false) {
				fc.utobj().throwsException("was not able to verify Public to all corporate users(Status Driven) Campaign created by Corporate user who does not privilege to see private campaign by another corporate user(C1)");
			}
			
			//Verify Private Campaign created by Corporate user who has privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Private Campaign created by Corporate user(Status Driven) who does not have privilege to see private campaign by another corporate user(C1)");
			campaignCenterPage.searchCampaignByFilter(driver, thirdcampaignNamecorpstatus);
			
			boolean isCampaignNamePresentCorpaccessibilityprivatecorpstatusC1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + thirdcampaignNamecorpstatus + "')]");
			if (isCampaignNamePresentCorpaccessibilityprivatecorpstatusC1 == false) {
				fc.utobj().throwsException("was not able to Private Campaign created by Corporate user(Status Driven) who does not have privilege to see private campaign by another corporate user(C1)");
			}
				
			fc.utobj().printTestStep("Navigate to CRM>Campaign Center>email template");
			emailtemplate.clickonemailtamplate(driver);
			
			//Verify Public to all users template created by Corporate user who does not have privilege to see private template by itself
			fc.utobj().printTestStep("Verify Public to all users template created by Corporate user who does not have privilege to see private template by another corporate user(C1)");
			campaignCenterPage.searchTemplateByFilter(driver, firsttemplateNamecorp);

			boolean isTemplateNamePresentCorpaccessibilitypublictoalluserscorpC1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firsttemplateNamecorp + "')]");
			if (isTemplateNamePresentCorpaccessibilitypublictoalluserscorpC1 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users template created by Corporate user who has privilege to see private template by another corporate user(C1)");
			}
			//Verify Public to all corporate users template created by Corporate user who has privilege to see private template by itself
			fc.utobj().printTestStep("Verify Public to all corporate users template created by Corporate user who does not have privilege to see private template by another corporate user(C1)");
			campaignCenterPage.searchTemplateByFilter(driver, secondtemplateNamecorp);
			
			boolean isTemplateNamePresentCorpaccessibilitypublictoallcorporateuserscorpC1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondtemplateNamecorp + "')]");
			if (isTemplateNamePresentCorpaccessibilitypublictoallcorporateuserscorpC1 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users template created by Corporate user who has privilege to see private template by another corporate user(C1)");
			}
			
			//Verify Private template created by Corporate user who has privilege to see private template by itself
			fc.utobj().printTestStep("Verify Privare template created by Corporate user who does not have privilege to see private template by another corporate user(C1)");
			campaignCenterPage.searchTemplateByFilter(driver, thirdtemplateNamecorp);
			
			boolean isTemplateNamePresentCorpaccessibilityprivatecorpC1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + thirdtemplateNamecorp + "')]");
			if (isTemplateNamePresentCorpaccessibilityprivatecorpC1 == false) {
				fc.utobj().throwsException("was not able to verify private template created by Corporate user who has privilege to see private template by another corporate user(C1)");
			}
			
			//Verify Public to all users (Status driven)template created by Corporate user who does not have privilege to see private template by itself
			fc.utobj().printTestStep("Verify Public to all users (Status driven)template created by Corporate user who does not have privilege to see private template by another corporate user(C1)");
			campaignCenterPage.searchTemplateByFilter(driver, firsttemplateNamecorpstatus);

			boolean isTemplateNamePresentCorpaccessibilitypublictoalluserscorpstatusC1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firsttemplateNamecorpstatus + "')]");
			if (isTemplateNamePresentCorpaccessibilitypublictoalluserscorpstatusC1 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users (Status driven)template created by Corporate user who has privilege to see private template by another corporate user(C1)");
			}
			//Verify Public to all corporate users template created by Corporate user who has privilege to see private template by itself
			fc.utobj().printTestStep("Verify Public to all corporate users (Status driven)template created by Corporate user who does not have privilege to see private template by another corporate user(C1)");
			campaignCenterPage.searchTemplateByFilter(driver, secondtemplateNamecorpstatus);
			
			boolean isTemplateNamePresentCorpaccessibilitypublictoallcorporateuserscorpstatusC1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondtemplateNamecorpstatus + "')]");
			if (isTemplateNamePresentCorpaccessibilitypublictoallcorporateuserscorpstatusC1 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users (Status driven)template created by Corporate user who has privilege to see private template by another corporate user(C1)");
			}
			
			//Verify Private template created by Corporate user who has privilege to see private template by itself
			fc.utobj().printTestStep("Verify Privare (Status driven)template created by Corporate user who does not have privilege to see private template by another corporate user(C1)");
			campaignCenterPage.searchTemplateByFilter(driver, thirdtemplateNamecorpstatus);
			
			boolean isTemplateNamePresentCorpaccessibilityprivatecorpstatusC1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + thirdtemplateNamecorpstatus + "')]");
			if (isTemplateNamePresentCorpaccessibilityprivatecorpstatusC1 == false) {
				fc.utobj().throwsException("was not able to verify private (Status driven)template created by Corporate user who has privilege to see private template by another corporate user(C1)");
			}
		
			fc.utobj().printTestStep("Navigate to CRM>Campaign Center>campaign template");
			emailtemplate.clickoncampaigntemplate(driver);
			//Verify Public to all users of my region Campaign created by Regional user who has privilege to see private campaign by itself(R1)
			//Verifying Campaigns and templates of Regional user (R1) after logging with Corporate user (C1)
			fc.utobj().printTestStep("Verifying Campaigns and templates of Regional user (R1) after logging with Corporate user (C1)");
			fc.utobj().printTestStep("Verify Public to all users of my region Campaign created by Regional user who has privilege to see private campaign by another corporate user(C1)");
			campaignCenterPage.searchCampaignByFilter(driver, firstcampaignNamereg);

			boolean isCampaignNamePresentRegpaccessibilitypublictoallusersofmyregionC1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firstcampaignNamereg + "')]");
			if (isCampaignNamePresentRegpaccessibilitypublictoallusersofmyregionC1 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my region Campaign created by Regional user who has privilege to see private campaign by another corporate user(C1)");
			}
			
			//Verify Private Campaign created by Regional user who has privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Private Campaign created by Regional user who has privilege to see private campaign by itself");
			campaignCenterPage.searchCampaignByFilter(driver, secondcampaignNamereg);
			
			boolean isCampaignNamePresentRegpaccessibilityprivateC1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondcampaignNamereg + "')]");
			if (isCampaignNamePresentRegpaccessibilityprivateC1 == false) {
				fc.utobj().throwsException("was not able to verify Private Campaign created by Regional user who has privilege to see private campaign by another corporate user(C1)");
			}
			
			//Verify Public to all users of my region (Status Driven)Campaign created by Regional user who has privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Public to all users of my region (Status Driven)Campaign created by Regional user who has privilege to see private campaign by another corporate user(C1)");
			campaignCenterPage.searchCampaignByFilter(driver, firstcampaignNameregstatus);

			boolean isCampaignNamePresentRegpaccessibilitypublictoallusersofmyregionstatusC1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firstcampaignNameregstatus + "')]");
			if (isCampaignNamePresentRegpaccessibilitypublictoallusersofmyregionstatusC1 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my region (Status Driven)Campaign created by Regional user who has privilege to see private campaign by another corporate user(C1)");
			}
			
			//Verify Private Campaign created by Regional user who has privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Private (Status Driven)Campaign created by Regional user who has privilege to see private campaign by another corporate user(C1)");
			campaignCenterPage.searchCampaignByFilter(driver, secondcampaignNameregstatus);
			
			boolean isCampaignNamePresentRegpaccessibilityprivatestatusC1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondcampaignNameregstatus + "')]");
			if (isCampaignNamePresentRegpaccessibilityprivatestatusC1 == false) {
				fc.utobj().throwsException("was not able to verify Private (Status Driven)Campaign created by Regional user who has privilege to see private campaign by another corporate user(C1)");
			}
			
			fc.utobj().printTestStep("Navigate to CRM>Campaign Center>email template");
			emailtemplate.clickonemailtamplate(driver);
			
			//Verify Public to all users template created by Corporate user who has privilege to see private template by itself
			fc.utobj().printTestStep("Verify Public to all users of my region template created by Regional user who has privilege to see private template by another corporate user(C1)");
			campaignCenterPage.searchTemplateByFilter(driver, firsttemplateNamereg);

			boolean isTemplateNamePresentRegaccessibilitypublictoallusersofmyregionC1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firsttemplateNamereg + "')]");
			if (isTemplateNamePresentRegaccessibilitypublictoallusersofmyregionC1 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users template created by Regional user who has privilege to see private template by another corporate user(C1)");
			}
			
			//Verify Private template created by Regional user who has privilege to see private template by itself
			fc.utobj().printTestStep("Verify Private template created by Regional user who has privilege to see private template by another corporate user(C1)");
			campaignCenterPage.searchTemplateByFilter(driver, secondtemplateNamereg);
			
			boolean isTemplateNamePresentRegaccessibilityprivateC1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondtemplateNamereg + "')]");
			if (isTemplateNamePresentRegaccessibilityprivateC1 == false) {
				fc.utobj().throwsException("was not able to verify private template created by Regional user who has privilege to see private template by another corporate user(C1)");
			}
			
			//Verify Public to all users(Status Driven) template created by Regional user who has privilege to see private template by itself
			fc.utobj().printTestStep("Verify Public to all users of my region (Status Driven)template created by Regional user who has privilege to see private template by another corporate user(C1)");
			campaignCenterPage.searchTemplateByFilter(driver, firsttemplateNameregstatus);

			boolean isTemplateNamePresentRegaccessibilitypublictoallusersofmyregionstatusC1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firsttemplateNameregstatus + "')]");
			if (isTemplateNamePresentRegaccessibilitypublictoallusersofmyregionstatusC1 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my region (Status Driven)template created by Regional user who has privilege to see private template by another corporate user(C1)");
			}
			
			//Verify Private template created by Regional user who has privilege to see private template by itself
			fc.utobj().printTestStep("Verify Private (Status Driven)template created by Regional user who has privilege to see private template by another corporate user(C1)");
			campaignCenterPage.searchTemplateByFilter(driver, secondtemplateNameregstatus);
			
			boolean isTemplateNamePresentRegaccessibilityprivatestatusC1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondtemplateNameregstatus + "')]");
			if (isTemplateNamePresentRegaccessibilityprivatestatusC1 == false) {
				fc.utobj().throwsException("was not able to verify private (Status Driven)template created by Regional user who has privilege to see private template by another corporate user(C1)");
			}
			fc.utobj().printTestStep("Navigate to CRM>Campaign Center>campaign template");
			emailtemplate.clickoncampaigntemplate(driver);
			
			//Verify Public to all users Campaign created by Corporate user who has privilege to see private campaign by itself
			fc.utobj().printTestStep("Verifying Campaigns and templates of Corporate user (C1) by itself");
			fc.utobj().printTestStep("Verify Public to all users Campaign created by Corporate user who has privilege to see private campaign by itself");
			campaignCenterPage.searchCampaignByFilter(driver, firstcampaignName);

			boolean isCampaignNamePresentCorpaccessibilitypublictoallusersC1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firstcampaignName + "')]");
			if (isCampaignNamePresentCorpaccessibilitypublictoallusersC1 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users Campaign created by Corporate user who has privilege to see private campaign by itself");
			}
			//Verify Public to all corporate users Campaign created by Corporate user who has privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Public to all corporate users Campaign created by Corporate user who has privilege to see private campaign");
			campaignCenterPage.searchCampaignByFilter(driver, secondcampaignName);
			
			boolean isCampaignNamePresentCorpaccessibilitypublictoallcorporateusersC1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondcampaignName + "')]");
			if (isCampaignNamePresentCorpaccessibilitypublictoallcorporateusersC1 == false) {
				fc.utobj().throwsException("was not able to verify Public to all corporate users Campaign created by Corporate user who has privilege to see private campaign by itself");
			}
			
			//Verify Private Campaign created by Corporate user who has privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Private Campaign created by Corporate user who has privilege to see private campaign by itself");
			campaignCenterPage.searchCampaignByFilter(driver, thirdcampaignName);
			
			boolean isCampaignNamePresentCorpaccessibilityprivateC1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + thirdcampaignName + "')]");
			if (isCampaignNamePresentCorpaccessibilityprivateC1 == false) {
				fc.utobj().throwsException("was not able to Private Campaign created by Corporate user who has privilege to see private campaign by itself");
			}
			
			//Verify Public to all users(Status Driven) Campaign created by Corporate user who has privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Public to all users(Status Driven) Campaign created by Corporate user who has privilege to see private campaign by itself");
			campaignCenterPage.searchCampaignByFilter(driver, firstcampaignNamestatus);

			boolean isCampaignNamePresentCorpaccessibilitypublictoallusersstatusC1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firstcampaignNamestatus + "')]");
			if (isCampaignNamePresentCorpaccessibilitypublictoallusersstatusC1 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users(Status Driven) Campaign created by Corporate user who has privilege to see private campaign by itself");
			}
			//Verify Public to all corporate users(Status Driven) Campaign created by Corporate user who has privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Public to all corporate users(Status Driven) Campaign created by Corporate user who has privilege to see private campaign");
			campaignCenterPage.searchCampaignByFilter(driver, secondcampaignNamestatus);
			
			boolean isCampaignNamePresentCorpaccessibilitypublictoallcorporateusersstatusC1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondcampaignNamestatus + "')]");
			if (isCampaignNamePresentCorpaccessibilitypublictoallcorporateusersstatusC1 == false) {
				fc.utobj().throwsException("was not able to verify Public to all corporate users(Status Driven) Campaign created by Corporate user who has privilege to see private campaign by itself");
			}
			
			//Verify Private Campaign created by Corporate user(Status Driven) who has privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Private Campaign(Status Driven) created by Corporate user who has privilege to see private campaign by itself");
			campaignCenterPage.searchCampaignByFilter(driver, thirdcampaignNamestatus);
			
			boolean isCampaignNamePresentCorpaccessibilityprivatestatusC1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + thirdcampaignNamestatus + "')]");
			if (isCampaignNamePresentCorpaccessibilityprivatestatusC1 == false) {
				fc.utobj().throwsException("was not able to Private Campaign(Status Driven) created by Corporate user who has privilege to see private campaign by itself");
			}
		
			fc.utobj().printTestStep("Navigate to CRM>Campaign Center>email template");
			emailtemplate.clickonemailtamplate(driver);
			
			//Verify Public to all users template created by Corporate user who has privilege to see private template by itself
			fc.utobj().printTestStep("Verify Public to all users template created by Corporate user who has privilege to see private template by itself");
			campaignCenterPage.searchTemplateByFilter(driver, firsttemplateName);

			boolean isTemplateNamePresentCorpaccessibilitypublictoallusersC1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firsttemplateName + "')]");
			if (isTemplateNamePresentCorpaccessibilitypublictoallusersC1 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users template created by Corporate user who has privilege to see private template by itself");
			}
			//Verify Public to all corporate users template created by Corporate user who has privilege to see private template by itself
			fc.utobj().printTestStep("Verify Public to all corporate users template created by Corporate user who has privilege to see private template by itself");
			campaignCenterPage.searchTemplateByFilter(driver, secondtemplateName);
			
			boolean isTemplateNamePresentCorpaccessibilitypublictoallcorporateusersC1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondtemplateName + "')]");
			if (isTemplateNamePresentCorpaccessibilitypublictoallcorporateusersC1 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users template created by Corporate user who has privilege to see private template by itself");
			}
			
			//Verify Private template created by Corporate user who has privilege to see private template by itself
			fc.utobj().printTestStep("Verify Privare template created by Corporate user who has privilege to see private template by itself");
			campaignCenterPage.searchTemplateByFilter(driver, thirdtemplateName);
			
			boolean isTemplateNamePresentCorpaccessibilityprivateC1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + thirdtemplateName + "')]");
			if (isTemplateNamePresentCorpaccessibilityprivateC1 == false) {
				fc.utobj().throwsException("was not able to verify private template created by Corporate user who has privilege to see private template by itself");
			}
				
			//Verify Public to all users template(Status Driven) created by Corporate user who has privilege to see private template by itself
			fc.utobj().printTestStep("Verify Public to all users template(Status Driven) created by Corporate user who has privilege to see private template by itself");
			campaignCenterPage.searchTemplateByFilter(driver, firsttemplateNamestatus);

			boolean isTemplateNamePresentCorpaccessibilitypublictoallusersstatusC1= fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firsttemplateNamestatus + "')]");
			if (isTemplateNamePresentCorpaccessibilitypublictoallusersstatusC1 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users template(Status Driven) created by Corporate user who has privilege to see private template by itself");
			}
			//Verify Public to all corporate users template(Status Driven) created by Corporate user who has privilege to see private template by itself
			fc.utobj().printTestStep("Verify Public to all corporate users template(Status Driven) created by Corporate user who has privilege to see private template by itself");
			campaignCenterPage.searchTemplateByFilter(driver, secondtemplateNamestatus);
			
			boolean isTemplateNamePresentCorpaccessibilitypublictoallcorporateusersstatusC1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondtemplateNamestatus + "')]");
			if (isTemplateNamePresentCorpaccessibilitypublictoallcorporateusersstatusC1 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users template(Status Driven) created by Corporate user who has privilege to see private template by itself");
			}
			
			//Verify Private template(Status Driven) created by Corporate user who has privilege to see private template by itself
			fc.utobj().printTestStep("Verify Privare template(Status Driven) created by Corporate user who has privilege to see private template by itself");
			campaignCenterPage.searchTemplateByFilter(driver, thirdtemplateNamestatus);
			
			boolean isTemplateNamePresentCorpaccessibilityprivatestatusC1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + thirdtemplateNamestatus + "')]");
			if (isTemplateNamePresentCorpaccessibilityprivatestatusC1 == false) {
				fc.utobj().throwsException("was not able to verify private template(Status Driven) created by Corporate user who has privilege to see private template by itself");
			}
		
			fc.utobj().printTestStep("Navigate to CRM>Campaign Center>campaign template");
			emailtemplate.clickoncampaigntemplate(driver);
			
			//Verify Public to all users Campaign created by Corporate user who has privilege to see private campaign by itself
			fc.utobj().printTestStep("Verifying Campaigns and templates of Regional user (R2) by corporate user(C1)");
			//Verify Public to all users of my region Campaign created by Regional user who does not have privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Public to all users of my region Campaign created by Regional user who does not have privilege to see private campaign by corporate user(C1)");
			campaignCenterPage.searchCampaignByFilter(driver, firstcampaignNamereg1);

			boolean isCampaignNamePresentRegpaccessibilitypublictoallusersofmyregion1C1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firstcampaignNamereg1 + "')]");
			if (isCampaignNamePresentRegpaccessibilitypublictoallusersofmyregion1C1 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my region Campaign created by Regional user who does not have privilege to see private campaign by corporate user(C1)");
			}
			
			//Verify Private Campaign created by Regional user who does not have privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Private Campaign created by Regional user who does not have privilege to see private campaign by corporate user(C1)");
			campaignCenterPage.searchCampaignByFilter(driver, secondcampaignNamereg1);
			
			boolean isCampaignNamePresentRegpaccessibilityprivate1C1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondcampaignNamereg1 + "')]");
			if (isCampaignNamePresentRegpaccessibilityprivate1C1 == false) {
				fc.utobj().throwsException("was not able to verify Private Campaign created by Regional user who does not have privilege to see private campaign by corporate user(C1)");
			}
			
			//Verify Public to all users of my region (Status Driven)Campaign created by Regional user who does not have privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Public to all users of my region (Status Driven)Campaign created by Regional user who does not have privilege to see private campaign by corporate user(C1)");
			campaignCenterPage.searchCampaignByFilter(driver, firstcampaignNamereg1status);

			boolean isCampaignNamePresentRegpaccessibilitypublictoallusersofmyregion1statusC1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firstcampaignNamereg1status + "')]");
			if (isCampaignNamePresentRegpaccessibilitypublictoallusersofmyregion1statusC1 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my region (Status Driven)Campaign created by Regional user who does not have privilege to see private campaign by corporate user(C1)");
			}
			
			//Verify Private (Status Driven)Campaign created by Regional user who does not have privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Private (Status Driven)Campaign created by Regional user who does not have privilege to see private campaign by corporate user(C1)");
			campaignCenterPage.searchCampaignByFilter(driver, secondcampaignNamereg1status);
			
			boolean isCampaignNamePresentRegpaccessibilityprivate1statusC1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondcampaignNamereg1status + "')]");
			if (isCampaignNamePresentRegpaccessibilityprivate1statusC1 == false) {
				fc.utobj().throwsException("was not able to verify Private (Status Driven)Campaign created by Regional user who does not have privilege to see private campaign by corporate user(C1)");
			}
		
			fc.utobj().printTestStep("Navigate to CRM>Campaign Center>email template");
			emailtemplate.clickonemailtamplate(driver);
			
			//Verify Public to all users template created by Corporate user who does not have privilege to see private template by itself
			fc.utobj().printTestStep("Verify Public to all users of my region template created by Regional user who does not have privilege to see private template by corporate user(C1)");
			campaignCenterPage.searchTemplateByFilter(driver, firsttemplateNamereg1);

			boolean isTemplateNamePresentRegaccessibilitypublictoallusersofmyregion1C1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firsttemplateNamereg1 + "')]");
			if (isTemplateNamePresentRegaccessibilitypublictoallusersofmyregion1C1 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my region template created by Regional user who does not have privilege to see private template by corporate user(C1)");
			}
			
			//Verify Private template created by Regional user who does not have privilege to see private template by itself
			fc.utobj().printTestStep("Verify Private template created by Regional user does not have privilege to see private template by corporate user(C1)");
			campaignCenterPage.searchTemplateByFilter(driver, secondtemplateNamereg1);
			
			boolean isTemplateNamePresentRegaccessibilityprivate1C1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondtemplateNamereg1 + "')]");
			if (isTemplateNamePresentRegaccessibilityprivate1C1 == false) {
				fc.utobj().throwsException("was not able to verify Private template created by Regional user does not have privilege to see private template by corporate user(C1)");
			}
			
			//Verify Public to all users (Status Driven)template created by Regional user who does not have privilege to see private template by itself
			fc.utobj().printTestStep("Verify Public to all users of my region (Status Driven)template created by Regional user who does not have privilege to see private template by corporate user(C1)");
			campaignCenterPage.searchTemplateByFilter(driver, firsttemplateNamereg1status);

			boolean isTemplateNamePresentRegaccessibilitypublictoallusersofmyregion1statusC1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firsttemplateNamereg1status + "')]");
			if (isTemplateNamePresentRegaccessibilitypublictoallusersofmyregion1statusC1 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my region (Status Driven)template created by Regional user who does not have privilege to see private template by corporate user(C1)");
			}
			
			//Verify Private template created by Regional user who does not have privilege to see private template by itself
			fc.utobj().printTestStep("Verify Private (Status Driven)template created by Regional user does not have privilege to see private template by corporate user(C1)");
			campaignCenterPage.searchTemplateByFilter(driver, secondtemplateNamereg1status);
			
			boolean isTemplateNamePresentRegaccessibilityprivate1statusC1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondtemplateNamereg1status + "')]");
			if (isTemplateNamePresentRegaccessibilityprivate1statusC1 == false) {
				fc.utobj().throwsException("was not able to verify Private (Status Driven)template created by Regional user does not have privilege to see private template by corporate user(C1)");
			}
			
			fc.utobj().printTestStep("Navigate to CRM>Campaign Center>campaign template");
			emailtemplate.clickoncampaigntemplate(driver);
			
			fc.utobj().clickElement(driver, crmcenter.locationCampaign);
			fc.utobj().printTestStep("Verifying Campaigns and templates of Franchise mu owner user (F0New) by corporate user(C1)");
			fc.utobj().printTestStep("Verify Public to all users of my Franchise Campaign created by Franchise user who has privilege to see private campaign by corporate user(C1)");
			campaignCenterPage.searchCampaignByFilter(driver, firstcampaignNamefran);

			boolean isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchiseC1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firstcampaignNamefran + "')]");
			if (isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchiseC1 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my Franchise Campaign created by Franchise user who has privilege to see private campaign by corporate user(C1)");
			}
			
			//Verify Private Campaign created by Franchise user who has privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Private Campaign created by Franchise user who has privilege to see private campaign by corporate user(C1)");
			campaignCenterPage.searchCampaignByFilter(driver, secondcampaignNamefran);
			
			boolean isCampaignNamePresentFranpaccessibilityprivateC1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondcampaignNamefran + "')]");
			if (isCampaignNamePresentFranpaccessibilityprivateC1 == false) {
				fc.utobj().throwsException("was not able to verify Private Campaign created by Franchise user who has privilege to see private campaign by corporate user(C1)");
			}
			
			
			//Verify Public to all users of my Franchise (Status Driven)Campaign created by Franchise user who has privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Public to all users of my Franchise (Status Driven)Campaign created by Franchise user who has privilege to see private campaign by corporate user(C1)");
			campaignCenterPage.searchCampaignByFilter(driver, firstcampaignNamefranstatus);

			boolean isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchisestatusC1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firstcampaignNamefranstatus + "')]");
			if (isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchisestatusC1 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my Franchise (Status Driven)Campaign created by Franchise user who has privilege to see private campaign by corporate user(C1)");
			}
			
			//Verify Private Campaign created by Franchise user who has privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Private (Status Driven)Campaign created by Franchise user who has privilege to see private campaign by corporate user(C1)");
			campaignCenterPage.searchCampaignByFilter(driver, secondcampaignNamefranstatus);
			
			boolean isCampaignNamePresentFranpaccessibilityprivatestatusC1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondcampaignNamefranstatus + "')]");
			if (isCampaignNamePresentFranpaccessibilityprivatestatusC1 == false) {
				fc.utobj().throwsException("was not able to verify Private (Status Driven)Campaign created by Franchise user who has privilege to see private campaign by corporate user(C1)");
			}
			
			fc.utobj().printTestStep("Navigate to CRM>Campaign Center>email template");
			emailtemplate.clickonemailtamplate(driver);
			fc.utobj().clickElement(driver, crmcenter.locationtemplate);
			//Verify Public to all users template created by Corporate user who has privilege to see private template by itself
			fc.utobj().printTestStep("Verify Public to all users of my franchise template created by franchise user who has privilege to see private template by corporate user(C1)");
			campaignCenterPage.searchTemplateByFilter(driver, firsttemplateNamefran);

			boolean isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchiseC1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firsttemplateNamefran + "')]");
			if (isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchiseC1 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my franchise template created by franchise user who has privilege to see private template by corporate user(C1)");
			}
			
			//Verify Private template created by franchise user who has privilege to see private template by itself
			fc.utobj().printTestStep("Verify Private template created by franchise user who has privilege to see private template by corporate user(C1)");
			campaignCenterPage.searchTemplateByFilter(driver, secondtemplateNamefran);
			
			boolean isTemplateNamePresentfranaccessibilityprivateC1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondtemplateNamefran + "')]");
			if (isTemplateNamePresentfranaccessibilityprivateC1 == false) {
				fc.utobj().throwsException("was not able to verify private template created by franchise user who has privilege to see private template by corporate user(C1)");
			}
			
			//Verify Public to all users (Status Driven)template created by Corporate user who has privilege to see private template by itself
			fc.utobj().printTestStep("Verify Public to all users of my franchise (Status Driven)template created by franchise user who has privilege to see private template by corporate user(C1)");
			campaignCenterPage.searchTemplateByFilter(driver, firsttemplateNamefranstatus);

			boolean isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchisestatusC1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firsttemplateNamefranstatus + "')]");
			if (isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchisestatusC1 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my franchise (Status Driven)template created by franchise user who has privilege to see private template by corporate user(C1)");
			}
			
			//Verify Private template created by franchise user who has privilege to see private template by itself
			fc.utobj().printTestStep("Verify Private (Status Driven)template created by franchise user who has privilege to see private template by corporate user(C1)");
			campaignCenterPage.searchTemplateByFilter(driver, secondtemplateNamefranstatus);
			
			boolean isTemplateNamePresentfranaccessibilityprivatestatusC1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondtemplateNamefranstatus + "')]");
			if (isTemplateNamePresentfranaccessibilityprivatestatusC1 == false) {
				fc.utobj().throwsException("was not able to verify private (Status Driven)template created by franchise user who has privilege to see private template by corporate user(C1)");
			}
			
			fc.utobj().printTestStep("Navigate to CRM>Campaign Center>campaign template");
			emailtemplate.clickoncampaigntemplate(driver);
			fc.utobj().clickElement(driver, crmcenter.locationCampaign);
			fc.utobj().printTestStep("Verifying Campaigns and templates of Franchise Mu user (V1) by corporate user(C1)");
			
			fc.utobj().printTestStep("Verify Public to all users of my Franchise Campaign created by Franchise MU user(V1) who has privilege to see private campaign by corporate user(C1)");
			campaignCenterPage.searchCampaignByFilter(driver, firstcampaignNamefranmu);

			boolean isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchisemuC1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firstcampaignNamefranmu + "')]");
			if (isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchisemuC1 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my Franchise Campaign created by Franchise MU user(V1) who has privilege to see private campaign by corporate user(C1)");
			}
			
			//Verify Private Campaign created by Franchise user who has privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Private Campaign created by Franchise MU user(V1) who has privilege to see private campaign by corporate user(C1)");
			campaignCenterPage.searchCampaignByFilter(driver, secondcampaignNamefranmu);
			
			boolean isCampaignNamePresentFranpaccessibilityprivatemuC1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondcampaignNamefranmu + "')]");
			if (isCampaignNamePresentFranpaccessibilityprivatemuC1 == false) {
				fc.utobj().throwsException("was not able to verify Private Campaign created by Franchise MU user(V1) who has privilege to see private campaign by corporate user(C1)");
			}
			
			
			//Verify Public to all users of my Franchise (Status Driven)Campaign created by Franchise user who has privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Public to all users of my Franchise (Status Driven)Campaign created by Franchise MU user(V1) who has privilege to see private campaign by corporate user(C1)");
			campaignCenterPage.searchCampaignByFilter(driver, firstcampaignNamefranmustatus);

			boolean isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchisemustatusC1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firstcampaignNamefranmustatus + "')]");
			if (isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchisemustatusC1 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my Franchise (Status Driven)Campaign created by Franchise MU user(V1) who has privilege to see private campaign by corporate user(C1)");
			}
			
			//Verify Private Campaign created by Franchise user who has privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Private (Status Driven)Campaign created by Franchise MU user(V1) who has privilege to see private campaign by corporate user(C1)");
			campaignCenterPage.searchCampaignByFilter(driver, secondcampaignNamefranmustatus);
			
			boolean isCampaignNamePresentFranpaccessibilityprivatemustatusC1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondcampaignNamefranmustatus + "')]");
			if (isCampaignNamePresentFranpaccessibilityprivatemustatusC1 == false) {
				fc.utobj().throwsException("was not able to verify Private (Status Driven)Campaign created by Franchise MU user(V1) who has privilege to see private campaign by corporate user(C1)");
			}
			
			fc.utobj().printTestStep("Navigate to CRM>Campaign Center>email template");
			emailtemplate.clickonemailtamplate(driver);
			fc.utobj().clickElement(driver, crmcenter.locationtemplate);
			//Verify Public to all users template created by Corporate user who has privilege to see private template by itself
			fc.utobj().printTestStep("Verify Public to all users of my franchise template created by franchise MU user(V1) who has privilege to see private template by corporate user(C1)");
			campaignCenterPage.searchTemplateByFilter(driver, firsttemplateNamefranmu);

			boolean isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchisemuC1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firsttemplateNamefranmu + "')]");
			if (isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchisemuC1 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my franchise template created by franchise MU user(V1) who has privilege to see private template by corporate user(C1)");
			}
			
			//Verify Private template created by franchise user who has privilege to see private template by itself
			fc.utobj().printTestStep("Verify Private template created by franchise MU user(V1) who has privilege to see private template by corporate user(C1)");
			campaignCenterPage.searchTemplateByFilter(driver, secondtemplateNamefranmu);
			
			boolean isTemplateNamePresentfranaccessibilityprivatemuC1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondtemplateNamefranmu + "')]");
			if (isTemplateNamePresentfranaccessibilityprivatemuC1 == false) {
				fc.utobj().throwsException("was not able to verify private template created by franchise MU user(V1) who has privilege to see private template by corporate user(C1)");
			}
			
			//Verify Public to all users template created by Corporate user who has privilege to see private template by itself
			fc.utobj().printTestStep("Verify Public to all users of my franchise (Status Driven)template created by franchise MU user(V1) who has privilege to see private template by corporate user(C1)");
			campaignCenterPage.searchTemplateByFilter(driver, firsttemplateNamefranmustatus);

			boolean isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchisemustatusC1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firsttemplateNamefranmustatus + "')]");
			if (isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchisemustatusC1 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my franchise (Status Driven)template created by franchise MU user(V1) who has privilege to see private template by corporate user(C1)");
			}
			
			//Verify Private template created by franchise user who has privilege to see private template by itself
			fc.utobj().printTestStep("Verify Private (Status Driven)template created by franchise MU user(V1) who has privilege to see private template by corporate user(C1)");
			campaignCenterPage.searchTemplateByFilter(driver, secondtemplateNamefranmustatus);
			
			boolean isTemplateNamePresentfranaccessibilityprivatemustatusC1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondtemplateNamefranmustatus + "')]");
			if (isTemplateNamePresentfranaccessibilityprivatemustatusC1 == false) {
				fc.utobj().throwsException("was not able to verify private (Status Driven)template created by franchise MU user(V1) who has privilege to see private template by corporate user(C1)");
			}
			
			
			
			
			fc.utobj().printTestStep("Navigate to CRM>Campaign Center>campaign template");
			emailtemplate.clickoncampaigntemplate(driver);
			fc.utobj().clickElement(driver, crmcenter.locationCampaign);
			fc.utobj().printTestStep("Verifying Campaigns and templates of Franchise Employee user (V2) by corporate user(C1)");
			fc.utobj().printTestStep("Verify Public to all users of my Franchise Campaign created by Franchise Employee user(V2) who does not have privilege to see private campaign by corporate user(C1)");
			campaignCenterPage.searchCampaignByFilter(driver, firstcampaignNamefranmue2);

			boolean isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchisemue2C1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firstcampaignNamefranmue2 + "')]");
			if (isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchisemue2C1 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my Franchise Campaign created by Franchise Employee user(V2) who does not have privilege to see private campaign by corporate user(C1)");
			}
			
			//Verify Private Campaign created by Franchise user who has privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Private Campaign created by Franchise Employee user(V2) who does not have privilege to see private campaign by corporate user(C1)");
			campaignCenterPage.searchCampaignByFilter(driver, secondcampaignNamefranmue2);
			
			boolean isCampaignNamePresentFranpaccessibilityprivatemue2C1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondcampaignNamefranmue2 + "')]");
			if (isCampaignNamePresentFranpaccessibilityprivatemue2C1 == false) {
				fc.utobj().throwsException("was not able to verify Private Campaign created by Franchise Employee user(V2) who does not have privilege to see private campaign by corporate user(C1)");
			}
			
			
			//Verify Public to all users of my Franchise Campaign created by Franchise Employee user(V2) who has privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Public to all users of my Franchise (Status Driven)Campaign created by Franchise Employee user(V2) who does not have privilege to see private campaign by corporate user(C1)");
			campaignCenterPage.searchCampaignByFilter(driver, firstcampaignNamefranmue2status);

			boolean isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchisemue2statusC1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firstcampaignNamefranmue2status + "')]");
			if (isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchisemue2statusC1 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my Franchise (Status Driven)Campaign created by Franchise Employee user(V2) who does not have privilege to see private campaign by corporate user(C1)");
			}
			
			//Verify Private Campaign created by Franchise user who has privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Private (Status Driven)Campaign created by Franchise Employee user(V2) who does not have privilege to see private campaign by corporate user(C1)");
			campaignCenterPage.searchCampaignByFilter(driver, secondcampaignNamefranmue2status);
			
			boolean isCampaignNamePresentFranpaccessibilityprivatemue2statusC1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondcampaignNamefranmue2status + "')]");
			if (isCampaignNamePresentFranpaccessibilityprivatemue2statusC1 == false) {
				fc.utobj().throwsException("was not able to verify Private (Status Driven)Campaign created by Franchise Employee user(V2) who does not have privilege to see private campaign by corporate user(C1)");
			}
			
			fc.utobj().printTestStep("Navigate to CRM>Campaign Center>email template");
			emailtemplate.clickonemailtamplate(driver);
			fc.utobj().clickElement(driver, crmcenter.locationtemplate);
			//Verify Public to all users template created by Franchise user who has privilege to see private template by itself
			fc.utobj().printTestStep("Verify Public to all users of my franchise template created by franchise Employee user(V2) who does not have privilege to see private template by corporate user(C1)");
			campaignCenterPage.searchTemplateByFilter(driver, firsttemplateNamefranmue2);

			boolean isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchisemue2C1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firsttemplateNamefranmue2 + "')]");
			if (isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchisemue2C1 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my franchise template created by franchise Employee user(V2) who has privilege to see private template by corporate user(C1)");
			}
			
			//Verify Private template created by franchise user who has privilege to see private template by itself
			fc.utobj().printTestStep("Verify Private template created by franchise Employee user(V2) who has privilege to see private template by corporate user(C1)");
			campaignCenterPage.searchTemplateByFilter(driver, secondtemplateNamefranmue2);
			
			boolean isTemplateNamePresentfranaccessibilityprivatemue2C1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondtemplateNamefranmue2 + "')]");
			if (isTemplateNamePresentfranaccessibilityprivatemue2C1 == false) {
				fc.utobj().throwsException("was not able to verify private template created by franchise Employee user(V2) who has privilege to see private template by corporate user(C1)");
			}
			
			//Verify Public to all users template created by Franchise user who has privilege to see private template by itself
			fc.utobj().printTestStep("Verify Public to all users of my franchise (Status Driven)template created by franchise Employee user(V2) who does not have privilege to see private template by corporate user(C1)");
			campaignCenterPage.searchTemplateByFilter(driver, firsttemplateNamefranmue2status);

			boolean isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchisemue2statusC1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firsttemplateNamefranmue2status + "')]");
			if (isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchisemue2statusC1 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my franchise (Status Driven)template created by franchise Employee user(V2) who has privilege to see private template by corporate user(C1)");
			}
			
			//Verify Private template created by franchise user who has privilege to see private template by itself
			fc.utobj().printTestStep("Verify Private (Status Driven)template created by franchise Employee user(V2) who has privilege to see private template by corporate user(C1)");
			campaignCenterPage.searchTemplateByFilter(driver, secondtemplateNamefranmue2status);
			
			boolean isTemplateNamePresentfranaccessibilityprivatemue2statusC1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondtemplateNamefranmue2status + "')]");
			if (isTemplateNamePresentfranaccessibilityprivatemue2statusC1 == false) {
				fc.utobj().throwsException("was not able to verify private (Status Driven)template created by franchise Employee user(V2) who has privilege to see private template by corporate user(C1)");
			}
			
			if(isCampaignNamePresentCorpaccessibilitypublictoalluserscorpC1 && isCampaignNamePresentCorpaccessibilitypublictoallcorporateuserscorpC1 && isCampaignNamePresentCorpaccessibilityprivatecorpC1 
					&& isCampaignNamePresentCorpaccessibilitypublictoalluserscorpstatusC1 && isCampaignNamePresentCorpaccessibilitypublictoallcorporateuserscorpstatusC1 && isCampaignNamePresentCorpaccessibilityprivatecorpstatusC1 
					&& isTemplateNamePresentCorpaccessibilitypublictoalluserscorpC1 && isTemplateNamePresentCorpaccessibilitypublictoallcorporateuserscorpC1 && isTemplateNamePresentCorpaccessibilityprivatecorpC1 
					&& isTemplateNamePresentCorpaccessibilitypublictoalluserscorpstatusC1 && isTemplateNamePresentCorpaccessibilitypublictoallcorporateuserscorpstatusC1 && isTemplateNamePresentCorpaccessibilityprivatecorpstatusC1 
					&& isCampaignNamePresentRegpaccessibilitypublictoallusersofmyregionC1 && isCampaignNamePresentRegpaccessibilityprivateC1 && isCampaignNamePresentRegpaccessibilitypublictoallusersofmyregionstatusC1 
					&& isCampaignNamePresentRegpaccessibilityprivatestatusC1 && isTemplateNamePresentRegaccessibilitypublictoallusersofmyregionC1 && isTemplateNamePresentRegaccessibilityprivateC1 
					&& isTemplateNamePresentRegaccessibilitypublictoallusersofmyregionstatusC1 && isTemplateNamePresentRegaccessibilityprivatestatusC1 && isCampaignNamePresentCorpaccessibilitypublictoallusersC1 
					&& isCampaignNamePresentCorpaccessibilitypublictoallcorporateusersC1 && isCampaignNamePresentCorpaccessibilityprivateC1 && isCampaignNamePresentCorpaccessibilitypublictoallusersstatusC1 
					&& isCampaignNamePresentCorpaccessibilitypublictoallcorporateusersstatusC1 && isCampaignNamePresentCorpaccessibilityprivatestatusC1 && isTemplateNamePresentCorpaccessibilitypublictoallusersC1 
					&& isTemplateNamePresentCorpaccessibilitypublictoallcorporateusersC1 && isTemplateNamePresentCorpaccessibilityprivateC1 && isTemplateNamePresentCorpaccessibilitypublictoallusersstatusC1 
					&& isTemplateNamePresentCorpaccessibilitypublictoallcorporateusersstatusC1 && isTemplateNamePresentCorpaccessibilityprivatestatusC1 && isCampaignNamePresentRegpaccessibilitypublictoallusersofmyregion1C1
					&& isCampaignNamePresentRegpaccessibilityprivate1C1 && isCampaignNamePresentRegpaccessibilitypublictoallusersofmyregion1statusC1 && isCampaignNamePresentRegpaccessibilityprivate1statusC1
					&& isTemplateNamePresentRegaccessibilitypublictoallusersofmyregion1C1 && isTemplateNamePresentRegaccessibilityprivate1C1 && isTemplateNamePresentRegaccessibilitypublictoallusersofmyregion1statusC1 
					&& isTemplateNamePresentRegaccessibilityprivate1statusC1 && isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchiseC1 && isCampaignNamePresentFranpaccessibilityprivateC1 
					&& isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchisestatusC1 && isCampaignNamePresentFranpaccessibilityprivatestatusC1 && isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchiseC1 
					&& isTemplateNamePresentfranaccessibilityprivateC1 && isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchisestatusC1 && isTemplateNamePresentfranaccessibilityprivatestatusC1  
					&& isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchisemuC1 && isCampaignNamePresentFranpaccessibilityprivatemuC1 && isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchisemustatusC1 
					&& isCampaignNamePresentFranpaccessibilityprivatemustatusC1 && isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchisemuC1 && isTemplateNamePresentfranaccessibilityprivatemuC1 
					&& isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchisemustatusC1 && isTemplateNamePresentfranaccessibilityprivatemustatusC1 && isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchisemue2C1 
					&& isCampaignNamePresentFranpaccessibilityprivatemue2C1 && isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchisemue2statusC1 && isCampaignNamePresentFranpaccessibilityprivatemue2statusC1 
					&& isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchisemue2C1 && isTemplateNamePresentfranaccessibilityprivatemue2C1 && isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchisemue2statusC1
					&& isTemplateNamePresentfranaccessibilityprivatemue2statusC1) {
				fc.utobj().printTestStep("able to verify all types of campaigns and templates created by corporate/rgional/franchise users when loging with Corporate user(C1) who has priviledge to see private campaign");
			}
			else {
				fc.utobj().throwsException("was not able to verify all types of campaigns and templates created by corporate/rgional/franchise users when loging with Corporate user(C1) who has priviledge to see private campaign");
			}
			
			
			//Login with Corporate user(C2) who does not have role to see private campaign
			fc.utobj().printTestStep("Login with Corporate user(C2) who does not have role to see private campaign");
			fc.loginpage().loginWithParameter(driver, CorpUserName1, CorpUserPassword1);
			fc.crm().crm_common().openCRMCampaignCenterPage(fcHomePageTest, driver);
			fc.utobj().printTestStep("Click on the menu option and select campaign");
			/*CRMCampaignCenterPage pobj = new CRMCampaignCenterPage(driver);*/
			fc.utobj().clickElement(driver, pobj1.menuOptions);
			fc.utobj().clickElement(driver, pobj1.campaignLinks);
			
			//Verifying Campaigns and templates of corporate user (C2) by itself
			fc.utobj().printTestStep("Verifying Campaigns and templates of corporate user (C2) by itself");
			fc.utobj().printTestStep("Verify Public to all users Campaign created by Corporate user(C2) who does not have privilege to see private campaign by itself");
			campaignCenterPage.searchCampaignByFilter(driver, firstcampaignNamecorp);

			boolean isCampaignNamePresentCorpaccessibilitypublictoalluserscorpC2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firstcampaignNamecorp + "')]");
			if (isCampaignNamePresentCorpaccessibilitypublictoalluserscorpC2 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users Campaign created by Corporate user who does not have privilege to see private campaign by itself");
			}
			//Verify Public to all corporate users Campaign created by Corporate user who has privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Public to all corporate users Campaign created by Corporate user who does not have privilege to see private campaign by itself");
			campaignCenterPage.searchCampaignByFilter(driver, secondcampaignNamecorp);
			
			boolean isCampaignNamePresentCorpaccessibilitypublictoallcorporateuserscorpC2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondcampaignNamecorp + "')]");
			if (isCampaignNamePresentCorpaccessibilitypublictoallcorporateuserscorpC2 == false) {
				fc.utobj().throwsException("was not able to verify Public to all corporate users Campaign created by Corporate user who does not privilege to see private campaign by itself");
			}
			
			//Verify Private Campaign created by Corporate user who has privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Private Campaign created by Corporate user who does not have privilege to see private campaign by itself");
			campaignCenterPage.searchCampaignByFilter(driver, thirdcampaignNamecorp);
			
			boolean isCampaignNamePresentCorpaccessibilityprivatecorpC2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + thirdcampaignNamecorp + "')]");
			if (isCampaignNamePresentCorpaccessibilityprivatecorpC2 == false) {
				fc.utobj().throwsException("was not able to Private Campaign created by Corporate user who does not have privilege to see private campaign by itself");
			}
		
			//Verify Public to all users (Status Driven)Campaign created by Corporate user who does not have privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Public to all users(Status Driven) Campaign created by Corporate user who does not have privilege to see private campaign by itself");
			campaignCenterPage.searchCampaignByFilter(driver, firstcampaignNamecorpstatus);

			boolean isCampaignNamePresentCorpaccessibilitypublictoalluserscorpstatusC2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firstcampaignNamecorpstatus + "')]");
			if (isCampaignNamePresentCorpaccessibilitypublictoalluserscorpstatusC2 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users(Status Driven) Campaign created by Corporate user who does not have privilege to see private campaign by itself");
			}
			//Verify Public to all corporate users Campaign created by Corporate user who has privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Public to all corporate users(Status Driven) Campaign created by Corporate user who does not have privilege to see private campaign by itself");
			campaignCenterPage.searchCampaignByFilter(driver, secondcampaignNamecorpstatus);
			
			boolean isCampaignNamePresentCorpaccessibilitypublictoallcorporateuserscorpstatusC2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondcampaignNamecorpstatus + "')]");
			if (isCampaignNamePresentCorpaccessibilitypublictoallcorporateuserscorpstatusC2 == false) {
				fc.utobj().throwsException("was not able to verify Public to all corporate users(Status Driven) Campaign created by Corporate user who does not privilege to see private campaign by itself");
			}
			
			//Verify Private Campaign created by Corporate user who has privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Private Campaign created by Corporate user(Status Driven) who does not have privilege to see private campaign by itself");
			campaignCenterPage.searchCampaignByFilter(driver, thirdcampaignNamecorpstatus);
			
			boolean isCampaignNamePresentCorpaccessibilityprivatecorpstatusC2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + thirdcampaignNamecorpstatus + "')]");
			if (isCampaignNamePresentCorpaccessibilityprivatecorpstatusC2 == false) {
				fc.utobj().throwsException("was not able to Private Campaign created by Corporate user(Status Driven) who does not have privilege to see private campaign by itself");
			}
				
			fc.utobj().printTestStep("Navigate to CRM>Campaign Center>email template");
			emailtemplate.clickonemailtamplate(driver);
			
			//Verify Public to all users template created by Corporate user who does not have privilege to see private template by itself
			fc.utobj().printTestStep("Verify Public to all users template created by Corporate user who does not have privilege to see private template by itself");
			campaignCenterPage.searchTemplateByFilter(driver, firsttemplateNamecorp);

			boolean isTemplateNamePresentCorpaccessibilitypublictoalluserscorpC2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firsttemplateNamecorp + "')]");
			if (isTemplateNamePresentCorpaccessibilitypublictoalluserscorpC2 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users template created by Corporate user who has privilege to see private template by itself");
			}
			//Verify Public to all corporate users template created by Corporate user who has privilege to see private template by itself
			fc.utobj().printTestStep("Verify Public to all corporate users template created by Corporate user who does not have privilege to see private template by itself");
			campaignCenterPage.searchTemplateByFilter(driver, secondtemplateNamecorp);
			
			boolean isTemplateNamePresentCorpaccessibilitypublictoallcorporateuserscorpC2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondtemplateNamecorp + "')]");
			if (isTemplateNamePresentCorpaccessibilitypublictoallcorporateuserscorpC2 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users template created by Corporate user who has privilege to see private template by itself");
			}
			
			//Verify Private template created by Corporate user who has privilege to see private template by itself
			fc.utobj().printTestStep("Verify Privare template created by Corporate user who does not have privilege to see private template by itself");
			campaignCenterPage.searchTemplateByFilter(driver, thirdtemplateNamecorp);
			
			boolean isTemplateNamePresentCorpaccessibilityprivatecorpC2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + thirdtemplateNamecorp + "')]");
			if (isTemplateNamePresentCorpaccessibilityprivatecorpC2 == false) {
				fc.utobj().throwsException("was not able to verify private template created by Corporate user who has privilege to see private template by itself");
			}
			
			//Verify Public to all users (Status driven)template created by Corporate user who does not have privilege to see private template by itself
			fc.utobj().printTestStep("Verify Public to all users (Status driven)template created by Corporate user who does not have privilege to see private template by itself");
			campaignCenterPage.searchTemplateByFilter(driver, firsttemplateNamecorpstatus);

			boolean isTemplateNamePresentCorpaccessibilitypublictoalluserscorpstatusC2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firsttemplateNamecorpstatus + "')]");
			if (isTemplateNamePresentCorpaccessibilitypublictoalluserscorpstatusC2 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users (Status driven)template created by Corporate user who has privilege to see private template by itself");
			}
			//Verify Public to all corporate users template created by Corporate user who has privilege to see private template by itself
			fc.utobj().printTestStep("Verify Public to all corporate users (Status driven)template created by Corporate user who does not have privilege to see private template by itself");
			campaignCenterPage.searchTemplateByFilter(driver, secondtemplateNamecorpstatus);
			
			boolean isTemplateNamePresentCorpaccessibilitypublictoallcorporateuserscorpstatusC2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondtemplateNamecorpstatus + "')]");
			if (isTemplateNamePresentCorpaccessibilitypublictoallcorporateuserscorpstatusC2 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users (Status driven)template created by Corporate user who has privilege to see private template by itself");
			}
			
			//Verify Private template created by Corporate user who has privilege to see private template by itself
			fc.utobj().printTestStep("Verify Privare (Status driven)template created by Corporate user who does not have privilege to see private template by itself");
			campaignCenterPage.searchTemplateByFilter(driver, thirdtemplateNamecorpstatus);
			
			boolean isTemplateNamePresentCorpaccessibilityprivatecorpstatusC2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + thirdtemplateNamecorpstatus + "')]");
			if (isTemplateNamePresentCorpaccessibilityprivatecorpstatusC2 == false) {
				fc.utobj().throwsException("was not able to verify private (Status driven)template created by Corporate user who has privilege to see private template by itself");
			}
		
			fc.utobj().printTestStep("Navigate to CRM>Campaign Center>campaign template");
			emailtemplate.clickoncampaigntemplate(driver);
			//Verify Public to all users of my region Campaign created by Regional user who has privilege to see private campaign by itself(R1)
			//Verifying Campaigns and templates of Regional user (R1) after logging with Corporate user (C2)
			fc.utobj().printTestStep("Verifying Campaigns and templates of Regional user (R1) after logging with Corporate user (C2)");
			fc.utobj().printTestStep("Verify Public to all users of my region Campaign created by Regional user who has privilege to see private campaign by another corporate user(C2)");
			campaignCenterPage.searchCampaignByFilter(driver, firstcampaignNamereg);

			boolean isCampaignNamePresentRegpaccessibilitypublictoallusersofmyregionC2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firstcampaignNamereg + "')]");
			if (isCampaignNamePresentRegpaccessibilitypublictoallusersofmyregionC2 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my region Campaign created by Regional user who has privilege to see private campaign by another corporate user(C2)");
			}
			
			//Verify Private Campaign created by Regional user who has privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Private Campaign created by Regional user who has privilege to see private campaign by Corporate user(C2)");
			campaignCenterPage.searchCampaignByFilter(driver, secondcampaignNamereg);
			
			boolean isCampaignNamePresentRegpaccessibilityprivateC2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondcampaignNamereg + "')]");
			System.out.println(isCampaignNamePresentRegpaccessibilityprivateC2);
			System.out.println(!isCampaignNamePresentRegpaccessibilityprivateC2);
			if (!isCampaignNamePresentRegpaccessibilityprivateC2 == false) {
				fc.utobj().throwsException("was not able to verify Private Campaign created by Regional user who has privilege to see private campaign by another corporate user(C2)");
			}
			
			//Verify Public to all users of my region (Status Driven)Campaign created by Regional user who has privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Public to all users of my region (Status Driven)Campaign created by Regional user who has privilege to see private campaign by another corporate user(C2)");
			campaignCenterPage.searchCampaignByFilter(driver, firstcampaignNameregstatus);

			boolean isCampaignNamePresentRegpaccessibilitypublictoallusersofmyregionstatusC2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firstcampaignNameregstatus + "')]");
			if (isCampaignNamePresentRegpaccessibilitypublictoallusersofmyregionstatusC2 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my region (Status Driven)Campaign created by Regional user who has privilege to see private campaign by another corporate user(C2)");
			}
			
			//Verify Private Campaign created by Regional user who has privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Private (Status Driven)Campaign created by Regional user who has privilege to see private campaign by another corporate user(C2)");
			campaignCenterPage.searchCampaignByFilter(driver, secondcampaignNameregstatus);
			
			boolean isCampaignNamePresentRegpaccessibilityprivatestatusC2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondcampaignNameregstatus + "')]");
			if (!isCampaignNamePresentRegpaccessibilityprivatestatusC2 == false) {
				fc.utobj().throwsException("was not able to verify Private (Status Driven)Campaign created by Regional user who has privilege to see private campaign by another corporate user(C2)");
			}
			
			fc.utobj().printTestStep("Navigate to CRM>Campaign Center>email template");
			emailtemplate.clickonemailtamplate(driver);
			
			//Verify Public to all users template created by Corporate user who has privilege to see private template by itself
			fc.utobj().printTestStep("Verify Public to all users of my region template created by Regional user who has privilege to see private template by another corporate user(C2)");
			campaignCenterPage.searchTemplateByFilter(driver, firsttemplateNamereg);

			boolean isTemplateNamePresentRegaccessibilitypublictoallusersofmyregionC2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firsttemplateNamereg + "')]");
			if (isTemplateNamePresentRegaccessibilitypublictoallusersofmyregionC2 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users template created by Regional user who has privilege to see private template by another corporate user(C2)");
			}
			
			//Verify Private template created by Regional user who has privilege to see private template by itself
			fc.utobj().printTestStep("Verify Private template created by Regional user who has privilege to see private template by another corporate user(C2)");
			campaignCenterPage.searchTemplateByFilter(driver, secondtemplateNamereg);
			
			boolean isTemplateNamePresentRegaccessibilityprivateC2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondtemplateNamereg + "')]");
			if (!isTemplateNamePresentRegaccessibilityprivateC2 == false) {
				fc.utobj().throwsException("was not able to verify private template created by Regional user who has privilege to see private template by another corporate user(C2)");
			}
			
			//Verify Public to all users(Status Driven) template created by Regional user who has privilege to see private template by itself
			fc.utobj().printTestStep("Verify Public to all users of my region (Status Driven)template created by Regional user who has privilege to see private template by another corporate user(C2)");
			campaignCenterPage.searchTemplateByFilter(driver, firsttemplateNameregstatus);

			boolean isTemplateNamePresentRegaccessibilitypublictoallusersofmyregionstatusC2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firsttemplateNameregstatus + "')]");
			if (isTemplateNamePresentRegaccessibilitypublictoallusersofmyregionstatusC2 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my region (Status Driven)template created by Regional user who has privilege to see private template by another corporate user(C2)");
			}
			
			//Verify Private template created by Regional user who has privilege to see private template by itself
			fc.utobj().printTestStep("Verify Private (Status Driven)template created by Regional user who has privilege to see private template by another corporate user(C2)");
			campaignCenterPage.searchTemplateByFilter(driver, secondtemplateNameregstatus);
			
			boolean isTemplateNamePresentRegaccessibilityprivatestatusC2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondtemplateNameregstatus + "')]");
			if (!isTemplateNamePresentRegaccessibilityprivatestatusC2 == false) {
				fc.utobj().throwsException("was not able to verify private (Status Driven)template created by Regional user who has privilege to see private template by another corporate user(C2)");
			}
			fc.utobj().printTestStep("Navigate to CRM>Campaign Center>campaign template");
			emailtemplate.clickoncampaigntemplate(driver);
			
			//Verify Public to all users Campaign created by Corporate user who has privilege to see private campaign by itself
			fc.utobj().printTestStep("Verifying Campaigns and templates of Corporate user (C1) by corporate user(C2)");
			fc.utobj().printTestStep("Verify Public to all users Campaign created by Corporate user who has privilege to see private campaign by corporate user(C2)");
			campaignCenterPage.searchCampaignByFilter(driver, firstcampaignName);

			boolean isCampaignNamePresentCorpaccessibilitypublictoallusersC2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firstcampaignName + "')]");
			if (isCampaignNamePresentCorpaccessibilitypublictoallusersC2 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users Campaign created by Corporate user who has privilege to see private campaign by corporate user(C2)");
			}
			//Verify Public to all corporate users Campaign created by Corporate user who has privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Public to all corporate users Campaign created by Corporate user who has privilege to see private campaign");
			campaignCenterPage.searchCampaignByFilter(driver, secondcampaignName);
			
			boolean isCampaignNamePresentCorpaccessibilitypublictoallcorporateusersC2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondcampaignName + "')]");
			if (isCampaignNamePresentCorpaccessibilitypublictoallcorporateusersC2 == false) {
				fc.utobj().throwsException("was not able to verify Public to all corporate users Campaign created by Corporate user who has privilege to see private campaign by corporate user(C2)");
			}
			
			//Verify Private Campaign created by Corporate user who has privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Private Campaign created by Corporate user who has privilege to see private campaign by corporate user(C2)");
			campaignCenterPage.searchCampaignByFilter(driver, thirdcampaignName);
			
			boolean isCampaignNamePresentCorpaccessibilityprivateC2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + thirdcampaignName + "')]");
			if (!isCampaignNamePresentCorpaccessibilityprivateC2 == false) {
				fc.utobj().throwsException("was not able to Private Campaign created by Corporate user who has privilege to see private campaign by corporate user(C2)");
			}
			
			//Verify Public to all users(Status Driven) Campaign created by Corporate user who has privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Public to all users(Status Driven) Campaign created by Corporate user who has privilege to see private campaign by corporate user(C2)");
			campaignCenterPage.searchCampaignByFilter(driver, firstcampaignNamestatus);

			boolean isCampaignNamePresentCorpaccessibilitypublictoallusersstatusC2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firstcampaignNamestatus + "')]");
			if (isCampaignNamePresentCorpaccessibilitypublictoallusersstatusC2 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users(Status Driven) Campaign created by Corporate user who has privilege to see private campaign by corporate user(C2)");
			}
			//Verify Public to all corporate users(Status Driven) Campaign created by Corporate user who has privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Public to all corporate users(Status Driven) Campaign created by Corporate user who has privilege to see private campaign");
			campaignCenterPage.searchCampaignByFilter(driver, secondcampaignNamestatus);
			
			boolean isCampaignNamePresentCorpaccessibilitypublictoallcorporateusersstatusC2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondcampaignNamestatus + "')]");
			if (isCampaignNamePresentCorpaccessibilitypublictoallcorporateusersstatusC2 == false) {
				fc.utobj().throwsException("was not able to verify Public to all corporate users(Status Driven) Campaign created by Corporate user who has privilege to see private campaign by corporate user(C2)");
			}
			
			//Verify Private Campaign created by Corporate user(Status Driven) who has privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Private Campaign(Status Driven) created by Corporate user who has privilege to see private campaign by corporate user(C2)");
			campaignCenterPage.searchCampaignByFilter(driver, thirdcampaignNamestatus);
			
			boolean isCampaignNamePresentCorpaccessibilityprivatestatusC2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + thirdcampaignNamestatus + "')]");
			if (!isCampaignNamePresentCorpaccessibilityprivatestatusC2 == false) {
				fc.utobj().throwsException("was not able to Private Campaign(Status Driven) created by Corporate user who has privilege to see private campaign by corporate user(C2)");
			}
		
			fc.utobj().printTestStep("Navigate to CRM>Campaign Center>email template");
			emailtemplate.clickonemailtamplate(driver);
			
			//Verify Public to all users template created by Corporate user who has privilege to see private template by itself
			fc.utobj().printTestStep("Verify Public to all users template created by Corporate user who has privilege to see private template by corporate user(C2)");
			campaignCenterPage.searchTemplateByFilter(driver, firsttemplateName);

			boolean isTemplateNamePresentCorpaccessibilitypublictoallusersC2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firsttemplateName + "')]");
			if (isTemplateNamePresentCorpaccessibilitypublictoallusersC2 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users template created by Corporate user who has privilege to see private template by corporate user(C2)");
			}
			//Verify Public to all corporate users template created by Corporate user who has privilege to see private template by itself
			fc.utobj().printTestStep("Verify Public to all corporate users template created by Corporate user who has privilege to see private template by corporate user(C2)");
			campaignCenterPage.searchTemplateByFilter(driver, secondtemplateName);
			
			boolean isTemplateNamePresentCorpaccessibilitypublictoallcorporateusersC2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondtemplateName + "')]");
			if (isTemplateNamePresentCorpaccessibilitypublictoallcorporateusersC2 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users template created by Corporate user who has privilege to see private template by corporate user(C2)");
			}
			
			//Verify Private template created by Corporate user who has privilege to see private template by itself
			fc.utobj().printTestStep("Verify Privare template created by Corporate user who has privilege to see private template by corporate user(C2)");
			campaignCenterPage.searchTemplateByFilter(driver, thirdtemplateName);
			
			boolean isTemplateNamePresentCorpaccessibilityprivateC2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + thirdtemplateName + "')]");
			if (!isTemplateNamePresentCorpaccessibilityprivateC2 == false) {
				fc.utobj().throwsException("was not able to verify private template created by Corporate user who has privilege to see private template by corporate user(C2)");
			}
				
			//Verify Public to all users template(Status Driven) created by Corporate user who has privilege to see private template by itself
			fc.utobj().printTestStep("Verify Public to all users template(Status Driven) created by Corporate user who has privilege to see private template by corporate user(C2)");
			campaignCenterPage.searchTemplateByFilter(driver, firsttemplateNamestatus);

			boolean isTemplateNamePresentCorpaccessibilitypublictoallusersstatusC2= fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firsttemplateNamestatus + "')]");
			if (isTemplateNamePresentCorpaccessibilitypublictoallusersstatusC2 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users template(Status Driven) created by Corporate user who has privilege to see private template by corporate user(C2)");
			}
			//Verify Public to all corporate users template(Status Driven) created by Corporate user who has privilege to see private template by itself
			fc.utobj().printTestStep("Verify Public to all corporate users template(Status Driven) created by Corporate user who has privilege to see private template by corporate user(C2)");
			campaignCenterPage.searchTemplateByFilter(driver, secondtemplateNamestatus);
			
			boolean isTemplateNamePresentCorpaccessibilitypublictoallcorporateusersstatusC2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondtemplateNamestatus + "')]");
			if (isTemplateNamePresentCorpaccessibilitypublictoallcorporateusersstatusC2 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users template(Status Driven) created by Corporate user who has privilege to see private template by corporate user(C2)");
			}
			
			//Verify Private template(Status Driven) created by Corporate user who has privilege to see private template by itself
			fc.utobj().printTestStep("Verify Privare template(Status Driven) created by Corporate user who has privilege to see private template by corporate user(C2)");
			campaignCenterPage.searchTemplateByFilter(driver, thirdtemplateNamestatus);
			
			boolean isTemplateNamePresentCorpaccessibilityprivatestatusC2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + thirdtemplateNamestatus + "')]");
			if (!isTemplateNamePresentCorpaccessibilityprivatestatusC2 == false) {
				fc.utobj().throwsException("was not able to verify private template(Status Driven) created by Corporate user who has privilege to see private template by corporate user(C2)");
			}
		
			fc.utobj().printTestStep("Navigate to CRM>Campaign Center>campaign template");
			emailtemplate.clickoncampaigntemplate(driver);
			
			//Verify Public to all users Campaign created by Corporate user who has privilege to see private campaign by itself
			fc.utobj().printTestStep("Verifying Campaigns and templates of Regional user (R2) by corporate user(C2)");
			//Verify Public to all users of my region Campaign created by Regional user who does not have privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Public to all users of my region Campaign created by Regional user who does not have privilege to see private campaign by corporate user(C2)");
			campaignCenterPage.searchCampaignByFilter(driver, firstcampaignNamereg1);

			boolean isCampaignNamePresentRegpaccessibilitypublictoallusersofmyregion1C2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firstcampaignNamereg1 + "')]");
			if (isCampaignNamePresentRegpaccessibilitypublictoallusersofmyregion1C2 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my region Campaign created by Regional user who does not have privilege to see private campaign by corporate user(C2)");
			}
			
			//Verify Private Campaign created by Regional user who does not have privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Private Campaign created by Regional user who does not have privilege to see private campaign by corporate user(C2)");
			campaignCenterPage.searchCampaignByFilter(driver, secondcampaignNamereg1);
			
			boolean isCampaignNamePresentRegpaccessibilityprivate1C2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondcampaignNamereg1 + "')]");
			if (!isCampaignNamePresentRegpaccessibilityprivate1C2 == false) {
				fc.utobj().throwsException("was not able to verify Private Campaign created by Regional user who does not have privilege to see private campaign by corporate user(C2)");
			}
			
			//Verify Public to all users of my region (Status Driven)Campaign created by Regional user who does not have privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Public to all users of my region (Status Driven)Campaign created by Regional user who does not have privilege to see private campaign by corporate user(C2)");
			campaignCenterPage.searchCampaignByFilter(driver, firstcampaignNamereg1status);

			boolean isCampaignNamePresentRegpaccessibilitypublictoallusersofmyregion1statusC2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firstcampaignNamereg1status + "')]");
			if (isCampaignNamePresentRegpaccessibilitypublictoallusersofmyregion1statusC2 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my region (Status Driven)Campaign created by Regional user who does not have privilege to see private campaign by corporate user(C2)");
			}
			
			//Verify Private (Status Driven)Campaign created by Regional user who does not have privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Private (Status Driven)Campaign created by Regional user who does not have privilege to see private campaign by corporate user(C2)");
			campaignCenterPage.searchCampaignByFilter(driver, secondcampaignNamereg1status);
			
			boolean isCampaignNamePresentRegpaccessibilityprivate1statusC2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondcampaignNamereg1status + "')]");
			if (!isCampaignNamePresentRegpaccessibilityprivate1statusC2 == false) {
				fc.utobj().throwsException("was not able to verify Private (Status Driven)Campaign created by Regional user who does not have privilege to see private campaign by corporate user(C2)");
			}
		
			fc.utobj().printTestStep("Navigate to CRM>Campaign Center>email template");
			emailtemplate.clickonemailtamplate(driver);
			
			//Verify Public to all users template created by Corporate user who does not have privilege to see private template by itself
			fc.utobj().printTestStep("Verify Public to all users of my region template created by Regional user who does not have privilege to see private template by corporate user(C2)");
			campaignCenterPage.searchTemplateByFilter(driver, firsttemplateNamereg1);

			boolean isTemplateNamePresentRegaccessibilitypublictoallusersofmyregion1C2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firsttemplateNamereg1 + "')]");
			if (isTemplateNamePresentRegaccessibilitypublictoallusersofmyregion1C2 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my region template created by Regional user who does not have privilege to see private template by corporate user(C2)");
			}
			
			//Verify Private template created by Regional user who does not have privilege to see private template by itself
			fc.utobj().printTestStep("Verify Private template created by Regional user does not have privilege to see private template by corporate user(C2)");
			campaignCenterPage.searchTemplateByFilter(driver, secondtemplateNamereg1);
			
			boolean isTemplateNamePresentRegaccessibilityprivate1C2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondtemplateNamereg1 + "')]");
			if (!isTemplateNamePresentRegaccessibilityprivate1C2 == false) {
				fc.utobj().throwsException("was not able to verify Private template created by Regional user does not have privilege to see private template by corporate user(C2)");
			}
			
			//Verify Public to all users (Status Driven)template created by Regional user who does not have privilege to see private template by itself
			fc.utobj().printTestStep("Verify Public to all users of my region (Status Driven)template created by Regional user who does not have privilege to see private template by corporate user(C2)");
			campaignCenterPage.searchTemplateByFilter(driver, firsttemplateNamereg1status);

			boolean isTemplateNamePresentRegaccessibilitypublictoallusersofmyregion1statusC2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firsttemplateNamereg1status + "')]");
			if (isTemplateNamePresentRegaccessibilitypublictoallusersofmyregion1statusC2 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my region (Status Driven)template created by Regional user who does not have privilege to see private template by corporate user(C2)");
			}
			
			//Verify Private template created by Regional user who does not have privilege to see private template by itself
			fc.utobj().printTestStep("Verify Private (Status Driven)template created by Regional user does not have privilege to see private template by corporate user(C2)");
			campaignCenterPage.searchTemplateByFilter(driver, secondtemplateNamereg1status);
			
			boolean isTemplateNamePresentRegaccessibilityprivate1statusC2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondtemplateNamereg1status + "')]");
			if (!isTemplateNamePresentRegaccessibilityprivate1statusC2 == false) {
				fc.utobj().throwsException("was not able to verify Private (Status Driven)template created by Regional user does not have privilege to see private template by corporate user(C2)");
			}
			
			fc.utobj().printTestStep("Navigate to CRM>Campaign Center>campaign template");
			emailtemplate.clickoncampaigntemplate(driver);
			
			fc.utobj().clickElement(driver, crmcenter.locationCampaign);
			fc.utobj().printTestStep("Verifying Campaigns and templates of Franchise owner user (F0New) by corporate user(C2)");
			fc.utobj().printTestStep("Verify Public to all users of my Franchise Campaign created by Franchise user who has privilege to see private campaign by corporate user(C2)");
			campaignCenterPage.searchCampaignByFilter(driver, firstcampaignNamefran);

			boolean isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchiseC2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firstcampaignNamefran + "')]");
			if (isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchiseC2 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my Franchise Campaign created by Franchise user who has privilege to see private campaign by corporate user(C2)");
			}
			
			//Verify Private Campaign created by Franchise user who has privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Private Campaign created by Franchise user who has privilege to see private campaign by corporate user(C2)");
			campaignCenterPage.searchCampaignByFilter(driver, secondcampaignNamefran);
			
			boolean isCampaignNamePresentFranpaccessibilityprivateC2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondcampaignNamefran + "')]");
			if (!isCampaignNamePresentFranpaccessibilityprivateC2 == false) {
				fc.utobj().throwsException("was not able to verify Private Campaign created by Franchise user who has privilege to see private campaign by corporate user(C2)");
			}
			
			
			//Verify Public to all users of my Franchise (Status Driven)Campaign created by Franchise user who has privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Public to all users of my Franchise (Status Driven)Campaign created by Franchise user who has privilege to see private campaign by corporate user(C2)");
			campaignCenterPage.searchCampaignByFilter(driver, firstcampaignNamefranstatus);

			boolean isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchisestatusC2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firstcampaignNamefranstatus + "')]");
			if (isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchisestatusC2 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my Franchise (Status Driven)Campaign created by Franchise user who has privilege to see private campaign by corporate user(C2)");
			}
			
			//Verify Private Campaign created by Franchise user who has privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Private (Status Driven)Campaign created by Franchise user who has privilege to see private campaign by corporate user(C2)");
			campaignCenterPage.searchCampaignByFilter(driver, secondcampaignNamefranstatus);
			
			boolean isCampaignNamePresentFranpaccessibilityprivatestatusC2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondcampaignNamefranstatus + "')]");
			if (!isCampaignNamePresentFranpaccessibilityprivatestatusC2 == false) {
				fc.utobj().throwsException("was not able to verify Private (Status Driven)Campaign created by Franchise user who has privilege to see private campaign by corporate user(C2)");
			}
			
			fc.utobj().printTestStep("Navigate to CRM>Campaign Center>email template");
			emailtemplate.clickonemailtamplate(driver);
			fc.utobj().clickElement(driver, crmcenter.locationtemplate);
			//Verify Public to all users template created by Corporate user who has privilege to see private template by itself
			fc.utobj().printTestStep("Verify Public to all users of my franchise template created by franchise user who has privilege to see private template by corporate user(C2)");
			campaignCenterPage.searchTemplateByFilter(driver, firsttemplateNamefran);

			boolean isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchiseC2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firsttemplateNamefran + "')]");
			if (isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchiseC2 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my franchise template created by franchise user who has privilege to see private template by corporate user(C2)");
			}
			
			//Verify Private template created by franchise user who has privilege to see private template by itself
			fc.utobj().printTestStep("Verify Private template created by franchise user who has privilege to see private template by corporate user(C2)");
			campaignCenterPage.searchTemplateByFilter(driver, secondtemplateNamefran);
			
			boolean isTemplateNamePresentfranaccessibilityprivateC2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondtemplateNamefran + "')]");
			if (!isTemplateNamePresentfranaccessibilityprivateC2 == false) {
				fc.utobj().throwsException("was not able to verify private template created by franchise user who has privilege to see private template by corporate user(C2)");
			}
			
			//Verify Public to all users (Status Driven)template created by Corporate user who has privilege to see private template by itself
			fc.utobj().printTestStep("Verify Public to all users of my franchise (Status Driven)template created by franchise user who has privilege to see private template by corporate user(C2)");
			campaignCenterPage.searchTemplateByFilter(driver, firsttemplateNamefranstatus);

			boolean isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchisestatusC2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firsttemplateNamefranstatus + "')]");
			if (isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchisestatusC2 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my franchise (Status Driven)template created by franchise user who has privilege to see private template by corporate user(C2)");
			}
			
			//Verify Private template created by franchise user who has privilege to see private template by itself
			fc.utobj().printTestStep("Verify Private (Status Driven)template created by franchise user who has privilege to see private template by corporate user(C2)");
			campaignCenterPage.searchTemplateByFilter(driver, secondtemplateNamefranstatus);
			
			boolean isTemplateNamePresentfranaccessibilityprivatestatusC2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondtemplateNamefranstatus + "')]");
			if (!isTemplateNamePresentfranaccessibilityprivatestatusC2 == false) {
				fc.utobj().throwsException("was not able to verify private (Status Driven)template created by franchise user who has privilege to see private template by corporate user(C2)");
			}
		
			
			fc.utobj().printTestStep("Navigate to CRM>Campaign Center>campaign template");
			emailtemplate.clickoncampaigntemplate(driver);
			fc.utobj().clickElement(driver, crmcenter.locationCampaign);
			fc.utobj().printTestStep("Verifying Campaigns and templates of Franchise Mu user (V1) by corporate user(C2)");
			
			fc.utobj().printTestStep("Verify Public to all users of my Franchise Campaign created by Franchise MU user(V1) who has privilege to see private campaign by corporate user(C2)");
			campaignCenterPage.searchCampaignByFilter(driver, firstcampaignNamefranmu);

			boolean isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchisemuC2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firstcampaignNamefranmu + "')]");
			if (isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchisemuC2 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my Franchise Campaign created by Franchise MU user(V1) who has privilege to see private campaign by corporate user(C2)");
			}
			
			//Verify Private Campaign created by Franchise user who has privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Private Campaign created by Franchise MU user(V1) who has privilege to see private campaign by corporate user(C2)");
			campaignCenterPage.searchCampaignByFilter(driver, secondcampaignNamefranmu);
			
			boolean isCampaignNamePresentFranpaccessibilityprivatemuC2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondcampaignNamefranmu + "')]");
			if (!isCampaignNamePresentFranpaccessibilityprivatemuC2 == false) {
				fc.utobj().throwsException("was not able to verify Private Campaign created by Franchise MU user(V1) who has privilege to see private campaign by corporate user(C2)");
			}
			
			
			//Verify Public to all users of my Franchise (Status Driven)Campaign created by Franchise user who has privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Public to all users of my Franchise (Status Driven)Campaign created by Franchise MU user(V1) who has privilege to see private campaign by corporate user(C2)");
			campaignCenterPage.searchCampaignByFilter(driver, firstcampaignNamefranmustatus);

			boolean isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchisemustatusC2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firstcampaignNamefranmustatus + "')]");
			if (isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchisemustatusC2 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my Franchise (Status Driven)Campaign created by Franchise MU user(V1) who has privilege to see private campaign by corporate user(C2)");
			}
			
			//Verify Private Campaign created by Franchise user who has privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Private (Status Driven)Campaign created by Franchise MU user(V1) who has privilege to see private campaign by corporate user(C2)");
			campaignCenterPage.searchCampaignByFilter(driver, secondcampaignNamefranmustatus);
			
			boolean isCampaignNamePresentFranpaccessibilityprivatemustatusC2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondcampaignNamefranmustatus + "')]");
			if (!isCampaignNamePresentFranpaccessibilityprivatemustatusC2 == false) {
				fc.utobj().throwsException("was not able to verify Private (Status Driven)Campaign created by Franchise MU user(V1) who has privilege to see private campaign by corporate user(C2)");
			}
			
			fc.utobj().printTestStep("Navigate to CRM>Campaign Center>email template");
			emailtemplate.clickonemailtamplate(driver);
			fc.utobj().clickElement(driver, crmcenter.locationtemplate);
			//Verify Public to all users template created by Corporate user who has privilege to see private template by itself
			fc.utobj().printTestStep("Verify Public to all users of my franchise template created by franchise MU user(V1) who has privilege to see private template by corporate user(C2)");
			campaignCenterPage.searchTemplateByFilter(driver, firsttemplateNamefranmu);

			boolean isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchisemuC2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firsttemplateNamefranmu + "')]");
			if (isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchisemuC2 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my franchise template created by franchise MU user(V1) who has privilege to see private template by corporate user(C2)");
			}
			
			//Verify Private template created by franchise user who has privilege to see private template by itself
			fc.utobj().printTestStep("Verify Private template created by franchise MU user(V1) who has privilege to see private template by corporate user(C2)");
			campaignCenterPage.searchTemplateByFilter(driver, secondtemplateNamefranmu);
			
			boolean isTemplateNamePresentfranaccessibilityprivatemuC2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondtemplateNamefranmu + "')]");
			if (!isTemplateNamePresentfranaccessibilityprivatemuC2 == false) {
				fc.utobj().throwsException("was not able to verify private template created by franchise MU user(V1) who has privilege to see private template by corporate user(C2)");
			}
			
			//Verify Public to all users template created by Corporate user who has privilege to see private template by itself
			fc.utobj().printTestStep("Verify Public to all users of my franchise (Status Driven)template created by franchise MU user(V1) who has privilege to see private template by corporate user(C2)");
			campaignCenterPage.searchTemplateByFilter(driver, firsttemplateNamefranmustatus);

			boolean isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchisemustatusC2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firsttemplateNamefranmustatus + "')]");
			if (isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchisemustatusC2 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my franchise (Status Driven)template created by franchise MU user(V1) who has privilege to see private template by corporate user(C2)");
			}
			
			//Verify Private template created by franchise user who has privilege to see private template by itself
			fc.utobj().printTestStep("Verify Private (Status Driven)template created by franchise MU user(V1) who has privilege to see private template by corporate user(C2)");
			campaignCenterPage.searchTemplateByFilter(driver, secondtemplateNamefranmustatus);
			
			boolean isTemplateNamePresentfranaccessibilityprivatemustatusC2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondtemplateNamefranmustatus + "')]");
			if (!isTemplateNamePresentfranaccessibilityprivatemustatusC2 == false) {
				fc.utobj().throwsException("was not able to verify private (Status Driven)template created by franchise MU user(V1) who has privilege to see private template by corporate user(C2)");
			}
			
			
			
			
			fc.utobj().printTestStep("Navigate to CRM>Campaign Center>campaign template");
			emailtemplate.clickoncampaigntemplate(driver);
			fc.utobj().clickElement(driver, crmcenter.locationCampaign);
			fc.utobj().printTestStep("Verifying Campaigns and templates of Franchise Employee user (V2) by corporate user(C1)");
			fc.utobj().printTestStep("Verify Public to all users of my Franchise Campaign created by Franchise Employee user(V2) who does not have privilege to see private campaign by corporate user(C2)");
			campaignCenterPage.searchCampaignByFilter(driver, firstcampaignNamefranmue2);

			boolean isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchisemue2C2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firstcampaignNamefranmue2 + "')]");
			if (isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchisemue2C2 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my Franchise Campaign created by Franchise Employee user(V2) who does not have privilege to see private campaign by corporate user(C2)");
			}
			
			//Verify Private Campaign created by Franchise user who has privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Private Campaign created by Franchise Employee user(V2) who does not have privilege to see private campaign by corporate user(C2)");
			campaignCenterPage.searchCampaignByFilter(driver, secondcampaignNamefranmue2);
			
			boolean isCampaignNamePresentFranpaccessibilityprivatemue2C2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondcampaignNamefranmue2 + "')]");
			if (!isCampaignNamePresentFranpaccessibilityprivatemue2C2 == false) {
				fc.utobj().throwsException("was not able to verify Private Campaign created by Franchise Employee user(V2) who does not have privilege to see private campaign by corporate user(C2)");
			}
			
			
			//Verify Public to all users of my Franchise Campaign created by Franchise Employee user(V2) who has privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Public to all users of my Franchise (Status Driven)Campaign created by Franchise Employee user(V2) who does not have privilege to see private campaign by corporate user(C2)");
			campaignCenterPage.searchCampaignByFilter(driver, firstcampaignNamefranmue2status);

			boolean isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchisemue2statusC2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firstcampaignNamefranmue2status + "')]");
			if (isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchisemue2statusC2 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my Franchise (Status Driven)Campaign created by Franchise Employee user(V2) who does not have privilege to see private campaign by corporate user(C2)");
			}
			
			//Verify Private Campaign created by Franchise user who has privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Private (Status Driven)Campaign created by Franchise Employee user(V2) who does not have privilege to see private campaign by corporate user(C2)");
			campaignCenterPage.searchCampaignByFilter(driver, secondcampaignNamefranmue2status);
			
			boolean isCampaignNamePresentFranpaccessibilityprivatemue2statusC2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondcampaignNamefranmue2status + "')]");
			if (!isCampaignNamePresentFranpaccessibilityprivatemue2statusC2 == false) {
				fc.utobj().throwsException("was not able to verify Private (Status Driven)Campaign created by Franchise Employee user(V2) who does not have privilege to see private campaign by corporate user(C2)");
			}
			
			fc.utobj().printTestStep("Navigate to CRM>Campaign Center>email template");
			emailtemplate.clickonemailtamplate(driver);
			fc.utobj().clickElement(driver, crmcenter.locationtemplate);
			//Verify Public to all users template created by Franchise user who has privilege to see private template by itself
			fc.utobj().printTestStep("Verify Public to all users of my franchise template created by franchise Employee user(V2) who does not have privilege to see private template by corporate user(C2)");
			campaignCenterPage.searchTemplateByFilter(driver, firsttemplateNamefranmue2);

			boolean isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchisemue2C2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firsttemplateNamefranmue2 + "')]");
			if (isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchisemue2C2 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my franchise template created by franchise Employee user(V2) who has privilege to see private template by corporate user(C2)");
			}
			
			//Verify Private template created by franchise user who has privilege to see private template by itself
			fc.utobj().printTestStep("Verify Private template created by franchise Employee user(V2) who has privilege to see private template by corporate user(C2");
			campaignCenterPage.searchTemplateByFilter(driver, secondtemplateNamefranmue2);
			
			boolean isTemplateNamePresentfranaccessibilityprivatemue2C2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondtemplateNamefranmue2 + "')]");
			if (!isTemplateNamePresentfranaccessibilityprivatemue2C2 == false) {
				fc.utobj().throwsException("was not able to verify private template created by franchise Employee user(V2) who has privilege to see private template by corporate user(C2)");
			}
			
			//Verify Public to all users template created by Franchise user who has privilege to see private template by itself
			fc.utobj().printTestStep("Verify Public to all users of my franchise (Status Driven)template created by franchise Employee user(V2) who does not have privilege to see private template by corporate user(C2)");
			campaignCenterPage.searchTemplateByFilter(driver, firsttemplateNamefranmue2status);

			boolean isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchisemue2statusC2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firsttemplateNamefranmue2status + "')]");
			if (isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchisemue2statusC2 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my franchise (Status Driven)template created by franchise Employee user(V2) who has privilege to see private template by corporate user(C2)");
			}
			
			//Verify Private template created by franchise user who has privilege to see private template by itself
			fc.utobj().printTestStep("Verify Private (Status Driven)template created by franchise Employee user(V2) who has privilege to see private template by corporate user(C2)");
			campaignCenterPage.searchTemplateByFilter(driver, secondtemplateNamefranmue2status);
			
			boolean isTemplateNamePresentfranaccessibilityprivatemue2statusC2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondtemplateNamefranmue2status + "')]");
			if (!isTemplateNamePresentfranaccessibilityprivatemue2statusC2 == false) {
				fc.utobj().throwsException("was not able to verify private (Status Driven)template created by franchise Employee user(V2) who has privilege to see private template by corporate user(C2)");
			}
			
			if(isCampaignNamePresentCorpaccessibilitypublictoalluserscorpC2 && isCampaignNamePresentCorpaccessibilitypublictoallcorporateuserscorpC2 && isCampaignNamePresentCorpaccessibilityprivatecorpC2 
					&& isCampaignNamePresentCorpaccessibilitypublictoalluserscorpstatusC2 && isCampaignNamePresentCorpaccessibilitypublictoallcorporateuserscorpstatusC2 && isCampaignNamePresentCorpaccessibilityprivatecorpstatusC2 
					&& isTemplateNamePresentCorpaccessibilitypublictoalluserscorpC2 && isTemplateNamePresentCorpaccessibilitypublictoallcorporateuserscorpC2 && isTemplateNamePresentCorpaccessibilityprivatecorpC2 
					&& isTemplateNamePresentCorpaccessibilitypublictoalluserscorpstatusC2 && isTemplateNamePresentCorpaccessibilitypublictoallcorporateuserscorpstatusC2 && isTemplateNamePresentCorpaccessibilityprivatecorpstatusC2 
					&& isCampaignNamePresentRegpaccessibilitypublictoallusersofmyregionC2 && !isCampaignNamePresentRegpaccessibilityprivateC2 && isCampaignNamePresentRegpaccessibilitypublictoallusersofmyregionstatusC2 
					&& !isCampaignNamePresentRegpaccessibilityprivatestatusC2 && isTemplateNamePresentRegaccessibilitypublictoallusersofmyregionC2 && !isTemplateNamePresentRegaccessibilityprivateC2 
					&& isTemplateNamePresentRegaccessibilitypublictoallusersofmyregionstatusC2 && !isTemplateNamePresentRegaccessibilityprivatestatusC2 && isCampaignNamePresentCorpaccessibilitypublictoallusersC2 
					&& isCampaignNamePresentCorpaccessibilitypublictoallcorporateusersC2 && !isCampaignNamePresentCorpaccessibilityprivateC2 && isCampaignNamePresentCorpaccessibilitypublictoallusersstatusC2 
					&& isCampaignNamePresentCorpaccessibilitypublictoallcorporateusersstatusC2 && !isCampaignNamePresentCorpaccessibilityprivatestatusC2 && isTemplateNamePresentCorpaccessibilitypublictoallusersC2 
					&& isTemplateNamePresentCorpaccessibilitypublictoallcorporateusersC2 && !isTemplateNamePresentCorpaccessibilityprivateC2 && isTemplateNamePresentCorpaccessibilitypublictoallusersstatusC2 
					&& isTemplateNamePresentCorpaccessibilitypublictoallcorporateusersstatusC2 && !isTemplateNamePresentCorpaccessibilityprivatestatusC2 && isCampaignNamePresentRegpaccessibilitypublictoallusersofmyregion1C2
					&& !isCampaignNamePresentRegpaccessibilityprivate1C2 && isCampaignNamePresentRegpaccessibilitypublictoallusersofmyregion1statusC2 && !isCampaignNamePresentRegpaccessibilityprivate1statusC2
					&& isTemplateNamePresentRegaccessibilitypublictoallusersofmyregion1C2 && !isTemplateNamePresentRegaccessibilityprivate1C2 && isTemplateNamePresentRegaccessibilitypublictoallusersofmyregion1statusC2 
					&& !isTemplateNamePresentRegaccessibilityprivate1statusC2 && isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchiseC2 && !isCampaignNamePresentFranpaccessibilityprivateC2 
					&& isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchisestatusC2 && !isCampaignNamePresentFranpaccessibilityprivatestatusC2 && isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchiseC2 
					&& !isTemplateNamePresentfranaccessibilityprivateC2 && isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchisestatusC2 && !isTemplateNamePresentfranaccessibilityprivatestatusC2 
					&& isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchisemuC2 && !isCampaignNamePresentFranpaccessibilityprivatemuC2 && isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchisemustatusC2 
					&& !isCampaignNamePresentFranpaccessibilityprivatemustatusC2 && isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchisemuC2 && !isTemplateNamePresentfranaccessibilityprivatemuC2 
					&& isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchisemustatusC2 && !isTemplateNamePresentfranaccessibilityprivatemustatusC2 && isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchisemue2C2 
					&& !isCampaignNamePresentFranpaccessibilityprivatemue2C2 && isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchisemue2statusC2 && !isCampaignNamePresentFranpaccessibilityprivatemue2statusC2 
					&& isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchisemue2C2 && !isTemplateNamePresentfranaccessibilityprivatemue2C2 && isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchisemue2statusC2
					&& !isTemplateNamePresentfranaccessibilityprivatemue2statusC2) {
				fc.utobj().printTestStep("able to verify all types of campaigns and templates created by corporate/rgional/franchise users when loging with Corporate user(C2) who does not have priviledge to see private campaign");
			}
			else {
				fc.utobj().throwsException("was not able to verify all types of campaigns and templates created by corporate/rgional/franchise users when loging with Corporate user(C2) who does not have priviledge to see private campaign");
			}
			
			
			
			
			
			//Login with Regional user(R1) who has role to see private campaign
			fc.utobj().printTestStep("Login with Regional user(R1) who has role to see private campaign");
			fc.loginpage().loginWithParameter(driver, Reguname, Regpassword);
			fc.crm().crm_common().openCRMCampaignCenterPage(fcHomePageTest, driver);
			fc.utobj().printTestStep("Click on the menu option and select campaign");
			/*CRMCampaignCenterPage pobj = new CRMCampaignCenterPage(driver);*/
			fc.utobj().clickElement(driver, pobj1.menuOptions);
			fc.utobj().clickElement(driver, pobj1.campaignLinks);
			 
			//Verifying Campaigns and templates of corporate user (C2) after logging with Regional user (R1)
			fc.utobj().printTestStep("Verifying Campaigns and templates of corporate user (C2) after logging with Regional user (R1)");
			fc.utobj().printTestStep("Verify Public to all users Campaign created by Corporate user(C2) who does not have privilege to see private campaign by Regional user (R1)");
			campaignCenterPage.searchCampaignByFilter(driver, firstcampaignNamecorp);

			boolean isCampaignNamePresentCorpaccessibilitypublictoalluserscorpR1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firstcampaignNamecorp + "')]");
			if (isCampaignNamePresentCorpaccessibilitypublictoalluserscorpR1 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users Campaign created by Corporate user who does not have privilege to see private campaign by Regional user (R1)");
			}
			//Verify Public to all corporate users Campaign created by Corporate user who has privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Public to all corporate users Campaign created by Corporate user who does not have privilege to see private campaign by Regional user (R1)");
			campaignCenterPage.searchCampaignByFilter(driver, secondcampaignNamecorp);
			
			boolean isCampaignNamePresentCorpaccessibilitypublictoallcorporateuserscorpR1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondcampaignNamecorp + "')]");
			if (!isCampaignNamePresentCorpaccessibilitypublictoallcorporateuserscorpR1 == false) {
				fc.utobj().throwsException("was not able to verify Public to all corporate users Campaign created by Corporate user who does not privilege to see private campaign by Regional user (R1)");
			}
			
			//Verify Private Campaign created by Corporate user who has privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Private Campaign created by Corporate user who does not have privilege to see private campaign by Regional user (R1)");
			campaignCenterPage.searchCampaignByFilter(driver, thirdcampaignNamecorp);
			
			boolean isCampaignNamePresentCorpaccessibilityprivatecorpR1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + thirdcampaignNamecorp + "')]");
			if (!isCampaignNamePresentCorpaccessibilityprivatecorpR1 == false) {
				fc.utobj().throwsException("was not able to Private Campaign created by Corporate user who does not have privilege to see private campaign by Regional user (R1)");
			}
		
			//Verify Public to all users (Status Driven)Campaign created by Corporate user who does not have privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Public to all users(Status Driven) Campaign created by Corporate user who does not have privilege to see private campaign by Regional user (R1)");
			campaignCenterPage.searchCampaignByFilter(driver, firstcampaignNamecorpstatus);

			boolean isCampaignNamePresentCorpaccessibilitypublictoalluserscorpstatusR1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firstcampaignNamecorpstatus + "')]");
			if (isCampaignNamePresentCorpaccessibilitypublictoalluserscorpstatusR1 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users(Status Driven) Campaign created by Corporate user who does not have privilege to see private campaign by Regional user (R1)");
			}
			//Verify Public to all corporate users Campaign created by Corporate user who has privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Public to all corporate users(Status Driven) Campaign created by Corporate user who does not have privilege to see private campaign by Regional user (R1)");
			campaignCenterPage.searchCampaignByFilter(driver, secondcampaignNamecorpstatus);
			
			boolean isCampaignNamePresentCorpaccessibilitypublictoallcorporateuserscorpstatusR1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondcampaignNamecorpstatus + "')]");
			if (!isCampaignNamePresentCorpaccessibilitypublictoallcorporateuserscorpstatusR1 == false) {
				fc.utobj().throwsException("was not able to verify Public to all corporate users(Status Driven) Campaign created by Corporate user who does not privilege to see private campaign by Regional user (R1)");
			}
			
			//Verify Private Campaign created by Corporate user who has privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Private Campaign created by Corporate user(Status Driven) who does not have privilege to see private campaign by Regional user (R1)");
			campaignCenterPage.searchCampaignByFilter(driver, thirdcampaignNamecorpstatus);
			
			boolean isCampaignNamePresentCorpaccessibilityprivatecorpstatusR1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + thirdcampaignNamecorpstatus + "')]");
			if (!isCampaignNamePresentCorpaccessibilityprivatecorpstatusR1 == false) {
				fc.utobj().throwsException("was not able to Private Campaign created by Corporate user(Status Driven) who does not have privilege to see private campaign by Regional user (R1)");
			}
				
			fc.utobj().printTestStep("Navigate to CRM>Campaign Center>email template");
			emailtemplate.clickonemailtamplate(driver);
			
			//Verify Public to all users template created by Corporate user who does not have privilege to see private template by itself
			fc.utobj().printTestStep("Verify Public to all users template created by Corporate user who does not have privilege to see private template by Regional user (R1)");
			campaignCenterPage.searchTemplateByFilter(driver, firsttemplateNamecorp);

			boolean isTemplateNamePresentCorpaccessibilitypublictoalluserscorpR1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firsttemplateNamecorp + "')]");
			if (isTemplateNamePresentCorpaccessibilitypublictoalluserscorpR1 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users template created by Corporate user who has privilege to see private template by Regional user (R1)");
			}
			//Verify Public to all corporate users template created by Corporate user who has privilege to see private template by itself
			fc.utobj().printTestStep("Verify Public to all corporate users template created by Corporate user who does not have privilege to see private template by Regional user (R1)");
			campaignCenterPage.searchTemplateByFilter(driver, secondtemplateNamecorp);
			
			boolean isTemplateNamePresentCorpaccessibilitypublictoallcorporateuserscorpR1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondtemplateNamecorp + "')]");
			if (!isTemplateNamePresentCorpaccessibilitypublictoallcorporateuserscorpR1 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users template created by Corporate user who has privilege to see private template by Regional user (R1)");
			}
			
			//Verify Private template created by Corporate user who has privilege to see private template by itself
			fc.utobj().printTestStep("Verify Privare template created by Corporate user who does not have privilege to see private template by Regional user (R1)");
			campaignCenterPage.searchTemplateByFilter(driver, thirdtemplateNamecorp);
			
			boolean isTemplateNamePresentCorpaccessibilityprivatecorpR1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + thirdtemplateNamecorp + "')]");
			if (!isTemplateNamePresentCorpaccessibilityprivatecorpR1 == false) {
				fc.utobj().throwsException("was not able to verify private template created by Corporate user who has privilege to see private template by Regional user (R1)");
			}
			
			//Verify Public to all users (Status driven)template created by Corporate user who does not have privilege to see private template by itself
			fc.utobj().printTestStep("Verify Public to all users (Status driven)template created by Corporate user who does not have privilege to see private template by Regional user (R1)");
			campaignCenterPage.searchTemplateByFilter(driver, firsttemplateNamecorpstatus);

			boolean isTemplateNamePresentCorpaccessibilitypublictoalluserscorpstatusR1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firsttemplateNamecorpstatus + "')]");
			if (isTemplateNamePresentCorpaccessibilitypublictoalluserscorpstatusR1 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users (Status driven)template created by Corporate user who has privilege to see private template by Regional user (R1)");
			}
			//Verify Public to all corporate users template created by Corporate user who has privilege to see private template by itself
			fc.utobj().printTestStep("Verify Public to all corporate users (Status driven)template created by Corporate user who does not have privilege to see private template by Regional user (R1)");
			campaignCenterPage.searchTemplateByFilter(driver, secondtemplateNamecorpstatus);
			
			boolean isTemplateNamePresentCorpaccessibilitypublictoallcorporateuserscorpstatusR1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondtemplateNamecorpstatus + "')]");
			if (!isTemplateNamePresentCorpaccessibilitypublictoallcorporateuserscorpstatusR1 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users (Status driven)template created by Corporate user who has privilege to see private template by Regional user (R1)");
			}
			
			//Verify Private template created by Corporate user who has privilege to see private template by Regional user(R1)
			fc.utobj().printTestStep("Verify Privare (Status driven)template created by Corporate user who does not have privilege to see private template by Regional user (R1)");
			campaignCenterPage.searchTemplateByFilter(driver, thirdtemplateNamecorpstatus);
			
			boolean isTemplateNamePresentCorpaccessibilityprivatecorpstatusR1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + thirdtemplateNamecorpstatus + "')]");
			if (!isTemplateNamePresentCorpaccessibilityprivatecorpstatusR1 == false) {
				fc.utobj().throwsException("was not able to verify private (Status driven)template created by Corporate user who has privilege to see private template by Regional user (R1)");
			}
		
			fc.utobj().printTestStep("Navigate to CRM>Campaign Center>campaign template");
			emailtemplate.clickoncampaigntemplate(driver);
			//Verify Public to all users of my region Campaign created by Regional user who has privilege to see private campaign by itself(R1)
			//Verifying Campaigns and templates of Regional user (R1) by itself
			fc.utobj().printTestStep("Verifying Campaigns and templates of Regional user (R1) by itself");
			fc.utobj().printTestStep("Verify Public to all users of my region Campaign created by Regional user who has privilege to see private campaign by itself");
			campaignCenterPage.searchCampaignByFilter(driver, firstcampaignNamereg);

			boolean isCampaignNamePresentRegpaccessibilitypublictoallusersofmyregionR1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firstcampaignNamereg + "')]");
			if (isCampaignNamePresentRegpaccessibilitypublictoallusersofmyregionR1 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my region Campaign created by Regional user who has privilege to see private campaign by itself");
			}
			
			//Verify Private Campaign created by Regional user who has privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Private Campaign created by Regional user who has privilege to see private campaign by itself");
			campaignCenterPage.searchCampaignByFilter(driver, secondcampaignNamereg);
			
			boolean isCampaignNamePresentRegpaccessibilityprivateR1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondcampaignNamereg + "')]");
			if (isCampaignNamePresentRegpaccessibilityprivateR1 == false) {
				fc.utobj().throwsException("was not able to verify Private Campaign created by Regional user who has privilege to see private campaign by itself");
			}
			
			//Verify Public to all users of my region (Status Driven)Campaign created by Regional user who has privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Public to all users of my region (Status Driven)Campaign created by Regional user who has privilege to see private campaign by itself");
			campaignCenterPage.searchCampaignByFilter(driver, firstcampaignNameregstatus);

			boolean isCampaignNamePresentRegpaccessibilitypublictoallusersofmyregionstatusR1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firstcampaignNameregstatus + "')]");
			if (isCampaignNamePresentRegpaccessibilitypublictoallusersofmyregionstatusR1 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my region (Status Driven)Campaign created by Regional user who has privilege to see private campaign by itself");
			}
			
			//Verify Private Campaign created by Regional user who has privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Private (Status Driven)Campaign created by Regional user who has privilege to see private campaign by itself");
			campaignCenterPage.searchCampaignByFilter(driver, secondcampaignNameregstatus);
			
			boolean isCampaignNamePresentRegpaccessibilityprivatestatusR1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondcampaignNameregstatus + "')]");
			if (isCampaignNamePresentRegpaccessibilityprivatestatusR1 == false) {
				fc.utobj().throwsException("was not able to verify Private (Status Driven)Campaign created by Regional user who has privilege to see private campaign by itself");
			}
			
			fc.utobj().printTestStep("Navigate to CRM>Campaign Center>email template");
			emailtemplate.clickonemailtamplate(driver);
			
			//Verify Public to all users template created by Corporate user who has privilege to see private template by itself
			fc.utobj().printTestStep("Verify Public to all users of my region template created by Regional user who has privilege to see private template by itself");
			campaignCenterPage.searchTemplateByFilter(driver, firsttemplateNamereg);

			boolean isTemplateNamePresentRegaccessibilitypublictoallusersofmyregionR1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firsttemplateNamereg + "')]");
			if (isTemplateNamePresentRegaccessibilitypublictoallusersofmyregionR1 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users template created by Regional user who has privilege to see private template by itself");
			}
			
			//Verify Private template created by Regional user who has privilege to see private template by itself
			fc.utobj().printTestStep("Verify Private template created by Regional user who has privilege to see private template by itself");
			campaignCenterPage.searchTemplateByFilter(driver, secondtemplateNamereg);
			
			boolean isTemplateNamePresentRegaccessibilityprivateR1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondtemplateNamereg + "')]");
			if (isTemplateNamePresentRegaccessibilityprivateR1 == false) {
				fc.utobj().throwsException("was not able to verify private template created by Regional user who has privilege to see private template by itself");
			}
			
			//Verify Public to all users(Status Driven) template created by Regional user who has privilege to see private template by itself
			fc.utobj().printTestStep("Verify Public to all users of my region (Status Driven)template created by Regional user who has privilege to see private template by itself");
			campaignCenterPage.searchTemplateByFilter(driver, firsttemplateNameregstatus);

			boolean isTemplateNamePresentRegaccessibilitypublictoallusersofmyregionstatusR1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firsttemplateNameregstatus + "')]");
			if (isTemplateNamePresentRegaccessibilitypublictoallusersofmyregionstatusR1 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my region (Status Driven)template created by Regional user who has privilege to see private template by itself");
			}
			
			//Verify Private template created by Regional user who has privilege to see private template by itself
			fc.utobj().printTestStep("Verify Private (Status Driven)template created by Regional user who has privilege to see private template by itself");
			campaignCenterPage.searchTemplateByFilter(driver, secondtemplateNameregstatus);
			
			boolean isTemplateNamePresentRegaccessibilityprivatestatusR1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondtemplateNameregstatus + "')]");
			if (isTemplateNamePresentRegaccessibilityprivatestatusR1 == false) {
				fc.utobj().throwsException("was not able to verify private (Status Driven)template created by Regional user who has privilege to see private template by itself");
			}
			fc.utobj().printTestStep("Navigate to CRM>Campaign Center>campaign template");
			emailtemplate.clickoncampaigntemplate(driver);
			
			//Verify Public to all users Campaign created by Corporate user who has privilege to see private campaign by Regional user (R1
			fc.utobj().printTestStep("Verifying Campaigns and templates of Corporate user (C1) by Regional user (R1");
			fc.utobj().printTestStep("Verify Public to all users Campaign created by Corporate user who has privilege to see private campaign by Regional user (R1)");
			campaignCenterPage.searchCampaignByFilter(driver, firstcampaignName);

			boolean isCampaignNamePresentCorpaccessibilitypublictoallusersR1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firstcampaignName + "')]");
			if (isCampaignNamePresentCorpaccessibilitypublictoallusersR1 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users Campaign created by Corporate user who has privilege to see private campaign by Regional user (R1)");
			}
			//Verify Public to all corporate users Campaign created by Corporate user who has privilege to see private campaign by Regional user (R1)
			fc.utobj().printTestStep("Verify Public to all corporate users Campaign created by Corporate user who has privilege to see private campaign");
			campaignCenterPage.searchCampaignByFilter(driver, secondcampaignName);
			
			boolean isCampaignNamePresentCorpaccessibilitypublictoallcorporateusersR1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondcampaignName + "')]");
			if (!isCampaignNamePresentCorpaccessibilitypublictoallcorporateusersR1 == false) {
				fc.utobj().throwsException("was not able to verify Public to all corporate users Campaign created by Corporate user who has privilege to see private campaign by Regional user (R1)");
			}
			
			//Verify Private Campaign created by Corporate user who has privilege to see private campaign by Regional user (R1
			fc.utobj().printTestStep("Verify Private Campaign created by Corporate user who has privilege to see private campaign by Regional user (R1)");
			campaignCenterPage.searchCampaignByFilter(driver, thirdcampaignName);
			
			boolean isCampaignNamePresentCorpaccessibilityprivateR1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + thirdcampaignName + "')]");
			if (!isCampaignNamePresentCorpaccessibilityprivateR1 == false) {
				fc.utobj().throwsException("was not able to Private Campaign created by Corporate user who has privilege to see private campaign by Regional user (R1)");
			}
			
			//Verify Public to all users(Status Driven) Campaign created by Corporate user who has privilege to see private campaign by Regional user (R1)
			fc.utobj().printTestStep("Verify Public to all users(Status Driven) Campaign created by Corporate user who has privilege to see private campaign by Regional user (R1)");
			campaignCenterPage.searchCampaignByFilter(driver, firstcampaignNamestatus);

			boolean isCampaignNamePresentCorpaccessibilitypublictoallusersstatusR1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firstcampaignNamestatus + "')]");
			if (isCampaignNamePresentCorpaccessibilitypublictoallusersstatusR1 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users(Status Driven) Campaign created by Corporate user who has privilege to see private campaign by Regional user (R1)");
			}
			//Verify Public to all corporate users(Status Driven) Campaign created by Corporate user who has privilege to see private campaign by Regional user (R1)
			fc.utobj().printTestStep("Verify Public to all corporate users(Status Driven) Campaign created by Corporate user who has privilege to see private campaign");
			campaignCenterPage.searchCampaignByFilter(driver, secondcampaignNamestatus);
			
			boolean isCampaignNamePresentCorpaccessibilitypublictoallcorporateusersstatusR1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondcampaignNamestatus + "')]");
			if (!isCampaignNamePresentCorpaccessibilitypublictoallcorporateusersstatusR1 == false) {
				fc.utobj().throwsException("was not able to verify Public to all corporate users(Status Driven) Campaign created by Corporate user who has privilege to see private campaign by Regional user (R1)");
			}
			
			//Verify Private Campaign created by Corporate user(Status Driven) who has privilege to see private campaign by Regional user (R1)
			fc.utobj().printTestStep("Verify Private Campaign(Status Driven) created by Corporate user who has privilege to see private campaign by Regional user (R1)");
			campaignCenterPage.searchCampaignByFilter(driver, thirdcampaignNamestatus);
			
			boolean isCampaignNamePresentCorpaccessibilityprivatestatusR1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + thirdcampaignNamestatus + "')]");
			if (!isCampaignNamePresentCorpaccessibilityprivatestatusR1 == false) {
				fc.utobj().throwsException("was not able to Private Campaign(Status Driven) created by Corporate user who has privilege to see private campaign by Regional user (R1)");
			}
		
			fc.utobj().printTestStep("Navigate to CRM>Campaign Center>email template");
			emailtemplate.clickonemailtamplate(driver);
			
			//Verify Public to all users template created by Corporate user who has privilege to see private template by Regional user (R1)
			fc.utobj().printTestStep("Verify Public to all users template created by Corporate user who has privilege to see private template by Regional user (R1)");
			campaignCenterPage.searchTemplateByFilter(driver, firsttemplateName);

			boolean isTemplateNamePresentCorpaccessibilitypublictoallusersR1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firsttemplateName + "')]");
			if (isTemplateNamePresentCorpaccessibilitypublictoallusersR1 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users template created by Corporate user who has privilege to see private template by Regional user (R1)");
			}
			//Verify Public to all corporate users template created by Corporate user who has privilege to see private template by Regional user (R1)
			fc.utobj().printTestStep("Verify Public to all corporate users template created by Corporate user who has privilege to see private template by Regional user (R1)");
			campaignCenterPage.searchTemplateByFilter(driver, secondtemplateName);
			
			boolean isTemplateNamePresentCorpaccessibilitypublictoallcorporateusersR1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondtemplateName + "')]");
			if (!isTemplateNamePresentCorpaccessibilitypublictoallcorporateusersR1 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users template created by Corporate user who has privilege to see private template by Regional user (R1)");
			}
			
			//Verify Private template created by Corporate user who has privilege to see private template by Regional user (R1)
			fc.utobj().printTestStep("Verify Privare template created by Corporate user who has privilege to see private template by Regional user (R1)");
			campaignCenterPage.searchTemplateByFilter(driver, thirdtemplateName);
			
			boolean isTemplateNamePresentCorpaccessibilityprivateR1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + thirdtemplateName + "')]");
			if (!isTemplateNamePresentCorpaccessibilityprivateR1 == false) {
				fc.utobj().throwsException("was not able to verify private template created by Corporate user who has privilege to see private template by Regional user (R1");
			}
				
			//Verify Public to all users template(Status Driven) created by Corporate user who has privilege to see private template by Regional user (R1)
			fc.utobj().printTestStep("Verify Public to all users template(Status Driven) created by Corporate user who has privilege to see private template by Regional user (R1)");
			campaignCenterPage.searchTemplateByFilter(driver, firsttemplateNamestatus);

			boolean isTemplateNamePresentCorpaccessibilitypublictoallusersstatusR1= fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firsttemplateNamestatus + "')]");
			if (isTemplateNamePresentCorpaccessibilitypublictoallusersstatusR1 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users template(Status Driven) created by Corporate user who has privilege to see private template by Regional user (R1)");
			}
			//Verify Public to all corporate users template(Status Driven) created by Corporate user who has privilege to see private template by Regional user (R1)
			fc.utobj().printTestStep("Verify Public to all corporate users template(Status Driven) created by Corporate user who has privilege to see private template by Regional user (R1)");
			campaignCenterPage.searchTemplateByFilter(driver, secondtemplateNamestatus);
			
			boolean isTemplateNamePresentCorpaccessibilitypublictoallcorporateusersstatusR1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondtemplateNamestatus + "')]");
			if (!isTemplateNamePresentCorpaccessibilitypublictoallcorporateusersstatusR1 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users template(Status Driven) created by Corporate user who has privilege to see private template by Regional user (R1)");
			}
			
			//Verify Private template(Status Driven) created by Corporate user who has privilege to see private template by Regional user (R1)
			fc.utobj().printTestStep("Verify Privare template(Status Driven) created by Corporate user who has privilege to see private template by Regional user (R1)");
			campaignCenterPage.searchTemplateByFilter(driver, thirdtemplateNamestatus);
			
			boolean isTemplateNamePresentCorpaccessibilityprivatestatusR1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + thirdtemplateNamestatus + "')]");
			if (!isTemplateNamePresentCorpaccessibilityprivatestatusR1 == false) {
				fc.utobj().throwsException("was not able to verify private template(Status Driven) created by Corporate user who has privilege to see private template by Regional user (R1)");
			}
		
			fc.utobj().printTestStep("Navigate to CRM>Campaign Center>campaign template");
			emailtemplate.clickoncampaigntemplate(driver);
			
			//Verify Public to all users Campaign created by Corporate user who has privilege to see private campaign by Regional user (R1)
			fc.utobj().printTestStep("Verifying Campaigns and templates of Regional user (R2) by Regional user (R1");
			//Verify Public to all users of my region Campaign created by Regional user who does not have privilege to see private campaign by Regional user (R1)
			fc.utobj().printTestStep("Verify Public to all users of my region Campaign created by Regional user who does not have privilege to see private campaign by Regional user (R1");
			campaignCenterPage.searchCampaignByFilter(driver, firstcampaignNamereg1);

			boolean isCampaignNamePresentRegpaccessibilitypublictoallusersofmyregion1R1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firstcampaignNamereg1 + "')]");
			if (isCampaignNamePresentRegpaccessibilitypublictoallusersofmyregion1R1 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my region Campaign created by Regional user who does not have privilege to see private campaign by Regional user (R1");
			}
			
			//Verify Private Campaign created by Regional user who does not have privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Private Campaign created by Regional user who does not have privilege to see private campaign by Regional user (R1");
			campaignCenterPage.searchCampaignByFilter(driver, secondcampaignNamereg1);
			
			boolean isCampaignNamePresentRegpaccessibilityprivate1R1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondcampaignNamereg1 + "')]");
			if (isCampaignNamePresentRegpaccessibilityprivate1R1 == false) {
				fc.utobj().throwsException("was not able to verify Private Campaign created by Regional user who does not have privilege to see private campaign by Regional user (R1");
			}
			
			//Verify Public to all users of my region (Status Driven)Campaign created by Regional user who does not have privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Public to all users of my region (Status Driven)Campaign created by Regional user who does not have privilege to see private campaign by Regional user (R1");
			campaignCenterPage.searchCampaignByFilter(driver, firstcampaignNamereg1status);

			boolean isCampaignNamePresentRegpaccessibilitypublictoallusersofmyregion1statusR1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firstcampaignNamereg1status + "')]");
			if (isCampaignNamePresentRegpaccessibilitypublictoallusersofmyregion1statusR1 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my region (Status Driven)Campaign created by Regional user who does not have privilege to see private campaign by Regional user (R1");
			}
			
			//Verify Private (Status Driven)Campaign created by Regional user who does not have privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Private (Status Driven)Campaign created by Regional user who does not have privilege to see private campaign by Regional user (R1");
			campaignCenterPage.searchCampaignByFilter(driver, secondcampaignNamereg1status);
			
			boolean isCampaignNamePresentRegpaccessibilityprivate1statusR1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondcampaignNamereg1status + "')]");
			if (isCampaignNamePresentRegpaccessibilityprivate1statusR1 == false) {
				fc.utobj().throwsException("was not able to verify Private (Status Driven)Campaign created by Regional user who does not have privilege to see private campaign by Regional user (R1");
			}
		
			fc.utobj().printTestStep("Navigate to CRM>Campaign Center>email template");
			emailtemplate.clickonemailtamplate(driver);
			
			//Verify Public to all users template created by Corporate user who does not have privilege to see private template by itself
			fc.utobj().printTestStep("Verify Public to all users of my region template created by Regional user who does not have privilege to see private template by Regional user (R1");
			campaignCenterPage.searchTemplateByFilter(driver, firsttemplateNamereg1);

			boolean isTemplateNamePresentRegaccessibilitypublictoallusersofmyregion1R1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firsttemplateNamereg1 + "')]");
			if (isTemplateNamePresentRegaccessibilitypublictoallusersofmyregion1R1 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my region template created by Regional user who does not have privilege to see private template by Regional user (R1");
			}
			
			//Verify Private template created by Regional user who does not have privilege to see private template by itself
			fc.utobj().printTestStep("Verify Private template created by Regional user does not have privilege to see private template by Regional user (R1");
			campaignCenterPage.searchTemplateByFilter(driver, secondtemplateNamereg1);
			
			boolean isTemplateNamePresentRegaccessibilityprivate1R1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondtemplateNamereg1 + "')]");
			if (isTemplateNamePresentRegaccessibilityprivate1R1 == false) {
				fc.utobj().throwsException("was not able to verify Private template created by Regional user does not have privilege to see private template by Regional user (R1");
			}
			
			//Verify Public to all users (Status Driven)template created by Regional user who does not have privilege to see private template by itself
			fc.utobj().printTestStep("Verify Public to all users of my region (Status Driven)template created by Regional user who does not have privilege to see private template by Regional user (R1");
			campaignCenterPage.searchTemplateByFilter(driver, firsttemplateNamereg1status);

			boolean isTemplateNamePresentRegaccessibilitypublictoallusersofmyregion1statusR1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firsttemplateNamereg1status + "')]");
			if (isTemplateNamePresentRegaccessibilitypublictoallusersofmyregion1statusR1 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my region (Status Driven)template created by Regional user who does not have privilege to see private template by Regional user (R1");
			}
			
			//Verify Private template created by Regional user who does not have privilege to see private template by itself
			fc.utobj().printTestStep("Verify Private (Status Driven)template created by Regional user does not have privilege to see private template by Regional user (R1");
			campaignCenterPage.searchTemplateByFilter(driver, secondtemplateNamereg1status);
			
			boolean isTemplateNamePresentRegaccessibilityprivate1statusR1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondtemplateNamereg1status + "')]");
			if (isTemplateNamePresentRegaccessibilityprivate1statusR1 == false) {
				fc.utobj().throwsException("was not able to verify Private (Status Driven)template created by Regional user does not have privilege to see private template by Regional user (R1");
			}
			
			fc.utobj().printTestStep("Navigate to CRM>Campaign Center>campaign template");
			emailtemplate.clickoncampaigntemplate(driver);
			
			fc.utobj().clickElement(driver, crmcenter.locationCampaign);
			fc.utobj().printTestStep("Verifying Campaigns and templates of Franchise owner user (F0New) by Regional user (R1");
			fc.utobj().printTestStep("Verify Public to all users of my Franchise Campaign created by Franchise user who has privilege to see private campaign by Regional user (R1");
			campaignCenterPage.searchCampaignByFilter(driver, firstcampaignNamefran);

			boolean isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchiseR1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firstcampaignNamefran + "')]");
			if (isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchiseR1 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my Franchise Campaign created by Franchise user who has privilege to see private campaign by Regional user (R1");
			}
			
			//Verify Private Campaign created by Franchise user who has privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Private Campaign created by Franchise user who has privilege to see private campaign by Regional user (R1");
			campaignCenterPage.searchCampaignByFilter(driver, secondcampaignNamefran);
			
			boolean isCampaignNamePresentFranpaccessibilityprivateR1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondcampaignNamefran + "')]");
			if (isCampaignNamePresentFranpaccessibilityprivateR1 == false) {
				fc.utobj().throwsException("was not able to verify Private Campaign created by Franchise user who has privilege to see private campaign by Regional user (R1");
			}
			
			
			//Verify Public to all users of my Franchise (Status Driven)Campaign created by Franchise user who has privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Public to all users of my Franchise (Status Driven)Campaign created by Franchise user who has privilege to see private campaign by Regional user (R1");
			campaignCenterPage.searchCampaignByFilter(driver, firstcampaignNamefranstatus);

			boolean isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchisestatusR1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firstcampaignNamefranstatus + "')]");
			if (isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchisestatusR1 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my Franchise (Status Driven)Campaign created by Franchise user who has privilege to see private campaign by Regional user (R1");
			}
			
			//Verify Private Campaign created by Franchise user who has privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Private (Status Driven)Campaign created by Franchise user who has privilege to see private campaign by Regional user (R1");
			campaignCenterPage.searchCampaignByFilter(driver, secondcampaignNamefranstatus);
			
			boolean isCampaignNamePresentFranpaccessibilityprivatestatusR1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondcampaignNamefranstatus + "')]");
			if (isCampaignNamePresentFranpaccessibilityprivatestatusR1 == false) {
				fc.utobj().throwsException("was not able to verify Private (Status Driven)Campaign created by Franchise user who has privilege to see private campaign by Regional user (R1");
			}
			
			fc.utobj().printTestStep("Navigate to CRM>Campaign Center>email template");
			emailtemplate.clickonemailtamplate(driver);
			fc.utobj().clickElement(driver, crmcenter.locationtemplate);
			//Verify Public to all users template created by Corporate user who has privilege to see private template by itself
			fc.utobj().printTestStep("Verify Public to all users of my franchise template created by franchise user who has privilege to see private template by Regional user (R1");
			campaignCenterPage.searchTemplateByFilter(driver, firsttemplateNamefran);

			boolean isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchiseR1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firsttemplateNamefran + "')]");
			if (isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchiseR1 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my franchise template created by franchise user who has privilege to see private template by Regional user (R1");
			}
			
			//Verify Private template created by franchise user who has privilege to see private template by itself
			fc.utobj().printTestStep("Verify Private template created by franchise user who has privilege to see private template by Regional user (R1");
			campaignCenterPage.searchTemplateByFilter(driver, secondtemplateNamefran);
			
			boolean isTemplateNamePresentfranaccessibilityprivateR1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondtemplateNamefran + "')]");
			if (isTemplateNamePresentfranaccessibilityprivateR1 == false) {
				fc.utobj().throwsException("was not able to verify private template created by franchise user who has privilege to see private template by corporate user(R1)");
			}
			
			//Verify Public to all users (Status Driven)template created by Corporate user who has privilege to see private template by itself
			fc.utobj().printTestStep("Verify Public to all users of my franchise (Status Driven)template created by franchise user who has privilege to see private template by Regional user (R1");
			campaignCenterPage.searchTemplateByFilter(driver, firsttemplateNamefranstatus);

			boolean isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchisestatusR1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firsttemplateNamefranstatus + "')]");
			if (isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchisestatusR1 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my franchise (Status Driven)template created by franchise user who has privilege to see private template by Regional user (R1");
			}
			
			//Verify Private template created by franchise user who has privilege to see private template by itself
			fc.utobj().printTestStep("Verify Private (Status Driven)template created by franchise user who has privilege to see private template by Regional user (R1");
			campaignCenterPage.searchTemplateByFilter(driver, secondtemplateNamefranstatus);
			
			boolean isTemplateNamePresentfranaccessibilityprivatestatusR1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondtemplateNamefranstatus + "')]");
			if (isTemplateNamePresentfranaccessibilityprivatestatusR1 == false) {
				fc.utobj().throwsException("was not able to verify private (Status Driven)template created by franchise user who has privilege to see private template by Regional user (R1");
			}
		
			fc.utobj().printTestStep("Navigate to CRM>Campaign Center>campaign template");
			emailtemplate.clickoncampaigntemplate(driver);
			fc.utobj().clickElement(driver, crmcenter.locationCampaign);
			fc.utobj().printTestStep("Verifying Campaigns and templates of Franchise Mu user (V1) by Regional user (R1)");
			
			fc.utobj().printTestStep("Verify Public to all users of my Franchise Campaign created by Franchise MU user(V1) who has privilege to see private campaign by Regional user (R1)");
			campaignCenterPage.searchCampaignByFilter(driver, firstcampaignNamefranmu);

			boolean isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchisemuR1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firstcampaignNamefranmu + "')]");
			if (isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchisemuR1 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my Franchise Campaign created by Franchise MU user(V1) who has privilege to see private campaign by Regional user (R1)");
			}
			
			//Verify Private Campaign created by Franchise user who has privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Private Campaign created by Franchise MU user(V1) who has privilege to see private campaign by Regional user (R1)");
			campaignCenterPage.searchCampaignByFilter(driver, secondcampaignNamefranmu);
			
			boolean isCampaignNamePresentFranpaccessibilityprivatemuR1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondcampaignNamefranmu + "')]");
			if (isCampaignNamePresentFranpaccessibilityprivatemuR1 == false) {
				fc.utobj().throwsException("was not able to verify Private Campaign created by Franchise MU user(V1) who has privilege to see private campaign by Regional user (R1)");
			}
			
			
			//Verify Public to all users of my Franchise (Status Driven)Campaign created by Franchise user who has privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Public to all users of my Franchise (Status Driven)Campaign created by Franchise MU user(V1) who has privilege to see private campaign by Regional user (R1)");
			campaignCenterPage.searchCampaignByFilter(driver, firstcampaignNamefranmustatus);

			boolean isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchisemustatusR1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firstcampaignNamefranmustatus + "')]");
			if (isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchisemustatusR1 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my Franchise (Status Driven)Campaign created by Franchise MU user(V1) who has privilege to see private campaign by corporate user(R1)");
			}
			
			//Verify Private Campaign created by Franchise user who has privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Private (Status Driven)Campaign created by Franchise MU user(V1) who has privilege to see private campaign by Regional user (R1)");
			campaignCenterPage.searchCampaignByFilter(driver, secondcampaignNamefranmustatus);
			
			boolean isCampaignNamePresentFranpaccessibilityprivatemustatusR1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondcampaignNamefranmustatus + "')]");
			if (isCampaignNamePresentFranpaccessibilityprivatemustatusR1 == false) {
				fc.utobj().throwsException("was not able to verify Private (Status Driven)Campaign created by Franchise MU user(V1) who has privilege to see private campaign by Regional user (R1)");
			}
			
			fc.utobj().printTestStep("Navigate to CRM>Campaign Center>email template");
			emailtemplate.clickonemailtamplate(driver);
			fc.utobj().clickElement(driver, crmcenter.locationtemplate);
			//Verify Public to all users template created by Corporate user who has privilege to see private template by itself
			fc.utobj().printTestStep("Verify Public to all users of my franchise template created by franchise MU user(V1) who has privilege to see private template by Regional user (R1)");
			campaignCenterPage.searchTemplateByFilter(driver, firsttemplateNamefranmu);

			boolean isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchisemuR1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firsttemplateNamefranmu + "')]");
			if (isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchisemuR1 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my franchise template created by franchise MU user(V1) who has privilege to see private template by Regional user (R1)");
			}
			
			//Verify Private template created by franchise user who has privilege to see private template by itself
			fc.utobj().printTestStep("Verify Private template created by franchise MU user(V1) who has privilege to see private template by Regional user (R1)");
			campaignCenterPage.searchTemplateByFilter(driver, secondtemplateNamefranmu);
			
			boolean isTemplateNamePresentfranaccessibilityprivatemuR1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondtemplateNamefranmu + "')]");
			if (isTemplateNamePresentfranaccessibilityprivatemuR1 == false) {
				fc.utobj().throwsException("was not able to verify private template created by franchise MU user(V1) who has privilege to see private template by Regional user (R1)");
			}
			
			//Verify Public to all users template created by Corporate user who has privilege to see private template by itself
			fc.utobj().printTestStep("Verify Public to all users of my franchise (Status Driven)template created by franchise MU user(V1) who has privilege to see private template by Regional user (R1)");
			campaignCenterPage.searchTemplateByFilter(driver, firsttemplateNamefranmustatus);

			boolean isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchisemustatusR1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firsttemplateNamefranmustatus + "')]");
			if (isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchisemustatusR1 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my franchise (Status Driven)template created by franchise MU user(V1) who has privilege to see private template by Regional user (R1)");
			}
			
			//Verify Private template created by franchise user who has privilege to see private template by itself
			fc.utobj().printTestStep("Verify Private (Status Driven)template created by franchise MU user(V1) who has privilege to see private template by Regional user (R1)");
			campaignCenterPage.searchTemplateByFilter(driver, secondtemplateNamefranmustatus);
			
			boolean isTemplateNamePresentfranaccessibilityprivatemustatusR1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondtemplateNamefranmustatus + "')]");
			if (isTemplateNamePresentfranaccessibilityprivatemustatusR1 == false) {
				fc.utobj().throwsException("was not able to verify private (Status Driven)template created by franchise MU user(V1) who has privilege to see private template by Regional user (R1)");
			}
			
			
			
			
			fc.utobj().printTestStep("Navigate to CRM>Campaign Center>campaign template");
			emailtemplate.clickoncampaigntemplate(driver);
			fc.utobj().clickElement(driver, crmcenter.locationCampaign);
			fc.utobj().printTestStep("Verifying Campaigns and templates of Franchise Employee user (V2) by Regional user (R1)");
			fc.utobj().printTestStep("Verify Public to all users of my Franchise Campaign created by Franchise Employee user(V2) who does not have privilege to see private campaign by Regional user (R1)");
			campaignCenterPage.searchCampaignByFilter(driver, firstcampaignNamefranmue2);

			boolean isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchisemue2R1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firstcampaignNamefranmue2 + "')]");
			if (isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchisemue2R1 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my Franchise Campaign created by Franchise Employee user(V2) who does not have privilege to see private campaign by Regional user (R1)");
			}
			
			//Verify Private Campaign created by Franchise user who has privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Private Campaign created by Franchise Employee user(V2) who does not have privilege to see private campaign by Regional user (R1)");
			campaignCenterPage.searchCampaignByFilter(driver, secondcampaignNamefranmue2);
			
			boolean isCampaignNamePresentFranpaccessibilityprivatemue2R1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondcampaignNamefranmue2 + "')]");
			if (isCampaignNamePresentFranpaccessibilityprivatemue2R1 == false) {
				fc.utobj().throwsException("was not able to verify Private Campaign created by Franchise Employee user(V2) who does not have privilege to see private campaign by Regional user (R1)");
			}
			
			
			//Verify Public to all users of my Franchise Campaign created by Franchise Employee user(V2) who has privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Public to all users of my Franchise (Status Driven)Campaign created by Franchise Employee user(V2) who does not have privilege to see private campaign by Regional user (R1)");
			campaignCenterPage.searchCampaignByFilter(driver, firstcampaignNamefranmue2status);

			boolean isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchisemue2statusR1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firstcampaignNamefranmue2status + "')]");
			if (isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchisemue2statusR1 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my Franchise (Status Driven)Campaign created by Franchise Employee user(V2) who does not have privilege to see private campaign by Regional user (R1)");
			}
			
			//Verify Private Campaign created by Franchise user who has privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Private (Status Driven)Campaign created by Franchise Employee user(V2) who does not have privilege to see private campaign by Regional user (R1)");
			campaignCenterPage.searchCampaignByFilter(driver, secondcampaignNamefranmue2status);
			
			boolean isCampaignNamePresentFranpaccessibilityprivatemue2statusR1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondcampaignNamefranmue2status + "')]");
			if (isCampaignNamePresentFranpaccessibilityprivatemue2statusR1 == false) {
				fc.utobj().throwsException("was not able to verify Private (Status Driven)Campaign created by Franchise Employee user(V2) who does not have privilege to see private campaign by Regional user (R1)");
			}
			
			fc.utobj().printTestStep("Navigate to CRM>Campaign Center>email template");
			emailtemplate.clickonemailtamplate(driver);
			fc.utobj().clickElement(driver, crmcenter.locationtemplate);
			//Verify Public to all users template created by Franchise user who has privilege to see private template by itself
			fc.utobj().printTestStep("Verify Public to all users of my franchise template created by franchise Employee user(V2) who does not have privilege to see private template by Regional user (R1)");
			campaignCenterPage.searchTemplateByFilter(driver, firsttemplateNamefranmue2);

			boolean isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchisemue2R1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firsttemplateNamefranmue2 + "')]");
			if (isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchisemue2R1 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my franchise template created by franchise Employee user(V2) who has privilege to see private template by Regional user (R1)");
			}
			
			//Verify Private template created by franchise user who has privilege to see private template by itself
			fc.utobj().printTestStep("Verify Private template created by franchise Employee user(V2) who has privilege to see private template by Regional user (R1)");
			campaignCenterPage.searchTemplateByFilter(driver, secondtemplateNamefranmue2);
			
			boolean isTemplateNamePresentfranaccessibilityprivatemue2R1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondtemplateNamefranmue2 + "')]");
			if (isTemplateNamePresentfranaccessibilityprivatemue2R1 == false) {
				fc.utobj().throwsException("was not able to verify private template created by franchise Employee user(V2) who has privilege to see private template by Regional user (R1)");
			}
			
			//Verify Public to all users template created by Franchise user who has privilege to see private template by itself
			fc.utobj().printTestStep("Verify Public to all users of my franchise (Status Driven)template created by franchise Employee user(V2) who does not have privilege to see private template by Regional user (R1)");
			campaignCenterPage.searchTemplateByFilter(driver, firsttemplateNamefranmue2status);

			boolean isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchisemue2statusR1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firsttemplateNamefranmue2status + "')]");
			if (isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchisemue2statusR1 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my franchise (Status Driven)template created by franchise Employee user(V2) who has privilege to see private template by Regional user (R1)");
			}
			
			//Verify Private template created by franchise user who has privilege to see private template by itself
			fc.utobj().printTestStep("Verify Private (Status Driven)template created by franchise Employee user(V2) who has privilege to see private template by Regional user (R1)");
			campaignCenterPage.searchTemplateByFilter(driver, secondtemplateNamefranmue2status);
			
			boolean isTemplateNamePresentfranaccessibilityprivatemue2statusR1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondtemplateNamefranmue2status + "')]");
			if (isTemplateNamePresentfranaccessibilityprivatemue2statusR1 == false) {
				fc.utobj().throwsException("was not able to verify private (Status Driven)template created by franchise Employee user(V2) who has privilege to see private template by Regional user (R1)");
			}
			
			if(isCampaignNamePresentCorpaccessibilitypublictoalluserscorpR1 && !isCampaignNamePresentCorpaccessibilitypublictoallcorporateuserscorpR1 && !isCampaignNamePresentCorpaccessibilityprivatecorpR1 
					&& isCampaignNamePresentCorpaccessibilitypublictoalluserscorpstatusR1 && !isCampaignNamePresentCorpaccessibilitypublictoallcorporateuserscorpstatusR1 && !isCampaignNamePresentCorpaccessibilityprivatecorpstatusR1 
					&& isTemplateNamePresentCorpaccessibilitypublictoalluserscorpR1 && !isTemplateNamePresentCorpaccessibilitypublictoallcorporateuserscorpR1 && !isTemplateNamePresentCorpaccessibilityprivatecorpR1 
					&& isTemplateNamePresentCorpaccessibilitypublictoalluserscorpstatusR1 && !isTemplateNamePresentCorpaccessibilitypublictoallcorporateuserscorpstatusR1 && !isTemplateNamePresentCorpaccessibilityprivatecorpstatusR1 
					&& isCampaignNamePresentRegpaccessibilitypublictoallusersofmyregionR1 && isCampaignNamePresentRegpaccessibilityprivateR1 && isCampaignNamePresentRegpaccessibilitypublictoallusersofmyregionstatusR1 
					&& isCampaignNamePresentRegpaccessibilityprivatestatusR1 && isTemplateNamePresentRegaccessibilitypublictoallusersofmyregionR1 && isTemplateNamePresentRegaccessibilityprivateR1 
					&& isTemplateNamePresentRegaccessibilitypublictoallusersofmyregionstatusR1 && isTemplateNamePresentRegaccessibilityprivatestatusR1 && isCampaignNamePresentCorpaccessibilitypublictoallusersR1 
					&& !isCampaignNamePresentCorpaccessibilitypublictoallcorporateusersR1 && !isCampaignNamePresentCorpaccessibilityprivateR1 && isCampaignNamePresentCorpaccessibilitypublictoallusersstatusR1 
					&& !isCampaignNamePresentCorpaccessibilitypublictoallcorporateusersstatusR1 && !isCampaignNamePresentCorpaccessibilityprivatestatusR1 && isTemplateNamePresentCorpaccessibilitypublictoallusersR1 
					&& !isTemplateNamePresentCorpaccessibilitypublictoallcorporateusersR1 && !isTemplateNamePresentCorpaccessibilityprivateR1 && isTemplateNamePresentCorpaccessibilitypublictoallusersstatusR1 
					&& !isTemplateNamePresentCorpaccessibilitypublictoallcorporateusersstatusR1 && !isTemplateNamePresentCorpaccessibilityprivatestatusR1 && isCampaignNamePresentRegpaccessibilitypublictoallusersofmyregion1R1
					&& isCampaignNamePresentRegpaccessibilityprivate1R1 && isCampaignNamePresentRegpaccessibilitypublictoallusersofmyregion1statusR1 && isCampaignNamePresentRegpaccessibilityprivate1statusR1
					&& isTemplateNamePresentRegaccessibilitypublictoallusersofmyregion1R1 && isTemplateNamePresentRegaccessibilityprivate1R1 && isTemplateNamePresentRegaccessibilitypublictoallusersofmyregion1statusR1 
					&& isTemplateNamePresentRegaccessibilityprivate1statusR1 && isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchiseR1 && isCampaignNamePresentFranpaccessibilityprivateR1 
					&& isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchisestatusR1 && isCampaignNamePresentFranpaccessibilityprivatestatusR1 && isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchiseR1 
					&& isTemplateNamePresentfranaccessibilityprivateR1 && isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchisestatusR1 && isTemplateNamePresentfranaccessibilityprivatestatusR1  
					&& isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchisemuR1 && isCampaignNamePresentFranpaccessibilityprivatemuR1 && isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchisemustatusR1 
					&& isCampaignNamePresentFranpaccessibilityprivatemustatusR1 && isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchisemuR1 && isTemplateNamePresentfranaccessibilityprivatemuR1 
					&& isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchisemustatusR1 && isTemplateNamePresentfranaccessibilityprivatemustatusR1 && isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchisemue2R1 
					&& isCampaignNamePresentFranpaccessibilityprivatemue2R1 && isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchisemue2statusR1 && isCampaignNamePresentFranpaccessibilityprivatemue2statusR1 
					&& isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchisemue2R1 && isTemplateNamePresentfranaccessibilityprivatemue2R1 && isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchisemue2statusR1
					&& isTemplateNamePresentfranaccessibilityprivatemue2statusR1) {
				fc.utobj().printTestStep("able to verify all types of campaigns and templates created by corporate/rgional/franchise users when loging with Regional user (R1) who has priviledge to see private campaign");
			}
			else {
				fc.utobj().throwsException("was not able to verify all types of campaigns and templates created by corporate/rgional/franchise users when loging with Regional user (R1) who has priviledge to see private campaign");
			}
		
			
			
			
			//Login with Regional user(R2) who does not have role to see private campaign
			fc.utobj().printTestStep("Login with Regional user(R2) who does not have role to see private campaign");
			fc.loginpage().loginWithParameter(driver, Reguname1, Regpassword1);
			fc.crm().crm_common().openCRMCampaignCenterPage(fcHomePageTest, driver);
			fc.utobj().printTestStep("Click on the menu option and select campaign");
			/*CRMCampaignCenterPage pobj = new CRMCampaignCenterPage(driver);*/
			fc.utobj().clickElement(driver, pobj1.menuOptions);
			fc.utobj().clickElement(driver, pobj1.campaignLinks);
			
			//Verifying Campaigns and templates of corporate user (C2) after logging with Regional user (R2)
			fc.utobj().printTestStep("Verifying Campaigns and templates of corporate user (C2) by Regional user(R2)");
			fc.utobj().printTestStep("Verify Public to all users Campaign created by Corporate user(C2) who does not have privilege to see private campaign by Regional user(R2)");
			campaignCenterPage.searchCampaignByFilter(driver, firstcampaignNamecorp);

			boolean isCampaignNamePresentCorpaccessibilitypublictoalluserscorpR2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firstcampaignNamecorp + "')]");
			if (isCampaignNamePresentCorpaccessibilitypublictoalluserscorpR2 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users Campaign created by Corporate user who does not have privilege to see private campaign by Regional user(R2)");
			}
			//Verify Public to all corporate users Campaign created by Corporate user who has privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Public to all corporate users Campaign created by Corporate user who does not have privilege to see private campaign by Regional user(R2)");
			campaignCenterPage.searchCampaignByFilter(driver, secondcampaignNamecorp);
			
			boolean isCampaignNamePresentCorpaccessibilitypublictoallcorporateuserscorpR2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondcampaignNamecorp + "')]");
			if (!isCampaignNamePresentCorpaccessibilitypublictoallcorporateuserscorpR2 == false) {
				fc.utobj().throwsException("was not able to verify Public to all corporate users Campaign created by Corporate user who does not privilege to see private campaign by Regional user(R2)");
			}
			
			//Verify Private Campaign created by Corporate user who has privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Private Campaign created by Corporate user who does not have privilege to see private campaign by Regional user(R2)");
			campaignCenterPage.searchCampaignByFilter(driver, thirdcampaignNamecorp);
			
			boolean isCampaignNamePresentCorpaccessibilityprivatecorpR2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + thirdcampaignNamecorp + "')]");
			if (!isCampaignNamePresentCorpaccessibilityprivatecorpR2 == false) {
				fc.utobj().throwsException("was not able to Private Campaign created by Corporate user who does not have privilege to see private campaign by Regional user(R2)");
			}
		
			//Verify Public to all users (Status Driven)Campaign created by Corporate user who does not have privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Public to all users(Status Driven) Campaign created by Corporate user who does not have privilege to see private campaign by Regional user(R2)");
			campaignCenterPage.searchCampaignByFilter(driver, firstcampaignNamecorpstatus);

			boolean isCampaignNamePresentCorpaccessibilitypublictoalluserscorpstatusR2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firstcampaignNamecorpstatus + "')]");
			if (isCampaignNamePresentCorpaccessibilitypublictoalluserscorpstatusR2 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users(Status Driven) Campaign created by Corporate user who does not have privilege to see private campaign by Regional user(R2)");
			}
			//Verify Public to all corporate users Campaign created by Corporate user who has privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Public to all corporate users(Status Driven) Campaign created by Corporate user who does not have privilege to see private campaign by Regional user(R2)");
			campaignCenterPage.searchCampaignByFilter(driver, secondcampaignNamecorpstatus);
			
			boolean isCampaignNamePresentCorpaccessibilitypublictoallcorporateuserscorpstatusR2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondcampaignNamecorpstatus + "')]");
			if (!isCampaignNamePresentCorpaccessibilitypublictoallcorporateuserscorpstatusR2 == false) {
				fc.utobj().throwsException("was not able to verify Public to all corporate users(Status Driven) Campaign created by Corporate user who does not privilege to see private campaign by Regional user(R2)");
			}
			
			//Verify Private Campaign created by Corporate user who has privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Private Campaign created by Corporate user(Status Driven) who does not have privilege to see private campaign by Regional user(R2)");
			campaignCenterPage.searchCampaignByFilter(driver, thirdcampaignNamecorpstatus);
			
			boolean isCampaignNamePresentCorpaccessibilityprivatecorpstatusR2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + thirdcampaignNamecorpstatus + "')]");
			if (!isCampaignNamePresentCorpaccessibilityprivatecorpstatusR2 == false) {
				fc.utobj().throwsException("was not able to Private Campaign created by Corporate user(Status Driven) who does not have privilege to see private campaign by Regional user(R2)");
			}
				
			fc.utobj().printTestStep("Navigate to CRM>Campaign Center>email template");
			emailtemplate.clickonemailtamplate(driver);
			
			//Verify Public to all users template created by Corporate user who does not have privilege to see private template by itself
			fc.utobj().printTestStep("Verify Public to all users template created by Corporate user who does not have privilege to see private template by Regional user(R2)");
			campaignCenterPage.searchTemplateByFilter(driver, firsttemplateNamecorp);

			boolean isTemplateNamePresentCorpaccessibilitypublictoalluserscorpR2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firsttemplateNamecorp + "')]");
			if (isTemplateNamePresentCorpaccessibilitypublictoalluserscorpR2 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users template created by Corporate user who has privilege to see private template by Regional user(R2)");
			}
			//Verify Public to all corporate users template created by Corporate user who has privilege to see private template by itself
			fc.utobj().printTestStep("Verify Public to all corporate users template created by Corporate user who does not have privilege to see private template by Regional user(R2)");
			campaignCenterPage.searchTemplateByFilter(driver, secondtemplateNamecorp);
			
			boolean isTemplateNamePresentCorpaccessibilitypublictoallcorporateuserscorpR2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondtemplateNamecorp + "')]");
			if (!isTemplateNamePresentCorpaccessibilitypublictoallcorporateuserscorpR2 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users template created by Corporate user who has privilege to see private template by Regional user(R2)");
			}
			
			//Verify Private template created by Corporate user who has privilege to see private template by itself
			fc.utobj().printTestStep("Verify Privare template created by Corporate user who does not have privilege to see private template by Regional user(R2)");
			campaignCenterPage.searchTemplateByFilter(driver, thirdtemplateNamecorp);
			
			boolean isTemplateNamePresentCorpaccessibilityprivatecorpR2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + thirdtemplateNamecorp + "')]");
			if (!isTemplateNamePresentCorpaccessibilityprivatecorpR2 == false) {
				fc.utobj().throwsException("was not able to verify private template created by Corporate user who has privilege to see private template by Regional user(R2)");
			}
			
			//Verify Public to all users (Status driven)template created by Corporate user who does not have privilege to see private template by itself
			fc.utobj().printTestStep("Verify Public to all users (Status driven)template created by Corporate user who does not have privilege to see private template by Regional user(R2)");
			campaignCenterPage.searchTemplateByFilter(driver, firsttemplateNamecorpstatus);

			boolean isTemplateNamePresentCorpaccessibilitypublictoalluserscorpstatusR2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firsttemplateNamecorpstatus + "')]");
			if (isTemplateNamePresentCorpaccessibilitypublictoalluserscorpstatusR2 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users (Status driven)template created by Corporate user who has privilege to see private template by Regional user(R2)");
			}
			//Verify Public to all corporate users template created by Corporate user who has privilege to see private template by itself
			fc.utobj().printTestStep("Verify Public to all corporate users (Status driven)template created by Corporate user who does not have privilege to see private template by Regional user(R2)");
			campaignCenterPage.searchTemplateByFilter(driver, secondtemplateNamecorpstatus);
			
			boolean isTemplateNamePresentCorpaccessibilitypublictoallcorporateuserscorpstatusR2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondtemplateNamecorpstatus + "')]");
			if (!isTemplateNamePresentCorpaccessibilitypublictoallcorporateuserscorpstatusR2 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users (Status driven)template created by Corporate user who has privilege to see private template by Regional user(R2)");
			}
			
			//Verify Private template created by Corporate user who has privilege to see private template by itself
			fc.utobj().printTestStep("Verify Privare (Status driven)template created by Corporate user who does not have privilege to see private template by Regional user(R2)");
			campaignCenterPage.searchTemplateByFilter(driver, thirdtemplateNamecorpstatus);
			
			boolean isTemplateNamePresentCorpaccessibilityprivatecorpstatusR2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + thirdtemplateNamecorpstatus + "')]");
			if (!isTemplateNamePresentCorpaccessibilityprivatecorpstatusR2 == false) {
				fc.utobj().throwsException("was not able to verify private (Status driven)template created by Corporate user who has privilege to see private template by Regional user(R2)");
			}
		
			fc.utobj().printTestStep("Navigate to CRM>Campaign Center>campaign template");
			emailtemplate.clickoncampaigntemplate(driver);
			//Verify Public to all users of my region Campaign created by Regional user who has privilege to see private campaign by itself(R1)
			//Verifying Campaigns and templates of Regional user (R1) after logging with Regional user (R2)
			fc.utobj().printTestStep("Verifying Campaigns and templates of Regional user (R1) after logging with Regional user (R2)");
			fc.utobj().printTestStep("Verify Public to all users of my region Campaign created by Regional user who has privilege to see private campaign by Regional user(R2)");
			campaignCenterPage.searchCampaignByFilter(driver, firstcampaignNamereg);

			boolean isCampaignNamePresentRegpaccessibilitypublictoallusersofmyregionR2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firstcampaignNamereg + "')]");
			if (isCampaignNamePresentRegpaccessibilitypublictoallusersofmyregionR2 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my region Campaign created by Regional user who has privilege to see private campaign by Regional user(R2)");
			}
			
			//Verify Private Campaign created by Regional user who has privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Private Campaign created by Regional user who has privilege to see private campaign by itself");
			campaignCenterPage.searchCampaignByFilter(driver, secondcampaignNamereg);
			
			boolean isCampaignNamePresentRegpaccessibilityprivateR2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondcampaignNamereg + "')]");
			if (!isCampaignNamePresentRegpaccessibilityprivateR2 == false) {
				fc.utobj().throwsException("was not able to verify Private Campaign created by Regional user who has privilege to see private campaign by Regional user(R2)");
			}
			
			//Verify Public to all users of my region (Status Driven)Campaign created by Regional user who has privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Public to all users of my region (Status Driven)Campaign created by Regional user who has privilege to see private campaign by Regional user(R2)");
			campaignCenterPage.searchCampaignByFilter(driver, firstcampaignNameregstatus);

			boolean isCampaignNamePresentRegpaccessibilitypublictoallusersofmyregionstatusR2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firstcampaignNameregstatus + "')]");
			if (isCampaignNamePresentRegpaccessibilitypublictoallusersofmyregionstatusR2 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my region (Status Driven)Campaign created by Regional user who has privilege to see private campaign by Regional user(R2)");
			}
			
			//Verify Private Campaign created by Regional user who has privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Private (Status Driven)Campaign created by Regional user who has privilege to see private campaign by Regional user(R2)");
			campaignCenterPage.searchCampaignByFilter(driver, secondcampaignNameregstatus);
			
			boolean isCampaignNamePresentRegpaccessibilityprivatestatusR2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondcampaignNameregstatus + "')]");
			if (!isCampaignNamePresentRegpaccessibilityprivatestatusR2 == false) {
				fc.utobj().throwsException("was not able to verify Private (Status Driven)Campaign created by Regional user who has privilege to see private campaign by Regional user(R2)");
			}
			
			fc.utobj().printTestStep("Navigate to CRM>Campaign Center>email template");
			emailtemplate.clickonemailtamplate(driver);
			
			//Verify Public to all users template created by Corporate user who has privilege to see private template by itself
			fc.utobj().printTestStep("Verify Public to all users of my region template created by Regional user who has privilege to see private template by Regional user(R2)");
			campaignCenterPage.searchTemplateByFilter(driver, firsttemplateNamereg);

			boolean isTemplateNamePresentRegaccessibilitypublictoallusersofmyregionR2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firsttemplateNamereg + "')]");
			if (isTemplateNamePresentRegaccessibilitypublictoallusersofmyregionR2 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users template created by Regional user who has privilege to see private template by Regional user(R2)");
			}
			
			//Verify Private template created by Regional user who has privilege to see private template by itself
			fc.utobj().printTestStep("Verify Private template created by Regional user who has privilege to see private template by Regional user(R2)");
			campaignCenterPage.searchTemplateByFilter(driver, secondtemplateNamereg);
			
			boolean isTemplateNamePresentRegaccessibilityprivateR2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondtemplateNamereg + "')]");
			if (!isTemplateNamePresentRegaccessibilityprivateR2 == false) {
				fc.utobj().throwsException("was not able to verify private template created by Regional user who has privilege to see private template by Regional user(R2)");
			}
			
			//Verify Public to all users(Status Driven) template created by Regional user who has privilege to see private template by itself
			fc.utobj().printTestStep("Verify Public to all users of my region (Status Driven)template created by Regional user who has privilege to see private template by Regional user(R2)");
			campaignCenterPage.searchTemplateByFilter(driver, firsttemplateNameregstatus);

			boolean isTemplateNamePresentRegaccessibilitypublictoallusersofmyregionstatusR2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firsttemplateNameregstatus + "')]");
			if (isTemplateNamePresentRegaccessibilitypublictoallusersofmyregionstatusR2 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my region (Status Driven)template created by Regional user who has privilege to see private template by Regional user(R2)");
			}
			
			//Verify Private template created by Regional user who has privilege to see private template by itself
			fc.utobj().printTestStep("Verify Private (Status Driven)template created by Regional user who has privilege to see private template by Regional user(R2)");
			campaignCenterPage.searchTemplateByFilter(driver, secondtemplateNameregstatus);
			
			boolean isTemplateNamePresentRegaccessibilityprivatestatusR2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondtemplateNameregstatus + "')]");
			if (!isTemplateNamePresentRegaccessibilityprivatestatusR2 == false) {
				fc.utobj().throwsException("was not able to verify private (Status Driven)template created by Regional user who has privilege to see private template by Regional user(R2)");
			}
			fc.utobj().printTestStep("Navigate to CRM>Campaign Center>campaign template");
			emailtemplate.clickoncampaigntemplate(driver);
			
			//Verify Public to all users Campaign created by Corporate user who has privilege to see private campaign by itself
			fc.utobj().printTestStep("Verifying Campaigns and templates of Corporate user (C1) by Regional user(R2)");
			fc.utobj().printTestStep("Verify Public to all users Campaign created by Corporate user who has privilege to see private campaign by Regional user(R2)");
			campaignCenterPage.searchCampaignByFilter(driver, firstcampaignName);

			boolean isCampaignNamePresentCorpaccessibilitypublictoallusersR2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firstcampaignName + "')]");
			if (isCampaignNamePresentCorpaccessibilitypublictoallusersR2 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users Campaign created by Corporate user who has privilege to see private campaign by Regional user(R2)");
			}
			//Verify Public to all corporate users Campaign created by Corporate user who has privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Public to all corporate users Campaign created by Corporate user who has privilege to see private campaign");
			campaignCenterPage.searchCampaignByFilter(driver, secondcampaignName);
			
			boolean isCampaignNamePresentCorpaccessibilitypublictoallcorporateusersR2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondcampaignName + "')]");
			if (!isCampaignNamePresentCorpaccessibilitypublictoallcorporateusersR2 == false) {
				fc.utobj().throwsException("was not able to verify Public to all corporate users Campaign created by Corporate user who has privilege to see private campaign by Regional user(R2)");
			}
			
			//Verify Private Campaign created by Corporate user who has privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Private Campaign created by Corporate user who has privilege to see private campaign by Regional user(R2)");
			campaignCenterPage.searchCampaignByFilter(driver, thirdcampaignName);
			
			boolean isCampaignNamePresentCorpaccessibilityprivateR2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + thirdcampaignName + "')]");
			if (!isCampaignNamePresentCorpaccessibilityprivateR2 == false) {
				fc.utobj().throwsException("was not able to Private Campaign created by Corporate user who has privilege to see private campaign by Regional user(R2)");
			}
			
			//Verify Public to all users(Status Driven) Campaign created by Corporate user who has privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Public to all users(Status Driven) Campaign created by Corporate user who has privilege to see private campaign by Regional user(R2)");
			campaignCenterPage.searchCampaignByFilter(driver, firstcampaignNamestatus);

			boolean isCampaignNamePresentCorpaccessibilitypublictoallusersstatusR2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firstcampaignNamestatus + "')]");
			if (isCampaignNamePresentCorpaccessibilitypublictoallusersstatusR2 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users(Status Driven) Campaign created by Corporate user who has privilege to see private campaign by Regional user(R2)");
			}
			//Verify Public to all corporate users(Status Driven) Campaign created by Corporate user who has privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Public to all corporate users(Status Driven) Campaign created by Corporate user who has privilege to see private campaign");
			campaignCenterPage.searchCampaignByFilter(driver, secondcampaignNamestatus);
			
			boolean isCampaignNamePresentCorpaccessibilitypublictoallcorporateusersstatusR2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondcampaignNamestatus + "')]");
			if (!isCampaignNamePresentCorpaccessibilitypublictoallcorporateusersstatusR2 == false) {
				fc.utobj().throwsException("was not able to verify Public to all corporate users(Status Driven) Campaign created by Corporate user who has privilege to see private campaign by Regional user(R2)");
			}
			
			//Verify Private Campaign created by Corporate user(Status Driven) who has privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Private Campaign(Status Driven) created by Corporate user who has privilege to see private campaign by Regional user(R2)");
			campaignCenterPage.searchCampaignByFilter(driver, thirdcampaignNamestatus);
			
			boolean isCampaignNamePresentCorpaccessibilityprivatestatusR2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + thirdcampaignNamestatus + "')]");
			if (!isCampaignNamePresentCorpaccessibilityprivatestatusR2 == false) {
				fc.utobj().throwsException("was not able to Private Campaign(Status Driven) created by Corporate user who has privilege to see private campaign by Regional user(R2)");
			}
		
			fc.utobj().printTestStep("Navigate to CRM>Campaign Center>email template");
			emailtemplate.clickonemailtamplate(driver);
			
			//Verify Public to all users template created by Corporate user who has privilege to see private template by itself
			fc.utobj().printTestStep("Verify Public to all users template created by Corporate user who has privilege to see private template by Regional user(R2)");
			campaignCenterPage.searchTemplateByFilter(driver, firsttemplateName);

			boolean isTemplateNamePresentCorpaccessibilitypublictoallusersR2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firsttemplateName + "')]");
			if (isTemplateNamePresentCorpaccessibilitypublictoallusersR2 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users template created by Corporate user who has privilege to see private template by Regional user(R2)");
			}
			//Verify Public to all corporate users template created by Corporate user who has privilege to see private template by itself
			fc.utobj().printTestStep("Verify Public to all corporate users template created by Corporate user who has privilege to see private template by Regional user(R2)");
			campaignCenterPage.searchTemplateByFilter(driver, secondtemplateName);
			
			boolean isTemplateNamePresentCorpaccessibilitypublictoallcorporateusersR2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondtemplateName + "')]");
			if (!isTemplateNamePresentCorpaccessibilitypublictoallcorporateusersR2 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users template created by Corporate user who has privilege to see private template by Regional user(R2)");
			}
			
			//Verify Private template created by Corporate user who has privilege to see private template by itself
			fc.utobj().printTestStep("Verify Privare template created by Corporate user who has privilege to see private template by Regional user(R2)");
			campaignCenterPage.searchTemplateByFilter(driver, thirdtemplateName);
			
			boolean isTemplateNamePresentCorpaccessibilityprivateR2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + thirdtemplateName + "')]");
			if (!isTemplateNamePresentCorpaccessibilityprivateR2 == false) {
				fc.utobj().throwsException("was not able to verify private template created by Corporate user who has privilege to see private template by Regional user(R2)");
			}
				
			//Verify Public to all users template(Status Driven) created by Corporate user who has privilege to see private template by itself
			fc.utobj().printTestStep("Verify Public to all users template(Status Driven) created by Corporate user who has privilege to see private template by Regional user(R2)");
			campaignCenterPage.searchTemplateByFilter(driver, firsttemplateNamestatus);

			boolean isTemplateNamePresentCorpaccessibilitypublictoallusersstatusR2= fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firsttemplateNamestatus + "')]");
			if (isTemplateNamePresentCorpaccessibilitypublictoallusersstatusR2 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users template(Status Driven) created by Corporate user who has privilege to see private template by Regional user(R2)");
			}
			//Verify Public to all corporate users template(Status Driven) created by Corporate user who has privilege to see private template by itself
			fc.utobj().printTestStep("Verify Public to all corporate users template(Status Driven) created by Corporate user who has privilege to see private template by Regional user(R2)");
			campaignCenterPage.searchTemplateByFilter(driver, secondtemplateNamestatus);
			
			boolean isTemplateNamePresentCorpaccessibilitypublictoallcorporateusersstatusR2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondtemplateNamestatus + "')]");
			if (!isTemplateNamePresentCorpaccessibilitypublictoallcorporateusersstatusR2 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users template(Status Driven) created by Corporate user who has privilege to see private template by Regional user(R2)");
			}
			
			//Verify Private template(Status Driven) created by Corporate user who has privilege to see private template by itself
			fc.utobj().printTestStep("Verify Privare template(Status Driven) created by Corporate user who has privilege to see private template by Regional user(R2)");
			campaignCenterPage.searchTemplateByFilter(driver, thirdtemplateNamestatus);
			
			boolean isTemplateNamePresentCorpaccessibilityprivatestatusR2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + thirdtemplateNamestatus + "')]");
			if (!isTemplateNamePresentCorpaccessibilityprivatestatusR2 == false) {
				fc.utobj().throwsException("was not able to verify private template(Status Driven) created by Corporate user who has privilege to see private template by Regional user(R2)");
			}
		
			fc.utobj().printTestStep("Navigate to CRM>Campaign Center>campaign template");
			emailtemplate.clickoncampaigntemplate(driver);
			
			//Verify Public to all users Campaign created by Corporate user who has privilege to see private campaign by itself
			fc.utobj().printTestStep("Verifying Campaigns and templates of Regional user (R2) by itself");
			//Verify Public to all users of my region Campaign created by Regional user who does not have privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Public to all users of my region Campaign created by Regional user who does not have privilege to see private campaign by itself");
			campaignCenterPage.searchCampaignByFilter(driver, firstcampaignNamereg1);

			boolean isCampaignNamePresentRegpaccessibilitypublictoallusersofmyregion1R2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firstcampaignNamereg1 + "')]");
			if (isCampaignNamePresentRegpaccessibilitypublictoallusersofmyregion1R2 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my region Campaign created by Regional user who does not have privilege to see private campaign by itself");
			}
			
			//Verify Private Campaign created by Regional user who does not have privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Private Campaign created by Regional user who does not have privilege to see private campaign by itself");
			campaignCenterPage.searchCampaignByFilter(driver, secondcampaignNamereg1);
			
			boolean isCampaignNamePresentRegpaccessibilityprivate1R2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondcampaignNamereg1 + "')]");
			if (isCampaignNamePresentRegpaccessibilityprivate1R2 == false) {
				fc.utobj().throwsException("was not able to verify Private Campaign created by Regional user who does not have privilege to see private campaign by itself");
			}
			
			//Verify Public to all users of my region (Status Driven)Campaign created by Regional user who does not have privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Public to all users of my region (Status Driven)Campaign created by Regional user who does not have privilege to see private campaign by itself");
			campaignCenterPage.searchCampaignByFilter(driver, firstcampaignNamereg1status);

			boolean isCampaignNamePresentRegpaccessibilitypublictoallusersofmyregion1statusR2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firstcampaignNamereg1status + "')]");
			if (isCampaignNamePresentRegpaccessibilitypublictoallusersofmyregion1statusR2 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my region (Status Driven)Campaign created by Regional user who does not have privilege to see private campaign by itself");
			}
			
			//Verify Private (Status Driven)Campaign created by Regional user who does not have privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Private (Status Driven)Campaign created by Regional user who does not have privilege to see private campaign by itself");
			campaignCenterPage.searchCampaignByFilter(driver, secondcampaignNamereg1status);
			
			boolean isCampaignNamePresentRegpaccessibilityprivate1statusR2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondcampaignNamereg1status + "')]");
			if (isCampaignNamePresentRegpaccessibilityprivate1statusR2 == false) {
				fc.utobj().throwsException("was not able to verify Private (Status Driven)Campaign created by Regional user who does not have privilege to see private campaign itself");
			}
		
			fc.utobj().printTestStep("Navigate to CRM>Campaign Center>email template");
			emailtemplate.clickonemailtamplate(driver);
			
			//Verify Public to all users template created by Corporate user who does not have privilege to see private template by itself
			fc.utobj().printTestStep("Verify Public to all users of my region template created by Regional user who does not have privilege to see private template by itself");
			campaignCenterPage.searchTemplateByFilter(driver, firsttemplateNamereg1);

			boolean isTemplateNamePresentRegaccessibilitypublictoallusersofmyregion1R2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firsttemplateNamereg1 + "')]");
			if (isTemplateNamePresentRegaccessibilitypublictoallusersofmyregion1R2 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my region template created by Regional user who does not have privilege to see private template by itself");
			}
			
			//Verify Private template created by Regional user who does not have privilege to see private template by itself
			fc.utobj().printTestStep("Verify Private template created by Regional user does not have privilege to see private template by itself");
			campaignCenterPage.searchTemplateByFilter(driver, secondtemplateNamereg1);
			
			boolean isTemplateNamePresentRegaccessibilityprivate1R2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondtemplateNamereg1 + "')]");
			if (isTemplateNamePresentRegaccessibilityprivate1R2 == false) {
				fc.utobj().throwsException("was not able to verify Private template created by Regional user does not have privilege to see private template by itself");
			}
			
			//Verify Public to all users (Status Driven)template created by Regional user who does not have privilege to see private template by itself
			fc.utobj().printTestStep("Verify Public to all users of my region (Status Driven)template created by Regional user who does not have privilege to see private template by itself");
			campaignCenterPage.searchTemplateByFilter(driver, firsttemplateNamereg1status);

			boolean isTemplateNamePresentRegaccessibilitypublictoallusersofmyregion1statusR2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firsttemplateNamereg1status + "')]");
			if (isTemplateNamePresentRegaccessibilitypublictoallusersofmyregion1statusR2 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my region (Status Driven)template created by Regional user who does not have privilege to see private template by itself");
			}
			
			//Verify Private template created by Regional user who does not have privilege to see private template by itself
			fc.utobj().printTestStep("Verify Private (Status Driven)template created by Regional user does not have privilege to see private template by itself");
			campaignCenterPage.searchTemplateByFilter(driver, secondtemplateNamereg1status);
			
			boolean isTemplateNamePresentRegaccessibilityprivate1statusR2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondtemplateNamereg1status + "')]");
			if (isTemplateNamePresentRegaccessibilityprivate1statusR2 == false) {
				fc.utobj().throwsException("was not able to verify Private (Status Driven)template created by Regional user does not have privilege to see private template by itself");
			}
			
			fc.utobj().printTestStep("Navigate to CRM>Campaign Center>campaign template");
			emailtemplate.clickoncampaigntemplate(driver);
			
			fc.utobj().clickElement(driver, crmcenter.locationCampaign);
			fc.utobj().printTestStep("Verifying Campaigns and templates of Franchise owner user (F0New) by Regional user(R2)");
			fc.utobj().printTestStep("Verify Public to all users of my Franchise Campaign created by Franchise user who has privilege to see private campaign by Regional user(R2)");
			campaignCenterPage.searchCampaignByFilter(driver, firstcampaignNamefran);

			boolean isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchiseR2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firstcampaignNamefran + "')]");
			if (isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchiseR2 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my Franchise Campaign created by Franchise user who has privilege to see private campaign by Regional user(R2)");
			}
			
			//Verify Private Campaign created by Franchise user who has privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Private Campaign created by Franchise user who has privilege to see private campaign by Regional user(R2)");
			campaignCenterPage.searchCampaignByFilter(driver, secondcampaignNamefran);
			
			boolean isCampaignNamePresentFranpaccessibilityprivateR2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondcampaignNamefran + "')]");
			if (!isCampaignNamePresentFranpaccessibilityprivateR2 == false) {
				fc.utobj().throwsException("was not able to verify Private Campaign created by Franchise user who has privilege to see private campaign by Regional user(R2)");
			}
			
			
			//Verify Public to all users of my Franchise (Status Driven)Campaign created by Franchise user who has privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Public to all users of my Franchise (Status Driven)Campaign created by Franchise user who has privilege to see private campaign by Regional user(R2)");
			campaignCenterPage.searchCampaignByFilter(driver, firstcampaignNamefranstatus);

			boolean isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchisestatusR2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firstcampaignNamefranstatus + "')]");
			if (isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchisestatusR2 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my Franchise (Status Driven)Campaign created by Franchise user who has privilege to see private campaign by Regional user(R2)");
			}
			
			//Verify Private Campaign created by Franchise user who has privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Private (Status Driven)Campaign created by Franchise user who has privilege to see private campaign by Regional user(R2)");
			campaignCenterPage.searchCampaignByFilter(driver, secondcampaignNamefranstatus);
			
			boolean isCampaignNamePresentFranpaccessibilityprivatestatusR2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondcampaignNamefranstatus + "')]");
			if (!isCampaignNamePresentFranpaccessibilityprivatestatusR2 == false) {
				fc.utobj().throwsException("was not able to verify Private (Status Driven)Campaign created by Franchise user who has privilege to see private campaign by Regional user(R2)");
			}
			
			fc.utobj().printTestStep("Navigate to CRM>Campaign Center>email template");
			emailtemplate.clickonemailtamplate(driver);
			fc.utobj().clickElement(driver, crmcenter.locationtemplate);
			//Verify Public to all users template created by Corporate user who has privilege to see private template by itself
			fc.utobj().printTestStep("Verify Public to all users of my franchise template created by franchise user who has privilege to see private template by Regional user(R2)");
			campaignCenterPage.searchTemplateByFilter(driver, firsttemplateNamefran);

			boolean isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchiseR2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firsttemplateNamefran + "')]");
			if (isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchiseR2 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my franchise template created by franchise user who has privilege to see private template by Regional user(R2)");
			}
			
			//Verify Private template created by franchise user who has privilege to see private template by itself
			fc.utobj().printTestStep("Verify Private template created by franchise user who has privilege to see private template by Regional user(R2)");
			campaignCenterPage.searchTemplateByFilter(driver, secondtemplateNamefran);
			
			boolean isTemplateNamePresentfranaccessibilityprivateR2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondtemplateNamefran + "')]");
			if (!isTemplateNamePresentfranaccessibilityprivateR2 == false) {
				fc.utobj().throwsException("was not able to verify private template created by franchise user who has privilege to see private template by Regional user(R2)");
			}
			
			//Verify Public to all users (Status Driven)template created by Corporate user who has privilege to see private template by itself
			fc.utobj().printTestStep("Verify Public to all users of my franchise (Status Driven)template created by franchise user who has privilege to see private template by Regional user(R2)");
			campaignCenterPage.searchTemplateByFilter(driver, firsttemplateNamefranstatus);

			boolean isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchisestatusR2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firsttemplateNamefranstatus + "')]");
			if (isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchisestatusR2 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my franchise (Status Driven)template created by franchise user who has privilege to see private template by Regional user(R2)");
			}
			
			//Verify Private template created by franchise user who has privilege to see private template by itself
			fc.utobj().printTestStep("Verify Private (Status Driven)template created by franchise user who has privilege to see private template by Regional user(R2)");
			campaignCenterPage.searchTemplateByFilter(driver, secondtemplateNamefranstatus);
			
			boolean isTemplateNamePresentfranaccessibilityprivatestatusR2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondtemplateNamefranstatus + "')]");
			if (!isTemplateNamePresentfranaccessibilityprivatestatusR2 == false) {
				fc.utobj().throwsException("was not able to verify private (Status Driven)template created by franchise user who has privilege to see private template by Regional user(R2)");
			}
				
			
			fc.utobj().printTestStep("Navigate to CRM>Campaign Center>campaign template");
			emailtemplate.clickoncampaigntemplate(driver);
			fc.utobj().clickElement(driver, crmcenter.locationCampaign);
			fc.utobj().printTestStep("Verifying Campaigns and templates of Franchise Mu user (V1) by Regional user(R2)");
			
			fc.utobj().printTestStep("Verify Public to all users of my Franchise Campaign created by Franchise MU user(V1) who has privilege to see private campaign by Regional user(R2)");
			campaignCenterPage.searchCampaignByFilter(driver, firstcampaignNamefranmu);

			boolean isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchisemuR2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firstcampaignNamefranmu + "')]");
			if (isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchisemuR2 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my Franchise Campaign created by Franchise MU user(V1) who has privilege to see private campaign by Regional user(R2)");
			}
			
			//Verify Private Campaign created by Franchise user who has privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Private Campaign created by Franchise MU user(V1) who has privilege to see private campaign by Regional user(R2)");
			campaignCenterPage.searchCampaignByFilter(driver, secondcampaignNamefranmu);
			
			boolean isCampaignNamePresentFranpaccessibilityprivatemuR2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondcampaignNamefranmu + "')]");
			if (!isCampaignNamePresentFranpaccessibilityprivatemuR2 == false) {
				fc.utobj().throwsException("was not able to verify Private Campaign created by Franchise MU user(V1) who has privilege to see private campaign by Regional user(R2)");
			}
			
			
			//Verify Public to all users of my Franchise (Status Driven)Campaign created by Franchise user who has privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Public to all users of my Franchise (Status Driven)Campaign created by Franchise MU user(V1) who has privilege to see private campaign by Regional user(R2)");
			campaignCenterPage.searchCampaignByFilter(driver, firstcampaignNamefranmustatus);

			boolean isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchisemustatusR2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firstcampaignNamefranmustatus + "')]");
			if (isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchisemustatusR2 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my Franchise (Status Driven)Campaign created by Franchise MU user(V1) who has privilege to see private campaign by Regional user(R2)");
			}
			
			//Verify Private Campaign created by Franchise user who has privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Private (Status Driven)Campaign created by Franchise MU user(V1) who has privilege to see private campaign by Regional user(R2)");
			campaignCenterPage.searchCampaignByFilter(driver, secondcampaignNamefranmustatus);
			
			boolean isCampaignNamePresentFranpaccessibilityprivatemustatusR2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondcampaignNamefranmustatus + "')]");
			if (!isCampaignNamePresentFranpaccessibilityprivatemustatusR2 == false) {
				fc.utobj().throwsException("was not able to verify Private (Status Driven)Campaign created by Franchise MU user(V1) who has privilege to see private campaign by Regional user(R2)");
			}
			
			fc.utobj().printTestStep("Navigate to CRM>Campaign Center>email template");
			emailtemplate.clickonemailtamplate(driver);
			fc.utobj().clickElement(driver, crmcenter.locationtemplate);
			//Verify Public to all users template created by Corporate user who has privilege to see private template by itself
			fc.utobj().printTestStep("Verify Public to all users of my franchise template created by franchise MU user(V1) who has privilege to see private template by Regional user(R2)");
			campaignCenterPage.searchTemplateByFilter(driver, firsttemplateNamefranmu);

			boolean isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchisemuR2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firsttemplateNamefranmu + "')]");
			if (isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchisemuR2 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my franchise template created by franchise MU user(V1) who has privilege to see private template by Regional user(R2)");
			}
			
			//Verify Private template created by franchise user who has privilege to see private template by itself
			fc.utobj().printTestStep("Verify Private template created by franchise MU user(V1) who has privilege to see private template by Regional user(R2)");
			campaignCenterPage.searchTemplateByFilter(driver, secondtemplateNamefranmu);
			
			boolean isTemplateNamePresentfranaccessibilityprivatemuR2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondtemplateNamefranmu + "')]");
			if (!isTemplateNamePresentfranaccessibilityprivatemuR2 == false) {
				fc.utobj().throwsException("was not able to verify private template created by franchise MU user(V1) who has privilege to see private template by Regional user(R2)");
			}
			
			//Verify Public to all users template created by Corporate user who has privilege to see private template by itself
			fc.utobj().printTestStep("Verify Public to all users of my franchise (Status Driven)template created by franchise MU user(V1) who has privilege to see private template by Regional user(R2)");
			campaignCenterPage.searchTemplateByFilter(driver, firsttemplateNamefranmustatus);

			boolean isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchisemustatusR2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firsttemplateNamefranmustatus + "')]");
			if (isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchisemustatusR2 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my franchise (Status Driven)template created by franchise MU user(V1) who has privilege to see private template by Regional user(R2)");
			}
			
			//Verify Private template created by franchise user who has privilege to see private template by itself
			fc.utobj().printTestStep("Verify Private (Status Driven)template created by franchise MU user(V1) who has privilege to see private template by Regional user(R2)");
			campaignCenterPage.searchTemplateByFilter(driver, secondtemplateNamefranmustatus);
			
			boolean isTemplateNamePresentfranaccessibilityprivatemustatusR2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondtemplateNamefranmustatus + "')]");
			if (!isTemplateNamePresentfranaccessibilityprivatemustatusR2 == false) {
				fc.utobj().throwsException("was not able to verify private (Status Driven)template created by franchise MU user(V1) who has privilege to see private template by Regional user(R2)");
			}
			
			
			
			
			fc.utobj().printTestStep("Navigate to CRM>Campaign Center>campaign template");
			emailtemplate.clickoncampaigntemplate(driver);
			fc.utobj().clickElement(driver, crmcenter.locationCampaign);
			fc.utobj().printTestStep("Verifying Campaigns and templates of Franchise Employee user (V2) by Regional user(R2)");
			fc.utobj().printTestStep("Verify Public to all users of my Franchise Campaign created by Franchise Employee user(V2) who does not have privilege to see private campaign by Regional user(R2)");
			campaignCenterPage.searchCampaignByFilter(driver, firstcampaignNamefranmue2);

			boolean isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchisemue2R2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firstcampaignNamefranmue2 + "')]");
			if (isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchisemue2R2 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my Franchise Campaign created by Franchise Employee user(V2) who does not have privilege to see private campaign by Regional user(R2)");
			}
			
			//Verify Private Campaign created by Franchise user who has privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Private Campaign created by Franchise Employee user(V2) who does not have privilege to see private campaign by Regional user(R2)");
			campaignCenterPage.searchCampaignByFilter(driver, secondcampaignNamefranmue2);
			
			boolean isCampaignNamePresentFranpaccessibilityprivatemue2R2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondcampaignNamefranmue2 + "')]");
			if (!isCampaignNamePresentFranpaccessibilityprivatemue2R2 == false) {
				fc.utobj().throwsException("was not able to verify Private Campaign created by Franchise Employee user(V2) who does not have privilege to see private campaign by Regional user(R2)");
			}
			
			
			//Verify Public to all users of my Franchise Campaign created by Franchise Employee user(V2) who has privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Public to all users of my Franchise (Status Driven)Campaign created by Franchise Employee user(V2) who does not have privilege to see private campaign by Regional user(R2)");
			campaignCenterPage.searchCampaignByFilter(driver, firstcampaignNamefranmue2status);

			boolean isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchisemue2statusR2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firstcampaignNamefranmue2status + "')]");
			if (isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchisemue2statusR2 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my Franchise (Status Driven)Campaign created by Franchise Employee user(V2) who does not have privilege to see private campaign by Regional user(R2)");
			}
			
			//Verify Private Campaign created by Franchise user who has privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Private (Status Driven)Campaign created by Franchise Employee user(V2) who does not have privilege to see private campaign by Regional user(R2)");
			campaignCenterPage.searchCampaignByFilter(driver, secondcampaignNamefranmue2status);
			
			boolean isCampaignNamePresentFranpaccessibilityprivatemue2statusR2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondcampaignNamefranmue2status + "')]");
			if (!isCampaignNamePresentFranpaccessibilityprivatemue2statusR2 == false) {
				fc.utobj().throwsException("was not able to verify Private (Status Driven)Campaign created by Franchise Employee user(V2) who does not have privilege to see private campaign by Regional user(R2)");
			}
			
			fc.utobj().printTestStep("Navigate to CRM>Campaign Center>email template");
			emailtemplate.clickonemailtamplate(driver);
			fc.utobj().clickElement(driver, crmcenter.locationtemplate);
			//Verify Public to all users template created by Franchise user who has privilege to see private template by itself
			fc.utobj().printTestStep("Verify Public to all users of my franchise template created by franchise Employee user(V2) who does not have privilege to see private template by Regional user(R2)");
			campaignCenterPage.searchTemplateByFilter(driver, firsttemplateNamefranmue2);

			boolean isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchisemue2R2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firsttemplateNamefranmue2 + "')]");
			if (isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchisemue2R2 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my franchise template created by franchise Employee user(V2) who has privilege to see private template by Regional user(R2)");
			}
			
			//Verify Private template created by franchise user who has privilege to see private template by itself
			fc.utobj().printTestStep("Verify Private template created by franchise Employee user(V2) who has privilege to see private template by Regional user(R2)");
			campaignCenterPage.searchTemplateByFilter(driver, secondtemplateNamefranmue2);
			
			boolean isTemplateNamePresentfranaccessibilityprivatemue2R2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondtemplateNamefranmue2 + "')]");
			if (!isTemplateNamePresentfranaccessibilityprivatemue2R2 == false) {
				fc.utobj().throwsException("was not able to verify private template created by franchise Employee user(V2) who has privilege to see private template by Regional user(R2)");
			}
			
			//Verify Public to all users template created by Franchise user who has privilege to see private template by itself
			fc.utobj().printTestStep("Verify Public to all users of my franchise (Status Driven)template created by franchise Employee user(V2) who does not have privilege to see private template by Regional user(R2)");
			campaignCenterPage.searchTemplateByFilter(driver, firsttemplateNamefranmue2status);

			boolean isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchisemue2statusR2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firsttemplateNamefranmue2status + "')]");
			if (isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchisemue2statusR2 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my franchise (Status Driven)template created by franchise Employee user(V2) who has privilege to see private template by Regional user(R2)");
			}
			
			//Verify Private template created by franchise user who has privilege to see private template by itself
			fc.utobj().printTestStep("Verify Private (Status Driven)template created by franchise Employee user(V2) who has privilege to see private template by Regional user(R2)");
			campaignCenterPage.searchTemplateByFilter(driver, secondtemplateNamefranmue2status);
			
			boolean isTemplateNamePresentfranaccessibilityprivatemue2statusR2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondtemplateNamefranmue2status + "')]");
			if (!isTemplateNamePresentfranaccessibilityprivatemue2statusR2 == false) {
				fc.utobj().throwsException("was not able to verify private (Status Driven)template created by franchise Employee user(V2) who has privilege to see private template by Regional user(R2)");
			}
			
			if(isCampaignNamePresentCorpaccessibilitypublictoalluserscorpR2 && !isCampaignNamePresentCorpaccessibilitypublictoallcorporateuserscorpR2 && !isCampaignNamePresentCorpaccessibilityprivatecorpR2 
					&& isCampaignNamePresentCorpaccessibilitypublictoalluserscorpstatusR2 && !isCampaignNamePresentCorpaccessibilitypublictoallcorporateuserscorpstatusR2 && !isCampaignNamePresentCorpaccessibilityprivatecorpstatusR2 
					&& isTemplateNamePresentCorpaccessibilitypublictoalluserscorpR2 && !isTemplateNamePresentCorpaccessibilitypublictoallcorporateuserscorpR2 && !isTemplateNamePresentCorpaccessibilityprivatecorpR2 
					&& isTemplateNamePresentCorpaccessibilitypublictoalluserscorpstatusR2 && !isTemplateNamePresentCorpaccessibilitypublictoallcorporateuserscorpstatusR2 && !isTemplateNamePresentCorpaccessibilityprivatecorpstatusR2 
					&& isCampaignNamePresentRegpaccessibilitypublictoallusersofmyregionR2 && !isCampaignNamePresentRegpaccessibilityprivateR2 && isCampaignNamePresentRegpaccessibilitypublictoallusersofmyregionstatusR2 
					&& !isCampaignNamePresentRegpaccessibilityprivatestatusR2 && isTemplateNamePresentRegaccessibilitypublictoallusersofmyregionR2 && !isTemplateNamePresentRegaccessibilityprivateR2 
					&& isTemplateNamePresentRegaccessibilitypublictoallusersofmyregionstatusR2 && !isTemplateNamePresentRegaccessibilityprivatestatusR2 && isCampaignNamePresentCorpaccessibilitypublictoallusersR2 
					&& !isCampaignNamePresentCorpaccessibilitypublictoallcorporateusersR2 && !isCampaignNamePresentCorpaccessibilityprivateR2 && isCampaignNamePresentCorpaccessibilitypublictoallusersstatusR2 
					&& !isCampaignNamePresentCorpaccessibilitypublictoallcorporateusersstatusR2 && !isCampaignNamePresentCorpaccessibilityprivatestatusR2 && isTemplateNamePresentCorpaccessibilitypublictoallusersR2 
					&& !isTemplateNamePresentCorpaccessibilitypublictoallcorporateusersR2 && !isTemplateNamePresentCorpaccessibilityprivateR2 && isTemplateNamePresentCorpaccessibilitypublictoallusersstatusR2 
					&& !isTemplateNamePresentCorpaccessibilitypublictoallcorporateusersstatusR2 && !isTemplateNamePresentCorpaccessibilityprivatestatusR2 && isCampaignNamePresentRegpaccessibilitypublictoallusersofmyregion1R2
					&& isCampaignNamePresentRegpaccessibilityprivate1R2 && isCampaignNamePresentRegpaccessibilitypublictoallusersofmyregion1statusR2 && isCampaignNamePresentRegpaccessibilityprivate1statusR2
					&& isTemplateNamePresentRegaccessibilitypublictoallusersofmyregion1R2 && isTemplateNamePresentRegaccessibilityprivate1R2 && isTemplateNamePresentRegaccessibilitypublictoallusersofmyregion1statusR2 
					&& isTemplateNamePresentRegaccessibilityprivate1statusR2 && isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchiseR2 && !isCampaignNamePresentFranpaccessibilityprivateR2 
					&& isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchisestatusR2 && !isCampaignNamePresentFranpaccessibilityprivatestatusR2 && isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchiseR2 
					&& !isTemplateNamePresentfranaccessibilityprivateR2 && isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchisestatusR2 && !isTemplateNamePresentfranaccessibilityprivatestatusR2  
					&& isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchisemuR2 && !isCampaignNamePresentFranpaccessibilityprivatemuR2 && isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchisemustatusR2 
					&& !isCampaignNamePresentFranpaccessibilityprivatemustatusR2 && isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchisemuR2 && !isTemplateNamePresentfranaccessibilityprivatemuR2 
					&& isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchisemustatusR2 && !isTemplateNamePresentfranaccessibilityprivatemustatusR2 && isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchisemue2R2 
					&& !isCampaignNamePresentFranpaccessibilityprivatemue2R2 && isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchisemue2statusR2 && !isCampaignNamePresentFranpaccessibilityprivatemue2statusR2 
					&& isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchisemue2R2 && !isTemplateNamePresentfranaccessibilityprivatemue2R2 && isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchisemue2statusR2
					&& !isTemplateNamePresentfranaccessibilityprivatemue2statusR2) {
				fc.utobj().printTestStep("able to verify all types of campaigns and templates created by corporate/rgional/franchise users when loging with Regional user(R2) who does not have priviledge to see private campaign");
			}
			else {
				fc.utobj().throwsException("was not able to verify all types of campaigns and templates created by corporate/rgional/franchise users when loging with Regional user(R2) who does not have priviledge to see private campaign");
			}
			
			
			
			
			
			
			
			
			

			//Login with Franchisee mu owner user(V1) who has role to see private campaign
			fc.utobj().printTestStep("Login with Franchisee(mu) owner user (V1) who has role to see private campaign");
			fc.loginpage().loginWithParameter(driver, userNameFOmu, passwordFOmu);
			fc.crm().crm_common().openCRMCampaignCenterPage(fcHomePageTest, driver);
			fc.utobj().printTestStep("Click on the menu option and select campaign");
			fc.utobj().clickElement(driver, pobj1.menuOptions);
			fc.utobj().clickElement(driver, pobj1.campaignLinks);
			fc.utobj().clickElement(driver, crmcenter.corporatCampaign);
			//Verifying Campaigns and templates of corporate user (C2) after logging with Franchisee owner user(mu)
			fc.utobj().printTestStep("Verifying Campaigns and templates of corporate user (C2) after logging with Franchisee owner user(mu)");
			fc.utobj().printTestStep("Verify Public to all users Campaign created by Corporate user(C2) who does not have privilege to see private campaign by Franchisee owner user(mu)");
	
			
			campaignCenterPage.searchCampaignByFilter(driver, firstcampaignNamecorp);

			boolean isCampaignNamePresentCorpaccessibilitypublictoalluserscorpV1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firstcampaignNamecorp + "')]");
			if (isCampaignNamePresentCorpaccessibilitypublictoalluserscorpV1 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users Campaign created by Corporate user who does not have privilege to see private campaign by Franchisee owner user(mu)");
			}
			//Verify Public to all corporate users Campaign created by Corporate user who has privilege to see private campaign by Franchisee mu user(V1)
			fc.utobj().printTestStep("Verify Public to all corporate users Campaign created by Corporate user who does not have privilege to see private campaign by Franchisee owner user(mu)");
			campaignCenterPage.searchCampaignByFilter(driver, secondcampaignNamecorp);
			
			boolean isCampaignNamePresentCorpaccessibilitypublictoallcorporateuserscorpV1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondcampaignNamecorp + "')]");
			if (!isCampaignNamePresentCorpaccessibilitypublictoallcorporateuserscorpV1 == false) {
				fc.utobj().throwsException("was not able to verify Public to all corporate users Campaign created by Corporate user who does not privilege to see private campaign by Franchisee owner user(mu)");
			}
			
			//Verify Private Campaign created by Corporate user who has privilege to see private campaign by Franchisee mu user(V1)
			fc.utobj().printTestStep("Verify Private Campaign created by Corporate user who does not have privilege to see private campaign by Franchisee owner user(mu)");
			campaignCenterPage.searchCampaignByFilter(driver, thirdcampaignNamecorp);
			
			boolean isCampaignNamePresentCorpaccessibilityprivatecorpV1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + thirdcampaignNamecorp + "')]");
			if (!isCampaignNamePresentCorpaccessibilityprivatecorpV1 == false) {
				fc.utobj().throwsException("was not able to Private Campaign created by Corporate user who does not have privilege to see private campaign by Franchisee owner user(mu)");
			}
		
			//Verify Public to all users (Status Driven)Campaign created by Corporate user who does not have privilege to see private campaign by Franchisee mu user(V1)
			fc.utobj().printTestStep("Verify Public to all users(Status Driven) Campaign created by Corporate user who does not have privilege to see private campaign by Franchisee owner user(mu)");
			campaignCenterPage.searchCampaignByFilter(driver, firstcampaignNamecorpstatus);

			boolean isCampaignNamePresentCorpaccessibilitypublictoalluserscorpstatusV1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firstcampaignNamecorpstatus + "')]");
			if (isCampaignNamePresentCorpaccessibilitypublictoalluserscorpstatusV1 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users(Status Driven) Campaign created by Corporate user who does not have privilege to see private campaign by Franchisee owner user(mu)");
			}
			//Verify Public to all corporate users Campaign created by Corporate user who has privilege to see private campaign by Franchisee mu user(V1)
			fc.utobj().printTestStep("Verify Public to all corporate users(Status Driven) Campaign created by Corporate user who does not have privilege to see private campaign by Franchisee owner user(mu)");
			campaignCenterPage.searchCampaignByFilter(driver, secondcampaignNamecorpstatus);
			
			boolean isCampaignNamePresentCorpaccessibilitypublictoallcorporateuserscorpstatusV1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondcampaignNamecorpstatus + "')]");
			if (!isCampaignNamePresentCorpaccessibilitypublictoallcorporateuserscorpstatusV1 == false) {
				fc.utobj().throwsException("was not able to verify Public to all corporate users(Status Driven) Campaign created by Corporate user who does not privilege to see private campaign by Franchisee owner user(mu)");
			}
			
			//Verify Private Campaign created by Corporate user who has privilege to see private campaign by Franchisee mu user(V1)
			fc.utobj().printTestStep("Verify Private Campaign created by Corporate user(Status Driven) who does not have privilege to see private campaign by Franchisee owner user(mu)");
			campaignCenterPage.searchCampaignByFilter(driver, thirdcampaignNamecorpstatus);
			
			boolean isCampaignNamePresentCorpaccessibilityprivatecorpstatusV1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + thirdcampaignNamecorpstatus + "')]");
			if (!isCampaignNamePresentCorpaccessibilityprivatecorpstatusV1 == false) {
				fc.utobj().throwsException("was not able to Private Campaign created by Corporate user(Status Driven) who does not have privilege to see private campaign by Franchisee owner user(mu)");
			}
				
			fc.utobj().printTestStep("Navigate to CRM>Campaign Center>email template");
			emailtemplate.clickonemailtamplate(driver);
			
			//Verify Public to all users template created by Corporate user who does not have privilege to see private template by Franchisee mu user(V1)
			fc.utobj().printTestStep("Verify Public to all users template created by Corporate user who does not have privilege to see private template by Franchisee owner user(mu)");
			campaignCenterPage.searchTemplateByFilter(driver, firsttemplateNamecorp);

			boolean isTemplateNamePresentCorpaccessibilitypublictoalluserscorpV1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firsttemplateNamecorp + "')]");
			if (isTemplateNamePresentCorpaccessibilitypublictoalluserscorpV1 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users template created by Corporate user who has privilege to see private template by Franchisee owner user(mu)");
			}
			//Verify Public to all corporate users template created by Corporate user who has privilege to see private template by Franchisee mu user(V1)
			fc.utobj().printTestStep("Verify Public to all corporate users template created by Corporate user who does not have privilege to see private template by Franchisee owner user(mu)");
			campaignCenterPage.searchTemplateByFilter(driver, secondtemplateNamecorp);
			
			boolean isTemplateNamePresentCorpaccessibilitypublictoallcorporateuserscorpV1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondtemplateNamecorp + "')]");
			if (!isTemplateNamePresentCorpaccessibilitypublictoallcorporateuserscorpV1 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users template created by Corporate user who has privilege to see private template by Franchisee owner user(mu)");
			}
			
			//Verify Private template created by Corporate user who has privilege to see private template by Franchisee mu user(V1)
			fc.utobj().printTestStep("Verify Privare template created by Corporate user who does not have privilege to see private template by Franchisee owner user(mu)");
			campaignCenterPage.searchTemplateByFilter(driver, thirdtemplateNamecorp);
			
			boolean isTemplateNamePresentCorpaccessibilityprivatecorpV1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + thirdtemplateNamecorp + "')]");
			if (!isTemplateNamePresentCorpaccessibilityprivatecorpV1 == false) {
				fc.utobj().throwsException("was not able to verify private template created by Corporate user who has privilege to see private template by Franchisee owner user(mu)");
			}
			
			//Verify Public to all users (Status driven)template created by Corporate user who does not have privilege to see private template by Franchisee mu user(V1)
			fc.utobj().printTestStep("Verify Public to all users (Status driven)template created by Corporate user who does not have privilege to see private template by Franchisee owner user(mu)");
			campaignCenterPage.searchTemplateByFilter(driver, firsttemplateNamecorpstatus);

			boolean isTemplateNamePresentCorpaccessibilitypublictoalluserscorpstatusV1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firsttemplateNamecorpstatus + "')]");
			if (isTemplateNamePresentCorpaccessibilitypublictoalluserscorpstatusV1 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users (Status driven)template created by Corporate user who has privilege to see private template by Franchisee owner user(mu)");
			}
			//Verify Public to all corporate users template created by Corporate user who has privilege to see private template by Franchisee mu user(V1)
			fc.utobj().printTestStep("Verify Public to all corporate users (Status driven)template created by Corporate user who does not have privilege to see private template by Franchisee owner user(mu)");
			campaignCenterPage.searchTemplateByFilter(driver, secondtemplateNamecorpstatus);
			
			boolean isTemplateNamePresentCorpaccessibilitypublictoallcorporateuserscorpstatusV1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondtemplateNamecorpstatus + "')]");
			if (!isTemplateNamePresentCorpaccessibilitypublictoallcorporateuserscorpstatusV1 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users (Status driven)template created by Corporate user who has privilege to see private template by Franchisee owner user(mu)");
			}
			
			//Verify Private template created by Corporate user who has privilege to see private template by Franchisee mu user(V1)
			fc.utobj().printTestStep("Verify Privare (Status driven)template created by Corporate user who does not have privilege to see private template by Franchisee owner user(mu)");
			campaignCenterPage.searchTemplateByFilter(driver, thirdtemplateNamecorpstatus);
			
			boolean isTemplateNamePresentCorpaccessibilityprivatecorpstatusV1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + thirdtemplateNamecorpstatus + "')]");
			if (!isTemplateNamePresentCorpaccessibilityprivatecorpstatusV1 == false) {
				fc.utobj().throwsException("was not able to verify private (Status driven)template created by Corporate user who has privilege to see private template by Franchisee owner user(mu)");
			}
		
			fc.utobj().printTestStep("Navigate to CRM>Campaign Center>campaign");
			emailtemplate.clickoncampaigntemplate(driver);
			fc.utobj().clickElement(driver, crmcenter.corporatCampaign);
			//Verify Public to all users of my region Campaign created by Regional user who has privilege to see private campaign by Franchisee mu user(V1)(R1)
			//Verifying Campaigns and templates of Regional user (R1) after logging with Franchisee owner user(mu)
			fc.utobj().printTestStep("Verifying Campaigns and templates of Regional user (R1) after logging with Franchisee owner user(mu)");
			fc.utobj().printTestStep("Verify Public to all users of my region Campaign created by Regional user who has privilege to see private campaign by Franchisee owner user(mu)");
			campaignCenterPage.searchCampaignByFilter(driver, firstcampaignNamereg);

			boolean isCampaignNamePresentRegpaccessibilitypublictoallusersofmyregionV1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firstcampaignNamereg + "')]");
			if (isCampaignNamePresentRegpaccessibilitypublictoallusersofmyregionV1 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my region Campaign created by Regional user who has privilege to see private campaign by Franchisee owner user(mu)");
			}
			
			//Verify Private Campaign created by Regional user who has privilege to see private campaign by Franchisee mu user(V1)
			fc.utobj().printTestStep("Verify Private Campaign created by Regional user who has privilege to see private campaign by Franchisee mu user(V1)");
			campaignCenterPage.searchCampaignByFilter(driver, secondcampaignNamereg);
			
			boolean isCampaignNamePresentRegpaccessibilityprivateV1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondcampaignNamereg + "')]");
			if (!isCampaignNamePresentRegpaccessibilityprivateV1 == false) {
				fc.utobj().throwsException("was not able to verify Private Campaign created by Regional user who has privilege to see private campaign by Franchisee owner user(mu)");
			}
			
			//Verify Public to all users of my region (Status Driven)Campaign created by Regional user who has privilege to see private campaign by Franchisee mu user(V1)
			fc.utobj().printTestStep("Verify Public to all users of my region (Status Driven)Campaign created by Regional user who has privilege to see private campaign by Franchisee owner user(mu)");
			campaignCenterPage.searchCampaignByFilter(driver, firstcampaignNameregstatus);

			boolean isCampaignNamePresentRegpaccessibilitypublictoallusersofmyregionstatusV1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firstcampaignNameregstatus + "')]");
			if (isCampaignNamePresentRegpaccessibilitypublictoallusersofmyregionstatusV1 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my region (Status Driven)Campaign created by Regional user who has privilege to see private campaign by Franchisee owner user(mu)");
			}
			
			//Verify Private Campaign created by Regional user who has privilege to see private campaign by Franchisee mu user(V1)
			fc.utobj().printTestStep("Verify Private (Status Driven)Campaign created by Regional user who has privilege to see private campaign by Franchisee owner user(mu)");
			campaignCenterPage.searchCampaignByFilter(driver, secondcampaignNameregstatus);
			
			boolean isCampaignNamePresentRegpaccessibilityprivatestatusV1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondcampaignNameregstatus + "')]");
			if (!isCampaignNamePresentRegpaccessibilityprivatestatusV1 == false) {
				fc.utobj().throwsException("was not able to verify Private (Status Driven)Campaign created by Regional user who has privilege to see private campaign by Franchisee owner user(mu)");
			}
			
			fc.utobj().printTestStep("Navigate to CRM>Campaign Center>email template");
			emailtemplate.clickonemailtamplate(driver);
			
			//Verify Public to all users template created by Corporate user who has privilege to see private template by Franchisee mu user(V1)
			fc.utobj().printTestStep("Verify Public to all users of my region template created by Regional user who has privilege to see private template by Franchisee owner user(mu)");
			campaignCenterPage.searchTemplateByFilter(driver, firsttemplateNamereg);

			boolean isTemplateNamePresentRegaccessibilitypublictoallusersofmyregionV1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firsttemplateNamereg + "')]");
			if (isTemplateNamePresentRegaccessibilitypublictoallusersofmyregionV1 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users template created by Regional user who has privilege to see private template by Franchisee owner user(mu)");
			}
			
			//Verify Private template created by Regional user who has privilege to see private template by Franchisee mu user(V1)
			fc.utobj().printTestStep("Verify Private template created by Regional user who has privilege to see private template by Franchisee owner user(mu)");
			campaignCenterPage.searchTemplateByFilter(driver, secondtemplateNamereg);
			
			boolean isTemplateNamePresentRegaccessibilityprivateV1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondtemplateNamereg + "')]");
			if (!isTemplateNamePresentRegaccessibilityprivateV1 == false) {
				fc.utobj().throwsException("was not able to verify private template created by Regional user who has privilege to see private template by Franchisee owner user(mu)");
			}
			
			//Verify Public to all users(Status Driven) template created by Regional user who has privilege to see private template by Franchisee mu user(V1)
			fc.utobj().printTestStep("Verify Public to all users of my region (Status Driven)template created by Regional user who has privilege to see private template by Franchisee owner user(mu)");
			campaignCenterPage.searchTemplateByFilter(driver, firsttemplateNameregstatus);

			boolean isTemplateNamePresentRegaccessibilitypublictoallusersofmyregionstatusV1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firsttemplateNameregstatus + "')]");
			if (isTemplateNamePresentRegaccessibilitypublictoallusersofmyregionstatusV1 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my region (Status Driven)template created by Regional user who has privilege to see private template by Franchisee owner user(mu)");
			}
			
			//Verify Private template created by Regional user who has privilege to see private template by Franchisee mu user(V1)
			fc.utobj().printTestStep("Verify Private (Status Driven)template created by Regional user who has privilege to see private template by Franchisee owner user(mu)");
			campaignCenterPage.searchTemplateByFilter(driver, secondtemplateNameregstatus);
			
			boolean isTemplateNamePresentRegaccessibilityprivatestatusV1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondtemplateNameregstatus + "')]");
			if (!isTemplateNamePresentRegaccessibilityprivatestatusV1 == false) {
				fc.utobj().throwsException("was not able to verify private (Status Driven)template created by Regional user who has privilege to see private template by Franchisee owner user(mu)");
			}
			fc.utobj().printTestStep("Navigate to CRM>Campaign Center>campaign template");
			emailtemplate.clickoncampaigntemplate(driver);
			fc.utobj().clickElement(driver, crmcenter.corporatCampaign);
			
			//Verify Public to all users Campaign created by Corporate user who has privilege to see private campaign by Franchisee mu user(V1)
			fc.utobj().printTestStep("Verifying Campaigns and templates of C1 by Franchisee mu user(V1)");
			fc.utobj().printTestStep("Verify Public to all users Campaign created by Corporate user who has privilege to see private campaign by Franchisee mu user(V1)");
			campaignCenterPage.searchCampaignByFilter(driver, firstcampaignName);

			boolean isCampaignNamePresentCorpaccessibilitypublictoallusersV1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firstcampaignName + "')]");
			if (isCampaignNamePresentCorpaccessibilitypublictoallusersV1 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users Campaign created by Corporate user who has privilege to see private campaign by Franchisee mu user(V1)");
			}
			//Verify Public to all corporate users Campaign created by Corporate user who has privilege to see private campaign by Franchisee mu user(V1)
			fc.utobj().printTestStep("Verify Public to all corporate users Campaign created by Corporate user who has privilege to see private campaign");
			campaignCenterPage.searchCampaignByFilter(driver, secondcampaignName);
			
			boolean isCampaignNamePresentCorpaccessibilitypublictoallcorporateusersV1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondcampaignName + "')]");
			if (!isCampaignNamePresentCorpaccessibilitypublictoallcorporateusersV1 == false) {
				fc.utobj().throwsException("was not able to verify Public to all corporate users Campaign created by Corporate user who has privilege to see private campaign by Franchisee mu user(V1)");
			}
			
			//Verify Private Campaign created by Corporate user who has privilege to see private campaign by Franchisee mu user(V1)
			fc.utobj().printTestStep("Verify Private Campaign created by Corporate user who has privilege to see private campaign by Franchisee mu user(V1)");
			campaignCenterPage.searchCampaignByFilter(driver, thirdcampaignName);
			
			boolean isCampaignNamePresentCorpaccessibilityprivateV1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + thirdcampaignName + "')]");
			if (!isCampaignNamePresentCorpaccessibilityprivateV1 == false) {
				fc.utobj().throwsException("was not able to Private Campaign created by Corporate user who has privilege to see private campaign by Franchisee mu user(V1)");
			}
			
			//Verify Public to all users(Status Driven) Campaign created by Corporate user who has privilege to see private campaign by Franchisee mu user(V1)
			fc.utobj().printTestStep("Verify Public to all users(Status Driven) Campaign created by Corporate user who has privilege to see private campaign by Franchisee mu user(V1)");
			campaignCenterPage.searchCampaignByFilter(driver, firstcampaignNamestatus);

			boolean isCampaignNamePresentCorpaccessibilitypublictoallusersstatusV1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firstcampaignNamestatus + "')]");
			if (isCampaignNamePresentCorpaccessibilitypublictoallusersstatusV1 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users(Status Driven) Campaign created by Corporate user who has privilege to see private campaign by Franchisee mu user(V1)");
			}
			//Verify Public to all corporate users(Status Driven) Campaign created by Corporate user who has privilege to see private campaign by Franchisee mu user(V1)
			fc.utobj().printTestStep("Verify Public to all corporate users(Status Driven) Campaign created by Corporate user who has privilege to see private campaign");
			campaignCenterPage.searchCampaignByFilter(driver, secondcampaignNamestatus);
			
			boolean isCampaignNamePresentCorpaccessibilitypublictoallcorporateusersstatusV1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondcampaignNamestatus + "')]");
			if (!isCampaignNamePresentCorpaccessibilitypublictoallcorporateusersstatusV1 == false) {
				fc.utobj().throwsException("was not able to verify Public to all corporate users(Status Driven) Campaign created by Corporate user who has privilege to see private campaign by Franchisee mu user(V1)");
			}
			
			//Verify Private Campaign created by Corporate user(Status Driven) who has privilege to see private campaign by Franchisee mu user(V1)
			fc.utobj().printTestStep("Verify Private Campaign(Status Driven) created by Corporate user who has privilege to see private campaign by Franchisee mu user(V1)");
			campaignCenterPage.searchCampaignByFilter(driver, thirdcampaignNamestatus);
			
			boolean isCampaignNamePresentCorpaccessibilityprivatestatusV1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + thirdcampaignNamestatus + "')]");
			if (!isCampaignNamePresentCorpaccessibilityprivatestatusV1 == false) {
				fc.utobj().throwsException("was not able to Private Campaign(Status Driven) created by Corporate user who has privilege to see private campaign by Franchisee mu user(V1)");
			}
		
			fc.utobj().printTestStep("Navigate to CRM>Campaign Center>email template");
			emailtemplate.clickonemailtamplate(driver);
			
			//Verify Public to all users template created by Corporate user who has privilege to see private template by Franchisee mu user(V1)
			fc.utobj().printTestStep("Verify Public to all users template created by Corporate user who has privilege to see private template by Franchisee mu user(V1)");
			campaignCenterPage.searchTemplateByFilter(driver, firsttemplateName);

			boolean isTemplateNamePresentCorpaccessibilitypublictoallusersV1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firsttemplateName + "')]");
			if (isTemplateNamePresentCorpaccessibilitypublictoallusersV1 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users template created by Corporate user who has privilege to see private template by Franchisee mu user(V1)");
			}
			//Verify Public to all corporate users template created by Corporate user who has privilege to see private template by Franchisee mu user(V1)
			fc.utobj().printTestStep("Verify Public to all corporate users template created by Corporate user who has privilege to see private template by Franchisee mu user(V1)");
			campaignCenterPage.searchTemplateByFilter(driver, secondtemplateName);
			
			boolean isTemplateNamePresentCorpaccessibilitypublictoallcorporateusersV1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondtemplateName + "')]");
			if (!isTemplateNamePresentCorpaccessibilitypublictoallcorporateusersV1 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users template created by Corporate user who has privilege to see private template by Franchisee mu user(V1)");
			}
			
			//Verify Private template created by Corporate user who has privilege to see private template by Franchisee mu user(V1)
			fc.utobj().printTestStep("Verify Privare template created by Corporate user who has privilege to see private template by Franchisee mu user(V1)");
			campaignCenterPage.searchTemplateByFilter(driver, thirdtemplateName);
			
			boolean isTemplateNamePresentCorpaccessibilityprivateV1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + thirdtemplateName + "')]");
			if (!isTemplateNamePresentCorpaccessibilityprivateV1 == false) {
				fc.utobj().throwsException("was not able to verify private template created by Corporate user who has privilege to see private template by Franchisee mu user(V1)");
			}
				
			//Verify Public to all users template(Status Driven) created by Corporate user who has privilege to see private template by Franchisee mu user(V1)
			fc.utobj().printTestStep("Verify Public to all users template(Status Driven) created by Corporate user who has privilege to see private template by Franchisee mu user(V1)");
			campaignCenterPage.searchTemplateByFilter(driver, firsttemplateNamestatus);

			boolean isTemplateNamePresentCorpaccessibilitypublictoallusersstatusV1= fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firsttemplateNamestatus + "')]");
			if (isTemplateNamePresentCorpaccessibilitypublictoallusersstatusV1 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users template(Status Driven) created by Corporate user who has privilege to see private template by Franchisee mu user(V1)");
			}
			//Verify Public to all corporate users template(Status Driven) created by Corporate user who has privilege to see private template by Franchisee mu user(V1)
			fc.utobj().printTestStep("Verify Public to all corporate users template(Status Driven) created by Corporate user who has privilege to see private template by Franchisee mu user(V1)");
			campaignCenterPage.searchTemplateByFilter(driver, secondtemplateNamestatus);
			
			boolean isTemplateNamePresentCorpaccessibilitypublictoallcorporateusersstatusV1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondtemplateNamestatus + "')]");
			if (!isTemplateNamePresentCorpaccessibilitypublictoallcorporateusersstatusV1 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users template(Status Driven) created by Corporate user who has privilege to see private template by Franchisee mu user(V1)");
			}
			
			//Verify Private template(Status Driven) created by Corporate user who has privilege to see private template by Franchisee mu user(V1)
			fc.utobj().printTestStep("Verify Privare template(Status Driven) created by Corporate user who has privilege to see private template by Franchisee mu user(V1)");
			campaignCenterPage.searchTemplateByFilter(driver, thirdtemplateNamestatus);
			
			boolean isTemplateNamePresentCorpaccessibilityprivatestatusV1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + thirdtemplateNamestatus + "')]");
			if (!isTemplateNamePresentCorpaccessibilityprivatestatusV1 == false) {
				fc.utobj().throwsException("was not able to verify private template(Status Driven) created by Corporate user who has privilege to see private template by Franchisee mu user(V1)");
			}
		
			fc.utobj().printTestStep("Navigate to CRM>Campaign Center>campaign template");
			emailtemplate.clickoncampaigntemplate(driver);
			fc.utobj().clickElement(driver, crmcenter.corporatCampaign);
			//Verify Public to all users Campaign created by Corporate user who has privilege to see private campaign by Franchisee mu user(V1)
			fc.utobj().printTestStep("Verifying Campaigns and templates of Regional user (R2) by Franchisee owner user(mu)");
			//Verify Public to all users of my region Campaign created by Regional user who does not have privilege to see private campaign by Franchisee mu user(V1)
			fc.utobj().printTestStep("Verify Public to all users of my region Campaign created by Regional user who does not have privilege to see private campaign by Franchisee owner user(mu)");
			campaignCenterPage.searchCampaignByFilter(driver, firstcampaignNamereg1);

			boolean isCampaignNamePresentRegpaccessibilitypublictoallusersofmyregion1V1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firstcampaignNamereg1 + "')]");
			if (isCampaignNamePresentRegpaccessibilitypublictoallusersofmyregion1V1 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my region Campaign created by Regional user who does not have privilege to see private campaign by Franchisee owner user(mu)");
			}
			
			//Verify Private Campaign created by Regional user who does not have privilege to see private campaign by Franchisee mu user(V1)
			fc.utobj().printTestStep("Verify Private Campaign created by Regional user who does not have privilege to see private campaign by Franchisee owner user(mu)");
			campaignCenterPage.searchCampaignByFilter(driver, secondcampaignNamereg1);
			
			boolean isCampaignNamePresentRegpaccessibilityprivate1V1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondcampaignNamereg1 + "')]");
			if (!isCampaignNamePresentRegpaccessibilityprivate1V1 == false) {
				fc.utobj().throwsException("was not able to verify Private Campaign created by Regional user who does not have privilege to see private campaign by Franchisee owner user(mu)");
			}
			
			//Verify Public to all users of my region (Status Driven)Campaign created by Regional user who does not have privilege to see private campaign by Franchisee mu user(V1)
			fc.utobj().printTestStep("Verify Public to all users of my region (Status Driven)Campaign created by Regional user who does not have privilege to see private campaign by Franchisee owner user(mu)");
			campaignCenterPage.searchCampaignByFilter(driver, firstcampaignNamereg1status);

			boolean isCampaignNamePresentRegpaccessibilitypublictoallusersofmyregion1statusV1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firstcampaignNamereg1status + "')]");
			if (isCampaignNamePresentRegpaccessibilitypublictoallusersofmyregion1statusV1 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my region (Status Driven)Campaign created by Regional user who does not have privilege to see private campaign by Franchisee owner user(mu)");
			}
			
			//Verify Private (Status Driven)Campaign created by Regional user who does not have privilege to see private campaign by Franchisee mu user(V1)
			fc.utobj().printTestStep("Verify Private (Status Driven)Campaign created by Regional user who does not have privilege to see private campaign by Franchisee owner user(mu)");
			campaignCenterPage.searchCampaignByFilter(driver, secondcampaignNamereg1status);
			
			boolean isCampaignNamePresentRegpaccessibilityprivate1statusV1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondcampaignNamereg1status + "')]");
			if (!isCampaignNamePresentRegpaccessibilityprivate1statusV1 == false) {
				fc.utobj().throwsException("was not able to verify Private (Status Driven)Campaign created by Regional user who does not have privilege to see private campaign by Franchisee owner user(mu)");
			}
		
			fc.utobj().printTestStep("Navigate to CRM>Campaign Center>email template");
			emailtemplate.clickonemailtamplate(driver);
			
			//Verify Public to all users template created by Corporate user who does not have privilege to see private template by Franchisee mu user(V1)
			fc.utobj().printTestStep("Verify Public to all users of my region template created by Regional user who does not have privilege to see private template by Franchisee owner user(mu)");
			campaignCenterPage.searchTemplateByFilter(driver, firsttemplateNamereg1);

			boolean isTemplateNamePresentRegaccessibilitypublictoallusersofmyregion1V1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firsttemplateNamereg1 + "')]");
			if (isTemplateNamePresentRegaccessibilitypublictoallusersofmyregion1V1 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my region template created by Regional user who does not have privilege to see private template by Franchisee owner user(mu)");
			}
			
			//Verify Private template created by Regional user who does not have privilege to see private template by Franchisee mu user(V1)
			fc.utobj().printTestStep("Verify Private template created by Regional user does not have privilege to see private template by Franchisee owner user(mu)");
			campaignCenterPage.searchTemplateByFilter(driver, secondtemplateNamereg1);
			
			boolean isTemplateNamePresentRegaccessibilityprivate1V1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondtemplateNamereg1 + "')]");
			if (!isTemplateNamePresentRegaccessibilityprivate1V1 == false) {
				fc.utobj().throwsException("was not able to verify Private template created by Regional user does not have privilege to see private template by Franchisee owner user(mu)");
			}
			
			//Verify Public to all users (Status Driven)template created by Regional user who does not have privilege to see private template by Franchisee mu user(V1)
			fc.utobj().printTestStep("Verify Public to all users of my region (Status Driven)template created by Regional user who does not have privilege to see private template by Franchisee owner user(mu)");
			campaignCenterPage.searchTemplateByFilter(driver, firsttemplateNamereg1status);

			boolean isTemplateNamePresentRegaccessibilitypublictoallusersofmyregion1statusV1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firsttemplateNamereg1status + "')]");
			if (isTemplateNamePresentRegaccessibilitypublictoallusersofmyregion1statusV1 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my region (Status Driven)template created by Regional user who does not have privilege to see private template by Franchisee owner user(mu)");
			}
			
			//Verify Private template created by Regional user who does not have privilege to see private template by Franchisee mu user(V1)
			fc.utobj().printTestStep("Verify Private (Status Driven)template created by Regional user does not have privilege to see private template by Franchisee owner user(mu)");
			campaignCenterPage.searchTemplateByFilter(driver, secondtemplateNamereg1status);
			
			boolean isTemplateNamePresentRegaccessibilityprivate1statusV1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondtemplateNamereg1status + "')]");
			if (!isTemplateNamePresentRegaccessibilityprivate1statusV1 == false) {
				fc.utobj().throwsException("was not able to verify Private (Status Driven)template created by Regional user does not have privilege to see private template by Franchisee owner user(mu)");
			}
			
			fc.utobj().printTestStep("Navigate to CRM>Campaign Center>campaign template");
			emailtemplate.clickoncampaigntemplate(driver);
			
			fc.utobj().printTestStep("Verifying Campaigns and templates of Franchise owner user (F0New) by Franchisee owner user(mu)(V1)");
			fc.utobj().printTestStep("Verify Public to all users of my Franchise Campaign created by Franchise user who has privilege to see private campaign by Franchisee owner user(mu)");
			campaignCenterPage.searchCampaignByFilter(driver, firstcampaignNamefran);

			boolean isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchiseV1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firstcampaignNamefran + "')]");
			if (isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchiseV1 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my Franchise Campaign created by Franchise user who has privilege to see private campaign by Franchisee owner user(mu)");
			}
			
			//Verify Private Campaign created by Franchise user who has privilege to see private campaign by Franchisee mu user(V1)
			fc.utobj().printTestStep("Verify Private Campaign created by Franchise user who has privilege to see private campaign by Franchisee owner user(mu)");
			campaignCenterPage.searchCampaignByFilter(driver, secondcampaignNamefran);
			
			boolean isCampaignNamePresentFranpaccessibilityprivateV1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondcampaignNamefran + "')]");
			if (isCampaignNamePresentFranpaccessibilityprivateV1 == false) {
				fc.utobj().throwsException("was not able to verify Private Campaign created by Franchise user who has privilege to see private campaign by Franchisee owner user(mu)");
			}
			
			
			//Verify Public to all users of my Franchise (Status Driven)Campaign created by Franchise user who has privilege to see private campaign by Franchisee mu user(V1)
			fc.utobj().printTestStep("Verify Public to all users of my Franchise (Status Driven)Campaign created by Franchise user who has privilege to see private campaign by Franchisee owner user(mu)");
			campaignCenterPage.searchCampaignByFilter(driver, firstcampaignNamefranstatus);

			boolean isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchisestatusV1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firstcampaignNamefranstatus + "')]");
			if (isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchisestatusV1 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my Franchise (Status Driven)Campaign created by Franchise user who has privilege to see private campaign by Franchisee owner user(mu)");
			}
			
			//Verify Private Campaign created by Franchise user who has privilege to see private campaign by Franchisee mu user(V1)
			fc.utobj().printTestStep("Verify Private (Status Driven)Campaign created by Franchise user who has privilege to see private campaign by Franchisee owner user(mu)");
			campaignCenterPage.searchCampaignByFilter(driver, secondcampaignNamefranstatus);
			
			boolean isCampaignNamePresentFranpaccessibilityprivatestatusV1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondcampaignNamefranstatus + "')]");
			if (isCampaignNamePresentFranpaccessibilityprivatestatusV1 == false) {
				fc.utobj().throwsException("was not able to verify Private (Status Driven)Campaign created by Franchise user who has privilege to see private campaign by Franchisee owner user(mu)");
			}
			
			fc.utobj().printTestStep("Navigate to CRM>Campaign Center>email template");
			emailtemplate.clickonemailtamplate(driver);
			fc.utobj().clickElement(driver, crmcenter.locationtemplate);
			//Verify Public to all users template created by Corporate user who has privilege to see private template by Franchisee mu user(V1)
			fc.utobj().printTestStep("Verify Public to all users of my franchise template created by franchise user who has privilege to see private template by Franchisee owner user(mu)");
			campaignCenterPage.searchTemplateByFilter(driver, firsttemplateNamefran);

			boolean isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchiseV1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firsttemplateNamefran + "')]");
			if (isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchiseV1 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my franchise template created by franchise user who has privilege to see private template by Franchisee owner user(mu)");
			}
			
			//Verify Private template created by franchise user who has privilege to see private template by Franchisee mu user(V1)
			fc.utobj().printTestStep("Verify Private template created by franchise user who has privilege to see private template by Franchisee owner user(mu)");
			campaignCenterPage.searchTemplateByFilter(driver, secondtemplateNamefran);
			
			boolean isTemplateNamePresentfranaccessibilityprivateV1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondtemplateNamefran + "')]");
			if (isTemplateNamePresentfranaccessibilityprivateV1 == false) {
				fc.utobj().throwsException("was not able to verify private template created by franchise user who has privilege to see private template by Franchisee owner user(mu)");
			}
			
			//Verify Public to all users (Status Driven)template created by Corporate user who has privilege to see private template by Franchisee mu user(V1)
			fc.utobj().printTestStep("Verify Public to all users of my franchise (Status Driven)template created by franchise user who has privilege to see private template by Franchisee owner user(mu)");
			campaignCenterPage.searchTemplateByFilter(driver, firsttemplateNamefranstatus);

			boolean isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchisestatusV1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firsttemplateNamefranstatus + "')]");
			if (isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchisestatusV1 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my franchise (Status Driven)template created by franchise user who has privilege to see private template by Franchisee owner user(mu)");
			}
			
			//Verify Private template created by franchise user who has privilege to see private template by Franchisee mu user(V1)
			fc.utobj().printTestStep("Verify Private (Status Driven)template created by franchise user who has privilege to see private template by Franchisee owner user(mu)");
			campaignCenterPage.searchTemplateByFilter(driver, secondtemplateNamefranstatus);
			
			boolean isTemplateNamePresentfranaccessibilityprivatestatusV1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondtemplateNamefranstatus + "')]");
			if (isTemplateNamePresentfranaccessibilityprivatestatusV1 == false) {
				fc.utobj().throwsException("was not able to verify private (Status Driven)template created by franchise user who has privilege to see private template by Franchisee owner user(mu)");
			}
			
			fc.utobj().printTestStep("Navigate to CRM>Campaign Center>campaign template");
			emailtemplate.clickoncampaigntemplate(driver);
			
			fc.utobj().printTestStep("Verifying Campaigns and templates of Franchise mu owner user (V1) by itself");
			
			fc.utobj().printTestStep("Verify Public to all users of my Franchise Campaign created by Franchise MU user(V1) who has privilege to see private campaign by itself");
			campaignCenterPage.searchCampaignByFilter(driver, firstcampaignNamefranmu);

			boolean isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchisemuV1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firstcampaignNamefranmu + "')]");
			if (isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchisemuV1 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my Franchise Campaign created by Franchise MU user(V1) who has privilege to see private campaign by Franchisee owner user(mu)");
			}
			
			//Verify Private Campaign created by Franchise user who has privilege to see private campaign by Franchisee mu user(V1)
			fc.utobj().printTestStep("Verify Private Campaign created by Franchise MU user(V1) who has privilege to see private campaign by Franchisee owner user(mu)");
			campaignCenterPage.searchCampaignByFilter(driver, secondcampaignNamefranmu);
			
			boolean isCampaignNamePresentFranpaccessibilityprivatemuV1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondcampaignNamefranmu + "')]");
			if (isCampaignNamePresentFranpaccessibilityprivatemuV1 == false) {
				fc.utobj().throwsException("was not able to verify Private Campaign created by Franchise MU user(V1) who has privilege to see private campaign by Franchisee owner user(mu)");
			}
			
			
			//Verify Public to all users of my Franchise (Status Driven)Campaign created by Franchise user who has privilege to see private campaign by Franchisee mu user(V1)
			fc.utobj().printTestStep("Verify Public to all users of my Franchise (Status Driven)Campaign created by Franchise MU user(V1) who has privilege to see private campaign by Franchisee owner user(mu)");
			campaignCenterPage.searchCampaignByFilter(driver, firstcampaignNamefranmustatus);

			boolean isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchisemustatusV1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firstcampaignNamefranmustatus + "')]");
			if (isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchisemustatusV1 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my Franchise (Status Driven)Campaign created by Franchise MU user(V1) who has privilege to see private campaign by Franchisee owner user(mu)");
			}
			
			//Verify Private Campaign created by Franchise user who has privilege to see private campaign by Franchisee mu user(V1)
			fc.utobj().printTestStep("Verify Private (Status Driven)Campaign created by Franchise MU user(V1) who has privilege to see private campaign by Franchisee owner user(mu)");
			campaignCenterPage.searchCampaignByFilter(driver, secondcampaignNamefranmustatus);
			
			boolean isCampaignNamePresentFranpaccessibilityprivatemustatusV1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondcampaignNamefranmustatus + "')]");
			if (isCampaignNamePresentFranpaccessibilityprivatemustatusV1 == false) {
				fc.utobj().throwsException("was not able to verify Private (Status Driven)Campaign created by Franchise MU user(V1) who has privilege to see private campaign by Franchisee owner user(mu)");
			}
			
			fc.utobj().printTestStep("Navigate to CRM>Campaign Center>email template");
			emailtemplate.clickonemailtamplate(driver);
			fc.utobj().clickElement(driver, crmcenter.locationtemplate);
			//Verify Public to all users template created by Corporate user who has privilege to see private template by Franchisee mu user(V1)
			fc.utobj().printTestStep("Verify Public to all users of my franchise template created by franchise MU user(V1) who has privilege to see private template by Franchisee owner user(mu)");
			campaignCenterPage.searchTemplateByFilter(driver, firsttemplateNamefranmu);

			boolean isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchisemuV1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firsttemplateNamefranmu + "')]");
			if (isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchisemuV1 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my franchise template created by franchise MU user(V1) who has privilege to see private template by Franchisee owner user(mu)");
			}
			
			//Verify Private template created by franchise user who has privilege to see private template by Franchisee mu user(V1)
			fc.utobj().printTestStep("Verify Private template created by franchise MU user(V1) who has privilege to see private template by Franchisee owner user(mu)");
			campaignCenterPage.searchTemplateByFilter(driver, secondtemplateNamefranmu);
			
			boolean isTemplateNamePresentfranaccessibilityprivatemuV1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondtemplateNamefranmu + "')]");
			if (isTemplateNamePresentfranaccessibilityprivatemuV1 == false) {
				fc.utobj().throwsException("was not able to verify private template created by franchise MU user(V1) who has privilege to see private template by Franchisee owner user(mu)");
			}
			
			//Verify Public to all users template created by Corporate user who has privilege to see private template by Franchisee mu user(V1)
			fc.utobj().printTestStep("Verify Public to all users of my franchise (Status Driven)template created by franchise MU user(V1) who has privilege to see private template by Franchisee owner user(mu)");
			campaignCenterPage.searchTemplateByFilter(driver, firsttemplateNamefranmustatus);

			boolean isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchisemustatusV1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firsttemplateNamefranmustatus + "')]");
			if (isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchisemustatusV1 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my franchise (Status Driven)template created by franchise MU user(V1) who has privilege to see private template by Franchisee owner user(mu)");
			}
			
			//Verify Private template created by franchise user who has privilege to see private template by Franchisee mu user(V1)
			fc.utobj().printTestStep("Verify Private (Status Driven)template created by franchise MU user(V1) who has privilege to see private template by Franchisee owner user(mu)");
			campaignCenterPage.searchTemplateByFilter(driver, secondtemplateNamefranmustatus);
			
			boolean isTemplateNamePresentfranaccessibilityprivatemustatusV1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondtemplateNamefranmustatus + "')]");
			if (isTemplateNamePresentfranaccessibilityprivatemustatusV1 == false) {
				fc.utobj().throwsException("was not able to verify private (Status Driven)template created by franchise MU user(V1) who has privilege to see private template by Franchisee owner user(mu)");
			}
			
			
			
			
			fc.utobj().printTestStep("Navigate to CRM>Campaign Center>campaign template");
			emailtemplate.clickoncampaigntemplate(driver);
			fc.utobj().printTestStep("Verifying Campaigns and templates of Franchise Employee user (V2) by Franchisee owner user(mu)");
			fc.utobj().printTestStep("Verify Public to all users of my Franchise Campaign created by Franchise Employee user(V2) who does not have privilege to see private campaign by Franchisee owner user(mu)");
			campaignCenterPage.searchCampaignByFilter(driver, firstcampaignNamefranmue2);

			boolean isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchisemue2V1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firstcampaignNamefranmue2 + "')]");
			if (isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchisemue2V1 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my Franchise Campaign created by Franchise Employee user(V2) who does not have privilege to see private campaign by Franchisee owner user(mu)");
			}
			
			//Verify Private Campaign created by Franchise user who has privilege to see private campaign by Franchisee mu user(V1)
			fc.utobj().printTestStep("Verify Private Campaign created by Franchise Employee user(V2) who does not have privilege to see private campaign by Franchisee owner user(mu)");
			campaignCenterPage.searchCampaignByFilter(driver, secondcampaignNamefranmue2);
			
			boolean isCampaignNamePresentFranpaccessibilityprivatemue2V1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondcampaignNamefranmue2 + "')]");
			if (isCampaignNamePresentFranpaccessibilityprivatemue2V1 == false) {
				fc.utobj().throwsException("was not able to verify Private Campaign created by Franchise Employee user(V2) who does not have privilege to see private campaign by Franchisee owner user(mu)");
			}
			
			
			//Verify Public to all users of my Franchise Campaign created by Franchise Employee user(V2) who has privilege to see private campaign by Franchisee mu user(V1)
			fc.utobj().printTestStep("Verify Public to all users of my Franchise (Status Driven)Campaign created by Franchise Employee user(V2) who does not have privilege to see private campaign by Franchisee owner user(mu)");
			campaignCenterPage.searchCampaignByFilter(driver, firstcampaignNamefranmue2status);

			boolean isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchisemue2statusV1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firstcampaignNamefranmue2status + "')]");
			if (isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchisemue2statusV1 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my Franchise (Status Driven)Campaign created by Franchise Employee user(V2) who does not have privilege to see private campaign by Franchisee owner user(mu)");
			}
			
			//Verify Private Campaign created by Franchise user who has privilege to see private campaign by Franchisee mu user(V1)
			fc.utobj().printTestStep("Verify Private (Status Driven)Campaign created by Franchise Employee user(V2) who does not have privilege to see private campaign by Franchisee owner user(mu)");
			campaignCenterPage.searchCampaignByFilter(driver, secondcampaignNamefranmue2status);
			
			boolean isCampaignNamePresentFranpaccessibilityprivatemue2statusV1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondcampaignNamefranmue2status + "')]");
			if (isCampaignNamePresentFranpaccessibilityprivatemue2statusV1 == false) {
				fc.utobj().throwsException("was not able to verify Private (Status Driven)Campaign created by Franchise Employee user(V2) who does not have privilege to see private campaign by Franchisee owner user(mu)");
			}
			
			fc.utobj().printTestStep("Navigate to CRM>Campaign Center>email template");
			emailtemplate.clickonemailtamplate(driver);
			fc.utobj().clickElement(driver, crmcenter.locationtemplate);
			//Verify Public to all users template created by Franchise user who has privilege to see private template by Franchisee mu user(V1)
			fc.utobj().printTestStep("Verify Public to all users of my franchise template created by franchise Employee user(V2) who does not have privilege to see private template by Franchisee owner user(mu)");
			campaignCenterPage.searchTemplateByFilter(driver, firsttemplateNamefranmue2);

			boolean isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchisemue2V1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firsttemplateNamefranmue2 + "')]");
			if (isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchisemue2V1 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my franchise template created by franchise Employee user(V2) who has privilege to see private template by Franchisee owner user(mu)");
			}
			
			//Verify Private template created by franchise user who has privilege to see private template by Franchisee mu user(V1)
			fc.utobj().printTestStep("Verify Private template created by franchise Employee user(V2) who has privilege to see private template by Franchisee owner user(mu)");
			campaignCenterPage.searchTemplateByFilter(driver, secondtemplateNamefranmue2);
			
			boolean isTemplateNamePresentfranaccessibilityprivatemue2V1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondtemplateNamefranmue2 + "')]");
			if (isTemplateNamePresentfranaccessibilityprivatemue2V1 == false) {
				fc.utobj().throwsException("was not able to verify private template created by franchise Employee user(V2) who has privilege to see private template by Franchisee owner user(mu)");
			}
			
			//Verify Public to all users template created by Franchise user who has privilege to see private template by Franchisee mu user(V1)
			fc.utobj().printTestStep("Verify Public to all users of my franchise (Status Driven)template created by franchise Employee user(V2) who does not have privilege to see private template by Franchisee owner user(mu)");
			campaignCenterPage.searchTemplateByFilter(driver, firsttemplateNamefranmue2status);

			boolean isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchisemue2statusV1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firsttemplateNamefranmue2status + "')]");
			if (isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchisemue2statusV1 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my franchise (Status Driven)template created by franchise Employee user(V2) who has privilege to see private template by Franchisee owner user(mu)");
			}
			
			//Verify Private template created by franchise user who has privilege to see private template by Franchisee mu user(V1)
			fc.utobj().printTestStep("Verify Private (Status Driven)template created by franchise Employee user(V2) who has privilege to see private template by Franchisee owner user(mu)");
			campaignCenterPage.searchTemplateByFilter(driver, secondtemplateNamefranmue2status);
			
			boolean isTemplateNamePresentfranaccessibilityprivatemue2statusV1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondtemplateNamefranmue2status + "')]");
			if (isTemplateNamePresentfranaccessibilityprivatemue2statusV1 == false) {
				fc.utobj().throwsException("was not able to verify private (Status Driven)template created by franchise Employee user(V2) who has privilege to see private template by Franchisee owner user(mu)");
			}
			
			if(isCampaignNamePresentCorpaccessibilitypublictoalluserscorpV1 && !isCampaignNamePresentCorpaccessibilitypublictoallcorporateuserscorpV1 && !isCampaignNamePresentCorpaccessibilityprivatecorpV1 
					&& isCampaignNamePresentCorpaccessibilitypublictoalluserscorpstatusV1 && !isCampaignNamePresentCorpaccessibilitypublictoallcorporateuserscorpstatusV1 && !isCampaignNamePresentCorpaccessibilityprivatecorpstatusV1 
					&& isTemplateNamePresentCorpaccessibilitypublictoalluserscorpV1 && !isTemplateNamePresentCorpaccessibilitypublictoallcorporateuserscorpV1 && !isTemplateNamePresentCorpaccessibilityprivatecorpV1 
					&& isTemplateNamePresentCorpaccessibilitypublictoalluserscorpstatusV1 && !isTemplateNamePresentCorpaccessibilitypublictoallcorporateuserscorpstatusV1 && !isTemplateNamePresentCorpaccessibilityprivatecorpstatusV1 
					&& isCampaignNamePresentRegpaccessibilitypublictoallusersofmyregionV1 && !isCampaignNamePresentRegpaccessibilityprivateV1 && isCampaignNamePresentRegpaccessibilitypublictoallusersofmyregionstatusV1 
					&& !isCampaignNamePresentRegpaccessibilityprivatestatusV1 && isTemplateNamePresentRegaccessibilitypublictoallusersofmyregionV1 && !isTemplateNamePresentRegaccessibilityprivateV1 
					&& isTemplateNamePresentRegaccessibilitypublictoallusersofmyregionstatusV1 && !isTemplateNamePresentRegaccessibilityprivatestatusV1 && isCampaignNamePresentCorpaccessibilitypublictoallusersV1 
					&& !isCampaignNamePresentCorpaccessibilitypublictoallcorporateusersV1 && !isCampaignNamePresentCorpaccessibilityprivateV1 && isCampaignNamePresentCorpaccessibilitypublictoallusersstatusV1 
					&& !isCampaignNamePresentCorpaccessibilitypublictoallcorporateusersstatusV1 && !isCampaignNamePresentCorpaccessibilityprivatestatusV1 && isTemplateNamePresentCorpaccessibilitypublictoallusersV1 
					&& !isTemplateNamePresentCorpaccessibilitypublictoallcorporateusersV1 && !isTemplateNamePresentCorpaccessibilityprivateV1 && isTemplateNamePresentCorpaccessibilitypublictoallusersstatusV1 
					&& !isTemplateNamePresentCorpaccessibilitypublictoallcorporateusersstatusV1 && !isTemplateNamePresentCorpaccessibilityprivatestatusV1 && isCampaignNamePresentRegpaccessibilitypublictoallusersofmyregion1V1
					&& !isCampaignNamePresentRegpaccessibilityprivate1V1 && isCampaignNamePresentRegpaccessibilitypublictoallusersofmyregion1statusV1 && !isCampaignNamePresentRegpaccessibilityprivate1statusV1
					&& isTemplateNamePresentRegaccessibilitypublictoallusersofmyregion1V1 && !isTemplateNamePresentRegaccessibilityprivate1V1 && isTemplateNamePresentRegaccessibilitypublictoallusersofmyregion1statusV1 
					&& !isTemplateNamePresentRegaccessibilityprivate1statusV1 && isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchiseV1 && isCampaignNamePresentFranpaccessibilityprivateV1 
					&& isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchisestatusV1 && isCampaignNamePresentFranpaccessibilityprivatestatusV1 && isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchiseV1 
					&& isTemplateNamePresentfranaccessibilityprivateV1 && isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchisestatusV1 && isTemplateNamePresentfranaccessibilityprivatestatusV1  
					&& isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchisemuV1 && isCampaignNamePresentFranpaccessibilityprivatemuV1 && isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchisemustatusV1 
					&& isCampaignNamePresentFranpaccessibilityprivatemustatusV1 && isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchisemuV1 && isTemplateNamePresentfranaccessibilityprivatemuV1 
					&& isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchisemustatusV1 && isTemplateNamePresentfranaccessibilityprivatemustatusV1 && isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchisemue2V1 
					&& isCampaignNamePresentFranpaccessibilityprivatemue2V1 && isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchisemue2statusV1 && isCampaignNamePresentFranpaccessibilityprivatemue2statusV1 
					&& isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchisemue2V1 && isTemplateNamePresentfranaccessibilityprivatemue2V1 && isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchisemue2statusV1
					&& isTemplateNamePresentfranaccessibilityprivatemue2statusV1) {
				fc.utobj().printTestStep("able to verify all types of campaigns and templates created by corporate/rgional/franchise users when loging with Franchisee owner user(mu)(V1) who has priviledge to see private campaign");
			}
			else {
				fc.utobj().throwsException("was not able to verify all types of campaigns and templates created by corporate/rgional/franchise users when loging with Franchisee owner user(mu)(V1) who has priviledge to see private campaign");
			}
			
			
			
			
			
			
			
			//Login with Franchisee Employee user(V2) who has role to see private campaign
			fc.utobj().printTestStep("Login with Franchisee Employee user(V2) who has role to see private campaign");
			fc.loginpage().loginWithParameter(driver, userNameFEmu, passwordFEmu);
			fc.crm().crm_common().openCRMCampaignCenterPage(fcHomePageTest, driver);
			fc.utobj().printTestStep("Click on the menu option and select campaign");
			fc.utobj().clickElement(driver, pobj1.menuOptions);
			fc.utobj().clickElement(driver, pobj1.campaignLinks);
			fc.utobj().clickElement(driver, crmcenter.corporatCampaign);
			//Verifying Campaigns and templates of corporate user (C2) after logging with Franchisee Employee user(V2)
			fc.utobj().printTestStep("Verifying Campaigns and templates of corporate user (C2) after logging with Franchisee Employee user(V2)");
			fc.utobj().printTestStep("Verify Public to all users Campaign created by Corporate user(C2) who does not have privilege to see private campaign by Franchisee Employee user(V2)");
	
			
			campaignCenterPage.searchCampaignByFilter(driver, firstcampaignNamecorp);

			boolean isCampaignNamePresentCorpaccessibilitypublictoalluserscorpV2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firstcampaignNamecorp + "')]");
			if (isCampaignNamePresentCorpaccessibilitypublictoalluserscorpV2 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users Campaign created by Corporate user who does not have privilege to see private campaign by Franchisee Employee user(V2)");
			}
			//Verify Public to all corporate users Campaign created by Corporate user who has privilege to see private campaign by Franchisee mu user(V2)
			fc.utobj().printTestStep("Verify Public to all corporate users Campaign created by Corporate user who does not have privilege to see private campaign by Franchisee Employee user(V2)");
			campaignCenterPage.searchCampaignByFilter(driver, secondcampaignNamecorp);
			
			boolean isCampaignNamePresentCorpaccessibilitypublictoallcorporateuserscorpV2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondcampaignNamecorp + "')]");
			if (!isCampaignNamePresentCorpaccessibilitypublictoallcorporateuserscorpV2 == false) {
				fc.utobj().throwsException("was not able to verify Public to all corporate users Campaign created by Corporate user who does not privilege to see private campaign by Franchisee Employee user(V2)");
			}
			
			//Verify Private Campaign created by Corporate user who has privilege to see private campaign by Franchisee mu user(V2)
			fc.utobj().printTestStep("Verify Private Campaign created by Corporate user who does not have privilege to see private campaign by Franchisee Employee user(V2)");
			campaignCenterPage.searchCampaignByFilter(driver, thirdcampaignNamecorp);
			
			boolean isCampaignNamePresentCorpaccessibilityprivatecorpV2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + thirdcampaignNamecorp + "')]");
			if (!isCampaignNamePresentCorpaccessibilityprivatecorpV2 == false) {
				fc.utobj().throwsException("was not able to Private Campaign created by Corporate user who does not have privilege to see private campaign by Franchisee Employee user(V2)");
			}
		
			//Verify Public to all users (Status Driven)Campaign created by Corporate user who does not have privilege to see private campaign by Franchisee mu user(V2)
			fc.utobj().printTestStep("Verify Public to all users(Status Driven) Campaign created by Corporate user who does not have privilege to see private campaign by Franchisee Employee user(V2)");
			campaignCenterPage.searchCampaignByFilter(driver, firstcampaignNamecorpstatus);

			boolean isCampaignNamePresentCorpaccessibilitypublictoalluserscorpstatusV2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firstcampaignNamecorpstatus + "')]");
			if (isCampaignNamePresentCorpaccessibilitypublictoalluserscorpstatusV2 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users(Status Driven) Campaign created by Corporate user who does not have privilege to see private campaign by Franchisee Employee user(V2)");
			}
			//Verify Public to all corporate users Campaign created by Corporate user who has privilege to see private campaign by Franchisee mu user(V2)
			fc.utobj().printTestStep("Verify Public to all corporate users(Status Driven) Campaign created by Corporate user who does not have privilege to see private campaign by Franchisee Employee user(V2)");
			campaignCenterPage.searchCampaignByFilter(driver, secondcampaignNamecorpstatus);
			
			boolean isCampaignNamePresentCorpaccessibilitypublictoallcorporateuserscorpstatusV2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondcampaignNamecorpstatus + "')]");
			if (!isCampaignNamePresentCorpaccessibilitypublictoallcorporateuserscorpstatusV2 == false) {
				fc.utobj().throwsException("was not able to verify Public to all corporate users(Status Driven) Campaign created by Corporate user who does not privilege to see private campaign by Franchisee Employee user(V2)");
			}
			
			//Verify Private Campaign created by Corporate user who has privilege to see private campaign by Franchisee mu user(V2)
			fc.utobj().printTestStep("Verify Private Campaign created by Corporate user(Status Driven) who does not have privilege to see private campaign by Franchisee Employee user(V2)");
			campaignCenterPage.searchCampaignByFilter(driver, thirdcampaignNamecorpstatus);
			
			boolean isCampaignNamePresentCorpaccessibilityprivatecorpstatusV2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + thirdcampaignNamecorpstatus + "')]");
			if (!isCampaignNamePresentCorpaccessibilityprivatecorpstatusV2 == false) {
				fc.utobj().throwsException("was not able to Private Campaign created by Corporate user(Status Driven) who does not have privilege to see private campaign by Franchisee Employee user(V2)");
			}
				
			fc.utobj().printTestStep("Navigate to CRM>Campaign Center>email template");
			emailtemplate.clickonemailtamplate(driver);
			
			//Verify Public to all users template created by Corporate user who does not have privilege to see private template by Franchisee mu user(V2)
			fc.utobj().printTestStep("Verify Public to all users template created by Corporate user who does not have privilege to see private template by Franchisee Employee user(V2)");
			campaignCenterPage.searchTemplateByFilter(driver, firsttemplateNamecorp);

			boolean isTemplateNamePresentCorpaccessibilitypublictoalluserscorpV2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firsttemplateNamecorp + "')]");
			if (isTemplateNamePresentCorpaccessibilitypublictoalluserscorpV2 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users template created by Corporate user who has privilege to see private template by Franchisee Employee user(V2)");
			}
			//Verify Public to all corporate users template created by Corporate user who has privilege to see private template by Franchisee mu user(V2)
			fc.utobj().printTestStep("Verify Public to all corporate users template created by Corporate user who does not have privilege to see private template by Franchisee Employee user(V2)");
			campaignCenterPage.searchTemplateByFilter(driver, secondtemplateNamecorp);
			
			boolean isTemplateNamePresentCorpaccessibilitypublictoallcorporateuserscorpV2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondtemplateNamecorp + "')]");
			if (!isTemplateNamePresentCorpaccessibilitypublictoallcorporateuserscorpV2 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users template created by Corporate user who has privilege to see private template by Franchisee Employee user(V2)");
			}
			
			//Verify Private template created by Corporate user who has privilege to see private template by Franchisee mu user(V2)
			fc.utobj().printTestStep("Verify Privare template created by Corporate user who does not have privilege to see private template by Franchisee Employee user(V2)");
			campaignCenterPage.searchTemplateByFilter(driver, thirdtemplateNamecorp);
			
			boolean isTemplateNamePresentCorpaccessibilityprivatecorpV2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + thirdtemplateNamecorp + "')]");
			if (!isTemplateNamePresentCorpaccessibilityprivatecorpV2 == false) {
				fc.utobj().throwsException("was not able to verify private template created by Corporate user who has privilege to see private template by Franchisee Employee user(V2)");
			}
			
			//Verify Public to all users (Status driven)template created by Corporate user who does not have privilege to see private template by Franchisee mu user(V2)
			fc.utobj().printTestStep("Verify Public to all users (Status driven)template created by Corporate user who does not have privilege to see private template by Franchisee Employee user(V2)");
			campaignCenterPage.searchTemplateByFilter(driver, firsttemplateNamecorpstatus);

			boolean isTemplateNamePresentCorpaccessibilitypublictoalluserscorpstatusV2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firsttemplateNamecorpstatus + "')]");
			if (isTemplateNamePresentCorpaccessibilitypublictoalluserscorpstatusV2 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users (Status driven)template created by Corporate user who has privilege to see private template by Franchisee Employee user(V2)");
			}
			//Verify Public to all corporate users template created by Corporate user who has privilege to see private template by Franchisee mu user(V2)
			fc.utobj().printTestStep("Verify Public to all corporate users (Status driven)template created by Corporate user who does not have privilege to see private template by Franchisee Employee user(V2)");
			campaignCenterPage.searchTemplateByFilter(driver, secondtemplateNamecorpstatus);
			
			boolean isTemplateNamePresentCorpaccessibilitypublictoallcorporateuserscorpstatusV2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondtemplateNamecorpstatus + "')]");
			if (!isTemplateNamePresentCorpaccessibilitypublictoallcorporateuserscorpstatusV2 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users (Status driven)template created by Corporate user who has privilege to see private template by Franchisee Employee user(V2)");
			}
			
			//Verify Private template created by Corporate user who has privilege to see private template by Franchisee mu user(V2)
			fc.utobj().printTestStep("Verify Privare (Status driven)template created by Corporate user who does not have privilege to see private template by Franchisee Employee user(V2)");
			campaignCenterPage.searchTemplateByFilter(driver, thirdtemplateNamecorpstatus);
			
			boolean isTemplateNamePresentCorpaccessibilityprivatecorpstatusV2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + thirdtemplateNamecorpstatus + "')]");
			if (!isTemplateNamePresentCorpaccessibilityprivatecorpstatusV2 == false) {
				fc.utobj().throwsException("was not able to verify private (Status driven)template created by Corporate user who has privilege to see private template by Franchisee Employee user(V2)");
			}
		
			fc.utobj().printTestStep("Navigate to CRM>Campaign Center>campaign");
			emailtemplate.clickoncampaigntemplate(driver);
			fc.utobj().clickElement(driver, crmcenter.corporatCampaign);
			//Verify Public to all users of my region Campaign created by Regional user who has privilege to see private campaign by Franchisee mu user(V2)(R1)
			//Verifying Campaigns and templates of Regional user (R1) after logging with Franchisee Employee user(V2)
			fc.utobj().printTestStep("Verifying Campaigns and templates of Regional user (R1) after logging with Franchisee Employee user(V2)");
			fc.utobj().printTestStep("Verify Public to all users of my region Campaign created by Regional user who has privilege to see private campaign by Franchisee Employee user(V2)");
			campaignCenterPage.searchCampaignByFilter(driver, firstcampaignNamereg);

			boolean isCampaignNamePresentRegpaccessibilitypublictoallusersofmyregionV2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firstcampaignNamereg + "')]");
			if (isCampaignNamePresentRegpaccessibilitypublictoallusersofmyregionV2 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my region Campaign created by Regional user who has privilege to see private campaign by Franchisee Employee user(V2)");
			}
			
			//Verify Private Campaign created by Regional user who has privilege to see private campaign by Franchisee mu user(V2)
			fc.utobj().printTestStep("Verify Private Campaign created by Regional user who has privilege to see private campaign by Franchisee mu user(V2)");
			campaignCenterPage.searchCampaignByFilter(driver, secondcampaignNamereg);
			
			boolean isCampaignNamePresentRegpaccessibilityprivateV2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondcampaignNamereg + "')]");
			if (!isCampaignNamePresentRegpaccessibilityprivateV2 == false) {
				fc.utobj().throwsException("was not able to verify Private Campaign created by Regional user who has privilege to see private campaign by Franchisee Employee user(V2)");
			}
			
			//Verify Public to all users of my region (Status Driven)Campaign created by Regional user who has privilege to see private campaign by Franchisee mu user(V2)
			fc.utobj().printTestStep("Verify Public to all users of my region (Status Driven)Campaign created by Regional user who has privilege to see private campaign by Franchisee Employee user(V2)");
			campaignCenterPage.searchCampaignByFilter(driver, firstcampaignNameregstatus);

			boolean isCampaignNamePresentRegpaccessibilitypublictoallusersofmyregionstatusV2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firstcampaignNameregstatus + "')]");
			if (isCampaignNamePresentRegpaccessibilitypublictoallusersofmyregionstatusV2 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my region (Status Driven)Campaign created by Regional user who has privilege to see private campaign by Franchisee Employee user(V2)");
			}
			
			//Verify Private Campaign created by Regional user who has privilege to see private campaign by Franchisee mu user(V2)
			fc.utobj().printTestStep("Verify Private (Status Driven)Campaign created by Regional user who has privilege to see private campaign by Franchisee Employee user(V2)");
			campaignCenterPage.searchCampaignByFilter(driver, secondcampaignNameregstatus);
			
			boolean isCampaignNamePresentRegpaccessibilityprivatestatusV2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondcampaignNameregstatus + "')]");
			if (!isCampaignNamePresentRegpaccessibilityprivatestatusV2 == false) {
				fc.utobj().throwsException("was not able to verify Private (Status Driven)Campaign created by Regional user who has privilege to see private campaign by Franchisee Employee user(V2)");
			}
			
			fc.utobj().printTestStep("Navigate to CRM>Campaign Center>email template");
			emailtemplate.clickonemailtamplate(driver);
			
			//Verify Public to all users template created by Corporate user who has privilege to see private template by Franchisee mu user(V2)
			fc.utobj().printTestStep("Verify Public to all users of my region template created by Regional user who has privilege to see private template by Franchisee Employee user(V2)");
			campaignCenterPage.searchTemplateByFilter(driver, firsttemplateNamereg);

			boolean isTemplateNamePresentRegaccessibilitypublictoallusersofmyregionV2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firsttemplateNamereg + "')]");
			if (isTemplateNamePresentRegaccessibilitypublictoallusersofmyregionV2 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users template created by Regional user who has privilege to see private template by Franchisee Employee user(V2)");
			}
			
			//Verify Private template created by Regional user who has privilege to see private template by Franchisee mu user(V2)
			fc.utobj().printTestStep("Verify Private template created by Regional user who has privilege to see private template by Franchisee Employee user(V2)");
			campaignCenterPage.searchTemplateByFilter(driver, secondtemplateNamereg);
			
			boolean isTemplateNamePresentRegaccessibilityprivateV2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondtemplateNamereg + "')]");
			if (!isTemplateNamePresentRegaccessibilityprivateV2 == false) {
				fc.utobj().throwsException("was not able to verify private template created by Regional user who has privilege to see private template by Franchisee Employee user(V2)");
			}
			
			//Verify Public to all users(Status Driven) template created by Regional user who has privilege to see private template by Franchisee mu user(V2)
			fc.utobj().printTestStep("Verify Public to all users of my region (Status Driven)template created by Regional user who has privilege to see private template by Franchisee Employee user(V2)");
			campaignCenterPage.searchTemplateByFilter(driver, firsttemplateNameregstatus);

			boolean isTemplateNamePresentRegaccessibilitypublictoallusersofmyregionstatusV2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firsttemplateNameregstatus + "')]");
			if (isTemplateNamePresentRegaccessibilitypublictoallusersofmyregionstatusV2 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my region (Status Driven)template created by Regional user who has privilege to see private template by Franchisee Employee user(V2)");
			}
			
			//Verify Private template created by Regional user who has privilege to see private template by Franchisee mu user(V2)
			fc.utobj().printTestStep("Verify Private (Status Driven)template created by Regional user who has privilege to see private template by Franchisee Employee user(V2)");
			campaignCenterPage.searchTemplateByFilter(driver, secondtemplateNameregstatus);
			
			boolean isTemplateNamePresentRegaccessibilityprivatestatusV2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondtemplateNameregstatus + "')]");
			if (!isTemplateNamePresentRegaccessibilityprivatestatusV2 == false) {
				fc.utobj().throwsException("was not able to verify private (Status Driven)template created by Regional user who has privilege to see private template by Franchisee Employee user(V2)");
			}
			fc.utobj().printTestStep("Navigate to CRM>Campaign Center>campaign template");
			emailtemplate.clickoncampaigntemplate(driver);
			fc.utobj().clickElement(driver, crmcenter.corporatCampaign);
			
			//Verify Public to all users Campaign created by Corporate user who has privilege to see private campaign by Franchisee mu user(V2)
			fc.utobj().printTestStep("Verifying Campaigns and templates of C1 by Franchisee mu user(V2)");
			fc.utobj().printTestStep("Verify Public to all users Campaign created by Corporate user who has privilege to see private campaign by Franchisee mu user(V2)");
			campaignCenterPage.searchCampaignByFilter(driver, firstcampaignName);

			boolean isCampaignNamePresentCorpaccessibilitypublictoallusersV2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firstcampaignName + "')]");
			if (isCampaignNamePresentCorpaccessibilitypublictoallusersV2 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users Campaign created by Corporate user who has privilege to see private campaign by Franchisee mu user(V2)");
			}
			//Verify Public to all corporate users Campaign created by Corporate user who has privilege to see private campaign by Franchisee mu user(V2)
			fc.utobj().printTestStep("Verify Public to all corporate users Campaign created by Corporate user who has privilege to see private campaign");
			campaignCenterPage.searchCampaignByFilter(driver, secondcampaignName);
			
			boolean isCampaignNamePresentCorpaccessibilitypublictoallcorporateusersV2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondcampaignName + "')]");
			if (!isCampaignNamePresentCorpaccessibilitypublictoallcorporateusersV2 == false) {
				fc.utobj().throwsException("was not able to verify Public to all corporate users Campaign created by Corporate user who has privilege to see private campaign by Franchisee mu user(V2)");
			}
			
			//Verify Private Campaign created by Corporate user who has privilege to see private campaign by Franchisee mu user(V2)
			fc.utobj().printTestStep("Verify Private Campaign created by Corporate user who has privilege to see private campaign by Franchisee mu user(V2)");
			campaignCenterPage.searchCampaignByFilter(driver, thirdcampaignName);
			
			boolean isCampaignNamePresentCorpaccessibilityprivateV2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + thirdcampaignName + "')]");
			if (!isCampaignNamePresentCorpaccessibilityprivateV2 == false) {
				fc.utobj().throwsException("was not able to Private Campaign created by Corporate user who has privilege to see private campaign by Franchisee mu user(V2)");
			}
			
			//Verify Public to all users(Status Driven) Campaign created by Corporate user who has privilege to see private campaign by Franchisee mu user(V2)
			fc.utobj().printTestStep("Verify Public to all users(Status Driven) Campaign created by Corporate user who has privilege to see private campaign by Franchisee mu user(V2)");
			campaignCenterPage.searchCampaignByFilter(driver, firstcampaignNamestatus);

			boolean isCampaignNamePresentCorpaccessibilitypublictoallusersstatusV2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firstcampaignNamestatus + "')]");
			if (isCampaignNamePresentCorpaccessibilitypublictoallusersstatusV2 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users(Status Driven) Campaign created by Corporate user who has privilege to see private campaign by Franchisee mu user(V2)");
			}
			//Verify Public to all corporate users(Status Driven) Campaign created by Corporate user who has privilege to see private campaign by Franchisee mu user(V2)
			fc.utobj().printTestStep("Verify Public to all corporate users(Status Driven) Campaign created by Corporate user who has privilege to see private campaign");
			campaignCenterPage.searchCampaignByFilter(driver, secondcampaignNamestatus);
			
			boolean isCampaignNamePresentCorpaccessibilitypublictoallcorporateusersstatusV2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondcampaignNamestatus + "')]");
			if (!isCampaignNamePresentCorpaccessibilitypublictoallcorporateusersstatusV2 == false) {
				fc.utobj().throwsException("was not able to verify Public to all corporate users(Status Driven) Campaign created by Corporate user who has privilege to see private campaign by Franchisee mu user(V2)");
			}
			
			//Verify Private Campaign created by Corporate user(Status Driven) who has privilege to see private campaign by Franchisee mu user(V2)
			fc.utobj().printTestStep("Verify Private Campaign(Status Driven) created by Corporate user who has privilege to see private campaign by Franchisee mu user(V2)");
			campaignCenterPage.searchCampaignByFilter(driver, thirdcampaignNamestatus);
			
			boolean isCampaignNamePresentCorpaccessibilityprivatestatusV2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + thirdcampaignNamestatus + "')]");
			if (!isCampaignNamePresentCorpaccessibilityprivatestatusV2 == false) {
				fc.utobj().throwsException("was not able to Private Campaign(Status Driven) created by Corporate user who has privilege to see private campaign by Franchisee mu user(V2)");
			}
		
			fc.utobj().printTestStep("Navigate to CRM>Campaign Center>email template");
			emailtemplate.clickonemailtamplate(driver);
			
			//Verify Public to all users template created by Corporate user who has privilege to see private template by Franchisee mu user(V2)
			fc.utobj().printTestStep("Verify Public to all users template created by Corporate user who has privilege to see private template by Franchisee mu user(V2)");
			campaignCenterPage.searchTemplateByFilter(driver, firsttemplateName);

			boolean isTemplateNamePresentCorpaccessibilitypublictoallusersV2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firsttemplateName + "')]");
			if (isTemplateNamePresentCorpaccessibilitypublictoallusersV2 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users template created by Corporate user who has privilege to see private template by Franchisee mu user(V2)");
			}
			//Verify Public to all corporate users template created by Corporate user who has privilege to see private template by Franchisee mu user(V2)
			fc.utobj().printTestStep("Verify Public to all corporate users template created by Corporate user who has privilege to see private template by Franchisee mu user(V2)");
			campaignCenterPage.searchTemplateByFilter(driver, secondtemplateName);
			
			boolean isTemplateNamePresentCorpaccessibilitypublictoallcorporateusersV2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondtemplateName + "')]");
			if (!isTemplateNamePresentCorpaccessibilitypublictoallcorporateusersV2 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users template created by Corporate user who has privilege to see private template by Franchisee mu user(V2)");
			}
			
			//Verify Private template created by Corporate user who has privilege to see private template by Franchisee mu user(V2)
			fc.utobj().printTestStep("Verify Privare template created by Corporate user who has privilege to see private template by Franchisee mu user(V2)");
			campaignCenterPage.searchTemplateByFilter(driver, thirdtemplateName);
			
			boolean isTemplateNamePresentCorpaccessibilityprivateV2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + thirdtemplateName + "')]");
			if (!isTemplateNamePresentCorpaccessibilityprivateV2 == false) {
				fc.utobj().throwsException("was not able to verify private template created by Corporate user who has privilege to see private template by Franchisee mu user(V2)");
			}
				
			//Verify Public to all users template(Status Driven) created by Corporate user who has privilege to see private template by Franchisee mu user(V2)
			fc.utobj().printTestStep("Verify Public to all users template(Status Driven) created by Corporate user who has privilege to see private template by Franchisee mu user(V2)");
			campaignCenterPage.searchTemplateByFilter(driver, firsttemplateNamestatus);

			boolean isTemplateNamePresentCorpaccessibilitypublictoallusersstatusV2= fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firsttemplateNamestatus + "')]");
			if (isTemplateNamePresentCorpaccessibilitypublictoallusersstatusV2 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users template(Status Driven) created by Corporate user who has privilege to see private template by Franchisee mu user(V2)");
			}
			//Verify Public to all corporate users template(Status Driven) created by Corporate user who has privilege to see private template by Franchisee mu user(V2)
			fc.utobj().printTestStep("Verify Public to all corporate users template(Status Driven) created by Corporate user who has privilege to see private template by Franchisee mu user(V2)");
			campaignCenterPage.searchTemplateByFilter(driver, secondtemplateNamestatus);
			
			boolean isTemplateNamePresentCorpaccessibilitypublictoallcorporateusersstatusV2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondtemplateNamestatus + "')]");
			if (!isTemplateNamePresentCorpaccessibilitypublictoallcorporateusersstatusV2 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users template(Status Driven) created by Corporate user who has privilege to see private template by Franchisee mu user(V2)");
			}
			
			//Verify Private template(Status Driven) created by Corporate user who has privilege to see private template by Franchisee mu user(V2)
			fc.utobj().printTestStep("Verify Privare template(Status Driven) created by Corporate user who has privilege to see private template by Franchisee mu user(V2)");
			campaignCenterPage.searchTemplateByFilter(driver, thirdtemplateNamestatus);
			
			boolean isTemplateNamePresentCorpaccessibilityprivatestatusV2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + thirdtemplateNamestatus + "')]");
			if (!isTemplateNamePresentCorpaccessibilityprivatestatusV2 == false) {
				fc.utobj().throwsException("was not able to verify private template(Status Driven) created by Corporate user who has privilege to see private template by Franchisee mu user(V2)");
			}
		
			fc.utobj().printTestStep("Navigate to CRM>Campaign Center>campaign template");
			emailtemplate.clickoncampaigntemplate(driver);
			fc.utobj().clickElement(driver, crmcenter.corporatCampaign);
			//Verify Public to all users Campaign created by Corporate user who has privilege to see private campaign by Franchisee mu user(V2)
			fc.utobj().printTestStep("Verifying Campaigns and templates of Regional user (R2) by Franchisee Employee user(V2)");
			//Verify Public to all users of my region Campaign created by Regional user who does not have privilege to see private campaign by Franchisee mu user(V2)
			fc.utobj().printTestStep("Verify Public to all users of my region Campaign created by Regional user who does not have privilege to see private campaign by Franchisee Employee user(V2)");
			campaignCenterPage.searchCampaignByFilter(driver, firstcampaignNamereg1);

			boolean isCampaignNamePresentRegpaccessibilitypublictoallusersofmyregion1V2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firstcampaignNamereg1 + "')]");
			if (isCampaignNamePresentRegpaccessibilitypublictoallusersofmyregion1V2 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my region Campaign created by Regional user who does not have privilege to see private campaign by Franchisee Employee user(V2)");
			}
			
			//Verify Private Campaign created by Regional user who does not have privilege to see private campaign by Franchisee mu user(V2)
			fc.utobj().printTestStep("Verify Private Campaign created by Regional user who does not have privilege to see private campaign by Franchisee Employee user(V2)");
			campaignCenterPage.searchCampaignByFilter(driver, secondcampaignNamereg1);
			
			boolean isCampaignNamePresentRegpaccessibilityprivate1V2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondcampaignNamereg1 + "')]");
			if (!isCampaignNamePresentRegpaccessibilityprivate1V2 == false) {
				fc.utobj().throwsException("was not able to verify Private Campaign created by Regional user who does not have privilege to see private campaign by Franchisee Employee user(V2)");
			}
			
			//Verify Public to all users of my region (Status Driven)Campaign created by Regional user who does not have privilege to see private campaign by Franchisee mu user(V2)
			fc.utobj().printTestStep("Verify Public to all users of my region (Status Driven)Campaign created by Regional user who does not have privilege to see private campaign by Franchisee Employee user(V2)");
			campaignCenterPage.searchCampaignByFilter(driver, firstcampaignNamereg1status);

			boolean isCampaignNamePresentRegpaccessibilitypublictoallusersofmyregion1statusV2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firstcampaignNamereg1status + "')]");
			if (isCampaignNamePresentRegpaccessibilitypublictoallusersofmyregion1statusV2 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my region (Status Driven)Campaign created by Regional user who does not have privilege to see private campaign by Franchisee Employee user(V2)");
			}
			
			//Verify Private (Status Driven)Campaign created by Regional user who does not have privilege to see private campaign by Franchisee mu user(V2)
			fc.utobj().printTestStep("Verify Private (Status Driven)Campaign created by Regional user who does not have privilege to see private campaign by Franchisee Employee user(V2)");
			campaignCenterPage.searchCampaignByFilter(driver, secondcampaignNamereg1status);
			
			boolean isCampaignNamePresentRegpaccessibilityprivate1statusV2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondcampaignNamereg1status + "')]");
			if (!isCampaignNamePresentRegpaccessibilityprivate1statusV2 == false) {
				fc.utobj().throwsException("was not able to verify Private (Status Driven)Campaign created by Regional user who does not have privilege to see private campaign by Franchisee Employee user(V2)");
			}
		
			fc.utobj().printTestStep("Navigate to CRM>Campaign Center>email template");
			emailtemplate.clickonemailtamplate(driver);
			
			//Verify Public to all users template created by Corporate user who does not have privilege to see private template by Franchisee mu user(V2)
			fc.utobj().printTestStep("Verify Public to all users of my region template created by Regional user who does not have privilege to see private template by Franchisee Employee user(V2)");
			campaignCenterPage.searchTemplateByFilter(driver, firsttemplateNamereg1);

			boolean isTemplateNamePresentRegaccessibilitypublictoallusersofmyregion1V2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firsttemplateNamereg1 + "')]");
			if (isTemplateNamePresentRegaccessibilitypublictoallusersofmyregion1V2 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my region template created by Regional user who does not have privilege to see private template by Franchisee Employee user(V2)");
			}
			
			//Verify Private template created by Regional user who does not have privilege to see private template by Franchisee mu user(V2)
			fc.utobj().printTestStep("Verify Private template created by Regional user does not have privilege to see private template by Franchisee Employee user(V2)");
			campaignCenterPage.searchTemplateByFilter(driver, secondtemplateNamereg1);
			
			boolean isTemplateNamePresentRegaccessibilityprivate1V2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondtemplateNamereg1 + "')]");
			if (!isTemplateNamePresentRegaccessibilityprivate1V2 == false) {
				fc.utobj().throwsException("was not able to verify Private template created by Regional user does not have privilege to see private template by Franchisee Employee user(V2)");
			}
			
			//Verify Public to all users (Status Driven)template created by Regional user who does not have privilege to see private template by Franchisee mu user(V2)
			fc.utobj().printTestStep("Verify Public to all users of my region (Status Driven)template created by Regional user who does not have privilege to see private template by Franchisee Employee user(V2)");
			campaignCenterPage.searchTemplateByFilter(driver, firsttemplateNamereg1status);

			boolean isTemplateNamePresentRegaccessibilitypublictoallusersofmyregion1statusV2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firsttemplateNamereg1status + "')]");
			if (isTemplateNamePresentRegaccessibilitypublictoallusersofmyregion1statusV2 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my region (Status Driven)template created by Regional user who does not have privilege to see private template by Franchisee Employee user(V2)");
			}
			
			//Verify Private template created by Regional user who does not have privilege to see private template by Franchisee mu user(V2)
			fc.utobj().printTestStep("Verify Private (Status Driven)template created by Regional user does not have privilege to see private template by Franchisee Employee user(V2)");
			campaignCenterPage.searchTemplateByFilter(driver, secondtemplateNamereg1status);
			
			boolean isTemplateNamePresentRegaccessibilityprivate1statusV2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondtemplateNamereg1status + "')]");
			if (!isTemplateNamePresentRegaccessibilityprivate1statusV2 == false) {
				fc.utobj().throwsException("was not able to verify Private (Status Driven)template created by Regional user does not have privilege to see private template by Franchisee Employee user(V2)");
			}
			
			fc.utobj().printTestStep("Navigate to CRM>Campaign Center>campaign template");
			emailtemplate.clickoncampaigntemplate(driver);
			
			fc.utobj().printTestStep("Verifying Campaigns and templates of Franchise owner user (F0New) by Franchisee Employee user(V2)(V2)");
			fc.utobj().printTestStep("Verify Public to all users of my Franchise Campaign created by Franchise user who has privilege to see private campaign by Franchisee Employee user(V2)");
			campaignCenterPage.searchCampaignByFilter(driver, firstcampaignNamefran);

			boolean isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchiseV2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firstcampaignNamefran + "')]");
			if (isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchiseV2 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my Franchise Campaign created by Franchise user who has privilege to see private campaign by Franchisee Employee user(V2)");
			}
			
			//Verify Private Campaign created by Franchise user who has privilege to see private campaign by Franchisee mu user(V2)
			fc.utobj().printTestStep("Verify Private Campaign created by Franchise user who has privilege to see private campaign by Franchisee Employee user(V2)");
			campaignCenterPage.searchCampaignByFilter(driver, secondcampaignNamefran);
			
			boolean isCampaignNamePresentFranpaccessibilityprivateV2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondcampaignNamefran + "')]");
			if (isCampaignNamePresentFranpaccessibilityprivateV2 == false) {
				fc.utobj().throwsException("was not able to verify Private Campaign created by Franchise user who has privilege to see private campaign by Franchisee Employee user(V2)");
			}
			
			
			//Verify Public to all users of my Franchise (Status Driven)Campaign created by Franchise user who has privilege to see private campaign by Franchisee mu user(V2)
			fc.utobj().printTestStep("Verify Public to all users of my Franchise (Status Driven)Campaign created by Franchise user who has privilege to see private campaign by Franchisee Employee user(V2)");
			campaignCenterPage.searchCampaignByFilter(driver, firstcampaignNamefranstatus);

			boolean isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchisestatusV2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firstcampaignNamefranstatus + "')]");
			if (isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchisestatusV2 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my Franchise (Status Driven)Campaign created by Franchise user who has privilege to see private campaign by Franchisee Employee user(V2)");
			}
			
			//Verify Private Campaign created by Franchise user who has privilege to see private campaign by Franchisee mu user(V2)
			fc.utobj().printTestStep("Verify Private (Status Driven)Campaign created by Franchise user who has privilege to see private campaign by Franchisee Employee user(V2)");
			campaignCenterPage.searchCampaignByFilter(driver, secondcampaignNamefranstatus);
			
			boolean isCampaignNamePresentFranpaccessibilityprivatestatusV2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondcampaignNamefranstatus + "')]");
			if (isCampaignNamePresentFranpaccessibilityprivatestatusV2 == false) {
				fc.utobj().throwsException("was not able to verify Private (Status Driven)Campaign created by Franchise user who has privilege to see private campaign by Franchisee Employee user(V2)");
			}
			
			fc.utobj().printTestStep("Navigate to CRM>Campaign Center>email template");
			emailtemplate.clickonemailtamplate(driver);
			fc.utobj().clickElement(driver, crmcenter.locationtemplate);
			//Verify Public to all users template created by Corporate user who has privilege to see private template by Franchisee mu user(V2)
			fc.utobj().printTestStep("Verify Public to all users of my franchise template created by franchise user who has privilege to see private template by Franchisee Employee user(V2)");
			campaignCenterPage.searchTemplateByFilter(driver, firsttemplateNamefran);

			boolean isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchiseV2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firsttemplateNamefran + "')]");
			if (isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchiseV2 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my franchise template created by franchise user who has privilege to see private template by Franchisee Employee user(V2)");
			}
			
			//Verify Private template created by franchise user who has privilege to see private template by Franchisee mu user(V2)
			fc.utobj().printTestStep("Verify Private template created by franchise user who has privilege to see private template by Franchisee Employee user(V2)");
			campaignCenterPage.searchTemplateByFilter(driver, secondtemplateNamefran);
			
			boolean isTemplateNamePresentfranaccessibilityprivateV2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondtemplateNamefran + "')]");
			if (isTemplateNamePresentfranaccessibilityprivateV2 == false) {
				fc.utobj().throwsException("was not able to verify private template created by franchise user who has privilege to see private template by Franchisee Employee user(V2)");
			}
			
			//Verify Public to all users (Status Driven)template created by Corporate user who has privilege to see private template by Franchisee mu user(V2)
			fc.utobj().printTestStep("Verify Public to all users of my franchise (Status Driven)template created by franchise user who has privilege to see private template by Franchisee Employee user(V2)");
			campaignCenterPage.searchTemplateByFilter(driver, firsttemplateNamefranstatus);

			boolean isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchisestatusV2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firsttemplateNamefranstatus + "')]");
			if (isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchisestatusV2 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my franchise (Status Driven)template created by franchise user who has privilege to see private template by Franchisee Employee user(V2)");
			}
			
			//Verify Private template created by franchise user who has privilege to see private template by Franchisee mu user(V2)
			fc.utobj().printTestStep("Verify Private (Status Driven)template created by franchise user who has privilege to see private template by Franchisee Employee user(V2)");
			campaignCenterPage.searchTemplateByFilter(driver, secondtemplateNamefranstatus);
			
			boolean isTemplateNamePresentfranaccessibilityprivatestatusV2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondtemplateNamefranstatus + "')]");
			if (isTemplateNamePresentfranaccessibilityprivatestatusV2 == false) {
				fc.utobj().throwsException("was not able to verify private (Status Driven)template created by franchise user who has privilege to see private template by Franchisee Employee user(V2)");
			}
			
			fc.utobj().printTestStep("Navigate to CRM>Campaign Center>campaign template");
			emailtemplate.clickoncampaigntemplate(driver);
			
			fc.utobj().printTestStep("Verifying Campaigns and templates of Franchise mu owner user (V1) by Franchisee Employee user(V2)");
			
			fc.utobj().printTestStep("Verify Public to all users of my Franchise Campaign created by Franchise MU user(V1) who has privilege to see private campaign by Franchisee Employee user(V2)");
			campaignCenterPage.searchCampaignByFilter(driver, firstcampaignNamefranmu);

			boolean isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchisemuV2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firstcampaignNamefranmu + "')]");
			if (isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchisemuV2 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my Franchise Campaign created by Franchise MU user(V1) who has privilege to see private campaign by Franchisee Employee user(V2)");
			}
			
			//Verify Private Campaign created by Franchise user who has privilege to see private campaign by Franchisee mu user(V2)
			fc.utobj().printTestStep("Verify Private Campaign created by Franchise MU user(V1) who has privilege to see private campaign by Franchisee Employee user(V2)");
			campaignCenterPage.searchCampaignByFilter(driver, secondcampaignNamefranmu);
			
			boolean isCampaignNamePresentFranpaccessibilityprivatemuV2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondcampaignNamefranmu + "')]");
			if (isCampaignNamePresentFranpaccessibilityprivatemuV2 == false) {
				fc.utobj().throwsException("was not able to verify Private Campaign created by Franchise MU user(V1) who has privilege to see private campaign by Franchisee Employee user(V2)");
			}
			
			
			//Verify Public to all users of my Franchise (Status Driven)Campaign created by Franchise user who has privilege to see private campaign by Franchisee mu user(V2)
			fc.utobj().printTestStep("Verify Public to all users of my Franchise (Status Driven)Campaign created by Franchise MU user(V1) who has privilege to see private campaign by Franchisee Employee user(V2)");
			campaignCenterPage.searchCampaignByFilter(driver, firstcampaignNamefranmustatus);

			boolean isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchisemustatusV2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firstcampaignNamefranmustatus + "')]");
			if (isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchisemustatusV2 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my Franchise (Status Driven)Campaign created by Franchise MU user(V1) who has privilege to see private campaign by Franchisee Employee user(V2)");
			}
			
			//Verify Private Campaign created by Franchise user who has privilege to see private campaign by Franchisee mu user(V2)
			fc.utobj().printTestStep("Verify Private (Status Driven)Campaign created by Franchise MU user(V1) who has privilege to see private campaign by Franchisee Employee user(V2)");
			campaignCenterPage.searchCampaignByFilter(driver, secondcampaignNamefranmustatus);
			
			boolean isCampaignNamePresentFranpaccessibilityprivatemustatusV2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondcampaignNamefranmustatus + "')]");
			if (isCampaignNamePresentFranpaccessibilityprivatemustatusV2 == false) {
				fc.utobj().throwsException("was not able to verify Private (Status Driven)Campaign created by Franchise MU user(V1) who has privilege to see private campaign by Franchisee Employee user(V2)");
			}
			
			fc.utobj().printTestStep("Navigate to CRM>Campaign Center>email template");
			emailtemplate.clickonemailtamplate(driver);
			fc.utobj().clickElement(driver, crmcenter.locationtemplate);
			//Verify Public to all users template created by Corporate user who has privilege to see private template by Franchisee mu user(V2)
			fc.utobj().printTestStep("Verify Public to all users of my franchise template created by franchise MU user(V1) who has privilege to see private template by Franchisee Employee user(V2)");
			campaignCenterPage.searchTemplateByFilter(driver, firsttemplateNamefranmu);

			boolean isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchisemuV2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firsttemplateNamefranmu + "')]");
			if (isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchisemuV2 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my franchise template created by franchise MU user(V1) who has privilege to see private template by Franchisee Employee user(V2)");
			}
			
			//Verify Private template created by franchise user who has privilege to see private template by Franchisee mu user(V2)
			fc.utobj().printTestStep("Verify Private template created by franchise MU user(V1) who has privilege to see private template by Franchisee Employee user(V2)");
			campaignCenterPage.searchTemplateByFilter(driver, secondtemplateNamefranmu);
			
			boolean isTemplateNamePresentfranaccessibilityprivatemuV2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondtemplateNamefranmu + "')]");
			if (isTemplateNamePresentfranaccessibilityprivatemuV2 == false) {
				fc.utobj().throwsException("was not able to verify private template created by franchise MU user(V1) who has privilege to see private template by Franchisee Employee user(V2)");
			}
			
			//Verify Public to all users template created by Corporate user who has privilege to see private template by Franchisee mu user(V2)
			fc.utobj().printTestStep("Verify Public to all users of my franchise (Status Driven)template created by franchise MU user(V1) who has privilege to see private template by Franchisee Employee user(V2)");
			campaignCenterPage.searchTemplateByFilter(driver, firsttemplateNamefranmustatus);

			boolean isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchisemustatusV2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firsttemplateNamefranmustatus + "')]");
			if (isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchisemustatusV2 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my franchise (Status Driven)template created by franchise MU user(V1) who has privilege to see private template by Franchisee Employee user(V2)");
			}
			
			//Verify Private template created by franchise user who has privilege to see private template by Franchisee mu user(V2)
			fc.utobj().printTestStep("Verify Private (Status Driven)template created by franchise MU user(V1) who has privilege to see private template by Franchisee Employee user(V2)");
			campaignCenterPage.searchTemplateByFilter(driver, secondtemplateNamefranmustatus);
			
			boolean isTemplateNamePresentfranaccessibilityprivatemustatusV2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondtemplateNamefranmustatus + "')]");
			if (isTemplateNamePresentfranaccessibilityprivatemustatusV2 == false) {
				fc.utobj().throwsException("was not able to verify private (Status Driven)template created by franchise MU user(V1) who has privilege to see private template by Franchisee Employee user(V2)");
			}
			
			
			
			
			fc.utobj().printTestStep("Navigate to CRM>Campaign Center>campaign template");
			emailtemplate.clickoncampaigntemplate(driver);
			fc.utobj().printTestStep("Verifying Campaigns and templates of Franchise Employee user (V2) by itself");
			fc.utobj().printTestStep("Verify Public to all users of my Franchise Campaign created by Franchise Employee user(V2) who does not have privilege to see private campaign by itself");
			campaignCenterPage.searchCampaignByFilter(driver, firstcampaignNamefranmue2);

			boolean isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchisemue2V2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firstcampaignNamefranmue2 + "')]");
			if (isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchisemue2V2 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my Franchise Campaign created by Franchise Employee user(V2) who does not have privilege to see private campaign by itself");
			}
			
			//Verify Private Campaign created by Franchise user who has privilege to see private campaign by Franchisee mu user(V2)
			fc.utobj().printTestStep("Verify Private Campaign created by Franchise Employee user(V2) who does not have privilege to see private campaign by itself");
			campaignCenterPage.searchCampaignByFilter(driver, secondcampaignNamefranmue2);
			
			boolean isCampaignNamePresentFranpaccessibilityprivatemue2V2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondcampaignNamefranmue2 + "')]");
			if (isCampaignNamePresentFranpaccessibilityprivatemue2V2 == false) {
				fc.utobj().throwsException("was not able to verify Private Campaign created by Franchise Employee user(V2) who does not have privilege to see private campaign by itself");
			}
			
			
			//Verify Public to all users of my Franchise Campaign created by Franchise Employee user(V2) who has privilege to see private campaign by Franchisee mu user(V2)
			fc.utobj().printTestStep("Verify Public to all users of my Franchise (Status Driven)Campaign created by Franchise Employee user(V2) who does not have privilege to see private campaign by itself");
			campaignCenterPage.searchCampaignByFilter(driver, firstcampaignNamefranmue2status);

			boolean isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchisemue2statusV2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firstcampaignNamefranmue2status + "')]");
			if (isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchisemue2statusV2 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my Franchise (Status Driven)Campaign created by Franchise Employee user(V2) who does not have privilege to see private campaign by itself");
			}
			
			//Verify Private Campaign created by Franchise user who has privilege to see private campaign by Franchisee mu user(V2)
			fc.utobj().printTestStep("Verify Private (Status Driven)Campaign created by Franchise Employee user(V2) who does not have privilege to see private campaign by itself");
			campaignCenterPage.searchCampaignByFilter(driver, secondcampaignNamefranmue2status);
			
			boolean isCampaignNamePresentFranpaccessibilityprivatemue2statusV2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondcampaignNamefranmue2status + "')]");
			if (isCampaignNamePresentFranpaccessibilityprivatemue2statusV2 == false) {
				fc.utobj().throwsException("was not able to verify Private (Status Driven)Campaign created by Franchise Employee user(V2) who does not have privilege to see private campaign by itself");
			}
			
			fc.utobj().printTestStep("Navigate to CRM>Campaign Center>email template");
			emailtemplate.clickonemailtamplate(driver);
			fc.utobj().clickElement(driver, crmcenter.locationtemplate);
			//Verify Public to all users template created by Franchise user who has privilege to see private template by Franchisee mu user(V2)
			fc.utobj().printTestStep("Verify Public to all users of my franchise template created by franchise Employee user(V2) who does not have privilege to see private template by itself");
			campaignCenterPage.searchTemplateByFilter(driver, firsttemplateNamefranmue2);

			boolean isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchisemue2V2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firsttemplateNamefranmue2 + "')]");
			if (isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchisemue2V2 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my franchise template created by franchise Employee user(V2) who has privilege to see private template by itself");
			}
			
			//Verify Private template created by franchise user who has privilege to see private template by Franchisee mu user(V2)
			fc.utobj().printTestStep("Verify Private template created by franchise Employee user(V2) who has privilege to see private template by itself");
			campaignCenterPage.searchTemplateByFilter(driver, secondtemplateNamefranmue2);
			
			boolean isTemplateNamePresentfranaccessibilityprivatemue2V2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondtemplateNamefranmue2 + "')]");
			if (isTemplateNamePresentfranaccessibilityprivatemue2V2 == false) {
				fc.utobj().throwsException("was not able to verify private template created by franchise Employee user(V2) who has privilege to see private template by itself");
			}
			
			//Verify Public to all users template created by Franchise user who has privilege to see private template by Franchisee mu user(V2)
			fc.utobj().printTestStep("Verify Public to all users of my franchise (Status Driven)template created by franchise Employee user(V2) who does not have privilege to see private template by itself");
			campaignCenterPage.searchTemplateByFilter(driver, firsttemplateNamefranmue2status);

			boolean isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchisemue2statusV2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firsttemplateNamefranmue2status + "')]");
			if (isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchisemue2statusV2 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my franchise (Status Driven)template created by franchise Employee user(V2) who has privilege to see private template by itself");
			}
			
			//Verify Private template created by franchise user who has privilege to see private template by Franchisee mu user(V2)
			fc.utobj().printTestStep("Verify Private (Status Driven)template created by franchise Employee user(V2) who has privilege to see private template by itself");
			campaignCenterPage.searchTemplateByFilter(driver, secondtemplateNamefranmue2status);
			
			boolean isTemplateNamePresentfranaccessibilityprivatemue2statusV2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondtemplateNamefranmue2status + "')]");
			if (isTemplateNamePresentfranaccessibilityprivatemue2statusV2 == false) {
				fc.utobj().throwsException("was not able to verify private (Status Driven)template created by franchise Employee user(V2) who has privilege to see private template by itself");
			}
			
			if(isCampaignNamePresentCorpaccessibilitypublictoalluserscorpV2 && !isCampaignNamePresentCorpaccessibilitypublictoallcorporateuserscorpV2 && !isCampaignNamePresentCorpaccessibilityprivatecorpV2 
					&& isCampaignNamePresentCorpaccessibilitypublictoalluserscorpstatusV2 && !isCampaignNamePresentCorpaccessibilitypublictoallcorporateuserscorpstatusV2 && !isCampaignNamePresentCorpaccessibilityprivatecorpstatusV2 
					&& isTemplateNamePresentCorpaccessibilitypublictoalluserscorpV2 && !isTemplateNamePresentCorpaccessibilitypublictoallcorporateuserscorpV2 && !isTemplateNamePresentCorpaccessibilityprivatecorpV2 
					&& isTemplateNamePresentCorpaccessibilitypublictoalluserscorpstatusV2 && !isTemplateNamePresentCorpaccessibilitypublictoallcorporateuserscorpstatusV2 && !isTemplateNamePresentCorpaccessibilityprivatecorpstatusV2 
					&& isCampaignNamePresentRegpaccessibilitypublictoallusersofmyregionV2 && !isCampaignNamePresentRegpaccessibilityprivateV2 && isCampaignNamePresentRegpaccessibilitypublictoallusersofmyregionstatusV2 
					&& !isCampaignNamePresentRegpaccessibilityprivatestatusV2 && isTemplateNamePresentRegaccessibilitypublictoallusersofmyregionV2 && !isTemplateNamePresentRegaccessibilityprivateV2 
					&& isTemplateNamePresentRegaccessibilitypublictoallusersofmyregionstatusV2 && !isTemplateNamePresentRegaccessibilityprivatestatusV2 && isCampaignNamePresentCorpaccessibilitypublictoallusersV2 
					&& !isCampaignNamePresentCorpaccessibilitypublictoallcorporateusersV2 && !isCampaignNamePresentCorpaccessibilityprivateV2 && isCampaignNamePresentCorpaccessibilitypublictoallusersstatusV2 
					&& !isCampaignNamePresentCorpaccessibilitypublictoallcorporateusersstatusV2 && !isCampaignNamePresentCorpaccessibilityprivatestatusV2 && isTemplateNamePresentCorpaccessibilitypublictoallusersV2 
					&& !isTemplateNamePresentCorpaccessibilitypublictoallcorporateusersV2 && !isTemplateNamePresentCorpaccessibilityprivateV2 && isTemplateNamePresentCorpaccessibilitypublictoallusersstatusV2 
					&& !isTemplateNamePresentCorpaccessibilitypublictoallcorporateusersstatusV2 && !isTemplateNamePresentCorpaccessibilityprivatestatusV2 && isCampaignNamePresentRegpaccessibilitypublictoallusersofmyregion1V2
					&& !isCampaignNamePresentRegpaccessibilityprivate1V2 && isCampaignNamePresentRegpaccessibilitypublictoallusersofmyregion1statusV2 && !isCampaignNamePresentRegpaccessibilityprivate1statusV2
					&& isTemplateNamePresentRegaccessibilitypublictoallusersofmyregion1V2 && !isTemplateNamePresentRegaccessibilityprivate1V2 && isTemplateNamePresentRegaccessibilitypublictoallusersofmyregion1statusV2 
					&& !isTemplateNamePresentRegaccessibilityprivate1statusV2 && isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchiseV2 && isCampaignNamePresentFranpaccessibilityprivateV2 
					&& isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchisestatusV2 && isCampaignNamePresentFranpaccessibilityprivatestatusV2 && isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchiseV2 
					&& isTemplateNamePresentfranaccessibilityprivateV2 && isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchisestatusV2 && isTemplateNamePresentfranaccessibilityprivatestatusV2  
					&& isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchisemuV2 && isCampaignNamePresentFranpaccessibilityprivatemuV2 && isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchisemustatusV2 
					&& isCampaignNamePresentFranpaccessibilityprivatemustatusV2 && isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchisemuV2 && isTemplateNamePresentfranaccessibilityprivatemuV2 
					&& isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchisemustatusV2 && isTemplateNamePresentfranaccessibilityprivatemustatusV2 && isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchisemue2V2 
					&& isCampaignNamePresentFranpaccessibilityprivatemue2V2 && isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchisemue2statusV2 && isCampaignNamePresentFranpaccessibilityprivatemue2statusV2 
					&& isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchisemue2V2 && isTemplateNamePresentfranaccessibilityprivatemue2V2 && isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchisemue2statusV2
					&& isTemplateNamePresentfranaccessibilityprivatemue2statusV2) {
				fc.utobj().printTestStep("able to verify all types of campaigns and templates created by corporate/rgional/franchise users when loging with Franchisee Employee user(V2) who has priviledge to see private campaign");
			}
			else {
				fc.utobj().throwsException("was not able to verify all types of campaigns and templates created by corporate/rgional/franchise users when loging with Franchisee Employee user(V2) who has priviledge to see private campaign");
			}
			
			
			
			
			
			
			//Login with Franchisee owner user(FONew) who has role to see private campaign
			fc.utobj().printTestStep("Login with Franchisee owner user(FONew) who has role to see private campaign");
			fc.loginpage().loginWithParameter(driver, userNameFONew, passwordFONew);
			fc.crm().crm_common().openCRMCampaignCenterPage(fcHomePageTest, driver);
			fc.utobj().printTestStep("Click on the menu option and select campaign");
			fc.utobj().clickElement(driver, pobj1.menuOptions);
			fc.utobj().clickElement(driver, pobj1.campaignLinks);
			fc.utobj().clickElement(driver, crmcenter.corporatCampaign);
			//Verifying Campaigns and templates of corporate user (C2) after logging with Franchisee owner user(FONew))
			fc.utobj().printTestStep("Verifying Campaigns and templates of corporate user (C2) after logging with Franchisee owner user(FONew))");
			fc.utobj().printTestStep("Verify Public to all users Campaign created by Corporate user(C2) who does not have privilege to see private campaign by Franchisee owner user(FONew))");
	
			
			campaignCenterPage.searchCampaignByFilter(driver, firstcampaignNamecorp);

			boolean isCampaignNamePresentCorpaccessibilitypublictoalluserscorpFONew = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firstcampaignNamecorp + "')]");
			if (isCampaignNamePresentCorpaccessibilitypublictoalluserscorpFONew == false) {
				fc.utobj().throwsException("was not able to verify Public to all users Campaign created by Corporate user who does not have privilege to see private campaign by Franchisee owner user(FONew))");
			}
			//Verify Public to all corporate users Campaign created by Corporate user who has privilege to see private campaign by Franchisee owner user(FONew)
			fc.utobj().printTestStep("Verify Public to all corporate users Campaign created by Corporate user who does not have privilege to see private campaign by Franchisee owner user(FONew))");
			campaignCenterPage.searchCampaignByFilter(driver, secondcampaignNamecorp);
			
			boolean isCampaignNamePresentCorpaccessibilitypublictoallcorporateuserscorpFONew = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondcampaignNamecorp + "')]");
			if (!isCampaignNamePresentCorpaccessibilitypublictoallcorporateuserscorpFONew == false) {
				fc.utobj().throwsException("was not able to verify Public to all corporate users Campaign created by Corporate user who does not privilege to see private campaign by Franchisee owner user(FONew))");
			}
			
			//Verify Private Campaign created by Corporate user who has privilege to see private campaign by Franchisee owner user(FONew)
			fc.utobj().printTestStep("Verify Private Campaign created by Corporate user who does not have privilege to see private campaign by Franchisee owner user(FONew))");
			campaignCenterPage.searchCampaignByFilter(driver, thirdcampaignNamecorp);
			
			boolean isCampaignNamePresentCorpaccessibilityprivatecorpFONew = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + thirdcampaignNamecorp + "')]");
			if (!isCampaignNamePresentCorpaccessibilityprivatecorpFONew == false) {
				fc.utobj().throwsException("was not able to Private Campaign created by Corporate user who does not have privilege to see private campaign by Franchisee owner user(FONew))");
			}
		
			//Verify Public to all users (Status Driven)Campaign created by Corporate user who does not have privilege to see private campaign by Franchisee owner user(FONew)
			fc.utobj().printTestStep("Verify Public to all users(Status Driven) Campaign created by Corporate user who does not have privilege to see private campaign by Franchisee owner user(FONew))");
			campaignCenterPage.searchCampaignByFilter(driver, firstcampaignNamecorpstatus);

			boolean isCampaignNamePresentCorpaccessibilitypublictoalluserscorpstatusFONew = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firstcampaignNamecorpstatus + "')]");
			if (isCampaignNamePresentCorpaccessibilitypublictoalluserscorpstatusFONew == false) {
				fc.utobj().throwsException("was not able to verify Public to all users(Status Driven) Campaign created by Corporate user who does not have privilege to see private campaign by Franchisee owner user(FONew))");
			}
			//Verify Public to all corporate users Campaign created by Corporate user who has privilege to see private campaign by Franchisee owner user(FONew)
			fc.utobj().printTestStep("Verify Public to all corporate users(Status Driven) Campaign created by Corporate user who does not have privilege to see private campaign by Franchisee owner user(FONew))");
			campaignCenterPage.searchCampaignByFilter(driver, secondcampaignNamecorpstatus);
			
			boolean isCampaignNamePresentCorpaccessibilitypublictoallcorporateuserscorpstatusFONew = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondcampaignNamecorpstatus + "')]");
			if (!isCampaignNamePresentCorpaccessibilitypublictoallcorporateuserscorpstatusFONew == false) {
				fc.utobj().throwsException("was not able to verify Public to all corporate users(Status Driven) Campaign created by Corporate user who does not privilege to see private campaign by Franchisee owner user(FONew))");
			}
			
			//Verify Private Campaign created by Corporate user who has privilege to see private campaign by Franchisee owner user(FONew)
			fc.utobj().printTestStep("Verify Private Campaign created by Corporate user(Status Driven) who does not have privilege to see private campaign by Franchisee owner user(FONew))");
			campaignCenterPage.searchCampaignByFilter(driver, thirdcampaignNamecorpstatus);
			
			boolean isCampaignNamePresentCorpaccessibilityprivatecorpstatusFONew = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + thirdcampaignNamecorpstatus + "')]");
			if (!isCampaignNamePresentCorpaccessibilityprivatecorpstatusFONew == false) {
				fc.utobj().throwsException("was not able to Private Campaign created by Corporate user(Status Driven) who does not have privilege to see private campaign by Franchisee owner user(FONew))");
			}
				
			fc.utobj().printTestStep("Navigate to CRM>Campaign Center>email template");
			emailtemplate.clickonemailtamplate(driver);
			
			//Verify Public to all users template created by Corporate user who does not have privilege to see private template by Franchisee owner user(FONew)
			fc.utobj().printTestStep("Verify Public to all users template created by Corporate user who does not have privilege to see private template by Franchisee owner user(FONew))");
			campaignCenterPage.searchTemplateByFilter(driver, firsttemplateNamecorp);

			boolean isTemplateNamePresentCorpaccessibilitypublictoalluserscorpFONew = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firsttemplateNamecorp + "')]");
			if (isTemplateNamePresentCorpaccessibilitypublictoalluserscorpFONew == false) {
				fc.utobj().throwsException("was not able to verify Public to all users template created by Corporate user who has privilege to see private template by Franchisee owner user(FONew))");
			}
			//Verify Public to all corporate users template created by Corporate user who has privilege to see private template by Franchisee owner user(FONew)
			fc.utobj().printTestStep("Verify Public to all corporate users template created by Corporate user who does not have privilege to see private template by Franchisee owner user(FONew))");
			campaignCenterPage.searchTemplateByFilter(driver, secondtemplateNamecorp);
			
			boolean isTemplateNamePresentCorpaccessibilitypublictoallcorporateuserscorpFONew = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondtemplateNamecorp + "')]");
			if (!isTemplateNamePresentCorpaccessibilitypublictoallcorporateuserscorpFONew == false) {
				fc.utobj().throwsException("was not able to verify Public to all users template created by Corporate user who has privilege to see private template by Franchisee owner user(FONew))");
			}
			
			//Verify Private template created by Corporate user who has privilege to see private template by Franchisee owner user(FONew)
			fc.utobj().printTestStep("Verify Privare template created by Corporate user who does not have privilege to see private template by Franchisee owner user(FONew))");
			campaignCenterPage.searchTemplateByFilter(driver, thirdtemplateNamecorp);
			
			boolean isTemplateNamePresentCorpaccessibilityprivatecorpFONew = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + thirdtemplateNamecorp + "')]");
			if (!isTemplateNamePresentCorpaccessibilityprivatecorpFONew == false) {
				fc.utobj().throwsException("was not able to verify private template created by Corporate user who has privilege to see private template by Franchisee owner user(FONew))");
			}
			
			//Verify Public to all users (Status driven)template created by Corporate user who does not have privilege to see private template by Franchisee owner user(FONew)
			fc.utobj().printTestStep("Verify Public to all users (Status driven)template created by Corporate user who does not have privilege to see private template by Franchisee owner user(FONew))");
			campaignCenterPage.searchTemplateByFilter(driver, firsttemplateNamecorpstatus);

			boolean isTemplateNamePresentCorpaccessibilitypublictoalluserscorpstatusFONew = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firsttemplateNamecorpstatus + "')]");
			if (isTemplateNamePresentCorpaccessibilitypublictoalluserscorpstatusFONew == false) {
				fc.utobj().throwsException("was not able to verify Public to all users (Status driven)template created by Corporate user who has privilege to see private template by Franchisee owner user(FONew))");
			}
			//Verify Public to all corporate users template created by Corporate user who has privilege to see private template by Franchisee owner user(FONew)
			fc.utobj().printTestStep("Verify Public to all corporate users (Status driven)template created by Corporate user who does not have privilege to see private template by Franchisee owner user(FONew))");
			campaignCenterPage.searchTemplateByFilter(driver, secondtemplateNamecorpstatus);
			
			boolean isTemplateNamePresentCorpaccessibilitypublictoallcorporateuserscorpstatusFONew = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondtemplateNamecorpstatus + "')]");
			if (!isTemplateNamePresentCorpaccessibilitypublictoallcorporateuserscorpstatusFONew == false) {
				fc.utobj().throwsException("was not able to verify Public to all users (Status driven)template created by Corporate user who has privilege to see private template by Franchisee owner user(FONew))");
			}
			
			//Verify Private template created by Corporate user who has privilege to see private template by Franchisee owner user(FONew)
			fc.utobj().printTestStep("Verify Privare (Status driven)template created by Corporate user who does not have privilege to see private template by Franchisee owner user(FONew))");
			campaignCenterPage.searchTemplateByFilter(driver, thirdtemplateNamecorpstatus);
			
			boolean isTemplateNamePresentCorpaccessibilityprivatecorpstatusFONew = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + thirdtemplateNamecorpstatus + "')]");
			if (!isTemplateNamePresentCorpaccessibilityprivatecorpstatusFONew == false) {
				fc.utobj().throwsException("was not able to verify private (Status driven)template created by Corporate user who has privilege to see private template by Franchisee owner user(FONew))");
			}
		
			fc.utobj().printTestStep("Navigate to CRM>Campaign Center>campaign");
			emailtemplate.clickoncampaigntemplate(driver);
			fc.utobj().clickElement(driver, crmcenter.corporatCampaign);
			//Verify Public to all users of my region Campaign created by Regional user who has privilege to see private campaign by Franchisee owner user(FONew)(R1)
			//Verifying Campaigns and templates of Regional user (R1) after logging with Franchisee owner user(FONew))
			fc.utobj().printTestStep("Verifying Campaigns and templates of Regional user (R1) after logging with Franchisee owner user(FONew))");
			fc.utobj().printTestStep("Verify Public to all users of my region Campaign created by Regional user who has privilege to see private campaign by Franchisee owner user(FONew))");
			campaignCenterPage.searchCampaignByFilter(driver, firstcampaignNamereg);

			boolean isCampaignNamePresentRegpaccessibilitypublictoallusersofmyregionFONew = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firstcampaignNamereg + "')]");
			if (isCampaignNamePresentRegpaccessibilitypublictoallusersofmyregionFONew == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my region Campaign created by Regional user who has privilege to see private campaign by Franchisee owner user(FONew))");
			}
			
			//Verify Private Campaign created by Regional user who has privilege to see private campaign by Franchisee owner user(FONew)
			fc.utobj().printTestStep("Verify Private Campaign created by Regional user who has privilege to see private campaign by Franchisee owner user(FONew)");
			campaignCenterPage.searchCampaignByFilter(driver, secondcampaignNamereg);
			
			boolean isCampaignNamePresentRegpaccessibilityprivateFONew = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondcampaignNamereg + "')]");
			if (!isCampaignNamePresentRegpaccessibilityprivateFONew == false) {
				fc.utobj().throwsException("was not able to verify Private Campaign created by Regional user who has privilege to see private campaign by Franchisee owner user(FONew))");
			}
			
			//Verify Public to all users of my region (Status Driven)Campaign created by Regional user who has privilege to see private campaign by Franchisee owner user(FONew)
			fc.utobj().printTestStep("Verify Public to all users of my region (Status Driven)Campaign created by Regional user who has privilege to see private campaign by Franchisee owner user(FONew))");
			campaignCenterPage.searchCampaignByFilter(driver, firstcampaignNameregstatus);

			boolean isCampaignNamePresentRegpaccessibilitypublictoallusersofmyregionstatusFONew = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firstcampaignNameregstatus + "')]");
			if (isCampaignNamePresentRegpaccessibilitypublictoallusersofmyregionstatusFONew == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my region (Status Driven)Campaign created by Regional user who has privilege to see private campaign by Franchisee owner user(FONew))");
			}
			
			//Verify Private Campaign created by Regional user who has privilege to see private campaign by Franchisee owner user(FONew)
			fc.utobj().printTestStep("Verify Private (Status Driven)Campaign created by Regional user who has privilege to see private campaign by Franchisee owner user(FONew))");
			campaignCenterPage.searchCampaignByFilter(driver, secondcampaignNameregstatus);
			
			boolean isCampaignNamePresentRegpaccessibilityprivatestatusFONew = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondcampaignNameregstatus + "')]");
			if (!isCampaignNamePresentRegpaccessibilityprivatestatusFONew == false) {
				fc.utobj().throwsException("was not able to verify Private (Status Driven)Campaign created by Regional user who has privilege to see private campaign by Franchisee owner user(FONew))");
			}
			
			fc.utobj().printTestStep("Navigate to CRM>Campaign Center>email template");
			emailtemplate.clickonemailtamplate(driver);
			
			//Verify Public to all users template created by Corporate user who has privilege to see private template by Franchisee owner user(FONew)
			fc.utobj().printTestStep("Verify Public to all users of my region template created by Regional user who has privilege to see private template by Franchisee owner user(FONew))");
			campaignCenterPage.searchTemplateByFilter(driver, firsttemplateNamereg);

			boolean isTemplateNamePresentRegaccessibilitypublictoallusersofmyregionFONew = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firsttemplateNamereg + "')]");
			if (isTemplateNamePresentRegaccessibilitypublictoallusersofmyregionFONew == false) {
				fc.utobj().throwsException("was not able to verify Public to all users template created by Regional user who has privilege to see private template by Franchisee owner user(FONew))");
			}
			
			//Verify Private template created by Regional user who has privilege to see private template by Franchisee owner user(FONew)
			fc.utobj().printTestStep("Verify Private template created by Regional user who has privilege to see private template by Franchisee owner user(FONew))");
			campaignCenterPage.searchTemplateByFilter(driver, secondtemplateNamereg);
			
			boolean isTemplateNamePresentRegaccessibilityprivateFONew = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondtemplateNamereg + "')]");
			if (!isTemplateNamePresentRegaccessibilityprivateFONew == false) {
				fc.utobj().throwsException("was not able to verify private template created by Regional user who has privilege to see private template by Franchisee owner user(FONew))");
			}
			
			//Verify Public to all users(Status Driven) template created by Regional user who has privilege to see private template by Franchisee owner user(FONew)
			fc.utobj().printTestStep("Verify Public to all users of my region (Status Driven)template created by Regional user who has privilege to see private template by Franchisee owner user(FONew))");
			campaignCenterPage.searchTemplateByFilter(driver, firsttemplateNameregstatus);

			boolean isTemplateNamePresentRegaccessibilitypublictoallusersofmyregionstatusFONew = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firsttemplateNameregstatus + "')]");
			if (isTemplateNamePresentRegaccessibilitypublictoallusersofmyregionstatusFONew == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my region (Status Driven)template created by Regional user who has privilege to see private template by Franchisee owner user(FONew))");
			}
			
			//Verify Private template created by Regional user who has privilege to see private template by Franchisee owner user(FONew)
			fc.utobj().printTestStep("Verify Private (Status Driven)template created by Regional user who has privilege to see private template by Franchisee owner user(FONew))");
			campaignCenterPage.searchTemplateByFilter(driver, secondtemplateNameregstatus);
			
			boolean isTemplateNamePresentRegaccessibilityprivatestatusFONew = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondtemplateNameregstatus + "')]");
			if (!isTemplateNamePresentRegaccessibilityprivatestatusFONew == false) {
				fc.utobj().throwsException("was not able to verify private (Status Driven)template created by Regional user who has privilege to see private template by Franchisee owner user(FONew))");
			}
			fc.utobj().printTestStep("Navigate to CRM>Campaign Center>campaign template");
			emailtemplate.clickoncampaigntemplate(driver);
			fc.utobj().clickElement(driver, crmcenter.corporatCampaign);
			
			//Verify Public to all users Campaign created by Corporate user who has privilege to see private campaign by Franchisee owner user(FONew)
			fc.utobj().printTestStep("Verifying Campaigns and templates of C1 by Franchisee Owner user(FONew)");
			fc.utobj().printTestStep("Verify Public to all users Campaign created by Corporate user who has privilege to see private campaign by Franchisee owner user(FONew)");
			campaignCenterPage.searchCampaignByFilter(driver, firstcampaignName);

			boolean isCampaignNamePresentCorpaccessibilitypublictoallusersFONew = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firstcampaignName + "')]");
			if (isCampaignNamePresentCorpaccessibilitypublictoallusersFONew == false) {
				fc.utobj().throwsException("was not able to verify Public to all users Campaign created by Corporate user who has privilege to see private campaign by Franchisee owner user(FONew)");
			}
			//Verify Public to all corporate users Campaign created by Corporate user who has privilege to see private campaign by Franchisee owner user(FONew)
			fc.utobj().printTestStep("Verify Public to all corporate users Campaign created by Corporate user who has privilege to see private campaign");
			campaignCenterPage.searchCampaignByFilter(driver, secondcampaignName);
			
			boolean isCampaignNamePresentCorpaccessibilitypublictoallcorporateusersFONew = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondcampaignName + "')]");
			if (!isCampaignNamePresentCorpaccessibilitypublictoallcorporateusersFONew == false) {
				fc.utobj().throwsException("was not able to verify Public to all corporate users Campaign created by Corporate user who has privilege to see private campaign by Franchisee owner user(FONew)");
			}
			
			//Verify Private Campaign created by Corporate user who has privilege to see private campaign by Franchisee owner user(FONew)
			fc.utobj().printTestStep("Verify Private Campaign created by Corporate user who has privilege to see private campaign by Franchisee owner user(FONew)");
			campaignCenterPage.searchCampaignByFilter(driver, thirdcampaignName);
			
			boolean isCampaignNamePresentCorpaccessibilityprivateFONew = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + thirdcampaignName + "')]");
			if (!isCampaignNamePresentCorpaccessibilityprivateFONew == false) {
				fc.utobj().throwsException("was not able to Private Campaign created by Corporate user who has privilege to see private campaign by Franchisee owner user(FONew)");
			}
			
			//Verify Public to all users(Status Driven) Campaign created by Corporate user who has privilege to see private campaign by Franchisee owner user(FONew)
			fc.utobj().printTestStep("Verify Public to all users(Status Driven) Campaign created by Corporate user who has privilege to see private campaign by Franchisee owner user(FONew)");
			campaignCenterPage.searchCampaignByFilter(driver, firstcampaignNamestatus);

			boolean isCampaignNamePresentCorpaccessibilitypublictoallusersstatusFONew = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firstcampaignNamestatus + "')]");
			if (isCampaignNamePresentCorpaccessibilitypublictoallusersstatusFONew == false) {
				fc.utobj().throwsException("was not able to verify Public to all users(Status Driven) Campaign created by Corporate user who has privilege to see private campaign by Franchisee owner user(FONew)");
			}
			//Verify Public to all corporate users(Status Driven) Campaign created by Corporate user who has privilege to see private campaign by Franchisee owner user(FONew)
			fc.utobj().printTestStep("Verify Public to all corporate users(Status Driven) Campaign created by Corporate user who has privilege to see private campaign");
			campaignCenterPage.searchCampaignByFilter(driver, secondcampaignNamestatus);
			
			boolean isCampaignNamePresentCorpaccessibilitypublictoallcorporateusersstatusFONew = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondcampaignNamestatus + "')]");
			if (!isCampaignNamePresentCorpaccessibilitypublictoallcorporateusersstatusFONew == false) {
				fc.utobj().throwsException("was not able to verify Public to all corporate users(Status Driven) Campaign created by Corporate user who has privilege to see private campaign by Franchisee owner user(FONew)");
			}
			
			//Verify Private Campaign created by Corporate user(Status Driven) who has privilege to see private campaign by Franchisee owner user(FONew)
			fc.utobj().printTestStep("Verify Private Campaign(Status Driven) created by Corporate user who has privilege to see private campaign by Franchisee owner user(FONew)");
			campaignCenterPage.searchCampaignByFilter(driver, thirdcampaignNamestatus);
			
			boolean isCampaignNamePresentCorpaccessibilityprivatestatusFONew = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + thirdcampaignNamestatus + "')]");
			if (!isCampaignNamePresentCorpaccessibilityprivatestatusFONew == false) {
				fc.utobj().throwsException("was not able to Private Campaign(Status Driven) created by Corporate user who has privilege to see private campaign by Franchisee owner user(FONew)");
			}
		
			fc.utobj().printTestStep("Navigate to CRM>Campaign Center>email template");
			emailtemplate.clickonemailtamplate(driver);
			
			//Verify Public to all users template created by Corporate user who has privilege to see private template by Franchisee owner user(FONew)
			fc.utobj().printTestStep("Verify Public to all users template created by Corporate user who has privilege to see private template by Franchisee owner user(FONew)");
			campaignCenterPage.searchTemplateByFilter(driver, firsttemplateName);

			boolean isTemplateNamePresentCorpaccessibilitypublictoallusersFONew = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firsttemplateName + "')]");
			if (isTemplateNamePresentCorpaccessibilitypublictoallusersFONew == false) {
				fc.utobj().throwsException("was not able to verify Public to all users template created by Corporate user who has privilege to see private template by Franchisee owner user(FONew)");
			}
			//Verify Public to all corporate users template created by Corporate user who has privilege to see private template by Franchisee owner user(FONew)
			fc.utobj().printTestStep("Verify Public to all corporate users template created by Corporate user who has privilege to see private template by Franchisee owner user(FONew)");
			campaignCenterPage.searchTemplateByFilter(driver, secondtemplateName);
			
			boolean isTemplateNamePresentCorpaccessibilitypublictoallcorporateusersFONew = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondtemplateName + "')]");
			if (!isTemplateNamePresentCorpaccessibilitypublictoallcorporateusersFONew == false) {
				fc.utobj().throwsException("was not able to verify Public to all users template created by Corporate user who has privilege to see private template by Franchisee owner user(FONew)");
			}
			
			//Verify Private template created by Corporate user who has privilege to see private template by Franchisee owner user(FONew)
			fc.utobj().printTestStep("Verify Privare template created by Corporate user who has privilege to see private template by Franchisee owner user(FONew)");
			campaignCenterPage.searchTemplateByFilter(driver, thirdtemplateName);
			
			boolean isTemplateNamePresentCorpaccessibilityprivateFONew = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + thirdtemplateName + "')]");
			if (!isTemplateNamePresentCorpaccessibilityprivateFONew == false) {
				fc.utobj().throwsException("was not able to verify private template created by Corporate user who has privilege to see private template by Franchisee owner user(FONew)");
			}
				
			//Verify Public to all users template(Status Driven) created by Corporate user who has privilege to see private template by Franchisee owner user(FONew)
			fc.utobj().printTestStep("Verify Public to all users template(Status Driven) created by Corporate user who has privilege to see private template by Franchisee owner user(FONew)");
			campaignCenterPage.searchTemplateByFilter(driver, firsttemplateNamestatus);

			boolean isTemplateNamePresentCorpaccessibilitypublictoallusersstatusFONew= fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firsttemplateNamestatus + "')]");
			if (isTemplateNamePresentCorpaccessibilitypublictoallusersstatusFONew == false) {
				fc.utobj().throwsException("was not able to verify Public to all users template(Status Driven) created by Corporate user who has privilege to see private template by Franchisee owner user(FONew)");
			}
			//Verify Public to all corporate users template(Status Driven) created by Corporate user who has privilege to see private template by Franchisee owner user(FONew)
			fc.utobj().printTestStep("Verify Public to all corporate users template(Status Driven) created by Corporate user who has privilege to see private template by Franchisee owner user(FONew)");
			campaignCenterPage.searchTemplateByFilter(driver, secondtemplateNamestatus);
			
			boolean isTemplateNamePresentCorpaccessibilitypublictoallcorporateusersstatusFONew = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondtemplateNamestatus + "')]");
			if (!isTemplateNamePresentCorpaccessibilitypublictoallcorporateusersstatusFONew == false) {
				fc.utobj().throwsException("was not able to verify Public to all users template(Status Driven) created by Corporate user who has privilege to see private template by Franchisee owner user(FONew)");
			}
			
			//Verify Private template(Status Driven) created by Corporate user who has privilege to see private template by Franchisee owner user(FONew)
			fc.utobj().printTestStep("Verify Privare template(Status Driven) created by Corporate user who has privilege to see private template by Franchisee owner user(FONew)");
			campaignCenterPage.searchTemplateByFilter(driver, thirdtemplateNamestatus);
			
			boolean isTemplateNamePresentCorpaccessibilityprivatestatusFONew = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + thirdtemplateNamestatus + "')]");
			if (!isTemplateNamePresentCorpaccessibilityprivatestatusFONew == false) {
				fc.utobj().throwsException("was not able to verify private template(Status Driven) created by Corporate user who has privilege to see private template by Franchisee owner user(FONew)");
			}
		
			fc.utobj().printTestStep("Navigate to CRM>Campaign Center>campaign template");
			emailtemplate.clickoncampaigntemplate(driver);
			fc.utobj().clickElement(driver, crmcenter.corporatCampaign);
			//Verify Public to all users Campaign created by Corporate user who has privilege to see private campaign by Franchisee owner user(FONew)
			fc.utobj().printTestStep("Verifying Campaigns and templates of Regional user (R2) by Franchisee owner user(FONew))");
			//Verify Public to all users of my region Campaign created by Regional user who does not have privilege to see private campaign by Franchisee owner user(FONew)
			fc.utobj().printTestStep("Verify Public to all users of my region Campaign created by Regional user who does not have privilege to see private campaign by Franchisee owner user(FONew))");
			campaignCenterPage.searchCampaignByFilter(driver, firstcampaignNamereg1);

			boolean isCampaignNamePresentRegpaccessibilitypublictoallusersofmyregion1FONew = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firstcampaignNamereg1 + "')]");
			if (isCampaignNamePresentRegpaccessibilitypublictoallusersofmyregion1FONew == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my region Campaign created by Regional user who does not have privilege to see private campaign by Franchisee owner user(FONew))");
			}
			
			//Verify Private Campaign created by Regional user who does not have privilege to see private campaign by Franchisee owner user(FONew)
			fc.utobj().printTestStep("Verify Private Campaign created by Regional user who does not have privilege to see private campaign by Franchisee owner user(FONew))");
			campaignCenterPage.searchCampaignByFilter(driver, secondcampaignNamereg1);
			
			boolean isCampaignNamePresentRegpaccessibilityprivate1FONew = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondcampaignNamereg1 + "')]");
			if (!isCampaignNamePresentRegpaccessibilityprivate1FONew == false) {
				fc.utobj().throwsException("was not able to verify Private Campaign created by Regional user who does not have privilege to see private campaign by Franchisee owner user(FONew))");
			}
			
			//Verify Public to all users of my region (Status Driven)Campaign created by Regional user who does not have privilege to see private campaign by Franchisee owner user(FONew)
			fc.utobj().printTestStep("Verify Public to all users of my region (Status Driven)Campaign created by Regional user who does not have privilege to see private campaign by Franchisee owner user(FONew))");
			campaignCenterPage.searchCampaignByFilter(driver, firstcampaignNamereg1status);

			boolean isCampaignNamePresentRegpaccessibilitypublictoallusersofmyregion1statusFONew = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firstcampaignNamereg1status + "')]");
			if (isCampaignNamePresentRegpaccessibilitypublictoallusersofmyregion1statusFONew == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my region (Status Driven)Campaign created by Regional user who does not have privilege to see private campaign by Franchisee owner user(FONew))");
			}
			
			//Verify Private (Status Driven)Campaign created by Regional user who does not have privilege to see private campaign by Franchisee owner user(FONew)
			fc.utobj().printTestStep("Verify Private (Status Driven)Campaign created by Regional user who does not have privilege to see private campaign by Franchisee owner user(FONew))");
			campaignCenterPage.searchCampaignByFilter(driver, secondcampaignNamereg1status);
			
			boolean isCampaignNamePresentRegpaccessibilityprivate1statusFONew = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondcampaignNamereg1status + "')]");
			if (!isCampaignNamePresentRegpaccessibilityprivate1statusFONew == false) {
				fc.utobj().throwsException("was not able to verify Private (Status Driven)Campaign created by Regional user who does not have privilege to see private campaign by Franchisee owner user(FONew))");
			}
		
			fc.utobj().printTestStep("Navigate to CRM>Campaign Center>email template");
			emailtemplate.clickonemailtamplate(driver);
			
			//Verify Public to all users template created by Corporate user who does not have privilege to see private template by Franchisee owner user(FONew)
			fc.utobj().printTestStep("Verify Public to all users of my region template created by Regional user who does not have privilege to see private template by Franchisee owner user(FONew))");
			campaignCenterPage.searchTemplateByFilter(driver, firsttemplateNamereg1);

			boolean isTemplateNamePresentRegaccessibilitypublictoallusersofmyregion1FONew = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firsttemplateNamereg1 + "')]");
			if (isTemplateNamePresentRegaccessibilitypublictoallusersofmyregion1FONew == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my region template created by Regional user who does not have privilege to see private template by Franchisee owner user(FONew))");
			}
			
			//Verify Private template created by Regional user who does not have privilege to see private template by Franchisee owner user(FONew)
			fc.utobj().printTestStep("Verify Private template created by Regional user does not have privilege to see private template by Franchisee owner user(FONew))");
			campaignCenterPage.searchTemplateByFilter(driver, secondtemplateNamereg1);
			
			boolean isTemplateNamePresentRegaccessibilityprivate1FONew = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondtemplateNamereg1 + "')]");
			if (!isTemplateNamePresentRegaccessibilityprivate1FONew == false) {
				fc.utobj().throwsException("was not able to verify Private template created by Regional user does not have privilege to see private template by Franchisee owner user(FONew))");
			}
			
			//Verify Public to all users (Status Driven)template created by Regional user who does not have privilege to see private template by Franchisee owner user(FONew)
			fc.utobj().printTestStep("Verify Public to all users of my region (Status Driven)template created by Regional user who does not have privilege to see private template by Franchisee owner user(FONew))");
			campaignCenterPage.searchTemplateByFilter(driver, firsttemplateNamereg1status);

			boolean isTemplateNamePresentRegaccessibilitypublictoallusersofmyregion1statusFONew = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firsttemplateNamereg1status + "')]");
			if (isTemplateNamePresentRegaccessibilitypublictoallusersofmyregion1statusFONew == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my region (Status Driven)template created by Regional user who does not have privilege to see private template by Franchisee owner user(FONew))");
			}
			
			//Verify Private template created by Regional user who does not have privilege to see private template by Franchisee owner user(FONew)
			fc.utobj().printTestStep("Verify Private (Status Driven)template created by Regional user does not have privilege to see private template by Franchisee owner user(FONew))");
			campaignCenterPage.searchTemplateByFilter(driver, secondtemplateNamereg1status);
			
			boolean isTemplateNamePresentRegaccessibilityprivate1statusFONew = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondtemplateNamereg1status + "')]");
			if (!isTemplateNamePresentRegaccessibilityprivate1statusFONew == false) {
				fc.utobj().throwsException("was not able to verify Private (Status Driven)template created by Regional user does not have privilege to see private template by Franchisee owner user(FONew))");
			}
			
			fc.utobj().printTestStep("Navigate to CRM>Campaign Center>campaign template");
			emailtemplate.clickoncampaigntemplate(driver);
			
			fc.utobj().printTestStep("Verifying Campaigns and templates of Franchise owner user (F0New) by Franchisee owner user(FONew))(FONew)");
			fc.utobj().printTestStep("Verify Public to all users of my Franchise Campaign created by Franchise user who has privilege to see private campaign by itself");
			campaignCenterPage.searchCampaignByFilter(driver, firstcampaignNamefran);

			boolean isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchiseFONew = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firstcampaignNamefran + "')]");
			if (isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchiseFONew == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my Franchise Campaign created by Franchise user who has privilege to see private campaign by itself");
			}
			
			//Verify Private Campaign created by Franchise user who has privilege to see private campaign by Franchisee owner user(FONew)
			fc.utobj().printTestStep("Verify Private Campaign created by Franchise user who has privilege to see private campaign by itself");
			campaignCenterPage.searchCampaignByFilter(driver, secondcampaignNamefran);
			
			boolean isCampaignNamePresentFranpaccessibilityprivateFONew = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondcampaignNamefran + "')]");
			if (isCampaignNamePresentFranpaccessibilityprivateFONew == false) {
				fc.utobj().throwsException("was not able to verify Private Campaign created by Franchise user who has privilege to see private campaign by itself");
			}
			
			
			//Verify Public to all users of my Franchise (Status Driven)Campaign created by Franchise user who has privilege to see private campaign by Franchisee owner user(FONew)
			fc.utobj().printTestStep("Verify Public to all users of my Franchise (Status Driven)Campaign created by Franchise user who has privilege to see private campaign by itself");
			campaignCenterPage.searchCampaignByFilter(driver, firstcampaignNamefranstatus);

			boolean isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchisestatusFONew = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firstcampaignNamefranstatus + "')]");
			if (isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchisestatusFONew == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my Franchise (Status Driven)Campaign created by Franchise user who has privilege to see private campaign by itself");
			}
			
			//Verify Private Campaign created by Franchise user who has privilege to see private campaign by Franchisee owner user(FONew)
			fc.utobj().printTestStep("Verify Private (Status Driven)Campaign created by Franchise user who has privilege to see private campaign by itself");
			campaignCenterPage.searchCampaignByFilter(driver, secondcampaignNamefranstatus);
			
			boolean isCampaignNamePresentFranpaccessibilityprivatestatusFONew = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondcampaignNamefranstatus + "')]");
			if (isCampaignNamePresentFranpaccessibilityprivatestatusFONew == false) {
				fc.utobj().throwsException("was not able to verify Private (Status Driven)Campaign created by Franchise user who has privilege to see private campaign by itself");
			}
			
			fc.utobj().printTestStep("Navigate to CRM>Campaign Center>email template");
			emailtemplate.clickonemailtamplate(driver);
			fc.utobj().clickElement(driver, crmcenter.locationtemplate);
			//Verify Public to all users template created by Corporate user who has privilege to see private template by Franchisee owner user(FONew)
			fc.utobj().printTestStep("Verify Public to all users of my franchise template created by franchise user who has privilege to see private template by itself");
			campaignCenterPage.searchTemplateByFilter(driver, firsttemplateNamefran);

			boolean isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchiseFONew = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firsttemplateNamefran + "')]");
			if (isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchiseFONew == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my franchise template created by franchise user who has privilege to see private template by itself");
			}
			
			//Verify Private template created by franchise user who has privilege to see private template by Franchisee owner user(FONew)
			fc.utobj().printTestStep("Verify Private template created by franchise user who has privilege to see private template by itself");
			campaignCenterPage.searchTemplateByFilter(driver, secondtemplateNamefran);
			
			boolean isTemplateNamePresentfranaccessibilityprivateFONew = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondtemplateNamefran + "')]");
			if (isTemplateNamePresentfranaccessibilityprivateFONew == false) {
				fc.utobj().throwsException("was not able to verify private template created by franchise user who has privilege to see private template by itself");
			}
			
			//Verify Public to all users (Status Driven)template created by Corporate user who has privilege to see private template by Franchisee owner user(FONew)
			fc.utobj().printTestStep("Verify Public to all users of my franchise (Status Driven)template created by franchise user who has privilege to see private template by itself");
			campaignCenterPage.searchTemplateByFilter(driver, firsttemplateNamefranstatus);

			boolean isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchisestatusFONew = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firsttemplateNamefranstatus + "')]");
			if (isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchisestatusFONew == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my franchise (Status Driven)template created by franchise user who has privilege to see private template by itself");
			}
			
			//Verify Private template created by franchise user who has privilege to see private template by Franchisee owner user(FONew)
			fc.utobj().printTestStep("Verify Private (Status Driven)template created by franchise user who has privilege to see private template by itself");
			campaignCenterPage.searchTemplateByFilter(driver, secondtemplateNamefranstatus);
			
			boolean isTemplateNamePresentfranaccessibilityprivatestatusFONew = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondtemplateNamefranstatus + "')]");
			if (isTemplateNamePresentfranaccessibilityprivatestatusFONew == false) {
				fc.utobj().throwsException("was not able to verify private (Status Driven)template created by franchise user who has privilege to see private template by itself");
			}
			
			fc.utobj().printTestStep("Navigate to CRM>Campaign Center>campaign template");
			emailtemplate.clickoncampaigntemplate(driver);
			
			fc.utobj().printTestStep("Verifying Campaigns and templates of Franchise mu owner user (V1) by Franchisee owner user(FONew)");
			
			fc.utobj().printTestStep("Verify Public to all users of my Franchise Campaign created by Franchise MU user(V1) who has privilege to see private campaign by Franchisee owner user(FONew)");
			campaignCenterPage.searchCampaignByFilter(driver, firstcampaignNamefranmu);

			boolean isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchisemuFONew = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firstcampaignNamefranmu + "')]");
			if (isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchisemuFONew == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my Franchise Campaign created by Franchise MU user(V1) who has privilege to see private campaign by Franchisee owner user(FONew))");
			}
			
			//Verify Private Campaign created by Franchise user who has privilege to see private campaign by Franchisee owner user(FONew)
			fc.utobj().printTestStep("Verify Private Campaign created by Franchise MU user(V1) who has privilege to see private campaign by Franchisee owner user(FONew))");
			campaignCenterPage.searchCampaignByFilter(driver, secondcampaignNamefranmu);
			
			boolean isCampaignNamePresentFranpaccessibilityprivatemuFONew = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondcampaignNamefranmu + "')]");
			if (isCampaignNamePresentFranpaccessibilityprivatemuFONew == false) {
				fc.utobj().throwsException("was not able to verify Private Campaign created by Franchise MU user(V1) who has privilege to see private campaign by Franchisee owner user(FONew))");
			}
			
			
			//Verify Public to all users of my Franchise (Status Driven)Campaign created by Franchise user who has privilege to see private campaign by Franchisee owner user(FONew)
			fc.utobj().printTestStep("Verify Public to all users of my Franchise (Status Driven)Campaign created by Franchise MU user(V1) who has privilege to see private campaign by Franchisee owner user(FONew))");
			campaignCenterPage.searchCampaignByFilter(driver, firstcampaignNamefranmustatus);

			boolean isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchisemustatusFONew = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firstcampaignNamefranmustatus + "')]");
			if (isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchisemustatusFONew == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my Franchise (Status Driven)Campaign created by Franchise MU user(V1) who has privilege to see private campaign by Franchisee owner user(FONew))");
			}
			
			//Verify Private Campaign created by Franchise user who has privilege to see private campaign by Franchisee owner user(FONew)
			fc.utobj().printTestStep("Verify Private (Status Driven)Campaign created by Franchise MU user(V1) who has privilege to see private campaign by Franchisee owner user(FONew))");
			campaignCenterPage.searchCampaignByFilter(driver, secondcampaignNamefranmustatus);
			
			boolean isCampaignNamePresentFranpaccessibilityprivatemustatusFONew = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondcampaignNamefranmustatus + "')]");
			if (isCampaignNamePresentFranpaccessibilityprivatemustatusFONew == false) {
				fc.utobj().throwsException("was not able to verify Private (Status Driven)Campaign created by Franchise MU user(V1) who has privilege to see private campaign by Franchisee owner user(FONew))");
			}
			
			fc.utobj().printTestStep("Navigate to CRM>Campaign Center>email template");
			emailtemplate.clickonemailtamplate(driver);
			fc.utobj().clickElement(driver, crmcenter.locationtemplate);
			//Verify Public to all users template created by Corporate user who has privilege to see private template by Franchisee owner user(FONew)
			fc.utobj().printTestStep("Verify Public to all users of my franchise template created by franchise MU user(V1) who has privilege to see private template by Franchisee owner user(FONew))");
			campaignCenterPage.searchTemplateByFilter(driver, firsttemplateNamefranmu);

			boolean isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchisemuFONew = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firsttemplateNamefranmu + "')]");
			if (isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchisemuFONew == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my franchise template created by franchise MU user(V1) who has privilege to see private template by Franchisee owner user(FONew))");
			}
			
			//Verify Private template created by franchise user who has privilege to see private template by Franchisee owner user(FONew)
			fc.utobj().printTestStep("Verify Private template created by franchise MU user(V1) who has privilege to see private template by Franchisee owner user(FONew))");
			campaignCenterPage.searchTemplateByFilter(driver, secondtemplateNamefranmu);
			
			boolean isTemplateNamePresentfranaccessibilityprivatemuFONew = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondtemplateNamefranmu + "')]");
			if (isTemplateNamePresentfranaccessibilityprivatemuFONew == false) {
				fc.utobj().throwsException("was not able to verify private template created by franchise MU user(V1) who has privilege to see private template by Franchisee owner user(FONew))");
			}
			
			//Verify Public to all users template created by Corporate user who has privilege to see private template by Franchisee owner user(FONew)
			fc.utobj().printTestStep("Verify Public to all users of my franchise (Status Driven)template created by franchise MU user(V1) who has privilege to see private template by Franchisee owner user(FONew))");
			campaignCenterPage.searchTemplateByFilter(driver, firsttemplateNamefranmustatus);

			boolean isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchisemustatusFONew = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firsttemplateNamefranmustatus + "')]");
			if (isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchisemustatusFONew == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my franchise (Status Driven)template created by franchise MU user(V1) who has privilege to see private template by Franchisee owner user(FONew))");
			}
			
			//Verify Private template created by franchise user who has privilege to see private template by Franchisee owner user(FONew)
			fc.utobj().printTestStep("Verify Private (Status Driven)template created by franchise MU user(V1) who has privilege to see private template by Franchisee owner user(FONew))");
			campaignCenterPage.searchTemplateByFilter(driver, secondtemplateNamefranmustatus);
			
			boolean isTemplateNamePresentfranaccessibilityprivatemustatusFONew = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondtemplateNamefranmustatus + "')]");
			if (isTemplateNamePresentfranaccessibilityprivatemustatusFONew == false) {
				fc.utobj().throwsException("was not able to verify private (Status Driven)template created by franchise MU user(V1) who has privilege to see private template by Franchisee owner user(FONew))");
			}
			
			
			
			
			fc.utobj().printTestStep("Navigate to CRM>Campaign Center>campaign template");
			emailtemplate.clickoncampaigntemplate(driver);
			fc.utobj().printTestStep("Verifying Campaigns and templates of V2 by Franchisee owner user(FONew)");
			fc.utobj().printTestStep("Verify Public to all users of my Franchise Campaign created by V2) who does not have privilege to see private campaign by Franchisee owner user(FONew)");
			campaignCenterPage.searchCampaignByFilter(driver, firstcampaignNamefranmue2);

			boolean isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchisemue2FONew = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firstcampaignNamefranmue2 + "')]");
			if (isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchisemue2FONew == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my Franchise Campaign created by V2) who does not have privilege to see private campaign by Franchisee owner user(FONew)");
			}
			
			//Verify Private Campaign created by Franchise user who has privilege to see private campaign by Franchisee owner user(FONew)
			fc.utobj().printTestStep("Verify Private Campaign created by V2) who does not have privilege to see private campaign by Franchisee owner user(FONew)");
			campaignCenterPage.searchCampaignByFilter(driver, secondcampaignNamefranmue2);
			
			boolean isCampaignNamePresentFranpaccessibilityprivatemue2FONew = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondcampaignNamefranmue2 + "')]");
			if (isCampaignNamePresentFranpaccessibilityprivatemue2FONew == false) {
				fc.utobj().throwsException("was not able to verify Private Campaign created by V2) who does not have privilege to see private campaign by Franchisee owner user(FONew)");
			}
			
			
			//Verify Public to all users of my Franchise Campaign created by V2) who has privilege to see private campaign by Franchisee owner user(FONew)
			fc.utobj().printTestStep("Verify Public to all users of my Franchise (Status Driven)Campaign created by V2) who does not have privilege to see private campaign by Franchisee owner user(FONew)");
			campaignCenterPage.searchCampaignByFilter(driver, firstcampaignNamefranmue2status);

			boolean isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchisemue2statusFONew = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firstcampaignNamefranmue2status + "')]");
			if (isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchisemue2statusFONew == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my Franchise (Status Driven)Campaign created by V2) who does not have privilege to see private campaign by Franchisee owner user(FONew)");
			}
			
			//Verify Private Campaign created by Franchise user who has privilege to see private campaign by Franchisee owner user(FONew)
			fc.utobj().printTestStep("Verify Private (Status Driven)Campaign created by V2) who does not have privilege to see private campaign by Franchisee owner user(FONew)");
			campaignCenterPage.searchCampaignByFilter(driver, secondcampaignNamefranmue2status);
			
			boolean isCampaignNamePresentFranpaccessibilityprivatemue2statusFONew = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondcampaignNamefranmue2status + "')]");
			if (isCampaignNamePresentFranpaccessibilityprivatemue2statusFONew == false) {
				fc.utobj().throwsException("was not able to verify Private (Status Driven)Campaign created by V2) who does not have privilege to see private campaign by Franchisee owner user(FONew)");
			}
			
			fc.utobj().printTestStep("Navigate to CRM>Campaign Center>email template");
			emailtemplate.clickonemailtamplate(driver);
			fc.utobj().clickElement(driver, crmcenter.locationtemplate);
			//Verify Public to all users template created by Franchise user who has privilege to see private template by Franchisee owner user(FONew)
			fc.utobj().printTestStep("Verify Public to all users of my franchise template created by V2) who does not have privilege to see private template by Franchisee owner user(FONew)");
			campaignCenterPage.searchTemplateByFilter(driver, firsttemplateNamefranmue2);

			boolean isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchisemue2FONew = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firsttemplateNamefranmue2 + "')]");
			if (isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchisemue2FONew == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my franchise template created by V2) who has privilege to see private template by Franchisee owner user(FONew)");
			}
			
			//Verify Private template created by franchise user who has privilege to see private template by Franchisee owner user(FONew)
			fc.utobj().printTestStep("Verify Private template created by V2) who has privilege to see private template by Franchisee owner user(FONew)");
			campaignCenterPage.searchTemplateByFilter(driver, secondtemplateNamefranmue2);
			
			boolean isTemplateNamePresentfranaccessibilityprivatemue2FONew = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondtemplateNamefranmue2 + "')]");
			if (isTemplateNamePresentfranaccessibilityprivatemue2FONew == false) {
				fc.utobj().throwsException("was not able to verify private template created by V2) who has privilege to see private template by Franchisee owner user(FONew)");
			}
			
			//Verify Public to all users template created by Franchise user who has privilege to see private template by Franchisee owner user(FONew)
			fc.utobj().printTestStep("Verify Public to all users of my franchise (Status Driven)template created by V2) who does not have privilege to see private template by Franchisee owner user(FONew)");
			campaignCenterPage.searchTemplateByFilter(driver, firsttemplateNamefranmue2status);

			boolean isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchisemue2statusFONew = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firsttemplateNamefranmue2status + "')]");
			if (isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchisemue2statusFONew == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my franchise (Status Driven)template created by V2) who has privilege to see private template by Franchisee owner user(FONew)");
			}
			
			//Verify Private template created by franchise user who has privilege to see private template by Franchisee owner user(FONew)
			fc.utobj().printTestStep("Verify Private (Status Driven)template created by V2) who has privilege to see private template by Franchisee owner user(FONew)");
			campaignCenterPage.searchTemplateByFilter(driver, secondtemplateNamefranmue2status);
			
			boolean isTemplateNamePresentfranaccessibilityprivatemue2statusFONew = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondtemplateNamefranmue2status + "')]");
			if (isTemplateNamePresentfranaccessibilityprivatemue2statusFONew == false) {
				fc.utobj().throwsException("was not able to verify private (Status Driven)template created by V2) who has privilege to see private template by Franchisee owner user(FONew)");
			}
			
			if(isCampaignNamePresentCorpaccessibilitypublictoalluserscorpFONew && !isCampaignNamePresentCorpaccessibilitypublictoallcorporateuserscorpFONew && !isCampaignNamePresentCorpaccessibilityprivatecorpFONew 
					&& isCampaignNamePresentCorpaccessibilitypublictoalluserscorpstatusFONew && !isCampaignNamePresentCorpaccessibilitypublictoallcorporateuserscorpstatusFONew && !isCampaignNamePresentCorpaccessibilityprivatecorpstatusFONew 
					&& isTemplateNamePresentCorpaccessibilitypublictoalluserscorpFONew && !isTemplateNamePresentCorpaccessibilitypublictoallcorporateuserscorpFONew && !isTemplateNamePresentCorpaccessibilityprivatecorpFONew 
					&& isTemplateNamePresentCorpaccessibilitypublictoalluserscorpstatusFONew && !isTemplateNamePresentCorpaccessibilitypublictoallcorporateuserscorpstatusFONew && !isTemplateNamePresentCorpaccessibilityprivatecorpstatusFONew 
					&& isCampaignNamePresentRegpaccessibilitypublictoallusersofmyregionFONew && !isCampaignNamePresentRegpaccessibilityprivateFONew && isCampaignNamePresentRegpaccessibilitypublictoallusersofmyregionstatusFONew 
					&& !isCampaignNamePresentRegpaccessibilityprivatestatusFONew && isTemplateNamePresentRegaccessibilitypublictoallusersofmyregionFONew && !isTemplateNamePresentRegaccessibilityprivateFONew 
					&& isTemplateNamePresentRegaccessibilitypublictoallusersofmyregionstatusFONew && !isTemplateNamePresentRegaccessibilityprivatestatusFONew && isCampaignNamePresentCorpaccessibilitypublictoallusersFONew 
					&& !isCampaignNamePresentCorpaccessibilitypublictoallcorporateusersFONew && !isCampaignNamePresentCorpaccessibilityprivateFONew && isCampaignNamePresentCorpaccessibilitypublictoallusersstatusFONew 
					&& !isCampaignNamePresentCorpaccessibilitypublictoallcorporateusersstatusFONew && !isCampaignNamePresentCorpaccessibilityprivatestatusFONew && isTemplateNamePresentCorpaccessibilitypublictoallusersFONew 
					&& !isTemplateNamePresentCorpaccessibilitypublictoallcorporateusersFONew && !isTemplateNamePresentCorpaccessibilityprivateFONew && isTemplateNamePresentCorpaccessibilitypublictoallusersstatusFONew 
					&& !isTemplateNamePresentCorpaccessibilitypublictoallcorporateusersstatusFONew && !isTemplateNamePresentCorpaccessibilityprivatestatusFONew && isCampaignNamePresentRegpaccessibilitypublictoallusersofmyregion1FONew
					&& !isCampaignNamePresentRegpaccessibilityprivate1FONew && isCampaignNamePresentRegpaccessibilitypublictoallusersofmyregion1statusFONew && !isCampaignNamePresentRegpaccessibilityprivate1statusFONew
					&& isTemplateNamePresentRegaccessibilitypublictoallusersofmyregion1FONew && !isTemplateNamePresentRegaccessibilityprivate1FONew && isTemplateNamePresentRegaccessibilitypublictoallusersofmyregion1statusFONew 
					&& !isTemplateNamePresentRegaccessibilityprivate1statusFONew && isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchiseFONew && isCampaignNamePresentFranpaccessibilityprivateFONew 
					&& isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchisestatusFONew && isCampaignNamePresentFranpaccessibilityprivatestatusFONew && isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchiseFONew 
					&& isTemplateNamePresentfranaccessibilityprivateFONew && isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchisestatusFONew && isTemplateNamePresentfranaccessibilityprivatestatusFONew  
					&& isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchisemuFONew && isCampaignNamePresentFranpaccessibilityprivatemuFONew && isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchisemustatusFONew 
					&& isCampaignNamePresentFranpaccessibilityprivatemustatusFONew && isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchisemuFONew && isTemplateNamePresentfranaccessibilityprivatemuFONew 
					&& isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchisemustatusFONew && isTemplateNamePresentfranaccessibilityprivatemustatusFONew && isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchisemue2FONew 
					&& isCampaignNamePresentFranpaccessibilityprivatemue2FONew && isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchisemue2statusFONew && isCampaignNamePresentFranpaccessibilityprivatemue2statusFONew 
					&& isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchisemue2FONew && isTemplateNamePresentfranaccessibilityprivatemue2FONew && isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchisemue2statusFONew
					&& isTemplateNamePresentfranaccessibilityprivatemue2statusFONew) {
				fc.utobj().printTestStep("able to verify all types of campaigns and templates created by corporate/rgional/franchise users when loging with Franchisee owner user(FONew)) who has priviledge to see private campaign");
			}
			else {
				fc.utobj().throwsException("was not able to verify all types of campaigns and templates created by corporate/rgional/franchise users when loging with Franchisee owner user(FONew)) who has priviledge to see private campaign");
			}
			
			fc.utobj().printTestStep("Done");
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
			fc.utobj().printTestStep("LogOut");
			
		}
		catch(Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}
	
	
	
	
	@Test(groups = {"CRM_Visibility_Mu","crm"})
	@TestCase(createdOn = "2018-10-23", updatedOn = "2018-10-23", testCaseDescription = "To verify the visibility of all types of campaign/template (Code your Own and layout and Theme)created by mu,employee and owner users with eachother", testCaseId = "Verify_Campaign_Templates_MU")
	public void visibilityofcampaignMu() throws Exception {
	
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());
		String area="Campaign_Visibility";
		/*Map<String, String> dataSetmu = fc.utobj().readTestDatawithsqllite("crm", testCaseId,area);*/
		Map<String, String> dataSet = fc.utobj().readTestDatawithsqllite("crm", "PTA_Campaign_From_Corporate_User",area);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		AdminUsersManageManageFranchiseUsersPage pobj = new AdminUsersManageManageFranchiseUsersPage(driver);
		AdminUsersRolesAddNewRolePageTest setpriviledge=new AdminUsersRolesAddNewRolePageTest();
		try {
			driver = fc.loginpage().login(driver);
			FCHomePageTest fcHomePageTest= new FCHomePageTest();
			CRMCampaignCenterPage crmcenter=new CRMCampaignCenterPage(driver);
			String moduleName = dataSet.get("CorpmoduleName");
	
			//Add New Franchise Role(FR2) which can not View Private Campaigns / Templates of Other Users
			fc.utobj().printTestStep("Add New Franchise Role(FR2) which can not View Private Campaigns / Templates of Other Users");
			String roleNamewithoutpri = fc.utobj().generateTestData(dataSet.get("FranroleNamewithoutpri2"));//FR2
			String roleType = dataSet.get("FranroleType2");
			Map<String, String> withoutprivileges = new HashMap<String, String>();
			withoutprivileges.put(dataSet.get("Privilege"), dataSet.get("PrivilegeNo"));
			setpriviledge.addRolesWithModulesPrivileges(driver, roleType, roleNamewithoutpri, moduleName, withoutprivileges, testCaseId);
			
			fc.utobj().printTestStep("Add a new area region");
			AdminAreaRegionAddAreaRegionPageTest p1 = new AdminAreaRegionAddAreaRegionPageTest();
			String regionName = fc.utobj().generateTestData(dataSet.get("Regionname"));
			p1.addAreaRegionwithSQlite(driver, regionName,dataSet);
	
			
	
			//Mu user		
			//Add a franchise location with same region
			//L6
			fc.utobj().printTestStep("Add a franchise location with same region");
			String firstNameF5 = fc.utobj().generateTestData(dataSet.get("Franlocfirstname"));
			String lastNameF5 = fc.utobj().generateTestData(dataSet.get("Franloclasttname"));
			fc.utobj().printTestStep("Naviaget To Admin > Franchise Location > Add Franchise Location");
			AdminFranchiseLocationAddFranchiseLocationPageTest franchiseLocation5 = new AdminFranchiseLocationAddFranchiseLocationPageTest();
			String franchiseId5 = fc.utobj().generateTestData(dataSet.get("Franchiseid"));
			franchiseLocation5.addFranchiseLocationForMktwithoutaddingarearegion(driver, franchiseId5, regionName, firstNameF5, lastNameF5,dataSet);
			
			//Add Franchise Mu User
			//V1
			//Add Franchise owner mu User who does not have role to see private campaign
			fc.utobj().printTestStep("Add Franchise owner mu User who does not have role to see private campaign");
			AdminUsersManageManageFranchiseUsersPageTest addFranUsermu = new AdminUsersManageManageFranchiseUsersPageTest();
			//username and password for Owner type franchise user
			String userNameFOmu = fc.utobj().generateTestData(dataSet.get("Franowneruname"));
			String roleNameFOmu = roleNamewithoutpri;//dataSet.get("Franownerrolename");
			String passwordFOmu = dataSet.get("Franownerpassword");
			String emailIdFOmu =fc.utobj().generateTestData(dataSet.get("Franowneremail"))+"@staffex.com";
			userNameFOmu = addFranUsermu.addFranchiseUserwithoutchangeinsettings(driver, userNameFOmu, passwordFOmu, franchiseId5, roleNameFOmu, emailIdFOmu,dataSet);
			System.out.println("(V1)Username and password for franchise mu user is"+userNameFOmu+" "+passwordFOmu);
			
			//Add a new Franchise location with same region but owner as existing owner
			//L7
			fc.utobj().printTestStep("Add a new Franchise location with same region but owner as existing owner");
			String franchiseId6 = fc.utobj().generateTestData(dataSet.get("Franchiseid"));
			AdminFranchiseLocationAddFranchiseLocationPageTest franchiseLocation6 = new AdminFranchiseLocationAddFranchiseLocationPageTest();
			franchiseLocation6.addFranchiseLocationForMktwithoutaddingarearegionwithexistingowner(driver, franchiseId6, regionName, firstNameF5, lastNameF5, dataSet);
			
			//Add Franchise employee user
			//V2
			fc.utobj().printTestStep("Add Franchise employee User who does not have role to see private campaign");
			AdminUsersManageManageFranchiseUsersPageTest addFranEmpUsermu = new AdminUsersManageManageFranchiseUsersPageTest();
			String userNameFEmu = fc.utobj().generateTestData(dataSet.get("Franempuname"));
			String roleNameFEmu =roleNamewithoutpri;//dataSet.get("Franownerrolename");
			String passwordFEmu = dataSet.get("Franemppassword");
			String emailIdFEmu =fc.utobj().generateTestData(dataSet.get("Franempemail"))+"@staffex.com";
			userNameFEmu = addFranEmpUsermu.addFranchiseEmployeeUserwithoutchangeinsettings(driver, userNameFEmu, passwordFEmu, franchiseId6, roleNameFEmu, emailIdFEmu,dataSet);
			System.out.println("(V2)Username and password for franchise employee existing mu user is"+userNameFEmu+" "+passwordFEmu);
			
			//Add a new Franchisee owner user with location same as existing location of V2
			AdminPageTest admninpage=new AdminPageTest();
			admninpage.manageFranchiseLocationLnk(driver);
			AdminPage adminpage=new AdminPage(driver);
			fc.utobj().selectValFromMultiSelect(driver, adminpage.franchiseid,franchiseId6);//franchiseId6//Locationjain2
			fc.utobj().clickElement(driver,adminpage.searchbuttonoffranchisee);
			fc.utobj().clickElement(driver, adminpage.actionmenuoffranchisee);
			fc.utobj().clickElement(driver,adminpage.usersoffranchisee);
			fc.utobj().clickElement(driver,adminpage.addowner);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			String first=fc.utobj().generateTestData(dataSet.get("FranNewowner"));
			String last=fc.utobj().generateTestData(dataSet.get("FranNewownersurname"));
			String fullname=first+" "+last;
			fc.utobj().sendKeys(driver,adminpage.firstName,first);
			fc.utobj().sendKeys(driver,adminpage.lastName,last);
			fc.utobj().sleep(3000);
			fc.utobj().clickElement(driver, adminpage.save);
			fc.utobj().sleep(3000);
			fc.commonMethods().Click_Close_Input_ByValue(driver);
			fc.utobj().sleep(3000);
			fc.utobj().switchFrameToDefault(driver);
			fc.utobj().clickElement(driver, ".//*[contains(text(),'"+fullname+"')]/ancestor::tr[1]/td[4]/div/layer/a/img");
			fc.utobj().clickElement(driver,"//span[contains(text(),'Generate Login')]");
			/*fc.utobj().actionImgOption(driver, fullname,dataSet.get("Frangeneratlogin"));*/
			
		
			//Add New Franchise owner User who does not role to see private campaign
			fc.utobj().printTestStep("Add New Franchise owner User who does not have role to see private campaign");
			//username and password for Owner type franchise user
			String userNameFONew = fc.utobj().generateTestData(dataSet.get("Franowneruname"));
			String roleNameFONew = roleNamewithoutpri;//dataSet.get("Franownerrolename");
			String passwordFONew = dataSet.get("Franownerpassword");
			fc.utobj().sendKeys(driver, pobj.userName, userNameFONew);
			fc.utobj().sendKeys(driver, pobj.password, passwordFONew);
			fc.utobj().sendKeys(driver, pobj.confirmPassword, passwordFONew);
			fc.utobj().selectValFromMultiSelect(driver, pobj.selectRole, roleNameFONew);
			fc.utobj().clickElement(driver, pobj.submit);
			System.out.println("Franchisee owner(FONew) userName and Password are"+userNameFONew+" "+passwordFONew);
			
			
			
			
			CRMCampaignCenterPageTest campaignCenterPage = new CRMCampaignCenterPageTest();
			CRMCampaignCenterCampaignsPage pobjcam = new CRMCampaignCenterCampaignsPage(driver);
			CRMCampaignCenterPageTest emailtemplate=new CRMCampaignCenterPageTest();
		
			
			/*fc.utobj().clickElement(driver,"//*[@id='franchisee_no']/span/a");
			fc.utobj().clickElement(driver, "//div[@class='scroller-content']/*[contains(text(),'123456w25131065')]");*/
			
			//Login with Franchise Owner MU user(V1) who does not have role to see private campaign
			fc.utobj().printTestStep("Login with Franchise Owner MU user(V1) who does not have role to see private campaign");
			fc.loginpage().loginWithParameter(driver, userNameFOmu, passwordFOmu);
			fc.crm().crm_common().openCRMCampaignCenterPage(fcHomePageTest, driver);
			
			//Create Campaign At Campaign Center > Campaigns
			fc.utobj().printTestStep("Create Campaign At Campaign Center > Campaigns");
			String firstcampaignNamefranmu = fc.utobj().generateTestData(dataSet.get("CorpcampaignName"));
			String firstaccessibilityfranmu = dataSet.get("FranaccessibilityPublictoallUsersofmyFranchise");
			String firstcampaignTypefranmu = dataSet.get("CorpcampaignType");
			String firsttemplateNamefranmu = fc.utobj().generateTestData(dataSet.get("CorptemplateName"));
			String firstemailSubjectfranmu = fc.utobj().generateTestData(dataSet.get("CorpemailSubject"));
			String firstplainTextEditorfranmu = fc.utobj().generateTestData(dataSet.get("CorpplainTextEditor"));
			campaignCenterPage.createCampaignGenericwithSqlite(driver, dataSet, firstcampaignNamefranmu, firstaccessibilityfranmu,
					firstcampaignTypefranmu, firsttemplateNamefranmu, firstemailSubjectfranmu, firstplainTextEditorfranmu);
			
			//Create another campaign at Campaign Center > Campaigns
			fc.utobj().printTestStep("Create another campaign at Campaign Center > Campaigns");
			String secondcampaignNamefranmu = fc.utobj().generateTestData(dataSet.get("CorpcampaignName"));
			String privateaccessibilityfranmu = dataSet.get("Franaccessibilityprivate");
			String secondcampaignTypefranmu = dataSet.get("CorpcampaignType");
			String secondtemplateNamefranmu = fc.utobj().generateTestData(dataSet.get("CorptemplateName"));
			String secondemailSubjectfranmu = fc.utobj().generateTestData(dataSet.get("CorpemailSubject"));
			String secondplainTextEditorfranmu = fc.utobj().generateTestData(dataSet.get("CorpplainTextEditor"));
			campaignCenterPage.createCampaignGenericwithSqlite(driver, dataSet, secondcampaignNamefranmu, privateaccessibilityfranmu,
					secondcampaignTypefranmu, secondtemplateNamefranmu, secondemailSubjectfranmu, secondplainTextEditorfranmu);
			
			//Create Status driven campaign at At Campaign Center > Campaigns
			fc.utobj().printTestStep("Create Status driven campaign At Campaign Center > Campaigns");
			String firstcampaignNamefranmustatus = fc.utobj().generateTestData(dataSet.get("CorpcampaignName"));
			String firstaccessibilityfranmustatus = dataSet.get("FranaccessibilityPublictoallUsersofmyFranchise");
			String firstcampaignTypefranmustatus = dataSet.get("StatusCampaigntype");
			String firsttemplateNamefranmustatus = fc.utobj().generateTestData(dataSet.get("CorptemplateName"));
			String firstemailSubjectfranmustatus = fc.utobj().generateTestData(dataSet.get("CorpemailSubject"));
			String firstplainTextEditorfranmustatus = fc.utobj().generateTestData(dataSet.get("CorpplainTextEditor"));
			campaignCenterPage.createCampaignGenericwithSqlite(driver, dataSet, firstcampaignNamefranmustatus, firstaccessibilityfranmustatus,
					firstcampaignTypefranmustatus, firsttemplateNamefranmustatus, firstemailSubjectfranmustatus, firstplainTextEditorfranmustatus);
			
			//Create Status Driven campaign at Campaign Center > Campaigns
			fc.utobj().printTestStep("Create Status Driven campaign at Campaign Center > Campaigns");
			String secondcampaignNamefranmustatus = fc.utobj().generateTestData(dataSet.get("CorpcampaignName"));
			String privateaccessibilityfranmustatus = dataSet.get("Franaccessibilityprivate");
			String secondcampaignTypefranmustatus = dataSet.get("StatusCampaigntype");
			String secondtemplateNamefranmustatus = fc.utobj().generateTestData(dataSet.get("CorptemplateName"));
			String secondemailSubjectfranmustatus = fc.utobj().generateTestData(dataSet.get("CorpemailSubject"));
			String secondplainTextEditorfranmustatus = fc.utobj().generateTestData(dataSet.get("CorpplainTextEditor"));
			campaignCenterPage.createCampaignGenericwithSqlite(driver, dataSet, secondcampaignNamefranmustatus, privateaccessibilityfranmustatus,
					secondcampaignTypefranmustatus, secondtemplateNamefranmustatus, secondemailSubjectfranmustatus, secondplainTextEditorfranmustatus);
			
			fc.utobj().clickElement(driver, pobjcam.viewAllButton);
			
			//Verify Public to all users of my Franchise Campaign created by Franchise user who does not have privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Public to all users of my Franchise Campaign created by Franchise MU user(V1) who does not have privilege to see private campaign by itself");
			campaignCenterPage.searchCampaignByFilter(driver, firstcampaignNamefranmu);

			boolean isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchisemu = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firstcampaignNamefranmu + "')]");
			if (isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchisemu == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my Franchise Campaign created by Franchise MU user(V1) who does not have privilege to see private campaign by itself");
			}
			
			//Verify Private Campaign created by Franchise user who does not have privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Private Campaign created by Franchise MU user(V1) who does not have privilege to see private campaign by itself");
			campaignCenterPage.searchCampaignByFilter(driver, secondcampaignNamefranmu);
			
			boolean isCampaignNamePresentFranpaccessibilityprivatemu = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondcampaignNamefranmu + "')]");
			if (isCampaignNamePresentFranpaccessibilityprivatemu == false) {
				fc.utobj().throwsException("was not able to verify Private Campaign created by Franchise MU user(V1) who does not have privilege to see private campaign by itself");
			}
			
			
			//Verify Public to all users of my Franchise (Status Driven)Campaign created by Franchise user who does not have privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Public to all users of my Franchise (Status Driven)Campaign created by Franchise MU user(V1) who does not have privilege to see private campaign by itself");
			campaignCenterPage.searchCampaignByFilter(driver, firstcampaignNamefranmustatus);

			boolean isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchisemustatus = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firstcampaignNamefranmustatus + "')]");
			if (isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchisemustatus == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my Franchise (Status Driven)Campaign created by Franchise MU user(V1) who does not have privilege to see private campaign by itself");
			}
			
			//Verify Private Campaign created by Franchise user who does not have privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Private (Status Driven)Campaign created by Franchise MU user(V1) who does not have privilege to see private campaign by itself");
			campaignCenterPage.searchCampaignByFilter(driver, secondcampaignNamefranmustatus);
			
			boolean isCampaignNamePresentFranpaccessibilityprivatemustatus = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondcampaignNamefranmustatus + "')]");
			if (isCampaignNamePresentFranpaccessibilityprivatemustatus == false) {
				fc.utobj().throwsException("was not able to verify Private (Status Driven)Campaign created by Franchise MU user(V1) who does not have privilege to see private campaign by itself");
			}
			
			fc.utobj().printTestStep("Navigate to CRM>Campaign Center>email template");
			emailtemplate.clickonemailtamplate(driver);
			fc.utobj().clickElement(driver, crmcenter.locationtemplate);
			//Verify Public to all users template created by Corporate user who does not have privilege to see private template by itself
			fc.utobj().printTestStep("Verify Public to all users of my franchise template created by franchise MU user(V1) who does not have privilege to see private template by itself");
			campaignCenterPage.searchTemplateByFilter(driver, firsttemplateNamefranmu);

			boolean isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchisemu = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firsttemplateNamefranmu + "')]");
			if (isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchisemu == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my franchise template created by franchise MU user(V1) who does not have privilege to see private template by itself");
			}
			
			//Verify Private template created by franchise user who does not have privilege to see private template by itself
			fc.utobj().printTestStep("Verify Private template created by franchise MU user(V1) who does not have privilege to see private template by itself");
			campaignCenterPage.searchTemplateByFilter(driver, secondtemplateNamefranmu);
			
			boolean isTemplateNamePresentfranaccessibilityprivatemu = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondtemplateNamefranmu + "')]");
			if (isTemplateNamePresentfranaccessibilityprivatemu == false) {
				fc.utobj().throwsException("was not able to verify private template created by franchise MU user(V1) who does not have privilege to see private template by itself");
			}
			
			//Verify Public to all users template created by Corporate user who does not have privilege to see private template by itself
			fc.utobj().printTestStep("Verify Public to all users of my franchise (Status Driven)template created by franchise MU user(V1) who does not have privilege to see private template by itself");
			campaignCenterPage.searchTemplateByFilter(driver, firsttemplateNamefranmustatus);

			boolean isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchisemustatus = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firsttemplateNamefranmustatus + "')]");
			if (isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchisemustatus == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my franchise (Status Driven)template created by franchise MU user(V1) who does not have privilege to see private template by itself");
			}
			
			//Verify Private template created by franchise user who does not have privilege to see private template by itself
			fc.utobj().printTestStep("Verify Private (Status Driven)template created by franchise MU user(V1) who does not have privilege to see private template by itself");
			campaignCenterPage.searchTemplateByFilter(driver, secondtemplateNamefranmustatus);
			
			boolean isTemplateNamePresentfranaccessibilityprivatemustatus = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondtemplateNamefranmustatus + "')]");
			if (isTemplateNamePresentfranaccessibilityprivatemustatus == false) {
				fc.utobj().throwsException("was not able to verify private (Status Driven)template created by franchise MU user(V1) who does not have privilege to see private template by itself");
			}
			
			//Login with Franchise Employee user(V2) who does not have role to see private campaign
			fc.utobj().printTestStep("Login with Franchise Employee user(V2) who does not have role to see private campaign");
			fc.loginpage().loginWithParameter(driver, userNameFEmu, passwordFEmu);
			fc.crm().crm_common().openCRMCampaignCenterPage(fcHomePageTest, driver);
			
			//Create Campaign At Campaign Center > Campaigns
			fc.utobj().printTestStep("Create Campaign At Campaign Center > Campaigns");
			String firstcampaignNamefranmue2 = fc.utobj().generateTestData(dataSet.get("CorpcampaignName"));
			String firstaccessibilityfranmue2 = dataSet.get("FranaccessibilityPublictoallUsersofmyFranchise");
			String firstcampaignTypefranmue2 = dataSet.get("CorpcampaignType");
			String firsttemplateNamefranmue2 = fc.utobj().generateTestData(dataSet.get("CorptemplateName"));
			String firstemailSubjectfranmue2 = fc.utobj().generateTestData(dataSet.get("CorpemailSubject"));
			String firstplainTextEditorfranmue2 = fc.utobj().generateTestData(dataSet.get("CorpplainTextEditor"));
			campaignCenterPage.createCampaignGenericwithSqlite(driver, dataSet, firstcampaignNamefranmue2, firstaccessibilityfranmue2,
					firstcampaignTypefranmue2, firsttemplateNamefranmue2, firstemailSubjectfranmue2, firstplainTextEditorfranmue2);
			
			//Create another campaign at Campaign Center > Campaigns
			fc.utobj().printTestStep("Create another campaign at Campaign Center > Campaigns");
			String secondcampaignNamefranmue2 = fc.utobj().generateTestData(dataSet.get("CorpcampaignName"));
			String privateaccessibilityfranmue2 = dataSet.get("Franaccessibilityprivate");
			String secondcampaignTypefranmue2 = dataSet.get("CorpcampaignType");
			String secondtemplateNamefranmue2 = fc.utobj().generateTestData(dataSet.get("CorptemplateName"));
			String secondemailSubjectfranmue2 = fc.utobj().generateTestData(dataSet.get("CorpemailSubject"));
			String secondplainTextEditorfranmue2 = fc.utobj().generateTestData(dataSet.get("CorpplainTextEditor"));
			campaignCenterPage.createCampaignGenericwithSqlite(driver, dataSet, secondcampaignNamefranmue2, privateaccessibilityfranmue2,
					secondcampaignTypefranmue2, secondtemplateNamefranmue2, secondemailSubjectfranmue2, secondplainTextEditorfranmue2);
			
			
			//Create Status Driven campaign At Campaign Center > Campaigns
			fc.utobj().printTestStep("Create Status Driven campaign At Campaign Center > Campaigns");
			String firstcampaignNamefranmue2status = fc.utobj().generateTestData(dataSet.get("CorpcampaignName"));
			String firstaccessibilityfranmue2status = dataSet.get("FranaccessibilityPublictoallUsersofmyFranchise");
			String firstcampaignTypefranmue2status = dataSet.get("StatusCampaigntype");
			String firsttemplateNamefranmue2status = fc.utobj().generateTestData(dataSet.get("CorptemplateName"));
			String firstemailSubjectfranmue2status = fc.utobj().generateTestData(dataSet.get("CorpemailSubject"));
			String firstplainTextEditorfranmue2status = fc.utobj().generateTestData(dataSet.get("CorpplainTextEditor"));
			campaignCenterPage.createCampaignGenericwithSqlite(driver, dataSet, firstcampaignNamefranmue2status, firstaccessibilityfranmue2status,
					firstcampaignTypefranmue2status, firsttemplateNamefranmue2status, firstemailSubjectfranmue2status, firstplainTextEditorfranmue2status);
			
			//Create another campaign at Campaign Center > Campaigns
			fc.utobj().printTestStep("Create another campaign at Campaign Center > Campaigns");
			String secondcampaignNamefranmue2status = fc.utobj().generateTestData(dataSet.get("CorpcampaignName"));
			String privateaccessibilityfranmue2status = dataSet.get("Franaccessibilityprivate");
			String secondcampaignTypefranmue2status = dataSet.get("StatusCampaigntype");
			String secondtemplateNamefranmue2status = fc.utobj().generateTestData(dataSet.get("CorptemplateName"));
			String secondemailSubjectfranmue2status = fc.utobj().generateTestData(dataSet.get("CorpemailSubject"));
			String secondplainTextEditorfranmue2status = fc.utobj().generateTestData(dataSet.get("CorpplainTextEditor"));
			campaignCenterPage.createCampaignGenericwithSqlite(driver, dataSet, secondcampaignNamefranmue2status, privateaccessibilityfranmue2status,
					secondcampaignTypefranmue2status, secondtemplateNamefranmue2status, secondemailSubjectfranmue2status, secondplainTextEditorfranmue2status);
			
			
			fc.utobj().clickElement(driver, pobjcam.viewAllButton);
			
			//Verify Public to all users of my Franchise Campaign created by Franchise Employee user(V2) who does not have privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Public to all users of my Franchise Campaign created by Franchise Employee user(V2) who does not have privilege to see private campaign by itself");
			campaignCenterPage.searchCampaignByFilter(driver, firstcampaignNamefranmue2);

			boolean isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchisemue2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firstcampaignNamefranmue2 + "')]");
			if (isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchisemue2 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my Franchise Campaign created by Franchise Employee user(V2) who does not have privilege to see private campaign by itself");
			}
			
			//Verify Private Campaign created by Franchise user who does not have privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Private Campaign created by Franchise Employee user(V2) who does not have privilege to see private campaign by itself");
			campaignCenterPage.searchCampaignByFilter(driver, secondcampaignNamefranmue2);
			
			boolean isCampaignNamePresentFranpaccessibilityprivatemue2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondcampaignNamefranmue2 + "')]");
			if (isCampaignNamePresentFranpaccessibilityprivatemue2 == false) {
				fc.utobj().throwsException("was not able to verify Private Campaign created by Franchise Employee user(V2) who does not have privilege to see private campaign by itself");
			}
			
			
			//Verify Public to all users of my Franchise Campaign created by Franchise Employee user(V2) who does not have privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Public to all users of my Franchise (Status Driven)Campaign created by Franchise Employee user(V2) who does not have privilege to see private campaign by itself");
			campaignCenterPage.searchCampaignByFilter(driver, firstcampaignNamefranmue2status);

			boolean isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchisemue2status = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firstcampaignNamefranmue2status + "')]");
			if (isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchisemue2status == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my Franchise (Status Driven)Campaign created by Franchise Employee user(V2) who does not have privilege to see private campaign by itself");
			}
			
			//Verify Private Campaign created by Franchise user who does not have privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Private (Status Driven)Campaign created by Franchise Employee user(V2) who does not have privilege to see private campaign by itself");
			campaignCenterPage.searchCampaignByFilter(driver, secondcampaignNamefranmue2status);
			
			boolean isCampaignNamePresentFranpaccessibilityprivatemue2status = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondcampaignNamefranmue2status + "')]");
			if (isCampaignNamePresentFranpaccessibilityprivatemue2status == false) {
				fc.utobj().throwsException("was not able to verify Private (Status Driven)Campaign created by Franchise Employee user(V2) who does not have privilege to see private campaign by itself");
			}
			
			fc.utobj().printTestStep("Navigate to CRM>Campaign Center>email template");
			emailtemplate.clickonemailtamplate(driver);
			fc.utobj().clickElement(driver, crmcenter.locationtemplate);
			//Verify Public to all users template created by Franchise user who does not have privilege to see private template by itself
			fc.utobj().printTestStep("Verify Public to all users of my franchise template created by franchise Employee user(V2) who does not have privilege to see private template by itself");
			campaignCenterPage.searchTemplateByFilter(driver, firsttemplateNamefranmue2);

			boolean isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchisemue2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firsttemplateNamefranmue2 + "')]");
			if (isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchisemue2 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my franchise template created by franchise Employee user(V2) who does not have privilege to see private template by itself");
			}
			
			//Verify Private template created by franchise user who does not have privilege to see private template by itself
			fc.utobj().printTestStep("Verify Private template created by franchise Employee user(V2) who does not have privilege to see private template by itself");
			campaignCenterPage.searchTemplateByFilter(driver, secondtemplateNamefranmue2);
			
			boolean isTemplateNamePresentfranaccessibilityprivatemue2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondtemplateNamefranmue2 + "')]");
			if (isTemplateNamePresentfranaccessibilityprivatemue2 == false) {
				fc.utobj().throwsException("was not able to verify private template created by franchise Employee user(V2) who does not have privilege to see private template by itself");
			}
			
			//Verify Public to all users template created by Franchise user who does not have privilege to see private template by itself
			fc.utobj().printTestStep("Verify Public to all users of my franchise (Status Driven)template created by franchise Employee user(V2) who does not have privilege to see private template by itself");
			campaignCenterPage.searchTemplateByFilter(driver, firsttemplateNamefranmue2status);

			boolean isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchisemue2status = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firsttemplateNamefranmue2status + "')]");
			if (isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchisemue2status == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my franchise (Status Driven)template created by franchise Employee user(V2) who does not have privilege to see private template by itself");
			}
			
			//Verify Private template created by franchise user who does not have privilege to see private template by itself
			fc.utobj().printTestStep("Verify Private (Status Driven)template created by franchise Employee user(V2) who does not have privilege to see private template by itself");
			campaignCenterPage.searchTemplateByFilter(driver, secondtemplateNamefranmue2status);
			
			boolean isTemplateNamePresentfranaccessibilityprivatemue2status = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondtemplateNamefranmue2status + "')]");
			if (isTemplateNamePresentfranaccessibilityprivatemue2status == false) {
				fc.utobj().throwsException("was not able to verify private (Status Driven)template created by franchise Employee user(V2) who does not have privilege to see private template by itself");
			}
			
			
			//Login with Franchise Owner user(FONew) who does not have role to see private campaign
			fc.utobj().printTestStep("Login with Franchise Owner user(FONew) who does not have role to see private campaign");
			fc.loginpage().loginWithParameter(driver, userNameFONew, passwordFONew);
			fc.crm().crm_common().openCRMCampaignCenterPage(fcHomePageTest, driver);
			
			//Create Campaign At Campaign Center > Campaigns
			fc.utobj().printTestStep("Create Campaign At Campaign Center > Campaigns");
			String firstcampaignNamefran = fc.utobj().generateTestData(dataSet.get("CorpcampaignName"));
			String firstaccessibilityfran = dataSet.get("FranaccessibilityPublictoallUsersofmyFranchise");
			String firstcampaignTypefran = dataSet.get("CorpcampaignType");
			String firsttemplateNamefran = fc.utobj().generateTestData(dataSet.get("CorptemplateName"));
			String firstemailSubjectfran = fc.utobj().generateTestData(dataSet.get("CorpemailSubject"));
			String firstplainTextEditorfran = fc.utobj().generateTestData(dataSet.get("CorpplainTextEditor"));
			campaignCenterPage.createCampaignGenericwithSqlite(driver, dataSet, firstcampaignNamefran, firstaccessibilityfran,
					firstcampaignTypefran, firsttemplateNamefran, firstemailSubjectfran, firstplainTextEditorfran);
			
			//Create another campaign at Campaign Center > Campaigns
			fc.utobj().printTestStep("Create another campaign at Campaign Center > Campaigns");
			String secondcampaignNamefran = fc.utobj().generateTestData(dataSet.get("CorpcampaignName"));
			String privateaccessibilityfran = dataSet.get("Franaccessibilityprivate");
			String secondcampaignTypefran = dataSet.get("CorpcampaignType");
			String secondtemplateNamefran = fc.utobj().generateTestData(dataSet.get("CorptemplateName"));
			String secondemailSubjectfran = fc.utobj().generateTestData(dataSet.get("CorpemailSubject"));
			String secondplainTextEditorfran = fc.utobj().generateTestData(dataSet.get("CorpplainTextEditor"));
			campaignCenterPage.createCampaignGenericwithSqlite(driver, dataSet, secondcampaignNamefran, privateaccessibilityfran,
					secondcampaignTypefran, secondtemplateNamefran, secondemailSubjectfran, secondplainTextEditorfran);
			
			//Create Status Driven campaign At Campaign Center > Campaigns
			fc.utobj().printTestStep("Create Status Driven campaign At Campaign Center > Campaigns");
			String firstcampaignNamefranstatus = fc.utobj().generateTestData(dataSet.get("CorpcampaignName"));
			String firstaccessibilityfranstatus = dataSet.get("FranaccessibilityPublictoallUsersofmyFranchise");
			String firstcampaignTypefranstatus = dataSet.get("StatusCampaigntype");
			String firsttemplateNamefranstatus = fc.utobj().generateTestData(dataSet.get("CorptemplateName"));
			String firstemailSubjectfranstatus = fc.utobj().generateTestData(dataSet.get("CorpemailSubject"));
			String firstplainTextEditorfranstatus = fc.utobj().generateTestData(dataSet.get("CorpplainTextEditor"));
			campaignCenterPage.createCampaignGenericwithSqlite(driver, dataSet, firstcampaignNamefranstatus, firstaccessibilityfranstatus,
					firstcampaignTypefranstatus, firsttemplateNamefranstatus, firstemailSubjectfranstatus, firstplainTextEditorfranstatus);
			
			//Create Status Driven campaign 1 at Campaign Center > Campaigns
			fc.utobj().printTestStep("Create Status Driven campaign 1 at Campaign Center > Campaigns");
			String secondcampaignNamefranstatus = fc.utobj().generateTestData(dataSet.get("CorpcampaignName"));
			String privateaccessibilityfranstatus = dataSet.get("Franaccessibilityprivate");
			String secondcampaignTypefranstatus = dataSet.get("StatusCampaigntype");
			String secondtemplateNamefranstatus = fc.utobj().generateTestData(dataSet.get("CorptemplateName"));
			String secondemailSubjectfranstatus = fc.utobj().generateTestData(dataSet.get("CorpemailSubject"));
			String secondplainTextEditorfranstatus = fc.utobj().generateTestData(dataSet.get("CorpplainTextEditor"));
			campaignCenterPage.createCampaignGenericwithSqlite(driver, dataSet, secondcampaignNamefranstatus, privateaccessibilityfranstatus,
					secondcampaignTypefranstatus, secondtemplateNamefranstatus, secondemailSubjectfranstatus, secondplainTextEditorfranstatus);
			
			fc.utobj().clickElement(driver, pobjcam.viewAllButton);
			
			//Verify Public to all users of my Franchise Campaign created by Franchise user who does not have privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Public to all users of my Franchise Campaign created by Franchise user who does not have privilege to see private campaign by itself");
			campaignCenterPage.searchCampaignByFilter(driver, firstcampaignNamefran);

			boolean isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchise = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firstcampaignNamefran + "')]");
			if (isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchise == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my Franchise Campaign created by Franchise user who does not have privilege to see private campaign by itself");
			}
			
			//Verify Private Campaign created by Franchise user who does not have privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Private Campaign created by Franchise user who does not have privilege to see private campaign by itself");
			campaignCenterPage.searchCampaignByFilter(driver, secondcampaignNamefran);
			
			boolean isCampaignNamePresentFranpaccessibilityprivate = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondcampaignNamefran + "')]");
			if (isCampaignNamePresentFranpaccessibilityprivate == false) {
				fc.utobj().throwsException("was not able to verify Private Campaign created by Franchise user who does not have privilege to see private campaign by itself");
			}
			
			
			//Verify Public to all users of my Franchise (Status Driven)Campaign created by Franchise user who does not have privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Public to all users of my Franchise (Status Driven)Campaign created by Franchise user who does not have privilege to see private campaign by itself");
			campaignCenterPage.searchCampaignByFilter(driver, firstcampaignNamefranstatus);

			boolean isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchisestatus = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firstcampaignNamefranstatus + "')]");
			if (isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchisestatus == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my Franchise (Status Driven)Campaign created by Franchise user who does not have privilege to see private campaign by itself");
			}
			
			//Verify Private Campaign created by Franchise user who does not have privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Private (Status Driven)Campaign created by Franchise user who does not have privilege to see private campaign by itself");
			campaignCenterPage.searchCampaignByFilter(driver, secondcampaignNamefranstatus);
			
			boolean isCampaignNamePresentFranpaccessibilityprivatestatus = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondcampaignNamefranstatus + "')]");
			if (isCampaignNamePresentFranpaccessibilityprivatestatus == false) {
				fc.utobj().throwsException("was not able to verify Private (Status Driven)Campaign created by Franchise user who does not have privilege to see private campaign by itself");
			}
			
			fc.utobj().printTestStep("Navigate to CRM>Campaign Center>email template");
			emailtemplate.clickonemailtamplate(driver);
			fc.utobj().clickElement(driver, crmcenter.locationtemplate);
			//Verify Public to all users template created by Corporate user who does not have privilege to see private template by itself
			fc.utobj().printTestStep("Verify Public to all users of my franchise template created by franchise user who does not have privilege to see private template by itself");
			campaignCenterPage.searchTemplateByFilter(driver, firsttemplateNamefran);

			boolean isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchise = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firsttemplateNamefran + "')]");
			if (isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchise == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my franchise template created by franchise user who does not have privilege to see private template by itself");
			}
			
			//Verify Private template created by franchise user who does not have privilege to see private template by itself
			fc.utobj().printTestStep("Verify Private template created by franchise user who does not have privilege to see private template by itself");
			campaignCenterPage.searchTemplateByFilter(driver, secondtemplateNamefran);
			
			boolean isTemplateNamePresentfranaccessibilityprivate = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondtemplateNamefran + "')]");
			if (isTemplateNamePresentfranaccessibilityprivate == false) {
				fc.utobj().throwsException("was not able to verify private template created by franchise user who does not have privilege to see private template by itself");
			}
			
			//Verify Public to all users (Status Driven)template created by Corporate user who does not have privilege to see private template by itself
			fc.utobj().printTestStep("Verify Public to all users of my franchise (Status Driven)template created by franchise user who does not have privilege to see private template by itself");
			campaignCenterPage.searchTemplateByFilter(driver, firsttemplateNamefranstatus);

			boolean isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchisestatus = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firsttemplateNamefranstatus + "')]");
			if (isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchisestatus == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my franchise (Status Driven)template created by franchise user who does not have privilege to see private template by itself");
			}
			
			//Verify Private template created by franchise user who does not have privilege to see private template by itself
			fc.utobj().printTestStep("Verify Private (Status Driven)template created by franchise user who does not have privilege to see private template by itself");
			campaignCenterPage.searchTemplateByFilter(driver, secondtemplateNamefranstatus);
			
			boolean isTemplateNamePresentfranaccessibilityprivatestatus = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondtemplateNamefranstatus + "')]");
			if (isTemplateNamePresentfranaccessibilityprivatestatus == false) {
				fc.utobj().throwsException("was not able to verify private (Status Driven)template created by franchise user who does not have privilege to see private template by itself");
			}
			
		
			//Verification of Mu user based on their locations
			//Login with Franchisee mu owner user(V1) who does not have role to see private campaign
			fc.utobj().printTestStep("Login with Franchisee(mu) owner user (V1) who does not have role to see private campaign");
			fc.loginpage().loginWithParameter(driver, userNameFOmu, passwordFOmu);
			
			
			//We have to write code Location wise
			//Let us assumes that first he clicked on mu user L6(V1) then it will show data only of mu user and not of employee(V2) and owner user(FONew) i.e of Location L7
			
			fc.utobj().printTestStep("Let us assumes that first he clicked on mu user L6(V1) then it will show data only of mu user and not of employee(V2) and owner user(FONew) i.e of Location L7");
			fc.utobj().clickElement(driver,"//*[@id='franchisee_no']/span/a");
			fc.utobj().clickElement(driver, "//div[@class='scroller-content']/*[contains(text(),'"+franchiseId5+"')]");
			
			fc.crm().crm_common().openCRMCampaignCenterPage(fcHomePageTest, driver);
			fc.utobj().printTestStep("Click on the menu option and select campaign");
			fc.utobj().printTestStep("Navigate to CRM>Campaign Center>campaign template");
			emailtemplate.clickoncampaigntemplate(driver);
			
			fc.utobj().printTestStep("Verifying Campaigns and templates of Franchise owner user (F0New) by Franchisee owner user(mu) with location of mu user(V1)(V1L6)");
			fc.utobj().printTestStep("Verify Public to all users of my Franchise Campaign created by Franchise user who does not have privilege to see private campaign by Franchisee owner user(mu) with location of mu user(V1)");
			campaignCenterPage.searchCampaignByFilter(driver, firstcampaignNamefran);

			boolean isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchiseV1L6 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firstcampaignNamefran + "')]");
			if (!isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchiseV1L6 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my Franchise Campaign created by Franchise user who does not have privilege to see private campaign by Franchisee owner user(mu) with location of mu user(V1)");
			}
			
			//Verify Private Campaign created by Franchise user who does not have privilege to see private campaign by Franchisee mu user(V1L6)
			fc.utobj().printTestStep("Verify Private Campaign created by Franchise user who does not have privilege to see private campaign by Franchisee owner user(mu) with location of mu user(V1)");
			campaignCenterPage.searchCampaignByFilter(driver, secondcampaignNamefran);
			
			boolean isCampaignNamePresentFranpaccessibilityprivateV1L6 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondcampaignNamefran + "')]");
			if (!isCampaignNamePresentFranpaccessibilityprivateV1L6 == false) {
				fc.utobj().throwsException("was not able to verify Private Campaign created by Franchise user who does not have privilege to see private campaign by Franchisee owner user(mu) with location of mu user(V1)");
			}
			
			
			//Verify Public to all users of my Franchise (Status Driven)Campaign created by Franchise user who does not have privilege to see private campaign by Franchisee mu user(V1L6)
			fc.utobj().printTestStep("Verify Public to all users of my Franchise (Status Driven)Campaign created by Franchise user who does not have privilege to see private campaign by Franchisee owner user(mu) with location of mu user(V1)");
			campaignCenterPage.searchCampaignByFilter(driver, firstcampaignNamefranstatus);

			boolean isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchisestatusV1L6 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firstcampaignNamefranstatus + "')]");
			if (!isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchisestatusV1L6 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my Franchise (Status Driven)Campaign created by Franchise user who does not have privilege to see private campaign by Franchisee owner user(mu) with location of mu user(V1)");
			}
			
			//Verify Private Campaign created by Franchise user who does not have privilege to see private campaign by Franchisee mu user(V1L6)
			fc.utobj().printTestStep("Verify Private (Status Driven)Campaign created by Franchise user who does not have privilege to see private campaign by Franchisee owner user(mu) with location of mu user(V1)");
			campaignCenterPage.searchCampaignByFilter(driver, secondcampaignNamefranstatus);
			
			boolean isCampaignNamePresentFranpaccessibilityprivatestatusV1L6 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondcampaignNamefranstatus + "')]");
			if (!isCampaignNamePresentFranpaccessibilityprivatestatusV1L6 == false) {
				fc.utobj().throwsException("was not able to verify Private (Status Driven)Campaign created by Franchise user who does not have privilege to see private campaign by Franchisee owner user(mu) with location of mu user(V1)");
			}
			
			fc.utobj().printTestStep("Navigate to CRM>Campaign Center>email template");
			emailtemplate.clickonemailtamplate(driver);
			fc.utobj().clickElement(driver, crmcenter.locationtemplate);
			//Verify Public to all users template created by Corporate user who does not have privilege to see private template by Franchisee mu user(V1L6)
			fc.utobj().printTestStep("Verify Public to all users of my franchise template created by franchise user who does not have privilege to see private template by Franchisee owner user(mu) with location of mu user(V1)");
			campaignCenterPage.searchTemplateByFilter(driver, firsttemplateNamefran);

			boolean isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchiseV1L6 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firsttemplateNamefran + "')]");
			if (!isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchiseV1L6 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my franchise template created by franchise user who does not have privilege to see private template by Franchisee owner user(mu) with location of mu user(V1)");
			}
			
			//Verify Private template created by franchise user who does not have privilege to see private template by Franchisee mu user(V1L6)
			fc.utobj().printTestStep("Verify Private template created by franchise user who does not have privilege to see private template by Franchisee owner user(mu) with location of mu user(V1)");
			campaignCenterPage.searchTemplateByFilter(driver, secondtemplateNamefran);
			
			boolean isTemplateNamePresentfranaccessibilityprivateV1L6 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondtemplateNamefran + "')]");
			if (!isTemplateNamePresentfranaccessibilityprivateV1L6 == false) {
				fc.utobj().throwsException("was not able to verify private template created by franchise user who does not have privilege to see private template by Franchisee owner user(mu) with location of mu user(V1)");
			}
			
			//Verify Public to all users (Status Driven)template created by Corporate user who does not have privilege to see private template by Franchisee mu user(V1L6)
			fc.utobj().printTestStep("Verify Public to all users of my franchise (Status Driven)template created by franchise user who does not have privilege to see private template by Franchisee owner user(mu) with location of mu user(V1)");
			campaignCenterPage.searchTemplateByFilter(driver, firsttemplateNamefranstatus);

			boolean isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchisestatusV1L6 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firsttemplateNamefranstatus + "')]");
			if (!isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchisestatusV1L6 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my franchise (Status Driven)template created by franchise user who does not have privilege to see private template by Franchisee owner user(mu) with location of mu user(V1)");
			}
			
			//Verify Private template created by franchise user who does not have privilege to see private template by Franchisee mu user(V1L6)
			fc.utobj().printTestStep("Verify Private (Status Driven)template created by franchise user who does not have privilege to see private template by Franchisee owner user(mu) with location of mu user(V1)");
			campaignCenterPage.searchTemplateByFilter(driver, secondtemplateNamefranstatus);
			
			boolean isTemplateNamePresentfranaccessibilityprivatestatusV1L6 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondtemplateNamefranstatus + "')]");
			if (!isTemplateNamePresentfranaccessibilityprivatestatusV1L6 == false) {
				fc.utobj().throwsException("was not able to verify private (Status Driven)template created by franchise user who does not have privilege to see private template by Franchisee owner user(mu) with location of mu user(V1)");
			}
			
			
			fc.utobj().printTestStep("Navigate to CRM>Campaign Center>campaign template");
			emailtemplate.clickoncampaigntemplate(driver);
			fc.utobj().printTestStep("Verifying Campaigns and templates of Franchise Employee user (V2) by Franchisee owner user(mu) with location of mu user(V1)");
			fc.utobj().printTestStep("Verify Public to all users of my Franchise Campaign created by Franchise Employee user(V2) who does not have privilege to see private campaign by Franchisee owner user(mu) with location of mu user(V1)");
			campaignCenterPage.searchCampaignByFilter(driver, firstcampaignNamefranmue2);

			boolean isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchisemue2V1L6 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firstcampaignNamefranmue2 + "')]");
			if (!isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchisemue2V1L6 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my Franchise Campaign created by Franchise Employee user(V2) who does not have privilege to see private campaign by Franchisee owner user(mu) with location of mu user(V1)");
			}
			
			//Verify Private Campaign created by Franchise user who does not have privilege to see private campaign by Franchisee mu user(V1L6)
			fc.utobj().printTestStep("Verify Private Campaign created by Franchise Employee user(V2) who does not have privilege to see private campaign by Franchisee owner user(mu) with location of mu user(V1)");
			campaignCenterPage.searchCampaignByFilter(driver, secondcampaignNamefranmue2);
			
			boolean isCampaignNamePresentFranpaccessibilityprivatemue2V1L6 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondcampaignNamefranmue2 + "')]");
			if (!isCampaignNamePresentFranpaccessibilityprivatemue2V1L6 == false) {
				fc.utobj().throwsException("was not able to verify Private Campaign created by Franchise Employee user(V2) who does not have privilege to see private campaign by Franchisee owner user(mu) with location of mu user(V1)");
			}
			
			
			//Verify Public to all users of my Franchise Campaign created by Franchise Employee user(V2) who does not have privilege to see private campaign by Franchisee mu user(V1L6)
			fc.utobj().printTestStep("Verify Public to all users of my Franchise (Status Driven)Campaign created by Franchise Employee user(V2) who does not have privilege to see private campaign by Franchisee owner user(mu) with location of mu user(V1)");
			campaignCenterPage.searchCampaignByFilter(driver, firstcampaignNamefranmue2status);

			boolean isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchisemue2statusV1L6 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firstcampaignNamefranmue2status + "')]");
			if (!isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchisemue2statusV1L6 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my Franchise (Status Driven)Campaign created by Franchise Employee user(V2) who does not have privilege to see private campaign by Franchisee owner user(mu) with location of mu user(V1)");
			}
			
			//Verify Private Campaign created by Franchise user who does not have privilege to see private campaign by Franchisee mu user(V1L6)
			fc.utobj().printTestStep("Verify Private (Status Driven)Campaign created by Franchise Employee user(V2) who does not have privilege to see private campaign by Franchisee owner user(mu) with location of mu user(V1)");
			campaignCenterPage.searchCampaignByFilter(driver, secondcampaignNamefranmue2status);
			
			boolean isCampaignNamePresentFranpaccessibilityprivatemue2statusV1L6 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondcampaignNamefranmue2status + "')]");
			if (!isCampaignNamePresentFranpaccessibilityprivatemue2statusV1L6 == false) {
				fc.utobj().throwsException("was not able to verify Private (Status Driven)Campaign created by Franchise Employee user(V2) who does not have privilege to see private campaign by Franchisee owner user(mu) with location of mu user(V1)");
			}
			
			fc.utobj().printTestStep("Navigate to CRM>Campaign Center>email template");
			emailtemplate.clickonemailtamplate(driver);
			fc.utobj().clickElement(driver, crmcenter.locationtemplate);
			//Verify Public to all users template created by Franchise user who does not have privilege to see private template by Franchisee mu user(V1L6)
			fc.utobj().printTestStep("Verify Public to all users of my franchise template created by franchise Employee user(V2) who does not have privilege to see private template by Franchisee owner user(mu) with location of mu user(V1)");
			campaignCenterPage.searchTemplateByFilter(driver, firsttemplateNamefranmue2);

			boolean isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchisemue2V1L6 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firsttemplateNamefranmue2 + "')]");
			if (!isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchisemue2V1L6 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my franchise template created by franchise Employee user(V2) who does not have privilege to see private template by Franchisee owner user(mu) with location of mu user(V1)");
			}
			
			//Verify Private template created by franchise user who does not have privilege to see private template by Franchisee mu user(V1L6)
			fc.utobj().printTestStep("Verify Private template created by franchise Employee user(V2) who does not have privilege to see private template by Franchisee owner user(mu) with location of mu user(V1)");
			campaignCenterPage.searchTemplateByFilter(driver, secondtemplateNamefranmue2);
			
			boolean isTemplateNamePresentfranaccessibilityprivatemue2V1L6 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondtemplateNamefranmue2 + "')]");
			if (!isTemplateNamePresentfranaccessibilityprivatemue2V1L6 == false) {
				fc.utobj().throwsException("was not able to verify private template created by franchise Employee user(V2) who does not have privilege to see private template by Franchisee owner user(mu) with location of mu user(V1)");
			}
			
			//Verify Public to all users template created by Franchise user who does not have privilege to see private template by Franchisee mu user(V1L6)
			fc.utobj().printTestStep("Verify Public to all users of my franchise (Status Driven)template created by franchise Employee user(V2) who does not have privilege to see private template by Franchisee owner user(mu) with location of mu user(V1)");
			campaignCenterPage.searchTemplateByFilter(driver, firsttemplateNamefranmue2status);

			boolean isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchisemue2statusV1L6 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firsttemplateNamefranmue2status + "')]");
			if (!isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchisemue2statusV1L6 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my franchise (Status Driven)template created by franchise Employee user(V2) who does not have privilege to see private template by Franchisee owner user(mu) with location of mu user(V1)");
			}
			
			//Verify Private template created by franchise user who does not have privilege to see private template by Franchisee mu user(V1L6)
			fc.utobj().printTestStep("Verify Private (Status Driven)template created by franchise Employee user(V2) who does not have privilege to see private template by Franchisee owner user(mu) with location of mu user(V1)");
			campaignCenterPage.searchTemplateByFilter(driver, secondtemplateNamefranmue2status);
			
			boolean isTemplateNamePresentfranaccessibilityprivatemue2statusV1L6 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondtemplateNamefranmue2status + "')]");
			if (!isTemplateNamePresentfranaccessibilityprivatemue2statusV1L6 == false) {
				fc.utobj().throwsException("was not able to verify private (Status Driven)template created by franchise Employee user(V2) who does not have privilege to see private template by Franchisee owner user(mu) with location of mu user(V1)");
			}
			
			
			
			fc.utobj().printTestStep("Navigate to CRM>Campaign Center>campaign template");
			emailtemplate.clickoncampaigntemplate(driver);
			
			fc.utobj().printTestStep("Verifying Campaigns and templates of Franchise mu owner user (V1) by itself");
			
			fc.utobj().printTestStep("Verify Public to all users of my Franchise Campaign created by Franchise MU user(V1) who does not have privilege to see private campaign by itself");
			campaignCenterPage.searchCampaignByFilter(driver, firstcampaignNamefranmu);

			boolean isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchisemuV1L6 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firstcampaignNamefranmu + "')]");
			if (isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchisemuV1L6 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my Franchise Campaign created by Franchise MU user(V1) who does not have privilege to see private campaign by itself");
			}
			
			//Verify Private Campaign created by Franchise user who does not have privilege to see private campaign by Franchisee mu user(V1L6)
			fc.utobj().printTestStep("Verify Private Campaign created by Franchise MU user(V1) who does not have privilege to see private campaign by itself");
			campaignCenterPage.searchCampaignByFilter(driver, secondcampaignNamefranmu);
			
			boolean isCampaignNamePresentFranpaccessibilityprivatemuV1L6 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondcampaignNamefranmu + "')]");
			if (isCampaignNamePresentFranpaccessibilityprivatemuV1L6 == false) {
				fc.utobj().throwsException("was not able to verify Private Campaign created by Franchise MU user(V1) who does not have privilege to see private campaign by itself");
			}
			
			
			//Verify Public to all users of my Franchise (Status Driven)Campaign created by Franchise user who does not have privilege to see private campaign by Franchisee mu user(V1L6)
			fc.utobj().printTestStep("Verify Public to all users of my Franchise (Status Driven)Campaign created by Franchise MU user(V1) who does not have privilege to see private campaign by itself");
			campaignCenterPage.searchCampaignByFilter(driver, firstcampaignNamefranmustatus);

			boolean isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchisemustatusV1L6 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firstcampaignNamefranmustatus + "')]");
			if (isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchisemustatusV1L6 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my Franchise (Status Driven)Campaign created by Franchise MU user(V1) who does not have privilege to see private campaign by itself");
			}
			
			//Verify Private Campaign created by Franchise user who does not have privilege to see private campaign by Franchisee mu user(V1L6)
			fc.utobj().printTestStep("Verify Private (Status Driven)Campaign created by Franchise MU user(V1) who does not have privilege to see private campaign by itself");
			campaignCenterPage.searchCampaignByFilter(driver, secondcampaignNamefranmustatus);
			
			boolean isCampaignNamePresentFranpaccessibilityprivatemustatusV1L6 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondcampaignNamefranmustatus + "')]");
			if (isCampaignNamePresentFranpaccessibilityprivatemustatusV1L6 == false) {
				fc.utobj().throwsException("was not able to verify Private (Status Driven)Campaign created by Franchise MU user(V1) who does not have privilege to see private campaign by itself");
			}
			
			fc.utobj().printTestStep("Navigate to CRM>Campaign Center>email template");
			emailtemplate.clickonemailtamplate(driver);
			fc.utobj().clickElement(driver, crmcenter.locationtemplate);
			//Verify Public to all users template created by Corporate user who does not have privilege to see private template by Franchisee mu user(V1L6)
			fc.utobj().printTestStep("Verify Public to all users of my franchise template created by franchise MU user(V1) who does not have privilege to see private template by itself");
			campaignCenterPage.searchTemplateByFilter(driver, firsttemplateNamefranmu);

			boolean isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchisemuV1L6 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firsttemplateNamefranmu + "')]");
			if (isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchisemuV1L6 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my franchise template created by franchise MU user(V1) who does not have privilege to see private template by itself");
			}
			
			//Verify Private template created by franchise user who does not have privilege to see private template by Franchisee mu user(V1L6)
			fc.utobj().printTestStep("Verify Private template created by franchise MU user(V1) who does not have privilege to see private template by itself");
			campaignCenterPage.searchTemplateByFilter(driver, secondtemplateNamefranmu);
			
			boolean isTemplateNamePresentfranaccessibilityprivatemuV1L6 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondtemplateNamefranmu + "')]");
			if (isTemplateNamePresentfranaccessibilityprivatemuV1L6 == false) {
				fc.utobj().throwsException("was not able to verify private template created by franchise MU user(V1) who does not have privilege to see private template by itself");
			}
			
			//Verify Public to all users template created by Corporate user who does not have privilege to see private template by Franchisee mu user(V1L6)
			fc.utobj().printTestStep("Verify Public to all users of my franchise (Status Driven)template created by franchise MU user(V1) who does not have privilege to see private template by itself");
			campaignCenterPage.searchTemplateByFilter(driver, firsttemplateNamefranmustatus);

			boolean isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchisemustatusV1L6 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firsttemplateNamefranmustatus + "')]");
			if (isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchisemustatusV1L6 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my franchise (Status Driven)template created by franchise MU user(V1) who does not have privilege to see private template by itself");
			}
			
			//Verify Private template created by franchise user who does not have privilege to see private template by Franchisee mu user(V1L6)
			fc.utobj().printTestStep("Verify Private (Status Driven)template created by franchise MU user(V1) who does not have privilege to see private template by itself");
			campaignCenterPage.searchTemplateByFilter(driver, secondtemplateNamefranmustatus);
			
			boolean isTemplateNamePresentfranaccessibilityprivatemustatusV1L6 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondtemplateNamefranmustatus + "')]");
			if (isTemplateNamePresentfranaccessibilityprivatemustatusV1L6 == false) {
				fc.utobj().throwsException("was not able to verify private (Status Driven)template created by franchise MU user(V1) who does not have privilege to see private template by itself");
			}
			
			
			if(!isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchiseV1L6 && !isCampaignNamePresentFranpaccessibilityprivateV1L6 
					&& !isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchisestatusV1L6 && !isCampaignNamePresentFranpaccessibilityprivatestatusV1L6 && !isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchiseV1L6 
					&& !isTemplateNamePresentfranaccessibilityprivateV1L6 && !isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchisestatusV1L6 && !isTemplateNamePresentfranaccessibilityprivatestatusV1L6  
					&& !isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchisemue2V1L6 
					&& !isCampaignNamePresentFranpaccessibilityprivatemue2V1L6 && !isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchisemue2statusV1L6 && !isCampaignNamePresentFranpaccessibilityprivatemue2statusV1L6 
					&& !isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchisemue2V1L6 && !isTemplateNamePresentfranaccessibilityprivatemue2V1L6 && !isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchisemue2statusV1L6
					&& !isTemplateNamePresentfranaccessibilityprivatemue2statusV1L6 && isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchisemuV1L6 && isCampaignNamePresentFranpaccessibilityprivatemuV1L6 
					&& isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchisemustatusV1L6 && isCampaignNamePresentFranpaccessibilityprivatemustatusV1L6 && isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchisemuV1L6 
					&& isTemplateNamePresentfranaccessibilityprivatemuV1L6 && isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchisemustatusV1L6 && isTemplateNamePresentfranaccessibilityprivatemustatusV1L6) {
				fc.utobj().printTestStep("able to verify all types of campaigns and templates created by mu/employee and owner users when loging with Franchisee owner user(mu)(V1L6) who does not have priviledge to see private campaign and when selected location as mu user location(L6)");
			}
			else {
				fc.utobj().throwsException("was not able to verify all types of campaigns and templates created by mu/employee and owner users when loging with Franchisee owner user(mu)(V1L6) who does not have priviledge to see private campaign and when selected location as mu user location(L6)");
			}
			
			//We have to write code Location wise
			//Let us assumes that now he clicks on L7(V2(employee user) & FONew(owner user)) then it will show data of employee and owner user. Note that it will show up data of mu user also because it is a mu user 
			fc.utobj().printTestStep("Let us assumes that now he clicks on L7(V2(employee user) & FONew(owner user)) then it will show data of employee and owner user. Note that it will show up data of mu user also because it is a mu user");
			fc.utobj().clickElement(driver,"//*[@id='franchisee_no']/span/a");
			fc.utobj().clickElement(driver, "//div[@class='scroller-content']/*[contains(text(),'"+franchiseId6+"')]");
			
			fc.crm().crm_common().openCRMCampaignCenterPage(fcHomePageTest, driver);
			fc.utobj().printTestStep("Click on the menu option and select campaign");
			fc.utobj().printTestStep("Navigate to CRM>Campaign Center>campaign template");
			emailtemplate.clickoncampaigntemplate(driver);
			
			fc.utobj().printTestStep("Verifying Campaigns and templates of Franchise owner user (F0New) by Franchisee owner user(mu) with location of mu user(V1)(V1L7)");
			fc.utobj().printTestStep("Verify Public to all users of my Franchise Campaign created by Franchise user who does not have privilege to see private campaign by Franchisee owner user(mu) with location of mu user(V1)");
			campaignCenterPage.searchCampaignByFilter(driver, firstcampaignNamefran);

			boolean isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchiseV1L7 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firstcampaignNamefran + "')]");
			if (isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchiseV1L7 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my Franchise Campaign created by Franchise user who does not have privilege to see private campaign by Franchisee owner user(mu) with location of mu user(V1)");
			}
			
			//Verify Private Campaign created by Franchise user who does not have privilege to see private campaign by Franchisee mu user(V1L7)
			fc.utobj().printTestStep("Verify Private Campaign created by Franchise user who does not have privilege to see private campaign by Franchisee owner user(mu) with location of mu user(V1)");
			campaignCenterPage.searchCampaignByFilter(driver, secondcampaignNamefran);
			
			boolean isCampaignNamePresentFranpaccessibilityprivateV1L7 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondcampaignNamefran + "')]");
			if (!isCampaignNamePresentFranpaccessibilityprivateV1L7 == false) {
				fc.utobj().throwsException("was not able to verify Private Campaign created by Franchise user who does not have privilege to see private campaign by Franchisee owner user(mu) with location of mu user(V1)");
			}
			
			
			//Verify Public to all users of my Franchise (Status Driven)Campaign created by Franchise user who does not have privilege to see private campaign by Franchisee mu user(V1L7)
			fc.utobj().printTestStep("Verify Public to all users of my Franchise (Status Driven)Campaign created by Franchise user who does not have privilege to see private campaign by Franchisee owner user(mu) with location of mu user(V1)");
			campaignCenterPage.searchCampaignByFilter(driver, firstcampaignNamefranstatus);

			boolean isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchisestatusV1L7 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firstcampaignNamefranstatus + "')]");
			if (isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchisestatusV1L7 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my Franchise (Status Driven)Campaign created by Franchise user who does not have privilege to see private campaign by Franchisee owner user(mu) with location of mu user(V1)");
			}
			
			//Verify Private Campaign created by Franchise user who does not have privilege to see private campaign by Franchisee mu user(V1L7)
			fc.utobj().printTestStep("Verify Private (Status Driven)Campaign created by Franchise user who does not have privilege to see private campaign by Franchisee owner user(mu) with location of mu user(V1)");
			campaignCenterPage.searchCampaignByFilter(driver, secondcampaignNamefranstatus);
			
			boolean isCampaignNamePresentFranpaccessibilityprivatestatusV1L7 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondcampaignNamefranstatus + "')]");
			if (!isCampaignNamePresentFranpaccessibilityprivatestatusV1L7 == false) {
				fc.utobj().throwsException("was not able to verify Private (Status Driven)Campaign created by Franchise user who does not have privilege to see private campaign by Franchisee owner user(mu) with location of mu user(V1)");
			}
			
			fc.utobj().printTestStep("Navigate to CRM>Campaign Center>email template");
			emailtemplate.clickonemailtamplate(driver);
			fc.utobj().clickElement(driver, crmcenter.locationtemplate);
			//Verify Public to all users template created by Corporate user who does not have privilege to see private template by Franchisee mu user(V1L7)
			fc.utobj().printTestStep("Verify Public to all users of my franchise template created by franchise user who does not have privilege to see private template by Franchisee owner user(mu) with location of mu user(V1)");
			campaignCenterPage.searchTemplateByFilter(driver, firsttemplateNamefran);

			boolean isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchiseV1L7 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firsttemplateNamefran + "')]");
			if (isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchiseV1L7 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my franchise template created by franchise user who does not have privilege to see private template by Franchisee owner user(mu) with location of mu user(V1)");
			}
			
			//Verify Private template created by franchise user who does not have privilege to see private template by Franchisee mu user(V1L7)
			fc.utobj().printTestStep("Verify Private template created by franchise user who does not have privilege to see private template by Franchisee owner user(mu) with location of mu user(V1)");
			campaignCenterPage.searchTemplateByFilter(driver, secondtemplateNamefran);
			
			boolean isTemplateNamePresentfranaccessibilityprivateV1L7 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondtemplateNamefran + "')]");
			if (!isTemplateNamePresentfranaccessibilityprivateV1L7 == false) {
				fc.utobj().throwsException("was not able to verify private template created by franchise user who does not have privilege to see private template by Franchisee owner user(mu) with location of mu user(V1)");
			}
			
			//Verify Public to all users (Status Driven)template created by Corporate user who does not have privilege to see private template by Franchisee mu user(V1L7)
			fc.utobj().printTestStep("Verify Public to all users of my franchise (Status Driven)template created by franchise user who does not have privilege to see private template by Franchisee owner user(mu) with location of mu user(V1)");
			campaignCenterPage.searchTemplateByFilter(driver, firsttemplateNamefranstatus);

			boolean isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchisestatusV1L7 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firsttemplateNamefranstatus + "')]");
			if (isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchisestatusV1L7 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my franchise (Status Driven)template created by franchise user who does not have privilege to see private template by Franchisee owner user(mu) with location of mu user(V1)");
			}
			
			//Verify Private template created by franchise user who does not have privilege to see private template by Franchisee mu user(V1L7)
			fc.utobj().printTestStep("Verify Private (Status Driven)template created by franchise user who does not have privilege to see private template by Franchisee owner user(mu) with location of mu user(V1)");
			campaignCenterPage.searchTemplateByFilter(driver, secondtemplateNamefranstatus);
			
			boolean isTemplateNamePresentfranaccessibilityprivatestatusV1L7 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondtemplateNamefranstatus + "')]");
			if (!isTemplateNamePresentfranaccessibilityprivatestatusV1L7 == false) {
				fc.utobj().throwsException("was not able to verify private (Status Driven)template created by franchise user who does not have privilege to see private template by Franchisee owner user(mu) with location of mu user(V1)");
			}
			
			
			fc.utobj().printTestStep("Navigate to CRM>Campaign Center>campaign template");
			emailtemplate.clickoncampaigntemplate(driver);
			fc.utobj().printTestStep("Verifying Campaigns and templates of Franchise Employee user (V2) by Franchisee owner user(mu) with location of mu user(V1)");
			fc.utobj().printTestStep("Verify Public to all users of my Franchise Campaign created by Franchise Employee user(V2) who does not have privilege to see private campaign by Franchisee owner user(mu) with location of mu user(V1)");
			campaignCenterPage.searchCampaignByFilter(driver, firstcampaignNamefranmue2);

			boolean isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchisemue2V1L7 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firstcampaignNamefranmue2 + "')]");
			if (isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchisemue2V1L7 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my Franchise Campaign created by Franchise Employee user(V2) who does not have privilege to see private campaign by Franchisee owner user(mu) with location of mu user(V1)");
			}
			
			//Verify Private Campaign created by Franchise user who does not have privilege to see private campaign by Franchisee mu user(V1L7)
			fc.utobj().printTestStep("Verify Private Campaign created by Franchise Employee user(V2) who does not have privilege to see private campaign by Franchisee owner user(mu) with location of mu user(V1)");
			campaignCenterPage.searchCampaignByFilter(driver, secondcampaignNamefranmue2);
			
			boolean isCampaignNamePresentFranpaccessibilityprivatemue2V1L7 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondcampaignNamefranmue2 + "')]");
			if (!isCampaignNamePresentFranpaccessibilityprivatemue2V1L7 == false) {
				fc.utobj().throwsException("was not able to verify Private Campaign created by Franchise Employee user(V2) who does not have privilege to see private campaign by Franchisee owner user(mu) with location of mu user(V1)");
			}
			
			
			//Verify Public to all users of my Franchise Campaign created by Franchise Employee user(V2) who does not have privilege to see private campaign by Franchisee mu user(V1L7)
			fc.utobj().printTestStep("Verify Public to all users of my Franchise (Status Driven)Campaign created by Franchise Employee user(V2) who does not have privilege to see private campaign by Franchisee owner user(mu) with location of mu user(V1)");
			campaignCenterPage.searchCampaignByFilter(driver, firstcampaignNamefranmue2status);

			boolean isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchisemue2statusV1L7 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firstcampaignNamefranmue2status + "')]");
			if (isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchisemue2statusV1L7 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my Franchise (Status Driven)Campaign created by Franchise Employee user(V2) who does not have privilege to see private campaign by Franchisee owner user(mu) with location of mu user(V1)");
			}
			
			//Verify Private Campaign created by Franchise user who does not have privilege to see private campaign by Franchisee mu user(V1L7)
			fc.utobj().printTestStep("Verify Private (Status Driven)Campaign created by Franchise Employee user(V2) who does not have privilege to see private campaign by Franchisee owner user(mu) with location of mu user(V1)");
			campaignCenterPage.searchCampaignByFilter(driver, secondcampaignNamefranmue2status);
			
			boolean isCampaignNamePresentFranpaccessibilityprivatemue2statusV1L7 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondcampaignNamefranmue2status + "')]");
			if (!isCampaignNamePresentFranpaccessibilityprivatemue2statusV1L7 == false) {
				fc.utobj().throwsException("was not able to verify Private (Status Driven)Campaign created by Franchise Employee user(V2) who does not have privilege to see private campaign by Franchisee owner user(mu) with location of mu user(V1)");
			}
			
			fc.utobj().printTestStep("Navigate to CRM>Campaign Center>email template");
			emailtemplate.clickonemailtamplate(driver);
			fc.utobj().clickElement(driver, crmcenter.locationtemplate);
			//Verify Public to all users template created by Franchise user who does not have privilege to see private template by Franchisee mu user(V1L7)
			fc.utobj().printTestStep("Verify Public to all users of my franchise template created by franchise Employee user(V2) who does not have privilege to see private template by Franchisee owner user(mu) with location of mu user(V1)");
			campaignCenterPage.searchTemplateByFilter(driver, firsttemplateNamefranmue2);

			boolean isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchisemue2V1L7 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firsttemplateNamefranmue2 + "')]");
			if (isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchisemue2V1L7 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my franchise template created by franchise Employee user(V2) who does not have privilege to see private template by Franchisee owner user(mu) with location of mu user(V1)");
			}
			
			//Verify Private template created by franchise user who does not have privilege to see private template by Franchisee mu user(V1L7)
			fc.utobj().printTestStep("Verify Private template created by franchise Employee user(V2) who does not have privilege to see private template by Franchisee owner user(mu) with location of mu user(V1)");
			campaignCenterPage.searchTemplateByFilter(driver, secondtemplateNamefranmue2);
			
			boolean isTemplateNamePresentfranaccessibilityprivatemue2V1L7 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondtemplateNamefranmue2 + "')]");
			if (!isTemplateNamePresentfranaccessibilityprivatemue2V1L7 == false) {
				fc.utobj().throwsException("was not able to verify private template created by franchise Employee user(V2) who does not have privilege to see private template by Franchisee owner user(mu) with location of mu user(V1)");
			}
			
			//Verify Public to all users template created by Franchise user who does not have privilege to see private template by Franchisee mu user(V1L7)
			fc.utobj().printTestStep("Verify Public to all users of my franchise (Status Driven)template created by franchise Employee user(V2) who does not have privilege to see private template by Franchisee owner user(mu) with location of mu user(V1)");
			campaignCenterPage.searchTemplateByFilter(driver, firsttemplateNamefranmue2status);

			boolean isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchisemue2statusV1L7 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firsttemplateNamefranmue2status + "')]");
			if (isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchisemue2statusV1L7 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my franchise (Status Driven)template created by franchise Employee user(V2) who does not have privilege to see private template by Franchisee owner user(mu) with location of mu user(V1)");
			}
			
			//Verify Private template created by franchise user who does not have privilege to see private template by Franchisee mu user(V1L7)
			fc.utobj().printTestStep("Verify Private (Status Driven)template created by franchise Employee user(V2) who does not have privilege to see private template by Franchisee owner user(mu) with location of mu user(V1)");
			campaignCenterPage.searchTemplateByFilter(driver, secondtemplateNamefranmue2status);
			
			boolean isTemplateNamePresentfranaccessibilityprivatemue2statusV1L7 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondtemplateNamefranmue2status + "')]");
			if (!isTemplateNamePresentfranaccessibilityprivatemue2statusV1L7 == false) {
				fc.utobj().throwsException("was not able to verify private (Status Driven)template created by franchise Employee user(V2) who does not have privilege to see private template by Franchisee owner user(mu) with location of mu user(V1)");
			}
			
			
			
			fc.utobj().printTestStep("Navigate to CRM>Campaign Center>campaign template");
			emailtemplate.clickoncampaigntemplate(driver);
			
			fc.utobj().printTestStep("Verifying Campaigns and templates of Franchise mu owner user (V1) by itself");
			
			fc.utobj().printTestStep("Verify Public to all users of my Franchise Campaign created by Franchise MU user(V1) who does not have privilege to see private campaign by itself");
			campaignCenterPage.searchCampaignByFilter(driver, firstcampaignNamefranmu);

			boolean isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchisemuV1L7 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firstcampaignNamefranmu + "')]");
			if (isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchisemuV1L7 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my Franchise Campaign created by Franchise MU user(V1) who does not have privilege to see private campaign by itself");
			}
			
			//Verify Private Campaign created by Franchise user who does not have privilege to see private campaign by Franchisee mu user(V1L7)
			fc.utobj().printTestStep("Verify Private Campaign created by Franchise MU user(V1) who does not have privilege to see private campaign by itself");
			campaignCenterPage.searchCampaignByFilter(driver, secondcampaignNamefranmu);
			
			boolean isCampaignNamePresentFranpaccessibilityprivatemuV1L7 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondcampaignNamefranmu + "')]");
			if (isCampaignNamePresentFranpaccessibilityprivatemuV1L7 == false) {
				fc.utobj().throwsException("was not able to verify Private Campaign created by Franchise MU user(V1) who does not have privilege to see private campaign by itself");
			}
			
			
			//Verify Public to all users of my Franchise (Status Driven)Campaign created by Franchise user who does not have privilege to see private campaign by Franchisee mu user(V1L7)
			fc.utobj().printTestStep("Verify Public to all users of my Franchise (Status Driven)Campaign created by Franchise MU user(V1) who does not have privilege to see private campaign by itself");
			campaignCenterPage.searchCampaignByFilter(driver, firstcampaignNamefranmustatus);

			boolean isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchisemustatusV1L7 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firstcampaignNamefranmustatus + "')]");
			if (isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchisemustatusV1L7 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my Franchise (Status Driven)Campaign created by Franchise MU user(V1) who does not have privilege to see private campaign by itself");
			}
			
			//Verify Private Campaign created by Franchise user who does not have privilege to see private campaign by Franchisee mu user(V1L7)
			fc.utobj().printTestStep("Verify Private (Status Driven)Campaign created by Franchise MU user(V1) who does not have privilege to see private campaign by itself");
			campaignCenterPage.searchCampaignByFilter(driver, secondcampaignNamefranmustatus);
			
			boolean isCampaignNamePresentFranpaccessibilityprivatemustatusV1L7 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondcampaignNamefranmustatus + "')]");
			if (isCampaignNamePresentFranpaccessibilityprivatemustatusV1L7 == false) {
				fc.utobj().throwsException("was not able to verify Private (Status Driven)Campaign created by Franchise MU user(V1) who does not have privilege to see private campaign by itself");
			}
			
			fc.utobj().printTestStep("Navigate to CRM>Campaign Center>email template");
			emailtemplate.clickonemailtamplate(driver);
			fc.utobj().clickElement(driver, crmcenter.locationtemplate);
			//Verify Public to all users template created by Corporate user who does not have privilege to see private template by Franchisee mu user(V1L7)
			fc.utobj().printTestStep("Verify Public to all users of my franchise template created by franchise MU user(V1) who does not have privilege to see private template by itself");
			campaignCenterPage.searchTemplateByFilter(driver, firsttemplateNamefranmu);

			boolean isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchisemuV1L7 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firsttemplateNamefranmu + "')]");
			if (isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchisemuV1L7 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my franchise template created by franchise MU user(V1) who does not have privilege to see private template by itself");
			}
			
			//Verify Private template created by franchise user who does not have privilege to see private template by Franchisee mu user(V1L7)
			fc.utobj().printTestStep("Verify Private template created by franchise MU user(V1) who does not have privilege to see private template by itself");
			campaignCenterPage.searchTemplateByFilter(driver, secondtemplateNamefranmu);
			
			boolean isTemplateNamePresentfranaccessibilityprivatemuV1L7 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondtemplateNamefranmu + "')]");
			if (isTemplateNamePresentfranaccessibilityprivatemuV1L7 == false) {
				fc.utobj().throwsException("was not able to verify private template created by franchise MU user(V1) who does not have privilege to see private template by itself");
			}
			
			//Verify Public to all users template created by Corporate user who does not have privilege to see private template by Franchisee mu user(V1L7)
			fc.utobj().printTestStep("Verify Public to all users of my franchise (Status Driven)template created by franchise MU user(V1) who does not have privilege to see private template by itself");
			campaignCenterPage.searchTemplateByFilter(driver, firsttemplateNamefranmustatus);

			boolean isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchisemustatusV1L7 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firsttemplateNamefranmustatus + "')]");
			if (isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchisemustatusV1L7 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my franchise (Status Driven)template created by franchise MU user(V1) who does not have privilege to see private template by itself");
			}
			
			//Verify Private template created by franchise user who does not have privilege to see private template by Franchisee mu user(V1L7)
			fc.utobj().printTestStep("Verify Private (Status Driven)template created by franchise MU user(V1) who does not have privilege to see private template by itself");
			campaignCenterPage.searchTemplateByFilter(driver, secondtemplateNamefranmustatus);
			
			boolean isTemplateNamePresentfranaccessibilityprivatemustatusV1L7 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondtemplateNamefranmustatus + "')]");
			if (isTemplateNamePresentfranaccessibilityprivatemustatusV1L7 == false) {
				fc.utobj().throwsException("was not able to verify private (Status Driven)template created by franchise MU user(V1) who does not have privilege to see private template by itself");
			}
			
			
			if(isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchiseV1L7 && !isCampaignNamePresentFranpaccessibilityprivateV1L7 
					&& isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchisestatusV1L7 && !isCampaignNamePresentFranpaccessibilityprivatestatusV1L7 && isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchiseV1L7 
					&& !isTemplateNamePresentfranaccessibilityprivateV1L7 && isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchisestatusV1L7 && !isTemplateNamePresentfranaccessibilityprivatestatusV1L7  
					&& isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchisemue2V1L7 
					&& !isCampaignNamePresentFranpaccessibilityprivatemue2V1L7 && isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchisemue2statusV1L7 && !isCampaignNamePresentFranpaccessibilityprivatemue2statusV1L7 
					&& isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchisemue2V1L7 && !isTemplateNamePresentfranaccessibilityprivatemue2V1L7 && isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchisemue2statusV1L7
					&& !isTemplateNamePresentfranaccessibilityprivatemue2statusV1L7 && isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchisemuV1L7 && isCampaignNamePresentFranpaccessibilityprivatemuV1L7 
					&& isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchisemustatusV1L7 && isCampaignNamePresentFranpaccessibilityprivatemustatusV1L7 && isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchisemuV1L7 
					&& isTemplateNamePresentfranaccessibilityprivatemuV1L7 && isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchisemustatusV1L7 && isTemplateNamePresentfranaccessibilityprivatemustatusV1L7) {
				fc.utobj().printTestStep("able to verify all types of campaigns and templates created by mu/employee and owner users when loging with Franchisee owner user(mu)(V1L7) who does not have priviledge to see private campaign and when selected location as Employee user location(L7)");
			}
			else {
				fc.utobj().throwsException("was not able to verify all types of campaigns and templates created by mu/employee and owner users when loging with Franchisee owner user(mu)(V1L7) who does not have priviledge to see private campaign and when selected location as Employee user location(L7)");
			}
	
			//Verification with all users
			//Login with Franchisee mu owner user(V1) who does not have role to see private campaign
			fc.utobj().printTestStep("Login with Franchisee(mu) owner user (V1) who does not have role to see private campaign");
			fc.loginpage().loginWithParameter(driver, userNameFOmu, passwordFOmu);
			fc.crm().crm_common().openCRMCampaignCenterPage(fcHomePageTest, driver);
			fc.utobj().printTestStep("Click on the menu option and select campaign");
			fc.utobj().printTestStep("Navigate to CRM>Campaign Center>campaign template");
			emailtemplate.clickoncampaigntemplate(driver);
			
			fc.utobj().printTestStep("Verifying Campaigns and templates of Franchise owner user (F0New) by Franchisee owner user(mu)(V1)");
			fc.utobj().printTestStep("Verify Public to all users of my Franchise Campaign created by Franchise user who does not have privilege to see private campaign by Franchisee owner user(mu)");
			campaignCenterPage.searchCampaignByFilter(driver, firstcampaignNamefran);

			boolean isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchiseV1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firstcampaignNamefran + "')]");
			if (isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchiseV1 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my Franchise Campaign created by Franchise user who does not have privilege to see private campaign by Franchisee owner user(mu)");
			}
			
			//Verify Private Campaign created by Franchise user who does not have privilege to see private campaign by Franchisee mu user(V1)
			fc.utobj().printTestStep("Verify Private Campaign created by Franchise user who does not have privilege to see private campaign by Franchisee owner user(mu)");
			campaignCenterPage.searchCampaignByFilter(driver, secondcampaignNamefran);
			
			boolean isCampaignNamePresentFranpaccessibilityprivateV1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondcampaignNamefran + "')]");
			if (!isCampaignNamePresentFranpaccessibilityprivateV1 == false) {
				fc.utobj().throwsException("was not able to verify Private Campaign created by Franchise user who does not have privilege to see private campaign by Franchisee owner user(mu)");
			}
			
			
			//Verify Public to all users of my Franchise (Status Driven)Campaign created by Franchise user who does not have privilege to see private campaign by Franchisee mu user(V1)
			fc.utobj().printTestStep("Verify Public to all users of my Franchise (Status Driven)Campaign created by Franchise user who does not have privilege to see private campaign by Franchisee owner user(mu)");
			campaignCenterPage.searchCampaignByFilter(driver, firstcampaignNamefranstatus);

			boolean isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchisestatusV1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firstcampaignNamefranstatus + "')]");
			if (isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchisestatusV1 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my Franchise (Status Driven)Campaign created by Franchise user who does not have privilege to see private campaign by Franchisee owner user(mu)");
			}
			
			//Verify Private Campaign created by Franchise user who does not have privilege to see private campaign by Franchisee mu user(V1)
			fc.utobj().printTestStep("Verify Private (Status Driven)Campaign created by Franchise user who does not have privilege to see private campaign by Franchisee owner user(mu)");
			campaignCenterPage.searchCampaignByFilter(driver, secondcampaignNamefranstatus);
			
			boolean isCampaignNamePresentFranpaccessibilityprivatestatusV1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondcampaignNamefranstatus + "')]");
			if (!isCampaignNamePresentFranpaccessibilityprivatestatusV1 == false) {
				fc.utobj().throwsException("was not able to verify Private (Status Driven)Campaign created by Franchise user who does not have privilege to see private campaign by Franchisee owner user(mu)");
			}
			
			fc.utobj().printTestStep("Navigate to CRM>Campaign Center>email template");
			emailtemplate.clickonemailtamplate(driver);
			fc.utobj().clickElement(driver, crmcenter.locationtemplate);
			//Verify Public to all users template created by Corporate user who does not have privilege to see private template by Franchisee mu user(V1)
			fc.utobj().printTestStep("Verify Public to all users of my franchise template created by franchise user who does not have privilege to see private template by Franchisee owner user(mu)");
			campaignCenterPage.searchTemplateByFilter(driver, firsttemplateNamefran);

			boolean isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchiseV1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firsttemplateNamefran + "')]");
			if (isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchiseV1 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my franchise template created by franchise user who does not have privilege to see private template by Franchisee owner user(mu)");
			}
			
			//Verify Private template created by franchise user who does not have privilege to see private template by Franchisee mu user(V1)
			fc.utobj().printTestStep("Verify Private template created by franchise user who does not have privilege to see private template by Franchisee owner user(mu)");
			campaignCenterPage.searchTemplateByFilter(driver, secondtemplateNamefran);
			
			boolean isTemplateNamePresentfranaccessibilityprivateV1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondtemplateNamefran + "')]");
			if (!isTemplateNamePresentfranaccessibilityprivateV1 == false) {
				fc.utobj().throwsException("was not able to verify private template created by franchise user who does not have privilege to see private template by Franchisee owner user(mu)");
			}
			
			//Verify Public to all users (Status Driven)template created by Corporate user who does not have privilege to see private template by Franchisee mu user(V1)
			fc.utobj().printTestStep("Verify Public to all users of my franchise (Status Driven)template created by franchise user who does not have privilege to see private template by Franchisee owner user(mu)");
			campaignCenterPage.searchTemplateByFilter(driver, firsttemplateNamefranstatus);

			boolean isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchisestatusV1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firsttemplateNamefranstatus + "')]");
			if (isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchisestatusV1 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my franchise (Status Driven)template created by franchise user who does not have privilege to see private template by Franchisee owner user(mu)");
			}
			
			//Verify Private template created by franchise user who does not have privilege to see private template by Franchisee mu user(V1)
			fc.utobj().printTestStep("Verify Private (Status Driven)template created by franchise user who does not have privilege to see private template by Franchisee owner user(mu)");
			campaignCenterPage.searchTemplateByFilter(driver, secondtemplateNamefranstatus);
			
			boolean isTemplateNamePresentfranaccessibilityprivatestatusV1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondtemplateNamefranstatus + "')]");
			if (!isTemplateNamePresentfranaccessibilityprivatestatusV1 == false) {
				fc.utobj().throwsException("was not able to verify private (Status Driven)template created by franchise user who does not have privilege to see private template by Franchisee owner user(mu)");
			}
			
			
			fc.utobj().printTestStep("Navigate to CRM>Campaign Center>campaign template");
			emailtemplate.clickoncampaigntemplate(driver);
			fc.utobj().printTestStep("Verifying Campaigns and templates of Franchise Employee user (V2) by Franchisee owner user(mu)");
			fc.utobj().printTestStep("Verify Public to all users of my Franchise Campaign created by Franchise Employee user(V2) who does not have privilege to see private campaign by Franchisee owner user(mu)");
			campaignCenterPage.searchCampaignByFilter(driver, firstcampaignNamefranmue2);

			boolean isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchisemue2V1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firstcampaignNamefranmue2 + "')]");
			if (isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchisemue2V1 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my Franchise Campaign created by Franchise Employee user(V2) who does not have privilege to see private campaign by Franchisee owner user(mu)");
			}
			
			//Verify Private Campaign created by Franchise user who does not have privilege to see private campaign by Franchisee mu user(V1)
			fc.utobj().printTestStep("Verify Private Campaign created by Franchise Employee user(V2) who does not have privilege to see private campaign by Franchisee owner user(mu)");
			campaignCenterPage.searchCampaignByFilter(driver, secondcampaignNamefranmue2);
			
			boolean isCampaignNamePresentFranpaccessibilityprivatemue2V1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondcampaignNamefranmue2 + "')]");
			if (!isCampaignNamePresentFranpaccessibilityprivatemue2V1 == false) {
				fc.utobj().throwsException("was not able to verify Private Campaign created by Franchise Employee user(V2) who does not have privilege to see private campaign by Franchisee owner user(mu)");
			}
			
			
			//Verify Public to all users of my Franchise Campaign created by Franchise Employee user(V2) who does not have privilege to see private campaign by Franchisee mu user(V1)
			fc.utobj().printTestStep("Verify Public to all users of my Franchise (Status Driven)Campaign created by Franchise Employee user(V2) who does not have privilege to see private campaign by Franchisee owner user(mu)");
			campaignCenterPage.searchCampaignByFilter(driver, firstcampaignNamefranmue2status);

			boolean isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchisemue2statusV1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firstcampaignNamefranmue2status + "')]");
			if (isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchisemue2statusV1 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my Franchise (Status Driven)Campaign created by Franchise Employee user(V2) who does not have privilege to see private campaign by Franchisee owner user(mu)");
			}
			
			//Verify Private Campaign created by Franchise user who does not have privilege to see private campaign by Franchisee mu user(V1)
			fc.utobj().printTestStep("Verify Private (Status Driven)Campaign created by Franchise Employee user(V2) who does not have privilege to see private campaign by Franchisee owner user(mu)");
			campaignCenterPage.searchCampaignByFilter(driver, secondcampaignNamefranmue2status);
			
			boolean isCampaignNamePresentFranpaccessibilityprivatemue2statusV1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondcampaignNamefranmue2status + "')]");
			if (!isCampaignNamePresentFranpaccessibilityprivatemue2statusV1 == false) {
				fc.utobj().throwsException("was not able to verify Private (Status Driven)Campaign created by Franchise Employee user(V2) who does not have privilege to see private campaign by Franchisee owner user(mu)");
			}
			
			fc.utobj().printTestStep("Navigate to CRM>Campaign Center>email template");
			emailtemplate.clickonemailtamplate(driver);
			fc.utobj().clickElement(driver, crmcenter.locationtemplate);
			//Verify Public to all users template created by Franchise user who does not have privilege to see private template by Franchisee mu user(V1)
			fc.utobj().printTestStep("Verify Public to all users of my franchise template created by franchise Employee user(V2) who does not have privilege to see private template by Franchisee owner user(mu)");
			campaignCenterPage.searchTemplateByFilter(driver, firsttemplateNamefranmue2);

			boolean isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchisemue2V1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firsttemplateNamefranmue2 + "')]");
			if (isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchisemue2V1 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my franchise template created by franchise Employee user(V2) who does not have privilege to see private template by Franchisee owner user(mu)");
			}
			
			//Verify Private template created by franchise user who does not have privilege to see private template by Franchisee mu user(V1)
			fc.utobj().printTestStep("Verify Private template created by franchise Employee user(V2) who does not have privilege to see private template by Franchisee owner user(mu)");
			campaignCenterPage.searchTemplateByFilter(driver, secondtemplateNamefranmue2);
			
			boolean isTemplateNamePresentfranaccessibilityprivatemue2V1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondtemplateNamefranmue2 + "')]");
			if (!isTemplateNamePresentfranaccessibilityprivatemue2V1 == false) {
				fc.utobj().throwsException("was not able to verify private template created by franchise Employee user(V2) who does not have privilege to see private template by Franchisee owner user(mu)");
			}
			
			//Verify Public to all users template created by Franchise user who does not have privilege to see private template by Franchisee mu user(V1)
			fc.utobj().printTestStep("Verify Public to all users of my franchise (Status Driven)template created by franchise Employee user(V2) who does not have privilege to see private template by Franchisee owner user(mu)");
			campaignCenterPage.searchTemplateByFilter(driver, firsttemplateNamefranmue2status);

			boolean isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchisemue2statusV1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firsttemplateNamefranmue2status + "')]");
			if (isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchisemue2statusV1 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my franchise (Status Driven)template created by franchise Employee user(V2) who does not have privilege to see private template by Franchisee owner user(mu)");
			}
			
			//Verify Private template created by franchise user who does not have privilege to see private template by Franchisee mu user(V1)
			fc.utobj().printTestStep("Verify Private (Status Driven)template created by franchise Employee user(V2) who does not have privilege to see private template by Franchisee owner user(mu)");
			campaignCenterPage.searchTemplateByFilter(driver, secondtemplateNamefranmue2status);
			
			boolean isTemplateNamePresentfranaccessibilityprivatemue2statusV1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondtemplateNamefranmue2status + "')]");
			if (!isTemplateNamePresentfranaccessibilityprivatemue2statusV1 == false) {
				fc.utobj().throwsException("was not able to verify private (Status Driven)template created by franchise Employee user(V2) who does not have privilege to see private template by Franchisee owner user(mu)");
			}
			
			
			if(isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchiseV1 && !isCampaignNamePresentFranpaccessibilityprivateV1 
					&& isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchisestatusV1 && !isCampaignNamePresentFranpaccessibilityprivatestatusV1 && isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchiseV1 
					&& !isTemplateNamePresentfranaccessibilityprivateV1 && isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchisestatusV1 && !isTemplateNamePresentfranaccessibilityprivatestatusV1  
					&& isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchisemue2V1 
					&& !isCampaignNamePresentFranpaccessibilityprivatemue2V1 && isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchisemue2statusV1 && !isCampaignNamePresentFranpaccessibilityprivatemue2statusV1 
					&& isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchisemue2V1 && !isTemplateNamePresentfranaccessibilityprivatemue2V1 && isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchisemue2statusV1
					&& !isTemplateNamePresentfranaccessibilityprivatemue2statusV1) {
				fc.utobj().printTestStep("able to verify all types of campaigns and templates created by mu/employee and owner users when loging with Franchisee owner user(mu)(V1) who does not have priviledge to see private campaign");
			}
			else {
				fc.utobj().throwsException("was not able to verify all types of campaigns and templates created by mu/employee and owner users when loging with Franchisee owner user(mu)(V1) who does not have priviledge to see private campaign");
			}
		
			//Login with Franchisee Employee user(V2) who does not have role to see private campaign
			fc.utobj().printTestStep("Login with Franchisee Employee user(V2) who does not have role to see private campaign");
			fc.loginpage().loginWithParameter(driver, userNameFEmu, passwordFEmu);
			fc.crm().crm_common().openCRMCampaignCenterPage(fcHomePageTest, driver);
			fc.utobj().printTestStep("Click on the menu option and select campaign");
			
			fc.utobj().printTestStep("Navigate to CRM>Campaign Center>campaign template");
			emailtemplate.clickoncampaigntemplate(driver);
			
			fc.utobj().printTestStep("Verifying Campaigns and templates of Franchise owner user (F0New) by Franchisee Employee user(V2)(V2)");
			fc.utobj().printTestStep("Verify Public to all users of my Franchise Campaign created by Franchise user who does not have privilege to see private campaign by Franchisee Employee user(V2)");
			campaignCenterPage.searchCampaignByFilter(driver, firstcampaignNamefran);

			boolean isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchiseV2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firstcampaignNamefran + "')]");
			if (isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchiseV2 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my Franchise Campaign created by Franchise user who does not have privilege to see private campaign by Franchisee Employee user(V2)");
			}
			
			//Verify Private Campaign created by Franchise user who does not have privilege to see private campaign by Franchisee mu user(V2)
			fc.utobj().printTestStep("Verify Private Campaign created by Franchise user who does not have privilege to see private campaign by Franchisee Employee user(V2)");
			campaignCenterPage.searchCampaignByFilter(driver, secondcampaignNamefran);
			
			boolean isCampaignNamePresentFranpaccessibilityprivateV2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondcampaignNamefran + "')]");
			if (!isCampaignNamePresentFranpaccessibilityprivateV2 == false) {
				fc.utobj().throwsException("was not able to verify Private Campaign created by Franchise user who does not have privilege to see private campaign by Franchisee Employee user(V2)");
			}
			
			
			//Verify Public to all users of my Franchise (Status Driven)Campaign created by Franchise user who does not have privilege to see private campaign by Franchisee mu user(V2)
			fc.utobj().printTestStep("Verify Public to all users of my Franchise (Status Driven)Campaign created by Franchise user who does not have privilege to see private campaign by Franchisee Employee user(V2)");
			campaignCenterPage.searchCampaignByFilter(driver, firstcampaignNamefranstatus);

			boolean isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchisestatusV2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firstcampaignNamefranstatus + "')]");
			if (isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchisestatusV2 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my Franchise (Status Driven)Campaign created by Franchise user who does not have privilege to see private campaign by Franchisee Employee user(V2)");
			}
			
			//Verify Private Campaign created by Franchise user who does not have privilege to see private campaign by Franchisee mu user(V2)
			fc.utobj().printTestStep("Verify Private (Status Driven)Campaign created by Franchise user who does not have privilege to see private campaign by Franchisee Employee user(V2)");
			campaignCenterPage.searchCampaignByFilter(driver, secondcampaignNamefranstatus);
			
			boolean isCampaignNamePresentFranpaccessibilityprivatestatusV2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondcampaignNamefranstatus + "')]");
			if (!isCampaignNamePresentFranpaccessibilityprivatestatusV2 == false) {
				fc.utobj().throwsException("was not able to verify Private (Status Driven)Campaign created by Franchise user who does not have privilege to see private campaign by Franchisee Employee user(V2)");
			}
			
			fc.utobj().printTestStep("Navigate to CRM>Campaign Center>email template");
			emailtemplate.clickonemailtamplate(driver);
			fc.utobj().clickElement(driver, crmcenter.locationtemplate);
			//Verify Public to all users template created by Corporate user who does not have privilege to see private template by Franchisee mu user(V2)
			fc.utobj().printTestStep("Verify Public to all users of my franchise template created by franchise user who does not have privilege to see private template by Franchisee Employee user(V2)");
			campaignCenterPage.searchTemplateByFilter(driver, firsttemplateNamefran);

			boolean isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchiseV2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firsttemplateNamefran + "')]");
			if (isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchiseV2 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my franchise template created by franchise user who does not have privilege to see private template by Franchisee Employee user(V2)");
			}
			
			//Verify Private template created by franchise user who does not have privilege to see private template by Franchisee mu user(V2)
			fc.utobj().printTestStep("Verify Private template created by franchise user who does not have privilege to see private template by Franchisee Employee user(V2)");
			campaignCenterPage.searchTemplateByFilter(driver, secondtemplateNamefran);
			
			boolean isTemplateNamePresentfranaccessibilityprivateV2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondtemplateNamefran + "')]");
			if (!isTemplateNamePresentfranaccessibilityprivateV2 == false) {
				fc.utobj().throwsException("was not able to verify private template created by franchise user who does not have privilege to see private template by Franchisee Employee user(V2)");
			}
			
			//Verify Public to all users (Status Driven)template created by Corporate user who does not have privilege to see private template by Franchisee mu user(V2)
			fc.utobj().printTestStep("Verify Public to all users of my franchise (Status Driven)template created by franchise user who does not have privilege to see private template by Franchisee Employee user(V2)");
			campaignCenterPage.searchTemplateByFilter(driver, firsttemplateNamefranstatus);

			boolean isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchisestatusV2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firsttemplateNamefranstatus + "')]");
			if (isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchisestatusV2 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my franchise (Status Driven)template created by franchise user who does not have privilege to see private template by Franchisee Employee user(V2)");
			}
			
			//Verify Private template created by franchise user who does not have privilege to see private template by Franchisee mu user(V2)
			fc.utobj().printTestStep("Verify Private (Status Driven)template created by franchise user who does not have privilege to see private template by Franchisee Employee user(V2)");
			campaignCenterPage.searchTemplateByFilter(driver, secondtemplateNamefranstatus);
			
			boolean isTemplateNamePresentfranaccessibilityprivatestatusV2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondtemplateNamefranstatus + "')]");
			if (!isTemplateNamePresentfranaccessibilityprivatestatusV2 == false) {
				fc.utobj().throwsException("was not able to verify private (Status Driven)template created by franchise user who does not have privilege to see private template by Franchisee Employee user(V2)");
			}
			
			
			fc.utobj().printTestStep("Navigate to CRM>Campaign Center>campaign template");
			emailtemplate.clickoncampaigntemplate(driver);
			
			fc.utobj().printTestStep("Verifying Campaigns and templates of Franchise mu owner user (V1) by Franchisee Employee user(V2)");
			
			fc.utobj().printTestStep("Verify Public to all users of my Franchise Campaign created by Franchise MU user(V1) who does not have privilege to see private campaign by Franchisee Employee user(V2)");
			campaignCenterPage.searchCampaignByFilter(driver, firstcampaignNamefranmu);

			boolean isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchisemuV2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firstcampaignNamefranmu + "')]");
			if (isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchisemuV2 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my Franchise Campaign created by Franchise MU user(V1) who does not have privilege to see private campaign by Franchisee Employee user(V2)");
			}
			
			//Verify Private Campaign created by Franchise user who does not have privilege to see private campaign by Franchisee mu user(V2)
			fc.utobj().printTestStep("Verify Private Campaign created by Franchise MU user(V1) who does not have privilege to see private campaign by Franchisee Employee user(V2)");
			campaignCenterPage.searchCampaignByFilter(driver, secondcampaignNamefranmu);
			
			boolean isCampaignNamePresentFranpaccessibilityprivatemuV2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondcampaignNamefranmu + "')]");
			if (!isCampaignNamePresentFranpaccessibilityprivatemuV2 == false) {
				fc.utobj().throwsException("was not able to verify Private Campaign created by Franchise MU user(V1) who does not have privilege to see private campaign by Franchisee Employee user(V2)");
			}
			
			
			//Verify Public to all users of my Franchise (Status Driven)Campaign created by Franchise user who does not have privilege to see private campaign by Franchisee mu user(V2)
			fc.utobj().printTestStep("Verify Public to all users of my Franchise (Status Driven)Campaign created by Franchise MU user(V1) who does not have privilege to see private campaign by Franchisee Employee user(V2)");
			campaignCenterPage.searchCampaignByFilter(driver, firstcampaignNamefranmustatus);

			boolean isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchisemustatusV2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firstcampaignNamefranmustatus + "')]");
			if (isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchisemustatusV2 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my Franchise (Status Driven)Campaign created by Franchise MU user(V1) who does not have privilege to see private campaign by Franchisee Employee user(V2)");
			}
			
			//Verify Private Campaign created by Franchise user who does not have privilege to see private campaign by Franchisee mu user(V2)
			fc.utobj().printTestStep("Verify Private (Status Driven)Campaign created by Franchise MU user(V1) who does not have privilege to see private campaign by Franchisee Employee user(V2)");
			campaignCenterPage.searchCampaignByFilter(driver, secondcampaignNamefranmustatus);
			
			boolean isCampaignNamePresentFranpaccessibilityprivatemustatusV2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondcampaignNamefranmustatus + "')]");
			if (!isCampaignNamePresentFranpaccessibilityprivatemustatusV2 == false) {
				fc.utobj().throwsException("was not able to verify Private (Status Driven)Campaign created by Franchise MU user(V1) who does not have privilege to see private campaign by Franchisee Employee user(V2)");
			}
			
			fc.utobj().printTestStep("Navigate to CRM>Campaign Center>email template");
			emailtemplate.clickonemailtamplate(driver);
			fc.utobj().clickElement(driver, crmcenter.locationtemplate);
			//Verify Public to all users template created by Corporate user who does not have privilege to see private template by Franchisee mu user(V2)
			fc.utobj().printTestStep("Verify Public to all users of my franchise template created by franchise MU user(V1) who does not have privilege to see private template by Franchisee Employee user(V2)");
			campaignCenterPage.searchTemplateByFilter(driver, firsttemplateNamefranmu);

			boolean isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchisemuV2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firsttemplateNamefranmu + "')]");
			if (isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchisemuV2 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my franchise template created by franchise MU user(V1) who does not have privilege to see private template by Franchisee Employee user(V2)");
			}
			
			//Verify Private template created by franchise user who does not have privilege to see private template by Franchisee mu user(V2)
			fc.utobj().printTestStep("Verify Private template created by franchise MU user(V1) who does not have privilege to see private template by Franchisee Employee user(V2)");
			campaignCenterPage.searchTemplateByFilter(driver, secondtemplateNamefranmu);
			
			boolean isTemplateNamePresentfranaccessibilityprivatemuV2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondtemplateNamefranmu + "')]");
			if (!isTemplateNamePresentfranaccessibilityprivatemuV2 == false) {
				fc.utobj().throwsException("was not able to verify private template created by franchise MU user(V1) who does not have privilege to see private template by Franchisee Employee user(V2)");
			}
			
			//Verify Public to all users template created by Corporate user who does not have privilege to see private template by Franchisee mu user(V2)
			fc.utobj().printTestStep("Verify Public to all users of my franchise (Status Driven)template created by franchise MU user(V1) who does not have privilege to see private template by Franchisee Employee user(V2)");
			campaignCenterPage.searchTemplateByFilter(driver, firsttemplateNamefranmustatus);

			boolean isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchisemustatusV2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firsttemplateNamefranmustatus + "')]");
			if (isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchisemustatusV2 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my franchise (Status Driven)template created by franchise MU user(V1) who does not have privilege to see private template by Franchisee Employee user(V2)");
			}
			
			//Verify Private template created by franchise user who does not have privilege to see private template by Franchisee mu user(V2)
			fc.utobj().printTestStep("Verify Private (Status Driven)template created by franchise MU user(V1) who does not have privilege to see private template by Franchisee Employee user(V2)");
			campaignCenterPage.searchTemplateByFilter(driver, secondtemplateNamefranmustatus);
			
			boolean isTemplateNamePresentfranaccessibilityprivatemustatusV2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondtemplateNamefranmustatus + "')]");
			if (!isTemplateNamePresentfranaccessibilityprivatemustatusV2 == false) {
				fc.utobj().throwsException("was not able to verify private (Status Driven)template created by franchise MU user(V1) who does not have privilege to see private template by Franchisee Employee user(V2)");
			}
			
			if(isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchiseV2 && !isCampaignNamePresentFranpaccessibilityprivateV2 
					&& isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchisestatusV2 && !isCampaignNamePresentFranpaccessibilityprivatestatusV2 && isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchiseV2 
					&& !isTemplateNamePresentfranaccessibilityprivateV2 && isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchisestatusV2 && !isTemplateNamePresentfranaccessibilityprivatestatusV2  
					&& isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchisemuV2 && !isCampaignNamePresentFranpaccessibilityprivatemuV2 && isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchisemustatusV2 
					&& !isCampaignNamePresentFranpaccessibilityprivatemustatusV2 && isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchisemuV2 && !isTemplateNamePresentfranaccessibilityprivatemuV2 
					&& isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchisemustatusV2 && !isTemplateNamePresentfranaccessibilityprivatemustatusV2) {
				fc.utobj().printTestStep("able to verify all types of campaigns and templates created by corporate/rgional/franchise users when loging with Franchisee Employee user(V2) who has priviledge to see private campaign");
			}
			else {
				fc.utobj().throwsException("was not able to verify all types of campaigns and templates created by corporate/rgional/franchise users when loging with Franchisee Employee user(V2) who has priviledge to see private campaign");
			}
			
			//Login with Franchisee owner user(FONew) who does not have role to see private campaign
			fc.utobj().printTestStep("Login with Franchisee owner user(FONew) who does not have role to see private campaign");
			fc.loginpage().loginWithParameter(driver, userNameFONew, passwordFONew);
			fc.crm().crm_common().openCRMCampaignCenterPage(fcHomePageTest, driver);
			fc.utobj().printTestStep("Click on the menu option and select campaign");
			
			fc.utobj().printTestStep("Navigate to CRM>Campaign Center>campaign template");
			emailtemplate.clickoncampaigntemplate(driver);
			
			fc.utobj().printTestStep("Verifying Campaigns and templates of Franchise mu owner user (V1) by Franchisee owner user(FONew)");
			
			fc.utobj().printTestStep("Verify Public to all users of my Franchise Campaign created by Franchise MU user(V1) who does not have privilege to see private campaign by Franchisee owner user(FONew)");
			campaignCenterPage.searchCampaignByFilter(driver, firstcampaignNamefranmu);

			boolean isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchisemuFONew = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firstcampaignNamefranmu + "')]");
			if (isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchisemuFONew == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my Franchise Campaign created by Franchise MU user(V1) who does not have privilege to see private campaign by Franchisee owner user(FONew))");
			}
			
			//Verify Private Campaign created by Franchise user who does not have privilege to see private campaign by Franchisee owner user(FONew)
			fc.utobj().printTestStep("Verify Private Campaign created by Franchise MU user(V1) who does not have privilege to see private campaign by Franchisee owner user(FONew))");
			campaignCenterPage.searchCampaignByFilter(driver, secondcampaignNamefranmu);
			
			boolean isCampaignNamePresentFranpaccessibilityprivatemuFONew = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondcampaignNamefranmu + "')]");
			if (!isCampaignNamePresentFranpaccessibilityprivatemuFONew == false) {
				fc.utobj().throwsException("was not able to verify Private Campaign created by Franchise MU user(V1) who does not have privilege to see private campaign by Franchisee owner user(FONew))");
			}
			
			
			//Verify Public to all users of my Franchise (Status Driven)Campaign created by Franchise user who does not have privilege to see private campaign by Franchisee owner user(FONew)
			fc.utobj().printTestStep("Verify Public to all users of my Franchise (Status Driven)Campaign created by Franchise MU user(V1) who does not have privilege to see private campaign by Franchisee owner user(FONew))");
			campaignCenterPage.searchCampaignByFilter(driver, firstcampaignNamefranmustatus);

			boolean isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchisemustatusFONew = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firstcampaignNamefranmustatus + "')]");
			if (isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchisemustatusFONew == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my Franchise (Status Driven)Campaign created by Franchise MU user(V1) who does not have privilege to see private campaign by Franchisee owner user(FONew))");
			}
			
			//Verify Private Campaign created by Franchise user who does not have privilege to see private campaign by Franchisee owner user(FONew)
			fc.utobj().printTestStep("Verify Private (Status Driven)Campaign created by Franchise MU user(V1) who does not have privilege to see private campaign by Franchisee owner user(FONew))");
			campaignCenterPage.searchCampaignByFilter(driver, secondcampaignNamefranmustatus);
			
			boolean isCampaignNamePresentFranpaccessibilityprivatemustatusFONew = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondcampaignNamefranmustatus + "')]");
			if (!isCampaignNamePresentFranpaccessibilityprivatemustatusFONew == false) {
				fc.utobj().throwsException("was not able to verify Private (Status Driven)Campaign created by Franchise MU user(V1) who does not have privilege to see private campaign by Franchisee owner user(FONew))");
			}
			
			fc.utobj().printTestStep("Navigate to CRM>Campaign Center>email template");
			emailtemplate.clickonemailtamplate(driver);
			fc.utobj().clickElement(driver, crmcenter.locationtemplate);
			//Verify Public to all users template created by Corporate user who does not have privilege to see private template by Franchisee owner user(FONew)
			fc.utobj().printTestStep("Verify Public to all users of my franchise template created by franchise MU user(V1) who does not have privilege to see private template by Franchisee owner user(FONew))");
			campaignCenterPage.searchTemplateByFilter(driver, firsttemplateNamefranmu);

			boolean isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchisemuFONew = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firsttemplateNamefranmu + "')]");
			if (isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchisemuFONew == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my franchise template created by franchise MU user(V1) who does not have privilege to see private template by Franchisee owner user(FONew))");
			}
			
			//Verify Private template created by franchise user who does not have privilege to see private template by Franchisee owner user(FONew)
			fc.utobj().printTestStep("Verify Private template created by franchise MU user(V1) who does not have privilege to see private template by Franchisee owner user(FONew))");
			campaignCenterPage.searchTemplateByFilter(driver, secondtemplateNamefranmu);
			
			boolean isTemplateNamePresentfranaccessibilityprivatemuFONew = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondtemplateNamefranmu + "')]");
			if (!isTemplateNamePresentfranaccessibilityprivatemuFONew == false) {
				fc.utobj().throwsException("was not able to verify private template created by franchise MU user(V1) who does not have privilege to see private template by Franchisee owner user(FONew))");
			}
			
			//Verify Public to all users template created by Corporate user who does not have privilege to see private template by Franchisee owner user(FONew)
			fc.utobj().printTestStep("Verify Public to all users of my franchise (Status Driven)template created by franchise MU user(V1) who does not have privilege to see private template by Franchisee owner user(FONew))");
			campaignCenterPage.searchTemplateByFilter(driver, firsttemplateNamefranmustatus);

			boolean isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchisemustatusFONew = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firsttemplateNamefranmustatus + "')]");
			if (isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchisemustatusFONew == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my franchise (Status Driven)template created by franchise MU user(V1) who does not have privilege to see private template by Franchisee owner user(FONew))");
			}
			
			//Verify Private template created by franchise user who does not have privilege to see private template by Franchisee owner user(FONew)
			fc.utobj().printTestStep("Verify Private (Status Driven)template created by franchise MU user(V1) who does not have privilege to see private template by Franchisee owner user(FONew))");
			campaignCenterPage.searchTemplateByFilter(driver, secondtemplateNamefranmustatus);
			
			boolean isTemplateNamePresentfranaccessibilityprivatemustatusFONew = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondtemplateNamefranmustatus + "')]");
			if (!isTemplateNamePresentfranaccessibilityprivatemustatusFONew == false) {
				fc.utobj().throwsException("was not able to verify private (Status Driven)template created by franchise MU user(V1) who does not have privilege to see private template by Franchisee owner user(FONew))");
			}
			
			fc.utobj().printTestStep("Navigate to CRM>Campaign Center>campaign template");
			emailtemplate.clickoncampaigntemplate(driver);
			fc.utobj().printTestStep("Verifying Campaigns and templates of V2 by Franchisee owner user(FONew)");
			fc.utobj().printTestStep("Verify Public to all users of my Franchise Campaign created by V2) who does not have privilege to see private campaign by Franchisee owner user(FONew)");
			campaignCenterPage.searchCampaignByFilter(driver, firstcampaignNamefranmue2);

			boolean isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchisemue2FONew = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firstcampaignNamefranmue2 + "')]");
			if (isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchisemue2FONew == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my Franchise Campaign created by V2) who does not have privilege to see private campaign by Franchisee owner user(FONew)");
			}
			
			//Verify Private Campaign created by Franchise user who does not have privilege to see private campaign by Franchisee owner user(FONew)
			fc.utobj().printTestStep("Verify Private Campaign created by V2) who does not have privilege to see private campaign by Franchisee owner user(FONew)");
			campaignCenterPage.searchCampaignByFilter(driver, secondcampaignNamefranmue2);
			
			boolean isCampaignNamePresentFranpaccessibilityprivatemue2FONew = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondcampaignNamefranmue2 + "')]");
			if (!isCampaignNamePresentFranpaccessibilityprivatemue2FONew == false) {
				fc.utobj().throwsException("was not able to verify Private Campaign created by V2) who does not have privilege to see private campaign by Franchisee owner user(FONew)");
			}
			
			
			//Verify Public to all users of my Franchise Campaign created by V2) who does not have privilege to see private campaign by Franchisee owner user(FONew)
			fc.utobj().printTestStep("Verify Public to all users of my Franchise (Status Driven)Campaign created by V2) who does not have privilege to see private campaign by Franchisee owner user(FONew)");
			campaignCenterPage.searchCampaignByFilter(driver, firstcampaignNamefranmue2status);

			boolean isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchisemue2statusFONew = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firstcampaignNamefranmue2status + "')]");
			if (isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchisemue2statusFONew == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my Franchise (Status Driven)Campaign created by V2) who does not have privilege to see private campaign by Franchisee owner user(FONew)");
			}
			
			//Verify Private Campaign created by Franchise user who does not have privilege to see private campaign by Franchisee owner user(FONew)
			fc.utobj().printTestStep("Verify Private (Status Driven)Campaign created by V2) who does not have privilege to see private campaign by Franchisee owner user(FONew)");
			campaignCenterPage.searchCampaignByFilter(driver, secondcampaignNamefranmue2status);
			
			boolean isCampaignNamePresentFranpaccessibilityprivatemue2statusFONew = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondcampaignNamefranmue2status + "')]");
			if (!isCampaignNamePresentFranpaccessibilityprivatemue2statusFONew == false) {
				fc.utobj().throwsException("was not able to verify Private (Status Driven)Campaign created by V2) who does not have privilege to see private campaign by Franchisee owner user(FONew)");
			}
			
			fc.utobj().printTestStep("Navigate to CRM>Campaign Center>email template");
			emailtemplate.clickonemailtamplate(driver);
			fc.utobj().clickElement(driver, crmcenter.locationtemplate);
			//Verify Public to all users template created by Franchise user who does not have privilege to see private template by Franchisee owner user(FONew)
			fc.utobj().printTestStep("Verify Public to all users of my franchise template created by V2) who does not have privilege to see private template by Franchisee owner user(FONew)");
			campaignCenterPage.searchTemplateByFilter(driver, firsttemplateNamefranmue2);

			boolean isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchisemue2FONew = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firsttemplateNamefranmue2 + "')]");
			if (isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchisemue2FONew == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my franchise template created by V2) who does not have privilege to see private template by Franchisee owner user(FONew)");
			}
			
			//Verify Private template created by franchise user who does not have privilege to see private template by Franchisee owner user(FONew)
			fc.utobj().printTestStep("Verify Private template created by V2) who does not have privilege to see private template by Franchisee owner user(FONew)");
			campaignCenterPage.searchTemplateByFilter(driver, secondtemplateNamefranmue2);
			
			boolean isTemplateNamePresentfranaccessibilityprivatemue2FONew = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondtemplateNamefranmue2 + "')]");
			if (!isTemplateNamePresentfranaccessibilityprivatemue2FONew == false) {
				fc.utobj().throwsException("was not able to verify private template created by V2) who does not have privilege to see private template by Franchisee owner user(FONew)");
			}
			
			
			//Verify Public to all users template created by Franchise user who does not have privilege to see private template by Franchisee owner user(FONew)
			fc.utobj().printTestStep("Verify Public to all users of my franchise (Status Driven)template created by V2) who does not have privilege to see private template by Franchisee owner user(FONew)");
			campaignCenterPage.searchTemplateByFilter(driver, firsttemplateNamefranmue2status);

			boolean isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchisemue2statusFONew = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firsttemplateNamefranmue2status + "')]");
			if (isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchisemue2statusFONew == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my franchise (Status Driven)template created by V2) who does not have privilege to see private template by Franchisee owner user(FONew)");
			}
			
			//Verify Private template created by franchise user who does not have privilege to see private template by Franchisee owner user(FONew)
			fc.utobj().printTestStep("Verify Private (Status Driven)template created by V2) who does not have priviloege to see private template by Franchisee owner user(FONew)");
			campaignCenterPage.searchTemplateByFilter(driver, secondtemplateNamefranmue2status);
			
			boolean isTemplateNamePresentfranaccessibilityprivatemue2statusFONew = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondtemplateNamefranmue2status + "')]");
			if (!isTemplateNamePresentfranaccessibilityprivatemue2statusFONew == false) {
				fc.utobj().throwsException("was not able to verify private (Status Driven)template created by V2) who does not have privilege to see private template by Franchisee owner user(FONew)");
			}
			
			if( isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchisemuFONew && !isCampaignNamePresentFranpaccessibilityprivatemuFONew && isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchisemustatusFONew 
					&& !isCampaignNamePresentFranpaccessibilityprivatemustatusFONew && isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchisemuFONew && !isTemplateNamePresentfranaccessibilityprivatemuFONew 
					&& isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchisemustatusFONew && !isTemplateNamePresentfranaccessibilityprivatemustatusFONew && isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchisemue2FONew 
					&& !isCampaignNamePresentFranpaccessibilityprivatemue2FONew && isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchisemue2statusFONew && !isCampaignNamePresentFranpaccessibilityprivatemue2statusFONew 
					&& isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchisemue2FONew && !isTemplateNamePresentfranaccessibilityprivatemue2FONew && isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchisemue2statusFONew
					&& !isTemplateNamePresentfranaccessibilityprivatemue2statusFONew) {
				fc.utobj().printTestStep("able to verify all types of campaigns and templates created by mu/employee and owner users when loging with Franchisee owner user(FONew)) who does not have priviledge to see private campaign");
			}
			else {
				fc.utobj().throwsException("was not able to verify all types of campaigns and templates created by mu/employee and owner users users when loging with Franchisee owner user(FONew)) who does not have priviledge to see private campaign");
			}
			
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		}
					
		catch(Exception e)
		{
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	
	}
	
	
	@Test(groups = {"crm","crmcampaignfinal"})
	@TestCase(createdOn = "2018-10-18", updatedOn = "2018-10-18", testCaseDescription = "To verify campaign center visibility from all types of users through corporate,regional and franchisee users if don't have priviledge", testCaseId = "Verify_CampaignCenter_Link")
	public void visibilityofcampaigncenter() throws Exception {
		fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());
		String testCaseId="PTA_Campaign_From_Corporate_User";
		String area="Campaign_Visibility";
		Map<String, String> dataSet = fc.utobj().readTestDatawithsqllite("crm", testCaseId,area);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		AdminUsersManageManageFranchiseUsersPage pobj = new AdminUsersManageManageFranchiseUsersPage(driver);
		try {
			driver = fc.loginpage().login(driver);
			FCHomePageTest fcHomePageTest= new FCHomePageTest();
			CRMCampaignCenterPage crmcenter=new CRMCampaignCenterPage(driver);
			//Add New Corporate Role which Can not View Campaign Center
			fc.utobj().printTestStep("Add New Corporate Role which Can not View Campaign Center");
			String roleType = dataSet.get("CorproleType");
			String roleNamewithoutpri = fc.utobj().generateTestData(dataSet.get("CorproleNamewithoutpri"));//CR2
			String moduleName = dataSet.get("CorpmoduleName");
			Map<String, String> withoutprivileges = new HashMap<String, String>();
			String canviewcampaigncenter="Can View Campaign Center";
			withoutprivileges.put(canviewcampaigncenter,dataSet.get("PrivilegeNo"));
			AdminUsersRolesAddNewRolePageTest setpriviledge=new AdminUsersRolesAddNewRolePageTest();
			setpriviledge.addRolesWithModulesPrivileges(driver, roleType, roleNamewithoutpri, moduleName, withoutprivileges, testCaseId);
			
			//Add New Regional Role which Can not View Campaign Center
			fc.utobj().printTestStep("Add New Regional Role which Can not View Campaign Center");
			String roleType1 = dataSet.get("RegroleType1");
			String roleNamewithoutpri1 = fc.utobj().generateTestData(dataSet.get("RegroleNamewithoutpri1"));//RR2
			Map<String, String> withoutprivileges1 = new HashMap<String, String>();
			withoutprivileges1.put(canviewcampaigncenter,dataSet.get("PrivilegeNo"));
			setpriviledge.addRolesWithModulesPrivileges(driver, roleType1, roleNamewithoutpri1, moduleName, withoutprivileges1, testCaseId);
			
			//Add New Franchise Role which Can not View Campaign Center
			fc.utobj().printTestStep("Add New Franchise Role which Can not View Campaign Center");
			String roleNamewithpri2 = fc.utobj().generateTestData(dataSet.get("FranroleNamewithpri2"));//FR1
			String roleType2 = dataSet.get("FranroleType2");
			Map<String, String> withprivileges2 = new HashMap<String, String>();
			withprivileges2.put(canviewcampaigncenter, dataSet.get("PrivilegeNo"));
			setpriviledge.addRolesWithModulesPrivileges(driver, roleType2, roleNamewithpri2, moduleName, withprivileges2, testCaseId);
			
			//Adding a corporate user who doesn't have role to view campaign center
			//C1
			fc.utobj().printTestStep("Adding a corporate user who has role to view private campaign(C1)");
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporate_user = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser = new CorporateUser();
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUserwithSQLite(corpUser,dataSet);
			//corpUser.setRole(roleNamewithpri);
			corpUser = corporate_user.createDefaultUser(driver, corpUser);
			//Getting the username and password for corporate user
			fc.utobj().printTestStep("Getting the username and password for corporate user");
			String CorpUserName=corpUser.getUserName();
			String CorpUserPassword=corpUser.getPassword();
			System.out.println("Corporate(C1) userid and password"+CorpUserName+CorpUserPassword);
			
			
			//Creating a corporate user who does not have role to view private campaign(C2)
			//C2
			fc.utobj().printTestStep("Creating a corporate user who does not have role to view private campaign(C2)");
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporate_user1 = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser1 = new CorporateUser();
			corpUser1 = fc.commonMethods().fillDefaultDataFor_CorporateUserwithSQLite(corpUser1,dataSet);
			corpUser1.setRole(roleNamewithoutpri);
			corpUser1 = corporate_user1.createDefaultUser(driver, corpUser1);
			//Getting the username and password for corporate user
			fc.utobj().printTestStep("Getting the username and password for another corporate user");
			String CorpUserName1=corpUser1.getUserName();
			String CorpUserPassword1=corpUser1.getPassword();
			System.out.println("Corporate(C2) userid and password"+CorpUserName1+CorpUserPassword1);
			
			//Adding a region and creating a regional user who has role to view private campaign(Region and R1)
			//R1
			fc.utobj().printTestStep("Adding a region and creating a regional user who has role to view private campaign(Region and R1)");
			AdminUsersManageRegionalUsersAddRegionalUserPageTest regionalUserPage = new AdminUsersManageRegionalUsersAddRegionalUserPageTest();
			String regionName = fc.utobj().generateTestData(dataSet.get("Regionname"));
			//username and password for regional user
			String Reguname = fc.utobj().generateTestData(dataSet.get("Reguname"));//AreaRegion
			String Regpassword=dataSet.get("Regpassword");
			//regionalUserPage.addRegionalUserwithunamepass(driver, Reguname, regionName,Regpassword,dataSet,roleNamewithpri1);
			System.out.println("Regional(R1) user id and password"+Reguname+" "+Regpassword);
			
			//Adding the same region area and creating a regional user who does not have role to view private campaign(R2)
			//R2
			fc.utobj().printTestStep("Adding the same region area and creating a regional user who does not have role to view private campaign(R2)");
			//username and password for regional user
			String Reguname1 = fc.utobj().generateTestData(dataSet.get("Reguname"));
			String Regpassword1=dataSet.get("Regpassword");
			regionalUserPage.addRegionalUserwithunamepasswithoutaddingarea(driver, Reguname1, regionName,Regpassword1,dataSet,roleNamewithoutpri1);
			fc.utobj().printTestStep("R2 added");
			System.out.println("Regional(R2) user id and password"+Reguname1+" "+Regpassword1);
			
			//Mu user		
			//Add a franchise location with same region
			//L6
			fc.utobj().printTestStep("Add a franchise location with same region");
			String firstNameF5 = fc.utobj().generateTestData(dataSet.get("Franlocfirstname"));
			String lastNameF5 = fc.utobj().generateTestData(dataSet.get("Franloclasttname"));
			fc.utobj().printTestStep("Naviaget To Admin > Franchise Location > Add Franchise Location");
			AdminFranchiseLocationAddFranchiseLocationPageTest franchiseLocation5 = new AdminFranchiseLocationAddFranchiseLocationPageTest();
			String franchiseId5 = fc.utobj().generateTestData(dataSet.get("Franchiseid"));
			franchiseLocation5.addFranchiseLocationForMktwithoutaddingarearegion(driver, franchiseId5, regionName, firstNameF5, lastNameF5,dataSet);
			
			//Add Franchise Mu User
			//V1
			//Add Franchise owner mu User who has role to see private campaign
			fc.utobj().printTestStep("Add Franchise owner mu User who has role to see private campaign");
			AdminUsersManageManageFranchiseUsersPageTest addFranUsermu = new AdminUsersManageManageFranchiseUsersPageTest();
			//username and password for Owner type franchise user
			String userNameFOmu = fc.utobj().generateTestData(dataSet.get("Franowneruname"));
			String roleNameFOmu = roleNamewithpri2;//dataSet.get("Franownerrolename");
			String passwordFOmu = dataSet.get("Franownerpassword");
			String emailIdFOmu =fc.utobj().generateTestData(dataSet.get("Franowneremail"))+"@staffex.com";
			userNameFOmu = addFranUsermu.addFranchiseUserwithoutchangeinsettings(driver, userNameFOmu, passwordFOmu, franchiseId5, roleNameFOmu, emailIdFOmu,dataSet);
			System.out.println("(V1)Username and password for franchise mu user is"+userNameFOmu+" "+passwordFOmu);
			
			//Add a new Franchise location with same region but owner as existing owner
			//L7
			fc.utobj().printTestStep("Add a new Franchise location with same region but owner as existing owner");
			String franchiseId6 = fc.utobj().generateTestData(dataSet.get("Franchiseid"));
			AdminFranchiseLocationAddFranchiseLocationPageTest franchiseLocation6 = new AdminFranchiseLocationAddFranchiseLocationPageTest();
			franchiseLocation6.addFranchiseLocationForMktwithoutaddingarearegionwithexistingowner(driver, franchiseId6, regionName, firstNameF5, lastNameF5, dataSet);
			
			//Add Franchise employee user
			//V2
			fc.utobj().printTestStep("Add Franchise employee User who has role to see private campaign");
			AdminUsersManageManageFranchiseUsersPageTest addFranEmpUsermu = new AdminUsersManageManageFranchiseUsersPageTest();
			String userNameFEmu = fc.utobj().generateTestData(dataSet.get("Franempuname"));
			String roleNameFEmu =roleNamewithpri2;//dataSet.get("Franownerrolename");
			String passwordFEmu = dataSet.get("Franemppassword");
			String emailIdFEmu =fc.utobj().generateTestData(dataSet.get("Franempemail"))+"@staffex.com";
			userNameFEmu = addFranEmpUsermu.addFranchiseEmployeeUserwithoutchangeinsettings(driver, userNameFEmu, passwordFEmu, franchiseId6, roleNameFEmu, emailIdFEmu,dataSet);
			System.out.println("(V2)Username and password for franchise employee existing mu user is"+userNameFEmu+" "+passwordFEmu);
			
			//Add a new Franchisee owner user with location same as existing location of V2
			AdminPageTest admninpage=new AdminPageTest();
			admninpage.manageFranchiseLocationLnk(driver);
			AdminPage adminpage=new AdminPage(driver);
			fc.utobj().selectValFromMultiSelect(driver, adminpage.franchiseid,franchiseId6);//franchiseId6//Locationjain2
			fc.utobj().clickElement(driver,adminpage.searchbuttonoffranchisee);
			fc.utobj().clickElement(driver, adminpage.actionmenuoffranchisee);
			fc.utobj().clickElement(driver,adminpage.usersoffranchisee);
			fc.utobj().clickElement(driver,adminpage.addowner);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			String first=fc.utobj().generateTestData(dataSet.get("FranNewowner"));
			String last=fc.utobj().generateTestData(dataSet.get("FranNewownersurname"));
			String fullname=first+" "+last;
			fc.utobj().sendKeys(driver,adminpage.firstName,first);
			fc.utobj().sendKeys(driver,adminpage.lastName,last);
			fc.utobj().sleep(3000);
			fc.utobj().clickElement(driver, adminpage.save);
			fc.utobj().sleep(3000);
			fc.commonMethods().Click_Close_Input_ByValue(driver);
			fc.utobj().sleep(3000);
			fc.utobj().switchFrameToDefault(driver);
			fc.utobj().clickElement(driver, ".//*[contains(text(),'"+fullname+"')]/ancestor::tr[1]/td[4]/div/layer/a/img");
			fc.utobj().clickElement(driver,"//span[contains(text(),'Generate Login')]");
			/*fc.utobj().actionImgOption(driver, fullname,dataSet.get("Frangeneratlogin"));*/
			
		
			//Add New Franchise owner User who has role to see private campaign
			fc.utobj().printTestStep("Add New Franchise owner User who has role to see private campaign");
			//username and password for Owner type franchise user
			String userNameFONew = fc.utobj().generateTestData(dataSet.get("Franowneruname"));
			String roleNameFONew = roleNamewithpri2;//dataSet.get("Franownerrolename");
			String passwordFONew = dataSet.get("Franownerpassword");
			fc.utobj().sendKeys(driver, pobj.userName, userNameFONew);
			fc.utobj().sendKeys(driver, pobj.password, passwordFONew);
			fc.utobj().sendKeys(driver, pobj.confirmPassword, passwordFONew);
			fc.utobj().selectValFromMultiSelect(driver, pobj.selectRole, roleNameFONew);
			fc.utobj().clickElement(driver, pobj.submit);
			System.out.println("Franchisee owner(FONew) userName and Password are"+userNameFONew+" "+passwordFONew);
		}
		catch(Exception e) {
			
		}
	}
}
