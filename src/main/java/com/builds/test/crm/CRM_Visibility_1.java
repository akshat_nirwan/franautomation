package com.builds.test.crm;

import java.util.HashMap;
import java.util.Map;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.Test;
import com.builds.test.admin.AdminFranchiseLocationAddFranchiseLocationPageTest;
import com.builds.test.admin.AdminUsersManageCorporateUsersAddCorporateUserPageTest;
import com.builds.test.admin.AdminUsersManageManageFranchiseUsersPageTest;
import com.builds.test.admin.AdminUsersManageRegionalUsersAddRegionalUserPageTest;
import com.builds.test.admin.AdminUsersRolesAddNewRolePageTest;
import com.builds.test.common.CorporateUser;
import com.builds.test.common.FCHomePageTest;
import com.builds.uimaps.campaigncentercrm.CRMCampaignCenterCampaignsPage;
import com.builds.uimaps.campaigncentercrm.CRMCampaignCenterPage;
import com.builds.utilities.FranconnectUtil;
import com.builds.utilities.TestCase;

public class CRM_Visibility_1 {
	
	FranconnectUtil fc = new FranconnectUtil();
	
	@Test(groups = "CRM_Visibility_1")
	@TestCase(createdOn = "2018-09-25", updatedOn = "2018-09-25", testCaseDescription = "To verify the visibility of public to all user campaign/template (Code your Own and layout and Theme)created by Corporate user through regional/franchisee users", testCaseId = "PTA_Campaign_From_Corporate_User")
	public void visibilityofcampaign() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());
		String area="Campaign_Visibility";
		Map<String, String> dataSet = fc.utobj().readTestDatawithsqllite("crm", testCaseId,area);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		
		try {
			driver = fc.loginpage().login(driver);	
			//----------------------Create Roles and users. Assign roles to users
			//Add New Corporate Role which Can not View Private Folders / Templates of Other Users
			fc.utobj().printTestStep("Add New Corporate Role which Can not View Private Folders / Templates of Other Users");
			String roleType = dataSet.get("CorproleType");
			String roleNamewithoutpri = fc.utobj().generateTestData(dataSet.get("CorproleNamewithoutpri"));//CR2
			String moduleName = dataSet.get("CorpmoduleName");
			
			/*Map<String, String> withoutprivileges = new HashMap<String, String>();
			withoutprivileges.put(dataSet.get("Privilege"),dataSet.get("PrivilegeNo"));
			CRMLead_Area_RoleBase_TestCase setpriviledge= new CRMLead_Area_RoleBase_TestCase();
			setpriviledge.addRolesPrivilegetoseepriavatecampaign(driver, roleType, roleNamewithoutpri, moduleName, withoutprivileges, testCaseId);*/
			
			Map<String, String> withoutprivileges = new HashMap<String, String>();
			withoutprivileges.put(dataSet.get("Privilege"),dataSet.get("PrivilegeNo"));
			AdminUsersRolesAddNewRolePageTest setpriviledge=new AdminUsersRolesAddNewRolePageTest();
			setpriviledge.addRolesWithModulesPrivileges(driver, roleType, roleNamewithoutpri, moduleName, withoutprivileges, testCaseId);
			
			//Add New Corporate Role which Can View Private Folders / Templates of Other Users
			fc.utobj().printTestStep("Add New Corporate Role which Can View Private Folders / Templates of Other Users");
			String roleNamewithpri = fc.utobj().generateTestData(dataSet.get("CorproleNamewithpri"));//CR1
			/*Map<String, String> withprivileges = new HashMap<String, String>();
			withprivileges.put(dataSet.get("Privilege"), dataSet.get("PrivilegeYes"));
			setpriviledge.addRolesPrivilegetoseepriavatecampaign(driver, roleType, roleNamewithpri, moduleName, withprivileges, testCaseId);*/
			
			Map<String, String> withprivileges = new HashMap<String, String>();
			withprivileges.put(dataSet.get("Privilege"), dataSet.get("PrivilegeYes"));
			setpriviledge.addRolesWithModulesPrivileges(driver, roleType, roleNamewithpri, moduleName, withprivileges, testCaseId);
			
			//Add New Regional Role which Can not View Private Folders / Templates of Other Users Leads Privilege
			fc.utobj().printTestStep("Add New Regional Role which Can not View Private Folders / Templates of Other Users Leads Privilege");
			//AdminUsersRolesAddNewRolePageTest roles_page1 = new AdminUsersRolesAddNewRolePageTest();
			String roleType1 = dataSet.get("RegroleType1");
			String roleNamewithoutpri1 = fc.utobj().generateTestData(dataSet.get("RegroleNamewithoutpri1"));//RR2
			

		/*	Map<String, String> withoutprivileges1 = new HashMap<String, String>();
			withoutprivileges1.put(dataSet.get("Privilege"),dataSet.get("PrivilegeNo"));
			setpriviledge.addRolesPrivilegetoseepriavatecampaign(driver, roleType1, roleNamewithoutpri1, moduleName, withoutprivileges1, testCaseId);*/
			
			Map<String, String> withoutprivileges1 = new HashMap<String, String>();
			withoutprivileges1.put(dataSet.get("Privilege"),dataSet.get("PrivilegeNo"));
			setpriviledge.addRolesWithModulesPrivileges(driver, roleType1, roleNamewithoutpri1, moduleName, withoutprivileges1, testCaseId);
			
			//Add New Regional Role which Can View Private Folders / Templates of Other Users Leads Privilege
			fc.utobj().printTestStep("Add New Regional Role which Can View Private Folders / Templates of Other Users Leads Privilege");
			String roleNamewithpri1 = fc.utobj().generateTestData(dataSet.get("RegroleNamewithpri1"));//RR1
			/*Map<String, String> withprivileges1 = new HashMap<String, String>();
			withprivileges1.put(dataSet.get("Privilege"), dataSet.get("PrivilegeYes"));
			setpriviledge.addRolesPrivilegetoseepriavatecampaign(driver, roleType1, roleNamewithpri1, moduleName, withprivileges1, testCaseId);*/
			
			Map<String, String> withprivileges1 = new HashMap<String, String>();
			withprivileges1.put(dataSet.get("Privilege"), dataSet.get("PrivilegeYes"));
			setpriviledge.addRolesWithModulesPrivileges(driver, roleType1, roleNamewithpri1, moduleName, withprivileges1, testCaseId);
			
			//Add New Franchise Role which Can not View Private Folders / Templates of Other Users Leads Privilege
			fc.utobj().printTestStep("Add New Franchise Role which Can not View Private Folders / Templates of Other Users Leads Privilege");
			String roleType2 = dataSet.get("FranroleType2");
			String roleNamewithoutpri2 = fc.utobj().generateTestData(dataSet.get("FranroleNamewithoutpri2"));//FR2
			
			/*Map<String, String> withoutprivileges2 = new HashMap<String, String>();
			withoutprivileges2.put(dataSet.get("Privilege"),dataSet.get("PrivilegeNo"));
			setpriviledge.addRolesPrivilegetoseepriavatecampaign(driver, roleType2, roleNamewithoutpri2, moduleName, withoutprivileges2, testCaseId);*/
			Map<String, String> withoutprivileges2 = new HashMap<String, String>();
			withoutprivileges2.put(dataSet.get("Privilege"),dataSet.get("PrivilegeNo"));
			setpriviledge.addRolesWithModulesPrivileges(driver, roleType2, roleNamewithoutpri2, moduleName, withoutprivileges2, testCaseId);
			
			//Add New Franchise Role which Can View Private Folders / Templates of Other Users Leads Privilege
			fc.utobj().printTestStep("Add New Franchise Role which Can View Private Folders / Templates of Other Users Leads Privilege");
			String roleNamewithpri2 = fc.utobj().generateTestData(dataSet.get("FranroleNamewithpri2"));//FR1
			/*Map<String, String> withprivileges2 = new HashMap<String, String>();
			withprivileges2.put(dataSet.get("Privilege"), dataSet.get("PrivilegeYes"));
			setpriviledge.addRolesPrivilegetoseepriavatecampaign(driver, roleType2, roleNamewithpri2, moduleName, withprivileges2, testCaseId);*/
			
			Map<String, String> withprivileges2 = new HashMap<String, String>();
			withprivileges2.put(dataSet.get("Privilege"), dataSet.get("PrivilegeYes"));
			setpriviledge.addRolesWithModulesPrivileges(driver, roleType2, roleNamewithpri2, moduleName, withprivileges2, testCaseId);
			FCHomePageTest fcHomePageTest= new FCHomePageTest();
			
			//Adding a corporate user who has role to view private campaign
			//C1
			fc.utobj().printTestStep("Adding a corporate user who has role to view private campaign");
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporate_user = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser = new CorporateUser();
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUserwithSQLite(corpUser,dataSet);
			corpUser.setRole(roleNamewithpri);
			corpUser = corporate_user.createDefaultUser(driver, corpUser);
			//Getting the username and password for corporate user
			fc.utobj().printTestStep("Getting the username and password for corporate user");
			String CorpUserName=corpUser.getUserName();
			String CorpUserPassword=corpUser.getPassword();
			System.out.println("Corporate(C1) userid and password"+CorpUserName+CorpUserPassword);
			
			//Creating a corporate user who does not have role to view private campaign
			//C2
			fc.utobj().printTestStep("Creating a corporate user who does not have role to view private campaign");
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporate_user1 = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser1 = new CorporateUser();
			corpUser1 = fc.commonMethods().fillDefaultDataFor_CorporateUserwithSQLite(corpUser1,dataSet);
			corpUser1.setRole(roleNamewithoutpri);
			corpUser1 = corporate_user1.createDefaultUser(driver, corpUser1);
			//Getting the username and password for corporate user
			fc.utobj().printTestStep("Getting the username and password for another corporate user");
			String CorpUserName1=corpUser1.getUserName();
			String CorpUserPassword1=corpUser1.getPassword();
			System.out.println("Corporate(C2) userid and password"+CorpUserName1+CorpUserPassword1);
			
			//Adding a region and creating a regional user who has role to view private campaign
			//R1
			fc.utobj().printTestStep("Adding a region and creating a regional user who has role to view private campaign");
			AdminUsersManageRegionalUsersAddRegionalUserPageTest regionalUserPage = new AdminUsersManageRegionalUsersAddRegionalUserPageTest();
			String regionName = fc.utobj().generateTestData(dataSet.get("Regionname"));
			//username and password for regional user
			String Reguname = fc.utobj().generateTestData(dataSet.get("Reguname"));//AreaRegion
			String Regpassword=dataSet.get("Regpassword");
			regionalUserPage.addRegionalUserwithunamepass(driver, Reguname, regionName,Regpassword,dataSet,roleNamewithpri1);
			System.out.println("Regional(R1) user id and password"+Reguname+" "+Regpassword);
			
			
			//Adding the same region area and creating a regional user who does not have role to view private campaign
			//R2
			fc.utobj().printTestStep("Adding the same region area and creating a regional user who does not have role to view private campaign");
			//username and password for regional user
			String Reguname1 = fc.utobj().generateTestData(dataSet.get("Reguname"));
			String Regpassword1=dataSet.get("Regpassword");
			regionalUserPage.addRegionalUserwithunamepasswithoutaddingarea(driver, Reguname1, regionName,Regpassword1,dataSet,roleNamewithoutpri1);
			fc.utobj().printTestStep("R2 added");
			System.out.println("Regional user id and password"+Reguname1+" "+Regpassword1);
			
			//Add a franchise location with same region
			//Location1
			fc.utobj().printTestStep("Add a franchise location with same region");
			String firstNameF = fc.utobj().generateTestData(dataSet.get("Franlocfirstname"));
			String lastNameF = fc.utobj().generateTestData(dataSet.get("Franloclasttname"));
			fc.utobj().printTestStep("Naviaget To Admin > Franchise Location > Add Franchise Location");
			AdminFranchiseLocationAddFranchiseLocationPageTest franchiseLocation = new AdminFranchiseLocationAddFranchiseLocationPageTest();
			String franchiseId = fc.utobj().generateTestData(dataSet.get("Franchiseid"));
			franchiseLocation.addFranchiseLocationForMktwithoutaddingarearegion(driver, franchiseId, regionName, firstNameF, lastNameF,dataSet);
			
			//Add Franchise owner User
			fc.utobj().printTestStep("Add Franchise owner User who has role to see private campaign");
			AdminUsersManageManageFranchiseUsersPageTest addFranUser = new AdminUsersManageManageFranchiseUsersPageTest();
			//username and password for Owner type franchise user
			String userNameFO = fc.utobj().generateTestData(dataSet.get("Franowneruname"));
			String roleNameFO = roleNamewithpri2;
			String passwordFO = dataSet.get("Franownerpassword");
			String emailIdFO =fc.utobj().generateTestData(dataSet.get("Franowneremail"))+"@staffex.com";
			userNameFO = addFranUser.addFranchiseUserwithoutchangeinsettings(driver, userNameFO, passwordFO, franchiseId, roleNameFO, emailIdFO,dataSet);
			System.out.println("Franchise(F01) owner user id and password"+userNameFO+" "+passwordFO);
			//Add Franchise employee user
			//Add Franchise employee User who has role to see private campaign
			fc.utobj().printTestStep("Add Franchise employee User who has role to see private campaign");
			String userNameFE = fc.utobj().generateTestData(dataSet.get("Franempuname"));
			//String roleNameFE = dataSet.get("Franemprole");
			String roleNameFE =roleNamewithpri2;
			String passwordFE = dataSet.get("Franemppassword");
			String emailIdFE =fc.utobj().generateTestData(dataSet.get("Franempemail"))+"@staffex.com";
			userNameFE = addFranUser.addFranchiseEmployeeUserwithoutchangeinsettings(driver, userNameFE, passwordFE, franchiseId, roleNameFE, emailIdFE,dataSet);
			System.out.println("Franchise(FE1) owner user id and password"+userNameFE+" "+passwordFE);
			//Add second franchise location with same region
			//Location2
			fc.utobj().printTestStep("Add a franchise location with same region");
			String firstNameF1 = fc.utobj().generateTestData(dataSet.get("Franlocfirstname"));
			String lastNameF1 = fc.utobj().generateTestData(dataSet.get("Franloclasttname"));
			fc.utobj().printTestStep("Naviaget To Admin > Franchise Location > Add Franchise Location");
			AdminFranchiseLocationAddFranchiseLocationPageTest franchiseLocation1 = new AdminFranchiseLocationAddFranchiseLocationPageTest();
			String franchiseId1 = fc.utobj().generateTestData(dataSet.get("Franchiseid"));
			franchiseLocation1.addFranchiseLocationForMktwithoutaddingarearegion(driver, franchiseId1, regionName, firstNameF1, lastNameF1,dataSet);
			
			//Add Second Franchise owner User who does not have role to see private campaign
			fc.utobj().printTestStep("Add Second Franchise owner User who does not have role to see private campaign");
			AdminUsersManageManageFranchiseUsersPageTest addFranUser1 = new AdminUsersManageManageFranchiseUsersPageTest();
			//username and password for Owner type franchise user
			String userNameFO1 = fc.utobj().generateTestData(dataSet.get("Franowneruname"));
			String roleNameFO1 = roleNamewithoutpri2;
			String passwordFO1 = dataSet.get("Franownerpassword");
			String emailIdFO1 =fc.utobj().generateTestData(dataSet.get("Franowneremail"))+"@staffex.com";
			userNameFO1 = addFranUser1.addFranchiseUserwithoutchangeinsettings(driver, userNameFO1, passwordFO1, franchiseId1, roleNameFO1, emailIdFO1,dataSet);
			System.out.println("Franchise(FO2) owner user id and password"+userNameFO1+" "+passwordFO1);
			//Add Franchise employee user who does not have role to see private campaign
			fc.utobj().printTestStep("Add Franchise employee User who does not have role to see private campaign");
			String userNameFE1 = fc.utobj().generateTestData(dataSet.get("Franempuname"));
			//String roleNameFE = dataSet.get("Franemprole");
			String roleNameFE1 =roleNamewithoutpri2;
			String passwordFE1 = dataSet.get("Franemppassword");
			String emailIdFE1 =fc.utobj().generateTestData(dataSet.get("Franempemail"))+"@staffex.com";
			userNameFE1 = addFranUser.addFranchiseEmployeeUserwithoutchangeinsettings(driver, userNameFE1, passwordFE1, franchiseId1, roleNameFE1, emailIdFE1,dataSet);
			System.out.println("Franchise(FE2) owner user id and password"+userNameFE1+" "+passwordFE1);
			
			//Mu user
			
			//Add a franchise location with same region
			//L6
			fc.utobj().printTestStep("Add a franchise location with same region");
			String firstNameF5 = fc.utobj().generateTestData(dataSet.get("Franlocfirstname"));
			String lastNameF5 = fc.utobj().generateTestData(dataSet.get("Franloclasttname"));
			fc.utobj().printTestStep("Naviaget To Admin > Franchise Location > Add Franchise Location");
			AdminFranchiseLocationAddFranchiseLocationPageTest franchiseLocation5 = new AdminFranchiseLocationAddFranchiseLocationPageTest();
			String franchiseId5 = fc.utobj().generateTestData(dataSet.get("Franchiseid"));
			franchiseLocation5.addFranchiseLocationForMktwithoutaddingarearegion(driver, franchiseId5, regionName, firstNameF5, lastNameF5,dataSet);
			
			//Add Franchise owner User
			//V1
			//Add Franchise owner mu User who has role to see private campaign
			fc.utobj().printTestStep("Add Franchise owner mu User who has role to see private campaign");
			AdminUsersManageManageFranchiseUsersPageTest addFranUsermu = new AdminUsersManageManageFranchiseUsersPageTest();
			//username and password for Owner type franchise user
			String userNameFOmu = fc.utobj().generateTestData(dataSet.get("Franowneruname"));
			String roleNameFOmu = roleNamewithpri2;//dataSet.get("Franownerrolename");
			String passwordFOmu = dataSet.get("Franownerpassword");
			String emailIdFOmu =fc.utobj().generateTestData(dataSet.get("Franowneremail"))+"@staffex.com";
			userNameFOmu = addFranUsermu.addFranchiseUserwithoutchangeinsettings(driver, userNameFOmu, passwordFOmu, franchiseId5, roleNameFOmu, emailIdFOmu,dataSet);
			System.out.println("(V1)Username and password for franchise mu user is"+userNameFOmu+" "+passwordFOmu);
			
			//Add a new Franchise location with same region but owner as existing owner
			//L7
			fc.utobj().printTestStep("Add a new Franchise location with same region but owner as existing owner");
			String franchiseId6 = fc.utobj().generateTestData(dataSet.get("Franchiseid"));
			AdminFranchiseLocationAddFranchiseLocationPageTest franchiseLocation6 = new AdminFranchiseLocationAddFranchiseLocationPageTest();
			franchiseLocation6.addFranchiseLocationForMktwithoutaddingarearegionwithexistingowner(driver, franchiseId6, regionName, firstNameF5, lastNameF5, dataSet);
			
			//Add Franchise employee user
			//V2
			fc.utobj().printTestStep("Add Franchise employee User who does not have role to see private campaign");
			AdminUsersManageManageFranchiseUsersPageTest addFranEmpUsermu = new AdminUsersManageManageFranchiseUsersPageTest();
			String userNameFEmu = fc.utobj().generateTestData(dataSet.get("Franempuname"));
			//String roleNameFE = dataSet.get("Franemprole");
			String roleNameFEmu =roleNamewithoutpri2;//dataSet.get("Franownerrolename");
			String passwordFEmu = dataSet.get("Franemppassword");
			String emailIdFEmu =fc.utobj().generateTestData(dataSet.get("Franempemail"))+"@staffex.com";
			userNameFEmu = addFranEmpUsermu.addFranchiseEmployeeUserwithoutchangeinsettings(driver, userNameFEmu, passwordFEmu, franchiseId6, roleNameFEmu, emailIdFEmu,dataSet);
			System.out.println("(V2)Username and password for franchise employee existing mu user is"+userNameFEmu+" "+passwordFEmu);
			
			
			//Login with Corporate user who has role to see private campaign(C1)
			fc.utobj().printTestStep("Login with Corporate user");
			fc.loginpage().loginWithParameter(driver, CorpUserName, CorpUserPassword);
			fc.crm().crm_common().openCRMCampaignCenterPage(fcHomePageTest, driver);
			
			//Create Campaign At Campaign Center > Campaigns
			CRMCampaignCenterPageTest campaignCenterPage = new CRMCampaignCenterPageTest();
			CRMCampaignCenterCampaignsPage pobjcam = new CRMCampaignCenterCampaignsPage(driver);
			fc.utobj().printTestStep("Create Campaign At Campaign Center > Campaigns");
			String firstcampaignName = fc.utobj().generateTestData(dataSet.get("CorpcampaignName"));
			String firstaccessibility = dataSet.get("Corpaccessibilitypublictoallusers");
			String firstcampaignType = dataSet.get("CorpcampaignType");
			String firsttemplateName = fc.utobj().generateTestData(dataSet.get("CorptemplateName"));
			String firstemailSubject = fc.utobj().generateTestData(dataSet.get("CorpemailSubject"));
			String firstplainTextEditor = fc.utobj().generateTestData(dataSet.get("CorpplainTextEditor"));
			campaignCenterPage.createCampaignGenericwithSqlite(driver, dataSet, firstcampaignName, firstaccessibility,
					firstcampaignType, firsttemplateName, firstemailSubject, firstplainTextEditor);
			
			//Create another campaign at Campaign Center > Campaigns
			fc.utobj().printTestStep("Create another campaign at Campaign Center > Campaigns");
			String secondcampaignName = fc.utobj().generateTestData(dataSet.get("CorpcampaignName"));
			String publictocorporateusersaccessibility = dataSet.get("Corpaccessibilitypublictoallcorporateusers");
			String secondcampaignType = dataSet.get("CorpcampaignType");
			String secondtemplateName = fc.utobj().generateTestData(dataSet.get("CorptemplateName"));
			String secondemailSubject = fc.utobj().generateTestData(dataSet.get("CorpemailSubject"));
			String secondplainTextEditor = fc.utobj().generateTestData(dataSet.get("CorpplainTextEditor"));
			campaignCenterPage.createCampaignGenericwithSqlite(driver, dataSet, secondcampaignName, publictocorporateusersaccessibility,
					secondcampaignType, secondtemplateName, secondemailSubject, secondplainTextEditor);
			
			//Create another campaign1 at Campaign Center > Campaigns
			fc.utobj().printTestStep("Create another campaign at Campaign Center > Campaigns");
			String thirdcampaignName = fc.utobj().generateTestData(dataSet.get("CorpcampaignName"));
			String privatecorpaccessibility = dataSet.get("Corpaccessibilityprivate");
			String thirdcampaignType = dataSet.get("CorpcampaignType");
			String thirdtemplateName = fc.utobj().generateTestData(dataSet.get("CorptemplateName"));
			String thirdemailSubject = fc.utobj().generateTestData(dataSet.get("CorpemailSubject"));
			String thirdplainTextEditor = fc.utobj().generateTestData(dataSet.get("CorpplainTextEditor"));
			campaignCenterPage.createCampaignGenericwithSqlite(driver, dataSet, thirdcampaignName, privatecorpaccessibility,
					thirdcampaignType, thirdtemplateName, thirdemailSubject, thirdplainTextEditor);
			
			//Create Status Driven campaign At Campaign Center > Campaigns
			fc.utobj().printTestStep("Create Status Driven campaign At Campaign Center > Campaigns");
			String firstcampaignNamestatus = fc.utobj().generateTestData(dataSet.get("CorpcampaignName"));
			String firstaccessibilitystatus = dataSet.get("Corpaccessibilitypublictoallusers");
			String firstcampaignTypestatus = dataSet.get("StatusCampaigntype");
			String firsttemplateNamestatus = fc.utobj().generateTestData(dataSet.get("CorptemplateName"));
			String firstemailSubjectstatus = fc.utobj().generateTestData(dataSet.get("CorpemailSubject"));
			String firstplainTextEditorstatus = fc.utobj().generateTestData(dataSet.get("CorpplainTextEditor"));
			campaignCenterPage.createCampaignGenericwithSqlite(driver, dataSet, firstcampaignNamestatus, firstaccessibilitystatus,
					firstcampaignTypestatus, firsttemplateNamestatus, firstemailSubjectstatus, firstplainTextEditorstatus);
			
			//Create Status Driven campaign 1 at Campaign Center > Campaigns
			fc.utobj().printTestStep("Create Status Driven campaign 1 at Campaign Center > Campaigns");
			String secondcampaignNamestatus = fc.utobj().generateTestData(dataSet.get("CorpcampaignName"));
			String publictocorporateusersaccessibilitystatus = dataSet.get("Corpaccessibilitypublictoallcorporateusers");
			String secondcampaignTypestatus = dataSet.get("StatusCampaigntype");
			String secondtemplateNamestatus = fc.utobj().generateTestData(dataSet.get("CorptemplateName"));
			String secondemailSubjectstatus = fc.utobj().generateTestData(dataSet.get("CorpemailSubject"));
			String secondplainTextEditorstatus = fc.utobj().generateTestData(dataSet.get("CorpplainTextEditor"));
			campaignCenterPage.createCampaignGenericwithSqlite(driver, dataSet, secondcampaignNamestatus, publictocorporateusersaccessibilitystatus,
					secondcampaignTypestatus, secondtemplateNamestatus, secondemailSubjectstatus, secondplainTextEditorstatus);
			
			//Create another Status Driven campaign 2 at Campaign Center > Campaigns
			fc.utobj().printTestStep("Create another Status Driven campaign 2 at Campaign Center > Campaigns");
			String thirdcampaignNamestatus = fc.utobj().generateTestData(dataSet.get("CorpcampaignName"));
			String privatecorpaccessibilitystatus = dataSet.get("Corpaccessibilityprivate");
			String thirdcampaignTypestatus = dataSet.get("StatusCampaigntype");
			String thirdtemplateNamestatus = fc.utobj().generateTestData(dataSet.get("CorptemplateName"));
			String thirdemailSubjectstatus = fc.utobj().generateTestData(dataSet.get("CorpemailSubject"));
			String thirdplainTextEditorstatus = fc.utobj().generateTestData(dataSet.get("CorpplainTextEditor"));
			campaignCenterPage.createCampaignGenericwithSqlite(driver, dataSet, thirdcampaignNamestatus, privatecorpaccessibilitystatus,
					thirdcampaignTypestatus, thirdtemplateNamestatus, thirdemailSubjectstatus, thirdplainTextEditorstatus);
			
			fc.utobj().clickElement(driver, pobjcam.viewAllButton);
			
			//Verify Public to all users Campaign created by Corporate user who has privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Public to all users Campaign created by Corporate user who has privilege to see private campaign by itself");
			campaignCenterPage.searchCampaignByFilter(driver, firstcampaignName);

			boolean isCampaignNamePresentCorpaccessibilitypublictoallusers = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firstcampaignName + "')]");
			if (isCampaignNamePresentCorpaccessibilitypublictoallusers == false) {
				fc.utobj().throwsException("was not able to verify Public to all users Campaign created by Corporate user who has privilege to see private campaign by itself");
			}
			//Verify Public to all corporate users Campaign created by Corporate user who has privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Public to all corporate users Campaign created by Corporate user who has privilege to see private campaign");
			campaignCenterPage.searchCampaignByFilter(driver, secondcampaignName);
			
			boolean isCampaignNamePresentCorpaccessibilitypublictoallcorporateusers = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondcampaignName + "')]");
			if (isCampaignNamePresentCorpaccessibilitypublictoallcorporateusers == false) {
				fc.utobj().throwsException("was not able to verify Public to all corporate users Campaign created by Corporate user who has privilege to see private campaign by itself");
			}
			
			//Verify Private Campaign created by Corporate user who has privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Private Campaign created by Corporate user who has privilege to see private campaign by itself");
			campaignCenterPage.searchCampaignByFilter(driver, thirdcampaignName);
			
			boolean isCampaignNamePresentCorpaccessibilityprivate = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + thirdcampaignName + "')]");
			if (isCampaignNamePresentCorpaccessibilityprivate == false) {
				fc.utobj().throwsException("was not able to Private Campaign created by Corporate user who has privilege to see private campaign by itself");
			}
			
			//Verify Public to all users(Status Driven) Campaign created by Corporate user who has privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Public to all users(Status Driven) Campaign created by Corporate user who has privilege to see private campaign by itself");
			campaignCenterPage.searchCampaignByFilter(driver, firstcampaignNamestatus);

			boolean isCampaignNamePresentCorpaccessibilitypublictoallusersstatus = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firstcampaignNamestatus + "')]");
			if (isCampaignNamePresentCorpaccessibilitypublictoallusersstatus == false) {
				fc.utobj().throwsException("was not able to verify Public to all users(Status Driven) Campaign created by Corporate user who has privilege to see private campaign by itself");
			}
			//Verify Public to all corporate users(Status Driven) Campaign created by Corporate user who has privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Public to all corporate users(Status Driven) Campaign created by Corporate user who has privilege to see private campaign");
			campaignCenterPage.searchCampaignByFilter(driver, secondcampaignNamestatus);
			
			boolean isCampaignNamePresentCorpaccessibilitypublictoallcorporateusersstatus = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondcampaignNamestatus + "')]");
			if (isCampaignNamePresentCorpaccessibilitypublictoallcorporateusersstatus == false) {
				fc.utobj().throwsException("was not able to verify Public to all corporate users(Status Driven) Campaign created by Corporate user who has privilege to see private campaign by itself");
			}
			
			//Verify Private Campaign created by Corporate user(Status Driven) who has privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Private Campaign(Status Driven) created by Corporate user who has privilege to see private campaign by itself");
			campaignCenterPage.searchCampaignByFilter(driver, thirdcampaignNamestatus);
			
			boolean isCampaignNamePresentCorpaccessibilityprivatestatus = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + thirdcampaignNamestatus + "')]");
			if (isCampaignNamePresentCorpaccessibilityprivatestatus == false) {
				fc.utobj().throwsException("was not able to Private Campaign(Status Driven) created by Corporate user who has privilege to see private campaign by itself");
			}
		
			fc.utobj().printTestStep("Navigate to CRM>Campaign Center>email template");
			CRMCampaignCenterPageTest emailtemplate=new CRMCampaignCenterPageTest();
			emailtemplate.clickonemailtamplate(driver);
			
			//Verify Public to all users template created by Corporate user who has privilege to see private template by itself
			fc.utobj().printTestStep("Verify Public to all users template created by Corporate user who has privilege to see private template by itself");
			campaignCenterPage.searchTemplateByFilter(driver, firsttemplateName);

			boolean isTemplateNamePresentCorpaccessibilitypublictoallusers = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firsttemplateName + "')]");
			if (isTemplateNamePresentCorpaccessibilitypublictoallusers == false) {
				fc.utobj().throwsException("was not able to verify Public to all users template created by Corporate user who has privilege to see private template by itself");
			}
			//Verify Public to all corporate users template created by Corporate user who has privilege to see private template by itself
			fc.utobj().printTestStep("Verify Public to all corporate users template created by Corporate user who has privilege to see private template by itself");
			campaignCenterPage.searchTemplateByFilter(driver, secondtemplateName);
			
			boolean isTemplateNamePresentCorpaccessibilitypublictoallcorporateusers = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondtemplateName + "')]");
			if (isTemplateNamePresentCorpaccessibilitypublictoallcorporateusers == false) {
				fc.utobj().throwsException("was not able to verify Public to all users template created by Corporate user who has privilege to see private template by itself");
			}
			
			//Verify Private template created by Corporate user who has privilege to see private template by itself
			fc.utobj().printTestStep("Verify Privare template created by Corporate user who has privilege to see private template by itself");
			campaignCenterPage.searchTemplateByFilter(driver, thirdtemplateName);
			
			boolean isTemplateNamePresentCorpaccessibilityprivate = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + thirdtemplateName + "')]");
			if (isTemplateNamePresentCorpaccessibilityprivate == false) {
				fc.utobj().throwsException("was not able to verify private template created by Corporate user who has privilege to see private template by itself");
			}
				
			//Verify Public to all users template(Status Driven) created by Corporate user who has privilege to see private template by itself
			fc.utobj().printTestStep("Verify Public to all users template(Status Driven) created by Corporate user who has privilege to see private template by itself");
			campaignCenterPage.searchTemplateByFilter(driver, firsttemplateNamestatus);

			boolean isTemplateNamePresentCorpaccessibilitypublictoallusersstatus = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firsttemplateNamestatus + "')]");
			if (isTemplateNamePresentCorpaccessibilitypublictoallusersstatus == false) {
				fc.utobj().throwsException("was not able to verify Public to all users template(Status Driven) created by Corporate user who has privilege to see private template by itself");
			}
			//Verify Public to all corporate users template(Status Driven) created by Corporate user who has privilege to see private template by itself
			fc.utobj().printTestStep("Verify Public to all corporate users template(Status Driven) created by Corporate user who has privilege to see private template by itself");
			campaignCenterPage.searchTemplateByFilter(driver, secondtemplateNamestatus);
			
			boolean isTemplateNamePresentCorpaccessibilitypublictoallcorporateusersstatus = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondtemplateNamestatus + "')]");
			if (isTemplateNamePresentCorpaccessibilitypublictoallcorporateusersstatus == false) {
				fc.utobj().throwsException("was not able to verify Public to all users template(Status Driven) created by Corporate user who has privilege to see private template by itself");
			}
			
			//Verify Private template(Status Driven) created by Corporate user who has privilege to see private template by itself
			fc.utobj().printTestStep("Verify Privare template(Status Driven) created by Corporate user who has privilege to see private template by itself");
			campaignCenterPage.searchTemplateByFilter(driver, thirdtemplateNamestatus);
			
			boolean isTemplateNamePresentCorpaccessibilityprivatestatus = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + thirdtemplateNamestatus + "')]");
			if (isTemplateNamePresentCorpaccessibilityprivatestatus == false) {
				fc.utobj().throwsException("was not able to verify private template(Status Driven) created by Corporate user who has privilege to see private template by itself");
			}
			
			//Login with Corporate user who does not have role to see private campaign(C2)
			fc.utobj().printTestStep("Login with Corporate user who does not have role to see private campaign(C2)");
			fc.loginpage().loginWithParameter(driver, CorpUserName1, CorpUserPassword1);
			fc.crm().crm_common().openCRMCampaignCenterPage(fcHomePageTest, driver);
		
			//Create Campaign At Campaign Center > Campaigns
			fc.utobj().printTestStep("Create Campaign At Campaign Center > Campaigns");
			String firstcampaignNamecorp = fc.utobj().generateTestData(dataSet.get("CorpcampaignName"));
			String firstaccessibilitycorp = dataSet.get("Corpaccessibilitypublictoallusers");
			String firstcampaignTypecorp = dataSet.get("CorpcampaignType");
			String firsttemplateNamecorp = fc.utobj().generateTestData(dataSet.get("CorptemplateName"));
			String firstemailSubjectcorp = fc.utobj().generateTestData(dataSet.get("CorpemailSubject"));
			String firstplainTextEditorcorp = fc.utobj().generateTestData(dataSet.get("CorpplainTextEditor"));
			campaignCenterPage.createCampaignGenericwithSqlite(driver, dataSet, firstcampaignNamecorp, firstaccessibilitycorp,
					firstcampaignTypecorp, firsttemplateNamecorp, firstemailSubjectcorp, firstplainTextEditorcorp);
			
			//Create another campaign at Campaign Center > Campaigns
			fc.utobj().printTestStep("Create another campaign at Campaign Center > Campaigns");
			String secondcampaignNamecorp = fc.utobj().generateTestData(dataSet.get("CorpcampaignName"));
			String publictocorporateusersaccessibilitycorp = dataSet.get("Corpaccessibilitypublictoallcorporateusers");
			String secondcampaignTypecorp = dataSet.get("CorpcampaignType");
			String secondtemplateNamecorp = fc.utobj().generateTestData(dataSet.get("CorptemplateName"));
			String secondemailSubjectcorp = fc.utobj().generateTestData(dataSet.get("CorpemailSubject"));
			String secondplainTextEditorcorp = fc.utobj().generateTestData(dataSet.get("CorpplainTextEditor"));
			campaignCenterPage.createCampaignGenericwithSqlite(driver, dataSet, secondcampaignNamecorp, publictocorporateusersaccessibilitycorp,
					secondcampaignTypecorp, secondtemplateNamecorp, secondemailSubjectcorp, secondplainTextEditorcorp);
			
			//Create another campaign1 at Campaign Center > Campaigns
			fc.utobj().printTestStep("Create another campaign at Campaign Center > Campaigns");
			String thirdcampaignNamecorp = fc.utobj().generateTestData(dataSet.get("CorpcampaignName"));
			String privatecorpaccessibilitycorp = dataSet.get("Corpaccessibilityprivate");
			String thirdcampaignTypecorp = dataSet.get("CorpcampaignType");
			String thirdtemplateNamecorp = fc.utobj().generateTestData(dataSet.get("CorptemplateName"));
			String thirdemailSubjectcorp = fc.utobj().generateTestData(dataSet.get("CorpemailSubject"));
			String thirdplainTextEditorcorp = fc.utobj().generateTestData(dataSet.get("CorpplainTextEditor"));
			campaignCenterPage.createCampaignGenericwithSqlite(driver, dataSet, thirdcampaignNamecorp, privatecorpaccessibilitycorp,
					thirdcampaignTypecorp, thirdtemplateNamecorp, thirdemailSubjectcorp, thirdplainTextEditorcorp);
			
			
			
			//Create Status Driven Campaign At Campaign Center > Campaigns
			fc.utobj().printTestStep("Create Status Driven Campaign At Campaign Center > Campaigns");
			String firstcampaignNamecorpstatus = fc.utobj().generateTestData(dataSet.get("CorpcampaignName"));
			String firstaccessibilitycorpstatus = dataSet.get("Corpaccessibilitypublictoallusers");
			String firstcampaignTypecorpstatus = dataSet.get("StatusCampaigntype");
			String firsttemplateNamecorpstatus = fc.utobj().generateTestData(dataSet.get("CorptemplateName"));
			String firstemailSubjectcorpstatus = fc.utobj().generateTestData(dataSet.get("CorpemailSubject"));
			String firstplainTextEditorcorpstatus = fc.utobj().generateTestData(dataSet.get("CorpplainTextEditor"));
			campaignCenterPage.createCampaignGenericwithSqlite(driver, dataSet, firstcampaignNamecorpstatus, firstaccessibilitycorpstatus,
					firstcampaignTypecorpstatus, firsttemplateNamecorpstatus, firstemailSubjectcorpstatus, firstplainTextEditorcorpstatus);
			
			//Create Status Driven campaign 1 at Campaign Center > Campaigns
			fc.utobj().printTestStep("Create Status Driven campaign 1 at Campaign Center > Campaigns");
			String secondcampaignNamecorpstatus = fc.utobj().generateTestData(dataSet.get("CorpcampaignName"));
			String publictocorporateusersaccessibilitycorpstatus = dataSet.get("Corpaccessibilitypublictoallcorporateusers");
			String secondcampaignTypecorpstatus = dataSet.get("StatusCampaigntype");
			String secondtemplateNamecorpstatus = fc.utobj().generateTestData(dataSet.get("CorptemplateName"));
			String secondemailSubjectcorpstatus = fc.utobj().generateTestData(dataSet.get("CorpemailSubject"));
			String secondplainTextEditorcorpstatus = fc.utobj().generateTestData(dataSet.get("CorpplainTextEditor"));
			campaignCenterPage.createCampaignGenericwithSqlite(driver, dataSet, secondcampaignNamecorpstatus, publictocorporateusersaccessibilitycorpstatus,
					secondcampaignTypecorpstatus, secondtemplateNamecorpstatus, secondemailSubjectcorpstatus, secondplainTextEditorcorpstatus);
			
			//Create Status Driver campaign 2 at Campaign Center > Campaigns
			fc.utobj().printTestStep("Create Status Driver campaign 2 at Campaign Center > Campaigns");
			String thirdcampaignNamecorpstatus = fc.utobj().generateTestData(dataSet.get("CorpcampaignName"));
			String privatecorpaccessibilitycorpstatus = dataSet.get("Corpaccessibilityprivate");
			String thirdcampaignTypecorpstatus = dataSet.get("StatusCampaigntype");
			String thirdtemplateNamecorpstatus = fc.utobj().generateTestData(dataSet.get("CorptemplateName"));
			String thirdemailSubjectcorpstatus = fc.utobj().generateTestData(dataSet.get("CorpemailSubject"));
			String thirdplainTextEditorcorpstatus = fc.utobj().generateTestData(dataSet.get("CorpplainTextEditor"));
			campaignCenterPage.createCampaignGenericwithSqlite(driver, dataSet, thirdcampaignNamecorpstatus, privatecorpaccessibilitycorpstatus,
					thirdcampaignTypecorpstatus, thirdtemplateNamecorpstatus, thirdemailSubjectcorpstatus, thirdplainTextEditorcorpstatus);
		
			fc.utobj().clickElement(driver, pobjcam.viewAllButton);
			
			//Verify Public to all users Campaign created by Corporate user who does not have privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Public to all users Campaign created by Corporate user who does not have privilege to see private campaign by itself");
			campaignCenterPage.searchCampaignByFilter(driver, firstcampaignNamecorp);

			boolean isCampaignNamePresentCorpaccessibilitypublictoalluserscorp = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firstcampaignNamecorp + "')]");
			if (isCampaignNamePresentCorpaccessibilitypublictoalluserscorp == false) {
				fc.utobj().throwsException("was not able to verify Public to all users Campaign created by Corporate user who does not have privilege to see private campaign by itself");
			}
			//Verify Public to all corporate users Campaign created by Corporate user who has privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Public to all corporate users Campaign created by Corporate user who does not have privilege to see private campaign");
			campaignCenterPage.searchCampaignByFilter(driver, secondcampaignNamecorp);
			
			boolean isCampaignNamePresentCorpaccessibilitypublictoallcorporateuserscorp = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondcampaignNamecorp + "')]");
			if (isCampaignNamePresentCorpaccessibilitypublictoallcorporateuserscorp == false) {
				fc.utobj().throwsException("was not able to verify Public to all corporate users Campaign created by Corporate user who does not privilege to see private campaign by itself");
			}
			
			//Verify Private Campaign created by Corporate user who has privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Private Campaign created by Corporate user who does not have privilege to see private campaign by itself");
			campaignCenterPage.searchCampaignByFilter(driver, thirdcampaignNamecorp);
			
			boolean isCampaignNamePresentCorpaccessibilityprivatecorp = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + thirdcampaignNamecorp + "')]");
			if (isCampaignNamePresentCorpaccessibilityprivatecorp == false) {
				fc.utobj().throwsException("was not able to Private Campaign created by Corporate user who does not have privilege to see private campaign by itself");
			}
			
			
			//Verify Public to all users (Status Driven)Campaign created by Corporate user who does not have privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Public to all users(Status Driven) Campaign created by Corporate user who does not have privilege to see private campaign by itself");
			campaignCenterPage.searchCampaignByFilter(driver, firstcampaignNamecorpstatus);

			boolean isCampaignNamePresentCorpaccessibilitypublictoalluserscorpstatus = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firstcampaignNamecorpstatus + "')]");
			if (isCampaignNamePresentCorpaccessibilitypublictoalluserscorpstatus == false) {
				fc.utobj().throwsException("was not able to verify Public to all users(Status Driven) Campaign created by Corporate user who does not have privilege to see private campaign by itself");
			}
			//Verify Public to all corporate users Campaign created by Corporate user who has privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Public to all corporate users(Status Driven) Campaign created by Corporate user who does not have privilege to see private campaign");
			campaignCenterPage.searchCampaignByFilter(driver, secondcampaignNamecorpstatus);
			
			boolean isCampaignNamePresentCorpaccessibilitypublictoallcorporateuserscorpstatus = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondcampaignNamecorpstatus + "')]");
			if (isCampaignNamePresentCorpaccessibilitypublictoallcorporateuserscorpstatus == false) {
				fc.utobj().throwsException("was not able to verify Public to all corporate users(Status Driven) Campaign created by Corporate user who does not privilege to see private campaign by itself");
			}
			
			//Verify Private Campaign created by Corporate user who has privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Private Campaign created by Corporate user(Status Driven) who does not have privilege to see private campaign by itself");
			campaignCenterPage.searchCampaignByFilter(driver, thirdcampaignNamecorpstatus);
			
			boolean isCampaignNamePresentCorpaccessibilityprivatecorpstatus = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + thirdcampaignNamecorpstatus + "')]");
			if (isCampaignNamePresentCorpaccessibilityprivatecorpstatus == false) {
				fc.utobj().throwsException("was not able to Private Campaign created by Corporate user(Status Driven) who does not have privilege to see private campaign by itself");
			}
				
			fc.utobj().printTestStep("Navigate to CRM>Campaign Center>email template");
			emailtemplate.clickonemailtamplate(driver);
			
			//Verify Public to all users template created by Corporate user who does not have privilege to see private template by itself
			fc.utobj().printTestStep("Verify Public to all users template created by Corporate user who does not have privilege to see private template by itself");
			campaignCenterPage.searchTemplateByFilter(driver, firsttemplateNamecorp);

			boolean isTemplateNamePresentCorpaccessibilitypublictoalluserscorp = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firsttemplateNamecorp + "')]");
			if (isTemplateNamePresentCorpaccessibilitypublictoalluserscorp == false) {
				fc.utobj().throwsException("was not able to verify Public to all users template created by Corporate user who has privilege to see private template by itself");
			}
			//Verify Public to all corporate users template created by Corporate user who has privilege to see private template by itself
			fc.utobj().printTestStep("Verify Public to all corporate users template created by Corporate user who does not have privilege to see private template by itself");
			campaignCenterPage.searchTemplateByFilter(driver, secondtemplateNamecorp);
			
			boolean isTemplateNamePresentCorpaccessibilitypublictoallcorporateuserscorp = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondtemplateNamecorp + "')]");
			if (isTemplateNamePresentCorpaccessibilitypublictoallcorporateuserscorp == false) {
				fc.utobj().throwsException("was not able to verify Public to all users template created by Corporate user who has privilege to see private template by itself");
			}
			
			//Verify Private template created by Corporate user who has privilege to see private template by itself
			fc.utobj().printTestStep("Verify Privare template created by Corporate user who does not have privilege to see private template by itself");
			campaignCenterPage.searchTemplateByFilter(driver, thirdtemplateNamecorp);
			
			boolean isTemplateNamePresentCorpaccessibilityprivatecorp = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + thirdtemplateNamecorp + "')]");
			if (isTemplateNamePresentCorpaccessibilityprivatecorp == false) {
				fc.utobj().throwsException("was not able to verify private template created by Corporate user who has privilege to see private template by itself");
			}
			
			
			//Verify Public to all users (Status driven)template created by Corporate user who does not have privilege to see private template by itself
			fc.utobj().printTestStep("Verify Public to all users (Status driven)template created by Corporate user who does not have privilege to see private template by itself");
			campaignCenterPage.searchTemplateByFilter(driver, firsttemplateNamecorpstatus);

			boolean isTemplateNamePresentCorpaccessibilitypublictoalluserscorpstatus = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firsttemplateNamecorpstatus + "')]");
			if (isTemplateNamePresentCorpaccessibilitypublictoalluserscorpstatus == false) {
				fc.utobj().throwsException("was not able to verify Public to all users (Status driven)template created by Corporate user who has privilege to see private template by itself");
			}
			//Verify Public to all corporate users template created by Corporate user who has privilege to see private template by itself
			fc.utobj().printTestStep("Verify Public to all corporate users (Status driven)template created by Corporate user who does not have privilege to see private template by itself");
			campaignCenterPage.searchTemplateByFilter(driver, secondtemplateNamecorpstatus);
			
			boolean isTemplateNamePresentCorpaccessibilitypublictoallcorporateuserscorpstatus = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondtemplateNamecorpstatus + "')]");
			if (isTemplateNamePresentCorpaccessibilitypublictoallcorporateuserscorpstatus == false) {
				fc.utobj().throwsException("was not able to verify Public to all users (Status driven)template created by Corporate user who has privilege to see private template by itself");
			}
			
			//Verify Private template created by Corporate user who has privilege to see private template by itself
			fc.utobj().printTestStep("Verify Privare (Status driven)template created by Corporate user who does not have privilege to see private template by itself");
			campaignCenterPage.searchTemplateByFilter(driver, thirdtemplateNamecorpstatus);
			
			boolean isTemplateNamePresentCorpaccessibilityprivatecorpstatus = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + thirdtemplateNamecorpstatus + "')]");
			if (isTemplateNamePresentCorpaccessibilityprivatecorpstatus == false) {
				fc.utobj().throwsException("was not able to verify private (Status driven)template created by Corporate user who has privilege to see private template by itself");
			}
			
			
			//Login with Regional user who has role to see private campaign(R1)
			fc.utobj().printTestStep("Login with Regional user who has role to see private campaign(R1)");
			fc.loginpage().loginWithParameter(driver, Reguname, Regpassword);
			fc.crm().crm_common().openCRMCampaignCenterPage(fcHomePageTest, driver);
			
			//Create Campaign At Campaign Center > Campaigns
			fc.utobj().printTestStep("Create Campaign At Campaign Center > Campaigns");
			String firstcampaignNamereg = fc.utobj().generateTestData(dataSet.get("CorpcampaignName"));
			String firstaccessibilityreg = dataSet.get("Regaccessibilitypublictoallusersofmyregion");
			String firstcampaignTypereg = dataSet.get("CorpcampaignType");
			String firsttemplateNamereg = fc.utobj().generateTestData(dataSet.get("CorptemplateName"));
			String firstemailSubjectreg = fc.utobj().generateTestData(dataSet.get("CorpemailSubject"));
			String firstplainTextEditorreg = fc.utobj().generateTestData(dataSet.get("CorpplainTextEditor"));
			campaignCenterPage.createCampaignGenericwithSqlite(driver, dataSet, firstcampaignNamereg, firstaccessibilityreg,
					firstcampaignTypereg, firsttemplateNamereg, firstemailSubjectreg, firstplainTextEditorreg);
			
			//Create another campaign at Campaign Center > Campaigns
			fc.utobj().printTestStep("Create another campaign at Campaign Center > Campaigns");
			String secondcampaignNamereg = fc.utobj().generateTestData(dataSet.get("CorpcampaignName"));
			String privateaccessibilityreg = dataSet.get("Regaccessibilityprivate");
			String secondcampaignTypereg = dataSet.get("CorpcampaignType");
			String secondtemplateNamereg = fc.utobj().generateTestData(dataSet.get("CorptemplateName"));
			String secondemailSubjectreg = fc.utobj().generateTestData(dataSet.get("CorpemailSubject"));
			String secondplainTextEditorreg = fc.utobj().generateTestData(dataSet.get("CorpplainTextEditor"));
			campaignCenterPage.createCampaignGenericwithSqlite(driver, dataSet, secondcampaignNamereg, privateaccessibilityreg,
					secondcampaignTypereg, secondtemplateNamereg, secondemailSubjectreg, secondplainTextEditorreg);
			
			//Create Status Driven Campaign At Campaign Center > Campaigns
			fc.utobj().printTestStep("Create Status Driven Campaign At Campaign Center > Campaigns");
			String firstcampaignNameregstatus = fc.utobj().generateTestData(dataSet.get("CorpcampaignName"));
			String firstaccessibilityregstatus = dataSet.get("Regaccessibilitypublictoallusersofmyregion");
			String firstcampaignTyperegstatus = dataSet.get("StatusCampaigntype");
			String firsttemplateNameregstatus = fc.utobj().generateTestData(dataSet.get("CorptemplateName"));
			String firstemailSubjectregstatus = fc.utobj().generateTestData(dataSet.get("CorpemailSubject"));
			String firstplainTextEditorregstatus = fc.utobj().generateTestData(dataSet.get("CorpplainTextEditor"));
			campaignCenterPage.createCampaignGenericwithSqlite(driver, dataSet, firstcampaignNameregstatus, firstaccessibilityregstatus,
					firstcampaignTyperegstatus, firsttemplateNameregstatus, firstemailSubjectregstatus, firstplainTextEditorregstatus);
			
			//Create Status driven campaign 1 at Campaign Center > Campaigns
			fc.utobj().printTestStep("Create Status Driven campaign 1 at Campaign Center > Campaigns");
			String secondcampaignNameregstatus = fc.utobj().generateTestData(dataSet.get("CorpcampaignName"));
			String privateaccessibilityregstatus = dataSet.get("Regaccessibilityprivate");
			String secondcampaignTyperegstatus = dataSet.get("StatusCampaigntype");
			String secondtemplateNameregstatus = fc.utobj().generateTestData(dataSet.get("CorptemplateName"));
			String secondemailSubjectregstatus = fc.utobj().generateTestData(dataSet.get("CorpemailSubject"));
			String secondplainTextEditorregstatus = fc.utobj().generateTestData(dataSet.get("CorpplainTextEditor"));
			campaignCenterPage.createCampaignGenericwithSqlite(driver, dataSet, secondcampaignNameregstatus, privateaccessibilityregstatus,
					secondcampaignTyperegstatus, secondtemplateNameregstatus, secondemailSubjectregstatus, secondplainTextEditorregstatus);
			
			fc.utobj().clickElement(driver, pobjcam.viewAllButton);
			
			//Verify Public to all users of my region Campaign created by Regional user who has privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Public to all users of my region Campaign created by Regional user who has privilege to see private campaign by itself");
			campaignCenterPage.searchCampaignByFilter(driver, firstcampaignNamereg);

			boolean isCampaignNamePresentRegpaccessibilitypublictoallusersofmyregion = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firstcampaignNamereg + "')]");
			if (isCampaignNamePresentRegpaccessibilitypublictoallusersofmyregion == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my region Campaign created by Regional user who has privilege to see private campaign by itself");
			}
			
			//Verify Private Campaign created by Regional user who has privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Private Campaign created by Regional user who has privilege to see private campaign by itself");
			campaignCenterPage.searchCampaignByFilter(driver, secondcampaignNamereg);
			
			boolean isCampaignNamePresentRegpaccessibilityprivate = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondcampaignNamereg + "')]");
			if (isCampaignNamePresentRegpaccessibilityprivate == false) {
				fc.utobj().throwsException("was not able to verify Private Campaign created by Regional user who has privilege to see private campaign by itself");
			}
			
			//Verify Public to all users of my region (Status Driven)Campaign created by Regional user who has privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Public to all users of my region (Status Driven)Campaign created by Regional user who has privilege to see private campaign by itself");
			campaignCenterPage.searchCampaignByFilter(driver, firstcampaignNameregstatus);

			boolean isCampaignNamePresentRegpaccessibilitypublictoallusersofmyregionstatus = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firstcampaignNameregstatus + "')]");
			if (isCampaignNamePresentRegpaccessibilitypublictoallusersofmyregionstatus == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my region (Status Driven)Campaign created by Regional user who has privilege to see private campaign by itself");
			}
			
			//Verify Private Campaign created by Regional user who has privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Private (Status Driven)Campaign created by Regional user who has privilege to see private campaign by itself");
			campaignCenterPage.searchCampaignByFilter(driver, secondcampaignNameregstatus);
			
			boolean isCampaignNamePresentRegpaccessibilityprivatestatus = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondcampaignNameregstatus + "')]");
			if (isCampaignNamePresentRegpaccessibilityprivatestatus == false) {
				fc.utobj().throwsException("was not able to verify Private (Status Driven)Campaign created by Regional user who has privilege to see private campaign by itself");
			}
			
			fc.utobj().printTestStep("Navigate to CRM>Campaign Center>email template");
			emailtemplate.clickonemailtamplate(driver);
			
			//Verify Public to all users template created by Corporate user who has privilege to see private template by itself
			fc.utobj().printTestStep("Verify Public to all users of my region template created by Regional user who has privilege to see private template by itself");
			campaignCenterPage.searchTemplateByFilter(driver, firsttemplateNamereg);

			boolean isTemplateNamePresentRegaccessibilitypublictoallusersofmyregion = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firsttemplateNamereg + "')]");
			if (isTemplateNamePresentRegaccessibilitypublictoallusersofmyregion == false) {
				fc.utobj().throwsException("was not able to verify Public to all users template created by Regional user who has privilege to see private template by itself");
			}
			
			//Verify Private template created by Regional user who has privilege to see private template by itself
			fc.utobj().printTestStep("Verify Private template created by Regional user who has privilege to see private template by itself");
			campaignCenterPage.searchTemplateByFilter(driver, secondtemplateNamereg);
			
			boolean isTemplateNamePresentRegaccessibilityprivate = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondtemplateNamereg + "')]");
			if (isTemplateNamePresentRegaccessibilityprivate == false) {
				fc.utobj().throwsException("was not able to verify private template created by Regional user who has privilege to see private template by itself");
			}
			
			//Verify Public to all users(Status Driven) template created by Regional user who has privilege to see private template by itself
			fc.utobj().printTestStep("Verify Public to all users of my region (Status Driven)template created by Regional user who has privilege to see private template by itself");
			campaignCenterPage.searchTemplateByFilter(driver, firsttemplateNameregstatus);

			boolean isTemplateNamePresentRegaccessibilitypublictoallusersofmyregionstatus = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firsttemplateNameregstatus + "')]");
			if (isTemplateNamePresentRegaccessibilitypublictoallusersofmyregionstatus == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my region (Status Driven)template created by Regional user who has privilege to see private template by itself");
			}
			
			//Verify Private template created by Regional user who has privilege to see private template by itself
			fc.utobj().printTestStep("Verify Private (Status Driven)template created by Regional user who has privilege to see private template by itself");
			campaignCenterPage.searchTemplateByFilter(driver, secondtemplateNameregstatus);
			
			boolean isTemplateNamePresentRegaccessibilityprivatestatus = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondtemplateNameregstatus + "')]");
			if (isTemplateNamePresentRegaccessibilityprivatestatus == false) {
				fc.utobj().throwsException("was not able to verify private (Status Driven)template created by Regional user who has privilege to see private template by itself");
			}
			
			
			//Login with Regional user who does not have role to see private campaign(R2)
			fc.utobj().printTestStep("Login with Regional user who does not have role to see private campaign(R2)");
			fc.loginpage().loginWithParameter(driver, Reguname1, Regpassword1);
			fc.crm().crm_common().openCRMCampaignCenterPage(fcHomePageTest, driver);
			
			//Create Campaign At Campaign Center > Campaigns
			fc.utobj().printTestStep("Create Campaign At Campaign Center > Campaigns");
			String firstcampaignNamereg1 = fc.utobj().generateTestData(dataSet.get("CorpcampaignName"));
			String firstaccessibilityreg1 = dataSet.get("Regaccessibilitypublictoallusersofmyregion");
			String firstcampaignTypereg1 = dataSet.get("CorpcampaignType");
			String firsttemplateNamereg1 = fc.utobj().generateTestData(dataSet.get("CorptemplateName"));
			String firstemailSubjectreg1= fc.utobj().generateTestData(dataSet.get("CorpemailSubject"));
			String firstplainTextEditorreg1 = fc.utobj().generateTestData(dataSet.get("CorpplainTextEditor"));
			campaignCenterPage.createCampaignGenericwithSqlite(driver, dataSet, firstcampaignNamereg1, firstaccessibilityreg1,
					firstcampaignTypereg1, firsttemplateNamereg1, firstemailSubjectreg1, firstplainTextEditorreg1);
			
			//Create another campaign at Campaign Center > Campaigns
			fc.utobj().printTestStep("Create another campaign at Campaign Center > Campaigns");
			String secondcampaignNamereg1 = fc.utobj().generateTestData(dataSet.get("CorpcampaignName"));
			String privateaccessibilityreg1 = dataSet.get("Regaccessibilityprivate");
			String secondcampaignTypereg1 = dataSet.get("CorpcampaignType");
			String secondtemplateNamereg1 = fc.utobj().generateTestData(dataSet.get("CorptemplateName"));
			String secondemailSubjectreg1 = fc.utobj().generateTestData(dataSet.get("CorpemailSubject"));
			String secondplainTextEditorreg1 = fc.utobj().generateTestData(dataSet.get("CorpplainTextEditor"));
			campaignCenterPage.createCampaignGenericwithSqlite(driver, dataSet, secondcampaignNamereg1, privateaccessibilityreg1,
					secondcampaignTypereg1, secondtemplateNamereg1, secondemailSubjectreg1, secondplainTextEditorreg1);
			
			//Create Status Driven Campaign At Campaign Center > Campaigns
			fc.utobj().printTestStep("Create Status Driven Campaign At Campaign Center > Campaigns");
			String firstcampaignNamereg1status = fc.utobj().generateTestData(dataSet.get("CorpcampaignName"));
			String firstaccessibilityreg1status = dataSet.get("Regaccessibilitypublictoallusersofmyregion");
			String firstcampaignTypereg1status = dataSet.get("StatusCampaigntype");
			String firsttemplateNamereg1status = fc.utobj().generateTestData(dataSet.get("CorptemplateName"));
			String firstemailSubjectreg1status= fc.utobj().generateTestData(dataSet.get("CorpemailSubject"));
			String firstplainTextEditorreg1status = fc.utobj().generateTestData(dataSet.get("CorpplainTextEditor"));
			campaignCenterPage.createCampaignGenericwithSqlite(driver, dataSet, firstcampaignNamereg1status, firstaccessibilityreg1status,
					firstcampaignTypereg1status, firsttemplateNamereg1status, firstemailSubjectreg1status, firstplainTextEditorreg1status);
			
			//Create Status Driven campaign at Campaign Center > Campaigns
			fc.utobj().printTestStep("Create Status Driven campaign at Campaign Center > Campaigns");
			String secondcampaignNamereg1status = fc.utobj().generateTestData(dataSet.get("CorpcampaignName"));
			String privateaccessibilityreg1status = dataSet.get("Regaccessibilityprivate");
			String secondcampaignTypereg1status = dataSet.get("StatusCampaigntype");
			String secondtemplateNamereg1status = fc.utobj().generateTestData(dataSet.get("CorptemplateName"));
			String secondemailSubjectreg1status = fc.utobj().generateTestData(dataSet.get("CorpemailSubject"));
			String secondplainTextEditorreg1status = fc.utobj().generateTestData(dataSet.get("CorpplainTextEditor"));
			campaignCenterPage.createCampaignGenericwithSqlite(driver, dataSet, secondcampaignNamereg1status, privateaccessibilityreg1status,
					secondcampaignTypereg1status, secondtemplateNamereg1status, secondemailSubjectreg1status, secondplainTextEditorreg1status);
			
			fc.utobj().clickElement(driver, pobjcam.viewAllButton);
			
			//Verify Public to all users of my region Campaign created by Regional user who does not have privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Public to all users of my region Campaign created by Regional user who does not have privilege to see private campaign by itself");
			campaignCenterPage.searchCampaignByFilter(driver, firstcampaignNamereg1);

			boolean isCampaignNamePresentRegpaccessibilitypublictoallusersofmyregion1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firstcampaignNamereg1 + "')]");
			if (isCampaignNamePresentRegpaccessibilitypublictoallusersofmyregion1 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my region Campaign created by Regional user who does not have privilege to see private campaign by itself");
			}
			
			//Verify Private Campaign created by Regional user who does not have privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Private Campaign created by Regional user who does not have privilege to see private campaign by itself");
			campaignCenterPage.searchCampaignByFilter(driver, secondcampaignNamereg1);
			
			boolean isCampaignNamePresentRegpaccessibilityprivate1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondcampaignNamereg1 + "')]");
			if (isCampaignNamePresentRegpaccessibilityprivate1 == false) {
				fc.utobj().throwsException("was not able to verify Private Campaign created by Regional user who does not have privilege to see private campaign by itself");
			}
			
			//Verify Public to all users of my region (Status Driven)Campaign created by Regional user who does not have privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Public to all users of my region (Status Driven)Campaign created by Regional user who does not have privilege to see private campaign by itself");
			campaignCenterPage.searchCampaignByFilter(driver, firstcampaignNamereg1status);

			boolean isCampaignNamePresentRegpaccessibilitypublictoallusersofmyregion1status = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firstcampaignNamereg1status + "')]");
			if (isCampaignNamePresentRegpaccessibilitypublictoallusersofmyregion1status == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my region (Status Driven)Campaign created by Regional user who does not have privilege to see private campaign by itself");
			}
			
			//Verify Private (Status Driven)Campaign created by Regional user who does not have privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Private (Status Driven)Campaign created by Regional user who does not have privilege to see private campaign by itself");
			campaignCenterPage.searchCampaignByFilter(driver, secondcampaignNamereg1status);
			
			boolean isCampaignNamePresentRegpaccessibilityprivate1status = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondcampaignNamereg1status + "')]");
			if (isCampaignNamePresentRegpaccessibilityprivate1status == false) {
				fc.utobj().throwsException("was not able to verify Private (Status Driven)Campaign created by Regional user who does not have privilege to see private campaign by itself");
			}
		
			fc.utobj().printTestStep("Navigate to CRM>Campaign Center>email template");
			emailtemplate.clickonemailtamplate(driver);
			
			//Verify Public to all users template created by Corporate user who does not have privilege to see private template by itself
			fc.utobj().printTestStep("Verify Public to all users of my region template created by Regional user who does not have privilege to see private template by itself");
			campaignCenterPage.searchTemplateByFilter(driver, firsttemplateNamereg1);

			boolean isTemplateNamePresentRegaccessibilitypublictoallusersofmyregion1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firsttemplateNamereg1 + "')]");
			if (isTemplateNamePresentRegaccessibilitypublictoallusersofmyregion1 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my region template created by Regional user who does not have privilege to see private template by itself");
			}
			
			//Verify Private template created by Regional user who does not have privilege to see private template by itself
			fc.utobj().printTestStep("Verify Private template created by Regional user does not have privilege to see private template by itself");
			campaignCenterPage.searchTemplateByFilter(driver, secondtemplateNamereg1);
			
			boolean isTemplateNamePresentRegaccessibilityprivate1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondtemplateNamereg1 + "')]");
			if (isTemplateNamePresentRegaccessibilityprivate1 == false) {
				fc.utobj().throwsException("was not able to verify Private template created by Regional user does not have privilege to see private template by itself");
			}
			
			//Verify Public to all users (Status Driven)template created by Regional user who does not have privilege to see private template by itself
			fc.utobj().printTestStep("Verify Public to all users of my region (Status Driven)template created by Regional user who does not have privilege to see private template by itself");
			campaignCenterPage.searchTemplateByFilter(driver, firsttemplateNamereg1status);

			boolean isTemplateNamePresentRegaccessibilitypublictoallusersofmyregion1status = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firsttemplateNamereg1status + "')]");
			if (isTemplateNamePresentRegaccessibilitypublictoallusersofmyregion1status == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my region (Status Driven)template created by Regional user who does not have privilege to see private template by itself");
			}
			
			//Verify Private template created by Regional user who does not have privilege to see private template by itself
			fc.utobj().printTestStep("Verify Private (Status Driven)template created by Regional user does not have privilege to see private template by itself");
			campaignCenterPage.searchTemplateByFilter(driver, secondtemplateNamereg1status);
			
			boolean isTemplateNamePresentRegaccessibilityprivate1status = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondtemplateNamereg1status + "')]");
			if (isTemplateNamePresentRegaccessibilityprivate1status == false) {
				fc.utobj().throwsException("was not able to verify Private (Status Driven)template created by Regional user does not have privilege to see private template by itself");
			}
			
			
			//Login with Franchise Owner user(FO1) who has role to see private campaign
			fc.utobj().printTestStep("Login with Franchise Owner user(FO1) who has role to see private campaign");
			fc.loginpage().loginWithParameter(driver, userNameFO, passwordFO);
			fc.crm().crm_common().openCRMCampaignCenterPage(fcHomePageTest, driver);
			
			//Create Campaign At Campaign Center > Campaigns
			fc.utobj().printTestStep("Create Campaign At Campaign Center > Campaigns");
			String firstcampaignNamefran = fc.utobj().generateTestData(dataSet.get("CorpcampaignName"));
			String firstaccessibilityfran = dataSet.get("FranaccessibilityPublictoallUsersofmyFranchise");
			String firstcampaignTypefran = dataSet.get("CorpcampaignType");
			String firsttemplateNamefran = fc.utobj().generateTestData(dataSet.get("CorptemplateName"));
			String firstemailSubjectfran = fc.utobj().generateTestData(dataSet.get("CorpemailSubject"));
			String firstplainTextEditorfran = fc.utobj().generateTestData(dataSet.get("CorpplainTextEditor"));
			campaignCenterPage.createCampaignGenericwithSqlite(driver, dataSet, firstcampaignNamefran, firstaccessibilityfran,
					firstcampaignTypefran, firsttemplateNamefran, firstemailSubjectfran, firstplainTextEditorfran);
			
			
			//Create another campaign at Campaign Center > Campaigns
			fc.utobj().printTestStep("Create another campaign at Campaign Center > Campaigns");
			String secondcampaignNamefran = fc.utobj().generateTestData(dataSet.get("CorpcampaignName"));
			String privateaccessibilityfran = dataSet.get("Franaccessibilityprivate");
			String secondcampaignTypefran = dataSet.get("CorpcampaignType");
			String secondtemplateNamefran = fc.utobj().generateTestData(dataSet.get("CorptemplateName"));
			String secondemailSubjectfran = fc.utobj().generateTestData(dataSet.get("CorpemailSubject"));
			String secondplainTextEditorfran = fc.utobj().generateTestData(dataSet.get("CorpplainTextEditor"));
			campaignCenterPage.createCampaignGenericwithSqlite(driver, dataSet, secondcampaignNamefran, privateaccessibilityfran,
					secondcampaignTypefran, secondtemplateNamefran, secondemailSubjectfran, secondplainTextEditorfran);
			
			//Create Status Driven campaign At Campaign Center > Campaigns
			fc.utobj().printTestStep("Create Status Driven campaign At Campaign Center > Campaigns");
			String firstcampaignNamefranstatus = fc.utobj().generateTestData(dataSet.get("CorpcampaignName"));
			String firstaccessibilityfranstatus = dataSet.get("FranaccessibilityPublictoallUsersofmyFranchise");
			String firstcampaignTypefranstatus = dataSet.get("StatusCampaigntype");
			String firsttemplateNamefranstatus = fc.utobj().generateTestData(dataSet.get("CorptemplateName"));
			String firstemailSubjectfranstatus = fc.utobj().generateTestData(dataSet.get("CorpemailSubject"));
			String firstplainTextEditorfranstatus = fc.utobj().generateTestData(dataSet.get("CorpplainTextEditor"));
			campaignCenterPage.createCampaignGenericwithSqlite(driver, dataSet, firstcampaignNamefranstatus, firstaccessibilityfranstatus,
					firstcampaignTypefranstatus, firsttemplateNamefranstatus, firstemailSubjectfranstatus, firstplainTextEditorfranstatus);
			
			//Create Status Driven campaign 1 at Campaign Center > Campaigns
			fc.utobj().printTestStep("Create Status Driven campaign 1 at Campaign Center > Campaigns");
			String secondcampaignNamefranstatus = fc.utobj().generateTestData(dataSet.get("CorpcampaignName"));
			String privateaccessibilityfranstatus = dataSet.get("Franaccessibilityprivate");
			String secondcampaignTypefranstatus = dataSet.get("StatusCampaigntype");
			String secondtemplateNamefranstatus = fc.utobj().generateTestData(dataSet.get("CorptemplateName"));
			String secondemailSubjectfranstatus = fc.utobj().generateTestData(dataSet.get("CorpemailSubject"));
			String secondplainTextEditorfranstatus = fc.utobj().generateTestData(dataSet.get("CorpplainTextEditor"));
			campaignCenterPage.createCampaignGenericwithSqlite(driver, dataSet, secondcampaignNamefranstatus, privateaccessibilityfranstatus,
					secondcampaignTypefranstatus, secondtemplateNamefranstatus, secondemailSubjectfranstatus, secondplainTextEditorfranstatus);
			
			fc.utobj().clickElement(driver, pobjcam.viewAllButton);
			
			//Verify Public to all users of my Franchise Campaign created by Franchise user who has privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Public to all users of my Franchise Campaign created by Franchise user who has privilege to see private campaign by itself");
			campaignCenterPage.searchCampaignByFilter(driver, firstcampaignNamefran);

			boolean isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchise = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firstcampaignNamefran + "')]");
			if (isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchise == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my Franchise Campaign created by Franchise user who has privilege to see private campaign by itself");
			}
			
			//Verify Private Campaign created by Franchise user who has privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Private Campaign created by Franchise user who has privilege to see private campaign by itself");
			campaignCenterPage.searchCampaignByFilter(driver, secondcampaignNamefran);
			
			boolean isCampaignNamePresentFranpaccessibilityprivate = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondcampaignNamefran + "')]");
			if (isCampaignNamePresentFranpaccessibilityprivate == false) {
				fc.utobj().throwsException("was not able to verify Private Campaign created by Franchise user who has privilege to see private campaign by itself");
			}
			
			
			//Verify Public to all users of my Franchise (Status Driven)Campaign created by Franchise user who has privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Public to all users of my Franchise (Status Driven)Campaign created by Franchise user who has privilege to see private campaign by itself");
			campaignCenterPage.searchCampaignByFilter(driver, firstcampaignNamefranstatus);

			boolean isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchisestatus = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firstcampaignNamefranstatus + "')]");
			if (isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchisestatus == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my Franchise (Status Driven)Campaign created by Franchise user who has privilege to see private campaign by itself");
			}
			
			//Verify Private Campaign created by Franchise user who has privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Private (Status Driven)Campaign created by Franchise user who has privilege to see private campaign by itself");
			campaignCenterPage.searchCampaignByFilter(driver, secondcampaignNamefranstatus);
			
			boolean isCampaignNamePresentFranpaccessibilityprivatestatus = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondcampaignNamefranstatus + "')]");
			if (isCampaignNamePresentFranpaccessibilityprivatestatus == false) {
				fc.utobj().throwsException("was not able to verify Private (Status Driven)Campaign created by Franchise user who has privilege to see private campaign by itself");
			}
			
			fc.utobj().printTestStep("Navigate to CRM>Campaign Center>email template");
			emailtemplate.clickonemailtamplate(driver);
			CRMCampaignCenterPage crmcenter=new CRMCampaignCenterPage(driver);
			fc.utobj().clickElement(driver, crmcenter.locationtemplate);
			//Verify Public to all users template created by Corporate user who has privilege to see private template by itself
			fc.utobj().printTestStep("Verify Public to all users of my franchise template created by franchise user who has privilege to see private template by itself");
			campaignCenterPage.searchTemplateByFilter(driver, firsttemplateNamefran);

			boolean isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchise = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firsttemplateNamefran + "')]");
			if (isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchise == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my franchise template created by franchise user who has privilege to see private template by itself");
			}
			
			//Verify Private template created by franchise user who has privilege to see private template by itself
			fc.utobj().printTestStep("Verify Private template created by franchise user who has privilege to see private template by itself");
			campaignCenterPage.searchTemplateByFilter(driver, secondtemplateNamefran);
			
			boolean isTemplateNamePresentfranaccessibilityprivate = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondtemplateNamefran + "')]");
			if (isTemplateNamePresentfranaccessibilityprivate == false) {
				fc.utobj().throwsException("was not able to verify private template created by franchise user who has privilege to see private template by itself");
			}
			
			//Verify Public to all users (Status Driven)template created by Corporate user who has privilege to see private template by itself
			fc.utobj().printTestStep("Verify Public to all users of my franchise (Status Driven)template created by franchise user who has privilege to see private template by itself");
			campaignCenterPage.searchTemplateByFilter(driver, firsttemplateNamefranstatus);

			boolean isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchisestatus = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firsttemplateNamefranstatus + "')]");
			if (isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchisestatus == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my franchise (Status Driven)template created by franchise user who has privilege to see private template by itself");
			}
			
			//Verify Private template created by franchise user who has privilege to see private template by itself
			fc.utobj().printTestStep("Verify Private (Status Driven)template created by franchise user who has privilege to see private template by itself");
			campaignCenterPage.searchTemplateByFilter(driver, secondtemplateNamefranstatus);
			
			boolean isTemplateNamePresentfranaccessibilityprivatestatus = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondtemplateNamefranstatus + "')]");
			if (isTemplateNamePresentfranaccessibilityprivatestatus == false) {
				fc.utobj().throwsException("was not able to verify private (Status Driven)template created by franchise user who has privilege to see private template by itself");
			}
			
			//Login with Franchise Employee user(FE1) who has role to see private campaign
			fc.utobj().printTestStep("Login with Franchise Employee user(FE1) who has role to see private campaign");
			fc.loginpage().loginWithParameter(driver, userNameFE, passwordFE);
			fc.crm().crm_common().openCRMCampaignCenterPage(fcHomePageTest, driver);
			
			//Create Campaign At Campaign Center > Campaigns
			fc.utobj().printTestStep("Create Campaign At Campaign Center > Campaigns");
			String firstcampaignNamefrane = fc.utobj().generateTestData(dataSet.get("CorpcampaignName"));
			String firstaccessibilityfrane = dataSet.get("FranaccessibilityPublictoallUsersofmyFranchise");
			String firstcampaignTypefrane = dataSet.get("CorpcampaignType");
			String firsttemplateNamefrane = fc.utobj().generateTestData(dataSet.get("CorptemplateName"));
			String firstemailSubjectfrane = fc.utobj().generateTestData(dataSet.get("CorpemailSubject"));
			String firstplainTextEditorfrane = fc.utobj().generateTestData(dataSet.get("CorpplainTextEditor"));
			campaignCenterPage.createCampaignGenericwithSqlite(driver, dataSet, firstcampaignNamefrane, firstaccessibilityfrane,
					firstcampaignTypefrane, firsttemplateNamefrane, firstemailSubjectfrane, firstplainTextEditorfrane);
			
			//Create another campaign at Campaign Center > Campaigns
			fc.utobj().printTestStep("Create another campaign at Campaign Center > Campaigns");
			String secondcampaignNamefrane = fc.utobj().generateTestData(dataSet.get("CorpcampaignName"));
			String privateaccessibilityfrane = dataSet.get("Franaccessibilityprivate");
			String secondcampaignTypefrane = dataSet.get("CorpcampaignType");
			String secondtemplateNamefrane = fc.utobj().generateTestData(dataSet.get("CorptemplateName"));
			String secondemailSubjectfrane = fc.utobj().generateTestData(dataSet.get("CorpemailSubject"));
			String secondplainTextEditorfrane = fc.utobj().generateTestData(dataSet.get("CorpplainTextEditor"));
			campaignCenterPage.createCampaignGenericwithSqlite(driver, dataSet, secondcampaignNamefrane, privateaccessibilityfrane,
					secondcampaignTypefrane, secondtemplateNamefrane, secondemailSubjectfrane, secondplainTextEditorfrane);
		
			//Create Status Driven campaign At Campaign Center > Campaigns
			fc.utobj().printTestStep("Create Campaign At Campaign Center > Campaigns");
			String firstcampaignNamefranestatus = fc.utobj().generateTestData(dataSet.get("CorpcampaignName"));
			String firstaccessibilityfranestatus = dataSet.get("FranaccessibilityPublictoallUsersofmyFranchise");
			String firstcampaignTypefranestatus = dataSet.get("StatusCampaigntype");
			String firsttemplateNamefranestatus = fc.utobj().generateTestData(dataSet.get("CorptemplateName"));
			String firstemailSubjectfranestatus = fc.utobj().generateTestData(dataSet.get("CorpemailSubject"));
			String firstplainTextEditorfranestatus = fc.utobj().generateTestData(dataSet.get("CorpplainTextEditor"));
			campaignCenterPage.createCampaignGenericwithSqlite(driver, dataSet, firstcampaignNamefranestatus, firstaccessibilityfranestatus,
					firstcampaignTypefranestatus, firsttemplateNamefranestatus, firstemailSubjectfranestatus, firstplainTextEditorfranestatus);
			
			//Create Status driven campaign at Campaign Center > Campaigns
			fc.utobj().printTestStep("Create Status Driven campaign at Campaign Center > Campaigns");
			String secondcampaignNamefranestatus = fc.utobj().generateTestData(dataSet.get("CorpcampaignName"));
			String privateaccessibilityfranestatus = dataSet.get("Franaccessibilityprivate");
			String secondcampaignTypefranestatus = dataSet.get("StatusCampaigntype");
			String secondtemplateNamefranestatus = fc.utobj().generateTestData(dataSet.get("CorptemplateName"));
			String secondemailSubjectfranestatus = fc.utobj().generateTestData(dataSet.get("CorpemailSubject"));
			String secondplainTextEditorfranestatus = fc.utobj().generateTestData(dataSet.get("CorpplainTextEditor"));
			campaignCenterPage.createCampaignGenericwithSqlite(driver, dataSet, secondcampaignNamefranestatus, privateaccessibilityfranestatus,
					secondcampaignTypefranestatus, secondtemplateNamefranestatus, secondemailSubjectfranestatus, secondplainTextEditorfranestatus);
			
			fc.utobj().clickElement(driver, pobjcam.viewAllButton);
			
			//Verify Public to all users of my Franchise Campaign created by Franchise user who has privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Public to all users of my Franchise Campaign created by Franchise Employee user(FE1) who has privilege to see private campaign by itself");
			campaignCenterPage.searchCampaignByFilter(driver, firstcampaignNamefrane);

			boolean isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchiseE = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firstcampaignNamefrane + "')]");
			if (isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchiseE == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my Franchise Campaign created by Franchise Employee user(FE1) who has privilege to see private campaign by itself");
			}
			
			//Verify Private Campaign created by Franchise user who has privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Private Campaign created by Franchise Employee user(FE1) who has privilege to see private campaign by itself");
			campaignCenterPage.searchCampaignByFilter(driver, secondcampaignNamefrane);
			
			boolean isCampaignNamePresentFranpaccessibilityprivateE = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondcampaignNamefrane + "')]");
			if (isCampaignNamePresentFranpaccessibilityprivateE == false) {
				fc.utobj().throwsException("was not able to verify Private Campaign created by Franchise Employee user(FE1) who has privilege to see private campaign by itself");
			}
			
			
			//Verify Public to all users of my Franchise (Status Driven)Campaign created by Franchise user who has privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Public to all users of my Franchise (Status Driven)Campaign created by Franchise Employee user(FE1) who has privilege to see private campaign by itself");
			campaignCenterPage.searchCampaignByFilter(driver, firstcampaignNamefranestatus);

			boolean isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchiseEstatus = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firstcampaignNamefranestatus + "')]");
			if (isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchiseEstatus == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my Franchise (Status Driven)Campaign created by Franchise Employee user(FE1) who has privilege to see private campaign by itself");
			}
			
			//Verify Private Campaign created by Franchise user who has privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Private (Status Driven)Campaign created by Franchise Employee user(FE1) who has privilege to see private campaign by itself");
			campaignCenterPage.searchCampaignByFilter(driver, secondcampaignNamefranestatus);
			
			boolean isCampaignNamePresentFranpaccessibilityprivateEstatus = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondcampaignNamefranestatus + "')]");
			if (isCampaignNamePresentFranpaccessibilityprivateEstatus == false) {
				fc.utobj().throwsException("was not able to verify Private (Status Driven)Campaign created by Franchise Employee user(FE1) who has privilege to see private campaign by itself");
			}
		
			fc.utobj().printTestStep("Navigate to CRM>Campaign Center>email template");
			emailtemplate.clickonemailtamplate(driver);
			fc.utobj().clickElement(driver, crmcenter.locationtemplate);
			//Verify Public to all users template created by Franchise Employee user user who has privilege to see private template by itself
			fc.utobj().printTestStep("Verify Public to all users of my franchise template created by franchise Employee user(FE1) who has privilege to see private template by itself");
			campaignCenterPage.searchTemplateByFilter(driver, firsttemplateNamefrane);

			boolean isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchiseE = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firsttemplateNamefrane + "')]");
			if (isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchiseE == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my franchise template created by franchise Employee user(FE1) who has privilege to see private template by itself");
			}
			
			//Verify Private template created by franchise Employee user who has privilege to see private template by itself
			fc.utobj().printTestStep("Verify Private template created by franchise Employee user(FE1) who has privilege to see private template by itself");
			campaignCenterPage.searchTemplateByFilter(driver, secondtemplateNamefrane);
			
			boolean isTemplateNamePresentfranaccessibilityprivateE = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondtemplateNamefrane + "')]");
			if (isTemplateNamePresentfranaccessibilityprivateE == false) {
				fc.utobj().throwsException("was not able to verify private template created by franchise Employee user(FE1) who has privilege to see private template by itself");
			}
			
			
			//Verify Public to all users (Status Driven)template created by Franchise Employee user user who has privilege to see private template by itself
			fc.utobj().printTestStep("Verify Public to all users of my franchise (Status Driven)template created by franchise Employee user(FE1) who has privilege to see private template by itself");
			campaignCenterPage.searchTemplateByFilter(driver, firsttemplateNamefranestatus);

			boolean isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchiseEstatus = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firsttemplateNamefranestatus + "')]");
			if (isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchiseEstatus == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my franchise (Status Driven)template created by franchise Employee user(FE1) who has privilege to see private template by itself");
			}
			
			//Verify Private template created by franchise Employee user who has privilege to see private template by itself
			fc.utobj().printTestStep("Verify Private (Status Driven)template created by franchise Employee user(FE1) who has privilege to see private template by itself");
			campaignCenterPage.searchTemplateByFilter(driver, secondtemplateNamefranestatus);
			
			boolean isTemplateNamePresentfranaccessibilityprivateEstatus = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondtemplateNamefranestatus + "')]");
			if (isTemplateNamePresentfranaccessibilityprivateEstatus == false) {
				fc.utobj().throwsException("was not able to verify private (Status Driven)template created by franchise Employee user(FE1) who has privilege to see private template by itself");
			}
			
			//Login with Franchise Owner user(FO2) who does not have role to see private campaign
			fc.utobj().printTestStep("Login with Franchise Owner user(FO2) who does not have role to see private campaign");
			fc.loginpage().loginWithParameter(driver, userNameFO1, passwordFO1);
			fc.crm().crm_common().openCRMCampaignCenterPage(fcHomePageTest, driver);
			
			//Create Campaign At Campaign Center > Campaigns
			fc.utobj().printTestStep("Create Campaign At Campaign Center > Campaigns");
			String firstcampaignNamefranO2 = fc.utobj().generateTestData(dataSet.get("CorpcampaignName"));
			String firstaccessibilityfranO2 = dataSet.get("FranaccessibilityPublictoallUsersofmyFranchise");
			String firstcampaignTypefranO2 = dataSet.get("CorpcampaignType");
			String firsttemplateNamefranO2 = fc.utobj().generateTestData(dataSet.get("CorptemplateName"));
			String firstemailSubjectfranO2 = fc.utobj().generateTestData(dataSet.get("CorpemailSubject"));
			String firstplainTextEditorfranO2 = fc.utobj().generateTestData(dataSet.get("CorpplainTextEditor"));
			campaignCenterPage.createCampaignGenericwithSqlite(driver, dataSet, firstcampaignNamefranO2, firstaccessibilityfranO2,
					firstcampaignTypefranO2, firsttemplateNamefranO2, firstemailSubjectfranO2, firstplainTextEditorfranO2);
			
			//Create another campaign at Campaign Center > Campaigns
			fc.utobj().printTestStep("Create another campaign at Campaign Center > Campaigns");
			String secondcampaignNamefranO2 = fc.utobj().generateTestData(dataSet.get("CorpcampaignName"));
			String privateaccessibilityfranO2 = dataSet.get("Franaccessibilityprivate");
			String secondcampaignTypefranO2 = dataSet.get("CorpcampaignType");
			String secondtemplateNamefranO2 = fc.utobj().generateTestData(dataSet.get("CorptemplateName"));
			String secondemailSubjectfranO2 = fc.utobj().generateTestData(dataSet.get("CorpemailSubject"));
			String secondplainTextEditorfranO2 = fc.utobj().generateTestData(dataSet.get("CorpplainTextEditor"));
			campaignCenterPage.createCampaignGenericwithSqlite(driver, dataSet, secondcampaignNamefranO2, privateaccessibilityfranO2,
					secondcampaignTypefranO2, secondtemplateNamefranO2, secondemailSubjectfranO2, secondplainTextEditorfranO2);
			
			
			//Create Status Driven campaign At Campaign Center > Campaigns
			fc.utobj().printTestStep("Create Status Driven campaign At Campaign Center > Campaigns");
			String firstcampaignNamefranO2status = fc.utobj().generateTestData(dataSet.get("CorpcampaignName"));
			String firstaccessibilityfranO2status = dataSet.get("FranaccessibilityPublictoallUsersofmyFranchise");
			String firstcampaignTypefranO2status = dataSet.get("StatusCampaigntype");
			String firsttemplateNamefranO2status = fc.utobj().generateTestData(dataSet.get("CorptemplateName"));
			String firstemailSubjectfranO2status = fc.utobj().generateTestData(dataSet.get("CorpemailSubject"));
			String firstplainTextEditorfranO2status = fc.utobj().generateTestData(dataSet.get("CorpplainTextEditor"));
			campaignCenterPage.createCampaignGenericwithSqlite(driver, dataSet, firstcampaignNamefranO2status, firstaccessibilityfranO2status,
					firstcampaignTypefranO2status, firsttemplateNamefranO2status, firstemailSubjectfranO2status, firstplainTextEditorfranO2status);
			
			//Create Status Driven campaign at Campaign Center > Campaigns
			fc.utobj().printTestStep("Create Status Driven campaign 1 at Campaign Center > Campaigns");
			String secondcampaignNamefranO2status = fc.utobj().generateTestData(dataSet.get("CorpcampaignName"));
			String privateaccessibilityfranO2status = dataSet.get("Franaccessibilityprivate");
			String secondcampaignTypefranO2status = dataSet.get("StatusCampaigntype");
			String secondtemplateNamefranO2status = fc.utobj().generateTestData(dataSet.get("CorptemplateName"));
			String secondemailSubjectfranO2status = fc.utobj().generateTestData(dataSet.get("CorpemailSubject"));
			String secondplainTextEditorfranO2status = fc.utobj().generateTestData(dataSet.get("CorpplainTextEditor"));
			campaignCenterPage.createCampaignGenericwithSqlite(driver, dataSet, secondcampaignNamefranO2status, privateaccessibilityfranO2status,
					secondcampaignTypefranO2status, secondtemplateNamefranO2status, secondemailSubjectfranO2status, secondplainTextEditorfranO2status);
			
			fc.utobj().clickElement(driver, pobjcam.viewAllButton);
			
			//Verify Public to all users of my Franchise Campaign created by Franchise user who does not have privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Public to all users of my Franchise Campaign created by Franchise user(FO2) who does not have privilege to see private campaign by itself");
			campaignCenterPage.searchCampaignByFilter(driver, firstcampaignNamefranO2);

			boolean isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchiseO2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firstcampaignNamefranO2 + "')]");
			if (isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchiseO2 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my Franchise Campaign created by Franchise user(FO2) who does not have privilege to see private campaign by itself");
			}
			
			//Verify Private Campaign created by Franchise user who has privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Private Campaign created by Franchise user(FO2) who does not have privilege to see private campaign by itself");
			campaignCenterPage.searchCampaignByFilter(driver, secondcampaignNamefranO2);
			
			boolean isCampaignNamePresentFranpaccessibilityprivateO2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondcampaignNamefranO2 + "')]");
			if (isCampaignNamePresentFranpaccessibilityprivateO2 == false) {
				fc.utobj().throwsException("was not able to verify Private Campaign created by Franchise user(FO2) who does not have privilege to see private campaign by itself");
			}
			
			//Verify Public to all users of my Franchise (Status Driven)Campaign created by Franchise user who does not have privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Public to all users of my Franchise (Status Driven)Campaign created by Franchise user(FO2) who does not have privilege to see private campaign by itself");
			campaignCenterPage.searchCampaignByFilter(driver, firstcampaignNamefranO2status);

			boolean isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchiseO2status = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firstcampaignNamefranO2status + "')]");
			if (isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchiseO2status == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my (Status Driven)Franchise Campaign created by Franchise user(FO2) who does not have privilege to see private campaign by itself");
			}
			
			//Verify Private Campaign created by Franchise user who has privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Private (Status Driven)Campaign created by Franchise user(FO2) who does not have privilege to see private campaign by itself");
			campaignCenterPage.searchCampaignByFilter(driver, secondcampaignNamefranO2status);
			
			boolean isCampaignNamePresentFranpaccessibilityprivateO2status = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondcampaignNamefranO2status + "')]");
			if (isCampaignNamePresentFranpaccessibilityprivateO2status == false) {
				fc.utobj().throwsException("was not able to verify Private (Status Driven)Campaign created by Franchise user(FO2) who does not have privilege to see private campaign by itself");
			}
		
			fc.utobj().printTestStep("Navigate to CRM>Campaign Center>email template");
			emailtemplate.clickonemailtamplate(driver);
			fc.utobj().clickElement(driver, crmcenter.locationtemplate);
			//Verify Public to all users template created by Corporate user who has privilege to see private template by itself
			fc.utobj().printTestStep("Verify Public to all users of my franchise template created by franchise user(FO2) who does not have privilege to see private template by itself");
			campaignCenterPage.searchTemplateByFilter(driver, firsttemplateNamefranO2);

			boolean isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchiseO2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firsttemplateNamefranO2 + "')]");
			if (isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchiseO2 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my franchise template created by franchise user(FO2) who does not have privilege to see private template by itself");
			}
			
			//Verify Private template created by franchise user who has privilege to see private template by itself
			fc.utobj().printTestStep("Verify Private template created by franchise user(FO2) who does not have privilege to see private template by itself");
			campaignCenterPage.searchTemplateByFilter(driver, secondtemplateNamefranO2);
			
			boolean isTemplateNamePresentfranaccessibilityprivateO2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondtemplateNamefranO2 + "')]");
			if (isTemplateNamePresentfranaccessibilityprivateO2 == false) {
				fc.utobj().throwsException("was not able to verify private template created by franchise user(FO2) who does not have privilege to see private template by itself");
			}
			
			//Verify Public to all users template created by Corporate user who has privilege to see private template by itself
			fc.utobj().printTestStep("Verify Public to all users of my franchise (Status Driven)template created by franchise user(FO2) who does not have privilege to see private template by itself");
			campaignCenterPage.searchTemplateByFilter(driver, firsttemplateNamefranO2status);

			boolean isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchiseO2status = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firsttemplateNamefranO2status + "')]");
			if (isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchiseO2status == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my franchise (Status Driven)template created by franchise user(FO2) who does not have privilege to see private template by itself");
			}
			
			//Verify Private template created by franchise user who has privilege to see private template by itself
			fc.utobj().printTestStep("Verify Private (Status Driven)template created by franchise user(FO2) who does not have privilege to see private template by itself");
			campaignCenterPage.searchTemplateByFilter(driver, secondtemplateNamefranO2status);
			
			boolean isTemplateNamePresentfranaccessibilityprivateO2status = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondtemplateNamefranO2status + "')]");
			if (isTemplateNamePresentfranaccessibilityprivateO2status == false) {
				fc.utobj().throwsException("was not able to verify private (Status Driven)template created by franchise user(FO2) who does not have privilege to see private template by itself");
			}
			
			//Login with Franchise Employee user(FE2) who does not have role to see private campaign
			fc.utobj().printTestStep("Login with Franchise Employee user(FE2) who does not have role to see private campaign");
			fc.loginpage().loginWithParameter(driver, userNameFE1, passwordFE1);
			fc.crm().crm_common().openCRMCampaignCenterPage(fcHomePageTest, driver);
			
			//Create Campaign At Campaign Center > Campaigns
			fc.utobj().printTestStep("Create Campaign At Campaign Center > Campaigns");
			String firstcampaignNamefrane2 = fc.utobj().generateTestData(dataSet.get("CorpcampaignName"));
			String firstaccessibilityfrane2 = dataSet.get("FranaccessibilityPublictoallUsersofmyFranchise");
			String firstcampaignTypefrane2 = dataSet.get("CorpcampaignType");
			String firsttemplateNamefrane2 = fc.utobj().generateTestData(dataSet.get("CorptemplateName"));
			String firstemailSubjectfrane2 = fc.utobj().generateTestData(dataSet.get("CorpemailSubject"));
			String firstplainTextEditorfrane2 = fc.utobj().generateTestData(dataSet.get("CorpplainTextEditor"));
			campaignCenterPage.createCampaignGenericwithSqlite(driver, dataSet, firstcampaignNamefrane2, firstaccessibilityfrane2,
					firstcampaignTypefrane2, firsttemplateNamefrane2, firstemailSubjectfrane2, firstplainTextEditorfrane2);
			
			//Create another campaign at Campaign Center > Campaigns
			fc.utobj().printTestStep("Create another campaign at Campaign Center > Campaigns");
			String secondcampaignNamefrane2 = fc.utobj().generateTestData(dataSet.get("CorpcampaignName"));
			String privateaccessibilityfrane2 = dataSet.get("Franaccessibilityprivate");
			String secondcampaignTypefrane2 = dataSet.get("CorpcampaignType");
			String secondtemplateNamefrane2 = fc.utobj().generateTestData(dataSet.get("CorptemplateName"));
			String secondemailSubjectfrane2 = fc.utobj().generateTestData(dataSet.get("CorpemailSubject"));
			String secondplainTextEditorfrane2 = fc.utobj().generateTestData(dataSet.get("CorpplainTextEditor"));
			campaignCenterPage.createCampaignGenericwithSqlite(driver, dataSet, secondcampaignNamefrane2, privateaccessibilityfrane2,
					secondcampaignTypefrane2, secondtemplateNamefrane2, secondemailSubjectfrane2, secondplainTextEditorfrane2);
			
			
			//Create Status Driven campaign At Campaign Center > Campaigns
			fc.utobj().printTestStep("Create Status Driven campaign At Campaign Center > Campaigns");
			String firstcampaignNamefrane2status = fc.utobj().generateTestData(dataSet.get("CorpcampaignName"));
			String firstaccessibilityfrane2status = dataSet.get("FranaccessibilityPublictoallUsersofmyFranchise");
			String firstcampaignTypefrane2status = dataSet.get("StatusCampaigntype");
			String firsttemplateNamefrane2status = fc.utobj().generateTestData(dataSet.get("CorptemplateName"));
			String firstemailSubjectfrane2status = fc.utobj().generateTestData(dataSet.get("CorpemailSubject"));
			String firstplainTextEditorfrane2status = fc.utobj().generateTestData(dataSet.get("CorpplainTextEditor"));
			campaignCenterPage.createCampaignGenericwithSqlite(driver, dataSet, firstcampaignNamefrane2status, firstaccessibilityfrane2status,
					firstcampaignTypefrane2status, firsttemplateNamefrane2status, firstemailSubjectfrane2status, firstplainTextEditorfrane2status);
			
			//Create Status Driven campaign at Campaign Center > Campaigns
			fc.utobj().printTestStep("Create Status Driven campaign at Campaign Center > Campaigns");
			String secondcampaignNamefrane2status = fc.utobj().generateTestData(dataSet.get("CorpcampaignName"));
			String privateaccessibilityfrane2status = dataSet.get("Franaccessibilityprivate");
			String secondcampaignTypefrane2status = dataSet.get("StatusCampaigntype");
			String secondtemplateNamefrane2status = fc.utobj().generateTestData(dataSet.get("CorptemplateName"));
			String secondemailSubjectfrane2status = fc.utobj().generateTestData(dataSet.get("CorpemailSubject"));
			String secondplainTextEditorfrane2status = fc.utobj().generateTestData(dataSet.get("CorpplainTextEditor"));
			campaignCenterPage.createCampaignGenericwithSqlite(driver, dataSet, secondcampaignNamefrane2status, privateaccessibilityfrane2status,
					secondcampaignTypefrane2status, secondtemplateNamefrane2status, secondemailSubjectfrane2status, secondplainTextEditorfrane2status);
			
			fc.utobj().clickElement(driver, pobjcam.viewAllButton);
			
			//Verify Public to all users of my Franchise Campaign created by Franchise user who has privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Public to all users of my Franchise Campaign created by Franchise Employee user(FE2) who does not have privilege to see private campaign by itself");
			campaignCenterPage.searchCampaignByFilter(driver, firstcampaignNamefrane2);

			boolean isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchiseE2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firstcampaignNamefrane2 + "')]");
			if (isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchiseE2 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my Franchise Campaign created by Franchise Employee user(FE2) who does not have privilege to see private campaign by itself");
			}
			
			//Verify Private Campaign created by Franchise user who has privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Private Campaign created by Franchise Employee user(FE2) who does not have privilege to see private campaign by itself");
			campaignCenterPage.searchCampaignByFilter(driver, secondcampaignNamefrane2);
			
			boolean isCampaignNamePresentFranpaccessibilityprivateE2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondcampaignNamefrane2 + "')]");
			if (isCampaignNamePresentFranpaccessibilityprivateE2 == false) {
				fc.utobj().throwsException("was not able to verify Private Campaign created by Franchise Employee user(FE2) who does not have privilege to see private campaign by itself");
			}
			
			
			//Verify Public to all users of my Franchise Campaign created by Franchise user who has privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Public to all users of my Franchise (Status Driven)Campaign created by Franchise Employee user(FE2) who does not have privilege to see private campaign by itself");
			campaignCenterPage.searchCampaignByFilter(driver, firstcampaignNamefrane2status);

			boolean isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchiseE2status = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firstcampaignNamefrane2status + "')]");
			if (isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchiseE2status == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my Franchise (Status Driven)Campaign created by Franchise Employee user(FE2) who does not have privilege to see private campaign by itself");
			}
			
			//Verify Private Campaign created by Franchise user who has privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Private (Status Driven)Campaign created by Franchise Employee user(FE2) who does not have privilege to see private campaign by itself");
			campaignCenterPage.searchCampaignByFilter(driver, secondcampaignNamefrane2status);
			
			boolean isCampaignNamePresentFranpaccessibilityprivateE2status = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondcampaignNamefrane2status + "')]");
			if (isCampaignNamePresentFranpaccessibilityprivateE2status == false) {
				fc.utobj().throwsException("was not able to verify Private (Status Driven)Campaign created by Franchise Employee user(FE2) who does not have privilege to see private campaign by itself");
			}
			
		
			fc.utobj().printTestStep("Navigate to CRM>Campaign Center>email template");
			emailtemplate.clickonemailtamplate(driver);
			fc.utobj().clickElement(driver, crmcenter.locationtemplate);
			//Verify Public to all users template created by Franchise Employee user user who has privilege to see private template by itself
			fc.utobj().printTestStep("Verify Public to all users of my franchise template created by franchise Employee user(FE2) who does not have privilege to see private template by itself");
			campaignCenterPage.searchTemplateByFilter(driver, firsttemplateNamefrane2);

			boolean isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchiseE2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firsttemplateNamefrane2 + "')]");
			if (isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchiseE2 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my franchise template created by franchise Employee user(FE2) who does not have privilege to see private template by itself");
			}
			
			//Verify Private template created by franchise Employee user who has privilege to see private template by itself
			fc.utobj().printTestStep("Verify Private template created by franchise Employee user(FE2) who does not have privilege to see private template by itself");
			campaignCenterPage.searchTemplateByFilter(driver, secondtemplateNamefrane2);
			
			boolean isTemplateNamePresentfranaccessibilityprivateE2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondtemplateNamefrane2 + "')]");
			if (isTemplateNamePresentfranaccessibilityprivateE2 == false) {
				fc.utobj().throwsException("was not able to verify private template created by franchise Employee user(FE2) who does not have privilege to see private template by itself");
			}
			
			//Verify Public to all users template created by Franchise Employee user user who has privilege to see private template by itself
			fc.utobj().printTestStep("Verify Public to all users of my franchise (Status Driven)template created by franchise Employee user(FE2) who does not have privilege to see private template by itself");
			campaignCenterPage.searchTemplateByFilter(driver, firsttemplateNamefrane2status);

			boolean isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchiseE2status = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firsttemplateNamefrane2status + "')]");
			if (isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchiseE2status == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my franchise(Status Driven) template created by franchise Employee user(FE2) who does not have privilege to see private template by itself");
			}
			
			//Verify Private template created by franchise Employee user who has privilege to see private template by itself
			fc.utobj().printTestStep("Verify Private (Status Driven)template created by franchise Employee user(FE2) who does not have privilege to see private template by itself");
			campaignCenterPage.searchTemplateByFilter(driver, secondtemplateNamefrane2status);
			
			boolean isTemplateNamePresentfranaccessibilityprivateE2status = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondtemplateNamefrane2status + "')]");
			if (isTemplateNamePresentfranaccessibilityprivateE2status == false) {
				fc.utobj().throwsException("was not able to verify private (Status Driven)template created by franchise Employee user(FE2) who does not have privilege to see private template by itself");
			}
			
			
			//Login with Franchise Owner MU user(V1) who has role to see private campaign
			fc.utobj().printTestStep("Login with Franchise Owner MU user(V1) who has role to see private campaign");
			fc.loginpage().loginWithParameter(driver, userNameFOmu, passwordFOmu);
			fc.crm().crm_common().openCRMCampaignCenterPage(fcHomePageTest, driver);
			
			//Create Campaign At Campaign Center > Campaigns
			fc.utobj().printTestStep("Create Campaign At Campaign Center > Campaigns");
			String firstcampaignNamefranmu = fc.utobj().generateTestData(dataSet.get("CorpcampaignName"));
			String firstaccessibilityfranmu = dataSet.get("FranaccessibilityPublictoallUsersofmyFranchise");
			String firstcampaignTypefranmu = dataSet.get("CorpcampaignType");
			String firsttemplateNamefranmu = fc.utobj().generateTestData(dataSet.get("CorptemplateName"));
			String firstemailSubjectfranmu = fc.utobj().generateTestData(dataSet.get("CorpemailSubject"));
			String firstplainTextEditorfranmu = fc.utobj().generateTestData(dataSet.get("CorpplainTextEditor"));
			campaignCenterPage.createCampaignGenericwithSqlite(driver, dataSet, firstcampaignNamefranmu, firstaccessibilityfranmu,
					firstcampaignTypefranmu, firsttemplateNamefranmu, firstemailSubjectfranmu, firstplainTextEditorfranmu);
			
			//Create another campaign at Campaign Center > Campaigns
			fc.utobj().printTestStep("Create another campaign at Campaign Center > Campaigns");
			String secondcampaignNamefranmu = fc.utobj().generateTestData(dataSet.get("CorpcampaignName"));
			String privateaccessibilityfranmu = dataSet.get("Franaccessibilityprivate");
			String secondcampaignTypefranmu = dataSet.get("CorpcampaignType");
			String secondtemplateNamefranmu = fc.utobj().generateTestData(dataSet.get("CorptemplateName"));
			String secondemailSubjectfranmu = fc.utobj().generateTestData(dataSet.get("CorpemailSubject"));
			String secondplainTextEditorfranmu = fc.utobj().generateTestData(dataSet.get("CorpplainTextEditor"));
			campaignCenterPage.createCampaignGenericwithSqlite(driver, dataSet, secondcampaignNamefranmu, privateaccessibilityfranmu,
					secondcampaignTypefranmu, secondtemplateNamefranmu, secondemailSubjectfranmu, secondplainTextEditorfranmu);
			
			//Create Status driven campaign at At Campaign Center > Campaigns
			fc.utobj().printTestStep("Create Status driven campaign At Campaign Center > Campaigns");
			String firstcampaignNamefranmustatus = fc.utobj().generateTestData(dataSet.get("CorpcampaignName"));
			String firstaccessibilityfranmustatus = dataSet.get("FranaccessibilityPublictoallUsersofmyFranchise");
			String firstcampaignTypefranmustatus = dataSet.get("StatusCampaigntype");
			String firsttemplateNamefranmustatus = fc.utobj().generateTestData(dataSet.get("CorptemplateName"));
			String firstemailSubjectfranmustatus = fc.utobj().generateTestData(dataSet.get("CorpemailSubject"));
			String firstplainTextEditorfranmustatus = fc.utobj().generateTestData(dataSet.get("CorpplainTextEditor"));
			campaignCenterPage.createCampaignGenericwithSqlite(driver, dataSet, firstcampaignNamefranmustatus, firstaccessibilityfranmustatus,
					firstcampaignTypefranmustatus, firsttemplateNamefranmustatus, firstemailSubjectfranmustatus, firstplainTextEditorfranmustatus);
			
			//Create Status Driven campaign at Campaign Center > Campaigns
			fc.utobj().printTestStep("Create Status Driven campaign at Campaign Center > Campaigns");
			String secondcampaignNamefranmustatus = fc.utobj().generateTestData(dataSet.get("CorpcampaignName"));
			String privateaccessibilityfranmustatus = dataSet.get("Franaccessibilityprivate");
			String secondcampaignTypefranmustatus = dataSet.get("StatusCampaigntype");
			String secondtemplateNamefranmustatus = fc.utobj().generateTestData(dataSet.get("CorptemplateName"));
			String secondemailSubjectfranmustatus = fc.utobj().generateTestData(dataSet.get("CorpemailSubject"));
			String secondplainTextEditorfranmustatus = fc.utobj().generateTestData(dataSet.get("CorpplainTextEditor"));
			campaignCenterPage.createCampaignGenericwithSqlite(driver, dataSet, secondcampaignNamefranmustatus, privateaccessibilityfranmustatus,
					secondcampaignTypefranmustatus, secondtemplateNamefranmustatus, secondemailSubjectfranmustatus, secondplainTextEditorfranmustatus);
			
			fc.utobj().clickElement(driver, pobjcam.viewAllButton);
			
			//Verify Public to all users of my Franchise Campaign created by Franchise user who has privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Public to all users of my Franchise Campaign created by Franchise MU user(V1) who has privilege to see private campaign by itself");
			campaignCenterPage.searchCampaignByFilter(driver, firstcampaignNamefranmu);

			boolean isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchisemu = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firstcampaignNamefranmu + "')]");
			if (isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchisemu == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my Franchise Campaign created by Franchise MU user(V1) who has privilege to see private campaign by itself");
			}
			
			//Verify Private Campaign created by Franchise user who has privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Private Campaign created by Franchise MU user(V1) who has privilege to see private campaign by itself");
			campaignCenterPage.searchCampaignByFilter(driver, secondcampaignNamefranmu);
			
			boolean isCampaignNamePresentFranpaccessibilityprivatemu = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondcampaignNamefranmu + "')]");
			if (isCampaignNamePresentFranpaccessibilityprivatemu == false) {
				fc.utobj().throwsException("was not able to verify Private Campaign created by Franchise MU user(V1) who has privilege to see private campaign by itself");
			}
			
			
			//Verify Public to all users of my Franchise (Status Driven)Campaign created by Franchise user who has privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Public to all users of my Franchise (Status Driven)Campaign created by Franchise MU user(V1) who has privilege to see private campaign by itself");
			campaignCenterPage.searchCampaignByFilter(driver, firstcampaignNamefranmustatus);

			boolean isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchisemustatus = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firstcampaignNamefranmustatus + "')]");
			if (isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchisemustatus == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my Franchise (Status Driven)Campaign created by Franchise MU user(V1) who has privilege to see private campaign by itself");
			}
			
			//Verify Private Campaign created by Franchise user who has privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Private (Status Driven)Campaign created by Franchise MU user(V1) who has privilege to see private campaign by itself");
			campaignCenterPage.searchCampaignByFilter(driver, secondcampaignNamefranmustatus);
			
			boolean isCampaignNamePresentFranpaccessibilityprivatemustatus = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondcampaignNamefranmustatus + "')]");
			if (isCampaignNamePresentFranpaccessibilityprivatemustatus == false) {
				fc.utobj().throwsException("was not able to verify Private (Status Driven)Campaign created by Franchise MU user(V1) who has privilege to see private campaign by itself");
			}
			
			fc.utobj().printTestStep("Navigate to CRM>Campaign Center>email template");
			emailtemplate.clickonemailtamplate(driver);
			fc.utobj().clickElement(driver, crmcenter.locationtemplate);
			//Verify Public to all users template created by Corporate user who has privilege to see private template by itself
			fc.utobj().printTestStep("Verify Public to all users of my franchise template created by franchise MU user(V1) who has privilege to see private template by itself");
			campaignCenterPage.searchTemplateByFilter(driver, firsttemplateNamefranmu);

			boolean isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchisemu = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firsttemplateNamefranmu + "')]");
			if (isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchisemu == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my franchise template created by franchise MU user(V1) who has privilege to see private template by itself");
			}
			
			//Verify Private template created by franchise user who has privilege to see private template by itself
			fc.utobj().printTestStep("Verify Private template created by franchise MU user(V1) who has privilege to see private template by itself");
			campaignCenterPage.searchTemplateByFilter(driver, secondtemplateNamefranmu);
			
			boolean isTemplateNamePresentfranaccessibilityprivatemu = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondtemplateNamefranmu + "')]");
			if (isTemplateNamePresentfranaccessibilityprivatemu == false) {
				fc.utobj().throwsException("was not able to verify private template created by franchise MU user(V1) who has privilege to see private template by itself");
			}
			
			//Verify Public to all users template created by Corporate user who has privilege to see private template by itself
			fc.utobj().printTestStep("Verify Public to all users of my franchise (Status Driven)template created by franchise MU user(V1) who has privilege to see private template by itself");
			campaignCenterPage.searchTemplateByFilter(driver, firsttemplateNamefranmustatus);

			boolean isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchisemustatus = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firsttemplateNamefranmustatus + "')]");
			if (isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchisemustatus == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my franchise (Status Driven)template created by franchise MU user(V1) who has privilege to see private template by itself");
			}
			
			//Verify Private template created by franchise user who has privilege to see private template by itself
			fc.utobj().printTestStep("Verify Private (Status Driven)template created by franchise MU user(V1) who has privilege to see private template by itself");
			campaignCenterPage.searchTemplateByFilter(driver, secondtemplateNamefranmustatus);
			
			boolean isTemplateNamePresentfranaccessibilityprivatemustatus = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondtemplateNamefranmustatus + "')]");
			if (isTemplateNamePresentfranaccessibilityprivatemustatus == false) {
				fc.utobj().throwsException("was not able to verify private (Status Driven)template created by franchise MU user(V1) who has privilege to see private template by itself");
			}
			
			
			//Login with Franchise Employee user(V2) who does not have role to see private campaign
			fc.utobj().printTestStep("Login with Franchise Employee user(V2) who does not have role to see private campaign");
			fc.loginpage().loginWithParameter(driver, userNameFEmu, passwordFEmu);
			fc.crm().crm_common().openCRMCampaignCenterPage(fcHomePageTest, driver);
			
			//Create Campaign At Campaign Center > Campaigns
			fc.utobj().printTestStep("Create Campaign At Campaign Center > Campaigns");
			String firstcampaignNamefranmue2 = fc.utobj().generateTestData(dataSet.get("CorpcampaignName"));
			String firstaccessibilityfranmue2 = dataSet.get("FranaccessibilityPublictoallUsersofmyFranchise");
			String firstcampaignTypefranmue2 = dataSet.get("CorpcampaignType");
			String firsttemplateNamefranmue2 = fc.utobj().generateTestData(dataSet.get("CorptemplateName"));
			String firstemailSubjectfranmue2 = fc.utobj().generateTestData(dataSet.get("CorpemailSubject"));
			String firstplainTextEditorfranmue2 = fc.utobj().generateTestData(dataSet.get("CorpplainTextEditor"));
			campaignCenterPage.createCampaignGenericwithSqlite(driver, dataSet, firstcampaignNamefranmue2, firstaccessibilityfranmue2,
					firstcampaignTypefranmue2, firsttemplateNamefranmue2, firstemailSubjectfranmue2, firstplainTextEditorfranmue2);
			
			//Create another campaign at Campaign Center > Campaigns
			fc.utobj().printTestStep("Create another campaign at Campaign Center > Campaigns");
			String secondcampaignNamefranmue2 = fc.utobj().generateTestData(dataSet.get("CorpcampaignName"));
			String privateaccessibilityfranmue2 = dataSet.get("Franaccessibilityprivate");
			String secondcampaignTypefranmue2 = dataSet.get("CorpcampaignType");
			String secondtemplateNamefranmue2 = fc.utobj().generateTestData(dataSet.get("CorptemplateName"));
			String secondemailSubjectfranmue2 = fc.utobj().generateTestData(dataSet.get("CorpemailSubject"));
			String secondplainTextEditorfranmue2 = fc.utobj().generateTestData(dataSet.get("CorpplainTextEditor"));
			campaignCenterPage.createCampaignGenericwithSqlite(driver, dataSet, secondcampaignNamefranmue2, privateaccessibilityfranmue2,
					secondcampaignTypefranmue2, secondtemplateNamefranmue2, secondemailSubjectfranmue2, secondplainTextEditorfranmue2);
			
			
			//Create Status Driven campaign At Campaign Center > Campaigns
			fc.utobj().printTestStep("Create Status Driven campaign At Campaign Center > Campaigns");
			String firstcampaignNamefranmue2status = fc.utobj().generateTestData(dataSet.get("CorpcampaignName"));
			String firstaccessibilityfranmue2status = dataSet.get("FranaccessibilityPublictoallUsersofmyFranchise");
			String firstcampaignTypefranmue2status = dataSet.get("StatusCampaigntype");
			String firsttemplateNamefranmue2status = fc.utobj().generateTestData(dataSet.get("CorptemplateName"));
			String firstemailSubjectfranmue2status = fc.utobj().generateTestData(dataSet.get("CorpemailSubject"));
			String firstplainTextEditorfranmue2status = fc.utobj().generateTestData(dataSet.get("CorpplainTextEditor"));
			campaignCenterPage.createCampaignGenericwithSqlite(driver, dataSet, firstcampaignNamefranmue2status, firstaccessibilityfranmue2status,
					firstcampaignTypefranmue2status, firsttemplateNamefranmue2status, firstemailSubjectfranmue2status, firstplainTextEditorfranmue2status);
			
			//Create another campaign at Campaign Center > Campaigns
			fc.utobj().printTestStep("Create another campaign at Campaign Center > Campaigns");
			String secondcampaignNamefranmue2status = fc.utobj().generateTestData(dataSet.get("CorpcampaignName"));
			String privateaccessibilityfranmue2status = dataSet.get("Franaccessibilityprivate");
			String secondcampaignTypefranmue2status = dataSet.get("StatusCampaigntype");
			String secondtemplateNamefranmue2status = fc.utobj().generateTestData(dataSet.get("CorptemplateName"));
			String secondemailSubjectfranmue2status = fc.utobj().generateTestData(dataSet.get("CorpemailSubject"));
			String secondplainTextEditorfranmue2status = fc.utobj().generateTestData(dataSet.get("CorpplainTextEditor"));
			campaignCenterPage.createCampaignGenericwithSqlite(driver, dataSet, secondcampaignNamefranmue2status, privateaccessibilityfranmue2status,
					secondcampaignTypefranmue2status, secondtemplateNamefranmue2status, secondemailSubjectfranmue2status, secondplainTextEditorfranmue2status);
			
			
			fc.utobj().clickElement(driver, pobjcam.viewAllButton);
			
			//Verify Public to all users of my Franchise Campaign created by Franchise Employee user(V2) who has privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Public to all users of my Franchise Campaign created by Franchise Employee user(V2) who does not have privilege to see private campaign by itself");
			campaignCenterPage.searchCampaignByFilter(driver, firstcampaignNamefranmue2);

			boolean isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchisemue2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firstcampaignNamefranmue2 + "')]");
			if (isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchisemue2 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my Franchise Campaign created by Franchise Employee user(V2) who does not have privilege to see private campaign by itself");
			}
			
			//Verify Private Campaign created by Franchise user who has privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Private Campaign created by Franchise Employee user(V2) who does not have privilege to see private campaign by itself");
			campaignCenterPage.searchCampaignByFilter(driver, secondcampaignNamefranmue2);
			
			boolean isCampaignNamePresentFranpaccessibilityprivatemue2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondcampaignNamefranmue2 + "')]");
			if (isCampaignNamePresentFranpaccessibilityprivatemue2 == false) {
				fc.utobj().throwsException("was not able to verify Private Campaign created by Franchise Employee user(V2) who does not have privilege to see private campaign by itself");
			}
			
			
			//Verify Public to all users of my Franchise Campaign created by Franchise Employee user(V2) who has privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Public to all users of my Franchise (Status Driven)Campaign created by Franchise Employee user(V2) who does not have privilege to see private campaign by itself");
			campaignCenterPage.searchCampaignByFilter(driver, firstcampaignNamefranmue2status);

			boolean isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchisemue2status = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firstcampaignNamefranmue2status + "')]");
			if (isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchisemue2status == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my Franchise (Status Driven)Campaign created by Franchise Employee user(V2) who does not have privilege to see private campaign by itself");
			}
			
			//Verify Private Campaign created by Franchise user who has privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Private (Status Driven)Campaign created by Franchise Employee user(V2) who does not have privilege to see private campaign by itself");
			campaignCenterPage.searchCampaignByFilter(driver, secondcampaignNamefranmue2status);
			
			boolean isCampaignNamePresentFranpaccessibilityprivatemue2status = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondcampaignNamefranmue2status + "')]");
			if (isCampaignNamePresentFranpaccessibilityprivatemue2status == false) {
				fc.utobj().throwsException("was not able to verify Private (Status Driven)Campaign created by Franchise Employee user(V2) who does not have privilege to see private campaign by itself");
			}
			
			fc.utobj().printTestStep("Navigate to CRM>Campaign Center>email template");
			emailtemplate.clickonemailtamplate(driver);
			fc.utobj().clickElement(driver, crmcenter.locationtemplate);
			//Verify Public to all users template created by Franchise user who has privilege to see private template by itself
			fc.utobj().printTestStep("Verify Public to all users of my franchise template created by franchise Employee user(V2) who does not have privilege to see private template by itself");
			campaignCenterPage.searchTemplateByFilter(driver, firsttemplateNamefranmue2);

			boolean isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchisemue2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firsttemplateNamefranmue2 + "')]");
			if (isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchisemue2 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my franchise template created by franchise Employee user(V2) who has privilege to see private template by itself");
			}
			
			//Verify Private template created by franchise user who has privilege to see private template by itself
			fc.utobj().printTestStep("Verify Private template created by franchise Employee user(V2) who has privilege to see private template by itself");
			campaignCenterPage.searchTemplateByFilter(driver, secondtemplateNamefranmue2);
			
			boolean isTemplateNamePresentfranaccessibilityprivatemue2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondtemplateNamefranmue2 + "')]");
			if (isTemplateNamePresentfranaccessibilityprivatemue2 == false) {
				fc.utobj().throwsException("was not able to verify private template created by franchise Employee user(V2) who has privilege to see private template by itself");
			}
			
			//Verify Public to all users template created by Franchise user who has privilege to see private template by itself
			fc.utobj().printTestStep("Verify Public to all users of my franchise (Status Driven)template created by franchise Employee user(V2) who does not have privilege to see private template by itself");
			campaignCenterPage.searchTemplateByFilter(driver, firsttemplateNamefranmue2status);

			boolean isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchisemue2status = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firsttemplateNamefranmue2status + "')]");
			if (isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchisemue2status == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my franchise (Status Driven)template created by franchise Employee user(V2) who has privilege to see private template by itself");
			}
			
			//Verify Private template created by franchise user who has privilege to see private template by itself
			fc.utobj().printTestStep("Verify Private (Status Driven)template created by franchise Employee user(V2) who has privilege to see private template by itself");
			campaignCenterPage.searchTemplateByFilter(driver, secondtemplateNamefranmue2status);
			
			boolean isTemplateNamePresentfranaccessibilityprivatemue2status = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondtemplateNamefranmue2status + "')]");
			if (isTemplateNamePresentfranaccessibilityprivatemue2status == false) {
				fc.utobj().throwsException("was not able to verify private (Status Driven)template created by franchise Employee user(V2) who has privilege to see private template by itself");
			}
			
			//Verification of ALL campaigns with different different users
			
			//Login with Corporate user(C1) who has role to see private campaign
			fc.utobj().printTestStep("Login with Corporate user(C1) who has role to see private campaign");
			fc.loginpage().loginWithParameter(driver, CorpUserName, CorpUserPassword);
			fc.crm().crm_common().openCRMCampaignCenterPage(fcHomePageTest, driver);
			fc.utobj().printTestStep("Click on the menu option and select campaign");
			CRMCampaignCenterPage pobj = new CRMCampaignCenterPage(driver);
			fc.utobj().clickElement(driver, pobj.menuOptions);
			fc.utobj().clickElement(driver, pobj.campaignLinks);
			
			//Verifying Campaigns and templates of corporate user (C2) after logging with Corporate user (C1)
			fc.utobj().printTestStep("Verifying Campaigns and templates of corporate user (C2) after logging with Corporate user (C1)");
			fc.utobj().printTestStep("Verify Public to all users Campaign created by Corporate user(C2) who does not have privilege to see private campaign by Corporate user(C1)");
			campaignCenterPage.searchCampaignByFilter(driver, firstcampaignNamecorp);

			boolean isCampaignNamePresentCorpaccessibilitypublictoalluserscorpC1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firstcampaignNamecorp + "')]");
			if (isCampaignNamePresentCorpaccessibilitypublictoalluserscorpC1 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users Campaign created by Corporate user who does not have privilege to see private campaign by another corporate user(C1)");
			}
			//Verify Public to all corporate users Campaign created by Corporate user who has privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Public to all corporate users Campaign created by Corporate user who does not have privilege to see private campaign by another corporate user(C1)");
			campaignCenterPage.searchCampaignByFilter(driver, secondcampaignNamecorp);
			
			boolean isCampaignNamePresentCorpaccessibilitypublictoallcorporateuserscorpC1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondcampaignNamecorp + "')]");
			if (isCampaignNamePresentCorpaccessibilitypublictoallcorporateuserscorpC1 == false) {
				fc.utobj().throwsException("was not able to verify Public to all corporate users Campaign created by Corporate user who does not privilege to see private campaign by another corporate user(C1)");
			}
			
			//Verify Private Campaign created by Corporate user who has privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Private Campaign created by Corporate user who does not have privilege to see private campaign by another corporate user(C1)");
			campaignCenterPage.searchCampaignByFilter(driver, thirdcampaignNamecorp);
			
			boolean isCampaignNamePresentCorpaccessibilityprivatecorpC1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + thirdcampaignNamecorp + "')]");
			if (isCampaignNamePresentCorpaccessibilityprivatecorpC1 == false) {
				fc.utobj().throwsException("was not able to Private Campaign created by Corporate user who does not have privilege to see private campaign by another corporate user(C1)");
			}
		
			//Verify Public to all users (Status Driven)Campaign created by Corporate user who does not have privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Public to all users(Status Driven) Campaign created by Corporate user who does not have privilege to see private campaign by another corporate user(C1)");
			campaignCenterPage.searchCampaignByFilter(driver, firstcampaignNamecorpstatus);

			boolean isCampaignNamePresentCorpaccessibilitypublictoalluserscorpstatusC1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firstcampaignNamecorpstatus + "')]");
			if (isCampaignNamePresentCorpaccessibilitypublictoalluserscorpstatusC1 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users(Status Driven) Campaign created by Corporate user who does not have privilege to see private campaign by another corporate user(C1)");
			}
			//Verify Public to all corporate users Campaign created by Corporate user who has privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Public to all corporate users(Status Driven) Campaign created by Corporate user who does not have privilege to see private campaign by another corporate user(C1)");
			campaignCenterPage.searchCampaignByFilter(driver, secondcampaignNamecorpstatus);
			
			boolean isCampaignNamePresentCorpaccessibilitypublictoallcorporateuserscorpstatusC1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondcampaignNamecorpstatus + "')]");
			if (isCampaignNamePresentCorpaccessibilitypublictoallcorporateuserscorpstatusC1 == false) {
				fc.utobj().throwsException("was not able to verify Public to all corporate users(Status Driven) Campaign created by Corporate user who does not privilege to see private campaign by another corporate user(C1)");
			}
			
			//Verify Private Campaign created by Corporate user who has privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Private Campaign created by Corporate user(Status Driven) who does not have privilege to see private campaign by another corporate user(C1)");
			campaignCenterPage.searchCampaignByFilter(driver, thirdcampaignNamecorpstatus);
			
			boolean isCampaignNamePresentCorpaccessibilityprivatecorpstatusC1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + thirdcampaignNamecorpstatus + "')]");
			if (isCampaignNamePresentCorpaccessibilityprivatecorpstatusC1 == false) {
				fc.utobj().throwsException("was not able to Private Campaign created by Corporate user(Status Driven) who does not have privilege to see private campaign by another corporate user(C1)");
			}
				
			fc.utobj().printTestStep("Navigate to CRM>Campaign Center>email template");
			emailtemplate.clickonemailtamplate(driver);
			
			//Verify Public to all users template created by Corporate user who does not have privilege to see private template by itself
			fc.utobj().printTestStep("Verify Public to all users template created by Corporate user who does not have privilege to see private template by another corporate user(C1)");
			campaignCenterPage.searchTemplateByFilter(driver, firsttemplateNamecorp);

			boolean isTemplateNamePresentCorpaccessibilitypublictoalluserscorpC1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firsttemplateNamecorp + "')]");
			if (isTemplateNamePresentCorpaccessibilitypublictoalluserscorpC1 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users template created by Corporate user who has privilege to see private template by another corporate user(C1)");
			}
			//Verify Public to all corporate users template created by Corporate user who has privilege to see private template by itself
			fc.utobj().printTestStep("Verify Public to all corporate users template created by Corporate user who does not have privilege to see private template by another corporate user(C1)");
			campaignCenterPage.searchTemplateByFilter(driver, secondtemplateNamecorp);
			
			boolean isTemplateNamePresentCorpaccessibilitypublictoallcorporateuserscorpC1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondtemplateNamecorp + "')]");
			if (isTemplateNamePresentCorpaccessibilitypublictoallcorporateuserscorpC1 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users template created by Corporate user who has privilege to see private template by another corporate user(C1)");
			}
			
			//Verify Private template created by Corporate user who has privilege to see private template by itself
			fc.utobj().printTestStep("Verify Privare template created by Corporate user who does not have privilege to see private template by another corporate user(C1)");
			campaignCenterPage.searchTemplateByFilter(driver, thirdtemplateNamecorp);
			
			boolean isTemplateNamePresentCorpaccessibilityprivatecorpC1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + thirdtemplateNamecorp + "')]");
			if (isTemplateNamePresentCorpaccessibilityprivatecorpC1 == false) {
				fc.utobj().throwsException("was not able to verify private template created by Corporate user who has privilege to see private template by another corporate user(C1)");
			}
			
			//Verify Public to all users (Status driven)template created by Corporate user who does not have privilege to see private template by itself
			fc.utobj().printTestStep("Verify Public to all users (Status driven)template created by Corporate user who does not have privilege to see private template by another corporate user(C1)");
			campaignCenterPage.searchTemplateByFilter(driver, firsttemplateNamecorpstatus);

			boolean isTemplateNamePresentCorpaccessibilitypublictoalluserscorpstatusC1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firsttemplateNamecorpstatus + "')]");
			if (isTemplateNamePresentCorpaccessibilitypublictoalluserscorpstatusC1 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users (Status driven)template created by Corporate user who has privilege to see private template by another corporate user(C1)");
			}
			//Verify Public to all corporate users template created by Corporate user who has privilege to see private template by itself
			fc.utobj().printTestStep("Verify Public to all corporate users (Status driven)template created by Corporate user who does not have privilege to see private template by another corporate user(C1)");
			campaignCenterPage.searchTemplateByFilter(driver, secondtemplateNamecorpstatus);
			
			boolean isTemplateNamePresentCorpaccessibilitypublictoallcorporateuserscorpstatusC1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondtemplateNamecorpstatus + "')]");
			if (isTemplateNamePresentCorpaccessibilitypublictoallcorporateuserscorpstatusC1 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users (Status driven)template created by Corporate user who has privilege to see private template by another corporate user(C1)");
			}
			
			//Verify Private template created by Corporate user who has privilege to see private template by itself
			fc.utobj().printTestStep("Verify Privare (Status driven)template created by Corporate user who does not have privilege to see private template by another corporate user(C1)");
			campaignCenterPage.searchTemplateByFilter(driver, thirdtemplateNamecorpstatus);
			
			boolean isTemplateNamePresentCorpaccessibilityprivatecorpstatusC1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + thirdtemplateNamecorpstatus + "')]");
			if (isTemplateNamePresentCorpaccessibilityprivatecorpstatusC1 == false) {
				fc.utobj().throwsException("was not able to verify private (Status driven)template created by Corporate user who has privilege to see private template by another corporate user(C1)");
			}
		
			fc.utobj().printTestStep("Navigate to CRM>Campaign Center>campaign template");
			emailtemplate.clickoncampaigntemplate(driver);
			//Verify Public to all users of my region Campaign created by Regional user who has privilege to see private campaign by itself(R1)
			//Verifying Campaigns and templates of Regional user (R1) after logging with Corporate user (C1)
			fc.utobj().printTestStep("Verifying Campaigns and templates of Regional user (R1) after logging with Corporate user (C1)");
			fc.utobj().printTestStep("Verify Public to all users of my region Campaign created by Regional user who has privilege to see private campaign by another corporate user(C1)");
			campaignCenterPage.searchCampaignByFilter(driver, firstcampaignNamereg);

			boolean isCampaignNamePresentRegpaccessibilitypublictoallusersofmyregionC1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firstcampaignNamereg + "')]");
			if (isCampaignNamePresentRegpaccessibilitypublictoallusersofmyregionC1 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my region Campaign created by Regional user who has privilege to see private campaign by another corporate user(C1)");
			}
			
			//Verify Private Campaign created by Regional user who has privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Private Campaign created by Regional user who has privilege to see private campaign by itself");
			campaignCenterPage.searchCampaignByFilter(driver, secondcampaignNamereg);
			
			boolean isCampaignNamePresentRegpaccessibilityprivateC1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondcampaignNamereg + "')]");
			if (isCampaignNamePresentRegpaccessibilityprivateC1 == false) {
				fc.utobj().throwsException("was not able to verify Private Campaign created by Regional user who has privilege to see private campaign by another corporate user(C1)");
			}
			
			//Verify Public to all users of my region (Status Driven)Campaign created by Regional user who has privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Public to all users of my region (Status Driven)Campaign created by Regional user who has privilege to see private campaign by another corporate user(C1)");
			campaignCenterPage.searchCampaignByFilter(driver, firstcampaignNameregstatus);

			boolean isCampaignNamePresentRegpaccessibilitypublictoallusersofmyregionstatusC1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firstcampaignNameregstatus + "')]");
			if (isCampaignNamePresentRegpaccessibilitypublictoallusersofmyregionstatusC1 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my region (Status Driven)Campaign created by Regional user who has privilege to see private campaign by another corporate user(C1)");
			}
			
			//Verify Private Campaign created by Regional user who has privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Private (Status Driven)Campaign created by Regional user who has privilege to see private campaign by another corporate user(C1)");
			campaignCenterPage.searchCampaignByFilter(driver, secondcampaignNameregstatus);
			
			boolean isCampaignNamePresentRegpaccessibilityprivatestatusC1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondcampaignNameregstatus + "')]");
			if (isCampaignNamePresentRegpaccessibilityprivatestatusC1 == false) {
				fc.utobj().throwsException("was not able to verify Private (Status Driven)Campaign created by Regional user who has privilege to see private campaign by another corporate user(C1)");
			}
			
			fc.utobj().printTestStep("Navigate to CRM>Campaign Center>email template");
			emailtemplate.clickonemailtamplate(driver);
			
			//Verify Public to all users template created by Corporate user who has privilege to see private template by itself
			fc.utobj().printTestStep("Verify Public to all users of my region template created by Regional user who has privilege to see private template by another corporate user(C1)");
			campaignCenterPage.searchTemplateByFilter(driver, firsttemplateNamereg);

			boolean isTemplateNamePresentRegaccessibilitypublictoallusersofmyregionC1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firsttemplateNamereg + "')]");
			if (isTemplateNamePresentRegaccessibilitypublictoallusersofmyregionC1 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users template created by Regional user who has privilege to see private template by another corporate user(C1)");
			}
			
			//Verify Private template created by Regional user who has privilege to see private template by itself
			fc.utobj().printTestStep("Verify Private template created by Regional user who has privilege to see private template by another corporate user(C1)");
			campaignCenterPage.searchTemplateByFilter(driver, secondtemplateNamereg);
			
			boolean isTemplateNamePresentRegaccessibilityprivateC1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondtemplateNamereg + "')]");
			if (isTemplateNamePresentRegaccessibilityprivateC1 == false) {
				fc.utobj().throwsException("was not able to verify private template created by Regional user who has privilege to see private template by another corporate user(C1)");
			}
			
			//Verify Public to all users(Status Driven) template created by Regional user who has privilege to see private template by itself
			fc.utobj().printTestStep("Verify Public to all users of my region (Status Driven)template created by Regional user who has privilege to see private template by another corporate user(C1)");
			campaignCenterPage.searchTemplateByFilter(driver, firsttemplateNameregstatus);

			boolean isTemplateNamePresentRegaccessibilitypublictoallusersofmyregionstatusC1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firsttemplateNameregstatus + "')]");
			if (isTemplateNamePresentRegaccessibilitypublictoallusersofmyregionstatusC1 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my region (Status Driven)template created by Regional user who has privilege to see private template by another corporate user(C1)");
			}
			
			//Verify Private template created by Regional user who has privilege to see private template by itself
			fc.utobj().printTestStep("Verify Private (Status Driven)template created by Regional user who has privilege to see private template by another corporate user(C1)");
			campaignCenterPage.searchTemplateByFilter(driver, secondtemplateNameregstatus);
			
			boolean isTemplateNamePresentRegaccessibilityprivatestatusC1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondtemplateNameregstatus + "')]");
			if (isTemplateNamePresentRegaccessibilityprivatestatusC1 == false) {
				fc.utobj().throwsException("was not able to verify private (Status Driven)template created by Regional user who has privilege to see private template by another corporate user(C1)");
			}
			fc.utobj().printTestStep("Navigate to CRM>Campaign Center>campaign template");
			emailtemplate.clickoncampaigntemplate(driver);
			
			//Verify Public to all users Campaign created by Corporate user who has privilege to see private campaign by itself
			fc.utobj().printTestStep("Verifying Campaigns and templates of Corporate user (C1) by itself");
			fc.utobj().printTestStep("Verify Public to all users Campaign created by Corporate user who has privilege to see private campaign by itself");
			campaignCenterPage.searchCampaignByFilter(driver, firstcampaignName);

			boolean isCampaignNamePresentCorpaccessibilitypublictoallusersC1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firstcampaignName + "')]");
			if (isCampaignNamePresentCorpaccessibilitypublictoallusersC1 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users Campaign created by Corporate user who has privilege to see private campaign by itself");
			}
			//Verify Public to all corporate users Campaign created by Corporate user who has privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Public to all corporate users Campaign created by Corporate user who has privilege to see private campaign");
			campaignCenterPage.searchCampaignByFilter(driver, secondcampaignName);
			
			boolean isCampaignNamePresentCorpaccessibilitypublictoallcorporateusersC1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondcampaignName + "')]");
			if (isCampaignNamePresentCorpaccessibilitypublictoallcorporateusersC1 == false) {
				fc.utobj().throwsException("was not able to verify Public to all corporate users Campaign created by Corporate user who has privilege to see private campaign by itself");
			}
			
			//Verify Private Campaign created by Corporate user who has privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Private Campaign created by Corporate user who has privilege to see private campaign by itself");
			campaignCenterPage.searchCampaignByFilter(driver, thirdcampaignName);
			
			boolean isCampaignNamePresentCorpaccessibilityprivateC1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + thirdcampaignName + "')]");
			if (isCampaignNamePresentCorpaccessibilityprivateC1 == false) {
				fc.utobj().throwsException("was not able to Private Campaign created by Corporate user who has privilege to see private campaign by itself");
			}
			
			//Verify Public to all users(Status Driven) Campaign created by Corporate user who has privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Public to all users(Status Driven) Campaign created by Corporate user who has privilege to see private campaign by itself");
			campaignCenterPage.searchCampaignByFilter(driver, firstcampaignNamestatus);

			boolean isCampaignNamePresentCorpaccessibilitypublictoallusersstatusC1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firstcampaignNamestatus + "')]");
			if (isCampaignNamePresentCorpaccessibilitypublictoallusersstatusC1 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users(Status Driven) Campaign created by Corporate user who has privilege to see private campaign by itself");
			}
			//Verify Public to all corporate users(Status Driven) Campaign created by Corporate user who has privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Public to all corporate users(Status Driven) Campaign created by Corporate user who has privilege to see private campaign");
			campaignCenterPage.searchCampaignByFilter(driver, secondcampaignNamestatus);
			
			boolean isCampaignNamePresentCorpaccessibilitypublictoallcorporateusersstatusC1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondcampaignNamestatus + "')]");
			if (isCampaignNamePresentCorpaccessibilitypublictoallcorporateusersstatusC1 == false) {
				fc.utobj().throwsException("was not able to verify Public to all corporate users(Status Driven) Campaign created by Corporate user who has privilege to see private campaign by itself");
			}
			
			//Verify Private Campaign created by Corporate user(Status Driven) who has privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Private Campaign(Status Driven) created by Corporate user who has privilege to see private campaign by itself");
			campaignCenterPage.searchCampaignByFilter(driver, thirdcampaignNamestatus);
			
			boolean isCampaignNamePresentCorpaccessibilityprivatestatusC1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + thirdcampaignNamestatus + "')]");
			if (isCampaignNamePresentCorpaccessibilityprivatestatusC1 == false) {
				fc.utobj().throwsException("was not able to Private Campaign(Status Driven) created by Corporate user who has privilege to see private campaign by itself");
			}
		
			fc.utobj().printTestStep("Navigate to CRM>Campaign Center>email template");
			emailtemplate.clickonemailtamplate(driver);
			
			//Verify Public to all users template created by Corporate user who has privilege to see private template by itself
			fc.utobj().printTestStep("Verify Public to all users template created by Corporate user who has privilege to see private template by itself");
			campaignCenterPage.searchTemplateByFilter(driver, firsttemplateName);

			boolean isTemplateNamePresentCorpaccessibilitypublictoallusersC1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firsttemplateName + "')]");
			if (isTemplateNamePresentCorpaccessibilitypublictoallusersC1 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users template created by Corporate user who has privilege to see private template by itself");
			}
			//Verify Public to all corporate users template created by Corporate user who has privilege to see private template by itself
			fc.utobj().printTestStep("Verify Public to all corporate users template created by Corporate user who has privilege to see private template by itself");
			campaignCenterPage.searchTemplateByFilter(driver, secondtemplateName);
			
			boolean isTemplateNamePresentCorpaccessibilitypublictoallcorporateusersC1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondtemplateName + "')]");
			if (isTemplateNamePresentCorpaccessibilitypublictoallcorporateusersC1 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users template created by Corporate user who has privilege to see private template by itself");
			}
			
			//Verify Private template created by Corporate user who has privilege to see private template by itself
			fc.utobj().printTestStep("Verify Privare template created by Corporate user who has privilege to see private template by itself");
			campaignCenterPage.searchTemplateByFilter(driver, thirdtemplateName);
			
			boolean isTemplateNamePresentCorpaccessibilityprivateC1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + thirdtemplateName + "')]");
			if (isTemplateNamePresentCorpaccessibilityprivateC1 == false) {
				fc.utobj().throwsException("was not able to verify private template created by Corporate user who has privilege to see private template by itself");
			}
				
			//Verify Public to all users template(Status Driven) created by Corporate user who has privilege to see private template by itself
			fc.utobj().printTestStep("Verify Public to all users template(Status Driven) created by Corporate user who has privilege to see private template by itself");
			campaignCenterPage.searchTemplateByFilter(driver, firsttemplateNamestatus);

			boolean isTemplateNamePresentCorpaccessibilitypublictoallusersstatusC1= fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firsttemplateNamestatus + "')]");
			if (isTemplateNamePresentCorpaccessibilitypublictoallusersstatusC1 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users template(Status Driven) created by Corporate user who has privilege to see private template by itself");
			}
			//Verify Public to all corporate users template(Status Driven) created by Corporate user who has privilege to see private template by itself
			fc.utobj().printTestStep("Verify Public to all corporate users template(Status Driven) created by Corporate user who has privilege to see private template by itself");
			campaignCenterPage.searchTemplateByFilter(driver, secondtemplateNamestatus);
			
			boolean isTemplateNamePresentCorpaccessibilitypublictoallcorporateusersstatusC1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondtemplateNamestatus + "')]");
			if (isTemplateNamePresentCorpaccessibilitypublictoallcorporateusersstatusC1 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users template(Status Driven) created by Corporate user who has privilege to see private template by itself");
			}
			
			//Verify Private template(Status Driven) created by Corporate user who has privilege to see private template by itself
			fc.utobj().printTestStep("Verify Privare template(Status Driven) created by Corporate user who has privilege to see private template by itself");
			campaignCenterPage.searchTemplateByFilter(driver, thirdtemplateNamestatus);
			
			boolean isTemplateNamePresentCorpaccessibilityprivatestatusC1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + thirdtemplateNamestatus + "')]");
			if (isTemplateNamePresentCorpaccessibilityprivatestatusC1 == false) {
				fc.utobj().throwsException("was not able to verify private template(Status Driven) created by Corporate user who has privilege to see private template by itself");
			}
		
			fc.utobj().printTestStep("Navigate to CRM>Campaign Center>campaign template");
			emailtemplate.clickoncampaigntemplate(driver);
			
			//Verify Public to all users Campaign created by Corporate user who has privilege to see private campaign by itself
			fc.utobj().printTestStep("Verifying Campaigns and templates of Regional user (R2) by corporate user(C1)");
			//Verify Public to all users of my region Campaign created by Regional user who does not have privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Public to all users of my region Campaign created by Regional user who does not have privilege to see private campaign by corporate user(C1)");
			campaignCenterPage.searchCampaignByFilter(driver, firstcampaignNamereg1);

			boolean isCampaignNamePresentRegpaccessibilitypublictoallusersofmyregion1C1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firstcampaignNamereg1 + "')]");
			if (isCampaignNamePresentRegpaccessibilitypublictoallusersofmyregion1C1 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my region Campaign created by Regional user who does not have privilege to see private campaign by corporate user(C1)");
			}
			
			//Verify Private Campaign created by Regional user who does not have privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Private Campaign created by Regional user who does not have privilege to see private campaign by corporate user(C1)");
			campaignCenterPage.searchCampaignByFilter(driver, secondcampaignNamereg1);
			
			boolean isCampaignNamePresentRegpaccessibilityprivate1C1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondcampaignNamereg1 + "')]");
			if (isCampaignNamePresentRegpaccessibilityprivate1C1 == false) {
				fc.utobj().throwsException("was not able to verify Private Campaign created by Regional user who does not have privilege to see private campaign by corporate user(C1)");
			}
			
			//Verify Public to all users of my region (Status Driven)Campaign created by Regional user who does not have privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Public to all users of my region (Status Driven)Campaign created by Regional user who does not have privilege to see private campaign by corporate user(C1)");
			campaignCenterPage.searchCampaignByFilter(driver, firstcampaignNamereg1status);

			boolean isCampaignNamePresentRegpaccessibilitypublictoallusersofmyregion1statusC1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firstcampaignNamereg1status + "')]");
			if (isCampaignNamePresentRegpaccessibilitypublictoallusersofmyregion1statusC1 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my region (Status Driven)Campaign created by Regional user who does not have privilege to see private campaign by corporate user(C1)");
			}
			
			//Verify Private (Status Driven)Campaign created by Regional user who does not have privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Private (Status Driven)Campaign created by Regional user who does not have privilege to see private campaign by corporate user(C1)");
			campaignCenterPage.searchCampaignByFilter(driver, secondcampaignNamereg1status);
			
			boolean isCampaignNamePresentRegpaccessibilityprivate1statusC1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondcampaignNamereg1status + "')]");
			if (isCampaignNamePresentRegpaccessibilityprivate1statusC1 == false) {
				fc.utobj().throwsException("was not able to verify Private (Status Driven)Campaign created by Regional user who does not have privilege to see private campaign by corporate user(C1)");
			}
		
			fc.utobj().printTestStep("Navigate to CRM>Campaign Center>email template");
			emailtemplate.clickonemailtamplate(driver);
			
			//Verify Public to all users template created by Corporate user who does not have privilege to see private template by itself
			fc.utobj().printTestStep("Verify Public to all users of my region template created by Regional user who does not have privilege to see private template by corporate user(C1)");
			campaignCenterPage.searchTemplateByFilter(driver, firsttemplateNamereg1);

			boolean isTemplateNamePresentRegaccessibilitypublictoallusersofmyregion1C1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firsttemplateNamereg1 + "')]");
			if (isTemplateNamePresentRegaccessibilitypublictoallusersofmyregion1C1 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my region template created by Regional user who does not have privilege to see private template by corporate user(C1)");
			}
			
			//Verify Private template created by Regional user who does not have privilege to see private template by itself
			fc.utobj().printTestStep("Verify Private template created by Regional user does not have privilege to see private template by corporate user(C1)");
			campaignCenterPage.searchTemplateByFilter(driver, secondtemplateNamereg1);
			
			boolean isTemplateNamePresentRegaccessibilityprivate1C1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondtemplateNamereg1 + "')]");
			if (isTemplateNamePresentRegaccessibilityprivate1C1 == false) {
				fc.utobj().throwsException("was not able to verify Private template created by Regional user does not have privilege to see private template by corporate user(C1)");
			}
			
			//Verify Public to all users (Status Driven)template created by Regional user who does not have privilege to see private template by itself
			fc.utobj().printTestStep("Verify Public to all users of my region (Status Driven)template created by Regional user who does not have privilege to see private template by corporate user(C1)");
			campaignCenterPage.searchTemplateByFilter(driver, firsttemplateNamereg1status);

			boolean isTemplateNamePresentRegaccessibilitypublictoallusersofmyregion1statusC1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firsttemplateNamereg1status + "')]");
			if (isTemplateNamePresentRegaccessibilitypublictoallusersofmyregion1statusC1 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my region (Status Driven)template created by Regional user who does not have privilege to see private template by corporate user(C1)");
			}
			
			//Verify Private template created by Regional user who does not have privilege to see private template by itself
			fc.utobj().printTestStep("Verify Private (Status Driven)template created by Regional user does not have privilege to see private template by corporate user(C1)");
			campaignCenterPage.searchTemplateByFilter(driver, secondtemplateNamereg1status);
			
			boolean isTemplateNamePresentRegaccessibilityprivate1statusC1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondtemplateNamereg1status + "')]");
			if (isTemplateNamePresentRegaccessibilityprivate1statusC1 == false) {
				fc.utobj().throwsException("was not able to verify Private (Status Driven)template created by Regional user does not have privilege to see private template by corporate user(C1)");
			}
			
			fc.utobj().printTestStep("Navigate to CRM>Campaign Center>campaign template");
			emailtemplate.clickoncampaigntemplate(driver);
			
			fc.utobj().clickElement(driver, crmcenter.locationCampaign);
			fc.utobj().printTestStep("Verifying Campaigns and templates of Franchise owner user (F01) by corporate user(C1)");
			fc.utobj().printTestStep("Verify Public to all users of my Franchise Campaign created by Franchise user who has privilege to see private campaign by corporate user(C1)");
			campaignCenterPage.searchCampaignByFilter(driver, firstcampaignNamefran);

			boolean isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchiseC1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firstcampaignNamefran + "')]");
			if (isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchiseC1 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my Franchise Campaign created by Franchise user who has privilege to see private campaign by corporate user(C1)");
			}
			
			//Verify Private Campaign created by Franchise user who has privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Private Campaign created by Franchise user who has privilege to see private campaign by corporate user(C1)");
			campaignCenterPage.searchCampaignByFilter(driver, secondcampaignNamefran);
			
			boolean isCampaignNamePresentFranpaccessibilityprivateC1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondcampaignNamefran + "')]");
			if (isCampaignNamePresentFranpaccessibilityprivateC1 == false) {
				fc.utobj().throwsException("was not able to verify Private Campaign created by Franchise user who has privilege to see private campaign by corporate user(C1)");
			}
			
			
			//Verify Public to all users of my Franchise (Status Driven)Campaign created by Franchise user who has privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Public to all users of my Franchise (Status Driven)Campaign created by Franchise user who has privilege to see private campaign by corporate user(C1)");
			campaignCenterPage.searchCampaignByFilter(driver, firstcampaignNamefranstatus);

			boolean isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchisestatusC1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firstcampaignNamefranstatus + "')]");
			if (isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchisestatusC1 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my Franchise (Status Driven)Campaign created by Franchise user who has privilege to see private campaign by corporate user(C1)");
			}
			
			//Verify Private Campaign created by Franchise user who has privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Private (Status Driven)Campaign created by Franchise user who has privilege to see private campaign by corporate user(C1)");
			campaignCenterPage.searchCampaignByFilter(driver, secondcampaignNamefranstatus);
			
			boolean isCampaignNamePresentFranpaccessibilityprivatestatusC1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondcampaignNamefranstatus + "')]");
			if (isCampaignNamePresentFranpaccessibilityprivatestatusC1 == false) {
				fc.utobj().throwsException("was not able to verify Private (Status Driven)Campaign created by Franchise user who has privilege to see private campaign by corporate user(C1)");
			}
			
			fc.utobj().printTestStep("Navigate to CRM>Campaign Center>email template");
			emailtemplate.clickonemailtamplate(driver);
			fc.utobj().clickElement(driver, crmcenter.locationtemplate);
			//Verify Public to all users template created by Corporate user who has privilege to see private template by itself
			fc.utobj().printTestStep("Verify Public to all users of my franchise template created by franchise user who has privilege to see private template by corporate user(C1)");
			campaignCenterPage.searchTemplateByFilter(driver, firsttemplateNamefran);

			boolean isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchiseC1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firsttemplateNamefran + "')]");
			if (isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchiseC1 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my franchise template created by franchise user who has privilege to see private template by corporate user(C1)");
			}
			
			//Verify Private template created by franchise user who has privilege to see private template by itself
			fc.utobj().printTestStep("Verify Private template created by franchise user who has privilege to see private template by corporate user(C1)");
			campaignCenterPage.searchTemplateByFilter(driver, secondtemplateNamefran);
			
			boolean isTemplateNamePresentfranaccessibilityprivateC1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondtemplateNamefran + "')]");
			if (isTemplateNamePresentfranaccessibilityprivateC1 == false) {
				fc.utobj().throwsException("was not able to verify private template created by franchise user who has privilege to see private template by corporate user(C1)");
			}
			
			//Verify Public to all users (Status Driven)template created by Corporate user who has privilege to see private template by itself
			fc.utobj().printTestStep("Verify Public to all users of my franchise (Status Driven)template created by franchise user who has privilege to see private template by corporate user(C1)");
			campaignCenterPage.searchTemplateByFilter(driver, firsttemplateNamefranstatus);

			boolean isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchisestatusC1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firsttemplateNamefranstatus + "')]");
			if (isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchisestatusC1 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my franchise (Status Driven)template created by franchise user who has privilege to see private template by corporate user(C1)");
			}
			
			//Verify Private template created by franchise user who has privilege to see private template by itself
			fc.utobj().printTestStep("Verify Private (Status Driven)template created by franchise user who has privilege to see private template by corporate user(C1)");
			campaignCenterPage.searchTemplateByFilter(driver, secondtemplateNamefranstatus);
			
			boolean isTemplateNamePresentfranaccessibilityprivatestatusC1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondtemplateNamefranstatus + "')]");
			if (isTemplateNamePresentfranaccessibilityprivatestatusC1 == false) {
				fc.utobj().throwsException("was not able to verify private (Status Driven)template created by franchise user who has privilege to see private template by corporate user(C1)");
			}
			fc.utobj().printTestStep("Navigate to CRM>Campaign Center>campaign template");
			emailtemplate.clickoncampaigntemplate(driver);
			fc.utobj().clickElement(driver, crmcenter.locationCampaign);
			fc.utobj().printTestStep("Verifying Campaigns and templates of Franchise Employee user (FE1) by corporate user(C1)");
			
			fc.utobj().printTestStep("Verify Public to all users of my Franchise Campaign created by Franchise Employee user(FE1) who has privilege to see private campaign by corporate user(C1)");
			campaignCenterPage.searchCampaignByFilter(driver, firstcampaignNamefrane);

			boolean isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchiseEC1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firstcampaignNamefrane + "')]");
			if (isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchiseEC1 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my Franchise Campaign created by Franchise Employee user(FE1) who has privilege to see private campaign by corporate user(C1)");
			}
			
			//Verify Private Campaign created by Franchise user who has privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Private Campaign created by Franchise Employee user(FE1) who has privilege to see private campaign by corporate user(C1)");
			campaignCenterPage.searchCampaignByFilter(driver, secondcampaignNamefrane);
			
			boolean isCampaignNamePresentFranpaccessibilityprivateEC1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondcampaignNamefrane + "')]");
			if (isCampaignNamePresentFranpaccessibilityprivateEC1 == false) {
				fc.utobj().throwsException("was not able to verify Private Campaign created by Franchise Employee user(FE1) who has privilege to see private campaign by corporate user(C1)");
			}
			
			
			//Verify Public to all users of my Franchise (Status Driven)Campaign created by Franchise user who has privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Public to all users of my Franchise (Status Driven)Campaign created by Franchise Employee user(FE1) who has privilege to see private campaign by corporate user(C1)");
			campaignCenterPage.searchCampaignByFilter(driver, firstcampaignNamefranestatus);

			boolean isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchiseEstatusC1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firstcampaignNamefranestatus + "')]");
			if (isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchiseEstatusC1 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my Franchise (Status Driven)Campaign created by Franchise Employee user(FE1) who has privilege to see private campaign by corporate user(C1)");
			}
			
			//Verify Private Campaign created by Franchise user who has privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Private (Status Driven)Campaign created by Franchise Employee user(FE1) who has privilege to see private campaign by corporate user(C1)");
			campaignCenterPage.searchCampaignByFilter(driver, secondcampaignNamefranestatus);
			
			boolean isCampaignNamePresentFranpaccessibilityprivateEstatusC1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondcampaignNamefranestatus + "')]");
			if (isCampaignNamePresentFranpaccessibilityprivateEstatusC1 == false) {
				fc.utobj().throwsException("was not able to verify Private (Status Driven)Campaign created by Franchise Employee user(FE1) who has privilege to see private campaign by corporate user(C1)");
			}
		
			fc.utobj().printTestStep("Navigate to CRM>Campaign Center>email template");
			emailtemplate.clickonemailtamplate(driver);
			fc.utobj().clickElement(driver, crmcenter.locationtemplate);
			//Verify Public to all users template created by Franchise Employee user user who has privilege to see private template by itself
			fc.utobj().printTestStep("Verify Public to all users of my franchise template created by franchise Employee user(FE1) who has privilege to see private template by corporate user(C1)");
			campaignCenterPage.searchTemplateByFilter(driver, firsttemplateNamefrane);

			boolean isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchiseEC1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firsttemplateNamefrane + "')]");
			if (isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchiseEC1 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my franchise template created by franchise Employee user(FE1) who has privilege to see private template by corporate user(C1)");
			}
			
			//Verify Private template created by franchise Employee user who has privilege to see private template by itself
			fc.utobj().printTestStep("Verify Private template created by franchise Employee user(FE1) who has privilege to see private template by corporate user(C1)");
			campaignCenterPage.searchTemplateByFilter(driver, secondtemplateNamefrane);
			
			boolean isTemplateNamePresentfranaccessibilityprivateEC1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondtemplateNamefrane + "')]");
			if (isTemplateNamePresentfranaccessibilityprivateEC1 == false) {
				fc.utobj().throwsException("was not able to verify private template created by franchise Employee user(FE1) who has privilege to see private template by corporate user(C1)");
			}
			
			
			//Verify Public to all users (Status Driven)template created by Franchise Employee user user who has privilege to see private template by itself
			fc.utobj().printTestStep("Verify Public to all users of my franchise (Status Driven)template created by franchise Employee user(FE1) who has privilege to see private template by corporate user(C1)");
			campaignCenterPage.searchTemplateByFilter(driver, firsttemplateNamefranestatus);

			boolean isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchiseEstatusC1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firsttemplateNamefranestatus + "')]");
			if (isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchiseEstatusC1 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my franchise (Status Driven)template created by franchise Employee user(FE1) who has privilege to see private template by corporate user(C1)");
			}
			
			//Verify Private template created by franchise Employee user who has privilege to see private template by itself
			fc.utobj().printTestStep("Verify Private (Status Driven)template created by franchise Employee user(FE1) who has privilege to see private template by corporate user(C1)");
			campaignCenterPage.searchTemplateByFilter(driver, secondtemplateNamefranestatus);
			
			boolean isTemplateNamePresentfranaccessibilityprivateEstatusC1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondtemplateNamefranestatus + "')]");
			if (isTemplateNamePresentfranaccessibilityprivateEstatusC1 == false) {
				fc.utobj().throwsException("was not able to verify private (Status Driven)template created by franchise Employee user(FE1) who has privilege to see private template by corporate user(C1)");
			}
			
			
			fc.utobj().printTestStep("Navigate to CRM>Campaign Center>campaign template");
			emailtemplate.clickoncampaigntemplate(driver);
			fc.utobj().clickElement(driver, crmcenter.locationCampaign);
			fc.utobj().printTestStep("Verifying Campaigns and templates of Franchise Employee user (F02) by corporate user(C1)");
			
			
			//Verify Public to all users of my Franchise Campaign created by Franchise user who does not have privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Public to all users of my Franchise Campaign created by Franchise user(FO2) who does not have privilege to see private campaign by corporate user(C1)");
			campaignCenterPage.searchCampaignByFilter(driver, firstcampaignNamefranO2);

			boolean isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchiseO2C1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firstcampaignNamefranO2 + "')]");
			if (isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchiseO2C1 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my Franchise Campaign created by Franchise user(FO2) who does not have privilege to see private campaign by corporate user(C1)");
			}
			
			//Verify Private Campaign created by Franchise user who has privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Private Campaign created by Franchise user(FO2) who does not have privilege to see private campaign by corporate user(C1)");
			campaignCenterPage.searchCampaignByFilter(driver, secondcampaignNamefranO2);
			
			boolean isCampaignNamePresentFranpaccessibilityprivateO2C1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondcampaignNamefranO2 + "')]");
			if (isCampaignNamePresentFranpaccessibilityprivateO2C1 == false) {
				fc.utobj().throwsException("was not able to verify Private Campaign created by Franchise user(FO2) who does not have privilege to see private campaign by corporate user(C1)");
			}
			
			//Verify Public to all users of my Franchise (Status Driven)Campaign created by Franchise user who does not have privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Public to all users of my Franchise (Status Driven)Campaign created by Franchise user(FO2) who does not have privilege to see private campaign by corporate user(C1)");
			campaignCenterPage.searchCampaignByFilter(driver, firstcampaignNamefranO2status);

			boolean isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchiseO2statusC1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firstcampaignNamefranO2status + "')]");
			if (isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchiseO2statusC1 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my (Status Driven)Franchise Campaign created by Franchise user(FO2) who does not have privilege to see private campaign by corporate user(C1)");
			}
			
			//Verify Private Campaign created by Franchise user who has privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Private (Status Driven)Campaign created by Franchise user(FO2) who does not have privilege to see private campaign by corporate user(C1)");
			campaignCenterPage.searchCampaignByFilter(driver, secondcampaignNamefranO2status);
			
			boolean isCampaignNamePresentFranpaccessibilityprivateO2statusC1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondcampaignNamefranO2status + "')]");
			if (isCampaignNamePresentFranpaccessibilityprivateO2statusC1 == false) {
				fc.utobj().throwsException("was not able to verify Private (Status Driven)Campaign created by Franchise user(FO2) who does not have privilege to see private campaign by corporate user(C1)");
			}
		
			fc.utobj().printTestStep("Navigate to CRM>Campaign Center>email template");
			emailtemplate.clickonemailtamplate(driver);
			fc.utobj().clickElement(driver, crmcenter.locationtemplate);
			//Verify Public to all users template created by Corporate user who has privilege to see private template by itself
			fc.utobj().printTestStep("Verify Public to all users of my franchise template created by franchise user(FO2) who does not have privilege to see private template by corporate user(C1)");
			campaignCenterPage.searchTemplateByFilter(driver, firsttemplateNamefranO2);

			boolean isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchiseO2C1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firsttemplateNamefranO2 + "')]");
			if (isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchiseO2C1 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my franchise template created by franchise user(FO2) who does not have privilege to see private template by corporate user(C1)");
			}
			
			//Verify Private template created by franchise user who has privilege to see private template by itself
			fc.utobj().printTestStep("Verify Private template created by franchise user(FO2) who does not have privilege to see private template by corporate user(C1)");
			campaignCenterPage.searchTemplateByFilter(driver, secondtemplateNamefranO2);
			
			boolean isTemplateNamePresentfranaccessibilityprivateO2C1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondtemplateNamefranO2 + "')]");
			if (isTemplateNamePresentfranaccessibilityprivateO2C1 == false) {
				fc.utobj().throwsException("was not able to verify private template created by franchise user(FO2) who does not have privilege to see private template by corporate user(C1)");
			}
			
			//Verify Public to all users template created by Corporate user who has privilege to see private template by itself
			fc.utobj().printTestStep("Verify Public to all users of my franchise (Status Driven)template created by franchise user(FO2) who does not have privilege to see private template by corporate user(C1)");
			campaignCenterPage.searchTemplateByFilter(driver, firsttemplateNamefranO2status);

			boolean isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchiseO2statusC1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firsttemplateNamefranO2status + "')]");
			if (isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchiseO2statusC1 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my franchise (Status Driven)template created by franchise user(FO2) who does not have privilege to see private template by corporate user(C1)");
			}
			
			//Verify Private template created by franchise user who has privilege to see private template by itself
			fc.utobj().printTestStep("Verify Private (Status Driven)template created by franchise user(FO2) who does not have privilege to see private template by corporate user(C1)");
			campaignCenterPage.searchTemplateByFilter(driver, secondtemplateNamefranO2status);
			
			boolean isTemplateNamePresentfranaccessibilityprivateO2statusC1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondtemplateNamefranO2status + "')]");
			if (isTemplateNamePresentfranaccessibilityprivateO2statusC1 == false) {
				fc.utobj().throwsException("was not able to verify private (Status Driven)template created by franchise user(FO2) who does not have privilege to see private template by corporate user(C1)");
			}
			
			fc.utobj().printTestStep("Navigate to CRM>Campaign Center>campaign template");
			emailtemplate.clickoncampaigntemplate(driver);
			fc.utobj().clickElement(driver, crmcenter.locationCampaign);
			fc.utobj().printTestStep("Verifying Campaigns and templates of Franchise Employee user (FE2) by corporate user(C1)");
			
			//Verify Public to all users of my Franchise Campaign created by Franchise user who has privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Public to all users of my Franchise Campaign created by Franchise Employee user(FE2) who does not have privilege to see private campaign by corporate user(C1)");
			campaignCenterPage.searchCampaignByFilter(driver, firstcampaignNamefrane2);

			boolean isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchiseE2C1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firstcampaignNamefrane2 + "')]");
			if (isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchiseE2C1 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my Franchise Campaign created by Franchise Employee user(FE2) who does not have privilege to see private campaign by corporate user(C1)");
			}
			
			//Verify Private Campaign created by Franchise user who has privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Private Campaign created by Franchise Employee user(FE2) who does not have privilege to see private campaign by corporate user(C1)");
			campaignCenterPage.searchCampaignByFilter(driver, secondcampaignNamefrane2);
			
			boolean isCampaignNamePresentFranpaccessibilityprivateE2C1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondcampaignNamefrane2 + "')]");
			if (isCampaignNamePresentFranpaccessibilityprivateE2C1 == false) {
				fc.utobj().throwsException("was not able to verify Private Campaign created by Franchise Employee user(FE2) who does not have privilege to see private campaign by corporate user(C1)");
			}
			
			
			//Verify Public to all users of my Franchise Campaign created by Franchise user who has privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Public to all users of my Franchise (Status Driven)Campaign created by Franchise Employee user(FE2) who does not have privilege to see private campaign by corporate user(C1)");
			campaignCenterPage.searchCampaignByFilter(driver, firstcampaignNamefrane2status);

			boolean isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchiseE2statusC1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firstcampaignNamefrane2status + "')]");
			if (isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchiseE2statusC1 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my Franchise (Status Driven)Campaign created by Franchise Employee user(FE2) who does not have privilege to see private campaign by corporate user(C1)");
			}
			
			//Verify Private Campaign created by Franchise user who has privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Private (Status Driven)Campaign created by Franchise Employee user(FE2) who does not have privilege to see private campaign by corporate user(C1)");
			campaignCenterPage.searchCampaignByFilter(driver, secondcampaignNamefrane2status);
			
			boolean isCampaignNamePresentFranpaccessibilityprivateE2statusC1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondcampaignNamefrane2status + "')]");
			if (isCampaignNamePresentFranpaccessibilityprivateE2statusC1 == false) {
				fc.utobj().throwsException("was not able to verify Private (Status Driven)Campaign created by Franchise Employee user(FE2) who does not have privilege to see private campaign by corporate user(C1)");
			}
			
		
			fc.utobj().printTestStep("Navigate to CRM>Campaign Center>email template");
			emailtemplate.clickonemailtamplate(driver);
			fc.utobj().clickElement(driver, crmcenter.locationtemplate);
			//Verify Public to all users template created by Franchise Employee user user who has privilege to see private template by itself
			fc.utobj().printTestStep("Verify Public to all users of my franchise template created by franchise Employee user(FE2) who does not have privilege to see private template by corporate user(C1)");
			campaignCenterPage.searchTemplateByFilter(driver, firsttemplateNamefrane2);

			boolean isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchiseE2C1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firsttemplateNamefrane2 + "')]");
			if (isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchiseE2C1 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my franchise template created by franchise Employee user(FE2) who does not have privilege to see private template by corporate user(C1)");
			}
			
			//Verify Private template created by franchise Employee user who has privilege to see private template by itself
			fc.utobj().printTestStep("Verify Private template created by franchise Employee user(FE2) who does not have privilege to see private template by corporate user(C1)");
			campaignCenterPage.searchTemplateByFilter(driver, secondtemplateNamefrane2);
			
			boolean isTemplateNamePresentfranaccessibilityprivateE2C1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondtemplateNamefrane2 + "')]");
			if (isTemplateNamePresentfranaccessibilityprivateE2C1 == false) {
				fc.utobj().throwsException("was not able to verify private template created by franchise Employee user(FE2) who does not have privilege to see private template by corporate user(C1)");
			}
			
			//Verify Public to all users template created by Franchise Employee user user who has privilege to see private template by itself
			fc.utobj().printTestStep("Verify Public to all users of my franchise (Status Driven)template created by franchise Employee user(FE2) who does not have privilege to see private template by corporate user(C1)");
			campaignCenterPage.searchTemplateByFilter(driver, firsttemplateNamefrane2status);

			boolean isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchiseE2statusC1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firsttemplateNamefrane2status + "')]");
			if (isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchiseE2statusC1 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my franchise(Status Driven) template created by franchise Employee user(FE2) who does not have privilege to see private template by corporate user(C1)");
			}
			
			//Verify Private template created by franchise Employee user who has privilege to see private template by itself
			fc.utobj().printTestStep("Verify Private (Status Driven)template created by franchise Employee user(FE2) who does not have privilege to see private template by corporate user(C1)");
			campaignCenterPage.searchTemplateByFilter(driver, secondtemplateNamefrane2status);
			
			boolean isTemplateNamePresentfranaccessibilityprivateE2statusC1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondtemplateNamefrane2status + "')]");
			if (isTemplateNamePresentfranaccessibilityprivateE2statusC1 == false) {
				fc.utobj().throwsException("was not able to verify private (Status Driven)template created by franchise Employee user(FE2) who does not have privilege to see private template by corporate user(C1)");
			}
		
			
			fc.utobj().printTestStep("Navigate to CRM>Campaign Center>campaign template");
			emailtemplate.clickoncampaigntemplate(driver);
			fc.utobj().clickElement(driver, crmcenter.locationCampaign);
			fc.utobj().printTestStep("Verifying Campaigns and templates of Franchise Mu user (V1) by corporate user(C1)");
			
			fc.utobj().printTestStep("Verify Public to all users of my Franchise Campaign created by Franchise MU user(V1) who has privilege to see private campaign by corporate user(C1)");
			campaignCenterPage.searchCampaignByFilter(driver, firstcampaignNamefranmu);

			boolean isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchisemuC1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firstcampaignNamefranmu + "')]");
			if (isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchisemuC1 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my Franchise Campaign created by Franchise MU user(V1) who has privilege to see private campaign by corporate user(C1)");
			}
			
			//Verify Private Campaign created by Franchise user who has privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Private Campaign created by Franchise MU user(V1) who has privilege to see private campaign by corporate user(C1)");
			campaignCenterPage.searchCampaignByFilter(driver, secondcampaignNamefranmu);
			
			boolean isCampaignNamePresentFranpaccessibilityprivatemuC1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondcampaignNamefranmu + "')]");
			if (isCampaignNamePresentFranpaccessibilityprivatemuC1 == false) {
				fc.utobj().throwsException("was not able to verify Private Campaign created by Franchise MU user(V1) who has privilege to see private campaign by corporate user(C1)");
			}
			
			
			//Verify Public to all users of my Franchise (Status Driven)Campaign created by Franchise user who has privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Public to all users of my Franchise (Status Driven)Campaign created by Franchise MU user(V1) who has privilege to see private campaign by corporate user(C1)");
			campaignCenterPage.searchCampaignByFilter(driver, firstcampaignNamefranmustatus);

			boolean isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchisemustatusC1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firstcampaignNamefranmustatus + "')]");
			if (isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchisemustatusC1 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my Franchise (Status Driven)Campaign created by Franchise MU user(V1) who has privilege to see private campaign by corporate user(C1)");
			}
			
			//Verify Private Campaign created by Franchise user who has privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Private (Status Driven)Campaign created by Franchise MU user(V1) who has privilege to see private campaign by corporate user(C1)");
			campaignCenterPage.searchCampaignByFilter(driver, secondcampaignNamefranmustatus);
			
			boolean isCampaignNamePresentFranpaccessibilityprivatemustatusC1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondcampaignNamefranmustatus + "')]");
			if (isCampaignNamePresentFranpaccessibilityprivatemustatusC1 == false) {
				fc.utobj().throwsException("was not able to verify Private (Status Driven)Campaign created by Franchise MU user(V1) who has privilege to see private campaign by corporate user(C1)");
			}
			
			fc.utobj().printTestStep("Navigate to CRM>Campaign Center>email template");
			emailtemplate.clickonemailtamplate(driver);
			fc.utobj().clickElement(driver, crmcenter.locationtemplate);
			//Verify Public to all users template created by Corporate user who has privilege to see private template by itself
			fc.utobj().printTestStep("Verify Public to all users of my franchise template created by franchise MU user(V1) who has privilege to see private template by corporate user(C1)");
			campaignCenterPage.searchTemplateByFilter(driver, firsttemplateNamefranmu);

			boolean isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchisemuC1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firsttemplateNamefranmu + "')]");
			if (isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchisemuC1 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my franchise template created by franchise MU user(V1) who has privilege to see private template by corporate user(C1)");
			}
			
			//Verify Private template created by franchise user who has privilege to see private template by itself
			fc.utobj().printTestStep("Verify Private template created by franchise MU user(V1) who has privilege to see private template by corporate user(C1)");
			campaignCenterPage.searchTemplateByFilter(driver, secondtemplateNamefranmu);
			
			boolean isTemplateNamePresentfranaccessibilityprivatemuC1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondtemplateNamefranmu + "')]");
			if (isTemplateNamePresentfranaccessibilityprivatemuC1 == false) {
				fc.utobj().throwsException("was not able to verify private template created by franchise MU user(V1) who has privilege to see private template by corporate user(C1)");
			}
			
			//Verify Public to all users template created by Corporate user who has privilege to see private template by itself
			fc.utobj().printTestStep("Verify Public to all users of my franchise (Status Driven)template created by franchise MU user(V1) who has privilege to see private template by corporate user(C1)");
			campaignCenterPage.searchTemplateByFilter(driver, firsttemplateNamefranmustatus);

			boolean isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchisemustatusC1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firsttemplateNamefranmustatus + "')]");
			if (isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchisemustatusC1 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my franchise (Status Driven)template created by franchise MU user(V1) who has privilege to see private template by corporate user(C1)");
			}
			
			//Verify Private template created by franchise user who has privilege to see private template by itself
			fc.utobj().printTestStep("Verify Private (Status Driven)template created by franchise MU user(V1) who has privilege to see private template by corporate user(C1)");
			campaignCenterPage.searchTemplateByFilter(driver, secondtemplateNamefranmustatus);
			
			boolean isTemplateNamePresentfranaccessibilityprivatemustatusC1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondtemplateNamefranmustatus + "')]");
			if (isTemplateNamePresentfranaccessibilityprivatemustatusC1 == false) {
				fc.utobj().throwsException("was not able to verify private (Status Driven)template created by franchise MU user(V1) who has privilege to see private template by corporate user(C1)");
			}
			
			
			
			
			fc.utobj().printTestStep("Navigate to CRM>Campaign Center>campaign template");
			emailtemplate.clickoncampaigntemplate(driver);
			fc.utobj().clickElement(driver, crmcenter.locationCampaign);
			fc.utobj().printTestStep("Verifying Campaigns and templates of Franchise Employee user (V2) by corporate user(C1)");
			fc.utobj().printTestStep("Verify Public to all users of my Franchise Campaign created by Franchise Employee user(V2) who does not have privilege to see private campaign by corporate user(C1)");
			campaignCenterPage.searchCampaignByFilter(driver, firstcampaignNamefranmue2);

			boolean isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchisemue2C1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firstcampaignNamefranmue2 + "')]");
			if (isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchisemue2C1 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my Franchise Campaign created by Franchise Employee user(V2) who does not have privilege to see private campaign by corporate user(C1)");
			}
			
			//Verify Private Campaign created by Franchise user who has privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Private Campaign created by Franchise Employee user(V2) who does not have privilege to see private campaign by corporate user(C1)");
			campaignCenterPage.searchCampaignByFilter(driver, secondcampaignNamefranmue2);
			
			boolean isCampaignNamePresentFranpaccessibilityprivatemue2C1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondcampaignNamefranmue2 + "')]");
			if (isCampaignNamePresentFranpaccessibilityprivatemue2C1 == false) {
				fc.utobj().throwsException("was not able to verify Private Campaign created by Franchise Employee user(V2) who does not have privilege to see private campaign by corporate user(C1)");
			}
			
			
			//Verify Public to all users of my Franchise Campaign created by Franchise Employee user(V2) who has privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Public to all users of my Franchise (Status Driven)Campaign created by Franchise Employee user(V2) who does not have privilege to see private campaign by corporate user(C1)");
			campaignCenterPage.searchCampaignByFilter(driver, firstcampaignNamefranmue2status);

			boolean isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchisemue2statusC1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firstcampaignNamefranmue2status + "')]");
			if (isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchisemue2statusC1 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my Franchise (Status Driven)Campaign created by Franchise Employee user(V2) who does not have privilege to see private campaign by corporate user(C1)");
			}
			
			//Verify Private Campaign created by Franchise user who has privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Private (Status Driven)Campaign created by Franchise Employee user(V2) who does not have privilege to see private campaign by corporate user(C1)");
			campaignCenterPage.searchCampaignByFilter(driver, secondcampaignNamefranmue2status);
			
			boolean isCampaignNamePresentFranpaccessibilityprivatemue2statusC1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondcampaignNamefranmue2status + "')]");
			if (isCampaignNamePresentFranpaccessibilityprivatemue2statusC1 == false) {
				fc.utobj().throwsException("was not able to verify Private (Status Driven)Campaign created by Franchise Employee user(V2) who does not have privilege to see private campaign by corporate user(C1)");
			}
			
			fc.utobj().printTestStep("Navigate to CRM>Campaign Center>email template");
			emailtemplate.clickonemailtamplate(driver);
			fc.utobj().clickElement(driver, crmcenter.locationtemplate);
			//Verify Public to all users template created by Franchise user who has privilege to see private template by itself
			fc.utobj().printTestStep("Verify Public to all users of my franchise template created by franchise Employee user(V2) who does not have privilege to see private template by corporate user(C1)");
			campaignCenterPage.searchTemplateByFilter(driver, firsttemplateNamefranmue2);

			boolean isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchisemue2C1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firsttemplateNamefranmue2 + "')]");
			if (isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchisemue2C1 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my franchise template created by franchise Employee user(V2) who has privilege to see private template by corporate user(C1)");
			}
			
			//Verify Private template created by franchise user who has privilege to see private template by itself
			fc.utobj().printTestStep("Verify Private template created by franchise Employee user(V2) who has privilege to see private template by corporate user(C1)");
			campaignCenterPage.searchTemplateByFilter(driver, secondtemplateNamefranmue2);
			
			boolean isTemplateNamePresentfranaccessibilityprivatemue2C1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondtemplateNamefranmue2 + "')]");
			if (isTemplateNamePresentfranaccessibilityprivatemue2C1 == false) {
				fc.utobj().throwsException("was not able to verify private template created by franchise Employee user(V2) who has privilege to see private template by corporate user(C1)");
			}
			
			//Verify Public to all users template created by Franchise user who has privilege to see private template by itself
			fc.utobj().printTestStep("Verify Public to all users of my franchise (Status Driven)template created by franchise Employee user(V2) who does not have privilege to see private template by corporate user(C1)");
			campaignCenterPage.searchTemplateByFilter(driver, firsttemplateNamefranmue2status);

			boolean isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchisemue2statusC1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firsttemplateNamefranmue2status + "')]");
			if (isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchisemue2statusC1 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my franchise (Status Driven)template created by franchise Employee user(V2) who has privilege to see private template by corporate user(C1)");
			}
			
			//Verify Private template created by franchise user who has privilege to see private template by itself
			fc.utobj().printTestStep("Verify Private (Status Driven)template created by franchise Employee user(V2) who has privilege to see private template by corporate user(C1)");
			campaignCenterPage.searchTemplateByFilter(driver, secondtemplateNamefranmue2status);
			
			boolean isTemplateNamePresentfranaccessibilityprivatemue2statusC1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondtemplateNamefranmue2status + "')]");
			if (isTemplateNamePresentfranaccessibilityprivatemue2statusC1 == false) {
				fc.utobj().throwsException("was not able to verify private (Status Driven)template created by franchise Employee user(V2) who has privilege to see private template by corporate user(C1)");
			}
			
			if(isCampaignNamePresentCorpaccessibilitypublictoalluserscorpC1 && isCampaignNamePresentCorpaccessibilitypublictoallcorporateuserscorpC1 && isCampaignNamePresentCorpaccessibilityprivatecorpC1 
					&& isCampaignNamePresentCorpaccessibilitypublictoalluserscorpstatusC1 && isCampaignNamePresentCorpaccessibilitypublictoallcorporateuserscorpstatusC1 && isCampaignNamePresentCorpaccessibilityprivatecorpstatusC1 
					&& isTemplateNamePresentCorpaccessibilitypublictoalluserscorpC1 && isTemplateNamePresentCorpaccessibilitypublictoallcorporateuserscorpC1 && isTemplateNamePresentCorpaccessibilityprivatecorpC1 
					&& isTemplateNamePresentCorpaccessibilitypublictoalluserscorpstatusC1 && isTemplateNamePresentCorpaccessibilitypublictoallcorporateuserscorpstatusC1 && isTemplateNamePresentCorpaccessibilityprivatecorpstatusC1 
					&& isCampaignNamePresentRegpaccessibilitypublictoallusersofmyregionC1 && isCampaignNamePresentRegpaccessibilityprivateC1 && isCampaignNamePresentRegpaccessibilitypublictoallusersofmyregionstatusC1 
					&& isCampaignNamePresentRegpaccessibilityprivatestatusC1 && isTemplateNamePresentRegaccessibilitypublictoallusersofmyregionC1 && isTemplateNamePresentRegaccessibilityprivateC1 
					&& isTemplateNamePresentRegaccessibilitypublictoallusersofmyregionstatusC1 && isTemplateNamePresentRegaccessibilityprivatestatusC1 && isCampaignNamePresentCorpaccessibilitypublictoallusersC1 
					&& isCampaignNamePresentCorpaccessibilitypublictoallcorporateusersC1 && isCampaignNamePresentCorpaccessibilityprivateC1 && isCampaignNamePresentCorpaccessibilitypublictoallusersstatusC1 
					&& isCampaignNamePresentCorpaccessibilitypublictoallcorporateusersstatusC1 && isCampaignNamePresentCorpaccessibilityprivatestatusC1 && isTemplateNamePresentCorpaccessibilitypublictoallusersC1 
					&& isTemplateNamePresentCorpaccessibilitypublictoallcorporateusersC1 && isTemplateNamePresentCorpaccessibilityprivateC1 && isTemplateNamePresentCorpaccessibilitypublictoallusersstatusC1 
					&& isTemplateNamePresentCorpaccessibilitypublictoallcorporateusersstatusC1 && isTemplateNamePresentCorpaccessibilityprivatestatusC1 && isCampaignNamePresentRegpaccessibilitypublictoallusersofmyregion1C1
					&& isCampaignNamePresentRegpaccessibilityprivate1C1 && isCampaignNamePresentRegpaccessibilitypublictoallusersofmyregion1statusC1 && isCampaignNamePresentRegpaccessibilityprivate1statusC1
					&& isTemplateNamePresentRegaccessibilitypublictoallusersofmyregion1C1 && isTemplateNamePresentRegaccessibilityprivate1C1 && isTemplateNamePresentRegaccessibilitypublictoallusersofmyregion1statusC1 
					&& isTemplateNamePresentRegaccessibilityprivate1statusC1 && isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchiseC1 && isCampaignNamePresentFranpaccessibilityprivateC1 
					&& isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchisestatusC1 && isCampaignNamePresentFranpaccessibilityprivatestatusC1 && isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchiseC1 
					&& isTemplateNamePresentfranaccessibilityprivateC1 && isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchisestatusC1 && isTemplateNamePresentfranaccessibilityprivatestatusC1 
					&& isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchiseEC1 && isCampaignNamePresentFranpaccessibilityprivateEC1 && isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchiseEstatusC1 
					&& isCampaignNamePresentFranpaccessibilityprivateEstatusC1 && isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchiseEC1 && isTemplateNamePresentfranaccessibilityprivateEC1 
					&& isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchiseEstatusC1 && isTemplateNamePresentfranaccessibilityprivateEstatusC1 && isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchiseO2C1 
					&& isCampaignNamePresentFranpaccessibilityprivateO2C1 && isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchiseO2statusC1 && isCampaignNamePresentFranpaccessibilityprivateO2statusC1 
					&& isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchiseO2C1 && isTemplateNamePresentfranaccessibilityprivateO2C1 && isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchiseO2statusC1 
					&& isTemplateNamePresentfranaccessibilityprivateO2statusC1 && isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchiseE2C1 && isCampaignNamePresentFranpaccessibilityprivateE2C1 
					&& isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchiseE2statusC1 && isCampaignNamePresentFranpaccessibilityprivateE2statusC1 && isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchiseE2C1
					&& isTemplateNamePresentfranaccessibilityprivateE2C1 && isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchiseE2statusC1 && isTemplateNamePresentfranaccessibilityprivateE2statusC1 
					&& isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchisemuC1 && isCampaignNamePresentFranpaccessibilityprivatemuC1 && isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchisemustatusC1 
					&& isCampaignNamePresentFranpaccessibilityprivatemustatusC1 && isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchisemuC1 && isTemplateNamePresentfranaccessibilityprivatemuC1 
					&& isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchisemustatusC1 && isTemplateNamePresentfranaccessibilityprivatemustatusC1 && isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchisemue2C1 
					&& isCampaignNamePresentFranpaccessibilityprivatemue2C1 && isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchisemue2statusC1 && isCampaignNamePresentFranpaccessibilityprivatemue2statusC1 
					&& isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchisemue2C1 && isTemplateNamePresentfranaccessibilityprivatemue2C1 && isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchisemue2statusC1
					&& isTemplateNamePresentfranaccessibilityprivatemue2statusC1) {
				fc.utobj().printTestStep("able to verify all types of campaigns and templates created by corporate/rgional/franchise users when loging with Corporate user(C1) who has priviledge to see private campaign");
			}
			else {
				fc.utobj().throwsException("was not able to verify all types of campaigns and templates created by corporate/rgional/franchise users when loging with Corporate user(C1) who has priviledge to see private campaign");
			}
			
			
			
			
			//Login with Corporate user(C2) who does not have role to see private campaign
			fc.utobj().printTestStep("Login with Corporate user(C2) who does not have role to see private campaign");
			fc.loginpage().loginWithParameter(driver, CorpUserName1, CorpUserPassword1);
			fc.crm().crm_common().openCRMCampaignCenterPage(fcHomePageTest, driver);
			fc.utobj().printTestStep("Click on the menu option and select campaign");
			/*CRMCampaignCenterPage pobj = new CRMCampaignCenterPage(driver);*/
			fc.utobj().clickElement(driver, pobj.menuOptions);
			fc.utobj().clickElement(driver, pobj.campaignLinks);
			
			//Verifying Campaigns and templates of corporate user (C2) by itself
			fc.utobj().printTestStep("Verifying Campaigns and templates of corporate user (C2) by itself");
			fc.utobj().printTestStep("Verify Public to all users Campaign created by Corporate user(C2) who does not have privilege to see private campaign by itself");
			campaignCenterPage.searchCampaignByFilter(driver, firstcampaignNamecorp);

			boolean isCampaignNamePresentCorpaccessibilitypublictoalluserscorpC2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firstcampaignNamecorp + "')]");
			if (isCampaignNamePresentCorpaccessibilitypublictoalluserscorpC2 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users Campaign created by Corporate user who does not have privilege to see private campaign by itself");
			}
			//Verify Public to all corporate users Campaign created by Corporate user who has privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Public to all corporate users Campaign created by Corporate user who does not have privilege to see private campaign by itself");
			campaignCenterPage.searchCampaignByFilter(driver, secondcampaignNamecorp);
			
			boolean isCampaignNamePresentCorpaccessibilitypublictoallcorporateuserscorpC2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondcampaignNamecorp + "')]");
			if (isCampaignNamePresentCorpaccessibilitypublictoallcorporateuserscorpC2 == false) {
				fc.utobj().throwsException("was not able to verify Public to all corporate users Campaign created by Corporate user who does not privilege to see private campaign by itself");
			}
			
			//Verify Private Campaign created by Corporate user who has privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Private Campaign created by Corporate user who does not have privilege to see private campaign by itself");
			campaignCenterPage.searchCampaignByFilter(driver, thirdcampaignNamecorp);
			
			boolean isCampaignNamePresentCorpaccessibilityprivatecorpC2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + thirdcampaignNamecorp + "')]");
			if (isCampaignNamePresentCorpaccessibilityprivatecorpC2 == false) {
				fc.utobj().throwsException("was not able to Private Campaign created by Corporate user who does not have privilege to see private campaign by itself");
			}
		
			//Verify Public to all users (Status Driven)Campaign created by Corporate user who does not have privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Public to all users(Status Driven) Campaign created by Corporate user who does not have privilege to see private campaign by itself");
			campaignCenterPage.searchCampaignByFilter(driver, firstcampaignNamecorpstatus);

			boolean isCampaignNamePresentCorpaccessibilitypublictoalluserscorpstatusC2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firstcampaignNamecorpstatus + "')]");
			if (isCampaignNamePresentCorpaccessibilitypublictoalluserscorpstatusC2 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users(Status Driven) Campaign created by Corporate user who does not have privilege to see private campaign by itself");
			}
			//Verify Public to all corporate users Campaign created by Corporate user who has privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Public to all corporate users(Status Driven) Campaign created by Corporate user who does not have privilege to see private campaign by itself");
			campaignCenterPage.searchCampaignByFilter(driver, secondcampaignNamecorpstatus);
			
			boolean isCampaignNamePresentCorpaccessibilitypublictoallcorporateuserscorpstatusC2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondcampaignNamecorpstatus + "')]");
			if (isCampaignNamePresentCorpaccessibilitypublictoallcorporateuserscorpstatusC2 == false) {
				fc.utobj().throwsException("was not able to verify Public to all corporate users(Status Driven) Campaign created by Corporate user who does not privilege to see private campaign by itself");
			}
			
			//Verify Private Campaign created by Corporate user who has privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Private Campaign created by Corporate user(Status Driven) who does not have privilege to see private campaign by itself");
			campaignCenterPage.searchCampaignByFilter(driver, thirdcampaignNamecorpstatus);
			
			boolean isCampaignNamePresentCorpaccessibilityprivatecorpstatusC2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + thirdcampaignNamecorpstatus + "')]");
			if (isCampaignNamePresentCorpaccessibilityprivatecorpstatusC2 == false) {
				fc.utobj().throwsException("was not able to Private Campaign created by Corporate user(Status Driven) who does not have privilege to see private campaign by itself");
			}
				
			fc.utobj().printTestStep("Navigate to CRM>Campaign Center>email template");
			emailtemplate.clickonemailtamplate(driver);
			
			//Verify Public to all users template created by Corporate user who does not have privilege to see private template by itself
			fc.utobj().printTestStep("Verify Public to all users template created by Corporate user who does not have privilege to see private template by itself");
			campaignCenterPage.searchTemplateByFilter(driver, firsttemplateNamecorp);

			boolean isTemplateNamePresentCorpaccessibilitypublictoalluserscorpC2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firsttemplateNamecorp + "')]");
			if (isTemplateNamePresentCorpaccessibilitypublictoalluserscorpC2 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users template created by Corporate user who has privilege to see private template by itself");
			}
			//Verify Public to all corporate users template created by Corporate user who has privilege to see private template by itself
			fc.utobj().printTestStep("Verify Public to all corporate users template created by Corporate user who does not have privilege to see private template by itself");
			campaignCenterPage.searchTemplateByFilter(driver, secondtemplateNamecorp);
			
			boolean isTemplateNamePresentCorpaccessibilitypublictoallcorporateuserscorpC2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondtemplateNamecorp + "')]");
			if (isTemplateNamePresentCorpaccessibilitypublictoallcorporateuserscorpC2 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users template created by Corporate user who has privilege to see private template by itself");
			}
			
			//Verify Private template created by Corporate user who has privilege to see private template by itself
			fc.utobj().printTestStep("Verify Privare template created by Corporate user who does not have privilege to see private template by itself");
			campaignCenterPage.searchTemplateByFilter(driver, thirdtemplateNamecorp);
			
			boolean isTemplateNamePresentCorpaccessibilityprivatecorpC2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + thirdtemplateNamecorp + "')]");
			if (isTemplateNamePresentCorpaccessibilityprivatecorpC2 == false) {
				fc.utobj().throwsException("was not able to verify private template created by Corporate user who has privilege to see private template by itself");
			}
			
			//Verify Public to all users (Status driven)template created by Corporate user who does not have privilege to see private template by itself
			fc.utobj().printTestStep("Verify Public to all users (Status driven)template created by Corporate user who does not have privilege to see private template by itself");
			campaignCenterPage.searchTemplateByFilter(driver, firsttemplateNamecorpstatus);

			boolean isTemplateNamePresentCorpaccessibilitypublictoalluserscorpstatusC2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firsttemplateNamecorpstatus + "')]");
			if (isTemplateNamePresentCorpaccessibilitypublictoalluserscorpstatusC2 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users (Status driven)template created by Corporate user who has privilege to see private template by itself");
			}
			//Verify Public to all corporate users template created by Corporate user who has privilege to see private template by itself
			fc.utobj().printTestStep("Verify Public to all corporate users (Status driven)template created by Corporate user who does not have privilege to see private template by itself");
			campaignCenterPage.searchTemplateByFilter(driver, secondtemplateNamecorpstatus);
			
			boolean isTemplateNamePresentCorpaccessibilitypublictoallcorporateuserscorpstatusC2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondtemplateNamecorpstatus + "')]");
			if (isTemplateNamePresentCorpaccessibilitypublictoallcorporateuserscorpstatusC2 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users (Status driven)template created by Corporate user who has privilege to see private template by itself");
			}
			
			//Verify Private template created by Corporate user who has privilege to see private template by itself
			fc.utobj().printTestStep("Verify Privare (Status driven)template created by Corporate user who does not have privilege to see private template by itself");
			campaignCenterPage.searchTemplateByFilter(driver, thirdtemplateNamecorpstatus);
			
			boolean isTemplateNamePresentCorpaccessibilityprivatecorpstatusC2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + thirdtemplateNamecorpstatus + "')]");
			if (isTemplateNamePresentCorpaccessibilityprivatecorpstatusC2 == false) {
				fc.utobj().throwsException("was not able to verify private (Status driven)template created by Corporate user who has privilege to see private template by itself");
			}
		
			fc.utobj().printTestStep("Navigate to CRM>Campaign Center>campaign template");
			emailtemplate.clickoncampaigntemplate(driver);
			//Verify Public to all users of my region Campaign created by Regional user who has privilege to see private campaign by itself(R1)
			//Verifying Campaigns and templates of Regional user (R1) after logging with Corporate user (C2)
			fc.utobj().printTestStep("Verifying Campaigns and templates of Regional user (R1) after logging with Corporate user (C2)");
			fc.utobj().printTestStep("Verify Public to all users of my region Campaign created by Regional user who has privilege to see private campaign by another corporate user(C2)");
			campaignCenterPage.searchCampaignByFilter(driver, firstcampaignNamereg);

			boolean isCampaignNamePresentRegpaccessibilitypublictoallusersofmyregionC2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firstcampaignNamereg + "')]");
			if (isCampaignNamePresentRegpaccessibilitypublictoallusersofmyregionC2 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my region Campaign created by Regional user who has privilege to see private campaign by another corporate user(C2)");
			}
			
			//Verify Private Campaign created by Regional user who has privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Private Campaign created by Regional user who has privilege to see private campaign by Corporate user(C2)");
			campaignCenterPage.searchCampaignByFilter(driver, secondcampaignNamereg);
			
			boolean isCampaignNamePresentRegpaccessibilityprivateC2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondcampaignNamereg + "')]");
			System.out.println(isCampaignNamePresentRegpaccessibilityprivateC2);
			System.out.println(!isCampaignNamePresentRegpaccessibilityprivateC2);
			if (!isCampaignNamePresentRegpaccessibilityprivateC2 == false) {
				fc.utobj().throwsException("was not able to verify Private Campaign created by Regional user who has privilege to see private campaign by another corporate user(C2)");
			}
			
			//Verify Public to all users of my region (Status Driven)Campaign created by Regional user who has privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Public to all users of my region (Status Driven)Campaign created by Regional user who has privilege to see private campaign by another corporate user(C2)");
			campaignCenterPage.searchCampaignByFilter(driver, firstcampaignNameregstatus);

			boolean isCampaignNamePresentRegpaccessibilitypublictoallusersofmyregionstatusC2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firstcampaignNameregstatus + "')]");
			if (isCampaignNamePresentRegpaccessibilitypublictoallusersofmyregionstatusC2 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my region (Status Driven)Campaign created by Regional user who has privilege to see private campaign by another corporate user(C2)");
			}
			
			//Verify Private Campaign created by Regional user who has privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Private (Status Driven)Campaign created by Regional user who has privilege to see private campaign by another corporate user(C2)");
			campaignCenterPage.searchCampaignByFilter(driver, secondcampaignNameregstatus);
			
			boolean isCampaignNamePresentRegpaccessibilityprivatestatusC2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondcampaignNameregstatus + "')]");
			if (!isCampaignNamePresentRegpaccessibilityprivatestatusC2 == false) {
				fc.utobj().throwsException("was not able to verify Private (Status Driven)Campaign created by Regional user who has privilege to see private campaign by another corporate user(C2)");
			}
			
			fc.utobj().printTestStep("Navigate to CRM>Campaign Center>email template");
			emailtemplate.clickonemailtamplate(driver);
			
			//Verify Public to all users template created by Corporate user who has privilege to see private template by itself
			fc.utobj().printTestStep("Verify Public to all users of my region template created by Regional user who has privilege to see private template by another corporate user(C2)");
			campaignCenterPage.searchTemplateByFilter(driver, firsttemplateNamereg);

			boolean isTemplateNamePresentRegaccessibilitypublictoallusersofmyregionC2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firsttemplateNamereg + "')]");
			if (isTemplateNamePresentRegaccessibilitypublictoallusersofmyregionC2 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users template created by Regional user who has privilege to see private template by another corporate user(C2)");
			}
			
			//Verify Private template created by Regional user who has privilege to see private template by itself
			fc.utobj().printTestStep("Verify Private template created by Regional user who has privilege to see private template by another corporate user(C2)");
			campaignCenterPage.searchTemplateByFilter(driver, secondtemplateNamereg);
			
			boolean isTemplateNamePresentRegaccessibilityprivateC2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondtemplateNamereg + "')]");
			if (!isTemplateNamePresentRegaccessibilityprivateC2 == false) {
				fc.utobj().throwsException("was not able to verify private template created by Regional user who has privilege to see private template by another corporate user(C2)");
			}
			
			//Verify Public to all users(Status Driven) template created by Regional user who has privilege to see private template by itself
			fc.utobj().printTestStep("Verify Public to all users of my region (Status Driven)template created by Regional user who has privilege to see private template by another corporate user(C2)");
			campaignCenterPage.searchTemplateByFilter(driver, firsttemplateNameregstatus);

			boolean isTemplateNamePresentRegaccessibilitypublictoallusersofmyregionstatusC2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firsttemplateNameregstatus + "')]");
			if (isTemplateNamePresentRegaccessibilitypublictoallusersofmyregionstatusC2 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my region (Status Driven)template created by Regional user who has privilege to see private template by another corporate user(C2)");
			}
			
			//Verify Private template created by Regional user who has privilege to see private template by itself
			fc.utobj().printTestStep("Verify Private (Status Driven)template created by Regional user who has privilege to see private template by another corporate user(C2)");
			campaignCenterPage.searchTemplateByFilter(driver, secondtemplateNameregstatus);
			
			boolean isTemplateNamePresentRegaccessibilityprivatestatusC2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondtemplateNameregstatus + "')]");
			if (!isTemplateNamePresentRegaccessibilityprivatestatusC2 == false) {
				fc.utobj().throwsException("was not able to verify private (Status Driven)template created by Regional user who has privilege to see private template by another corporate user(C2)");
			}
			fc.utobj().printTestStep("Navigate to CRM>Campaign Center>campaign template");
			emailtemplate.clickoncampaigntemplate(driver);
			
			//Verify Public to all users Campaign created by Corporate user who has privilege to see private campaign by itself
			fc.utobj().printTestStep("Verifying Campaigns and templates of Corporate user (C1) by corporate user(C2)");
			fc.utobj().printTestStep("Verify Public to all users Campaign created by Corporate user who has privilege to see private campaign by corporate user(C2)");
			campaignCenterPage.searchCampaignByFilter(driver, firstcampaignName);

			boolean isCampaignNamePresentCorpaccessibilitypublictoallusersC2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firstcampaignName + "')]");
			if (isCampaignNamePresentCorpaccessibilitypublictoallusersC2 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users Campaign created by Corporate user who has privilege to see private campaign by corporate user(C2)");
			}
			//Verify Public to all corporate users Campaign created by Corporate user who has privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Public to all corporate users Campaign created by Corporate user who has privilege to see private campaign");
			campaignCenterPage.searchCampaignByFilter(driver, secondcampaignName);
			
			boolean isCampaignNamePresentCorpaccessibilitypublictoallcorporateusersC2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondcampaignName + "')]");
			if (isCampaignNamePresentCorpaccessibilitypublictoallcorporateusersC2 == false) {
				fc.utobj().throwsException("was not able to verify Public to all corporate users Campaign created by Corporate user who has privilege to see private campaign by corporate user(C2)");
			}
			
			//Verify Private Campaign created by Corporate user who has privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Private Campaign created by Corporate user who has privilege to see private campaign by corporate user(C2)");
			campaignCenterPage.searchCampaignByFilter(driver, thirdcampaignName);
			
			boolean isCampaignNamePresentCorpaccessibilityprivateC2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + thirdcampaignName + "')]");
			if (!isCampaignNamePresentCorpaccessibilityprivateC2 == false) {
				fc.utobj().throwsException("was not able to Private Campaign created by Corporate user who has privilege to see private campaign by corporate user(C2)");
			}
			
			//Verify Public to all users(Status Driven) Campaign created by Corporate user who has privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Public to all users(Status Driven) Campaign created by Corporate user who has privilege to see private campaign by corporate user(C2)");
			campaignCenterPage.searchCampaignByFilter(driver, firstcampaignNamestatus);

			boolean isCampaignNamePresentCorpaccessibilitypublictoallusersstatusC2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firstcampaignNamestatus + "')]");
			if (isCampaignNamePresentCorpaccessibilitypublictoallusersstatusC2 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users(Status Driven) Campaign created by Corporate user who has privilege to see private campaign by corporate user(C2)");
			}
			//Verify Public to all corporate users(Status Driven) Campaign created by Corporate user who has privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Public to all corporate users(Status Driven) Campaign created by Corporate user who has privilege to see private campaign");
			campaignCenterPage.searchCampaignByFilter(driver, secondcampaignNamestatus);
			
			boolean isCampaignNamePresentCorpaccessibilitypublictoallcorporateusersstatusC2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondcampaignNamestatus + "')]");
			if (isCampaignNamePresentCorpaccessibilitypublictoallcorporateusersstatusC2 == false) {
				fc.utobj().throwsException("was not able to verify Public to all corporate users(Status Driven) Campaign created by Corporate user who has privilege to see private campaign by corporate user(C2)");
			}
			
			//Verify Private Campaign created by Corporate user(Status Driven) who has privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Private Campaign(Status Driven) created by Corporate user who has privilege to see private campaign by corporate user(C2)");
			campaignCenterPage.searchCampaignByFilter(driver, thirdcampaignNamestatus);
			
			boolean isCampaignNamePresentCorpaccessibilityprivatestatusC2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + thirdcampaignNamestatus + "')]");
			if (!isCampaignNamePresentCorpaccessibilityprivatestatusC2 == false) {
				fc.utobj().throwsException("was not able to Private Campaign(Status Driven) created by Corporate user who has privilege to see private campaign by corporate user(C2)");
			}
		
			fc.utobj().printTestStep("Navigate to CRM>Campaign Center>email template");
			emailtemplate.clickonemailtamplate(driver);
			
			//Verify Public to all users template created by Corporate user who has privilege to see private template by itself
			fc.utobj().printTestStep("Verify Public to all users template created by Corporate user who has privilege to see private template by corporate user(C2)");
			campaignCenterPage.searchTemplateByFilter(driver, firsttemplateName);

			boolean isTemplateNamePresentCorpaccessibilitypublictoallusersC2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firsttemplateName + "')]");
			if (isTemplateNamePresentCorpaccessibilitypublictoallusersC2 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users template created by Corporate user who has privilege to see private template by corporate user(C2)");
			}
			//Verify Public to all corporate users template created by Corporate user who has privilege to see private template by itself
			fc.utobj().printTestStep("Verify Public to all corporate users template created by Corporate user who has privilege to see private template by corporate user(C2)");
			campaignCenterPage.searchTemplateByFilter(driver, secondtemplateName);
			
			boolean isTemplateNamePresentCorpaccessibilitypublictoallcorporateusersC2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondtemplateName + "')]");
			if (isTemplateNamePresentCorpaccessibilitypublictoallcorporateusersC2 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users template created by Corporate user who has privilege to see private template by corporate user(C2)");
			}
			
			//Verify Private template created by Corporate user who has privilege to see private template by itself
			fc.utobj().printTestStep("Verify Privare template created by Corporate user who has privilege to see private template by corporate user(C2)");
			campaignCenterPage.searchTemplateByFilter(driver, thirdtemplateName);
			
			boolean isTemplateNamePresentCorpaccessibilityprivateC2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + thirdtemplateName + "')]");
			if (!isTemplateNamePresentCorpaccessibilityprivateC2 == false) {
				fc.utobj().throwsException("was not able to verify private template created by Corporate user who has privilege to see private template by corporate user(C2)");
			}
				
			//Verify Public to all users template(Status Driven) created by Corporate user who has privilege to see private template by itself
			fc.utobj().printTestStep("Verify Public to all users template(Status Driven) created by Corporate user who has privilege to see private template by corporate user(C2)");
			campaignCenterPage.searchTemplateByFilter(driver, firsttemplateNamestatus);

			boolean isTemplateNamePresentCorpaccessibilitypublictoallusersstatusC2= fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firsttemplateNamestatus + "')]");
			if (isTemplateNamePresentCorpaccessibilitypublictoallusersstatusC2 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users template(Status Driven) created by Corporate user who has privilege to see private template by corporate user(C2)");
			}
			//Verify Public to all corporate users template(Status Driven) created by Corporate user who has privilege to see private template by itself
			fc.utobj().printTestStep("Verify Public to all corporate users template(Status Driven) created by Corporate user who has privilege to see private template by corporate user(C2)");
			campaignCenterPage.searchTemplateByFilter(driver, secondtemplateNamestatus);
			
			boolean isTemplateNamePresentCorpaccessibilitypublictoallcorporateusersstatusC2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondtemplateNamestatus + "')]");
			if (isTemplateNamePresentCorpaccessibilitypublictoallcorporateusersstatusC2 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users template(Status Driven) created by Corporate user who has privilege to see private template by corporate user(C2)");
			}
			
			//Verify Private template(Status Driven) created by Corporate user who has privilege to see private template by itself
			fc.utobj().printTestStep("Verify Privare template(Status Driven) created by Corporate user who has privilege to see private template by corporate user(C2)");
			campaignCenterPage.searchTemplateByFilter(driver, thirdtemplateNamestatus);
			
			boolean isTemplateNamePresentCorpaccessibilityprivatestatusC2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + thirdtemplateNamestatus + "')]");
			if (!isTemplateNamePresentCorpaccessibilityprivatestatusC2 == false) {
				fc.utobj().throwsException("was not able to verify private template(Status Driven) created by Corporate user who has privilege to see private template by corporate user(C2)");
			}
		
			fc.utobj().printTestStep("Navigate to CRM>Campaign Center>campaign template");
			emailtemplate.clickoncampaigntemplate(driver);
			
			//Verify Public to all users Campaign created by Corporate user who has privilege to see private campaign by itself
			fc.utobj().printTestStep("Verifying Campaigns and templates of Regional user (R2) by corporate user(C2)");
			//Verify Public to all users of my region Campaign created by Regional user who does not have privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Public to all users of my region Campaign created by Regional user who does not have privilege to see private campaign by corporate user(C2)");
			campaignCenterPage.searchCampaignByFilter(driver, firstcampaignNamereg1);

			boolean isCampaignNamePresentRegpaccessibilitypublictoallusersofmyregion1C2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firstcampaignNamereg1 + "')]");
			if (isCampaignNamePresentRegpaccessibilitypublictoallusersofmyregion1C2 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my region Campaign created by Regional user who does not have privilege to see private campaign by corporate user(C2)");
			}
			
			//Verify Private Campaign created by Regional user who does not have privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Private Campaign created by Regional user who does not have privilege to see private campaign by corporate user(C2)");
			campaignCenterPage.searchCampaignByFilter(driver, secondcampaignNamereg1);
			
			boolean isCampaignNamePresentRegpaccessibilityprivate1C2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondcampaignNamereg1 + "')]");
			if (!isCampaignNamePresentRegpaccessibilityprivate1C2 == false) {
				fc.utobj().throwsException("was not able to verify Private Campaign created by Regional user who does not have privilege to see private campaign by corporate user(C2)");
			}
			
			//Verify Public to all users of my region (Status Driven)Campaign created by Regional user who does not have privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Public to all users of my region (Status Driven)Campaign created by Regional user who does not have privilege to see private campaign by corporate user(C2)");
			campaignCenterPage.searchCampaignByFilter(driver, firstcampaignNamereg1status);

			boolean isCampaignNamePresentRegpaccessibilitypublictoallusersofmyregion1statusC2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firstcampaignNamereg1status + "')]");
			if (isCampaignNamePresentRegpaccessibilitypublictoallusersofmyregion1statusC2 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my region (Status Driven)Campaign created by Regional user who does not have privilege to see private campaign by corporate user(C2)");
			}
			
			//Verify Private (Status Driven)Campaign created by Regional user who does not have privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Private (Status Driven)Campaign created by Regional user who does not have privilege to see private campaign by corporate user(C2)");
			campaignCenterPage.searchCampaignByFilter(driver, secondcampaignNamereg1status);
			
			boolean isCampaignNamePresentRegpaccessibilityprivate1statusC2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondcampaignNamereg1status + "')]");
			if (!isCampaignNamePresentRegpaccessibilityprivate1statusC2 == false) {
				fc.utobj().throwsException("was not able to verify Private (Status Driven)Campaign created by Regional user who does not have privilege to see private campaign by corporate user(C2)");
			}
		
			fc.utobj().printTestStep("Navigate to CRM>Campaign Center>email template");
			emailtemplate.clickonemailtamplate(driver);
			
			//Verify Public to all users template created by Corporate user who does not have privilege to see private template by itself
			fc.utobj().printTestStep("Verify Public to all users of my region template created by Regional user who does not have privilege to see private template by corporate user(C2)");
			campaignCenterPage.searchTemplateByFilter(driver, firsttemplateNamereg1);

			boolean isTemplateNamePresentRegaccessibilitypublictoallusersofmyregion1C2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firsttemplateNamereg1 + "')]");
			if (isTemplateNamePresentRegaccessibilitypublictoallusersofmyregion1C2 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my region template created by Regional user who does not have privilege to see private template by corporate user(C2)");
			}
			
			//Verify Private template created by Regional user who does not have privilege to see private template by itself
			fc.utobj().printTestStep("Verify Private template created by Regional user does not have privilege to see private template by corporate user(C2)");
			campaignCenterPage.searchTemplateByFilter(driver, secondtemplateNamereg1);
			
			boolean isTemplateNamePresentRegaccessibilityprivate1C2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondtemplateNamereg1 + "')]");
			if (!isTemplateNamePresentRegaccessibilityprivate1C2 == false) {
				fc.utobj().throwsException("was not able to verify Private template created by Regional user does not have privilege to see private template by corporate user(C2)");
			}
			
			//Verify Public to all users (Status Driven)template created by Regional user who does not have privilege to see private template by itself
			fc.utobj().printTestStep("Verify Public to all users of my region (Status Driven)template created by Regional user who does not have privilege to see private template by corporate user(C2)");
			campaignCenterPage.searchTemplateByFilter(driver, firsttemplateNamereg1status);

			boolean isTemplateNamePresentRegaccessibilitypublictoallusersofmyregion1statusC2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firsttemplateNamereg1status + "')]");
			if (isTemplateNamePresentRegaccessibilitypublictoallusersofmyregion1statusC2 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my region (Status Driven)template created by Regional user who does not have privilege to see private template by corporate user(C2)");
			}
			
			//Verify Private template created by Regional user who does not have privilege to see private template by itself
			fc.utobj().printTestStep("Verify Private (Status Driven)template created by Regional user does not have privilege to see private template by corporate user(C2)");
			campaignCenterPage.searchTemplateByFilter(driver, secondtemplateNamereg1status);
			
			boolean isTemplateNamePresentRegaccessibilityprivate1statusC2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondtemplateNamereg1status + "')]");
			if (!isTemplateNamePresentRegaccessibilityprivate1statusC2 == false) {
				fc.utobj().throwsException("was not able to verify Private (Status Driven)template created by Regional user does not have privilege to see private template by corporate user(C2)");
			}
			
			fc.utobj().printTestStep("Navigate to CRM>Campaign Center>campaign template");
			emailtemplate.clickoncampaigntemplate(driver);
			
			fc.utobj().clickElement(driver, crmcenter.locationCampaign);
			fc.utobj().printTestStep("Verifying Campaigns and templates of Franchise owner user (F01) by corporate user(C2)");
			fc.utobj().printTestStep("Verify Public to all users of my Franchise Campaign created by Franchise user who has privilege to see private campaign by corporate user(C2)");
			campaignCenterPage.searchCampaignByFilter(driver, firstcampaignNamefran);

			boolean isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchiseC2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firstcampaignNamefran + "')]");
			if (isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchiseC2 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my Franchise Campaign created by Franchise user who has privilege to see private campaign by corporate user(C2)");
			}
			
			//Verify Private Campaign created by Franchise user who has privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Private Campaign created by Franchise user who has privilege to see private campaign by corporate user(C2)");
			campaignCenterPage.searchCampaignByFilter(driver, secondcampaignNamefran);
			
			boolean isCampaignNamePresentFranpaccessibilityprivateC2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondcampaignNamefran + "')]");
			if (!isCampaignNamePresentFranpaccessibilityprivateC2 == false) {
				fc.utobj().throwsException("was not able to verify Private Campaign created by Franchise user who has privilege to see private campaign by corporate user(C2)");
			}
			
			
			//Verify Public to all users of my Franchise (Status Driven)Campaign created by Franchise user who has privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Public to all users of my Franchise (Status Driven)Campaign created by Franchise user who has privilege to see private campaign by corporate user(C2)");
			campaignCenterPage.searchCampaignByFilter(driver, firstcampaignNamefranstatus);

			boolean isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchisestatusC2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firstcampaignNamefranstatus + "')]");
			if (isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchisestatusC2 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my Franchise (Status Driven)Campaign created by Franchise user who has privilege to see private campaign by corporate user(C2)");
			}
			
			//Verify Private Campaign created by Franchise user who has privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Private (Status Driven)Campaign created by Franchise user who has privilege to see private campaign by corporate user(C2)");
			campaignCenterPage.searchCampaignByFilter(driver, secondcampaignNamefranstatus);
			
			boolean isCampaignNamePresentFranpaccessibilityprivatestatusC2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondcampaignNamefranstatus + "')]");
			if (!isCampaignNamePresentFranpaccessibilityprivatestatusC2 == false) {
				fc.utobj().throwsException("was not able to verify Private (Status Driven)Campaign created by Franchise user who has privilege to see private campaign by corporate user(C2)");
			}
			
			fc.utobj().printTestStep("Navigate to CRM>Campaign Center>email template");
			emailtemplate.clickonemailtamplate(driver);
			fc.utobj().clickElement(driver, crmcenter.locationtemplate);
			//Verify Public to all users template created by Corporate user who has privilege to see private template by itself
			fc.utobj().printTestStep("Verify Public to all users of my franchise template created by franchise user who has privilege to see private template by corporate user(C2)");
			campaignCenterPage.searchTemplateByFilter(driver, firsttemplateNamefran);

			boolean isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchiseC2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firsttemplateNamefran + "')]");
			if (isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchiseC2 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my franchise template created by franchise user who has privilege to see private template by corporate user(C2)");
			}
			
			//Verify Private template created by franchise user who has privilege to see private template by itself
			fc.utobj().printTestStep("Verify Private template created by franchise user who has privilege to see private template by corporate user(C2)");
			campaignCenterPage.searchTemplateByFilter(driver, secondtemplateNamefran);
			
			boolean isTemplateNamePresentfranaccessibilityprivateC2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondtemplateNamefran + "')]");
			if (!isTemplateNamePresentfranaccessibilityprivateC2 == false) {
				fc.utobj().throwsException("was not able to verify private template created by franchise user who has privilege to see private template by corporate user(C2)");
			}
			
			//Verify Public to all users (Status Driven)template created by Corporate user who has privilege to see private template by itself
			fc.utobj().printTestStep("Verify Public to all users of my franchise (Status Driven)template created by franchise user who has privilege to see private template by corporate user(C2)");
			campaignCenterPage.searchTemplateByFilter(driver, firsttemplateNamefranstatus);

			boolean isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchisestatusC2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firsttemplateNamefranstatus + "')]");
			if (isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchisestatusC2 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my franchise (Status Driven)template created by franchise user who has privilege to see private template by corporate user(C2)");
			}
			
			//Verify Private template created by franchise user who has privilege to see private template by itself
			fc.utobj().printTestStep("Verify Private (Status Driven)template created by franchise user who has privilege to see private template by corporate user(C2)");
			campaignCenterPage.searchTemplateByFilter(driver, secondtemplateNamefranstatus);
			
			boolean isTemplateNamePresentfranaccessibilityprivatestatusC2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondtemplateNamefranstatus + "')]");
			if (!isTemplateNamePresentfranaccessibilityprivatestatusC2 == false) {
				fc.utobj().throwsException("was not able to verify private (Status Driven)template created by franchise user who has privilege to see private template by corporate user(C2)");
			}
			fc.utobj().printTestStep("Navigate to CRM>Campaign Center>campaign template");
			emailtemplate.clickoncampaigntemplate(driver);
			fc.utobj().clickElement(driver, crmcenter.locationCampaign);
			fc.utobj().printTestStep("Verifying Campaigns and templates of Franchise Employee user (FE1) by corporate user(C1)");
			
			fc.utobj().printTestStep("Verify Public to all users of my Franchise Campaign created by Franchise Employee user(FE1) who has privilege to see private campaign by corporate user(C2)");
			campaignCenterPage.searchCampaignByFilter(driver, firstcampaignNamefrane);

			boolean isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchiseEC2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firstcampaignNamefrane + "')]");
			if (isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchiseEC2 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my Franchise Campaign created by Franchise Employee user(FE1) who has privilege to see private campaign by corporate user(C2)");
			}
			
			//Verify Private Campaign created by Franchise user who has privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Private Campaign created by Franchise Employee user(FE1) who has privilege to see private campaign by corporate user(C2)");
			campaignCenterPage.searchCampaignByFilter(driver, secondcampaignNamefrane);
			
			boolean isCampaignNamePresentFranpaccessibilityprivateEC2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondcampaignNamefrane + "')]");
			if (!isCampaignNamePresentFranpaccessibilityprivateEC2 == false) {
				fc.utobj().throwsException("was not able to verify Private Campaign created by Franchise Employee user(FE1) who has privilege to see private campaign by corporate user(C2)");
			}
			
			
			//Verify Public to all users of my Franchise (Status Driven)Campaign created by Franchise user who has privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Public to all users of my Franchise (Status Driven)Campaign created by Franchise Employee user(FE1) who has privilege to see private campaign by corporate user(C2)");
			campaignCenterPage.searchCampaignByFilter(driver, firstcampaignNamefranestatus);

			boolean isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchiseEstatusC2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firstcampaignNamefranestatus + "')]");
			if (isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchiseEstatusC2 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my Franchise (Status Driven)Campaign created by Franchise Employee user(FE1) who has privilege to see private campaign by corporate user(C2)");
			}
			
			//Verify Private Campaign created by Franchise user who has privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Private (Status Driven)Campaign created by Franchise Employee user(FE1) who has privilege to see private campaign by corporate user(C2)");
			campaignCenterPage.searchCampaignByFilter(driver, secondcampaignNamefranestatus);
			
			boolean isCampaignNamePresentFranpaccessibilityprivateEstatusC2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondcampaignNamefranestatus + "')]");
			if (!isCampaignNamePresentFranpaccessibilityprivateEstatusC2 == false) {
				fc.utobj().throwsException("was not able to verify Private (Status Driven)Campaign created by Franchise Employee user(FE1) who has privilege to see private campaign by corporate user(C2)");
			}
		
			fc.utobj().printTestStep("Navigate to CRM>Campaign Center>email template");
			emailtemplate.clickonemailtamplate(driver);
			fc.utobj().clickElement(driver, crmcenter.locationtemplate);
			//Verify Public to all users template created by Franchise Employee user user who has privilege to see private template by itself
			fc.utobj().printTestStep("Verify Public to all users of my franchise template created by franchise Employee user(FE1) who has privilege to see private template by corporate user(C2)");
			campaignCenterPage.searchTemplateByFilter(driver, firsttemplateNamefrane);

			boolean isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchiseEC2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firsttemplateNamefrane + "')]");
			if (isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchiseEC2 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my franchise template created by franchise Employee user(FE1) who has privilege to see private template by corporate user(C2)");
			}
			
			//Verify Private template created by franchise Employee user who has privilege to see private template by itself
			fc.utobj().printTestStep("Verify Private template created by franchise Employee user(FE1) who has privilege to see private template by corporate user(C2)");
			campaignCenterPage.searchTemplateByFilter(driver, secondtemplateNamefrane);
			
			boolean isTemplateNamePresentfranaccessibilityprivateEC2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondtemplateNamefrane + "')]");
			if (!isTemplateNamePresentfranaccessibilityprivateEC2 == false) {
				fc.utobj().throwsException("was not able to verify private template created by franchise Employee user(FE1) who has privilege to see private template by corporate user(C2)");
			}
			
			
			//Verify Public to all users (Status Driven)template created by Franchise Employee user user who has privilege to see private template by itself
			fc.utobj().printTestStep("Verify Public to all users of my franchise (Status Driven)template created by franchise Employee user(FE1) who has privilege to see private template by corporate user(C2)");
			campaignCenterPage.searchTemplateByFilter(driver, firsttemplateNamefranestatus);

			boolean isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchiseEstatusC2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firsttemplateNamefranestatus + "')]");
			if (isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchiseEstatusC2 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my franchise (Status Driven)template created by franchise Employee user(FE1) who has privilege to see private template by corporate user(C2)");
			}
			
			//Verify Private template created by franchise Employee user who has privilege to see private template by itself
			fc.utobj().printTestStep("Verify Private (Status Driven)template created by franchise Employee user(FE1) who has privilege to see private template by corporate user(C2)");
			campaignCenterPage.searchTemplateByFilter(driver, secondtemplateNamefranestatus);
			
			boolean isTemplateNamePresentfranaccessibilityprivateEstatusC2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondtemplateNamefranestatus + "')]");
			if (!isTemplateNamePresentfranaccessibilityprivateEstatusC2 == false) {
				fc.utobj().throwsException("was not able to verify private (Status Driven)template created by franchise Employee user(FE1) who has privilege to see private template by corporate user(C2)");
			}
			
			
			fc.utobj().printTestStep("Navigate to CRM>Campaign Center>campaign template");
			emailtemplate.clickoncampaigntemplate(driver);
			fc.utobj().clickElement(driver, crmcenter.locationCampaign);
			fc.utobj().printTestStep("Verifying Campaigns and templates of Franchise Employee user (F02) by corporate user(C2)");
			
			
			//Verify Public to all users of my Franchise Campaign created by Franchise user who does not have privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Public to all users of my Franchise Campaign created by Franchise user(FO2) who does not have privilege to see private campaign by corporate user(C2)");
			campaignCenterPage.searchCampaignByFilter(driver, firstcampaignNamefranO2);

			boolean isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchiseO2C2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firstcampaignNamefranO2 + "')]");
			if (isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchiseO2C2 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my Franchise Campaign created by Franchise user(FO2) who does not have privilege to see private campaign by corporate user(C2)");
			}
			
			//Verify Private Campaign created by Franchise user who has privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Private Campaign created by Franchise user(FO2) who does not have privilege to see private campaign by corporate user(C2)");
			campaignCenterPage.searchCampaignByFilter(driver, secondcampaignNamefranO2);
			
			boolean isCampaignNamePresentFranpaccessibilityprivateO2C2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondcampaignNamefranO2 + "')]");
			if (!isCampaignNamePresentFranpaccessibilityprivateO2C2 == false) {
				fc.utobj().throwsException("was not able to verify Private Campaign created by Franchise user(FO2) who does not have privilege to see private campaign by corporate user(C2)");
			}
			
			//Verify Public to all users of my Franchise (Status Driven)Campaign created by Franchise user who does not have privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Public to all users of my Franchise (Status Driven)Campaign created by Franchise user(FO2) who does not have privilege to see private campaign by corporate user(C2)");
			campaignCenterPage.searchCampaignByFilter(driver, firstcampaignNamefranO2status);

			boolean isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchiseO2statusC2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firstcampaignNamefranO2status + "')]");
			if (isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchiseO2statusC2 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my (Status Driven)Franchise Campaign created by Franchise user(FO2) who does not have privilege to see private campaign by corporate user(C2)");
			}
			
			//Verify Private Campaign created by Franchise user who has privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Private (Status Driven)Campaign created by Franchise user(FO2) who does not have privilege to see private campaign by corporate user(C2)");
			campaignCenterPage.searchCampaignByFilter(driver, secondcampaignNamefranO2status);
			
			boolean isCampaignNamePresentFranpaccessibilityprivateO2statusC2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondcampaignNamefranO2status + "')]");
			if (!isCampaignNamePresentFranpaccessibilityprivateO2statusC2 == false) {
				fc.utobj().throwsException("was not able to verify Private (Status Driven)Campaign created by Franchise user(FO2) who does not have privilege to see private campaign by corporate user(C2)");
			}
		
			fc.utobj().printTestStep("Navigate to CRM>Campaign Center>email template");
			emailtemplate.clickonemailtamplate(driver);
			fc.utobj().clickElement(driver, crmcenter.locationtemplate);
			//Verify Public to all users template created by Corporate user who has privilege to see private template by itself
			fc.utobj().printTestStep("Verify Public to all users of my franchise template created by franchise user(FO2) who does not have privilege to see private template by corporate user(C2)");
			campaignCenterPage.searchTemplateByFilter(driver, firsttemplateNamefranO2);

			boolean isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchiseO2C2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firsttemplateNamefranO2 + "')]");
			if (isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchiseO2C2 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my franchise template created by franchise user(FO2) who does not have privilege to see private template by corporate user(C2)");
			}
			
			//Verify Private template created by franchise user who has privilege to see private template by itself
			fc.utobj().printTestStep("Verify Private template created by franchise user(FO2) who does not have privilege to see private template by corporate user(C2)");
			campaignCenterPage.searchTemplateByFilter(driver, secondtemplateNamefranO2);
			
			boolean isTemplateNamePresentfranaccessibilityprivateO2C2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondtemplateNamefranO2 + "')]");
			if (!isTemplateNamePresentfranaccessibilityprivateO2C2 == false) {
				fc.utobj().throwsException("was not able to verify private template created by franchise user(FO2) who does not have privilege to see private template by corporate user(C2)");
			}
			
			//Verify Public to all users template created by Corporate user who has privilege to see private template by itself
			fc.utobj().printTestStep("Verify Public to all users of my franchise (Status Driven)template created by franchise user(FO2) who does not have privilege to see private template by corporate user(C2)");
			campaignCenterPage.searchTemplateByFilter(driver, firsttemplateNamefranO2status);

			boolean isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchiseO2statusC2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firsttemplateNamefranO2status + "')]");
			if (isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchiseO2statusC2 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my franchise (Status Driven)template created by franchise user(FO2) who does not have privilege to see private template by corporate user(C2)");
			}
			
			//Verify Private template created by franchise user who has privilege to see private template by itself
			fc.utobj().printTestStep("Verify Private (Status Driven)template created by franchise user(FO2) who does not have privilege to see private template by corporate user(C2)");
			campaignCenterPage.searchTemplateByFilter(driver, secondtemplateNamefranO2status);
			
			boolean isTemplateNamePresentfranaccessibilityprivateO2statusC2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondtemplateNamefranO2status + "')]");
			if (!isTemplateNamePresentfranaccessibilityprivateO2statusC2 == false) {
				fc.utobj().throwsException("was not able to verify private (Status Driven)template created by franchise user(FO2) who does not have privilege to see private template by corporate user(C2)");
			}
			
			fc.utobj().printTestStep("Navigate to CRM>Campaign Center>campaign template");
			emailtemplate.clickoncampaigntemplate(driver);
			fc.utobj().clickElement(driver, crmcenter.locationCampaign);
			fc.utobj().printTestStep("Verifying Campaigns and templates of Franchise Employee user (FE2) by corporate user(C2)");
			
			//Verify Public to all users of my Franchise Campaign created by Franchise user who has privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Public to all users of my Franchise Campaign created by Franchise Employee user(FE2) who does not have privilege to see private campaign by corporate user(C2)");
			campaignCenterPage.searchCampaignByFilter(driver, firstcampaignNamefrane2);

			boolean isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchiseE2C2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firstcampaignNamefrane2 + "')]");
			if (isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchiseE2C2 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my Franchise Campaign created by Franchise Employee user(FE2) who does not have privilege to see private campaign by corporate user(C2)");
			}
			
			//Verify Private Campaign created by Franchise user who has privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Private Campaign created by Franchise Employee user(FE2) who does not have privilege to see private campaign by corporate user(C2)");
			campaignCenterPage.searchCampaignByFilter(driver, secondcampaignNamefrane2);
			
			boolean isCampaignNamePresentFranpaccessibilityprivateE2C2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondcampaignNamefrane2 + "')]");
			if (!isCampaignNamePresentFranpaccessibilityprivateE2C2 == false) {
				fc.utobj().throwsException("was not able to verify Private Campaign created by Franchise Employee user(FE2) who does not have privilege to see private campaign by corporate user(C2)");
			}
			
			
			//Verify Public to all users of my Franchise Campaign created by Franchise user who has privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Public to all users of my Franchise (Status Driven)Campaign created by Franchise Employee user(FE2) who does not have privilege to see private campaign by corporate user(C2)");
			campaignCenterPage.searchCampaignByFilter(driver, firstcampaignNamefrane2status);

			boolean isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchiseE2statusC2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firstcampaignNamefrane2status + "')]");
			if (isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchiseE2statusC2 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my Franchise (Status Driven)Campaign created by Franchise Employee user(FE2) who does not have privilege to see private campaign by corporate user(C2)");
			}
			
			//Verify Private Campaign created by Franchise user who has privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Private (Status Driven)Campaign created by Franchise Employee user(FE2) who does not have privilege to see private campaign by corporate user(C2)");
			campaignCenterPage.searchCampaignByFilter(driver, secondcampaignNamefrane2status);
			
			boolean isCampaignNamePresentFranpaccessibilityprivateE2statusC2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondcampaignNamefrane2status + "')]");
			if (!isCampaignNamePresentFranpaccessibilityprivateE2statusC2 == false) {
				fc.utobj().throwsException("was not able to verify Private (Status Driven)Campaign created by Franchise Employee user(FE2) who does not have privilege to see private campaign by corporate user(C2)");
			}
			
		
			fc.utobj().printTestStep("Navigate to CRM>Campaign Center>email template");
			emailtemplate.clickonemailtamplate(driver);
			fc.utobj().clickElement(driver, crmcenter.locationtemplate);
			//Verify Public to all users template created by Franchise Employee user user who has privilege to see private template by itself
			fc.utobj().printTestStep("Verify Public to all users of my franchise template created by franchise Employee user(FE2) who does not have privilege to see private template by corporate user(C2)");
			campaignCenterPage.searchTemplateByFilter(driver, firsttemplateNamefrane2);

			boolean isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchiseE2C2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firsttemplateNamefrane2 + "')]");
			if (isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchiseE2C2 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my franchise template created by franchise Employee user(FE2) who does not have privilege to see private template by corporate user(C2)");
			}
			
			//Verify Private template created by franchise Employee user who has privilege to see private template by itself
			fc.utobj().printTestStep("Verify Private template created by franchise Employee user(FE2) who does not have privilege to see private template by corporate user(C2)");
			campaignCenterPage.searchTemplateByFilter(driver, secondtemplateNamefrane2);
			
			boolean isTemplateNamePresentfranaccessibilityprivateE2C2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondtemplateNamefrane2 + "')]");
			if (!isTemplateNamePresentfranaccessibilityprivateE2C2 == false) {
				fc.utobj().throwsException("was not able to verify private template created by franchise Employee user(FE2) who does not have privilege to see private template by corporate user(C2)");
			}
			
			//Verify Public to all users template created by Franchise Employee user user who has privilege to see private template by itself
			fc.utobj().printTestStep("Verify Public to all users of my franchise (Status Driven)template created by franchise Employee user(FE2) who does not have privilege to see private template by corporate user(C2)");
			campaignCenterPage.searchTemplateByFilter(driver, firsttemplateNamefrane2status);

			boolean isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchiseE2statusC2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firsttemplateNamefrane2status + "')]");
			if (isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchiseE2statusC2 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my franchise(Status Driven) template created by franchise Employee user(FE2) who does not have privilege to see private template by corporate user(C2)");
			}
			
			//Verify Private template created by franchise Employee user who has privilege to see private template by itself
			fc.utobj().printTestStep("Verify Private (Status Driven)template created by franchise Employee user(FE2) who does not have privilege to see private template by corporate user(C2)");
			campaignCenterPage.searchTemplateByFilter(driver, secondtemplateNamefrane2status);
			
			boolean isTemplateNamePresentfranaccessibilityprivateE2statusC2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondtemplateNamefrane2status + "')]");
			if (!isTemplateNamePresentfranaccessibilityprivateE2statusC2 == false) {
				fc.utobj().throwsException("was not able to verify private (Status Driven)template created by franchise Employee user(FE2) who does not have privilege to see private template by corporate user(C2)");
			}
		
			
			fc.utobj().printTestStep("Navigate to CRM>Campaign Center>campaign template");
			emailtemplate.clickoncampaigntemplate(driver);
			fc.utobj().clickElement(driver, crmcenter.locationCampaign);
			fc.utobj().printTestStep("Verifying Campaigns and templates of Franchise Mu user (V1) by corporate user(C2)");
			
			fc.utobj().printTestStep("Verify Public to all users of my Franchise Campaign created by Franchise MU user(V1) who has privilege to see private campaign by corporate user(C2)");
			campaignCenterPage.searchCampaignByFilter(driver, firstcampaignNamefranmu);

			boolean isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchisemuC2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firstcampaignNamefranmu + "')]");
			if (isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchisemuC2 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my Franchise Campaign created by Franchise MU user(V1) who has privilege to see private campaign by corporate user(C2)");
			}
			
			//Verify Private Campaign created by Franchise user who has privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Private Campaign created by Franchise MU user(V1) who has privilege to see private campaign by corporate user(C2)");
			campaignCenterPage.searchCampaignByFilter(driver, secondcampaignNamefranmu);
			
			boolean isCampaignNamePresentFranpaccessibilityprivatemuC2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondcampaignNamefranmu + "')]");
			if (!isCampaignNamePresentFranpaccessibilityprivatemuC2 == false) {
				fc.utobj().throwsException("was not able to verify Private Campaign created by Franchise MU user(V1) who has privilege to see private campaign by corporate user(C2)");
			}
			
			
			//Verify Public to all users of my Franchise (Status Driven)Campaign created by Franchise user who has privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Public to all users of my Franchise (Status Driven)Campaign created by Franchise MU user(V1) who has privilege to see private campaign by corporate user(C2)");
			campaignCenterPage.searchCampaignByFilter(driver, firstcampaignNamefranmustatus);

			boolean isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchisemustatusC2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firstcampaignNamefranmustatus + "')]");
			if (isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchisemustatusC2 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my Franchise (Status Driven)Campaign created by Franchise MU user(V1) who has privilege to see private campaign by corporate user(C2)");
			}
			
			//Verify Private Campaign created by Franchise user who has privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Private (Status Driven)Campaign created by Franchise MU user(V1) who has privilege to see private campaign by corporate user(C2)");
			campaignCenterPage.searchCampaignByFilter(driver, secondcampaignNamefranmustatus);
			
			boolean isCampaignNamePresentFranpaccessibilityprivatemustatusC2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondcampaignNamefranmustatus + "')]");
			if (!isCampaignNamePresentFranpaccessibilityprivatemustatusC2 == false) {
				fc.utobj().throwsException("was not able to verify Private (Status Driven)Campaign created by Franchise MU user(V1) who has privilege to see private campaign by corporate user(C2)");
			}
			
			fc.utobj().printTestStep("Navigate to CRM>Campaign Center>email template");
			emailtemplate.clickonemailtamplate(driver);
			fc.utobj().clickElement(driver, crmcenter.locationtemplate);
			//Verify Public to all users template created by Corporate user who has privilege to see private template by itself
			fc.utobj().printTestStep("Verify Public to all users of my franchise template created by franchise MU user(V1) who has privilege to see private template by corporate user(C2)");
			campaignCenterPage.searchTemplateByFilter(driver, firsttemplateNamefranmu);

			boolean isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchisemuC2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firsttemplateNamefranmu + "')]");
			if (isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchisemuC2 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my franchise template created by franchise MU user(V1) who has privilege to see private template by corporate user(C2)");
			}
			
			//Verify Private template created by franchise user who has privilege to see private template by itself
			fc.utobj().printTestStep("Verify Private template created by franchise MU user(V1) who has privilege to see private template by corporate user(C2)");
			campaignCenterPage.searchTemplateByFilter(driver, secondtemplateNamefranmu);
			
			boolean isTemplateNamePresentfranaccessibilityprivatemuC2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondtemplateNamefranmu + "')]");
			if (!isTemplateNamePresentfranaccessibilityprivatemuC2 == false) {
				fc.utobj().throwsException("was not able to verify private template created by franchise MU user(V1) who has privilege to see private template by corporate user(C2)");
			}
			
			//Verify Public to all users template created by Corporate user who has privilege to see private template by itself
			fc.utobj().printTestStep("Verify Public to all users of my franchise (Status Driven)template created by franchise MU user(V1) who has privilege to see private template by corporate user(C2)");
			campaignCenterPage.searchTemplateByFilter(driver, firsttemplateNamefranmustatus);

			boolean isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchisemustatusC2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firsttemplateNamefranmustatus + "')]");
			if (isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchisemustatusC2 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my franchise (Status Driven)template created by franchise MU user(V1) who has privilege to see private template by corporate user(C2)");
			}
			
			//Verify Private template created by franchise user who has privilege to see private template by itself
			fc.utobj().printTestStep("Verify Private (Status Driven)template created by franchise MU user(V1) who has privilege to see private template by corporate user(C2)");
			campaignCenterPage.searchTemplateByFilter(driver, secondtemplateNamefranmustatus);
			
			boolean isTemplateNamePresentfranaccessibilityprivatemustatusC2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondtemplateNamefranmustatus + "')]");
			if (!isTemplateNamePresentfranaccessibilityprivatemustatusC2 == false) {
				fc.utobj().throwsException("was not able to verify private (Status Driven)template created by franchise MU user(V1) who has privilege to see private template by corporate user(C2)");
			}
			
			
			
			
			fc.utobj().printTestStep("Navigate to CRM>Campaign Center>campaign template");
			emailtemplate.clickoncampaigntemplate(driver);
			fc.utobj().clickElement(driver, crmcenter.locationCampaign);
			fc.utobj().printTestStep("Verifying Campaigns and templates of Franchise Employee user (V2) by corporate user(C1)");
			fc.utobj().printTestStep("Verify Public to all users of my Franchise Campaign created by Franchise Employee user(V2) who does not have privilege to see private campaign by corporate user(C2)");
			campaignCenterPage.searchCampaignByFilter(driver, firstcampaignNamefranmue2);

			boolean isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchisemue2C2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firstcampaignNamefranmue2 + "')]");
			if (isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchisemue2C2 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my Franchise Campaign created by Franchise Employee user(V2) who does not have privilege to see private campaign by corporate user(C2)");
			}
			
			//Verify Private Campaign created by Franchise user who has privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Private Campaign created by Franchise Employee user(V2) who does not have privilege to see private campaign by corporate user(C2)");
			campaignCenterPage.searchCampaignByFilter(driver, secondcampaignNamefranmue2);
			
			boolean isCampaignNamePresentFranpaccessibilityprivatemue2C2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondcampaignNamefranmue2 + "')]");
			if (!isCampaignNamePresentFranpaccessibilityprivatemue2C2 == false) {
				fc.utobj().throwsException("was not able to verify Private Campaign created by Franchise Employee user(V2) who does not have privilege to see private campaign by corporate user(C2)");
			}
			
			
			//Verify Public to all users of my Franchise Campaign created by Franchise Employee user(V2) who has privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Public to all users of my Franchise (Status Driven)Campaign created by Franchise Employee user(V2) who does not have privilege to see private campaign by corporate user(C2)");
			campaignCenterPage.searchCampaignByFilter(driver, firstcampaignNamefranmue2status);

			boolean isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchisemue2statusC2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firstcampaignNamefranmue2status + "')]");
			if (isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchisemue2statusC2 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my Franchise (Status Driven)Campaign created by Franchise Employee user(V2) who does not have privilege to see private campaign by corporate user(C2)");
			}
			
			//Verify Private Campaign created by Franchise user who has privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Private (Status Driven)Campaign created by Franchise Employee user(V2) who does not have privilege to see private campaign by corporate user(C2)");
			campaignCenterPage.searchCampaignByFilter(driver, secondcampaignNamefranmue2status);
			
			boolean isCampaignNamePresentFranpaccessibilityprivatemue2statusC2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondcampaignNamefranmue2status + "')]");
			if (!isCampaignNamePresentFranpaccessibilityprivatemue2statusC2 == false) {
				fc.utobj().throwsException("was not able to verify Private (Status Driven)Campaign created by Franchise Employee user(V2) who does not have privilege to see private campaign by corporate user(C2)");
			}
			
			fc.utobj().printTestStep("Navigate to CRM>Campaign Center>email template");
			emailtemplate.clickonemailtamplate(driver);
			fc.utobj().clickElement(driver, crmcenter.locationtemplate);
			//Verify Public to all users template created by Franchise user who has privilege to see private template by itself
			fc.utobj().printTestStep("Verify Public to all users of my franchise template created by franchise Employee user(V2) who does not have privilege to see private template by corporate user(C2)");
			campaignCenterPage.searchTemplateByFilter(driver, firsttemplateNamefranmue2);

			boolean isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchisemue2C2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firsttemplateNamefranmue2 + "')]");
			if (isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchisemue2C2 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my franchise template created by franchise Employee user(V2) who has privilege to see private template by corporate user(C2)");
			}
			
			//Verify Private template created by franchise user who has privilege to see private template by itself
			fc.utobj().printTestStep("Verify Private template created by franchise Employee user(V2) who has privilege to see private template by corporate user(C2");
			campaignCenterPage.searchTemplateByFilter(driver, secondtemplateNamefranmue2);
			
			boolean isTemplateNamePresentfranaccessibilityprivatemue2C2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondtemplateNamefranmue2 + "')]");
			if (!isTemplateNamePresentfranaccessibilityprivatemue2C2 == false) {
				fc.utobj().throwsException("was not able to verify private template created by franchise Employee user(V2) who has privilege to see private template by corporate user(C2)");
			}
			
			//Verify Public to all users template created by Franchise user who has privilege to see private template by itself
			fc.utobj().printTestStep("Verify Public to all users of my franchise (Status Driven)template created by franchise Employee user(V2) who does not have privilege to see private template by corporate user(C2)");
			campaignCenterPage.searchTemplateByFilter(driver, firsttemplateNamefranmue2status);

			boolean isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchisemue2statusC2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firsttemplateNamefranmue2status + "')]");
			if (isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchisemue2statusC2 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my franchise (Status Driven)template created by franchise Employee user(V2) who has privilege to see private template by corporate user(C2)");
			}
			
			//Verify Private template created by franchise user who has privilege to see private template by itself
			fc.utobj().printTestStep("Verify Private (Status Driven)template created by franchise Employee user(V2) who has privilege to see private template by corporate user(C2)");
			campaignCenterPage.searchTemplateByFilter(driver, secondtemplateNamefranmue2status);
			
			boolean isTemplateNamePresentfranaccessibilityprivatemue2statusC2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondtemplateNamefranmue2status + "')]");
			if (!isTemplateNamePresentfranaccessibilityprivatemue2statusC2 == false) {
				fc.utobj().throwsException("was not able to verify private (Status Driven)template created by franchise Employee user(V2) who has privilege to see private template by corporate user(C2)");
			}
			
			if(isCampaignNamePresentCorpaccessibilitypublictoalluserscorpC2 && isCampaignNamePresentCorpaccessibilitypublictoallcorporateuserscorpC2 && isCampaignNamePresentCorpaccessibilityprivatecorpC2 
					&& isCampaignNamePresentCorpaccessibilitypublictoalluserscorpstatusC2 && isCampaignNamePresentCorpaccessibilitypublictoallcorporateuserscorpstatusC2 && isCampaignNamePresentCorpaccessibilityprivatecorpstatusC2 
					&& isTemplateNamePresentCorpaccessibilitypublictoalluserscorpC2 && isTemplateNamePresentCorpaccessibilitypublictoallcorporateuserscorpC2 && isTemplateNamePresentCorpaccessibilityprivatecorpC2 
					&& isTemplateNamePresentCorpaccessibilitypublictoalluserscorpstatusC2 && isTemplateNamePresentCorpaccessibilitypublictoallcorporateuserscorpstatusC2 && isTemplateNamePresentCorpaccessibilityprivatecorpstatusC2 
					&& isCampaignNamePresentRegpaccessibilitypublictoallusersofmyregionC2 && !isCampaignNamePresentRegpaccessibilityprivateC2 && isCampaignNamePresentRegpaccessibilitypublictoallusersofmyregionstatusC2 
					&& !isCampaignNamePresentRegpaccessibilityprivatestatusC2 && isTemplateNamePresentRegaccessibilitypublictoallusersofmyregionC2 && !isTemplateNamePresentRegaccessibilityprivateC2 
					&& isTemplateNamePresentRegaccessibilitypublictoallusersofmyregionstatusC2 && !isTemplateNamePresentRegaccessibilityprivatestatusC2 && isCampaignNamePresentCorpaccessibilitypublictoallusersC2 
					&& isCampaignNamePresentCorpaccessibilitypublictoallcorporateusersC2 && !isCampaignNamePresentCorpaccessibilityprivateC2 && isCampaignNamePresentCorpaccessibilitypublictoallusersstatusC2 
					&& isCampaignNamePresentCorpaccessibilitypublictoallcorporateusersstatusC2 && !isCampaignNamePresentCorpaccessibilityprivatestatusC2 && isTemplateNamePresentCorpaccessibilitypublictoallusersC2 
					&& isTemplateNamePresentCorpaccessibilitypublictoallcorporateusersC2 && !isTemplateNamePresentCorpaccessibilityprivateC2 && isTemplateNamePresentCorpaccessibilitypublictoallusersstatusC2 
					&& isTemplateNamePresentCorpaccessibilitypublictoallcorporateusersstatusC2 && !isTemplateNamePresentCorpaccessibilityprivatestatusC2 && isCampaignNamePresentRegpaccessibilitypublictoallusersofmyregion1C2
					&& !isCampaignNamePresentRegpaccessibilityprivate1C2 && isCampaignNamePresentRegpaccessibilitypublictoallusersofmyregion1statusC2 && !isCampaignNamePresentRegpaccessibilityprivate1statusC2
					&& isTemplateNamePresentRegaccessibilitypublictoallusersofmyregion1C2 && !isTemplateNamePresentRegaccessibilityprivate1C2 && isTemplateNamePresentRegaccessibilitypublictoallusersofmyregion1statusC2 
					&& !isTemplateNamePresentRegaccessibilityprivate1statusC2 && isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchiseC2 && !isCampaignNamePresentFranpaccessibilityprivateC2 
					&& isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchisestatusC2 && !isCampaignNamePresentFranpaccessibilityprivatestatusC2 && isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchiseC2 
					&& !isTemplateNamePresentfranaccessibilityprivateC2 && isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchisestatusC2 && !isTemplateNamePresentfranaccessibilityprivatestatusC2 
					&& isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchiseEC2 && !isCampaignNamePresentFranpaccessibilityprivateEC2 && isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchiseEstatusC2 
					&& !isCampaignNamePresentFranpaccessibilityprivateEstatusC2 && isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchiseEC2 && !isTemplateNamePresentfranaccessibilityprivateEC2 
					&& isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchiseEstatusC2 && !isTemplateNamePresentfranaccessibilityprivateEstatusC2 && isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchiseO2C2 
					&& !isCampaignNamePresentFranpaccessibilityprivateO2C2 && isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchiseO2statusC2 && !isCampaignNamePresentFranpaccessibilityprivateO2statusC2 
					&& isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchiseO2C2 && !isTemplateNamePresentfranaccessibilityprivateO2C2 && isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchiseO2statusC2 
					&& !isTemplateNamePresentfranaccessibilityprivateO2statusC2 && isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchiseE2C2 && !isCampaignNamePresentFranpaccessibilityprivateE2C2 
					&& isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchiseE2statusC2 && !isCampaignNamePresentFranpaccessibilityprivateE2statusC2 && isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchiseE2C2
					&& !isTemplateNamePresentfranaccessibilityprivateE2C2 && isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchiseE2statusC2 && !isTemplateNamePresentfranaccessibilityprivateE2statusC2 
					&& isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchisemuC2 && !isCampaignNamePresentFranpaccessibilityprivatemuC2 && isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchisemustatusC2 
					&& !isCampaignNamePresentFranpaccessibilityprivatemustatusC2 && isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchisemuC2 && !isTemplateNamePresentfranaccessibilityprivatemuC2 
					&& isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchisemustatusC2 && !isTemplateNamePresentfranaccessibilityprivatemustatusC2 && isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchisemue2C2 
					&& !isCampaignNamePresentFranpaccessibilityprivatemue2C2 && isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchisemue2statusC2 && !isCampaignNamePresentFranpaccessibilityprivatemue2statusC2 
					&& isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchisemue2C2 && !isTemplateNamePresentfranaccessibilityprivatemue2C2 && isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchisemue2statusC2
					&& !isTemplateNamePresentfranaccessibilityprivatemue2statusC2) {
				fc.utobj().printTestStep("able to verify all types of campaigns and templates created by corporate/rgional/franchise users when loging with Corporate user(C2) who does not have priviledge to see private campaign");
			}
			else {
				fc.utobj().throwsException("was not able to verify all types of campaigns and templates created by corporate/rgional/franchise users when loging with Corporate user(C2) who does not have priviledge to see private campaign");
			}
				
			
			
			
			//Login with Regional user(R1) who has role to see private campaign
			fc.utobj().printTestStep("Login with Regional user(R1) who has role to see private campaign");
			fc.loginpage().loginWithParameter(driver, Reguname, Regpassword);
			fc.crm().crm_common().openCRMCampaignCenterPage(fcHomePageTest, driver);
			fc.utobj().printTestStep("Click on the menu option and select campaign");
			/*CRMCampaignCenterPage pobj = new CRMCampaignCenterPage(driver);*/
			fc.utobj().clickElement(driver, pobj.menuOptions);
			fc.utobj().clickElement(driver, pobj.campaignLinks);
			 
			//Verifying Campaigns and templates of corporate user (C2) after logging with Regional user (R1)
			fc.utobj().printTestStep("Verifying Campaigns and templates of corporate user (C2) after logging with Regional user (R1)");
			fc.utobj().printTestStep("Verify Public to all users Campaign created by Corporate user(C2) who does not have privilege to see private campaign by Regional user (R1)");
			campaignCenterPage.searchCampaignByFilter(driver, firstcampaignNamecorp);

			boolean isCampaignNamePresentCorpaccessibilitypublictoalluserscorpR1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firstcampaignNamecorp + "')]");
			if (isCampaignNamePresentCorpaccessibilitypublictoalluserscorpR1 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users Campaign created by Corporate user who does not have privilege to see private campaign by Regional user (R1)");
			}
			//Verify Public to all corporate users Campaign created by Corporate user who has privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Public to all corporate users Campaign created by Corporate user who does not have privilege to see private campaign by Regional user (R1)");
			campaignCenterPage.searchCampaignByFilter(driver, secondcampaignNamecorp);
			
			boolean isCampaignNamePresentCorpaccessibilitypublictoallcorporateuserscorpR1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondcampaignNamecorp + "')]");
			if (!isCampaignNamePresentCorpaccessibilitypublictoallcorporateuserscorpR1 == false) {
				fc.utobj().throwsException("was not able to verify Public to all corporate users Campaign created by Corporate user who does not privilege to see private campaign by Regional user (R1)");
			}
			
			//Verify Private Campaign created by Corporate user who has privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Private Campaign created by Corporate user who does not have privilege to see private campaign by Regional user (R1)");
			campaignCenterPage.searchCampaignByFilter(driver, thirdcampaignNamecorp);
			
			boolean isCampaignNamePresentCorpaccessibilityprivatecorpR1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + thirdcampaignNamecorp + "')]");
			if (!isCampaignNamePresentCorpaccessibilityprivatecorpR1 == false) {
				fc.utobj().throwsException("was not able to Private Campaign created by Corporate user who does not have privilege to see private campaign by Regional user (R1)");
			}
		
			//Verify Public to all users (Status Driven)Campaign created by Corporate user who does not have privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Public to all users(Status Driven) Campaign created by Corporate user who does not have privilege to see private campaign by Regional user (R1)");
			campaignCenterPage.searchCampaignByFilter(driver, firstcampaignNamecorpstatus);

			boolean isCampaignNamePresentCorpaccessibilitypublictoalluserscorpstatusR1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firstcampaignNamecorpstatus + "')]");
			if (isCampaignNamePresentCorpaccessibilitypublictoalluserscorpstatusR1 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users(Status Driven) Campaign created by Corporate user who does not have privilege to see private campaign by Regional user (R1)");
			}
			//Verify Public to all corporate users Campaign created by Corporate user who has privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Public to all corporate users(Status Driven) Campaign created by Corporate user who does not have privilege to see private campaign by Regional user (R1)");
			campaignCenterPage.searchCampaignByFilter(driver, secondcampaignNamecorpstatus);
			
			boolean isCampaignNamePresentCorpaccessibilitypublictoallcorporateuserscorpstatusR1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondcampaignNamecorpstatus + "')]");
			if (!isCampaignNamePresentCorpaccessibilitypublictoallcorporateuserscorpstatusR1 == false) {
				fc.utobj().throwsException("was not able to verify Public to all corporate users(Status Driven) Campaign created by Corporate user who does not privilege to see private campaign by Regional user (R1)");
			}
			
			//Verify Private Campaign created by Corporate user who has privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Private Campaign created by Corporate user(Status Driven) who does not have privilege to see private campaign by Regional user (R1)");
			campaignCenterPage.searchCampaignByFilter(driver, thirdcampaignNamecorpstatus);
			
			boolean isCampaignNamePresentCorpaccessibilityprivatecorpstatusR1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + thirdcampaignNamecorpstatus + "')]");
			if (!isCampaignNamePresentCorpaccessibilityprivatecorpstatusR1 == false) {
				fc.utobj().throwsException("was not able to Private Campaign created by Corporate user(Status Driven) who does not have privilege to see private campaign by Regional user (R1)");
			}
				
			fc.utobj().printTestStep("Navigate to CRM>Campaign Center>email template");
			emailtemplate.clickonemailtamplate(driver);
			
			//Verify Public to all users template created by Corporate user who does not have privilege to see private template by itself
			fc.utobj().printTestStep("Verify Public to all users template created by Corporate user who does not have privilege to see private template by Regional user (R1)");
			campaignCenterPage.searchTemplateByFilter(driver, firsttemplateNamecorp);

			boolean isTemplateNamePresentCorpaccessibilitypublictoalluserscorpR1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firsttemplateNamecorp + "')]");
			if (isTemplateNamePresentCorpaccessibilitypublictoalluserscorpR1 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users template created by Corporate user who has privilege to see private template by Regional user (R1)");
			}
			//Verify Public to all corporate users template created by Corporate user who has privilege to see private template by itself
			fc.utobj().printTestStep("Verify Public to all corporate users template created by Corporate user who does not have privilege to see private template by Regional user (R1)");
			campaignCenterPage.searchTemplateByFilter(driver, secondtemplateNamecorp);
			
			boolean isTemplateNamePresentCorpaccessibilitypublictoallcorporateuserscorpR1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondtemplateNamecorp + "')]");
			if (!isTemplateNamePresentCorpaccessibilitypublictoallcorporateuserscorpR1 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users template created by Corporate user who has privilege to see private template by Regional user (R1)");
			}
			
			//Verify Private template created by Corporate user who has privilege to see private template by itself
			fc.utobj().printTestStep("Verify Privare template created by Corporate user who does not have privilege to see private template by Regional user (R1)");
			campaignCenterPage.searchTemplateByFilter(driver, thirdtemplateNamecorp);
			
			boolean isTemplateNamePresentCorpaccessibilityprivatecorpR1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + thirdtemplateNamecorp + "')]");
			if (!isTemplateNamePresentCorpaccessibilityprivatecorpR1 == false) {
				fc.utobj().throwsException("was not able to verify private template created by Corporate user who has privilege to see private template by Regional user (R1)");
			}
			
			//Verify Public to all users (Status driven)template created by Corporate user who does not have privilege to see private template by itself
			fc.utobj().printTestStep("Verify Public to all users (Status driven)template created by Corporate user who does not have privilege to see private template by Regional user (R1)");
			campaignCenterPage.searchTemplateByFilter(driver, firsttemplateNamecorpstatus);

			boolean isTemplateNamePresentCorpaccessibilitypublictoalluserscorpstatusR1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firsttemplateNamecorpstatus + "')]");
			if (isTemplateNamePresentCorpaccessibilitypublictoalluserscorpstatusR1 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users (Status driven)template created by Corporate user who has privilege to see private template by Regional user (R1)");
			}
			//Verify Public to all corporate users template created by Corporate user who has privilege to see private template by itself
			fc.utobj().printTestStep("Verify Public to all corporate users (Status driven)template created by Corporate user who does not have privilege to see private template by Regional user (R1)");
			campaignCenterPage.searchTemplateByFilter(driver, secondtemplateNamecorpstatus);
			
			boolean isTemplateNamePresentCorpaccessibilitypublictoallcorporateuserscorpstatusR1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondtemplateNamecorpstatus + "')]");
			if (!isTemplateNamePresentCorpaccessibilitypublictoallcorporateuserscorpstatusR1 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users (Status driven)template created by Corporate user who has privilege to see private template by Regional user (R1)");
			}
			
			//Verify Private template created by Corporate user who has privilege to see private template by Regional user(R1)
			fc.utobj().printTestStep("Verify Privare (Status driven)template created by Corporate user who does not have privilege to see private template by Regional user (R1)");
			campaignCenterPage.searchTemplateByFilter(driver, thirdtemplateNamecorpstatus);
			
			boolean isTemplateNamePresentCorpaccessibilityprivatecorpstatusR1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + thirdtemplateNamecorpstatus + "')]");
			if (!isTemplateNamePresentCorpaccessibilityprivatecorpstatusR1 == false) {
				fc.utobj().throwsException("was not able to verify private (Status driven)template created by Corporate user who has privilege to see private template by Regional user (R1)");
			}
		
			fc.utobj().printTestStep("Navigate to CRM>Campaign Center>campaign template");
			emailtemplate.clickoncampaigntemplate(driver);
			//Verify Public to all users of my region Campaign created by Regional user who has privilege to see private campaign by itself(R1)
			//Verifying Campaigns and templates of Regional user (R1) by itself
			fc.utobj().printTestStep("Verifying Campaigns and templates of Regional user (R1) by itself");
			fc.utobj().printTestStep("Verify Public to all users of my region Campaign created by Regional user who has privilege to see private campaign by itself");
			campaignCenterPage.searchCampaignByFilter(driver, firstcampaignNamereg);

			boolean isCampaignNamePresentRegpaccessibilitypublictoallusersofmyregionR1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firstcampaignNamereg + "')]");
			if (isCampaignNamePresentRegpaccessibilitypublictoallusersofmyregionR1 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my region Campaign created by Regional user who has privilege to see private campaign by itself");
			}
			
			//Verify Private Campaign created by Regional user who has privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Private Campaign created by Regional user who has privilege to see private campaign by itself");
			campaignCenterPage.searchCampaignByFilter(driver, secondcampaignNamereg);
			
			boolean isCampaignNamePresentRegpaccessibilityprivateR1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondcampaignNamereg + "')]");
			if (isCampaignNamePresentRegpaccessibilityprivateR1 == false) {
				fc.utobj().throwsException("was not able to verify Private Campaign created by Regional user who has privilege to see private campaign by itself");
			}
			
			//Verify Public to all users of my region (Status Driven)Campaign created by Regional user who has privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Public to all users of my region (Status Driven)Campaign created by Regional user who has privilege to see private campaign by itself");
			campaignCenterPage.searchCampaignByFilter(driver, firstcampaignNameregstatus);

			boolean isCampaignNamePresentRegpaccessibilitypublictoallusersofmyregionstatusR1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firstcampaignNameregstatus + "')]");
			if (isCampaignNamePresentRegpaccessibilitypublictoallusersofmyregionstatusR1 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my region (Status Driven)Campaign created by Regional user who has privilege to see private campaign by itself");
			}
			
			//Verify Private Campaign created by Regional user who has privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Private (Status Driven)Campaign created by Regional user who has privilege to see private campaign by itself");
			campaignCenterPage.searchCampaignByFilter(driver, secondcampaignNameregstatus);
			
			boolean isCampaignNamePresentRegpaccessibilityprivatestatusR1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondcampaignNameregstatus + "')]");
			if (isCampaignNamePresentRegpaccessibilityprivatestatusR1 == false) {
				fc.utobj().throwsException("was not able to verify Private (Status Driven)Campaign created by Regional user who has privilege to see private campaign by itself");
			}
			
			fc.utobj().printTestStep("Navigate to CRM>Campaign Center>email template");
			emailtemplate.clickonemailtamplate(driver);
			
			//Verify Public to all users template created by Corporate user who has privilege to see private template by itself
			fc.utobj().printTestStep("Verify Public to all users of my region template created by Regional user who has privilege to see private template by itself");
			campaignCenterPage.searchTemplateByFilter(driver, firsttemplateNamereg);

			boolean isTemplateNamePresentRegaccessibilitypublictoallusersofmyregionR1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firsttemplateNamereg + "')]");
			if (isTemplateNamePresentRegaccessibilitypublictoallusersofmyregionR1 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users template created by Regional user who has privilege to see private template by itself");
			}
			
			//Verify Private template created by Regional user who has privilege to see private template by itself
			fc.utobj().printTestStep("Verify Private template created by Regional user who has privilege to see private template by itself");
			campaignCenterPage.searchTemplateByFilter(driver, secondtemplateNamereg);
			
			boolean isTemplateNamePresentRegaccessibilityprivateR1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondtemplateNamereg + "')]");
			if (isTemplateNamePresentRegaccessibilityprivateR1 == false) {
				fc.utobj().throwsException("was not able to verify private template created by Regional user who has privilege to see private template by itself");
			}
			
			//Verify Public to all users(Status Driven) template created by Regional user who has privilege to see private template by itself
			fc.utobj().printTestStep("Verify Public to all users of my region (Status Driven)template created by Regional user who has privilege to see private template by itself");
			campaignCenterPage.searchTemplateByFilter(driver, firsttemplateNameregstatus);

			boolean isTemplateNamePresentRegaccessibilitypublictoallusersofmyregionstatusR1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firsttemplateNameregstatus + "')]");
			if (isTemplateNamePresentRegaccessibilitypublictoallusersofmyregionstatusR1 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my region (Status Driven)template created by Regional user who has privilege to see private template by itself");
			}
			
			//Verify Private template created by Regional user who has privilege to see private template by itself
			fc.utobj().printTestStep("Verify Private (Status Driven)template created by Regional user who has privilege to see private template by itself");
			campaignCenterPage.searchTemplateByFilter(driver, secondtemplateNameregstatus);
			
			boolean isTemplateNamePresentRegaccessibilityprivatestatusR1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondtemplateNameregstatus + "')]");
			if (isTemplateNamePresentRegaccessibilityprivatestatusR1 == false) {
				fc.utobj().throwsException("was not able to verify private (Status Driven)template created by Regional user who has privilege to see private template by itself");
			}
			fc.utobj().printTestStep("Navigate to CRM>Campaign Center>campaign template");
			emailtemplate.clickoncampaigntemplate(driver);
			
			//Verify Public to all users Campaign created by Corporate user who has privilege to see private campaign by Regional user (R1
			fc.utobj().printTestStep("Verifying Campaigns and templates of Corporate user (C1) by Regional user (R1");
			fc.utobj().printTestStep("Verify Public to all users Campaign created by Corporate user who has privilege to see private campaign by Regional user (R1)");
			campaignCenterPage.searchCampaignByFilter(driver, firstcampaignName);

			boolean isCampaignNamePresentCorpaccessibilitypublictoallusersR1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firstcampaignName + "')]");
			if (isCampaignNamePresentCorpaccessibilitypublictoallusersR1 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users Campaign created by Corporate user who has privilege to see private campaign by Regional user (R1)");
			}
			//Verify Public to all corporate users Campaign created by Corporate user who has privilege to see private campaign by Regional user (R1)
			fc.utobj().printTestStep("Verify Public to all corporate users Campaign created by Corporate user who has privilege to see private campaign");
			campaignCenterPage.searchCampaignByFilter(driver, secondcampaignName);
			
			boolean isCampaignNamePresentCorpaccessibilitypublictoallcorporateusersR1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondcampaignName + "')]");
			if (!isCampaignNamePresentCorpaccessibilitypublictoallcorporateusersR1 == false) {
				fc.utobj().throwsException("was not able to verify Public to all corporate users Campaign created by Corporate user who has privilege to see private campaign by Regional user (R1)");
			}
			
			//Verify Private Campaign created by Corporate user who has privilege to see private campaign by Regional user (R1
			fc.utobj().printTestStep("Verify Private Campaign created by Corporate user who has privilege to see private campaign by Regional user (R1)");
			campaignCenterPage.searchCampaignByFilter(driver, thirdcampaignName);
			
			boolean isCampaignNamePresentCorpaccessibilityprivateR1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + thirdcampaignName + "')]");
			if (!isCampaignNamePresentCorpaccessibilityprivateR1 == false) {
				fc.utobj().throwsException("was not able to Private Campaign created by Corporate user who has privilege to see private campaign by Regional user (R1)");
			}
			
			//Verify Public to all users(Status Driven) Campaign created by Corporate user who has privilege to see private campaign by Regional user (R1)
			fc.utobj().printTestStep("Verify Public to all users(Status Driven) Campaign created by Corporate user who has privilege to see private campaign by Regional user (R1)");
			campaignCenterPage.searchCampaignByFilter(driver, firstcampaignNamestatus);

			boolean isCampaignNamePresentCorpaccessibilitypublictoallusersstatusR1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firstcampaignNamestatus + "')]");
			if (isCampaignNamePresentCorpaccessibilitypublictoallusersstatusR1 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users(Status Driven) Campaign created by Corporate user who has privilege to see private campaign by Regional user (R1)");
			}
			//Verify Public to all corporate users(Status Driven) Campaign created by Corporate user who has privilege to see private campaign by Regional user (R1)
			fc.utobj().printTestStep("Verify Public to all corporate users(Status Driven) Campaign created by Corporate user who has privilege to see private campaign");
			campaignCenterPage.searchCampaignByFilter(driver, secondcampaignNamestatus);
			
			boolean isCampaignNamePresentCorpaccessibilitypublictoallcorporateusersstatusR1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondcampaignNamestatus + "')]");
			if (!isCampaignNamePresentCorpaccessibilitypublictoallcorporateusersstatusR1 == false) {
				fc.utobj().throwsException("was not able to verify Public to all corporate users(Status Driven) Campaign created by Corporate user who has privilege to see private campaign by Regional user (R1)");
			}
			
			//Verify Private Campaign created by Corporate user(Status Driven) who has privilege to see private campaign by Regional user (R1)
			fc.utobj().printTestStep("Verify Private Campaign(Status Driven) created by Corporate user who has privilege to see private campaign by Regional user (R1)");
			campaignCenterPage.searchCampaignByFilter(driver, thirdcampaignNamestatus);
			
			boolean isCampaignNamePresentCorpaccessibilityprivatestatusR1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + thirdcampaignNamestatus + "')]");
			if (!isCampaignNamePresentCorpaccessibilityprivatestatusR1 == false) {
				fc.utobj().throwsException("was not able to Private Campaign(Status Driven) created by Corporate user who has privilege to see private campaign by Regional user (R1)");
			}
		
			fc.utobj().printTestStep("Navigate to CRM>Campaign Center>email template");
			emailtemplate.clickonemailtamplate(driver);
			
			//Verify Public to all users template created by Corporate user who has privilege to see private template by Regional user (R1)
			fc.utobj().printTestStep("Verify Public to all users template created by Corporate user who has privilege to see private template by Regional user (R1)");
			campaignCenterPage.searchTemplateByFilter(driver, firsttemplateName);

			boolean isTemplateNamePresentCorpaccessibilitypublictoallusersR1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firsttemplateName + "')]");
			if (isTemplateNamePresentCorpaccessibilitypublictoallusersR1 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users template created by Corporate user who has privilege to see private template by Regional user (R1)");
			}
			//Verify Public to all corporate users template created by Corporate user who has privilege to see private template by Regional user (R1)
			fc.utobj().printTestStep("Verify Public to all corporate users template created by Corporate user who has privilege to see private template by Regional user (R1)");
			campaignCenterPage.searchTemplateByFilter(driver, secondtemplateName);
			
			boolean isTemplateNamePresentCorpaccessibilitypublictoallcorporateusersR1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondtemplateName + "')]");
			if (!isTemplateNamePresentCorpaccessibilitypublictoallcorporateusersR1 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users template created by Corporate user who has privilege to see private template by Regional user (R1)");
			}
			
			//Verify Private template created by Corporate user who has privilege to see private template by Regional user (R1)
			fc.utobj().printTestStep("Verify Privare template created by Corporate user who has privilege to see private template by Regional user (R1)");
			campaignCenterPage.searchTemplateByFilter(driver, thirdtemplateName);
			
			boolean isTemplateNamePresentCorpaccessibilityprivateR1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + thirdtemplateName + "')]");
			if (!isTemplateNamePresentCorpaccessibilityprivateR1 == false) {
				fc.utobj().throwsException("was not able to verify private template created by Corporate user who has privilege to see private template by Regional user (R1");
			}
				
			//Verify Public to all users template(Status Driven) created by Corporate user who has privilege to see private template by Regional user (R1)
			fc.utobj().printTestStep("Verify Public to all users template(Status Driven) created by Corporate user who has privilege to see private template by Regional user (R1)");
			campaignCenterPage.searchTemplateByFilter(driver, firsttemplateNamestatus);

			boolean isTemplateNamePresentCorpaccessibilitypublictoallusersstatusR1= fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firsttemplateNamestatus + "')]");
			if (isTemplateNamePresentCorpaccessibilitypublictoallusersstatusR1 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users template(Status Driven) created by Corporate user who has privilege to see private template by Regional user (R1)");
			}
			//Verify Public to all corporate users template(Status Driven) created by Corporate user who has privilege to see private template by Regional user (R1)
			fc.utobj().printTestStep("Verify Public to all corporate users template(Status Driven) created by Corporate user who has privilege to see private template by Regional user (R1)");
			campaignCenterPage.searchTemplateByFilter(driver, secondtemplateNamestatus);
			
			boolean isTemplateNamePresentCorpaccessibilitypublictoallcorporateusersstatusR1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondtemplateNamestatus + "')]");
			if (!isTemplateNamePresentCorpaccessibilitypublictoallcorporateusersstatusR1 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users template(Status Driven) created by Corporate user who has privilege to see private template by Regional user (R1)");
			}
			
			//Verify Private template(Status Driven) created by Corporate user who has privilege to see private template by Regional user (R1)
			fc.utobj().printTestStep("Verify Privare template(Status Driven) created by Corporate user who has privilege to see private template by Regional user (R1)");
			campaignCenterPage.searchTemplateByFilter(driver, thirdtemplateNamestatus);
			
			boolean isTemplateNamePresentCorpaccessibilityprivatestatusR1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + thirdtemplateNamestatus + "')]");
			if (!isTemplateNamePresentCorpaccessibilityprivatestatusR1 == false) {
				fc.utobj().throwsException("was not able to verify private template(Status Driven) created by Corporate user who has privilege to see private template by Regional user (R1)");
			}
		
			fc.utobj().printTestStep("Navigate to CRM>Campaign Center>campaign template");
			emailtemplate.clickoncampaigntemplate(driver);
			
			//Verify Public to all users Campaign created by Corporate user who has privilege to see private campaign by Regional user (R1)
			fc.utobj().printTestStep("Verifying Campaigns and templates of Regional user (R2) by Regional user (R1");
			//Verify Public to all users of my region Campaign created by Regional user who does not have privilege to see private campaign by Regional user (R1)
			fc.utobj().printTestStep("Verify Public to all users of my region Campaign created by Regional user who does not have privilege to see private campaign by Regional user (R1");
			campaignCenterPage.searchCampaignByFilter(driver, firstcampaignNamereg1);

			boolean isCampaignNamePresentRegpaccessibilitypublictoallusersofmyregion1R1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firstcampaignNamereg1 + "')]");
			if (isCampaignNamePresentRegpaccessibilitypublictoallusersofmyregion1R1 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my region Campaign created by Regional user who does not have privilege to see private campaign by Regional user (R1");
			}
			
			//Verify Private Campaign created by Regional user who does not have privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Private Campaign created by Regional user who does not have privilege to see private campaign by Regional user (R1");
			campaignCenterPage.searchCampaignByFilter(driver, secondcampaignNamereg1);
			
			boolean isCampaignNamePresentRegpaccessibilityprivate1R1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondcampaignNamereg1 + "')]");
			if (isCampaignNamePresentRegpaccessibilityprivate1R1 == false) {
				fc.utobj().throwsException("was not able to verify Private Campaign created by Regional user who does not have privilege to see private campaign by Regional user (R1");
			}
			
			//Verify Public to all users of my region (Status Driven)Campaign created by Regional user who does not have privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Public to all users of my region (Status Driven)Campaign created by Regional user who does not have privilege to see private campaign by Regional user (R1");
			campaignCenterPage.searchCampaignByFilter(driver, firstcampaignNamereg1status);

			boolean isCampaignNamePresentRegpaccessibilitypublictoallusersofmyregion1statusR1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firstcampaignNamereg1status + "')]");
			if (isCampaignNamePresentRegpaccessibilitypublictoallusersofmyregion1statusR1 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my region (Status Driven)Campaign created by Regional user who does not have privilege to see private campaign by Regional user (R1");
			}
			
			//Verify Private (Status Driven)Campaign created by Regional user who does not have privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Private (Status Driven)Campaign created by Regional user who does not have privilege to see private campaign by Regional user (R1");
			campaignCenterPage.searchCampaignByFilter(driver, secondcampaignNamereg1status);
			
			boolean isCampaignNamePresentRegpaccessibilityprivate1statusR1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondcampaignNamereg1status + "')]");
			if (isCampaignNamePresentRegpaccessibilityprivate1statusR1 == false) {
				fc.utobj().throwsException("was not able to verify Private (Status Driven)Campaign created by Regional user who does not have privilege to see private campaign by Regional user (R1");
			}
		
			fc.utobj().printTestStep("Navigate to CRM>Campaign Center>email template");
			emailtemplate.clickonemailtamplate(driver);
			
			//Verify Public to all users template created by Corporate user who does not have privilege to see private template by itself
			fc.utobj().printTestStep("Verify Public to all users of my region template created by Regional user who does not have privilege to see private template by Regional user (R1");
			campaignCenterPage.searchTemplateByFilter(driver, firsttemplateNamereg1);

			boolean isTemplateNamePresentRegaccessibilitypublictoallusersofmyregion1R1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firsttemplateNamereg1 + "')]");
			if (isTemplateNamePresentRegaccessibilitypublictoallusersofmyregion1R1 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my region template created by Regional user who does not have privilege to see private template by Regional user (R1");
			}
			
			//Verify Private template created by Regional user who does not have privilege to see private template by itself
			fc.utobj().printTestStep("Verify Private template created by Regional user does not have privilege to see private template by Regional user (R1");
			campaignCenterPage.searchTemplateByFilter(driver, secondtemplateNamereg1);
			
			boolean isTemplateNamePresentRegaccessibilityprivate1R1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondtemplateNamereg1 + "')]");
			if (isTemplateNamePresentRegaccessibilityprivate1R1 == false) {
				fc.utobj().throwsException("was not able to verify Private template created by Regional user does not have privilege to see private template by Regional user (R1");
			}
			
			//Verify Public to all users (Status Driven)template created by Regional user who does not have privilege to see private template by itself
			fc.utobj().printTestStep("Verify Public to all users of my region (Status Driven)template created by Regional user who does not have privilege to see private template by Regional user (R1");
			campaignCenterPage.searchTemplateByFilter(driver, firsttemplateNamereg1status);

			boolean isTemplateNamePresentRegaccessibilitypublictoallusersofmyregion1statusR1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firsttemplateNamereg1status + "')]");
			if (isTemplateNamePresentRegaccessibilitypublictoallusersofmyregion1statusR1 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my region (Status Driven)template created by Regional user who does not have privilege to see private template by Regional user (R1");
			}
			
			//Verify Private template created by Regional user who does not have privilege to see private template by itself
			fc.utobj().printTestStep("Verify Private (Status Driven)template created by Regional user does not have privilege to see private template by Regional user (R1");
			campaignCenterPage.searchTemplateByFilter(driver, secondtemplateNamereg1status);
			
			boolean isTemplateNamePresentRegaccessibilityprivate1statusR1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondtemplateNamereg1status + "')]");
			if (isTemplateNamePresentRegaccessibilityprivate1statusR1 == false) {
				fc.utobj().throwsException("was not able to verify Private (Status Driven)template created by Regional user does not have privilege to see private template by Regional user (R1");
			}
			
			fc.utobj().printTestStep("Navigate to CRM>Campaign Center>campaign template");
			emailtemplate.clickoncampaigntemplate(driver);
			
			fc.utobj().clickElement(driver, crmcenter.locationCampaign);
			fc.utobj().printTestStep("Verifying Campaigns and templates of Franchise owner user (F01) by Regional user (R1");
			fc.utobj().printTestStep("Verify Public to all users of my Franchise Campaign created by Franchise user who has privilege to see private campaign by Regional user (R1");
			campaignCenterPage.searchCampaignByFilter(driver, firstcampaignNamefran);

			boolean isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchiseR1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firstcampaignNamefran + "')]");
			if (isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchiseR1 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my Franchise Campaign created by Franchise user who has privilege to see private campaign by Regional user (R1");
			}
			
			//Verify Private Campaign created by Franchise user who has privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Private Campaign created by Franchise user who has privilege to see private campaign by Regional user (R1");
			campaignCenterPage.searchCampaignByFilter(driver, secondcampaignNamefran);
			
			boolean isCampaignNamePresentFranpaccessibilityprivateR1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondcampaignNamefran + "')]");
			if (isCampaignNamePresentFranpaccessibilityprivateR1 == false) {
				fc.utobj().throwsException("was not able to verify Private Campaign created by Franchise user who has privilege to see private campaign by Regional user (R1");
			}
			
			
			//Verify Public to all users of my Franchise (Status Driven)Campaign created by Franchise user who has privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Public to all users of my Franchise (Status Driven)Campaign created by Franchise user who has privilege to see private campaign by Regional user (R1");
			campaignCenterPage.searchCampaignByFilter(driver, firstcampaignNamefranstatus);

			boolean isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchisestatusR1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firstcampaignNamefranstatus + "')]");
			if (isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchisestatusR1 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my Franchise (Status Driven)Campaign created by Franchise user who has privilege to see private campaign by Regional user (R1");
			}
			
			//Verify Private Campaign created by Franchise user who has privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Private (Status Driven)Campaign created by Franchise user who has privilege to see private campaign by Regional user (R1");
			campaignCenterPage.searchCampaignByFilter(driver, secondcampaignNamefranstatus);
			
			boolean isCampaignNamePresentFranpaccessibilityprivatestatusR1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondcampaignNamefranstatus + "')]");
			if (isCampaignNamePresentFranpaccessibilityprivatestatusR1 == false) {
				fc.utobj().throwsException("was not able to verify Private (Status Driven)Campaign created by Franchise user who has privilege to see private campaign by Regional user (R1");
			}
			
			fc.utobj().printTestStep("Navigate to CRM>Campaign Center>email template");
			emailtemplate.clickonemailtamplate(driver);
			fc.utobj().clickElement(driver, crmcenter.locationtemplate);
			//Verify Public to all users template created by Corporate user who has privilege to see private template by itself
			fc.utobj().printTestStep("Verify Public to all users of my franchise template created by franchise user who has privilege to see private template by Regional user (R1");
			campaignCenterPage.searchTemplateByFilter(driver, firsttemplateNamefran);

			boolean isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchiseR1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firsttemplateNamefran + "')]");
			if (isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchiseR1 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my franchise template created by franchise user who has privilege to see private template by Regional user (R1");
			}
			
			//Verify Private template created by franchise user who has privilege to see private template by itself
			fc.utobj().printTestStep("Verify Private template created by franchise user who has privilege to see private template by Regional user (R1");
			campaignCenterPage.searchTemplateByFilter(driver, secondtemplateNamefran);
			
			boolean isTemplateNamePresentfranaccessibilityprivateR1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondtemplateNamefran + "')]");
			if (isTemplateNamePresentfranaccessibilityprivateR1 == false) {
				fc.utobj().throwsException("was not able to verify private template created by franchise user who has privilege to see private template by corporate user(R1)");
			}
			
			//Verify Public to all users (Status Driven)template created by Corporate user who has privilege to see private template by itself
			fc.utobj().printTestStep("Verify Public to all users of my franchise (Status Driven)template created by franchise user who has privilege to see private template by Regional user (R1");
			campaignCenterPage.searchTemplateByFilter(driver, firsttemplateNamefranstatus);

			boolean isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchisestatusR1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firsttemplateNamefranstatus + "')]");
			if (isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchisestatusR1 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my franchise (Status Driven)template created by franchise user who has privilege to see private template by Regional user (R1");
			}
			
			//Verify Private template created by franchise user who has privilege to see private template by itself
			fc.utobj().printTestStep("Verify Private (Status Driven)template created by franchise user who has privilege to see private template by Regional user (R1");
			campaignCenterPage.searchTemplateByFilter(driver, secondtemplateNamefranstatus);
			
			boolean isTemplateNamePresentfranaccessibilityprivatestatusR1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondtemplateNamefranstatus + "')]");
			if (isTemplateNamePresentfranaccessibilityprivatestatusR1 == false) {
				fc.utobj().throwsException("was not able to verify private (Status Driven)template created by franchise user who has privilege to see private template by Regional user (R1");
			}
			fc.utobj().printTestStep("Navigate to CRM>Campaign Center>campaign template");
			emailtemplate.clickoncampaigntemplate(driver);
			fc.utobj().clickElement(driver, crmcenter.locationCampaign);
			fc.utobj().printTestStep("Verifying Campaigns and templates of Franchise Employee user (FE1) by Regional user (R1");
			
			fc.utobj().printTestStep("Verify Public to all users of my Franchise Campaign created by Franchise Employee user(FE1) who has privilege to see private campaign by Regional user (R1");
			campaignCenterPage.searchCampaignByFilter(driver, firstcampaignNamefrane);

			boolean isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchiseER1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firstcampaignNamefrane + "')]");
			if (isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchiseER1 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my Franchise Campaign created by Franchise Employee user(FE1) who has privilege to see private campaign by Regional user (R1");
			}
			
			//Verify Private Campaign created by Franchise user who has privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Private Campaign created by Franchise Employee user(FE1) who has privilege to see private campaign by Regional user (R1");
			campaignCenterPage.searchCampaignByFilter(driver, secondcampaignNamefrane);
			
			boolean isCampaignNamePresentFranpaccessibilityprivateER1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondcampaignNamefrane + "')]");
			if (isCampaignNamePresentFranpaccessibilityprivateER1 == false) {
				fc.utobj().throwsException("was not able to verify Private Campaign created by Franchise Employee user(FE1) who has privilege to see private campaign by Regional user (R1");
			}
			
			
			//Verify Public to all users of my Franchise (Status Driven)Campaign created by Franchise user who has privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Public to all users of my Franchise (Status Driven)Campaign created by Franchise Employee user(FE1) who has privilege to see private campaign by Regional user (R1)");
			campaignCenterPage.searchCampaignByFilter(driver, firstcampaignNamefranestatus);

			boolean isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchiseEstatusR1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firstcampaignNamefranestatus + "')]");
			if (isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchiseEstatusR1 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my Franchise (Status Driven)Campaign created by Franchise Employee user(FE1) who has privilege to see private campaign by Regional user (R1)");
			}
			
			//Verify Private Campaign created by Franchise user who has privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Private (Status Driven)Campaign created by Franchise Employee user(FE1) who has privilege to see private campaign by Regional user (R1)");
			campaignCenterPage.searchCampaignByFilter(driver, secondcampaignNamefranestatus);
			
			boolean isCampaignNamePresentFranpaccessibilityprivateEstatusR1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondcampaignNamefranestatus + "')]");
			if (isCampaignNamePresentFranpaccessibilityprivateEstatusR1 == false) {
				fc.utobj().throwsException("was not able to verify Private (Status Driven)Campaign created by Franchise Employee user(FE1) who has privilege to see private campaign by Regional user (R1)");
			}
		
			fc.utobj().printTestStep("Navigate to CRM>Campaign Center>email template");
			emailtemplate.clickonemailtamplate(driver);
			fc.utobj().clickElement(driver, crmcenter.locationtemplate);
			//Verify Public to all users template created by Franchise Employee user user who has privilege to see private template by itself
			fc.utobj().printTestStep("Verify Public to all users of my franchise template created by franchise Employee user(FE1) who has privilege to see private template by Regional user (R1)");
			campaignCenterPage.searchTemplateByFilter(driver, firsttemplateNamefrane);

			boolean isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchiseER1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firsttemplateNamefrane + "')]");
			if (isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchiseER1 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my franchise template created by franchise Employee user(FE1) who has privilege to see private template by Regional user (R1)");
			}
			
			//Verify Private template created by franchise Employee user who has privilege to see private template by itself
			fc.utobj().printTestStep("Verify Private template created by franchise Employee user(FE1) who has privilege to see private template by Regional user (R1)");
			campaignCenterPage.searchTemplateByFilter(driver, secondtemplateNamefrane);
			
			boolean isTemplateNamePresentfranaccessibilityprivateER1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondtemplateNamefrane + "')]");
			if (isTemplateNamePresentfranaccessibilityprivateER1 == false) {
				fc.utobj().throwsException("was not able to verify private template created by franchise Employee user(FE1) who has privilege to see private template by Regional user (R1)");
			}
			
			
			//Verify Public to all users (Status Driven)template created by Franchise Employee user user who has privilege to see private template by itself
			fc.utobj().printTestStep("Verify Public to all users of my franchise (Status Driven)template created by franchise Employee user(FE1) who has privilege to see private template by Regional user (R1)");
			campaignCenterPage.searchTemplateByFilter(driver, firsttemplateNamefranestatus);

			boolean isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchiseEstatusR1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firsttemplateNamefranestatus + "')]");
			if (isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchiseEstatusR1 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my franchise (Status Driven)template created by franchise Employee user(FE1) who has privilege to see private template by Regional user (R1)");
			}
			
			//Verify Private template created by franchise Employee user who has privilege to see private template by itself
			fc.utobj().printTestStep("Verify Private (Status Driven)template created by franchise Employee user(FE1) who has privilege to see private template by Regional user (R1)");
			campaignCenterPage.searchTemplateByFilter(driver, secondtemplateNamefranestatus);
			
			boolean isTemplateNamePresentfranaccessibilityprivateEstatusR1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondtemplateNamefranestatus + "')]");
			if (isTemplateNamePresentfranaccessibilityprivateEstatusR1 == false) {
				fc.utobj().throwsException("was not able to verify private (Status Driven)template created by franchise Employee user(FE1) who has privilege to see private template by Regional user (R1)");
			}
			
			
			fc.utobj().printTestStep("Navigate to CRM>Campaign Center>campaign template");
			emailtemplate.clickoncampaigntemplate(driver);
			fc.utobj().clickElement(driver, crmcenter.locationCampaign);
			fc.utobj().printTestStep("Verifying Campaigns and templates of Franchise Employee user (F02) by Regional user (R1)");
			
			
			//Verify Public to all users of my Franchise Campaign created by Franchise user who does not have privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Public to all users of my Franchise Campaign created by Franchise user(FO2) who does not have privilege to see private campaign by Regional user (R1)");
			campaignCenterPage.searchCampaignByFilter(driver, firstcampaignNamefranO2);

			boolean isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchiseO2R1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firstcampaignNamefranO2 + "')]");
			if (isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchiseO2R1 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my Franchise Campaign created by Franchise user(FO2) who does not have privilege to see private campaign by Regional user (R1)");
			}
			
			//Verify Private Campaign created by Franchise user who has privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Private Campaign created by Franchise user(FO2) who does not have privilege to see private campaign by Regional user (R1)");
			campaignCenterPage.searchCampaignByFilter(driver, secondcampaignNamefranO2);
			
			boolean isCampaignNamePresentFranpaccessibilityprivateO2R1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondcampaignNamefranO2 + "')]");
			if (isCampaignNamePresentFranpaccessibilityprivateO2R1 == false) {
				fc.utobj().throwsException("was not able to verify Private Campaign created by Franchise user(FO2) who does not have privilege to see private campaign by Regional user (R1)");
			}
			
			//Verify Public to all users of my Franchise (Status Driven)Campaign created by Franchise user who does not have privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Public to all users of my Franchise (Status Driven)Campaign created by Franchise user(FO2) who does not have privilege to see private campaign by Regional user (R1)");
			campaignCenterPage.searchCampaignByFilter(driver, firstcampaignNamefranO2status);

			boolean isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchiseO2statusR1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firstcampaignNamefranO2status + "')]");
			if (isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchiseO2statusR1 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my (Status Driven)Franchise Campaign created by Franchise user(FO2) who does not have privilege to see private campaign by Regional user (R1)");
			}
			
			//Verify Private Campaign created by Franchise user who has privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Private (Status Driven)Campaign created by Franchise user(FO2) who does not have privilege to see private campaign by Regional user (R1)");
			campaignCenterPage.searchCampaignByFilter(driver, secondcampaignNamefranO2status);
			
			boolean isCampaignNamePresentFranpaccessibilityprivateO2statusR1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondcampaignNamefranO2status + "')]");
			if (isCampaignNamePresentFranpaccessibilityprivateO2statusR1 == false) {
				fc.utobj().throwsException("was not able to verify Private (Status Driven)Campaign created by Franchise user(FO2) who does not have privilege to see private campaign by Regional user (R1)");
			}
		
			fc.utobj().printTestStep("Navigate to CRM>Campaign Center>email template");
			emailtemplate.clickonemailtamplate(driver);
			fc.utobj().clickElement(driver, crmcenter.locationtemplate);
			//Verify Public to all users template created by Corporate user who has privilege to see private template by itself
			fc.utobj().printTestStep("Verify Public to all users of my franchise template created by franchise user(FO2) who does not have privilege to see private template by Regional user (R1)");
			campaignCenterPage.searchTemplateByFilter(driver, firsttemplateNamefranO2);

			boolean isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchiseO2R1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firsttemplateNamefranO2 + "')]");
			if (isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchiseO2R1 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my franchise template created by franchise user(FO2) who does not have privilege to see private template by Regional user (R1)");
			}
			
			//Verify Private template created by franchise user who has privilege to see private template by itself
			fc.utobj().printTestStep("Verify Private template created by franchise user(FO2) who does not have privilege to see private template by Regional user (R1)");
			campaignCenterPage.searchTemplateByFilter(driver, secondtemplateNamefranO2);
			
			boolean isTemplateNamePresentfranaccessibilityprivateO2R1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondtemplateNamefranO2 + "')]");
			if (isTemplateNamePresentfranaccessibilityprivateO2R1 == false) {
				fc.utobj().throwsException("was not able to verify private template created by franchise user(FO2) who does not have privilege to see private template by Regional user (R1)");
			}
			
			//Verify Public to all users template created by Corporate user who has privilege to see private template by itself
			fc.utobj().printTestStep("Verify Public to all users of my franchise (Status Driven)template created by franchise user(FO2) who does not have privilege to see private template by Regional user (R1)");
			campaignCenterPage.searchTemplateByFilter(driver, firsttemplateNamefranO2status);

			boolean isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchiseO2statusR1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firsttemplateNamefranO2status + "')]");
			if (isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchiseO2statusR1 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my franchise (Status Driven)template created by franchise user(FO2) who does not have privilege to see private template by Regional user (R1)");
			}
			
			//Verify Private template created by franchise user who has privilege to see private template by itself
			fc.utobj().printTestStep("Verify Private (Status Driven)template created by franchise user(FO2) who does not have privilege to see private template by Regional user (R1)");
			campaignCenterPage.searchTemplateByFilter(driver, secondtemplateNamefranO2status);
			
			boolean isTemplateNamePresentfranaccessibilityprivateO2statusR1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondtemplateNamefranO2status + "')]");
			if (isTemplateNamePresentfranaccessibilityprivateO2statusR1 == false) {
				fc.utobj().throwsException("was not able to verify private (Status Driven)template created by franchise user(FO2) who does not have privilege to see private template by Regional user (R1)");
			}
			
			fc.utobj().printTestStep("Navigate to CRM>Campaign Center>campaign template");
			emailtemplate.clickoncampaigntemplate(driver);
			fc.utobj().clickElement(driver, crmcenter.locationCampaign);
			fc.utobj().printTestStep("Verifying Campaigns and templates of Franchise Employee user (FE2) by Regional user (R1)");
			
			//Verify Public to all users of my Franchise Campaign created by Franchise user who has privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Public to all users of my Franchise Campaign created by Franchise Employee user(FE2) who does not have privilege to see private campaign by Regional user (R1)");
			campaignCenterPage.searchCampaignByFilter(driver, firstcampaignNamefrane2);

			boolean isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchiseE2R1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firstcampaignNamefrane2 + "')]");
			if (isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchiseE2R1 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my Franchise Campaign created by Franchise Employee user(FE2) who does not have privilege to see private campaign by Regional user (R1)");
			}
			
			//Verify Private Campaign created by Franchise user who has privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Private Campaign created by Franchise Employee user(FE2) who does not have privilege to see private campaign by Regional user (R1)");
			campaignCenterPage.searchCampaignByFilter(driver, secondcampaignNamefrane2);
			
			boolean isCampaignNamePresentFranpaccessibilityprivateE2R1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondcampaignNamefrane2 + "')]");
			if (isCampaignNamePresentFranpaccessibilityprivateE2R1 == false) {
				fc.utobj().throwsException("was not able to verify Private Campaign created by Franchise Employee user(FE2) who does not have privilege to see private campaign by Regional user (R1)");
			}
			
			
			//Verify Public to all users of my Franchise Campaign created by Franchise user who has privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Public to all users of my Franchise (Status Driven)Campaign created by Franchise Employee user(FE2) who does not have privilege to see private campaign by Regional user (R1)");
			campaignCenterPage.searchCampaignByFilter(driver, firstcampaignNamefrane2status);

			boolean isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchiseE2statusR1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firstcampaignNamefrane2status + "')]");
			if (isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchiseE2statusR1 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my Franchise (Status Driven)Campaign created by Franchise Employee user(FE2) who does not have privilege to see private campaign by Regional user (R1)");
			}
			
			//Verify Private Campaign created by Franchise user who has privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Private (Status Driven)Campaign created by Franchise Employee user(FE2) who does not have privilege to see private campaign by Regional user (R1)");
			campaignCenterPage.searchCampaignByFilter(driver, secondcampaignNamefrane2status);
			
			boolean isCampaignNamePresentFranpaccessibilityprivateE2statusR1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondcampaignNamefrane2status + "')]");
			if (isCampaignNamePresentFranpaccessibilityprivateE2statusR1 == false) {
				fc.utobj().throwsException("was not able to verify Private (Status Driven)Campaign created by Franchise Employee user(FE2) who does not have privilege to see private campaign by Regional user (R1)");
			}
			
		
			fc.utobj().printTestStep("Navigate to CRM>Campaign Center>email template");
			emailtemplate.clickonemailtamplate(driver);
			fc.utobj().clickElement(driver, crmcenter.locationtemplate);
			//Verify Public to all users template created by Franchise Employee user user who has privilege to see private template by itself
			fc.utobj().printTestStep("Verify Public to all users of my franchise template created by franchise Employee user(FE2) who does not have privilege to see private template by Regional user (R1)");
			campaignCenterPage.searchTemplateByFilter(driver, firsttemplateNamefrane2);

			boolean isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchiseE2R1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firsttemplateNamefrane2 + "')]");
			if (isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchiseE2R1 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my franchise template created by franchise Employee user(FE2) who does not have privilege to see private template by Regional user (R1)");
			}
			
			//Verify Private template created by franchise Employee user who has privilege to see private template by itself
			fc.utobj().printTestStep("Verify Private template created by franchise Employee user(FE2) who does not have privilege to see private template by Regional user (R1)");
			campaignCenterPage.searchTemplateByFilter(driver, secondtemplateNamefrane2);
			
			boolean isTemplateNamePresentfranaccessibilityprivateE2R1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondtemplateNamefrane2 + "')]");
			if (isTemplateNamePresentfranaccessibilityprivateE2R1 == false) {
				fc.utobj().throwsException("was not able to verify private template created by franchise Employee user(FE2) who does not have privilege to see private template by Regional user (R1)");
			}
			
			//Verify Public to all users template created by Franchise Employee user user who has privilege to see private template by itself
			fc.utobj().printTestStep("Verify Public to all users of my franchise (Status Driven)template created by franchise Employee user(FE2) who does not have privilege to see private template by Regional user (R1)");
			campaignCenterPage.searchTemplateByFilter(driver, firsttemplateNamefrane2status);

			boolean isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchiseE2statusR1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firsttemplateNamefrane2status + "')]");
			if (isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchiseE2statusR1 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my franchise(Status Driven) template created by franchise Employee user(FE2) who does not have privilege to see private template by Regional user (R1)");
			}
			
			//Verify Private template created by franchise Employee user who has privilege to see private template by itself
			fc.utobj().printTestStep("Verify Private (Status Driven)template created by franchise Employee user(FE2) who does not have privilege to see private template by Regional user (R1)");
			campaignCenterPage.searchTemplateByFilter(driver, secondtemplateNamefrane2status);
			
			boolean isTemplateNamePresentfranaccessibilityprivateE2statusR1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondtemplateNamefrane2status + "')]");
			if (isTemplateNamePresentfranaccessibilityprivateE2statusR1 == false) {
				fc.utobj().throwsException("was not able to verify private (Status Driven)template created by franchise Employee user(FE2) who does not have privilege to see private template by Regional user (R1)");
			}
		
			
			fc.utobj().printTestStep("Navigate to CRM>Campaign Center>campaign template");
			emailtemplate.clickoncampaigntemplate(driver);
			fc.utobj().clickElement(driver, crmcenter.locationCampaign);
			fc.utobj().printTestStep("Verifying Campaigns and templates of Franchise Mu user (V1) by Regional user (R1)");
			
			fc.utobj().printTestStep("Verify Public to all users of my Franchise Campaign created by Franchise MU user(V1) who has privilege to see private campaign by Regional user (R1)");
			campaignCenterPage.searchCampaignByFilter(driver, firstcampaignNamefranmu);

			boolean isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchisemuR1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firstcampaignNamefranmu + "')]");
			if (isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchisemuR1 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my Franchise Campaign created by Franchise MU user(V1) who has privilege to see private campaign by Regional user (R1)");
			}
			
			//Verify Private Campaign created by Franchise user who has privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Private Campaign created by Franchise MU user(V1) who has privilege to see private campaign by Regional user (R1)");
			campaignCenterPage.searchCampaignByFilter(driver, secondcampaignNamefranmu);
			
			boolean isCampaignNamePresentFranpaccessibilityprivatemuR1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondcampaignNamefranmu + "')]");
			if (isCampaignNamePresentFranpaccessibilityprivatemuR1 == false) {
				fc.utobj().throwsException("was not able to verify Private Campaign created by Franchise MU user(V1) who has privilege to see private campaign by Regional user (R1)");
			}
			
			
			//Verify Public to all users of my Franchise (Status Driven)Campaign created by Franchise user who has privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Public to all users of my Franchise (Status Driven)Campaign created by Franchise MU user(V1) who has privilege to see private campaign by Regional user (R1)");
			campaignCenterPage.searchCampaignByFilter(driver, firstcampaignNamefranmustatus);

			boolean isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchisemustatusR1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firstcampaignNamefranmustatus + "')]");
			if (isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchisemustatusR1 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my Franchise (Status Driven)Campaign created by Franchise MU user(V1) who has privilege to see private campaign by corporate user(R1)");
			}
			
			//Verify Private Campaign created by Franchise user who has privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Private (Status Driven)Campaign created by Franchise MU user(V1) who has privilege to see private campaign by Regional user (R1)");
			campaignCenterPage.searchCampaignByFilter(driver, secondcampaignNamefranmustatus);
			
			boolean isCampaignNamePresentFranpaccessibilityprivatemustatusR1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondcampaignNamefranmustatus + "')]");
			if (isCampaignNamePresentFranpaccessibilityprivatemustatusR1 == false) {
				fc.utobj().throwsException("was not able to verify Private (Status Driven)Campaign created by Franchise MU user(V1) who has privilege to see private campaign by Regional user (R1)");
			}
			
			fc.utobj().printTestStep("Navigate to CRM>Campaign Center>email template");
			emailtemplate.clickonemailtamplate(driver);
			fc.utobj().clickElement(driver, crmcenter.locationtemplate);
			//Verify Public to all users template created by Corporate user who has privilege to see private template by itself
			fc.utobj().printTestStep("Verify Public to all users of my franchise template created by franchise MU user(V1) who has privilege to see private template by Regional user (R1)");
			campaignCenterPage.searchTemplateByFilter(driver, firsttemplateNamefranmu);

			boolean isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchisemuR1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firsttemplateNamefranmu + "')]");
			if (isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchisemuR1 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my franchise template created by franchise MU user(V1) who has privilege to see private template by Regional user (R1)");
			}
			
			//Verify Private template created by franchise user who has privilege to see private template by itself
			fc.utobj().printTestStep("Verify Private template created by franchise MU user(V1) who has privilege to see private template by Regional user (R1)");
			campaignCenterPage.searchTemplateByFilter(driver, secondtemplateNamefranmu);
			
			boolean isTemplateNamePresentfranaccessibilityprivatemuR1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondtemplateNamefranmu + "')]");
			if (isTemplateNamePresentfranaccessibilityprivatemuR1 == false) {
				fc.utobj().throwsException("was not able to verify private template created by franchise MU user(V1) who has privilege to see private template by Regional user (R1)");
			}
			
			//Verify Public to all users template created by Corporate user who has privilege to see private template by itself
			fc.utobj().printTestStep("Verify Public to all users of my franchise (Status Driven)template created by franchise MU user(V1) who has privilege to see private template by Regional user (R1)");
			campaignCenterPage.searchTemplateByFilter(driver, firsttemplateNamefranmustatus);

			boolean isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchisemustatusR1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firsttemplateNamefranmustatus + "')]");
			if (isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchisemustatusR1 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my franchise (Status Driven)template created by franchise MU user(V1) who has privilege to see private template by Regional user (R1)");
			}
			
			//Verify Private template created by franchise user who has privilege to see private template by itself
			fc.utobj().printTestStep("Verify Private (Status Driven)template created by franchise MU user(V1) who has privilege to see private template by Regional user (R1)");
			campaignCenterPage.searchTemplateByFilter(driver, secondtemplateNamefranmustatus);
			
			boolean isTemplateNamePresentfranaccessibilityprivatemustatusR1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondtemplateNamefranmustatus + "')]");
			if (isTemplateNamePresentfranaccessibilityprivatemustatusR1 == false) {
				fc.utobj().throwsException("was not able to verify private (Status Driven)template created by franchise MU user(V1) who has privilege to see private template by Regional user (R1)");
			}
			
			
			
			
			fc.utobj().printTestStep("Navigate to CRM>Campaign Center>campaign template");
			emailtemplate.clickoncampaigntemplate(driver);
			fc.utobj().clickElement(driver, crmcenter.locationCampaign);
			fc.utobj().printTestStep("Verifying Campaigns and templates of Franchise Employee user (V2) by Regional user (R1)");
			fc.utobj().printTestStep("Verify Public to all users of my Franchise Campaign created by Franchise Employee user(V2) who does not have privilege to see private campaign by Regional user (R1)");
			campaignCenterPage.searchCampaignByFilter(driver, firstcampaignNamefranmue2);

			boolean isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchisemue2R1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firstcampaignNamefranmue2 + "')]");
			if (isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchisemue2R1 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my Franchise Campaign created by Franchise Employee user(V2) who does not have privilege to see private campaign by Regional user (R1)");
			}
			
			//Verify Private Campaign created by Franchise user who has privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Private Campaign created by Franchise Employee user(V2) who does not have privilege to see private campaign by Regional user (R1)");
			campaignCenterPage.searchCampaignByFilter(driver, secondcampaignNamefranmue2);
			
			boolean isCampaignNamePresentFranpaccessibilityprivatemue2R1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondcampaignNamefranmue2 + "')]");
			if (isCampaignNamePresentFranpaccessibilityprivatemue2R1 == false) {
				fc.utobj().throwsException("was not able to verify Private Campaign created by Franchise Employee user(V2) who does not have privilege to see private campaign by Regional user (R1)");
			}
			
			
			//Verify Public to all users of my Franchise Campaign created by Franchise Employee user(V2) who has privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Public to all users of my Franchise (Status Driven)Campaign created by Franchise Employee user(V2) who does not have privilege to see private campaign by Regional user (R1)");
			campaignCenterPage.searchCampaignByFilter(driver, firstcampaignNamefranmue2status);

			boolean isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchisemue2statusR1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firstcampaignNamefranmue2status + "')]");
			if (isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchisemue2statusR1 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my Franchise (Status Driven)Campaign created by Franchise Employee user(V2) who does not have privilege to see private campaign by Regional user (R1)");
			}
			
			//Verify Private Campaign created by Franchise user who has privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Private (Status Driven)Campaign created by Franchise Employee user(V2) who does not have privilege to see private campaign by Regional user (R1)");
			campaignCenterPage.searchCampaignByFilter(driver, secondcampaignNamefranmue2status);
			
			boolean isCampaignNamePresentFranpaccessibilityprivatemue2statusR1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondcampaignNamefranmue2status + "')]");
			if (isCampaignNamePresentFranpaccessibilityprivatemue2statusR1 == false) {
				fc.utobj().throwsException("was not able to verify Private (Status Driven)Campaign created by Franchise Employee user(V2) who does not have privilege to see private campaign by Regional user (R1)");
			}
			
			fc.utobj().printTestStep("Navigate to CRM>Campaign Center>email template");
			emailtemplate.clickonemailtamplate(driver);
			fc.utobj().clickElement(driver, crmcenter.locationtemplate);
			//Verify Public to all users template created by Franchise user who has privilege to see private template by itself
			fc.utobj().printTestStep("Verify Public to all users of my franchise template created by franchise Employee user(V2) who does not have privilege to see private template by Regional user (R1)");
			campaignCenterPage.searchTemplateByFilter(driver, firsttemplateNamefranmue2);

			boolean isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchisemue2R1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firsttemplateNamefranmue2 + "')]");
			if (isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchisemue2R1 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my franchise template created by franchise Employee user(V2) who has privilege to see private template by Regional user (R1)");
			}
			
			//Verify Private template created by franchise user who has privilege to see private template by itself
			fc.utobj().printTestStep("Verify Private template created by franchise Employee user(V2) who has privilege to see private template by Regional user (R1)");
			campaignCenterPage.searchTemplateByFilter(driver, secondtemplateNamefranmue2);
			
			boolean isTemplateNamePresentfranaccessibilityprivatemue2R1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondtemplateNamefranmue2 + "')]");
			if (isTemplateNamePresentfranaccessibilityprivatemue2R1 == false) {
				fc.utobj().throwsException("was not able to verify private template created by franchise Employee user(V2) who has privilege to see private template by Regional user (R1)");
			}
			
			//Verify Public to all users template created by Franchise user who has privilege to see private template by itself
			fc.utobj().printTestStep("Verify Public to all users of my franchise (Status Driven)template created by franchise Employee user(V2) who does not have privilege to see private template by Regional user (R1)");
			campaignCenterPage.searchTemplateByFilter(driver, firsttemplateNamefranmue2status);

			boolean isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchisemue2statusR1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firsttemplateNamefranmue2status + "')]");
			if (isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchisemue2statusR1 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my franchise (Status Driven)template created by franchise Employee user(V2) who has privilege to see private template by Regional user (R1)");
			}
			
			//Verify Private template created by franchise user who has privilege to see private template by itself
			fc.utobj().printTestStep("Verify Private (Status Driven)template created by franchise Employee user(V2) who has privilege to see private template by Regional user (R1)");
			campaignCenterPage.searchTemplateByFilter(driver, secondtemplateNamefranmue2status);
			
			boolean isTemplateNamePresentfranaccessibilityprivatemue2statusR1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondtemplateNamefranmue2status + "')]");
			if (isTemplateNamePresentfranaccessibilityprivatemue2statusR1 == false) {
				fc.utobj().throwsException("was not able to verify private (Status Driven)template created by franchise Employee user(V2) who has privilege to see private template by Regional user (R1)");
			}
			
			if(isCampaignNamePresentCorpaccessibilitypublictoalluserscorpR1 && !isCampaignNamePresentCorpaccessibilitypublictoallcorporateuserscorpR1 && !isCampaignNamePresentCorpaccessibilityprivatecorpR1 
					&& isCampaignNamePresentCorpaccessibilitypublictoalluserscorpstatusR1 && !isCampaignNamePresentCorpaccessibilitypublictoallcorporateuserscorpstatusR1 && !isCampaignNamePresentCorpaccessibilityprivatecorpstatusR1 
					&& isTemplateNamePresentCorpaccessibilitypublictoalluserscorpR1 && !isTemplateNamePresentCorpaccessibilitypublictoallcorporateuserscorpR1 && !isTemplateNamePresentCorpaccessibilityprivatecorpR1 
					&& isTemplateNamePresentCorpaccessibilitypublictoalluserscorpstatusR1 && !isTemplateNamePresentCorpaccessibilitypublictoallcorporateuserscorpstatusR1 && !isTemplateNamePresentCorpaccessibilityprivatecorpstatusR1 
					&& isCampaignNamePresentRegpaccessibilitypublictoallusersofmyregionR1 && isCampaignNamePresentRegpaccessibilityprivateR1 && isCampaignNamePresentRegpaccessibilitypublictoallusersofmyregionstatusR1 
					&& isCampaignNamePresentRegpaccessibilityprivatestatusR1 && isTemplateNamePresentRegaccessibilitypublictoallusersofmyregionR1 && isTemplateNamePresentRegaccessibilityprivateR1 
					&& isTemplateNamePresentRegaccessibilitypublictoallusersofmyregionstatusR1 && isTemplateNamePresentRegaccessibilityprivatestatusR1 && isCampaignNamePresentCorpaccessibilitypublictoallusersR1 
					&& !isCampaignNamePresentCorpaccessibilitypublictoallcorporateusersR1 && !isCampaignNamePresentCorpaccessibilityprivateR1 && isCampaignNamePresentCorpaccessibilitypublictoallusersstatusR1 
					&& !isCampaignNamePresentCorpaccessibilitypublictoallcorporateusersstatusR1 && !isCampaignNamePresentCorpaccessibilityprivatestatusR1 && isTemplateNamePresentCorpaccessibilitypublictoallusersR1 
					&& !isTemplateNamePresentCorpaccessibilitypublictoallcorporateusersR1 && !isTemplateNamePresentCorpaccessibilityprivateR1 && isTemplateNamePresentCorpaccessibilitypublictoallusersstatusR1 
					&& !isTemplateNamePresentCorpaccessibilitypublictoallcorporateusersstatusR1 && !isTemplateNamePresentCorpaccessibilityprivatestatusR1 && isCampaignNamePresentRegpaccessibilitypublictoallusersofmyregion1R1
					&& isCampaignNamePresentRegpaccessibilityprivate1R1 && isCampaignNamePresentRegpaccessibilitypublictoallusersofmyregion1statusR1 && isCampaignNamePresentRegpaccessibilityprivate1statusR1
					&& isTemplateNamePresentRegaccessibilitypublictoallusersofmyregion1R1 && isTemplateNamePresentRegaccessibilityprivate1R1 && isTemplateNamePresentRegaccessibilitypublictoallusersofmyregion1statusR1 
					&& isTemplateNamePresentRegaccessibilityprivate1statusR1 && isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchiseR1 && isCampaignNamePresentFranpaccessibilityprivateR1 
					&& isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchisestatusR1 && isCampaignNamePresentFranpaccessibilityprivatestatusR1 && isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchiseR1 
					&& isTemplateNamePresentfranaccessibilityprivateR1 && isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchisestatusR1 && isTemplateNamePresentfranaccessibilityprivatestatusR1 
					&& isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchiseER1 && isCampaignNamePresentFranpaccessibilityprivateER1 && isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchiseEstatusR1 
					&& isCampaignNamePresentFranpaccessibilityprivateEstatusR1 && isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchiseER1 && isTemplateNamePresentfranaccessibilityprivateER1 
					&& isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchiseEstatusR1 && isTemplateNamePresentfranaccessibilityprivateEstatusR1 && isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchiseO2R1 
					&& isCampaignNamePresentFranpaccessibilityprivateO2R1 && isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchiseO2statusR1 && isCampaignNamePresentFranpaccessibilityprivateO2statusR1 
					&& isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchiseO2R1 && isTemplateNamePresentfranaccessibilityprivateO2R1 && isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchiseO2statusR1 
					&& isTemplateNamePresentfranaccessibilityprivateO2statusR1 && isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchiseE2R1 && isCampaignNamePresentFranpaccessibilityprivateE2R1 
					&& isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchiseE2statusR1 && isCampaignNamePresentFranpaccessibilityprivateE2statusR1 && isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchiseE2R1
					&& isTemplateNamePresentfranaccessibilityprivateE2R1 && isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchiseE2statusR1 && isTemplateNamePresentfranaccessibilityprivateE2statusR1 
					&& isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchisemuR1 && isCampaignNamePresentFranpaccessibilityprivatemuR1 && isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchisemustatusR1 
					&& isCampaignNamePresentFranpaccessibilityprivatemustatusR1 && isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchisemuR1 && isTemplateNamePresentfranaccessibilityprivatemuR1 
					&& isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchisemustatusR1 && isTemplateNamePresentfranaccessibilityprivatemustatusR1 && isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchisemue2R1 
					&& isCampaignNamePresentFranpaccessibilityprivatemue2R1 && isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchisemue2statusR1 && isCampaignNamePresentFranpaccessibilityprivatemue2statusR1 
					&& isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchisemue2R1 && isTemplateNamePresentfranaccessibilityprivatemue2R1 && isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchisemue2statusR1
					&& isTemplateNamePresentfranaccessibilityprivatemue2statusR1) {
				fc.utobj().printTestStep("able to verify all types of campaigns and templates created by corporate/rgional/franchise users when loging with Regional user (R1) who has priviledge to see private campaign");
			}
			else {
				fc.utobj().throwsException("was not able to verify all types of campaigns and templates created by corporate/rgional/franchise users when loging with Regional user (R1) who has priviledge to see private campaign");
			}
		
			
			
			
			
			
			
			
			
			
			//Login with Regional user(R2) who does not have role to see private campaign
			fc.utobj().printTestStep("Login with Regional user(R2) who does not have role to see private campaign");
			fc.loginpage().loginWithParameter(driver, Reguname1, Regpassword1);
			fc.crm().crm_common().openCRMCampaignCenterPage(fcHomePageTest, driver);
			fc.utobj().printTestStep("Click on the menu option and select campaign");
			/*CRMCampaignCenterPage pobj = new CRMCampaignCenterPage(driver);*/
			fc.utobj().clickElement(driver, pobj.menuOptions);
			fc.utobj().clickElement(driver, pobj.campaignLinks);
			
			//Verifying Campaigns and templates of corporate user (C2) after logging with Regional user (R2)
			fc.utobj().printTestStep("Verifying Campaigns and templates of corporate user (C2) by Regional user(R2)");
			fc.utobj().printTestStep("Verify Public to all users Campaign created by Corporate user(C2) who does not have privilege to see private campaign by Regional user(R2)");
			campaignCenterPage.searchCampaignByFilter(driver, firstcampaignNamecorp);

			boolean isCampaignNamePresentCorpaccessibilitypublictoalluserscorpR2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firstcampaignNamecorp + "')]");
			if (isCampaignNamePresentCorpaccessibilitypublictoalluserscorpR2 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users Campaign created by Corporate user who does not have privilege to see private campaign by Regional user(R2)");
			}
			//Verify Public to all corporate users Campaign created by Corporate user who has privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Public to all corporate users Campaign created by Corporate user who does not have privilege to see private campaign by Regional user(R2)");
			campaignCenterPage.searchCampaignByFilter(driver, secondcampaignNamecorp);
			
			boolean isCampaignNamePresentCorpaccessibilitypublictoallcorporateuserscorpR2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondcampaignNamecorp + "')]");
			if (!isCampaignNamePresentCorpaccessibilitypublictoallcorporateuserscorpR2 == false) {
				fc.utobj().throwsException("was not able to verify Public to all corporate users Campaign created by Corporate user who does not privilege to see private campaign by Regional user(R2)");
			}
			
			//Verify Private Campaign created by Corporate user who has privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Private Campaign created by Corporate user who does not have privilege to see private campaign by Regional user(R2)");
			campaignCenterPage.searchCampaignByFilter(driver, thirdcampaignNamecorp);
			
			boolean isCampaignNamePresentCorpaccessibilityprivatecorpR2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + thirdcampaignNamecorp + "')]");
			if (!isCampaignNamePresentCorpaccessibilityprivatecorpR2 == false) {
				fc.utobj().throwsException("was not able to Private Campaign created by Corporate user who does not have privilege to see private campaign by Regional user(R2)");
			}
		
			//Verify Public to all users (Status Driven)Campaign created by Corporate user who does not have privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Public to all users(Status Driven) Campaign created by Corporate user who does not have privilege to see private campaign by Regional user(R2)");
			campaignCenterPage.searchCampaignByFilter(driver, firstcampaignNamecorpstatus);

			boolean isCampaignNamePresentCorpaccessibilitypublictoalluserscorpstatusR2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firstcampaignNamecorpstatus + "')]");
			if (isCampaignNamePresentCorpaccessibilitypublictoalluserscorpstatusR2 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users(Status Driven) Campaign created by Corporate user who does not have privilege to see private campaign by Regional user(R2)");
			}
			//Verify Public to all corporate users Campaign created by Corporate user who has privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Public to all corporate users(Status Driven) Campaign created by Corporate user who does not have privilege to see private campaign by Regional user(R2)");
			campaignCenterPage.searchCampaignByFilter(driver, secondcampaignNamecorpstatus);
			
			boolean isCampaignNamePresentCorpaccessibilitypublictoallcorporateuserscorpstatusR2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondcampaignNamecorpstatus + "')]");
			if (!isCampaignNamePresentCorpaccessibilitypublictoallcorporateuserscorpstatusR2 == false) {
				fc.utobj().throwsException("was not able to verify Public to all corporate users(Status Driven) Campaign created by Corporate user who does not privilege to see private campaign by Regional user(R2)");
			}
			
			//Verify Private Campaign created by Corporate user who has privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Private Campaign created by Corporate user(Status Driven) who does not have privilege to see private campaign by Regional user(R2)");
			campaignCenterPage.searchCampaignByFilter(driver, thirdcampaignNamecorpstatus);
			
			boolean isCampaignNamePresentCorpaccessibilityprivatecorpstatusR2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + thirdcampaignNamecorpstatus + "')]");
			if (!isCampaignNamePresentCorpaccessibilityprivatecorpstatusR2 == false) {
				fc.utobj().throwsException("was not able to Private Campaign created by Corporate user(Status Driven) who does not have privilege to see private campaign by Regional user(R2)");
			}
				
			fc.utobj().printTestStep("Navigate to CRM>Campaign Center>email template");
			emailtemplate.clickonemailtamplate(driver);
			
			//Verify Public to all users template created by Corporate user who does not have privilege to see private template by itself
			fc.utobj().printTestStep("Verify Public to all users template created by Corporate user who does not have privilege to see private template by Regional user(R2)");
			campaignCenterPage.searchTemplateByFilter(driver, firsttemplateNamecorp);

			boolean isTemplateNamePresentCorpaccessibilitypublictoalluserscorpR2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firsttemplateNamecorp + "')]");
			if (isTemplateNamePresentCorpaccessibilitypublictoalluserscorpR2 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users template created by Corporate user who has privilege to see private template by Regional user(R2)");
			}
			//Verify Public to all corporate users template created by Corporate user who has privilege to see private template by itself
			fc.utobj().printTestStep("Verify Public to all corporate users template created by Corporate user who does not have privilege to see private template by Regional user(R2)");
			campaignCenterPage.searchTemplateByFilter(driver, secondtemplateNamecorp);
			
			boolean isTemplateNamePresentCorpaccessibilitypublictoallcorporateuserscorpR2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondtemplateNamecorp + "')]");
			if (!isTemplateNamePresentCorpaccessibilitypublictoallcorporateuserscorpR2 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users template created by Corporate user who has privilege to see private template by Regional user(R2)");
			}
			
			//Verify Private template created by Corporate user who has privilege to see private template by itself
			fc.utobj().printTestStep("Verify Privare template created by Corporate user who does not have privilege to see private template by Regional user(R2)");
			campaignCenterPage.searchTemplateByFilter(driver, thirdtemplateNamecorp);
			
			boolean isTemplateNamePresentCorpaccessibilityprivatecorpR2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + thirdtemplateNamecorp + "')]");
			if (!isTemplateNamePresentCorpaccessibilityprivatecorpR2 == false) {
				fc.utobj().throwsException("was not able to verify private template created by Corporate user who has privilege to see private template by Regional user(R2)");
			}
			
			//Verify Public to all users (Status driven)template created by Corporate user who does not have privilege to see private template by itself
			fc.utobj().printTestStep("Verify Public to all users (Status driven)template created by Corporate user who does not have privilege to see private template by Regional user(R2)");
			campaignCenterPage.searchTemplateByFilter(driver, firsttemplateNamecorpstatus);

			boolean isTemplateNamePresentCorpaccessibilitypublictoalluserscorpstatusR2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firsttemplateNamecorpstatus + "')]");
			if (isTemplateNamePresentCorpaccessibilitypublictoalluserscorpstatusR2 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users (Status driven)template created by Corporate user who has privilege to see private template by Regional user(R2)");
			}
			//Verify Public to all corporate users template created by Corporate user who has privilege to see private template by itself
			fc.utobj().printTestStep("Verify Public to all corporate users (Status driven)template created by Corporate user who does not have privilege to see private template by Regional user(R2)");
			campaignCenterPage.searchTemplateByFilter(driver, secondtemplateNamecorpstatus);
			
			boolean isTemplateNamePresentCorpaccessibilitypublictoallcorporateuserscorpstatusR2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondtemplateNamecorpstatus + "')]");
			if (!isTemplateNamePresentCorpaccessibilitypublictoallcorporateuserscorpstatusR2 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users (Status driven)template created by Corporate user who has privilege to see private template by Regional user(R2)");
			}
			
			//Verify Private template created by Corporate user who has privilege to see private template by itself
			fc.utobj().printTestStep("Verify Privare (Status driven)template created by Corporate user who does not have privilege to see private template by Regional user(R2)");
			campaignCenterPage.searchTemplateByFilter(driver, thirdtemplateNamecorpstatus);
			
			boolean isTemplateNamePresentCorpaccessibilityprivatecorpstatusR2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + thirdtemplateNamecorpstatus + "')]");
			if (!isTemplateNamePresentCorpaccessibilityprivatecorpstatusR2 == false) {
				fc.utobj().throwsException("was not able to verify private (Status driven)template created by Corporate user who has privilege to see private template by Regional user(R2)");
			}
		
			fc.utobj().printTestStep("Navigate to CRM>Campaign Center>campaign template");
			emailtemplate.clickoncampaigntemplate(driver);
			//Verify Public to all users of my region Campaign created by Regional user who has privilege to see private campaign by itself(R1)
			//Verifying Campaigns and templates of Regional user (R1) after logging with Corporate user (R2)
			fc.utobj().printTestStep("Verifying Campaigns and templates of Regional user (R1) after logging with Regional user (R2)");
			fc.utobj().printTestStep("Verify Public to all users of my region Campaign created by Regional user who has privilege to see private campaign by Regional user(R2)");
			campaignCenterPage.searchCampaignByFilter(driver, firstcampaignNamereg);

			boolean isCampaignNamePresentRegpaccessibilitypublictoallusersofmyregionR2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firstcampaignNamereg + "')]");
			if (isCampaignNamePresentRegpaccessibilitypublictoallusersofmyregionR2 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my region Campaign created by Regional user who has privilege to see private campaign by Regional user(R2)");
			}
			
			//Verify Private Campaign created by Regional user who has privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Private Campaign created by Regional user who has privilege to see private campaign by itself");
			campaignCenterPage.searchCampaignByFilter(driver, secondcampaignNamereg);
			
			boolean isCampaignNamePresentRegpaccessibilityprivateR2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondcampaignNamereg + "')]");
			if (!isCampaignNamePresentRegpaccessibilityprivateR2 == false) {
				fc.utobj().throwsException("was not able to verify Private Campaign created by Regional user who has privilege to see private campaign by Regional user(R2)");
			}
			
			//Verify Public to all users of my region (Status Driven)Campaign created by Regional user who has privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Public to all users of my region (Status Driven)Campaign created by Regional user who has privilege to see private campaign by Regional user(R2)");
			campaignCenterPage.searchCampaignByFilter(driver, firstcampaignNameregstatus);

			boolean isCampaignNamePresentRegpaccessibilitypublictoallusersofmyregionstatusR2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firstcampaignNameregstatus + "')]");
			if (isCampaignNamePresentRegpaccessibilitypublictoallusersofmyregionstatusR2 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my region (Status Driven)Campaign created by Regional user who has privilege to see private campaign by Regional user(R2)");
			}
			
			//Verify Private Campaign created by Regional user who has privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Private (Status Driven)Campaign created by Regional user who has privilege to see private campaign by Regional user(R2)");
			campaignCenterPage.searchCampaignByFilter(driver, secondcampaignNameregstatus);
			
			boolean isCampaignNamePresentRegpaccessibilityprivatestatusR2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondcampaignNameregstatus + "')]");
			if (!isCampaignNamePresentRegpaccessibilityprivatestatusR2 == false) {
				fc.utobj().throwsException("was not able to verify Private (Status Driven)Campaign created by Regional user who has privilege to see private campaign by Regional user(R2)");
			}
			
			fc.utobj().printTestStep("Navigate to CRM>Campaign Center>email template");
			emailtemplate.clickonemailtamplate(driver);
			
			//Verify Public to all users template created by Corporate user who has privilege to see private template by itself
			fc.utobj().printTestStep("Verify Public to all users of my region template created by Regional user who has privilege to see private template by Regional user(R2)");
			campaignCenterPage.searchTemplateByFilter(driver, firsttemplateNamereg);

			boolean isTemplateNamePresentRegaccessibilitypublictoallusersofmyregionR2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firsttemplateNamereg + "')]");
			if (isTemplateNamePresentRegaccessibilitypublictoallusersofmyregionR2 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users template created by Regional user who has privilege to see private template by Regional user(R2)");
			}
			
			//Verify Private template created by Regional user who has privilege to see private template by itself
			fc.utobj().printTestStep("Verify Private template created by Regional user who has privilege to see private template by Regional user(R2)");
			campaignCenterPage.searchTemplateByFilter(driver, secondtemplateNamereg);
			
			boolean isTemplateNamePresentRegaccessibilityprivateR2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondtemplateNamereg + "')]");
			if (!isTemplateNamePresentRegaccessibilityprivateR2 == false) {
				fc.utobj().throwsException("was not able to verify private template created by Regional user who has privilege to see private template by Regional user(R2)");
			}
			
			//Verify Public to all users(Status Driven) template created by Regional user who has privilege to see private template by itself
			fc.utobj().printTestStep("Verify Public to all users of my region (Status Driven)template created by Regional user who has privilege to see private template by Regional user(R2)");
			campaignCenterPage.searchTemplateByFilter(driver, firsttemplateNameregstatus);

			boolean isTemplateNamePresentRegaccessibilitypublictoallusersofmyregionstatusR2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firsttemplateNameregstatus + "')]");
			if (isTemplateNamePresentRegaccessibilitypublictoallusersofmyregionstatusR2 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my region (Status Driven)template created by Regional user who has privilege to see private template by Regional user(R2)");
			}
			
			//Verify Private template created by Regional user who has privilege to see private template by itself
			fc.utobj().printTestStep("Verify Private (Status Driven)template created by Regional user who has privilege to see private template by Regional user(R2)");
			campaignCenterPage.searchTemplateByFilter(driver, secondtemplateNameregstatus);
			
			boolean isTemplateNamePresentRegaccessibilityprivatestatusR2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondtemplateNameregstatus + "')]");
			if (!isTemplateNamePresentRegaccessibilityprivatestatusR2 == false) {
				fc.utobj().throwsException("was not able to verify private (Status Driven)template created by Regional user who has privilege to see private template by Regional user(R2)");
			}
			fc.utobj().printTestStep("Navigate to CRM>Campaign Center>campaign template");
			emailtemplate.clickoncampaigntemplate(driver);
			
			//Verify Public to all users Campaign created by Corporate user who has privilege to see private campaign by itself
			fc.utobj().printTestStep("Verifying Campaigns and templates of Corporate user (C1) by Regional user(R2)");
			fc.utobj().printTestStep("Verify Public to all users Campaign created by Corporate user who has privilege to see private campaign by Regional user(R2)");
			campaignCenterPage.searchCampaignByFilter(driver, firstcampaignName);

			boolean isCampaignNamePresentCorpaccessibilitypublictoallusersR2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firstcampaignName + "')]");
			if (isCampaignNamePresentCorpaccessibilitypublictoallusersR2 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users Campaign created by Corporate user who has privilege to see private campaign by Regional user(R2)");
			}
			//Verify Public to all corporate users Campaign created by Corporate user who has privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Public to all corporate users Campaign created by Corporate user who has privilege to see private campaign");
			campaignCenterPage.searchCampaignByFilter(driver, secondcampaignName);
			
			boolean isCampaignNamePresentCorpaccessibilitypublictoallcorporateusersR2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondcampaignName + "')]");
			if (!isCampaignNamePresentCorpaccessibilitypublictoallcorporateusersR2 == false) {
				fc.utobj().throwsException("was not able to verify Public to all corporate users Campaign created by Corporate user who has privilege to see private campaign by Regional user(R2)");
			}
			
			//Verify Private Campaign created by Corporate user who has privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Private Campaign created by Corporate user who has privilege to see private campaign by Regional user(R2)");
			campaignCenterPage.searchCampaignByFilter(driver, thirdcampaignName);
			
			boolean isCampaignNamePresentCorpaccessibilityprivateR2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + thirdcampaignName + "')]");
			if (!isCampaignNamePresentCorpaccessibilityprivateR2 == false) {
				fc.utobj().throwsException("was not able to Private Campaign created by Corporate user who has privilege to see private campaign by Regional user(R2)");
			}
			
			//Verify Public to all users(Status Driven) Campaign created by Corporate user who has privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Public to all users(Status Driven) Campaign created by Corporate user who has privilege to see private campaign by Regional user(R2)");
			campaignCenterPage.searchCampaignByFilter(driver, firstcampaignNamestatus);

			boolean isCampaignNamePresentCorpaccessibilitypublictoallusersstatusR2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firstcampaignNamestatus + "')]");
			if (isCampaignNamePresentCorpaccessibilitypublictoallusersstatusR2 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users(Status Driven) Campaign created by Corporate user who has privilege to see private campaign by Regional user(R2)");
			}
			//Verify Public to all corporate users(Status Driven) Campaign created by Corporate user who has privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Public to all corporate users(Status Driven) Campaign created by Corporate user who has privilege to see private campaign");
			campaignCenterPage.searchCampaignByFilter(driver, secondcampaignNamestatus);
			
			boolean isCampaignNamePresentCorpaccessibilitypublictoallcorporateusersstatusR2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondcampaignNamestatus + "')]");
			if (!isCampaignNamePresentCorpaccessibilitypublictoallcorporateusersstatusR2 == false) {
				fc.utobj().throwsException("was not able to verify Public to all corporate users(Status Driven) Campaign created by Corporate user who has privilege to see private campaign by Regional user(R2)");
			}
			
			//Verify Private Campaign created by Corporate user(Status Driven) who has privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Private Campaign(Status Driven) created by Corporate user who has privilege to see private campaign by Regional user(R2)");
			campaignCenterPage.searchCampaignByFilter(driver, thirdcampaignNamestatus);
			
			boolean isCampaignNamePresentCorpaccessibilityprivatestatusR2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + thirdcampaignNamestatus + "')]");
			if (!isCampaignNamePresentCorpaccessibilityprivatestatusR2 == false) {
				fc.utobj().throwsException("was not able to Private Campaign(Status Driven) created by Corporate user who has privilege to see private campaign by Regional user(R2)");
			}
		
			fc.utobj().printTestStep("Navigate to CRM>Campaign Center>email template");
			emailtemplate.clickonemailtamplate(driver);
			
			//Verify Public to all users template created by Corporate user who has privilege to see private template by itself
			fc.utobj().printTestStep("Verify Public to all users template created by Corporate user who has privilege to see private template by Regional user(R2)");
			campaignCenterPage.searchTemplateByFilter(driver, firsttemplateName);

			boolean isTemplateNamePresentCorpaccessibilitypublictoallusersR2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firsttemplateName + "')]");
			if (isTemplateNamePresentCorpaccessibilitypublictoallusersR2 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users template created by Corporate user who has privilege to see private template by Regional user(R2)");
			}
			//Verify Public to all corporate users template created by Corporate user who has privilege to see private template by itself
			fc.utobj().printTestStep("Verify Public to all corporate users template created by Corporate user who has privilege to see private template by Regional user(R2)");
			campaignCenterPage.searchTemplateByFilter(driver, secondtemplateName);
			
			boolean isTemplateNamePresentCorpaccessibilitypublictoallcorporateusersR2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondtemplateName + "')]");
			if (!isTemplateNamePresentCorpaccessibilitypublictoallcorporateusersR2 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users template created by Corporate user who has privilege to see private template by Regional user(R2)");
			}
			
			//Verify Private template created by Corporate user who has privilege to see private template by itself
			fc.utobj().printTestStep("Verify Privare template created by Corporate user who has privilege to see private template by Regional user(R2)");
			campaignCenterPage.searchTemplateByFilter(driver, thirdtemplateName);
			
			boolean isTemplateNamePresentCorpaccessibilityprivateR2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + thirdtemplateName + "')]");
			if (!isTemplateNamePresentCorpaccessibilityprivateR2 == false) {
				fc.utobj().throwsException("was not able to verify private template created by Corporate user who has privilege to see private template by Regional user(R2)");
			}
				
			//Verify Public to all users template(Status Driven) created by Corporate user who has privilege to see private template by itself
			fc.utobj().printTestStep("Verify Public to all users template(Status Driven) created by Corporate user who has privilege to see private template by Regional user(R2)");
			campaignCenterPage.searchTemplateByFilter(driver, firsttemplateNamestatus);

			boolean isTemplateNamePresentCorpaccessibilitypublictoallusersstatusR2= fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firsttemplateNamestatus + "')]");
			if (isTemplateNamePresentCorpaccessibilitypublictoallusersstatusR2 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users template(Status Driven) created by Corporate user who has privilege to see private template by Regional user(R2)");
			}
			//Verify Public to all corporate users template(Status Driven) created by Corporate user who has privilege to see private template by itself
			fc.utobj().printTestStep("Verify Public to all corporate users template(Status Driven) created by Corporate user who has privilege to see private template by Regional user(R2)");
			campaignCenterPage.searchTemplateByFilter(driver, secondtemplateNamestatus);
			
			boolean isTemplateNamePresentCorpaccessibilitypublictoallcorporateusersstatusR2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondtemplateNamestatus + "')]");
			if (!isTemplateNamePresentCorpaccessibilitypublictoallcorporateusersstatusR2 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users template(Status Driven) created by Corporate user who has privilege to see private template by Regional user(R2)");
			}
			
			//Verify Private template(Status Driven) created by Corporate user who has privilege to see private template by itself
			fc.utobj().printTestStep("Verify Privare template(Status Driven) created by Corporate user who has privilege to see private template by Regional user(R2)");
			campaignCenterPage.searchTemplateByFilter(driver, thirdtemplateNamestatus);
			
			boolean isTemplateNamePresentCorpaccessibilityprivatestatusR2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + thirdtemplateNamestatus + "')]");
			if (!isTemplateNamePresentCorpaccessibilityprivatestatusR2 == false) {
				fc.utobj().throwsException("was not able to verify private template(Status Driven) created by Corporate user who has privilege to see private template by Regional user(R2)");
			}
		
			fc.utobj().printTestStep("Navigate to CRM>Campaign Center>campaign template");
			emailtemplate.clickoncampaigntemplate(driver);
			
			//Verify Public to all users Campaign created by Corporate user who has privilege to see private campaign by itself
			fc.utobj().printTestStep("Verifying Campaigns and templates of Regional user (R2) by itself");
			//Verify Public to all users of my region Campaign created by Regional user who does not have privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Public to all users of my region Campaign created by Regional user who does not have privilege to see private campaign by itself");
			campaignCenterPage.searchCampaignByFilter(driver, firstcampaignNamereg1);

			boolean isCampaignNamePresentRegpaccessibilitypublictoallusersofmyregion1R2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firstcampaignNamereg1 + "')]");
			if (isCampaignNamePresentRegpaccessibilitypublictoallusersofmyregion1R2 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my region Campaign created by Regional user who does not have privilege to see private campaign by itself");
			}
			
			//Verify Private Campaign created by Regional user who does not have privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Private Campaign created by Regional user who does not have privilege to see private campaign by itself");
			campaignCenterPage.searchCampaignByFilter(driver, secondcampaignNamereg1);
			
			boolean isCampaignNamePresentRegpaccessibilityprivate1R2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondcampaignNamereg1 + "')]");
			if (isCampaignNamePresentRegpaccessibilityprivate1R2 == false) {
				fc.utobj().throwsException("was not able to verify Private Campaign created by Regional user who does not have privilege to see private campaign by itself");
			}
			
			//Verify Public to all users of my region (Status Driven)Campaign created by Regional user who does not have privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Public to all users of my region (Status Driven)Campaign created by Regional user who does not have privilege to see private campaign by itself");
			campaignCenterPage.searchCampaignByFilter(driver, firstcampaignNamereg1status);

			boolean isCampaignNamePresentRegpaccessibilitypublictoallusersofmyregion1statusR2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firstcampaignNamereg1status + "')]");
			if (isCampaignNamePresentRegpaccessibilitypublictoallusersofmyregion1statusR2 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my region (Status Driven)Campaign created by Regional user who does not have privilege to see private campaign by itself");
			}
			
			//Verify Private (Status Driven)Campaign created by Regional user who does not have privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Private (Status Driven)Campaign created by Regional user who does not have privilege to see private campaign by itself");
			campaignCenterPage.searchCampaignByFilter(driver, secondcampaignNamereg1status);
			
			boolean isCampaignNamePresentRegpaccessibilityprivate1statusR2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondcampaignNamereg1status + "')]");
			if (isCampaignNamePresentRegpaccessibilityprivate1statusR2 == false) {
				fc.utobj().throwsException("was not able to verify Private (Status Driven)Campaign created by Regional user who does not have privilege to see private campaign itself");
			}
		
			fc.utobj().printTestStep("Navigate to CRM>Campaign Center>email template");
			emailtemplate.clickonemailtamplate(driver);
			
			//Verify Public to all users template created by Corporate user who does not have privilege to see private template by itself
			fc.utobj().printTestStep("Verify Public to all users of my region template created by Regional user who does not have privilege to see private template by itself");
			campaignCenterPage.searchTemplateByFilter(driver, firsttemplateNamereg1);

			boolean isTemplateNamePresentRegaccessibilitypublictoallusersofmyregion1R2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firsttemplateNamereg1 + "')]");
			if (isTemplateNamePresentRegaccessibilitypublictoallusersofmyregion1R2 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my region template created by Regional user who does not have privilege to see private template by itself");
			}
			
			//Verify Private template created by Regional user who does not have privilege to see private template by itself
			fc.utobj().printTestStep("Verify Private template created by Regional user does not have privilege to see private template by itself");
			campaignCenterPage.searchTemplateByFilter(driver, secondtemplateNamereg1);
			
			boolean isTemplateNamePresentRegaccessibilityprivate1R2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondtemplateNamereg1 + "')]");
			if (isTemplateNamePresentRegaccessibilityprivate1R2 == false) {
				fc.utobj().throwsException("was not able to verify Private template created by Regional user does not have privilege to see private template by itself");
			}
			
			//Verify Public to all users (Status Driven)template created by Regional user who does not have privilege to see private template by itself
			fc.utobj().printTestStep("Verify Public to all users of my region (Status Driven)template created by Regional user who does not have privilege to see private template by itself");
			campaignCenterPage.searchTemplateByFilter(driver, firsttemplateNamereg1status);

			boolean isTemplateNamePresentRegaccessibilitypublictoallusersofmyregion1statusR2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firsttemplateNamereg1status + "')]");
			if (isTemplateNamePresentRegaccessibilitypublictoallusersofmyregion1statusR2 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my region (Status Driven)template created by Regional user who does not have privilege to see private template by itself");
			}
			
			//Verify Private template created by Regional user who does not have privilege to see private template by itself
			fc.utobj().printTestStep("Verify Private (Status Driven)template created by Regional user does not have privilege to see private template by itself");
			campaignCenterPage.searchTemplateByFilter(driver, secondtemplateNamereg1status);
			
			boolean isTemplateNamePresentRegaccessibilityprivate1statusR2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondtemplateNamereg1status + "')]");
			if (isTemplateNamePresentRegaccessibilityprivate1statusR2 == false) {
				fc.utobj().throwsException("was not able to verify Private (Status Driven)template created by Regional user does not have privilege to see private template by itself");
			}
			
			fc.utobj().printTestStep("Navigate to CRM>Campaign Center>campaign template");
			emailtemplate.clickoncampaigntemplate(driver);
			
			fc.utobj().clickElement(driver, crmcenter.locationCampaign);
			fc.utobj().printTestStep("Verifying Campaigns and templates of Franchise owner user (F01) by Regional user(R2)");
			fc.utobj().printTestStep("Verify Public to all users of my Franchise Campaign created by Franchise user who has privilege to see private campaign by Regional user(R2)");
			campaignCenterPage.searchCampaignByFilter(driver, firstcampaignNamefran);

			boolean isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchiseR2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firstcampaignNamefran + "')]");
			if (isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchiseR2 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my Franchise Campaign created by Franchise user who has privilege to see private campaign by Regional user(R2)");
			}
			
			//Verify Private Campaign created by Franchise user who has privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Private Campaign created by Franchise user who has privilege to see private campaign by Regional user(R2)");
			campaignCenterPage.searchCampaignByFilter(driver, secondcampaignNamefran);
			
			boolean isCampaignNamePresentFranpaccessibilityprivateR2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondcampaignNamefran + "')]");
			if (!isCampaignNamePresentFranpaccessibilityprivateR2 == false) {
				fc.utobj().throwsException("was not able to verify Private Campaign created by Franchise user who has privilege to see private campaign by Regional user(R2)");
			}
			
			
			//Verify Public to all users of my Franchise (Status Driven)Campaign created by Franchise user who has privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Public to all users of my Franchise (Status Driven)Campaign created by Franchise user who has privilege to see private campaign by Regional user(R2)");
			campaignCenterPage.searchCampaignByFilter(driver, firstcampaignNamefranstatus);

			boolean isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchisestatusR2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firstcampaignNamefranstatus + "')]");
			if (isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchisestatusR2 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my Franchise (Status Driven)Campaign created by Franchise user who has privilege to see private campaign by Regional user(R2)");
			}
			
			//Verify Private Campaign created by Franchise user who has privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Private (Status Driven)Campaign created by Franchise user who has privilege to see private campaign by Regional user(R2)");
			campaignCenterPage.searchCampaignByFilter(driver, secondcampaignNamefranstatus);
			
			boolean isCampaignNamePresentFranpaccessibilityprivatestatusR2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondcampaignNamefranstatus + "')]");
			if (!isCampaignNamePresentFranpaccessibilityprivatestatusR2 == false) {
				fc.utobj().throwsException("was not able to verify Private (Status Driven)Campaign created by Franchise user who has privilege to see private campaign by Regional user(R2)");
			}
			
			fc.utobj().printTestStep("Navigate to CRM>Campaign Center>email template");
			emailtemplate.clickonemailtamplate(driver);
			fc.utobj().clickElement(driver, crmcenter.locationtemplate);
			//Verify Public to all users template created by Corporate user who has privilege to see private template by itself
			fc.utobj().printTestStep("Verify Public to all users of my franchise template created by franchise user who has privilege to see private template by Regional user(R2)");
			campaignCenterPage.searchTemplateByFilter(driver, firsttemplateNamefran);

			boolean isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchiseR2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firsttemplateNamefran + "')]");
			if (isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchiseR2 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my franchise template created by franchise user who has privilege to see private template by Regional user(R2)");
			}
			
			//Verify Private template created by franchise user who has privilege to see private template by itself
			fc.utobj().printTestStep("Verify Private template created by franchise user who has privilege to see private template by Regional user(R2)");
			campaignCenterPage.searchTemplateByFilter(driver, secondtemplateNamefran);
			
			boolean isTemplateNamePresentfranaccessibilityprivateR2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondtemplateNamefran + "')]");
			if (!isTemplateNamePresentfranaccessibilityprivateR2 == false) {
				fc.utobj().throwsException("was not able to verify private template created by franchise user who has privilege to see private template by Regional user(R2)");
			}
			
			//Verify Public to all users (Status Driven)template created by Corporate user who has privilege to see private template by itself
			fc.utobj().printTestStep("Verify Public to all users of my franchise (Status Driven)template created by franchise user who has privilege to see private template by Regional user(R2)");
			campaignCenterPage.searchTemplateByFilter(driver, firsttemplateNamefranstatus);

			boolean isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchisestatusR2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firsttemplateNamefranstatus + "')]");
			if (isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchisestatusR2 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my franchise (Status Driven)template created by franchise user who has privilege to see private template by Regional user(R2)");
			}
			
			//Verify Private template created by franchise user who has privilege to see private template by itself
			fc.utobj().printTestStep("Verify Private (Status Driven)template created by franchise user who has privilege to see private template by Regional user(R2)");
			campaignCenterPage.searchTemplateByFilter(driver, secondtemplateNamefranstatus);
			
			boolean isTemplateNamePresentfranaccessibilityprivatestatusR2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondtemplateNamefranstatus + "')]");
			if (!isTemplateNamePresentfranaccessibilityprivatestatusR2 == false) {
				fc.utobj().throwsException("was not able to verify private (Status Driven)template created by franchise user who has privilege to see private template by Regional user(R2)");
			}
			fc.utobj().printTestStep("Navigate to CRM>Campaign Center>campaign template");
			emailtemplate.clickoncampaigntemplate(driver);
			fc.utobj().clickElement(driver, crmcenter.locationCampaign);
			fc.utobj().printTestStep("Verifying Campaigns and templates of Franchise Employee user (FE1) by Regional user(R2)");
			
			fc.utobj().printTestStep("Verify Public to all users of my Franchise Campaign created by Franchise Employee user(FE1) who has privilege to see private campaign by Regional user(R2)");
			campaignCenterPage.searchCampaignByFilter(driver, firstcampaignNamefrane);

			boolean isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchiseER2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firstcampaignNamefrane + "')]");
			if (isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchiseER2 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my Franchise Campaign created by Franchise Employee user(FE1) who has privilege to see private campaign by Regional user(R2)");
			}
			
			//Verify Private Campaign created by Franchise user who has privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Private Campaign created by Franchise Employee user(FE1) who has privilege to see private campaign by Regional user(R2)");
			campaignCenterPage.searchCampaignByFilter(driver, secondcampaignNamefrane);
			
			boolean isCampaignNamePresentFranpaccessibilityprivateER2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondcampaignNamefrane + "')]");
			if (!isCampaignNamePresentFranpaccessibilityprivateER2 == false) {
				fc.utobj().throwsException("was not able to verify Private Campaign created by Franchise Employee user(FE1) who has privilege to see private campaign by Regional user(R2)");
			}
			
			
			//Verify Public to all users of my Franchise (Status Driven)Campaign created by Franchise user who has privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Public to all users of my Franchise (Status Driven)Campaign created by Franchise Employee user(FE1) who has privilege to see private campaign by Regional user(R2)");
			campaignCenterPage.searchCampaignByFilter(driver, firstcampaignNamefranestatus);

			boolean isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchiseEstatusR2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firstcampaignNamefranestatus + "')]");
			if (isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchiseEstatusR2 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my Franchise (Status Driven)Campaign created by Franchise Employee user(FE1) who has privilege to see private campaign by Regional user(R2)");
			}
			
			//Verify Private Campaign created by Franchise user who has privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Private (Status Driven)Campaign created by Franchise Employee user(FE1) who has privilege to see private campaign by Regional user(R2)");
			campaignCenterPage.searchCampaignByFilter(driver, secondcampaignNamefranestatus);
			
			boolean isCampaignNamePresentFranpaccessibilityprivateEstatusR2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondcampaignNamefranestatus + "')]");
			if (!isCampaignNamePresentFranpaccessibilityprivateEstatusR2 == false) {
				fc.utobj().throwsException("was not able to verify Private (Status Driven)Campaign created by Franchise Employee user(FE1) who has privilege to see private campaign by Regional user(R2)");
			}
		
			fc.utobj().printTestStep("Navigate to CRM>Campaign Center>email template");
			emailtemplate.clickonemailtamplate(driver);
			fc.utobj().clickElement(driver, crmcenter.locationtemplate);
			//Verify Public to all users template created by Franchise Employee user user who has privilege to see private template by itself
			fc.utobj().printTestStep("Verify Public to all users of my franchise template created by franchise Employee user(FE1) who has privilege to see private template by Regional user(R2)");
			campaignCenterPage.searchTemplateByFilter(driver, firsttemplateNamefrane);

			boolean isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchiseER2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firsttemplateNamefrane + "')]");
			if (isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchiseER2 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my franchise template created by franchise Employee user(FE1) who has privilege to see private template by Regional user(R2)");
			}
			
			//Verify Private template created by franchise Employee user who has privilege to see private template by itself
			fc.utobj().printTestStep("Verify Private template created by franchise Employee user(FE1) who has privilege to see private template by Regional user(R2)");
			campaignCenterPage.searchTemplateByFilter(driver, secondtemplateNamefrane);
			
			boolean isTemplateNamePresentfranaccessibilityprivateER2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondtemplateNamefrane + "')]");
			if (!isTemplateNamePresentfranaccessibilityprivateER2 == false) {
				fc.utobj().throwsException("was not able to verify private template created by franchise Employee user(FE1) who has privilege to see private template by Regional user(R2)");
			}
			
			
			//Verify Public to all users (Status Driven)template created by Franchise Employee user user who has privilege to see private template by itself
			fc.utobj().printTestStep("Verify Public to all users of my franchise (Status Driven)template created by franchise Employee user(FE1) who has privilege to see private template by Regional user(R2)");
			campaignCenterPage.searchTemplateByFilter(driver, firsttemplateNamefranestatus);

			boolean isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchiseEstatusR2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firsttemplateNamefranestatus + "')]");
			if (isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchiseEstatusR2 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my franchise (Status Driven)template created by franchise Employee user(FE1) who has privilege to see private template by Regional user(R2)");
			}
			
			//Verify Private template created by franchise Employee user who has privilege to see private template by itself
			fc.utobj().printTestStep("Verify Private (Status Driven)template created by franchise Employee user(FE1) who has privilege to see private template by Regional user(R2)");
			campaignCenterPage.searchTemplateByFilter(driver, secondtemplateNamefranestatus);
			
			boolean isTemplateNamePresentfranaccessibilityprivateEstatusR2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondtemplateNamefranestatus + "')]");
			if (!isTemplateNamePresentfranaccessibilityprivateEstatusR2 == false) {
				fc.utobj().throwsException("was not able to verify private (Status Driven)template created by franchise Employee user(FE1) who has privilege to see private template by Regional user(R2)");
			}
			
			
			fc.utobj().printTestStep("Navigate to CRM>Campaign Center>campaign template");
			emailtemplate.clickoncampaigntemplate(driver);
			fc.utobj().clickElement(driver, crmcenter.locationCampaign);
			fc.utobj().printTestStep("Verifying Campaigns and templates of Franchise Employee user (F02) by Regional user(R2)");
			
			
			//Verify Public to all users of my Franchise Campaign created by Franchise user who does not have privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Public to all users of my Franchise Campaign created by Franchise user(FO2) who does not have privilege to see private campaign by Regional user(R2)");
			campaignCenterPage.searchCampaignByFilter(driver, firstcampaignNamefranO2);

			boolean isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchiseO2R2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firstcampaignNamefranO2 + "')]");
			if (isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchiseO2R2 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my Franchise Campaign created by Franchise user(FO2) who does not have privilege to see private campaign by Regional user(R2)");
			}
			
			//Verify Private Campaign created by Franchise user who has privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Private Campaign created by Franchise user(FO2) who does not have privilege to see private campaign by Regional user(R2)");
			campaignCenterPage.searchCampaignByFilter(driver, secondcampaignNamefranO2);
			
			boolean isCampaignNamePresentFranpaccessibilityprivateO2R2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondcampaignNamefranO2 + "')]");
			if (!isCampaignNamePresentFranpaccessibilityprivateO2R2 == false) {
				fc.utobj().throwsException("was not able to verify Private Campaign created by Franchise user(FO2) who does not have privilege to see private campaign by Regional user(R2)");
			}
			
			//Verify Public to all users of my Franchise (Status Driven)Campaign created by Franchise user who does not have privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Public to all users of my Franchise (Status Driven)Campaign created by Franchise user(FO2) who does not have privilege to see private campaign by Regional user(R2)");
			campaignCenterPage.searchCampaignByFilter(driver, firstcampaignNamefranO2status);

			boolean isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchiseO2statusR2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firstcampaignNamefranO2status + "')]");
			if (isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchiseO2statusR2 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my (Status Driven)Franchise Campaign created by Franchise user(FO2) who does not have privilege to see private campaign by Regional user(R2)");
			}
			
			//Verify Private Campaign created by Franchise user who has privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Private (Status Driven)Campaign created by Franchise user(FO2) who does not have privilege to see private campaign by Regional user(R2)");
			campaignCenterPage.searchCampaignByFilter(driver, secondcampaignNamefranO2status);
			
			boolean isCampaignNamePresentFranpaccessibilityprivateO2statusR2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondcampaignNamefranO2status + "')]");
			if (!isCampaignNamePresentFranpaccessibilityprivateO2statusR2 == false) {
				fc.utobj().throwsException("was not able to verify Private (Status Driven)Campaign created by Franchise user(FO2) who does not have privilege to see private campaign by Regional user(R2)");
			}
		
			fc.utobj().printTestStep("Navigate to CRM>Campaign Center>email template");
			emailtemplate.clickonemailtamplate(driver);
			fc.utobj().clickElement(driver, crmcenter.locationtemplate);
			//Verify Public to all users template created by Corporate user who has privilege to see private template by itself
			fc.utobj().printTestStep("Verify Public to all users of my franchise template created by franchise user(FO2) who does not have privilege to see private template by Regional user(R2)");
			campaignCenterPage.searchTemplateByFilter(driver, firsttemplateNamefranO2);

			boolean isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchiseO2R2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firsttemplateNamefranO2 + "')]");
			if (isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchiseO2R2 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my franchise template created by franchise user(FO2) who does not have privilege to see private template by Regional user(R2)");
			}
			
			//Verify Private template created by franchise user who has privilege to see private template by itself
			fc.utobj().printTestStep("Verify Private template created by franchise user(FO2) who does not have privilege to see private template by Regional user(R2)");
			campaignCenterPage.searchTemplateByFilter(driver, secondtemplateNamefranO2);
			
			boolean isTemplateNamePresentfranaccessibilityprivateO2R2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondtemplateNamefranO2 + "')]");
			if (!isTemplateNamePresentfranaccessibilityprivateO2R2 == false) {
				fc.utobj().throwsException("was not able to verify private template created by franchise user(FO2) who does not have privilege to see private template by Regional user(R2)");
			}
			
			//Verify Public to all users template created by Corporate user who has privilege to see private template by itself
			fc.utobj().printTestStep("Verify Public to all users of my franchise (Status Driven)template created by franchise user(FO2) who does not have privilege to see private template by Regional user(R2)");
			campaignCenterPage.searchTemplateByFilter(driver, firsttemplateNamefranO2status);

			boolean isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchiseO2statusR2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firsttemplateNamefranO2status + "')]");
			if (isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchiseO2statusR2 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my franchise (Status Driven)template created by franchise user(FO2) who does not have privilege to see private template by Regional user(R2)");
			}
			
			//Verify Private template created by franchise user who has privilege to see private template by itself
			fc.utobj().printTestStep("Verify Private (Status Driven)template created by franchise user(FO2) who does not have privilege to see private template by Regional user(R2)");
			campaignCenterPage.searchTemplateByFilter(driver, secondtemplateNamefranO2status);
			
			boolean isTemplateNamePresentfranaccessibilityprivateO2statusR2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondtemplateNamefranO2status + "')]");
			if (!isTemplateNamePresentfranaccessibilityprivateO2statusR2 == false) {
				fc.utobj().throwsException("was not able to verify private (Status Driven)template created by franchise user(FO2) who does not have privilege to see private template by Regional user(R2)");
			}
			
			fc.utobj().printTestStep("Navigate to CRM>Campaign Center>campaign template");
			emailtemplate.clickoncampaigntemplate(driver);
			fc.utobj().clickElement(driver, crmcenter.locationCampaign);
			fc.utobj().printTestStep("Verifying Campaigns and templates of Franchise Employee user (FE2) by Regional user(R2)");
			
			//Verify Public to all users of my Franchise Campaign created by Franchise user who has privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Public to all users of my Franchise Campaign created by Franchise Employee user(FE2) who does not have privilege to see private campaign by Regional user(R2)");
			campaignCenterPage.searchCampaignByFilter(driver, firstcampaignNamefrane2);

			boolean isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchiseE2R2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firstcampaignNamefrane2 + "')]");
			if (isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchiseE2R2 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my Franchise Campaign created by Franchise Employee user(FE2) who does not have privilege to see private campaign by Regional user(R2)");
			}
			
			//Verify Private Campaign created by Franchise user who has privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Private Campaign created by Franchise Employee user(FE2) who does not have privilege to see private campaign by Regional user(R2)");
			campaignCenterPage.searchCampaignByFilter(driver, secondcampaignNamefrane2);
			
			boolean isCampaignNamePresentFranpaccessibilityprivateE2R2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondcampaignNamefrane2 + "')]");
			if (!isCampaignNamePresentFranpaccessibilityprivateE2R2 == false) {
				fc.utobj().throwsException("was not able to verify Private Campaign created by Franchise Employee user(FE2) who does not have privilege to see private campaign by Regional user(R2)");
			}
			
			
			//Verify Public to all users of my Franchise Campaign created by Franchise user who has privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Public to all users of my Franchise (Status Driven)Campaign created by Franchise Employee user(FE2) who does not have privilege to see private campaign by Regional user(R2)");
			campaignCenterPage.searchCampaignByFilter(driver, firstcampaignNamefrane2status);

			boolean isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchiseE2statusR2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firstcampaignNamefrane2status + "')]");
			if (isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchiseE2statusR2 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my Franchise (Status Driven)Campaign created by Franchise Employee user(FE2) who does not have privilege to see private campaign by Regional user(R2)");
			}
			
			//Verify Private Campaign created by Franchise user who has privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Private (Status Driven)Campaign created by Franchise Employee user(FE2) who does not have privilege to see private campaign by Regional user(R2)");
			campaignCenterPage.searchCampaignByFilter(driver, secondcampaignNamefrane2status);
			
			boolean isCampaignNamePresentFranpaccessibilityprivateE2statusR2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondcampaignNamefrane2status + "')]");
			if (!isCampaignNamePresentFranpaccessibilityprivateE2statusR2 == false) {
				fc.utobj().throwsException("was not able to verify Private (Status Driven)Campaign created by Franchise Employee user(FE2) who does not have privilege to see private campaign by Regional user(R2)");
			}
			
		
			fc.utobj().printTestStep("Navigate to CRM>Campaign Center>email template");
			emailtemplate.clickonemailtamplate(driver);
			fc.utobj().clickElement(driver, crmcenter.locationtemplate);
			//Verify Public to all users template created by Franchise Employee user user who has privilege to see private template by itself
			fc.utobj().printTestStep("Verify Public to all users of my franchise template created by franchise Employee user(FE2) who does not have privilege to see private template by Regional user(R2)");
			campaignCenterPage.searchTemplateByFilter(driver, firsttemplateNamefrane2);

			boolean isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchiseE2R2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firsttemplateNamefrane2 + "')]");
			if (isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchiseE2R2 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my franchise template created by franchise Employee user(FE2) who does not have privilege to see private template by Regional user(R2)");
			}
			
			//Verify Private template created by franchise Employee user who has privilege to see private template by itself
			fc.utobj().printTestStep("Verify Private template created by franchise Employee user(FE2) who does not have privilege to see private template by Regional user(R2)");
			campaignCenterPage.searchTemplateByFilter(driver, secondtemplateNamefrane2);
			
			boolean isTemplateNamePresentfranaccessibilityprivateE2R2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondtemplateNamefrane2 + "')]");
			if (!isTemplateNamePresentfranaccessibilityprivateE2R2 == false) {
				fc.utobj().throwsException("was not able to verify private template created by franchise Employee user(FE2) who does not have privilege to see private template by Regional user(R2)");
			}
			
			//Verify Public to all users template created by Franchise Employee user user who has privilege to see private template by itself
			fc.utobj().printTestStep("Verify Public to all users of my franchise (Status Driven)template created by franchise Employee user(FE2) who does not have privilege to see private template by Regional user(R2)");
			campaignCenterPage.searchTemplateByFilter(driver, firsttemplateNamefrane2status);

			boolean isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchiseE2statusR2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firsttemplateNamefrane2status + "')]");
			if (isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchiseE2statusR2 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my franchise(Status Driven) template created by franchise Employee user(FE2) who does not have privilege to see private template by Regional user(R2)");
			}
			
			//Verify Private template created by franchise Employee user who has privilege to see private template by itself
			fc.utobj().printTestStep("Verify Private (Status Driven)template created by franchise Employee user(FE2) who does not have privilege to see private template by Regional user(R2)");
			campaignCenterPage.searchTemplateByFilter(driver, secondtemplateNamefrane2status);
			
			boolean isTemplateNamePresentfranaccessibilityprivateE2statusR2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondtemplateNamefrane2status + "')]");
			if (!isTemplateNamePresentfranaccessibilityprivateE2statusR2 == false) {
				fc.utobj().throwsException("was not able to verify private (Status Driven)template created by franchise Employee user(FE2) who does not have privilege to see private template by Regional user(R2)");
			}
		
			
			fc.utobj().printTestStep("Navigate to CRM>Campaign Center>campaign template");
			emailtemplate.clickoncampaigntemplate(driver);
			fc.utobj().clickElement(driver, crmcenter.locationCampaign);
			fc.utobj().printTestStep("Verifying Campaigns and templates of Franchise Mu user (V1) by Regional user(R2)");
			
			fc.utobj().printTestStep("Verify Public to all users of my Franchise Campaign created by Franchise MU user(V1) who has privilege to see private campaign by Regional user(R2)");
			campaignCenterPage.searchCampaignByFilter(driver, firstcampaignNamefranmu);

			boolean isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchisemuR2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firstcampaignNamefranmu + "')]");
			if (isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchisemuR2 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my Franchise Campaign created by Franchise MU user(V1) who has privilege to see private campaign by Regional user(R2)");
			}
			
			//Verify Private Campaign created by Franchise user who has privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Private Campaign created by Franchise MU user(V1) who has privilege to see private campaign by Regional user(R2)");
			campaignCenterPage.searchCampaignByFilter(driver, secondcampaignNamefranmu);
			
			boolean isCampaignNamePresentFranpaccessibilityprivatemuR2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondcampaignNamefranmu + "')]");
			if (!isCampaignNamePresentFranpaccessibilityprivatemuR2 == false) {
				fc.utobj().throwsException("was not able to verify Private Campaign created by Franchise MU user(V1) who has privilege to see private campaign by Regional user(R2)");
			}
			
			
			//Verify Public to all users of my Franchise (Status Driven)Campaign created by Franchise user who has privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Public to all users of my Franchise (Status Driven)Campaign created by Franchise MU user(V1) who has privilege to see private campaign by Regional user(R2)");
			campaignCenterPage.searchCampaignByFilter(driver, firstcampaignNamefranmustatus);

			boolean isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchisemustatusR2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firstcampaignNamefranmustatus + "')]");
			if (isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchisemustatusR2 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my Franchise (Status Driven)Campaign created by Franchise MU user(V1) who has privilege to see private campaign by Regional user(R2)");
			}
			
			//Verify Private Campaign created by Franchise user who has privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Private (Status Driven)Campaign created by Franchise MU user(V1) who has privilege to see private campaign by Regional user(R2)");
			campaignCenterPage.searchCampaignByFilter(driver, secondcampaignNamefranmustatus);
			
			boolean isCampaignNamePresentFranpaccessibilityprivatemustatusR2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondcampaignNamefranmustatus + "')]");
			if (!isCampaignNamePresentFranpaccessibilityprivatemustatusR2 == false) {
				fc.utobj().throwsException("was not able to verify Private (Status Driven)Campaign created by Franchise MU user(V1) who has privilege to see private campaign by Regional user(R2)");
			}
			
			fc.utobj().printTestStep("Navigate to CRM>Campaign Center>email template");
			emailtemplate.clickonemailtamplate(driver);
			fc.utobj().clickElement(driver, crmcenter.locationtemplate);
			//Verify Public to all users template created by Corporate user who has privilege to see private template by itself
			fc.utobj().printTestStep("Verify Public to all users of my franchise template created by franchise MU user(V1) who has privilege to see private template by Regional user(R2)");
			campaignCenterPage.searchTemplateByFilter(driver, firsttemplateNamefranmu);

			boolean isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchisemuR2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firsttemplateNamefranmu + "')]");
			if (isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchisemuR2 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my franchise template created by franchise MU user(V1) who has privilege to see private template by Regional user(R2)");
			}
			
			//Verify Private template created by franchise user who has privilege to see private template by itself
			fc.utobj().printTestStep("Verify Private template created by franchise MU user(V1) who has privilege to see private template by Regional user(R2)");
			campaignCenterPage.searchTemplateByFilter(driver, secondtemplateNamefranmu);
			
			boolean isTemplateNamePresentfranaccessibilityprivatemuR2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondtemplateNamefranmu + "')]");
			if (!isTemplateNamePresentfranaccessibilityprivatemuR2 == false) {
				fc.utobj().throwsException("was not able to verify private template created by franchise MU user(V1) who has privilege to see private template by Regional user(R2)");
			}
			
			//Verify Public to all users template created by Corporate user who has privilege to see private template by itself
			fc.utobj().printTestStep("Verify Public to all users of my franchise (Status Driven)template created by franchise MU user(V1) who has privilege to see private template by Regional user(R2)");
			campaignCenterPage.searchTemplateByFilter(driver, firsttemplateNamefranmustatus);

			boolean isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchisemustatusR2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firsttemplateNamefranmustatus + "')]");
			if (isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchisemustatusR2 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my franchise (Status Driven)template created by franchise MU user(V1) who has privilege to see private template by Regional user(R2)");
			}
			
			//Verify Private template created by franchise user who has privilege to see private template by itself
			fc.utobj().printTestStep("Verify Private (Status Driven)template created by franchise MU user(V1) who has privilege to see private template by Regional user(R2)");
			campaignCenterPage.searchTemplateByFilter(driver, secondtemplateNamefranmustatus);
			
			boolean isTemplateNamePresentfranaccessibilityprivatemustatusR2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondtemplateNamefranmustatus + "')]");
			if (!isTemplateNamePresentfranaccessibilityprivatemustatusR2 == false) {
				fc.utobj().throwsException("was not able to verify private (Status Driven)template created by franchise MU user(V1) who has privilege to see private template by Regional user(R2)");
			}
			
			
			
			
			fc.utobj().printTestStep("Navigate to CRM>Campaign Center>campaign template");
			emailtemplate.clickoncampaigntemplate(driver);
			fc.utobj().clickElement(driver, crmcenter.locationCampaign);
			fc.utobj().printTestStep("Verifying Campaigns and templates of Franchise Employee user (V2) by Regional user(R2)");
			fc.utobj().printTestStep("Verify Public to all users of my Franchise Campaign created by Franchise Employee user(V2) who does not have privilege to see private campaign by Regional user(R2)");
			campaignCenterPage.searchCampaignByFilter(driver, firstcampaignNamefranmue2);

			boolean isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchisemue2R2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firstcampaignNamefranmue2 + "')]");
			if (isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchisemue2R2 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my Franchise Campaign created by Franchise Employee user(V2) who does not have privilege to see private campaign by Regional user(R2)");
			}
			
			//Verify Private Campaign created by Franchise user who has privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Private Campaign created by Franchise Employee user(V2) who does not have privilege to see private campaign by Regional user(R2)");
			campaignCenterPage.searchCampaignByFilter(driver, secondcampaignNamefranmue2);
			
			boolean isCampaignNamePresentFranpaccessibilityprivatemue2R2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondcampaignNamefranmue2 + "')]");
			if (!isCampaignNamePresentFranpaccessibilityprivatemue2R2 == false) {
				fc.utobj().throwsException("was not able to verify Private Campaign created by Franchise Employee user(V2) who does not have privilege to see private campaign by Regional user(R2)");
			}
			
			
			//Verify Public to all users of my Franchise Campaign created by Franchise Employee user(V2) who has privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Public to all users of my Franchise (Status Driven)Campaign created by Franchise Employee user(V2) who does not have privilege to see private campaign by Regional user(R2)");
			campaignCenterPage.searchCampaignByFilter(driver, firstcampaignNamefranmue2status);

			boolean isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchisemue2statusR2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firstcampaignNamefranmue2status + "')]");
			if (isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchisemue2statusR2 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my Franchise (Status Driven)Campaign created by Franchise Employee user(V2) who does not have privilege to see private campaign by Regional user(R2)");
			}
			
			//Verify Private Campaign created by Franchise user who has privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Private (Status Driven)Campaign created by Franchise Employee user(V2) who does not have privilege to see private campaign by Regional user(R2)");
			campaignCenterPage.searchCampaignByFilter(driver, secondcampaignNamefranmue2status);
			
			boolean isCampaignNamePresentFranpaccessibilityprivatemue2statusR2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondcampaignNamefranmue2status + "')]");
			if (!isCampaignNamePresentFranpaccessibilityprivatemue2statusR2 == false) {
				fc.utobj().throwsException("was not able to verify Private (Status Driven)Campaign created by Franchise Employee user(V2) who does not have privilege to see private campaign by Regional user(R2)");
			}
			
			fc.utobj().printTestStep("Navigate to CRM>Campaign Center>email template");
			emailtemplate.clickonemailtamplate(driver);
			fc.utobj().clickElement(driver, crmcenter.locationtemplate);
			//Verify Public to all users template created by Franchise user who has privilege to see private template by itself
			fc.utobj().printTestStep("Verify Public to all users of my franchise template created by franchise Employee user(V2) who does not have privilege to see private template by Regional user(R2)");
			campaignCenterPage.searchTemplateByFilter(driver, firsttemplateNamefranmue2);

			boolean isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchisemue2R2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firsttemplateNamefranmue2 + "')]");
			if (isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchisemue2R2 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my franchise template created by franchise Employee user(V2) who has privilege to see private template by Regional user(R2)");
			}
			
			//Verify Private template created by franchise user who has privilege to see private template by itself
			fc.utobj().printTestStep("Verify Private template created by franchise Employee user(V2) who has privilege to see private template by Regional user(R2)");
			campaignCenterPage.searchTemplateByFilter(driver, secondtemplateNamefranmue2);
			
			boolean isTemplateNamePresentfranaccessibilityprivatemue2R2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondtemplateNamefranmue2 + "')]");
			if (!isTemplateNamePresentfranaccessibilityprivatemue2R2 == false) {
				fc.utobj().throwsException("was not able to verify private template created by franchise Employee user(V2) who has privilege to see private template by Regional user(R2)");
			}
			
			//Verify Public to all users template created by Franchise user who has privilege to see private template by itself
			fc.utobj().printTestStep("Verify Public to all users of my franchise (Status Driven)template created by franchise Employee user(V2) who does not have privilege to see private template by Regional user(R2)");
			campaignCenterPage.searchTemplateByFilter(driver, firsttemplateNamefranmue2status);

			boolean isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchisemue2statusR2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firsttemplateNamefranmue2status + "')]");
			if (isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchisemue2statusR2 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my franchise (Status Driven)template created by franchise Employee user(V2) who has privilege to see private template by Regional user(R2)");
			}
			
			//Verify Private template created by franchise user who has privilege to see private template by itself
			fc.utobj().printTestStep("Verify Private (Status Driven)template created by franchise Employee user(V2) who has privilege to see private template by Regional user(R2)");
			campaignCenterPage.searchTemplateByFilter(driver, secondtemplateNamefranmue2status);
			
			boolean isTemplateNamePresentfranaccessibilityprivatemue2statusR2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondtemplateNamefranmue2status + "')]");
			if (!isTemplateNamePresentfranaccessibilityprivatemue2statusR2 == false) {
				fc.utobj().throwsException("was not able to verify private (Status Driven)template created by franchise Employee user(V2) who has privilege to see private template by Regional user(R2)");
			}
			
			if(isCampaignNamePresentCorpaccessibilitypublictoalluserscorpR2 && !isCampaignNamePresentCorpaccessibilitypublictoallcorporateuserscorpR2 && !isCampaignNamePresentCorpaccessibilityprivatecorpR2 
					&& isCampaignNamePresentCorpaccessibilitypublictoalluserscorpstatusR2 && !isCampaignNamePresentCorpaccessibilitypublictoallcorporateuserscorpstatusR2 && !isCampaignNamePresentCorpaccessibilityprivatecorpstatusR2 
					&& isTemplateNamePresentCorpaccessibilitypublictoalluserscorpR2 && !isTemplateNamePresentCorpaccessibilitypublictoallcorporateuserscorpR2 && !isTemplateNamePresentCorpaccessibilityprivatecorpR2 
					&& isTemplateNamePresentCorpaccessibilitypublictoalluserscorpstatusR2 && !isTemplateNamePresentCorpaccessibilitypublictoallcorporateuserscorpstatusR2 && !isTemplateNamePresentCorpaccessibilityprivatecorpstatusR2 
					&& isCampaignNamePresentRegpaccessibilitypublictoallusersofmyregionR2 && !isCampaignNamePresentRegpaccessibilityprivateR2 && isCampaignNamePresentRegpaccessibilitypublictoallusersofmyregionstatusR2 
					&& !isCampaignNamePresentRegpaccessibilityprivatestatusR2 && isTemplateNamePresentRegaccessibilitypublictoallusersofmyregionR2 && !isTemplateNamePresentRegaccessibilityprivateR2 
					&& isTemplateNamePresentRegaccessibilitypublictoallusersofmyregionstatusR2 && !isTemplateNamePresentRegaccessibilityprivatestatusR2 && isCampaignNamePresentCorpaccessibilitypublictoallusersR2 
					&& !isCampaignNamePresentCorpaccessibilitypublictoallcorporateusersR2 && !isCampaignNamePresentCorpaccessibilityprivateR2 && isCampaignNamePresentCorpaccessibilitypublictoallusersstatusR2 
					&& !isCampaignNamePresentCorpaccessibilitypublictoallcorporateusersstatusR2 && !isCampaignNamePresentCorpaccessibilityprivatestatusR2 && isTemplateNamePresentCorpaccessibilitypublictoallusersR2 
					&& !isTemplateNamePresentCorpaccessibilitypublictoallcorporateusersR2 && !isTemplateNamePresentCorpaccessibilityprivateR2 && isTemplateNamePresentCorpaccessibilitypublictoallusersstatusR2 
					&& !isTemplateNamePresentCorpaccessibilitypublictoallcorporateusersstatusR2 && !isTemplateNamePresentCorpaccessibilityprivatestatusR2 && isCampaignNamePresentRegpaccessibilitypublictoallusersofmyregion1R2
					&& isCampaignNamePresentRegpaccessibilityprivate1R2 && isCampaignNamePresentRegpaccessibilitypublictoallusersofmyregion1statusR2 && isCampaignNamePresentRegpaccessibilityprivate1statusR2
					&& isTemplateNamePresentRegaccessibilitypublictoallusersofmyregion1R2 && isTemplateNamePresentRegaccessibilityprivate1R2 && isTemplateNamePresentRegaccessibilitypublictoallusersofmyregion1statusR2 
					&& isTemplateNamePresentRegaccessibilityprivate1statusR2 && isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchiseR2 && !isCampaignNamePresentFranpaccessibilityprivateR2 
					&& isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchisestatusR2 && !isCampaignNamePresentFranpaccessibilityprivatestatusR2 && isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchiseR2 
					&& !isTemplateNamePresentfranaccessibilityprivateR2 && isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchisestatusR2 && !isTemplateNamePresentfranaccessibilityprivatestatusR2 
					&& isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchiseER2 && !isCampaignNamePresentFranpaccessibilityprivateER2 && isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchiseEstatusR2 
					&& !isCampaignNamePresentFranpaccessibilityprivateEstatusR2 && isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchiseER2 && !isTemplateNamePresentfranaccessibilityprivateER2 
					&& isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchiseEstatusR2 && !isTemplateNamePresentfranaccessibilityprivateEstatusR2 && isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchiseO2R2 
					&& !isCampaignNamePresentFranpaccessibilityprivateO2R2 && isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchiseO2statusR2 && !isCampaignNamePresentFranpaccessibilityprivateO2statusR2 
					&& isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchiseO2R2 && !isTemplateNamePresentfranaccessibilityprivateO2R2 && isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchiseO2statusR2 
					&& !isTemplateNamePresentfranaccessibilityprivateO2statusR2 && isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchiseE2R2 && !isCampaignNamePresentFranpaccessibilityprivateE2R2 
					&& isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchiseE2statusR2 && !isCampaignNamePresentFranpaccessibilityprivateE2statusR2 && isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchiseE2R2
					&& !isTemplateNamePresentfranaccessibilityprivateE2R2 && isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchiseE2statusR2 && !isTemplateNamePresentfranaccessibilityprivateE2statusR2 
					&& isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchisemuR2 && !isCampaignNamePresentFranpaccessibilityprivatemuR2 && isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchisemustatusR2 
					&& !isCampaignNamePresentFranpaccessibilityprivatemustatusR2 && isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchisemuR2 && !isTemplateNamePresentfranaccessibilityprivatemuR2 
					&& isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchisemustatusR2 && !isTemplateNamePresentfranaccessibilityprivatemustatusR2 && isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchisemue2R2 
					&& !isCampaignNamePresentFranpaccessibilityprivatemue2R2 && isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchisemue2statusR2 && !isCampaignNamePresentFranpaccessibilityprivatemue2statusR2 
					&& isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchisemue2R2 && !isTemplateNamePresentfranaccessibilityprivatemue2R2 && isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchisemue2statusR2
					&& !isTemplateNamePresentfranaccessibilityprivatemue2statusR2) {
				fc.utobj().printTestStep("able to verify all types of campaigns and templates created by corporate/rgional/franchise users when loging with Regional user(R2) who does not have priviledge to see private campaign");
			}
			else {
				fc.utobj().throwsException("was not able to verify all types of campaigns and templates created by corporate/rgional/franchise users when loging with Regional user(R2) who does not have priviledge to see private campaign");
			}
				
			
			
			
			//Login with Franchise owner user(FO1) who has role to see private campaign
			fc.utobj().printTestStep("Login with Franchise owner user(FO1) who has role to see private campaign");
			fc.loginpage().loginWithParameter(driver, userNameFO, passwordFO);
			fc.crm().crm_common().openCRMCampaignCenterPage(fcHomePageTest, driver);
			fc.utobj().printTestStep("Click on the menu option and select campaign");
			/*CRMCampaignCenterPage pobj = new CRMCampaignCenterPage(driver);*/
			fc.utobj().clickElement(driver, pobj.menuOptions);
			fc.utobj().clickElement(driver, pobj.campaignLinks);
			
			//Verifying Campaigns and templates of corporate user (C2) after logging with Franchise owner user(F01)
			fc.utobj().printTestStep("Verifying Campaigns and templates of corporate user (C2) after logging with Franchise owner user(F01)");
			fc.utobj().printTestStep("Verify Public to all users Campaign created by Corporate user(C2) who does not have privilege to see private campaign by Franchise owner user(FO1)");
			campaignCenterPage.searchCampaignByFilter(driver, firstcampaignNamecorp);

			boolean isCampaignNamePresentCorpaccessibilitypublictoalluserscorpFO1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firstcampaignNamecorp + "')]");
			if (isCampaignNamePresentCorpaccessibilitypublictoalluserscorpFO1 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users Campaign created by Corporate user who does not have privilege to see private campaign by Franchise owner user(FO1)");
			}
			//Verify Public to all corporate users Campaign created by Corporate user who has privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Public to all corporate users Campaign created by Corporate user who does not have privilege to see private campaign by Franchise owner user(FO1)");
			campaignCenterPage.searchCampaignByFilter(driver, secondcampaignNamecorp);
			
			boolean isCampaignNamePresentCorpaccessibilitypublictoallcorporateuserscorpFO1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondcampaignNamecorp + "')]");
			if (!isCampaignNamePresentCorpaccessibilitypublictoallcorporateuserscorpFO1 == false) {
				fc.utobj().throwsException("was not able to verify Public to all corporate users Campaign created by Corporate user who does not privilege to see private campaign by Franchise owner user(FO1)");
			}
			
			//Verify Private Campaign created by Corporate user who has privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Private Campaign created by Corporate user who does not have privilege to see private campaign by Franchise owner user(FO1)");
			campaignCenterPage.searchCampaignByFilter(driver, thirdcampaignNamecorp);
			
			boolean isCampaignNamePresentCorpaccessibilityprivatecorpFO1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + thirdcampaignNamecorp + "')]");
			if (!isCampaignNamePresentCorpaccessibilityprivatecorpFO1 == false) {
				fc.utobj().throwsException("was not able to Private Campaign created by Corporate user who does not have privilege to see private campaign by Franchise owner user(FO1)");
			}
		
			//Verify Public to all users (Status Driven)Campaign created by Corporate user who does not have privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Public to all users(Status Driven) Campaign created by Corporate user who does not have privilege to see private campaign by Franchise owner user(FO1)");
			campaignCenterPage.searchCampaignByFilter(driver, firstcampaignNamecorpstatus);

			boolean isCampaignNamePresentCorpaccessibilitypublictoalluserscorpstatusFO1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firstcampaignNamecorpstatus + "')]");
			if (isCampaignNamePresentCorpaccessibilitypublictoalluserscorpstatusFO1 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users(Status Driven) Campaign created by Corporate user who does not have privilege to see private campaign by Franchise owner user(FO1)");
			}
			//Verify Public to all corporate users Campaign created by Corporate user who has privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Public to all corporate users(Status Driven) Campaign created by Corporate user who does not have privilege to see private campaign by Franchise owner user(FO1)");
			campaignCenterPage.searchCampaignByFilter(driver, secondcampaignNamecorpstatus);
			
			boolean isCampaignNamePresentCorpaccessibilitypublictoallcorporateuserscorpstatusFO1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondcampaignNamecorpstatus + "')]");
			if (!isCampaignNamePresentCorpaccessibilitypublictoallcorporateuserscorpstatusFO1 == false) {
				fc.utobj().throwsException("was not able to verify Public to all corporate users(Status Driven) Campaign created by Corporate user who does not privilege to see private campaign by Franchise owner user(FO1)");
			}
			
			//Verify Private Campaign created by Corporate user who has privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Private Campaign created by Corporate user(Status Driven) who does not have privilege to see private campaign by Franchise owner user(FO1)");
			campaignCenterPage.searchCampaignByFilter(driver, thirdcampaignNamecorpstatus);
			
			boolean isCampaignNamePresentCorpaccessibilityprivatecorpstatusFO1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + thirdcampaignNamecorpstatus + "')]");
			if (!isCampaignNamePresentCorpaccessibilityprivatecorpstatusFO1 == false) {
				fc.utobj().throwsException("was not able to Private Campaign created by Corporate user(Status Driven) who does not have privilege to see private campaign by Franchise owner user(FO1)");
			}
				
			fc.utobj().printTestStep("Navigate to CRM>Campaign Center>email template");
			emailtemplate.clickonemailtamplate(driver);
			
			//Verify Public to all users template created by Corporate user who does not have privilege to see private template by itself
			fc.utobj().printTestStep("Verify Public to all users template created by Corporate user who does not have privilege to see private template by Franchise owner user(FO1)");
			campaignCenterPage.searchTemplateByFilter(driver, firsttemplateNamecorp);

			boolean isTemplateNamePresentCorpaccessibilitypublictoalluserscorpFO1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firsttemplateNamecorp + "')]");
			if (isTemplateNamePresentCorpaccessibilitypublictoalluserscorpFO1 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users template created by Corporate user who has privilege to see private template by Franchise owner user(FO1)");
			}
			//Verify Public to all corporate users template created by Corporate user who has privilege to see private template by itself
			fc.utobj().printTestStep("Verify Public to all corporate users template created by Corporate user who does not have privilege to see private template by Franchise owner user(FO1)");
			campaignCenterPage.searchTemplateByFilter(driver, secondtemplateNamecorp);
			
			boolean isTemplateNamePresentCorpaccessibilitypublictoallcorporateuserscorpFO1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondtemplateNamecorp + "')]");
			if (!isTemplateNamePresentCorpaccessibilitypublictoallcorporateuserscorpFO1 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users template created by Corporate user who has privilege to see private template by Franchise owner user(FO1)");
			}
			
			//Verify Private template created by Corporate user who has privilege to see private template by itself
			fc.utobj().printTestStep("Verify Privare template created by Corporate user who does not have privilege to see private template by Franchise owner user(FO1)");
			campaignCenterPage.searchTemplateByFilter(driver, thirdtemplateNamecorp);
			
			boolean isTemplateNamePresentCorpaccessibilityprivatecorpFO1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + thirdtemplateNamecorp + "')]");
			if (!isTemplateNamePresentCorpaccessibilityprivatecorpFO1 == false) {
				fc.utobj().throwsException("was not able to verify private template created by Corporate user who has privilege to see private template by Franchise owner user(FO1)");
			}
			
			//Verify Public to all users (Status driven)template created by Corporate user who does not have privilege to see private template by itself
			fc.utobj().printTestStep("Verify Public to all users (Status driven)template created by Corporate user who does not have privilege to see private template by Franchise owner user(FO1)");
			campaignCenterPage.searchTemplateByFilter(driver, firsttemplateNamecorpstatus);

			boolean isTemplateNamePresentCorpaccessibilitypublictoalluserscorpstatusFO1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firsttemplateNamecorpstatus + "')]");
			if (isTemplateNamePresentCorpaccessibilitypublictoalluserscorpstatusFO1 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users (Status driven)template created by Corporate user who has privilege to see private template by Franchise owner user(FO1)");
			}
			//Verify Public to all corporate users template created by Corporate user who has privilege to see private template by itself
			fc.utobj().printTestStep("Verify Public to all corporate users (Status driven)template created by Corporate user who does not have privilege to see private template by Franchise owner user(FO1)");
			campaignCenterPage.searchTemplateByFilter(driver, secondtemplateNamecorpstatus);
			
			boolean isTemplateNamePresentCorpaccessibilitypublictoallcorporateuserscorpstatusFO1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondtemplateNamecorpstatus + "')]");
			if (!isTemplateNamePresentCorpaccessibilitypublictoallcorporateuserscorpstatusFO1 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users (Status driven)template created by Corporate user who has privilege to see private template by Franchise owner user(FO1)");
			}
			
			//Verify Private template created by Corporate user who has privilege to see private template by itself
			fc.utobj().printTestStep("Verify Privare (Status driven)template created by Corporate user who does not have privilege to see private template by Franchise owner user(FO1)");
			campaignCenterPage.searchTemplateByFilter(driver, thirdtemplateNamecorpstatus);
			
			boolean isTemplateNamePresentCorpaccessibilityprivatecorpstatusFO1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + thirdtemplateNamecorpstatus + "')]");
			if (!isTemplateNamePresentCorpaccessibilityprivatecorpstatusFO1 == false) {
				fc.utobj().throwsException("was not able to verify private (Status driven)template created by Corporate user who has privilege to see private template by Franchise owner user(FO1)");
			}
		
			fc.utobj().printTestStep("Navigate to CRM>Campaign Center>campaign template");
			emailtemplate.clickoncampaigntemplate(driver);
			//Verify Public to all users of my region Campaign created by Regional user who has privilege to see private campaign by itself(R1)
			//Verifying Campaigns and templates of Regional user (R1) after logging with Corporate user (FO1)
			fc.utobj().printTestStep("Verifying Campaigns and templates of Regional user (R1) after logging with Corporate user (FO1)");
			fc.utobj().printTestStep("Verify Public to all users of my region Campaign created by Regional user who has privilege to see private campaign by Franchise owner user(FO1)");
			campaignCenterPage.searchCampaignByFilter(driver, firstcampaignNamereg);

			boolean isCampaignNamePresentRegpaccessibilitypublictoallusersofmyregionFO1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firstcampaignNamereg + "')]");
			if (isCampaignNamePresentRegpaccessibilitypublictoallusersofmyregionFO1 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my region Campaign created by Regional user who has privilege to see private campaign by Franchise owner user(FO1)");
			}
			
			//Verify Private Campaign created by Regional user who has privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Private Campaign created by Regional user who has privilege to see private campaign by itself");
			campaignCenterPage.searchCampaignByFilter(driver, secondcampaignNamereg);
			
			boolean isCampaignNamePresentRegpaccessibilityprivateFO1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondcampaignNamereg + "')]");
			if (!isCampaignNamePresentRegpaccessibilityprivateFO1 == false) {
				fc.utobj().throwsException("was not able to verify Private Campaign created by Regional user who has privilege to see private campaign by Franchise owner user(FO1)");
			}
			
			//Verify Public to all users of my region (Status Driven)Campaign created by Regional user who has privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Public to all users of my region (Status Driven)Campaign created by Regional user who has privilege to see private campaign by Franchise owner user(FO1)");
			campaignCenterPage.searchCampaignByFilter(driver, firstcampaignNameregstatus);

			boolean isCampaignNamePresentRegpaccessibilitypublictoallusersofmyregionstatusFO1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firstcampaignNameregstatus + "')]");
			if (isCampaignNamePresentRegpaccessibilitypublictoallusersofmyregionstatusFO1 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my region (Status Driven)Campaign created by Regional user who has privilege to see private campaign by Franchise owner user(FO1)");
			}
			
			//Verify Private Campaign created by Regional user who has privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Private (Status Driven)Campaign created by Regional user who has privilege to see private campaign by Franchise owner user(FO1)");
			campaignCenterPage.searchCampaignByFilter(driver, secondcampaignNameregstatus);
			
			boolean isCampaignNamePresentRegpaccessibilityprivatestatusFO1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondcampaignNameregstatus + "')]");
			if (!isCampaignNamePresentRegpaccessibilityprivatestatusFO1 == false) {
				fc.utobj().throwsException("was not able to verify Private (Status Driven)Campaign created by Regional user who has privilege to see private campaign by Franchise owner user(FO1)");
			}
			
			fc.utobj().printTestStep("Navigate to CRM>Campaign Center>email template");
			emailtemplate.clickonemailtamplate(driver);
			
			//Verify Public to all users template created by Corporate user who has privilege to see private template by itself
			fc.utobj().printTestStep("Verify Public to all users of my region template created by Regional user who has privilege to see private template by Franchise owner user(FO1)");
			campaignCenterPage.searchTemplateByFilter(driver, firsttemplateNamereg);

			boolean isTemplateNamePresentRegaccessibilitypublictoallusersofmyregionFO1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firsttemplateNamereg + "')]");
			if (isTemplateNamePresentRegaccessibilitypublictoallusersofmyregionFO1 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users template created by Regional user who has privilege to see private template by Franchise owner user(FO1)");
			}
			
			//Verify Private template created by Regional user who has privilege to see private template by itself
			fc.utobj().printTestStep("Verify Private template created by Regional user who has privilege to see private template by Franchise owner user(FO1)");
			campaignCenterPage.searchTemplateByFilter(driver, secondtemplateNamereg);
			
			boolean isTemplateNamePresentRegaccessibilityprivateFO1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondtemplateNamereg + "')]");
			if (!isTemplateNamePresentRegaccessibilityprivateFO1 == false) {
				fc.utobj().throwsException("was not able to verify private template created by Regional user who has privilege to see private template by Franchise owner user(FO1)");
			}
			
			//Verify Public to all users(Status Driven) template created by Regional user who has privilege to see private template by itself
			fc.utobj().printTestStep("Verify Public to all users of my region (Status Driven)template created by Regional user who has privilege to see private template by Franchise owner user(FO1)");
			campaignCenterPage.searchTemplateByFilter(driver, firsttemplateNameregstatus);

			boolean isTemplateNamePresentRegaccessibilitypublictoallusersofmyregionstatusFO1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firsttemplateNameregstatus + "')]");
			if (isTemplateNamePresentRegaccessibilitypublictoallusersofmyregionstatusFO1 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my region (Status Driven)template created by Regional user who has privilege to see private template by Franchise owner user(FO1)");
			}
			
			//Verify Private template created by Regional user who has privilege to see private template by itself
			fc.utobj().printTestStep("Verify Private (Status Driven)template created by Regional user who has privilege to see private template by Franchise owner user(FO1)");
			campaignCenterPage.searchTemplateByFilter(driver, secondtemplateNameregstatus);
			
			boolean isTemplateNamePresentRegaccessibilityprivatestatusFO1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondtemplateNameregstatus + "')]");
			if (!isTemplateNamePresentRegaccessibilityprivatestatusFO1 == false) {
				fc.utobj().throwsException("was not able to verify private (Status Driven)template created by Regional user who has privilege to see private template by Franchise owner user(FO1)");
			}
			fc.utobj().printTestStep("Navigate to CRM>Campaign Center>campaign template");
			emailtemplate.clickoncampaigntemplate(driver);
			
			//Verify Public to all users Campaign created by Corporate user who has privilege to see private campaign by Franchise owner user(FO1)
			fc.utobj().printTestStep("Verifying Campaigns and templates of Corporate user (C1) by Franchise owner user(FO1)");
			fc.utobj().printTestStep("Verify Public to all users Campaign created by Corporate user who has privilege to see private campaign by Franchise owner user(FO1)");
			campaignCenterPage.searchCampaignByFilter(driver, firstcampaignName);

			boolean isCampaignNamePresentCorpaccessibilitypublictoallusersFO1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firstcampaignName + "')]");
			if (isCampaignNamePresentCorpaccessibilitypublictoallusersFO1 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users Campaign created by Corporate user who has privilege to see private campaign by Franchise owner user(FO1)");
			}
			//Verify Public to all corporate users Campaign created by Corporate user who has privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Public to all corporate users Campaign created by Corporate user who has privilege to see private campaign");
			campaignCenterPage.searchCampaignByFilter(driver, secondcampaignName);
			
			boolean isCampaignNamePresentCorpaccessibilitypublictoallcorporateusersFO1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondcampaignName + "')]");
			if (!isCampaignNamePresentCorpaccessibilitypublictoallcorporateusersFO1 == false) {
				fc.utobj().throwsException("was not able to verify Public to all corporate users Campaign created by Corporate user who has privilege to see private campaign by Franchise owner user(FO1)");
			}
			
			//Verify Private Campaign created by Corporate user who has privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Private Campaign created by Corporate user who has privilege to see private campaign by Franchise owner user(FO1)");
			campaignCenterPage.searchCampaignByFilter(driver, thirdcampaignName);
			
			boolean isCampaignNamePresentCorpaccessibilityprivateFO1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + thirdcampaignName + "')]");
			if (!isCampaignNamePresentCorpaccessibilityprivateFO1 == false) {
				fc.utobj().throwsException("was not able to Private Campaign created by Corporate user who has privilege to see private campaign by Franchise owner user(FO1)");
			}
			
			//Verify Public to all users(Status Driven) Campaign created by Corporate user who has privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Public to all users(Status Driven) Campaign created by Corporate user who has privilege to see private campaign by Franchise owner user(FO1)");
			campaignCenterPage.searchCampaignByFilter(driver, firstcampaignNamestatus);

			boolean isCampaignNamePresentCorpaccessibilitypublictoallusersstatusFO1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firstcampaignNamestatus + "')]");
			if (isCampaignNamePresentCorpaccessibilitypublictoallusersstatusFO1 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users(Status Driven) Campaign created by Corporate user who has privilege to see private campaign by Franchise owner user(FO1)");
			}
			//Verify Public to all corporate users(Status Driven) Campaign created by Corporate user who has privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Public to all corporate users(Status Driven) Campaign created by Corporate user who has privilege to see private campaign");
			campaignCenterPage.searchCampaignByFilter(driver, secondcampaignNamestatus);
			
			boolean isCampaignNamePresentCorpaccessibilitypublictoallcorporateusersstatusFO1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondcampaignNamestatus + "')]");
			if (!isCampaignNamePresentCorpaccessibilitypublictoallcorporateusersstatusFO1 == false) {
				fc.utobj().throwsException("was not able to verify Public to all corporate users(Status Driven) Campaign created by Corporate user who has privilege to see private campaign by Franchise owner user(FO1)");
			}
			
			//Verify Private Campaign created by Corporate user(Status Driven) who has privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Private Campaign(Status Driven) created by Corporate user who has privilege to see private campaign by Franchise owner user(FO1)");
			campaignCenterPage.searchCampaignByFilter(driver, thirdcampaignNamestatus);
			
			boolean isCampaignNamePresentCorpaccessibilityprivatestatusFO1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + thirdcampaignNamestatus + "')]");
			if (!isCampaignNamePresentCorpaccessibilityprivatestatusFO1 == false) {
				fc.utobj().throwsException("was not able to Private Campaign(Status Driven) created by Corporate user who has privilege to see private campaign by Franchise owner user(FO1)");
			}
		
			fc.utobj().printTestStep("Navigate to CRM>Campaign Center>email template");
			emailtemplate.clickonemailtamplate(driver);
			
			//Verify Public to all users template created by Corporate user who has privilege to see private template by itself
			fc.utobj().printTestStep("Verify Public to all users template created by Corporate user who has privilege to see private template by Franchise owner user(FO1)");
			campaignCenterPage.searchTemplateByFilter(driver, firsttemplateName);

			boolean isTemplateNamePresentCorpaccessibilitypublictoallusersFO1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firsttemplateName + "')]");
			if (isTemplateNamePresentCorpaccessibilitypublictoallusersFO1 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users template created by Corporate user who has privilege to see private template by Franchise owner user(FO1)");
			}
			//Verify Public to all corporate users template created by Corporate user who has privilege to see private template by itself
			fc.utobj().printTestStep("Verify Public to all corporate users template created by Corporate user who has privilege to see private template by Franchise owner user(FO1)");
			campaignCenterPage.searchTemplateByFilter(driver, secondtemplateName);
			
			boolean isTemplateNamePresentCorpaccessibilitypublictoallcorporateusersFO1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondtemplateName + "')]");
			if (!isTemplateNamePresentCorpaccessibilitypublictoallcorporateusersFO1 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users template created by Corporate user who has privilege to see private template by Franchise owner user(FO1)");
			}
			
			//Verify Private template created by Corporate user who has privilege to see private template by itself
			fc.utobj().printTestStep("Verify Privare template created by Corporate user who has privilege to see private template by Franchise owner user(FO1)");
			campaignCenterPage.searchTemplateByFilter(driver, thirdtemplateName);
			
			boolean isTemplateNamePresentCorpaccessibilityprivateFO1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + thirdtemplateName + "')]");
			if (!isTemplateNamePresentCorpaccessibilityprivateFO1 == false) {
				fc.utobj().throwsException("was not able to verify private template created by Corporate user who has privilege to see private template by Franchise owner user(FO1)");
			}
				
			//Verify Public to all users template(Status Driven) created by Corporate user who has privilege to see private template by itself
			fc.utobj().printTestStep("Verify Public to all users template(Status Driven) created by Corporate user who has privilege to see private template by Franchise owner user(FO1)");
			campaignCenterPage.searchTemplateByFilter(driver, firsttemplateNamestatus);

			boolean isTemplateNamePresentCorpaccessibilitypublictoallusersstatusFO1= fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firsttemplateNamestatus + "')]");
			if (isTemplateNamePresentCorpaccessibilitypublictoallusersstatusFO1 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users template(Status Driven) created by Corporate user who has privilege to see private template by Franchise owner user(FO1)");
			}
			//Verify Public to all corporate users template(Status Driven) created by Corporate user who has privilege to see private template by itself
			fc.utobj().printTestStep("Verify Public to all corporate users template(Status Driven) created by Corporate user who has privilege to see private template by Franchise owner user(FO1)");
			campaignCenterPage.searchTemplateByFilter(driver, secondtemplateNamestatus);
			
			boolean isTemplateNamePresentCorpaccessibilitypublictoallcorporateusersstatusFO1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondtemplateNamestatus + "')]");
			if (!isTemplateNamePresentCorpaccessibilitypublictoallcorporateusersstatusFO1 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users template(Status Driven) created by Corporate user who has privilege to see private template by Franchise owner user(FO1)");
			}
			
			//Verify Private template(Status Driven) created by Corporate user who has privilege to see private template by itself
			fc.utobj().printTestStep("Verify Privare template(Status Driven) created by Corporate user who has privilege to see private template by Franchise owner user(FO1)");
			campaignCenterPage.searchTemplateByFilter(driver, thirdtemplateNamestatus);
			
			boolean isTemplateNamePresentCorpaccessibilityprivatestatusFO1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + thirdtemplateNamestatus + "')]");
			if (!isTemplateNamePresentCorpaccessibilityprivatestatusFO1 == false) {
				fc.utobj().throwsException("was not able to verify private template(Status Driven) created by Corporate user who has privilege to see private template by Franchise owner user(FO1)");
			}
		
			fc.utobj().printTestStep("Navigate to CRM>Campaign Center>campaign template");
			emailtemplate.clickoncampaigntemplate(driver);
			
			//Verify Public to all users Campaign created by Corporate user who has privilege to see private campaign by itself
			fc.utobj().printTestStep("Verifying Campaigns and templates of Regional user (R2) by Franchise owner user(FO1)");
			//Verify Public to all users of my region Campaign created by Regional user who does not have privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Public to all users of my region Campaign created by Regional user who does not have privilege to see private campaign by Franchise owner user(FO1)");
			campaignCenterPage.searchCampaignByFilter(driver, firstcampaignNamereg1);

			boolean isCampaignNamePresentRegpaccessibilitypublictoallusersofmyregion1FO1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firstcampaignNamereg1 + "')]");
			if (isCampaignNamePresentRegpaccessibilitypublictoallusersofmyregion1FO1 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my region Campaign created by Regional user who does not have privilege to see private campaign by Franchise owner user(FO1)");
			}
			
			//Verify Private Campaign created by Regional user who does not have privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Private Campaign created by Regional user who does not have privilege to see private campaign by Franchise owner user(FO1)");
			campaignCenterPage.searchCampaignByFilter(driver, secondcampaignNamereg1);
			
			boolean isCampaignNamePresentRegpaccessibilityprivate1FO1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondcampaignNamereg1 + "')]");
			if (!isCampaignNamePresentRegpaccessibilityprivate1FO1 == false) {
				fc.utobj().throwsException("was not able to verify Private Campaign created by Regional user who does not have privilege to see private campaign by Franchise owner user(FO1)");
			}
			
			//Verify Public to all users of my region (Status Driven)Campaign created by Regional user who does not have privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Public to all users of my region (Status Driven)Campaign created by Regional user who does not have privilege to see private campaign by Franchise owner user(FO1)");
			campaignCenterPage.searchCampaignByFilter(driver, firstcampaignNamereg1status);

			boolean isCampaignNamePresentRegpaccessibilitypublictoallusersofmyregion1statusFO1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firstcampaignNamereg1status + "')]");
			if (isCampaignNamePresentRegpaccessibilitypublictoallusersofmyregion1statusFO1 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my region (Status Driven)Campaign created by Regional user who does not have privilege to see private campaign by Franchise owner user(FO1)");
			}
			
			//Verify Private (Status Driven)Campaign created by Regional user who does not have privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Private (Status Driven)Campaign created by Regional user who does not have privilege to see private campaign by Franchise owner user(FO1)");
			campaignCenterPage.searchCampaignByFilter(driver, secondcampaignNamereg1status);
			
			boolean isCampaignNamePresentRegpaccessibilityprivate1statusFO1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondcampaignNamereg1status + "')]");
			if (!isCampaignNamePresentRegpaccessibilityprivate1statusFO1 == false) {
				fc.utobj().throwsException("was not able to verify Private (Status Driven)Campaign created by Regional user who does not have privilege to see private campaign by Franchise owner user(FO1)");
			}
		
			fc.utobj().printTestStep("Navigate to CRM>Campaign Center>email template");
			emailtemplate.clickonemailtamplate(driver);
			
			//Verify Public to all users template created by Corporate user who does not have privilege to see private template by itself
			fc.utobj().printTestStep("Verify Public to all users of my region template created by Regional user who does not have privilege to see private template by Franchise owner user(FO1)");
			campaignCenterPage.searchTemplateByFilter(driver, firsttemplateNamereg1);

			boolean isTemplateNamePresentRegaccessibilitypublictoallusersofmyregion1FO1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firsttemplateNamereg1 + "')]");
			if (isTemplateNamePresentRegaccessibilitypublictoallusersofmyregion1FO1 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my region template created by Regional user who does not have privilege to see private template by Franchise owner user(FO1)");
			}
			
			//Verify Private template created by Regional user who does not have privilege to see private template by itself
			fc.utobj().printTestStep("Verify Private template created by Regional user does not have privilege to see private template by Franchise owner user(FO1)");
			campaignCenterPage.searchTemplateByFilter(driver, secondtemplateNamereg1);
			
			boolean isTemplateNamePresentRegaccessibilityprivate1FO1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondtemplateNamereg1 + "')]");
			if (!isTemplateNamePresentRegaccessibilityprivate1FO1 == false) {
				fc.utobj().throwsException("was not able to verify Private template created by Regional user does not have privilege to see private template by Franchise owner user(FO1)");
			}
			
			//Verify Public to all users (Status Driven)template created by Regional user who does not have privilege to see private template by itself
			fc.utobj().printTestStep("Verify Public to all users of my region (Status Driven)template created by Regional user who does not have privilege to see private template by Franchise owner user(FO1)");
			campaignCenterPage.searchTemplateByFilter(driver, firsttemplateNamereg1status);

			boolean isTemplateNamePresentRegaccessibilitypublictoallusersofmyregion1statusFO1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firsttemplateNamereg1status + "')]");
			if (isTemplateNamePresentRegaccessibilitypublictoallusersofmyregion1statusFO1 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my region (Status Driven)template created by Regional user who does not have privilege to see private template by Franchise owner user(FO1)");
			}
			
			//Verify Private template created by Regional user who does not have privilege to see private template by itself
			fc.utobj().printTestStep("Verify Private (Status Driven)template created by Regional user does not have privilege to see private template by Franchise owner user(FO1)");
			campaignCenterPage.searchTemplateByFilter(driver, secondtemplateNamereg1status);
			
			boolean isTemplateNamePresentRegaccessibilityprivate1statusFO1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondtemplateNamereg1status + "')]");
			if (!isTemplateNamePresentRegaccessibilityprivate1statusFO1 == false) {
				fc.utobj().throwsException("was not able to verify Private (Status Driven)template created by Regional user does not have privilege to see private template by Franchise owner user(FO1)");
			}
			
			fc.utobj().printTestStep("Navigate to CRM>Campaign Center>campaign template");
			emailtemplate.clickoncampaigntemplate(driver);
			
			fc.utobj().clickElement(driver, crmcenter.locationCampaign);
			fc.utobj().printTestStep("Verifying Campaigns and templates of Franchise owner user (F01) by Franchise owner user(FO1)");
			fc.utobj().printTestStep("Verify Public to all users of my Franchise Campaign created by Franchise user who has privilege to see private campaign by Franchise owner user(FO1)");
			campaignCenterPage.searchCampaignByFilter(driver, firstcampaignNamefran);

			boolean isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchiseFO1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firstcampaignNamefran + "')]");
			if (isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchiseFO1 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my Franchise Campaign created by Franchise user who has privilege to see private campaign by Franchise owner user(FO1)");
			}
			
			//Verify Private Campaign created by Franchise user who has privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Private Campaign created by Franchise user who has privilege to see private campaign by Franchise owner user(FO1)");
			campaignCenterPage.searchCampaignByFilter(driver, secondcampaignNamefran);
			
			boolean isCampaignNamePresentFranpaccessibilityprivateFO1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondcampaignNamefran + "')]");
			if (isCampaignNamePresentFranpaccessibilityprivateFO1 == false) {
				fc.utobj().throwsException("was not able to verify Private Campaign created by Franchise user who has privilege to see private campaign by Franchise owner user(FO1)");
			}
			
			
			//Verify Public to all users of my Franchise (Status Driven)Campaign created by Franchise user who has privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Public to all users of my Franchise (Status Driven)Campaign created by Franchise user who has privilege to see private campaign by Franchise owner user(FO1)");
			campaignCenterPage.searchCampaignByFilter(driver, firstcampaignNamefranstatus);

			boolean isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchisestatusFO1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firstcampaignNamefranstatus + "')]");
			if (isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchisestatusFO1 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my Franchise (Status Driven)Campaign created by Franchise user who has privilege to see private campaign by Franchise owner user(FO1)");
			}
			
			//Verify Private Campaign created by Franchise user who has privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Private (Status Driven)Campaign created by Franchise user who has privilege to see private campaign by Franchise owner user(FO1)");
			campaignCenterPage.searchCampaignByFilter(driver, secondcampaignNamefranstatus);
			
			boolean isCampaignNamePresentFranpaccessibilityprivatestatusFO1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondcampaignNamefranstatus + "')]");
			if (isCampaignNamePresentFranpaccessibilityprivatestatusFO1 == false) {
				fc.utobj().throwsException("was not able to verify Private (Status Driven)Campaign created by Franchise user who has privilege to see private campaign by Franchise owner user(FO1)");
			}
			
			fc.utobj().printTestStep("Navigate to CRM>Campaign Center>email template");
			emailtemplate.clickonemailtamplate(driver);
			fc.utobj().clickElement(driver, crmcenter.locationtemplate);
			//Verify Public to all users template created by Corporate user who has privilege to see private template by itself
			fc.utobj().printTestStep("Verify Public to all users of my franchise template created by franchise user who has privilege to see private template by Franchise owner user(FO1)");
			campaignCenterPage.searchTemplateByFilter(driver, firsttemplateNamefran);

			boolean isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchiseFO1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firsttemplateNamefran + "')]");
			if (isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchiseFO1 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my franchise template created by franchise user who has privilege to see private template by Franchise owner user(FO1)");
			}
			
			//Verify Private template created by franchise user who has privilege to see private template by itself
			fc.utobj().printTestStep("Verify Private template created by franchise user who has privilege to see private template by Franchise owner user(FO1)");
			campaignCenterPage.searchTemplateByFilter(driver, secondtemplateNamefran);
			
			boolean isTemplateNamePresentfranaccessibilityprivateFO1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondtemplateNamefran + "')]");
			if (isTemplateNamePresentfranaccessibilityprivateFO1 == false) {
				fc.utobj().throwsException("was not able to verify private template created by franchise user who has privilege to see private template by Franchise owner user(FO1)");
			}
			
			//Verify Public to all users (Status Driven)template created by Corporate user who has privilege to see private template by itself
			fc.utobj().printTestStep("Verify Public to all users of my franchise (Status Driven)template created by franchise user who has privilege to see private template by Franchise owner user(FO1)");
			campaignCenterPage.searchTemplateByFilter(driver, firsttemplateNamefranstatus);

			boolean isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchisestatusFO1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firsttemplateNamefranstatus + "')]");
			if (isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchisestatusFO1 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my franchise (Status Driven)template created by franchise user who has privilege to see private template by Franchise owner user(FO1)");
			}
			
			//Verify Private template created by franchise user who has privilege to see private template by itself
			fc.utobj().printTestStep("Verify Private (Status Driven)template created by franchise user who has privilege to see private template by Franchise owner user(FO1)");
			campaignCenterPage.searchTemplateByFilter(driver, secondtemplateNamefranstatus);
			
			boolean isTemplateNamePresentfranaccessibilityprivatestatusFO1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondtemplateNamefranstatus + "')]");
			if (isTemplateNamePresentfranaccessibilityprivatestatusFO1 == false) {
				fc.utobj().throwsException("was not able to verify private (Status Driven)template created by franchise user who has privilege to see private template by Franchise owner user(FO1)");
			}
			
			fc.utobj().printTestStep("Navigate to CRM>Campaign Center>campaign template");
			emailtemplate.clickoncampaigntemplate(driver);
			fc.utobj().clickElement(driver, crmcenter.locationCampaign);
			fc.utobj().printTestStep("Verifying Campaigns and templates of Franchise Employee user (FE1) by Franchise owner user(FO1)");
			
			fc.utobj().printTestStep("Verify Public to all users of my Franchise Campaign created by Franchise Employee user(FE1) who has privilege to see private campaign by Franchise owner user(FO1)");
			campaignCenterPage.searchCampaignByFilter(driver, firstcampaignNamefrane);

			boolean isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchiseEFO1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firstcampaignNamefrane + "')]");
			if (isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchiseEFO1 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my Franchise Campaign created by Franchise Employee user(FE1) who has privilege to see private campaign by Franchise owner user(FO1)");
			}
			
			//Verify Private Campaign created by Franchise user who has privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Private Campaign created by Franchise Employee user(FE1) who has privilege to see private campaign by Franchise owner user(FO1)");
			campaignCenterPage.searchCampaignByFilter(driver, secondcampaignNamefrane);
			
			boolean isCampaignNamePresentFranpaccessibilityprivateEFO1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondcampaignNamefrane + "')]");
			if (isCampaignNamePresentFranpaccessibilityprivateEFO1 == false) {
				fc.utobj().throwsException("was not able to verify Private Campaign created by Franchise Employee user(FE1) who has privilege to see private campaign by Franchise owner user(FO1)");
			}
			
			
			//Verify Public to all users of my Franchise (Status Driven)Campaign created by Franchise user who has privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Public to all users of my Franchise (Status Driven)Campaign created by Franchise Employee user(FE1) who has privilege to see private campaign by Franchise owner user(FO1)");
			campaignCenterPage.searchCampaignByFilter(driver, firstcampaignNamefranestatus);

			boolean isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchiseEstatusFO1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firstcampaignNamefranestatus + "')]");
			if (isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchiseEstatusFO1 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my Franchise (Status Driven)Campaign created by Franchise Employee user(FE1) who has privilege to see private campaign by Franchise owner user(FO1)");
			}
			
			//Verify Private Campaign created by Franchise user who has privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Private (Status Driven)Campaign created by Franchise Employee user(FE1) who has privilege to see private campaign by Franchise owner user(FO1)");
			campaignCenterPage.searchCampaignByFilter(driver, secondcampaignNamefranestatus);
			
			boolean isCampaignNamePresentFranpaccessibilityprivateEstatusFO1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondcampaignNamefranestatus + "')]");
			if (isCampaignNamePresentFranpaccessibilityprivateEstatusFO1 == false) {
				fc.utobj().throwsException("was not able to verify Private (Status Driven)Campaign created by Franchise Employee user(FE1) who has privilege to see private campaign by Franchise owner user(FO1)");
			}
		
			fc.utobj().printTestStep("Navigate to CRM>Campaign Center>email template");
			emailtemplate.clickonemailtamplate(driver);
			fc.utobj().clickElement(driver, crmcenter.locationtemplate);
			//Verify Public to all users template created by Franchise Employee user user who has privilege to see private template by itself
			fc.utobj().printTestStep("Verify Public to all users of my franchise template created by franchise Employee user(FE1) who has privilege to see private template by Franchise owner user(FO1)");
			campaignCenterPage.searchTemplateByFilter(driver, firsttemplateNamefrane);

			boolean isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchiseEFO1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firsttemplateNamefrane + "')]");
			if (isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchiseEFO1 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my franchise template created by franchise Employee user(FE1) who has privilege to see private template by Franchise owner user(FO1)");
			}
			
			//Verify Private template created by franchise Employee user who has privilege to see private template by itself
			fc.utobj().printTestStep("Verify Private template created by franchise Employee user(FE1) who has privilege to see private template by Franchise owner user(FO1)");
			campaignCenterPage.searchTemplateByFilter(driver, secondtemplateNamefrane);
			
			boolean isTemplateNamePresentfranaccessibilityprivateEFO1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondtemplateNamefrane + "')]");
			if (isTemplateNamePresentfranaccessibilityprivateEFO1 == false) {
				fc.utobj().throwsException("was not able to verify private template created by franchise Employee user(FE1) who has privilege to see private template by Franchise owner user(FO1)");
			}
			
			
			//Verify Public to all users (Status Driven)template created by Franchise Employee user user who has privilege to see private template by itself
			fc.utobj().printTestStep("Verify Public to all users of my franchise (Status Driven)template created by franchise Employee user(FE1) who has privilege to see private template by Franchise owner user(FO1)");
			campaignCenterPage.searchTemplateByFilter(driver, firsttemplateNamefranestatus);

			boolean isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchiseEstatusFO1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firsttemplateNamefranestatus + "')]");
			if (isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchiseEstatusFO1 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my franchise (Status Driven)template created by franchise Employee user(FE1) who has privilege to see private template by Franchise owner user(FO1)");
			}
			
			//Verify Private template created by franchise Employee user who has privilege to see private template by itself
			fc.utobj().printTestStep("Verify Private (Status Driven)template created by franchise Employee user(FE1) who has privilege to see private template by Franchise owner user(FO1)");
			campaignCenterPage.searchTemplateByFilter(driver, secondtemplateNamefranestatus);
			
			boolean isTemplateNamePresentfranaccessibilityprivateEstatusFO1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondtemplateNamefranestatus + "')]");
			if (isTemplateNamePresentfranaccessibilityprivateEstatusFO1 == false) {
				fc.utobj().throwsException("was not able to verify private (Status Driven)template created by franchise Employee user(FE1) who has privilege to see private template by Franchise owner user(FO1)");
			}
			
			
			fc.utobj().printTestStep("Navigate to CRM>Campaign Center>campaign template");
			emailtemplate.clickoncampaigntemplate(driver);
			fc.utobj().clickElement(driver, crmcenter.locationCampaign);
			fc.utobj().printTestStep("Verifying Campaigns and templates of Franchise Employee user (F02) by Franchise owner user(FO1)");
			
			
			//Verify Public to all users of my Franchise Campaign created by Franchise user who does not have privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Public to all users of my Franchise Campaign created by Franchise user(FO2) who does not have privilege to see private campaign by Franchise owner user(FO1)");
			campaignCenterPage.searchCampaignByFilter(driver, firstcampaignNamefranO2);

			boolean isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchiseO2FO1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firstcampaignNamefranO2 + "')]");
			if (!isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchiseO2FO1 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my Franchise Campaign created by Franchise user(FO2) who does not have privilege to see private campaign by Franchise owner user(FO1)");
			}
			
			//Verify Private Campaign created by Franchise user who has privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Private Campaign created by Franchise user(FO2) who does not have privilege to see private campaign by Franchise owner user(FO1)");
			campaignCenterPage.searchCampaignByFilter(driver, secondcampaignNamefranO2);
			
			boolean isCampaignNamePresentFranpaccessibilityprivateO2FO1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondcampaignNamefranO2 + "')]");
			if (!isCampaignNamePresentFranpaccessibilityprivateO2FO1 == false) {
				fc.utobj().throwsException("was not able to verify Private Campaign created by Franchise user(FO2) who does not have privilege to see private campaign by Franchise owner user(FO1)");
			}
			
			//Verify Public to all users of my Franchise (Status Driven)Campaign created by Franchise user who does not have privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Public to all users of my Franchise (Status Driven)Campaign created by Franchise user(FO2) who does not have privilege to see private campaign by Franchise owner user(FO1)");
			campaignCenterPage.searchCampaignByFilter(driver, firstcampaignNamefranO2status);

			boolean isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchiseO2statusFO1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firstcampaignNamefranO2status + "')]");
			if (!isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchiseO2statusFO1 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my (Status Driven)Franchise Campaign created by Franchise user(FO2) who does not have privilege to see private campaign by Franchise owner user(FO1)");
			}
			
			//Verify Private Campaign created by Franchise user who has privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Private (Status Driven)Campaign created by Franchise user(FO2) who does not have privilege to see private campaign by Franchise owner user(FO1)");
			campaignCenterPage.searchCampaignByFilter(driver, secondcampaignNamefranO2status);
			
			boolean isCampaignNamePresentFranpaccessibilityprivateO2statusFO1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondcampaignNamefranO2status + "')]");
			if (!isCampaignNamePresentFranpaccessibilityprivateO2statusFO1 == false) {
				fc.utobj().throwsException("was not able to verify Private (Status Driven)Campaign created by Franchise user(FO2) who does not have privilege to see private campaign by Franchise owner user(FO1)");
			}
		
			fc.utobj().printTestStep("Navigate to CRM>Campaign Center>email template");
			emailtemplate.clickonemailtamplate(driver);
			fc.utobj().clickElement(driver, crmcenter.locationtemplate);
			//Verify Public to all users template created by Corporate user who has privilege to see private template by itself
			fc.utobj().printTestStep("Verify Public to all users of my franchise template created by franchise user(FO2) who does not have privilege to see private template by Franchise owner user(FO1)");
			campaignCenterPage.searchTemplateByFilter(driver, firsttemplateNamefranO2);

			boolean isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchiseO2FO1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firsttemplateNamefranO2 + "')]");
			if (!isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchiseO2FO1 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my franchise template created by franchise user(FO2) who does not have privilege to see private template by Franchise owner user(FO1)");
			}
			
			//Verify Private template created by franchise user who has privilege to see private template by itself
			fc.utobj().printTestStep("Verify Private template created by franchise user(FO2) who does not have privilege to see private template by Franchise owner user(FO1)");
			campaignCenterPage.searchTemplateByFilter(driver, secondtemplateNamefranO2);
			
			boolean isTemplateNamePresentfranaccessibilityprivateO2FO1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondtemplateNamefranO2 + "')]");
			if (!isTemplateNamePresentfranaccessibilityprivateO2FO1 == false) {
				fc.utobj().throwsException("was not able to verify private template created by franchise user(FO2) who does not have privilege to see private template by Franchise owner user(FO1)");
			}
			
			//Verify Public to all users template created by Corporate user who has privilege to see private template by itself
			fc.utobj().printTestStep("Verify Public to all users of my franchise (Status Driven)template created by franchise user(FO2) who does not have privilege to see private template by Franchise owner user(FO1)");
			campaignCenterPage.searchTemplateByFilter(driver, firsttemplateNamefranO2status);

			boolean isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchiseO2statusFO1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firsttemplateNamefranO2status + "')]");
			if (!isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchiseO2statusFO1 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my franchise (Status Driven)template created by franchise user(FO2) who does not have privilege to see private template by Franchise owner user(FO1)");
			}
			
			//Verify Private template created by franchise user who has privilege to see private template by itself
			fc.utobj().printTestStep("Verify Private (Status Driven)template created by franchise user(FO2) who does not have privilege to see private template by Franchise owner user(FO1)");
			campaignCenterPage.searchTemplateByFilter(driver, secondtemplateNamefranO2status);
			
			boolean isTemplateNamePresentfranaccessibilityprivateO2statusFO1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondtemplateNamefranO2status + "')]");
			if (!isTemplateNamePresentfranaccessibilityprivateO2statusFO1 == false) {
				fc.utobj().throwsException("was not able to verify private (Status Driven)template created by franchise user(FO2) who does not have privilege to see private template by Franchise owner user(FO1)");
			}
			
			fc.utobj().printTestStep("Navigate to CRM>Campaign Center>campaign template");
			emailtemplate.clickoncampaigntemplate(driver);
			fc.utobj().clickElement(driver, crmcenter.locationCampaign);
			fc.utobj().printTestStep("Verifying Campaigns and templates of Franchise Employee user (FE2) by Franchise owner user(FO1)");
			
			//Verify Public to all users of my Franchise Campaign created by Franchise user who has privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Public to all users of my Franchise Campaign created by Franchise Employee user(FE2) who does not have privilege to see private campaign by Franchise owner user(FO1)");
			campaignCenterPage.searchCampaignByFilter(driver, firstcampaignNamefrane2);

			boolean isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchiseE2FO1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firstcampaignNamefrane2 + "')]");
			if (!isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchiseE2FO1 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my Franchise Campaign created by Franchise Employee user(FE2) who does not have privilege to see private campaign by Franchise owner user(FO1)");
			}
			
			//Verify Private Campaign created by Franchise user who has privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Private Campaign created by Franchise Employee user(FE2) who does not have privilege to see private campaign by Franchise owner user(FO1)");
			campaignCenterPage.searchCampaignByFilter(driver, secondcampaignNamefrane2);
			
			boolean isCampaignNamePresentFranpaccessibilityprivateE2FO1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondcampaignNamefrane2 + "')]");
			if (!isCampaignNamePresentFranpaccessibilityprivateE2FO1 == false) {
				fc.utobj().throwsException("was not able to verify Private Campaign created by Franchise Employee user(FE2) who does not have privilege to see private campaign by Franchise owner user(FO1)");
			}
			
			
			//Verify Public to all users of my Franchise Campaign created by Franchise user who has privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Public to all users of my Franchise (Status Driven)Campaign created by Franchise Employee user(FE2) who does not have privilege to see private campaign by Franchise owner user(FO1)");
			campaignCenterPage.searchCampaignByFilter(driver, firstcampaignNamefrane2status);

			boolean isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchiseE2statusFO1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firstcampaignNamefrane2status + "')]");
			if (!isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchiseE2statusFO1 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my Franchise (Status Driven)Campaign created by Franchise Employee user(FE2) who does not have privilege to see private campaign by Franchise owner user(FO1)");
			}
			
			//Verify Private Campaign created by Franchise user who has privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Private (Status Driven)Campaign created by Franchise Employee user(FE2) who does not have privilege to see private campaign by Franchise owner user(FO1)");
			campaignCenterPage.searchCampaignByFilter(driver, secondcampaignNamefrane2status);
			
			boolean isCampaignNamePresentFranpaccessibilityprivateE2statusFO1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondcampaignNamefrane2status + "')]");
			if (!isCampaignNamePresentFranpaccessibilityprivateE2statusFO1 == false) {
				fc.utobj().throwsException("was not able to verify Private (Status Driven)Campaign created by Franchise Employee user(FE2) who does not have privilege to see private campaign by Franchise owner user(FO1)");
			}
			
		
			fc.utobj().printTestStep("Navigate to CRM>Campaign Center>email template");
			emailtemplate.clickonemailtamplate(driver);
			fc.utobj().clickElement(driver, crmcenter.locationtemplate);
			//Verify Public to all users template created by Franchise Employee user user who has privilege to see private template by itself
			fc.utobj().printTestStep("Verify Public to all users of my franchise template created by franchise Employee user(FE2) who does not have privilege to see private template by Franchise owner user(FO1)");
			campaignCenterPage.searchTemplateByFilter(driver, firsttemplateNamefrane2);

			boolean isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchiseE2FO1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firsttemplateNamefrane2 + "')]");
			if (!isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchiseE2FO1 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my franchise template created by franchise Employee user(FE2) who does not have privilege to see private template by Franchise owner user(FO1)");
			}
			
			//Verify Private template created by franchise Employee user who has privilege to see private template by itself
			fc.utobj().printTestStep("Verify Private template created by franchise Employee user(FE2) who does not have privilege to see private template by Franchise owner user(FO1)");
			campaignCenterPage.searchTemplateByFilter(driver, secondtemplateNamefrane2);
			
			boolean isTemplateNamePresentfranaccessibilityprivateE2FO1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondtemplateNamefrane2 + "')]");
			if (!isTemplateNamePresentfranaccessibilityprivateE2FO1 == false) {
				fc.utobj().throwsException("was not able to verify private template created by franchise Employee user(FE2) who does not have privilege to see private template by Franchise owner user(FO1)");
			}
			
			//Verify Public to all users template created by Franchise Employee user user who has privilege to see private template by itself
			fc.utobj().printTestStep("Verify Public to all users of my franchise (Status Driven)template created by franchise Employee user(FE2) who does not have privilege to see private template by Franchise owner user(FO1)");
			campaignCenterPage.searchTemplateByFilter(driver, firsttemplateNamefrane2status);

			boolean isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchiseE2statusFO1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firsttemplateNamefrane2status + "')]");
			if (!isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchiseE2statusFO1 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my franchise(Status Driven) template created by franchise Employee user(FE2) who does not have privilege to see private template by Franchise owner user(FO1)");
			}
			
			//Verify Private template created by franchise Employee user who has privilege to see private template by itself
			fc.utobj().printTestStep("Verify Private (Status Driven)template created by franchise Employee user(FE2) who does not have privilege to see private template by Franchise owner user(FO1)");
			campaignCenterPage.searchTemplateByFilter(driver, secondtemplateNamefrane2status);
			
			boolean isTemplateNamePresentfranaccessibilityprivateE2statusFO1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondtemplateNamefrane2status + "')]");
			if (!isTemplateNamePresentfranaccessibilityprivateE2statusFO1 == false) {
				fc.utobj().throwsException("was not able to verify private (Status Driven)template created by franchise Employee user(FE2) who does not have privilege to see private template by Franchise owner user(FO1)");
			}
		
			
			fc.utobj().printTestStep("Navigate to CRM>Campaign Center>campaign template");
			emailtemplate.clickoncampaigntemplate(driver);
			fc.utobj().clickElement(driver, crmcenter.locationCampaign);
			fc.utobj().printTestStep("Verifying Campaigns and templates of Franchise Mu user (V1) by Franchise owner user(FO1)");
			
			fc.utobj().printTestStep("Verify Public to all users of my Franchise Campaign created by Franchise MU user(V1) who has privilege to see private campaign by Franchise owner user(FO1)");
			campaignCenterPage.searchCampaignByFilter(driver, firstcampaignNamefranmu);

			boolean isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchisemuFO1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firstcampaignNamefranmu + "')]");
			if (!isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchisemuFO1 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my Franchise Campaign created by Franchise MU user(V1) who has privilege to see private campaign by Franchise owner user(FO1)");
			}
			
			//Verify Private Campaign created by Franchise user who has privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Private Campaign created by Franchise MU user(V1) who has privilege to see private campaign by Franchise owner user(FO1)");
			campaignCenterPage.searchCampaignByFilter(driver, secondcampaignNamefranmu);
			
			boolean isCampaignNamePresentFranpaccessibilityprivatemuFO1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondcampaignNamefranmu + "')]");
			if (!isCampaignNamePresentFranpaccessibilityprivatemuFO1 == false) {
				fc.utobj().throwsException("was not able to verify Private Campaign created by Franchise MU user(V1) who has privilege to see private campaign by Franchise owner user(FO1)");
			}
			
			
			//Verify Public to all users of my Franchise (Status Driven)Campaign created by Franchise user who has privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Public to all users of my Franchise (Status Driven)Campaign created by Franchise MU user(V1) who has privilege to see private campaign by Franchise owner user(FO1)");
			campaignCenterPage.searchCampaignByFilter(driver, firstcampaignNamefranmustatus);

			boolean isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchisemustatusFO1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firstcampaignNamefranmustatus + "')]");
			if (!isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchisemustatusFO1 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my Franchise (Status Driven)Campaign created by Franchise MU user(V1) who has privilege to see private campaign by Franchise owner user(FO1)");
			}
			
			//Verify Private Campaign created by Franchise user who has privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Private (Status Driven)Campaign created by Franchise MU user(V1) who has privilege to see private campaign by Franchise owner user(FO1)");
			campaignCenterPage.searchCampaignByFilter(driver, secondcampaignNamefranmustatus);
			
			boolean isCampaignNamePresentFranpaccessibilityprivatemustatusFO1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondcampaignNamefranmustatus + "')]");
			if (!isCampaignNamePresentFranpaccessibilityprivatemustatusFO1 == false) {
				fc.utobj().throwsException("was not able to verify Private (Status Driven)Campaign created by Franchise MU user(V1) who has privilege to see private campaign by Franchise owner user(FO1)");
			}
			
			fc.utobj().printTestStep("Navigate to CRM>Campaign Center>email template");
			emailtemplate.clickonemailtamplate(driver);
			fc.utobj().clickElement(driver, crmcenter.locationtemplate);
			//Verify Public to all users template created by Corporate user who has privilege to see private template by itself
			fc.utobj().printTestStep("Verify Public to all users of my franchise template created by franchise MU user(V1) who has privilege to see private template by Franchise owner user(FO1)");
			campaignCenterPage.searchTemplateByFilter(driver, firsttemplateNamefranmu);

			boolean isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchisemuFO1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firsttemplateNamefranmu + "')]");
			if (!isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchisemuFO1 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my franchise template created by franchise MU user(V1) who has privilege to see private template by Franchise owner user(FO1)");
			}
			
			//Verify Private template created by franchise user who has privilege to see private template by itself
			fc.utobj().printTestStep("Verify Private template created by franchise MU user(V1) who has privilege to see private template by Franchise owner user(FO1)");
			campaignCenterPage.searchTemplateByFilter(driver, secondtemplateNamefranmu);
			
			boolean isTemplateNamePresentfranaccessibilityprivatemuFO1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondtemplateNamefranmu + "')]");
			if (!isTemplateNamePresentfranaccessibilityprivatemuFO1 == false) {
				fc.utobj().throwsException("was not able to verify private template created by franchise MU user(V1) who has privilege to see private template by Franchise owner user(FO1)");
			}
			
			//Verify Public to all users template created by Corporate user who has privilege to see private template by itself
			fc.utobj().printTestStep("Verify Public to all users of my franchise (Status Driven)template created by franchise MU user(V1) who has privilege to see private template by Franchise owner user(FO1)");
			campaignCenterPage.searchTemplateByFilter(driver, firsttemplateNamefranmustatus);

			boolean isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchisemustatusFO1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firsttemplateNamefranmustatus + "')]");
			if (!isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchisemustatusFO1 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my franchise (Status Driven)template created by franchise MU user(V1) who has privilege to see private template by Franchise owner user(FO1)");
			}
			
			//Verify Private template created by franchise user who has privilege to see private template by itself
			fc.utobj().printTestStep("Verify Private (Status Driven)template created by franchise MU user(V1) who has privilege to see private template by Franchise owner user(FO1)");
			campaignCenterPage.searchTemplateByFilter(driver, secondtemplateNamefranmustatus);
			
			boolean isTemplateNamePresentfranaccessibilityprivatemustatusFO1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondtemplateNamefranmustatus + "')]");
			if (!isTemplateNamePresentfranaccessibilityprivatemustatusFO1 == false) {
				fc.utobj().throwsException("was not able to verify private (Status Driven)template created by franchise MU user(V1) who has privilege to see private template by Franchise owner user(FO1)");
			}
			
			
			
			
			fc.utobj().printTestStep("Navigate to CRM>Campaign Center>campaign template");
			emailtemplate.clickoncampaigntemplate(driver);
			fc.utobj().clickElement(driver, crmcenter.locationCampaign);
			fc.utobj().printTestStep("Verifying Campaigns and templates of Franchise Employee user (V2) by Franchise owner user(FO1)");
			fc.utobj().printTestStep("Verify Public to all users of my Franchise Campaign created by Franchise Employee user(V2) who does not have privilege to see private campaign by Franchise owner user(FO1)");
			campaignCenterPage.searchCampaignByFilter(driver, firstcampaignNamefranmue2);

			boolean isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchisemue2FO1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firstcampaignNamefranmue2 + "')]");
			if (!isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchisemue2FO1 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my Franchise Campaign created by Franchise Employee user(V2) who does not have privilege to see private campaign by Franchise owner user(FO1)");
			}
			
			//Verify Private Campaign created by Franchise user who has privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Private Campaign created by Franchise Employee user(V2) who does not have privilege to see private campaign by Franchise owner user(FO1)");
			campaignCenterPage.searchCampaignByFilter(driver, secondcampaignNamefranmue2);
			
			boolean isCampaignNamePresentFranpaccessibilityprivatemue2FO1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondcampaignNamefranmue2 + "')]");
			if (!isCampaignNamePresentFranpaccessibilityprivatemue2FO1 == false) {
				fc.utobj().throwsException("was not able to verify Private Campaign created by Franchise Employee user(V2) who does not have privilege to see private campaign by Franchise owner user(FO1)");
			}
			
			
			//Verify Public to all users of my Franchise Campaign created by Franchise Employee user(V2) who has privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Public to all users of my Franchise (Status Driven)Campaign created by Franchise Employee user(V2) who does not have privilege to see private campaign by Franchise owner user(FO1)");
			campaignCenterPage.searchCampaignByFilter(driver, firstcampaignNamefranmue2status);

			boolean isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchisemue2statusFO1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firstcampaignNamefranmue2status + "')]");
			if (!isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchisemue2statusFO1 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my Franchise (Status Driven)Campaign created by Franchise Employee user(V2) who does not have privilege to see private campaign by Franchise owner user(FO1)");
			}
			
			//Verify Private Campaign created by Franchise user who has privilege to see private campaign by itself
			fc.utobj().printTestStep("Verify Private (Status Driven)Campaign created by Franchise Employee user(V2) who does not have privilege to see private campaign by Franchise owner user(FO1)");
			campaignCenterPage.searchCampaignByFilter(driver, secondcampaignNamefranmue2status);
			
			boolean isCampaignNamePresentFranpaccessibilityprivatemue2statusFO1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondcampaignNamefranmue2status + "')]");
			if (!isCampaignNamePresentFranpaccessibilityprivatemue2statusFO1 == false) {
				fc.utobj().throwsException("was not able to verify Private (Status Driven)Campaign created by Franchise Employee user(V2) who does not have privilege to see private campaign by Franchise owner user(FO1)");
			}
			
			fc.utobj().printTestStep("Navigate to CRM>Campaign Center>email template");
			emailtemplate.clickonemailtamplate(driver);
			fc.utobj().clickElement(driver, crmcenter.locationtemplate);
			//Verify Public to all users template created by Franchise user who has privilege to see private template by itself
			fc.utobj().printTestStep("Verify Public to all users of my franchise template created by franchise Employee user(V2) who does not have privilege to see private template by Franchise owner user(FO1)");
			campaignCenterPage.searchTemplateByFilter(driver, firsttemplateNamefranmue2);

			boolean isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchisemue2FO1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firsttemplateNamefranmue2 + "')]");
			if (!isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchisemue2FO1 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my franchise template created by franchise Employee user(V2) who has privilege to see private template by Franchise owner user(FO1)");
			}
			
			//Verify Private template created by franchise user who has privilege to see private template by itself
			fc.utobj().printTestStep("Verify Private template created by franchise Employee user(V2) who has privilege to see private template by Franchise owner user(FO1)");
			campaignCenterPage.searchTemplateByFilter(driver, secondtemplateNamefranmue2);
			
			boolean isTemplateNamePresentfranaccessibilityprivatemue2FO1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondtemplateNamefranmue2 + "')]");
			if (!isTemplateNamePresentfranaccessibilityprivatemue2FO1 == false) {
				fc.utobj().throwsException("was not able to verify private template created by franchise Employee user(V2) who has privilege to see private template by Franchise owner user(FO1)");
			}
			
			//Verify Public to all users template created by Franchise user who has privilege to see private template by itself
			fc.utobj().printTestStep("Verify Public to all users of my franchise (Status Driven)template created by franchise Employee user(V2) who does not have privilege to see private template by Franchise owner user(FO1)");
			campaignCenterPage.searchTemplateByFilter(driver, firsttemplateNamefranmue2status);

			boolean isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchisemue2statusFO1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + firsttemplateNamefranmue2status + "')]");
			if (!isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchisemue2statusFO1 == false) {
				fc.utobj().throwsException("was not able to verify Public to all users of my franchise (Status Driven)template created by franchise Employee user(V2) who has privilege to see private template by Franchise owner user(FO1)");
			}
			
			//Verify Private template created by franchise user who has privilege to see private template by itself
			fc.utobj().printTestStep("Verify Private (Status Driven)template created by franchise Employee user(V2) who has privilege to see private template by Franchise owner user(FO1)");
			campaignCenterPage.searchTemplateByFilter(driver, secondtemplateNamefranmue2status);
			
			boolean isTemplateNamePresentfranaccessibilityprivatemue2statusFO1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + secondtemplateNamefranmue2status + "')]");
			if (!isTemplateNamePresentfranaccessibilityprivatemue2statusFO1 == false) {
				fc.utobj().throwsException("was not able to verify private (Status Driven)template created by franchise Employee user(V2) who has privilege to see private template by Franchise owner user(FO1)");
			}
			
			if(isCampaignNamePresentCorpaccessibilitypublictoalluserscorpFO1 && !isCampaignNamePresentCorpaccessibilitypublictoallcorporateuserscorpFO1 && !isCampaignNamePresentCorpaccessibilityprivatecorpFO1 
					&& isCampaignNamePresentCorpaccessibilitypublictoalluserscorpstatusFO1 && !isCampaignNamePresentCorpaccessibilitypublictoallcorporateuserscorpstatusFO1 && !isCampaignNamePresentCorpaccessibilityprivatecorpstatusFO1 
					&& isTemplateNamePresentCorpaccessibilitypublictoalluserscorpFO1 && !isTemplateNamePresentCorpaccessibilitypublictoallcorporateuserscorpFO1 && !isTemplateNamePresentCorpaccessibilityprivatecorpFO1 
					&& isTemplateNamePresentCorpaccessibilitypublictoalluserscorpstatusFO1 && !isTemplateNamePresentCorpaccessibilitypublictoallcorporateuserscorpstatusFO1 && !isTemplateNamePresentCorpaccessibilityprivatecorpstatusFO1 
					&& isCampaignNamePresentRegpaccessibilitypublictoallusersofmyregionFO1 && !isCampaignNamePresentRegpaccessibilityprivateFO1 && isCampaignNamePresentRegpaccessibilitypublictoallusersofmyregionstatusFO1 
					&& !isCampaignNamePresentRegpaccessibilityprivatestatusFO1 && isTemplateNamePresentRegaccessibilitypublictoallusersofmyregionFO1 && !isTemplateNamePresentRegaccessibilityprivateFO1 
					&& isTemplateNamePresentRegaccessibilitypublictoallusersofmyregionstatusFO1 && !isTemplateNamePresentRegaccessibilityprivatestatusFO1 && isCampaignNamePresentCorpaccessibilitypublictoallusersFO1 
					&& !isCampaignNamePresentCorpaccessibilitypublictoallcorporateusersFO1 && !isCampaignNamePresentCorpaccessibilityprivateFO1 && isCampaignNamePresentCorpaccessibilitypublictoallusersstatusFO1 
					&& !isCampaignNamePresentCorpaccessibilitypublictoallcorporateusersstatusFO1 && !isCampaignNamePresentCorpaccessibilityprivatestatusFO1 && isTemplateNamePresentCorpaccessibilitypublictoallusersFO1 
					&& !isTemplateNamePresentCorpaccessibilitypublictoallcorporateusersFO1 && !isTemplateNamePresentCorpaccessibilityprivateFO1 && isTemplateNamePresentCorpaccessibilitypublictoallusersstatusFO1 
					&& !isTemplateNamePresentCorpaccessibilitypublictoallcorporateusersstatusFO1 && !isTemplateNamePresentCorpaccessibilityprivatestatusFO1 && isCampaignNamePresentRegpaccessibilitypublictoallusersofmyregion1FO1
					&& !isCampaignNamePresentRegpaccessibilityprivate1FO1 && isCampaignNamePresentRegpaccessibilitypublictoallusersofmyregion1statusFO1 && !isCampaignNamePresentRegpaccessibilityprivate1statusFO1
					&& isTemplateNamePresentRegaccessibilitypublictoallusersofmyregion1FO1 && !isTemplateNamePresentRegaccessibilityprivate1FO1 && isTemplateNamePresentRegaccessibilitypublictoallusersofmyregion1statusFO1 
					&& !isTemplateNamePresentRegaccessibilityprivate1statusFO1 && isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchiseFO1 && isCampaignNamePresentFranpaccessibilityprivateFO1 
					&& isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchisestatusFO1 && isCampaignNamePresentFranpaccessibilityprivatestatusFO1 && isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchiseFO1 
					&& isTemplateNamePresentfranaccessibilityprivateFO1 && isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchisestatusFO1 && isTemplateNamePresentfranaccessibilityprivatestatusFO1 
					&& isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchiseEFO1 && isCampaignNamePresentFranpaccessibilityprivateEFO1 && isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchiseEstatusFO1 
					&& isCampaignNamePresentFranpaccessibilityprivateEstatusFO1 && isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchiseEFO1 && isTemplateNamePresentfranaccessibilityprivateEFO1 
					&& isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchiseEstatusFO1 && isTemplateNamePresentfranaccessibilityprivateEstatusFO1 && !isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchiseO2FO1 
					&& !isCampaignNamePresentFranpaccessibilityprivateO2FO1 && !isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchiseO2statusFO1 && !isCampaignNamePresentFranpaccessibilityprivateO2statusFO1 
					&& !isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchiseO2FO1 && !isTemplateNamePresentfranaccessibilityprivateO2FO1 && !isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchiseO2statusFO1 
					&& !isTemplateNamePresentfranaccessibilityprivateO2statusFO1 && !isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchiseE2FO1 && !isCampaignNamePresentFranpaccessibilityprivateE2FO1 
					&& !isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchiseE2statusFO1 && !isCampaignNamePresentFranpaccessibilityprivateE2statusFO1 && !isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchiseE2FO1
					&& !isTemplateNamePresentfranaccessibilityprivateE2FO1 && !isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchiseE2statusFO1 && !isTemplateNamePresentfranaccessibilityprivateE2statusFO1 
					&& !isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchisemuFO1 && !isCampaignNamePresentFranpaccessibilityprivatemuFO1 && !isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchisemustatusFO1 
					&& !isCampaignNamePresentFranpaccessibilityprivatemustatusFO1 && !isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchisemuFO1 && !isTemplateNamePresentfranaccessibilityprivatemuFO1 
					&& !isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchisemustatusFO1 && !isTemplateNamePresentfranaccessibilityprivatemustatusFO1 && !isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchisemue2FO1 
					&& !isCampaignNamePresentFranpaccessibilityprivatemue2FO1 && !isCampaignNamePresentFranpaccessibilitypublictoallusersofmyfranchisemue2statusFO1 && !isCampaignNamePresentFranpaccessibilityprivatemue2statusFO1 
					&& !isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchisemue2FO1 && !isTemplateNamePresentfranaccessibilityprivatemue2FO1 && !isTemplateNamePresentfranaccessibilitypublictoallusersofmyfranchisemue2statusFO1
					&& !isTemplateNamePresentfranaccessibilityprivatemue2statusFO1) {
				fc.utobj().printTestStep("able to verify all types of campaigns and templates created by corporate/rgional/franchise users when loging with Franchise Owner user(FO1) who has priviledge to see private campaign");
			}
			else {
				fc.utobj().throwsException("was not able to verify all types of campaigns and templates created by corporate/rgional/franchise users when loging with Franchise Owner user(FO1) who has priviledge to see private campaign");
			}
				
			
			fc.utobj().printTestStep("Done");
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
			fc.utobj().printTestStep("LogOut");
		}
		catch(Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}
	
}
