package com.builds.test.crm;

import org.openqa.selenium.WebDriver;

import com.builds.uimaps.crm.CRM_LeadinfopageUI;
import com.builds.utilities.FranconnectUtil;

public class CRM_leadInfoPageOptions {
	FranconnectUtil fc = new FranconnectUtil();
	
    public void modifyLeadOnInfopage(WebDriver driver) throws Exception
	{
    	CRM_LeadinfopageUI mod = new CRM_LeadinfopageUI(driver);
    	fc.utobj().printTestStep("click on modify lead button");
		fc.utobj().clickElement(driver, mod.Modifybtn);
	}

	public void logtaskOnInfoPage(WebDriver driver) throws Exception
	{
		CRM_LeadinfopageUI mod = new CRM_LeadinfopageUI(driver);
		fc.utobj().printTestStep("click on log a task button");
		fc.utobj().clickElement(driver, mod.LogaTaskbtn);
	}
	public void SendEmailOnInfoPage(WebDriver driver) throws Exception
	{
		CRM_LeadinfopageUI mod = new CRM_LeadinfopageUI(driver);
		fc.utobj().printTestStep("click on Send Email button");
		fc.utobj().clickElement(driver, mod.SendEmail);
	}
	
	public void SendSMSOnInfoPage(WebDriver driver) throws Exception
	{
		CRM_LeadinfopageUI mod = new CRM_LeadinfopageUI(driver);
		fc.utobj().printTestStep("click on send SMS button");
		fc.utobj().clickElement(driver, mod.SendSMS);
	}
	
	public void NextbtnOnInfoPage(WebDriver driver) throws Exception
	{
		CRM_LeadinfopageUI mod = new CRM_LeadinfopageUI(driver);
		fc.utobj().printTestStep("click on Next button");
		fc.utobj().clickElement(driver, mod.Nextbutton);
	}
	
	public void PrevbtnOnInfoPage(WebDriver driver) throws Exception
	{
		CRM_LeadinfopageUI mod = new CRM_LeadinfopageUI(driver);
		fc.utobj().printTestStep("click on Previous button");
		fc.utobj().clickElement(driver, mod.Previousbutton);
	}
	
	public void changestatusOnInfoPage(WebDriver driver) throws Exception
	{
		CRM_LeadinfopageUI mod = new CRM_LeadinfopageUI(driver);
		fc.utobj().printTestStep("click on change Status button");
		fc.utobj().clickElement(driver, mod.changestatusbutton);
	}
	
	public void convertOnInfoPage(WebDriver driver) throws Exception
	{
		CRM_LeadinfopageUI mod = new CRM_LeadinfopageUI(driver);
		fc.utobj().printTestStep("click on Convert button");
		fc.utobj().clickElement(driver, mod.convertLead);
	}
	
	public void assignOnInfoPage(WebDriver driver) throws Exception
	{
		CRM_LeadinfopageUI mod = new CRM_LeadinfopageUI(driver);
		fc.utobj().printTestStep("click on Assign button");
		fc.utobj().clickElement(driver, mod.Assignbtn);
	}
	public void DetailhistoryOnInfoPage(WebDriver driver) throws Exception
	{
		CRM_LeadinfopageUI mod = new CRM_LeadinfopageUI(driver);
		fc.utobj().printTestStep("click on Details History button");
		fc.utobj().clickElement(driver, mod.DetailedHistory);
	}
	public void AssociatecampOnInfoPage(WebDriver driver) throws Exception
	{
		CRM_LeadinfopageUI mod = new CRM_LeadinfopageUI(driver);
		fc.utobj().printTestStep("click on Associate With Campaign button");
		fc.utobj().clickElement(driver, mod.AssociateWithCampaign);
	}
	
	public void logtaskbottomOnInfoPage(WebDriver driver) throws Exception
	{
		CRM_LeadinfopageUI mod = new CRM_LeadinfopageUI(driver);
		fc.utobj().printTestStep("click on log a task bottom button");
		fc.utobj().moveToElement(driver, mod.LogaTaskbottom);  
		fc.utobj().clickElement(driver,mod.LogaTaskbottom);
	}
	
	public void eportExcelOnInfoPage(WebDriver driver) throws Exception
	{
		CRM_LeadinfopageUI mod = new CRM_LeadinfopageUI(driver);
		fc.utobj().printTestStep("click on export to excel button");
		fc.utobj().moveToElement(driver, mod.ExportAsExcel); 
		fc.utobj().clickElement(driver, mod.ExportAsExcel);
	}
	
	public void printConcentOnInfoPage(WebDriver driver) throws Exception
	{
		CRM_LeadinfopageUI mod = new CRM_LeadinfopageUI(driver);
		fc.utobj().moveToElement(driver, mod.PrintConcentbtn);
		fc.utobj().clickElement(driver, mod.PrintConcentbtn);
	}
	
	public void ownerChangeOnInfoPage(WebDriver driver) throws Exception
	{
		CRM_LeadinfopageUI mod = new CRM_LeadinfopageUI(driver);
		fc.utobj().printTestStep("click on Owner change button");
		fc.utobj().moveToElement(driver, mod.changeOwnerbottom);
		fc.utobj().clickElement(driver, mod.changeOwnerbottom);
	}
	
	public void addToGroupOnInfoPage(WebDriver driver) throws Exception
	{
		CRM_LeadinfopageUI mod = new CRM_LeadinfopageUI(driver);
		fc.utobj().clickElement(driver, mod.addToGroupbtn);
	}
	
	public void addToNewGroupbtnOnIframe(WebDriver driver) throws Exception
	{
		CRM_LeadinfopageUI mod = new CRM_LeadinfopageUI(driver);
		fc.utobj().printTestStep("click on Add group frame and click add new group button");
		fc.commonMethods().switch_cboxIframe_frameId(driver);
		fc.utobj().clickElement(driver, mod.AddToNewGroup);
	}
	
	public void campaignbottomOnInfoPage(WebDriver driver) throws Exception
	{
		CRM_LeadinfopageUI mod = new CRM_LeadinfopageUI(driver);
		fc.utobj().printTestStep("click on associate with campaign button");
		fc.utobj().clickElement(driver, mod.AssociateWithRegularCampaignButton);
	}
	
	public void printbottomOnInfoPage(WebDriver driver) throws Exception
	{
		CRM_LeadinfopageUI mod = new CRM_LeadinfopageUI(driver);
		fc.utobj().printTestStep("click on print button");
		fc.utobj().clickElement(driver, mod.printbtnBottom);
	}
	
	public void logACallomOnInfoPage(WebDriver driver) throws Exception
	{
		CRM_LeadinfopageUI mod = new CRM_LeadinfopageUI(driver);
		fc.utobj().printTestStep("click on log a call button");
		fc.utobj().clickElement(driver, mod.LogAcall);
	}
	
	public void remarkOnInfoPage(WebDriver driver) throws Exception
	{
		CRM_LeadinfopageUI mod = new CRM_LeadinfopageUI(driver);
		fc.utobj().printTestStep("click on Add remark button");
		fc.utobj().clickElement(driver, mod.Addremark);
	}
	
	//Email Tab on lead info page
	public void Emails_TabOnInfoPage(WebDriver driver) throws Exception
	{
		CRM_LeadinfopageUI mod = new CRM_LeadinfopageUI(driver);
		fc.utobj().printTestStep("click on Email Tab button");
		fc.utobj().clickElement(driver, mod.Emails_tab);
	}
	
	public void Backbtn_Emails_Tab(WebDriver driver) throws Exception
	{
		CRM_LeadinfopageUI mod = new CRM_LeadinfopageUI(driver);
		fc.utobj().printTestStep("click on back button on Email Tab ");
		fc.utobj().clickElement(driver, mod.Backbtn_Emails_tab);
	}
	
	public void SendEmail_Emails_Tab(WebDriver driver) throws Exception
	{
		CRM_LeadinfopageUI mod = new CRM_LeadinfopageUI(driver);
		fc.utobj().printTestStep("click on Send Email button on Email Tab ");
		fc.utobj().clickElement(driver, mod.sendEmail_Emails_tab);
	}
	
	public void Reply_Emails_Tab(WebDriver driver) throws Exception
	{
		CRM_LeadinfopageUI mod = new CRM_LeadinfopageUI(driver);
		fc.utobj().printTestStep("click on reply button on Email Tab ");
		fc.utobj().clickElement(driver, mod.Reply_Emails_tab);
	}
	
	public void Searchbtn_Emails_Tab(WebDriver driver) throws Exception
	{
		CRM_LeadinfopageUI mod = new CRM_LeadinfopageUI(driver);
		fc.utobj().printTestStep("click on search button on Email Tab ");
		fc.utobj().clickElement(driver, mod.Searchbtn_Emails_tab);
	}
	
	//SMS tab on lead info page
	public void SMS_TabOnInfoPage(WebDriver driver) throws Exception
	{
		
	}
	
	public void Backbtn_SMS_Tab(WebDriver driver) throws Exception 
	{
		CRM_LeadinfopageUI mod = new CRM_LeadinfopageUI(driver);
		fc.utobj().printTestStep("click on back button of SMS tab");
		fc.utobj().clickElement(driver, mod.BackBtn_SMSTab);
		
	}
	
	public void SendSMSbtn_SMS_Tab(WebDriver driver) throws Exception 
	{
		CRM_LeadinfopageUI mod = new CRM_LeadinfopageUI(driver);
		fc.utobj().printTestStep("click on back button of SMS tab");
		fc.utobj().clickElement(driver, mod.Send_SMSTab);
		
	}
	
	//More action menu
	public void MoreAction(WebDriver driver) throws Exception
	{
		CRM_LeadinfopageUI mod = new CRM_LeadinfopageUI(driver);
		fc.utobj().printTestStep("click on More Action button");
		fc.utobj().clickElement(driver, mod.MoreActionMenu);
	}
	
	public void MoreAction_changeowner(WebDriver driver) throws Exception
	{
		CRM_LeadinfopageUI mod = new CRM_LeadinfopageUI(driver);
		//MoreAction(driver);
		fc.utobj().printTestStep("click on More Action change owner button");
		fc.utobj().clickElement(driver, mod.MoreActionMenu_changeOwner);
	}
	
	public void MoreAction_AddtoGroup(WebDriver driver) throws Exception
	{
		CRM_LeadinfopageUI mod = new CRM_LeadinfopageUI(driver);
		MoreAction(driver);
		fc.utobj().printTestStep("click on More Action change owner button");
		fc.utobj().clickElement(driver, mod.MoreActionMenu_AddtoGroup);
	}
	
	public void MoreAction_ArchivedLead(WebDriver driver) throws Exception
	{
		CRM_LeadinfopageUI mod = new CRM_LeadinfopageUI(driver);
		MoreAction(driver);
		fc.utobj().printTestStep("click on More Action change owner button");
		fc.utobj().clickElement(driver, mod.MoreActionMenu_ArchivedLead);
	}
	public void MoreAction_Associatecampaign(WebDriver driver) throws Exception
	{
		CRM_LeadinfopageUI mod = new CRM_LeadinfopageUI(driver);
		MoreAction(driver);
		fc.utobj().printTestStep("click on More Action change owner button");
		fc.utobj().clickElement(driver, mod.MoreActionMenu_Associatewithcampaign);
	}
	public void MoreAction_DeleteLead(WebDriver driver) throws Exception
	{
		CRM_LeadinfopageUI mod = new CRM_LeadinfopageUI(driver);
		//MoreAction(driver);
		fc.utobj().printTestStep("click on More Action change owner button");
		fc.utobj().selectMoreActionMenuItemsWithTagA(driver, "Delete Lead");;
	}
	
	public void MoreAction_DetailedHistory(WebDriver driver) throws Exception
	{
		CRM_LeadinfopageUI mod = new CRM_LeadinfopageUI(driver);
		MoreAction(driver);
		fc.utobj().printTestStep("click on More Action change owner button");
		fc.utobj().clickElement(driver, mod.MoreActionMenu_DetailedHistory);
	}
	
	
	
}
