package com.builds.test.crm;

import org.openqa.selenium.WebDriver;

import com.builds.uimaps.crm.LeadsummarypageUI;
import com.builds.utilities.FranconnectUtil;

public class LeadSummaryPage {
	FranconnectUtil fc = new FranconnectUtil();
	
	public Addleadpage clickAddLead(WebDriver driver) throws Exception
	{
		fc.utobj().printTestStep("Click on Add Lead");
		
		LeadsummarypageUI ui = new LeadsummarypageUI(driver);
		fc.utobj().clickElement(driver, ui.addNew);
		fc.utobj().clickElement(driver, ui.lead);
		
		return new Addleadpage();
		
	}

}

