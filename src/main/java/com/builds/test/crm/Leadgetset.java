package com.builds.test.crm;

public class Leadgetset {
	
	private String title;
	private String firstname;
	private String lastname;
	private String primarycontact;
	private String compnayname;
	private String assign;
	private String owner;
	private String address;
	private String city;
	private String country;
	private String zipcode;
	private String state;
	private String phonenumber;
	private String extension;
	private String fax;
	private String mobile;
	private String email;
	private String alternarteemail;
	private String suffix;
	private String jobtitle;
	private String Birthday;
	private String Anniversary;
	private String Rating;
	private String source;
	private String source1;
	private String contactMedium;
	private String Besttime;
	private String groupid;
	private String comment;
	private String submit;
	private String Web_Title;
	private String Web_firstname;
	private String Web_lastname;
	private String Web_Primarycontact;
	private String Web_companyname;
	private String Webservice_AssignTo;
	private String WebService_LeadOwner;
	private String WebService_areaid;
	private String WebService_franchiseNo;
	private String Webservice_divisionID;
	private String Web_Adress;
	private String Web_city;
	private String Web_country;
	private String Web_State;
	private String Web_zipcode;
	private String Web_phone;
	private String Web_phoneExt;
	private String Web_fax;
	private String Web_mobile;
	private String Web_Email;
	private String Web_alternateEmail;
	private String Web_Suffix;
	private String Web_jobtitle;
	private String Web_leadtype;
	private String Web_rating;
	private String Web_source;
	private String Web_source1;
	private String Web_contactmedium;
	private String Web_besttime;
	private String Web_comment;
	
	
	
	
	public String getBirthday() {
		return Birthday;
	}
	public void setBirthday(String birthday) {
		Birthday = birthday;
	}
	public String getAnniversary() {
		return Anniversary;
	}
	public void setAnniversary(String anniversary) {
		Anniversary = anniversary;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getFirstname() {
		return firstname;
	}
	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}
	public String getLastname() {
		return lastname;
	}
	public void setLastname(String lastname) {
		this.lastname = lastname;
	}
	public String getPrimarycontact() {
		return primarycontact;
	}
	public void setPrimarycontact(String primarycontact) {
		this.primarycontact = primarycontact;
	}
	public String getCompnayname() {
		return compnayname;
	}
	public void setCompnayname(String compnayname) {
		this.compnayname = compnayname;
	}
	public String getAssign() {
		return assign;
	}
	public void setAssign(String assign) {
		this.assign = assign;
	}
	public String getOwner() {
		return owner;
	}
	public void setOwner(String owner) {
		this.owner = owner;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public String getZipcode() {
		return zipcode;
	}
	public void setZipcode(String zipcode) {
		this.zipcode = zipcode;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getPhonenumber() {
		return phonenumber;
	}
	public void setPhonenumber(String phonenumber) {
		this.phonenumber = phonenumber;
	}
	public String getExtension() {
		return extension;
	}
	public void setExtension(String extension) {
		this.extension = extension;
	}
	public String getFax() {
		return fax;
	}
	public void setFax(String fax) {
		this.fax = fax;
	}
	public String getMobile() {
		return mobile;
	}
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getAlternarteemail() {
		return alternarteemail;
	}
	public void setAlternarteemail(String alternarteemail) {
		this.alternarteemail = alternarteemail;
	}
	public String getSuffix() {
		return suffix;
	}
	public void setSuffix(String suffix) {
		this.suffix = suffix;
	}
	public String getJobtitle() {
		return jobtitle;
	}
	public void setJobtitle(String jobtitle) {
		this.jobtitle = jobtitle;
	}
	public String getRating() {
		return Rating;
	}
	public void setRating(String rating) {
		Rating = rating;
	}
	public String getSource() {
		return source;
	}
	public void setSource(String source) {
		this.source = source;
	}
	public String getSource1() {
		return source1;
	}
	public void setSource1(String source1) {
		this.source1 = source1;
	}
	public String getContactMedium() {
		return contactMedium;
	}
	public void setContactMedium(String contactMedium) {
		this.contactMedium = contactMedium;
	}
	public String getBesttime() {
		return Besttime;
	}
	public void setBesttime(String besttime) {
		Besttime = besttime;
	}
	public String getGroupid() {
		return groupid;
	}
	public void setGroupid(String groupid) {
		this.groupid = groupid;
	}
	public String getComment() {
		return comment;
	}
	public void setComment(String comment) {
		this.comment = comment;
	}
	public String getSubmit() {
		return submit;
	}
	public void setSubmit(String submit) {
		this.submit = submit;
	}
	public String getWebservice_AssignTo() {
		return Webservice_AssignTo;
	}
	public void setWebservice_AssignTo(String webservice_AssignTo) {
		Webservice_AssignTo = webservice_AssignTo;
	}
	public String getWebService_LeadOwner() {
		return WebService_LeadOwner;
	}
	public void setWebService_LeadOwner(String webService_LeadOwner) {
		WebService_LeadOwner = webService_LeadOwner;
	}
	public String getWebService_areaid() {
		return WebService_areaid;
	}
	public void setWebService_areaid(String webService_areaid) {
		WebService_areaid = webService_areaid;
	}
	public String getWebService_franchiseNo() {
		return WebService_franchiseNo;
	}
	public void setWebService_franchiseNo(String webService_franchiseNo) {
		WebService_franchiseNo = webService_franchiseNo;
	}
	public String getWebservice_divisionID() {
		return Webservice_divisionID;
	}
	public void setWebservice_divisionID(String webservice_divisionID) {
		Webservice_divisionID = webservice_divisionID;
	}
	public String getWeb_Title() {
		return Web_Title;
	}
	public void setWeb_Title(String web_Title) {
		Web_Title = web_Title;
	}
	public String getWeb_firstname() {
		return Web_firstname;
	}
	public void setWeb_firstname(String web_firstname) {
		Web_firstname = web_firstname;
	}
	public String getWeb_lastname() {
		return Web_lastname;
	}
	public void setWeb_lastname(String web_lastname) {
		Web_lastname = web_lastname;
	}
	public String getWeb_Primarycontact() {
		return Web_Primarycontact;
	}
	public void setWeb_Primarycontact(String web_Primarycontact) {
		Web_Primarycontact = web_Primarycontact;
	}
	public String getWeb_companyname() {
		return Web_companyname;
	}
	public void setWeb_companyname(String web_companyname) {
		Web_companyname = web_companyname;
	}
	public String getWeb_Adress() {
		return Web_Adress;
	}
	public void setWeb_Adress(String web_Adress) {
		Web_Adress = web_Adress;
	}
	public String getWeb_city() {
		return Web_city;
	}
	public void setWeb_city(String web_city) {
		Web_city = web_city;
	}
	public String getWeb_country() {
		return Web_country;
	}
	public void setWeb_country(String web_country) {
		Web_country = web_country;
	}
	public String getWeb_State() {
		return Web_State;
	}
	public void setWeb_State(String web_State) {
		Web_State = web_State;
	}
	public String getWeb_zipcode() {
		return Web_zipcode;
	}
	public void setWeb_zipcode(String web_zipcode) {
		Web_zipcode = web_zipcode;
	}
	public String getWeb_phone() {
		return Web_phone;
	}
	public void setWeb_phone(String web_phone) {
		Web_phone = web_phone;
	}
	public String getWeb_phoneExt() {
		return Web_phoneExt;
	}
	public void setWeb_phoneExt(String web_phoneExt) {
		Web_phoneExt = web_phoneExt;
	}
	public String getWeb_fax() {
		return Web_fax;
	}
	public void setWeb_fax(String web_fax) {
		Web_fax = web_fax;
	}
	public String getWeb_mobile() {
		return Web_mobile;
	}
	public void setWeb_mobile(String web_mobile) {
		Web_mobile = web_mobile;
	}
	public String getWeb_Email() {
		return Web_Email;
	}
	public void setWeb_Email(String web_Email) {
		Web_Email = web_Email;
	}
	public String getWeb_alternateEmail() {
		return Web_alternateEmail;
	}
	public void setWeb_alternateEmail(String web_alternateEmail) {
		Web_alternateEmail = web_alternateEmail;
	}
	public String getWeb_Suffix() {
		return Web_Suffix;
	}
	public void setWeb_Suffix(String web_Suffix) {
		Web_Suffix = web_Suffix;
	}
	public String getWeb_jobtitle() {
		return Web_jobtitle;
	}
	public void setWeb_jobtitle(String web_jobtitle) {
		Web_jobtitle = web_jobtitle;
	}
	public String getWeb_leadtype() {
		return Web_leadtype;
	}
	public void setWeb_leadtype(String web_leadtype) {
		Web_leadtype = web_leadtype;
	}
	public String getWeb_rating() {
		return Web_rating;
	}
	public void setWeb_rating(String web_rating) {
		Web_rating = web_rating;
	}
	public String getWeb_source() {
		return Web_source;
	}
	public void setWeb_source(String web_source) {
		Web_source = web_source;
	}
	public String getWeb_source1() {
		return Web_source1;
	}
	public void setWeb_source1(String web_source1) {
		Web_source1 = web_source1;
	}
	public String getWeb_contactmedium() {
		return Web_contactmedium;
	}
	public void setWeb_contactmedium(String web_contactmedium) {
		Web_contactmedium = web_contactmedium;
	}
	public String getWeb_besttime() {
		return Web_besttime;
	}
	public void setWeb_besttime(String web_besttime) {
		Web_besttime = web_besttime;
	}
	public String getWeb_comment() {
		return Web_comment;
	}
	public void setWeb_comment(String web_comment) {
		Web_comment = web_comment;
	}

	
	
	
	
	

}
