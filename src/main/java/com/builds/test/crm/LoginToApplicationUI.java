package com.builds.test.crm;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class LoginToApplicationUI {

	@FindBy(xpath="//*[@id='title']")
	public WebElement title ;
	
	@FindBy(xpath="//*[@id='leadFirstName']")
	public WebElement firstName ;
	
	@FindBy(xpath="//*[@id='leadLastName']")
	public WebElement lastName ;
	
	@FindBy(xpath="//*[@name='leadOwnerID2']")
	public WebElement corporateuser ;
	
	@FindBy(xpath="//*[@id='companyName']")
	public WebElement companyname ;
	
	@FindBy(xpath="//*[@id='address']")
	public WebElement address ;
	
	@FindBy(xpath="//*[@id='phoneNumbers']")
	public WebElement phonenumber ;
	
	@FindBy(xpath="//*[@id='mobileNumbers']")
	public WebElement mobilenumber ;
	
	@FindBy(xpath="//*[@id='emailIds']")
	public WebElement emailid ;
	
	@FindBy(xpath="//*[@id='Submit']")
	public WebElement save;
	
	@FindBy(xpath="//*[@id='primaryContactMethod']")
	public WebElement primaryContactMethod;
	
	public LoginToApplicationUI(WebDriver driver)
	{
		PageFactory.initElements(driver, this);
	}
	

	
}
