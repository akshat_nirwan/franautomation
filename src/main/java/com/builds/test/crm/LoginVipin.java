package com.builds.test.crm;

import java.util.Map;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Test;

import com.builds.utilities.FranconnectUtil;
import com.builds.utilities.TestCase;

public class LoginVipin {

	FranconnectUtil fc=new FranconnectUtil();
	@Test(groups = "login_vipin")
	@TestCase(createdOn = "2018-07-18", updatedOn = "2018-07-18", testCaseDescription = "Login is Successful", testCaseId = "TC_01_Login")
	public void loginpage() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			LoginUI loginui=new LoginUI(driver);
			
			fc.utobj().sendKeys(driver,loginui.SearchBox,"Testing");
			fc.utobj().clickEnterOnElement(driver,loginui.SearchBox);
		
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}
	
	
}
