package com.builds.test.crm;

import java.util.Map;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.Test;

import com.builds.uimaps.crm.CRMLeadsPage;
import com.builds.utilities.FranconnectUtil;
import com.builds.utilities.TestCase;

public class LogintoApplication {
	FranconnectUtil fc = new FranconnectUtil();
	
	@Test(groups = "helloworldt")
	@TestCase(createdOn = "2017-09-18", updatedOn = "2018-09-18", testCaseDescription = "Login to the Application", testCaseId = "2")
	public void loginuser1() throws Exception
	{
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());
		String area="Visibility";
		//Map<String, String> dataSet = fc.utobj().readTestDatawithsqllite("crm", testCaseId,area);
		//System.out.println(dataSet);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		
		try {
			/*driver.get("https://automation9.franconnectqa.net/fc");
			
			driver.findElement(By.xpath("//*[@id='user_id']")).sendKeys(dataSet.get("username"));
			driver.findElement(By.xpath("//*[@id='password']")).sendKeys(dataSet.get("password"));	
			driver.findElement(By.xpath("//*[@id='ulogin']")).click();
			
			CRMLeadsPage pobj = new CRMLeadsPage(driver);
			fc.utobj().printTestStep("Navigate To CRM > Leads > Leads Summary");
			
			fc.crm().crm_common().CRMLeadsLnk(driver);
			fc.utobj().printTestStep("Click on Add New");
			fc.utobj().clickElement(driver, pobj.addNew);
			fc.utobj().printTestStep("Click on Lead Link");
			fc.utobj().clickElement(driver, pobj.leadLnk);
			fc.utobj().printTestStep("Add Lead");
			Thread.sleep(30000);
			testCaseId="Leadinfo";
		//	Map<String, String> dataSetLead = fc.utobj().readTestDatawithsqllite("crm", testCaseId, area);
			LoginToApplicationUI logintoappui=new LoginToApplicationUI(driver);
			fc.utobj().selectDropDownByVisibleText(driver, logintoappui.title,dataSetLead.get("title"));
			fc.utobj().sendKeys(driver, logintoappui.firstName,dataSetLead.get("firstname"));
			fc.utobj().sendKeys(driver, logintoappui.lastName,dataSetLead.get("lastname"));
			fc.utobj().sendKeys(driver, logintoappui.companyname,dataSetLead.get("company"));
			fc.utobj().sendKeys(driver, logintoappui.address,dataSetLead.get("address"));
			fc.utobj().sendKeys(driver, logintoappui.phonenumber,dataSetLead.get("phone"));
			fc.utobj().sendKeys(driver, logintoappui.mobilenumber,dataSetLead.get("mobile"));
			fc.utobj().selectDropDownByVisibleText(driver, logintoappui.corporateuser,dataSetLead.get("corporateuser"));
			String emailid=dataSetLead.get("email");
			String emaiIdd=fc.utobj().generateTestData(emailid)+"@staffex.com";
			fc.utobj().sendKeys(driver, logintoappui.emailid,emaiIdd);
			fc.utobj().selectDropDownByVisibleText(driver,logintoappui.primaryContactMethod,dataSetLead.get("primarycontactmethod"));
			fc.utobj().clickElement(driver,logintoappui.save);
			fc.utobj().printTestStep("Lead Added");
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);*/
		}
		catch(Exception e){
			
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}
	
	/*@Test(groups = "helloworld119")
	@TestCase(createdOn = "2017-09-18", updatedOn = "2018-09-18", testCaseDescription = "Login to the Application", testCaseId = "Login")
	public void loginuser2() throws Exception
	{
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestDatawithsqllite("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		
		try {
			driver.get("https://automation9.franconnectqa.net/fc");
			
			driver.findElement(By.xpath("//*[@id='user_id']")).sendKeys(dataSet.get("username"));
			driver.findElement(By.xpath("//*[@id='password']")).sendKeys(dataSet.get("password"));	
			driver.findElement(By.xpath("//*[@id='ulogin']")).click();
			
			CRMLeadsPage pobj = new CRMLeadsPage(driver);
			fc.utobj().printTestStep("Navigate To CRM > Leads > Leads Summary");
			
			fc.crm().crm_common().CRMLeadsLnk(driver);
			fc.utobj().printTestStep("Click on Add New");
			fc.utobj().clickElement(driver, pobj.addNew);
			fc.utobj().printTestStep("Click on Lead Link");
			fc.utobj().clickElement(driver, pobj.leadLnk);
			fc.utobj().printTestStep("Add Lead");
			Thread.sleep(30000);
			testCaseId="Leadinfo";
			Map<String, String> dataSetLead = fc.utobj().readTestDatawithsqllite("crm", testCaseId);
			LoginToApplicationUI logintoappui=new LoginToApplicationUI(driver);
			fc.utobj().selectDropDownByVisibleText(driver, logintoappui.title,dataSetLead.get("title"));
			fc.utobj().sendKeys(driver, logintoappui.firstName,dataSetLead.get("firstname"));
			fc.utobj().sendKeys(driver, logintoappui.lastName,dataSetLead.get("lastname"));
			fc.utobj().sendKeys(driver, logintoappui.companyname,dataSetLead.get("company"));
			fc.utobj().sendKeys(driver, logintoappui.address,dataSetLead.get("address"));
			fc.utobj().sendKeys(driver, logintoappui.phonenumber,dataSetLead.get("phone"));
			fc.utobj().sendKeys(driver, logintoappui.mobilenumber,dataSetLead.get("mobile"));
			fc.utobj().selectDropDownByVisibleText(driver, logintoappui.corporateuser,dataSetLead.get("corporateuser"));
			String emailid=dataSetLead.get("email");
			String emaiIdd=fc.utobj().generateTestData(emailid)+"@staffex.com";
			fc.utobj().sendKeys(driver, logintoappui.emailid,emaiIdd);
			fc.utobj().selectDropDownByVisibleText(driver,logintoappui.primaryContactMethod,dataSetLead.get("primarycontactmethod"));
			fc.utobj().clickElement(driver,logintoappui.save);
			fc.utobj().printTestStep("Lead Added");
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		}
		catch(Exception e){
			
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}
	@Test(groups = "helloworld119")
	@TestCase(createdOn = "2017-09-18", updatedOn = "2018-09-18", testCaseDescription = "Login to the Application", testCaseId = "Login")
	public void loginuser3() throws Exception
	{
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestDatawithsqllite("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		
		try {
			driver.get("https://automation9.franconnectqa.net/fc");
			
			driver.findElement(By.xpath("//*[@id='user_id']")).sendKeys(dataSet.get("username"));
			driver.findElement(By.xpath("//*[@id='password']")).sendKeys(dataSet.get("password"));	
			driver.findElement(By.xpath("//*[@id='ulogin']")).click();
			
			CRMLeadsPage pobj = new CRMLeadsPage(driver);
			fc.utobj().printTestStep("Navigate To CRM > Leads > Leads Summary");
			
			fc.crm().crm_common().CRMLeadsLnk(driver);
			fc.utobj().printTestStep("Click on Add New");
			fc.utobj().clickElement(driver, pobj.addNew);
			fc.utobj().printTestStep("Click on Lead Link");
			fc.utobj().clickElement(driver, pobj.leadLnk);
			fc.utobj().printTestStep("Add Lead");
			Thread.sleep(30000);
			testCaseId="Leadinfo";
			Map<String, String> dataSetLead = fc.utobj().readTestDatawithsqllite("crm", testCaseId);
			LoginToApplicationUI logintoappui=new LoginToApplicationUI(driver);
			fc.utobj().selectDropDownByVisibleText(driver, logintoappui.title,dataSetLead.get("title"));
			fc.utobj().sendKeys(driver, logintoappui.firstName,dataSetLead.get("firstname"));
			fc.utobj().sendKeys(driver, logintoappui.lastName,dataSetLead.get("lastname"));
			fc.utobj().sendKeys(driver, logintoappui.companyname,dataSetLead.get("company"));
			fc.utobj().sendKeys(driver, logintoappui.address,dataSetLead.get("address"));
			fc.utobj().sendKeys(driver, logintoappui.phonenumber,dataSetLead.get("phone"));
			fc.utobj().sendKeys(driver, logintoappui.mobilenumber,dataSetLead.get("mobile"));
			fc.utobj().selectDropDownByVisibleText(driver, logintoappui.corporateuser,dataSetLead.get("corporateuser"));
			String emailid=dataSetLead.get("email");
			String emaiIdd=fc.utobj().generateTestData(emailid)+"@staffex.com";
			fc.utobj().sendKeys(driver, logintoappui.emailid,emaiIdd);
			fc.utobj().selectDropDownByVisibleText(driver,logintoappui.primaryContactMethod,dataSetLead.get("primarycontactmethod"));
			fc.utobj().clickElement(driver,logintoappui.save);
			fc.utobj().printTestStep("Lead Added");
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		}
		catch(Exception e){
			
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}
	@Test(groups = "helloworld119")
	@TestCase(createdOn = "2017-09-18", updatedOn = "2018-09-18", testCaseDescription = "Login to the Application", testCaseId = "Login")
	public void loginuser4() throws Exception
	{
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestDatawithsqllite("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		
		try {
			driver.get("https://automation9.franconnectqa.net/fc");
			
			driver.findElement(By.xpath("//*[@id='user_id']")).sendKeys(dataSet.get("username"));
			driver.findElement(By.xpath("//*[@id='password']")).sendKeys(dataSet.get("password"));	
			driver.findElement(By.xpath("//*[@id='ulogin']")).click();
			
			CRMLeadsPage pobj = new CRMLeadsPage(driver);
			fc.utobj().printTestStep("Navigate To CRM > Leads > Leads Summary");
			
			fc.crm().crm_common().CRMLeadsLnk(driver);
			fc.utobj().printTestStep("Click on Add New");
			fc.utobj().clickElement(driver, pobj.addNew);
			fc.utobj().printTestStep("Click on Lead Link");
			fc.utobj().clickElement(driver, pobj.leadLnk);
			fc.utobj().printTestStep("Add Lead");
			Thread.sleep(30000);
			testCaseId="Leadinfo";
			Map<String, String> dataSetLead = fc.utobj().readTestDatawithsqllite("crm", testCaseId);
			LoginToApplicationUI logintoappui=new LoginToApplicationUI(driver);
			fc.utobj().selectDropDownByVisibleText(driver, logintoappui.title,dataSetLead.get("title"));
			fc.utobj().sendKeys(driver, logintoappui.firstName,dataSetLead.get("firstname"));
			fc.utobj().sendKeys(driver, logintoappui.lastName,dataSetLead.get("lastname"));
			fc.utobj().sendKeys(driver, logintoappui.companyname,dataSetLead.get("company"));
			fc.utobj().sendKeys(driver, logintoappui.address,dataSetLead.get("address"));
			fc.utobj().sendKeys(driver, logintoappui.phonenumber,dataSetLead.get("phone"));
			fc.utobj().sendKeys(driver, logintoappui.mobilenumber,dataSetLead.get("mobile"));
			fc.utobj().selectDropDownByVisibleText(driver, logintoappui.corporateuser,dataSetLead.get("corporateuser"));
			String emailid=dataSetLead.get("email");
			String emaiIdd=fc.utobj().generateTestData(emailid)+"@staffex.com";
			fc.utobj().sendKeys(driver, logintoappui.emailid,emaiIdd);
			fc.utobj().selectDropDownByVisibleText(driver,logintoappui.primaryContactMethod,dataSetLead.get("primarycontactmethod"));
			fc.utobj().clickElement(driver,logintoappui.save);
			fc.utobj().printTestStep("Lead Added");
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		}
		catch(Exception e){
			
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}
	@Test(groups = "helloworld119")
	@TestCase(createdOn = "2017-09-18", updatedOn = "2018-09-18", testCaseDescription = "Login to the Application", testCaseId = "Login")
	public void loginuser5() throws Exception
	{
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestDatawithsqllite("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		
		try {
			driver.get("https://automation9.franconnectqa.net/fc");
			
			driver.findElement(By.xpath("//*[@id='user_id']")).sendKeys(dataSet.get("username"));
			driver.findElement(By.xpath("//*[@id='password']")).sendKeys(dataSet.get("password"));	
			driver.findElement(By.xpath("//*[@id='ulogin']")).click();
			
			CRMLeadsPage pobj = new CRMLeadsPage(driver);
			fc.utobj().printTestStep("Navigate To CRM > Leads > Leads Summary");
			
			fc.crm().crm_common().CRMLeadsLnk(driver);
			fc.utobj().printTestStep("Click on Add New");
			fc.utobj().clickElement(driver, pobj.addNew);
			fc.utobj().printTestStep("Click on Lead Link");
			fc.utobj().clickElement(driver, pobj.leadLnk);
			fc.utobj().printTestStep("Add Lead");
			Thread.sleep(30000);
			testCaseId="Leadinfo";
			Map<String, String> dataSetLead = fc.utobj().readTestDatawithsqllite("crm", testCaseId);
			LoginToApplicationUI logintoappui=new LoginToApplicationUI(driver);
			fc.utobj().selectDropDownByVisibleText(driver, logintoappui.title,dataSetLead.get("title"));
			fc.utobj().sendKeys(driver, logintoappui.firstName,dataSetLead.get("firstname"));
			fc.utobj().sendKeys(driver, logintoappui.lastName,dataSetLead.get("lastname"));
			fc.utobj().sendKeys(driver, logintoappui.companyname,dataSetLead.get("company"));
			fc.utobj().sendKeys(driver, logintoappui.address,dataSetLead.get("address"));
			fc.utobj().sendKeys(driver, logintoappui.phonenumber,dataSetLead.get("phone"));
			fc.utobj().sendKeys(driver, logintoappui.mobilenumber,dataSetLead.get("mobile"));
			fc.utobj().selectDropDownByVisibleText(driver, logintoappui.corporateuser,dataSetLead.get("corporateuser"));
			String emailid=dataSetLead.get("email");
			String emaiIdd=fc.utobj().generateTestData(emailid)+"@staffex.com";
			fc.utobj().sendKeys(driver, logintoappui.emailid,emaiIdd);
			fc.utobj().selectDropDownByVisibleText(driver,logintoappui.primaryContactMethod,dataSetLead.get("primarycontactmethod"));
			fc.utobj().clickElement(driver,logintoappui.save);
			fc.utobj().printTestStep("Lead Added");
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		}
		catch(Exception e){
			
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}
	@Test(groups = "helloworld119")
	@TestCase(createdOn = "2017-09-18", updatedOn = "2018-09-18", testCaseDescription = "Login to the Application", testCaseId = "Login")
	public void loginuser6() throws Exception
	{
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestDatawithsqllite("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		
		try {
			driver.get("https://automation9.franconnectqa.net/fc");
			
			driver.findElement(By.xpath("//*[@id='user_id']")).sendKeys(dataSet.get("username"));
			driver.findElement(By.xpath("//*[@id='password']")).sendKeys(dataSet.get("password"));	
			driver.findElement(By.xpath("//*[@id='ulogin']")).click();
			
			CRMLeadsPage pobj = new CRMLeadsPage(driver);
			fc.utobj().printTestStep("Navigate To CRM > Leads > Leads Summary");
			
			fc.crm().crm_common().CRMLeadsLnk(driver);
			fc.utobj().printTestStep("Click on Add New");
			fc.utobj().clickElement(driver, pobj.addNew);
			fc.utobj().printTestStep("Click on Lead Link");
			fc.utobj().clickElement(driver, pobj.leadLnk);
			fc.utobj().printTestStep("Add Lead");
			Thread.sleep(30000);
			testCaseId="Leadinfo";
			Map<String, String> dataSetLead = fc.utobj().readTestDatawithsqllite("crm", testCaseId);
			LoginToApplicationUI logintoappui=new LoginToApplicationUI(driver);
			fc.utobj().selectDropDownByVisibleText(driver, logintoappui.title,dataSetLead.get("title"));
			fc.utobj().sendKeys(driver, logintoappui.firstName,dataSetLead.get("firstname"));
			fc.utobj().sendKeys(driver, logintoappui.lastName,dataSetLead.get("lastname"));
			fc.utobj().sendKeys(driver, logintoappui.companyname,dataSetLead.get("company"));
			fc.utobj().sendKeys(driver, logintoappui.address,dataSetLead.get("address"));
			fc.utobj().sendKeys(driver, logintoappui.phonenumber,dataSetLead.get("phone"));
			fc.utobj().sendKeys(driver, logintoappui.mobilenumber,dataSetLead.get("mobile"));
			fc.utobj().selectDropDownByVisibleText(driver, logintoappui.corporateuser,dataSetLead.get("corporateuser"));
			String emailid=dataSetLead.get("email");
			String emaiIdd=fc.utobj().generateTestData(emailid)+"@staffex.com";
			fc.utobj().sendKeys(driver, logintoappui.emailid,emaiIdd);
			fc.utobj().selectDropDownByVisibleText(driver,logintoappui.primaryContactMethod,dataSetLead.get("primarycontactmethod"));
			fc.utobj().clickElement(driver,logintoappui.save);
			fc.utobj().printTestStep("Lead Added");
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		}
		catch(Exception e){
			
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}
	@Test(groups = "helloworld119")
	@TestCase(createdOn = "2017-09-18", updatedOn = "2018-09-18", testCaseDescription = "Login to the Application", testCaseId = "Login")
	public void loginuser7() throws Exception
	{
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestDatawithsqllite("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		
		try {
			driver.get("https://automation9.franconnectqa.net/fc");
			
			driver.findElement(By.xpath("//*[@id='user_id']")).sendKeys(dataSet.get("username"));
			driver.findElement(By.xpath("//*[@id='password']")).sendKeys(dataSet.get("password"));	
			driver.findElement(By.xpath("//*[@id='ulogin']")).click();
			
			CRMLeadsPage pobj = new CRMLeadsPage(driver);
			fc.utobj().printTestStep("Navigate To CRM > Leads > Leads Summary");
			
			fc.crm().crm_common().CRMLeadsLnk(driver);
			fc.utobj().printTestStep("Click on Add New");
			fc.utobj().clickElement(driver, pobj.addNew);
			fc.utobj().printTestStep("Click on Lead Link");
			fc.utobj().clickElement(driver, pobj.leadLnk);
			fc.utobj().printTestStep("Add Lead");
			Thread.sleep(30000);
			testCaseId="Leadinfo";
			Map<String, String> dataSetLead = fc.utobj().readTestDatawithsqllite("crm", testCaseId);
			LoginToApplicationUI logintoappui=new LoginToApplicationUI(driver);
			fc.utobj().selectDropDownByVisibleText(driver, logintoappui.title,dataSetLead.get("title"));
			fc.utobj().sendKeys(driver, logintoappui.firstName,dataSetLead.get("firstname"));
			fc.utobj().sendKeys(driver, logintoappui.lastName,dataSetLead.get("lastname"));
			fc.utobj().sendKeys(driver, logintoappui.companyname,dataSetLead.get("company"));
			fc.utobj().sendKeys(driver, logintoappui.address,dataSetLead.get("address"));
			fc.utobj().sendKeys(driver, logintoappui.phonenumber,dataSetLead.get("phone"));
			fc.utobj().sendKeys(driver, logintoappui.mobilenumber,dataSetLead.get("mobile"));
			fc.utobj().selectDropDownByVisibleText(driver, logintoappui.corporateuser,dataSetLead.get("corporateuser"));
			String emailid=dataSetLead.get("email");
			String emaiIdd=fc.utobj().generateTestData(emailid)+"@staffex.com";
			fc.utobj().sendKeys(driver, logintoappui.emailid,emaiIdd);
			fc.utobj().selectDropDownByVisibleText(driver,logintoappui.primaryContactMethod,dataSetLead.get("primarycontactmethod"));
			fc.utobj().clickElement(driver,logintoappui.save);
			fc.utobj().printTestStep("Lead Added");
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		}
		catch(Exception e){
			
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}
	@Test(groups = "helloworld119")
	@TestCase(createdOn = "2017-09-18", updatedOn = "2018-09-18", testCaseDescription = "Login to the Application", testCaseId = "Login")
	public void loginuser8() throws Exception
	{
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestDatawithsqllite("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		
		try {
			driver.get("https://automation9.franconnectqa.net/fc");
			
			driver.findElement(By.xpath("//*[@id='user_id']")).sendKeys(dataSet.get("username"));
			driver.findElement(By.xpath("//*[@id='password']")).sendKeys(dataSet.get("password"));	
			driver.findElement(By.xpath("//*[@id='ulogin']")).click();
			
			CRMLeadsPage pobj = new CRMLeadsPage(driver);
			fc.utobj().printTestStep("Navigate To CRM > Leads > Leads Summary");
			
			fc.crm().crm_common().CRMLeadsLnk(driver);
			fc.utobj().printTestStep("Click on Add New");
			fc.utobj().clickElement(driver, pobj.addNew);
			fc.utobj().printTestStep("Click on Lead Link");
			fc.utobj().clickElement(driver, pobj.leadLnk);
			fc.utobj().printTestStep("Add Lead");
			Thread.sleep(30000);
			testCaseId="Leadinfo";
			Map<String, String> dataSetLead = fc.utobj().readTestDatawithsqllite("crm", testCaseId);
			LoginToApplicationUI logintoappui=new LoginToApplicationUI(driver);
			fc.utobj().selectDropDownByVisibleText(driver, logintoappui.title,dataSetLead.get("title"));
			fc.utobj().sendKeys(driver, logintoappui.firstName,dataSetLead.get("firstname"));
			fc.utobj().sendKeys(driver, logintoappui.lastName,dataSetLead.get("lastname"));
			fc.utobj().sendKeys(driver, logintoappui.companyname,dataSetLead.get("company"));
			fc.utobj().sendKeys(driver, logintoappui.address,dataSetLead.get("address"));
			fc.utobj().sendKeys(driver, logintoappui.phonenumber,dataSetLead.get("phone"));
			fc.utobj().sendKeys(driver, logintoappui.mobilenumber,dataSetLead.get("mobile"));
			fc.utobj().selectDropDownByVisibleText(driver, logintoappui.corporateuser,dataSetLead.get("corporateuser"));
			String emailid=dataSetLead.get("email");
			String emaiIdd=fc.utobj().generateTestData(emailid)+"@staffex.com";
			fc.utobj().sendKeys(driver, logintoappui.emailid,emaiIdd);
			fc.utobj().selectDropDownByVisibleText(driver,logintoappui.primaryContactMethod,dataSetLead.get("primarycontactmethod"));
			fc.utobj().clickElement(driver,logintoappui.save);
			fc.utobj().printTestStep("Lead Added");
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		}
		catch(Exception e){
			
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}
	@Test(groups = "helloworld119")
	@TestCase(createdOn = "2017-09-18", updatedOn = "2018-09-18", testCaseDescription = "Login to the Application", testCaseId = "Login")
	public void loginuser9() throws Exception
	{
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestDatawithsqllite("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		
		try {
			driver.get("https://automation9.franconnectqa.net/fc");
			
			driver.findElement(By.xpath("//*[@id='user_id']")).sendKeys(dataSet.get("username"));
			driver.findElement(By.xpath("//*[@id='password']")).sendKeys(dataSet.get("password"));	
			driver.findElement(By.xpath("//*[@id='ulogin']")).click();
			
			CRMLeadsPage pobj = new CRMLeadsPage(driver);
			fc.utobj().printTestStep("Navigate To CRM > Leads > Leads Summary");
			
			fc.crm().crm_common().CRMLeadsLnk(driver);
			fc.utobj().printTestStep("Click on Add New");
			fc.utobj().clickElement(driver, pobj.addNew);
			fc.utobj().printTestStep("Click on Lead Link");
			fc.utobj().clickElement(driver, pobj.leadLnk);
			fc.utobj().printTestStep("Add Lead");
			Thread.sleep(30000);
			testCaseId="Leadinfo";
			Map<String, String> dataSetLead = fc.utobj().readTestDatawithsqllite("crm", testCaseId);
			LoginToApplicationUI logintoappui=new LoginToApplicationUI(driver);
			fc.utobj().selectDropDownByVisibleText(driver, logintoappui.title,dataSetLead.get("title"));
			fc.utobj().sendKeys(driver, logintoappui.firstName,dataSetLead.get("firstname"));
			fc.utobj().sendKeys(driver, logintoappui.lastName,dataSetLead.get("lastname"));
			fc.utobj().sendKeys(driver, logintoappui.companyname,dataSetLead.get("company"));
			fc.utobj().sendKeys(driver, logintoappui.address,dataSetLead.get("address"));
			fc.utobj().sendKeys(driver, logintoappui.phonenumber,dataSetLead.get("phone"));
			fc.utobj().sendKeys(driver, logintoappui.mobilenumber,dataSetLead.get("mobile"));
			fc.utobj().selectDropDownByVisibleText(driver, logintoappui.corporateuser,dataSetLead.get("corporateuser"));
			String emailid=dataSetLead.get("email");
			String emaiIdd=fc.utobj().generateTestData(emailid)+"@staffex.com";
			fc.utobj().sendKeys(driver, logintoappui.emailid,emaiIdd);
			fc.utobj().selectDropDownByVisibleText(driver,logintoappui.primaryContactMethod,dataSetLead.get("primarycontactmethod"));
			fc.utobj().clickElement(driver,logintoappui.save);
			fc.utobj().printTestStep("Lead Added");
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		}
		catch(Exception e){
			
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}
	@Test(groups = "helloworld119")
	@TestCase(createdOn = "2017-09-18", updatedOn = "2018-09-18", testCaseDescription = "Login to the Application", testCaseId = "Login")
	public void loginuser10() throws Exception
	{
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestDatawithsqllite("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		
		try {
			driver.get("https://automation9.franconnectqa.net/fc");
			
			driver.findElement(By.xpath("//*[@id='user_id']")).sendKeys(dataSet.get("username"));
			driver.findElement(By.xpath("//*[@id='password']")).sendKeys(dataSet.get("password"));	
			driver.findElement(By.xpath("//*[@id='ulogin']")).click();
			
			CRMLeadsPage pobj = new CRMLeadsPage(driver);
			fc.utobj().printTestStep("Navigate To CRM > Leads > Leads Summary");
			
			fc.crm().crm_common().CRMLeadsLnk(driver);
			fc.utobj().printTestStep("Click on Add New");
			fc.utobj().clickElement(driver, pobj.addNew);
			fc.utobj().printTestStep("Click on Lead Link");
			fc.utobj().clickElement(driver, pobj.leadLnk);
			fc.utobj().printTestStep("Add Lead");
			Thread.sleep(30000);
			testCaseId="Leadinfo";
			Map<String, String> dataSetLead = fc.utobj().readTestDatawithsqllite("crm", testCaseId);
			LoginToApplicationUI logintoappui=new LoginToApplicationUI(driver);
			fc.utobj().selectDropDownByVisibleText(driver, logintoappui.title,dataSetLead.get("title"));
			fc.utobj().sendKeys(driver, logintoappui.firstName,dataSetLead.get("firstname"));
			fc.utobj().sendKeys(driver, logintoappui.lastName,dataSetLead.get("lastname"));
			fc.utobj().sendKeys(driver, logintoappui.companyname,dataSetLead.get("company"));
			fc.utobj().sendKeys(driver, logintoappui.address,dataSetLead.get("address"));
			fc.utobj().sendKeys(driver, logintoappui.phonenumber,dataSetLead.get("phone"));
			fc.utobj().sendKeys(driver, logintoappui.mobilenumber,dataSetLead.get("mobile"));
			fc.utobj().selectDropDownByVisibleText(driver, logintoappui.corporateuser,dataSetLead.get("corporateuser"));
			String emailid=dataSetLead.get("email");
			String emaiIdd=fc.utobj().generateTestData(emailid)+"@staffex.com";
			fc.utobj().sendKeys(driver, logintoappui.emailid,emaiIdd);
			fc.utobj().selectDropDownByVisibleText(driver,logintoappui.primaryContactMethod,dataSetLead.get("primarycontactmethod"));
			fc.utobj().clickElement(driver,logintoappui.save);
			fc.utobj().printTestStep("Lead Added");
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		}
		catch(Exception e){
			
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}
	@Test(groups = "helloworld119")
	@TestCase(createdOn = "2017-09-18", updatedOn = "2018-09-18", testCaseDescription = "Login to the Application", testCaseId = "Login")
	public void loginuser11() throws Exception
	{
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestDatawithsqllite("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		
		try {
			driver.get("https://automation9.franconnectqa.net/fc");
			
			driver.findElement(By.xpath("//*[@id='user_id']")).sendKeys(dataSet.get("username"));
			driver.findElement(By.xpath("//*[@id='password']")).sendKeys(dataSet.get("password"));	
			driver.findElement(By.xpath("//*[@id='ulogin']")).click();
			
			CRMLeadsPage pobj = new CRMLeadsPage(driver);
			fc.utobj().printTestStep("Navigate To CRM > Leads > Leads Summary");
			
			fc.crm().crm_common().CRMLeadsLnk(driver);
			fc.utobj().printTestStep("Click on Add New");
			fc.utobj().clickElement(driver, pobj.addNew);
			fc.utobj().printTestStep("Click on Lead Link");
			fc.utobj().clickElement(driver, pobj.leadLnk);
			fc.utobj().printTestStep("Add Lead");
			Thread.sleep(30000);
			testCaseId="Leadinfo";
			Map<String, String> dataSetLead = fc.utobj().readTestDatawithsqllite("crm", testCaseId);
			LoginToApplicationUI logintoappui=new LoginToApplicationUI(driver);
			fc.utobj().selectDropDownByVisibleText(driver, logintoappui.title,dataSetLead.get("title"));
			fc.utobj().sendKeys(driver, logintoappui.firstName,dataSetLead.get("firstname"));
			fc.utobj().sendKeys(driver, logintoappui.lastName,dataSetLead.get("lastname"));
			fc.utobj().sendKeys(driver, logintoappui.companyname,dataSetLead.get("company"));
			fc.utobj().sendKeys(driver, logintoappui.address,dataSetLead.get("address"));
			fc.utobj().sendKeys(driver, logintoappui.phonenumber,dataSetLead.get("phone"));
			fc.utobj().sendKeys(driver, logintoappui.mobilenumber,dataSetLead.get("mobile"));
			fc.utobj().selectDropDownByVisibleText(driver, logintoappui.corporateuser,dataSetLead.get("corporateuser"));
			String emailid=dataSetLead.get("email");
			String emaiIdd=fc.utobj().generateTestData(emailid)+"@staffex.com";
			fc.utobj().sendKeys(driver, logintoappui.emailid,emaiIdd);
			fc.utobj().selectDropDownByVisibleText(driver,logintoappui.primaryContactMethod,dataSetLead.get("primarycontactmethod"));
			fc.utobj().clickElement(driver,logintoappui.save);
			fc.utobj().printTestStep("Lead Added");
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		}
		catch(Exception e){
			
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}
	@Test(groups = "helloworld119")
	@TestCase(createdOn = "2017-09-18", updatedOn = "2018-09-18", testCaseDescription = "Login to the Application", testCaseId = "Login")
	public void loginuser12() throws Exception
	{
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestDatawithsqllite("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		
		try {
			driver.get("https://automation9.franconnectqa.net/fc");
			
			driver.findElement(By.xpath("//*[@id='user_id']")).sendKeys(dataSet.get("username"));
			driver.findElement(By.xpath("//*[@id='password']")).sendKeys(dataSet.get("password"));	
			driver.findElement(By.xpath("//*[@id='ulogin']")).click();
			
			CRMLeadsPage pobj = new CRMLeadsPage(driver);
			fc.utobj().printTestStep("Navigate To CRM > Leads > Leads Summary");
			
			fc.crm().crm_common().CRMLeadsLnk(driver);
			fc.utobj().printTestStep("Click on Add New");
			fc.utobj().clickElement(driver, pobj.addNew);
			fc.utobj().printTestStep("Click on Lead Link");
			fc.utobj().clickElement(driver, pobj.leadLnk);
			fc.utobj().printTestStep("Add Lead");
			Thread.sleep(30000);
			testCaseId="Leadinfo";
			Map<String, String> dataSetLead = fc.utobj().readTestDatawithsqllite("crm", testCaseId);
			LoginToApplicationUI logintoappui=new LoginToApplicationUI(driver);
			fc.utobj().selectDropDownByVisibleText(driver, logintoappui.title,dataSetLead.get("title"));
			fc.utobj().sendKeys(driver, logintoappui.firstName,dataSetLead.get("firstname"));
			fc.utobj().sendKeys(driver, logintoappui.lastName,dataSetLead.get("lastname"));
			fc.utobj().sendKeys(driver, logintoappui.companyname,dataSetLead.get("company"));
			fc.utobj().sendKeys(driver, logintoappui.address,dataSetLead.get("address"));
			fc.utobj().sendKeys(driver, logintoappui.phonenumber,dataSetLead.get("phone"));
			fc.utobj().sendKeys(driver, logintoappui.mobilenumber,dataSetLead.get("mobile"));
			fc.utobj().selectDropDownByVisibleText(driver, logintoappui.corporateuser,dataSetLead.get("corporateuser"));
			String emailid=dataSetLead.get("email");
			String emaiIdd=fc.utobj().generateTestData(emailid)+"@staffex.com";
			fc.utobj().sendKeys(driver, logintoappui.emailid,emaiIdd);
			fc.utobj().selectDropDownByVisibleText(driver,logintoappui.primaryContactMethod,dataSetLead.get("primarycontactmethod"));
			fc.utobj().clickElement(driver,logintoappui.save);
			fc.utobj().printTestStep("Lead Added");
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		}
		catch(Exception e){
			
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}
	@Test(groups = "helloworld119")
	@TestCase(createdOn = "2017-09-18", updatedOn = "2018-09-18", testCaseDescription = "Login to the Application", testCaseId = "Login")
	public void loginuser13() throws Exception
	{
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestDatawithsqllite("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		
		try {
			driver.get("https://automation9.franconnectqa.net/fc");
			
			driver.findElement(By.xpath("//*[@id='user_id']")).sendKeys(dataSet.get("username"));
			driver.findElement(By.xpath("//*[@id='password']")).sendKeys(dataSet.get("password"));	
			driver.findElement(By.xpath("//*[@id='ulogin']")).click();
			
			CRMLeadsPage pobj = new CRMLeadsPage(driver);
			fc.utobj().printTestStep("Navigate To CRM > Leads > Leads Summary");
			
			fc.crm().crm_common().CRMLeadsLnk(driver);
			fc.utobj().printTestStep("Click on Add New");
			fc.utobj().clickElement(driver, pobj.addNew);
			fc.utobj().printTestStep("Click on Lead Link");
			fc.utobj().clickElement(driver, pobj.leadLnk);
			fc.utobj().printTestStep("Add Lead");
			Thread.sleep(30000);
			testCaseId="Leadinfo";
			Map<String, String> dataSetLead = fc.utobj().readTestDatawithsqllite("crm", testCaseId);
			LoginToApplicationUI logintoappui=new LoginToApplicationUI(driver);
			fc.utobj().selectDropDownByVisibleText(driver, logintoappui.title,dataSetLead.get("title"));
			fc.utobj().sendKeys(driver, logintoappui.firstName,dataSetLead.get("firstname"));
			fc.utobj().sendKeys(driver, logintoappui.lastName,dataSetLead.get("lastname"));
			fc.utobj().sendKeys(driver, logintoappui.companyname,dataSetLead.get("company"));
			fc.utobj().sendKeys(driver, logintoappui.address,dataSetLead.get("address"));
			fc.utobj().sendKeys(driver, logintoappui.phonenumber,dataSetLead.get("phone"));
			fc.utobj().sendKeys(driver, logintoappui.mobilenumber,dataSetLead.get("mobile"));
			fc.utobj().selectDropDownByVisibleText(driver, logintoappui.corporateuser,dataSetLead.get("corporateuser"));
			String emailid=dataSetLead.get("email");
			String emaiIdd=fc.utobj().generateTestData(emailid)+"@staffex.com";
			fc.utobj().sendKeys(driver, logintoappui.emailid,emaiIdd);
			fc.utobj().selectDropDownByVisibleText(driver,logintoappui.primaryContactMethod,dataSetLead.get("primarycontactmethod"));
			fc.utobj().clickElement(driver,logintoappui.save);
			fc.utobj().printTestStep("Lead Added");
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		}
		catch(Exception e){
			
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}
	@Test(groups = "helloworld119")
	@TestCase(createdOn = "2017-09-18", updatedOn = "2018-09-18", testCaseDescription = "Login to the Application", testCaseId = "Login")
	public void loginuser14() throws Exception
	{
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestDatawithsqllite("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		
		try {
			driver.get("https://automation9.franconnectqa.net/fc");
			
			driver.findElement(By.xpath("//*[@id='user_id']")).sendKeys(dataSet.get("username"));
			driver.findElement(By.xpath("//*[@id='password']")).sendKeys(dataSet.get("password"));	
			driver.findElement(By.xpath("//*[@id='ulogin']")).click();
			
			CRMLeadsPage pobj = new CRMLeadsPage(driver);
			fc.utobj().printTestStep("Navigate To CRM > Leads > Leads Summary");
			
			fc.crm().crm_common().CRMLeadsLnk(driver);
			fc.utobj().printTestStep("Click on Add New");
			fc.utobj().clickElement(driver, pobj.addNew);
			fc.utobj().printTestStep("Click on Lead Link");
			fc.utobj().clickElement(driver, pobj.leadLnk);
			fc.utobj().printTestStep("Add Lead");
			Thread.sleep(30000);
			testCaseId="Leadinfo";
			Map<String, String> dataSetLead = fc.utobj().readTestDatawithsqllite("crm", testCaseId);
			LoginToApplicationUI logintoappui=new LoginToApplicationUI(driver);
			fc.utobj().selectDropDownByVisibleText(driver, logintoappui.title,dataSetLead.get("title"));
			fc.utobj().sendKeys(driver, logintoappui.firstName,dataSetLead.get("firstname"));
			fc.utobj().sendKeys(driver, logintoappui.lastName,dataSetLead.get("lastname"));
			fc.utobj().sendKeys(driver, logintoappui.companyname,dataSetLead.get("company"));
			fc.utobj().sendKeys(driver, logintoappui.address,dataSetLead.get("address"));
			fc.utobj().sendKeys(driver, logintoappui.phonenumber,dataSetLead.get("phone"));
			fc.utobj().sendKeys(driver, logintoappui.mobilenumber,dataSetLead.get("mobile"));
			fc.utobj().selectDropDownByVisibleText(driver, logintoappui.corporateuser,dataSetLead.get("corporateuser"));
			String emailid=dataSetLead.get("email");
			String emaiIdd=fc.utobj().generateTestData(emailid)+"@staffex.com";
			fc.utobj().sendKeys(driver, logintoappui.emailid,emaiIdd);
			fc.utobj().selectDropDownByVisibleText(driver,logintoappui.primaryContactMethod,dataSetLead.get("primarycontactmethod"));
			fc.utobj().clickElement(driver,logintoappui.save);
			fc.utobj().printTestStep("Lead Added");
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		}
		catch(Exception e){
			
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}
	@Test(groups = "helloworld119")
	@TestCase(createdOn = "2017-09-18", updatedOn = "2018-09-18", testCaseDescription = "Login to the Application", testCaseId = "Login")
	public void loginuser15() throws Exception
	{
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestDatawithsqllite("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		
		try {
			driver.get("https://automation9.franconnectqa.net/fc");
			
			driver.findElement(By.xpath("//*[@id='user_id']")).sendKeys(dataSet.get("username"));
			driver.findElement(By.xpath("//*[@id='password']")).sendKeys(dataSet.get("password"));	
			driver.findElement(By.xpath("//*[@id='ulogin']")).click();
			
			CRMLeadsPage pobj = new CRMLeadsPage(driver);
			fc.utobj().printTestStep("Navigate To CRM > Leads > Leads Summary");
			
			fc.crm().crm_common().CRMLeadsLnk(driver);
			fc.utobj().printTestStep("Click on Add New");
			fc.utobj().clickElement(driver, pobj.addNew);
			fc.utobj().printTestStep("Click on Lead Link");
			fc.utobj().clickElement(driver, pobj.leadLnk);
			fc.utobj().printTestStep("Add Lead");
			Thread.sleep(30000);
			testCaseId="Leadinfo";
			Map<String, String> dataSetLead = fc.utobj().readTestDatawithsqllite("crm", testCaseId);
			LoginToApplicationUI logintoappui=new LoginToApplicationUI(driver);
			fc.utobj().selectDropDownByVisibleText(driver, logintoappui.title,dataSetLead.get("title"));
			fc.utobj().sendKeys(driver, logintoappui.firstName,dataSetLead.get("firstname"));
			fc.utobj().sendKeys(driver, logintoappui.lastName,dataSetLead.get("lastname"));
			fc.utobj().sendKeys(driver, logintoappui.companyname,dataSetLead.get("company"));
			fc.utobj().sendKeys(driver, logintoappui.address,dataSetLead.get("address"));
			fc.utobj().sendKeys(driver, logintoappui.phonenumber,dataSetLead.get("phone"));
			fc.utobj().sendKeys(driver, logintoappui.mobilenumber,dataSetLead.get("mobile"));
			fc.utobj().selectDropDownByVisibleText(driver, logintoappui.corporateuser,dataSetLead.get("corporateuser"));
			String emailid=dataSetLead.get("email");
			String emaiIdd=fc.utobj().generateTestData(emailid)+"@staffex.com";
			fc.utobj().sendKeys(driver, logintoappui.emailid,emaiIdd);
			fc.utobj().selectDropDownByVisibleText(driver,logintoappui.primaryContactMethod,dataSetLead.get("primarycontactmethod"));
			fc.utobj().clickElement(driver,logintoappui.save);
			fc.utobj().printTestStep("Lead Added");
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		}
		catch(Exception e){
			
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}
	@Test(groups = "helloworld119")
	@TestCase(createdOn = "2017-09-18", updatedOn = "2018-09-18", testCaseDescription = "Login to the Application", testCaseId = "Login")
	public void loginuser16() throws Exception
	{
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestDatawithsqllite("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		
		try {
			driver.get("https://automation9.franconnectqa.net/fc");
			
			driver.findElement(By.xpath("//*[@id='user_id']")).sendKeys(dataSet.get("username"));
			driver.findElement(By.xpath("//*[@id='password']")).sendKeys(dataSet.get("password"));	
			driver.findElement(By.xpath("//*[@id='ulogin']")).click();
			
			CRMLeadsPage pobj = new CRMLeadsPage(driver);
			fc.utobj().printTestStep("Navigate To CRM > Leads > Leads Summary");
			
			fc.crm().crm_common().CRMLeadsLnk(driver);
			fc.utobj().printTestStep("Click on Add New");
			fc.utobj().clickElement(driver, pobj.addNew);
			fc.utobj().printTestStep("Click on Lead Link");
			fc.utobj().clickElement(driver, pobj.leadLnk);
			fc.utobj().printTestStep("Add Lead");
			Thread.sleep(30000);
			testCaseId="Leadinfo";
			Map<String, String> dataSetLead = fc.utobj().readTestDatawithsqllite("crm", testCaseId);
			LoginToApplicationUI logintoappui=new LoginToApplicationUI(driver);
			fc.utobj().selectDropDownByVisibleText(driver, logintoappui.title,dataSetLead.get("title"));
			fc.utobj().sendKeys(driver, logintoappui.firstName,dataSetLead.get("firstname"));
			fc.utobj().sendKeys(driver, logintoappui.lastName,dataSetLead.get("lastname"));
			fc.utobj().sendKeys(driver, logintoappui.companyname,dataSetLead.get("company"));
			fc.utobj().sendKeys(driver, logintoappui.address,dataSetLead.get("address"));
			fc.utobj().sendKeys(driver, logintoappui.phonenumber,dataSetLead.get("phone"));
			fc.utobj().sendKeys(driver, logintoappui.mobilenumber,dataSetLead.get("mobile"));
			fc.utobj().selectDropDownByVisibleText(driver, logintoappui.corporateuser,dataSetLead.get("corporateuser"));
			String emailid=dataSetLead.get("email");
			String emaiIdd=fc.utobj().generateTestData(emailid)+"@staffex.com";
			fc.utobj().sendKeys(driver, logintoappui.emailid,emaiIdd);
			fc.utobj().selectDropDownByVisibleText(driver,logintoappui.primaryContactMethod,dataSetLead.get("primarycontactmethod"));
			fc.utobj().clickElement(driver,logintoappui.save);
			fc.utobj().printTestStep("Lead Added");
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		}
		catch(Exception e){
			
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}
	@Test(groups = "helloworld119")
	@TestCase(createdOn = "2017-09-18", updatedOn = "2018-09-18", testCaseDescription = "Login to the Application", testCaseId = "Login")
	public void loginuser17() throws Exception
	{
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestDatawithsqllite("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		
		try {
			driver.get("https://automation9.franconnectqa.net/fc");
			
			driver.findElement(By.xpath("//*[@id='user_id']")).sendKeys(dataSet.get("username"));
			driver.findElement(By.xpath("//*[@id='password']")).sendKeys(dataSet.get("password"));	
			driver.findElement(By.xpath("//*[@id='ulogin']")).click();
			
			CRMLeadsPage pobj = new CRMLeadsPage(driver);
			fc.utobj().printTestStep("Navigate To CRM > Leads > Leads Summary");
			
			fc.crm().crm_common().CRMLeadsLnk(driver);
			fc.utobj().printTestStep("Click on Add New");
			fc.utobj().clickElement(driver, pobj.addNew);
			fc.utobj().printTestStep("Click on Lead Link");
			fc.utobj().clickElement(driver, pobj.leadLnk);
			fc.utobj().printTestStep("Add Lead");
			Thread.sleep(30000);
			testCaseId="Leadinfo";
			Map<String, String> dataSetLead = fc.utobj().readTestDatawithsqllite("crm", testCaseId);
			LoginToApplicationUI logintoappui=new LoginToApplicationUI(driver);
			fc.utobj().selectDropDownByVisibleText(driver, logintoappui.title,dataSetLead.get("title"));
			fc.utobj().sendKeys(driver, logintoappui.firstName,dataSetLead.get("firstname"));
			fc.utobj().sendKeys(driver, logintoappui.lastName,dataSetLead.get("lastname"));
			fc.utobj().sendKeys(driver, logintoappui.companyname,dataSetLead.get("company"));
			fc.utobj().sendKeys(driver, logintoappui.address,dataSetLead.get("address"));
			fc.utobj().sendKeys(driver, logintoappui.phonenumber,dataSetLead.get("phone"));
			fc.utobj().sendKeys(driver, logintoappui.mobilenumber,dataSetLead.get("mobile"));
			fc.utobj().selectDropDownByVisibleText(driver, logintoappui.corporateuser,dataSetLead.get("corporateuser"));
			String emailid=dataSetLead.get("email");
			String emaiIdd=fc.utobj().generateTestData(emailid)+"@staffex.com";
			fc.utobj().sendKeys(driver, logintoappui.emailid,emaiIdd);
			fc.utobj().selectDropDownByVisibleText(driver,logintoappui.primaryContactMethod,dataSetLead.get("primarycontactmethod"));
			fc.utobj().clickElement(driver,logintoappui.save);
			fc.utobj().printTestStep("Lead Added");
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		}
		catch(Exception e){
			
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}
	@Test(groups = "helloworld119")
	@TestCase(createdOn = "2017-09-18", updatedOn = "2018-09-18", testCaseDescription = "Login to the Application", testCaseId = "Login")
	public void loginuser18() throws Exception
	{
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestDatawithsqllite("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		
		try {
			driver.get("https://automation9.franconnectqa.net/fc");
			
			driver.findElement(By.xpath("//*[@id='user_id']")).sendKeys(dataSet.get("username"));
			driver.findElement(By.xpath("//*[@id='password']")).sendKeys(dataSet.get("password"));	
			driver.findElement(By.xpath("//*[@id='ulogin']")).click();
			
			CRMLeadsPage pobj = new CRMLeadsPage(driver);
			fc.utobj().printTestStep("Navigate To CRM > Leads > Leads Summary");
			
			fc.crm().crm_common().CRMLeadsLnk(driver);
			fc.utobj().printTestStep("Click on Add New");
			fc.utobj().clickElement(driver, pobj.addNew);
			fc.utobj().printTestStep("Click on Lead Link");
			fc.utobj().clickElement(driver, pobj.leadLnk);
			fc.utobj().printTestStep("Add Lead");
			Thread.sleep(30000);
			testCaseId="Leadinfo";
			Map<String, String> dataSetLead = fc.utobj().readTestDatawithsqllite("crm", testCaseId);
			LoginToApplicationUI logintoappui=new LoginToApplicationUI(driver);
			fc.utobj().selectDropDownByVisibleText(driver, logintoappui.title,dataSetLead.get("title"));
			fc.utobj().sendKeys(driver, logintoappui.firstName,dataSetLead.get("firstname"));
			fc.utobj().sendKeys(driver, logintoappui.lastName,dataSetLead.get("lastname"));
			fc.utobj().sendKeys(driver, logintoappui.companyname,dataSetLead.get("company"));
			fc.utobj().sendKeys(driver, logintoappui.address,dataSetLead.get("address"));
			fc.utobj().sendKeys(driver, logintoappui.phonenumber,dataSetLead.get("phone"));
			fc.utobj().sendKeys(driver, logintoappui.mobilenumber,dataSetLead.get("mobile"));
			fc.utobj().selectDropDownByVisibleText(driver, logintoappui.corporateuser,dataSetLead.get("corporateuser"));
			String emailid=dataSetLead.get("email");
			String emaiIdd=fc.utobj().generateTestData(emailid)+"@staffex.com";
			fc.utobj().sendKeys(driver, logintoappui.emailid,emaiIdd);
			fc.utobj().selectDropDownByVisibleText(driver,logintoappui.primaryContactMethod,dataSetLead.get("primarycontactmethod"));
			fc.utobj().clickElement(driver,logintoappui.save);
			fc.utobj().printTestStep("Lead Added");
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		}
		catch(Exception e){
			
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}
	@Test(groups = "helloworld119")
	@TestCase(createdOn = "2017-09-18", updatedOn = "2018-09-18", testCaseDescription = "Login to the Application", testCaseId = "Login")
	public void loginuser19() throws Exception
	{
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestDatawithsqllite("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		
		try {
			driver.get("https://automation9.franconnectqa.net/fc");
			
			driver.findElement(By.xpath("//*[@id='user_id']")).sendKeys(dataSet.get("username"));
			driver.findElement(By.xpath("//*[@id='password']")).sendKeys(dataSet.get("password"));	
			driver.findElement(By.xpath("//*[@id='ulogin']")).click();
			
			CRMLeadsPage pobj = new CRMLeadsPage(driver);
			fc.utobj().printTestStep("Navigate To CRM > Leads > Leads Summary");
			
			fc.crm().crm_common().CRMLeadsLnk(driver);
			fc.utobj().printTestStep("Click on Add New");
			fc.utobj().clickElement(driver, pobj.addNew);
			fc.utobj().printTestStep("Click on Lead Link");
			fc.utobj().clickElement(driver, pobj.leadLnk);
			fc.utobj().printTestStep("Add Lead");
			Thread.sleep(30000);
			testCaseId="Leadinfo";
			Map<String, String> dataSetLead = fc.utobj().readTestDatawithsqllite("crm", testCaseId);
			LoginToApplicationUI logintoappui=new LoginToApplicationUI(driver);
			fc.utobj().selectDropDownByVisibleText(driver, logintoappui.title,dataSetLead.get("title"));
			fc.utobj().sendKeys(driver, logintoappui.firstName,dataSetLead.get("firstname"));
			fc.utobj().sendKeys(driver, logintoappui.lastName,dataSetLead.get("lastname"));
			fc.utobj().sendKeys(driver, logintoappui.companyname,dataSetLead.get("company"));
			fc.utobj().sendKeys(driver, logintoappui.address,dataSetLead.get("address"));
			fc.utobj().sendKeys(driver, logintoappui.phonenumber,dataSetLead.get("phone"));
			fc.utobj().sendKeys(driver, logintoappui.mobilenumber,dataSetLead.get("mobile"));
			fc.utobj().selectDropDownByVisibleText(driver, logintoappui.corporateuser,dataSetLead.get("corporateuser"));
			String emailid=dataSetLead.get("email");
			String emaiIdd=fc.utobj().generateTestData(emailid)+"@staffex.com";
			fc.utobj().sendKeys(driver, logintoappui.emailid,emaiIdd);
			fc.utobj().selectDropDownByVisibleText(driver,logintoappui.primaryContactMethod,dataSetLead.get("primarycontactmethod"));
			fc.utobj().clickElement(driver,logintoappui.save);
			fc.utobj().printTestStep("Lead Added");
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		}
		catch(Exception e){
			
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}
	@Test(groups = "helloworld119")
	@TestCase(createdOn = "2017-09-18", updatedOn = "2018-09-18", testCaseDescription = "Login to the Application", testCaseId = "Login")
	public void loginuser20() throws Exception
	{
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestDatawithsqllite("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		
		try {
			driver.get("https://automation9.franconnectqa.net/fc");
			
			driver.findElement(By.xpath("//*[@id='user_id']")).sendKeys(dataSet.get("username"));
			driver.findElement(By.xpath("//*[@id='password']")).sendKeys(dataSet.get("password"));	
			driver.findElement(By.xpath("//*[@id='ulogin']")).click();
			
			CRMLeadsPage pobj = new CRMLeadsPage(driver);
			fc.utobj().printTestStep("Navigate To CRM > Leads > Leads Summary");
			
			fc.crm().crm_common().CRMLeadsLnk(driver);
			fc.utobj().printTestStep("Click on Add New");
			fc.utobj().clickElement(driver, pobj.addNew);
			fc.utobj().printTestStep("Click on Lead Link");
			fc.utobj().clickElement(driver, pobj.leadLnk);
			fc.utobj().printTestStep("Add Lead");
			Thread.sleep(30000);
			testCaseId="Leadinfo";
			Map<String, String> dataSetLead = fc.utobj().readTestDatawithsqllite("crm", testCaseId);
			LoginToApplicationUI logintoappui=new LoginToApplicationUI(driver);
			fc.utobj().selectDropDownByVisibleText(driver, logintoappui.title,dataSetLead.get("title"));
			fc.utobj().sendKeys(driver, logintoappui.firstName,dataSetLead.get("firstname"));
			fc.utobj().sendKeys(driver, logintoappui.lastName,dataSetLead.get("lastname"));
			fc.utobj().sendKeys(driver, logintoappui.companyname,dataSetLead.get("company"));
			fc.utobj().sendKeys(driver, logintoappui.address,dataSetLead.get("address"));
			fc.utobj().sendKeys(driver, logintoappui.phonenumber,dataSetLead.get("phone"));
			fc.utobj().sendKeys(driver, logintoappui.mobilenumber,dataSetLead.get("mobile"));
			fc.utobj().selectDropDownByVisibleText(driver, logintoappui.corporateuser,dataSetLead.get("corporateuser"));
			String emailid=dataSetLead.get("email");
			String emaiIdd=fc.utobj().generateTestData(emailid)+"@staffex.com";
			fc.utobj().sendKeys(driver, logintoappui.emailid,emaiIdd);
			fc.utobj().selectDropDownByVisibleText(driver,logintoappui.primaryContactMethod,dataSetLead.get("primarycontactmethod"));
			fc.utobj().clickElement(driver,logintoappui.save);
			fc.utobj().printTestStep("Lead Added");
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		}
		catch(Exception e){
			
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}
	@Test(groups = "helloworld119")
	@TestCase(createdOn = "2017-09-18", updatedOn = "2018-09-18", testCaseDescription = "Login to the Application", testCaseId = "Login")
	public void loginuser21() throws Exception
	{
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestDatawithsqllite("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		
		try {
			driver.get("https://automation9.franconnectqa.net/fc");
			
			driver.findElement(By.xpath("//*[@id='user_id']")).sendKeys(dataSet.get("username"));
			driver.findElement(By.xpath("//*[@id='password']")).sendKeys(dataSet.get("password"));	
			driver.findElement(By.xpath("//*[@id='ulogin']")).click();
			
			CRMLeadsPage pobj = new CRMLeadsPage(driver);
			fc.utobj().printTestStep("Navigate To CRM > Leads > Leads Summary");
			
			fc.crm().crm_common().CRMLeadsLnk(driver);
			fc.utobj().printTestStep("Click on Add New");
			fc.utobj().clickElement(driver, pobj.addNew);
			fc.utobj().printTestStep("Click on Lead Link");
			fc.utobj().clickElement(driver, pobj.leadLnk);
			fc.utobj().printTestStep("Add Lead");
			Thread.sleep(30000);
			testCaseId="Leadinfo";
			Map<String, String> dataSetLead = fc.utobj().readTestDatawithsqllite("crm", testCaseId);
			LoginToApplicationUI logintoappui=new LoginToApplicationUI(driver);
			fc.utobj().selectDropDownByVisibleText(driver, logintoappui.title,dataSetLead.get("title"));
			fc.utobj().sendKeys(driver, logintoappui.firstName,dataSetLead.get("firstname"));
			fc.utobj().sendKeys(driver, logintoappui.lastName,dataSetLead.get("lastname"));
			fc.utobj().sendKeys(driver, logintoappui.companyname,dataSetLead.get("company"));
			fc.utobj().sendKeys(driver, logintoappui.address,dataSetLead.get("address"));
			fc.utobj().sendKeys(driver, logintoappui.phonenumber,dataSetLead.get("phone"));
			fc.utobj().sendKeys(driver, logintoappui.mobilenumber,dataSetLead.get("mobile"));
			fc.utobj().selectDropDownByVisibleText(driver, logintoappui.corporateuser,dataSetLead.get("corporateuser"));
			String emailid=dataSetLead.get("email");
			String emaiIdd=fc.utobj().generateTestData(emailid)+"@staffex.com";
			fc.utobj().sendKeys(driver, logintoappui.emailid,emaiIdd);
			fc.utobj().selectDropDownByVisibleText(driver,logintoappui.primaryContactMethod,dataSetLead.get("primarycontactmethod"));
			fc.utobj().clickElement(driver,logintoappui.save);
			fc.utobj().printTestStep("Lead Added");
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		}
		catch(Exception e){
			
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}
	@Test(groups = "helloworld119")
	@TestCase(createdOn = "2017-09-18", updatedOn = "2018-09-18", testCaseDescription = "Login to the Application", testCaseId = "Login")
	public void loginuser22() throws Exception
	{
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestDatawithsqllite("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		
		try {
			driver.get("https://automation9.franconnectqa.net/fc");
			
			driver.findElement(By.xpath("//*[@id='user_id']")).sendKeys(dataSet.get("username"));
			driver.findElement(By.xpath("//*[@id='password']")).sendKeys(dataSet.get("password"));	
			driver.findElement(By.xpath("//*[@id='ulogin']")).click();
			
			CRMLeadsPage pobj = new CRMLeadsPage(driver);
			fc.utobj().printTestStep("Navigate To CRM > Leads > Leads Summary");
			
			fc.crm().crm_common().CRMLeadsLnk(driver);
			fc.utobj().printTestStep("Click on Add New");
			fc.utobj().clickElement(driver, pobj.addNew);
			fc.utobj().printTestStep("Click on Lead Link");
			fc.utobj().clickElement(driver, pobj.leadLnk);
			fc.utobj().printTestStep("Add Lead");
			Thread.sleep(30000);
			testCaseId="Leadinfo";
			Map<String, String> dataSetLead = fc.utobj().readTestDatawithsqllite("crm", testCaseId);
			LoginToApplicationUI logintoappui=new LoginToApplicationUI(driver);
			fc.utobj().selectDropDownByVisibleText(driver, logintoappui.title,dataSetLead.get("title"));
			fc.utobj().sendKeys(driver, logintoappui.firstName,dataSetLead.get("firstname"));
			fc.utobj().sendKeys(driver, logintoappui.lastName,dataSetLead.get("lastname"));
			fc.utobj().sendKeys(driver, logintoappui.companyname,dataSetLead.get("company"));
			fc.utobj().sendKeys(driver, logintoappui.address,dataSetLead.get("address"));
			fc.utobj().sendKeys(driver, logintoappui.phonenumber,dataSetLead.get("phone"));
			fc.utobj().sendKeys(driver, logintoappui.mobilenumber,dataSetLead.get("mobile"));
			fc.utobj().selectDropDownByVisibleText(driver, logintoappui.corporateuser,dataSetLead.get("corporateuser"));
			String emailid=dataSetLead.get("email");
			String emaiIdd=fc.utobj().generateTestData(emailid)+"@staffex.com";
			fc.utobj().sendKeys(driver, logintoappui.emailid,emaiIdd);
			fc.utobj().selectDropDownByVisibleText(driver,logintoappui.primaryContactMethod,dataSetLead.get("primarycontactmethod"));
			fc.utobj().clickElement(driver,logintoappui.save);
			fc.utobj().printTestStep("Lead Added");
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		}
		catch(Exception e){
			
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}
	@Test(groups = "helloworld119")
	@TestCase(createdOn = "2017-09-18", updatedOn = "2018-09-18", testCaseDescription = "Login to the Application", testCaseId = "Login")
	public void loginuser23() throws Exception
	{
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestDatawithsqllite("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		
		try {
			driver.get("https://automation9.franconnectqa.net/fc");
			
			driver.findElement(By.xpath("//*[@id='user_id']")).sendKeys(dataSet.get("username"));
			driver.findElement(By.xpath("//*[@id='password']")).sendKeys(dataSet.get("password"));	
			driver.findElement(By.xpath("//*[@id='ulogin']")).click();
			
			CRMLeadsPage pobj = new CRMLeadsPage(driver);
			fc.utobj().printTestStep("Navigate To CRM > Leads > Leads Summary");
			
			fc.crm().crm_common().CRMLeadsLnk(driver);
			fc.utobj().printTestStep("Click on Add New");
			fc.utobj().clickElement(driver, pobj.addNew);
			fc.utobj().printTestStep("Click on Lead Link");
			fc.utobj().clickElement(driver, pobj.leadLnk);
			fc.utobj().printTestStep("Add Lead");
			Thread.sleep(30000);
			testCaseId="Leadinfo";
			Map<String, String> dataSetLead = fc.utobj().readTestDatawithsqllite("crm", testCaseId);
			LoginToApplicationUI logintoappui=new LoginToApplicationUI(driver);
			fc.utobj().selectDropDownByVisibleText(driver, logintoappui.title,dataSetLead.get("title"));
			fc.utobj().sendKeys(driver, logintoappui.firstName,dataSetLead.get("firstname"));
			fc.utobj().sendKeys(driver, logintoappui.lastName,dataSetLead.get("lastname"));
			fc.utobj().sendKeys(driver, logintoappui.companyname,dataSetLead.get("company"));
			fc.utobj().sendKeys(driver, logintoappui.address,dataSetLead.get("address"));
			fc.utobj().sendKeys(driver, logintoappui.phonenumber,dataSetLead.get("phone"));
			fc.utobj().sendKeys(driver, logintoappui.mobilenumber,dataSetLead.get("mobile"));
			fc.utobj().selectDropDownByVisibleText(driver, logintoappui.corporateuser,dataSetLead.get("corporateuser"));
			String emailid=dataSetLead.get("email");
			String emaiIdd=fc.utobj().generateTestData(emailid)+"@staffex.com";
			fc.utobj().sendKeys(driver, logintoappui.emailid,emaiIdd);
			fc.utobj().selectDropDownByVisibleText(driver,logintoappui.primaryContactMethod,dataSetLead.get("primarycontactmethod"));
			fc.utobj().clickElement(driver,logintoappui.save);
			fc.utobj().printTestStep("Lead Added");
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		}
		catch(Exception e){
			
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}
	@Test(groups = "helloworld119")
	@TestCase(createdOn = "2017-09-18", updatedOn = "2018-09-18", testCaseDescription = "Login to the Application", testCaseId = "Login")
	public void loginuser24() throws Exception
	{
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestDatawithsqllite("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		
		try {
			driver.get("https://automation9.franconnectqa.net/fc");
			
			driver.findElement(By.xpath("//*[@id='user_id']")).sendKeys(dataSet.get("username"));
			driver.findElement(By.xpath("//*[@id='password']")).sendKeys(dataSet.get("password"));	
			driver.findElement(By.xpath("//*[@id='ulogin']")).click();
			
			CRMLeadsPage pobj = new CRMLeadsPage(driver);
			fc.utobj().printTestStep("Navigate To CRM > Leads > Leads Summary");
			
			fc.crm().crm_common().CRMLeadsLnk(driver);
			fc.utobj().printTestStep("Click on Add New");
			fc.utobj().clickElement(driver, pobj.addNew);
			fc.utobj().printTestStep("Click on Lead Link");
			fc.utobj().clickElement(driver, pobj.leadLnk);
			fc.utobj().printTestStep("Add Lead");
			Thread.sleep(30000);
			testCaseId="Leadinfo";
			Map<String, String> dataSetLead = fc.utobj().readTestDatawithsqllite("crm", testCaseId);
			LoginToApplicationUI logintoappui=new LoginToApplicationUI(driver);
			fc.utobj().selectDropDownByVisibleText(driver, logintoappui.title,dataSetLead.get("title"));
			fc.utobj().sendKeys(driver, logintoappui.firstName,dataSetLead.get("firstname"));
			fc.utobj().sendKeys(driver, logintoappui.lastName,dataSetLead.get("lastname"));
			fc.utobj().sendKeys(driver, logintoappui.companyname,dataSetLead.get("company"));
			fc.utobj().sendKeys(driver, logintoappui.address,dataSetLead.get("address"));
			fc.utobj().sendKeys(driver, logintoappui.phonenumber,dataSetLead.get("phone"));
			fc.utobj().sendKeys(driver, logintoappui.mobilenumber,dataSetLead.get("mobile"));
			fc.utobj().selectDropDownByVisibleText(driver, logintoappui.corporateuser,dataSetLead.get("corporateuser"));
			String emailid=dataSetLead.get("email");
			String emaiIdd=fc.utobj().generateTestData(emailid)+"@staffex.com";
			fc.utobj().sendKeys(driver, logintoappui.emailid,emaiIdd);
			fc.utobj().selectDropDownByVisibleText(driver,logintoappui.primaryContactMethod,dataSetLead.get("primarycontactmethod"));
			fc.utobj().clickElement(driver,logintoappui.save);
			fc.utobj().printTestStep("Lead Added");
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		}
		catch(Exception e){
			
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}
	@Test(groups = "helloworld119")
	@TestCase(createdOn = "2017-09-18", updatedOn = "2018-09-18", testCaseDescription = "Login to the Application", testCaseId = "Login")
	public void loginuser25() throws Exception
	{
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestDatawithsqllite("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		
		try {
			driver.get("https://automation9.franconnectqa.net/fc");
			
			driver.findElement(By.xpath("//*[@id='user_id']")).sendKeys(dataSet.get("username"));
			driver.findElement(By.xpath("//*[@id='password']")).sendKeys(dataSet.get("password"));	
			driver.findElement(By.xpath("//*[@id='ulogin']")).click();
			
			CRMLeadsPage pobj = new CRMLeadsPage(driver);
			fc.utobj().printTestStep("Navigate To CRM > Leads > Leads Summary");
			
			fc.crm().crm_common().CRMLeadsLnk(driver);
			fc.utobj().printTestStep("Click on Add New");
			fc.utobj().clickElement(driver, pobj.addNew);
			fc.utobj().printTestStep("Click on Lead Link");
			fc.utobj().clickElement(driver, pobj.leadLnk);
			fc.utobj().printTestStep("Add Lead");
			Thread.sleep(30000);
			testCaseId="Leadinfo";
			Map<String, String> dataSetLead = fc.utobj().readTestDatawithsqllite("crm", testCaseId);
			LoginToApplicationUI logintoappui=new LoginToApplicationUI(driver);
			fc.utobj().selectDropDownByVisibleText(driver, logintoappui.title,dataSetLead.get("title"));
			fc.utobj().sendKeys(driver, logintoappui.firstName,dataSetLead.get("firstname"));
			fc.utobj().sendKeys(driver, logintoappui.lastName,dataSetLead.get("lastname"));
			fc.utobj().sendKeys(driver, logintoappui.companyname,dataSetLead.get("company"));
			fc.utobj().sendKeys(driver, logintoappui.address,dataSetLead.get("address"));
			fc.utobj().sendKeys(driver, logintoappui.phonenumber,dataSetLead.get("phone"));
			fc.utobj().sendKeys(driver, logintoappui.mobilenumber,dataSetLead.get("mobile"));
			fc.utobj().selectDropDownByVisibleText(driver, logintoappui.corporateuser,dataSetLead.get("corporateuser"));
			String emailid=dataSetLead.get("email");
			String emaiIdd=fc.utobj().generateTestData(emailid)+"@staffex.com";
			fc.utobj().sendKeys(driver, logintoappui.emailid,emaiIdd);
			fc.utobj().selectDropDownByVisibleText(driver,logintoappui.primaryContactMethod,dataSetLead.get("primarycontactmethod"));
			fc.utobj().clickElement(driver,logintoappui.save);
			fc.utobj().printTestStep("Lead Added");
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		}
		catch(Exception e){
			
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}
	@Test(groups = "helloworld119")
	@TestCase(createdOn = "2017-09-18", updatedOn = "2018-09-18", testCaseDescription = "Login to the Application", testCaseId = "Login")
	public void loginuser26() throws Exception
	{
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestDatawithsqllite("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		
		try {
			driver.get("https://automation9.franconnectqa.net/fc");
			
			driver.findElement(By.xpath("//*[@id='user_id']")).sendKeys(dataSet.get("username"));
			driver.findElement(By.xpath("//*[@id='password']")).sendKeys(dataSet.get("password"));	
			driver.findElement(By.xpath("//*[@id='ulogin']")).click();
			
			CRMLeadsPage pobj = new CRMLeadsPage(driver);
			fc.utobj().printTestStep("Navigate To CRM > Leads > Leads Summary");
			
			fc.crm().crm_common().CRMLeadsLnk(driver);
			fc.utobj().printTestStep("Click on Add New");
			fc.utobj().clickElement(driver, pobj.addNew);
			fc.utobj().printTestStep("Click on Lead Link");
			fc.utobj().clickElement(driver, pobj.leadLnk);
			fc.utobj().printTestStep("Add Lead");
			Thread.sleep(30000);
			testCaseId="Leadinfo";
			Map<String, String> dataSetLead = fc.utobj().readTestDatawithsqllite("crm", testCaseId);
			LoginToApplicationUI logintoappui=new LoginToApplicationUI(driver);
			fc.utobj().selectDropDownByVisibleText(driver, logintoappui.title,dataSetLead.get("title"));
			fc.utobj().sendKeys(driver, logintoappui.firstName,dataSetLead.get("firstname"));
			fc.utobj().sendKeys(driver, logintoappui.lastName,dataSetLead.get("lastname"));
			fc.utobj().sendKeys(driver, logintoappui.companyname,dataSetLead.get("company"));
			fc.utobj().sendKeys(driver, logintoappui.address,dataSetLead.get("address"));
			fc.utobj().sendKeys(driver, logintoappui.phonenumber,dataSetLead.get("phone"));
			fc.utobj().sendKeys(driver, logintoappui.mobilenumber,dataSetLead.get("mobile"));
			fc.utobj().selectDropDownByVisibleText(driver, logintoappui.corporateuser,dataSetLead.get("corporateuser"));
			String emailid=dataSetLead.get("email");
			String emaiIdd=fc.utobj().generateTestData(emailid)+"@staffex.com";
			fc.utobj().sendKeys(driver, logintoappui.emailid,emaiIdd);
			fc.utobj().selectDropDownByVisibleText(driver,logintoappui.primaryContactMethod,dataSetLead.get("primarycontactmethod"));
			fc.utobj().clickElement(driver,logintoappui.save);
			fc.utobj().printTestStep("Lead Added");
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		}
		catch(Exception e){
			
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}
	@Test(groups = "helloworld119")
	@TestCase(createdOn = "2017-09-18", updatedOn = "2018-09-18", testCaseDescription = "Login to the Application", testCaseId = "Login")
	public void loginuser27() throws Exception
	{
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestDatawithsqllite("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		
		try {
			driver.get("https://automation9.franconnectqa.net/fc");
			
			driver.findElement(By.xpath("//*[@id='user_id']")).sendKeys(dataSet.get("username"));
			driver.findElement(By.xpath("//*[@id='password']")).sendKeys(dataSet.get("password"));	
			driver.findElement(By.xpath("//*[@id='ulogin']")).click();
			
			CRMLeadsPage pobj = new CRMLeadsPage(driver);
			fc.utobj().printTestStep("Navigate To CRM > Leads > Leads Summary");
			
			fc.crm().crm_common().CRMLeadsLnk(driver);
			fc.utobj().printTestStep("Click on Add New");
			fc.utobj().clickElement(driver, pobj.addNew);
			fc.utobj().printTestStep("Click on Lead Link");
			fc.utobj().clickElement(driver, pobj.leadLnk);
			fc.utobj().printTestStep("Add Lead");
			Thread.sleep(30000);
			testCaseId="Leadinfo";
			Map<String, String> dataSetLead = fc.utobj().readTestDatawithsqllite("crm", testCaseId);
			LoginToApplicationUI logintoappui=new LoginToApplicationUI(driver);
			fc.utobj().selectDropDownByVisibleText(driver, logintoappui.title,dataSetLead.get("title"));
			fc.utobj().sendKeys(driver, logintoappui.firstName,dataSetLead.get("firstname"));
			fc.utobj().sendKeys(driver, logintoappui.lastName,dataSetLead.get("lastname"));
			fc.utobj().sendKeys(driver, logintoappui.companyname,dataSetLead.get("company"));
			fc.utobj().sendKeys(driver, logintoappui.address,dataSetLead.get("address"));
			fc.utobj().sendKeys(driver, logintoappui.phonenumber,dataSetLead.get("phone"));
			fc.utobj().sendKeys(driver, logintoappui.mobilenumber,dataSetLead.get("mobile"));
			fc.utobj().selectDropDownByVisibleText(driver, logintoappui.corporateuser,dataSetLead.get("corporateuser"));
			String emailid=dataSetLead.get("email");
			String emaiIdd=fc.utobj().generateTestData(emailid)+"@staffex.com";
			fc.utobj().sendKeys(driver, logintoappui.emailid,emaiIdd);
			fc.utobj().selectDropDownByVisibleText(driver,logintoappui.primaryContactMethod,dataSetLead.get("primarycontactmethod"));
			fc.utobj().clickElement(driver,logintoappui.save);
			fc.utobj().printTestStep("Lead Added");
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		}
		catch(Exception e){
			
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}
	@Test(groups = "helloworld119")
	@TestCase(createdOn = "2017-09-18", updatedOn = "2018-09-18", testCaseDescription = "Login to the Application", testCaseId = "Login")
	public void loginuser28() throws Exception
	{
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestDatawithsqllite("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		
		try {
			driver.get("https://automation9.franconnectqa.net/fc");
			
			driver.findElement(By.xpath("//*[@id='user_id']")).sendKeys(dataSet.get("username"));
			driver.findElement(By.xpath("//*[@id='password']")).sendKeys(dataSet.get("password"));	
			driver.findElement(By.xpath("//*[@id='ulogin']")).click();
			
			CRMLeadsPage pobj = new CRMLeadsPage(driver);
			fc.utobj().printTestStep("Navigate To CRM > Leads > Leads Summary");
			
			fc.crm().crm_common().CRMLeadsLnk(driver);
			fc.utobj().printTestStep("Click on Add New");
			fc.utobj().clickElement(driver, pobj.addNew);
			fc.utobj().printTestStep("Click on Lead Link");
			fc.utobj().clickElement(driver, pobj.leadLnk);
			fc.utobj().printTestStep("Add Lead");
			Thread.sleep(30000);
			testCaseId="Leadinfo";
			Map<String, String> dataSetLead = fc.utobj().readTestDatawithsqllite("crm", testCaseId);
			LoginToApplicationUI logintoappui=new LoginToApplicationUI(driver);
			fc.utobj().selectDropDownByVisibleText(driver, logintoappui.title,dataSetLead.get("title"));
			fc.utobj().sendKeys(driver, logintoappui.firstName,dataSetLead.get("firstname"));
			fc.utobj().sendKeys(driver, logintoappui.lastName,dataSetLead.get("lastname"));
			fc.utobj().sendKeys(driver, logintoappui.companyname,dataSetLead.get("company"));
			fc.utobj().sendKeys(driver, logintoappui.address,dataSetLead.get("address"));
			fc.utobj().sendKeys(driver, logintoappui.phonenumber,dataSetLead.get("phone"));
			fc.utobj().sendKeys(driver, logintoappui.mobilenumber,dataSetLead.get("mobile"));
			fc.utobj().selectDropDownByVisibleText(driver, logintoappui.corporateuser,dataSetLead.get("corporateuser"));
			String emailid=dataSetLead.get("email");
			String emaiIdd=fc.utobj().generateTestData(emailid)+"@staffex.com";
			fc.utobj().sendKeys(driver, logintoappui.emailid,emaiIdd);
			fc.utobj().selectDropDownByVisibleText(driver,logintoappui.primaryContactMethod,dataSetLead.get("primarycontactmethod"));
			fc.utobj().clickElement(driver,logintoappui.save);
			fc.utobj().printTestStep("Lead Added");
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		}
		catch(Exception e){
			
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}
	@Test(groups = "helloworld119")
	@TestCase(createdOn = "2017-09-18", updatedOn = "2018-09-18", testCaseDescription = "Login to the Application", testCaseId = "Login")
	public void loginuser29() throws Exception
	{
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestDatawithsqllite("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		
		try {
			driver.get("https://automation9.franconnectqa.net/fc");
			
			driver.findElement(By.xpath("//*[@id='user_id']")).sendKeys(dataSet.get("username"));
			driver.findElement(By.xpath("//*[@id='password']")).sendKeys(dataSet.get("password"));	
			driver.findElement(By.xpath("//*[@id='ulogin']")).click();
			
			CRMLeadsPage pobj = new CRMLeadsPage(driver);
			fc.utobj().printTestStep("Navigate To CRM > Leads > Leads Summary");
			
			fc.crm().crm_common().CRMLeadsLnk(driver);
			fc.utobj().printTestStep("Click on Add New");
			fc.utobj().clickElement(driver, pobj.addNew);
			fc.utobj().printTestStep("Click on Lead Link");
			fc.utobj().clickElement(driver, pobj.leadLnk);
			fc.utobj().printTestStep("Add Lead");
			Thread.sleep(30000);
			testCaseId="Leadinfo";
			Map<String, String> dataSetLead = fc.utobj().readTestDatawithsqllite("crm", testCaseId);
			LoginToApplicationUI logintoappui=new LoginToApplicationUI(driver);
			fc.utobj().selectDropDownByVisibleText(driver, logintoappui.title,dataSetLead.get("title"));
			fc.utobj().sendKeys(driver, logintoappui.firstName,dataSetLead.get("firstname"));
			fc.utobj().sendKeys(driver, logintoappui.lastName,dataSetLead.get("lastname"));
			fc.utobj().sendKeys(driver, logintoappui.companyname,dataSetLead.get("company"));
			fc.utobj().sendKeys(driver, logintoappui.address,dataSetLead.get("address"));
			fc.utobj().sendKeys(driver, logintoappui.phonenumber,dataSetLead.get("phone"));
			fc.utobj().sendKeys(driver, logintoappui.mobilenumber,dataSetLead.get("mobile"));
			fc.utobj().selectDropDownByVisibleText(driver, logintoappui.corporateuser,dataSetLead.get("corporateuser"));
			String emailid=dataSetLead.get("email");
			String emaiIdd=fc.utobj().generateTestData(emailid)+"@staffex.com";
			fc.utobj().sendKeys(driver, logintoappui.emailid,emaiIdd);
			fc.utobj().selectDropDownByVisibleText(driver,logintoappui.primaryContactMethod,dataSetLead.get("primarycontactmethod"));
			fc.utobj().clickElement(driver,logintoappui.save);
			fc.utobj().printTestStep("Lead Added");
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		}
		catch(Exception e){
			
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}
	@Test(groups = "helloworld119")
	@TestCase(createdOn = "2017-09-18", updatedOn = "2018-09-18", testCaseDescription = "Login to the Application", testCaseId = "Login")
	public void loginuser30() throws Exception
	{
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestDatawithsqllite("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		
		try {
			driver.get("https://automation9.franconnectqa.net/fc");
			
			driver.findElement(By.xpath("//*[@id='user_id']")).sendKeys(dataSet.get("username"));
			driver.findElement(By.xpath("//*[@id='password']")).sendKeys(dataSet.get("password"));	
			driver.findElement(By.xpath("//*[@id='ulogin']")).click();
			
			CRMLeadsPage pobj = new CRMLeadsPage(driver);
			fc.utobj().printTestStep("Navigate To CRM > Leads > Leads Summary");
			
			fc.crm().crm_common().CRMLeadsLnk(driver);
			fc.utobj().printTestStep("Click on Add New");
			fc.utobj().clickElement(driver, pobj.addNew);
			fc.utobj().printTestStep("Click on Lead Link");
			fc.utobj().clickElement(driver, pobj.leadLnk);
			fc.utobj().printTestStep("Add Lead");
			Thread.sleep(30000);
			testCaseId="Leadinfo";
			Map<String, String> dataSetLead = fc.utobj().readTestDatawithsqllite("crm", testCaseId);
			LoginToApplicationUI logintoappui=new LoginToApplicationUI(driver);
			fc.utobj().selectDropDownByVisibleText(driver, logintoappui.title,dataSetLead.get("title"));
			fc.utobj().sendKeys(driver, logintoappui.firstName,dataSetLead.get("firstname"));
			fc.utobj().sendKeys(driver, logintoappui.lastName,dataSetLead.get("lastname"));
			fc.utobj().sendKeys(driver, logintoappui.companyname,dataSetLead.get("company"));
			fc.utobj().sendKeys(driver, logintoappui.address,dataSetLead.get("address"));
			fc.utobj().sendKeys(driver, logintoappui.phonenumber,dataSetLead.get("phone"));
			fc.utobj().sendKeys(driver, logintoappui.mobilenumber,dataSetLead.get("mobile"));
			fc.utobj().selectDropDownByVisibleText(driver, logintoappui.corporateuser,dataSetLead.get("corporateuser"));
			String emailid=dataSetLead.get("email");
			String emaiIdd=fc.utobj().generateTestData(emailid)+"@staffex.com";
			fc.utobj().sendKeys(driver, logintoappui.emailid,emaiIdd);
			fc.utobj().selectDropDownByVisibleText(driver,logintoappui.primaryContactMethod,dataSetLead.get("primarycontactmethod"));
			fc.utobj().clickElement(driver,logintoappui.save);
			fc.utobj().printTestStep("Lead Added");
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		}
		catch(Exception e){
			
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}
	@Test(groups = "helloworld119")
	@TestCase(createdOn = "2017-09-18", updatedOn = "2018-09-18", testCaseDescription = "Login to the Application", testCaseId = "Login")
	public void loginuser31() throws Exception
	{
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestDatawithsqllite("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		
		try {
			driver.get("https://automation9.franconnectqa.net/fc");
			
			driver.findElement(By.xpath("//*[@id='user_id']")).sendKeys(dataSet.get("username"));
			driver.findElement(By.xpath("//*[@id='password']")).sendKeys(dataSet.get("password"));	
			driver.findElement(By.xpath("//*[@id='ulogin']")).click();
			
			CRMLeadsPage pobj = new CRMLeadsPage(driver);
			fc.utobj().printTestStep("Navigate To CRM > Leads > Leads Summary");
			
			fc.crm().crm_common().CRMLeadsLnk(driver);
			fc.utobj().printTestStep("Click on Add New");
			fc.utobj().clickElement(driver, pobj.addNew);
			fc.utobj().printTestStep("Click on Lead Link");
			fc.utobj().clickElement(driver, pobj.leadLnk);
			fc.utobj().printTestStep("Add Lead");
			Thread.sleep(30000);
			testCaseId="Leadinfo";
			Map<String, String> dataSetLead = fc.utobj().readTestDatawithsqllite("crm", testCaseId);
			LoginToApplicationUI logintoappui=new LoginToApplicationUI(driver);
			fc.utobj().selectDropDownByVisibleText(driver, logintoappui.title,dataSetLead.get("title"));
			fc.utobj().sendKeys(driver, logintoappui.firstName,dataSetLead.get("firstname"));
			fc.utobj().sendKeys(driver, logintoappui.lastName,dataSetLead.get("lastname"));
			fc.utobj().sendKeys(driver, logintoappui.companyname,dataSetLead.get("company"));
			fc.utobj().sendKeys(driver, logintoappui.address,dataSetLead.get("address"));
			fc.utobj().sendKeys(driver, logintoappui.phonenumber,dataSetLead.get("phone"));
			fc.utobj().sendKeys(driver, logintoappui.mobilenumber,dataSetLead.get("mobile"));
			fc.utobj().selectDropDownByVisibleText(driver, logintoappui.corporateuser,dataSetLead.get("corporateuser"));
			String emailid=dataSetLead.get("email");
			String emaiIdd=fc.utobj().generateTestData(emailid)+"@staffex.com";
			fc.utobj().sendKeys(driver, logintoappui.emailid,emaiIdd);
			fc.utobj().selectDropDownByVisibleText(driver,logintoappui.primaryContactMethod,dataSetLead.get("primarycontactmethod"));
			fc.utobj().clickElement(driver,logintoappui.save);
			fc.utobj().printTestStep("Lead Added");
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		}
		catch(Exception e){
			
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}
	@Test(groups = "helloworld119")
	@TestCase(createdOn = "2017-09-18", updatedOn = "2018-09-18", testCaseDescription = "Login to the Application", testCaseId = "Login")
	public void loginuser32() throws Exception
	{
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestDatawithsqllite("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		
		try {
			driver.get("https://automation9.franconnectqa.net/fc");
			
			driver.findElement(By.xpath("//*[@id='user_id']")).sendKeys(dataSet.get("username"));
			driver.findElement(By.xpath("//*[@id='password']")).sendKeys(dataSet.get("password"));	
			driver.findElement(By.xpath("//*[@id='ulogin']")).click();
			
			CRMLeadsPage pobj = new CRMLeadsPage(driver);
			fc.utobj().printTestStep("Navigate To CRM > Leads > Leads Summary");
			
			fc.crm().crm_common().CRMLeadsLnk(driver);
			fc.utobj().printTestStep("Click on Add New");
			fc.utobj().clickElement(driver, pobj.addNew);
			fc.utobj().printTestStep("Click on Lead Link");
			fc.utobj().clickElement(driver, pobj.leadLnk);
			fc.utobj().printTestStep("Add Lead");
			Thread.sleep(30000);
			testCaseId="Leadinfo";
			Map<String, String> dataSetLead = fc.utobj().readTestDatawithsqllite("crm", testCaseId);
			LoginToApplicationUI logintoappui=new LoginToApplicationUI(driver);
			fc.utobj().selectDropDownByVisibleText(driver, logintoappui.title,dataSetLead.get("title"));
			fc.utobj().sendKeys(driver, logintoappui.firstName,dataSetLead.get("firstname"));
			fc.utobj().sendKeys(driver, logintoappui.lastName,dataSetLead.get("lastname"));
			fc.utobj().sendKeys(driver, logintoappui.companyname,dataSetLead.get("company"));
			fc.utobj().sendKeys(driver, logintoappui.address,dataSetLead.get("address"));
			fc.utobj().sendKeys(driver, logintoappui.phonenumber,dataSetLead.get("phone"));
			fc.utobj().sendKeys(driver, logintoappui.mobilenumber,dataSetLead.get("mobile"));
			fc.utobj().selectDropDownByVisibleText(driver, logintoappui.corporateuser,dataSetLead.get("corporateuser"));
			String emailid=dataSetLead.get("email");
			String emaiIdd=fc.utobj().generateTestData(emailid)+"@staffex.com";
			fc.utobj().sendKeys(driver, logintoappui.emailid,emaiIdd);
			fc.utobj().selectDropDownByVisibleText(driver,logintoappui.primaryContactMethod,dataSetLead.get("primarycontactmethod"));
			fc.utobj().clickElement(driver,logintoappui.save);
			fc.utobj().printTestStep("Lead Added");
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		}
		catch(Exception e){
			
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}
	@Test(groups = "helloworld119")
	@TestCase(createdOn = "2017-09-18", updatedOn = "2018-09-18", testCaseDescription = "Login to the Application", testCaseId = "Login")
	public void loginuser33() throws Exception
	{
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestDatawithsqllite("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		
		try {
			driver.get("https://automation9.franconnectqa.net/fc");
			
			driver.findElement(By.xpath("//*[@id='user_id']")).sendKeys(dataSet.get("username"));
			driver.findElement(By.xpath("//*[@id='password']")).sendKeys(dataSet.get("password"));	
			driver.findElement(By.xpath("//*[@id='ulogin']")).click();
			
			CRMLeadsPage pobj = new CRMLeadsPage(driver);
			fc.utobj().printTestStep("Navigate To CRM > Leads > Leads Summary");
			
			fc.crm().crm_common().CRMLeadsLnk(driver);
			fc.utobj().printTestStep("Click on Add New");
			fc.utobj().clickElement(driver, pobj.addNew);
			fc.utobj().printTestStep("Click on Lead Link");
			fc.utobj().clickElement(driver, pobj.leadLnk);
			fc.utobj().printTestStep("Add Lead");
			Thread.sleep(30000);
			testCaseId="Leadinfo";
			Map<String, String> dataSetLead = fc.utobj().readTestDatawithsqllite("crm", testCaseId);
			LoginToApplicationUI logintoappui=new LoginToApplicationUI(driver);
			fc.utobj().selectDropDownByVisibleText(driver, logintoappui.title,dataSetLead.get("title"));
			fc.utobj().sendKeys(driver, logintoappui.firstName,dataSetLead.get("firstname"));
			fc.utobj().sendKeys(driver, logintoappui.lastName,dataSetLead.get("lastname"));
			fc.utobj().sendKeys(driver, logintoappui.companyname,dataSetLead.get("company"));
			fc.utobj().sendKeys(driver, logintoappui.address,dataSetLead.get("address"));
			fc.utobj().sendKeys(driver, logintoappui.phonenumber,dataSetLead.get("phone"));
			fc.utobj().sendKeys(driver, logintoappui.mobilenumber,dataSetLead.get("mobile"));
			fc.utobj().selectDropDownByVisibleText(driver, logintoappui.corporateuser,dataSetLead.get("corporateuser"));
			String emailid=dataSetLead.get("email");
			String emaiIdd=fc.utobj().generateTestData(emailid)+"@staffex.com";
			fc.utobj().sendKeys(driver, logintoappui.emailid,emaiIdd);
			fc.utobj().selectDropDownByVisibleText(driver,logintoappui.primaryContactMethod,dataSetLead.get("primarycontactmethod"));
			fc.utobj().clickElement(driver,logintoappui.save);
			fc.utobj().printTestStep("Lead Added");
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		}
		catch(Exception e){
			
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}
	@Test(groups = "helloworld119")
	@TestCase(createdOn = "2017-09-18", updatedOn = "2018-09-18", testCaseDescription = "Login to the Application", testCaseId = "Login")
	public void loginuser34() throws Exception
	{
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestDatawithsqllite("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		
		try {
			driver.get("https://automation9.franconnectqa.net/fc");
			
			driver.findElement(By.xpath("//*[@id='user_id']")).sendKeys(dataSet.get("username"));
			driver.findElement(By.xpath("//*[@id='password']")).sendKeys(dataSet.get("password"));	
			driver.findElement(By.xpath("//*[@id='ulogin']")).click();
			
			CRMLeadsPage pobj = new CRMLeadsPage(driver);
			fc.utobj().printTestStep("Navigate To CRM > Leads > Leads Summary");
			
			fc.crm().crm_common().CRMLeadsLnk(driver);
			fc.utobj().printTestStep("Click on Add New");
			fc.utobj().clickElement(driver, pobj.addNew);
			fc.utobj().printTestStep("Click on Lead Link");
			fc.utobj().clickElement(driver, pobj.leadLnk);
			fc.utobj().printTestStep("Add Lead");
			Thread.sleep(30000);
			testCaseId="Leadinfo";
			Map<String, String> dataSetLead = fc.utobj().readTestDatawithsqllite("crm", testCaseId);
			LoginToApplicationUI logintoappui=new LoginToApplicationUI(driver);
			fc.utobj().selectDropDownByVisibleText(driver, logintoappui.title,dataSetLead.get("title"));
			fc.utobj().sendKeys(driver, logintoappui.firstName,dataSetLead.get("firstname"));
			fc.utobj().sendKeys(driver, logintoappui.lastName,dataSetLead.get("lastname"));
			fc.utobj().sendKeys(driver, logintoappui.companyname,dataSetLead.get("company"));
			fc.utobj().sendKeys(driver, logintoappui.address,dataSetLead.get("address"));
			fc.utobj().sendKeys(driver, logintoappui.phonenumber,dataSetLead.get("phone"));
			fc.utobj().sendKeys(driver, logintoappui.mobilenumber,dataSetLead.get("mobile"));
			fc.utobj().selectDropDownByVisibleText(driver, logintoappui.corporateuser,dataSetLead.get("corporateuser"));
			String emailid=dataSetLead.get("email");
			String emaiIdd=fc.utobj().generateTestData(emailid)+"@staffex.com";
			fc.utobj().sendKeys(driver, logintoappui.emailid,emaiIdd);
			fc.utobj().selectDropDownByVisibleText(driver,logintoappui.primaryContactMethod,dataSetLead.get("primarycontactmethod"));
			fc.utobj().clickElement(driver,logintoappui.save);
			fc.utobj().printTestStep("Lead Added");
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		}
		catch(Exception e){
			
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}
	@Test(groups = "helloworld119")
	@TestCase(createdOn = "2017-09-18", updatedOn = "2018-09-18", testCaseDescription = "Login to the Application", testCaseId = "Login")
	public void loginuser35() throws Exception
	{
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestDatawithsqllite("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		
		try {
			driver.get("https://automation9.franconnectqa.net/fc");
			
			driver.findElement(By.xpath("//*[@id='user_id']")).sendKeys(dataSet.get("username"));
			driver.findElement(By.xpath("//*[@id='password']")).sendKeys(dataSet.get("password"));	
			driver.findElement(By.xpath("//*[@id='ulogin']")).click();
			
			CRMLeadsPage pobj = new CRMLeadsPage(driver);
			fc.utobj().printTestStep("Navigate To CRM > Leads > Leads Summary");
			
			fc.crm().crm_common().CRMLeadsLnk(driver);
			fc.utobj().printTestStep("Click on Add New");
			fc.utobj().clickElement(driver, pobj.addNew);
			fc.utobj().printTestStep("Click on Lead Link");
			fc.utobj().clickElement(driver, pobj.leadLnk);
			fc.utobj().printTestStep("Add Lead");
			Thread.sleep(30000);
			testCaseId="Leadinfo";
			Map<String, String> dataSetLead = fc.utobj().readTestDatawithsqllite("crm", testCaseId);
			LoginToApplicationUI logintoappui=new LoginToApplicationUI(driver);
			fc.utobj().selectDropDownByVisibleText(driver, logintoappui.title,dataSetLead.get("title"));
			fc.utobj().sendKeys(driver, logintoappui.firstName,dataSetLead.get("firstname"));
			fc.utobj().sendKeys(driver, logintoappui.lastName,dataSetLead.get("lastname"));
			fc.utobj().sendKeys(driver, logintoappui.companyname,dataSetLead.get("company"));
			fc.utobj().sendKeys(driver, logintoappui.address,dataSetLead.get("address"));
			fc.utobj().sendKeys(driver, logintoappui.phonenumber,dataSetLead.get("phone"));
			fc.utobj().sendKeys(driver, logintoappui.mobilenumber,dataSetLead.get("mobile"));
			fc.utobj().selectDropDownByVisibleText(driver, logintoappui.corporateuser,dataSetLead.get("corporateuser"));
			String emailid=dataSetLead.get("email");
			String emaiIdd=fc.utobj().generateTestData(emailid)+"@staffex.com";
			fc.utobj().sendKeys(driver, logintoappui.emailid,emaiIdd);
			fc.utobj().selectDropDownByVisibleText(driver,logintoappui.primaryContactMethod,dataSetLead.get("primarycontactmethod"));
			fc.utobj().clickElement(driver,logintoappui.save);
			fc.utobj().printTestStep("Lead Added");
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		}
		catch(Exception e){
			
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}
	@Test(groups = "helloworld119")
	@TestCase(createdOn = "2017-09-18", updatedOn = "2018-09-18", testCaseDescription = "Login to the Application", testCaseId = "Login")
	public void loginuser36() throws Exception
	{
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestDatawithsqllite("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		
		try {
			driver.get("https://automation9.franconnectqa.net/fc");
			
			driver.findElement(By.xpath("//*[@id='user_id']")).sendKeys(dataSet.get("username"));
			driver.findElement(By.xpath("//*[@id='password']")).sendKeys(dataSet.get("password"));	
			driver.findElement(By.xpath("//*[@id='ulogin']")).click();
			
			CRMLeadsPage pobj = new CRMLeadsPage(driver);
			fc.utobj().printTestStep("Navigate To CRM > Leads > Leads Summary");
			
			fc.crm().crm_common().CRMLeadsLnk(driver);
			fc.utobj().printTestStep("Click on Add New");
			fc.utobj().clickElement(driver, pobj.addNew);
			fc.utobj().printTestStep("Click on Lead Link");
			fc.utobj().clickElement(driver, pobj.leadLnk);
			fc.utobj().printTestStep("Add Lead");
			Thread.sleep(30000);
			testCaseId="Leadinfo";
			Map<String, String> dataSetLead = fc.utobj().readTestDatawithsqllite("crm", testCaseId);
			LoginToApplicationUI logintoappui=new LoginToApplicationUI(driver);
			fc.utobj().selectDropDownByVisibleText(driver, logintoappui.title,dataSetLead.get("title"));
			fc.utobj().sendKeys(driver, logintoappui.firstName,dataSetLead.get("firstname"));
			fc.utobj().sendKeys(driver, logintoappui.lastName,dataSetLead.get("lastname"));
			fc.utobj().sendKeys(driver, logintoappui.companyname,dataSetLead.get("company"));
			fc.utobj().sendKeys(driver, logintoappui.address,dataSetLead.get("address"));
			fc.utobj().sendKeys(driver, logintoappui.phonenumber,dataSetLead.get("phone"));
			fc.utobj().sendKeys(driver, logintoappui.mobilenumber,dataSetLead.get("mobile"));
			fc.utobj().selectDropDownByVisibleText(driver, logintoappui.corporateuser,dataSetLead.get("corporateuser"));
			String emailid=dataSetLead.get("email");
			String emaiIdd=fc.utobj().generateTestData(emailid)+"@staffex.com";
			fc.utobj().sendKeys(driver, logintoappui.emailid,emaiIdd);
			fc.utobj().selectDropDownByVisibleText(driver,logintoappui.primaryContactMethod,dataSetLead.get("primarycontactmethod"));
			fc.utobj().clickElement(driver,logintoappui.save);
			fc.utobj().printTestStep("Lead Added");
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		}
		catch(Exception e){
			
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}
	@Test(groups = "helloworld119")
	@TestCase(createdOn = "2017-09-18", updatedOn = "2018-09-18", testCaseDescription = "Login to the Application", testCaseId = "Login")
	public void loginuser37() throws Exception
	{
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestDatawithsqllite("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		
		try {
			driver.get("https://automation9.franconnectqa.net/fc");
			
			driver.findElement(By.xpath("//*[@id='user_id']")).sendKeys(dataSet.get("username"));
			driver.findElement(By.xpath("//*[@id='password']")).sendKeys(dataSet.get("password"));	
			driver.findElement(By.xpath("//*[@id='ulogin']")).click();
			
			CRMLeadsPage pobj = new CRMLeadsPage(driver);
			fc.utobj().printTestStep("Navigate To CRM > Leads > Leads Summary");
			
			fc.crm().crm_common().CRMLeadsLnk(driver);
			fc.utobj().printTestStep("Click on Add New");
			fc.utobj().clickElement(driver, pobj.addNew);
			fc.utobj().printTestStep("Click on Lead Link");
			fc.utobj().clickElement(driver, pobj.leadLnk);
			fc.utobj().printTestStep("Add Lead");
			Thread.sleep(30000);
			testCaseId="Leadinfo";
			Map<String, String> dataSetLead = fc.utobj().readTestDatawithsqllite("crm", testCaseId);
			LoginToApplicationUI logintoappui=new LoginToApplicationUI(driver);
			fc.utobj().selectDropDownByVisibleText(driver, logintoappui.title,dataSetLead.get("title"));
			fc.utobj().sendKeys(driver, logintoappui.firstName,dataSetLead.get("firstname"));
			fc.utobj().sendKeys(driver, logintoappui.lastName,dataSetLead.get("lastname"));
			fc.utobj().sendKeys(driver, logintoappui.companyname,dataSetLead.get("company"));
			fc.utobj().sendKeys(driver, logintoappui.address,dataSetLead.get("address"));
			fc.utobj().sendKeys(driver, logintoappui.phonenumber,dataSetLead.get("phone"));
			fc.utobj().sendKeys(driver, logintoappui.mobilenumber,dataSetLead.get("mobile"));
			fc.utobj().selectDropDownByVisibleText(driver, logintoappui.corporateuser,dataSetLead.get("corporateuser"));
			String emailid=dataSetLead.get("email");
			String emaiIdd=fc.utobj().generateTestData(emailid)+"@staffex.com";
			fc.utobj().sendKeys(driver, logintoappui.emailid,emaiIdd);
			fc.utobj().selectDropDownByVisibleText(driver,logintoappui.primaryContactMethod,dataSetLead.get("primarycontactmethod"));
			fc.utobj().clickElement(driver,logintoappui.save);
			fc.utobj().printTestStep("Lead Added");
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		}
		catch(Exception e){
			
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}
	@Test(groups = "helloworld119")
	@TestCase(createdOn = "2017-09-18", updatedOn = "2018-09-18", testCaseDescription = "Login to the Application", testCaseId = "Login")
	public void loginuser38() throws Exception
	{
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestDatawithsqllite("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		
		try {
			driver.get("https://automation9.franconnectqa.net/fc");
			
			driver.findElement(By.xpath("//*[@id='user_id']")).sendKeys(dataSet.get("username"));
			driver.findElement(By.xpath("//*[@id='password']")).sendKeys(dataSet.get("password"));	
			driver.findElement(By.xpath("//*[@id='ulogin']")).click();
			
			CRMLeadsPage pobj = new CRMLeadsPage(driver);
			fc.utobj().printTestStep("Navigate To CRM > Leads > Leads Summary");
			
			fc.crm().crm_common().CRMLeadsLnk(driver);
			fc.utobj().printTestStep("Click on Add New");
			fc.utobj().clickElement(driver, pobj.addNew);
			fc.utobj().printTestStep("Click on Lead Link");
			fc.utobj().clickElement(driver, pobj.leadLnk);
			fc.utobj().printTestStep("Add Lead");
			Thread.sleep(30000);
			testCaseId="Leadinfo";
			Map<String, String> dataSetLead = fc.utobj().readTestDatawithsqllite("crm", testCaseId);
			LoginToApplicationUI logintoappui=new LoginToApplicationUI(driver);
			fc.utobj().selectDropDownByVisibleText(driver, logintoappui.title,dataSetLead.get("title"));
			fc.utobj().sendKeys(driver, logintoappui.firstName,dataSetLead.get("firstname"));
			fc.utobj().sendKeys(driver, logintoappui.lastName,dataSetLead.get("lastname"));
			fc.utobj().sendKeys(driver, logintoappui.companyname,dataSetLead.get("company"));
			fc.utobj().sendKeys(driver, logintoappui.address,dataSetLead.get("address"));
			fc.utobj().sendKeys(driver, logintoappui.phonenumber,dataSetLead.get("phone"));
			fc.utobj().sendKeys(driver, logintoappui.mobilenumber,dataSetLead.get("mobile"));
			fc.utobj().selectDropDownByVisibleText(driver, logintoappui.corporateuser,dataSetLead.get("corporateuser"));
			String emailid=dataSetLead.get("email");
			String emaiIdd=fc.utobj().generateTestData(emailid)+"@staffex.com";
			fc.utobj().sendKeys(driver, logintoappui.emailid,emaiIdd);
			fc.utobj().selectDropDownByVisibleText(driver,logintoappui.primaryContactMethod,dataSetLead.get("primarycontactmethod"));
			fc.utobj().clickElement(driver,logintoappui.save);
			fc.utobj().printTestStep("Lead Added");
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		}
		catch(Exception e){
			
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}
	@Test(groups = "helloworld119")
	@TestCase(createdOn = "2017-09-18", updatedOn = "2018-09-18", testCaseDescription = "Login to the Application", testCaseId = "Login")
	public void loginuser39() throws Exception
	{
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestDatawithsqllite("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		
		try {
			driver.get("https://automation9.franconnectqa.net/fc");
			
			driver.findElement(By.xpath("//*[@id='user_id']")).sendKeys(dataSet.get("username"));
			driver.findElement(By.xpath("//*[@id='password']")).sendKeys(dataSet.get("password"));	
			driver.findElement(By.xpath("//*[@id='ulogin']")).click();
			
			CRMLeadsPage pobj = new CRMLeadsPage(driver);
			fc.utobj().printTestStep("Navigate To CRM > Leads > Leads Summary");
			
			fc.crm().crm_common().CRMLeadsLnk(driver);
			fc.utobj().printTestStep("Click on Add New");
			fc.utobj().clickElement(driver, pobj.addNew);
			fc.utobj().printTestStep("Click on Lead Link");
			fc.utobj().clickElement(driver, pobj.leadLnk);
			fc.utobj().printTestStep("Add Lead");
			Thread.sleep(30000);
			testCaseId="Leadinfo";
			Map<String, String> dataSetLead = fc.utobj().readTestDatawithsqllite("crm", testCaseId);
			LoginToApplicationUI logintoappui=new LoginToApplicationUI(driver);
			fc.utobj().selectDropDownByVisibleText(driver, logintoappui.title,dataSetLead.get("title"));
			fc.utobj().sendKeys(driver, logintoappui.firstName,dataSetLead.get("firstname"));
			fc.utobj().sendKeys(driver, logintoappui.lastName,dataSetLead.get("lastname"));
			fc.utobj().sendKeys(driver, logintoappui.companyname,dataSetLead.get("company"));
			fc.utobj().sendKeys(driver, logintoappui.address,dataSetLead.get("address"));
			fc.utobj().sendKeys(driver, logintoappui.phonenumber,dataSetLead.get("phone"));
			fc.utobj().sendKeys(driver, logintoappui.mobilenumber,dataSetLead.get("mobile"));
			fc.utobj().selectDropDownByVisibleText(driver, logintoappui.corporateuser,dataSetLead.get("corporateuser"));
			String emailid=dataSetLead.get("email");
			String emaiIdd=fc.utobj().generateTestData(emailid)+"@staffex.com";
			fc.utobj().sendKeys(driver, logintoappui.emailid,emaiIdd);
			fc.utobj().selectDropDownByVisibleText(driver,logintoappui.primaryContactMethod,dataSetLead.get("primarycontactmethod"));
			fc.utobj().clickElement(driver,logintoappui.save);
			fc.utobj().printTestStep("Lead Added");
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		}
		catch(Exception e){
			
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}
	@Test(groups = "helloworld119")
	@TestCase(createdOn = "2017-09-18", updatedOn = "2018-09-18", testCaseDescription = "Login to the Application", testCaseId = "Login")
	public void loginuser40() throws Exception
	{
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestDatawithsqllite("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		
		try {
			driver.get("https://automation9.franconnectqa.net/fc");
			
			driver.findElement(By.xpath("//*[@id='user_id']")).sendKeys(dataSet.get("username"));
			driver.findElement(By.xpath("//*[@id='password']")).sendKeys(dataSet.get("password"));	
			driver.findElement(By.xpath("//*[@id='ulogin']")).click();
			
			CRMLeadsPage pobj = new CRMLeadsPage(driver);
			fc.utobj().printTestStep("Navigate To CRM > Leads > Leads Summary");
			
			fc.crm().crm_common().CRMLeadsLnk(driver);
			fc.utobj().printTestStep("Click on Add New");
			fc.utobj().clickElement(driver, pobj.addNew);
			fc.utobj().printTestStep("Click on Lead Link");
			fc.utobj().clickElement(driver, pobj.leadLnk);
			fc.utobj().printTestStep("Add Lead");
			Thread.sleep(30000);
			testCaseId="Leadinfo";
			Map<String, String> dataSetLead = fc.utobj().readTestDatawithsqllite("crm", testCaseId);
			LoginToApplicationUI logintoappui=new LoginToApplicationUI(driver);
			fc.utobj().selectDropDownByVisibleText(driver, logintoappui.title,dataSetLead.get("title"));
			fc.utobj().sendKeys(driver, logintoappui.firstName,dataSetLead.get("firstname"));
			fc.utobj().sendKeys(driver, logintoappui.lastName,dataSetLead.get("lastname"));
			fc.utobj().sendKeys(driver, logintoappui.companyname,dataSetLead.get("company"));
			fc.utobj().sendKeys(driver, logintoappui.address,dataSetLead.get("address"));
			fc.utobj().sendKeys(driver, logintoappui.phonenumber,dataSetLead.get("phone"));
			fc.utobj().sendKeys(driver, logintoappui.mobilenumber,dataSetLead.get("mobile"));
			fc.utobj().selectDropDownByVisibleText(driver, logintoappui.corporateuser,dataSetLead.get("corporateuser"));
			String emailid=dataSetLead.get("email");
			String emaiIdd=fc.utobj().generateTestData(emailid)+"@staffex.com";
			fc.utobj().sendKeys(driver, logintoappui.emailid,emaiIdd);
			fc.utobj().selectDropDownByVisibleText(driver,logintoappui.primaryContactMethod,dataSetLead.get("primarycontactmethod"));
			fc.utobj().clickElement(driver,logintoappui.save);
			fc.utobj().printTestStep("Lead Added");
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		}
		catch(Exception e){
			
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}		*/
}
