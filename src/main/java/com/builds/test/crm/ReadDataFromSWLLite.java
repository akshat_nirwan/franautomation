package com.builds.test.crm;

import java.sql.DriverManager;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class ReadDataFromSWLLite {
 
    private Connection connect() {
        // SQLite connection string
        String url = "jdbc:sqlite:C:/Users/Vipin/Downloads/sqllite/test.db";
        Connection conn = null;
        try {
            conn = DriverManager.getConnection(url);
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return conn;
    }
 

    public void selectAll(){
        String sql = "SELECT * FROM CRM";
        
        try (Connection conn = this.connect();
             Statement stmt  = conn.createStatement();
             ResultSet rs    = stmt.executeQuery(sql)){
            
            // loop through the result set
            while (rs.next()) {
                System.out.println(rs.getString("FirstName") +  "\t" + 
                                   rs.getString("LastName"));
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }
    
    public static void main(String[] args) {
    	ReadDataFromSWLLite app = new ReadDataFromSWLLite();
        app.selectAll();
    }
 
}