package com.builds.test.crm.RestAPI;

import java.io.IOException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.xml.parsers.ParserConfigurationException;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Test;
import org.xml.sax.SAXException;

import com.builds.uimaps.crm.CRMAccountsPage;
import com.builds.utilities.FranconnectUtil;
import com.builds.utilities.TestCase;

public class CRMAccountRestServiceAction extends BaseRestUtil {
	FranconnectUtil fc = new FranconnectUtil();

	@Test(groups = { "crmRestAPI" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2017-07-03", testCaseDescription = "Create and Delete Account CRM in REST API", testCaseId = "TC_33_CRM_Delete_Account")
	public void CreateAndDeleteAccount() throws IOException, Exception, ParserConfigurationException, SAXException {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		Map<String, String> UniqueKey_Account = new HashMap<String, String>();

		try {
			fc.utobj().printTestStep("Navigating to CRM > RestAPI > Adding Account");
			driver.get(config.get("buildUrl") + DataServicePage.restURL);
			boolean createAccount = createAccount(driver, config, dataSet, testCaseId, UniqueKey_Account);
			if (!createAccount)
				fc.utobj().throwsException("Account not Created in Rest Api " + testCaseId);
			fc.utobj().printTestStep("Navigating to CRM > RestAPI > Deleting Account");
			boolean DeleteAccount = DeleteAccount(driver, config, dataSet, testCaseId, UniqueKey_Account);
			if (!DeleteAccount)
				fc.utobj().throwsException("Account not Deleted in Rest Api " + testCaseId);
			fc.utobj().printTestStep("Navigating to CRM > View Page > Verify Deleting of  Account");
			boolean result = verifyAccount(driver, config, dataSet, testCaseId, UniqueKey_Account);
			if (result)
				fc.utobj().throwsException("Account found on View Page" + testCaseId);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception ex) {
			fc.utobj().quitBrowserOnCatch(driver, ex, testCaseId);
		}
	}

	private boolean DeleteAccount(WebDriver driver, Map<String, String> config, Map<String, String> dataSet,
			String testCaseId, Map<String, String> uniqueKey_Account) {
		try {
			uniqueKey_Account.put("referenceId", DataServicePage.ReferenceStatus_keys.get(testCaseId + "Account Info"));
			boolean createAcc = ActivityPerform(driver, dataSet, config, uniqueKey_Account, DataServicePage.CRM,
					DataServicePage.AccountInfo, DataServicePage.Delete, DataServicePage.deleteDiv, testCaseId);
			return createAcc;
		} catch (Exception ex) {
			return false;
		}
	}

	@Test(groups = { "crmRestAPI" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2017-07-03", testCaseDescription = "Create and Retrieve Account CRM in REST API", testCaseId = "TC_32_CRM_Retrieve_Account")
	public void CreateAndRetrieveAccount() throws IOException, Exception, ParserConfigurationException, SAXException {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		Map<String, String> UniqueKey_Account = new HashMap<String, String>();
		try {
			driver.get(config.get("buildUrl") + DataServicePage.restURL);
			fc.utobj().printTestStep("Navigating to CRM > RestAPI > Adding Account");
			boolean createAccount = createAccount(driver, config, dataSet, testCaseId, UniqueKey_Account);
			if (!createAccount)
				fc.utobj().throwsException("Account not Created in Rest Api " + testCaseId);
			fc.utobj().printTestStep("Navigating to CRM > RestAPI > Retrieving Account");
			boolean RetrieveAccount = RetrieveAccount(driver, config, dataSet, testCaseId, UniqueKey_Account);
			if (!RetrieveAccount)
				fc.utobj().throwsException("Account not Created in Rest Api " + testCaseId);
			fc.utobj().printTestStep("Navigating to CRM > On View Page> Verifying the Account");
			boolean result = verifyAccount(driver, config, dataSet, testCaseId, UniqueKey_Account);
			if (result)
				System.out.println("Account Created");
			else
				fc.utobj().throwsException("Account not found on viewPage" + testCaseId);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	private boolean RetrieveAccount(WebDriver driver, Map<String, String> config, Map<String, String> dataSet,
			String testCaseId, Map<String, String> uniqueKey_Account) {
		try {
			uniqueKey_Account.put("referenceId", DataServicePage.ReferenceStatus_keys.get(testCaseId + "Account Info"));
			boolean createAcc = ActivityPerform(driver, dataSet, config, uniqueKey_Account, DataServicePage.CRM,
					DataServicePage.AccountInfo, DataServicePage.Retrieve, DataServicePage.retrieveDiv, testCaseId);
			return createAcc;
		} catch (Exception ex) {
			return false;
		}
	}

	@Test(groups = { "crmRestAPI123" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2017-07-03", testCaseDescription = "Create And Update Account CRM in REST API", testCaseId = "TC_31_CRM_Update_Account")
	public void CreateAndUpdateAccount() throws IOException, Exception, ParserConfigurationException, SAXException {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		Map<String, String> UniqueKey_Account = new HashMap<String, String>();
		try {
			driver.get(config.get("buildUrl") + DataServicePage.restURL);
			fc.utobj().printTestStep("Navigating to CRM > RestAPI > Adding Account");
			boolean createAccount = createAccount(driver, config, dataSet, testCaseId, UniqueKey_Account);
			if (!createAccount)
				fc.utobj().throwsException("Account not Created in Rest Api " + testCaseId);
			;
			fc.utobj().printTestStep("Navigating to CRM > RestAPI > Updating the Account");
			boolean UpdateAccount = UpdateAccount(driver, config, dataSet, testCaseId, UniqueKey_Account);
			if (!UpdateAccount)
				fc.utobj().throwsException("Account not Created in Rest Api " + testCaseId);
			;
			fc.utobj().printTestStep("Navigating to CRM > On view Page > Verifyng Account");
			boolean result = verifyAccount(driver, config, dataSet, testCaseId, UniqueKey_Account);
			if (result)
				System.out.println("Account Created");
			else
				fc.utobj().throwsException("Accoutn not found on view page" + testCaseId);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	private boolean UpdateAccount(WebDriver driver, Map<String, String> config, Map<String, String> dataSet,
			String testCaseId, Map<String, String> uniqueKey_Account) {
		try {
			uniqueKey_Account.put("accountName", "UpdatedAccName" + fc.utobj().generateRandomNumber());
			uniqueKey_Account.put("referenceId", DataServicePage.ReferenceStatus_keys.get(testCaseId + "Account Info"));
			uniqueKey_Account.put("accountType", "B2B");
			uniqueKey_Account.put("type", "Corporate");
			uniqueKey_Account.put("isParent", "Yes");
			boolean createAcc = ActivityPerform(driver, dataSet, config, uniqueKey_Account, DataServicePage.CRM,
					DataServicePage.AccountInfo, DataServicePage.Update, DataServicePage.updateDiv, testCaseId);
			return createAcc;
		} catch (Exception ex) {
			return false;
		}
	}

	@Test(groups = { "crmRestAPI" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2017-07-03", testCaseDescription = "Create Account CRM  in REST API", testCaseId = "TC_30_CRM_Create_Account")
	public void CreateAccount() throws IOException, Exception, ParserConfigurationException, SAXException {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		Map<String, String> UniqueKey_Account = new HashMap<String, String>();
		try {
			driver.get(config.get("buildUrl") + DataServicePage.restURL);
			fc.utobj().printTestStep("Navigating to CRM > RestAPI > Adding Account");
			boolean createAccount = createAccount(driver, config, dataSet, testCaseId, UniqueKey_Account);
			if (!createAccount)
				fc.utobj().throwsException("Account not Created in Rest Api " + testCaseId);
			fc.utobj().printTestStep("Navigating to CRM > View Page > Verifying Account");
			boolean result = verifyAccount(driver, config, dataSet, testCaseId, UniqueKey_Account);
			if (result)
				System.out.println("Account Created");
			else
				fc.utobj().throwsException("Account not found on view Page" + testCaseId);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	private boolean verifyAccount(WebDriver driver, Map<String, String> config, Map<String, String> dataSet,
			String testCaseId, Map<String, String> UniqueKey_Account) {
		boolean lead = false;

		try {
			fc.loginpage().login(driver);
			fc.crm().crm_common().CRMAccountsLnk(driver);
			CRMAccountsPage pobj = new CRMAccountsPage(driver);
			fc.utobj().sendKeys(driver, pobj.SearchAccounts, UniqueKey_Account.get("accountName"));
			// fc.utobj().clickElement(driver, pobj.showfilters);
			// fc.utobj().selectValFromMultiSelect(driver,
			// pobj.franchiseIdfilter, franchiseID);
			// fc.utobj().clickElement(driver,pobj.serchfilterbtn );
			fc.utobj().clickElement(driver, pobj.exactSearchGroup);
			fc.utobj().clickElement(driver, pobj.searchAccountBtn);
			UniqueKey_Account.remove("referenceId");
			UniqueKey_Account.remove("isParent");
			try {
				fc.utobj().clickPartialLinkText(driver, UniqueKey_Account.get("accountName"));
				List<String> newlist = new LinkedList<String>(UniqueKey_Account.values());
				lead = fc.utobj().assertPageSourceWithMultipleRecords(driver, newlist);
				return lead;
			} catch (Exception e) {
				return lead;
			}

		} catch (Exception e) {
			return lead;
		}

	}

	public boolean createAccount(WebDriver driver, Map<String, String> config, Map<String, String> dataSet,
			String testCaseId, Map<String, String> uniqueKey_Account) {
		try {
			uniqueKey_Account.put("accountName", "AccName" + fc.utobj().generateRandomNumber());
			uniqueKey_Account.put("accountType", "B2B");
			uniqueKey_Account.put("type", "Corporate");
			uniqueKey_Account.put("isParent", "Yes");
			boolean createAcc = ActivityPerform(driver, dataSet, config, uniqueKey_Account, DataServicePage.CRM,
					DataServicePage.AccountInfo, DataServicePage.Create, DataServicePage.createDiv, testCaseId);
			return createAcc;
		} catch (Exception ex) {
			return false;
		}
	}
}
