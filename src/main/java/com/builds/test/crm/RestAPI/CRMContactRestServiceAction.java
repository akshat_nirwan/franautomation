package com.builds.test.crm.RestAPI;

import java.io.IOException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.xml.parsers.ParserConfigurationException;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Test;
import org.xml.sax.SAXException;

import com.builds.uimaps.crm.CRMContactsPage;
import com.builds.utilities.FranconnectUtil;
import com.builds.utilities.TestCase;

public class CRMContactRestServiceAction extends BaseRestUtil {
	FranconnectUtil fc = new FranconnectUtil();
	// test

	@Test(groups = { "crmRestAPI", "crmR=" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2017-07-03", testCaseDescription = "Create and RetrieveContact CRM in REST API", testCaseId = "TC_36_CRM_Retrieve_Contact")
	public void CreateAndRetrieveContact() throws IOException, Exception, ParserConfigurationException, SAXException {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		Map<String, String> UniqueKey_ContactPI = new HashMap<String, String>();
		Map<String, String> UniqueKey_CorpUser = new HashMap<String, String>();

		try {

			driver.get(config.get("buildUrl") + DataServicePage.restURL);
			UserCreation uc = new UserCreation();
			fc.utobj().sleep(8000);
			fc.utobj().printTestStep("Navigating to CRM > RestAPI > Adding Corporate User");
			boolean corpUser = uc.createCorpUser(driver, config, dataSet, testCaseId, UniqueKey_CorpUser);
			if (!corpUser)
				fc.utobj().throwsException("Corporate User not Created in Rest Api " + testCaseId);
			fc.utobj().sleep(8000);
			fc.utobj().printTestStep("Navigating to CRM > RestAPI > Adding Account");
			boolean CreateContact = createContact(driver, config, dataSet, testCaseId, UniqueKey_ContactPI,
					UniqueKey_CorpUser);
			if (!CreateContact)
				fc.utobj().throwsException("Contact not Created in Rest Api " + testCaseId);
			fc.utobj().sleep(8000);
			fc.utobj().printTestStep("Navigating to CRM > RestAPI > Retrieving Account");
			boolean RetrieveContact = retrieveContact(driver, config, dataSet, testCaseId, UniqueKey_ContactPI);
			if (!RetrieveContact)
				fc.utobj().throwsException("Contact not Retrieved in Rest Api " + testCaseId);
			fc.utobj().sleep(8000);
			fc.utobj().printTestStep("Navigating to CRM > On View Page> Verifying Account");
			boolean result = verifyContact(driver, config, dataSet, testCaseId, UniqueKey_ContactPI);
			if (result)
				System.out.println("Contact Created");
			else
				fc.utobj().throwsException("Contact Not Found on View Page" + testCaseId);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception ex) {
			fc.utobj().quitBrowserOnCatch(driver, ex, testCaseId);
		}
	}

	private boolean retrieveContact(WebDriver driver, Map<String, String> config, Map<String, String> dataSet,
			String testCaseId, Map<String, String> UniqueKey_ContactPI) throws Exception {
		UniqueKey_ContactPI.put("referenceId",
				DataServicePage.ReferenceStatus_keys.get(testCaseId + DataServicePage.PrimaryInfo));
		boolean retrieveContact = ActivityPerform(driver, dataSet, config, UniqueKey_ContactPI, DataServicePage.CRM,
				DataServicePage.PrimaryInfo, DataServicePage.Retrieve, DataServicePage.retrieveDiv, testCaseId);
		return retrieveContact;

	}

	@Test(groups = { "crmRestAPI" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2017-07-03", testCaseDescription = "Create And Update Contact CRM  in REST API", testCaseId = "TC_35_CRM_Update_Contact")
	public void CreateAndUpdateContact() throws IOException, Exception, ParserConfigurationException, SAXException {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		Map<String, String> UniqueKey_ContactPI = new HashMap<String, String>();
		Map<String, String> UniqueKey_CorpUser = new HashMap<String, String>();

		try {
			driver.get(config.get("buildUrl") + DataServicePage.restURL);
			UserCreation uc = new UserCreation();
			fc.utobj().sleep(8000);
			fc.utobj().printTestStep("Navigating to CRM > RestAPI > Adding Corporate User");
			boolean corpUser = uc.createCorpUser(driver, config, dataSet, testCaseId, UniqueKey_CorpUser);
			if (!corpUser)
				fc.utobj().throwsException("Corporate User not Created in Rest Api " + testCaseId);
			fc.utobj().sleep(8000);
			fc.utobj().printTestStep("Navigating to CRM > RestAPI > Adding Account");
			boolean CreateContact = createContact(driver, config, dataSet, testCaseId, UniqueKey_ContactPI,
					UniqueKey_CorpUser);
			if (!CreateContact)
				fc.utobj().throwsException("Contact not Created in Rest Api " + testCaseId);
			fc.utobj().sleep(8000);
			fc.utobj().printTestStep("Navigating to CRM > RestAPI > Updating Account");
			boolean updateContact = updateContact(driver, config, dataSet, testCaseId, UniqueKey_ContactPI,
					UniqueKey_CorpUser);
			if (!updateContact)
				fc.utobj().throwsException("Contact not Updated in Rest Api " + testCaseId);
			fc.utobj().sleep(8000);
			fc.utobj().printTestStep("Navigating to CRM > On VIew Page> Verifying Account");
			boolean result = verifyContact(driver, config, dataSet, testCaseId, UniqueKey_ContactPI);
			if (result)
				System.out.println("Contact Created");
			else
				fc.utobj().throwsException("Contact not Created");
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception ex) {
			fc.utobj().quitBrowserOnCatch(driver, ex, testCaseId);
		}
	}

	private boolean updateContact(WebDriver driver, Map<String, String> config, Map<String, String> dataSet,
			String testCaseId, Map<String, String> UniqueKey_ContactPI, Map<String, String> UniqueKey_CorpUser)
			throws Exception {
		UniqueKey_ContactPI.put("contactType", "Contacts");
		UniqueKey_ContactPI.put("referenceId",
				DataServicePage.ReferenceStatus_keys.get(testCaseId + DataServicePage.PrimaryInfo));
		UniqueKey_ContactPI.put("contactFirstName", "AddLeadFName" + fc.utobj().generateRandomNumber());
		UniqueKey_ContactPI.put("contactLastName", "AddLeadLName" + fc.utobj().generateRandomNumber());
		UniqueKey_ContactPI.put("position", "Position");
		UniqueKey_ContactPI.put("address", "Hall of Fame");
		UniqueKey_ContactPI.put("city", "new Delhi");
		UniqueKey_ContactPI.put("country", "USA");
		UniqueKey_ContactPI.put("zipcode", "10084");
		UniqueKey_ContactPI.put("state", "New York");
		UniqueKey_ContactPI.put("cmLeadStatusID", "New");
		UniqueKey_ContactPI.put("primaryContactMethod", "Email");
		UniqueKey_ContactPI.put("bestTimeToContact", "10:30 AM");
		UniqueKey_ContactPI.put("emailIds", "test@franconnect.net");
		UniqueKey_ContactPI.put("birthdate", "10/10/2000");
		UniqueKey_ContactPI.put("sendAutomaticMail", "Yes");
		UniqueKey_ContactPI.put("anniversarydate", fc.utobj().generatefutureDatewithformat("MM/dd/yyyy", 95));
		UniqueKey_ContactPI.put("ownerTypeValue", "CORP");
		UniqueKey_ContactPI.put("contactOwnerID",
				UniqueKey_CorpUser.get("firstname") + " " + UniqueKey_CorpUser.get("lastname"));
		boolean createLeadRemark = ActivityPerform(driver, dataSet, config, UniqueKey_ContactPI, DataServicePage.CRM,
				DataServicePage.PrimaryInfo, DataServicePage.Update, DataServicePage.updateDiv, testCaseId);
		return createLeadRemark;
	}

	private boolean verifyContact(WebDriver driver, Map<String, String> config, Map<String, String> dataSet,
			String testCaseId, Map<String, String> UniqueKey_ContactPI) throws Exception {
		try {
			fc.loginpage().login(driver);
			fc.crm().crm_common().CRMContactsLnk(driver);
			CRMContactsPage CCPT = new CRMContactsPage(driver);
			fc.utobj().sendKeys(driver, CCPT.searchAtGroup,
					UniqueKey_ContactPI.get("contactFirstName") + " " + UniqueKey_ContactPI.get("contactLastName"));
			fc.utobj().clickElement(driver, CCPT.exactSearchGroup);
			fc.utobj().clickElement(driver, CCPT.searchAccountBtn);
			fc.utobj().clickPartialLinkText(driver, UniqueKey_ContactPI.get("contactFirstName"));
			UniqueKey_ContactPI.remove("referenceId");// sendAutomaticMail
			UniqueKey_ContactPI.remove("parentReferenceId");
			UniqueKey_ContactPI.remove("sendAutomaticMail");
			List<String> newlist = new LinkedList<String>(UniqueKey_ContactPI.values());
			boolean lead = fc.utobj().assertPageSourceWithMultipleRecords(driver, newlist);
			if (lead)
				return lead;
			else
				fc.utobj().throwsException("Contact not found on View Page " + testCaseId);
		} catch (Exception ex) {
			fc.utobj().quitBrowserOnCatch(driver, ex, testCaseId);
		}
		return false;
	}

	private boolean createContact(WebDriver driver, Map<String, String> config, Map<String, String> dataSet,
			String testCaseId, Map<String, String> UniqueKey_ContactPI, Map<String, String> UniqueKey_CorpUser)
			throws Exception {
		UniqueKey_ContactPI.put("contactType", "Contacts");
		UniqueKey_ContactPI.put("contactFirstName", "AddLeadFName" + fc.utobj().generateRandomNumber());
		UniqueKey_ContactPI.put("contactLastName", "AddLeadLName" + fc.utobj().generateRandomNumber());
		UniqueKey_ContactPI.put("position", "Position");
		UniqueKey_ContactPI.put("address", "Hall of Fame");
		UniqueKey_ContactPI.put("city", "new Delhi");
		UniqueKey_ContactPI.put("country", "USA");
		UniqueKey_ContactPI.put("zipcode", "10084");
		UniqueKey_ContactPI.put("state", "New York");
		UniqueKey_ContactPI.put("primaryContactMethod", "Email");
		UniqueKey_ContactPI.put("bestTimeToContact", "10:30 AM");
		UniqueKey_ContactPI.put("emailIds", "test@franconnect.net");
		UniqueKey_ContactPI.put("birthdate", "10/10/2000");
		UniqueKey_ContactPI.put("sendAutomaticMail", "Yes");
		UniqueKey_ContactPI.put("anniversarydate", fc.utobj().generatefutureDatewithformat("MM/dd/yyyy", 95));
		UniqueKey_ContactPI.put("ownerTypeValue", "CORP");
		UniqueKey_ContactPI.put("contactOwnerID",
				UniqueKey_CorpUser.get("firstname") + " " + UniqueKey_CorpUser.get("lastname"));
		boolean createLeadRemark = ActivityPerform(driver, dataSet, config, UniqueKey_ContactPI, DataServicePage.CRM,
				DataServicePage.PrimaryInfo, DataServicePage.Create, DataServicePage.createDiv, testCaseId);
		return createLeadRemark;
	}
}
