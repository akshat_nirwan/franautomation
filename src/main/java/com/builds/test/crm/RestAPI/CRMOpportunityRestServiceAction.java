package com.builds.test.crm.RestAPI;

import java.io.IOException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.xml.parsers.ParserConfigurationException;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Test;
import org.xml.sax.SAXException;

import com.builds.uimaps.crm.CRMOpportunitiesPageUI;
import com.builds.utilities.FranconnectUtil;
import com.builds.utilities.TestCase;

public class CRMOpportunityRestServiceAction extends BaseRestUtil {

	FranconnectUtil fc = new FranconnectUtil();

	@Test(groups = { "crmRestAPI" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2017-07-03", testCaseDescription = "Create Opportunity CRM in REST API", testCaseId = "TC_38_CRM_Create_Opportunity")
	public void CreateOpportunity() throws IOException, Exception, ParserConfigurationException, SAXException {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		Map<String, String> UniqueKey_OpportunityPI = new HashMap<String, String>();
		Map<String, String> UniqueKey_CorpUser = new HashMap<String, String>();
		Map<String, String> UniqueKey_Account = new HashMap<String, String>();

		try {
			driver.get(config.get("buildUrl") + DataServicePage.restURL);
			UserCreation uc = new UserCreation();
			fc.utobj().printTestStep("Navigating to CRM > RestAPI > Adding Corporate User");
			boolean corpUser = uc.createCorpUser(driver, config, dataSet, testCaseId, UniqueKey_CorpUser);
			if (!corpUser)
				fc.utobj().throwsException("Corp user not created in rest api " + testCaseId);
			fc.utobj().printTestStep("Navigating to CRM > RestAPI > Adding Opportunity");
			boolean Opportunity = createOpportunity(driver, config, dataSet, testCaseId, UniqueKey_OpportunityPI,
					UniqueKey_CorpUser, UniqueKey_Account);
			if (!Opportunity)
				fc.utobj().throwsException("Opportunity not created in rest api " + testCaseId);
			fc.utobj().printTestStep("Navigating to CRM > On VIew Page> Veryfying Opportunity");
			boolean result = verifyOpportunity(driver, config, dataSet, testCaseId, UniqueKey_OpportunityPI);
			if (result)
				System.out.println("Opportunity Created");
			else
				fc.utobj().throwsException("Opportunity not found in RestApi " + testCaseId);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception ex) {
			fc.utobj().quitBrowserOnCatch(driver, ex, testCaseId);
		}
	}

	private boolean verifyOpportunity(WebDriver driver, Map<String, String> config, Map<String, String> dataSet,
			String testCaseID, Map<String, String> uniqueKey_OpportunityPI) {
		try {
			fc.loginpage().login(driver);
			Thread.sleep(1000);
			fc.crm().crm_common().CRMOpportunitiesLnk(driver);
			Thread.sleep(1000);
			CRMOpportunitiesPageUI CCPT = new CRMOpportunitiesPageUI(driver);
			Thread.sleep(1000);
			fc.utobj().sendKeys(driver, CCPT.SearchBox, uniqueKey_OpportunityPI.get("opportunityName"));
			Thread.sleep(1000);
			fc.utobj().clickElement(driver, CCPT.exactSearchGroup);
			Thread.sleep(1000);
			fc.utobj().clickElement(driver, CCPT.systemSearchBtn);
			Thread.sleep(1000);
			fc.utobj().clickPartialLinkText(driver, uniqueKey_OpportunityPI.get("opportunityName"));
			uniqueKey_OpportunityPI.remove("oppAccountID");
			uniqueKey_OpportunityPI.remove("referenceId");
			List<String> newlist = new LinkedList<String>(uniqueKey_OpportunityPI.values());
			boolean lead = fc.utobj().assertPageSourceWithMultipleRecords(driver, newlist);
			return lead;
		} catch (Exception ex) {
			return false;
		}
	}

	private boolean createOpportunity(WebDriver driver, Map<String, String> config, Map<String, String> dataSet,
			String testCaseId, Map<String, String> uniqueKey_Opportunity, Map<String, String> UniqueKey_CorpUser,
			Map<String, String> UniqueKey_Account) {
		try {

			uniqueKey_Opportunity.put("opportunityName", "Opportunity" + fc.utobj().generateRandomNumber());
			CRMAccountRestServiceAction AccAction = new CRMAccountRestServiceAction();
			AccAction.createAccount(driver, config, dataSet, testCaseId, UniqueKey_Account);
			uniqueKey_Opportunity.put("oppAccountID",
					DataServicePage.ReferenceStatus_keys.get(testCaseId + "Account Info"));
			uniqueKey_Opportunity.put("opportunityOwner",
					UniqueKey_CorpUser.get("firstname") + " " + UniqueKey_CorpUser.get("lastname"));
			uniqueKey_Opportunity.put("stage", "Prospecting");
			uniqueKey_Opportunity.put("salesAmount", "235.00");
			boolean createOpp = ActivityPerform(driver, dataSet, config, uniqueKey_Opportunity, DataServicePage.CRM,
					DataServicePage.OpportunityInformation, DataServicePage.Create, DataServicePage.createDiv,
					testCaseId);
			return createOpp;

		} catch (Exception e) {
			return false;
		}
	}

	@Test(groups = { "crmRestAPI" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2017-07-03", testCaseDescription = "Create and Update Opportunity CRM in REST API", testCaseId = "TC_39_CRM_Update_Opportunity")
	public void CreateAndUpdateOpportunity() throws IOException, Exception, ParserConfigurationException, SAXException {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		Map<String, String> UniqueKey_OpportunityPI = new HashMap<String, String>();
		Map<String, String> UniqueKey_CorpUser = new HashMap<String, String>();
		Map<String, String> UniqueKey_Account = new HashMap<String, String>();
		try {
			driver.get(config.get("buildUrl") + DataServicePage.restURL);
			UserCreation uc = new UserCreation();
			fc.utobj().printTestStep("Navigating to CRM > RestAPI > Adding Corporate User");
			boolean corpUser = uc.createCorpUser(driver, config, dataSet, testCaseId, UniqueKey_CorpUser);
			if (!corpUser)
				fc.utobj().throwsException("Corp user not created in rest api " + testCaseId);
			fc.utobj().printTestStep("Navigating to CRM > RestAPI > Adding Opprtunity");
			boolean Opportunity = createOpportunity(driver, config, dataSet, testCaseId, UniqueKey_OpportunityPI,
					UniqueKey_CorpUser, UniqueKey_Account);
			if (!Opportunity)
				fc.utobj().throwsException("Opportunity not created in rest api " + testCaseId);
			fc.utobj().printTestStep("Navigating to CRM > RestAPI > Updating Opportunity");
			boolean UOpportunity = updateOpportunity(driver, config, dataSet, testCaseId, UniqueKey_OpportunityPI,
					UniqueKey_CorpUser, UniqueKey_Account);
			if (!UOpportunity)
				fc.utobj().throwsException("Opportunity not updated in rest api " + testCaseId);
			fc.utobj().printTestStep("Navigating to CRM > On View Page> Verifying Opportunity");
			boolean result = verifyOpportunity(driver, config, dataSet, testCaseId, UniqueKey_OpportunityPI);
			if (result)
				System.out.println("Opportunity Created");
			else
				System.out.println("Opportunity Not Created");
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception ex) {
			fc.utobj().quitBrowserOnCatch(driver, ex, testCaseId);
		}
	}

	private boolean updateOpportunity(WebDriver driver, Map<String, String> config, Map<String, String> dataSet,
			String testCaseId, Map<String, String> uniqueKey_Opportunity, Map<String, String> UniqueKey_CorpUser,
			Map<String, String> UniqueKey_Account) {
		try {

			uniqueKey_Opportunity.put("opportunityName", "ModifiedOpportunity" + fc.utobj().generateRandomNumber());
			CRMAccountRestServiceAction AccAction = new CRMAccountRestServiceAction();
			AccAction.createAccount(driver, config, dataSet, testCaseId, UniqueKey_Account);
			uniqueKey_Opportunity.put("oppAccountID",
					DataServicePage.ReferenceStatus_keys.get(testCaseId + "Account Info"));
			uniqueKey_Opportunity.put("opportunityOwner",
					UniqueKey_CorpUser.get("firstname") + " " + UniqueKey_CorpUser.get("lastname"));
			uniqueKey_Opportunity.put("stage", "Prospecting");
			uniqueKey_Opportunity.put("salesAmount", "335.00");
			uniqueKey_Opportunity.put("referenceId",
					DataServicePage.ReferenceStatus_keys.get(testCaseId + "Opportunity Information"));
			boolean createOpp = ActivityPerform(driver, dataSet, config, uniqueKey_Opportunity, DataServicePage.CRM,
					DataServicePage.OpportunityInformation, DataServicePage.Update, DataServicePage.updateDiv,
					testCaseId);
			return createOpp;

		} catch (Exception e) {
			return false;
		}
	}

	@Test(groups = { "crmRestAPI" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2017-07-03", testCaseDescription = "Create And Retrieve Opportunity CRM  in REST API", testCaseId = "TC_40_CRM_Retrieve_Opportunity")
	public void CreateAndRetrieveOpportunity()
			throws IOException, Exception, ParserConfigurationException, SAXException {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		Map<String, String> UniqueKey_OpportunityPI = new HashMap<String, String>();
		Map<String, String> UniqueKey_CorpUser = new HashMap<String, String>();
		Map<String, String> UniqueKey_Account = new HashMap<String, String>();
		try {
			driver.get(config.get("buildUrl") + DataServicePage.restURL);
			UserCreation uc = new UserCreation();
			fc.utobj().printTestStep("Navigating to CRM > RestAPI > Adding Corporate User");
			boolean corpUser = uc.createCorpUser(driver, config, dataSet, testCaseId, UniqueKey_CorpUser);
			if (!corpUser)
				fc.utobj().throwsException("Corp user not created in rest api " + testCaseId);
			fc.utobj().printTestStep("Navigating to CRM > RestAPI > Adding Opportunity");
			boolean oppo = createOpportunity(driver, config, dataSet, testCaseId, UniqueKey_OpportunityPI,
					UniqueKey_CorpUser, UniqueKey_Account);
			if (!oppo)
				fc.utobj().throwsException("Opportunity not created in rest api " + testCaseId);
			fc.utobj().printTestStep("Navigating to CRM > RestAPI > Retrieving Opportunity");
			boolean RetriOpp = retrieveOpportunity(driver, config, dataSet, testCaseId, UniqueKey_OpportunityPI);
			if (!RetriOpp)
				fc.utobj().throwsException("Retrieve Opportunity not created in rest api " + testCaseId);
			fc.utobj().printTestStep("Navigating to CRM > On view Page> Verifying Opportunity");
			boolean result = verifyOpportunity(driver, config, dataSet, testCaseId, UniqueKey_OpportunityPI);
			if (result)
				System.out.println("Opportunity Created");
			else
				fc.utobj().throwsException("Opportunity Not Created " + testCaseId);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception ex) {
			fc.utobj().quitBrowserOnCatch(driver, ex, testCaseId);
		}
	}

	private boolean retrieveOpportunity(WebDriver driver, Map<String, String> config, Map<String, String> dataSet,
			String testCaseId, Map<String, String> uniqueKey_Opportunity) throws Exception {
		uniqueKey_Opportunity.put("referenceId",
				DataServicePage.ReferenceStatus_keys.get(testCaseId + "Opportunity Information"));
		boolean RetrieveOpp = ActivityPerform(driver, dataSet, config, uniqueKey_Opportunity, DataServicePage.CRM,
				DataServicePage.OpportunityInformation, DataServicePage.Retrieve, DataServicePage.retrieveDiv,
				testCaseId);
		return RetrieveOpp;
	}

	@Test(groups = { "crmRestAPI" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2017-07-03", testCaseDescription = "Create And Delete Opportunity CRM in REST API", testCaseId = "TC_41_CRM_Delete_Opportunity")
	public void CreateAndDeleteOpportunity() throws IOException, Exception, ParserConfigurationException, SAXException {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		Map<String, String> UniqueKey_OpportunityPI = new HashMap<String, String>();
		Map<String, String> UniqueKey_CorpUser = new HashMap<String, String>();
		Map<String, String> UniqueKey_Account = new HashMap<String, String>();

		try {
			driver.get(config.get("buildUrl") + DataServicePage.restURL);
			UserCreation uc = new UserCreation();
			fc.utobj().printTestStep("Navigating to CRM > RestAPI > Adding CorP user");
			boolean corpUser = uc.createCorpUser(driver, config, dataSet, testCaseId, UniqueKey_CorpUser);
			if (!corpUser)
				fc.utobj().throwsException("Corp user not created in rest api " + testCaseId);
			fc.utobj().printTestStep("Navigating to CRM > RestAPI > Adding Opportunity");
			boolean Opportunity = createOpportunity(driver, config, dataSet, testCaseId, UniqueKey_OpportunityPI,
					UniqueKey_CorpUser, UniqueKey_Account);
			if (!Opportunity)
				fc.utobj().throwsException("Opportunity not created in rest api " + testCaseId);
			fc.utobj().printTestStep("Navigating to CRM > RestAPI > Deleting Opportunity");
			boolean DOpportunity = deleteOpportunity(driver, config, dataSet, testCaseId, UniqueKey_OpportunityPI);
			if (!DOpportunity)
				fc.utobj().throwsException("Opportunity not Deleted in rest api " + testCaseId);
			fc.utobj().printTestStep("Navigating to CRM > RestAPI > Veryfying Opportunity");
			boolean result = verifyOpportunity(driver, config, dataSet, testCaseId, UniqueKey_OpportunityPI);
			if (!result)
				System.out.println("Opportunity Deleted");
			else
				System.out.println("Opportunity Not Deleted");
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception ex) {
			fc.utobj().quitBrowserOnCatch(driver, ex, testCaseId);
		}
	}

	private boolean deleteOpportunity(WebDriver driver, Map<String, String> config, Map<String, String> dataSet,
			String testCaseId, Map<String, String> uniqueKey_Opportunity) throws Exception {
		uniqueKey_Opportunity.put("referenceId",
				DataServicePage.ReferenceStatus_keys.get(testCaseId + "Opportunity Information"));
		boolean DelOpp = ActivityPerform(driver, dataSet, config, uniqueKey_Opportunity, DataServicePage.CRM,
				DataServicePage.OpportunityInformation, DataServicePage.Delete, DataServicePage.deleteDiv, testCaseId);
		return DelOpp;
	}
}
