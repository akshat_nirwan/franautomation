package com.builds.test.crm.RestAPI;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.xml.parsers.ParserConfigurationException;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Test;
import org.xml.sax.SAXException;

import com.builds.test.crm.CRMLeadsPageTest;
import com.builds.uimaps.crm.CRMContactsPage;
import com.builds.uimaps.crm.CRMExportPage;
import com.builds.utilities.FranconnectUtil;
import com.builds.utilities.TestCase;

public class CRMRestServiceAction extends BaseRestUtil {
	FranconnectUtil fc = new FranconnectUtil();

	@Test(groups = { "crmRestAPI" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2017-07-03", testCaseDescription = "Delete Primary Info In CRM in REST API", testCaseId = "TC_26_CRM_DeletePI")
	public void DeleteCrmPrimaryInfo() throws IOException, Exception, ParserConfigurationException, SAXException {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		Map<String, String> UniqueKey_CorpUser = new HashMap<String, String>();
		Map<String, String> UniqueKey_ContactPI = new HashMap<String, String>();

		try {/*
				 * Adding the lead with corporate user account if you want to
				 * add it with regional or franchisee call method already
				 * defined from UserCreation.class and use that one
				 */
			UserCreation uc = new UserCreation();
			fc.utobj().printTestStep("Navigating to CRM > RestAPI > Adding Corp User");
			boolean corpuser = uc.createCorpUser(driver, config, dataSet, testCaseId, UniqueKey_CorpUser);
			if (!corpuser)
				fc.utobj().throwsException("Corp usr hasn't been added in rest api " + testCaseId);
			fc.utobj().printTestStep("Navigating to CRM > RestAPI > Adding Account");
			boolean lead = CreateContactPrimaryInfo(driver, config, dataSet, UniqueKey_ContactPI, UniqueKey_CorpUser,
					testCaseId);
			if (!lead)
				fc.utobj().throwsException("Contact Primary Info not added in rest api " + testCaseId);
			fc.loginpage().login(driver);
			fc.crm().crm_common().CRMContactsLnk(driver);
			CRMContactsPage CCPT = new CRMContactsPage(driver);
			fc.utobj().sendKeys(driver, CCPT.searchAtGroup,
					UniqueKey_ContactPI.get("contactFirstName") + " " + UniqueKey_ContactPI.get("contactLastName"));
			fc.utobj().clickElement(driver, CCPT.exactSearchGroup);
			fc.utobj().clickElement(driver, CCPT.searchAccountBtn);
			UniqueKey_ContactPI.remove("ownerTypeValue");
			UniqueKey_ContactPI.remove("sendAutomaticMail");
			fc.utobj().clickPartialLinkText(driver, UniqueKey_ContactPI.get("contactFirstName"));
			List<String> newlist = new LinkedList<String>(UniqueKey_ContactPI.values());
			lead = fc.utobj().assertPageSourceWithMultipleRecords(driver, newlist);
			if (!lead) {
				fc.utobj().throwsException("Contact Primary Info not found in view page" + testCaseId);
			}
			driver.navigate().to(config.get("buildUrl") + DataServicePage.restURL);
			fc.utobj().printTestStep("Navigating to CRM > RestAPI > Deleting Contact Primay Info");
			lead = DeleteContactPrimaryInfo(driver, config, dataSet, testCaseId, UniqueKey_ContactPI);
			fc.loginpage().login(driver);
			fc.crm().crm_common().CRMContactsLnk(driver);
			fc.utobj().sendKeys(driver, CCPT.searchAtGroup,
					UniqueKey_ContactPI.get("contactFirstName") + " " + UniqueKey_ContactPI.get("contactLastName"));
			fc.utobj().clickElement(driver, CCPT.exactSearchGroup);
			fc.utobj().clickElement(driver, CCPT.searchAccountBtn);
			try {
				fc.utobj().clickPartialLinkText(driver, UniqueKey_ContactPI.get("contactFirstName"));
				fc.utobj().throwsException("Primary info found on View Page" + testCaseId);
			} catch (Exception ex) {

			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception ex) {
			fc.utobj().quitBrowserOnCatch(driver, ex, testCaseId);
		}
	}

	private boolean DeleteContactPrimaryInfo(WebDriver driver, Map<String, String> config, Map<String, String> dataSet,
			String testCaseId, Map<String, String> UniqueKey_ContactPI) throws Exception {

		UniqueKey_ContactPI.put("referenceId",
				DataServicePage.ReferenceStatus_keys.get(testCaseId + DataServicePage.PrimaryInfo));
		boolean createLeadRemark = ActivityPerform(driver, dataSet, config, UniqueKey_ContactPI, DataServicePage.CRM,
				DataServicePage.PrimaryInfo, DataServicePage.Delete, DataServicePage.deleteDiv, testCaseId);
		return createLeadRemark;
	}

	private boolean CreateContactPrimaryInfo(WebDriver driver, Map<String, String> config, Map<String, String> dataSet,
			Map<String, String> UniqueKey_ContactPI, Map<String, String> UniqueKey_CorpUser, String testCaseId)
			throws Exception {
		UniqueKey_ContactPI.put("contactType", "Contacts");
		UniqueKey_ContactPI.put("contactFirstName", "AddLeadFName" + fc.utobj().generateRandomNumber());
		UniqueKey_ContactPI.put("contactLastName", "AddLeadLName" + fc.utobj().generateRandomNumber());
		UniqueKey_ContactPI.put("position", "Position");
		UniqueKey_ContactPI.put("address", "Hall of Fame");
		UniqueKey_ContactPI.put("city", "new Delhi");
		UniqueKey_ContactPI.put("country", "USA");
		UniqueKey_ContactPI.put("zipcode", "10084");
		UniqueKey_ContactPI.put("state", "New York");
		UniqueKey_ContactPI.put("primaryContactMethod", "Email");
		UniqueKey_ContactPI.put("bestTimeToContact", "10:30 AM");
		UniqueKey_ContactPI.put("emailIds", "test@franconnect.net");
		UniqueKey_ContactPI.put("birthdate", "10/10/2000");
		UniqueKey_ContactPI.put("sendAutomaticMail", "Yes");
		UniqueKey_ContactPI.put("anniversarydate", fc.utobj().generatefutureDatewithformat("MM/dd/yyyy", 95));
		UniqueKey_ContactPI.put("ownerTypeValue", "CORP");
		UniqueKey_ContactPI.put("contactOwnerID",
				UniqueKey_CorpUser.get("firstname") + " " + UniqueKey_CorpUser.get("lastname"));
		boolean createLeadRemark = ActivityPerform(driver, dataSet, config, UniqueKey_ContactPI, DataServicePage.CRM,
				DataServicePage.PrimaryInfo, DataServicePage.Create, DataServicePage.createDiv, testCaseId);
		return createLeadRemark;
	}

	@Test(groups = { "crmRestAPI" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2017-07-03", testCaseDescription = "Retrieve and Verify Lead CRM in REST API", testCaseId = "TC_24_CRM_RetrieveLead")
	public void RetrieveAndVarifyLead() throws IOException, Exception, ParserConfigurationException, SAXException {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		Map<String, String> UniqueKey_Lead = new HashMap<String, String>();
		Map<String, String> UniqueKey_CorpUser = new HashMap<String, String>();
		Map<String, String> UniqueKey_LeadCall = new HashMap<String, String>();
		Map<String, String> UniqueKey_LeadRemark = new HashMap<String, String>();
		Map<String, String> UniqueKey_LeadTask = new HashMap<String, String>();

		try {/*
				 * Adding the lead with corporate user account if you want to
				 * add it with regional or franchisee call method already
				 * defined from UserCreation.class and use that one
				 */
			UserCreation uc = new UserCreation();
			fc.utobj().printTestStep("Navigating to CRM > RestAPI > Adding Corp User");
			boolean corp = uc.createCorpUser(driver, config, dataSet, testCaseId, UniqueKey_CorpUser);
			if (!corp)
				fc.utobj().throwsException("Corp User not created in restApi" + testCaseId);
			fc.utobj().printTestStep("Navigating to CRM > RestAPI > Adding Lead");
			boolean lead = CreateLead(driver, config, dataSet, UniqueKey_Lead, UniqueKey_CorpUser, testCaseId);
			if (!lead)
				fc.utobj().throwsException("lead not created in restApi" + testCaseId);
			fc.utobj().printTestStep("Navigating to CRM > RestAPI > Adding Call Remark and Test for Lead");
			boolean AllInfo = FillAllInfo(driver, config, dataSet, testCaseId, UniqueKey_LeadCall, UniqueKey_LeadRemark,
					UniqueKey_LeadTask);
			if (!AllInfo)
				fc.utobj().throwsException("SubModule not created in restApi" + testCaseId);
			fc.utobj().printTestStep("Navigating to CRM > RestAPI > Retriving Lead");
			lead = RetrieveLead(driver, config, dataSet, UniqueKey_LeadTask, testCaseId);
			if (!lead)
				fc.utobj().throwsException("Lead not Retrieved in restApi" + testCaseId);
			// lead=RetrieveAllInfo(driver, config,
			// dataSet,testCaseId,UniqueKey_LeadCall,UniqueKey_LeadRemark,UniqueKey_LeadTask);
			if (!lead)
				fc.utobj().throwsException("SubModule not Retrieved in restApi" + testCaseId);
			fc.loginpage().login(driver);
			fc.crm().crm_common().CRMLeadsLnk(driver);
			CRMLeadsPageTest CLPT = new CRMLeadsPageTest();
			CLPT.systemExactSearch(driver, UniqueKey_Lead.get("leadFirstName"));
			fc.utobj().clickPartialLinkText(driver, UniqueKey_Lead.get("leadFirstName"));
			List<String> list_leads = new ArrayList<String>(UniqueKey_Lead.values());
			lead = fc.utobj().assertPageSourceWithMultipleRecords(driver, list_leads);
			if (lead) {
				System.out.println("Lead is present");
			} else {
				fc.utobj().throwsException("Lead not found on view page " + testCaseId);
			}
			// list_leads.add(DataServicePage.ReferenceStatus_keys.get(testCaseId+DataServicePage.LeadCall));
			// list_leads.add(DataServicePage.ReferenceStatus_keys.get(testCaseId+DataServicePage.LeadRemarks));
			// list_leads.add(DataServicePage.ReferenceStatus_keys.get(testCaseId+DataServicePage.LeadTask));
			// lead = fc.utobj().assertPageSourceWithMultipleRecords(driver,
			// list_leads);
			if (lead) {
				System.out.println("SubModules are present");
			} else {
				fc.utobj().throwsException("Submodules not found on view page " + testCaseId);
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception ex) {
			fc.utobj().quitBrowserOnCatch(driver, ex, testCaseId);
		}
	}

	@Test(groups = { "crmRestAPI" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2017-07-03", testCaseDescription = "Delete AND verify Lead CRM in REST API", testCaseId = "TC_25_CRM_DeleteLead")
	public void DeleteAndVarifyLead() throws IOException, Exception, ParserConfigurationException, SAXException {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		Map<String, String> UniqueKey_Lead = new HashMap<String, String>();
		Map<String, String> UniqueKey_CorpUser = new HashMap<String, String>();
		Map<String, String> UniqueKey_LeadCall = new HashMap<String, String>();
		Map<String, String> UniqueKey_LeadRemark = new HashMap<String, String>();
		Map<String, String> UniqueKey_LeadTask = new HashMap<String, String>();

		try {/*
				 * Adding the lead with corporate user account if you want to
				 * add it with regional or franchisee call method already
				 * defined from UserCreation.class and use that one
				 */
			UserCreation uc = new UserCreation();
			fc.utobj().printTestStep("Navigating to CRM > RestAPI > Adding Corporate User");
			boolean corpuser = uc.createCorpUser(driver, config, dataSet, testCaseId, UniqueKey_CorpUser);
			if (!corpuser)
				fc.utobj().throwsException("Corp user not created in " + testCaseId);
			fc.utobj().printTestStep("Navigating to CRM > RestAPI > Adding Lead");
			boolean lead = CreateLead(driver, config, dataSet, UniqueKey_Lead, UniqueKey_CorpUser, testCaseId);
			if (!lead)
				fc.utobj().throwsException("lead not created in restApi" + testCaseId);
			fc.utobj().printTestStep("Navigating to CRM > RestAPI > Adding Call Remark and Task for lead");
			boolean AllInfo = FillAllInfo(driver, config, dataSet, testCaseId, UniqueKey_LeadCall, UniqueKey_LeadRemark,
					UniqueKey_LeadTask);
			if (!AllInfo)
				fc.utobj().throwsException("SubModule is not created in RestApi " + testCaseId);
			fc.utobj().printTestStep("Navigating to CRM > RestAPI > Delete Call Remark and Task for Lead Id");
			AllInfo = deleteAllInfo(driver, config, dataSet, testCaseId, UniqueKey_LeadCall, UniqueKey_LeadRemark,
					UniqueKey_LeadTask);
			if (!AllInfo)
				fc.utobj().throwsException("SubModule is not Deleted in RestApi " + testCaseId);
			fc.loginpage().login(driver);
			fc.crm().crm_common().CRMLeadsLnk(driver);
			CRMLeadsPageTest CLPT = new CRMLeadsPageTest();
			CLPT.systemExactSearch(driver, UniqueKey_Lead.get("leadFirstName"));
			fc.utobj().clickPartialLinkText(driver, UniqueKey_Lead.get("leadFirstName"));
			List<String> list_leads = new ArrayList<String>(UniqueKey_Lead.values());
			lead = fc.utobj().assertPageSourceWithMultipleRecords(driver, list_leads);
			if (lead) {
				boolean status = false;
				// Validate for Call
				UniqueKey_LeadCall.remove("referenceId");
				UniqueKey_LeadCall.remove("parentReferenceId");
				UniqueKey_LeadCall.remove("timeAdded");
				UniqueKey_Lead.put("calldate", UniqueKey_LeadCall.get("callDate"));
				List<String> listCAll = new ArrayList<String>(UniqueKey_LeadCall.values());
				fc.utobj().printTestStep("Navigating to CRM > on View Page > Validating for Call");
				status = ValidateForExportCall(driver, config, listCAll, DataServicePage.Call, UniqueKey_Lead,
						UniqueKey_LeadCall.get("subject"));
				if (status) {
					fc.utobj().throwsException("Lead Call found on view page");
				}
				// Validate for Task
				UniqueKey_LeadTask.remove("referenceId");
				UniqueKey_LeadTask.remove("parentReferenceId");
				UniqueKey_LeadTask.put("subjecttask", UniqueKey_LeadTask.get("subject"));
				UniqueKey_LeadTask.remove("assignmentTo");
				UniqueKey_LeadTask.remove("priority");
				UniqueKey_LeadTask.remove("date");
				UniqueKey_LeadTask.remove("timelessTask");
				UniqueKey_LeadTask.remove("calendarTask");
				UniqueKey_LeadTask.remove("startTime");
				UniqueKey_LeadTask.remove("endTime");
				UniqueKey_LeadTask.remove("status");
				UniqueKey_LeadTask.remove("reminderTime");
				UniqueKey_LeadTask.remove("reminderDate");
				List<String> listTask = new ArrayList<String>(UniqueKey_LeadTask.values());
				fc.utobj().printTestStep("Navigating to CRM > On View Page > Validating for Task");
				status = ValidateForExportTask(driver, config, listTask, DataServicePage.Call, UniqueKey_Lead,
						UniqueKey_LeadTask.get("subject"));
				if (status) {
					fc.utobj().throwsException("Lead Call found on view page");
				}
			} else {
				fc.utobj().throwsException("Lead is not found on view Page" + testCaseId);
			}
			fc.utobj().printTestStep("Navigating to CRM > RestAPI > Delete Lead");
			lead = DeleteLead(driver, config, dataSet, UniqueKey_LeadTask, testCaseId);
			if (lead) {
				fc.loginpage().login(driver);
				fc.crm().crm_common().CRMLeadsLnk(driver);
				CRMLeadsPageTest leadpage = new CRMLeadsPageTest();
				leadpage.systemExactSearch(driver, UniqueKey_Lead.get("leadFirstName"));
				try {
					fc.utobj().printTestStep("Navigating to CRM > On View Page > Verifying the Deleted Lead");
					boolean found = fc.utobj().assertPageSource(driver, UniqueKey_Lead.get("leadFirstName"));
					if (found)
						fc.utobj().throwsException("Lead found on view Page");
				} catch (Exception e) {

				}
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception ex) {
			fc.utobj().quitBrowserOnCatch(driver, ex, testCaseId);
		}
	}

	public boolean DeleteLead(WebDriver driver, Map<String, String> config, Map<String, String> dataSet,
			Map<String, String> UniqueKey_Lead, String testCaseId) throws Exception {
		UniqueKey_Lead.put("referenceId", DataServicePage.ReferenceStatus_keys.get(testCaseId + "Lead Info"));
		boolean deleteLead = ActivityPerform(driver, dataSet, config, UniqueKey_Lead, DataServicePage.CRM,
				DataServicePage.LeadInfo, DataServicePage.Delete, DataServicePage.deleteDiv, testCaseId);
		System.out.println("hello testign" + DataServicePage.ReferenceStatus_keys);
		return deleteLead;

	}

	private boolean deleteAllInfo(WebDriver driver, Map<String, String> config, Map<String, String> dataSet,
			String testCaseId, Map<String, String> UniqueKey_LeadCall, Map<String, String> UniqueKey_LeadRemark,
			Map<String, String> UniqueKey_LeadTask) throws Exception {
		UniqueKey_LeadCall.put("referenceId",
				DataServicePage.ReferenceStatus_keys.get(testCaseId + DataServicePage.LeadCall));
		boolean RetrieveLeadCall = ActivityPerform(driver, dataSet, config, UniqueKey_LeadCall, DataServicePage.CRM,
				DataServicePage.LeadCall, DataServicePage.Delete, DataServicePage.deleteDiv, testCaseId);
		UniqueKey_LeadTask.put("referenceId",
				DataServicePage.ReferenceStatus_keys.get(testCaseId + DataServicePage.LeadTask));
		boolean RetrieveLeadTask = ActivityPerform(driver, dataSet, config, UniqueKey_LeadTask, DataServicePage.CRM,
				DataServicePage.LeadTask, DataServicePage.Delete, DataServicePage.deleteDiv, testCaseId);
		UniqueKey_LeadRemark.put("referenceId",
				DataServicePage.ReferenceStatus_keys.get(testCaseId + DataServicePage.LeadRemarks));
		boolean RetrieveLeadRemark = true;// ActivityPerform(driver, dataSet,
											// config, UniqueKey_LeadRemark,
											// DataServicePage.CRM,
		// DataServicePage.LeadRemarks, DataServicePage.Delete,
		// DataServicePage.deleteDiv,testCaseId);
		if (RetrieveLeadCall && RetrieveLeadTask && RetrieveLeadRemark)
			return true;
		else
			return false;
	}

	@Test(groups = { "crmRestApi" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2017-07-03", testCaseDescription = "UpdateLead CRM  in REST API", testCaseId = "TC_23_CRM_UpdateLead")
	public void UpdateAndVarifyLead() throws IOException, Exception, ParserConfigurationException, SAXException {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		Map<String, String> UniqueKey_Lead = new HashMap<String, String>();
		Map<String, String> UniqueKey_CorpUser = new HashMap<String, String>();
		Map<String, String> UniqueKey_LeadCall = new HashMap<String, String>();
		Map<String, String> UniqueKey_LeadRemark = new HashMap<String, String>();
		Map<String, String> UniqueKey_LeadTask = new HashMap<String, String>();

		try {/*
				 * Adding the lead with corporate user account if you want to
				 * add it with regional or franchisee call method already
				 * defined from UserCreation.class and use that one
				 */

			UserCreation uc = new UserCreation();
			fc.utobj().printTestStep("Navigating to CRM > RestAPI > Adding Corporate User");
			boolean corpuser = uc.createCorpUser(driver, config, dataSet, testCaseId, UniqueKey_CorpUser);
			if (!corpuser)
				fc.utobj().throwsException("Corporate user not added " + testCaseId);
			fc.utobj().printTestStep("Navigating to CRM > RestAPI > Adding Lead");
			boolean lead = CreateLead(driver, config, dataSet, UniqueKey_Lead, UniqueKey_CorpUser, testCaseId);
			if (!lead)
				fc.utobj().throwsException("Lead hasn't been added " + testCaseId);
			fc.utobj().printTestStep("Navigating to CRM > RestAPI > Updating Lead");
			lead = UpdateLead(driver, config, dataSet, UniqueKey_Lead, UniqueKey_CorpUser, testCaseId);
			if (!lead)
				fc.utobj().throwsException("Lead hasn't been Updated " + testCaseId);
			fc.utobj().printTestStep("Navigating to CRM > RestAPI > Adding Lead, Remark and Task for Lead");
			boolean AllInfo = FillAllInfo(driver, config, dataSet, testCaseId, UniqueKey_LeadCall, UniqueKey_LeadRemark,
					UniqueKey_LeadTask);
			if (!AllInfo)
				fc.utobj().throwsException("SubModule  hasn't been added " + testCaseId);
			fc.utobj().printTestStep("Navigating to CRM > RestAPI > Updating Lead, Remark and Task for Lead");
			AllInfo = updateAllInfo(driver, config, dataSet, testCaseId, UniqueKey_LeadCall, UniqueKey_LeadRemark,
					UniqueKey_LeadTask);
			if (!AllInfo)
				fc.utobj().throwsException("SubModule  hasn't been Updated " + testCaseId);
			fc.loginpage().login(driver);
			fc.crm().crm_common().CRMLeadsLnk(driver);
			CRMLeadsPageTest CLPT = new CRMLeadsPageTest();
			CLPT.systemExactSearch(driver, UniqueKey_Lead.get("leadFirstName"));

			fc.utobj().clickPartialLinkText(driver, UniqueKey_Lead.get("leadFirstName"));

			UniqueKey_Lead.remove("position");
			UniqueKey_Lead.remove("state");
			UniqueKey_Lead.remove("referenceId");
			UniqueKey_Lead.remove("ownerTypeValue");
			List<String> list_leads = new ArrayList<String>(UniqueKey_Lead.values());
			lead = fc.utobj().assertPageSourceWithMultipleRecords(driver, list_leads);
			if (lead) {
				boolean status = false;
				fc.utobj().printTestStep("Navigating to CRM > On VIew Page > Verifying Call for Lead");
				UniqueKey_LeadCall.remove("referenceId");
				UniqueKey_LeadCall.remove("parentReferenceId");
				UniqueKey_LeadCall.remove("timeAdded");
				UniqueKey_Lead.put("calldate", UniqueKey_LeadCall.get("callDate"));
				List<String> listCAll = new ArrayList<String>(UniqueKey_LeadCall.values());
				status = ValidateForExportCall(driver, config, listCAll, DataServicePage.Call, UniqueKey_Lead,
						UniqueKey_LeadCall.get("subject"));
				if (!status) {
					fc.utobj().throwsException("Lead Call not found on view page");
				}
				fc.utobj().printTestStep("Navigating to CRM > On VIew Page > Verifying Task for Lead");
				UniqueKey_LeadTask.remove("referenceId");
				UniqueKey_LeadTask.remove("parentReferenceId");
				UniqueKey_LeadTask.put("subjecttask", UniqueKey_LeadTask.get("subject"));
				UniqueKey_LeadTask.remove("assignmentTo");
				UniqueKey_LeadTask.remove("priority");
				UniqueKey_LeadTask.remove("date");
				UniqueKey_LeadTask.remove("timelessTask");
				UniqueKey_LeadTask.remove("calendarTask");
				UniqueKey_LeadTask.remove("startTime");
				UniqueKey_LeadTask.remove("endTime");
				UniqueKey_LeadTask.remove("status");
				UniqueKey_LeadTask.remove("reminderTime");
				UniqueKey_LeadTask.remove("reminderDate");
				List<String> listTask = new ArrayList<String>(UniqueKey_LeadTask.values());
				status = ValidateForExportTask(driver, config, listTask, DataServicePage.Call, UniqueKey_Lead,
						UniqueKey_LeadTask.get("subject"));
				if (!status) {
					fc.utobj().throwsException("Lead Call not found on view page");
				}
			} else {
				System.out.println("Not Updated SuccessFully");
				fc.utobj().throwsException("Lead is not found on view Page");
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		}

		catch (Exception ex) {
			fc.utobj().quitBrowserOnCatch(driver, ex, testCaseId);
		}
	}

	private boolean updateAllInfo(WebDriver driver, Map<String, String> config, Map<String, String> dataSet,
			String testCaseId, Map<String, String> UniqueKey_LeadCall, Map<String, String> UniqueKey_LeadRemark,
			Map<String, String> UniqueKey_LeadTask) throws Exception {
		try {
			UniqueKey_LeadCall.put("referenceId",
					DataServicePage.ReferenceStatus_keys.get(testCaseId + DataServicePage.LeadCall));
			UniqueKey_LeadCall.put("callDate", fc.utobj().generatefutureDatewithformat("MM/dd/yyyy", 365));
			UniqueKey_LeadCall.put("timeAdded", "03:00Z");
			UniqueKey_LeadCall.put("callStatus", "Contacted");
			UniqueKey_LeadCall.put("callType", "Outbound Call");
			UniqueKey_LeadCall.put("callSubject", "subject" + fc.utobj().generateRandomNumber());
			boolean createLeadCall = ActivityPerform(driver, dataSet, config, UniqueKey_LeadCall, DataServicePage.CRM,
					DataServicePage.LeadCall, DataServicePage.Update, DataServicePage.updateDiv, testCaseId);
			if (!createLeadCall)
				fc.utobj()
						.throwsException("Lead Call can't be createed in rest api testcase id is >>>>>> " + testCaseId);
			UniqueKey_LeadTask.put("referenceId",
					DataServicePage.ReferenceStatus_keys.get(testCaseId + DataServicePage.LeadTask));
			UniqueKey_LeadTask.put("assignmentTo", "OWNER");
			UniqueKey_LeadTask.put("subject", "LeadTask" + fc.utobj().generateRandomNumber());
			UniqueKey_LeadTask.put("priority", "High");
			UniqueKey_LeadTask.put("date", "10/10/2017");
			UniqueKey_LeadTask.put("timelessTask", "Y");
			UniqueKey_LeadTask.put("calendarTask", "Y");
			UniqueKey_LeadTask.put("startTime", "06:00 AM");
			UniqueKey_LeadTask.put("endTime", "06:00 PM");
			UniqueKey_LeadTask.put("status", "Completed");
			UniqueKey_LeadTask.put("reminderTime", "07:00 AM");
			UniqueKey_LeadTask.put("reminderDate", "10/10/2017");
			boolean createLeadTask = ActivityPerform(driver, dataSet, config, UniqueKey_LeadTask, DataServicePage.CRM,
					DataServicePage.LeadTask, DataServicePage.Update, DataServicePage.updateDiv, testCaseId);
			if (!createLeadTask)
				fc.utobj()
						.throwsException("Lead Task can't be Updated in rest api testcase id is >>>>>> " + testCaseId);
			return true;
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
		return false;
	}

	@Test(groups = { "crmRestAPI" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2017-07-03", testCaseDescription = "Add Lead and Verify at CRM  in REST API", testCaseId = "TC_22_CRM_AddLead")
	public void CreateAndVarifyLead() throws IOException, Exception, ParserConfigurationException, SAXException {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("crm", testCaseId);
		Map<String, String> UniqueKey_Lead = new HashMap<String, String>();
		Map<String, String> UniqueKey_CorpUser = new HashMap<String, String>();
		Map<String, String> UniqueKey_LeadCall = new HashMap<String, String>();
		Map<String, String> UniqueKey_LeadRemark = new HashMap<String, String>();
		Map<String, String> UniqueKey_LeadTask = new HashMap<String, String>();
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {/*
				 * Adding the lead with corporate user account if you want to
				 * add it with regional or franchisee call method already
				 * defined from UserCreation.class and use that one
				 */

			UserCreation uc = new UserCreation();
			fc.utobj().printTestStep("Navigating to CRM > On Rest API > Adding Corporate user");
			boolean corpuser = uc.createCorpUser(driver, config, dataSet, testCaseId, UniqueKey_CorpUser);
			if (!corpuser) {
				fc.utobj().throwsException("CorpUser Not Created ");
			}
			fc.utobj().printTestStep("Navigating to CRM > On Rest API > Adding Lead");
			boolean lead = CreateLead(driver, config, dataSet, UniqueKey_Lead, UniqueKey_CorpUser, testCaseId);
			fc.utobj().printTestStep("Navigating to CRM > On Rest API > Adding Lead, Task and Remark for Lead");
			FillAllInfo(driver, config, dataSet, testCaseId, UniqueKey_LeadCall, UniqueKey_LeadRemark,
					UniqueKey_LeadTask);
			// fc.loginpage().login(driver, config);
			// fc.crm().crm_common().CRMLeadsLnk(driver);
			// CRMLeadsPageTest CLPT = new CRMLeadsPageTest();
			// CLPT.systemExactSearch(driver,
			// UniqueKey_Lead.get("leadFirstName"));
			// fc.utobj().clickPartialLinkText(driver,
			// UniqueKey_Lead.get("leadFirstName"));
			// List<String> list_leads = new
			// ArrayList<String>(UniqueKey_Lead.values());
			// lead = fc.utobj().assertPageSourceWithMultipleRecords(driver,
			// list_leads);
			if (lead) {
				boolean status = false;
				// Validate for Call
				UniqueKey_LeadCall.remove("referenceId");
				UniqueKey_LeadCall.remove("parentReferenceId");
				UniqueKey_LeadCall.remove("timeAdded");
				UniqueKey_Lead.put("calldate", UniqueKey_LeadCall.get("callDate"));
				UniqueKey_Lead.put("subjecttask", UniqueKey_LeadTask.get("subject"));
				List<String> listCAll = new ArrayList<String>(UniqueKey_LeadCall.values());
				fc.utobj().printTestStep("Navigating to CRM > On VIew Page > Verifying Call for Lead");
				status = ValidateForExportCall(driver, config, listCAll, DataServicePage.Call, UniqueKey_Lead,
						UniqueKey_LeadCall.get("subject"));
				if (!status) {
					fc.utobj().throwsException("Lead Call not found on view page");
				}
				// Validate for Task
				UniqueKey_LeadTask.remove("referenceId");
				UniqueKey_LeadTask.remove("parentReferenceId");
				UniqueKey_LeadTask.put("subjecttask", UniqueKey_LeadTask.get("subject"));
				UniqueKey_LeadTask.remove("assignmentTo");
				UniqueKey_LeadTask.remove("priority");
				UniqueKey_LeadTask.remove("date");
				UniqueKey_LeadTask.remove("timelessTask");
				UniqueKey_LeadTask.remove("calendarTask");
				UniqueKey_LeadTask.remove("startTime");
				UniqueKey_LeadTask.remove("endTime");
				UniqueKey_LeadTask.remove("status");
				UniqueKey_LeadTask.remove("reminderTime");
				UniqueKey_LeadTask.remove("reminderDate");
				List<String> listTask = new ArrayList<String>(UniqueKey_LeadTask.values());
				fc.utobj().printTestStep("Navigating to CRM > On VIew Page > Verifying Task for Lead");
				status = ValidateForExportTask(driver, config, listTask, DataServicePage.Call, UniqueKey_Lead,
						UniqueKey_LeadTask.get("subject"));
				if (!status) {
					fc.utobj().throwsException("Lead Call not found on view page");
				}
			}

			else {
				fc.utobj().throwsException("Lead value is not found on view Page" + testCaseId);
			}
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception ex) {
			fc.utobj().switchFrameToDefault(driver);
			fc.utobj().quitBrowserOnCatch(driver, ex, testCaseId);
		}
	}

	private boolean ValidateForExportCall(WebDriver driver, Map<String, String> config, List<String> listVI,
			String subModule, Map<String, String> uniqueKey, String string) throws Exception {
		try {
			boolean status = false;
			fc.loginpage().login(driver);
			fc.crm().crm_common().CRMExportLnk(driver);
			CRMExportPage pobj = new CRMExportPage(driver);
			fc.utobj().clickElement(driver, pobj.exportLead);
			fc.utobj().clickElement(driver, pobj.ProceedBtn);
			fc.utobj().clickElement(driver, pobj.chkbxActHistoryCall);
			fc.utobj().clickElement(driver, pobj.srchDataBtn);
			fc.utobj().sendKeys(driver, pobj.firstname, uniqueKey.get("leadFirstName"));
			fc.utobj().sendKeys(driver, pobj.lastname, uniqueKey.get("leadLastName"));
			// fc.utobj().sendKeys(driver,
			// pobj.leadDatefrom,uniqueKey.get("calldate"));
			// fc.utobj().sendKeys(driver,
			// pobj.leadDateto,uniqueKey.get("calldate"));
			fc.utobj().sendKeys(driver, pobj.leadcallDatefrom, uniqueKey.get("calldate"));
			fc.utobj().sendKeys(driver, pobj.leadcallDateto, uniqueKey.get("calldate"));
			fc.utobj().clickElement(driver, pobj.viewdata);
			status = fc.utobj().assertPageSourceWithMultipleRecords(driver, listVI);
			return status;
		} catch (Exception ex) {
			fc.utobj().quitBrowserOnCatch(driver, ex, "validate failed for " + subModule);
			return false;
		}

	}

	private boolean ValidateForExportTask(WebDriver driver, Map<String, String> config, List<String> listVI,
			String subModule, Map<String, String> uniqueKey, String string) throws Exception {
		try {
			boolean status = false;
			fc.loginpage().login(driver);
			fc.crm().crm_common().CRMExportLnk(driver);
			CRMExportPage pobj = new CRMExportPage(driver);
			fc.utobj().clickElement(driver, pobj.exportLead);
			fc.utobj().clickElement(driver, pobj.ProceedBtn);
			fc.utobj().clickElement(driver, pobj.chkbxActHistoryTask);
			fc.utobj().clickElement(driver, pobj.srchDataBtn);
			fc.utobj().sendKeys(driver, pobj.firstname, uniqueKey.get("leadFirstName"));
			fc.utobj().sendKeys(driver, pobj.lastname, uniqueKey.get("leadLastName"));
			// fc.utobj().sendKeys(driver,
			// pobj.leadDatefrom,uniqueKey.get("calldate"));
			// fc.utobj().sendKeys(driver,
			// pobj.leadDateto,uniqueKey.get("calldate"));
			fc.utobj().sendKeys(driver, pobj.ExportTaskSubject, string);
			// fc.utobj().sendKeys(driver,
			// pobj.leadcallDateto,uniqueKey.get("calldate"));
			fc.utobj().clickElement(driver, pobj.viewdata);
			status = fc.utobj().assertPageSourceWithMultipleRecords(driver, listVI);
			return status;

		} catch (Exception ex) {
			// fc.utobj().quitBrowserOnCatch(driver, config, ex, "validate
			// failed for "+subModule);
			// fc.utobj().throwsException(exceptionMsg)(driver, config, ex,
			// "validate failed for "+subModule);
			return false;
		}
	}

	private boolean FillAllInfo(WebDriver driver, Map<String, String> config, Map<String, String> dataSet,
			String testCaseId, Map<String, String> UniqueKey_LeadCall, Map<String, String> UniqueKey_LeadRemark,
			Map<String, String> UniqueKey_LeadTask) {
		try {
			UniqueKey_LeadCall.put("parentReferenceId",
					DataServicePage.ReferenceStatus_keys.get(testCaseId + "Lead Info"));
			UniqueKey_LeadCall.put("callDate", fc.utobj().generatefutureDatewithformat("MM/dd/yyyy", 3));
			UniqueKey_LeadCall.put("timeAdded", "03:00Z");
			UniqueKey_LeadCall.put("callStatus", "Contacted");
			UniqueKey_LeadCall.put("callType", "Outbound Call");
			UniqueKey_LeadCall.put("callSubject", "subjent" + fc.utobj().generateRandomNumber());
			boolean createLeadCall = ActivityPerform(driver, dataSet, config, UniqueKey_LeadCall, DataServicePage.CRM,
					DataServicePage.LeadCall, DataServicePage.Create, DataServicePage.createDiv, testCaseId);
			if (!createLeadCall)
				fc.utobj().throwsException("Lead Call not created in rest api witd id " + testCaseId);
			UniqueKey_LeadRemark.put("parentReferenceId",
					DataServicePage.ReferenceStatus_keys.get(testCaseId + "Lead Info"));
			UniqueKey_LeadRemark.put("remark", "Testing Remark Done" + fc.utobj().generateRandomNumber());
			boolean createLeadRemark = ActivityPerform(driver, dataSet, config, UniqueKey_LeadRemark,
					DataServicePage.CRM, DataServicePage.LeadRemarks, DataServicePage.Create, DataServicePage.createDiv,
					testCaseId);
			if (!createLeadRemark)
				fc.utobj().throwsException("createLeadRemark not created in rest api with id " + testCaseId);
			UniqueKey_LeadTask.put("parentReferenceId",
					DataServicePage.ReferenceStatus_keys.get(testCaseId + "Lead Info"));
			UniqueKey_LeadTask.put("assignmentTo", "OWNER");
			UniqueKey_LeadTask.put("subject", "LeadTask" + fc.utobj().generateRandomNumber());
			UniqueKey_LeadTask.put("priority", "High");
			UniqueKey_LeadTask.put("date", fc.utobj().generateCurrentDatewithformat("yyyy-MM-dd"));
			UniqueKey_LeadTask.put("timelessTask", "Y");
			UniqueKey_LeadTask.put("calendarTask", "Y");
			UniqueKey_LeadTask.put("startTime", "06:00 AM");
			UniqueKey_LeadTask.put("endTime", "06:00 PM");
			UniqueKey_LeadTask.put("status", "Completed");
			UniqueKey_LeadTask.put("reminderTime", "07:00 AM");
			UniqueKey_LeadTask.put("reminderDate", fc.utobj().generateCurrentDatewithformat("yyyy-MM-dd"));
			boolean createLeadTask = ActivityPerform(driver, dataSet, config, UniqueKey_LeadTask, DataServicePage.CRM,
					DataServicePage.LeadTask, DataServicePage.Create, DataServicePage.createDiv, testCaseId);
			if (!createLeadTask)
				fc.utobj().throwsException("createLeadTask not created in rest api with id " + testCaseId);
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	private boolean CreateLead(WebDriver driver, Map<String, String> config, Map<String, String> dataSet,
			Map<String, String> UniqueKey_Lead, Map<String, String> UniqueKey_CorpUser, String testCaseId)
			throws Exception {
		UniqueKey_Lead.put("leadFirstName", "AddLeadFName" + fc.utobj().generateRandomNumber());
		UniqueKey_Lead.put("leadLastName", "AddLeadLName" + fc.utobj().generateRandomNumber());
		UniqueKey_Lead.put("primaryContactMethod", "Email");
		UniqueKey_Lead.put("companyName", "Franconnect India Software Pvt Ltd");
		UniqueKey_Lead.put("ownerTypeValue", "CORP");
		UniqueKey_Lead.put("leadOwnerID",
				UniqueKey_CorpUser.get("firstname") + " " + UniqueKey_CorpUser.get("lastname"));
		UniqueKey_Lead.put("address", "c-94 sector-8");
		UniqueKey_Lead.put("city", "NYC");
		UniqueKey_Lead.put("country", "USA");
		UniqueKey_Lead.put("zipcode", "10030");
		UniqueKey_Lead.put("state", "Alabama");
		UniqueKey_Lead.put("emailIds", "");
		UniqueKey_Lead.put("mobileNumbers", "");
		// UniqueKey_Lead.put("position", "");
		UniqueKey_Lead.put("title", "Dr.");
		UniqueKey_Lead.put("emailIds", "test@franconnect.net");
		boolean createLead = ActivityPerform(driver, dataSet, config, UniqueKey_Lead, DataServicePage.CRM,
				DataServicePage.LeadInfo, DataServicePage.Create, DataServicePage.createDiv, testCaseId);
		System.out.println("hello testign" + DataServicePage.ReferenceStatus_keys);
		if (createLead)
			return createLead;
		else
			fc.utobj().quitBrowser(driver, config, "Lead Can not be Created");
		return createLead;
	}

	private boolean UpdateLead(WebDriver driver, Map<String, String> config, Map<String, String> dataSet,
			Map<String, String> UniqueKey_Lead, Map<String, String> UniqueKey_CorpUser, String testCaseId)
			throws Exception {
		UniqueKey_Lead.put("referenceId", DataServicePage.ReferenceStatus_keys.get(testCaseId + "Lead Info"));
		UniqueKey_Lead.put("cmLeadStatusID", "New");
		UniqueKey_Lead.put("leadFirstName", "AddLeadFName" + fc.utobj().generateRandomNumber());
		UniqueKey_Lead.put("leadLastName", "AddLeadLName" + fc.utobj().generateRandomNumber());
		UniqueKey_Lead.put("primaryContactMethod", "Email");
		UniqueKey_Lead.put("companyName", "Franconnect India Software Pvt Ltd");
		UniqueKey_Lead.put("ownerTypeValue", "CORP");
		UniqueKey_Lead.put("leadOwnerID",
				UniqueKey_CorpUser.get("firstname") + " " + UniqueKey_CorpUser.get("lastname"));
		UniqueKey_Lead.put("address", "c-94 sector-8");
		UniqueKey_Lead.put("city", "NYC");
		UniqueKey_Lead.put("country", "USA");
		UniqueKey_Lead.put("zipcode", "10030");
		UniqueKey_Lead.put("state", "");
		UniqueKey_Lead.put("emailIds", "");
		UniqueKey_Lead.put("mobileNumbers", "");
		UniqueKey_Lead.put("position", "");
		UniqueKey_Lead.put("title", "Dr.");
		UniqueKey_Lead.put("emailIds", "test@franconnect.net");
		boolean updateLead = ActivityPerform(driver, dataSet, config, UniqueKey_Lead, DataServicePage.CRM,
				DataServicePage.LeadInfo, DataServicePage.Update, DataServicePage.updateDiv, testCaseId);
		System.out.println("hello testign" + DataServicePage.ReferenceStatus_keys);
		if (updateLead)
			return updateLead;
		else
			fc.utobj().quitBrowser(driver, config, "Lead Can not be Created");
		return updateLead;
	}

	private boolean RetrieveLead(WebDriver driver, Map<String, String> config, Map<String, String> dataSet,
			Map<String, String> UniqueKey_Lead, String testCaseId) throws Exception {
		UniqueKey_Lead.put("referenceId", DataServicePage.ReferenceStatus_keys.get(testCaseId + "Lead Info"));
		boolean retrieveLead = ActivityPerform(driver, dataSet, config, UniqueKey_Lead, DataServicePage.CRM,
				DataServicePage.LeadInfo, DataServicePage.Retrieve, DataServicePage.retrieveDiv, testCaseId);
		System.out.println("hello testign" + DataServicePage.ReferenceStatus_keys);
		if (retrieveLead)
			return retrieveLead;
		else
			fc.utobj().quitBrowser(driver, config, "Lead Can not be Created");
		return retrieveLead;
	}

}
