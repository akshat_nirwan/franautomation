package com.builds.test.crm.RestAPI;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Test;

import com.builds.test.common.CRMModule;
import com.builds.test.common.WebService;
import com.builds.test.crm.Leadgetset;
import com.builds.uimaps.crm.CRMLeadsPage;
import com.builds.utilities.FranconnectUtil;

public class CRM_AddLeadFrom_Webservice {
	@Test(groups = "crm")
	public void Addleadfrom_Webservices() throws Exception
	{
		FranconnectUtil fc = new FranconnectUtil();
		Leadgetset lead = new Leadgetset();
		CRMModule crm = new CRMModule();
		lead = crm.crm_common().getDefaultDataForLead(lead);
		
		fc.utobj().printTestStep("Add a CRM Lead Through WebServices");
		
		String module = "cm";
		String subModule = "lead";
		String isISODate = "no";
		
		String xmlString = 
		"<fcRequest>"
		+"<cmLead>"
		+"<title>"+lead.getTitle()+"</title>"
		+"<leadFirstName>"+lead.getWeb_firstname()+"</leadFirstName>"
        +"<leadLastName>"+lead.getWeb_lastname()+"</leadLastName>"
        +"<primaryContactMethod>"+lead.getPrimarycontact()+"</primaryContactMethod>"
        +"<companyName>"+lead.getCompnayname()+"</companyName>"
        +"<ownerTypeValue>"+lead.getWebservice_AssignTo()+"</ownerTypeValue>"
        +"<leadOwnerID>"+lead.getWebService_LeadOwner()+"</leadOwnerID>"
        +"<leadOwnerReferenceId>"+"</leadOwnerReferenceId>"
        +"<franchiseeNo>"+"</franchiseeNo>"
        +"<franchiseeReferenceId>"+"</franchiseeReferenceId>"
        +"<areaReferenceId>"+"</areaReferenceId>"
        +"<areaID>"+"</areaID>"
        +"<divisionReferenceId>"+"</divisionReferenceId>"
        +"<divisionID>"+"</divisionID>"
        +"<address>"+lead.getAddress()+"</address>"
        +"<city>"+lead.getCity()+"</city>"
        +"<country>"+lead.getCountry()+"</country>"
        +"<zipcode>"+lead.getZipcode()+"</zipcode>"
        +"<state>"+lead.getState()+"</state>"
        +"<phoneNumbers>"+lead.getPhonenumber()+"</phoneNumbers>"
        +"<extn>"+lead.getExtension()+"</extn>"
        +"<faxNumbers>"+lead.getFax()+"</faxNumbers>"
        +"<sendAutomaticSMS>"+"Yes"+"</sendAutomaticSMS>"
        +"<mobileNumbers>"+lead.getMobile()+"</mobileNumbers>"
        +"<emailIds>"+lead.getEmail()+"</emailIds>"
        +"<alternateEmail>"+lead.getAlternarteemail()+"</alternateEmail>"
        +"<suffix>"+lead.getSuffix()+"</suffix>"
        +"<position>"+lead.getJobtitle()+"</position>"
        +"<cmLeadTypeID>"+"</cmLeadTypeID>"
        +"<cmRatingID>"+lead.getRating()+"</cmRatingID>"
        +"<cmSource2ID>"+lead.getSource()+"</cmSource2ID>"
        +"<cmSource3ID>"+lead.getSource1()+"</cmSource3ID>"
        +"<cmSource1ID>"+lead.getContactMedium()+"</cmSource1ID>"
        +"<bestTimeToContact>"+lead.getBesttime()+"</bestTimeToContact>"
        +"<comments>"+lead.getComment()+"</comments>"
        +"<dataUseConsent>"+"Yes"+"</dataUseConsent>"
        +"<dataUseConsentText>"+"</dataUseConsentText>"
        +"<emailOptInRequestConsent>"+"Yes"+"</emailOptInRequestConsent>"
        +"<emailOptInRequestConsentText>"+"</emailOptInRequestConsentText>"
        +"<smsOptInRequestConsent>"+"Yes"+"</smsOptInRequestConsent>"
        +"<smsOptInRequestConsentText>"+"</smsOptInRequestConsentText>"
        +"<sendAutomaticMail>"+"Yes"+"</sendAutomaticMail>"
        +"</cmLead>"
        +"</fcRequest>";
		
		
	
		
		WebService rest = new WebService();
		rest.create(module, subModule, xmlString, isISODate);
		
	
	}
	
	
	//VipinG
	public void Addleadfrom_Webservices_Verify(WebDriver driver) throws Exception
	{
			FranconnectUtil fc = new FranconnectUtil();
			
		try {
			Leadgetset lead = new Leadgetset();
			CRMModule crm = new CRMModule();
			CRMLeadsPage crmleadpage=new CRMLeadsPage(driver);
			lead = crm.crm_common().getDefaultDataForLeadwithSqlite(lead);
			
			fc.utobj().printTestStep("Add a CRM Lead Through WebServices");
			
			String module = "cm";
			String subModule = "lead";
			String isISODate = "no";
			
			String xmlString = 
			"<fcRequest>"
			+"<cmLead>"
			+"<title>"+lead.getTitle()+"</title>"
			+"<leadFirstName>"+lead.getWeb_firstname()+"</leadFirstName>"
	        +"<leadLastName>"+lead.getWeb_lastname()+"</leadLastName>"
	        +"<primaryContactMethod>"+lead.getPrimarycontact()+"</primaryContactMethod>"
	        +"<companyName>"+lead.getCompnayname()+"</companyName>"
	        +"<ownerTypeValue>"+lead.getWebservice_AssignTo()+"</ownerTypeValue>"
	        +"<leadOwnerID>"+lead.getWebService_LeadOwner()+"</leadOwnerID>"
	        +"<leadOwnerReferenceId>"+"</leadOwnerReferenceId>"
	        +"<franchiseeNo>"+"</franchiseeNo>"
	        +"<franchiseeReferenceId>"+"</franchiseeReferenceId>"
	        +"<areaReferenceId>"+"</areaReferenceId>"
	        +"<areaID>"+"</areaID>"
	        +"<divisionReferenceId>"+"</divisionReferenceId>"
	        +"<divisionID>"+"</divisionID>"
	        +"<address>"+lead.getAddress()+"</address>"
	        +"<city>"+lead.getCity()+"</city>"
	        +"<country>"+lead.getCountry()+"</country>"
	        +"<zipcode>"+lead.getZipcode()+"</zipcode>"
	        +"<state>"+lead.getState()+"</state>"
	        +"<phoneNumbers>"+lead.getPhonenumber()+"</phoneNumbers>"
	        +"<extn>"+lead.getExtension()+"</extn>"
	        +"<faxNumbers>"+lead.getFax()+"</faxNumbers>"
	        +"<sendAutomaticSMS>"+"Yes"+"</sendAutomaticSMS>"
	        +"<mobileNumbers>"+lead.getMobile()+"</mobileNumbers>"
	        +"<emailIds>"+lead.getEmail()+"</emailIds>"
	        +"<alternateEmail>"+lead.getAlternarteemail()+"</alternateEmail>"
	        +"<suffix>"+lead.getSuffix()+"</suffix>"
	        +"<position>"+lead.getJobtitle()+"</position>"
	        +"<cmLeadTypeID>"+"</cmLeadTypeID>"
	        +"<cmRatingID>"+lead.getRating()+"</cmRatingID>"
	        +"<cmSource2ID>"+lead.getSource()+"</cmSource2ID>"
	        +"<cmSource3ID>"+lead.getSource1()+"</cmSource3ID>"
	        +"<cmSource1ID>"+lead.getContactMedium()+"</cmSource1ID>"
	        +"<bestTimeToContact>"+lead.getBesttime()+"</bestTimeToContact>"
	        +"<comments>"+lead.getComment()+"</comments>"
	        +"<dataUseConsent>"+"Yes"+"</dataUseConsent>"
	        +"<dataUseConsentText>"+"</dataUseConsentText>"
	        +"<emailOptInRequestConsent>"+"Yes"+"</emailOptInRequestConsent>"
	        +"<emailOptInRequestConsentText>"+"</emailOptInRequestConsentText>"
	        +"<smsOptInRequestConsent>"+"Yes"+"</smsOptInRequestConsent>"
	        +"<smsOptInRequestConsentText>"+"</smsOptInRequestConsentText>"
	        +"<sendAutomaticMail>"+"Yes"+"</sendAutomaticMail>"
	        +"</cmLead>"
	        +"</fcRequest>";
			
			
		
			
			WebService rest = new WebService();
			rest.create(module, subModule, xmlString, isISODate);
			
			
			String firstName=lead.getWeb_firstname();
			String lastName=lead.getWeb_lastname();
			String fullName=firstName+" "+lastName;
			
			
			fc.utobj().printTestStep("Navigate To CRM > Leads > Leads Summary");
			fc.crm().crm_common().CRMLeadsLnk(driver);
			fc.utobj().sendKeys(driver, crmleadpage.searchLeads, fullName);
			fc.utobj().clickElement(driver,"//*[@id='systemSearchBtn']");
			
			boolean verifyName = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[.='" + firstName + " " + lastName + "']");
			if (verifyName == false) {
				fc.utobj().throwsException("was not able to verify Name");
			}
			boolean ownerOfLead = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[.='" + firstName + " " + lastName
					+ "']/ancestor::tr/td[contains(text () , 'FranConnect Administrator')]");
			
			if (ownerOfLead == false) {
				fc.utobj().throwsException("was not able to verify owner of the lead");
			}
			boolean statusOfLead = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[.='" + firstName + " " + lastName + "']/ancestor::tr/td[contains(text () , 'New')]");
			if (statusOfLead == false) {
				fc.utobj().throwsException("was not able to verify staus of the lead");
			}
		
			fc.utobj().printTestStep("Lead is added through REST API and Verified also");
		}
		catch(Exception e) {
			fc.utobj().throwsException("Lead was not added through REST API");
		}
	}
	
}
