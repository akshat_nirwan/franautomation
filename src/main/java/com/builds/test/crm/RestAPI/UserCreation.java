package com.builds.test.crm.RestAPI;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.xml.parsers.ParserConfigurationException;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.testng.Reporter;
import org.testng.annotations.Test;
import org.xml.sax.SAXException;

import com.builds.test.admin.AdminAreaRegionAddAreaRegionPageTest;
import com.builds.test.admin.AdminDivisionAddDivisionPageTest;
import com.builds.test.admin.AdminFranchiseLocationAddFranchiseLocationPageTest;
import com.builds.uimaps.common.LoginPage;
import com.builds.utilities.FranconnectUtil;
import com.builds.utilities.TestCase;

public class UserCreation {
	FranconnectUtil fc = new FranconnectUtil();

	public boolean addFranUser(WebDriver driver, Map<String, String> config, Map<String, String> dataSet,
			Map<String, String> UniqueKey) {
		boolean userstatus = false;
		try {
			driver.navigate().to(config.get("buildUrl"));
			fc.loginpage().login(driver);
			AdminFranchiseLocationAddFranchiseLocationPageTest AFAFP = new AdminFranchiseLocationAddFranchiseLocationPageTest();
			String RandomRegionName = "Region" + fc.utobj().generateRandomNumber() + fc.utobj().getRandomChar();
			UniqueKey.put("franchiseName", "Fname" + fc.utobj().generateRandomNumber());
			UniqueKey.put("centerName", "N" + fc.utobj().generateRandomNumber());
			UniqueKey.put("openingDate", "12/12/2017");
			UniqueKey.put("franchiseName", "N" + fc.utobj().generateRandomNumber());
			UniqueKey.put("ownerLastName", "N" + fc.utobj().generateRandomNumber());
			String franchiseName = UniqueKey.get("franchiseName");
			String centerName = UniqueKey.get("centerName");
			String openingDate = UniqueKey.get("openingDate");
			String ownerFirstName = UniqueKey.get("franchiseName");
			String ownerLastName = UniqueKey.get("ownerLastName");
			String storeType = "Test" + fc.utobj().generateRandomNumber();
			AFAFP.addFranchiseLocationZC(driver, RandomRegionName, storeType, franchiseName, centerName, openingDate,
					ownerFirstName, ownerLastName, config);
			driver.navigate().to(config.get("buildUrl") + "dataservices/dataServicesTest.jsp");
			String SecureToken = GetSecureToken(driver, dataSet, config);
			String EcryptedToken = GetEcryptedToken(driver, dataSet, config, SecureToken);
			String RoleType = "Franchise Level (0)";
			String module = "Admin";
			String submodule = "User";
			String operation = "Create";
			UniqueKey.put("userID", dataSet.get("userID") + fc.utobj().generateRandomNumber());
			UniqueKey.put("franchiseeNo", franchiseName);
			UniqueKey.put("locationType", "Franchise");
			UniqueKey.put("role", " Default Franchise Role");
			UniqueKey.put("userLevel", "Employee");
			UniqueKey.put("canAddEmployee", "N");
			UniqueKey.put("salutation", "Mr.");
			UniqueKey.put("userType", "New");
			String Query = GetQuery(driver, dataSet, config, EcryptedToken, RoleType, module, submodule, operation);
			System.out.println(Query);
			String ResultantQuery = GetResultantQuery_R(Query, dataSet, UniqueKey);
			userstatus = CRUDByQuery(driver, dataSet, config, EcryptedToken, ResultantQuery, "Admin", "User", "Create",
					"createDiv", DataServicePage.ReferenceStatus_keys);
			if (userstatus) {
				System.out.println("Created Successfully");
			} else
				System.out.println("Corporate user not Created ");
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return userstatus;
	}

	@Test(groups = { "crmRestAPI", "crm_userCreationRestAPI" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2017-07-03", testCaseDescription = "Verify The Add Franchise User", testCaseId = "TC_04_Add_Franchisee_User")
	public void addFranchiseUser() throws IOException, Exception, ParserConfigurationException, SAXException {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());
		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData(testCaseId);
		Map<String, String> UniqueKey = new HashMap<String, String>();
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		try {
			fc.utobj().printTestStep("Navigating to RestAPI > Adding Franchisee User");
			boolean status = addFranUser(driver, config, dataSet, UniqueKey);
			if (status) {
				try {
					fc.utobj().printTestStep("Navigating to Loginpage > login and Validating for Franchisee User");
					String userName = UniqueKey.get("userID");
					driver.get(config.get("buildUrl"));
					LoginPage npobj = new LoginPage(driver);
					fc.utobj().sendKeys(driver, npobj.userid, userName);
					fc.utobj().sendKeys(driver, npobj.password, "T0n1ght1");
					fc.utobj().clickElement(driver, npobj.login);
					fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
				} catch (Exception ex) {
					fc.utobj().quitBrowserOnCatch(driver, ex, testCaseId);
				}
			} else {
				fc.utobj().throwsException("Franchisee User not Created Error in >> " + testCaseId);
			}
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "crmRestAPI", "crm_userCreationRestAPI" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2017-07-03", testCaseDescription = "Verify The Add Divisional User", testCaseId = "TC_03_Add_Divisional_User")
	public void addDivisionalUser() throws IOException, Exception, ParserConfigurationException, SAXException {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData(testCaseId);
		Map<String, String> UniqueKey = new HashMap<String, String>();

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		driver = fc.loginpage().login(driver);
		fc.utobj().printTestStep("Navigating to login Page>Admin> Adding Division");
		String divisionName = "Div" + fc.utobj().generateRandomChar() + fc.utobj().generateRandomNumber();
		AdminDivisionAddDivisionPageTest ADADP = new AdminDivisionAddDivisionPageTest();
		String Divison = ADADP.addDivisionName(driver, divisionName);
		fc.utobj().printTestStep("Navigating to RestAPI > Adding Divisiaonal User");
		driver.navigate().to(config.get("buildUrl") + "dataservices/dataServicesTest.jsp");
		try {
			String SecureToken = GetSecureToken(driver, dataSet, config);
			String EcryptedToken = GetEcryptedToken(driver, dataSet, config, SecureToken);
			String RoleType = "Division Level (6)";
			String module = "Admin";
			String submodule = "User";
			String operation = "Create";
			UniqueKey.put("userID", dataSet.get("userID") + fc.utobj().generateRandomNumber());
			UniqueKey.put("division", Divison);
			UniqueKey.put("role", /*" Default Divisional Role"*/"Default Division Role");
			UniqueKey.put("timezone", "GMT +00:30");
			UniqueKey.put("type", "Normal");
			UniqueKey.put("userLevel", "Divisional");
			UniqueKey.put("expirationTime", "NA");
			UniqueKey.put("firstname", "TestRegUserFname");
			UniqueKey.put("lastname", "TestRegUserLname");
			UniqueKey.put("city", "NYC");
			UniqueKey.put("state", "New york");
			UniqueKey.put("phone1", "67412587894");
			UniqueKey.put("email", "test@franconnect.net");
			UniqueKey.put("sendNotification", "Y");
			UniqueKey.put("isBillable", "N");
			UniqueKey.put("isDaylight", "N");
			UniqueKey.put("isAuditor", "N");
			String Query = GetQuery(driver, dataSet, config, EcryptedToken, RoleType, module, submodule, operation);
			String ResultantQuery = GetResultantQuery_R(Query, dataSet, UniqueKey);
			boolean CreateDivisionalUser = CRUDByQuery(driver, dataSet, config, EcryptedToken, ResultantQuery, "Admin",
					"User", "Create", "createDiv");
			if (CreateDivisionalUser)
				System.out.println("Created Successfully");
			else
				fc.utobj()
						.throwsException("Divisiona user not Created error in RestApi >>>> TC_03_Add_Divisional_User");
			if (CreateDivisionalUser) {
				try {
					fc.utobj().printTestStep("Navigating to Loginpage > login and Validating for Divisional User");
					driver.navigate().to(config.get("buildUrl"));
					fc.loginpage().loginWithParameter(driver, UniqueKey.get("userID"), "T0n1ght1");
					fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
				} catch (Exception ex) {
					fc.utobj().quitBrowserOnCatch(driver, ex, testCaseId);
				}
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception ex) {
			fc.utobj().quitBrowserOnCatch(driver, ex, testCaseId);
		}
	}

	@Test(groups = { "crmRestAPI", "crm_userCreationRestAPI" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2017-07-03", testCaseDescription = "Verify The Add Regional User", testCaseId = "TC_02_Add_Regional_User")
	public void addRegionalUser() throws IOException, Exception, ParserConfigurationException, SAXException {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData(testCaseId);
		Map<String, String> UniqueKey = new HashMap<String, String>();
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		try {
			driver = fc.loginpage().login(driver);
			String region = "region" + fc.utobj().generateRandomNumber();
			AdminAreaRegionAddAreaRegionPageTest addRegion = new AdminAreaRegionAddAreaRegionPageTest();
			fc.utobj().printTestStep("Navigating to login Page>Admin> Adding Region");
			addRegion.addAreaRegion(driver, region);
			driver.navigate().to(config.get("buildUrl") + "dataservices/dataServicesTest.jsp");
			String SecureToken = GetSecureToken(driver, dataSet, config);
			String EcryptedToken = GetEcryptedToken(driver, dataSet, config, SecureToken);
			String RoleType = "Regional Level (2)";
			String module = "Admin";
			String submodule = "User";
			String operation = "Create";
			UniqueKey.put("userID", dataSet.get("userID") + fc.utobj().generateRandomNumber());
			UniqueKey.put("userLevel", "Regional");// Akshay division
			UniqueKey.put("timezone", "GMT +00:30");// Default Divisional Role
			UniqueKey.put("type", "Normal");
			UniqueKey.put("expirationTime", "NA");
			UniqueKey.put("role", "Default Regional Role");
			UniqueKey.put("firstname", "TestRegUserFname");
			UniqueKey.put("lastname", "TestRegUserLname");
			UniqueKey.put("city", "NYC");
			UniqueKey.put("state", "New york");
			UniqueKey.put("phone1", "67412587894");
			UniqueKey.put("email", "test@franconnect.net");
			UniqueKey.put("sendNotification", "Y");
			UniqueKey.put("isBillable", "N");
			UniqueKey.put("isDaylight", "N");
			UniqueKey.put("region", region);
			String Query = GetQuery(driver, dataSet, config, EcryptedToken, RoleType, module, submodule, operation);
			System.out.println(Query);
			String ResultantQuery = GetResultantQuery_R(Query, dataSet, UniqueKey);
			boolean CreateRegionalUser = CRUDByQuery(driver, dataSet, config, EcryptedToken, ResultantQuery, "Admin",
					"User", "Create", "createDiv");
			if (CreateRegionalUser)
				System.out.println("Created Successfully");
			else
				fc.utobj().throwsException("Regional user not Created TC_02_Add_Regional_User");
			if (CreateRegionalUser) {
				try {
					fc.utobj().printTestStep("Navigating to Loginpage > login and Validating for Regional User");
					driver.navigate().to(config.get("buildUrl"));
					fc.loginpage().loginWithParameter(driver, UniqueKey.get("userID"), "T0n1ght1");
					fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
				} catch (Exception ex) {
					fc.utobj().quitBrowserOnCatch(driver, ex, testCaseId);
				}
			}
		} catch (Exception ex) {
			fc.utobj().quitBrowserOnCatch(driver, ex, testCaseId);
		}
	}

	@Test(groups = { "crmRestAPI", "crm_userCreationRestAPI" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2017-07-03", testCaseDescription = "Verify The Add Coroporate User", testCaseId = "TC_01_Add_Corporate_User")
	public void addCorporateUser() throws IOException, Exception, ParserConfigurationException, SAXException {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData(testCaseId);
		Map<String, String> UniqueKey = new HashMap<String, String>();
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			boolean CreateCorporateUser = createCorpUser(driver, config, dataSet, testCaseId, UniqueKey);

			if (CreateCorporateUser)
				System.out.println("Created Successfully");
			else
				fc.utobj().throwsException("Corporate user not Create  Problem with TC_01_Add_Corporate_User");
			if (CreateCorporateUser) {
				try {
					fc.utobj().printTestStep("Navigating to Loginpage > login and Validating for Corporate User");
					driver.navigate().to(config.get("buildUrl"));
					fc.loginpage().loginWithParameter(driver, UniqueKey.get("userID"), "T0n1ght1");
					fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
				} catch (Exception ex) {
					fc.utobj().quitBrowserOnCatch(driver, ex, testCaseId);
				}
			}
		} catch (Exception ex) {
			fc.utobj().quitBrowserOnCatch(driver, ex, testCaseId);
		}
	}

	public boolean createCorpUser(WebDriver driver, Map<String, String> config, Map<String, String> dataSet,
			String testCaseId, Map<String, String> UniqueKey) throws Exception {
		boolean val = false;
		try {
			driver.navigate().to(config.get("buildUrl") + "dataservices/dataServicesTest.jsp");
			String SecureToken = GetSecureToken(driver, dataSet, config);
			String EcryptedToken = GetEcryptedToken(driver, dataSet, config, SecureToken);
			UniqueKey.put("userID", dataSet.get("userID") + fc.utobj().generateRandomNumber());
			UniqueKey.put("password", dataSet.get("password"));
			UniqueKey.put("userLevel", "Corporate");
			UniqueKey.put("timezone", "GMT +00:30");
			UniqueKey.put("type", "Normal");
			UniqueKey.put("expirationTime", "NA");
			UniqueKey.put("role", "Corporate Administrator");
			UniqueKey.put("firstname", "TestCorpUserFname" + fc.utobj().generateRandomNumber6Digit());
			UniqueKey.put("lastname", "TestCorpUserLname" + fc.utobj().generateRandomNumber6Digit());
			UniqueKey.put("city", "NYC");
			UniqueKey.put("state", "New york");
			UniqueKey.put("phone1", "67412587894");
			UniqueKey.put("email", "test@franconnect.net");
			UniqueKey.put("sendNotification", "Y");
			UniqueKey.put("isBillable", "N");
			UniqueKey.put("isDaylight", "N");
			UniqueKey.put("isAuditor", "N");
			String RoleType = "Corporate Level (1)";
			String module = "Admin";
			String submodule = "User";
			String operation = "Create";
			Thread.sleep(1000);
			String Query = GetQuery(driver, dataSet, config, EcryptedToken, RoleType, module, submodule, operation);
			Thread.sleep(1000);
			String ResultantQuery = GetResultantQuery_R(Query, dataSet, UniqueKey);
			Thread.sleep(1000);
			val = CRUDByQuery(driver, dataSet, config, EcryptedToken, ResultantQuery, "Admin", "User", "Create",
					"createDiv");
			Thread.sleep(1000);

		} catch (Exception ex) {
			fc.utobj().quitBrowserOnCatch(driver, ex, testCaseId);
		}
		return val;
	}

	public String GetResultantQuery_R(String query, Map<String, String> dataSet, Map<String, String> UniqueKey) {
		try {
			String format = query;
			HashMap<String, String> map = new HashMap<String, String>();
			Pattern p = Pattern.compile("<([^\\s>/]+)");
			Matcher m = p.matcher(format);
			int i = 0;
			while (m.find()) {
				String tag = m.group(1);
				map.put(String.valueOf(i), tag);
				i++;
			}
			int j = 2;
			while (i > 0) {

				System.out.println(map.get(String.valueOf(j)) + ">>>>>>>>>>>>>>>>>>>>>>"
						+ dataSet.get(map.get(String.valueOf(j))));
				if (dataSet.get(map.get(String.valueOf(j))) != null
						|| UniqueKey.get(map.get(String.valueOf(j))) != null) {
					// if (map.get(String.valueOf(j)).equals("userID"))
					if (UniqueKey.get(map.get(String.valueOf(j))) != null) {
						format = format.replaceAll(
								"(?<=<" + map.get(String.valueOf(j)) + ">).*?(?=</" + map.get(String.valueOf(j)) + ">)",
								UniqueKey.get(map.get(String.valueOf(j))));
						System.out.println(format);
					} else {
						format = format.replaceAll(
								"(?<=<" + map.get(String.valueOf(j)) + ">).*?(?=</" + map.get(String.valueOf(j)) + ">)",
								dataSet.get(map.get(String.valueOf(j))));
					}
				} else {
					format = format.replaceAll(
							"(?<=<" + map.get(String.valueOf(j)) + ">).*?(?=</" + map.get(String.valueOf(j)) + ">)",
							"");
				}
				i--;
				j++;
			}

			System.out.println(format);
			return format;
		} catch (Exception ex) {
			ex.printStackTrace();
			return null;
		}
	}

	public boolean CRUDByQuery(WebDriver driver, Map<String, String> dataSet, Map<String, String> config,
			String ecryptedToken, String resultantQuery, String Module, String SubModule, String Operation,
			String Divtag) {
		try {
			Select DropDownServiceType = new Select(fc.utobj().getElementByID(driver, "serviceType"));
			DropDownServiceType.selectByVisibleText(Operation);
			fc.utobj().sendKeys(driver,
					fc.utobj().getElementByXpath(driver, ".//*[@id='" + Divtag + "']//*[@id='key']"), ecryptedToken);
			Select CModule = new Select(
					fc.utobj().getElementByXpath(driver, "//*[@id='" + Divtag + "']//*[@name='module']"));
			CModule.selectByVisibleText(Module);
			Select CSubModule = new Select(
					fc.utobj().getElementByXpath(driver, "//*[@id='" + Divtag + "']//*[@name='subModule']"));
			CSubModule.selectByVisibleText(SubModule);
			fc.utobj().sendKeys(driver,
					fc.utobj().getElementByXpath(driver, ".//*[@id='" + Divtag + "']//*[@id='xmlString']"),
					resultantQuery);
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//*[@id='" + Divtag + "']//*[@name='Submit']"));

			driver.switchTo().frame("resultIfr");
			Thread.sleep(3000);
			String FinalResult = driver.findElement(By.tagName("responseStatus")).getText();
			driver.switchTo().defaultContent();
			if (FinalResult.equals("Success") || FinalResult.equals("Warning"))
				return true;
			else
				return false;

		} catch (Exception ex) {
			driver.switchTo().defaultContent();
			ex.printStackTrace();
			return false;
		}
	}

	public boolean CRUDByQuery(WebDriver driver, Map<String, String> dataSet, Map<String, String> config,
			String ecryptedToken, String resultantQuery, String Module, String SubModule, String Operation,
			String Divtag, Map<String, String> ReferenceStatus) {
		try {
			Select DropDownServiceType = new Select(fc.utobj().getElementByID(driver, "serviceType"));
			DropDownServiceType.selectByVisibleText(Operation);
			Thread.sleep(2000);
			fc.utobj().sendKeys(driver,
					fc.utobj().getElementByXpath(driver, ".//*[@id='" + Divtag + "']//*[@id='key']"), ecryptedToken);
			Select CModule = new Select(
					fc.utobj().getElementByXpath(driver, "//*[@id='" + Divtag + "']//*[@name='module']"));
			CModule.selectByVisibleText(Module);
			Select CSubModule = new Select(
					fc.utobj().getElementByXpath(driver, "//*[@id='" + Divtag + "']//*[@name='subModule']"));
			CSubModule.selectByVisibleText(SubModule);
			if (Divtag.equals("retrieveDiv")) {
				fc.utobj().sendKeys(driver,
						fc.utobj().getElementByXpath(driver, ".//*[@id='" + Divtag + "']//*[@id='filterXML']"),
						resultantQuery);
			} else {
				fc.utobj().sendKeys(driver,
						fc.utobj().getElementByXpath(driver, ".//*[@id='" + Divtag + "']//*[@id='xmlString']"),
						resultantQuery);
			}
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//*[@id='" + Divtag + "']//*[@name='Submit']"));

			driver.switchTo().frame("resultIfr");
			Thread.sleep(3000);
			String FinalResult = driver.findElement(By.tagName("responseStatus")).getText();
			if (!FinalResult.equals("Error"))
				ReferenceStatus.put(SubModule.toString(), driver.findElement(By.tagName("referenceId")).getText());
			else
				ReferenceStatus.put(SubModule.toString(), "");
			driver.switchTo().defaultContent();
			if (FinalResult.equals("Success") || FinalResult.equals("Warning"))
				return true;
			else
				return false;

		} catch (Exception ex) {
			driver.switchTo().defaultContent();
			ex.printStackTrace();
			return false;
		}
	}

	/*
	 * private String GetResultantQuery(String query, Map<String, String>
	 * dataSet) { String format = query; HashMap<String, String> map = new
	 * HashMap<String, String>(); Pattern p = Pattern.compile("<([^\\s>/]+)");
	 * Matcher m = p.matcher(format); int i = 0; while (m.find()) { String tag =
	 * m.group(1); map.put(String.valueOf(i), tag); i++; } int j = 2; while (i >
	 * 0) {
	 * 
	 * System.out.println( map.get(String.valueOf(j)) + ">>>>>>>>>>>>>>>>>>>>>>"
	 * + dataSet.get(map.get(String.valueOf(j)))); if
	 * (dataSet.get(map.get(String.valueOf(j))) != null) { if
	 * (map.get(String.valueOf(j)).equals("userID")) { format =
	 * format.replaceAll( "(?<=<" + map.get(String.valueOf(j)) + ">).*?(?=</" +
	 * map.get(String.valueOf(j)) + ">)",
	 * dataSet.get(map.get(String.valueOf(j))) +
	 * fc.utobj().generateRandomNumber()); System.out.println(format); } else {
	 * format = format.replaceAll( "(?<=<" + map.get(String.valueOf(j)) +
	 * ">).*?(?=</" + map.get(String.valueOf(j)) + ">)",
	 * dataSet.get(map.get(String.valueOf(j)))); } } else { format =
	 * format.replaceAll( "(?<=<" + map.get(String.valueOf(j)) + ">).*?(?=</" +
	 * map.get(String.valueOf(j)) + ">)", ""); } i--; j++; }
	 * 
	 * System.out.println(format); return format; }
	 */

	public String GetQuery(WebDriver driver, Map<String, String> dataSet, Map<String, String> config,
			String ecryptedToken, String roleType, String module, String subModule, String operation) {
		try {
			;
			Select DropDownServiceType = new Select(fc.utobj().getElementByID(driver, "serviceType"));
			DropDownServiceType.selectByVisibleText("Query");
			;
			fc.utobj().sendKeys(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='queryDiv']//*[@id='key']"),
					ecryptedToken);
			;
			Select Module = new Select(fc.utobj().getElementByXpath(driver, "//*[@id='queryDiv']//*[@name='module']"));
			Module.selectByVisibleText(module);
			;
			Select SubModule = new Select(
					fc.utobj().getElementByXpath(driver, "//*[@id='queryDiv']//*[@name='subModule']"));
			SubModule.selectByVisibleText(subModule);
			;
			Select Operation = new Select(
					fc.utobj().getElementByXpath(driver, "//*[@id='queryDiv']//*[@name='operation']"));
			Operation.selectByVisibleText(operation);
			;
			if (roleType != null) {
				Select roleTypeForQuery = new Select(
						fc.utobj().getElementByXpath(driver, "//*[@id='queryDiv']//*[@name='roleTypeForQuery']"));
				roleTypeForQuery.selectByVisibleText(roleType);
			}
			;
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//*[@id='queryDiv']//*[@id='Submit']"));
			driver.switchTo().frame("resultIfr");
			String str1 = driver.getPageSource();
			System.out.println(str1);
			int firstOpenP = str1.indexOf("<fcRequest");
			int lastOpenP = str1.lastIndexOf("fcRequest>");
			String format = str1.substring(firstOpenP, lastOpenP + 10);
			driver.switchTo().defaultContent();
			System.out.println(format);
			return format;
		} catch (Exception ex) {
			driver.switchTo().defaultContent();
			ex.printStackTrace();
		}
		return null;
	}

	public String GetEcryptedToken(WebDriver driver, Map<String, String> dataSet, Map<String, String> config,
			String SecureToken) {
		try {
			;
			Select DropDownServiceType = new Select(fc.utobj().getElementByID(driver, "serviceType"));
			DropDownServiceType.selectByVisibleText("Get Encrypted Token");
			Thread.sleep(2000);
			DataServicePage de = new DataServicePage(driver);
			;
			fc.utobj().sendKeys(driver, de.SecureToken, SecureToken);
			;
			fc.utobj().sendKeys(driver,
					fc.utobj().getElementByXpath(driver, ".//*[@id='getEncTokenDiv']//*[@id='clientCode']"),
					dataSet.get("clientcode"));
			;
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//*[@id='getEncTokenDiv']//*[@name='Submit']"));
			driver.switchTo().frame("resultIfr");
			String enctext = fc.utobj().getElementByXpath(driver, ".//html/body").getText();
			System.out.println(enctext);
			int index = enctext.indexOf(":");
			String str = enctext.substring(index + 2, enctext.length());
			driver.switchTo().defaultContent();
			System.out.println(str);
			return str;
		} catch (Exception ex) {
			driver.switchTo().defaultContent();
		}
		return null;
	}

	public String GetSecureToken(WebDriver driver, Map<String, String> dataSet, Map<String, String> config) {
		try {
			;
			Select DropDownServiceType = new Select(fc.utobj().getElementByID(driver, "serviceType"));
			DropDownServiceType.selectByVisibleText("Login");
			;
			fc.utobj().sendKeys(driver,
					fc.utobj().getElementByXpath(driver, ".//*[@id='loginDiv']//*[@id='clientCode']"),
					dataSet.get("clientcode"));
			;
			Select DropDownResponseType = new Select(fc.utobj().getElementByID(driver, "responseType"));
			DropDownResponseType.selectByVisibleText("XML");
			;
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//*[@id='loginDiv']//*[@id='Submit']"));
			;
			driver.switchTo().frame("resultIfr");
			String text = driver.findElement(By.tagName("secureToken")).getText();
			driver.switchTo().defaultContent();
			System.out.println(text);
			return text;
		} catch (Exception e) {
			driver.switchTo().defaultContent();
			e.printStackTrace();
			return null;
		}

	}

	public void clickElement(WebDriver driver, WebElement element) throws Exception {

		fc.utobj().moveToElement(driver, element);
		try {

			element.click();
			// analyzeLog(driver);
		} catch (Exception e) {
			throwsException("Element not clickable : " + element);
		}
	}

	public String throwsException(String exceptionMsg) throws Exception {
		Reporter.log("");
		throw new Exception(exceptionMsg);
	}

	public String addFranchise(WebDriver driver, Map<String, String> config, Map<String, String> dataSet,
			Map<String, String> UniqueKey, String referenceID) {
		try {
			UniqueKey.put("status", "No");
			UniqueKey.put("franchiseeName", referenceID);
			driver.navigate().to(config.get("buildUrl"));
			driver = fc.loginpage().login(driver);
			String region = "region" + fc.utobj().generateRandomNumber();
			AdminAreaRegionAddAreaRegionPageTest addRegion = new AdminAreaRegionAddAreaRegionPageTest();
			addRegion.addAreaRegion(driver, region);
			UniqueKey.put("centerName", "Center" + fc.utobj().generateRandomNumber());
			UniqueKey.put("areaID", region);
			UniqueKey.put("openingDate", "10/10/2017");
			UniqueKey.put("", "");
			UniqueKey.put("", "");
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return null;
	}
}
