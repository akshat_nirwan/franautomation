package com.builds.test.crm;

import java.io.FileInputStream;
import java.io.FileOutputStream;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.testng.annotations.Test;

public class Temp {

	@Test
	public void tem() throws Exception {

		String fileName = "C:\\Users\\Inzmam\\Desktop\\temp\\Test Report.xls";// Test
																				// Report
		System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>" + fileName);
		String basicFileName = "C:\\Users\\Inzmam\\Desktop\\temp\\Test_Case_Status.xls";
		System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>" + basicFileName);

		FileInputStream fis = new FileInputStream(basicFileName);
		Workbook wb = WorkbookFactory.create(fis);
		Sheet sh = wb.getSheetAt(0);

		FileInputStream fis1 = new FileInputStream(fileName);
		Workbook wb1 = WorkbookFactory.create(fis1);

		for (int x = 7; x < sh.getLastRowNum() + 1; x++) {
			Row row = sh.getRow(x);
			Cell cellTCID = row.getCell(0);
			String basicTCID = cellTCID.getStringCellValue();
			Cell cellTestStep = row.getCell(2);
			Cell cellTestStatus = row.getCell(3);
			Cell cellScreeShot = row.getCell(4);
			Cell cellException = row.getCell(5);

			for (int p = 0; p < wb1.getNumberOfSheets(); p++) {
				Sheet sh1 = wb1.getSheetAt(p);

				System.out.println(sh1.getSheetName());

				for (int q = 9; q < sh1.getLastRowNum() + 1; q++) {
					Row r = sh1.getRow(q);
					Cell cell0 = null;
					try {
						cell0 = r.getCell(0);
						String tc_id = cell0.getStringCellValue();
						if (basicTCID != null && tc_id != null && basicTCID.equalsIgnoreCase(tc_id)) {

							System.out.println(basicTCID + " - " + tc_id);

							try {
								Cell cellTestStep1 = r.createCell(2);
								cellTestStep1.setCellValue(cellTestStep.getStringCellValue());
								System.out.println(cellTestStep1.getStringCellValue());
							} catch (Exception e) {

							}

							try {
								Cell cellTestStatus1 = r.createCell(3);
								cellTestStatus1.setCellValue(cellTestStatus.getStringCellValue());

								System.out.println(cellTestStatus1.getStringCellValue());
							} catch (Exception e) {

							}

							try {
								Cell cellScreeShot1 = r.createCell(4);
								cellScreeShot1.setCellValue(cellScreeShot.getStringCellValue());
								System.out.println(cellScreeShot1.getStringCellValue());
							} catch (Exception e) {

							}

							try {
								Cell cellException1 = r.createCell(5);
								cellException1.setCellValue(cellException.getStringCellValue());
								System.out.println(cellException1.getStringCellValue());
							} catch (Exception e) {

							}

							q = sh1.getLastRowNum();
							p = wb1.getNumberOfSheets();
						}
					} catch (Exception e) {

					}
				}

			}
		}

		for (int z = 0; z < wb1.getNumberOfSheets(); z++) {

			Sheet shtemp = wb1.getSheetAt(z);

			shtemp.autoSizeColumn(0);
			shtemp.autoSizeColumn(1);
			shtemp.autoSizeColumn(2);
			shtemp.autoSizeColumn(3);
			shtemp.autoSizeColumn(4);
			shtemp.autoSizeColumn(5);
			shtemp.autoSizeColumn(6);

			int totalTC = 0;
			int passedTC = 0;
			int skippedTC = 0;
			int failedTC = 0;

			for (int j = 9; j < shtemp.getLastRowNum() + 1; j++) {
				totalTC = totalTC + 1;

				Row tcrow = shtemp.getRow(j);
				Cell cellc = tcrow.getCell(3);
				if (cellc != null && cellc.getStringCellValue().equalsIgnoreCase("Passed")) {
					passedTC = passedTC + 1;
				} else if (cellc != null && cellc.getStringCellValue().equalsIgnoreCase("Failed")) {
					failedTC = failedTC + 1;
				} else if (cellc != null && cellc.getStringCellValue().equalsIgnoreCase("Pending")) {
					skippedTC = skippedTC + 1;
				}
			}

			System.out.println(totalTC);
			System.out.println(passedTC);
			System.out.println(failedTC);
			System.out.println(skippedTC);

			Row row2 = shtemp.getRow(2);
			Cell row2cell1 = row2.getCell(1);
			String total = row2cell1.getStringCellValue();
			row2cell1.setCellValue(total + "" + totalTC);

			Row row3 = shtemp.getRow(3);
			Cell row3cell1 = row3.getCell(1);
			String Passedtotal = row3cell1.getStringCellValue();
			row3cell1.setCellValue(Passedtotal + "" + passedTC);

			Row row4 = shtemp.getRow(4);
			Cell row4cell1 = row4.getCell(1);
			String pendingtotal = row4cell1.getStringCellValue();
			row4cell1.setCellValue(pendingtotal + "" + skippedTC);

			Row row5 = shtemp.getRow(5);
			Cell row5cell1 = row5.getCell(1);
			String failedtotal = row5cell1.getStringCellValue();
			row5cell1.setCellValue(failedtotal + "" + failedTC);

		}

		System.out.println(fileName);
		FileOutputStream fout = new FileOutputStream(fileName);
		wb1.write(fout);
		fout.close();
		fout.flush();
		wb.close();

	}
}
