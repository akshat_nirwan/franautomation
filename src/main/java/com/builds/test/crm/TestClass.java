package com.builds.test.crm;

import java.io.IOException;
import java.util.Map;

import javax.xml.parsers.ParserConfigurationException;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Test;
import org.xml.sax.SAXException;

import com.builds.test.admin.AdminUsersManageCorporateUsersAddCorporateUserPageTest;
import com.builds.test.common.CorporateUser;
import com.builds.uimaps.crm.CRMLeadsPage;
import com.builds.utilities.FranconnectUtil;
import com.builds.utilities.TestCase;

public class TestClass {
	
	FranconnectUtil fc = new FranconnectUtil();
	
	@Test(groups = { "crm555", "crmCheck112" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2017-06-26", testCaseDescription = "Verify The Lead Associate with email campaign By Action Button Option At CRM > Lead > Lead Summary", testCaseId = "TC_73_Associate_With_Email_Campaign")
	public void AssociateWithEmailCompaignActionBtn()
			throws IOException, Exception, ParserConfigurationException, SAXException {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			
			
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}
}
