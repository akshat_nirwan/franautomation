package com.builds.test.crm; 


import java.util.List;
import java.util.Map;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.Test;

import com.builds.test.admin.AdminUsersManageCorporateUsersAddCorporateUserPageTest;
import com.builds.test.common.CRMModule;
import com.builds.test.common.CorporateUser;
import com.builds.test.common.FCHomePageTest;
import com.builds.uimaps.common.FCHomePage;
import com.builds.uimaps.crm.CRMTasksPage;
import com.builds.uimaps.crm.CRM_CampaignFinalandLunchUI;
import com.builds.uimaps.crm.CRM_CampaignSelectRecipientUI;
import com.builds.uimaps.crm.CRM_CreateWorkFlowPageUI;
import com.builds.uimaps.crm.CRM_LeadinfopageUI;
import com.builds.uimaps.crm.CRM_SendEmailUI;
import com.builds.uimaps.crm.CRM_Verify_TaskUI;
import com.builds.uimaps.crm.LeadUI;
import com.builds.uimaps.crm.LeadsummarypageUI;
import com.builds.utilities.FranconnectUtil;
import com.builds.utilities.TestCase;

public class vipin_test {

	FranconnectUtil fc = new FranconnectUtil();
	

	@Test(groups = "MI154654")
	@TestCase(createdOn = "2018-05-07", updatedOn = "2018-05-07", testCaseDescription = "CRM > Lead Summary >add lead type when add lead ", testCaseId = "TC-12968_Add_Lead type when add lead ")
	public void leadInfopage32() throws Exception {


		FranconnectUtil fc = new FranconnectUtil();
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		
			
		try {
			driver = fc.loginpage().login(driver);
			fc.utobj().printTestStep("Navigate To Admin Users Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			String emailId = "crmautomation@staffex.com";
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUser = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser.setUserName(fc.utobj().generateTestData("corp"));
			corpUser.setPassword("fran1234");
			String username=corpUser.getUserName();
			String password=corpUser.getPassword();
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporateUser.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);
			fc.loginpage().loginWithParameter(driver, username, password);
			
			fc.crm().crm_common().CRMTasksLnk(driver);
			/*fc.utobj().printTestStep("Navigate To CRM > Tasks");
			fc.utobj().printTestStep("Add Task");
			fc.utobj().clickElement(driver, pobj.taskLinks);*/
			CRM_Verify_TaskUI a=new CRM_Verify_TaskUI(driver);
			
			fc.utobj().clickElement(driver,a.arrow);
			
		
			
			fc.utobj().clickElement(driver,"//*[@id='showNotification']");
			
			fc.utobj().clickElement(driver,"//a[@href='viewAllNotifications?viewCriteria=16']");
			
			boolean b=fc.utobj().isElementPresent(driver,"//b[contains(text(),'dfg')]");
			 if(b==false) {
				 fc.utobj().throwsException("abc");
			 }
			
			 boolean b1=fc.utobj().isElementPresent(driver,"//b[contains(text(),'TestSubjectc21145967')]");
			 if(b1==false) {
				 fc.utobj().throwsException("abc");
			 }
			 boolean b2=fc.utobj().isElementPresent(driver,"//b[contains(text(),'TestSubjecte21145661')]");
			 if(b2==false) {
				 fc.utobj().throwsException("abc");
			 }
			 boolean b3=fc.utobj().isElementPresent(driver,"//b[contains(text(),'TestSubjecto21145350')]");
			 if(b3==false) {
				 fc.utobj().throwsException("abc");
			 }
			 
			 
			
			/*fc.utobj().printTestStep("Navigate To Admin Users Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			String emailId = "crmautomation@staffex.com";
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUser = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser = new CorporateUser();
			corpUser.setEmail(emailId);
			corpUser.setUserName("admin123");
			corpUser.setPassword("fran123");
			String username=corpUser.getUserName();
			String password=corpUser.getPassword();
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporateUser.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);
			fc.loginpage().loginWithParameter(driver, username, password);*/
/*
			CRM_Verify_TaskUI taskui=new CRM_Verify_TaskUI(driver);
			
			fc.utobj().clickElement(driver,taskui.User);
			fc.utobj().clickElement(driver,taskui.Options);
			fc.utobj().moveToElement(driver,taskui.More);
			fc.utobj().doubleClickElement(driver, taskui.More);
			fc.utobj().clickElement(driver, taskui.More);
			fc.utobj().clickElement(driver, taskui.More);
			fc.utobj().clickElement(driver,taskui.Notifications);
			if(!fc.utobj().isSelected(driver, taskui.testcreationcheckbox)) {
				fc.utobj().clickElement(driver,taskui.testcreationcheckbox);
			}
			fc.utobj().clickElement(driver, taskui.save);*/
			
			/*
			fc.utobj().clickElement(driver,"//a[@class='userx']");
			fc.utobj().clickElement(driver,"//a[contains(text(),'Options')]");
			fc.utobj().moveToElement(driver,"//td[contains(text(),'More')]");
			fc.utobj().clickElement(driver, "//td[contains(text(),'More')]");
			fc.utobj().clickElement(driver, "//td[contains(text(),'More')]");
			fc.utobj().clickElement(driver,"//*[@qat_tabname='Notifications']");*/
			
			/*//Verify
			fc.crm().crm_common().CRMTasksLnk(driver);

			fc.utobj().clickElement(driver,"//*[@id='showNotification']");
			boolean b=fc.utobj().isElementPresent(driver,"//b[contains(text(),'tasksubject')]");
			 if(b==false) {
				 fc.utobj().throwsException("abc");
			 }*/
			
			
			
			
			/*CRMModule crm = new CRMModule(); 
			fc.crm().crm_common().adminCRMConfigureLeadTypeLnk(driver);
			AdminCRMConfigureLeadTypePageTest type = new AdminCRMConfigureLeadTypePageTest();
			String LeadType = fc.utobj().generateTestData("SeleType");
			type.addLeadType(driver, LeadType);
			
			
			crm.crm_common().openCRMLeadsPage(fcHomePageTest, driver).clickAddLead(driver);
			Leadgetset lead = new Leadgetset();
			lead = crm.crm_common().getDefaultDataForLead(lead);
			Addleadpage addlead = new Addleadpage();
			addlead.fillLeadDetails(driver, lead);
			LeadUI leadui = new LeadUI(driver);
			fc.utobj().selectDropDown(driver, leadui.LeadType, LeadType);
			addlead.save(driver);
			*/
			
				/*fc.utobj().logoutAndQuitBrowser(driver, testCaseId);*/

		} catch (Exception e) {
				/*fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);*/
			}
	}

}



			 
			 
			 
			 