package com.builds.test.crmtemp;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.Select;

import com.builds.uimaps.crmtemp.AddLeadPageUI;
import com.builds.utilities.FranconnectUtil;

public class AddLeadPage {
	
	WebDriver driver;
	FranconnectUtil fc=new FranconnectUtil();

	public AddLeadPage(WebDriver driver) {
		this.driver=driver;
	}
	
	
	public LeadInfoPage filldetailslead(FillDetailsOfLead details) throws Exception {
		
	/*	Obsolete method
	 * Select select =new Select(driver.findElement(By.name("leadOwnerID2")));
	select.selectByIndex(1);*/
		
		
	AddLeadPageUI add=new AddLeadPageUI(driver);
	
	if(details.getTitle()!=null)
	{
		fc.utobj().selectDropDownByVisibleText(driver,add.title,details.getTitle());
	}
	if(details.getFirstName()!=null)
	{
		fc.utobj().sendKeys(driver,add.leadFirstName,details.getFirstName());
	}
	if(details.getLastName()!=null)
	{
		fc.utobj().sendKeys(driver,add.leadLastName,details.getLastName());
	}
	if(details.getPrimaryContact()!=null)
	{
		fc.utobj().selectDropDownByVisibleText(driver,add.primaryContactMethod,details.getPrimaryContact());
	}
	if(details.getCompany()!=null)
	{
		fc.utobj().sendKeys(driver,add.companyName,details.getCompany());
	}
	
	
	/*if(lead.getAssignTo()!=null && lead.getAssignTo().equalsIgnoreCase("Corporate"))
	{
		fc.utobj().clickElement(driver, ui.corporateOwnerType);
		fc.utobj().selectDropDownByVisibleText(driver, ui.ownerName, lead.getCorporateUser());
	}else if(lead.getAssignTo()!=null && lead.getAssignTo().equalsIgnoreCase("Regional"))
	{
		fc.utobj().clickElement(driver, ui.RegionalOwnerType);
	}*/
	
	
	
	if(details.getAssignTo()!=null && details.getAssignTo().equalsIgnoreCase("Corporate"))
	{
		fc.utobj().clickElement(driver,add.Corporate);
		System.out.println("Clicked on Assigned to");
		fc.utobj().selectDropDownByVisibleText(driver,add.CorporateUser,details.getCorporatUser());
	}
	else if(details.getAssignTo()!=null && details.getAssignTo().equalsIgnoreCase("Regional"))
	{
		fc.utobj().clickElement(driver,add.Regional);
	}
	
	if(details.getFax()!=null)
	{
		fc.utobj().sendKeys(driver,add.faxNumbers,details.getFax());
	}
	
	if(details.getEmail()!=null)
	{
		fc.utobj().sendKeys(driver,add.emailIds,details.getEmail());
	}
	
	fc.utobj().clickElement(driver,add.save);
	
	return new LeadInfoPage(driver);
}
	
	
}

