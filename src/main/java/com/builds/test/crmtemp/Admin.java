package com.builds.test.crmtemp;

import org.openqa.selenium.WebDriver;

import com.builds.uimaps.crm.AdminUI;
import com.builds.utilities.FranconnectUtil;

public class Admin {

	WebDriver driver;
	FranconnectUtil fc=new FranconnectUtil();
	
	public Admin(WebDriver driver) {
		this.driver=driver;
	}

	public ManageCorporateUsers ClickOnCorporateUser() throws Exception
	{
		AdminUI admin=new AdminUI(driver);
		fc.utobj().clickElement(driver,admin.CorporateUser);
		return new ManageCorporateUsers(driver);
	}
	
	
}
