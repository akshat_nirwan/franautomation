package com.builds.test.crmtemp;

import org.openqa.selenium.WebDriver;

import com.builds.uimaps.common.FCHomePage;
import com.builds.uimaps.crmtemp.CRMModuleUI;
import com.builds.utilities.FranconnectUtil;

public class CRMModule {
	
	WebDriver driver;
	FranconnectUtil fc=new FranconnectUtil();
	
	public CRMModule(WebDriver driver)
	{
		this.driver=driver;
	}
	
	public LeadSummary clickOnLead() throws Exception
	{
		CRMModuleUI clickonlead=new CRMModuleUI(driver);
		fc.utobj().printTestStep("Click on Lead");
		fc.utobj().clickElement(driver,clickonlead.ClickOnLead);
		return new LeadSummary(driver);
		
	}
}
