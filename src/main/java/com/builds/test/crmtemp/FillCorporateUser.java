package com.builds.test.crmtemp;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.builds.uimaps.crm.LeadUI;
import com.builds.uimaps.crmtemp.FillCorporateUserUI;
import com.builds.utilities.FranconnectUtil;

public class FillCorporateUser {

	WebDriver driver;
	FranconnectUtil fc=new FranconnectUtil();
	
	public FillCorporateUser(WebDriver driver) {
		this.driver=driver;	
	}
	
	public void filldetails(FillCorporateUserPOJO corporate) throws Exception
	{
		String FirstName=corporate.getFirstname();
		String LastName=corporate.getLastname();
		
		FillCorporateUserUI fillcorporateuserui=new FillCorporateUserUI(driver);
		if(corporate.getLoginid()!=null)
		{
			fc.utobj().sendKeys(driver,fillcorporateuserui.userName,corporate.getLoginid());
		}
		
		if(corporate.getPassword()!=null)
		{
			fc.utobj().sendKeys(driver,fillcorporateuserui.password,corporate.getLoginid());
		}
		
		if(corporate.getConfirmpassword()!=null)
		{
			fc.utobj().sendKeys(driver,fillcorporateuserui.confirmPassword,corporate.getLoginid());
		}
		
		
		if(corporate.getTimezone()!=null)
		{
			fc.utobj().selectDropDownByVisibleText(driver,fillcorporateuserui.timezone,corporate.getTimezone());
		}
		
		if(corporate.getFirstname()!=null)
		{
			
			fc.utobj().sendKeys(driver,fillcorporateuserui.firstName,FirstName);
		}
		
		if(corporate.getLastname()!=null)
		{
			
			fc.utobj().sendKeys(driver,fillcorporateuserui.lastName,LastName);
		}
		
		if(corporate.getCity()!=null)
		{
			fc.utobj().sendKeys(driver,fillcorporateuserui.city,corporate.getCity());
		}
		
		if(corporate.getState()!=null)
		{
			fc.utobj().selectDropDownByVisibleText(driver,fillcorporateuserui.state,corporate.getState());
		}
		
		if(corporate.getPhone1()!=null)
		{
			fc.utobj().sendKeys(driver,fillcorporateuserui.phone1,corporate.getPhone1());
		}
		
		if(corporate.getEmail()!=null)
		{
			fc.utobj().sendKeys(driver,fillcorporateuserui.email,corporate.getEmail());
		}
		
		if(corporate.getRole()!=null)
		{
			fc.utobj().printTestStep("click on multiselect_dropdown option");
			fc.utobj().clickElement(driver, "//span[@class='placeholder']");
			fc.utobj().printTestStep("search value name");
			fc.utobj().sendKeys(driver, fillcorporateuserui.Searchtab_insideGroup,corporate.getRole());
			Thread.sleep(500);
			fc.utobj().clickElement(driver, fillcorporateuserui.Searchbtn_insideGroup);
			Thread.sleep(1000);
			fc.utobj().clickElement(driver, fillcorporateuserui.checkbox_insideGroup);

		}
		
		
		
		
			fc.utobj().clickElement(driver,fillcorporateuserui.Submit);
	}

}
