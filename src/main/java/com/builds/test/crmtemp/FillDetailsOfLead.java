package com.builds.test.crmtemp;

public class FillDetailsOfLead {

	private String Title;
	private String FirstName;
	private String LastName;
	private String PrimaryContact;
	private String Company;
	private String AssignTo;
	private String CorporatUser;
	private String Fax;
	private String Email;
	
	public String getFirstName() {
		return FirstName;
	}
	public void setFirstName(String firstName) {
		FirstName = firstName;
	}
	public String getLastName() {
		return LastName;
	}
	public void setLastName(String lastName) {
		LastName = lastName;
	}
	public String getTitle() {
		return Title;
	}
	public void setTitle(String title) {
		Title = title;
	}
	public String getPrimaryContact() {
		return PrimaryContact;
	}
	public void setPrimaryContact(String primaryContact) {
		PrimaryContact = primaryContact;
	}
	public String getCompany() {
		return Company;
	}
	public void setCompany(String company) {
		Company = company;
	}
	public String getAssignTo() {
		return AssignTo;
	}
	public void setAssignTo(String assignTo) {
		AssignTo = assignTo;
	}
	public String getCorporatUser() {
		return CorporatUser;
	}
	public void setCorporatUser(String corporatUser) {
		CorporatUser = corporatUser;
	}
	public String getFax() {
		return Fax;
	}
	public void setFax(String fax) {
		Fax = fax;
	}
	public String getEmail() {
		return Email;
	}
	public void setEmail(String email) {
		Email = email;
	}
}

