package com.builds.test.crmtemp;

import java.util.Map;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Test;

import com.builds.test.common.FCHomePageTest;
import com.builds.test.crm.LoginUI;
import com.builds.uimaps.crm.HomePageUI;
import com.builds.uimaps.crmtemp.LeadInfoPageUI;
import com.builds.utilities.FranconnectUtil;
import com.builds.utilities.TestCase;

public class FillLeadDetails {
	
	
	FranconnectUtil fc=new FranconnectUtil();	
	@Test(groups = "lead_vipin")
	@TestCase(createdOn = "2018-07-19", updatedOn = "2018-07-19", testCaseDescription = "Fill Details of lead", testCaseId = "TC_01_fill_detail_lead")
	public void login() throws Exception
	{
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		fc.utobj().readTestData("crm", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		try {			
		driver = fc.loginpage().login(driver);
		
		//Open Admin
		
		FillCorporateUserPOJO fillcorporateuserpojo=new FillCorporateUserPOJO();
		
		fillcorporateuserpojo.setLoginid(fc.utobj().generateTestData("loginuser"));
		fillcorporateuserpojo.setPassword("passw0rd");
		fillcorporateuserpojo.setConfirmpassword("passw0rd");
		fillcorporateuserpojo.setRole("Corporate Administrator");
		fillcorporateuserpojo.setTimezone("GMT +05:30 India");
		fillcorporateuserpojo.setFirstname(fc.utobj().generateTestData("Vipin"));
		fillcorporateuserpojo.setLastname(fc.utobj().generateTestData("Grover"));
		fillcorporateuserpojo.setCity("Rewari");
		fillcorporateuserpojo.setState("Indiana");
		fillcorporateuserpojo.setPhone1("8901465286");
		fillcorporateuserpojo.setEmail("Vipin.Grover@franvonnect.com");
		
		String FirstName=fillcorporateuserpojo.getFirstname();
		String LastName=fillcorporateuserpojo.getLastname();
		
		String FullName=FirstName+" "+LastName;
		
		HomePage homepage=new HomePage(driver);
		homepage.ClickOnAdmin().ClickOnCorporateUser().AddCorporateUser().filldetails(fillcorporateuserpojo);
		
		//Write yours code here
		//Open CRM Module
		fc.commonMethods().getModules().openCRMModule(driver);
		CRMModule crm=new CRMModule(driver);
		FillDetailsOfLead fill=new FillDetailsOfLead();
		fill.setTitle("Mr.");
		fill.setFirstName("Vipin");
		fill.setLastName("Grover");
		fill.setPrimaryContact("Email");
		fill.setCompany("First Company");
		fill.setAssignTo("Corporate");
		fill.setCorporatUser(FullName);
		fill.setFax("1234567890");
		fill.setEmail("Vipin.Grover@franconnectt.com");
		
		String FirstNameOfLead=fill.getFirstName();
		String LastNameOfLead=fill.getLastName();
		crm.clickOnLead().ClickOnAddNew().filldetailslead(fill);
		
		//Verify Lead is added 
		LeadInfoPageUI leadpageinfopageui=new LeadInfoPageUI(driver);
		if(leadpageinfopageui.ActivityHistory.getText().contains("Lead is added through FranConnect Application."))
		{
			fc.utobj().printTestStep("Lead is added");
			System.out.println("Lead is added");
		}
		
		if(leadpageinfopageui.FirstNameText.getText().contains(FirstNameOfLead))
		{
			fc.utobj().printTestStep("Lead is added with verification as First Name");
			System.out.println("Lead is added with verification as First Name");
		}
		
		if(leadpageinfopageui.LastNameText.getText().contains(LastNameOfLead))
		{
			fc.utobj().printTestStep("Lead is added with verification as Last Name");
			System.out.println("Lead is added with verification as Last Name");
		}
		
		fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		}
		catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}

	}
	
}
