package com.builds.test.crmtemp;

import org.openqa.selenium.WebDriver;

import com.builds.uimaps.crm.HomePageUI;
import com.builds.utilities.FranconnectUtil;

public class HomePage {

	WebDriver driver;
	FranconnectUtil fc=new FranconnectUtil();
	public HomePage(WebDriver driver)
	{
		this.driver=driver;
	}
	
	public Admin ClickOnAdmin() throws Exception
	{
		HomePageUI homepageui=new HomePageUI(driver);
		fc.utobj().clickElement(driver,homepageui.FranconnectAdministrator);
		fc.utobj().clickElement(driver,homepageui.Admin);
		return new Admin(driver);
	}
	
}
