package com.builds.test.crmtemp;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.builds.uimaps.crmtemp.LeadSummaryUI;
import com.builds.utilities.FranconnectUtil;

public class LeadSummary {
	
	FranconnectUtil fc=new FranconnectUtil();
	WebDriver driver;
	public LeadSummary(WebDriver driver) {
		this.driver=driver;
	}

	public AddLeadPage ClickOnAddNew() throws Exception
	{
		LeadSummaryUI leadsummaryui=new LeadSummaryUI(driver);
		fc.utobj().printTestStep("click on Add New");
		fc.utobj().clickElement(driver,leadsummaryui.addnew);
		fc.utobj().printTestStep("click on Lead");
		
		List<WebElement> options=driver.findElements(By.xpath("//*[contains(@onclick,'javascript:quickLinkContact')]"));
		
		for(WebElement opt: options)
		{
			if(opt.getText().equalsIgnoreCase("Lead"))
			{
				fc.utobj().clickElement(driver,opt);
				break;
			}
		}
		
		return new AddLeadPage(driver);
	}
	
}
