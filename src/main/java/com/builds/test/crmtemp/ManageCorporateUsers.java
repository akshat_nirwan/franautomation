package com.builds.test.crmtemp;

import org.openqa.selenium.WebDriver;

import com.builds.uimaps.crmtemp.AddCorporateUserUI;
import com.builds.utilities.FranconnectUtil;

public class ManageCorporateUsers {

	WebDriver driver;
	FranconnectUtil fc=new FranconnectUtil();
	public ManageCorporateUsers(WebDriver driver) {
		this.driver=driver;
	}
	
	public FillCorporateUser AddCorporateUser() throws Exception
	{
		AddCorporateUserUI addcorporateuserui=new AddCorporateUserUI(driver);
		fc.utobj().clickElement(driver,addcorporateuserui.addcorporateuser);
		return new FillCorporateUser(driver);
	}

}
