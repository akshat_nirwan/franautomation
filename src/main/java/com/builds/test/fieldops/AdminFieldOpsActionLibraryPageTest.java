package com.builds.test.fieldops;

import java.util.Map;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Test;

import com.builds.uimaps.fieldops.AdminFieldOpsActionLibraryPage;
import com.builds.utilities.FranconnectUtil;
import com.builds.utilities.TestCase;

public class AdminFieldOpsActionLibraryPageTest {

	FranconnectUtil fc = new FranconnectUtil();

	@Test(groups = { "fieldops", "fieldopsTest" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Add , Modify And Delete Action Item in Action Library", testCaseId = "TC_31_Add_Action_Item")
	public void addActionItem() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("fieldops", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			AdminFieldOpsActionLibraryPage pobj = new AdminFieldOpsActionLibraryPage(driver);

			fc.utobj().printTestStep("Navigate To Admin > FieldOps > Action Library");
			fc.utobj().printTestStep("Add Action Item");
			String subject = addActionItem(driver, dataSet);

			fc.utobj().printTestStep("Search subject of Action item");
			fc.utobj().sendKeys(driver, pobj.auditAdminTopSearchString, subject);
			fc.utobj().clickElement(driver, pobj.searchActionItem);
			fc.utobj().printTestStep("Verify Add Action Item");

			boolean isSubjectPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[contains(text () , '" + subject + "')]");

			if (isSubjectPresent == false) {
				fc.utobj().throwsException("was not able to verify subject of action item");
			}

			boolean isDescriptionPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[contains(text () , '" + subject
					+ "')]/ancestor::tr/td[contains(text () , '" + dataSet.get("description") + "')]");

			if (isDescriptionPresent == false) {
				fc.utobj().throwsException("was not able to verify description of action item");
			}

			boolean isDueDatePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[contains(text () , '" + subject
					+ "')]/ancestor::tr/td[contains(text () , '" + dataSet.get("scheduleCompletionDays") + "')]");
			if (isDueDatePresent == false) {
				fc.utobj().verifyElementOnVisible_ByXpath(driver, "was not verify Schedule Completion Within Day(s)");
			}

			boolean isPriorityPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[contains(text () , '" + subject
					+ "')]/ancestor::tr/td[contains(text () , '" + dataSet.get("priority") + "')]");
			if (isPriorityPresent == false) {
				fc.utobj().verifyElementOnVisible_ByXpath(driver, "was not verify Priority");
			}

			fc.utobj().printTestStep("Modify Action Item");
			fc.utobj().actionImgOption(driver, subject, "Modify");
			fc.commonMethods().switch_cboxIframe_frameId(driver);

			String subjectM = fc.utobj().generateTestData(dataSet.get("subject"));

			fc.utobj().sendKeys(driver, pobj.subject, subjectM);

			fc.utobj().selectDropDown(driver, pobj.selectPriority, dataSet.get("priority"));
			fc.utobj().sendKeys(driver, pobj.scheduleCompletionDays, dataSet.get("scheduleCompletionDays"));
			fc.utobj().sendKeys(driver, pobj.description, dataSet.get("description"));
			fc.utobj().clickElement(driver, pobj.addBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Filter Records by View Per Page Option");
			fc.utobj().selectDropDown(driver, pobj.resultsPerPage, "500");

			fc.utobj().printTestStep("Verify The Modify Action Item");

			boolean isSubjectMPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[contains(text () , '" + subjectM + "')]");

			if (isSubjectMPresent == false) {
				fc.utobj().throwsException("was not able to verify subject of action item");
			}

			boolean isDescriptionMPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[contains(text () , '" + subjectM
					+ "')]/ancestor::tr/td[contains(text () , '" + dataSet.get("description") + "')]");

			if (isDescriptionMPresent == false) {
				fc.utobj().throwsException("was not able to verify description of action item");
			}

			boolean isDueDateMPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[contains(text () , '" + subjectM
					+ "')]/ancestor::tr/td[contains(text () , '" + dataSet.get("scheduleCompletionDays") + "')]");
			if (isDueDateMPresent == false) {
				fc.utobj().verifyElementOnVisible_ByXpath(driver, "was not verify Schedule Completion Within Day(s)");
			}

			boolean isPriorityMPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[contains(text () , '" + subjectM
					+ "')]/ancestor::tr/td[contains(text () , '" + dataSet.get("priority") + "')]");
			if (isPriorityMPresent == false) {
				fc.utobj().verifyElementOnVisible_ByXpath(driver, "was not verify Priority");
			}

			fc.utobj().printTestStep("Search Action Item");
			fc.utobj().sendKeys(driver, pobj.auditAdminTopSearchString, subjectM);
			fc.utobj().clickElement(driver, pobj.searchActionItem);

			fc.utobj().printTestStep("Delete Action Item");
			fc.utobj().actionImgOption(driver, subjectM, "Delete");
			fc.utobj().acceptAlertBox(driver);

			fc.utobj().printTestStep("Search Action Item");
			fc.utobj().sendKeys(driver, pobj.auditAdminTopSearchString, subjectM);
			fc.utobj().clickElement(driver, pobj.searchActionItem);

			fc.utobj().printTestStep("Verify The Delete Action Item");
			boolean isTextPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//td[contains(text () , 'No records found.')]");

			if (isTextPresent == false) {
				fc.utobj().throwsException("Was not able to Delete a Action Item in Action Library");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}

	}

	public String addActionItem(WebDriver driver, Map<String, String> dataSet) throws Exception {

		String testCaseId = "TC_Add_Action_Item";
		String subject = null;

		if (fc.utobj().validate(testCaseId)) {
			try {
				AdminFieldOpsActionLibraryPage pobj = new AdminFieldOpsActionLibraryPage(driver);
				fc.fieldops().fieldops_common().adminFieldopsActionLibrary(driver);
				fc.utobj().clickElement(driver, pobj.addActionItem);
				fc.commonMethods().switch_cboxIframe_frameId(driver);
				subject = fc.utobj().generateTestData(dataSet.get("subject"));
				fc.utobj().sendKeys(driver, pobj.subject, subject);

				fc.utobj().selectDropDown(driver, pobj.selectPriority, dataSet.get("priority"));
				fc.utobj().sendKeys(driver, pobj.scheduleCompletionDays, dataSet.get("scheduleCompletionDays"));
				fc.utobj().sendKeys(driver, pobj.description, dataSet.get("description"));
				fc.utobj().clickElement(driver, pobj.addBtn);
				fc.utobj().switchFrameToDefault(driver);
				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException("was not able to To add Action Item :: Admin > Field Ops Acion Library");

		}
		return subject;
	}
}
