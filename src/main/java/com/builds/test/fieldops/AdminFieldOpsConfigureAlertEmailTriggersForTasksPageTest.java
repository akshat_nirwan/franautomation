package com.builds.test.fieldops;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Random;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import com.builds.test.admin.AdminUsersManageCorporateUsersAddCorporateUserPageTest;
import com.builds.test.common.CorporateUser;
import com.builds.uimaps.fieldops.AdminFieldOpsConfigureAlertEmailTriggersForTasksPage;
import com.builds.utilities.FranconnectUtil;
import com.builds.utilities.TestCase;

public class AdminFieldOpsConfigureAlertEmailTriggersForTasksPageTest {
	FranconnectUtil fc = new FranconnectUtil();

	@Test(groups = { "fieldops", "fieldopsTest" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Configure , Modify And Delete Alert Email Triggers for Tasks", testCaseId = "TC_45_Add_Alert")
	public void addAlert() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("fieldops", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			fc.utobj().printTestStep("Navigate To Admin > Users > Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");

			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUser = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			String emailId = "fieldopsautomation@staffex.com";
			CorporateUser corpUser = new CorporateUser();
			corpUser.setConsultant("Y");
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporateUser.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			AdminFieldOpsConfigureAlertEmailTriggersForTasksPage pobj = new AdminFieldOpsConfigureAlertEmailTriggersForTasksPage(
					driver);
			fc.utobj().printTestStep("Configure Alert Email Triggers for Tasks");

			String fromEmail = fc.utobj().generateRandomChar().concat("@gmail.com");
			addAlert(driver, config, dataSet, corpUser.getuserFullName(), fromEmail);
			fc.utobj().showAll(driver);

			fc.utobj().printTestStep("Verify The Configure Alert Email Triggers for Tasks");
			boolean isTextPresent = fc.utobj().assertPageSource(driver, fromEmail);
			if (isTextPresent == false) {
				fc.utobj().throwsException("Was Not able to Add Alert");
			}

			String value = randomValue();
			value = uniqueRandamValue(driver, value);

			fc.utobj().actionImgOption(driver, fromEmail, "Modify");

			fc.commonMethods().switch_cboxIframe_frameId(driver);

			fromEmail = fc.utobj().generateRandomChar().concat("@gmail.com");
			fc.utobj().sendKeys(driver, pobj.fromEmail, fromEmail);

			fc.utobj().sendKeys(driver, pobj.frequencyAlertDays, value);
			fc.utobj().selectDropDown(driver, pobj.criticalLevel, "Critical");
			fc.utobj().sendKeys(driver, pobj.alertEmailSubject, dataSet.get("alertEmailSubject"));
			fc.utobj().sendKeys(driver, pobj.emailContent, dataSet.get("emailContent"));
			fc.utobj().clickElement(driver, pobj.saveBtn);
			fc.utobj().clickElement(driver, pobj.closeBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().showAll(driver);

			fc.utobj().printTestStep("Verify The Configure Alert Email Triggers for Tasks");
			boolean isMFromEmailPresent = fc.utobj().assertPageSource(driver, fromEmail);
			if (isMFromEmailPresent == false) {
				fc.utobj().throwsException("Was Not able to add Alert");
			}

			fc.utobj().actionImgOption(driver, fromEmail, "Delete");
			fc.utobj().acceptAlertBox(driver);
			fc.utobj().showAll(driver);

			fc.utobj().printTestStep("Verify The Configure Alert Email Triggers for Tasks");
			boolean isTextDPresent = fc.utobj().assertPageSource(driver, fromEmail);
			if (isTextDPresent == true) {
				fc.utobj().throwsException("was not able to delete Alert");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
			;

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	public String addAlert(WebDriver driver, Map<String, String> config, Map<String, String> dataSet, String userName,
			String fromEmail) throws Exception {

		String testCaseId = "TC_Add_Alert_FieldOps";

		if (fc.utobj().validate(testCaseId)) {
			try {
				AdminFieldOpsConfigureAlertEmailTriggersForTasksPage pobj = new AdminFieldOpsConfigureAlertEmailTriggersForTasksPage(
						driver);
				fc.fieldops().fieldops_common().adminFieldOpsConfigureAlertEmailTriggersforTasks(driver);

				String value = randomValue();
				value = uniqueRandamValue(driver, value);

				fc.utobj().clickElement(driver, pobj.addAlert);
				fc.commonMethods().switch_cboxIframe_frameId(driver);
				fc.utobj().sendKeys(driver, pobj.fromEmail, fromEmail);

				fc.utobj().sendKeys(driver, pobj.frequencyAlertDays, value);
				fc.utobj().selectDropDown(driver, pobj.criticalLevel, "Critical");
				fc.utobj().sendKeys(driver, pobj.alertEmailSubject, dataSet.get("alertEmailSubject"));
				fc.utobj().sendKeys(driver, pobj.emailContent, dataSet.get("emailContent"));
				fc.utobj().selectValFromMultiSelect(driver, pobj.selectMultiBtn, userName);

				fc.utobj().clickElement(driver, pobj.saveBtn);
				fc.utobj().clickElement(driver, pobj.closeBtn);

				fc.utobj().switchFrameToDefault(driver);

				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException(
					"Was not able to Configure Alert Email Triggers For Task :: Admin > Filed Ops > Configuure Emai Triggers For Task");

		}

		return fromEmail;
	}

	public String randomValue() {
		Random random = new Random();
		int i = random.nextInt(99);
		String value = Integer.toString(i);
		return value;
	}

	public String uniqueRandamValue(WebDriver driver, String value) throws Exception {

		fc.utobj().showAll(driver);
		List<WebElement> elementOverDueDays = null;
		try {
			elementOverDueDays = driver.findElements(
					By.xpath(".//td[contains(text () , 'Overdue Day')]/ancestor::tr/following-sibling::tr/td[1]"));
			ArrayList<String> list = new ArrayList<String>();
			for (int i = 0; i < elementOverDueDays.size(); i++) {
				list.add(elementOverDueDays.get(i).getText().trim());
			}
			for (int j = 0; j < list.size(); j++) {

				if (value.equalsIgnoreCase(list.get(j))) {
					value = randomValue();
				} else {
				}
			}
		} catch (Exception e) {
		}
		return value;
	}
}
