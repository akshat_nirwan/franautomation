package com.builds.test.fieldops;

import java.util.Map;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Test;

import com.builds.uimaps.fieldops.AdminFieldOpsConfigureMediaLibraryPage;
import com.builds.utilities.FranconnectUtil;
import com.builds.utilities.TestCase;

public class AdminFieldOpsConfigureMediaLibraryPageTest {

	FranconnectUtil fc = new FranconnectUtil();

	@Test(groups = { "fieldops", "fieldopsTest" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify the Add , Modify And Delete Media Folder In Media Library", testCaseId = "TC_48_Add_Media_Folder")
	public void addMediaFolder() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("fieldops", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			AdminFieldOpsConfigureMediaLibraryPage pobj = new AdminFieldOpsConfigureMediaLibraryPage(driver);

			fc.utobj().printTestStep("Navigate To Admin > FieldOps > Configure Media Library Page");
			fc.utobj().printTestStep("Add Media Folder");

			String folderName = addMediaFolder(driver, dataSet);
			fc.utobj().showAll(driver);

			fc.utobj().printTestStep("Verify Add Media Folder");
			boolean isTextPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//td[contains(text () , '" + folderName + "')]");
			if (isTextPresent == false) {
				fc.utobj().throwsException("Was Not Able To add Media Folder");
			}

			boolean isDescrPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//td[contains(text () , '" + folderName
					+ "')]/ancestor::tr/td[contains(text () , '" + dataSet.get("folderSummary") + "')]");
			if (isDescrPresent == false) {
				fc.utobj().throwsException("Was Not Able To Verify Media Folder Description");
			}

			fc.utobj().printTestStep("Modify Media Folder");
			fc.utobj().actionImgOption(driver, folderName, "Modify");

			fc.commonMethods().switch_cboxIframe_frameId(driver);
			folderName = fc.utobj().generateTestData(dataSet.get("folderName"));
			fc.utobj().sendKeys(driver, pobj.folderName, folderName);
			fc.utobj().sendKeys(driver, pobj.folderSummary, dataSet.get("folderSummary"));
			fc.utobj().clickElement(driver, pobj.submitBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().showAll(driver);

			fc.utobj().printTestStep("Verify Modify Media Folder");
			boolean isFMPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//td[contains(text () , '" + folderName + "')]");
			if (isFMPresent == false) {
				fc.utobj().throwsException("Was Not Able To Modify Media Folder");
			}

			boolean isDescrMPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//td[contains(text () , '" + folderName
					+ "')]/ancestor::tr/td[contains(text () , '" + dataSet.get("folderSummary") + "')]");
			if (isDescrMPresent == false) {
				fc.utobj().throwsException("Was Not Able To Verify Media Folder Description");
			}

			fc.utobj().printTestStep("Delete Media Folder");
			fc.utobj().actionImgOption(driver, folderName, "Delete");
			fc.utobj().acceptAlertBox(driver);

			fc.utobj().showAll(driver);
			fc.utobj().printTestStep("Verify The Delete Media Folder");
			boolean isFDPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//td[contains(text () , '" + folderName + "')]");
			if (isFDPresent == true) {
				fc.utobj().throwsException("Was Not Able To add Media Folder");
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
			;
		}
	}

	public String addMediaFolder(WebDriver driver, Map<String, String> dataSet) throws Exception {

		String testCaseId = "TC_Add_Media_Folder";
		String folderName = null;

		if (fc.utobj().validate(testCaseId)) {
			try {
				AdminFieldOpsConfigureMediaLibraryPage pobj = new AdminFieldOpsConfigureMediaLibraryPage(driver);
				fc.fieldops().fieldops_common().adminFieldOpsConfigureMediaLibrary(driver);

				fc.utobj().clickElement(driver, pobj.manageMediaFolder);
				fc.utobj().clickElement(driver, pobj.addMediaFolder);
				fc.commonMethods().switch_cboxIframe_frameId(driver);
				folderName = fc.utobj().generateTestData(dataSet.get("folderName"));
				fc.utobj().sendKeys(driver, pobj.folderName, folderName);
				fc.utobj().sendKeys(driver, pobj.folderSummary, dataSet.get("folderSummary"));
				fc.utobj().clickElement(driver, pobj.submitBtn);
				fc.utobj().switchFrameToDefault(driver);

				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException("was no able to add Folder :: Admin > FieldOps > Configure Media Library");

		}
		return folderName;
	}

	@Test(groups = { "fieldops", "fieldops0808","fieldops08081"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify the Add Media In Folder In Media Library", testCaseId = "TC_51_Add_Media_In_Folder")
	public void addMediaInFolder() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("fieldops", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			AdminFieldOpsConfigureMediaLibraryPage pobj = new AdminFieldOpsConfigureMediaLibraryPage(driver);

			fc.utobj().printTestStep("Navigate To Admin > FieldOps > Configure Media Library Page");
			fc.utobj().printTestStep("Add Media Folder");
			String folderName = addMediaFolder(driver, dataSet);

			fc.utobj().showAll(driver);
			fc.utobj().printTestStep("Add Media In Folder");
			fc.utobj().actionImgOption(driver, folderName, "Add Media");

			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().selectDropDown(driver, pobj.selectFolder, folderName);
			String mediaTitle = fc.utobj().generateTestData(dataSet.get("mediaTitle"));
			fc.utobj().sendKeys(driver, pobj.mediaTitle, mediaTitle);
			fc.utobj().sendKeys(driver, pobj.mediaDescription, dataSet.get("mediaDescription"));
			String fileName = fc.utobj().getFilePathFromTestData(dataSet.get("uploadMediaName"));

			fc.utobj().sendKeys(driver, pobj.uploadMediaName, fileName);

			fc.utobj().clickElement(driver, pobj.addBtn);
			fc.utobj().clickElement(driver, pobj.closeBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().sendKeys(driver, pobj.searchByMediaName, mediaTitle);
			fc.utobj().clickElement(driver, pobj.searchBtn); 	 

			fc.utobj().printTestStep("Verify Media In Folder");
			boolean isTextPresent = fc.utobj().assertPageSource(driver, mediaTitle);
			if (isTextPresent == false) {
				fc.utobj().throwsException("was not able to add Media In Media Folder");
			}

			fc.utobj().printTestStep("Verify Media count in folder");
			fc.utobj().clickElement(driver, pobj.manageMediaFolder);
			fc.utobj().showAll(driver);

			boolean isCountTextPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () , '" + folderName + "')]/ancestor::tr/td/a[contains(text () , '1')]");
			if (isCountTextPresent == false) {
				fc.utobj().throwsException("was not able verify media file count");
			}

			fc.utobj().printTestStep("Verify Media File Count Redirection In Folder");
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//td[contains(text () , '" + folderName + "')]/ancestor::tr/td/a[contains(text () , '1')]"));
			boolean isRedirection = fc.utobj().assertPageSource(driver, mediaTitle);
			if (isRedirection == false) {
				fc.utobj().throwsException("was not able verify media file redirection");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}

	}

	@Test(groups = { "fieldops", "fieldops0808" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify the Add , Modify and Delete Media In Media Library ", testCaseId = "TC_52_Add_Media")
	public void addMedia() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("fieldops", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			AdminFieldOpsConfigureMediaLibraryPage pobj = new AdminFieldOpsConfigureMediaLibraryPage(driver);

			fc.utobj().printTestStep("Navigate To Admin > FieldOps > Configure Media Library Page");
			fc.utobj().printTestStep("Add Media Folder");

			String folderName = addMediaFolder(driver, dataSet);
			fc.utobj().clickElement(driver, pobj.backBtn);

			fc.utobj().printTestStep("Add Media");
			String mediaTitle = addMedia(driver, dataSet, folderName);
			fc.utobj().printTestStep("Verify Add Media");

			boolean isTextPresent = fc.utobj().assertPageSource(driver, mediaTitle);
			if (isTextPresent == false) {
				fc.utobj().throwsException("was not able to add Media In Media Folder");
			}

			fc.utobj().printTestStep("Modify Media");
			fc.utobj().clickElement(driver, pobj.modifyMedia);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().selectDropDown(driver, pobj.selectFolder, folderName);
			mediaTitle = fc.utobj().generateTestData(dataSet.get("mediaTitle"));
			fc.utobj().sendKeys(driver, pobj.mediaTitle, mediaTitle);
			fc.utobj().sendKeys(driver, pobj.mediaDescription, dataSet.get("mediaDescription"));
			String fileName = fc.utobj().getFilePathFromTestData(dataSet.get("uploadMediaName"));
			fc.utobj().sendKeys(driver, pobj.uploadMediaName, fileName);

			fc.utobj().clickElement(driver, pobj.addBtn);
			fc.utobj().clickElement(driver, pobj.closeBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Search Media By Folder Name");
			fc.utobj().selectDropDown(driver, pobj.searchMediaFolder, folderName);
			fc.utobj().clickElement(driver, pobj.searchBtn);

			fc.utobj().printTestStep("Verify The Modify Media");
			boolean isMMPresent = fc.utobj().assertPageSource(driver, mediaTitle);
			if (isMMPresent == false) {
				fc.utobj().throwsException("was not able to modify Media In Media Folder");
			}

			fc.utobj().printTestStep("Delete Media");

			fc.utobj().clickElement(driver, pobj.deleteMedia);
			fc.utobj().acceptAlertBox(driver);

			fc.utobj().printTestStep("Verify Delete Media");
			boolean isDMPresent = fc.utobj().assertPageSource(driver, mediaTitle);
			if (isDMPresent == true) {
				fc.utobj().throwsException("was not able to Delete Media In Media Folder");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	public String addMedia(WebDriver driver, Map<String, String> dataSet, String folderName) throws Exception {

		String testCaseId = "TC_Add_Media_FieldOps";
		String mediaTitle = null;

		if (fc.utobj().validate(testCaseId)) {
			try {
				AdminFieldOpsConfigureMediaLibraryPage pobj = new AdminFieldOpsConfigureMediaLibraryPage(driver);

				fc.utobj().clickElement(driver, pobj.addMedia);
				fc.commonMethods().switch_cboxIframe_frameId(driver);
				fc.utobj().selectDropDown(driver, pobj.selectFolder, folderName);
				mediaTitle = fc.utobj().generateTestData(dataSet.get("mediaTitle"));
				fc.utobj().sendKeys(driver, pobj.mediaTitle, mediaTitle);
				fc.utobj().sendKeys(driver, pobj.mediaDescription, dataSet.get("mediaDescription"));

				String fileName =fc.utobj().getFilePathFromTestData(dataSet.get("uploadMediaName"));
				fc.utobj().sendKeys(driver, pobj.uploadMediaName, fileName);
				fc.utobj().clickElement(driver, pobj.addBtn);
				fc.utobj().clickElement(driver, pobj.closeBtn);
				fc.utobj().switchFrameToDefault(driver);
				fc.utobj().sendKeys(driver, pobj.searchByMediaName, mediaTitle);
				fc.utobj().clickElement(driver, pobj.searchBtn);
				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException(
					"Was not able to add Media :: Admin > Filed Ops > Configure Media Library Page");

		}
		return mediaTitle;
	}
}
