package com.builds.test.fieldops;

import java.util.List;
import java.util.Map;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import com.builds.uimaps.fieldops.AdminFieldOpsConfigureReminderAlertsForTasksPage;
import com.builds.utilities.FranconnectUtil;
import com.builds.utilities.TestCase;

public class AdminFieldOpsConfigureReminderAlertsForTasksPageTest {
	FranconnectUtil fc = new FranconnectUtil();

	@Test(groups = { "fieldops", "fieldopsTest" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Configure Reminder Alerts for Tasks with popup option only", testCaseId = "TC_38_Popup_Alert_Type_Reminder")
	public void popupAlertTypeReminder() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("fieldops", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			AdminFieldOpsConfigureReminderAlertsForTasksPage pobj = new AdminFieldOpsConfigureReminderAlertsForTasksPage(
					driver);

			fc.utobj().printTestStep("Configure Reminder Alerts for Tasks with popup option only");
			fc.fieldops().fieldops_common().adminFieldOpsConfigureReminderAlertsforTasks(driver);

			List<WebElement> list = pobj.reminderType;
			if (fc.utobj().isSelected(driver,list.get(0))) {
				// do nothing
			} else {
				fc.utobj().clickElement(driver, list.get(0));
			}

			String alertDaysPrior = dataSet.get("alertDaysPrior");
			fc.utobj().sendKeys(driver, pobj.alertDaysPrior, alertDaysPrior);
			fc.utobj().clickElement(driver, pobj.submit);

			fc.utobj().printTestStep("Verify The Configure Reminder Alerts for Tasks with popup option only");

			String text = fc.utobj().getElement(driver, pobj.alertDaysPrior).getAttribute("value").trim();
			if (text.equalsIgnoreCase(alertDaysPrior)) {
				// do nothing
			} else {
				fc.utobj().throwsException("Was not able to configure Reminder Alerts for Tasks with popup option");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "fieldops", "fieldopsTest" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Configure Reminder Alerts for Tasks with Email option only", testCaseId = "TC_39_Email_Type_Reminder")
	public void emailTypeReminder() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("fieldops", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			AdminFieldOpsConfigureReminderAlertsForTasksPage pobj = new AdminFieldOpsConfigureReminderAlertsForTasksPage(
					driver);

			// Navigate
			fc.utobj().printTestStep("Configure Reminder Alerts for Tasks with popup option only");
			fc.fieldops().fieldops_common().adminFieldOpsConfigureReminderAlertsforTasks(driver);

			List<WebElement> list = pobj.reminderType;
			if (fc.utobj().isSelected(driver,list.get(1))) {
				// do nothing
			} else {
				fc.utobj().clickElement(driver, list.get(1));
			}

			List<WebElement> radio = pobj.SendAlertMailAssignor;
			if (fc.utobj().isSelected(driver,radio.get(0))) {
				// do nothing
			} else {
				fc.utobj().clickElement(driver, radio.get(0));
			}

			String alertDaysPrior = dataSet.get("alertDaysPrior");

			fc.utobj().sendKeys(driver, pobj.alertDaysPrior, alertDaysPrior);

			String frequencyAlertDays = dataSet.get("frequencyAlertDays");
			fc.utobj().sendKeys(driver, pobj.frequencyAlertDays, frequencyAlertDays);

			fc.utobj().sendKeys(driver, pobj.fromEmail, dataSet.get("fromEmail"));
			fc.utobj().sendKeys(driver, pobj.StartAlertEmailContent, dataSet.get("StartAlertEmailContent"));
			fc.utobj().sendKeys(driver, pobj.overdueEmailContent, dataSet.get("overdueEmailContent"));
			fc.utobj().sendKeys(driver, pobj.emailContentUnassigned, dataSet.get("emailContentUnassigned"));
			fc.utobj().sendKeys(driver, pobj.overdueEmailContentUnassigned,
					dataSet.get("overdueEmailContentUnassigned"));

			fc.utobj().clickElement(driver, pobj.submit);

			fc.utobj().printTestStep("Verify The Configure Reminder Alerts for Tasks with popup option only");
			String text = pobj.fromEmail.getAttribute("value").trim();
			System.out.println(text);
			if (text.equalsIgnoreCase(dataSet.get("fromEmail"))) {
				// do nothing
			} else {
				fc.utobj().throwsException("Was not able to Configure Reminder Alerts for Tasks with Email option ");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "fieldops", "fieldopsTest" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Configure Reminder Alerts for Tasks Both (Pop-up Alert & Email)", testCaseId = "TC_40_Popup_Alert_Email_Type_Reminder")
	public void popupAlertAndEmailTypeReminder() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("fieldops", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			AdminFieldOpsConfigureReminderAlertsForTasksPage pobj = new AdminFieldOpsConfigureReminderAlertsForTasksPage(
					driver);

			// Navigate
			fc.utobj().printTestStep("Configure Reminder Alerts for Tasks Both (Pop-up Alert & Email)");
			fc.fieldops().fieldops_common().adminFieldOpsConfigureReminderAlertsforTasks(driver);

			List<WebElement> list = pobj.reminderType;
			if (fc.utobj().isSelected(driver,list.get(2))) {
				// do nothing
			} else {
				fc.utobj().clickElement(driver, list.get(2));
			}

			List<WebElement> radio = pobj.SendAlertMailAssignor;
			if (fc.utobj().isSelected(driver,radio.get(0))) {
				// do nothing
			} else {
				fc.utobj().clickElement(driver, radio.get(0));
			}

			String alertDaysPrior = dataSet.get("alertDaysPrior");

			fc.utobj().sendKeys(driver, pobj.alertDaysPrior, alertDaysPrior);

			String frequencyAlertDays = dataSet.get("frequencyAlertDays");
			fc.utobj().sendKeys(driver, pobj.frequencyAlertDays, frequencyAlertDays);

			fc.utobj().sendKeys(driver, pobj.fromEmail, dataSet.get("fromEmail"));
			fc.utobj().sendKeys(driver, pobj.StartAlertEmailContent, dataSet.get("StartAlertEmailContent"));
			fc.utobj().sendKeys(driver, pobj.overdueEmailContent, dataSet.get("overdueEmailContent"));
			fc.utobj().sendKeys(driver, pobj.emailContentUnassigned, dataSet.get("emailContentUnassigned"));
			fc.utobj().sendKeys(driver, pobj.overdueEmailContentUnassigned,
					dataSet.get("overdueEmailContentUnassigned"));

			fc.utobj().clickElement(driver, pobj.submit);

			fc.utobj().printTestStep("Verify The Configure Reminder Alerts for Tasks Both (Pop-up Alert & Email)");
			String text = pobj.fromEmail.getAttribute("value").trim();
			System.out.println(text);
			if (text.equalsIgnoreCase(dataSet.get("fromEmail"))) {
				// do nothing
			} else {
				fc.utobj().throwsException("Was not able to Configure Reminder Alerts for Tasks with Email option ");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

}
