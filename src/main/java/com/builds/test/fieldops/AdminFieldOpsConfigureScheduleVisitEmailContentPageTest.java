package com.builds.test.fieldops;

import java.util.List;
import java.util.Map;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.builds.uimaps.fieldops.AdminFieldOpsConfigureScheduleVisitEmailContentPage;
import com.builds.utilities.FranconnectUtil;

public class AdminFieldOpsConfigureScheduleVisitEmailContentPageTest {
	FranconnectUtil fc = new FranconnectUtil();

	public void scheduleVisitEmailContentYes(WebDriver driver, String subject, Map<String, String> config)
			throws Exception {

		String testCaseId = "TC_Schedule_Visit_Email_Content_Yes";

		if (fc.utobj().validate(testCaseId)) {
			try {
				AdminFieldOpsConfigureScheduleVisitEmailContentPage pobj = new AdminFieldOpsConfigureScheduleVisitEmailContentPage(
						driver);
				fc.fieldops().fieldops_common().adminFieldOpsConfigureScheduleVisitEmailContent(driver);

				List<WebElement> list = pobj.showPopupEachVisit;
				if (fc.utobj().isSelected(driver,list.get(0))) {
					// do nothing
				} else {
					fc.utobj().clickElement(driver, list.get(0));
				}
				fc.utobj().sendKeys(driver, pobj.subject, subject);

				if (fc.utobj().isSelected(driver,pobj.attachVisitForm1)) {
					// do nothing
				} else {
					fc.utobj().clickElement(driver, pobj.attachVisitForm1);
				}

				fc.utobj().clickElement(driver, pobj.configureBtn);

				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException(
					"was not able scedule Visit Email Contetent Yes :: Sdmin > Field Ops > Schedule Visit > Email Content");

		}
	}
}
