package com.builds.test.fieldops;

import java.util.Map;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import com.builds.uimaps.fieldops.AdminFieldOpsConfigureScorePage;
import com.builds.utilities.FranconnectUtil;
import com.builds.utilities.TestCase;

public class AdminFieldOpsConfigureScorePageTest {

	FranconnectUtil fc = new FranconnectUtil();

	@Test(groups = { "fieldops", "fieldopsTest" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Configure Score with Grading Option Yes and Label Option Yes", testCaseId = "TC_41_Configure_Score_01")
	public void configureScoreGradingYesLabelYes() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("fieldops", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			AdminFieldOpsConfigureScorePage pobj = new AdminFieldOpsConfigureScorePage(driver);
			boolean status = false;

			fc.utobj().printTestStep("Configure Score with Grading Option Yes and Label Option Yes");
			fc.fieldops().fieldops_common().adminFieldOpsConfigureScore(driver);
			try {
				pobj.grade3.isEnabled();
				status = true;
			} catch (Exception e) {

			}
			if (status == false) {
				fc.utobj().clickElement(driver, pobj.addNewImage);
				status = true;
			}

			fc.utobj().printTestStep("Select Grade Options Yes");
			if (!fc.utobj().isSelected(driver, pobj.gradeOptions)) {
				fc.utobj().clickElement(driver, pobj.gradeOptions);
			}

			if (status == true) {
				fc.utobj().sendKeys(driver, pobj.highScore1, dataSet.get("highScore1"));
				fc.utobj().sendKeys(driver, pobj.highScore2, dataSet.get("highScore2"));

				fc.utobj().sendKeys(driver, pobj.grade1, dataSet.get("grade1"));
				fc.utobj().sendKeys(driver, pobj.grade2, dataSet.get("grade2"));
				fc.utobj().sendKeys(driver, pobj.grade3, dataSet.get("grade3"));

				fc.utobj().printTestStep("Select Label Option Yes");
				if (!fc.utobj().isSelected(driver, pobj.labelOptionsYes)) {
					fc.utobj().clickElement(driver, pobj.labelOptionsYes);
				}

				fc.utobj().sendKeys(driver, pobj.label1, dataSet.get("label1"));
				fc.utobj().sendKeys(driver, pobj.label2, dataSet.get("label2"));
				fc.utobj().sendKeys(driver, pobj.label3, dataSet.get("label3"));

				WebElement element = fc.utobj().getElementByXpath(driver,
						".//input[@name='label1']/../following-sibling::td//span[@title='Click To Open Color Picker']");
				fc.utobj().clickElement(driver, element);
				fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
						".//*[@id='siteMainBody']/div[1]/table/tbody/tr[2]/td[4]/div/span[3]"));
				fc.utobj().clickElement(driver, pobj.okBtn);

				element = fc.utobj().getElementByXpath(driver,
						".//input[@name='label2']/../following-sibling::td//span[@title='Click To Open Color Picker']");
				fc.utobj().clickElement(driver, element);
				fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
						".//*[@id='siteMainBody']/div[2]/table/tbody/tr[2]/td[4]/div/span[10]"));
				fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
						".//*[@id='siteMainBody']/div[2]/table/tbody/tr[2]/td[4]/input[1]"));

				element = fc.utobj().getElementByXpath(driver,
						".//input[@name='label3']/../following-sibling::td//span[@title='Click To Open Color Picker']");
				fc.utobj().clickElement(driver, element);
				fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
						".//*[@id='siteMainBody']/div[3]/table/tbody/tr[2]/td[4]/div/span[21]"));
				fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
						".//*[@id='siteMainBody']/div[3]/table/tbody/tr[2]/td[4]/input[1]"));

				fc.utobj().clickElement(driver, pobj.saveBtn);

				fc.utobj().printTestStep("Verify The Configure Score with Grading Option Yes and Label Option Yes");
				String text = pobj.highScore2.getAttribute("value").trim();
				if (!text.equalsIgnoreCase(dataSet.get("highScore2").concat(".00"))) {
					fc.utobj().throwsException("Was not able to configure Score highscore");
				}

				text = pobj.grade3.getAttribute("value").trim();
				if (!text.equalsIgnoreCase(dataSet.get("grade3"))) {
					fc.utobj().throwsException("Was not able to configure Score with grade");
				}

				text = pobj.label3.getAttribute("value").trim();
				if (!text.equalsIgnoreCase(dataSet.get("label3"))) {
					fc.utobj().throwsException("Was not able to configure Score with label");
				}
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "fieldops", "fieldopsTest" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Configure Score with Grading Option Yes and Label Option No", testCaseId = "TC_42_Configure_Score_02")
	public void configureScoreGradingYesLabelNo() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("fieldops", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {

			driver = fc.loginpage().login(driver);
			AdminFieldOpsConfigureScorePage pobj = new AdminFieldOpsConfigureScorePage(driver);
			boolean status = false;

			fc.utobj().printTestStep("Configure Score with Grading Option Yes and Label Option No");

			fc.fieldops().fieldops_common().adminFieldOpsConfigureScore(driver);

			try {
				pobj.grade3.isEnabled();
				status = true;
			} catch (Exception e) {

			}
			if (status == false) {
				fc.utobj().clickElement(driver, pobj.addNewImage);
				status = true;
			}

			fc.utobj().printTestStep("Select Grade Options Yes");
			if (!fc.utobj().isSelected(driver, pobj.gradeOptions)) {
				fc.utobj().clickElement(driver, pobj.gradeOptions);
			}

			if (status == true) {
				fc.utobj().sendKeys(driver, pobj.highScore1, dataSet.get("highScore1"));
				fc.utobj().sendKeys(driver, pobj.highScore2, dataSet.get("highScore2"));

				fc.utobj().sendKeys(driver, pobj.grade1, dataSet.get("grade1"));
				fc.utobj().sendKeys(driver, pobj.grade2, dataSet.get("grade2"));
				fc.utobj().sendKeys(driver, pobj.grade3, dataSet.get("grade3"));

				fc.utobj().printTestStep("Select Label Option No");
				fc.utobj().clickElement(driver, pobj.labelOptionsNo);

				WebElement element = fc.utobj().getElementByXpath(driver,
						".//input[@name='label1']/../following-sibling::td//span[@title='Click To Open Color Picker']");
				fc.utobj().clickElement(driver, element);
				fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
						".//*[@id='siteMainBody']/div[1]/table/tbody/tr[2]/td[4]/div/span[3]"));
				fc.utobj().clickElement(driver, pobj.okBtn);

				element = fc.utobj().getElementByXpath(driver,
						".//input[@name='label2']/../following-sibling::td//span[@title='Click To Open Color Picker']");
				fc.utobj().clickElement(driver, element);
				fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
						".//*[@id='siteMainBody']/div[2]/table/tbody/tr[2]/td[4]/div/span[10]"));
				fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
						".//*[@id='siteMainBody']/div[2]/table/tbody/tr[2]/td[4]/input[1]"));

				element = fc.utobj().getElementByXpath(driver,
						".//input[@name='label3']/../following-sibling::td//span[@title='Click To Open Color Picker']");
				fc.utobj().clickElement(driver, element);
				fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
						".//*[@id='siteMainBody']/div[3]/table/tbody/tr[2]/td[4]/div/span[21]"));
				fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
						".//*[@id='siteMainBody']/div[3]/table/tbody/tr[2]/td[4]/input[1]"));

				fc.utobj().clickElement(driver, pobj.saveBtn);

				fc.utobj().printTestStep("Verify The Configure Score with Grading Option Yes and Label Option No");
				String text = pobj.highScore1.getAttribute("value").trim();
				if (!text.equalsIgnoreCase(dataSet.get("highScore1").concat(".00"))) {
					fc.utobj().throwsException("Was not able to configure Score");
				}
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "fieldops", "fieldopsTest" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Configure Score with Percentage Option Yes and Label Option Yes", testCaseId = "TC_43_Configure_Score_03")
	public void configureScoreGradingNoLabelYes() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("fieldops", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {

			driver = fc.loginpage().login(driver);
			AdminFieldOpsConfigureScorePage pobj = new AdminFieldOpsConfigureScorePage(driver);
			boolean status = false;

			fc.utobj().printTestStep("Configure Score with Grading Option NO and Label Option Yes");
			fc.fieldops().fieldops_common().adminFieldOpsConfigureScore(driver);

			try {
				pobj.grade3.isEnabled();
				status = true;
			} catch (Exception e) {

			}
			if (status == false) {
				fc.utobj().clickElement(driver, pobj.addNewImage);
				status = true;
			}

			if (status == true) {
				fc.utobj().sendKeys(driver, pobj.highScore1, dataSet.get("highScore1"));
				fc.utobj().sendKeys(driver, pobj.highScore2, dataSet.get("highScore2"));

				fc.utobj().printTestStep("Select Percentage Option");
				if (!fc.utobj().isSelected(driver, pobj.percenrageOptions)) {
					fc.utobj().clickElement(driver, pobj.percenrageOptions);
				}

				fc.utobj().printTestStep("Select Label Option Yes");

				if (!fc.utobj().isSelected(driver, pobj.labelOptionsYes)) {
					fc.utobj().clickElement(driver, pobj.labelOptionsYes);
				}

				fc.utobj().sendKeys(driver, pobj.label1, dataSet.get("label1"));
				fc.utobj().sendKeys(driver, pobj.label2, dataSet.get("label2"));
				fc.utobj().sendKeys(driver, pobj.label3, dataSet.get("label3"));

				WebElement element = fc.utobj().getElementByXpath(driver,
						".//input[@name='label1']/../following-sibling::td//span[@title='Click To Open Color Picker']");
				fc.utobj().clickElement(driver, element);
				fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
						".//*[@id='siteMainBody']/div[1]/table/tbody/tr[2]/td[4]/div/span[3]"));
				fc.utobj().clickElement(driver, pobj.okBtn);

				element = fc.utobj().getElementByXpath(driver,
						".//input[@name='label2']/../following-sibling::td//span[@title='Click To Open Color Picker']");
				fc.utobj().clickElement(driver, element);
				fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
						".//*[@id='siteMainBody']/div[2]/table/tbody/tr[2]/td[4]/div/span[10]"));
				fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
						".//*[@id='siteMainBody']/div[2]/table/tbody/tr[2]/td[4]/input[1]"));

				element = fc.utobj().getElementByXpath(driver,
						".//input[@name='label3']/../following-sibling::td//span[@title='Click To Open Color Picker']");
				fc.utobj().clickElement(driver, element);
				fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
						".//*[@id='siteMainBody']/div[3]/table/tbody/tr[2]/td[4]/div/span[21]"));
				fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
						".//*[@id='siteMainBody']/div[3]/table/tbody/tr[2]/td[4]/input[1]"));

				fc.utobj().clickElement(driver, pobj.saveBtn);

				fc.utobj().printTestStep("Verify The Configure Score with Grading Option NO and Label Option Yes");
				String text = pobj.highScore2.getAttribute("value").trim();
				if (!text.equalsIgnoreCase(dataSet.get("highScore2").concat(".00"))) {
					fc.utobj().throwsException("Was not able to configure Score");
				}
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	public void configureScoreGradingNoLabelYes(WebDriver driver, Map<String, String> config,
			Map<String, String> dataSet) throws Exception {

		String testCaseId = "TC_Configure_Percentage_Option_Label_Yes";

		if (fc.utobj().validate(testCaseId)) {
			try {
				fc.fieldops().fieldops_common().adminFieldOpsConfigureScore(driver);
				AdminFieldOpsConfigureScorePage pobj = new AdminFieldOpsConfigureScorePage(driver);
				boolean status = false;

				try {
					pobj.grade3.isEnabled();
					status = true;
				} catch (Exception e) {

				}
				if (status == false) {
					fc.utobj().clickElement(driver, pobj.addNewImage);
					status = true;
				}

				if (status == true) {
					fc.utobj().sendKeys(driver, pobj.highScore1, dataSet.get("highScore1"));
					fc.utobj().sendKeys(driver, pobj.highScore2, dataSet.get("highScore2"));

					if (!fc.utobj().isSelected(driver, pobj.percenrageOptions)) {
						fc.utobj().clickElement(driver, pobj.percenrageOptions);
					}

					if (!fc.utobj().isSelected(driver, pobj.labelOptionsYes)) {
						fc.utobj().clickElement(driver, pobj.labelOptionsYes);
					}

					fc.utobj().sendKeys(driver, pobj.label1, dataSet.get("label1"));
					fc.utobj().sendKeys(driver, pobj.label2, dataSet.get("label2"));
					fc.utobj().sendKeys(driver, pobj.label3, dataSet.get("label3"));

					WebElement element = fc.utobj().getElementByXpath(driver,
							".//input[@name='label1']/../following-sibling::td//span[@title='Click To Open Color Picker']");
					fc.utobj().clickElement(driver, element);
					fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
							".//*[@id='siteMainBody']/div[1]/table/tbody/tr[2]/td[4]/div/span[3]"));
					fc.utobj().clickElement(driver, pobj.okBtn);

					element = fc.utobj().getElementByXpath(driver,
							".//input[@name='label2']/../following-sibling::td//span[@title='Click To Open Color Picker']");
					fc.utobj().clickElement(driver, element);
					fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
							".//*[@id='siteMainBody']/div[2]/table/tbody/tr[2]/td[4]/div/span[10]"));
					fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
							".//*[@id='siteMainBody']/div[2]/table/tbody/tr[2]/td[4]/input[1]"));

					element = fc.utobj().getElementByXpath(driver,
							".//input[@name='label3']/../following-sibling::td//span[@title='Click To Open Color Picker']");
					fc.utobj().clickElement(driver, element);
					fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
							".//*[@id='siteMainBody']/div[3]/table/tbody/tr[2]/td[4]/div/span[21]"));
					fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
							".//*[@id='siteMainBody']/div[3]/table/tbody/tr[2]/td[4]/input[1]"));

					fc.utobj().clickElement(driver, pobj.saveBtn);
				}

				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException(
					"Was not able Configure Grading No Label Yes :: Admin > FieldOps > Configure Socre Page");
		}
	}

	@Test(groups = { "fieldops", "fieldopsTest" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Configure Score with Percentage Option Yes and Label Option No", testCaseId = "TC_44_Configure_Score_04")
	public void configureScoreGradingNoLabelNo() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("fieldops", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {

			driver = fc.loginpage().login(driver);
			AdminFieldOpsConfigureScorePage pobj = new AdminFieldOpsConfigureScorePage(driver);
			boolean status = false;

			fc.utobj().printTestStep("Configure Score with Grading Option NO and Label Option No");
			fc.fieldops().fieldops_common().adminFieldOpsConfigureScore(driver);

			try {
				pobj.grade3.isEnabled();
				status = true;
			} catch (Exception e) {

			}
			if (status == false) {
				fc.utobj().clickElement(driver, pobj.addNewImage);
				status = true;
			}

			if (status == true) {
				fc.utobj().sendKeys(driver, pobj.highScore1, dataSet.get("highScore1"));
				fc.utobj().sendKeys(driver, pobj.highScore2, dataSet.get("highScore2"));

				fc.utobj().printTestStep("Select Percentage Option");
				if (!fc.utobj().isSelected(driver, pobj.percenrageOptions)) {
					fc.utobj().clickElement(driver, pobj.percenrageOptions);
				}

				fc.utobj().printTestStep("Select Label Option Yes");

				if (!fc.utobj().isSelected(driver, pobj.labelOptionsNo)) {
					fc.utobj().clickElement(driver, pobj.labelOptionsNo);
				}

				WebElement element = fc.utobj().getElementByXpath(driver,
						".//input[@name='label1']/../following-sibling::td//span[@title='Click To Open Color Picker']");
				fc.utobj().clickElement(driver, element);
				fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
						".//*[@id='siteMainBody']/div[1]/table/tbody/tr[2]/td[4]/div/span[3]"));
				fc.utobj().clickElement(driver, pobj.okBtn);

				element = fc.utobj().getElementByXpath(driver,
						".//input[@name='label2']/../following-sibling::td//span[@title='Click To Open Color Picker']");
				fc.utobj().clickElement(driver, element);
				fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
						".//*[@id='siteMainBody']/div[2]/table/tbody/tr[2]/td[4]/div/span[10]"));
				fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
						".//*[@id='siteMainBody']/div[2]/table/tbody/tr[2]/td[4]/input[1]"));

				element = fc.utobj().getElementByXpath(driver,
						".//input[@name='label3']/../following-sibling::td//span[@title='Click To Open Color Picker']");
				fc.utobj().clickElement(driver, element);
				fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
						".//*[@id='siteMainBody']/div[3]/table/tbody/tr[2]/td[4]/div/span[21]"));
				fc.utobj().getElementByXpath(driver,
						".//*[@id='siteMainBody']/div[3]/table/tbody/tr[2]/td[4]/input[1]");

				fc.utobj().clickElement(driver, pobj.saveBtn);

				fc.utobj().printTestStep("Verify The Configure Score with Grading Option NO and Label Option No");
				String text = pobj.highScore2.getAttribute("value").trim();
				if (!text.equalsIgnoreCase(dataSet.get("highScore2").concat(".00"))) {
					fc.utobj().throwsException("Was not able to configure Score");
				}
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "fieldops", "fieldopsTest" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Configure Score with Score Yes Option Yes and Label Option Yes", testCaseId = "TC_Configure_Score_Label_Yes")
	public void configureScoreScoreYesLabelYes() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("fieldops", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {

			driver = fc.loginpage().login(driver);
			AdminFieldOpsConfigureScorePage pobj = new AdminFieldOpsConfigureScorePage(driver);
			boolean status = false;

			fc.utobj().printTestStep("Configure Score with Grading Option NO and Label Option Yes");
			fc.fieldops().fieldops_common().adminFieldOpsConfigureScore(driver);

			try {
				pobj.grade3.isEnabled();
				status = true;
			} catch (Exception e) {

			}
			if (status == false) {
				fc.utobj().clickElement(driver, pobj.addNewImage);
				status = true;
			}

			if (status == true) {
				fc.utobj().sendKeys(driver, pobj.highScore1, dataSet.get("highScore1"));
				fc.utobj().sendKeys(driver, pobj.highScore2, dataSet.get("highScore2"));

				fc.utobj().printTestStep("Select Score Option");
				if (!fc.utobj().isSelected(driver, pobj.scoreOptions)) {
					fc.utobj().clickElement(driver, pobj.scoreOptions);
				}

				fc.utobj().printTestStep("Select Label Option Yes");

				if (!fc.utobj().isSelected(driver, pobj.labelOptionsYes)) {
					fc.utobj().clickElement(driver, pobj.labelOptionsYes);
				}

				fc.utobj().sendKeys(driver, pobj.label1, dataSet.get("label1"));
				fc.utobj().sendKeys(driver, pobj.label2, dataSet.get("label2"));
				fc.utobj().sendKeys(driver, pobj.label3, dataSet.get("label3"));

				WebElement element = fc.utobj().getElementByXpath(driver,
						".//input[@name='label1']/../following-sibling::td//span[@title='Click To Open Color Picker']");
				fc.utobj().clickElement(driver, element);
				fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
						".//*[@id='siteMainBody']/div[1]/table/tbody/tr[2]/td[4]/div/span[3]"));
				fc.utobj().clickElement(driver, pobj.okBtn);

				element = fc.utobj().getElementByXpath(driver,
						".//input[@name='label2']/../following-sibling::td//span[@title='Click To Open Color Picker']");
				fc.utobj().clickElement(driver, element);
				fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
						".//*[@id='siteMainBody']/div[2]/table/tbody/tr[2]/td[4]/div/span[10]"));
				fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
						".//*[@id='siteMainBody']/div[2]/table/tbody/tr[2]/td[4]/input[1]"));

				element = fc.utobj().getElementByXpath(driver,
						".//input[@name='label3']/../following-sibling::td//span[@title='Click To Open Color Picker']");
				fc.utobj().clickElement(driver, element);
				fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
						".//*[@id='siteMainBody']/div[3]/table/tbody/tr[2]/td[4]/div/span[21]"));
				fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
						".//*[@id='siteMainBody']/div[3]/table/tbody/tr[2]/td[4]/input[1]"));

				fc.utobj().clickElement(driver, pobj.saveBtn);

				fc.utobj().printTestStep("Verify The Configure Score with Grading Option NO and Label Option Yes");
				String text = pobj.highScore2.getAttribute("value").trim();
				if (!text.equalsIgnoreCase(dataSet.get("highScore2").concat(".00"))) {
					fc.utobj().throwsException("Was not able to configure Score");
				}
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "fieldops", "fieldopsTest" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Configure Score with Score Option Yes and Label Option No", testCaseId = "TC_Configure_Score_Label_No")
	public void configureScoreScoreYesLabelNo() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("fieldops", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {

			driver = fc.loginpage().login(driver);
			AdminFieldOpsConfigureScorePage pobj = new AdminFieldOpsConfigureScorePage(driver);
			boolean status = false;

			fc.utobj().printTestStep("Configure Score with Grading Option NO and Label Option No");
			fc.fieldops().fieldops_common().adminFieldOpsConfigureScore(driver);

			try {
				pobj.grade3.isEnabled();
				status = true;
			} catch (Exception e) {

			}
			if (status == false) {
				fc.utobj().clickElement(driver, pobj.addNewImage);
				status = true;
			}

			if (status == true) {
				fc.utobj().sendKeys(driver, pobj.highScore1, dataSet.get("highScore1"));
				fc.utobj().sendKeys(driver, pobj.highScore2, dataSet.get("highScore2"));

				fc.utobj().printTestStep("Select Score Option");
				if (!fc.utobj().isSelected(driver, pobj.scoreOptions)) {
					fc.utobj().clickElement(driver, pobj.scoreOptions);
				}

				fc.utobj().printTestStep("Select Label Option Yes");

				if (!fc.utobj().isSelected(driver, pobj.labelOptionsNo)) {
					fc.utobj().clickElement(driver, pobj.labelOptionsNo);
				}

				WebElement element = fc.utobj().getElementByXpath(driver,
						".//input[@name='label1']/../following-sibling::td//span[@title='Click To Open Color Picker']");
				fc.utobj().clickElement(driver, element);
				fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
						".//*[@id='siteMainBody']/div[1]/table/tbody/tr[2]/td[4]/div/span[3]"));
				fc.utobj().clickElement(driver, pobj.okBtn);

				element = fc.utobj().getElementByXpath(driver,
						".//input[@name='label2']/../following-sibling::td//span[@title='Click To Open Color Picker']");
				fc.utobj().clickElement(driver, element);
				fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
						".//*[@id='siteMainBody']/div[2]/table/tbody/tr[2]/td[4]/div/span[10]"));
				fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
						".//*[@id='siteMainBody']/div[2]/table/tbody/tr[2]/td[4]/input[1]"));

				element = fc.utobj().getElementByXpath(driver,
						".//input[@name='label3']/../following-sibling::td//span[@title='Click To Open Color Picker']");
				fc.utobj().clickElement(driver, element);
				fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
						".//*[@id='siteMainBody']/div[3]/table/tbody/tr[2]/td[4]/div/span[21]"));
				fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
						".//*[@id='siteMainBody']/div[3]/table/tbody/tr[2]/td[4]/input[1]"));

				fc.utobj().clickElement(driver, pobj.saveBtn);

				fc.utobj().printTestStep("Verify The Configure Score with Grading Option NO and Label Option No");
				String text = pobj.highScore2.getAttribute("value").trim();
				if (!text.equalsIgnoreCase(dataSet.get("highScore2").concat(".00"))) {
					fc.utobj().throwsException("Was not able to configure Score");
				}
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

}
