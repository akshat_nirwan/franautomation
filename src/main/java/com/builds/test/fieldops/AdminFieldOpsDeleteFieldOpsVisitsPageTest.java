package com.builds.test.fieldops;

import java.util.Map;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.Test;

import com.builds.test.admin.AdminFranchiseLocationAddFranchiseLocationPageTest;
import com.builds.test.admin.AdminUsersManageCorporateUsersAddCorporateUserPageTest;
import com.builds.test.common.CorporateUser;
import com.builds.uimaps.fieldops.AdminFieldOpsDeleteFieldOpsVisitsPage;
import com.builds.utilities.FranconnectUtil;
import com.builds.utilities.TestCase;

public class AdminFieldOpsDeleteFieldOpsVisitsPageTest {
	FranconnectUtil fc = new FranconnectUtil();

	@Test(groups = { "fieldops", "foFailedTC"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify Deletion of a FieldOps Visit From Admin", testCaseId = "TC_58_Delete_Field_Ops_Visit")
	public void deleteFieldOpsVisits() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("fieldops", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			AdminFieldOpsDeleteFieldOpsVisitsPage pobj = new AdminFieldOpsDeleteFieldOpsVisitsPage(driver);

			fc.utobj().printTestStep("Navigate To Admin > Fieldops > Manage Visit Form");
			fc.utobj().printTestStep("Add Private Visit Form");

			AdminFieldOpsManageVisitFormPageTest visitFormPage = new AdminFieldOpsManageVisitFormPageTest();
			String visitFormName = visitFormPage.addVisitFormPrivate(driver, dataSet);

			fc.utobj().printTestStep("Add Question In Visit Form");
			fc.utobj().clickElement(driver, pobj.addQuestion);
			AdminFieldOpsManageVisitFormCustomizeVisitFormPageTest customizeVisit = new AdminFieldOpsManageVisitFormCustomizeVisitFormPageTest();
			customizeVisit.addQuestion(driver, dataSet);
			fc.utobj().clickElement(driver, pobj.finishBtn);

			fc.utobj().printTestStep("Navigate To Admin > Franchise Location > Add Franchise Location");
			fc.utobj().printTestStep("Add Franchise Location");

			String regionName = fc.utobj().generateTestData("Test");
			String storeType = fc.utobj().generateTestData("Test");
			String franchiseId = fc.utobj().generateTestData("Test");
			AdminFranchiseLocationAddFranchiseLocationPageTest franchise = new AdminFranchiseLocationAddFranchiseLocationPageTest();
			franchiseId = franchise.addFranchiseLocation_All(driver, franchiseId, regionName, storeType);

			fc.utobj().printTestStep("Navigate To Admin > Users > Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");

			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUser = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			String emailId = "fieldopsautomation@staffex.com";
			CorporateUser corpUser = new CorporateUser();
			corpUser.setConsultant("Y");
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporateUser.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			fc.utobj().printTestStep("Navigate To FieldOps > Home Page");
			fc.utobj().printTestStep("Create Visit");
			FieldOpsHomePageTest createVisitPage = new FieldOpsHomePageTest();
			createVisitPage.createVisitAtHomePageForDelete(driver, config, dataSet, visitFormName, franchiseId,	corpUser.getuserFullName());

			fc.utobj().printTestStep("Navigate To Admin > FieldOps > Delete Field Ops Visits Page");
			fc.fieldops().fieldops_common().adminFieldOpsDeleteFieldOpsVisits(driver);

			//fc.utobj().printTestStep("Search Visit By Filter");
			//fc.utobj().sendKeys(driver, pobj.visitNumber, "123");
			//pobj.visitNumber.clear();
			//fc.utobj().setToDefault(driver, pobj.visitFormSelectBtn);
			fc.utobj().setToDefault(driver, pobj.franchiseIdSelect);
			//fc.utobj().setToDefault(driver, pobj.consultantSelectBtn);
			//fc.utobj().setToDefault(driver, pobj.statusSelectBtn);
			/*
			if(pobj.divisionSelectDropDown.isDisplayed()) 
			{
			fc.utobj().setToDefault(driver, pobj.divisionSelectDropDown);
			}
			*/
			fc.utobj().clickElement(driver, pobj.saveViewBtn);

		//	fc.utobj().selectValFromMultiSelect(driver, pobj.franchiseIdSelect,	fc.utobj().getElement(driver, pobj.franchiseIdSelect).findElement(By.xpath(".//input[@id='selectAll']")), franchiseId);
			fc.utobj().selectValFromMultiSelect(driver, pobj.franchiseIdSelect, franchiseId);
			fc.utobj().clickElement(driver, pobj.viewBtn);

			fc.utobj().printTestStep("Delete Visit Form");
			fc.utobj().clickElement(driver, pobj.allSelect);
			fc.utobj().clickElement(driver, pobj.deleteBtn);
			fc.utobj().acceptAlertBox(driver);

			//fc.utobj().selectValFromMultiSelect(driver, pobj.franchiseIdSelect,	fc.utobj().getElement(driver, pobj.franchiseIdSelect).findElement(By.xpath(".//input[@id='selectAll']")), franchiseId);
			
			fc.utobj().selectValFromMultiSelect(driver, pobj.franchiseIdSelect, franchiseId);
			fc.utobj().clickElement(driver, pobj.viewBtn);
			
			fc.utobj().printTestStep("Verify Deleted Visit");
			
			boolean isTextPresent = fc.utobj().assertPageSource(driver, visitFormName);
			
			if (isTextPresent) {
				
				fc.utobj().throwsException("Was not able to delete a FieldOps Visit");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

}
