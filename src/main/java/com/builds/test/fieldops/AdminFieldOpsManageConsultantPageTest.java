package com.builds.test.fieldops;

import java.util.Map;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import com.builds.test.admin.AdminUsersManageCorporateUsersAddCorporateUserPageTest;
import com.builds.test.common.CorporateUser;
import com.builds.uimaps.fieldops.AdminFieldOpsManageConsultantPage;
import com.builds.utilities.FranconnectUtil;
import com.builds.utilities.TestCase;

public class AdminFieldOpsManageConsultantPageTest {

	FranconnectUtil fc = new FranconnectUtil();

	@Test(groups = { "fieldops"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify the Add User As A Consultant And Remove User As A Consultant", testCaseId = "TC_36_AddUser_As_Consultant")
	public void addUserAsConsultant() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("fieldops", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			AdminFieldOpsManageConsultantPage pobj = new AdminFieldOpsManageConsultantPage(driver);

			fc.utobj().printTestStep("Navigate To Admin > Users > Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUser = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();

			String emailId = "fieldopsautomation@staffex.com";
			CorporateUser corpUser = new CorporateUser();
			//corpUser.setConsultant("Y");
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporateUser.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			fc.utobj().printTestStep("Navigate To Admin > FieldOps > Manage Consultant Page");
			fc.utobj().printTestStep("Add User As A Consultant");
			addUserAsConsultant(driver, dataSet, corpUser.getuserFullName());
			fc.utobj().refresh(driver);

			fc.utobj().printTestStep("Verify Assign User As A Consultant");
			String text = fc.utobj().getElementByXpath(driver, ".//*[@id='CorporateUsers_y']/option[contains(text () ,"
					+ "'" + corpUser.getuserFullName() + "'" + ")]").getText().trim();

			if (text.equalsIgnoreCase(corpUser.getuserFullName().trim())) {
				System.out.println("Succesfully Assign corporate User as a consultant");
			} else {
				fc.utobj().throwsException("Was not able to assign user as Consultant");
			}

			fc.utobj().refresh(driver);

			fc.utobj().printTestStep("Remove User As A Consultant");
			WebElement element = fc.utobj().getElementByXpath(driver,
					".//*[@id='CorporateUsers_y']/option[contains(text () ," + "'" + corpUser.getuserFullName() + "'"
							+ ")]");
			fc.utobj().clickElement(driver, element);

			fc.utobj().clickElement(driver, pobj.optMoveLeft);

			fc.utobj().acceptAlertBox(driver);

			fc.utobj().printTestStep("Verify Remove User As A Consultant");
			String text1 = fc.utobj().getElementByXpath(driver, ".//*[@id='CorporateUsers_n']/option[contains(text () ,"
					+ "'" + corpUser.getuserFullName() + "'" + ")]").getText().trim();
			if (text1.equalsIgnoreCase(corpUser.getuserFullName().trim())) {
				System.out.println("Succesfully Assign corporate User as a consultant");
			} else {
				fc.utobj().throwsException("Was not able to assign user as Consultant");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	public void addUserAsConsultant(WebDriver driver, Map<String, String> dataSet, String userName) throws Exception {

		String testCaseId = "TC_Add_User_As_Consultant";

		if (fc.utobj().validate(testCaseId)) {
			try {
				AdminFieldOpsManageConsultantPage pobj = new AdminFieldOpsManageConsultantPage(driver);
				// Assign user as a consultant
				fc.fieldops().fieldops_common().adminFieldOpsManageConsultant(driver);
				WebElement element = fc.utobj().getElementByXpath(driver,
						".//*[@id='CorporateUsers_n']/option[contains(text () ," + "'" + userName + "'" + ")]");
				fc.utobj().clickElement(driver, element);
				fc.utobj().clickElement(driver, pobj.optMoveRight);
				fc.utobj().acceptAlertBox(driver);
				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException(
					"Was not able to add User As Consultant :: Admin > Field Ops > Manage Consultant");
		}
	}
}
