package com.builds.test.fieldops;

import java.util.List;
import java.util.Map;
import java.util.Set;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;
import com.builds.test.admin.AdminFranchiseLocationAddFranchiseLocationPageTest;
import com.builds.test.admin.AdminHiddenLinksConfigureFieldOpsSettingsPageTest;
import com.builds.test.admin.AdminUsersManageCorporateUsersAddCorporateUserPageTest;
import com.builds.test.common.CorporateUser;
import com.builds.uimaps.fieldops.AdminFieldOpsManageVisitFormCustomizeVisitFormPage;
import com.builds.uimaps.fieldops.AdminFieldOpsManageVisitFormPage;
import com.builds.uimaps.fieldops.AdminFieldOpsQuestionLibraryPage;
import com.builds.uimaps.fieldops.FieldOpsVisitsPage;
import com.builds.utilities.FranconnectUtil;
import com.builds.utilities.TestCase;

public class AdminFieldOpsManageVisitFormCustomizeVisitFormPageTest {
	FranconnectUtil fc = new FranconnectUtil();

	@Test(groups = { "fieldops", "fieldopsTest" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify the Add , Modify And Delete Question Into a visit Form", testCaseId = "TC_08_Add_Question")
	private void addQuestion() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("fieldops", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			AdminFieldOpsManageVisitFormCustomizeVisitFormPage pobj = new AdminFieldOpsManageVisitFormCustomizeVisitFormPage(driver);

			fc.utobj().printTestStep("Navigate To Admin > Fieldops > Manage Visit Form");
			fc.utobj().printTestStep("Add Private Visit Form");
			AdminFieldOpsManageVisitFormPageTest visitForm = new AdminFieldOpsManageVisitFormPageTest();
			visitForm.addVisitFormPrivate(driver, dataSet);

			fc.utobj().printTestStep("Add Question in Visit Form");
			fc.utobj().clickElement(driver, pobj.addQuestion);
			
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			List<WebElement> list = pobj.addQuestionNewOrLibraray;
			if (fc.utobj().isSelected(driver,list.get(1))) {
				fc.utobj().clickElement(driver, list.get(0));
			}
			String questionText = fc.utobj().generateTestData(dataSet.get("questionText"));
			fc.utobj().sendKeys(driver, pobj.questionTextArea, questionText);
			fc.utobj().sendKeys(driver, pobj.helpTextForUser, dataSet.get("helpForUserText"));
			fc.utobj().clickElement(driver, pobj.questionMandantory);
			fc.utobj().selectDropDown(driver, pobj.criticalLevel, dataSet.get("criticalLevel"));
			fc.utobj().clickElement(driver, pobj.nextBtn);

			String responseType = dataSet.get("responseTypeQuestion");
			fc.utobj().selectDropDown(driver, pobj.selectResponseType, responseType);

			fc.utobj().clickElement(driver, pobj.addBtn);

			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Verify Add Question In Visit Form");
			boolean isTextPresent = fc.utobj().assertPageSource(driver, questionText);
			if (isTextPresent == false) {
				fc.utobj().throwsException("was not able to add a question into a Visit Form!!");
			}

			fc.utobj().printTestStep("Modify Question In Visit Form");
			fc.utobj().clickElement(driver, pobj.modifyBtn);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			String questionTextM = fc.utobj().generateTestData(dataSet.get("questionText"));
			fc.utobj().sendKeys(driver, pobj.questionTextArea, questionTextM);
			fc.utobj().sendKeys(driver, pobj.helpTextForUser, dataSet.get("helpForUserText"));
			fc.utobj().selectDropDown(driver, pobj.criticalLevel, dataSet.get("criticalLevel"));
			fc.utobj().clickElement(driver, pobj.nextBtn);

			String responseTypeM = dataSet.get("responseTypeQuestionM");
			fc.utobj().selectDropDown(driver, pobj.selectResponseType, responseTypeM);

			fc.utobj().clickElement(driver, pobj.saveBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Verify The Modify Question In Visit Form");
			boolean isQuestionMPresent = fc.utobj().assertPageSource(driver, questionTextM);
			if (isQuestionMPresent == false) {
				fc.utobj().throwsException("was not able to modify a question into a Visit Form!!");
			}

			fc.utobj().printTestStep("Delete Question From Visit Form");
			fc.utobj().clickElement(driver, pobj.deleteBtn);
			fc.utobj().acceptAlertBox(driver);

			fc.utobj().printTestStep("Verify The Delete Question From Visit Form");
			boolean isTextDPresent = fc.utobj().assertPageSource(driver, questionTextM);

			if (isTextDPresent == true) {
				fc.utobj().throwsException("was not able to delete a question into a Visit Form!!");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
			
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	public String addQuestion(WebDriver driver, Map<String, String> dataSet) throws Exception {

		String testCaseId = "TC_Add_Question_Visit_Form";
		String questionText = null;

		if (fc.utobj().validate(testCaseId)) {
			try {
				AdminFieldOpsManageVisitFormCustomizeVisitFormPage pobj = new AdminFieldOpsManageVisitFormCustomizeVisitFormPage(
						driver);

				fc.commonMethods().switch_cboxIframe_frameId(driver);

				if (dataSet.get("questionTypeNewOrLibrary").equalsIgnoreCase("New Question")) {

					List<WebElement> list = pobj.addQuestionNewOrLibraray;

					if (fc.utobj().isSelected(driver,list.get(1))) {
						fc.utobj().clickElement(driver, list.get(0));
					} else {

					}
				}

				questionText = fc.utobj().generateTestData(dataSet.get("questionText"));
				fc.utobj().sendKeys(driver, pobj.questionTextArea, questionText);
				fc.utobj().selectDropDown(driver, pobj.criticalLevel, dataSet.get("criticalLevel"));
				fc.utobj().clickElement(driver, pobj.nextBtn);

				String responseType = dataSet.get("responseTypeQuestion");
				fc.utobj().selectDropDown(driver, pobj.selectResponseType, responseType);
				fc.utobj().clickElement(driver, pobj.addBtn);
				fc.utobj().switchFrameToDefault(driver);

				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException(
					"was not able to add Question In visit Form At Admin > Field Ops > Manage visit Form");

		}
		return questionText;
	}

	public String addQuestionInCategory(WebDriver driver, Map<String, String> dataSet, String categoryName)
			throws Exception {

		String testCaseId = "TC_Add_Question_In_Category";
		String questionText = null;

		if (fc.utobj().validate(testCaseId)) {
			try {
				AdminFieldOpsManageVisitFormCustomizeVisitFormPage pobj = new AdminFieldOpsManageVisitFormCustomizeVisitFormPage(
						driver);

				fc.commonMethods().switch_cboxIframe_frameId(driver);

				if (dataSet.get("questionTypeNewOrLibrary").equalsIgnoreCase("New Question")) {

					List<WebElement> list = pobj.addQuestionNewOrLibraray;

					if (fc.utobj().isSelected(driver,list.get(1))) {
						fc.utobj().clickElement(driver, list.get(0));
					} else {
					}
				}

				questionText = fc.utobj().generateTestData(dataSet.get("questionText"));
				fc.utobj().sendKeys(driver, pobj.questionTextArea, questionText);
				fc.utobj().selectDropDown(driver, pobj.criticalLevel, dataSet.get("criticalLevel"));

				fc.utobj().selectDropDown(driver, pobj.selectCategory, categoryName);
				fc.utobj().clickElement(driver, pobj.nextBtn);

				String responseType = dataSet.get("responseTypeQuestion");
				fc.utobj().selectDropDown(driver, pobj.selectResponseType, responseType);
				fc.utobj().clickElement(driver, pobj.addBtn);
				fc.utobj().switchFrameToDefault(driver);

				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException(
					"was not able to add Question In visit Form At Admin > Field Ops > Manage visit Form");

		}
		return questionText;
	}

	public String modifyQuestion(WebDriver driver, Map<String, String> dataSet) throws Exception {

		String testCaseId = "TC_Modify_Question_Visit_Form";
		String questionTextM = null;

		if (fc.utobj().validate(testCaseId)) {
			try {
				AdminFieldOpsManageVisitFormCustomizeVisitFormPage pobj = new AdminFieldOpsManageVisitFormCustomizeVisitFormPage(
						driver);
				/* fc.utobj().clickElement(driver, pobj.modifyBtn); */
				fc.commonMethods().switch_cboxIframe_frameId(driver);
				questionTextM = fc.utobj().generateTestData(dataSet.get("questionText"));
				fc.utobj().sendKeys(driver, pobj.questionTextArea, questionTextM);
				fc.utobj().sendKeys(driver, pobj.helpTextForUser, dataSet.get("helpForUserText"));
				fc.utobj().selectDropDown(driver, pobj.criticalLevel, dataSet.get("criticalLevel"));
				fc.utobj().clickElement(driver, pobj.nextBtn);

				String responseType = dataSet.get("responseTypeQuestion");
				fc.utobj().selectDropDown(driver, pobj.selectResponseType, responseType);

				fc.utobj().clickElement(driver, pobj.saveBtn);
				fc.utobj().switchFrameToDefault(driver);

				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException("was not able to modify Question in Visit Form");

		}
		return questionTextM;
	}

	@Test(groups = { "fieldops", "fieldopsTest" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify the Add , Modify And Delete Section From visit Form", testCaseId = "TC_11_Add_Section")
	private void addSection() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("fieldops", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			AdminFieldOpsManageVisitFormCustomizeVisitFormPage pobj = new AdminFieldOpsManageVisitFormCustomizeVisitFormPage(
					driver);

			fc.utobj().printTestStep("Navigate To Admin > Field Ops > Manage Visit Form");
			fc.utobj().printTestStep("Add Private Visit Form");

			AdminFieldOpsManageVisitFormPageTest visitForm = new AdminFieldOpsManageVisitFormPageTest();
			visitForm.addVisitFormPrivate(driver, dataSet);

			fc.utobj().printTestStep("Add Section In Visit Form");
			String sectionName = addSection(driver, config, dataSet);

			fc.utobj().printTestStep("Verify The Add Section In Visit Form");
			boolean isTextPresent = fc.utobj().assertPageSource(driver, sectionName);
			if (isTextPresent == false) {
				fc.utobj().throwsException("was not able to Add a Section into a Visit Form!!");
			}

			fc.utobj().printTestStep("Modify Section");
			fc.utobj().clickElement(driver, pobj.modifySection);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			String sectionNameM = fc.utobj().generateTestData(dataSet.get("sectionName"));
			fc.utobj().sendKeys(driver, pobj.sectionName, sectionNameM);
			fc.utobj().clickElement(driver, pobj.saveBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Verify The Modify Section In Visit Form");
			boolean isMPresent = fc.utobj().assertPageSource(driver, sectionNameM);
			if (isMPresent == false) {
				fc.utobj().throwsException("was not able to Modify a Section into a Visit Form!!");
			}

			fc.utobj().printTestStep("Delete Section From Visit Form");
			fc.utobj().clickElement(driver, pobj.deleteSection);
			fc.utobj().acceptAlertBox(driver);

			fc.utobj().printTestStep("Verify The Delete Section From Visit Form");
			boolean isDPresent = fc.utobj().assertPageSource(driver, sectionNameM);
			if (isDPresent == true) {
				fc.utobj().throwsException("was not able to Modify a Section into a Visit Form!!");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	public String addSection(WebDriver driver, Map<String, String> config, Map<String, String> dataSet)
			throws Exception {

		String testCaseId = "TC_Add_Section_Visit_Form";
		String sectionName = null;

		if (fc.utobj().validate(testCaseId)) {
			try {
				AdminFieldOpsManageVisitFormCustomizeVisitFormPage pobj = new AdminFieldOpsManageVisitFormCustomizeVisitFormPage(
						driver);
				fc.utobj().clickElement(driver, pobj.addSection);
				fc.commonMethods().switch_cboxIframe_frameId(driver);
				sectionName = fc.utobj().generateTestData(dataSet.get("sectionName"));
				fc.utobj().sendKeys(driver, pobj.sectionName, sectionName);
				fc.utobj().clickElement(driver, pobj.saveBtn);
				fc.utobj().switchFrameToDefault(driver);

				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException("was not abe to add Visit Form :: Admin > FieldOps  > Manage Visit Form");

		}
		return sectionName;
	}

	@Test(groups = { "fieldops", "fieldopsTest" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Add, Modify And Delete question From A section of Visit Form", testCaseId = "TC_14_Add_Question_Section")
	private void addQuestionInSection() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("fieldops", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			AdminFieldOpsManageVisitFormCustomizeVisitFormPage pobj = new AdminFieldOpsManageVisitFormCustomizeVisitFormPage(
					driver);

			fc.utobj().printTestStep("Navigate To Admin > Field Ops > Manage Visit Form");
			fc.utobj().printTestStep("Add Private Visit Form");

			AdminFieldOpsManageVisitFormPageTest visitForm = new AdminFieldOpsManageVisitFormPageTest();
			visitForm.addVisitFormPrivate(driver, dataSet);

			fc.utobj().printTestStep("Add Section In Visit Form");
			addSection(driver, config, dataSet);

			fc.utobj().printTestStep("Add Question In Section");
			fc.utobj().clickElement(driver, pobj.addQuestionInSection);
			String questionText = addQuestion(driver, dataSet);

			fc.utobj().printTestStep("Verify The Add Question In Section");
			boolean isTextPresent = fc.utobj().assertPageSource(driver, questionText);
			if (isTextPresent == false) {
				fc.utobj().throwsException("was not able to Add Question into Section of a Visit Form!!");
			}

			fc.utobj().printTestStep("Modify Question From Section");
			fc.utobj().clickElement(driver, pobj.modifyBtn);
			String questionTextM = modifyQuestion(driver, dataSet);

			fc.utobj().printTestStep("Verify The Modify Question From Section");
			boolean isMPresent = fc.utobj().assertPageSource(driver, questionTextM);
			if (isMPresent == false) {
				fc.utobj().throwsException("was not able to Add Question into Section of a Visit Form!!");
			}

			fc.utobj().printTestStep("Delete Question Form Section");
			fc.utobj().clickElement(driver, pobj.deleteBtn);
			fc.utobj().acceptAlertBox(driver);

			fc.utobj().printTestStep("Verify The Delete Question In Section");
			boolean isDPresent = fc.utobj().assertPageSource(driver, questionText);
			if (isDPresent == true) {
				fc.utobj().throwsException("was not able to Add Question into Section of a Visit Form!!");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "fieldops", "fieldopsTest" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify the Preview of Visit Form Into A Design Page", testCaseId = "TC_17_Preview_Visit_Form_Section")
	private void previewVisitFormInSection() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("fieldops", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {

			driver = fc.loginpage().login(driver);
			AdminFieldOpsManageVisitFormCustomizeVisitFormPage pobj = new AdminFieldOpsManageVisitFormCustomizeVisitFormPage(
					driver);
			String parentWindowHandle = driver.getWindowHandle();

			fc.utobj().printTestStep("Navigate To Admin > Fieldops > Manage Visit Form");
			fc.utobj().printTestStep("Add Private Visit Form");

			AdminFieldOpsManageVisitFormPageTest visitFormPage = new AdminFieldOpsManageVisitFormPageTest();
			String visitName = visitFormPage.addVisitFormPrivate(driver, dataSet);

			fc.utobj().printTestStep("Add Question In Visit Form");
			fc.utobj().clickElement(driver, pobj.addQuestion);
			String questionText = addQuestion(driver, dataSet);

			fc.utobj().printTestStep("Add Section In Visit Form");
			String sectionName = addSection(driver, config, dataSet);

			fc.utobj().printTestStep("Add Question In Section");
			fc.utobj().clickElement(driver, pobj.addQuestionInSection);
			String questionTextInSection = addQuestion(driver, dataSet);

			fc.utobj().printTestStep("Preview Visit Form");
			fc.utobj().clickElement(driver, pobj.previewForm);

			fc.utobj().printTestStep("Verify The Preview Of Visit Form At Design Page");
			// WindowHandle
			Set<String> allWindowHandles = driver.getWindowHandles();
			System.out.println(allWindowHandles.size());
			for (String currentWindowHandle : allWindowHandles) {
				if (!currentWindowHandle.equals(parentWindowHandle)) {
					driver.switchTo().window(currentWindowHandle);

					try {

						fc.utobj().getElementByXpath(driver, ".//strong[.=" + "" + visitName + "" + "]").isEnabled();

						boolean isTextPresent = fc.utobj().assertPageSource(driver, visitName);
						if (isTextPresent == false) {
							fc.utobj().throwsException("was not able to add a Visit form");
						}

						isTextPresent = fc.utobj().assertPageSource(driver, questionText);
						if (isTextPresent == false) {
							fc.utobj().throwsException("was not able to add a question into a Visit form");
						}

						isTextPresent = fc.utobj().assertPageSource(driver, sectionName);
						if (isTextPresent == false) {
							fc.utobj().throwsException("was not able to add section into a Visit form");
						}

						isTextPresent = fc.utobj().assertPageSource(driver, questionTextInSection);
						if (isTextPresent == false) {
							fc.utobj()
									.throwsException("was not able to add a  question into a section of a Visit form");
						}
						driver.close();

					} catch (Exception e) {
						driver.close();
					}
					driver.switchTo().window(parentWindowHandle);
				}

			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "fieldops", "fieldopsTest" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Add , Modify And Delete A Tab In Visit form", testCaseId = "TC_19_Add_Tab_Visit_Form")
	private void addTabInVisitForm() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("fieldops", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			AdminFieldOpsManageVisitFormCustomizeVisitFormPage pobj = new AdminFieldOpsManageVisitFormCustomizeVisitFormPage(
					driver);

			fc.utobj().printTestStep("Navigate To Admin > FieldOps > Manage Visit Form Page");
			fc.utobj().printTestStep("Add Private Visit Form With Tab View");
			AdminFieldOpsManageVisitFormPageTest visitForm = new AdminFieldOpsManageVisitFormPageTest();
			visitForm.addVisitFormTabViewPrivateYes(driver, config, dataSet);

			fc.utobj().printTestStep("Add Tab");
			String tabName = addTab(driver, config, dataSet);

			fc.utobj().printTestStep("Verify The Add Tab In Visit Form");
			boolean isTextPresent = fc.utobj().assertPageSource(driver, tabName);
			if (isTextPresent == false) {
				fc.utobj().throwsException("was not able to add a Tab into a Visit Form!!");
			}

			fc.utobj().printTestStep("Modify Tab");
			fc.utobj().clickElement(driver, pobj.modifyTab);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			String tabNameM = fc.utobj().generateTestData(dataSet.get("tabName"));
			fc.utobj().sendKeys(driver, pobj.tabName, tabNameM);
			fc.utobj().clickElement(driver, pobj.addBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Verify The Modify Tab");
			boolean isMPresent = fc.utobj().assertPageSource(driver, tabNameM);
			if (isMPresent == false) {
				fc.utobj().throwsException("was not able to Modify A Tab In Visit Form!!");
			}

			fc.utobj().printTestStep("Delete Tab");
			fc.utobj().clickElement(driver, pobj.deleteTab);
			fc.utobj().acceptAlertBox(driver);

			fc.utobj().printTestStep("Verify Delete The Tab From Visit Form");
			boolean isDPresent = fc.utobj().assertPageSource(driver, tabNameM);
			if (isDPresent == true) {
				fc.utobj().throwsException("was not able to Verify The Delete A Tab From A Visit Form!!");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	public String addTab(WebDriver driver, Map<String, String> config, Map<String, String> dataSet) throws Exception {

		String testCaseId = "TC_Add_Tab_Visit_Form";
		String tabName = null;

		if (fc.utobj().validate(testCaseId)) {
			try {
				AdminFieldOpsManageVisitFormCustomizeVisitFormPage pobj = new AdminFieldOpsManageVisitFormCustomizeVisitFormPage(
						driver);
				fc.utobj().clickElement(driver, pobj.addTab);
				fc.commonMethods().switch_cboxIframe_frameId(driver);
				tabName = fc.utobj().generateTestData(dataSet.get("tabName"));
				fc.utobj().sendKeys(driver, pobj.tabName, tabName);
				fc.utobj().clickElement(driver, pobj.addBtn);
				fc.utobj().switchFrameToDefault(driver);
				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException("was not able to add Tab :: Admin > Field Ops > Manage Visit Form");

		}
		return tabName;
	}

	public String addVisitFormTabViewPrivateNo(WebDriver driver, Map<String, String> config,
			Map<String, String> dataSet, String visitFormName) throws Exception {

		String testCaseId = "TC_500_Questions_Visit";
		
		try {
			AdminFieldOpsManageVisitFormPage pobj = new AdminFieldOpsManageVisitFormPage(driver);
			
			fc.fieldops().fieldops_common().adminFieldOpsManageVisitForm(driver);
			
			fc.utobj().clickElement(driver, pobj.addVisitForm);
			
			//visitFormName = fc.utobj().generateTestData(dataSet.get("visitFormName"));
			
			fc.utobj().sendKeys(driver, pobj.visitFormName, visitFormName);
			
			String description = fc.utobj().generateTestData(dataSet.get("description"));
			
			fc.utobj().sendKeys(driver, pobj.description, description);
			
			fc.utobj().selectDropDown(driver, pobj.selectFrequency, "Not Applicable");
			
			if (pobj.isPrivate.isSelected()) {
				fc.utobj().clickElement(driver, pobj.isPrivate);
			}
			
			fc.utobj().selectDropDown(driver, pobj.type, "Multi Page - Tab View");
			
			fc.utobj().clickElement(driver, pobj.tableLayout);
			
			WebElement enableOwnerResponse = driver.findElement(By.id("allowOwnerResponse"));
			
			if (enableOwnerResponse.isDisplayed()) {
				
				if (enableOwnerResponse.isSelected()) {
				
				} else {
					
					fc.utobj().clickElement(driver, enableOwnerResponse);
				}
			} else {

			}
			
			fc.utobj().clickElement(driver, pobj.showResponseScore);
			List<WebElement> list = pobj.allowComments;

			if (list.get(1).isSelected()) {
				fc.utobj().clickElement(driver, list.get(0));
			}
			//fc.utobj().sendKeys(driver, pobj.response, dataSet.get("response"));
			//fc.utobj().sendKeys(driver, pobj.comments, dataSet.get("comments"));

			fc.utobj().clickElement(driver, pobj.saveAndNextBtn);

			fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
		}
		return visitFormName;
	}
	
	@Test(groups = { "fieldops", "fieldopsTest" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Add Question in a Tab of Visit Form", testCaseId = "TC_22_Add_Question_In_Tab")
	private void addQuestionInTab() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("fieldops", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			AdminFieldOpsManageVisitFormCustomizeVisitFormPage pobj = new AdminFieldOpsManageVisitFormCustomizeVisitFormPage(
					driver);

			fc.utobj().printTestStep("Navigate To Admin > FieldOps > Manage Visit Form Page");
			fc.utobj().printTestStep("Add Private Visit Form With Tab View");
			AdminFieldOpsManageVisitFormPageTest visitForm = new AdminFieldOpsManageVisitFormPageTest();
			visitForm.addVisitFormTabViewPrivateYes(driver, config, dataSet);

			fc.utobj().printTestStep("Add Tab");
			addTab(driver, config, dataSet);

			fc.utobj().printTestStep("Add Question In Tab");
			fc.utobj().clickElement(driver, pobj.addQuestion);

			fc.commonMethods().switch_cboxIframe_frameId(driver);

			List<WebElement> list = pobj.addQuestionNewOrLibraray;
			if (fc.utobj().isSelected(driver,list.get(1))) {
				fc.utobj().clickElement(driver, list.get(0));
			} else {

			}
			String questionText = fc.utobj().generateTestData(dataSet.get("questionText"));
			fc.utobj().sendKeys(driver, pobj.questionTextArea, questionText);
			fc.utobj().sendKeys(driver, pobj.helpTextForUser, dataSet.get("helpForUserText"));
			fc.utobj().clickElement(driver, pobj.questionMandantory);
			fc.utobj().selectDropDown(driver, pobj.criticalLevel, dataSet.get("criticalLevel"));
			fc.utobj().clickElement(driver, pobj.nextBtn);

			String responseType = dataSet.get("responseTypeQuestion");
			fc.utobj().selectDropDown(driver, pobj.selectResponseType, responseType);

			fc.utobj().clickElement(driver, pobj.scoreInclude);
			fc.utobj().clickElement(driver, pobj.excludeNonResponse);
			fc.utobj().clickElement(driver, pobj.complianceNotApplicable);
			fc.utobj().clickElement(driver, pobj.addOnImg);
			fc.utobj().sendKeys(driver, pobj.rangeDetails_1rangeTo, dataSet.get("range1"));
			fc.utobj().sendKeys(driver, pobj.rangeDetails_1score, dataSet.get("score1"));
			fc.utobj().sendKeys(driver, pobj.rangeDetails_2score, dataSet.get("score2"));
			fc.utobj().clickElement(driver, pobj.addBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Verify The Add Question In Tab");
			boolean isTextPresent = fc.utobj().assertPageSource(driver, questionText);
			if (isTextPresent == false) {
				fc.utobj().throwsException("was not able to add question into a Tab of Visit Form");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "fieldopsYes")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify numbering in questions in preview audit form while Allow Questions Numbering in Section Wise is Yes", testCaseId = "TC_118_Verify_Question_Numbering_Visit_Form_Preview")
	private void verifyQuestionNumberingVisitFormPreview() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("fieldops", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {

			driver = fc.loginpage().login(driver);
			AdminFieldOpsManageVisitFormCustomizeVisitFormPage pobj = new AdminFieldOpsManageVisitFormCustomizeVisitFormPage(
					driver);
			String parentWindowHandle = driver.getWindowHandle();

			fc.utobj().printTestStep("Navigate To Admin > Hidden Links > Configure FieldOps Settings Page");
			fc.utobj().printTestStep("Allow Questions Numbering In Section Wise Option Yes");

			AdminHiddenLinksConfigureFieldOpsSettingsPageTest fieldopsSettings = new AdminHiddenLinksConfigureFieldOpsSettingsPageTest();
			fieldopsSettings.allowQuestionsNumberingInSectionWiseYes(driver, config);

			fc.utobj().printTestStep("Navigate To Admin > Fieldops > Manage Visit Form");
			fc.utobj().printTestStep("Add Private Visit Form");

			AdminFieldOpsManageVisitFormPageTest VisitFormPage = new AdminFieldOpsManageVisitFormPageTest();
			String visitName = VisitFormPage.addVisitFormPrivate(driver, dataSet);

			fc.utobj().printTestStep("Add Section In Visit Form");
			String sectionName = addSection(driver, config, dataSet);

			fc.utobj().printTestStep("Add Question In Section");
			fc.utobj().clickElement(driver, pobj.addQuestionInSection);
			addQuestion(driver, dataSet);

			fc.utobj().printTestStep("Add Another Question In Section");
			fc.utobj().clickElement(driver, pobj.addQuestionInSection);
			addQuestion(driver, dataSet);

			fc.utobj().printTestStep("Add Another Question In Section");
			fc.utobj().clickElement(driver, pobj.addQuestionInSection);
			addQuestion(driver, dataSet);

			fc.utobj().printTestStep("Add Second Section In Visit Form");
			String sectionName1 = addSection(driver, config, dataSet);

			fc.utobj().printTestStep("Add Question In Second Section");
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//*[.=" + "'" + sectionName1 + "'" + "]/../a[@id='addqueslink']"));
			addQuestion(driver, dataSet);

			fc.utobj().printTestStep("Add Question In Second Section");
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//*[.=" + "'" + sectionName1 + "'" + "]/../a[@id='addqueslink']"));
			addQuestion(driver, dataSet);

			fc.utobj().printTestStep("Add Question In Second Section");
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//*[.=" + "'" + sectionName1 + "'" + "]/../a[@id='addqueslink']"));
			addQuestion(driver, dataSet);

			fc.utobj().printTestStep("Preview Form");
			fc.utobj().clickElement(driver, pobj.previewForm);

			fc.utobj().printTestStep("Verify Question Numbering");
			// WindowHandle
			Set<String> allWindowHandles = driver.getWindowHandles();
			System.out.println(allWindowHandles.size());
			for (String currentWindowHandle : allWindowHandles) {
				if (!currentWindowHandle.equals(parentWindowHandle)) {
					driver.switchTo().window(currentWindowHandle);

					boolean status = false;

					try {
						fc.utobj()
								.getElementByXpath(driver,
										".//tr/td/label/strong[contains(text () ," + "'" + visitName + "'" + ")]")
								.isEnabled();
						status = true;
					} catch (Exception e) {
						driver.close();
					}

					if (status == true) {

						if (!fc.utobj()
								.getElementByXpath(driver,
										".//tr/td/strong[contains(text () ," + "'" + sectionName + "'"
												+ ")]/../../../../following-sibling::table[1]//strong[contains(text() , '1')]")
								.isEnabled()) {
							fc.utobj().throwsSkipException(
									"Was not able to Verify numbering in questions in preview audit form");
						}

						if (!fc.utobj()
								.getElementByXpath(driver,
										".//tr/td/strong[contains(text () ," + "'" + sectionName + "'"
												+ ")]/../../../../following-sibling::table[2]//strong[contains(text() , '2')]")
								.isEnabled()) {
							fc.utobj().throwsSkipException(
									"Was not able to Verify numbering in questions in preview audit form");
						}

						if (!fc.utobj()
								.getElementByXpath(driver,
										".//tr/td/strong[contains(text () ," + "'" + sectionName + "'"
												+ ")]/../../../../following-sibling::table[3]//strong[contains(text() , '3')]")
								.isEnabled()) {
							fc.utobj().throwsSkipException(
									"Was not able to Verify numbering in questions in preview audit form");
						}

						if (!fc.utobj()
								.getElementByXpath(driver,
										".//tr/td/strong[contains(text () ," + "'" + sectionName1 + "'"
												+ ")]/../../../../following-sibling::table[1]//strong[contains(text() , '1')]")
								.isEnabled()) {
							fc.utobj().throwsSkipException(
									"Was not able to Verify numbering in questions in preview audit form");
						}

						if (!fc.utobj()
								.getElementByXpath(driver,
										".//tr/td/strong[contains(text () ," + "'" + sectionName1 + "'"
												+ ")]/../../../../following-sibling::table[2]//strong[contains(text() , '2')]")
								.isEnabled()) {
							fc.utobj().throwsSkipException(
									"Was not able to Verify numbering in questions in preview audit form");
						}

						if (!fc.utobj()
								.getElementByXpath(driver,
										".//tr/td/strong[contains(text () ," + "'" + sectionName1 + "'"
												+ ")]/../../../../following-sibling::table[3]//strong[contains(text() , '3')]")
								.isEnabled()) {
							fc.utobj().throwsSkipException(
									"Was not able to Verify numbering in questions in preview audit form");
						}

						driver.close();
					}

					driver.switchTo().window(parentWindowHandle);
				}

			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "fieldops", "fieldopsTest" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify numbering in questions in preview audit form while Allow Questions Numbering in Section Wise is No", testCaseId = "TC_119_Verify_Question_Numbering_Visit_Form_Preview_No")
	private void verifyQuestionNumberingVisitFormPreviewNo() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("fieldops", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {

			driver = fc.loginpage().login(driver);
			AdminFieldOpsManageVisitFormCustomizeVisitFormPage pobj = new AdminFieldOpsManageVisitFormCustomizeVisitFormPage(
					driver);
			String parentWindowHandle = driver.getWindowHandle();

			fc.utobj().printTestStep("Navigate To Admin > Hidden Links > Configure FieldOps Settings Page");
			fc.utobj().printTestStep("Allow Questions Numbering In Section Wise Option No");

			AdminHiddenLinksConfigureFieldOpsSettingsPageTest fieldopsSettings = new AdminHiddenLinksConfigureFieldOpsSettingsPageTest();
			fieldopsSettings.allowQuestionsNumberingInSectionWiseNo(driver, config);

			fc.utobj().printTestStep("Navigate To Admin > Fieldops > Manage Visit Form");
			fc.utobj().printTestStep("Add Private Visit Form");

			AdminFieldOpsManageVisitFormPageTest visitForm = new AdminFieldOpsManageVisitFormPageTest();
			String visitName = visitForm.addVisitFormPrivate(driver, dataSet);

			fc.utobj().printTestStep("Add First Section In Visit Form");
			String sectionName = addSection(driver, config, dataSet);

			fc.utobj().printTestStep("Add Question In Section");
			fc.utobj().clickElement(driver, pobj.addQuestionInSection);
			addQuestion(driver, dataSet);

			fc.utobj().printTestStep("Add Question In Section");
			fc.utobj().clickElement(driver, pobj.addQuestionInSection);
			addQuestion(driver, dataSet);

			fc.utobj().printTestStep("Add Question In Section");
			fc.utobj().clickElement(driver, pobj.addQuestionInSection);
			addQuestion(driver, dataSet);

			fc.utobj().printTestStep("Add Section");
			String sectionName1 = addSection(driver, config, dataSet);

			fc.utobj().printTestStep("Add Question In Section");
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//*[.=" + "'" + sectionName1 + "'" + "]/../a[@id='addqueslink']"));
			addQuestion(driver, dataSet);

			fc.utobj().printTestStep("Add Question In Section");
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//*[.=" + "'" + sectionName1 + "'" + "]/../a[@id='addqueslink']"));
			addQuestion(driver, dataSet);

			fc.utobj().printTestStep("Add Question In Section");
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//*[.=" + "'" + sectionName1 + "'" + "]/../a[@id='addqueslink']"));
			addQuestion(driver, dataSet);

			fc.utobj().printTestStep("Preview Form");
			fc.utobj().clickElement(driver, pobj.previewForm);

			fc.utobj().printTestStep("Verify Question Numbering");
			// WindowHandle
			Set<String> allWindowHandles = driver.getWindowHandles();
			System.out.println(allWindowHandles.size());
			for (String currentWindowHandle : allWindowHandles) {
				if (!currentWindowHandle.equals(parentWindowHandle)) {
					driver.switchTo().window(currentWindowHandle);

					boolean status = false;

					try {

						fc.utobj()
								.getElementByXpath(driver,
										".//tr/td/label/strong[contains(text () ," + "'" + visitName + "'" + ")]")
								.isEnabled();
						status = true;
					} catch (Exception e) {
						driver.close();
					}

					if (status == true) {

						if (!fc.utobj()
								.getElementByXpath(driver,
										".//tr/td/strong[contains(text () ," + "'" + sectionName + "'"
												+ ")]/../../../../following-sibling::table[1]//strong[contains(text() , '1')]")
								.isEnabled()) {
							fc.utobj().throwsSkipException(
									"Was not able to Verify numbering in questions in preview audit form");
						}

						if (!fc.utobj()
								.getElementByXpath(driver,
										".//tr/td/strong[contains(text () ," + "'" + sectionName + "'"
												+ ")]/../../../../following-sibling::table[2]//strong[contains(text() , '2')]")
								.isEnabled()) {
							fc.utobj().throwsSkipException(
									"Was not able to Verify numbering in questions in preview audit form");
						}

						if (!fc.utobj()
								.getElementByXpath(driver,
										".//tr/td/strong[contains(text () ," + "'" + sectionName + "'"
												+ ")]/../../../../following-sibling::table[3]//strong[contains(text() , '3')]")
								.isEnabled()) {
							fc.utobj().throwsSkipException(
									"Was not able to Verify numbering in questions in preview audit form");
						}

						if (!fc.utobj()
								.getElementByXpath(driver,
										".//tr/td/strong[contains(text () ," + "'" + sectionName1 + "'"
												+ ")]/../../../../following-sibling::table[1]//strong[contains(text() , '4')]")
								.isEnabled()) {
							fc.utobj().throwsSkipException(
									"Was not able to Verify numbering in questions in preview audit form");
						}

						if (!fc.utobj()
								.getElementByXpath(driver,
										".//tr/td/strong[contains(text () ," + "'" + sectionName1 + "'"
												+ ")]/../../../../following-sibling::table[2]//strong[contains(text() , '5')]")
								.isEnabled()) {
							fc.utobj().throwsSkipException(
									"Was not able to Verify numbering in questions in preview audit form");
						}

						if (!fc.utobj()
								.getElementByXpath(driver,
										".//tr/td/strong[contains(text () ," + "'" + sectionName1 + "'"
												+ ")]/../../../../following-sibling::table[3]//strong[contains(text() , '6')]")
								.isEnabled()) {
							fc.utobj().throwsSkipException(
									"Was not able to Verify numbering in questions in preview audit form");
						}

						driver.close();
					}

					driver.switchTo().window(parentWindowHandle);
				}

			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "fieldops", "fieldops_5050", "fieldopsTest" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify Question Numbering in Visit Form After Modifying Visit Form while Allow Questions Numbering in Section Wise is No", testCaseId = "TC_125_Verify_Question_Numbering_After_Modify_Visit_Form_No")
	private void verifyQuestionNumberingModifyVisitNo() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("fieldops", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		Set<String> allWindowHandles = null;

		try {

			driver = fc.loginpage().login(driver);
			AdminFieldOpsManageVisitFormCustomizeVisitFormPage pobj = new AdminFieldOpsManageVisitFormCustomizeVisitFormPage(
					driver);
			String parentWindowHandle = driver.getWindowHandle();

			fc.utobj().printTestStep("Navigate To Admin > Hidden Links > Configure FieldOps Settings Page");
			fc.utobj().printTestStep("Allow Questions Numbering In Section Wise Option No");

			AdminHiddenLinksConfigureFieldOpsSettingsPageTest fieldopsSettings = new AdminHiddenLinksConfigureFieldOpsSettingsPageTest();
			fieldopsSettings.allowQuestionsNumberingInSectionWiseNo(driver, config);

			fc.utobj().printTestStep("Navigate To Admin > Fieldops > Manage Visit Form");
			fc.utobj().printTestStep("Add Private Visit Form");

			AdminFieldOpsManageVisitFormPageTest VisitFormPage = new AdminFieldOpsManageVisitFormPageTest();
			String visitName = VisitFormPage.addVisitFormPrivate(driver, dataSet);

			fc.utobj().printTestStep("Add Section In Visit Form");
			String sectionName = addSection(driver, config, dataSet);

			fc.utobj().printTestStep("Add Question In Section");
			fc.utobj().clickElement(driver, pobj.addQuestionInSection);
			addQuestion(driver, dataSet);

			fc.utobj().printTestStep("Add Question In Section");
			fc.utobj().clickElement(driver, pobj.addQuestionInSection);
			addQuestion(driver, dataSet);

			fc.utobj().printTestStep("Add Question In Section");
			fc.utobj().clickElement(driver, pobj.addQuestionInSection);
			addQuestion(driver, dataSet);

			fc.utobj().printTestStep("Add Section");
			String sectionName1 = addSection(driver, config, dataSet);

			fc.utobj().printTestStep("Add Question In Section");
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//*[.=" + "'" + sectionName1 + "'" + "]/../a[@id='addqueslink']"));
			addQuestion(driver, dataSet);

			fc.utobj().printTestStep("Add Question In Section");
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//*[.=" + "'" + sectionName1 + "'" + "]/../a[@id='addqueslink']"));
			addQuestion(driver, dataSet);

			fc.utobj().printTestStep("Add Question In Section");
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//*[.=" + "'" + sectionName1 + "'" + "]/../a[@id='addqueslink']"));
			addQuestion(driver, dataSet);

			fc.utobj().printTestStep("Preview Form");
			fc.utobj().clickElement(driver, pobj.previewForm);

			fc.utobj().printTestStep("Verify Question Numbering");
			// WindowHandle
			allWindowHandles = driver.getWindowHandles();
			System.out.println(allWindowHandles.size());
			for (String currentWindowHandle : allWindowHandles) {
				if (!currentWindowHandle.equals(parentWindowHandle)) {
					driver.switchTo().window(currentWindowHandle);

					boolean status = false;

					try {

						fc.utobj()
								.getElementByXpath(driver,
										".//tr/td/label/strong[contains(text () ," + "'" + visitName + "'" + ")]")
								.isEnabled();
						status = true;
					} catch (Exception e) {
						driver.close();
					}

					if (status == true) {

						if (!fc.utobj()
								.getElementByXpath(driver,
										".//tr/td/strong[contains(text () ," + "'" + sectionName + "'"
												+ ")]/../../../../following-sibling::table[1]//strong[contains(text() , '1')]")
								.isEnabled()) {
							fc.utobj().throwsSkipException(
									"Was not able to Verify numbering in questions in preview audit form");
						}

						if (!fc.utobj()
								.getElementByXpath(driver,
										".//tr/td/strong[contains(text () ," + "'" + sectionName + "'"
												+ ")]/../../../../following-sibling::table[2]//strong[contains(text() , '2')]")
								.isEnabled()) {
							fc.utobj().throwsSkipException(
									"Was not able to Verify numbering in questions in preview audit form");
						}

						if (!fc.utobj()
								.getElementByXpath(driver,
										".//tr/td/strong[contains(text () ," + "'" + sectionName + "'"
												+ ")]/../../../../following-sibling::table[3]//strong[contains(text() , '3')]")
								.isEnabled()) {
							fc.utobj().throwsSkipException(
									"Was not able to Verify numbering in questions in preview audit form");
						}

						if (!fc.utobj()
								.getElementByXpath(driver,
										".//tr/td/strong[contains(text () ," + "'" + sectionName1 + "'"
												+ ")]/../../../../following-sibling::table[1]//strong[contains(text() , '4')]")
								.isEnabled()) {
							fc.utobj().throwsSkipException(
									"Was not able to Verify numbering in questions in preview audit form");
						}

						if (!fc.utobj()
								.getElementByXpath(driver,
										".//tr/td/strong[contains(text () ," + "'" + sectionName1 + "'"
												+ ")]/../../../../following-sibling::table[2]//strong[contains(text() , '5')]")
								.isEnabled()) {
							fc.utobj().throwsSkipException(
									"Was not able to Verify numbering in questions in preview audit form");
						}

						if (!fc.utobj()
								.getElementByXpath(driver,
										".//tr/td/strong[contains(text () ," + "'" + sectionName1 + "'"
												+ ")]/../../../../following-sibling::table[3]//strong[contains(text() , '6')]")
								.isEnabled()) {
							fc.utobj().throwsSkipException(
									"Was not able to Verify numbering in questions in preview audit form");
						}

						driver.close();
					}

					driver.switchTo().window(parentWindowHandle);
				}

			}

			fc.utobj().clickElement(driver, pobj.finishBtn);
			fc.utobj().printTestStep("Modify Visit");
			AdminFieldOpsManageVisitFormPage pobj1 = new AdminFieldOpsManageVisitFormPage(driver);

			try {
				fc.utobj().clickElement(driver, pobj1.showAll);

			} catch (Exception e) {
				System.out.println(e.getMessage());
			}

			String text = fc.utobj()
					.getElementByXpath(driver,
							".//*[.=" + "'" + visitName + "'" + "]/../../td/div[@id='menuBar']/layer")
					.getAttribute("id");

			String alterText = text.replace("Actions_dynamicmenu", "");
			alterText = alterText.replace("Bar", "");
			System.out.println(alterText);

			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//*[.=" + "'" + visitName + "'" + "]/../../td/div[@id='menuBar']/layer/a/img"));

			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//*[@id='Actions_dynamicmenu" + alterText + "Menu']/span[contains(text () , 'Modify')]")); // modify

			String visitName1 = fc.utobj().generateTestData(dataSet.get("visitFormName"));
			fc.utobj().sendKeys(driver, pobj1.visitFormName, visitName1);
			fc.utobj().sendKeys(driver, pobj1.description, dataSet.get("description"));
			fc.utobj().clickElement(driver, pobj1.saveAndNextBtn);

			fc.utobj().printTestStep("Preview Form");
			fc.utobj().clickElement(driver, pobj.previewForm);
			fc.utobj().printTestStep("Verify Question Numbering After Modify Visit Form");
			// WindowHandle
			allWindowHandles = driver.getWindowHandles();
			System.out.println(allWindowHandles.size());
			for (String currentWindowHandle : allWindowHandles) {
				if (!currentWindowHandle.equals(parentWindowHandle)) {
					driver.switchTo().window(currentWindowHandle);

					boolean status = false;

					try {

						fc.utobj()
								.getElementByXpath(driver,
										".//tr/td/label/strong[contains(text () ," + "'" + visitName1 + "'" + ")]")
								.isEnabled();
						status = true;
					} catch (Exception e) {
						driver.close();
					}

					if (status == true) {

						if (!fc.utobj()
								.getElementByXpath(driver,
										".//tr/td/strong[contains(text () ," + "'" + sectionName + "'"
												+ ")]/../../../../following-sibling::table[1]//strong[contains(text() , '1')]")
								.isEnabled()) {
							fc.utobj().throwsSkipException(
									"Was not able to Verify numbering in questions in preview audit form");
						}

						if (!fc.utobj()
								.getElementByXpath(driver,
										".//tr/td/strong[contains(text () ," + "'" + sectionName + "'"
												+ ")]/../../../../following-sibling::table[2]//strong[contains(text() , '2')]")
								.isEnabled()) {
							fc.utobj().throwsSkipException(
									"Was not able to Verify numbering in questions in preview audit form");
						}

						if (!fc.utobj()
								.getElementByXpath(driver,
										".//tr/td/strong[contains(text () ," + "'" + sectionName + "'"
												+ ")]/../../../../following-sibling::table[3]//strong[contains(text() , '3')]")
								.isEnabled()) {
							fc.utobj().throwsSkipException(
									"Was not able to Verify numbering in questions in preview audit form");
						}

						if (!fc.utobj()
								.getElementByXpath(driver,
										".//tr/td/strong[contains(text () ," + "'" + sectionName1 + "'"
												+ ")]/../../../../following-sibling::table[1]//strong[contains(text() , '4')]")
								.isEnabled()) {
							fc.utobj().throwsSkipException(
									"Was not able to Verify numbering in questions in preview audit form");
						}

						if (!fc.utobj()
								.getElementByXpath(driver,
										".//tr/td/strong[contains(text () ," + "'" + sectionName1 + "'"
												+ ")]/../../../../following-sibling::table[2]//strong[contains(text() , '5')]")
								.isEnabled()) {
							fc.utobj().throwsSkipException(
									"Was not able to Verify numbering in questions in preview audit form");
						}

						if (!fc.utobj()
								.getElementByXpath(driver,
										".//tr/td/strong[contains(text () ," + "'" + sectionName1 + "'"
												+ ")]/../../../../following-sibling::table[3]//strong[contains(text() , '6')]")
								.isEnabled()) {
							fc.utobj().throwsSkipException(
									"Was not able to Verify numbering in questions in preview audit form");
						}

						driver.close();
					}

					driver.switchTo().window(parentWindowHandle);
				}

			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "fieldops", "fieldops_5050", "fieldopsTest" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify Question Numbering in Visit Form After Deleting question From Visit Form while Allow Questions Numbering in Section Wise is No", testCaseId = "TC_127_Verify_Question_Numbering_After_Delete_Question_No")
	private void verifyQuestionNumberingDeleteQuestionNo() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("fieldops", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		Set<String> allWindowHandles = null;

		try {

			driver = fc.loginpage().login(driver);
			AdminFieldOpsManageVisitFormCustomizeVisitFormPage pobj = new AdminFieldOpsManageVisitFormCustomizeVisitFormPage(
					driver);
			String parentWindowHandle = driver.getWindowHandle();

			fc.utobj().printTestStep("Navigate To Admin > Hidden Links > Configure FieldOps Settings Page");
			fc.utobj().printTestStep("Allow Questions Numbering In Section Wise Option No");

			AdminHiddenLinksConfigureFieldOpsSettingsPageTest fieldopsSettings = new AdminHiddenLinksConfigureFieldOpsSettingsPageTest();
			fieldopsSettings.allowQuestionsNumberingInSectionWiseNo(driver, config);

			fc.utobj().printTestStep("Navigate To Admin > Fieldops > Manage Visit Form");
			fc.utobj().printTestStep("Add Private Visit Form");

			AdminFieldOpsManageVisitFormPageTest VisitFormPage = new AdminFieldOpsManageVisitFormPageTest();
			String visitName = VisitFormPage.addVisitFormPrivate(driver, dataSet);

			fc.utobj().printTestStep("Add Section In Visit Form");
			String sectionName = addSection(driver, config, dataSet);

			fc.utobj().printTestStep("Add Question In Section");
			fc.utobj().clickElement(driver, pobj.addQuestionInSection);
			addQuestion(driver, dataSet);

			fc.utobj().printTestStep("Add Question In Section");
			fc.utobj().clickElement(driver, pobj.addQuestionInSection);
			addQuestion(driver, dataSet);

			fc.utobj().printTestStep("Add Question In Section");
			fc.utobj().clickElement(driver, pobj.addQuestionInSection);
			addQuestion(driver, dataSet);

			fc.utobj().printTestStep("Add Second Section In Visit Form");
			String sectionName1 = addSection(driver, config, dataSet);

			fc.utobj().printTestStep("Add Question In Section");
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//*[.=" + "'" + sectionName1 + "'" + "]/../a[@id='addqueslink']"));
			addQuestion(driver, dataSet);

			fc.utobj().printTestStep("Add Question In Section");
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//*[.=" + "'" + sectionName1 + "'" + "]/../a[@id='addqueslink']"));
			addQuestion(driver, dataSet);

			fc.utobj().printTestStep("Add Question In Section");
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//*[.=" + "'" + sectionName1 + "'" + "]/../a[@id='addqueslink']"));
			addQuestion(driver, dataSet);

			fc.utobj().printTestStep("Preview Form");
			fc.utobj().clickElement(driver, pobj.previewForm);

			fc.utobj().printTestStep("Verify Question Numbering");
			// WindowHandle
			allWindowHandles = driver.getWindowHandles();
			System.out.println(allWindowHandles.size());
			for (String currentWindowHandle : allWindowHandles) {
				if (!currentWindowHandle.equals(parentWindowHandle)) {
					driver.switchTo().window(currentWindowHandle);

					boolean status = false;

					try {

						fc.utobj()
								.getElementByXpath(driver,
										".//tr/td/label/strong[contains(text () ," + "'" + visitName + "'" + ")]")
								.isEnabled();
						status = true;
					} catch (Exception e) {
						driver.close();
					}

					if (status == true) {

						if (!fc.utobj()
								.getElementByXpath(driver,
										".//tr/td/strong[contains(text () ," + "'" + sectionName + "'"
												+ ")]/../../../../following-sibling::table[1]//strong[contains(text() , '1')]")
								.isEnabled()) {
							fc.utobj().throwsSkipException(
									"Was not able to Verify numbering in questions in preview audit form");
						}

						if (!fc.utobj()
								.getElementByXpath(driver,
										".//tr/td/strong[contains(text () ," + "'" + sectionName + "'"
												+ ")]/../../../../following-sibling::table[2]//strong[contains(text() , '2')]")
								.isEnabled()) {
							fc.utobj().throwsSkipException(
									"Was not able to Verify numbering in questions in preview audit form");
						}

						if (!fc.utobj()
								.getElementByXpath(driver,
										".//tr/td/strong[contains(text () ," + "'" + sectionName + "'"
												+ ")]/../../../../following-sibling::table[3]//strong[contains(text() , '3')]")
								.isEnabled()) {
							fc.utobj().throwsSkipException(
									"Was not able to Verify numbering in questions in preview audit form");
						}

						if (!fc.utobj()
								.getElementByXpath(driver,
										".//tr/td/strong[contains(text () ," + "'" + sectionName1 + "'"
												+ ")]/../../../../following-sibling::table[1]//strong[contains(text() , '4')]")
								.isEnabled()) {
							fc.utobj().throwsSkipException(
									"Was not able to Verify numbering in questions in preview audit form");
						}

						if (!fc.utobj()
								.getElementByXpath(driver,
										".//tr/td/strong[contains(text () ," + "'" + sectionName1 + "'"
												+ ")]/../../../../following-sibling::table[2]//strong[contains(text() , '5')]")
								.isEnabled()) {
							fc.utobj().throwsSkipException(
									"Was not able to Verify numbering in questions in preview audit form");
						}

						if (!fc.utobj()
								.getElementByXpath(driver,
										".//tr/td/strong[contains(text () ," + "'" + sectionName1 + "'"
												+ ")]/../../../../following-sibling::table[3]//strong[contains(text() , '6')]")
								.isEnabled()) {
							fc.utobj().throwsSkipException(
									"Was not able to Verify numbering in questions in preview audit form");
						}

						driver.close();
					}

					driver.switchTo().window(parentWindowHandle);
				}

			}

			fc.utobj().printTestStep("Delete Question");

			/*
			 * JavascriptExecutor executor = (JavascriptExecutor) driver;
			 * executor.executeScript("arguments[0].click();",
			 * fc.utobj().getElementByXpath(driver,".//*[@id='delete_2']/img")))
			 * ;
			 */

			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='delete_2']/img"));

			fc.utobj().acceptAlertBox(driver);

			fc.utobj().printTestStep("Preview Form");
			fc.utobj().clickElement(driver, pobj.previewForm);

			fc.utobj().printTestStep("Verify Question Numbering After Delete Question");
			// WindowHandle
			allWindowHandles = driver.getWindowHandles();
			System.out.println(allWindowHandles.size());
			for (String currentWindowHandle : allWindowHandles) {
				if (!currentWindowHandle.equals(parentWindowHandle)) {
					driver.switchTo().window(currentWindowHandle);

					boolean status = false;

					try {

						fc.utobj()
								.getElementByXpath(driver,
										".//tr/td/label/strong[contains(text () ," + "'" + visitName + "'" + ")]")
								.isEnabled();
						status = true;
					} catch (Exception e) {
						driver.close();
					}

					if (status == true) {

						if (!fc.utobj()
								.getElementByXpath(driver,
										".//tr/td/strong[contains(text () ," + "'" + sectionName + "'"
												+ ")]/../../../../following-sibling::table[1]//strong[contains(text() , '1')]")
								.isEnabled()) {
							fc.utobj().throwsSkipException(
									"Was not able to Verify numbering in questions in preview audit form");
						}

						if (!fc.utobj()
								.getElementByXpath(driver,
										".//tr/td/strong[contains(text () ," + "'" + sectionName + "'"
												+ ")]/../../../../following-sibling::table[2]//strong[contains(text() , '2')]")
								.isEnabled()) {
							fc.utobj().throwsSkipException(
									"Was not able to Verify numbering in questions in preview audit form");
						}

						if (!fc.utobj()
								.getElementByXpath(driver,
										".//tr/td/strong[contains(text () ," + "'" + sectionName1 + "'"
												+ ")]/../../../../following-sibling::table[1]//strong[contains(text() , '3')]")
								.isEnabled()) {
							fc.utobj().throwsSkipException(
									"Was not able to Verify numbering in questions in preview audit form");
						}

						if (!fc.utobj()
								.getElementByXpath(driver,
										".//tr/td/strong[contains(text () ," + "'" + sectionName1 + "'"
												+ ")]/../../../../following-sibling::table[2]//strong[contains(text() , '4')]")
								.isEnabled()) {
							fc.utobj().throwsSkipException(
									"Was not able to Verify numbering in questions in preview audit form");
						}

						if (!fc.utobj()
								.getElementByXpath(driver,
										".//tr/td/strong[contains(text () ," + "'" + sectionName1 + "'"
												+ ")]/../../../../following-sibling::table[3]//strong[contains(text() , '5')]")
								.isEnabled()) {
							fc.utobj().throwsSkipException(
									"Was not able to Verify numbering in questions in preview audit form");
						}

						driver.close();
					}

					driver.switchTo().window(parentWindowHandle);
				}

			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	// Create Visit Form With All Type Question On It

	@Test(groups = { "fieldops", "fieldopsTest" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify the Add All Type Questions Into a visit Form", testCaseId = "TC_Add_All_Type_Questions_In_Visit_Form_142")
	private void addAllTypeQuestionInVisit() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("fieldops", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			AdminFieldOpsManageVisitFormCustomizeVisitFormPage pobj = new AdminFieldOpsManageVisitFormCustomizeVisitFormPage(
					driver);

			fc.utobj().printTestStep("Navigate To Admin > Fieldops > Manage Visit Form");
			fc.utobj().printTestStep("Add Private Visit Form");
			AdminFieldOpsManageVisitFormPageTest visitForm = new AdminFieldOpsManageVisitFormPageTest();
			String visitFormName = visitForm.addVisitFormPrivate(driver, dataSet);

			fc.utobj().printTestStep("Add Paragraph Type Question in Visit Form");
			fc.utobj().clickElement(driver, pobj.addQuestion);
			fc.commonMethods().switch_cboxIframe_frameId(driver);

			List<WebElement> list = pobj.addQuestionNewOrLibraray;
			if (fc.utobj().isSelected(driver,list.get(1))) {
				fc.utobj().clickElement(driver, list.get(0));
			}
			
			String questionTextForParagraph = fc.utobj().generateTestData(dataSet.get("questionTextForParagraph"));
			fc.utobj().sendKeys(driver, pobj.questionTextArea, questionTextForParagraph);

			// add Attachement
			fc.utobj().selectDropDownByValue(driver, pobj.uploadType, "op1");

			String filePath = fc.utobj().getFilePathFromTestData(dataSet.get("fileName"));

			fc.utobj().sendKeys(driver, pobj.uploadAttachmentPath, filePath);
			fc.utobj().selectDropDown(driver, pobj.criticalLevel, dataSet.get("criticalLevel"));
			fc.utobj().clickElement(driver, pobj.nextBtn);

			fc.utobj().selectDropDown(driver, pobj.selectResponseType, "Paragraph Text");

			fc.utobj().clickElement(driver, pobj.addBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Add Single Line Text Type Question In Visit Form");
			fc.utobj().clickElement(driver, pobj.addQuestion);
			fc.commonMethods().switch_cboxIframe_frameId(driver);

			String questionTextForSingleLineText = fc.utobj()
					.generateTestData(dataSet.get("questionTextForSingleLineText"));
			fc.utobj().sendKeys(driver, pobj.questionTextArea, questionTextForSingleLineText);

			fc.utobj().selectDropDown(driver, pobj.criticalLevel, dataSet.get("criticalLevel"));
			fc.utobj().clickElement(driver, pobj.nextBtn);

			fc.utobj().selectDropDown(driver, pobj.selectResponseType, "Single Line Text");

			fc.utobj().clickElement(driver, pobj.addBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Add Date Type Question In Visit Form");
			fc.utobj().clickElement(driver, pobj.addQuestion);
			fc.commonMethods().switch_cboxIframe_frameId(driver);

			String questionTextForDate = fc.utobj().generateTestData(dataSet.get("questionTextForDate"));
			fc.utobj().sendKeys(driver, pobj.questionTextArea, questionTextForDate);

			fc.utobj().selectDropDown(driver, pobj.criticalLevel, dataSet.get("criticalLevel"));
			fc.utobj().clickElement(driver, pobj.nextBtn);

			fc.utobj().selectDropDown(driver, pobj.selectResponseType, "Date");

			fc.utobj().clickElement(driver, pobj.addBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Add Rating Type Question In Visit Form");
			fc.utobj().clickElement(driver, pobj.addQuestion);
			fc.commonMethods().switch_cboxIframe_frameId(driver);

			String questionTextForRating = fc.utobj().generateTestData(dataSet.get("questionTextForRating"));
			fc.utobj().sendKeys(driver, pobj.questionTextArea, questionTextForRating);

			fc.utobj().selectDropDown(driver, pobj.criticalLevel, dataSet.get("criticalLevel"));
			fc.utobj().clickElement(driver, pobj.nextBtn);

			fc.utobj().selectDropDown(driver, pobj.selectResponseType, "Rating");

			fc.utobj().clickElement(driver, pobj.scoreInclude);
			fc.utobj().clickElement(driver, pobj.excludeNonResponse);
			fc.utobj().clickElement(driver, pobj.notApplicableResponse);
			fc.utobj().clickElement(driver, pobj.complianceNotApplicable);
			fc.utobj().sendKeys(driver, pobj.minRange, dataSet.get("minRange"));
			fc.utobj().sendKeys(driver, pobj.maxRange, dataSet.get("maxRange"));
			fc.utobj().clickElement(driver, pobj.addBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Add Number Type Question In Visit Form");
			fc.utobj().clickElement(driver, pobj.addQuestion);
			fc.commonMethods().switch_cboxIframe_frameId(driver);

			String questionTextForNumber = fc.utobj().generateTestData(dataSet.get("questionTextForNumber"));
			fc.utobj().sendKeys(driver, pobj.questionTextArea, questionTextForNumber);

			fc.utobj().selectDropDown(driver, pobj.criticalLevel, dataSet.get("criticalLevel"));
			fc.utobj().clickElement(driver, pobj.nextBtn);

			fc.utobj().selectDropDown(driver, pobj.selectResponseType, "Number");
			fc.utobj().clickElement(driver, pobj.scoreInclude);
			fc.utobj().clickElement(driver, pobj.excludeNonResponse);
			fc.utobj().clickElement(driver, pobj.notApplicableResponse);

			fc.utobj().sendKeys(driver, pobj.rangeTo, dataSet.get("rangeTo"));
			fc.utobj().sendKeys(driver, pobj.rangeScore, dataSet.get("rangeScore"));
			fc.utobj().clickElement(driver, pobj.addBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Add Single Select Radio Button Type Question In Visit Form");
			fc.utobj().clickElement(driver, pobj.addQuestion);
			fc.commonMethods().switch_cboxIframe_frameId(driver);

			String questionTextForSingleRadioBtn = fc.utobj()
					.generateTestData(dataSet.get("questionTextForSingleRadioBtn"));
			fc.utobj().sendKeys(driver, pobj.questionTextArea, questionTextForSingleRadioBtn);

			fc.utobj().selectDropDown(driver, pobj.criticalLevel, dataSet.get("criticalLevel"));
			fc.utobj().clickElement(driver, pobj.nextBtn);

			AdminFieldOpsQuestionLibraryPage question_Library = new AdminFieldOpsQuestionLibraryPage(driver);
			fc.utobj().selectDropDownByValue(driver, pobj.selectResponseType, "radio");
			fc.utobj().clickElement(driver, pobj.scoreInclude);
			fc.utobj().clickElement(driver, pobj.excludeNonResponse);
			fc.utobj().clickElement(driver, pobj.complianceNotApplicable);
			fc.utobj().sendKeys(driver, question_Library.firstChoice, dataSet.get("firstChoice"));
			fc.utobj().sendKeys(driver, question_Library.secondChoice, dataSet.get("secondChoice"));
			fc.utobj().sendKeys(driver, question_Library.firstScore, dataSet.get("firstScore"));
			fc.utobj().sendKeys(driver, question_Library.secondScore, dataSet.get("secondScore"));
			fc.utobj().clickElement(driver, pobj.addBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Add Single Select Drop Down Type Question In Visit Form");
			fc.utobj().clickElement(driver, pobj.addQuestion);
			fc.commonMethods().switch_cboxIframe_frameId(driver);

			String questionTextForSingleSelectDropDown = fc.utobj()
					.generateTestData(dataSet.get("questionTextForSingleSelectDropDown"));
			fc.utobj().sendKeys(driver, pobj.questionTextArea, questionTextForSingleSelectDropDown);

			fc.utobj().selectDropDown(driver, pobj.criticalLevel, dataSet.get("criticalLevel"));
			fc.utobj().clickElement(driver, pobj.nextBtn);

			fc.utobj().selectDropDownByValue(driver, pobj.selectResponseType, "combo");
			fc.utobj().clickElement(driver, pobj.scoreInclude);
			fc.utobj().clickElement(driver, pobj.excludeNonResponse);
			fc.utobj().clickElement(driver, pobj.notApplicableResponse);
			fc.utobj().clickElement(driver, pobj.complianceNotApplicable);
			fc.utobj().sendKeys(driver, question_Library.firstChoice, dataSet.get("firstChoice"));
			fc.utobj().sendKeys(driver, question_Library.secondChoice, dataSet.get("secondChoice"));
			fc.utobj().sendKeys(driver, question_Library.firstScore, dataSet.get("firstScore"));
			fc.utobj().sendKeys(driver, question_Library.secondScore, dataSet.get("secondScore"));
			fc.utobj().clickElement(driver, pobj.addBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Add MultiSelect CheckBox Type Question In Visit Form");
			fc.utobj().clickElement(driver, pobj.addQuestion);
			fc.commonMethods().switch_cboxIframe_frameId(driver);

			String questionTextForMultiSelectCheckBox = fc.utobj()
					.generateTestData(dataSet.get("questionTextForMultiSelectCheckBox"));
			fc.utobj().sendKeys(driver, pobj.questionTextArea, questionTextForMultiSelectCheckBox);

			fc.utobj().selectDropDown(driver, pobj.criticalLevel, dataSet.get("criticalLevel"));
			fc.utobj().clickElement(driver, pobj.nextBtn);

			fc.utobj().selectDropDownByValue(driver, pobj.selectResponseType, "checkbox");
			fc.utobj().clickElement(driver, pobj.scoreInclude);
			fc.utobj().clickElement(driver, pobj.excludeNonResponse);
			fc.utobj().clickElement(driver, pobj.notApplicableResponse);
			fc.utobj().clickElement(driver, pobj.complianceNotApplicable);
			fc.utobj().sendKeys(driver, question_Library.firstChoice, dataSet.get("firstChoice"));
			fc.utobj().sendKeys(driver, question_Library.secondChoice, dataSet.get("secondChoice"));
			fc.utobj().sendKeys(driver, question_Library.firstScore, dataSet.get("firstScore"));
			fc.utobj().sendKeys(driver, question_Library.secondScore, dataSet.get("secondScore"));
			fc.utobj().clickElement(driver, pobj.addBtn);
			fc.utobj().switchFrameToDefault(driver);
/*
			fc.utobj().printTestStep("Add Smart Type Question In Visit Form");
			fc.utobj().clickElement(driver, pobj.addQuestion);
			fc.commonMethods().switch_cboxIframe_frameId(driver);

			String questionTextForSmart = fc.utobj().generateTestData(dataSet.get("questionTextForSmart"));
			fc.utobj().sendKeys(driver, pobj.questionTextArea, questionTextForSmart);

			fc.utobj().selectDropDown(driver, pobj.criticalLevel, dataSet.get("criticalLevel"));
			fc.utobj().clickElement(driver, pobj.nextBtn);

			// add Smart Type Question
			fc.utobj().selectDropDown(driver, pobj.selectResponseType, "Smart Question");
			
			// FiledOps Type Question
			/*WebElement element = fc.utobj().getElementByXpath(driver, ".//*[@id='ms-parentmoduleName']");
			fc.utobj().clickElement(driver, element);
			WebElement elementInput = element.findElement(By.xpath("./div/div/input"));
			String value = "Field Ops";
			fc.utobj().sendKeys(driver, elementInput, value);
			WebElement element3 = element.findElement(By.xpath(".//label[contains(text(),'" + value + "')]/input"));
			fc.utobj().clickElement(driver, element3);*/
			
		//	fc.utobj().selectValFromMultiSelect(driver, pobj.smartQModuleSelect, "Field Ops");
			

			// select field

			/*WebElement element1 = fc.utobj().getElementByXpath(driver, ".//*[@id='ms-parentmoduleField']");
			fc.utobj().clickElement(driver, element1);
			WebElement elementInput1 = element1.findElement(By.xpath("./div/div/input"));
			String value1 = "Date of last visit logged";
			fc.utobj().sendKeys(driver, elementInput1, value1);
			WebElement element4 = element1.findElement(By.xpath(".//label[contains(text(),'" + value1 + "')]/input"));
			fc.utobj().clickElement(driver, element4);*/
		/*	
			fc.utobj().selectValFromMultiSelect(driver, pobj.parentmoduleField, "Date of last visit logged");

			fc.utobj().clickElement(driver, pobj.addBtn);
			fc.utobj().switchFrameToDefault(driver);
*/
			fc.utobj().printTestStep("Add Multiple Input Type Question In Visit Form");
			fc.utobj().clickElement(driver, pobj.addQuestion);
			fc.commonMethods().switch_cboxIframe_frameId(driver);

			String questionTextForMultiInput = fc.utobj().generateTestData(dataSet.get("questionTextForMultiInput"));
			fc.utobj().sendKeys(driver, pobj.questionTextArea, questionTextForMultiInput);

			fc.utobj().selectDropDown(driver, pobj.criticalLevel, dataSet.get("criticalLevel"));
			fc.utobj().clickElement(driver, pobj.nextBtn);

			fc.utobj().selectDropDownByValue(driver, pobj.selectResponseType, "multiinput");
			fc.utobj().clickElement(driver, pobj.scoreInclude);
			fc.utobj().clickElement(driver, pobj.excludeNonResponse);
			fc.utobj().clickElement(driver, pobj.notApplicableResponse);
			fc.utobj().clickElement(driver, pobj.complianceNotApplicable);
			fc.utobj().sendKeys(driver, pobj.inputLabels1, "Input Label 1");
			fc.utobj().sendKeys(driver, pobj.inputLabels2, "Input Label 2");
			fc.utobj().sendKeys(driver, pobj.calculationResultLabel, "Calculation Result Label");
			fc.utobj().sendKeys(driver, pobj.rangeScore, "100");
			fc.utobj().clickElement(driver, pobj.addBtn);
			fc.utobj().switchFrameToDefault(driver);

			boolean isquestionTextForParagraph = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//label[contains(text () , '" + questionTextForParagraph + "')]");
			boolean isquestionTextForSingleLineText = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//label[contains(text () , '" + questionTextForSingleLineText + "')]");
			boolean isquestionTextForDate = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//label[contains(text () , '" + questionTextForDate + "')]");
			boolean isquestionTextForRating = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//label[contains(text () , '" + questionTextForRating + "')]");
			boolean isquestionTextForNumber = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//label[contains(text () , '" + questionTextForNumber + "')]");
			boolean isquestionTextForSingleRadioBtn = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//label[contains(text () , '" + questionTextForSingleRadioBtn + "')]");
			boolean isquestionTextForSingleSelectDropDown = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//label[contains(text () , '" + questionTextForSingleSelectDropDown + "')]");
			boolean isquestionTextForMultiSelectCheckBox = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//label[contains(text () , '" + questionTextForMultiSelectCheckBox + "')]");
			
			boolean isquestionTextForMultiInput = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//label[contains(text () , '" + questionTextForMultiInput + "')]");

			if (isquestionTextForParagraph == false) {
				fc.utobj().throwsException("was not able to verify the Paragraph Type");
			}

			if (isquestionTextForSingleLineText == false) {
				fc.utobj().throwsException("was not able to verify the Single Line Text");
			}

			if (isquestionTextForDate == false) {
				fc.utobj().throwsException("was not able to verify the Date Type");
			}

			if (isquestionTextForRating == false) {
				fc.utobj().throwsException("was not able to verify the Rating Type");
			}

			if (isquestionTextForNumber == false) {
				fc.utobj().throwsException("was not able to verify the Number Type");
			}

			if (isquestionTextForSingleRadioBtn == false) {
				fc.utobj().throwsException("was not able to verify the Single Select Radio Button Type Type");
			}

			if (isquestionTextForSingleSelectDropDown == false) {
				fc.utobj().throwsException("was not able to verify the Single Select Drop Down Type");
			}

			if (isquestionTextForMultiSelectCheckBox == false) {
				fc.utobj().throwsException("was not able to verify the Multi Select CheckBox Type Type");
			}
/*
			boolean isquestionTextForSmart = fc.utobj().verifyElementOnVisible_ByXpath(driver,".//label[contains(text () , '" + questionTextForSmart + "')]");
			
			if (isquestionTextForSmart == false) {
				fc.utobj().throwsException("was not able to verify the Smart Question Type");
			}
*/
			if (isquestionTextForMultiInput == false) {
				fc.utobj().throwsException("was not able to verify the Multiple Input Type");
			}

			fc.utobj().clickElement(driver, pobj.finishBtn);

			boolean isVisitFormIsPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () , '" + visitFormName + "')]");
			if (isVisitFormIsPresent == false) {
				fc.utobj().throwsException("was not able to add Visit Form With All Type Question In It");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "fieldops" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The After Modification of Visit containing smart question , score should reset", testCaseId = "TC_Verify_The_Reset_Score_143")
	public void createVisit() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("fieldops", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			FieldOpsVisitsPage pobj = new FieldOpsVisitsPage(driver);

			fc.utobj().printTestStep("Navigate To Admin > Users > Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUser = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			String emailId = "fieldopsautomation@staffex.com";
			CorporateUser corpUser = new CorporateUser();
			corpUser.setConsultant("Y");
			corpUser.setEmail(emailId);
			corpUser.setConsultant("Y");
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporateUser.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			fc.utobj().printTestStep("Navigate To Admin > Franchise Location > Add Franchise Location");
			fc.utobj().printTestStep("Add Franchise Location");
			String regionName = fc.utobj().generateTestData("TestR");
			String storeType = fc.utobj().generateTestData("TestS");
			String franchiseId = fc.utobj().generateTestData("TestFID");
			AdminFranchiseLocationAddFranchiseLocationPageTest franchise = new AdminFranchiseLocationAddFranchiseLocationPageTest();
			franchiseId = franchise.addFranchiseLocation_All(driver, franchiseId, regionName, storeType);

			fc.utobj().printTestStep("Add Private Visit Form");
			AdminFieldOpsManageVisitFormPageTest visitFormPage = new AdminFieldOpsManageVisitFormPageTest();
			String visitFormName = visitFormPage.addVisitFormTabViewPrivateYes(driver, config, dataSet);

			fc.utobj().printTestStep("Add First Tab");
			String tabName1 = fc.utobj().generateTestData(dataSet.get("tabName1"));
			addTab(driver, config, tabName1);

			fc.utobj().printTestStep("Add Second Tab");
			String tabName2 = fc.utobj().generateTestData(dataSet.get("tabName2"));
			addTab(driver, config, tabName2);

			fc.utobj().printTestStep("Add First Section");
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//a[.='" + tabName1 + "']"));
			
			String section1 = fc.utobj().generateTestData(dataSet.get("sectionName1"));
			addSection(driver, config, section1);

			fc.utobj().printTestStep("Add new smart question under section1 in tab1 without any score.");
			String questionText1 = dataSet.get("questionText1");
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//td[contains(text () , '" + section1 + "')]/a/span[.='Add Question']"));
			//	addQuestionSingleLineTextType(driver, config, dataSet, questionText1);
			addSmartTypeQuestion(driver, config, dataSet, questionText1);

			fc.utobj().printTestStep("Add Second Section");
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//a[.='" + tabName2 + "']"));
			String section2 = fc.utobj().generateTestData(dataSet.get("sectionName2"));
			addSection(driver, config, section2);

			fc.utobj().printTestStep("Now add new simple scorable question under section2 in tab2.");
			String questionText2 = dataSet.get("questionText2");
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//td[contains(text () , '" + section2 + "')]/a/span[.='Add Question']"));

			addQuestionSingleSelectDropDownType(driver, config, dataSet, questionText2);

			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//input[@value='Finish']"));

			fc.utobj().printTestStep("Create Visit Form");
			fc.fieldops().fieldops_common().fieldopsVisit(driver);

			fc.utobj().clickElement(driver, pobj.createVisit);

			// select visit Form
			fc.utobj().clickElement(driver, pobj.selectClick);
			fc.utobj().sendKeys(driver, pobj.searchBox, visitFormName);
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver,
							".//*[@id='ms-parentmasterVisitId']/div/ul/li/label[contains(text () ," + "'"
									+ visitFormName + "'" + ")]/input"));

			// select FranchiseID
			fc.utobj().clickElement(driver, pobj.franchiseeSelect);
			fc.utobj().sendKeys(driver, pobj.searchBoxFranchisee, franchiseId);
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver,
							".//*[@id='ms-parentfranchiseeNo']/div/ul/li/label[contains(text () , " + "'" + franchiseId
									+ "'" + ")]/input"));
			fc.utobj().clickElement(driver, pobj.franchiseeSelect);

			// Select Consultant
			fc.utobj().clickElement(driver, pobj.selectConsultant);
			fc.utobj().sendKeys(driver, pobj.consultantSearchBox, corpUser.getuserFullName());
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver,
							".//*[@id='ms-parentassignTo']/div/ul/li/label[contains(text () , " + "'"
									+ corpUser.getuserFullName() + "'" + ")]/input"));

			// schedule Date
			String curDate = fc.utobj().getCurrentDateUSFormat();

			fc.utobj().sendKeys(driver, pobj.scheduleDateTime, curDate);
			fc.utobj().sendKeys(driver, pobj.comments, "Test Comment");
			fc.utobj().clickElement(driver, pobj.srartNow);

			fc.utobj().printTestStep("Start Visit And Submit Response");

			// Click FirstTab
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//a[.='" + tabName1 + "']"));
			String curDate1 = fc.utobj().getCurrentDateUSFormat();

			tabName1 = tabName1.toLowerCase();
			fc.utobj().sendKeys(driver,
					fc.utobj().getElementByXpath(driver,
							".//table[contains(@id , '" + tabName1 + "')]//tr[@data-role='questionRow']//input"),
					curDate1);

			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//a[.='" + tabName2 + "']"));
			tabName2 = tabName2.toLowerCase();
			fc.utobj().selectDropDownByIndex(driver,
					fc.utobj().getElementByXpath(driver,
							".//table[contains(@id , '" + tabName2 + "')]//tr[@data-role='questionRow']//td/select"),
					1);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='nextSubmitAllButton']"));

			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='nextButton']"));
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='submitButton']"));

			String scoreOfConsultant = fc.utobj().getText(driver,
					fc.utobj().getElementByXpath(driver, ".//td[contains(text () , 'Score')]/following-sibling::td"));

			fc.utobj().printTestStep("Modify The Visit");

			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//input[@value='Modify']"));
			System.out.println(scoreOfConsultant);

			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//input[@name='button']"));
			fc.utobj().switchFrameToDefault(driver);

			String finalScore = fc.utobj().getText(driver,
					fc.utobj().getElementByXpath(driver, ".//*[@id='progressbar']/div[2]"));
			System.out.println(finalScore);

			fc.utobj().printTestStep("Verify The Final Score");

			if (finalScore.contains(scoreOfConsultant)) {
				fc.utobj().throwsException("was not able to Reset the Score ");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	public String addTab(WebDriver driver, Map<String, String> config, String tabName) throws Exception {

		String testCaseId = "TC_Add_Tab_Visit_Form";

		if (fc.utobj().validate(testCaseId)) {
			try {
				AdminFieldOpsManageVisitFormCustomizeVisitFormPage pobj = new AdminFieldOpsManageVisitFormCustomizeVisitFormPage(
						driver);
				fc.utobj().clickElement(driver, pobj.addTab);
				
				fc.commonMethods().switch_cboxIframe_frameId(driver);
				fc.utobj().sendKeys(driver, pobj.tabName, tabName);
				fc.utobj().clickElement(driver, pobj.addBtn);
				fc.utobj().switchFrameToDefault(driver);
				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
				
			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException("was not able to add Tab :: Admin > Field Ops > Manage Visit Form");

		}
		return tabName;
	}

	public String addSection(WebDriver driver, Map<String, String> config, String sectionName) throws Exception {

		String testCaseId = "TC_Add_Section_Visit_Form";

		if (fc.utobj().validate(testCaseId)) {
			try {
				AdminFieldOpsManageVisitFormCustomizeVisitFormPage pobj = new AdminFieldOpsManageVisitFormCustomizeVisitFormPage(
						driver);
				fc.utobj().clickElement(driver, pobj.addSection);
				fc.commonMethods().switch_cboxIframe_frameId(driver);
				fc.utobj().sendKeys(driver, pobj.sectionName, sectionName);
				fc.utobj().clickElement(driver, pobj.saveBtn);
				fc.utobj().switchFrameToDefault(driver);

				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException("was not abe to add Visit Form :: Admin > FieldOps  > Manage Visit Form");

		}
		return sectionName;
	}
	/*
	public String addSmartTypeQuestionQuestion(WebDriver driver, Map<String, String> config,
			Map<String, String> dataSet, String questionText) throws Exception {

		String testCaseId = "TC_Add_Question_Visit_Form";

		if (fc.utobj().validate(testCaseId)) {
			try {
				AdminFieldOpsManageVisitFormCustomizeVisitFormPage pobj = new AdminFieldOpsManageVisitFormCustomizeVisitFormPage(
						driver);

				fc.commonMethods().switch_cboxIframe_frameId(driver);
				if (dataSet.get("questionTypeNewOrLibrary").equalsIgnoreCase("New Question")) {
					List<WebElement> list = pobj.addQuestionNewOrLibraray;
					if (fc.utobj().isSelected(driver,list.get(1))) {
						fc.utobj().clickElement(driver, list.get(0));
					}
				}

				fc.utobj().sendKeys(driver, pobj.questionTextArea, questionText);
				fc.utobj().sendKeys(driver, pobj.helpTextForUser, dataSet.get("helpForUserText"));
				fc.utobj().clickElement(driver, pobj.questionMandantory);
				fc.utobj().selectDropDown(driver, pobj.criticalLevel, dataSet.get("criticalLevel"));
				fc.utobj().clickElement(driver, pobj.nextBtn);

				// add Smart Type Question
				fc.utobj().selectDropDown(driver, pobj.selectResponseType, "Smart Question");

				// FiledOps Type Question
*/
				/*WebElement element = fc.utobj().getElementByXpath(driver, ".//*[@id='ms-parentmoduleName']");
				fc.utobj().clickElement(driver, element);
				WebElement elementInput = element.findElement(By.xpath("./div/div/input"));
				String value = "Field Ops";
				fc.utobj().sendKeys(driver, elementInput, value);
				WebElement element3 = element.findElement(By.xpath(".//label[contains(text(),'" + value + "')]/input"));
				fc.utobj().clickElement(driver, element3);*/
/*
				fc.utobj().selectValFromMultiSelect(driver, pobj.smartQModuleSelect, "Field Ops");
*/				
				// select field

				/*WebElement element1 = fc.utobj().getElementByXpath(driver, ".//*[@id='ms-parentmoduleField']");
				fc.utobj().clickElement(driver, element1);
				WebElement elementInput1 = element1.findElement(By.xpath("./div/div/input"));
				String value1 = "Date of last visit logged";
				fc.utobj().sendKeys(driver, elementInput1, value1);
				WebElement element4 = element1
						.findElement(By.xpath(".//label[contains(text(),'" + value1 + "')]/input"));
				fc.utobj().clickElement(driver, element4);*/
/*
				fc.utobj().selectValFromMultiSelect(driver, pobj.parentmoduleField, "Date of last visit logged");
				
				fc.utobj().clickElement(driver, pobj.addBtn);

				fc.utobj().switchFrameToDefault(driver);

				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException(
					"was not able to add Question In visit Form :: Admin > Field Ops > Manage visit Form");

		}
		return questionText;
	}
*/
	public String addQuestionSingleSelectDropDownType(WebDriver driver, Map<String, String> config,
			Map<String, String> dataSet, String questionText) throws Exception {

		String testCaseId = "TC_Add_Question_Single_Select_DropDown";

		if (fc.utobj().validate(testCaseId)) {
			try {
				AdminFieldOpsQuestionLibraryPage pobj = new AdminFieldOpsQuestionLibraryPage(driver);
				fc.commonMethods().switch_cboxIframe_frameId(driver);
				fc.utobj().sendKeys(driver, pobj.questionTextArea, questionText);
				fc.utobj().sendKeys(driver, pobj.helpTextForUser, dataSet.get("helpForUserText"));
				fc.utobj().clickElement(driver, pobj.questionMandantory);
				fc.utobj().selectDropDown(driver, pobj.criticalLevel, dataSet.get("criticalLevel"));
				fc.utobj().clickElement(driver, pobj.nextBtn);

				fc.utobj().selectDropDown(driver, pobj.selectResponseType, "Single Select (Drop Down)");
				// fc.utobj().clickElement(driver, pobj.allowUserComments);
				fc.utobj().clickElement(driver, pobj.scoreInclude);
				fc.utobj().clickElement(driver, pobj.excludeNonResponse);
				fc.utobj().clickElement(driver, pobj.notApplicableResponse);
				fc.utobj().clickElement(driver, pobj.complianceNotApplicable);
				fc.utobj().sendKeys(driver, pobj.firstChoice, dataSet.get("firstChoice"));
				fc.utobj().sendKeys(driver, pobj.secondChoice, dataSet.get("secondChoice"));
				fc.utobj().sendKeys(driver, pobj.firstScore, dataSet.get("firstScore"));
				fc.utobj().sendKeys(driver, pobj.secondScore, dataSet.get("secondScore"));
				fc.utobj().clickElement(driver, pobj.saveBtn);
				fc.utobj().switchFrameToDefault(driver);
				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException("Was not able to add Question Single Select DropDown Type");

		}
		return questionText;
	}
	
	/*
	
	public String addQuestionSingleSelectDropDownType(WebDriver driver, Map<String, String> config,
			Map<String, String> dataSet, String questionText) throws Exception {

		String testCaseId = "TC_Add_Question_Single_Select_DropDown";

		if (fc.utobj().validate(testCaseId)) {
			try {
				AdminFieldOpsQuestionLibraryPage pobj = new AdminFieldOpsQuestionLibraryPage(driver);
				fc.commonMethods().switch_cboxIframe_frameId(driver);
				fc.utobj().sendKeys(driver, pobj.questionTextArea, questionText);
				fc.utobj().sendKeys(driver, pobj.helpTextForUser, dataSet.get("helpForUserText"));
				fc.utobj().clickElement(driver, pobj.questionMandantory);
				fc.utobj().selectDropDown(driver, pobj.criticalLevel, dataSet.get("criticalLevel"));
				fc.utobj().clickElement(driver, pobj.nextBtn);

				fc.utobj().selectDropDown(driver, pobj.selectResponseType, "Single Select (Drop Down)");
				// fc.utobj().clickElement(driver, pobj.allowUserComments);
				fc.utobj().clickElement(driver, pobj.scoreInclude);
				fc.utobj().clickElement(driver, pobj.excludeNonResponse);
				fc.utobj().clickElement(driver, pobj.notApplicableResponse);
				fc.utobj().clickElement(driver, pobj.complianceNotApplicable);
				fc.utobj().sendKeys(driver, pobj.firstChoice, dataSet.get("firstChoice"));
				fc.utobj().sendKeys(driver, pobj.secondChoice, dataSet.get("secondChoice"));
				fc.utobj().sendKeys(driver, pobj.firstScore, dataSet.get("firstScore"));
				fc.utobj().sendKeys(driver, pobj.secondScore, dataSet.get("secondScore"));
				fc.utobj().clickElement(driver, pobj.saveBtn);
				fc.utobj().switchFrameToDefault(driver);
				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException("Was not able to add Question Single Select DropDown Type");

		}
		return questionText;
	}
	
	public String addQuestionMultipleInputType(WebDriver driver, Map<String, String> config,
			Map<String, String> dataSet, String questionText) throws Exception {

		String testCaseId = "TC_Add_Question_Multiple_Input_Type";

		if (fc.utobj().validate(testCaseId)) {
			try {
				AdminFieldOpsManageVisitFormCustomizeVisitFormPage pobj = new AdminFieldOpsManageVisitFormCustomizeVisitFormPage(
						driver);
				fc.commonMethods().switch_cboxIframe_frameId(driver);
				
				 * String questionTextForMultiInput =
				 * fc.utobj().generateTestData(dataSet.get(
				 * "questionTextForMultiInput"));
				 
				fc.utobj().sendKeys(driver, pobj.questionTextArea, questionText);
				fc.utobj().selectDropDown(driver, pobj.criticalLevel, dataSet.get("criticalLevel"));
				fc.utobj().clickElement(driver, pobj.nextBtn);

				fc.utobj().selectDropDownByValue(driver, pobj.selectResponseType, "multiinput");
				fc.utobj().clickElement(driver, pobj.scoreInclude);
				fc.utobj().clickElement(driver, pobj.excludeNonResponse);
				fc.utobj().clickElement(driver, pobj.notApplicableResponse);
				fc.utobj().clickElement(driver, pobj.complianceNotApplicable);
				fc.utobj().sendKeys(driver, pobj.inputLabels1,  "Input Label 1"  dataSet.get("inputLabel1"));
				fc.utobj().sendKeys(driver, pobj.inputLabels2,  "Input Label 2"  dataSet.get("inputLabel2"));
				fc.utobj().sendKeys(driver, pobj.calculationResultLabel,  "Calculation Result Label"  dataSet.get("calculationResultLabel"));
				fc.utobj().sendKeys(driver, pobj.rangeScore, dataSet.get("rangeScore"));
				fc.utobj().clickElement(driver, pobj.addBtn);
				fc.utobj().switchFrameToDefault(driver);
				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException("Was not able to add Question Multiple Input Type");

		}
		return questionText;
	}
	*/
	
	public String addQuestionMultipleInputType( WebDriver driver, Map<String, String> config,
		Map<String, String> dataSet, String questionText) throws Exception {
			
			try {
			
			AdminFieldOpsManageVisitFormCustomizeVisitFormPage pobj = new AdminFieldOpsManageVisitFormCustomizeVisitFormPage(driver);
						
			fc.utobj().printTestStep("Add Multiple Input Type Question In Visit Form");
			
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().sendKeys(driver, pobj.questionTextArea, questionText);
			fc.utobj().selectDropDown(driver, pobj.criticalLevel, dataSet.get("criticalLevel"));
			fc.utobj().clickElement(driver, pobj.nextBtn);
			fc.utobj().selectDropDownByValue(driver, pobj.selectResponseType, "multiinput");
			fc.utobj().clickElement(driver, pobj.scoreInclude);
			fc.utobj().clickElement(driver, pobj.excludeNonResponse);
			fc.utobj().clickElement(driver, pobj.notApplicableResponse);
			fc.utobj().clickElement(driver, pobj.complianceNotApplicable);
			fc.utobj().sendKeys(driver, pobj.inputLabels1, "Input Label 1");
			fc.utobj().sendKeys(driver, pobj.inputLabels2, "Input Label 2");
			fc.utobj().sendKeys(driver, pobj.calculationResultLabel, "Calculation Result Label");
			fc.utobj().sendKeys(driver, pobj.rangeScore, "100");
			fc.utobj().clickElement(driver, pobj.addBtn);
			fc.utobj().switchFrameToDefault(driver);
								
			} catch (Exception e) {
				
			}
			return questionText;
		}
	
	public String addSmartTypeQuestion(WebDriver driver, Map<String, String> config,
			Map<String, String> dataSet, String questionText) throws Exception {

		String testCaseId = "TC_Add_Question_Visit_Form";

		if (fc.utobj().validate(testCaseId)) {
			try {
				AdminFieldOpsManageVisitFormCustomizeVisitFormPage pobj = new AdminFieldOpsManageVisitFormCustomizeVisitFormPage(
						driver);
				String questionTypeNewOrLibrary = "New Question";
				fc.commonMethods().switch_cboxIframe_frameId(driver);
				if (questionTypeNewOrLibrary.equalsIgnoreCase("New Question")) {
					List<WebElement> list = pobj.addQuestionNewOrLibraray;
					if (fc.utobj().isSelected(driver,list.get(1))) {
						fc.utobj().clickElement(driver, list.get(0));
					}
				}

				fc.utobj().sendKeys(driver, pobj.questionTextArea, questionText);
				fc.utobj().sendKeys(driver, pobj.helpTextForUser, "helpForUserText");
				fc.utobj().clickElement(driver, pobj.questionMandantory);
				//fc.utobj().selectDropDown(driver, pobj.criticalLevel, "");
				fc.utobj().clickElement(driver, pobj.nextBtn);

				// add Smart Type Question
				fc.utobj().selectDropDown(driver, pobj.selectResponseType, "Smart Question");

				// FiledOps Type Question

				WebElement element = fc.utobj().getElementByXpath(driver, ".//*[@id='ms-parentmoduleName']");
				fc.utobj().clickElement(driver, element);
				WebElement elementInput = element.findElement(By.xpath("./div/div/input"));
				String value = "Field Ops";
				fc.utobj().sendKeys(driver, elementInput, value);
				WebElement element3 = element.findElement(By.xpath(".//label[contains(text(),'" + value + "')]/input"));
				fc.utobj().clickElement(driver, element3);

				// select field
				Thread.sleep(1000);
				
				WebElement element1 = driver.findElement(By.xpath(".//*[@id='ms-parentmoduleField']/button"));
				fc.utobj().clickElement(driver, element1);
				
				//WebElement elementInput1 = element1.findElement(By.xpath("./div/div/input"));
				//String value1 = "Date of last visit logged";
				//fc.utobj().sendKeys(driver, elementInput1, value1);
				//WebElement element4 = element1.findElement(By.xpath(".//label[contains(text(),'" + value1 + "')]/input"));
				//fc.utobj().clickElement(driver, element4);
				
				WebElement element5 = fc.utobj().getElementByXpath(driver, ".//*[@id='ms-parentmoduleField']//*[contains(text(), 'Date of last visit logged')]");
				fc.utobj().clickElement(driver, element5);

				fc.utobj().clickElement(driver, pobj.addBtn);

				fc.utobj().switchFrameToDefault(driver);

				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException(
					"was not able to add Question In visit Form :: Admin > Field Ops > Manage visit Form");

		}
		return questionText;
	}

	public String addQuestionParagraphType( WebDriver driver, Map<String, String> config,
	Map<String, String> dataSet, String questionText) throws Exception {
		
		try {
		
		AdminFieldOpsManageVisitFormCustomizeVisitFormPage pobj = new AdminFieldOpsManageVisitFormCustomizeVisitFormPage(driver);
		
		fc.utobj().printTestStep("Add Paragraph Type Question In First Tab");
					
		fc.commonMethods().switch_cboxIframe_frameId(driver);

		List<WebElement> list = pobj.addQuestionNewOrLibraray;
		if (fc.utobj().isSelected(driver,list.get(1))) {
			fc.utobj().clickElement(driver, list.get(0));
		} 
		fc.utobj().sendKeys(driver, pobj.questionTextArea, questionText);

		fc.utobj().selectDropDown(driver, pobj.criticalLevel, dataSet.get("criticalLevel"));
		fc.utobj().clickElement(driver, pobj.nextBtn);

		fc.utobj().selectDropDown(driver, pobj.selectResponseType, "Paragraph Text");

		fc.utobj().clickElement(driver, pobj.addBtn);
		fc.utobj().switchFrameToDefault(driver);
		
		} catch (Exception e) {
			
		}
		return questionText;
	}

	public String addQuestionSingleLineTextType( WebDriver driver, Map<String, String> config,
	Map<String, String> dataSet, String questionText) throws Exception {
		
		try {
		
		AdminFieldOpsManageVisitFormCustomizeVisitFormPage pobj = new AdminFieldOpsManageVisitFormCustomizeVisitFormPage(driver);
		fc.utobj().printTestStep("Add Single Line Text Type Question In Visit Form");
		
		fc.commonMethods().switch_cboxIframe_frameId(driver);
		
		fc.utobj().sendKeys(driver, pobj.questionTextArea, questionText);

		fc.utobj().selectDropDown(driver, pobj.criticalLevel, dataSet.get("criticalLevel"));
		fc.utobj().clickElement(driver, pobj.nextBtn);

		fc.utobj().selectDropDown(driver, pobj.selectResponseType, "Single Line Text");

		fc.utobj().clickElement(driver, pobj.addBtn);
		fc.utobj().switchFrameToDefault(driver);
		
		} catch (Exception e) {
			
		}
		return questionText;
	}
	
	public String addQuestionDateType( WebDriver driver, Map<String, String> config,
			Map<String, String> dataSet, String questionText) throws Exception {
				
				try {
				
				AdminFieldOpsManageVisitFormCustomizeVisitFormPage pobj = new AdminFieldOpsManageVisitFormCustomizeVisitFormPage(driver);
				
				fc.utobj().printTestStep("Add Date Type Question In Visit Form");
				
				fc.commonMethods().switch_cboxIframe_frameId(driver);
				
				fc.utobj().sendKeys(driver, pobj.questionTextArea, questionText);

				fc.utobj().selectDropDown(driver, pobj.criticalLevel, dataSet.get("criticalLevel"));
				
				fc.utobj().clickElement(driver, pobj.nextBtn);

				fc.utobj().selectDropDown(driver, pobj.selectResponseType, "Date");

				fc.utobj().clickElement(driver, pobj.addBtn);
				
				fc.utobj().switchFrameToDefault(driver);
				
				} catch (Exception e) {
					
				}
				return questionText;
			}
			
	public String addQuestionNumberType( WebDriver driver, Map<String, String> config,
			Map<String, String> dataSet, String questionText) throws Exception {
				
				try {
				
				AdminFieldOpsManageVisitFormCustomizeVisitFormPage pobj = new AdminFieldOpsManageVisitFormCustomizeVisitFormPage(driver);
				
				fc.utobj().printTestStep("Add Number Type Question In Visit Form");
				
				fc.commonMethods().switch_cboxIframe_frameId(driver);
				
				fc.utobj().sendKeys(driver, pobj.questionTextArea, questionText);
				
				fc.utobj().selectDropDown(driver, pobj.criticalLevel, dataSet.get("criticalLevel"));
				fc.utobj().clickElement(driver, pobj.nextBtn);

				fc.utobj().selectDropDown(driver, pobj.selectResponseType, "Number");
				fc.utobj().clickElement(driver, pobj.scoreInclude);
				fc.utobj().clickElement(driver, pobj.excludeNonResponse);
				fc.utobj().clickElement(driver, pobj.notApplicableResponse);

				fc.utobj().sendKeys(driver, pobj.rangeTo, dataSet.get("rangeTo"));
				fc.utobj().sendKeys(driver, pobj.rangeScore, dataSet.get("rangeScore"));
				fc.utobj().clickElement(driver, pobj.addBtn);
				fc.utobj().switchFrameToDefault(driver);
				
				} catch (Exception e) {
					
				}
				return questionText;
			}
			
	public String addQuestionMultiSelectCheckboxType( WebDriver driver, Map<String, String> config,
			Map<String, String> dataSet, String questionText) throws Exception {
				
				try {
				
				AdminFieldOpsManageVisitFormCustomizeVisitFormPage pobj = new AdminFieldOpsManageVisitFormCustomizeVisitFormPage(driver);
				AdminFieldOpsQuestionLibraryPage question_Library = new AdminFieldOpsQuestionLibraryPage(driver);
				fc.utobj().printTestStep("Add MultiSelect CheckBox Type Question In Visit Form");
				
				fc.commonMethods().switch_cboxIframe_frameId(driver);
				
				fc.utobj().sendKeys(driver, pobj.questionTextArea, questionText);

				fc.utobj().selectDropDown(driver, pobj.criticalLevel, dataSet.get("criticalLevel"));
				fc.utobj().clickElement(driver, pobj.nextBtn);

				fc.utobj().selectDropDownByValue(driver, pobj.selectResponseType, "checkbox");
				fc.utobj().clickElement(driver, pobj.scoreInclude);
				fc.utobj().clickElement(driver, pobj.excludeNonResponse);
				fc.utobj().clickElement(driver, pobj.notApplicableResponse);
				fc.utobj().clickElement(driver, pobj.complianceNotApplicable);
				fc.utobj().sendKeys(driver, question_Library.firstChoice, dataSet.get("firstChoice"));
				fc.utobj().sendKeys(driver, question_Library.secondChoice, dataSet.get("secondChoice"));
				fc.utobj().sendKeys(driver, question_Library.firstScore, dataSet.get("firstScore"));
				fc.utobj().sendKeys(driver, question_Library.secondScore, dataSet.get("secondScore"));
				fc.utobj().clickElement(driver, pobj.addBtn);
				fc.utobj().switchFrameToDefault(driver);
				
				} catch (Exception e) {
					
				}
				return questionText;
			}

	public String addQuestionSingleSelectRadioButtonTypeWithAttachmentsAndTask(WebDriver driver,
			Map<String, String> config, Map<String, String> dataSet, String questionText ) throws Exception {

		String testCaseId = "TC_500_Questions_Visit";
		
		if (fc.utobj().validate(testCaseId)) {
			try {
				AdminFieldOpsQuestionLibraryPage pobj = new AdminFieldOpsQuestionLibraryPage(driver);
				fc.commonMethods().switch_cboxIframe_frameId(driver);
				
				fc.utobj().sendKeys(driver, pobj.questionTextArea, questionText);
				fc.utobj().sendKeys(driver, pobj.helpTextForUser, "Test Question");
				fc.utobj().clickElement(driver, pobj.questionMandantory);
				/* fc.utobj().clickElement(driver, pobj.allowPrivateComment); */
				fc.utobj().selectDropDown(driver, pobj.criticalLevel, dataSet.get("criticalLevel"));
				fc.utobj().clickElement(driver, pobj.nextBtn);

				// Response page
				fc.utobj().selectDropDown(driver, pobj.selectResponseType, dataSet.get("selectResponseType"));
				
				if (pobj.allowAttachments.isDisplayed()) {
					String allowAttachments = dataSet.get("allowAttachments");
					if (allowAttachments.equalsIgnoreCase("Yes")) {
						fc.utobj().clickElement(driver, pobj.allowAttachments);

					}
				}
				WebElement e1 = driver.findElement(By.xpath(
						".//td[contains(text () , 'Choices')]//following::td/input[@name='optionResponseName']/../../../tr[@id='row_1']/td/input[@name='complianceCheck']"));
				WebElement e2 = driver.findElement(By.xpath(
						".//td[contains(text () , 'Choices')]//following::td/input[@name='optionResponseName']/../../../tr[@id='row_2']/td/input[@name='complianceCheck']"));

				fc.utobj().clickElement(driver, pobj.scoreInclude);
				fc.utobj().clickElement(driver, pobj.notApplicableResponse);
				//fc.utobj().clickElement(driver, pobj.excludeNonResponse);

				fc.utobj().sendKeys(driver, pobj.firstChoice, dataSet.get("firstChoice"));
				fc.utobj().sendKeys(driver, pobj.firstScore, dataSet.get("firstScore"));
				fc.utobj().sendKeys(driver, pobj.secondChoice, dataSet.get("secondChoice"));
				fc.utobj().sendKeys(driver, pobj.secondScore, dataSet.get("secondScore"));

				String addNonComTask = "Yes";
				
				if (addNonComTask.equalsIgnoreCase("Yes")) {
					
					String nonComOption = dataSet.get("nonComOption");
					
					if (nonComOption.equalsIgnoreCase("responsechoice")) {
						
						fc.utobj().clickElement(driver, pobj.complianceByOption);
						
						fc.utobj().clickElement(driver, e2);
						
						fc.utobj().clickElement(driver, pobj.assignTaskonNoncompliance);

					} else if (nonComOption.equalsIgnoreCase("questionScore")) {

						fc.utobj().clickElement(driver, pobj.complianceByValue);
						
						String nonComScore = dataSet.get("nonComScore");
						
						fc.utobj().sendKeys(driver, pobj.complianceInput, nonComScore);
					}

				} 

				String assignAct = "New"; //dataSet.get("assignAction");

				if (assignAct.equalsIgnoreCase("New")) {
					
					fc.utobj().clickElement(driver, pobj.newAction);
					
					String taskSubject = questionText + "task";
					
					fc.utobj().sendKeys(driver, pobj.actionSubject, taskSubject);
					
					fc.utobj().selectDropDownByVisibleText(driver, pobj.actionPriority, "High");
					
					fc.utobj().sendKeys(driver, pobj.scheduleCompletionBox, "1");
					
					fc.utobj().sendKeys(driver, pobj.descriptionBox, taskSubject);

				} else {
					fc.utobj().clickElement(driver, pobj.actionFromLibrary);
					
					fc.utobj().selectDropDownByIndex(driver, pobj.actionFromLibrary, 2);

				}
				fc.utobj().clickElement(driver, pobj.saveBtn);
				
				fc.utobj().switchFrameToDefault(driver);
				
				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException(
					"Was not able to add Question Single Select Radio Btn Type:: Admin > Field Osp > Question Library");

		}
		return questionText;
	}

	/*public String addQuestionSingleSelectDropDownType(WebDriver driver, Map<String, String> config,
			Map<String, String> dataSet) throws Exception {

		String testCaseId = "TC_Add_Question_Single_Select_DropDown";
		String questionText = null;

		if (fc.utobj().validate(testCaseId)) {
			try {
				AdminFieldOpsQuestionLibraryPage pobj = new AdminFieldOpsQuestionLibraryPage(driver);
				fc.commonMethods().switch_cboxIframe_frameId(driver);
				questionText = fc.utobj().generateTestData(dataSet.get("questionText"));
				fc.utobj().sendKeys(driver, pobj.questionTextArea, questionText);
				fc.utobj().sendKeys(driver, pobj.helpTextForUser, dataSet.get("helpForUserText"));
				fc.utobj().clickElement(driver, pobj.questionMandantory);
				 fc.utobj().clickElement(driver, pobj.allowPrivateComment); 
				fc.utobj().selectDropDown(driver, pobj.criticalLevel, dataSet.get("criticalLevel"));
				fc.utobj().clickElement(driver, pobj.nextBtn);
				;

				fc.utobj().selectDropDown(driver, pobj.selectResponseType,
						dataSet.get("responseTypeQuestionSingleSelectDrop"));
				fc.utobj().clickElement(driver, pobj.allowUserComments);
				fc.utobj().clickElement(driver, pobj.scoreInclude);
				fc.utobj().clickElement(driver, pobj.excludeNonResponse);
				fc.utobj().clickElement(driver, pobj.notApplicableResponse);
				fc.utobj().clickElement(driver, pobj.complianceNotApplicable);
				fc.utobj().sendKeys(driver, pobj.firstChoice, dataSet.get("firstChoice"));
				fc.utobj().sendKeys(driver, pobj.secondChoice, dataSet.get("secondChoice"));
				fc.utobj().sendKeys(driver, pobj.firstScore, dataSet.get("firstScore"));
				fc.utobj().sendKeys(driver, pobj.secondScore, dataSet.get("secondScore"));
				fc.utobj().clickElement(driver, pobj.saveBtn);
				fc.utobj().switchFrameToDefault(driver);
				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException("Was not able to add Question Single Select DropDown Type");
		}
		return questionText;
	}
*/
	
	public String addQuestionRatingType(WebDriver driver, Map<String, String> config, Map<String, String> dataSet, String questionText)
			throws Exception {

		String testCaseId = "TC_Add_Question_Rating_Type";

		if (fc.utobj().validate(testCaseId)) {
			try {
				AdminFieldOpsQuestionLibraryPage pobj = new AdminFieldOpsQuestionLibraryPage(driver);
				fc.commonMethods().switch_cboxIframe_frameId(driver);
				
				fc.utobj().sendKeys(driver, pobj.questionTextArea, questionText);
				fc.utobj().sendKeys(driver, pobj.helpTextForUser, questionText);
				fc.utobj().clickElement(driver, pobj.questionMandantory);
				
				fc.utobj().selectDropDown(driver, pobj.criticalLevel, "Critical");
				fc.utobj().clickElement(driver, pobj.nextBtn);
				fc.utobj().selectDropDown(driver, pobj.selectResponseType, "Rating");
				fc.utobj().clickElement(driver, pobj.scoreInclude);
				fc.utobj().clickElement(driver, pobj.excludeNonResponse);
				fc.utobj().clickElement(driver, pobj.notApplicableResponse);
				fc.utobj().clickElement(driver, pobj.complianceNotApplicable);
				fc.utobj().sendKeys(driver, pobj.minRange, "0");
				fc.utobj().sendKeys(driver, pobj.maxRange, "10");
				fc.utobj().clickElement(driver, pobj.saveBtn);
				fc.utobj().switchFrameToDefault(driver);
				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException(
					"Was not able to add Question Rating Type :: Admin > Field Ops > Question Library");

		}
		return questionText;
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}
