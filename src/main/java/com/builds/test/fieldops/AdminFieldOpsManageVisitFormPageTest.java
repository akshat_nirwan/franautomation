package com.builds.test.fieldops;

import java.util.List;
import java.util.Map;
import java.util.Set;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import com.builds.uimaps.fieldops.AdminFieldOpsManageVisitFormCustomizeVisitFormPage;
import com.builds.uimaps.fieldops.AdminFieldOpsManageVisitFormPage;
import com.builds.uimaps.fieldops.AdminFieldOpsQuestionLibraryPage;
import com.builds.utilities.FranconnectUtil;
import com.builds.utilities.TestCase;

public class AdminFieldOpsManageVisitFormPageTest {
	FranconnectUtil fc = new FranconnectUtil();

	@Test(groups = { "fieldops", "fieldopsTest" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Add , Modify And Copy Private Visit Form", testCaseId = "TC_01_Add_Visit_Form")
	private void addVisitForm() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("fieldops", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			AdminFieldOpsManageVisitFormPage pobj = new AdminFieldOpsManageVisitFormPage(driver);

			fc.utobj().printTestStep("Navigate To Admin > FieldOps > Manage Visit Form Page");
			fc.utobj().printTestStep("Add Private Visit Form");

			fc.fieldops().fieldops_common().adminFieldOpsManageVisitForm(driver);
			fc.utobj().clickElement(driver, pobj.addVisitForm);
			String visitFormName = fc.utobj().generateTestData(dataSet.get("visitFormName"));
			fc.utobj().sendKeys(driver, pobj.visitFormName, visitFormName);
			fc.utobj().sendKeys(driver, pobj.description, dataSet.get("description"));
			fc.utobj().selectDropDown(driver, pobj.selectFrequency, dataSet.get("frequency"));

			if (dataSet.get("isPrivate").equalsIgnoreCase("Yes")) {
				fc.utobj().clickElement(driver, pobj.isPrivate);
			}

			fc.utobj().selectDropDown(driver, pobj.type, dataSet.get("type"));
			if (dataSet.get("layout").equalsIgnoreCase("List")) {
				fc.utobj().clickElement(driver, pobj.listLayout);
			}
			if (dataSet.get("showResponseScore").equalsIgnoreCase("Yes")) {
				fc.utobj().clickElement(driver, pobj.showResponseScore);
			}
			fc.utobj().clickElement(driver, pobj.saveAndNextBtn);
			fc.utobj().clickElement(driver, pobj.finishBtn);

			fc.utobj().printTestStep("Verify Add Visit Form");
			boolean isTextPresent = fc.utobj().assertPageSource(driver, visitFormName);
			if (isTextPresent == false) {
				fc.utobj().throwsException("was not able to add a Visit Form!!");
			}

			fc.utobj().printTestStep("Modify Visit Form");
			fc.utobj().actionImgOption(driver, visitFormName, "Modify");
			String visitFormNameM = fc.utobj().generateTestData(dataSet.get("visitFormName"));
			fc.utobj().sendKeys(driver, pobj.visitFormName, visitFormNameM);
			fc.utobj().sendKeys(driver, pobj.description, dataSet.get("description"));

			if (dataSet.get("showResponseScoreM").equalsIgnoreCase("Yes")) {
				fc.utobj().clickElement(driver, pobj.showResponseScore);
			}
			fc.utobj().clickElement(driver, pobj.saveAndNextBtn);
			fc.utobj().clickElement(driver, pobj.finishBtn);

			fc.utobj().printTestStep("Verify The Modify Visit Form");
			boolean isVMPresent = fc.utobj().assertPageSource(driver, visitFormNameM);
			if (isVMPresent == false) {
				fc.utobj().throwsException("was not able to modify a Visit Form!!");
			}

			fc.utobj().printTestStep("Copy Visit Form");
			fc.utobj().actionImgOption(driver, visitFormNameM, "Copy");
			fc.utobj().acceptAlertBox(driver);

			String visitFormNameCopy = fc.utobj().generateTestData(dataSet.get("visitFormName"));
			fc.utobj().sendKeys(driver, pobj.visitFormName, visitFormNameCopy);
			fc.utobj().sendKeys(driver, pobj.description, dataSet.get("description"));
			fc.utobj().clickElement(driver, pobj.saveAndNextBtn);
			fc.utobj().clickElement(driver, pobj.finishBtn);

			fc.utobj().printTestStep("Verify Copy Visit Form");
			boolean isVCPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () , '" + visitFormNameCopy + "')]");

			if (isVCPresent == false) {
				fc.utobj().throwsException("was not able to Copy a Visit Form!!");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	public String addVisitFormPrivate(WebDriver driver, Map<String, String> dataSet) throws Exception {

		String testCaseId = "TC_Add_Visit_Form_Private";
		String visitFormName = null;

		if (fc.utobj().validate(testCaseId)) {
			try {

				AdminFieldOpsManageVisitFormPage pobj = new AdminFieldOpsManageVisitFormPage(driver);
				fc.fieldops().fieldops_common().adminFieldOpsManageVisitForm(driver);

				fc.utobj().clickElement(driver, pobj.addVisitForm);
				visitFormName = fc.utobj().generateTestData(dataSet.get("visitFormName"));
				fc.utobj().sendKeys(driver, pobj.visitFormName, visitFormName);
				fc.utobj().sendKeys(driver, pobj.description, dataSet.get("description"));
				fc.utobj().selectDropDown(driver, pobj.selectFrequency, dataSet.get("frequency"));

				if (!fc.utobj().isSelected(driver,pobj.isPrivate)) {
					fc.utobj().clickElement(driver, pobj.isPrivate);
				}

				fc.utobj().selectDropDown(driver, pobj.type, dataSet.get("type"));
				if (dataSet.get("layout").equalsIgnoreCase("List")) {
					fc.utobj().clickElement(driver, pobj.listLayout);
				}

				fc.utobj().clickElement(driver, pobj.saveAndNextBtn);

				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException(
					"was not able to add Visit Form Private :: Admin > Field Ops > Manage Visit Form");

		}
		return visitFormName;
	}

	public String addVisitFormPrivateNo(WebDriver driver, Map<String, String> config, Map<String, String> dataSet)
			throws Exception {

		String testCaseId = "TC_Add_Visit_Form_Private_No";
		String visitFormName = null;

		if (fc.utobj().validate(testCaseId)) {
			try {
				AdminFieldOpsManageVisitFormPage pobj = new AdminFieldOpsManageVisitFormPage(driver);
				fc.fieldops().fieldops_common().adminFieldOpsManageVisitForm(driver);
				fc.utobj().clickElement(driver, pobj.addVisitForm);
				visitFormName = fc.utobj().generateTestData(dataSet.get("visitFormName"));
				fc.utobj().sendKeys(driver, pobj.visitFormName, visitFormName);
				fc.utobj().sendKeys(driver, pobj.description, dataSet.get("description"));
				fc.utobj().selectDropDown(driver, pobj.selectFrequency, dataSet.get("frequency"));
				if (fc.utobj().isSelected(driver,pobj.isPrivate)) {
					fc.utobj().clickElement(driver, pobj.isPrivate);
				}
				fc.utobj().selectDropDown(driver, pobj.type, dataSet.get("type"));
				if (dataSet.get("layout").equalsIgnoreCase("List")) {
					fc.utobj().clickElement(driver, pobj.listLayout);
				} else if (dataSet.get("layout").equalsIgnoreCase("Table")) {
					fc.utobj().clickElement(driver, pobj.tableLayout);
				}
				if (dataSet.get("showResponseScore").equalsIgnoreCase("Yes")) {
					fc.utobj().clickElement(driver, pobj.showResponseScore);
				}
				fc.utobj().clickElement(driver, pobj.saveAndNextBtn);

				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException(
					"was not able to add visit form Private No :: Admin > Field Ops > Manage Visit Form");

		}
		return visitFormName;
	}

	@Test(groups = { "fieldops", "fieldopsTest" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Preview Visit Form.", testCaseId = "TC_04_Preview_Visit_Form")
	private void previewVisitForm() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("fieldops", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			AdminFieldOpsManageVisitFormPage pobj = new AdminFieldOpsManageVisitFormPage(driver);

			String parentWindowHandle = driver.getWindowHandle();

			fc.utobj().printTestStep("Navigate To Admin > Fieldops > Manage Visit Form");
			fc.utobj().printTestStep("Add Private Visit Form");

			String visitFormName = addVisitFormPrivate(driver, dataSet);
			fc.utobj().clickElement(driver, pobj.finishBtn);
			fc.utobj().printTestStep("Preview Visit Form");

			fc.utobj().actionImgOption(driver, visitFormName, "Preview");
			fc.utobj().printTestStep("Verify Preview Visit Form");

			Set<String> allWindowHandles = driver.getWindowHandles();
			System.out.println(allWindowHandles.size());
			for (String currentWindowHandle : allWindowHandles) {
				if (!currentWindowHandle.equals(parentWindowHandle)) {
					driver.switchTo().window(currentWindowHandle);

					try {

						fc.utobj().getElementByXpath(driver, ".//strong[.=" + "" + visitFormName + "" + "]")
								.isEnabled();

						boolean isTextPresent = fc.utobj().assertPageSource(driver, visitFormName);
						if (isTextPresent == false) {
							fc.utobj().throwsException("was not able to Preview a Visit Form!!");
						}

						driver.close();

					} catch (Exception e) {
						driver.close();
					}

					driver.switchTo().window(parentWindowHandle);
				}
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}

	}

	@Test(groups = { "fieldops", "fieldopsTest" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The View Logs Of Visit Form.", testCaseId = "TC_05_View_Logs")
	private void viewLogs() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("fieldops", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			AdminFieldOpsManageVisitFormPage pobj = new AdminFieldOpsManageVisitFormPage(driver);

			fc.utobj().printTestStep("Navigate To Admin > Fieldops > Manage Visit Form");
			fc.utobj().printTestStep("Add Private Visit Form");

			String visitFormName = addVisitFormPrivate(driver, dataSet);
			fc.utobj().clickElement(driver, pobj.finishBtn);

			fc.utobj().printTestStep("View Logs");
			fc.utobj().actionImgOption(driver, visitFormName, "View Logs");

			fc.utobj().printTestStep("Verify View Logs");
			boolean isTextPresent = fc.utobj().assertPageSource(driver, visitFormName);
			if (isTextPresent == false) {
				fc.utobj().throwsException("was not able to View Log A Visit Form!!");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "fieldops", "fieldopsTest" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Deactivate , Activate And Delete Visit Form", testCaseId = "TC_07_Deactivate_Visit_Form")
	private void deactivateVisitForm() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("fieldops", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			AdminFieldOpsManageVisitFormPage pobj = new AdminFieldOpsManageVisitFormPage(driver);

			fc.utobj().printTestStep("Navigate To Admin > Fieldops > Manage Visit Form");
			fc.utobj().printTestStep("Add Private Visit Form");

			String visitFormName = addVisitFormPrivate(driver, dataSet);
			fc.utobj().clickElement(driver, pobj.finishBtn);

			fc.utobj().printTestStep("Deactivate Visit Form");

			fc.utobj().actionImgOption(driver, visitFormName, "Deactivate");
			fc.utobj().acceptAlertBox(driver);
			fc.utobj().printTestStep("Verify Deactivate Visit Form");

			String text = fc.utobj()
					.getElementByXpath(driver,
							".//*[.=" + "'" + visitFormName + "'" + "]/../../td/div[@id='menuBar']/layer")
					.getAttribute("id");
			String alterText = text.replace("Actions_dynamicmenu", "");
			alterText = alterText.replace("Bar", "");
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//*[.=" + "'" + visitFormName + "'" + "]/../../td/div[@id='menuBar']/layer/a/img"));

			String resultText = fc.utobj()
					.getElementByXpath(driver,
							".//*[@id='Actions_dynamicmenu" + alterText + "Menu']/span[contains(text () , 'Activate')]")
					.getText().trim();

			if (!resultText.equalsIgnoreCase("Activate")) {
				fc.utobj().throwsException("was not able to De-Activate a Visit Form!!");
			}

			fc.utobj().printTestStep("Activate Visit Form");

			fc.utobj().actionImgOption(driver, visitFormName, "Activate");
			fc.utobj().acceptAlertBox(driver);

			String text1 = fc.utobj()
					.getElementByXpath(driver,
							".//*[.=" + "'" + visitFormName + "'" + "]/../../td/div[@id='menuBar']/layer")
					.getAttribute("id");
			String alterText1 = text1.replace("Actions_dynamicmenu", "");
			alterText1 = alterText1.replace("Bar", "");
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//*[.=" + "'" + visitFormName + "'" + "]/../../td/div[@id='menuBar']/layer/a/img"));

			String resultText1 = fc.utobj().getElementByXpath(driver,
					".//*[@id='Actions_dynamicmenu" + alterText + "Menu']/span[contains(text () , 'Deactivate')]")
					.getText().trim();

			fc.utobj().printTestStep("Verify Activate Visit Form");
			if (!resultText1.equalsIgnoreCase("Deactivate")) {
				fc.utobj().throwsException("was not able to Activate a Visit Form!!");
			}

			fc.utobj().printTestStep("Delete Visit Form");

			fc.utobj().actionImgOption(driver, visitFormName, "Delete");
			fc.utobj().acceptAlertBox(driver);
			fc.utobj().printTestStep("Verify Delete Visit Form");
			boolean isTextPresent = fc.utobj().assertPageSource(driver, visitFormName);

			if (isTextPresent == true) {
				fc.utobj().throwsException("was not able to Delete a Visit Form!!");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "fieldops", "fieldopsTest" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify the Add Visit Form with Tab View Option", testCaseId = "TC_18_Add_Visit_Form_Tab_View")
	private void addVisitFormTabView() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("fieldops", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			AdminFieldOpsManageVisitFormPage pobj = new AdminFieldOpsManageVisitFormPage(driver);

			fc.utobj().printTestStep("Navigate To Admin > FieldOps > Manage Visit Form");
			fc.utobj().printTestStep("Add Visit Form Tab View");
			fc.fieldops().fieldops_common().adminFieldOpsManageVisitForm(driver);
			fc.utobj().clickElement(driver, pobj.addVisitForm);
			String visitFormName = fc.utobj().generateTestData(dataSet.get("visitFormName"));
			fc.utobj().sendKeys(driver, pobj.visitFormName, visitFormName);
			fc.utobj().sendKeys(driver, pobj.description, dataSet.get("description"));
			fc.utobj().selectDropDown(driver, pobj.selectFrequency, dataSet.get("frequency"));

			if (dataSet.get("isPrivate").equalsIgnoreCase("Yes")) {
				fc.utobj().clickElement(driver, pobj.isPrivate);
			}

			fc.utobj().selectDropDown(driver, pobj.type, dataSet.get("type"));
			if (dataSet.get("layout").equalsIgnoreCase("Table")) {
				fc.utobj().clickElement(driver, pobj.tableLayout);
			}

			if (dataSet.get("showResponseScore").equalsIgnoreCase("Yes")) {
				fc.utobj().clickElement(driver, pobj.showResponseScore);
			}

			if (dataSet.get("allowComments").equalsIgnoreCase("Yes")) {
				List<WebElement> list = pobj.allowComments;

				if (fc.utobj().isSelected(driver,list.get(1))) {
					fc.utobj().clickElement(driver, list.get(0));
				}
			}

			fc.utobj().sendKeys(driver, pobj.response, dataSet.get("response"));
			fc.utobj().sendKeys(driver, pobj.comments, dataSet.get("comments"));

			fc.utobj().clickElement(driver, pobj.saveAndNextBtn);
			fc.utobj().clickElement(driver, pobj.finishBtn);

			fc.utobj().printTestStep("Verify Visit Form With Tab View");
			boolean isTextPresent = fc.utobj().assertPageSource(driver, visitFormName);
			if (isTextPresent == false) {
				fc.utobj().throwsException("was not able to Add a Visit Form with Tab View!!");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "fieldops", "fieldopsTest" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify add question from library when we create new audit form.", testCaseId = "TC_115_Verify_Add_Question_From_Library_Visit_Form")
	private void verifyAddQuestionLibraryVisitForm() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("fieldops", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			AdminFieldOpsQuestionLibraryPageTest fQuestionPage = new AdminFieldOpsQuestionLibraryPageTest();

			fc.utobj().printTestStep("Navigate To Admin > FieldOps > Question Library Page");
			fc.utobj().printTestStep("Add Category");
			AdminFieldOpsQuestionLibraryPage pobj1 = new AdminFieldOpsQuestionLibraryPage(driver);
			String categoryName = fQuestionPage.addCategory(driver, dataSet);

			fc.utobj().printTestStep("Search Category");
			fc.utobj().selectDropDown(driver, pobj1.selectCategory, categoryName);

			fc.utobj().printTestStep("Add Question");
			AdminFieldOpsManageVisitFormCustomizeVisitFormPage pobj2 = new AdminFieldOpsManageVisitFormCustomizeVisitFormPage(
					driver);
			fc.utobj().clickElement(driver, pobj1.addQuestion);

			AdminFieldOpsManageVisitFormCustomizeVisitFormPageTest cVisitPage = new AdminFieldOpsManageVisitFormCustomizeVisitFormPageTest();
			String questionText = cVisitPage.addQuestion(driver, dataSet);

			fc.utobj().printTestStep("Navigate To Admin > Fieldops > Manage Visit Form");
			fc.utobj().printTestStep("Add Private Visit Form");
			addVisitFormPrivate(driver, dataSet);

			fc.utobj().printTestStep("Add Question From Library");
			fc.utobj().clickElement(driver, pobj2.addQuestion);

			fc.commonMethods().switch_cboxIframe_frameId(driver);

			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//tr/td/label/input[@id='qType'][@value='libQue']"));

			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//span[.='Search and Select Question']"));
			fc.utobj().sendKeys(driver,
					fc.utobj().getElementByXpath(driver, ".//input[@placeholder='Search Question']"), questionText);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//span[.='Check All']"));
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//li/a[@class='ui-multiselect-close']/span"));

			fc.utobj().clickElement(driver, pobj2.addBtnQstnLib);

			fc.utobj().printTestStep("Verify Add Question From Library In Visit Form");
			fc.utobj().switchFrameToDefault(driver);
			String alterText = fc.utobj().getElementByXpath(driver, ".//div[@id='ModifyFormDiv']/ul/li")
					.getAttribute("id");
			String isText = fc.utobj().getText(driver,
					fc.utobj().getElementByXpath(driver, ".//tr/td/label[@id=" + "'" + alterText + "_0']"));

			if (!isText.contains(questionText)) {
				fc.utobj().throwsException("Verify add question from library when we create new audit form");
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}

	}

	@Test(groups = { "fieldops", "fieldopsTest" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify N/A option should be selected for smart question in audit form.", testCaseId = "TC_116_Verify_Not_Applicable_Option_Smart_Question")
	private void verifyNotApplicableOptionSmartQuestion() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("fieldops", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			AdminFieldOpsManageVisitFormPage pobj = new AdminFieldOpsManageVisitFormPage(driver);

			fc.utobj().printTestStep("Navigate To Admin > FieldOps > Manage Visit Form");
			fc.utobj().printTestStep("Add Visit Form");
			fc.fieldops().fieldops_common().adminFieldOpsManageVisitForm(driver);
			fc.utobj().clickElement(driver, pobj.addVisitForm);
			String visitFormName = fc.utobj().generateTestData(dataSet.get("visitFormName"));
			fc.utobj().sendKeys(driver, pobj.visitFormName, visitFormName);
			fc.utobj().sendKeys(driver, pobj.description, dataSet.get("description"));
			fc.utobj().selectDropDown(driver, pobj.selectFrequency, dataSet.get("frequency"));

			if (dataSet.get("isPrivate").equalsIgnoreCase("Yes")) {
				fc.utobj().clickElement(driver, pobj.isPrivate);
			}

			fc.utobj().selectDropDown(driver, pobj.type, dataSet.get("type"));
			if (dataSet.get("layout").equalsIgnoreCase("List")) {
				fc.utobj().clickElement(driver, pobj.listLayout);
			}
			if (dataSet.get("showResponseScore").equalsIgnoreCase("Yes")) {
				fc.utobj().clickElement(driver, pobj.showResponseScore);
			}
			fc.utobj().clickElement(driver, pobj.saveAndNextBtn);

			fc.utobj().clickElement(driver, pobj.addQuestionBtn);

			AdminFieldOpsManageVisitFormCustomizeVisitFormPage pobj1 = new AdminFieldOpsManageVisitFormCustomizeVisitFormPage(
					driver);

			fc.commonMethods().switch_cboxIframe_frameId(driver);

			fc.utobj().printTestStep("Add Smart Question");

			if (dataSet.get("questionTypeNewOrLibrary").equalsIgnoreCase("New Question")) {
				List<WebElement> list = pobj1.addQuestionNewOrLibraray;
				if (fc.utobj().isSelected(driver,list.get(1))) {
					fc.utobj().clickElement(driver, list.get(0));
				} else {
					System.out.println("Do nothing");
				}
			}

			String questionText = fc.utobj().generateTestData(dataSet.get("questionText"));
			fc.utobj().sendKeys(driver, pobj1.questionTextArea, questionText);
			fc.utobj().sendKeys(driver, pobj1.helpTextForUser, dataSet.get("helpForUserText"));
			fc.utobj().clickElement(driver, pobj1.questionMandantory);
			fc.utobj().selectDropDown(driver, pobj1.criticalLevel, dataSet.get("criticalLevel"));
			fc.utobj().clickElement(driver, pobj1.nextBtn);

			fc.utobj().selectDropDownByVisibleTextTrimed(driver, pobj1.selectResponseType, "Smart Question");
			fc.utobj().clickElement(driver, pobj1.moduleTypeBtn);
			fc.utobj().sendKeys(driver, pobj1.moduleTypeSearch, dataSet.get("moduleType"));
			fc.utobj().clickElement(driver, pobj1.moduleTypeSelect);
			fc.utobj().clickElement(driver, pobj1.moduleFieldBtn);
			fc.utobj().sendKeys(driver, pobj1.moduleFieldSearch, dataSet.get("moduleFieldSelect"));
			fc.utobj().clickElement(driver, pobj1.moduleFieldSelect);

			fc.utobj().printTestStep("Verify Not Applicable Option");

			fc.utobj().isSelected(driver,pobj1.notApplicableResponse);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}

	}

	public String addVisitFormTabViewPrivateYes(WebDriver driver, Map<String, String> config,
			Map<String, String> dataSet) throws Exception {

		String testCaseId = "TC_Add_Visit_Form_Tab_View_Yes";
		String visitFormName = null;

		if (fc.utobj().validate(testCaseId)) {
			try {
				AdminFieldOpsManageVisitFormPage pobj = new AdminFieldOpsManageVisitFormPage(driver);
				fc.fieldops().fieldops_common().adminFieldOpsManageVisitForm(driver);
				fc.utobj().clickElement(driver, pobj.addVisitForm);
				visitFormName = fc.utobj().generateTestData(dataSet.get("visitFormName"));
				fc.utobj().sendKeys(driver, pobj.visitFormName, visitFormName);
				fc.utobj().sendKeys(driver, pobj.description, dataSet.get("description"));
				fc.utobj().selectDropDown(driver, pobj.selectFrequency, dataSet.get("frequency"));

				if (fc.utobj().isSelected(driver,pobj.isPrivate)) {
					// do notinh
				} else {
					fc.utobj().clickElement(driver, pobj.isPrivate);
				}

				fc.utobj().selectDropDown(driver, pobj.type, dataSet.get("type"));

				if (dataSet.get("layout").equalsIgnoreCase("List")) {
					fc.utobj().clickElement(driver, pobj.listLayout);

				} else if (dataSet.get("layout").equalsIgnoreCase("Table")) {
					fc.utobj().clickElement(driver, pobj.tableLayout);
					List<WebElement> list = pobj.allowComments;

					if (fc.utobj().isSelected(driver,list.get(1))) {
						fc.utobj().clickElement(driver, list.get(0));
					}

					fc.utobj().sendKeys(driver, pobj.response, dataSet.get("response"));
					fc.utobj().sendKeys(driver, pobj.comments, dataSet.get("comments"));
				}

				fc.utobj().clickElement(driver, pobj.showResponseScore);
				/*
				 * List<WebElement> list=pobj.allowComments;
				 * 
				 * if(fc.utobj().isSelected(driver,list.get(1))){ fc.utobj().clickElement(driver,
				 * list.get(0)); }
				 * 
				 * fc.utobj().sendKeys(driver, pobj.response,
				 * dataSet.get("response")); fc.utobj().sendKeys(driver,
				 * pobj.comments, dataSet.get("comments"));
				 */

				fc.utobj().clickElement(driver, pobj.saveAndNextBtn);

				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException(
					"Was not able to add Visit Form with Tab View Yes :: Admin > Field Ops > Manage Visit Form");

		}
		return visitFormName;
	}

	public String addVisitFormTabViewPrivateNo(WebDriver driver, Map<String, String> config,
			Map<String, String> dataSet) throws Exception {

		String testCaseId = "TC_Add_Visit_Form_TabView_No";
		String visitFormName = null;

		if (fc.utobj().validate(testCaseId)) {
			try {
				AdminFieldOpsManageVisitFormPage pobj = new AdminFieldOpsManageVisitFormPage(driver);
				fc.fieldops().fieldops_common().adminFieldOpsManageVisitForm(driver);
				fc.utobj().clickElement(driver, pobj.addVisitForm);
				visitFormName = fc.utobj().generateTestData(dataSet.get("visitFormName"));
				fc.utobj().sendKeys(driver, pobj.visitFormName, visitFormName);
				fc.utobj().sendKeys(driver, pobj.description, dataSet.get("description"));

				fc.utobj().selectDropDown(driver, pobj.selectFrequency, dataSet.get("frequency"));

				if (fc.utobj().isSelected(driver,pobj.isPrivate)) {
					fc.utobj().clickElement(driver, pobj.isPrivate);
				} else {
				}

				fc.utobj().selectDropDown(driver, pobj.type, dataSet.get("type"));
				fc.utobj().clickElement(driver, pobj.tableLayout);

				fc.utobj().clickElement(driver, pobj.showResponseScore);
				List<WebElement> list = pobj.allowComments;

				if (fc.utobj().isSelected(driver,list.get(1))) {
					fc.utobj().clickElement(driver, list.get(0));
				}
				fc.utobj().sendKeys(driver, pobj.response, dataSet.get("response"));
				fc.utobj().sendKeys(driver, pobj.comments, dataSet.get("comments"));

				fc.utobj().clickElement(driver, pobj.saveAndNextBtn);

				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException(
					"Was not able to add Visit Form with Tab View No :: Admin > Field Ops > Manage Visit Form");

		}
		return visitFormName;
	}
}
