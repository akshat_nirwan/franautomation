package com.builds.test.fieldops;

import java.util.List;
import java.util.Map;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import com.builds.test.admin.AdminFranchiseLocationAddFranchiseLocationPageTest;
import com.builds.uimaps.fieldops.AdminFieldOpsQATabIntegrationPage;
import com.builds.utilities.FranconnectUtil;
import com.builds.utilities.TestCase;

public class AdminFieldOpsQATabIntegrationPageTest {

	FranconnectUtil fc = new FranconnectUtil();

	@Test(groups = { "fieldops", "fieldops0808" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify QA Tab integration with option Yes", testCaseId = "TC_34_QA_Tab_Integration_Yes")
	private void qaTabIntegrationYes() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("fieldops", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			AdminFieldOpsQATabIntegrationPage pobj = new AdminFieldOpsQATabIntegrationPage(driver);

			fc.utobj().printTestStep("Navigate To Admin > Field Ops > QA Tab Integration");
			fc.fieldops().fieldops_common().adminFieldOpsQATabIntegration(driver);
			fc.utobj().printTestStep("QA Tab Integration With Option Yes");

			List<WebElement> list = pobj.enableQATab;
			if (dataSet.get("qaTabIntegrationOption").equalsIgnoreCase("Yes")) {
				if (fc.utobj().isSelected(driver,list.get(0))) {
					// do nothing
				} else {
					fc.utobj().clickElement(driver, list.get(0));
				}
			}

			fc.utobj().clickElement(driver, pobj.submit);

			fc.utobj().printTestStep("Navigate To Admin > Franchise Location > Add Franchise Location");
			fc.utobj().printTestStep("Add Franchise Location");

			AdminFranchiseLocationAddFranchiseLocationPageTest franchiseeLoc = new AdminFranchiseLocationAddFranchiseLocationPageTest();
			String franchiseId = fc.utobj().generateTestData(dataSet.get("franchiseId"));
			String regionName = fc.utobj().generateTestData(dataSet.get("regionName"));
			String storeType = fc.utobj().generateTestData(dataSet.get("storeType"));
			String franchiseeId = franchiseeLoc.addFranchiseLocation_All(driver, franchiseId, regionName, storeType);

			fc.utobj().printTestStep("Navigate To InfoMgr > Franchise Page");
			fc.infomgr().infomgr_common().InfoMgrFranchisees(driver);

			fc.utobj().sendKeys(driver, pobj.searchFranchisee, franchiseeId);
			fc.utobj().clickElement(driver, pobj.searchBoxBtn);
			fc.utobj().clickLink(driver, franchiseeId);
			
			fc.utobj().printTestStep("Navigate To QA History Tab");
			fc.utobj().clickElement(driver, pobj.qaHistory);

			fc.utobj().printTestStep("Verify Create Visit Link At Info Mgr > Franchise > QA HIstory");
			String text = fc.utobj().getElement(driver, pobj.createVisitText).getText().trim();
			if (text.equalsIgnoreCase("Create Visit")) {
				// do nothing
				System.out.println(text);
			} else {
				fc.utobj().throwsException(
						"Was not able to verify Qa Tab Integration with Yes Option :: Admin > FieldOpsc> QA Tab Integration");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	public void qaTabIntegrationYes(WebDriver driver, Map<String, String> config) throws Exception {

		String testCaseId = "TC_Qa_Tab_Integration_Yes";

		if (fc.utobj().validate(testCaseId)) {
			try {
				AdminFieldOpsQATabIntegrationPage pobj = new AdminFieldOpsQATabIntegrationPage(driver);
				fc.fieldops().fieldops_common().adminFieldOpsQATabIntegration(driver);
				List<WebElement> list = pobj.enableQATab;
				if (fc.utobj().isSelected(driver,list.get(0))) {
					// do nothing
				} else {
					fc.utobj().clickElement(driver, list.get(0));
				}
				fc.utobj().clickElement(driver, pobj.submit);
				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException("Was not able to configure QA Tab Integration Yes");

		}
	}
}
