package com.builds.test.fieldops;

import java.util.Map;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Test;

import com.builds.uimaps.fieldops.AdminFieldOpsQuestionLibraryPage;
import com.builds.utilities.FranconnectUtil;
import com.builds.utilities.TestCase;

public class AdminFieldOpsQuestionLibraryPageTest {
	FranconnectUtil fc = new FranconnectUtil();

	@Test(groups = { "fieldops", "fieldopsTest" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify the Add , Modify And Delete Category in question Library", testCaseId = "TC_23_Add_Category")
	private void addCategory() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("fieldops", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			AdminFieldOpsQuestionLibraryPage pobj = new AdminFieldOpsQuestionLibraryPage(driver);

			fc.utobj().printTestStep("Navigate To Admin > Field Ops > Question Library");
			fc.utobj().printTestStep("Add Category");
			String categoryName = addCategory(driver, dataSet);

			fc.utobj().printTestStep("Verify Add Category");
			boolean isTextPresent = fc.utobj().assertPageSource(driver, categoryName);
			if (isTextPresent == false) {
				fc.utobj().throwsException("was not able to add a Category!!");
			}

			fc.utobj().printTestStep("Modify Category");
			fc.utobj().selectDropDown(driver, pobj.selectCategory, categoryName);

			fc.utobj().clickElement(driver, pobj.modifyCategory);

			fc.commonMethods().switch_cboxIframe_frameId(driver);
			String categoryNameM = fc.utobj().generateTestData(dataSet.get("categoryName"));
			fc.utobj().sendKeys(driver, pobj.categoryName, categoryNameM);
			fc.utobj().clickElement(driver, pobj.saveBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Verify The Modify Category");
			boolean isCMPresent = fc.utobj().assertPageSource(driver, categoryNameM);
			if (isCMPresent == false) {

				fc.utobj().throwsException("was not able to modify a Category!!");
			}

			fc.utobj().printTestStep("Delete Category");
			fc.utobj().selectDropDown(driver, pobj.selectCategory, categoryNameM);

			fc.utobj().clickElement(driver, pobj.deleteCategory);
			fc.utobj().acceptAlertBox(driver);

			fc.utobj().printTestStep("Verify The Deleted Category");
			boolean isDCPresent = fc.utobj().assertPageSource(driver, categoryName);
			if (isDCPresent == true) {
				fc.utobj().throwsException("was not able to delete a Category from Question Library");
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);

		}
	}

	public String addCategory(WebDriver driver, Map<String, String> dataSet) throws Exception {

		String testCaseId = "TC_Add_Category_Question_Library_FieldOps";
		String categoryName = null;
		if (fc.utobj().validate(testCaseId)) {
			try {
				AdminFieldOpsQuestionLibraryPage pobj = new AdminFieldOpsQuestionLibraryPage(driver);
				fc.fieldops().fieldops_common().adminFieldOpsQuestionLibrary(driver);

				fc.utobj().clickElement(driver, pobj.addCategory);
				fc.commonMethods().switch_cboxIframe_frameId(driver);
				categoryName = fc.utobj().generateTestData(dataSet.get("categoryName"));
				fc.utobj().sendKeys(driver, pobj.categoryName, categoryName);
				fc.utobj().clickElement(driver, pobj.saveBtn);
				fc.utobj().switchFrameToDefault(driver);

				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException(
					"was not able to add Category In Question Library :: Admin > Field Ops > Question Library");

		}
		return categoryName;
	}

	@Test(groups = { "fieldops", "fieldopsTest" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Add , Modify And Delete A Question From category", testCaseId = "TC_26_Add_Question_InCategory")
	private void addQuestionInCategory() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("fieldops", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {

			driver = fc.loginpage().login(driver);
			AdminFieldOpsQuestionLibraryPage pobj = new AdminFieldOpsQuestionLibraryPage(driver);

			fc.utobj().printTestStep("Navigate To Admin > Field Ops > Question Library");
			fc.utobj().printTestStep("Add Category");
			String categoryName = addCategory(driver, dataSet);

			fc.utobj().printTestStep("Add Question in category");
			fc.utobj().clickElement(driver, pobj.addQuestion);

			AdminFieldOpsManageVisitFormCustomizeVisitFormPageTest cVisitPage = new AdminFieldOpsManageVisitFormCustomizeVisitFormPageTest();
			String questionText = cVisitPage.addQuestionInCategory(driver, dataSet, categoryName);

			fc.utobj().printTestStep("Verify Question In Category");
			fc.utobj().selectDropDown(driver, pobj.selectCategory, categoryName);

			boolean isQuestionTextPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () , '" + questionText + "')]");
			if (isQuestionTextPresent == false) {
				fc.utobj().throwsException("was not able to verify question in Category");
			}

			boolean isQuestionTypeTextPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () , '" + questionText + "')]/ancestor::tr/td[contains(text () , '"
							+ dataSet.get("responseTypeQuestion") + "')]");
			if (isQuestionTypeTextPresent == false) {
				fc.utobj().throwsException("was not able to verify Question Type in Category");
			}

			fc.utobj().printTestStep("Modify Question In category");
			fc.utobj().actionImgOption(driver, questionText, "Modify");

			String questionTextM = cVisitPage.modifyQuestion(driver, dataSet);

			fc.utobj().printTestStep("Verify The Modify Question In Category");

			boolean isQuestionMTextPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () , '" + questionTextM + "')]");
			if (isQuestionMTextPresent == false) {
				fc.utobj().throwsException("was not able to verify question in Category");
			}

			boolean isQuestionTypeMTextPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () , '" + questionTextM + "')]/ancestor::tr/td[contains(text () , '"
							+ dataSet.get("responseTypeQuestion") + "')]");
			if (isQuestionTypeMTextPresent == false) {
				fc.utobj().throwsException("was not able to verify Question Type in Category");
			}

			fc.utobj().printTestStep("Delete Question In Category");
			fc.utobj().actionImgOption(driver, questionTextM, "Delete");
			fc.utobj().acceptAlertBox(driver);

			fc.utobj().printTestStep("Verify The Delete Question In Category");

			boolean isDeletedPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//td[contains(text () , 'No records found.')]");
			if (isDeletedPresent == false) {
				fc.utobj().throwsException("was not able to delete a question From category");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "fieldops", "fieldopsTest" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Move Question From One Category To Another", testCaseId = "TC_30_Move_Question")
	private void moveQuestion() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("fieldops", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {

			driver = fc.loginpage().login(driver);
			AdminFieldOpsQuestionLibraryPage pobj = new AdminFieldOpsQuestionLibraryPage(driver);

			fc.utobj().printTestStep("Navigate To Admin > Field Ops > Question Library");
			fc.utobj().printTestStep("Add Category");
			String categoryName1 = addCategory(driver, dataSet);

			fc.utobj().printTestStep("Add Another Category");
			String categoryName = addCategory(driver, dataSet);

			fc.utobj().printTestStep("Select Category");
			fc.utobj().selectDropDown(driver, pobj.selectCategory, categoryName);

			fc.utobj().printTestStep("Add Question In First Category");
			fc.utobj().clickElement(driver, pobj.addQuestion);
			AdminFieldOpsManageVisitFormCustomizeVisitFormPageTest cVisitPage = new AdminFieldOpsManageVisitFormCustomizeVisitFormPageTest();
			String questionText = cVisitPage.addQuestion(driver, dataSet);

			fc.utobj().printTestStep("Move Question From First Category To Another Category");

			fc.utobj().actionImgOption(driver, questionText, "Move To");
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().selectDropDown(driver, pobj.categoryCombo, categoryName1);
			fc.utobj().clickElement(driver, pobj.saveBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Verify The Move Question from One Category To Another Category");
			boolean isMovedQuestionPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () , 'No records found.')]");
			if (isMovedQuestionPresent == false) {
				fc.utobj().throwsException("was not able to verify Move Question From One Category To Another");
			}

			fc.utobj().printTestStep("Verify The Moved Question In Another Category");
			fc.utobj().selectDropDown(driver, pobj.selectCategory, categoryName1);

			boolean isQuestionMTextPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () , '" + questionText + "')]");
			if (isQuestionMTextPresent == false) {
				fc.utobj().throwsException("was not able to verify question in Category");
			}

			boolean isQuestionTypeMTextPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () , '" + questionText + "')]/ancestor::tr/td[contains(text () , '"
							+ dataSet.get("responseTypeQuestion") + "')]");
			if (isQuestionTypeMTextPresent == false) {
				fc.utobj().throwsException("was not able to verify Question Type in Category");
			}

			fc.utobj().printTestStep("Verify Question And Count Redirection");
			fc.utobj().selectDropDown(driver, pobj.selectCategory, "All");

			fc.utobj().showAll(driver);

			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//td[contains(text () , '" + categoryName1 + "')]/ancestor::tr/td/a[contains(text () , '1')]"));

			boolean isQuestionMTextPresent1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () , '" + questionText + "')]");
			if (isQuestionMTextPresent1 == false) {
				fc.utobj().throwsException("was not able to verify question in Category After Click Count");
			}

			boolean isQuestionTypeMTextPresent1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () , '" + questionText + "')]/ancestor::tr/td[contains(text () , '"
							+ dataSet.get("responseTypeQuestion") + "')]");
			if (isQuestionTypeMTextPresent1 == false) {
				fc.utobj().throwsException("was not able to verify Question Type in Category Aftre Click Count");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "fieldops", "fieldopsTest" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify Deletion of All a question from category of Question Library.", testCaseId = "TC_30_Delete_AllQuestions")
	private void deleteAllQuestions() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("fieldops", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		boolean isTextPresent = false;

		try {

			driver = fc.loginpage().login(driver);
			AdminFieldOpsQuestionLibraryPage pobj = new AdminFieldOpsQuestionLibraryPage(driver);

			fc.utobj().printTestStep("Navigate To Admin > Field Ops > Question Library");
			fc.utobj().printTestStep("Add Category");
			String categoryName = addCategory(driver, dataSet);

			fc.utobj().selectDropDown(driver, pobj.selectCategory, categoryName);

			fc.utobj().printTestStep("Add Question Date Type");
			fc.utobj().clickElement(driver, pobj.addQuestion);
			AdminFieldOpsManageVisitFormCustomizeVisitFormPageTest cVisitPage = new AdminFieldOpsManageVisitFormCustomizeVisitFormPageTest();
			String Date = cVisitPage.addQuestion(driver, dataSet);

			fc.utobj().printTestStep("Add Question Rating Type");

			fc.utobj().clickElement(driver, pobj.addQuestion);
			String rating = addQuestionRatingType(driver, dataSet);

			fc.utobj().printTestStep("Add Question Single Radio Button Type");

			fc.utobj().clickElement(driver, pobj.addQuestion);
			String singleSelectRadio = addQuestionSingleSelectRadioButtonType(driver, dataSet);

			fc.utobj().printTestStep("Add Question Single Select Drop Down Type");

			fc.utobj().clickElement(driver, pobj.addQuestion);
			String singleSelectDrop = addQuestionSingleSelectDropDownType(driver, dataSet);

			fc.utobj().printTestStep("Delete All Question");
			fc.utobj().clickElement(driver, pobj.deleteAllQuestions);
			fc.utobj().acceptAlertBox(driver);

			fc.utobj().printTestStep("Verify Delete All Question");
			isTextPresent = fc.utobj().assertPageSource(driver, Date);
			if (isTextPresent == true) {
				fc.utobj().throwsException("Was not able to delete Date Type question");
			}

			isTextPresent = fc.utobj().assertPageSource(driver, rating);
			if (isTextPresent == true) {
				fc.utobj().throwsException("was not able to delete Rating Type question");
			}

			isTextPresent = fc.utobj().assertPageSource(driver, singleSelectRadio);
			if (isTextPresent == true) {
				fc.utobj().throwsException("was not able to delete Single select radio button Type question");
			}

			isTextPresent = fc.utobj().assertPageSource(driver, singleSelectDrop);
			if (isTextPresent == true) {
				fc.utobj().throwsException("was not able to delete Single select Drop Down Type question");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}

	}

	public String addQuestionRatingType(WebDriver driver, Map<String, String> dataSet) throws Exception {

		String testCaseId = "TC_Add_Question_Rating_Type";
		String questionText = null;

		if (fc.utobj().validate(testCaseId)) {
			try {
				AdminFieldOpsQuestionLibraryPage pobj = new AdminFieldOpsQuestionLibraryPage(driver);
				fc.commonMethods().switch_cboxIframe_frameId(driver);
				questionText = fc.utobj().generateTestData(dataSet.get("questionText"));
				fc.utobj().sendKeys(driver, pobj.questionTextArea, questionText);
				fc.utobj().sendKeys(driver, pobj.helpTextForUser, dataSet.get("helpForUserText"));
				fc.utobj().clickElement(driver, pobj.questionMandantory);
				fc.utobj().selectDropDown(driver, pobj.criticalLevel, dataSet.get("criticalLevel"));
				fc.utobj().clickElement(driver, pobj.nextBtn);
				fc.utobj().selectDropDown(driver, pobj.selectResponseType, dataSet.get("responseTypeQuestionR"));
				fc.utobj().clickElement(driver, pobj.scoreInclude);
				fc.utobj().clickElement(driver, pobj.excludeNonResponse);
				fc.utobj().clickElement(driver, pobj.notApplicableResponse);
				fc.utobj().clickElement(driver, pobj.complianceNotApplicable);
				fc.utobj().sendKeys(driver, pobj.minRange, dataSet.get("minRange"));
				fc.utobj().sendKeys(driver, pobj.maxRange, dataSet.get("maxRange"));
				fc.utobj().clickElement(driver, pobj.saveBtn);
				fc.utobj().switchFrameToDefault(driver);
				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException(
					"Was not able to add Question Rating Type :: Admin > Field Ops > Question Library");

		}
		return questionText;
	}

	public String addQuestionSingleSelectRadioButtonType(WebDriver driver, Map<String, String> dataSet)
			throws Exception {

		String testCaseId = "TC_Add_Question_Single_Select_Radio_Button";
		String questionText = null;

		if (fc.utobj().validate(testCaseId)) {
			try {
				AdminFieldOpsQuestionLibraryPage pobj = new AdminFieldOpsQuestionLibraryPage(driver);
				fc.commonMethods().switch_cboxIframe_frameId(driver);
				questionText = fc.utobj().generateTestData(dataSet.get("questionText"));
				fc.utobj().sendKeys(driver, pobj.questionTextArea, questionText);
				fc.utobj().sendKeys(driver, pobj.helpTextForUser, dataSet.get("helpForUserText"));
				fc.utobj().clickElement(driver, pobj.questionMandantory);
				fc.utobj().selectDropDown(driver, pobj.criticalLevel, dataSet.get("criticalLevel"));
				fc.utobj().clickElement(driver, pobj.nextBtn);

				fc.utobj().selectDropDown(driver, pobj.selectResponseType,dataSet.get("responseTypeQuestionSingleSelectR"));
				fc.utobj().clickElement(driver, pobj.allowUserComments);
				fc.utobj().clickElement(driver, pobj.scoreInclude);
				fc.utobj().clickElement(driver, pobj.excludeNonResponse);
				fc.utobj().clickElement(driver, pobj.complianceNotApplicable);
				fc.utobj().sendKeys(driver, pobj.firstChoice, dataSet.get("firstChoice"));
				fc.utobj().sendKeys(driver, pobj.secondChoice, dataSet.get("secondChoice"));
				fc.utobj().sendKeys(driver, pobj.firstScore, dataSet.get("firstScore"));
				fc.utobj().sendKeys(driver, pobj.secondScore, dataSet.get("secondScore"));
				fc.utobj().clickElement(driver, pobj.saveBtn);
				fc.utobj().switchFrameToDefault(driver);
				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException(
					"Was not able to add Question Single Select Radio Btn Type:: Admin > Field Osp > Question Library");

		}
		return questionText;
	}

	public String addQuestionSingleSelectDropDownType(WebDriver driver, Map<String, String> dataSet) throws Exception {

		String testCaseId = "TC_Add_Question_Single_Select_DropDown";
		String questionText = null;

		if (fc.utobj().validate(testCaseId)) {
			try {
				AdminFieldOpsQuestionLibraryPage pobj = new AdminFieldOpsQuestionLibraryPage(driver);
				fc.commonMethods().switch_cboxIframe_frameId(driver);
				questionText = fc.utobj().generateTestData(dataSet.get("questionText"));
				fc.utobj().sendKeys(driver, pobj.questionTextArea, questionText);
				fc.utobj().sendKeys(driver, pobj.helpTextForUser, dataSet.get("helpForUserText"));
				fc.utobj().clickElement(driver, pobj.questionMandantory);
				fc.utobj().selectDropDown(driver, pobj.criticalLevel, dataSet.get("criticalLevel"));
				fc.utobj().clickElement(driver, pobj.nextBtn);

				fc.utobj().selectDropDown(driver, pobj.selectResponseType,
						dataSet.get("responseTypeQuestionSingleSelectDrop"));
				fc.utobj().clickElement(driver, pobj.allowUserComments);
				fc.utobj().clickElement(driver, pobj.scoreInclude);
				fc.utobj().clickElement(driver, pobj.excludeNonResponse);
				fc.utobj().clickElement(driver, pobj.notApplicableResponse);
				fc.utobj().clickElement(driver, pobj.complianceNotApplicable);
				fc.utobj().sendKeys(driver, pobj.firstChoice, dataSet.get("firstChoice"));
				fc.utobj().sendKeys(driver, pobj.secondChoice, dataSet.get("secondChoice"));
				fc.utobj().sendKeys(driver, pobj.firstScore, dataSet.get("firstScore"));
				fc.utobj().sendKeys(driver, pobj.secondScore, dataSet.get("secondScore"));
				fc.utobj().clickElement(driver, pobj.saveBtn);
				fc.utobj().switchFrameToDefault(driver);
				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException("Was not able to add Question Single Select DropDown Type");
		}
		return questionText;
	}
}
