package com.builds.test.fieldops;

import java.util.List;
import java.util.Map;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import com.builds.test.admin.AdminFranchiseLocationAddFranchiseLocationPageTest;
import com.builds.test.admin.AdminHiddenLinksConfigureFieldOpsSettingsPageTest;
import com.builds.test.admin.AdminUsersManageCorporateUsersAddCorporateUserPageTest;
import com.builds.test.admin.AdminUsersManageManageFranchiseUsersPageTest;
import com.builds.test.common.CorporateUser;
import com.builds.uimaps.fieldops.FieldOpsHomePage;
import com.builds.utilities.FranconnectUtil;
import com.builds.utilities.TestCase;

public class FieldOpsHomePageTest {

	FranconnectUtil fc = new FranconnectUtil();

	@Test(groups = {"fieldops" ,"fieldops0808"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Create Visit at Field Ops Home Page", testCaseId = "TC_59_Create_Visit_At_Home")
	public void createVisitAtHomePage() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("fieldops", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			FieldOpsHomePage pobj = new FieldOpsHomePage(driver);

			fc.utobj().printTestStep("Navigate To Admin > Fieldops > Manage Visit Form");
			fc.utobj().printTestStep("Add Private Visit Form");

			AdminFieldOpsManageVisitFormPageTest visitFormPage = new AdminFieldOpsManageVisitFormPageTest();
			String visitFormName = visitFormPage.addVisitFormPrivate(driver, dataSet);

			fc.utobj().printTestStep("Add Question In Visit Form");
			fc.utobj().clickElement(driver, pobj.addQuestion);

			AdminFieldOpsManageVisitFormCustomizeVisitFormPageTest customizeVisit = new AdminFieldOpsManageVisitFormCustomizeVisitFormPageTest();
			customizeVisit.addQuestion(driver, dataSet);

			fc.utobj().printTestStep("Add Rating Type Question In Visit Form");
			AdminFieldOpsQuestionLibraryPageTest questionLibrary = new AdminFieldOpsQuestionLibraryPageTest();
			fc.utobj().clickElement(driver, pobj.addQuestion);
			questionLibrary.addQuestionRatingType(driver, dataSet);

			fc.utobj().printTestStep("Add Single Select Drop Down Type Question In Visit Form");
			fc.utobj().clickElement(driver, pobj.addQuestion);
			questionLibrary.addQuestionSingleSelectDropDownType(driver, dataSet);

			fc.utobj().printTestStep("Add Single Select Radio Button Type Question In Visit Form");
			fc.utobj().clickElement(driver, pobj.addQuestion);
			questionLibrary.addQuestionSingleSelectRadioButtonType(driver, dataSet);

			fc.utobj().clickElement(driver, pobj.finishBtn);

			fc.utobj().printTestStep("Navigate To Admin > Franchise Location > Add Franchise Location");
			fc.utobj().printTestStep("Add Franchise Location");

			String regionName = fc.utobj().generateTestData("Test");
			String storeType = fc.utobj().generateTestData("Test");
			String franchiseId = fc.utobj().generateTestData("Test");
			AdminFranchiseLocationAddFranchiseLocationPageTest franchise = new AdminFranchiseLocationAddFranchiseLocationPageTest();
			franchiseId = franchise.addFranchiseLocation_All(driver, franchiseId, regionName, storeType);

			fc.utobj().printTestStep("Navigate To Admin > Users > Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");

			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUser = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			String emailId = "fieldopsautomation@staffex.com";
			CorporateUser corpUser = new CorporateUser();
			corpUser.setConsultant("Y");
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporateUser.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			fc.utobj().printTestStep("Navigate To Fieldops > Home Page");
			fc.fieldops().fieldops_common().fieldopsHomeCreateVisitPage(driver);

			fc.utobj().printTestStep("Create Visit");
			fc.utobj().clickElement(driver, pobj.createVisit);

			fc.utobj().selectValFromMultiSelectRadioBtn(driver, pobj.selectVisitForm, visitFormName);

			fc.utobj().selectValFromMultiSelectWithoutReset(driver, pobj.selectfranchiseeFromMultiSelect, franchiseId);

			fc.utobj().selectValFromMultiSelectRadioBtn(driver, pobj.selectConsultantVisit, corpUser.getuserFullName());

			String previousdate = fc.utobj().getFutureDateUSFormat(-1);

			// schedule Date
			fc.utobj().sendKeys(driver, pobj.scheduleDateTime, previousdate);
			String comments = dataSet.get("comments");
			fc.utobj().sendKeys(driver, pobj.comments, comments);
			fc.utobj().clickElement(driver, pobj.schedule);

			fc.utobj().setToDefault(driver, pobj.selectFranchiseId);
			fc.utobj().selectValFromMultiSelectWithoutReset(driver, pobj.selectFranchiseId, franchiseId);

			fc.utobj().printTestStep("Verify Visit At Visit Page");
			boolean isStatusPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//*[.='" + franchiseId + "']/ancestor::tr/td[contains(text () ,'Scheduled')]");
			if (isStatusPresent == false) {
				fc.utobj().throwsException("Was not able to create Visit");
			}

			fc.utobj().printTestStep("Navigate To Home Page");
			fc.utobj().clickElement(driver, pobj.homePageLnk);
			fc.utobj().printTestStep("Verify The Redirection Of YTD Tab's Overdue Visit At Home Page");
			WebElement element = fc.utobj().getElementByXpath(driver,
					".//td[contains(text () , 'Overdue Visits')]/ancestor::tr/td/a");
			fc.utobj().clickElement(driver, element);
			
			new FieldOpsVisitsPageTest().defaultViewByFranchiseId(driver, franchiseId);
			fc.utobj().selectDropDownByValue(driver, pobj.resultsPerPage, "100");

			boolean isTextPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//td[contains(text () , '" + visitFormName + "')]");
			if (isTextPresent == false) {
				fc.utobj().throwsException("was not able to verify Visit Form Name");
			}

			boolean isTextFranchiseId = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//td[contains(text () , '" + visitFormName
					+ "')]/ancestor::tr/td[contains(text () , '" + franchiseId + "')]");
			if (isTextFranchiseId == false) {
				fc.utobj().throwsException("was not able to verify Visit Form Name");
			}

			boolean isTextConsultant = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//td[contains(text () , '" + visitFormName
					+ "')]/ancestor::tr/td[contains(text () , '" + corpUser.getuserFullName() + "')]");
			if (isTextConsultant == false) {
				fc.utobj().throwsException("was not able to verify Visit Form Name");
			}

			boolean isTextStatus = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//td[contains(text () , '" + visitFormName
					+ "')]/ancestor::tr/td[contains(text () , 'Scheduled')]");
			if (isTextStatus == false) {
				fc.utobj().throwsException("was not able to verify Visit Form Name");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	public String createVisitAtHomePage(WebDriver driver, Map<String, String> config, String visitFormName,
			String franchiseId, String userName) throws Exception {

		String testCaseId = "TC_Create_Visit_At_HomePage";

		if (fc.utobj().validate(testCaseId)) {
			try {
				FieldOpsHomePage pobj = new FieldOpsHomePage(driver);
				fc.fieldops().fieldops_common().fieldopsHomeCreateVisitPage(driver);
				fc.utobj().clickElement(driver, pobj.createVisit);

				fc.utobj().selectValFromMultiSelectRadioBtn(driver, pobj.selectVisitForm, visitFormName);

				fc.utobj().selectValFromMultiSelectWithoutReset(driver, pobj.selectfranchiseeFromMultiSelect,
						franchiseId);

				fc.utobj().selectValFromMultiSelectRadioBtn(driver, pobj.selectConsultantVisit, userName);

				fc.utobj().sendKeys(driver, pobj.scheduleDateTime, fc.utobj().getCurrentDateUSFormat());
				fc.utobj().clickElement(driver, pobj.schedule);

				FieldOpsVisitsPageTest visitPage = new FieldOpsVisitsPageTest();
				visitPage.defaultViewByFranchiseId(driver, franchiseId);

				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException("was not able to create Visit At Home Page :: Field Ops > Home");

		}
		return visitFormName;
	}

	public String createVisitAtHomePageForDelete(WebDriver driver, Map<String, String> config,
			Map<String, String> dataSet, String visitFormName, String franchiseId, String userName) throws Exception {

		String testCaseId = "TC_Create_Visit_At_Home_Page_For_Delete";

		if (fc.utobj().validate(testCaseId)) {
			try {
				FieldOpsHomePage pobj = new FieldOpsHomePage(driver);

				// navigate to createVisitFormPage

				fc.fieldops().fieldops_common().fieldopsHomeCreateVisitPage(driver);

				// Click createVisitForm
				fc.utobj().clickElement(driver, pobj.createVisit);

				fc.utobj().selectValFromMultiSelectRadioBtn(driver, pobj.selectVisitForm, visitFormName);

				fc.utobj().selectValFromMultiSelectWithoutReset(driver, pobj.selectfranchiseeFromMultiSelect,
						franchiseId);

				fc.utobj().selectValFromMultiSelectRadioBtn(driver, pobj.selectConsultantVisit, userName);

				// schedule Date
				fc.utobj().sendKeys(driver, pobj.scheduleDateTime, fc.utobj().getCurrentDateUSFormat());
				String comments = dataSet.get("comments");
				fc.utobj().sendKeys(driver, pobj.comments, comments);
				fc.utobj().clickElement(driver, pobj.schedule);

				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException("was not able to Create Visit At Home Page For Delete");

		}
		return visitFormName;
	}

	@Test(groups = "fieldops")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify Create Visit Link At Home Page,  Login By FranchiseUser", testCaseId = "TC_73_Create_Visit_Link_01")
	private void createVisitLinkSelfAuditYesAtFOps() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("fieldops", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			FieldOpsHomePage pobj = new FieldOpsHomePage(driver);

			fc.utobj().printTestStep("Navigate To Admin > Hidden Links > Configure FieldOps Settings Page");
			fc.utobj().printTestStep("Configure FieldOps Settings Franchise Self Audit Yes Owner Scoring Yes");
			AdminHiddenLinksConfigureFieldOpsSettingsPageTest selfAudit = new AdminHiddenLinksConfigureFieldOpsSettingsPageTest();
			selfAudit.configureFieldOpsSettingsFranchiseSelfAuditYesOwnerScoringYes(driver, config);

			fc.utobj().printTestStep("Navigate To Admin > Franchise Location > Add Franchise Location");
			fc.utobj().printTestStep("Add Franchise Location");

			AdminFranchiseLocationAddFranchiseLocationPageTest franchiseLocation = new AdminFranchiseLocationAddFranchiseLocationPageTest();
			String franchiseId = fc.utobj().generateTestData("Test");
			String regionName = fc.utobj().generateTestData("Test");
			String storeType = fc.utobj().generateTestData("Test");
			franchiseLocation.addFranchiseLocation_All(driver, franchiseId, regionName, storeType);

			fc.utobj().printTestStep("Add Franchise User");
			AdminUsersManageManageFranchiseUsersPageTest franchiseUser = new AdminUsersManageManageFranchiseUsersPageTest();
			String userName = fc.utobj().generateTestData(dataSet.get("userName").trim());
			String password = dataSet.get("password").trim();
			String roleName = "Default Franchise Role";
			String emailId = "fieldopsautomation@staffex.com";
			userName = franchiseUser.addFranchiseUser(driver, userName, password, franchiseId, roleName, emailId);

			fc.home_page().logout(driver);
			fc.utobj().printTestStep("Login With Franchise User");
			fc.loginpage().loginWithParameter(driver, userName, password);

			fc.utobj().printTestStep("Navigate To Field Ops > Visits Page");

			fc.utobj().printTestStep("Verify Create Visit Link");
			fc.home_page().openFieldopsModule(driver);

			fc.fieldops().fieldops_common().openFieldOpsVisitSubModule(fc.home_page(), driver);

			fc.utobj().getElement(driver, pobj.createVisit).isEnabled();
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "fieldops")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify Create Visit Link At Smart Connect Page,  Login By FranchiseUser", testCaseId = "TC_74_Create_Visit_Link_02")
	private void createVisitLinkSelfAuditYesAtSC() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("fieldops", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			FieldOpsHomePage pobj = new FieldOpsHomePage(driver);

			fc.utobj().printTestStep("Navigate To Admin > Hidden Links > Configure FieldOps Settings Page");
			fc.utobj().printTestStep("Configure FieldOps Settings Franchise Self Audit Yes Owner Scoring Yes");

			// Configure Hidden Links FieldOps
			AdminHiddenLinksConfigureFieldOpsSettingsPageTest selfAudit = new AdminHiddenLinksConfigureFieldOpsSettingsPageTest();
			selfAudit.configureFieldOpsSettingsFranchiseSelfAuditYesOwnerScoringYes(driver, config);

			fc.utobj().printTestStep("Navigate To Admin > Franchise Location > Add Franchise Location");
			fc.utobj().printTestStep("Add Franchise Location");
			AdminFranchiseLocationAddFranchiseLocationPageTest franchiseLocation = new AdminFranchiseLocationAddFranchiseLocationPageTest();
			String franchiseId = fc.utobj().generateTestData("Test");
			String regionName = fc.utobj().generateTestData("Test");
			String storeType = fc.utobj().generateTestData("Test");
			franchiseLocation.addFranchiseLocation_All(driver, franchiseId, regionName, storeType);

			fc.utobj().printTestStep("Add Franchise User");
			AdminUsersManageManageFranchiseUsersPageTest franchiseUser = new AdminUsersManageManageFranchiseUsersPageTest();
			String userName = fc.utobj().generateTestData(dataSet.get("userName").trim());
			String password = dataSet.get("password").trim();
			String roleName = "Default Franchise Role";
			String emailId = "fieldopsautomation@staffex.com";
			userName = franchiseUser.addFranchiseUser(driver, userName, password, franchiseId, roleName, emailId);

			fc.home_page().logout(driver);
			fc.utobj().printTestStep("Login With Franchise User");
			fc.loginpage().loginWithParameter(driver, userName, password);

			fc.utobj().printTestStep("Navigate To SmartConnect Module");
			fc.home_page().openSmartConnectModule(driver);

			fc.utobj().printTestStep("Verify Create Visit Link");

			fc.utobj().getElement(driver, pobj.createVisit).isEnabled();

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "fieldops")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Allow Consultant Private Comment if log private comments which are not visible to location users option is Yes", testCaseId = "TC_77_Allow_Consultant_Private_Comment_01")
	private void allowConsultantPrivateCommentYes() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("fieldops", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			FieldOpsHomePage pobj = new FieldOpsHomePage(driver);

			fc.utobj().printTestStep("Navigate To Admin > Hidden Links > Configure Field Ops Settings Page");
			fc.utobj().printTestStep("Allow Private Comment For Consultant");

			// Configure Hidden Links FieldOps
			AdminHiddenLinksConfigureFieldOpsSettingsPageTest selfAudit = new AdminHiddenLinksConfigureFieldOpsSettingsPageTest();
			selfAudit.configureFieldOpsSettingsLogPrivateCommentYes(driver, config);

			fc.utobj().printTestStep("Navigate To Admin > Fieldops > Manage Visit Form");
			fc.utobj().printTestStep("Add Private Visit Form");
			AdminFieldOpsManageVisitFormPageTest addVisitForm = new AdminFieldOpsManageVisitFormPageTest();
			addVisitForm.addVisitFormPrivate(driver, dataSet);

			fc.utobj().printTestStep("Add Question In Visit Form");
			AdminFieldOpsManageVisitFormCustomizeVisitFormPageTest manageVisitForm = new AdminFieldOpsManageVisitFormCustomizeVisitFormPageTest();
			fc.utobj().clickElement(driver, pobj.addQuestion);
			manageVisitForm.addQuestion(driver, dataSet);

			fc.utobj().printTestStep("Verify Private Comment Box");

			fc.utobj().getElement(driver, pobj.privateBox).isEnabled();

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "fieldops")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Private Visit form will not visible to Franchise User", testCaseId = "TC_79_Private_Visit_Form_01")
	private void privateVisitFormNotVisibleToFranchise() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("fieldops", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			FieldOpsHomePage pobj = new FieldOpsHomePage(driver);

			fc.utobj().printTestStep("Navigate To Admin > Hidden Links > Configure Field Ops Settings Page");
			fc.utobj().printTestStep("Configure FieldOps Settings Franchise Self Audit Yes Owner Scoring Yes");

			// Configure Hidden Links FieldOps
			AdminHiddenLinksConfigureFieldOpsSettingsPageTest selfAudit = new AdminHiddenLinksConfigureFieldOpsSettingsPageTest();
			selfAudit.configureFieldOpsSettingsFranchiseSelfAuditYesOwnerScoringYes(driver, config);

			fc.utobj().printTestStep("Navigate To Admin > Fieldops > Manage Visit Form");
			fc.utobj().printTestStep("Add Private Visit Form");

			AdminFieldOpsManageVisitFormPageTest addVisitForm = new AdminFieldOpsManageVisitFormPageTest();
			String visitFormName = addVisitForm.addVisitFormPrivate(driver, dataSet);

			fc.utobj().printTestStep("Add Question In Visit Form");
			AdminFieldOpsManageVisitFormCustomizeVisitFormPageTest manageVisitForm = new AdminFieldOpsManageVisitFormCustomizeVisitFormPageTest();
			fc.utobj().clickElement(driver, pobj.addQuestion);
			manageVisitForm.addQuestion(driver, dataSet);
			fc.utobj().clickElement(driver, pobj.finishBtn);

			fc.utobj().printTestStep("Navigate To Admin > Franchise Location > Add Franchise Location");
			fc.utobj().printTestStep("Add Franchise Location");

			AdminFranchiseLocationAddFranchiseLocationPageTest franchiseLocation = new AdminFranchiseLocationAddFranchiseLocationPageTest();
			String franchiseId = fc.utobj().generateTestData("Test");
			String regionName = fc.utobj().generateTestData("Test");
			String storeType = fc.utobj().generateTestData("Test");
			franchiseLocation.addFranchiseLocation_All(driver, franchiseId, regionName, storeType);

			fc.utobj().printTestStep("Add Franchise User");
			AdminUsersManageManageFranchiseUsersPageTest franchiseUser = new AdminUsersManageManageFranchiseUsersPageTest();
			String userName = fc.utobj().generateTestData(dataSet.get("userName").trim());
			String password = dataSet.get("password").trim();
			String roleName = "Default Franchise Role";

			String emailId = "fieldopsautomation@staffex.com";
			userName = franchiseUser.addFranchiseUser(driver, userName, password, franchiseId, roleName, emailId);

			fc.home_page().logout(driver);

			fc.utobj().printTestStep("Login With Franchise User");
			fc.loginpage().loginWithParameter(driver, userName, password);

			fc.utobj().printTestStep("Navigate To FielsOps > Visits Page");
			fc.home_page().openFieldopsModule(driver);

			fc.fieldops().fieldops_common().openFieldOpsVisitSubModule(fc.home_page(), driver);

			fc.utobj().printTestStep("Verify Private Visit Form At The Time Of Visit Creation");
			fc.utobj().clickElement(driver, pobj.createVisit);

			fc.utobj().clickElement(driver, pobj.selectClick);

			List<WebElement> list = driver.findElements(By.xpath(".//*[@id='ms-parentmasterVisitId']/div/ul/li/label"));

			String[] arrayList = new String[list.size()];
			for (int i = 0; i < list.size(); i++) {
				arrayList[i] = list.get(i).getText().trim();
				System.out.println(arrayList[i]);
			}

			for (int k = 0; k < arrayList.length; k++) {
				if (arrayList[k].equalsIgnoreCase(visitFormName)) {
					fc.utobj().throwsException("Was not able to verify private form");
				}
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "fieldops")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Private Visit form will not visible to Franchise User in Report", testCaseId = "TC_80_Private_Visit_Form_02")
	private void privateVisitFormNotVisibleToFranchiseReport() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("fieldops", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			FieldOpsHomePage pobj = new FieldOpsHomePage(driver);

			fc.utobj().printTestStep("Navigate To Admin > Hidden Links > Configure Field Ops Settings Page");
			fc.utobj().printTestStep("Configure FieldOps Settings Franchise Self Audit Yes Owner Scoring Yes");

			// Configure Hidden Links FieldOps
			AdminHiddenLinksConfigureFieldOpsSettingsPageTest selfAudit = new AdminHiddenLinksConfigureFieldOpsSettingsPageTest();
			selfAudit.configureFieldOpsSettingsFranchiseSelfAuditYesOwnerScoringYes(driver, config);

			fc.utobj().printTestStep("Navigate To Admin > Fieldops > Manage Visit Form");
			fc.utobj().printTestStep("Add Private Visit Form");

			AdminFieldOpsManageVisitFormPageTest addVisitForm = new AdminFieldOpsManageVisitFormPageTest();
			String visitFormName = addVisitForm.addVisitFormPrivate(driver, dataSet);

			fc.utobj().printTestStep("Add Question In Visit Form");
			AdminFieldOpsManageVisitFormCustomizeVisitFormPageTest manageVisitForm = new AdminFieldOpsManageVisitFormCustomizeVisitFormPageTest();
			fc.utobj().clickElement(driver, pobj.addQuestion);
			manageVisitForm.addQuestion(driver, dataSet);
			fc.utobj().clickElement(driver, pobj.finishBtn);

			fc.utobj().printTestStep("Navigate To Admin > Franchise Location > Add Franchise Location");
			fc.utobj().printTestStep("Add Franchise Location");
			AdminFranchiseLocationAddFranchiseLocationPageTest franchiseLocation = new AdminFranchiseLocationAddFranchiseLocationPageTest();
			String franchiseId = fc.utobj().generateTestData("Test");
			String regionName = fc.utobj().generateTestData("Test");
			String storeType = fc.utobj().generateTestData("Test");
			franchiseLocation.addFranchiseLocation_All(driver, franchiseId, regionName, storeType);

			fc.utobj().printTestStep("Add Franchise User");
			AdminUsersManageManageFranchiseUsersPageTest franchiseUser = new AdminUsersManageManageFranchiseUsersPageTest();
			String userName = fc.utobj().generateTestData(dataSet.get("userName").trim());
			String password = dataSet.get("password").trim();
			String roleName = "Default Franchise Role";
			String emailId = "fieldopsautomation@staffex.com";
			userName = franchiseUser.addFranchiseUser(driver, userName, password, franchiseId, roleName, emailId);

			fc.home_page().logout(driver);
			fc.utobj().printTestStep("Login With Franchise User");
			fc.loginpage().loginWithParameter(driver, userName, password);

			fc.utobj().printTestStep("Navigate To Visit Summary Report Page");
			fc.home_page().openFieldopsModule(driver);

			fc.fieldops().fieldops_common().openFieldOpsReportsSubModule(fc.home_page(), driver);
			fc.utobj().clickElement(driver, pobj.visitSummaryReport);

			fc.utobj().printTestStep("Verify Private Visit Form At Visit Summary Report");
			fc.utobj().clickElement(driver, pobj.visitFormInReports);

			List<WebElement> list = driver.findElements(By.xpath(".//*[@id='ms-parentformName']/div/ul/li"));

			String[] arrayList = new String[list.size()];
			for (int i = 0; i < list.size(); i++) {
				arrayList[i] = list.get(i).getText().trim();
				System.out.println(arrayList[i]);
			}

			for (int k = 0; k < arrayList.length; k++) {
				if (arrayList[k].equalsIgnoreCase(visitFormName)) {
					fc.utobj().throwsException("Was not able to verify private form");
				}
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}
}
