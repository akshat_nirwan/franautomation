package com.builds.test.fieldops;

import java.io.IOException;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.xml.parsers.ParserConfigurationException;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;
import org.xml.sax.SAXException;

import com.builds.test.infomgr.AdminInfoMgrCommonMethods;
import com.builds.test.infomgr.FranchiseesCommonMethods;
import com.builds.uimaps.common.FCHomePage;
import com.builds.uimaps.infomgr.InfoMgrPagination;
import com.builds.utilities.FranconnectUtil;
import com.builds.utilities.TestCase;
import com.graphbuilder.struc.LinkedList;

public class FieldOpsPaginationTest {
	FranconnectUtil fc = new FranconnectUtil();

	@Test(groups = { "fieldops" })

	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "TC_Field_Ops_Paging_Report_Filter_Tasks", testCaseDescription = "This test case will verify Tasks Paging all the summary of  Field Ops Module ", reference = {
			"" })
	public void VerifyFieldOpsSummaryPagingTasks() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		FranchiseesCommonMethods franchise = fc.infomgr().franchisees();
		AdminInfoMgrCommonMethods adminInfoMgr = fc.adminpage().adminInfoMgr();
		List<String> listItems = new ArrayList<String>();
		FCHomePage pobj = new FCHomePage(driver);
		try {
			driver = fc.loginpage().login(driver);
			List<WebElement> elementofSummaryPage = null;

			fc.utobj().printTestStep("verify Paging for  Tasks");
			showPagingFieldOpsModule(FranconnectUtil.config, driver, "Tasks", 20);

			// End Lead Summary paging

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "fieldops" })

	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "TC_Field_Ops_Paging_Report_Filter_Visits", testCaseDescription = "This test case will verify Paging all the summary of  Field Ops Module ", reference = {
			"" })
	public void VerifyFieldOpsSummaryPagingVisits() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		FranchiseesCommonMethods franchise = fc.infomgr().franchisees();
		AdminInfoMgrCommonMethods adminInfoMgr = fc.adminpage().adminInfoMgr();
		List<String> listItems = new ArrayList<String>();
		FCHomePage pobj = new FCHomePage(driver);
		try {
			driver = fc.loginpage().login(driver);
			List<WebElement> elementofSummaryPage = null;

			fc.utobj().printTestStep("verify Paging for  Field Ops Visits  ");
			showPagingFieldOpsModule(FranconnectUtil.config, driver, "Visits", 20);

			// End Lead Summary paging

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	public void showPagingFieldOpsModule(Map<String, String> config, WebDriver driver, String subModuleName,
			int viewPerPage)
			throws ParserConfigurationException, SAXException, IOException, InterruptedException, Exception

	{
		String testCaseId = "TC_Field_Ops_Paging_Report_Filter";
		InfoMgrPagination InfoMgrPagination = new InfoMgrPagination(driver);

		if (subModuleName == null)
			subModuleName = "Visits";
		List<WebElement> records = null;
		fc.utobj().printTestStep("Search " + subModuleName + " and click");

		if (subModuleName != null && "Visits".equals(subModuleName)) {
			fc.fieldops().fieldops_common().fieldopsVisit(driver);
			try {
				records = driver.findElements(By.xpath(".//*[@id='ajaxDiv']"));
				// Thread.sleep(1000);
			} catch (Exception e) {
				e.printStackTrace();
			}
		} else if (subModuleName != null && ("Tasks".equals(subModuleName))) {
			fc.fieldops().fieldops_common().fieldopsTasks(driver);
			try {
				records = driver.findElements(By.xpath(".//*[@id='siteMainTable']"));
				// Thread.sleep(1000);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		String header;
		int totalRecordCount = 0;
		int totalrecordPerPage = 0;
		String opnerPageingCheckList = "pageid";
		try {
			header = fc.utobj().getElementByXpath(driver, ".//td[@id='pageid']").getText();
			String options[] = header.split(" ");
			if (options != null) {

				totalRecordCount = Integer.valueOf(options[5]);
				totalrecordPerPage = Integer.valueOf(options[3]);
			}
			fc.utobj().printTestStep(subModuleName + " ToTal Record " + totalRecordCount);

		} catch (Exception E) {
			fc.utobj().printTestStep("Pagination not found in " + subModuleName);
			// System.out.println("Pagination not found");
			totalRecordCount = 0;
		}

		if (totalRecordCount > 0 && totalRecordCount > viewPerPage) {
			fc.utobj().printTestStep("Pagination exists in " + subModuleName);

			int clickCount = totalRecordCount / viewPerPage;
			String optionsShoud[] = new String[clickCount + 1];
			;
			int inLastRerord = totalRecordCount - (viewPerPage * clickCount);
			for (int cnt = 0; cnt <= clickCount; cnt++) {

				optionsShoud[cnt] = viewPerPage + "";
				if (cnt == clickCount) {
					optionsShoud[cnt] = inLastRerord + "";
				}

			}

			int foundRecordCount = 0;
			int counter = 1;
			try {
				if (optionsShoud != null && optionsShoud.length > 0) {
					for (int i = 0; i < (optionsShoud.length); i++) {
						// System.out.println("===Val=="+lstOptions.get(i)
						// +"==New=="+lstOptions.get(i).getText()
						// +"===clickCount=="+clickCount+"optionsShoud=="+optionsShoud[i]);
						if (true) {

							if (i == 1 && i != 0) {
								fc.utobj().clickElement(driver,
										fc.utobj().getElementByXpath(driver, ".//*[@id='pageid']/a[" + (i) + "]/u"));
								counter = i;
							} else if (i > 1 && i != 0) {
								fc.utobj().clickElement(driver,
										fc.utobj().getElementByXpath(driver, ".//*[@id='pageid']/a[" + (i) + "]/u"));
								counter = i + 1;
							}
							foundRecordCount = pagingRecordsFiledsOps(driver, records, subModuleName).size();
							if (foundRecordCount > 0) {

								if (foundRecordCount == Integer.parseInt(optionsShoud[i])) {
									// System.out.println("found record
									// "+foundRecordCount+"= Test Case Pass for
									// the page "+(i));
									fc.utobj().printTestStep("" + foundRecordCount + " records for page = " + counter);
								} else {

									if (foundRecordCount <= Integer.parseInt(optionsShoud[i])) {
										// System.out.println("found record
										// "+foundRecordCount+"= Test Case Pass
										// for the Last page "+(i));
										fc.utobj().printTestStep(
												"" + foundRecordCount + " records for page = " + counter);
									} else {
										// System.out.println("found record
										// "+foundRecordCount+"= Test Case Fail
										// for the page "+(i));
										// fc.utobj().printTestStep(testCaseId,
										// "found record "+foundRecordCount+"=
										// Test Case Fail for the page "+(i));
										fc.utobj().throwsException("found record " + foundRecordCount
												+ "= Test Case Fail for the page Almost Last Page " + counter);
									}
								}
							} else {
								fc.utobj().throwsException(
										"" + foundRecordCount + " records for page " + counter + " Test case fails");
							}
							// Check for paging after sort function

							fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
									".//table[@class='summaryTblex']/tbody/tr[1]/td[2]//a[not(*)]"));
							foundRecordCount = pagingRecordsFiledsOps(driver, records, subModuleName).size();
							if (foundRecordCount > 0) {

								if (foundRecordCount == Integer.parseInt(optionsShoud[i])) {
									// System.out.println("found record
									// "+foundRecordCount+"= Test Case Pass for
									// the page "+(i));
									fc.utobj().printTestStep(
											"After Sort record " + foundRecordCount + " for the page " + counter);
								} else {

									if (foundRecordCount <= Integer.parseInt(optionsShoud[i])) {
										// System.out.println("found record
										// "+foundRecordCount+"= Test Case Pass
										// for the Last page "+(i));
										fc.utobj().printTestStep(
												"After Sort record " + foundRecordCount + " for the page " + counter);
									} else {
										fc.utobj().throwsException("After Sort record " + foundRecordCount
												+ "= Test Case Fail for the page Almost Last Page " + (i + 1));
									}

								}
							} else {
								fc.utobj().throwsException("" + foundRecordCount + " records for page " + counter
										+ " Test case fails on click sort");
							}
							//

						}
					}
				}

			} catch (Exception e) {
				e.printStackTrace();
			}

			try {
				// fc.utobj().printTestStep(testCaseId, "Reset our page to the
				// page 1 to check the results Per Page verification.");
				fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='pageid']/a[1]/u"));
				fc.utobj().selectDropDown(driver,
						fc.utobj().getElementByXpath(driver, ".//*[@id='ajaxDiv']//*[@id='resultsPerPage']"), "100");
				foundRecordCount = pagingRecordsFiledsOps(driver, records, subModuleName).size();
				if (foundRecordCount > 0) {

				}
				if (foundRecordCount <= 100) {
					// System.out.println("found record in Result Per page all
					// "+foundRecordCount+" test case passed at View Per Page");
					fc.utobj().printTestStep("View Per Page " + foundRecordCount + " records.");

				}
				// Check for paging after click on Sort function
				fc.utobj().clickElement(driver,
						fc.utobj().getElementByXpath(driver, ".//table[@class='summaryTblex']/tbody/tr[1]/td[2]/a"));
				foundRecordCount = pagingRecordsFiledsOps(driver, records, subModuleName).size();
				if (foundRecordCount > 0) {

				}
				if (foundRecordCount <= 100) {
					// System.out.println("found record in Result Per page all
					// "+foundRecordCount+" test case passed at View Per Page");
					fc.utobj().printTestStep("After Sort found record in Result Per page  " + foundRecordCount
							+ " test case passed at View Per Page");

				} /*
					 * else { fc.utobj().throwsException(
					 * "After Sort found record in Result Per page  "
					 * +foundRecordCount+" test case wrong at View Per Page"); }
					 */
				fc.utobj().selectDropDown(driver,
						fc.utobj().getElementByXpath(driver, ".//*[@id='ajaxDiv']//*[@id='resultsPerPage']"), "20");

			} catch (Exception E) {
				// System.out.println("resultsPerPage Field Not Found!");
				fc.utobj().printTestStep("View Per Page  Field Not Found! in " + subModuleName);

				// totalRecordCount = 0;
			}
			try {

				fc.utobj().clickLink(driver, "Show All");

				// System.out.println("totalRecordCount===in show
				// all"+totalRecordCount);
				fc.utobj().printTestStep("totalRecordCount in show all " + subModuleName + " = " + totalRecordCount);
				foundRecordCount = pagingRecordsFiledsOps(driver, records, subModuleName).size();
				if (foundRecordCount > 0) {

				}
				if (totalRecordCount == foundRecordCount) {
					// System.out.println("found record in show all
					// "+foundRecordCount+" test case passed");
					fc.utobj().printTestStep("found record in show all " + foundRecordCount + " test case passed");
				} else {
					fc.utobj()
							.printTestStep("found record in show all " + foundRecordCount + " test case please check");
				}
				// System.out.println("found record "+foundRecordCount+" in
				// "+lstOptions.get(i).getText()+" th Page");

			} catch (Exception E) {
				// System.out.println("Show All Field Not Found!");
				fc.utobj().printTestStep("Show All field not found!");
				// totalRecordCount = 0;
			}

		} else {
			// System.out.println("Pagination Not Exists");
			fc.utobj().printTestStep("Pagination Not Exists in " + subModuleName);
		}

		// sortedList
		try {
			boolean isSorttable = sortedListFieldsOps(driver, config, records, subModuleName);
			if (isSorttable == true) {
				fc.utobj().printTestStep("Sorting function working for  " + subModuleName);
			} else {
				fc.utobj().printTestStep("Sorting function not Working for  " + subModuleName + " OR not applicable.");
			}

		} catch (Exception e) {
			// e.printStackTrace();
			fc.utobj().printTestStep("Eception in Sorting function which Working for  " + subModuleName);
		}

		// End sortedList

		fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
	}

	public boolean sortedListFieldsOps(WebDriver driver, Map<String, String> config, List<WebElement> records,
			String subModuleName) {

		boolean isSorttable = false;
		try {
			records = pagingRecordsFiledsOps(driver, records, subModuleName);
			ArrayList<String> obtainedList = new ArrayList<>();
			ArrayList<String> sortedList = new ArrayList<>();
			List<WebElement> elementList = records;
			for (WebElement we : elementList) {
				obtainedList.add(we.getText());
			}
			if (subModuleName != null && "Visits".equals(subModuleName)) {
				fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
						".//table[@class='summaryTblex']/tbody/tr[1]/td[1]//a[not(*)]"));

			} else {
				fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
						".//table[@class='summaryTblex']/tbody/tr[1]/td[2]//a[not(*)]"));

			}

			records = pagingRecordsFiledsOps(driver, records, subModuleName);
			for (WebElement we : records) {
				sortedList.add(we.getText());
			}
			try {
				if (sortedList != null) {
					for (int i = 0; i < sortedList.size(); i++) {
						// System.out.println(sortedList.get(i));
						if (sortedList.get(0) != obtainedList.get(0)) {
							// System.out.println(sortedList.get(0)+"======="+obtainedList.get(0));
							isSorttable = true;
							break;
						}
					}
				}

			} catch (Exception e) {
				isSorttable = false;

			}

		} catch (Exception e) {
			isSorttable = false;
			// e.printStackTrace();
		}

		return isSorttable;
	}

	public List<WebElement> pagingRecordsFiledsOps(WebDriver driver, List<WebElement> records, String subModuleName) {

		if (subModuleName == null)
			subModuleName = "LeadSummary";
		List<WebElement> listofElements = null;
		int rc = 0;
		if (subModuleName != null && "Visits".equals(subModuleName)) {
			listofElements = driver.findElements(
					By.xpath(".//table[@class='summaryTblex']/tbody/tr/td[@class='botBorder colPadding'][3]/a"));

		} else if (subModuleName != null && ("Tasks".equals(subModuleName))) {
			listofElements = driver.findElements(
					By.xpath(".//table[@class='summaryTblex']/tbody/tr/td[@class='botBorder colPadding'][2]/a"));
		}
		rc = listofElements.size();
		return listofElements;

	}

}
