package com.builds.test.fieldops;

import java.util.Map;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.Test;

import com.builds.test.admin.AdminFranchiseLocationAddFranchiseLocationPageTest;
import com.builds.test.admin.AdminUsersManageCorporateUsersAddCorporateUserPageTest;
import com.builds.test.common.CorporateUser;
import com.builds.test.support.SupportReportsPageTest;
import com.builds.uimaps.fieldops.FieldOpsReportsPage;
import com.builds.uimaps.fieldops.FieldOpsVisitsPage;
import com.builds.utilities.FranconnectUtil;
import com.builds.utilities.TestCase;

public class FieldOpsReportsPageTest {

	FranconnectUtil fc = new FranconnectUtil();
	SupportReportsPageTest rpPageTest = new SupportReportsPageTest();

	@Test(groups = "fieldops")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify Visit Form Details Reports For Not Applicable Frequency Of Visit Form", testCaseId = "TC_Visit_Form_Details_Report")
	private void visitFormDetailsReport() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("fieldops", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			FieldOpsVisitsPage pobj = new FieldOpsVisitsPage(driver);
			FieldOpsReportsPage report_page = new FieldOpsReportsPage(driver);

			fc.utobj().printTestStep("Navigate To Admin > Fieldops > Manage Visit Form");
			fc.utobj().printTestStep("Add Private Visit Form");
			AdminFieldOpsManageVisitFormPageTest visitFormPage = new AdminFieldOpsManageVisitFormPageTest();
			String visitFormName = visitFormPage.addVisitFormPrivate(driver, dataSet);
			fc.utobj().printTestStep("Add Question");
			fc.utobj().clickElement(driver, pobj.addQuestion);
			AdminFieldOpsManageVisitFormCustomizeVisitFormPageTest customizeVisit = new AdminFieldOpsManageVisitFormCustomizeVisitFormPageTest();
			customizeVisit.addQuestion(driver, dataSet);
			fc.utobj().clickElement(driver, pobj.finishBtn);

			fc.utobj().printTestStep("Navigate To Admin > Franchise Location > Add Franchise Location");
			fc.utobj().printTestStep("Add Franchise Location");
			String regionName = fc.utobj().generateTestData("TestRegion");
			String storeType = fc.utobj().generateTestData("TestStore");
			String franchiseId = fc.utobj().generateTestData("TestFID");
			String divisionName = fc.utobj().generateTestData("TestDiv");
			AdminFranchiseLocationAddFranchiseLocationPageTest franchise = new AdminFranchiseLocationAddFranchiseLocationPageTest();
			franchiseId = franchise.addFranchiseLocationWithDivision(driver, config, franchiseId, regionName, storeType,
					divisionName);

			fc.utobj().printTestStep("Navigate To Admin > Users > Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUser = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			String emailId = "fieldopsautomation@staffex.com";
			CorporateUser corpUser = new CorporateUser();
			corpUser.setConsultant("Y");
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporateUser.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			fc.utobj().printTestStep("Navigate To FieldOps > Visits Page");
			fc.fieldops().fieldops_common().fieldopsVisitCreateVisitPage(driver);
			fc.utobj().printTestStep("Create Visit");

			FieldOpsVisitsPageTest visit_page = new FieldOpsVisitsPageTest();
			String scheduleDate = visit_page.createVisitAtVisitPage(driver, config, dataSet, visitFormName, franchiseId,
					corpUser.getuserFullName());

			String statusOfVisit = fc.utobj().getText(driver,
					fc.utobj().getElementByXpath(driver, ".//td[contains(text () , '" + visitFormName
							+ "')]/ancestor::tr/td[contains(text () , 'Scheduled')]"));

			String visitNumber = fc.utobj().getText(driver, fc.utobj().getElementByXpath(driver,
					".//td[contains(text () , '" + visitFormName + "')]/preceding-sibling::td/a"));

			fc.utobj().printTestStep("Navigate To Field Ops > Reports > Visit Form Details Report");
			fc.fieldops().fieldops_common().fieldopsReports(driver);
			fc.utobj().clickElement(driver, report_page.visitFormDetailsReportLnk);

			filterAtReportsPageForVisitSelect(driver, visitFormName, scheduleDate);
			filterAtReportsPageDivisionSelect(driver, divisionName);
			filterAtReportsFranchiseId(driver, franchiseId);
			filterAtReportsConsultant(driver, corpUser.getuserFullName());
			filterAtReportsStatus(driver, statusOfVisit);

			fc.utobj().clickElement(driver, report_page.viewReport);

			fc.utobj().switchFrameById(driver, "reportiframe");

			boolean isFranchisePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td/a[contains(text () , '" + franchiseId + "')]");

			if (isFranchisePresent == false) {
				fc.utobj().throwsException("was not able to verify Franchise Id At Reports Page");
			}

			boolean isFrequencyPrsent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//td/a[contains(text () , '" + franchiseId
					+ "')]/ancestor::tr/td[contains(text () , '" + dataSet.get("frequency") + "')]");

			if (isFrequencyPrsent == false) {
				fc.utobj().throwsException("was not able to verify Visit Form Frequency");
			}

			boolean isScheduleDatePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//td/a[contains(text () , '" + franchiseId
					+ "')]/ancestor::tr/td[contains(text () , '" + scheduleDate + "')]");

			if (isScheduleDatePresent == false) {
				fc.utobj().throwsException("was not able to verify schedule Date of Visit");
			}

			boolean isConsultantPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//td/a[contains(text () , '" + franchiseId
					+ "')]/ancestor::tr/td[contains(text () , '" + corpUser.getuserFullName() + "')]");

			if (isConsultantPresent == false) {
				fc.utobj().throwsException("was not able to verify Consultant");
			}

			boolean isStatusPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//td/a[contains(text () , '" + franchiseId
					+ "')]/ancestor::tr/td[contains(text () , '" + statusOfVisit + "')]");

			if (isStatusPresent == false) {
				fc.utobj().throwsException("was not able to verify status Of Visit");
			}

			boolean isVisitNumberPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//td/a[contains(text () , '" + franchiseId
					+ "')]/ancestor::tr/td/a[contains(text () , '" + visitNumber + "')]");

			if (isVisitNumberPresent == false) {
				fc.utobj().verifyElementOnVisible_ByXpath(driver, "was not able to verify Visit Number");
			}
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "fieldopsTest12")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify Visit Summary Report", testCaseId = "TC_Visit_Summary_Report")
	private void visitSummaryReport() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("fieldops", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			FieldOpsVisitsPage pobj = new FieldOpsVisitsPage(driver);
			FieldOpsReportsPage report_page = new FieldOpsReportsPage(driver);

			fc.utobj().printTestStep("Navigate To Admin > Fieldops > Manage Visit Form");
			fc.utobj().printTestStep("Add Private Visit Form");
			AdminFieldOpsManageVisitFormPageTest visitFormPage = new AdminFieldOpsManageVisitFormPageTest();
			String visitFormName = visitFormPage.addVisitFormPrivate(driver, dataSet);
			fc.utobj().printTestStep("Add Question");
			fc.utobj().clickElement(driver, pobj.addQuestion);
			AdminFieldOpsManageVisitFormCustomizeVisitFormPageTest customizeVisit = new AdminFieldOpsManageVisitFormCustomizeVisitFormPageTest();
			customizeVisit.addQuestion(driver, dataSet);
			fc.utobj().clickElement(driver, pobj.finishBtn);

			fc.utobj().printTestStep("Navigate To Admin > Franchise Location > Add Franchise Location");
			fc.utobj().printTestStep("Add Franchise Location");
			String regionName = fc.utobj().generateTestData("TestRegion");
			String storeType = fc.utobj().generateTestData("TestStore");
			String franchiseId = fc.utobj().generateTestData("TestFID");
			String divisionName = fc.utobj().generateTestData("TestDiv");
			AdminFranchiseLocationAddFranchiseLocationPageTest franchise = new AdminFranchiseLocationAddFranchiseLocationPageTest();
			franchiseId = franchise.addFranchiseLocationWithDivision(driver, config, franchiseId, regionName, storeType,
					divisionName);

			fc.utobj().printTestStep("Navigate To Admin > Users > Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUser = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			String emailId = "fieldopsautomation@staffex.com";
			CorporateUser corpUser = new CorporateUser();
			corpUser.setConsultant("Y");
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporateUser.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			fc.utobj().printTestStep("Navigate To FieldOps > Visits Page");
			fc.fieldops().fieldops_common().fieldopsVisitCreateVisitPage(driver);
			fc.utobj().printTestStep("Create Visit");

			FieldOpsVisitsPageTest visit_page = new FieldOpsVisitsPageTest();
			String scheduleDate = visit_page.createVisitAtVisitPage(driver, config, dataSet, visitFormName, franchiseId,
					corpUser.getuserFullName());

			String statusOfVisit = fc.utobj().getText(driver,
					fc.utobj().getElementByXpath(driver, ".//td[contains(text () , '" + visitFormName
							+ "')]/ancestor::tr/td[contains(text () , 'Scheduled')]"));

			String visitNumber = fc.utobj().getText(driver, fc.utobj().getElementByXpath(driver,
					".//td[contains(text () , '" + visitFormName + "')]/preceding-sibling::td/a"));

			fc.utobj().printTestStep("Navigate To Field Ops > Reports > Visit Form Details Report");
			fc.fieldops().fieldops_common().fieldopsReports(driver);
			fc.utobj().clickElement(driver, report_page.visitFormDetailsReportLnk);

			filterAtReportsPageForVisitSelect(driver, visitFormName, scheduleDate);
			filterAtReportsPageDivisionSelect(driver, divisionName);
			filterAtReportsFranchiseId(driver, franchiseId);
			filterAtReportsConsultant(driver, corpUser.getuserFullName());
			filterAtReportsStatus(driver, statusOfVisit);

			fc.utobj().clickElement(driver, report_page.viewReport);

			fc.utobj().switchFrameById(driver, "reportiframe");

			boolean isFranchisePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td/a[contains(text () , '" + franchiseId + "')]");

			if (isFranchisePresent == false) {
				fc.utobj().throwsException("was not able to verify Franchise Id At Reports Page");
			}

			boolean isFrequencyPrsent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//td/a[contains(text () , '" + franchiseId
					+ "')]/ancestor::tr/td[contains(text () , '" + dataSet.get("frequency") + "')]");

			if (isFrequencyPrsent == false) {
				fc.utobj().throwsException("was not able to verify Visit Form Frequency");
			}

			boolean isScheduleDatePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//td/a[contains(text () , '" + franchiseId
					+ "')]/ancestor::tr/td[contains(text () , '" + scheduleDate + "')]");

			if (isScheduleDatePresent == false) {
				fc.utobj().throwsException("was not able to verify schedule Date of Visit");
			}

			boolean isConsultantPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//td/a[contains(text () , '" + franchiseId
					+ "')]/ancestor::tr/td[contains(text () , '" + corpUser.getuserFullName() + "')]");

			if (isConsultantPresent == false) {
				fc.utobj().throwsException("was not able to verify Consultant");
			}

			boolean isStatusPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//td/a[contains(text () , '" + franchiseId
					+ "')]/ancestor::tr/td[contains(text () , '" + statusOfVisit + "')]");

			if (isStatusPresent == false) {
				fc.utobj().throwsException("was not able to verify status Of Visit");
			}

			boolean isVisitNumberPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//td/a[contains(text () , '" + franchiseId
					+ "')]/ancestor::tr/td/a[contains(text () , '" + visitNumber + "')]");

			if (isVisitNumberPresent == false) {
				fc.utobj().verifyElementOnVisible_ByXpath(driver, "was not able to verify Visit Number");
			}
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	public void filterAtReportsPageForVisitSelect(WebDriver driver, String visitFormName, String scheduleDate)
			throws Exception {

		FieldOpsReportsPage report_page = new FieldOpsReportsPage(driver);

		fc.utobj().clickElement(driver, report_page.selectVisitFormName);
		if (!fc.utobj().isSelected(driver, report_page.isVisitSelected)) {
			fc.utobj().clickElement(driver, report_page.isVisitSelected);
			fc.utobj().clickElement(driver, report_page.selectVisitFormName);
		}

		fc.utobj().sendKeys(driver, fc.utobj().getElement(driver, report_page.selectVisitFormName)
				.findElement(By.xpath("./div/div/input")), visitFormName);
		fc.utobj().clickElement(driver, fc.utobj().getElement(driver, report_page.selectVisitFormName)
				.findElement(By.xpath("//li/label[contains(text () , '" + visitFormName + "')]")));

		fc.utobj().sendKeys(driver, report_page.frequencyDateFrom, scheduleDate);
		fc.utobj().sendKeys(driver, report_page.frequencyDateTo, scheduleDate);
	}

	public void filterAtReportsPageDivisionSelect(WebDriver driver, String divisionName) throws Exception {
		FieldOpsReportsPage report_page = new FieldOpsReportsPage(driver);
		fc.utobj().setToDefault(driver, report_page.divisionSelect);
		fc.utobj().selectValFromMultiSelect(driver, report_page.divisionSelect, fc.utobj()
				.getElement(driver, report_page.divisionSelect).findElement(By.xpath("./div/div/input")),
				divisionName);
	}

	public void filterAtReportsFranchiseId(WebDriver driver, String franchiseId) throws Exception {
		FieldOpsReportsPage report_page = new FieldOpsReportsPage(driver);
		fc.utobj().setToDefault(driver, report_page.franchiseSelect);
		fc.utobj().selectValFromMultiSelect(driver, report_page.franchiseSelect, fc.utobj()
				.getElement(driver, report_page.franchiseSelect).findElement(By.xpath("./div/div/input")),
				franchiseId);

	}

	public void filterAtReportsConsultant(WebDriver driver, String consultant) throws Exception {
		FieldOpsReportsPage report_page = new FieldOpsReportsPage(driver);
		fc.utobj().setToDefault(driver, report_page.consultantSelect);
		fc.utobj().selectValFromMultiSelect(driver, report_page.consultantSelect, fc.utobj()
				.getElement(driver, report_page.consultantSelect).findElement(By.xpath("./div/div/input")),
				consultant);
	}

	public void filterAtReportsStatus(WebDriver driver, String status) throws Exception {
		FieldOpsReportsPage report_page = new FieldOpsReportsPage(driver);
		fc.utobj().setToDefault(driver, report_page.selectStatus);
		fc.utobj()
				.selectValFromMultiSelect(driver, report_page.selectStatus, fc.utobj()
						.getElement(driver, report_page.selectStatus).findElement(By.xpath("./div/div/input")),
						status);
	}

	@Test(groups = { "fieldopsReports" })

	@TestCase(createdOn = "2017-08-01", updatedOn = "2017-08-01", testCaseDescription = "Click Over Visit Form Details Report", testCaseId = "TC_Fieldops_Report_01")
	private void fieldopsReports01() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		// Map<String,String> dataSet = fc.utobj().readTestData("fieldops",
		// testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			fc.utobj().printTestStep("Navigate To FieldOps > Reports Page > Visit Form Details Report");
			fc.fieldops().fieldops_common().fieldopsReports(driver);
			rpPageTest.navigateToSessionSizePage(driver);
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//a[contains(text () ,'Visit Form Details Report')]"));
			rpPageTest.setSelectAllMultiDropDown(driver);
			rpPageTest.navigateToSessionSizePage(driver);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "fieldopsReports" })

	@TestCase(createdOn = "2017-08-01", updatedOn = "2017-08-01", testCaseDescription = "Click Over Visit Summary Report", testCaseId = "TC_Fieldops_Report_02")
	private void fieldopsReports02() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		// Map<String,String> dataSet = fc.utobj().readTestData("fieldops",
		// testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			fc.utobj().printTestStep("Navigate To FieldOps > Reports Page > Visit Summary Report");
			fc.fieldops().fieldops_common().fieldopsReports(driver);
			rpPageTest.navigateToSessionSizePage(driver);
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//a[contains(text () ,'Visit Summary Report')]"));
			rpPageTest.setSelectAllMultiDropDown(driver);
			rpPageTest.navigateToSessionSizePage(driver);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "fieldopsReports" })

	@TestCase(createdOn = "2017-08-01", updatedOn = "2017-08-01", testCaseDescription = "Click Over Store Performance Report", testCaseId = "TC_Fieldops_Report_03")
	private void fieldopsReports03() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		// Map<String,String> dataSet = fc.utobj().readTestData("fieldops",
		// testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			fc.utobj().printTestStep("Navigate To FieldOps > Reports Page > Store Performance Report");
			fc.fieldops().fieldops_common().fieldopsReports(driver);
			rpPageTest.navigateToSessionSizePage(driver);
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//a[contains(text () ,'Store Performance Report')]"));
			rpPageTest.setSelectAllMultiDropDown(driver);
			rpPageTest.navigateToSessionSizePage(driver);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "fieldopsReports" })

	@TestCase(createdOn = "2017-08-01", updatedOn = "2017-08-01", testCaseDescription = "Click Over Outstanding Action Report", testCaseId = "TC_Fieldops_Report_04")
	private void fieldopsReports04() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		// Map<String,String> dataSet = fc.utobj().readTestData("fieldops",
		// testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			fc.utobj().printTestStep("Navigate To FieldOps > Reports Page > Outstanding Action Report");
			fc.fieldops().fieldops_common().fieldopsReports(driver);
			rpPageTest.navigateToSessionSizePage(driver);
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//a[contains(text () ,'Outstanding Action Report')]"));
			rpPageTest.setSelectAllMultiDropDown(driver);
			rpPageTest.navigateToSessionSizePage(driver);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "fieldopsReports" })

	@TestCase(createdOn = "2017-08-01", updatedOn = "2017-08-01", testCaseDescription = "Click Over Form Level Performance Report", testCaseId = "TC_Fieldops_Report_05")
	private void fieldopsReports05() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		// Map<String,String> dataSet = fc.utobj().readTestData("fieldops",
		// testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			fc.utobj().printTestStep("Navigate To FieldOps > Reports Page > Form Level Performance Report");
			fc.fieldops().fieldops_common().fieldopsReports(driver);
			rpPageTest.navigateToSessionSizePage(driver);
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//a[contains(text () ,'Form Level Performance Report')]"));
			rpPageTest.setSelectAllMultiDropDown(driver);
			rpPageTest.navigateToSessionSizePage(driver);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "fieldopsReports" })

	@TestCase(createdOn = "2017-08-01", updatedOn = "2017-08-01", testCaseDescription = "Click Over Question Level Performance Report", testCaseId = "TC_Fieldops_Report_06")
	private void fieldopsReports06() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		// Map<String,String> dataSet = fc.utobj().readTestData("fieldops",
		// testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			fc.utobj().printTestStep("Navigate To FieldOps > Reports Page > Question Level Performance Report");
			fc.fieldops().fieldops_common().fieldopsReports(driver);
			rpPageTest.navigateToSessionSizePage(driver);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//a[contains(text () ,'Question Level Performance Report')]"));
			rpPageTest.setSelectAllMultiDropDown(driver);
			rpPageTest.navigateToSessionSizePage(driver);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "fieldopsReports" })

	@TestCase(createdOn = "2017-08-01", updatedOn = "2017-08-01", testCaseDescription = "Click Over Corrective Action Plan(CAP) Report", testCaseId = "TC_Fieldops_Report_07")
	private void fieldopsReports07() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		// Map<String,String> dataSet = fc.utobj().readTestData("fieldops",
		// testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			fc.utobj().printTestStep("Navigate To FieldOps > Reports Page > Corrective Action Plan(CAP) Report");
			fc.fieldops().fieldops_common().fieldopsReports(driver);
			rpPageTest.navigateToSessionSizePage(driver);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//a[contains(text () ,'Corrective Action Plan(CAP) Report')]"));
			rpPageTest.setSelectAllMultiDropDown(driver);
			rpPageTest.navigateToSessionSizePage(driver);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "fieldopsReports" })

	@TestCase(createdOn = "2017-08-01", updatedOn = "2017-08-01", testCaseDescription = "Click Over Delta Report", testCaseId = "TC_Fieldops_Report_08")
	private void fieldopsReports08() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		// Map<String,String> dataSet = fc.utobj().readTestData("fieldops",
		// testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			fc.utobj().printTestStep("Navigate To FieldOps > Reports Page > Delta Report");
			fc.fieldops().fieldops_common().fieldopsReports(driver);
			rpPageTest.navigateToSessionSizePage(driver);
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//a[contains(text () ,'Delta Report')]"));
			rpPageTest.setSelectAllMultiDropDown(driver);
			rpPageTest.navigateToSessionSizePage(driver);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "fieldopsReports" })

	@TestCase(createdOn = "2017-08-01", updatedOn = "2017-08-01", testCaseDescription = "Click Over Visit Timeline Report", testCaseId = "TC_Fieldops_Report_09")
	private void fieldopsReports09() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		// Map<String,String> dataSet = fc.utobj().readTestData("fieldops",
		// testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			fc.utobj().printTestStep("Navigate To FieldOps > Reports Page > Visit Timeline Report");
			fc.fieldops().fieldops_common().fieldopsReports(driver);
			rpPageTest.navigateToSessionSizePage(driver);
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//a[contains(text () ,'Visit Timeline Report')]"));
			rpPageTest.setSelectAllMultiDropDown(driver);
			rpPageTest.navigateToSessionSizePage(driver);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "fieldopsReports" })

	@TestCase(createdOn = "2017-08-01", updatedOn = "2017-08-01", testCaseDescription = "Click Over Visit Response Analysis Report", testCaseId = "TC_Fieldops_Report_10")
	private void fieldopsReports10() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		// Map<String,String> dataSet = fc.utobj().readTestData("fieldops",
		// testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			fc.utobj().printTestStep("Navigate To FieldOps > Reports Page > Visit Response Analysis Report");
			fc.fieldops().fieldops_common().fieldopsReports(driver);
			rpPageTest.navigateToSessionSizePage(driver);
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//a[contains(text () ,'Visit Response Analysis Report')]"));
			rpPageTest.setSelectAllMultiDropDown(driver);
			rpPageTest.navigateToSessionSizePage(driver);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "fieldopsReports" })

	@TestCase(createdOn = "2017-08-01", updatedOn = "2017-08-01", testCaseDescription = "Click Over Location Not Visited Report", testCaseId = "TC_Fieldops_Report_11")
	private void fieldopsReports11() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		// Map<String,String> dataSet = fc.utobj().readTestData("fieldops",
		// testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			fc.utobj().printTestStep("Navigate To FieldOps > Reports Page > Location Not Visited Report");
			fc.fieldops().fieldops_common().fieldopsReports(driver);
			rpPageTest.navigateToSessionSizePage(driver);
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//a[contains(text () ,'Location Not Visited Report')]"));
			rpPageTest.setSelectAllMultiDropDown(driver);
			rpPageTest.navigateToSessionSizePage(driver);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "fieldopsReports" })

	@TestCase(createdOn = "2017-08-01", updatedOn = "2017-08-01", testCaseDescription = "Click Over Picture Report", testCaseId = "TC_Fieldops_Report_12")
	private void fieldopsReports12() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		// Map<String,String> dataSet = fc.utobj().readTestData("fieldops",
		// testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			fc.utobj().printTestStep("Navigate To FieldOps > Reports Page > Picture Report");
			fc.fieldops().fieldops_common().fieldopsReports(driver);
			rpPageTest.navigateToSessionSizePage(driver);
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//a[contains(text () ,'Picture Report')]"));
			rpPageTest.setSelectAllMultiDropDown(driver);
			rpPageTest.navigateToSessionSizePage(driver);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "fieldopsReports" })

	@TestCase(createdOn = "2017-08-01", updatedOn = "2017-08-01", testCaseDescription = "Click Over Form Answer Report", testCaseId = "TC_Fieldops_Report_13")
	private void fieldopsReports13() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		// Map<String,String> dataSet = fc.utobj().readTestData("fieldops",
		// testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			fc.utobj().printTestStep("Navigate To FieldOps > Reports Page > Form Answer Report");
			fc.fieldops().fieldops_common().fieldopsReports(driver);
			rpPageTest.navigateToSessionSizePage(driver);
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//a[contains(text () ,'Form Answer Report')]"));
			rpPageTest.setSelectAllMultiDropDown(driver);
			rpPageTest.navigateToSessionSizePage(driver);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}
}
