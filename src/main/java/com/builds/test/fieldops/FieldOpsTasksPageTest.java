package com.builds.test.fieldops;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.Reporter;
import org.testng.annotations.Test;

import com.builds.test.admin.AdminAreaRegionAddAreaRegionPageTest;
import com.builds.test.admin.AdminConfigurationConfigureStoreTypePageTest;
import com.builds.test.admin.AdminDivisionAddDivisionPageTest;
import com.builds.test.admin.AdminFranchiseLocationAddFranchiseLocationPageTest;
import com.builds.test.admin.AdminHiddenLinksConfigureFieldOpsSettingsPageTest;
import com.builds.test.admin.AdminUsersManageCorporateUsersAddCorporateUserPageTest;
import com.builds.test.admin.AdminUsersManageDivisionalUsersAddDivisionalUsersPageTest;
import com.builds.test.admin.AdminUsersManageManageFranchiseUsersPageTest;
import com.builds.test.admin.AdminUsersManageRegionalUsersAddRegionalUserPageTest;
import com.builds.test.common.CorporateUser;
import com.builds.uimaps.fieldops.FieldOpsHomePage;
import com.builds.uimaps.fieldops.FieldOpsTasksPage;
import com.builds.uimaps.fieldops.FieldOpsVisitsPage;
import com.builds.utilities.FranconnectUtil;
import com.builds.utilities.TestCase;

public class FieldOpsTasksPageTest {
	FranconnectUtil fc = new FranconnectUtil();

	@Test(groups = { "fieldops","fieldops0808" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Add , View And Delete Task At FieldOps Tasks Page", testCaseId = "TC_67_Add_Task")
	private void addTask() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("fieldops", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			FieldOpsTasksPage pobj = new FieldOpsTasksPage(driver);

			fc.utobj().printTestStep("Navigate To Admin > Franchise Location > Add Franchise Location");
			fc.utobj().printTestStep("Add Franchise Location");

			String regionName = fc.utobj().generateTestData("Test");
			String storeType = fc.utobj().generateTestData("Test");
			String franchiseId = fc.utobj().generateTestData("Test");
			AdminFranchiseLocationAddFranchiseLocationPageTest franchise = new AdminFranchiseLocationAddFranchiseLocationPageTest();
			franchiseId = franchise.addFranchiseLocation_All(driver, franchiseId, regionName, storeType);

			fc.utobj().printTestStep("Navigate To Admin > Users > Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");

			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUser = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			String emailId = "fieldopsautomation@staffex.com";
			CorporateUser corpUser = new CorporateUser();
			corpUser.setConsultant("Y");
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporateUser.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			fc.utobj().printTestStep("Navigate To Admin > FieldOps > Action Library");
			fc.utobj().printTestStep("Add Action Item");

			AdminFieldOpsActionLibraryPageTest actionPage = new AdminFieldOpsActionLibraryPageTest();
			String subject = actionPage.addActionItem(driver, dataSet);

			fc.utobj().printTestStep("Navigate To FieldOps > Tasks Page");
			fc.utobj().printTestStep("Add Task");
			fc.fieldops().fieldops_common().fieldopsTasks(driver);
			fc.utobj().clickElement(driver, pobj.addTask);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().selectDropDown(driver, pobj.selectFranchiseeId, franchiseId);

			List<WebElement> list = pobj.linkToVisit;

			if (fc.utobj().isSelected(driver,list.get(0))) {
				// do nothing
			} else {
				fc.utobj().clickElement(driver, list.get(0));
			}

			fc.utobj().selectValFromMultiSelectRadioBtn(driver, pobj.subject, subject.trim());
			fc.utobj().sendKeys(driver, pobj.actionRequired, dataSet.get("actionRequired"));

			String taskStatus = dataSet.get("taskStatus");

			fc.utobj().selectDropDown(driver, pobj.taskStatus, taskStatus);

			String futureDate = fc.utobj().getFutureDateUSFormat(20);
			fc.utobj().sendKeys(driver, pobj.dueDate, futureDate);

			fc.utobj().selectDropDown(driver, pobj.actionBy, corpUser.getuserFullName());
			fc.utobj().selectDropDown(driver, pobj.priority, dataSet.get("priority"));
			fc.utobj().clickElement(driver, pobj.submit);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Verify The Add Task");
			setToDefaultFilter(driver);
			filterByFranchiseID(driver, franchiseId);

			boolean isSubjectPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//*[@id='displayDataTd']//*[contains(text () , '" + subject + "')]");

			if (isSubjectPresent == false) {
				fc.utobj().throwsException("was not able to verify subject of task");
			}

			boolean isFranchiseIdPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//*[@id='displayDataTd']//*[contains(text () , '" + subject
							+ "')]/ancestor::tr/td/a[contains(text () , '" + franchiseId + "')]");

			if (isFranchiseIdPresent == false) {
				fc.utobj().throwsException("was not able to verify Franchise ID of task");
			}

			boolean isAssignToPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//*[@id='displayDataTd']//*[contains(text () , '" + subject
							+ "')]/ancestor::tr/td[contains(text () , '" + corpUser.getuserFullName() + "')]");

			if (isAssignToPresent == false) {
				fc.utobj().throwsException("was not able to verify Assign To of task");
			}

			boolean isStatusPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//*[@id='displayDataTd']//*[contains(text () , '"
					+ subject + "')]/ancestor::tr/td[contains(text () , '" + dataSet.get("taskStatus") + "')]");

			if (isStatusPresent == false) {
				fc.utobj().throwsException("was not able to verify subject of task");
			}

			fc.utobj().printTestStep("View Task");
			fc.utobj().actionImgOption(driver, subject, "View");
			fc.commonMethods().switch_cboxIframe_frameId(driver);

			fc.utobj().printTestStep("Verify View Task Detail");

			String textSubject = fc.utobj().getElementByXpath(driver, ".//*[.=" + "'" + subject + "'" + "]").getText()
					.trim();
			if (textSubject.equalsIgnoreCase(subject)) {
			} else {
				fc.utobj().throwsException("Was not able to view the task");
			}

			fc.utobj().clickElement(driver, pobj.closeBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Delete Task");
			fc.utobj().actionImgOption(driver, subject, "Delete");
			fc.utobj().acceptAlertBox(driver);

			fc.utobj().printTestStep("Verify The Deleted Task");
			filterByFranchiseID(driver, franchiseId);

			boolean istTextPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//td[contains(text () , 'No records found.')]");
			if (istTextPresent == false) {
				fc.utobj().throwsException("Was not able to delete a Task");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	public void addTask(WebDriver driver, Map<String, String> config, Map<String, String> dataSet, String franchiseId,
			String subject, String userName) throws Exception {

		String testCaseId = "TC_Add_Task_FiledOps";

		if (fc.utobj().validate(testCaseId)) {
			try {
				FieldOpsTasksPage pobj = new FieldOpsTasksPage(driver);

				fc.fieldops().fieldops_common().fieldopsTasks(driver);
				fc.utobj().clickElement(driver, pobj.addTask);

				fc.commonMethods().switch_cboxIframe_frameId(driver);
				fc.utobj().selectDropDown(driver, pobj.selectFranchiseeId, franchiseId);
				List<WebElement> list = pobj.linkToVisit;
				if (fc.utobj().isSelected(driver,list.get(0))) {
				} else {
					fc.utobj().clickElement(driver, list.get(0));
				}
				fc.utobj().selectValFromMultiSelectRadioBtn(driver, pobj.subject, subject.trim());
				fc.utobj().sendKeys(driver, pobj.actionRequired, dataSet.get("actionRequired"));
				fc.utobj().selectDropDown(driver, pobj.taskStatus, dataSet.get("taskStatus"));

				String curDate = fc.utobj().getCurrentDateUSFormat();
				fc.utobj().sendKeys(driver, pobj.dueDate, curDate);

				fc.utobj().selectDropDown(driver, pobj.actionBy, userName);
				fc.utobj().selectDropDown(driver, pobj.priority, dataSet.get("priority"));
				fc.utobj().clickElement(driver, pobj.submit);
				fc.utobj().switchFrameToDefault(driver);

				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException("was not able to add Task :: Field Ops > Tasks");

		}
	}

	public void addTaskIntoVisit(WebDriver driver, Map<String, String> config, Map<String, String> dataSet,
			String franchiseId, String subject, String userName, String visitId) throws Exception {

		String testCaseId = "TC_Add_Task_Into_Visit_FieldOps";

		if (fc.utobj().validate(testCaseId)) {
			try {
				FieldOpsTasksPage pobj = new FieldOpsTasksPage(driver);

				fc.fieldops().fieldops_common().fieldopsTasks(driver);

				fc.utobj().clickElement(driver, pobj.addTask);

				fc.commonMethods().switch_cboxIframe_frameId(driver);

				fc.utobj().selectDropDown(driver, pobj.selectFranchiseeId, franchiseId);

				List<WebElement> list = pobj.linkToVisit;
				if (fc.utobj().isSelected(driver,list.get(1))) {
					// do nothing
				} else {
					fc.utobj().clickElement(driver, list.get(1));
				}

				fc.utobj().selectDropDown(driver, pobj.selectVisitID, visitId);
				fc.utobj().selectValFromMultiSelectRadioBtn(driver, pobj.subject, subject.trim());
				fc.utobj().sendKeys(driver, pobj.actionRequired, dataSet.get("actionRequired"));
				fc.utobj().selectDropDown(driver, pobj.taskStatus, dataSet.get("taskStatus"));

				String curDate = fc.utobj().getCurrentDateUSFormat();
				fc.utobj().sendKeys(driver, pobj.dueDate, curDate);

				fc.utobj().selectDropDown(driver, pobj.actionBy, userName);
				fc.utobj().selectDropDown(driver, pobj.priority, dataSet.get("priority"));
				fc.utobj().clickElement(driver, pobj.submit);
				fc.utobj().switchFrameToDefault(driver);

				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException("was not able to Add Task In Visit:: FieldOps > Tasks");
		}
	}

	@Test(groups = { "fieldops", "fieldops0808" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify Change Status Of Task At FieldOps Tasks Page", testCaseId = "TC_70_Change_Status")
	private void changeStatus() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("fieldops", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			FieldOpsTasksPage pobj = new FieldOpsTasksPage(driver);

			fc.utobj().printTestStep("Navigate To Admin > Franchise Location > Add Franchise Location");
			fc.utobj().printTestStep("Add Franchise Location");

			String regionName = fc.utobj().generateTestData("Test");
			String storeType = fc.utobj().generateTestData("Test");
			String franchiseId = fc.utobj().generateTestData("Test");
			AdminFranchiseLocationAddFranchiseLocationPageTest franchise = new AdminFranchiseLocationAddFranchiseLocationPageTest();
			franchiseId = franchise.addFranchiseLocation_All(driver, franchiseId, regionName, storeType);

			fc.utobj().printTestStep("Navigate To Admin > Users > Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");

			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUser = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			String emailId = "fieldopsautomation@staffex.com";
			CorporateUser corpUser = new CorporateUser();
			corpUser.setConsultant("Y");
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporateUser.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			fc.utobj().printTestStep("Navigate To Admin > FieldOps > Action Library");
			fc.utobj().printTestStep("Add Action Item");
			AdminFieldOpsActionLibraryPageTest actionPage = new AdminFieldOpsActionLibraryPageTest();
			String subject = actionPage.addActionItem(driver, dataSet);

			fc.utobj().printTestStep("Navigate To FieldOps > Tasks Page");
			fc.utobj().printTestStep("Add Task");
			addTask(driver, config, dataSet, franchiseId, subject, corpUser.getuserFullName());

			fc.utobj().printTestStep("Verify The Task's More Link Redirection At Home Page");
			fc.utobj().printTestStep("Navigate To Home Page");
			FieldOpsHomePage home_page = new FieldOpsHomePage(driver);
			fc.utobj().clickElement(driver, home_page.homePageLnk);

			try {
				fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
						".//*[contains(text () , '" + subject + "')]/preceding-sibling::td/a"));
				fc.commonMethods().switch_cboxIframe_frameId(driver);

				boolean isSubjectPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
						".//*[contains(text () , 'Subject')]/ancestor::tr/td[contains(text () , '" + subject + "')]");

				if (isSubjectPresent == false) {
					fc.utobj().throwsException("was not able to verify Subject Of Task");
				}

				fc.utobj().clickElement(driver, pobj.closeBtn);
				fc.utobj().switchFrameToDefault(driver);

				fc.utobj().printTestStep("Verify The Franchise Id Redirection");
				fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//*[contains(text () , '"
						+ subject + "')]/following-sibling::td/a[.='" + franchiseId + "']"));

				boolean isTextIsPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
						".//td[contains(text () , 'Store Type')]/following-sibling::td[contains(text () , '" + storeType
								+ "')]");
				if (isTextIsPresent == false) {
					fc.utobj().throwsException("was not able to verify the Franchise Redirection");
				}

				fc.fieldops().fieldops_common().fieldopsTasks(driver);

			} catch (Exception e) {
				WebElement element = fc.utobj().getElementByXpath(driver,
						".//div[contains(text () ,'Task')]/ancestor::tr/td/div/div/a[.='More']");
				fc.utobj().clickElement(driver, element);
				setToDefaultFilter(driver);
				filterByFranchiseID(driver, franchiseId);
				boolean isSubjectPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[contains(text () , '" + subject + "')]");
				if (isSubjectPresent == false) {
					fc.utobj().throwsException("was not able to verify subject of task");
				}
			}

			fc.utobj().clickElement(driver, pobj.tasksPageLnk);
			setToDefaultFilter(driver);
			filterByFranchiseID(driver, franchiseId);

			fc.utobj().printTestStep("Change Status Of Task");

			String text = fc.utobj()
					.getElementByXpath(driver, ".//*[.=" + "'" + subject + "'" + "]/../../td/div[@id='menuBar']/layer")
					.getAttribute("id");
			String alterText = text.replace("Actions_dynamicmenu", "");
			alterText = alterText.replace("Bar", "");
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//*[.=" + "'" + subject + "'" + "]/../../td/div[@id='menuBar']/layer/a/img"));

			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//*[@id='Actions_dynamicmenu" + alterText + "Menu']/span[contains(text () , 'Change Status')]"));
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().selectDropDown(driver, pobj.taskStatus, dataSet.get("taskStatusChanged"));
			fc.utobj().clickElement(driver, pobj.statusModifyBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Verify Change Status Of Task");
			setToDefaultFilter(driver);
			filterByFranchiseID(driver, franchiseId);

			String textStatus = fc.utobj()
					.getElementByXpath(driver, ".//*[.=" + "'" + subject + "'" + "]/../following-sibling::td[4]")
					.getText().trim();
			System.out.println(textStatus);
			if (textStatus.equalsIgnoreCase(dataSet.get("taskStatusChanged"))) {
				System.out.println("Sucessfully cahnged Status");
			} else {
				fc.utobj().throwsException("Was not able to change status of task");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "fieldops", "fieldops0808"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify Link To Visit Task At Fieldops Tasks Page", testCaseId = "TC_72_Link_To_Visit_Task")
	private void linkToVisit() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("fieldops", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			FieldOpsTasksPage pobj = new FieldOpsTasksPage(driver);

			FieldOpsVisitsPageTest visitPage = new FieldOpsVisitsPageTest();

			fc.utobj().printTestStep("Navigate To Admin > Hidden Links > Configure FieldOps Settings Page");
			fc.utobj().printTestStep("Configure Field Ops Settings Log Private Comment Yes");

			AdminHiddenLinksConfigureFieldOpsSettingsPageTest privateComments = new AdminHiddenLinksConfigureFieldOpsSettingsPageTest();
			privateComments.configureFieldOpsSettingsLogPrivateCommentYes(driver, config);

			fc.utobj().printTestStep("Navigate To Admin > Franchise Location > Add Franchise Location");
			fc.utobj().printTestStep("Add Franchise Location");
			String regionName = fc.utobj().generateTestData("Test");
			String storeType = fc.utobj().generateTestData("Test");
			String franchiseId = fc.utobj().generateTestData("Test");
			AdminFranchiseLocationAddFranchiseLocationPageTest franchise = new AdminFranchiseLocationAddFranchiseLocationPageTest();
			franchiseId = franchise.addFranchiseLocation_All(driver, franchiseId, regionName, storeType);

			fc.utobj().printTestStep("Navigate To Admin > Users > Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUser = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			String emailId = "fieldopsautomation@staffex.com";
			CorporateUser corpUser = new CorporateUser();
			corpUser.setConsultant("Y");
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporateUser.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			fc.utobj().printTestStep("Navigate To Admin > Fieldops > Manage Visit Form");
			fc.utobj().printTestStep("Add Private Visit Form");
			AdminFieldOpsManageVisitFormPageTest visitFormPage = new AdminFieldOpsManageVisitFormPageTest();
			String visitFormName = visitFormPage.addVisitFormPrivate(driver, dataSet);

			fc.utobj().printTestStep("Add Question In Visit Form");
			fc.utobj().clickElement(driver, pobj.addQuestion);

			AdminFieldOpsManageVisitFormCustomizeVisitFormPageTest customizeVisit = new AdminFieldOpsManageVisitFormCustomizeVisitFormPageTest();
			customizeVisit.addQuestion(driver, dataSet);

			fc.utobj().clickElement(driver, pobj.finishBtn);

			fc.utobj().printTestStep("Create Visit");
			FieldOpsHomePageTest fieldOpsHomeP = new FieldOpsHomePageTest();
			visitFormName = fieldOpsHomeP.createVisitAtHomePage(driver, config, visitFormName, franchiseId,
					corpUser.getuserFullName());

			String visitId = fc.utobj()
					.getElementByXpath(driver,
							".//tr[2]/td[contains(text () ," + "'" + visitFormName + "'" + ")]/../td[1]/a")
					.getText().trim();

			fc.utobj().printTestStep("Link A Task Into Visit");
			fc.fieldops().fieldops_common().fieldopsTasks(driver);
			fc.utobj().clickElement(driver, pobj.addTask);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().selectDropDown(driver, pobj.selectFranchiseeId, franchiseId);

			List<WebElement> list = pobj.linkToVisit;
			if (fc.utobj().isSelected(driver,list.get(1))) {
				// do nothing
			} else {
				fc.utobj().clickElement(driver, list.get(1));
			}

			fc.utobj().selectDropDown(driver, pobj.selectVisitID, visitId);
			
			//fc.utobj().selectDropDown(driver, pobj.subject, "Add New Action");
			fc.utobj().selectValFromMultiSelectRadioBtn(driver, pobj.subject, "Add New Action");
			
			String addNewAction = fc.utobj().generateTestData(dataSet.get("addNewAction"));
			fc.utobj().sendKeys(driver, pobj.addNewAction, addNewAction);

			fc.utobj().sendKeys(driver, pobj.actionRequired, dataSet.get("actionRequired"));
			fc.utobj().selectDropDown(driver, pobj.taskStatus, dataSet.get("taskStatus"));

			// Due Date
			String curDate = fc.utobj().getCurrentDateUSFormat();
			fc.utobj().sendKeys(driver, pobj.dueDate, curDate);

			fc.utobj().selectDropDown(driver, pobj.actionBy, corpUser.getuserFullName());

			fc.utobj().selectDropDown(driver, pobj.priority, dataSet.get("priority"));
			fc.utobj().clickElement(driver, pobj.submit);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().clickElement(driver, pobj.visitBtn);

			fc.utobj().printTestStep("Verify Link To Visit A Task");

			// save view
			visitPage.defaultViewByFranchiseId(driver, franchiseId);

			fc.utobj().clickElement(driver, fc.utobj().getElementByLinkText(driver, visitId));

			String alterText = fc.utobj().getElementByXpath(driver, ".//table[@class='bText12gr']//tr/td/label")
					.getAttribute("id").trim();
			alterText = alterText.replace("_0", "");

			fc.utobj().sendKeys(driver,
					fc.utobj().getElementByXpath(driver, ".//textarea[@id='Response_" + alterText + "']"),
					"It is a sample answer");
			fc.utobj().sendKeys(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='private" + alterText + "']"),
					"It is a private Answer");
			fc.utobj().clickElement(driver, pobj.nextBtnSubmit);

			String textSubject = fc.utobj().getElementByID(driver, "subject0").getAttribute("value").trim();

			if (textSubject.equalsIgnoreCase(addNewAction.trim())) {
				System.out.println("sucessfully Add Task");
			} else {
				fc.utobj().throwsException("Was Not able to Add Task in A Visit");
			}

			fc.utobj().clickElement(driver, pobj.submitBtn);

			String textTask = fc.utobj()
					.getElementByXpath(driver, ".//tbody/tr/td[contains(text () ," + "'" + addNewAction + "'" + ")]")
					.getText().trim();

			if (textTask.equalsIgnoreCase(addNewAction.trim())) {
				System.out.println("sucessfully Add Task");
			} else {
				fc.utobj().throwsException("Was Not able to Add Task in A Visit");
			}

			fc.utobj().clickElement(driver, pobj.okBtn);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	public String linkToVisit(WebDriver driver, Map<String, String> config, Map<String, String> dataSet,
			String franchiseId, String visitId, String franUserName) throws Exception {

		String testCaseId = "TC_link_To_Visit_FieldOps";
		String addNewAction = null;

		if (fc.utobj().validate(testCaseId)) {
			try {
				// SUbject "Add New Action Option" and Link to Visit a task
				FieldOpsTasksPage pobj = new FieldOpsTasksPage(driver);
				fc.fieldops().fieldops_common().fieldopsTasks(driver);
				fc.utobj().clickElement(driver, pobj.addTask);
				fc.commonMethods().switch_cboxIframe_frameId(driver);
				fc.utobj().selectDropDown(driver, pobj.selectFranchiseeId, franchiseId);
				List<WebElement> list = pobj.linkToVisit;

				if (fc.utobj().isSelected(driver,list.get(1))) {
					// do nothing
				} else {
					fc.utobj().clickElement(driver, list.get(1));
				}

				fc.utobj().selectDropDown(driver, pobj.selectVisitID, visitId);

				//fc.utobj().selectDropDown(driver, pobj.subject, "Add New Action");
				
				fc.utobj().selectValFromMultiSelectRadioBtn(driver, pobj.subject, "Add New Action");
				
				addNewAction = fc.utobj().generateTestData(dataSet.get("addNewAction"));
				fc.utobj().sendKeys(driver, pobj.addNewAction, addNewAction);
				fc.utobj().sendKeys(driver, pobj.actionRequired, dataSet.get("actionRequired"));
				fc.utobj().selectDropDown(driver, pobj.taskStatus, dataSet.get("taskStatus"));

				// Due Date
				String curDate = fc.utobj().getCurrentDateUSFormat();
				fc.utobj().sendKeys(driver, pobj.dueDate, curDate);

				fc.utobj().selectDropDown(driver, pobj.actionBy, franUserName);
				fc.utobj().selectDropDown(driver, pobj.priority, dataSet.get("priority"));
				fc.utobj().clickElement(driver, pobj.submit);

				fc.utobj().switchFrameToDefault(driver);

				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException("was not able to link task With Visit :: FieldOps > Visit");

		}
		return addNewAction;
	}

	@Test(groups = "fieldops")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify Add Task Link Should Be Visible and Clickable by Corporate User In FieldOps Page", testCaseId = "TC_82_Verify_Task_Link_Corporate_User")
	private void verifyTaskLinkForCorporateUser() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("fieldops", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {

			driver = fc.loginpage().login(driver);
			FieldOpsTasksPage pobj = new FieldOpsTasksPage(driver);

			fc.utobj().printTestStep("Navigate To Admin > Users > Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUser = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			String emailId = "fieldopsautomation@staffex.com";
			CorporateUser corpUser = new CorporateUser();
			corpUser.setConsultant("Y");
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporateUser.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			fc.home_page().logout(driver);

			fc.utobj().printTestStep("Login With Corporate User");
			fc.loginpage().loginWithParameter(driver, corpUser.getUserName(), corpUser.getPassword());

			fc.utobj().printTestStep("Navigate To FieldOps > Tasks Page");
			fc.fieldops().fieldops_common().fieldopsTasks(driver);

			fc.utobj().printTestStep("Verify Add Task Link");

			fc.utobj().clickElement(driver, pobj.addTask);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			pobj.selectFranchiseeId.isEnabled();
			fc.utobj().switchFrameToDefault(driver);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "fieldops")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify Add Task Link Should Be Visible and Clickable by Divisional User in FieldOps Page", testCaseId = "TC_83_Verify_Task_Link_Divisional_User")
	private void verifyTaskLinkForDivisionalUser() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("fieldops", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {

			driver = fc.loginpage().login(driver);
			FieldOpsTasksPage pobj = new FieldOpsTasksPage(driver);

			fc.utobj().printTestStep("Navigate To Admin > Division > Add Division");
			fc.utobj().printTestStep("Add Division");

			AdminDivisionAddDivisionPageTest addDiv = new AdminDivisionAddDivisionPageTest();
			String division_UsName = fc.utobj().generateTestData("Test");
			/* addDiv.addDivision(driver, config, dataSet, division_UsName); */

			addDiv.addDivision(driver, division_UsName);
			fc.utobj().printTestStep("Navigate To Admin > Users > Manage Divisional Users");
			fc.utobj().printTestStep("Add Divisional User");

			AdminUsersManageDivisionalUsersAddDivisionalUsersPageTest divUser = new AdminUsersManageDivisionalUsersAddDivisionalUsersPageTest();
			String divUserName = fc.utobj().generateTestData(dataSet.get("divUserName"));
			String password = "t0n1ght123";
			String emailId = "fieldopsautomation@staffex.com";
			divUser.addDivisionalUser(driver, divUserName, password, division_UsName, emailId);

			fc.home_page().logout(driver);

			fc.utobj().printTestStep("Login With Divisional User");
			fc.loginpage().loginWithParameter(driver, divUserName, password);
			fc.fieldops().fieldops_common().fieldopsTasks(driver);

			fc.utobj().printTestStep("Verify Task Link");
			fc.utobj().clickElement(driver, pobj.addTask);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			pobj.selectFranchiseeId.isEnabled();
			fc.utobj().switchFrameToDefault(driver);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "fieldops", "fieldops_5050" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify Add Task Link Should Be Visible and Clickable by Regional User in FieldOps Page", testCaseId = "TC_84_Verify_Task_Link_Regional_User")
	private void verifyTaskLinkForRegionalUser() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("fieldops", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {

			driver = fc.loginpage().login(driver);
			FieldOpsTasksPage pobj = new FieldOpsTasksPage(driver);

			fc.utobj().printTestStep("Navigate To Admin > Add Area/Region");
			fc.utobj().printTestStep("Add Region");

			AdminUsersManageRegionalUsersAddRegionalUserPageTest addRegionUser = new AdminUsersManageRegionalUsersAddRegionalUserPageTest();
			String regUserName = fc.utobj().generateTestData(dataSet.get("regUserName"));
			String password = "t0n1ght123";
			AdminAreaRegionAddAreaRegionPageTest addRegion = new AdminAreaRegionAddAreaRegionPageTest();
			String regionName = fc.utobj().generateTestData("TestRegion");
			addRegion.addAreaRegion(driver, regionName);

			fc.utobj().printTestStep("Navigate To Admin > Users > Manage Regional Users");
			fc.utobj().printTestStep("Add Regional User");
			String emailId = "fieldopsautomation@staffex.com";
			addRegionUser.addRegionalUser(driver, regUserName, password, regionName, emailId);

			fc.home_page().logout(driver);
			fc.utobj().printTestStep("Login With Regional User");
			fc.loginpage().loginWithParameter(driver, regUserName, password);

			fc.utobj().printTestStep("Verify Add Task Link");
			fc.fieldops().fieldops_common().fieldopsTasks(driver);

			fc.utobj().clickElement(driver, pobj.addTask);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			pobj.selectFranchiseeId.isEnabled();
			fc.utobj().switchFrameToDefault(driver);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "fieldops")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify Add Task Link Should Be Visible and Clickable by Franchise User in FieldOps Page", testCaseId = "TC_85_Verify_Task_Link_Franchise_User")
	private void verifyTaskLinkForFranchiseUser() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("fieldops", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {

			driver = fc.loginpage().login(driver);
			FieldOpsTasksPage pobj = new FieldOpsTasksPage(driver);

			fc.utobj().printTestStep("Navigate To Admin > Franchise Location > Add Franchise Location");
			fc.utobj().printTestStep("Add Franchise Location");
			AdminFranchiseLocationAddFranchiseLocationPageTest addFranLoc = new AdminFranchiseLocationAddFranchiseLocationPageTest();
			String franchiseId = fc.utobj().generateTestData("TestFId");
			String regionName = fc.utobj().generateTestData("TestRegName");
			String storeType = fc.utobj().generateTestData("TestStore");
			franchiseId = addFranLoc.addFranchiseLocation_All(driver, franchiseId, regionName, storeType);

			fc.utobj().printTestStep("Add Franchise User");
			AdminUsersManageManageFranchiseUsersPageTest franUser = new AdminUsersManageManageFranchiseUsersPageTest();
			String franUserName = fc.utobj().generateTestData(dataSet.get("franUserName"));
			String password = "t0n1ght123";
			String roleName = "Default Franchise Role";
			String emailId = "fieldopsautomation@staffex.com";
			franUser.addFranchiseUser(driver, franUserName, password, franchiseId, roleName, emailId);

			fc.home_page().logout(driver);
			fc.utobj().printTestStep("Login With Franchise User");
			fc.loginpage().loginWithParameter(driver, franUserName, password);

			fc.utobj().printTestStep("Navigate To FieldOps > Tasks Page");
			fc.utobj().printTestStep("Verify Add Task Link");
			fc.fieldops().fieldops_common().fieldopsTasks(driver);

			fc.utobj().clickElement(driver, pobj.addTask);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			pobj.selectFranchiseeId.isEnabled();
			fc.utobj().switchFrameToDefault(driver);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = {"fieldops"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify add task button in smart connect which is visible and Clickable by Corporate User In SmartConnect Module", testCaseId = "TC_94_Verify_Add_Task_Link_SmartConnect_CU")
	private void verifyTaskLinkSmartConnectCorporateUser() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());
		
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {

			driver = fc.loginpage().login(driver);
			FieldOpsTasksPage pobj = new FieldOpsTasksPage(driver);

			fc.utobj().printTestStep("Navigate To Admin > Users > Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUser = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			String emailId = "fieldopsautomation@staffex.com";
			CorporateUser corpUser = new CorporateUser();
			corpUser.setConsultant("Y");
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporateUser.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			fc.home_page().logout(driver);
			fc.utobj().printTestStep("Login With Corporate User");
			fc.loginpage().loginWithParameter(driver, corpUser.getUserName(), corpUser.getPassword());

			fc.utobj().printTestStep("Navigate To SmartConnect Module");
			fc.adminpage().openSmartConnectModulePage(driver);

			fc.utobj().printTestStep("Verify Add Task Link");
			fc.utobj().clickElement(driver, pobj.addTask);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			pobj.selectFranchiseeId.isEnabled();
			fc.utobj().switchFrameToDefault(driver);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = {"fieldops"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify Add Task Link Should Be Visible and Clickable by Divisional User in SmartConnect Module", testCaseId = "TC_95_Verify_Add_Task_Link_SmartConnect_DU")
	private void verifyTaskLinkSmartConnectDivisionalUser() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("fieldops", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {

			driver = fc.loginpage().login(driver);
			FieldOpsTasksPage pobj = new FieldOpsTasksPage(driver);

			fc.utobj().printTestStep("Navigate To Admin > Division > Add Division");
			fc.utobj().printTestStep("Add Division");
			AdminDivisionAddDivisionPageTest addDiv = new AdminDivisionAddDivisionPageTest();
			String division_UsName = fc.utobj().generateTestData("Test");
			/* addDiv.addDivision(driver, config, dataSet, division_UsName); */
			addDiv.addDivision(driver, division_UsName);
			fc.utobj().printTestStep("Navigate To Admin > Users > Manage Divisional Users");
			fc.utobj().printTestStep("Add Divisional User");

			AdminUsersManageDivisionalUsersAddDivisionalUsersPageTest divUser = new AdminUsersManageDivisionalUsersAddDivisionalUsersPageTest();
			String divUserName = fc.utobj().generateTestData(dataSet.get("divUserName"));
			String password = "t0n1ght123";
			String emailId = "fieldopsautomation@staffex.com";
			divUser.addDivisionalUser(driver, divUserName, password, division_UsName, emailId);

			fc.home_page().logout(driver);
			fc.utobj().printTestStep("Login With Divisional User");
			fc.loginpage().loginWithParameter(driver, divUserName, password);

			fc.utobj().printTestStep("Naviagate To SmartConnect Module");
			fc.adminpage().openSmartConnectModulePage(driver);

			fc.utobj().printTestStep("Verify Add Task Link");
			fc.utobj().clickElement(driver, pobj.addTask);

			fc.commonMethods().switch_cboxIframe_frameId(driver);

			pobj.selectFranchiseeId.isEnabled();

			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = {"fieldops"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify Add Task Link Should Be Visible and Clickable by Regional User in SmartConnect Module", testCaseId = "TC_96_Verify_Add_Task_Link_SmartConnect_RU")
	private void verifyTaskLinkSmartConnectRegionalUser() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("fieldops", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {

			driver = fc.loginpage().login(driver);
			FieldOpsTasksPage pobj = new FieldOpsTasksPage(driver);

			AdminUsersManageRegionalUsersAddRegionalUserPageTest addRegionUser = new AdminUsersManageRegionalUsersAddRegionalUserPageTest();
			String regUserName = fc.utobj().generateTestData(dataSet.get("regUserName"));
			String password = "t0n1ght123";

			AdminAreaRegionAddAreaRegionPageTest addRegion = new AdminAreaRegionAddAreaRegionPageTest();
			String regionName = fc.utobj().generateTestData("TestRegion");
			fc.utobj().printTestStep("Navigate To Admin > Add Area/Region");
			fc.utobj().printTestStep("Add Region");
			addRegion.addAreaRegion(driver, regionName);

			fc.utobj().printTestStep("Navigate To Admin > Users > Manage Regional Users");
			fc.utobj().printTestStep("Add Regional User");
			String emailId = "fieldopsautomation@staffex.com";
			addRegionUser.addRegionalUser(driver, regUserName, password, regionName, emailId);

			fc.home_page().logout(driver);
			fc.utobj().printTestStep("Login With Regional User");
			fc.loginpage().loginWithParameter(driver, regUserName, password);

			fc.utobj().printTestStep("Navigate To SmartConnect Module");
			fc.adminpage().openSmartConnectModulePage(driver);

			fc.utobj().printTestStep("Verify Add Task Link");
			fc.utobj().clickElement(driver, pobj.addTask);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			pobj.selectFranchiseeId.isEnabled();
			fc.utobj().switchFrameToDefault(driver);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "fieldops")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify Add Task Link Should Be Visible and Clickable by Franchise User in SmartConnet Module", testCaseId = "TC_97_Verify_Add_Task_Link_SmartConnect_FU")
	private void verifyTaskLinkSmartConnectFranchiseUser() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("fieldops", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {

			driver = fc.loginpage().login(driver);
			FieldOpsTasksPage pobj = new FieldOpsTasksPage(driver);

			fc.utobj().printTestStep("Navigate To Admin > Franchise Location > Add Franchise Location");
			fc.utobj().printTestStep("Add Franchise Location");

			AdminFranchiseLocationAddFranchiseLocationPageTest addFranLoc = new AdminFranchiseLocationAddFranchiseLocationPageTest();
			String franchiseId = fc.utobj().generateTestData("TestFId");
			String regionName = fc.utobj().generateTestData("TestRegName");
			String storeType = fc.utobj().generateTestData("TestStore");
			franchiseId = addFranLoc.addFranchiseLocation_All(driver, franchiseId, regionName, storeType);

			fc.utobj().printTestStep("Add Franchise User");
			AdminUsersManageManageFranchiseUsersPageTest franUser = new AdminUsersManageManageFranchiseUsersPageTest();
			String franUserName = fc.utobj().generateTestData(dataSet.get("franUserName"));
			String password = "t0n1ght123";
			String roleName = "Default Franchise Role";
			String emailId = "fieldopsautomation@staffex.com";
			franUser.addFranchiseUser(driver, franUserName, password, franchiseId, roleName, emailId);

			fc.home_page().logout(driver);
			fc.utobj().printTestStep("Login With Franchise User");
			fc.loginpage().loginWithParameter(driver, franUserName, password);

			fc.utobj().printTestStep("Navigate To Smart Connect Module");
			fc.home_page().openSmartConnectModule(driver);

			fc.utobj().printTestStep("Verify Add Task Link");

			try {
				if (fc.utobj().getElement(driver, pobj.addTask).isEnabled()) {
					fc.utobj().throwsException("Was not able to verify Add Task Link In Smart Connect");
				}

			} catch (Exception e) {

			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "fieldops", "fieldops0808"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify Task of private visit should be visible to franchise user in task tab And In Email Visit id should not visible to franchise", testCaseId = "TC_86_Verify_Task_Private_Visit")
	private void verifyTaskPrivateVisitFranchiseUser() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("fieldops", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			FieldOpsTasksPage pobj = new FieldOpsTasksPage(driver);
			String password = "ton1ght123";

			fc.utobj().printTestStep("Navigate To Admin > Hidden Links > Configure FieldOps Settings Page");
			fc.utobj().printTestStep("Configure Field Ops Settings Log Private Comment Yes");

			AdminHiddenLinksConfigureFieldOpsSettingsPageTest privateComments = new AdminHiddenLinksConfigureFieldOpsSettingsPageTest();
			privateComments.configureFieldOpsSettingsLogPrivateCommentYes(driver, config);

			fc.utobj().printTestStep("Navigate To Admin > Fieldops > Manage Visit Form");
			fc.utobj().printTestStep("Add Private Visit Form");
			AdminFieldOpsManageVisitFormPageTest visitFormPage = new AdminFieldOpsManageVisitFormPageTest();
			String visitFormName = visitFormPage.addVisitFormPrivate(driver, dataSet);

			fc.utobj().printTestStep("Add Question");
			fc.utobj().clickElement(driver, pobj.addQuestion);

			AdminFieldOpsManageVisitFormCustomizeVisitFormPageTest customizeVisit = new AdminFieldOpsManageVisitFormCustomizeVisitFormPageTest();
			customizeVisit.addQuestion(driver, dataSet);
			fc.utobj().clickElement(driver, pobj.finishBtn);

			fc.utobj().printTestStep("Navigate To Admin > Franchise Location > Add Franchise Location");
			fc.utobj().printTestStep("Add Franchise Location");
			String regionName = fc.utobj().generateTestData("Test");
			String storeType = fc.utobj().generateTestData("Test");
			String franchiseId = fc.utobj().generateTestData("Test");
			String firstName = fc.utobj().generateTestData("Fname");
			AdminFranchiseLocationAddFranchiseLocationPageTest franchise = new AdminFranchiseLocationAddFranchiseLocationPageTest();
			franchiseId = franchise.addFranchiseLocation_AllWithFName(driver, franchiseId, regionName, storeType,
					firstName);

			fc.utobj().printTestStep("Navigate To Admin > Users > Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUser = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			String emailId = "fieldopsautomation@staffex.com";
			CorporateUser corpUser = new CorporateUser();
			corpUser.setConsultant("Y");
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporateUser.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			fc.utobj().printTestStep("Add Franchise User");
			AdminUsersManageManageFranchiseUsersPageTest franUser = new AdminUsersManageManageFranchiseUsersPageTest();
			String FranUserName = fc.utobj().generateTestData("TestF");
			String roleName = "Default Franchise Role";
			franUser.addFranchiseUser(driver, FranUserName, password, franchiseId, roleName, emailId);

			fc.utobj().printTestStep("Navigate To FieldOps > Visits Page");
			fc.utobj().printTestStep("Create Visit");

			FieldOpsVisitsPageTest visitPage = new FieldOpsVisitsPageTest();
			visitPage.createVisitAtVisitPage(driver, config, dataSet, visitFormName, franchiseId,
					corpUser.getuserFullName());

			String visitId = fc.utobj()
					.getElementByXpath(driver,
							".//tr[2]/td[contains(text () ," + "'" + visitFormName + "'" + ")]/../td[1]/a")
					.getText().trim();
			String franUserName = firstName +" "+ firstName;

			fc.utobj().printTestStep("Link Task With Visit");
			String subject = linkToVisit(driver, config, dataSet, franchiseId, visitId, franUserName);

			fc.home_page().logout(driver);

			fc.utobj().printTestStep("Login With Franchise User");
			fc.loginpage().loginWithParameter(driver, FranUserName, password);

			fc.utobj().printTestStep("Navigate To FieldOps > Task Page");
			fc.fieldops().fieldops_common().fieldopsTasks(driver);

			fc.utobj().printTestStep("Verify Task");
			fc.utobj().clickElement(driver, pobj.showFilter);

			fc.utobj().clickElement(driver, fc.utobj().getElementByID(driver, "Button1"));
			fc.utobj().sendKeys(driver, fc.utobj().getElementByID(driver, "subject"), subject);
			if (!fc.utobj().isSelected(driver, pobj.lookInSubjectChck)) {
				fc.utobj().clickElement(driver, pobj.lookInSubjectChck);
			}

			fc.utobj().clickElement(driver, pobj.searchBtn);
			fc.utobj().clickElement(driver, pobj.hideFilter);

			fc.utobj().getElementByXpath(driver, ".//tr/td/a[.=" + "'" + subject + "'" + "]").isEnabled();
			String text = fc.utobj()
					.getElementByXpath(driver, ".//tr/td/a[.=" + "'" + subject + "'" + "]/../following-sibling::td[3]")
					.getText().trim();

			if (text.equalsIgnoreCase(dataSet.get("taskStatus"))) {
			} else {
				fc.utobj().throwsException("Was not able to verify task status at Franchise task Page");
			}

			fc.utobj().printTestStep("Verify The Visit Form And Visit Number");
			fc.utobj().actionImgOption(driver, subject, "View");
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			boolean isVisitPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//*[contains(text () , 'Visit No')]/../following-sibling::td[contains(text () ,'" + visitId
							+ "')]");
			if (isVisitPresent == true) {
				fc.utobj().throwsException("was not able to verify Visit Id");
			}

			boolean isVisitFormNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//*[contains(text () , 'Visit Form')]/../following-sibling::td[contains(text () ,'"
							+ visitFormName + "')]");
			if (isVisitFormNamePresent == true) {
				fc.utobj().throwsException("Was Not able to verify Visit FormName");
			}

			fc.utobj().clickElement(driver, pobj.closeBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Complete The Task");
			fc.utobj().actionImgOption(driver, subject, "Change Status");
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().selectDropDown(driver, pobj.selectTaskStatus, "Completed");
			fc.utobj().clickElement(driver, pobj.statusModifyBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Verify Visit Id and visit Form Name in email");
			String expectedSubject = "Operations Task Completion Alert";
			String expectedMessageBody = "Dear " + firstName;

			Map<String, String> mailData = new HashMap<String, String>();
			mailData = fc.utobj().readMailBox(expectedSubject, expectedMessageBody, emailId, "sdg@1a@Hfs");

			if (mailData.size() == 0 || mailData.get("mailBody").contains(visitId)) {

				fc.utobj().throwsException("was not able to verify that VisitId should not visible in email");
			}

			if (mailData.size() == 0 || mailData.get("mailBody").contains(visitFormName)) {

				fc.utobj().throwsException("was not able to verify that Visit Form should not visible in email");
			}

			if (mailData.size() == 0 || !mailData.get("mailBody").contains("Task Completion Alert")) {

				fc.utobj().throwsException("was not able to verify Mail After Completion Of Task");
			}

			fc.utobj().printTestStep("Verify Task Completion Mail For Consultant");

			Map<String, String> mailData1 = new HashMap<String, String>();

			expectedMessageBody = "Dear " + corpUser.getFirstName();

			mailData1 = fc.utobj().readMailBox(expectedSubject, expectedMessageBody, emailId, "sdg@1a@Hfs");

			if (mailData1.size() == 0 || mailData1.get("mailBody").contains(visitId)) {

				fc.utobj().throwsException(
						"was not able to verify that VisitId should not visible in email for consultant");
			}

			if (mailData1.size() == 0 || mailData1.get("mailBody").contains(visitFormName)) {

				fc.utobj().throwsException(
						"was not able to verify that Visit Form should not visible in email consultant");
			}

			if (mailData1.size() == 0 || !mailData1.get("mailBody").contains("Task Completion Alert")) {

				fc.utobj().throwsException("was not able to verify Mail After Completion Of Task consultant");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = {"fieldops" ,"fieldops0808"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify visit id for private visit task will not display to franchise user", testCaseId = "TC_87_Verify_Visit_ID_Private_Visit_Task")
	private void verifyVisitIDPrivateVisitTaskFranchiseUser() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("fieldops", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			FieldOpsTasksPage pobj = new FieldOpsTasksPage(driver);
			String password = "t0n1ght123";

			fc.utobj().printTestStep("Navigate To Admin > Hidden Links > Configure FieldOps Settings Page");
			fc.utobj().printTestStep("Configure Field Ops Settings Log Private Comment Yes");

			AdminHiddenLinksConfigureFieldOpsSettingsPageTest privateComments = new AdminHiddenLinksConfigureFieldOpsSettingsPageTest();
			privateComments.configureFieldOpsSettingsLogPrivateCommentYes(driver, config);

			fc.utobj().printTestStep("Navigate To Admin > Fieldops > Manage Visit Form");
			fc.utobj().printTestStep("Add Private Visit Form");
			AdminFieldOpsManageVisitFormPageTest visitFormPage = new AdminFieldOpsManageVisitFormPageTest();
			String visitFormName = visitFormPage.addVisitFormPrivate(driver, dataSet);

			fc.utobj().printTestStep("Add Question");
			fc.utobj().clickElement(driver, pobj.addQuestion);

			AdminFieldOpsManageVisitFormCustomizeVisitFormPageTest customizeVisit = new AdminFieldOpsManageVisitFormCustomizeVisitFormPageTest();
			customizeVisit.addQuestion(driver, dataSet);
			fc.utobj().clickElement(driver, pobj.finishBtn);

			fc.utobj().printTestStep("Navigate To Admin > Franchise Location > Add Franchise Location");
			fc.utobj().printTestStep("Add Franchise Location");
			String regionName = fc.utobj().generateTestData("Test");
			String storeType = fc.utobj().generateTestData("Test");
			String franchiseId = fc.utobj().generateTestData("Test");
			AdminFranchiseLocationAddFranchiseLocationPageTest franchise = new AdminFranchiseLocationAddFranchiseLocationPageTest();
			franchiseId = franchise.addFranchiseLocation_All(driver, franchiseId, regionName, storeType);

			fc.utobj().printTestStep("Navigate To Admin > Users > Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUser = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();

			String emailId = "fieldopsautomation@staffex.com";
			CorporateUser corpUser = new CorporateUser();
			corpUser.setConsultant("Y");
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporateUser.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			fc.utobj().printTestStep("Add Franchise User");
			AdminUsersManageManageFranchiseUsersPageTest franUser = new AdminUsersManageManageFranchiseUsersPageTest();
			String FranUserName = fc.utobj().generateTestData("TestF");
			String roleName = "Default Franchise Role";
			franUser.addFranchiseUser(driver, FranUserName, password, franchiseId, roleName, emailId);

			fc.utobj().printTestStep("Navigate To FieldOps > Visits Page");
			fc.utobj().printTestStep("Create Visit");
			FieldOpsVisitsPageTest visitPage = new FieldOpsVisitsPageTest();
			visitPage.createVisitAtVisitPage(driver, config, dataSet, visitFormName, franchiseId,
					corpUser.getuserFullName());

			String visitId = fc.utobj()
					.getElementByXpath(driver,
							".//tr[2]/td[contains(text () ," + "'" + visitFormName + "'" + ")]/../td[1]/a")
					.getText().trim();

			String franUserName = "firstName lastName";

			fc.utobj().printTestStep("Link Task With Visit");
			String subject = linkToVisit(driver, config, dataSet, franchiseId, visitId, franUserName);

			fc.home_page().logout(driver);
			fc.utobj().printTestStep("Login With Franchise User");
			fc.loginpage().loginWithParameter(driver, FranUserName, password);

			fc.utobj().printTestStep("Navigate To FieldOps > Tasks Page");

			fc.fieldops().fieldops_common().fieldopsTasks(driver);

			fc.utobj().printTestStep("Verify Visit Id");

			fc.utobj().clickElement(driver, pobj.showFilter);

			fc.utobj().clickElement(driver, fc.utobj().getElementByID(driver, "Button1"));
			fc.utobj().sendKeys(driver, fc.utobj().getElementByID(driver, "subject"), subject);

			if (!fc.utobj().isSelected(driver, pobj.lookInSubjectChck)) {
				fc.utobj().clickElement(driver, pobj.lookInSubjectChck);
			}
			fc.utobj().clickElement(driver, pobj.searchBtn);
			fc.utobj().clickElement(driver, pobj.hideFilter);

			String text = fc.utobj()
					.getElementByXpath(driver, ".//tr/td/a[.=" + "'" + subject + "'" + "]/../preceding-sibling::td[1]")
					.getText().trim();
			if (text.equalsIgnoreCase("")) {
				System.out.println("Pass");
			} else {
				fc.utobj().throwsException(
						"was not able to verify visit id for private visit task will not display to franchise user ");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = {"fieldops" ,"fieldops0808"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify Practice visit task will be display in task tab.", testCaseId = "TC_91_Verify_Practice_Visit_Task")
	private void verifyPracticeVisitTask() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("fieldops", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			FieldOpsVisitsPage pobj = new FieldOpsVisitsPage(driver);
			String password = "t0n1ght123";

			fc.utobj().printTestStep("Navigate To Admin > Hidden Links > Configure FieldOps Settings Page");
			fc.utobj().printTestStep("Configure Field Ops Settings Log Private Comment Yes");

			AdminHiddenLinksConfigureFieldOpsSettingsPageTest privateComments = new AdminHiddenLinksConfigureFieldOpsSettingsPageTest();
			privateComments.configureFieldOpsSettingsLogPrivateCommentYes(driver, config);

			fc.utobj().printTestStep("Configure FieldOps Settings Franchise Self Audit Yes Owner Scoring Yes");
			privateComments.configureFieldOpsSettingsFranchiseSelfAuditYesOwnerScoringYes(driver, config);

			fc.utobj().printTestStep("Navigate To Admin > Users > Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");

			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUser = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			String emailId = "fieldopsautomation@staffex.com";
			CorporateUser corpUser = new CorporateUser();
			corpUser.setConsultant("Y");
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporateUser.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			fc.utobj().printTestStep("Add Franchise Location");
			String regionName = fc.utobj().generateTestData("Test");
			String storeType = fc.utobj().generateTestData("Test");
			String franchiseId = fc.utobj().generateTestData("Test");
			AdminFranchiseLocationAddFranchiseLocationPageTest franchisePage = new AdminFranchiseLocationAddFranchiseLocationPageTest();
			String fName = fc.utobj().generateTestData("FNAME");
			franchisePage.addFranchiseLocation_AllWithFName(driver, franchiseId, regionName, storeType, fName);

			fc.utobj().printTestStep("Add Franchise User");
			AdminUsersManageManageFranchiseUsersPageTest franUser = new AdminUsersManageManageFranchiseUsersPageTest();
			String FranUserName = fc.utobj().generateTestData("TestF");
			String roleName = "Default Franchise Role";
			String franUserName = franUser.addFranchiseUser(driver, FranUserName, password, franchiseId, roleName,
					emailId);

			fc.utobj().printTestStep("Navigate To Admin > FieldOps > Manage Visit Form");
			fc.utobj().printTestStep("Add Non-Private Visit Form With Tab View");
			AdminFieldOpsManageVisitFormPageTest visitFormPage = new AdminFieldOpsManageVisitFormPageTest();
			String visitFormName = visitFormPage.addVisitFormTabViewPrivateNo(driver, config, dataSet);

			fc.utobj().printTestStep("Add Tab In Visit Form");
			AdminFieldOpsManageVisitFormCustomizeVisitFormPageTest customizeVisit = new AdminFieldOpsManageVisitFormCustomizeVisitFormPageTest();
			customizeVisit.addTab(driver, config, dataSet);

			fc.utobj().printTestStep("Add Question In Tab");
			fc.utobj().clickElement(driver, pobj.addQuestion);
			customizeVisit.addQuestion(driver, dataSet);
			fc.utobj().clickElement(driver, pobj.finishBtn);

			fc.home_page().logout(driver);
			fc.utobj().printTestStep("Login With Franchise User");
			fc.loginpage().loginWithParameter(driver, franUserName, password);

			fc.utobj().printTestStep("Navigate To FieldOsp > Visits Page");
			fc.utobj().printTestStep("Create Practice Visit");
			fc.fieldops().fieldops_common().fieldopsVisitCreateVisitPage(driver);

			FieldOpsVisitsPageTest visitPage = new FieldOpsVisitsPageTest();
			visitPage.createVisitAtFranchiseVisitPage(driver, config, dataSet, visitFormName, franchiseId,
					corpUser.getuserFullName());

			String visitID = fc.utobj().getText(driver, fc.utobj().getElementByXpath(driver,
					".//tr/td[contains(text () ," + "'" + visitFormName + "'" + ")]/preceding-sibling::td/a"));

			fc.utobj().printTestStep("Link Task To Visit");
			fName = fName + " " + fName;
			String subject = linkToVisit(driver, config, dataSet, franchiseId, visitID, fName);

			String taskID = fc.utobj().getText(driver, fc.utobj().getElementByXpath(driver,
					".//tr/td/a[.=" + "'" + subject + "'" + "]/../preceding-sibling::td[2]/a")).trim();

			fc.home_page().logout(driver);
			fc.loginpage().loginWithParameter(driver, corpUser.getUserName(), corpUser.getPassword());

			fc.utobj().printTestStep("Navigate To Fieldops > Tasks Page");

			fc.utobj().printTestStep("Verify Practice Visit Task");
			fc.fieldops().fieldops_common().fieldopsTasks(driver);

			setToDefaultFilter(driver);
			filterByFranchiseID(driver, franchiseId);

			String visitIdNum = fc.utobj()
					.getText(driver, fc.utobj().getElementByXpath(driver,
							".//tr/td/a[contains(text () ," + "'" + subject + "'" + ")]/../preceding-sibling::td[1]"))
					.trim();
			System.out.println(visitIdNum);
			String taskIDAtCorp = fc.utobj()
					.getText(driver, fc.utobj().getElementByXpath(driver,
							".//tr/td/a[contains(text () ," + "'" + subject + "'" + ")]/../preceding-sibling::td[2]/a"))
					.trim();

			if (taskID.equalsIgnoreCase(taskIDAtCorp)) {
			} else {
				fc.utobj().throwsException("Was not able to Verify Practice Visit Task");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);

		}
	}

	@Test(groups = {"fieldops" ,"fieldops0808"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify practice visit task in smart connect.", testCaseId = "TC_92_Verify_Practice_Visit_Task_SmartConnect")
	private void verifyPracticeVisitTaskSmartConnect() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("fieldops", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			FieldOpsVisitsPage pobj = new FieldOpsVisitsPage(driver);
			boolean textStatus = false;
			String password = "t0n1ght123";

			fc.utobj().printTestStep("Navigate To Admin > Hidden Links > Configure FieldOps Settings Page");
			fc.utobj().printTestStep("Configure Field Ops Settings Log Private Comment Yes");

			AdminHiddenLinksConfigureFieldOpsSettingsPageTest privateComments = new AdminHiddenLinksConfigureFieldOpsSettingsPageTest();
			privateComments.configureFieldOpsSettingsLogPrivateCommentYes(driver, config);

			fc.utobj().printTestStep("Configure FieldOps Settings Franchise Self Audit Yes Owner Scoring Yes");
			privateComments.configureFieldOpsSettingsFranchiseSelfAuditYesOwnerScoringYes(driver, config);

			fc.utobj().printTestStep("Navigate To Admin > Users > Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");

			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUser = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			String emailId = "fieldopsautomation@staffex.com";
			CorporateUser corpUser = new CorporateUser();
			corpUser.setConsultant("Y");
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporateUser.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			fc.utobj().printTestStep("Add Franchise Location");
			String regionName = fc.utobj().generateTestData("Test");
			String storeType = fc.utobj().generateTestData("Test");
			String franchiseId = fc.utobj().generateTestData("Test");
			AdminFranchiseLocationAddFranchiseLocationPageTest franchisePage = new AdminFranchiseLocationAddFranchiseLocationPageTest();
			String fName = fc.utobj().generateTestData("FNAME");
			franchisePage.addFranchiseLocation_AllWithFName(driver, franchiseId, regionName, storeType, fName);

			fc.utobj().printTestStep("Add Franchise User");
			AdminUsersManageManageFranchiseUsersPageTest franUser = new AdminUsersManageManageFranchiseUsersPageTest();
			String FranUserName = fc.utobj().generateTestData("TestF");
			String roleName = "Default Franchise Role";
			String franUserName = franUser.addFranchiseUser(driver, FranUserName, password, franchiseId, roleName,
					emailId);

			fc.utobj().printTestStep("Navigate To Admin > FieldOps > Manage Visit Form");
			fc.utobj().printTestStep("Add Non-Private Visit Form With Tab View");
			AdminFieldOpsManageVisitFormPageTest visitFormPage = new AdminFieldOpsManageVisitFormPageTest();
			String visitFormName = visitFormPage.addVisitFormTabViewPrivateNo(driver, config, dataSet);

			fc.utobj().printTestStep("Add Tab In Visit Form");
			AdminFieldOpsManageVisitFormCustomizeVisitFormPageTest customizeVisit = new AdminFieldOpsManageVisitFormCustomizeVisitFormPageTest();
			customizeVisit.addTab(driver, config, dataSet);

			fc.utobj().printTestStep("Add Question In Visit Form");
			fc.utobj().clickElement(driver, pobj.addQuestion);
			customizeVisit.addQuestion(driver, dataSet);
			fc.utobj().clickElement(driver, pobj.finishBtn);

			fc.home_page().logout(driver);

			fc.utobj().printTestStep("Login With Franchise User");
			fc.loginpage().loginWithParameter(driver, franUserName, password);

			fc.utobj().printTestStep("Navigate to FieldOps > Visits Page");

			fc.utobj().printTestStep("Create Practice Visit");

			fc.fieldops().fieldops_common().fieldopsVisitCreateVisitPage(driver);
			FieldOpsVisitsPageTest visitPage = new FieldOpsVisitsPageTest();
			visitPage.createVisitAtFranchiseVisitPage(driver, config, dataSet, visitFormName, franchiseId,
					corpUser.getuserFullName());

			String visitID = fc.utobj().getText(driver, fc.utobj().getElementByXpath(driver,
					".//tr/td[contains(text () ," + "'" + visitFormName + "'" + ")]/preceding-sibling::td/a"));

			fc.utobj().printTestStep("Link Task With Visit");
			fName = fName + " " + fName;
			String subject = linkToVisit(driver, config, dataSet, franchiseId, visitID, fName);

			fc.utobj().printTestStep("Verify Task At SmartConnect Module");
			fc.home_page().openSmartConnectModule(driver);

			List<WebElement> list = driver.findElements(
					By.xpath(".//tr/td[contains(text () ,'Tasks')]/../following-sibling::tr/td/ul/li/a/span"));

			String[] arrayList = new String[list.size()];
			for (int l = 0; l < list.size(); l++) {
				arrayList[l] = list.get(l).getText().trim();
			}

			for (int i = 0; i < arrayList.length; i++) {

				if (subject.equalsIgnoreCase(arrayList[i])) {
					textStatus = true;
				}
			}

			if (textStatus == false) {
				fc.utobj().throwsException("Was not able to verify task of practiceVisit at SmartConnect Page");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = {"fieldops" ,"fieldops0808"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify practice visit task in Hub", testCaseId = "TC_93_Verify_Practice_Visit_Task_The_Hub")
	private void verifyPracticeVisitTaskTheHub() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("fieldops", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			FieldOpsVisitsPage pobj = new FieldOpsVisitsPage(driver);

			fc.utobj().printTestStep("Navigate To Admin > Hidden Links > Configure FieldOps Settings Page");
			fc.utobj().printTestStep("Configure Field Ops Settings Log Private Comment Yes");

			AdminHiddenLinksConfigureFieldOpsSettingsPageTest privateComments = new AdminHiddenLinksConfigureFieldOpsSettingsPageTest();
			privateComments.configureFieldOpsSettingsLogPrivateCommentYes(driver, config);

			fc.utobj().printTestStep("Configure FieldOps Settings Franchise Self Audit Yes Owner Scoring Yes");
			privateComments.configureFieldOpsSettingsFranchiseSelfAuditYesOwnerScoringYes(driver, config);
			String password = "t0n1ght123";

			fc.utobj().printTestStep("Navigate To Admin > Users > Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUser = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			String emailId = "fieldopsautomation@staffex.com";
			CorporateUser corpUser = new CorporateUser();
			corpUser.setConsultant("Y");
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporateUser.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			fc.utobj().printTestStep("Add Franchise Location");
			String regionName = fc.utobj().generateTestData("Test");
			String storeType = fc.utobj().generateTestData("Test");
			String franchiseId = fc.utobj().generateTestData("Test");
			AdminFranchiseLocationAddFranchiseLocationPageTest franchisePage = new AdminFranchiseLocationAddFranchiseLocationPageTest();
			String fName = fc.utobj().generateTestData("FNAME");
			franchisePage.addFranchiseLocation_AllWithFName(driver, franchiseId, regionName, storeType, fName);

			fc.utobj().printTestStep("Add Franchise User");
			AdminUsersManageManageFranchiseUsersPageTest franUser = new AdminUsersManageManageFranchiseUsersPageTest();
			String FranUserName = fc.utobj().generateTestData("TestF");
			String roleName = "Default Franchise Role";
			String franUserName = franUser.addFranchiseUser(driver, FranUserName, password, franchiseId, roleName,
					emailId);

			fc.utobj().printTestStep("Navigate To Admin > FieldOps > Manage Visit Form");
			fc.utobj().printTestStep("Add Non-Private Visit Form With Tab View");
			AdminFieldOpsManageVisitFormPageTest visitFormPage = new AdminFieldOpsManageVisitFormPageTest();
			String visitFormName = visitFormPage.addVisitFormTabViewPrivateNo(driver, config, dataSet);

			fc.utobj().printTestStep("Add Tab");
			AdminFieldOpsManageVisitFormCustomizeVisitFormPageTest customizeVisit = new AdminFieldOpsManageVisitFormCustomizeVisitFormPageTest();
			customizeVisit.addTab(driver, config, dataSet);

			fc.utobj().printTestStep("Add Question In Tab");
			fc.utobj().clickElement(driver, pobj.addQuestion);
			customizeVisit.addQuestion(driver, dataSet);
			fc.utobj().clickElement(driver, pobj.finishBtn);

			fc.home_page().logout(driver);

			fc.utobj().printTestStep("Login With Franchise User");
			fc.loginpage().loginWithParameter(driver, franUserName, password);

			fc.utobj().printTestStep("Navigate To FieldOps > Visits Page");
			fc.utobj().printTestStep("Create Practice Visit");

			fc.fieldops().fieldops_common().fieldopsVisitCreateVisitPage(driver);
			FieldOpsVisitsPageTest visitPage = new FieldOpsVisitsPageTest();
			visitPage.createVisitAtFranchiseVisitPage(driver, config, dataSet, visitFormName, franchiseId,
					corpUser.getuserFullName());

			String visitID = fc.utobj().getText(driver, fc.utobj().getElementByXpath(driver,
					".//tr/td[contains(text () ," + "'" + visitFormName + "'" + ")]/preceding-sibling::td/a"));

			fc.utobj().printTestStep("Link Task With Visit");
			fName = fName + " " + fName;
			String subject = linkToVisit(driver, config, dataSet, franchiseId, visitID, fName);

			fc.home_page().logout(driver);

			fc.utobj().printTestStep("Login With Corporate User");
			fc.loginpage().loginWithParameter(driver, corpUser.getUserName(), corpUser.getPassword());

			fc.utobj().printTestStep("Verify Task At The Hub Module");
			fc.hub().hub_common().theHubTaskSubModule(driver);

			filterByFielOpsSubject(driver, subject);

			fc.utobj().getElementByXpath(driver, ".//tr/td/a[contains(text () ," + "'" + subject + "'" + ")]")
					.isEnabled();

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = {"fieldops" , "fieldops0808"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify that all task will be display if all is selected from drop down", testCaseId = "TC_104_All_Task_VIsible_All_Muid")
	private void allTaskVisibleForAllMuid() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("fieldops", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			String password = "t0n1ght123";

			fc.utobj().printTestStep("Navigate To Admin > FieldOps > Action Library");
			fc.utobj().printTestStep("Add Action Item");
			AdminFieldOpsActionLibraryPageTest actionPage = new AdminFieldOpsActionLibraryPageTest();
			String subject = fc.utobj().generateTestData("subject");
			subject = actionPage.addActionItem(driver, dataSet);

			fc.utobj().printTestStep("Navigate To Admin > Franchise Location > Add Franchise Location");
			fc.utobj().printTestStep("Add Franchise Location With MUId");
			AdminFranchiseLocationAddFranchiseLocationPageTest addLoc = new AdminFranchiseLocationAddFranchiseLocationPageTest();
			String franchiseId = fc.utobj().generateTestData("TestFID");
			String regionName1 = fc.utobj().generateTestData("TestR");
			String storeType1 = fc.utobj().generateTestData("TestS");
			String firstName = fc.utobj().generateTestData("firstName");
			String lastName = "lastName";
			franchiseId = addLoc.addFranchiseLocation_AllMuid(driver, franchiseId, regionName1, storeType1, firstName,
					lastName, config);

			fc.utobj().printTestStep("Add Franchise User");
			AdminUsersManageManageFranchiseUsersPageTest addFranUser = new AdminUsersManageManageFranchiseUsersPageTest();
			String userName = fc.utobj().generateTestData("Test");
			String roleName = "Default Franchise Role";
			String emailId = "fieldopsautomation@staffex.com";
			addFranUser.addFranchiseUser(driver, userName, password, franchiseId, roleName, emailId);

			AdminFranchiseLocationAddFranchiseLocationPageTest franchiseLoc = new AdminFranchiseLocationAddFranchiseLocationPageTest();
			String franchiseeID = fc.utobj().generateTestData("TestFID");
			String centerName = fc.utobj().generateTestData("TestC");

			String curDate = fc.utobj().getCurrentDateUSFormat();

			String openingDate = curDate;
			String muId = fc.utobj().generateTestData("TestMuid");
			String regionName = fc.utobj().generateTestData("TestR");
			String storeType = fc.utobj().generateTestData("TestS");

			fc.utobj().printTestStep("Navigate To Admin > Add Area/Region");
			fc.utobj().printTestStep("Add Region");

			AdminAreaRegionAddAreaRegionPageTest addregion = new AdminAreaRegionAddAreaRegionPageTest();
			addregion.addAreaRegion(driver, regionName);

			fc.utobj().printTestStep("Navigate To Admin > Configuration > Configure Store Type Page");
			fc.utobj().printTestStep("Add Store Type");
			AdminConfigurationConfigureStoreTypePageTest addStore = new AdminConfigurationConfigureStoreTypePageTest();
			addStore.addStoreType(driver, storeType);

			String franchiseUserName = firstName + " " + lastName;

			fc.utobj().printTestStep("Add Franchise Location With MUid");
			muId = franchiseLoc.franchiseLocationWithMUID(driver, franchiseeID, regionName, centerName, openingDate,
					storeType, franchiseUserName, muId, config);

			fc.utobj().printTestStep("Add Muid User");
			String muIdUserName = fc.utobj().generateTestData("TestM");
			String firstMuidName = fc.utobj().generateTestData("firstMuidName");

			muIdUserName = addFranUser.addMUIDUser(driver, muIdUserName, password, muId, firstMuidName, config,
					emailId);

			fc.home_page().logout(driver);
			fc.utobj().printTestStep("Login WIth MU ID");
			fc.loginpage().loginWithParameter(driver, muIdUserName, password);

			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='franchisee_no']/span/a"));
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//*[@id='franchisee_no']/div/div/span[.='All ']"));

			fc.fieldops().fieldops_common().fieldopsTasks(driver);

			fc.utobj().printTestStep("Navigate To FieldOps > Tasks Page");
			fc.utobj().printTestStep("Add Task For Location");
			FieldOpsTasksPageTest taskPage = new FieldOpsTasksPageTest();
			taskPage.addTask(driver, config, dataSet, franchiseId, subject, franchiseUserName);

			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='franchisee_no']/span/a"));
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//*[@id='franchisee_no']/div/div/span[.='All ']"));

			fc.fieldops().fieldops_common().fieldopsTasks(driver);

			fc.utobj().printTestStep("Navigate To FieldOps > Tasks Page");
			fc.utobj().printTestStep("Add Task For Second Location");
			taskPage.addTask(driver, config, dataSet, franchiseeID, subject, franchiseUserName);

			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='franchisee_no']/span/a"));
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//*[@id='franchisee_no']/div/div/span[.='All ']"));

			fc.utobj().printTestStep("Verify All Task If All Location Selected");
			fc.fieldops().fieldops_common().fieldopsTasks(driver);

			String franchiseIdText = fc.utobj().getText(driver, fc.utobj().getElementByXpath(driver,
					".//tr[2]/td[4]//a[contains(text () ," + "'" + subject + "'" + ")]/../following-sibling::td[1]"));
			String franchiseeIDtext = fc.utobj().getText(driver, fc.utobj().getElementByXpath(driver,
					".//tr[3]/td[4]//a[contains(text () ," + "'" + subject + "'" + ")]/../following-sibling::td[1]"));

			if (franchiseIdText.contains(franchiseeID) && franchiseeIDtext.contains(franchiseId)) {

			} else {
				fc.utobj().throwsException("was not able to verify Muid with add task show");
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = {"fieldops" ,"fieldops0808"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify that only task will be display which is associate to that location if we select that location from drop down", testCaseId = "TC_106_Visible_Task_Associates_FranchiseID")
	private void visibleTaskFranchiseIdMuid() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("fieldops", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			String password = "t0n1ght123";

			fc.utobj().printTestStep("Navigate To Admin > FieldOps > Action Library");
			fc.utobj().printTestStep("Add Action Item");
			AdminFieldOpsActionLibraryPageTest actionPage = new AdminFieldOpsActionLibraryPageTest();
			String subject = fc.utobj().generateTestData("subject");
			subject = actionPage.addActionItem(driver, dataSet);

			fc.utobj().printTestStep("Navigate To Admin > FieldOps > Action Library");
			fc.utobj().printTestStep("Add Action Item");
			String subject1 = fc.utobj().generateTestData("subject");
			subject1 = actionPage.addActionItem(driver, dataSet);

			fc.utobj().printTestStep("Navigate To Admin > Franchise Location > Add Franchise Location");
			fc.utobj().printTestStep("Add Franchise Location With Muid");
			AdminFranchiseLocationAddFranchiseLocationPageTest addLoc = new AdminFranchiseLocationAddFranchiseLocationPageTest();
			String franchiseId = fc.utobj().generateTestData("TestFID");
			String regionName1 = fc.utobj().generateTestData("TestR");
			String storeType1 = fc.utobj().generateTestData("TestS");
			String firstName = fc.utobj().generateTestData("firstName");
			String lastName = "lastName";
			franchiseId = addLoc.addFranchiseLocation_AllMuid(driver, franchiseId, regionName1, storeType1, firstName,
					lastName, config);

			fc.utobj().printTestStep("Add Franchise User");
			AdminUsersManageManageFranchiseUsersPageTest addFranUser = new AdminUsersManageManageFranchiseUsersPageTest();
			String userName = fc.utobj().generateTestData("Test");
			String roleName = "Default Franchise Role";
			String emailId = "fieldopsautomation@staffex.com";
			addFranUser.addFranchiseUser(driver, userName, password, franchiseId, roleName, emailId);

			AdminFranchiseLocationAddFranchiseLocationPageTest franchiseLoc = new AdminFranchiseLocationAddFranchiseLocationPageTest();
			String franchiseeID = fc.utobj().generateTestData("TestFID");
			String centerName = fc.utobj().generateTestData("TestC");

			String curDate = fc.utobj().getCurrentDateUSFormat();
			String openingDate = curDate;
			String muId = fc.utobj().generateTestData("TestMuid");
			String regionName = fc.utobj().generateTestData("TestR");
			String storeType = fc.utobj().generateTestData("TestS");

			fc.utobj().printTestStep("Navigate To Admin > Add Area/Region");
			fc.utobj().printTestStep("Add Region");

			AdminAreaRegionAddAreaRegionPageTest addregion = new AdminAreaRegionAddAreaRegionPageTest();
			addregion.addAreaRegion(driver, regionName);

			AdminConfigurationConfigureStoreTypePageTest addStore = new AdminConfigurationConfigureStoreTypePageTest();
			addStore.addStoreType(driver, storeType);
			String franchiseUserName = firstName + " " + lastName;

			fc.utobj().printTestStep("Add Franchise Location With Muid");

			muId = franchiseLoc.franchiseLocationWithMUID(driver, franchiseeID, regionName, centerName, openingDate,
					storeType, franchiseUserName, muId, config);

			fc.utobj().printTestStep("Add MUID User");
			String muIdUserName = fc.utobj().generateTestData("TestM");
			String firstMuidName = fc.utobj().generateTestData("firstMuidName");

			muIdUserName = addFranUser.addMUIDUser(driver, muIdUserName, password, muId, firstMuidName, config,
					emailId);

			fc.home_page().logout(driver);
			fc.utobj().printTestStep("Login With MU Id");
			fc.loginpage().loginWithParameter(driver, muIdUserName, password);

			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='franchisee_no']/span/a"));
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//*[@id='franchisee_no']/div/div/span[contains(text () ," + "'" + franchiseId + "'" + ")]"));

			fc.utobj().printTestStep("Add Task For First Location");

			fc.fieldops().fieldops_common().fieldopsTasks(driver);
			fc.utobj().printTestStep("Navigate To FieldOps > Tasks Page");
			fc.utobj().printTestStep("Add Task For First Location");
			FieldOpsTasksPageTest taskPage = new FieldOpsTasksPageTest();
			taskPage.addTask(driver, config, dataSet, franchiseId, subject, franchiseUserName);

			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='franchisee_no']/span/a"));
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//*[@id='franchisee_no']/div/div/span[contains(text () ," + "'" + franchiseeID + "'" + ")]"));
			fc.fieldops().fieldops_common().fieldopsTasks(driver);

			fc.utobj().printTestStep("Navigate To FieldOps > Tasks Page");
			fc.utobj().printTestStep("Add Task For Second Location");
			taskPage.addTask(driver, config, dataSet, franchiseeID, subject1, franchiseUserName);

			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='franchisee_no']/span/a"));
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//*[@id='franchisee_no']/div/div/span[contains(text () ," + "'" + franchiseId + "'" + ")]"));

			fc.utobj().printTestStep("Verify Task If First Location Selected");
			fc.fieldops().fieldops_common().fieldopsTasks(driver);
			String subjectText = fc.utobj().getText(driver,
					fc.utobj().getElementByXpath(driver, ".//tr/td/a[.=" + "'" + subject + "'" + "]"));

			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='franchisee_no']/span/a"));
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//*[@id='franchisee_no']/div/div/span[contains(text () ," + "'" + franchiseeID + "'" + ")]"));

			fc.utobj().printTestStep("Verify Task If Second Location Selected");
			fc.fieldops().fieldops_common().fieldopsTasks(driver);
			String subject1Text = fc.utobj().getText(driver,
					fc.utobj().getElementByXpath(driver, ".//tr/td/a[.=" + "'" + subject1 + "'" + "]"));

			if (subjectText.equalsIgnoreCase(subject) && subject1Text.equalsIgnoreCase(subject1)) {
			} else {
				fc.utobj().throwsException("Was not able to verify Task franchise ID at Muid");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "fieldops", "fieldops0808" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify that all task will be display if all is selected from drop down", testCaseId = "TC_107_Verify_Task_The_Hub_Muid_All")
	private void verifyTaskTheHubMuidAll() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("fieldops", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			String password = "t0n1ght123";

			fc.utobj().printTestStep("Navigate To Admin > FieldOps > Action Library");
			fc.utobj().printTestStep("Add Action Item");
			AdminFieldOpsActionLibraryPageTest actionPage = new AdminFieldOpsActionLibraryPageTest();
			String subject = fc.utobj().generateTestData("subject");
			subject = actionPage.addActionItem(driver, dataSet);

			fc.utobj().printTestStep("Navigate To Admin > Franchise Location > Add Franchise Location");
			fc.utobj().printTestStep("Add Franchise Location With Muid");

			AdminFranchiseLocationAddFranchiseLocationPageTest addLoc = new AdminFranchiseLocationAddFranchiseLocationPageTest();
			String franchiseId = fc.utobj().generateTestData("TestFID");
			String regionName1 = fc.utobj().generateTestData("TestR");
			String storeType1 = fc.utobj().generateTestData("TestS");
			String firstName = fc.utobj().generateTestData("firstName");
			String lastName = "lastName";
			franchiseId = addLoc.addFranchiseLocation_AllMuid(driver, franchiseId, regionName1, storeType1, firstName,
					lastName, config);

			fc.utobj().printTestStep("Add Franchise User");
			AdminUsersManageManageFranchiseUsersPageTest addFranUser = new AdminUsersManageManageFranchiseUsersPageTest();
			String userName = fc.utobj().generateTestData("Test");
			String roleName = "Default Franchise Role";
			String emailId = "fieldopsautomation@staffex.com";
			addFranUser.addFranchiseUser(driver, userName, password, franchiseId, roleName, emailId);

			AdminFranchiseLocationAddFranchiseLocationPageTest franchiseLoc = new AdminFranchiseLocationAddFranchiseLocationPageTest();
			String franchiseeID = fc.utobj().generateTestData("TestFID");
			String centerName = fc.utobj().generateTestData("TestC");

			String curDate = fc.utobj().getCurrentDateUSFormat();
			String openingDate = curDate;
			String muId = fc.utobj().generateTestData("TestMuid");
			String regionName = fc.utobj().generateTestData("TestR");
			String storeType = fc.utobj().generateTestData("TestS");

			fc.utobj().printTestStep("Navigate To Admin > Add Area/Region");
			fc.utobj().printTestStep("Add Region");

			AdminAreaRegionAddAreaRegionPageTest addregion = new AdminAreaRegionAddAreaRegionPageTest();
			addregion.addAreaRegion(driver, regionName);

			fc.utobj().printTestStep("Add Franchise Location With MUID");

			AdminConfigurationConfigureStoreTypePageTest addStore = new AdminConfigurationConfigureStoreTypePageTest();
			addStore.addStoreType(driver, storeType);
			String franchiseUserName = firstName + " " + lastName;
			muId = franchiseLoc.franchiseLocationWithMUID(driver, franchiseeID, regionName, centerName, openingDate,
					storeType, franchiseUserName, muId, config);

			fc.utobj().printTestStep("Add MuId user");
			String muIdUserName = fc.utobj().generateTestData("TestM");
			String firstMuidName = fc.utobj().generateTestData("firstMuidName");

			muIdUserName = addFranUser.addMUIDUser(driver, muIdUserName, password, muId, firstMuidName, config,
					emailId);

			fc.home_page().logout(driver);
			fc.utobj().printTestStep("Login WIth MU ID");
			fc.loginpage().loginWithParameter(driver, muIdUserName, password);

			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='franchisee_no']/span/a"));
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//*[@id='franchisee_no']/div/div/span[.='All ']"));

			fc.utobj().printTestStep("Navigate To FieldOps > Tasks Page");
			fc.utobj().printTestStep("Add Task For First Location");
			fc.fieldops().fieldops_common().fieldopsTasks(driver);

			FieldOpsTasksPageTest taskPage = new FieldOpsTasksPageTest();
			taskPage.addTask(driver, config, dataSet, franchiseId, subject, franchiseUserName);

			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='franchisee_no']/span/a"));
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//*[@id='franchisee_no']/div/div/span[.='All ']"));

			fc.utobj().printTestStep("Navigate To FieldOps > Tasks Page");
			fc.utobj().printTestStep("Add Task For Second Location");
			fc.fieldops().fieldops_common().fieldopsTasks(driver);

			taskPage.addTask(driver, config, dataSet, franchiseeID, subject, franchiseUserName);

			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='franchisee_no']/span/a"));
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//*[@id='franchisee_no']/div/div/span[.='All ']"));

			fc.utobj().printTestStep("Navigate To The Hub Module");
			fc.hub().hub_common().theHubTaskSubModule(driver);

			fc.utobj().printTestStep("Verify All Task");
			filterByFielOpsSubject(driver, subject);

			String text = fc.utobj()
					.getText(driver,
							fc.utobj().getElementByXpath(driver,
									".//tr/td/a[.=" + "'" + subject + "'"
											+ "]/../following-sibling::td[contains(text () ," + "'" + franchiseId + "'"
											+ ")]"));
			String text1 = fc.utobj()
					.getText(driver,
							fc.utobj().getElementByXpath(driver,
									".//tr/td/a[.=" + "'" + subject + "'"
											+ "]/../following-sibling::td[contains(text () ," + "'" + franchiseeID + "'"
											+ ")]"));

			if (text.contains(franchiseId) && text1.contains(franchiseeID)) {
				System.out.println(text);
				System.out.println(text1);
			} else {
				fc.utobj().throwsException("Was not able to verify task at The Hub With Muid");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "fieldops", "fieldops0808" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify that only task will be display which is associate to that location if we select that location from drop down", testCaseId = "TC_108_Verify_Task_The_Hub_Muid_FranchiseId")
	private void verifyTaskTheHubMuidFranchiseId() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("fieldops", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			String password = "t0n1ght123";

			fc.utobj().printTestStep("Navigate To Admin > FieldOps > Action Library");
			fc.utobj().printTestStep("Add Action Item");
			AdminFieldOpsActionLibraryPageTest actionPage = new AdminFieldOpsActionLibraryPageTest();
			String subject = fc.utobj().generateTestData("subject");
			subject = actionPage.addActionItem(driver, dataSet);

			fc.utobj().printTestStep("Navigate To Admin > FieldOps > Action Library");
			fc.utobj().printTestStep("Add Action Item");
			String subject1 = fc.utobj().generateTestData("subject");
			subject1 = actionPage.addActionItem(driver, dataSet);

			fc.utobj().printTestStep("Navigate To Admin > Franchise Location > Add Franchise Location");
			fc.utobj().printTestStep("Add Franchise Location With Muid");

			AdminFranchiseLocationAddFranchiseLocationPageTest addLoc = new AdminFranchiseLocationAddFranchiseLocationPageTest();
			String franchiseId = fc.utobj().generateTestData("Test108FID");
			String regionName1 = fc.utobj().generateTestData("Test108R");
			String storeType1 = fc.utobj().generateTestData("Test108S");
			String firstName = fc.utobj().generateTestData("firstName");
			String lastName = "lastName";
			franchiseId = addLoc.addFranchiseLocation_AllMuid(driver, franchiseId, regionName1, storeType1, firstName,
					lastName, config);

			fc.utobj().printTestStep("Add Franchise User");
			AdminUsersManageManageFranchiseUsersPageTest addFranUser = new AdminUsersManageManageFranchiseUsersPageTest();
			String userName = fc.utobj().generateTestData("Test");
			String roleName = "Default Franchise Role";
			String emailId = "fieldopsautomation@staffex.com";
			addFranUser.addFranchiseUser(driver, userName, password, franchiseId, roleName, emailId);

			AdminFranchiseLocationAddFranchiseLocationPageTest franchiseLoc = new AdminFranchiseLocationAddFranchiseLocationPageTest();
			String franchiseeID = fc.utobj().generateTestData("TestFID");
			String centerName = fc.utobj().generateTestData("TestC");

			String curDate = fc.utobj().getCurrentDateUSFormat();
			String openingDate = curDate;
			String muId = fc.utobj().generateTestData("Test108Muid");
			String regionName = fc.utobj().generateTestData("Test108R");
			String storeType = fc.utobj().generateTestData("Test108S");

			fc.utobj().printTestStep("Navigate To Admin > Add Area/Region");
			fc.utobj().printTestStep("Add Region");

			AdminAreaRegionAddAreaRegionPageTest addregion = new AdminAreaRegionAddAreaRegionPageTest();
			addregion.addAreaRegion(driver, regionName);

			fc.utobj().printTestStep("Add Store Type");

			AdminConfigurationConfigureStoreTypePageTest addStore = new AdminConfigurationConfigureStoreTypePageTest();
			addStore.addStoreType(driver, storeType);

			String franchiseUserName = firstName + " " + lastName;
			fc.utobj().printTestStep("Add Franchise Location With MUID");

			muId = franchiseLoc.franchiseLocationWithMUID(driver, franchiseeID, regionName, centerName, openingDate,
					storeType, franchiseUserName, muId, config);
			fc.utobj().printTestStep("Add MUID User");
			String muIdUserName = fc.utobj().generateTestData("Test108M");
			String firstMuidName = fc.utobj().generateTestData("firstMuidName");

			muIdUserName = addFranUser.addMUIDUser(driver, muIdUserName, password, muId, firstMuidName, config,
					emailId);

			fc.utobj().printTestStep("Configure FieldOps Settings Franchise Self Audit Yes Owner Scoring Yes");
			AdminHiddenLinksConfigureFieldOpsSettingsPageTest fieldOpsPage = new AdminHiddenLinksConfigureFieldOpsSettingsPageTest();
			fieldOpsPage.configureFieldOpsSettingsFranchiseSelfAuditYesOwnerScoringYes(driver, config);

			fc.home_page().logout(driver);
			fc.utobj().printTestStep("Login With MU Id");
			fc.loginpage().loginWithParameter(driver, muIdUserName, password);

			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='franchisee_no']/span/a"));
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//*[@id='franchisee_no']/div/div/span[contains(text () ," + "'" + franchiseId + "'" + ")]"));

			// navigate to fieldopstask
			fc.utobj().printTestStep("Navigate To FieldOps > Tasks Page");
			fc.utobj().printTestStep("Add Task For First Location");
			fc.fieldops().fieldops_common().fieldopsTasks(driver);

			FieldOpsTasksPageTest taskPage = new FieldOpsTasksPageTest();
			taskPage.addTask(driver, config, dataSet, franchiseId, subject, franchiseUserName);

			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='franchisee_no']/span/a"));
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//*[@id='franchisee_no']/div/div/span[contains(text () ," + "'" + franchiseeID + "'" + ")]"));

			fc.utobj().printTestStep("Navigate To FieldOps > Tasks Page");
			fc.utobj().printTestStep("Add Task For Second Location");
			// navigate to task page
			fc.fieldops().fieldops_common().fieldopsTasks(driver);

			firstMuidName = firstMuidName + " " + "lastName";

			taskPage.addTask(driver, config, dataSet, franchiseeID, subject1, firstMuidName);

			fc.utobj().printTestStep("Navigate To The Hub Module");

			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='franchisee_no']/span/a"));
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//*[@id='franchisee_no']/div/div/span[contains(text () ," + "'" + franchiseId + "'" + ")]"));

			fc.hub().hub_common().theHubTaskSubModule(driver);
			filterByFielOpsSubject(driver, "");

			fc.utobj().printTestStep("Verify Task For First Location");
			String assignTo = fc.utobj().getText(driver,
					fc.utobj().getElementByXpath(driver, ".//tr/td[contains(text () ," + "'" + firstName + "'" + ")]"));
			String associatedWith = fc.utobj().getText(driver, fc.utobj().getElementByXpath(driver,
					".//tr/td[contains(text ()," + "'" + franchiseId + "'" + ")]"));
			String subjectText = fc.utobj().getText(driver,
					fc.utobj().getElementByXpath(driver, ".//a[contains(text () ," + "'" + subject + "'" + ")]"));

			if (assignTo.contains(firstName) && associatedWith.equalsIgnoreCase(franchiseId)
					&& subjectText.equalsIgnoreCase(subject)) {
				System.out.println("Pass 1");
			} else {
				fc.utobj().throwsException("was not able to verify Franchise With Task at The Hub of MUID");
			}

			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='franchisee_no']/span/a"));
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//*[@id='franchisee_no']/div/div/span[contains(text () ," + "'" + franchiseeID + "'" + ")]"));

			fc.utobj().printTestStep("Verify Task For Second Location");
			fc.hub().hub_common().theHubTaskSubModule(driver);
			filterByFielOpsSubject(driver, "");

			String assignTo1 = fc.utobj().getText(driver, fc.utobj().getElementByXpath(driver,
					".//tr/td[contains(text () ," + "'" + firstMuidName + "'" + ")]"));
			String associatedWith1 = fc.utobj().getText(driver, fc.utobj().getElementByXpath(driver,
					".//tr/td[contains(text ()," + "'" + franchiseeID + "'" + ")]"));
			String subjectText1 = fc.utobj().getText(driver,
					fc.utobj().getElementByXpath(driver, ".//a[contains(text () ," + "'" + subject1 + "'" + ")]"));

			if (assignTo1.contains(firstMuidName) && associatedWith1.equalsIgnoreCase(franchiseeID)
					&& subjectText1.equalsIgnoreCase(subject1)) {
				System.out.println("Pass 2");
			} else {
				fc.utobj().throwsException("was not able to verify Franchise With Task at The Hub of MUID");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "fieldops", "fieldops0808" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify Task in smartconnect after selecting All Franchise from drop down.", testCaseId = "TC_111_Verify_Task_Smart_Connect_All")
	private void verifyTaskSmartConnectMuidAll() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("fieldops", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			String password = "t0n1ght123";

			fc.utobj().printTestStep("Navigate To Admin > Hidden Links > Configure FieldOps Settings Page");
			fc.utobj().printTestStep("Configure FieldOps Settings Franchise Self Audit Yes Owner Scoring Yes");
			AdminHiddenLinksConfigureFieldOpsSettingsPageTest fieldOpsSettingPage = new AdminHiddenLinksConfigureFieldOpsSettingsPageTest();
			fieldOpsSettingPage.configureFieldOpsSettingsFranchiseSelfAuditYesOwnerScoringYes(driver, config);

			fc.utobj().printTestStep("Navigate To Admin > FieldOps > Action Library");
			fc.utobj().printTestStep("Add Action Item");
			AdminFieldOpsActionLibraryPageTest actionPage = new AdminFieldOpsActionLibraryPageTest();
			String subject = fc.utobj().generateTestData("subject");
			subject = actionPage.addActionItem(driver, dataSet);

			fc.utobj().printTestStep("Navigate To Admin > Users > Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");

			fc.utobj().printTestStep("Navigate To Admin > Franchise Location > Add Franchise Location");
			fc.utobj().printTestStep("Add Franchise Location With Muid");
			AdminFranchiseLocationAddFranchiseLocationPageTest addLoc = new AdminFranchiseLocationAddFranchiseLocationPageTest();
			String franchiseId = fc.utobj().generateTestData("TestFID");
			String regionName1 = fc.utobj().generateTestData("TestR");
			String storeType1 = fc.utobj().generateTestData("TestS");
			String firstName = fc.utobj().generateTestData("firstName");
			String lastName = "lastName";
			franchiseId = addLoc.addFranchiseLocation_AllMuid(driver, franchiseId, regionName1, storeType1, firstName,
					lastName, config);

			fc.utobj().printTestStep("Add Franchise User");
			AdminUsersManageManageFranchiseUsersPageTest addFranUser = new AdminUsersManageManageFranchiseUsersPageTest();
			String userName = fc.utobj().generateTestData("TestFUser");
			String roleName = "Default Franchise Role";
			String emailId = "fieldopsautomation@staffex.com";
			addFranUser.addFranchiseUser(driver, userName, password, franchiseId, roleName, emailId);

			AdminFranchiseLocationAddFranchiseLocationPageTest franchiseLoc = new AdminFranchiseLocationAddFranchiseLocationPageTest();
			String franchiseeID = fc.utobj().generateTestData("TestFID");
			String centerName = fc.utobj().generateTestData("TestC");

			String openingDate = fc.utobj().getCurrentDateUSFormat();
			String muId = fc.utobj().generateTestData("TestMuid");
			String regionName = fc.utobj().generateTestData("TestR");
			String storeType = fc.utobj().generateTestData("TestS");

			fc.utobj().printTestStep("Navigate To Admin > Add Area/Region");
			fc.utobj().printTestStep("Add Region");
			AdminAreaRegionAddAreaRegionPageTest addregion = new AdminAreaRegionAddAreaRegionPageTest();
			addregion.addAreaRegion(driver, regionName);

			fc.utobj().printTestStep("Add Franchise Location With MUID");

			AdminConfigurationConfigureStoreTypePageTest addStore = new AdminConfigurationConfigureStoreTypePageTest();
			addStore.addStoreType(driver, storeType);
			String franchiseUserName = firstName + " " + lastName;
			muId = franchiseLoc.franchiseLocationWithMUID(driver, franchiseeID, regionName, centerName, openingDate,
					storeType, franchiseUserName, muId, config);

			fc.utobj().printTestStep("Add MUID User");
			String muIdUserName = fc.utobj().generateTestData("TestM");
			String firstMuidName = fc.utobj().generateTestData("firstMuidName");
			muIdUserName = addFranUser.addMUIDUser(driver, muIdUserName, password, muId, firstMuidName, config,
					emailId);

			fc.home_page().logout(driver);

			fc.utobj().printTestStep("Login With MU ID");
			fc.loginpage().loginWithParameter(driver, muIdUserName, password);

			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='franchisee_no']/span/a"));
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//*[@id='franchisee_no']/div/div/span[.='All ']"));

			fc.utobj().printTestStep("Navigate To FieldOps > Tasks Page");
			fc.utobj().printTestStep("Add Task For First Location");
			// navigate to Add task

			fc.fieldops().fieldops_common().fieldopsTasks(driver);

			FieldOpsTasksPageTest taskPage = new FieldOpsTasksPageTest();
			taskPage.addTask(driver, config, dataSet, franchiseId, subject, franchiseUserName);

			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='franchisee_no']/span/a"));
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//*[@id='franchisee_no']/div/div/span[.='All ']"));

			fc.utobj().printTestStep("Navigate To FieldOps > Tasks Page");
			fc.utobj().printTestStep("Add Task For Second Location");
			// navigate to Add Tasks
			fc.fieldops().fieldops_common().fieldopsTasks(driver);

			taskPage.addTask(driver, config, dataSet, franchiseeID, subject, franchiseUserName);

			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='franchisee_no']/span/a"));
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//*[@id='franchisee_no']/div/div/span[.='All ']"));

			fc.utobj().printTestStep("Navigate To SmartConnect Module");

			fc.utobj().printTestStep("Verify All Task");
			fc.home_page().openSmartConnectModule(driver);

			fc.utobj().getElementByXpath(driver, ".//tr/td/ul/li[@id='task0']/a/span[.=" + "'" + subject + "'" + "]")
					.isEnabled();
			fc.utobj().getElementByXpath(driver, ".//tr/td/ul/li[@id='task1']/a/span[.=" + "'" + subject + "'" + "]")
					.isEnabled();

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "fieldops", "fieldops0808" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify Task in smartconnect after selecting FranchiseID from drop down.", testCaseId = "TC_112_Verify_Task_Smart_Connect_FranchiseId")
	private void verifyTaskSmartConnectMuidFranchiseId() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("fieldops", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			String password = "t0n1ght123";

			fc.utobj().printTestStep("Configure FieldOps Settings Franchise Self Audit Yes Owner Scoring Yes");
			AdminHiddenLinksConfigureFieldOpsSettingsPageTest fieldOpsSettingPage = new AdminHiddenLinksConfigureFieldOpsSettingsPageTest();
			fieldOpsSettingPage.configureFieldOpsSettingsFranchiseSelfAuditYesOwnerScoringYes(driver, config);

			fc.utobj().printTestStep("Navigate To Admin > FieldOps > Action Library");
			fc.utobj().printTestStep("Add Action Item");
			AdminFieldOpsActionLibraryPageTest actionPage = new AdminFieldOpsActionLibraryPageTest();
			String subject = fc.utobj().generateTestData("subject");
			subject = actionPage.addActionItem(driver, dataSet);

			fc.utobj().printTestStep("Navigate To Admin > Franchise Location > Add Franchise Location");
			fc.utobj().printTestStep("Add Franchise Location With Muid");
			AdminFranchiseLocationAddFranchiseLocationPageTest addLoc = new AdminFranchiseLocationAddFranchiseLocationPageTest();
			String franchiseId = fc.utobj().generateTestData("TestFID");
			String regionName1 = fc.utobj().generateTestData("TestR");
			String storeType1 = fc.utobj().generateTestData("TestS");
			String firstName = fc.utobj().generateTestData("firstName");
			String lastName = "lastName";
			franchiseId = addLoc.addFranchiseLocation_AllMuid(driver, franchiseId, regionName1, storeType1, firstName,
					lastName, config);

			fc.utobj().printTestStep("Add Franchise User");
			AdminUsersManageManageFranchiseUsersPageTest addFranUser = new AdminUsersManageManageFranchiseUsersPageTest();
			String userName = fc.utobj().generateTestData("Test");
			String roleName = "Default Franchise Role";
			String emailId = "fieldopsautomation@staffex.com";
			addFranUser.addFranchiseUser(driver, userName, password, franchiseId, roleName, emailId);
			System.out.println("Franchise User Name::" + userName);

			AdminFranchiseLocationAddFranchiseLocationPageTest franchiseLoc = new AdminFranchiseLocationAddFranchiseLocationPageTest();
			String franchiseeID = fc.utobj().generateTestData("TestFID");
			String centerName = fc.utobj().generateTestData("TestC");

			DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
			Date date = new Date();
			String curDate = dateFormat.format(date);
			System.out.println(curDate);

			String openingDate = curDate;
			String muId = fc.utobj().generateTestData("TestMuid");
			String regionName = fc.utobj().generateTestData("TestR");
			String storeType = fc.utobj().generateTestData("TestS");

			fc.utobj().printTestStep("Navigate To Admin > Add Area/Region");
			fc.utobj().printTestStep("Add Region");

			AdminAreaRegionAddAreaRegionPageTest addregion = new AdminAreaRegionAddAreaRegionPageTest();
			addregion.addAreaRegion(driver, regionName);

			fc.utobj().printTestStep("Navigate To Admin > Configuration > Configure Store Type Page");
			fc.utobj().printTestStep("Add Store Type");

			AdminConfigurationConfigureStoreTypePageTest addStore = new AdminConfigurationConfigureStoreTypePageTest();
			addStore.addStoreType(driver, storeType);
			String franchiseUserName = firstName + " " + lastName;

			fc.utobj().printTestStep("Add Franchise Location With MUID");

			muId = franchiseLoc.franchiseLocationWithMUID(driver, franchiseeID, regionName, centerName, openingDate,
					storeType, franchiseUserName, muId, config);

			fc.utobj().printTestStep("Add MUID User");
			String muIdUserName = fc.utobj().generateTestData("TestM");
			String firstMuidName = fc.utobj().generateTestData("firstMuidName");

			muIdUserName = addFranUser.addMUIDUser(driver, muIdUserName, password, muId, firstMuidName, config,
					emailId);

			fc.home_page().logout(driver);

			fc.utobj().printTestStep("Login With MU Id");
			fc.loginpage().loginWithParameter(driver, muIdUserName, password);

			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='franchisee_no']/span/a"));
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//*[@id='franchisee_no']/div/div/span[.='All ']"));

			// navigate to Add task

			fc.utobj().printTestStep("Navigate To FieldOps > Tasks Page");
			fc.utobj().printTestStep("Add Task For First Location");
			fc.fieldops().fieldops_common().fieldopsTasks(driver);

			FieldOpsTasksPageTest taskPage = new FieldOpsTasksPageTest();
			taskPage.addTask(driver, config, dataSet, franchiseId, subject, franchiseUserName);

			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='franchisee_no']/span/a"));
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//*[@id='franchisee_no']/div/div/span[.='All ']"));

			fc.utobj().printTestStep("Navigate To FieldOps > Tasks Page");
			fc.utobj().printTestStep("Add Task For Second Location");
			// navigate to Add Tasks
			fc.fieldops().fieldops_common().fieldopsTasks(driver);

			taskPage.addTask(driver, config, dataSet, franchiseeID, subject, franchiseUserName);

			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='franchisee_no']/span/a"));
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//*[@id='franchisee_no']/div/div/span[.='All ']"));

			// navigate to the Smart Connect Task

			fc.utobj().printTestStep("Navigate To SmartConnect Module");
			fc.home_page().openSmartConnectModule(driver);

			fc.utobj().printTestStep("Verify Task For First Location");

			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='franchisee_no']/span/a"));
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//*[@id='franchisee_no']/div/div/span[contains(text () ," + "'" + franchiseId + "'" + ")]"));

			fc.home_page().openSmartConnectModule(driver);

			fc.utobj().printTestStep("Verify Task For Second Location");
			fc.utobj().getElementByXpath(driver, ".//tr/td/ul/li[@id='task0']/a/span[.=" + "'" + subject + "'" + "]")
					.isEnabled();

			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='franchisee_no']/span/a"));
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//*[@id='franchisee_no']/div/div/span[contains(text () ," + "'" + franchiseeID + "'" + ")]"));

			fc.home_page().openSmartConnectModule(driver);

			fc.utobj().getElementByXpath(driver, ".//tr/td/ul/li[@id='task0']/a/span[.=" + "'" + subject + "'" + "]")
					.isEnabled();

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	public void filterByFranchiseID(WebDriver driver, String franchiseId) throws Exception {

		FieldOpsTasksPage pobj = new FieldOpsTasksPage(driver);

		fc.utobj().clickElement(driver, pobj.showFilter);

		fc.utobj().setToDefault(driver, pobj.selectFranIDForSearch);
		fc.utobj().selectValFromMultiSelect(driver, pobj.selectFranIDForSearch, franchiseId);
		fc.utobj().clickElement(driver, pobj.searchBtn);

		fc.utobj().clickElement(driver, pobj.hideFilter);
	}

	public void setToDefaultFilter(WebDriver driver) throws Exception {

		try {
			FieldOpsTasksPage pobj = new FieldOpsTasksPage(driver);

			fc.utobj().clickElement(driver, pobj.showFilter);
			if (!fc.utobj().isSelected(driver, pobj.lookInCheck)) {
				fc.utobj().clickElement(driver, pobj.lookInCheck);
			}
			pobj.hubSubject.clear();
			fc.utobj().setToDefault(driver, pobj.selectFranIDForSearch);
			fc.utobj().setToDefault(driver, pobj.taskStatusSelectAtFilter);
			fc.utobj().setToDefault(driver, pobj.visitFormSelectAtFilter);
			fc.utobj().setToDefault(driver, pobj.assignToSelectAtFilter);
			fc.utobj().setToDefault(driver, pobj.consultantToSelectAtFilter);
			fc.utobj().setToDefault(driver, pobj.prioritySelectAtFilter);
			fc.utobj().clickElement(driver, pobj.saveViewBtn);

			fc.utobj().clickElement(driver, pobj.hideFilter);
		} catch (Exception e) {
			Reporter.log(e.getMessage());
		}
		
		
	}

	public void filterByFielOpsSubject(WebDriver driver, String subject) throws Exception {

		FieldOpsTasksPage pobj = new FieldOpsTasksPage(driver);

		fc.utobj().clickElement(driver, pobj.showFilter);
		fc.utobj().selectDropDown(driver, pobj.selectCategory, "Field Ops");
		fc.utobj().sendKeys(driver, pobj.hubSubject, subject);
		fc.utobj().setToDefault(driver, pobj.taskStatusSelectAtFilter);
		fc.utobj().sendKeys(driver, pobj.fromDate, "");
		fc.utobj().sendKeys(driver, pobj.toDate, "");
		fc.utobj().selectDropDown(driver, pobj.selectReminder, "Select");
		fc.utobj().setToDefault(driver, pobj.prioritySelectAtFilter);
		fc.utobj().setToDefault(driver, pobj.viewSelect);
		fc.utobj().setToDefault(driver, pobj.taskTypeSelect);

		fc.utobj().clickElement(driver, pobj.hubSearchBtn);

		fc.utobj().clickElement(driver, pobj.hideFilter);
	}
}
