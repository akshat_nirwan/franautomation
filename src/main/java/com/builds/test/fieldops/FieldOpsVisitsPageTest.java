package com.builds.test.fieldops;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.Reporter;
import org.testng.annotations.Test;

import com.builds.test.admin.AdminAreaRegionAddAreaRegionPageTest;
import com.builds.test.admin.AdminConfigurationConfigureStoreTypePageTest;
import com.builds.test.admin.AdminFranchiseLocationAddFranchiseLocationPageTest;
import com.builds.test.admin.AdminHiddenLinksConfigureFieldOpsSettingsPageTest;
import com.builds.test.admin.AdminHiddenLinksConfigureNewHierarchyLevelPageTest;
import com.builds.test.admin.AdminUsersManageCorporateUsersAddCorporateUserPageTest;
import com.builds.test.admin.AdminUsersManageManageFranchiseUsersPageTest;
import com.builds.test.admin.AdminUsersManageRegionalUsersAddRegionalUserPageTest;
import com.builds.test.admin.AdminUsersRolesAddNewRolePageTest;
import com.builds.test.common.CorporateUser;
import com.builds.test.opener.OpenerStoreSummaryStoreListPageTest;
import com.builds.uimaps.fieldops.AdminFieldOpsConfigureScheduleVisitEmailContentPage;
import com.builds.uimaps.fieldops.AdminFieldOpsDeleteFieldOpsVisitsPage;
import com.builds.uimaps.fieldops.AdminFieldOpsManageVisitFormCustomizeVisitFormPage;
import com.builds.uimaps.fieldops.FieldOpsHomePage;
import com.builds.uimaps.fieldops.FieldOpsVisitsPage;
import com.builds.utilities.FranconnectUtil;
import com.builds.utilities.TestCase;

	public class FieldOpsVisitsPageTest {
	FranconnectUtil fc = new FranconnectUtil();

	@Test(groups = { "fieldops" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Create Visit at Field Ops Visits Page", testCaseId = "TC_60_Create_Visit_At_Visit")
	private void createVisitAtVisitPage() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("fieldops", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			FieldOpsVisitsPage pobj = new FieldOpsVisitsPage(driver);

			fc.utobj().printTestStep("Navigate To Admin > Fieldops > Manage Visit Form");
			fc.utobj().printTestStep("Add Private Visit Form");
			AdminFieldOpsManageVisitFormPageTest visitFormPage = new AdminFieldOpsManageVisitFormPageTest();
			String visitFormName = visitFormPage.addVisitFormPrivate(driver, dataSet);
			fc.utobj().printTestStep("Add Question");
			fc.utobj().clickElement(driver, pobj.addQuestion);
			AdminFieldOpsManageVisitFormCustomizeVisitFormPageTest customizeVisit = new AdminFieldOpsManageVisitFormCustomizeVisitFormPageTest();
			customizeVisit.addQuestion(driver, dataSet);
			fc.utobj().printTestStep("Add Question In Visit Form Rating Type");
			AdminFieldOpsQuestionLibraryPageTest questionLibrary = new AdminFieldOpsQuestionLibraryPageTest();
			fc.utobj().clickElement(driver, pobj.addQuestion);
			questionLibrary.addQuestionRatingType(driver, dataSet);
			fc.utobj().printTestStep("Add Question In Visit Form Single Select Drop Down Type");
			fc.utobj().clickElement(driver, pobj.addQuestion);
			questionLibrary.addQuestionSingleSelectDropDownType(driver, dataSet);
			fc.utobj().printTestStep("Add Question In Visit Form Single Select Radio Button Type");
			fc.utobj().clickElement(driver, pobj.addQuestion);
			questionLibrary.addQuestionSingleSelectRadioButtonType(driver, dataSet);
			fc.utobj().clickElement(driver, pobj.finishBtn);

			fc.utobj().printTestStep("Navigate To Admin > Franchise Location > Add Franchise Location");
			fc.utobj().printTestStep("Add Franchise Location");
			String regionName = fc.utobj().generateTestData("TestR");
			String storeType = fc.utobj().generateTestData("TestS");
			String franchiseId = fc.utobj().generateTestData("TestFID");
			AdminFranchiseLocationAddFranchiseLocationPageTest franchise = new AdminFranchiseLocationAddFranchiseLocationPageTest();
			franchiseId = franchise.addFranchiseLocation_All(driver, franchiseId, regionName, storeType);

			fc.utobj().printTestStep("Navigate To Admin > Users > Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUser = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();

			String emailId = "fieldopsautomation@staffex.com";
			CorporateUser corpUser = new CorporateUser();
			corpUser.setConsultant("Y");
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporateUser.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);
			fc.utobj().printTestStep("Navigate To FieldOps > Visits Page");
			fc.fieldops().fieldops_common().fieldopsVisitCreateVisitPage(driver);
			fc.utobj().printTestStep("Create Visit");
			fc.utobj().clickElement(driver, pobj.createVisit);

			fc.utobj().selectValFromMultiSelectRadioBtn(driver, pobj.selectVisitForm, visitFormName);
			fc.utobj().selectValFromMultiSelectWithoutReset(driver, pobj.selectFranchiseId, franchiseId);
			fc.utobj().selectValFromMultiSelectRadioBtn(driver, pobj.selectConsultantMultiSelect,
					corpUser.getuserFullName());
			String futureDate = fc.utobj().getFutureDateUSFormat(20);
			fc.utobj().sendKeys(driver, pobj.scheduleDateTime, futureDate);
			String comments = dataSet.get("comments");
			fc.utobj().sendKeys(driver, pobj.comments, comments);
			fc.utobj().clickElement(driver, pobj.schedule);

			fc.utobj().setToDefault(driver, pobj.selectFranchiseId);
			WebElement elementInput = fc.utobj().getElement(driver, pobj.selectFranchiseId)
					.findElement(By.xpath("./div/div/input"));
			fc.utobj().selectValFromMultiSelectWithoutReset(driver, pobj.selectFranchiseId, franchiseId);

			fc.utobj().printTestStep("Verify Visit At FieldOps Visit Page");
			boolean isStatusPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//*[.='" + franchiseId + "']/ancestor::tr/td[contains(text () ,'Scheduled')]");
			if (isStatusPresent == false) {
				fc.utobj().throwsException("Was not able to create Visit");
			}

			fc.utobj().printTestStep("Navigate To Home Page");
			FieldOpsHomePage home_page = new FieldOpsHomePage(driver);
			fc.utobj().clickElement(driver, home_page.homePageLnk);
			fc.utobj().printTestStep("Verify The Redirection Of Next 30 Days Tab's Due Visit At Home Page");
			WebElement element = fc.utobj().getElementByXpath(driver,
					".//td[contains(text () , 'Visits Due')]/ancestor::tr/td/a");
			fc.utobj().clickElement(driver, element);

			fc.utobj().selectDropDownByValue(driver, home_page.resultsPerPage, "100");

			boolean isTextPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//td[contains(text () , '" + visitFormName + "')]");
			if (isTextPresent == false) {
				fc.utobj().throwsException("was not able to verify Visit Form Name");
			}

			boolean isTextFranchiseId = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//td[contains(text () , '" + visitFormName
					+ "')]/ancestor::tr/td[contains(text () , '" + franchiseId + "')]");
			if (isTextFranchiseId == false) {
				fc.utobj().throwsException("was not able to verify Franchise Id Name");
			}

			boolean isTextConsultant = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//td[contains(text () , '" + visitFormName
					+ "')]/ancestor::tr/td[contains(text () , '" + corpUser.getuserFullName() + "')]");
			if (isTextConsultant == false) {
				fc.utobj().throwsException("was not able to verify User Name");
			}

			boolean isTextStatus = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//td[contains(text () , '" + visitFormName
					+ "')]/ancestor::tr/td[contains(text () , 'Scheduled')]");
			if (isTextStatus == false) {
				fc.utobj().throwsException("was not able to verify Status Name");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	public String createVisitAtVisitPage(WebDriver driver, Map<String, String> config, Map<String, String> dataSet,
			String visitFormName, String franchiseId, String userName) throws Exception {

		String testCaseId = "TC_Create_Visit_At_Visit_Page";
		String curDate = null;
		if (fc.utobj().validate(testCaseId)) {
			try {
				FieldOpsVisitsPage pobj = new FieldOpsVisitsPage(driver);
				fc.fieldops().fieldops_common().fieldopsVisitCreateVisitPage(driver);

				fc.utobj().clickElement(driver, pobj.createVisit);
				fc.utobj().selectValFromMultiSelectRadioBtn(driver, pobj.selectVisitForm, visitFormName);
				fc.utobj().selectValFromMultiSelectWithoutReset(driver, pobj.selectFranchiseId, franchiseId);
				fc.utobj().selectValFromMultiSelectRadioBtn(driver, pobj.selectConsultantMultiSelect, userName);

				curDate = fc.utobj().getCurrentDateUSFormat();
				fc.utobj().sendKeys(driver, pobj.scheduleDateTime, curDate);

				try {
					if (dataSet.get("franchiseLogin").equalsIgnoreCase("Yes")) {
						fc.utobj().clickElement(driver, pobj.sendMailCheckNotiFranchiseeOwner);
					//	fc.utobj().clickElement(driver, pobj.schedule);
						fc.commonMethods().switch_cboxIframe_frameId(driver);
						Thread.sleep(1000);
						fc.utobj().clickElement(driver, pobj.ownerSelect);//selectValFromMultiSelect(driver, pobj.ownerSelect, "Select All");
						fc.utobj().clickElement(driver, ".//div[@class='ms-drop bottom']//input[@name='selectGroupOwnersCombo']");
						fc.utobj().clickElement(driver, pobj.sendBtn);
						fc.utobj().switchFrameToDefault(driver);
					}
				} catch (Exception e) {
					Reporter.log(e.getMessage());
				}
				
				fc.utobj().clickElement(driver, pobj.schedule);
				defaultViewByFranchiseId(driver, franchiseId);

				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException("was not able to create Visit At Visit Page :: Field Ops > Visits Page");

		}
		return curDate;
	}

	public String createVisitAtInfoMgrQAHistory(WebDriver driver, Map<String, String> config,
			Map<String, String> dataSet, String visitFormName, String franchiseId, String userName) throws Exception {

		String testCaseId = "TC_create_Visit_At_InfoMgr_QA_History";
		String curDate = null;

		if (fc.utobj().validate(testCaseId)) {
			try {
				FieldOpsVisitsPage pobj = new FieldOpsVisitsPage(driver);

				// Navigate

				fc.infomgr().infomgr_common().fimModule(driver);

				// QA History
				fc.utobj().clickElement(driver, pobj.franchisees);

				fc.utobj().sendKeys(driver, pobj.searchFranchisee, franchiseId);
				fc.utobj().clickElement(driver, pobj.topSearchBtn);

				fc.utobj().clickElement(driver, fc.utobj().getElementByPartialLinkText(driver, franchiseId));

				fc.utobj().clickElement(driver, pobj.qaHistory);

				// Click createVisitForm
				fc.utobj().clickElement(driver, pobj.createVisit);

				// select visit Form
				fc.utobj().clickElement(driver, pobj.selectClick);
				fc.utobj().sendKeys(driver, pobj.searchBox, visitFormName);
				fc.utobj().clickElement(driver,
						fc.utobj().getElementByXpath(driver,
								".//*[@id='ms-parentmasterVisitId']/div/ul/li/label[contains(text () ," + "'"
										+ visitFormName + "'" + ")]/input"));

				// Select Consultant
				fc.utobj().clickElement(driver, pobj.selectConsultant);
				fc.utobj().sendKeys(driver, pobj.consultantSearchBox, userName);
				fc.utobj().clickElement(driver,
						fc.utobj().getElementByXpath(driver,
								".//*[@id='ms-parentassignTo']/div/ul/li/label[contains(text () , " + "'" + userName
										+ "'" + ")]/input"));

				curDate = fc.utobj().getCurrentDateUSFormat();
				fc.utobj().sendKeys(driver, pobj.scheduleDateTime, curDate);

				String comments = dataSet.get("comments");
				fc.utobj().sendKeys(driver, pobj.comments, comments);

				try {
					if (dataSet.get("franchiseLogin").equalsIgnoreCase("Yes")) {
						fc.utobj().clickElement(driver, pobj.sendMailCheckNotiFranchiseeOwner);
					}
				} catch (Exception e) {

				}

				fc.utobj().clickElement(driver, pobj.schedule);

				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException("was not able to create Visit At Info Mgr QA History Page");

		}
		return curDate;
	}

	public String createVisitAtFranchiseVisitPage(WebDriver driver, Map<String, String> config,
			Map<String, String> dataSet, String visitFormName, String franchiseId, String userName) throws Exception {

		String testCaseId = "TC_create_Visit_At_Franchise_Visit_Page";

		if (fc.utobj().validate(testCaseId)) {
			try {
				FieldOpsVisitsPage pobj = new FieldOpsVisitsPage(driver);

				fc.fieldops().fieldops_common().fieldopsVisitCreateVisitPage(driver);

				// Click createVisitForm
				fc.utobj().clickElement(driver, pobj.createVisit);

				// select visit Form
				fc.utobj().clickElement(driver, pobj.selectClick);
				fc.utobj().sendKeys(driver, pobj.searchBox, visitFormName);
				fc.utobj().clickElement(driver,
						fc.utobj().getElementByXpath(driver,
								".//*[@id='ms-parentmasterVisitId']/div/ul/li/label[contains(text () ," + "'"
										+ visitFormName + "'" + ")]/input"));

				// select FranchiseID
				fc.utobj().clickElement(driver, pobj.franchiseeSelect);
				fc.utobj().sendKeys(driver, pobj.searchBoxFranchisee, franchiseId);
				fc.utobj().clickElement(driver,
						fc.utobj().getElementByXpath(driver,
								".//*[@id='ms-parentfranchiseeNo']/div/ul/li/label[contains(text () , " + "'"
										+ franchiseId + "'" + ")]/input"));
				fc.utobj().clickElement(driver, pobj.franchiseeSelect);

				// schedule Date
				DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
				Date date = new Date();
				String curDate = dateFormat.format(date);
				System.out.println(curDate);

				fc.utobj().sendKeys(driver, pobj.scheduleDateTime, curDate);

				// fc.utobj().clickElement(driver,
				// fc.utobj().getElementByXpath(driver,".//*[@id='calendar']/table/tbody/tr[1]/td/table/tbody/tr/td[2]/a/img")));

				String comments = dataSet.get("comments");
				fc.utobj().sendKeys(driver, pobj.comments, comments);

				if (fc.utobj().isSelected(driver,pobj.includeVisitPractise)) {
					fc.utobj().clickElement(driver, pobj.includeVisitPractise);
				}

				fc.utobj().clickElement(driver, pobj.schedule);

				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException("was not able to create Visit Franchise User Page");

		}
		return visitFormName;
	}

	@Test(groups = { "fieldops", "fieldops0808" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify MUID while creating any visit", testCaseId = "TC_63_Verify_Muid_Create_Visit")
	private void verifyMuidCreateVisit() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());
		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("fieldops", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			FieldOpsVisitsPage pobj = new FieldOpsVisitsPage(driver);
			String password = "t0n1ght123";

			fc.utobj().printTestStep("Navigate To Admin > Users > Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");

			fc.utobj().printTestStep("Navigate To Admin > FieldOps > Manage Visit Form");
			fc.utobj().printTestStep("Add Non-Private Visit Form With Tab View");

			AdminFieldOpsManageVisitFormPageTest visitFormPage = new AdminFieldOpsManageVisitFormPageTest();
			String visitFormName = visitFormPage.addVisitFormTabViewPrivateNo(driver, config, dataSet);

			fc.utobj().printTestStep("Add Tab In Visit Form");
			AdminFieldOpsManageVisitFormCustomizeVisitFormPageTest customizeVisit = new AdminFieldOpsManageVisitFormCustomizeVisitFormPageTest();
			customizeVisit.addTab(driver, config, dataSet);

			fc.utobj().printTestStep("Add Question In Tab");
			fc.utobj().clickElement(driver, pobj.addQuestion);
			customizeVisit.addQuestion(driver, dataSet);
			fc.utobj().clickElement(driver, pobj.finishBtn);

			fc.utobj().printTestStep("Navigate To Admin > Franchise Location > Add Franchise Location");
			fc.utobj().printTestStep("Add Franchise Location With Muid");
			AdminFranchiseLocationAddFranchiseLocationPageTest addLoc = new AdminFranchiseLocationAddFranchiseLocationPageTest();
			String franchiseId = fc.utobj().generateTestData("TestFID");
			String regionName1 = fc.utobj().generateTestData("TestR");
			String storeType1 = fc.utobj().generateTestData("TestS");
			String firstName = fc.utobj().generateTestData("firstName");
			String lastName = "lastName";
			franchiseId = addLoc.addFranchiseLocation_AllMuid(driver, franchiseId, regionName1, storeType1, firstName,
					lastName, config);

			fc.utobj().printTestStep("Add Franchise User");
			AdminUsersManageManageFranchiseUsersPageTest addFranUser = new AdminUsersManageManageFranchiseUsersPageTest();
			String userName = fc.utobj().generateTestData("Test");
			String roleName = "Default Franchise Role";
			String emailId = "fieldopsautomation@staffex.com";
			addFranUser.addFranchiseUser(driver, userName, password, franchiseId, roleName, emailId);

			AdminFranchiseLocationAddFranchiseLocationPageTest franchiseLoc = new AdminFranchiseLocationAddFranchiseLocationPageTest();
			String franchiseeID = fc.utobj().generateTestData("TestFID");
			String centerName = fc.utobj().generateTestData("TestC");

			String curDate = fc.utobj().getCurrentDateUSFormat();
			String openingDate = curDate;
			String muId = fc.utobj().generateTestData("TestMuid");
			String regionName = fc.utobj().generateTestData("TestR");
			String storeType = fc.utobj().generateTestData("TestS");

			fc.utobj().printTestStep("Navigate To Admin > Add Area/Region");
			fc.utobj().printTestStep("Add Region");

			AdminAreaRegionAddAreaRegionPageTest addregion = new AdminAreaRegionAddAreaRegionPageTest();
			addregion.addAreaRegion(driver, regionName);

			fc.utobj().printTestStep("Add Store Type");
			AdminConfigurationConfigureStoreTypePageTest addStore = new AdminConfigurationConfigureStoreTypePageTest();
			addStore.addStoreType(driver, storeType);
			String franchiseUserName = firstName + " " + lastName;

			fc.utobj().printTestStep("Add Franchise Location With MUID");
			muId = franchiseLoc.franchiseLocationWithMUID(driver, franchiseeID, regionName, centerName, openingDate,
					storeType, franchiseUserName, muId, config);

			fc.utobj().printTestStep("Add MUID User");
			String muIdUserName = fc.utobj().generateTestData("TestM");
			String firstMuidName = fc.utobj().generateTestData("firstMuidName");
			muIdUserName = addFranUser.addMUIDUser(driver, muIdUserName, password, muId, firstMuidName, config,
					emailId);

			fc.home_page().logout(driver);

			fc.utobj().printTestStep("Login With MU User");
			fc.loginpage().loginWithParameter(driver, muIdUserName, password);

			fc.utobj().printTestStep("Navigate To Field Ops > Visits Page");
			fc.fieldops().fieldops_common().fieldopsVisitCreateVisitPage(driver);

			fc.utobj().printTestStep("Create Visit");
			createVisitAtFranchiseVisitPage(driver, config, dataSet, visitFormName, franchiseId, muIdUserName);

			fc.utobj().getElementByID(driver, "visitNumber").clear();

			fc.utobj().printTestStep("Verify Create Visit");
			boolean isStatusPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//*[contains(text () ,'" + franchiseId + "')]/ancestor::tr/td[contains(text () ,'Scheduled')]");
			if (isStatusPresent == false) {
				fc.utobj().throwsException("Was not able to verify muid while creating visit");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "fieldops")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify the Start Visit at Field Ops Visits Page", testCaseId = "TC_61_Start_Visit_At_Visit")
	private void startVisitAtVisitPage() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("fieldops", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			FieldOpsVisitsPage pobj = new FieldOpsVisitsPage(driver);

			fc.utobj().printTestStep("Navigate To Admin > Hidden Links > Configure FieldOps Settings Page");
			fc.utobj().printTestStep("Configure Field Ops Settings Log Private Comment Yes");

			AdminHiddenLinksConfigureFieldOpsSettingsPageTest hiddenLinks = new AdminHiddenLinksConfigureFieldOpsSettingsPageTest();
			hiddenLinks.configureFieldOpsSettingsLogPrivateCommentYes(driver, config);

			fc.utobj().printTestStep("Navigate To Admin > Fieldops > Manage Visit Form");
			fc.utobj().printTestStep("Add Private Visit Form");
			AdminFieldOpsManageVisitFormPageTest visitFormPage = new AdminFieldOpsManageVisitFormPageTest();
			String visitFormName = visitFormPage.addVisitFormPrivate(driver, dataSet);

			fc.utobj().printTestStep("Add Question In Visit Form");
			fc.utobj().clickElement(driver, pobj.addQuestion);
			AdminFieldOpsManageVisitFormCustomizeVisitFormPageTest customizeVisit = new AdminFieldOpsManageVisitFormCustomizeVisitFormPageTest();
			customizeVisit.addQuestion(driver, dataSet);
			fc.utobj().clickElement(driver, pobj.finishBtn);

			fc.utobj().printTestStep("Navigate To Admin > Franchise Location > Add Franchise Location");
			fc.utobj().printTestStep("Add Franchise Location");
			String regionName = fc.utobj().generateTestData("TestR");
			String storeType = fc.utobj().generateTestData("TestS");
			String franchiseId = fc.utobj().generateTestData("TestID");
			AdminFranchiseLocationAddFranchiseLocationPageTest franchise = new AdminFranchiseLocationAddFranchiseLocationPageTest();
			franchiseId = franchise.addFranchiseLocation_All(driver, franchiseId, regionName, storeType);

			fc.utobj().printTestStep("Navigate To Admin > Users > Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUser = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			String emailId = "fieldopsautomation@staffex.com";
			CorporateUser corpUser = new CorporateUser();
			corpUser.setConsultant("Y");
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporateUser.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			fc.utobj().printTestStep("Navigate To FieldOps > Visits Page");
			fc.utobj().printTestStep("Create Visit");
			createVisitAtVisitPage(driver, config, dataSet, visitFormName, franchiseId, corpUser.getuserFullName());

			fc.utobj().printTestStep("Verify The More Link Of Scheduled/Pending Visits At Home Page");
			fc.utobj().printTestStep("Navigate To Home Page");
			FieldOpsHomePage home_page = new FieldOpsHomePage(driver);
			fc.utobj().clickElement(driver, home_page.homePageLnk);

			try {
				fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
						".//a[contains(text () , '" + franchiseId + "')]/../preceding-sibling::td/a"));

				boolean isFranchisePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
						".//td[contains(text () , 'Franchise ID')]/following-sibling::td[.='" + franchiseId + "']");
				if (isFranchisePresent == false) {
					fc.utobj().throwsException("was not able to verify the franchise Id");
				}

				boolean isVisitFormPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
						".//td[contains(text () , 'Visit Form')]/following-sibling::td[.='" + visitFormName + "']");
				if (isVisitFormPresent == false) {
					fc.utobj().throwsException("was not able to verify the Visit Form");
				}

				boolean isConsultantFormPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
						".//td[contains(text () , 'Consultant')]/following-sibling::td[.='" + corpUser.getuserFullName()
								+ "']");
				if (isConsultantFormPresent == false) {
					fc.utobj().throwsException("was not able to verify the Consultant");
				}

				fc.utobj().printTestStep("Verify The Franchise Id Redirection");
				fc.utobj().clickElement(driver, home_page.homePageLnk);
				fc.utobj().clickElement(driver,
						fc.utobj().getElementByXpath(driver, ".//a[contains(text () , '" + franchiseId + "')]"));

				boolean isTextIsPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
						".//td[contains(text () , 'Store Type')]/following-sibling::td[contains(text () , '" + storeType
								+ "')]");
				if (isTextIsPresent == false) {
					fc.utobj().throwsException("was not able to verify the Franchise Rediresction");
				}

				fc.fieldops().fieldops_common().fieldopsVisit(driver);

			} catch (Exception e) {

				WebElement element = fc.utobj().getElementByXpath(driver,
						".//td[contains(text () , 'Scheduled/Pending Visits')]/ancestor::tr/td/div/div/a[.='More']");
				fc.utobj().clickElement(driver, element);

				fc.utobj().selectDropDownByValue(driver, home_page.resultsPerPage, "100");

				boolean isTextPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
						".//td[contains(text () , '" + visitFormName + "')]");
				if (isTextPresent == false) {
					fc.utobj().throwsException("was not able to verify Visit Form Name");
				}

				boolean isTextFranchiseId = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//td[contains(text () , '" + visitFormName
						+ "')]/ancestor::tr/td[contains(text () , '" + franchiseId + "')]");
				if (isTextFranchiseId == false) {
					fc.utobj().throwsException("was not able to verify Visit Form Name");
				}

				boolean isTextConsultant = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//td[contains(text () , '" + visitFormName
						+ "')]/ancestor::tr/td[contains(text () , '" + corpUser.getuserFullName() + "')]");
				if (isTextConsultant == false) {
					fc.utobj().throwsException("was not able to verify Visit Form Name");
				}

				boolean isTextStatus = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//td[contains(text () , '" + visitFormName
						+ "')]/ancestor::tr/td[contains(text () , 'Scheduled')]");
				if (isTextStatus == false) {
					fc.utobj().throwsException("was not able to verify Visit Form Name");
				}

			}

			fc.utobj().clickElement(driver, pobj.visitPageLnk);
			defaultViewByFranchiseId(driver, franchiseId);

			// start Visit
			fc.utobj().printTestStep("Start Visit");
			actionImgOption(driver, franchiseId, "Start Visit");

			String text = fc.utobj().getElementByXpath(driver, ".//table[@class='bText12gr']//tr/td/label")
					.getAttribute("id").trim();
			text = text.replace("_0", "");

			fc.utobj().sendKeys(driver,
					fc.utobj().getElementByXpath(driver, ".//textarea[@id='Response_" + text + "']"),
					"It is a sample answer");
			fc.utobj().sendKeys(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='private" + text + "']"),
					"It is a private Answer");
			fc.utobj().clickElement(driver, pobj.nextBtnSubmit);

			fc.utobj().clickElement(driver, pobj.submitBtnAtVisit);
			fc.utobj().clickElement(driver, pobj.okBtn);

			defaultViewByFranchiseId(driver, franchiseId);

			fc.utobj().printTestStep("Verify Visit Status");
			boolean isStatusPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//*[.='" + franchiseId + "']/ancestor::tr/td[contains(text () ,'In Process')]");
			if (isStatusPresent == false) {
				fc.utobj().throwsException("was not able to veriy Status of visit");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	public void startVisitWithCompleteStatus(WebDriver driver, Map<String, String> config, Map<String, String> dataSet,
			String franchiseId) throws Exception {

		String testCaseId = "TC_Start_Visit_With_Complete_Status";

		if (fc.utobj().validate(testCaseId)) {
			try {
				FieldOpsVisitsPage pobj = new FieldOpsVisitsPage(driver);

				fc.fieldops().fieldops_common().fieldopsVisitCreateVisitPage(driver);

				defaultViewByFranchiseId(driver, franchiseId);

				actionImgOption(driver, franchiseId, "Start Visit");

				String text = fc.utobj().getElementByXpath(driver, ".//table[@class='bText12gr']//tr/td/label")
						.getAttribute("id").trim();
				text = text.replace("_0", "");
				System.out.println(text);

				fc.utobj().sendKeys(driver,
						fc.utobj().getElementByXpath(driver, ".//textarea[@id='Response_" + text + "']"),
						"It is a sample answer");
				fc.utobj().sendKeys(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='private" + text + "']"),
						"It is a private Answer");
				fc.utobj().clickElement(driver, pobj.submitAllBtn);

				fc.utobj().clickElement(driver, pobj.submitBtnAtVisit);
				fc.utobj().clickElement(driver, pobj.okBtn);

				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException("Was not able to start visit");

		}
	}

	@Test(groups = "fieldops")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify the Update Visit at Field Ops Visits Page", testCaseId = "TC_62_Update_Visit_At_Visit")
	private void updateVisitAtVisitPage() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("fieldops", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			FieldOpsVisitsPage pobj = new FieldOpsVisitsPage(driver);

			fc.utobj().printTestStep("Navigate To Admin > Hidden Links > Configure FieldOps Settings Page");
			fc.utobj().printTestStep("Configure Field Ops Settings Log Private Comment Yes");
			AdminHiddenLinksConfigureFieldOpsSettingsPageTest hiddenLinks = new AdminHiddenLinksConfigureFieldOpsSettingsPageTest();
			hiddenLinks.configureFieldOpsSettingsLogPrivateCommentYes(driver, config);

			fc.utobj().printTestStep("Navigate To Admin > Fieldops > Manage Visit Form");
			fc.utobj().printTestStep("Add Private Visit Form");
			AdminFieldOpsManageVisitFormPageTest visitFormPage = new AdminFieldOpsManageVisitFormPageTest();
			String visitFormName = visitFormPage.addVisitFormPrivate(driver, dataSet);

			fc.utobj().printTestStep("Add Question In Visit Form");
			fc.utobj().clickElement(driver, pobj.addQuestion);
			AdminFieldOpsManageVisitFormCustomizeVisitFormPageTest customizeVisit = new AdminFieldOpsManageVisitFormCustomizeVisitFormPageTest();
			customizeVisit.addQuestion(driver, dataSet);
			fc.utobj().clickElement(driver, pobj.finishBtn);

			fc.utobj().printTestStep("Navigate To Admin > Franchise Location > Add Franchise Location");
			fc.utobj().printTestStep("Add Franchise Location");
			String regionName = fc.utobj().generateTestData("Test");
			String storeType = fc.utobj().generateTestData("Test");
			String franchiseId = fc.utobj().generateTestData("Test");
			AdminFranchiseLocationAddFranchiseLocationPageTest franchise = new AdminFranchiseLocationAddFranchiseLocationPageTest();
			franchiseId = franchise.addFranchiseLocation_All(driver, franchiseId, regionName, storeType);

			fc.utobj().printTestStep("Navigate To Admin > Users > Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUser = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			String emailId = "fieldopsautomation@staffex.com";
			CorporateUser corpUser = new CorporateUser();
			corpUser.setConsultant("Y");
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporateUser.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			fc.utobj().printTestStep("Navigate To FieldOps > Visits Page");
			fc.utobj().printTestStep("Create Visit");
			createVisitAtVisitPage(driver, config, dataSet, visitFormName, franchiseId, corpUser.getuserFullName());

			startVisitWithCompleteStatus(driver, config, dataSet, franchiseId);

			defaultViewByFranchiseId(driver, franchiseId);

			fc.utobj().printTestStep("Update Visit");
			actionImgOption(driver, franchiseId, "Update Visit");

			String text = fc.utobj().getElementByXpath(driver, ".//table[@class='bText12gr']//tr/td/label")
					.getAttribute("id").trim();
			text = text.replace("_0", "");

			String comentsVisit = "It is a sample answer updated";
			String privateCommentVisit = "It is a private Answer updated";
			fc.utobj().sendKeys(driver,
					fc.utobj().getElementByXpath(driver, ".//textarea[@id='Response_" + text + "']"), comentsVisit);
			fc.utobj().sendKeys(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='private" + text + "']"),
					privateCommentVisit);
			fc.utobj().clickElement(driver, pobj.submitAllBtn);
			fc.utobj().clickElement(driver, pobj.submitBtnAtVisit);

			text = fc.utobj().getElementByXpath(driver, ".//table[@class='bText12gr']//tr/td/label").getAttribute("id")
					.trim();
			text = text.replace("_0", "");

			String comments = fc.utobj()
					.getElementByXpath(driver, ".//*[@id='" + text + "_0']/../../following-sibling::tr[1]/td[1]")
					.getText().trim();
			String privateComm = fc.utobj()
					.getElementByXpath(driver, ".//*[@id='" + text + "_0']/../../following-sibling::tr[2]/td[1]")
					.getText().trim();

			fc.utobj().printTestStep("Verify Update Visit");
			if (comentsVisit.equalsIgnoreCase(comments) && privateCommentVisit.equalsIgnoreCase(privateComm)) {
				// do nothing
			} else {
				fc.utobj().throwsException("Was not able to Updated Visit");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
			;
		}
	}

	@Test(groups = "fieldopsTest")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify Visit Summary At Visits Page", testCaseId = "TC_64_Visit_Summary_At_Visit")
	private void visitSummary() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("fieldops", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			FieldOpsVisitsPage pobj = new FieldOpsVisitsPage(driver);

			fc.utobj().printTestStep("Navigate To Admin > Hidden Links > Configure FieldOps Settings Page");
			fc.utobj().printTestStep("Configure Field Ops Settings Log Private Comment Yes");
			AdminHiddenLinksConfigureFieldOpsSettingsPageTest hiddenLinks = new AdminHiddenLinksConfigureFieldOpsSettingsPageTest();
			hiddenLinks.configureFieldOpsSettingsLogPrivateCommentYes(driver, config);

			fc.utobj().printTestStep("Configure Score Grading No Label Yes");
			AdminFieldOpsConfigureScorePageTest scorePageTest = new AdminFieldOpsConfigureScorePageTest();
			scorePageTest.configureScoreGradingNoLabelYes(driver, config, dataSet);

			fc.utobj().printTestStep("Navigate To Admin > Fieldops > Manage Visit Form");
			fc.utobj().printTestStep("Add Private Visit Form");
			AdminFieldOpsManageVisitFormPageTest visitFormPage = new AdminFieldOpsManageVisitFormPageTest();
			String visitFormName = visitFormPage.addVisitFormPrivate(driver, dataSet);

			fc.utobj().printTestStep("Add Question In Visit Form");
			fc.utobj().clickElement(driver, pobj.addQuestion);
			AdminFieldOpsManageVisitFormCustomizeVisitFormPageTest customizeVisit = new AdminFieldOpsManageVisitFormCustomizeVisitFormPageTest();
			customizeVisit.addQuestion(driver, dataSet);

			fc.utobj().printTestStep("Add Rating Type Question In Visit Form");
			AdminFieldOpsQuestionLibraryPageTest questionLibrary = new AdminFieldOpsQuestionLibraryPageTest();
			fc.utobj().clickElement(driver, pobj.addQuestion);
			questionLibrary.addQuestionRatingType(driver, dataSet);

			fc.utobj().printTestStep("Add Single Select Drop Down Type Question In Visit Form");
			fc.utobj().clickElement(driver, pobj.addQuestion);
			questionLibrary.addQuestionSingleSelectDropDownType(driver, dataSet);

			fc.utobj().printTestStep("Add Single Select Radio Button Type Question");
			fc.utobj().clickElement(driver, pobj.addQuestion);
			questionLibrary.addQuestionSingleSelectRadioButtonType(driver, dataSet);
			fc.utobj().clickElement(driver, pobj.finishBtn);

			fc.utobj().printTestStep("Navigate To Admin > Franchise Location > Add Franchise Location");
			fc.utobj().printTestStep("Add Franchise Location");
			String regionName = fc.utobj().generateTestData("Test");
			String storeType = fc.utobj().generateTestData("Test");
			String franchiseId = fc.utobj().generateTestData("Test");
			AdminFranchiseLocationAddFranchiseLocationPageTest franchise = new AdminFranchiseLocationAddFranchiseLocationPageTest();
			franchiseId = franchise.addFranchiseLocation_All(driver, franchiseId, regionName, storeType);

			fc.utobj().printTestStep("Navigate To Admin > Users > Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUser = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();

			String emailId = "fieldopsautomation@staffex.com";
			CorporateUser corpUser = new CorporateUser();
			corpUser.setConsultant("Y");
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporateUser.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			fc.utobj().printTestStep("Navigate To FieldOps > Visits Page");
			fc.utobj().printTestStep("Create Visit");
			createVisitAtVisitPage(driver, config, dataSet, visitFormName, franchiseId, corpUser.getuserFullName());

			fc.utobj().printTestStep("Start Visit");
			actionImgOption(driver, franchiseId, "Start Visit");

			// question

			fc.utobj().sendKeys(driver,
					fc.utobj().getElementByXpath(driver,
							".//div[@id='formDiv']//table[@class='bText12gr']/../table[1]//tr[2]/td/textarea"),
					"It is comment box");
			fc.utobj().sendKeys(driver,
					fc.utobj().getElementByXpath(driver,
							".//div[@id='formDiv']//table[@class='bText12gr']/../table[1]//tr[3]/td/textarea"),
					"It is a private comment box");

			fc.utobj()
					.sendKeys(driver,
							fc.utobj().getElementByXpath(driver,
									".//div[@id='formDiv']//table[@class='bText12gr']/../table[2]//tr[2]/td/input"),
							"60.00");
			fc.utobj().sendKeys(driver,
					fc.utobj().getElementByXpath(driver,
							".//div[@id='formDiv']//table[@class='bText12gr']/../table[2]//tr[3]/td/textarea"),
					"2 nd private Comment");

			fc.utobj()
					.selectDropDownByIndex(driver,
							fc.utobj().getElementByXpath(driver,
									".//div[@id='formDiv']//table[@class='bText12gr']/../table[3]//tr[2]/td/select"),
							1);
			fc.utobj().sendKeys(driver,
					fc.utobj().getElementByXpath(driver,
							".//div[@id='formDiv']//table[@class='bText12gr']/../table[3]//tr[3]/td/textarea"),
					"3rd question Comment box");
			fc.utobj().sendKeys(driver,
					fc.utobj().getElementByXpath(driver,
							".//div[@id='formDiv']//table[@class='bText12gr']/../table[3]//tr[4]/td/textarea"),
					"3rd private box");

			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//div[@id='formDiv']//table[@class='bText12gr']/../table[4]//tr[2]/td/label[1]/input/../label[contains(text (), 'Yes')]"));
			fc.utobj().sendKeys(driver,
					fc.utobj().getElementByXpath(driver,
							".//div[@id='formDiv']//table[@class='bText12gr']/../table[4]//tr[3]/td/textarea"),
					"4th comment box");
			fc.utobj().sendKeys(driver,
					fc.utobj().getElementByXpath(driver,
							".//div[@id='formDiv']//table[@class='bText12gr']/../table[4]//tr[4]/td/textarea"),
					"4th private box");

			fc.utobj().clickElement(driver, pobj.submitAllBtn);
			fc.utobj().clickElement(driver, pobj.submitBtnAtVisit);
			fc.utobj().clickElement(driver, pobj.okBtn);

			fc.utobj().printTestStep("Verify Visit Summary");
			// Visit Summary
			defaultViewByFranchiseId(driver, franchiseId);
			actionImgOption(driver, franchiseId, "Visit Summary");

			String franchiseIdText = fc.utobj()
					.getElementByXpath(driver, ".//*[.='Franchise ID:']/following-sibling::td").getText().trim();
			String visitStatusText = fc.utobj()
					.getElementByXpath(driver, ".//*[contains(text () ,'Status')]/following-sibling::td[1]").getText()
					.trim();
			String consultantText = fc.utobj().getElementByXpath(driver, ".//*[.='Consultant :']/following-sibling::td")
					.getText().trim();
			String visitFormNameText = fc.utobj()
					.getElementByXpath(driver, ".//*[.='Visit Form :']/following-sibling::td").getText().trim();

			if (!franchiseIdText.contains(franchiseId)) {
				fc.utobj().throwsException("Was not able to verify the Franchise Id");
			}

			if (!visitStatusText.equalsIgnoreCase("Completed")) {
				fc.utobj().throwsException("Was not able to verify the Status Of Visit");
			}

			if (!consultantText.contains(corpUser.getuserFullName())) {
				fc.utobj().throwsException("Was not able to verify the Consultant Of Visit");
			}

			if (!visitFormNameText.equalsIgnoreCase(visitFormName)) {
				fc.utobj().throwsException("Was not able to verify the Visit Form Name");
			}

			fc.utobj().printTestStep("Navigate to FieldOps Home Page");
			FieldOpsHomePage pobj1 = new FieldOpsHomePage(driver);
			fc.utobj().clickElement(driver, pobj1.homePageLnk);
			fc.utobj().printTestStep("Verify The Redirection Of YTD Tab's Completed At Home Page");
			WebElement element = fc.utobj().getElementByXpath(driver,
					".//td[contains(text () , 'Visits Completed')]/ancestor::tr/td/a");
			fc.utobj().clickElement(driver, element);

			fc.utobj().selectDropDownByValue(driver, pobj1.resultsPerPage, "100");

			boolean isTextPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//td[contains(text () , '" + visitFormName + "')]");
			if (isTextPresent == false) {
				fc.utobj().throwsException("was not able to verify Visit Form Name");
			}

			boolean isTextFranchiseId = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//td[contains(text () , '" + visitFormName
					+ "')]/ancestor::tr/td[contains(text () , '" + franchiseId + "')]");
			if (isTextFranchiseId == false) {
				fc.utobj().throwsException("was not able to verify Visit Form Name");
			}

			boolean isTextConsultant = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//td[contains(text () , '" + visitFormName
					+ "')]/ancestor::tr/td[contains(text () , '" + corpUser.getuserFullName() + "')]");
			if (isTextConsultant == false) {
				fc.utobj().throwsException("was not able to verify Visit Form Name");
			}

			boolean isTextStatus = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//td[contains(text () , '" + visitFormName
					+ "')]/ancestor::tr/td[contains(text () , 'Completed')]");
			if (isTextStatus == false) {
				fc.utobj().throwsException("was not able to verify Visit Form Name");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "fieldopsTest")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify Modify , Cancel And Delete Of Visit", testCaseId = "TC_65_Modify_Visit_At_Visit")
	private void modifyVisit() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("fieldops", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			FieldOpsVisitsPage pobj = new FieldOpsVisitsPage(driver);

			fc.utobj().printTestStep("Navigate To Admin > Hidden Links > Configure FieldOps Settings Page");
			fc.utobj().printTestStep("Configure Field Ops Settings Log Private Comment Yes");
			AdminHiddenLinksConfigureFieldOpsSettingsPageTest hiddenLinks = new AdminHiddenLinksConfigureFieldOpsSettingsPageTest();
			hiddenLinks.configureFieldOpsSettingsLogPrivateCommentYes(driver, config);

			fc.utobj().printTestStep("Navigate To Admin > Fieldops > Manage Visit Form");
			fc.utobj().printTestStep("Add Private Visit Form");
			AdminFieldOpsManageVisitFormPageTest visitFormPage = new AdminFieldOpsManageVisitFormPageTest();
			String visitFormName = visitFormPage.addVisitFormPrivate(driver, dataSet);

			fc.utobj().printTestStep("Add Question In Visit Form");
			fc.utobj().clickElement(driver, pobj.addQuestion);
			AdminFieldOpsManageVisitFormCustomizeVisitFormPageTest customizeVisit = new AdminFieldOpsManageVisitFormCustomizeVisitFormPageTest();
			customizeVisit.addQuestion(driver, dataSet);
			fc.utobj().clickElement(driver, pobj.finishBtn);

			fc.utobj().printTestStep("Navigate To Admin > Franchise Location > Add Franchise Location");
			fc.utobj().printTestStep("Add Franchise Location");
			String regionName = fc.utobj().generateTestData("Test");
			String storeType = fc.utobj().generateTestData("Test");
			String franchiseId = fc.utobj().generateTestData("Test");
			AdminFranchiseLocationAddFranchiseLocationPageTest franchise = new AdminFranchiseLocationAddFranchiseLocationPageTest();
			franchiseId = franchise.addFranchiseLocation_All(driver, franchiseId, regionName, storeType);

			fc.utobj().printTestStep("Navigate To Admin > Users > Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUser = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();

			String emailId = "fieldopsautomation@staffex.com";
			CorporateUser corpUser = new CorporateUser();
			corpUser.setConsultant("Y");
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporateUser.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			fc.utobj().printTestStep("Navigate To FieldOps > Visits Page");
			fc.utobj().printTestStep("Create Visit");
			createVisitAtVisitPage(driver, config, dataSet, visitFormName, franchiseId, corpUser.getuserFullName());

			fc.utobj().printTestStep("Verify The Visit No Redirection");
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//td[contains(text () , '" + visitFormName + "')]/preceding-sibling::td/a"));

			boolean isFranchiseIdPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () , 'Franchise ID')]/following-sibling::td[contains(text () , '" + franchiseId
							+ "')]");
			if (isFranchiseIdPresent == false) {
				fc.utobj().throwsException("was not able to verify The Franchise Id");
			}

			boolean isVisitFormPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () , 'Visit Form')]/following-sibling::td[contains(text () , '" + visitFormName
							+ "')]");
			if (isVisitFormPresent == false) {
				fc.utobj().throwsException("was not able to verify The Visit Form Name");
			}

			boolean isConsultantPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () , 'Consultant ')]/following-sibling::td[contains(text () , '"
							+ corpUser.getuserFullName() + "')]");
			if (isConsultantPresent == false) {
				fc.utobj().throwsException("was not able to verify The Consultant");
			}

			fc.utobj().printTestStep("verify The Franchise Id Redirection");
			fc.utobj().clickElement(driver, pobj.visitPageLnk);
			defaultViewByFranchiseId(driver, franchiseId);

			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//*[contains(text () , '"
					+ visitFormName + "')]/following-sibling::td/a[contains(text () , '" + franchiseId + "')]"));

			boolean isStoreTypePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () , 'Store Type')]/following-sibling::td[contains(text () , '" + storeType
							+ "')]");
			if (isStoreTypePresent == false) {
				fc.utobj().throwsException("was not able to verify franchise Redirection");
			}

			fc.fieldops().fieldops_common().fieldopsVisit(driver);
			defaultViewByFranchiseId(driver, franchiseId);

			fc.utobj().printTestStep("Modify Visit");
			actionImgOption(driver, franchiseId, "Modify");

			fc.utobj().sendKeys(driver, pobj.mNotes, "It is a Sample Visit Updated");
			fc.utobj().selectDropDown(driver, pobj.selectStatus, "Cancelled");
			fc.utobj().clickElement(driver, pobj.submitBtnM);

			defaultViewByFranchiseId(driver, franchiseId);

			boolean isStatusOfTaskPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () , '" + franchiseId + "')]/ancestor::tr/td[contains(text () , 'Cancelled')]");

			if (isStatusOfTaskPresent == false) {
				fc.utobj().throwsException("was not able to verify The Cancelled Status Of Visit");
			}

			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//tr/td/a[contains(text () ," + "'"
					+ franchiseId + "'" + ")]/..//following-sibling::td/div[@id='menuBar']/layer/a/img"));
			String alterText = fc.utobj()
					.getElementByXpath(driver,
							".//tr/td/a[contains(text () ," + "'" + franchiseId + "'"
									+ ")]/..//following-sibling::td/div[@id='menuBar']/layer")
					.getAttribute("id").trim();
			alterText = alterText.replace("Actions_dynamicmenu", "");
			alterText = alterText.replace("Bar", "");
			System.out.println(alterText);

			List<WebElement> list = driver.findElements(
					By.xpath(".//div[@id='actionDiv']/div[@id='Actions_dynamicmenu" + alterText + "Menu']/div"));

			fc.utobj().printTestStep("Verify Modify Visit");
			boolean isTextPresent = false;

			for (int x = 0; x < list.size(); x++) {
				String optionName = list.get(x).getText().trim();

				System.out.println(optionName);
				if (optionName.equalsIgnoreCase("Restore")) {
					isTextPresent = true;
				}
			}

			if (isTextPresent == false) {
				fc.utobj().throwsException("Visit not Modify And Not Cancelled");
			}

			fc.utobj().printTestStep("Restore Visit");
			defaultViewByFranchiseId(driver, franchiseId);
			fc.utobj().actionImgOption(driver, franchiseId, "Restore");
			fc.utobj().acceptAlertBox(driver);

			fc.utobj().printTestStep("Verify Restore Visit");
			defaultViewByFranchiseId(driver, franchiseId);

			boolean isStatusVisitPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () , '" + franchiseId + "')]/ancestor::tr/td[contains(text () , 'Scheduled')]");

			if (isStatusVisitPresent == false) {
				fc.utobj().throwsException("was not able to verify Restored Visit");
			}

			fc.utobj().printTestStep("Delete Visit");
			actionImgOption(driver, franchiseId, "Delete");

			fc.utobj().acceptAlertBox(driver);

			defaultViewByFranchiseId(driver, franchiseId);

			fc.utobj().printTestStep("Verify Delete Visit");
			String textPresent = fc.utobj()
					.getElementByXpath(driver, ".//*[@id='ajaxDiv']/table/tbody/tr/td/table/tbody/tr[2]/td").getText()
					.trim();

			if (textPresent.equalsIgnoreCase("No records found.")) {
				/* System.out.println("Pass"); */
			} else {
				fc.utobj().throwsException("Was not able to delete Visit");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "fieldops")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify task if visit status is non compliance", testCaseId = "TC_81_Visit_Status_Non_Compliance")
	private void visitSatatusNonCompliance() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());
		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("fieldops", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			FieldOpsVisitsPage pobj = new FieldOpsVisitsPage(driver);

			fc.utobj().printTestStep("Navigate To Admin > Hidden Links > Configure FieldOps Settings Page");
			fc.utobj().printTestStep("Configure Field Ops Settings Log Private Comment Yes");
			AdminHiddenLinksConfigureFieldOpsSettingsPageTest hiddenLinks = new AdminHiddenLinksConfigureFieldOpsSettingsPageTest();
			hiddenLinks.configureFieldOpsSettingsLogPrivateCommentYes(driver, config);

			fc.utobj().printTestStep("Navigate To Admin > Fieldops > Manage Visit Form");
			fc.utobj().printTestStep("Add Private Visit Form");
			AdminFieldOpsManageVisitFormPageTest visitFormPage = new AdminFieldOpsManageVisitFormPageTest();
			String visitFormName = visitFormPage.addVisitFormPrivate(driver, dataSet);

			fc.utobj().printTestStep("Add Question In Visit Form");
			fc.utobj().clickElement(driver, pobj.addQuestion);

			AdminFieldOpsManageVisitFormCustomizeVisitFormPageTest customizeVisit = new AdminFieldOpsManageVisitFormCustomizeVisitFormPageTest();
			customizeVisit.addQuestion(driver, dataSet);

			fc.utobj().clickElement(driver, pobj.finishBtn);

			fc.utobj().printTestStep("Navigate To Admin > Franchise Location > Add Franchise Location");
			fc.utobj().printTestStep("Add Franchise Location");
			String regionName = fc.utobj().generateTestData("Test");
			String storeType = fc.utobj().generateTestData("Test");
			String franchiseId = fc.utobj().generateTestData("Test");
			AdminFranchiseLocationAddFranchiseLocationPageTest franchise = new AdminFranchiseLocationAddFranchiseLocationPageTest();
			franchiseId = franchise.addFranchiseLocation_All(driver, franchiseId, regionName, storeType);

			fc.utobj().printTestStep("Navigate To Admin > Users > Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUser = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			String emailId = "fieldopsautomation@staffex.com";
			CorporateUser corpUser = new CorporateUser();
			corpUser.setConsultant("Y");
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporateUser.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			fc.utobj().printTestStep("Navigate To FieldOps > Visits Page");
			fc.utobj().printTestStep("Create Visit");
			visitFormName = createVisitAtVisitPage(driver, config, dataSet, visitFormName, franchiseId,
					corpUser.getuserFullName());

			String visitId = fc.utobj()
					.getElementByXpath(driver,
							".//tr[2]/td[contains(text () ," + "'" + visitFormName + "'" + ")]/../td[1]/a")
					.getText().trim();

			fc.utobj().printTestStep("Navigate To Admin > FieldOps > Action Library");
			fc.utobj().printTestStep("Add Action Item");
			AdminFieldOpsActionLibraryPageTest actionPage = new AdminFieldOpsActionLibraryPageTest();
			String subject = actionPage.addActionItem(driver, dataSet);

			fc.utobj().printTestStep("Add Task");
			FieldOpsTasksPageTest tasksPage = new FieldOpsTasksPageTest();
			tasksPage.addTaskIntoVisit(driver, config, dataSet, franchiseId, subject, corpUser.getuserFullName(),
					visitId);

			fc.utobj().printTestStep("Start Visit");
			startVisitWithCompleteStatus(driver, config, dataSet, franchiseId);

			fc.utobj().printTestStep("Verify Visit Status");
			defaultViewByFranchiseId(driver, franchiseId);

			boolean isTaskStatusPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//*[.='" + franchiseId + "']/ancestor::tr/td[contains(text () ,'Non-compliance')]");
			if (isTaskStatusPresent == false) {
				fc.utobj().throwsException("was not able to verify Non Compliance Task");
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups ={"fieldops" ,"fieldops0808"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify Practice visit will be display only to franchise user And Verify the Follow Up Status Of Visit", testCaseId = "TC_88_Verify_Practice_Visit_01")
	private void verifyPracticeVisitVisibleFranchiseUser() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("fieldops", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			FieldOpsVisitsPage pobj = new FieldOpsVisitsPage(driver);

			fc.utobj().printTestStep("Navigate To Admin > Hidden Links > Configure FieldOps Settings Page");
			fc.utobj().printTestStep("Configure Field Ops Settings Log Private Comment Yes");
			AdminHiddenLinksConfigureFieldOpsSettingsPageTest hiddenLinks = new AdminHiddenLinksConfigureFieldOpsSettingsPageTest();
			hiddenLinks.configureFieldOpsSettingsLogPrivateCommentYes(driver, config);

			fc.utobj().printTestStep("Configure FieldOps Settings Franchise Self Audit Yes Owner Scoring Yes");
			hiddenLinks.configureFieldOpsSettingsFranchiseSelfAuditYesOwnerScoringYes(driver, config);
			String password = "t0n1ght123";

			fc.utobj().printTestStep("Navigate To Admin > Users > Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUser = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			String emailId = "fieldopsautomation@staffex.com";
			CorporateUser corpUser = new CorporateUser();
			corpUser.setConsultant("Y");
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporateUser.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			fc.utobj().printTestStep("Navigate To Admin > Franchise Location > Add Franchise Location");
			fc.utobj().printTestStep("Add Franchise Location");
			String regionName = fc.utobj().generateTestData("Test");
			String storeType = fc.utobj().generateTestData("Test");
			String franchiseId = fc.utobj().generateTestData("Test");
			AdminFranchiseLocationAddFranchiseLocationPageTest franchise = new AdminFranchiseLocationAddFranchiseLocationPageTest();
			franchiseId = franchise.addFranchiseLocation_All(driver, franchiseId, regionName, storeType);

			fc.utobj().printTestStep("Add Franchise User");
			AdminUsersManageManageFranchiseUsersPageTest franUser = new AdminUsersManageManageFranchiseUsersPageTest();
			String FranUserName = fc.utobj().generateTestData("TestF");
			String roleName = "Default Franchise Role";
			String franUserName = franUser.addFranchiseUser(driver, FranUserName, password, franchiseId, roleName,
					emailId);

			fc.utobj().printTestStep("Navigate To Admin > FieldOps > Manage Visit Form");
			fc.utobj().printTestStep("Add Non-Private Visit Form With Tab View");
			AdminFieldOpsManageVisitFormPageTest visitFormPage = new AdminFieldOpsManageVisitFormPageTest();
			String visitFormName = visitFormPage.addVisitFormTabViewPrivateNo(driver, config, dataSet);

			fc.utobj().printTestStep("Add Tab In Visit Form");
			AdminFieldOpsManageVisitFormCustomizeVisitFormPageTest customizeVisit = new AdminFieldOpsManageVisitFormCustomizeVisitFormPageTest();
			customizeVisit.addTab(driver, config, dataSet);

			fc.utobj().printTestStep("Add Question In Tab");
			fc.utobj().clickElement(driver, pobj.addQuestion);
			customizeVisit.addQuestion(driver, dataSet);
			fc.utobj().clickElement(driver, pobj.finishBtn);

			fc.home_page().logout(driver);
			fc.utobj().printTestStep("Login With Franchise User");
			fc.loginpage().loginWithParameter(driver, franUserName, password);

			fc.utobj().printTestStep("Navigate To Fieldops > Visits Page");
			fc.fieldops().fieldops_common().fieldopsVisitCreateVisitPage(driver);
			fc.utobj().printTestStep("Create Visit");
			createVisitAtFranchiseVisitPage(driver, config, dataSet, visitFormName, franchiseId,
					corpUser.getuserFullName());

			String visitID = fc.utobj().getText(driver, fc.utobj().getElementByXpath(driver,
					".//tr/td[contains(text () ," + "'" + visitFormName + "'" + ")]/preceding-sibling::td/a"));

			fc.utobj().printTestStep("Start Visit And Save As Draft Visit");
			fc.utobj().actionImgOption(driver, visitFormName, "Start Visit");
			fc.utobj().sendKeys(driver, pobj.ownerResponse, fc.utobj().generateTestData("Oresponse"));
			fc.utobj().sendKeys(driver, pobj.ownerComment, fc.utobj().generateTestData("Ocomment"));
			fc.utobj().clickElement(driver, pobj.nextBtnSubmit);
			fc.utobj().clickElement(driver, pobj.exitVisit);

			fc.utobj().printTestStep("Verify The Status of Visit");
			boolean isStatusPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//td[contains(text () , '" + visitFormName
					+ "')]/ancestor::tr/td[contains(text () , 'Follow Up')]");
			if (isStatusPresent == false) {
				fc.utobj().throwsException("was not able to verify the Follow Up status of visit");
			}

			fc.home_page().logout(driver);

			fc.utobj().printTestStep("Login With Corporate User");
			fc.loginpage().loginWithParameter(driver, corpUser.getUserName(), corpUser.getPassword());

			fc.utobj().printTestStep("Navigate To FieldOps > Visits Page");
			fc.fieldops().fieldops_common().fieldopsVisit(driver);

			defaultViewByVisitIdCorporate(driver, visitID, config);

			fc.utobj().printTestStep("Verify Practice Visit At Consultant");
			fc.utobj().getElementByXpath(driver, ".//*[@id='ajaxDiv']//tr/td[contains(text () , 'No records found.')]")
					.isEnabled();

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}

	}

	@Test(groups = {"fieldops" , "fieldops0808"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify practice visit in qa history tab in Info Manager", testCaseId = "TC_89_Verify_Practice_Visit_Info_Mgr_QA_History")
	private void verifyPracticeVisitInfoMgrQAHistory() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("fieldops", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			FieldOpsVisitsPage pobj = new FieldOpsVisitsPage(driver);

			fc.utobj().printTestStep("Navigate To Admin > Hidden Links > Configure FieldOps Settings Page");
			fc.utobj().printTestStep("Configure Field Ops Settings Log Private Comment Yes");
			AdminHiddenLinksConfigureFieldOpsSettingsPageTest hiddenLinks = new AdminHiddenLinksConfigureFieldOpsSettingsPageTest();
			hiddenLinks.configureFieldOpsSettingsLogPrivateCommentYes(driver, config);

			fc.utobj().printTestStep("Configure FieldOps Settings Franchise SelfAudit Yes Owner Scoring Yes");
			hiddenLinks.configureFieldOpsSettingsFranchiseSelfAuditYesOwnerScoringYes(driver, config);

			fc.utobj().printTestStep("Navigate To Admin > FieldOps > QA Tab Integration Page");
			fc.utobj().printTestStep("Enable QA Tab Integration Yes");
			AdminFieldOpsQATabIntegrationPageTest QATabPage = new AdminFieldOpsQATabIntegrationPageTest();
			QATabPage.qaTabIntegrationYes(driver, config);
			String password = "t0n1ght123";

			fc.utobj().printTestStep("Navigate To Admin > Users > Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUser = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			String emailId = "fieldopsautomation@staffex.com";
			CorporateUser corpUser = new CorporateUser();
			corpUser.setConsultant("Y");
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporateUser.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			fc.utobj().printTestStep("Navigate To Admin > Franchise Location > Add Franchise Location");
			fc.utobj().printTestStep("Add Franchise Location");
			String regionName = fc.utobj().generateTestData("TestR");
			String storeType = fc.utobj().generateTestData("TestS");
			String franchiseId = fc.utobj().generateTestData("TestFID");
			AdminFranchiseLocationAddFranchiseLocationPageTest franchise = new AdminFranchiseLocationAddFranchiseLocationPageTest();
			franchiseId = franchise.addFranchiseLocation_All(driver, franchiseId, regionName, storeType);

			fc.utobj().printTestStep("Add Franchise User");
			AdminUsersManageManageFranchiseUsersPageTest franUser = new AdminUsersManageManageFranchiseUsersPageTest();
			String FranUserName = fc.utobj().generateTestData("TestF");
			String roleName = "Default Franchise Role";
			String franUserName = franUser.addFranchiseUser(driver, FranUserName, password, franchiseId, roleName,
					emailId);

			fc.utobj().printTestStep("Navigate To Admin > FieldOps > Manage Visit Form");
			fc.utobj().printTestStep("Add Non-Private Visit Form With Tab View");
			AdminFieldOpsManageVisitFormPageTest visitFormPage = new AdminFieldOpsManageVisitFormPageTest();
			String visitFormName = visitFormPage.addVisitFormTabViewPrivateNo(driver, config, dataSet);

			fc.utobj().printTestStep("Add Tab In Visit Form");
			AdminFieldOpsManageVisitFormCustomizeVisitFormPageTest customizeVisit = new AdminFieldOpsManageVisitFormCustomizeVisitFormPageTest();
			customizeVisit.addTab(driver, config, dataSet);

			fc.utobj().printTestStep("Add Question In Tab");
			fc.utobj().clickElement(driver, pobj.addQuestion);
			customizeVisit.addQuestion(driver, dataSet);
			fc.utobj().clickElement(driver, pobj.finishBtn);

			fc.home_page().logout(driver);

			fc.utobj().printTestStep("Login With Franchise User");
			fc.loginpage().loginWithParameter(driver, franUserName, password);

			fc.utobj().printTestStep("Navigate To Fieldops > Visits Page");
			fc.fieldops().fieldops_common().fieldopsVisitCreateVisitPage(driver);
			fc.utobj().printTestStep("Create Practice Visit");
			createVisitAtFranchiseVisitPage(driver, config, dataSet, visitFormName, franchiseId,
					corpUser.getuserFullName());

			String visitID = fc.utobj().getText(driver, fc.utobj().getElementByXpath(driver,
					".//tr/td[contains(text () ," + "'" + visitFormName + "'" + ")]/preceding-sibling::td/a"));

			fc.home_page().logout(driver);

			fc.utobj().printTestStep("Login With Corporate User");
			fc.loginpage().loginWithParameter(driver, corpUser.getUserName(), corpUser.getPassword());

			fc.utobj().printTestStep("Navigate To Info Mgr > Franchisee > QA History Tab");
			fc.utobj().clickElement(driver, pobj.infoMgr);
			fc.utobj().clickElement(driver, pobj.franchisees);
			fc.utobj().sendKeys(driver, pobj.searchFranchisee, franchiseId);
			fc.utobj().clickElement(driver, pobj.searchBoxBtn);
			fc.utobj().clickLink(driver, franchiseId);
			fc.utobj().clickElement(driver, pobj.qaHistory);

			
			fc.utobj().sendKeys(driver, pobj.visitNumber, visitID);
			// changed By Inzamam 09/22/2016
			String text = fc.utobj().getText(driver,
					fc.utobj().getElementByXpath(driver, ".//*[@id='ms-parentmasterVisitId']/button/span"));

			if ((text.equalsIgnoreCase("All Selected"))) {
				fc.utobj().clickElement(driver, pobj.visitFormSelectBtn);
				fc.utobj().clickElement(driver, pobj.visitFormSelectAll);
				fc.utobj().clickElement(driver, pobj.visitFormSelectBtn);
			} else {
				fc.utobj().clickElement(driver, pobj.visitFormSelectBtn);
				fc.utobj().clickElement(driver, pobj.visitFormSelectAll);
				fc.utobj().clickElement(driver, pobj.visitFormSelectAll);
				fc.utobj().clickElement(driver, pobj.visitFormSelectBtn);

			}

			fc.utobj().printTestStep("Verify Practice Visit");
			fc.utobj().getElementByXpath(driver, ".//*[@id='ajaxDiv']//tr/td[contains(text () , 'No records found.')]")
					.isEnabled();

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "fieldops")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify Practice visit should not display in smart connect", testCaseId = "TC_90_Verify_Practice_Visit_SmartConnect")
	private void verifyPracticeVisitSmartConnect() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("fieldops", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			FieldOpsVisitsPage pobj = new FieldOpsVisitsPage(driver);

			fc.utobj().printTestStep("Navigate To Admin > Hidden Links > Configure FieldOps Settings Page");
			fc.utobj().printTestStep("Configure Field Ops Settings Log Private Comment Yes");
			AdminHiddenLinksConfigureFieldOpsSettingsPageTest hiddenLinks = new AdminHiddenLinksConfigureFieldOpsSettingsPageTest();
			hiddenLinks.configureFieldOpsSettingsLogPrivateCommentYes(driver, config);

			fc.utobj().printTestStep("Configure FieldOps Settings Franchise Self Audit Yes Owner Scoring Yes");
			hiddenLinks.configureFieldOpsSettingsFranchiseSelfAuditYesOwnerScoringYes(driver, config);
			String password = "t0n1ght123";

			fc.utobj().printTestStep("Navigate To Admin > Users > Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUser = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			String emailId = "fieldopsautomation@staffex.com";
			CorporateUser corpUser = new CorporateUser();
			corpUser.setConsultant("Y");
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporateUser.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			fc.utobj().printTestStep("Navigate To Admin > Franchise Location > Add Franchise Location");
			fc.utobj().printTestStep("Add Franchise Location");
			String regionName = fc.utobj().generateTestData("Test");
			String storeType = fc.utobj().generateTestData("Test");
			String franchiseId = fc.utobj().generateTestData("Test");
			AdminFranchiseLocationAddFranchiseLocationPageTest franchise = new AdminFranchiseLocationAddFranchiseLocationPageTest();
			franchiseId = franchise.addFranchiseLocation_All(driver, franchiseId, regionName, storeType);

			fc.utobj().printTestStep("Add Franchise User");
			AdminUsersManageManageFranchiseUsersPageTest franUser = new AdminUsersManageManageFranchiseUsersPageTest();
			String FranUserName = fc.utobj().generateTestData("TestF");
			String roleName = "Default Franchise Role";
			String franUserName = franUser.addFranchiseUser(driver, FranUserName, password, franchiseId, roleName,
					emailId);

			fc.utobj().printTestStep("Navigate To Admin > FieldOps > Manage Visit Form");
			fc.utobj().printTestStep("Add Non-Private Visit Form With Tab View");
			AdminFieldOpsManageVisitFormPageTest visitFormPage = new AdminFieldOpsManageVisitFormPageTest();
			String visitFormName = visitFormPage.addVisitFormTabViewPrivateNo(driver, config, dataSet);

			fc.utobj().printTestStep("Add Tab In Visit Form");
			AdminFieldOpsManageVisitFormCustomizeVisitFormPageTest customizeVisit = new AdminFieldOpsManageVisitFormCustomizeVisitFormPageTest();
			customizeVisit.addTab(driver, config, dataSet);

			fc.utobj().printTestStep("Add Question In Tab");
			fc.utobj().clickElement(driver, pobj.addQuestion);
			customizeVisit.addQuestion(driver, dataSet);
			fc.utobj().clickElement(driver, pobj.finishBtn);

			fc.home_page().logout(driver);

			fc.utobj().printTestStep("Login With Franchise User");
			fc.loginpage().loginWithParameter(driver, franUserName, password);

			fc.utobj().printTestStep("Navigate To Fieldops > Visits Page");
			fc.fieldops().fieldops_common().fieldopsVisitCreateVisitPage(driver);

			fc.utobj().printTestStep("Create Practice Visit");
			createVisitAtFranchiseVisitPage(driver, config, dataSet, visitFormName, franchiseId,
					corpUser.getuserFullName());

			String visitID = fc.utobj().getText(driver, fc.utobj().getElementByXpath(driver,
					".//tr/td[contains(text () ," + "'" + visitFormName + "'" + ")]/preceding-sibling::td/a"));

			fc.home_page().logout(driver);

			fc.utobj().printTestStep("Login With Corporate User");
			fc.loginpage().loginWithParameter(driver, corpUser.getUserName(), corpUser.getPassword());

			fc.utobj().printTestStep("Navigate To SmartConnect Module");
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='test1']/span/a/span/span"));
			fc.utobj().clickElement(driver, fc.utobj().getElementByLinkText(driver, "SmartConnect"));

			fc.utobj().printTestStep("Verify Practice Visit At SmartConnect Module");

			List<WebElement> list = driver.findElements(
					By.xpath(".//tr/td/span[.='Scheduled/Pending Visits']/../../following-sibling::tr/td/ul/li/a"));
			String[] arrayList = new String[list.size()];
			for (int i = 0; i < list.size(); i++) {
				arrayList[i] = list.get(i).getText().trim();
			}

			for (int k = 0; k < arrayList.length; k++) {

				if (arrayList[k].equalsIgnoreCase(visitID)) {
					fc.utobj().throwsException("Was not able to verify Visit At SmartConnect Page");
				} else {
					System.out.println("Pass" + visitID);
				}
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "fieldops")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify that only one visit should be allowed on any particular date if visit frequency is not applicable", testCaseId = "TC_98_Verify_Visit_Frequency_Not_Applicable")
	private void verifyVisitFrequencyNotApplicable() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("fieldops", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			FieldOpsVisitsPage pobj = new FieldOpsVisitsPage(driver);
			String password = "t0n1ght123";

			fc.utobj().printTestStep("Navigate To Admin > Users > Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");

			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUser = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			String emailId = "fieldopsautomation@staffex.com";
			CorporateUser corpUser = new CorporateUser();
			corpUser.setConsultant("Y");
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporateUser.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			fc.utobj().printTestStep("Navigate To Admin > Franchise Location > Add Franchise Location");
			fc.utobj().printTestStep("Add Franchise Location");
			String regionName = fc.utobj().generateTestData("Test");
			String storeType = fc.utobj().generateTestData("Test");
			String franchiseId = fc.utobj().generateTestData("Test");
			AdminFranchiseLocationAddFranchiseLocationPageTest franchise = new AdminFranchiseLocationAddFranchiseLocationPageTest();
			franchiseId = franchise.addFranchiseLocation_All(driver, franchiseId, regionName, storeType);

			fc.utobj().printTestStep("Add Franchise User");
			AdminUsersManageManageFranchiseUsersPageTest franUser = new AdminUsersManageManageFranchiseUsersPageTest();
			String FranUserName = fc.utobj().generateTestData("TestF");
			String roleName = "Default Franchise Role";
			franUser.addFranchiseUser(driver, FranUserName, password, franchiseId, roleName, emailId);

			fc.utobj().printTestStep("Navigate To Admin > Fieldops > Manage Visit Form");
			fc.utobj().printTestStep("Add Private Visit Form");
			AdminFieldOpsManageVisitFormPageTest manageVistPage = new AdminFieldOpsManageVisitFormPageTest();
			String visitFormName = manageVistPage.addVisitFormPrivate(driver, dataSet);
			fc.utobj().clickElement(driver, pobj.finishBtn);

			fc.utobj().printTestStep("Navigate To FieldOps > Visits Page");
			fc.utobj().printTestStep("Create Visit");
			createVisitAtVisitPage(driver, config, dataSet, visitFormName, franchiseId, corpUser.getuserFullName());

			fc.utobj().printTestStep("Create Another Visit");

			// Click createVisitForm
			fc.utobj().clickElement(driver, pobj.createVisit);

			// select visit Form
			fc.utobj().clickElement(driver, pobj.selectClick);
			fc.utobj().sendKeys(driver, pobj.searchBox, visitFormName);
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver,
							".//*[@id='ms-parentmasterVisitId']/div/ul/li/label[contains(text () ," + "'"
									+ visitFormName + "'" + ")]/input"));

			// select FranchiseID
			fc.utobj().clickElement(driver, pobj.franchiseeSelect);
			fc.utobj().sendKeys(driver, pobj.searchBoxFranchisee, franchiseId);
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver,
							".//*[@id='ms-parentfranchiseeNo']/div/ul/li/label[contains(text () , " + "'" + franchiseId
									+ "'" + ")]/input"));
			fc.utobj().clickElement(driver, pobj.franchiseeSelect);

			// Select Consultant
			fc.utobj().clickElement(driver, pobj.selectConsultant);
			fc.utobj().sendKeys(driver, pobj.consultantSearchBox, corpUser.getuserFullName());
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver,
							".//*[@id='ms-parentassignTo']/div/ul/li/label[contains(text () , " + "'"
									+ corpUser.getuserFullName() + "'" + ")]/input"));

			// schedule Date
			String curDate = fc.utobj().getCurrentDateUSFormat();
			fc.utobj().sendKeys(driver, pobj.scheduleDateTime, curDate);

			try {
				if (dataSet.get("franchiseLogin").equalsIgnoreCase("Yes")) {
					fc.utobj().clickElement(driver, pobj.sendMailCheckNotiFranchiseeOwner);
				}
			} catch (Exception e) {
			}

			fc.utobj().clickElement(driver, pobj.schedule);

			String alertText = driver.switchTo().alert().getText().trim();

			fc.utobj().acceptAlertBox(driver);

			if (alertText.contains(franchiseId) && alertText.contains(curDate)) {
				// do nothing
			} else {
				fc.utobj().throwsException("was  not able to verify No Applicable Case");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = {"fieldops", "foFailedTC"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verfiy Visits Respective To MUID", testCaseId = "TC_103_All_Visit_VIsible_All_Muid")
	private void allVisitVisibleForAllMuid() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("fieldops", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			FieldOpsVisitsPage pobj = new FieldOpsVisitsPage(driver);
			String password = "t0n1ght123";

			fc.utobj().printTestStep("Configure FieldOps Settings Franchise Self Audit Yes Owner Scoring Yes");
			new AdminHiddenLinksConfigureFieldOpsSettingsPageTest()
					.configureFieldOpsSettingsFranchiseSelfAuditYesOwnerScoringYes(driver, config);

			fc.utobj().printTestStep("Navigate To Admin > FieldOps > Manage Visit Form");
			fc.utobj().printTestStep("Add Non-Private Visit Form With Tab View");
			AdminFieldOpsManageVisitFormPageTest visitFormPage = new AdminFieldOpsManageVisitFormPageTest();
			String visitFormName = visitFormPage.addVisitFormTabViewPrivateNo(driver, config, dataSet);

			fc.utobj().printTestStep("Add Tab In Visit Form");
			AdminFieldOpsManageVisitFormCustomizeVisitFormPageTest customizeVisit = new AdminFieldOpsManageVisitFormCustomizeVisitFormPageTest();
			customizeVisit.addTab(driver, config, dataSet);

			fc.utobj().printTestStep("Add Question In Tab");
			fc.utobj().clickElement(driver, pobj.addQuestion);
			customizeVisit.addQuestion(driver, dataSet);
			fc.utobj().clickElement(driver, pobj.finishBtn);

			fc.utobj().printTestStep("Navigate To Admin > Franchise Location > Add Franchise Location");
			fc.utobj().printTestStep("Add Franchise Location With Muid");
			AdminFranchiseLocationAddFranchiseLocationPageTest addLoc = new AdminFranchiseLocationAddFranchiseLocationPageTest();
			String franchiseId = fc.utobj().generateTestData("TestFID");
			String regionName1 = fc.utobj().generateTestData("TestR");
			String storeType1 = fc.utobj().generateTestData("TestS");
			String firstName = fc.utobj().generateTestData("firstName");
			String lastName = "lastName";
			franchiseId = addLoc.addFranchiseLocation_AllMuid(driver, franchiseId, regionName1, storeType1, firstName,
					lastName, config);

			fc.utobj().printTestStep("Add Franchise User");
			AdminUsersManageManageFranchiseUsersPageTest addFranUser = new AdminUsersManageManageFranchiseUsersPageTest();
			String userName = fc.utobj().generateTestData("Test");
			String roleName = "Default Franchise Role";
			String emailId = "fieldopsautomation@staffex.com";
			addFranUser.addFranchiseUser(driver, userName, password, franchiseId, roleName, emailId);

			fc.utobj().printTestStep("Add Franchise Location With MUID");
			AdminFranchiseLocationAddFranchiseLocationPageTest franchiseLoc = new AdminFranchiseLocationAddFranchiseLocationPageTest();
			String franchiseeID = fc.utobj().generateTestData("TestFID");
			String centerName = fc.utobj().generateTestData("TestC");

			String curDate = fc.utobj().getCurrentDateUSFormat();
			String openingDate = curDate;
			String muId = fc.utobj().generateTestData("TestMuid");
			String regionName = fc.utobj().generateTestData("TestR");
			String storeType = fc.utobj().generateTestData("TestS");

			fc.utobj().printTestStep("Navigate To Admin > Add Area/Region");
			fc.utobj().printTestStep("Add Region");

			AdminAreaRegionAddAreaRegionPageTest addregion = new AdminAreaRegionAddAreaRegionPageTest();
			addregion.addAreaRegion(driver, regionName);

			fc.utobj().printTestStep("Add Store Type");
			AdminConfigurationConfigureStoreTypePageTest addStore = new AdminConfigurationConfigureStoreTypePageTest();
			addStore.addStoreType(driver, storeType);
			String franchiseUserName = firstName + " " + lastName;

			fc.utobj().printTestStep("Add Franchise Location With MUID");
			muId = franchiseLoc.franchiseLocationWithMUID(driver, franchiseeID, regionName, centerName, openingDate,
					storeType, franchiseUserName, muId, config);

			fc.utobj().printTestStep("Add MUID User");
			String muIdUserName = fc.utobj().generateTestData("TestM");
			String firstMuidName = fc.utobj().generateTestData("firstMuidName");
			muIdUserName = addFranUser.addMUIDUser(driver, muIdUserName, password, muId, firstMuidName, config,
					emailId);

			fc.home_page().logout(driver);

			fc.utobj().printTestStep("Login With MU Id");
			fc.loginpage().loginWithParameter(driver, muIdUserName, password);

			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='franchisee_no']/span/a"));
			fc.utobj().clickElement(driver,	fc.utobj().getElementByXpath(driver, ".//*[@id='franchisee_no']/div/div/span[.='All ']"));

			fc.utobj().printTestStep("Navigate To Fieldops > Visits Page");
			fc.fieldops().fieldops_common().fieldopsVisitCreateVisitPage(driver);

			fc.utobj().printTestStep("Create Visit By First Location");
			createVisitAtFranchiseVisitPage(driver, config, dataSet, visitFormName, franchiseId, muIdUserName);

			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='franchisee_no']/span/a"));
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//*[@id='franchisee_no']/div/div/span[.='All ']"));

			fc.utobj().printTestStep("Navigate To Fieldops > Visits Page");
			fc.fieldops().fieldops_common().fieldopsVisitCreateVisitPage(driver);

			fc.utobj().printTestStep("Create Visit By Second Location");
			createVisitAtFranchiseVisitPage(driver, config, dataSet, visitFormName, franchiseeID, muIdUserName);

			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='franchisee_no']/span/a"));
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//*[@id='franchisee_no']/div/div/span[.='All ']"));

			fc.utobj().printTestStep("Navigate To Fieldops > Visits Page");
			fc.fieldops().fieldops_common().fieldopsVisitCreateVisitPage(driver);

			fc.utobj().printTestStep("Verify All Visit");

			boolean isFirstIdVisitPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//div[@id='ajaxDiv']//tr/td[contains(text () , '" + franchiseId
							+ "')]/ancestor::tr/td[contains(text () ,'" + visitFormName + "')]");

			if (isFirstIdVisitPresent == false) {
				fc.utobj().throwsException("was not able to verify Visit With First Franchise Id");
			}

			boolean isSecondIdVisitPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//div[@id='ajaxDiv']//tr/td[contains(text () , '" + franchiseeID
							+ "')]/ancestor::tr/td[contains(text () ,'" + visitFormName + "')]");

			if (isSecondIdVisitPresent == false) {
				fc.utobj().throwsException("was not able to verify Visit With Second Franchise Id");
			}

			fc.utobj().printTestStep("Verify First Visit Selecting First Location");
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='franchisee_no']/span/a"));
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='franchisee_no']/div/div/span[contains(text () ," + "'" + franchiseId + "'" + ")]"));

			boolean isFSelectVisitPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//div[@id='ajaxDiv']//tr/td[contains(text () , '" + visitFormName + "')]");

			if (isFSelectVisitPresent == false) {
				fc.utobj().throwsException("was not able to verify Visit With First Franchise Id");
			}

			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='franchisee_no']/span/a"));
			
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='franchisee_no']/div/div/span[contains(text () ," + "'"+franchiseeID+"'" + ")]"));
			
			fc.utobj().printTestStep("Verify Second Visit Selecting Second Location");
			
			boolean isSSelectVisitPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//div[@id='ajaxDiv']//tr/td[contains(text () , '"+visitFormName+"')]");
			
			if (isSSelectVisitPresent == false) {
				fc.utobj().throwsException(
						"was not able to verify Visit With Second Franchise Id on selecting franchise id from drop down");
			}
			
			fc.utobj().printTestStep("Verify Visits In Smartconnect After Selecting All location from drop down");
			
			fc.home_page().openSmartConnectModule(driver);
			
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='franchisee_no']/span/a"));
			
			fc.utobj().clickElement(driver,	fc.utobj().getElementByXpath(driver, ".//*[@id='franchisee_no']/div/div/span[.='All ']"));
			
			boolean isFFranchise = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//*[contains(text () , 'Scheduled/Pending Visits')]/ancestor::tr//ul/li/*[contains(text () , '"+franchiseId+"')]");
			if (isFFranchise == false) {
				fc.utobj().throwsException("was not able to verify Franchise Id At SmartConnect Module");
			}

			boolean isSFranchise = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//*[contains(text () , 'Scheduled/Pending Visits')]/ancestor::tr//ul/li/*[contains(text () , '"+franchiseeID+"')]");
			if (isSFranchise == false) {
				fc.utobj().throwsException("was not able to verify Second Franchise Id At SmartConnect Module");
			}

			fc.utobj().printTestStep("Verify Visit After Selecting First Location At SmartConnect Module");

			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='franchisee_no']/span/a"));
			
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='franchisee_no']/div/div/span[contains(text () ," + "'" + franchiseId + "'" + ")]"));

			WebElement element = fc.utobj().getElementByXpath(driver, ".//tr/td/span[.='Scheduled/Pending Visits']/../../following-sibling::tr//ul/li/a/span");
			
			Actions ac = new Actions(driver);
			ac.moveToElement(element).build().perform();
			//String visitFormNameForFranchiseId = fc.utobj().getElementByXpath(driver, ".//*[@id='siteMainBody']/div[17]/div/table/tbody/tr[1]/td[3]/b").getText().trim();
			//String visitFormNameForFranchiseId = fc.utobj().getElementByXpath(driver, ".//li[@class='taskToDoL hoverRecentDocs']//b[1]").getText().trim();

			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='franchisee_no']/span/a"));
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,".//*[@id='franchisee_no']/div/div/span[contains(text () ," + "'" + franchiseeID + "'" + ")]"));
			/*
			WebElement element1 = fc.utobj().getElementByXpath(driver, ".//tr/td/span[.='Scheduled/Pending Visits']/../../following-sibling::tr//ul/li/a/span");
			Actions ac1 = new Actions(driver);
			ac1.moveToElement(element1).build().perform();

			//String visitFormNameForFranchiseSecond = fc.utobj().getElementByXpath(driver, ".//li[@class='taskToDoL hoverRecentDocs']//b[1]").getText().trim();

			if (!visitFormNameForFranchiseId.equalsIgnoreCase(visitFormName)) {
				fc.utobj().throwsException(
						"Was not able to verify visit at smartConnect on selecting first Franchise Id");
			}

			if (!visitFormNameForFranchiseSecond.equalsIgnoreCase(visitFormName)) {
				fc.utobj().throwsException(
						"Was not able to verify visit at smartConnect on selecting second franchise Id");
			}
*/
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "fieldops" ,"fieldops0808"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify Draft status of visit and modify draft visit", testCaseId = "TC_113_Verify_Visit_Status_Draft")
	private void verifyVisitStatusDraft() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("fieldops", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			FieldOpsVisitsPage pobj = new FieldOpsVisitsPage(driver);

			fc.utobj().printTestStep("Navigate To Admin > Hidden Links > Configure FieldOps Settings Page");
			fc.utobj().printTestStep("Configure Field Ops Settings Log Private Comment Yes");
			AdminHiddenLinksConfigureFieldOpsSettingsPageTest hiddenLinks = new AdminHiddenLinksConfigureFieldOpsSettingsPageTest();
			hiddenLinks.configureFieldOpsSettingsLogPrivateCommentYes(driver, config);

			fc.utobj().printTestStep("Navigate To Admin > Fieldops > Manage Visit Form");
			fc.utobj().printTestStep("Add Private Visit Form");
			AdminFieldOpsManageVisitFormPageTest visitFormPage = new AdminFieldOpsManageVisitFormPageTest();
			String visitFormName = visitFormPage.addVisitFormPrivate(driver, dataSet);

			fc.utobj().printTestStep("Add Question In Visit Form");
			fc.utobj().clickElement(driver, pobj.addQuestion);
			AdminFieldOpsManageVisitFormCustomizeVisitFormPageTest customizeVisit = new AdminFieldOpsManageVisitFormCustomizeVisitFormPageTest();
			customizeVisit.addQuestion(driver, dataSet);
			fc.utobj().clickElement(driver, pobj.finishBtn);

			fc.utobj().printTestStep("Navigate To Admin > Franchise Location > Add Franchise Location");
			fc.utobj().printTestStep("Add Franchise Location");
			String regionName = fc.utobj().generateTestData("TestR");
			String storeType = fc.utobj().generateTestData("TestS");
			String franchiseId = fc.utobj().generateTestData("TestID");
			AdminFranchiseLocationAddFranchiseLocationPageTest franchise = new AdminFranchiseLocationAddFranchiseLocationPageTest();
			franchiseId = franchise.addFranchiseLocation_All(driver, franchiseId, regionName, storeType);

			fc.utobj().printTestStep("Navigate To Admin > Users > Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUser = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			String emailId = "fieldopsautomation@staffex.com";
			CorporateUser corpUser = new CorporateUser();
			corpUser.setConsultant("Y");
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporateUser.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			fc.utobj().printTestStep("Navigate To FieldOps > Visits Page");
			fc.utobj().printTestStep("Create Visit");
			createVisitAtVisitPage(driver, config, dataSet, visitFormName, franchiseId, corpUser.getuserFullName());

			// start Visit
			fc.utobj().printTestStep("Start Visit");
			actionImgOption(driver, franchiseId, "Start Visit");
			fc.utobj().sendKeys(driver, pobj.sampleQuestionAnswer, fc.utobj().generateTestData("TestAnswer"));
			fc.utobj().clickElement(driver, pobj.nextBtnSubmit);
			fc.utobj().clickElement(driver, pobj.exitVisit);

			defaultViewByFranchiseId(driver, franchiseId);
			fc.utobj().printTestStep("Verify Status Of Visit");
			fc.utobj().getElementByXpath(driver, ".//*[@id='ajaxDiv']//tr/td[contains(text () ," + "'" + visitFormName
					+ "'" + ")]/../td[contains(text () , 'Draft')]").isEnabled();

			fc.utobj().printTestStep("Modify Draft Visit");
			actionImgOption(driver, franchiseId, "Modify");
			fc.utobj().getElementByXpath(driver, ".//tr/td[.=" + "'" + visitFormName + "'" + "]").isEnabled();
			fc.utobj().getElementByXpath(driver, ".//tr/td[.=" + "'" + franchiseId + "'" + "]").isEnabled();
			fc.utobj().getElementByXpath(driver,
					".//tr/td[contains(text () , " + "'" + corpUser.getuserFullName() + "'" + ")]").isEnabled();
			fc.utobj().selectDropDown(driver, pobj.selectStatus, "Cancelled");
			fc.utobj().clickElement(driver, pobj.submitBtnM);

			fc.utobj().printTestStep("Verify The Modify Draft Visit");
			defaultViewByFranchiseId(driver, franchiseId);
			fc.utobj().getElementByXpath(driver, ".//tr/td/a[contains(text () , " + "'" + franchiseId + "'"
					+ ")]/../following-sibling::td[contains(text () , 'Cancelled')]").isEnabled();

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = {"fieldops" ,"foFailedTC"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify Follow Up Visit Status", testCaseId = "TC_114_Verify_Visit_Status_Follow_Up")
	private void verifyVisitStatusFollowUp() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("fieldops", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			FieldOpsVisitsPage pobj = new FieldOpsVisitsPage(driver);
			String password = "t0n1ght123";

			fc.utobj().printTestStep("Navigate To Admin > Hidden Links > Configure FieldOps Settings Page");
			fc.utobj().printTestStep("Configure Field Ops Settings Log Private Comment Yes");
			AdminHiddenLinksConfigureFieldOpsSettingsPageTest hiddenLinks = new AdminHiddenLinksConfigureFieldOpsSettingsPageTest();
			hiddenLinks.configureFieldOpsSettingsLogPrivateCommentYes(driver, config);
			hiddenLinks.configureFieldOpsSettingsFranchiseSelfAuditYesOwnerScoringYes(driver, config);
			fc.utobj().printTestStep("Navigate To Admin > Users > Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUser = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			String emailId = "fieldopsautomation@staffex.com";
			CorporateUser corpUser = new CorporateUser();
			corpUser.setConsultant("Y");
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporateUser.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			fc.utobj().printTestStep("Navigate To Admin > Franchise Location > Add Franchise Location");
			fc.utobj().printTestStep("Add Franchise Location");
			String regionName = fc.utobj().generateTestData("Test");
			String storeType = fc.utobj().generateTestData("Test");
			String franchiseId = fc.utobj().generateTestData("Test");
			AdminFranchiseLocationAddFranchiseLocationPageTest franchise = new AdminFranchiseLocationAddFranchiseLocationPageTest();
			String ownerFirstName=fc.utobj().generateTestData("Testfname");
			String ownerLastName=fc.utobj().generateTestData("Testlname");
			franchiseId = franchise.addFranchiseLocationZC(driver, regionName, storeType, 
					franchiseId, "Test Center", fc.utobj().getFutureDateUSFormat(2), ownerFirstName, ownerLastName, config);

			fc.utobj().printTestStep("Add Franchise User");
			AdminUsersManageManageFranchiseUsersPageTest franUser = new AdminUsersManageManageFranchiseUsersPageTest();
			String FranUserName = fc.utobj().generateTestData("TestF");
			String roleName = "Default Franchise Role";
			String franUserName = franUser.addFranchiseUser(driver, FranUserName, password, franchiseId, roleName,
					emailId);

			fc.utobj().printTestStep("Navigate To Admin > FieldOps > Manage Visit Form");
			fc.utobj().printTestStep("Add Non-Private Visit Form With Tab View");
			AdminFieldOpsManageVisitFormPageTest visitFormPage = new AdminFieldOpsManageVisitFormPageTest();
			String visitFormName = visitFormPage.addVisitFormTabViewPrivateNo(driver, config, dataSet);

			fc.utobj().printTestStep("Add Tab In Visit Form");
			AdminFieldOpsManageVisitFormCustomizeVisitFormPageTest customizeVisit = new AdminFieldOpsManageVisitFormCustomizeVisitFormPageTest();
			customizeVisit.addTab(driver, config, dataSet);

			fc.utobj().printTestStep("Add Question In Tab");
			fc.utobj().clickElement(driver, pobj.addQuestion);
			customizeVisit.addQuestion(driver, dataSet);
			fc.utobj().clickElement(driver, pobj.finishBtn);

			fc.utobj().printTestStep("Navigate To FieldOps > Visits Page");
			fc.utobj().printTestStep("Create Visit For Franchise User");
			dataSet.put("fullName", ownerFirstName+" "+ownerLastName);
			
			createVisitAtVisitWithEmailNotification(driver, config, dataSet, visitFormName, franchiseId, corpUser.getuserFullName());

			fc.home_page().logout(driver);

			fc.utobj().printTestStep("Login With Franchise User");
			fc.loginpage().loginWithParameter(driver, franUserName, password);

			fc.utobj().printTestStep("Navigate To FieldOps > Visits Page");
			fc.fieldops().fieldops_common().fieldopsVisit(driver);
			
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//tr/td[contains(text () ," + "'" + visitFormName + "'" + ")]/preceding-sibling::td/a"));
			
			String alterText = fc.utobj().getElementByXpath(driver, ".//div[@id='tabs']//label").getAttribute("id");
			alterText = alterText.replace("_1", "");
			String ownerResponse = "Owner Response123";
			String ownerComments = "Owner Comments123";
			fc.utobj().sendKeys(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='Response2_" + alterText + "']"), ownerResponse);
			fc.utobj().sendKeys(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='Comment2_" + alterText + "']"),	ownerComments);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='nextSubmitAllButton']"));
			fc.utobj().printTestStep("Verify Follow Up Status OF Visit At Franchise Side");

			fc.utobj().getElementByXpath(driver, ".//tr/td[contains(text () , 'Follow Up')]").isEnabled();

			fc.home_page().logout(driver);

			fc.utobj().printTestStep("Login With Corporate User");
			fc.loginpage().loginWithParameter(driver, corpUser.getUserName(), corpUser.getPassword());

			fc.utobj().printTestStep("Navigate To FieldOps > Visits Page");
			fc.fieldops().fieldops_common().fieldopsVisit(driver);
			//defaultViewByFranchiseId(driver, franchiseId);

			fc.utobj().printTestStep("Verify FolloW Up Status of Visit At Corporate User Side");
			fc.utobj().getElementByXpath(driver, ".//tr/td[contains(text () , 'Follow Up')]").isEnabled();
			fc.utobj().printTestStep("Start Visit");
			actionImgOption(driver, franchiseId, "Start Visit");

			fc.utobj().sendKeys(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='Response1_" + alterText + "']"),
					"Consultant Response");
			fc.utobj().sendKeys(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='Comment1_" + alterText + "']"),
					"Consultant Comments");
			fc.utobj().sendKeys(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='private" + alterText + "']"),
					"Private Comments");

			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='nextSubmitAllButton']"));
			fc.utobj().clickElement(driver, pobj.submitAllBtn);

			fc.utobj().printTestStep("Verify Completed Status Of Visit");

			fc.utobj().getElementByXpath(driver,
					".//*[contains(text () , 'Status')]/following-sibling::td[.='Completed']").isEnabled();
			fc.utobj().getElementByXpath(driver, ".//tr/td[contains(text () ," + "'" + ownerResponse + "'" + ")]")
					.isEnabled();
			fc.utobj().getElementByXpath(driver, ".//tr/td[contains(text () ," + "'" + ownerComments + "'" + ")]")
					.isEnabled();
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//tr/td/input[@value='OK']"));
			defaultViewByFranchiseId(driver, franchiseId);
			fc.utobj().getElementByXpath(driver, ".//tr/td[contains(text () , 'Completed')]").isEnabled();

			fc.home_page().logout(driver);

			fc.utobj().printTestStep("Login With Franchise User");
			fc.loginpage().loginWithParameter(driver, franUserName, password);
			fc.utobj().printTestStep("Navigate To FieldOps > Visits Page");
			fc.fieldops().fieldops_common().fieldopsVisit(driver);

			fc.utobj().printTestStep("Verify Complete Status of Visit");
			fc.utobj().getElementByXpath(driver, ".//tr/td[contains(text () , 'Completed')]").isEnabled();

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}


	public String createVisitAtVisitWithEmailNotification(WebDriver driver, Map<String, String> config, Map<String, String> dataSet,
			String visitFormName, String franchiseId, String userName) throws Exception {

		String testCaseId = "TC_Create_Visit_At_Visit_Page";
		String curDate = null;
		if (fc.utobj().validate(testCaseId)) {
			try {
				FieldOpsVisitsPage pobj = new FieldOpsVisitsPage(driver);
				fc.fieldops().fieldops_common().fieldopsVisitCreateVisitPage(driver);

				fc.utobj().clickElement(driver, pobj.createVisit);
				fc.utobj().selectValFromMultiSelectRadioBtn(driver, pobj.selectVisitForm, visitFormName);
				fc.utobj().selectValFromMultiSelectWithoutReset(driver, pobj.selectFranchiseId, franchiseId);
				fc.utobj().selectValFromMultiSelectRadioBtn(driver, pobj.selectConsultantMultiSelect, userName);

				curDate = fc.utobj().getCurrentDateUSFormat();
				fc.utobj().sendKeys(driver, pobj.scheduleDateTime, curDate);
				if (dataSet.get("franchiseLogin").equalsIgnoreCase("Yes")) {
					fc.utobj().clickElement(driver, pobj.sendMailCheckNotiFranchiseeOwner);
					fc.utobj().clickElement(driver, pobj.schedule);
					fc.commonMethods().switch_cboxIframe_frameId(driver);
					Thread.sleep(1000);
					fc.utobj().clickElement(driver, pobj.ownerSelect);// selectValFromMultiSelect(driver, pobj.ownerSelect, "Select All");
					fc.utobj().clickElement(driver,	".//div[@class='ms-drop bottom']//input[@name='selectGroupOwnersCombo']");
					fc.utobj().clickElement(driver, pobj.sendBtn);
					fc.utobj().switchFrameToDefault(driver);
				}

				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException("was not able to create Visit At Visit Page :: Field Ops > Visits Page");

		}
		return curDate;
	}
	
	@Test(groups = {"fieldops" ,"foFailedTC"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify N/A option should be clickable while filling response from both franchise user and consultant", testCaseId = "TC_117_Verify_Not_Applicable_Option_01")
	private void verifyNotApplicableOptionFillingResponse() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());
		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("fieldops", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			FieldOpsVisitsPage pobj = new FieldOpsVisitsPage(driver);
			String password = "t0n1ght123";

			fc.utobj().printTestStep("Navigate To Admin > Hidden Links > Configure FieldOps Settings Page");
			fc.utobj().printTestStep("Configure Field Ops Settings Log Private Comment Yes");
			AdminHiddenLinksConfigureFieldOpsSettingsPageTest hiddenLinks = new AdminHiddenLinksConfigureFieldOpsSettingsPageTest();
			hiddenLinks.configureFieldOpsSettingsLogPrivateCommentYes(driver, config);
			hiddenLinks.configureFieldOpsSettingsFranchiseSelfAuditYesOwnerScoringYes(driver, config);

			fc.utobj().printTestStep("Navigate To Admin > Users > Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUser = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			String emailId = "fieldopsautomation@staffex.com";
			CorporateUser corpUser = new CorporateUser();
			corpUser.setConsultant("Y");
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporateUser.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			fc.utobj().printTestStep("Navigate To Admin > Franchise Location > Add Franchise Location");
			fc.utobj().printTestStep("Add Franchise Location");
			String regionName = fc.utobj().generateTestData("Test");
			String storeType = fc.utobj().generateTestData("Test");
			String franchiseId = fc.utobj().generateTestData("Test");
			AdminFranchiseLocationAddFranchiseLocationPageTest franchise = new AdminFranchiseLocationAddFranchiseLocationPageTest();

			String ownerFirstName=fc.utobj().generateTestData("Testfname");
			String ownerLastName=fc.utobj().generateTestData("Testlname");
			franchiseId = franchise.addFranchiseLocation(driver, regionName, storeType, franchiseId, 
					"Test Center", fc.utobj().getFutureDateUSFormat(2), ownerFirstName, ownerLastName, config);
			
			fc.utobj().printTestStep("Add Franchise User");
			AdminUsersManageManageFranchiseUsersPageTest franUser = new AdminUsersManageManageFranchiseUsersPageTest();
			String FranUserName = fc.utobj().generateTestData("TestF");
			String roleName = "Default Franchise Role";
			String franUserName = franUser.addFranchiseUser(driver, FranUserName, password, franchiseId, roleName,
					emailId);

			fc.utobj().printTestStep("Navigate To Admin > FieldOps > Manage Visit Form");
			fc.utobj().printTestStep("Add Non-Private Visit Form With Tab View");
			AdminFieldOpsManageVisitFormPageTest visitFormPage = new AdminFieldOpsManageVisitFormPageTest();
			String visitFormName = visitFormPage.addVisitFormTabViewPrivateNo(driver, config, dataSet);

			fc.utobj().printTestStep("Add Tab In Visit Form");
			AdminFieldOpsManageVisitFormCustomizeVisitFormPageTest customizeVisit = new AdminFieldOpsManageVisitFormCustomizeVisitFormPageTest();
			customizeVisit.addTab(driver, config, dataSet);

			fc.utobj().clickElement(driver, pobj.addQuestion);
			AdminFieldOpsQuestionLibraryPageTest addQuestionPage = new AdminFieldOpsQuestionLibraryPageTest();
			addQuestionPage.addQuestionRatingType(driver, dataSet);
			fc.utobj().clickElement(driver, pobj.finishBtn);

			fc.utobj().printTestStep("Navigate To FieldOps > Visits Page");
			fc.utobj().printTestStep("Create Visit For Franchise User");
			dataSet.put("fullName", ownerFirstName+" "+ownerLastName);
			
			createVisitAtVisitWithEmailNotification(driver, config, dataSet, visitFormName, franchiseId, corpUser.getuserFullName());
			
			fc.home_page().logout(driver);

			fc.utobj().printTestStep("Login With Franchise User");
			fc.loginpage().loginWithParameter(driver, franUserName, password);

			fc.utobj().printTestStep("Navigate To FieldOps > Visits Page");
			fc.fieldops().fieldops_common().fieldopsVisit(driver);

			fc.utobj().printTestStep("Verify Not Applicable Option While Filling Response");
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//tr/td[contains(text () ," + "'" + visitFormName + "'" + ")]/preceding-sibling::td/a"));

			String alterText = fc.utobj().getElementByXpath(driver, ".//div[@id='tabs']//label").getAttribute("id");
			alterText = alterText.replace("_1", "");

			String ownerResponse = "5";
			String ownerComments = "Owner Comments123";

			fc.utobj().sendKeys(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='Response2_" + alterText + "']"),
					ownerResponse);
			fc.utobj().sendKeys(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='Comment2_" + alterText + "']"),
					ownerComments);
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//*[@id='NotApplicable" + alterText + "']"));

			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='nextSubmitAllButton']"));

			fc.home_page().logout(driver);

			fc.utobj().printTestStep("Login With Corporate User");
			fc.loginpage().loginWithParameter(driver, corpUser.getUserName(), corpUser.getPassword());
			fc.utobj().printTestStep("Navigate To FieldOps > Visits Page");
			fc.fieldops().fieldops_common().fieldopsVisit(driver);
			defaultViewByFranchiseId(driver, franchiseId);
			fc.utobj().printTestStep("Start Visit");

			actionImgOption(driver, franchiseId, "Start Visit");

			fc.utobj().sendKeys(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='Response1_" + alterText + "']"),
					"5");
			fc.utobj().sendKeys(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='Comment1_" + alterText + "']"),
					"Consultant Comments");
			fc.utobj().sendKeys(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='private" + alterText + "']"),
					"Private Comments");
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//*[@id='NotApplicable" + alterText + "']"));

			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='nextSubmitAllButton']"));
			fc.utobj().clickElement(driver, pobj.submitAllBtn);

			fc.utobj().printTestStep("Verify Not Applicable Option");

			fc.utobj().getElementByXpath(driver, ".//tr[2]/td[2][contains(text () , 'N/A')]").isEnabled();
			fc.utobj().getElementByXpath(driver, ".//tr[2]/td[5][contains(text () , 'N/A')]").isEnabled();

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "fieldopsTest")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify numbering after submitting response while Allow Questions Numbering in Section Wise is Yes", testCaseId = "TC_120_Verify_Question_Numbering_After_Filling_Response")
	private void verifyQuestionNumberingFillingResposne() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("fieldops", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {

			driver = fc.loginpage().login(driver);
			FieldOpsVisitsPage pobj = new FieldOpsVisitsPage(driver);
			String password = "t0n1ght123";

			fc.utobj().printTestStep("Navigate To Admin > Hidden Links > Configure FieldOps Settings Page");
			fc.utobj().printTestStep("Configure Field Ops Settings Log Private Comment Yes");
			AdminHiddenLinksConfigureFieldOpsSettingsPageTest hiddenLinks = new AdminHiddenLinksConfigureFieldOpsSettingsPageTest();
			hiddenLinks.configureFieldOpsSettingsLogPrivateCommentYes(driver, config);

			fc.utobj().printTestStep("Navigate To Admin > Users > Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUser = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			String emailId = "fieldopsautomation@staffex.com";
			CorporateUser corpUser = new CorporateUser();
			corpUser.setConsultant("Y");
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporateUser.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			fc.utobj().printTestStep("Navigate To Admin > Franchise Location > Add Franchise Location");
			fc.utobj().printTestStep("Add Franchise Location");
			String regionName = fc.utobj().generateTestData("Test");
			String storeType = fc.utobj().generateTestData("Test");
			String franchiseId = fc.utobj().generateTestData("Test");
			AdminFranchiseLocationAddFranchiseLocationPageTest franchise = new AdminFranchiseLocationAddFranchiseLocationPageTest();
			franchiseId = franchise.addFranchiseLocation_All(driver, franchiseId, regionName, storeType);

			fc.utobj().printTestStep("Add Franchise User");
			AdminUsersManageManageFranchiseUsersPageTest franUser = new AdminUsersManageManageFranchiseUsersPageTest();
			String FranUserName = fc.utobj().generateTestData("TestF");
			String roleName = "Default Franchise Role";
			franUser.addFranchiseUser(driver, FranUserName, password, franchiseId, roleName, emailId);

			AdminFieldOpsManageVisitFormCustomizeVisitFormPage pobj1 = new AdminFieldOpsManageVisitFormCustomizeVisitFormPage(
					driver);

			AdminHiddenLinksConfigureFieldOpsSettingsPageTest fieldopsSettings = new AdminHiddenLinksConfigureFieldOpsSettingsPageTest();
			fieldopsSettings.allowQuestionsNumberingInSectionWiseYes(driver, config);

			fc.utobj().printTestStep("Navigate To Admin > Fieldops > Manage Visit Form");
			fc.utobj().printTestStep("Add Private Visit Form");
			AdminFieldOpsManageVisitFormPageTest visitForm = new AdminFieldOpsManageVisitFormPageTest();
			String visitName = visitForm.addVisitFormPrivate(driver, dataSet);

			// Add Section
			AdminFieldOpsManageVisitFormCustomizeVisitFormPageTest customizeVisitPage = new AdminFieldOpsManageVisitFormCustomizeVisitFormPageTest();
			String sectionName = customizeVisitPage.addSection(driver, config, dataSet);

			fc.utobj().printTestStep("Add Question In Section");
			fc.utobj().clickElement(driver, pobj1.addQuestionInSection);
			customizeVisitPage.addQuestion(driver, dataSet);

			fc.utobj().printTestStep("Add Question In Section");
			fc.utobj().clickElement(driver, pobj1.addQuestionInSection);
			customizeVisitPage.addQuestion(driver, dataSet);

			fc.utobj().printTestStep("Add Question In Section");
			fc.utobj().clickElement(driver, pobj1.addQuestionInSection);
			customizeVisitPage.addQuestion(driver, dataSet);

			// Add Section
			String sectionName1 = customizeVisitPage.addSection(driver, config, dataSet);

			fc.utobj().printTestStep("Add Question In Section");
			// Add Question into a section
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//*[.=" + "'" + sectionName1 + "'" + "]/../a[@id='addqueslink']"));
			customizeVisitPage.addQuestion(driver, dataSet);

			fc.utobj().printTestStep("Add Question In Section");
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//*[.=" + "'" + sectionName1 + "'" + "]/../a[@id='addqueslink']"));
			customizeVisitPage.addQuestion(driver, dataSet);

			fc.utobj().printTestStep("Add Question In Section");
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//*[.=" + "'" + sectionName1 + "'" + "]/../a[@id='addqueslink']"));
			customizeVisitPage.addQuestion(driver, dataSet);

			fc.utobj().clickElement(driver, pobj1.finishBtn);
			fc.utobj().printTestStep("Navigate To FieldOps > Visits Page");
			fc.fieldops().fieldops_common().fieldopsVisit(driver);

			fc.utobj().printTestStep("Create Visit");
			createVisitAtVisitPage(driver, config, dataSet, visitName, franchiseId, corpUser.getuserFullName());

			fc.utobj().printTestStep("Start Visit");
			defaultViewByFranchiseId(driver, franchiseId);
			actionImgOption(driver, franchiseId, "Start Visit");

			fc.utobj().printTestStep("Verify Question Numbering Before Submit Response");
			// Before Submit Response

			fc.utobj()
					.getElementByXpath(driver,
							".//tr/td/strong[contains(text () ," + "'" + sectionName + "'"
									+ ")]/../../../../following-sibling::table[1]//strong[contains(text() , '1')]")
					.isEnabled();
			fc.utobj()
					.getElementByXpath(driver,
							".//tr/td/strong[contains(text () ," + "'" + sectionName + "'"
									+ ")]/../../../../following-sibling::table[2]//strong[contains(text() , '2')]")
					.isEnabled();
			fc.utobj()
					.getElementByXpath(driver,
							".//tr/td/strong[contains(text () ," + "'" + sectionName + "'"
									+ ")]/../../../../following-sibling::table[3]//strong[contains(text() , '3')]")
					.isEnabled();
			fc.utobj()
					.getElementByXpath(driver,
							".//tr/td/strong[contains(text () ," + "'" + sectionName1 + "'"
									+ ")]/../../../../following-sibling::table[1]//strong[contains(text() , '1')]")
					.isEnabled();
			fc.utobj()
					.getElementByXpath(driver,
							".//tr/td/strong[contains(text () ," + "'" + sectionName1 + "'"
									+ ")]/../../../../following-sibling::table[2]//strong[contains(text() , '2')]")
					.isEnabled();
			fc.utobj()
					.getElementByXpath(driver,
							".//tr/td/strong[contains(text () ," + "'" + sectionName1 + "'"
									+ ")]/../../../../following-sibling::table[3]//strong[contains(text() , '3')]")
					.isEnabled();

			fc.utobj().clickElement(driver, pobj.nextBtnSubmit);

			fc.utobj().clickElement(driver, pobj.submitBtnAtVisit);

			fc.utobj().printTestStep("Verify Question Numbering At Summary Page After Submit Response");
			// At summary Page
			fc.utobj()
					.getElementByXpath(driver,
							".//tr/td/strong[contains(text () ," + "'" + sectionName + "'"
									+ ")]/../../../../following-sibling::table[1]//strong[contains(text() , '1')]")
					.isEnabled();
			fc.utobj()
					.getElementByXpath(driver,
							".//tr/td/strong[contains(text () ," + "'" + sectionName + "'"
									+ ")]/../../../../following-sibling::table[2]//strong[contains(text() , '2')]")
					.isEnabled();
			fc.utobj()
					.getElementByXpath(driver,
							".//tr/td/strong[contains(text () ," + "'" + sectionName + "'"
									+ ")]/../../../../following-sibling::table[3]//strong[contains(text() , '3')]")
					.isEnabled();
			fc.utobj()
					.getElementByXpath(driver,
							".//tr/td/strong[contains(text () ," + "'" + sectionName1 + "'"
									+ ")]/../../../../following-sibling::table[1]//strong[contains(text() , '1')]")
					.isEnabled();
			fc.utobj()
					.getElementByXpath(driver,
							".//tr/td/strong[contains(text () ," + "'" + sectionName1 + "'"
									+ ")]/../../../../following-sibling::table[2]//strong[contains(text() , '2')]")
					.isEnabled();
			fc.utobj()
					.getElementByXpath(driver,
							".//tr/td/strong[contains(text () ," + "'" + sectionName1 + "'"
									+ ")]/../../../../following-sibling::table[3]//strong[contains(text() , '3')]")
					.isEnabled();

			fc.utobj().clickElement(driver, pobj.okBtn);

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "fieldops")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify numbering after submitting response while Allow Questions Numbering in Section Wise is No", testCaseId = "TC_121_Verify_Question_Numbering_After_Filling_Response_No")
	private void verifyQuestionNumberingFillingResposneNo() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("fieldops", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {

			driver = fc.loginpage().login(driver);
			FieldOpsVisitsPage pobj = new FieldOpsVisitsPage(driver);
			String password = "t0n1ght123";

			fc.utobj().printTestStep("Navigate To Admin > Hidden Links > Configure FieldOps Settings Page");
			fc.utobj().printTestStep("Configure Field Ops Settings Log Private Comment Yes");
			AdminHiddenLinksConfigureFieldOpsSettingsPageTest hiddenLinks = new AdminHiddenLinksConfigureFieldOpsSettingsPageTest();
			hiddenLinks.configureFieldOpsSettingsLogPrivateCommentYes(driver, config);

			fc.utobj().printTestStep("Navigate To Admin > Users > Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUser = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			String emailId = "fieldopsautomation@staffex.com";
			CorporateUser corpUser = new CorporateUser();
			corpUser.setConsultant("Y");
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporateUser.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			fc.utobj().printTestStep("Navigate To Admin > Franchise Location > Add Franchise Location");
			fc.utobj().printTestStep("Add Franchise Location");
			String regionName = fc.utobj().generateTestData("Test");
			String storeType = fc.utobj().generateTestData("Test");
			String franchiseId = fc.utobj().generateTestData("Test");
			AdminFranchiseLocationAddFranchiseLocationPageTest franchise = new AdminFranchiseLocationAddFranchiseLocationPageTest();
			franchiseId = franchise.addFranchiseLocation_All(driver, franchiseId, regionName, storeType);

			fc.utobj().printTestStep("Add Franchise User");
			AdminUsersManageManageFranchiseUsersPageTest franUser = new AdminUsersManageManageFranchiseUsersPageTest();
			String FranUserName = fc.utobj().generateTestData("TestF");
			String roleName = "Default Franchise Role";
			franUser.addFranchiseUser(driver, FranUserName, password, franchiseId, roleName, emailId);

			AdminFieldOpsManageVisitFormCustomizeVisitFormPage pobj1 = new AdminFieldOpsManageVisitFormCustomizeVisitFormPage(
					driver);

			AdminHiddenLinksConfigureFieldOpsSettingsPageTest fieldopsSettings = new AdminHiddenLinksConfigureFieldOpsSettingsPageTest();
			fieldopsSettings.allowQuestionsNumberingInSectionWiseNo(driver, config);

			fc.utobj().printTestStep("Navigate To Admin > Fieldops > Manage Visit Form");
			fc.utobj().printTestStep("Add Private Visit Form");
			AdminFieldOpsManageVisitFormPageTest visitForm = new AdminFieldOpsManageVisitFormPageTest();
			String visitName = visitForm.addVisitFormPrivate(driver, dataSet);

			// Add Section
			AdminFieldOpsManageVisitFormCustomizeVisitFormPageTest customizeVisitPage = new AdminFieldOpsManageVisitFormCustomizeVisitFormPageTest();
			String sectionName = customizeVisitPage.addSection(driver, config, dataSet);

			fc.utobj().printTestStep("Add Question In Section");
			fc.utobj().clickElement(driver, pobj1.addQuestionInSection);
			customizeVisitPage.addQuestion(driver, dataSet);

			fc.utobj().printTestStep("Add Question In Section");
			fc.utobj().clickElement(driver, pobj1.addQuestionInSection);
			customizeVisitPage.addQuestion(driver, dataSet);

			fc.utobj().printTestStep("Add Question In Section");
			fc.utobj().clickElement(driver, pobj1.addQuestionInSection);
			customizeVisitPage.addQuestion(driver, dataSet);

			// Add Section
			String sectionName1 = customizeVisitPage.addSection(driver, config, dataSet);

			fc.utobj().printTestStep("Add Question In Section");
			// Add Question into a section
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//*[.=" + "'" + sectionName1 + "'" + "]/../a[@id='addqueslink']"));
			customizeVisitPage.addQuestion(driver, dataSet);

			fc.utobj().printTestStep("Add Question In Section");
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//*[.=" + "'" + sectionName1 + "'" + "]/../a[@id='addqueslink']"));
			customizeVisitPage.addQuestion(driver, dataSet);

			fc.utobj().printTestStep("Add Question In Section");
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//*[.=" + "'" + sectionName1 + "'" + "]/../a[@id='addqueslink']"));
			customizeVisitPage.addQuestion(driver, dataSet);

			fc.utobj().clickElement(driver, pobj1.finishBtn);

			fc.utobj().printTestStep("Navigate To FieldOps > Visits Page");
			fc.fieldops().fieldops_common().fieldopsVisit(driver);

			fc.utobj().printTestStep("Create Visit");
			createVisitAtVisitPage(driver, config, dataSet, visitName, franchiseId, corpUser.getuserFullName());

			fc.utobj().printTestStep("Start Visit");
			defaultViewByFranchiseId(driver, franchiseId);
			actionImgOption(driver, franchiseId, "Start Visit");

			fc.utobj().printTestStep("Verify Numbering Before Submit Response");

			// Before Submit Response

			fc.utobj()
					.getElementByXpath(driver,
							".//tr/td/strong[contains(text () ," + "'" + sectionName + "'"
									+ ")]/../../../../following-sibling::table[1]//strong[contains(text() , '1')]")
					.isEnabled();
			fc.utobj()
					.getElementByXpath(driver,
							".//tr/td/strong[contains(text () ," + "'" + sectionName + "'"
									+ ")]/../../../../following-sibling::table[2]//strong[contains(text() , '2')]")
					.isEnabled();
			fc.utobj()
					.getElementByXpath(driver,
							".//tr/td/strong[contains(text () ," + "'" + sectionName + "'"
									+ ")]/../../../../following-sibling::table[3]//strong[contains(text() , '3')]")
					.isEnabled();
			fc.utobj()
					.getElementByXpath(driver,
							".//tr/td/strong[contains(text () ," + "'" + sectionName1 + "'"
									+ ")]/../../../../following-sibling::table[1]//strong[contains(text() , '4')]")
					.isEnabled();
			fc.utobj()
					.getElementByXpath(driver,
							".//tr/td/strong[contains(text () ," + "'" + sectionName1 + "'"
									+ ")]/../../../../following-sibling::table[2]//strong[contains(text() , '5')]")
					.isEnabled();
			fc.utobj()
					.getElementByXpath(driver,
							".//tr/td/strong[contains(text () ," + "'" + sectionName1 + "'"
									+ ")]/../../../../following-sibling::table[3]//strong[contains(text() , '6')]")
					.isEnabled();

			fc.utobj().clickElement(driver, pobj.nextBtnSubmit);

			fc.utobj().clickElement(driver, pobj.submitBtnAtVisit);

			fc.utobj().printTestStep("Verify Numbering At Summary Page After Submit Response");
			// At summary Page
			fc.utobj()
					.getElementByXpath(driver,
							".//tr/td/strong[contains(text () ," + "'" + sectionName + "'"
									+ ")]/../../../../following-sibling::table[1]//strong[contains(text() , '1')]")
					.isEnabled();
			fc.utobj()
					.getElementByXpath(driver,
							".//tr/td/strong[contains(text () ," + "'" + sectionName + "'"
									+ ")]/../../../../following-sibling::table[2]//strong[contains(text() , '2')]")
					.isEnabled();
			fc.utobj()
					.getElementByXpath(driver,
							".//tr/td/strong[contains(text () ," + "'" + sectionName + "'"
									+ ")]/../../../../following-sibling::table[3]//strong[contains(text() , '3')]")
					.isEnabled();
			fc.utobj()
					.getElementByXpath(driver,
							".//tr/td/strong[contains(text () ," + "'" + sectionName1 + "'"
									+ ")]/../../../../following-sibling::table[1]//strong[contains(text() , '4')]")
					.isEnabled();
			fc.utobj()
					.getElementByXpath(driver,
							".//tr/td/strong[contains(text () ," + "'" + sectionName1 + "'"
									+ ")]/../../../../following-sibling::table[2]//strong[contains(text() , '5')]")
					.isEnabled();
			fc.utobj()
					.getElementByXpath(driver,
							".//tr/td/strong[contains(text () ," + "'" + sectionName1 + "'"
									+ ")]/../../../../following-sibling::table[3]//strong[contains(text() , '6')]")
					.isEnabled();

			fc.utobj().clickElement(driver, pobj.okBtn);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "fieldops", "fieldops_5050" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify Question numbering after Upadeting Visit while Allow Questions Numbering in Section Wise is No", testCaseId = "TC_123_Verify_Question_Numbering_After_Update_Visit_No")
	private void verifyQuestionNumberingUpdateVisitNo() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("fieldops", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {

			driver = fc.loginpage().login(driver);
			FieldOpsVisitsPage pobj = new FieldOpsVisitsPage(driver);
			String password = "t0n1ght123";

			fc.utobj().printTestStep("Navigate To Admin > Hidden Links > Configure FieldOps Settings Page");
			fc.utobj().printTestStep("Configure Field Ops Settings Log Private Comment Yes");
			AdminHiddenLinksConfigureFieldOpsSettingsPageTest hiddenLinks = new AdminHiddenLinksConfigureFieldOpsSettingsPageTest();
			hiddenLinks.configureFieldOpsSettingsLogPrivateCommentYes(driver, config);

			fc.utobj().printTestStep("Navigate To Admin > Users > Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUser = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			String emailId = "fieldopsautomation@staffex.com";
			CorporateUser corpUser = new CorporateUser();
			corpUser.setConsultant("Y");
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporateUser.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			fc.utobj().printTestStep("Navigate To Admin > Franchise Location > Add Franchise Location");
			fc.utobj().printTestStep("Add Franchise Location");
			String regionName = fc.utobj().generateTestData("Test");
			String storeType = fc.utobj().generateTestData("Test");
			String franchiseId = fc.utobj().generateTestData("Test");
			AdminFranchiseLocationAddFranchiseLocationPageTest franchise = new AdminFranchiseLocationAddFranchiseLocationPageTest();
			franchiseId = franchise.addFranchiseLocation_All(driver, franchiseId, regionName, storeType);

			fc.utobj().printTestStep("Add Franchise User");
			AdminUsersManageManageFranchiseUsersPageTest franUser = new AdminUsersManageManageFranchiseUsersPageTest();
			String FranUserName = fc.utobj().generateTestData("TestF");
			String roleName = "Default Franchise Role";

			franUser.addFranchiseUser(driver, FranUserName, password, franchiseId, roleName, emailId);

			AdminFieldOpsManageVisitFormCustomizeVisitFormPage pobj1 = new AdminFieldOpsManageVisitFormCustomizeVisitFormPage(
					driver);

			AdminHiddenLinksConfigureFieldOpsSettingsPageTest fieldopsSettings = new AdminHiddenLinksConfigureFieldOpsSettingsPageTest();
			fieldopsSettings.allowQuestionsNumberingInSectionWiseNo(driver, config);

			fc.utobj().printTestStep("Navigate To Admin > Fieldops > Manage Visit Form");
			fc.utobj().printTestStep("Add Private Visit Form");
			AdminFieldOpsManageVisitFormPageTest visitForm = new AdminFieldOpsManageVisitFormPageTest();
			String visitName = visitForm.addVisitFormPrivate(driver, dataSet);

			// Add Section
			AdminFieldOpsManageVisitFormCustomizeVisitFormPageTest customizeVisitPage = new AdminFieldOpsManageVisitFormCustomizeVisitFormPageTest();
			String sectionName = customizeVisitPage.addSection(driver, config, dataSet);

			fc.utobj().printTestStep("Add Question In Section");
			fc.utobj().clickElement(driver, pobj1.addQuestionInSection);
			customizeVisitPage.addQuestion(driver, dataSet);

			fc.utobj().printTestStep("Add Question In Section");
			fc.utobj().clickElement(driver, pobj1.addQuestionInSection);
			customizeVisitPage.addQuestion(driver, dataSet);

			fc.utobj().printTestStep("Add Question In Section");
			fc.utobj().clickElement(driver, pobj1.addQuestionInSection);
			customizeVisitPage.addQuestion(driver, dataSet);

			// Add Section
			String sectionName1 = customizeVisitPage.addSection(driver, config, dataSet);

			fc.utobj().printTestStep("Add Question In Section");
			// Add Question into a section
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//*[.=" + "'" + sectionName1 + "'" + "]/../a[@id='addqueslink']"));
			customizeVisitPage.addQuestion(driver, dataSet);

			fc.utobj().printTestStep("Add Question In Section");
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//*[.=" + "'" + sectionName1 + "'" + "]/../a[@id='addqueslink']"));
			customizeVisitPage.addQuestion(driver, dataSet);

			fc.utobj().printTestStep("Add Question In Section");
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//*[.=" + "'" + sectionName1 + "'" + "]/../a[@id='addqueslink']"));
			customizeVisitPage.addQuestion(driver, dataSet);

			fc.utobj().clickElement(driver, pobj1.finishBtn);

			fc.utobj().printTestStep("Navigate To FieldOps > Visits Page");
			fc.utobj().printTestStep("Create Visit");
			fc.fieldops().fieldops_common().fieldopsVisit(driver);
			createVisitAtVisitPage(driver, config, dataSet, visitName, franchiseId, corpUser.getuserFullName());

			fc.utobj().printTestStep("Start Visit");
			defaultViewByFranchiseId(driver, franchiseId);
			actionImgOption(driver, franchiseId, "Start Visit");

			fc.utobj().printTestStep("Verify Question Numbering Before Submit Response");

			// Before Submit Response

			fc.utobj()
					.getElementByXpath(driver,
							".//tr/td/strong[contains(text () ," + "'" + sectionName + "'"
									+ ")]/../../../../following-sibling::table[1]//strong[contains(text() , '1')]")
					.isEnabled();
			fc.utobj()
					.getElementByXpath(driver,
							".//tr/td/strong[contains(text () ," + "'" + sectionName + "'"
									+ ")]/../../../../following-sibling::table[2]//strong[contains(text() , '2')]")
					.isEnabled();
			fc.utobj()
					.getElementByXpath(driver,
							".//tr/td/strong[contains(text () ," + "'" + sectionName + "'"
									+ ")]/../../../../following-sibling::table[3]//strong[contains(text() , '3')]")
					.isEnabled();
			fc.utobj()
					.getElementByXpath(driver,
							".//tr/td/strong[contains(text () ," + "'" + sectionName1 + "'"
									+ ")]/../../../../following-sibling::table[1]//strong[contains(text() , '4')]")
					.isEnabled();
			fc.utobj()
					.getElementByXpath(driver,
							".//tr/td/strong[contains(text () ," + "'" + sectionName1 + "'"
									+ ")]/../../../../following-sibling::table[2]//strong[contains(text() , '5')]")
					.isEnabled();
			fc.utobj()
					.getElementByXpath(driver,
							".//tr/td/strong[contains(text () ," + "'" + sectionName1 + "'"
									+ ")]/../../../../following-sibling::table[3]//strong[contains(text() , '6')]")
					.isEnabled();

			// Enter Data
			String alterText = fc.utobj()
					.getElementByXpath(driver, ".//table[@class='bText12gr']//tr/td/strong[.='1.']/../label")
					.getAttribute("id").trim();
			alterText = alterText.replace("_0", "");

			fc.utobj().sendKeys(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='Response_" + alterText + "']"),
					"Response 1");
			fc.utobj().sendKeys(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='private" + alterText + "']"),
					"Private Respponse 1");

			alterText = fc.utobj()
					.getElementByXpath(driver, ".//table[@class='bText12gr grAltRw2b']//tr/td/strong[.='2.']/../label")
					.getAttribute("id").trim();
			alterText = alterText.replace("_1", "");

			fc.utobj().sendKeys(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='Response_" + alterText + "']"),
					"Response 2");
			fc.utobj().sendKeys(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='private" + alterText + "']"),
					"Private Respponse 2");

			alterText = fc.utobj()
					.getElementByXpath(driver, ".//table[@class='bText12gr']//tr/td/strong[.='3.']/../label")
					.getAttribute("id").trim();
			alterText = alterText.replace("_2", "");

			fc.utobj().sendKeys(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='Response_" + alterText + "']"),
					"Response 3");
			fc.utobj().sendKeys(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='private" + alterText + "']"),
					"Private Respponse 3");

			alterText = fc.utobj()
					.getElementByXpath(driver, ".//table[@class='bText12gr grAltRw2b']//tr/td/strong[.='4.']/../label")
					.getAttribute("id").trim();
			alterText = alterText.replace("_0", "");

			fc.utobj().sendKeys(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='Response_" + alterText + "']"),
					"Response 21");
			fc.utobj().sendKeys(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='private" + alterText + "']"),
					"First Private Respponse 21");

			alterText = fc.utobj()
					.getElementByXpath(driver, ".//table[@class='bText12gr']//tr/td/strong[.='5.']/../label")
					.getAttribute("id").trim();
			alterText = alterText.replace("_1", "");

			fc.utobj().sendKeys(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='Response_" + alterText + "']"),
					"Response 22");
			fc.utobj().sendKeys(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='private" + alterText + "']"),
					"Private Respponse 22");

			alterText = fc.utobj()
					.getElementByXpath(driver, ".//table[@class='bText12gr grAltRw2b']//tr/td/strong[.='6.']/../label")
					.getAttribute("id").trim();
			alterText = alterText.replace("_2", "");

			fc.utobj().sendKeys(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='Response_" + alterText + "']"),
					"Response 23");
			fc.utobj().sendKeys(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='private" + alterText + "']"),
					"First Private Respponse 23");

			fc.utobj().clickElement(driver, pobj.submitAllBtn);
			fc.utobj().clickElement(driver, pobj.submitBtnAtVisit);

			fc.utobj().printTestStep("Verify Question Numbering At Summary Page After Submit Response");
			// At summary Page
			fc.utobj()
					.getElementByXpath(driver,
							".//tr/td/strong[contains(text () ," + "'" + sectionName + "'"
									+ ")]/../../../../following-sibling::table[1]//strong[contains(text() , '1')]")
					.isEnabled();
			fc.utobj()
					.getElementByXpath(driver,
							".//tr/td/strong[contains(text () ," + "'" + sectionName + "'"
									+ ")]/../../../../following-sibling::table[2]//strong[contains(text() , '2')]")
					.isEnabled();
			fc.utobj()
					.getElementByXpath(driver,
							".//tr/td/strong[contains(text () ," + "'" + sectionName + "'"
									+ ")]/../../../../following-sibling::table[3]//strong[contains(text() , '3')]")
					.isEnabled();
			fc.utobj()
					.getElementByXpath(driver,
							".//tr/td/strong[contains(text () ," + "'" + sectionName1 + "'"
									+ ")]/../../../../following-sibling::table[1]//strong[contains(text() , '4')]")
					.isEnabled();
			fc.utobj()
					.getElementByXpath(driver,
							".//tr/td/strong[contains(text () ," + "'" + sectionName1 + "'"
									+ ")]/../../../../following-sibling::table[2]//strong[contains(text() , '5')]")
					.isEnabled();
			fc.utobj()
					.getElementByXpath(driver,
							".//tr/td/strong[contains(text () ," + "'" + sectionName1 + "'"
									+ ")]/../../../../following-sibling::table[3]//strong[contains(text() , '6')]")
					.isEnabled();

			fc.utobj().clickElement(driver, pobj.okBtn);
			defaultViewByFranchiseId(driver, franchiseId);

			fc.utobj().printTestStep("Update Visit");

			actionImgOption(driver, franchiseId, "Update Visit");

			fc.utobj().printTestStep("Verify Question Numbering At Update Page");
			// At Update Page
			fc.utobj()
					.getElementByXpath(driver,
							".//tr/td/strong[contains(text () ," + "'" + sectionName + "'"
									+ ")]/../../../../following-sibling::table[1]//strong[contains(text() , '1')]")
					.isEnabled();
			fc.utobj()
					.getElementByXpath(driver,
							".//tr/td/strong[contains(text () ," + "'" + sectionName + "'"
									+ ")]/../../../../following-sibling::table[2]//strong[contains(text() , '2')]")
					.isEnabled();
			fc.utobj()
					.getElementByXpath(driver,
							".//tr/td/strong[contains(text () ," + "'" + sectionName + "'"
									+ ")]/../../../../following-sibling::table[3]//strong[contains(text() , '3')]")
					.isEnabled();
			fc.utobj()
					.getElementByXpath(driver,
							".//tr/td/strong[contains(text () ," + "'" + sectionName1 + "'"
									+ ")]/../../../../following-sibling::table[1]//strong[contains(text() , '4')]")
					.isEnabled();
			fc.utobj()
					.getElementByXpath(driver,
							".//tr/td/strong[contains(text () ," + "'" + sectionName1 + "'"
									+ ")]/../../../../following-sibling::table[2]//strong[contains(text() , '5')]")
					.isEnabled();
			fc.utobj()
					.getElementByXpath(driver,
							".//tr/td/strong[contains(text () ," + "'" + sectionName1 + "'"
									+ ")]/../../../../following-sibling::table[3]//strong[contains(text() , '6')]")
					.isEnabled();

			// Update Visit Data
			alterText = fc.utobj()
					.getElementByXpath(driver, ".//table[@class='bText12gr']//tr/td/strong[.='1.']/../label")
					.getAttribute("id").trim();
			alterText = alterText.replace("_0", "");

			fc.utobj().sendKeys(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='Response_" + alterText + "']"),
					"Response 1 Update");
			fc.utobj().sendKeys(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='private" + alterText + "']"),
					"Private Respponse 1 Update");

			alterText = fc.utobj()
					.getElementByXpath(driver, ".//table[@class='bText12gr grAltRw2b']//tr/td/strong[.='2.']/../label")
					.getAttribute("id").trim();
			alterText = alterText.replace("_1", "");

			fc.utobj().sendKeys(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='Response_" + alterText + "']"),
					"Response 2 Update");
			fc.utobj().sendKeys(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='private" + alterText + "']"),
					"Private Respponse 2 Update");

			alterText = fc.utobj()
					.getElementByXpath(driver, ".//table[@class='bText12gr']//tr/td/strong[.='3.']/../label")
					.getAttribute("id").trim();
			alterText = alterText.replace("_2", "");

			fc.utobj().sendKeys(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='Response_" + alterText + "']"),
					"Response 3 Update");
			fc.utobj().sendKeys(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='private" + alterText + "']"),
					"Private Respponse 3 Update");

			alterText = fc.utobj()
					.getElementByXpath(driver, ".//table[@class='bText12gr grAltRw2b']//tr/td/strong[.='4.']/../label")
					.getAttribute("id").trim();
			alterText = alterText.replace("_0", "");

			fc.utobj().sendKeys(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='Response_" + alterText + "']"),
					"Response 21 Update");
			fc.utobj().sendKeys(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='private" + alterText + "']"),
					"First Private Respponse 21 Update");

			alterText = fc.utobj()
					.getElementByXpath(driver, ".//table[@class='bText12gr']//tr/td/strong[.='5.']/../label")
					.getAttribute("id").trim();
			alterText = alterText.replace("_1", "");

			fc.utobj().sendKeys(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='Response_" + alterText + "']"),
					"Response 22 Update");
			fc.utobj().sendKeys(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='private" + alterText + "']"),
					"Private Respponse 22 Update");

			alterText = fc.utobj()
					.getElementByXpath(driver, ".//table[@class='bText12gr grAltRw2b']//tr/td/strong[.='6.']/../label")
					.getAttribute("id").trim();
			alterText = alterText.replace("_2", "");

			fc.utobj().sendKeys(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='Response_" + alterText + "']"),
					"Response 23 Update");
			fc.utobj().sendKeys(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='private" + alterText + "']"),
					"First Private Respponse 23 Update");

			fc.utobj().clickElement(driver, pobj.submitAllBtn);
			fc.utobj().clickElement(driver, pobj.submitBtnAtVisit);

			fc.utobj().printTestStep("Verify Question Numbering After Update Visit At Summary Page");
			// After Update At summary Page

			fc.utobj()
					.getElementByXpath(driver,
							".//tr/td/strong[contains(text () ," + "'" + sectionName + "'"
									+ ")]/../../../../following-sibling::table[1]//strong[contains(text() , '1')]")
					.isEnabled();
			fc.utobj()
					.getElementByXpath(driver,
							".//tr/td/strong[contains(text () ," + "'" + sectionName + "'"
									+ ")]/../../../../following-sibling::table[2]//strong[contains(text() , '2')]")
					.isEnabled();
			fc.utobj()
					.getElementByXpath(driver,
							".//tr/td/strong[contains(text () ," + "'" + sectionName + "'"
									+ ")]/../../../../following-sibling::table[3]//strong[contains(text() , '3')]")
					.isEnabled();
			fc.utobj()
					.getElementByXpath(driver,
							".//tr/td/strong[contains(text () ," + "'" + sectionName1 + "'"
									+ ")]/../../../../following-sibling::table[1]//strong[contains(text() , '4')]")
					.isEnabled();
			fc.utobj()
					.getElementByXpath(driver,
							".//tr/td/strong[contains(text () ," + "'" + sectionName1 + "'"
									+ ")]/../../../../following-sibling::table[2]//strong[contains(text() , '5')]")
					.isEnabled();
			fc.utobj()
					.getElementByXpath(driver,
							".//tr/td/strong[contains(text () ," + "'" + sectionName1 + "'"
									+ ")]/../../../../following-sibling::table[3]//strong[contains(text() , '6')]")
					.isEnabled();

			fc.utobj().clickElement(driver, pobj.okBtn);

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "fieldops", "fieldopsemail" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify visit mail should go to both consultant and franchise user and also verify owner should be mandatory while creating visit", testCaseId = "TC_128_Verify_Visit_Mail_Consultant_Franchise_User")
	private void verifyVisitMailConsultantFranchiseUser() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("fieldops", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			FieldOpsVisitsPage pobj = new FieldOpsVisitsPage(driver);
			String password1 = "ton1ght123";

			fc.utobj().printTestStep("Navigate To Admin > FieldOps > Manage Visit Form");
			fc.utobj().printTestStep("Add Non-Private Visit Form With Tab View");
			AdminFieldOpsManageVisitFormPageTest visitFormPage = new AdminFieldOpsManageVisitFormPageTest();
			String visitFormName = visitFormPage.addVisitFormTabViewPrivateNo(driver, config, dataSet);

			fc.utobj().printTestStep("Add Franchise Location");
			String regionName = fc.utobj().generateTestData("TestR");
			String storeType = fc.utobj().generateTestData("TestS");
			String franchiseId = fc.utobj().generateTestData("TestFID");
			AdminFranchiseLocationAddFranchiseLocationPageTest franchise = new AdminFranchiseLocationAddFranchiseLocationPageTest();
			String firstName = fc.utobj().generateTestData("firstName");
			franchiseId = franchise.addFranchiseLocation_AllWithFName(driver, franchiseId, regionName, storeType,
					firstName);

			fc.utobj().printTestStep("Add Franchise User");
			AdminUsersManageManageFranchiseUsersPageTest addFranUser = new AdminUsersManageManageFranchiseUsersPageTest();
			String franUserName = fc.utobj().generateTestData("Test");
			String roleName = "Default Franchise Role";
			String emailId = "fieldopsautomation@staffex.com";
			addFranUser.addFranchiseUser(driver, franUserName, password1, franchiseId, roleName, emailId);

			fc.utobj().printTestStep("Navigate To Admin > Users > Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUser = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser = new CorporateUser();
			corpUser.setConsultant("Y");
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporateUser.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			// navigate to createVisitFormPage

			fc.utobj().printTestStep("Navigate To FieldOps > Visits Page");
			fc.fieldops().fieldops_common().fieldopsVisitCreateVisitPage(driver);
			fc.utobj().printTestStep("Create Visit");

			// Click createVisit
			fc.utobj().clickElement(driver, pobj.createVisit);

			// select visit Form
			fc.utobj().clickElement(driver, pobj.selectClick);
			fc.utobj().sendKeys(driver, pobj.searchBox, visitFormName);
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver,
							".//*[@id='ms-parentmasterVisitId']/div/ul/li/label[contains(text () ," + "'"
									+ visitFormName + "'" + ")]/input"));

			// select FranchiseID
			fc.utobj().clickElement(driver, pobj.franchiseeSelect);
			fc.utobj().sendKeys(driver, pobj.searchBoxFranchisee, franchiseId);
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver,
							".//*[@id='ms-parentfranchiseeNo']/div/ul/li/label[contains(text () , " + "'" + franchiseId
									+ "'" + ")]/input"));
			fc.utobj().clickElement(driver, pobj.franchiseeSelect);

			// Select Consultant
			fc.utobj().clickElement(driver, pobj.selectConsultant);
			fc.utobj().sendKeys(driver, pobj.consultantSearchBox, corpUser.getuserFullName());
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver,
							".//*[@id='ms-parentassignTo']/div/ul/li/label[contains(text () , " + "'"
									+ corpUser.getuserFullName() + "'" + ")]/input"));

			// schedule Date
			fc.utobj().sendKeys(driver, pobj.scheduleDateTime, fc.utobj().getCurrentDateUSFormat());
			String comments = dataSet.get("comments");
			fc.utobj().sendKeys(driver, pobj.comments, comments);

			if (fc.utobj().isSelected(driver,pobj.sendMailCheckNotiFranchiseeOwner)) {
			} else {
				fc.utobj().clickElement(driver, pobj.sendMailCheckNotiFranchiseeOwner);
			}

			if (fc.utobj().isSelected(driver,pobj.visitCompletionEmailFranchiseeOwner)) {
			} else {
				fc.utobj().clickElement(driver, pobj.visitCompletionEmailFranchiseeOwner);
			}
			fc.utobj().setToDefault(driver, pobj.ownerSelectBtn);

			fc.utobj().printTestStep("Verify Owner Should Be Mandatory");
			fc.utobj().clickElement(driver, pobj.schedule);

			String alertText = fc.utobj().acceptAlertBox(driver);

			if (!alertText.equalsIgnoreCase("Please select at least one owner for all selected Franchisees.")) {
				fc.utobj().throwsException("was not able to verify owner should be mandatory");
			}

			WebElement elementInput = fc.utobj().getElement(driver, pobj.ownerSelectBtn)
					.findElement(By.xpath("./div/div/input"));
			fc.utobj().selectValFromMultiSelect(driver, pobj.ownerSelectBtn, elementInput, firstName);

			fc.utobj().clickElement(driver, pobj.schedule);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			AdminFieldOpsConfigureScheduleVisitEmailContentPage pobj1 = new AdminFieldOpsConfigureScheduleVisitEmailContentPage(
					driver);

			fc.utobj().setToDefault(driver, pobj.ownerSelect);
			WebElement elementInput1 = fc.utobj().getElement(driver, pobj.ownerSelect)
					.findElement(By.xpath("./div/div/input"));
			fc.utobj().selectValFromMultiSelect(driver, pobj.ownerSelect, elementInput1, firstName);

			String subject1 = dataSet.get("subject");
			fc.utobj().sendKeys(driver, pobj1.subject, subject1);

			if (fc.utobj().isSelected(driver,fc.utobj().getElementByID(driver, "attachAuditForm"))) {
			} else {
				fc.utobj().clickElement(driver, fc.utobj().getElementByID(driver, "attachAuditForm"));
			}

			fc.utobj().clickElement(driver, pobj.sendBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Verify Mail For Consultant");

			String expectedSubject = subject1;
			String expectedMessageBody = visitFormName;
			Map<String, String> mailData = new HashMap<String, String>();

			mailData = fc.utobj().readMailBox(expectedSubject, expectedMessageBody, emailId, "sdg@1a@Hfs");

			if (mailData.size() == 0) {
				mailData = fc.utobj().readMailBox(expectedSubject, expectedMessageBody, emailId, "sdg@1a@Hfs");
			}

			if (mailData.size() == 0 || !mailData.get("mailBody").contains(franchiseId)) {
				fc.utobj().throwsException("was not able to verify Franchise Id In email body");
			}

			if (mailData.size() == 0 || !mailData.get("mailBody").contains(visitFormName)) {
				fc.utobj().throwsException("was not able to verify Visit Form Name In email body");
			}

			if (mailData.size() == 0 || !mailData.get("mailBody").contains(corpUser.getuserFullName())) {
				fc.utobj().throwsException("was not able to verify User Name In email body");
			}

			Map<String, String> mailData1 = new HashMap<String, String>();

			fc.utobj().printTestStep("Verify Visit Schedule Mail To Owner");

			mailData1 = fc.utobj().readMailBox(expectedSubject, firstName, emailId, "sdg@1a@Hfs");

			if (mailData1.size() == 0) {
				mailData1 = fc.utobj().readMailBox(expectedSubject, firstName, emailId, "sdg@1a@Hfs");
			}

			if (mailData1.size() == 0 || !mailData1.get("mailBody").contains(firstName)) {

				fc.utobj().throwsException("was not able to verify Schedule Visit Mail To Owner");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "fieldops")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify Start visit at Info Mgr (QA History). Info Mgr >  Franchisees > QA History ", testCaseId = "TC_129_Verify_Start_Visit_At_Info_Mgr")
	private void verifyStartVisitAtInfoMgr() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("fieldops", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			FieldOpsVisitsPage pobj = new FieldOpsVisitsPage(driver);

			fc.utobj().printTestStep("Navigate To Admin > Hidden Links > Configure FieldOps Settings Page");
			fc.utobj().printTestStep("Configure Field Ops Settings Log Private Comment Yes");
			AdminHiddenLinksConfigureFieldOpsSettingsPageTest hiddenLinks = new AdminHiddenLinksConfigureFieldOpsSettingsPageTest();
			hiddenLinks.configureFieldOpsSettingsLogPrivateCommentYes(driver, config);

			fc.utobj().printTestStep("Navigate To Admin > Fieldops > Manage Visit Form");
			fc.utobj().printTestStep("Add Private Visit Form");
			AdminFieldOpsManageVisitFormPageTest visitFormPage = new AdminFieldOpsManageVisitFormPageTest();
			String visitFormName = visitFormPage.addVisitFormPrivate(driver, dataSet);

			fc.utobj().printTestStep("Add Question In Section");
			fc.utobj().clickElement(driver, pobj.addQuestion);

			AdminFieldOpsManageVisitFormCustomizeVisitFormPageTest customizeVisit = new AdminFieldOpsManageVisitFormCustomizeVisitFormPageTest();
			customizeVisit.addQuestion(driver, dataSet);

			fc.utobj().clickElement(driver, pobj.finishBtn);

			fc.utobj().printTestStep("Navigate To Admin > Franchise Location > Add Franchise Location");
			fc.utobj().printTestStep("Add Franchise Location");
			String regionName = fc.utobj().generateTestData("TestR");
			String storeType = fc.utobj().generateTestData("TestS");
			String franchiseId = fc.utobj().generateTestData("TestID");
			AdminFranchiseLocationAddFranchiseLocationPageTest franchise = new AdminFranchiseLocationAddFranchiseLocationPageTest();
			franchiseId = franchise.addFranchiseLocation_All(driver, franchiseId, regionName, storeType);

			fc.utobj().printTestStep("Navigate To Admin > Users > Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUser = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			/*
			 * String userName=fc.utobj().generateTestData("Test"); String
			 * password="ton1ght123";
			 * userName=corporateUser.addCorporateUser(driver, userName,
			 * password, config);
			 */
			String emailId = "fieldopsautomation@staffex.com";
			CorporateUser corpUser = new CorporateUser();
			corpUser.setConsultant("Y");
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporateUser.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			fc.utobj().printTestStep("Navigate To Admin > FieldOps > QA Tab Integration Page");
			fc.utobj().printTestStep("Enable QA Tab Integration Yes");

			AdminFieldOpsQATabIntegrationPageTest QaTabPage = new AdminFieldOpsQATabIntegrationPageTest();
			QaTabPage.qaTabIntegrationYes(driver, config);

			fc.utobj().printTestStep("Navigate To Info Mgr > Franchisee");
			fc.utobj().printTestStep("Create Visit");
			createVisitAtInfoMgrQAHistory(driver, config, dataSet, visitFormName, franchiseId,
					corpUser.getuserFullName());

			defaultViewByFranchiseId(driver, franchiseId);
			fc.utobj().getElementByXpath(driver, ".//tr/td/a[.=" + "'" + franchiseId + "'"
					+ "]/../following-sibling::td[contains(text () , 'Scheduled')]").isEnabled();

			String visitID = fc.utobj().getText(driver, fc.utobj().getElementByXpath(driver,
					".//tr/td[contains(text () ," + "'" + visitFormName + "'" + ")]/preceding-sibling::td/a"));

			navigateToQAHistory(driver, franchiseId);

			// Enter VisitId
			fc.utobj().sendKeys(driver, pobj.visitNumber, visitID);

			fc.utobj().getElementByXpath(driver, ".//tr/td[contains(text () , " + "'" + visitFormName + "'"
					+ ")]/following-sibling::td[contains(text () , 'Scheduled')]").isEnabled();

			fc.utobj().printTestStep("Start Visit");

			// start Visit
			fc.utobj().clickElement(driver, fc.utobj().getElementByLinkText(driver, visitID));
			String text = fc.utobj().getElementByXpath(driver, ".//table[@class='bText12gr']//tr/td/label")
					.getAttribute("id").trim();
			text = text.replace("_0", "");

			fc.utobj().sendKeys(driver,
					fc.utobj().getElementByXpath(driver, ".//textarea[@id='Response_" + text + "']"),
					"It is a sample answer");
			fc.utobj().sendKeys(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='private" + text + "']"),
					"It is a private Answer");
			fc.utobj().clickElement(driver, pobj.nextBtnSubmit);

			fc.utobj().clickElement(driver, pobj.submitBtnAtVisit);
			fc.utobj().clickElement(driver, pobj.okBtn);

			fc.utobj().printTestStep("Verify Visit At Info Mgr QA History Tab");
			fc.utobj().getElementByXpath(driver, ".//tr/td[contains(text () , " + "'" + visitFormName + "'"
					+ ")]/following-sibling::td[contains(text () , 'In Process')]").isEnabled();

			fc.utobj().printTestStep("Navigate To FieldOps > Visits Page");
			fc.fieldops().fieldops_common().fieldopsVisit(driver);
			defaultViewByFranchiseId(driver, franchiseId);

			boolean isStatusPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//*[.='" + franchiseId + "']/ancestor::tr/td[contains(text () ,'In Process')]");
			if (isStatusPresent == false) {
				fc.utobj().throwsException("was not able to verify Status of Visit");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "fieldops")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify Modify , Cancel , Restore And Delete Option Of A Visit At Info Mgr (QA History). Info Mgr >  Franchisees > QA History", testCaseId = "TC_130_Verify_Modify_Visit_At_Info_Mgr")
	private void verifyModifyVisitAtInfoMgr() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("fieldops", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			FieldOpsVisitsPage pobj = new FieldOpsVisitsPage(driver);

			fc.utobj().printTestStep("Navigate To Admin > Hidden Links > Configure FieldOps Settings Page");
			fc.utobj().printTestStep("Configure Field Ops Settings Log Private Comment Yes");
			AdminHiddenLinksConfigureFieldOpsSettingsPageTest hiddenLinks = new AdminHiddenLinksConfigureFieldOpsSettingsPageTest();
			hiddenLinks.configureFieldOpsSettingsLogPrivateCommentYes(driver, config);

			fc.utobj().printTestStep("Navigate To Admin > Fieldops > Manage Visit Form");
			fc.utobj().printTestStep("Add Private Visit Form");
			AdminFieldOpsManageVisitFormPageTest visitFormPage = new AdminFieldOpsManageVisitFormPageTest();
			String visitFormName = visitFormPage.addVisitFormPrivate(driver, dataSet);

			fc.utobj().printTestStep("Add Question In Section");
			fc.utobj().clickElement(driver, pobj.addQuestion);
			AdminFieldOpsManageVisitFormCustomizeVisitFormPageTest customizeVisit = new AdminFieldOpsManageVisitFormCustomizeVisitFormPageTest();
			customizeVisit.addQuestion(driver, dataSet);
			fc.utobj().clickElement(driver, pobj.finishBtn);

			fc.utobj().printTestStep("Navigate To Admin > Franchise Location > Add Franchise Location");
			fc.utobj().printTestStep("Add Franchise Location");

			String regionName = fc.utobj().generateTestData("TestR");
			String storeType = fc.utobj().generateTestData("TestS");
			String franchiseId = fc.utobj().generateTestData("TestID");
			AdminFranchiseLocationAddFranchiseLocationPageTest franchise = new AdminFranchiseLocationAddFranchiseLocationPageTest();
			franchiseId = franchise.addFranchiseLocation_All(driver, franchiseId, regionName, storeType);

			fc.utobj().printTestStep("Navigate To Admin > Users > Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");

			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUser = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			String emailId = "fieldopsautomation@staffex.com";
			CorporateUser corpUser = new CorporateUser();
			corpUser.setConsultant("Y");
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporateUser.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			AdminFieldOpsQATabIntegrationPageTest QaTabPage = new AdminFieldOpsQATabIntegrationPageTest();
			QaTabPage.qaTabIntegrationYes(driver, config);

			fc.utobj().printTestStep("Navigate To Info Mgr > Franchisee");
			fc.utobj().printTestStep("Create Visit");
			createVisitAtInfoMgrQAHistory(driver, config, dataSet, visitFormName, franchiseId,
					corpUser.getuserFullName());
			defaultViewByFranchiseId(driver, franchiseId);

			fc.utobj().getElementByXpath(driver, ".//tr/td/a[.=" + "'" + franchiseId + "'"
					+ "]/../following-sibling::td[contains(text () , 'Scheduled')]").isEnabled();

			String visitID = fc.utobj().getText(driver, fc.utobj().getElementByXpath(driver,
					".//tr/td[contains(text () ," + "'" + visitFormName + "'" + ")]/preceding-sibling::td/a"));

			navigateToQAHistory(driver, franchiseId);

			// Enter VisitId
			fc.utobj().sendKeys(driver, pobj.visitNumber, visitID);

			fc.utobj().getElementByXpath(driver, ".//tr/td[contains(text () , " + "'" + visitFormName + "'"
					+ ")]/following-sibling::td[contains(text () , 'Scheduled')]").isEnabled();

			fc.utobj().printTestStep("Modify Visit");
			actionImgOptionInfoMgr(driver, visitFormName, "Modify");

			fc.utobj().getElementByXpath(driver, ".//*[@id='ms-parentmasterVisitId']/button/span[contains(text () ,"
					+ "'" + visitFormName + "'" + ")]").isEnabled();
			fc.utobj().getElementByXpath(driver, ".//tr/td[contains(text () , " + "'" + franchiseId + "'" + ")]")
					.isEnabled();

			fc.utobj().selectDropDown(driver, pobj.selectStatus, "Cancelled");
			fc.utobj().clickElement(driver, pobj.submitBtnM);

			fc.utobj().printTestStep("Navigate To FieldOps > Visits Page");
			fc.fieldops().fieldops_common().fieldopsVisit(driver);
			defaultViewByFranchiseId(driver, franchiseId);
			fc.utobj().getElementByXpath(driver, ".//tr/td/a[contains(text () ," + "'" + franchiseId + "'"
					+ ")]/..//following-sibling::td[contains(text () ,'Cancelled')]");
			navigateToQAHistory(driver, franchiseId);

			fc.utobj().printTestStep("Verify Modified Visit At Info Mgr QA History Tab");
			fc.utobj().getElementByXpath(driver, ".//tr/td[contains(text () , " + "'" + visitFormName + "'"
					+ ")]/following-sibling::td[contains(text () , 'Cancelled')]").isEnabled();
			actionImgOptionInfoMgr(driver, visitFormName, "Restore");
			fc.utobj().acceptAlertBox(driver);

			defaultViewByFranchiseId(driver, franchiseId);
			fc.utobj().getElementByXpath(driver, ".//tr/td/a[contains(text () ," + "'" + franchiseId + "'"
					+ ")]/..//following-sibling::td[contains(text () ,'Scheduled')]");
			navigateToQAHistory(driver, franchiseId);
			fc.utobj().getElementByXpath(driver, ".//tr/td[contains(text () , " + "'" + visitFormName + "'"
					+ ")]/following-sibling::td[contains(text () , 'Scheduled')]").isEnabled();

			fc.utobj().printTestStep("Delete Visit");
			actionImgOptionInfoMgr(driver, visitFormName, "Delete");
			fc.utobj().acceptAlertBox(driver);

			fc.utobj().printTestStep("Navigate To FieldOps > Visits Page");
			fc.utobj().printTestStep("Verify Deleted Visit");
			fc.fieldops().fieldops_common().fieldopsVisit(driver);
			defaultViewByFranchiseId(driver, franchiseId);
			fc.utobj().getElementByXpath(driver, ".//tr/td[contains(text () , 'No records found.')]");
			navigateToQAHistory(driver, franchiseId);
			fc.utobj().getElementByXpath(driver, ".//tr/td[contains(text () , 'No records found.')]");

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "fieldops")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Modify And Delete practice Visit", testCaseId = "TC_132_Modfiy_Practice_Visit")
	private void modifyPracticeVisit() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("fieldops", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			FieldOpsVisitsPage pobj = new FieldOpsVisitsPage(driver);

			fc.utobj().printTestStep("Navigate To Admin > Hidden Links > Configure FieldOps Settings Page");
			fc.utobj().printTestStep("Configure Field Ops Settings Log Private Comment Yes");
			AdminHiddenLinksConfigureFieldOpsSettingsPageTest hiddenLinks = new AdminHiddenLinksConfigureFieldOpsSettingsPageTest();
			hiddenLinks.configureFieldOpsSettingsLogPrivateCommentYes(driver, config);

			fc.utobj().printTestStep("Configure FieldOps Settings Franchise Self Audit Yes Owner Scoring Yes");
			hiddenLinks.configureFieldOpsSettingsFranchiseSelfAuditYesOwnerScoringYes(driver, config);

			String password = "t0n1ght123";

			fc.utobj().printTestStep("Navigate To Admin > Users > Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUser = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			String emailId = "fieldopsautomation@staffex.com";
			CorporateUser corpUser = new CorporateUser();
			corpUser.setConsultant("Y");
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporateUser.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			fc.utobj().printTestStep("Navigate To Admin > Franchise Location > Add Franchise Location");
			fc.utobj().printTestStep("Add Franchise Location");
			String regionName = fc.utobj().generateTestData("Test");
			String storeType = fc.utobj().generateTestData("Test");
			String franchiseId = fc.utobj().generateTestData("Test");
			AdminFranchiseLocationAddFranchiseLocationPageTest franchise = new AdminFranchiseLocationAddFranchiseLocationPageTest();
			franchiseId = franchise.addFranchiseLocation_All(driver, franchiseId, regionName, storeType);

			fc.utobj().printTestStep("Add Franchise User");
			AdminUsersManageManageFranchiseUsersPageTest franUser = new AdminUsersManageManageFranchiseUsersPageTest();
			String FranUserName = fc.utobj().generateTestData("TestF");
			String roleName = "Default Franchise Role";
			String franUserName = franUser.addFranchiseUser(driver, FranUserName, password, franchiseId, roleName,
					emailId);

			fc.utobj().printTestStep("Navigate To Admin > FieldOps > Manage Visit Form");
			fc.utobj().printTestStep("Add Non-Private Visit Form With Tab View");
			AdminFieldOpsManageVisitFormPageTest visitFormPage = new AdminFieldOpsManageVisitFormPageTest();
			String visitFormName = visitFormPage.addVisitFormTabViewPrivateNo(driver, config, dataSet);

			fc.utobj().printTestStep("Add Tab In Visit Form");
			AdminFieldOpsManageVisitFormCustomizeVisitFormPageTest customizeVisit = new AdminFieldOpsManageVisitFormCustomizeVisitFormPageTest();
			customizeVisit.addTab(driver, config, dataSet);

			fc.utobj().printTestStep("Add Question In Tab");
			fc.utobj().clickElement(driver, pobj.addQuestion);
			customizeVisit.addQuestion(driver, dataSet);
			fc.utobj().clickElement(driver, pobj.finishBtn);

			fc.home_page().logout(driver);

			fc.utobj().printTestStep("Login With Franchise User");
			fc.loginpage().loginWithParameter(driver, franUserName, password);

			fc.utobj().printTestStep("Navigate To Fieldops > Visits Page");
			fc.fieldops().fieldops_common().fieldopsVisitCreateVisitPage(driver);

			fc.utobj().printTestStep("Create Practice Visit");
			createVisitAtFranchiseVisitPage(driver, config, dataSet, visitFormName, franchiseId,
					corpUser.getuserFullName());

			String visitID = fc.utobj().getText(driver, fc.utobj().getElementByXpath(driver,
					".//tr/td[contains(text () ," + "'" + visitFormName + "'" + ")]/preceding-sibling::td/a"));

			fc.utobj().printTestStep("Modify Practice Visit");
			actionImgOptionAtFranchise(driver, visitFormName, "Modify");
			fc.utobj().getElementByXpath(driver, ".//*[@id='ms-parentmasterVisitId']/button/span[contains(text () , "
					+ "'" + visitFormName + "'" + ")]").isEnabled();
			fc.utobj().getElementByXpath(driver, ".//*[@id='ms-parentfranchiseeNo']/button/span[contains(text () , "
					+ "'" + franchiseId + "'" + ")]").isEnabled();
			fc.utobj().selectDropDown(driver, pobj.selectStatus, "Cancelled");
			pobj.includeVisitPractise.isEnabled();
			fc.utobj().clickElement(driver, pobj.submitBtnM);

			fc.utobj().printTestStep("Verify Modify Visit");
			// DefaultView
			fc.utobj().sendKeys(driver, pobj.visitNumber, visitID);

			fc.utobj().getElementByXpath(driver, ".//tr/td[contains(text () , " + "'" + visitFormName + "'"
					+ ")]/following-sibling::td[contains(text () , 'Cancelled')]").isEnabled();
			fc.utobj()
					.getElementByXpath(driver,
							".//tr/td[contains(text () , 'Cancelled')]/preceding-sibling::td[contains(text () ," + "'"
									+ visitID + "'" + ")]")
					.isEnabled();

			fc.utobj().printTestStep("Restore Visit");
			actionImgOptionAtFranchise(driver, visitFormName, "Restore");
			fc.utobj().acceptAlertBox(driver);

			fc.utobj().printTestStep("Verify Restore Visit");
			// DefaultView
			fc.utobj().sendKeys(driver, pobj.visitNumber, visitID);
			fc.utobj().getElementByXpath(driver, ".//tr/td/a[.=" + "'" + visitID + "'" + "]").isEnabled();
			fc.utobj().getElementByXpath(driver, ".//tr/td[contains(text () , " + "'" + visitFormName + "'"
					+ ")]/following-sibling::td[contains(text () , 'Scheduled')]").isEnabled();

			fc.utobj().printTestStep("Delete Practice Visit");
			actionImgOptionAtFranchise(driver, visitFormName, "Delete");
			fc.utobj().acceptAlertBox(driver);
			fc.utobj().acceptAlertBox(driver);

			fc.utobj().printTestStep("Verify Delete Practice Visit");
			// DefaultView
			fc.utobj().sendKeys(driver, pobj.visitNumber, visitID);
			fc.utobj().getElementByXpath(driver, ".//tr/td[contains(text () , 'No records found.')]");

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = {"fieldops" ,"fieldops0808"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify Owner Drop Down Value At Info Mgr > Franchise > QA History", testCaseId = "TC_135_Verify_Owner_DropDown_At_Info_Mgr")
	private void verifyOwnerDropDownAtInfoMgr() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("fieldops", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			FieldOpsVisitsPage pobj = new FieldOpsVisitsPage(driver);
			String password = "ton1ght123";

			fc.utobj().printTestStep("Navigate To Admin > Hidden Links > Configure FieldOps Settings Page");
			fc.utobj().printTestStep("Configure Field Ops Settings Log Private Comment Yes");
			AdminHiddenLinksConfigureFieldOpsSettingsPageTest hiddenLinks = new AdminHiddenLinksConfigureFieldOpsSettingsPageTest();
			hiddenLinks.configureFieldOpsSettingsLogPrivateCommentYes(driver, config);

			fc.utobj().printTestStep("Add Non-Private Visit Form");
			AdminFieldOpsManageVisitFormPageTest visitFormPage = new AdminFieldOpsManageVisitFormPageTest();
			String visitFormName = visitFormPage.addVisitFormPrivateNo(driver, config, dataSet);

			fc.utobj().printTestStep("Add Question In Section");
			fc.utobj().clickElement(driver, pobj.addQuestion);

			AdminFieldOpsManageVisitFormCustomizeVisitFormPageTest customizeVisit = new AdminFieldOpsManageVisitFormCustomizeVisitFormPageTest();
			customizeVisit.addQuestion(driver, dataSet);

			fc.utobj().clickElement(driver, pobj.finishBtn);

			fc.utobj().printTestStep("Add Franchise Location");
			String regionName = fc.utobj().generateTestData("TestR");
			String storeType = fc.utobj().generateTestData("TestS");
			String franchiseId = fc.utobj().generateTestData("TestID");
			AdminFranchiseLocationAddFranchiseLocationPageTest franchise = new AdminFranchiseLocationAddFranchiseLocationPageTest();
			String firstName = fc.utobj().generateTestData("firstName");
			franchiseId = franchise.addFranchiseLocation_AllWithFName(driver, franchiseId, regionName, storeType,
					firstName);

			// Add franchiseUser
			fc.utobj().printTestStep("Add Franchise User");
			AdminUsersManageManageFranchiseUsersPageTest franUser = new AdminUsersManageManageFranchiseUsersPageTest();
			String FranUserName = fc.utobj().generateTestData("TestF");
			String roleName = "Default Franchise Role";
			String emailId = "fieldopsautomation@staffex.com";
			franUser.addFranchiseUser(driver, FranUserName, password, franchiseId, roleName, emailId);

			fc.utobj().printTestStep("Navigate To Admin > Users > Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUser = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser = new CorporateUser();
			corpUser.setConsultant("Y");
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporateUser.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			AdminFieldOpsQATabIntegrationPageTest QaTabPage = new AdminFieldOpsQATabIntegrationPageTest();
			QaTabPage.qaTabIntegrationYes(driver, config);

			// Create Visit

			fc.utobj().printTestStep("Navigate To Info Mgr > Franchisee > QA HIstory Tab");
			fc.infomgr().infomgr_common().fimModule(driver);

			// QA History
			fc.utobj().clickElement(driver, pobj.franchisees);
			fc.utobj().sendKeys(driver, pobj.searchFranchisee, franchiseId);
			fc.utobj().clickElement(driver, pobj.searchBoxBtn);
			fc.utobj().clickLink(driver, franchiseId);
			fc.utobj().clickElement(driver, pobj.qaHistory);
			
			fc.utobj().printTestStep("Create Visit");
			// Click create
			fc.utobj().clickElement(driver, pobj.createVisit);

			// select visit Form
			fc.utobj().clickElement(driver, pobj.selectClick);
			fc.utobj().sendKeys(driver, pobj.searchBox, visitFormName);
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver,
							".//*[@id='ms-parentmasterVisitId']/div/ul/li/label[contains(text () ," + "'"
									+ visitFormName + "'" + ")]/input"));

			// Select Consultant
			fc.utobj().clickElement(driver, pobj.selectConsultant);
			fc.utobj().sendKeys(driver, pobj.consultantSearchBox, corpUser.getuserFullName());
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver,
							".//*[@id='ms-parentassignTo']/div/ul/li/label[contains(text () , " + "'"
									+ corpUser.getuserFullName() + "'" + ")]/input"));

			// schedule Date
			String curDate = fc.utobj().getCurrentDateUSFormat();
			fc.utobj().sendKeys(driver, pobj.scheduleDateTime, curDate);
			String comments = dataSet.get("comments");
			fc.utobj().sendKeys(driver, pobj.comments, comments);

			if (dataSet.get("franchiseLogin").equalsIgnoreCase("Yes")) {
				fc.utobj().clickElement(driver, pobj.visitCompletionEmailFranchiseeOwner);
			}

			
			fc.utobj().printTestStep("Verify Owner DropDown");

			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//*[@id='ms-parentOwnersComboMail']/button"));
			fc.utobj().sendKeys(driver,
					fc.utobj().getElementByXpath(driver, ".//*[@id='ms-parentOwnersComboMail']/div/div/input"),
					firstName);

			if (fc.utobj().isSelected(driver,fc.utobj().getElementByXpath(driver, ".//*[@id='selectAll']"))) {
			} else {
				fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='selectAll']"));
			}

			fc.utobj().clickElement(driver, pobj.schedule);

			defaultViewByFranchiseId(driver, franchiseId);
			fc.utobj().getElementByXpath(driver, ".//tr/td/a[contains(text () , " + "'" + franchiseId + "'"
					+ ")]/../following-sibling::td[contains(text () , 'Scheduled')]").isEnabled();

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "fieldops", "fieldopsemail" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify Email After Visit Status is Completed For Consultant And Owner", testCaseId = "TC_136_Verify_Mail_After_Visit_Status_Completed")
	private void verifyMailAfterVisitStatusCompleted() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("fieldops", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			FieldOpsVisitsPage pobj = new FieldOpsVisitsPage(driver);

			fc.utobj().printTestStep("Navigate To Admin > Hidden Links > Configure FieldOps Settings Page");
			fc.utobj().printTestStep("Configure Field Ops Settings Log Private Comment Yes");
			AdminHiddenLinksConfigureFieldOpsSettingsPageTest hiddenLinks = new AdminHiddenLinksConfigureFieldOpsSettingsPageTest();
			hiddenLinks.configureFieldOpsSettingsLogPrivateCommentYes(driver, config);

			fc.utobj().printTestStep("Navigate To Admin > Fieldops > Manage Visit Form");
			fc.utobj().printTestStep("Add Private Visit Form");
			AdminFieldOpsManageVisitFormPageTest visitFormPage = new AdminFieldOpsManageVisitFormPageTest();
			String visitFormName = visitFormPage.addVisitFormPrivateNo(driver, config, dataSet);

			fc.utobj().printTestStep("Add Question In Section");
			fc.utobj().clickElement(driver, pobj.addQuestion);

			AdminFieldOpsManageVisitFormCustomizeVisitFormPageTest customizeVisit = new AdminFieldOpsManageVisitFormCustomizeVisitFormPageTest();
			customizeVisit.addQuestion(driver, dataSet);
			fc.utobj().clickElement(driver, pobj.finishBtn);

			fc.utobj().printTestStep("Navigate To Admin > Franchise Location > Add Franchise Location");
			fc.utobj().printTestStep("Add Franchise Location");
			String regionName = fc.utobj().generateTestData("Test");
			String storeType = fc.utobj().generateTestData("Test");
			String franchiseId = fc.utobj().generateTestData("Test");
			AdminFranchiseLocationAddFranchiseLocationPageTest franchise = new AdminFranchiseLocationAddFranchiseLocationPageTest();
			String firstName = fc.utobj().generateTestData("firstName");
			franchiseId = franchise.addFranchiseLocation_AllWithFName(driver, franchiseId, regionName, storeType,
					firstName);

			fc.utobj().printTestStep("Add Franchise User");
			AdminUsersManageManageFranchiseUsersPageTest addFranUser = new AdminUsersManageManageFranchiseUsersPageTest();
			String franUserName = fc.utobj().generateTestData("Test");
			String roleName = "Default Franchise Role";
			String emailId = "fieldopsautomation@staffex.com";
			String password1 = "t0n1ght@123";
			addFranUser.addFranchiseUser(driver, franUserName, password1, franchiseId, roleName, emailId);

			fc.utobj().printTestStep("Navigate To Admin > Users > Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUser = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			/* String emailId="fieldopsautomation@staffex.com"; */
			CorporateUser corpUser = new CorporateUser();
			corpUser.setConsultant("Y");
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporateUser.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			fc.utobj().printTestStep("Navigate To Admin > FieldOps > Configure Schedule Visit Email Content Page");
			fc.utobj().printTestStep("Schedule Visit Email Content Yes");
			String subject = "Field Ops Visit Schedule Alert";
			AdminFieldOpsConfigureScheduleVisitEmailContentPageTest visitEmailConfigurePage = new AdminFieldOpsConfigureScheduleVisitEmailContentPageTest();
			visitEmailConfigurePage.scheduleVisitEmailContentYes(driver, subject, config);

			fc.utobj().printTestStep("Navigate To FieldOps > Visits Page");
			fc.utobj().printTestStep("Create Visit");

			// navigate to createVisitFormPage

			fc.utobj().printTestStep("Navigate To FieldOps > Visits Page");
			fc.fieldops().fieldops_common().fieldopsVisitCreateVisitPage(driver);
			fc.utobj().printTestStep("Create Visit");

			// Click createVisit
			fc.utobj().clickElement(driver, pobj.createVisit);

			// select visit Form
			fc.utobj().clickElement(driver, pobj.selectClick);
			fc.utobj().sendKeys(driver, pobj.searchBox, visitFormName);
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver,
							".//*[@id='ms-parentmasterVisitId']/div/ul/li/label[contains(text () ," + "'"
									+ visitFormName + "'" + ")]/input"));

			// select FranchiseID
			fc.utobj().clickElement(driver, pobj.franchiseeSelect);
			fc.utobj().sendKeys(driver, pobj.searchBoxFranchisee, franchiseId);
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver,
							".//*[@id='ms-parentfranchiseeNo']/div/ul/li/label[contains(text () , " + "'" + franchiseId
									+ "'" + ")]/input"));
			fc.utobj().clickElement(driver, pobj.franchiseeSelect);

			// Select Consultant
			fc.utobj().clickElement(driver, pobj.selectConsultant);
			fc.utobj().sendKeys(driver, pobj.consultantSearchBox, corpUser.getuserFullName());
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver,
							".//*[@id='ms-parentassignTo']/div/ul/li/label[contains(text () , " + "'"
									+ corpUser.getuserFullName() + "'" + ")]/input"));

			// schedule Date
			fc.utobj().sendKeys(driver, pobj.scheduleDateTime, fc.utobj().getCurrentDateUSFormat());
			String comments = dataSet.get("comments");
			fc.utobj().sendKeys(driver, pobj.comments, comments);

			if (fc.utobj().isSelected(driver,pobj.sendMailCheckNotiFranchiseeOwner)) {
			} else {
				fc.utobj().clickElement(driver, pobj.sendMailCheckNotiFranchiseeOwner);
			}

			if (fc.utobj().isSelected(driver,pobj.visitCompletionEmailFranchiseeOwner)) {
			} else {
				fc.utobj().clickElement(driver, pobj.visitCompletionEmailFranchiseeOwner);
			}
			fc.utobj().setToDefault(driver, pobj.ownerSelectBtn);

			WebElement elementInput = fc.utobj().getElement(driver, pobj.ownerSelectBtn)
					.findElement(By.xpath("./div/div/input"));
			fc.utobj().selectValFromMultiSelect(driver, pobj.ownerSelectBtn, elementInput, firstName);

			fc.utobj().clickElement(driver, pobj.schedule);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			AdminFieldOpsConfigureScheduleVisitEmailContentPage pobj1 = new AdminFieldOpsConfigureScheduleVisitEmailContentPage(
					driver);

			fc.utobj().setToDefault(driver, pobj.ownerSelect);
			WebElement elementInput1 = fc.utobj().getElement(driver, pobj.ownerSelect)
					.findElement(By.xpath("./div/div/input"));
			fc.utobj().selectValFromMultiSelect(driver, pobj.ownerSelect, elementInput1, firstName);

			String subject1 = "Field Ops Visit Schedule Alert";
			fc.utobj().sendKeys(driver, pobj1.subject, subject1);

			if (fc.utobj().isSelected(driver,fc.utobj().getElementByID(driver, "attachAuditForm"))) {
			} else {
				fc.utobj().clickElement(driver, fc.utobj().getElementByID(driver, "attachAuditForm"));
			}

			fc.utobj().clickElement(driver, pobj.sendBtn);
			fc.utobj().switchFrameToDefault(driver);
			defaultViewByFranchiseId(driver, franchiseId);

			fc.utobj().printTestStep("Start Visit");
			actionImgOption(driver, franchiseId, "Start Visit");

			// question
			fc.utobj().sendKeys(driver,
					fc.utobj().getElementByXpath(driver,
							".//div[@id='formDiv']//table[@class='bText12gr']/../table[1]//tr[2]/td/textarea"),
					"It is comment box");
			fc.utobj().sendKeys(driver,
					fc.utobj().getElementByXpath(driver,
							".//div[@id='formDiv']//table[@class='bText12gr']/../table[1]//tr[3]/td/textarea"),
					"It is a private comment box");

			fc.utobj().clickElement(driver, pobj.submitAllBtn);
			fc.utobj().clickElement(driver, pobj.submitBtnAtVisit);
			fc.utobj().clickElement(driver, pobj.okBtn);

			fc.utobj().printTestStep("Verify Mail For Consultant After Visit Completed");

			String expectedSubject = "Field Ops Visit Submission Alert";
			String expectedMessageBody = visitFormName;
			Map<String, String> mailData = new HashMap<String, String>();

			mailData = fc.utobj().readMailBox(expectedSubject, expectedMessageBody, emailId, "sdg@1a@Hfs");

			if (mailData.size() == 0) {
				mailData = fc.utobj().readMailBox(expectedSubject, expectedMessageBody, emailId, "sdg@1a@Hfs");
			}

			if (mailData.size() == 0 || !mailData.get("mailBody").contains(franchiseId)) {
				fc.utobj().throwsSkipException("was not able to verify franchise id in Consultant email");
			}

			if (mailData.size() == 0 || !mailData.get("mailBody").contains(corpUser.getuserFullName())) {
				fc.utobj().throwsSkipException("was not able to verify Consultant id in Consultant email");
			}

			if (mailData.size() == 0 || !mailData.get("mailBody").contains(visitFormName)) {
				fc.utobj().throwsSkipException("was not able to verify Visit Form in Consultant email");
			}

			Map<String, String> mailData1 = new HashMap<String, String>();
			fc.utobj().printTestStep("Verify Visit Submission mail To Owner");

			mailData1 = fc.utobj().readMailBox(expectedSubject, firstName, emailId, "sdg@1a@Hfs");

			if (mailData1.size() == 0) {
				mailData1 = fc.utobj().readMailBox(expectedSubject, firstName, emailId, "sdg@1a@Hfs");
			}

			if (mailData1.size() == 0 || !mailData1.get("mailBody").contains(firstName)) {
				fc.utobj().throwsSkipException("was not able to verify Visit Submission mail Sent To Owner");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "fieldops", "fieldopsemail" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify Email After Visit get Modify", testCaseId = "TC_137_Verify_Mail_After_Visit_Status_Modify")
	private void verifyMailAfterVisitStatusModify() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("fieldops", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			FieldOpsVisitsPage pobj = new FieldOpsVisitsPage(driver);

			fc.utobj().printTestStep("Navigate To Admin > Hidden Links > Configure FieldOps Settings Page");
			fc.utobj().printTestStep("Configure Field Ops Settings Log Private Comment Yes");
			AdminHiddenLinksConfigureFieldOpsSettingsPageTest hiddenLinks = new AdminHiddenLinksConfigureFieldOpsSettingsPageTest();
			hiddenLinks.configureFieldOpsSettingsLogPrivateCommentYes(driver, config);

			fc.utobj().printTestStep("Navigate To Admin > Fieldops > Manage Visit Form");
			fc.utobj().printTestStep("Add Private Visit Form");
			AdminFieldOpsManageVisitFormPageTest visitFormPage = new AdminFieldOpsManageVisitFormPageTest();
			String visitFormName = visitFormPage.addVisitFormPrivate(driver, dataSet);

			fc.utobj().printTestStep("Add Question In Section");
			fc.utobj().clickElement(driver, pobj.addQuestion);

			AdminFieldOpsManageVisitFormCustomizeVisitFormPageTest customizeVisit = new AdminFieldOpsManageVisitFormCustomizeVisitFormPageTest();
			customizeVisit.addQuestion(driver, dataSet);
			fc.utobj().clickElement(driver, pobj.finishBtn);

			fc.utobj().printTestStep("Navigate To Admin > Franchise Location > Add Franchise Location");
			fc.utobj().printTestStep("Add Franchise Location");
			String regionName = fc.utobj().generateTestData("Test");
			String storeType = fc.utobj().generateTestData("Test");
			String franchiseId = fc.utobj().generateTestData("Test");
			AdminFranchiseLocationAddFranchiseLocationPageTest franchise = new AdminFranchiseLocationAddFranchiseLocationPageTest();
			franchiseId = franchise.addFranchiseLocation_All(driver, franchiseId, regionName, storeType);

			fc.utobj().printTestStep("Navigate To Admin > Users > Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUser = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			String emailId = "fieldopsautomation@staffex.com";
			CorporateUser corpUser = new CorporateUser();
			corpUser.setConsultant("Y");
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporateUser.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			fc.utobj().printTestStep("Navigate To Admin > FieldOps > Configure Schedule Visit Email Content Page");
			fc.utobj().printTestStep("Schedule Visit Email Content Yes");

			// Configure Visit Email Content
			String subject = "Field Ops Visit Schedule Alert";
			AdminFieldOpsConfigureScheduleVisitEmailContentPageTest visitEmailConfigurePage = new AdminFieldOpsConfigureScheduleVisitEmailContentPageTest();
			visitEmailConfigurePage.scheduleVisitEmailContentYes(driver, subject, config);

			fc.utobj().printTestStep("Navigate To FieldOps > Visits Page");
			fc.utobj().printTestStep("Create Visit");
			// create Visit
			String curDate = createVisitAtVisitPage(driver, config, dataSet, visitFormName, franchiseId,
					corpUser.getuserFullName());

			fc.utobj().printTestStep("Modify Visit");
			actionImgOption(driver, franchiseId, "Modify");

			String modifyCom = fc.utobj().generateTestData("It is a Sample Visit Updated");

			fc.utobj().sendKeys(driver, pobj.mNotes, modifyCom);
			fc.utobj().clickElement(driver, pobj.submitBtnM);

			fc.utobj().printTestStep("Verify Email After Visit Get Modified");

			String expectedSubject = subject + " " + "(Modified)";
			;
			String expectedMessageBody = modifyCom;
			Map<String, String> mailData = new HashMap<String, String>();

			mailData = fc.utobj().readMailBox(expectedSubject, expectedMessageBody, emailId, "sdg@1a@Hfs");

			if (mailData.size() == 0) {
				mailData = fc.utobj().readMailBox(expectedSubject, expectedMessageBody, emailId, "sdg@1a@Hfs");
			}

			if (mailData.size() == 0 || !mailData.get("mailBody").contains(franchiseId)) {
				fc.utobj().throwsSkipException("was not able to verify franchise id In Consultant email");
			}

			if (mailData.size() == 0 || !mailData.get("mailBody").contains(modifyCom)) {
				fc.utobj().throwsSkipException("was not able to verify Comment In Consultant email");
			}

			if (mailData.size() == 0 || !mailData.get("mailBody").contains(corpUser.getuserFullName())) {
				fc.utobj().throwsSkipException("was not able to verify User Name In Consultant email");
			}

			if (mailData.size() == 0 || !mailData.get("mailBody").contains(visitFormName)) {
				fc.utobj().throwsSkipException("was not able to verify Visit Form In Consultant email");
			}

			if (mailData.size() == 0 || !mailData.get("mailBody").contains(curDate)) {
				fc.utobj().throwsSkipException("was not able to verify Date In Consultant email");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "fieldops", "fieldopsemail" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify Email After Visit get Canceled", testCaseId = "TC_138_Verify_Mail_After_Visit_Cancelled")
	private void verifyMailAfterVisitCancelled() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("fieldops", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			FieldOpsVisitsPage pobj = new FieldOpsVisitsPage(driver);

			fc.utobj().printTestStep("Navigate To Admin > Hidden Links > Configure FieldOps Settings Page");
			fc.utobj().printTestStep("Configure Field Ops Settings Log Private Comment Yes");
			AdminHiddenLinksConfigureFieldOpsSettingsPageTest hiddenLinks = new AdminHiddenLinksConfigureFieldOpsSettingsPageTest();
			hiddenLinks.configureFieldOpsSettingsLogPrivateCommentYes(driver, config);

			fc.utobj().printTestStep("Navigate To Admin > Fieldops > Manage Visit Form");
			fc.utobj().printTestStep("Add Private Visit Form");
			AdminFieldOpsManageVisitFormPageTest visitFormPage = new AdminFieldOpsManageVisitFormPageTest();
			String visitFormName = visitFormPage.addVisitFormPrivate(driver, dataSet);

			fc.utobj().printTestStep("Add Question In Section");
			fc.utobj().clickElement(driver, pobj.addQuestion);

			AdminFieldOpsManageVisitFormCustomizeVisitFormPageTest customizeVisit = new AdminFieldOpsManageVisitFormCustomizeVisitFormPageTest();
			customizeVisit.addQuestion(driver, dataSet);
			fc.utobj().clickElement(driver, pobj.finishBtn);

			fc.utobj().printTestStep("Navigate To Admin > Franchise Location > Add Franchise Location");
			fc.utobj().printTestStep("Add Franchise Location");
			String regionName = fc.utobj().generateTestData("Test");
			String storeType = fc.utobj().generateTestData("Test");
			String franchiseId = fc.utobj().generateTestData("Test");
			AdminFranchiseLocationAddFranchiseLocationPageTest franchise = new AdminFranchiseLocationAddFranchiseLocationPageTest();
			franchiseId = franchise.addFranchiseLocation_All(driver, franchiseId, regionName, storeType);

			fc.utobj().printTestStep("Navigate To Admin > Users > Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUser = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			String emailId = "fieldopsautomation@staffex.com";
			CorporateUser corpUser = new CorporateUser();
			corpUser.setConsultant("Y");
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporateUser.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			fc.utobj().printTestStep("Navigate To Admin > FieldOps > Configure Schedule Visit Email Content Page");
			fc.utobj().printTestStep("Schedule Visit Email Content Yes");
			String subject = "Field Ops Visit Schedule Alert";
			AdminFieldOpsConfigureScheduleVisitEmailContentPageTest visitEmailConfigurePage = new AdminFieldOpsConfigureScheduleVisitEmailContentPageTest();
			visitEmailConfigurePage.scheduleVisitEmailContentYes(driver, subject, config);

			fc.utobj().printTestStep("Navigate To FieldOps > Visits Page");
			fc.utobj().printTestStep("Create Visit");
			// create Visit
			createVisitAtVisitPage(driver, config, dataSet, visitFormName, franchiseId, corpUser.getuserFullName());

			fc.utobj().printTestStep("Cancelled Visit");
			actionImgOption(driver, franchiseId, "Modify");

			String modifyCom = fc.utobj().generateTestData("It is a Sample Visit Updated");
			fc.utobj().selectDropDown(driver, pobj.selectStatus, "Cancelled");
			fc.utobj().sendKeys(driver, pobj.mNotes, modifyCom);
			fc.utobj().clickElement(driver, pobj.submitBtnM);

			defaultViewByFranchiseId(driver, franchiseId);
			String visitID = fc.utobj().getText(driver,
					fc.utobj().getElementByXpath(driver, ".//a[.='" + franchiseId + "']/../preceding-sibling::td[2]"));

			fc.utobj().printTestStep("Verify Mail After Visit Cancelled");
			// Verify Mail For Consultant

			String expectedSubject = subject + " " + "(Cancelled)";
			visitID = visitID.replace(" **", "");
			String expectedMessageBody = visitID;
			Map<String, String> mailData = new HashMap<String, String>();

			mailData = fc.utobj().readMailBox(expectedSubject, expectedMessageBody, emailId, "sdg@1a@Hfs");

			if (mailData.size() == 0) {

				mailData = fc.utobj().readMailBox(expectedSubject, expectedMessageBody, emailId, "sdg@1a@Hfs");

			}

			if (mailData.size() == 0 || !mailData.get("mailBody").contains(corpUser.getuserFullName())) {
				fc.utobj().throwsException("was not able to verify user Name");
			}

			if (mailData.size() == 0 || !mailData.get("mailBody").contains(visitID)) {
				fc.utobj().throwsException("was not able to verify Visit Id");
			}

			if (mailData.size() == 0 || !mailData.get("mailBody").contains("Following visit has been Cancelled.")) {
				fc.utobj().throwsException("was not able to verify Mail After visit got Cancelled");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "fieldops", "fieldopsemail" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify mail if visit status is non Compliance", testCaseId = "TC_139_Verify_Mail_After_Visit_Status_Non_Compliance")
	private void verifyMailAfterVisitNonComplience() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("fieldops", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			FieldOpsVisitsPage pobj = new FieldOpsVisitsPage(driver);

			fc.utobj().printTestStep("Navigate To Admin > Hidden Links > Configure FieldOps Settings Page");
			fc.utobj().printTestStep("Configure Field Ops Settings Log Private Comment Yes");
			AdminHiddenLinksConfigureFieldOpsSettingsPageTest hiddenLinks = new AdminHiddenLinksConfigureFieldOpsSettingsPageTest();
			hiddenLinks.configureFieldOpsSettingsLogPrivateCommentYes(driver, config);

			fc.utobj().printTestStep("Navigate To Admin > Fieldops > Manage Visit Form");
			fc.utobj().printTestStep("Add Private Visit Form");
			AdminFieldOpsManageVisitFormPageTest visitFormPage = new AdminFieldOpsManageVisitFormPageTest();
			String visitFormName = visitFormPage.addVisitFormPrivate(driver, dataSet);

			fc.utobj().printTestStep("Add Question In Section");
			fc.utobj().clickElement(driver, pobj.addQuestion);

			AdminFieldOpsManageVisitFormCustomizeVisitFormPageTest customizeVisit = new AdminFieldOpsManageVisitFormCustomizeVisitFormPageTest();
			customizeVisit.addQuestion(driver, dataSet);

			fc.utobj().clickElement(driver, pobj.finishBtn);

			fc.utobj().printTestStep("Navigate To Admin > Franchise Location > Add Franchise Location");
			fc.utobj().printTestStep("Add Franchise Location");
			String regionName = fc.utobj().generateTestData("Test");
			String storeType = fc.utobj().generateTestData("Test");
			String franchiseId = fc.utobj().generateTestData("Test");
			AdminFranchiseLocationAddFranchiseLocationPageTest franchise = new AdminFranchiseLocationAddFranchiseLocationPageTest();
			franchiseId = franchise.addFranchiseLocation_All(driver, franchiseId, regionName, storeType);

			fc.utobj().printTestStep("Navigate To Admin > Users > Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUser = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			String emailId = "fieldopsautomation@staffex.com";
			CorporateUser corpUser = new CorporateUser();
			corpUser.setConsultant("Y");
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporateUser.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			fc.utobj().printTestStep("Navigate To FieldOps > Visits Page");
			fc.utobj().printTestStep("Create Visit");
			// create Visit
			createVisitAtVisitPage(driver, config, dataSet, visitFormName, franchiseId, corpUser.getuserFullName());

			fc.utobj().printTestStep("Add Task");

			String visitId = fc.utobj()
					.getElementByXpath(driver,
							".//tr[2]/td[contains(text () ," + "'" + visitFormName + "'" + ")]/../td[1]/a")
					.getText().trim();

			fc.utobj().printTestStep("Navigate To Admin > FieldOps > Action Library");
			fc.utobj().printTestStep("Add Action Item");
			AdminFieldOpsActionLibraryPageTest actionPage = new AdminFieldOpsActionLibraryPageTest();
			String subject = actionPage.addActionItem(driver, dataSet);

			// Add Task
			FieldOpsTasksPageTest tasksPage = new FieldOpsTasksPageTest();
			tasksPage.addTaskIntoVisit(driver, config, dataSet, franchiseId, subject, corpUser.getuserFullName(),
					visitId);

			fc.utobj().printTestStep("Start Visit");
			// Start Visit
			startVisitWithCompleteStatus(driver, config, dataSet, franchiseId);

			// Verification
			defaultViewByFranchiseId(driver, franchiseId);
			String visitID = fc.utobj().getText(driver,
					fc.utobj().getElementByXpath(driver, ".//a[.='" + franchiseId + "']/../preceding-sibling::td[2]"));
			String totalTask = fc.utobj().getText(driver, fc.utobj().getElementByXpath(driver,
					".//a[.='" + franchiseId + "']/ancestor::tr/td/a[contains(@href , 'visitTask')]"));

			totalTask = "Total Task : " + totalTask;

			fc.utobj().printTestStep("Verify Mail After Visit Is Non-Coplience");

			String expectedSubject = "Field Ops Visit Submission Alert";
			String expectedMessageBody = franchiseId;
			visitID = visitID.replace(" **", "");
			Map<String, String> mailData = new HashMap<String, String>();

			mailData = fc.utobj().readMailBox(expectedSubject, expectedMessageBody, emailId, "sdg@1a@Hfs");

			if (mailData.size() == 0) {
				mailData = fc.utobj().readMailBox(expectedSubject, expectedMessageBody, emailId, "sdg@1a@Hfs");
			}

			if (mailData.size() == 0 || !mailData.get("mailBody").contains(visitID)) {
				fc.utobj().throwsException("was not able to verify the Visit Id In Email");
			}

			if (mailData.size() == 0 || !mailData.get("mailBody").contains(totalTask)) {
				fc.utobj().throwsException("was not able to verify Task In Email");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "fieldops")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify Privileges for franchise user", testCaseId = "TC_140_Verify_Privileges_Franchise_User")
	public void verifyPrivilegesFranchiseUser() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());
		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("fieldops", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			FieldOpsVisitsPage pobj = new FieldOpsVisitsPage(driver);

			fc.utobj().printTestStep("Navigate To Admin > Hidden Links > Configure FieldOps Settings Page");
			fc.utobj().printTestStep("Configure Field Ops Settings Log Private Comment Yes");
			AdminHiddenLinksConfigureFieldOpsSettingsPageTest hiddenLinks = new AdminHiddenLinksConfigureFieldOpsSettingsPageTest();
			hiddenLinks.configureFieldOpsSettingsLogPrivateCommentYes(driver, config);

			fc.utobj().printTestStep("Configure FieldOps Settings Franchise Self Audit Yes Owner Scoring Yes");
			hiddenLinks.configureFieldOpsSettingsFranchiseSelfAuditYesOwnerScoringYes(driver, config);

			String password = "t0n1ght123";

			fc.utobj().printTestStep("Navigate To Admin > Users > Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUser = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			String emailId = "fieldopsautomation@staffex.com";
			CorporateUser corpUser = new CorporateUser();
			corpUser.setConsultant("Y");
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporateUser.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			fc.utobj().printTestStep("Navigate To Admin > Franchise Location > Add Franchise Location");
			fc.utobj().printTestStep("Add Franchise Location");
			String regionName = fc.utobj().generateTestData("Test");
			String storeType = fc.utobj().generateTestData("Test");
			String franchiseId = fc.utobj().generateTestData("Test");
			AdminFranchiseLocationAddFranchiseLocationPageTest franchise = new AdminFranchiseLocationAddFranchiseLocationPageTest();
			franchiseId = franchise.addFranchiseLocation_All(driver, franchiseId, regionName, storeType);

			fc.utobj().printTestStep("Add Franchise User With Not Access To Delete Visit");

			// Add A franchiseUser
			AdminUsersManageManageFranchiseUsersPageTest franUser = new AdminUsersManageManageFranchiseUsersPageTest();
			String FranUserName = fc.utobj().generateTestData("TestF");

			AdminUsersRolesAddNewRolePageTest addRolePage = new AdminUsersRolesAddNewRolePageTest();
			String roleName = fc.utobj().generateTestData("FieldOps Privileges");
			addRolePage.addFranchiselRoleWithFieldpsAccess(driver, config, roleName);
			fc.utobj().printTestStep("Add Franchise User");

			String franUserName = franUser.addFranchiseUser(driver, FranUserName, password, franchiseId, roleName,
					emailId);

			fc.utobj().printTestStep("Navigate To Admin > FieldOps > Manage Visit Form");
			fc.utobj().printTestStep("Add Non-Private Visit Form With Tab View");
			AdminFieldOpsManageVisitFormPageTest visitFormPage = new AdminFieldOpsManageVisitFormPageTest();
			String visitFormName = visitFormPage.addVisitFormTabViewPrivateNo(driver, config, dataSet);

			fc.utobj().printTestStep("Add Tab In Visit Form");
			AdminFieldOpsManageVisitFormCustomizeVisitFormPageTest customizeVisit = new AdminFieldOpsManageVisitFormCustomizeVisitFormPageTest();
			customizeVisit.addTab(driver, config, dataSet);

			fc.utobj().printTestStep("Add Question In Tab");
			fc.utobj().clickElement(driver, pobj.addQuestion);
			customizeVisit.addQuestion(driver, dataSet);
			fc.utobj().clickElement(driver, pobj.finishBtn);

			fc.home_page().logout(driver);
			fc.utobj().printTestStep("Login With Franchise User");
			fc.loginpage().loginWithParameter(driver, franUserName, password);

			fc.utobj().printTestStep("Navigate To Fieldops > Visits Page");
			fc.fieldops().fieldops_common().fieldopsVisitCreateVisitPage(driver);

			fc.utobj().printTestStep("Create Visit");
			createVisitAtFranchiseVisitPage(driver, config, dataSet, visitFormName, franchiseId,
					corpUser.getuserFullName());

			fc.utobj().printTestStep("Verify Franchise User Privilages");

			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//*[contains(text () ,'" + visitFormName + "')]/ancestor::tr/td/div[@id='menuBar']/layer/a/img"));
			String alterText = fc.utobj()
					.getElementByXpath(driver,
							".//*[contains(text () ,'" + visitFormName + "')]/ancestor::tr/td/div[@id='menuBar']/layer")
					.getAttribute("id").trim();
			alterText = alterText.replace("Actions_dynamicmenu", "");
			alterText = alterText.replace("Bar", "");
			List<WebElement> list = driver
					.findElements(By.xpath(".//*[@id='Actions_dynamicmenu" + alterText + "Menu']/div"));

			for (int k = 0; k < list.size(); k++) {

				System.out.println(list.get(k).getText().trim());

				if (list.get(k).getText().trim().equalsIgnoreCase("Delete")) {
					fc.utobj().throwsException("Was not able verify Franchise user Privileges");
				}
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}

	}

	@Test(groups = "fieldops")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify the Create Visit When Multiple Franchise Id Selected at FieldOps Visits Page", testCaseId = "TC_141_Verify_Start_Schedule")
	private void createVisitMultipleFranchiseId() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("fieldops", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			FieldOpsVisitsPage pobj = new FieldOpsVisitsPage(driver);

			fc.utobj().printTestStep("Navigate To Admin > Fieldops > Manage Visit Form");
			fc.utobj().printTestStep("Add Private Visit Form");

			AdminFieldOpsManageVisitFormPageTest visitFormPage = new AdminFieldOpsManageVisitFormPageTest();
			String visitFormName = visitFormPage.addVisitFormPrivate(driver, dataSet);

			fc.utobj().printTestStep("Add Question");
			fc.utobj().clickElement(driver, pobj.addQuestion);

			AdminFieldOpsManageVisitFormCustomizeVisitFormPageTest customizeVisit = new AdminFieldOpsManageVisitFormCustomizeVisitFormPageTest();
			customizeVisit.addQuestion(driver, dataSet);
			fc.utobj().clickElement(driver, pobj.finishBtn);

			fc.utobj().printTestStep("Navigate To Admin > Franchise Location > Add Franchise Location");
			fc.utobj().printTestStep("Add Franchise Location");

			String regionName = fc.utobj().generateTestData("TestR");
			String storeType = fc.utobj().generateTestData("TestS");
			String franchiseId = fc.utobj().generateTestData("TestFID");
			AdminFranchiseLocationAddFranchiseLocationPageTest franchise = new AdminFranchiseLocationAddFranchiseLocationPageTest();
			franchiseId = franchise.addFranchiseLocation_All(driver, franchiseId, regionName, storeType);

			fc.utobj().printTestStep("Navigate To Admin > Franchise Location > Add Franchise Location");
			fc.utobj().printTestStep("Add Another Franchise Location");

			String regionName1 = fc.utobj().generateTestData("Test1R");
			String storeType1 = fc.utobj().generateTestData("Test1S");
			String franchiseId1 = fc.utobj().generateTestData("Test1FID");
			franchiseId1 = franchise.addFranchiseLocation_All(driver, franchiseId1, regionName1, storeType1);

			fc.utobj().printTestStep("Navigate To Admin > Users > Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");

			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUser = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			String emailId = "fieldopsautomation@staffex.com";
			CorporateUser corpUser = new CorporateUser();
			corpUser.setConsultant("Y");
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporateUser.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			fc.utobj().printTestStep("Navigate To FieldOps > Visits Page");
			fc.fieldops().fieldops_common().fieldopsVisitCreateVisitPage(driver);

			fc.utobj().printTestStep("Create Visit");
			fc.utobj().clickElement(driver, pobj.createVisit);

			// select visit Form
			fc.utobj().clickElement(driver, pobj.selectClick);
			fc.utobj().sendKeys(driver, pobj.searchBox, visitFormName);
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver,
							".//*[@id='ms-parentmasterVisitId']/div/ul/li/label[contains(text () ," + "'"
									+ visitFormName + "'" + ")]/input"));

			// select FranchiseID
			fc.utobj().clickElement(driver, pobj.franchiseeSelect);
			fc.utobj().sendKeys(driver, pobj.searchBoxFranchisee, franchiseId);
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver,
							".//*[@id='ms-parentfranchiseeNo']/div/ul/li/label[contains(text () , " + "'" + franchiseId
									+ "'" + ")]/input"));
			fc.utobj().clickElement(driver, pobj.franchiseeSelect);
			fc.utobj().clickElement(driver, pobj.franchiseeSelect);

			fc.utobj().sendKeys(driver, pobj.searchBoxFranchisee, franchiseId1);
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver,
							".//*[@id='ms-parentfranchiseeNo']/div/ul/li/label[contains(text () , " + "'" + franchiseId1
									+ "'" + ")]/input"));
			fc.utobj().clickElement(driver, pobj.franchiseeSelect);

			// Select Consultant
			fc.utobj().clickElement(driver, pobj.selectConsultant);
			fc.utobj().sendKeys(driver, pobj.consultantSearchBox, corpUser.getuserFullName());
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver,
							".//*[@id='ms-parentassignTo']/div/ul/li/label[contains(text () , " + "'"
									+ corpUser.getuserFullName() + "'" + ")]/input"));

			// schedule Date
			String currentDate = fc.utobj().getCurrentDateUSFormat();

			fc.utobj().sendKeys(driver, pobj.scheduleDateTime, currentDate);
			// fc.utobj().clickElement(driver, pobj.schedule);

			fc.utobj().printTestStep("Verify The Start Now Button");
			fc.utobj().clickElement(driver, pobj.startNowButton);

			String msg = fc.utobj().acceptAlertBox(driver);

			if (!msg.equalsIgnoreCase("Start Now works for only Single selected Franchise ID.")) {

				fc.utobj()
						.throwsException("was not able to verify the Start Button When Multiple Franchise ID Selected");
			}

			fc.utobj().printTestStep("Verify The Schedule Buttom When Multiple Franchise Id Selected");
			fc.utobj().clickElement(driver, pobj.schedule);

			defaultViewByFranchiseId(driver, franchiseId);

			boolean isTextPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//td[contains(text () , '" + visitFormName + "')]");
			if (isTextPresent == false) {
				fc.utobj().throwsException("was not able to verify Visit Form Name");
			}

			boolean isTextFranchiseId = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//td[contains(text () , '" + visitFormName
					+ "')]/ancestor::tr/td[contains(text () , '" + franchiseId + "')]");
			if (isTextFranchiseId == false) {
				fc.utobj().throwsException("was not able to verify Franchise Id Name");
			}

			boolean isTextConsultant = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//td[contains(text () , '" + visitFormName
					+ "')]/ancestor::tr/td[contains(text () , '" + corpUser.getuserFullName() + "')]");
			if (isTextConsultant == false) {
				fc.utobj().throwsException("was not able to verify user Name");
			}

			boolean isTextStatus = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//td[contains(text () , '" + visitFormName
					+ "')]/ancestor::tr/td[contains(text () , 'Scheduled')]");
			if (isTextStatus == false) {
				fc.utobj().throwsException("was not able to verify Status Name");
			}

			defaultViewByFranchiseId(driver, franchiseId1);
			boolean isTextPresent1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () , '" + visitFormName + "')]");
			if (isTextPresent1 == false) {
				fc.utobj().throwsException("was not able to verify Visit Form Name");
			}

			boolean isTextFranchiseId1 = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//td[contains(text () , '" + visitFormName
					+ "')]/ancestor::tr/td[contains(text () , '" + franchiseId1 + "')]");
			if (isTextFranchiseId1 == false) {
				fc.utobj().throwsException("was not able to verify Another Franchise Id  Name");
			}

			boolean isTextConsultant1 = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//td[contains(text () , '" + visitFormName
					+ "')]/ancestor::tr/td[contains(text () , '" + corpUser.getuserFullName() + "')]");
			if (isTextConsultant1 == false) {
				fc.utobj().throwsException("was not able to verify User Name");
			}

			boolean isTextStatus1 = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//td[contains(text () , '" + visitFormName
					+ "')]/ancestor::tr/td[contains(text () , 'Scheduled')]");
			if (isTextStatus1 == false) {
				fc.utobj().throwsException("was not able to verify Status Name");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "fieldops")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify Cancel Visit Status , Login With Consultant", testCaseId = "TC_Cancel_Visit_At_Visits_Page_144")
	private void cancelVisit() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("fieldops", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			FieldOpsVisitsPage pobj = new FieldOpsVisitsPage(driver);

			fc.utobj().printTestStep("Navigate To Admin > Hidden Links > Configure FieldOps Settings Page");
			fc.utobj().printTestStep("Configure Field Ops Settings Log Private Comment Yes");
			AdminHiddenLinksConfigureFieldOpsSettingsPageTest hiddenLinks = new AdminHiddenLinksConfigureFieldOpsSettingsPageTest();
			hiddenLinks.configureFieldOpsSettingsLogPrivateCommentYes(driver, config);

			fc.utobj().printTestStep("Navigate To Admin > Fieldops > Manage Visit Form");
			fc.utobj().printTestStep("Add Private Visit Form");
			AdminFieldOpsManageVisitFormPageTest visitFormPage = new AdminFieldOpsManageVisitFormPageTest();
			String visitFormName = visitFormPage.addVisitFormPrivate(driver, dataSet);

			fc.utobj().printTestStep("Add Question In Visit Form");
			fc.utobj().clickElement(driver, pobj.addQuestion);
			AdminFieldOpsManageVisitFormCustomizeVisitFormPageTest customizeVisit = new AdminFieldOpsManageVisitFormCustomizeVisitFormPageTest();
			customizeVisit.addQuestion(driver, dataSet);
			fc.utobj().clickElement(driver, pobj.finishBtn);

			fc.utobj().printTestStep("Navigate To Admin > Franchise Location > Add Franchise Location");
			fc.utobj().printTestStep("Add Franchise Location");
			String regionName = fc.utobj().generateTestData("Test");
			String storeType = fc.utobj().generateTestData("Test");
			String franchiseId = fc.utobj().generateTestData("Test");
			AdminFranchiseLocationAddFranchiseLocationPageTest franchise = new AdminFranchiseLocationAddFranchiseLocationPageTest();
			franchiseId = franchise.addFranchiseLocation_All(driver, franchiseId, regionName, storeType);

			fc.utobj().printTestStep("Navigate To Admin > Users > Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUser = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			String emailId = "fieldopsautomation@staffex.com";
			CorporateUser corpUser = new CorporateUser();
			corpUser.setConsultant("Y");
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporateUser.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			fc.home_page().logout(driver);
			fc.utobj().printTestStep("Login With Corporate User");
			fc.loginpage().loginWithParameter(driver, corpUser.getUserName(), corpUser.getPassword());

			fc.utobj().printTestStep("Navigate To FieldOps > Visits Page");
			fc.utobj().printTestStep("Create Visit");
			createVisitAtVisitPage(driver, config, dataSet, visitFormName, franchiseId, corpUser.getuserFullName());

			fc.utobj().printTestStep("Modify Visit");
			actionImgOption(driver, franchiseId, "Modify");

			fc.utobj().sendKeys(driver, pobj.mNotes, "It is a Sample Visit Updated");
			fc.utobj().selectDropDown(driver, pobj.selectStatus, "Cancelled");
			fc.utobj().clickElement(driver, pobj.submitBtnM);

			defaultViewByFranchiseId(driver, franchiseId);

			boolean isStatusPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () , '" + franchiseId + "')]/ancestor::tr/td[contains(text () , 'Cancelled')]");
			if (isStatusPresent == false) {
				fc.utobj().throwsException("was not able to verify Status Of Visit");
			}

			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//tr/td/a[contains(text () ," + "'"
					+ franchiseId + "'" + ")]/..//following-sibling::td/div[@id='menuBar']/layer/a/img"));
			String alterText = fc.utobj()
					.getElementByXpath(driver,
							".//tr/td/a[contains(text () ," + "'" + franchiseId + "'"
									+ ")]/..//following-sibling::td/div[@id='menuBar']/layer")
					.getAttribute("id").trim();
			alterText = alterText.replace("Actions_dynamicmenu", "");
			alterText = alterText.replace("Bar", "");

			List<WebElement> list = driver.findElements(
					By.xpath(".//div[@id='actionDiv']/div[@id='Actions_dynamicmenu" + alterText + "Menu']/div"));

			fc.utobj().printTestStep("Verify Modify Visit");
			boolean isTextPresent = false;

			for (int x = 0; x < list.size(); x++) {
				String optionName = list.get(x).getText().trim();

				System.out.println(optionName);
				if (optionName.equalsIgnoreCase("Restore")) {
					isTextPresent = true;
				}
			}

			if (isTextPresent == false) {
				fc.utobj().throwsException("Visit not Modify And Not Cancelled");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "fieldops")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify Opener location should be clickable At Visit Summary Page If we use Opener Location For Create Visit", testCaseId = "TC_Create_Visit_For_Opener_Location_145")
	private void createVisitForOpnerLocation() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("fieldops", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			FieldOpsVisitsPage pobj = new FieldOpsVisitsPage(driver);

			fc.utobj().printTestStep("Navigate To Admin > Fieldops > Manage Visit Form");
			fc.utobj().printTestStep("Add Private Visit Form");

			AdminFieldOpsManageVisitFormPageTest visitFormPage = new AdminFieldOpsManageVisitFormPageTest();
			String visitFormName = visitFormPage.addVisitFormPrivate(driver, dataSet);

			fc.utobj().printTestStep("Add Question");
			fc.utobj().clickElement(driver, pobj.addQuestion);

			AdminFieldOpsManageVisitFormCustomizeVisitFormPageTest customizeVisit = new AdminFieldOpsManageVisitFormCustomizeVisitFormPageTest();
			customizeVisit.addQuestion(driver, dataSet);
			fc.utobj().clickElement(driver, pobj.finishBtn);

			fc.utobj().printTestStep("Navigate To Opener > Add Franchise Location");
			OpenerStoreSummaryStoreListPageTest store_list = new OpenerStoreSummaryStoreListPageTest();

			String emailId = "fieldopsautomation@staffex.com";
			String franchiseId = store_list.addNewFranchiseLocation(driver, dataSet, emailId);

			fc.utobj().printTestStep("Navigate To Admin > Users > Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");

			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUser = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser = new CorporateUser();
			corpUser.setConsultant("Y");
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporateUser.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			fc.utobj().printTestStep("Navigate To FieldOps > Visits Page");
			fc.fieldops().fieldops_common().fieldopsVisitCreateVisitPage(driver);

			fc.utobj().printTestStep("Create Visit");
			fc.utobj().clickElement(driver, pobj.createVisit);

			// select visit Form
			fc.utobj().clickElement(driver, pobj.selectClick);
			fc.utobj().sendKeys(driver, pobj.searchBox, visitFormName);
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver,
							".//*[@id='ms-parentmasterVisitId']/div/ul/li/label[contains(text () ," + "'"
									+ visitFormName + "'" + ")]/input"));

			// select FranchiseID
			fc.utobj().clickElement(driver, pobj.franchiseeSelect);
			fc.utobj().sendKeys(driver, pobj.searchBoxFranchisee, franchiseId);
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver,
							".//*[@id='ms-parentfranchiseeNo']/div/ul/li/label[contains(text () , " + "'" + franchiseId
									+ "'" + ")]/input"));
			fc.utobj().clickElement(driver, pobj.franchiseeSelect);

			// Select Consultant
			fc.utobj().clickElement(driver, pobj.selectConsultant);
			fc.utobj().sendKeys(driver, pobj.consultantSearchBox, corpUser.getuserFullName());
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver,
							".//*[@id='ms-parentassignTo']/div/ul/li/label[contains(text () , " + "'"
									+ corpUser.getuserFullName() + "'" + ")]/input"));

			// schedule Date
			String futureDate = fc.utobj().getFutureDateUSFormat(20);

			fc.utobj().sendKeys(driver, pobj.scheduleDateTime, futureDate);
			fc.utobj().clickElement(driver, pobj.schedule);

			defaultViewByFranchiseId(driver, franchiseId);
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//a[contains(text () , '" + franchiseId + "')]"));

			boolean isLocationPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () , '" + franchiseId + "')]");
			if (isLocationPresent == false) {
				fc.utobj().throwsException(
						"was not able to verify Franchise Location At Info Mgr > In Development > Center Info Page");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	// Start Visit And AdditionAl Task In It

	@Test(groups = "fieldops")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Add Additional Task In Visit", testCaseId = "TC_Add_Additional_Task_146")
	private void startVisitAtVisitPage01() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("fieldops", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			FieldOpsVisitsPage pobj = new FieldOpsVisitsPage(driver);

			fc.utobj().printTestStep("Navigate To Admin > Hidden Links > Configure FieldOps Settings Page");
			fc.utobj().printTestStep("Configure Field Ops Settings Log Private Comment Yes");

			AdminHiddenLinksConfigureFieldOpsSettingsPageTest hiddenLinks = new AdminHiddenLinksConfigureFieldOpsSettingsPageTest();
			hiddenLinks.configureFieldOpsSettingsLogPrivateCommentYes(driver, config);

			fc.utobj().printTestStep("Navigate To Admin > FieldOps > Action Library");
			fc.utobj().printTestStep("Add Action Item");

			AdminFieldOpsActionLibraryPageTest actionPage = new AdminFieldOpsActionLibraryPageTest();
			String subject = actionPage.addActionItem(driver, dataSet);

			fc.utobj().printTestStep("Navigate To Admin > Fieldops > Manage Visit Form");
			fc.utobj().printTestStep("Add Private Visit Form");
			AdminFieldOpsManageVisitFormPageTest visitFormPage = new AdminFieldOpsManageVisitFormPageTest();
			String visitFormName = visitFormPage.addVisitFormPrivate(driver, dataSet);

			fc.utobj().printTestStep("Add Question In Visit Form");
			fc.utobj().clickElement(driver, pobj.addQuestion);
			AdminFieldOpsManageVisitFormCustomizeVisitFormPageTest customizeVisit = new AdminFieldOpsManageVisitFormCustomizeVisitFormPageTest();
			customizeVisit.addQuestion(driver, dataSet);
			fc.utobj().clickElement(driver, pobj.finishBtn);

			fc.utobj().printTestStep("Navigate To Admin > Franchise Location > Add Franchise Location");
			fc.utobj().printTestStep("Add Franchise Location");
			String regionName = fc.utobj().generateTestData("TestR");
			String storeType = fc.utobj().generateTestData("TestS");
			String franchiseId = fc.utobj().generateTestData("TestID");
			AdminFranchiseLocationAddFranchiseLocationPageTest franchise = new AdminFranchiseLocationAddFranchiseLocationPageTest();
			franchiseId = franchise.addFranchiseLocation_All(driver, franchiseId, regionName, storeType);

			fc.utobj().printTestStep("Navigate To Admin > Users > Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUser = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			String emailId = "fieldopsautomation@staffex.com";
			CorporateUser corpUser = new CorporateUser();
			corpUser.setConsultant("Y");
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporateUser.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			fc.home_page().logout(driver);
			fc.utobj().printTestStep("Login With Corporate User");
			fc.loginpage().loginWithParameter(driver, corpUser.getUserName(), corpUser.getPassword());

			fc.utobj().printTestStep("Navigate To FieldOps > Visits Page");
			fc.utobj().printTestStep("Create Visit");
			createVisitAtVisitPage(driver, config, dataSet, visitFormName, franchiseId, corpUser.getuserFullName());

			// start Visit
			fc.utobj().printTestStep("Start Visit By Action Image Option");
			actionImgOption(driver, franchiseId, "Start Visit");

			String text = fc.utobj().getElementByXpath(driver, ".//table[@class='bText12gr']//tr/td/label")
					.getAttribute("id").trim();
			text = text.replace("_0", "");

			fc.utobj().sendKeys(driver,
					fc.utobj().getElementByXpath(driver, ".//textarea[@id='Response_" + text + "']"),
					"It is a sample answer");
			fc.utobj().sendKeys(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='private" + text + "']"),
					"It is a private Answer");
			fc.utobj().clickElement(driver, pobj.nextBtnSubmit);

			fc.utobj().printTestStep("Add Additional Task");
			fc.utobj().clickElement(driver, pobj.addMoreTaskButton);

			fc.utobj().selectDropDown(driver, pobj.selectAction, subject);

			fc.utobj().selectDropDown(driver, pobj.actionBy, corpUser.getuserFullName());
			fc.utobj().sendKeys(driver, pobj.dueDate, fc.utobj().getFutureDateUSFormat(12));
			fc.utobj().clickElement(driver, pobj.submitBtnAtVisit);

			boolean isSubjectOfTaskPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () , '" + subject + "')]");

			if (isSubjectOfTaskPresent == false) {
				fc.utobj().throwsException("was not able to verify the subject of task");
			}

			boolean isAssignToPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//td[contains(text () , '" + subject
					+ "')]/ancestor::tr/td[contains(text () ,'" + corpUser.getuserFullName() + "')]");

			if (isAssignToPresent == false) {
				fc.utobj().throwsException("was not able to verify Assign To");
			}
			fc.utobj().clickElement(driver, pobj.okBtn);

			defaultViewByFranchiseId(driver, franchiseId);

			fc.utobj().printTestStep("Verify Visit Status");
			boolean isStatusPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//*[.='" + franchiseId + "']/ancestor::tr/td[contains(text () ,'In Process')]");
			if (isStatusPresent == false) {
				fc.utobj().throwsException("was not able to veriy Status of visit");
			}

			boolean isTaskCountPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () , '" + franchiseId + "')]/ancestor::tr/td/a[.='1']");
			if (isTaskCountPresent == false) {

				fc.utobj().throwsException("was not able to verify task count");
			}

			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//a[contains(text () , '" + franchiseId + "')]/ancestor::tr/td/a[.='1']"));

			boolean isSubjectPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[contains(text () , '" + subject + "')]");
			if (isSubjectPresent == false) {
				fc.utobj().throwsException("was not able to verify Task at Task Page");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "fieldops")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify Modification of Visit and Cancle Visit By Regional User", testCaseId = "TC_Modify_Visit_At_Visit_Page_By_Regional_User_147")
	private void modifyVisit01() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("fieldops", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			FieldOpsVisitsPage pobj = new FieldOpsVisitsPage(driver);

			fc.utobj().printTestStep("Navigate To Admin > Hidden Links > Configure FieldOps Settings Page");
			fc.utobj().printTestStep("Configure Field Ops Settings Log Private Comment Yes");
			AdminHiddenLinksConfigureFieldOpsSettingsPageTest hiddenLinks = new AdminHiddenLinksConfigureFieldOpsSettingsPageTest();
			hiddenLinks.configureFieldOpsSettingsLogPrivateCommentYes(driver, config);

			fc.utobj().printTestStep("Navigate To Admin > Franchise Location > Add Franchise Location");
			fc.utobj().printTestStep("Add Franchise Location");
			String regionName = fc.utobj().generateTestData("Test");
			String storeType = fc.utobj().generateTestData("Test");
			String franchiseId = fc.utobj().generateTestData("Test");
			AdminFranchiseLocationAddFranchiseLocationPageTest franchise = new AdminFranchiseLocationAddFranchiseLocationPageTest();
			franchiseId = franchise.addFranchiseLocation_All(driver, franchiseId, regionName, storeType);

			AdminUsersManageRegionalUsersAddRegionalUserPageTest regional_user = new AdminUsersManageRegionalUsersAddRegionalUserPageTest();
			String userName_region = fc.utobj().generateTestData("TestRUser");
			String password = "t0n1ght123";
			String emailId = "fieldopsautomation@staffex.com";
			regional_user.addRegionalUser(driver, userName_region, password, regionName, emailId);

			fc.utobj().printTestStep("Navigate To Admin > Users > Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUser = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();

			CorporateUser corpUser = new CorporateUser();
			corpUser.setConsultant("Y");
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporateUser.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			fc.home_page().logout(driver);
			fc.utobj().printTestStep("Login By Corporate User");
			fc.loginpage().loginWithParameter(driver, corpUser.getUserName(), corpUser.getPassword());

			fc.utobj().printTestStep("Navigate To Admin > Fieldops > Manage Visit Form");
			fc.utobj().printTestStep("Add Private Visit Form");
			AdminFieldOpsManageVisitFormPageTest visitFormPage = new AdminFieldOpsManageVisitFormPageTest();
			String visitFormName = visitFormPage.addVisitFormPrivate(driver, dataSet);

			fc.utobj().printTestStep("Add Question In Visit Form");
			fc.utobj().clickElement(driver, pobj.addQuestion);
			AdminFieldOpsManageVisitFormCustomizeVisitFormPageTest customizeVisit = new AdminFieldOpsManageVisitFormCustomizeVisitFormPageTest();
			customizeVisit.addQuestion(driver, dataSet);
			fc.utobj().clickElement(driver, pobj.finishBtn);

			fc.home_page().logout(driver);
			fc.utobj().printTestStep("Login With Regional User");
			fc.loginpage().loginWithParameter(driver, userName_region, password);

			fc.utobj().printTestStep("Navigate To FieldOps > Visits Page");
			fc.utobj().printTestStep("Create Visit");
			createVisitAtVisitPage(driver, config, dataSet, visitFormName, franchiseId, corpUser.getuserFullName());

			fc.utobj().printTestStep("Modify Visit");
			actionImgOption(driver, franchiseId, "Modify");

			fc.utobj().sendKeys(driver, pobj.mNotes, "It is a Sample Visit Updated");
			fc.utobj().selectDropDown(driver, pobj.selectStatus, "Cancelled");
			fc.utobj().clickElement(driver, pobj.submitBtnM);

			defaultViewByFranchiseId(driver, franchiseId);

			boolean isStatusOfTaskPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () , '" + franchiseId + "')]/ancestor::tr/td[contains(text () , 'Cancelled')]");

			if (isStatusOfTaskPresent == false) {
				fc.utobj().throwsException("was not able to verify The Cancelled Status Of Visit");
			}

			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//tr/td/a[contains(text () ," + "'"
					+ franchiseId + "'" + ")]/..//following-sibling::td/div[@id='menuBar']/layer/a/img"));
			String alterText = fc.utobj()
					.getElementByXpath(driver,
							".//tr/td/a[contains(text () ," + "'" + franchiseId + "'"
									+ ")]/..//following-sibling::td/div[@id='menuBar']/layer")
					.getAttribute("id").trim();
			alterText = alterText.replace("Actions_dynamicmenu", "");
			alterText = alterText.replace("Bar", "");

			List<WebElement> list = driver.findElements(
					By.xpath(".//div[@id='actionDiv']/div[@id='Actions_dynamicmenu" + alterText + "Menu']/div"));

			fc.utobj().printTestStep("Verify Modify Visit");
			boolean isTextPresent = false;

			for (int x = 0; x < list.size(); x++) {
				String optionName = list.get(x).getText().trim();
				if (optionName.equalsIgnoreCase("Restore")) {
					isTextPresent = true;
				}
			}

			if (isTextPresent == false) {
				fc.utobj().throwsException("Visit not Modify And Not Cancelled");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = {"fieldops", "foFailedTC"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify visit details while updating visit", testCaseId = "TC_Update_Visit_148")
	private void updateVisit01() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("fieldops", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			FieldOpsVisitsPage pobj = new FieldOpsVisitsPage(driver);

			fc.utobj().printTestStep("Navigate To Admin > Hidden Links > Configure FieldOps Settings Page");
			fc.utobj().printTestStep("Configure Field Ops Settings Log Private Comment Yes");

			AdminHiddenLinksConfigureFieldOpsSettingsPageTest hiddenLinks = new AdminHiddenLinksConfigureFieldOpsSettingsPageTest();
			hiddenLinks.configureFieldOpsSettingsLogPrivateCommentYes(driver, config);

			fc.utobj().printTestStep("Navigate To Admin > Fieldops > Manage Visit Form");
			fc.utobj().printTestStep("Add Private Visit Form");
			AdminFieldOpsManageVisitFormPageTest visitFormPage = new AdminFieldOpsManageVisitFormPageTest();
			String visitFormName = visitFormPage.addVisitFormPrivate(driver, dataSet);

			fc.utobj().printTestStep("Add Multiple Input Type Question In Visit Form");
			fc.utobj().clickElement(driver, pobj.addQuestion);

			AdminFieldOpsManageVisitFormCustomizeVisitFormPageTest customizeVisit = new AdminFieldOpsManageVisitFormCustomizeVisitFormPageTest();
			String questionText = fc.utobj().generateTestData(dataSet.get("questionText"));
			customizeVisit.addQuestionMultipleInputType(driver, config, dataSet, questionText);
			fc.utobj().clickElement(driver, pobj.finishBtn);

			fc.utobj().printTestStep("Navigate To Admin > Franchise Location > Add Franchise Location");
			fc.utobj().printTestStep("Add Franchise Location");
			String regionName = fc.utobj().generateTestData("TestR");
			String storeType = fc.utobj().generateTestData("TestS");
			String franchiseId = fc.utobj().generateTestData("TestID");
			AdminFranchiseLocationAddFranchiseLocationPageTest franchise = new AdminFranchiseLocationAddFranchiseLocationPageTest();
			franchiseId = franchise.addFranchiseLocation_All(driver, franchiseId, regionName, storeType);
						
			fc.utobj().printTestStep("Navigate To Admin > Users > Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUser = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			String emailId = "fieldopsautomation@staffex.com";
			CorporateUser corpUser = new CorporateUser();
			corpUser.setConsultant("Y");
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporateUser.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			fc.utobj().printTestStep("Navigate To FieldOps > Visits Page");
			fc.utobj().printTestStep("Create Visit");
			//createVisitAtVisitPage(driver, config, dataSet, visitFormName, franchiseId, corpUser.getuserFullName());

			fc.fieldops().fieldops_common().fieldopsVisitCreateVisitPage(driver);

			fc.utobj().clickElement(driver, pobj.createVisit);
			fc.utobj().selectValFromMultiSelectRadioBtn(driver, pobj.selectVisitForm, visitFormName);
			fc.utobj().selectValFromMultiSelectWithoutReset(driver, pobj.selectFranchiseId, franchiseId);
			fc.utobj().selectValFromMultiSelectRadioBtn(driver, pobj.selectConsultantMultiSelect, corpUser.getuserFullName());

			String curDate = fc.utobj().getCurrentDateUSFormat();
			fc.utobj().sendKeys(driver, pobj.scheduleDateTime, curDate);

			if (pobj.sendMailCheckNotiFranchiseeOwner.isDisplayed()) {
				if (!pobj.sendMailCheckNotiFranchiseeOwner.isSelected()) {
					fc.utobj().clickElement(driver, pobj.sendMailCheckNotiFranchiseeOwner);
				}
			}
			fc.utobj().clickElement(driver, pobj.schedule);		
			fc.utobj().printTestStep("Start Visit At Visit Page");
			actionImgOption(driver, franchiseId, "Start Visit");
			String inputLabel1 = "Input Label";
			fc.utobj().sendKeys(driver, fc.utobj().getElementByXpath(driver, ".//td[contains(text () , '" + inputLabel1 + "')]/input[1]"), "50");
			fc.utobj().sendKeys(driver, fc.utobj().getElementByXpath(driver, ".//td[contains(text () , '" + inputLabel1 + "')]/input[2]"), "35");

			fc.utobj().sendKeys(driver,	fc.utobj().getElementByXpath(driver, ".//*[@placeholder='This Private comment will not be visible to Franchise']"),	"This Private comment will not be visible to Franchise");
			fc.utobj().clickElement(driver, pobj.submitAllBtn);
			fc.utobj().clickElement(driver, pobj.submitBtnAtVisit);
			fc.utobj().clickElement(driver, pobj.okBtn);

			defaultViewByFranchiseId(driver, franchiseId);
			fc.utobj().printTestStep("Update Visit And Verify Visit Details");
			fc.utobj().actionImgOption(driver, visitFormName, "Update Visit");

			String fstLabelValue = fc.utobj().getTextTxtFld(driver, fc.utobj().getElementByXpath(driver, ".//td[contains(text () , '" + inputLabel1 + "')]/input[1]"));
			String secondLebelValue = fc.utobj().getTextTxtFld(driver, fc.utobj().getElementByXpath(driver,	".//td[contains(text () , '" + inputLabel1 + "')]/input[2]"));

			if (!fstLabelValue.equalsIgnoreCase("50")) {
				fc.utobj().throwsException("was not able to verify response 1");
			}

			if (!secondLebelValue.equalsIgnoreCase("35")) {
				fc.utobj().throwsException("was not able to verify response 2");
			}

			// score at update visit page
			String scoreAdd = fc.utobj().getTextTxtFld(driver,
					fc.utobj().getElementByXpath(driver, ".//*[contains(text () , '" + inputLabel1
							+ "')]//input[starts-with(@id , 'Response')]"));
			// scoreAdd=scoreAdd.replaceFirst(".00", "");

			fc.utobj().clickElement(driver, pobj.submitAllBtn);
			fc.utobj().clickElement(driver, pobj.submitBtnAtVisit);

			// sum at score page
			String totalSumScore = fc.utobj()
					.getText(driver,
							fc.utobj().getElementByXpath(driver,
									".//*[contains(text () , '" + questionText
											+ " ')]/ancestor::tr/following-sibling::tr/td[contains(text () , '"
											+ scoreAdd + "')]"));

			totalSumScore = totalSumScore.replace(".00", "");
			scoreAdd = scoreAdd.replaceFirst(".00", "");

			if (!scoreAdd.equalsIgnoreCase(totalSumScore)) {
				fc.utobj().throwsException("was not able to verify result at update page and summary page");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "fieldops" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Create Visit using opener location", testCaseId = "TC_Verify_Practice_Visit_Opener_Location_149")
	private void verifyPracticeVisitByOpenerLocation() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("fieldops", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			FieldOpsVisitsPage pobj = new FieldOpsVisitsPage(driver);

			fc.utobj().printTestStep("Navigate To Admin > Hidden Links > Configure FieldOps Settings Page");
			fc.utobj().printTestStep("Configure Field Ops Settings Log Private Comment Yes");
			AdminHiddenLinksConfigureFieldOpsSettingsPageTest hiddenLinks = new AdminHiddenLinksConfigureFieldOpsSettingsPageTest();
			hiddenLinks.configureFieldOpsSettingsLogPrivateCommentYes(driver, config);

			fc.utobj().printTestStep("Configure FieldOps Settings Franchise Self Audit Yes Owner Scoring Yes");
			hiddenLinks.configureFieldOpsSettingsFranchiseSelfAuditYesOwnerScoringYes(driver, config);
			String password = "t0n1ght123";

			fc.utobj().printTestStep("Navigate To Admin > Users > Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUser = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			String emailId = "fieldopsautomation@staffex.com";
			CorporateUser corpUser = new CorporateUser();
			corpUser.setConsultant("Y");
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporateUser.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			fc.utobj().printTestStep("Navigate To Opener > Add Franchise Location");
			OpenerStoreSummaryStoreListPageTest store_list = new OpenerStoreSummaryStoreListPageTest();
			String franchiseId = store_list.addNewFranchiseLocation(driver, dataSet, emailId);

			fc.utobj().printTestStep("Add Franchise User");
			AdminUsersManageManageFranchiseUsersPageTest franUser = new AdminUsersManageManageFranchiseUsersPageTest();
			String FranUserName = fc.utobj().generateTestData("TestF");
			String roleName = "Default Franchise Role";
			String franUserName = franUser.addFranchiseUser(driver, FranUserName, password, franchiseId, roleName,
					emailId);

			fc.utobj().printTestStep("Navigate To Admin > FieldOps > Manage Visit Form");
			fc.utobj().printTestStep("Add Non-Private Visit Form With Tab View");
			AdminFieldOpsManageVisitFormPageTest visitFormPage = new AdminFieldOpsManageVisitFormPageTest();
			String visitFormName = visitFormPage.addVisitFormTabViewPrivateNo(driver, config, dataSet);

			fc.utobj().printTestStep("Add Tab In Visit Form");
			AdminFieldOpsManageVisitFormCustomizeVisitFormPageTest customizeVisit = new AdminFieldOpsManageVisitFormCustomizeVisitFormPageTest();
			customizeVisit.addTab(driver, config, dataSet);

			fc.utobj().printTestStep("Add Question In Tab");
			fc.utobj().clickElement(driver, pobj.addQuestion);
			customizeVisit.addQuestion(driver, dataSet);
			fc.utobj().clickElement(driver, pobj.finishBtn);

			fc.home_page().logout(driver);
			fc.utobj().printTestStep("Login With Franchise User");
			fc.loginpage().loginWithParameter(driver, franUserName, password);

			fc.utobj().printTestStep("Navigate To Fieldops > Visits Page");
			fc.fieldops().fieldops_common().fieldopsVisitCreateVisitPage(driver);
			fc.utobj().printTestStep("Create Visit");
			createVisitAtFranchiseVisitPage(driver, config, dataSet, visitFormName, franchiseId,
					corpUser.getuserFullName());

			fc.utobj().getText(driver, fc.utobj().getElementByXpath(driver,
					".//tr/td[contains(text () ," + "'" + visitFormName + "'" + ")]/preceding-sibling::td/a"));

			fc.utobj().printTestStep("Modify Visit");
			fc.utobj().actionImgOption(driver, visitFormName, "Modify");

			String franchiseIdText = fc.utobj()
					.getElementByXpath(driver,
							".//*[@id='ms-parentfranchiseeNo']/button/span[contains(text () , '" + franchiseId + "')]")
					.getText().trim();

			if (!franchiseIdText.equalsIgnoreCase(franchiseId)) {
				fc.utobj().throwsException("was not able to verify opener location in combo");
			}

			fc.utobj().clickElement(driver, pobj.submitBtnM);

			boolean isFranchisePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () , '" + visitFormName + "')]");
			if (isFranchisePresent == false) {
				fc.utobj().throwsException("Not able to modify Visit Created with opener location");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}

	}

	@Test(groups = "fieldopsTest")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify the change of franchise id while modifying visit", testCaseId = "TC_Modify_Franchise_In_Visit_Form_150")
	private void modifyVisit02() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("fieldops", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			FieldOpsVisitsPage pobj = new FieldOpsVisitsPage(driver);

			fc.utobj().printTestStep("Navigate To Admin > Hidden Links > Configure FieldOps Settings Page");
			fc.utobj().printTestStep("Configure Field Ops Settings Log Private Comment Yes");
			AdminHiddenLinksConfigureFieldOpsSettingsPageTest hiddenLinks = new AdminHiddenLinksConfigureFieldOpsSettingsPageTest();
			hiddenLinks.configureFieldOpsSettingsLogPrivateCommentYes(driver, config);

			fc.utobj().printTestStep("Navigate To Admin > Fieldops > Manage Visit Form");
			fc.utobj().printTestStep("Add Private Visit Form");
			AdminFieldOpsManageVisitFormPageTest visitFormPage = new AdminFieldOpsManageVisitFormPageTest();
			String visitFormName = visitFormPage.addVisitFormPrivate(driver, dataSet);

			fc.utobj().printTestStep("Add Question In Visit Form");
			fc.utobj().clickElement(driver, pobj.addQuestion);
			AdminFieldOpsManageVisitFormCustomizeVisitFormPageTest customizeVisit = new AdminFieldOpsManageVisitFormCustomizeVisitFormPageTest();
			customizeVisit.addQuestion(driver, dataSet);
			fc.utobj().clickElement(driver, pobj.finishBtn);

			fc.utobj().printTestStep("Navigate To Admin > Franchise Location > Add Franchise Location");
			fc.utobj().printTestStep("Add Franchise Location");
			String regionName = fc.utobj().generateTestData("Test");
			String storeType = fc.utobj().generateTestData("Test");
			String franchiseId = fc.utobj().generateTestData("Test");
			AdminFranchiseLocationAddFranchiseLocationPageTest franchise = new AdminFranchiseLocationAddFranchiseLocationPageTest();
			franchiseId = franchise.addFranchiseLocation_All(driver, franchiseId, regionName, storeType);

			fc.utobj().printTestStep("Navigate To Admin > Users > Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUser = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			String emailId = "fieldopsautomation@staffex.com";
			CorporateUser corpUser = new CorporateUser();
			corpUser.setConsultant("Y");
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporateUser.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			fc.utobj().printTestStep("Navigate To Admin > Franchise Location > Add Franchise Location");
			fc.utobj().printTestStep("Add new Franchise Location");
			String regionName1 = fc.utobj().generateTestData("Test");
			String storeType1 = fc.utobj().generateTestData("Test");
			String franchiseId1 = fc.utobj().generateTestData("Test");

			franchiseId1 = franchise.addFranchiseLocation_All(driver, franchiseId1, regionName1, storeType1);

			fc.utobj().printTestStep("Navigate To FieldOps > Visits Page");
			fc.utobj().printTestStep("Create Visit");
			createVisitAtVisitPage(driver, config, dataSet, visitFormName, franchiseId, corpUser.getuserFullName());

			fc.utobj().printTestStep("Modify Visit");
			actionImgOption(driver, franchiseId, "Modify");

			fc.utobj().printTestStep("Modify Franchise Id");

			fc.utobj().clickElement(driver, pobj.franchiseeSelect);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//*[@id='ms-parentfranchiseeNo']/div/ul/li/label[contains(text () , 'Select')]/input"));

			// select FranchiseID
			fc.utobj().clickElement(driver, pobj.franchiseeSelect);
			fc.utobj().sendKeys(driver, pobj.searchBoxFranchisee, franchiseId1);
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver,
							".//*[@id='ms-parentfranchiseeNo']/div/ul/li/label[contains(text () , " + "'" + franchiseId1
									+ "'" + ")]/input"));

			fc.utobj().printTestStep("Uncheck Consultant and verify");

			fc.utobj().clickElement(driver, pobj.selectConsultant);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//*[@id='ms-parentassignTo']/div/ul/li/label[contains(text () , 'Select')]/input"));

			fc.utobj().clickElement(driver, pobj.submitBtnM);

			String alertText = fc.utobj().acceptAlertBox(driver);

			if (!alertText.equalsIgnoreCase("Please Select Consultant.")) {
				fc.utobj().throwsException("was not able to verify the Consultant is Mandatory");
			}

			// Select Consultant
			fc.utobj().clickElement(driver, pobj.selectConsultant);
			fc.utobj().sendKeys(driver, pobj.consultantSearchBox, corpUser.getuserFullName());
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver,
							".//*[@id='ms-parentassignTo']/div/ul/li/label[contains(text () , " + "'"
									+ corpUser.getuserFullName() + "'" + ")]/input"));

			fc.utobj().sendKeys(driver, pobj.mNotes, "It is a Sample Visit Updated");
			fc.utobj().selectDropDown(driver, pobj.selectStatus, "Cancelled");
			fc.utobj().clickElement(driver, pobj.submitBtnM);
			defaultViewByFranchiseId(driver, franchiseId1);

			boolean isStatusOfTaskPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[contains(text () , '" + franchiseId1
					+ "')]/ancestor::tr/td[contains(text () , 'Cancelled')]");

			if (isStatusOfTaskPresent == false) {
				fc.utobj().throwsException("was not able to verify The Cancelled Status Of Visit");
			}

			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//tr/td/a[contains(text () ," + "'"
					+ franchiseId1 + "'" + ")]/..//following-sibling::td/div[@id='menuBar']/layer/a/img"));
			String alterText = fc.utobj()
					.getElementByXpath(driver,
							".//tr/td/a[contains(text () ," + "'" + franchiseId1 + "'"
									+ ")]/..//following-sibling::td/div[@id='menuBar']/layer")
					.getAttribute("id").trim();
			alterText = alterText.replace("Actions_dynamicmenu", "");
			alterText = alterText.replace("Bar", "");
			System.out.println(alterText);

			List<WebElement> list = driver.findElements(
					By.xpath(".//div[@id='actionDiv']/div[@id='Actions_dynamicmenu" + alterText + "Menu']/div"));

			fc.utobj().printTestStep("Verify Modify Visit");
			boolean isTextPresent = false;

			for (int x = 0; x < list.size(); x++) {
				String optionName = list.get(x).getText().trim();

				System.out.println(optionName);
				if (optionName.equalsIgnoreCase("Restore")) {
					isTextPresent = true;
				}
			}

			if (isTextPresent == false) {
				fc.utobj().throwsException("Visit not Modify And Not Cancelled");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "fieldopsTest")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify for monthly frequency only single visit should be schedule for any franchise.", testCaseId = "TC_Verify_Monthly_Frequency_Visit_151")
	private void createVisit() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("fieldops", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			FieldOpsVisitsPage pobj = new FieldOpsVisitsPage(driver);

			fc.utobj().printTestStep("Navigate To Admin > Fieldops > Manage Visit Form");
			fc.utobj().printTestStep("Add Private Visit Form");

			AdminFieldOpsManageVisitFormPageTest visitFormPage = new AdminFieldOpsManageVisitFormPageTest();
			String visitFormName = visitFormPage.addVisitFormPrivate(driver, dataSet);

			fc.utobj().printTestStep("Add Question");
			fc.utobj().clickElement(driver, pobj.addQuestion);
			AdminFieldOpsManageVisitFormCustomizeVisitFormPageTest customize_Visit = new AdminFieldOpsManageVisitFormCustomizeVisitFormPageTest();
			customize_Visit.addQuestion(driver, dataSet);
			fc.utobj().clickElement(driver, pobj.finishBtn);

			fc.utobj().printTestStep("Navigate To Admin > Franchise Location > Add Franchise Location");
			fc.utobj().printTestStep("Add Franchise Location");

			String regionName = fc.utobj().generateTestData("TestR");
			String storeType = fc.utobj().generateTestData("TestS");
			String franchiseId = fc.utobj().generateTestData("TestFID");
			AdminFranchiseLocationAddFranchiseLocationPageTest franchise = new AdminFranchiseLocationAddFranchiseLocationPageTest();
			franchiseId = franchise.addFranchiseLocation_All(driver, franchiseId, regionName, storeType);

			fc.utobj().printTestStep("Navigate To Admin > Users > Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");

			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUser = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			String emailId = "fieldopsautomation@staffex.com";
			CorporateUser corpUser = new CorporateUser();
			corpUser.setConsultant("Y");
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporateUser.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			fc.utobj().printTestStep("Navigate To FieldOps > Visits Page");
			fc.fieldops().fieldops_common().fieldopsVisitCreateVisitPage(driver);

			fc.utobj().printTestStep("Create Visit");
			fc.utobj().clickElement(driver, pobj.createVisit);

			// select visit Form
			fc.utobj().clickElement(driver, pobj.selectClick);
			fc.utobj().sendKeys(driver, pobj.searchBox, visitFormName);
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver,
							".//*[@id='ms-parentmasterVisitId']/div/ul/li/label[contains(text () ," + "'"
									+ visitFormName + "'" + ")]/input"));

			// select FranchiseID
			fc.utobj().clickElement(driver, pobj.franchiseeSelect);
			fc.utobj().sendKeys(driver, pobj.searchBoxFranchisee, franchiseId);
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver,
							".//*[@id='ms-parentfranchiseeNo']/div/ul/li/label[contains(text () , " + "'" + franchiseId
									+ "'" + ")]/input"));
			fc.utobj().clickElement(driver, pobj.franchiseeSelect);

			// Select Consultant
			fc.utobj().clickElement(driver, pobj.selectConsultant);
			fc.utobj().sendKeys(driver, pobj.consultantSearchBox, corpUser.getuserFullName());
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver,
							".//*[@id='ms-parentassignTo']/div/ul/li/label[contains(text () , " + "'"
									+ corpUser.getuserFullName() + "'" + ")]/input"));

			// schedule Date
			String currentDate = fc.utobj().getCurrentDateUSFormat();

			fc.utobj().sendKeys(driver, pobj.scheduleDateTime, currentDate);
			fc.utobj().clickElement(driver, pobj.schedule);

			fc.utobj().printTestStep("Create Visit using same location and same monthly Visit Form");
			fc.utobj().clickElement(driver, pobj.createVisit);

			// select visit Form
			fc.utobj().clickElement(driver, pobj.selectClick);
			fc.utobj().sendKeys(driver, pobj.searchBox, visitFormName);
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver,
							".//*[@id='ms-parentmasterVisitId']/div/ul/li/label[contains(text () ," + "'"
									+ visitFormName + "'" + ")]/input"));

			// select FranchiseID
			fc.utobj().clickElement(driver, pobj.franchiseeSelect);
			fc.utobj().sendKeys(driver, pobj.searchBoxFranchisee, franchiseId);
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver,
							".//*[@id='ms-parentfranchiseeNo']/div/ul/li/label[contains(text () , " + "'" + franchiseId
									+ "'" + ")]/input"));
			fc.utobj().clickElement(driver, pobj.franchiseeSelect);

			// Select Consultant
			fc.utobj().clickElement(driver, pobj.selectConsultant);
			fc.utobj().sendKeys(driver, pobj.consultantSearchBox, corpUser.getuserFullName());
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver,
							".//*[@id='ms-parentassignTo']/div/ul/li/label[contains(text () , " + "'"
									+ corpUser.getuserFullName() + "'" + ")]/input"));

			// schedule Date
			String fDate = fc.utobj().getFutureDateUSFormat(2);

			fc.utobj().sendKeys(driver, pobj.scheduleDateTime, fDate);
			fc.utobj().clickElement(driver, pobj.schedule);

			String alertText = fc.utobj().acceptAlertBox(driver);

			if (!alertText.contains("A Mothly Visit is already scheduled for " + franchiseId)) {
				fc.utobj().throwsException("Was not able to verify the Monthly Frequency");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	public void defaultViewByFranchiseId(WebDriver driver, String franchiseId)
			throws Exception {
		fc.fieldops().fieldops_common().fieldopsVisit(driver);
		AdminFieldOpsDeleteFieldOpsVisitsPage pobj = new AdminFieldOpsDeleteFieldOpsVisitsPage(driver);

		fc.utobj().setToDefault(driver, pobj.visitFormSelect);
		fc.utobj().setToDefault(driver, pobj.franchiseIdSelect);
		fc.utobj().setToDefault(driver, pobj.consultantSelect);
		fc.utobj().setToDefault(driver, pobj.statusSelect);
		try
		{
			fc.utobj().setToDefault(driver, pobj.divisionSelect);
		}catch(Exception e)
		{
			
		}
		fc.utobj().clickElement(driver, pobj.saveViewBtn);
		fc.utobj().selectValFromMultiSelect(driver, pobj.franchiseIdSelect, pobj.franchaseIDTxBx, franchiseId);
		fc.utobj().clickElement(driver, pobj.viewBtn);
	}

	public void defaultViewByVisitId(WebDriver driver, String visitID, Map<String, String> config) throws Exception {

		AdminHiddenLinksConfigureNewHierarchyLevelPageTest hLevelPage = new AdminHiddenLinksConfigureNewHierarchyLevelPageTest();
		hLevelPage.ConfigureNewHierarchyLevel(driver);

		AdminFieldOpsDeleteFieldOpsVisitsPage pobj = new AdminFieldOpsDeleteFieldOpsVisitsPage(driver);

		fc.utobj().setToDefault(driver, pobj.visitFormSelect);
		fc.utobj().setToDefault(driver, pobj.franchiseIdSelect);
		fc.utobj().setToDefault(driver, pobj.consultantSelect);
		fc.utobj().setToDefault(driver, pobj.statusSelect);
		fc.utobj().setToDefault(driver, pobj.divisionSelect);
		fc.utobj().clickElement(driver, pobj.saveViewBtn);

		fc.utobj().sendKeys(driver, pobj.visitNumber, visitID);
		fc.utobj().clickElement(driver, pobj.viewBtn);

	}

	public void defaultViewByVisitIdCorporate(WebDriver driver, String visitID, Map<String, String> config)
			throws Exception {

		AdminFieldOpsDeleteFieldOpsVisitsPage pobj = new AdminFieldOpsDeleteFieldOpsVisitsPage(driver);

		fc.utobj().setToDefault(driver, pobj.visitFormSelect);
		fc.utobj().setToDefault(driver, pobj.franchiseIdSelect);
		fc.utobj().setToDefault(driver, pobj.consultantSelect);
		fc.utobj().setToDefault(driver, pobj.statusSelect);

		try {
			fc.utobj().setToDefault(driver, pobj.divisionSelect);
		} catch (Exception e) {
			// TODO: handle exception
		}
		fc.utobj().clickElement(driver, pobj.saveViewBtn);

		fc.utobj().sendKeys(driver, pobj.visitNumber, visitID);
		fc.utobj().clickElement(driver, pobj.viewBtn);

	}

	public void actionImgOption(WebDriver driver, String franchiseId, String option) throws Exception {

		fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//tr/td/a[contains(text () ," + "'"
				+ franchiseId + "'" + ")]/..//following-sibling::td/div[@id='menuBar']/layer/a/img"));
		String alterText = fc.utobj().getElementByXpath(driver, ".//tr/td/a[contains(text () ," + "'" + franchiseId
				+ "'" + ")]/..//following-sibling::td/div[@id='menuBar']/layer").getAttribute("id").trim();
		alterText = alterText.replace("Actions_dynamicmenu", "");
		alterText = alterText.replace("Bar", "");
		System.out.println(alterText);

		fc.utobj().clickElement(driver,
				fc.utobj().getElementByXpath(driver, ".//div[@id='actionDiv']/div[@id='Actions_dynamicmenu" + alterText
						+ "Menu']/div[contains(text () ," + "'" + option + "'" + ")]"));
	}

	public void actionImgOptionInfoMgr(WebDriver driver, String visitFormName, String option) throws Exception {

		fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//tr/td[contains(text () ," + "'"
				+ visitFormName + "'" + ")]/following-sibling::td/div[@id='menuBar']/layer/a/img"));
		String alterText = fc.utobj().getElementByXpath(driver, ".//tr/td[contains(text () ," + "'" + visitFormName
				+ "'" + ")]/following-sibling::td/div[@id='menuBar']/layer").getAttribute("id").trim();
		alterText = alterText.replace("Actions_dynamicmenu", "");
		alterText = alterText.replace("Bar", "");
		System.out.println(alterText);

		fc.utobj().clickElement(driver,
				fc.utobj().getElementByXpath(driver, ".//div[@id='actionDiv']/div[@id='Actions_dynamicmenu" + alterText
						+ "Menu']/div[contains(text () ," + "'" + option + "'" + ")]"));
	}

	public void navigateToQAHistory(WebDriver driver, String franchiseId) throws Exception {

		FieldOpsVisitsPage pobj = new FieldOpsVisitsPage(driver);
		// QA History

		fc.infomgr().infomgr_common().fimModule(driver);
		fc.utobj().clickElement(driver, pobj.franchisees);

		fc.utobj().sendKeys(driver, pobj.searchFranchisee, franchiseId);
		fc.utobj().clickElement(driver, pobj.topSearchBtn);

		fc.utobj().clickElement(driver, fc.utobj().getElementByPartialLinkText(driver, franchiseId));

		fc.utobj().clickElement(driver, pobj.qaHistory);

		// default View at QA History Page
		String text = fc.utobj().getText(driver,
				fc.utobj().getElementByXpath(driver, ".//*[@id='ms-parentmasterVisitId']/button/span"));
		if (text.equalsIgnoreCase("All Selected")) {
			fc.utobj().clickElement(driver, pobj.visitFormSelectBtn);
			fc.utobj().clickElement(driver, pobj.visitFormSelectAll);
			fc.utobj().clickElement(driver, pobj.visitFormSelectBtn);
		} else {
			fc.utobj().clickElement(driver, pobj.visitFormSelectBtn);
			fc.utobj().clickElement(driver, pobj.visitFormSelectAll);
			fc.utobj().clickElement(driver, pobj.visitFormSelectAll);
			fc.utobj().clickElement(driver, pobj.visitFormSelectBtn);
		}
	}

	public void actionImgOptionAtFranchise(WebDriver driver, String visitFormName, String option) throws Exception {

		fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//tr/td[contains(text () ," + "'"
				+ visitFormName + "'" + ")]/following-sibling::td/div[@id='menuBar']/layer/a/img"));
		String alterText = fc.utobj().getElementByXpath(driver, ".//tr/td[contains(text () ," + "'" + visitFormName
				+ "'" + ")]/following-sibling::td/div[@id='menuBar']/layer").getAttribute("id").trim();
		alterText = alterText.replace("Actions_dynamicmenu", "");
		alterText = alterText.replace("Bar", "");
		System.out.println(alterText);

		fc.utobj().clickElement(driver,
				fc.utobj().getElementByXpath(driver, ".//div[@id='actionDiv']/div[@id='Actions_dynamicmenu" + alterText
						+ "Menu']/div[contains(text () ," + "'" + option + "'" + ")]"));
	}
}
