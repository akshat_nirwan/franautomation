package com.builds.test.fieldops;

import org.openqa.selenium.WebDriver;
import org.testng.Reporter;

import com.builds.test.admin.AdminPageTest;
import com.builds.test.common.FCHomePageTest;
import com.builds.utilities.FranconnectUtil;

public class FieldOps_Common extends FranconnectUtil {

	public void adminFieldopsActionLibrary(WebDriver driver) throws Exception {
		Reporter.log("Navigating to adminFieldopsActionLibrary...");
		adminpage().adminPage(driver);
		AdminPageTest p2 = new AdminPageTest();
		p2.openActionLibrary(driver);
	}

	public void adminFieldOpsQuestionLibrary(WebDriver driver) throws Exception {
		Reporter.log("Navigating to adminPerformanceWiseQuestionLibrary...");
		adminpage().adminPage(driver);
		AdminPageTest p2 = new AdminPageTest();
		p2.openQuestionLibraryLnk(driver);
	}

	public void adminFieldOpsConfigureScore(WebDriver driver) throws Exception {
		Reporter.log("Navigating to adminFieldOpsConfigureScore...");
		adminpage().adminPage(driver);
		AdminPageTest p2 = new AdminPageTest();
		p2.configureScoreLnk(driver);
	}

	public void adminFieldOpsManageVisitForm(WebDriver driver) throws Exception {
		Reporter.log("Navigating to amdinFieldOpsManageVisitForm...");
		adminpage().adminPage(driver);
		AdminPageTest p2 = new AdminPageTest();
		p2.openManageVisitForm(driver);
	}

	public void adminFieldOpsQATabIntegration(WebDriver driver) throws Exception {
		Reporter.log("Navigating to amdinFieldOpsQATabIntegration...");
		adminpage().adminPage(driver);
		AdminPageTest p2 = new AdminPageTest();
		p2.qaTabIntegrationLnk(driver);
	}

	public void adminFieldOpsManageConsultant(WebDriver driver) throws Exception {
		Reporter.log("Navigating to amdinFieldOpsManageConsultant...");
		adminpage().adminPage(driver);

		AdminPageTest p2 = new AdminPageTest();
		p2.openManageConsultant(driver);
	}

	public void adminFieldOpsConfigureReminderAlertsforTasks(WebDriver driver) throws Exception {
		Reporter.log("Navigating to adminFieldOpsConfigureReminderAlertsforTasks...");
		adminpage().adminPage(driver);
		AdminPageTest p2 = new AdminPageTest();
		p2.configureReminderAlertsForTasksLnk(driver);
	}

	public void adminFieldOpsConfigureAlertEmailTriggersforTasks(WebDriver driver) throws Exception {
		Reporter.log("Navigating to adminFieldOpsConfigureAlertEmailTriggersforTasks...");
		adminpage().adminPage(driver);
		AdminPageTest p2 = new AdminPageTest();
		p2.configureAlertEmailTriggersForTasksLnk(driver);
	}

	public void adminFieldOpsConfigureMediaLibrary(WebDriver driver) throws Exception {
		Reporter.log("Navigating to adminFieldOpsConfigureMediaLibrary...");
		adminpage().adminPage(driver);
		AdminPageTest p2 = new AdminPageTest();
		p2.configureMediaLibraryLnk(driver);
	}

	public void adminFieldOpsConfigureScheduleVisitEmailContent(WebDriver driver) throws Exception {
		Reporter.log("Navigating to adminFieldOpsConfigureScheduleVisitEmailContent...");
		adminpage().adminPage(driver);
		AdminPageTest p2 = new AdminPageTest();
		p2.openConfigureScheduleVisitEmailContent(driver);
	}

	public void adminFieldOpsDeleteFieldOpsVisits(WebDriver driver) throws Exception {
		Reporter.log("Navigating to adminFieldOpsConfigureScheduleVisitEmailContent...");
		adminpage().adminPage(driver);
		AdminPageTest p2 = new AdminPageTest();
		p2.openDeleteFieldOpsVisits(driver);

	}

	public void fieldopsHomeCreateVisitPage(WebDriver driver) throws Exception {
		Reporter.log("Navigating to fieldopsHomeCreateVisitPage...");

		fieldops().fieldops_common().openFieldOpsHomeSubModule(home_page(), driver);
	}

	public void fieldopsVisitCreateVisitPage(WebDriver driver) throws Exception {
		Reporter.log("Navigating to fieldopsVisitCreateVisitPage...");
		fieldops().fieldops_common().openFieldOpsVisitSubModule(home_page(), driver);

	}

	public void fieldopsTasks(WebDriver driver) throws Exception {
		Reporter.log("Navigating to fieldopsTasks...");

		fieldops().fieldops_common().openFieldOpsTasksSubModule(home_page(), driver);

	}

	public void fieldopsVisit(WebDriver driver) throws Exception {
		Reporter.log("Navigating to fieldopsVisit...");
		fieldops().fieldops_common().openFieldOpsVisitSubModule(home_page(), driver);

	}

	public void fieldopsReports(WebDriver driver) throws Exception {
		Reporter.log("Navigating to fieldopsReports...");
		fieldops().fieldops_common().openFieldOpsReportsSubModule(home_page(), driver);
	}

	public void openFieldOpsHomeSubModule(FCHomePageTest fcHomePageTest, WebDriver driver) throws Exception {

		fcHomePageTest.openFieldopsModule(driver);
		utobj().getElementByLinkText(driver, "Home").click();
		Reporter.log("Navigating to FieldOps Home Page...");
	}

	public void openFieldOpsVisitSubModule(FCHomePageTest fcHomePageTest, WebDriver driver) throws Exception {

		fcHomePageTest.openFieldopsModule(driver);
		utobj().getElementByLinkText(driver, "Visits").click();
		Reporter.log("Navigating to FieldOps Visits Page...");
	}

	public void openFieldOpsTasksSubModule(FCHomePageTest fcHomePageTest, WebDriver driver) throws Exception {

		fcHomePageTest.openFieldopsModule(driver);
		utobj().getElementByLinkText(driver, "Tasks").click();
		Reporter.log("Navigating to FieldOps Tasks Page...");
	}

	public void openFieldOpsReportsSubModule(FCHomePageTest fcHomePageTest, WebDriver driver) throws Exception {

		fcHomePageTest.openFieldopsModule(driver);
		utobj().getElementByLinkText(driver, "Reports").click();
		Reporter.log("Navigating to FieldOps Reports Page...");
	}

}
