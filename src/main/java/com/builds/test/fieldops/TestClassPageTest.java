package com.builds.test.fieldops;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import com.builds.test.admin.AdminFranchiseLocationAddFranchiseLocationPageTest;
import com.builds.test.admin.AdminUsersManageCorporateUsersAddCorporateUserPageTest;
import com.builds.test.common.CorporateUser;
import com.builds.uimaps.fieldops.AdminFieldOpsManageVisitFormCustomizeVisitFormPage;
import com.builds.uimaps.fieldops.AdminFieldOpsManageVisitFormPage;
import com.builds.uimaps.fieldops.AdminFieldOpsQuestionLibraryPage;
import com.builds.uimaps.fieldops.FieldOpsVisitsPage;
import com.builds.utilities.FranconnectUtil;
import com.builds.utilities.TestCase;

import test.edge2.GetTestData;

public class TestClassPageTest {

	FranconnectUtil fc = new FranconnectUtil();

	@Test(groups = "fieldopstest123456")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Creation of Visit Forms & Participation by Users", testCaseId = "TC_Creation_Of_Visit")
	public void createVisit() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData(testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			FieldOpsVisitsPage pobj = new FieldOpsVisitsPage(driver);

			fc.utobj().printTestStep("Navigate To Admin > Users > Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUser = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();

			String emailId = "fieldopsautomation@staffex.com";
			//
			CorporateUser corpUser = new CorporateUser();
			corpUser.setConsultant("Y");
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporateUser.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			fc.utobj().printTestStep("Navigate To Admin > Franchise Location > Add Franchise Location");
			fc.utobj().printTestStep("Add Franchise Location");
			String regionName = fc.utobj().generateTestData("TestR");
			String storeType = fc.utobj().generateTestData("TestS");
			String franchiseId = fc.utobj().generateTestData("TestFID");
			AdminFranchiseLocationAddFranchiseLocationPageTest franchise = new AdminFranchiseLocationAddFranchiseLocationPageTest();
			franchiseId = franchise.addFranchiseLocation_All(driver, franchiseId, regionName, storeType);

			fc.utobj().printTestStep("Add Private Visit Form");
			AdminFieldOpsManageVisitFormPageTest visitFormPage = new AdminFieldOpsManageVisitFormPageTest();
			String visitFormName = visitFormPage.addVisitFormTabViewPrivateYes(driver, config, dataSet);

			fc.utobj().printTestStep("Add First Tab");
			String tabName1 = fc.utobj().generateTestData(dataSet.get("tabName1"));
			addTab(driver, config, tabName1);

			fc.utobj().printTestStep("Add Second Tab");
			String tabName2 = fc.utobj().generateTestData(dataSet.get("tabName2"));
			addTab(driver, config, tabName2);

			fc.utobj().printTestStep("Add First Section");
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//a[.='" + tabName1 + "']"));

			String section1 = fc.utobj().generateTestData(dataSet.get("sectionName1"));
			addSection(driver, config, section1);

			fc.utobj().printTestStep("Add new smart question under section1 in tab1 without any score.");
			String questionText1 = dataSet.get("questionText1");
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//td[contains(text () , '" + section1 + "')]/a/span[.='Add Question']"));
			//addSmartTypeQuestionQuestion(driver, config, dataSet, questionText1);

			fc.utobj().printTestStep("Add Second Section");
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//a[.='" + tabName2 + "']"));
			String section2 = fc.utobj().generateTestData(dataSet.get("sectionName2"));
			addSection(driver, config, section2);

			fc.utobj().printTestStep("Now add new simple scorable question under section2 in tab2.");
			String questionText2 = dataSet.get("questionText2");
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//td[contains(text () , '" + section2 + "')]/a/span[.='Add Question']"));

			addQuestionSingleSelectDropDownType(driver, config, dataSet, questionText2);

			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//input[@value='Finish']"));

			fc.utobj().printTestStep("Create Visit Form");
			fc.fieldops().fieldops_common().fieldopsVisit(driver);

			fc.utobj().clickElement(driver, pobj.createVisit);

			// select visit Form
			fc.utobj().clickElement(driver, pobj.selectClick);
			fc.utobj().sendKeys(driver, pobj.searchBox, visitFormName);
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver,
							".//*[@id='ms-parentmasterVisitId']/div/ul/li/label[contains(text () ," + "'"
									+ visitFormName + "'" + ")]/input"));

			// select FranchiseID
			fc.utobj().clickElement(driver, pobj.franchiseeSelect);
			fc.utobj().sendKeys(driver, pobj.searchBoxFranchisee, franchiseId);
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver,
							".//*[@id='ms-parentfranchiseeNo']/div/ul/li/label[contains(text () , " + "'" + franchiseId
									+ "'" + ")]/input"));
			fc.utobj().clickElement(driver, pobj.franchiseeSelect);

			// Select Consultant
			fc.utobj().clickElement(driver, pobj.selectConsultant);
			fc.utobj().sendKeys(driver, pobj.consultantSearchBox, corpUser.getuserFullName());
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver,
							".//*[@id='ms-parentassignTo']/div/ul/li/label[contains(text () , " + "'"
									+ corpUser.getuserFullName() + "'" + ")]/input"));

			// schedule Date
			String curDate = fc.utobj().getCurrentDateUSFormat();

			fc.utobj().sendKeys(driver, pobj.scheduleDateTime, curDate);
			fc.utobj().sendKeys(driver, pobj.comments, "Test Comment");
			fc.utobj().clickElement(driver, pobj.srartNow);

			fc.utobj().printTestStep("Start Visit And Submit Response");

			// Click FirstTab
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//a[.='" + tabName1 + "']"));
			String curDate1 = fc.utobj().getCurrentDateUSFormat();

			tabName1 = tabName1.toLowerCase();
			fc.utobj().sendKeys(driver,
					fc.utobj().getElementByXpath(driver,
							".//table[contains(@id , '" + tabName1 + "')]//tr[@data-role='questionRow']//input"),
					curDate1);

			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//a[.='" + tabName2 + "']"));
			tabName2 = tabName2.toLowerCase();
			fc.utobj().selectDropDownByIndex(driver,
					fc.utobj().getElementByXpath(driver,
							".//table[contains(@id , '" + tabName2 + "')]//tr[@data-role='questionRow']//td/select"),
					1);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='nextSubmitAllButton']"));

			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='nextButton']"));
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='submitButton']"));

			String scoreOfConsultant = fc.utobj().getText(driver,
					fc.utobj().getElementByXpath(driver, ".//td[contains(text () , 'Score')]/following-sibling::td"));

			fc.utobj().printTestStep("Modify The Visit");

			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//input[@value='Modify']"));
			System.out.println(scoreOfConsultant);

			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//input[@name='button']"));
			fc.utobj().switchFrameToDefault(driver);

			String finalScore = fc.utobj().getText(driver,
					fc.utobj().getElementByXpath(driver, ".//*[@id='progressbar']/div[2]"));
			System.out.println(finalScore);

			fc.utobj().printTestStep("Verify The Final Score");

			if (finalScore.contains(scoreOfConsultant)) {
				fc.utobj().throwsException("was not able to Reset the Score ");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	public String addTab(WebDriver driver, Map<String, String> config, String tabName) throws Exception {

		String testCaseId = "TC_Add_Tab_Visit_Form";

		if (fc.utobj().validate(testCaseId)) {
			try {
				AdminFieldOpsManageVisitFormCustomizeVisitFormPage pobj = new AdminFieldOpsManageVisitFormCustomizeVisitFormPage(driver);
				fc.utobj().clickElement(driver, pobj.addTab);
				
				fc.commonMethods().switch_cboxIframe_frameId(driver);
				fc.utobj().sendKeys(driver, pobj.tabName, tabName);
				fc.utobj().clickElement(driver, pobj.addBtn);
				fc.utobj().switchFrameToDefault(driver);
				
				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException("was not able to add Tab :: Admin > Field Ops > Manage Visit Form");

		}
		return tabName;
	}

	public String addSection(WebDriver driver, Map<String, String> config, String sectionName) throws Exception {

		String testCaseId = "TC_Add_Section_Visit_Form";

		if (fc.utobj().validate(testCaseId)) {
			try {
				AdminFieldOpsManageVisitFormCustomizeVisitFormPage pobj = new AdminFieldOpsManageVisitFormCustomizeVisitFormPage(
						driver);
				fc.utobj().clickElement(driver, pobj.addSection);
				fc.commonMethods().switch_cboxIframe_frameId(driver);
				fc.utobj().sendKeys(driver, pobj.sectionName, sectionName);
				fc.utobj().clickElement(driver, pobj.saveBtn);
				fc.utobj().switchFrameToDefault(driver);

				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException("was not abe to add Visit Form :: Admin > FieldOps  > Manage Visit Form");

		}
		return sectionName;
	}

	public String addSmartTypeQuestion(WebDriver driver, Map<String, String> config,
			Map<String, String> dataSet, String questionText) throws Exception {

		String testCaseId = "TC_Add_Question_Visit_Form";

		if (fc.utobj().validate(testCaseId)) {
			try {
				AdminFieldOpsManageVisitFormCustomizeVisitFormPage pobj = new AdminFieldOpsManageVisitFormCustomizeVisitFormPage(
						driver);

				fc.commonMethods().switch_cboxIframe_frameId(driver);
				if (dataSet.get("questionTypeNewOrLibrary").equalsIgnoreCase("New Question")) {
					List<WebElement> list = pobj.addQuestionNewOrLibraray;
					if (fc.utobj().isSelected(driver,list.get(1))) {
						fc.utobj().clickElement(driver, list.get(0));
					} else {
						System.out.println("Do nothing");
					}
				}

				fc.utobj().sendKeys(driver, pobj.questionTextArea, questionText);
				fc.utobj().sendKeys(driver, pobj.helpTextForUser, dataSet.get("helpForUserText"));
				fc.utobj().clickElement(driver, pobj.questionMandantory);
				fc.utobj().selectDropDown(driver, pobj.criticalLevel, dataSet.get("criticalLevel"));
				fc.utobj().clickElement(driver, pobj.nextBtn);

				// add Smart Type Question
				fc.utobj().selectDropDown(driver, pobj.selectResponseType, "Smart Question");

				// FiledOps Type Question

				WebElement element = fc.utobj().getElementByXpath(driver, ".//*[@id='ms-parentmoduleName']");
				fc.utobj().clickElement(driver, element);
				WebElement elementInput = element.findElement(By.xpath("./div/div/input"));
				String value = "Field Ops";
				fc.utobj().sendKeys(driver, elementInput, value);
				WebElement element3 = element.findElement(By.xpath(".//label[contains(text(),'" + value + "')]/input"));
				fc.utobj().clickElement(driver, element3);

				// select field

				WebElement element1 = fc.utobj().getElementByXpath(driver, ".//*[@id='ms-parentmoduleField']");
				fc.utobj().clickElement(driver, element1);
				WebElement elementInput1 = element1.findElement(By.xpath("./div/div/input"));
				String value1 = "Date of last visit logged";
				fc.utobj().sendKeys(driver, elementInput1, value1);
				WebElement element4 = element1
						.findElement(By.xpath(".//label[contains(text(),'" + value1 + "')]/input"));
				fc.utobj().clickElement(driver, element4);

				fc.utobj().clickElement(driver, pobj.addBtn);

				fc.utobj().switchFrameToDefault(driver);

				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException(
					"was not able to add Question In visit Form :: Admin > Field Ops > Manage visit Form");

		}
		return questionText;
	}

	public String addQuestionSingleSelectDropDownType(WebDriver driver, Map<String, String> config,
			Map<String, String> dataSet, String questionText) throws Exception {

		String testCaseId = "TC_Add_Question_Single_Select_DropDown";

		if (fc.utobj().validate(testCaseId)) {
			try {
				AdminFieldOpsQuestionLibraryPage pobj = new AdminFieldOpsQuestionLibraryPage(driver);
				fc.commonMethods().switch_cboxIframe_frameId(driver);
				fc.utobj().sendKeys(driver, pobj.questionTextArea, questionText);
				fc.utobj().sendKeys(driver, pobj.helpTextForUser, dataSet.get("helpForUserText"));
				fc.utobj().clickElement(driver, pobj.questionMandantory);
				fc.utobj().selectDropDown(driver, pobj.criticalLevel, dataSet.get("criticalLevel"));
				fc.utobj().clickElement(driver, pobj.nextBtn);

				fc.utobj().selectDropDown(driver, pobj.selectResponseType, "Single Select (Drop Down)");
				// fc.utobj().clickElement(driver, pobj.allowUserComments);
				fc.utobj().clickElement(driver, pobj.scoreInclude);
				fc.utobj().clickElement(driver, pobj.excludeNonResponse);
				fc.utobj().clickElement(driver, pobj.notApplicableResponse);
				fc.utobj().clickElement(driver, pobj.complianceNotApplicable);
				fc.utobj().sendKeys(driver, pobj.firstChoice, dataSet.get("firstChoice"));
				fc.utobj().sendKeys(driver, pobj.secondChoice, dataSet.get("secondChoice"));
				fc.utobj().sendKeys(driver, pobj.firstScore, dataSet.get("firstScore"));
				fc.utobj().sendKeys(driver, pobj.secondScore, dataSet.get("secondScore"));
				fc.utobj().clickElement(driver, pobj.saveBtn);
				fc.utobj().switchFrameToDefault(driver);
				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException("Was not able to add Question Single Select DropDown Type");

		}
		return questionText;
	}

	// Add Private Form

	@Test(groups = "TestDummay")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Verify The Add , Modify And Copy Private Visit Form", testCaseId = "TC_Test_Visit")
	private void addVisitFormTest() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData(testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			AdminFieldOpsManageVisitFormCustomizeVisitFormPage pobj = new AdminFieldOpsManageVisitFormCustomizeVisitFormPage(
					driver);

			fc.utobj().printTestStep("Navigate To Admin > Fieldops > Manage Visit Form");
			fc.utobj().printTestStep("Add Private Visit Form");
			AdminFieldOpsManageVisitFormPageTest visitForm = new AdminFieldOpsManageVisitFormPageTest();
			visitForm.addVisitFormPrivate(driver, dataSet);

			/*
			 * //Add Tab fc.utobj().printTestStep(testCaseId, "Add First Tab");
			 * String
			 * tabName1=fc.utobj().generateTestData(dataSet.get("tabName1"));
			 * addTab(driver, config, tabName1); fc.utobj().clickElement(driver,
			 * fc.utobj().getElementByXpath(driver,
			 * ".//*[@id='addquestionlink']/span"))) ;
			 */

			fc.utobj().printTestStep("Add Paragraph Type Question in Visit Form");
			// fc.utobj().clickElement(driver, pobj.addQuestion);

			// Add Section

			String section1 = fc.utobj().generateTestData(dataSet.get("sectionName1"));
			addSection(driver, config, section1);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//td/strong[contains(text () , '" + section1 + "')]/../a[@id='addqueslink']"));

			fc.commonMethods().switch_cboxIframe_frameId(driver);

			List<WebElement> list = pobj.addQuestionNewOrLibraray;
			if (fc.utobj().isSelected(driver,list.get(1))) {
				fc.utobj().clickElement(driver, list.get(0));
			} else {

			}
			String questionTextForParagraph = fc.utobj().generateTestData(dataSet.get("questionTextForParagraph"));
			fc.utobj().sendKeys(driver, pobj.questionTextArea, questionTextForParagraph);

			fc.utobj().selectDropDownByValue(driver, pobj.uploadType, "op1");
			String filePath = FranconnectUtil.config.get("inputDirectory") + "\\testData\\document\\"
					+ dataSet.get("fileName");

			fc.utobj().sendKeys(driver, pobj.uploadAttachmentPath, filePath);
			fc.utobj().selectDropDown(driver, pobj.criticalLevel, dataSet.get("criticalLevel"));
			fc.utobj().clickElement(driver, pobj.nextBtn);

			fc.utobj().selectDropDown(driver, pobj.selectResponseType, "Paragraph Text");

			fc.utobj().clickElement(driver, pobj.addBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Add Single Line Text Type Question In Visit Form");
			// fc.utobj().clickElement(driver, pobj.addQuestion);

			// fc.utobj().printTestStep(testCaseId, "Add First Tab");
			// String
			// tabName2=fc.utobj().generateTestData(dataSet.get("tabName2"));
			// addTab(driver, config, tabName2);
			// fc.utobj().clickElement(driver,
			// fc.utobj().getElementByXpath(driver,".//*[@id='addquestionlink']/span")));

			// Add Section

			String section2 = fc.utobj().generateTestData(dataSet.get("sectionName2"));
			addSection(driver, config, section2);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//td/strong[contains(text () , '" + section2 + "')]/../a[@id='addqueslink']"));

			fc.commonMethods().switch_cboxIframe_frameId(driver);

			String questionTextForSingleLineText = fc.utobj()
					.generateTestData(dataSet.get("questionTextForSingleLineText"));
			fc.utobj().sendKeys(driver, pobj.questionTextArea, questionTextForSingleLineText);

			fc.utobj().selectDropDown(driver, pobj.criticalLevel, dataSet.get("criticalLevel"));
			fc.utobj().clickElement(driver, pobj.nextBtn);

			fc.utobj().selectDropDown(driver, pobj.selectResponseType, "Single Line Text");

			fc.utobj().clickElement(driver, pobj.addBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Add Date Type Question In Visit Form");
			// fc.utobj().clickElement(driver, pobj.addQuestion);

			/*
			 * fc.utobj().printTestStep(testCaseId, "Add First Tab"); String
			 * tabName3=fc.utobj().generateTestData(dataSet.get("tabName3"));
			 * addTab(driver, config, tabName3); fc.utobj().clickElement(driver,
			 * fc.utobj().getElementByXpath(driver,
			 * ".//*[@id='addquestionlink']/span"))) ;
			 */

			// Add Section

			String section4 = fc.utobj().generateTestData(dataSet.get("sectionName4"));
			addSection(driver, config, section4);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//td/strong[contains(text () , '" + section4 + "')]/../a[@id='addqueslink']"));

			fc.commonMethods().switch_cboxIframe_frameId(driver);

			String questionTextForDate = fc.utobj().generateTestData(dataSet.get("questionTextForDate"));
			fc.utobj().sendKeys(driver, pobj.questionTextArea, questionTextForDate);

			fc.utobj().selectDropDown(driver, pobj.criticalLevel, dataSet.get("criticalLevel"));
			fc.utobj().clickElement(driver, pobj.nextBtn);

			fc.utobj().selectDropDown(driver, pobj.selectResponseType, "Date");

			fc.utobj().clickElement(driver, pobj.addBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Add Rating Type Question In Visit Form");
			// fc.utobj().clickElement(driver, pobj.addQuestion);

			/*
			 * fc.utobj().printTestStep(testCaseId, "Add First Tab"); String
			 * tabName4=fc.utobj().generateTestData(dataSet.get("tabName4"));
			 * addTab(driver, config, tabName4); fc.utobj().clickElement(driver,
			 * fc.utobj().getElementByXpath(driver,
			 * ".//*[@id='addquestionlink']/span"))) ;
			 */

			// Add Section

			String section3 = fc.utobj().generateTestData(dataSet.get("sectionName3"));
			addSection(driver, config, section3);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//td/strong[contains(text () , '" + section3 + "')]/../a[@id='addqueslink']"));

			fc.commonMethods().switch_cboxIframe_frameId(driver);

			String questionTextForRating = fc.utobj().generateTestData(dataSet.get("questionTextForRating"));
			fc.utobj().sendKeys(driver, pobj.questionTextArea, questionTextForRating);

			fc.utobj().selectDropDown(driver, pobj.criticalLevel, dataSet.get("criticalLevel"));
			fc.utobj().clickElement(driver, pobj.nextBtn);

			fc.utobj().selectDropDown(driver, pobj.selectResponseType, "Rating");

			fc.utobj().clickElement(driver, pobj.scoreInclude);
			fc.utobj().clickElement(driver, pobj.excludeNonResponse);
			fc.utobj().clickElement(driver, pobj.notApplicableResponse);
			fc.utobj().clickElement(driver, pobj.complianceNotApplicable);
			fc.utobj().sendKeys(driver, pobj.minRange, dataSet.get("minRange"));
			fc.utobj().sendKeys(driver, pobj.maxRange, dataSet.get("maxRange"));
			fc.utobj().clickElement(driver, pobj.addBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Add Number Type Question In Visit Form");
			// fc.utobj().clickElement(driver, pobj.addQuestion);

			/*
			 * fc.utobj().printTestStep(testCaseId, "Add First Tab"); String
			 * tabName5=fc.utobj().generateTestData(dataSet.get("tabName5"));
			 * addTab(driver, config, tabName5); fc.utobj().clickElement(driver,
			 * fc.utobj().getElementByXpath(driver,
			 * ".//*[@id='addquestionlink']/span"))) ;
			 */

			// Add Section

			String section5 = fc.utobj().generateTestData(dataSet.get("sectionName5"));
			addSection(driver, config, section5);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//td/strong[contains(text () , '" + section5 + "')]/../a[@id='addqueslink']"));

			fc.commonMethods().switch_cboxIframe_frameId(driver);

			String questionTextForNumber = fc.utobj().generateTestData(dataSet.get("questionTextForNumber"));
			fc.utobj().sendKeys(driver, pobj.questionTextArea, questionTextForNumber);

			fc.utobj().selectDropDown(driver, pobj.criticalLevel, dataSet.get("criticalLevel"));
			fc.utobj().clickElement(driver, pobj.nextBtn);

			fc.utobj().selectDropDown(driver, pobj.selectResponseType, "Number");
			fc.utobj().clickElement(driver, pobj.scoreInclude);
			fc.utobj().clickElement(driver, pobj.excludeNonResponse);
			fc.utobj().clickElement(driver, pobj.notApplicableResponse);

			fc.utobj().sendKeys(driver, pobj.rangeTo, dataSet.get("rangeTo"));
			fc.utobj().sendKeys(driver, pobj.rangeScore, dataSet.get("rangeScore"));
			fc.utobj().clickElement(driver, pobj.addBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Add Single Select Radio Button Type Question In Visit Form");
			// fc.utobj().clickElement(driver, pobj.addQuestion);

			/*
			 * fc.utobj().printTestStep(testCaseId, "Add First Tab"); String
			 * tabName6=fc.utobj().generateTestData(dataSet.get("tabName6"));
			 * addTab(driver, config, tabName6); fc.utobj().clickElement(driver,
			 * fc.utobj().getElementByXpath(driver,
			 * ".//*[@id='addquestionlink']/span"))) ;
			 */

			// Add Section

			String section6 = fc.utobj().generateTestData(dataSet.get("sectionName2"));
			addSection(driver, config, section6);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//td/strong[contains(text () , '" + section6 + "')]/../a[@id='addqueslink']"));

			fc.commonMethods().switch_cboxIframe_frameId(driver);

			String questionTextForSingleRadioBtn = fc.utobj()
					.generateTestData(dataSet.get("questionTextForSingleRadioBtn"));
			fc.utobj().sendKeys(driver, pobj.questionTextArea, questionTextForSingleRadioBtn);

			fc.utobj().selectDropDown(driver, pobj.criticalLevel, dataSet.get("criticalLevel"));
			fc.utobj().clickElement(driver, pobj.nextBtn);

			AdminFieldOpsQuestionLibraryPage question_Library = new AdminFieldOpsQuestionLibraryPage(driver);
			fc.utobj().selectDropDownByValue(driver, pobj.selectResponseType, "radio");
			fc.utobj().clickElement(driver, pobj.scoreInclude);
			fc.utobj().clickElement(driver, pobj.excludeNonResponse);
			fc.utobj().clickElement(driver, pobj.complianceNotApplicable);
			fc.utobj().sendKeys(driver, question_Library.firstChoice, dataSet.get("firstChoice"));
			fc.utobj().sendKeys(driver, question_Library.secondChoice, dataSet.get("secondChoice"));
			fc.utobj().sendKeys(driver, question_Library.firstScore, dataSet.get("firstScore"));
			fc.utobj().sendKeys(driver, question_Library.secondScore, dataSet.get("secondScore"));
			fc.utobj().clickElement(driver, pobj.addBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Add Single Select Drop Down Type Question In Visit Form");
			// fc.utobj().clickElement(driver, pobj.addQuestion);

			/*
			 * fc.utobj().printTestStep(testCaseId, "Add First Tab"); String
			 * tabName7=fc.utobj().generateTestData(dataSet.get("tabName7"));
			 * addTab(driver, config, tabName7); fc.utobj().clickElement(driver,
			 * fc.utobj().getElementByXpath(driver,
			 * ".//*[@id='addquestionlink']/span"))) ;
			 */

			// Add Section

			String section7 = fc.utobj().generateTestData(dataSet.get("sectionName2"));
			addSection(driver, config, section7);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//td/strong[contains(text () , '" + section7 + "')]/../a[@id='addqueslink']"));

			fc.commonMethods().switch_cboxIframe_frameId(driver);

			String questionTextForSingleSelectDropDown = fc.utobj()
					.generateTestData(dataSet.get("questionTextForSingleSelectDropDown"));
			fc.utobj().sendKeys(driver, pobj.questionTextArea, questionTextForSingleSelectDropDown);

			fc.utobj().selectDropDown(driver, pobj.criticalLevel, dataSet.get("criticalLevel"));
			fc.utobj().clickElement(driver, pobj.nextBtn);

			fc.utobj().selectDropDownByValue(driver, pobj.selectResponseType, "combo");
			fc.utobj().clickElement(driver, pobj.scoreInclude);
			fc.utobj().clickElement(driver, pobj.excludeNonResponse);
			fc.utobj().clickElement(driver, pobj.notApplicableResponse);
			fc.utobj().clickElement(driver, pobj.complianceNotApplicable);
			fc.utobj().sendKeys(driver, question_Library.firstChoice, dataSet.get("firstChoice"));
			fc.utobj().sendKeys(driver, question_Library.secondChoice, dataSet.get("secondChoice"));
			fc.utobj().sendKeys(driver, question_Library.firstScore, dataSet.get("firstScore"));
			fc.utobj().sendKeys(driver, question_Library.secondScore, dataSet.get("secondScore"));
			fc.utobj().clickElement(driver, pobj.addBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Add MultiSelect CheckBox Type Question In Visit Form");
			// fc.utobj().clickElement(driver, pobj.addQuestion);

			/*
			 * fc.utobj().printTestStep(testCaseId, "Add First Tab"); String
			 * tabName8=fc.utobj().generateTestData(dataSet.get("tabName8"));
			 * addTab(driver, config, tabName8); fc.utobj().clickElement(driver,
			 * fc.utobj().getElementByXpath(driver,
			 * ".//*[@id='addquestionlink']/span"))) ;
			 */

			// Add Section

			String section8 = fc.utobj().generateTestData(dataSet.get("sectionName2"));
			addSection(driver, config, section8);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//td/strong[contains(text () , '" + section8 + "')]/../a[@id='addqueslink']"));

			fc.commonMethods().switch_cboxIframe_frameId(driver);

			String questionTextForMultiSelectCheckBox = fc.utobj()
					.generateTestData(dataSet.get("questionTextForMultiSelectCheckBox"));
			fc.utobj().sendKeys(driver, pobj.questionTextArea, questionTextForMultiSelectCheckBox);

			fc.utobj().selectDropDown(driver, pobj.criticalLevel, dataSet.get("criticalLevel"));
			fc.utobj().clickElement(driver, pobj.nextBtn);

			fc.utobj().selectDropDownByValue(driver, pobj.selectResponseType, "checkbox");
			fc.utobj().clickElement(driver, pobj.scoreInclude);
			fc.utobj().clickElement(driver, pobj.excludeNonResponse);
			fc.utobj().clickElement(driver, pobj.notApplicableResponse);
			fc.utobj().clickElement(driver, pobj.complianceNotApplicable);
			fc.utobj().sendKeys(driver, question_Library.firstChoice, dataSet.get("firstChoice"));
			fc.utobj().sendKeys(driver, question_Library.secondChoice, dataSet.get("secondChoice"));
			fc.utobj().sendKeys(driver, question_Library.firstScore, dataSet.get("firstScore"));
			fc.utobj().sendKeys(driver, question_Library.secondScore, dataSet.get("secondScore"));
			fc.utobj().clickElement(driver, pobj.addBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Add Smart Type Question In Visit Form");
			// fc.utobj().clickElement(driver, pobj.addQuestion);

			/*
			 * fc.utobj().printTestStep(testCaseId, "Add First Tab"); String
			 * tabName9=fc.utobj().generateTestData(dataSet.get("tabName9"));
			 * addTab(driver, config, tabName9); fc.utobj().clickElement(driver,
			 * fc.utobj().getElementByXpath(driver,
			 * ".//*[@id='addquestionlink']/span"))) ;
			 */

			// Add Section

			String section9 = fc.utobj().generateTestData(dataSet.get("sectionName2"));
			addSection(driver, config, section9);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//td/strong[contains(text () , '" + section9 + "')]/../a[@id='addqueslink']"));

			fc.commonMethods().switch_cboxIframe_frameId(driver);

			String questionTextForSmart = fc.utobj().generateTestData(dataSet.get("questionTextForSmart"));
			fc.utobj().sendKeys(driver, pobj.questionTextArea, questionTextForSmart);

			fc.utobj().selectDropDown(driver, pobj.criticalLevel, dataSet.get("criticalLevel"));
			fc.utobj().clickElement(driver, pobj.nextBtn);

			// add Smart Type Question
			fc.utobj().selectDropDown(driver, pobj.selectResponseType, "Smart Question");
			// FiledOps Type Question
			WebElement element = fc.utobj().getElementByXpath(driver, ".//*[@id='ms-parentmoduleName']");
			fc.utobj().clickElement(driver, element);
			WebElement elementInput = element.findElement(By.xpath("./div/div/input"));
			String value = "Field Ops";
			fc.utobj().sendKeys(driver, elementInput, value);
			WebElement element3 = element.findElement(By.xpath(".//label[contains(text(),'" + value + "')]/input"));
			fc.utobj().clickElement(driver, element3);

			// select field

			WebElement element1 = fc.utobj().getElementByXpath(driver, ".//*[@id='ms-parentmoduleField']");
			fc.utobj().clickElement(driver, element1);
			WebElement elementInput1 = element1.findElement(By.xpath("./div/div/input"));
			String value1 = "Date of last visit logged";
			fc.utobj().sendKeys(driver, elementInput1, value1);
			WebElement element4 = element1.findElement(By.xpath(".//label[contains(text(),'" + value1 + "')]/input"));
			fc.utobj().clickElement(driver, element4);

			fc.utobj().clickElement(driver, pobj.addBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Add Multiple Input Type Question In Visit Form");
			// fc.utobj().clickElement(driver, pobj.addQuestion);

			/*
			 * fc.utobj().printTestStep(testCaseId, "Add First Tab"); String
			 * tabName10=fc.utobj().generateTestData(dataSet.get("tabName10"));
			 * addTab(driver, config, tabName10);
			 * fc.utobj().clickElement(driver,
			 * fc.utobj().getElementByXpath(driver,
			 * ".//*[@id='addquestionlink']/span"))) ;
			 */

			// Add Section

			String section10 = fc.utobj().generateTestData(dataSet.get("sectionName10"));
			addSection(driver, config, section10);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//td/strong[contains(text () , '" + section10 + "')]/../a[@id='addqueslink']"));

			fc.commonMethods().switch_cboxIframe_frameId(driver);

			String questionTextForMultiInput = fc.utobj().generateTestData(dataSet.get("questionTextForMultiInput"));
			fc.utobj().sendKeys(driver, pobj.questionTextArea, questionTextForMultiInput);

			fc.utobj().selectDropDown(driver, pobj.criticalLevel, dataSet.get("criticalLevel"));
			fc.utobj().clickElement(driver, pobj.nextBtn);

			fc.utobj().selectDropDownByValue(driver, pobj.selectResponseType, "multiinput");
			fc.utobj().clickElement(driver, pobj.scoreInclude);
			fc.utobj().clickElement(driver, pobj.excludeNonResponse);
			fc.utobj().clickElement(driver, pobj.notApplicableResponse);
			fc.utobj().clickElement(driver, pobj.complianceNotApplicable);
			fc.utobj().sendKeys(driver, pobj.inputLabels1, "Input Label 1");
			fc.utobj().sendKeys(driver, pobj.inputLabels2, "Input Label 2");
			fc.utobj().sendKeys(driver, pobj.calculationResultLabel, "Calculation Result Label");
			fc.utobj().sendKeys(driver, pobj.rangeScore, "100");
			fc.utobj().clickElement(driver, pobj.addBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	/*
	 * testCaseId = "TC_19_Add_Tab_Visit_Form"
	 * 
	 * testCaseId = "TC_Add_All_Type_Questions_In_Visit_Form_142"
	 * 
	 * 
	 * 
	 */

	@Test(groups = "addVisitForm")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Add Visit Form With 50 Tab 1000 Question In It", testCaseId = "TC_Create_Visit_Form_Test")
	public void createVisitForm() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData(testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			AdminFieldOpsManageVisitFormCustomizeVisitFormPage pobj = new AdminFieldOpsManageVisitFormCustomizeVisitFormPage(
					driver);
			AdminFieldOpsManageVisitFormCustomizeVisitFormPageTest customize_Visit_Form = new AdminFieldOpsManageVisitFormCustomizeVisitFormPageTest();
			AdminFieldOpsQuestionLibraryPage question_Library = new AdminFieldOpsQuestionLibraryPage(driver);

			fc.utobj().printTestStep("Navigate To Admin > FieldOps > Manage Visit Form Page");
			fc.utobj().printTestStep("Add Private Visit Form With Tab View");

			AdminFieldOpsManageVisitFormPageTest visitForm = new AdminFieldOpsManageVisitFormPageTest();
			visitForm.addVisitFormTabViewPrivateYes(driver, config, dataSet);

			for (int i = 0; i < 100; i++) {

				fc.utobj().printTestStep("Add Tab");
				customize_Visit_Form.addTab(driver, config, dataSet);

				for (int j = 0; j < 1; j++) {
					fc.utobj().printTestStep("Add Paragraph Type Question In First Tab");
					// fc.utobj().clickElement(driver,
					// fc.utobj().getElementByXpath(driver,".//a[contains(text
					// () ,
					// '"+tabName+"')]")));
					// fc.utobj().clickElement(driver, pobj.addQuestion);
					fc.utobj().getElement(driver, pobj.addQuestion).click();
					fc.commonMethods().switch_cboxIframe_frameId(driver);

					List<WebElement> list = pobj.addQuestionNewOrLibraray;
					if (fc.utobj().isSelected(driver,list.get(1))) {
						fc.utobj().clickElement(driver, list.get(0));
					} else {

					}
					String questionTextForParagraph = fc.utobj()
							.generateTestData(dataSet.get("questionTextForParagraph"));
					fc.utobj().sendKeys(driver, pobj.questionTextArea, questionTextForParagraph);

					// add Attachement
					fc.utobj().selectDropDownByValue(driver, pobj.uploadType, "op1");
					String filePath = fc.utobj().getFilePathFromTestData(dataSet.get("fileName"));

					fc.utobj().sendKeys(driver, pobj.uploadAttachmentPath, filePath);
					fc.utobj().selectDropDown(driver, pobj.criticalLevel, dataSet.get("criticalLevel"));
					fc.utobj().clickElement(driver, pobj.nextBtn);

					fc.utobj().selectDropDown(driver, pobj.selectResponseType, "Paragraph Text");

					fc.utobj().clickElement(driver, pobj.addBtn);
					fc.utobj().switchFrameToDefault(driver);
				}

				for (int j = 0; j < 1; j++) {
					fc.utobj().printTestStep("Add Single Line Text Type Question In Visit Form");
					// fc.utobj().clickElement(driver,
					// fc.utobj().getElementByXpath(driver,".//a[contains(text
					// () ,
					// '"+tabName+"')]")));
					// fc.utobj().clickElement(driver, pobj.addQuestion);
					fc.utobj().getElement(driver, pobj.addQuestion).click();
					fc.commonMethods().switch_cboxIframe_frameId(driver);

					String questionTextForSingleLineText = fc.utobj()
							.generateTestData(dataSet.get("questionTextForSingleLineText"));
					fc.utobj().sendKeys(driver, pobj.questionTextArea, questionTextForSingleLineText);

					fc.utobj().selectDropDown(driver, pobj.criticalLevel, dataSet.get("criticalLevel"));
					fc.utobj().clickElement(driver, pobj.nextBtn);

					fc.utobj().selectDropDown(driver, pobj.selectResponseType, "Single Line Text");

					fc.utobj().clickElement(driver, pobj.addBtn);
					fc.utobj().switchFrameToDefault(driver);
				}

				for (int j = 0; j < 1; j++) {
					fc.utobj().printTestStep("Add Date Type Question In Visit Form");
					// fc.utobj().clickElement(driver,
					// fc.utobj().getElementByXpath(driver,".//a[contains(text
					// () ,
					// '"+tabName+"')]")));
					/* fc.utobj().clickElement(driver, pobj.addQuestion); */
					fc.utobj().getElement(driver, pobj.addQuestion).click();
					fc.commonMethods().switch_cboxIframe_frameId(driver);

					String questionTextForDate = fc.utobj().generateTestData(dataSet.get("questionTextForDate"));
					fc.utobj().sendKeys(driver, pobj.questionTextArea, questionTextForDate);

					fc.utobj().selectDropDown(driver, pobj.criticalLevel, dataSet.get("criticalLevel"));
					fc.utobj().clickElement(driver, pobj.nextBtn);

					fc.utobj().selectDropDown(driver, pobj.selectResponseType, "Date");

					fc.utobj().clickElement(driver, pobj.addBtn);
					fc.utobj().switchFrameToDefault(driver);
				}

				for (int j = 0; j < 1; j++) {

					fc.utobj().printTestStep("Add Rating Type Question In Visit Form");
					// fc.utobj().clickElement(driver,
					// fc.utobj().getElementByXpath(driver,".//a[contains(text
					// () ,
					// '"+tabName+"')]")));
					/* fc.utobj().clickElement(driver, pobj.addQuestion); */
					fc.utobj().getElement(driver, pobj.addQuestion).click();
					fc.commonMethods().switch_cboxIframe_frameId(driver);

					String questionTextForRating = fc.utobj().generateTestData(dataSet.get("questionTextForRating"));
					fc.utobj().sendKeys(driver, pobj.questionTextArea, questionTextForRating);

					fc.utobj().selectDropDown(driver, pobj.criticalLevel, dataSet.get("criticalLevel"));
					fc.utobj().clickElement(driver, pobj.nextBtn);

					fc.utobj().selectDropDown(driver, pobj.selectResponseType, "Rating");

					fc.utobj().clickElement(driver, pobj.scoreInclude);
					fc.utobj().clickElement(driver, pobj.excludeNonResponse);
					fc.utobj().clickElement(driver, pobj.notApplicableResponse);
					fc.utobj().clickElement(driver, pobj.complianceNotApplicable);
					fc.utobj().sendKeys(driver, pobj.minRange, dataSet.get("minRange"));
					fc.utobj().sendKeys(driver, pobj.maxRange, dataSet.get("maxRange"));
					fc.utobj().clickElement(driver, pobj.addBtn);
					fc.utobj().switchFrameToDefault(driver);
				}

				for (int j = 0; j < 1; j++) {

					fc.utobj().printTestStep("Add Number Type Question In Visit Form");
					// fc.utobj().clickElement(driver,
					// fc.utobj().getElementByXpath(driver,".//a[contains(text
					// () ,
					// '"+tabName+"')]")));
					/* fc.utobj().clickElement(driver, pobj.addQuestion); */
					fc.utobj().getElement(driver, pobj.addQuestion).click();
					fc.commonMethods().switch_cboxIframe_frameId(driver);

					String questionTextForNumber = fc.utobj().generateTestData(dataSet.get("questionTextForNumber"));
					fc.utobj().sendKeys(driver, pobj.questionTextArea, questionTextForNumber);

					fc.utobj().selectDropDown(driver, pobj.criticalLevel, dataSet.get("criticalLevel"));
					fc.utobj().clickElement(driver, pobj.nextBtn);

					fc.utobj().selectDropDown(driver, pobj.selectResponseType, "Number");
					fc.utobj().clickElement(driver, pobj.scoreInclude);
					fc.utobj().clickElement(driver, pobj.excludeNonResponse);
					fc.utobj().clickElement(driver, pobj.notApplicableResponse);

					fc.utobj().sendKeys(driver, pobj.rangeTo, dataSet.get("rangeTo"));
					fc.utobj().sendKeys(driver, pobj.rangeScore, dataSet.get("rangeScore"));
					fc.utobj().clickElement(driver, pobj.addBtn);
					fc.utobj().switchFrameToDefault(driver);
				}

				for (int j = 0; j < 1; j++) {

					fc.utobj().printTestStep("Add Single Select Radio Button Type Question In Visit Form");
					// fc.utobj().clickElement(driver,
					// fc.utobj().getElementByXpath(driver,".//a[contains(text
					// () ,
					// '"+tabName+"')]")));
					/* fc.utobj().clickElement(driver, pobj.addQuestion); */
					fc.utobj().getElement(driver, pobj.addQuestion).click();
					fc.commonMethods().switch_cboxIframe_frameId(driver);

					String questionTextForSingleRadioBtn = fc.utobj()
							.generateTestData(dataSet.get("questionTextForSingleRadioBtn"));
					fc.utobj().sendKeys(driver, pobj.questionTextArea, questionTextForSingleRadioBtn);

					fc.utobj().selectDropDown(driver, pobj.criticalLevel, dataSet.get("criticalLevel"));
					fc.utobj().clickElement(driver, pobj.nextBtn);

					fc.utobj().selectDropDownByValue(driver, pobj.selectResponseType, "radio");
					fc.utobj().clickElement(driver, pobj.scoreInclude);
					fc.utobj().clickElement(driver, pobj.excludeNonResponse);
					fc.utobj().clickElement(driver, pobj.complianceNotApplicable);
					fc.utobj().sendKeys(driver, question_Library.firstChoice, dataSet.get("firstChoice"));
					fc.utobj().sendKeys(driver, question_Library.secondChoice, dataSet.get("secondChoice"));
					fc.utobj().sendKeys(driver, question_Library.firstScore, dataSet.get("firstScore"));
					fc.utobj().sendKeys(driver, question_Library.secondScore, dataSet.get("secondScore"));
					fc.utobj().clickElement(driver, pobj.addBtn);
					fc.utobj().switchFrameToDefault(driver);

				}

				for (int j = 0; j < 1; j++) {

					fc.utobj().printTestStep("Add Single Select Drop Down Type Question In Visit Form");
					// fc.utobj().clickElement(driver,
					// fc.utobj().getElementByXpath(driver,".//a[contains(text
					// () ,
					// '"+tabName+"')]")));
					/* fc.utobj().clickElement(driver, pobj.addQuestion); */
					fc.utobj().getElement(driver, pobj.addQuestion).click();
					fc.commonMethods().switch_cboxIframe_frameId(driver);

					String questionTextForSingleSelectDropDown = fc.utobj()
							.generateTestData(dataSet.get("questionTextForSingleSelectDropDown"));
					fc.utobj().sendKeys(driver, pobj.questionTextArea, questionTextForSingleSelectDropDown);

					fc.utobj().selectDropDown(driver, pobj.criticalLevel, dataSet.get("criticalLevel"));
					fc.utobj().clickElement(driver, pobj.nextBtn);

					fc.utobj().selectDropDownByValue(driver, pobj.selectResponseType, "combo");
					fc.utobj().clickElement(driver, pobj.scoreInclude);
					fc.utobj().clickElement(driver, pobj.excludeNonResponse);
					fc.utobj().clickElement(driver, pobj.notApplicableResponse);
					fc.utobj().clickElement(driver, pobj.complianceNotApplicable);
					fc.utobj().sendKeys(driver, question_Library.firstChoice, dataSet.get("firstChoice"));
					fc.utobj().sendKeys(driver, question_Library.secondChoice, dataSet.get("secondChoice"));
					fc.utobj().sendKeys(driver, question_Library.firstScore, dataSet.get("firstScore"));
					fc.utobj().sendKeys(driver, question_Library.secondScore, dataSet.get("secondScore"));
					fc.utobj().clickElement(driver, pobj.addBtn);
					fc.utobj().switchFrameToDefault(driver);

				}

				for (int j = 0; j < 1; j++) {
					fc.utobj().printTestStep("Add MultiSelect CheckBox Type Question In Visit Form");
					// fc.utobj().clickElement(driver,
					// fc.utobj().getElementByXpath(driver,".//a[contains(text
					// () ,
					// '"+tabName+"')]")));
					/* fc.utobj().clickElement(driver, pobj.addQuestion); */
					fc.utobj().getElement(driver, pobj.addQuestion).click();
					fc.commonMethods().switch_cboxIframe_frameId(driver);

					String questionTextForMultiSelectCheckBox = fc.utobj()
							.generateTestData(dataSet.get("questionTextForMultiSelectCheckBox"));
					fc.utobj().sendKeys(driver, pobj.questionTextArea, questionTextForMultiSelectCheckBox);

					fc.utobj().selectDropDown(driver, pobj.criticalLevel, dataSet.get("criticalLevel"));
					fc.utobj().clickElement(driver, pobj.nextBtn);

					fc.utobj().selectDropDownByValue(driver, pobj.selectResponseType, "checkbox");
					fc.utobj().clickElement(driver, pobj.scoreInclude);
					fc.utobj().clickElement(driver, pobj.excludeNonResponse);
					fc.utobj().clickElement(driver, pobj.notApplicableResponse);
					fc.utobj().clickElement(driver, pobj.complianceNotApplicable);
					fc.utobj().sendKeys(driver, question_Library.firstChoice, dataSet.get("firstChoice"));
					fc.utobj().sendKeys(driver, question_Library.secondChoice, dataSet.get("secondChoice"));
					fc.utobj().sendKeys(driver, question_Library.firstScore, dataSet.get("firstScore"));
					fc.utobj().sendKeys(driver, question_Library.secondScore, dataSet.get("secondScore"));
					fc.utobj().clickElement(driver, pobj.addBtn);
					fc.utobj().switchFrameToDefault(driver);
				}

				for (int j = 0; j < 1; j++) {
					fc.utobj().printTestStep("Add Smart Type Question In Visit Form");
					// fc.utobj().clickElement(driver,
					// fc.utobj().getElementByXpath(driver,".//a[contains(text
					// () ,
					// '"+tabName+"')]")));
					/* fc.utobj().clickElement(driver, pobj.addQuestion); */
					fc.utobj().getElement(driver, pobj.addQuestion).click();
					fc.commonMethods().switch_cboxIframe_frameId(driver);

					String questionTextForSmart = fc.utobj().generateTestData(dataSet.get("questionTextForSmart"));
					fc.utobj().sendKeys(driver, pobj.questionTextArea, questionTextForSmart);

					fc.utobj().selectDropDown(driver, pobj.criticalLevel, dataSet.get("criticalLevel"));
					fc.utobj().clickElement(driver, pobj.nextBtn);

					// add Smart Type Question
					fc.utobj().selectDropDown(driver, pobj.selectResponseType, "Smart Question");
					// FiledOps Type Question
					WebElement element = fc.utobj().getElementByXpath(driver, ".//*[@id='ms-parentmoduleName']");
					fc.utobj().clickElement(driver, element);
					WebElement elementInput = element.findElement(By.xpath("./div/div/input"));
					String value = "Field Ops";
					fc.utobj().sendKeys(driver, elementInput, value);
					WebElement element3 = element
							.findElement(By.xpath(".//label[contains(text(),'" + value + "')]/input"));
					fc.utobj().clickElement(driver, element3);

					// select field

					WebElement element1 = fc.utobj().getElementByXpath(driver, ".//*[@id='ms-parentmoduleField']");
					fc.utobj().clickElement(driver, element1);
					WebElement elementInput1 = element1.findElement(By.xpath("./div/div/input"));
					String value1 = "Date of last visit logged";
					fc.utobj().sendKeys(driver, elementInput1, value1);
					WebElement element4 = element1
							.findElement(By.xpath(".//label[contains(text(),'" + value1 + "')]/input"));
					fc.utobj().clickElement(driver, element4);

					fc.utobj().clickElement(driver, pobj.addBtn);
					fc.utobj().switchFrameToDefault(driver);
				}

				for (int j = 0; j < 1; j++) {
					fc.utobj().printTestStep("Add Multiple Input Type Question In Visit Form");
					// fc.utobj().clickElement(driver,
					// fc.utobj().getElementByXpath(driver,".//a[contains(text
					// () ,
					// '"+tabName+"')]")));
					/* fc.utobj().clickElement(driver, pobj.addQuestion); */
					fc.utobj().getElement(driver, pobj.addQuestion).click();
					fc.commonMethods().switch_cboxIframe_frameId(driver);

					String questionTextForMultiInput = fc.utobj()
							.generateTestData(dataSet.get("questionTextForMultiInput"));
					fc.utobj().sendKeys(driver, pobj.questionTextArea, questionTextForMultiInput);

					fc.utobj().selectDropDown(driver, pobj.criticalLevel, dataSet.get("criticalLevel"));
					fc.utobj().clickElement(driver, pobj.nextBtn);

					fc.utobj().selectDropDownByValue(driver, pobj.selectResponseType, "multiinput");
					fc.utobj().clickElement(driver, pobj.scoreInclude);
					fc.utobj().clickElement(driver, pobj.excludeNonResponse);
					fc.utobj().clickElement(driver, pobj.notApplicableResponse);
					fc.utobj().clickElement(driver, pobj.complianceNotApplicable);
					fc.utobj().sendKeys(driver, pobj.inputLabels1, "Input Label 1");
					fc.utobj().sendKeys(driver, pobj.inputLabels2, "Input Label 2");
					fc.utobj().sendKeys(driver, pobj.calculationResultLabel, "Calculation Result Label");
					fc.utobj().sendKeys(driver, pobj.rangeScore, "100");
					fc.utobj().clickElement(driver, pobj.addBtn);
					fc.utobj().switchFrameToDefault(driver);
				}
			}

			fc.utobj().clickElement(driver, pobj.finishBtn);

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "TestDummayTest")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Create Visit And Complete", testCaseId = "TC_Create_Visit_And_Complete")
	public void createVisitComplete() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		// Map<String,String> dataSet = fc.utobj().readTestData(config,
		// testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		String visitFormName = "TestVisitx26120747";

		try {
			driver = fc.loginpage().login(driver);
			FieldOpsVisitsPage pobj = new FieldOpsVisitsPage(driver);

			fc.utobj().printTestStep("Navigate To Admin > Franchise Location > Add Franchise Location");
			fc.utobj().printTestStep("Add Franchise Location");
			String regionName = fc.utobj().generateTestData("TestR");
			String storeType = fc.utobj().generateTestData("TestS");
			String franchiseId = fc.utobj().generateTestData("TestFID");
			AdminFranchiseLocationAddFranchiseLocationPageTest franchise = new AdminFranchiseLocationAddFranchiseLocationPageTest();
			franchiseId = franchise.addFranchiseLocation_All(driver, franchiseId, regionName, storeType);

			fc.utobj().printTestStep("Navigate To Admin > Users > Manage Corporate User");
			fc.utobj().printTestStep("Add Corporate User");
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corporateUser = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();

			String emailId = "fieldopsautomation@staffex.com";

			CorporateUser corpUser = new CorporateUser();
			corpUser.setConsultant("Y");
			corpUser.setEmail(emailId);
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corporateUser.createDefaultCorporateUser_Through_WebService_AfterLogin(driver, corpUser);

			fc.utobj().printTestStep("Navigate To FieldOps > Visits Page");
			fc.fieldops().fieldops_common().fieldopsVisitCreateVisitPage(driver);
			fc.utobj().printTestStep("Create Visit");

			fc.utobj().clickElement(driver, pobj.createVisit);

			fc.utobj().selectValFromMultiSelectRadioBtn(driver, pobj.selectVisitForm, visitFormName);
			fc.utobj().selectValFromMultiSelect(driver, pobj.selectFranchiseId, franchiseId);
			fc.utobj().selectValFromMultiSelectRadioBtn(driver, pobj.selectConsultantMultiSelect,
					corpUser.getuserFullName());
			String futureDate = fc.utobj().getFutureDateUSFormat(20);
			fc.utobj().sendKeys(driver, pobj.scheduleDateTime, futureDate);

			fc.utobj().printTestStep("Start Now");
			fc.utobj().clickElement(driver, pobj.startNowButton);

			for (int p = 0; p < 51; p++) {

				try {

					// for Text Area

					List<WebElement> list1 = driver.findElements(By.xpath(
							".//div[@aria-expanded='true']//table[contains(@id ,  '_testtabnames26') or contains(@id , '_testtabnames27')]//textarea[contains(@id , 'Response1')]"));

					for (int i = 0; i < list1.size(); i++) {

						try {
							list1.get(i).isDisplayed();
							fc.utobj().sendKeys(driver, list1.get(i), fc.utobj().generateTestData("Test Data"));
						} catch (Exception e) {
							System.out.println("Error Msg For Text Area: " + e.toString());
						}
					}

					// for Date Field
					List<WebElement> list0 = driver.findElements(By.xpath(
							".//div[@aria-expanded='true']//table[contains(@id ,  '_testtabnames26') or contains(@id , '_testtabnames27')]//input[contains(@id , 'Response1') and contains(@onblur , 'dateUtility')]"));

					for (int l = 0; l < list0.size(); l++) {

						try {

							list0.get(l).isDisplayed();
							fc.utobj().sendKeys(driver, list0.get(l), fc.utobj().getCurrentDateUSFormat());
						} catch (Exception e) {

						}
					}

					// for Input String
					List<WebElement> list2 = driver.findElements(By.xpath(
							".//div[@aria-expanded='true']//table[contains(@id ,  '_testtabnames26') or contains(@id , '_testtabnames27')]//input[contains(@id , 'Response1')]"));
					;

					for (int k = 0; k < list2.size(); k++) {

						try {

							String text = list2.get(k).getAttribute("onfocus");

							if (text == null) {

								fc.utobj().sendKeys(driver, list2.get(k), "70");
							}
						} catch (Exception e) {
							System.out.println("Error Msg For Input Field: " + e.toString());
						}
					}

					// For CheckBox Radio Button

					List<WebElement> list3 = driver.findElements(By.xpath(
							".//div[@aria-expanded='true']//table[contains(@id ,  '_testtabnames26') or contains(@id , '_testtabnames27')]//label[contains(text () , 'USA')]"));

					for (int n = 0; n < list3.size(); n++) {

						try {
							fc.utobj().clickElement(driver, list3.get(n));
						} catch (Exception e) {
							System.out.println("Error Msg For Check Box And Radio Button: " + e.toString());
						}
					}

					// For selectDrop

					List<WebElement> list4 = driver.findElements(By.xpath(
							".//div[@aria-expanded='true']//table[contains(@id ,  '_testtabnames26') or contains(@id , '_testtabnames27')]//select"));

					for (int g = 0; g < list4.size(); g++) {

						try {

							fc.utobj().selectDropDownByIndex(driver, list4.get(g), 1);

						} catch (Exception e) {
							System.out.println("Error Msg For Select Field: " + e.toString());
						}
					}

					// For Comment
					List<WebElement> list5 = driver.findElements(By.xpath(
							".//div[@aria-expanded='true']//table[contains(@id ,  '_testtabnames26') or contains(@id , '_testtabnames27')]//textarea[contains(@id , 'Comment1')]"));

					for (int b = 0; b < list5.size(); b++) {

						try {

							fc.utobj().sendKeys(driver, list5.get(b), fc.utobj().generateTestData("Comment Data"));

						} catch (Exception e) {
							System.out.println("Error Msg For Select Field: " + e.toString());
						}
					}

					fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='nextButton']"));

				} catch (Exception e) {
					System.out.println("Final Error :" + e.getMessage());
				}

			}

			// fc.utobj().logoutAndQuitBrowser(driver, config, testCaseId);
		} catch (Exception e) {
			// fc.utobj().quitBrowserOnCatch(driver, config, e, testCaseId);
		}
	}

	/*
	 * Add A visit for with 1000 question Witout Tab : 1 . Single page , list
	 * layout
	 */

	@Test(groups = "fieldops10Harish12", invocationCount = 5, threadPoolSize = 5)
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Add Visit Form With 50 Tab 1000 Question In It", testCaseId = "TC_Create_Visit_Form_Test_With_Question")
	public void createVisitFormWIthQuestion() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData(testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			AdminFieldOpsManageVisitFormCustomizeVisitFormPage pobj = new AdminFieldOpsManageVisitFormCustomizeVisitFormPage(
					driver);
			AdminFieldOpsQuestionLibraryPage question_Library = new AdminFieldOpsQuestionLibraryPage(driver);

			fc.utobj().printTestStep("Navigate To Admin > FieldOps > Manage Visit Form Page");
			fc.utobj().printTestStep("Add Private Visit Form With Tab View");

			AdminFieldOpsManageVisitFormPageTest visitForm = new AdminFieldOpsManageVisitFormPageTest();
			visitForm.addVisitFormPrivateNo(driver, config, dataSet);

			for (int j = 0; j < 10; j++) {
				fc.utobj().printTestStep("Add Paragraph Type Question In First Tab");
				fc.utobj().getElement(driver, pobj.addQuestion).click();
				
				fc.commonMethods().switch_cboxIframe_frameId(driver);

				List<WebElement> list = pobj.addQuestionNewOrLibraray;
				if (fc.utobj().isSelected(driver,list.get(1))) {
					fc.utobj().clickElement(driver, list.get(0));
				} else {

				}
				String questionTextForParagraph = fc.utobj().generateTestData(dataSet.get("questionTextForParagraph"));
				fc.utobj().sendKeys(driver, pobj.questionTextArea, questionTextForParagraph);

				// add Attachement
				fc.utobj().selectDropDownByValue(driver, pobj.uploadType, "op1");
				String filePath = fc.utobj().getFilePathFromTestData(dataSet.get("fileName"));

				fc.utobj().sendKeys(driver, pobj.uploadAttachmentPath, filePath);
				fc.utobj().selectDropDown(driver, pobj.criticalLevel, dataSet.get("criticalLevel"));
				fc.utobj().clickElement(driver, pobj.nextBtn);

				fc.utobj().selectDropDown(driver, pobj.selectResponseType, "Paragraph Text");

				fc.utobj().clickElement(driver, pobj.addBtn);
				fc.utobj().switchFrameToDefault(driver);
			}

			for (int j = 0; j < 10; j++) {
				fc.utobj().printTestStep("Add Single Line Text Type Question In Visit Form");
				fc.utobj().getElement(driver, pobj.addQuestion).click();
				fc.commonMethods().switch_cboxIframe_frameId(driver);

				String questionTextForSingleLineText = fc.utobj()
						.generateTestData(dataSet.get("questionTextForSingleLineText"));
				fc.utobj().sendKeys(driver, pobj.questionTextArea, questionTextForSingleLineText);

				fc.utobj().selectDropDown(driver, pobj.criticalLevel, dataSet.get("criticalLevel"));
				fc.utobj().clickElement(driver, pobj.nextBtn);

				fc.utobj().selectDropDown(driver, pobj.selectResponseType, "Single Line Text");

				fc.utobj().clickElement(driver, pobj.addBtn);
				fc.utobj().switchFrameToDefault(driver);
			}

			for (int j = 0; j < 1; j++) {
				fc.utobj().printTestStep("Add Date Type Question In Visit Form");
				fc.utobj().getElement(driver, pobj.addQuestion).click();
				fc.commonMethods().switch_cboxIframe_frameId(driver);

				String questionTextForDate = fc.utobj().generateTestData(dataSet.get("questionTextForDate"));
				fc.utobj().sendKeys(driver, pobj.questionTextArea, questionTextForDate);

				fc.utobj().selectDropDown(driver, pobj.criticalLevel, dataSet.get("criticalLevel"));
				fc.utobj().clickElement(driver, pobj.nextBtn);

				fc.utobj().selectDropDown(driver, pobj.selectResponseType, "Date");

				fc.utobj().clickElement(driver, pobj.addBtn);
				fc.utobj().switchFrameToDefault(driver);
			}

			for (int j = 0; j < 10; j++) {

				fc.utobj().printTestStep("Add Rating Type Question In Visit Form");
				fc.utobj().getElement(driver, pobj.addQuestion).click();
				fc.commonMethods().switch_cboxIframe_frameId(driver);

				String questionTextForRating = fc.utobj().generateTestData(dataSet.get("questionTextForRating"));
				fc.utobj().sendKeys(driver, pobj.questionTextArea, questionTextForRating);

				fc.utobj().selectDropDown(driver, pobj.criticalLevel, dataSet.get("criticalLevel"));
				fc.utobj().clickElement(driver, pobj.nextBtn);

				fc.utobj().selectDropDown(driver, pobj.selectResponseType, "Rating");

				fc.utobj().clickElement(driver, pobj.scoreInclude);
				fc.utobj().clickElement(driver, pobj.excludeNonResponse);
				fc.utobj().clickElement(driver, pobj.notApplicableResponse);
				fc.utobj().clickElement(driver, pobj.complianceNotApplicable);
				fc.utobj().sendKeys(driver, pobj.minRange, dataSet.get("minRange"));
				fc.utobj().sendKeys(driver, pobj.maxRange, dataSet.get("maxRange"));
				fc.utobj().clickElement(driver, pobj.addBtn);
				fc.utobj().switchFrameToDefault(driver);
			}

			for (int j = 0; j < 10; j++) {

				fc.utobj().printTestStep("Add Number Type Question In Visit Form");
				fc.utobj().getElement(driver, pobj.addQuestion).click();
				fc.commonMethods().switch_cboxIframe_frameId(driver);

				String questionTextForNumber = fc.utobj().generateTestData(dataSet.get("questionTextForNumber"));
				fc.utobj().sendKeys(driver, pobj.questionTextArea, questionTextForNumber);

				fc.utobj().selectDropDown(driver, pobj.criticalLevel, dataSet.get("criticalLevel"));
				fc.utobj().clickElement(driver, pobj.nextBtn);

				fc.utobj().selectDropDown(driver, pobj.selectResponseType, "Number");
				fc.utobj().clickElement(driver, pobj.scoreInclude);
				fc.utobj().clickElement(driver, pobj.excludeNonResponse);
				fc.utobj().clickElement(driver, pobj.notApplicableResponse);

				fc.utobj().sendKeys(driver, pobj.rangeTo, dataSet.get("rangeTo"));
				fc.utobj().sendKeys(driver, pobj.rangeScore, dataSet.get("rangeScore"));
				fc.utobj().clickElement(driver, pobj.addBtn);
				fc.utobj().switchFrameToDefault(driver);
			}

			for (int j = 0; j < 10; j++) {

				fc.utobj().printTestStep("Add Single Select Radio Button Type Question In Visit Form");
				fc.utobj().getElement(driver, pobj.addQuestion).click();
				fc.commonMethods().switch_cboxIframe_frameId(driver);

				String questionTextForSingleRadioBtn = fc.utobj()
						.generateTestData(dataSet.get("questionTextForSingleRadioBtn"));
				fc.utobj().sendKeys(driver, pobj.questionTextArea, questionTextForSingleRadioBtn);

				fc.utobj().selectDropDown(driver, pobj.criticalLevel, dataSet.get("criticalLevel"));
				fc.utobj().clickElement(driver, pobj.nextBtn);

				fc.utobj().selectDropDownByValue(driver, pobj.selectResponseType, "radio");
				fc.utobj().clickElement(driver, pobj.scoreInclude);
				fc.utobj().clickElement(driver, pobj.excludeNonResponse);
				fc.utobj().clickElement(driver, pobj.complianceNotApplicable);
				fc.utobj().sendKeys(driver, question_Library.firstChoice, dataSet.get("firstChoice"));
				fc.utobj().sendKeys(driver, question_Library.secondChoice, dataSet.get("secondChoice"));
				fc.utobj().sendKeys(driver, question_Library.firstScore, dataSet.get("firstScore"));
				fc.utobj().sendKeys(driver, question_Library.secondScore, dataSet.get("secondScore"));
				fc.utobj().clickElement(driver, pobj.addBtn);
				fc.utobj().switchFrameToDefault(driver);

			}

			for (int j = 0; j < 10; j++) {

				fc.utobj().printTestStep("Add Single Select Drop Down Type Question In Visit Form");
				fc.utobj().getElement(driver, pobj.addQuestion).click();
				fc.commonMethods().switch_cboxIframe_frameId(driver);

				String questionTextForSingleSelectDropDown = fc.utobj()
						.generateTestData(dataSet.get("questionTextForSingleSelectDropDown"));
				fc.utobj().sendKeys(driver, pobj.questionTextArea, questionTextForSingleSelectDropDown);

				fc.utobj().selectDropDown(driver, pobj.criticalLevel, dataSet.get("criticalLevel"));
				fc.utobj().clickElement(driver, pobj.nextBtn);

				fc.utobj().selectDropDownByValue(driver, pobj.selectResponseType, "combo");
				fc.utobj().clickElement(driver, pobj.scoreInclude);
				fc.utobj().clickElement(driver, pobj.excludeNonResponse);
				fc.utobj().clickElement(driver, pobj.notApplicableResponse);
				fc.utobj().clickElement(driver, pobj.complianceNotApplicable);
				fc.utobj().sendKeys(driver, question_Library.firstChoice, dataSet.get("firstChoice"));
				fc.utobj().sendKeys(driver, question_Library.secondChoice, dataSet.get("secondChoice"));
				fc.utobj().sendKeys(driver, question_Library.firstScore, dataSet.get("firstScore"));
				fc.utobj().sendKeys(driver, question_Library.secondScore, dataSet.get("secondScore"));
				fc.utobj().clickElement(driver, pobj.addBtn);
				fc.utobj().switchFrameToDefault(driver);

			}

			for (int j = 0; j < 10; j++) {
				fc.utobj().printTestStep("Add MultiSelect CheckBox Type Question In Visit Form");
				fc.utobj().getElement(driver, pobj.addQuestion).click();
				fc.commonMethods().switch_cboxIframe_frameId(driver);

				String questionTextForMultiSelectCheckBox = fc.utobj()
						.generateTestData(dataSet.get("questionTextForMultiSelectCheckBox"));
				fc.utobj().sendKeys(driver, pobj.questionTextArea, questionTextForMultiSelectCheckBox);

				fc.utobj().selectDropDown(driver, pobj.criticalLevel, dataSet.get("criticalLevel"));
				fc.utobj().clickElement(driver, pobj.nextBtn);

				fc.utobj().selectDropDownByValue(driver, pobj.selectResponseType, "checkbox");
				fc.utobj().clickElement(driver, pobj.scoreInclude);
				fc.utobj().clickElement(driver, pobj.excludeNonResponse);
				fc.utobj().clickElement(driver, pobj.notApplicableResponse);
				fc.utobj().clickElement(driver, pobj.complianceNotApplicable);
				fc.utobj().sendKeys(driver, question_Library.firstChoice, dataSet.get("firstChoice"));
				fc.utobj().sendKeys(driver, question_Library.secondChoice, dataSet.get("secondChoice"));
				fc.utobj().sendKeys(driver, question_Library.firstScore, dataSet.get("firstScore"));
				fc.utobj().sendKeys(driver, question_Library.secondScore, dataSet.get("secondScore"));
				fc.utobj().clickElement(driver, pobj.addBtn);
				fc.utobj().switchFrameToDefault(driver);
			}

			for (int j = 0; j < 10; j++) {
				fc.utobj().printTestStep("Add Smart Type Question In Visit Form");
				fc.utobj().getElement(driver, pobj.addQuestion).click();
				fc.commonMethods().switch_cboxIframe_frameId(driver);

				String questionTextForSmart = fc.utobj().generateTestData(dataSet.get("questionTextForSmart"));
				fc.utobj().sendKeys(driver, pobj.questionTextArea, questionTextForSmart);

				fc.utobj().selectDropDown(driver, pobj.criticalLevel, dataSet.get("criticalLevel"));
				fc.utobj().clickElement(driver, pobj.nextBtn);

				// add Smart Type Question
				fc.utobj().selectDropDown(driver, pobj.selectResponseType, "Smart Question");
				// FiledOps Type Question
				WebElement element = fc.utobj().getElementByXpath(driver, ".//*[@id='ms-parentmoduleName']");
				fc.utobj().clickElement(driver, element);
				WebElement elementInput = element.findElement(By.xpath("./div/div/input"));
				String value = "Field Ops";
				fc.utobj().sendKeys(driver, elementInput, value);
				WebElement element3 = element.findElement(By.xpath(".//label[contains(text(),'" + value + "')]/input"));
				fc.utobj().clickElement(driver, element3);

				// select field

				WebElement element1 = fc.utobj().getElementByXpath(driver, ".//*[@id='ms-parentmoduleField']");
				fc.utobj().clickElement(driver, element1);
				WebElement elementInput1 = element1.findElement(By.xpath("./div/div/input"));
				String value1 = "Date of last visit logged";
				fc.utobj().sendKeys(driver, elementInput1, value1);
				WebElement element4 = element1
						.findElement(By.xpath(".//label[contains(text(),'" + value1 + "')]/input"));
				fc.utobj().clickElement(driver, element4);

				fc.utobj().clickElement(driver, pobj.addBtn);
				fc.utobj().switchFrameToDefault(driver);
			}

			for (int j = 0; j < 10; j++) {
				fc.utobj().printTestStep("Add Multiple Input Type Question In Visit Form");
				fc.utobj().getElement(driver, pobj.addQuestion).click();
				fc.commonMethods().switch_cboxIframe_frameId(driver);

				String questionTextForMultiInput = fc.utobj()
						.generateTestData(dataSet.get("questionTextForMultiInput"));
				fc.utobj().sendKeys(driver, pobj.questionTextArea, questionTextForMultiInput);

				fc.utobj().selectDropDown(driver, pobj.criticalLevel, dataSet.get("criticalLevel"));
				fc.utobj().clickElement(driver, pobj.nextBtn);

				fc.utobj().selectDropDownByValue(driver, pobj.selectResponseType, "multiinput");
				fc.utobj().clickElement(driver, pobj.scoreInclude);
				fc.utobj().clickElement(driver, pobj.excludeNonResponse);
				fc.utobj().clickElement(driver, pobj.notApplicableResponse);
				fc.utobj().clickElement(driver, pobj.complianceNotApplicable);
				fc.utobj().sendKeys(driver, pobj.inputLabels1, "Input Label 1");
				fc.utobj().sendKeys(driver, pobj.inputLabels2, "Input Label 2");
				fc.utobj().sendKeys(driver, pobj.calculationResultLabel, "Calculation Result Label");
				fc.utobj().sendKeys(driver, pobj.rangeScore, "100");
				fc.utobj().clickElement(driver, pobj.addBtn);
				fc.utobj().switchFrameToDefault(driver);
			}

			fc.utobj().clickElement(driver, pobj.finishBtn);

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	/*
	 * Add A visit for with 1000 question Witout Tab : 2 . Single page , Table
	 * layout
	 */

	@Test(groups = "fieldops250Anshu")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Add Visit Form With 50 Tab 1000 Question In It", testCaseId = "TC_Create_Visit_Form_Test_With_Question_01")
	public void SinglePageTableLayout() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData(testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			AdminFieldOpsManageVisitFormCustomizeVisitFormPage pobj = new AdminFieldOpsManageVisitFormCustomizeVisitFormPage(
					driver);
			AdminFieldOpsQuestionLibraryPage question_Library = new AdminFieldOpsQuestionLibraryPage(driver);

			fc.utobj().printTestStep("Navigate To Admin > FieldOps > Manage Visit Form Page");
			fc.utobj().printTestStep("Add Private Visit Form With Tab View");

			for (int j = 0; j < 13; j++) {
				fc.utobj().printTestStep("Add Multiple Input Type Question In Visit Form");
				fc.utobj().getElement(driver, pobj.addQuestion).click();
				fc.commonMethods().switch_cboxIframe_frameId(driver);

				String questionTextForMultiInput = fc.utobj()
						.generateTestData(dataSet.get("questionTextForMultiInput"));
				fc.utobj().sendKeys(driver, pobj.questionTextArea, questionTextForMultiInput);

				fc.utobj().selectDropDown(driver, pobj.criticalLevel, dataSet.get("criticalLevel"));
				fc.utobj().clickElement(driver, pobj.nextBtn);

				fc.utobj().selectDropDownByValue(driver, pobj.selectResponseType, "multiinput");
				fc.utobj().clickElement(driver, pobj.scoreInclude);
				fc.utobj().clickElement(driver, pobj.excludeNonResponse);
				fc.utobj().clickElement(driver, pobj.notApplicableResponse);
				fc.utobj().clickElement(driver, pobj.complianceNotApplicable);
				fc.utobj().sendKeys(driver, pobj.inputLabels1, "Input Label 1");
				fc.utobj().sendKeys(driver, pobj.inputLabels2, "Input Label 2");
				fc.utobj().sendKeys(driver, pobj.calculationResultLabel, "Calculation Result Label");
				fc.utobj().sendKeys(driver, pobj.rangeScore, "100");
				fc.utobj().clickElement(driver, pobj.addBtn);
				fc.utobj().switchFrameToDefault(driver);
			}

			fc.utobj().clickElement(driver, pobj.finishBtn);

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	/*
	 * Add A visit for with 1000 question Witout Tab : 3 . Multi Page , Table
	 * layout
	 */

	@Test(groups = "fieldops10Harish123", invocationCount = 20, threadPoolSize = 20)
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Add Visit Form With 50 Tab 1000 Question In It", testCaseId = "TC_Create_Visit_Form_Test_With_Question_02")
	public void MultiPageTable() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData(testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			AdminFieldOpsManageVisitFormCustomizeVisitFormPage pobj = new AdminFieldOpsManageVisitFormCustomizeVisitFormPage(
					driver);
			AdminFieldOpsManageVisitFormCustomizeVisitFormPageTest customize_Visit_Form = new AdminFieldOpsManageVisitFormCustomizeVisitFormPageTest();
			AdminFieldOpsQuestionLibraryPage question_Library = new AdminFieldOpsQuestionLibraryPage(driver);

			fc.utobj().printTestStep("Navigate To Admin > FieldOps > Manage Visit Form Page");
			fc.utobj().printTestStep("Add Private Visit Form With Tab View");

			AdminFieldOpsManageVisitFormPageTest visitForm = new AdminFieldOpsManageVisitFormPageTest();
			visitForm.addVisitFormTabViewPrivateYes(driver, config, dataSet);

			for (int i = 0; i < 1; i++) {

				fc.utobj().printTestStep("Add Tab");
				customize_Visit_Form.addTab(driver, config, dataSet);

				for (int j = 0; j < 15; j++) {
					fc.utobj().printTestStep("Add Paragraph Type Question In First Tab");
					fc.utobj().getElement(driver, pobj.addQuestion).click();
					fc.commonMethods().switch_cboxIframe_frameId(driver);

					List<WebElement> list = pobj.addQuestionNewOrLibraray;
					if (fc.utobj().isSelected(driver,list.get(1))) {
						fc.utobj().clickElement(driver, list.get(0));
					} else {

					}
					String questionTextForParagraph = fc.utobj()
							.generateTestData(dataSet.get("questionTextForParagraph"));
					fc.utobj().sendKeys(driver, pobj.questionTextArea, questionTextForParagraph);

					// add Attachement
					fc.utobj().selectDropDownByValue(driver, pobj.uploadType, "op1");
					String filePath = fc.utobj().getFilePathFromTestData(dataSet.get("fileName"));

					fc.utobj().sendKeys(driver, pobj.uploadAttachmentPath, filePath);
					fc.utobj().selectDropDown(driver, pobj.criticalLevel, dataSet.get("criticalLevel"));
					fc.utobj().clickElement(driver, pobj.nextBtn);

					fc.utobj().selectDropDown(driver, pobj.selectResponseType, "Paragraph Text");

					fc.utobj().clickElement(driver, pobj.addBtn);
					fc.utobj().switchFrameToDefault(driver);
				}

				for (int j = 0; j < 15; j++) {
					fc.utobj().printTestStep("Add Single Line Text Type Question In Visit Form");
					fc.utobj().getElement(driver, pobj.addQuestion).click();
					fc.commonMethods().switch_cboxIframe_frameId(driver);

					String questionTextForSingleLineText = fc.utobj()
							.generateTestData(dataSet.get("questionTextForSingleLineText"));
					fc.utobj().sendKeys(driver, pobj.questionTextArea, questionTextForSingleLineText);

					fc.utobj().selectDropDown(driver, pobj.criticalLevel, dataSet.get("criticalLevel"));
					fc.utobj().clickElement(driver, pobj.nextBtn);

					fc.utobj().selectDropDown(driver, pobj.selectResponseType, "Single Line Text");

					fc.utobj().clickElement(driver, pobj.addBtn);
					fc.utobj().switchFrameToDefault(driver);
				}

				for (int j = 0; j < 15; j++) {
					fc.utobj().printTestStep("Add Date Type Question In Visit Form");
					fc.utobj().getElement(driver, pobj.addQuestion).click();
					fc.commonMethods().switch_cboxIframe_frameId(driver);

					String questionTextForDate = fc.utobj().generateTestData(dataSet.get("questionTextForDate"));
					fc.utobj().sendKeys(driver, pobj.questionTextArea, questionTextForDate);

					fc.utobj().selectDropDown(driver, pobj.criticalLevel, dataSet.get("criticalLevel"));
					fc.utobj().clickElement(driver, pobj.nextBtn);

					fc.utobj().selectDropDown(driver, pobj.selectResponseType, "Date");

					fc.utobj().clickElement(driver, pobj.addBtn);
					fc.utobj().switchFrameToDefault(driver);
				}

				for (int j = 0; j < 15; j++) {

					fc.utobj().printTestStep("Add Rating Type Question In Visit Form");
					fc.utobj().getElement(driver, pobj.addQuestion).click();
					fc.commonMethods().switch_cboxIframe_frameId(driver);

					String questionTextForRating = fc.utobj().generateTestData(dataSet.get("questionTextForRating"));
					fc.utobj().sendKeys(driver, pobj.questionTextArea, questionTextForRating);

					fc.utobj().selectDropDown(driver, pobj.criticalLevel, dataSet.get("criticalLevel"));
					fc.utobj().clickElement(driver, pobj.nextBtn);

					fc.utobj().selectDropDown(driver, pobj.selectResponseType, "Rating");

					fc.utobj().clickElement(driver, pobj.scoreInclude);
					fc.utobj().clickElement(driver, pobj.excludeNonResponse);
					fc.utobj().clickElement(driver, pobj.notApplicableResponse);
					fc.utobj().clickElement(driver, pobj.complianceNotApplicable);
					fc.utobj().sendKeys(driver, pobj.minRange, dataSet.get("minRange"));
					fc.utobj().sendKeys(driver, pobj.maxRange, dataSet.get("maxRange"));
					fc.utobj().clickElement(driver, pobj.addBtn);
					fc.utobj().switchFrameToDefault(driver);
				}

				for (int j = 0; j < 15; j++) {

					fc.utobj().printTestStep("Add Number Type Question In Visit Form");
					fc.utobj().getElement(driver, pobj.addQuestion).click();
					fc.commonMethods().switch_cboxIframe_frameId(driver);

					String questionTextForNumber = fc.utobj().generateTestData(dataSet.get("questionTextForNumber"));
					fc.utobj().sendKeys(driver, pobj.questionTextArea, questionTextForNumber);

					fc.utobj().selectDropDown(driver, pobj.criticalLevel, dataSet.get("criticalLevel"));
					fc.utobj().clickElement(driver, pobj.nextBtn);

					fc.utobj().selectDropDown(driver, pobj.selectResponseType, "Number");
					fc.utobj().clickElement(driver, pobj.scoreInclude);
					fc.utobj().clickElement(driver, pobj.excludeNonResponse);
					fc.utobj().clickElement(driver, pobj.notApplicableResponse);

					fc.utobj().sendKeys(driver, pobj.rangeTo, dataSet.get("rangeTo"));
					fc.utobj().sendKeys(driver, pobj.rangeScore, dataSet.get("rangeScore"));
					fc.utobj().clickElement(driver, pobj.addBtn);
					fc.utobj().switchFrameToDefault(driver);
				}

				for (int j = 0; j < 15; j++) {

					fc.utobj().printTestStep("Add Single Select Radio Button Type Question In Visit Form");
					fc.utobj().getElement(driver, pobj.addQuestion).click();
					fc.commonMethods().switch_cboxIframe_frameId(driver);

					String questionTextForSingleRadioBtn = fc.utobj()
							.generateTestData(dataSet.get("questionTextForSingleRadioBtn"));
					fc.utobj().sendKeys(driver, pobj.questionTextArea, questionTextForSingleRadioBtn);

					fc.utobj().selectDropDown(driver, pobj.criticalLevel, dataSet.get("criticalLevel"));
					fc.utobj().clickElement(driver, pobj.nextBtn);

					fc.utobj().selectDropDownByValue(driver, pobj.selectResponseType, "radio");
					fc.utobj().clickElement(driver, pobj.scoreInclude);
					fc.utobj().clickElement(driver, pobj.excludeNonResponse);
					fc.utobj().clickElement(driver, pobj.complianceNotApplicable);
					fc.utobj().sendKeys(driver, question_Library.firstChoice, dataSet.get("firstChoice"));
					fc.utobj().sendKeys(driver, question_Library.secondChoice, dataSet.get("secondChoice"));
					fc.utobj().sendKeys(driver, question_Library.firstScore, dataSet.get("firstScore"));
					fc.utobj().sendKeys(driver, question_Library.secondScore, dataSet.get("secondScore"));
					fc.utobj().clickElement(driver, pobj.addBtn);
					fc.utobj().switchFrameToDefault(driver);

				}

				for (int j = 0; j < 15; j++) {

					fc.utobj().printTestStep("Add Single Select Drop Down Type Question In Visit Form");
					fc.utobj().getElement(driver, pobj.addQuestion).click();
					fc.commonMethods().switch_cboxIframe_frameId(driver);

					String questionTextForSingleSelectDropDown = fc.utobj()
							.generateTestData(dataSet.get("questionTextForSingleSelectDropDown"));
					fc.utobj().sendKeys(driver, pobj.questionTextArea, questionTextForSingleSelectDropDown);

					fc.utobj().selectDropDown(driver, pobj.criticalLevel, dataSet.get("criticalLevel"));
					fc.utobj().clickElement(driver, pobj.nextBtn);

					fc.utobj().selectDropDownByValue(driver, pobj.selectResponseType, "combo");
					fc.utobj().clickElement(driver, pobj.scoreInclude);
					fc.utobj().clickElement(driver, pobj.excludeNonResponse);
					fc.utobj().clickElement(driver, pobj.notApplicableResponse);
					fc.utobj().clickElement(driver, pobj.complianceNotApplicable);
					fc.utobj().sendKeys(driver, question_Library.firstChoice, dataSet.get("firstChoice"));
					fc.utobj().sendKeys(driver, question_Library.secondChoice, dataSet.get("secondChoice"));
					fc.utobj().sendKeys(driver, question_Library.firstScore, dataSet.get("firstScore"));
					fc.utobj().sendKeys(driver, question_Library.secondScore, dataSet.get("secondScore"));
					fc.utobj().clickElement(driver, pobj.addBtn);
					fc.utobj().switchFrameToDefault(driver);

				}

				for (int j = 0; j < 15; j++) {
					fc.utobj().printTestStep("Add MultiSelect CheckBox Type Question In Visit Form");
					fc.utobj().getElement(driver, pobj.addQuestion).click();
					fc.commonMethods().switch_cboxIframe_frameId(driver);

					String questionTextForMultiSelectCheckBox = fc.utobj()
							.generateTestData(dataSet.get("questionTextForMultiSelectCheckBox"));
					fc.utobj().sendKeys(driver, pobj.questionTextArea, questionTextForMultiSelectCheckBox);

					fc.utobj().selectDropDown(driver, pobj.criticalLevel, dataSet.get("criticalLevel"));
					fc.utobj().clickElement(driver, pobj.nextBtn);

					fc.utobj().selectDropDownByValue(driver, pobj.selectResponseType, "checkbox");
					fc.utobj().clickElement(driver, pobj.scoreInclude);
					fc.utobj().clickElement(driver, pobj.excludeNonResponse);
					fc.utobj().clickElement(driver, pobj.notApplicableResponse);
					fc.utobj().clickElement(driver, pobj.complianceNotApplicable);
					fc.utobj().sendKeys(driver, question_Library.firstChoice, dataSet.get("firstChoice"));
					fc.utobj().sendKeys(driver, question_Library.secondChoice, dataSet.get("secondChoice"));
					fc.utobj().sendKeys(driver, question_Library.firstScore, dataSet.get("firstScore"));
					fc.utobj().sendKeys(driver, question_Library.secondScore, dataSet.get("secondScore"));
					fc.utobj().clickElement(driver, pobj.addBtn);
					fc.utobj().switchFrameToDefault(driver);
				}

				for (int j = 0; j < 15; j++) {
					fc.utobj().printTestStep("Add Smart Type Question In Visit Form");
					fc.utobj().getElement(driver, pobj.addQuestion).click();
					fc.commonMethods().switch_cboxIframe_frameId(driver);

					String questionTextForSmart = fc.utobj().generateTestData(dataSet.get("questionTextForSmart"));
					fc.utobj().sendKeys(driver, pobj.questionTextArea, questionTextForSmart);

					fc.utobj().selectDropDown(driver, pobj.criticalLevel, dataSet.get("criticalLevel"));
					fc.utobj().clickElement(driver, pobj.nextBtn);

					// add Smart Type Question
					fc.utobj().selectDropDown(driver, pobj.selectResponseType, "Smart Question");
					// FiledOps Type Question
					WebElement element = fc.utobj().getElementByXpath(driver, ".//*[@id='ms-parentmoduleName']");
					fc.utobj().clickElement(driver, element);
					WebElement elementInput = element.findElement(By.xpath("./div/div/input"));
					String value = "Field Ops";
					fc.utobj().sendKeys(driver, elementInput, value);
					WebElement element3 = element
							.findElement(By.xpath(".//label[contains(text(),'" + value + "')]/input"));
					fc.utobj().clickElement(driver, element3);

					// select field

					WebElement element1 = fc.utobj().getElementByXpath(driver, ".//*[@id='ms-parentmoduleField']");
					fc.utobj().clickElement(driver, element1);
					WebElement elementInput1 = element1.findElement(By.xpath("./div/div/input"));
					String value1 = "Date of last visit logged";
					fc.utobj().sendKeys(driver, elementInput1, value1);
					WebElement element4 = element1
							.findElement(By.xpath(".//label[contains(text(),'" + value1 + "')]/input"));
					fc.utobj().clickElement(driver, element4);

					fc.utobj().clickElement(driver, pobj.addBtn);
					fc.utobj().switchFrameToDefault(driver);
				}

				for (int j = 0; j < 15; j++) {
					fc.utobj().printTestStep("Add Multiple Input Type Question In Visit Form");
					fc.utobj().getElement(driver, pobj.addQuestion).click();
					fc.commonMethods().switch_cboxIframe_frameId(driver);

					String questionTextForMultiInput = fc.utobj()
							.generateTestData(dataSet.get("questionTextForMultiInput"));
					fc.utobj().sendKeys(driver, pobj.questionTextArea, questionTextForMultiInput);

					fc.utobj().selectDropDown(driver, pobj.criticalLevel, dataSet.get("criticalLevel"));
					fc.utobj().clickElement(driver, pobj.nextBtn);

					fc.utobj().selectDropDownByValue(driver, pobj.selectResponseType, "multiinput");
					fc.utobj().clickElement(driver, pobj.scoreInclude);
					fc.utobj().clickElement(driver, pobj.excludeNonResponse);
					fc.utobj().clickElement(driver, pobj.notApplicableResponse);
					fc.utobj().clickElement(driver, pobj.complianceNotApplicable);
					fc.utobj().sendKeys(driver, pobj.inputLabels1, "Input Label 1");
					fc.utobj().sendKeys(driver, pobj.inputLabels2, "Input Label 2");
					fc.utobj().sendKeys(driver, pobj.calculationResultLabel, "Calculation Result Label");
					fc.utobj().sendKeys(driver, pobj.rangeScore, "100");
					fc.utobj().clickElement(driver, pobj.addBtn);
					fc.utobj().switchFrameToDefault(driver);
				}
			}

			fc.utobj().clickElement(driver, pobj.finishBtn);

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	/*
	 * Add A visit for with 1000 question Witout Tab : 4 . Multi Page , List
	 * layout
	 */

	@Test(groups = "fieldops250Vinod")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Add Visit Form With 1 Tab 100 Question In It", testCaseId = "TC_Create_Visit_Form_Test_With_Question_03")
	public void MultiPageList() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData(testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			AdminFieldOpsManageVisitFormCustomizeVisitFormPage pobj = new AdminFieldOpsManageVisitFormCustomizeVisitFormPage(
					driver);
			AdminFieldOpsManageVisitFormCustomizeVisitFormPageTest customize_Visit_Form = new AdminFieldOpsManageVisitFormCustomizeVisitFormPageTest();
			AdminFieldOpsQuestionLibraryPage question_Library = new AdminFieldOpsQuestionLibraryPage(driver);

			fc.utobj().printTestStep("Navigate To Admin > FieldOps > Manage Visit Form Page");
			fc.utobj().printTestStep("Add Private Visit Form With Tab View");

			AdminFieldOpsManageVisitFormPageTest visitForm = new AdminFieldOpsManageVisitFormPageTest();
			visitForm.addVisitFormTabViewPrivateYes(driver, config, dataSet);

			for (int i = 0; i < 1; i++) {

				fc.utobj().printTestStep("Add Tab");
				customize_Visit_Form.addTab(driver, config, dataSet);

				for (int j = 0; j < 10; j++) {
					fc.utobj().printTestStep("Add Paragraph Type Question In First Tab");
					fc.utobj().getElement(driver, pobj.addQuestion).click();
					fc.commonMethods().switch_cboxIframe_frameId(driver);

					List<WebElement> list = pobj.addQuestionNewOrLibraray;
					if (fc.utobj().isSelected(driver,list.get(1))) {
						fc.utobj().clickElement(driver, list.get(0));
					} else {

					}
					String questionTextForParagraph = fc.utobj()
							.generateTestData(dataSet.get("questionTextForParagraph"));
					fc.utobj().sendKeys(driver, pobj.questionTextArea, questionTextForParagraph);

					// add Attachement
					fc.utobj().selectDropDownByValue(driver, pobj.uploadType, "op1");
					String filePath = fc.utobj().getFilePathFromTestData(dataSet.get("fileName"));

					fc.utobj().sendKeys(driver, pobj.uploadAttachmentPath, filePath);
					fc.utobj().selectDropDown(driver, pobj.criticalLevel, dataSet.get("criticalLevel"));
					fc.utobj().clickElement(driver, pobj.nextBtn);

					fc.utobj().selectDropDown(driver, pobj.selectResponseType, "Paragraph Text");

					fc.utobj().clickElement(driver, pobj.addBtn);
					fc.utobj().switchFrameToDefault(driver);
				}

				/*
				 * for (int j = 0; j < 25; j++) {
				 * fc.utobj().printTestStep(testCaseId,
				 * "Add Single Line Text Type Question In Visit Form");
				 * fc.utobj().getElement(driver,
				 * pobj.addQuestion).click(); fc.utobj().switchFrameById(driver,
				 * "cboxIframe");
				 * 
				 * String questionTextForSingleLineText =
				 * fc.utobj().generateTestData(dataSet.get(
				 * "questionTextForSingleLineText"));
				 * fc.utobj().sendKeys(driver, pobj.questionTextArea,
				 * questionTextForSingleLineText);
				 * 
				 * fc.utobj().selectDropDown(driver, pobj.criticalLevel,
				 * dataSet.get("criticalLevel"));
				 * fc.utobj().clickElement(driver, pobj.nextBtn);
				 * 
				 * 
				 * fc.utobj().selectDropDown(driver, pobj.selectResponseType,
				 * "Single Line Text"); fc.utobj().clickElement(driver,
				 * pobj.addBtn); fc.utobj().switchFrameToDefault(driver); }
				 */

				/*
				 * for (int j = 0; j < 25; j++) {
				 * fc.utobj().printTestStep(testCaseId,
				 * "Add Date Type Question In Visit Form");
				 * fc.utobj().getElement(driver,
				 * pobj.addQuestion).click(); fc.utobj().switchFrameById(driver,
				 * "cboxIframe");
				 * 
				 * String questionTextForDate =
				 * fc.utobj().generateTestData(dataSet.get("questionTextForDate"
				 * )); fc.utobj().sendKeys(driver, pobj.questionTextArea,
				 * questionTextForDate);
				 * 
				 * fc.utobj().selectDropDown(driver, pobj.criticalLevel,
				 * dataSet.get("criticalLevel"));
				 * fc.utobj().clickElement(driver, pobj.nextBtn);
				 * 
				 * 
				 * fc.utobj().selectDropDown(driver, pobj.selectResponseType,
				 * "Date"); fc.utobj().clickElement(driver, pobj.addBtn);
				 * fc.utobj().switchFrameToDefault(driver); }
				 */

				for (int j = 0; j < 10; j++) {

					fc.utobj().printTestStep("Add Rating Type Question In Visit Form");
					fc.utobj().getElement(driver, pobj.addQuestion).click();
					fc.commonMethods().switch_cboxIframe_frameId(driver);

					String questionTextForRating = fc.utobj().generateTestData(dataSet.get("questionTextForRating"));
					fc.utobj().sendKeys(driver, pobj.questionTextArea, questionTextForRating);

					fc.utobj().selectDropDown(driver, pobj.criticalLevel, dataSet.get("criticalLevel"));
					fc.utobj().clickElement(driver, pobj.nextBtn);

					fc.utobj().selectDropDown(driver, pobj.selectResponseType, "Rating");

					fc.utobj().clickElement(driver, pobj.scoreInclude);
					fc.utobj().clickElement(driver, pobj.excludeNonResponse);
					fc.utobj().clickElement(driver, pobj.notApplicableResponse);
					fc.utobj().clickElement(driver, pobj.complianceNotApplicable);
					fc.utobj().sendKeys(driver, pobj.minRange, dataSet.get("minRange"));
					fc.utobj().sendKeys(driver, pobj.maxRange, dataSet.get("maxRange"));
					fc.utobj().clickElement(driver, pobj.addBtn);
					fc.utobj().switchFrameToDefault(driver);
				}

				for (int j = 0; j < 10; j++) {

					fc.utobj().printTestStep("Add Number Type Question In Visit Form");
					fc.utobj().getElement(driver, pobj.addQuestion).click();
					fc.commonMethods().switch_cboxIframe_frameId(driver);

					String questionTextForNumber = fc.utobj().generateTestData(dataSet.get("questionTextForNumber"));
					fc.utobj().sendKeys(driver, pobj.questionTextArea, questionTextForNumber);

					fc.utobj().selectDropDown(driver, pobj.criticalLevel, dataSet.get("criticalLevel"));
					fc.utobj().clickElement(driver, pobj.nextBtn);

					fc.utobj().selectDropDown(driver, pobj.selectResponseType, "Number");
					fc.utobj().clickElement(driver, pobj.scoreInclude);
					fc.utobj().clickElement(driver, pobj.excludeNonResponse);
					fc.utobj().clickElement(driver, pobj.notApplicableResponse);

					fc.utobj().sendKeys(driver, pobj.rangeTo, dataSet.get("rangeTo"));
					fc.utobj().sendKeys(driver, pobj.rangeScore, dataSet.get("rangeScore"));
					fc.utobj().clickElement(driver, pobj.addBtn);
					fc.utobj().switchFrameToDefault(driver);
				}

				for (int j = 0; j < 10; j++) {

					fc.utobj().printTestStep("Add Single Select Radio Button Type Question In Visit Form");
					fc.utobj().getElement(driver, pobj.addQuestion).click();
					fc.commonMethods().switch_cboxIframe_frameId(driver);

					String questionTextForSingleRadioBtn = fc.utobj()
							.generateTestData(dataSet.get("questionTextForSingleRadioBtn"));
					fc.utobj().sendKeys(driver, pobj.questionTextArea, questionTextForSingleRadioBtn);

					fc.utobj().selectDropDown(driver, pobj.criticalLevel, dataSet.get("criticalLevel"));
					fc.utobj().clickElement(driver, pobj.nextBtn);

					fc.utobj().selectDropDownByValue(driver, pobj.selectResponseType, "radio");
					fc.utobj().clickElement(driver, pobj.scoreInclude);
					fc.utobj().clickElement(driver, pobj.excludeNonResponse);
					fc.utobj().clickElement(driver, pobj.complianceNotApplicable);
					fc.utobj().sendKeys(driver, question_Library.firstChoice, dataSet.get("firstChoice"));
					fc.utobj().sendKeys(driver, question_Library.secondChoice, dataSet.get("secondChoice"));
					fc.utobj().sendKeys(driver, question_Library.firstScore, dataSet.get("firstScore"));
					fc.utobj().sendKeys(driver, question_Library.secondScore, dataSet.get("secondScore"));
					fc.utobj().clickElement(driver, pobj.addBtn);
					fc.utobj().switchFrameToDefault(driver);

				}

				for (int j = 0; j < 10; j++) {

					fc.utobj().printTestStep("Add Single Select Drop Down Type Question In Visit Form");
					fc.utobj().getElement(driver, pobj.addQuestion).click();
					fc.commonMethods().switch_cboxIframe_frameId(driver);

					String questionTextForSingleSelectDropDown = fc.utobj()
							.generateTestData(dataSet.get("questionTextForSingleSelectDropDown"));
					fc.utobj().sendKeys(driver, pobj.questionTextArea, questionTextForSingleSelectDropDown);

					fc.utobj().selectDropDown(driver, pobj.criticalLevel, dataSet.get("criticalLevel"));
					fc.utobj().clickElement(driver, pobj.nextBtn);

					fc.utobj().selectDropDownByValue(driver, pobj.selectResponseType, "combo");
					fc.utobj().clickElement(driver, pobj.scoreInclude);
					fc.utobj().clickElement(driver, pobj.excludeNonResponse);
					fc.utobj().clickElement(driver, pobj.notApplicableResponse);
					fc.utobj().clickElement(driver, pobj.complianceNotApplicable);
					fc.utobj().sendKeys(driver, question_Library.firstChoice, dataSet.get("firstChoice"));
					fc.utobj().sendKeys(driver, question_Library.secondChoice, dataSet.get("secondChoice"));
					fc.utobj().sendKeys(driver, question_Library.firstScore, dataSet.get("firstScore"));
					fc.utobj().sendKeys(driver, question_Library.secondScore, dataSet.get("secondScore"));
					fc.utobj().clickElement(driver, pobj.addBtn);
					fc.utobj().switchFrameToDefault(driver);

				}

				/*
				 * for (int j = 0; j < 25; j++) {
				 * fc.utobj().printTestStep(testCaseId,
				 * "Add MultiSelect CheckBox Type Question In Visit Form");
				 * fc.utobj().getElement(driver,
				 * pobj.addQuestion).click(); fc.utobj().switchFrameById(driver,
				 * "cboxIframe");
				 * 
				 * String questionTextForMultiSelectCheckBox =
				 * fc.utobj().generateTestData(dataSet.get(
				 * "questionTextForMultiSelectCheckBox"));
				 * fc.utobj().sendKeys(driver, pobj.questionTextArea,
				 * questionTextForMultiSelectCheckBox);
				 * 
				 * fc.utobj().selectDropDown(driver, pobj.criticalLevel,
				 * dataSet.get("criticalLevel"));
				 * fc.utobj().clickElement(driver, pobj.nextBtn);
				 * 
				 * 
				 * fc.utobj().selectDropDownByValue(driver,
				 * pobj.selectResponseType, "checkbox");
				 * fc.utobj().clickElement(driver, pobj.scoreInclude);
				 * fc.utobj().clickElement(driver, pobj.excludeNonResponse);
				 * fc.utobj().clickElement(driver, pobj.notApplicableResponse);
				 * fc.utobj().clickElement(driver,
				 * pobj.complianceNotApplicable); fc.utobj().sendKeys(driver,
				 * question_Library.firstChoice, dataSet.get("firstChoice"));
				 * fc.utobj().sendKeys(driver, question_Library.secondChoice,
				 * dataSet.get("secondChoice")); fc.utobj().sendKeys(driver,
				 * question_Library.firstScore, dataSet.get("firstScore"));
				 * fc.utobj().sendKeys(driver, question_Library.secondScore,
				 * dataSet.get("secondScore")); fc.utobj().clickElement(driver,
				 * pobj.addBtn); fc.utobj().switchFrameToDefault(driver); }
				 */

				/*
				 * for (int j = 0; j < 25; j++) {
				 * fc.utobj().printTestStep(testCaseId,
				 * "Add Smart Type Question In Visit Form");
				 * fc.utobj().getElement(driver,
				 * pobj.addQuestion).click(); fc.utobj().switchFrameById(driver,
				 * "cboxIframe");
				 * 
				 * String questionTextForSmart =
				 * fc.utobj().generateTestData(dataSet.get(
				 * "questionTextForSmart")); fc.utobj().sendKeys(driver,
				 * pobj.questionTextArea, questionTextForSmart);
				 * 
				 * fc.utobj().selectDropDown(driver, pobj.criticalLevel,
				 * dataSet.get("criticalLevel"));
				 * fc.utobj().clickElement(driver, pobj.nextBtn);
				 * 
				 * 
				 * //add Smart Type Question fc.utobj().selectDropDown(driver,
				 * pobj.selectResponseType, "Smart Question"); //FiledOps Type
				 * Question WebElement
				 * element=fc.utobj().getElementByXpath(driver,
				 * ".//*[@id='ms-parentmoduleName']"));
				 * fc.utobj().clickElement(driver, element); WebElement
				 * elementInput=element.findElement(By.xpath("./div/div/input"))
				 * ; String value="Field Ops"; fc.utobj().sendKeys(driver,
				 * elementInput, value); WebElement element3 =
				 * element.findElement(By.xpath(".//label[contains(text(),'"+
				 * value+"')]/input")); fc.utobj().clickElement(driver,
				 * element3);
				 * 
				 * //select field WebElement
				 * element1=fc.utobj().getElementByXpath(driver,
				 * ".//*[@id='ms-parentmoduleField']"));
				 * fc.utobj().clickElement(driver, element1); WebElement
				 * elementInput1=element1.findElement(By.xpath("./div/div/input"
				 * )); String value1="Date of last visit logged";
				 * fc.utobj().sendKeys(driver, elementInput1, value1);
				 * WebElement element4 =
				 * element1.findElement(By.xpath(".//label[contains(text(),'"+
				 * value1+"')]/input")); fc.utobj().clickElement(driver,
				 * element4); fc.utobj().clickElement(driver, pobj.addBtn);
				 * fc.utobj().switchFrameToDefault(driver); }
				 */

				/*
				 * for (int j = 0; j < 25; j++) {
				 * fc.utobj().printTestStep(testCaseId,
				 * "Add Multiple Input Type Question In Visit Form");
				 * fc.utobj().getElement(driver,
				 * pobj.addQuestion).click(); fc.utobj().switchFrameById(driver,
				 * "cboxIframe");
				 * 
				 * String questionTextForMultiInput =
				 * fc.utobj().generateTestData(dataSet.get(
				 * "questionTextForMultiInput")); fc.utobj().sendKeys(driver,
				 * pobj.questionTextArea, questionTextForMultiInput);
				 * 
				 * fc.utobj().selectDropDown(driver, pobj.criticalLevel,
				 * dataSet.get("criticalLevel"));
				 * fc.utobj().clickElement(driver, pobj.nextBtn);
				 * 
				 * 
				 * fc.utobj().selectDropDownByValue(driver,
				 * pobj.selectResponseType, "multiinput");
				 * fc.utobj().clickElement(driver, pobj.scoreInclude);
				 * fc.utobj().clickElement(driver, pobj.excludeNonResponse);
				 * fc.utobj().clickElement(driver, pobj.notApplicableResponse);
				 * fc.utobj().clickElement(driver,
				 * pobj.complianceNotApplicable); fc.utobj().sendKeys(driver,
				 * pobj.inputLabels1, "Input Label 1");
				 * fc.utobj().sendKeys(driver, pobj.inputLabels2,
				 * "Input Label 2"); fc.utobj().sendKeys(driver,
				 * pobj.calculationResultLabel, "Calculation Result Label");
				 * fc.utobj().sendKeys(driver, pobj.rangeScore, "100");
				 * fc.utobj().clickElement(driver, pobj.addBtn);
				 * fc.utobj().switchFrameToDefault(driver); }
				 */
			}

			fc.utobj().clickElement(driver, pobj.finishBtn);

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	/*
	 * 
	 * Data From XLS sheet : Question
	 * 
	 */

	@Test(groups = "TestVipin")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Add Question", testCaseId = "TC_14_Add_Question_Section")
	private void addQuestionInSection() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		// Map<String, String> dataSet = fc.utobj().readTestData("fieldops",
		// testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			AdminFieldOpsManageVisitFormPage pobj = new AdminFieldOpsManageVisitFormPage(driver);

			// add section and question
			ArrayList<String> questionName = new ArrayList<String>();
			GetTestData sbc = new GetTestData();

			ArrayList<String> listArray = new ArrayList<String>();
			/*
			 * listArray.add("Minimum Standards"); listArray.add(
			 * "Cleanliness / Environment"); listArray.add(
			 * "Cafeteria Observation"); listArray.add("After School Program");
			 */

			listArray.add("Physical Environment");
			listArray.add("Curriculum");
			listArray.add("Health & Safety");
			listArray.add("Interactions");

			fc.utobj().printTestStep("Navigate To Admin > FieldOps > Manage Visit Form Page");
			fc.utobj().printTestStep("Add Private Visit Form");

			fc.fieldops().fieldops_common().adminFieldOpsManageVisitForm(driver);
			fc.utobj().clickElement(driver, pobj.addVisitForm);

			fc.utobj().sendKeys(driver, pobj.visitFormName, "Classroom");
			fc.utobj().selectDropDown(driver, pobj.selectFrequency, "Not Applicable");
			if (fc.utobj().isSelected(driver,pobj.isPrivate)) {
				fc.utobj().clickElement(driver, pobj.isPrivate);
			}
			fc.utobj().selectDropDown(driver, pobj.type, "Single Page");
			if (!fc.utobj().isSelected(driver,pobj.listLayout)) {
				fc.utobj().clickElement(driver, pobj.listLayout);
			}
			if (!fc.utobj().isSelected(driver,pobj.showResponseScore)) {
				fc.utobj().clickElement(driver, pobj.showResponseScore);
			}
			fc.utobj().clickElement(driver, pobj.saveAndNextBtn);

			for (int i = 0; i < listArray.size(); i++) {

				String sectionName = listArray.get(i);

				// Add Section
				AdminFieldOpsManageVisitFormCustomizeVisitFormPage pobj123 = new AdminFieldOpsManageVisitFormCustomizeVisitFormPage(
						driver);
				fc.utobj().clickElementByJS(driver, fc.utobj().getElement(driver, pobj123.addSection));
				fc.commonMethods().switch_cboxIframe_frameId(driver);
				fc.utobj().sendKeys(driver, pobj123.sectionName, sectionName);
				fc.utobj().clickElement(driver, pobj123.saveBtn);
				fc.utobj().switchFrameToDefault(driver);

				questionName = sbc.getTestDataFromRow(sectionName);

				for (int j = 0; j < questionName.size(); j++) {

					// Add Radio Type Question
					fc.utobj().clickElementByJS(driver, fc.utobj().getElementByXpath(driver,
							".//*[contains(text(),'" + sectionName + "')]/../a[@id='addqueslink']"));

					fc.commonMethods().switch_cboxIframe_frameId(driver);

					fc.utobj().sendKeys(driver, pobj123.questionTextArea, questionName.get(j));
					fc.utobj().clickElement(driver, pobj123.addToQueLib);
					fc.utobj().selectDropDown(driver, pobj123.criticalLevel, "System Item");
					fc.utobj().clickElement(driver, pobj123.nextBtn);

					AdminFieldOpsQuestionLibraryPage question_Library = new AdminFieldOpsQuestionLibraryPage(driver);
					fc.utobj().selectDropDownByValue(driver, pobj123.selectResponseType, "radio");
					fc.utobj().clickElement(driver, fc.utobj().getElementByID(driver, "withComments"));
					fc.utobj().clickElement(driver, fc.utobj().getElementByID(driver, "withAttachments"));
					fc.utobj().clickElement(driver, pobj123.scoreInclude);
					fc.utobj().sendKeys(driver, question_Library.firstChoice, "Yes");
					fc.utobj().sendKeys(driver, question_Library.secondChoice, "No");
					fc.utobj().sendKeys(driver, question_Library.firstScore, "1");
					fc.utobj().sendKeys(driver, question_Library.secondScore, "0");
					fc.utobj().clickElement(driver, pobj123.addBtn);
					fc.utobj().switchFrameToDefault(driver);
				}
			}

			fc.utobj().clickElement(driver, pobj.finishBtn);

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}
	
	@Test(groups = "fieldops1")
	@TestCase(createdOn = "", updatedOn = "", testCaseDescription = "Create a visit having 500 questions", testCaseId = "TC_500_Questions_Visit")

	public void creatVisitWith500Questions() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

			Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
			Map<String, String> dataSet = fc.utobj().readTestData(testCaseId);

			WebDriver driver = fc.commonMethods().browsers().openBrowser();
			AdminFieldOpsManageVisitFormCustomizeVisitFormPageTest CustomizeVisit = new AdminFieldOpsManageVisitFormCustomizeVisitFormPageTest();
			try {
				driver = fc.loginpage().login(driver);
				fc.utobj().printTestStep("logged in into build");
				//fc.utobj().printTestStep("Add Consultant");
				
				//fc.utobj().printTestStep("Navigate To Admin > Fieldops > Manage Visit Form");
				//fc.utobj().printTestStep("Add Visit Form with non compliance tasks");
							
				int x = 0, y=0;
				
				String Layout = "Table";
				
				String visitFormName = fc.utobj().generateTestData("GT");
				visitFormName = CustomizeVisit.addVisitFormTabViewPrivateNo(driver, config, dataSet, visitFormName);
				
				for (int i = 0; i < 10; i++) {
					
					String tabName = "Tab "+(i+1);
					addTab(driver, config, tabName);
					
					for (int j = 0; j < 2; j++, y++) {
						
						WebElement e1 = driver.findElement(By.xpath(".//a[.='"+tabName+"']"));
						fc.utobj().clickElement(driver, e1);
						String section = "Section "+(y+1);
						addSection(driver, config, section);
						
						for (int k = 0; k < 10; k++, x++) {
							
							fc.utobj().clickElementByJS(driver, driver.findElement(By.xpath(".//*[contains(text(), '"+section+"')]/a[@id='addqueslink']")));
							String questionText = "Question Single Select Radio Button "+(x+1);
							addQuestionSingleSelectRadioButtonTypeWithAttachmentsAndTask(driver, config, dataSet, questionText );
											
							/*													
							fc.utobj().clickElementByJS(driver, driver.findElement(By.xpath(".//*[contains(text(), '"+section+"')]/a[@id='addqueslink']")));
							questionText = "QuestionRadioType "+(x+1);
							CustomizeVisit.addQuestionRatingType(driver, config, dataSet, questionText);
							
							fc.utobj().printTestStep("Now add new simple scorable question");
							questionText = "SingleSelectDropDownQuestion "+(x+1);
							fc.utobj().clickElementByJS(driver, driver.findElement(By.xpath(".//*[contains(text(), '"+section+"')]/a[@id='addqueslink']")));
							CustomizeVisit.addQuestionSingleSelectDropDownType(driver, config, dataSet, questionText);
							
							fc.utobj().printTestStep("Now add new paragraph question");
							questionText = "ParagraphType "+(x+1);
							fc.utobj().clickElementByJS(driver, driver.findElement(By.xpath(".//*[contains(text(), '"+section+"')]/a[@id='addqueslink']")));
							CustomizeVisit.addQuestionParagraphType(driver, config, dataSet, questionText);
							
							fc.utobj().printTestStep("Now add new Single Line Text type question");
							questionText = "SingleLineTextQuestionType "+(x+1);
							fc.utobj().clickElementByJS(driver, driver.findElement(By.xpath(".//*[contains(text(), '"+section+"')]/a[@id='addqueslink']")));
							CustomizeVisit.addQuestionSingleLineTextType(driver, config, dataSet, questionText);
							
							fc.utobj().printTestStep("Now add new Number type question");
							questionText = "NumberTypeQuestion "+(x+1);
							fc.utobj().clickElementByJS(driver, driver.findElement(By.xpath(".//*[contains(text(), '"+section+"')]/a[@id='addqueslink']")));
							CustomizeVisit.addQuestionNumberType(driver, config, dataSet, questionText);
							
							fc.utobj().printTestStep("Now add new Date type question");
							questionText = "DateTypeQuestion "+(x+1);
							fc.utobj().clickElementByJS(driver, driver.findElement(By.xpath(".//*[contains(text(), '"+section+"')]/a[@id='addqueslink']")));
							CustomizeVisit.addQuestionDateType(driver, config, dataSet, questionText);
							
							fc.utobj().printTestStep("Now add new MultiSelect Input Type question");
							questionText = "MultipleInputTypeQuestion "+(x+1);
							fc.utobj().clickElementByJS(driver, driver.findElement(By.xpath(".//*[contains(text(), '"+section+"')]/a[@id='addqueslink']")));
							CustomizeVisit.addQuestionMultipleInputType(driver, config, dataSet, questionText);
							
							fc.utobj().printTestStep("Now add new MultiSelect Checkbox type question");
							questionText = "MultiSelectCheckboxTypeQuestion "+(x+1);
							fc.utobj().clickElementByJS(driver, driver.findElement(By.xpath(".//*[contains(text(), '"+section+"')]/a[@id='addqueslink']")));
							CustomizeVisit.addQuestionMultiSelectCheckboxType(driver, config, dataSet, questionText);
							
							fc.utobj().printTestStep("Add new smart question without any score.");
							questionText = "SmartQuestionType "+(x+1);
							fc.utobj().clickElementByJS(driver, driver.findElement(By.xpath(".//*[contains(text(), '"+section+"')]/a[@id='addqueslink']")));
							CustomizeVisit.addSmartTypeQuestion(driver, config, dataSet, questionText);
*/
						}
					}
				}
				
				/*
				 // Add Questions in library
				  
				 fc.fieldops().fieldops_common().adminFieldOpsQuestionLibrary(driver);
				 fc.utobj().printTestStep("Navigate To Admin > Field Ops > Question Library");
				 
				 AdminFieldOpsQuestionLibraryPage pobj = new AdminFieldOpsQuestionLibraryPage(driver);
				 
				 int x = 0;
				 for (int i = 0; i < 1000; i++) {
				 
				 fc.utobj().clickElement(driver, pobj.addQuestion);
				 String questionText = "Question Single Select Radio Button "+(x+1);
				 addQuestionSingleSelectRadioButtonTypeWithAttachmentsAndTask(driver, config, dataSet, questionText );
				 
				 }
				*/
				fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

			} catch (Exception e) {

				fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
			}
		}
		
	public String addQuestionSingleSelectRadioButtonTypeWithAttachmentsAndTask(WebDriver driver,
			Map<String, String> config, Map<String, String> dataSet, String questionText ) throws Exception {

			try {
				AdminFieldOpsQuestionLibraryPage pobj = new AdminFieldOpsQuestionLibraryPage(driver);
				fc.commonMethods().switch_cboxIframe_frameId(driver);
				
				fc.utobj().sendKeys(driver, pobj.questionTextArea, questionText);
				fc.utobj().sendKeys(driver, pobj.helpTextForUser, "Test Question");
				fc.utobj().clickElement(driver, pobj.questionMandantory);
				/* fc.utobj().clickElement(driver, pobj.allowPrivateComment); */
				//fc.utobj().selectDropDown(driver, pobj.criticalLevel, dataSet.get("criticalLevel"));
				fc.utobj().clickElement(driver, pobj.nextBtn);
				
				// Response page
				fc.utobj().selectDropDown(driver, pobj.selectResponseType, "Single Select (Radio Button)");
				
				if (pobj.allowAttachments.isDisplayed()) {
					fc.utobj().clickElement(driver, pobj.allowAttachments);
					
					/*
					String allowAttachments = dataSet.get("allowAttachments");
					if (allowAttachments.equalsIgnoreCase("Yes")) {
						fc.utobj().clickElement(driver, pobj.allowAttachments);
					}*/
				
				}
				WebElement e1 = driver.findElement(By.xpath(
						".//td[contains(text () , 'Choices')]//following::td/input[@name='optionResponseName']/../../../tr[@id='row_1']/td/input[@name='complianceCheck']"));
				WebElement e2 = driver.findElement(By.xpath(
						".//td[contains(text () , 'Choices')]//following::td/input[@name='optionResponseName']/../../../tr[@id='row_2']/td/input[@name='complianceCheck']"));

				fc.utobj().clickElement(driver, pobj.scoreInclude);
				fc.utobj().clickElement(driver, pobj.notApplicableResponse);
				//fc.utobj().clickElement(driver, pobj.excludeNonResponse);

				fc.utobj().sendKeys(driver, pobj.firstChoice, "Yes");
				fc.utobj().sendKeys(driver, pobj.firstScore, "10");
				fc.utobj().sendKeys(driver, pobj.secondChoice, "No");
				fc.utobj().sendKeys(driver, pobj.secondScore, "0");

				String addNonComTask = "Yes";
				
				if (addNonComTask.equalsIgnoreCase("Yes")) {
					
					String nonComOption =  "responsechoice"; //dataSet.get("nonComOption");
					
					if (nonComOption.equalsIgnoreCase("responsechoice")) {
						
						fc.utobj().clickElement(driver, pobj.complianceByOption);
						
						fc.utobj().clickElement(driver, e2);
						
						fc.utobj().clickElement(driver, pobj.assignTaskonNoncompliance);

					} else if (nonComOption.equalsIgnoreCase("questionScore")) {

						fc.utobj().clickElement(driver, pobj.complianceByValue);
						
						String nonComScore = dataSet.get("nonComScore");
						
						fc.utobj().sendKeys(driver, pobj.complianceInput, nonComScore);
					}

				} 

				String assignAct = "New"; //dataSet.get("assignAction");

				if (assignAct.equalsIgnoreCase("New")) {
					
					fc.utobj().clickElement(driver, pobj.newAction);
					
					String taskSubject = questionText + "task";
					
					fc.utobj().sendKeys(driver, pobj.actionSubject, taskSubject);
					
					fc.utobj().selectDropDownByVisibleText(driver, pobj.actionPriority, "High");
					
					fc.utobj().sendKeys(driver, pobj.scheduleCompletionBox, "1");
					
					fc.utobj().sendKeys(driver, pobj.descriptionBox, taskSubject);

				} else {
					fc.utobj().clickElement(driver, pobj.actionFromLibrary);
					
					fc.utobj().selectDropDownByIndex(driver, pobj.actionFromLibrary, 2);

				}
				fc.utobj().clickElement(driver, pobj.saveBtn);
				
				fc.utobj().switchFrameToDefault(driver);
				
			} catch (Exception e) {
				
				fc.utobj().throwsException("Question Single Select Radio Button Type With Attachments And Task is not added");
			}

		return questionText;
	}
	
	

	@Test(groups = "fieldopsMSA")
	@TestCase(createdOn = "", updatedOn = "", testCaseDescription = "Create Visit", testCaseId = "TC_FieldOps_Visits_CreateVisit")

	public void creatVisitMSA() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

			Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
			Map<String, String> dataSet = fc.utobj().readTestData(testCaseId);

			WebDriver driver = fc.commonMethods().browsers().openBrowser();
			AdminFieldOpsManageVisitFormCustomizeVisitFormPageTest CustomizeVisit = new AdminFieldOpsManageVisitFormCustomizeVisitFormPageTest();
			try {
				driver = fc.loginpage().login(driver);
				fc.utobj().printTestStep("logged in into build");
				//fc.utobj().printTestStep("Add Consultant");
				
				fc.utobj().printTestStep("Navigate To Fieldops > Visits > Create Visit");
				fc.fieldops().fieldops_common().fieldopsVisitCreateVisitPage(driver);
				
				fc.utobj().clickElement(driver, driver.findElement(By.xpath(".//button[@class='mat-raised-button mat-accent']")));
				
				
				
				fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

			} catch (Exception e) {

				fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
			}
		}
		
	
	
	
	
	
	
	
	
	
	
	
	
}
