package com.builds.test.fin;

import static org.testng.Assert.assertEquals;
import java.io.IOException;
import java.util.Map;
import javax.xml.parsers.ParserConfigurationException;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.Test;
import org.xml.sax.SAXException;
import com.builds.uimaps.fin.AdminFinanceAdditionalInvoiceItemSummaryPage;
import com.builds.uimaps.fin.AdminFinanceAgreementVersionsPage;
import com.builds.utilities.FranconnectUtil;
import com.builds.utilities.TestCase;

public class AdminFinanceAdditionalInvoiceItemSummaryPageTest {
	FranconnectUtil fc = new FranconnectUtil();

	@Test(groups = { "finance" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-05-22", testCaseId = "TC_FC_Additional_Invoice_Item_Summary_042", testCaseDescription = "Verify the Configure Additional Invoice Item(s) Summary")
	private void addAdditionalInvoiceItems() throws ParserConfigurationException, SAXException, IOException, Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("finance", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		
		String itemValue= "00", itemType="Fixed";
		
		try {
			driver = fc.loginpage().login(driver);
			
			AdminFinanceAdditionalInvoiceItemSummaryPage pobj = new AdminFinanceAdditionalInvoiceItemSummaryPage(
					driver);
			
			String invoicItem = fc.utobj().generateTestData("InvItm");
			
			fc.utobj().printTestStep("Navigate To Admin > Finance > Additional Invoice Item(s) Summary");
			
			fc.finance().finance_common().adminFinConfigureAdditionalInvoiceItemPage(driver);
			
			fc.utobj().printTestStep("Add New Item");
			
			fc.utobj().clickElement(driver, pobj.addNewItem);
			
			//invoicItem = addAdditionalInvoiceItems(driver, invoicItem, itemValue, itemType);
			
			fc.commonMethods().switch_cboxIframe_frameId(driver);

			fc.utobj().sendKeys(driver, pobj.invoiceItem, invoicItem);
	
			fc.utobj().sendKeys(driver, pobj.defaultValue, itemValue);
			
			if(itemType.equalsIgnoreCase("Fixed")) {
				
				if (!fc.utobj().isSelected(driver,pobj.fixed)) {
					
					fc.utobj().clickElement(driver, pobj.fixed);
				}
			}
			
			if (itemType.equalsIgnoreCase("Variable")) {
		
				if (!fc.utobj().isSelected(driver,pobj.variable)) {
			
					fc.utobj().clickElement(driver, pobj.variable);
				}
			}
			
			fc.utobj().clickElement(driver, pobj.saveBtn);

			fc.utobj().printTestStep("Verify Confirmation Alert");
	
			String actualConfirmationAlert = fc.utobj().getText(driver, pobj.confirmationText);
			
			String expectedConfirmationAlert = "Item added successfully."; //Item updated successfully.
			
			assertEquals(actualConfirmationAlert, expectedConfirmationAlert, "Confirmation message is wrong");	
	
			fc.utobj().clickElement(driver, pobj.closeBtn);
			
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Verify Added Additional Invoice");

			boolean isTextPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//td[contains(text () , '" + invoicItem + "')]");

			if (isTextPresent == false) {
				
				fc.utobj().selectDropDownByValue(driver, new AdminFinanceAgreementVersionsPage(driver).viewPerPageSelect, "500");
				{
					boolean isTextPresent1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
							".//td[contains(text () , '" + invoicItem + "')]");
					
					if (isTextPresent1 == false) {
						
						fc.utobj().throwsException("Was not able to add Additional Invoice");
					}
				}
			}

			fc.utobj().printTestStep("Modify Invoice Item");
			
			fc.utobj().actionImgOption(driver, invoicItem, "Modify");

			invoicItem = fc.utobj().generateTestData("InvItm");
			
			itemType="Variable";
			
			//invoicItem = addAdditionalInvoiceItems(driver, invoicItem, itemValue, itemType);
			
			fc.commonMethods().switch_cboxIframe_frameId(driver);

			fc.utobj().sendKeys(driver, pobj.invoiceItem, invoicItem);
	
			fc.utobj().sendKeys(driver, pobj.defaultValue, itemValue);
			
			if(itemType.equalsIgnoreCase("Fixed")) {
				
				if (!fc.utobj().isSelected(driver,pobj.fixed)) {
					
					fc.utobj().clickElement(driver, pobj.fixed);
				}
			}
			
			if (itemType.equalsIgnoreCase("Variable")) {
		
				if (!fc.utobj().isSelected(driver,pobj.variable)) {
			
					fc.utobj().clickElement(driver, pobj.variable);
				}
			}
			
			fc.utobj().clickElement(driver, pobj.saveBtn);

			fc.utobj().printTestStep("Verify Confirmation Alert");
	
			String actualConfirmationAlert1 = fc.utobj().getText(driver, pobj.confirmationText);
			
			String expectedConfirmationAlert1 = "Item updated successfully.";
			
			assertEquals(actualConfirmationAlert1, expectedConfirmationAlert1, "Confirmation message is wrong");	
	
			fc.utobj().clickElement(driver, pobj.closeBtn);
			
			fc.utobj().switchFrameToDefault(driver);

			boolean isTextPresent1 = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//td[contains(text () , '" + invoicItem + "')]");

			if (isTextPresent1 == false) {
				
				fc.utobj().selectDropDownByValue(driver,
						
						new AdminFinanceAgreementVersionsPage(driver).viewPerPageSelect, "500");
				{
					boolean isTextPresent2 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
							".//td[contains(text () , '" + invoicItem + "')]");
					
					if (isTextPresent2 == false) {
						
						fc.utobj().throwsException("Was not able to Modify Additional Invoice Item");
					}
				}
			}

			fc.utobj().printTestStep("Deactivate Invoice Item");
			
			fc.utobj().actionImgOption(driver, invoicItem, "Deactivate");

			fc.commonMethods().switch_cboxIframe_frameId(driver);
			
			fc.utobj().clickElement(driver, pobj.saveBtn);

			fc.utobj().clickElement(driver, pobj.closeBtn);
			
			fc.utobj().switchFrameToDefault(driver);

			boolean isTextPresent3 = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//td[contains(text () , 'Inactive')]");

			if (isTextPresent3 == false) {
				
				fc.utobj().selectDropDownByValue(driver,
						
						new AdminFinanceAgreementVersionsPage(driver).viewPerPageSelect, "500");
				{
					boolean isTextPresent4 = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//td[contains(text () , 'Inactive')]");
					
					if (isTextPresent4 == false) {
						fc.utobj().throwsException("Was not able to Deactivate Invoice Item");
					}
				}
			}

			fc.utobj().printTestStep("Activate Invoice Item");
			
			fc.utobj().actionImgOption(driver, invoicItem, "Activate");

			fc.commonMethods().switch_cboxIframe_frameId(driver);
			
			fc.utobj().clickElement(driver, pobj.saveBtn);

			fc.utobj().clickElement(driver, pobj.closeBtn);
			
			fc.utobj().switchFrameToDefault(driver);

			boolean isTextPresent4 = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//td[contains(text () , 'Active')]");

			if (isTextPresent4 == false) {
				fc.utobj().selectDropDownByValue(driver,
						new AdminFinanceAgreementVersionsPage(driver).viewPerPageSelect, "500");
				{
					boolean isTextPresent5 = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//td[contains(text () , 'Active')]");
					if (isTextPresent5 == false) {
						
						fc.utobj().throwsException("Was not able to Activate Invoice Item");
					}
				}
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	
	@Test(groups = { "finance" })
	@TestCase(createdOn = "2018-05-22", updatedOn = "2018-05-22", testCaseId = "TC_FC_Additional_Invoice_Item_Summary_042_1", 
									testCaseDescription = "Verify Duplicate Alert Message for Configure Additional Invoice Item(s) Summary")
	private void verifyDuplicateAdditionalInvoiceItems() throws ParserConfigurationException, SAXException, IOException, Exception {
		
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());
		
		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("finance", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		String invoicItem = fc.utobj().generateTestData("InvItem");
		String itemValue= "00", itemType="Fixed";
		
		try {
			driver = fc.loginpage().login(driver);
			
			AdminFinanceAdditionalInvoiceItemSummaryPage pobj = new AdminFinanceAdditionalInvoiceItemSummaryPage(
					driver);
			
			fc.utobj().printTestStep("Navigate To Admin > Finance > Additional Invoice Item(s) Summary");
			
			/*
			fc.finance().finance_common().adminFinConfigureAdditionalInvoiceItemPage(driver);
			
			fc.utobj().printTestStep("Add New Item");
			
			fc.utobj().clickElement(driver, pobj.addNewItem);
			*/
			
			addAdditionalInvoiceItems(driver, invoicItem, itemValue, itemType);
			
			fc.utobj().printTestStep("Verify The Add Additional Invoice");
			
			boolean isTextPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//td[contains(text () , '" + invoicItem + "')]");
			
			if (isTextPresent == false) {
				
				fc.utobj().selectDropDownByValue(driver,
						
						new AdminFinanceAgreementVersionsPage(driver).viewPerPageSelect, "500");
				{
					boolean isTextPresent1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
							".//td[contains(text () , '" + invoicItem + "')]");
					
					if (isTextPresent1 == false) {
						
						fc.utobj().throwsException("Was not able to add Additional Invoice");
					}
				}
			}
			
			String expectedAlert = "This Item already exists in the system. If Item status is inactive, please activate.";
			
			fc.utobj().clickElement(driver, pobj.addNewItem);
			
			fc.commonMethods().switch_cboxIframe_frameId(driver);

			fc.utobj().sendKeys(driver, pobj.invoiceItem, invoicItem);
			
			fc.utobj().sendKeys(driver, pobj.defaultValue, itemValue);
			
			if(itemType.equalsIgnoreCase("Fixed")) {
			
				if (!fc.utobj().isSelected(driver,pobj.fixed)) {
					
					fc.utobj().clickElement(driver, pobj.fixed);
				}
			}
			
			if (itemType.equalsIgnoreCase("Variable")) {
				
				if (!fc.utobj().isSelected(driver,pobj.variable)) {
					
					fc.utobj().clickElement(driver, pobj.variable);
					}
				}
			
			fc.utobj().clickElement(driver, pobj.saveBtn);
			
			String ActualConfAlert = driver.findElement(By.xpath(".//td[contains(text(), 'Confirmation')]/ancestor::tr[2]//td[contains(text(), 'Item already exists')]")).getText();
			
			if(!expectedAlert.equalsIgnoreCase(ActualConfAlert)) {
				
				fc.utobj().throwsException("Duplicate alert message is not verified for Additional Invoice Item(s)");
			}
			
			fc.utobj().clickElement(driver, pobj.closeBtn);
			
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	
	public String addAdditionalInvoiceItems(WebDriver driver, String invoicItem, String itemValue, String itemType) throws Exception {

		AdminFinanceAdditionalInvoiceItemSummaryPage pobj = new AdminFinanceAdditionalInvoiceItemSummaryPage(driver);
			
		fc.utobj().printTestStep("Navigate To Admin > Finance > Additional Invoice Item(s) Summary");
		
		fc.finance().finance_common().adminFinConfigureAdditionalInvoiceItemPage(driver);
		
		fc.utobj().printTestStep("Add New Item");
		
		fc.utobj().clickElement(driver, pobj.addNewItem);
		
		fc.commonMethods().switch_cboxIframe_frameId(driver);

		fc.utobj().sendKeys(driver, pobj.invoiceItem, invoicItem);
		
		fc.utobj().sendKeys(driver, pobj.defaultValue, itemValue);
		
		if(itemType.equalsIgnoreCase("Fixed")) {
		
			if (!fc.utobj().isSelected(driver,pobj.fixed)) {
				
				fc.utobj().clickElement(driver, pobj.fixed);
			}
		}
		if (itemType.equalsIgnoreCase("Variable")) {
			
			if (!fc.utobj().isSelected(driver,pobj.variable)) {
				
				fc.utobj().clickElement(driver, pobj.variable);
				}
			}
		
		fc.utobj().clickElement(driver, pobj.saveBtn);

		fc.utobj().printTestStep("Verify Confirmation Alert");
		
		String actualConfirmationAlert = fc.utobj().getText(driver, pobj.confirmationText);
	
		String expectedConfirmationAlert = "Item added successfully."; //Item updated successfully.
		
		assertEquals(actualConfirmationAlert, expectedConfirmationAlert, "Confirmation message is wrong");	
		
		fc.utobj().clickElement(driver, pobj.closeBtn);
		
		fc.utobj().switchFrameToDefault(driver);
		
		return invoicItem;
	}
}
