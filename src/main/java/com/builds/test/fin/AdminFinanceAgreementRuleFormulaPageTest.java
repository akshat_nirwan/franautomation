package com.builds.test.fin;

import java.util.Map;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.Test;
import com.builds.uimaps.fin.AdminFinanceAgreementRuleFormulaPage;
import com.builds.utilities.FranconnectUtil;
import com.builds.utilities.TestCase;


public class AdminFinanceAgreementRuleFormulaPageTest {

	FranconnectUtil fc = new FranconnectUtil();

	@Test(groups = { "finance" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "TC_Admin_Finance_001", testCaseDescription = "Verify Rule Formula Sales Amount Percent With Growth Based")
	private void addRuleFormulaSalesAmountPercentWithGrowthBased() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);

		Map<String, String> dataSet = fc.utobj().readTestData("finance", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		String formulaNameSAPG = fc.utobj().generateTestData(dataSet.get("AddRuleFormulaSAPG"));
		String rngToFieldZeroRow = dataSet.get("rangeToFieldZeroRow");
		String apldValueForZeroRow = dataSet.get("appliedValueForZeroRow");
		String apldValueForFirstRow = dataSet.get("appliedValueForFirstRow");

		try {
			driver = fc.loginpage().login(driver);
			AdminFinanceAgreementRuleFormulaPage pobj = new AdminFinanceAgreementRuleFormulaPage(driver);

			fc.utobj().printTestStep("Admin >  Finance > Rule Formula");
			fc.utobj().printTestStep(
					"Add Rule Formula with type Sales Amount Based and apply as a percentage with growth Based");
			formulaNameSAPG = addRuleFormulaSalesAmountPercentWithGrowth(driver, config, formulaNameSAPG,
					rngToFieldZeroRow, apldValueForZeroRow, apldValueForFirstRow);

			fc.utobj().printTestStep(
					"Verify Add Rule Formula with type Sales Amount Based and apply as a percentage with growth Based");

			boolean isTextPresent = fc.utobj().assertLinkText(driver, formulaNameSAPG);
			if (isTextPresent == false) {
				fc.utobj().selectDropDownByValue(driver, pobj.selectViewPerPage, "500");
				{
					boolean isTextPresent1 = fc.utobj().assertLinkText(driver, formulaNameSAPG);
					if (isTextPresent1 == false) {
						fc.utobj().throwsException("Rule Formula Sales Amount Percent With Growth Based is Not added!");
					}
				}
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	public String addRuleFormulaSalesAmountPercentWithGrowth(WebDriver driver, Map<String, String> config,
			String formulaNameSAPG, String rngToFieldZeroRow, String apldValueForZeroRow, String apldValueForFirstRow)
			throws Exception {

		fc.finance().finance_common().adminFinAgreementVersionsPage(driver);
		AdminFinanceAgreementRuleFormulaPage pobj = new AdminFinanceAgreementRuleFormulaPage(driver);
		fc.utobj().clickElement(driver, pobj.RuleFormulaBtn);
		fc.utobj().clickElement(driver, pobj.AddRuleFormulaBtn);
		fc.utobj().clickElement(driver, pobj.RFSalesAmountBased);
		fc.utobj().clickElement(driver, pobj.ContinueBtn);
		fc.utobj().sendKeys(driver, pobj.RuleFormulaName, formulaNameSAPG);
		fc.utobj().clickElement(driver, pobj.PercentageRadioBtn);
		fc.utobj().clickElement(driver, pobj.GrothBasedYesRadioBtn);
		int rowCount = driver.findElements(By.xpath("//table[@id='table1']/tbody")).size();
		if (rowCount == 1) {
			fc.utobj().clickElement(driver, pobj.AddMoreBtn);
		}
		fc.utobj().sendKeys(driver, pobj.SetRangeToFieldZeroRow, rngToFieldZeroRow);
		fc.utobj().sendKeys(driver, pobj.AppliedValueForZeroRow, apldValueForZeroRow);
		fc.utobj().sendKeys(driver, pobj.AppliedValueForFirstRow, apldValueForFirstRow);
		fc.utobj().clickElement(driver, pobj.SaveBtn);
		return formulaNameSAPG;
	}

	@Test(groups = { "finance" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "TC_Admin_Finance_002", testCaseDescription = "Verify Rule Formula Sales Amount Percent without Growth Based")
	private void addRuleFormulaSalesAmountPercentWithoutGrowthBased() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("finance", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		String formulaNameSAP = fc.utobj().generateTestData(dataSet.get("AddRuleFormulaSAP"));
		String rngToFieldZeroRow = dataSet.get("rangeToFieldZeroRow");
		String apldValueForZeroRow = dataSet.get("appliedValueForZeroRow");
		String apldValueForFirstRow = dataSet.get("appliedValueForFirstRow");

		try {
			driver = fc.loginpage().login(driver);
			AdminFinanceAgreementRuleFormulaPage pobj = new AdminFinanceAgreementRuleFormulaPage(driver);
			fc.utobj().printTestStep("Admin >  Finance > Rule Formula");
			fc.utobj().printTestStep(
					"Add Rule Formula with type Sales Amount Based and apply as a percentage without growth Based");

			formulaNameSAP = addRuleFormulaSalesAmountPercentWithoutGrowth(driver, config, formulaNameSAP,
					rngToFieldZeroRow, apldValueForZeroRow, apldValueForFirstRow);

			fc.utobj().printTestStep(
					"Verify Add Rule Formula with type Sales Amount Based and apply as a percentage without growth Based");

			boolean isTextPresent = fc.utobj().assertLinkText(driver, formulaNameSAP);
			if (isTextPresent == false) {
				fc.utobj().selectDropDownByValue(driver, pobj.selectViewPerPage, "500");
				{
					boolean isTextPresent1 = fc.utobj().assertLinkText(driver, formulaNameSAP);
					if (isTextPresent1 == false) {
						fc.utobj().throwsException(
								"Rule Formula Sales Amount Percent without Growth Based is Not added!");
					}
				}
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	public String addRuleFormulaSalesAmountPercentWithoutGrowth(WebDriver driver, Map<String, String> config,
			String formulaNameSAP, String rngToFieldZeroRow, String apldValueForZeroRow, String apldValueForFirstRow)
			throws Exception {

		fc.finance().finance_common().adminFinAgreementVersionsPage(driver);
		AdminFinanceAgreementRuleFormulaPage pobj = new AdminFinanceAgreementRuleFormulaPage(driver);
		fc.utobj().clickElement(driver, pobj.RuleFormulaBtn);
		fc.utobj().clickElement(driver, pobj.AddRuleFormulaBtn);
		fc.utobj().clickElement(driver, pobj.RFSalesAmountBased);
		fc.utobj().clickElement(driver, pobj.ContinueBtn);
		fc.utobj().sendKeys(driver, pobj.RuleFormulaName, formulaNameSAP);
		fc.utobj().clickElement(driver, pobj.PercentageRadioBtn);
		fc.utobj().clickElement(driver, pobj.GrothBasedNoRadioBtn);
		int rowCount = driver.findElements(By.xpath("//table[@id='table1']/tbody")).size();
		if (rowCount == 1) {
			fc.utobj().clickElement(driver, pobj.AddMoreBtn);
		}
		fc.utobj().sendKeys(driver, pobj.SetRangeToFieldZeroRow, rngToFieldZeroRow);
		fc.utobj().sendKeys(driver, pobj.AppliedValueForZeroRow, apldValueForZeroRow);
		fc.utobj().sendKeys(driver, pobj.AppliedValueForFirstRow, apldValueForFirstRow);
		fc.utobj().clickElement(driver, pobj.SaveBtn);
		return formulaNameSAP;
	}

	@Test(groups = "finance")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "TC_FC_RuleFormula_SalesAmount_Flat_Growth_003", testCaseDescription = "Verify Rule Formula Sales Amount Flat With Growth Based")
	private void addRuleFormulaSalesAmountFlatWithGrowthBased() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("finance", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		String formulaNameSAFG = fc.utobj().generateTestData(dataSet.get("AddRuleFormulaSAFG"));
		String rngToFieldZeroRow = dataSet.get("rangeToFieldZeroRow");
		String apldValueForZeroRow = dataSet.get("appliedValueForZeroRow");
		String apldValueForFirstRow = dataSet.get("appliedValueForFirstRow");

		try {
			driver = fc.loginpage().login(driver);
			AdminFinanceAgreementRuleFormulaPage pobj = new AdminFinanceAgreementRuleFormulaPage(driver);
			fc.utobj().printTestStep("Admin >  Finance > Rule Formula");
			fc.utobj().printTestStep(
					"Add Rule Formula with type Sales Amount Based and apply as a Flat with growth Based");

			formulaNameSAFG = addRuleFormulaSalesAmountFlatWithGrowth(driver, config, formulaNameSAFG,
					rngToFieldZeroRow, apldValueForZeroRow, apldValueForFirstRow);

			fc.utobj().printTestStep(
					"Verify Add Rule Formula with type Sales Amount Based and apply as a Flat with growth Based");

			boolean isTextPresent = fc.utobj().assertLinkText(driver, formulaNameSAFG);
			if (isTextPresent == false) {
				fc.utobj().selectDropDownByValue(driver, pobj.selectViewPerPage, "500");
				{
					boolean isTextPresent1 = fc.utobj().assertLinkText(driver, formulaNameSAFG);
					if (isTextPresent1 == false) {
						fc.utobj().throwsException("Rule Formula Sales Amount Flat With Growth Based is Not added!");
					}
				}
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	public String addRuleFormulaSalesAmountFlatWithGrowth(WebDriver driver, Map<String, String> config,
			String formulaNameSAFG, String rngToFieldZeroRow, String apldValueForZeroRow, String apldValueForFirstRow)
			throws Exception {

		fc.finance().finance_common().adminFinAgreementVersionsPage(driver);
		AdminFinanceAgreementRuleFormulaPage pobj = new AdminFinanceAgreementRuleFormulaPage(driver);

		fc.utobj().clickElement(driver, pobj.RuleFormulaBtn);
		fc.utobj().clickElement(driver, pobj.AddRuleFormulaBtn);
		fc.utobj().clickElement(driver, pobj.RFSalesAmountBased);
		fc.utobj().clickElement(driver, pobj.ContinueBtn);

		fc.utobj().sendKeys(driver, pobj.RuleFormulaName, formulaNameSAFG);
		fc.utobj().clickElement(driver, pobj.FlatRadioBtn);
		fc.utobj().clickElement(driver, pobj.GrothBasedYesRadioBtn);

		int rowCount = driver.findElements(By.xpath("//table[@id='table1']/tbody")).size();
		if (rowCount == 1) {
			fc.utobj().clickElement(driver, pobj.AddMoreBtn);
		}

		fc.utobj().sendKeys(driver, pobj.SetRangeToFieldZeroRow, rngToFieldZeroRow);
		fc.utobj().sendKeys(driver, pobj.AppliedValueForZeroRow, apldValueForZeroRow);
		fc.utobj().sendKeys(driver, pobj.AppliedValueForFirstRow, apldValueForFirstRow);
		fc.utobj().clickElement(driver, pobj.SaveBtn);

		return formulaNameSAFG;
	}

	@Test(groups = "finance")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "TC_FC_RuleFormula_SalesAmount_Flat_004", testCaseDescription = "Verify Rule Formula Sales Amount Flat Without Growth Based")
	public void addRuleFormulaSalesAmountFlatWithoutGrowthBased() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("finance", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		String formulaNameSAF = fc.utobj().generateTestData(dataSet.get("AddRuleFormulaSAF"));
		String rngToFieldZeroRow = dataSet.get("rangeToFieldZeroRow");
		String apldValueForZeroRow = dataSet.get("appliedValueForZeroRow");
		String apldValueForFirstRow = dataSet.get("appliedValueForFirstRow");

		try {
			driver = fc.loginpage().login(driver);
			AdminFinanceAgreementRuleFormulaPage pobj = new AdminFinanceAgreementRuleFormulaPage(driver);
			fc.utobj().printTestStep("Admin >  Finance > Rule Formula");
			fc.utobj().printTestStep(
					"Add Rule Formula with type Sales Amount Based and apply as a Flat without growth Based");

			formulaNameSAF = addRuleFormulaSalesAmountFlatWithoutGrowth(driver, config, formulaNameSAF,
					rngToFieldZeroRow, apldValueForZeroRow, apldValueForFirstRow);

			fc.utobj().printTestStep(
					"Verify Add Rule Formula with type Sales Amount Based and apply as a Flat without growth Based");
			boolean isTextPresent = fc.utobj().assertLinkText(driver, formulaNameSAF);
			if (isTextPresent == false) {
				fc.utobj().selectDropDownByValue(driver, pobj.selectViewPerPage, "500");
				{
					boolean isTextPresent1 = fc.utobj().assertLinkText(driver, formulaNameSAF);
					if (isTextPresent1 == false) {
						fc.utobj().throwsException("Rule Formula Sales Amount Flat Without Growth Based is Not added!");
					}
				}
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	public String addRuleFormulaSalesAmountFlatWithoutGrowth(WebDriver driver, Map<String, String> config,
			String formulaNameSAF, String rngToFieldZeroRow, String apldValueForZeroRow, String apldValueForFirstRow)
			throws Exception {

		fc.finance().finance_common().adminFinAgreementVersionsPage(driver);
		AdminFinanceAgreementRuleFormulaPage pobj = new AdminFinanceAgreementRuleFormulaPage(driver);

		fc.utobj().clickElement(driver, pobj.RuleFormulaBtn);
		fc.utobj().clickElement(driver, pobj.AddRuleFormulaBtn);
		fc.utobj().clickElement(driver, pobj.RFSalesAmountBased);
		fc.utobj().clickElement(driver, pobj.ContinueBtn);

		fc.utobj().sendKeys(driver, pobj.RuleFormulaName, formulaNameSAF);
		fc.utobj().clickElement(driver, pobj.FlatRadioBtn);
		fc.utobj().clickElement(driver, pobj.GrothBasedNoRadioBtn);
		int rowCount = driver.findElements(By.xpath("//table[@id='table1']/tbody")).size();
		if (rowCount == 1) {
			fc.utobj().clickElement(driver, pobj.AddMoreBtn);
		}
		fc.utobj().sendKeys(driver, pobj.SetRangeToFieldZeroRow, rngToFieldZeroRow);
		fc.utobj().sendKeys(driver, pobj.AppliedValueForZeroRow, apldValueForZeroRow);
		fc.utobj().sendKeys(driver, pobj.AppliedValueForFirstRow, apldValueForFirstRow);
		fc.utobj().clickElement(driver, pobj.SaveBtn);

		return formulaNameSAF;
	}

	@Test(groups = "finance")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "TC_FC_RuleFormula_StoreAge_Percent_005", testCaseDescription = "Verify Rule Formula Store Age Percent Based")
	public void addRuleFormulaStoreAgePercentBased() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("finance", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		String formulaNameStoreAgePercent = fc.utobj().generateTestData(dataSet.get("AddRuleFormulaStoreAgePercent"));
		String rngToFieldZeroRow = dataSet.get("rangeToFieldZeroRow");
		String apldValueForZeroRow = dataSet.get("appliedValueForZeroRow");
		String apldValueForFirstRow = dataSet.get("appliedValueForFirstRow");

		try {
			driver = fc.loginpage().login(driver);
			AdminFinanceAgreementRuleFormulaPage pobj = new AdminFinanceAgreementRuleFormulaPage(driver);
			fc.utobj().printTestStep("Admin >  Finance > Rule Formula");
			fc.utobj().printTestStep("Add Rule Formula with type Store Age Based and apply as a Percentage");

			formulaNameStoreAgePercent = addRuleFormulaStoreAgePercent(driver, config, formulaNameStoreAgePercent,
					rngToFieldZeroRow, apldValueForZeroRow, apldValueForFirstRow);

			fc.utobj().printTestStep("Verify Add Rule Formula with type Store Age Based and apply as a Percentage");
			boolean isTextPresent = fc.utobj().assertLinkText(driver, formulaNameStoreAgePercent);
			if (isTextPresent == false) {
				fc.utobj().selectDropDownByValue(driver, pobj.selectViewPerPage, "500");
				{
					boolean isTextPresent1 = fc.utobj().assertLinkText(driver, formulaNameStoreAgePercent);
					if (isTextPresent1 == false) {
						fc.utobj().throwsException("Rule Formula Store Age Percent Based is Not added!");
					}
				}
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	public String addRuleFormulaStoreAgePercent(WebDriver driver, Map<String, String> config,
			String formulaNameStoreAgePercent, String rngToFieldZeroRow, String apldValueForZeroRow,
			String apldValueForFirstRow) throws Exception {

		fc.finance().finance_common().adminFinAgreementVersionsPage(driver);
		AdminFinanceAgreementRuleFormulaPage pobj = new AdminFinanceAgreementRuleFormulaPage(driver);

		fc.utobj().clickElement(driver, pobj.RuleFormulaBtn);
		fc.utobj().clickElement(driver, pobj.AddRuleFormulaBtn);
		fc.utobj().clickElement(driver, pobj.RFStoreAgeBased);
		fc.utobj().clickElement(driver, pobj.ContinueBtn);

		fc.utobj().sendKeys(driver, pobj.RuleFormulaName, formulaNameStoreAgePercent);
		fc.utobj().clickElement(driver, pobj.PercentageRadioBtn);

		int rowCount = driver.findElements(By.xpath("//table[@id='table1']/tbody")).size();
		if (rowCount == 1) {
			fc.utobj().clickElement(driver, pobj.AddMoreBtn);
		}

		fc.utobj().sendKeys(driver, pobj.SetRangeToFieldZeroRow, rngToFieldZeroRow);
		fc.utobj().sendKeys(driver, pobj.AppliedValueForZeroRow, apldValueForZeroRow);
		fc.utobj().sendKeys(driver, pobj.AppliedValueForFirstRow, apldValueForFirstRow);
		fc.utobj().clickElement(driver, pobj.SaveBtn);
		return formulaNameStoreAgePercent;
	}

	@Test(groups = "finance")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "TC_FC_RuleFormula_StoreAge_Flat_006", testCaseDescription = "Rule Formula Store Age Flat Based")
	public void addRuleFormulaStoreAgeFlatBased() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("finance", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		String formulaNameStoreAgeFlat = fc.utobj().generateTestData(dataSet.get("AddRuleFormulaStoreAgeFlat"));
		String rngToFieldZeroRow = dataSet.get("rangeToFieldZeroRow");
		String apldValueForZeroRow = dataSet.get("appliedValueForZeroRow");
		String apldValueForFirstRow = dataSet.get("appliedValueForFirstRow");

		try {
			driver = fc.loginpage().login(driver);
			AdminFinanceAgreementRuleFormulaPage pobj = new AdminFinanceAgreementRuleFormulaPage(driver);
			fc.utobj().printTestStep("Admin >  Finance > Rule Formula");
			fc.utobj().printTestStep("Add Rule Formula with type Store Age Based and apply as a Flat");

			formulaNameStoreAgeFlat = addRuleFormulaStoreAgeFlat(driver, config, formulaNameStoreAgeFlat,
					rngToFieldZeroRow, apldValueForZeroRow, apldValueForFirstRow);

			fc.utobj().printTestStep("Verify Add Rule Formula with type Store Age Based and apply as a Flat");
			boolean isTextPresent = fc.utobj().assertLinkText(driver, formulaNameStoreAgeFlat);
			if (isTextPresent == false) {
				fc.utobj().selectDropDownByValue(driver, pobj.selectViewPerPage, "500");
				{
					boolean isTextPresent1 = fc.utobj().assertLinkText(driver, formulaNameStoreAgeFlat);
					if (isTextPresent1 == false) {
						fc.utobj().throwsException("Rule Formula Store Age Flat Based is Not added!");
					}
				}
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	public String addRuleFormulaStoreAgeFlat(WebDriver driver, Map<String, String> config,
			String formulaNameStoreAgeFlat, String rngToFieldZeroRow, String apldValueForZeroRow,
			String apldValueForFirstRow) throws Exception {

		fc.finance().finance_common().adminFinAgreementVersionsPage(driver);
		AdminFinanceAgreementRuleFormulaPage pobj = new AdminFinanceAgreementRuleFormulaPage(driver);

		fc.utobj().clickElement(driver, pobj.RuleFormulaBtn);
		fc.utobj().clickElement(driver, pobj.AddRuleFormulaBtn);
		fc.utobj().clickElement(driver, pobj.RFStoreAgeBased);
		fc.utobj().clickElement(driver, pobj.ContinueBtn);

		fc.utobj().sendKeys(driver, pobj.RuleFormulaName, formulaNameStoreAgeFlat);
		fc.utobj().clickElement(driver, pobj.FlatRadioBtn);

		int rowCount = driver.findElements(By.xpath("//table[@id='table1']/tbody")).size();
		if (rowCount == 1) {
			fc.utobj().clickElement(driver, pobj.AddMoreBtn);
		}

		fc.utobj().sendKeys(driver, pobj.SetRangeToFieldZeroRow, rngToFieldZeroRow);
		fc.utobj().sendKeys(driver, pobj.AppliedValueForZeroRow, apldValueForZeroRow);
		fc.utobj().sendKeys(driver, pobj.AppliedValueForFirstRow, apldValueForFirstRow);
		fc.utobj().clickElement(driver, pobj.SaveBtn);

		return formulaNameStoreAgeFlat;
	}

	@Test(groups = { "finance" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "TC_FC_RuleFormula_Quantity_Percent_007", testCaseDescription = "Verify Add Rule Formula")
	public void addRuleFormulaQuantityPercentBased() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("finance", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		String formulaNameQPB = fc.utobj().generateTestData(dataSet.get("AddRuleFormulaQuantityPercent"));
		String rngToFieldZeroRow = dataSet.get("rangeToFieldZeroRow");
		String apldValueForZeroRow = dataSet.get("appliedValueForZeroRow");
		String apldValueForFirstRow = dataSet.get("appliedValueForFirstRow");

		try {
			driver = fc.loginpage().login(driver);
			AdminFinanceAgreementRuleFormulaPage pobj = new AdminFinanceAgreementRuleFormulaPage(driver);
			fc.utobj().printTestStep("Admin >  Finance > Rule Formula");
			fc.utobj().printTestStep("Add Rule Formula with type Quantity Based and apply as a Percentage");

			formulaNameQPB = addRuleFormulaQuantityPercent(driver, config, formulaNameQPB, rngToFieldZeroRow,
					apldValueForZeroRow, apldValueForFirstRow);

			fc.utobj().printTestStep("Verify Add Rule Formula with type Quantity Based and apply as a Percentage");
			boolean isTextPresent = fc.utobj().assertLinkText(driver, formulaNameQPB);
			if (isTextPresent == false) {

				fc.utobj().selectDropDownByValue(driver, pobj.selectViewPerPage, "500");
				{
					boolean isTextPresent1 = fc.utobj().assertLinkText(driver, formulaNameQPB);
					if (isTextPresent1 == false) {
						fc.utobj().throwsException("Rule Formula Quantity Percent Based is Not added!");
					}
				}
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	public String addRuleFormulaQuantityPercent(WebDriver driver, Map<String, String> config, String formulaNameQPB,
			String rngToFieldZeroRow, String apldValueForZeroRow, String apldValueForFirstRow) throws Exception {

		fc.finance().finance_common().adminFinAgreementVersionsPage(driver);
		AdminFinanceAgreementRuleFormulaPage pobj = new AdminFinanceAgreementRuleFormulaPage(driver);

		fc.utobj().clickElement(driver, pobj.RuleFormulaBtn);
		fc.utobj().clickElement(driver, pobj.AddRuleFormulaBtn);
		fc.utobj().clickElement(driver, pobj.RFQuantityBased);
		fc.utobj().clickElement(driver, pobj.ContinueBtn);

		fc.utobj().sendKeys(driver, pobj.RuleFormulaName, formulaNameQPB);
		fc.utobj().clickElement(driver, pobj.PercentageRadioBtn);

		int rowCount = driver.findElements(By.xpath("//table[@id='table1']/tbody")).size();
		if (rowCount == 1) {
			fc.utobj().clickElement(driver, pobj.AddMoreBtn);
		}

		fc.utobj().sendKeys(driver, pobj.SetRangeToFieldZeroRow, rngToFieldZeroRow);
		fc.utobj().sendKeys(driver, pobj.AppliedValueForZeroRow, apldValueForZeroRow);
		fc.utobj().sendKeys(driver, pobj.AppliedValueForFirstRow, apldValueForFirstRow);
		fc.utobj().clickElement(driver, pobj.SaveBtn);

		return formulaNameQPB;
	}

	@Test(groups = { "finance" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "TC_FC_RuleFormula_Quantity_Flat_008", testCaseDescription = "Verify Add Rule Formula with type Quantity Based and apply as a Flat")
	public void addRuleFormulaQuantityFlatBased() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("finance", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		String formulaNameQFB = dataSet.get("AddRuleFormulaQuantityFlat");
		formulaNameQFB = fc.utobj().generateTestData(formulaNameQFB);
		String rngToFieldZeroRow = dataSet.get("rangeToFieldZeroRow");
		String apldValueForZeroRow = dataSet.get("appliedValueForZeroRow");
		String apldValueForFirstRow = dataSet.get("appliedValueForFirstRow");

		try {
			driver = fc.loginpage().login(driver);
			AdminFinanceAgreementRuleFormulaPage pobj = new AdminFinanceAgreementRuleFormulaPage(driver);
			fc.utobj().printTestStep("Admin >  Finance > Rule Formula");
			fc.utobj().printTestStep("Add Rule Formula with type Quantity Based and apply as a Flat");

			formulaNameQFB = addRuleFormulaQuantityFlat(driver, config, formulaNameQFB, rngToFieldZeroRow,
					apldValueForZeroRow, apldValueForFirstRow);

			fc.utobj().printTestStep("Verify Add Rule Formula with type Quantity Based and apply as a Flat");
			boolean isTextPresent = fc.utobj().assertLinkText(driver, formulaNameQFB);
			if (isTextPresent == false) {
				fc.utobj().selectDropDownByValue(driver, pobj.selectViewPerPage, "500");
				{
					boolean isTextPresent1 = fc.utobj().assertLinkText(driver, formulaNameQFB);
					if (isTextPresent1 == false) {
						fc.utobj().throwsException("Rule Formula Quantity Flat Based is Not added!");
					}
				}
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	public String addRuleFormulaQuantityFlat(WebDriver driver, Map<String, String> config, String formulaNameQFB,
			String rngToFieldZeroRow, String apldValueForZeroRow, String apldValueForFirstRow) throws Exception {

		fc.finance().finance_common().adminFinAgreementVersionsPage(driver);
		AdminFinanceAgreementRuleFormulaPage pobj = new AdminFinanceAgreementRuleFormulaPage(driver);

		fc.utobj().clickElement(driver, pobj.RuleFormulaBtn);
		fc.utobj().clickElement(driver, pobj.AddRuleFormulaBtn);
		fc.utobj().clickElement(driver, pobj.RFQuantityBased);
		fc.utobj().clickElement(driver, pobj.ContinueBtn);

		fc.utobj().sendKeys(driver, pobj.RuleFormulaName, formulaNameQFB);
		fc.utobj().clickElement(driver, pobj.FlatRadioBtn);

		int rowCount = driver.findElements(By.xpath("//table[@id='table1']/tbody")).size();
		if (rowCount == 1) {
			fc.utobj().clickElement(driver, pobj.AddMoreBtn);
		}
		fc.utobj().sendKeys(driver, pobj.SetRangeToFieldZeroRow, rngToFieldZeroRow);
		fc.utobj().sendKeys(driver, pobj.AppliedValueForZeroRow, apldValueForZeroRow);
		fc.utobj().sendKeys(driver, pobj.AppliedValueForFirstRow, apldValueForFirstRow);
		fc.utobj().clickElement(driver, pobj.SaveBtn);

		return formulaNameQFB;
	}

	@Test(groups = "finance")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "TC_FC_RuleFormula_Modify_055", testCaseDescription = "Verify Modification of Rule Formula")
	public void modifyRuleFormula() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("finance", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		String formulaNameSAPG = dataSet.get("AddRuleFormulaSAPG");
		formulaNameSAPG = fc.utobj().generateTestData(formulaNameSAPG);
		String rngToFieldZeroRow = dataSet.get("rangeToFieldZeroRow");
		String apldValueForZeroRow = dataSet.get("appliedValueForZeroRow");
		String apldValueForFirstRow = dataSet.get("appliedValueForFirstRow");

		String modifiedformulaName = dataSet.get("AddRuleFormula");
		modifiedformulaName = fc.utobj().generateTestData(modifiedformulaName);
		String modifyrangeToFieldZeroRow = dataSet.get("modifyrangeToFieldZeroRow");
		String modifyappliedValueForZeroRow = dataSet.get("modifyappliedValueForZeroRow");
		String modifyappliedValueForFirstRow = dataSet.get("modifyappliedValueForFirstRow");

		try {
			driver = fc.loginpage().login(driver);

			AdminFinanceAgreementRuleFormulaPage pobj = new AdminFinanceAgreementRuleFormulaPage(driver);
			fc.utobj().printTestStep("Add New Rule Formula");
			AdminFinanceAgreementRuleFormulaPageTest addNewFormula = new AdminFinanceAgreementRuleFormulaPageTest();
			String ruleFormulaName = addNewFormula.addRuleFormulaSalesAmountPercentWithGrowth(driver, config,
					formulaNameSAPG, rngToFieldZeroRow, apldValueForZeroRow, apldValueForFirstRow);

			fc.utobj().printTestStep("Modify Rule Formula");
			boolean isTextPresent2 = fc.utobj().assertLinkText(driver, ruleFormulaName);
			if (isTextPresent2 == false) {
				fc.utobj().selectDropDownByValue(driver, pobj.viewPerPageSelect, "500");
			}
			fc.utobj().actionImgOption(driver, ruleFormulaName, "Modify");

			fc.utobj().sendKeys(driver, pobj.RuleFormulaName, modifiedformulaName);
			fc.utobj().sendKeys(driver, pobj.SetRangeToFieldZeroRow, modifyrangeToFieldZeroRow);
			fc.utobj().sendKeys(driver, pobj.AppliedValueForZeroRow, modifyappliedValueForZeroRow);
			fc.utobj().sendKeys(driver, pobj.AppliedValueForFirstRow, modifyappliedValueForFirstRow);
			fc.utobj().clickElement(driver, pobj.SaveBtn);

			fc.utobj().printTestStep("Verify The Modify Rule Formula");
			boolean isTextPresent1 = driver.findElement(By.xpath(".//td[@id='pageid' and @class='bText12']"))
					.isDisplayed();
			if (isTextPresent1 == true) {
				fc.utobj().selectDropDownByValue(driver, pobj.selectViewPerPage, "500");
			}
			boolean isTextPresent = fc.utobj().assertLinkText(driver, modifiedformulaName);
			if (isTextPresent == false) {
				fc.utobj().throwsException("Rule Formula is Not Modified!");

			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "finance")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "TC_FC_RuleFormula_Delete_056", testCaseDescription = "Verify Deletion of Rule Formula")
	public void deleteRuleFormulaSalesAmountGrowthBased() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("finance", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		String formulaNameSAPG = dataSet.get("AddRuleFormulaSAPG");
		formulaNameSAPG = fc.utobj().generateTestData(formulaNameSAPG);
		String rngToFieldZeroRow = dataSet.get("rangeToFieldZeroRow");
		String apldValueForZeroRow = dataSet.get("appliedValueForZeroRow");
		String apldValueForFirstRow = dataSet.get("appliedValueForFirstRow");

		try {
			driver = fc.loginpage().login(driver);
			AdminFinanceAgreementRuleFormulaPage pobj = new AdminFinanceAgreementRuleFormulaPage(driver);
			fc.utobj().printTestStep("Add New Rule Formula");
			AdminFinanceAgreementRuleFormulaPageTest addNewFormula = new AdminFinanceAgreementRuleFormulaPageTest();
			String ruleFormulaName = addNewFormula.addRuleFormulaSalesAmountPercentWithGrowth(driver, config,
					formulaNameSAPG, rngToFieldZeroRow, apldValueForZeroRow, apldValueForFirstRow);

			fc.utobj().printTestStep("Delete Rule Formula");
			boolean isTextPresent2 = fc.utobj().assertLinkText(driver, ruleFormulaName);
			if (isTextPresent2 == false) {
				fc.utobj().selectDropDownByValue(driver, pobj.viewPerPageSelect, "500");
			}
			fc.utobj().actionImgOption(driver, ruleFormulaName, "Delete");
			fc.utobj().acceptAlertBox(driver);

			fc.utobj().printTestStep("Verify The Deleted Rule Formula");
			boolean isTextPresent4 = driver.findElement(By.xpath(".//td[@id='pageid' and @class='bText12']"))
					.isDisplayed();
			if (isTextPresent4 == true) {
				fc.utobj().selectDropDownByValue(driver, pobj.selectViewPerPage, "500");
			}
			boolean isTextPresent1 = fc.utobj().assertLinkText(driver, ruleFormulaName);
			if (isTextPresent1 == true) {
				fc.utobj().throwsException("Rule Formula Not deleted!");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}
}
