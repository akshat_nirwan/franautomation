package com.builds.test.fin;

import java.util.Map;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.Test;
import com.builds.uimaps.fin.AdminFinanceAgreementRulesPage;
import com.builds.uimaps.fin.AdminFinanceAgreementVersionsPage;
import com.builds.utilities.FranconnectUtil;
import com.builds.utilities.TestCase;

public class AdminFinanceAgreementRulesPageTest {

	FranconnectUtil fc = new FranconnectUtil();

	@Test(groups = { "finance" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "TC_FC_AgreementRule_SalesAmount_Percent_YTD_009", testCaseDescription = "Verify The Add Agreement Rule with Type Sales Amount Based Percent and Accounting Basis is YTD")
	public void addAgreementRuleSalesAmountPercentYTDBased() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("finance", testCaseId);

		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		String ruleNameSAPYTD = fc.utobj().generateTestData(dataSet.get("agreementRuleNameSAPYTDB"));
		String applyMinCapValue = dataSet.get("applyMinCapValue");
		String minCapValue = dataSet.get("minCapValue");
		String applyMaxCapValue = dataSet.get("applyMaxCapValue");
		String maxCapValue = dataSet.get("maxCapValue");
		String rngToFieldZeroRowAR = dataSet.get("rangeToFieldZeroRowAR");
		String apldValueForZeroRowAR = dataSet.get("appliedValueForZeroRowAR");
		String apldValueForFirstRowAR = dataSet.get("appliedValueForFirstRowAR");

		try {
			driver = fc.loginpage().login(driver);
			AdminFinanceAgreementRulesPage pobj = new AdminFinanceAgreementRulesPage(driver);

			fc.utobj().printTestStep("Navigate To Admin > Finance > Agreement Rules");

			fc.utobj().printTestStep(
					"Add Agreement Rule with Type Sales Amount Based Percent and Accounting Basis is YTD");
			ruleNameSAPYTD = addAgreementRuleSalesAmountPercentYTD(driver, config, ruleNameSAPYTD, applyMinCapValue,
					applyMaxCapValue, minCapValue, maxCapValue, rngToFieldZeroRowAR, apldValueForZeroRowAR,
					apldValueForFirstRowAR);

			fc.utobj().printTestStep(
					"Verify The Add Agreement Rule with Type Sales Amount Based Percent and Accounting Basis is YTD");
			boolean isTextPresent = fc.utobj().assertLinkText(driver, ruleNameSAPYTD);
			if (isTextPresent == false) {
				fc.utobj().selectDropDownByValue(driver, pobj.selectViewPerPage, "500");
				{
					boolean isTextPresent1 = fc.utobj().assertLinkText(driver, ruleNameSAPYTD);
					if (isTextPresent1 == false) {
						fc.utobj().throwsException(
								"Agreement Rule with Type Sales Amount Based Percent and Accounting Basis is YTD Not added!");
					}
				}
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	public String addAgreementRuleSalesAmountPercentYTD(WebDriver driver, Map<String, String> config,
			String ruleNameSAPYTD, String applyMinCapValue, String applyMaxCapValue, String minCapValue,
			String maxCapValue, String rngToFieldZeroRowAR, String apldValueForZeroRowAR, String apldValueForFirstRowAR)
			throws Exception {

		fc.finance().finance_common().adminFinAgreementVersionsPage(driver);
		AdminFinanceAgreementRulesPage pobj = new AdminFinanceAgreementRulesPage(driver);

		fc.utobj().clickElement(driver, pobj.AgreementRulesBtn);
		fc.utobj().clickElement(driver, pobj.AddAgreementRule);

		if (!fc.utobj().isSelected(driver,pobj.ARSalesAmountBased)) {
			fc.utobj().clickElement(driver, pobj.ARSalesAmountBased);
		}

		fc.utobj().clickElement(driver, pobj.continueBtn);
		fc.utobj().sendKeys(driver, pobj.AgreementRuleName, ruleNameSAPYTD);

		if (applyMinCapValue.equalsIgnoreCase("Y")) {
			fc.utobj().clickElement(driver, pobj.CheckboxMinimumCap);
			fc.utobj().sendKeys(driver, pobj.MinimumCap, minCapValue);
		}

		if (applyMaxCapValue.equalsIgnoreCase("Y")) {
			fc.utobj().clickElement(driver, pobj.CheckBoxMaximumCap);
			fc.utobj().sendKeys(driver, pobj.MaximumCap, maxCapValue);
		}

		fc.utobj().sendKeys(driver, pobj.Description, "Agreement Rule Sales Amount, Percent and YTD Based");

		if (!fc.utobj().isSelected(driver,pobj.RulePercentageRadioBtn)) {
			fc.utobj().clickElement(driver, pobj.RulePercentageRadioBtn);
		}

		if (!fc.utobj().isSelected(driver,pobj.YTDAccountingBasis)) {
			fc.utobj().clickElement(driver, pobj.YTDAccountingBasis);
		}

		int rowCount = driver.findElements(By.xpath("//table[@id='table1']/tbody/tr")).size();
		if (rowCount == 1) {
			fc.utobj().clickElement(driver, pobj.RuleAddMoreBtn);
		}

		fc.utobj().sendKeys(driver, pobj.RuleSetRangeToFieldZeroRow, rngToFieldZeroRowAR);
		fc.utobj().sendKeys(driver, pobj.RuleAppliedValueForZeroRow, apldValueForZeroRowAR);
		fc.utobj().sendKeys(driver, pobj.RuleAppliedValueForFirstRow, apldValueForFirstRowAR);
		fc.utobj().clickElement(driver, pobj.submitBtn);

		return ruleNameSAPYTD;
	}

	@Test(groups = { "finance" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "TC_FC_AgreementRule_SalesAmount_Percent_Cumulative_010", testCaseDescription = "Verify The Add Agreement Rule with Type Sales Amount Based Percent and Accounting Basis is Cumulative")
	public void addAgreementRuleSalesAmountPercentCumulativeBased() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("finance", testCaseId);

		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		String ruleNameSAPC = fc.utobj().generateTestData(dataSet.get("agreementRuleNSAPCB"));
		String applyMinCapValue = dataSet.get("applyMinCapValue");
		String minCapValue = dataSet.get("minCapValue");
		String applyMaxCapValue = dataSet.get("applyMaxCapValue");
		String maxCapValue = dataSet.get("maxCapValue");
		String rngToFieldZeroRowAR = dataSet.get("rangeToFieldZeroRowAR");
		String apldValueForZeroRowAR = dataSet.get("appliedValueForZeroRowAR");
		String apldValueForFirstRowAR = dataSet.get("appliedValueForFirstRowAR");

		try {
			driver = fc.loginpage().login(driver);
			AdminFinanceAgreementRulesPage pobj = new AdminFinanceAgreementRulesPage(driver);

			fc.utobj().printTestStep(
					"Add Agreement Rule with Type Sales Amount Based Percent and Accounting Basis is Cumulative");

			ruleNameSAPC = addAgreementRuleSalesAmountPercentCumulative(driver, config, ruleNameSAPC, applyMinCapValue,
					applyMaxCapValue, minCapValue, maxCapValue, rngToFieldZeroRowAR, apldValueForZeroRowAR,
					apldValueForFirstRowAR);

			fc.utobj().printTestStep(
					"Verify The Add Agreement Rule with Type Sales Amount Based Percent and Accounting Basis is Cumulative");

			boolean isTextPresent = fc.utobj().assertLinkText(driver, ruleNameSAPC);
			if (isTextPresent == false) {
				fc.utobj().selectDropDownByValue(driver, pobj.selectViewPerPage, "500");
				{
					boolean isTextPresent1 = fc.utobj().assertLinkText(driver, ruleNameSAPC);
					if (isTextPresent1 == false) {
						fc.utobj().throwsException(
								"Agreement Rule with Type Sales Amount Based Percent and Accounting Basis is Cumulative Not added!");
					}
				}
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	public String addAgreementRuleSalesAmountPercentCumulative(WebDriver driver, Map<String, String> config,
			String ruleNameSAPC, String applyMinCapValue, String applyMaxCapValue, String minCapValue,
			String maxCapValue, String rngToFieldZeroRowAR, String apldValueForZeroRowAR, String apldValueForFirstRowAR)
			throws Exception {

		fc.finance().finance_common().adminFinAgreementVersionsPage(driver);
		AdminFinanceAgreementRulesPage pobj = new AdminFinanceAgreementRulesPage(driver);

		fc.utobj().clickElement(driver, pobj.AgreementRulesBtn);
		fc.utobj().clickElement(driver, pobj.AddAgreementRule);

		if (!fc.utobj().isSelected(driver,pobj.ARSalesAmountBased)) {
			fc.utobj().clickElement(driver, pobj.ARSalesAmountBased);
		}

		fc.utobj().clickElement(driver, pobj.continueBtn);

		fc.utobj().sendKeys(driver, pobj.AgreementRuleName, ruleNameSAPC);

		if (applyMinCapValue.equalsIgnoreCase("Y")) {
			fc.utobj().clickElement(driver, pobj.CheckboxMinimumCap);
			fc.utobj().sendKeys(driver, pobj.MinimumCap, minCapValue);
		}

		if (applyMaxCapValue.equalsIgnoreCase("Y")) {
			fc.utobj().clickElement(driver, pobj.CheckBoxMaximumCap);
			fc.utobj().sendKeys(driver, pobj.MaximumCap, maxCapValue);
		}
		fc.utobj().sendKeys(driver, pobj.Description, "Agreement Rule Sales Amount Percent Cumulative Based");

		if (!fc.utobj().isSelected(driver,pobj.RulePercentageRadioBtn)) {
			fc.utobj().clickElement(driver, pobj.RulePercentageRadioBtn);
		}

		if (!fc.utobj().isSelected(driver,pobj.CumulativeAccountingBasis)) {
			fc.utobj().clickElement(driver, pobj.CumulativeAccountingBasis);
		}

		int rowCount = driver.findElements(By.xpath("//table[@id='table1']/tbody")).size();
		if (rowCount == 1) {
			fc.utobj().clickElement(driver, pobj.RuleAddMoreBtn);
		}
		fc.utobj().sendKeys(driver, pobj.RuleSetRangeToFieldZeroRow, rngToFieldZeroRowAR);
		fc.utobj().sendKeys(driver, pobj.RuleAppliedValueForZeroRow, apldValueForZeroRowAR);
		fc.utobj().sendKeys(driver, pobj.RuleAppliedValueForFirstRow, apldValueForFirstRowAR);
		fc.utobj().clickElement(driver, pobj.submitBtn);

		return ruleNameSAPC;
	}

	@Test(groups = { "finance" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "TC_FC_AgreementRule_SalesAmount_Percent_Periodic_Sales_011", testCaseDescription = "Verify Agreement Rule Sales Amount, Percent and Periodic Sales Based")
	public void addAgreementRuleSalesAmountPercentPeriodicSalesBased() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("finance", testCaseId);

		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		String ruleNameSAPPS = fc.utobj().generateTestData(dataSet.get("agreementRuleNameSAPPSB"));
		String applyMinCapValue = dataSet.get("applyMinCapValue");
		String minCapValue = dataSet.get("minCapValue");
		String applyMaxCapValue = dataSet.get("applyMaxCapValue");
		String maxCapValue = dataSet.get("maxCapValue");
		String rngToFieldZeroRowAR = dataSet.get("rangeToFieldZeroRowAR");
		String apldValueForZeroRowAR = dataSet.get("appliedValueForZeroRowAR");
		String apldValueForFirstRowAR = dataSet.get("appliedValueForFirstRowAR");

		try {
			driver = fc.loginpage().login(driver);
			AdminFinanceAgreementRulesPage pobj = new AdminFinanceAgreementRulesPage(driver);
			fc.utobj().printTestStep(
					"Add Agreement Rule with Type Sales Amount Based Percent and Accounting Basis is Periodic Sales");

			ruleNameSAPPS = addAgreementRuleSalesAmountPercentPeriodicSales(driver, config, ruleNameSAPPS,
					applyMinCapValue, applyMaxCapValue, minCapValue, maxCapValue, rngToFieldZeroRowAR,
					apldValueForZeroRowAR, apldValueForFirstRowAR);

			fc.utobj().printTestStep(
					"Verify The Add Agreement Rule with Type Sales Amount Based Percent and Accounting Basis is Periodic Sales");

			boolean isTextPresent = fc.utobj().assertLinkText(driver, ruleNameSAPPS);
			if (isTextPresent == false) {
				fc.utobj().selectDropDownByValue(driver, pobj.selectViewPerPage, "500");
				{
					boolean isTextPresent1 = fc.utobj().assertLinkText(driver, ruleNameSAPPS);
					if (isTextPresent1 == false) {
						fc.utobj().throwsException(
								"Agreement Rule Sales Amount, Percent and Periodic Sales Based is Not added!");
					}
				}
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	public String addAgreementRuleSalesAmountPercentPeriodicSales(WebDriver driver, Map<String, String> config,
			String ruleNameSAPPS, String applyMinCapValue, String applyMaxCapValue, String minCapValue,
			String maxCapValue, String rngToFieldZeroRowAR, String apldValueForZeroRowAR, String apldValueForFirstRowAR)
			throws Exception {

		fc.finance().finance_common().adminFinAgreementVersionsPage(driver);
		AdminFinanceAgreementRulesPage pobj = new AdminFinanceAgreementRulesPage(driver);

		fc.utobj().clickElement(driver, pobj.AgreementRulesBtn);
		fc.utobj().clickElement(driver, pobj.AddAgreementRule);

		if (!fc.utobj().isSelected(driver,pobj.ARSalesAmountBased)) {
			fc.utobj().clickElement(driver, pobj.ARSalesAmountBased);
		}

		fc.utobj().clickElement(driver, pobj.continueBtn);

		fc.utobj().sendKeys(driver, pobj.AgreementRuleName, ruleNameSAPPS);

		if (applyMinCapValue.equalsIgnoreCase("Y")) {
			fc.utobj().clickElement(driver, pobj.CheckboxMinimumCap);
			fc.utobj().sendKeys(driver, pobj.MinimumCap, minCapValue);
		}

		if (applyMaxCapValue.equalsIgnoreCase("Y")) {
			fc.utobj().clickElement(driver, pobj.CheckBoxMaximumCap);
			fc.utobj().sendKeys(driver, pobj.MaximumCap, maxCapValue);
		}
		fc.utobj().sendKeys(driver, pobj.Description, "Agreement Rule Sales Amount, Percent and Periodic Sales Based");

		if (!fc.utobj().isSelected(driver,pobj.RulePercentageRadioBtn)) {
			fc.utobj().clickElement(driver, pobj.RulePercentageRadioBtn);
		}

		if (!fc.utobj().isSelected(driver,pobj.PeriodicSalesAccountingBasis)) {
			fc.utobj().clickElement(driver, pobj.PeriodicSalesAccountingBasis);
		}

		int rowCount = driver.findElements(By.xpath("//table[@id='table1']/tbody")).size();
		if (rowCount == 1) {
			fc.utobj().clickElement(driver, pobj.RuleAddMoreBtn);
		}
		fc.utobj().sendKeys(driver, pobj.RuleSetRangeToFieldZeroRow, rngToFieldZeroRowAR);
		fc.utobj().sendKeys(driver, pobj.RuleAppliedValueForZeroRow, apldValueForZeroRowAR);
		fc.utobj().sendKeys(driver, pobj.RuleAppliedValueForFirstRow, apldValueForFirstRowAR);
		fc.utobj().clickElement(driver, pobj.submitBtn);

		return ruleNameSAPPS;
	}

	@Test(groups = { "finance" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "TC_FC_AgreementRule_SalesAmount_Percent_YTD_RRSD_012", testCaseDescription = "Verify Agreement Rule Sales Amount, Percent and YTD from Royalty Reporting Start Date Based")
	public void addAgreementRuleSalesAmountPercentYTDfromRRSDBased() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("finance", testCaseId);

		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		String ruleNameSAPYTDfromRRSD = fc.utobj().generateTestData(dataSet.get("agreementRuleNameSAPYTDfromRRSDB"));
		String applyMinCapValue = dataSet.get("applyMinCapValue");
		String applyMaxCapValue = dataSet.get("applyMaxCapValue");
		String minCapValue = dataSet.get("minCapValue");
		String maxCapValue = dataSet.get("maxCapValue");
		String rngToFieldZeroRowAR = dataSet.get("rangeToFieldZeroRowAR");
		String apldValueForZeroRowAR = dataSet.get("appliedValueForZeroRowAR");
		String apldValueForFirstRowAR = dataSet.get("appliedValueForFirstRowAR");

		try {
			driver = fc.loginpage().login(driver);
			AdminFinanceAgreementRulesPage pobj = new AdminFinanceAgreementRulesPage(driver);
			fc.utobj().printTestStep(
					"Add Agreement Rule with Type Sales Amount Based Percent and Accounting Basis is YTD from Royalty Reporting Start Date");

			ruleNameSAPYTDfromRRSD = addAgreementRuleSalesAmountPercentYTDfromRRSD(driver, config,
					ruleNameSAPYTDfromRRSD, applyMinCapValue, applyMaxCapValue, minCapValue, maxCapValue,
					rngToFieldZeroRowAR, apldValueForZeroRowAR, apldValueForFirstRowAR);

			fc.utobj().printTestStep(
					"Add Agreement Rule with Type Sales Amount Based Percent and Accounting Basis is YTD from Royalty Reporting Start Date");

			boolean isTextPresent = fc.utobj().assertLinkText(driver, ruleNameSAPYTDfromRRSD);
			if (isTextPresent == false) {

				fc.utobj().selectDropDownByValue(driver, pobj.selectViewPerPage, "500");
				{
					boolean isTextPresent1 = fc.utobj().assertLinkText(driver, ruleNameSAPYTDfromRRSD);
					if (isTextPresent1 == false) {
						fc.utobj().throwsException(
								"Agreement Rule Sales Amount, Percent and YTD from Royalty Reporting Start Date Based is Not added!");
					}
				}
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	public String addAgreementRuleSalesAmountPercentYTDfromRRSD(WebDriver driver, Map<String, String> config,
			String ruleNameSAPYTDfromRRSD, String applyMinCapValue, String applyMaxCapValue, String minCapValue,
			String maxCapValue, String rngToFieldZeroRowAR, String apldValueForZeroRowAR, String apldValueForFirstRowAR)
			throws Exception {

		fc.finance().finance_common().adminFinAgreementVersionsPage(driver);
		AdminFinanceAgreementRulesPage pobj = new AdminFinanceAgreementRulesPage(driver);

		fc.utobj().clickElement(driver, pobj.AgreementRulesBtn);
		fc.utobj().clickElement(driver, pobj.AddAgreementRule);

		if (!fc.utobj().isSelected(driver,pobj.ARSalesAmountBased)) {
			fc.utobj().clickElement(driver, pobj.ARSalesAmountBased);
		}

		fc.utobj().clickElement(driver, pobj.continueBtn);

		fc.utobj().sendKeys(driver, pobj.AgreementRuleName, ruleNameSAPYTDfromRRSD);

		if (applyMinCapValue.equalsIgnoreCase("Y")) {
			fc.utobj().clickElement(driver, pobj.CheckboxMinimumCap);
			fc.utobj().sendKeys(driver, pobj.MinimumCap, minCapValue);
		}

		if (applyMaxCapValue.equalsIgnoreCase("Y")) {
			fc.utobj().clickElement(driver, pobj.CheckBoxMaximumCap);
			fc.utobj().sendKeys(driver, pobj.MaximumCap, maxCapValue);
		}
		fc.utobj().sendKeys(driver, pobj.Description,
				"Agreement Rule Sales Amount, Percent and YTD from Royalty Reporting Start Date Based");

		if (!fc.utobj().isSelected(driver,pobj.RulePercentageRadioBtn)) {
			fc.utobj().clickElement(driver, pobj.RulePercentageRadioBtn);
		}

		if (!fc.utobj().isSelected(driver,pobj.YTDfromRoyaltyReportingStartDateAccountingBasis)) {
			fc.utobj().clickElement(driver, pobj.YTDfromRoyaltyReportingStartDateAccountingBasis);
		}

		int rowCount = driver.findElements(By.xpath("//table[@id='table1']/tbody")).size();
		if (rowCount == 1) {
			fc.utobj().clickElement(driver, pobj.RuleAddMoreBtn);
		}

		fc.utobj().sendKeys(driver, pobj.RuleSetRangeToFieldZeroRow, rngToFieldZeroRowAR);
		fc.utobj().sendKeys(driver, pobj.RuleAppliedValueForZeroRow, apldValueForZeroRowAR);
		fc.utobj().sendKeys(driver, pobj.RuleAppliedValueForFirstRow, apldValueForFirstRowAR);
		fc.utobj().clickElement(driver, pobj.submitBtn);

		return ruleNameSAPYTDfromRRSD;
	}

	@Test(groups = { "finance" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "TC_FC_AgreementRule_SalesAmount_Flat_YTD_013", testCaseDescription = "Verify Agreement Rule Sales Amount, Flat and YTD Based")
	public void addAgreementRuleSalesAmountFlatYTDBased() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("finance", testCaseId);

		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		String ruleNameSAFYTD = fc.utobj().generateTestData(dataSet.get("agreementRuleNameSAFYTDBased"));
		String minCapValue = dataSet.get("minCapValue");
		String applyMinCapValue = dataSet.get("applyMinCapValue");
		String maxCapValue = dataSet.get("maxCapValue");
		String applyMaxCapValue = dataSet.get("applyMaxCapValue");
		String rngToFieldZeroRowAR = dataSet.get("rangeToFieldZeroRowAR");
		String apldValueForZeroRowAR = dataSet.get("appliedValueForZeroRowAR");
		String apldValueForFirstRowAR = dataSet.get("appliedValueForFirstRowAR");

		try {
			driver = fc.loginpage().login(driver);
			AdminFinanceAgreementRulesPage pobj = new AdminFinanceAgreementRulesPage(driver);
			fc.utobj()
					.printTestStep("Add Agreement Rule with Type Sales Amount Based Flat and Accounting Basis is YTD");

			ruleNameSAFYTD = addAgreementRuleSalesAmountFlatYTD(driver, config, ruleNameSAFYTD, applyMinCapValue,
					applyMaxCapValue, minCapValue, maxCapValue, rngToFieldZeroRowAR, apldValueForZeroRowAR,
					apldValueForFirstRowAR);

			fc.utobj().printTestStep(
					"Verify Add Agreement Rule with Type Sales Amount Based Flat and Accounting Basis is YTD");
			boolean isTextPresent = fc.utobj().assertLinkText(driver, ruleNameSAFYTD);
			if (isTextPresent == false) {
				fc.utobj().selectDropDownByValue(driver, pobj.selectViewPerPage, "500");
				{
					boolean isTextPresent1 = fc.utobj().assertLinkText(driver, ruleNameSAFYTD);
					if (isTextPresent1 == false) {
						fc.utobj().throwsException("Agreement Rule Sales Amount Flat YTD Based is Not added!");
					}
				}
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	public String addAgreementRuleSalesAmountFlatYTD(WebDriver driver, Map<String, String> config,
			String ruleNameSAFYTD, String applyMinCapValue, String applyMaxCapValue, String minCapValue,
			String maxCapValue, String rngToFieldZeroRowAR, String apldValueForZeroRowAR, String apldValueForFirstRowAR)
			throws Exception {

		fc.finance().finance_common().adminFinAgreementVersionsPage(driver);
		AdminFinanceAgreementRulesPage pobj = new AdminFinanceAgreementRulesPage(driver);

		fc.utobj().clickElement(driver, pobj.AgreementRulesBtn);
		fc.utobj().clickElement(driver, pobj.AddAgreementRule);

		if (!fc.utobj().isSelected(driver,pobj.ARSalesAmountBased)) {
			fc.utobj().clickElement(driver, pobj.ARSalesAmountBased);
		}

		fc.utobj().clickElement(driver, pobj.continueBtn);

		fc.utobj().sendKeys(driver, pobj.AgreementRuleName, ruleNameSAFYTD);

		if (applyMinCapValue.equalsIgnoreCase("Y")) {
			fc.utobj().clickElement(driver, pobj.CheckboxMinimumCap);
			fc.utobj().sendKeys(driver, pobj.MinimumCap, minCapValue);
		}

		if (applyMaxCapValue.equalsIgnoreCase("Y")) {
			fc.utobj().clickElement(driver, pobj.CheckBoxMaximumCap);
			fc.utobj().sendKeys(driver, pobj.MaximumCap, maxCapValue);
		}

		fc.utobj().sendKeys(driver, pobj.Description, "Agreement Rule Sales Amount, Flat and YTD Based");

		if (!fc.utobj().isSelected(driver,pobj.RuleFlatRadioBtn)) {
			fc.utobj().clickElement(driver, pobj.RuleFlatRadioBtn);
		}

		if (!fc.utobj().isSelected(driver,pobj.YTDAccountingBasis)) {
			fc.utobj().clickElement(driver, pobj.YTDAccountingBasis);
		}

		int rowCount = driver.findElements(By.xpath("//table[@id='table1']/tbody")).size();
		if (rowCount == 1) {
			fc.utobj().clickElement(driver, pobj.RuleAddMoreBtn);
		}

		fc.utobj().sendKeys(driver, pobj.RuleSetRangeToFieldZeroRow, rngToFieldZeroRowAR);
		fc.utobj().sendKeys(driver, pobj.RuleAppliedValueForZeroRow, apldValueForZeroRowAR);
		fc.utobj().sendKeys(driver, pobj.RuleAppliedValueForFirstRow, apldValueForFirstRowAR);
		fc.utobj().clickElement(driver, pobj.submitBtn);

		return ruleNameSAFYTD;
	}

	@Test(groups = { "finance" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "TC_FC_AgreementRule_SalesAmount_Flat_Cumulative_014", testCaseDescription = "Agreement Rule Sales Amount Flat Cumulative Based")
	public void addAgreementRuleSalesAmountFlatCumulativeBased() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("finance", testCaseId);

		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		String ruleNameSAFC = fc.utobj().generateTestData(dataSet.get("agreementRuleNSAFCB"));
		String applyMaxCapValue = dataSet.get("applyMaxCapValue");
		String applyMinCapValue = dataSet.get("applyMinCapValue");
		String minCapValue = dataSet.get("minCapValue");
		String maxCapValue = dataSet.get("maxCapValue");
		String rngToFieldZeroRowAR = dataSet.get("rangeToFieldZeroRowAR");
		String apldValueForZeroRowAR = dataSet.get("appliedValueForZeroRowAR");
		String apldValueForFirstRowAR = dataSet.get("appliedValueForFirstRowAR");

		try {
			driver = fc.loginpage().login(driver);
			AdminFinanceAgreementRulesPage pobj = new AdminFinanceAgreementRulesPage(driver);
			fc.utobj().printTestStep(
					"Add Agreement Rule with Type Sales Amount Based Flat and Accounting Basis is Cumulative");

			ruleNameSAFC = addAgreementRuleSalesAmountFlatCumulative(driver, config, ruleNameSAFC, applyMinCapValue,
					applyMaxCapValue, minCapValue, maxCapValue, rngToFieldZeroRowAR, apldValueForZeroRowAR,
					apldValueForFirstRowAR);

			fc.utobj().printTestStep(
					"Verify Add Agreement Rule with Type Sales Amount Based Flat and Accounting Basis is Cumulative");
			boolean isTextPresent = fc.utobj().assertLinkText(driver, ruleNameSAFC);
			if (isTextPresent == false) {

				fc.utobj().selectDropDownByValue(driver, pobj.selectViewPerPage, "500");
				{
					boolean isTextPresent1 = fc.utobj().assertLinkText(driver, ruleNameSAFC);
					if (isTextPresent1 == false) {
						fc.utobj().throwsException("Agreement Rule Sales Amount Flat Cumulative Based is Not added!");
					}
				}
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	public String addAgreementRuleSalesAmountFlatCumulative(WebDriver driver, Map<String, String> config,
			String ruleNameSAFC, String applyMinCapValue, String applyMaxCapValue, String minCapValue,
			String maxCapValue, String rngToFieldZeroRowAR, String apldValueForZeroRowAR, String apldValueForFirstRowAR)
			throws Exception {

		fc.finance().finance_common().adminFinAgreementVersionsPage(driver);
		AdminFinanceAgreementRulesPage pobj = new AdminFinanceAgreementRulesPage(driver);

		fc.utobj().clickElement(driver, pobj.AgreementRulesBtn);
		fc.utobj().clickElement(driver, pobj.AddAgreementRule);

		if (!fc.utobj().isSelected(driver,pobj.ARSalesAmountBased)) {
			fc.utobj().clickElement(driver, pobj.ARSalesAmountBased);
		}

		fc.utobj().clickElement(driver, pobj.continueBtn);

		fc.utobj().sendKeys(driver, pobj.AgreementRuleName, ruleNameSAFC);

		if (applyMinCapValue.equalsIgnoreCase("Y")) {
			fc.utobj().clickElement(driver, pobj.CheckboxMinimumCap);
			fc.utobj().sendKeys(driver, pobj.MinimumCap, minCapValue);
		}

		if (applyMaxCapValue.equalsIgnoreCase("Y")) {
			fc.utobj().clickElement(driver, pobj.CheckBoxMaximumCap);
			fc.utobj().sendKeys(driver, pobj.MaximumCap, maxCapValue);
		}
		fc.utobj().sendKeys(driver, pobj.Description, "Agreement Rule Sales Amount Flat Cumulative Based");

		if (!fc.utobj().isSelected(driver,pobj.RuleFlatRadioBtn)) {
			fc.utobj().clickElement(driver, pobj.RuleFlatRadioBtn);
		}

		if (!fc.utobj().isSelected(driver,pobj.CumulativeAccountingBasis)) {
			fc.utobj().clickElement(driver, pobj.CumulativeAccountingBasis);
		}

		int rowCount = driver.findElements(By.xpath("//table[@id='table1']/tbody")).size();
		if (rowCount == 1) {
			fc.utobj().clickElement(driver, pobj.RuleAddMoreBtn);
		}
		fc.utobj().sendKeys(driver, pobj.RuleSetRangeToFieldZeroRow, rngToFieldZeroRowAR);
		fc.utobj().sendKeys(driver, pobj.RuleAppliedValueForZeroRow, apldValueForZeroRowAR);
		fc.utobj().sendKeys(driver, pobj.RuleAppliedValueForFirstRow, apldValueForFirstRowAR);
		fc.utobj().clickElement(driver, pobj.submitBtn);

		return ruleNameSAFC;
	}

	@Test(groups = { "finance" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "TC_FC_AgreementRule_SalesAmount_Flat_Periodic_Sales_015", testCaseDescription = "Verify add Agreement Rule Sales Amount Flat Periodic Sales Based")
	public void addAgreementRuleSalesAmountFlatPeriodicSalesBased() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("finance", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		String ruleNameSAFPS = fc.utobj().generateTestData(dataSet.get("agreementRuleNameSAFPSB"));
		String applyMinCapValue = dataSet.get("applyMinCapValue");
		String applyMaxCapValue = dataSet.get("applyMaxCapValue");
		String minCapValue = dataSet.get("minCapValue");
		String maxCapValue = dataSet.get("maxCapValue");
		String rngToFieldZeroRowAR = dataSet.get("rangeToFieldZeroRowAR");
		String apldValueForZeroRowAR = dataSet.get("appliedValueForZeroRowAR");
		String apldValueForFirstRowAR = dataSet.get("appliedValueForFirstRowAR");

		try {
			driver = fc.loginpage().login(driver);
			AdminFinanceAgreementRulesPage pobj = new AdminFinanceAgreementRulesPage(driver);
			fc.utobj().printTestStep(
					"Add Agreement Rule with Type Sales Amount Based Flat and Accounting Basis is Periodic Sales");

			ruleNameSAFPS = addAgreementRuleSalesAmountFlatPeriodicSales(driver, config, ruleNameSAFPS,
					applyMinCapValue, applyMaxCapValue, minCapValue, maxCapValue, rngToFieldZeroRowAR,
					apldValueForZeroRowAR, apldValueForFirstRowAR);
			fc.utobj().printTestStep(
					"Add Agreement Rule with Type Sales Amount Based Flat and Accounting Basis is Periodic Sales");

			boolean isTextPresent = fc.utobj().assertLinkText(driver, ruleNameSAFPS);
			if (isTextPresent == false) {
				fc.utobj().selectDropDownByValue(driver, pobj.selectViewPerPage, "500");
				{
					boolean isTextPresent1 = fc.utobj().assertLinkText(driver, ruleNameSAFPS);
					if (isTextPresent1 == false) {

						fc.utobj().throwsException(
								"Agreement Rule Sales Amount, Flat and Periodic Sales Based is Not added!");
					}
				}
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	public String addAgreementRuleSalesAmountFlatPeriodicSales(WebDriver driver, Map<String, String> config,
			String ruleNameSAFPS, String applyMinCapValue, String applyMaxCapValue, String minCapValue,
			String maxCapValue, String rngToFieldZeroRowAR, String apldValueForZeroRowAR, String apldValueForFirstRowAR)
			throws Exception {

		fc.finance().finance_common().adminFinAgreementVersionsPage(driver);
		AdminFinanceAgreementRulesPage pobj = new AdminFinanceAgreementRulesPage(driver);

		fc.utobj().clickElement(driver, pobj.AgreementRulesBtn);
		fc.utobj().clickElement(driver, pobj.AddAgreementRule);

		if (!fc.utobj().isSelected(driver,pobj.ARSalesAmountBased)) {
			fc.utobj().clickElement(driver, pobj.ARSalesAmountBased);
		}

		fc.utobj().clickElement(driver, pobj.continueBtn);

		fc.utobj().sendKeys(driver, pobj.AgreementRuleName, ruleNameSAFPS);

		if (applyMinCapValue.equalsIgnoreCase("Y")) {
			fc.utobj().clickElement(driver, pobj.CheckboxMinimumCap);
			fc.utobj().sendKeys(driver, pobj.MinimumCap, minCapValue);
		}

		if (applyMaxCapValue.equalsIgnoreCase("Y")) {
			fc.utobj().clickElement(driver, pobj.CheckBoxMaximumCap);
			fc.utobj().sendKeys(driver, pobj.MaximumCap, maxCapValue);
		}
		fc.utobj().sendKeys(driver, pobj.Description, "Agreement Rule Sales Amount, Flat and Periodic Sales Based");

		if (!fc.utobj().isSelected(driver,pobj.RuleFlatRadioBtn)) {
			fc.utobj().clickElement(driver, pobj.RuleFlatRadioBtn);
		}

		if (!fc.utobj().isSelected(driver,pobj.PeriodicSalesAccountingBasis)) {
			fc.utobj().clickElement(driver, pobj.PeriodicSalesAccountingBasis);
		}

		int rowCount = driver.findElements(By.xpath("//table[@id='table1']/tbody")).size();
		if (rowCount == 1) {
			fc.utobj().clickElement(driver, pobj.RuleAddMoreBtn);
		}

		fc.utobj().sendKeys(driver, pobj.RuleSetRangeToFieldZeroRow, rngToFieldZeroRowAR);
		fc.utobj().sendKeys(driver, pobj.RuleAppliedValueForZeroRow, apldValueForZeroRowAR);
		fc.utobj().sendKeys(driver, pobj.RuleAppliedValueForFirstRow, apldValueForFirstRowAR);
		fc.utobj().clickElement(driver, pobj.submitBtn);

		return ruleNameSAFPS;
	}

	@Test(groups = { "finance" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "TC_FC_AgreementRule_SalesAmount_Flat_YTD_RRSD_016", testCaseDescription = "Agreement Rule Sales Amount, Flat and YTD from Royalty Reporting Start Date Based")
	public void addAgreementRuleSalesAmountFlatYTDfromRRSDBased() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("finance", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		String ruleNameSAFYTDfromRRSD = fc.utobj().generateTestData(dataSet.get("agreementRuleNameSAFYTDfromRRSD"));
		String applyMinCapValue = dataSet.get("applyMinCapValue");
		String applyMaxCapValue = dataSet.get("applyMaxCapValue");
		String minCapValue = dataSet.get("minCapValue");
		String maxCapValue = dataSet.get("maxCapValue");
		String rngToFieldZeroRowAR = dataSet.get("rangeToFieldZeroRowAR");
		String apldValueForZeroRowAR = dataSet.get("appliedValueForZeroRowAR");
		String apldValueForFirstRowAR = dataSet.get("appliedValueForFirstRowAR");

		try {
			driver = fc.loginpage().login(driver);
			AdminFinanceAgreementRulesPage pobj = new AdminFinanceAgreementRulesPage(driver);
			fc.utobj().printTestStep(
					"Add Agreement Rule with Type Sales Amount Based Flat and Accounting Basis is YTD from Royalty Reporting Start Date");

			ruleNameSAFYTDfromRRSD = addAgreementRuleSalesAmountFlatYTDfromRRSD(driver, config, ruleNameSAFYTDfromRRSD,
					applyMinCapValue, applyMaxCapValue, minCapValue, maxCapValue, rngToFieldZeroRowAR,
					apldValueForZeroRowAR, apldValueForFirstRowAR);
			fc.utobj().printTestStep(
					"Verify Add Agreement Rule with Type Sales Amount Based Flat and Accounting Basis is YTD from Royalty Reporting Start Date");

			boolean isTextPresent = fc.utobj().assertLinkText(driver, ruleNameSAFYTDfromRRSD);
			if (isTextPresent == false) {
				fc.utobj().selectDropDownByValue(driver, pobj.selectViewPerPage, "500");
				{
					boolean isTextPresent1 = fc.utobj().assertLinkText(driver, ruleNameSAFYTDfromRRSD);
					if (isTextPresent1 == false) {

						fc.utobj().throwsException(
								"Agreement Rule Sales Amount, Flat and YTD from Royalty Reporting Start Date Based is Not added!");
					}
				}
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	public String addAgreementRuleSalesAmountFlatYTDfromRRSD(WebDriver driver, Map<String, String> config,
			String ruleNameSAFYTDfromRRSD, String applyMinCapValue, String applyMaxCapValue, String minCapValue,
			String maxCapValue, String rngToFieldZeroRowAR, String apldValueForZeroRowAR, String apldValueForFirstRowAR)
			throws Exception {

		fc.finance().finance_common().adminFinAgreementVersionsPage(driver);
		AdminFinanceAgreementRulesPage pobj = new AdminFinanceAgreementRulesPage(driver);

		fc.utobj().clickElement(driver, pobj.AgreementRulesBtn);
		fc.utobj().clickElement(driver, pobj.AddAgreementRule);

		if (!fc.utobj().isSelected(driver,pobj.ARSalesAmountBased)) {
			fc.utobj().clickElement(driver, pobj.ARSalesAmountBased);
		}
		fc.utobj().clickElement(driver, pobj.continueBtn);

		fc.utobj().sendKeys(driver, pobj.AgreementRuleName, ruleNameSAFYTDfromRRSD);

		if (applyMinCapValue.equalsIgnoreCase("Y")) {
			fc.utobj().clickElement(driver, pobj.CheckboxMinimumCap);
			fc.utobj().sendKeys(driver, pobj.MinimumCap, minCapValue);
		}

		if (applyMaxCapValue.equalsIgnoreCase("Y")) {
			fc.utobj().clickElement(driver, pobj.CheckBoxMaximumCap);
			fc.utobj().sendKeys(driver, pobj.MaximumCap, maxCapValue);
		}
		fc.utobj().sendKeys(driver, pobj.Description,
				"Agreement Rule Sales Amount, Flat and YTD from Royalty Reporting Start Date Based");

		if (!fc.utobj().isSelected(driver,pobj.RuleFlatRadioBtn)) {
			fc.utobj().clickElement(driver, pobj.RuleFlatRadioBtn);
		}

		if (!fc.utobj().isSelected(driver,pobj.YTDfromRoyaltyReportingStartDateAccountingBasis)) {
			fc.utobj().clickElement(driver, pobj.YTDfromRoyaltyReportingStartDateAccountingBasis);
		}

		int rowCount = driver.findElements(By.xpath("//table[@id='table1']/tbody")).size();
		if (rowCount == 1) {
			fc.utobj().clickElement(driver, pobj.RuleAddMoreBtn);
		}

		fc.utobj().sendKeys(driver, pobj.RuleSetRangeToFieldZeroRow, rngToFieldZeroRowAR);
		fc.utobj().sendKeys(driver, pobj.RuleAppliedValueForZeroRow, apldValueForZeroRowAR);
		fc.utobj().sendKeys(driver, pobj.RuleAppliedValueForFirstRow, apldValueForFirstRowAR);
		fc.utobj().clickElement(driver, pobj.submitBtn);

		return ruleNameSAFYTDfromRRSD;
	}

	@Test(groups = { "finance" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "TC_FC_AgreementRule_Sales_Report_Period_Percent_017", testCaseDescription = "Agreement Rule Sales Report Period Percent Based")
	public void addAgreementRuleSalesReportPeriodPercentBased() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("finance", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		String ruleNameSRPP = fc.utobj().generateTestData(dataSet.get("agreementRuleSRPPB"));
		String applyMinCapValue = dataSet.get("applyMinCapValue");
		String applyMaxCapValue = dataSet.get("applyMaxCapValue");
		String minCapValue = dataSet.get("minCapValue");
		String maxCapValue = dataSet.get("maxCapValue");
		String rngToFieldZeroRowAR = dataSet.get("rangeToFieldZeroRowAR");
		String apldValueForZeroRowAR = dataSet.get("appliedValueForZeroRowAR");
		String apldValueForFirstRowAR = dataSet.get("appliedValueForFirstRowAR");

		try {
			driver = fc.loginpage().login(driver);
			AdminFinanceAgreementRulesPage pobj = new AdminFinanceAgreementRulesPage(driver);
			fc.utobj()
					.printTestStep("Add Agreement Rule with Type Sales Report Period Based and Apply As A Percentage");

			ruleNameSRPP = addAgreementRuleSalesReportPeriodPercent(driver, config, ruleNameSRPP, applyMinCapValue,
					applyMaxCapValue, minCapValue, maxCapValue, rngToFieldZeroRowAR, apldValueForZeroRowAR,
					apldValueForFirstRowAR);
			fc.utobj().printTestStep(
					"Verify Add Agreement Rule with Type Sales Report Period Based and Apply As A Percentage");

			boolean isTextPresent = fc.utobj().assertLinkText(driver, ruleNameSRPP);
			if (isTextPresent == false) {
				fc.utobj().selectDropDownByValue(driver, pobj.selectViewPerPage, "500");
				{
					boolean isTextPresent1 = fc.utobj().assertLinkText(driver, ruleNameSRPP);
					if (isTextPresent1 == false) {

						fc.utobj()
								.throwsException("Agreement Rule Sales Report Period and Percent Based is Not added!");
					}
				}
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	public String addAgreementRuleSalesReportPeriodPercent(WebDriver driver, Map<String, String> config,
			String ruleNameSRPP, String applyMinCapValue, String applyMaxCapValue, String minCapValue,
			String maxCapValue, String rngToFieldZeroRowAR, String apldValueForZeroRowAR, String apldValueForFirstRowAR)
			throws Exception {

		fc.finance().finance_common().adminFinAgreementVersionsPage(driver);
		AdminFinanceAgreementRulesPage pobj = new AdminFinanceAgreementRulesPage(driver);

		fc.utobj().clickElement(driver, pobj.AgreementRulesBtn);
		fc.utobj().clickElement(driver, pobj.AddAgreementRule);

		if (!fc.utobj().isSelected(driver,pobj.ARSalesReportPeriodBased)) {
			fc.utobj().clickElement(driver, pobj.ARSalesReportPeriodBased);
		}

		fc.utobj().clickElement(driver, pobj.continueBtn);

		fc.utobj().sendKeys(driver, pobj.AgreementRuleName, ruleNameSRPP);

		if (applyMinCapValue.equalsIgnoreCase("Y")) {
			fc.utobj().clickElement(driver, pobj.CheckboxMinimumCap);
			fc.utobj().sendKeys(driver, pobj.MinimumCap, minCapValue);
		}

		if (applyMaxCapValue.equalsIgnoreCase("Y")) {
			fc.utobj().clickElement(driver, pobj.CheckBoxMaximumCap);
			fc.utobj().sendKeys(driver, pobj.MaximumCap, maxCapValue);
		}
		fc.utobj().sendKeys(driver, pobj.Description, "Agreement Rule Sales Report Period Percent Based");

		if (!fc.utobj().isSelected(driver,pobj.RulePercentageRadioBtn)) {
			fc.utobj().clickElement(driver, pobj.RulePercentageRadioBtn);
		}

		fc.utobj().sendKeys(driver, pobj.RangeToConf, rngToFieldZeroRowAR);
		fc.utobj().sendKeys(driver, pobj.RuleAppliedValueForZeroRow, apldValueForZeroRowAR);
		fc.utobj().sendKeys(driver, pobj.RuleAppliedValueForFirstRow, apldValueForFirstRowAR);
		fc.utobj().clickElement(driver, pobj.submitBtn);

		return ruleNameSRPP;
	}

	@Test(groups = { "finance" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "TC_FC_AgreementRule_Sales_Report_Period_Flat_018", testCaseDescription = "Verify Agreement Rule Sales Report Period")
	public void addAgreementRuleSalesReportPeriodFlatBased() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("finance", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		String ruleNameSRPF = fc.utobj().generateTestData(dataSet.get("agreementRuleSRPFB"));
		String applyMinCapValue = dataSet.get("applyMinCapValue");
		String applyMaxCapValue = dataSet.get("applyMaxCapValue");
		String minCapValue = dataSet.get("minCapValue");
		String maxCapValue = dataSet.get("maxCapValue");
		String rngToFieldZeroRowAR = dataSet.get("rangeToFieldZeroRowAR");
		String apldValueForZeroRowAR = dataSet.get("appliedValueForZeroRowAR");
		String apldValueForFirstRowAR = dataSet.get("appliedValueForFirstRowAR");

		try {
			driver = fc.loginpage().login(driver);
			AdminFinanceAgreementRulesPage pobj = new AdminFinanceAgreementRulesPage(driver);
			fc.utobj().printTestStep("Add Agreement Rule with Type Sales Report Period Based and Apply As A Flat");

			ruleNameSRPF = addAgreementRuleSalesReportPeriodFlat(driver, config, ruleNameSRPF, applyMinCapValue,
					applyMaxCapValue, minCapValue, maxCapValue, rngToFieldZeroRowAR, apldValueForZeroRowAR,
					apldValueForFirstRowAR);

			fc.utobj()
					.printTestStep("Verify Add Agreement Rule with Type Sales Report Period Based and Apply As A Flat");
			boolean isTextPresent = fc.utobj().assertLinkText(driver, ruleNameSRPF);
			if (isTextPresent == false) {

				fc.utobj().selectDropDownByValue(driver, pobj.selectViewPerPage, "500");
				{
					boolean isTextPresent1 = fc.utobj().assertLinkText(driver, ruleNameSRPF);
					if (isTextPresent1 == false) {

						fc.utobj().throwsException("Agreement Rule Sales Report Period and Flat Based is Not added!");
					}
				}
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	public String addAgreementRuleSalesReportPeriodFlat(WebDriver driver, Map<String, String> config,
			String ruleNameSRPF, String applyMinCapValue, String applyMaxCapValue, String minCapValue,
			String maxCapValue, String rngToFieldZeroRowAR, String apldValueForZeroRowAR, String apldValueForFirstRowAR)
			throws Exception {

		fc.finance().finance_common().adminFinAgreementVersionsPage(driver);
		AdminFinanceAgreementRulesPage pobj = new AdminFinanceAgreementRulesPage(driver);

		fc.utobj().clickElement(driver, pobj.AgreementRulesBtn);
		fc.utobj().clickElement(driver, pobj.AddAgreementRule);
		fc.utobj().clickElement(driver, pobj.ARSalesReportPeriodBased);
		fc.utobj().clickElement(driver, pobj.continueBtn);

		fc.utobj().sendKeys(driver, pobj.AgreementRuleName, ruleNameSRPF);

		if (applyMinCapValue.equalsIgnoreCase("Y")) {
			fc.utobj().clickElement(driver, pobj.CheckboxMinimumCap);
			fc.utobj().sendKeys(driver, pobj.MinimumCap, minCapValue);
		}

		if (applyMaxCapValue.equalsIgnoreCase("Y")) {
			fc.utobj().clickElement(driver, pobj.CheckBoxMaximumCap);
			fc.utobj().sendKeys(driver, pobj.MaximumCap, maxCapValue);
		}

		fc.utobj().sendKeys(driver, pobj.Description, "Agreement Rule Sales Report Period and Flat Based");
		fc.utobj().clickElement(driver, pobj.RuleFlatRadioBtn);
		fc.utobj().sendKeys(driver, pobj.RangeToConf, rngToFieldZeroRowAR);
		fc.utobj().sendKeys(driver, pobj.RuleAppliedValueForZeroRow, apldValueForZeroRowAR);
		fc.utobj().sendKeys(driver, pobj.RuleAppliedValueForFirstRow, apldValueForFirstRowAR);
		fc.utobj().clickElement(driver, pobj.submitBtn);

		return ruleNameSRPF;
	}

	@Test(groups = { "finance" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "TC_FC_AgreementRule_Store_Age_Percent_019", testCaseDescription = "Verify Add Agreement Rule with Type Store Age Based and Apply As A Percentage")
	public void addAgreementRuleStoreAgePercentBased() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("finance", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		String ruleNameSAgeP = fc.utobj().generateTestData(dataSet.get("agreementRuleSAgePB"));
		String applyMinCapValue = dataSet.get("applyMinCapValue");
		String applyMaxCapValue = dataSet.get("applyMaxCapValue");
		String minCapValue = dataSet.get("minCapValue");
		String maxCapValue = dataSet.get("maxCapValue");
		String rngToFieldZeroRowAR = dataSet.get("rangeToFieldZeroRowAR");
		String apldValueForZeroRowAR = dataSet.get("appliedValueForZeroRowAR");
		String apldValueForFirstRowAR = dataSet.get("appliedValueForFirstRowAR");

		try {
			driver = fc.loginpage().login(driver);
			AdminFinanceAgreementRulesPage pobj = new AdminFinanceAgreementRulesPage(driver);
			fc.utobj().printTestStep("Add Agreement Rule with Type Store Age Based and Apply As A Percentage");

			ruleNameSAgeP = addAgreementRuleStoreAgePercent(driver, config, ruleNameSAgeP, applyMinCapValue,
					applyMaxCapValue, minCapValue, maxCapValue, rngToFieldZeroRowAR, apldValueForZeroRowAR,
					apldValueForFirstRowAR);
			fc.utobj().printTestStep("Verify Add Agreement Rule with Type Store Age Based and Apply As A Percentage");

			boolean isTextPresent = fc.utobj().assertLinkText(driver, ruleNameSAgeP);
			if (isTextPresent == false) {
				fc.utobj().selectDropDownByValue(driver, pobj.selectViewPerPage, "500");
				{
					boolean isTextPresent1 = fc.utobj().assertLinkText(driver, ruleNameSAgeP);
					if (isTextPresent1 == false) {
						fc.utobj().throwsException("Agreement Rule Store Age and Percent Based is Not added!");
					}
				}
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	public String addAgreementRuleStoreAgePercent(WebDriver driver, Map<String, String> config, String ruleNameSAgeP,
			String applyMinCapValue, String applyMaxCapValue, String minCapValue, String maxCapValue,
			String rngToFieldZeroRowAR, String apldValueForZeroRowAR, String apldValueForFirstRowAR) throws Exception {

		fc.finance().finance_common().adminFinAgreementVersionsPage(driver);
		AdminFinanceAgreementRulesPage pobj = new AdminFinanceAgreementRulesPage(driver);

		fc.utobj().clickElement(driver, pobj.AgreementRulesBtn);
		fc.utobj().clickElement(driver, pobj.AddAgreementRule);
		fc.utobj().clickElement(driver, pobj.ARStoreAgeBased);
		fc.utobj().clickElement(driver, pobj.continueBtn);

		fc.utobj().sendKeys(driver, pobj.AgreementRuleName, ruleNameSAgeP);

		if (applyMinCapValue.equalsIgnoreCase("Y")) {
			fc.utobj().clickElement(driver, pobj.CheckboxMinimumCap);
			fc.utobj().sendKeys(driver, pobj.MinimumCap, minCapValue);
		}

		if (applyMaxCapValue.equalsIgnoreCase("Y")) {
			fc.utobj().clickElement(driver, pobj.CheckBoxMaximumCap);
			fc.utobj().sendKeys(driver, pobj.MaximumCap, maxCapValue);
		}
		fc.utobj().sendKeys(driver, pobj.Description, "Agreement Rule Store Age and Percent Based");
		fc.utobj().clickElement(driver, pobj.RulePercentageRadioBtn);
		int rowCount = driver.findElements(By.xpath("//table[@id='table1']/tbody")).size();
		if (rowCount == 1) {
			fc.utobj().clickElement(driver, pobj.RuleAddMoreBtn);
		}

		fc.utobj().sendKeys(driver, pobj.RuleSetRangeToFieldZeroRow, rngToFieldZeroRowAR);
		fc.utobj().sendKeys(driver, pobj.RuleAppliedValueForZeroRow, apldValueForZeroRowAR);
		fc.utobj().sendKeys(driver, pobj.RuleAppliedValueForFirstRow, apldValueForFirstRowAR);
		fc.utobj().clickElement(driver, pobj.submitBtn);

		return ruleNameSAgeP;
	}

	@Test(groups = { "finance" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "TC_FC_AgreementRule_Store_Age_Flat_020", testCaseDescription = "Verify Add Agreement Rule with Type Store Age Based and Apply As A Flat")
	public void addAgreementRuleStoreAgeFlatBased() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("finance", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		String ruleNameSAgeF = fc.utobj().generateTestData(dataSet.get("agreementRuleSAgeFB"));
		String applyMinCapValue = dataSet.get("applyMinCapValue");
		String applyMaxCapValue = dataSet.get("applyMaxCapValue");
		String minCapValue = dataSet.get("minCapValue");
		String maxCapValue = dataSet.get("maxCapValue");
		String rngToFieldZeroRowAR = dataSet.get("rangeToFieldZeroRowAR");
		String apldValueForZeroRowAR = dataSet.get("appliedValueForZeroRowAR");
		String apldValueForFirstRowAR = dataSet.get("appliedValueForFirstRowAR");

		try {
			driver = fc.loginpage().login(driver);

			AdminFinanceAgreementRulesPage pobj = new AdminFinanceAgreementRulesPage(driver);
			fc.utobj().printTestStep("Add Agreement Rule with Type Store Age Based and Apply As A Flat");

			ruleNameSAgeF = addAgreementRuleStoreAgeFlat(driver, config, ruleNameSAgeF, applyMinCapValue,
					applyMaxCapValue, minCapValue, maxCapValue, rngToFieldZeroRowAR, apldValueForZeroRowAR,
					apldValueForFirstRowAR);

			fc.utobj().printTestStep("Verify Add Agreement Rule with Type Store Age Based and Apply As A Flat");
			boolean isTextPresent = fc.utobj().assertLinkText(driver, ruleNameSAgeF);
			if (isTextPresent == false) {
				fc.utobj().selectDropDownByValue(driver, pobj.selectViewPerPage, "500");

				{
					boolean isTextPresent1 = fc.utobj().assertLinkText(driver, ruleNameSAgeF);
					if (isTextPresent1 == false) {
						fc.utobj().throwsException("Agreement Rule Store Age and Flat Based is Not added!");
					}
				}
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	public String addAgreementRuleStoreAgeFlat(WebDriver driver, Map<String, String> config, String ruleNameSAgeF,
			String applyMinCapValue, String applyMaxCapValue, String minCapValue, String maxCapValue,
			String rngToFieldZeroRowAR, String apldValueForZeroRowAR, String apldValueForFirstRowAR) throws Exception {

		fc.finance().finance_common().adminFinAgreementVersionsPage(driver);
		AdminFinanceAgreementRulesPage pobj = new AdminFinanceAgreementRulesPage(driver);

		fc.utobj().clickElement(driver, pobj.AgreementRulesBtn);
		fc.utobj().clickElement(driver, pobj.AddAgreementRule);
		fc.utobj().clickElement(driver, pobj.ARStoreAgeBased);
		fc.utobj().clickElement(driver, pobj.continueBtn);

		fc.utobj().sendKeys(driver, pobj.AgreementRuleName, ruleNameSAgeF);

		if (applyMinCapValue.equalsIgnoreCase("Y")) {
			fc.utobj().clickElement(driver, pobj.CheckboxMinimumCap);
			fc.utobj().sendKeys(driver, pobj.MinimumCap, minCapValue);
		}

		if (applyMaxCapValue.equalsIgnoreCase("Y")) {
			fc.utobj().clickElement(driver, pobj.CheckBoxMaximumCap);
			fc.utobj().sendKeys(driver, pobj.MaximumCap, maxCapValue);
		}
		fc.utobj().sendKeys(driver, pobj.Description, "Agreement Rule Store Age and Flat Based");
		fc.utobj().clickElement(driver, pobj.RuleFlatRadioBtn);

		int rowCount = driver.findElements(By.xpath("//table[@id='table1']/tbody")).size();
		if (rowCount == 1) {
			fc.utobj().clickElement(driver, pobj.RuleAddMoreBtn);
		}
		fc.utobj().sendKeys(driver, pobj.RuleSetRangeToFieldZeroRow, rngToFieldZeroRowAR);
		fc.utobj().sendKeys(driver, pobj.RuleAppliedValueForZeroRow, apldValueForZeroRowAR);
		fc.utobj().sendKeys(driver, pobj.RuleAppliedValueForFirstRow, apldValueForFirstRowAR);
		fc.utobj().clickElement(driver, pobj.submitBtn);

		return ruleNameSAgeF;
	}

	@Test(groups = { "finance" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "TC_FC_AgreementRule_Quantity_Percent_021", testCaseDescription = "Verify Add Agreement Rule with Type Quantity Based and Apply As A Percentage")
	public void addAgreementRuleQuantityPercentBased() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("finance", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		String ruleNameQP = fc.utobj().generateTestData(dataSet.get("agreementRuleQPB"));
		String applyMinCapValue = dataSet.get("applyMinCapValue");
		String applyMaxCapValue = dataSet.get("applyMaxCapValue");
		String minCapValue = dataSet.get("minCapValue");
		String maxCapValue = dataSet.get("maxCapValue");
		String rngToFieldZeroRowAR = dataSet.get("rangeToFieldZeroRowAR");
		String apldValueForZeroRowAR = dataSet.get("appliedValueForZeroRowAR");
		String apldValueForFirstRowAR = dataSet.get("appliedValueForFirstRowAR");

		try {
			driver = fc.loginpage().login(driver);
			AdminFinanceAgreementRulesPage pobj = new AdminFinanceAgreementRulesPage(driver);
			fc.utobj().printTestStep("Add Agreement Rule with Type Quantity Based and Apply As A Percentage");

			ruleNameQP = addAgreementRuleQuantityPercent(driver, config, ruleNameQP, applyMinCapValue, applyMaxCapValue,
					minCapValue, maxCapValue, rngToFieldZeroRowAR, apldValueForZeroRowAR, apldValueForFirstRowAR);

			fc.utobj().printTestStep("Verify Add Agreement Rule with Type Quantity Based and Apply As A Percentage");

			boolean isTextPresent = fc.utobj().assertLinkText(driver, ruleNameQP);
			if (isTextPresent == false) {
				fc.utobj().selectDropDownByValue(driver, pobj.selectViewPerPage, "500");
				{
					boolean isTextPresent1 = fc.utobj().assertLinkText(driver, ruleNameQP);
					if (isTextPresent1 == false) {
						fc.utobj().throwsException("Agreement Rule Quantity and Percent Based is Not added!");
					}
				}
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	public String addAgreementRuleQuantityPercent(WebDriver driver, Map<String, String> config, String ruleNameQP,
			String applyMinCapValue, String applyMaxCapValue, String minCapValue, String maxCapValue,
			String rngToFieldZeroRowAR, String apldValueForZeroRowAR, String apldValueForFirstRowAR) throws Exception {

		fc.finance().finance_common().adminFinAgreementVersionsPage(driver);
		AdminFinanceAgreementRulesPage pobj = new AdminFinanceAgreementRulesPage(driver);

		fc.utobj().clickElement(driver, pobj.AgreementRulesBtn);
		fc.utobj().clickElement(driver, pobj.AddAgreementRule);
		fc.utobj().clickElement(driver, pobj.ARQuantityBased);
		fc.utobj().clickElement(driver, pobj.continueBtn);

		fc.utobj().sendKeys(driver, pobj.AgreementRuleName, ruleNameQP);

		if (applyMinCapValue.equalsIgnoreCase("Y")) {
			fc.utobj().clickElement(driver, pobj.CheckboxMinimumCap);
			fc.utobj().sendKeys(driver, pobj.MinimumCap, minCapValue);
		}

		if (applyMaxCapValue.equalsIgnoreCase("Y")) {
			fc.utobj().clickElement(driver, pobj.CheckBoxMaximumCap);
			fc.utobj().sendKeys(driver, pobj.MaximumCap, maxCapValue);
		}
		fc.utobj().sendKeys(driver, pobj.Description, "Agreement Rule Quantity and Percent Based");
		fc.utobj().clickElement(driver, pobj.RulePercentageRadioBtn);
		int rowCount = driver.findElements(By.xpath("//table[@id='table1']/tbody")).size();
		if (rowCount == 1) {
			fc.utobj().clickElement(driver, pobj.RuleAddMoreBtn);
		}
		fc.utobj().sendKeys(driver, pobj.RuleSetRangeToFieldZeroRow, rngToFieldZeroRowAR);
		fc.utobj().sendKeys(driver, pobj.RuleAppliedValueForZeroRow, apldValueForZeroRowAR);
		fc.utobj().sendKeys(driver, pobj.RuleAppliedValueForFirstRow, apldValueForFirstRowAR);
		fc.utobj().clickElement(driver, pobj.submitBtn);

		return ruleNameQP;
	}

	@Test(groups = { "finance" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "TC_FC_AgreementRule_Quantity_Flat_022", testCaseDescription = "Verify add Agreement Rule Quantity Flat Based")
	public void addAgreementRuleQuantityFlatBased() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("finance", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		String ruleNameQF = fc.utobj().generateTestData(dataSet.get("agreementRuleQFB"));
		String applyMinCapValue = dataSet.get("applyMinCapValue");
		String applyMaxCapValue = dataSet.get("applyMaxCapValue");
		String minCapValue = dataSet.get("minCapValue");
		String maxCapValue = dataSet.get("maxCapValue");
		String rngToFieldZeroRowAR = dataSet.get("rangeToFieldZeroRowAR");
		String apldValueForZeroRowAR = dataSet.get("appliedValueForZeroRowAR");
		String apldValueForFirstRowAR = dataSet.get("appliedValueForFirstRowAR");

		try {
			driver = fc.loginpage().login(driver);

			AdminFinanceAgreementRulesPage pobj = new AdminFinanceAgreementRulesPage(driver);
			fc.utobj().printTestStep("Add Agreement Rule with Type Quantity Based and Apply As A Flat");

			ruleNameQF = addAgreementRuleQuantityFlat(driver, config, ruleNameQF, applyMinCapValue, applyMaxCapValue,
					minCapValue, maxCapValue, rngToFieldZeroRowAR, apldValueForZeroRowAR, apldValueForFirstRowAR);

			fc.utobj().printTestStep("Verify Add Agreement Rule with Type Quantity Based and Apply As A Flat");

			boolean isTextPresent = fc.utobj().assertLinkText(driver, ruleNameQF);
			if (isTextPresent == false) {
				fc.utobj().selectDropDownByValue(driver, pobj.selectViewPerPage, "500");

				{
					boolean isTextPresent1 = fc.utobj().assertLinkText(driver, ruleNameQF);
					if (isTextPresent1 == false) {
						fc.utobj().throwsException("Agreement Rule Quantity and Flat Based is Not added!");
					}
				}
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	public String addAgreementRuleQuantityFlat(WebDriver driver, Map<String, String> config, String ruleNameQF,
			String applyMinCapValue, String applyMaxCapValue, String minCapValue, String maxCapValue,
			String rngToFieldZeroRowAR, String apldValueForZeroRowAR, String apldValueForFirstRowAR) throws Exception {

		fc.finance().finance_common().adminFinAgreementVersionsPage(driver);
		AdminFinanceAgreementRulesPage pobj = new AdminFinanceAgreementRulesPage(driver);

		fc.utobj().clickElement(driver, pobj.AgreementRulesBtn);
		fc.utobj().clickElement(driver, pobj.AddAgreementRule);
		fc.utobj().clickElement(driver, pobj.ARQuantityBased);
		fc.utobj().clickElement(driver, pobj.continueBtn);

		fc.utobj().sendKeys(driver, pobj.AgreementRuleName, ruleNameQF);

		if (applyMinCapValue.equalsIgnoreCase("Y")) {
			fc.utobj().clickElement(driver, pobj.CheckboxMinimumCap);
			fc.utobj().sendKeys(driver, pobj.MinimumCap, minCapValue);
		}

		if (applyMaxCapValue.equalsIgnoreCase("Y")) {
			fc.utobj().clickElement(driver, pobj.CheckBoxMaximumCap);
			fc.utobj().sendKeys(driver, pobj.MaximumCap, maxCapValue);
		}
		fc.utobj().sendKeys(driver, pobj.Description, "Agreement Rule Quantity and Flat Based");
		fc.utobj().clickElement(driver, pobj.RuleFlatRadioBtn);

		int rowCount = driver.findElements(By.xpath("//table[@id='table1']/tbody")).size();
		if (rowCount == 1) {
			fc.utobj().clickElement(driver, pobj.RuleAddMoreBtn);
		}
		fc.utobj().sendKeys(driver, pobj.RuleSetRangeToFieldZeroRow, rngToFieldZeroRowAR);
		fc.utobj().sendKeys(driver, pobj.RuleAppliedValueForZeroRow, apldValueForZeroRowAR);
		fc.utobj().sendKeys(driver, pobj.RuleAppliedValueForFirstRow, apldValueForFirstRowAR);
		fc.utobj().clickElement(driver, pobj.submitBtn);

		return ruleNameQF;
	}

	@Test(groups = { "finance" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "TC_FC_Category_Sales_Percent_023", testCaseDescription = "Verify add Agreement Rule Category SubCategory Quantity Sales Category Sales Percent Based")
	public void addAgreementRuleCategorySubCategoryQuantitySalesCategorySalesPercentBased() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("finance", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		String ruleCSP = fc.utobj().generateTestData(dataSet.get("agreementRuleCSPB"));
		String applyMinCapValue = dataSet.get("applyMinCapValue");
		String applyMaxCapValue = dataSet.get("applyMaxCapValue");
		String minCapValue = dataSet.get("minCapValue");
		String maxCapValue = dataSet.get("maxCapValue");
		String rngToFieldDefaultZeroRowAR = dataSet.get("rangeToFieldDefaultZeroRowAR");
		String appliedValueForDefaultZeroRow = dataSet.get("appliedValueForDefaultZeroRowAR");
		String appliedValueForDefaultFirstRow = dataSet.get("appliedValueForDefaultFirstRowAR");
		String rngToFieldServiceZeroRow = dataSet.get("rangeToFieldServiceZeroRowAR");
		String appliedValueForServiceZeroRow = dataSet.get("appliedValueForServiceZeroRowAR");
		String appliedValueForServiceFirstRow = dataSet.get("appliedValueForServiceFirstRowAR");

		try {
			driver = fc.loginpage().login(driver);
			AdminFinanceAgreementRulesPage pobj = new AdminFinanceAgreementRulesPage(driver);
			fc.utobj().printTestStep("Add Agreement Rule with Type Category Sales Based and Apply As A Percentage");

			ruleCSP = addAgreementRuleCategorySubCategoryQuantitySalesCategorySalesPercent(driver, config, ruleCSP,
					applyMinCapValue, applyMaxCapValue, minCapValue, maxCapValue, rngToFieldDefaultZeroRowAR,
					appliedValueForDefaultZeroRow, appliedValueForDefaultFirstRow, rngToFieldServiceZeroRow,
					appliedValueForServiceZeroRow, appliedValueForServiceFirstRow);

			fc.utobj().printTestStep(
					"Verify Add Agreement Rule with Type Category Sales Based and Apply As A Percentage");

			boolean isTextPresent = fc.utobj().assertLinkText(driver, ruleCSP);
			if (isTextPresent == false) {
				fc.utobj().selectDropDownByValue(driver, pobj.selectViewPerPage, "500");

				{
					boolean isTextPresent1 = fc.utobj().assertLinkText(driver, ruleCSP);
					if (isTextPresent1 == false) {
						fc.utobj().throwsException(
								"Agreement Rule 'Category-Sub Category Quantity / Sales Based', Category Sales and Percent Based is Not added!");
					}
				}
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	public String addAgreementRuleCategorySubCategoryQuantitySalesCategorySalesPercent(WebDriver driver,
			Map<String, String> config, String ruleCSP, String applyMinCapValue, String applyMaxCapValue,
			String minCapValue, String maxCapValue, String rngToFieldDefaultZeroRowAR,
			String appliedValueForDefaultZeroRow, String appliedValueForDefaultFirstRow,
			String rngToFieldServiceZeroRow, String appliedValueForServiceZeroRow,
			String appliedValueForServiceFirstRow) throws Exception {

		fc.finance().finance_common().adminFinAgreementVersionsPage(driver);
		AdminFinanceAgreementRulesPage pobj = new AdminFinanceAgreementRulesPage(driver);

		fc.utobj().clickElement(driver, pobj.AgreementRulesBtn);
		fc.utobj().clickElement(driver, pobj.AddAgreementRule);
		fc.utobj().clickElement(driver, pobj.ARCategorySubCategoryQuantitySalesBased);
		fc.utobj().clickElement(driver, pobj.continueBtn);

		fc.utobj().clickElement(driver, pobj.categorySalesBased);
		fc.utobj().sendKeys(driver, pobj.AgreementRuleName, ruleCSP);

		if (applyMinCapValue.equalsIgnoreCase("Y")) {
			fc.utobj().clickElement(driver, pobj.CheckboxMinimumCap);
			fc.utobj().sendKeys(driver, pobj.MinimumCap, minCapValue);
		}

		if (applyMaxCapValue.equalsIgnoreCase("Y")) {
			fc.utobj().clickElement(driver, pobj.CheckBoxMaximumCap);
			fc.utobj().sendKeys(driver, pobj.MaximumCap, maxCapValue);
		}

		fc.utobj().sendKeys(driver, pobj.Description,
				"Agreement Rule 'Category-Sub Category Quantity / Sales Based', Category Sales and Percent Based");
		fc.utobj().clickElement(driver, pobj.RulePercentageRadioBtn);
		fc.utobj().clickElement(driver, pobj.categoryDropDown);

		WebElement element = pobj.categoryDropDown.findElement(By.xpath(".//div//div[@class='ms-search']//input"));
		WebElement element2 = pobj.categoryDropDown.findElement(By.xpath(".//input[@id='selectItemcategories']"));
		fc.utobj().clickElement(driver, element2);
		fc.utobj().sendKeys(driver, element, "Service");

		int rowCount = driver.findElements(By.xpath("//table[@id='table_-1']/tbody")).size();
		if (rowCount == 1) {
			fc.utobj().clickElement(driver, pobj.defaultAddMoreBtn);
		}

		fc.utobj().sendKeys(driver, pobj.RuleSetRangeToFieldDefaultZerothRow, rngToFieldDefaultZeroRowAR);
		fc.utobj().sendKeys(driver, pobj.RuleAppliedValueForDefaultZeroRow, appliedValueForDefaultZeroRow);
		fc.utobj().sendKeys(driver, pobj.RuleAppliedValueForDefaultFirstRow, appliedValueForDefaultFirstRow);
		Thread.sleep(2000);

		int rowCountService = driver.findElements(By.xpath("//table[@id='table_1']/tbody")).size();
		if (rowCountService == 1) {
			fc.utobj().clickElement(driver, pobj.serviceAddMoreBtn);
		}

		fc.utobj().sendKeys(driver, pobj.RuleSetRangeToFieldServiceZeroRow, rngToFieldServiceZeroRow);
		fc.utobj().sendKeys(driver, pobj.RuleAppliedValueForServiceZeroRow, appliedValueForServiceZeroRow);
		fc.utobj().sendKeys(driver, pobj.RuleAppliedValueForServiceFirstRow, appliedValueForServiceFirstRow);
		fc.utobj().clickElement(driver, pobj.submitBtn);

		return ruleCSP;
	}

	@Test(groups = { "finance" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "TC_FC_Quantity_Category_Sales_Flat_024", testCaseDescription = "Add Agreement Rule Category SubCategory Quantity Sales Category Sales Flat Based")
	public void addAgreementRuleCategorySubCategoryQuantitySalesCategorySalesFlatBased() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("finance", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		String ruleCSF = fc.utobj().generateTestData(dataSet.get("agreementRuleCSFB"));
		String applyMinCapValue = dataSet.get("applyMinCapValue");
		String applyMaxCapValue = dataSet.get("applyMaxCapValue");
		String minCapValue = dataSet.get("minCapValue");
		String maxCapValue = dataSet.get("maxCapValue");
		String rngToFieldDefaultZeroRowAR = dataSet.get("rangeToFieldDefaultZeroRowAR");
		String appliedValueForDefaultZeroRow = dataSet.get("appliedValueForDefaultZeroRowAR");
		String appliedValueForDefaultFirstRow = dataSet.get("appliedValueForDefaultFirstRowAR");
		String rngToFieldServiceZeroRow = dataSet.get("rangeToFieldServiceZeroRowAR");
		String appliedValueForServiceZeroRow = dataSet.get("appliedValueForServiceZeroRowAR");
		String appliedValueForServiceFirstRow = dataSet.get("appliedValueForServiceFirstRowAR");

		try {
			driver = fc.loginpage().login(driver);
			AdminFinanceAgreementRulesPage pobj = new AdminFinanceAgreementRulesPage(driver);
			fc.utobj().printTestStep("Add Agreement Rule with Type Category Sales Based and Apply As A Flat");

			ruleCSF = addAgreementRuleCategorySubCategoryQuantitySalesCategorySalesFlat(driver, config, ruleCSF,
					applyMinCapValue, applyMaxCapValue, minCapValue, maxCapValue, rngToFieldDefaultZeroRowAR,
					appliedValueForDefaultZeroRow, appliedValueForDefaultFirstRow, rngToFieldServiceZeroRow,
					appliedValueForServiceZeroRow, appliedValueForServiceFirstRow);

			fc.utobj().printTestStep("Verify Add Agreement Rule with Type Category Sales Based and Apply As A Flat");
			boolean isTextPresent = fc.utobj().assertLinkText(driver, ruleCSF);
			if (isTextPresent == false) {
				fc.utobj().selectDropDownByValue(driver, pobj.selectViewPerPage, "500");

				{
					boolean isTextPresent1 = fc.utobj().assertLinkText(driver, ruleCSF);
					if (isTextPresent1 == false) {
						fc.utobj().throwsException(
								"Agreement Rule 'Category-Sub Category Quantity / Sales Based', Category Sales and Flat Based is Not added!");
					}
				}
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	public String addAgreementRuleCategorySubCategoryQuantitySalesCategorySalesFlat(WebDriver driver,
			Map<String, String> config, String ruleCSF, String applyMinCapValue, String applyMaxCapValue,
			String minCapValue, String maxCapValue, String rngToFieldDefaultZeroRowAR,
			String appliedValueForDefaultZeroRow, String appliedValueForDefaultFirstRow,
			String rngToFieldServiceZeroRow, String appliedValueForServiceZeroRow,
			String appliedValueForServiceFirstRow) throws Exception {

		fc.finance().finance_common().adminFinAgreementVersionsPage(driver);
		AdminFinanceAgreementRulesPage pobj = new AdminFinanceAgreementRulesPage(driver);

		fc.utobj().clickElement(driver, pobj.AgreementRulesBtn);
		fc.utobj().clickElement(driver, pobj.AddAgreementRule);
		fc.utobj().clickElement(driver, pobj.ARCategorySubCategoryQuantitySalesBased);
		fc.utobj().clickElement(driver, pobj.continueBtn);

		fc.utobj().clickElement(driver, pobj.categorySalesBased);
		fc.utobj().sendKeys(driver, pobj.AgreementRuleName, ruleCSF);

		if (applyMinCapValue.equalsIgnoreCase("Y")) {
			fc.utobj().clickElement(driver, pobj.CheckboxMinimumCap);
			fc.utobj().sendKeys(driver, pobj.MinimumCap, minCapValue);
		}

		if (applyMaxCapValue.equalsIgnoreCase("Y")) {
			fc.utobj().clickElement(driver, pobj.CheckBoxMaximumCap);
			fc.utobj().sendKeys(driver, pobj.MaximumCap, maxCapValue);
		}
		fc.utobj().sendKeys(driver, pobj.Description,
				"Agreement Rule 'Category-Sub Category Quantity / Sales Based', Category Sales and Flat Based");
		fc.utobj().clickElement(driver, pobj.RuleFlatRadioBtn);
		fc.utobj().clickElement(driver, pobj.categoryDropDown);
		WebElement element = pobj.categoryDropDown.findElement(By.xpath(".//div//div[@class='ms-search']//input"));
		WebElement element2 = pobj.categoryDropDown.findElement(By.xpath(".//input[@id='selectItemcategories']"));
		fc.utobj().clickElement(driver, element2);
		fc.utobj().sendKeys(driver, element, "Service");

		int rowCount = driver.findElements(By.xpath("//table[@id='table_-1']/tbody")).size();
		if (rowCount == 1) {
			fc.utobj().clickElement(driver, pobj.defaultAddMoreBtn);
		}
		fc.utobj().sendKeys(driver, pobj.RuleSetRangeToFieldDefaultZerothRow, rngToFieldDefaultZeroRowAR);
		fc.utobj().sendKeys(driver, pobj.RuleAppliedValueForDefaultZeroRow, appliedValueForDefaultZeroRow);
		fc.utobj().sendKeys(driver, pobj.RuleAppliedValueForDefaultFirstRow, appliedValueForDefaultFirstRow);

		int rowCountService = driver.findElements(By.xpath("//table[@id='table_1']/tbody")).size();
		if (rowCountService == 1) {
			fc.utobj().clickElement(driver, pobj.serviceAddMoreBtn);
		}
		fc.utobj().sendKeys(driver, pobj.RuleSetRangeToFieldServiceZeroRow, rngToFieldServiceZeroRow);
		fc.utobj().sendKeys(driver, pobj.RuleAppliedValueForServiceZeroRow, appliedValueForServiceZeroRow);
		fc.utobj().sendKeys(driver, pobj.RuleAppliedValueForServiceFirstRow, appliedValueForServiceFirstRow);
		fc.utobj().clickElement(driver, pobj.submitBtn);

		return ruleCSF;
	}

	@Test(groups = { "finance", "finaceDummay" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "TC_FC_Category_Quantity_Percent_025", testCaseDescription = "add Agreement Rule Category SubCategory Quantity Sales Category Quantity Percent Based")
	public void addAgreementRuleCategorySubCategoryQuantitySalesCategoryQuantityPercentBased() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("finance", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		String ruleCQP = dataSet.get("agreementRuleCQPB");
		ruleCQP = fc.utobj().generateTestData(ruleCQP);
		String applyMinCapValue = dataSet.get("applyMinCapValue");
		String applyMaxCapValue = dataSet.get("applyMaxCapValue");
		String minCapValue = dataSet.get("minCapValue");
		String maxCapValue = dataSet.get("maxCapValue");
		String rngToFieldDefaultZeroRowAR = dataSet.get("rangeToFieldDefaultZeroRowAR");
		String appliedValueForDefaultZeroRow = dataSet.get("appliedValueForDefaultZeroRowAR");
		String appliedValueForDefaultFirstRow = dataSet.get("appliedValueForDefaultFirstRowAR");
		String rngToFieldServiceZeroRow = dataSet.get("rangeToFieldServiceZeroRowAR");
		String appliedValueForServiceZeroRow = dataSet.get("appliedValueForServiceZeroRowAR");
		String appliedValueForServiceFirstRow = dataSet.get("appliedValueForServiceFirstRowAR");

		try {
			driver = fc.loginpage().login(driver);
			AdminFinanceAgreementRulesPage pobj = new AdminFinanceAgreementRulesPage(driver);
			fc.utobj().printTestStep("Add Agreement Rule with Type Category Quantity Based and Apply As A Percentage");

			ruleCQP = addAgreementRuleCategorySubCategoryQuantitySalesCategoryQuantityPercent(driver, config, ruleCQP,
					applyMinCapValue, applyMaxCapValue, minCapValue, maxCapValue, rngToFieldDefaultZeroRowAR,
					appliedValueForDefaultZeroRow, appliedValueForDefaultFirstRow, rngToFieldServiceZeroRow,
					appliedValueForServiceZeroRow, appliedValueForServiceFirstRow);

			fc.utobj().printTestStep(
					"Verify Add Agreement Rule with Type Category Quantity Based and Apply As A Percentage");
			boolean isTextPresent = fc.utobj().assertLinkText(driver, ruleCQP);
			if (isTextPresent == false) {
				fc.utobj().selectDropDownByValue(driver, pobj.selectViewPerPage, "500");

				{
					boolean isTextPresent1 = fc.utobj().assertLinkText(driver, ruleCQP);
					if (isTextPresent1 == false) {
						fc.utobj().throwsException(
								"Agreement Rule 'Category-Sub Category Quantity / Sales Based', Category Quantity and Percent Based is Not added!");
					}
				}
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	public String addAgreementRuleCategorySubCategoryQuantitySalesCategoryQuantityPercent(WebDriver driver,
			Map<String, String> config, String ruleCQP, String applyMinCapValue, String applyMaxCapValue,
			String minCapValue, String maxCapValue, String rngToFieldDefaultZeroRowAR,
			String appliedValueForDefaultZeroRow, String appliedValueForDefaultFirstRow,
			String rngToFieldServiceZeroRow, String appliedValueForServiceZeroRow,
			String appliedValueForServiceFirstRow) throws Exception {

		fc.finance().finance_common().adminFinAgreementVersionsPage(driver);
		AdminFinanceAgreementRulesPage pobj = new AdminFinanceAgreementRulesPage(driver);

		fc.utobj().clickElement(driver, pobj.AgreementRulesBtn);
		fc.utobj().clickElement(driver, pobj.AddAgreementRule);
		fc.utobj().clickElement(driver, pobj.ARCategorySubCategoryQuantitySalesBased);
		fc.utobj().clickElement(driver, pobj.continueBtn);

		fc.utobj().clickElement(driver, pobj.categoryQuantityBased);
		fc.utobj().sendKeys(driver, pobj.AgreementRuleName, ruleCQP);

		if (applyMinCapValue.equalsIgnoreCase("Y")) {
			fc.utobj().clickElement(driver, pobj.CheckboxMinimumCap);
			fc.utobj().sendKeys(driver, pobj.MinimumCap, minCapValue);
		}

		if (applyMaxCapValue.equalsIgnoreCase("Y")) {
			fc.utobj().clickElement(driver, pobj.CheckBoxMaximumCap);
			fc.utobj().sendKeys(driver, pobj.MaximumCap, maxCapValue);
		}

		fc.utobj().sendKeys(driver, pobj.Description,
				"Agreement Rule 'Category-Sub Category Quantity / Sales Based', Category Quantity and Percent Based");
		fc.utobj().clickElement(driver, pobj.RulePercentageRadioBtn);
		fc.utobj().clickElement(driver, pobj.categoryDropDown);
		WebElement element = pobj.categoryDropDown.findElement(By.xpath(".//div//div[@class='ms-search']//input"));
		WebElement element2 = pobj.categoryDropDown.findElement(By.xpath(".//input[@id='selectItemcategories']"));
		fc.utobj().clickElement(driver, element2);
		fc.utobj().sendKeys(driver, element, "Service");

		int rowCount = driver.findElements(By.xpath("//table[@id='table_-1']/tbody")).size();
		if (rowCount == 1) {
			fc.utobj().clickElement(driver, pobj.defaultAddMoreBtn);
		}
		fc.utobj().sendKeys(driver, pobj.RuleSetRangeToFieldDefaultZerothRow, rngToFieldDefaultZeroRowAR);
		fc.utobj().sendKeys(driver, pobj.RuleAppliedValueForDefaultZeroRow, appliedValueForDefaultZeroRow);
		fc.utobj().sendKeys(driver, pobj.RuleAppliedValueForDefaultFirstRow, appliedValueForDefaultFirstRow);
		int rowCountService = driver.findElements(By.xpath("//table[@id='table_1']/tbody")).size();
		if (rowCountService == 1) {
			fc.utobj().clickElement(driver, pobj.serviceAddMoreBtn);
		}
		fc.utobj().sendKeys(driver, pobj.RuleSetRangeToFieldServiceZeroRow, rngToFieldServiceZeroRow);
		fc.utobj().sendKeys(driver, pobj.RuleAppliedValueForServiceZeroRow, appliedValueForServiceZeroRow);
		fc.utobj().sendKeys(driver, pobj.RuleAppliedValueForServiceFirstRow, appliedValueForServiceFirstRow);
		fc.utobj().clickElement(driver, pobj.submitBtn);

		return ruleCQP;
	}

	@Test(groups = { "finance" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "TC_Category_Quantity_Flat_026", testCaseDescription = "Verify add Agreement Rule Category SubCategory Quantity Sales Category Quantity Flat Based")
	public void addAgreementRuleCategorySubCategoryQuantitySalesCategoryQuantityFlatBased() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("finance", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		String ruleCQF = dataSet.get("agreementRuleCQFB");

		ruleCQF = fc.utobj().generateTestData(ruleCQF);
		String applyMinCapValue = dataSet.get("applyMinCapValue");
		String applyMaxCapValue = dataSet.get("applyMaxCapValue");
		String minCapValue = dataSet.get("minCapValue");
		String maxCapValue = dataSet.get("maxCapValue");
		String rngToFieldDefaultZeroRowAR = dataSet.get("rangeToFieldDefaultZeroRowAR");
		String appliedValueForDefaultZeroRow = dataSet.get("appliedValueForDefaultZeroRowAR");
		String appliedValueForDefaultFirstRow = dataSet.get("appliedValueForDefaultFirstRowAR");
		String rngToFieldServiceZeroRow = dataSet.get("rangeToFieldServiceZeroRowAR");
		String appliedValueForServiceZeroRow = dataSet.get("appliedValueForServiceZeroRowAR");
		String appliedValueForServiceFirstRow = dataSet.get("appliedValueForServiceFirstRowAR");

		try {
			driver = fc.loginpage().login(driver);
			AdminFinanceAgreementRulesPage pobj = new AdminFinanceAgreementRulesPage(driver);
			fc.utobj().printTestStep("Add Agreement Rule with Type Category Quantity Based and Apply As A Flat");

			ruleCQF = addAgreementRuleCategorySubCategoryQuantitySalesCategoryQuantityFlat(driver, config, ruleCQF,
					applyMinCapValue, applyMaxCapValue, minCapValue, maxCapValue, rngToFieldDefaultZeroRowAR,
					appliedValueForDefaultZeroRow, appliedValueForDefaultFirstRow, rngToFieldServiceZeroRow,
					appliedValueForServiceZeroRow, appliedValueForServiceFirstRow);

			fc.utobj().printTestStep("Verify Add Agreement Rule with Type Category Quantity Based And Apply As A Flat");

			boolean isTextPresent = fc.utobj().assertLinkText(driver, ruleCQF);
			if (isTextPresent == false) {
				fc.utobj().selectDropDownByValue(driver, pobj.selectViewPerPage, "500");

				{
					boolean isTextPresent1 = fc.utobj().assertLinkText(driver, ruleCQF);
					if (isTextPresent1 == false) {
						fc.utobj().throwsException(
								"Agreement Rule 'Category-Sub Category Quantity / Sales Based', Category Quantity and Flat Based is Not added!");
					}
				}
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	public String addAgreementRuleCategorySubCategoryQuantitySalesCategoryQuantityFlat(WebDriver driver,
			Map<String, String> config, String ruleCQF, String applyMinCapValue, String applyMaxCapValue,
			String minCapValue, String maxCapValue, String rngToFieldDefaultZeroRowAR,
			String appliedValueForDefaultZeroRow, String appliedValueForDefaultFirstRow,
			String rngToFieldServiceZeroRow, String appliedValueForServiceZeroRow,
			String appliedValueForServiceFirstRow) throws Exception {

		fc.finance().finance_common().adminFinAgreementVersionsPage(driver);
		AdminFinanceAgreementRulesPage pobj = new AdminFinanceAgreementRulesPage(driver);

		fc.utobj().clickElement(driver, pobj.AgreementRulesBtn);
		fc.utobj().clickElement(driver, pobj.AddAgreementRule);
		fc.utobj().clickElement(driver, pobj.ARCategorySubCategoryQuantitySalesBased);
		fc.utobj().clickElement(driver, pobj.continueBtn);

		fc.utobj().clickElement(driver, pobj.categoryQuantityBased);
		fc.utobj().sendKeys(driver, pobj.AgreementRuleName, ruleCQF);

		if (applyMinCapValue.equalsIgnoreCase("Y")) {
			fc.utobj().clickElement(driver, pobj.CheckboxMinimumCap);
			fc.utobj().sendKeys(driver, pobj.MinimumCap, minCapValue);
		}

		if (applyMaxCapValue.equalsIgnoreCase("Y")) {
			fc.utobj().clickElement(driver, pobj.CheckBoxMaximumCap);
			fc.utobj().sendKeys(driver, pobj.MaximumCap, maxCapValue);
		}
		fc.utobj().sendKeys(driver, pobj.Description,
				"Agreement Rule 'Category-Sub Category Quantity / Sales Based', Category Quantity and Flat Based");
		fc.utobj().clickElement(driver, pobj.RuleFlatRadioBtn);
		fc.utobj().clickElement(driver, pobj.categoryDropDown);
		WebElement element = pobj.categoryDropDown.findElement(By.xpath(".//div//div[@class='ms-search']//input"));
		WebElement element2 = pobj.categoryDropDown.findElement(By.xpath(".//input[@id='selectItemcategories']"));
		fc.utobj().clickElement(driver, element2);
		fc.utobj().sendKeys(driver, element, "Service");

		int rowCount = driver.findElements(By.xpath("//table[@id='table_-1']/tbody")).size();
		if (rowCount == 1) {
			fc.utobj().clickElement(driver, pobj.defaultAddMoreBtn);
		}
		fc.utobj().sendKeys(driver, pobj.RuleSetRangeToFieldDefaultZerothRow, rngToFieldDefaultZeroRowAR);
		fc.utobj().sendKeys(driver, pobj.RuleAppliedValueForDefaultZeroRow, appliedValueForDefaultZeroRow);
		fc.utobj().sendKeys(driver, pobj.RuleAppliedValueForDefaultFirstRow, appliedValueForDefaultFirstRow);

		int rowCountService = driver.findElements(By.xpath("//table[@id='table_1']/tbody")).size();
		if (rowCountService == 1) {
			fc.utobj().clickElement(driver, pobj.serviceAddMoreBtn);
		}
		fc.utobj().sendKeys(driver, pobj.RuleSetRangeToFieldServiceZeroRow, rngToFieldServiceZeroRow);
		fc.utobj().sendKeys(driver, pobj.RuleAppliedValueForServiceZeroRow, appliedValueForServiceZeroRow);
		fc.utobj().sendKeys(driver, pobj.RuleAppliedValueForServiceFirstRow, appliedValueForServiceFirstRow);
		fc.utobj().clickElement(driver, pobj.submitBtn);

		return ruleCQF;
	}

	@Test(groups = { "finance" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "TC_FC_Quantity_Custom_Standard_027", testCaseDescription = "Verify add Agreement Rule Category SubCategory Quantity Custom Standard")
	public void addAgreementRuleCategorySubCategoryQuantityCustomStandard() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("finance", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		String ruleCustomStandard = dataSet.get("agreementRuleCSB");
		ruleCustomStandard = fc.utobj().generateTestData(ruleCustomStandard);
		String applyMinCapValue = dataSet.get("applyMinCapValue");
		String applyMaxCapValue = dataSet.get("applyMaxCapValue");
		String minCapValue = dataSet.get("minCapValue");
		String maxCapValue = dataSet.get("maxCapValue");

		try {
			driver = fc.loginpage().login(driver);
			
			AdminFinanceAgreementRulesPage pobj = new AdminFinanceAgreementRulesPage(driver);
			
			fc.utobj().printTestStep("Add Agreement Rule with Type Custom and Max / Min Cap is Standard");

			ruleCustomStandard = addAgreementRuleCustomStandard(driver, config, ruleCustomStandard, applyMinCapValue,
					applyMaxCapValue, minCapValue, maxCapValue);

			fc.utobj().printTestStep("Verify Add Agreement Rule with Type Custom and Max / Min Cap is Standard");
		
			boolean isTextPresent = fc.utobj().assertLinkText(driver, ruleCustomStandard);
			
			if (isTextPresent == false) {
				
				fc.utobj().selectDropDownByValue(driver, pobj.selectViewPerPage, "500");

				{
					boolean isTextPresent1 = fc.utobj().assertLinkText(driver, ruleCustomStandard);
					
					if (isTextPresent1 == false) {
						
						fc.utobj().throwsException(
								"Agreement Rule 'Category-Sub Category Quantity / Sales Based', Custom and Standard Based is Not added!");
					}
				}
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	public String addAgreementRuleCustomStandard(WebDriver driver, Map<String, String> config,
			String ruleCustomStandard, String applyMinCapValue, String applyMaxCapValue, String minCapValue,
			String maxCapValue) throws Exception {

		fc.finance().finance_common().adminFinAgreementVersionsPage(driver);

		AdminFinanceAgreementRulesPage pobj = new AdminFinanceAgreementRulesPage(driver);

		fc.utobj().clickElement(driver, pobj.AgreementRulesBtn);
		
		fc.utobj().clickElement(driver, pobj.AddAgreementRule);
		
		fc.utobj().clickElement(driver, pobj.ARCategorySubCategoryQuantitySalesBased);
		
		fc.utobj().clickElement(driver, pobj.continueBtn);

		fc.utobj().clickElement(driver, pobj.customRule);
		
		fc.utobj().sendKeys(driver, pobj.AgreementRuleName, ruleCustomStandard);

		fc.utobj().sendKeys(driver, pobj.Description,
				"Agreement Rule 'Category-Sub Category Quantity / Sales Based', Custom and Standard Based");

		fc.utobj().clickElement(driver, pobj.categoryDropDown);
		
		//WebElement element = pobj.categoryDropDown.findElement(By.xpath(".//div//div[@class='ms-search']//input"));
		
		//fc.utobj().sendKeys(driver, element, "Service");
		
		WebElement element2 = pobj.categoryDropDown.findElement(By.xpath(".//input[@id='selectItemcategories']"));
		
		fc.utobj().clickElement(driver, element2);

		if (applyMinCapValue.equalsIgnoreCase("Y")) {
			
			fc.utobj().clickElement(driver, pobj.CheckboxMinimumCap);
			
			fc.utobj().sendKeys(driver, pobj.MinimumCap, minCapValue);
		}

		if (applyMaxCapValue.equalsIgnoreCase("Y")) {
			
			fc.utobj().clickElement(driver, pobj.CheckBoxMaximumCap);
			
			fc.utobj().sendKeys(driver, pobj.MaximumCap, maxCapValue);
		}
		
		
		Select select1 = new Select(pobj.defaultCalType);
		
		select1.selectByValue("P");
		
		//fc.utobj().selectDropDownByValue(driver, pobj.defaultCalType, "P");
		
		fc.utobj().sendKeys(driver, pobj.appliedValueDefaultCategory, "10");

		Select select2 = new Select(pobj.serviceCalType);
		
		select2.selectByValue("P");
		
		//fc.utobj().selectDropDownByValue(driver, pobj.serviceCalType, "P");
		
		fc.utobj().sendKeys(driver, pobj.appliedValuePercent, "8");
		
		fc.utobj().clickElement(driver, pobj.submitBtn);

		return ruleCustomStandard;
	}

	@Test(groups = { "finance" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "TC_FC_Custom_Standard_SubCategory_028", testCaseDescription = "Verify add Agreement Rule Category SubCategory Quantity Custom Standard SubCategory")
	public void addAgreementRuleCategorySubCategoryQuantityCustomStandardSubCategory() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("finance", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		String ruleCustomStandardSCat = dataSet.get("agreementRuleCSSB");
		ruleCustomStandardSCat = fc.utobj().generateTestData(ruleCustomStandardSCat);

		String applyMinCapValue = dataSet.get("applyMinCapValue");
		String applyMaxCapValue = dataSet.get("applyMaxCapValue");
		String minCapValue = dataSet.get("minCapValue");
		String maxCapValue = dataSet.get("maxCapValue");

		try {
			driver = fc.loginpage().login(driver);
			AdminFinanceAgreementRulesPage pobj = new AdminFinanceAgreementRulesPage(driver);
			fc.utobj().printTestStep("Add Agreement Rule Custom Standard Sub category");

			ruleCustomStandardSCat = addAgreementRuleCustomStandardSubCategory(driver, config, ruleCustomStandardSCat,
					applyMinCapValue, applyMaxCapValue, minCapValue, maxCapValue);

			fc.utobj().printTestStep("Verify Add Agreement Rule Custom Standard Sub category");
			
			boolean isTextPresent = fc.utobj().assertLinkText(driver, ruleCustomStandardSCat);
			
			if (isTextPresent == false) {
				fc.utobj().selectDropDownByValue(driver, pobj.selectViewPerPage, "500");

				{
					boolean isTextPresent1 = fc.utobj().assertLinkText(driver, ruleCustomStandardSCat);
					
					if (isTextPresent1 == false) {
						
						fc.utobj().throwsException(
								"Agreement Rule 'Category-Sub Category Quantity / Sales Based', Custom and Standard Based is Not added!");
					}
				}
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	public String addAgreementRuleCustomStandardSubCategory(WebDriver driver, Map<String, String> config,
			String ruleCustomStandardSCat, String applyMinCapValue, String applyMaxCapValue, String minCapValue,
			String maxCapValue) throws Exception {

		fc.finance().finance_common().adminFinAgreementVersionsPage(driver);

		AdminFinanceAgreementRulesPage pobj = new AdminFinanceAgreementRulesPage(driver);

		fc.utobj().clickElement(driver, pobj.AgreementRulesBtn);
		
		fc.utobj().clickElement(driver, pobj.AddAgreementRule);
		
		fc.utobj().clickElement(driver, pobj.ARCategorySubCategoryQuantitySalesBased);
		
		fc.utobj().clickElement(driver, pobj.continueBtn);

		fc.utobj().clickElement(driver, pobj.customRule);
		
		fc.utobj().sendKeys(driver, pobj.AgreementRuleName, ruleCustomStandardSCat);

		fc.utobj().sendKeys(driver, pobj.Description,
				"Agreement Rule 'Category-Sub Category Quantity / Sales Based', Custom, Standard and SubCategory Based");

		fc.utobj().clickElement(driver, pobj.categoryDropDown);
		
		//WebElement element = pobj.categoryDropDown.findElement(By.xpath(".//div//div[@class='ms-search']//input"));
		
		//fc.utobj().sendKeys(driver, element, "Service");

		WebElement element2 = pobj.categoryDropDown.findElement(By.xpath(".//input[@id='selectItemcategories']"));
		
		fc.utobj().clickElement(driver, element2);

		fc.utobj().clickElement(driver, pobj.clickMaxMin);
		
		//fc.utobj().clickElement(driver, pobj.checkboxIncludeSubCategory);

		if (applyMinCapValue.equalsIgnoreCase("Y")) {
			
			fc.utobj().clickElement(driver, pobj.CheckboxMinimumCap);
			
			fc.utobj().sendKeys(driver, pobj.MinimumCap, minCapValue);
		}

		if (applyMaxCapValue.equalsIgnoreCase("Y")) {
			
			fc.utobj().clickElement(driver, pobj.CheckBoxMaximumCap);
			
			fc.utobj().sendKeys(driver, pobj.MaximumCap, maxCapValue);
		}

		fc.utobj().selectDropDownByValue(driver, pobj.defaultCalType, "P");
		
		fc.utobj().sendKeys(driver, pobj.appliedValueDefaultCategory, "10");

		fc.utobj().selectDropDownByValue(driver, pobj.serviceCalType, "P");
		
		fc.utobj().sendKeys(driver, pobj.appliedValuePercent, "8");
		
		fc.utobj().clickElement(driver, pobj.submitBtn);

		return ruleCustomStandardSCat;
	}

	@Test(groups = { "finance" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "TC_FC_Quantity_Custom_StoreAge_029", testCaseDescription = "Verify add Agreement Rule Category SubCategory Quantity Custom Store Age")
	public void addAgreementRuleCategorySubCategoryQuantityCustomStoreAge() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("finance", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		String ruleCustomStoreAge = dataSet.get("agreementRuleCStoreAgeB");
		ruleCustomStoreAge = fc.utobj().generateTestData(ruleCustomStoreAge);

		String applyMinCapZeroValue = dataSet.get("applyMinCapZeroValue");
		String applyMaxCapZeroValue = dataSet.get("applyMaxCapZeroValue");
		String minCapZeroValue = dataSet.get("minCapZeroValue");
		String maxCapZeroValue = dataSet.get("maxCapZeroValue");

		String applyMinCapFirstValue = dataSet.get("applyMinCapFirstValue");
		String applyMaxCapFirstValue = dataSet.get("applyMaxCapFirstValue");
		String minCapFirstValue = dataSet.get("minCapFirstValue");
		String maxCapFirstValue = dataSet.get("maxCapFirstValue");

		try {
			driver = fc.loginpage().login(driver);
			AdminFinanceAgreementRulesPage pobj = new AdminFinanceAgreementRulesPage(driver);
			fc.utobj().printTestStep("Add Agreement Rule with Type Custom and Max / Min Cap is Store Age Based");

			ruleCustomStoreAge = addAgreementRuleCustomStoreAge(driver, config, ruleCustomStoreAge,
					applyMinCapZeroValue, applyMaxCapZeroValue, minCapZeroValue, maxCapZeroValue, applyMinCapFirstValue,
					applyMaxCapFirstValue, minCapFirstValue, maxCapFirstValue);

			fc.utobj().printTestStep("Verify Add Agreement Rule with Type Custom and Max / Min Cap is Store Age Based");
			boolean isTextPresent = fc.utobj().assertLinkText(driver, ruleCustomStoreAge);
			if (isTextPresent == false) {
				fc.utobj().selectDropDownByValue(driver, pobj.selectViewPerPage, "500");

				{
					boolean isTextPresent1 = fc.utobj().assertLinkText(driver, ruleCustomStoreAge);
					if (isTextPresent1 == false) {
						fc.utobj().throwsException(
								"Agreement Rule 'Category-Sub Category Quantity / Sales Based', Custom and Store Age Based is Not added!");
					}
				}
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	public String addAgreementRuleCustomStoreAge(WebDriver driver, Map<String, String> config,
			String ruleCustomStoreAge, String applyMinCapZeroValue, String applyMaxCapZeroValue, String minCapZeroValue,
			String maxCapZeroValue, String applyMinCapFirstValue, String applyMaxCapFirstValue, String minCapFirstValue,
			String maxCapFirstValue) throws Exception {

		fc.finance().finance_common().adminFinAgreementVersionsPage(driver);

		AdminFinanceAgreementRulesPage pobj = new AdminFinanceAgreementRulesPage(driver);

		fc.utobj().clickElement(driver, pobj.AgreementRulesBtn);
		fc.utobj().clickElement(driver, pobj.AddAgreementRule);
		fc.utobj().clickElement(driver, pobj.ARCategorySubCategoryQuantitySalesBased);
		fc.utobj().clickElement(driver, pobj.continueBtn);

		fc.utobj().clickElement(driver, pobj.customRule);
		fc.utobj().sendKeys(driver, pobj.AgreementRuleName, ruleCustomStoreAge);

		fc.utobj().sendKeys(driver, pobj.Description,
				"Agreement Rule 'Category-Sub Category Quantity / Sales Based', Custom and Store Age Based");

		fc.utobj().clickElement(driver, pobj.categoryDropDown);
		WebElement element = pobj.categoryDropDown.findElement(By.xpath(".//div//div[@class='ms-search']//input"));
		fc.utobj().sendKeys(driver, element, "Service");
		WebElement element2 = pobj.categoryDropDown.findElement(By.xpath(".//input[@id='selectItemcategories']"));
		fc.utobj().clickElement(driver, element2);

		fc.utobj().clickElement(driver, pobj.clickMaxMin);

		fc.utobj().clickElement(driver, pobj.selectStoreAgeBased);

		int rowCountService = driver.findElements(By.xpath("//table[@id='table1']/tbody")).size();
		if (rowCountService == 1) {
			fc.utobj().clickElement(driver, pobj.addMoreMaxMinCap);
		}
		fc.utobj().sendKeys(driver, pobj.storeAgeToMonth, "24");

		if (applyMinCapZeroValue.equalsIgnoreCase("Y")) {
			fc.utobj().clickElement(driver, pobj.CheckboxMinCapZeroCustom);
			fc.utobj().sendKeys(driver, pobj.minCapValueZeroCustom, minCapZeroValue);
		}

		if (applyMaxCapZeroValue.equalsIgnoreCase("Y")) {
			fc.utobj().clickElement(driver, pobj.CheckBoxMaxCapZeroCustom);
			fc.utobj().sendKeys(driver, pobj.MaxCapValueZeroCustom, maxCapZeroValue);
		}

		if (applyMinCapFirstValue.equalsIgnoreCase("Y")) {
			fc.utobj().clickElement(driver, pobj.CheckboxMinCapFirstCustom);
			fc.utobj().sendKeys(driver, pobj.minCapValueFirstCustom, minCapFirstValue);
		}

		if (applyMaxCapFirstValue.equalsIgnoreCase("Y")) {
			fc.utobj().clickElement(driver, pobj.CheckBoxMaxCapFirstCustom);
			fc.utobj().sendKeys(driver, pobj.MaxCapValueFirstCustom, maxCapFirstValue);
		}

		fc.utobj().selectDropDownByValue(driver, pobj.defaultCalType, "P");
		fc.utobj().sendKeys(driver, pobj.appliedValueDefaultCategory, "10");

		fc.utobj().selectDropDownByValue(driver, pobj.serviceCalType, "P");
		fc.utobj().sendKeys(driver, pobj.appliedValuePercent, "8");
		fc.utobj().clickElement(driver, pobj.submitBtn);

		return ruleCustomStoreAge;
	}

	@Test(groups = { "finance" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "TC_FC_Custom_StoreAge_SubCategories_030", testCaseDescription = "Verify add Agreement Rule Category SubCategory Quantity Custom Store Age SubCat")
	public void addAgreementRuleCategorySubCategoryQuantityCustomStoreAgeSubCat() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("finance", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		String ruleCustomStoreAgeSC = dataSet.get("agreementRuleCStoreAgeSubCatB");
		ruleCustomStoreAgeSC = fc.utobj().generateTestData(ruleCustomStoreAgeSC);

		String applyMinCapZeroValue = dataSet.get("applyMinCapZeroValue");
		String applyMaxCapZeroValue = dataSet.get("applyMaxCapZeroValue");
		String minCapZeroValue = dataSet.get("minCapZeroValue");
		String maxCapZeroValue = dataSet.get("maxCapZeroValue");

		String applyMinCapFirstValue = dataSet.get("applyMinCapFirstValue");
		String applyMaxCapFirstValue = dataSet.get("applyMaxCapFirstValue");
		String minCapFirstValue = dataSet.get("minCapFirstValue");
		String maxCapFirstValue = dataSet.get("maxCapFirstValue");

		try {
			driver = fc.loginpage().login(driver);
			AdminFinanceAgreementRulesPage pobj = new AdminFinanceAgreementRulesPage(driver);
			fc.utobj().printTestStep("Add Agreement Rule Custom Store Age SubCategory");

			ruleCustomStoreAgeSC = addAgreementRuleCustomStoreAgeSubCat(driver, config, ruleCustomStoreAgeSC,
					applyMinCapZeroValue, applyMaxCapZeroValue, minCapZeroValue, maxCapZeroValue, applyMinCapFirstValue,
					applyMaxCapFirstValue, minCapFirstValue, maxCapFirstValue);
			fc.utobj().printTestStep("Verify Add Agreement Rule Custom Store Age SubCategory");

			boolean isTextPresent = fc.utobj().assertLinkText(driver, ruleCustomStoreAgeSC);
			if (isTextPresent == false) {
				fc.utobj().selectDropDownByValue(driver, pobj.selectViewPerPage, "500");

				{
					boolean isTextPresent1 = fc.utobj().assertLinkText(driver, ruleCustomStoreAgeSC);
					if (isTextPresent1 == false) {
						fc.utobj().throwsException(
								"Agreement Rule 'Category-Sub Category Quantity / Sales Based', Custom, Store Age and SubCategory Based is Not added!");
					}
				}
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	public String addAgreementRuleCustomStoreAgeSubCat(WebDriver driver, Map<String, String> config,
			String ruleCustomStoreAgeSC, String applyMinCapZeroValue, String applyMaxCapZeroValue,
			String minCapZeroValue, String maxCapZeroValue, String applyMinCapFirstValue, String applyMaxCapFirstValue,
			String minCapFirstValue, String maxCapFirstValue) throws Exception {

		fc.finance().finance_common().adminFinAgreementVersionsPage(driver);

		AdminFinanceAgreementRulesPage pobj = new AdminFinanceAgreementRulesPage(driver);

		fc.utobj().clickElement(driver, pobj.AgreementRulesBtn);
		fc.utobj().clickElement(driver, pobj.AddAgreementRule);
		fc.utobj().clickElement(driver, pobj.ARCategorySubCategoryQuantitySalesBased);
		fc.utobj().clickElement(driver, pobj.continueBtn);

		fc.utobj().clickElement(driver, pobj.customRule);
		fc.utobj().sendKeys(driver, pobj.AgreementRuleName, ruleCustomStoreAgeSC);

		fc.utobj().sendKeys(driver, pobj.Description,
				"Agreement Rule 'Category-Sub Category Quantity / Sales Based', Custom, Store Age and SubCategory Based");

		fc.utobj().clickElement(driver, pobj.categoryDropDown);
		WebElement element = pobj.categoryDropDown.findElement(By.xpath(".//div//div[@class='ms-search']//input"));
		fc.utobj().sendKeys(driver, element, "Service");
		WebElement element2 = pobj.categoryDropDown.findElement(By.xpath(".//input[@id='selectItemcategories']"));
		fc.utobj().clickElement(driver, element2);

		fc.utobj().clickElement(driver, pobj.clickMaxMin);

		fc.utobj().clickElement(driver, pobj.checkboxIncludeSubCategory);

		fc.utobj().clickElement(driver, pobj.selectStoreAgeBased);

		int rowCountService = driver.findElements(By.xpath("//table[@id='table1']/tbody")).size();
		if (rowCountService == 1) {
			fc.utobj().clickElement(driver, pobj.addMoreMaxMinCap);
		}
		fc.utobj().sendKeys(driver, pobj.storeAgeToMonth, "24");

		if (applyMinCapZeroValue.equalsIgnoreCase("Y")) {
			fc.utobj().clickElement(driver, pobj.CheckboxMinCapZeroCustom);
			fc.utobj().sendKeys(driver, pobj.minCapValueZeroCustom, minCapZeroValue);
		}

		if (applyMaxCapZeroValue.equalsIgnoreCase("Y")) {
			fc.utobj().clickElement(driver, pobj.CheckBoxMaxCapZeroCustom);
			fc.utobj().sendKeys(driver, pobj.MaxCapValueZeroCustom, maxCapZeroValue);
		}

		if (applyMinCapFirstValue.equalsIgnoreCase("Y")) {
			fc.utobj().clickElement(driver, pobj.CheckboxMinCapFirstCustom);
			fc.utobj().sendKeys(driver, pobj.minCapValueFirstCustom, minCapFirstValue);
		}

		if (applyMaxCapFirstValue.equalsIgnoreCase("Y")) {
			fc.utobj().clickElement(driver, pobj.CheckBoxMaxCapFirstCustom);
			fc.utobj().sendKeys(driver, pobj.MaxCapValueFirstCustom, maxCapFirstValue);
		}

		fc.utobj().selectDropDownByValue(driver, pobj.defaultCalType, "P");
		fc.utobj().sendKeys(driver, pobj.appliedValueDefaultCategory, "10");

		fc.utobj().selectDropDownByValue(driver, pobj.serviceCalType, "P");
		fc.utobj().sendKeys(driver, pobj.appliedValuePercent, "8");
		fc.utobj().clickElement(driver, pobj.submitBtn);

		return ruleCustomStoreAgeSC;
	}

	@Test(groups = { "finance" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "TC_FC_Percent_Previous_Year_YTD_031", testCaseDescription = "Verify add Agreement Rule Previous Years Sales Percent Previous Year YTD Based")
	public void addAgreementRulePreviousYearsSalesPercentPreviousYearYTDBased() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("finance", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		String ruleNamePYSPYTD = dataSet.get("agreementRulePYSPPYYTDB");
		ruleNamePYSPYTD = fc.utobj().generateTestData(ruleNamePYSPYTD);
		String applyMinCapValue = dataSet.get("applyMinCapValue");
		String applyMaxCapValue = dataSet.get("applyMaxCapValue");
		String minCapValue = dataSet.get("minCapValue");
		String maxCapValue = dataSet.get("maxCapValue");
		String rngToFieldZeroRowAR = dataSet.get("rangeToFieldZeroRowAR");
		String apldValueForZeroRowAR = dataSet.get("appliedValueForZeroRowAR");
		String apldValueForFirstRowAR = dataSet.get("appliedValueForFirstRowAR");

		try {
			driver = fc.loginpage().login(driver);
			AdminFinanceAgreementRulesPage pobj = new AdminFinanceAgreementRulesPage(driver);
			fc.utobj().printTestStep(
					"Add Agreement Rule with Type Previous Year's Sales Based and Accounting Basis is Previous Year YTD ");

			ruleNamePYSPYTD = addAgreementRulePreviousYearsSalesPercentPreviousYearYTD(driver, config, ruleNamePYSPYTD,
					applyMinCapValue, applyMaxCapValue, minCapValue, maxCapValue, rngToFieldZeroRowAR,
					apldValueForZeroRowAR, apldValueForFirstRowAR);
			fc.utobj().printTestStep(
					"Verify Add Agreement Rule with Type Previous Year's Sales Based and Accounting Basis is Previous Year YTD ");
			boolean isTextPresent = fc.utobj().assertLinkText(driver, ruleNamePYSPYTD);
			if (isTextPresent == false) {
				fc.utobj().selectDropDownByValue(driver, pobj.selectViewPerPage, "500");
				{
					boolean isTextPresent1 = fc.utobj().assertLinkText(driver, ruleNamePYSPYTD);
					if (isTextPresent1 == false) {
						fc.utobj().throwsException(
								"Agreement Rule Previous Years Sales, Percent and Previous Year YTD Based is Not added!");
					}
				}
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	public String addAgreementRulePreviousYearsSalesPercentPreviousYearYTD(WebDriver driver, Map<String, String> config,
			String ruleNamePYSPYTD, String applyMinCapValue, String applyMaxCapValue, String minCapValue,
			String maxCapValue, String rngToFieldZeroRowAR, String apldValueForZeroRowAR, String apldValueForFirstRowAR)
			throws Exception {

		fc.finance().finance_common().adminFinAgreementVersionsPage(driver);
		AdminFinanceAgreementRulesPage pobj = new AdminFinanceAgreementRulesPage(driver);

		fc.utobj().clickElement(driver, pobj.AgreementRulesBtn);
		fc.utobj().clickElement(driver, pobj.AddAgreementRule);
		fc.utobj().clickElement(driver, pobj.ARPreviousYearsSalesBased);
		fc.utobj().clickElement(driver, pobj.continueBtn);

		fc.utobj().sendKeys(driver, pobj.AgreementRuleName, ruleNamePYSPYTD);

		if (applyMinCapValue.equalsIgnoreCase("Y")) {
			fc.utobj().clickElement(driver, pobj.CheckboxMinimumCap);
			fc.utobj().sendKeys(driver, pobj.MinimumCap, minCapValue);
		}

		if (applyMaxCapValue.equalsIgnoreCase("Y")) {
			fc.utobj().clickElement(driver, pobj.CheckBoxMaximumCap);
			fc.utobj().sendKeys(driver, pobj.MaximumCap, maxCapValue);
		}

		fc.utobj().sendKeys(driver, pobj.Description,
				"Agreement Rule Previous Years Sales, Percent and Previous Year YTD Based");
		fc.utobj().clickElement(driver, pobj.RulePercentageRadioBtn);
		fc.utobj().clickElement(driver, pobj.YTDAccountingBasis);

		int rowCountService = driver.findElements(By.xpath("//table[@id='table1']/tbody")).size();
		if (rowCountService == 1) {
			fc.utobj().clickElement(driver, pobj.RuleAddMoreBtn);
		}
		fc.utobj().sendKeys(driver, pobj.RuleSetRangeToFieldZeroRow, rngToFieldZeroRowAR);
		fc.utobj().sendKeys(driver, pobj.RuleAppliedValueForZeroRow, apldValueForZeroRowAR);
		fc.utobj().sendKeys(driver, pobj.RuleAppliedValueForFirstRow, apldValueForFirstRowAR);
		fc.utobj().clickElement(driver, pobj.submitBtn);

		return ruleNamePYSPYTD;
	}

	@Test(groups = { "finance" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "TC_FC_Sales_Percent_Previous_Year_Cumulative_032", testCaseDescription = "Verify add Agreement Rule Previous Years Sales Percent Previous Year Cumulative Based")
	public void addAgreementRulePreviousYearsSalesPercentPreviousYearCumulativeBased() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("finance", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		String ruleNamePYSPC = dataSet.get("agreementRulePYSPPYCB");
		ruleNamePYSPC = fc.utobj().generateTestData(ruleNamePYSPC);
		String applyMinCapValue = dataSet.get("applyMinCapValue");
		String applyMaxCapValue = dataSet.get("applyMaxCapValue");
		String minCapValue = dataSet.get("minCapValue");
		String maxCapValue = dataSet.get("maxCapValue");
		String rngToFieldZeroRowAR = dataSet.get("rangeToFieldZeroRowAR");
		String apldValueForZeroRowAR = dataSet.get("appliedValueForZeroRowAR");
		String apldValueForFirstRowAR = dataSet.get("appliedValueForFirstRowAR");

		try {
			driver = fc.loginpage().login(driver);
			AdminFinanceAgreementRulesPage pobj = new AdminFinanceAgreementRulesPage(driver);
			fc.utobj().printTestStep(
					"Add Agreement Rule with Type Previous Year's Sales Based and Accounting Basis is Previous Year Cumulative");

			ruleNamePYSPC = addAgreementRulePreviousYearsSalesPercentPreviousYearCumulative(driver, config,
					ruleNamePYSPC, applyMinCapValue, applyMaxCapValue, minCapValue, maxCapValue, rngToFieldZeroRowAR,
					apldValueForZeroRowAR, apldValueForFirstRowAR);
			fc.utobj().printTestStep(
					"Verify Add Agreement Rule with Type Previous Year's Sales Based and Accounting Basis is Previous Year Cumulative");
			boolean isTextPresent = fc.utobj().assertLinkText(driver, ruleNamePYSPC);
			if (isTextPresent == false) {
				fc.utobj().selectDropDownByValue(driver, pobj.selectViewPerPage, "500");

				{
					boolean isTextPresent1 = fc.utobj().assertLinkText(driver, ruleNamePYSPC);
					if (isTextPresent1 == false) {
						fc.utobj().throwsException(
								"Agreement Rule Previous Years Sales, Percent and Previous Year Cumulative Based is Not added!");
					}
				}
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	public String addAgreementRulePreviousYearsSalesPercentPreviousYearCumulative(WebDriver driver,
			Map<String, String> config, String ruleNamePYSPC, String applyMinCapValue, String applyMaxCapValue,
			String minCapValue, String maxCapValue, String rngToFieldZeroRowAR, String apldValueForZeroRowAR,
			String apldValueForFirstRowAR) throws Exception {

		fc.finance().finance_common().adminFinAgreementVersionsPage(driver);
		AdminFinanceAgreementRulesPage pobj = new AdminFinanceAgreementRulesPage(driver);

		fc.utobj().clickElement(driver, pobj.AgreementRulesBtn);
		fc.utobj().clickElement(driver, pobj.AddAgreementRule);
		fc.utobj().clickElement(driver, pobj.ARPreviousYearsSalesBased);
		fc.utobj().clickElement(driver, pobj.continueBtn);

		fc.utobj().sendKeys(driver, pobj.AgreementRuleName, ruleNamePYSPC);

		if (applyMinCapValue.equalsIgnoreCase("Y")) {
			fc.utobj().clickElement(driver, pobj.CheckboxMinimumCap);
			fc.utobj().sendKeys(driver, pobj.MinimumCap, minCapValue);
		}

		if (applyMaxCapValue.equalsIgnoreCase("Y")) {
			fc.utobj().clickElement(driver, pobj.CheckBoxMaximumCap);
			fc.utobj().sendKeys(driver, pobj.MaximumCap, maxCapValue);
		}

		fc.utobj().sendKeys(driver, pobj.Description,
				"Agreement Rule Previous Years Sales, Percent and Previous Year Cumulative Based");

		fc.utobj().clickElement(driver, pobj.RulePercentageRadioBtn);
		fc.utobj().clickElement(driver, pobj.CumulativeAccountingBasis);

		int rowCountService = driver.findElements(By.xpath("//table[@id='table1']/tbody")).size();
		if (rowCountService == 1) {
			fc.utobj().clickElement(driver, pobj.RuleAddMoreBtn);
		}
		fc.utobj().sendKeys(driver, pobj.RuleSetRangeToFieldZeroRow, rngToFieldZeroRowAR);
		fc.utobj().sendKeys(driver, pobj.RuleAppliedValueForZeroRow, apldValueForZeroRowAR);
		fc.utobj().sendKeys(driver, pobj.RuleAppliedValueForFirstRow, apldValueForFirstRowAR);
		fc.utobj().clickElement(driver, pobj.submitBtn);

		return ruleNamePYSPC;
	}

	@Test(groups = { "finance" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "TC_FC_AgreementRule_Percent_Previous_Year_Periodic_Sales_033", testCaseDescription = "Verify add Agreement Rule Previous Years Sales Percent Previous Year Periodic Sales Based")
	public void addAgreementRulePreviousYearsSalesPercentPreviousYearPeriodicSalesBased() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("finance", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		String ruleNamePYSPPYPS = dataSet.get("agreementRulePYSPPYPSB");
		ruleNamePYSPPYPS = fc.utobj().generateTestData(ruleNamePYSPPYPS);
		String applyMinCapValue = dataSet.get("applyMinCapValue");
		String applyMaxCapValue = dataSet.get("applyMaxCapValue");
		String minCapValue = dataSet.get("minCapValue");
		String maxCapValue = dataSet.get("maxCapValue");
		String rngToFieldZeroRowAR = dataSet.get("rangeToFieldZeroRowAR");
		String apldValueForZeroRowAR = dataSet.get("appliedValueForZeroRowAR");
		String apldValueForFirstRowAR = dataSet.get("appliedValueForFirstRowAR");

		try {
			driver = fc.loginpage().login(driver);
			AdminFinanceAgreementRulesPage pobj = new AdminFinanceAgreementRulesPage(driver);
			fc.utobj().printTestStep(
					"Add Agreement Rule with Type Previous Year's Sales Based and Accounting Basis is Previous Year Periodic Sales");

			ruleNamePYSPPYPS = addAgreementRulePreviousYearsSalesPercentPreviousYearPeriodicSales(driver, config,
					ruleNamePYSPPYPS, applyMinCapValue, applyMaxCapValue, minCapValue, maxCapValue, rngToFieldZeroRowAR,
					apldValueForZeroRowAR, apldValueForFirstRowAR);

			fc.utobj().printTestStep(
					"Verify Add Agreement Rule with Type Previous Year's Sales Based and Accounting Basis is Previous Year Periodic Sales");
			boolean isTextPresent = fc.utobj().assertLinkText(driver, ruleNamePYSPPYPS);
			if (isTextPresent == false) {
				fc.utobj().selectDropDownByValue(driver, pobj.selectViewPerPage, "500");

				{
					boolean isTextPresent1 = fc.utobj().assertLinkText(driver, ruleNamePYSPPYPS);
					if (isTextPresent1 == false) {
						fc.utobj().throwsException(
								"Agreement Rule Sales Amount, Percent and Previous Year Periodic Sales Based is Not added!");
					}
				}
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	public String addAgreementRulePreviousYearsSalesPercentPreviousYearPeriodicSales(WebDriver driver,
			Map<String, String> config, String ruleNamePYSPPYPS, String applyMinCapValue, String applyMaxCapValue,
			String minCapValue, String maxCapValue, String rngToFieldZeroRowAR, String apldValueForZeroRowAR,
			String apldValueForFirstRowAR) throws Exception {

		fc.finance().finance_common().adminFinAgreementVersionsPage(driver);
		AdminFinanceAgreementRulesPage pobj = new AdminFinanceAgreementRulesPage(driver);

		fc.utobj().clickElement(driver, pobj.AgreementRulesBtn);
		fc.utobj().clickElement(driver, pobj.AddAgreementRule);
		fc.utobj().clickElement(driver, pobj.ARSalesAmountBased);
		fc.utobj().clickElement(driver, pobj.continueBtn);

		fc.utobj().sendKeys(driver, pobj.AgreementRuleName, ruleNamePYSPPYPS);

		if (applyMinCapValue.equalsIgnoreCase("Y")) {
			fc.utobj().clickElement(driver, pobj.CheckboxMinimumCap);
			fc.utobj().sendKeys(driver, pobj.MinimumCap, minCapValue);
		}

		if (applyMaxCapValue.equalsIgnoreCase("Y")) {
			fc.utobj().clickElement(driver, pobj.CheckBoxMaximumCap);
			fc.utobj().sendKeys(driver, pobj.MaximumCap, maxCapValue);
		}
		fc.utobj().sendKeys(driver, pobj.Description,
				"Agreement Rule Sales Amount, Percent and Previous Year Periodic Sales Based");
		fc.utobj().clickElement(driver, pobj.RulePercentageRadioBtn);
		fc.utobj().clickElement(driver, pobj.PeriodicSalesAccountingBasis);

		int rowCount = driver.findElements(By.xpath("//table[@id='table1']/tbody")).size();
		if (rowCount == 1) {
			fc.utobj().clickElement(driver, pobj.RuleAddMoreBtn);
		}

		fc.utobj().sendKeys(driver, pobj.RuleSetRangeToFieldZeroRow, rngToFieldZeroRowAR);
		fc.utobj().sendKeys(driver, pobj.RuleAppliedValueForZeroRow, apldValueForZeroRowAR);
		fc.utobj().sendKeys(driver, pobj.RuleAppliedValueForFirstRow, apldValueForFirstRowAR);
		fc.utobj().clickElement(driver, pobj.submitBtn);

		return ruleNamePYSPPYPS;
	}

	@Test(groups = { "finance", "test123" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "TC_FC_AgreementRule_Percent_Previous_Year_YTD_RRSD_034", testCaseDescription = "Verify add Agreement Rule Previous Years Sales Percent Previous Year YTD from RRSD Based")
	public void addAgreementRulePreviousYearsSalesPercentPreviousYearYTDfromRRSDBased() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("finance", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		String ruleNamePYSPPYYTDfromRRSD = dataSet.get("agreementRulePYSPPYYTDfromRRSDB");
		ruleNamePYSPPYYTDfromRRSD = fc.utobj().generateTestData(ruleNamePYSPPYYTDfromRRSD);
		String applyMinCapValue = dataSet.get("applyMinCapValue");
		String applyMaxCapValue = dataSet.get("applyMaxCapValue");
		String minCapValue = dataSet.get("minCapValue");
		String maxCapValue = dataSet.get("maxCapValue");
		String rngToFieldZeroRowAR = dataSet.get("rangeToFieldZeroRowAR");
		String apldValueForZeroRowAR = dataSet.get("appliedValueForZeroRowAR");
		String apldValueForFirstRowAR = dataSet.get("appliedValueForFirstRowAR");

		try {
			driver = fc.loginpage().login(driver);
			AdminFinanceAgreementRulesPage pobj = new AdminFinanceAgreementRulesPage(driver);
			fc.utobj().printTestStep(
					"Add Agreement Rule with Type Previous Year's Sales Based and Accounting Basis is Previous Year YTD from Royalty");

			ruleNamePYSPPYYTDfromRRSD = addAgreementRulePreviousYearsSalesPercentPreviousYearYTDfromRRSD(driver, config,
					ruleNamePYSPPYYTDfromRRSD, applyMinCapValue, applyMaxCapValue, minCapValue, maxCapValue,
					rngToFieldZeroRowAR, apldValueForZeroRowAR, apldValueForFirstRowAR);
			fc.utobj().printTestStep(
					"Verify Add Agreement Rule with Type Previous Year's Sales Based and Accounting Basis is Previous Year YTD from Royalty");
			boolean isTextPresent = fc.utobj().assertLinkText(driver, ruleNamePYSPPYYTDfromRRSD);
			if (isTextPresent == false) {
				fc.utobj().selectDropDownByValue(driver, pobj.selectViewPerPage, "500");

				{
					boolean isTextPresent1 = fc.utobj().assertLinkText(driver, ruleNamePYSPPYYTDfromRRSD);
					if (isTextPresent1 == false) {
						fc.utobj().throwsException(
								"Agreement Rule Previous Years Sales, Percent and Previous Year YTD from Royalty Reporting Start Date Based is Not added!");
					}
				}
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	public String addAgreementRulePreviousYearsSalesPercentPreviousYearYTDfromRRSD(WebDriver driver,
			Map<String, String> config, String ruleNamePYSPPYYTDfromRRSD, String applyMinCapValue,
			String applyMaxCapValue, String minCapValue, String maxCapValue, String rngToFieldZeroRowAR,
			String apldValueForZeroRowAR, String apldValueForFirstRowAR) throws Exception {

		fc.finance().finance_common().adminFinAgreementVersionsPage(driver);
		AdminFinanceAgreementRulesPage pobj = new AdminFinanceAgreementRulesPage(driver);

		fc.utobj().clickElement(driver, pobj.AgreementRulesBtn);
		fc.utobj().clickElement(driver, pobj.AddAgreementRule);
		fc.utobj().clickElement(driver, pobj.ARSalesAmountBased);
		fc.utobj().clickElement(driver, pobj.continueBtn);

		fc.utobj().sendKeys(driver, pobj.AgreementRuleName, ruleNamePYSPPYYTDfromRRSD);

		if (applyMinCapValue.equalsIgnoreCase("Y")) {
			fc.utobj().clickElement(driver, pobj.CheckboxMinimumCap);
			fc.utobj().sendKeys(driver, pobj.MinimumCap, minCapValue);
		}

		if (applyMaxCapValue.equalsIgnoreCase("Y")) {
			fc.utobj().clickElement(driver, pobj.CheckBoxMaximumCap);
			fc.utobj().sendKeys(driver, pobj.MaximumCap, maxCapValue);
		}
		fc.utobj().sendKeys(driver, pobj.Description,
				"Agreement Rule Previous Years Sales, Percent and Previous Year YTD from Royalty Reporting Start Date Based");
		fc.utobj().clickElement(driver, pobj.RulePercentageRadioBtn);
		fc.utobj().clickElement(driver, pobj.YTDfromRoyaltyReportingStartDateAccountingBasis);

		int rowCount = driver.findElements(By.xpath("//table[@id='table1']/tbody")).size();
		if (rowCount == 1) {
			fc.utobj().clickElement(driver, pobj.RuleAddMoreBtn);
		}
		fc.utobj().sendKeys(driver, pobj.RuleSetRangeToFieldZeroRow, rngToFieldZeroRowAR);
		fc.utobj().sendKeys(driver, pobj.RuleAppliedValueForZeroRow, apldValueForZeroRowAR);
		fc.utobj().sendKeys(driver, pobj.RuleAppliedValueForFirstRow, apldValueForFirstRowAR);
		fc.utobj().clickElement(driver, pobj.submitBtn);

		return ruleNamePYSPPYYTDfromRRSD;
	}

	@Test(groups = { "finance" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "TC_FC_Sales_Flat_Previous_Year_YTD_035", testCaseDescription = "Verify add Agreement Rule Previous Years Sales Flat Previous Year YTD Based")
	public void addAgreementRulePreviousYearsSalesFlatPreviousYearYTDBased() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("finance", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		String ruleNamePYSFPYYTD = fc.utobj().generateTestData(dataSet.get("agreementRulePYSFPYYTDB"));
		String applyMinCapValue = dataSet.get("applyMinCapValue");
		String applyMaxCapValue = dataSet.get("applyMaxCapValue");
		String minCapValue = dataSet.get("minCapValue");
		String maxCapValue = dataSet.get("maxCapValue");
		String rngToFieldZeroRowAR = dataSet.get("rangeToFieldZeroRowAR");
		String apldValueForZeroRowAR = dataSet.get("appliedValueForZeroRowAR");
		String apldValueForFirstRowAR = dataSet.get("appliedValueForFirstRowAR");

		try {
			driver = fc.loginpage().login(driver);
			fc.utobj().printTestStep(
					"Add Agreement Rule with Type Previous Year's Sales Based Apply As A Flat and Accounting Basis is Previous Year YTD");

			ruleNamePYSFPYYTD = addAgreementRulePreviousYearsSalesFlatPreviousYearYTD(driver, config, ruleNamePYSFPYYTD,
					applyMinCapValue, applyMaxCapValue, minCapValue, maxCapValue, rngToFieldZeroRowAR,
					apldValueForZeroRowAR, apldValueForFirstRowAR);

			fc.utobj().printTestStep(
					"Verify Add Agreement Rule with Type Previous Year's Sales Based Apply As A Flat and Accounting Basis is Previous Year YTD");

			boolean recordNotPresent = new AdminFinanceAgreementVersionsPageTest().filterRecordByPaging(driver,
					new AdminFinanceAgreementVersionsPage(driver), ruleNamePYSFPYYTD);
			if (recordNotPresent == false) {
				fc.utobj().throwsException(
						"Agreement Rule Previous Years Sales, Flat and Previous Year YTD Based is Not added!");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	public String addAgreementRulePreviousYearsSalesFlatPreviousYearYTD(WebDriver driver, Map<String, String> config,
			String ruleNamePYSFPYYTD, String applyMinCapValue, String applyMaxCapValue, String minCapValue,
			String maxCapValue, String rngToFieldZeroRowAR, String apldValueForZeroRowAR, String apldValueForFirstRowAR)
			throws Exception {

		fc.finance().finance_common().adminFinAgreementVersionsPage(driver);
		AdminFinanceAgreementRulesPage pobj = new AdminFinanceAgreementRulesPage(driver);

		fc.utobj().clickElement(driver, pobj.AgreementRulesBtn);
		fc.utobj().clickElement(driver, pobj.AddAgreementRule);
		fc.utobj().clickElement(driver, pobj.ARPreviousYearsSalesBased);
		fc.utobj().clickElement(driver, pobj.continueBtn);

		fc.utobj().sendKeys(driver, pobj.AgreementRuleName, ruleNamePYSFPYYTD);

		if (applyMinCapValue.equalsIgnoreCase("Y")) {
			fc.utobj().clickElement(driver, pobj.CheckboxMinimumCap);
			fc.utobj().sendKeys(driver, pobj.MinimumCap, minCapValue);
		}

		if (applyMaxCapValue.equalsIgnoreCase("Y")) {
			fc.utobj().clickElement(driver, pobj.CheckBoxMaximumCap);
			fc.utobj().sendKeys(driver, pobj.MaximumCap, maxCapValue);
		}

		fc.utobj().sendKeys(driver, pobj.Description,
				"Agreement Rule Previous Years Sales, Flat and Previous Year YTD Based");
		fc.utobj().clickElement(driver, pobj.RuleFlatRadioBtn);
		fc.utobj().clickElement(driver, pobj.YTDAccountingBasis);

		int rowCountService = driver.findElements(By.xpath("//table[@id='table1']/tbody")).size();
		if (rowCountService == 1) {
			fc.utobj().clickElement(driver, pobj.RuleAddMoreBtn);
		}
		fc.utobj().sendKeys(driver, pobj.RuleSetRangeToFieldZeroRow, rngToFieldZeroRowAR);
		fc.utobj().sendKeys(driver, pobj.RuleAppliedValueForZeroRow, apldValueForZeroRowAR);
		fc.utobj().sendKeys(driver, pobj.RuleAppliedValueForFirstRow, apldValueForFirstRowAR);
		fc.utobj().clickElement(driver, pobj.submitBtn);

		return ruleNamePYSFPYYTD;
	}

	@Test(groups = { "finance" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "TC_FC_Flat_Previous_Year_Cumulative_036", testCaseDescription = "Verify add Agreement Rule Previous Years Sales Flat Previous Year Cumulative Based")
	public void addAgreementRulePreviousYearsSalesFlatPreviousYearCumulativeBased() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("finance", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		String ruleNamePYSFPYC = dataSet.get("agreementRulePYSFPYCB");
		ruleNamePYSFPYC = fc.utobj().generateTestData(ruleNamePYSFPYC);
		String applyMinCapValue = dataSet.get("applyMinCapValue");
		String applyMaxCapValue = dataSet.get("applyMaxCapValue");
		String minCapValue = dataSet.get("minCapValue");
		String maxCapValue = dataSet.get("maxCapValue");
		String rngToFieldZeroRowAR = dataSet.get("rangeToFieldZeroRowAR");
		String apldValueForZeroRowAR = dataSet.get("appliedValueForZeroRowAR");
		String apldValueForFirstRowAR = dataSet.get("appliedValueForFirstRowAR");

		try {
			driver = fc.loginpage().login(driver);

			AdminFinanceAgreementRulesPage pobj = new AdminFinanceAgreementRulesPage(driver);
			fc.utobj().printTestStep(
					"Add Agreement Rule with Type Previous Year's Sales Based Apply As A Flat and Accounting Basis is Previous Year Cumulative");

			ruleNamePYSFPYC = addAgreementRulePreviousYearsSalesFlatPreviousYearCumulative(driver, config,
					ruleNamePYSFPYC, applyMinCapValue, applyMaxCapValue, minCapValue, maxCapValue, rngToFieldZeroRowAR,
					apldValueForZeroRowAR, apldValueForFirstRowAR);
			fc.utobj().printTestStep(
					"Add Agreement Rule with Type Previous Year's Sales Based Apply As A Flat and Accounting Basis is Previous Year Cumulative");

			boolean isTextPresent = fc.utobj().assertLinkText(driver, ruleNamePYSFPYC);
			if (isTextPresent == false) {
				fc.utobj().selectDropDownByValue(driver, pobj.selectViewPerPage, "500");
				{
					boolean isTextPresent1 = fc.utobj().assertLinkText(driver, ruleNamePYSFPYC);
					if (isTextPresent1 == false) {
						fc.utobj().throwsException(
								"Agreement Rule Previous Years Sales, Flat and Previous Year Cumulative Based is Not added!");
					}
				}
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	public String addAgreementRulePreviousYearsSalesFlatPreviousYearCumulative(WebDriver driver,
			Map<String, String> config, String ruleNamePYSFPYC, String applyMinCapValue, String applyMaxCapValue,
			String minCapValue, String maxCapValue, String rngToFieldZeroRowAR, String apldValueForZeroRowAR,
			String apldValueForFirstRowAR) throws Exception {

		fc.finance().finance_common().adminFinAgreementVersionsPage(driver);
		AdminFinanceAgreementRulesPage pobj = new AdminFinanceAgreementRulesPage(driver);

		fc.utobj().clickElement(driver, pobj.AgreementRulesBtn);
		fc.utobj().clickElement(driver, pobj.AddAgreementRule);
		fc.utobj().clickElement(driver, pobj.ARPreviousYearsSalesBased);
		fc.utobj().clickElement(driver, pobj.continueBtn);

		fc.utobj().sendKeys(driver, pobj.AgreementRuleName, ruleNamePYSFPYC);

		if (applyMinCapValue.equalsIgnoreCase("Y")) {
			fc.utobj().clickElement(driver, pobj.CheckboxMinimumCap);
			fc.utobj().sendKeys(driver, pobj.MinimumCap, minCapValue);
		}

		if (applyMaxCapValue.equalsIgnoreCase("Y")) {
			fc.utobj().clickElement(driver, pobj.CheckBoxMaximumCap);
			fc.utobj().sendKeys(driver, pobj.MaximumCap, maxCapValue);
		}

		fc.utobj().sendKeys(driver, pobj.Description,
				"Agreement Rule Previous Years Sales, Flat and Previous Year Cumulative Based");

		fc.utobj().clickElement(driver, pobj.RuleFlatRadioBtn);
		fc.utobj().clickElement(driver, pobj.CumulativeAccountingBasis);

		int rowCountService = driver.findElements(By.xpath("//table[@id='table1']/tbody")).size();
		if (rowCountService == 1) {
			fc.utobj().clickElement(driver, pobj.RuleAddMoreBtn);
		}
		fc.utobj().sendKeys(driver, pobj.RuleSetRangeToFieldZeroRow, rngToFieldZeroRowAR);
		fc.utobj().sendKeys(driver, pobj.RuleAppliedValueForZeroRow, apldValueForZeroRowAR);
		fc.utobj().sendKeys(driver, pobj.RuleAppliedValueForFirstRow, apldValueForFirstRowAR);
		fc.utobj().clickElement(driver, pobj.submitBtn);

		return ruleNamePYSFPYC;
	}

	@Test(groups = { "finance" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "TC_FC_Sales_Flat_Previous_Year_Periodic_Sales_037", testCaseDescription = "Verify add Agreement Rule Previous Years Sales Flat Previous Year Periodic Sales Based")
	public void addAgreementRulePreviousYearsSalesFlatPreviousYearPeriodicSalesBased() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("finance", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		String ruleNamePYSFPYPS = dataSet.get("agreementRulePYSFPYPSB");
		ruleNamePYSFPYPS = fc.utobj().generateTestData(ruleNamePYSFPYPS);
		String applyMinCapValue = dataSet.get("applyMinCapValue");
		String applyMaxCapValue = dataSet.get("applyMaxCapValue");
		String minCapValue = dataSet.get("minCapValue");
		String maxCapValue = dataSet.get("maxCapValue");
		String rngToFieldZeroRowAR = dataSet.get("rangeToFieldZeroRowAR");
		String apldValueForZeroRowAR = dataSet.get("appliedValueForZeroRowAR");
		String apldValueForFirstRowAR = dataSet.get("appliedValueForFirstRowAR");

		try {
			driver = fc.loginpage().login(driver);
			AdminFinanceAgreementRulesPage pobj = new AdminFinanceAgreementRulesPage(driver);
			fc.utobj().printTestStep(
					"Add Agreement Rule with Type Previous Year's Sales Based Apply As A Flat and Accounting Basis is Previous Year Periodic Sales");

			ruleNamePYSFPYPS = addAgreementRulePreviousYearsSalesFlatPreviousYearPeriodicSales(driver, config,
					ruleNamePYSFPYPS, applyMinCapValue, applyMaxCapValue, minCapValue, maxCapValue, rngToFieldZeroRowAR,
					apldValueForZeroRowAR, apldValueForFirstRowAR);
			fc.utobj().printTestStep("Add Agreement Rule - Periodic Sales");
			boolean isTextPresent = fc.utobj().assertLinkText(driver, ruleNamePYSFPYPS);
			if (isTextPresent == false) {
				fc.utobj().selectDropDownByValue(driver, pobj.selectViewPerPage, "500");
				{
					boolean isTextPresent1 = fc.utobj().assertLinkText(driver, ruleNamePYSFPYPS);
					if (isTextPresent1 == false) {
						fc.utobj().throwsException(
								"Agreement Rule Sales Amount, Flat and Previous Year Periodic Sales Based is Not added!");
					}
				}
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	public String addAgreementRulePreviousYearsSalesFlatPreviousYearPeriodicSales(WebDriver driver,
			Map<String, String> config, String ruleNamePYSFPYPS, String applyMinCapValue, String applyMaxCapValue,
			String minCapValue, String maxCapValue, String rngToFieldZeroRowAR, String apldValueForZeroRowAR,
			String apldValueForFirstRowAR) throws Exception {

		fc.finance().finance_common().adminFinAgreementVersionsPage(driver);
		AdminFinanceAgreementRulesPage pobj = new AdminFinanceAgreementRulesPage(driver);

		fc.utobj().clickElement(driver, pobj.AgreementRulesBtn);
		fc.utobj().clickElement(driver, pobj.AddAgreementRule);
		fc.utobj().clickElement(driver, pobj.ARSalesAmountBased);
		fc.utobj().clickElement(driver, pobj.continueBtn);

		fc.utobj().sendKeys(driver, pobj.AgreementRuleName, ruleNamePYSFPYPS);

		if (applyMinCapValue.equalsIgnoreCase("Y")) {
			fc.utobj().clickElement(driver, pobj.CheckboxMinimumCap);
			fc.utobj().sendKeys(driver, pobj.MinimumCap, minCapValue);
		}

		if (applyMaxCapValue.equalsIgnoreCase("Y")) {
			fc.utobj().clickElement(driver, pobj.CheckBoxMaximumCap);
			fc.utobj().sendKeys(driver, pobj.MaximumCap, maxCapValue);
		}
		fc.utobj().sendKeys(driver, pobj.Description,
				"Agreement Rule Sales Amount, Flat and Previous Year Periodic Sales Based");
		fc.utobj().clickElement(driver, pobj.RuleFlatRadioBtn);
		fc.utobj().clickElement(driver, pobj.PeriodicSalesAccountingBasis);

		int rowCount = driver.findElements(By.xpath("//table[@id='table1']/tbody")).size();
		if (rowCount == 1) {
			fc.utobj().clickElement(driver, pobj.RuleAddMoreBtn);
		}

		fc.utobj().sendKeys(driver, pobj.RuleSetRangeToFieldZeroRow, rngToFieldZeroRowAR);
		fc.utobj().sendKeys(driver, pobj.RuleAppliedValueForZeroRow, apldValueForZeroRowAR);
		fc.utobj().sendKeys(driver, pobj.RuleAppliedValueForFirstRow, apldValueForFirstRowAR);
		fc.utobj().clickElement(driver, pobj.submitBtn);

		return ruleNamePYSFPYPS;
	}

	@Test(groups = { "finance" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "TC_FC_Sales_Flat_Previous_Year_YTD_RRSD_038", testCaseDescription = "Verify add Agreement Rule Previous Years Sales Flat Previous Year YTD from RRSD Based")
	public void addAgreementRulePreviousYearsSalesFlatPreviousYearYTDfromRRSDBased() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("finance", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		String ruleNamePYSFPYYTDfromRRSD = dataSet.get("agreementRulePYSFPYYTDfromRRSDB");
		ruleNamePYSFPYYTDfromRRSD = fc.utobj().generateTestData(ruleNamePYSFPYYTDfromRRSD);
		String applyMinCapValue = dataSet.get("applyMinCapValue");
		String applyMaxCapValue = dataSet.get("applyMaxCapValue");
		String minCapValue = dataSet.get("minCapValue");
		String maxCapValue = dataSet.get("maxCapValue");
		String rngToFieldZeroRowAR = dataSet.get("rangeToFieldZeroRowAR");
		String apldValueForZeroRowAR = dataSet.get("appliedValueForZeroRowAR");
		String apldValueForFirstRowAR = dataSet.get("appliedValueForFirstRowAR");

		try {
			driver = fc.loginpage().login(driver);
			AdminFinanceAgreementRulesPage pobj = new AdminFinanceAgreementRulesPage(driver);
			fc.utobj().printTestStep(
					"Add Agreement Rule with Type Previous Year's Sales Based Apply As A Flat and Accounting Basis is Previous Year YTD from Royalty Reporting Start Date ");

			ruleNamePYSFPYYTDfromRRSD = addAgreementRulePreviousYearsSalesFlatPreviousYearYTDfromRRSD(driver, config,
					ruleNamePYSFPYYTDfromRRSD, applyMinCapValue, applyMaxCapValue, minCapValue, maxCapValue,
					rngToFieldZeroRowAR, apldValueForZeroRowAR, apldValueForFirstRowAR);

			fc.utobj().printTestStep(
					"Verify Add Agreement Rule with Type Previous Year's Sales Based Apply As A Flat and Accounting Basis is Previous Year YTD from Royalty Reporting Start Date ");
			boolean isTextPresent = fc.utobj().assertLinkText(driver, ruleNamePYSFPYYTDfromRRSD);
			if (isTextPresent == false) {
				fc.utobj().selectDropDownByValue(driver, pobj.selectViewPerPage, "500");
				{
					boolean isTextPresent1 = fc.utobj().assertLinkText(driver, ruleNamePYSFPYYTDfromRRSD);
					if (isTextPresent1 == false) {
						fc.utobj().throwsException(
								"Agreement Rule Previous Years Sales, Flat and Previous Year YTD from Royalty Reporting Start Date Based is Not added!");
					}
				}
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	public String addAgreementRulePreviousYearsSalesFlatPreviousYearYTDfromRRSD(WebDriver driver,
			Map<String, String> config, String ruleNamePYSFPYYTDfromRRSD, String applyMinCapValue,
			String applyMaxCapValue, String minCapValue, String maxCapValue, String rngToFieldZeroRowAR,
			String apldValueForZeroRowAR, String apldValueForFirstRowAR) throws Exception {

		fc.finance().finance_common().adminFinAgreementVersionsPage(driver);
		AdminFinanceAgreementRulesPage pobj = new AdminFinanceAgreementRulesPage(driver);

		fc.utobj().clickElement(driver, pobj.AgreementRulesBtn);
		fc.utobj().clickElement(driver, pobj.AddAgreementRule);
		fc.utobj().clickElement(driver, pobj.ARSalesAmountBased);
		fc.utobj().clickElement(driver, pobj.continueBtn);

		fc.utobj().sendKeys(driver, pobj.AgreementRuleName, ruleNamePYSFPYYTDfromRRSD);

		if (applyMinCapValue.equalsIgnoreCase("Y")) {
			fc.utobj().clickElement(driver, pobj.CheckboxMinimumCap);
			fc.utobj().sendKeys(driver, pobj.MinimumCap, minCapValue);
		}

		if (applyMaxCapValue.equalsIgnoreCase("Y")) {
			fc.utobj().clickElement(driver, pobj.CheckBoxMaximumCap);
			fc.utobj().sendKeys(driver, pobj.MaximumCap, maxCapValue);
		}
		fc.utobj().sendKeys(driver, pobj.Description,
				"Agreement Rule Previous Years Sales, Flat and Previous Year YTD from Royalty Reporting Start Date Based");
		fc.utobj().clickElement(driver, pobj.RuleFlatRadioBtn);
		fc.utobj().clickElement(driver, pobj.YTDfromRoyaltyReportingStartDateAccountingBasis);

		int rowCount = driver.findElements(By.xpath("//table[@id='table1']/tbody")).size();
		if (rowCount == 1) {
			fc.utobj().clickElement(driver, pobj.RuleAddMoreBtn);
		}
		fc.utobj().sendKeys(driver, pobj.RuleSetRangeToFieldZeroRow, rngToFieldZeroRowAR);
		fc.utobj().sendKeys(driver, pobj.RuleAppliedValueForZeroRow, apldValueForZeroRowAR);
		fc.utobj().sendKeys(driver, pobj.RuleAppliedValueForFirstRow, apldValueForFirstRowAR);
		fc.utobj().clickElement(driver, pobj.submitBtn);

		return ruleNamePYSFPYYTDfromRRSD;
	}

	@Test(groups = { "finance" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "TC_FC_Agreement_Rule_Modify_057", testCaseDescription = "Verify Modify Agreement Rule")
	public void modifyAgreementRule() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("finance", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		String ruleNameSAPYTD = dataSet.get("agreementRuleName");
		ruleNameSAPYTD = fc.utobj().generateTestData(ruleNameSAPYTD);
		String applyMinCapValue = dataSet.get("applyMinCapValue");
		String minCapValue = dataSet.get("minCapValue");
		String applyMaxCapValue = dataSet.get("applyMaxCapValue");
		String maxCapValue = dataSet.get("maxCapValue");
		String rngToFieldZeroRowAR = dataSet.get("rangeToFieldZeroRow");
		String apldValueForZeroRowAR = dataSet.get("appliedValueForZeroRow");
		String apldValueForFirstRowAR = dataSet.get("appliedValueForFirstRow");

		String modifiedAgreementName = dataSet.get("modifyName");
		modifiedAgreementName = fc.utobj().generateTestData(modifiedAgreementName);
		String modifyrangeToFieldZeroRow = dataSet.get("modifyrangeToFieldZeroRow");
		String modifyappliedValueForZeroRow = dataSet.get("modifyappliedValueForZeroRow");
		String modifyappliedValueForFirstRow = dataSet.get("modifyappliedValueForFirstRow");

		try {
			driver = fc.loginpage().login(driver);
			AdminFinanceAgreementRulesPage pobj = new AdminFinanceAgreementRulesPage(driver);

			fc.utobj().printTestStep("Add New Agreement Rule");

			AdminFinanceAgreementRulesPageTest addNewAgreementRule = new AdminFinanceAgreementRulesPageTest();
			String agreementRuleName = addNewAgreementRule.addAgreementRuleSalesAmountPercentYTD(driver, config,
					ruleNameSAPYTD, applyMinCapValue, applyMaxCapValue, minCapValue, maxCapValue, rngToFieldZeroRowAR,
					apldValueForZeroRowAR, apldValueForFirstRowAR);

			fc.utobj().printTestStep("Modify Agreement Rule");
			boolean isTextPresent2 = fc.utobj().assertLinkText(driver, agreementRuleName);
			if (isTextPresent2 == false) {
				fc.utobj().selectDropDownByValue(driver, pobj.viewPerPageSelect, "500");
			}
			fc.utobj().actionImgOption(driver, agreementRuleName, "Modify");

			fc.utobj().sendKeys(driver, pobj.AgreementRuleName, modifiedAgreementName);
			fc.utobj().sendKeys(driver, pobj.RuleSetRangeToFieldZeroRow, modifyrangeToFieldZeroRow);
			fc.utobj().sendKeys(driver, pobj.RuleAppliedValueForZeroRow, modifyappliedValueForZeroRow);
			fc.utobj().sendKeys(driver, pobj.RuleAppliedValueForFirstRow, modifyappliedValueForFirstRow);
			fc.utobj().clickElement(driver, pobj.submitBtn);

			boolean isTextPresent3 = fc.utobj().assertLinkText(driver, agreementRuleName);
			if (isTextPresent3 == false) {
				fc.utobj().selectDropDownByValue(driver, pobj.viewPerPageSelect, "500");
			}

			fc.utobj().printTestStep("Verify The Modify Agreement Rule");
			boolean isTextPresent = fc.utobj().assertLinkText(driver, modifiedAgreementName);
			if (isTextPresent == false) {
				fc.utobj().selectDropDownByValue(driver, pobj.selectViewPerPage, "500");
				{
					boolean isTextPresent1 = fc.utobj().assertLinkText(driver, modifiedAgreementName);
					if (isTextPresent1 == false) {
						fc.utobj().throwsException("Agreement Rule is Not Modified!");
					}
				}
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "finance" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "TC_FC_Agreement_Rule_Delete_058", testCaseDescription = "Verify delete Agreement Rule")
	public void deleteAgreementRule() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("finance", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		String ruleNameSAPYTD = dataSet.get("agreementRuleName");
		ruleNameSAPYTD = fc.utobj().generateTestData(ruleNameSAPYTD);
		String applyMinCapValue = dataSet.get("applyMinCapValue");
		String minCapValue = dataSet.get("minCapValue");
		String applyMaxCapValue = dataSet.get("applyMaxCapValue");
		String maxCapValue = dataSet.get("maxCapValue");
		String rngToFieldZeroRowAR = dataSet.get("rangeToFieldZeroRow");
		String apldValueForZeroRowAR = dataSet.get("appliedValueForZeroRow");
		String apldValueForFirstRowAR = dataSet.get("appliedValueForFirstRow");

		try {
			driver = fc.loginpage().login(driver);
			AdminFinanceAgreementRulesPage pobj = new AdminFinanceAgreementRulesPage(driver);
			fc.utobj().printTestStep("Add New Agreement Rule");

			AdminFinanceAgreementRulesPageTest addNewAgreementRule = new AdminFinanceAgreementRulesPageTest();
			String agreementRuleName = addNewAgreementRule.addAgreementRuleSalesAmountPercentYTD(driver, config,
					ruleNameSAPYTD, applyMinCapValue, applyMaxCapValue, minCapValue, maxCapValue, rngToFieldZeroRowAR,
					apldValueForZeroRowAR, apldValueForFirstRowAR);

			fc.utobj().printTestStep("Delete Agreement Rule");
			boolean isTextPresent2 = fc.utobj().assertLinkText(driver, agreementRuleName);
			if (isTextPresent2 == false) {
				fc.utobj().selectDropDownByValue(driver, pobj.viewPerPageSelect, "500");
			}
			fc.utobj().actionImgOption(driver, agreementRuleName, "Delete");
			fc.utobj().acceptAlertBox(driver);

			fc.utobj().printTestStep("Verify The Deleted Agreement Rule");
			boolean isTextPresent = fc.utobj().assertLinkText(driver, agreementRuleName);
			if (isTextPresent == false) {
				fc.utobj().selectDropDownByValue(driver, pobj.selectViewPerPage, "500");
				{
					boolean isTextPresent1 = fc.utobj().assertLinkText(driver, agreementRuleName);
					if (isTextPresent1 == true) {
						fc.utobj().throwsException("Agreement Rule is Not Delete!");
					}
				}
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	public String agreementRuleCustomRuleFormulaBased(WebDriver driver, Map<String, String> config,
			String ruleCustomFormula, String applyMinCapValue, String applyMaxCapValue, String minCapValue,
			String maxCapValue, String ruleFormulaName) throws Exception {

		fc.finance().finance_common().adminFinAgreementVersionsPage(driver);
		AdminFinanceAgreementRulesPage pobj = new AdminFinanceAgreementRulesPage(driver);
		fc.utobj().clickElement(driver, pobj.AgreementRulesBtn);
		fc.utobj().clickElement(driver, pobj.AddAgreementRule);
		fc.utobj().clickElement(driver, pobj.ARCategorySubCategoryQuantitySalesBased);
		fc.utobj().clickElement(driver, pobj.continueBtn);
		fc.utobj().clickElement(driver, pobj.customRule);
		fc.utobj().sendKeys(driver, pobj.AgreementRuleName, ruleCustomFormula);
		fc.utobj().sendKeys(driver, pobj.Description, "Agreement Rule 'Custom Rule Formula' Based");
		fc.utobj().clickElement(driver, pobj.categoryDropDown);
		WebElement element = pobj.categoryDropDown.findElement(By.xpath(".//div//div[@class='ms-search']//input"));
		fc.utobj().sendKeys(driver, element, "Service");
		WebElement element2 = pobj.categoryDropDown.findElement(By.xpath(".//input[@id='selectItemcategories']"));
		fc.utobj().clickElement(driver, element2);
		fc.utobj().clickElement(driver, pobj.clickMaxMin);
		fc.utobj().clickElement(driver, pobj.checkboxIncludeSubCategory);

		if (applyMinCapValue.equalsIgnoreCase("Y")) {
			fc.utobj().clickElement(driver, pobj.CheckboxMinimumCap);
			fc.utobj().sendKeys(driver, pobj.MinimumCap, minCapValue);
		}

		if (applyMaxCapValue.equalsIgnoreCase("Y")) {
			fc.utobj().clickElement(driver, pobj.CheckBoxMaximumCap);
			fc.utobj().sendKeys(driver, pobj.MaximumCap, maxCapValue);
		}

		fc.utobj().selectDropDown(driver, pobj.defaultCalType, "Rule Formula");
		fc.utobj().selectDropDown(driver, pobj.selectCat1RuleFormula, ruleFormulaName);
		fc.utobj().selectDropDown(driver, pobj.serviceCalType, "Rule Formula");
		fc.utobj().selectDropDown(driver, pobj.selectCat2RuleFormula, ruleFormulaName);

		/*
		 * fc.utobj().selectDropDown(driver, pobj.serviceSubCat1CalType,
		 * "Rule Formula"); fc.utobj().selectDropDown(driver,
		 * pobj.selectCat3RuleFormula, ruleFormulaName);
		 * fc.utobj().selectDropDown(driver, pobj.serviceSubCat2CalType,
		 * "Rule Formula"); fc.utobj().selectDropDown(driver,
		 * pobj.selectCat4RuleFormula, ruleFormulaName);
		 */
		fc.utobj().clickElement(driver, pobj.submitBtn);

		return ruleCustomFormula;
	}
}
