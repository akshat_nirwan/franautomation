package com.builds.test.fin;

import java.io.IOException;
import java.util.Map;
import javax.xml.parsers.ParserConfigurationException;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;
import org.xml.sax.SAXException;
import com.builds.test.admin.AdminFranchiseLocationAddFranchiseLocationPageTest;
import com.builds.uimaps.fin.AdminFinanceAgreementVersionsPage;
import com.builds.utilities.FranconnectUtil;
import com.builds.utilities.TestCase;

public class AdminFinanceAgreementVersionsPageTest {

	FranconnectUtil fc = new FranconnectUtil();

	@Test(groups = {"finance"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "TC_FC_Agreement_Versions_039", testCaseDescription = "Verify Add Agreement Version")
	public void addAgreementVersion() throws IOException, Exception, ParserConfigurationException, SAXException {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("finance", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		String agrmntVrsnName = fc.utobj().generateTestData(dataSet.get("agreementVersionName"));
				
		String royaltyPrcntg = dataSet.get("royaltyPercentage");
		String advPercentage = dataSet.get("advertisementPercentage");
		String royaltyAreaFranchiseValue = dataSet.get("RoyaltyAreaFranchise");
		String advAreaFranchiseValue = dataSet.get("AdvAreaFranchise");
		
		try {
			driver = fc.loginpage().login(driver);

			AdminFinanceAgreementVersionsPage pobj = new AdminFinanceAgreementVersionsPage(driver);
			fc.utobj().printTestStep("Navigate To Admin > Finance > Agreement Versions");
			fc.finance().finance_common().adminFinAgreementVersionsPage(driver);

			fc.utobj().printTestStep("Add Agreement Version");
			
			agrmntVrsnName = addAgreement(driver, config, agrmntVrsnName, royaltyPrcntg, advPercentage, royaltyAreaFranchiseValue, advAreaFranchiseValue );
	
			fc.utobj().printTestStep("Verify Added Agreement Version");

			boolean recordNotPresent = filterRecordByPaging(driver, pobj, agrmntVrsnName);
			if (recordNotPresent == false) {
				fc.utobj().throwsException("Agreement Version is Not added!");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "finance" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-04", testCaseId = "TC_FC_Agreement_Versions_Yearly_Cap_049", testCaseDescription = "Verify Add Yearly Cap")
	public void addYearlyCap() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("finance", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		String capYear = fc.utobj().getCurrentYear();
		String yearlyCapAmount = "325";
		String fType = dataSet.get("feeType");
		String agrmntVrsnName = fc.utobj().generateTestData(dataSet.get("agreementVersionName"));
		
		
		String royaltyPrcntg = dataSet.get("royaltyPercentage");
		String advPercentage = dataSet.get("advertisementPercentage");
		String royaltyAreaFranchiseValue = dataSet.get("RoyaltyAreaFranchise");
		String advAreaFranchiseValue = dataSet.get("AdvAreaFranchise");

		try {
			driver = fc.loginpage().login(driver);
			AdminFinanceAgreementVersionsPage pobj = new AdminFinanceAgreementVersionsPage(driver);

			fc.utobj().printTestStep("Add Agreement");

			String agreementVersion = addAgreement(driver, config, agrmntVrsnName,
					royaltyPrcntg, advPercentage, royaltyAreaFranchiseValue, advAreaFranchiseValue );

			fc.utobj().printTestStep("Configure Yearly Cap");

			boolean recordNotPresent = filterRecordByPaging(driver, pobj, agrmntVrsnName);
		
			if (recordNotPresent == false) {
				fc.utobj().throwsException("Agreement Version is not present");
			}

			fc.utobj().actionImgOption(driver, agreementVersion, "Configure Yearly Cap");

			String feeType = AddYearlyCap(driver, config, agreementVersion, capYear, yearlyCapAmount, fType);
			
			WebElement element = driver.findElement(By.xpath(
					".//td[contains(text () , '" + feeType + "')]/ancestor::tr/td[2][contains(text () , '.00')]"));
			
			String textAmount = fc.utobj().getText(driver, element);

			try {
				if (textAmount.contains(",")) {
					
					textAmount = textAmount.replaceAll(",", "").trim();			
				}
				if(textAmount.contains(".00")) {
				
					textAmount = textAmount.replace(".00", "").trim();
				}
			} catch (Exception e) {
			}

			fc.utobj().printTestStep("Verify Configured Yearly Cap Value");

			if (!textAmount.equalsIgnoreCase(yearlyCapAmount)) {
				
				fc.utobj().throwsException("was not able to Add Yearly Cap Value");
			}
			
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}
			
	@Test(groups = { "finance" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "TC_FC_Agreement_Version_Modify_059", testCaseDescription = "Verify Modify Agreement Version")
	public void modifyAgreementVersion() throws IOException, Exception, ParserConfigurationException, SAXException {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("finance", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		String agrmntVrsnName = fc.utobj().generateTestData(dataSet.get("agreementVersionName"));
		
		
		String royaltyPrcntg = dataSet.get("royaltyPercentage");
		String advPercentage = dataSet.get("advertisementPercentage");
		String royaltyAreaFranchiseValue = dataSet.get("RoyaltyAreaFranchise");
		String advAreaFranchiseValue = dataSet.get("AdvAreaFranchise");

		String modifiedAV = dataSet.get("modifyAVName");
		modifiedAV = fc.utobj().generateTestData(modifiedAV);
		String modifiedAgrmntStartDate = fc.utobj().getFutureDateUSFormat(10);
		String modifiedAgrmntEndDate = fc.utobj().getFutureDateUSFormat(20);
		String modifiedRoyaltyPrcntg = dataSet.get("modifyRoyaltyPercentage");
		String modifiedAdvPercentage = dataSet.get("modifyAdvertisementPercentage");
		String modifyRoyaltyAreaFranchiseValue = dataSet.get("modifyRoyaltyAreaFranchise");
		String modifyAdvAreaFranchiseValue = dataSet.get("modifyAdvAreaFranchise");

		try {
			driver = fc.loginpage().login(driver);

			AdminFinanceAgreementVersionsPage pobj = new AdminFinanceAgreementVersionsPage(driver);
			fc.utobj().printTestStep("Add Agreement Version");
			String AgreementVersionName = addAgreement(driver, config, agrmntVrsnName,
					royaltyPrcntg, advPercentage, royaltyAreaFranchiseValue, advAreaFranchiseValue );

			fc.utobj().printTestStep("Modify Agreement Version");

			boolean recordNotPresent = filterRecordByPaging(driver, pobj, agrmntVrsnName);
			if (recordNotPresent == false) {
				fc.utobj().throwsException("Agreement Version is not present");
			}

			fc.utobj().actionImgOption(driver, AgreementVersionName, "Modify");

			fc.utobj().sendKeys(driver, pobj.AgreementName, modifiedAV);
			
			fc.utobj().sendKeys(driver, pobj.AgreementStartDate, modifiedAgrmntStartDate);
			
			fc.utobj().sendKeys(driver, pobj.AgreementEndDate, modifiedAgrmntEndDate);
			
			fc.utobj().sendKeys(driver, pobj.RoyaltyValue, modifiedRoyaltyPrcntg);
			
			fc.utobj().sendKeys(driver, pobj.AdvValue, modifiedAdvPercentage);
			
			fc.utobj().sendKeys(driver, pobj.RoyaltyAreaFranchiseValue, modifyRoyaltyAreaFranchiseValue);
			
			fc.utobj().sendKeys(driver, pobj.AdvAreaFranchiseValue, modifyAdvAreaFranchiseValue);

			fc.utobj().clickElement(driver, pobj.SubmitBtn);

			boolean recordNotPresent1 = filterRecordByPaging(driver, pobj, modifiedAV);
			
			if (recordNotPresent1 == false) {
				fc.utobj().throwsException("Agreement Version is not Modify");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "finance" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "TC_FC_Agreement_Version_Delete_060", testCaseDescription = "Verify Deleted Agreement Version")
	public void deleteAgreementVersion() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("finance", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		String agrmntVrsnName = fc.utobj().generateTestData(dataSet.get("agreementVersionName"));
		
		
		String royaltyPrcntg = dataSet.get("royaltyPercentage");
		String advPercentage = dataSet.get("advertisementPercentage");
		String royaltyAreaFranchiseValue = dataSet.get("RoyaltyAreaFranchise");
		String advAreaFranchiseValue = dataSet.get("AdvAreaFranchise");

		try {
			driver = fc.loginpage().login(driver);

			fc.finance().finance_common().adminFinAgreementVersionsPage(driver);

			AdminFinanceAgreementVersionsPage pobj = new AdminFinanceAgreementVersionsPage(driver);

			fc.utobj().printTestStep("Add Agreement");
			String AgreementVersionName = addAgreement(driver, config, agrmntVrsnName,
					royaltyPrcntg, advPercentage, royaltyAreaFranchiseValue, advAreaFranchiseValue );

			fc.utobj().printTestStep("Delete Agreement Version");

			boolean recordNotPresent = filterRecordByPaging(driver, pobj, AgreementVersionName);
		
			if (recordNotPresent == false) {
				
				fc.utobj().throwsException("Agreement Version is not present");
			}

			fc.utobj().actionImgOption(driver, AgreementVersionName, "Delete");
			
			fc.utobj().acceptAlertBox(driver);

			boolean recordNotPresent1 = filterRecordByPaging(driver, pobj, AgreementVersionName);
			
			if (recordNotPresent1 == true) {
				
				fc.utobj().throwsException("Agreement Version is not Deleted");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "finance")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "TC_FC_Agreement_Version_Map_174", testCaseDescription = "Verify View Map Franchise With Agreement Version")
	public void viewMapFranchiseWithAgreementVersion()
			throws IOException, Exception, ParserConfigurationException, SAXException {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("finance", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		String agrmntVrsnName = fc.utobj().generateTestData(dataSet.get("agreementVersionName"));
		String royaltyPrcntg = dataSet.get("royaltyPercentage");
		String advPercentage = dataSet.get("advertisementPercentage");
		String royaltyAreaFranchiseValue = dataSet.get("RoyaltyAreaFranchise");
		String advAreaFranchiseValue = dataSet.get("AdvAreaFranchise");

		try {
			driver = fc.loginpage().login(driver);
			AdminFinanceAgreementVersionsPage pobj = new AdminFinanceAgreementVersionsPage(driver);

			fc.utobj().printTestStep("Navigate To Admin > Franchise Location > Add Franchise Location");
			
			fc.utobj().printTestStep("Add Franchise Location");
			
			String regionName = fc.utobj().generateTestData("Test");
			
			String storeType = fc.utobj().generateTestData("Test");
			
			String franchiseId = fc.utobj().generateTestData("Test");
			
			AdminFranchiseLocationAddFranchiseLocationPageTest franchise = new AdminFranchiseLocationAddFranchiseLocationPageTest();
			
			franchiseId = franchise.addFranchiseLocation_All(driver, franchiseId, regionName, storeType);

			fc.utobj().printTestStep("Navigate To Admin > Finance > Agreement Versions");
			
			fc.utobj().printTestStep("Add Agreement");
			
			String agreementVersion = addAgreement(driver, config, agrmntVrsnName,
					royaltyPrcntg, advPercentage, royaltyAreaFranchiseValue, advAreaFranchiseValue );

			fc.utobj().printTestStep("Map With Agreement Version");

			boolean recordNotPresent = filterRecordByPaging(driver, pobj, agrmntVrsnName);
			
			if (recordNotPresent == false) {
				
				fc.utobj().throwsException("Agreement Version is not present");
			}

			fc.utobj().actionImgOption(driver, agreementVersion, "View / Map Franchises");
			
			fc.utobj().selectDropDown(driver, pobj.searchFranID, franchiseId);

			fc.utobj().clickElement(driver, pobj.checkAllSFAV);
			
			fc.utobj().clickElement(driver, pobj.mapFranchise);

			fc.utobj().printTestStep(
					"Verify Map Franchise With Agreement Version At Admin > Franchise Location > Manage Franchise Locations");
			fc.adminpage().manageFranchiseLocationLnk(driver);
		
			fc.utobj().selectValFromMultiSelect(driver, pobj.selectFranchiseLocation, fc.utobj()
					.getElement(driver, pobj.selectFranchiseLocation).findElement(By.xpath("./div/div/input")),
					franchiseId);

			fc.utobj().clickElement(driver, pobj.searchButton);
			
			fc.utobj().actionImgOption(driver, franchiseId, "Modify");

			String isVerTextPresent = fc.utobj().getText(driver,
					driver.findElement(By.xpath(".//*[@id='versionID']/option[@selected='']")));

			if (!isVerTextPresent.equalsIgnoreCase(agreementVersion)) {
				
				fc.utobj().throwsException("was not able to verify Agreement Version At Manage Franchise Location");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "finance","finance_FailedTC" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "TC_Copy_Agreement_Version", testCaseDescription = "Verify Copy Agreement Version")
	public void copyAgreementVersion() throws IOException, Exception, ParserConfigurationException, SAXException {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("finance", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		String agrmntVrsnName = fc.utobj().generateTestData(dataSet.get("agreementVersionName"));
		String royaltyPrcntg = dataSet.get("royaltyPercentage");
		String advPercentage = dataSet.get("advertisementPercentage");
		String royaltyAreaFranchiseValue = dataSet.get("RoyaltyAreaFranchise");
		String advAreaFranchiseValue = dataSet.get("AdvAreaFranchise");

		try {
			driver = fc.loginpage().login(driver);
			AdminFinanceAgreementVersionsPage pobj = new AdminFinanceAgreementVersionsPage(driver);

			fc.utobj().printTestStep("Navigate To Admin > Franchise Location > Add Franchise Location");
			
			fc.utobj().printTestStep("Add Franchise Location");

			String regionName = fc.utobj().generateTestData("Test");
		
			String storeType = fc.utobj().generateTestData("Test");
			
			String franchiseId = fc.utobj().generateTestData("Test");
			
			AdminFranchiseLocationAddFranchiseLocationPageTest franchise = new AdminFranchiseLocationAddFranchiseLocationPageTest();
			
			franchiseId = franchise.addFranchiseLocation_All(driver, franchiseId, regionName, storeType);

			fc.utobj().printTestStep("Navigate To Admin > Finance > Agreement Versions");
			
			fc.utobj().printTestStep("Add Agreement");
			
			String agreementVersion = addAgreement(driver, config, agrmntVrsnName,
					royaltyPrcntg, advPercentage, royaltyAreaFranchiseValue, advAreaFranchiseValue );

			fc.utobj().printTestStep("Copy Agreement Version");

			boolean recordNotPresent = filterRecordByPaging(driver, pobj, agrmntVrsnName);

			if (recordNotPresent == false) {

				fc.utobj().throwsException("Agreement Version is not present");
			}

			fc.utobj().actionImgOption(driver, agreementVersion, "Copy Agreement Version");

			String modifiedAV = fc.utobj().generateTestData(agrmntVrsnName);
			
			fc.utobj().sendKeys(driver, pobj.AgreementName, modifiedAV);
			
			fc.utobj().clickElement(driver, pobj.SubmitBtn);

			fc.utobj().printTestStep("Verify Added Agreement Version");

			boolean recordNotPresent1 = filterRecordByPaging(driver, pobj, modifiedAV);

			if (recordNotPresent1 == false) {
				fc.utobj().throwsException("Agreement Version is not copied !");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "finance" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "TC_Copy_With_Mapped_Location", testCaseDescription = "Verify Copy With Mapped Location")
	public void copyWithMappedLocation() throws IOException, Exception, ParserConfigurationException, SAXException {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("finance", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		String agrmntVrsnName = fc.utobj().generateTestData(dataSet.get("agreementVersionName"));
		String royaltyPrcntg = dataSet.get("royaltyPercentage");
		String advPercentage = dataSet.get("advertisementPercentage");
		String royaltyAreaFranchiseValue = dataSet.get("RoyaltyAreaFranchise");
		String advAreaFranchiseValue = dataSet.get("AdvAreaFranchise");

		try {
			driver = fc.loginpage().login(driver);
			AdminFinanceAgreementVersionsPage pobj = new AdminFinanceAgreementVersionsPage(driver);

			fc.utobj().printTestStep("Navigate To Admin > Franchise Location > Add Franchise Location");
			
			fc.utobj().printTestStep("Add Franchise Location");

			String regionName = fc.utobj().generateTestData("Test");
			
			String storeType = fc.utobj().generateTestData("Test");
			
			String franchiseId = fc.utobj().generateTestData("Test");
			
			AdminFranchiseLocationAddFranchiseLocationPageTest franchise = new AdminFranchiseLocationAddFranchiseLocationPageTest();
			
			franchiseId = franchise.addFranchiseLocation_All(driver, franchiseId, regionName, storeType);

			fc.utobj().printTestStep("Navigate To Admin > Finance > Agreement Versions");
			
			fc.utobj().printTestStep("Add Agreement");
			
			String agreementVersion = addAgreement(driver, config, agrmntVrsnName,
					royaltyPrcntg, advPercentage, royaltyAreaFranchiseValue, advAreaFranchiseValue );

			fc.utobj().printTestStep("Map With Agreement Version");

			boolean recordNotPresent = filterRecordByPaging(driver, pobj, agrmntVrsnName);
		
			if (recordNotPresent == false) {
				fc.utobj().throwsException("Agreement Version is not present");
			}

			fc.utobj().actionImgOption(driver, agreementVersion, "View / Map Franchises");
			
			fc.utobj().selectDropDown(driver, pobj.searchFranID, franchiseId);

			fc.utobj().clickElement(driver, pobj.checkAllSFAV);
			
			fc.utobj().clickElement(driver, pobj.mapFranchise);

			fc.utobj().printTestStep("Copy with Mapped Locations");

			boolean recordNotPresent1 = filterRecordByPaging(driver, pobj, agrmntVrsnName);
			
			if (recordNotPresent1 == false) {
				
				fc.utobj().throwsException("Agreement Version is not present");
			}

			fc.utobj().actionImgOption(driver, agreementVersion, "Copy with Mapped Locations");

			String agreementName = fc.utobj().generateTestData(agrmntVrsnName);
			
			fc.utobj().sendKeys(driver, pobj.AgreementName, agreementName);
			
			fc.utobj().clickElement(driver, pobj.SubmitBtn);

			fc.utobj().printTestStep(
					"Verify Map Franchise With Agreement Version At Admin > Franchise Location > Manage Franchise Locations");
			fc.adminpage().manageFranchiseLocationLnk(driver);
			
			fc.utobj().selectValFromMultiSelect(driver, pobj.selectFranchiseLocation, fc.utobj()
					.getElement(driver, pobj.selectFranchiseLocation).findElement(By.xpath("./div/div/input")),
					franchiseId);

			fc.utobj().clickElement(driver, pobj.searchButton);
			
			fc.utobj().actionImgOption(driver, franchiseId, "Modify");

			String isVerTextPresent = fc.utobj().getText(driver,
					driver.findElement(By.xpath(".//*[@id='versionID']/option[@selected='']")));

			if (!isVerTextPresent.equalsIgnoreCase(agreementName)) {
				fc.utobj().throwsException(
						"was not able to verify Copy With Mapped Location Agreement Version At Manage Franchise Location");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "finance" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "TC_FC_Custom_Rule_And_Rule_Formula_Based_173", testCaseDescription = "Verify Add Agreement Version Custom Rule And Rule Formula Based")
	public void addAgreementVersionCustomRuleAndRuleFormulaBased() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("finance", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		String formulaName = fc.utobj().generateTestData(dataSet.get("ruleFormula"));
		String rngToFieldZeroRow = dataSet.get("rangeToFieldZeroRow");
		String apldValueForZeroRow = dataSet.get("appliedValueForZeroRow");
		String apldValueForFirstRow = dataSet.get("appliedValueForFirstRow");
		String ruleCustomFormula = fc.utobj().generateTestData(dataSet.get("agreementRuleCSSB"));
		String applyMinCapValue = dataSet.get("applyMinCapValue");
		String applyMaxCapValue = dataSet.get("applyMaxCapValue");
		String minCapValue = dataSet.get("minCapValue");
		String maxCapValue = dataSet.get("maxCapValue");
		String subCategoryName1 = dataSet.get("subCategoryName");
		String subCategoryName = fc.utobj().generateTestData(subCategoryName1);
		String agrmntVrsnName = fc.utobj().generateTestData(dataSet.get("agreementVersionName"));
		String royaltyPrcntg = dataSet.get("royaltyPercentage");
		String advPercentage = dataSet.get("advertisementPercentage");
		String royaltyAreaFranchiseValue = dataSet.get("RoyaltyAreaFranchise");
		String advAreaFranchiseValue = dataSet.get("AdvAreaFranchise");
		String lateFeeDays = dataSet.get("LateFeeDays");
		String lateFeeAF = dataSet.get("lateFeeAF");

		try {
			driver = fc.loginpage().login(driver);

			AdminFinanceAgreementVersionsPage pobj = new AdminFinanceAgreementVersionsPage(driver);
			
			AdminFinanceConfigureCategoriesforSalesReportPageTest category = new AdminFinanceConfigureCategoriesforSalesReportPageTest();

			fc.utobj().printTestStep("Navigate To Admin > Finance > Configure > Categories > for Sales > Report");
			
			fc.utobj().printTestStep("Add Main Category");
		
			String categoryName = fc.utobj().generateTestData("testCat");
			
			String calculationType = "Addition";
			
			category.addNewCategory(driver, config, categoryName, calculationType);

			fc.utobj().printTestStep("Navigate To Admin > Finance > Configure Categories for Sales Report");
			
			fc.utobj().printTestStep("Add Sub Category");
			
			category.addSubCategory(driver, config, categoryName, subCategoryName, "Addition");

			subCategoryName = fc.utobj().generateTestData(subCategoryName1);
			
			category.addSubCategory(driver, config, categoryName, subCategoryName, "Addition");

			AdminFinanceAgreementRuleFormulaPageTest ruleFormula = new AdminFinanceAgreementRuleFormulaPageTest();
			
			String ruleFormulaName = ruleFormula.addRuleFormulaSalesAmountPercentWithGrowth(driver, config, formulaName,
					rngToFieldZeroRow, apldValueForZeroRow, apldValueForFirstRow);

			AdminFinanceAgreementRulesPageTest agreementRule = new AdminFinanceAgreementRulesPageTest();
			
			String AgreementRule = agreementRule.agreementRuleCustomRuleFormulaBased(driver, config, ruleCustomFormula,
					applyMinCapValue, applyMaxCapValue, minCapValue, maxCapValue, ruleFormulaName);

			fc.utobj().printTestStep("Add Agreement Version");

			String agreementVersionName = addAVBasedCustomRuleAndRuleFormulaBased(driver, config, agrmntVrsnName,
					royaltyPrcntg, advPercentage, royaltyAreaFranchiseValue,
					advAreaFranchiseValue, lateFeeDays, lateFeeAF, AgreementRule);

			fc.utobj().printTestStep("Verify The Agreement Version Custom Rule Based");

			boolean recordNotPresent = filterRecordByPaging(driver, pobj, agreementVersionName);
		
			if (recordNotPresent == false) {
				
				fc.utobj().throwsException("Agreement Version is not present");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	public String addAVBasedCustomRuleAndRuleFormulaBased(WebDriver driver, Map<String, String> config,
			String agrmntVrsnName, String royaltyPrcntg,
			String advPercentage, String royaltyAreaFranchiseValue, String advAreaFranchiseValue, String lateFeeDays,
			String lateFeeAF, String AgreementRule) throws Exception {

		fc.finance().finance_common().adminFinAgreementVersionsPage(driver);

		AdminFinanceAgreementVersionsPage pobj = new AdminFinanceAgreementVersionsPage(driver);
		
		String agrmntStartDate = fc.utobj().getFutureDateUSFormat(-500);
		String agrmntEndDate = fc.utobj().getFutureDateUSFormat(400);

		fc.utobj().clickElement(driver, pobj.AgreemenmtVersionBtn);
		fc.utobj().clickElement(driver, pobj.AddAgreement);
		fc.utobj().sendKeys(driver, pobj.AgreementName, agrmntVrsnName);
		fc.utobj().sendKeys(driver, pobj.AgreementStartDate, agrmntStartDate);
		fc.utobj().sendKeys(driver, pobj.AgreementEndDate, agrmntEndDate);
		fc.utobj().selectDropDown(driver, pobj.AgreementFrequency, "Monthly");
		fc.utobj().selectDropDown(driver, pobj.RoyaltyType, "Rule Based");
		fc.utobj().selectDropDown(driver, pobj.RoyaltyValue, AgreementRule);
		fc.utobj().selectDropDown(driver, pobj.AdvType, "Rule Based");
		fc.utobj().selectDropDown(driver, pobj.AdvValue, AgreementRule);
		fc.utobj().sendKeys(driver, pobj.RoyaltyAreaFranchiseValue, royaltyAreaFranchiseValue);
		fc.utobj().sendKeys(driver, pobj.AdvAreaFranchiseValue, advAreaFranchiseValue);
		fc.utobj().clickElement(driver, pobj.SubmitBtn);

		return agrmntVrsnName;
	}
		
	@Test(groups = { "finance"})
	@TestCase(createdOn="2018-04-24",updatedOn="2018-04-25",testCaseId = "TC_FC_Agreement_Versions_039_11", testCaseDescription = "Verify Add Agreement Version with Flat Royalty")
	public void addAgreementVersionFlat()
			throws IOException, Exception, ParserConfigurationException, SAXException {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName()+"."+new Object(){}.getClass().getEnclosingMethod().getName());
		
		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String,String> dataSet = fc.utobj().readTestData("finance", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		String agrmntVrsnName = fc.utobj().generateTestData(dataSet.get("AVName"));
		String royaltyFlat = dataSet.get("royaltyFlat");
		String advFlat = dataSet.get("advFlat");
		String rArFlatValue = dataSet.get("rArFlatValue");
		String advArFlatValue = dataSet.get("advArFlatValue");
		String agrmntStartDate=fc.utobj().getCurrentDateUSFormat();
		String agrmntEndDate=fc.utobj().getFutureDateUSFormat(100);
		
		try {
			driver = fc.loginpage().login(driver);

			AdminFinanceAgreementVersionsPage pobj = new AdminFinanceAgreementVersionsPage(driver);
			
			fc.utobj().printTestStep("Navigate To Admin > Finance > Agreement Versions");
			
			fc.finance().finance_common().adminFinAgreementVersionsPage( driver);
			
			fc.utobj().printTestStep("Add Agreement Version");
			
			fc.utobj().clickElement(driver, pobj.AgreemenmtVersionBtn);
			
			fc.utobj().clickElement(driver, pobj.AddAgreement);
			
			fc.utobj().sendKeys(driver, pobj.AgreementName, agrmntVrsnName);
			
			fc.utobj().sendKeys(driver, pobj.AgreementStartDate, agrmntStartDate);
			
			fc.utobj().sendKeys(driver, pobj.AgreementEndDate, agrmntEndDate);
			
			fc.utobj().selectDropDown(driver, pobj.AgreementFrequency, "Monthly");
			
			fc.utobj().sendKeys(driver, pobj.AVDescription, "Test Description");
			
			fc.utobj().selectDropDownByValue(driver, pobj.RoyaltyType, "F");
			
			fc.utobj().sendKeys(driver, pobj.RoyaltyValue, royaltyFlat);
			
			fc.utobj().selectDropDownByValue(driver, pobj.AdvType, "F");
			
			fc.utobj().sendKeys(driver, pobj.AdvValue, advFlat);
			
			fc.utobj().selectDropDownByValue(driver, pobj.RoyaltyAreaFranchiseType, "F");
		
			fc.utobj().sendKeys(driver, pobj.RoyaltyAreaFranchiseValue, rArFlatValue);
			
			fc.utobj().selectDropDownByValue(driver, pobj.AdvAreaFranchiseType, "F");
			
			fc.utobj().sendKeys(driver, pobj.AdvAreaFranchiseValue, advArFlatValue);

			fc.utobj().clickElement(driver, pobj.SubmitBtn);
			
			fc.utobj().printTestStep("Verify Added Agreement Version");

			boolean recordNotPresent=filterRecordByPaging(driver, pobj, agrmntVrsnName);
		
			if (recordNotPresent==false) {
				
				fc.utobj().throwsException("Agreement Version is Not added!");
			}

			 fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId );
		}
	}
	
	@Test(groups = { "finance" })
	@TestCase(createdOn = "2018-04-25", updatedOn = "2018-04-27", testCaseId = "TC_FC_Agreement_Versions_Yearly_Cap_049_1", testCaseDescription = "Verify modify Yearly Cap")
	public void modifyYearlyCap() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("finance", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		String capYear = fc.utobj().getCurrentYear();
		String yearlyCapAmount = "2000";
		String modifyYCA = "1500";
		String fType = dataSet.get("feeType");
		String agrmntVrsnName = fc.utobj().generateTestData(dataSet.get("agreementVersionName"));
		
		
		String royaltyPrcntg = dataSet.get("royaltyPercentage");
		String advPercentage = dataSet.get("advertisementPercentage");
		String royaltyAreaFranchiseValue = dataSet.get("RoyaltyAreaFranchise");
		String advAreaFranchiseValue = dataSet.get("AdvAreaFranchise");

		try {
			driver = fc.loginpage().login(driver);
			AdminFinanceAgreementVersionsPage pobj = new AdminFinanceAgreementVersionsPage(driver);

			fc.utobj().printTestStep("Add Agreement");
			
			String agreementVersion = addAgreement(driver, config, agrmntVrsnName,
					royaltyPrcntg, advPercentage, royaltyAreaFranchiseValue, advAreaFranchiseValue );

			fc.utobj().printTestStep("Configure Yearly Cap");

			boolean recordNotPresent = filterRecordByPaging(driver, pobj, agrmntVrsnName);
			
			if (recordNotPresent == false) {
				fc.utobj().throwsException("Agreement Version is not present");
			}

			fc.utobj().actionImgOption(driver, agreementVersion, "Configure Yearly Cap");

			String feeType = AddYearlyCap(driver, config, agreementVersion, capYear, yearlyCapAmount, fType);
			Thread.sleep(1000);
			
			fc.utobj().actionImgOption(driver, feeType, "Modify");
			
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			
			fc.utobj().sendKeys(driver, pobj.capAmount, modifyYCA);
			
			WebElement element1 = driver.findElement(By.xpath(".//*[@id='feeType']/option[@value='" + fType + "']"));
		
			feeType = fc.utobj().getText(driver, element1);

			fc.utobj().clickElement(driver, pobj.saveCapFee);
			
			fc.utobj().clickElement(driver, pobj.closeBtn);
			
			WebElement element = driver.findElement(By.xpath(
					".//td[contains(text () , '" + feeType + "')]/preceding-sibling::td[1]"));
			String textAmount = fc.utobj().getText(driver, element);

			try {
				if (textAmount.contains(",")) {
					textAmount = textAmount.replaceAll(",", "").trim();
				}
				if(textAmount.contains(".00")) {
					textAmount = textAmount.replace(".00", "").trim();
				}
			} catch (Exception e) {
			}
			fc.utobj().printTestStep("Verify Modified Configured Yearly Cap Value");
			
			if (!textAmount.equalsIgnoreCase(modifyYCA)) {
				fc.utobj().throwsException("was not able to Modify Yearly Cap Value");
			}
			
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}
	
	@Test(groups = { "finance", "finance_FailedTC"})
	@TestCase(createdOn = "2018-04-25", updatedOn = "2018-04-27", testCaseId = "TC_FC_Agreement_Versions_Yearly_Cap_049_2", testCaseDescription = "Verify Delete Yearly Cap")
	public void deleteYearlyCap() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("finance", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		String capYear = fc.utobj().getCurrentYear();
		String yearlyCapAmount = "3500";
		String fType = "A2";
				//dataSet.get("feeType");
		String agrmntVrsnName = fc.utobj().generateTestData(dataSet.get("agreementVersionName"));
		
		
		String royaltyPrcntg = dataSet.get("royaltyPercentage");
		String advPercentage = dataSet.get("advertisementPercentage");
		String royaltyAreaFranchiseValue = dataSet.get("RoyaltyAreaFranchise");
		String advAreaFranchiseValue = dataSet.get("AdvAreaFranchise");

		try {
			driver = fc.loginpage().login(driver);
			AdminFinanceAgreementVersionsPage pobj = new AdminFinanceAgreementVersionsPage(driver);

			fc.utobj().printTestStep("Add Agreement");
			
			String agreementVersion = addAgreement(driver, config, agrmntVrsnName, royaltyPrcntg, advPercentage, royaltyAreaFranchiseValue, advAreaFranchiseValue );

			fc.utobj().printTestStep("Configure Yearly Cap");

			boolean recordNotPresent = filterRecordByPaging(driver, pobj, agrmntVrsnName);
			
			if (recordNotPresent == false) {
			
				fc.utobj().throwsException("Agreement Version is not present");
			}

			fc.utobj().actionImgOption(driver, agreementVersion, "Configure Yearly Cap");

			String feeType = AddYearlyCap(driver, config, agreementVersion, capYear, yearlyCapAmount, fType);

			fc.utobj().actionImgOption(driver, feeType, "Delete");

			fc.utobj().acceptAlertBox(driver);
			
			fc.utobj().printTestStep("Verify Deleted Yearly Cap Value");
				
			boolean isTextPresent = fc.utobj().assertLinkText(driver, feeType);

			if (isTextPresent == true) {
				fc.utobj().throwsException("Yearly Cap is not Deleted");
			}
			
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
			
		} catch (Exception e) {
			
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}
	
	@Test(groups = { "finance" })
	@TestCase(createdOn = "2018-05-03", updatedOn = "2018-05-03", testCaseId = "TC_FC_Agreement_Versions_Yearly_Cap_Logs_049_3", testCaseDescription = "Verify View Logs Functinality for Yearly Cap(TC-23197)")
	public void viewLogsYearlyCap() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("finance", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		String capYear = fc.utobj().getCurrentYear();
		String fType = dataSet.get("feeType");
		String agrmntVrsnName = fc.utobj().generateTestData(dataSet.get("agreementVersionName"));
		
		
		String royaltyPrcntg = dataSet.get("royaltyPercentage");
		String advPercentage = dataSet.get("advertisementPercentage");
		String royaltyAreaFranchiseValue = dataSet.get("RoyaltyAreaFranchise");
		String advAreaFranchiseValue = dataSet.get("AdvAreaFranchise");
		String yearlyCapAmount = dataSet.get("yearlyCapAmount");
		String modifyYCA = dataSet.get("modifyYCA");

		try {
			driver = fc.loginpage().login(driver);
			AdminFinanceAgreementVersionsPage pobj = new AdminFinanceAgreementVersionsPage(driver);

			fc.utobj().printTestStep("Add Agreement");
			String agreementVersion = addAgreement(driver, config, agrmntVrsnName,
					royaltyPrcntg, advPercentage, royaltyAreaFranchiseValue, advAreaFranchiseValue );

			fc.utobj().printTestStep("Configure Yearly Cap");
			
			fc.utobj().actionImgOption(driver, agreementVersion, "Configure Yearly Cap");

			String feeType = AddYearlyCap(driver, config, agreementVersion, capYear, yearlyCapAmount, fType);
			Thread.sleep(1000);
			
			fc.utobj().actionImgOption(driver, feeType, "Modify");
			
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			
			fc.utobj().sendKeys(driver, pobj.capAmount, modifyYCA);
			
			WebElement element1 = driver.findElement(By.xpath(".//*[@id='feeType']/option[@value='" + fType + "']"));
			
			feeType = fc.utobj().getText(driver, element1);

			fc.utobj().clickElement(driver, pobj.saveCapFee);
			
			fc.utobj().clickElement(driver, pobj.closeBtn);
			
			WebElement element = driver.findElement(By.xpath(
					".//td[contains(text () , '"+feeType+"')]/preceding-sibling::td[1]"));
			String textAmount = fc.utobj().getText(driver, element);
			
			try {
				if (textAmount.contains(",")) {
					
					textAmount = textAmount.replaceAll(",", "").trim();
				}
				
				if (textAmount.contains(".00"))
				
					textAmount = textAmount.replace(".00", "").trim();
			
			} catch (Exception e) {
			}
		
			fc.utobj().printTestStep("Verify Modified Configured Yearly Cap Value");
			
			if (!textAmount.equalsIgnoreCase(modifyYCA)) {
				
				fc.utobj().throwsException("was not able to Modify Yearly Cap Value");
			}
			
			fc.utobj().clickElement(driver, pobj.viewLogsForYearlyCap);
			
			WebElement element2 = driver.findElement(By.xpath(".//tr[td[contains(text () , 'Record Modified')]]/td[7]"));
			
			String txtAmount = fc.utobj().getText(driver, element2);
			
			if (!txtAmount.equalsIgnoreCase(modifyYCA)) {
				
				fc.utobj().throwsException("Logs are not verified for modified Yearly Cap Value");
			}
			
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		
		} catch (Exception e) {
			
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = {"finance", "finance_FailedTC"})
	@TestCase(createdOn = "2018-05-03", updatedOn = "2018-05-07", testCaseId = "TC_FC_Agreement_Versions_039_21", testCaseDescription = "view Agreement Version (TC-27714)")
	public void viewAgreementVersion() throws IOException, Exception, ParserConfigurationException, SAXException {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("finance", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		String agrmntVrsnName = fc.utobj().generateTestData(dataSet.get("agreementVersionName"));
		String royaltyPrcntg = dataSet.get("royaltyPercentage");
		String advPercentage = dataSet.get("advertisementPercentage");
		String royaltyAreaFranchiseValue = dataSet.get("RoyaltyAreaFranchise");
		String advAreaFranchiseValue = dataSet.get("AdvAreaFranchise");

		try {
			driver = fc.loginpage().login(driver);

			AdminFinanceAgreementVersionsPage pobj = new AdminFinanceAgreementVersionsPage(driver);
		
			fc.utobj().printTestStep("Navigate To Admin > Finance > Agreement Versions");
			
			fc.finance().finance_common().adminFinAgreementVersionsPage(driver);

			fc.utobj().printTestStep("Add Agreement Version");
			
			agrmntVrsnName = addAgreement(driver, config, agrmntVrsnName, royaltyPrcntg, advPercentage, royaltyAreaFranchiseValue, advAreaFranchiseValue );
			
			fc.utobj().printTestStep("Verify Added Agreement Version");

			
			boolean recordNotPresent = filterRecordByPaging(driver, pobj, agrmntVrsnName);
			
			if (recordNotPresent == false) {
				
				fc.utobj().throwsException("Agreement Version is Not added!");
			}
						
			WebElement e1 = driver.findElement(By.linkText(agrmntVrsnName));
			e1.click();
			
			//WebElement e2 = driver.findElement(By.xpath(".//td[contains(text(), 'Agreement Name')]/following-sibling::td[1]"));
			
			boolean isTextPresent = fc.utobj().isElementPresent(driver, driver.findElement(By.xpath(".//td[contains(text(), 'Agreement Name')]/following-sibling::td[1]")));
			
			if (isTextPresent == false) {
				
				fc.utobj().throwsException("Agreement Version Link is not working");
			}
			
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}
	
	public String addAgreement(WebDriver driver, Map<String, String> config, String agrmntVrsnName, String royaltyPrcntg, String advPercentage, String royaltyAreaFranchiseValue, String advAreaFranchiseValue) throws Exception {

		String testCaseId = "TC_Agreement_Versions";
		String enableAFShare="Yes";
		String payFreqAFShare="Weekly";
		String EFTDayDateForAFShare ="Monday";
		String agrmntStartDate = fc.utobj().getFutureDateUSFormat(-400);
		String agrmntEndDate = fc.utobj().getFutureDateUSFormat(400);
		if (fc.utobj().validate(testCaseId)) {

			try {

				AdminFinanceFinanceSetupPreferencesPageTest fin = new AdminFinanceFinanceSetupPreferencesPageTest();
				
				fin.setInvoice(driver, config);
				
				fin.AFSharePayments(driver, config, enableAFShare, payFreqAFShare, EFTDayDateForAFShare);
				
				fc.finance().finance_common().adminFinAgreementVersionsPage(driver);
				
				AdminFinanceAgreementVersionsPage pobj = new AdminFinanceAgreementVersionsPage(driver);

				boolean recordNotPresent = filterRecordByPaging(driver, pobj, agrmntVrsnName);
				
				if (recordNotPresent == false) {
					
					fc.utobj().clickElement(driver, pobj.AgreemenmtVersionBtn);

					fc.utobj().clickElement(driver, pobj.AddAgreement);
					
					fc.utobj().sendKeys(driver, pobj.AgreementName, agrmntVrsnName);
					
					fc.utobj().sendKeys(driver, pobj.AgreementStartDate, agrmntStartDate);
					
					fc.utobj().sendKeys(driver, pobj.AgreementEndDate, agrmntEndDate);

					try {
						
						fc.utobj().selectDropDown(driver, pobj.AgreementFrequency, "Monthly");
					} catch (Exception e) {
						
						fc.utobj().selectDropDown(driver, pobj.AgreementFrequency, "Weekly");
					}

					fc.utobj().selectDropDownByValue(driver, pobj.RoyaltyType, "P");
				
					fc.utobj().sendKeys(driver, pobj.RoyaltyValue, royaltyPrcntg);
					
					fc.utobj().selectDropDownByValue(driver, pobj.AdvType, "P");
					
					fc.utobj().sendKeys(driver, pobj.AdvValue, advPercentage);
					
					fc.utobj().selectDropDownByValue(driver, pobj.RoyaltyAreaFranchiseType, "P");
					
					fc.utobj().sendKeys(driver, pobj.RoyaltyAreaFranchiseValue, royaltyAreaFranchiseValue);
					
					fc.utobj().selectDropDownByValue(driver, pobj.AdvAreaFranchiseType, "P");
					
					fc.utobj().sendKeys(driver, pobj.AdvAreaFranchiseValue, advAreaFranchiseValue);
					
					fc.utobj().clickElement(driver, pobj.SubmitBtn);
				}
				

			} catch (Exception e) {
				
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}

		} else {
			
			fc.utobj().throwsSkipException("Was not Able to Add Agreement_Versions");
		}
		return agrmntVrsnName;
	}
	
	public String AddYearlyCap(WebDriver driver, Map<String, String> config, String agreementVersion, String capYear,
			String yearlyCapAmount, String fType) throws Exception {

		String testCaseId = "TC_Add_Yearly_Cap";
		String feeType = null;

		if (fc.utobj().validate(testCaseId)) {

			try {

				AdminFinanceAgreementVersionsPage pobj = new AdminFinanceAgreementVersionsPage(driver);

				fc.utobj().clickElement(driver, pobj.addYearlyCap);
				
				fc.commonMethods().switch_cboxIframe_frameId(driver);
				
				fc.utobj().selectDropDownByValue(driver, pobj.capYear, capYear);
				fc.utobj().sendKeys(driver, pobj.capAmount, yearlyCapAmount);
				fc.utobj().selectDropDownByValue(driver, pobj.feeType, fType);

				WebElement element = driver.findElement(By.xpath(".//*[@id='feeType']/option[@value='" + fType + "']"));
				feeType = fc.utobj().getText(driver, element);

				fc.utobj().clickElement(driver, pobj.saveCapFee);
				fc.utobj().clickElement(driver, pobj.closeBtn);

			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}

		} else {
			fc.utobj().throwsSkipException("Was not Able to Add Yearly Cap");
		}

		return feeType;
	}

	public boolean filterRecordByPaging(WebDriver driver, AdminFinanceAgreementVersionsPage pobj, String agrmntVrsnName)
			throws Exception {
		boolean isTextPresent2 = false;

		isTextPresent2 = fc.utobj().assertLinkText(driver, agrmntVrsnName);
		if (isTextPresent2 == false) {
			
			boolean isPagePresent = fc.utobj().isElementPresent(driver, pobj.selectViewPerPage);
			if(isPagePresent==true){
				fc.utobj().selectDropDownByValue(driver, pobj.viewPerPageSelect, "500");
				}
			isTextPresent2 = fc.utobj().assertLinkText(driver, agrmntVrsnName);

			if (isTextPresent2 == false) {
				try {

					fc.utobj().clickElement(driver, pobj.nextBtn);
					isTextPresent2 = fc.utobj().assertLinkText(driver, agrmntVrsnName);
				} catch (Exception e) {
					isTextPresent2 = false;
				}
			}
		}
		return isTextPresent2;
	}

	public String addAgreementWithLateFee(WebDriver driver, Map<String, String> config, String agrmntVrsnName, String royaltyPrcntg, String advPercentage,
			String royaltyAreaFranchiseValue, String advAreaFranchiseValue, String lateFeeType, String lateFeeAmount) throws Exception {

		String testCaseId = "TC_Agreement_Versions";
		String enableAFShare="Yes";
		String payFreqAFShare="Weekly";
		String EFTDayDateForAFShare ="Monday";
		String enableDisable="Yes";
		String agrmntStartDate = fc.utobj().getFutureDateUSFormat(-500);
		String agrmntEndDate = fc.utobj().getFutureDateUSFormat(400);
		
		if (fc.utobj().validate(testCaseId)) {

			try {
				
				AdminFinanceFinanceSetupPreferencesPageTest setup = new AdminFinanceFinanceSetupPreferencesPageTest();
				
				//setup.setInvoice(driver, config);
				
				//setup.AFSharePayments(driver, config, enableAFShare, payFreqAFShare, EFTDayDateForAFShare);
				
				setup.enableDisableLateFee(driver, config, enableDisable);
				
				fc.finance().finance_common().adminFinAgreementVersionsPage(driver);
				
				AdminFinanceAgreementVersionsPage pobj = new AdminFinanceAgreementVersionsPage(driver);

				boolean recordNotPresent = filterRecordByPaging(driver, pobj, agrmntVrsnName);
				
				if (recordNotPresent == false) {
					
					fc.utobj().clickElement(driver, pobj.AgreemenmtVersionBtn);
					
					fc.utobj().clickElement(driver, pobj.AddAgreement);
					
					fc.utobj().sendKeys(driver, pobj.AgreementName, agrmntVrsnName);
					
					fc.utobj().sendKeys(driver, pobj.AgreementStartDate, agrmntStartDate);
					
					fc.utobj().sendKeys(driver, pobj.AgreementEndDate, agrmntEndDate);
					
					try {
					
						fc.utobj().selectDropDown(driver, pobj.AgreementFrequency, "Monthly");
					
					} catch (Exception e) {
					
						fc.utobj().selectDropDown(driver, pobj.AgreementFrequency, "Weekly");
					}
					
					fc.utobj().selectDropDownByValue(driver, pobj.RoyaltyType, "P");
					
					fc.utobj().sendKeys(driver, pobj.RoyaltyValue, royaltyPrcntg);
					
					fc.utobj().selectDropDownByValue(driver, pobj.AdvType, "P");
					
					fc.utobj().sendKeys(driver, pobj.AdvValue, advPercentage);
					
					fc.utobj().selectDropDownByValue(driver, pobj.RoyaltyAreaFranchiseType, "P");
					
					fc.utobj().sendKeys(driver, pobj.RoyaltyAreaFranchiseValue, royaltyAreaFranchiseValue);
					
					fc.utobj().selectDropDownByValue(driver, pobj.AdvAreaFranchiseType, "P");
					
					fc.utobj().sendKeys(driver, pobj.AdvAreaFranchiseValue, advAreaFranchiseValue);
					
					fc.utobj().sendKeys(driver, pobj.LateFeeDaysNoOfDays, "10");
					
					fc.utobj().selectDropDownByValue(driver, pobj.selectLateFeeType, lateFeeType);
					
					fc.utobj().sendKeys(driver, pobj.LateFeePercentValue, lateFeeAmount);
					
					fc.utobj().selectDropDown(driver, pobj.selectLateFeeAFType, lateFeeType);
					
					fc.utobj().sendKeys(driver, pobj.LateFeePercentValueAreaFranchise, lateFeeAmount);
					
					fc.utobj().clickElement(driver, pobj.SubmitBtn);
				}
				
			} catch (Exception e) {
				
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}

		} else {
			
			fc.utobj().throwsSkipException("Was not Able to Add Agreement_Versions");
		}
		return agrmntVrsnName;
	}
	
}
