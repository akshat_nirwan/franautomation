package com.builds.test.fin;

import java.io.IOException;
import java.util.Map;
import javax.xml.parsers.ParserConfigurationException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.testng.Reporter;
import org.testng.annotations.Test;
import org.xml.sax.SAXException;
import com.builds.test.crm.CRMGroupsPageTest;
import com.builds.uimaps.fin.AdminFinanceConfigureCategoriesforSalesReportPage;
import com.builds.utilities.FranconnectUtil;
import com.builds.utilities.TestCase;

public class AdminFinanceConfigureCategoriesforSalesReportPageTest {

	FranconnectUtil fc = new FranconnectUtil();

	@Test(groups = "finance")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-05-07", testCaseId = "TC_Admin_FIN_Configure_Category_Add_Modify_Delete", testCaseDescription = "Verify Add / Modify / Delete of New Category for Sales Report")
	public void addModifyDeleteNewCategoryforSalesReport() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());
		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("finance", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		String calculationType = "Addition";
		String categoryName = fc.utobj().generateTestData("Cat");

		try {
			driver = fc.loginpage().login(driver);
			AdminFinanceConfigureCategoriesforSalesReportPage pobj = new AdminFinanceConfigureCategoriesforSalesReportPage(
					driver);
			fc.utobj().printTestStep("Add New Category");
			
			addNewCategory(driver, config, categoryName, calculationType);
			
			fc.utobj().printTestStep("Verify New Added Category");

			boolean isTextFound = fc.utobj().assertPageSource(driver, categoryName);
			
			if (isTextFound == false) {
				fc.utobj().throwsException("Category is Not added!");
			}

			fc.utobj().printTestStep("Modify The Category");

			String CatName = fc.utobj().generateTestData("Cat");
			
			categoryName = modifyCategoryForSaleReport(driver, config, categoryName, CatName, calculationType);

			fc.utobj().printTestStep("Verify The Modified Category");

			boolean isTextFoundM = fc.utobj().assertPageSource(driver, categoryName);
		
			if (isTextFoundM == false) {
				fc.utobj().throwsException("Category is Not Modified !");
			}

			fc.utobj().printTestStep("Delete The Category");
			
			deleteCategoryForSaleReport(driver, config, categoryName, calculationType);
			
			fc.utobj().printTestStep("Verify The Delete The Category");

			boolean isTextFoundD = fc.utobj().assertPageSource(driver, categoryName);
			if (isTextFoundD == true) {
				fc.utobj().throwsException("Category is Not Deleted !");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}
	
	
	@Test(groups = "finance")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-05-02", testCaseId = "TC_Admin_FIN_Configure_Category_Deactivate_Category", testCaseDescription = "Verify The Deactivate Category")
	public void deactivateCategoryOfSalesReport() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());
		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("finance", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		String calculationType = "Addition";
		String categoryName = fc.utobj().generateTestData("Cat");

		try {
			driver = fc.loginpage().login(driver);
			AdminFinanceConfigureCategoriesforSalesReportPage pobj = new AdminFinanceConfigureCategoriesforSalesReportPage(
					driver);

			fc.utobj().printTestStep("Navigate To Admin > Finance > Sales Report Preferences and check Finance and CRM Transactions Integration");
						
			fc.utobj().printTestStep("Add New Category");
			
			addNewCategory(driver, config, categoryName, calculationType);

			fc.utobj().printTestStep("Deactivate The Category");
			
			new CRMGroupsPageTest().actionImgOption(driver, categoryName, "Deactivate");

			fc.commonMethods().switch_frameClass_newLayoutcboxIframe(driver);
			
			fc.utobj().clickElement(driver, pobj.saveBtn);

			fc.utobj().clickElement(driver, pobj.closeBtn);
			
			fc.utobj().switchFrameToDefault(driver);
			
			fc.utobj().printTestStep("Verify The Deactivate Category");

			boolean isTextFound = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//td[contains(text () , '" + categoryName
					+ "')]/ancestor::tr/td[contains(text () , 'Inactive')]");
			if (isTextFound == false) {
				fc.utobj().throwsException("was not able to verify Deactivate Category");
			}

			fc.utobj().printTestStep("Activate The Category");
			
			new CRMGroupsPageTest().actionImgOption(driver, categoryName, "Activate");
			
			fc.commonMethods().switch_frameClass_newLayoutcboxIframe(driver);
			
			fc.utobj().clickElement(driver, pobj.saveBtn);

			fc.utobj().clickElement(driver, pobj.closeBtn);

			fc.utobj().printTestStep("Verify The Activate Category");

			boolean isTextFoundA = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () , '" + categoryName + "')]/ancestor::tr/td[contains(text () , 'Active')]");
			
			if (isTextFoundA == false) {
				fc.utobj().throwsException("was not able to verify Active Category");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	
	@Test(groups = { "finance", "finance_FailedTC" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-05-01", testCaseId = "TC_Admin_FIN_Configure_Category_Subcategory", testCaseDescription = "Verify Add New SubCategory for Sales Report")
	public void addNewSubCategoryforSalesReport()
			throws ParserConfigurationException, SAXException, IOException, Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());
		
		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		
		Map<String, String> dataSet = fc.utobj().readTestData("finance", testCaseId);
		
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		
		String subCategoryName = fc.utobj().generateTestData("SubCat");

		try {
			driver = fc.loginpage().login(driver);
			
			AdminFinanceConfigureCategoriesforSalesReportPage pobj = new AdminFinanceConfigureCategoriesforSalesReportPage(
					driver);

			fc.utobj().printTestStep("Add Main Category");
			
			String categoryName = fc.utobj().generateTestData(dataSet.get("categoryName"));
			
			String calculationType = "Addition";
			
			addNewCategory(driver, config, categoryName, calculationType);

			fc.utobj().printTestStep("Add Sub Category");

			fc.utobj().clickElement(driver, pobj.clickButtonThreeDots);

			fc.utobj().getElement(driver, pobj.addNewSubCategoryButton).click();

			fc.commonMethods().switch_frameClass_newLayoutcboxIframe(driver);
		
			fc.utobj().selectDropDown(driver, pobj.mainCategory, categoryName);
			
			fc.utobj().sendKeys(driver, pobj.subCategoryName, subCategoryName);

			if (!fc.utobj().isSelected(driver,pobj.calculationMethod1)) {
				
				fc.utobj().clickElement(driver, pobj.additionLabel);
			}

			fc.utobj().clickElement(driver, pobj.saveBtn);

			fc.utobj().clickElement(driver, pobj.closeBtn);
			
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Verify Added Sub Category");
			
			boolean isTextFound = fc.utobj().assertPageSource(driver, subCategoryName);
			
			if (isTextFound == false) {
				fc.utobj().throwsException("Sub-Category is Not added!");
			}

			fc.utobj().printTestStep("Modify Added Sub Category");
			
			new CRMGroupsPageTest().actionImgOption(driver, subCategoryName, "Modify");

			fc.commonMethods().switch_frameClass_newLayoutcboxIframe(driver);
			
			subCategoryName = fc.utobj().generateTestData(dataSet.get("subCategoryName"));
			
			fc.utobj().sendKeys(driver, pobj.subCategoryName, subCategoryName);

			if (!fc.utobj().isSelected(driver,pobj.calculationMethod2)) {
				
				fc.utobj().clickElement(driver, pobj.substractionLabel);
			}
			fc.utobj().clickElement(driver, pobj.saveBtn);

			fc.utobj().clickElement(driver, pobj.closeBtn);
			
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Verify Modified Sub Category");
			
			boolean isTextFoundM = fc.utobj().assertPageSource(driver, subCategoryName);
			
			if (isTextFoundM == false) {
				
				fc.utobj().throwsException("Sub-Category is Not Modified!");
			}

			fc.utobj().printTestStep("Delete Added Sub Category");
			
			new CRMGroupsPageTest().actionImgOption(driver, subCategoryName, "Delete");
			
			fc.commonMethods().switch_frameClass_newLayoutcboxIframe(driver);
			
			fc.utobj().clickElement(driver, pobj.saveBtn);

			fc.utobj().clickElement(driver, pobj.closeBtn);
			
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep(
					"Verify If There is only one sub category in Main Category than it should not be deleted");
			boolean isTextFoundD = fc.utobj().assertPageSource(driver, subCategoryName);
			
			if (isTextFoundD == false) {
				fc.utobj().throwsException("Sub-Category is Deleted!");
			}

			fc.utobj().printTestStep("Add Another Sub Category");

			fc.utobj().clickElement(driver, pobj.clickButtonThreeDots);

			fc.utobj().getElement(driver, pobj.addNewSubCategoryButton).click();

			fc.commonMethods().switch_frameClass_newLayoutcboxIframe(driver);
		
			fc.utobj().selectDropDown(driver, pobj.mainCategory, categoryName);
			
			subCategoryName = fc.utobj().generateTestData(dataSet.get("subCategoryName"));
			
			fc.utobj().sendKeys(driver, pobj.subCategoryName, subCategoryName);

			if (!fc.utobj().isSelected(driver,pobj.calculationMethod1)) {
				
				fc.utobj().clickElement(driver, pobj.additionLabel);
			}

			fc.utobj().clickElement(driver, pobj.saveBtn);

			fc.utobj().clickElement(driver, pobj.closeBtn);
			
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Delete Added Another Sub Category");
			
			new CRMGroupsPageTest().actionImgOption(driver, subCategoryName, "Delete");
			
			fc.commonMethods().switch_frameClass_newLayoutcboxIframe(driver);
			
			fc.utobj().clickElement(driver, pobj.saveBtn);

			fc.utobj().clickElement(driver, pobj.closeBtn);
			
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Verify Deleted Another Sub Category");
			
			boolean isTextFoundAd = fc.utobj().assertPageSource(driver, subCategoryName);
			
			if (isTextFoundAd == true) {
				
				fc.utobj().throwsException("Sub-Category is Not Deleted!");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "finance")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-04", testCaseId = "TC_Admin_FIN_Deactivate_Sub_Category", testCaseDescription = "Verify Deactivate Sub Category for Sales Report")
	public void deactivateSubCategory() throws ParserConfigurationException, SAXException, IOException, Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());
		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("finance", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		
		try {
			driver = fc.loginpage().login(driver);
			AdminFinanceConfigureCategoriesforSalesReportPage pobj = new AdminFinanceConfigureCategoriesforSalesReportPage(
					driver);

			fc.utobj().printTestStep("Add Main Category");
			
			String categoryName = fc.utobj().generateTestData(dataSet.get("categoryName"));
			
			String calculationType = "Addition";
			
			categoryName = addNewCategory(driver, config, categoryName, calculationType);

			fc.utobj().printTestStep("Add Sub Category");
			
			String calculationTypeSub = "Addition";
			
			String subCategoryName = fc.utobj().generateTestData(dataSet.get("subCategoryName"));
			
			subCategoryName = addSubCategory(driver, config, categoryName,  subCategoryName, calculationTypeSub);

			fc.utobj().printTestStep("Deactivate Added Sub Category");
			
			new CRMGroupsPageTest().actionImgOption(driver, subCategoryName, "Deactivate");

			fc.commonMethods().switch_frameClass_newLayoutcboxIframe(driver);
			
			fc.utobj().clickElement(driver, pobj.saveBtn);

			fc.utobj().clickElement(driver, pobj.closeBtn);
			
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Verify If only one Sub Category is present , It should not be deactivate");
			
			boolean isTextFoundM = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//*[contains(text () , '" + subCategoryName
					+ "')]/ancestor::tr/td[contains(text () , 'Active')]");
			
			if (isTextFoundM == false) {
				
				fc.utobj().throwsException("Sub-Category is Deactivate while ther is only one subcategory!");
			}

			fc.utobj().printTestStep("Add Another Sub Category");
			
			subCategoryName = fc.utobj().generateTestData(dataSet.get("subCategoryName"));
			
			addSubCategory(driver, config,  categoryName,  subCategoryName, calculationTypeSub);

			fc.utobj().printTestStep("Deactivate Added Another Sub Category");
			
			new CRMGroupsPageTest().actionImgOption(driver, subCategoryName, "Deactivate");
			
			fc.commonMethods().switch_frameClass_newLayoutcboxIframe(driver);
			
			fc.utobj().clickElement(driver, pobj.saveBtn);

			fc.utobj().clickElement(driver, pobj.closeBtn);
			
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Verify The Deactivated Sub Category");
			
			boolean isTextFoundDA = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//*[contains(text () , '" + subCategoryName
					+ "')]/ancestor::tr/td[contains(text () , 'Inactive')]");
			
			if (isTextFoundDA == false) {
				
				fc.utobj().throwsException("Sub-Category is not Deactivated !");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}
	
	@Test(groups = { "finance" })
	@TestCase(createdOn = "2018-05-07", updatedOn = "2018-05-09", testCaseId = "TC_Admin_FIN_041_1", testCaseDescription = "Verify Reorder of Category / Subcategory for Sales Report(TC-23233, TC-23234)")
	public void updateCategorySubcategoryOrderforSalesReport()
			throws ParserConfigurationException, SAXException, IOException, Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("finance", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		String categoryName = fc.utobj().generateTestData(dataSet.get("categoryName"));
		String subCategoryName = fc.utobj().generateTestData(dataSet.get("subCategoryName"));
		String calculationType = dataSet.get("catType");
		
		try {
			driver = fc.loginpage().login(driver);
			AdminFinanceConfigureCategoriesforSalesReportPage pobj = new AdminFinanceConfigureCategoriesforSalesReportPage(
					driver);
			
			fc.utobj().printTestStep("Add Main Category");
			
			categoryName= addNewCategory(driver, config, categoryName, calculationType);

			fc.utobj().printTestStep("Add Sub Category");
			
			String subCategoryName1 = addSubCategory(driver, config, categoryName, subCategoryName, calculationType);
			
			subCategoryName = fc.utobj().generateTestData(dataSet.get("subCategoryName"));
			
			String subCategoryName2 = addSubCategory(driver, config, categoryName, subCategoryName, calculationType);

			fc.utobj().printTestStep("Verify Added Sub Category");
		
			boolean isTextFound1 = fc.utobj().assertPageSource(driver, subCategoryName1);
		
			if (isTextFound1 == false) {
				
				fc.utobj().throwsException("Sub-Category1 is Not added!");
			}
			
			boolean isTextFound2 = fc.utobj().assertPageSource(driver, subCategoryName2);
			
			if (isTextFound2 == false) {
				
				fc.utobj().throwsException("Sub-Category2 is Not added!");
			}
			
			//reorder main categories
			
			fc.utobj().clickElement(driver, pobj.clickButtonThreeDots);
			
			fc.utobj().clickElementWithoutMove(driver, pobj.orderMainCategories);
			
			fc.commonMethods().switch_frameClass_newLayoutcboxIframe(driver);
			
			selectCatForReorder(driver, pobj.selectCategory, categoryName);
			
			fc.utobj().clickElement(driver, pobj.moveUp);
			
			fc.utobj().clickElement(driver, pobj.changeCategorySequence);
			
			fc.utobj().clickElement(driver, pobj.closeBtn);
			
			fc.utobj().switchFrameToDefault(driver);
			
			//reorder sub categories
			
			fc.utobj().clickElement(driver, pobj.clickButtonThreeDots);
			
			fc.utobj().clickElementWithoutMove(driver, pobj.orderSubCategories);
			
			fc.commonMethods().switch_frameClass_newLayoutcboxIframe(driver);
			
			fc.utobj().selectDropDown(driver, pobj.selectMainCatForSubCat, categoryName);
			
			selectCatForReorder(driver, pobj.selectCategory, subCategoryName2);
			
			fc.utobj().clickElement(driver, pobj.moveUp);
			
			fc.utobj().clickElement(driver, pobj.changeCategorySequence);
			
			fc.utobj().clickElement(driver, pobj.closeBtn);
			
			fc.utobj().switchFrameToDefault(driver);
						
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}


	public void selectCatForReorder(WebDriver driver, WebElement element, String text)
			throws Exception {

		try {
			fc.utobj().sleep(500);
			
			Select select = new Select(element);
			
			fc.utobj().sleep(500);
			
			select.selectByVisibleText(text);
			
		} catch (Exception e) {
			
			Reporter.log(e.getMessage());
			
			fc.utobj().throwsException("Problem in Multi Select Box! " + e.getMessage());
		}
	}
	
	
	public String addNewCategory(WebDriver driver, Map<String, String> config, String categoryName,
			String calculationType) throws Exception {
		
		AdminFinanceConfigureCategoriesforSalesReportPage pobj = new AdminFinanceConfigureCategoriesforSalesReportPage(
				driver);
		
		fc.utobj().printTestStep("Navigate To Admin > Finance > Finance Setup Preferences and check Finance and CRM Transactions Integration");
		
		AdminFinanceFinanceSetupPreferencesPageTest setup= new AdminFinanceFinanceSetupPreferencesPageTest();
		
		setup.financeAndCRMTransactionsIntegration(driver);
		
		fc.utobj().printTestStep("Navigate To Admin > Finance > Configure Categories for Sales Report");
		
		fc.finance().finance_common().adminFinConfigureCategoriesforSalesReportPage(driver);
		
		fc.utobj().clickElement(driver, pobj.clickButtonThreeDots);
		
		Thread.sleep(1000);
		
		fc.utobj().clickElementWithoutMove(driver, pobj.addNewCategory);
	
		fc.commonMethods().switch_frameClass_newLayoutcboxIframe(driver);

		fc.utobj().sendKeys(driver, pobj.CategoryName, categoryName);

		if (calculationType.equalsIgnoreCase("Addition")) {

			if (!fc.utobj().isSelected(driver,pobj.calculationMethod1)) {
				
				fc.utobj().clickElement(driver, pobj.additionLabel);
			}

		} else if (calculationType.equalsIgnoreCase("Subtraction")) {

			if (!fc.utobj().isSelected(driver,pobj.calculationMethod2)) {
				
				fc.utobj().clickElement(driver, pobj.substractionLabel);
			}
		}

		fc.utobj().clickElement(driver, pobj.saveBtn);

		fc.utobj().clickElement(driver, pobj.closeBtn);
	
		fc.utobj().switchFrameToDefault(driver);
		
		return categoryName;
	}
	

	public String modifyCategoryForSaleReport(WebDriver driver, Map<String, String> config, String categoryName, String modifyCatName,
			String calculationType) throws Exception {
		
		AdminFinanceConfigureCategoriesforSalesReportPage pobj = new AdminFinanceConfigureCategoriesforSalesReportPage(driver);

		new CRMGroupsPageTest().actionImgOption(driver, categoryName, "Modify");

		fc.commonMethods().switch_frameClass_newLayoutcboxIframe(driver);
		
		fc.utobj().sendKeys(driver, pobj.subCategoryName, modifyCatName);

		if (!fc.utobj().isSelected(driver,pobj.calculationMethod2)) {
			
			fc.utobj().clickElement(driver, pobj.substractionLabel);
		}

		fc.utobj().clickElement(driver, pobj.saveBtn);

		fc.utobj().clickElement(driver, pobj.closeBtn);
		
		fc.utobj().switchFrameToDefault(driver);	
		
		return modifyCatName;
	}

	public void deleteCategoryForSaleReport(WebDriver driver, Map<String, String> config, String categoryName,
			String calculationType) throws Exception {
		
		AdminFinanceConfigureCategoriesforSalesReportPage pobj = new AdminFinanceConfigureCategoriesforSalesReportPage(driver);


		new CRMGroupsPageTest().actionImgOption(driver, categoryName, "Delete");
		
		fc.commonMethods().switch_frameClass_newLayoutcboxIframe(driver);
		
		fc.utobj().clickElement(driver, pobj.saveBtn);

		fc.utobj().clickElement(driver, pobj.closeBtn);
		
		fc.utobj().switchFrameToDefault(driver);
		
	}

	public String addSubCategory(WebDriver driver, Map<String, String> config, String categoryName, String subCategoryName, String calculationType) throws Exception {
		
		AdminFinanceConfigureCategoriesforSalesReportPage pobj = new AdminFinanceConfigureCategoriesforSalesReportPage(driver);

		fc.utobj().clickElement(driver, pobj.clickButtonThreeDots);
		Thread.sleep(1000);
		
		fc.utobj().clickElementWithoutMove(driver, pobj.addNewSubCategoryButton);
		
		fc.commonMethods().switch_frameClass_newLayoutcboxIframe(driver);
	
		fc.utobj().selectDropDown(driver, pobj.mainCategory, categoryName);
		
		fc.utobj().sendKeys(driver, pobj.subCategoryName, subCategoryName);

		if (calculationType.equalsIgnoreCase("Addition")) {

			if (!fc.utobj().isSelected(driver,pobj.calculationMethod1)) {
				
				fc.utobj().clickElement(driver, pobj.additionLabel);
			}

		} else if (calculationType.equalsIgnoreCase("Subtraction")) {

			if (!fc.utobj().isSelected(driver,pobj.calculationMethod2)) {
				
				fc.utobj().clickElement(driver, pobj.substractionLabel);
			}
		}
		fc.utobj().clickElement(driver, pobj.saveBtn);

		fc.utobj().clickElement(driver, pobj.closeBtn);
	
		fc.utobj().switchFrameToDefault(driver);

		return subCategoryName;
	}
	
}
