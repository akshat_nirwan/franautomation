package com.builds.test.fin;

import java.util.Map;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Test;

import com.builds.uimaps.fin.AdminFinanceConfigureFinanceDocumentUploadedPage;
import com.builds.utilities.FranconnectUtil;
import com.builds.utilities.TestCase;

public class AdminFinanceConfigureFinanceDocumentUploadedPageTest {

	FranconnectUtil fc = new FranconnectUtil();

	@Test(groups = { "finance" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-05-16", testCaseId = "TC_Admin_FIN_Configure_Finance_Document", testCaseDescription = "Verify Add ,Modify And Delete Configure Finance Document(s) to be Uploaded")
	private void addDocumentName() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("finance", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		String documentName = fc.utobj().generateTestData(dataSet.get("documentName"));
		try {
			driver = fc.loginpage().login(driver);
			AdminFinanceConfigureFinanceDocumentUploadedPage pobj = new AdminFinanceConfigureFinanceDocumentUploadedPage(
					driver);

			fc.utobj().printTestStep("Navigate To Admin > Finance > Configure Finance Document(s) to be Uploaded");
			
			fc.utobj().printTestStep("Add Document Name");

			documentName = addDocumentName(driver, documentName);
			
			fc.utobj().printTestStep("Verify Added Document Name");

			boolean isDocPresent = fc.utobj().assertPageSource(driver, documentName);

			if (isDocPresent == false) {
				
				fc.utobj().selectDropDownByValue(driver, pobj.resultsPerPage, "500");

				boolean isTextPresent = fc.utobj().assertPageSource(driver, documentName);

				if (isTextPresent == false) {
					
					fc.utobj().throwsException("was not able to verify Add Document");
				}
			}

			fc.utobj().printTestStep("Modify Added Document Name");
			
			fc.utobj().actionImgOption(driver, documentName, "Modify");

			documentName = fc.utobj().generateTestData(dataSet.get("documentName"));
		
			documentName = modifyDocumentName(driver, documentName);
			
			fc.utobj().printTestStep("Verify Modified Document Name");

			boolean isDocPresent1 = fc.utobj().assertPageSource(driver, documentName);

			if (isDocPresent1 == false) {
				fc.utobj().selectDropDownByValue(driver, pobj.resultsPerPage, "500");

				boolean isTextPresent = fc.utobj().assertPageSource(driver, documentName);

				if (isTextPresent == false) {
					fc.utobj().throwsException("Not able to verify The Modification Of Document Name");
				}
			}
			
			fc.utobj().printTestStep("Delete Added Document Name");
			
			fc.utobj().actionImgOption(driver, documentName, "Delete");
			
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			
			fc.utobj().clickElement(driver, pobj.submitButton);
			
			fc.utobj().clickElement(driver, pobj.closeButton);
			
			fc.utobj().switchFrameToDefault(driver);
			
			fc.utobj().printTestStep("Verify Deleted Document Name");

			boolean isDocPresent2 = fc.utobj().assertPageSource(driver, documentName);

			if (isDocPresent2 == true) {

				try {
					fc.utobj().selectDropDownByValue(driver, pobj.resultsPerPage, "500");

				} catch (Exception e) {
				}

				boolean isTextPresent = fc.utobj().assertPageSource(driver, documentName);
				
				if (isTextPresent == true) {
					
					fc.utobj().throwsException("was not able to verify The Deletion Of Document Name");
				}
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	public String addDocumentName(WebDriver driver, String documentName) throws Exception {

		AdminFinanceConfigureFinanceDocumentUploadedPage pobj = new AdminFinanceConfigureFinanceDocumentUploadedPage(
				driver);
		
		fc.finance().finance_common().adminFinConfigureFinancialsDocumenttobeUploadedLnk(driver);
		
		fc.utobj().clickElement(driver, pobj.addDocumentNameBtn);
		
		fc.commonMethods().switch_cboxIframe_frameId(driver);
		
		fc.utobj().sendKeys(driver, pobj.txField, documentName);
		
		fc.utobj().clickElement(driver, pobj.saveBtn);
		
		fc.utobj().clickElement(driver, pobj.closeButton);
		
		fc.utobj().switchFrameToDefault(driver);
		
		return documentName;
	}

	public String modifyDocumentName(WebDriver driver, String documentName) throws Exception {

		AdminFinanceConfigureFinanceDocumentUploadedPage pobj = new AdminFinanceConfigureFinanceDocumentUploadedPage(
				driver);
			
		fc.commonMethods().switch_cboxIframe_frameId(driver);
		
		fc.utobj().sendKeys(driver, pobj.txField, documentName);
		
		fc.utobj().clickElement(driver, pobj.saveBtn);
		
		fc.utobj().clickElement(driver, pobj.closeButton);
		
		fc.utobj().switchFrameToDefault(driver);
		
		return documentName;
	}
}
