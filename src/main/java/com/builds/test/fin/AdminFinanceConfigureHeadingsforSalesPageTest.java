package com.builds.test.fin;

import java.util.Map;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.Test;
import com.builds.uimaps.fin.FinanceSalesPage;
import com.builds.utilities.FranconnectUtil;
import com.builds.utilities.TestCase;

public class AdminFinanceConfigureHeadingsforSalesPageTest {

	FranconnectUtil fc = new FranconnectUtil();

	@Test(groups = "finance")
	@TestCase(createdOn = "2018-08-01", updatedOn = "2018-08-01", testCaseId = "TC_FC_Disable/Enable_Headings_forSales", testCaseDescription = "To verify Disable/Enable headings for sales")
	public void disableEnableHeadingforSales() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());
		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("finance", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			fc.utobj().printTestStep("Navigate To Admin > Finance > Configure Headings for sales Page");
			
			fc.finance().finance_common().adminFinConfigureHeadingsforSalesPage(driver);

			String quantityColumn = "Quantity Column is";
			
			fc.utobj().printTestStep("Check if Quantity is not disabled");
			
			boolean isHeadingDisabled0 = fc.utobj().isElementPresent(driver,"//span[contains(text(),'\"Disable\"')]");
			
			if (isHeadingDisabled0 == false) {

				fc.utobj().printTestStep("Disable Quanity Column");
				
				fc.utobj().actionImgOption(driver, quantityColumn, "Disable");

				fc.commonMethods().switch_cboxIframe_frameId(driver);

				fc.utobj().clickElement(driver, driver.findElement(By.xpath(".//input[@name='submitButton']")));
				
				fc.utobj().printTestStep("Verify Disabled Quantity");
				
				boolean isHeadingDisabled = fc.utobj().isElementPresent(driver,	"//span[contains(text(),'\"Disable\"')]");

				if (isHeadingDisabled == false) {

					fc.utobj().throwsException("Heading for Sales is not disabled");
				}
				
			}
			
			boolean isHeadingEnabled0 = fc.utobj().isElementPresent(driver, "//span[contains(text(),'\"Enable\"')]");

			fc.utobj().printTestStep("Check if Quantity is not Enabled");
			
			if (isHeadingEnabled0 == false) {

				fc.utobj().printTestStep("Enable Quantity");
				
				fc.utobj().actionImgOption(driver, quantityColumn, "Enable");

				fc.commonMethods().switch_cboxIframe_frameId(driver);

				fc.utobj().clickElement(driver, driver.findElement(By.xpath(".//input[@name='submitButton']")));

				fc.utobj().printTestStep("Verify Enabled Quantity");
				
				boolean isHeadingEnabled = fc.utobj().isElementPresent(driver,"//span[contains(text(),'\"Enable\"')]");

				if (isHeadingEnabled == false) {

					fc.utobj().throwsException("Heading for Sales is not enabled");
				}
			}
			
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	
	@Test(groups = "finance")
	@TestCase(createdOn = "2018-08-02", updatedOn = "2018-08-02", testCaseId = "TC_FC_Modify_Quantity_Column", testCaseDescription = "To verify modification of Quantity Column Name")
	public void modifyQuanitytColumnName() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());
		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("finance", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		String quantityName = fc.utobj().generateTestData("Col");
		
		try {
			driver = fc.loginpage().login(driver);

			fc.utobj().printTestStep("Navigate To Admin > Finance > Configure Headings for sales Page");
			
			fc.finance().finance_common().adminFinConfigureHeadingsforSalesPage(driver);
			
			fc.utobj().printTestStep("Verify Default Quantity Name");
			
			boolean isQuantityColumnRenamed0 = fc.utobj().isElementPresent(driver,
					driver.findElement(By.xpath(".//*[contains(text(), 'Quantity')]")));

			if (isQuantityColumnRenamed0 == false) {

				fc.utobj().throwsException("Default quantity column is not Quantity");
			}

			String quantityColumnName = "Quantity Column Name is";
			
			fc.utobj().printTestStep("Modify Quantity Name");
			
			fc.utobj().actionImgOption(driver, quantityColumnName, "Modify");

			fc.commonMethods().switch_cboxIframe_frameId(driver);

			fc.utobj().sendKeys(driver, driver.findElement(By.xpath(".//input[@name='dataValue']")), quantityName);

			fc.utobj().clickElement(driver, driver.findElement(By.xpath(".//input[@name='submitButton']")));

			boolean isQuantityColumnRenamed = fc.utobj().isElementPresent(driver,
					driver.findElement(By.xpath(".//*[contains(text(), '" + quantityName + "')]")));

			if (isQuantityColumnRenamed == false) {

				fc.utobj().throwsException("Quantity Column is not renamed");
			}

			fc.utobj().actionImgOption(driver, quantityColumnName, "Modify");

			fc.utobj().printTestStep("Again modify Quantity Name as it may have impact on other Test Cases");
			
			fc.commonMethods().switch_cboxIframe_frameId(driver);

			fc.utobj().sendKeys(driver, driver.findElement(By.xpath(".//input[@name='dataValue']")), "Quantity");

			fc.utobj().clickElement(driver, driver.findElement(By.xpath(".//input[@name='submitButton']")));

			boolean isQuantityColumnRenamed1 = fc.utobj().isElementPresent(driver,
					driver.findElement(By.xpath(".//*[contains(text(), 'Quantity')]")));

			if (isQuantityColumnRenamed1 == false) {

				fc.utobj().throwsException("Quantity Column is not renamed");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}


	@Test(groups = "finance")
	@TestCase(createdOn = "2018-08-03", updatedOn = "2018-08-03", testCaseId = "TC_FC_Quantity_Column_Name_for_NFC", testCaseDescription = "To verify default Quantity Column Name for Non Financial Category")
	public void defaultQuantityColumnNameForNFC() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());
		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("finance", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		String quaColNameNFC = fc.utobj().generateTestData("Count");
		try {
			driver = fc.loginpage().login(driver);

			fc.utobj().printTestStep("Navigate To Admin > Finance > Configure Headings for sales Page");
			
			fc.finance().finance_common().adminFinConfigureHeadingsforSalesPage(driver);

			boolean defaultQuantityColumnName = fc.utobj().isElementPresent(driver, driver.findElement(By.xpath(".//*[contains(text(), 'Count')]")));

			if (defaultQuantityColumnName == false) {

				fc.utobj().throwsException("Default quantity column name for Non Financial Category is not Count");
			}
			
			String quantityColumnNameNFC = "Quantity Column Name for Non-Financial Categories is";

			fc.utobj().actionImgOption(driver, quantityColumnNameNFC, "Modify");

			fc.commonMethods().switch_cboxIframe_frameId(driver);

			fc.utobj().sendKeys(driver, driver.findElement(By.xpath(".//input[@name='nonFinancialdatavalue']")), quaColNameNFC);

			fc.utobj().clickElement(driver, driver.findElement(By.xpath(".//input[@name='submitButton']")));

			boolean isQuantityColumnRenamed = fc.utobj().isElementPresent(driver,
					driver.findElement(By.xpath(".//*[contains(text(), '" + quaColNameNFC + "')]")));

			if (isQuantityColumnRenamed == false) {

				fc.utobj().throwsException("Quantity Column is not renamed");
			}

			fc.utobj().actionImgOption(driver, quantityColumnNameNFC, "Modify");

			fc.commonMethods().switch_cboxIframe_frameId(driver);

			fc.utobj().sendKeys(driver, driver.findElement(By.xpath(".//input[@name='nonFinancialdatavalue']")), "Count");

			fc.utobj().clickElement(driver, driver.findElement(By.xpath(".//input[@name='submitButton']")));

			boolean isQuantityColumnRenamed1 = fc.utobj().isElementPresent(driver,
					driver.findElement(By.xpath(".//*[contains(text(), 'Count')]")));

			if (isQuantityColumnRenamed1 == false) {

				fc.utobj().throwsException("Quantity Column is not renamed");
			}			
			
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}
	
	
	@Test(groups = "finance")
	@TestCase(createdOn = "2018-08-01", updatedOn = "2018-08-01", testCaseId = "TC_FC_Disable_Quantity_And_Verify_at_Sales_Page", testCaseDescription = "To verify Disabled quantity at Sales Page")
	public void verifyQuantityAtEnterSalesAfterDisabling() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());
		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("finance", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		
		String agrmntVrsnName = "AV";
		String royaltyPrcntg = "10";
		String advPercentage = "8";
		String royaltyAreaFranchiseValue = "5";
		String advAreaFranchiseValue = "3";
		String franchiseId = "001A";
		String salesReportType = "webForm";
		
		try {
			driver = fc.loginpage().login(driver);

			fc.utobj().printTestStep("Navigate To Admin > Finance > Configure Headings for sales Page");
			
			fc.finance().finance_common().adminFinConfigureHeadingsforSalesPage(driver);

			String quantityColumn = "Quantity Column is";

			boolean isHeadingDisabled0 = fc.utobj().isElementPresent(driver, "//span[contains(text(),'\"Disable\"')]");
			
			if (isHeadingDisabled0==false) {
				
				fc.utobj().actionImgOption(driver, quantityColumn, "Disable");
				
				fc.commonMethods().switch_cboxIframe_frameId(driver);

				fc.utobj().clickElement(driver, driver.findElement(By.xpath(".//input[@name='submitButton']")));

				boolean isHeadingDisabled = fc.utobj().isElementPresent(driver,	"//span[contains(text(),'\"Disable\"')]");

				if (isHeadingDisabled == false) {

					fc.utobj().throwsException("Heading for Sales is not disabled");
				}
			}
			
			fc.utobj().printTestStep("Navigate To Admin > Finance > Agreement Versions");

			fc.utobj().printTestStep("Add Agreement Version if already not added");
			
			AdminFinanceAgreementVersionsPageTest agreementforSales = new AdminFinanceAgreementVersionsPageTest();
			
			String agreementVersion = agreementforSales.addAgreement(driver, config, agrmntVrsnName,royaltyPrcntg, advPercentage, royaltyAreaFranchiseValue, advAreaFranchiseValue);
			
			fc.utobj().printTestStep("Navigate To Admin > Franchise Location > Add Franchise Location");

			fc.utobj().printTestStep("Add Franchise Location if already not added");
			
			AdminFranchiseLocationAddNewFranchiseLocationPageTest franchise = new AdminFranchiseLocationAddNewFranchiseLocationPageTest();
			
			franchiseId = franchise.addFranchiseLocation(driver, franchiseId, agreementVersion, config);
			
			fc.utobj().printTestStep("Navigate To Admin > Finance > Finance Setup Preferences");
			fc.utobj().printTestStep("Setup Medium of submission of Sales Reports");
			
			new AdminFinanceFinanceSetupPreferencesPageTest().mediumOfSubmissionOfSalesReports(driver, config, salesReportType);
			
			fc.utobj().printTestStep("Navigate To Finance > Sales");
			
			FinanceSalesPage pobj = new FinanceSalesPage(driver);
			
			fc.finance().finance_common().financeSalesPage(driver);

			fc.utobj().printTestStep("Enter Sales Report");
			
			fc.utobj().clickElement(driver, pobj.enterSalesReport);
			
			fc.utobj().selectDropDown(driver, pobj.selectFranchise, franchiseId);

			fc.utobj().selectDropDownByIndex(driver, pobj.selectReportPeriod, 0);

			fc.utobj().clickElement(driver, pobj.ContinueBtn);
			
			boolean isQuantityPresent = fc.utobj().isElementPresent(driver, ".//*[@class='summaryTbl']//*[contains(text(), 'Quantity')]");
			
			if (isQuantityPresent==true) {
				
				fc.utobj().throwsException("Quantity is showing while it is disabled from Admin");
			}
			
			fc.finance().finance_common().adminFinConfigureHeadingsforSalesPage(driver);
			
			boolean isHeadingEnabled1 = fc.utobj().isElementPresent(driver, ".//span[contains(text(), '\"Enable\"')]");

			if (isHeadingEnabled1 == false) {

				fc.utobj().actionImgOption(driver, quantityColumn, "Enable");

				fc.commonMethods().switch_cboxIframe_frameId(driver);

				fc.utobj().clickElement(driver, driver.findElement(By.xpath(".//input[@name='submitButton']")));

				boolean isHeadingEnabled = fc.utobj().isElementPresent(driver, ".//span[contains(text(), '\"Enable\"')]");

				if (isHeadingEnabled == false) {

					fc.utobj().throwsException("Heading for Sales is not enabled");
				}
			}
				
			
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	
	
}
