package com.builds.test.fin;

import java.util.Map;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;
import com.builds.uimaps.fin.FinanceTaxPage;
import com.builds.utilities.FranconnectUtil;
import com.builds.utilities.TestCase;

public class AdminFinanceConfigureTaxRate {

	FranconnectUtil fc = new FranconnectUtil();

	@Test(groups = "finance_FreshDB")
	@TestCase(createdOn = "2018-06-25", updatedOn = "2018-08-12", testCaseId = "TC_Admin_FIN_Add_Modify_Delete_Tax_Type", testCaseDescription = "To verify add/modify/Delete tax type")
	public void addModifyDeleteTaxType() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());
		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("finance", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		String taxTypeName = fc.utobj().generateTestData("TaxType");

		try {
			driver = fc.loginpage().login(driver);

			taxTypeName = addTaxType(driver, config, taxTypeName);

			boolean isTaxTypePresnet = fc.utobj().assertPageSource(driver, taxTypeName);

			if (isTaxTypePresnet == false) {

				fc.utobj().throwsException("Tax Type is not added");
			}

			fc.utobj().actionImgOption(driver, taxTypeName, "Modify");

			taxTypeName = fc.utobj().generateTestData("TaxType");

			taxTypeName = modifyTaxType(driver, taxTypeName);

			boolean isModifiedTaxTypePresnet = fc.utobj().assertPageSource(driver, taxTypeName);

			if (isModifiedTaxTypePresnet == false) {

				fc.utobj().throwsException("Tax Type is not modified");
			}

			fc.utobj().actionImgOption(driver, taxTypeName, "Delete");

			fc.utobj().acceptAlertBox(driver);

			boolean isDeletedTaxTypePresnet = fc.utobj().assertPageSource(driver, taxTypeName);

			if (isDeletedTaxTypePresnet == true) {

				fc.utobj().throwsException("Tax Type is not deleted");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "finance_FreshDB")
	@TestCase(createdOn = "2018-06-25", updatedOn = "2018-08-12", testCaseId = "TC_Admin_FIN_Add_Modify_Delete_Tax_Rate", testCaseDescription = "Verify Add, Modify and Delete Tax Rate")
	public void addModifyDeleteTaxRate() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());
		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("finance", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		String taxTypeName = "TaxType1234";
		String taxRateName = "TaxRate1234";

		try {
			driver = fc.loginpage().login(driver);

			FinanceTaxPage pobj = new FinanceTaxPage(driver);

			taxTypeName = addTaxType(driver, config, taxTypeName);

			taxRateName = addTaxRate(driver, config, taxRateName, taxTypeName);

			boolean isTaxRatePresnet = fc.utobj().assertPageSource(driver, taxRateName);

			if (isTaxRatePresnet == false) {

				fc.utobj().throwsException("Tax Rate is not added");
			}

			fc.utobj().actionImgOption(driver, taxRateName, "Modify");

			taxRateName = fc.utobj().generateTestData("TaxRateName");

			fc.commonMethods().switch_cboxIframe_frameId(driver);

			fc.utobj().sendKeys(driver, pobj.taxRateName, taxRateName);

			fc.utobj().sendKeys(driver, pobj.description, "For Testing");

			WebElement element = driver.findElement(By.xpath(".//*[contains(text(), '" + taxTypeName
					+ "')]/parent::*/following-sibling::td/input[@type='text']"));

			fc.utobj().sendKeys(driver, element, "10");

			fc.utobj().clickElement(driver, pobj.addTaxBtn);

			fc.utobj().clickElement(driver, pobj.closeBtn);

			fc.utobj().switchFrameToDefault(driver);

			boolean isTaxPresent = fc.utobj().assertPageSource(driver, taxRateName);

			if (isTaxPresent == false) {

				fc.utobj().throwsException("Tax is not Modified");

			}

			fc.utobj().actionImgOption(driver, taxRateName, "Delete");

			fc.utobj().acceptAlertBox(driver);

			boolean isTaxPresent1 = fc.utobj().assertPageSource(driver, taxRateName);

			if (isTaxPresent1 == true) {

				fc.utobj().throwsException("Tax is not Deleted");

			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "finance_FreshDB")
	@TestCase(createdOn = "2018-08-13", updatedOn = "2018-08-13", testCaseId = "TC_Admin_FIN_Verify_Alert_for_Tax_Type", testCaseDescription = "Verify alert for Tax Type")
	public void verifyAlertForTaxType() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());
		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("finance", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			FinanceTaxPage pobj = new FinanceTaxPage(driver);

			fc.utobj().printTestStep("Admin > Finance > Configure Tax Rate > Tax Rate > Add Tax Rate");

			fc.adminpage().openAdminFinConfigureTaxRatesLnk(driver);

			fc.utobj().clickElement(driver, pobj.taxRates);

			fc.utobj().clickElement(driver, pobj.addTaxRate);

			String expectedAlertMsg = "Please configure atleast one Tax Type First.";
			
			String alertMsg = driver.switchTo().alert().getText();

			if (!expectedAlertMsg.equals(alertMsg)) {

				fc.utobj().throwsException("Tax Type is already added");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
			
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "finance_FreshDB")
	@TestCase(createdOn = "2018-08-13", updatedOn = "2018-08-13", testCaseId = "TC_Admin_FIN_Verify_Tax_Rate_details", testCaseDescription = "Verify Tax Rate Details")
	public void verifyTaxRateDetails() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());
		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("finance", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		String taxTypeName = "TaxType1234";
		String taxRateName = "TaxRate1234";

		try {
			driver = fc.loginpage().login(driver);

			taxTypeName = addTaxType(driver, config, taxTypeName);

			taxRateName = addTaxRate(driver, config, taxRateName, taxTypeName);

			fc.utobj().clickElement(driver, ".//*[contains(text(), '"+taxRateName+"')]");

			fc.commonMethods().switch_cboxIframe_frameId(driver);
			
			boolean isTaxRatePresnet = fc.utobj().assertPageSource(driver, taxRateName);

			if (isTaxRatePresnet == false) {

				fc.utobj().throwsException("Tax view is not verified");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
			
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}
	
	@Test(groups = "finance_FreshDB")
	@TestCase(createdOn = "2018-06-25", updatedOn = "2018-08-12", testCaseId = "TC_Admin_FIN_Copy_Tax_Rate_with_Mapped_franchise_Location", testCaseDescription = "Verify copy of tax rate with franchise location")
	public void copyTaxRateWithMappedFranchiseLocations() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());
		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("finance", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		String taxTypeName = "TaxType1234";
		String taxRateName = "TaxRate1234";

		String franchiseId ="001A", agreementVersion = "";
		
		try {
			driver = fc.loginpage().login(driver);
			
			FinanceTaxPage pobj = new FinanceTaxPage(driver);
			
			taxTypeName = addTaxType(driver, config, taxTypeName);

			taxRateName = addTaxRate(driver, config, taxRateName, taxTypeName);
			
			AdminFranchiseLocationAddNewFranchiseLocationPageTest addFranchise = new AdminFranchiseLocationAddNewFranchiseLocationPageTest();
			
			addFranchise.addFranchiseLocationWithTax(driver, franchiseId, agreementVersion, taxRateName, config);
			
			fc.utobj().printTestStep("Admin > Finance > Configure Tax Rate > Tax Rate > Add Tax Rate");
			
			fc.adminpage().openAdminFinConfigureTaxRatesLnk(driver);

			fc.utobj().clickElement(driver, pobj.taxRates);
			
			fc.utobj().actionImgOption(driver, taxRateName, "Copy with Mapped Locations");
			
			fc.commonMethods().switch_cboxIframe_frameId(driver);

			taxRateName = fc.utobj().generateTestData("TaxRate");
			
			fc.utobj().sendKeys(driver, pobj.taxRateName, taxRateName);

			fc.utobj().sendKeys(driver, pobj.description, "Tax Rate with Mapped Franchise location");

			fc.utobj().clickElement(driver, pobj.addTaxBtn);

			fc.utobj().clickElement(driver, pobj.closeBtn);

			fc.utobj().switchFrameToDefault(driver);
			
			boolean isTaxRatePresent= fc.utobj().assertPageSource(driver, taxRateName);
			
			if (isTaxRatePresent==false) {
				
				fc.utobj().throwsException("Tax Rate copy with Mapped location is not added");
			}
			
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
			
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	
	public void configureTaxRateHiddenLnk(WebDriver driver) throws Exception {

		String testCaseId = "configureTaxRates";
		if (fc.utobj().validate(testCaseId)) {

			try {
				fc.utobj().printTestStep("Navigate To Admin > Hidden Link > Configure Tax Rates > Enable Tax");
				fc.adminpage().openAdminHiddenLinkConfigureTaxRateLnk(driver);
				FinanceTaxPage pobj = new FinanceTaxPage(driver);

				if (!fc.utobj().isSelected(driver, pobj.yesBtnConfigTaxRatesHidnLnk)) {
					fc.utobj().clickElement(driver, pobj.yesBtnConfigTaxRatesHidnLnk);
					fc.utobj().clickElement(driver, pobj.submitBtn);
				}

			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		}
	}

	public String addTaxType(WebDriver driver, Map<String, String> config, String taxTypeName) throws Exception {

		String testCaseId = "AddTaxType";
		if (fc.utobj().validate(testCaseId)) {

			try {
				FinanceTaxPage pobj = new FinanceTaxPage(driver);

				configureTaxRateHiddenLnk(driver);

				fc.adminpage().openAdminFinConfigureTaxRatesLnk(driver);
				fc.utobj().printTestStep("Admin > Finance > Configure Tax Rate > Tax Type > Add Tax Type");

				fc.utobj().clickElement(driver, pobj.taxTypes);

				boolean isTaxTypePresent = fc.utobj().assertPageSource(driver, taxTypeName);

				if (isTaxTypePresent == false) {

					fc.utobj().clickElement(driver, pobj.addTaxTypes);

					fc.commonMethods().switch_cboxIframe_frameId(driver);

					Thread.sleep(2000);

					fc.utobj().sendKeys(driver, pobj.taxTypeName, taxTypeName);

					fc.utobj().sendKeys(driver, pobj.description, "For Testing");

					fc.utobj().clickElement(driver, pobj.addTaxBtn);

					fc.utobj().clickElement(driver, pobj.closeBtn);

					fc.utobj().switchFrameToDefault(driver);
				}

			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		}
		return taxTypeName;
	}

	public String addTaxRate(WebDriver driver, Map<String, String> config, String taxRateName, String taxTypeName)
			throws Exception {

		String testCaseId = "AddTaxRate";
		if (fc.utobj().validate(testCaseId)) {

			try {
				FinanceTaxPage pobj = new FinanceTaxPage(driver);

				fc.utobj().printTestStep("Admin > Finance > Configure Tax Rate > Tax Rate > Add Tax Rate");
				fc.adminpage().openAdminFinConfigureTaxRatesLnk(driver);

				fc.utobj().clickElement(driver, pobj.taxRates);

				boolean isTaxPresent = fc.utobj().assertPageSource(driver, taxRateName);

				if (isTaxPresent == false) {

					fc.utobj().clickElement(driver, pobj.addTaxRate);

					fc.commonMethods().switch_cboxIframe_frameId(driver);

					fc.utobj().sendKeys(driver, pobj.taxRateName, taxRateName);

					fc.utobj().sendKeys(driver, pobj.description, "For Testing");

					fc.utobj().clickElement(driver, driver.findElement(By.xpath(".//*[contains(text(), '" + taxTypeName
							+ "')]/parent::*/following-sibling::td/input[@type='checkbox']")));

					WebElement element = driver.findElement(By.xpath(".//*[contains(text(), '" + taxTypeName
							+ "')]/parent::*/following-sibling::td/input[@type='text']"));

					fc.utobj().sendKeys(driver, element, "10");

					fc.utobj().clickElement(driver, pobj.addTaxBtn);

					fc.utobj().clickElement(driver, pobj.closeBtn);

					fc.utobj().switchFrameToDefault(driver);
				}
			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		}

		return taxRateName;
	}

	public String modifyTaxType(WebDriver driver, String taxTypeName) throws Exception {

		FinanceTaxPage pobj = new FinanceTaxPage(driver);

		fc.commonMethods().switch_cboxIframe_frameId(driver);

		Thread.sleep(1000);

		fc.utobj().sendKeys(driver, pobj.taxTypeName, taxTypeName);

		fc.utobj().sendKeys(driver, pobj.description, "For Testing");

		fc.utobj().clickElement(driver, pobj.addTaxBtn);

		fc.utobj().clickElement(driver, pobj.closeBtn);

		fc.utobj().switchFrameToDefault(driver);

		return taxTypeName;
	}

}
