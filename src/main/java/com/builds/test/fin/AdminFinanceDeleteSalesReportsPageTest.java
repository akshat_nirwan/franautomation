package com.builds.test.fin;

import java.util.Map;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.Test;
import com.builds.test.admin.AdminFranchiseLocationAddFranchiseLocationPageTest;
import com.builds.uimaps.fin.AdminFinanceDeleteSalesReportsPage;
import com.builds.uimaps.fin.FinancePaymentsPage;
import com.builds.uimaps.fin.FinanceRoyaltiesPage;
import com.builds.uimaps.fin.FinanceSalesPage;
import com.builds.utilities.FranconnectUtil;
import com.builds.utilities.TestCase;

public class AdminFinanceDeleteSalesReportsPageTest {
	FranconnectUtil fc = new FranconnectUtil();

	@Test(groups = {"finance", "finance_FailedTC"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-07-24", testCaseId = "TC_FC_Delete_Payment_Report_061", testCaseDescription = "Verify delete Payment Reports from Admin")
	public void deletePaymentReportsfromAdmin() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("finance", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		String agrmntVrsnName = fc.utobj().generateTestData(dataSet.get("agreementVersionName"));
		String royaltyPrcntg = dataSet.get("royaltyPercentage");
		String advPercentage = dataSet.get("advertisementPercentage");
		String royaltyAreaFranchiseValue = dataSet.get("RoyaltyAreaFranchise");
		String advAreaFranchiseValue = dataSet.get("AdvAreaFranchise");
		
		try {
			driver = fc.loginpage().login(driver);
			FinanceSalesPage sales_Page = new FinanceSalesPage(driver);
			FinanceSalesPageTest sale_pageTest = new FinanceSalesPageTest();

			fc.utobj().printTestStep("Navigate To Admin > Finance > Agreement Versions");
			AdminFinanceAgreementVersionsPageTest agreementforSales = new AdminFinanceAgreementVersionsPageTest();

			fc.utobj().printTestStep("Add Agreement");
			String agreementVersion = agreementforSales.addAgreement(driver, config, agrmntVrsnName, royaltyPrcntg, advPercentage, royaltyAreaFranchiseValue, advAreaFranchiseValue);

			fc.utobj().printTestStep("Navigate To Admin > Franchise Location > Add Franchise Location");
			fc.utobj().printTestStep("Add Franchise Location");
			
			String franchiseId =fc.utobj().generateTestData("GT");
			AdminFranchiseLocationAddNewFranchiseLocationPageTest franchise = new AdminFranchiseLocationAddNewFranchiseLocationPageTest();
			franchiseId = franchise.addFranchiseLocation(driver, franchiseId, agreementVersion, config);

			fc.utobj().printTestStep("Navigate To Admin > Finance > Finance Setup Preferences");
			fc.utobj().printTestStep("Setup Medium of submission of Sales Reports");
			String salesReportType = "webForm";
			new AdminFinanceFinanceSetupPreferencesPageTest().mediumOfSubmissionOfSalesReports(driver, config,
					salesReportType);

			fc.utobj().printTestStep("Set Invoice Frequency");
			new AdminFinanceFinanceSetupPreferencesPageTest().setInvoice(driver, config);

			fc.utobj().printTestStep("Navigate To Finance > Sales");
			fc.utobj().printTestStep("Enter Sales Report");
			String categoryQuantity = "110";
			String categoryAmount = "6000";
			String reportId = new FinanceSalesPageTest().enterSalesReport(driver, franchiseId, categoryQuantity,
					categoryAmount);

			fc.utobj().printTestStep("Filter Sales Report By Franchise Id");
			sale_pageTest.filterSalesReport(driver, franchiseId);

			fc.utobj().printTestStep("Approve Sales Report");
			fc.utobj().actionImgOption(driver, reportId, "Approve");

			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().sendKeys(driver, sales_Page.commentTxtArea,
					"Test Approve Comments" + fc.utobj().generateRandomNumber());
			fc.utobj().clickElement(driver, sales_Page.SaveBtn);

			fc.utobj().clickElement(driver, sales_Page.cloaseBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Generate Invoice");
			fc.utobj().printTestStep("Filter Sales Report By Franchise Id");
			sale_pageTest.filterSalesReport(driver, franchiseId);

			fc.utobj().printTestStep("Generate Invoice");
			fc.utobj().actionImgOption(driver, franchiseId, "Generate Invoice");

			fc.utobj().printTestStep("Navigate To Finance > Royalties");
			fc.finance().finance_common().financeRoyaltiesPage(driver);

			fc.utobj().printTestStep("Filter Report By Franchise Id");
			sale_pageTest.filterSalesReport(driver, franchiseId);
			
			String invoiceID = fc.utobj().getText(driver, driver.findElement(By.xpath(".//a[contains(text () , '" + franchiseId + "')]/ancestor::tr/td[2]/a[1]")));
			
			fc.utobj().printTestStep("Apply Payment For Royalty");
			fc.utobj().actionImgOption(driver, franchiseId, "Apply Payment");

			FinanceRoyaltiesPage pobj = new FinanceRoyaltiesPage(driver);

			if (!fc.utobj().isSelected(driver, pobj.selectPaymentAppliedforRoyalty)) {
				
				fc.utobj().clickElement(driver, pobj.selectPaymentAppliedforRoyalty);
			}
			fc.utobj().clickElement(driver, pobj.continueBtn);

			fc.utobj().printTestStep("Apply Payment By Cash Option");

			FinancePaymentsPage payments_page = new FinancePaymentsPage(driver);
			
			fc.utobj().sendKeys(driver, payments_page.recievedDate, fc.utobj().getCurrentDateUSFormat());

			double royaltyVal = Double.parseDouble(categoryAmount) * Double.parseDouble(royaltyPrcntg) / 100;
			
			fc.utobj().sendKeys(driver, pobj.paymentAmount, Double.toString(royaltyVal));
			
			fc.utobj().printTestStep("Test Ref No" + fc.utobj().generateRandomChar());

			if (!fc.utobj().isSelected(driver, payments_page.collectionMethodsCash)) {
				fc.utobj().clickElement(driver, payments_page.collectionMethodsCash);
			}

			fc.utobj().sendKeys(driver, payments_page.comments, "Test Comments" + fc.utobj().generateRandomChar());
			
			fc.utobj().clickElement(driver, pobj.saveBtn);
			
			String paymentId = fc.utobj().getText(driver, driver.findElement(By.xpath(".//td[contains(text(), '"+franchiseId+"')]/ancestor::tr/td[1]/a")));
						
			fc.utobj().printTestStep("Navigate To Admin > Finance > Delete Sales Report");
			fc.finance().finance_common().adminFinDeleteSalesReportsLnk(driver);

			AdminFinanceDeleteSalesReportsPage delete_Sales = new AdminFinanceDeleteSalesReportsPage(driver);

			fc.utobj().printTestStep("Go to Payments Tab");
			
			fc.utobj().clickElement(driver, delete_Sales.payments);

			fc.utobj().selectDropDownByValue(driver, delete_Sales.selectViewPerPage, "500");
			
			fc.utobj().clickElement(driver, driver.findElement(By.xpath(
					".//td[contains(text () , '" + franchiseId + "')]/ancestor::tr/td/input[@name='selectedItem']")));
			fc.utobj().clickElement(driver, delete_Sales.delete);

			fc.utobj().acceptAlertBox(driver);
			
			//sale_pageTest.filterSalesReport(driver, franchiseId);
			
			boolean isTextPresent = fc.utobj().assertPageSource(driver, paymentId);
			
			if (isTextPresent == true) {
				fc.utobj().throwsException("was not able to verify the Deleted Payment Report");
				
			}
			
			fc.utobj().printTestStep("Go to Royalties Tab");
			
			fc.utobj().clickElement(driver, delete_Sales.royalties);

			fc.utobj().selectDropDownByValue(driver, delete_Sales.selectViewPerPage, "500");
			
			boolean isTextPresent1 = fc.utobj().assertPageSource(driver, invoiceID);
			
			if (isTextPresent1 == false) {
				fc.utobj().throwsException("Royalty is not present after deleting payment from admin");
				
			}
			
			boolean isTextPresent2 = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//*[contains(text () ,'"+franchiseId+"')]/ancestor::tr/td[contains(text () ,'Unpaid')]");
			
			if (isTextPresent2 == false) {
				
				fc.utobj().throwsException("was not able to verify unpaid Status of royalty after deleting payment");
			}
			
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "finance" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-07-24", testCaseId = "TC_FC_Delete_Invoice_Report_062", testCaseDescription = "Verify delete Invoice Reports from Admin")
	public void deleteInvoiceReportsfromAdmin() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("finance", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		String agrmntVrsnName = fc.utobj().generateTestData(dataSet.get("agreementVersionName"));
		String royaltyPrcntg = dataSet.get("royaltyPercentage");
		String advPercentage = dataSet.get("advertisementPercentage");
		String royaltyAreaFranchiseValue = dataSet.get("RoyaltyAreaFranchise");
		String advAreaFranchiseValue = dataSet.get("AdvAreaFranchise");
		
		

		try {
			driver = fc.loginpage().login(driver);
			FinanceSalesPage pobj = new FinanceSalesPage(driver);
			FinanceSalesPageTest sales_page = new FinanceSalesPageTest();

			fc.utobj().printTestStep("Navigate To Admin > Finance > Agreement Versions");
			AdminFinanceAgreementVersionsPageTest agreementforSales = new AdminFinanceAgreementVersionsPageTest();

			fc.utobj().printTestStep("Add Agreement");
			String agreementVersion = agreementforSales.addAgreement(driver, config, agrmntVrsnName, royaltyPrcntg, advPercentage, royaltyAreaFranchiseValue, advAreaFranchiseValue);

			fc.utobj().printTestStep("Navigate To Admin > Finance >  Configure Finance Document(s) to be Uploaded");
			fc.utobj().printTestStep("Add Document Name");
			String documentName = fc.utobj().generateTestData("TestDoc");
			new AdminFinanceConfigureFinanceDocumentUploadedPageTest().addDocumentName(driver, documentName);

			fc.utobj().printTestStep("Navigate To Admin > Franchise Location > Add Franchise Location");
			fc.utobj().printTestStep("Add Franchise Location");

			String regionName = fc.utobj().generateTestData("Test");
			String franchiseId = fc.utobj().generateTestData("Test");
			AdminFranchiseLocationAddNewFranchiseLocationPageTest franchise = new AdminFranchiseLocationAddNewFranchiseLocationPageTest();
			franchiseId = franchise.addFranchiseLocation(driver, franchiseId, agreementVersion, config);

			fc.utobj().printTestStep("Navigate To Admin > Finance > Finance Setup Preferences");
			fc.utobj().printTestStep("Setup Medium of submission of Sales Reports");
			String salesReportType = "webForm";
			new AdminFinanceFinanceSetupPreferencesPageTest().mediumOfSubmissionOfSalesReports(driver, config,
					salesReportType);

			fc.utobj().printTestStep("Set Invoice Frequency");
			new AdminFinanceFinanceSetupPreferencesPageTest().setInvoice(driver, config);

			fc.utobj().printTestStep("Navigate To Finance > Sales");
			fc.utobj().printTestStep("Enter Sales Report");
			String categoryQuantity = "110";
			String categoryAmount = "6000";
			String reportId = sales_page.enterSalesReport(driver, franchiseId, categoryQuantity, categoryAmount);

			fc.utobj().printTestStep("Filter Sales Report By Franchise Id");
			sales_page.filterSalesReport(driver, franchiseId);

			fc.utobj().printTestStep("Approve Sales Report");
			fc.utobj().actionImgOption(driver, reportId, "Approve");

			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().sendKeys(driver, pobj.commentTxtArea,
					"Test Approve Comments" + fc.utobj().generateRandomNumber());
			fc.utobj().clickElement(driver, pobj.SaveBtn);

			fc.utobj().clickElement(driver, pobj.cloaseBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Filter Sales Report By Franchise Id");
			sales_page.filterSalesReport(driver, franchiseId);

			fc.utobj().printTestStep("Generate Invoice");
			fc.utobj().actionImgOption(driver, franchiseId, "Generate Invoice");

			fc.utobj().printTestStep("Navigate To Finance > Royalties");
			fc.finance().finance_common().financeRoyaltiesPage(driver);

			fc.utobj().printTestStep("Filter Sales Report By Franchise Id");
			sales_page.filterSalesReport(driver, franchiseId);

			String invoiceID = fc.utobj().getText(driver, driver.findElement(By.xpath(".//a[contains(text () , '" + franchiseId + "')]/ancestor::tr/td[2]/a[1]")));
			
			fc.utobj().printTestStep("Navigate To Admin > Finance > Delete Sales Report");
			fc.finance().finance_common().adminFinDeleteSalesReportsLnk(driver);

			AdminFinanceDeleteSalesReportsPage delete_Sales = new AdminFinanceDeleteSalesReportsPage(driver);

			fc.utobj().printTestStep("Go to Royalties Tab");
			fc.utobj().clickElement(driver, delete_Sales.royalties);

			fc.utobj().selectDropDownByValue(driver, delete_Sales.selectViewPerPage, "500");
			
			fc.utobj().clickElement(driver, driver.findElement(By.xpath(
					".//td[contains(text () , '" + franchiseId + "')]/ancestor::tr/td/input[@name='selectedItem']")));
			
			fc.utobj().clickElement(driver, delete_Sales.delete);

			fc.utobj().acceptAlertBox(driver);
			
			sales_page.filterSalesReport(driver, franchiseId);
			
			boolean isTextPresent = fc.utobj().assertPageSource(driver, invoiceID);
			if (isTextPresent == true) {
				fc.utobj().throwsException("was not able to verify the Deleted Royalty Report");

			}
			
			fc.utobj().printTestStep("Go to Sales Tab");
			fc.utobj().clickElement(driver, delete_Sales.sales);
			
			sales_page.filterSalesReport(driver, franchiseId);
			
			boolean isTextPresent1 = fc.utobj().assertPageSource(driver, reportId);
			if (isTextPresent1 == false) {
				fc.utobj().throwsException("Sales report is not present in Delete Sales Report Section");

			}
			
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "finance", "finanaceDummay", "finance_FailedTC" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "TC_FC_Delete_Sales_Report_063", testCaseDescription = "Verify delete Sales Reports from Admin")
	public void deleteSalesReportsfromAdmin() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("finance", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		String agrmntVrsnName = fc.utobj().generateTestData(dataSet.get("agreementVersionName"));
		String royaltyPrcntg = dataSet.get("royaltyPercentage");
		String advPercentage = dataSet.get("advertisementPercentage");
		String royaltyAreaFranchiseValue = dataSet.get("RoyaltyAreaFranchise");
		String advAreaFranchiseValue = dataSet.get("AdvAreaFranchise");
		
		

		try {
			driver = fc.loginpage().login(driver);
			FinanceSalesPageTest sales_page = new FinanceSalesPageTest();

			fc.utobj().printTestStep("Navigate To Admin > Finance > Agreement Versions");
			AdminFinanceAgreementVersionsPageTest agreementforSales = new AdminFinanceAgreementVersionsPageTest();

			fc.utobj().printTestStep("Add Agreement");
			String agreementVersion = agreementforSales.addAgreement(driver, config, agrmntVrsnName, royaltyPrcntg, advPercentage, royaltyAreaFranchiseValue, advAreaFranchiseValue);

			fc.utobj().printTestStep("Navigate To Admin > Finance >  Configure Finance Document(s) to be Uploaded");
			fc.utobj().printTestStep("Add Document Name");
			String documentName = fc.utobj().generateTestData("TestDoc");
			new AdminFinanceConfigureFinanceDocumentUploadedPageTest().addDocumentName(driver, documentName);

			fc.utobj().printTestStep("Navigate To Admin > Franchise Location > Add Franchise Location");
			fc.utobj().printTestStep("Add Franchise Location");

			String regionName = fc.utobj().generateTestData("Test");
			String storeType = fc.utobj().generateTestData("Test");
			String franchiseId = fc.utobj().generateTestData("Test");
			AdminFranchiseLocationAddFranchiseLocationPageTest franchise = new AdminFranchiseLocationAddFranchiseLocationPageTest();
			franchiseId = franchise.addFranchiseLocation_All_Agreement(driver, franchiseId, regionName, storeType,
					agreementVersion, config);

			fc.utobj().printTestStep("Navigate To Admin > Finance > Finance Setup Preferences");
			fc.utobj().printTestStep("Setup Medium of submission of Sales Reports");
			String salesReportType = "webForm";
			new AdminFinanceFinanceSetupPreferencesPageTest().mediumOfSubmissionOfSalesReports(driver, config,
					salesReportType);

			fc.utobj().printTestStep("Set Invoice Frequency");
			new AdminFinanceFinanceSetupPreferencesPageTest().setInvoice(driver, config);

			fc.utobj().printTestStep("Allow Adjustments after invoicing");
			String allowAdj ="Yes";
			new AdminFinanceFinanceSetupPreferencesPageTest().allowAdjustmentsAfterInvoicing(driver, config, allowAdj);

			fc.utobj().printTestStep("Navigate To Finance > Sales");
			fc.utobj().printTestStep("Enter Sales Report");
			String categoryQuantity = "110";
			String categoryAmount = "6000";
			String reportId = sales_page.enterSalesReport(driver, franchiseId, categoryQuantity, categoryAmount);

			fc.utobj().printTestStep("Approve Sales Report");
			FinanceSalesPageTest approveSales = new FinanceSalesPageTest();
			approveSales.approveSalesReport(driver, config, reportId);

			fc.utobj().printTestStep("Navigate To Admin > Finance > Delete Sales Report");
			fc.finance().finance_common().adminFinDeleteSalesReportsLnk(driver);

			AdminFinanceDeleteSalesReportsPage delete_Sales = new AdminFinanceDeleteSalesReportsPage(driver);

			fc.utobj().printTestStep("Go to Sales Tab");
			fc.utobj().clickElement(driver, delete_Sales.sales);

			fc.utobj().selectDropDownByValue(driver, delete_Sales.selectViewPerPage, "500");

			fc.utobj().clickElement(driver, driver.findElement(By.xpath(
					".//td[contains(text () , '" + franchiseId + "')]/ancestor::tr/td/input[@name='selectedItem']")));
			fc.utobj().clickElement(driver, delete_Sales.deleteSales);
			fc.utobj().acceptAlertBox(driver);

			boolean isTextPresent = fc.utobj().assertPageSource(driver, reportId);
			
			if (isTextPresent == true) {
				fc.utobj().throwsException("was not able to verify the Deleted Sales Report");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}
	

	@Test(groups = "finance")
	@TestCase(createdOn = "2018-07-16", updatedOn = "2018-07-20", testCaseId = "TC_Adjustment_Delete_From_Admin", testCaseDescription = "Verify deleted Adjustment Report at Sales Page After Deleteting it from Admin")
	public void verifyAdjustmentAtSalesAfterDeletingFromAdmin() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		Map<String, String> dataSet = fc.utobj().readTestData("finance", testCaseId);

		String agrmntVrsnName = "AV";
		String royaltyPrcntg = "10";
		String advPercentage = "8";
		String royaltyAreaFranchiseValue = "5";
		String advAreaFranchiseValue = "3";
		String agrmntStartDate = fc.utobj().getFutureDateUSFormat(-500);
		

		try {
			driver = fc.loginpage().login(driver);
			FinanceSalesPage pobj = new FinanceSalesPage(driver);

			fc.utobj().printTestStep("Navigate To Admin > Finance > Agreement Versions");
			AdminFinanceAgreementVersionsPageTest agreementforSales = new AdminFinanceAgreementVersionsPageTest();

			fc.utobj().printTestStep("Add Agreement");
			String agreementVersion = agreementforSales.addAgreement(driver, config, agrmntVrsnName, royaltyPrcntg, advPercentage, royaltyAreaFranchiseValue, advAreaFranchiseValue);

			fc.utobj().printTestStep("Navigate To Admin > Finance > Configure Categories for Sales Report");
			AdminFinanceConfigureCategoriesforSalesReportPageTest sales_Report_Page = new AdminFinanceConfigureCategoriesforSalesReportPageTest();

			fc.utobj().printTestStep("Add Main Category With Calculation Type Is Addition");
			String categoryName = fc.utobj().generateTestData("testCat");
			categoryName = sales_Report_Page.addNewCategory(driver, config, categoryName, "Addition");
			
			fc.utobj().printTestStep("Add Sub Category With Calculation Type Is Addition");
			String subCategoryName = fc.utobj().generateTestData("testSubCat");
			subCategoryName = sales_Report_Page.addSubCategory(driver, config, categoryName, subCategoryName, "Addition");
			
			fc.utobj().printTestStep("Navigate To Admin > Franchise Location > Add Franchise Location");
			fc.utobj().printTestStep("Add Franchise Location");

			String regionName = fc.utobj().generateTestData("Test");
			String storeType = fc.utobj().generateTestData("Test");
			String franchiseId = "GT";
			AdminFranchiseLocationAddNewFranchiseLocationPageTest franchise = new AdminFranchiseLocationAddNewFranchiseLocationPageTest();
			franchiseId = franchise.addFranchiseLocation(driver, franchiseId, agreementVersion, config);
			
			fc.utobj().printTestStep("Navigate To Admin > Finance > Finance Setup Preferences");
			fc.utobj().printTestStep("Setup Medium of submission of Sales Reports");
			String salesReportType = "webForm";
			new AdminFinanceFinanceSetupPreferencesPageTest().mediumOfSubmissionOfSalesReports(driver, config,
					salesReportType);

			fc.utobj().printTestStep("Set Invoice Frequency");
			new AdminFinanceFinanceSetupPreferencesPageTest().setInvoice(driver, config);
			fc.utobj().printTestStep("Allow Adjustments after invoicing");
			String allowAdj ="Yes";
			
			new AdminFinanceFinanceSetupPreferencesPageTest().allowAdjustmentsAfterInvoicing(driver, config, allowAdj);
			
			fc.utobj().printTestStep("Navigate To Finance > Sales");
			fc.utobj().printTestStep("Enter Sales Report");
			FinanceSalesPageTest EnterSales = new FinanceSalesPageTest();
			String categoryQuantity = "110";
			String categoryAmount = "6000";
			String reportId = EnterSales.enterSalesReport(driver, franchiseId, categoryQuantity, categoryAmount);

			fc.utobj().printTestStep("Filter Sales Report By Franchise Id");
			EnterSales.filterSalesReport(driver, franchiseId);

			fc.utobj().printTestStep("Approve Sales Report");
			fc.utobj().actionImgOption(driver, reportId, "Approve");

			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().sendKeys(driver, pobj.commentTxtArea,
					"Test Approve Comments" + fc.utobj().generateRandomNumber());
			fc.utobj().clickElement(driver, pobj.SaveBtn);

			fc.utobj().clickElement(driver, pobj.cloaseBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Filter Sales Report By Franchise Id");
			EnterSales.filterSalesReport(driver, franchiseId);

			fc.utobj().printTestStep("Generate Invoice");
			fc.utobj().actionImgOption(driver, franchiseId, "Generate Invoice");

			fc.utobj().printTestStep("Enter Adjustment");
			fc.utobj().actionImgOption(driver, franchiseId, "Enter Adjustment");

			categoryQuantity = "200";
			categoryAmount = "400";
			fc.utobj().sendKeys(driver,	driver.findElement(By.xpath(".//td[contains(text () , 'Service')]/following-sibling::td//input[contains(@name , 'quantity')]")), categoryQuantity);
			fc.utobj().sendKeys(driver,	driver.findElement(By.xpath(".//td[contains(text () , 'Service')]/following-sibling::td//input[contains(@name , 'amount')]")), categoryAmount);

			fc.utobj().sendKeys(driver, pobj.rcvdDate, fc.utobj().getFutureDateUSFormat(-1));

			fc.utobj().clickElement(driver, pobj.SaveBtn);

			if (!fc.utobj().isSelected(driver,pobj.acknowledgement)) {
				
				fc.utobj().clickElement(driver, pobj.acknowledgement);
			}
			fc.utobj().clickElement(driver, pobj.SaveBtn);

			String reportIdAdj=fc.utobj().getText(driver, pobj.reportId);
			
			fc.utobj().clickElement(driver, pobj.okayBtn);
			
			fc.utobj().printTestStep("Navigate To Admin > Finance > Delete Sales Report");
			fc.finance().finance_common().adminFinDeleteSalesReportsLnk(driver);

			AdminFinanceDeleteSalesReportsPage delete_Sales = new AdminFinanceDeleteSalesReportsPage(driver);

			fc.utobj().printTestStep("Go to Sales Tab");
			fc.utobj().clickElement(driver, delete_Sales.sales);

			fc.utobj().selectDropDownByValue(driver, delete_Sales.selectViewPerPage, "500");

			fc.utobj().clickElement(driver, driver.findElement(By.xpath(
					".//td[contains(text () , '" + franchiseId + "')]/ancestor::tr/td/input[@name='selectedItem']")));
			fc.utobj().clickElement(driver, delete_Sales.deleteSales);
			fc.utobj().acceptAlertBox(driver);

			boolean isTextPresent = fc.utobj().assertPageSource(driver, reportIdAdj);
			
			if (isTextPresent == true) {
				fc.utobj().throwsException("was not able to verify the Deleted Sales Report");
			}

			fc.finance().finance_common().financeSalesPage(driver);
			
			fc.utobj().printTestStep("Filter Sales Report By Franchise Id");
			EnterSales.filterSalesReport(driver, franchiseId);
			
			boolean isTextPresent1 = fc.utobj().assertPageSource(driver, reportIdAdj);
			
			if (isTextPresent1 == true) {
				fc.utobj().throwsException("was not able to verify the Deleted Sales Report at Sales Summary Page");
			}
			
			
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	
	@Test(groups = { "finance" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-05-24", testCaseId = "TC_FIN_Delete_Sales_Admin_Waiting_For_Acknowledgement", testCaseDescription = "Verify The Status OF Sales Report Waiting For Acknowledgement And Acknowledgement The Sales Report")
	public void deleteSalesFromAdminNotAcknoledge() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		Map<String, String> dataSet = fc.utobj().readTestData("finance", testCaseId);

		String agrmntVrsnName = "AV";
		String royaltyPrcntg = "10";
		String advPercentage = "8";
		String royaltyAreaFranchiseValue = "5";
		String advAreaFranchiseValue = "3";
		
		try {
			driver = fc.loginpage().login(driver);
			FinanceSalesPage pobj = new FinanceSalesPage(driver);
			FinanceSalesPageTest EnterSales = new FinanceSalesPageTest();

			fc.utobj().printTestStep("Navigate To Admin > Finance > Agreement Versions");
			AdminFinanceAgreementVersionsPageTest agreementforSales = new AdminFinanceAgreementVersionsPageTest();

			fc.utobj().printTestStep("Add Agreement");
			String agreementVersion = agreementforSales.addAgreement(driver, config, agrmntVrsnName, royaltyPrcntg, advPercentage, royaltyAreaFranchiseValue, advAreaFranchiseValue);

			fc.utobj().printTestStep("Navigate To Admin > Franchise Location > Add Franchise Location");
			fc.utobj().printTestStep("Add Franchise Location");

			
			String storeType = fc.utobj().generateTestData("GT");
			String franchiseId = "GT";
			AdminFranchiseLocationAddNewFranchiseLocationPageTest franchise = new AdminFranchiseLocationAddNewFranchiseLocationPageTest();
			franchiseId = franchise.addFranchiseLocation(driver, franchiseId, agreementVersion, config);

			fc.utobj().printTestStep("Navigate To Admin > Finance > Finance Setup Preferences");
			fc.utobj().printTestStep("Setup Medium of submission of Sales Reports");
			String salesReportType = "webForm";
			new AdminFinanceFinanceSetupPreferencesPageTest().mediumOfSubmissionOfSalesReports(driver, config,
					salesReportType);

			fc.utobj().printTestStep("Navigate To Finance > Sales");
			fc.utobj().printTestStep("Enter Sales Report");
			String categoryQuantity = "110";
			String categoryAmount = "6000";

			fc.finance().finance_common().financeSalesPage(driver);
			
			fc.utobj().clickElement(driver, pobj.enterSalesReport);
			fc.utobj().selectDropDown(driver, pobj.selectFranchise, franchiseId);

			fc.utobj().selectDropDownByIndex(driver, pobj.selectReportPeriod, 0);

			fc.utobj().clickElement(driver, pobj.ContinueBtn);

			fc.utobj().sendKeys(driver,
					driver.findElement(By
							.xpath(".//td[contains(text () , 'Service')]/following-sibling::td//input[contains(@name , 'quantity')]")),
					categoryQuantity);
			fc.utobj().sendKeys(driver,
					driver.findElement(By
							.xpath(".//td[contains(text () , 'Service')]/following-sibling::td//input[contains(@name , 'amount')]")),
					categoryAmount);

			fc.utobj().sendKeys(driver, pobj.rcvdDate, fc.utobj().getFutureDateUSFormat(-1));

			fc.utobj().clickElement(driver, pobj.SaveBtn);

			if (fc.utobj().isSelected(driver,pobj.acknowledgement)) {
				fc.utobj().clickElement(driver, pobj.acknowledgement);
			}
			fc.utobj().clickElement(driver, pobj.backBtn);
			
			fc.utobj().printTestStep("Filter Sales Report By Franchise Id");
			EnterSales.filterSalesReport(driver, franchiseId);
			
			String reportId = fc.utobj().getText(driver, driver.findElement(By.xpath(".//a[contains(text(), '"+franchiseId+"')]/ancestor::tr/td[2]/div/a")));
			
			fc.utobj().printTestStep("Navigate To Admin > Finance > Delete Sales Report");
			fc.finance().finance_common().adminFinDeleteSalesReportsLnk(driver);

			AdminFinanceDeleteSalesReportsPage delete_Sales = new AdminFinanceDeleteSalesReportsPage(driver);

			fc.utobj().printTestStep("Go to Sales Tab");
			fc.utobj().clickElement(driver, delete_Sales.sales);

			fc.utobj().selectDropDownByValue(driver, delete_Sales.selectViewPerPage, "500");

			fc.utobj().clickElement(driver, driver.findElement(By.xpath(
					".//td[contains(text () , '" + franchiseId + "')]/ancestor::tr/td/input[@name='selectedItem']")));
			
			fc.utobj().printTestStep("Delete Sales Report");
			fc.utobj().clickElement(driver, delete_Sales.deleteSales);
			
			fc.utobj().acceptAlertBox(driver);
			
			fc.utobj().printTestStep("Again Navigate to Sales Page and Check Deleted Sales Report");
			fc.finance().finance_common().financeSalesPage(driver);
			
			fc.utobj().printTestStep("Filter Sales Report By Franchise Id");
			EnterSales.filterSalesReport(driver, franchiseId);

			boolean isSalesPresent = fc.utobj().assertLinkText(driver, reportId);
			
			if (isSalesPresent==false) {
				fc.utobj().throwsException("Deleted Sales Report from Admin is visible at Sales Summary Page");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}
	
	@Test(groups = "finance")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2017-06-09", testCaseId = "TC_FC_Delete_Royalty_From_Admin", testCaseDescription = "Delete Royalty from Admin")
	public void deleteRoyaltyFromAdmin() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("finance", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		String agrmntVrsnName = "AV";
		String royaltyPrcntg = "10";
		String advPercentage = "8";
		String royaltyAreaFranchiseValue = "5";
		String advAreaFranchiseValue = "3";
		
		

		try {
			driver = fc.loginpage().login(driver);
			FinanceRoyaltiesPage pobj = new FinanceRoyaltiesPage(driver);
			FinanceSalesPage sales_Page = new FinanceSalesPage(driver);
			FinanceSalesPageTest sale_pageTest = new FinanceSalesPageTest();

			fc.utobj().printTestStep("Navigate To Admin > Finance > Agreement Versions");
			AdminFinanceAgreementVersionsPageTest agreementforSales = new AdminFinanceAgreementVersionsPageTest();

			fc.utobj().printTestStep("Add Agreement");
			String agreementVersion = agreementforSales.addAgreement(driver, config, agrmntVrsnName, royaltyPrcntg, advPercentage, royaltyAreaFranchiseValue, advAreaFranchiseValue);
			
			fc.utobj().printTestStep("Navigate To Admin > Franchise Location > Add Franchise Location");
			fc.utobj().printTestStep("Add Franchise Location");

			
			String franchiseId = "001A";
			AdminFranchiseLocationAddNewFranchiseLocationPageTest franchise = new AdminFranchiseLocationAddNewFranchiseLocationPageTest();
			franchiseId = franchise.addFranchiseLocation(driver, franchiseId, agreementVersion, config);
			fc.utobj().printTestStep("Navigate To Admin > Finance > Finance Setup Preferences");
			fc.utobj().printTestStep("Setup Medium of submission of Sales Reports");
			String salesReportType = "webForm";
			new AdminFinanceFinanceSetupPreferencesPageTest().mediumOfSubmissionOfSalesReports(driver, config,
					salesReportType);

			fc.utobj().printTestStep("Set Invoice Frequency");
			new AdminFinanceFinanceSetupPreferencesPageTest().setInvoice(driver, config);

			fc.utobj().printTestStep("Navigate To Finance > Sales");
			fc.utobj().printTestStep("Enter Sales Report");
			String categoryQuantity = "110";
			String categoryAmount = "6000";
			String reportId = new FinanceSalesPageTest().enterSalesReport(driver, franchiseId, categoryQuantity,
					categoryAmount);

			fc.utobj().printTestStep("Filter Sales Report By Franchise Id");
			sale_pageTest.filterSalesReport(driver, franchiseId);

			fc.utobj().printTestStep("Approve Sales Report");
			fc.utobj().actionImgOption(driver, reportId, "Approve");

			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().sendKeys(driver, sales_Page.commentTxtArea,
					"Test Approve Comments" + fc.utobj().generateRandomNumber());
			fc.utobj().clickElement(driver, sales_Page.SaveBtn);

			fc.utobj().clickElement(driver, sales_Page.cloaseBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Generate Invoice");
			fc.utobj().printTestStep("Filter Sales Report By Franchise Id");
			sale_pageTest.filterSalesReport(driver, franchiseId);

			fc.utobj().printTestStep("Generate Invoice");
			fc.utobj().actionImgOption(driver, franchiseId, "Generate Invoice");

			fc.utobj().printTestStep("Navigate To Finance > Royalties");
			fc.finance().finance_common().financeRoyaltiesPage(driver);

			fc.utobj().printTestStep("Filter Sales Report By Franchise Id");
			sale_pageTest.filterSalesReport(driver, franchiseId);

			String invoiceID = fc.utobj().getText(driver, driver.findElement(By.xpath(".//a[contains(text () , '" + franchiseId + "')]/ancestor::tr/td[2]/a[1]")));

			fc.utobj().printTestStep("Navigate To Admin > Finance > Delete Sales Report");
			fc.finance().finance_common().adminFinDeleteSalesReportsLnk(driver);

			AdminFinanceDeleteSalesReportsPage delete_Sales = new AdminFinanceDeleteSalesReportsPage(driver);

			fc.utobj().printTestStep("Go to Sales Tab");
			fc.utobj().clickElement(driver, delete_Sales.royalties);

			fc.utobj().selectDropDownByValue(driver, delete_Sales.selectViewPerPage, "500");

			fc.utobj().clickElement(driver, driver.findElement(By.xpath(
					".//td[contains(text () , '" + franchiseId + "')]/ancestor::tr/td/input[@name='selectedItem']")));
			
			fc.utobj().printTestStep("Delete Royalty Report");
			fc.utobj().clickElement(driver, delete_Sales.deleteSales);
			
			fc.utobj().acceptAlertBox(driver);
			
			fc.utobj().printTestStep("Navigate To Finance > Royalties");
			fc.finance().finance_common().financeRoyaltiesPage(driver);

			fc.utobj().printTestStep("Filter Sales Report By Franchise Id");
			sale_pageTest.filterSalesReport(driver, franchiseId);
			
			boolean isInvoicePresent = fc.utobj().assertPageSource(driver, invoiceID);
			
			if (isInvoicePresent == true) {
				fc.utobj().throwsException("Invoice is not deleted from Admin");
			}
			
			fc.utobj().printTestStep("Navigate To Finance > Royalties");
			fc.finance().finance_common().financeSalesPage(driver);

			fc.utobj().printTestStep("Filter Sales Report By Franchise Id");
			sale_pageTest.filterSalesReport(driver, franchiseId);
			
			boolean isInvoicePresent1 = fc.utobj().assertPageSource(driver, reportId);
			
			if (isInvoicePresent1 == false) {
				
				fc.utobj().throwsException("Sales ReportId is not visible at Sales Summary Page");
			}
			
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}
	
	@Test(groups = "finance")
	@TestCase(createdOn = "2018-07-24", updatedOn = "2018-07-24", testCaseId = "TC_FC_Delete_Payment_And_Verify_Invoice_Status", testCaseDescription = "Verify Invoice Status after deleting payment from Admin")
	public void deletePaymentfromAdminAndVerifyInvoiceStatus() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("finance", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		String agrmntVrsnName = "AV";
		String royaltyPrcntg = "10";
		String advPercentage = "8";
		String royaltyAreaFranchiseValue = "5";
		String advAreaFranchiseValue = "3";
		
		

		try {
			driver = fc.loginpage().login(driver);
			FinanceSalesPage sales_Page = new FinanceSalesPage(driver);
			FinanceSalesPageTest sale_pageTest = new FinanceSalesPageTest();

			fc.utobj().printTestStep("Navigate To Admin > Finance > Agreement Versions");
			AdminFinanceAgreementVersionsPageTest agreementforSales = new AdminFinanceAgreementVersionsPageTest();

			fc.utobj().printTestStep("Add Agreement");
			String agreementVersion = agreementforSales.addAgreement(driver, config, agrmntVrsnName, royaltyPrcntg, advPercentage, royaltyAreaFranchiseValue, advAreaFranchiseValue);
			fc.utobj().printTestStep("Navigate To Admin > Franchise Location > Add Franchise Location");
			fc.utobj().printTestStep("Add Franchise Location");

			String regionName = fc.utobj().generateTestData("GT");
			String franchiseId = fc.utobj().generateTestData("GT");
			AdminFranchiseLocationAddNewFranchiseLocationPageTest franchise = new AdminFranchiseLocationAddNewFranchiseLocationPageTest();
			franchiseId = franchise.addFranchiseLocation(driver, franchiseId, agreementVersion, config);
			
			fc.utobj().printTestStep("Navigate To Admin > Finance > Finance Setup Preferences");
			fc.utobj().printTestStep("Setup Medium of submission of Sales Reports");
			String salesReportType = "webForm";
			new AdminFinanceFinanceSetupPreferencesPageTest().mediumOfSubmissionOfSalesReports(driver, config, salesReportType);

			fc.utobj().printTestStep("Set Invoice Frequency");
			new AdminFinanceFinanceSetupPreferencesPageTest().setInvoice(driver, config);

			fc.utobj().printTestStep("Navigate To Finance > Sales");
			fc.utobj().printTestStep("Enter Sales Report");
			String categoryQuantity = "110";
			String categoryAmount = "6000";
			String reportId = new FinanceSalesPageTest().enterSalesReport(driver, franchiseId, categoryQuantity,
					categoryAmount);

			fc.utobj().printTestStep("Filter Sales Report By Franchise Id");
			sale_pageTest.filterSalesReport(driver, franchiseId);

			fc.utobj().printTestStep("Approve Sales Report");
			fc.utobj().actionImgOption(driver, reportId, "Approve");

			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().sendKeys(driver, sales_Page.commentTxtArea,
					"Test Approve Comments" + fc.utobj().generateRandomNumber());
			fc.utobj().clickElement(driver, sales_Page.SaveBtn);

			fc.utobj().clickElement(driver, sales_Page.cloaseBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Generate Invoice");
			fc.utobj().printTestStep("Filter Sales Report By Franchise Id");
			sale_pageTest.filterSalesReport(driver, franchiseId);

			fc.utobj().printTestStep("Generate Invoice");
			fc.utobj().actionImgOption(driver, franchiseId, "Generate Invoice");

			fc.utobj().printTestStep("Navigate To Finance > Royalties");
			fc.finance().finance_common().financeRoyaltiesPage(driver);

			fc.utobj().printTestStep("Filter Report By Franchise Id");
			sale_pageTest.filterSalesReport(driver, franchiseId);
			
			String invoiceID = fc.utobj().getText(driver, driver.findElement(By.xpath(".//a[contains(text () , '" + franchiseId + "')]/ancestor::tr/td[2]/a[1]")));
			
			fc.utobj().printTestStep("Apply Payment For Royalty");
			fc.utobj().actionImgOption(driver, franchiseId, "Apply Payment");

			FinanceRoyaltiesPage pobj = new FinanceRoyaltiesPage(driver);

			if (!fc.utobj().isSelected(driver, pobj.selectPaymentAppliedforRoyalty)) {
				
				fc.utobj().clickElement(driver, pobj.selectPaymentAppliedforRoyalty);
			}
			fc.utobj().clickElement(driver, pobj.continueBtn);

			fc.utobj().printTestStep("Apply Payment By Cash Option");

			FinancePaymentsPage payments_page = new FinancePaymentsPage(driver);
			
			fc.utobj().sendKeys(driver, payments_page.recievedDate, fc.utobj().getCurrentDateUSFormat());

			double royaltyVal = Double.parseDouble(categoryAmount) * Double.parseDouble(royaltyPrcntg) / 100;
			
			fc.utobj().sendKeys(driver, pobj.paymentAmount, Double.toString(royaltyVal));
			
			fc.utobj().printTestStep("Test Ref No" + fc.utobj().generateRandomChar());

			if (!fc.utobj().isSelected(driver, payments_page.collectionMethodsCash)) {
				fc.utobj().clickElement(driver, payments_page.collectionMethodsCash);
			}

			fc.utobj().sendKeys(driver, payments_page.comments, "Test Comments" + fc.utobj().generateRandomChar());
			
			fc.utobj().clickElement(driver, pobj.saveBtn);
			
			String paymentId = fc.utobj().getText(driver, driver.findElement(By.xpath(".//td[contains(text(), '"+franchiseId+"')]/ancestor::tr/td[1]/a")));
						
			fc.utobj().printTestStep("Navigate To Admin > Finance > Delete Sales Report");
			fc.finance().finance_common().adminFinDeleteSalesReportsLnk(driver);

			AdminFinanceDeleteSalesReportsPage delete_Sales = new AdminFinanceDeleteSalesReportsPage(driver);

			fc.utobj().printTestStep("Go to Payments Tab");
			
			fc.utobj().clickElement(driver, delete_Sales.payments);

			fc.utobj().selectDropDownByValue(driver, delete_Sales.selectViewPerPage, "500");
			
			fc.utobj().clickElement(driver, driver.findElement(By.xpath(
					".//td[contains(text () , '" + franchiseId + "')]/ancestor::tr/td/input[@name='selectedItem']")));
			fc.utobj().clickElement(driver, delete_Sales.delete);
			
			fc.utobj().acceptAlertBox(driver);
			
			fc.utobj().printTestStep("Navigate To Finance > Royalties");
			fc.finance().finance_common().financeRoyaltiesPage(driver);
			
			fc.utobj().printTestStep("Filter Report By Franchise Id");
			sale_pageTest.filterSalesReport(driver, franchiseId);
			
			boolean isTextPresent1 = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[contains(text () ,'"+franchiseId+"')]/ancestor::tr/td[contains(text () ,'Unpaid')]");
			
			if (isTextPresent1 == false) {
				
				fc.utobj().throwsException("was not able to verify unpaid Status of royalty after reverse payment");
			}
			
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	
	
	
	
	
}
