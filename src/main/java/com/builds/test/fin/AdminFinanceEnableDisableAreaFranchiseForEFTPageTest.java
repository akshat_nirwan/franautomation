package com.builds.test.fin;

import java.util.Map;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.Test;
import com.builds.test.admin.AdminFranchiseLocationAddFranchiseLocationPageTest;
import com.builds.uimaps.admin.AdminPage;
import com.builds.uimaps.fin.AdminFinanceAgreementVersionsPage;
import com.builds.uimaps.fin.AdminFinanceEnableDisableFranchiseforEFTPage;
import com.builds.utilities.FranconnectUtil;
import com.builds.utilities.TestCase;

public class AdminFinanceEnableDisableAreaFranchiseForEFTPageTest {

	FranconnectUtil fc = new FranconnectUtil();

	@Test(groups = { "finance" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "TC_Enable_Area_Franchise_EFT", testCaseDescription = "Verify Configure And Enable Area Franchise for EFT")
	public void enableFranchiseforEFT() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("finance", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		String agrmntVrsnName = fc.utobj().generateTestData(dataSet.get("agreementVersionName"));
		String royaltyPrcntg = dataSet.get("royaltyPercentage");
		String advPercentage = dataSet.get("advertisementPercentage");
		String royaltyAreaFranchiseValue = dataSet.get("RoyaltyAreaFranchise");
		String advAreaFranchiseValue = dataSet.get("AdvAreaFranchise");
		String regionName = fc.utobj().generateTestData("GT");
		String storeType = fc.utobj().generateTestData("GT");
		String franchiseId = fc.utobj().generateTestData("GT");

		try {
			driver = fc.loginpage().login(driver);
			AdminFinanceEnableDisableFranchiseforEFTPage pobj = new AdminFinanceEnableDisableFranchiseforEFTPage(
					driver);

			fc.utobj().printTestStep("Navigate To Admin > Franchise Location > Add Franchise Location");

			fc.utobj().printTestStep("Add Franchise Location");

			AdminFranchiseLocationAddFranchiseLocationPageTest franchise = new AdminFranchiseLocationAddFranchiseLocationPageTest();

			franchiseId = franchise.addFranchiseLocation_All(driver, franchiseId, regionName, storeType);

			fc.utobj().printTestStep("Navigate To Admin > Finance >  Agreement Versions");

			fc.utobj().printTestStep("Add Agreement");

			AdminFinanceAgreementVersionsPageTest agreementforFranchise = new AdminFinanceAgreementVersionsPageTest();

			String agreementVersion = agreementforFranchise.addAgreement(driver, config, agrmntVrsnName, royaltyPrcntg,
					advPercentage, royaltyAreaFranchiseValue, advAreaFranchiseValue);

			AdminFinanceAgreementVersionsPage agreementVersion_page = new AdminFinanceAgreementVersionsPage(driver);

			fc.utobj().printTestStep("Map With Agreement Version");

			boolean recordNotPresent1 = new AdminFinanceAgreementVersionsPageTest().filterRecordByPaging(driver,
					new AdminFinanceAgreementVersionsPage(driver), agrmntVrsnName);

			if (recordNotPresent1 == false) {
				fc.utobj().throwsException("Agreement Version is not present");
			}

			fc.utobj().actionImgOption(driver, agreementVersion, "View / Map Franchises");

			fc.utobj().selectDropDown(driver, agreementVersion_page.searchFranID, franchiseId);

			fc.utobj().clickElement(driver, agreementVersion_page.checkAllSFAV);

			fc.utobj().clickElement(driver, agreementVersion_page.mapFranchise);

			fc.utobj().printTestStep("Navigate To Admin > Finance > Enable / Disable Area Franchise(s) for EFT");

			fc.finance().finance_common().adminFinEnableDisableAreaFranchiseforEFTLnk(driver);

			fc.utobj().moveToElement(driver, pobj.areaID);

			fc.utobj().selectDropDownByValue(driver, pobj.areaID,
					driver.findElement(By.xpath(".//*[@id='areaID']/option[contains(text () , '" + regionName + "')]"))
							.getAttribute("value"));

			fc.utobj().clickElement(driver, pobj.searchBtn);

			fc.utobj().printTestStep("Verify EFT Configuration Status");

			boolean isEFTConfigurationP = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () , '" + regionName + "')]/ancestor::tr/td[contains(text () , 'Pending')]");

			if (isEFTConfigurationP == false) {

				fc.utobj().throwsException("was not able to verify EFT Configureation Status");
			}

			boolean isEnableIspresentP = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () , '" + regionName + "')]/ancestor::tr/td[contains(text () , 'Disabled')]");

			if (isEnableIspresentP == false) {

				fc.utobj().throwsException("was not able to verify Disable Franchise(s) for EFT");
			}

			fc.utobj().printTestStep("Configure EFT For Franchise");

			fc.utobj().actionImgOption(driver, regionName, "Configure");

			fc.utobj().sendKeys(driver, pobj.selectTransactionCodeArea, "Checking");

			fc.utobj().sendKeys(driver, pobj.receivingDFIIdentificationArea, "987987987");

			fc.utobj().sendKeys(driver, pobj.DFIAccountNumberArea, "922130");

			fc.utobj().sendKeys(driver, pobj.individualIdentificationNumberArea, "EFTAutomation");

			fc.utobj().sendKeys(driver, pobj.individualNameArea, "TestFran");

			fc.utobj().clickElement(driver, pobj.checkbxEnableFranchiseForEFTTransaction);

			fc.utobj().clickElement(driver, pobj.submitBtn);

			fc.utobj().clickElement(driver, pobj.okBtn);

			fc.utobj().moveToElement(driver, pobj.areaID);

			fc.utobj().selectDropDownByValue(driver, pobj.areaID,
					driver.findElement(By.xpath(".//*[@id='areaID']/option[contains(text () , '" + regionName + "')]"))
							.getAttribute("value"));

			fc.utobj().clickElement(driver, pobj.searchBtn);

			fc.utobj().printTestStep("Verify The Enable Franchise For EFT");

			boolean isEFTConfiguration = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () , '" + regionName + "')]/ancestor::tr/td[contains(text () , 'Complete')]");

			if (isEFTConfiguration == false) {

				fc.utobj().throwsException("was not able to verify EFT Configureation");
			}

			boolean isEnableIspresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () , '" + regionName + "')]/ancestor::tr/td[contains(text () , 'Enabled')]");

			if (isEnableIspresent == false) {
				fc.utobj().throwsException("was not able to verify EFT Configureation Status");
			}

			fc.utobj().printTestStep("Disable Franchise(s) for EFT");

			fc.utobj().actionImgOption(driver, regionName, "Disable");

			fc.utobj().clickElement(driver, pobj.okayBtn);

			fc.utobj().moveToElement(driver, pobj.areaID);

			fc.utobj().selectDropDownByValue(driver, pobj.areaID,
					driver.findElement(By.xpath(".//*[@id='areaID']/option[contains(text () , '" + regionName + "')]"))
							.getAttribute("value"));

			fc.utobj().clickElement(driver, pobj.searchBtn);

			fc.utobj().printTestStep("Verify Disable Franchise(s) for EFT");

			boolean isEFTConfigurationD = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () , '" + regionName + "')]/ancestor::tr/td[contains(text () , 'Complete')]");

			if (isEFTConfigurationD == false) {
				fc.utobj().throwsException("was not able to verify Disable Franchise(s) for EFT");
			}

			boolean isEnableIspresentD = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () , '" + regionName + "')]/ancestor::tr/td[contains(text () , 'Disabled')]");

			if (isEnableIspresentD == false) {

				fc.utobj().throwsException("was not able to verify EFT Configureation Status");
			}

			fc.utobj().printTestStep("Enable Franchise(s) for EFT");

			fc.utobj().actionImgOption(driver, regionName, "Enable");

			fc.utobj().clickElement(driver, pobj.okayBtn);

			fc.utobj().moveToElement(driver, pobj.areaID);

			fc.utobj().selectDropDownByValue(driver, pobj.areaID,
					driver.findElement(By.xpath(".//*[@id='areaID']/option[contains(text () , '" + regionName + "')]"))
							.getAttribute("value"));

			fc.utobj().clickElement(driver, pobj.searchBtn);

			fc.utobj().printTestStep("Verify Enable Franchise(s) for EFT");

			boolean isEFTConfigurationE = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () , '" + regionName + "')]/ancestor::tr/td[contains(text () , 'Complete')]");

			if (isEFTConfigurationE == false) {
				fc.utobj().throwsException("was not able to verify Disable Franchise(s) for EFT");
			}

			boolean isEnableIspresentE = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () , '" + regionName + "')]/ancestor::tr/td[contains(text () , 'Enable')]");

			if (isEnableIspresentE == false) {
				fc.utobj().throwsException("was not able to verify EFT Configureation Status");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "finance" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "TC_Enable_Area_Franchise_EFT_01", testCaseDescription = "Verify Configure And Enable Area Franchise for EFT By Bottom Button")
	public void enableFranchiseforEFTByBottomButton() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("finance", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		String agrmntVrsnName = fc.utobj().generateTestData(dataSet.get("agreementVersionName"));

		String royaltyPrcntg = dataSet.get("royaltyPercentage");
		String advPercentage = dataSet.get("advertisementPercentage");
		String royaltyAreaFranchiseValue = dataSet.get("RoyaltyAreaFranchise");
		String advAreaFranchiseValue = dataSet.get("AdvAreaFranchise");

		try {
			driver = fc.loginpage().login(driver);
			AdminFinanceEnableDisableFranchiseforEFTPage pobj = new AdminFinanceEnableDisableFranchiseforEFTPage(
					driver);

			fc.utobj().printTestStep("Navigate To Admin > Franchise Location > Add Franchise Location");
			fc.utobj().printTestStep("Add Franchise Location");

			String regionName = fc.utobj().generateTestData("Test");
			String storeType = fc.utobj().generateTestData("Test");
			String franchiseId = fc.utobj().generateTestData("Test");
			AdminFranchiseLocationAddFranchiseLocationPageTest franchise = new AdminFranchiseLocationAddFranchiseLocationPageTest();
			franchiseId = franchise.addFranchiseLocation_All(driver, franchiseId, regionName, storeType);

			fc.utobj().printTestStep("Navigate To Admin > Finance >  Agreement Versions");
			fc.utobj().printTestStep("Add Agreement");
			AdminFinanceAgreementVersionsPageTest agreementforFranchise = new AdminFinanceAgreementVersionsPageTest();
			String agreementVersion = agreementforFranchise.addAgreement(driver, config, agrmntVrsnName, royaltyPrcntg,
					advPercentage, royaltyAreaFranchiseValue, advAreaFranchiseValue);

			// map with

			AdminFinanceAgreementVersionsPage agreementVersion_page = new AdminFinanceAgreementVersionsPage(driver);
			fc.utobj().printTestStep("Map With Agreement Version");

			boolean recordNotPresent1 = new AdminFinanceAgreementVersionsPageTest().filterRecordByPaging(driver,
					new AdminFinanceAgreementVersionsPage(driver), agrmntVrsnName);
			if (recordNotPresent1 == false) {
				fc.utobj().throwsException("Agreement Version is not present");
			}

			fc.utobj().actionImgOption(driver, agreementVersion, "View / Map Franchises");
			fc.utobj().selectDropDown(driver, agreementVersion_page.searchFranID, franchiseId);

			fc.utobj().clickElement(driver, agreementVersion_page.checkAllSFAV);
			fc.utobj().clickElement(driver, agreementVersion_page.mapFranchise);

			fc.utobj().printTestStep("Navigate To Admin > Finance > Enable / Disable Franchise(s) for EFT");
			fc.finance().finance_common().adminFinEnableDisableAreaFranchiseforEFTLnk(driver);

			searchByAreaRegion(driver, pobj, regionName);

			fc.utobj().printTestStep("Verify EFT Configuration Status");

			boolean isEFTConfigurationP = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () , '" + regionName + "')]/ancestor::tr/td[contains(text () , 'Pending')]");

			if (isEFTConfigurationP == false) {
				fc.utobj().throwsException("was not able to verify Disable Franchise(s) for EFT");
			}

			boolean isEnableIspresentP = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () , '" + regionName + "')]/ancestor::tr/td[contains(text () , 'Disabled')]");

			if (isEnableIspresentP == false) {
				fc.utobj().throwsException("was not able to verify EFT Configuration Status");
			}

			fc.utobj().printTestStep("Configure EFT For Franchise");

			fc.utobj().actionImgOption(driver, regionName, "Configure");

			fc.utobj().sendKeys(driver, pobj.selectTransactionCodeArea, "Checking");

			fc.utobj().sendKeys(driver, pobj.receivingDFIIdentificationArea, "987987987");

			fc.utobj().sendKeys(driver, pobj.DFIAccountNumberArea, "224570");

			fc.utobj().sendKeys(driver, pobj.individualIdentificationNumberArea, "EFTAutomation");

			fc.utobj().sendKeys(driver, pobj.individualNameArea, "TestFran");

			fc.utobj().clickElement(driver, pobj.checkbxEnableFranchiseForEFTTransaction);

			fc.utobj().clickElement(driver, pobj.submitBtn);

			fc.utobj().clickElement(driver, pobj.okayBtn);

			fc.utobj().printTestStep("Verify The Configure Enable Franchise For EFT");

			fc.utobj().printTestStep("Filter EFT Configuration By Area Region");

			searchByAreaRegion(driver, pobj, regionName);

			boolean isEFTConfiguration = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () , '" + regionName + "')]/ancestor::tr/td[contains(text () , 'Complete')]");

			if (isEFTConfiguration == false) {

				fc.utobj().throwsException("was not able to verify EFT Configuration");
			}

			boolean isEnableIspresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () , '" + regionName + "')]/ancestor::tr/td[contains(text () , 'Enabled')]");

			if (isEnableIspresent == false) {

				fc.utobj().throwsException("was not able to verify EFT Configuration Status");
			}

			fc.utobj().printTestStep("Disable Franchise(s) for EFT");

			searchByAreaRegion(driver, pobj, regionName);

			fc.utobj().clickElement(driver, driver.findElement(By.xpath(
					".//*[contains(text () , '" + regionName + "')]/ancestor::tr/td/input[@name = 'selectedItem']")));
			fc.utobj().clickElement(driver, pobj.disableBottomBtn);

			fc.utobj().clickElement(driver, pobj.okayBtn);

			searchByAreaRegion(driver, pobj, regionName);

			fc.utobj().printTestStep("Verify Disable Franchise(s) for EFT");

			boolean isEFTConfigurationD = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () , '" + regionName + "')]/ancestor::tr/td[contains(text () , 'Complete')]");

			if (isEFTConfigurationD == false) {

				fc.utobj().throwsException("was not able to verify EFT Configuration Status");
			}

			boolean isEnableIspresentD = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () , '" + regionName + "')]/ancestor::tr/td[contains(text () , 'Disabled')]");

			if (isEnableIspresentD == false) {

				fc.utobj().throwsException("was not able to verify Disable Franchise(s) for EFT");
			}

			fc.utobj().printTestStep("Enable Franchise(s) for EFT");

			fc.utobj().clickElement(driver, driver.findElement(By.xpath(
					".//*[contains(text () , '" + regionName + "')]/ancestor::tr/td/input[@name = 'selectedItem']")));
			fc.utobj().clickElement(driver, pobj.enableBottomBtn);

			fc.utobj().clickElement(driver, pobj.okayBtn);

			searchByAreaRegion(driver, pobj, regionName);

			fc.utobj().printTestStep("Verify Enable Franchise(s) for EFT");

			boolean isEFTConfigurationE = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () , '" + regionName + "')]/ancestor::tr/td[contains(text () , 'Complete')]");

			if (isEFTConfigurationE == false) {

				fc.utobj().throwsException("was not able to verify EFT Configuration Status");
			}

			boolean isEnableIspresentE = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () , '" + regionName + "')]/ancestor::tr/td[contains(text () , 'Enabled')]");

			if (isEnableIspresentE == false) {

				fc.utobj().throwsException("was not able to Verify Enable Franchise(s) for EFT");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	public void searchByAreaRegion(WebDriver driver, AdminFinanceEnableDisableFranchiseforEFTPage pobj,
			String regionName) throws Exception {

		fc.utobj().moveToElement(driver, pobj.areaID);
		fc.utobj().selectDropDownByValue(driver, pobj.areaID,
				driver.findElement(By.xpath(".//*[@id='areaID']/option[contains(text () , '" + regionName + "')]"))
						.getAttribute("value"));
		fc.utobj().clickElement(driver, pobj.searchBtn);
	}

	@Test(groups = { "finance", "finance_FailedTC" })
	@TestCase(createdOn = "2018-05-18", updatedOn = "2018-05-18", testCaseId = "TC_Enable_Area_Franchise_EFT_2", testCaseDescription = "Verify Enable/Disable Area Franchise Link for EFT")
	public void verifyEnableDisableFranchiseforEFT() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			AdminFinanceEnableDisableFranchiseforEFTPage pobj = new AdminFinanceEnableDisableFranchiseforEFTPage(
					driver);
			Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);

			Map<String, String> dataSet = fc.utobj().readTestData("finance", testCaseId);

			AdminFinanceFinanceSetupPreferencesPageTest setUpPage = new AdminFinanceFinanceSetupPreferencesPageTest();

			String enableAFShare = "No", payFreqAFShare = "Weekly", EFTDayDateForAFShare = "Monday";

			setUpPage.AFSharePayments(driver, config, enableAFShare, payFreqAFShare, EFTDayDateForAFShare);

			fc.utobj().printTestStep("Go to Admin > Finance");

			driver.findElement(By.xpath(".//div[@id='dropdown']//span[contains(text(), 'FranConnect Administrator')]"))
					.click();

			driver.findElement(By.xpath(".//div[@id='dropdown']//a[contains(text(), 'Admin')]")).click();

			driver.findElement(By.xpath(".//*[@id='section1']/ul/li[14]/a/span")).click();

			AdminPage pobjAdmin = new AdminPage(driver);

			boolean isEnableDisableLinkPresent = fc.utobj().isElementPresent(driver,
					pobjAdmin.enableDisableAreaFranchiseforEFTLnk);

			if (isEnableDisableLinkPresent == true) {

				fc.utobj().throwsException(
						"Enable/disable Area franchise link is present even AF Share is disabled from admin");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "finance" })
	@TestCase(createdOn = "2018-05-18", updatedOn = "2018-05-18", testCaseId = "TC_Enable_Area_Franchise_EFT_3", testCaseDescription = "Verify Reset Functionality for Enable Area Franchise for EFT")
	public void resetFunctionalityforAreaFranchiseforEFT() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);

		Map<String, String> dataSet = fc.utobj().readTestData("finance", testCaseId);

		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		String agrmntVrsnName = fc.utobj().generateTestData("AV");

		String royaltyPrcntg = "10";
		String advPercentage = "8";
		String royaltyAreaFranchiseValue = "5";
		String advAreaFranchiseValue = "2";
		String transactionType = "checking";
		String recDFIId = "987987987";
		String DFIActNum = "224570";

		try {
			driver = fc.loginpage().login(driver);
			AdminFinanceEnableDisableFranchiseforEFTPage pobj = new AdminFinanceEnableDisableFranchiseforEFTPage(
					driver);

			fc.utobj().printTestStep("Navigate To Admin > Franchise Location > Add Franchise Location");

			fc.utobj().printTestStep("Add Franchise Location");

			String regionName = fc.utobj().generateTestData("GT");

			String storeType = fc.utobj().generateTestData("GT");

			String franchiseId = fc.utobj().generateTestData("GT");

			AdminFranchiseLocationAddFranchiseLocationPageTest franchise = new AdminFranchiseLocationAddFranchiseLocationPageTest();

			franchiseId = franchise.addFranchiseLocation_All(driver, franchiseId, regionName, storeType);

			fc.utobj().printTestStep("Navigate To Admin > Finance >  Agreement Versions");

			fc.utobj().printTestStep("Add Agreement");

			AdminFinanceAgreementVersionsPageTest agreementforFranchise = new AdminFinanceAgreementVersionsPageTest();

			String agreementVersion = agreementforFranchise.addAgreement(driver, config, agrmntVrsnName, royaltyPrcntg,
					advPercentage, royaltyAreaFranchiseValue, advAreaFranchiseValue);

			AdminFinanceAgreementVersionsPage agreementVersion_page = new AdminFinanceAgreementVersionsPage(driver);

			fc.utobj().printTestStep("Map With Agreement Version");

			boolean recordNotPresent1 = new AdminFinanceAgreementVersionsPageTest().filterRecordByPaging(driver,
					new AdminFinanceAgreementVersionsPage(driver), agrmntVrsnName);

			if (recordNotPresent1 == false) {

				fc.utobj().throwsException("Agreement Version is not present");
			}

			fc.utobj().actionImgOption(driver, agreementVersion, "View / Map Franchises");

			fc.utobj().selectDropDown(driver, agreementVersion_page.searchFranID, franchiseId);

			fc.utobj().clickElement(driver, agreementVersion_page.checkAllSFAV);

			fc.utobj().clickElement(driver, agreementVersion_page.mapFranchise);

			fc.utobj().printTestStep("Navigate To Admin > Finance > Enable / Disable Area Franchise(s) for EFT");

			fc.finance().finance_common().adminFinEnableDisableAreaFranchiseforEFTLnk(driver);

			fc.utobj().selectDropDownByValue(driver, pobj.areaID,
					driver.findElement(By.xpath(".//*[@id='areaID']/option[contains(text () , '" + regionName + "')]"))
							.getAttribute("value"));

			fc.utobj().clickElement(driver, pobj.resetButton);

			String areaIdSelectedValue = fc.utobj().getSelectedDropDownValue(driver, pobj.areaID);

			if (!areaIdSelectedValue.equalsIgnoreCase("Select")) {

				fc.utobj().throwsException("reset button functionality is not working");
			}

			fc.utobj().selectDropDownByValue(driver, pobj.areaID,
					driver.findElement(By.xpath(".//*[@id='areaID']/option[contains(text () , '" + regionName + "')]"))
							.getAttribute("value"));

			fc.utobj().printTestStep("Configure EFT For Franchise");

			fc.utobj().actionImgOption(driver, regionName, "Configure");

			EFTConfigurationForArea(driver, pobj, transactionType, recDFIId, DFIActNum);

			fc.utobj().printTestStep("Verify The Configure Enable Franchise For EFT");

			fc.utobj().printTestStep("Filter EFT Configuration By Area Region");

			searchByAreaRegion(driver, pobj, regionName);

			boolean isEFTConfiguration = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () , '" + regionName + "')]/ancestor::tr/td[contains(text () , 'Complete')]");

			if (isEFTConfiguration == false) {

				fc.utobj().throwsException("was not able to verify EFT Configuration");
			}

			boolean isEnableIspresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () , '" + regionName + "')]/ancestor::tr/td[contains(text () , 'Enabled')]");

			if (isEnableIspresent == false) {

				fc.utobj().throwsException("was not able to verify EFT Configuration Status");
			}

			fc.utobj().printTestStep("Modify EFT Configuration For Franchise");

			fc.utobj().actionImgOption(driver, regionName, "Configure");

			transactionType = "Savings";

			recDFIId = "123456789";

			DFIActNum = "123456";

			EFTConfigurationForArea(driver, pobj, transactionType, recDFIId, DFIActNum);

			fc.utobj().printTestStep("Verify The Configure Enable Franchise For EFT");

			fc.utobj().printTestStep("Filter EFT Configuration By Area Region");

			searchByAreaRegion(driver, pobj, regionName);

			boolean isEFTConfiguration1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () , '" + regionName + "')]/ancestor::tr/td[contains(text () , 'Complete')]");

			if (isEFTConfiguration1 == false) {

				fc.utobj().throwsException("was not able to verify modified EFT Configuration");
			}

			boolean isEnableIspresent1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () , '" + regionName + "')]/ancestor::tr/td[contains(text () , 'Enabled')]");

			if (isEnableIspresent1 == false) {

				fc.utobj().throwsException("was not able to verify modified EFT Configuration Status");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	public void EFTConfigurationForArea(WebDriver driver, AdminFinanceEnableDisableFranchiseforEFTPage pobj,
			String transactionType, String recDFIId, String DFIActNum) throws Exception {

		fc.utobj().sendKeys(driver, pobj.selectTransactionCodeArea, transactionType);

		fc.utobj().sendKeys(driver, pobj.receivingDFIIdentificationArea, recDFIId);

		fc.utobj().sendKeys(driver, pobj.DFIAccountNumberArea, DFIActNum);

		fc.utobj().sendKeys(driver, pobj.individualIdentificationNumberArea, "EFTAutomation");

		fc.utobj().sendKeys(driver, pobj.individualNameArea, "TestFran");

		if (!fc.utobj().isSelected(driver, pobj.checkbxEnableFranchiseForEFTTransaction)) {

			fc.utobj().clickElement(driver, pobj.checkbxEnableFranchiseForEFTTransaction);

		}

		fc.utobj().clickElement(driver, pobj.submitBtn);

		fc.utobj().clickElement(driver, pobj.okBtn);

	}

}
