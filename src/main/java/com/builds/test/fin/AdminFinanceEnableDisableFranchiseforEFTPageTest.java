package com.builds.test.fin;

import java.util.Map;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.Test;
import com.builds.test.admin.AdminFranchiseLocationAddFranchiseLocationPageTest;
import com.builds.uimaps.admin.AdminPage;
import com.builds.uimaps.fin.AdminFinanceAgreementVersionsPage;
import com.builds.uimaps.fin.AdminFinanceEnableDisableFranchiseforEFTPage;
import com.builds.utilities.FranconnectUtil;
import com.builds.utilities.TestCase;

public class AdminFinanceEnableDisableFranchiseforEFTPageTest {
	FranconnectUtil fc = new FranconnectUtil();

	@Test(groups = "finance")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-05-17", testCaseId = "TC_Enable_Franchise_EFT", testCaseDescription = "Verify Configure And Enable Franchise for EFT")
	public void enableFranchiseforEFT() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("finance", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		String agrmntVrsnName = fc.utobj().generateTestData(dataSet.get("agreementVersionName"));
		String royaltyPrcntg = dataSet.get("royaltyPercentage");
		String advPercentage = dataSet.get("advertisementPercentage");
		String royaltyAreaFranchiseValue = dataSet.get("RoyaltyAreaFranchise");
		String advAreaFranchiseValue = dataSet.get("AdvAreaFranchise");

		try {
			driver = fc.loginpage().login(driver);
			AdminFinanceEnableDisableFranchiseforEFTPage pobj = new AdminFinanceEnableDisableFranchiseforEFTPage(
					driver);

			fc.utobj().printTestStep("Navigate To Admin > Franchise Location > Add Franchise Location");

			fc.utobj().printTestStep("Add Franchise Location");

			String regionName = fc.utobj().generateTestData("GT");

			String storeType = fc.utobj().generateTestData("GT");

			String franchiseId = fc.utobj().generateTestData("GT");

			AdminFranchiseLocationAddFranchiseLocationPageTest franchise = new AdminFranchiseLocationAddFranchiseLocationPageTest();

			franchiseId = franchise.addFranchiseLocation_All(driver, franchiseId, regionName, storeType);

			fc.utobj().printTestStep("Navigate To Admin > Finance >  Agreement Versions");

			fc.utobj().printTestStep("Add Agreement");

			AdminFinanceAgreementVersionsPageTest agreementforFranchise = new AdminFinanceAgreementVersionsPageTest();

			String agreementVersion = agreementforFranchise.addAgreement(driver, config, agrmntVrsnName,
					  royaltyPrcntg, advPercentage, royaltyAreaFranchiseValue,
					advAreaFranchiseValue);

			// map with
			AdminFinanceAgreementVersionsPage agreementVersion_page = new AdminFinanceAgreementVersionsPage(driver);

			fc.utobj().printTestStep("Map With Agreement Version");

			boolean recordNotPresent = new AdminFinanceAgreementVersionsPageTest().filterRecordByPaging(driver,
					new AdminFinanceAgreementVersionsPage(driver), agrmntVrsnName);

			if (recordNotPresent == false) {

				fc.utobj().throwsException("Agreement Version is not present");
			}

			fc.utobj().actionImgOption(driver, agreementVersion, "View / Map Franchises");

			fc.utobj().selectDropDown(driver, agreementVersion_page.searchFranID, franchiseId);

			fc.utobj().clickElement(driver, agreementVersion_page.checkAllSFAV);

			fc.utobj().clickElement(driver, agreementVersion_page.mapFranchise);

			fc.utobj().printTestStep("Navigate To Admin > Finance > Enable / Disable Franchise(s) for EFT");

			fc.finance().finance_common().adminFinEnableDisableFranchiseforEFTLnk(driver);

			fc.utobj().moveToElement(driver, pobj.selectfranchiseID);

			fc.utobj().selectDropDownByValue(driver, pobj.selectfranchiseID,
					driver.findElement(
							By.xpath(".//*[@id='franchiseID']/option[contains(text () , '" + franchiseId + "')]"))
							.getAttribute("value"));

			fc.utobj().clickElement(driver, pobj.searchBtn);

			fc.utobj().printTestStep("Verify EFT Configuration Status");

			boolean isEFTConfigurationP = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () , '" + franchiseId + "')]/ancestor::tr/td[contains(text () , 'Pending')]");

			if (isEFTConfigurationP == false) {

				fc.utobj().throwsException("was not able to verify EFT Configuration Status");
			}

			boolean isEnableIspresentP = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () , '" + franchiseId + "')]/ancestor::tr/td[contains(text () , 'Disabled')]");

			if (isEnableIspresentP == false) {

				fc.utobj().throwsException("was not able to verify Disable Franchise(s) for EFT");
			}

			fc.utobj().printTestStep("Configure EFT For Franchise");

			fc.utobj().actionImgOption(driver, franchiseId, "Configure");

			fc.utobj().selectDropDown(driver, pobj.selectTransactionCode, "Checking");

			fc.utobj().sendKeys(driver, pobj.receivingDFIIdentification, "987987987");

			fc.utobj().sendKeys(driver, pobj.DFIAccountNumber, "922130");

			fc.utobj().sendKeys(driver, pobj.individualIdentificationNumber, "EFTAutomation");

			fc.utobj().sendKeys(driver, pobj.individualName, "TestFran");

			fc.utobj().clickElement(driver, pobj.checkbxEnableFranchiseForEFTTransaction);

			fc.utobj().clickElement(driver, pobj.submitBtn);

			fc.utobj().clickElement(driver, pobj.okBtn);

			fc.utobj().moveToElement(driver, pobj.selectfranchiseID);

			fc.utobj().selectDropDownByValue(driver, pobj.selectfranchiseID,
					driver.findElement(
							By.xpath(".//*[@id='franchiseID']/option[contains(text () , '" + franchiseId + "')]"))
							.getAttribute("value"));

			fc.utobj().clickElement(driver, pobj.searchBtn);

			fc.utobj().printTestStep("Verify EFT Configuration Status of Franchise under EFT Configuration Column");

			boolean isEFTConfiguration = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () , '" + franchiseId + "')]/ancestor::tr/td[contains(text () , 'Complete')]");

			if (isEFTConfiguration == false) {

				fc.utobj().throwsException("was not able to verify EFT Configureation column value");
			}

			fc.utobj().printTestStep("Verify Enabled/Disable Column value of Franchise under Enable/Disable Column");

			boolean isEnableIspresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () , '" + franchiseId + "')]/ancestor::tr/td[contains(text () , 'Enabled')]");

			if (isEnableIspresent == false) {

				fc.utobj().throwsException("was not able to verify Enable/Disable Column value");
			}

			fc.utobj().printTestStep("Modify EFT Configuration for Already EFT configured Franchise");
			
			fc.utobj().actionImgOption(driver, franchiseId, "Configure");

			fc.utobj().selectDropDown(driver, pobj.selectTransactionCode, "Savings");

			fc.utobj().sendKeys(driver, pobj.receivingDFIIdentification, "123456789");

			fc.utobj().sendKeys(driver, pobj.DFIAccountNumber, "130922");

			fc.utobj().clickElement(driver, pobj.submitBtn);

			fc.utobj().clickElement(driver, pobj.okayBtn);
		
			fc.utobj().printTestStep("Disable Franchise(s) for EFT");

			fc.utobj().actionImgOption(driver, franchiseId, "Disable");

			fc.utobj().printTestStep("Verify Disable Option in action menu for Franchise for which EFT is enabled");

			fc.utobj().clickElement(driver, pobj.okayBtn);

			fc.utobj().moveToElement(driver, pobj.selectfranchiseID);

			fc.utobj().selectDropDownByValue(driver, pobj.selectfranchiseID,
					driver.findElement(
							By.xpath(".//*[@id='franchiseID']/option[contains(text () , '" + franchiseId + "')]"))
							.getAttribute("value"));

			fc.utobj().clickElement(driver, pobj.searchBtn);

			fc.utobj().printTestStep("Verify Disabled Franchise(s) for EFT");

			boolean isEFTConfigurationD = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () , '" + franchiseId + "')]/ancestor::tr/td[contains(text () , 'Complete')]");

			if (isEFTConfigurationD == false) {

				fc.utobj().throwsException(
						"was not able to verify EFT Configureation Status after Disabling it from EFT");
			}

			boolean isEnableIspresentD = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () , '" + franchiseId + "')]/ancestor::tr/td[contains(text () , 'Disabled')]");

			if (isEnableIspresentD == false) {

				fc.utobj().throwsException(
						"was not able to verify Enable/Disable Column value of Franchise(s) after disabling it from EFT");
			}

			fc.utobj()
					.printTestStep("Enable Franchise(s) for EFT when EFT Configuration is filled but EFT is disabled");

			fc.utobj().actionImgOption(driver, franchiseId, "Enable");

			fc.utobj().printTestStep(
					"Verify Enable Option in action menu for Franchise for which EFT is Disabled but configuration is completed");

			fc.utobj().clickElement(driver, pobj.okayBtn);

			fc.utobj().moveToElement(driver, pobj.selectfranchiseID);

			fc.utobj().selectDropDownByValue(driver, pobj.selectfranchiseID,
					driver.findElement(
							By.xpath(".//*[@id='franchiseID']/option[contains(text () , '" + franchiseId + "')]"))
							.getAttribute("value"));

			fc.utobj().clickElement(driver, pobj.searchBtn);

			fc.utobj().printTestStep(
					"Verify Enable Franchise(s) for EFT when configuration is completed but EFT is Disabled from action menu");

			boolean isEFTConfigurationE = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () , '" + franchiseId + "')]/ancestor::tr/td[contains(text () , 'Complete')]");

			if (isEFTConfigurationE == false) {

				fc.utobj().throwsException(
						"was not able to verify EFT Configureation Status after re-enabled it for EFT");
			}

			boolean isEnableIspresentE = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () , '" + franchiseId + "')]/ancestor::tr/td[contains(text () , 'Enabled')]");

			if (isEnableIspresentE == false) {

				fc.utobj().throwsException("was not able to verify Enable/Disable Status after re-enabled it for EFT");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "finance")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-05-17", testCaseId = "TC_Enable_Franchise_EFT_01", testCaseDescription = "Verify Configure And Enable Franchise for EFT By Bottom Button")
	public void enableFranchiseforEFTByBottomButton() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("finance", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		String agrmntVrsnName = fc.utobj().generateTestData(dataSet.get("agreementVersionName"));
		String agrmntStartDate = fc.utobj().getFutureDateUSFormat(-100);
		String agrmntEndDate = fc.utobj().getFutureDateUSFormat(200);
		String royaltyPrcntg = dataSet.get("royaltyPercentage");
		String advPercentage = dataSet.get("advertisementPercentage");
		String royaltyAreaFranchiseValue = dataSet.get("RoyaltyAreaFranchise");
		String advAreaFranchiseValue = dataSet.get("AdvAreaFranchise");

		try {
			driver = fc.loginpage().login(driver);
			AdminFinanceEnableDisableFranchiseforEFTPage pobj = new AdminFinanceEnableDisableFranchiseforEFTPage(
					driver);

			fc.utobj().printTestStep("Navigate To Admin > Franchise Location > Add Franchise Location");

			fc.utobj().printTestStep("Add Franchise Location");

			String regionName = fc.utobj().generateTestData("GT");

			String storeType = fc.utobj().generateTestData("GT");

			String franchiseId = fc.utobj().generateTestData("GT");

			AdminFranchiseLocationAddFranchiseLocationPageTest franchise = new AdminFranchiseLocationAddFranchiseLocationPageTest();

			franchiseId = franchise.addFranchiseLocation_All(driver, franchiseId, regionName, storeType);

			fc.utobj().printTestStep("Navigate To Admin > Finance >  Agreement Versions");

			fc.utobj().printTestStep("Add Agreement");

			AdminFinanceAgreementVersionsPageTest agreementforFranchise = new AdminFinanceAgreementVersionsPageTest();

			String agreementVersion = agreementforFranchise.addAgreement(driver, config, agrmntVrsnName,
					  royaltyPrcntg, advPercentage, royaltyAreaFranchiseValue,
					advAreaFranchiseValue);

			// map with

			AdminFinanceAgreementVersionsPage agreementVersion_page = new AdminFinanceAgreementVersionsPage(driver);

			fc.utobj().printTestStep("Map With Agreement Version");

			boolean recordNotPresent = new AdminFinanceAgreementVersionsPageTest().filterRecordByPaging(driver,
					new AdminFinanceAgreementVersionsPage(driver), agrmntVrsnName);

			if (recordNotPresent == false) {

				fc.utobj().throwsException("Agreement Version is not present");
			}

			fc.utobj().actionImgOption(driver, agreementVersion, "View / Map Franchises");

			fc.utobj().selectDropDown(driver, agreementVersion_page.searchFranID, franchiseId);

			fc.utobj().clickElement(driver, agreementVersion_page.checkAllSFAV);

			fc.utobj().clickElement(driver, agreementVersion_page.mapFranchise);

			fc.utobj().printTestStep("Navigate To Admin > Finance > Enable / Disable Franchise(s) for EFT");

			fc.finance().finance_common().adminFinEnableDisableFranchiseforEFTLnk(driver);

			searchByFranchiseId(driver, pobj, franchiseId);

			fc.utobj().printTestStep("Verify EFT Configuration Status");

			boolean isEFTConfigurationP = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () , '" + franchiseId + "')]/ancestor::tr/td[contains(text () , 'Pending')]");

			if (isEFTConfigurationP == false) {
				
				fc.utobj().throwsException("was not able to verify EFT Configuration Status");
			}

			boolean isEnableIspresentP = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () , '" + franchiseId + "')]/ancestor::tr/td[contains(text () , 'Disabled')]");

			if (isEnableIspresentP == false) {

				fc.utobj().throwsException("was not able to verify Enable/Disable Column value for Franchise(s)");
			}

			fc.utobj().printTestStep("Configure EFT For Franchise");

			fc.utobj().actionImgOption(driver, franchiseId, "Configure");

			fc.utobj().selectDropDown(driver, pobj.selectTransactionCode, "Checking");

			fc.utobj().sendKeys(driver, pobj.receivingDFIIdentification, "987987987");

			fc.utobj().sendKeys(driver, pobj.DFIAccountNumber, "922130");

			fc.utobj().sendKeys(driver, pobj.individualIdentificationNumber, "EFTAutomation");

			fc.utobj().sendKeys(driver, pobj.individualName, "TestFran");

			fc.utobj().clickElement(driver, pobj.checkbxEnableFranchiseForEFTTransaction);

			fc.utobj().clickElement(driver, pobj.submitBtn);

			fc.utobj().clickElement(driver, pobj.okayBtn);

			fc.utobj().printTestStep("Verify Configuration Enable Franchise For EFT");

			searchByFranchiseId(driver, pobj, franchiseId);

			boolean isEFTConfiguration = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () , '" + franchiseId + "')]/ancestor::tr/td[contains(text () , 'Complete')]");

			if (isEFTConfiguration == false) {

				fc.utobj().throwsException("was not able to verify EFT Configuration Status after EFT Configuration");
			}

			boolean isEnableIspresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () , '" + franchiseId + "')]/ancestor::tr/td[contains(text () , 'Enabled')]");

			if (isEnableIspresent == false) {

				fc.utobj().throwsException("was not able to verify Enable/Disable Column Value after EFT configuration");
			}

			fc.utobj().printTestStep("Disable Franchise(s) for EFT");

			searchByFranchiseId(driver, pobj, franchiseId);

			fc.utobj().clickElement(driver, driver.findElement(By.xpath(
					".//*[contains(text () , '" + franchiseId + "')]/ancestor::tr/td/input[@name = 'selectedItem']")));

			fc.utobj().clickElement(driver, pobj.disableBottomBtn);

			fc.utobj().clickElement(driver, pobj.okayBtn);

			searchByFranchiseId(driver, pobj, franchiseId);

			fc.utobj().printTestStep("Verify Disable Franchise(s) for EFT");

			boolean isEFTConfigurationD = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () , '" + franchiseId + "')]/ancestor::tr/td[contains(text () , 'Complete')]");

			if (isEFTConfigurationD == false) {

				fc.utobj().throwsException("was not able to verify EFT Configuration Status");
			}

			boolean isEnableIspresentD = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () , '" + franchiseId + "')]/ancestor::tr/td[contains(text () , 'Disabled')]");

			if (isEnableIspresentD == false) {

				fc.utobj().throwsException("was not able to verify Disable Franchise(s) for EFT");
			}

			fc.utobj().printTestStep("Enable Franchise(s) for EFT");

			fc.utobj().clickElement(driver, driver.findElement(By.xpath(
					".//*[contains(text () , '" + franchiseId + "')]/ancestor::tr/td/input[@name = 'selectedItem']")));

			fc.utobj().clickElement(driver, pobj.enableBottomBtn);

			fc.utobj().clickElement(driver, pobj.okayBtn);

			searchByFranchiseId(driver, pobj, franchiseId);

			fc.utobj().printTestStep("Verify Enable Franchise(s) for EFT");

			boolean isEFTConfigurationE = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () , '" + franchiseId + "')]/ancestor::tr/td[contains(text () , 'Complete')]");

			if (isEFTConfigurationE == false) {

				fc.utobj().throwsException("was not able to verify EFT Configuration Status");
			}

			boolean isEnableIspresentE = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () , '" + franchiseId + "')]/ancestor::tr/td[contains(text () , 'Enabled')]");

			if (isEnableIspresentE == false) {

				fc.utobj().throwsException("was not able to Verify Enable Franchise(s) for EFT");
			}
		
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	public void searchByFranchiseId(WebDriver driver, AdminFinanceEnableDisableFranchiseforEFTPage pobj,
			String franchiseId) throws Exception {

		fc.utobj().moveToElement(driver, pobj.selectfranchiseID);

		fc.utobj().selectDropDownByValue(driver, pobj.selectfranchiseID,
				driver.findElement(
						By.xpath(".//*[@id='franchiseID']/option[contains(text () , '" + franchiseId + "')]"))
						.getAttribute("value"));

		fc.utobj().clickElement(driver, pobj.searchBtn);
	}
	
		
	@Test(groups = {"finance", "finance_FailedTC"} )
	@TestCase(createdOn = "2018-05-17", updatedOn = "2018-05-17", testCaseId = "TC_Enable_Franchise_EFT_2", testCaseDescription = "Verify Enable/Disable Franchise for EFT link if Recieve payments from franchise(s) through EFT is disabled")
	public void verifyEnableDisableFranchiseForEFTLink() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			AdminFinanceFinanceSetupPreferencesPageTest setUp = new AdminFinanceFinanceSetupPreferencesPageTest();
			
			String enableEFT="No";
			
			setUp.setReceivePaymentsFromFranchiseThroughEFT(driver, config, enableEFT);
			
			fc.utobj().printTestStep("Go to Admin > Finance");
			
			driver.findElement(By.xpath(".//div[@id='dropdown']//span[contains(text(), 'FranConnect Administrator')]")).click();
			
			driver.findElement(By.xpath(".//div[@id='dropdown']//a[contains(text(), 'Admin')]")).click();
			
			driver.findElement(By.xpath(".//*[@id='section1']/ul/li[14]/a/span")).click();
			
			AdminPage pobjAdmin = new AdminPage(driver);
			
			boolean isEnableDisableLinkPresent = fc.utobj().isElementPresent(driver, pobjAdmin.enableDisableFranchiseforEFTLnk);

			if (isEnableDisableLinkPresent == true) {

				fc.utobj().throwsException("Enable/disable a franchise for EFT link is present even EFT is disabled from admin");
			}
/*
			enableEFT="Yes";
			setUp.setReceivePaymentsFromFranchiseThroughEFT(driver, config, enableEFT);
			
			boolean isEnableDisableLinkPresent1 = fc.utobj().isElementPresent(driver, pobjAdmin.enableDisableFranchiseforEFTLnk);

			if (isEnableDisableLinkPresent1 == false) {

				fc.utobj().throwsException("Enable/disable a franchise for EFT link is not present when EFT is enabled from admin");
			}
			*/
			
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}
	
	
	
	
	
	
	
	
}
