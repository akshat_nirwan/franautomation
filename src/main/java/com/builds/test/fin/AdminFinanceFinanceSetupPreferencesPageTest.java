package com.builds.test.fin;

import static org.junit.Assert.assertEquals;
import java.util.Map;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Test;
import com.builds.test.admin.AdminFranchiseLocationAddFranchiseLocationPageTest;
import com.builds.uimaps.fin.AdminFinanceAgreementVersionsPage;
import com.builds.uimaps.fin.AdminFinanceDeleteSalesReportsPage;
import com.builds.uimaps.fin.AdminFinanceFinanceSetupPreferencesPage;
import com.builds.uimaps.fin.FinanceSalesPage;
import com.builds.utilities.FranconnectUtil;
import com.builds.utilities.TestCase;

public class AdminFinanceFinanceSetupPreferencesPageTest {
	FranconnectUtil fc = new FranconnectUtil();

	@Test(groups = "finance")
	@TestCase(createdOn = "2018-05-09", updatedOn = "2018-05-09", testCaseId = "TC_Admin_FIN_Setup_Medium_of_Sales", testCaseDescription = "To Verify medium of submission of sales report from admin")
	public void mediumOfSubmissionOfSalesReports() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());
		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);

		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			AdminFinanceFinanceSetupPreferencesPage pobj = new AdminFinanceFinanceSetupPreferencesPage(driver);

			fc.utobj().printTestStep(
					"Navigate To Admin > Finance > Sales Report Preferences and check Medium of submission of Sales Reports");

			String salesReportType = "csv";
			mediumOfSubmissionOfSalesReports(driver, config, salesReportType);

			boolean csvSelected = pobj.csvUploadFileType.isSelected();
			if (csvSelected == false) {
				fc.utobj().throwsException("CSV Medium of sales Submission is not selected");
			}

			salesReportType = "xls";
			mediumOfSubmissionOfSalesReports(driver, config, salesReportType);

			boolean xlsSelected = pobj.xlsUploadFileType.isSelected();
			if (xlsSelected == false) {
				fc.utobj().throwsException("XLS Medium of sales Submission is not selected");
			}

			salesReportType = "webform";
			mediumOfSubmissionOfSalesReports(driver, config, salesReportType);

			boolean webFormSelected = pobj.webFormType.isSelected();
			if (webFormSelected == false) {
				fc.utobj().throwsException("XLSWeb Form Medium of sales Submission is not selected");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "finance")
	@TestCase(createdOn = "2018-05-09", updatedOn = "2018-05-10", testCaseId = "TC_Admin_FIN_Setup_Medium_of_ProfitLoss", testCaseDescription = "To Verify medium of submission of Profit and Loss reports from admin")
	public void mediumOfSubmissionOfProfitAndLossReports() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());
		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("finance", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			AdminFinanceFinanceSetupPreferencesPage pobj = new AdminFinanceFinanceSetupPreferencesPage(driver);
			fc.utobj().printTestStep(
					"Navigate To Admin > Finance > Sales Report Preferences > Medium of Submission of Profit and Loss reports");

			String PLReportType = "csv";
			mediumOfSubmissionOfProfitAndLossReports(driver, config, PLReportType);

			boolean csvSelected = pobj.plcsvUploadFileType.isSelected();
			if (csvSelected == false) {
				fc.utobj().throwsException("CSV Medium of Profit and Loss Submission is not selected");
			}

			PLReportType = "xls";
			mediumOfSubmissionOfProfitAndLossReports(driver, config, PLReportType);

			boolean xlsSelected = pobj.plxlsUploadFileType.isSelected();
			if (xlsSelected == false) {
				fc.utobj().throwsException("XLS Medium of Profit and Loss Submission is not selected");
			}

			PLReportType = "QuickBooks";
			mediumOfSubmissionOfProfitAndLossReports(driver, config, PLReportType);

			boolean quickBookSelected = pobj.QuickBooks.isSelected();
			if (quickBookSelected == false) {
				fc.utobj().throwsException("Quick Books Medium of Profit and Loss Submission is not selected");
			}

			PLReportType = "webform";
			mediumOfSubmissionOfProfitAndLossReports(driver, config, PLReportType);

			boolean webFormSelected = pobj.plWebFormType.isSelected();
			if (webFormSelected == false) {
				fc.utobj().throwsException("Web Form Medium of Profit and Loss Submission is not selected");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "finance")
	@TestCase(createdOn = "2018-05-10", updatedOn = "2018-05-10", testCaseId = "TC_Admin_FIN_Setup_Royalty_Reporting_Frequency", testCaseDescription = "To Verify royalty reporting frequency in Admin > Finance > Finance Setup_Preferences")
	public void royaltyReportingFrequencies() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());
		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("finance", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			AdminFinanceFinanceSetupPreferencesPage pobj = new AdminFinanceFinanceSetupPreferencesPage(driver);
			fc.utobj().printTestStep(
					"Navigate To Admin > Finance > Sales Report Preferences > Royalty Reporting Frequencies");

			String rrFrequency = "Daily";
			royaltyReportingFrequency(driver, config, rrFrequency);

			boolean csvSelected = pobj.daily.isSelected();
			if (csvSelected == false) {
				fc.utobj().throwsException("Daily Royalty Reporting Frequency is not selected");
			}

			rrFrequency = "Weekly";
			royaltyReportingFrequency(driver, config, rrFrequency);

			boolean xlsSelected = pobj.weekly.isSelected();
			if (xlsSelected == false) {
				fc.utobj().throwsException("Weekly Royalty Reporting Frequency is not selected");
			}

			rrFrequency = "Every15Days";
			royaltyReportingFrequency(driver, config, rrFrequency);

			boolean quickBookSelected = pobj.every15Days.isSelected();
			if (quickBookSelected == false) {
				fc.utobj().throwsException("Every15Days Royalty Reporting Frequency is not selected");
			}

			rrFrequency = "Monthly";
			royaltyReportingFrequency(driver, config, rrFrequency);

			boolean webFormSelected = pobj.monthly.isSelected();
			if (webFormSelected == false) {
				fc.utobj().throwsException("Monthly Royalty Reporting Frequency is not selected");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "finance")
	@TestCase(createdOn = "2018-05-10", updatedOn = "2018-05-10", testCaseId = "TC_Admin_FIN_Royalty_Repo_Freq_After_AV", testCaseDescription = "To Verify royalty reporting frequency in Admin with agreement version")
	public void royaltyRepFrequencyWithAgreement() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());
		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("finance", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		String agrmntVrsnName = fc.utobj().generateTestData(dataSet.get("agreementVersionName"));
		String royaltyPrcntg = dataSet.get("royaltyPercentage");
		String advPercentage = dataSet.get("advertisementPercentage");
		String royaltyAreaFranchiseValue = dataSet.get("RoyaltyAreaFranchise");
		String advAreaFranchiseValue = dataSet.get("AdvAreaFranchise");

		try {
			driver = fc.loginpage().login(driver);
			AdminFinanceFinanceSetupPreferencesPage pobj = new AdminFinanceFinanceSetupPreferencesPage(driver);
			fc.utobj().printTestStep("Navigate To Admin > Finance > Agreement Version / Finance Setup Preferences");

			AdminFinanceAgreementVersionsPageTest addAgreementVersion = new AdminFinanceAgreementVersionsPageTest();

			addAgreementVersion.addAgreement(driver, config, agrmntVrsnName,  
					royaltyPrcntg, advPercentage, royaltyAreaFranchiseValue, advAreaFranchiseValue);

			String rrFrequency = "Monthly";

			fc.adminpage().openAdminFinanceSetupPreferencesLnk(driver);

			fc.utobj().clickElement(driver, pobj.royaltyReportFrequency);

			if (rrFrequency.equalsIgnoreCase("Monthly")) {
				boolean isEnabled = pobj.monthly.isEnabled();
				if (isEnabled == true) {
					fc.utobj().throwsException(" Monthly Frequency is enabled after using this in Agreement Version");
				}
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "finance_FreshDB")
	@TestCase(createdOn = "2018-05-10", updatedOn = "2018-05-10", testCaseId = "TC_Admin_FIN_Setup_Mandotry_Fill_Previous_Reports", testCaseDescription = "To Verify mandatory to fill previous reports")
	public void mandatoryToFillPreviousReport() throws Exception {
		
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());
		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("finance", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		String mandToFillPreRep = "No";
		
		try {
			driver = fc.loginpage().login(driver);
			AdminFinanceFinanceSetupPreferencesPage pobj = new AdminFinanceFinanceSetupPreferencesPage(driver);
			
			fc.utobj().printTestStep("Navigate To Admin > Finance > Finance Setup Preferences > Mandatory to Fill Previous Reports");
			
			mandatoryToFillPreviousReports(driver, config, mandToFillPreRep);
			
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "finance")
	@TestCase(createdOn = "2018-05-11", updatedOn = "2018-05-11", testCaseId = "TC_Admin_FIN_Setup_Enable_NonFIN_Cat_KPI", testCaseDescription = "To Verify enable non Financial category and KPI")
	public void enableNonFinancialCategoryForSales() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());
		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("finance", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		String enableKPI = "Yes";
		String enableNonFinCat = "Yes";

		try {
			driver = fc.loginpage().login(driver);
			
			AdminFinanceFinanceSetupPreferencesPage pobj = new AdminFinanceFinanceSetupPreferencesPage(driver);
			
			fc.utobj().printTestStep("Navigate To Admin > Finance > Finance Setup Preferences > Enable Non Financial Category");
			
			enableNonFinancialCategory(driver, config, enableNonFinCat, enableKPI);
			
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "finance_FreshDB")
	@TestCase(createdOn = "2018-06-25", updatedOn = "2018-06-25", testCaseId = "TC_Admin_FIN_First_Day_Of_Week", testCaseDescription = "To Verify set First Day of Week when sales is not submitted")
	public void setFirstDayOfWeek() throws Exception {
		
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());
		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("finance", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		String firstDay = "Monday";
		
		try {
			driver = fc.loginpage().login(driver);
			AdminFinanceFinanceSetupPreferencesPage pobj = new AdminFinanceFinanceSetupPreferencesPage(driver);
			
			fc.utobj().printTestStep("Navigate To Admin > Finance > Finance Setup Preferences > Set First Day Of Week");
			
			setFirstDayOfWeek(driver, config, firstDay);
			
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}
	
	@Test(groups = "finance_FreshDB")
	@TestCase(createdOn = "2018-06-25", updatedOn = "2018-06-25", testCaseId = "TC_Admin_FIN_Late_Fee", testCaseDescription = "To Verify late fee if it is disabled from admin")
	public void disableLateFee() throws Exception {
		
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());
		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("finance", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		String enableDisable = "No";
		
		try {
			driver = fc.loginpage().login(driver);
			AdminFinanceAgreementVersionsPage pobj = new AdminFinanceAgreementVersionsPage(driver);
			
			fc.utobj().printTestStep("Navigate To Admin > Finance > Finance Setup Preferences > Set First Day Of Week");
			
			enableDisableLateFee(driver, config, enableDisable);
			
			fc.adminpage().openAdminFinAgreementVersionsPage(driver);
			
			fc.utobj().clickElement(driver, pobj.AgreemenmtVersionBtn);

			fc.utobj().clickElement(driver, pobj.AddAgreement);
			
			boolean isLateFeeDisplyaed = fc.utobj().assertPageSource(driver, "Late Fee Items");
			
			if (isLateFeeDisplyaed == true) {
				
				fc.utobj().throwsException("Late Fee is displyaed in Agreement Version while it is disabled from set up preferences");
			}
			
			fc.finance().finance_common().financeSalesPage(driver);
			
			boolean isLateFeeDisplyaed1 = fc.utobj().assertPageSource(driver, "Late Fee ($)");
			
			if (isLateFeeDisplyaed1 == true) {
				
				fc.utobj().throwsException("Late Fee is displyaed in Agreement Version while it is disabled from set up preferences");
			}
			
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}
	
	@Test(groups = "finance")
	@TestCase(createdOn = "2018-06-12", updatedOn = "2018-06-12", testCaseId = "TC_Admin_Fin_Setup_disable_KPI", testCaseDescription = "To Verify Enable KPI if non financial category is enabled and Enable KPI is No")
	public void disableKPIForSales() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());
		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("finance", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		String enableNonFinCat = "Yes";
		String enableKPI = "No";

		try {
			driver = fc.loginpage().login(driver);
			
			AdminFinanceFinanceSetupPreferencesPage pobj = new AdminFinanceFinanceSetupPreferencesPage(driver);
			
			fc.utobj().printTestStep("Navigate To Admin > Finance > Finance Setup Preferences > Enable Non Financial Category");
			
			enableNonFinancialCategory(driver, config, enableNonFinCat, enableKPI);
			
			fc.finance().finance_common().financeSalesPage(driver);
			
			boolean isTabDisplayed = fc.utobj().assertPageSource(driver, "KPI"); //isElementPresent(driver, driver.findElement(By.xpath(".//*[@id='module_financials']//a[@qat_submodule='KPI']")));
		
			if (isTabDisplayed == true) {
				
				fc.utobj().throwsException("KPI tab is present while it is disabled from admin");
			}
			//enableNonFinCat = "Yes"; enableKPI = "Yes";
			
			//enableNonFinancialCategory(driver, config, enableNonFinCat, enableKPI);
			
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}
	
	@Test(groups = "finance_FreshDB")
	@TestCase(createdOn = "2018-05-11", updatedOn = "2018-05-11", testCaseId = "TC_Admin_FIN_Setup_Allow_Sales_In_local_Currency", testCaseDescription = "To Verify allow Sales Report entry in local currency")
	public void allowSalesInLocalCurrency() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());
		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("finance", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		String allowLocalCurr = "No";

		try {
			driver = fc.loginpage().login(driver);
			AdminFinanceFinanceSetupPreferencesPage pobj = new AdminFinanceFinanceSetupPreferencesPage(driver);
			fc.utobj().printTestStep(
					"Navigate To Admin > Finance > Finance Setup Preferences > Allow Sales Report entry in local currency");

			allowSalesReportEntryInLocalCurrency(driver, config, allowLocalCurr);

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}
	
	@Test(groups = "finance_FreshDB")
	@TestCase(createdOn = "2018-05-11", updatedOn = "2018-05-14", testCaseId = "TC_Admin_FIN_Setup_FISCAL_Year_Start_Month", testCaseDescription = "To Verify configure Fiscal Year Start Month")
	public void configureFiscalYearStartMonth() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());
		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("finance", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		String fiscalMonth = "April";

		try {
			driver = fc.loginpage().login(driver);
			AdminFinanceFinanceSetupPreferencesPage pobj = new AdminFinanceFinanceSetupPreferencesPage(driver);
			
			fc.utobj().printTestStep("Navigate To Admin > Finance > Delete Sales Report");
			
			deleteSalesReportFromAdmin(driver, config);
			
			fc.utobj().printTestStep("Navigate To Admin > Finance > Finance Setup Preferences > Configure Fiscal Year Start Month");
			
			FiscalYearStartMonth(driver, config, fiscalMonth);

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}
	
	@Test(groups = "finance")
	@TestCase(createdOn = "2018-05-14", updatedOn = "2018-05-14", testCaseId = "TC_Admin_FIN_Setup_AFSahre_Payments", testCaseDescription = "To Verify configuration of AF Share Payments")
	public void enableAFSharePayments() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());
		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("finance", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		String enableAFShare="Yes";
		String payFreqAFShare="Weekly";
		String EFTDayDateForAFShare ="Thursday";
		
		try {
			driver = fc.loginpage().login(driver);
			AdminFinanceFinanceSetupPreferencesPage pobj = new AdminFinanceFinanceSetupPreferencesPage(driver);

			fc.utobj().printTestStep("Navigate To Admin > Finance > Finance Setup Preferences > AF Share Payments");

			AFSharePayments(driver, config, enableAFShare, payFreqAFShare, EFTDayDateForAFShare);
			
			fc.finance().finance_common().financeAreaFranchisePage(driver);
			
			WebElement element =driver.findElement(By.xpath(".//*[@id='module_financials']//a[@qat_submodule='Area Franchise']"));
			
			boolean isTabDisplayed = fc.utobj().isElementPresent(driver, element);
			
			if (isTabDisplayed == false) {
				fc.utobj().throwsException("Area Franchise tab is not displaying while settings are updated in admin");
			}
			
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}
	
	@Test(groups = "finance")
	@TestCase(createdOn = "2018-05-31", updatedOn = "2018-05-31", testCaseId = "TC_Admin_Fin_Setup_Area_Franchise_In_EFT_Trans", testCaseDescription = "To verify area franchise detail sub tab in EFT Transactions tab")
	public void verifyAreaFranchiseInEFTTransactions() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());
		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("finance", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		String enableAFShare="Yes", enableEFT="Yes";
		String payFreqAFShare="Weekly";
		String EFTDayDateForAFShare ="Monday";
		
		try {
			driver = fc.loginpage().login(driver);
			AdminFinanceFinanceSetupPreferencesPage pobj = new AdminFinanceFinanceSetupPreferencesPage(driver);
			
			fc.utobj().printTestStep("Navigate To Admin > Finance > Finance Setup Preferences");
			
			setInvoice(driver, config);
			
			setReceivePaymentsFromFranchiseThroughEFT(driver, config, enableEFT);
			
			fc.utobj().printTestStep("Navigate To Admin > Finance > Finance Setup Preferences > AF Share Payments");
			
			AFSharePayments(driver, config, enableAFShare, payFreqAFShare, EFTDayDateForAFShare);
			
			fc.finance().finance_common().financeEFTTransactionsPage(driver);
			
			WebElement element =driver.findElement(By.xpath(".//*[@id='finTopTable']//table[@class='subTabs']//span[contains(text(), 'Area Franchise Detail')]"));
			
			boolean isTabDisplayed = fc.utobj().isElementPresent(driver, element);
			
			if (isTabDisplayed == false) {
				
				fc.utobj().throwsException("Area Franchise Detail sub tab is not displaying while settings are updated in admin");
			}
			
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}
	
	@Test(groups = {"finance1", "finance_FailedTC"})
	@TestCase(createdOn = "2018-05-14", updatedOn = "2018-05-14", testCaseId = "TC_Admin_FIN_Setup_Alert_Separate_Payment", testCaseDescription = "To Verify alert for Receive payments for Fee(s) / Additional Fees separately")
	public void alertForReceivePaymentsforFeesAdditionalFeesSeparately() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());
		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("finance", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		String expectedAlertText = "This will wipe the configured Corporate Details for EFT. To generate EFT you will have to enter the details again. Saving this will disable receiving Fee(s) / Additional Fees separately from Royalty components. Are you sure you want to continue?";
		try {
			driver = fc.loginpage().login(driver);
			AdminFinanceFinanceSetupPreferencesPage pobj = new AdminFinanceFinanceSetupPreferencesPage(driver);

			fc.utobj().printTestStep(
					"Navigate To Admin > Finance > Finance Setup Preferences > Receive payments for Fee(s) / Additional Fees separately");

			receivePaymentsforFeesAdditionalFeesSeparately(driver, config, expectedAlertText);

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}
	
	@Test(groups = {"finance", "finance_FailedTC"})
	@TestCase(createdOn = "2018-05-31", updatedOn = "2018-06-01", testCaseId = "TC_Admin_FIN_Setup_payment_frequency_combo", testCaseDescription = "To Verify configuration of AF Share Payments")
	public void payFreUnderAFShare() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());
		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("finance", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		String enableAFShare="Yes";

		try {
			driver = fc.loginpage().login(driver);
			AdminFinanceFinanceSetupPreferencesPage pobj = new AdminFinanceFinanceSetupPreferencesPage(driver);

			fc.utobj().printTestStep("Navigate To Admin > Finance > Finance Setup Preferences > AF Share Payments");
			
			String payFreqAFShare="Daily";
			String EFTDayDateForAFShare ="Thursday";
			AFShare(driver, config, enableAFShare, payFreqAFShare, EFTDayDateForAFShare);
			
			Select select = new Select(pobj.selectPayFreqForAFShare);
			
			String paymentFrequency = select.getFirstSelectedOption().getText();
			
			if(!payFreqAFShare.equalsIgnoreCase(paymentFrequency)) {
				
				fc.utobj().throwsException(payFreqAFShare+ " option is not selected");
			}
			

			payFreqAFShare="Weekly";
			EFTDayDateForAFShare ="Thursday";
			AFShare(driver, config, enableAFShare, payFreqAFShare, EFTDayDateForAFShare);
						
			paymentFrequency = select.getFirstSelectedOption().getText();
			
			if(!payFreqAFShare.equalsIgnoreCase(paymentFrequency)) {
				
				fc.utobj().throwsException(payFreqAFShare+ " option is not selected");
			}
			
			
			Select select1 = new Select(pobj.selectWeeklyDay);
			
			String eftDayDate = select1.getFirstSelectedOption().getText();
			
			if(!EFTDayDateForAFShare.equalsIgnoreCase(eftDayDate)) {
				
				fc.utobj().throwsException(EFTDayDateForAFShare+ " option is not selected");
			}
			
			
			payFreqAFShare="Every 15 Days";
			EFTDayDateForAFShare ="1, 16";
			AFShare(driver, config, enableAFShare, payFreqAFShare, EFTDayDateForAFShare);
						
			paymentFrequency = select.getFirstSelectedOption().getText();
			
			if(!payFreqAFShare.equalsIgnoreCase(paymentFrequency)) {
				
				fc.utobj().throwsException(payFreqAFShare+ " option is not selected");
			}
			
			Select select2 = new Select(pobj.selectEvery15Day);
			
			eftDayDate = select2.getFirstSelectedOption().getText();
			
			if(!EFTDayDateForAFShare.equalsIgnoreCase(eftDayDate)) {
				
				fc.utobj().throwsException(EFTDayDateForAFShare+ " option is not selected");
			}
						
			
			payFreqAFShare="Monthly";
			EFTDayDateForAFShare ="7";
			
			AFShare(driver, config, enableAFShare, payFreqAFShare, EFTDayDateForAFShare);
						
			paymentFrequency = select.getFirstSelectedOption().getText();
			
			if(!payFreqAFShare.equalsIgnoreCase(paymentFrequency)) {
				
				fc.utobj().throwsException(payFreqAFShare+ " option is not selected");
			}

			Select select3 = new Select(pobj.selectMonthDay);
			
			eftDayDate = select3.getFirstSelectedOption().getText();
			
			if(!EFTDayDateForAFShare.equalsIgnoreCase(eftDayDate)) {
				
				fc.utobj().throwsException(EFTDayDateForAFShare+ " option is not selected");
			}
			
			payFreqAFShare="Quarterly";
			EFTDayDateForAFShare ="15";
			
			AFShare(driver, config, enableAFShare, payFreqAFShare, EFTDayDateForAFShare);
						
			paymentFrequency = select.getFirstSelectedOption().getText();
			
			if(!payFreqAFShare.equalsIgnoreCase(paymentFrequency)) {
				
				fc.utobj().throwsException(payFreqAFShare+ " option is not selected");
			}
			
			Select select4 = new Select(pobj.selectQuarterDay);
			
			eftDayDate = select4.getFirstSelectedOption().getText();
			
			if(!EFTDayDateForAFShare.equalsIgnoreCase(eftDayDate)) {
				
				fc.utobj().throwsException(EFTDayDateForAFShare+ " option is not selected");
			}
			
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}
	
	public void AFShare(WebDriver driver, Map<String, String> config,
			String enableAFShare, String payFreqAFShare, String EFTDayDateForAFShare) throws Exception {
		
		String testCaseId = "TC_FIN_Set_AF_Share_Payments";
		
		if (fc.utobj().configurationTestCaseToExecute(testCaseId)) {
			
			try {
				AdminFinanceFinanceSetupPreferencesPage pobj = new AdminFinanceFinanceSetupPreferencesPage(driver);
				
				fc.adminpage().openAdminFinanceSetupPreferencesLnk(driver);
				
				if(enableAFShare.equalsIgnoreCase("Yes")) {
								
						fc.utobj().clickElement(driver, pobj.editAFSharePayment);
						
						fc.utobj().clickElement(driver, pobj.yesAFSharePayment);
						
						fc.utobj().selectDropDown(driver, pobj.selectPayFreqForAFShare, payFreqAFShare);
						
						if (payFreqAFShare.equalsIgnoreCase("Weekly")) {
							
							fc.utobj().selectDropDownByVisibleText(driver, pobj.selectWeeklyDay, EFTDayDateForAFShare);
							
							} else if (payFreqAFShare.equalsIgnoreCase("Every 15 Days")) {
								
								fc.utobj().selectDropDownByVisibleText(driver, pobj.selectEvery15Day, EFTDayDateForAFShare);
								
								}else if (payFreqAFShare.equalsIgnoreCase("Monthly")){
									
									fc.utobj().selectDropDownByVisibleText(driver, pobj.selectMonthDay, EFTDayDateForAFShare);
									
									}else if (payFreqAFShare.equalsIgnoreCase("Quarterly")) {
										
										fc.utobj().selectDropDownByVisibleText(driver, pobj.selectQuarterDay, EFTDayDateForAFShare);
									}
						
							
							fc.utobj().clickElement(driver, pobj.saveAFShare);
						}
				
				if (enableAFShare.equalsIgnoreCase("No")) {
						
							fc.utobj().clickElement(driver, pobj.editAFSharePayment);
							
							fc.utobj().clickElement(driver, pobj.noAFSharePayment);
							
							fc.utobj().clickElement(driver, pobj.saveAFShare);
				}
					
			} catch (Exception e) {
				
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		}
	}
	
	@Test(groups = {"finance"})
	@TestCase(createdOn = "2018-05-25", updatedOn = "2018-05-25", testCaseId = "TC_Admin_FIN_Setup_Corp_Approv_Process", testCaseDescription = "To Verify Corporate approval process setting is locked when sales are entered")
	public void verifyCorporateApprovalProcessLocked() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());
		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("finance", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		
		String agrmntVrsnName = "AV";
		String royaltyPrcntg = "10";
		String advPercentage = "8";
		String royaltyAreaFranchiseValue = "5";
		String advAreaFranchiseValue = "3";
		
		try {
			driver = fc.loginpage().login(driver);
			FinanceSalesPage pobj = new FinanceSalesPage(driver);
			fc.utobj().printTestStep("Navigate To Admin > Finance > Agreement Versions");
			AdminFinanceAgreementVersionsPageTest agreementforSales = new AdminFinanceAgreementVersionsPageTest();

			fc.utobj().printTestStep("Add Agreement");
			String agreementVersion = agreementforSales.addAgreement(driver, config, agrmntVrsnName, 
					 royaltyPrcntg, advPercentage, royaltyAreaFranchiseValue, advAreaFranchiseValue);

			fc.utobj().printTestStep("Navigate To Admin > Franchise Location > Add Franchise Location");
			fc.utobj().printTestStep("Add Franchise Location");

			String regionName = fc.utobj().generateTestData("GT");
			String storeType = fc.utobj().generateTestData("GT");
			String franchiseId = fc.utobj().generateTestData("GT");
			AdminFranchiseLocationAddFranchiseLocationPageTest franchise = new AdminFranchiseLocationAddFranchiseLocationPageTest();
			franchiseId = franchise.addFranchiseLocation_All_Agreement(driver, franchiseId, regionName, storeType,
					agreementVersion, config);

			fc.utobj().printTestStep("Navigate To Admin > Finance > Finance Setup Preferences");
			fc.utobj().printTestStep("Setup Medium of submission of Sales Reports");
			String salesReportType = "webForm";
			new AdminFinanceFinanceSetupPreferencesPageTest().mediumOfSubmissionOfSalesReports(driver, config,
					salesReportType);

			fc.utobj().printTestStep("Set Invoice Frequency");
			new AdminFinanceFinanceSetupPreferencesPageTest().setInvoice(driver, config);

			fc.utobj().printTestStep("Navigate To Finance > Sales");
			fc.utobj().printTestStep("Enter Sales Report");
			String categoryQuantity = "110";
			String categoryAmount = "6000";
			
			FinanceSalesPageTest enterSales = new FinanceSalesPageTest();
			
			String reportId = enterSales.enterSalesReport(driver, franchiseId, categoryQuantity, categoryAmount);

			fc.utobj().printTestStep("Filter Sales Report By Franchise Id");
			
			enterSales.filterSalesReport(driver, franchiseId);

			fc.utobj().printTestStep("Approve Sales Report");
			
			fc.utobj().actionImgOption(driver, reportId, "Approve");

			fc.commonMethods().switch_cboxIframe_frameId(driver);
			
			fc.utobj().sendKeys(driver, pobj.commentTxtArea, "Test Approve Comments" + fc.utobj().generateRandomNumber());
			
			fc.utobj().clickElement(driver, pobj.SaveBtn);

			fc.utobj().clickElement(driver, pobj.cloaseBtn);
			
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Filter Sales Report By Franchise Id");
			
			enterSales.filterSalesReport(driver, franchiseId);
			
			AdminFinanceFinanceSetupPreferencesPage pobj1 = new AdminFinanceFinanceSetupPreferencesPage(driver);

			fc.utobj().printTestStep(
					"Navigate To Admin > Finance > Finance Setup Preferences > Receive payments for Fee(s) / Additional Fees separately");
			
			fc.adminpage().openAdminFinanceSetupPreferencesLnk(driver);

			fc.utobj().clickElement(driver, pobj1.editCorpApovProcess);
			
			String expectedResult= "This preference is currently locked. Click here for details.";
			
			String actualResult = fc.utobj().getText(driver, pobj1.alertCorpApovProcess); 
			
			if(!actualResult.equalsIgnoreCase(expectedResult)) {
				
				fc.utobj().throwsException("Preferences are not locked even sales is entered");
			}
			
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}
	
	public void receivePaymentsforFeesAdditionalFeesSeparately(WebDriver driver, Map<String, String> config, String expectedAlertText) throws Exception {

		String testCaseId = "TC_Admin_FIN_TC-22993";

		if (fc.utobj().configurationTestCaseToExecute(testCaseId)) {
			try {
				fc.adminpage().openAdminFinanceSetupPreferencesLnk(driver);

				AdminFinanceFinanceSetupPreferencesPage pobj = new AdminFinanceFinanceSetupPreferencesPage(driver);
				
				fc.utobj().clickElement(driver, pobj.editPaymentFeesOrAdditionalFeesSep);
				
				Thread.sleep(1000);
				
				String actualAlertText = driver.switchTo().alert().getText();
				
				System.out.println(actualAlertText);
				System.out.println(expectedAlertText);
				assertEquals(expectedAlertText, actualAlertText);
				
				
			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {

		}
	}
		
	public void setInvoice(WebDriver driver, Map<String, String> config) throws Exception {

		String testCaseId = "TC_FIN_Set_Invoice";

		if (fc.utobj().configurationTestCaseToExecute(testCaseId)) {
			
			try {
				fc.adminpage().openAdminFinanceSetupPreferencesLnk(driver);
				
				AdminFinanceFinanceSetupPreferencesPage pobj = new AdminFinanceFinanceSetupPreferencesPage(driver);
				
				if (!fc.utobj().isSelected(driver, pobj.yesInvoiceDate)) {
					
					fc.utobj().clickElement(driver, pobj.changeGenerateInvoice);
					
					fc.utobj().clickElement(driver, pobj.yesInvoiceDate);
					
					fc.utobj().selectDropDown(driver, pobj.selectFrequency, "Daily");
					
					fc.utobj().clickElement(driver, pobj.saveGenerateInvoice);
				}
					fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
					
			} catch (Exception e) {
				
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {

		}
	}
	
	public void setReceivePaymentsFromFranchiseThroughEFT(WebDriver driver, Map<String, String> config, String enableEFT) throws Exception {

		String testCaseId = "TC_FIN_Set_EFT";

		if (fc.utobj().configurationTestCaseToExecute(testCaseId)) {
			try {
				fc.adminpage().openAdminFinanceSetupPreferencesLnk(driver);

				AdminFinanceFinanceSetupPreferencesPage pobj = new AdminFinanceFinanceSetupPreferencesPage(driver);
				
				if (enableEFT.equalsIgnoreCase("Yes")) {
					if (!fc.utobj().isSelected(driver, pobj.yesEFTOption)) {
						
						fc.utobj().clickElement(driver, pobj.editEFTOption);
						
						fc.utobj().clickElement(driver, pobj.yesEFTOption);
						
						fc.utobj().selectDropDown(driver, pobj.selectEFTFrequency, "Daily");
						
						fc.utobj().clickElementWithoutMove(driver, pobj.saveEFTOption);
					}
				}
			if(enableEFT.equalsIgnoreCase("No")) {
					
					if (!fc.utobj().isSelected(driver, pobj.noEFTOption)) {						
						fc.utobj().clickElement(driver, pobj.editEFTOption);
						fc.utobj().clickElementWithoutMove(driver, pobj.noEFTOption);
						fc.utobj().clickElementWithoutMove(driver, pobj.saveEFTOption);
					}					
				}
			
				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
				
			} catch (Exception e) {
				
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {

		}
	}
	
	public void allowAdjustmentsAfterInvoicing(WebDriver driver, Map<String, String> config, String allowAdj) throws Exception {

		String testCaseId = "TC_FIN_Allow_Adjustment";

		if (fc.utobj().configurationTestCaseToExecute(testCaseId)) {
			try {
				fc.adminpage().openAdminFinanceSetupPreferencesLnk(driver);
				
				AdminFinanceFinanceSetupPreferencesPage pobj = new AdminFinanceFinanceSetupPreferencesPage(driver);
				if (allowAdj.equalsIgnoreCase("Yes")){
			
				if (!fc.utobj().isSelected(driver, pobj.yesAdjust)) {
					
					fc.utobj().clickElement(driver, pobj.adjustmentModify);
					
					fc.utobj().acceptAlertBox(driver);
					
				}
			}
			if (allowAdj.equalsIgnoreCase("No")) {
				
				if (!fc.utobj().isSelected(driver, pobj.noAdjust)) {
						
						fc.utobj().clickElement(driver, pobj.adjustmentModify);
						
						fc.utobj().acceptAlertBox(driver);
					}
				}
				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
			} catch (Exception e) {
				
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {

		}
	}

	public void mediumOfSubmissionOfSalesReports(WebDriver driver, Map<String, String> config, String salesReportType)
			throws Exception {
		
		String testCaseId = "TC_FIN_Set_Submission_Sales_Report";
		
		if (fc.utobj().configurationTestCaseToExecute(testCaseId)) {
			
			try {
				AdminFinanceFinanceSetupPreferencesPage pobj = new AdminFinanceFinanceSetupPreferencesPage(driver);
				
				fc.adminpage().openAdminFinanceSetupPreferencesLnk(driver);
				
				if (salesReportType.equalsIgnoreCase("webForm")) {
					
					if (!fc.utobj().isSelected(driver, pobj.webFormType)) {
						
						fc.utobj().clickElement(driver, pobj.salesReportSubmission);
						
						fc.utobj().clickElement(driver, pobj.webFormType);
						
						fc.utobj().clickElement(driver, pobj.saveBtn);
					}
					
				} else if (salesReportType.equalsIgnoreCase("csv")) {

					if (!fc.utobj().isSelected(driver, pobj.csvUploadFileType)) {
						
						fc.utobj().clickElement(driver, pobj.salesReportSubmission);
						
						fc.utobj().clickElement(driver, pobj.csvUploadFileType);
						
						fc.utobj().clickElement(driver, pobj.saveBtn);
					}
					
				} else if (salesReportType.equalsIgnoreCase("xls")) {
					
					if (!fc.utobj().isSelected(driver, pobj.xlsUploadFileType)) {
						
						fc.utobj().clickElement(driver, pobj.salesReportSubmission);
						
						fc.utobj().clickElement(driver, pobj.xlsUploadFileType);
						
						fc.utobj().clickElement(driver, pobj.saveBtn);
					}
				}
				
			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {

		}
	}

	public void financeAndCRMTransactionsIntegration(WebDriver driver) throws Exception {
		
		String testCaseId = "TC_FIN_Set_Finance_And_CRM_Transactions_Integration";
		
		if (fc.utobj().configurationTestCaseToExecute(testCaseId)) {

			try {
				AdminFinanceFinanceSetupPreferencesPage pobj = new AdminFinanceFinanceSetupPreferencesPage(driver);
				
				fc.adminpage().openAdminFinanceSetupPreferencesLnk(driver);
				
				if (fc.utobj().isSelected(driver, pobj.ctfinintYesBtn)) {
					
					fc.utobj().clickElement(driver, pobj.finCRMInt);
					
					fc.utobj().clickElement(driver, pobj.ctfinintNoBtn);
					
					fc.utobj().clickElement(driver, pobj.ctfinIntSaveBtn);
				} else {
					// Do Nothing
				}
				
			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {

		}
	}

	public void mediumOfSubmissionOfProfitAndLossReports(WebDriver driver, Map<String, String> config,
			String PLReportType) throws Exception {

		String testCaseId = "TC_FIN_Set_Submission_ProfitLoss_Reports";
		
		if (fc.utobj().configurationTestCaseToExecute(testCaseId)) {

			try {
				AdminFinanceFinanceSetupPreferencesPage pobj = new AdminFinanceFinanceSetupPreferencesPage(driver);
				
				fc.adminpage().openAdminFinanceSetupPreferencesLnk(driver);

				fc.utobj().clickElement(driver, pobj.profitLossSubmission);

				if (PLReportType.equalsIgnoreCase("webForm")) {

					if (!fc.utobj().isSelected(driver, pobj.plWebFormType)) {

						fc.utobj().clickElement(driver, pobj.plWebFormType);
					}

				} else if (PLReportType.equalsIgnoreCase("csv")) {

					if (!fc.utobj().isSelected(driver, pobj.plcsvUploadFileType)) {

						fc.utobj().clickElement(driver, pobj.plcsvUploadFileType);
					}

				} else if (PLReportType.equalsIgnoreCase("xls")) {

					if (!fc.utobj().isSelected(driver, pobj.plxlsUploadFileType)) {

						fc.utobj().clickElement(driver, pobj.plxlsUploadFileType);
					}

				} else if (PLReportType.equalsIgnoreCase("QuickBooks")) {
					
					if (!fc.utobj().isSelected(driver, pobj.QuickBooks)) {

						fc.utobj().clickElement(driver, pobj.QuickBooks);
					}
				}

				fc.utobj().clickElement(driver, pobj.plSaveBtn);

			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {

		}
	}

	public void royaltyReportingFrequency(WebDriver driver, Map<String, String> config, String rrFrequency)
			throws Exception {

		String testCaseId = "TC_FIN_Set_Royalty_Reporting_Frequency";
		if (fc.utobj().configurationTestCaseToExecute(testCaseId)) {

			try {
				AdminFinanceFinanceSetupPreferencesPage pobj = new AdminFinanceFinanceSetupPreferencesPage(driver);
				
				fc.adminpage().openAdminFinanceSetupPreferencesLnk(driver);

				fc.utobj().clickElement(driver, pobj.royaltyReportFrequency);

				if (rrFrequency.equalsIgnoreCase("Daily")) {

					if (!fc.utobj().isSelected(driver, pobj.daily)) {

						fc.utobj().clickElement(driver, pobj.daily);
					}

				} else if (rrFrequency.equalsIgnoreCase("Weekly")) {

					if (!fc.utobj().isSelected(driver, pobj.weekly)) {

						fc.utobj().clickElement(driver, pobj.weekly);
					}

				} else if (rrFrequency.equalsIgnoreCase("Every15Days")) {

					if (!fc.utobj().isSelected(driver, pobj.every15Days)) {

						fc.utobj().clickElement(driver, pobj.every15Days);
					}

				} else if (rrFrequency.equalsIgnoreCase("Monthly")) {
					
					if (!fc.utobj().isSelected(driver, pobj.monthly)) {

						fc.utobj().clickElement(driver, pobj.monthly);
					}
				}

				fc.utobj().clickElement(driver, pobj.freSaveBtn);

			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {

		}
	}

	public void mandatoryToFillPreviousReports(WebDriver driver, Map<String, String> config, String mandToFillPreRep) throws Exception {

		String testCaseId = "TC_FIN_Set_Mandatory_to_Fill_Previous_Reports";
	
		if (fc.utobj().configurationTestCaseToExecute(testCaseId)) {
			
			try {
				AdminFinanceFinanceSetupPreferencesPage pobj = new AdminFinanceFinanceSetupPreferencesPage(driver);
								
				fc.adminpage().openAdminFinanceSetupPreferencesLnk(driver);
				
				if (mandToFillPreRep.equalsIgnoreCase("Yes")) {
					/*
					if(!fc.utobj().isSelected(driver, pobj.fillPRYes)) {
						
						fc.utobj().clickElement(driver, pobj.editFillPreviousReports);
						
						driver.switchTo().alert().accept();
						
						}*/
				}
				
				if(mandToFillPreRep.equalsIgnoreCase("No")) {
							
					if(!fc.utobj().isSelected(driver, pobj.fillPRNo)) {
						
						fc.utobj().clickElement(driver, pobj.editFillPreviousReports);
						
						driver.switchTo().alert().accept();
						}
					}
				
				} catch (Exception e) {
				
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {

		}
	}

	public void deleteSalesReportFromAdmin(WebDriver driver, Map<String, String> config) throws Exception {

		String testCaseId = "TC_FIN_Delete_Sales_Report";
	
		if (fc.utobj().configurationTestCaseToExecute(testCaseId)) {

			try {
				fc.finance().finance_common().adminFinDeleteSalesReportsLnk(driver);
				
				AdminFinanceDeleteSalesReportsPage delete_Sales = new AdminFinanceDeleteSalesReportsPage(driver);
				
				fc.utobj().printTestStep("Go to Sales Tab");
				
				fc.utobj().clickElement(driver, delete_Sales.sales);
				
					boolean isSalesReport = fc.utobj().assertPageSource(driver, "No records found.");
					
					if(isSalesReport == false) {
						
						fc.utobj().selectDropDownByValue(driver, delete_Sales.selectViewPerPage, "500");
						
						fc.utobj().clickElement(driver, delete_Sales.deleteAllSalesCheckbox);
						
						fc.utobj().clickElement(driver, delete_Sales.deleteSales);
						
						fc.utobj().acceptAlertBox(driver);
					}
					
			} catch (Exception e) {
				
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {

		}
	}

	public void deletePaymentReportFromAdmin(WebDriver driver, Map<String, String> config) throws Exception {

		String testCaseId = "TC_FIN_Delete_Payment_Report";
	
		if (fc.utobj().configurationTestCaseToExecute(testCaseId)) {

			try {
				fc.finance().finance_common().adminFinDeleteSalesReportsLnk(driver);
				
				AdminFinanceDeleteSalesReportsPage delete_Sales = new AdminFinanceDeleteSalesReportsPage(driver);
				
				fc.utobj().printTestStep("Go to Payment Tab");
				
				fc.utobj().clickElement(driver, delete_Sales.payments);
				
					boolean isPaymentReport = fc.utobj().assertPageSource(driver, "No records found.");
					
					if(isPaymentReport == false) {
						
						fc.utobj().selectDropDownByValue(driver, delete_Sales.selectViewPerPage, "500");
						
						fc.utobj().clickElement(driver, delete_Sales.deleteAllSalesCheckbox);
						
						fc.utobj().clickElement(driver, delete_Sales.deleteSales);
						
						fc.utobj().acceptAlertBox(driver);
					}
					
			} catch (Exception e) {
				
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {

		}
	}

	public void deleteRoyaltyReportFromAdmin(WebDriver driver, Map<String, String> config) throws Exception {

		String testCaseId = "TC_FIN_Delete_Royalty_Report";
	
		if (fc.utobj().configurationTestCaseToExecute(testCaseId)) {

			try {
				fc.finance().finance_common().adminFinDeleteSalesReportsLnk(driver);
				
				AdminFinanceDeleteSalesReportsPage delete_Sales = new AdminFinanceDeleteSalesReportsPage(driver);
				
				fc.utobj().printTestStep("Go to Royalty Tab");
				
				fc.utobj().clickElement(driver, delete_Sales.royalties);
				
					boolean isRoyaltyReport = fc.utobj().assertPageSource(driver, "No records found.");
					
					if(isRoyaltyReport == false) {
						
						fc.utobj().selectDropDownByValue(driver, delete_Sales.selectViewPerPage, "500");
						
						fc.utobj().clickElement(driver, delete_Sales.deleteAllSalesCheckbox);
						
						fc.utobj().clickElement(driver, delete_Sales.deleteSales);
						
						fc.utobj().acceptAlertBox(driver);
					}
					
			} catch (Exception e) {
				
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {

		}
	}
	
	public void enableCorpApprovProcess(WebDriver driver, Map<String, String> config) throws Exception {

		String testCaseId = "TC_FIN_enable_Corporate_Approval_Process";
		
		if (fc.utobj().configurationTestCaseToExecute(testCaseId)) {

			try {
				AdminFinanceFinanceSetupPreferencesPage pobj = new AdminFinanceFinanceSetupPreferencesPage(driver);
				
				fc.adminpage().openAdminFinanceSetupPreferencesLnk(driver);

				fc.utobj().clickElement(driver, pobj.editCorpApovProcess);

				driver.switchTo().alert().accept();

			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {

		}
	}

	public void enableNonFinancialCategory(WebDriver driver, Map<String, String> config, String enableNonFinCat,
			String enableKPI) throws Exception {

		String testCaseId = "TC_FIN_enable_Non_Financial_Category";
		
		if (fc.utobj().configurationTestCaseToExecute(testCaseId)) {

			try {
				AdminFinanceFinanceSetupPreferencesPage pobj = new AdminFinanceFinanceSetupPreferencesPage(driver);
				
				fc.adminpage().openAdminFinanceSetupPreferencesLnk(driver);

				fc.utobj().clickElement(driver, pobj.editEnableNonFinCategory);

				if (enableNonFinCat.equalsIgnoreCase("Yes")) {
					
					if (!fc.utobj().isSelected(driver, pobj.yesForEnableNonFinCat)) {
						
						fc.utobj().clickElement(driver, pobj.yesForEnableNonFinCat);
					}
					if (enableKPI.equalsIgnoreCase("Yes")) {
						
						fc.utobj().selectDropDown(driver, pobj.enableKPI, "Yes");
					} else {
						
						fc.utobj().selectDropDown(driver, pobj.enableKPI, "No");
					}
				} else {
					if (enableNonFinCat.equalsIgnoreCase("No")) {
						
						if (!fc.utobj().isSelected(driver, pobj.noForEnableNonFinCat)) {
							
							fc.utobj().clickElement(driver, pobj.noForEnableNonFinCat);
						}
					}
				}

				fc.utobj().clickElement(driver, pobj.saveEnableNonFinCat);

			} catch (Exception e) {
				
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}

		}
	}

	public void setFirstDayOfWeek(WebDriver driver, Map<String, String> config, String firstDay) throws Exception {

		String testCaseId = "TC_FIN_Set_First_Day_of_Week";
		if (fc.utobj().configurationTestCaseToExecute(testCaseId)) {

			try {
				AdminFinanceFinanceSetupPreferencesPage pobj = new AdminFinanceFinanceSetupPreferencesPage(driver);
				
				fc.adminpage().openAdminFinanceSetupPreferencesLnk(driver);
				
				fc.utobj().clickElement(driver, pobj.editStartFirstDayOfWeek);
				
				boolean prefrenceLocked = fc.utobj().assertPageSource(driver, "This preference is currently locked. Click here for details.");
				
				if (prefrenceLocked == false) {
					
					fc.utobj().selectDropDown(driver, pobj.selectFirstDayOfWeek, firstDay);
					
					fc.utobj().clickElement(driver, pobj.saveFirstDay);
					
				} else if (prefrenceLocked == true){
					
					fc.utobj().printTestStep("Sales is already Entered");
				}
				
			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {

		}
	}
		
	public void allowSalesReportEntryInLocalCurrency(WebDriver driver, Map<String, String> config,
			String allowLocalCurr) throws Exception {

		String testCaseId = "TC_FIN_Allow_Sales_Report_Entry_In_Local_Currency";
		if (fc.utobj().configurationTestCaseToExecute(testCaseId)) {

			try {
				AdminFinanceFinanceSetupPreferencesPage pobj = new AdminFinanceFinanceSetupPreferencesPage(driver);
				
				fc.adminpage().openAdminFinanceSetupPreferencesLnk(driver);

				if (allowLocalCurr.equalsIgnoreCase("Yes")) {
					if (!fc.utobj().isSelected(driver, pobj.yesAllowSalesInLocal)) {
						
						fc.utobj().clickElement(driver, pobj.editAllowSalesInLocal);
						boolean isEditable = fc.utobj().assertPageSource(driver, "This preference is currently locked. Click here for details.");
						
						if (isEditable== false) {
						driver.switchTo().alert().accept();
						}else if(isEditable== false){
							fc.utobj().throwsException("Alert Message: 'This preference is currently locked. Click here for details.' is visible. TC will work on blank DB");
						}
					}
				} else {
					// Do nothing
				}
			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {

		}
	}

	public void FiscalYearStartMonth(WebDriver driver, Map<String, String> config,
			String fiscalMonth) throws Exception {

		String testCaseId = "TC_FIN_Configure_Fiscal_Year_Start_Month";
		if (fc.utobj().configurationTestCaseToExecute(testCaseId)) {

			try {
				AdminFinanceFinanceSetupPreferencesPage pobj = new AdminFinanceFinanceSetupPreferencesPage(driver);
				
				fc.adminpage().openAdminFinanceSetupPreferencesLnk(driver);
				
				fc.utobj().clickElement(driver, pobj.editFISCALYearStartMonth);
				
				boolean prefrenceLocked = fc.utobj().isElementPresent(driver, pobj.preferenceLocked);
				
				if (prefrenceLocked == false) {
					
					fc.utobj().selectDropDown(driver, pobj.selectFISCALStartMonth, fiscalMonth);
					
					fc.utobj().clickElement(driver, pobj.saveFiscalMonth);
					
				} else {
					//Do Nothing
				}
				
			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {

		}
	}
	
	public void AFSharePayments(WebDriver driver, Map<String, String> config,
			String enableAFShare, String payFreqAFShare, String EFTDayDateForAFShare) throws Exception {
		
		String testCaseId = "TC_FIN_Set_AF_Share_Payments";
		
		if (fc.utobj().configurationTestCaseToExecute(testCaseId)) {
			
			try {
				AdminFinanceFinanceSetupPreferencesPage pobj = new AdminFinanceFinanceSetupPreferencesPage(driver);
				
				fc.adminpage().openAdminFinanceSetupPreferencesLnk(driver);
				
				if(enableAFShare.equalsIgnoreCase("Yes")) {
					if (!fc.utobj().isSelected(driver, pobj.yesAFSharePayment)) {
						
						fc.utobj().clickElement(driver, pobj.editAFSharePayment);
						
						fc.utobj().clickElement(driver, pobj.yesAFSharePayment);
						
						fc.utobj().selectDropDown(driver, pobj.selectPayFreqForAFShare, payFreqAFShare);
						
							if (payFreqAFShare.equalsIgnoreCase("Weekly")) {
								
								fc.utobj().selectDropDownByVisibleText(driver, pobj.selectWeeklyDay, EFTDayDateForAFShare);
								
								} else if (payFreqAFShare.equalsIgnoreCase("Every 15 Days")) {
									
									fc.utobj().selectDropDownByVisibleText(driver, pobj.selectEvery15Day, EFTDayDateForAFShare);
									
									}else if (payFreqAFShare.equalsIgnoreCase("Monthly")){
										
										fc.utobj().selectDropDownByVisibleText(driver, pobj.selectMonthDay, EFTDayDateForAFShare);
										
										}else if (payFreqAFShare.equalsIgnoreCase("Quarterly")) {
											
											fc.utobj().selectDropDownByVisibleText(driver, pobj.selectQuarterDay, EFTDayDateForAFShare);
										}
							
							fc.utobj().clickElement(driver, pobj.saveAFShare);
						}
					} else {
						if (!fc.utobj().isSelected(driver, pobj.noAFSharePayment))
						
							fc.utobj().clickElement(driver, pobj.editAFSharePayment);
							
							fc.utobj().clickElement(driver, pobj.noAFSharePayment);
							
							fc.utobj().clickElement(driver, pobj.saveAFShare);
				}
					
			} catch (Exception e) {
				
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		}
	}
	
	/*
	public void deleteFranchiseLocationFromHiddenLink(WebDriver driver, Map<String, String> config, String franchiseID) throws Exception {

		String testCaseId = "TC_AdminHiddenLink_Delete_FranchiseLocation";
	
		if (fc.utobj().configurationTestCaseToExecute(testCaseId)) {

			try {
				
							
				//fc.finance().finance_common().adminFinDeleteSalesReportsLnk(driver);
				
				AdminFinanceDeleteSalesReportsPage delete_Sales = new AdminFinanceDeleteSalesReportsPage(driver);
				
				fc.utobj().printTestStep("Go to Sales Tab");
				
				fc.utobj().clickElement(driver, delete_Sales.sales);
				
					boolean isSalesReport = fc.utobj().assertPageSource(driver, "No records found.");
					
					if(isSalesReport == false) {
						
						fc.utobj().selectDropDownByValue(driver, delete_Sales.selectViewPerPage, "500");
						
						fc.utobj().clickElement(driver, delete_Sales.deleteAllSalesCheckbox);
						
						fc.utobj().clickElement(driver, delete_Sales.deleteSales);
						
						fc.utobj().acceptAlertBox(driver);
					}
					
			} catch (Exception e) {
				
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {

		}
	}
	*/
	
	public void enableDisableLateFee(WebDriver driver, Map<String, String> config, String enableDisable) throws Exception {

		String testCaseId = "TC_FIN_Allow_Late_Fee";
		if (fc.utobj().configurationTestCaseToExecute(testCaseId)) {

			try {
				AdminFinanceFinanceSetupPreferencesPage pobj = new AdminFinanceFinanceSetupPreferencesPage(driver);
				
				fc.adminpage().openAdminFinanceSetupPreferencesLnk(driver);

				if (enableDisable.equalsIgnoreCase("Yes")) {
					if (!fc.utobj().isSelected(driver, pobj.yesLateFee)) {
						
						fc.utobj().clickElement(driver, pobj.editAllowLateFee);
						
						boolean isEditable = fc.utobj().assertPageSource(driver, "This preference is currently locked. Click here for details.");
						
						if (isEditable== false) {
							driver.switchTo().alert().accept();
						}else if(isEditable== false){
							fc.utobj().throwsException("Alert Message : 'This preference is currently locked. Click here for details.' is visible. TC will work on blank DB");
						}
						
					}
				}
				
				if (enableDisable.equalsIgnoreCase("No")) {
					if (!fc.utobj().isSelected(driver, pobj.noLateFee)) {
						
						fc.utobj().clickElement(driver, pobj.editAllowLateFee);
						
						boolean isEditable = fc.utobj().assertPageSource(driver, "This preference is currently locked. Click here for details.");
						
						if (isEditable== false) {
							
							driver.switchTo().alert().accept();
						}else if(isEditable== false){
							fc.utobj().throwsException("Alert Message : 'This preference is currently locked. Click here for details.' is visible. TC will work on blank DB");
						}
					}
				}
				
			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {

		}
	}
	
	
}
