package com.builds.test.fin;

import java.util.Map;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.Test;
import com.builds.uimaps.fin.AdminFinanceNonFinancialKPICategoriesSummaryPage;
import com.builds.utilities.FranconnectUtil;
import com.builds.utilities.TestCase;

public class AdminFinanceNonFinancialKPICategoriesSummaryPageTest {

	FranconnectUtil fc = new FranconnectUtil();
	
	@Test(groups = "finance")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-05-21", testCaseId = "TC_Admin_FIN_Add_New_Non_Financial_Category", testCaseDescription = "Verify Add , Modify And Delete Non Financials Category")
	public void addNewNonFinancialsCategory() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());
		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("finance", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		String category = fc.utobj().generateTestData(dataSet.get("category"));
		String enableNonFinCat ="Yes";
		String enableKPI ="Yes";

		try {
			driver = fc.loginpage().login(driver);
			AdminFinanceNonFinancialKPICategoriesSummaryPage pobj = new AdminFinanceNonFinancialKPICategoriesSummaryPage(
					driver);
			AdminFinanceFinanceSetupPreferencesPageTest setUp = new AdminFinanceFinanceSetupPreferencesPageTest();
			setUp.enableNonFinancialCategory(driver, config, enableNonFinCat, enableKPI);
			
			fc.utobj().printTestStep("Navigate To Admin > Finance > Non-Financial / KPI Categories Summary");
			
			fc.finance().finance_common().adminFinConfigureNonFinancialKPICategoriesforSalesReportLnk(driver);

			fc.utobj().printTestStep("Add New Non Financial Category");
			
			fc.utobj().clickElement(driver, pobj.addNewNonFinancialCategory);
			
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			
			fc.utobj().sendKeys(driver, pobj.category, category);
			
			fc.utobj().selectDropDownByValue(driver, pobj.selectDataType, "1");
			
			fc.utobj().clickElement(driver, pobj.submitButton);

			fc.utobj().clickElement(driver, pobj.closeBtn);

			fc.utobj().printTestStep("Verify Added Category");

			boolean isTextFound = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//td[contains(text () , '" + category + "')]");
			
			if (isTextFound == false) {
				
				fc.utobj().throwsException("Non Financials Category Not Added!");
			}

			fc.utobj().printTestStep("Modify The Category");
			
			fc.utobj().actionImgOption(driver, category, "Modify");

			fc.commonMethods().switch_cboxIframe_frameId(driver);
			
			category = fc.utobj().generateTestData(dataSet.get("category"));
			
			fc.utobj().sendKeys(driver, pobj.category, category);
			
			fc.utobj().selectDropDownByValue(driver, pobj.selectDataType, "2");
			
			fc.utobj().clickElement(driver, pobj.submitButton);

			fc.utobj().clickElement(driver, pobj.closeBtn);

			fc.utobj().printTestStep("Verify Modified Category");

			boolean isTextFoundM = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//td[contains(text () , '" + category + "')]");
			if (isTextFoundM == false) {
				fc.utobj().throwsException("Non Financials Category Not Modified !");
			}

			fc.utobj().printTestStep("Delete The Category");
			
			fc.utobj().actionImgOption(driver, category, "Delete");
			
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			
			fc.utobj().clickElement(driver, pobj.submitButton);

			fc.utobj().clickElement(driver, pobj.closeBtn);

			fc.utobj().printTestStep("Verify The Deleted Category");

			boolean isTextFoundD = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//td[contains(text () , '" + category + "')]");
			
			if (isTextFoundD == true) {
				fc.utobj().throwsException("Non Financials Category Not Deleted !");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	public String addNewNonFinancialsCategory(WebDriver driver, Map<String, String> config, String category)
			throws Exception {

		String testCaseId = "TC_Add_New_Non_Financial_Category";

		if (fc.utobj().validate(testCaseId)) {

			try {

				AdminFinanceNonFinancialKPICategoriesSummaryPage pobj = new AdminFinanceNonFinancialKPICategoriesSummaryPage(
						driver);
				fc.finance().finance_common().adminFinConfigureNonFinancialKPICategoriesforSalesReportLnk(driver);

				fc.utobj().clickElement(driver, pobj.addNewNonFinancialCategory);
				
				fc.commonMethods().switch_cboxIframe_frameId(driver);
				
				fc.utobj().sendKeys(driver, pobj.category, category);
				
				fc.utobj().selectDropDownByValue(driver, pobj.selectDataType, "1");
				
				fc.utobj().clickElement(driver, pobj.submitButton);

				fc.utobj().clickElement(driver, pobj.closeBtn);

				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);

			} catch (Exception e) {

				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
			
			
		} else {
			fc.utobj().throwsSkipException("was not able to Add Non Financial Category");
		}
		return category;
	}

	@Test(groups = "finance")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2017-05-30", testCaseId = "TC_Admin_FIN_Deactivate_Non_Financial_Category", testCaseDescription = "Verify Deactivate And Activate Non Financials Category")
	public void deactivateNonFinancialKPICategories() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());
		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("finance", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		String category = fc.utobj().generateTestData(dataSet.get("category"));
		String enableNonFinCat ="Yes";
		String enableKPI ="Yes";
		
		try {
			driver = fc.loginpage().login(driver);
			AdminFinanceNonFinancialKPICategoriesSummaryPage pobj = new AdminFinanceNonFinancialKPICategoriesSummaryPage(
					driver);
			AdminFinanceFinanceSetupPreferencesPageTest setUp = new AdminFinanceFinanceSetupPreferencesPageTest();
			setUp.enableNonFinancialCategory(driver, config, enableNonFinCat, enableKPI);
			
			fc.utobj().printTestStep("Navigate To Admin > Finance > Non-Financial / KPI Categories Summary");

			fc.utobj().printTestStep("Add New Non Financial Category");
			
			addNewNonFinancialsCategory(driver, FranconnectUtil.config, category);

			fc.utobj().printTestStep("Deactivate The Category");
			
			fc.utobj().actionImgOption(driver, category, "Deactivate");

			fc.commonMethods().switch_cboxIframe_frameId(driver);
			
			fc.utobj().clickElement(driver, pobj.submitButton);

			fc.utobj().clickElement(driver, pobj.closeBtn);

			fc.utobj().printTestStep("Verify Deactivated Category");

			boolean isTextFoundD = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () , '" + category + "')]/ancestor::tr/td[contains(text () , 'Inactive')]");
			if (isTextFoundD == false) {
				
				fc.utobj().throwsException("Non Financials Category Not Deactivated !");
			}

			fc.utobj().printTestStep("Activate The Category");
			
			fc.utobj().actionImgOption(driver, category, "Activate");
			
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			
			fc.utobj().clickElement(driver, pobj.submitButton);

			fc.utobj().clickElement(driver, pobj.closeBtn);

			fc.utobj().printTestStep("Verify The Activated Category");

			boolean isTextFoundA = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () , '" + category + "')]/ancestor::tr/td[contains(text () , 'Active')]");
			if (isTextFoundA == false) {
				fc.utobj().throwsException("Non Financials Category Not Activated !");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "finance")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-05-21", testCaseId = "TC_Admin_FIN_Enable_KPI", testCaseDescription = "Verify Enable and Disable Non Financials Category")
	public void enableKPI() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());
		
		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		
		Map<String, String> dataSet = fc.utobj().readTestData("finance", testCaseId);
		
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		
		String category = fc.utobj().generateTestData(dataSet.get("cat"));
		
		String enableNonFinCat ="Yes";
		
		String enableKPI ="Yes";
		
		try {
			driver = fc.loginpage().login(driver);
			AdminFinanceNonFinancialKPICategoriesSummaryPage pobj = new AdminFinanceNonFinancialKPICategoriesSummaryPage(
					driver);
			AdminFinanceFinanceSetupPreferencesPageTest setUp = new AdminFinanceFinanceSetupPreferencesPageTest();
			setUp.enableNonFinancialCategory(driver, config, enableNonFinCat, enableKPI);
			
			fc.utobj().printTestStep("Navigate To Admin > Finance > Non-Financial / KPI Categories Summary");

			fc.utobj().printTestStep("Add New Non Financial Category");
			
			addNewNonFinancialsCategory(driver, FranconnectUtil.config, category);

			fc.utobj().printTestStep("Disable KPI");
		
			fc.utobj().actionImgOption(driver, category, "Disable KPI");

			fc.commonMethods().switch_cboxIframe_frameId(driver);
			
			fc.utobj().clickElement(driver, pobj.submitButton);

			fc.utobj().clickElement(driver, pobj.closeBtn);

			fc.utobj().printTestStep("Verify Disable KPI");

			boolean isTextFoundD = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () , '" + category + "')]/ancestor::tr/td[contains(text () , 'No')]");
			if (isTextFoundD == false) {
				fc.utobj().throwsException("KPI is Disable !");
			}

			fc.utobj().printTestStep("Enable KPI");
			
			fc.utobj().actionImgOption(driver, category, "Enable KPI");
			
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			
			fc.utobj().clickElement(driver, pobj.submitButton);
			
			fc.utobj().clickElement(driver, pobj.closeBtn);

			fc.utobj().printTestStep("Verify The Enable KPI");

			boolean isTextFoundA = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () , '" + category + "')]/ancestor::tr/td[contains(text () , 'Yes')]");
			if (isTextFoundA == false) {
				fc.utobj().throwsException("KPI not Enable !");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}
}