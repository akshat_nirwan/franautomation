package com.builds.test.fin;

import java.util.Map;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;
import com.builds.uimaps.fin.AdminFinanceProfitLossCategoriesPage;
import com.builds.utilities.FranconnectUtil;
import com.builds.utilities.TestCase;


public class AdminFinanceProfitLossCategoriesPageTest {
	FranconnectUtil fc = new FranconnectUtil();

	@Test(groups = { "finance", "financeFC" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-05-15", testCaseId = "TC_Admin_FIN_Add_New_Main_Category", testCaseDescription = "Verify Add , Modify And Delete New Main Category")
	public void addIncomeCategory() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());
		//Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("finance", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		String categoryName = fc.utobj().generateTestData(dataSet.get("categoryName"));
		//String calculationType = "Addition";
		
		try {
			driver = fc.loginpage().login(driver);
			
			categoryName = addNewMainCategory(driver, categoryName);
			
			fc.utobj().printTestStep("Verify Added Main Category");
			
			boolean isTextPresent = fc.utobj().assertPageSource(driver, categoryName);
			
			if (isTextPresent == false) {
				fc.utobj().throwsException("was not able to add New Main Category");
			}
			
			fc.utobj().printTestStep("Modify Added Main Category");
			
			threeDotAction(driver, categoryName, "Modify");
			
			categoryName = modifyMainCategory(driver, categoryName);
			
			fc.utobj().printTestStep("Verify Modified Main Category");
			
			boolean isTextPresentM = fc.utobj().assertPageSource(driver, categoryName);
			
			if (isTextPresentM == false) {
				fc.utobj().throwsException("was not able to Modify Added New Main Category");
			}
			
			fc.utobj().printTestStep("Delete Added Main Category");
			
			threeDotAction(driver, categoryName, "Delete");
			
			fc.utobj().acceptAlertBox(driver);
			
			fc.utobj().printTestStep("Verify Deleted Main Category");
			
			boolean isTextPresentD = fc.utobj().assertPageSource(driver, categoryName);
			
			if (isTextPresentD == true) {
				fc.utobj().throwsException("was not able to Delete Added New Main Category");
			}
			
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
			
		} catch (Exception e) {
			
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "finance" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-05-15", testCaseId = "TC_Admin_FIN_Add_Sub_Category", testCaseDescription = "Verify Add , Modify , Add Sub Category And Delete Sub Category")
	public void addModifyDeleteSubCategories() throws Exception {
		
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());
		Map<String, String> dataSet = fc.utobj().readTestData("finance", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		String categoryName = fc.utobj().generateTestData(dataSet.get("categoryName"));
		String subCategoryName = fc.utobj().generateTestData(dataSet.get("subCategoryName"));
		
		try {
			driver = fc.loginpage().login(driver);
			
			fc.utobj().printTestStep("Navigate To Admin > Finance > Configure Profit & Loss Categories");
			
			fc.utobj().printTestStep("Add Main Category");
			
			categoryName = addNewMainCategory(driver, categoryName);

			fc.utobj().printTestStep("Add Sub Category");
			
			threeDotAction(driver, categoryName, "Add Sub-Categories");
			
			subCategoryName = addSubCategory(driver, categoryName, subCategoryName);
			
			fc.utobj().printTestStep("Verify Added Sub Category In New Main Category");
			
			boolean isTextPresent = fc.utobj().assertPageSource(driver, subCategoryName);
			
			if (isTextPresent == false) {
				
				fc.utobj().throwsException("was not able to add sub category in main category");
			}

			fc.utobj().printTestStep("Modify Sub Category");
			
			threeDotAction(driver, subCategoryName, "Modify");
			
			subCategoryName = fc.utobj().generateTestData(dataSet.get("subCategoryName"));
			
			subCategoryName = addSubCategory(driver, categoryName, subCategoryName);
			
			fc.utobj().printTestStep("Verify Modify Added Sub Category In New Main Category");
			
			boolean isTextPresentM = fc.utobj().assertPageSource(driver, subCategoryName);
			
			if (isTextPresentM == false) {
				
				fc.utobj().throwsException("was not able to Modified Added Sub category in main category");
			}
			
			fc.utobj().printTestStep("Add Sub Category In Sub Category");
			
			threeDotAction(driver, subCategoryName, "Add Sub-Categories");
			
			subCategoryName = fc.utobj().generateTestData(dataSet.get("subCategoryName"));
			
			subCategoryName = addSubCategory(driver, categoryName, subCategoryName);
		
			fc.utobj().printTestStep("Verify Added Sub Category In Sub Category");
			
			boolean isTextPresentA = fc.utobj().assertPageSource(driver, subCategoryName);
			
			if (isTextPresentA == false) {
				
				fc.utobj().throwsException("was not able to add sub category of Sub Category");
			}

			fc.utobj().printTestStep("Modify Sub Sub Category");
			
			threeDotAction(driver, subCategoryName, "Modify");
			
			subCategoryName = fc.utobj().generateTestData(dataSet.get("subCategoryName"));
			
			subCategoryName = addSubCategory(driver, categoryName, subCategoryName);
			
			fc.utobj().printTestStep("Verify Modify Added Sub Sub Category In New Main Category");
			
			boolean isTextPresentN = fc.utobj().assertPageSource(driver, subCategoryName);
			
			if (isTextPresentN == false) {
				
				fc.utobj().throwsException("was not able to Modified Added Sub category in main category");
			}
			
			fc.utobj().printTestStep("Delete Sub Category of sub category");
			
			threeDotAction(driver, subCategoryName, "Delete");
			
			fc.utobj().acceptAlertBox(driver);

			fc.utobj().printTestStep("Verify Deleted Added Sub Category of Sub Category");
			
			boolean isTextPresentD = fc.utobj().assertPageSource(driver, subCategoryName);
			
			if (isTextPresentD == true) {
				
				fc.utobj().throwsException("was not able to Delete Added Sub Category of Sub Category");
			}
			
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
			
		} catch (Exception e) {
			
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}
	
	@Test(groups = { "finance" })
	@TestCase(createdOn = "2018-05-15", updatedOn = "2018-05-15", testCaseId = "TC_Admin_FIN_Add_Sub_Category_1", testCaseDescription = "Verify delete of Sub Category")
	public void deleteSubCategory() throws Exception {
		
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());
		Map<String, String> dataSet = fc.utobj().readTestData("finance", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		String categoryName = fc.utobj().generateTestData(dataSet.get("categoryName"));
		String subCategoryName = fc.utobj().generateTestData(dataSet.get("subCategoryName"));
		
		try {
			driver = fc.loginpage().login(driver);
			
			fc.utobj().printTestStep("Navigate To Admin > Finance > Configure Profit & Loss Categories");
			
			fc.utobj().printTestStep("Add Main Category");
			
			categoryName = addNewMainCategory(driver, categoryName);

			fc.utobj().printTestStep("Add Sub Category");
			
			threeDotAction(driver, categoryName, "Add Sub-Categories");
			
			subCategoryName = addSubCategory(driver, categoryName, subCategoryName);
			
			fc.utobj().printTestStep("Verify Added Sub Category In New Main Category");
			
			boolean isTextPresent = fc.utobj().assertPageSource(driver, subCategoryName);
			
			if (isTextPresent == false) {
				
				fc.utobj().throwsException("was not able to add sub category in main category");
			}

			fc.utobj().printTestStep("Delete Sub Category");
			
			threeDotAction(driver, subCategoryName, "Delete");
			
			fc.utobj().acceptAlertBox(driver);

			fc.utobj().printTestStep("Verify Deleted Added Sub Category In New Main Category");
			
			boolean isTextPresentD = fc.utobj().assertPageSource(driver, subCategoryName);
			
			if (isTextPresentD == true) {
				
				fc.utobj().throwsException("was not able to Delete Added Sub Category in main category");
			}
			
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
			
		} catch (Exception e) {
			
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}
	

	@Test(groups = { "finance" })
	@TestCase(createdOn = "2018-05-15", updatedOn = "2018-05-15", testCaseId = "TC_Admin_FIN_PL_SubCat_Reorder", testCaseDescription = "Verify Reordering of Sub Category")
	public void reorderOfSubCategories() throws Exception {
		
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());
		Map<String, String> dataSet = fc.utobj().readTestData("finance", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		String categoryName = fc.utobj().generateTestData(dataSet.get("categoryName"));
		
		try {
			driver = fc.loginpage().login(driver);
			
			AdminFinanceProfitLossCategoriesPage pobj = new AdminFinanceProfitLossCategoriesPage(driver);

			fc.utobj().printTestStep("Navigate To Admin > Finance > Configure Profit & Loss Categories");
			
			fc.utobj().printTestStep("Add Main Category");
						
			categoryName = addNewMainCategory(driver, categoryName);
			
			fc.utobj().printTestStep("Add Sub Category");
			
			String subCategoryName = fc.utobj().generateTestData(dataSet.get("subCategoryName"));
			
			threeDotAction(driver, categoryName, "Add Sub-Categories");
			
			addSubCategory(driver, categoryName, subCategoryName);
			
			subCategoryName = fc.utobj().generateTestData(dataSet.get("subCategoryName"));
		
			threeDotAction(driver, categoryName, "Add Sub-Categories");
			
			addSubCategory(driver, categoryName, subCategoryName);
			
			subCategoryName = fc.utobj().generateTestData(dataSet.get("subCategoryName"));
			
			threeDotAction(driver, categoryName, "Add Sub-Categories");
			
			String subCategoryName3 = addSubCategory(driver, categoryName, subCategoryName);
			
			threeDotAction(driver, categoryName, "Order Sub-Categories");

			fc.commonMethods().switch_frameClass_newLayoutcboxIframe(driver);
			
			fc.utobj().selectDropDownByVisibleText(driver, pobj.selectCatForReorder, subCategoryName3);
			
			fc.utobj().clickElementWithoutMove(driver, pobj.moveToTop);
			
			fc.utobj().clickElement(driver, pobj.changeCategorySequence);
			
			fc.utobj().clickElement(driver, pobj.closeBtn);
			
			fc.utobj().switchFrameToDefault(driver);
			
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
			
		} catch (Exception e) {
			
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "finance" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-05-15", testCaseId = "TC_Admin_FIN_PLCat_Reordering", testCaseDescription = "Verify Reordering of Category")
	public void reorderOfCategory() throws Exception {
		
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());
		Map<String, String> dataSet = fc.utobj().readTestData("finance", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		
		try {
			driver = fc.loginpage().login(driver);
			
			AdminFinanceProfitLossCategoriesPage pobj = new AdminFinanceProfitLossCategoriesPage(driver);

			fc.utobj().printTestStep("Navigate To Admin > Finance > Configure Profit & Loss Categories");
			
			fc.utobj().printTestStep("Add TWo Main Category");
			
			String categoryName = fc.utobj().generateTestData(dataSet.get("categoryName"));
			
			addNewMainCategory(driver, categoryName);
			
			categoryName = fc.utobj().generateTestData(dataSet.get("categoryName"));
			
			String categoryName2 = addNewMainCategory(driver, categoryName);

			//WebElement element1 = driver.findElement(By.xpath(".//tbody[@class='radio-group']/tr[1]/td[contains(text(),'"+categoryName2+"')]"));
			
			fc.utobj().clickElement(driver, pobj.clickButtonThreeDots);
			
			fc.utobj().getElement(driver, pobj.orderMainCategory).click();
			
			fc.commonMethods().switch_frameClass_newLayoutcboxIframe(driver);
			
			fc.utobj().selectDropDownByVisibleText(driver, pobj.selectCatForReorder, categoryName2);
			
			fc.utobj().clickElementWithoutMove(driver, pobj.moveToTop);
			
			fc.utobj().clickElement(driver, pobj.changeCategorySequence);
			
			fc.utobj().clickElement(driver, pobj.closeBtn);
			
			fc.utobj().switchFrameToDefault(driver);
			
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
			
		} catch (Exception e) {
			
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}
	
	
	@Test(groups = "finance")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-05-15", testCaseId = "TC_Admin_FIN_Configure_Additional_Display_Row", testCaseDescription = "Verify Add , Modify And Delete New Display Row")
	public void configureAdditionalDisplayRow() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("finance", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			
			AdminFinanceProfitLossCategoriesPage pobj = new AdminFinanceProfitLossCategoriesPage(driver);

			fc.utobj().printTestStep("Navigate To Admin > Finance > Configure Profit & Loss Categories");
			
			fc.utobj().printTestStep("Add Main Category");
			
			String categoryName = fc.utobj().generateTestData(dataSet.get("categoryName"));
			
			addNewMainCategory(driver, categoryName);

			fc.utobj().printTestStep(
					"Navigate To Admin > Finance > Configure Profit & Loss Categories > Configure Additional Display Rows");
			
			fc.utobj().clickElement(driver, pobj.clickButtonThreeDots);
			
			fc.utobj().getElement(driver, pobj.configureAdditionalDisplayRows).click();

			fc.utobj().printTestStep("Add New Row");

			fc.utobj().clickElement(driver, pobj.addNewRow);
			
			fc.commonMethods().switch_frameClass_newLayoutcboxIframe(driver);
			
			String displayName = fc.utobj().generateTestData(dataSet.get("displayName"));
			
			fc.utobj().sendKeys(driver, pobj.displayName, displayName);
			
			fc.utobj()
					.selectValFromMultiSelect(
							driver, pobj.selectCategory, fc.utobj().getElement(driver, pobj.selectCategory)
									.findElement(By.xpath("./div/div/input[@class='searchInputMultiple']")),
							categoryName);
			
			fc.utobj().clickElement(driver, pobj.addBtn);

			fc.utobj().clickElement(driver, pobj.closeBtn);
			
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Verify Add New Row");
			
			boolean isTextPresentD = fc.utobj().assertPageSource(driver, displayName);
			if (isTextPresentD == false) {
				fc.utobj().throwsException("was not able to Add New Row");
			}

			fc.utobj().printTestStep("Modify Added Row");
			
			threeDotAction(driver, displayName, "Modify");

			fc.commonMethods().switch_frameClass_newLayoutcboxIframe(driver);
			
			displayName = fc.utobj().generateTestData(dataSet.get("displayName"));
		
			fc.utobj().sendKeys(driver, pobj.displayName, displayName);
			
			fc.utobj().setToDefault(driver, pobj.selectCategory);
			
			fc.utobj()
					.selectValFromMultiSelect(
							driver, pobj.selectCategory, fc.utobj().getElement(driver, pobj.selectCategory)
									.findElement(By.xpath("./div/div/input[@class='searchInputMultiple']")),
							categoryName);
			fc.utobj().clickElement(driver, pobj.modifyBtn);

			fc.utobj().clickElement(driver, pobj.closeBtn);
			
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Verify Modified Added New Row");
			
			boolean isTextPresentM = fc.utobj().assertPageSource(driver, displayName);
			if (isTextPresentM == false) {
				fc.utobj().throwsException("was not able to Modify Added New Row");
			}

			fc.utobj().printTestStep("Delete Added Row");
			
			threeDotAction(driver, displayName, "Delete");
			
			fc.utobj().acceptAlertBox(driver);

			fc.utobj().printTestStep("Verify Deleted New Row");
			
			boolean isTextPresentA = fc.utobj().assertPageSource(driver, displayName);
		
			if (isTextPresentA == true) {
				fc.utobj().throwsException("was not able to Delete Added New Row");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	public String addNewMainCategory(WebDriver driver, String categoryName) throws Exception {

		AdminFinanceProfitLossCategoriesPage pobj = new AdminFinanceProfitLossCategoriesPage(driver);
		
		fc.finance().finance_common().adminFinConfigureProfitLossCategoriesLnk(driver);
		
		boolean isTextPresent = fc.utobj().assertPageSource(driver, categoryName);
		
		if (isTextPresent == false) {
			
			fc.utobj().clickElement(driver, pobj.clickButtonThreeDots);
			
			fc.utobj().getElement(driver, pobj.addNewMainCategory).click();
			
			fc.commonMethods().switch_frameClass_newLayoutcboxIframe(driver);
			
			fc.utobj().sendKeys(driver, pobj.categoryName, categoryName);
			
			if (!fc.utobj().isSelected(driver,pobj.calculationMethod1)) {
				
				fc.utobj().clickElement(driver, pobj.additionLabel);
			}
			
			fc.utobj().clickElement(driver, pobj.addButton);

			fc.utobj().clickElement(driver, pobj.closeBtn);
			
			fc.utobj().switchFrameToDefault(driver);
			
		}		
		return categoryName;
	}

	public String modifyMainCategory(WebDriver driver, String categoryName) throws Exception {

		AdminFinanceProfitLossCategoriesPage pobj = new AdminFinanceProfitLossCategoriesPage(driver);

		fc.commonMethods().switch_frameClass_newLayoutcboxIframe(driver);
		
		fc.utobj().sendKeys(driver, pobj.categoryName, categoryName);

		if (!fc.utobj().isSelected(driver,pobj.calculationMethod2)) {
			
			fc.utobj().clickElement(driver, pobj.substractionLabel);
		}

		fc.utobj().clickElement(driver, pobj.addButton);

		fc.utobj().clickElement(driver, pobj.closeBtn);
		
		fc.utobj().switchFrameToDefault(driver);

		return categoryName;
	}
	
	public String addSubCategory(WebDriver driver, String categoryName, String subCategoryName) throws Exception {
		
		AdminFinanceProfitLossCategoriesPage pobj = new AdminFinanceProfitLossCategoriesPage(driver);
		
		fc.commonMethods().switch_frameClass_newLayoutcboxIframe(driver);
				
		fc.utobj().sendKeys(driver, pobj.categoryName, subCategoryName);
		
		if (!fc.utobj().isSelected(driver,pobj.calculationMethod2)) {
			
			fc.utobj().clickElement(driver, pobj.substractionLabel);
		}
		
		fc.utobj().clickElement(driver, pobj.addButton);
		
		fc.utobj().clickElement(driver, pobj.closeBtn);
		
		fc.utobj().switchFrameToDefault(driver);
		
		return subCategoryName;
	}

	public String modifySubCategory(WebDriver driver, String categoryName, String subCategoryName) throws Exception {

		AdminFinanceProfitLossCategoriesPage pobj = new AdminFinanceProfitLossCategoriesPage(driver);

		fc.commonMethods().switch_frameClass_newLayoutcboxIframe(driver);
		
		fc.utobj().sendKeys(driver, pobj.categoryName, subCategoryName);

		if (!fc.utobj().isSelected(driver,pobj.calculationMethod2)) {
		
			fc.utobj().clickElement(driver, pobj.additionLabel);
		}

		fc.utobj().clickElement(driver, pobj.addButton);

		fc.utobj().clickElement(driver, pobj.closeBtn);
		
		fc.utobj().switchFrameToDefault(driver);

		return subCategoryName;
	}
	
	public void threeDotAction(WebDriver driver, String testData, String option) throws Exception {

		fc.utobj().clickElement(driver, driver.findElement(By.xpath(
				".//*[contains(text () ,'" + testData + "')]/./following-sibling::td//div[@class='list-name']")));

		WebElement element = driver.findElement(By.xpath(".//*[contains(text () ,'" + testData
				+ "')]/./following-sibling::td//div[@class='list-name']/following-sibling::ul/li/a[contains(text () ,'"
				+ option + "')]"));
		fc.utobj().clickElement(driver, element);

	}
}
