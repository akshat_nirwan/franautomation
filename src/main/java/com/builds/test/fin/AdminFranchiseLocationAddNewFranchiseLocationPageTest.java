package com.builds.test.fin;

import java.util.Map;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.Test;
import com.builds.uimaps.admin.AdminAreaRegionAddAreaRegionPage;
import com.builds.uimaps.fin.AdminFranchiseLocationAddNewFranchiseLocationPage;
import com.builds.utilities.FranconnectUtil;
import com.builds.utilities.TestCase;

public class AdminFranchiseLocationAddNewFranchiseLocationPageTest {
	FranconnectUtil fc = new FranconnectUtil();
	
	public String addFranchiseLocation(WebDriver driver, String franchiseId, String agreementVersion, Map<String, String> config) throws Exception {

		String testCaseId = "TC_Add_Franchise_Location_All_Admin_Agreement_Version";
		
		if (fc.utobj().validate(testCaseId)) {
			
			try {
				
				String regionName = addAreaRegionWithState(driver);
				
				AdminFranchiseLocationAddNewFranchiseLocationPage pobj = new AdminFranchiseLocationAddNewFranchiseLocationPage(driver);
				
				fc.adminpage().adminPage(driver);
				
				fc.adminpage().openManageFranchiseLocation(driver);
				
				boolean isTextPresent = fc.utobj().assertLinkText(driver, franchiseId);
				
				if (isTextPresent == false) {
					
					try {
						boolean isPagePresent = fc.utobj().isElementPresent(driver, driver.findElement(By.xpath(".//*[contains(text(), 'Show All')]")));
						
						if(isPagePresent==true){
						
							fc.utobj().clickElement(driver, driver.findElement(By.xpath(".//*[contains(text(), 'Show All')]")));
							
						}
						
					} catch (Exception e) {
					
					}
				}
				
				isTextPresent = fc.utobj().assertLinkText(driver, franchiseId);
				if (isTextPresent == false)
				{
				
					fc.adminpage().adminAddNewFranchiseLocationLnk(driver);
					
					fc.utobj().sendKeys(driver, pobj.franchiseID, franchiseId);
				
					fc.utobj().sendKeys(driver, pobj.centerName, "Test Center");
				
					fc.utobj().selectDropDown(driver, pobj.areaRegionName, regionName);
					
					String curDate = fc.utobj().getFutureDateUSFormat(-365);
					
					fc.utobj().selectDropDown(driver, pobj.storeType, "Default");
					
					if(!agreementVersion.isEmpty()) {
					fc.utobj().selectDropDown(driver, pobj.versionID, agreementVersion);			
					
					fc.utobj().sendKeys(driver, pobj.RRSD, curDate);
					}
					
					fc.utobj().sendKeys(driver, pobj.openingDate, curDate);
					
					fc.utobj().sendKeys(driver, pobj.city, "test city");
					
					fc.utobj().selectDropDown(driver, pobj.StateProvince, 1);
				
					fc.utobj().sendKeys(driver, pobj.storePhone, "0001");
								
					fc.utobj().sendKeys(driver, pobj.ownerFirstName, "Fran");
					
					fc.utobj().sendKeys(driver, pobj.ownerLastName, "Automation");
				
					fc.utobj().clickElement(driver, pobj.submit);
					
					String loginID = fc.utobj().generateTestData("f1");
				
					fc.utobj().sendKeys(driver, pobj.loginid, loginID);
				
					String password = "sdg@1a@Hfs4";
					fc.utobj().sendKeys(driver, pobj.enterPass, password);
				
					fc.utobj().sendKeys(driver, pobj.confirmPass, password);
				
					driver.findElement(By.id("ms-parentroleID")).click();
				
					driver.findElement(By.xpath("//input[@class='searchInputMultiple']")).sendKeys("Default Franchise Role");
				
					driver.findElement(By.id("selectAll")).click();
				
					fc.utobj().sendKeys(driver, pobj.jobTitle, "Test");

					fc.utobj().selectDropDownByVisibleText(driver, driver.findElement(By.id("users")), "Owner");
				
					fc.utobj().sendKeys(driver, pobj.ownerEmail, "automation@franconnect.com");
				
					fc.utobj().clickElement(driver, pobj.submit);
				}
				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
				
			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException("was not able to add Franchise Location_All");

		}
		return franchiseId;
	}
	
	public String addAreaRegionWithState(WebDriver driver)
			throws Exception {
		fc.adminpage().adminPage(driver);
		fc.adminpage().openManageAreaRegion(driver);
		
		String regionName = "TestRegionGT";
		
		boolean isTextPresent = fc.utobj().assertLinkText(driver, regionName);
		
		if (isTextPresent == false) {
			try {
				boolean isPagePresent = fc.utobj().isElementPresent(driver, driver.findElement(By.xpath(".//*[contains(text(), 'Show All')]")));
				
				if(isPagePresent==true){
				fc.utobj().clickElement(driver, driver.findElement(By.xpath(".//*[contains(text(), 'Show All')]")));
				}
			} catch (Exception e) {
				// TODO: handle exception
			}
			
		isTextPresent = fc.utobj().assertLinkText(driver, regionName);
			}
		
		if (isTextPresent==false) {
			
			AdminAreaRegionAddAreaRegionPage pobj = new AdminAreaRegionAddAreaRegionPage(driver);
			
			fc.utobj().moveToElement(driver, driver.findElement(By.xpath(".//input[@value='Add Area / Region']")));
			
			fc.utobj().clickElement(driver, driver.findElement(By.xpath(".//input[@value='Add Area / Region']")));
			
			fc.utobj().selectDropDown(driver, pobj.category, "Domestic");
			
			fc.utobj().sendKeys(driver, pobj.aregRegionName, regionName);
			
			fc.utobj().selectDropDown(driver, pobj.groupBy, "States");
			
			fc.utobj().selectDropDownByVisibleText(driver, pobj.groupBy, "States");
			
			fc.utobj().clickElement(driver, pobj.all);
			
			fc.utobj().clickElement(driver, driver.findElement(By.xpath(".//*[@id='0_1']")));
			
			fc.utobj().clickElement(driver, pobj.Submit);
			
			fc.utobj().clickElement(driver, pobj.nextBtn);
		}
		return regionName;
	}
	
	@Test(groups = { "finance" })
	@TestCase(createdOn = "2018-06-25", updatedOn = "2018-08-16", testCaseId = "TC_FC_Verify_Fin_Privileges_In_Default_Roles", testCaseDescription = "To verify Finance Privileges in Default Roles")
	public void verifyFinancePrivilegesInDefaultRole() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());
		
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		
		try {
			driver = fc.loginpage().login(driver);
			
			fc.utobj().printTestStep("Navigate to Admin > Roles");
			fc.adminpage().adminUsersRolesPage(driver);

			fc.utobj().printTestStep("View Regional Default Role");
			fc.utobj().clickElement(driver, ".//*[contains(text(), 'Default Regional Role')]");
			
			fc.utobj().printTestStep( "Verify Financials privileges under Default Regional Role");
			boolean isTextPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,".//*[contains(text() , 'Grants access to Financials module')]");

			if (isTextPresent == false) {
				fc.utobj().throwsException("Not able to verify Financials privileges Under Default Regional Role");
			}
			
			fc.utobj().printTestStep("Navigate to Admin > Roles");
			fc.adminpage().adminUsersRolesPage(driver);

			fc.utobj().printTestStep("View Franchise Default Role");
			fc.utobj().clickElement(driver, ".//*[contains(text(), 'Default Franchise Role')]");
			
			fc.utobj().printTestStep( "Verify Financials privileges under Default Franchise Role");
			boolean isTextPresent1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,".//*[contains(text() , 'Grants access to Financials module')]");

			if (isTextPresent1 == false) {
				fc.utobj().throwsException("Not able to verify Financials privileges Under Default Franchise Role");
			}
			
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
			
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}
	
	
	public String addFranchiseLocationWithTax(WebDriver driver, String franchiseId, String agreementVersion, String taxRateName, Map<String, String> config) throws Exception {

		String testCaseId = "TC_Add_Franchise_Location_All_Admin_Agreement_Version";
		
		if (fc.utobj().validate(testCaseId)) {
			
			try {
				
				String regionName = addAreaRegionWithState(driver);
				
				AdminFranchiseLocationAddNewFranchiseLocationPage pobj = new AdminFranchiseLocationAddNewFranchiseLocationPage(driver);
				
				fc.adminpage().adminPage(driver);
				
				fc.adminpage().openManageFranchiseLocation(driver);
				
				boolean isTextPresent = fc.utobj().assertLinkText(driver, franchiseId);
				
				if (isTextPresent == false) {
					
					try {
						boolean isPagePresent = fc.utobj().isElementPresent(driver, driver.findElement(By.xpath(".//*[contains(text(), 'Show All')]")));
						
						if(isPagePresent==true){
						
							fc.utobj().clickElement(driver, driver.findElement(By.xpath(".//*[contains(text(), 'Show All')]")));
							
						}
						
					} catch (Exception e) {
					
					}
				}
				
				isTextPresent = fc.utobj().assertLinkText(driver, franchiseId);
				if (isTextPresent == false)
				{
				
					fc.adminpage().adminAddNewFranchiseLocationLnk(driver);
					
					fc.utobj().sendKeys(driver, pobj.franchiseID, franchiseId);
				
					fc.utobj().sendKeys(driver, pobj.centerName, "Test Center");
				
					fc.utobj().selectDropDown(driver, pobj.areaRegionName, regionName);
					
					fc.utobj().selectDropDown(driver, pobj.storeType, "Default");
														
					String curDate = fc.utobj().getFutureDateUSFormat(-365);
					
					if (!agreementVersion.isEmpty()) {
						
						fc.utobj().selectDropDown(driver, pobj.versionID, agreementVersion);
						
						fc.utobj().sendKeys(driver, pobj.RRSD, curDate);
					}
					
					boolean isTaxRatePresent = fc.utobj().assertPageSource(driver, "Tax Rate");
					
					if (isTaxRatePresent==true) {
						
						fc.utobj().clickElement(driver, pobj.taxRate);
						
						fc.utobj().clickElement(driver, driver.findElement(By.xpath(".//*[contains(text(), '"+taxRateName+"')]")));
					}
									
					fc.utobj().sendKeys(driver, pobj.openingDate, curDate);
					
					fc.utobj().sendKeys(driver, pobj.city, "test city");

					fc.utobj().selectDropDown(driver, pobj.StateProvince, 1);
				
					fc.utobj().sendKeys(driver, pobj.storePhone, "0001");
								
					fc.utobj().sendKeys(driver, pobj.ownerFirstName, "Fran");
					
					fc.utobj().sendKeys(driver, pobj.ownerLastName, "Automation");
				
					fc.utobj().clickElement(driver, pobj.submit);
					String loginID = fc.utobj().generateTestData("f1");
				
					fc.utobj().sendKeys(driver, pobj.loginid, loginID);
				
					String password = "sdg@1a@Hfs4";
					fc.utobj().sendKeys(driver, pobj.enterPass, password);
				
					fc.utobj().sendKeys(driver, pobj.confirmPass, password);
				
					driver.findElement(By.id("ms-parentroleID")).click();
				
					driver.findElement(By.xpath("//input[@class='searchInputMultiple']")).sendKeys("Default Franchise Role");
				
					driver.findElement(By.id("selectAll")).click();
				
					fc.utobj().sendKeys(driver, pobj.jobTitle, "Test");

					fc.utobj().selectDropDownByVisibleText(driver, driver.findElement(By.id("users")), "Owner");
				
					fc.utobj().sendKeys(driver, pobj.ownerEmail, "automation@franconnect.com");
				
					fc.utobj().clickElement(driver, pobj.submit);
				}
				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
				
			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException("was not able to add Franchise Location_All");

		}
		return franchiseId;
	}

	
}