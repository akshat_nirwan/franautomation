package com.builds.test.fin;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import com.builds.test.admin.AdminPageTest;
import com.builds.test.common.FCHomePageTest;
import com.builds.utilities.FranconnectUtil;

public class FIN_Common extends FranconnectUtil {
	FranconnectUtil fc = new FranconnectUtil();

	//Added and Modified by Gaurav Tomar
	
	public void adminFinSetupPreferencesPage(WebDriver driver) throws Exception {
		utobj().printTestStep("Navigating to Admin Fin Setup Preferences Page");
		fc.adminpage().adminPage(driver);
		AdminPageTest p2 = new AdminPageTest();
		p2.openAdminFinanceSetupPreferencesLnk(driver);
	}

	
	public void adminFinAgreementVersionsPage(WebDriver driver) throws Exception {
		utobj().printTestStep("Navigating to Admin Fin Agreement Versions Page");
		fc.adminpage().adminPage(driver);
		AdminPageTest p2 = new AdminPageTest();
		p2.openAdminFinAgreementVersionsPage(driver);
	}

	public void adminFinConfigureAdditionalInvoiceItemPage(WebDriver driver) throws Exception {
		utobj().printTestStep("Navigating to Admin Fin Configure Additional Invoice Item Page");
		fc.adminpage().adminPage(driver);
		AdminPageTest p2 = new AdminPageTest();
		p2.openAdminFinConfigureAdditionalInvoiceItemPage(driver);
	}
	
	public void adminFinConfigureHeadingsforSalesPage(WebDriver driver) throws Exception {
		utobj().printTestStep("Navigating to Admin Fin Configure Headings for Sales Page");
		AdminPageTest p2 = new AdminPageTest();
		p2.openAdminFinConfigureHeadingsforSalesPage(driver);
	}
	
	public void adminFinConfigureCategoriesforSalesReportPage(WebDriver driver) throws Exception {
		utobj().printTestStep("Navigating to Admin Fin Configure Categories for Sales Report Page");
		fc.adminpage().adminPage(driver);
		AdminPageTest p2 = new AdminPageTest();
		p2.openAdminFinConfigureCategoriesforSalesReportPage(driver);
	}

	public void adminFinConfigureNonFinancialKPICategoriesforSalesReportLnk(WebDriver driver) throws Exception {
		utobj().printTestStep("Navigating to Admin Fin Configure Non Financial KPI Categories for Sales Report Lnk");
		fc.adminpage().adminPage(driver);
		AdminPageTest p2 = new AdminPageTest();
		p2.openAdminFinConfigureNonFinancialKPICategoriesforSalesReportLnk(driver);
	}

	public void adminFinConfigureFinancialsDocumenttobeUploadedLnk(WebDriver driver) throws Exception {
		utobj().printTestStep("Navigating to Admin Fin Configure Financials Document to be Uploaded Lnk");
		fc.adminpage().adminPage(driver);
		AdminPageTest p2 = new AdminPageTest();
		p2.openAdminFinConfigureFinancialsDocumenttobeUploadedLnk(driver);
	}

	public void adminFinConfigureProfitLossCategoriesLnk(WebDriver driver) throws Exception {
		utobj().printTestStep("Navigating to Admin Fin Configure Profit Loss Categories Lnk");
		fc.adminpage().adminPage(driver);
		AdminPageTest p2 = new AdminPageTest();
		p2.openAdminFinConfigureProfitLossCategoriesLnk(driver);
	}

	public void adminFinEnableDisableFranchiseforEFTLnk(WebDriver driver) throws Exception {
		utobj().printTestStep("Navigating to Admin Fin Enable / Disable Franchise for EFT");
		fc.adminpage().adminPage(driver);
		AdminPageTest p2 = new AdminPageTest();
		p2.openAdminFinEnableDisableFranchiseforEFTLnk(driver);
	}

	public void adminFinEnableDisableAreaFranchiseforEFTLnk(WebDriver driver) throws Exception {
		utobj().printTestStep("Navigating to Admin Fin Enable / Disable Area Franchise for EFT");
		fc.adminpage().adminPage(driver);
		AdminPageTest p2 = new AdminPageTest();
		p2.openAdminFinEnableDisableAreaFranchiseforEFTLnk(driver);
	}

	public void adminFinDeleteSalesReportsLnk(WebDriver driver) throws Exception {
		utobj().printTestStep("Navigating to Admin Fin Delete Sales Reports");
		fc.adminpage().adminPage(driver);
		AdminPageTest p2 = new AdminPageTest();
		p2.openAdminFinDeleteSalesReportsLnk(driver);
	}

	public void adminFinConfigureTaxRatesLnk(WebDriver driver) throws Exception {
		utobj().printTestStep("Navigating to Admin > Finance > Configure Tax Rates");
		fc.adminpage().adminPage(driver);
		AdminPageTest p2 = new AdminPageTest();
		p2.openAdminFinConfigureTaxRatesLnk(driver);
	}

	public void financeSalesPage(WebDriver driver) throws Exception {
		utobj().printTestStep("Navigating to Finance > Sales Page");
		fc.finance().finance_common().openFinanceSalesPage(home_page(), driver);
	}

	public void financeKPIPage(WebDriver driver) throws Exception {
		utobj().printTestStep("Navigating to Finance > KPI Page");
		fc.finance().finance_common().openFinanceKPIPage(home_page(), driver);
	}

	public void financeProfitAndLossStatementPage(WebDriver driver) throws Exception {
		utobj().printTestStep("Navigating to Finance > Profit & Loss Statement > Profit / Loss Summary Page");
		fc.finance().finance_common().openFinanceProfitAndLossStatementPage(home_page(), driver);
	}

	public void financeRoyaltiesPage(WebDriver driver) throws Exception {
		utobj().printTestStep("Navigating to Finance > Royalties Page");
		fc.finance().finance_common().openFinanceRoyaltiesPage(home_page(), driver);
	}

	public void financePaymentsPage(WebDriver driver) throws Exception {
		utobj().printTestStep("Navigating to Finance > Payments Page");
		fc.finance().finance_common().openFinancePaymentsPage(home_page(), driver);
	}
	
	public void financeEFTTransactionsPage(WebDriver driver) throws Exception {
		utobj().printTestStep("Navigating to Finance > EFT Transactions Page");
		fc.finance().finance_common().openFinanceEFTTransactionsPage(home_page(), driver);
	}
	
	public void financeAreaFranchisePage(WebDriver driver) throws Exception {
		utobj().printTestStep("Navigating to Finance > Area Franchise");
		fc.finance().finance_common().openFinanceAreaFranchisePage(home_page(), driver);
	}
	
	public void financeStoreSummaryPage(WebDriver driver) throws Exception {
		utobj().printTestStep("Navigating to Finance > Area Franchise");
		fc.finance().finance_common().openFinanceStoreSummaryPage(home_page(), driver);
	}
	
	public void financeReportsPage(WebDriver driver) throws Exception {
		utobj().printTestStep("Navigating to Finance > Reports Page");
		fc.finance().finance_common().opneFinanceReportsPage(home_page(), driver);
	}

	private void openFinanceSalesPage(FCHomePageTest fcHomePageTest, WebDriver driver) throws Exception {
		fcHomePageTest.openFinanceModule(driver);
		driver.findElement(By.xpath(".//*[@id='module_financials']//a[@qat_submodule='Sales']")).click();
		utobj().printTestStep("Navigating to Finance > Sales Page ");
	}

	private void openFinanceKPIPage(FCHomePageTest fcHomePageTest, WebDriver driver) throws Exception {
		fcHomePageTest.openFinanceModule(driver);
		driver.findElement(By.xpath(".//*[@id='module_financials']//a[@qat_submodule='KPI']")).click();
		utobj().printTestStep("Navigating to Finance > KPI Page ");
	}

	private void openFinanceProfitAndLossStatementPage(FCHomePageTest fcHomePageTest, WebDriver driver)
			throws Exception {
		fcHomePageTest.openFinanceModule(driver);
		driver.findElement(By.xpath(".//*[@id='module_financials']//a[@qat_submodule='Profit & Loss Statement']"))
				.click();
		utobj().printTestStep("Navigating to Finance > Profit & Loss Statement > Profit / Loss Summary Page");
	}

	private void openFinanceRoyaltiesPage(FCHomePageTest fcHomePageTest, WebDriver driver) throws Exception {
		fcHomePageTest.openFinanceModule(driver);
		driver.findElement(By.xpath(".//*[@id='module_financials']//a[@qat_submodule='Royalties']")).click();
		utobj().printTestStep("Navigating to Finance > Royalties Page");
	}

	private void openFinancePaymentsPage(FCHomePageTest fcHomePageTest, WebDriver driver) throws Exception {
		fcHomePageTest.openFinanceModule(driver);
		driver.findElement(By.xpath(".//*[@id='module_financials']//a[@qat_submodule='Payments']")).click();
		utobj().printTestStep("Navigating to Finance > Payments Page");
	}

	private void openFinanceEFTTransactionsPage(FCHomePageTest fcHomePageTest, WebDriver driver) throws Exception {
		fcHomePageTest.openFinanceModule(driver);
		driver.findElement(By.xpath(".//*[@id='module_financials']//a[@qat_submodule='EFT Transactions']")).click();
		utobj().printTestStep("Navigating to Finance > EFT Transactions Page");
	}

	private void openFinanceAreaFranchisePage(FCHomePageTest fcHomePageTest, WebDriver driver) throws Exception {
		fcHomePageTest.openFinanceModule(driver);
		driver.findElement(By.xpath(".//*[@id='module_financials']//a[@qat_submodule='Area Franchise']")).click();
		utobj().printTestStep("Navigating to Finance > Area Franchise");
	}

	private void openFinanceStoreSummaryPage(FCHomePageTest fcHomePageTest, WebDriver driver) throws Exception {
		fcHomePageTest.openFinanceModule(driver);
		driver.findElement(By.xpath(".//*[@id='module_financials']//a[@qat_submodule='Store Summary']")).click();
		utobj().printTestStep("Navigating to Finance > Store Summary");
	}
	
	private void opneFinanceReportsPage(FCHomePageTest fcHomePageTest, WebDriver driver) throws Exception {
		utobj().printTestStep("Navigating to Finance > Reports Page");
		fcHomePageTest.openFinanceModule(driver);
		driver.findElement(By.xpath(".//*[@id='module_financials']//a[@qat_submodule='Reports']")).click();
	}
}
