package com.builds.test.fin;

import java.util.List;
import java.util.Map;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;
import com.builds.test.admin.AdminFranchiseLocationAddFranchiseLocationPageTest;
import com.builds.uimaps.fin.FinanceKPIPage;
import com.builds.utilities.FranconnectUtil;
import com.builds.utilities.TestCase;

public class FinanceKPIPageTest {

	FranconnectUtil fc = new FranconnectUtil();

	@Test(groups = { "finance" })
	@TestCase(createdOn = "2017-06-04", updatedOn = "2018-06-12", testCaseId = "TC_FIN_KPI_Add_Goals", testCaseDescription = "Verify The Add Goals")
	public void addGoals() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		Map<String, String> dataSet = fc.utobj().readTestData("finance", testCaseId);

		String agrmntVrsnName = fc.utobj().generateTestData(dataSet.get("agreementVersionName"));
		String royaltyPrcntg = dataSet.get("royaltyPercentage");
		String advPercentage = dataSet.get("advertisementPercentage");
		String royaltyAreaFranchiseValue = dataSet.get("RoyaltyAreaFranchise");
		String advAreaFranchiseValue = dataSet.get("AdvAreaFranchise");

		try {
			driver = fc.loginpage().login(driver);
			FinanceKPIPage pobj = new FinanceKPIPage(driver);

			fc.utobj().printTestStep("Navigate To Admin > Finance > Non-Financial / KPI Categories Summary");
			fc.utobj().printTestStep("Add New Non-Financial Category");
			String category = fc.utobj().generateTestData("TestKPICategory");
			category = new AdminFinanceNonFinancialKPICategoriesSummaryPageTest().addNewNonFinancialsCategory(driver,
					config, category);

			fc.utobj().printTestStep("Navigate To Admin > Finance > Agreement Versions");
			AdminFinanceAgreementVersionsPageTest agreementforSales = new AdminFinanceAgreementVersionsPageTest();

			fc.utobj().printTestStep("Add Agreement");
			String agreementVersion = agreementforSales.addAgreement(driver, config, agrmntVrsnName, royaltyPrcntg,
					advPercentage, royaltyAreaFranchiseValue, advAreaFranchiseValue);

			String regionName = fc.utobj().generateTestData("GT");
			String storeType = fc.utobj().generateTestData("GT");
			String franchiseId = fc.utobj().generateTestData("GT");
			AdminFranchiseLocationAddFranchiseLocationPageTest franchise = new AdminFranchiseLocationAddFranchiseLocationPageTest();
			franchiseId = franchise.addFranchiseLocation_All_Agreement(driver, franchiseId, regionName, storeType,
					agreementVersion, config);

			fc.utobj().printTestStep("Navigate To Finance > KPI > Goals Summary Page");
			fc.finance().finance_common().financeKPIPage(driver);
			fc.utobj().clickElement(driver, pobj.goalsTab);

			fc.utobj().printTestStep("Add Goals");
			fc.utobj().clickElement(driver, pobj.addGoals);
			fc.utobj().selectDropDownByIndex(driver, pobj.selectGoalYear, 1);

			fc.utobj().setToDefault(driver, pobj.selectFranchise);
			fc.utobj().selectValFromMultiSelect(driver, pobj.selectFranchise, franchiseId);

			String month = fc.utobj().getCurrentMonth();

			WebElement element = driver.findElement(By.id("finGoalsReportDetails_0" + month + ""));
			double totalSales = 100.00;
			String totalSalesString = Double.toString(totalSales);
			fc.utobj().sendKeys(driver, element, totalSalesString);

			WebElement elementKPI = driver.findElement(By.id("finGoalsReportDetails_1" + month + ""));
			double totalKPI = 200.00;
			String totalKPIString = Double.toString(totalKPI);
			fc.utobj().sendKeys(driver, elementKPI, totalKPIString);
			fc.utobj().clickElement(driver, pobj.saveBtn);

			String monthSales = fc.utobj().getText(driver, driver
					.findElement(By.xpath(".//td[contains(text () , '" + month + "')]/following-sibling::td[1]")));

			if (monthSales.contains(",")) {
				monthSales = monthSales.replaceAll(",", "");
			}

			double outSales = Double.parseDouble(monthSales);

			String monthKPI = fc.utobj().getText(driver, driver
					.findElement(By.xpath(".//td[contains(text () , '" + month + "')]/following-sibling::td[2]")));
			if (monthKPI.contains(",")) {
				monthKPI = monthKPI.replaceAll(",", "");
			}

			double outKPI = Double.parseDouble(monthKPI);

			fc.utobj().printTestStep("Verify The Total Sales");
			if (totalSales != outSales) {
				fc.utobj().throwsException("was not able to verify the Total Sale Goal Details Page");
			}

			fc.utobj().printTestStep("Verify The KPI Value");
			if (totalKPI != outKPI) {
				fc.utobj().throwsException("was not able to verify the KPI Value Goal Details Page");
			}

			String totalMntsSale = fc.utobj().getText(driver,
					driver.findElement(By.xpath(".//td[contains(text () , 'Total')]/following-sibling::td[1]")));
			if (totalMntsSale.contains(",")) {
				totalMntsSale = totalMntsSale.replaceAll(",", "");
			}

			double totalMntSalesD = Double.parseDouble(totalMntsSale);

			String totalMntsKPI = fc.utobj().getText(driver,
					driver.findElement(By.xpath(".//td[contains(text () , 'Total')]/following-sibling::td[2]")));
			if (totalMntsKPI.contains(",")) {
				totalMntsKPI = totalMntsKPI.replaceAll(",", "");
			}

			double totalMntsKPID = Double.parseDouble(totalMntsKPI);

			fc.utobj().clickElement(driver, pobj.okayBtn);

			fc.utobj().printTestStep("Search Goals By Franchise Id");
			fc.utobj().setToDefault(driver, pobj.searchFranchiseId);
			fc.utobj().selectValFromMultiSelect(driver, pobj.searchFranchiseId, franchiseId);
			fc.utobj().clickElement(driver, pobj.searchBtn);
			String totalSaleAtSummary = fc.utobj().getText(driver,
					driver.findElement(By.xpath(".//a[contains(text () , '" + franchiseId + "')]/ancestor::tr/td[4]")));
			if (totalSaleAtSummary.contains(",")) {
				totalSaleAtSummary = totalSaleAtSummary.replaceAll(",", "");
			}

			double totalSaleAtSummaryD = Double.parseDouble(totalSaleAtSummary);

			String totalKPIAtSummary = fc.utobj().getText(driver,
					driver.findElement(By.xpath(".//a[contains(text () , '" + franchiseId + "')]/ancestor::tr/td[5]")));

			if (totalKPIAtSummary.contains(",")) {
				totalKPIAtSummary = totalKPIAtSummary.replaceAll(",", "");
			}

			double totalKPIAtSummaryD = Double.parseDouble(totalKPIAtSummary);

			fc.utobj().printTestStep("Verify The Total Sale At Summary Page");
			if (totalMntSalesD != totalSaleAtSummaryD) {
				fc.utobj().throwsException("was not able to verify Total Sales At Goals Summary");
			}

			fc.utobj().printTestStep("Verify The KPI At Summary Page");
			if (totalMntsKPID != totalKPIAtSummaryD) {
				fc.utobj().throwsException("was not able to verify KPI Value At Goals Summary");
			}

			fc.utobj().clickElement(driver, pobj.KpiHomeTab);

			boolean isCategoryPresent = fc.utobj().assertPageSource(driver, category);

			if (isCategoryPresent == false) {
				fc.utobj().throwsException("Added KPI in Admin is not visible at KPI Home page");
			}

			// String franchiseId ="GTq15111456", category ="TestKPICategoryd15113448";

			fc.utobj().clickElement(driver, pobj.kpiDashboardTab);

			fc.utobj().clickElement(driver, pobj.selectFranchiseID);

			fc.utobj().selectDropDownByVisibleTextTrimed(driver, pobj.selectFranchiseID, franchiseId);

			fc.utobj().clickElement(driver, pobj.getKpiDashboard);

			boolean isCategoryPresent1 = fc.utobj().assertPageSource(driver, category);

			if (isCategoryPresent1 == false) {
				fc.utobj().throwsException("Added KPI in Admin is not visible at KPI Home page");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	public String addGoals(WebDriver driver, String franchiseId, double saleValue, double KPIValue) throws Exception {

		FinanceKPIPage pobj = new FinanceKPIPage(driver);
		fc.finance().finance_common().financeKPIPage(driver);
		fc.utobj().clickElement(driver, pobj.goalsTab);
		fc.utobj().clickElement(driver, pobj.addGoals);
		fc.utobj().selectDropDownByIndex(driver, pobj.selectGoalYear, 1);

		fc.utobj().setToDefault(driver, pobj.selectFranchise);
		fc.utobj().selectValFromMultiSelect(driver, pobj.selectFranchise, franchiseId);
		String month = fc.utobj().getCurrentMonth();

		WebElement element = driver.findElement(By.id("finGoalsReportDetails_0" + month + ""));
		// double totalSales=100050.00;
		String totalSalesString = Double.toString(saleValue);
		fc.utobj().sendKeys(driver, element, totalSalesString);

		WebElement elementKPI = driver.findElement(By.id("finGoalsReportDetails_1" + month + ""));
		// double totalKPI=48.00;
		String totalKPIString = Double.toString(KPIValue);
		fc.utobj().sendKeys(driver, elementKPI, totalKPIString);
		fc.utobj().clickElement(driver, pobj.saveBtn);

		fc.utobj().clickElement(driver, pobj.okayBtn);

		return month;
	}

	@Test(groups = { "finance" })
	@TestCase(createdOn = "2017-06-04", updatedOn = "2017-06-04", testCaseId = "TC_FIN_KPI_View_Goals_Details", testCaseDescription = "Verify The Details Of Added Goals By Action Icon Option")
	public void viewGoalsDetails() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		Map<String, String> dataSet = fc.utobj().readTestData("finance", testCaseId);

		String agrmntVrsnName = fc.utobj().generateTestData(dataSet.get("agreementVersionName"));
		String royaltyPrcntg = dataSet.get("royaltyPercentage");
		String advPercentage = dataSet.get("advertisementPercentage");
		String royaltyAreaFranchiseValue = dataSet.get("RoyaltyAreaFranchise");
		String advAreaFranchiseValue = dataSet.get("AdvAreaFranchise");

		try {
			driver = fc.loginpage().login(driver);
			FinanceKPIPage pobj = new FinanceKPIPage(driver);

			fc.utobj().printTestStep("Navigate To Admin > Finance > Non-Financial / KPI Categories Summary");
			fc.utobj().printTestStep("Add New Non-Financial Category");
			String category = fc.utobj().generateTestData("TestKPICategory");
			new AdminFinanceNonFinancialKPICategoriesSummaryPageTest().addNewNonFinancialsCategory(driver, config,
					category);

			fc.utobj().printTestStep("Navigate To Admin > Finance > Agreement Versions");
			AdminFinanceAgreementVersionsPageTest agreementforSales = new AdminFinanceAgreementVersionsPageTest();

			fc.utobj().printTestStep("Add Agreement");
			String agreementVersion = agreementforSales.addAgreement(driver, config, agrmntVrsnName, royaltyPrcntg,
					advPercentage, royaltyAreaFranchiseValue, advAreaFranchiseValue);

			String regionName = fc.utobj().generateTestData("GT");
			String storeType = fc.utobj().generateTestData("GT");
			String franchiseId = fc.utobj().generateTestData("GT");
			AdminFranchiseLocationAddFranchiseLocationPageTest franchise = new AdminFranchiseLocationAddFranchiseLocationPageTest();
			franchiseId = franchise.addFranchiseLocation_All_Agreement(driver, franchiseId, regionName, storeType,
					agreementVersion, config);

			fc.utobj().printTestStep("Navigate To Finance > KPI > Goals Summary Page");
			fc.utobj().printTestStep("Add Goals");

			double totalSales = 9865248.00;
			double totalKPI = 98658.00;
			String month = addGoals(driver, franchiseId, totalSales, totalKPI);

			fc.utobj().printTestStep("Search Goals By Franchise Id");
			fc.utobj().setToDefault(driver, pobj.searchFranchiseId);
			fc.utobj().selectValFromMultiSelect(driver, pobj.searchFranchiseId, franchiseId);
			fc.utobj().clickElement(driver, pobj.searchBtn);

			fc.utobj().printTestStep("View Goals Details");
			fc.utobj().actionImgOption(driver, franchiseId, "View Goal Details");

			String monthSales = fc.utobj().getText(driver, driver
					.findElement(By.xpath(".//td[contains(text () , '" + month + "')]/following-sibling::td[1]")));

			if (monthSales.contains(",")) {
				monthSales = monthSales.replaceAll(",", "");
			}

			double outSales = Double.parseDouble(monthSales);

			String monthKPI = fc.utobj().getText(driver, driver
					.findElement(By.xpath(".//td[contains(text () , '" + month + "')]/following-sibling::td[2]")));
			if (monthKPI.contains(",")) {
				monthKPI = monthKPI.replaceAll(",", "");
			}

			double outKPI = Double.parseDouble(monthKPI);

			fc.utobj().printTestStep("Verify The Month Sales");
			if (totalSales != outSales) {
				fc.utobj().throwsException("was not able to verify the Month At Goal Details Page");
			}

			fc.utobj().printTestStep("Verify The KPI Value");
			if (totalKPI != outKPI) {
				fc.utobj().throwsException("was not able to verify the KPI Value Goal Details Page");
			}

			String totalMntsSale = fc.utobj().getText(driver,
					driver.findElement(By.xpath(".//td[contains(text () , 'Total')]/following-sibling::td[1]")));
			if (totalMntsSale.contains(",")) {
				totalMntsSale = totalMntsSale.replaceAll(",", "");
			}

			double totalMntSalesD = Double.parseDouble(totalMntsSale);

			fc.utobj().printTestStep("Verify The Total Sales");
			if (totalMntSalesD != totalSales) {
				fc.utobj().throwsException("was not able to verify the Total Sales");
			}

			String totalMntsKPI = fc.utobj().getText(driver,
					driver.findElement(By.xpath(".//td[contains(text () , 'Total')]/following-sibling::td[2]")));
			if (totalMntsKPI.contains(",")) {
				totalMntsKPI = totalMntsKPI.replaceAll(",", "");
			}

			double totalMntsKPID = Double.parseDouble(totalMntsKPI);

			double avgKPI = totalKPI / 12;

			fc.utobj().printTestStep("Verify The Total KPI Value");
			if (avgKPI != totalMntsKPID) {
				fc.utobj().throwsException("was not able to verify the Total Sales");
			}

			fc.utobj().clickElement(driver, pobj.okayBtn);

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "finance" })
	@TestCase(createdOn = "2017-06-04", updatedOn = "2017-06-04", testCaseId = "TC_FIN_KPI_Modify_Goals_Details", testCaseDescription = "Verify The Modify Added Goals By Action Icon Option")
	public void modifyGoal() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		Map<String, String> dataSet = fc.utobj().readTestData("finance", testCaseId);

		String agrmntVrsnName = fc.utobj().generateTestData(dataSet.get("agreementVersionName"));
		String royaltyPrcntg = dataSet.get("royaltyPercentage");
		String advPercentage = dataSet.get("advertisementPercentage");
		String royaltyAreaFranchiseValue = dataSet.get("RoyaltyAreaFranchise");
		String advAreaFranchiseValue = dataSet.get("AdvAreaFranchise");

		try {
			driver = fc.loginpage().login(driver);
			FinanceKPIPage pobj = new FinanceKPIPage(driver);

			fc.utobj().printTestStep("Navigate To Admin > Finance > Non-Financial / KPI Categories Summary");
			fc.utobj().printTestStep("Add New Non-Financial Category");
			String category = fc.utobj().generateTestData("TestKPICategory");
			new AdminFinanceNonFinancialKPICategoriesSummaryPageTest().addNewNonFinancialsCategory(driver, config,
					category);

			fc.utobj().printTestStep("Navigate To Admin > Finance > Agreement Versions");
			AdminFinanceAgreementVersionsPageTest agreementforSales = new AdminFinanceAgreementVersionsPageTest();

			fc.utobj().printTestStep("Add Agreement");
			String agreementVersion = agreementforSales.addAgreement(driver, config, agrmntVrsnName, royaltyPrcntg,
					advPercentage, royaltyAreaFranchiseValue, advAreaFranchiseValue);

			String regionName = fc.utobj().generateTestData("GT");
			String storeType = fc.utobj().generateTestData("GT");
			String franchiseId = fc.utobj().generateTestData("GT");
			AdminFranchiseLocationAddFranchiseLocationPageTest franchise = new AdminFranchiseLocationAddFranchiseLocationPageTest();
			franchiseId = franchise.addFranchiseLocation_All_Agreement(driver, franchiseId, regionName, storeType,
					agreementVersion, config);

			fc.utobj().printTestStep("Navigate To Finance > KPI > Goals Summary Page");
			fc.utobj().printTestStep("Add Goals");

			double totalSales1 = 9865248.00;
			double totalKPI1 = 98658.00;
			String month = addGoals(driver, franchiseId, totalSales1, totalKPI1);

			fc.utobj().printTestStep("Search Goals By Franchise Id");
			fc.utobj().setToDefault(driver, pobj.searchFranchiseId);
			fc.utobj().selectValFromMultiSelect(driver, pobj.searchFranchiseId, franchiseId);
			fc.utobj().clickElement(driver, pobj.searchBtn);

			fc.utobj().printTestStep("Modify The Added Goals");
			fc.utobj().actionImgOption(driver, franchiseId, "Modify Goals");

			/* String month=fc.utobj().getCurrentMonth(); */

			WebElement element = driver.findElement(By.id("finGoalsReportDetails_0" + month + ""));
			double totalSales = 986547.00;
			String totalSalesString = Double.toString(totalSales);
			fc.utobj().sendKeys(driver, element, totalSalesString);

			WebElement elementKPI = driver.findElement(By.id("finGoalsReportDetails_1" + month + ""));
			double totalKPI = 32569.00;
			String totalKPIString = Double.toString(totalKPI);
			fc.utobj().sendKeys(driver, elementKPI, totalKPIString);
			fc.utobj().clickElement(driver, pobj.saveBtn);

			String monthSales = fc.utobj().getText(driver, driver
					.findElement(By.xpath(".//td[contains(text () , '" + month + "')]/following-sibling::td[1]")));

			if (monthSales.contains(",")) {
				monthSales = monthSales.replaceAll(",", "");
			}

			double outSales = Double.parseDouble(monthSales);

			String monthKPI = fc.utobj().getText(driver, driver
					.findElement(By.xpath(".//td[contains(text () , '" + month + "')]/following-sibling::td[2]")));
			if (monthKPI.contains(",")) {
				monthKPI = monthKPI.replaceAll(",", "");
			}

			double outKPI = Double.parseDouble(monthKPI);

			fc.utobj().printTestStep("Verify The Total Sales");
			if (totalSales != outSales) {
				fc.utobj().throwsException("was not able to verify the Total Sale Goal Details Page");
			}

			fc.utobj().printTestStep("Verify The KPI Value");
			if (totalKPI != outKPI) {
				fc.utobj().throwsException("was not able to verify the KPI Value Goal Details Page");
			}

			String totalMntsSale = fc.utobj().getText(driver,
					driver.findElement(By.xpath(".//td[contains(text () , 'Total')]/following-sibling::td[1]")));
			if (totalMntsSale.contains(",")) {
				totalMntsSale = totalMntsSale.replaceAll(",", "");
			}

			double totalMntSalesD = Double.parseDouble(totalMntsSale);

			String totalMntsKPI = fc.utobj().getText(driver,
					driver.findElement(By.xpath(".//td[contains(text () , 'Total')]/following-sibling::td[2]")));
			if (totalMntsKPI.contains(",")) {
				totalMntsKPI = totalMntsKPI.replaceAll(",", "");
			}

			double totalMntsKPID = Double.parseDouble(totalMntsKPI);

			fc.utobj().clickElement(driver, pobj.okayBtn);

			fc.utobj().printTestStep("Search Goals By Franchise Id");
			fc.utobj().setToDefault(driver, pobj.searchFranchiseId);
			fc.utobj().selectValFromMultiSelect(driver, pobj.searchFranchiseId, franchiseId);
			fc.utobj().clickElement(driver, pobj.searchBtn);
			String totalSaleAtSummary = fc.utobj().getText(driver,
					driver.findElement(By.xpath(".//a[contains(text () , '" + franchiseId + "')]/ancestor::tr/td[4]")));
			if (totalSaleAtSummary.contains(",")) {
				totalSaleAtSummary = totalSaleAtSummary.replaceAll(",", "");
			}

			double totalSaleAtSummaryD = Double.parseDouble(totalSaleAtSummary);

			String totalKPIAtSummary = fc.utobj().getText(driver,
					driver.findElement(By.xpath(".//a[contains(text () , '" + franchiseId + "')]/ancestor::tr/td[5]")));

			if (totalKPIAtSummary.contains(",")) {
				totalKPIAtSummary = totalKPIAtSummary.replaceAll(",", "");
			}

			double totalKPIAtSummaryD = Double.parseDouble(totalKPIAtSummary);

			fc.utobj().printTestStep("Verify The Total Sale At Summary Page");
			if (totalMntSalesD != totalSaleAtSummaryD) {
				fc.utobj().throwsException("was not able to verify Total Sales At Goals Summary");
			}

			fc.utobj().printTestStep("Verify The KPI At Summary Page");
			if (totalMntsKPID != totalKPIAtSummaryD) {
				fc.utobj().throwsException("was not able to verify KPI Value At Goals Summary");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "finance" })
	@TestCase(createdOn = "2017-06-04", updatedOn = "2017-06-04", testCaseId = "TC_FIN_KPI_Approve_Goal", testCaseDescription = "Approve the Goal By Action Image Option")
	public void approveGoal() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		Map<String, String> dataSet = fc.utobj().readTestData("finance", testCaseId);

		String agrmntVrsnName = fc.utobj().generateTestData(dataSet.get("agreementVersionName"));
		String royaltyPrcntg = dataSet.get("royaltyPercentage");
		String advPercentage = dataSet.get("advertisementPercentage");
		String royaltyAreaFranchiseValue = dataSet.get("RoyaltyAreaFranchise");
		String advAreaFranchiseValue = dataSet.get("AdvAreaFranchise");

		try {
			driver = fc.loginpage().login(driver);
			FinanceKPIPage pobj = new FinanceKPIPage(driver);

			fc.utobj().printTestStep("Navigate To Admin > Finance > Non-Financial / KPI Categories Summary");
			fc.utobj().printTestStep("Add New Non-Financial Category");
			String category = fc.utobj().generateTestData("TestKPICategory");
			new AdminFinanceNonFinancialKPICategoriesSummaryPageTest().addNewNonFinancialsCategory(driver, config,
					category);

			fc.utobj().printTestStep("Navigate To Admin > Finance > Agreement Versions");
			AdminFinanceAgreementVersionsPageTest agreementforSales = new AdminFinanceAgreementVersionsPageTest();

			fc.utobj().printTestStep("Add Agreement");
			String agreementVersion = agreementforSales.addAgreement(driver, config, agrmntVrsnName, royaltyPrcntg,
					advPercentage, royaltyAreaFranchiseValue, advAreaFranchiseValue);

			String franchiseId = fc.utobj().generateTestData("GT");
			AdminFranchiseLocationAddNewFranchiseLocationPageTest franchise = new AdminFranchiseLocationAddNewFranchiseLocationPageTest();
			franchiseId = franchise.addFranchiseLocation(driver, franchiseId, agreementVersion, config);

			fc.utobj().printTestStep("Navigate To Finance > KPI > Goals Summary Page");
			fc.utobj().printTestStep("Add Goals");

			double totalSales = 9865248.00;
			double totalKPI = 98658.00;
			addGoals(driver, franchiseId, totalSales, totalKPI);

			fc.utobj().printTestStep("Search Goals By Franchise Id");
			fc.utobj().setToDefault(driver, pobj.searchFranchiseId);
			fc.utobj().selectValFromMultiSelect(driver, pobj.searchFranchiseId, franchiseId);
			fc.utobj().clickElement(driver, pobj.searchBtn);

			fc.utobj().printTestStep("Approve Goals Details");
			fc.utobj().actionImgOption(driver, franchiseId, "Approve");

			fc.commonMethods().switch_cboxIframe_frameId(driver);

			boolean isTextPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () ,'" + franchiseId + "')]");

			fc.utobj().clickElement(driver, pobj.okayFrame);

			fc.utobj().switchFrameToDefault(driver);

			if (isTextPresent == false) {
				fc.utobj().throwsException("was not able to verify the approve Goals");
			}

			fc.utobj().moveToElement(driver, fc.utobj().getElementByXpath(driver,
					".//*[contains(text () ,'" + franchiseId + "')]/ancestor::tr/td/div[@id='menuBar']/layer"));
			String alterText = driver
					.findElement(By.xpath(
							".//*[contains(text () ,'" + franchiseId + "')]/ancestor::tr/td/div[@id='menuBar']/layer"))
					.getAttribute("id").trim();
			alterText = alterText.replace("Actions_dynamicmenu", "");
			alterText = alterText.replace("Bar", "");
			fc.utobj().clickElement(driver, driver.findElement(By.xpath(
					".//*[contains(text () ,'" + franchiseId + "')]/ancestor::tr/td/div[@id='menuBar']/layer/a/img")));
			List<WebElement> optionList = driver.findElements(By.xpath(".//div[@id='Actions_dynamicmenu1Menu']/span"));

			String option = null;
			boolean isOptionPresent = false;

			for (int i = 0; i < optionList.size(); i++) {

				option = optionList.get(i).getText().trim();

				if (option.equalsIgnoreCase("Approve")) {
					isOptionPresent = true;
					break;
				}
			}

			if (isOptionPresent == true) {
				fc.utobj().throwsException("Approve Option is comming in action icon After Approval of Goal");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "finance")
	@TestCase(createdOn = "2017-06-04", updatedOn = "2017-06-04", testCaseId = "TC_FIN_KPI_Approve_Goal_01", testCaseDescription = "Approve the Goal By Bottom Button Option")
	public void approveGoalByBottomBtn() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		Map<String, String> dataSet = fc.utobj().readTestData("finance", testCaseId);

		String agrmntVrsnName = fc.utobj().generateTestData(dataSet.get("agreementVersionName"));
		String royaltyPrcntg = dataSet.get("royaltyPercentage");
		String advPercentage = dataSet.get("advertisementPercentage");
		String royaltyAreaFranchiseValue = dataSet.get("RoyaltyAreaFranchise");
		String advAreaFranchiseValue = dataSet.get("AdvAreaFranchise");

		try {
			driver = fc.loginpage().login(driver);
			FinanceKPIPage pobj = new FinanceKPIPage(driver);

			fc.utobj().printTestStep("Navigate To Admin > Finance > Non-Financial / KPI Categories Summary");
			fc.utobj().printTestStep("Add New Non-Financial Category");
			String category = fc.utobj().generateTestData("TestKPICategory");
			new AdminFinanceNonFinancialKPICategoriesSummaryPageTest().addNewNonFinancialsCategory(driver, config,
					category);

			fc.utobj().printTestStep("Navigate To Admin > Finance > Agreement Versions");
			AdminFinanceAgreementVersionsPageTest agreementforSales = new AdminFinanceAgreementVersionsPageTest();

			fc.utobj().printTestStep("Add Agreement");
			String agreementVersion = agreementforSales.addAgreement(driver, config, agrmntVrsnName, royaltyPrcntg,
					advPercentage, royaltyAreaFranchiseValue, advAreaFranchiseValue);

			String regionName = fc.utobj().generateTestData("GT");
			String storeType = fc.utobj().generateTestData("GT");
			String franchiseId = fc.utobj().generateTestData("GT");
			AdminFranchiseLocationAddFranchiseLocationPageTest franchise = new AdminFranchiseLocationAddFranchiseLocationPageTest();
			franchiseId = franchise.addFranchiseLocation_All_Agreement(driver, franchiseId, regionName, storeType,
					agreementVersion, config);

			fc.utobj().printTestStep("Navigate To Finance > KPI > Goals Summary Page");
			fc.utobj().printTestStep("Add Goals");

			double totalSales = 9865248.00;
			double totalKPI = 98658.00;
			addGoals(driver, franchiseId, totalSales, totalKPI);

			fc.utobj().printTestStep("Search Goals By Franchise Id");
			fc.utobj().setToDefault(driver, pobj.searchFranchiseId);
			fc.utobj().selectValFromMultiSelect(driver, pobj.searchFranchiseId, franchiseId);
			fc.utobj().clickElement(driver, pobj.searchBtn);

			fc.utobj().printTestStep("Approve Goals Details");
			fc.utobj().clickElement(driver, driver.findElement(By.xpath(
					".//a[contains(text () , '" + franchiseId + "')]/ancestor::tr/td/input[@name='selectedItem']")));
			fc.utobj().clickElement(driver, pobj.approveGoalBtn);

			fc.commonMethods().switch_cboxIframe_frameId(driver);
			boolean isTextPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () ,'" + franchiseId + "')]");
			fc.utobj().clickElement(driver, pobj.okayFrame);
			fc.utobj().switchFrameToDefault(driver);

			if (isTextPresent == false) {
				fc.utobj().throwsException("was not able to verify the approve Goals");
			}

			fc.utobj().moveToElement(driver, fc.utobj().getElementByXpath(driver,
					".//*[contains(text () ,'" + franchiseId + "')]/ancestor::tr/td/div[@id='menuBar']/layer"));
			String alterText = driver
					.findElement(By.xpath(
							".//*[contains(text () ,'" + franchiseId + "')]/ancestor::tr/td/div[@id='menuBar']/layer"))
					.getAttribute("id").trim();
			alterText = alterText.replace("Actions_dynamicmenu", "");
			alterText = alterText.replace("Bar", "");
			fc.utobj().clickElement(driver, driver.findElement(By.xpath(
					".//*[contains(text () ,'" + franchiseId + "')]/ancestor::tr/td/div[@id='menuBar']/layer/a/img")));
			List<WebElement> optionList = driver.findElements(By.xpath(".//div[@id='Actions_dynamicmenu1Menu']/span"));

			String option = null;
			boolean isOptionPresent = false;

			for (int i = 0; i < optionList.size(); i++) {

				option = optionList.get(i).getText().trim();

				if (option.equalsIgnoreCase("Approve")) {
					isOptionPresent = true;
					break;
				}
			}

			if (isOptionPresent == true) {
				fc.utobj().throwsException("Approve Option is comming in action icon After Approval of Goal");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "finance" })
	@TestCase(createdOn = "2018-06-12", updatedOn = "2018-06-12", testCaseId = "TC_FIN_KPI_Modify_Approved_Goal", testCaseDescription = "Modify option for Approved Goal By Action Image Option")
	public void modifyApprovedGoal() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		String agrmntVrsnName = fc.utobj().generateTestData("GT");
		String royaltyPrcntg = "10";
		String advPercentage = "8";
		String royaltyAreaFranchiseValue = "6";
		String advAreaFranchiseValue = "2";

		try {
			driver = fc.loginpage().login(driver);
			FinanceKPIPage pobj = new FinanceKPIPage(driver);

			fc.utobj().printTestStep("Navigate To Admin > Finance > Non-Financial / KPI Categories Summary");
			fc.utobj().printTestStep("Add New Non-Financial Category");
			String category = fc.utobj().generateTestData("TestKPICategory");
			new AdminFinanceNonFinancialKPICategoriesSummaryPageTest().addNewNonFinancialsCategory(driver, config,
					category);

			fc.utobj().printTestStep("Navigate To Admin > Finance > Agreement Versions");
			AdminFinanceAgreementVersionsPageTest agreementforSales = new AdminFinanceAgreementVersionsPageTest();

			fc.utobj().printTestStep("Add Agreement");
			String agreementVersion = agreementforSales.addAgreement(driver, config, agrmntVrsnName, royaltyPrcntg,
					advPercentage, royaltyAreaFranchiseValue, advAreaFranchiseValue);

			String regionName = fc.utobj().generateTestData("GT");
			String storeType = fc.utobj().generateTestData("GT");
			String franchiseId = fc.utobj().generateTestData("GT");
			AdminFranchiseLocationAddFranchiseLocationPageTest franchise = new AdminFranchiseLocationAddFranchiseLocationPageTest();
			franchiseId = franchise.addFranchiseLocation_All_Agreement(driver, franchiseId, regionName, storeType,
					agreementVersion, config);

			fc.utobj().printTestStep("Navigate To Finance > KPI > Goals Summary Page");
			fc.utobj().printTestStep("Add Goals");

			double totalSales = 9865248.00;
			double totalKPI = 98658.00;
			addGoals(driver, franchiseId, totalSales, totalKPI);

			fc.utobj().printTestStep("Search Goals By Franchise Id");
			fc.utobj().setToDefault(driver, pobj.searchFranchiseId);
			fc.utobj().selectValFromMultiSelect(driver, pobj.searchFranchiseId, franchiseId);
			fc.utobj().clickElement(driver, pobj.searchBtn);

			fc.utobj().printTestStep("Approve Goals Details");
			fc.utobj().actionImgOption(driver, franchiseId, "Approve");

			fc.commonMethods().switch_cboxIframe_frameId(driver);
			boolean isTextPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () ,'" + franchiseId + "')]");
			fc.utobj().clickElement(driver, pobj.okayFrame);
			fc.utobj().switchFrameToDefault(driver);

			if (isTextPresent == false) {
				fc.utobj().throwsException("was not able to verify the approve Goals");
			}

			fc.utobj().moveToElement(driver, fc.utobj().getElementByXpath(driver,
					".//*[contains(text () ,'" + franchiseId + "')]/ancestor::tr/td/div[@id='menuBar']/layer"));

			String alterText = driver
					.findElement(By.xpath(
							".//*[contains(text () ,'" + franchiseId + "')]/ancestor::tr/td/div[@id='menuBar']/layer"))
					.getAttribute("id").trim();

			alterText = alterText.replace("Actions_dynamicmenu", "");

			alterText = alterText.replace("Bar", "");

			fc.utobj().clickElement(driver, driver.findElement(By.xpath(
					".//*[contains(text () ,'" + franchiseId + "')]/ancestor::tr/td/div[@id='menuBar']/layer/a/img")));

			List<WebElement> optionList = driver.findElements(By.xpath(".//div[@id='Actions_dynamicmenu1Menu']/span"));

			String option = null;

			boolean isOptionPresent = false;

			for (int i = 0; i < optionList.size(); i++) {

				option = optionList.get(i).getText().trim();

				if (option.equalsIgnoreCase("Modify Goals")) {
					isOptionPresent = true;
					break;
				}
			}

			if (isOptionPresent == true) {
				fc.utobj().throwsException("Modify Goals Option is comming in action icon After Approval of Goal");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = {"finance", "finance_FailedTC"})
	@TestCase(createdOn = "2018-06-12", updatedOn = "2018-06-12", testCaseId = "TC-23032-33_FIN_KPI_Add_Modify_Goal_Multiple_Franchisees", testCaseDescription = "Add / Modify Gaols for Multiple Franchisees")
	public void addModifyGoalsForMultipleFranchise() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		String agrmntVrsnName = fc.utobj().generateTestData("AV");
		String royaltyPrcntg = "10";
		String advPercentage = "8";
		String royaltyAreaFranchiseValue = "6";
		String advAreaFranchiseValue = "3";

		try {
			driver = fc.loginpage().login(driver);
			FinanceKPIPage pobj = new FinanceKPIPage(driver);

			fc.utobj().printTestStep("Navigate To Admin > Finance > Agreement Versions");
			AdminFinanceAgreementVersionsPageTest agreementforSales = new AdminFinanceAgreementVersionsPageTest();

			fc.utobj().printTestStep("Add Agreement");
			String agreementVersion = agreementforSales.addAgreement(driver, config, agrmntVrsnName, royaltyPrcntg,
					advPercentage, royaltyAreaFranchiseValue, advAreaFranchiseValue);

			fc.utobj().printTestStep("Add Two Franchise with same agreement");

			String franchiseId = fc.utobj().generateTestData("GT");
			AdminFranchiseLocationAddNewFranchiseLocationPageTest franchise = new AdminFranchiseLocationAddNewFranchiseLocationPageTest();
			String franchiseId1 = franchise.addFranchiseLocation(driver, franchiseId, agreementVersion, config);

			franchiseId = fc.utobj().generateTestData("GT");

			String franchiseId2 = franchise.addFranchiseLocation(driver, franchiseId, agreementVersion, config);

			fc.utobj().printTestStep("Navigate To Finance > KPI > Goals Summary Page");
			fc.utobj().printTestStep("Add Goals for added two locations");

			double totalSales = 500;
			double totalKPI = 600;

			fc.finance().finance_common().financeKPIPage(driver);

			fc.utobj().clickElement(driver, pobj.goalsTab);

			fc.utobj().clickElement(driver, pobj.addGoals);

			fc.utobj().selectDropDownByIndex(driver, pobj.selectGoalYear, 1);

			fc.utobj().setToDefault(driver, pobj.selectFranchise);

			fc.utobj().clickElement(driver,
					driver.findElement(By.xpath("//div[@id='ms-parentcomboFranchisee']//button[@type='button']")));

			fc.utobj().sendKeys(driver,
					driver.findElement(
							By.xpath(".//div[@id='ms-parentcomboFranchisee']//input[@class='searchInputMultiple']")),
					franchiseId1);

			fc.utobj().clickElement(driver, driver
					.findElement(By.xpath(".//div[@id='ms-parentcomboFranchisee']//input[@class='search-btn on']")));

			fc.utobj().clickElement(driver, driver.findElement(By.id("selectAll")));

			fc.utobj().sendKeys(driver,
					driver.findElement(
							By.xpath(".//div[@id='ms-parentcomboFranchisee']//input[@class='searchInputMultiple']")),
					franchiseId2);

			fc.utobj().clickElement(driver, driver
					.findElement(By.xpath(".//div[@id='ms-parentcomboFranchisee']//input[@class='search-btn on']")));

			fc.utobj().clickElement(driver, driver.findElement(By.id("selectAll")));

			String month = fc.utobj().getCurrentMonth();

			WebElement element = driver.findElement(By.id("finGoalsReportDetails_0" + month + ""));

			String totalSalesString = Double.toString(totalSales);

			fc.utobj().sendKeys(driver, element, totalSalesString);

			WebElement elementKPI = driver.findElement(By.id("finGoalsReportDetails_1" + month + ""));

			String totalKPIString = Double.toString(totalKPI);

			fc.utobj().sendKeys(driver, elementKPI, totalKPIString);

			fc.utobj().clickElement(driver, pobj.saveBtn);

			fc.utobj().clickElement(driver, pobj.okayBtn);

			fc.utobj().printTestStep("Search Goals By Franchise Id");

			fc.utobj().setToDefault(driver, pobj.searchFranchiseId);

			fc.utobj().selectValFromMultiSelect(driver, pobj.searchFranchiseId, franchiseId1);

			fc.utobj().clickElement(driver, pobj.searchBtn);

			fc.utobj().printTestStep("Modify The Added Goals");

			finActionImgOption(driver, franchiseId1, "Modify Goals");

			fc.utobj().clickElement(driver,
					driver.findElement(By.xpath(".//a[contains(text(), 'Apply to other Franchisee')]")));

			fc.commonMethods().switch_cboxIframe_frameId(driver);

			fc.utobj().selectValFromMultiSelect(driver, pobj.searchFranchiseIdAtPop, franchiseId2);

			fc.utobj().clickElement(driver, pobj.okayFrame);

			WebElement element1 = driver.findElement(By.id("finGoalsReportDetails_0" + month + ""));

			double totalSales1 = 900;

			String totalSalesString1 = Double.toString(totalSales1);

			fc.utobj().sendKeys(driver, element1, totalSalesString1);

			WebElement elementKPI1 = driver.findElement(By.id("finGoalsReportDetails_1" + month + ""));
			double totalKPI1 = 800;

			String totalKPIString1 = Double.toString(totalKPI1);

			fc.utobj().sendKeys(driver, elementKPI1, totalKPIString1);

			fc.utobj().clickElement(driver, pobj.saveBtn);

			String monthSales = fc.utobj().getText(driver, driver
					.findElement(By.xpath(".//td[contains(text () , '" + month + "')]/following-sibling::td[1]")));

			if (monthSales.contains(",")) {
				monthSales = monthSales.replaceAll(",", "");
			}

			double outSales = Double.parseDouble(monthSales);

			String monthKPI = fc.utobj().getText(driver, driver
					.findElement(By.xpath(".//td[contains(text () , '" + month + "')]/following-sibling::td[2]")));

			if (monthKPI.contains(",")) {
				monthKPI = monthKPI.replaceAll(",", "");
			}

			double outKPI = Double.parseDouble(monthKPI);

			fc.utobj().printTestStep("Verify The Total Sales");

			if (totalSales1 != outSales) {
				fc.utobj().throwsException("was not able to verify the Total Sale Goal Details Page");
			}

			fc.utobj().printTestStep("Verify The KPI Value");

			if (totalKPI1 != outKPI) {
				fc.utobj().throwsException("was not able to verify the KPI Value Goal Details Page");
			}

			String totalMntsSale = fc.utobj().getText(driver,
					driver.findElement(By.xpath(".//td[contains(text () , 'Total')]/following-sibling::td[1]")));

			if (totalMntsSale.contains(",")) {
				totalMntsSale = totalMntsSale.replaceAll(",", "");
			}

			double totalMntSalesD = Double.parseDouble(totalMntsSale);

			String totalMntsKPI = fc.utobj().getText(driver,
					driver.findElement(By.xpath(".//td[contains(text () , 'Total')]/following-sibling::td[2]")));

			if (totalMntsKPI.contains(",")) {
				totalMntsKPI = totalMntsKPI.replaceAll(",", "");
			}

			double totalMntsKPID = Double.parseDouble(totalMntsKPI);

			fc.utobj().clickElement(driver, pobj.okayBtn);

			fc.utobj().printTestStep("Search Goals By Franchise Id");

			fc.utobj().selectValFromMultiSelect(driver, pobj.searchFranchiseId, franchiseId2);

			fc.utobj().clickElement(driver, pobj.searchBtn);

			String totalSaleAtSummary = fc.utobj().getText(driver, driver
					.findElement(By.xpath(".//a[contains(text () , '" + franchiseId2 + "')]/ancestor::tr/td[4]")));

			if (totalSaleAtSummary.contains(",")) {
				totalSaleAtSummary = totalSaleAtSummary.replaceAll(",", "");
			}

			double totalSaleAtSummaryD = Double.parseDouble(totalSaleAtSummary);

			String totalKPIAtSummary = fc.utobj().getText(driver, driver
					.findElement(By.xpath(".//a[contains(text () , '" + franchiseId2 + "')]/ancestor::tr/td[5]")));

			if (totalKPIAtSummary.contains(",")) {
				totalKPIAtSummary = totalKPIAtSummary.replaceAll(",", "");
			}

			double totalKPIAtSummaryD = Double.parseDouble(totalKPIAtSummary);

			fc.utobj().printTestStep("Verify The Total Sale At Summary Page");

			if (totalMntSalesD != totalSaleAtSummaryD) {
				fc.utobj().throwsException("was not able to verify Total Sales At Goals Summary");
			}

			fc.utobj().printTestStep("Verify The KPI At Summary Page for second franchise");

			if (totalMntsKPID != totalKPIAtSummaryD) {
				fc.utobj().throwsException("was not able to verify KPI Value At Goals Summary");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	public void finActionImgOption(WebDriver driver, String task, String option) throws Exception {

		fc.utobj().moveToElement(driver, fc.utobj().getElementByXpath(driver,
				".//*[contains(text () ,'" + task + "')]/ancestor::tr//div[@id='menuBar']/layer"));
		String alterText = fc.utobj()
				.getElementByXpath(driver,
						".//*[contains(text () ,'" + task + "')]/ancestor::tr//div[@id='menuBar']/layer")
				.getAttribute("id").trim();
		alterText = alterText.replace("Actions_dynamicmenu", "");
		alterText = alterText.replace("Bar", "");
		fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
				".//*[contains(text () ,'" + task + "')]/ancestor::tr//div[@id='menuBar']/layer/a/img"));

		List<String> linkArray = fc.utobj().translate(option);
		if (!(linkArray.size() < 1)) {
			for (int i = 0; i < linkArray.size(); i++) {
				try {
					option = linkArray.get(i);
					fc.utobj().clickElement(driver, driver.findElement(By.xpath(".//div[@id='Actions_dynamicmenu"
							+ alterText + "Menu']/*[contains(text () , '" + option + "')]")));
					break;
				} catch (Exception e) {
				}
			}
		} else {
			fc.utobj().clickElement(driver, driver.findElement(By.xpath(
					".//div[@id='Actions_dynamicmenu" + alterText + "Menu']/*[contains(text () , '" + option + "')]")));
		}
	}

}
