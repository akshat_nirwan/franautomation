package com.builds.test.fin;

import java.text.DecimalFormat;
import java.util.List;
import java.util.Map;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;
import com.builds.test.admin.AdminFranchiseLocationAddFranchiseLocationPageTest;
import com.builds.test.admin.AdminUsersManageManageFranchiseUsersPageTest;
import com.builds.uimaps.fin.FinanceOwnerSalesRoyaltiesPaymentsPage;
import com.builds.uimaps.fin.FinancePaymentsPage;
import com.builds.uimaps.fin.FinanceRoyaltiesPage;
import com.builds.uimaps.fin.FinanceSalesPage;
import com.builds.utilities.FranconnectUtil;
import com.builds.utilities.TestCase;

public class FinanceOwnerSalesRoyaltiesPaymentsPageTest {
	FranconnectUtil fc = new FranconnectUtil();

	@Test(groups = { "finance" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-08-16", testCaseId = "TC_FC_enter_Sales_With_Owner_Account_for_Finance_161", testCaseDescription = "Verify enter Sales With Owner Account")
	public void enterSalesWithOwnerAccount() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());
		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		Map<String, String> dataSet = fc.utobj().readTestData("finance", testCaseId);

		String agrmntVrsnName = fc.utobj().generateTestData(dataSet.get("agreementVersionName"));
		String royaltyPrcntg = dataSet.get("royaltyPercentage");
		String advPercentage = dataSet.get("advertisementPercentage");
		String royaltyAreaFranchiseValue = dataSet.get("RoyaltyAreaFranchise");
		String advAreaFranchiseValue = dataSet.get("AdvAreaFranchise");

		try {
			driver = fc.loginpage().login(driver);

			String password = "t0n1ght123";
			FinanceSalesPage pobj = new FinanceSalesPage(driver);
			fc.utobj().printTestStep("Navigate To Admin > Finance > Agreement Versions");
			AdminFinanceAgreementVersionsPageTest agreementforSales = new AdminFinanceAgreementVersionsPageTest();

			fc.utobj().printTestStep("Add Agreement");
			String agreementVersion = agreementforSales.addAgreement(driver, config, agrmntVrsnName, 
					 royaltyPrcntg, advPercentage, royaltyAreaFranchiseValue, advAreaFranchiseValue);

			fc.utobj().printTestStep("Navigate To Admin > Franchise Location > Add Franchise Location");
			fc.utobj().printTestStep("Add Franchise Location");

			String regionName = fc.utobj().generateTestData("Test");
			String storeType = fc.utobj().generateTestData("Test");
			String franchiseId = fc.utobj().generateTestData("Test");
			AdminFranchiseLocationAddFranchiseLocationPageTest franchise = new AdminFranchiseLocationAddFranchiseLocationPageTest();
			franchiseId = franchise.addFranchiseLocation_All_Agreement(driver, franchiseId, regionName, storeType,
					agreementVersion, config);

			fc.utobj().printTestStep("Navigate To Admin > Finance > Finance Setup Preferences");
			fc.utobj().printTestStep("Setup Medium of submission of Sales Reports");
			String salesReportType = "webForm";
			new AdminFinanceFinanceSetupPreferencesPageTest().mediumOfSubmissionOfSalesReports(driver, config,
					salesReportType);

			fc.utobj().printTestStep("Add Franchise User");
			AdminUsersManageManageFranchiseUsersPageTest franUser = new AdminUsersManageManageFranchiseUsersPageTest();
			String FranUserName = fc.utobj().generateTestData("TestF");
			String roleName = "Default Franchise Role";
			String emailId = "commonautomation@staffex.com";
			String franUserName = franUser.addFranchiseUser(driver, FranUserName, password, franchiseId, roleName,
					emailId);

			fc.home_page().logout(driver);
			fc.utobj().printTestStep("Login With Franchise User");
			fc.loginpage().loginWithParameter(driver, franUserName, password);

			String categoryQuantity = "110";
			String categoryAmount = "6000";
			String reportID = enterSalesReportforOwner(driver, config, categoryQuantity, categoryAmount);

			boolean isTextPresent = fc.utobj().assertLinkText(driver, reportID);

			if (isTextPresent == false) {

				fc.utobj().selectDropDownByVisibleText(driver, pobj.selectViewPerPage, "500");
				{
					boolean isTextPresent1 = fc.utobj().assertLinkText(driver, reportID);

					if (isTextPresent1 == false) {
						fc.utobj().throwsException("Sales is Not added!");
					}
				}
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}

	}

	public String enterSalesReportforOwner(WebDriver driver, Map<String, String> config, String srvcQuantity,
			String srvcAmount) throws Exception {

		FinanceSalesPage pobj = new FinanceSalesPage(driver);
		fc.finance().finance_common().financeSalesPage(driver);
		fc.utobj().clickElement(driver, pobj.enterSalesReport);
		fc.utobj().selectDropDownByIndex(driver, pobj.selectReportPeriod, 0);

		fc.utobj().clickElement(driver, pobj.ContinueBtn);

		fc.utobj().sendKeys(driver,
				driver.findElement(By
						.xpath(".//td[contains(text () , 'Service')]/following-sibling::td//input[contains(@name , 'quantity')]")),
				srvcQuantity);
		fc.utobj().sendKeys(driver,
				driver.findElement(By
						.xpath(".//td[contains(text () , 'Service')]/following-sibling::td//input[contains(@name , 'amount')]")),
				srvcAmount);

		fc.utobj().clickElement(driver, pobj.SaveBtn);

		if (!fc.utobj().isSelected(driver,pobj.acknowledgement)) {
			fc.utobj().clickElement(driver, pobj.acknowledgement);
		}
		fc.utobj().clickElement(driver, pobj.SaveBtn);

		String reportId = fc.utobj().getText(driver, pobj.reportId);
		fc.utobj().clickElement(driver, pobj.okayBtn);
		return reportId;

	}

	@Test(groups = { "finance" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-08-16", testCaseId = "TC_FC_Approve_Sales_When_Sales_Entered_byOwner_162", testCaseDescription = "Verify approve Sales When Sales Entered by Owner")
	public void approveSalesWhenSalesEnteredbyOwner() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());
		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);

		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		Map<String, String> dataSet = fc.utobj().readTestData("finance", testCaseId);

		String agrmntVrsnName = fc.utobj().generateTestData(dataSet.get("agreementVersionName"));
		String royaltyPrcntg = dataSet.get("royaltyPercentage");
		String advPercentage = dataSet.get("advertisementPercentage");
		String royaltyAreaFranchiseValue = dataSet.get("RoyaltyAreaFranchise");
		String advAreaFranchiseValue = dataSet.get("AdvAreaFranchise");
		
		try {
			driver = fc.loginpage().login(driver);
			String password = "t0n1ght123";
			fc.utobj().printTestStep("Navigate To Admin > Finance > Agreement Versions");
			AdminFinanceAgreementVersionsPageTest agreementforSales = new AdminFinanceAgreementVersionsPageTest();

			fc.utobj().printTestStep("Add Agreement");
			String agreementVersion = agreementforSales.addAgreement(driver, config, agrmntVrsnName, 
					 royaltyPrcntg, advPercentage, royaltyAreaFranchiseValue, advAreaFranchiseValue);

			fc.utobj().printTestStep("Navigate To Admin > Franchise Location > Add Franchise Location");
			fc.utobj().printTestStep("Add Franchise Location");

			String regionName = fc.utobj().generateTestData("Test");
			String storeType = fc.utobj().generateTestData("Test");
			String franchiseId = fc.utobj().generateTestData("Test");
			AdminFranchiseLocationAddFranchiseLocationPageTest franchise = new AdminFranchiseLocationAddFranchiseLocationPageTest();
			franchiseId = franchise.addFranchiseLocation_All_Agreement(driver, franchiseId, regionName, storeType,
					agreementVersion, config);

			fc.utobj().printTestStep("Navigate To Admin > Finance > Finance Setup Preferences");
			fc.utobj().printTestStep("Setup Medium of submission of Sales Reports");
			String salesReportType = "webForm";
			new AdminFinanceFinanceSetupPreferencesPageTest().mediumOfSubmissionOfSalesReports(driver, config,
					salesReportType);

			fc.utobj().printTestStep("Add Franchise User");
			AdminUsersManageManageFranchiseUsersPageTest franUser = new AdminUsersManageManageFranchiseUsersPageTest();
			String FranUserName = fc.utobj().generateTestData("TestF");
			String roleName = "Default Franchise Role";
			String emailId = "commonautomation@staffex.com";
			String franUserName = franUser.addFranchiseUser(driver, FranUserName, password, franchiseId, roleName,
					emailId);

			fc.home_page().logout(driver);
			fc.utobj().printTestStep("Login With Franchise User");
			fc.loginpage().loginWithParameter(driver, franUserName, password);

			String categoryQuantity = "110";
			String categoryAmount = "6000";
			String reportId = enterSalesReportforOwner(driver, config, categoryQuantity, categoryAmount);

			fc.home_page().logout(driver);
			fc.loginpage().loginWithParameter(driver, config.get("userName"), config.get("password"));

			fc.utobj().printTestStep("Navigate To Finance > Sales Page");
			fc.finance().finance_common().financeSalesPage(driver);

			fc.utobj().printTestStep("Filter Sales Report By Franchise ID");
			new FinanceSalesPageTest().filterSalesReport(driver, franchiseId);

			FinanceSalesPageTest salesReport = new FinanceSalesPageTest();
			fc.utobj().printTestStep("Approve Sales Report");
			reportId = salesReport.approveSalesReport(driver, config, reportId);

			fc.utobj().printTestStep("Verify Approval");
			new FinanceSalesPageTest().filterSalesReport(driver, franchiseId);

			fc.utobj().moveToElement(driver, fc.utobj().getElementByXpath(driver,(".//*[contains(text () ,'" + reportId + "')]/ancestor::tr/td/div[@id='menuBar']/layer")));
			String alterText = driver
					.findElement(By.xpath(
							".//*[contains(text () ,'" + reportId + "')]/ancestor::tr/td/div[@id='menuBar']/layer"))
					.getAttribute("id").trim();
			alterText = alterText.replace("Actions_dynamicmenu", "");
			alterText = alterText.replace("Bar", "");
			fc.utobj().clickElement(driver, driver.findElement(By.xpath(
					".//*[contains(text () ,'" + reportId + "')]/ancestor::tr/td/div[@id='menuBar']/layer/a/img")));
			List<WebElement> optionList = driver.findElements(By.xpath(".//div[@id='Actions_dynamicmenu1Menu']/span"));

			String option = null;
			boolean isOptionPresent = false;

			for (int i = 0; i < optionList.size(); i++) {

				option = optionList.get(i).getText().trim();

				if (option.equalsIgnoreCase("Approve")) {
					isOptionPresent = true;
					break;
				}
			}

			if (isOptionPresent == true) {
				fc.utobj().throwsException("was not able to Verify Approval Of Finance Sales Report");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "finance" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-08-16", testCaseId = "TC_FC_Generate_Invoice_When_Sales_Entered_byOwner_163", testCaseDescription = "Verify generate Invoices When Sales Entered by Owner")
	public void generateInvoicesWhenSalesEnteredbyOwner() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());
		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		Map<String, String> dataSet = fc.utobj().readTestData("finance", testCaseId);

		String agrmntVrsnName = fc.utobj().generateTestData(dataSet.get("agreementVersionName"));
		String royaltyPrcntg = dataSet.get("royaltyPercentage");
		String advPercentage = dataSet.get("advertisementPercentage");
		String royaltyAreaFranchiseValue = dataSet.get("RoyaltyAreaFranchise");
		String advAreaFranchiseValue = dataSet.get("AdvAreaFranchise");

		try {
			driver = fc.loginpage().login(driver);

			String password = "t0n1ght123";
			fc.utobj().printTestStep("Navigate To Admin > Finance > Agreement Versions");
			AdminFinanceAgreementVersionsPageTest agreementforSales = new AdminFinanceAgreementVersionsPageTest();

			fc.utobj().printTestStep("Add Agreement");
			String agreementVersion = agreementforSales.addAgreement(driver, config, agrmntVrsnName, 
					 royaltyPrcntg, advPercentage, royaltyAreaFranchiseValue, advAreaFranchiseValue);

			fc.utobj().printTestStep("Navigate To Admin > Franchise Location > Add Franchise Location");
			fc.utobj().printTestStep("Add Franchise Location");

			String regionName = fc.utobj().generateTestData("Test");
			String storeType = fc.utobj().generateTestData("Test");
			String franchiseId = fc.utobj().generateTestData("Test");
			AdminFranchiseLocationAddFranchiseLocationPageTest franchise = new AdminFranchiseLocationAddFranchiseLocationPageTest();
			franchiseId = franchise.addFranchiseLocation_All_Agreement(driver, franchiseId, regionName, storeType,
					agreementVersion, config);

			fc.utobj().printTestStep("Navigate To Admin > Finance > Finance Setup Preferences");
			fc.utobj().printTestStep("Setup Medium of submission of Sales Reports");
			String salesReportType = "webForm";
			new AdminFinanceFinanceSetupPreferencesPageTest().mediumOfSubmissionOfSalesReports(driver, config,
					salesReportType);

			fc.utobj().printTestStep("Add Franchise User");
			AdminUsersManageManageFranchiseUsersPageTest franUser = new AdminUsersManageManageFranchiseUsersPageTest();
			String FranUserName = fc.utobj().generateTestData("TestF");
			String roleName = "Default Franchise Role";
			String emailId = "commonautomation@staffex.com";
			String franUserName = franUser.addFranchiseUser(driver, FranUserName, password, franchiseId, roleName,
					emailId);

			fc.home_page().logout(driver);
			fc.utobj().printTestStep("Login With Franchise User");
			fc.loginpage().loginWithParameter(driver, franUserName, password);

			String categoryQuantity = "110";
			String categoryAmount = "6000";
			String reportId = enterSalesReportforOwner(driver, config, categoryQuantity, categoryAmount);

			fc.home_page().logout(driver);
			fc.loginpage().loginWithParameter(driver, config.get("userName"), config.get("password"));

			fc.utobj().printTestStep("Navigate To Finance > Sales Page");
			fc.finance().finance_common().financeSalesPage(driver);

			fc.utobj().printTestStep("Filter Sales Report By Franchise ID");
			new FinanceSalesPageTest().filterSalesReport(driver, franchiseId);

			FinanceSalesPageTest salesReport = new FinanceSalesPageTest();
			fc.utobj().printTestStep("Approve Sales Report");
			reportId = salesReport.approveSalesReport(driver, config, reportId);

			fc.utobj().printTestStep("Filter Sales Report By Franchise Id");
			new FinanceSalesPageTest().filterSalesReport(driver, franchiseId);

			fc.utobj().printTestStep("Generate Invoice");
			fc.utobj().actionImgOption(driver, franchiseId, "Generate Invoice");

			fc.utobj().clickPartialLinkText(driver, reportId);
			boolean isInvoicedText = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//td[contains(text () , 'Invoiced')]");

			if (isInvoicedText == false) {
				fc.utobj().throwsException("Not verify Action Logs");
			}

			fc.utobj().clickElement(driver, new FinanceSalesPage(driver).royalityPage);
			fc.utobj().printTestStep("Filter Sales Report By Franchise Id");
			new FinanceSalesPageTest().filterSalesReport(driver, franchiseId);

			fc.utobj().printTestStep("Verify The Generated Invoice At Royalty Page");

			boolean isTextPresent = fc.utobj().assertLinkText(driver, franchiseId);

			if (isTextPresent == false) {
				fc.utobj().throwsException("was not able to verify Generated Invoice");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "finance" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-08-16", testCaseId = "TC_FC_Apply_Payment_When_Sales_Entered_byOwner_164", testCaseDescription = "Verify Apply Payments When Sales Entered by Owner")
	public void applyPaymentsWhenSalesEnteredbyOwner() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());
		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		Map<String, String> dataSet = fc.utobj().readTestData("finance", testCaseId);

		String agrmntVrsnName = fc.utobj().generateTestData(dataSet.get("agreementVersionName"));
		String royaltyPrcntg = dataSet.get("royaltyPercentage");
		String advPercentage = dataSet.get("advertisementPercentage");
		String royaltyAreaFranchiseValue = dataSet.get("RoyaltyAreaFranchise");
		String advAreaFranchiseValue = dataSet.get("AdvAreaFranchise");

		try {
			driver = fc.loginpage().login(driver);

			String password = "t0n1ght123";
			fc.utobj().printTestStep("Navigate To Admin > Finance > Agreement Versions");
			AdminFinanceAgreementVersionsPageTest agreementforSales = new AdminFinanceAgreementVersionsPageTest();

			fc.utobj().printTestStep("Add Agreement");
			String agreementVersion = agreementforSales.addAgreement(driver, config, agrmntVrsnName, 
					 royaltyPrcntg, advPercentage, royaltyAreaFranchiseValue, advAreaFranchiseValue);

			fc.utobj().printTestStep("Navigate To Admin > Franchise Location > Add Franchise Location");
			fc.utobj().printTestStep("Add Franchise Location");

			String regionName = fc.utobj().generateTestData("Test");
			String storeType = fc.utobj().generateTestData("Test");
			String franchiseId = fc.utobj().generateTestData("Test");
			AdminFranchiseLocationAddFranchiseLocationPageTest franchise = new AdminFranchiseLocationAddFranchiseLocationPageTest();
			franchiseId = franchise.addFranchiseLocation_All_Agreement(driver, franchiseId, regionName, storeType,
					agreementVersion, config);

			fc.utobj().printTestStep("Navigate To Admin > Finance > Finance Setup Preferences");
			fc.utobj().printTestStep("Setup Medium of submission of Sales Reports");
			String salesReportType = "webForm";
			new AdminFinanceFinanceSetupPreferencesPageTest().mediumOfSubmissionOfSalesReports(driver, config,
					salesReportType);

			fc.utobj().printTestStep("Add Franchise User");
			AdminUsersManageManageFranchiseUsersPageTest franUser = new AdminUsersManageManageFranchiseUsersPageTest();
			String FranUserName = fc.utobj().generateTestData("TestF");
			String roleName = "Default Franchise Role";
			String emailId = "commonautomation@staffex.com";
			String franUserName = franUser.addFranchiseUser(driver, FranUserName, password, franchiseId, roleName,
					emailId);

			fc.home_page().logout(driver);

			fc.utobj().printTestStep("Login With Franchise User");
			fc.loginpage().loginWithParameter(driver, franUserName, password);

			String categoryQuantity = "110";
			String categoryAmount = "6000";
			String reportId = enterSalesReportforOwner(driver, config, categoryQuantity, categoryAmount);

			fc.utobj().printTestStep("Logout From Franchise User");
			fc.home_page().logout(driver);

			fc.utobj().printTestStep("Login With Admin User");
			fc.loginpage().loginWithParameter(driver, config.get("userName"), config.get("password"));

			fc.utobj().printTestStep("Navigate To Finance > Sales Page");
			fc.finance().finance_common().financeSalesPage(driver);

			fc.utobj().printTestStep("Filter Sales Report By Franchise ID");
			new FinanceSalesPageTest().filterSalesReport(driver, franchiseId);

			FinanceSalesPageTest salesReport = new FinanceSalesPageTest();
			fc.utobj().printTestStep("Approve Sales Report");
			reportId = salesReport.approveSalesReport(driver, config, reportId);

			fc.utobj().printTestStep("Filter Sales Report By Franchise Id");
			new FinanceSalesPageTest().filterSalesReport(driver, franchiseId);

			fc.utobj().printTestStep("Generate Invoice");
			fc.utobj().actionImgOption(driver, franchiseId, "Generate Invoice");

			fc.utobj().printTestStep("Apply Payments");

			fc.utobj().printTestStep("Navigate To Finance > Payments Page");
			fc.finance().finance_common().financePaymentsPage(driver);

			double advVal = Double.parseDouble(categoryAmount) * Double.parseDouble(advPercentage) / 100;
			double royaltyVal = Double.parseDouble(categoryAmount) * Double.parseDouble(royaltyPrcntg) / 100;

			double amountToBePaid = royaltyVal + advVal;

			fc.utobj().printTestStep("Navigate To Apply / Receive Payment Tab");
			FinanceRoyaltiesPage royalties_Page = new FinanceRoyaltiesPage(driver);
			
			fc.utobj().clickElement(driver, royalties_Page.applyReceivePaymentLnk);
			
			fc.utobj().selectDropDownByPartialText(driver, royalties_Page.franchiseIDforPayment, franchiseId);
			
			fc.utobj().clickElement(driver, royalties_Page.selectPaymentAppliedforAll);
			
			fc.utobj().clickElement(driver, royalties_Page.continueBtn);
			
			fc.utobj().sendKeys(driver, royalties_Page.paymentAmount, Double.toString(amountToBePaid));
			
			fc.utobj().clickElement(driver, royalties_Page.collectionMethodCash);
			
			fc.utobj().sendKeys(driver, royalties_Page.comments, "Automation Testing");
			
			fc.utobj().clickElement(driver, royalties_Page.saveBtn);

			fc.utobj().printTestStep("Veriy The All Amount Paid");
			
			fc.utobj().selectDropDownByValue(driver, new FinancePaymentsPage(driver).resultsPerPage, "500");

			String amountApplied = fc.utobj().getText(driver, driver
					.findElement(By.xpath(".//td[contains(text () , '" + franchiseId + "')]/ancestor::tr/td[4]")));

			if (amountApplied.contains(",")) {
				amountApplied = amountApplied.replaceAll(",", "");
			}

			if (amountToBePaid != Double.parseDouble(amountApplied)) {
				fc.utobj().throwsException("wasa not able to verify The All Amount Paid");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}

	}

	@Test(groups = { "finance", "finance_FailedTC" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-08-16", testCaseId = "TC_FC_verify_Royalties_Payments_When_Sales_Entered_byOwner_165", testCaseDescription = "verify Royalties Payments When Sales Entered by Owner")
	public void verifyRoyaltiesPaymentsWhenSalesEnteredbyOwner() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());
		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		Map<String, String> dataSet = fc.utobj().readTestData("finance", testCaseId);

		String agrmntVrsnName = fc.utobj().generateTestData(dataSet.get("agreementVersionName"));
		String royaltyPrcntg = dataSet.get("royaltyPercentage");
		String advPercentage = dataSet.get("advertisementPercentage");
		String royaltyAreaFranchiseValue = dataSet.get("RoyaltyAreaFranchise");
		String advAreaFranchiseValue = dataSet.get("AdvAreaFranchise");

		try {
			driver = fc.loginpage().login(driver);
			String password = "t0n1ght123";
			fc.utobj().printTestStep("Navigate To Admin > Finance > Agreement Versions");
			AdminFinanceAgreementVersionsPageTest agreementforSales = new AdminFinanceAgreementVersionsPageTest();

			fc.utobj().printTestStep("Add Agreement");
			String agreementVersion = agreementforSales.addAgreement(driver, config, agrmntVrsnName, 
					 royaltyPrcntg, advPercentage, royaltyAreaFranchiseValue, advAreaFranchiseValue);

			fc.utobj().printTestStep("Navigate To Admin > Franchise Location > Add Franchise Location");
			fc.utobj().printTestStep("Add Franchise Location");

			String regionName = fc.utobj().generateTestData("Test");
			String storeType = fc.utobj().generateTestData("Test");
			String franchiseId = fc.utobj().generateTestData("Test");
			AdminFranchiseLocationAddFranchiseLocationPageTest franchise = new AdminFranchiseLocationAddFranchiseLocationPageTest();
			franchiseId = franchise.addFranchiseLocation_All_Agreement(driver, franchiseId, regionName, storeType,
					agreementVersion, config);

			fc.utobj().printTestStep("Navigate To Admin > Finance > Finance Setup Preferences");
			fc.utobj().printTestStep("Setup Medium of submission of Sales Reports");
			String salesReportType = "webForm";
			new AdminFinanceFinanceSetupPreferencesPageTest().mediumOfSubmissionOfSalesReports(driver, config,
					salesReportType);

			fc.utobj().printTestStep("Add Franchise User");
			AdminUsersManageManageFranchiseUsersPageTest franUser = new AdminUsersManageManageFranchiseUsersPageTest();
			String FranUserName = fc.utobj().generateTestData("TestF");
			String roleName = "Default Franchise Role";
			String emailId = "commonautomation@staffex.com";
			String franUserName = franUser.addFranchiseUser(driver, FranUserName, password, franchiseId, roleName,
					emailId);

			fc.home_page().logout(driver);
			fc.utobj().printTestStep("Login With Franchise User");
			fc.loginpage().loginWithParameter(driver, franUserName, password);

			String categoryQuantity = "110";
			String categoryAmount = "6000";
			String reportId = enterSalesReportforOwner(driver, config, categoryQuantity, categoryAmount);

			fc.home_page().logout(driver);
			fc.loginpage().loginWithParameter(driver, config.get("userName"), config.get("password"));

			fc.utobj().printTestStep("Navigate To Finance > Sales Page");
			fc.finance().finance_common().financeSalesPage(driver);

			fc.utobj().printTestStep("Filter Sales Report By Franchise ID");
			new FinanceSalesPageTest().filterSalesReport(driver, franchiseId);

			FinanceSalesPageTest salesReport = new FinanceSalesPageTest();
			fc.utobj().printTestStep("Approve Sales Report");
			reportId = salesReport.approveSalesReport(driver, config, reportId);

			fc.utobj().printTestStep("Filter Sales Report By Franchise Id");
			new FinanceSalesPageTest().filterSalesReport(driver, franchiseId);

			fc.utobj().printTestStep("Generate Invoice");
			fc.utobj().actionImgOption(driver, franchiseId, "Generate Invoice");

			fc.utobj().printTestStep("Navigate To Finance > Royality Page");
			fc.finance().finance_common().financeRoyaltiesPage(driver);
			fc.utobj().printTestStep("Filter Sales Report By Franchise Id");
			new FinanceSalesPageTest().filterSalesReport(driver, franchiseId);

			String invoiceId = fc.utobj().getText(driver, driver
					.findElement(By.xpath(".//a[contains(text () , '" + franchiseId + "')]/ancestor::tr/td[2]/a[1]")));

			fc.utobj().printTestStep("Apply Payments");

			fc.utobj().printTestStep("Navigate To Finance > Payments Page");
			fc.finance().finance_common().financePaymentsPage(driver);

			double advVal = Double.parseDouble(categoryAmount) * Double.parseDouble(advPercentage) / 100;
			double royaltyVal = Double.parseDouble(categoryAmount) * Double.parseDouble(royaltyPrcntg) / 100;

			double amountToBePaid = royaltyVal + advVal;

			fc.utobj().printTestStep("Navigate To Apply / Receive Payment Tab");
			FinanceRoyaltiesPage royalties_Page = new FinanceRoyaltiesPage(driver);
			fc.utobj().clickElement(driver, royalties_Page.applyReceivePaymentLnk);
			fc.utobj().selectDropDownByPartialText(driver, royalties_Page.franchiseIDforPayment, franchiseId);
			fc.utobj().clickElement(driver, royalties_Page.selectPaymentAppliedforAll);
			fc.utobj().clickElement(driver, royalties_Page.continueBtn);
			fc.utobj().sendKeys(driver, royalties_Page.paymentAmount, Double.toString(amountToBePaid));
			fc.utobj().clickElement(driver, royalties_Page.collectionMethodCash);
			fc.utobj().sendKeys(driver, royalties_Page.comments, "Automation Testing");
			fc.utobj().clickElement(driver, royalties_Page.saveBtn);

			fc.utobj().printTestStep("Verify Total Sales , Royality Amount And Adv Amount At Royalty Page");
			fc.home_page().logout(driver);
			fc.utobj().printTestStep("Login With Franchise User");
			fc.loginpage().loginWithParameter(driver, franUserName, password);

			fc.utobj().printTestStep("Navigate To Fianace > Royalty Page");
			fc.finance().finance_common().financeRoyaltiesPage(driver);

			FinanceOwnerSalesRoyaltiesPaymentsPage pobj = new FinanceOwnerSalesRoyaltiesPaymentsPage(driver);
			boolean isTextPresent = fc.utobj().assertLinkText(driver,
					driver.findElement(By.id("resultsPerPage")).getText());
			if (isTextPresent == true) {
				fc.utobj().selectDropDownByVisibleText(driver, pobj.selectViewPerPage, "500");
			}

			String totalSalesAmount = fc.utobj().getText(driver,
					driver.findElement(By.xpath(".//a[contains(text () , '" + invoiceId + "')]/ancestor::tr/td[11]")));
			if (totalSalesAmount.contains(",")) {
				totalSalesAmount = totalSalesAmount.replaceAll(",", "");
			}

			double totalSalesAmnt = Double.parseDouble(totalSalesAmount);
			double enteredSalesAmount = Double.parseDouble(categoryAmount);
			DecimalFormat salesAmnt = new DecimalFormat("#.##");
			enteredSalesAmount = Double.valueOf(salesAmnt.format(enteredSalesAmount));
			double royaltyPercent = Double.parseDouble(royaltyPrcntg);
			double calRoyaltyAmt = ((enteredSalesAmount * royaltyPercent) / 100.0);
			DecimalFormat royAmnt = new DecimalFormat("#.##");
			calRoyaltyAmt = Double.valueOf(royAmnt.format(calRoyaltyAmt));

			String totalRoyaltyatSummary = fc.utobj().getText(driver,
					driver.findElement(By.xpath(".//a[contains(text () , '" + invoiceId + "')]/ancestor::tr/td[4]")));
			if (totalRoyaltyatSummary.contains(",")) {
				totalRoyaltyatSummary = totalRoyaltyatSummary.replaceAll(",", "");
			}
			double totalRoyalty = Double.parseDouble(totalRoyaltyatSummary);
			double advPercent = Double.parseDouble(advPercentage);
			double calAdvAmt = ((enteredSalesAmount * advPercent) / 100.0);
			calAdvAmt = Double.valueOf(royAmnt.format(calAdvAmt));

			String totalAdvatSummary = fc.utobj().getText(driver,
					driver.findElement(By.xpath(".//a[contains(text () , '" + invoiceId + "')]/ancestor::tr/td[5]")));
			if (totalAdvatSummary.contains(",")) {
				totalAdvatSummary = totalAdvatSummary.replaceAll(",", "");
			}
			double totalAdvertisement = Double.parseDouble(totalAdvatSummary);

			System.out.println(totalSalesAmnt +" = "+enteredSalesAmount);
			
			if (totalSalesAmnt != enteredSalesAmount) {
				fc.utobj().throwsException(
						"Total Sales Amount at Royalty Summary Page is not same as Calculated Total Sales Amount! ");
			}

			if (calRoyaltyAmt != totalRoyalty) {
				fc.utobj().throwsException("Royalty Amount on Royalty Summary Page is not same as royalty calculated");
			}
			if (calAdvAmt != totalAdvertisement) {
				fc.utobj().throwsException(
						"Advertisement Amount on Royalty Summary Page is not same as Advertisement calculated");
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}

	}
}
