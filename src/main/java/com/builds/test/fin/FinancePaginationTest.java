package com.builds.test.fin;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.xml.parsers.ParserConfigurationException;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;
import org.xml.sax.SAXException;
import com.builds.uimaps.fin.FinanceSalesPage;
import com.builds.utilities.FranconnectUtil;
import com.builds.utilities.TestCase;

public class FinancePaginationTest {
	FranconnectUtil fc = new FranconnectUtil();

	@Test(groups = { "finance" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-05-17", testCaseId = "TC_Finance_Paging_Report_Filter_FinanceSales", testCaseDescription = "This test case will verify Finance Sales Paging all the summary of Finance Module ", reference = {
			"" })
	public void VerifyFinanceSummaryPaging_FinanceSales() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		try {
			driver = fc.loginpage().login(driver);
			fc.utobj().printTestStep("verify Paging for Finance Sales ");
			showPagingFinanceModule(config, driver, "FinanceSales", 20);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception ex) {
			fc.utobj().quitBrowserOnCatch(driver, ex, testCaseId);
		}
	}

	@Test(groups = { "finance" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "TC_Finance_Paging_Report_Filter_ProfitLostSales", testCaseDescription = "This test case will verify ProfitLostSales Paging all the summary of Finance Module ", reference = {
			"" })
	public void VerifyFinanceSummaryPaging_ProfitLostSales() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		try {
			driver = fc.loginpage().login(driver);
			fc.utobj().printTestStep("verify Paging for Profit / Loss Summary");
			showPagingFinanceModule(config, driver, "ProfitLostSales", 20);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception ex) {
			fc.utobj().quitBrowserOnCatch(driver, ex, testCaseId);
		}
	}

	@Test(groups = { "finance" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "TC_Finance_Paging_Report_Filter_Royalties", testCaseDescription = "This test case will verify Royalties Paging all the summary of Finance Module ", reference = {
			"" })
	public void VerifyFinanceSummaryPaging_Royalties() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		try {
			driver = fc.loginpage().login(driver);
			fc.utobj().printTestStep("verify Paging for Royalties Summary");
			showPagingFinanceModule(config, driver, "Royalties", 20);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception ex) {
			fc.utobj().quitBrowserOnCatch(driver, ex, testCaseId);
		}
	}

	@Test(groups = { "finance" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "TC_Finance_Paging_Report_Filter_PaymentSummary", testCaseDescription = "This test case will verify PaymentSummary Paging all the summary of Finance Module ", reference = {
			"" })
	public void VerifyFinanceSummaryPaging_PaymentSummary() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		try {
			driver = fc.loginpage().login(driver);
			fc.utobj().printTestStep("verify Paging for Payment Summary");
			showPagingFinanceModule(config, driver, "PaymentSummary", 20);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception ex) {
			fc.utobj().quitBrowserOnCatch(driver, ex, testCaseId);
		}
	}

	@Test(groups = { "finance" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "TC_Finance_Paging_Report_Filter_AreaFranchise", testCaseDescription = "This test case will verify AreaFranchise Paging all the summary of Finance Module ", reference = {
			"" })
	public void VerifyFinanceSummaryPaging_AreaFranchise() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		try {
			driver = fc.loginpage().login(driver);
			fc.utobj().printTestStep("verify Paging for 	Area Franchise");
			showPagingFinanceModule(config, driver, "AreaFranchise", 20);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception ex) {
			fc.utobj().quitBrowserOnCatch(driver, ex, testCaseId);
		}
	}

	@Test(groups = { "finance" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "TC_Finance_Paging_Report_Filter_EFTTransaction", testCaseDescription = "This test case will verify EFT Transaction Paging all the summary of Finance Module ", reference = {
			"" })
	public void VerifyFinanceSummaryPaging_EFTTransaction() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		try {
			driver = fc.loginpage().login(driver);
			
			fc.utobj().printTestStep("verify Paging for EFT Transaction");
			
			showPagingFinanceModule(config, driver, "EFTTransaction", 20);
		
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		
		} catch (Exception ex) {
			fc.utobj().quitBrowserOnCatch(driver, ex, testCaseId);
		}
	}

	@Test(groups = { "finance" })

	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "TC_Finance_Paging_Report_Filter_Documents", testCaseDescription = "This test case will verify Documents Paging ", reference = {
			"" })
	public void VerifyFinanceSummaryPaging() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		try {
			driver = fc.loginpage().login(driver);
			fc.utobj().printTestStep("verify Paging for Documents");
			showPagingFinanceModule(config, driver, "Documents", 20);
			// End Finance Summary paging

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	public void showPagingFinanceModule(Map<String, String> config, WebDriver driver, String subModuleName,
			int viewPerPage)
			throws ParserConfigurationException, SAXException, IOException, InterruptedException, Exception

	{
		String testCaseId = "TC_Finance_Paging_Report_Filter";

		if (subModuleName == null)
			subModuleName = "FinanceSales";
		List<WebElement> records = null;
		FinanceSalesPage pobj = new FinanceSalesPage(driver);
		fc.utobj().printTestStep("Search " + subModuleName + " and click");
		boolean financeModule = driver.findElement(By.xpath(".//li[@id='module_financials']/a")).isDisplayed();

		if (subModuleName != null && "FinanceSales".equals(subModuleName)) {

			try {
				if (financeModule == false) {

					fc.utobj().clickElement(driver, pobj.moreBtn);
				}

				fc.utobj().clickElement(driver, pobj.financeBtn);
				
				fc.utobj().clickElement(driver, pobj.salesReport);
			
				records = driver.findElements(By.xpath(".//*[@id='siteMainTable']/tbody/tr[1]/td[1]/form[6]/table"));
			
				// Thread.sleep(1000);
			} catch (Exception e) {
				// e.printStackTrace();
				
				fc.utobj().printTestStep("Finance Sales not found or Opernational.");
			}
		} else if (subModuleName != null && "ProfitLostSales".equals(subModuleName)) {

			try {
				if (financeModule == false) {

					fc.utobj().clickElement(driver, pobj.moreBtn);
				}

				fc.utobj().clickElement(driver, pobj.financeBtn);
				
				fc.utobj().clickElement(driver,
						driver.findElement(By.xpath("//a[contains(text(), 'Profit & Loss Statement')]")));
				
				records = driver.findElements(By.xpath(".//*[@id='siteMainTable']/tbody/tr/td[1]/table[4]"));
				// Thread.sleep(1000);
			} catch (Exception e) {
				// e.printStackTrace();
				fc.utobj().printTestStep("Profit & Loss Statement not found or Opernational.");
			}
		} else if (subModuleName != null && "Royalties".equals(subModuleName)) {

			try {
				if (financeModule == false) {

					fc.utobj().clickElement(driver, pobj.moreBtn);
				}

				fc.utobj().clickElement(driver, pobj.financeBtn);
				fc.utobj().clickElement(driver, driver.findElement(By.xpath("//a[contains(text(), 'Royalties')]")));
				records = driver.findElements(By.xpath(".//*[@id='siteMainTable']/tbody/tr/td[1]/table[3]"));
				// Thread.sleep(1000);
			} catch (Exception e) {
				// e.printStackTrace();
				fc.utobj().printTestStep("Royalties not found or Opernational.");
			}
		} else if (subModuleName != null && "PaymentSummary".equals(subModuleName)) {
			try {
				if (financeModule == false) {

					fc.utobj().clickElement(driver, pobj.moreBtn);
				}

				fc.utobj().clickElement(driver, pobj.financeBtn);
				fc.utobj().clickElement(driver, driver.findElement(By.xpath("//a[contains(text(), 'Payments')]")));
				records = driver.findElements(By.xpath(".//*[@id='siteMainTable']/tbody/tr/td[1]/table[3]"));
				// Thread.sleep(1000);
			} catch (Exception e) {
				// e.printStackTrace();
				fc.utobj().printTestStep("Payment summary not found or Opernational.");
			}
		} else if (subModuleName != null && "AreaFranchise".equals(subModuleName)) {

			try {
				if (financeModule == false) {

					fc.utobj().clickElement(driver, pobj.moreBtn);
				}

				fc.utobj().clickElement(driver, pobj.financeBtn);
				fc.utobj().clickElement(driver,
						driver.findElement(By.xpath("//a[contains(text(), 'Area Franchise')]")));
				records = driver.findElements(By.xpath(".//*[@id='printDiv']/table"));
				// Thread.sleep(1000);
			} catch (Exception e) {
				// e.printStackTrace();
				fc.utobj().printTestStep("Area franchise not found or Opernational.");
			}
		} else if (subModuleName != null && "EFTTransaction".equals(subModuleName)) {

			try {
				if (financeModule == false) {

					fc.utobj().clickElement(driver, pobj.moreBtn);
				}

				fc.utobj().clickElement(driver, pobj.financeBtn);
				fc.utobj().clickElement(driver,
						driver.findElement(By.xpath("//a[contains(text(), 'EFT Transactions')]")));
				records = driver.findElements(
						By.xpath(".//*[@id='siteMainTable']/tbody/tr/td/table[3]/tbody/tr/td[2]/table/tbody/tr[6]"));
				// Thread.sleep(1000);
			} catch (Exception e) {
				// e.printStackTrace();
				fc.utobj().printTestStep("EFT transaction not found or Opernational.");
			}
		} else if (subModuleName != null && "Documents".equals(subModuleName)) {

			try {
				if (financeModule == false) {

					fc.utobj().clickElement(driver, pobj.moreBtn);
				}

				fc.utobj().clickElement(driver, pobj.financeBtn);
				fc.utobj().clickElement(driver, driver.findElement(By.xpath("//a[contains(text(), 'Documents')]")));
				records = driver.findElements(By.xpath(".//*[@id='printDiv']/table/tbody"));
				// Thread.sleep(1000);
			} catch (Exception e) {
				// e.printStackTrace();
				fc.utobj().printTestStep("Documents Not Found or Opernational.");
			}
		}

		String header;
		int totalRecordCount = 0;
		int totalrecordPerPage = 0;
		try {
			header = driver.findElement(By.xpath(".//*[@id='pageid']")).getText();
			String options[] = header.split(" ");
			if (options != null) {
				totalRecordCount = Integer.valueOf(options[5]);
				totalrecordPerPage = Integer.valueOf(options[3]);
			}
			fc.utobj().printTestStep(subModuleName + " ToTal Record " + totalRecordCount);

		} catch (Exception E) {
			fc.utobj().printTestStep("Pagination not found in " + subModuleName);
			// System.out.println("Pagination not found");
			totalRecordCount = 0;
		}

		if (totalRecordCount > 0 && totalRecordCount > viewPerPage) {
			fc.utobj().printTestStep("Pagination exists in " + subModuleName);

			int clickCount = totalRecordCount / viewPerPage;
			String optionsShoud[] = new String[clickCount + 1];
			;
			int inLastRerord = totalRecordCount - (viewPerPage * clickCount);
			for (int cnt = 0; cnt <= clickCount; cnt++) {

				optionsShoud[cnt] = viewPerPage + "";
				if (cnt == clickCount) {
					optionsShoud[cnt] = inLastRerord + "";
				}
			}
			int foundRecordCount = 0;
			int counter = 1;
			try {
				if (optionsShoud != null && optionsShoud.length > 0) {
					for (int i = 0; i < (optionsShoud.length); i++) {
						// System.out.println("===Val=="+lstOptions.get(i)
						// +"==New=="+lstOptions.get(i).getText()
						// +"===clickCount=="+clickCount+"optionsShoud=="+optionsShoud[i]);
						if (true) {

							if (i == 1 && i != 0) {
								fc.utobj().clickElement(driver,
										driver.findElement(By.xpath(".//*[@id='pageid']/a[" + (i) + "]/u")));
								counter = i;
							} else if (i > 1 && i != 0) {
								fc.utobj().clickElement(driver,
										driver.findElement(By.xpath(".//*[@id='pageid']/a[" + (i + 1) + "]/u")));
								counter = i + 1;

							}
							foundRecordCount = pagingRecordsFinanceSales(driver, records, subModuleName).size();
							if (foundRecordCount > 0) {

								if (foundRecordCount == Integer.parseInt(optionsShoud[i])) {
									// System.out.println("found record
									// "+foundRecordCount+"= Test Case Pass for
									// the page "+(i));
									fc.utobj().printTestStep("" + foundRecordCount + " records for page = " + counter);
								} else {

									if (foundRecordCount <= Integer.parseInt(optionsShoud[i])) {
										// System.out.println("found record
										// "+foundRecordCount+"= Test Case Pass
										// for the Last page "+(i));
										fc.utobj().printTestStep(
												"" + foundRecordCount + " records for page = " + counter);
									} else {
										// System.out.println("found record
										// "+foundRecordCount+"= Test Case Fail
										// for the page "+(i));
										// fc.utobj().printTestStep(testCaseId,
										// "found record "+foundRecordCount+"=
										// Test Case Fail for the page "+(i));
										fc.utobj().throwsException("found record " + foundRecordCount
												+ "= Test Case Fail for the page Almost Last Page " + counter);
									}
								}
							} else {
								fc.utobj().throwsException(
										"" + foundRecordCount + " records for page " + counter + " Test case fails");
							}
							// Check for paging after sort function

							fc.utobj().clickElement(driver, driver
									.findElement(By.xpath(".//table[@class='summaryTblex']/tbody/tr[1]/td[2]/a")));
							foundRecordCount = pagingRecordsFinanceSales(driver, records, subModuleName).size();
							if (foundRecordCount > 0) {

								if (foundRecordCount == Integer.parseInt(optionsShoud[i])) {
									// System.out.println("found record
									// "+foundRecordCount+"= Test Case Pass for
									// the page "+(i));
									fc.utobj().printTestStep(
											"After Sort record " + foundRecordCount + " for the page " + counter);
								} else {

									if (foundRecordCount <= Integer.parseInt(optionsShoud[i])) {
										// System.out.println("found record
										// "+foundRecordCount+"= Test Case Pass
										// for the Last page "+(i));
										fc.utobj().printTestStep(
												"After Sort record " + foundRecordCount + " for the page " + counter);
									} else {
										fc.utobj().throwsException("After Sort record " + foundRecordCount
												+ "= Test Case Fail for the page Almost Last Page " + (i + 1));
									}

								}
							} else {
								fc.utobj().throwsException("" + foundRecordCount + " records for page " + counter
										+ " Test case fails on click sort");
							}
							//

						}
					}
				}

			} catch (Exception e) {
				e.printStackTrace();
			}

			try {
				// fc.utobj().printTestStep(testCaseId, "Reset our page to the
				// page 1 to check the results Per Page verification.");
				fc.utobj().clickElement(driver, driver.findElement(By.xpath(".//*[@id='pageid']/a[1]/u")));
				fc.utobj().selectDropDown(driver, driver.findElement(By.id("resultsPerPage")), "100");

				foundRecordCount = pagingRecordsFinanceSales(driver, records, subModuleName).size();
				if (foundRecordCount > 0) {
				}
				if (foundRecordCount <= 100) {
					// System.out.println("found record in Result Per page all
					// "+foundRecordCount+" test case passed at View Per Page");
					fc.utobj().printTestStep("View Per Page " + foundRecordCount + " records.");

				} else {
					if (foundRecordCount > 100) {
						fc.utobj().throwsException("View Per Page " + foundRecordCount + " records.");
					}
				}

				// Check for paging after click on Sort function
				fc.utobj().clickElement(driver,
						driver.findElement(By.xpath(".//table[@class='summaryTblex']/tbody/tr[1]/td[2]/a")));

				foundRecordCount = pagingRecordsFinanceSales(driver, records, subModuleName).size();
				if (foundRecordCount > 0) {
				}
				if (foundRecordCount <= 100) {
					// System.out.println("found record in Result Per page all
					// "+foundRecordCount+" test case passed at View Per Page");
					fc.utobj().printTestStep("After Sort found record in Result Per page  " + foundRecordCount
							+ " test case passed at View Per Page");

				} else {
					if (foundRecordCount > 100) {
						fc.utobj().throwsException("View Per Page " + foundRecordCount + " records.");
					}
				}

			} catch (Exception E) {
				// System.out.println("resultsPerPage Field Not Found!");
				fc.utobj().printTestStep("View Per Page  Field Not Found! in " + subModuleName);

				// totalRecordCount = 0;
			}

			try {
				fc.utobj().clickLink(driver, "Show All");
				// System.out.println("totalRecordCount===in show
				// all"+totalRecordCount);
				fc.utobj().printTestStep("totalRecordCount in show all " + subModuleName + " = " + totalRecordCount);
				foundRecordCount = pagingRecordsFinanceSales(driver, records, subModuleName).size();
				if (foundRecordCount > 0) {
				}
				if (totalRecordCount == foundRecordCount) {
					// System.out.println("found record in show all
					// "+foundRecordCount+" test case passed");
					fc.utobj().printTestStep("found record in show all " + foundRecordCount + " test case passed");
				} else {
					if (foundRecordCount > 0) {
						if (totalRecordCount != foundRecordCount) {
							fc.utobj()
									.throwsException("Show All " + foundRecordCount + " records not working correct.");
						}
					}
				}

			} catch (Exception E) {
				// System.out.println("Show All Field Not Found!");
				fc.utobj().printTestStep("Show All field not found!");
				// totalRecordCount = 0;
			}

		} else {
			// System.out.println("Pagination Not Exists");
			fc.utobj().printTestStep("Pagination Not Exists in " + subModuleName);
		}

		// sortedList
		try {
			boolean isSorttable = sortedListFinanceSales(driver, config, records, subModuleName);
			if (isSorttable == true) {
				fc.utobj().printTestStep("Sorting function working for  " + subModuleName);
			} else {
				fc.utobj().printTestStep("Sorting function not Working for  " + subModuleName + " OR not applicable.");
			}

		} catch (Exception e) {
			// e.printStackTrace();
			fc.utobj().printTestStep("Eception in Sorting function which Working for  " + subModuleName);
		}

		// End sortedList

		fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
	}

	public boolean sortedListFinanceSales(WebDriver driver, Map<String, String> config, List<WebElement> records,
			String subModuleName) {

		boolean isSorttable = false;
		try {
			records = pagingRecordsFinanceSales(driver, records, subModuleName);
			ArrayList<String> obtainedList = new ArrayList<>();
			ArrayList<String> sortedList = new ArrayList<>();
			List<WebElement> elementList = records; // driver.findElements(By.xpath(YourLocator));
			for (WebElement we : elementList) {
				obtainedList.add(we.getText());
			}
			fc.utobj().clickElement(driver,
					driver.findElement(By.xpath(".//table[@class='summaryTblex']/tbody/tr[1]/td[2]//a[not(*)]")));
			records = pagingRecordsFinanceSales(driver, records, subModuleName);
			for (WebElement we : records) {
				sortedList.add(we.getText());
			}
			try {
				if (sortedList != null) {
					for (int i = 0; i < sortedList.size(); i++) {
						if (sortedList.get(0) != obtainedList.get(0)) {
							isSorttable = true;
							break;
						}
					}
				}

			} catch (Exception e) {
				isSorttable = false;

			}

		} catch (Exception e) {
			isSorttable = false;
			// e.printStackTrace();
		}

		return isSorttable;
	}

	public List<WebElement> pagingRecordsFinanceSales(WebDriver driver, List<WebElement> records,
			String subModuleName) {

		if (subModuleName == null)
			subModuleName = "LeadSummary";
		List<WebElement> listofElements = null;
		int rc = 0;
		if (subModuleName != null && "FinanceSales".equals(subModuleName)) {
			listofElements = driver.findElements(By
					.xpath(".//table[@class='summaryTblex']/tbody/tr/td[@class='botBorder colPadding'][3]//a[not(*)]"));
		} else if (subModuleName != null && "ProfitLostSales".equals(subModuleName)) {
			listofElements = driver.findElements(By
					.xpath(".//table[@class='summaryTblex']/tbody/tr/td[@class='botBorder colPadding'][2]//a[not(*)]"));
		} else if (subModuleName != null && "Royalties".equals(subModuleName)) {
			listofElements = driver.findElements(By.xpath(
					".//div[@id='printReady']//table[@class='summaryTblex']/tbody/tr/td[@class='botBorder colPadding'][2]/a"));
		} else if (subModuleName != null && "PaymentSummary".equals(subModuleName)) {
			listofElements = driver.findElements(
					By.xpath(".//table[@class='summaryTblex']/tbody/tr/td[@class='botBorder colPadding'][2]/a"));
		} else if (subModuleName != null && "AreaFranchise".equals(subModuleName)) {
			listofElements = driver.findElements(
					By.xpath(".//table[@class='summaryTblex']/tbody/tr/td[@class='botBorder colPadding'][2]/a"));
		} else if (subModuleName != null && "EFTTransaction".equals(subModuleName)) {
			listofElements = driver.findElements(
					By.xpath(".//table[@class='summaryTblex']/tbody/tr/td[@class='botBorder colPadding'][1]/a"));
		} else if (subModuleName != null && "Documents".equals(subModuleName)) {
			listofElements = driver.findElements(
					By.xpath(".//table[@class='summaryTblex']/tbody/tr/td[@class='botBorder colPadding'][1]/a"));
		}
		rc = listofElements.size();
		return listofElements;

	}

}
