package com.builds.test.fin;

import java.text.DecimalFormat;
import java.util.Map;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.Test;
import com.builds.test.admin.AdminFranchiseLocationAddFranchiseLocationPageTest;
import com.builds.uimaps.fin.FinancePaymentsPage;
import com.builds.uimaps.fin.FinanceRoyaltiesPage;
import com.builds.uimaps.fin.FinanceSalesPage;
import com.builds.utilities.FranconnectUtil;
import com.builds.utilities.TestCase;

public class FinancePaymentsPageTest {

	FranconnectUtil fc = new FranconnectUtil();

	@Test(groups = "finance")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2017-06-12", testCaseId = "TC_FC_Add_CREDIT_MEMO_109", testCaseDescription = "Verify Add Credit Memo")
	public void addCreditMemoPaymentsPage() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());
		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		Map<String, String> dataSet = fc.utobj().readTestData("finance", testCaseId);

		String agrmntVrsnName = fc.utobj().generateTestData(dataSet.get("agreementVersionName"));
		String royaltyPrcntg = dataSet.get("royaltyPercentage");
		String advPercentage = dataSet.get("advertisementPercentage");
		String royaltyAreaFranchiseValue = dataSet.get("RoyaltyAreaFranchise");
		String advAreaFranchiseValue = dataSet.get("AdvAreaFranchise");
		
		try {
			driver = fc.loginpage().login(driver);
			FinancePaymentsPage pobj = new FinancePaymentsPage(driver);

			fc.utobj().printTestStep("Navigate To Admin > Finance > Agreement Versions");
			AdminFinanceAgreementVersionsPageTest agreementforSales = new AdminFinanceAgreementVersionsPageTest();

			fc.utobj().printTestStep("Add Agreement");
			String agreementVersion = agreementforSales.addAgreement(driver, config, agrmntVrsnName, 
					 royaltyPrcntg, advPercentage, royaltyAreaFranchiseValue, advAreaFranchiseValue);

			fc.utobj().printTestStep("Navigate To Admin > Franchise Location > Add Franchise Location");
			fc.utobj().printTestStep("Add Franchise Location");

			String regionName = fc.utobj().generateTestData("GT");
			String storeType = fc.utobj().generateTestData("GT");
			String franchiseId = fc.utobj().generateTestData("GT");
			AdminFranchiseLocationAddFranchiseLocationPageTest franchise = new AdminFranchiseLocationAddFranchiseLocationPageTest();
			franchiseId = franchise.addFranchiseLocation_All_Agreement(driver, franchiseId, regionName, storeType,
					agreementVersion, config);

			fc.utobj().printTestStep("Navigate To Finance > Payments Page");
			fc.finance().finance_common().financePaymentsPage(driver);

			fc.utobj().printTestStep("Credits/Debit Tab");
			fc.utobj().clickElement(driver, pobj.creaditDebitTab);

			fc.utobj().printTestStep("Add Credit Memo");
			fc.utobj().clickElement(driver, pobj.addCreditsDebitsMemo);
			fc.utobj().selectDropDownByPartialText(driver, pobj.memoFranchiseID, franchiseId);
			fc.utobj().selectDropDown(driver, pobj.memoType, "Credit");
			fc.utobj().sendKeys(driver, pobj.memoAmount, "1000");
			fc.utobj().sendKeys(driver, pobj.memoComments, "Automation Memo");
			fc.utobj().clickElement(driver, pobj.saveMemeoBtn);

			fc.utobj().selectDropDownByValue(driver, pobj.resultsPerPage, "500");

			boolean isTextPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//td[contains(text () , '" + franchiseId
					+ "')]/ancestor::tr/td[contains(text () , 'Credit Memo')]");
			if (isTextPresent == false) {
				fc.utobj().throwsException("was not able to add credit memo");
			}
			
			//filtersAtCreditDebitTab(driver, franchiseId);
			
			//fc.utobj().actionImgOption(driver, franchiseId, "Apply to Invoice");
		
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "finance")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2017-06-12", testCaseId = "TC_FC_Add_DEBIT_MEMO_110", testCaseDescription = "Verify Add Debit Memo")
	public void addDebitMemoPaymentsPage() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		Map<String, String> dataSet = fc.utobj().readTestData("finance", testCaseId);

		String agrmntVrsnName = fc.utobj().generateTestData(dataSet.get("agreementVersionName"));
		String royaltyPrcntg = dataSet.get("royaltyPercentage");
		String advPercentage = dataSet.get("advertisementPercentage");
		String royaltyAreaFranchiseValue = dataSet.get("RoyaltyAreaFranchise");
		String advAreaFranchiseValue = dataSet.get("AdvAreaFranchise");
		
		try {
			driver = fc.loginpage().login(driver);
			FinancePaymentsPage pobj = new FinancePaymentsPage(driver);

			fc.utobj().printTestStep("Navigate To Admin > Finance > Agreement Versions");
			AdminFinanceAgreementVersionsPageTest agreementforSales = new AdminFinanceAgreementVersionsPageTest();

			fc.utobj().printTestStep("Add Agreement");
			String agreementVersion = agreementforSales.addAgreement(driver, config, agrmntVrsnName, 
					 royaltyPrcntg, advPercentage, royaltyAreaFranchiseValue, advAreaFranchiseValue);

			fc.utobj().printTestStep("Navigate To Admin > Franchise Location > Add Franchise Location");
			fc.utobj().printTestStep("Add Franchise Location");

			String regionName = fc.utobj().generateTestData("GT");
			String storeType = fc.utobj().generateTestData("GT");
			String franchiseId = fc.utobj().generateTestData("GT");
			AdminFranchiseLocationAddFranchiseLocationPageTest franchise = new AdminFranchiseLocationAddFranchiseLocationPageTest();
			franchiseId = franchise.addFranchiseLocation_All_Agreement(driver, franchiseId, regionName, storeType,
					agreementVersion, config);

			fc.utobj().printTestStep("Navigate To Finance > Payments Page");
			fc.finance().finance_common().financePaymentsPage(driver);

			fc.utobj().printTestStep("Credits/Debit Tab");
			fc.utobj().clickElement(driver, pobj.creaditDebitTab);

			fc.utobj().printTestStep("Add Debit Memo");
			fc.utobj().clickElement(driver, pobj.addCreditsDebitsMemo);
			fc.utobj().selectDropDownByPartialText(driver, pobj.memoFranchiseID, franchiseId);
			fc.utobj().selectDropDown(driver, pobj.memoType, "Debit");
			fc.utobj().sendKeys(driver, pobj.memoAmount, "1000");
			fc.utobj().sendKeys(driver, pobj.memoComments, "Automation Memo");
			fc.utobj().clickElement(driver, pobj.saveMemeoBtn);

			fc.utobj().selectDropDownByValue(driver, pobj.resultsPerPage, "500");

			boolean isTextPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//td[contains(text () , '" + franchiseId
					+ "')]/ancestor::tr/td[contains(text () , 'Debit Memo')]");
			if (isTextPresent == false) {
				fc.utobj().throwsException("was not able to add Debit memo");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "finance")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2017-06-12", testCaseId = "TC_FIN_Apply_Payments", testCaseDescription = "Verify Apply Payments For Debit Memo")
	public void applyPayements() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		Map<String, String> dataSet = fc.utobj().readTestData("finance", testCaseId);

		String agrmntVrsnName = fc.utobj().generateTestData(dataSet.get("agreementVersionName"));
		String royaltyPrcntg = dataSet.get("royaltyPercentage");
		String advPercentage = dataSet.get("advertisementPercentage");
		String royaltyAreaFranchiseValue = dataSet.get("RoyaltyAreaFranchise");
		String advAreaFranchiseValue = dataSet.get("AdvAreaFranchise");
		
		try {
			driver = fc.loginpage().login(driver);
			FinancePaymentsPage pobj = new FinancePaymentsPage(driver);

			fc.utobj().printTestStep("Navigate To Admin > Finance > Agreement Versions");
			AdminFinanceAgreementVersionsPageTest agreementforSales = new AdminFinanceAgreementVersionsPageTest();

			fc.utobj().printTestStep("Add Agreement");
			String agreementVersion = agreementforSales.addAgreement(driver, config, agrmntVrsnName, 
					 royaltyPrcntg, advPercentage, royaltyAreaFranchiseValue, advAreaFranchiseValue);

			fc.utobj().printTestStep("Navigate To Admin > Franchise Location > Add Franchise Location");
			fc.utobj().printTestStep("Add Franchise Location");

			String regionName = fc.utobj().generateTestData("GT");
			String storeType = fc.utobj().generateTestData("GT");
			String franchiseId = fc.utobj().generateTestData("GT");
			AdminFranchiseLocationAddFranchiseLocationPageTest franchise = new AdminFranchiseLocationAddFranchiseLocationPageTest();
			franchiseId = franchise.addFranchiseLocation_All_Agreement(driver, franchiseId, regionName, storeType,
					agreementVersion, config);

			fc.utobj().printTestStep("Navigate To Finance > Payments Page");
			fc.finance().finance_common().financePaymentsPage(driver);

			fc.utobj().printTestStep("Credits/Debit Tab");
			fc.utobj().clickElement(driver, pobj.creaditDebitTab);

			fc.utobj().printTestStep("Add Debit Memo");
			fc.utobj().clickElement(driver, pobj.addCreditsDebitsMemo);
			fc.utobj().selectDropDownByPartialText(driver, pobj.memoFranchiseID, franchiseId);
			fc.utobj().selectDropDown(driver, pobj.memoType, "Debit");

			double memoAmountD = 400.00;

			fc.utobj().sendKeys(driver, pobj.memoAmount, Double.toString(memoAmountD));
			fc.utobj().sendKeys(driver, pobj.memoComments, "Automation Memo");
			fc.utobj().clickElement(driver, pobj.saveMemeoBtn);

			fc.utobj().selectDropDownByValue(driver, pobj.resultsPerPage, "500");
			fc.utobj().printTestStep("Apply Payments");
			fc.utobj().actionImgOption(driver, franchiseId, "Apply Payment");
			fc.utobj().sendKeys(driver, pobj.recievedDate, fc.utobj().getCurrentDateUSFormat());
			fc.utobj().sendKeys(driver, pobj.paymentAmount, Double.toString(memoAmountD));
			fc.utobj().sendKeys(driver, pobj.refrenceNo, "12453");

			if (!fc.utobj().isSelected(driver, pobj.collectionMethodsCash)) {
				fc.utobj().clickElement(driver, pobj.collectionMethodsCash);
			}

			fc.utobj().sendKeys(driver, pobj.memoComments, "Test Payment Comments" + fc.utobj().generateRandomChar());
			fc.utobj().clickElement(driver, pobj.saveBtn);

			fc.utobj().selectDropDownByValue(driver, pobj.resultsPerPage, "500");

			boolean isTextPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//td[contains(text () , '" + franchiseId
					+ "')]/ancestor::tr/td[contains(text () , '" + memoAmountD + "')]");
			if (isTextPresent == false) {
				fc.utobj().throwsException("was not able to verify The Amount Applied");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "finance" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2017-06-14", testCaseId = "TC_FC_Apply_Payment_forAll_Payments_Tab_111", testCaseDescription = "Verify apply Payment for All Via Payments Tab")
	public void applyPaymentforAllViaPaymentsTab() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("finance", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		String agrmntVrsnName = fc.utobj().generateTestData(dataSet.get("agreementVersionName"));
		String royaltyPrcntg = dataSet.get("royaltyPercentage");
		String advPercentage = dataSet.get("advertisementPercentage");
		String royaltyAreaFranchiseValue = dataSet.get("RoyaltyAreaFranchise");
		String advAreaFranchiseValue = dataSet.get("AdvAreaFranchise");
		
		try {
			driver = fc.loginpage().login(driver);
			FinancePaymentsPage pobj = new FinancePaymentsPage(driver);
			FinanceSalesPage sales_Page = new FinanceSalesPage(driver);
			FinanceSalesPageTest sale_pageTest = new FinanceSalesPageTest();

			fc.utobj().printTestStep("Navigate To Admin > Finance > Agreement Versions");
			AdminFinanceAgreementVersionsPageTest agreementforSales = new AdminFinanceAgreementVersionsPageTest();

			fc.utobj().printTestStep("Add Agreement");
			String agreementVersion = agreementforSales.addAgreement(driver, config, agrmntVrsnName, 
					 royaltyPrcntg, advPercentage, royaltyAreaFranchiseValue, advAreaFranchiseValue);

			fc.utobj().printTestStep("Navigate To Admin > Franchise Location > Add Franchise Location");
			fc.utobj().printTestStep("Add Franchise Location");

			String regionName = fc.utobj().generateTestData("GT");
			String storeType = fc.utobj().generateTestData("GT");
			String franchiseId = fc.utobj().generateTestData("GT");
			AdminFranchiseLocationAddFranchiseLocationPageTest franchise = new AdminFranchiseLocationAddFranchiseLocationPageTest();
			franchiseId = franchise.addFranchiseLocation_All_Agreement(driver, franchiseId, regionName, storeType,
					agreementVersion, config);

			fc.utobj().printTestStep("Navigate To Admin > Finance > Finance Setup Preferences");
			
			fc.utobj().printTestStep("Setup Medium of submission of Sales Reports");
			
			String salesReportType = "webForm";
			
			new AdminFinanceFinanceSetupPreferencesPageTest().mediumOfSubmissionOfSalesReports(driver, config,
					salesReportType);

			fc.utobj().printTestStep("Set Invoice Frequency");
			
			new AdminFinanceFinanceSetupPreferencesPageTest().setInvoice(driver, config);

			fc.utobj().printTestStep("Navigate To Finance > Sales");
			
			fc.utobj().printTestStep("Enter Sales Report");
			
			String categoryQuantity = "110";
			
			String categoryAmount = "6000";
			
			String reportId = new FinanceSalesPageTest().enterSalesReport(driver, franchiseId, categoryQuantity,
					categoryAmount);

			fc.utobj().printTestStep("Filter Sales Report By Franchise Id");
			
			sale_pageTest.filterSalesReport(driver, franchiseId);

			fc.utobj().printTestStep("Approve Sales Report");
			
			fc.utobj().actionImgOption(driver, reportId, "Approve");

			fc.commonMethods().switch_cboxIframe_frameId(driver);
			
			fc.utobj().sendKeys(driver, sales_Page.commentTxtArea,
					"Test Approve Comments" + fc.utobj().generateRandomNumber());
			
			fc.utobj().clickElement(driver, sales_Page.SaveBtn);

			fc.utobj().clickElement(driver, sales_Page.cloaseBtn);
			
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Generate Invoice");
			
			fc.utobj().printTestStep("Filter Sales Report By Franchise Id");
			
			sale_pageTest.filterSalesReport(driver, franchiseId);
			
			fc.utobj().actionImgOption(driver, franchiseId, "Generate Invoice");

			fc.utobj().printTestStep("Navigate To Finance > Payments Page");
			
			fc.finance().finance_common().financePaymentsPage(driver);

			double advVal = Double.parseDouble(categoryAmount) * Double.parseDouble(advPercentage) / 100;
			
			double royaltyVal = Double.parseDouble(categoryAmount) * Double.parseDouble(royaltyPrcntg) / 100;

			double amountToBePaid = royaltyVal + advVal;
			
			FinanceRoyaltiesPage royalties_Page = new FinanceRoyaltiesPage(driver);
			
			fc.utobj().printTestStep("Navigate To Apply / Receive Payment Tab");
						
			fc.utobj().clickElement(driver, royalties_Page.applyReceivePaymentLnk);
			
			fc.utobj().selectDropDownByPartialText(driver, royalties_Page.franchiseIDforPayment, franchiseId);
			
			fc.utobj().clickElement(driver, royalties_Page.selectPaymentAppliedforAll);
			
			fc.utobj().clickElement(driver, royalties_Page.continueBtn);
			
			fc.utobj().sendKeys(driver, royalties_Page.paymentAmount, Double.toString(amountToBePaid));
			
			fc.utobj().clickElement(driver, royalties_Page.collectionMethodCash);
			
			fc.utobj().sendKeys(driver, royalties_Page.comments, "Automation Testing");
			
			fc.utobj().clickElement(driver, royalties_Page.saveBtn);

			fc.utobj().printTestStep("Veriy The All Amount Paid");
			
			fc.utobj().selectDropDownByValue(driver, pobj.resultsPerPage, "500");

			String amountApplied = fc.utobj().getText(driver, driver
					.findElement(By.xpath(".//td[contains(text () , '" + franchiseId + "')]/ancestor::tr/td[4]")));

			if (amountApplied.contains(",")) {
				amountApplied = amountApplied.replaceAll(",", "");
			}

			if (amountToBePaid != Double.parseDouble(amountApplied)) {
				
				fc.utobj().throwsException("was not able to verify The All Amount Paid");
			}
			
		//	String franchiseId= "GTa21013074", reportId = "000030", agreementVersion = "AgreementVersionz21012360";
			
			fc.utobj().printTestStep("Navigate to Finance > Store Summary");
			
			fc.finance().finance_common().financeStoreSummaryPage(driver);
			
			fc.utobj().clickElement(driver, driver.findElement(By.id("franchiseID")));
			
			fc.utobj().clickElement(driver, driver.findElement(By.xpath(".//*[contains(text(), '"+franchiseId+"')]")));
			
			fc.utobj().clickElement(driver, driver.findElement(By.id("getSummary")));
			
			fc.utobj().printTestStep("Verify Franchise ID at Summary Page");
			
			boolean isReportPresent = fc.utobj().assertLinkText(driver, franchiseId);
			
			if (isReportPresent== false) {
			
				fc.utobj().throwsException("Franchise ID is not visible at Store Summary Page");
			}
			
			fc.utobj().printTestStep("Verify Sales Report at Summary Page");
			boolean isReportPresent1 = fc.utobj().assertLinkText(driver, reportId);
			
			if (isReportPresent1== false) {
			
				fc.utobj().throwsException("Sales Report is not visible at Store Summary Page");
			}
			
			fc.utobj().printTestStep("Verify Agreement Version at Summary Page");
			boolean isReportPresent2 = fc.utobj().assertLinkText(driver, agreementVersion);
			
			if (isReportPresent2 == false) {
			
				fc.utobj().throwsException("Agreement Version is not visible at Store Summary Page");
			}
			
			
			// View Dashboard

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
			
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "finance" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "TC_FC_Apply_Payment_forRoyalty_Payments_Tab_112", testCaseDescription = "Verify apply Payment for Royalty Via Payments Tab")
	public void applyPaymentforRoyaltyViaPaymentsTab() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("finance", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		String agrmntVrsnName = fc.utobj().generateTestData(dataSet.get("agreementVersionName"));
		String royaltyPrcntg = dataSet.get("royaltyPercentage");
		String advPercentage = dataSet.get("advertisementPercentage");
		String royaltyAreaFranchiseValue = dataSet.get("RoyaltyAreaFranchise");
		String advAreaFranchiseValue = dataSet.get("AdvAreaFranchise");
		
		try {
			driver = fc.loginpage().login(driver);
			FinancePaymentsPage pobj = new FinancePaymentsPage(driver);

			FinanceSalesPage sales_Page = new FinanceSalesPage(driver);
			FinanceSalesPageTest sale_pageTest = new FinanceSalesPageTest();

			fc.utobj().printTestStep("Navigate To Admin > Finance > Agreement Versions");
			AdminFinanceAgreementVersionsPageTest agreementforSales = new AdminFinanceAgreementVersionsPageTest();

			fc.utobj().printTestStep("Add Agreement");
			String agreementVersion = agreementforSales.addAgreement(driver, config, agrmntVrsnName, 
					 royaltyPrcntg, advPercentage, royaltyAreaFranchiseValue, advAreaFranchiseValue);

			fc.utobj().printTestStep("Navigate To Admin > Franchise Location > Add Franchise Location");
			fc.utobj().printTestStep("Add Franchise Location");

			String regionName = fc.utobj().generateTestData("Test");
			String storeType = fc.utobj().generateTestData("Test");
			String franchiseId = fc.utobj().generateTestData("Test");
			AdminFranchiseLocationAddFranchiseLocationPageTest franchise = new AdminFranchiseLocationAddFranchiseLocationPageTest();
			franchiseId = franchise.addFranchiseLocation_All_Agreement(driver, franchiseId, regionName, storeType,
					agreementVersion, config);

			fc.utobj().printTestStep("Navigate To Admin > Finance > Finance Setup Preferences");
			fc.utobj().printTestStep("Setup Medium of submission of Sales Reports");
			String salesReportType = "webForm";
			new AdminFinanceFinanceSetupPreferencesPageTest().mediumOfSubmissionOfSalesReports(driver, config,
					salesReportType);

			fc.utobj().printTestStep("Set Invoice Frequency");
			new AdminFinanceFinanceSetupPreferencesPageTest().setInvoice(driver, config);

			fc.utobj().printTestStep("Navigate To Finance > Sales");
			fc.utobj().printTestStep("Enter Sales Report");
			String categoryQuantity = "110";
			String categoryAmount = "6000";
			String reportId = new FinanceSalesPageTest().enterSalesReport(driver, franchiseId, categoryQuantity,
					categoryAmount);

			fc.utobj().printTestStep("Filter Sales Report By Franchise Id");
			sale_pageTest.filterSalesReport(driver, franchiseId);

			fc.utobj().printTestStep("Approve Sales Report");
			fc.utobj().actionImgOption(driver, reportId, "Approve");

			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().sendKeys(driver, sales_Page.commentTxtArea,
					"Test Approve Comments" + fc.utobj().generateRandomNumber());
			fc.utobj().clickElement(driver, sales_Page.SaveBtn);

			fc.utobj().clickElement(driver, sales_Page.cloaseBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Generate Invoice");
			fc.utobj().printTestStep("Filter Sales Report By Franchise Id");
			sale_pageTest.filterSalesReport(driver, franchiseId);

			fc.utobj().printTestStep("Generate Invoice");
			fc.utobj().actionImgOption(driver, franchiseId, "Generate Invoice");

			fc.utobj().printTestStep("Navigate To Finance > Payments Page");
			fc.finance().finance_common().financePaymentsPage(driver);

			// double
			// advVal=Double.parseDouble(categoryAmount)*Double.parseDouble(advPercentage)/100;
			double royaltyVal = Double.parseDouble(categoryAmount) * Double.parseDouble(royaltyPrcntg) / 100;

			// double amountToBePaid=royaltyVal+advVal;

			fc.utobj().printTestStep("Navigate To Apply / Receive Payment Tab");
			FinanceRoyaltiesPage royalties_Page = new FinanceRoyaltiesPage(driver);
			fc.utobj().clickElement(driver, royalties_Page.applyReceivePaymentLnk);
			fc.utobj().selectDropDownByPartialText(driver, royalties_Page.franchiseIDforPayment, franchiseId);
			fc.utobj().clickElement(driver, royalties_Page.selectPaymentAppliedforRoyalty);
			fc.utobj().clickElement(driver, royalties_Page.continueBtn);
			fc.utobj().sendKeys(driver, royalties_Page.paymentAmount, Double.toString(royaltyVal));
			fc.utobj().clickElement(driver, royalties_Page.collectionMethodCash);
			fc.utobj().sendKeys(driver, royalties_Page.comments, "Automation Testing");
			fc.utobj().clickElement(driver, royalties_Page.saveBtn);

			fc.utobj().printTestStep("Veriy The Royality Amount Paid");

			fc.utobj().selectDropDownByValue(driver, pobj.resultsPerPage, "500");
			String amountApplied = fc.utobj().getText(driver, driver
					.findElement(By.xpath(".//td[contains(text () , '" + franchiseId + "')]/ancestor::tr/td[4]")));

			if (amountApplied.contains(",")) {
				amountApplied = amountApplied.replaceAll(",", "");
			}

			if (royaltyVal != Double.parseDouble(amountApplied)) {
				fc.utobj().throwsException("wasa not able to verify The Royalty Paid");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "finance" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2017-06-09", testCaseId = "TC_FC_Apply_Payment_forAdvertisement_Payments_Tab_113", testCaseDescription = "Verify apply Payment for Advertisement Via Payments Tab")
	public void applyPaymentforAdvertisementViaPaymentsTab() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("finance", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		String agrmntVrsnName = fc.utobj().generateTestData(dataSet.get("agreementVersionName"));
		String royaltyPrcntg = dataSet.get("royaltyPercentage");
		String advPercentage = dataSet.get("advertisementPercentage");
		String royaltyAreaFranchiseValue = dataSet.get("RoyaltyAreaFranchise");
		String advAreaFranchiseValue = dataSet.get("AdvAreaFranchise");
		
		try {
			driver = fc.loginpage().login(driver);

			FinancePaymentsPage pobj = new FinancePaymentsPage(driver);

			FinanceSalesPage sales_Page = new FinanceSalesPage(driver);
			FinanceSalesPageTest sale_pageTest = new FinanceSalesPageTest();

			fc.utobj().printTestStep("Navigate To Admin > Finance > Agreement Versions");
			AdminFinanceAgreementVersionsPageTest agreementforSales = new AdminFinanceAgreementVersionsPageTest();

			fc.utobj().printTestStep("Add Agreement");
			String agreementVersion = agreementforSales.addAgreement(driver, config, agrmntVrsnName, 
					 royaltyPrcntg, advPercentage, royaltyAreaFranchiseValue, advAreaFranchiseValue);

			fc.utobj().printTestStep("Navigate To Admin > Franchise Location > Add Franchise Location");
			fc.utobj().printTestStep("Add Franchise Location");

			String regionName = fc.utobj().generateTestData("GT");
			String storeType = fc.utobj().generateTestData("GT");
			String franchiseId = fc.utobj().generateTestData("GT");
			AdminFranchiseLocationAddFranchiseLocationPageTest franchise = new AdminFranchiseLocationAddFranchiseLocationPageTest();
			franchiseId = franchise.addFranchiseLocation_All_Agreement(driver, franchiseId, regionName, storeType,
					agreementVersion, config);

			fc.utobj().printTestStep("Navigate To Admin > Finance > Finance Setup Preferences");
			fc.utobj().printTestStep("Setup Medium of submission of Sales Reports");
			String salesReportType = "webForm";
			new AdminFinanceFinanceSetupPreferencesPageTest().mediumOfSubmissionOfSalesReports(driver, config,
					salesReportType);

			fc.utobj().printTestStep("Set Invoice Frequency");
			new AdminFinanceFinanceSetupPreferencesPageTest().setInvoice(driver, config);

			fc.utobj().printTestStep("Navigate To Finance > Sales");
			fc.utobj().printTestStep("Enter Sales Report");
			String categoryQuantity = "110";
			String categoryAmount = "6000";
			String reportId = new FinanceSalesPageTest().enterSalesReport(driver, franchiseId, categoryQuantity,
					categoryAmount);

			fc.utobj().printTestStep("Filter Sales Report By Franchise Id");
			sale_pageTest.filterSalesReport(driver, franchiseId);

			fc.utobj().printTestStep("Approve Sales Report");
			fc.utobj().actionImgOption(driver, reportId, "Approve");

			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().sendKeys(driver, sales_Page.commentTxtArea,
					"Test Approve Comments" + fc.utobj().generateRandomNumber());
			fc.utobj().clickElement(driver, sales_Page.SaveBtn);

			fc.utobj().clickElement(driver, sales_Page.cloaseBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Generate Invoice");
			fc.utobj().printTestStep("Filter Sales Report By Franchise Id");
			sale_pageTest.filterSalesReport(driver, franchiseId);

			fc.utobj().printTestStep("Generate Invoice");
			fc.utobj().actionImgOption(driver, franchiseId, "Generate Invoice");

			fc.utobj().printTestStep("Navigate To Finance > Payments Page");
			fc.finance().finance_common().financePaymentsPage(driver);

			double advVal = Double.parseDouble(categoryAmount) * Double.parseDouble(advPercentage) / 100;

			fc.utobj().printTestStep("Navigate To Apply / Receive Payment Tab");
			FinanceRoyaltiesPage royalties_Page = new FinanceRoyaltiesPage(driver);
			fc.utobj().clickElement(driver, royalties_Page.applyReceivePaymentLnk);
			fc.utobj().selectDropDownByPartialText(driver, royalties_Page.franchiseIDforPayment, franchiseId);
			fc.utobj().clickElement(driver, royalties_Page.selectPaymentAppliedforRoyalty);
			fc.utobj().clickElement(driver, royalties_Page.continueBtn);
			fc.utobj().sendKeys(driver, royalties_Page.paymentAmount, Double.toString(advVal));
			fc.utobj().clickElement(driver, royalties_Page.collectionMethodCash);
			fc.utobj().sendKeys(driver, royalties_Page.comments, "Automation Testing");
			fc.utobj().clickElement(driver, royalties_Page.saveBtn);

			fc.utobj().printTestStep("Veriy The Adv Amount Paid");

			fc.utobj().selectDropDownByValue(driver, pobj.resultsPerPage, "500");
			String amountApplied = fc.utobj().getText(driver, driver
					.findElement(By.xpath(".//td[contains(text () , '" + franchiseId + "')]/ancestor::tr/td[4]")));

			if (amountApplied.contains(",")) {
				amountApplied = amountApplied.replaceAll(",", "");
			}

			if (advVal != Double.parseDouble(amountApplied)) {
				fc.utobj().throwsException("wasa not able to verify The Adv Paid");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "finance", "finance_FailedTC" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2017-06-12", testCaseId = "TC_FC_Apply_Payment_forAdditional_Fees_Payments_Tab_114", testCaseDescription = "Verify apply Payment for Additional Fees Via Payments Tab")
	public void applyPaymentforAdditionalFeesViaPaymentsTab() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("finance", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		String agrmntVrsnName = fc.utobj().generateTestData(dataSet.get("agreementVersionName"));
		String royaltyPrcntg = dataSet.get("royaltyPercentage");
		String advPercentage = dataSet.get("advertisementPercentage");
		String royaltyAreaFranchiseValue = dataSet.get("RoyaltyAreaFranchise");
		String advAreaFranchiseValue = dataSet.get("AdvAreaFranchise");
		
		String itemValue= "00", itemType="Fixed";
		
		try {
			driver = fc.loginpage().login(driver);

			FinancePaymentsPage pobj = new FinancePaymentsPage(driver);

			FinanceSalesPage sales_Page = new FinanceSalesPage(driver);
			FinanceSalesPageTest sale_pageTest = new FinanceSalesPageTest();

			fc.utobj().printTestStep("Navigate To Admin > Finance > Additional > Invoice Item Summary Page");
			fc.utobj().printTestStep("addAdditionalInvoiceItems");
			String invoicItem = fc.utobj().generateTestData("InvoiceItem");
			new AdminFinanceAdditionalInvoiceItemSummaryPageTest().addAdditionalInvoiceItems(driver, invoicItem, itemValue, itemType);

			fc.utobj().printTestStep("Navigate To Admin > Finance > Agreement Versions");
			AdminFinanceAgreementVersionsPageTest agreementforSales = new AdminFinanceAgreementVersionsPageTest();

			fc.utobj().printTestStep("Add Agreement");
			String agreementVersion = agreementforSales.addAgreement(driver, config, agrmntVrsnName, 
					 royaltyPrcntg, advPercentage, royaltyAreaFranchiseValue, advAreaFranchiseValue);

			fc.utobj().printTestStep("Navigate To Admin > Franchise Location > Add Franchise Location");
			fc.utobj().printTestStep("Add Franchise Location");

			String regionName = fc.utobj().generateTestData("GT");
			String storeType = fc.utobj().generateTestData("GT");
			String franchiseId = fc.utobj().generateTestData("GT");
			AdminFranchiseLocationAddFranchiseLocationPageTest franchise = new AdminFranchiseLocationAddFranchiseLocationPageTest();
			franchiseId = franchise.addFranchiseLocation_All_Agreement(driver, franchiseId, regionName, storeType,
					agreementVersion, config);

			fc.utobj().printTestStep("Navigate To Admin > Finance > Finance Setup Preferences");
			fc.utobj().printTestStep("Setup Medium of submission of Sales Reports");
			String salesReportType = "webForm";
			new AdminFinanceFinanceSetupPreferencesPageTest().mediumOfSubmissionOfSalesReports(driver, config,
					salesReportType);

			fc.utobj().printTestStep("Set Invoice Frequency");
			new AdminFinanceFinanceSetupPreferencesPageTest().setInvoice(driver, config);

			fc.utobj().printTestStep("Navigate To Finance > Sales");
			fc.utobj().printTestStep("Enter Sales Report");
			String categoryQuantity = "110";
			String categoryAmount = "6000";
			String additionalInvoiceValue = "400";
			String reportId = new FinanceSalesPageTest().enterSalesReportWithAdditionInvoice(driver, franchiseId,
					categoryQuantity, categoryAmount, invoicItem, additionalInvoiceValue);

			fc.utobj().printTestStep("Filter Sales Report By Franchise Id");
			sale_pageTest.filterSalesReport(driver, franchiseId);

			fc.utobj().printTestStep("Approve Sales Report");
			fc.utobj().actionImgOption(driver, reportId, "Approve");

			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().sendKeys(driver, sales_Page.commentTxtArea,
					"Test Approve Comments" + fc.utobj().generateRandomNumber());
			fc.utobj().clickElement(driver, sales_Page.SaveBtn);

			fc.utobj().clickElement(driver, sales_Page.cloaseBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Generate Invoice");
			fc.utobj().printTestStep("Filter Sales Report By Franchise Id");
			sale_pageTest.filterSalesReport(driver, franchiseId);

			fc.utobj().printTestStep("Generate Invoice");
			fc.utobj().actionImgOption(driver, franchiseId, "Generate Invoice");

			fc.utobj().printTestStep("Navigate To Finance > Payments Page");
			fc.finance().finance_common().financePaymentsPage(driver);

			// double
			// advVal=Double.parseDouble(categoryAmount)*Double.parseDouble(advPercentage)/100;
			double addFeeVal = Double.parseDouble(additionalInvoiceValue);

			fc.utobj().printTestStep("Navigate To Apply / Receive Payment Tab");
			FinanceRoyaltiesPage royalties_Page = new FinanceRoyaltiesPage(driver);
			fc.utobj().clickElement(driver, royalties_Page.applyReceivePaymentLnk);
			fc.utobj().selectDropDownByPartialText(driver, royalties_Page.franchiseIDforPayment, franchiseId);
			fc.utobj().clickElement(driver, royalties_Page.selectPaymentAppliedforAdditionalfees);
			fc.utobj().clickElement(driver, royalties_Page.continueBtn);
			fc.utobj().sendKeys(driver, royalties_Page.paymentAmount, additionalInvoiceValue);
			fc.utobj().clickElement(driver, royalties_Page.collectionMethodCash);
			fc.utobj().sendKeys(driver, royalties_Page.comments, "Automation Testing");
			fc.utobj().clickElement(driver, royalties_Page.saveBtn);

			fc.utobj().printTestStep("Veriy The Addition Invoice Amount Paid");

			fc.utobj().selectDropDownByValue(driver, pobj.resultsPerPage, "500");
			String amountApplied = fc.utobj().getText(driver, driver
					.findElement(By.xpath(".//td[contains(text () , '" + franchiseId + "')]/ancestor::tr/td[4]")));

			if (amountApplied.contains(",")) {
				amountApplied = amountApplied.replaceAll(",", "");
			}

			if (addFeeVal != Double.parseDouble(amountApplied)) {
				fc.utobj().throwsException("wasa not able to verify The Additional Amount Paid");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "finance", "finance_FailedTC" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2017-06-01", testCaseId = "TC_FC_Filters_Payments_Report_171", testCaseDescription = "Verify filters at Payments Page")
	public void filtersatPaymentsPage() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());
		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		Map<String, String> dataSet = fc.utobj().readTestData("finance", testCaseId);

		String agrmntVrsnName = fc.utobj().generateTestData(dataSet.get("agreementVersionName"));
		String royaltyPrcntg = dataSet.get("royaltyPercentage");
		String advPercentage = dataSet.get("advertisementPercentage");
		String royaltyAreaFranchiseValue = dataSet.get("RoyaltyAreaFranchise");
		String advAreaFranchiseValue = dataSet.get("AdvAreaFranchise");
		
		String itemValue= "00", itemType="Variable";
		
		try {
			driver = fc.loginpage().login(driver);
			FinanceSalesPage sales_Page = new FinanceSalesPage(driver);
			FinanceSalesPageTest sale_pageTest = new FinanceSalesPageTest();

			fc.utobj().printTestStep("Navigate To Admin > Finance > Additional > Invoice Item Summary Page");
			fc.utobj().printTestStep("Add Additional Invoice Items");
			String invoicItem = fc.utobj().generateTestData("InvoiceItem");
			new AdminFinanceAdditionalInvoiceItemSummaryPageTest().addAdditionalInvoiceItems(driver, invoicItem, itemValue, itemType);

			fc.utobj().printTestStep("Navigate To Admin > Finance > Agreement Versions");
			AdminFinanceAgreementVersionsPageTest agreementforSales = new AdminFinanceAgreementVersionsPageTest();

			fc.utobj().printTestStep("Add Agreement");
			String agreementVersion = agreementforSales.addAgreement(driver, config, agrmntVrsnName, 
					 royaltyPrcntg, advPercentage, royaltyAreaFranchiseValue, advAreaFranchiseValue);

			fc.utobj().printTestStep("Navigate To Admin > Franchise Location > Add Franchise Location");
			fc.utobj().printTestStep("Add Franchise Location");

			String regionName = fc.utobj().generateTestData("GT");
			String storeType = fc.utobj().generateTestData("GT");
			String franchiseId = fc.utobj().generateTestData("GT");
			AdminFranchiseLocationAddFranchiseLocationPageTest franchise = new AdminFranchiseLocationAddFranchiseLocationPageTest();
			franchiseId = franchise.addFranchiseLocation_All_Agreement(driver, franchiseId, regionName, storeType,
					agreementVersion, config);

			fc.utobj().printTestStep("Navigate To Admin > Finance > Finance Setup Preferences");
			fc.utobj().printTestStep("Setup Medium of submission of Sales Reports");
			String salesReportType = "webForm";
			new AdminFinanceFinanceSetupPreferencesPageTest().mediumOfSubmissionOfSalesReports(driver, config,
					salesReportType);

			fc.utobj().printTestStep("Set Invoice Frequency");
			new AdminFinanceFinanceSetupPreferencesPageTest().setInvoice(driver, config);

			fc.utobj().printTestStep("Navigate To Finance > Sales");
			fc.utobj().printTestStep("Enter Sales Report");
			String categoryQuantity = "110";
			String categoryAmount = "6000";
			String additionalInvoiceValue = "400";
			String reportId = new FinanceSalesPageTest().enterSalesReportWithAdditionInvoice(driver, franchiseId,
					categoryQuantity, categoryAmount, invoicItem, additionalInvoiceValue);

			fc.utobj().printTestStep("Filter Sales Report By Franchise Id");
			sale_pageTest.filterSalesReport(driver, franchiseId);

			fc.utobj().printTestStep("Approve Sales Report");
			fc.utobj().actionImgOption(driver, reportId, "Approve");

			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().sendKeys(driver, sales_Page.commentTxtArea,
					"Test Approve Comments" + fc.utobj().generateRandomNumber());
			fc.utobj().clickElement(driver, sales_Page.SaveBtn);

			fc.utobj().clickElement(driver, sales_Page.cloaseBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Generate Invoice");
			fc.utobj().printTestStep("Filter Sales Report By Franchise Id");
			sale_pageTest.filterSalesReport(driver, franchiseId);

			fc.utobj().printTestStep("Generate Invoice");
			fc.utobj().actionImgOption(driver, franchiseId, "Generate Invoice");

			fc.utobj().printTestStep("Navigate To Finance > Royalties Page");
			fc.finance().finance_common().financeRoyaltiesPage(driver);

			fc.utobj().printTestStep("Filter Sales Report By Franchise Id");
			sale_pageTest.filterSalesReport(driver, franchiseId);

			String invoiceID = fc.utobj().getText(driver, driver
					.findElement(By.xpath(".//a[contains(text () , '" + franchiseId + "')]/ancestor::tr/td[2]/a[1]")));

			fc.utobj().printTestStep("Navigate To Finance > Payments Page");
			fc.finance().finance_common().financePaymentsPage(driver);

			fc.utobj().printTestStep("Navigate To Apply / Receive Payment Tab");
			FinanceRoyaltiesPage royalties_Page = new FinanceRoyaltiesPage(driver);
			fc.utobj().clickElement(driver, royalties_Page.applyReceivePaymentLnk);
			fc.utobj().selectDropDownByPartialText(driver, royalties_Page.franchiseIDforPayment, franchiseId);
			fc.utobj().clickElement(driver, royalties_Page.selectPaymentAppliedforAdditionalfees);
			fc.utobj().clickElement(driver, royalties_Page.continueBtn);
			fc.utobj().sendKeys(driver, royalties_Page.paymentAmount, additionalInvoiceValue);
			fc.utobj().clickElement(driver, royalties_Page.collectionMethodCash);
			fc.utobj().sendKeys(driver, royalties_Page.comments, "Automation Testing");
			fc.utobj().clickElement(driver, royalties_Page.saveBtn);

			fc.utobj().printTestStep("Filter Payment Summary");
			
			fc.utobj().clickElement(driver, royalties_Page.paymentSummaryTab);

			fc.utobj().clickElement(driver, royalties_Page.showFilterLnk);
			
			fc.utobj().clickElement(driver, royalties_Page.franchiseIDFilteratPaymentSummary);
			fc.utobj().clickElement(driver,
					driver.findElement(By.xpath(".//div[@id='ms-parentfranchiseNo']//input[@id='selectAll']")));
			fc.utobj().sendKeys(driver,
					driver.findElement(
							By.xpath(".//div[@id='ms-parentfranchiseNo']//input[@class='searchInputMultiple']")),
					franchiseId);
			fc.utobj().clickElement(driver,
					driver.findElement(By.xpath(".//div[@id='ms-parentfranchiseNo']//input[@id='selectAll']")));
			fc.utobj().clickElement(driver, royalties_Page.franchiseIDFilteratPaymentSummary);

			fc.utobj().sendKeys(driver, royalties_Page.dateAppliedFrom, fc.utobj().getCurrentDateUSFormat());
			fc.utobj().sendKeys(driver, royalties_Page.dateAppliedTo, fc.utobj().getCurrentDateUSFormat());
			fc.utobj().sendKeys(driver, royalties_Page.invoiceNo, invoiceID);
			fc.utobj().clickElement(driver, royalties_Page.searchFilterBtn);

			int rowCount = driver
					.findElements(By.xpath(".//div[@id='printReady']//table[@class='summaryTblex']/tbody/tr")).size();
			if (rowCount > 2) {
				fc.utobj().throwsException("Filters at Payment Summary page are not working as expected");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "finance" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2017-06-02", testCaseId = "TC_FC_Filters_Payments_Credit_Debit_Report_177", testCaseDescription = "Verify filters at Payments Credit Debit Page")
	public void filtersatPaymentsCreditDebitPage() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		Map<String, String> dataSet = fc.utobj().readTestData("finance", testCaseId);

		String agrmntVrsnName = fc.utobj().generateTestData(dataSet.get("agreementVersionName"));
		String royaltyPrcntg = dataSet.get("royaltyPercentage");
		String advPercentage = dataSet.get("advertisementPercentage");
		String royaltyAreaFranchiseValue = dataSet.get("RoyaltyAreaFranchise");
		String advAreaFranchiseValue = dataSet.get("AdvAreaFranchise");
		String itemValue= "00", itemType="Variable";
		
		try {
			driver = fc.loginpage().login(driver);
			FinanceSalesPage sales_Page = new FinanceSalesPage(driver);
			FinanceSalesPageTest sale_pageTest = new FinanceSalesPageTest();

			fc.utobj().printTestStep("Navigate To Admin > Finance > Additional > Invoice Item Summary Page");
			fc.utobj().printTestStep("addAdditionalInvoiceItems");
			String invoicItem = fc.utobj().generateTestData("InvoiceItem");
			new AdminFinanceAdditionalInvoiceItemSummaryPageTest().addAdditionalInvoiceItems(driver, invoicItem, itemValue, itemType);

			fc.utobj().printTestStep("Navigate To Admin > Finance > Agreement Versions");
			AdminFinanceAgreementVersionsPageTest agreementforSales = new AdminFinanceAgreementVersionsPageTest();

			fc.utobj().printTestStep("Add Agreement");
			String agreementVersion = agreementforSales.addAgreement(driver, config, agrmntVrsnName, 
					 royaltyPrcntg, advPercentage, royaltyAreaFranchiseValue, advAreaFranchiseValue);

			fc.utobj().printTestStep("Navigate To Admin > Franchise Location > Add Franchise Location");
			fc.utobj().printTestStep("Add Franchise Location");

			String regionName = fc.utobj().generateTestData("GT");
			String storeType = fc.utobj().generateTestData("GT");
			String franchiseId = fc.utobj().generateTestData("GT");
			AdminFranchiseLocationAddFranchiseLocationPageTest franchise = new AdminFranchiseLocationAddFranchiseLocationPageTest();
			franchiseId = franchise.addFranchiseLocation_All_Agreement(driver, franchiseId, regionName, storeType,
					agreementVersion, config);

			fc.utobj().printTestStep("Navigate To Admin > Finance > Finance Setup Preferences");
			fc.utobj().printTestStep("Setup Medium of submission of Sales Reports");
			String salesReportType = "webForm";
			new AdminFinanceFinanceSetupPreferencesPageTest().mediumOfSubmissionOfSalesReports(driver, config,
					salesReportType);

			fc.utobj().printTestStep("Set Invoice Frequency");
			new AdminFinanceFinanceSetupPreferencesPageTest().setInvoice(driver, config);

			fc.utobj().printTestStep("Navigate To Finance > Sales");
			fc.utobj().printTestStep("Enter Sales Report");
			String categoryQuantity = "110";
			String categoryAmount = "6000";
			String additionalInvoiceValue = "400";
			String reportId = new FinanceSalesPageTest().enterSalesReportWithAdditionInvoice(driver, franchiseId,
					categoryQuantity, categoryAmount, invoicItem, additionalInvoiceValue);

			fc.utobj().printTestStep("Filter Sales Report By Franchise Id");
			sale_pageTest.filterSalesReport(driver, franchiseId);

			fc.utobj().printTestStep("Approve Sales Report");
			fc.utobj().actionImgOption(driver, reportId, "Approve");

			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().sendKeys(driver, sales_Page.commentTxtArea,
					"Test Approve Comments" + fc.utobj().generateRandomNumber());
			fc.utobj().clickElement(driver, sales_Page.SaveBtn);

			fc.utobj().clickElement(driver, sales_Page.cloaseBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Generate Invoice");
			fc.utobj().printTestStep("Filter Sales Report By Franchise Id");
			sale_pageTest.filterSalesReport(driver, franchiseId);

			fc.utobj().printTestStep("Generate Invoice");
			fc.utobj().actionImgOption(driver, franchiseId, "Generate Invoice");

			fc.utobj().printTestStep("Navigate To Finance > Payments Page");
			fc.finance().finance_common().financePaymentsPage(driver);

			fc.utobj().printTestStep("Navigate To Apply / Receive Payment Tab");
			FinanceRoyaltiesPage royalties_Page = new FinanceRoyaltiesPage(driver);
			fc.utobj().clickElement(driver, royalties_Page.applyReceivePaymentLnk);
			fc.utobj().selectDropDownByPartialText(driver, royalties_Page.franchiseIDforPayment, franchiseId);
			fc.utobj().clickElement(driver, royalties_Page.selectPaymentAppliedforAdditionalfees);
			fc.utobj().clickElement(driver, royalties_Page.continueBtn);
			fc.utobj().sendKeys(driver, royalties_Page.paymentAmount, additionalInvoiceValue);
			fc.utobj().clickElement(driver, royalties_Page.collectionMethodCash);
			fc.utobj().sendKeys(driver, royalties_Page.comments, "Automation Testing");
			fc.utobj().clickElement(driver, royalties_Page.saveBtn);

			fc.utobj().printTestStep("Verify Filter At Credit Debit Card Page");

			fc.utobj().clickElement(driver, royalties_Page.creditsDebitsLnk);
			fc.utobj().clickElement(driver, royalties_Page.showFilterLnk);
			fc.utobj().clickElement(driver, royalties_Page.clickFranchiseIDfilter);
			fc.utobj().clickElement(driver,
					driver.findElement(By.xpath(".//div[@id='ms-parentfranchiseID']//input[@id='selectAll']")));
			fc.utobj().sendKeys(driver,
					driver.findElement(
							By.xpath(".//div[@id='ms-parentfranchiseID']//input[@class='searchInputMultiple']")),
					franchiseId);
			fc.utobj().clickElement(driver,
					driver.findElement(By.xpath(".//div[@id='ms-parentfranchiseID']//input[@id='selectAll']")));
			fc.utobj().clickElement(driver, royalties_Page.clickFranchiseIDfilter);

			fc.utobj().sendKeys(driver, royalties_Page.memoDateFrom, fc.utobj().getCurrentDateUSFormat());
			fc.utobj().sendKeys(driver, royalties_Page.memoDateTo, fc.utobj().getCurrentDateUSFormat());

			fc.utobj().clickElement(driver, royalties_Page.creditMemoCheck);
			fc.utobj().clickElement(driver, royalties_Page.debitMemoCheck);
			fc.utobj().clickElement(driver, royalties_Page.paymentMemoCheck);
			fc.utobj().clickElement(driver, royalties_Page.searchFilterBtn);

			int rowCount = driver
					.findElements(By.xpath(".//div[@id='printReady']//table[@class='summaryTblex']/tbody/tr")).size();
			if (rowCount > 2) {
				fc.utobj().throwsException("Filters at Payments Credit / Debit page are not working as expected");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "finance" })
	@TestCase(createdOn = "2018-06-07", updatedOn = "2018-06-07", testCaseId = "TC_FC_Apply_Payment_forAdjustment", testCaseDescription = "Verify apply Payment for Adjustment Via Payments Tab")
	public void applyPaymentforAdjustmentViaPaymentsTab() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		String agrmntVrsnName = fc.utobj().generateTestData("AV");
		String royaltyPrcntg = "10";
		String advPercentage = "8";
		String royaltyAreaFranchiseValue = "5";
		String advAreaFranchiseValue = "3";
		
		String itemValue= "00", itemType="Variable";
		
		try {
			driver = fc.loginpage().login(driver);

			FinancePaymentsPage pobj = new FinancePaymentsPage(driver);
			FinanceSalesPage sales_Page = new FinanceSalesPage(driver);
			FinanceSalesPageTest sale_pageTest = new FinanceSalesPageTest();
			
			fc.utobj().printTestStep("Navigate To Admin > Finance > Additional > Invoice Item Summary Page");
			fc.utobj().printTestStep("addAdditionalInvoiceItems");
			String invoicItem = fc.utobj().generateTestData("InvoiceItem");
			new AdminFinanceAdditionalInvoiceItemSummaryPageTest().addAdditionalInvoiceItems(driver, invoicItem, itemValue, itemType);

			fc.utobj().printTestStep("Navigate To Admin > Finance > Agreement Versions");
			AdminFinanceAgreementVersionsPageTest agreementforSales = new AdminFinanceAgreementVersionsPageTest();

			fc.utobj().printTestStep("Add Agreement");
			String agreementVersion = agreementforSales.addAgreement(driver, config, agrmntVrsnName, 
					 royaltyPrcntg, advPercentage, royaltyAreaFranchiseValue, advAreaFranchiseValue);

			fc.utobj().printTestStep("Navigate To Admin > Franchise Location > Add Franchise Location");
			fc.utobj().printTestStep("Add Franchise Location");
		
			String franchiseId = "GT";
			AdminFranchiseLocationAddNewFranchiseLocationPageTest franchise = new AdminFranchiseLocationAddNewFranchiseLocationPageTest();
			franchiseId = franchise.addFranchiseLocation(driver, franchiseId, agreementVersion, config);

			fc.utobj().printTestStep("Navigate To Admin > Finance > Finance Setup Preferences");
			fc.utobj().printTestStep("Setup Medium of submission of Sales Reports");
			String salesReportType = "webForm";
			new AdminFinanceFinanceSetupPreferencesPageTest().mediumOfSubmissionOfSalesReports(driver, config,
					salesReportType);

			fc.utobj().printTestStep("Set Invoice Frequency");
			new AdminFinanceFinanceSetupPreferencesPageTest().setInvoice(driver, config);

			fc.utobj().printTestStep("Navigate To Finance > Sales");
			fc.utobj().printTestStep("Enter Sales Report");
			String categoryQuantity = "110";
			String categoryAmount = "6000";
			String additionalInvoiceValue = "400";
			String reportId = new FinanceSalesPageTest().enterSalesReportWithAdditionInvoice(driver, franchiseId,
					categoryQuantity, categoryAmount, invoicItem, additionalInvoiceValue);

			fc.utobj().printTestStep("Filter Sales Report By Franchise Id");
			sale_pageTest.filterSalesReport(driver, franchiseId);

			fc.utobj().printTestStep("Approve Sales Report");
			fc.utobj().actionImgOption(driver, reportId, "Approve");

			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().sendKeys(driver, sales_Page.commentTxtArea,
					"Test Approve Comments" + fc.utobj().generateRandomNumber());
			fc.utobj().clickElement(driver, sales_Page.SaveBtn);

			fc.utobj().clickElement(driver, sales_Page.cloaseBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Generate Invoice");
			fc.utobj().printTestStep("Filter Sales Report By Franchise Id");
			sale_pageTest.filterSalesReport(driver, franchiseId);

			fc.utobj().printTestStep("Generate Invoice");
			fc.utobj().actionImgOption(driver, franchiseId, "Generate Invoice");
			
			fc.utobj().printTestStep("Enter Adjustment");
			fc.utobj().actionImgOption(driver, franchiseId, "Enter Adjustment");

			categoryQuantity = "200";
			categoryAmount = "400";
			fc.utobj().sendKeys(driver,	driver.findElement(By.xpath(".//td[contains(text () , 'Service')]/following-sibling::td//input[contains(@name , 'quantity')]")), categoryQuantity);
			
			fc.utobj().sendKeys(driver,	driver.findElement(By.xpath(".//td[contains(text () , 'Service')]/following-sibling::td//input[contains(@name , 'amount')]")), categoryAmount);

			fc.utobj().sendKeys(driver, sales_Page.rcvdDate, fc.utobj().getFutureDateUSFormat(-1));

			fc.utobj().clickElement(driver, sales_Page.SaveBtn);

			if (!fc.utobj().isSelected(driver, sales_Page.acknowledgement)) {
				
				fc.utobj().clickElement(driver, sales_Page.acknowledgement);
			}
			fc.utobj().clickElement(driver, sales_Page.SaveBtn);

			String reportIdAdj = fc.utobj().getText(driver, sales_Page.reportId);
			
			fc.utobj().clickElement(driver, sales_Page.okayBtn);
		
			fc.finance().finance_common().financeSalesPage(driver);
			
			sale_pageTest.filterSalesReport(driver, franchiseId);
			
			fc.utobj().printTestStep("Approve Adjustment Sales Report");
			
			fc.utobj().actionImgOption(driver, reportIdAdj, "Approve");

			fc.commonMethods().switch_cboxIframe_frameId(driver);
			
			fc.utobj().sendKeys(driver, sales_Page.commentTxtArea, "Test Approve Comments" + fc.utobj().generateRandomNumber());
			
			fc.utobj().clickElement(driver, sales_Page.SaveBtn);

			fc.utobj().clickElement(driver, sales_Page.cloaseBtn);
			
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Filter Sales Report By Franchise Id");
			
			sale_pageTest.filterSalesReport(driver, franchiseId);
			
			fc.utobj().printTestStep("Generate Invoice for Adjustment");
			
			fc.utobj().actionImgOption(driver, reportIdAdj, "Generate Invoice");
			
			fc.finance().finance_common().financeRoyaltiesPage(driver);

			fc.utobj().printTestStep("Filter Report By Franchise Id");
			
			sale_pageTest.filterSalesReport(driver, franchiseId);
			
			boolean adjustmmentOnRoyalty = fc.utobj().assertLinkPartialText(driver, reportIdAdj);
			
			if(adjustmmentOnRoyalty == false) {
				
				fc.utobj().throwsException("Royalty for adjustment is not generated");
			}
			
			fc.utobj().printTestStep("Navigate To Finance > Payments Page");
			
			fc.finance().finance_common().financePaymentsPage(driver);

			double advVal1 = Double.parseDouble(categoryAmount) * Double.parseDouble(advPercentage) / 100;
			
			double royaltyVal1 = Double.parseDouble(categoryAmount) * Double.parseDouble(royaltyPrcntg) / 100;

			double amountToBePaid = royaltyVal1 + advVal1;

			fc.utobj().printTestStep("Navigate To Apply / Receive Payment Tab");
			FinanceRoyaltiesPage royalties_Page = new FinanceRoyaltiesPage(driver);
			
			fc.utobj().clickElement(driver, royalties_Page.applyReceivePaymentLnk);
			
			fc.utobj().selectDropDownByPartialText(driver, royalties_Page.franchiseIDforPayment, franchiseId);
			
			fc.utobj().clickElement(driver, royalties_Page.selectPaymentAppliedforAll);
			
			fc.utobj().clickElement(driver, royalties_Page.continueBtn);
			
			fc.utobj().sendKeys(driver, royalties_Page.paymentAmount, Double.toString(amountToBePaid));
			
			fc.utobj().clickElement(driver, royalties_Page.collectionMethodCash);
			
			fc.utobj().sendKeys(driver, royalties_Page.comments, "Automation Testing");
			
			fc.utobj().clickElement(driver, royalties_Page.saveBtn);
			
			fc.utobj().printTestStep("Veriy The All Amount Paid");
			fc.utobj().selectDropDownByValue(driver, pobj.resultsPerPage, "500");

			String amountApplied = fc.utobj().getText(driver, driver.findElement(By.xpath(".//td[contains(text () , '" + franchiseId + "')]/ancestor::tr/td[4]")));

			if (amountApplied.contains(",")) {
				amountApplied = amountApplied.replaceAll(",", "");
			}

			if (amountToBePaid != Double.parseDouble(amountApplied)) {
				fc.utobj().throwsException("wasa not able to verify The All Amount Paid");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "finance" })
	@TestCase(createdOn = "2018-06-08", updatedOn = "2018-06-11", testCaseId = "TC_FC_Invoice_Amount_at_Invoice_Details", testCaseDescription = "Verify Invoice Amount at Invoice Details Page from payment tab")
	public void verifyInvoiceAmountatInvoiceDetailsPage() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());
		
		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		Map<String, String> dataSet = fc.utobj().readTestData("finance", testCaseId);
		
		String agrmntVrsnName = fc.utobj().generateTestData("GT");
		String royaltyPrcntg = "10";
		String advPercentage = "7";
		String royaltyAreaFranchiseValue = "5";
		String advAreaFranchiseValue = "3";
		
		try {
			driver = fc.loginpage().login(driver);
			FinanceSalesPage pobj = new FinanceSalesPage(driver);
			FinanceSalesPageTest sales_PageTest = new FinanceSalesPageTest();
			
			fc.utobj().printTestStep("Navigate To Admin > Finance > Agreement Versions");
			AdminFinanceAgreementVersionsPageTest agreementforSales = new AdminFinanceAgreementVersionsPageTest();

			fc.utobj().printTestStep("Add Agreement");
			String agreementVersion = agreementforSales.addAgreement(driver, config, agrmntVrsnName, 
					 royaltyPrcntg, advPercentage, royaltyAreaFranchiseValue, advAreaFranchiseValue);

			fc.utobj().printTestStep("Navigate To Admin > Franchise Location > Add Franchise Location");
			fc.utobj().printTestStep("Add Franchise Location");

			String regionName = fc.utobj().generateTestData("GT");
			String storeType = fc.utobj().generateTestData("GT");
			String franchiseId = fc.utobj().generateTestData("GT");
			AdminFranchiseLocationAddFranchiseLocationPageTest franchise = new AdminFranchiseLocationAddFranchiseLocationPageTest();
			franchiseId = franchise.addFranchiseLocation_All_Agreement(driver, franchiseId, regionName, storeType,
					agreementVersion, config);

			fc.utobj().printTestStep("Navigate To Admin > Finance > Finance Setup Preferences");
			fc.utobj().printTestStep("Setup Medium of submission of Sales Reports");
			String salesReportType = "webForm";
			new AdminFinanceFinanceSetupPreferencesPageTest().mediumOfSubmissionOfSalesReports(driver, config,
					salesReportType);

			fc.utobj().printTestStep("Set Invoice Frequency");
			new AdminFinanceFinanceSetupPreferencesPageTest().setInvoice(driver, config);

			fc.utobj().printTestStep("Navigate To Finance > Sales");
			fc.utobj().printTestStep("Enter Sales Report");
			String categoryQuantity = "110";
			String categoryAmount = "6000";
			String reportId = sales_PageTest.enterSalesReport(driver, franchiseId, categoryQuantity, categoryAmount);

			fc.utobj().printTestStep("Filter Sales Report By Franchise Id");
			sales_PageTest.filterSalesReport(driver, franchiseId);

			fc.utobj().printTestStep("Approve Sales Report");
			fc.utobj().actionImgOption(driver, reportId, "Approve");

			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().sendKeys(driver, pobj.commentTxtArea,
					"Test Approve Comments" + fc.utobj().generateRandomNumber());
			fc.utobj().clickElement(driver, pobj.SaveBtn);

			fc.utobj().clickElement(driver, pobj.cloaseBtn);
			
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Filter Sales Report By Franchise Id");
			
			sales_PageTest.filterSalesReport(driver, franchiseId);

			fc.utobj().printTestStep("Generate Invoice");
			
			fc.utobj().actionImgOption(driver, franchiseId, "Generate Invoice");
			
			fc.utobj().printTestStep("Navigate To Apply / Receive Payment Tab");
			
			fc.finance().finance_common().financePaymentsPage(driver);
			
			fc.utobj().printTestStep("Filter Sales Report By Franchise Id");
			
			double advVal1 = Double.parseDouble(categoryAmount) * Double.parseDouble(advPercentage) / 100;
			
			double royaltyVal1 = Double.parseDouble(categoryAmount) * Double.parseDouble(royaltyPrcntg) / 100;
			
			double amountToBePaid = royaltyVal1 + advVal1;
			
			FinanceRoyaltiesPage royalties_Page = new FinanceRoyaltiesPage(driver);
			
			fc.utobj().clickElement(driver, royalties_Page.applyReceivePaymentLnk);
			
			fc.utobj().selectDropDownByPartialText(driver, royalties_Page.franchiseIDforPayment, franchiseId);
			
			fc.utobj().clickElement(driver, royalties_Page.selectPaymentAppliedforAll);
			
			fc.utobj().clickElement(driver, royalties_Page.continueBtn);
			
			fc.utobj().sendKeys(driver, royalties_Page.paymentAmount, Double.toString(amountToBePaid));
			
			fc.utobj().clickElement(driver, royalties_Page.collectionMethodCash);
			
			fc.utobj().sendKeys(driver, royalties_Page.comments, "Automation Testing");
			
			fc.utobj().clickElement(driver, royalties_Page.saveBtn);
					
			//String franchiseId ="GTu11124063",categoryAmount="6000";
			
			fc.finance().finance_common().financePaymentsPage(driver);
			
		//	filterPaymentTab(driver, franchiseId);
			
			fc.utobj().clickElement(driver, driver.findElement(By.xpath(".//td[contains(text () , '"+franchiseId+"')]/ancestor::tr/td[2]/a")));

			String totalInvoiceAmount = fc.utobj().getText(driver, driver.findElement(By.xpath(".//*[contains(text () , 'Invoice Report Details')]/ancestor::tr[2]//td[contains(text () , 'Invoice Amount')]/following-sibling::td")));
			
			if (totalInvoiceAmount.contains(",")) {
				totalInvoiceAmount = totalInvoiceAmount.replaceAll(",", "");
			}

			double invoiceAmount = Double.parseDouble(totalInvoiceAmount);
			
			double calServiceAmount = Double.parseDouble(categoryAmount);
			
			DecimalFormat df = new DecimalFormat("#.##");
			
			calServiceAmount = Double.valueOf(df.format(calServiceAmount));
			
			double advPercent = Double.parseDouble(advPercentage);
			
			double calAdv = ((calServiceAmount * advPercent) / 100.0);
			
			calAdv = Double.valueOf(df.format(calAdv));
			
			double royaltyPercent = Double.parseDouble(royaltyPrcntg);
			
			double calRoyalty = ((calServiceAmount * royaltyPercent) / 100.0);
			
			calRoyalty = Double.valueOf(df.format(calRoyalty));

			String additionalFees = fc.utobj().getText(driver, driver.findElement(By.xpath(
					".//*[contains(text () , 'Invoice Report Details')]/ancestor::tr[2]//td[contains(text () , 'Additional Fees')]/following-sibling::td")));

			if (additionalFees.contains(",")) {
				additionalFees = additionalFees.replaceAll(",", "");
			}

			double AdditionalFees = Double.parseDouble(additionalFees);
			
			double calInvoiceAmount = (calRoyalty + calAdv + AdditionalFees);

			if (calInvoiceAmount != invoiceAmount) {
				fc.utobj().throwsException(
						"Total Invoice Amount at Invoice Details Page is not Same as Calculated Invoice Amount");
			}
			
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	public void filtersAtCreditDebitTab(WebDriver driver, String franchiseId) throws Exception {
		
		FinanceRoyaltiesPage pobj = new FinanceRoyaltiesPage(driver);
		
		fc.utobj().clickElement(driver, pobj.creditsDebitsLnk);
		
		fc.utobj().clickElement(driver, pobj.showFilterLnk);

		fc.utobj().clickElement(driver, driver.findElement(By.xpath("//div[@id='ms-parentfranchiseID']//button[@type='button']")));
		
		fc.utobj().clickElement(driver,	driver.findElement(By.xpath(".//div[@id='ms-parentfranchiseID']//input[@id='selectAll']")));
		
		fc.utobj().sendKeys(driver,	driver.findElement(By.xpath(".//div[@id='ms-parentfranchiseID']//input[@class='searchInputMultiple']")), franchiseId);
		
		driver.findElement(By.xpath(".//div[@id='ms-parentfranchiseID']//input[@class='searchInputMultiple']")).sendKeys(Keys.ENTER);
		
		fc.utobj().clickElement(driver,	driver.findElement(By.xpath(".//div[@id='ms-parentfranchiseID']//input[@id='selectAll']")));
		
		fc.utobj().clickElement(driver, driver.findElement(By.xpath("//div[@id='ms-parentfranchiseID']//button[@type='button']")));
			
		fc.utobj().clickElement(driver, pobj.searchFilterBtn);

	}
	
	@Test(groups = { "finance_FreshDB" })
	@TestCase(createdOn = "2018-06-22", updatedOn = "2018-06-22", testCaseId = "TC_FC_Late_Fee_Payment_Page", testCaseDescription = "Verify late fee under payment tab")
	public void verifyLateFeeAtPaymentsPage() throws Exception {
		
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());
		
		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		Map<String, String> dataSet = fc.utobj().readTestData("finance", testCaseId);
		
		String agrmntVrsnName = fc.utobj().generateTestData("AV");
		String royaltyPrcntg = "10";
		String advPercentage = "8";
		String royaltyAreaFranchiseValue = "5";
		String advAreaFranchiseValue = "3";
		
		String lateFeeType="F", lateFeeAmount="50";
		String categoryQuantity="20", categoryAmount = "5000";
		
		try {
			driver = fc.loginpage().login(driver);
			FinanceSalesPage pobj = new FinanceSalesPage(driver);
			
			AdminFinanceFinanceSetupPreferencesPageTest setup = new AdminFinanceFinanceSetupPreferencesPageTest();
			
			fc.utobj().printTestStep("Add Agreement Version");
			AdminFinanceAgreementVersionsPageTest agreement = new AdminFinanceAgreementVersionsPageTest();
			String agreementVersion = agreement.addAgreementWithLateFee(driver, config, agrmntVrsnName,   royaltyPrcntg, advPercentage, royaltyAreaFranchiseValue, advAreaFranchiseValue, lateFeeType, lateFeeAmount);
			
			fc.utobj().printTestStep("Navigate To Admin > Franchise Location > Add Franchise Location");
			fc.utobj().printTestStep("Add Franchise Location");
			
			String franchiseId = fc.utobj().generateTestData("GT");
			
			AdminFranchiseLocationAddNewFranchiseLocationPageTest franchise = new AdminFranchiseLocationAddNewFranchiseLocationPageTest();
			franchiseId = franchise.addFranchiseLocation(driver, franchiseId, agreementVersion, config);
			
			fc.utobj().printTestStep("Navigate To Admin > Finance > Finance Setup Preferences");
			fc.utobj().printTestStep("Setup Medium of submission of Sales Reports");
			String salesReportType = "webForm";
			setup.mediumOfSubmissionOfSalesReports(driver, config, salesReportType);
			
			fc.utobj().printTestStep("Navigate To Finance > Sales");
			fc.utobj().printTestStep("Enter Sales Report");
			FinanceSalesPageTest enterSales = new FinanceSalesPageTest();
			String reportId = enterSales.enterSalesReport(driver, franchiseId, categoryQuantity, categoryAmount);
			
			fc.utobj().printTestStep("Filter Sales Report By Franchise Id");
			enterSales.filterSalesReport(driver, franchiseId);

			fc.utobj().printTestStep("Filter Sales Report By Franchise Id");
			enterSales.filterSalesReport(driver, franchiseId);

			fc.utobj().printTestStep("Approve Sales Report");
			fc.utobj().actionImgOption(driver, reportId, "Approve");

			fc.commonMethods().switch_cboxIframe_frameId(driver);
			
			fc.utobj().sendKeys(driver, pobj.commentTxtArea, "Test Approve Comments" + fc.utobj().generateRandomNumber());
			
			fc.utobj().clickElement(driver, pobj.SaveBtn);

			fc.utobj().clickElement(driver, pobj.cloaseBtn);
			
			fc.utobj().switchFrameToDefault(driver);
			
			fc.utobj().printTestStep("Filter Sales Report By Franchise Id");
			
			enterSales.filterSalesReport(driver, franchiseId);
			
			fc.utobj().printTestStep("Generate Invoice");
			
			fc.utobj().actionImgOption(driver, franchiseId, "Generate Invoice");
						
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
			
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}


	@Test(groups = { "finance_FreshDB" })
	@TestCase(createdOn = "2018-06-25", updatedOn = "2018-06-25", testCaseId = "TC_FC_Tax_at_Payment_And_Store_Summary_Page", testCaseDescription = "Verify Tax under payment and Store Summary tab")
	public void verifyTaxAtPaymentsPage() throws Exception {
		
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());
		
		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		Map<String, String> dataSet = fc.utobj().readTestData("finance", testCaseId);
		
		String agrmntVrsnName = fc.utobj().generateTestData("AV");
		String royaltyPrcntg = "10";
		String advPercentage = "8";
		String royaltyAreaFranchiseValue = "5";
		String advAreaFranchiseValue = "3";
		String lateFeeType="F", lateFeeAmount="50";
		String categoryQuantity="20", categoryAmount = "5000";
		
		try {
			driver = fc.loginpage().login(driver);
			FinanceSalesPage pobj = new FinanceSalesPage(driver);
			
			AdminFinanceFinanceSetupPreferencesPageTest setup = new AdminFinanceFinanceSetupPreferencesPageTest();
			
			fc.utobj().printTestStep("Add Agreement Version");
			AdminFinanceAgreementVersionsPageTest agreement = new AdminFinanceAgreementVersionsPageTest();
			String agreementVersion = agreement.addAgreementWithLateFee(driver, config, agrmntVrsnName,   royaltyPrcntg, advPercentage, royaltyAreaFranchiseValue, advAreaFranchiseValue, lateFeeType, lateFeeAmount);
			
			fc.utobj().printTestStep("Navigate To Admin > Franchise Location > Add Franchise Location");
			fc.utobj().printTestStep("Add Franchise Location");
			
			String franchiseId = fc.utobj().generateTestData("GT");
			
			AdminFranchiseLocationAddNewFranchiseLocationPageTest franchise = new AdminFranchiseLocationAddNewFranchiseLocationPageTest();
			franchiseId = franchise.addFranchiseLocation(driver, franchiseId, agreementVersion, config);
			
			fc.utobj().printTestStep("Navigate To Admin > Finance > Finance Setup Preferences");
			fc.utobj().printTestStep("Setup Medium of submission of Sales Reports");
			String salesReportType = "webForm";
			setup.mediumOfSubmissionOfSalesReports(driver, config, salesReportType);
			
			fc.utobj().printTestStep("Navigate To Finance > Sales");
			fc.utobj().printTestStep("Enter Sales Report");
			FinanceSalesPageTest enterSales = new FinanceSalesPageTest();
			String reportId = enterSales.enterSalesReport(driver, franchiseId, categoryQuantity, categoryAmount);
			
			fc.utobj().printTestStep("Filter Sales Report By Franchise Id");
			enterSales.filterSalesReport(driver, franchiseId);
			
			fc.utobj().printTestStep("Approve Sales Report");
			fc.utobj().actionImgOption(driver, reportId, "Approve");
			
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			
			fc.utobj().sendKeys(driver, pobj.commentTxtArea, "Test Approve Comments" + fc.utobj().generateRandomNumber());
			
			fc.utobj().clickElement(driver, pobj.SaveBtn);
			
			fc.utobj().clickElement(driver, pobj.cloaseBtn);
			
			fc.utobj().switchFrameToDefault(driver);
			
			fc.utobj().printTestStep("Filter Sales Report By Franchise Id");
			
			enterSales.filterSalesReport(driver, franchiseId);
			
			fc.utobj().printTestStep("Generate Invoice");
			
			fc.utobj().actionImgOption(driver, franchiseId, "Generate Invoice");
			
			// Something is missing here, need to check
			
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
			
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	
	
}
