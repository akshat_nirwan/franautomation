package com.builds.test.fin;

import static org.testng.Assert.assertEquals;
import java.io.File;
import java.util.Map;
import java.util.Set;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;
import com.builds.test.admin.AdminFranchiseLocationAddFranchiseLocationPageTest;
import com.builds.uimaps.fin.FinanceProfitLossStatementPage;
import com.builds.utilities.FranconnectUtil;
import com.builds.utilities.TestCase;

public class FinanceProfitLossStatementPageTest {

	FranconnectUtil fc = new FranconnectUtil();

	@Test(groups = {"finance", "finance_FailedTC"})
	@TestCase(createdOn = "2017-06-02", updatedOn = "2017-06-05", testCaseId = "TC_FIN_PLS_Create_Statement", testCaseDescription = "Verify The Created Profit and Lost Statement")
	public void createPLSStatement() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		Map<String, String> dataSet = fc.utobj().readTestData("finance", testCaseId);
		String agrmntVrsnName = fc.utobj().generateTestData(dataSet.get("agreementVersionName"));
		String royaltyPrcntg = dataSet.get("royaltyPercentage");
		String advPercentage = dataSet.get("advertisementPercentage");
		String royaltyAreaFranchiseValue = dataSet.get("RoyaltyAreaFranchise");
		String advAreaFranchiseValue = dataSet.get("AdvAreaFranchise");
		
		try {
			driver = fc.loginpage().login(driver);
			FinanceProfitLossStatementPage pobj = new FinanceProfitLossStatementPage(driver);

			fc.utobj().printTestStep("Navigate To Admin > Finance > Configure Profit & Loss Categories Page");
			fc.utobj().printTestStep("Add Main Category");

			String categoryName = fc.utobj().generateTestData("Test Main Category");
			categoryName = new AdminFinanceProfitLossCategoriesPageTest().addNewMainCategory(driver, categoryName);

			fc.utobj().printTestStep("Navigate To Admin > Finance > Agreement Versions");
			AdminFinanceAgreementVersionsPageTest agreementforSales = new AdminFinanceAgreementVersionsPageTest();

			fc.utobj().printTestStep("Add Agreement");
			String agreementVersion = agreementforSales.addAgreement(driver, config, agrmntVrsnName, royaltyPrcntg,
					advPercentage, royaltyAreaFranchiseValue, advAreaFranchiseValue);

			String regionName = fc.utobj().generateTestData("GT");
			String storeType = fc.utobj().generateTestData("GT");
			String franchiseId = fc.utobj().generateTestData("GT");
			AdminFranchiseLocationAddFranchiseLocationPageTest franchise = new AdminFranchiseLocationAddFranchiseLocationPageTest();
			franchiseId = franchise.addFranchiseLocation_All_Agreement(driver, franchiseId, regionName, storeType,
					agreementVersion, config);

			// String franchiseId = "GTi13142314", categoryName = "Default13014328d";
			fc.utobj().printTestStep("Navigate To Finance > Profit & Loss Statement > Profit / Loss Summary Page");
			fc.finance().finance_common().financeProfitAndLossStatementPage(driver);

			fc.utobj().printTestStep("Add Profit And Lost Statement");
			fc.utobj().clickElement(driver, pobj.newProfitLossStatement);
			fc.utobj().selectDropDown(driver, pobj.selectFranchise, franchiseId);

			fc.utobj().selectDropDownByIndex(driver, pobj.selectStatementPeriod, 1);

			fc.utobj().clickElement(driver, pobj.continueBtn);
			fc.utobj().sendKeys(driver, pobj.submissionDate, fc.utobj().getCurrentDateUSFormat());

			double income = 5000;
			double expense = 2000.00;
			double Overhead = 200.00;
			double categoryAmount = 1000;

			double netProfitVal = income + /* categoryAmount */ -expense - Overhead;

			fc.utobj().sendKeys(driver, pobj.incomeTxtBx, Double.toString(income));
			fc.utobj().sendKeys(driver, pobj.expenseTxtBx, Double.toString(expense));
			fc.utobj().sendKeys(driver, pobj.OverheadTxBx, Double.toString(Overhead));
			fc.utobj().sendKeys(driver,
					driver.findElement(By.xpath(".//td[contains(text () ,'" + categoryName
							+ "')]/following-sibling::td/input[contains(@name , 'amount')]")),
					Double.toString(categoryAmount));
			fc.utobj().clickElement(driver, pobj.submitBtn);

			fc.utobj().printTestStep("Verify The Income At Statement Details Page");
			String incomeTextVal = fc.utobj().getText(driver, driver
					.findElement(By.xpath(".//td[contains(text () , 'Income')]/following-sibling::td[@align='left']")));

			if (incomeTextVal.contains(",")) {
				incomeTextVal = incomeTextVal.replaceAll(",", "");
			}

			if (income != Double.parseDouble(incomeTextVal)) {
				fc.utobj().throwsException("was not able to verify the Income Value At Statement Details");
			}

			fc.utobj().printTestStep("Verify The Expense At Statement Details Page");
			String expenseTextVal = fc.utobj().getText(driver, fc.utobj().getElementByXpath(driver,
					(".//td[contains(text () , 'Expense')]/following-sibling::td[@align='left']")));
			if (expenseTextVal.contains(",") || expenseTextVal.contains("-")) {
				expenseTextVal = expenseTextVal.replaceAll("[-,]", "");
			}

			if (expense != Double.parseDouble(expenseTextVal)) {
				fc.utobj().throwsException("was not able to verify the Expense Value At Statement Details");
			}

			fc.utobj().printTestStep("Verify The Overhead At Statement Details Page");
			String overheadTextVal = fc.utobj().getText(driver, fc.utobj().getElementByXpath(driver,
					(".//td[contains(text () , 'Overhead')]/following-sibling::td[@align='left']")));
			if (overheadTextVal.contains(",") || overheadTextVal.contains("-")) {
				overheadTextVal = overheadTextVal.replaceAll("[-,]", "");
			}

			if (Overhead != Double.parseDouble(overheadTextVal)) {
				fc.utobj().throwsException("was not able to verify the Overhead Value At Statement Details");
			}

			fc.utobj().printTestStep("Verify The Added Category Value At Statement Details Page");
			String categoryTextVal = fc.utobj().getText(driver, fc.utobj().getElementByXpath(driver,
					".//td[contains(text () , '" + categoryName + "')]/following-sibling::td[@align='left']"));
			if (categoryTextVal.contains(",")) {
				categoryTextVal = categoryTextVal.replaceAll(",", "");
			}

			if (categoryAmount != Double.parseDouble(categoryTextVal)) {
				fc.utobj().throwsException("was not able to verify the Category Value At Statement Details");
			}

			fc.utobj().printTestStep("Verify The Net Profit For The Month");
			String netProfitTextVal = fc.utobj().getText(driver, fc.utobj().getElementByXpath(driver,
					(".//td[contains(text () , 'Net Profit')]/following-sibling::td[@align='left']")));
			if (netProfitTextVal.contains(",")) {
				netProfitTextVal = netProfitTextVal.replaceAll(",", "");
			}

			if (netProfitVal != Double.parseDouble(netProfitTextVal)) {
				fc.utobj().throwsException("was not able to verify the Net Profit Value At Statement Details");
			}

			fc.utobj().clickElement(driver, pobj.backBtn);

			filterByFranchiseId(driver, franchiseId);

			fc.utobj().printTestStep("Verify The Total Income At Profit Loss Summary Page");

			String totalIncomeVal = fc.utobj().getText(driver,
					driver.findElement(By.xpath(".//a[contains(text(), '" + franchiseId + "')]/ancestor::tr/td[5]")));

			if (totalIncomeVal.contains(",")) {
				totalIncomeVal = totalIncomeVal.replaceAll(",", "");
			}

			if (income != Double.parseDouble(totalIncomeVal)) {
				fc.utobj().throwsException("was not able to verify total income at summary page");
			}

			fc.utobj().printTestStep("Verify The Total Expense At Profit Loss Summary Page");
			String totalExpenseVal = fc.utobj().getText(driver,
					driver.findElement(By.xpath(".//a[contains(text () , '" + franchiseId + "')]/ancestor::tr/td[5]")));
			if (totalExpenseVal.contains(",") || totalExpenseVal.contains("-")) {
				totalExpenseVal = totalExpenseVal.replaceAll("[-,]", "");
			}

			if (expense != Double.parseDouble(totalExpenseVal)) {
				fc.utobj().throwsException("was not able to verify total expense at summary page");
			}

			fc.utobj().printTestStep("Verify The Total overhead At Profit Loss Summary Page");
			String totalOverheadVal = fc.utobj().getText(driver,
					driver.findElement(By.xpath(".//a[contains(text () , '" + franchiseId + "')]/ancestor::tr/td[6]")));
			if (totalOverheadVal.contains(",") || totalOverheadVal.contains("-")) {
				totalOverheadVal = totalOverheadVal.replaceAll("[-,]", "");
			}

			if (Overhead != Double.parseDouble(totalOverheadVal)) {
				fc.utobj().throwsException("was not able to verify total overhead at summary page");
			}

			boolean isCategoryValPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[contains(text () , '"
					+ franchiseId + "')]/ancestor::tr/td[contains(text () , '" + categoryAmount + "')]");
			if (isCategoryValPresent == false) {
				fc.utobj().throwsException("was not able to verify the Category Amount At Summary Page");
			}

			fc.utobj().printTestStep("Navigate to Finance > Store Summary");

			fc.finance().finance_common().financeStoreSummaryPage(driver);

			fc.utobj().clickElement(driver, driver.findElement(By.id("franchiseID")));

			fc.utobj().clickElement(driver,
					driver.findElement(By.xpath(".//*[contains(text(), '" + franchiseId + "')]")));

			fc.utobj().clickElement(driver, driver.findElement(By.id("getSummary")));

			fc.utobj().printTestStep("Verify Profit and Loss Statement at Summary Page");

			boolean isReportPresent = fc.utobj().assertLinkText(driver, franchiseId);

			if (isReportPresent == false) {

				fc.utobj().throwsException("Franchise ID is not visible at Store Summary Page");
			}

			// To verify PL Statement, Categories, Sub-categories , Statement Period,

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	public void createPLSStatement(WebDriver driver, String franchiseId, double income, double expense, double Overhead)
			throws Exception {
		FinanceProfitLossStatementPage pobj = new FinanceProfitLossStatementPage(driver);
		fc.finance().finance_common().financeProfitAndLossStatementPage(driver);
		fc.utobj().clickElement(driver, pobj.newProfitLossStatement);
		fc.utobj().selectDropDown(driver, pobj.selectFranchise, franchiseId);

		fc.utobj().selectDropDownByIndex(driver, pobj.selectStatementPeriod, 1);

		fc.utobj().clickElement(driver, pobj.continueBtn);
		fc.utobj().sendKeys(driver, pobj.submissionDate, fc.utobj().getCurrentDateUSFormat());
		fc.utobj().sendKeys(driver, pobj.incomeTxtBx, Double.toString(income));
		fc.utobj().sendKeys(driver, pobj.expenseTxtBx, Double.toString(expense));
		fc.utobj().sendKeys(driver, pobj.OverheadTxBx, Double.toString(Overhead));
		fc.utobj().clickElement(driver, pobj.submitBtn);

		fc.utobj().clickElement(driver, pobj.backBtn);
	}

	@Test(groups = {"finance", "finance_FailedTC"})
	@TestCase(createdOn = "2017-06-02", updatedOn = "2017-06-05", testCaseId = "TC_FIN_View_Statement", testCaseDescription = "Verify The View Profit and Loss Statement")
	public void viewStatement() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		Map<String, String> dataSet = fc.utobj().readTestData("finance", testCaseId);

		String agrmntVrsnName = fc.utobj().generateTestData(dataSet.get("agreementVersionName"));
		String royaltyPrcntg = dataSet.get("royaltyPercentage");
		String advPercentage = dataSet.get("advertisementPercentage");
		String royaltyAreaFranchiseValue = dataSet.get("RoyaltyAreaFranchise");
		String advAreaFranchiseValue = dataSet.get("AdvAreaFranchise");
		
		try {
			driver = fc.loginpage().login(driver);
			FinanceProfitLossStatementPage pobj = new FinanceProfitLossStatementPage(driver);

			fc.utobj().printTestStep("Navigate To Admin > Finance > Agreement Versions");
			AdminFinanceAgreementVersionsPageTest agreementforSales = new AdminFinanceAgreementVersionsPageTest();

			fc.utobj().printTestStep("Add Agreement");
			String agreementVersion = agreementforSales.addAgreement(driver, config, agrmntVrsnName, royaltyPrcntg,
					advPercentage, royaltyAreaFranchiseValue, advAreaFranchiseValue);

			String regionName = fc.utobj().generateTestData("GT");
			String storeType = fc.utobj().generateTestData("GT");
			String franchiseId = fc.utobj().generateTestData("GT");
			AdminFranchiseLocationAddFranchiseLocationPageTest franchise = new AdminFranchiseLocationAddFranchiseLocationPageTest();
			franchiseId = franchise.addFranchiseLocation_All_Agreement(driver, franchiseId, regionName, storeType,
					agreementVersion, config);

			fc.utobj().printTestStep("Navigate To Finance > Profit & Loss Statement > Profit / Loss Summary Page");
			fc.utobj().printTestStep("Add Profit And Lost Statement");

			double income = 456785.01;
			double expense = 2345.00;
			double Overhead = 122.00;

			double netProfitVal = income - expense - Overhead;

			createPLSStatement(driver, franchiseId, income, expense, Overhead);
			filterByFranchiseId(driver, franchiseId);

			fc.utobj().printTestStep("View Profit And Loss Statement");
			fc.utobj().actionImgOption(driver, franchiseId, "View Statement");

			fc.utobj().printTestStep("Verify The Income At Statement Details Page");
			String incomeTextVal = fc.utobj().getText(driver, driver
					.findElement(By.xpath(".//td[contains(text () , 'Income')]/following-sibling::td[@align='left']")));

			if (incomeTextVal.contains(",")) {
				incomeTextVal = incomeTextVal.replaceAll(",", "");
			}

			if (income != Double.parseDouble(incomeTextVal)) {
				fc.utobj().throwsException("was not able to verify the Income Value At Statement Details");
			}

			fc.utobj().printTestStep("Verify The Expense At Statement Details Page");
			String expenseTextVal = fc.utobj().getText(driver, fc.utobj().getElementByXpath(driver,
					(".//td[contains(text () , 'Expense')]/following-sibling::td[@align='left']")));
			if (expenseTextVal.contains(",") || expenseTextVal.contains("-")) {
				expenseTextVal = expenseTextVal.replaceAll("[-,]", "");
			}

			if (expense != Double.parseDouble(expenseTextVal)) {
				fc.utobj().throwsException("was not able to verify the Expense Value At Statement Details");
			}

			fc.utobj().printTestStep("Verify The Overhead At Statement Details Page");
			String overheadTextVal = fc.utobj().getText(driver, fc.utobj().getElementByXpath(driver,
					(".//td[contains(text () , 'Overhead')]/following-sibling::td[@align='left']")));
			if (overheadTextVal.contains(",") || overheadTextVal.contains("-")) {
				overheadTextVal = overheadTextVal.replaceAll("[-,]", "");
			}

			if (Overhead != Double.parseDouble(overheadTextVal)) {
				fc.utobj().throwsException("was not able to verify the Overhead Value At Statement Details");
			}

			fc.utobj().printTestStep("Verify The Net Profit For The Month");
			String netProfitTextVal = fc.utobj().getText(driver, fc.utobj().getElementByXpath(driver,
					(".//td[contains(text () , 'Net Profit')]/following-sibling::td[@align='left']")));
			if (netProfitTextVal.contains(",")) {
				netProfitTextVal = netProfitTextVal.replaceAll("[-,]", "");
			}

			if (netProfitVal != Double.parseDouble(netProfitTextVal)) {
				fc.utobj().throwsException("was not able to verify the Net Profit Value At Statement Details");
			}

			fc.utobj().clickElement(driver, pobj.backBtn);

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = {"finance", "finance_FailedTC"})
	@TestCase(createdOn = "2017-06-02", updatedOn = "2017-06-09", testCaseId = "TC_FIN_Modify_Statement", testCaseDescription = "Verify The ModifyProfit and Loss Statement")
	public void modifyStatement() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		Map<String, String> dataSet = fc.utobj().readTestData("finance", testCaseId);

		String agrmntVrsnName = fc.utobj().generateTestData(dataSet.get("agreementVersionName"));
		String royaltyPrcntg = dataSet.get("royaltyPercentage");
		String advPercentage = dataSet.get("advertisementPercentage");
		String royaltyAreaFranchiseValue = dataSet.get("RoyaltyAreaFranchise");
		String advAreaFranchiseValue = dataSet.get("AdvAreaFranchise");
		
		try {
			driver = fc.loginpage().login(driver);
			FinanceProfitLossStatementPage pobj = new FinanceProfitLossStatementPage(driver);

			fc.utobj().printTestStep("Navigate To Admin > Finance > Agreement Versions");
			AdminFinanceAgreementVersionsPageTest agreementforSales = new AdminFinanceAgreementVersionsPageTest();

			fc.utobj().printTestStep("Add Agreement");
			String agreementVersion = agreementforSales.addAgreement(driver, config, agrmntVrsnName, royaltyPrcntg,
					advPercentage, royaltyAreaFranchiseValue, advAreaFranchiseValue);

			String regionName = fc.utobj().generateTestData("GT");
			String storeType = fc.utobj().generateTestData("GT");
			String franchiseId = fc.utobj().generateTestData("GT");
			AdminFranchiseLocationAddFranchiseLocationPageTest franchise = new AdminFranchiseLocationAddFranchiseLocationPageTest();
			franchiseId = franchise.addFranchiseLocation_All_Agreement(driver, franchiseId, regionName, storeType,
					agreementVersion, config);

			fc.utobj().printTestStep("Navigate To Finance > Profit & Loss Statement > Profit / Loss Summary Page");
			fc.utobj().printTestStep("Add Profit And Lost Statement");

			double income = 456785.01;
			double expense = 2345.00;
			double Overhead = 122.00;

			createPLSStatement(driver, franchiseId, income, expense, Overhead);
			filterByFranchiseId(driver, franchiseId);

			fc.utobj().printTestStep("Modify Profit And Loss Statement");
			fc.utobj().actionImgOption(driver, franchiseId, "Modify Statement");

			income = 986584.01;
			expense = 1245.00;
			Overhead = 3695.00;

			double netProfitVal = income - expense - Overhead;

			fc.utobj().sendKeys(driver, pobj.incomeTxtBx, Double.toString(income));
			fc.utobj().sendKeys(driver, pobj.expenseTxtBx, Double.toString(expense));
			fc.utobj().sendKeys(driver, pobj.OverheadTxBx, Double.toString(Overhead));
			fc.utobj().clickElement(driver, pobj.submitBtn);
			fc.utobj().printTestStep("Verify The Income At Statement Details Page");
			String incomeTextVal = fc.utobj().getText(driver, driver
					.findElement(By.xpath(".//td[contains(text () , 'Income')]/following-sibling::td[@align='left']")));

			if (incomeTextVal.contains(",")) {
				incomeTextVal = incomeTextVal.replaceAll(",", "");
			}

			if (income != Double.parseDouble(incomeTextVal)) {
				fc.utobj().throwsException("was not able to verify the Income Value At Statement Details");
			}

			fc.utobj().printTestStep("Verify The Expense At Statement Details Page");
			String expenseTextVal = fc.utobj().getText(driver, fc.utobj().getElementByXpath(driver,
					(".//td[contains(text () , 'Expense')]/following-sibling::td[@align='left']")));
			if (expenseTextVal.contains(",") || expenseTextVal.contains("-")) {
				expenseTextVal = expenseTextVal.replaceAll("[-,]", "");
			}

			if (expense != Double.parseDouble(expenseTextVal)) {
				fc.utobj().throwsException("was not able to verify the Expense Value At Statement Details");
			}

			fc.utobj().printTestStep("Verify The Overhead At Statement Details Page");
			String overheadTextVal = fc.utobj().getText(driver, fc.utobj().getElementByXpath(driver,
					(".//td[contains(text () , 'Overhead')]/following-sibling::td[@align='left']")));
			if (overheadTextVal.contains(",") || overheadTextVal.contains("-")) {
				overheadTextVal = overheadTextVal.replaceAll("[-,]", "");
			}

			if (Overhead != Double.parseDouble(overheadTextVal)) {
				fc.utobj().throwsException("was not able to verify the Overhead Value At Statement Details");
			}

			fc.utobj().printTestStep("Verify The Net Profit For The Month");
			String netProfitTextVal = fc.utobj().getText(driver, fc.utobj().getElementByXpath(driver,
					(".//td[contains(text () , 'Net Profit')]/following-sibling::td[@align='left']")));
			if (netProfitTextVal.contains(",")) {
				netProfitTextVal = netProfitTextVal.replaceAll("[-,]", "");
			}

			if (netProfitVal != Double.parseDouble(netProfitTextVal)) {
				fc.utobj().throwsException("was not able to verify the Net Profit Value At Statement Details");
			}

			fc.utobj().clickElement(driver, pobj.backBtn);

			filterByFranchiseId(driver, franchiseId);

			fc.utobj().printTestStep("Verify The Total Income At Profit Loss Summary Page");
			String totalIncomeVal = fc.utobj().getText(driver,
					driver.findElement(By.xpath(".//a[contains(text () , '" + franchiseId + "')]/ancestor::tr/td[4]")));
			if (totalIncomeVal.contains(",")) {
				totalIncomeVal = totalIncomeVal.replaceAll(",", "");
			}

			if (income != Double.parseDouble(totalIncomeVal)) {
				fc.utobj().throwsException("was not able to verify total income at summary page");
			}

			fc.utobj().printTestStep("Verify The Total Expense At Profit Loss Summary Page");
			String totalExpenseVal = fc.utobj().getText(driver,
					driver.findElement(By.xpath(".//a[contains(text () , '" + franchiseId + "')]/ancestor::tr/td[5]")));
			if (totalExpenseVal.contains(",") || totalExpenseVal.contains("-")) {
				totalExpenseVal = totalExpenseVal.replaceAll("[-,]", "");
			}

			if (expense != Double.parseDouble(totalExpenseVal)) {
				fc.utobj().throwsException("was not able to verify total expense at summary page");
			}

			fc.utobj().printTestStep("Verify The Total overhead At Profit Loss Summary Page");
			String totalOverheadVal = fc.utobj().getText(driver,
					driver.findElement(By.xpath(".//a[contains(text () , '" + franchiseId + "')]/ancestor::tr/td[6]")));
			if (totalOverheadVal.contains(",") || totalOverheadVal.contains("-")) {
				totalOverheadVal = totalOverheadVal.replaceAll("[-,]", "");
			}

			if (Overhead != Double.parseDouble(totalOverheadVal)) {
				fc.utobj().throwsException("was not able to verify total overhead at summary page");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = {"finance", "finance_FailedTC" })
	@TestCase(createdOn = "2017-06-02", updatedOn = "2017-06-05", testCaseId = "TC_FIN_DashBoard_Profit_Loss", testCaseDescription = "Verify The Profit and Loss Statement At DashBoard")
	public void viewAtDashboard() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		Map<String, String> dataSet = fc.utobj().readTestData("finance", testCaseId);

		String agrmntVrsnName = fc.utobj().generateTestData(dataSet.get("agreementVersionName"));
		String royaltyPrcntg = dataSet.get("royaltyPercentage");
		String advPercentage = dataSet.get("advertisementPercentage");
		String royaltyAreaFranchiseValue = dataSet.get("RoyaltyAreaFranchise");
		String advAreaFranchiseValue = dataSet.get("AdvAreaFranchise");

		try {
			driver = fc.loginpage().login(driver);
			FinanceProfitLossStatementPage pobj = new FinanceProfitLossStatementPage(driver);

			fc.utobj().printTestStep("Navigate To Admin > Finance > Agreement Versions");
			AdminFinanceAgreementVersionsPageTest agreementforSales = new AdminFinanceAgreementVersionsPageTest();

			fc.utobj().printTestStep("Add Agreement");
			String agreementVersion = agreementforSales.addAgreement(driver, config, agrmntVrsnName, royaltyPrcntg,
					advPercentage, royaltyAreaFranchiseValue, advAreaFranchiseValue);

			String regionName = fc.utobj().generateTestData("GT");
			String storeType = fc.utobj().generateTestData("GT");
			String franchiseId = fc.utobj().generateTestData("GT");
			AdminFranchiseLocationAddFranchiseLocationPageTest franchise = new AdminFranchiseLocationAddFranchiseLocationPageTest();
			franchiseId = franchise.addFranchiseLocation_All_Agreement(driver, franchiseId, regionName, storeType,
					agreementVersion, config);

			fc.utobj().printTestStep("Navigate To Finance > Profit & Loss Statement > Profit / Loss Summary Page");
			fc.utobj().printTestStep("Add Profit And Lost Statement");

			double income = 456785.01;
			double expense = 2345.00;
			double Overhead = 122.00;

			createPLSStatement(driver, franchiseId, income, expense, Overhead);

			fc.utobj().printTestStep("Navigate To Finance > Profit And Loss Statement > DashBoard");
			fc.utobj().clickElement(driver, pobj.dashboardTab);
			fc.utobj().selectDropDownByVisibleTextTrimed(driver, pobj.selectFranchiseIdAtDash, franchiseId);
			fc.utobj().clickElement(driver, pobj.continueBtn);
			fc.utobj().selectDropDownByIndex(driver, pobj.selectMonth, 0);

			fc.utobj().printTestStep("Verify The My Income Analysis For The Current Month");
			String incomeTextVal = fc.utobj().getText(driver, fc.utobj().getElementByXpath(driver,
					(".//td[contains(text () , 'My Income')]/following-sibling::td[@align='right']")));
			if (incomeTextVal.contains(",")) {
				incomeTextVal = incomeTextVal.replaceAll(",", "");
			}

			if (income != Double.parseDouble(incomeTextVal)) {
				fc.utobj().printTestStep("Not able to verify the My Income At DashBoard");
			}

			fc.utobj().printTestStep("Verify The My Expense Analysis For The Current Month");
			String expenseTextVal = fc.utobj().getText(driver, fc.utobj().getElementByXpath(driver,
					(".//td[contains(text () , 'My Expense')]/following-sibling::td[@align='right']")));
			if (expenseTextVal.contains(",") || expenseTextVal.contains("-")) {
				expenseTextVal = expenseTextVal.replaceAll("[-,]", "");
			}

			if (expense != Double.parseDouble(expenseTextVal)) {
				fc.utobj().printTestStep("Not able to verify the My Expense At DashBoard");
			}

			fc.utobj().printTestStep("Verify The Overhead Analysis For The Current Month");
			String OverheadTextVal = fc.utobj().getText(driver, fc.utobj().getElementByXpath(driver,
					(".//td[contains(text () , 'My Overhead')]/following-sibling::td[@align='right']")));
			if (OverheadTextVal.contains(",") || OverheadTextVal.contains("-")) {
				OverheadTextVal = OverheadTextVal.replaceAll("[-,]", "");
			}

			if (Overhead != Double.parseDouble(OverheadTextVal)) {
				fc.utobj().printTestStep("Not able to verify the My Overhead At DashBoard");
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	public void filterByFranchiseId(WebDriver driver, String franchiseId) throws Exception {
		FinanceProfitLossStatementPage pobj = new FinanceProfitLossStatementPage(driver);
		fc.utobj().clickElement(driver, pobj.showFilter);
		fc.utobj().setToDefault(driver, pobj.selectFranchiseId);
		fc.utobj().selectValFromMultiSelect(driver, pobj.selectFranchiseId, franchiseId);
		fc.utobj().clickElement(driver, pobj.searchBtn);
		// fc.utobj().clickElement(driver, pobj.hideFilter);
	}

	@Test(groups = "finance_FreshDB")
	@TestCase(createdOn = "2018-06-14", updatedOn = "2018-06-14", testCaseId = "TC_FIN_Upload_P&L_Statement_Button", testCaseDescription = "Verify upload profit & Loss Statement Button")
	public void uploadPLButtonInDiffCases() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		Map<String, String> dataSet = fc.utobj().readTestData("finance", testCaseId);

		try {
			driver = fc.loginpage().login(driver);
			AdminFinanceFinanceSetupPreferencesPageTest setup = new AdminFinanceFinanceSetupPreferencesPageTest();

			fc.utobj().printTestStep(
					"Navigate To Admin > Finance > Sales Report Preferences > Medium of Submission of Profit and Loss reports");
			String PLReportType = "csv";
			setup.mediumOfSubmissionOfProfitAndLossReports(driver, config, PLReportType);

			fc.utobj().printTestStep("Navigate to Finance > Profit and Loss Statement > Upload P&L");
			fc.finance().finance_common().financeProfitAndLossStatementPage(driver);

			WebElement element = driver.findElement(By.xpath(".//input[@value='Upload Profit / Loss Statement']"));

			boolean isUploadPLStateButtonPresent = fc.utobj().isElementPresent(driver, element);

			if (isUploadPLStateButtonPresent == false) {

				fc.utobj().throwsException("Upload Profit / Loss Statement Button is not present");
			}

			PLReportType = "xls";
			setup.mediumOfSubmissionOfProfitAndLossReports(driver, config, PLReportType);
			fc.utobj().printTestStep("Navigate to Finance > Profit and Loss Statement > Upload P&L");
			fc.finance().finance_common().financeProfitAndLossStatementPage(driver);

			WebElement elementXLS = driver.findElement(By.xpath(".//input[@value='Upload Profit / Loss Statement']"));

			boolean uploadPLStateButtonPresent = fc.utobj().isElementPresent(driver, elementXLS);

			if (uploadPLStateButtonPresent == false) {

				fc.utobj().throwsException("Upload Profit / Loss Statement Button is not present in Case of XLS");
			}

			PLReportType = "QuickBooks";
			setup.mediumOfSubmissionOfProfitAndLossReports(driver, config, PLReportType);
			fc.utobj().printTestStep("Navigate to Finance > Profit and Loss Statement > Upload P&L");
			fc.finance().finance_common().financeProfitAndLossStatementPage(driver);

			WebElement elementQB = driver.findElement(By.xpath(".//input[@value='New Profit / Loss Statement']"));

			boolean isUploadPLStateButtonPresent1 = fc.utobj().isElementPresent(driver, elementQB);

			if (isUploadPLStateButtonPresent1 == false) {

				fc.utobj().throwsException("Upload Profit / Loss Statement Button is not present in Case of QB");
			}

			PLReportType = "webform";
			setup.mediumOfSubmissionOfProfitAndLossReports(driver, config, PLReportType);
			fc.utobj().printTestStep("Navigate to Finance > Profit and Loss Statement > Upload P&L");
			fc.finance().finance_common().financeProfitAndLossStatementPage(driver);

			WebElement elementWF = driver.findElement(By.xpath(".//input[@value='New Profit / Loss Statement']"));

			boolean isUploadPLStateButtonPresent2 = fc.utobj().isElementPresent(driver, elementWF);

			if (isUploadPLStateButtonPresent2 == false) {

				fc.utobj().throwsException("Upload Profit / Loss Statement Button is not present in case of WebForm");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "finance_FreshDB")
	@TestCase(createdOn = "2018-06-15", updatedOn = "2018-07-30", testCaseId = "TC_FIN_PLS_Upload_Statement_XLS", testCaseDescription = "Verify XLS uploaded Profit and Lost Statement")
	public void uploadPLSStatementXLS() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		Map<String, String> dataSet = fc.utobj().readTestData("finance", testCaseId);
		String agrmntVrsnName = "AV";
		String royaltyPrcntg = "10";
		String advPercentage = "8";
		String royaltyAreaFranchiseValue = "3";
		String advAreaFranchiseValue = "2";
		String categoryName = "TestCategory";
		String franchiseId = "001C";

		try {
			driver = fc.loginpage().login(driver);
			FinanceProfitLossStatementPage pobj = new FinanceProfitLossStatementPage(driver);

			fc.utobj().printTestStep("Navigate To Admin > Finance > Configure Profit & Loss Categories Page");
			fc.utobj().printTestStep("Add Main Category");

			categoryName = new AdminFinanceProfitLossCategoriesPageTest().addNewMainCategory(driver, categoryName);

			fc.utobj().printTestStep("Navigate To Admin > Finance > Agreement Versions");
			AdminFinanceAgreementVersionsPageTest agreementforSales = new AdminFinanceAgreementVersionsPageTest();

			fc.utobj().printTestStep("Add Agreement");
			String agreementVersion = agreementforSales.addAgreement(driver, config, agrmntVrsnName, royaltyPrcntg,
					advPercentage, royaltyAreaFranchiseValue, advAreaFranchiseValue);

			AdminFranchiseLocationAddNewFranchiseLocationPageTest franchise = new AdminFranchiseLocationAddNewFranchiseLocationPageTest();
			franchise.addFranchiseLocation(driver, franchiseId, agreementVersion, config);

			String PLReportType = "XLS";
			AdminFinanceFinanceSetupPreferencesPageTest setup = new AdminFinanceFinanceSetupPreferencesPageTest();

			setup.mediumOfSubmissionOfProfitAndLossReports(driver, config, PLReportType);

			fc.utobj().printTestStep("Navigate To Finance > Profit & Loss Statement > Profit / Loss Summary Page");
			fc.finance().finance_common().financeProfitAndLossStatementPage(driver);

			fc.utobj().printTestStep("Upload XLS file of Profit And Lost Statement");

			fc.utobj().clickElement(driver, pobj.uploadProfitLossStatement);

			fc.utobj().sendKeys(driver, pobj.clickBrowseBtnUploadFile,
					"C:\\Selenium_Test_Input\\testData\\ProfitAndLossStatement.xls");

			String parentWindow = driver.getWindowHandle();

			fc.utobj().clickElement(driver, pobj.contFileUploadBtn);

			fc.utobj().acceptAlertBox(driver);

			// fc.utobj().switchFrameById(driver, "progressIframe");

			Set<String> handles = driver.getWindowHandles();

			for (String windowHandle : handles) {
				if (!windowHandle.equals(parentWindow)) {
					driver.switchTo().window(windowHandle);

					fc.utobj().clickElement(driver, pobj.uploadBtn);

					fc.utobj().clickElement(driver, pobj.okBtn);
					Thread.sleep(1000);
					driver.switchTo().window(parentWindow);
				}
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "finance_FreshDB")
	@TestCase(createdOn = "2018-06-15", updatedOn = "2018-07-30", testCaseId = "TC_FIN_PLS_Upload_Statement_CSV", testCaseDescription = "Verify CSV uploaded Profit and Lost Statement")
	public void uploadPLSStatementCSV() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		Map<String, String> dataSet = fc.utobj().readTestData("finance", testCaseId);
		String agrmntVrsnName = "AV";
		String royaltyPrcntg = "10";
		String advPercentage = "8";
		String royaltyAreaFranchiseValue = "3";
		String advAreaFranchiseValue = "2";
		String categoryName = "TestCategory";
		String franchiseId = "001D";

		try {
			driver = fc.loginpage().login(driver);
			FinanceProfitLossStatementPage pobj = new FinanceProfitLossStatementPage(driver);

			fc.utobj().printTestStep("Navigate To Admin > Finance > Configure Profit & Loss Categories Page");
			fc.utobj().printTestStep("Add Main Category");

			categoryName = new AdminFinanceProfitLossCategoriesPageTest().addNewMainCategory(driver, categoryName);

			fc.utobj().printTestStep("Navigate To Admin > Finance > Agreement Versions");
			AdminFinanceAgreementVersionsPageTest agreementforSales = new AdminFinanceAgreementVersionsPageTest();

			fc.utobj().printTestStep("Add Agreement");
			String agreementVersion = agreementforSales.addAgreement(driver, config, agrmntVrsnName, royaltyPrcntg,
					advPercentage, royaltyAreaFranchiseValue, advAreaFranchiseValue);

			AdminFranchiseLocationAddNewFranchiseLocationPageTest franchise = new AdminFranchiseLocationAddNewFranchiseLocationPageTest();
			franchise.addFranchiseLocation(driver, franchiseId, agreementVersion, config);

			String PLReportType = "csv";
			AdminFinanceFinanceSetupPreferencesPageTest setup = new AdminFinanceFinanceSetupPreferencesPageTest();
			setup.mediumOfSubmissionOfProfitAndLossReports(driver, config, PLReportType);

			fc.utobj().printTestStep("Navigate To Finance > Profit & Loss Statement > Profit / Loss Summary Page");
			fc.finance().finance_common().financeProfitAndLossStatementPage(driver);

			fc.utobj().printTestStep("Upload CSV file of Profit And Lost Statement");

			fc.utobj().clickElement(driver, pobj.uploadProfitLossStatement);

			fc.utobj().sendKeys(driver, pobj.clickBrowseBtnUploadFile, "C:\\Selenium_Test_Input\\testData\\ProfitAndLossStatement.csv");

			fc.utobj().clickElement(driver, pobj.skipDataMapping);

			fc.utobj().clickElement(driver, pobj.contFileUploadBtn);

			fc.utobj().acceptAlertBox(driver);

			String parentWindow1 = driver.getWindowHandle();

			Set<String> handles1 = driver.getWindowHandles();

			for (String windowHandle1 : handles1) {
				
				if (!windowHandle1.equals(parentWindow1)) {
					
					driver.switchTo().window(windowHandle1);

				//	fc.utobj().clickElement(driver, pobj.uploadBtn);

					fc.utobj().clickElement(driver, pobj.okBtn);

					driver.switchTo().window(parentWindow1);
				}
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	
	@Test(groups = "finance_FreshDB")
	@TestCase(createdOn = "2018-06-15", updatedOn = "2018-06-20", testCaseId = "TC_FIN_Alert_at_PLS_Upload_Statement", testCaseDescription = "Verify alert for Profit and Lost Statement")
	public void alertMsgAtUploadPLSStatement() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		Map<String, String> dataSet = fc.utobj().readTestData("finance", testCaseId);
		String agrmntVrsnName = "AV";
		String royaltyPrcntg = "10";
		String advPercentage = "8";
		String royaltyAreaFranchiseValue = "3";
		String advAreaFranchiseValue = "2";

		String categoryName = "TestCategory";

		try {
			driver = fc.loginpage().login(driver);
			FinanceProfitLossStatementPage pobj = new FinanceProfitLossStatementPage(driver);

			fc.utobj().printTestStep("Navigate To Admin > Finance > Configure Profit & Loss Categories Page");
			fc.utobj().printTestStep("Add Main Category");

			categoryName = new AdminFinanceProfitLossCategoriesPageTest().addNewMainCategory(driver, categoryName);

			fc.utobj().printTestStep("Navigate To Admin > Finance > Agreement Versions");
			AdminFinanceAgreementVersionsPageTest agreementforSales = new AdminFinanceAgreementVersionsPageTest();

			fc.utobj().printTestStep("Add Agreement");
			String agreementVersion = agreementforSales.addAgreement(driver, config, agrmntVrsnName, royaltyPrcntg,
					advPercentage, royaltyAreaFranchiseValue, advAreaFranchiseValue);

			String franchiseId = "001A";

			AdminFranchiseLocationAddNewFranchiseLocationPageTest franchise = new AdminFranchiseLocationAddNewFranchiseLocationPageTest();
			franchiseId = franchise.addFranchiseLocation(driver, franchiseId, agreementVersion, config);

			String PLReportType = "CSV";
			AdminFinanceFinanceSetupPreferencesPageTest setup = new AdminFinanceFinanceSetupPreferencesPageTest();
			setup.mediumOfSubmissionOfProfitAndLossReports(driver, config, PLReportType);

			fc.utobj().printTestStep("Navigate To Finance > Profit & Loss Statement > Profit / Loss Summary Page");
			fc.finance().finance_common().financeProfitAndLossStatementPage(driver);

			fc.utobj().printTestStep("Try to upload Profit And Lost Statement");

			fc.utobj().clickElement(driver, pobj.uploadProfitLossStatement);

			fc.utobj().sendKeys(driver, pobj.clickBrowseBtnUploadFile,
					"C:\\Selenium_Test_Input\\testData\\ProfitAndLossStatement.xls");

			fc.utobj().clickElement(driver, pobj.contFileUploadBtn);

			String expectedAlert = "Format of the profit loss data file is not as per the selected file format.";

			String actualAlert = fc.utobj().acceptAlertBox(driver);

			assertEquals(actualAlert, expectedAlert);

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "finance11111")
	@TestCase(createdOn = "2018-06-22", updatedOn = "2018-06-22", testCaseId = "TC_FC_Download_of_Sample_File_XLS_CSV_PL", testCaseDescription = "To verify download of sample file for Upload Profit & Loss Statement")
	public void verifyDownloadSampleFilePLStatement() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());
		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("finance", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			FinanceProfitLossStatementPage pobj = new FinanceProfitLossStatementPage(driver);

			AdminFinanceFinanceSetupPreferencesPageTest setup = new AdminFinanceFinanceSetupPreferencesPageTest();

			String PLReportType = "XLS";
			setup.mediumOfSubmissionOfProfitAndLossReports(driver, config, PLReportType);

			fc.utobj().printTestStep("Navigate To Finance > Profit & Loss Statement > Profit / Loss Summary Page");
			fc.finance().finance_common().financeProfitAndLossStatementPage(driver);

			fc.utobj().printTestStep("Download XLS file of Profit And Lost Statement");

			fc.utobj().clickElement(driver, pobj.uploadProfitLossStatement);

			fc.utobj().clickElement(driver, pobj.downloadSample);

			boolean isFileFoundXLS = fc.utobj().isFileFound(new File(FranconnectUtil.config.get("downloadFilepath")),
					"importProfitLossFormat.xls");

			if (isFileFoundXLS == false) {

				fc.utobj().throwsException("Downloaded XLS PL file is not found");
			}

			PLReportType = "csv";

			setup.mediumOfSubmissionOfProfitAndLossReports(driver, config, PLReportType);

			fc.utobj().printTestStep("Navigate To Finance > Profit & Loss Statement > Profit / Loss Summary Page");

			fc.finance().finance_common().financeProfitAndLossStatementPage(driver);

			fc.utobj().printTestStep("Upload CSV file of Profit And Lost Statement");

			fc.utobj().clickElement(driver, pobj.uploadProfitLossStatement);

			fc.utobj().clickElement(driver, pobj.downloadSample);

			boolean isFileFoundCSV = fc.utobj().isFileFound(new File(FranconnectUtil.config.get("downloadFilepath")),
					"profitLossDataSample.csv");

			if (isFileFoundCSV == false) {

				fc.utobj().throwsException("Downloaded CSV PL file is not found");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

}
