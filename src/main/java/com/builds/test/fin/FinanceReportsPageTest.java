package com.builds.test.fin;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.Test;

import com.builds.test.support.SupportReportsPageTest;
import com.builds.utilities.FranconnectUtil;
import com.builds.utilities.TestCase;

public class FinanceReportsPageTest extends FranconnectUtil {

	FranconnectUtil fc = new FranconnectUtil();
	SupportReportsPageTest rpReport = new SupportReportsPageTest();

	@Test(groups = { "financeReports" })

	@TestCase(createdOn = "2017-08-01", updatedOn = "2017-08-01", testCaseDescription = "Click Over Franchise Activity (FRAC) Report", testCaseId = "TC_Finance_Report_01")
	private void financeReports01() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		// Map<String,String> dataSet = fc.utobj().readTestData("finance",
		// testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			fc.utobj().printTestStep("Navigate To Finance > Reports Page > Franchise Activity (FRAC) Report");
			fc.finance().finance_common().financeReportsPage(driver);
			rpReport.navigateToSessionSizePage(driver);
			fc.utobj().clickElement(driver,
					driver.findElement(By.xpath(".//a[contains(text () ,'Franchise Activity (FRAC) Report')]")));
			rpReport.setSelectAllMultiDropDown(driver);
			rpReport.navigateToSessionSizePage(driver);

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "financeReports" })

	@TestCase(createdOn = "2017-08-01", updatedOn = "2017-08-01", testCaseDescription = "Click Over FRAC Year To Year Comparison Report", testCaseId = "TC_Finance_Report_02")
	private void financeReports02() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		// Map<String,String> dataSet = fc.utobj().readTestData("finance",
		// testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			fc.utobj().printTestStep("Navigate To Finance > Reports Page > FRAC Year To Year Comparison Report");
			fc.finance().finance_common().financeReportsPage(driver);
			rpReport.navigateToSessionSizePage(driver);
			fc.utobj().clickElement(driver,
					driver.findElement(By.xpath(".//a[contains(text () ,'FRAC Year To Year Comparison Report')]")));
			rpReport.setSelectAllMultiDropDown(driver);
			rpReport.navigateToSessionSizePage(driver);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "financeReports" })

	@TestCase(createdOn = "2017-08-01", updatedOn = "2017-08-01", testCaseDescription = "Click Over Franchise Agreement Report", testCaseId = "TC_Finance_Report_03")
	private void financeReports03() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		// Map<String,String> dataSet = fc.utobj().readTestData("finance",
		// testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			fc.utobj().printTestStep("Navigate To Finance > Reports Page > Franchise Agreement Report");
			fc.finance().finance_common().financeReportsPage(driver);
			rpReport.navigateToSessionSizePage(driver);
			fc.utobj().clickElement(driver,
					driver.findElement(By.xpath(".//a[contains(text () ,'Franchise Agreement Report')]")));
			rpReport.setSelectAllMultiDropDown(driver);
			rpReport.navigateToSessionSizePage(driver);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "financeReports" })

	@TestCase(createdOn = "2017-08-01", updatedOn = "2017-08-01", testCaseDescription = "Click Over No Sales Report", testCaseId = "TC_Finance_Report_04")
	private void financeReports04() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		// Map<String,String> dataSet = fc.utobj().readTestData("finance",
		// testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			fc.utobj().printTestStep("Navigate To Finance > Reports Page > No Sales Report");
			fc.finance().finance_common().financeReportsPage(driver);
			rpReport.navigateToSessionSizePage(driver);
			fc.utobj().clickElement(driver, driver.findElement(By.xpath(".//a[contains(text () ,'No Sales Report')]")));
			rpReport.setSelectAllMultiDropDown(driver);
			rpReport.navigateToSessionSizePage(driver);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "financeReports" })

	@TestCase(createdOn = "2017-08-01", updatedOn = "2017-08-01", testCaseDescription = "Click Over Sales Ranking Report", testCaseId = "TC_Finance_Report_05")
	private void financeReports05() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		// Map<String,String> dataSet = fc.utobj().readTestData("finance",
		// testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			fc.utobj().printTestStep("Navigate To Finance > Reports Page > Sales Ranking Report");
			fc.finance().finance_common().financeReportsPage(driver);
			rpReport.navigateToSessionSizePage(driver);
			fc.utobj().clickElement(driver,
					driver.findElement(By.xpath(".//a[contains(text () ,'Sales Ranking Report')]")));
			rpReport.setSelectAllMultiDropDown(driver);
			rpReport.navigateToSessionSizePage(driver);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "financeReports" })

	@TestCase(createdOn = "2017-08-01", updatedOn = "2017-08-01", testCaseDescription = "Click Over Area Franchise Ranking Report", testCaseId = "TC_Finance_Report_06")
	private void financeReports06() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		// Map<String,String> dataSet = fc.utobj().readTestData("finance",
		// testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			fc.utobj().printTestStep("Navigate To Finance > Reports Page > Area Franchise Ranking Report");
			fc.finance().finance_common().financeReportsPage(driver);
			rpReport.navigateToSessionSizePage(driver);
			fc.utobj().clickElement(driver,
					driver.findElement(By.xpath(".//a[contains(text () ,'Area Franchise Ranking Report')]")));
			rpReport.setSelectAllMultiDropDown(driver);
			rpReport.navigateToSessionSizePage(driver);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "financeReports" })

	@TestCase(createdOn = "2017-08-01", updatedOn = "2017-08-01", testCaseDescription = "Click Over A / R Aging Summary Report", testCaseId = "TC_Finance_Report_07")
	private void financeReports07() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		// Map<String,String> dataSet = fc.utobj().readTestData("finance",
		// testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			fc.utobj().printTestStep("Navigate To Finance > Reports Page > A / R Aging Summary Report");
			fc.finance().finance_common().financeReportsPage(driver);
			rpReport.navigateToSessionSizePage(driver);
			fc.utobj().clickElement(driver,
					driver.findElement(By.xpath(".//a[contains(text () ,'A / R Aging Summary Report')]")));
			rpReport.setSelectAllMultiDropDown(driver);
			rpReport.navigateToSessionSizePage(driver);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "financeReports" })

	@TestCase(createdOn = "2017-08-01", updatedOn = "2017-08-01", testCaseDescription = "Click Over Total Sales Report", testCaseId = "TC_Finance_Report_08")
	private void financeReports08() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		// Map<String,String> dataSet = fc.utobj().readTestData("finance",
		// testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			fc.utobj().printTestStep("Navigate To Finance > Reports Page > Total Sales Report");
			fc.finance().finance_common().financeReportsPage(driver);
			rpReport.navigateToSessionSizePage(driver);
			fc.utobj().clickElement(driver,
					driver.findElement(By.xpath(".//a[contains(text () ,'Total Sales Report')]")));
			rpReport.setSelectAllMultiDropDown(driver);
			rpReport.navigateToSessionSizePage(driver);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "financeReports" })

	@TestCase(createdOn = "2017-08-01", updatedOn = "2017-08-01", testCaseDescription = "Click Over Franchise Metrics", testCaseId = "TC_Finance_Report_09")
	private void financeReports09() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		// Map<String,String> dataSet = fc.utobj().readTestData("finance",
		// testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			fc.utobj().printTestStep("Navigate To Finance > Reports Page > Franchise Metrics");
			fc.finance().finance_common().financeReportsPage(driver);
			rpReport.navigateToSessionSizePage(driver);
			fc.utobj().clickElement(driver,
					driver.findElement(By.xpath(".//a[contains(text () ,'Franchise Metrics')]")));
			rpReport.setSelectAllMultiDropDown(driver);
			rpReport.navigateToSessionSizePage(driver);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "financeReports" })

	@TestCase(createdOn = "2017-08-01", updatedOn = "2017-08-01", testCaseDescription = "Click Over Profit / Loss Statement Report", testCaseId = "TC_Finance_Report_10")
	private void financeReports10() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		// Map<String,String> dataSet = fc.utobj().readTestData("finance",
		// testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			fc.utobj().printTestStep("Navigate To Finance > Reports Page > Profit / Loss Statement Report");
			fc.finance().finance_common().financeReportsPage(driver);
			rpReport.navigateToSessionSizePage(driver);
			fc.utobj().clickElement(driver,
					driver.findElement(By.xpath(".//a[contains(text () ,'Profit / Loss Statement Report')]")));
			rpReport.setSelectAllMultiDropDown(driver);
			rpReport.navigateToSessionSizePage(driver);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "financeReports" })

	@TestCase(createdOn = "2017-08-01", updatedOn = "2017-08-01", testCaseDescription = "Click Over ACH Report", testCaseId = "TC_Finance_Report_11")
	private void financeReports11() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		// Map<String,String> dataSet = fc.utobj().readTestData("finance",
		// testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			fc.utobj().printTestStep("Navigate To Finance > Reports Page > ACH Report");
			fc.finance().finance_common().financeReportsPage(driver);
			rpReport.navigateToSessionSizePage(driver);
			fc.utobj().clickElement(driver, driver.findElement(By.xpath(".//a[contains(text () ,'ACH Report')]")));
			rpReport.setSelectAllMultiDropDown(driver);
			rpReport.navigateToSessionSizePage(driver);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "financeReports" })

	@TestCase(createdOn = "2017-08-01", updatedOn = "2017-08-01", testCaseDescription = "Click Over Performance Report", testCaseId = "TC_Finance_Report_12")
	private void financeReports12() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		// Map<String,String> dataSet = fc.utobj().readTestData("finance",
		// testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			fc.utobj().printTestStep("Navigate To Finance > Reports Page > Performance Report");
			fc.finance().finance_common().financeReportsPage(driver);
			rpReport.navigateToSessionSizePage(driver);
			fc.utobj().clickElement(driver,
					driver.findElement(By.xpath(".//a[contains(text () ,'Performance Report')]")));
			rpReport.setSelectAllMultiDropDown(driver);
			rpReport.navigateToSessionSizePage(driver);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "financeReports" })

	@TestCase(createdOn = "2017-08-01", updatedOn = "2017-08-01", testCaseDescription = "Click Over Category Sales Report for Specified Franchise(s)", testCaseId = "TC_Finance_Report_13")
	private void financeReports13() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		// Map<String,String> dataSet = fc.utobj().readTestData("finance",
		// testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			fc.utobj().printTestStep(
					"Navigate To Finance > Reports Page > Category Sales Report for Specified Franchise (s)");
			fc.finance().finance_common().financeReportsPage(driver);
			rpReport.navigateToSessionSizePage(driver);
			fc.utobj().clickElement(driver, driver
					.findElement(By.xpath(".//a[contains(text () ,'Category Sales Report for Specified Franchise')]")));
			rpReport.setSelectAllMultiDropDown(driver);
			rpReport.navigateToSessionSizePage(driver);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "financeReports" })

	@TestCase(createdOn = "2017-08-01", updatedOn = "2017-08-01", testCaseDescription = "Click Over Category Sales Report for Specified Area / Region", testCaseId = "TC_Finance_Report_14")
	private void financeReports14() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		// Map<String,String> dataSet = fc.utobj().readTestData("finance",
		// testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			fc.utobj().printTestStep(
					"Navigate To Finance > Reports Page > Category Sales Report for Specified Area / Region");
			fc.finance().finance_common().financeReportsPage(driver);
			rpReport.navigateToSessionSizePage(driver);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,(".//a[contains(text () ,'Category Sales Report for Specified Area / Region')]")));
			rpReport.setSelectAllMultiDropDown(driver);
			rpReport.navigateToSessionSizePage(driver);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "financeReports" })

	@TestCase(createdOn = "2017-08-01", updatedOn = "2017-08-01", testCaseDescription = "Click Over Category Sales Report for Specified State(s) / Province(s)", testCaseId = "TC_Finance_Report_15")
	private void financeReports15() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		// Map<String,String> dataSet = fc.utobj().readTestData("finance",
		// testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			fc.utobj().printTestStep(
					"Navigate To Finance > Reports Page > Category Sales Report for Specified State(s) / Province(s)");
			fc.finance().finance_common().financeReportsPage(driver);
			rpReport.navigateToSessionSizePage(driver);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,(".//a[contains(text () ,'Category Sales Report for Specified State(s) / Province(s)')]")));
			rpReport.setSelectAllMultiDropDown(driver);
			rpReport.navigateToSessionSizePage(driver);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "financeReports" })

	@TestCase(createdOn = "2017-08-01", updatedOn = "2017-08-01", testCaseDescription = "Click Over Royalty Report", testCaseId = "TC_Finance_Report_16")
	private void financeReports16() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		// Map<String,String> dataSet = fc.utobj().readTestData("finance",
		// testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			fc.utobj().printTestStep("Navigate To Finance > Reports Page > Royalty Report");
			fc.finance().finance_common().financeReportsPage(driver);
			rpReport.navigateToSessionSizePage(driver);
			fc.utobj().clickElement(driver, driver.findElement(By.xpath(".//a[contains(text () ,'Royalty Report')]")));
			rpReport.setSelectAllMultiDropDown(driver);
			rpReport.navigateToSessionSizePage(driver);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "financeReports" })

	@TestCase(createdOn = "2017-08-01", updatedOn = "2017-08-01", testCaseDescription = "Click Over Royalty Report By Store Type", testCaseId = "TC_Finance_Report_17")
	private void financeReports17() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		// Map<String,String> dataSet = fc.utobj().readTestData("finance",
		// testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			fc.utobj().printTestStep("Navigate To Finance > Reports Page > Royalty Report By Store Type");
			fc.finance().finance_common().financeReportsPage(driver);
			rpReport.navigateToSessionSizePage(driver);
			fc.utobj().clickElement(driver,
					driver.findElement(By.xpath(".//a[contains(text () ,'Royalty Report By Store Type')]")));
			rpReport.setSelectAllMultiDropDown(driver);
			rpReport.navigateToSessionSizePage(driver);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "financeReports" })

	@TestCase(createdOn = "2017-08-01", updatedOn = "2017-08-01", testCaseDescription = "Click Over Royalty Year To Year Comparison Report", testCaseId = "TC_Finance_Report_18")
	private void financeReports18() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		// Map<String,String> dataSet = fc.utobj().readTestData("finance",
		// testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			fc.utobj().printTestStep("Navigate To Finance > Reports Page > Royalty Year To Year Comparison Report");
			fc.finance().finance_common().financeReportsPage(driver);
			rpReport.navigateToSessionSizePage(driver);
			fc.utobj().clickElement(driver,
					driver.findElement(By.xpath(".//a[contains(text () ,'Royalty Year To Year Comparison Report')]")));
			rpReport.setSelectAllMultiDropDown(driver);
			rpReport.navigateToSessionSizePage(driver);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "financeReports" })

	@TestCase(createdOn = "2017-08-01", updatedOn = "2017-08-01", testCaseDescription = "Click Over Royalty Year To Year Comparison Report By Store Type", testCaseId = "TC_Finance_Report_19")
	private void financeReports19() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		// Map<String,String> dataSet = fc.utobj().readTestData("finance",
		// testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			fc.utobj().printTestStep(
					"Navigate To Finance > Reports Page > Royalty Year To Year Comparison Report By Store Type");
			fc.finance().finance_common().financeReportsPage(driver);
			rpReport.navigateToSessionSizePage(driver);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,(".//a[contains(text () ,'Royalty Year To Year Comparison Report By Store Type')]")));
			rpReport.setSelectAllMultiDropDown(driver);
			rpReport.navigateToSessionSizePage(driver);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "financeReports" })

	@TestCase(createdOn = "2017-08-01", updatedOn = "2017-08-01", testCaseDescription = "Click Over Royalty Ranking Report", testCaseId = "TC_Finance_Report_20")
	private void financeReports20() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		// Map<String,String> dataSet = fc.utobj().readTestData("finance",
		// testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			fc.utobj().printTestStep("Navigate To Finance > Reports Page > Royalty Ranking Report");
			fc.finance().finance_common().financeReportsPage(driver);
			rpReport.navigateToSessionSizePage(driver);
			fc.utobj().clickElement(driver,
					driver.findElement(By.xpath(".//a[contains(text () ,'Royalty Ranking Report')]")));
			rpReport.setSelectAllMultiDropDown(driver);
			rpReport.navigateToSessionSizePage(driver);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "financeReports" })

	@TestCase(createdOn = "2017-08-01", updatedOn = "2017-08-01", testCaseDescription = "Click Over Invoices Fully Paid Report", testCaseId = "TC_Finance_Report_21")
	private void financeReports21() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		// Map<String,String> dataSet = fc.utobj().readTestData("finance",
		// testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			fc.utobj().printTestStep("Navigate To Finance > Reports Page > Invoices Fully Paid Report");
			fc.finance().finance_common().financeReportsPage(driver);
			rpReport.navigateToSessionSizePage(driver);
			fc.utobj().clickElement(driver,
					driver.findElement(By.xpath(".//a[contains(text () ,'Invoices Fully Paid Report')]")));
			rpReport.setSelectAllMultiDropDown(driver);
			rpReport.navigateToSessionSizePage(driver);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "financeReports" })

	@TestCase(createdOn = "2017-08-01", updatedOn = "2017-08-01", testCaseDescription = "Click Over Exception Report", testCaseId = "TC_Finance_Report_22")
	private void financeReports22() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		// Map<String,String> dataSet = fc.utobj().readTestData("finance",
		// testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			fc.utobj().printTestStep("Navigate To Finance > Reports Page > Exception Report");
			fc.finance().finance_common().financeReportsPage(driver);
			rpReport.navigateToSessionSizePage(driver);
			fc.utobj().clickElement(driver,
					driver.findElement(By.xpath(".//a[contains(text () ,'Exception Report')]")));
			rpReport.setSelectAllMultiDropDown(driver);
			rpReport.navigateToSessionSizePage(driver);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "financeReports" })

	@TestCase(createdOn = "2017-08-01", updatedOn = "2017-08-01", testCaseDescription = "Click Over A / R Aging Detail Report", testCaseId = "TC_Finance_Report_23")
	private void financeReports23() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		// Map<String,String> dataSet = fc.utobj().readTestData("finance",
		// testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			fc.utobj().printTestStep("Navigate To Finance > Reports Page > A / R Aging Detail Report");
			fc.finance().finance_common().financeReportsPage(driver);
			rpReport.navigateToSessionSizePage(driver);
			fc.utobj().clickElement(driver,
					driver.findElement(By.xpath(".//a[contains(text () ,'A / R Aging Detail Report')]")));
			rpReport.setSelectAllMultiDropDown(driver);
			rpReport.navigateToSessionSizePage(driver);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}
}
