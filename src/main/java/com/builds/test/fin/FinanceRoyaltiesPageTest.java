package com.builds.test.fin;

import java.text.DecimalFormat;
import java.util.Map;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.Test;
import com.builds.test.admin.AdminFranchiseLocationAddFranchiseLocationPageTest;
import com.builds.uimaps.fin.FinancePaymentsPage;
import com.builds.uimaps.fin.FinanceRoyaltiesPage;
import com.builds.uimaps.fin.FinanceSalesPage;
import com.builds.utilities.FranconnectUtil;
import com.builds.utilities.TestCase;


public class FinanceRoyaltiesPageTest {
	FranconnectUtil fc = new FranconnectUtil();

	@Test(groups = "finance")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-06", testCaseId = "TC_FC_View_Invoice_Details_Page_095", testCaseDescription = "Verify View Invoice Details Page")
	public void viewInvoiceDetailsPage() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		Map<String, String> dataSet = fc.utobj().readTestData("finance", testCaseId);

		String agrmntVrsnName = fc.utobj().generateTestData(dataSet.get("agreementVersionName"));
		String royaltyPrcntg = dataSet.get("royaltyPercentage");
		String advPercentage = dataSet.get("advertisementPercentage");
		String royaltyAreaFranchiseValue = dataSet.get("RoyaltyAreaFranchise");
		String advAreaFranchiseValue = dataSet.get("AdvAreaFranchise");
		String regionName = fc.utobj().generateTestData("GT");
		String storeType = fc.utobj().generateTestData("GT");
		String franchiseId = fc.utobj().generateTestData("GT");

		try {
			driver = fc.loginpage().login(driver);
			FinanceSalesPage sales_Page = new FinanceSalesPage(driver);
			FinanceSalesPageTest sale_pageTest = new FinanceSalesPageTest();

			fc.utobj().printTestStep("Navigate To Admin > Finance > Agreement Versions");
			AdminFinanceAgreementVersionsPageTest agreementforSales = new AdminFinanceAgreementVersionsPageTest();

			fc.utobj().printTestStep("Add Agreement");
			String agreementVersion = agreementforSales.addAgreement(driver, config, agrmntVrsnName,
					 royaltyPrcntg, advPercentage, royaltyAreaFranchiseValue, advAreaFranchiseValue);

			fc.utobj().printTestStep("Navigate To Admin > Franchise Location > Add Franchise Location");
			fc.utobj().printTestStep("Add Franchise Location");

			AdminFranchiseLocationAddFranchiseLocationPageTest franchise = new AdminFranchiseLocationAddFranchiseLocationPageTest();
			franchiseId = franchise.addFranchiseLocation_All_Agreement(driver, franchiseId, regionName, storeType,
					agreementVersion, config);

			fc.utobj().printTestStep("Navigate To Admin > Finance > Finance Setup Preferences");
			fc.utobj().printTestStep("Setup Medium of submission of Sales Reports");
			String salesReportType = "webForm";
			new AdminFinanceFinanceSetupPreferencesPageTest().mediumOfSubmissionOfSalesReports(driver, config,
					salesReportType);

			fc.utobj().printTestStep("Set Invoice Frequency");
			new AdminFinanceFinanceSetupPreferencesPageTest().setInvoice(driver, config);

			fc.utobj().printTestStep("Navigate To Finance > Sales");
			fc.utobj().printTestStep("Enter Sales Report");
			String categoryQuantity = "110";
			String categoryAmount = "6000";
			String reportId = sale_pageTest.enterSalesReport(driver, franchiseId, categoryQuantity,
					categoryAmount);

			fc.utobj().printTestStep("Filter Sales Report By Franchise Id");
			sale_pageTest.filterSalesReport(driver, franchiseId);

			fc.utobj().printTestStep("Approve Sales Report");
			fc.utobj().actionImgOption(driver, reportId, "Approve");

			fc.commonMethods().switch_cboxIframe_frameId(driver);
			
			fc.utobj().sendKeys(driver, sales_Page.commentTxtArea,
					"Test Approve Comments" + fc.utobj().generateRandomNumber());
			
			fc.utobj().clickElement(driver, sales_Page.SaveBtn);

			fc.utobj().clickElement(driver, sales_Page.cloaseBtn);
			
			fc.utobj().switchFrameToDefault(driver);
			
			fc.utobj().printTestStep("Filter Sales Report By Franchise Id");
			
			sale_pageTest.filterSalesReport(driver, franchiseId);
			
			fc.utobj().printTestStep("Generate Invoice");
			
			fc.utobj().actionImgOption(driver, franchiseId, "Generate Invoice");

			fc.utobj().printTestStep("Navigate To Finance > Royalties");
			
			fc.finance().finance_common().financeRoyaltiesPage(driver);

			fc.utobj().printTestStep("Filter Report By Franchise Id");
			
			sale_pageTest.filterSalesReport(driver, franchiseId);

			fc.utobj().printTestStep("View Invoice Details");
			
			fc.utobj().actionImgOption(driver, franchiseId, "View Invoice Details");

			fc.utobj().printTestStep("Verify The Royalty Amount");
			
			double royaltyVal = Double.parseDouble(categoryAmount) * Double.parseDouble(royaltyPrcntg) / 100;

			String royaltyValAtInvoice = fc.utobj().getText(driver, driver.findElement(By.xpath(
					".//*[contains(text () , 'Invoice Report Details')]/ancestor::tr[2]//td[contains(text () , 'Royalty')]/following-sibling::td")));

			if (royaltyValAtInvoice.contains(",")) {
				royaltyValAtInvoice = royaltyValAtInvoice.replaceAll(",", "");
			}

			if (royaltyVal != Double.parseDouble(royaltyValAtInvoice)) {
				
				fc.utobj().throwsException("was not able to verify the royality value");
			}

			fc.utobj().printTestStep("Verify The Adv. Amount");
			
			double advVal = Double.parseDouble(categoryAmount) * Double.parseDouble(advPercentage) / 100;

			String AdvValAtInvoice = fc.utobj().getText(driver, driver.findElement(By.xpath(
					".//*[contains(text () , 'Invoice Report Details')]/ancestor::tr[2]//td[contains(text () , 'Adv')]/following-sibling::td")));

			if (AdvValAtInvoice.contains(",")) {
				
				AdvValAtInvoice = AdvValAtInvoice.replaceAll(",", "");
			}

			if (advVal != Double.parseDouble(AdvValAtInvoice)) {
				
				fc.utobj().throwsException("was not able to verify the Adv. value");
			}

			fc.utobj().printTestStep("Verify The Invoice Amount");

			String InvoiceAmountAtInvoice = fc.utobj().getText(driver, driver.findElement(By.xpath(
					".//*[contains(text () , 'Invoice Report Details')]/ancestor::tr[2]//td[contains(text () , 'Invoice Amount')]/following-sibling::td")));

			if (InvoiceAmountAtInvoice.contains(",")) {
				
				InvoiceAmountAtInvoice = InvoiceAmountAtInvoice.replaceAll(",", "");
			}

			double invoiceValueCal = royaltyVal + advVal;

			if (invoiceValueCal != Double.parseDouble(InvoiceAmountAtInvoice)) {
				
				fc.utobj().throwsException("was not able to verify the invoice amount at Invoice Report Details page");
			}					
			
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "finance")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2017-06-09", testCaseId = "TC_FC_Apply_Payment_Royalties_053", testCaseDescription = "Verify Apply Payment For Royalties")
	public void applyPaymentRoyalties() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("finance", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		String agrmntVrsnName = fc.utobj().generateTestData(dataSet.get("agreementVersionName"));
		String royaltyPrcntg = dataSet.get("royaltyPercentage");
		String advPercentage = dataSet.get("advertisementPercentage");
		String royaltyAreaFranchiseValue = dataSet.get("RoyaltyAreaFranchise");
		String advAreaFranchiseValue = dataSet.get("AdvAreaFranchise");
		
		try {
			driver = fc.loginpage().login(driver);
			FinanceRoyaltiesPage pobj = new FinanceRoyaltiesPage(driver);
			FinanceSalesPage sales_Page = new FinanceSalesPage(driver);
			FinanceSalesPageTest sale_pageTest = new FinanceSalesPageTest();

			fc.utobj().printTestStep("Navigate To Admin > Finance > Agreement Versions");
			AdminFinanceAgreementVersionsPageTest agreementforSales = new AdminFinanceAgreementVersionsPageTest();

			fc.utobj().printTestStep("Add Agreement");
			String agreementVersion = agreementforSales.addAgreement(driver, config, agrmntVrsnName,
					 royaltyPrcntg, advPercentage, royaltyAreaFranchiseValue, advAreaFranchiseValue);

			fc.utobj().printTestStep("Navigate To Admin > Franchise Location > Add Franchise Location");
			fc.utobj().printTestStep("Add Franchise Location");

			String regionName = fc.utobj().generateTestData("GT");
			String storeType = fc.utobj().generateTestData("GT");
			String franchiseId = fc.utobj().generateTestData("GT");
			AdminFranchiseLocationAddFranchiseLocationPageTest franchise = new AdminFranchiseLocationAddFranchiseLocationPageTest();
			franchiseId = franchise.addFranchiseLocation_All_Agreement(driver, franchiseId, regionName, storeType,
					agreementVersion, config);

			fc.utobj().printTestStep("Navigate To Admin > Finance > Finance Setup Preferences");
			fc.utobj().printTestStep("Setup Medium of submission of Sales Reports");
			String salesReportType = "webForm";
			new AdminFinanceFinanceSetupPreferencesPageTest().mediumOfSubmissionOfSalesReports(driver, config,
					salesReportType);

			fc.utobj().printTestStep("Set Invoice Frequency");
			new AdminFinanceFinanceSetupPreferencesPageTest().setInvoice(driver, config);

			fc.utobj().printTestStep("Navigate To Finance > Sales");
			fc.utobj().printTestStep("Enter Sales Report");
			String categoryQuantity = "110";
			String categoryAmount = "6000";
			String reportId = new FinanceSalesPageTest().enterSalesReport(driver, franchiseId, categoryQuantity,
					categoryAmount);

			fc.utobj().printTestStep("Filter Sales Report By Franchise Id");
			sale_pageTest.filterSalesReport(driver, franchiseId);

			fc.utobj().printTestStep("Approve Sales Report");
			fc.utobj().actionImgOption(driver, reportId, "Approve");

			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().sendKeys(driver, sales_Page.commentTxtArea,
					"Test Approve Comments" + fc.utobj().generateRandomNumber());
			fc.utobj().clickElement(driver, sales_Page.SaveBtn);

			fc.utobj().clickElement(driver, sales_Page.cloaseBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Generate Invoice");
			fc.utobj().printTestStep("Filter Sales Report By Franchise Id");
			sale_pageTest.filterSalesReport(driver, franchiseId);

			fc.utobj().printTestStep("Generate Invoice");
			fc.utobj().actionImgOption(driver, franchiseId, "Generate Invoice");

			fc.utobj().printTestStep("Navigate To Finance > Royalties");
			fc.finance().finance_common().financeRoyaltiesPage(driver);

			fc.utobj().printTestStep("Filter Sales Report By Franchise Id");
			sale_pageTest.filterSalesReport(driver, franchiseId);

			String invoiceID = fc.utobj().getText(driver, driver.findElement(By.xpath(".//a[contains(text () , '" + franchiseId + "')]/ancestor::tr/td[2]/a[1]")));

			fc.utobj().printTestStep("Apply Payment For Royalty");
			fc.utobj().actionImgOption(driver, franchiseId, "Apply Payment");

			if (!fc.utobj().isSelected(driver, pobj.selectPaymentAppliedforRoyalty)) {
				
				fc.utobj().clickElement(driver, pobj.selectPaymentAppliedforRoyalty);
			}
			fc.utobj().clickElement(driver, pobj.continueBtn);

			fc.utobj().printTestStep("Apply Payment By Cash Option");

			FinancePaymentsPage payments_page = new FinancePaymentsPage(driver);
			
			fc.utobj().sendKeys(driver, payments_page.recievedDate, fc.utobj().getCurrentDateUSFormat());

			double royaltyVal = Double.parseDouble(categoryAmount) * Double.parseDouble(royaltyPrcntg) / 100;
			
			fc.utobj().sendKeys(driver, pobj.paymentAmount, Double.toString(royaltyVal));
			
			fc.utobj().printTestStep("Test Ref No" + fc.utobj().generateRandomChar());

			if (!fc.utobj().isSelected(driver, payments_page.collectionMethodsCash)) {
				
				fc.utobj().clickElement(driver, payments_page.collectionMethodsCash);
			}

			fc.utobj().sendKeys(driver, payments_page.comments, "Test Comments" + fc.utobj().generateRandomChar());
			
			fc.utobj().clickElement(driver, pobj.saveBtn);

			fc.utobj().printTestStep("Veriy The Royality Amount Paid");
			
			fc.utobj().selectDropDownByValue(driver, payments_page.resultsPerPage, "500");
			
			String amountApplied = fc.utobj().getText(driver, driver.findElement(By.xpath(".//td[contains(text () , '" + franchiseId + "')]/ancestor::tr/td[4]")));

			if (amountApplied.contains(",")) {
				
				amountApplied = amountApplied.replaceAll(",", "");
			}

			if (royaltyVal != Double.parseDouble(amountApplied)) {
				
				fc.utobj().throwsException("was not able to verify The Royalty Paid");
			}

			fc.utobj().printTestStep("Navigate To Finance > Royalties");
			
			fc.finance().finance_common().financeRoyaltiesPage(driver);

			fc.utobj().printTestStep("Filter Sales Report By Franchise Id");
			
			new FinanceSalesPageTest().filterSalesReport(driver, franchiseId);

			fc.utobj().printTestStep("Verify Payment Status");
			
			boolean isTextPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[contains(text () ,'" + franchiseId
					+ "')]/ancestor::tr/td[contains(text () ,'Partially Paid')]");

			if (isTextPresent == false) {
				
				fc.utobj().throwsException("was not able to verify the Partially Paid Status Of Payments");
			}
			String openAmount = driver.findElement(By.xpath(".//a[contains(text () ,'"+franchiseId+"')]/ancestor::tr/td[9]")).getText();
			
			 if(openAmount.equalsIgnoreCase("0.00")) {
				 
				 fc.utobj().throwsException("Open balance is showing as Zero after partial payment");
			 }
			
			fc.utobj().printTestStep("Navigate To Finance > Royalties");
			fc.finance().finance_common().financeRoyaltiesPage(driver);

			fc.utobj().printTestStep("Filter Report By Franchise Id");
			sale_pageTest.filterSalesReport(driver, franchiseId);

			fc.utobj().printTestStep("Reverse Payment For Royalty");
			fc.utobj().actionImgOption(driver, franchiseId, "Reverse Payment");
			 
			fc.utobj().acceptAlertBox(driver);
			 
			sale_pageTest.filterSalesReport(driver, franchiseId);
			 
			boolean isTextPresent1 = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[contains(text () ,'"+franchiseId+"')]/ancestor::tr/td[contains(text () ,'Unpaid')]");
			
			if (isTextPresent1 == false) {
				
				fc.utobj().throwsException("was not able to verify unpaid Status of royalty after reverse payment");
			}
			
			//String franchiseId ="GTr11164306", invoiceID = "000245";
			fc.utobj().printTestStep("Navigate To Finance > Payments");
			fc.finance().finance_common().financePaymentsPage(driver);

			fc.utobj().printTestStep("Filter Payment Summary");
			
			fc.utobj().clickElement(driver, pobj.creditsDebitsLnk);
			
			fc.utobj().clickElement(driver, pobj.showFilterLnk);
			
			fc.utobj().clickElement(driver, driver.findElement(By.xpath("//div[@id='ms-parentfranchiseID']//button[@type='button']")));
			
			fc.utobj().clickElement(driver,	driver.findElement(By.xpath(".//div[@id='ms-parentfranchiseID']//input[@id='selectAll']")));
			
			fc.utobj().sendKeys(driver,	driver.findElement(By.xpath(".//div[@id='ms-parentfranchiseID']//input[@class='searchInputMultiple']")), franchiseId);
			
			driver.findElement(By.xpath(".//div[@id='ms-parentfranchiseID']//input[@class='searchInputMultiple']")).sendKeys(Keys.ENTER);
			
			fc.utobj().clickElement(driver,	driver.findElement(By.xpath(".//div[@id='ms-parentfranchiseID']//input[@id='selectAll']")));
			
			fc.utobj().clickElement(driver, driver.findElement(By.xpath("//div[@id='ms-parentfranchiseID']//button[@type='button']")));
			
			fc.utobj().clickElement(driver, pobj.searchFilterBtn);

			int rowCount = driver.findElements(By.xpath(".//div[@id='printReady']//table[@class='summaryTblex']/tbody/tr")).size();
			
			if (rowCount > 2) {
				
				fc.utobj().throwsException("Filters at Payment Summary page are not working as expected");
			}
			
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "finance")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2017-06-05", testCaseId = "TC_FC_View_Sales_Details", testCaseDescription = "Verify View Sales Details")
	public void viewSalesDetails() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		Map<String, String> dataSet = fc.utobj().readTestData("finance", testCaseId);

		String agrmntVrsnName = fc.utobj().generateTestData(dataSet.get("agreementVersionName"));
		String royaltyPrcntg = dataSet.get("royaltyPercentage");
		String advPercentage = dataSet.get("advertisementPercentage");
		String royaltyAreaFranchiseValue = dataSet.get("RoyaltyAreaFranchise");
		String advAreaFranchiseValue = dataSet.get("AdvAreaFranchise");
		
		try {
			driver = fc.loginpage().login(driver);
			
			FinanceSalesPageTest sales_pageTest = new FinanceSalesPageTest();
			FinanceSalesPage sales_page = new FinanceSalesPage(driver);

			fc.utobj().printTestStep("Navigate To Admin > Finance > Agreement Versions");
			AdminFinanceAgreementVersionsPageTest agreementforSales = new AdminFinanceAgreementVersionsPageTest();

			fc.utobj().printTestStep("Add Agreement");
			String agreementVersion = agreementforSales.addAgreement(driver, config, agrmntVrsnName,
					 royaltyPrcntg, advPercentage, royaltyAreaFranchiseValue, advAreaFranchiseValue);

			fc.utobj().printTestStep("Navigate To Admin > Franchise Location > Add Franchise Location");
			fc.utobj().printTestStep("Add Franchise Location");

			String regionName = fc.utobj().generateTestData("GT");
			String storeType = fc.utobj().generateTestData("GT");
			String franchiseId = fc.utobj().generateTestData("GT");
			AdminFranchiseLocationAddFranchiseLocationPageTest franchise = new AdminFranchiseLocationAddFranchiseLocationPageTest();
			franchiseId = franchise.addFranchiseLocation_All_Agreement(driver, franchiseId, regionName, storeType,
					agreementVersion, config);

			fc.utobj().printTestStep("Navigate To Admin > Finance > Finance Setup Preferences");
			fc.utobj().printTestStep("Setup Medium of submission of Sales Reports");
			String salesReportType = "webForm";
			new AdminFinanceFinanceSetupPreferencesPageTest().mediumOfSubmissionOfSalesReports(driver, config,
					salesReportType);

			fc.utobj().printTestStep("Navigate To Finance > Sales");
			fc.utobj().printTestStep("Enter Sales Report");
			String categoryQuantity = "110";
			String categoryAmount = "6000";
			String reportId = sales_pageTest.enterSalesReport(driver, franchiseId, categoryQuantity, categoryAmount);

			fc.utobj().printTestStep("Filter Sales Report By Franchise Id");
			sales_pageTest.filterSalesReport(driver, franchiseId);

			fc.utobj().printTestStep("Approve Sales Report");
			fc.utobj().actionImgOption(driver, reportId, "Approve");

			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().sendKeys(driver, sales_page.commentTxtArea,
					"Test Approve Comments" + fc.utobj().generateRandomNumber());
			fc.utobj().clickElement(driver, sales_page.SaveBtn);

			fc.utobj().clickElement(driver, sales_page.cloaseBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Filter Sales Report By Franchise Id");
			
			sales_pageTest.filterSalesReport(driver, franchiseId);

			fc.utobj().printTestStep("Generate Invoice");
			
			fc.utobj().actionImgOption(driver, franchiseId, "Generate Invoice");

			fc.utobj().printTestStep("Navigate To Finance > Royalties Page");
			
			//String franchiseId ="Testn06120555", reportId="000082";
			
			fc.finance().finance_common().financeRoyaltiesPage(driver);
			
			FinanceSalesPage pobjSalesPage = new FinanceSalesPage(driver);
			
				fc.utobj().clickElement(driver, pobjSalesPage.showFilter);
				
				//fc.utobj().setToDefault(driver, pobjSalesPage.selectFranchiseIdFilter);

				fc.utobj().selectValFromMultiSelect(driver, pobjSalesPage.selectFranchiseIdFilter, franchiseId);
				
				fc.utobj().clickElement(driver, pobjSalesPage.resetBtn);
				
				Select select = new Select(pobjSalesPage.selectFranchiseIdFilter);
				
				String franchiseFilterValue = select.getFirstSelectedOption().getText();
				
				if(!franchiseFilterValue.equalsIgnoreCase("All Selected")) {
					
					fc.utobj().throwsException("Reset button functionality is not working");
				}
				
				fc.utobj().clickElement(driver, pobjSalesPage.hideFilter);	
				
			fc.utobj().printTestStep("Filter Sales Report By Franchise Id");
			
			sales_pageTest.filterSalesReport(driver, franchiseId);

			fc.utobj().printTestStep("View Sales Report By Action Image Icon");
			
			fc.utobj().actionImgOption(driver, reportId, "View Sales Details");

			fc.utobj().printTestStep("Verify Total Sales Quantity");
			
			String totalQuantity = fc.utobj().getText(driver,
					driver.findElement(By.xpath(".//td[contains(text () ,' Total Sales')]/following-sibling::td[1]")));

			try {

				if (totalQuantity.contains(",")) {

					totalQuantity = totalQuantity.replaceAll(",", "");
				}

				float quantitiyValue = Float.parseFloat(totalQuantity);
				
				float categoryQuantityFloat = Float.parseFloat(categoryQuantity);

				if (quantitiyValue != categoryQuantityFloat) {

					fc.utobj().throwsException("was not able to verify Total Sales Quantitiy Value");
				}
			} catch (Exception e) {
				
				fc.utobj().throwsException("was not able to verify Total Sales Quantitiy Value");
			}

			fc.utobj().printTestStep("Verify Total Sales Amount");
			
			String totalAmount = fc.utobj().getText(driver,
					driver.findElement(By.xpath(".//td[contains(text () ,' Total Sales')]/following-sibling::td[2]")));

			try {

				if (totalAmount.contains(",")) {

					totalAmount = totalAmount.replaceAll(",", "");
				}

				float amountValue = Float.parseFloat(totalAmount);
				
				float categoryAmountFloat = Float.parseFloat(categoryAmount);

				if (amountValue != categoryAmountFloat) {

					fc.utobj().throwsException("was not able to verify Total Sales Amount Value");
				}
			} catch (Exception e) {
				fc.utobj().throwsException("was not able to verify Total Sales Amount Value");
			}
			
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "finance")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "TC_FC_Verify_Total_Sales_on_Royalties_Summary_Page_089", testCaseDescription = "Verify Total Sales Royalties Summary Page")
	public void verifyTotalSalesRoyaltiesSummaryPage() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		Map<String, String> dataSet = fc.utobj().readTestData("finance", testCaseId);

		String agrmntVrsnName = fc.utobj().generateTestData(dataSet.get("agreementVersionName"));
		String royaltyPrcntg = dataSet.get("royaltyPercentage");
		String advPercentage = dataSet.get("advertisementPercentage");
		String royaltyAreaFranchiseValue = dataSet.get("RoyaltyAreaFranchise");
		String advAreaFranchiseValue = dataSet.get("AdvAreaFranchise");
		
		try {
			driver = fc.loginpage().login(driver);

			FinanceSalesPage pobj = new FinanceSalesPage(driver);
			FinanceSalesPageTest sales_PageTest = new FinanceSalesPageTest();

			fc.utobj().printTestStep("Navigate To Admin > Finance > Agreement Versions");
			AdminFinanceAgreementVersionsPageTest agreementforSales = new AdminFinanceAgreementVersionsPageTest();

			fc.utobj().printTestStep("Add Agreement");
			String agreementVersion = agreementforSales.addAgreement(driver, config, agrmntVrsnName,
					 royaltyPrcntg, advPercentage, royaltyAreaFranchiseValue, advAreaFranchiseValue);

			fc.utobj().printTestStep("Navigate To Admin > Franchise Location > Add Franchise Location");
			fc.utobj().printTestStep("Add Franchise Location");

			String regionName = fc.utobj().generateTestData("GT");
			String storeType = fc.utobj().generateTestData("GT");
			String franchiseId = fc.utobj().generateTestData("GT");
			AdminFranchiseLocationAddFranchiseLocationPageTest franchise = new AdminFranchiseLocationAddFranchiseLocationPageTest();
			franchiseId = franchise.addFranchiseLocation_All_Agreement(driver, franchiseId, regionName, storeType,
					agreementVersion, config);

			fc.utobj().printTestStep("Navigate To Admin > Finance > Finance Setup Preferences");
			fc.utobj().printTestStep("Setup Medium of submission of Sales Reports");
			String salesReportType = "webForm";
			new AdminFinanceFinanceSetupPreferencesPageTest().mediumOfSubmissionOfSalesReports(driver, config,
					salesReportType);

			fc.utobj().printTestStep("Set Invoice Frequency");
			new AdminFinanceFinanceSetupPreferencesPageTest().setInvoice(driver, config);

			fc.utobj().printTestStep("Navigate To Finance > Sales");
			fc.utobj().printTestStep("Enter Sales Report");
			String categoryQuantity = "110";
			String categoryAmount = "6000";
			String reportId = sales_PageTest.enterSalesReport(driver, franchiseId, categoryQuantity, categoryAmount);

			fc.utobj().printTestStep("Filter Sales Report By Franchise Id");
			sales_PageTest.filterSalesReport(driver, franchiseId);

			fc.utobj().printTestStep("Approve Sales Report");
			fc.utobj().actionImgOption(driver, reportId, "Approve");

			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().sendKeys(driver, pobj.commentTxtArea,
					"Test Approve Comments " + fc.utobj().generateRandomNumber());
			fc.utobj().clickElement(driver, pobj.SaveBtn);

			fc.utobj().clickElement(driver, pobj.cloaseBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Filter Sales Report By Franchise Id");
			sales_PageTest.filterSalesReport(driver, franchiseId);

			fc.utobj().printTestStep("Generate Invoice");
			fc.utobj().actionImgOption(driver, franchiseId, "Generate Invoice");

			fc.utobj().printTestStep("Navigate To Finance > Royalties Page");
			fc.finance().finance_common().financeRoyaltiesPage(driver);

			fc.utobj().printTestStep("Filter Sales Report By Franchise Id");
			sales_PageTest.filterSalesReport(driver, franchiseId);

			fc.utobj().printTestStep("Verify total Sales on Royalty");

			String totalSalesAmount = fc.utobj().getText(driver, driver
					.findElement(By.xpath(".//a[contains(text () , '" + franchiseId + "')]/ancestor::tr/td[10]")));

			if (totalSalesAmount.contains(",")) {
				totalSalesAmount = totalSalesAmount.replaceAll(",", "");
			}

			double totalSalesAmnt = Double.parseDouble(totalSalesAmount);
			double enteredSalesAmount = Double.parseDouble(categoryAmount);
			DecimalFormat salesAmnt = new DecimalFormat("#.##");
			enteredSalesAmount = Double.valueOf(salesAmnt.format(enteredSalesAmount));

			if (totalSalesAmnt != enteredSalesAmount) {
				fc.utobj().throwsException(
						"Total Sales Amount at Royalty Summary Page is not same as Calculated Total Sales Amount! ");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "finance")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "TC_FC_Verify_Royalty_on_Royalties_Summary_Page_090", testCaseDescription = "To Verify Royalty On Royalties Summary Page")
	public void verifyRoyaltyOnRoyaltiesSummaryPage() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		Map<String, String> dataSet = fc.utobj().readTestData("finance", testCaseId);

		String agrmntVrsnName = fc.utobj().generateTestData(dataSet.get("agreementVersionName"));
		String royaltyPrcntg = dataSet.get("royaltyPercentage");
		String advPercentage = dataSet.get("advertisementPercentage");
		String royaltyAreaFranchiseValue = dataSet.get("RoyaltyAreaFranchise");
		String advAreaFranchiseValue = dataSet.get("AdvAreaFranchise");
		
		try {
			driver = fc.loginpage().login(driver);

			FinanceSalesPage pobj = new FinanceSalesPage(driver);
			FinanceSalesPageTest sales_PageTest = new FinanceSalesPageTest();

			fc.utobj().printTestStep("Navigate To Admin > Finance > Agreement Versions");
			AdminFinanceAgreementVersionsPageTest agreementforSales = new AdminFinanceAgreementVersionsPageTest();

			fc.utobj().printTestStep("Add Agreement");
			String agreementVersion = agreementforSales.addAgreement(driver, config, agrmntVrsnName,
					 royaltyPrcntg, advPercentage, royaltyAreaFranchiseValue, advAreaFranchiseValue);

			fc.utobj().printTestStep("Navigate To Admin > Franchise Location > Add Franchise Location");
			fc.utobj().printTestStep("Add Franchise Location");

			String regionName = fc.utobj().generateTestData("GT");
			String storeType = fc.utobj().generateTestData("GT");
			String franchiseId = fc.utobj().generateTestData("GT");
			AdminFranchiseLocationAddFranchiseLocationPageTest franchise = new AdminFranchiseLocationAddFranchiseLocationPageTest();
			franchiseId = franchise.addFranchiseLocation_All_Agreement(driver, franchiseId, regionName, storeType,
					agreementVersion, config);

			fc.utobj().printTestStep("Navigate To Admin > Finance > Finance Setup Preferences");
			fc.utobj().printTestStep("Setup Medium of submission of Sales Reports");
			String salesReportType = "webForm";
			new AdminFinanceFinanceSetupPreferencesPageTest().mediumOfSubmissionOfSalesReports(driver, config,
					salesReportType);

			fc.utobj().printTestStep("Set Invoice Frequency");
			new AdminFinanceFinanceSetupPreferencesPageTest().setInvoice(driver, config);

			fc.utobj().printTestStep("Navigate To Finance > Sales");
			fc.utobj().printTestStep("Enter Sales Report");
			String categoryQuantity = "110";
			String categoryAmount = "6000";
			String reportId = sales_PageTest.enterSalesReport(driver, franchiseId, categoryQuantity, categoryAmount);

			fc.utobj().printTestStep("Filter Sales Report By Franchise Id");
			sales_PageTest.filterSalesReport(driver, franchiseId);

			fc.utobj().printTestStep("Approve Sales Report");
			fc.utobj().actionImgOption(driver, reportId, "Approve");

			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().sendKeys(driver, pobj.commentTxtArea,
					"Test Approve Comments" + fc.utobj().generateRandomNumber());
			fc.utobj().clickElement(driver, pobj.SaveBtn);

			fc.utobj().clickElement(driver, pobj.cloaseBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Filter Sales Report By Franchise Id");
			sales_PageTest.filterSalesReport(driver, franchiseId);

			fc.utobj().printTestStep("Generate Invoice");
			fc.utobj().actionImgOption(driver, franchiseId, "Generate Invoice");

			fc.utobj().printTestStep("Navigate To Finance > Royalties Page");
			fc.finance().finance_common().financeRoyaltiesPage(driver);

			fc.utobj().printTestStep("Filter Sales Report By Franchise Id");
			sales_PageTest.filterSalesReport(driver, franchiseId);

			fc.utobj().printTestStep("Verify Royalty At Royalty Page");

			double enteredSalesAmount = Double.parseDouble(categoryAmount);
			double royaltyPercent = Double.parseDouble(royaltyPrcntg);
			double calRoyaltyAmt = ((enteredSalesAmount * royaltyPercent) / 100.0);
			DecimalFormat royAmnt = new DecimalFormat("#.##");
			calRoyaltyAmt = Double.valueOf(royAmnt.format(calRoyaltyAmt));

			String totalRoyaltyatSummary = fc.utobj().getText(driver,
					driver.findElement(By.xpath(".//a[contains(text () , '" + franchiseId + "')]/ancestor::tr/td[5]")));

			if (totalRoyaltyatSummary.contains(",")) {
				totalRoyaltyatSummary = totalRoyaltyatSummary.replaceAll(",", "");
			}
			double totalRoyalty = Double.parseDouble(totalRoyaltyatSummary);
			if (calRoyaltyAmt != totalRoyalty) {
				fc.utobj().throwsException("Royalty Amount on Royalty Summary Page is not same as royalty calculated");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "finance")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-06", testCaseId = "TC_FC_Verify_Adv_on_Royalties_Summary_Page_091", testCaseDescription = "Verify verify Adv Royalties Summary Page")
	public void verifyAdvRoyaltiesSummaryPage() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		Map<String, String> dataSet = fc.utobj().readTestData("finance", testCaseId);

		String agrmntVrsnName = fc.utobj().generateTestData(dataSet.get("agreementVersionName"));
		String royaltyPrcntg = dataSet.get("royaltyPercentage");
		String advPercentage = dataSet.get("advertisementPercentage");
		String royaltyAreaFranchiseValue = dataSet.get("RoyaltyAreaFranchise");
		String advAreaFranchiseValue = dataSet.get("AdvAreaFranchise");
		
		try {
			driver = fc.loginpage().login(driver);

			FinanceSalesPage pobj = new FinanceSalesPage(driver);
			FinanceSalesPageTest sales_PageTest = new FinanceSalesPageTest();

			fc.utobj().printTestStep("Navigate To Admin > Finance > Agreement Versions");
			AdminFinanceAgreementVersionsPageTest agreementforSales = new AdminFinanceAgreementVersionsPageTest();

			fc.utobj().printTestStep("Add Agreement");
			String agreementVersion = agreementforSales.addAgreement(driver, config, agrmntVrsnName,
					 royaltyPrcntg, advPercentage, royaltyAreaFranchiseValue, advAreaFranchiseValue);

			fc.utobj().printTestStep("Navigate To Admin > Franchise Location > Add Franchise Location");
			fc.utobj().printTestStep("Add Franchise Location");

			String regionName = fc.utobj().generateTestData("GT");
			String storeType = fc.utobj().generateTestData("GT");
			String franchiseId = fc.utobj().generateTestData("GT");
			AdminFranchiseLocationAddFranchiseLocationPageTest franchise = new AdminFranchiseLocationAddFranchiseLocationPageTest();
			franchiseId = franchise.addFranchiseLocation_All_Agreement(driver, franchiseId, regionName, storeType,
					agreementVersion, config);

			fc.utobj().printTestStep("Navigate To Admin > Finance > Finance Setup Preferences");
			fc.utobj().printTestStep("Setup Medium of submission of Sales Reports");
			String salesReportType = "webForm";
			new AdminFinanceFinanceSetupPreferencesPageTest().mediumOfSubmissionOfSalesReports(driver, config,
					salesReportType);

			fc.utobj().printTestStep("Set Invoice Frequency");
			new AdminFinanceFinanceSetupPreferencesPageTest().setInvoice(driver, config);

			fc.utobj().printTestStep("Navigate To Finance > Sales");
			fc.utobj().printTestStep("Enter Sales Report");
			String categoryQuantity = "110";
			String categoryAmount = "6000";
			String reportId = sales_PageTest.enterSalesReport(driver, franchiseId, categoryQuantity, categoryAmount);

			fc.utobj().printTestStep("Filter Sales Report By Franchise Id");
			sales_PageTest.filterSalesReport(driver, franchiseId);

			fc.utobj().printTestStep("Approve Sales Report");
			fc.utobj().actionImgOption(driver, reportId, "Approve");

			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().sendKeys(driver, pobj.commentTxtArea,
					"Test Approve Comments" + fc.utobj().generateRandomNumber());
			fc.utobj().clickElement(driver, pobj.SaveBtn);

			fc.utobj().clickElement(driver, pobj.cloaseBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Filter Sales Report By Franchise Id");
			sales_PageTest.filterSalesReport(driver, franchiseId);

			fc.utobj().printTestStep("Generate Invoice");
			fc.utobj().actionImgOption(driver, franchiseId, "Generate Invoice");

			fc.utobj().printTestStep("Navigate To Finance > Royalties Page");
			fc.finance().finance_common().financeRoyaltiesPage(driver);

			fc.utobj().printTestStep("Filter Sales Report By Franchise Id");
			sales_PageTest.filterSalesReport(driver, franchiseId);

			fc.utobj().printTestStep("Verify Adv on Royalty Summary Page");
			double enteredSalesAmount = Double.parseDouble(categoryAmount);
			double advPercent = Double.parseDouble(advPercentage);
			double calAdvAmt = ((enteredSalesAmount * advPercent) / 100.0);
			DecimalFormat royAmnt = new DecimalFormat("#.##");
			calAdvAmt = Double.valueOf(royAmnt.format(calAdvAmt));

			String totalAdvatSummary = fc.utobj().getText(driver,
					driver.findElement(By.xpath(".//a[contains(text () , '" + franchiseId + "')]/ancestor::tr/td[6]")));

			if (totalAdvatSummary.contains(",")) {
				totalAdvatSummary = totalAdvatSummary.replaceAll(",", "");

			}

			double totalAdvertisement = Double.parseDouble(totalAdvatSummary);

			if (calAdvAmt != totalAdvertisement) {
				fc.utobj().throwsException(
						"Advertisement Amount on Royalty Summary Page is not same as Advertisement calculated");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "finance" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "TC_FC_Verify_Report_Period_on_Royalties_Summary_Page_092", testCaseDescription = "Verify Report Period Royalties Summary Page")
	public void verifyReportPeriodRoyaltiesSummaryPage() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());
		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		Map<String, String> dataSet = fc.utobj().readTestData("finance", testCaseId);

		String agrmntVrsnName = fc.utobj().generateTestData(dataSet.get("agreementVersionName"));
		String royaltyPrcntg = dataSet.get("royaltyPercentage");
		String advPercentage = dataSet.get("advertisementPercentage");
		String royaltyAreaFranchiseValue = dataSet.get("RoyaltyAreaFranchise");
		String advAreaFranchiseValue = dataSet.get("AdvAreaFranchise");
		
		try {
			driver = fc.loginpage().login(driver);
			FinanceSalesPage pobj = new FinanceSalesPage(driver);
			FinanceSalesPageTest sales_PageTest = new FinanceSalesPageTest();

			fc.utobj().printTestStep("Navigate To Admin > Finance > Agreement Versions");
			AdminFinanceAgreementVersionsPageTest agreementforSales = new AdminFinanceAgreementVersionsPageTest();

			fc.utobj().printTestStep("Add Agreement");
			String agreementVersion = agreementforSales.addAgreement(driver, config, agrmntVrsnName,
					 royaltyPrcntg, advPercentage, royaltyAreaFranchiseValue, advAreaFranchiseValue);

			fc.utobj().printTestStep("Navigate To Admin > Franchise Location > Add Franchise Location");
			fc.utobj().printTestStep("Add Franchise Location");

			String regionName = fc.utobj().generateTestData("GT");
			String storeType = fc.utobj().generateTestData("GT");
			String franchiseId = fc.utobj().generateTestData("GT");
			AdminFranchiseLocationAddFranchiseLocationPageTest franchise = new AdminFranchiseLocationAddFranchiseLocationPageTest();
			franchiseId = franchise.addFranchiseLocation_All_Agreement(driver, franchiseId, regionName, storeType,
					agreementVersion, config);

			fc.utobj().printTestStep("Navigate To Admin > Finance > Finance Setup Preferences");
			fc.utobj().printTestStep("Setup Medium of submission of Sales Reports");
			String salesReportType = "webForm";
			new AdminFinanceFinanceSetupPreferencesPageTest().mediumOfSubmissionOfSalesReports(driver, config,
					salesReportType);

			fc.utobj().printTestStep("Set Invoice Frequency");
			new AdminFinanceFinanceSetupPreferencesPageTest().setInvoice(driver, config);

			fc.utobj().printTestStep("Navigate To Finance > Sales");
			fc.utobj().printTestStep("Enter Sales Report");
			String categoryQuantity = "110";
			String categoryAmount = "6000";

			fc.finance().finance_common().financeSalesPage(driver);
			fc.utobj().clickElement(driver, pobj.enterSalesReport);
			fc.utobj().selectDropDown(driver, pobj.selectFranchise, franchiseId);

			fc.utobj().selectDropDownByIndex(driver, pobj.selectReportPeriod, 0);

			String reportPrd = fc.utobj().getText(driver, pobj.getSelectdReportId);
			fc.utobj().clickElement(driver, pobj.ContinueBtn);

			fc.utobj().sendKeys(driver, pobj.rcvdDate, fc.utobj().getFutureDateUSFormat(-1));

			fc.utobj().sendKeys(driver,
					driver.findElement(By
							.xpath(".//td[contains(text () , 'Service')]/following-sibling::td//input[contains(@name , 'quantity')]")),
					categoryQuantity);
			fc.utobj().sendKeys(driver,
					driver.findElement(By
							.xpath(".//td[contains(text () , 'Service')]/following-sibling::td//input[contains(@name , 'amount')]")),
					categoryAmount);

			fc.utobj().clickElement(driver, pobj.SaveBtn);

			if (!fc.utobj().isSelected(driver,pobj.acknowledgement)) {
				fc.utobj().clickElement(driver, pobj.acknowledgement);
			}
			fc.utobj().clickElement(driver, pobj.SaveBtn);

			String reportId = fc.utobj().getText(driver, pobj.reportId);
			fc.utobj().clickElement(driver, pobj.okayBtn);

			fc.utobj().printTestStep("Filter Sales Report By Franchise Id");
			sales_PageTest.filterSalesReport(driver, franchiseId);

			fc.utobj().printTestStep("Approve Sales Report");
			fc.utobj().actionImgOption(driver, reportId, "Approve");

			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().sendKeys(driver, pobj.commentTxtArea,
					"Test Approve Comments" + fc.utobj().generateRandomNumber());
			fc.utobj().clickElement(driver, pobj.SaveBtn);

			fc.utobj().clickElement(driver, pobj.cloaseBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Filter Sales Report By Franchise Id");
			sales_PageTest.filterSalesReport(driver, franchiseId);

			fc.utobj().printTestStep("Generate Invoice");
			fc.utobj().actionImgOption(driver, franchiseId, "Generate Invoice");

			fc.utobj().printTestStep("Navigate To Finance > Royalties Page");
			fc.finance().finance_common().financeRoyaltiesPage(driver);

			fc.utobj().printTestStep("Filter Sales Report By Franchise Id");
			sales_PageTest.filterSalesReport(driver, franchiseId);

			fc.utobj().printTestStep("Verify Report Period On Royalty summary");

			String reportPeriod = fc.utobj().getText(driver,
					driver.findElement(By.xpath(".//a[contains(text () , '" + franchiseId + "')]/ancestor::tr/td[4]")));

			if (!reportPeriod.equalsIgnoreCase(reportPrd)) {
				fc.utobj().throwsException("Report Period on Royalty Summary Page is Wrong");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "finance")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "TC_FC_Verify_Invoice_Amount_on_Royalties_Summary_Page_093", testCaseDescription = "Verify Invoice Amount Royalties Summary Page")
	public void verifyInvoiceAmountRoyaltiesSummaryPage() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		Map<String, String> dataSet = fc.utobj().readTestData("finance", testCaseId);

		String agrmntVrsnName = fc.utobj().generateTestData(dataSet.get("agreementVersionName"));
		String royaltyPrcntg = dataSet.get("royaltyPercentage");
		String advPercentage = dataSet.get("advertisementPercentage");
		String royaltyAreaFranchiseValue = dataSet.get("RoyaltyAreaFranchise");
		String advAreaFranchiseValue = dataSet.get("AdvAreaFranchise");
		
		try {
			driver = fc.loginpage().login(driver);

			FinanceSalesPage pobj = new FinanceSalesPage(driver);
			FinanceSalesPageTest sales_PageTest = new FinanceSalesPageTest();

			fc.utobj().printTestStep("Navigate To Admin > Finance > Agreement Versions");
			AdminFinanceAgreementVersionsPageTest agreementforSales = new AdminFinanceAgreementVersionsPageTest();

			fc.utobj().printTestStep("Add Agreement");
			String agreementVersion = agreementforSales.addAgreement(driver, config, agrmntVrsnName,
					 royaltyPrcntg, advPercentage, royaltyAreaFranchiseValue, advAreaFranchiseValue);

			fc.utobj().printTestStep("Navigate To Admin > Franchise Location > Add Franchise Location");
			fc.utobj().printTestStep("Add Franchise Location");

			String regionName = fc.utobj().generateTestData("GT");
			String storeType = fc.utobj().generateTestData("GT");
			String franchiseId = fc.utobj().generateTestData("GT");
			AdminFranchiseLocationAddFranchiseLocationPageTest franchise = new AdminFranchiseLocationAddFranchiseLocationPageTest();
			franchiseId = franchise.addFranchiseLocation_All_Agreement(driver, franchiseId, regionName, storeType,
					agreementVersion, config);

			fc.utobj().printTestStep("Navigate To Admin > Finance > Finance Setup Preferences");
			fc.utobj().printTestStep("Setup Medium of submission of Sales Reports");
			String salesReportType = "webForm";
			new AdminFinanceFinanceSetupPreferencesPageTest().mediumOfSubmissionOfSalesReports(driver, config,
					salesReportType);

			fc.utobj().printTestStep("Set Invoice Frequency");
			new AdminFinanceFinanceSetupPreferencesPageTest().setInvoice(driver, config);

			fc.utobj().printTestStep("Navigate To Finance > Sales");
			fc.utobj().printTestStep("Enter Sales Report");
			String categoryQuantity = "110";
			String categoryAmount = "6000";
			String reportId = sales_PageTest.enterSalesReport(driver, franchiseId, categoryQuantity, categoryAmount);

			fc.utobj().printTestStep("Filter Sales Report By Franchise Id");
			sales_PageTest.filterSalesReport(driver, franchiseId);

			fc.utobj().printTestStep("Approve Sales Report");
			fc.utobj().actionImgOption(driver, reportId, "Approve");

			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().sendKeys(driver, pobj.commentTxtArea,
					"Test Approve Comments" + fc.utobj().generateRandomNumber());
			fc.utobj().clickElement(driver, pobj.SaveBtn);

			fc.utobj().clickElement(driver, pobj.cloaseBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Filter Sales Report By Franchise Id");
			sales_PageTest.filterSalesReport(driver, franchiseId);

			fc.utobj().printTestStep("Generate Invoice");
			fc.utobj().actionImgOption(driver, franchiseId, "Generate Invoice");

			fc.utobj().printTestStep("Navigate To Finance > Royalties Page");
			fc.finance().finance_common().financeRoyaltiesPage(driver);

			fc.utobj().printTestStep("Filter Sales Report By Franchise Id");
			sales_PageTest.filterSalesReport(driver, franchiseId);

			fc.utobj().printTestStep("Verify The Invoice Amount At Royalty Summary Page");

			double enteredSalesAmount = Double.parseDouble(categoryAmount);
			double royaltyPercent = Double.parseDouble(royaltyPrcntg);
			double calRoyaltyAmt = ((enteredSalesAmount * royaltyPercent) / 100.0);
			DecimalFormat royAmnt = new DecimalFormat("#.##");
			calRoyaltyAmt = Double.valueOf(royAmnt.format(calRoyaltyAmt));
			double advPercent = Double.parseDouble(advPercentage);
			double calAdvAmt = ((enteredSalesAmount * advPercent) / 100.0);
			DecimalFormat advAmnt = new DecimalFormat("#.##");
			calAdvAmt = Double.valueOf(advAmnt.format(calAdvAmt));

			WebElement e1 = driver.findElement(By.xpath(".//div[@id='printReady']//*[contains(text(), '" + franchiseId
					+ "')]/ancestor::tr[@class='bText12' or @class='bText12 grAltRw3']/td[7]"));
			fc.utobj().moveToElement(driver, e1);
			String additionalInvoice = e1.getText();

			if (additionalInvoice.contains(",")) {
				additionalInvoice = additionalInvoice.replaceAll(",", "");
			}
			double additionalInvoiceAmount = Double.parseDouble(additionalInvoice);
			double calInvoiceAmount = (calRoyaltyAmt + calAdvAmt + additionalInvoiceAmount);
			DecimalFormat Invc = new DecimalFormat("#.##");
			calInvoiceAmount = Double.valueOf(Invc.format(calInvoiceAmount));

			WebElement e2 = driver.findElement(By.xpath(".//div[@id='printReady']//*[contains(text(), '" + franchiseId
					+ "')]/ancestor::tr[@class='bText12' or @class='bText12 grAltRw3']/td[8]"));
			fc.utobj().moveToElement(driver, e2);
			String totalInvoiceAmountatSummary = e2.getText();

			if (totalInvoiceAmountatSummary.contains(",")) {
				totalInvoiceAmountatSummary = totalInvoiceAmountatSummary.replaceAll(",", "");
			}

			double totalInvoiceAmount = Double.parseDouble(totalInvoiceAmountatSummary);
			if (calInvoiceAmount != totalInvoiceAmount) {
				fc.utobj().throwsException(
						"Invoice Amount on Royalty Summary Page is not same as Invoice Amount calculated");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "finance")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2017-06-12", testCaseId = "TC_FC_Verify_Open_Balance_on_Royalties_Summary_Page_094", testCaseDescription = "Verify Open Balance Royalties Summary Page")
	public void verifyOpenBalanceRoyaltiesSummaryPage() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		Map<String, String> dataSet = fc.utobj().readTestData("finance", testCaseId);

		String agrmntVrsnName = fc.utobj().generateTestData(dataSet.get("agreementVersionName"));
		String royaltyPrcntg = dataSet.get("royaltyPercentage");
		String advPercentage = dataSet.get("advertisementPercentage");
		String royaltyAreaFranchiseValue = dataSet.get("RoyaltyAreaFranchise");
		String advAreaFranchiseValue = dataSet.get("AdvAreaFranchise");
		
		try {
			driver = fc.loginpage().login(driver);
			FinanceSalesPage pobj = new FinanceSalesPage(driver);
			FinanceSalesPageTest sales_PageTest = new FinanceSalesPageTest();

			fc.utobj().printTestStep("Navigate To Admin > Finance > Agreement Versions");
			AdminFinanceAgreementVersionsPageTest agreementforSales = new AdminFinanceAgreementVersionsPageTest();

			fc.utobj().printTestStep("Add Agreement");
			String agreementVersion = agreementforSales.addAgreement(driver, config, agrmntVrsnName,
					 royaltyPrcntg, advPercentage, royaltyAreaFranchiseValue, advAreaFranchiseValue);

			fc.utobj().printTestStep("Navigate To Admin > Franchise Location > Add Franchise Location");
			fc.utobj().printTestStep("Add Franchise Location");

			String regionName = fc.utobj().generateTestData("GT");
			String storeType = fc.utobj().generateTestData("GT");
			String franchiseId = fc.utobj().generateTestData("GT");
			AdminFranchiseLocationAddFranchiseLocationPageTest franchise = new AdminFranchiseLocationAddFranchiseLocationPageTest();
			franchiseId = franchise.addFranchiseLocation_All_Agreement(driver, franchiseId, regionName, storeType,
					agreementVersion, config);

			fc.utobj().printTestStep("Navigate To Admin > Finance > Finance Setup Preferences");
			fc.utobj().printTestStep("Setup Medium of submission of Sales Reports");
			String salesReportType = "webForm";
			new AdminFinanceFinanceSetupPreferencesPageTest().mediumOfSubmissionOfSalesReports(driver, config,
					salesReportType);

			fc.utobj().printTestStep("Set Invoice Frequency");
			new AdminFinanceFinanceSetupPreferencesPageTest().setInvoice(driver, config);

			fc.utobj().printTestStep("Navigate To Finance > Sales");
			fc.utobj().printTestStep("Enter Sales Report");
			String categoryQuantity = "110";
			String categoryAmount = "6000";
			String reportId = sales_PageTest.enterSalesReport(driver, franchiseId, categoryQuantity, categoryAmount);

			fc.utobj().printTestStep("Filter Sales Report By Franchise Id");
			sales_PageTest.filterSalesReport(driver, franchiseId);

			fc.utobj().printTestStep("Approve Sales Report");
			fc.utobj().actionImgOption(driver, reportId, "Approve");

			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().sendKeys(driver, pobj.commentTxtArea,
					"Test Approve Comments" + fc.utobj().generateRandomNumber());
			fc.utobj().clickElement(driver, pobj.SaveBtn);

			fc.utobj().clickElement(driver, pobj.cloaseBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Filter Sales Report By Franchise Id");
			sales_PageTest.filterSalesReport(driver, franchiseId);

			fc.utobj().printTestStep("Generate Invoice");
			fc.utobj().actionImgOption(driver, franchiseId, "Generate Invoice");

			fc.utobj().printTestStep("Navigate To Finance > Royalties Page");
			fc.finance().finance_common().financeRoyaltiesPage(driver);

			fc.utobj().printTestStep("Filter Sales Report By Franchise Id");
			sales_PageTest.filterSalesReport(driver, franchiseId);

			double enteredSalesAmount = Double.parseDouble(categoryAmount);
			double royaltyPercent = Double.parseDouble(royaltyPrcntg);
			double calRoyaltyAmt = ((enteredSalesAmount * royaltyPercent) / 100.0);
			DecimalFormat royAmnt = new DecimalFormat("#.##");
			calRoyaltyAmt = Double.valueOf(royAmnt.format(calRoyaltyAmt));
			double advPercent = Double.parseDouble(advPercentage);
			double calAdvAmt = ((enteredSalesAmount * advPercent) / 100.0);
			DecimalFormat advAmnt = new DecimalFormat("#.##");
			calAdvAmt = Double.valueOf(advAmnt.format(calAdvAmt));

			WebElement e = driver.findElement(By.xpath(".//div[@id='printReady']//*[contains(text(), '" + franchiseId
					+ "')]/ancestor::tr[@class='bText12' or @class='bText12 grAltRw3']/td[7]"));
			fc.utobj().moveToElement(driver, e);
			String additionalInvoice = e.getText();

			if (additionalInvoice.contains(",")) {
				additionalInvoice = additionalInvoice.replaceAll(",", "");
			}
			double additionalInvoiceAmount = Double.parseDouble(additionalInvoice);
			double calOpenBal = (calRoyaltyAmt + calAdvAmt + additionalInvoiceAmount);
			DecimalFormat Invc = new DecimalFormat("#.##");
			calOpenBal = Double.valueOf(Invc.format(calOpenBal));
			WebElement e1 = driver.findElement(By.xpath(".//div[@id='printReady']//*[contains(text(), '" + franchiseId
					+ "')]/ancestor::tr[@class='bText12' or @class='bText12 grAltRw3']/td[9]"));
			fc.utobj().moveToElement(driver, e1);
			String totalOpenBalanceatSummary = e1.getText();

			if (totalOpenBalanceatSummary.contains(",")) {
				totalOpenBalanceatSummary = totalOpenBalanceatSummary.replaceAll(",", "");
			}
			double totalInvoiceAmount = Double.parseDouble(totalOpenBalanceatSummary);
			if (calOpenBal != totalInvoiceAmount) {

				fc.utobj()
						.throwsException("Open Balance on Royalty Summary Page is not same as Open Balance calculated");
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "finance")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "TC_FC_Total_Sales_Amount_Invoice_Details_Page_102", testCaseDescription = "Verify Total Sales Amount at Invoice Details Page")
	public void verifyTotalSalesAmountatInvoiceDetailsPage() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		Map<String, String> dataSet = fc.utobj().readTestData("finance", testCaseId);

		String agrmntVrsnName = fc.utobj().generateTestData(dataSet.get("agreementVersionName"));
		String royaltyPrcntg = dataSet.get("royaltyPercentage");
		String advPercentage = dataSet.get("advertisementPercentage");
		String royaltyAreaFranchiseValue = dataSet.get("RoyaltyAreaFranchise");
		String advAreaFranchiseValue = dataSet.get("AdvAreaFranchise");
		
		try {
			driver = fc.loginpage().login(driver);

			FinanceSalesPage pobj = new FinanceSalesPage(driver);
			FinanceSalesPageTest sales_PageTest = new FinanceSalesPageTest();

			fc.utobj().printTestStep("Navigate To Admin > Finance > Agreement Versions");
			AdminFinanceAgreementVersionsPageTest agreementforSales = new AdminFinanceAgreementVersionsPageTest();

			fc.utobj().printTestStep("Add Agreement");
			String agreementVersion = agreementforSales.addAgreement(driver, config, agrmntVrsnName,
					 royaltyPrcntg, advPercentage, royaltyAreaFranchiseValue, advAreaFranchiseValue);

			fc.utobj().printTestStep("Navigate To Admin > Franchise Location > Add Franchise Location");
			fc.utobj().printTestStep("Add Franchise Location");

			String regionName = fc.utobj().generateTestData("GT");
			String storeType = fc.utobj().generateTestData("GT");
			String franchiseId = fc.utobj().generateTestData("GT");
			AdminFranchiseLocationAddFranchiseLocationPageTest franchise = new AdminFranchiseLocationAddFranchiseLocationPageTest();
			franchiseId = franchise.addFranchiseLocation_All_Agreement(driver, franchiseId, regionName, storeType,
					agreementVersion, config);

			fc.utobj().printTestStep("Navigate To Admin > Finance > Finance Setup Preferences");
			fc.utobj().printTestStep("Setup Medium of submission of Sales Reports");
			String salesReportType = "webForm";
			new AdminFinanceFinanceSetupPreferencesPageTest().mediumOfSubmissionOfSalesReports(driver, config,
					salesReportType);

			fc.utobj().printTestStep("Set Invoice Frequency");
			new AdminFinanceFinanceSetupPreferencesPageTest().setInvoice(driver, config);

			fc.utobj().printTestStep("Navigate To Finance > Sales");
			fc.utobj().printTestStep("Enter Sales Report");
			String categoryQuantity = "110";
			String categoryAmount = "6000";
			String reportId = sales_PageTest.enterSalesReport(driver, franchiseId, categoryQuantity, categoryAmount);

			fc.utobj().printTestStep("Filter Sales Report By Franchise Id");
			sales_PageTest.filterSalesReport(driver, franchiseId);

			fc.utobj().printTestStep("Approve Sales Report");
			fc.utobj().actionImgOption(driver, reportId, "Approve");

			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().sendKeys(driver, pobj.commentTxtArea,
					"Test Approve Comments" + fc.utobj().generateRandomNumber());
			fc.utobj().clickElement(driver, pobj.SaveBtn);

			fc.utobj().clickElement(driver, pobj.cloaseBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Filter Sales Report By Franchise Id");
			sales_PageTest.filterSalesReport(driver, franchiseId);

			fc.utobj().printTestStep("Generate Invoice");
			fc.utobj().actionImgOption(driver, franchiseId, "Generate Invoice");

			fc.utobj().printTestStep("Navigate To Finance > Royalties Page");
			fc.finance().finance_common().financeRoyaltiesPage(driver);

			fc.utobj().printTestStep("Filter Sales Report By Franchise Id");
			sales_PageTest.filterSalesReport(driver, franchiseId);

			fc.utobj().printTestStep("Verify Total Sales At Invoice Details Page");
			fc.utobj().actionImgOption(driver, franchiseId, "View Invoice Details");

			String totalSalesAmount = driver
					.findElement(By.xpath(".//tr[td[contains(text(), 'Total Sales ($)')]]/td[4]")).getText();

			if (totalSalesAmount.contains(",")) {
				totalSalesAmount = totalSalesAmount.replaceAll(",", "");
			}

			double totalSales = Double.parseDouble(totalSalesAmount);
			double calServiceAmount = Double.parseDouble(categoryAmount);
			DecimalFormat df = new DecimalFormat("#.##");
			calServiceAmount = Double.valueOf(df.format(calServiceAmount));
			if (totalSales != calServiceAmount) {
				fc.utobj().throwsException(
						"Total Sales Amount at Invoice Details Page is not Same as Calculated Sales Amount");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "finance")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2017-06-15", testCaseId = "TC_FC_Royalty_Amount_Invoice_Details_Page_103", testCaseDescription = "Verify Royalty Amount at Invoice Details Page")
	public void verifyRoyaltyAmountatInvoiceDetailsPage() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		Map<String, String> dataSet = fc.utobj().readTestData("finance", testCaseId);

		String agrmntVrsnName = fc.utobj().generateTestData(dataSet.get("agreementVersionName"));
		String royaltyPrcntg = dataSet.get("royaltyPercentage");
		String advPercentage = dataSet.get("advertisementPercentage");
		String royaltyAreaFranchiseValue = dataSet.get("RoyaltyAreaFranchise");
		String advAreaFranchiseValue = dataSet.get("AdvAreaFranchise");
		
		try {
			driver = fc.loginpage().login(driver);

			FinanceSalesPage pobj = new FinanceSalesPage(driver);
			FinanceSalesPageTest sales_PageTest = new FinanceSalesPageTest();

			fc.utobj().printTestStep("Navigate To Admin > Finance > Agreement Versions");
			AdminFinanceAgreementVersionsPageTest agreementforSales = new AdminFinanceAgreementVersionsPageTest();

			fc.utobj().printTestStep("Add Agreement");
			String agreementVersion = agreementforSales.addAgreement(driver, config, agrmntVrsnName,
					 royaltyPrcntg, advPercentage, royaltyAreaFranchiseValue, advAreaFranchiseValue);

			fc.utobj().printTestStep("Navigate To Admin > Franchise Location > Add Franchise Location");
			fc.utobj().printTestStep("Add Franchise Location");

			String regionName = fc.utobj().generateTestData("GT");
			String storeType = fc.utobj().generateTestData("GT");
			String franchiseId = fc.utobj().generateTestData("GT");
			AdminFranchiseLocationAddFranchiseLocationPageTest franchise = new AdminFranchiseLocationAddFranchiseLocationPageTest();
			franchiseId = franchise.addFranchiseLocation_All_Agreement(driver, franchiseId, regionName, storeType,
					agreementVersion, config);

			fc.utobj().printTestStep("Navigate To Admin > Finance > Finance Setup Preferences");
			fc.utobj().printTestStep("Setup Medium of submission of Sales Reports");
			String salesReportType = "webForm";
			new AdminFinanceFinanceSetupPreferencesPageTest().mediumOfSubmissionOfSalesReports(driver, config,
					salesReportType);

			fc.utobj().printTestStep("Set Invoice Frequency");
			new AdminFinanceFinanceSetupPreferencesPageTest().setInvoice(driver, config);

			fc.utobj().printTestStep("Navigate To Finance > Sales");
			fc.utobj().printTestStep("Enter Sales Report");
			String categoryQuantity = "110";
			String categoryAmount = "6000";
			String reportId = sales_PageTest.enterSalesReport(driver, franchiseId, categoryQuantity, categoryAmount);

			fc.utobj().printTestStep("Filter Sales Report By Franchise Id");
			sales_PageTest.filterSalesReport(driver, franchiseId);

			fc.utobj().printTestStep("Approve Sales Report");
			fc.utobj().actionImgOption(driver, reportId, "Approve");

			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().sendKeys(driver, pobj.commentTxtArea,
					"Test Approve Comments" + fc.utobj().generateRandomNumber());
			fc.utobj().clickElement(driver, pobj.SaveBtn);

			fc.utobj().clickElement(driver, pobj.cloaseBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Filter Sales Report By Franchise Id");
			sales_PageTest.filterSalesReport(driver, franchiseId);

			fc.utobj().printTestStep("Generate Invoice");
			fc.utobj().actionImgOption(driver, franchiseId, "Generate Invoice");

			fc.utobj().printTestStep("Navigate To Finance > Royalties Page");
			fc.finance().finance_common().financeRoyaltiesPage(driver);

			fc.utobj().printTestStep("Filter Sales Report By Franchise Id");
			sales_PageTest.filterSalesReport(driver, franchiseId);

			fc.utobj().actionImgOption(driver, franchiseId, "View Invoice Details");
			String totalRoyaltyAmount = driver.findElement(By.xpath(".//tr[td[contains(text(), 'Royalty ($)')]]/td[4]"))
					.getText();

			if (totalRoyaltyAmount.contains(",")) {
				totalRoyaltyAmount = totalRoyaltyAmount.replaceAll(",", "");
			}

			double royalty = Double.parseDouble(totalRoyaltyAmount);
			double calServiceAmount = Double.parseDouble(categoryAmount);
			DecimalFormat df = new DecimalFormat("#.##");
			calServiceAmount = Double.valueOf(df.format(calServiceAmount));
			double royaltyPercent = Double.parseDouble(royaltyPrcntg);
			double calRoyalty = ((calServiceAmount * royaltyPercent) / 100.0);
			calRoyalty = Double.valueOf(df.format(calRoyalty));
			if (royalty != calRoyalty) {
				fc.utobj().throwsException(
						"Total Royalty Amount at Invoice Details Page is not Same as Calculated Royalty Amount");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "finance" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "TC_FC_Advertisement_Amount_Invoice_Details_Page_104", testCaseDescription = "Verify Advertisement Amount at Invoice Details Page")
	public void verifyAdvertisementAmountatInvoiceDetailsPage() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		Map<String, String> dataSet = fc.utobj().readTestData("finance", testCaseId);

		String agrmntVrsnName = fc.utobj().generateTestData(dataSet.get("agreementVersionName"));
		String royaltyPrcntg = dataSet.get("royaltyPercentage");
		String advPercentage = dataSet.get("advertisementPercentage");
		String royaltyAreaFranchiseValue = dataSet.get("RoyaltyAreaFranchise");
		String advAreaFranchiseValue = dataSet.get("AdvAreaFranchise");
		
		try {
			driver = fc.loginpage().login(driver);

			FinanceSalesPage pobj = new FinanceSalesPage(driver);
			FinanceSalesPageTest sales_PageTest = new FinanceSalesPageTest();

			fc.utobj().printTestStep("Navigate To Admin > Finance > Agreement Versions");
			AdminFinanceAgreementVersionsPageTest agreementforSales = new AdminFinanceAgreementVersionsPageTest();

			fc.utobj().printTestStep("Add Agreement");
			String agreementVersion = agreementforSales.addAgreement(driver, config, agrmntVrsnName,
					 royaltyPrcntg, advPercentage, royaltyAreaFranchiseValue, advAreaFranchiseValue);

			fc.utobj().printTestStep("Navigate To Admin > Franchise Location > Add Franchise Location");
			fc.utobj().printTestStep("Add Franchise Location");

			String regionName = fc.utobj().generateTestData("GT");
			String storeType = fc.utobj().generateTestData("GT");
			String franchiseId = fc.utobj().generateTestData("GT");
			AdminFranchiseLocationAddFranchiseLocationPageTest franchise = new AdminFranchiseLocationAddFranchiseLocationPageTest();
			franchiseId = franchise.addFranchiseLocation_All_Agreement(driver, franchiseId, regionName, storeType,
					agreementVersion, config);

			fc.utobj().printTestStep("Navigate To Admin > Finance > Finance Setup Preferences");
			fc.utobj().printTestStep("Setup Medium of submission of Sales Reports");
			String salesReportType = "webForm";
			new AdminFinanceFinanceSetupPreferencesPageTest().mediumOfSubmissionOfSalesReports(driver, config,
					salesReportType);

			fc.utobj().printTestStep("Set Invoice Frequency");
			new AdminFinanceFinanceSetupPreferencesPageTest().setInvoice(driver, config);

			fc.utobj().printTestStep("Navigate To Finance > Sales");
			fc.utobj().printTestStep("Enter Sales Report");
			String categoryQuantity = "110";
			String categoryAmount = "6000";
			String reportId = sales_PageTest.enterSalesReport(driver, franchiseId, categoryQuantity, categoryAmount);

			fc.utobj().printTestStep("Filter Sales Report By Franchise Id");
			sales_PageTest.filterSalesReport(driver, franchiseId);

			fc.utobj().printTestStep("Approve Sales Report");
			fc.utobj().actionImgOption(driver, reportId, "Approve");

			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().sendKeys(driver, pobj.commentTxtArea,
					"Test Approve Comments" + fc.utobj().generateRandomNumber());
			fc.utobj().clickElement(driver, pobj.SaveBtn);

			fc.utobj().clickElement(driver, pobj.cloaseBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Filter Sales Report By Franchise Id");
			sales_PageTest.filterSalesReport(driver, franchiseId);

			fc.utobj().printTestStep("Generate Invoice");
			fc.utobj().actionImgOption(driver, franchiseId, "Generate Invoice");

			fc.utobj().printTestStep("Navigate To Finance > Royalties Page");
			fc.finance().finance_common().financeRoyaltiesPage(driver);

			fc.utobj().printTestStep("Filter Sales Report By Franchise Id");
			sales_PageTest.filterSalesReport(driver, franchiseId);

			fc.utobj().actionImgOption(driver, franchiseId, "View Invoice Details");

			String totalAdvAmount = driver.findElement(By.xpath(".//tr[td[contains(text(), 'Adv. ($)')]]/td[4]"))
					.getText();

			if (totalAdvAmount.contains(",")) {
				totalAdvAmount = totalAdvAmount.replaceAll(",", "");
			}

			double advertisement = Double.parseDouble(totalAdvAmount);
			double calServiceAmount = Double.parseDouble(categoryAmount);
			DecimalFormat df = new DecimalFormat("#.##");
			calServiceAmount = Double.valueOf(df.format(calServiceAmount));
			double advPercent = Double.parseDouble(advPercentage);
			double calAdv = ((calServiceAmount * advPercent) / 100.0);
			calAdv = Double.valueOf(df.format(calAdv));
			if (advertisement != calAdv) {
				fc.utobj().throwsException(
						"Total Advertisement Amount at Invoice Details Page is not Same as Calculated Advertisement Amount");

			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "finance" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2017-06-05", testCaseId = "TC_FC_Invoice_Amount_Invoice_Details_Page_105", testCaseDescription = "Verify Invoice Amount at Invoice Details Page")
	public void verifyInvoiceAmountatInvoiceDetailsPage() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		Map<String, String> dataSet = fc.utobj().readTestData("finance", testCaseId);

		String agrmntVrsnName = fc.utobj().generateTestData(dataSet.get("agreementVersionName"));
		String royaltyPrcntg = dataSet.get("royaltyPercentage");
		String advPercentage = dataSet.get("advertisementPercentage");
		String royaltyAreaFranchiseValue = dataSet.get("RoyaltyAreaFranchise");
		String advAreaFranchiseValue = dataSet.get("AdvAreaFranchise");
		
		

		try {
			driver = fc.loginpage().login(driver);
			FinanceSalesPage pobj = new FinanceSalesPage(driver);
			FinanceSalesPageTest sales_PageTest = new FinanceSalesPageTest();

			fc.utobj().printTestStep("Navigate To Admin > Finance > Agreement Versions");
			AdminFinanceAgreementVersionsPageTest agreementforSales = new AdminFinanceAgreementVersionsPageTest();

			fc.utobj().printTestStep("Add Agreement");
			String agreementVersion = agreementforSales.addAgreement(driver, config, agrmntVrsnName,
					 royaltyPrcntg, advPercentage, royaltyAreaFranchiseValue, advAreaFranchiseValue);

			fc.utobj().printTestStep("Navigate To Admin > Franchise Location > Add Franchise Location");
			fc.utobj().printTestStep("Add Franchise Location");

			String regionName = fc.utobj().generateTestData("GT");
			String storeType = fc.utobj().generateTestData("GT");
			String franchiseId = fc.utobj().generateTestData("GT");
			AdminFranchiseLocationAddFranchiseLocationPageTest franchise = new AdminFranchiseLocationAddFranchiseLocationPageTest();
			franchiseId = franchise.addFranchiseLocation_All_Agreement(driver, franchiseId, regionName, storeType,
					agreementVersion, config);

			fc.utobj().printTestStep("Navigate To Admin > Finance > Finance Setup Preferences");
			fc.utobj().printTestStep("Setup Medium of submission of Sales Reports");
			String salesReportType = "webForm";
			new AdminFinanceFinanceSetupPreferencesPageTest().mediumOfSubmissionOfSalesReports(driver, config,
					salesReportType);

			fc.utobj().printTestStep("Set Invoice Frequency");
			new AdminFinanceFinanceSetupPreferencesPageTest().setInvoice(driver, config);

			fc.utobj().printTestStep("Navigate To Finance > Sales");
			fc.utobj().printTestStep("Enter Sales Report");
			String categoryQuantity = "110";
			String categoryAmount = "6000";
			String reportId = sales_PageTest.enterSalesReport(driver, franchiseId, categoryQuantity, categoryAmount);

			fc.utobj().printTestStep("Filter Sales Report By Franchise Id");
			sales_PageTest.filterSalesReport(driver, franchiseId);

			fc.utobj().printTestStep("Approve Sales Report");
			fc.utobj().actionImgOption(driver, reportId, "Approve");

			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().sendKeys(driver, pobj.commentTxtArea,
					"Test Approve Comments" + fc.utobj().generateRandomNumber());
			fc.utobj().clickElement(driver, pobj.SaveBtn);

			fc.utobj().clickElement(driver, pobj.cloaseBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Filter Sales Report By Franchise Id");
			sales_PageTest.filterSalesReport(driver, franchiseId);

			fc.utobj().printTestStep("Generate Invoice");
			fc.utobj().actionImgOption(driver, franchiseId, "Generate Invoice");

			fc.utobj().printTestStep("Navigate To Finance > Royalties Page");
			fc.finance().finance_common().financeRoyaltiesPage(driver);

			fc.utobj().printTestStep("Filter Sales Report By Franchise Id");
			sales_PageTest.filterSalesReport(driver, franchiseId);

			fc.utobj().actionImgOption(driver, franchiseId, "View Invoice Details");

			String totalInvoiceAmount = fc.utobj().getText(driver, driver.findElement(By.xpath(
					".//*[contains(text () , 'Invoice Report Details')]/ancestor::tr[2]//td[contains(text () , 'Invoice Amount')]/following-sibling::td")));
			if (totalInvoiceAmount.contains(",")) {
				totalInvoiceAmount = totalInvoiceAmount.replaceAll(",", "");
			}

			double invoiceAmount = Double.parseDouble(totalInvoiceAmount);
			double calServiceAmount = Double.parseDouble(categoryAmount);
			DecimalFormat df = new DecimalFormat("#.##");
			calServiceAmount = Double.valueOf(df.format(calServiceAmount));
			double advPercent = Double.parseDouble(advPercentage);
			double calAdv = ((calServiceAmount * advPercent) / 100.0);
			calAdv = Double.valueOf(df.format(calAdv));
			double royaltyPercent = Double.parseDouble(royaltyPrcntg);
			double calRoyalty = ((calServiceAmount * royaltyPercent) / 100.0);
			calRoyalty = Double.valueOf(df.format(calRoyalty));

			String additionalFees = fc.utobj().getText(driver, driver.findElement(By.xpath(
					".//*[contains(text () , 'Invoice Report Details')]/ancestor::tr[2]//td[contains(text () , 'Additional Fees')]/following-sibling::td")));

			if (additionalFees.contains(",")) {
				additionalFees = additionalFees.replaceAll(",", "");
			}

			double AdditionalFees = Double.parseDouble(additionalFees);
			double calInvoiceAmount = (calRoyalty + calAdv + AdditionalFees);

			if (calInvoiceAmount != invoiceAmount) {
				fc.utobj().throwsException(
						"Total Invoice Amount at Invoice Details Page is not Same as Calculated Invoice Amount");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "finance" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2017-06-09", testCaseId = "TC_FC_Open_Balance_Invoice_Details_Page_106", testCaseDescription = "Verify Open Balance at Invoice Details Page")
	public void verifyOpenBalanceatInvoiceDetailsPage() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		Map<String, String> dataSet = fc.utobj().readTestData("finance", testCaseId);

		String agrmntVrsnName = fc.utobj().generateTestData(dataSet.get("agreementVersionName"));
		String royaltyPrcntg = dataSet.get("royaltyPercentage");
		String advPercentage = dataSet.get("advertisementPercentage");
		String royaltyAreaFranchiseValue = dataSet.get("RoyaltyAreaFranchise");
		String advAreaFranchiseValue = dataSet.get("AdvAreaFranchise");
		
		

		try {
			driver = fc.loginpage().login(driver);

			FinanceSalesPage pobj = new FinanceSalesPage(driver);
			FinanceSalesPageTest sales_PageTest = new FinanceSalesPageTest();

			fc.utobj().printTestStep("Navigate To Admin > Finance > Agreement Versions");
			AdminFinanceAgreementVersionsPageTest agreementforSales = new AdminFinanceAgreementVersionsPageTest();

			fc.utobj().printTestStep("Add Agreement");
			String agreementVersion = agreementforSales.addAgreement(driver, config, agrmntVrsnName,
					 royaltyPrcntg, advPercentage, royaltyAreaFranchiseValue, advAreaFranchiseValue);

			fc.utobj().printTestStep("Navigate To Admin > Franchise Location > Add Franchise Location");
			fc.utobj().printTestStep("Add Franchise Location");

			String regionName = fc.utobj().generateTestData("GT");
			String storeType = fc.utobj().generateTestData("GT");
			String franchiseId = fc.utobj().generateTestData("GT");
			AdminFranchiseLocationAddFranchiseLocationPageTest franchise = new AdminFranchiseLocationAddFranchiseLocationPageTest();
			franchiseId = franchise.addFranchiseLocation_All_Agreement(driver, franchiseId, regionName, storeType,
					agreementVersion, config);

			fc.utobj().printTestStep("Navigate To Admin > Finance > Finance Setup Preferences");
			fc.utobj().printTestStep("Setup Medium of submission of Sales Reports");
			String salesReportType = "webForm";
			new AdminFinanceFinanceSetupPreferencesPageTest().mediumOfSubmissionOfSalesReports(driver, config,
					salesReportType);

			fc.utobj().printTestStep("Set Invoice Frequency");
			new AdminFinanceFinanceSetupPreferencesPageTest().setInvoice(driver, config);

			fc.utobj().printTestStep("Navigate To Finance > Sales");
			fc.utobj().printTestStep("Enter Sales Report");
			String categoryQuantity = "110";
			String categoryAmount = "6000";
			String reportId = sales_PageTest.enterSalesReport(driver, franchiseId, categoryQuantity, categoryAmount);

			fc.utobj().printTestStep("Filter Sales Report By Franchise Id");
			sales_PageTest.filterSalesReport(driver, franchiseId);

			fc.utobj().printTestStep("Approve Sales Report");
			fc.utobj().actionImgOption(driver, reportId, "Approve");

			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().sendKeys(driver, pobj.commentTxtArea,
					"Test Approve Comments" + fc.utobj().generateRandomNumber());
			fc.utobj().clickElement(driver, pobj.SaveBtn);

			fc.utobj().clickElement(driver, pobj.cloaseBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Filter Sales Report By Franchise Id");
			sales_PageTest.filterSalesReport(driver, franchiseId);

			fc.utobj().printTestStep("Generate Invoice");
			fc.utobj().actionImgOption(driver, franchiseId, "Generate Invoice");

			fc.utobj().printTestStep("Navigate To Finance > Royalties Page");
			fc.finance().finance_common().financeRoyaltiesPage(driver);

			fc.utobj().printTestStep("Filter Sales Report By Franchise Id");
			sales_PageTest.filterSalesReport(driver, franchiseId);

			fc.utobj().actionImgOption(driver, franchiseId, "View Invoice Details");

			String openBalance = fc.utobj().getText(driver, driver.findElement(By.xpath(
					".//*[contains(text () , 'Invoice Report Details')]/ancestor::tr[2]//td[contains(text () , 'Open Balance')]/following-sibling::td")));

			if (openBalance.contains(",")) {
				openBalance = openBalance.replaceAll(",", "");
			}
			double OpenBalance = Double.parseDouble(openBalance);
			double calServiceAmount = Double.parseDouble(categoryAmount);
			DecimalFormat df = new DecimalFormat("#.##");
			calServiceAmount = Double.valueOf(df.format(calServiceAmount));
			double advPercent = Double.parseDouble(advPercentage);
			double calAdv = ((calServiceAmount * advPercent) / 100.0);
			calAdv = Double.valueOf(df.format(calAdv));
			double royaltyPercent = Double.parseDouble(royaltyPrcntg);
			double calRoyalty = ((calServiceAmount * royaltyPercent) / 100.0);
			calRoyalty = Double.valueOf(df.format(calRoyalty));

			String additionalFees = fc.utobj().getText(driver, driver.findElement(By.xpath(
					".//*[contains(text () , 'Invoice Report Details')]/ancestor::tr[2]//td[contains(text () , 'Additional Fees')]/following-sibling::td")));
			if (additionalFees.contains(",")) {
				additionalFees = additionalFees.replaceAll(",", "");
			}
			double AdditionalFees = Double.parseDouble(additionalFees);
			double calInvoiceAmount = (calRoyalty + calAdv + AdditionalFees);

			String amountPaid = fc.utobj().getText(driver, driver.findElement(By.xpath(
					".//*[contains(text () , 'Payment Details')]/ancestor::tr[2]//td[contains(text () , 'Amount Paid')]/following-sibling::td[1]")));
			if (amountPaid.contains(",")) {
				amountPaid = amountPaid.replaceAll(",", "");
			}
			double AmountPaid = Double.parseDouble(amountPaid);
			double calOpenBalance = (calInvoiceAmount - AmountPaid);
			if (OpenBalance != calOpenBalance) {
				fc.utobj().throwsException(
						"Total Open Balance at Invoice Details Page is not Same as Calculated Open Balance");
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "finance" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2017-06-05", testCaseId = "TC_FC_add_Line_Items_ForRoyalty_Invoice_Details_Page_107", testCaseDescription = "Verify add Line Items For Royalty at Invoice Details Page")
	public void addLineItemsForRoyaltyatInvoiceDetailsPage() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		Map<String, String> dataSet = fc.utobj().readTestData("finance", testCaseId);

		String agrmntVrsnName = fc.utobj().generateTestData(dataSet.get("agreementVersionName"));
		String royaltyPrcntg = dataSet.get("royaltyPercentage");
		String advPercentage = dataSet.get("advertisementPercentage");
		String royaltyAreaFranchiseValue = dataSet.get("RoyaltyAreaFranchise");
		String advAreaFranchiseValue = dataSet.get("AdvAreaFranchise");
		
		

		try {
			driver = fc.loginpage().login(driver);
			FinanceSalesPage pobj = new FinanceSalesPage(driver);
			FinanceSalesPageTest sales_PageTest = new FinanceSalesPageTest();

			fc.utobj().printTestStep("Navigate To Admin > Finance > Agreement Versions");
			AdminFinanceAgreementVersionsPageTest agreementforSales = new AdminFinanceAgreementVersionsPageTest();

			fc.utobj().printTestStep("Add Agreement");
			String agreementVersion = agreementforSales.addAgreement(driver, config, agrmntVrsnName,
					 royaltyPrcntg, advPercentage, royaltyAreaFranchiseValue, advAreaFranchiseValue);

			fc.utobj().printTestStep("Navigate To Admin > Franchise Location > Add Franchise Location");
			fc.utobj().printTestStep("Add Franchise Location");

			String regionName = fc.utobj().generateTestData("GT");
			String storeType = fc.utobj().generateTestData("GT");
			String franchiseId = fc.utobj().generateTestData("GT");
			AdminFranchiseLocationAddFranchiseLocationPageTest franchise = new AdminFranchiseLocationAddFranchiseLocationPageTest();
			franchiseId = franchise.addFranchiseLocation_All_Agreement(driver, franchiseId, regionName, storeType,
					agreementVersion, config);

			fc.utobj().printTestStep("Navigate To Admin > Finance > Finance Setup Preferences");
			fc.utobj().printTestStep("Setup Medium of submission of Sales Reports");
			String salesReportType = "webForm";
			new AdminFinanceFinanceSetupPreferencesPageTest().mediumOfSubmissionOfSalesReports(driver, config,
					salesReportType);

			fc.utobj().printTestStep("Set Invoice Frequency");
			new AdminFinanceFinanceSetupPreferencesPageTest().setInvoice(driver, config);

			fc.utobj().printTestStep("Navigate To Finance > Sales");
			fc.utobj().printTestStep("Enter Sales Report");
			String categoryQuantity = "110";
			String categoryAmount = "6000";
			String reportId = sales_PageTest.enterSalesReport(driver, franchiseId, categoryQuantity, categoryAmount);

			fc.utobj().printTestStep("Filter Sales Report By Franchise Id");
			sales_PageTest.filterSalesReport(driver, franchiseId);

			fc.utobj().printTestStep("Approve Sales Report");
			fc.utobj().actionImgOption(driver, reportId, "Approve");

			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().sendKeys(driver, pobj.commentTxtArea,
					"Test Approve Comments" + fc.utobj().generateRandomNumber());
			fc.utobj().clickElement(driver, pobj.SaveBtn);

			fc.utobj().clickElement(driver, pobj.cloaseBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Filter Sales Report By Franchise Id");
			sales_PageTest.filterSalesReport(driver, franchiseId);

			fc.utobj().printTestStep("Generate Invoice");
			fc.utobj().actionImgOption(driver, franchiseId, "Generate Invoice");

			fc.utobj().printTestStep("Navigate To Finance > Royalties Page");
			fc.finance().finance_common().financeRoyaltiesPage(driver);

			fc.utobj().printTestStep("Filter Sales Report By Franchise Id");
			
			sales_PageTest.filterSalesReport(driver, franchiseId);

			FinanceRoyaltiesPage royalty_Page = new FinanceRoyaltiesPage(driver);

			fc.utobj().actionImgOption(driver, franchiseId, "View Invoice Details");

			WebElement e1 = driver.findElement(By.xpath(
					".//td[contains(text () , 'Royalty')][not(contains(text () , 'Total Royalty'))][not(contains(text () , 'Open Royalty'))]/following-sibling::td[1]"));
			fc.utobj().moveToElement(driver, e1);
			String royaltyBeforeLineItem = e1.getText();
			fc.utobj().clickElement(driver, royalty_Page.addLineItems);
			String lineItem = fc.utobj().generateTestData("Litem");
			WebElement e2 = driver.findElement(By.xpath(".//span[contains(text(), 'Add Line Items')]"));
			fc.utobj().moveToElement(driver, e2);
			fc.utobj().clickElement(driver, e2);
			fc.utobj().sendKeys(driver, royalty_Page.itemDescription, lineItem);
			fc.utobj().selectDropDown(driver, royalty_Page.adjType, "Royalty");
			fc.utobj().sendKeys(driver, royalty_Page.adjAmount, "100");
			fc.utobj().clickElement(driver, royalty_Page.saveLineItem);

			WebElement e3 = driver.findElement(By.xpath(
					".//td[contains(text () , 'Royalty')][not(contains(text () , 'Total Royalty'))][not(contains(text () , 'Open Royalty'))]/following-sibling::td[1]"));
			fc.utobj().moveToElement(driver, e3);
			
			String royaltyAfterLineItem = e3.getText();
			
			double royalty1 = Double.parseDouble(royaltyBeforeLineItem);
			
			double royalty2 = Double.parseDouble(royaltyAfterLineItem);
			
			if (royalty2 != (royalty1 + 100)) {
				fc.utobj().throwsException("Line Item Amount is not updated in Royalty Amount");
			}
			boolean lineItemPresent = driver.findElement(By.xpath(".//*[contains(text(), '"+lineItem+"')]")).isDisplayed();

			if (lineItemPresent == false) {
				fc.utobj().throwsException("Line Item is not added");
			}
			
			fc.utobj().actionImgOption(driver, lineItem, "Modify Line Items");
			
			lineItem = fc.utobj().generateTestData("Litem");
			
			fc.utobj().sendKeys(driver, royalty_Page.itemDescription, lineItem);
			fc.utobj().selectDropDown(driver, royalty_Page.adjType, "Royalty");
			fc.utobj().sendKeys(driver, royalty_Page.adjAmount, "150");
			fc.utobj().clickElement(driver, royalty_Page.saveLineItem);
			

			WebElement e4 = driver.findElement(By.xpath(
					".//td[contains(text () , 'Royalty')][not(contains(text () , 'Total Royalty'))][not(contains(text () , 'Open Royalty'))]/following-sibling::td[1]"));
			fc.utobj().moveToElement(driver, e4);
			
			String royaltyAfterLineItem1 = e4.getText();
			
			double royalty3 = Double.parseDouble(royaltyBeforeLineItem);
			
			double royalty4 = Double.parseDouble(royaltyAfterLineItem1);
			
			if (royalty4 != (royalty3 + 150)) {
				fc.utobj().throwsException("Line Item Amount is not updated in Royalty Amount after modification of Line Item");
			}
			boolean lineItemPresent1 = driver.findElement(By.xpath(".//*[contains(text(), '"+lineItem+"')]")).isDisplayed();

			if (lineItemPresent1 == false) {
				fc.utobj().throwsException("Modified Line Item is not showing");
			}
			
			fc.utobj().printTestStep("Navigate To Finance > Royalties Page");
			fc.finance().finance_common().financeRoyaltiesPage(driver);

			fc.utobj().printTestStep("Filter Sales Report By Franchise Id");
			sales_PageTest.filterSalesReport(driver, franchiseId);

			fc.utobj().printTestStep("Apply Payments");
			fc.utobj().actionImgOption(driver, franchiseId, "Apply Payment");
			fc.utobj().clickElement(driver, royalty_Page.selectPaymentAppliedforRoyalty);
			fc.utobj().clickElement(driver, royalty_Page.continueBtn);

			fc.utobj().printTestStep("Apply Payment By Cash Option");
			FinancePaymentsPage payments_page = new FinancePaymentsPage(driver);
			fc.utobj().sendKeys(driver, payments_page.recievedDate, fc.utobj().getCurrentDateUSFormat());

			fc.utobj().printTestStep("Navigate To Finance > Royalties Page");
			fc.finance().finance_common().financeRoyaltiesPage(driver);

			fc.utobj().printTestStep("Filter Sales Report By Franchise Id");
			sales_PageTest.filterSalesReport(driver, franchiseId);
			
			fc.utobj().actionImgOption(driver, franchiseId, "View Invoice Details");
			
			boolean lineItemPresent2 = driver.findElement(By.xpath(".//*[contains(text(), '"+lineItem+"')]")).isDisplayed();

			if (lineItemPresent2 == false) {
				fc.utobj().throwsException("Modified Line Item is not showing");
			}
			
			boolean addLineItemLink = royalty_Page.addLineItems.isDisplayed();
			
			if(addLineItemLink == false) {
				fc.utobj().throwsException("Add Line Item link is not present in case of partial payment");
			}
			
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "finance" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2017-06-13", testCaseId = "TC_FC_apply_Payement_forAll_Invoice_Details_Page_108", testCaseDescription = "Apply Payement for All Via Invoice Details Page")
	public void applyPayementforAllViaInvoiceDetailsPage() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		Map<String, String> dataSet = fc.utobj().readTestData("finance", testCaseId);

		String agrmntVrsnName = fc.utobj().generateTestData(dataSet.get("agreementVersionName"));
		String royaltyPrcntg = dataSet.get("royaltyPercentage");
		String advPercentage = dataSet.get("advertisementPercentage");
		String royaltyAreaFranchiseValue = dataSet.get("RoyaltyAreaFranchise");
		String advAreaFranchiseValue = dataSet.get("AdvAreaFranchise");
		
		

		try {
			driver = fc.loginpage().login(driver);

			FinanceRoyaltiesPage pobj = new FinanceRoyaltiesPage(driver);
			FinanceSalesPage sales_Page = new FinanceSalesPage(driver);
			FinanceSalesPageTest sale_pageTest = new FinanceSalesPageTest();

			fc.utobj().printTestStep("Navigate To Admin > Finance > Agreement Versions");
			AdminFinanceAgreementVersionsPageTest agreementforSales = new AdminFinanceAgreementVersionsPageTest();

			fc.utobj().printTestStep("Add Agreement");
			String agreementVersion = agreementforSales.addAgreement(driver, config, agrmntVrsnName,
					 royaltyPrcntg, advPercentage, royaltyAreaFranchiseValue, advAreaFranchiseValue);

			fc.utobj().printTestStep("Navigate To Admin > Franchise Location > Add Franchise Location");
			fc.utobj().printTestStep("Add Franchise Location");

			String regionName = fc.utobj().generateTestData("GT");
			String storeType = fc.utobj().generateTestData("GT");
			String franchiseId = fc.utobj().generateTestData("GT");
			AdminFranchiseLocationAddFranchiseLocationPageTest franchise = new AdminFranchiseLocationAddFranchiseLocationPageTest();
			franchiseId = franchise.addFranchiseLocation_All_Agreement(driver, franchiseId, regionName, storeType,
					agreementVersion, config);

			fc.utobj().printTestStep("Navigate To Admin > Finance > Finance Setup Preferences");
			fc.utobj().printTestStep("Setup Medium of submission of Sales Reports");
			String salesReportType = "webForm";
			new AdminFinanceFinanceSetupPreferencesPageTest().mediumOfSubmissionOfSalesReports(driver, config,
					salesReportType);

			fc.utobj().printTestStep("Set Invoice Frequency");
			new AdminFinanceFinanceSetupPreferencesPageTest().setInvoice(driver, config);

			fc.utobj().printTestStep("Navigate To Finance > Sales");
			fc.utobj().printTestStep("Enter Sales Report");
			String categoryQuantity = "110";
			String categoryAmount = "6000";
			String reportId = new FinanceSalesPageTest().enterSalesReport(driver, franchiseId, categoryQuantity,
					categoryAmount);

			fc.utobj().printTestStep("Filter Sales Report By Franchise Id");
			sale_pageTest.filterSalesReport(driver, franchiseId);

			fc.utobj().printTestStep("Approve Sales Report");
			fc.utobj().actionImgOption(driver, reportId, "Approve");

			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().sendKeys(driver, sales_Page.commentTxtArea,
					"Test Approve Comments" + fc.utobj().generateRandomNumber());
			fc.utobj().clickElement(driver, sales_Page.SaveBtn);

			fc.utobj().clickElement(driver, sales_Page.cloaseBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Generate Invoice");
			fc.utobj().printTestStep("Filter Sales Report By Franchise Id");
			sale_pageTest.filterSalesReport(driver, franchiseId);

			fc.utobj().printTestStep("Generate Invoice");
			fc.utobj().actionImgOption(driver, franchiseId, "Generate Invoice");

			fc.utobj().printTestStep("Navigate To Finance > Royalties");
			fc.finance().finance_common().financeRoyaltiesPage(driver);

			fc.utobj().printTestStep("Filter Report By Franchise Id");
			sale_pageTest.filterSalesReport(driver, franchiseId);

			fc.utobj().printTestStep("View Invoice Details");
			fc.utobj().actionImgOption(driver, franchiseId, "View Invoice Details");

			fc.utobj().printTestStep("Apply Payment By Apply Payment Bottm Button");
			fc.utobj().clickElement(driver, pobj.applyPaymentInvoiceDetails);
			fc.utobj().clickElement(driver, pobj.selectPaymentAppliedforAll);
			fc.utobj().clickElement(driver, pobj.continueBtn);

			fc.utobj().printTestStep("Apply Payment for All Invoice Details");

			FinancePaymentsPage payments_page = new FinancePaymentsPage(driver);
			
			fc.utobj().sendKeys(driver, payments_page.recievedDate, fc.utobj().getCurrentDateUSFormat());

			double advVal = Double.parseDouble(categoryAmount) * Double.parseDouble(advPercentage) / 100;
			double royaltyVal = Double.parseDouble(categoryAmount) * Double.parseDouble(royaltyPrcntg) / 100;

			double amountToBePaid = royaltyVal + advVal;

			fc.utobj().sendKeys(driver, pobj.paymentAmount, Double.toString(amountToBePaid));
			fc.utobj().printTestStep("Test Ref No" + fc.utobj().generateRandomChar());

			fc.utobj().printTestStep("Apply Payment By Cash Option");

			if (!fc.utobj().isSelected(driver, payments_page.collectionMethodsCash)) {
				fc.utobj().clickElement(driver, payments_page.collectionMethodsCash);
			}

			fc.utobj().sendKeys(driver, payments_page.comments, "Test Comments" + fc.utobj().generateRandomChar());
			fc.utobj().clickElement(driver, pobj.saveBtn);

			fc.utobj().printTestStep("Veriy The All Amount Paid");
			fc.utobj().selectDropDownByValue(driver, payments_page.resultsPerPage, "500");

			String amountApplied = fc.utobj().getText(driver, driver
					.findElement(By.xpath(".//td[contains(text () , '" + franchiseId + "')]/ancestor::tr/td[4]")));

			if (amountApplied.contains(",")) {
				amountApplied = amountApplied.replaceAll(",", "");
			}

			if (amountToBePaid != Double.parseDouble(amountApplied)) {
				fc.utobj().throwsException("was not able to verify The Amount Paid");
			}

			fc.utobj().printTestStep("Navigate To Finance > Royalties");
			
			fc.finance().finance_common().financeRoyaltiesPage(driver);

			fc.utobj().printTestStep("Filter Sales Report By Franchise Id");
			
			sale_pageTest.filterSalesReport(driver, franchiseId);

			fc.utobj().printTestStep("Verify Payment Status");
			
			boolean isTextPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () ,'" + franchiseId + "')]/ancestor::tr/td[contains(text () ,'Fully Paid')]");

			if (isTextPresent == false) {
				fc.utobj().throwsException("was not able to verify the Fully Paid Status Of Payments");
			}
			
			fc.utobj().actionImgOption(driver, franchiseId, "View Invoice Details");
			
			boolean addLineItemPresent = fc.utobj().assertPageSource(driver, "Add Line Items");
						
			if(addLineItemPresent == true) {
				
				fc.utobj().throwsException("Add Line Item link is present in case of full payment");
			}
			
			
			//String franchiseId ="Testw07014946";
			fc.utobj().printTestStep("Navigate To Finance > Royalties");
			fc.finance().finance_common().financeRoyaltiesPage(driver);

			fc.utobj().printTestStep("Filter Report By Franchise Id");
			sale_pageTest.filterSalesReport(driver, franchiseId);

			fc.utobj().printTestStep("Reverse Payment For Royalty");
			fc.utobj().actionImgOption(driver, franchiseId, "Reverse Payment");
			 
			fc.utobj().acceptAlertBox(driver);
			 
			sale_pageTest.filterSalesReport(driver, franchiseId);
			 
			boolean isTextPresent1 = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[contains(text () ,'" + franchiseId
					+ "')]/ancestor::tr/td[contains(text () ,'Unpaid')]");

			if (isTextPresent1 == false) {
				
				fc.utobj().throwsException("was not able to verify unpaid Status of royalty after reverse payment");
			}
			
			
			
			
			
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "finance" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2017-06-09", testCaseId = "TC_FC_Apply_Payment_forRoyalty_Royalties_115", testCaseDescription = "Verify apply Payment for Royalty Via Royalties")
	public void applyPaymentforRoyaltyViaRoyalties() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("finance", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		String agrmntVrsnName = fc.utobj().generateTestData(dataSet.get("agreementVersionName"));
		String royaltyPrcntg = dataSet.get("royaltyPercentage");
		String advPercentage = dataSet.get("advertisementPercentage");
		String royaltyAreaFranchiseValue = dataSet.get("RoyaltyAreaFranchise");
		String advAreaFranchiseValue = dataSet.get("AdvAreaFranchise");
		
		

		try {
			driver = fc.loginpage().login(driver);
			FinanceSalesPage pobj = new FinanceSalesPage(driver);
			FinanceSalesPageTest sales_PageTest = new FinanceSalesPageTest();

			fc.utobj().printTestStep("Navigate To Admin > Finance > Agreement Versions");
			AdminFinanceAgreementVersionsPageTest agreementforSales = new AdminFinanceAgreementVersionsPageTest();

			fc.utobj().printTestStep("Add Agreement");
			String agreementVersion = agreementforSales.addAgreement(driver, config, agrmntVrsnName,
					 royaltyPrcntg, advPercentage, royaltyAreaFranchiseValue, advAreaFranchiseValue);

			fc.utobj().printTestStep("Navigate To Admin > Franchise Location > Add Franchise Location");
			fc.utobj().printTestStep("Add Franchise Location");

			String regionName = fc.utobj().generateTestData("GT");
			String storeType = fc.utobj().generateTestData("GT");
			String franchiseId = fc.utobj().generateTestData("GT");
			AdminFranchiseLocationAddFranchiseLocationPageTest franchise = new AdminFranchiseLocationAddFranchiseLocationPageTest();
			franchiseId = franchise.addFranchiseLocation_All_Agreement(driver, franchiseId, regionName, storeType,
					agreementVersion, config);

			fc.utobj().printTestStep("Navigate To Admin > Finance > Finance Setup Preferences");
			fc.utobj().printTestStep("Setup Medium of submission of Sales Reports");
			String salesReportType = "webForm";
			new AdminFinanceFinanceSetupPreferencesPageTest().mediumOfSubmissionOfSalesReports(driver, config,
					salesReportType);

			fc.utobj().printTestStep("Set Invoice Frequency");
			new AdminFinanceFinanceSetupPreferencesPageTest().setInvoice(driver, config);

			fc.utobj().printTestStep("Navigate To Finance > Sales");
			fc.utobj().printTestStep("Enter Sales Report");
			String categoryQuantity = "110";
			String categoryAmount = "6000";
			String reportId = sales_PageTest.enterSalesReport(driver, franchiseId, categoryQuantity, categoryAmount);

			fc.utobj().printTestStep("Filter Sales Report By Franchise Id");
			sales_PageTest.filterSalesReport(driver, franchiseId);

			fc.utobj().printTestStep("Approve Sales Report");
			fc.utobj().actionImgOption(driver, reportId, "Approve");

			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().sendKeys(driver, pobj.commentTxtArea,
					"Test Approve Comments" + fc.utobj().generateRandomNumber());
			fc.utobj().clickElement(driver, pobj.SaveBtn);

			fc.utobj().clickElement(driver, pobj.cloaseBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Filter Sales Report By Franchise Id");
			sales_PageTest.filterSalesReport(driver, franchiseId);

			fc.utobj().printTestStep("Generate Invoice");
			fc.utobj().actionImgOption(driver, franchiseId, "Generate Invoice");

			fc.utobj().printTestStep("Navigate To Finance > Royalties Page");
			fc.finance().finance_common().financeRoyaltiesPage(driver);

			fc.utobj().printTestStep("Filter Sales Report By Franchise Id");
			sales_PageTest.filterSalesReport(driver, franchiseId);

			FinanceRoyaltiesPage royalty_Page = new FinanceRoyaltiesPage(driver);
			fc.utobj().printTestStep("Apply Payments");
			fc.utobj().actionImgOption(driver, franchiseId, "Apply Payment");
			fc.utobj().clickElement(driver, royalty_Page.selectPaymentAppliedforRoyalty);
			fc.utobj().clickElement(driver, royalty_Page.continueBtn);

			fc.utobj().printTestStep("Apply Payment By Cash Option");
			FinancePaymentsPage payments_page = new FinancePaymentsPage(driver);
			fc.utobj().sendKeys(driver, payments_page.recievedDate, fc.utobj().getCurrentDateUSFormat());

			// double
			// advVal=Double.parseDouble(categoryAmount)*Double.parseDouble(advPercentage)/100;
			double royaltyVal = Double.parseDouble(categoryAmount) * Double.parseDouble(royaltyPrcntg) / 100;

			// double amountToBePaid=royaltyVal+advVal;

			fc.utobj().sendKeys(driver, payments_page.paymentAmount, Double.toString(royaltyVal));
			fc.utobj().printTestStep("Test Ref No" + fc.utobj().generateRandomChar());

			if (!fc.utobj().isSelected(driver, payments_page.collectionMethodsCash)) {
				fc.utobj().clickElement(driver, payments_page.collectionMethodsCash);
			}

			fc.utobj().sendKeys(driver, payments_page.comments, "Test Comments" + fc.utobj().generateRandomChar());
			fc.utobj().clickElement(driver, payments_page.saveBtn);

			fc.utobj().printTestStep("Veriy The Royality Amount Paid");
			fc.utobj().selectDropDownByValue(driver, payments_page.resultsPerPage, "500");

			String amountApplied = fc.utobj().getText(driver, driver
					.findElement(By.xpath(".//td[contains(text () , '" + franchiseId + "')]/ancestor::tr/td[4]")));

			if (amountApplied.contains(",")) {
				amountApplied = amountApplied.replaceAll(",", "");
			}

			if (royaltyVal != Double.parseDouble(amountApplied)) {
				fc.utobj().throwsException("was not able to verify The Amount Paid");
			}

			fc.utobj().printTestStep("Navigate To Finance > Royalties");
			fc.finance().finance_common().financeRoyaltiesPage(driver);

			fc.utobj().printTestStep("Filter Sales Report By Franchise Id");
			new FinanceSalesPageTest().filterSalesReport(driver, franchiseId);

			fc.utobj().printTestStep("Verify Payment Status");
			boolean isTextPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[contains(text () ,'" + franchiseId
					+ "')]/ancestor::tr/td[contains(text () ,'Partially Paid')]");

			if (isTextPresent == false) {
				fc.utobj().throwsException("was not able to verify the Fully Paid Status Of Payments");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "finance" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2017-06-08", testCaseId = "TC_FC_Apply_Payment_forAdv_Royalties_116", testCaseDescription = "Verify apply Payment for Adv Via Royalties")
	public void applyPaymentforAdvViaRoyalties() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("finance", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		String agrmntVrsnName = fc.utobj().generateTestData(dataSet.get("agreementVersionName"));
		String royaltyPrcntg = dataSet.get("royaltyPercentage");
		String advPercentage = dataSet.get("advertisementPercentage");
		String royaltyAreaFranchiseValue = dataSet.get("RoyaltyAreaFranchise");
		String advAreaFranchiseValue = dataSet.get("AdvAreaFranchise");
		
		

		try {
			driver = fc.loginpage().login(driver);
			FinanceSalesPage pobj = new FinanceSalesPage(driver);
			FinanceSalesPageTest sales_PageTest = new FinanceSalesPageTest();

			fc.utobj().printTestStep("Navigate To Admin > Finance > Agreement Versions");
			AdminFinanceAgreementVersionsPageTest agreementforSales = new AdminFinanceAgreementVersionsPageTest();

			fc.utobj().printTestStep("Add Agreement");
			String agreementVersion = agreementforSales.addAgreement(driver, config, agrmntVrsnName,
					 royaltyPrcntg, advPercentage, royaltyAreaFranchiseValue, advAreaFranchiseValue);

			fc.utobj().printTestStep("Navigate To Admin > Franchise Location > Add Franchise Location");
			fc.utobj().printTestStep("Add Franchise Location");

			String regionName = fc.utobj().generateTestData("GT");
			String storeType = fc.utobj().generateTestData("GT");
			String franchiseId = fc.utobj().generateTestData("GT");
			AdminFranchiseLocationAddFranchiseLocationPageTest franchise = new AdminFranchiseLocationAddFranchiseLocationPageTest();
			franchiseId = franchise.addFranchiseLocation_All_Agreement(driver, franchiseId, regionName, storeType,
					agreementVersion, config);

			fc.utobj().printTestStep("Navigate To Admin > Finance > Finance Setup Preferences");
			fc.utobj().printTestStep("Setup Medium of submission of Sales Reports");
			String salesReportType = "webForm";
			new AdminFinanceFinanceSetupPreferencesPageTest().mediumOfSubmissionOfSalesReports(driver, config,
					salesReportType);

			fc.utobj().printTestStep("Set Invoice Frequency");
			new AdminFinanceFinanceSetupPreferencesPageTest().setInvoice(driver, config);

			fc.utobj().printTestStep("Navigate To Finance > Sales");
			fc.utobj().printTestStep("Enter Sales Report");
			String categoryQuantity = "110";
			String categoryAmount = "6000";
			String reportId = sales_PageTest.enterSalesReport(driver, franchiseId, categoryQuantity, categoryAmount);

			fc.utobj().printTestStep("Filter Sales Report By Franchise Id");
			sales_PageTest.filterSalesReport(driver, franchiseId);

			fc.utobj().printTestStep("Approve Sales Report");
			fc.utobj().actionImgOption(driver, reportId, "Approve");

			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().sendKeys(driver, pobj.commentTxtArea,
					"Test Approve Comments" + fc.utobj().generateRandomNumber());
			fc.utobj().clickElement(driver, pobj.SaveBtn);

			fc.utobj().clickElement(driver, pobj.cloaseBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Filter Sales Report By Franchise Id");
			sales_PageTest.filterSalesReport(driver, franchiseId);

			fc.utobj().printTestStep("Generate Invoice");
			fc.utobj().actionImgOption(driver, franchiseId, "Generate Invoice");

			fc.utobj().printTestStep("Navigate To Finance > Royalties Page");
			fc.finance().finance_common().financeRoyaltiesPage(driver);

			fc.utobj().printTestStep("Filter Sales Report By Franchise Id");
			sales_PageTest.filterSalesReport(driver, franchiseId);

			FinanceRoyaltiesPage royalty_Page = new FinanceRoyaltiesPage(driver);
			fc.utobj().printTestStep("Apply Payments");
			fc.utobj().actionImgOption(driver, franchiseId, "Apply Payment");
			fc.utobj().clickElement(driver, royalty_Page.selectPaymentAppliedforAdv);
			fc.utobj().clickElement(driver, royalty_Page.continueBtn);

			fc.utobj().printTestStep("Apply Payment By Cheque Option");
			FinancePaymentsPage payments_page = new FinancePaymentsPage(driver);
			fc.utobj().sendKeys(driver, payments_page.recievedDate, fc.utobj().getCurrentDateUSFormat());

			double advVal = Double.parseDouble(categoryAmount) * Double.parseDouble(advPercentage) / 100;

			fc.utobj().sendKeys(driver, payments_page.paymentAmount, Double.toString(advVal));

			if (!fc.utobj().isSelected(driver, payments_page.collectionMethodsCheque)) {
				fc.utobj().clickElement(driver, payments_page.collectionMethodsCheque);
			}

			fc.utobj().sendKeys(driver, payments_page.chequeNo, "TestC" + fc.utobj().generateRandomNumber());

			fc.utobj().sendKeys(driver, payments_page.comments, "Test Comments" + fc.utobj().generateRandomChar());
			fc.utobj().clickElement(driver, payments_page.saveBtn);

			fc.utobj().printTestStep("Veriy The Adv Amount Paid");
			fc.utobj().selectDropDownByValue(driver, payments_page.resultsPerPage, "500");

			String amountApplied = fc.utobj().getText(driver, driver
					.findElement(By.xpath(".//td[contains(text () , '" + franchiseId + "')]/ancestor::tr/td[4]")));

			if (amountApplied.contains(",")) {
				amountApplied = amountApplied.replaceAll(",", "");
			}

			if (advVal != Double.parseDouble(amountApplied)) {
				fc.utobj().throwsException("was not able to verify The Amount Paid");
			}

			fc.utobj().printTestStep("Navigate To Finance > Royalties");
			fc.finance().finance_common().financeRoyaltiesPage(driver);

			fc.utobj().printTestStep("Filter Sales Report By Franchise Id");
			new FinanceSalesPageTest().filterSalesReport(driver, franchiseId);

			fc.utobj().printTestStep("Verify Payment Status");
			boolean isTextPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[contains(text () ,'" + franchiseId
					+ "')]/ancestor::tr/td[contains(text () ,'Partially Paid')]");

			if (isTextPresent == false) {
				fc.utobj().throwsException("was not able to verify the Fully Paid Status Of Payments");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "finance" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2017-06-07", testCaseId = "TC_FC_Apply_Payment_forAdditionalFees_Royalties_117", testCaseDescription = "Verify apply Payment for Additional Fees Via Royalties")
	public void applyPaymentforAdditionalFeesViaRoyalties() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("finance", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		String agrmntVrsnName = fc.utobj().generateTestData(dataSet.get("agreementVersionName"));
		String royaltyPrcntg = dataSet.get("royaltyPercentage");
		String advPercentage = dataSet.get("advertisementPercentage");
		String royaltyAreaFranchiseValue = dataSet.get("RoyaltyAreaFranchise");
		String advAreaFranchiseValue = dataSet.get("AdvAreaFranchise");
		String itemValue= "00", itemType="Variable";
		
		try {
			driver = fc.loginpage().login(driver);
			FinanceSalesPage pobj = new FinanceSalesPage(driver);
			FinanceSalesPageTest sales_PageTest = new FinanceSalesPageTest();

			fc.utobj().printTestStep("Navigate To Admin > Finance > Additional > Invoice Item Summary Page");
			fc.utobj().printTestStep("addAdditionalInvoiceItems");
			String invoicItem = fc.utobj().generateTestData("InvoiceItem");
			new AdminFinanceAdditionalInvoiceItemSummaryPageTest().addAdditionalInvoiceItems(driver, invoicItem, itemValue, itemType);

			fc.utobj().printTestStep("Navigate To Admin > Finance > Agreement Versions");
			AdminFinanceAgreementVersionsPageTest agreementforSales = new AdminFinanceAgreementVersionsPageTest();

			fc.utobj().printTestStep("Add Agreement");
			String agreementVersion = agreementforSales.addAgreement(driver, config, agrmntVrsnName,
					 royaltyPrcntg, advPercentage, royaltyAreaFranchiseValue, advAreaFranchiseValue);

			fc.utobj().printTestStep("Navigate To Admin > Franchise Location > Add Franchise Location");
			fc.utobj().printTestStep("Add Franchise Location");

			String regionName = fc.utobj().generateTestData("GT");
			String storeType = fc.utobj().generateTestData("GT");
			String franchiseId = fc.utobj().generateTestData("GT");
			AdminFranchiseLocationAddFranchiseLocationPageTest franchise = new AdminFranchiseLocationAddFranchiseLocationPageTest();
			franchiseId = franchise.addFranchiseLocation_All_Agreement(driver, franchiseId, regionName, storeType,
					agreementVersion, config);

			fc.utobj().printTestStep("Navigate To Admin > Finance > Finance Setup Preferences");
			fc.utobj().printTestStep("Setup Medium of submission of Sales Reports");
			String salesReportType = "webForm";
			new AdminFinanceFinanceSetupPreferencesPageTest().mediumOfSubmissionOfSalesReports(driver, config,
					salesReportType);

			fc.utobj().printTestStep("Set Invoice Frequency");
			new AdminFinanceFinanceSetupPreferencesPageTest().setInvoice(driver, config);

			fc.utobj().printTestStep("Navigate To Finance > Sales");
			fc.utobj().printTestStep("Enter Sales Report");
			String categoryQuantity = "110";
			String categoryAmount = "6000";
			String additionalInvoiceValue = "400";
			String reportId = sales_PageTest.enterSalesReportWithAdditionInvoice(driver, franchiseId, categoryQuantity,
					categoryAmount, invoicItem, additionalInvoiceValue);

			fc.utobj().printTestStep("Filter Sales Report By Franchise Id");
			sales_PageTest.filterSalesReport(driver, franchiseId);

			fc.utobj().printTestStep("Approve Sales Report");
			fc.utobj().actionImgOption(driver, reportId, "Approve");

			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().sendKeys(driver, pobj.commentTxtArea,
					"Test Approve Comments" + fc.utobj().generateRandomNumber());
			fc.utobj().clickElement(driver, pobj.SaveBtn);

			fc.utobj().clickElement(driver, pobj.cloaseBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Filter Sales Report By Franchise Id");
			sales_PageTest.filterSalesReport(driver, franchiseId);

			fc.utobj().printTestStep("Generate Invoice");
			fc.utobj().actionImgOption(driver, franchiseId, "Generate Invoice");

			fc.utobj().printTestStep("Navigate To Finance > Royalties Page");
			fc.finance().finance_common().financeRoyaltiesPage(driver);

			fc.utobj().printTestStep("Filter Sales Report By Franchise Id");
			sales_PageTest.filterSalesReport(driver, franchiseId);

			FinanceRoyaltiesPage royalty_Page = new FinanceRoyaltiesPage(driver);
			fc.utobj().printTestStep("Apply Payments For Additional Fee");
			fc.utobj().actionImgOption(driver, franchiseId, "Apply Payment");

			fc.utobj().clickElement(driver, royalty_Page.selectPaymentAppliedforAdditionalfees);
			fc.utobj().clickElement(driver, royalty_Page.continueBtn);

			fc.utobj().printTestStep("Apply Payment By Cheque Option");
			FinancePaymentsPage payments_page = new FinancePaymentsPage(driver);
			fc.utobj().sendKeys(driver, payments_page.recievedDate, fc.utobj().getCurrentDateUSFormat());

			fc.utobj().sendKeys(driver, payments_page.paymentAmount, additionalInvoiceValue);
			fc.utobj().printTestStep("Test Ref No" + fc.utobj().generateRandomChar());

			if (!fc.utobj().isSelected(driver, payments_page.collectionMethodsCreditCard)) {
				fc.utobj().clickElement(driver, payments_page.collectionMethodsCreditCard);
			}

			fc.utobj().selectDropDownByValue(driver, payments_page.selectCreditType, "405");

			fc.utobj().sendKeys(driver, payments_page.comments, "Test Comments" + fc.utobj().generateRandomChar());
			fc.utobj().clickElement(driver, payments_page.saveBtn);

			fc.utobj().printTestStep("Veriy The Adv Amount Paid");
			fc.utobj().selectDropDownByValue(driver, payments_page.resultsPerPage, "500");

			String amountApplied = fc.utobj().getText(driver, driver
					.findElement(By.xpath(".//td[contains(text () , '" + franchiseId + "')]/ancestor::tr/td[4]")));

			if (amountApplied.contains(",")) {
				amountApplied = amountApplied.replaceAll(",", "");
			}

			if (Double.parseDouble(additionalInvoiceValue) != Double.parseDouble(amountApplied)) {
				fc.utobj().throwsException("was not able to verify The Amount Paid");
			}

			fc.utobj().printTestStep("Navigate To Finance > Royalties");
			fc.finance().finance_common().financeRoyaltiesPage(driver);

			fc.utobj().printTestStep("Filter Sales Report By Franchise Id");
			new FinanceSalesPageTest().filterSalesReport(driver, franchiseId);

			fc.utobj().printTestStep("Verify Payment Status");
			boolean isTextPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[contains(text () ,'" + franchiseId
					+ "')]/ancestor::tr/td[contains(text () ,'Partially Paid')]");

			if (isTextPresent == false) {
				fc.utobj().throwsException("was not able to verify the Fully Paid Status Of Payments");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "finance1123" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-08", testCaseId = "TC_FC_apply_Payement_forRoyalty_Invoice_Details_Page_118", testCaseDescription = "Verify apply Payement for Royalty Via Invoice Details Page")
	public void applyPayementforRoyaltyViaInvoiceDetailsPage() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		Map<String, String> dataSet = fc.utobj().readTestData("finance", testCaseId);

		String agrmntVrsnName = fc.utobj().generateTestData(dataSet.get("agreementVersionName"));
		String royaltyPrcntg = dataSet.get("royaltyPercentage");
		String advPercentage = dataSet.get("advertisementPercentage");
		String royaltyAreaFranchiseValue = dataSet.get("RoyaltyAreaFranchise");
		String advAreaFranchiseValue = dataSet.get("AdvAreaFranchise");


		try {
			driver = fc.loginpage().login(driver);
			FinanceSalesPage pobj = new FinanceSalesPage(driver);
			FinanceSalesPageTest sales_PageTest = new FinanceSalesPageTest();

			fc.utobj().printTestStep("Navigate To Admin > Finance > Agreement Versions");
			AdminFinanceAgreementVersionsPageTest agreementforSales = new AdminFinanceAgreementVersionsPageTest();

			fc.utobj().printTestStep("Add Agreement");
			String agreementVersion = agreementforSales.addAgreement(driver, config, agrmntVrsnName,
					 royaltyPrcntg, advPercentage, royaltyAreaFranchiseValue, advAreaFranchiseValue);

			fc.utobj().printTestStep("Navigate To Admin > Franchise Location > Add Franchise Location");
			fc.utobj().printTestStep("Add Franchise Location");

			String regionName = fc.utobj().generateTestData("Gt");
			String storeType = fc.utobj().generateTestData("Gt");
			String franchiseId = fc.utobj().generateTestData("Gt");
			AdminFranchiseLocationAddFranchiseLocationPageTest franchise = new AdminFranchiseLocationAddFranchiseLocationPageTest();
			franchiseId = franchise.addFranchiseLocation_All_Agreement(driver, franchiseId, regionName, storeType,
					agreementVersion, config);

			fc.utobj().printTestStep("Navigate To Admin > Finance > Finance Setup Preferences");
			fc.utobj().printTestStep("Setup Medium of submission of Sales Reports");
			String salesReportType = "webForm";
			new AdminFinanceFinanceSetupPreferencesPageTest().mediumOfSubmissionOfSalesReports(driver, config,
					salesReportType);

			fc.utobj().printTestStep("Set Invoice Frequency");
			new AdminFinanceFinanceSetupPreferencesPageTest().setInvoice(driver, config);

			fc.utobj().printTestStep("Navigate To Finance > Sales");
			fc.utobj().printTestStep("Enter Sales Report");
			String categoryQuantity = "110";
			String categoryAmount = "6000";
			String reportId = sales_PageTest.enterSalesReport(driver, franchiseId, categoryQuantity, categoryAmount);

			fc.utobj().printTestStep("Filter Sales Report By Franchise Id");
			sales_PageTest.filterSalesReport(driver, franchiseId);

			fc.utobj().printTestStep("Approve Sales Report");
			fc.utobj().actionImgOption(driver, reportId, "Approve");

			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().sendKeys(driver, pobj.commentTxtArea,
					"Test Approve Comments" + fc.utobj().generateRandomNumber());
			fc.utobj().clickElement(driver, pobj.SaveBtn);

			fc.utobj().clickElement(driver, pobj.cloaseBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Filter Sales Report By Franchise Id");
			sales_PageTest.filterSalesReport(driver, franchiseId);

			fc.utobj().printTestStep("Generate Invoice");
			fc.utobj().actionImgOption(driver, franchiseId, "Generate Invoice");

			fc.utobj().printTestStep("Navigate To Finance > Royalties Page");
			fc.finance().finance_common().financeRoyaltiesPage(driver);

			fc.utobj().printTestStep("Filter Sales Report By Franchise Id");
			sales_PageTest.filterSalesReport(driver, franchiseId);

			fc.utobj().printTestStep("View Invoice Details");
			fc.utobj().actionImgOption(driver, franchiseId, "View Invoice Details");

			FinanceRoyaltiesPage royalty_Page = new FinanceRoyaltiesPage(driver);

			fc.utobj().clickElement(driver, royalty_Page.applyPaymentInvoiceDetails);
			fc.utobj().clickElement(driver, royalty_Page.selectPaymentAppliedforRoyalty);
			fc.utobj().clickElement(driver, royalty_Page.continueBtn);

			fc.utobj().printTestStep("Apply Payment By Cheque Option");
			FinancePaymentsPage payments_page = new FinancePaymentsPage(driver);
			fc.utobj().sendKeys(driver, payments_page.recievedDate, fc.utobj().getCurrentDateUSFormat());

			double royaltyVal = Double.parseDouble(categoryAmount) * Double.parseDouble(royaltyPrcntg) / 100;

			fc.utobj().sendKeys(driver, payments_page.paymentAmount, Double.toString(royaltyVal));
			fc.utobj().printTestStep("Test Ref No" + fc.utobj().generateRandomChar());

			if (!fc.utobj().isSelected(driver, payments_page.collectionMethodsCreditCard)) {
				fc.utobj().clickElement(driver, payments_page.collectionMethodsCreditCard);
			}

			fc.utobj().selectDropDownByValue(driver, payments_page.selectCreditType, "405");

			fc.utobj().sendKeys(driver, payments_page.comments, "Test Comments" + fc.utobj().generateRandomChar());
			fc.utobj().clickElement(driver, payments_page.saveBtn);

			fc.utobj().printTestStep("Veriy The Adv Amount Paid");
			fc.utobj().selectDropDownByValue(driver, payments_page.resultsPerPage, "500");

			String amountApplied = fc.utobj().getText(driver, driver
					.findElement(By.xpath(".//td[contains(text () , '" + franchiseId + "')]/ancestor::tr/td[4]")));

			if (amountApplied.contains(",")) {
				amountApplied = amountApplied.replaceAll(",", "");
			}

			if (royaltyVal != Double.parseDouble(amountApplied)) {
				fc.utobj().throwsException("was not able to verify The Amount Paid");
			}

			fc.utobj().printTestStep("Navigate To Finance > Royalties");
			fc.finance().finance_common().financeRoyaltiesPage(driver);

			fc.utobj().printTestStep("Filter Sales Report By Franchise Id");
			new FinanceSalesPageTest().filterSalesReport(driver, franchiseId);

			fc.utobj().printTestStep("Verify Payment Status");
			boolean isTextPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[contains(text () ,'" + franchiseId
					+ "')]/ancestor::tr/td[contains(text () ,'Partially Paid')]");

			if (isTextPresent == false) {
				fc.utobj().throwsException("was not able to verify the Fully Paid Status Of Payments");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "finance" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2017-06-01", testCaseId = "TC_FC_apply_Payement_forAdvertisement_Invoice_Details_Page_119", testCaseDescription = "Verify apply Payement for Advertisement Via Invoice Details Page")
	public void applyPayementforAdvertisementViaInvoiceDetailsPage() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		Map<String, String> dataSet = fc.utobj().readTestData("finance", testCaseId);

		String agrmntVrsnName = fc.utobj().generateTestData(dataSet.get("agreementVersionName"));
		String royaltyPrcntg = dataSet.get("royaltyPercentage");
		String advPercentage = dataSet.get("advertisementPercentage");
		String royaltyAreaFranchiseValue = dataSet.get("RoyaltyAreaFranchise");
		String advAreaFranchiseValue = dataSet.get("AdvAreaFranchise");
		
		

		try {
			driver = fc.loginpage().login(driver);

			FinanceSalesPage pobj = new FinanceSalesPage(driver);
			FinanceSalesPageTest sales_PageTest = new FinanceSalesPageTest();

			fc.utobj().printTestStep("Navigate To Admin > Finance > Agreement Versions");
			AdminFinanceAgreementVersionsPageTest agreementforSales = new AdminFinanceAgreementVersionsPageTest();

			fc.utobj().printTestStep("Add Agreement");
			String agreementVersion = agreementforSales.addAgreement(driver, config, agrmntVrsnName,
					 royaltyPrcntg, advPercentage, royaltyAreaFranchiseValue, advAreaFranchiseValue);

			fc.utobj().printTestStep("Navigate To Admin > Franchise Location > Add Franchise Location");
			fc.utobj().printTestStep("Add Franchise Location");

			String regionName = fc.utobj().generateTestData("GT");
			String storeType = fc.utobj().generateTestData("GT");
			String franchiseId = fc.utobj().generateTestData("GT");
			AdminFranchiseLocationAddFranchiseLocationPageTest franchise = new AdminFranchiseLocationAddFranchiseLocationPageTest();
			franchiseId = franchise.addFranchiseLocation_All_Agreement(driver, franchiseId, regionName, storeType,
					agreementVersion, config);

			fc.utobj().printTestStep("Navigate To Admin > Finance > Finance Setup Preferences");
			fc.utobj().printTestStep("Setup Medium of submission of Sales Reports");
			String salesReportType = "webForm";
			new AdminFinanceFinanceSetupPreferencesPageTest().mediumOfSubmissionOfSalesReports(driver, config,
					salesReportType);

			fc.utobj().printTestStep("Set Invoice Frequency");
			new AdminFinanceFinanceSetupPreferencesPageTest().setInvoice(driver, config);

			fc.utobj().printTestStep("Navigate To Finance > Sales");
			fc.utobj().printTestStep("Enter Sales Report");
			String categoryQuantity = "110";
			String categoryAmount = "6000";
			String reportId = sales_PageTest.enterSalesReport(driver, franchiseId, categoryQuantity, categoryAmount);

			fc.utobj().printTestStep("Filter Sales Report By Franchise Id");
			sales_PageTest.filterSalesReport(driver, franchiseId);

			fc.utobj().printTestStep("Approve Sales Report");
			fc.utobj().actionImgOption(driver, reportId, "Approve");

			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().sendKeys(driver, pobj.commentTxtArea,
					"Test Approve Comments" + fc.utobj().generateRandomNumber());
			fc.utobj().clickElement(driver, pobj.SaveBtn);

			fc.utobj().clickElement(driver, pobj.cloaseBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Filter Sales Report By Franchise Id");
			sales_PageTest.filterSalesReport(driver, franchiseId);

			fc.utobj().printTestStep("Generate Invoice");
			fc.utobj().actionImgOption(driver, franchiseId, "Generate Invoice");

			fc.utobj().printTestStep("Navigate To Finance > Royalties Page");
			fc.finance().finance_common().financeRoyaltiesPage(driver);

			fc.utobj().printTestStep("Filter Sales Report By Franchise Id");
			sales_PageTest.filterSalesReport(driver, franchiseId);

			fc.utobj().printTestStep("View Invoice Details");
			fc.utobj().actionImgOption(driver, franchiseId, "View Invoice Details");

			FinanceRoyaltiesPage royalty_Page = new FinanceRoyaltiesPage(driver);

			double advVal = Double.parseDouble(categoryAmount) * Double.parseDouble(advPercentage) / 100;

			fc.utobj().clickElement(driver, royalty_Page.applyPaymentInvoiceDetails);
			fc.utobj().clickElement(driver, royalty_Page.selectPaymentAppliedforAdv);
			fc.utobj().clickElement(driver, royalty_Page.continueBtn);

			fc.utobj().printTestStep("Apply Payment By Cheque Option");
			FinancePaymentsPage payments_page = new FinancePaymentsPage(driver);
			fc.utobj().sendKeys(driver, payments_page.recievedDate, fc.utobj().getCurrentDateUSFormat());

			fc.utobj().sendKeys(driver, payments_page.paymentAmount, Double.toString(advVal));
			fc.utobj().printTestStep("Test Ref No" + fc.utobj().generateRandomChar());

			if (!fc.utobj().isSelected(driver, payments_page.collectionMethodsCreditCard)) {
				fc.utobj().clickElement(driver, payments_page.collectionMethodsCreditCard);
			}

			fc.utobj().selectDropDownByValue(driver, payments_page.selectCreditType, "405");

			fc.utobj().sendKeys(driver, payments_page.comments, "Test Comments" + fc.utobj().generateRandomChar());
			fc.utobj().clickElement(driver, payments_page.saveBtn);

			fc.utobj().printTestStep("Veriy The Adv Amount Paid");
			fc.utobj().selectDropDownByValue(driver, payments_page.resultsPerPage, "500");

			String amountApplied = fc.utobj().getText(driver, driver
					.findElement(By.xpath(".//td[contains(text () , '" + franchiseId + "')]/ancestor::tr/td[4]")));

			if (amountApplied.contains(",")) {
				amountApplied = amountApplied.replaceAll(",", "");
			}

			if (advVal != Double.parseDouble(amountApplied)) {
				fc.utobj().throwsException("was not able to verify The Amount Paid");
			}

			fc.utobj().printTestStep("Navigate To Finance > Royalties");
			fc.finance().finance_common().financeRoyaltiesPage(driver);

			fc.utobj().printTestStep("Filter Sales Report By Franchise Id");
			new FinanceSalesPageTest().filterSalesReport(driver, franchiseId);

			fc.utobj().printTestStep("Verify Payment Status");
			boolean isTextPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[contains(text () ,'" + franchiseId
					+ "')]/ancestor::tr/td[contains(text () ,'Partially Paid')]");

			if (isTextPresent == false) {
				fc.utobj().throwsException("was not able to verify the Fully Paid Status Of Payments");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "finance" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2017-06-15", testCaseId = "TC_FC_apply_Payement_forAdditional_Fees_Invoice_Details_Page_120", testCaseDescription = "Verify apply Payement for Additional Fees Via Invoice Details Page")
	public void applyPayementforAdditionalFeesViaInvoiceDetailsPage() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		Map<String, String> dataSet = fc.utobj().readTestData("finance", testCaseId);

		String agrmntVrsnName = fc.utobj().generateTestData(dataSet.get("agreementVersionName"));
		String royaltyPrcntg = dataSet.get("royaltyPercentage");
		String advPercentage = dataSet.get("advertisementPercentage");
		String royaltyAreaFranchiseValue = dataSet.get("RoyaltyAreaFranchise");
		String advAreaFranchiseValue = dataSet.get("AdvAreaFranchise");
		String itemValue= "00", itemType="Variable";
		
		try {
			driver = fc.loginpage().login(driver);

			FinanceSalesPage pobj = new FinanceSalesPage(driver);
			FinanceSalesPageTest sales_PageTest = new FinanceSalesPageTest();

			fc.utobj().printTestStep("Navigate To Admin > Finance > Additional > Invoice Item Summary Page");
			fc.utobj().printTestStep("addAdditionalInvoiceItems");
			String invoicItem = fc.utobj().generateTestData("InvoiceItem");
			new AdminFinanceAdditionalInvoiceItemSummaryPageTest().addAdditionalInvoiceItems(driver, invoicItem, itemValue, itemType);

			fc.utobj().printTestStep("Navigate To Admin > Finance > Agreement Versions");
			AdminFinanceAgreementVersionsPageTest agreementforSales = new AdminFinanceAgreementVersionsPageTest();

			fc.utobj().printTestStep("Add Agreement");
			String agreementVersion = agreementforSales.addAgreement(driver, config, agrmntVrsnName,
					 royaltyPrcntg, advPercentage, royaltyAreaFranchiseValue, advAreaFranchiseValue);

			fc.utobj().printTestStep("Navigate To Admin > Franchise Location > Add Franchise Location");
			fc.utobj().printTestStep("Add Franchise Location");

			
			String franchiseId = "GT";
			AdminFranchiseLocationAddNewFranchiseLocationPageTest franchise = new AdminFranchiseLocationAddNewFranchiseLocationPageTest();
			franchiseId = franchise.addFranchiseLocation(driver, franchiseId, agreementVersion, config);
			
			fc.utobj().printTestStep("Navigate To Admin > Finance > Finance Setup Preferences");
			fc.utobj().printTestStep("Setup Medium of submission of Sales Reports");
			String salesReportType = "webForm";
			new AdminFinanceFinanceSetupPreferencesPageTest().mediumOfSubmissionOfSalesReports(driver, config,
					salesReportType);

			fc.utobj().printTestStep("Set Invoice Frequency");
			new AdminFinanceFinanceSetupPreferencesPageTest().setInvoice(driver, config);

			fc.utobj().printTestStep("Navigate To Finance > Sales");
			fc.utobj().printTestStep("Enter Sales Report");
			String categoryQuantity = "110";
			String categoryAmount = "6000";
			String additionalInvoiceValue = "400";
			String reportId = sales_PageTest.enterSalesReportWithAdditionInvoice(driver, franchiseId, categoryQuantity,
					categoryAmount, invoicItem, additionalInvoiceValue);

			fc.utobj().printTestStep("Filter Sales Report By Franchise Id");
			sales_PageTest.filterSalesReport(driver, franchiseId);

			fc.utobj().printTestStep("Approve Sales Report");
			fc.utobj().actionImgOption(driver, reportId, "Approve");

			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().sendKeys(driver, pobj.commentTxtArea,
					"Test Approve Comments" + fc.utobj().generateRandomNumber());
			fc.utobj().clickElement(driver, pobj.SaveBtn);

			fc.utobj().clickElement(driver, pobj.cloaseBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Filter Sales Report By Franchise Id");
			sales_PageTest.filterSalesReport(driver, franchiseId);

			fc.utobj().printTestStep("Generate Invoice");
			fc.utobj().actionImgOption(driver, franchiseId, "Generate Invoice");

			fc.utobj().printTestStep("Navigate To Finance > Royalties Page");
			fc.finance().finance_common().financeRoyaltiesPage(driver);

			fc.utobj().printTestStep("Filter Sales Report By Franchise Id");
			sales_PageTest.filterSalesReport(driver, franchiseId);

			fc.utobj().printTestStep("View Invoice Details");
			fc.utobj().actionImgOption(driver, franchiseId, "View Invoice Details");

			FinanceRoyaltiesPage royalty_page = new FinanceRoyaltiesPage(driver);

			fc.utobj().printTestStep("Apply Payment Invoice Details");
			fc.utobj().clickElement(driver, royalty_page.applyPaymentInvoiceDetails);

			fc.utobj().printTestStep("Select Payment Applied for Additional fees");
			fc.utobj().clickElement(driver, royalty_page.selectPaymentAppliedforAdditionalfees);
			fc.utobj().clickElement(driver, royalty_page.continueBtn);

			fc.utobj().printTestStep("Apply Payment By Cheque Option");
			FinancePaymentsPage payments_page = new FinancePaymentsPage(driver);
			fc.utobj().sendKeys(driver, payments_page.recievedDate, fc.utobj().getCurrentDateUSFormat());

			fc.utobj().sendKeys(driver, payments_page.paymentAmount, additionalInvoiceValue);
			fc.utobj().printTestStep("Test Ref No" + fc.utobj().generateRandomChar());

			if (!fc.utobj().isSelected(driver, payments_page.collectionMethodsCreditCard)) {
				fc.utobj().clickElement(driver, payments_page.collectionMethodsCreditCard);
			}

			fc.utobj().selectDropDownByValue(driver, payments_page.selectCreditType, "405");

			fc.utobj().sendKeys(driver, payments_page.comments, "Test Comments" + fc.utobj().generateRandomChar());
			fc.utobj().clickElement(driver, payments_page.saveBtn);

			fc.utobj().printTestStep("Veriy The Adv Amount Paid");
			fc.utobj().selectDropDownByValue(driver, payments_page.resultsPerPage, "500");

			String amountApplied = fc.utobj().getText(driver, driver
					.findElement(By.xpath(".//td[contains(text () , '" + franchiseId + "')]/ancestor::tr/td[4]")));

			if (amountApplied.contains(",")) {
				amountApplied = amountApplied.replaceAll(",", "");
			}

			if (Double.parseDouble(additionalInvoiceValue) != Double.parseDouble(amountApplied)) {
				fc.utobj().throwsException("was not able to verify The Amount Paid");
			}

			fc.utobj().printTestStep("Navigate To Finance > Royalties");
			fc.finance().finance_common().financeRoyaltiesPage(driver);

			fc.utobj().printTestStep("Filter Sales Report By Franchise Id");
			new FinanceSalesPageTest().filterSalesReport(driver, franchiseId);

			fc.utobj().printTestStep("Verify Payment Status");
			boolean isTextPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[contains(text () ,'" + franchiseId
					+ "')]/ancestor::tr/td[contains(text () ,'Partially Paid')]");

			if (isTextPresent == false) {
				fc.utobj().throwsException("was not able to verify the Fully Paid Status Of Payments");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "finance", "finance_FailedTC" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2017-06-02", testCaseId = "TC_FC_Filters_Royalties_Report_168", testCaseDescription = "Verify filters at Royalties Page")
	public void filtersAtRoyaltiesPage() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		Map<String, String> dataSet = fc.utobj().readTestData("finance", testCaseId);

		String agrmntVrsnName = fc.utobj().generateTestData(dataSet.get("agreementVersionName"));
		String royaltyPrcntg = dataSet.get("royaltyPercentage");
		String advPercentage = dataSet.get("advertisementPercentage");
		String royaltyAreaFranchiseValue = dataSet.get("RoyaltyAreaFranchise");
		String advAreaFranchiseValue = dataSet.get("AdvAreaFranchise");
		String itemValue= "00", itemType="Variable";
		
		try {
			driver = fc.loginpage().login(driver);
			FinanceSalesPage pobj = new FinanceSalesPage(driver);
			FinanceSalesPageTest sales_PageTest = new FinanceSalesPageTest();

			fc.utobj().printTestStep("Navigate To Admin > Finance > Additional > Invoice Item Summary Page");
			fc.utobj().printTestStep("addAdditionalInvoiceItems");
			String invoicItem = fc.utobj().generateTestData("InvoiceItem");
			new AdminFinanceAdditionalInvoiceItemSummaryPageTest().addAdditionalInvoiceItems(driver, invoicItem, itemValue, itemType);

			fc.utobj().printTestStep("Navigate To Admin > Finance > Agreement Versions");
			AdminFinanceAgreementVersionsPageTest agreementforSales = new AdminFinanceAgreementVersionsPageTest();

			fc.utobj().printTestStep("Add Agreement");
			String agreementVersion = agreementforSales.addAgreement(driver, config, agrmntVrsnName,
					 royaltyPrcntg, advPercentage, royaltyAreaFranchiseValue, advAreaFranchiseValue);

			fc.utobj().printTestStep("Navigate To Admin > Franchise Location > Add Franchise Location");
			fc.utobj().printTestStep("Add Franchise Location");

			String regionName = fc.utobj().generateTestData("GT");
			String storeType = fc.utobj().generateTestData("GT");
			String franchiseId = fc.utobj().generateTestData("GT");
			AdminFranchiseLocationAddFranchiseLocationPageTest franchise = new AdminFranchiseLocationAddFranchiseLocationPageTest();
			franchiseId = franchise.addFranchiseLocation_All_Agreement(driver, franchiseId, regionName, storeType,
					agreementVersion, config);

			fc.utobj().printTestStep("Navigate To Admin > Finance > Finance Setup Preferences");
			fc.utobj().printTestStep("Setup Medium of submission of Sales Reports");
			String salesReportType = "webForm";
			new AdminFinanceFinanceSetupPreferencesPageTest().mediumOfSubmissionOfSalesReports(driver, config,
					salesReportType);

			fc.utobj().printTestStep("Set Invoice Frequency");
			new AdminFinanceFinanceSetupPreferencesPageTest().setInvoice(driver, config);

			fc.utobj().printTestStep("Navigate To Finance > Sales");
			fc.utobj().printTestStep("Enter Sales Report");
			String categoryQuantity = "110";
			String categoryAmount = "6000";
			String additionalInvoiceValue = "400";
			String reportId = sales_PageTest.enterSalesReportWithAdditionInvoice(driver, franchiseId, categoryQuantity,
					categoryAmount, invoicItem, additionalInvoiceValue);

			fc.utobj().printTestStep("Filter Sales Report By Franchise Id");
			sales_PageTest.filterSalesReport(driver, franchiseId);

			fc.utobj().printTestStep("Approve Sales Report");
			fc.utobj().actionImgOption(driver, reportId, "Approve");

			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().sendKeys(driver, pobj.commentTxtArea, "Test Approve Comments" + fc.utobj().generateRandomNumber());
			fc.utobj().clickElement(driver, pobj.SaveBtn);

			fc.utobj().clickElement(driver, pobj.cloaseBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Filter Sales Report By Franchise Id");
			sales_PageTest.filterSalesReport(driver, franchiseId);

			fc.utobj().printTestStep("Generate Invoice");
			fc.utobj().actionImgOption(driver, franchiseId, "Generate Invoice");

		//	String franchiseId = "GTf06150113";
			
			fc.utobj().printTestStep("Navigate To Finance > Royalties Page");
			
			fc.finance().finance_common().financeRoyaltiesPage(driver);

			FinanceRoyaltiesPage royalty_page = new FinanceRoyaltiesPage(driver);

			fc.utobj().clickElement(driver, royalty_page.showFilterLnk);
			
			fc.utobj().clickElement(driver, royalty_page.clickFranchiseIDfilter);
			
			fc.utobj().clickElement(driver,	driver.findElement(By.xpath(".//div[@id='ms-parentfranchiseID']//input[@id='selectAll']")));
			
			fc.utobj().sendKeys(driver,	driver.findElement(By.xpath(".//div[@id='ms-parentfranchiseID']//input[@class='searchInputMultiple']")), franchiseId);
			
			fc.utobj().clickElement(driver,	driver.findElement(By.xpath(".//div[@id='ms-parentfranchiseID']//input[@id='selectAll']")));

			fc.utobj().sendKeys(driver, royalty_page.invoiceDateFrom, fc.utobj().getCurrentDateUSFormat());
			
			fc.utobj().sendKeys(driver, royalty_page.invoiceDateTo, fc.utobj().getCurrentDateUSFormat());
			
			fc.utobj().clickElement(driver, royalty_page.searchFilterBtn);

			int rowCount = driver.findElements(By.xpath(".//div[@id='printReady']//table[@class='summaryTblex']/tbody/tr")).size();
			
			if (rowCount > 2) {
				fc.utobj().throwsException("Filters at Royalties page are not working as expected");
			}
			
			
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	public void filtersatRoyaltiesPage(WebDriver driver, Map<String, String> config, String franchiseID,
			String invoiceID) throws Exception {

		FinanceRoyaltiesPage pobj = new FinanceRoyaltiesPage(driver);

		boolean financeModule = driver.findElement(By.xpath(".//li[@id='module_financials']/a")).isDisplayed();

		if (financeModule == false) {

			fc.utobj().clickElement(driver, pobj.moreBtn);
		}

		fc.utobj().clickElement(driver, pobj.financeBtn);

		fc.utobj().clickElement(driver, pobj.royaltiesTab);

		fc.utobj().clickElement(driver, pobj.showFilterLnk);

		fc.utobj().clickElement(driver, pobj.clickFranchiseIDfilter);

		fc.utobj().clickElement(driver,
				driver.findElement(By.xpath(".//div[@id='ms-parentfranchiseID']//input[@id='selectAll']")));

		fc.utobj().sendKeys(driver,
				driver.findElement(By.xpath(".//div[@id='ms-parentfranchiseID']//input[@class='searchInputMultiple']")),
				franchiseID);

		fc.utobj().clickElement(driver,
				driver.findElement(By.xpath(".//div[@id='ms-parentfranchiseID']//input[@id='selectAll']")));

		fc.utobj().clickElement(driver, pobj.storeTypeFilter);

		fc.utobj().clickElement(driver,
				driver.findElement(By.xpath(".//div[@id='ms-parentstoreType']//input[@id='selectAll']")));

		fc.utobj().sendKeys(driver,
				driver.findElement(By.xpath(".//div[@id='ms-parentstoreType']//input[@class='searchInputMultiple']")),
				"Default");

		fc.utobj().clickElement(driver,
				driver.findElement(By.xpath(".//div[@id='ms-parentstoreType']//input[@id='selectAll']")));

		fc.utobj().sendKeys(driver, pobj.invoiceDateFrom, "09/16/2016");

		fc.utobj().sendKeys(driver, pobj.invoiceDateTo, "09/16/2016");

		fc.utobj().sendKeys(driver, pobj.reportPeriodStartFrom, "05/01/2016");

		fc.utobj().sendKeys(driver, pobj.reportPeriodStartTo, "05/31/2016");
		
		fc.utobj().clickElement(driver, pobj.searchFilterBtn);

		int rowCount = driver.findElements(By.xpath(".//div[@id='printReady']//table[@class='summaryTblex']/tbody/tr"))
				.size();

		if (rowCount > 2) {

			fc.utobj().throwsException("Filters at Royalties page are not working as expected");
		}
	}

	public void filtersatPaymentSummaryPage(WebDriver driver, Map<String, String> config, String franchiseID,
			String invoiceID) throws Exception {

		FinanceRoyaltiesPage pobj = new FinanceRoyaltiesPage(driver);

		boolean financeModule = driver.findElement(By.xpath(".//li[@id='module_financials']/a")).isDisplayed();

		if (financeModule == false) {

			fc.utobj().clickElement(driver, pobj.moreBtn);
		}

		fc.utobj().clickElement(driver, pobj.financeBtn);

		fc.utobj().clickElement(driver, pobj.paymentsTab);

		fc.utobj().clickElement(driver, pobj.showFilterLnk);

		fc.utobj().clickElement(driver, pobj.franchiseIDFilteratPaymentSummary);

		fc.utobj().clickElement(driver,
				driver.findElement(By.xpath(".//div[@id='ms-parentfranchiseNo']//input[@id='selectAll']")));

		fc.utobj().sendKeys(driver,
				driver.findElement(By.xpath(".//div[@id='ms-parentfranchiseNo']//input[@class='searchInputMultiple']")),
				franchiseID);

		fc.utobj().clickElement(driver,
				driver.findElement(By.xpath(".//div[@id='ms-parentfranchiseNo']//input[@id='selectAll']")));

		fc.utobj().clickElement(driver, pobj.storeTypeFilter);

		fc.utobj().clickElement(driver,
				driver.findElement(By.xpath(".//div[@id='ms-parentstoreType']//input[@id='selectAll']")));

		fc.utobj().sendKeys(driver,
				driver.findElement(By.xpath(".//div[@id='ms-parentstoreType']//input[@class='searchInputMultiple']")),
				"Default");

		fc.utobj().clickElement(driver,
				driver.findElement(By.xpath(".//div[@id='ms-parentstoreType']//input[@id='selectAll']")));

		fc.utobj().sendKeys(driver, pobj.dateAppliedFrom, "09/16/2016");

		fc.utobj().sendKeys(driver, pobj.dateAppliedTo, "09/16/2016");

		fc.utobj().sendKeys(driver, pobj.invoiceNo, invoiceID);

		fc.utobj().clickElement(driver, pobj.searchFilterBtn);

		int rowCount = driver.findElements(By.xpath(".//div[@id='printReady']//table[@class='summaryTblex']/tbody/tr"))
				.size();

		if (rowCount > 2) {

			fc.utobj().throwsException("Filters at Payment Summary page are not working as expected");
		}

	}

	public void filtersatPaymentsCreditDebit(WebDriver driver, Map<String, String> config, String franchiseID,
			String invoiceID) throws Exception {

		FinanceRoyaltiesPage pobj = new FinanceRoyaltiesPage(driver);

		boolean financeModule = driver.findElement(By.xpath(".//li[@id='module_financials']/a")).isDisplayed();

		if (financeModule == false) {

			fc.utobj().clickElement(driver, pobj.moreBtn);
		}

		fc.utobj().clickElement(driver, pobj.financeBtn);

		fc.utobj().clickElement(driver, pobj.paymentsTab);

		fc.utobj().clickElement(driver, pobj.creditsDebitsLnk);

		fc.utobj().clickElement(driver, pobj.showFilterLnk);

		fc.utobj().clickElement(driver, pobj.clickFranchiseIDfilter);

		fc.utobj().clickElement(driver,
				driver.findElement(By.xpath(".//div[@id='ms-parentfranchiseID']//input[@id='selectAll']")));

		fc.utobj().sendKeys(driver,
				driver.findElement(By.xpath(".//div[@id='ms-parentfranchiseID']//input[@class='searchInputMultiple']")),
				franchiseID);

		fc.utobj().clickElement(driver,
				driver.findElement(By.xpath(".//div[@id='ms-parentfranchiseID']//input[@id='selectAll']")));

		fc.utobj().clickElement(driver, pobj.storeTypeFilter);

		fc.utobj().clickElement(driver,
				driver.findElement(By.xpath(".//div[@id='ms-parentstoreType']//input[@id='selectAll']")));

		fc.utobj().sendKeys(driver,
				driver.findElement(By.xpath(".//div[@id='ms-parentstoreType']//input[@class='searchInputMultiple']")),
				"Default");

		fc.utobj().clickElement(driver,
				driver.findElement(By.xpath(".//div[@id='ms-parentstoreType']//input[@id='selectAll']")));

		fc.utobj().sendKeys(driver, pobj.memoDateFrom, "09/16/2016");

		fc.utobj().sendKeys(driver, pobj.memoDateTo, "09/16/2016");

		fc.utobj().clickElement(driver, pobj.creditMemoCheck);

		fc.utobj().clickElement(driver, pobj.debitMemoCheck);

		fc.utobj().clickElement(driver, pobj.paymentMemoCheck);

		fc.utobj().clickElement(driver, pobj.searchFilterBtn);

		int rowCount = driver.findElements(By.xpath(".//div[@id='printReady']//table[@class='summaryTblex']/tbody/tr"))
				.size();

		if (rowCount > 2) {

			fc.utobj().throwsException("Filters at Payments Credit / Debit page are not working as expected");
		}
	}

	public String applyPayment(WebDriver driver, Map<String, String> config, String invoiceID, String paymentAmount)
			throws Exception {

		FinanceRoyaltiesPage pobj = new FinanceRoyaltiesPage(driver);
		fc.utobj().sendKeys(driver, pobj.paymentAmount, paymentAmount);
		fc.utobj().clickElement(driver, pobj.collectionMethodCash);
		fc.utobj().sendKeys(driver, pobj.comments, "Automation Testing");
		fc.utobj().clickElement(driver, pobj.saveBtn);
		String paymentID = driver.findElement(By.xpath(".//*[contains(text(), '" + invoiceID
				+ "')]/ancestor::tr[@class='bText12' or @class='bText12 grAltRw3']/td[1]/a")).getText();
		return paymentID;
	}

	public String returnMonthName(String monthName) {

		String month = null;

		if (monthName.equalsIgnoreCase("January")) {

			month = "December";

		} else if (monthName.equalsIgnoreCase("February")) {
			month = "January";
		} else if (monthName.equalsIgnoreCase("March")) {
			month = "February";
		} else if (monthName.equalsIgnoreCase("April")) {
			month = "March";
		} else if (monthName.equalsIgnoreCase("May")) {
			month = "April";
		} else if (monthName.equalsIgnoreCase("June")) {
			month = "May";
		} else if (monthName.equalsIgnoreCase("July")) {
			month = "June";
		} else if (monthName.equalsIgnoreCase("August")) {
			month = "July";
		} else if (monthName.equalsIgnoreCase("September")) {
			month = "August";
		} else if (monthName.equalsIgnoreCase("October")) {
			month = "September";
		} else if (monthName.equalsIgnoreCase("November")) {
			month = "October";
		} else if (monthName.equalsIgnoreCase("December")) {
			month = "November";
		}
		return month;
	}

	public String applyPaymentforAllPaymentsPage(WebDriver driver, Map<String, String> config, String franchiseID,
			String invoiceID, String paymentAmount, String testCaseId) throws Exception {

		FinanceRoyaltiesPage pobj = new FinanceRoyaltiesPage(driver);

		fc.utobj().clickElement(driver, pobj.paymentsTab);

		fc.utobj().clickElement(driver, pobj.applyReceivePaymentLnk);

		fc.utobj().selectDropDownByPartialText(driver, pobj.franchiseIDforPayment, franchiseID);

		fc.utobj().clickElement(driver, pobj.selectPaymentAppliedforAll);

		fc.utobj().clickElement(driver, pobj.continueBtn);

		fc.utobj().sendKeys(driver, pobj.paymentAmount, paymentAmount);

		fc.utobj().clickElement(driver, pobj.collectionMethodCash);

		fc.utobj().sendKeys(driver, pobj.comments, "Automation Testing");

		fc.utobj().clickElement(driver, pobj.saveBtn);

		String paymentID = driver.findElement(By.xpath(".//*[contains(text(), '" + invoiceID
				+ "')]/ancestor::tr[@class='bText12' or @class='bText12 grAltRw3']/td[1]/a")).getText();

		boolean isPaymentIDPresent = fc.utobj()
				.assertLinkText(driver,
						driver.findElement(By.xpath(".//*[contains(text(), '" + invoiceID
								+ "')]/ancestor::tr[@class='bText12' or @class='bText12 grAltRw3']/td[1]/a"))
								.getText());

		if (isPaymentIDPresent == false) {

			fc.utobj().throwsException("Payment is Not Applied!");

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		}

		return paymentID;
	}

	public String applyPaymentViaRoyaltiesActionMenu(WebDriver driver, Map<String, String> config, String invoiceID,
			String paymentAmount, String testCaseId) throws Exception {

		FinanceRoyaltiesPage pobj = new FinanceRoyaltiesPage(driver);

		WebElement e1 = driver.findElement(By.xpath(".//div[@id='printReady']//*[contains(text(), '" + invoiceID
				+ "')]/ancestor::tr[@class='bText12' or @class='bText12 grAltRw3']//div[@id='menuBar']/layer"));
		String actionToTake = "Apply Payment";

		String layerId = e1.getAttribute("id");

		WebElement element1 = driver.findElement(By.xpath(".//div[@id='printReady']//*[contains(text(), '" + invoiceID
				+ "')]/ancestor::tr[@class='bText12' or @class='bText12 grAltRw3']//div[@id='menuBar']/layer/a"));

		fc.utobj().clickElement(driver, element1);

		layerId = layerId.replace("Actions_dynamicmenu", "");

		layerId = layerId.replace("Bar", "");

		WebElement element2 = driver.findElement(By.xpath(
				".//*[@id='Actions_dynamicmenu" + layerId + "Menu']/span[contains(text(),'" + actionToTake + "')]"));
		fc.utobj().clickElement(driver, element2);

		fc.utobj().clickElement(driver, pobj.selectPaymentAppliedforAll);

		fc.utobj().clickElement(driver, pobj.continueBtn);

		fc.utobj().sendKeys(driver, pobj.paymentAmount, paymentAmount);

		fc.utobj().clickElement(driver, pobj.collectionMethodCash);

		fc.utobj().sendKeys(driver, pobj.comments, "Automation Testing");

		fc.utobj().clickElement(driver, pobj.saveBtn);

		String paymentID = driver.findElement(By.xpath(".//*[contains(text(), '" + invoiceID
				+ "')]/ancestor::tr[@class='bText12' or @class='bText12 grAltRw3']/td[1]/a")).getText();

		boolean isPaymentIDPresent = fc.utobj()
				.assertLinkText(driver,
						driver.findElement(By.xpath(".//*[contains(text(), '" + invoiceID
								+ "')]/ancestor::tr[@class='bText12' or @class='bText12 grAltRw3']/td[1]/a"))
								.getText());

		if (isPaymentIDPresent == false) {

			fc.utobj().throwsException("Payment is Not Applied!");

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		}

		return paymentID;
	}

	public String applyPaymentforRoyaltyPaymentsPage(WebDriver driver, Map<String, String> config, String franchiseID,
			String invoiceID, String paymentAmount, String testCaseId) throws Exception {

		FinanceRoyaltiesPage pobj = new FinanceRoyaltiesPage(driver);

		fc.utobj().clickElement(driver, pobj.paymentsTab);

		fc.utobj().clickElement(driver, pobj.applyReceivePaymentLnk);

		fc.utobj().selectDropDownByPartialText(driver, pobj.franchiseIDforPayment, franchiseID);

		fc.utobj().clickElement(driver, pobj.selectPaymentAppliedforRoyalty);

		fc.utobj().clickElement(driver, pobj.continueBtn);

		fc.utobj().sendKeys(driver, pobj.paymentAmount, paymentAmount);

		fc.utobj().clickElement(driver, pobj.collectionMethodCash);

		fc.utobj().sendKeys(driver, pobj.comments, "Automation Testing");

		fc.utobj().clickElement(driver, pobj.saveBtn);

		String paymentID = driver.findElement(By.xpath(".//*[contains(text(), '" + invoiceID
				+ "')]/ancestor::tr[@class='bText12' or @class='bText12 grAltRw3']/td[1]/a")).getText();

		boolean isPaymentIDPresent = fc.utobj()
				.assertLinkText(driver,
						driver.findElement(By.xpath(".//*[contains(text(), '" + invoiceID
								+ "')]/ancestor::tr[@class='bText12' or @class='bText12 grAltRw3']/td[1]/a"))
								.getText());

		if (isPaymentIDPresent == false) {
			fc.utobj().throwsException("Payment is Not Applied!");

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		}

		return paymentID;
	}

	public String applyPaymentforAdvertisementPaymentsPage(WebDriver driver, Map<String, String> config,
			String franchiseID, String invoiceID, String paymentAmount, String testCaseId) throws Exception {

		FinanceRoyaltiesPage pobj = new FinanceRoyaltiesPage(driver);

		fc.utobj().clickElement(driver, pobj.paymentsTab);

		fc.utobj().clickElement(driver, pobj.applyReceivePaymentLnk);

		fc.utobj().selectDropDownByPartialText(driver, pobj.franchiseIDforPayment, franchiseID);

		fc.utobj().clickElement(driver, pobj.selectPaymentAppliedforAdv);

		fc.utobj().clickElement(driver, pobj.continueBtn);

		fc.utobj().sendKeys(driver, pobj.paymentAmount, paymentAmount);

		fc.utobj().clickElement(driver, pobj.collectionMethodCash);

		fc.utobj().sendKeys(driver, pobj.comments, "Automation Testing");

		fc.utobj().clickElement(driver, pobj.saveBtn);

		String paymentID = driver.findElement(By.xpath(".//*[contains(text(), '" + invoiceID
				+ "')]/ancestor::tr[@class='bText12' or @class='bText12 grAltRw3']/td[1]/a")).getText();

		boolean isPaymentIDPresent = fc.utobj()
				.assertLinkText(driver,
						driver.findElement(By.xpath(".//*[contains(text(), '" + invoiceID
								+ "')]/ancestor::tr[@class='bText12' or @class='bText12 grAltRw3']/td[1]/a"))
								.getText());

		if (isPaymentIDPresent == false) {

			fc.utobj().throwsException("Payment is Not Applied!");

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		}

		return paymentID;
	}

	public String applyPaymentforAdditionalFeesPaymentsPage(WebDriver driver, Map<String, String> config,
			String franchiseID, String invoiceID, String paymentAmount, String testCaseId) throws Exception {

		FinanceRoyaltiesPage pobj = new FinanceRoyaltiesPage(driver);

		fc.utobj().clickElement(driver, pobj.paymentsTab);

		fc.utobj().clickElement(driver, pobj.applyReceivePaymentLnk);

		fc.utobj().selectDropDownByPartialText(driver, pobj.franchiseIDforPayment, franchiseID);

		fc.utobj().clickElement(driver, pobj.selectPaymentAppliedforAdditionalfees);

		fc.utobj().clickElement(driver, pobj.continueBtn);

		fc.utobj().sendKeys(driver, pobj.paymentAmount, paymentAmount);

		fc.utobj().clickElement(driver, pobj.collectionMethodCash);

		fc.utobj().sendKeys(driver, pobj.comments, "Automation Testing");

		fc.utobj().clickElement(driver, pobj.saveBtn);

		String paymentID = driver.findElement(By.xpath(".//*[contains(text(), '" + invoiceID
				+ "')]/ancestor::tr[@class='bText12' or @class='bText12 grAltRw3']/td[1]/a")).getText();

		boolean isPaymentIDPresent = fc.utobj()
				.assertLinkText(driver,
						driver.findElement(By.xpath(".//*[contains(text(), '" + invoiceID
								+ "')]/ancestor::tr[@class='bText12' or @class='bText12 grAltRw3']/td[1]/a"))
								.getText());

		if (isPaymentIDPresent == false) {
			fc.utobj().throwsException("Payment is Not Applied!");
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		}
		return paymentID;
	}

	@Test(groups = "finance")
	@TestCase(createdOn = "2018-06-07", updatedOn = "2018-06-07", testCaseId = "TC_FC_Adjustment_Royalty", testCaseDescription = "Verify enter Adjustment")
	public void royaltyForAdjustment() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		Map<String, String> dataSet = fc.utobj().readTestData("finance", testCaseId);

		String agrmntVrsnName = fc.utobj().generateTestData("AV");
		String royaltyPrcntg = "10";
		String advPercentage = "8";
		String royaltyAreaFranchiseValue = "5";
		String advAreaFranchiseValue = "3";
		
		try {
			driver = fc.loginpage().login(driver);
			
			FinanceSalesPage pobj = new FinanceSalesPage(driver);
			
			FinanceSalesPageTest sales_pageTest = new FinanceSalesPageTest();
			
			fc.utobj().printTestStep("Navigate To Admin > Finance > Agreement Versions");
			AdminFinanceAgreementVersionsPageTest agreementforSales = new AdminFinanceAgreementVersionsPageTest();

			fc.utobj().printTestStep("Add Agreement");
			String agreementVersion = agreementforSales.addAgreement(driver, config, agrmntVrsnName,
					 royaltyPrcntg, advPercentage, royaltyAreaFranchiseValue, advAreaFranchiseValue);

			fc.utobj().printTestStep("Navigate To Admin > Franchise Location > Add Franchise Location");
			fc.utobj().printTestStep("Add Franchise Location");

			String regionName = fc.utobj().generateTestData("GT");
			String storeType = fc.utobj().generateTestData("Gt");
			String franchiseId = fc.utobj().generateTestData("GT");
			AdminFranchiseLocationAddFranchiseLocationPageTest franchise = new AdminFranchiseLocationAddFranchiseLocationPageTest();
			franchiseId = franchise.addFranchiseLocation_All_Agreement(driver, franchiseId, regionName, storeType,
					agreementVersion, config);

			fc.utobj().printTestStep("Navigate To Admin > Finance > Finance Setup Preferences");
			fc.utobj().printTestStep("Setup Medium of submission of Sales Reports");
			String salesReportType = "webForm";
			new AdminFinanceFinanceSetupPreferencesPageTest().mediumOfSubmissionOfSalesReports(driver, config,
					salesReportType);
			
			fc.utobj().printTestStep("Set Invoice Frequency");
			new AdminFinanceFinanceSetupPreferencesPageTest().setInvoice(driver, config);
			
			fc.utobj().printTestStep("Allow Adjustments after invoicing");
			String allowAdj ="Yes";
			
			new AdminFinanceFinanceSetupPreferencesPageTest().allowAdjustmentsAfterInvoicing(driver, config, allowAdj);
			
			fc.utobj().printTestStep("Navigate To Finance > Sales");
			fc.utobj().printTestStep("Enter Sales Report");
			
			String categoryQuantity = "110";
			String categoryAmount = "6000";
			String reportId = sales_pageTest.enterSalesReport(driver, franchiseId, categoryQuantity, categoryAmount);

			fc.utobj().printTestStep("Filter Sales Report By Franchise Id");
			sales_pageTest.filterSalesReport(driver, franchiseId);

			fc.utobj().printTestStep("Approve Sales Report");
			
			fc.utobj().actionImgOption(driver, reportId, "Approve");

			fc.commonMethods().switch_cboxIframe_frameId(driver);
			
			fc.utobj().sendKeys(driver, pobj.commentTxtArea, "Test Approve Comments" + fc.utobj().generateRandomNumber());
			
			fc.utobj().clickElement(driver, pobj.SaveBtn);

			fc.utobj().clickElement(driver, pobj.cloaseBtn);
			
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Filter Sales Report By Franchise Id");
			sales_pageTest.filterSalesReport(driver, franchiseId);

			fc.utobj().printTestStep("Generate Invoice");
			fc.utobj().actionImgOption(driver, franchiseId, "Generate Invoice");

			fc.utobj().printTestStep("Enter Adjustment");
			fc.utobj().actionImgOption(driver, franchiseId, "Enter Adjustment");

			categoryQuantity = "200";
			categoryAmount = "400";
			fc.utobj().sendKeys(driver,	driver.findElement(By.xpath(".//td[contains(text () , 'Service')]/following-sibling::td//input[contains(@name , 'quantity')]")), categoryQuantity);
			fc.utobj().sendKeys(driver,	driver.findElement(By.xpath(".//td[contains(text () , 'Service')]/following-sibling::td//input[contains(@name , 'amount')]")), categoryAmount);

			fc.utobj().sendKeys(driver, pobj.rcvdDate, fc.utobj().getFutureDateUSFormat(-1));

			fc.utobj().clickElement(driver, pobj.SaveBtn);

			if (!fc.utobj().isSelected(driver,pobj.acknowledgement)) {
				
				fc.utobj().clickElement(driver, pobj.acknowledgement);
			}
			fc.utobj().clickElement(driver, pobj.SaveBtn);

			String reportIdAdj = fc.utobj().getText(driver, pobj.reportId);
			
			fc.utobj().clickElement(driver, pobj.okayBtn);

			float salesTotal = 0;
			float royaltyVal = 0;
			float advVal = 0;

			try {

				salesTotal = Float.parseFloat(categoryAmount);

				float royaltyPrcntgVal = Float.parseFloat(royaltyPrcntg);
				
				royaltyVal = salesTotal * royaltyPrcntgVal / 100;

				float advPrcntgVal = Float.parseFloat(advPercentage);
				
				advVal = salesTotal * advPrcntgVal / 100;

			} catch (Exception e) {
				
				fc.utobj().throwsException("Problem In Test Data Supplied");
			}

			boolean isLinkTextPresent = fc.utobj().assertLinkText(driver, reportId);

			if (isLinkTextPresent == false) {

				fc.utobj().throwsException("was not able to verify Report At Finance Sales Page");
			}

			fc.utobj().printTestStep("Verify Total Sales At Finance > Sales Page");
			
			boolean toSal = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[.='"+franchiseId+"']/ancestor::tr/td[contains(text () , '"+salesTotal+"')]");

			if (toSal == false) {
				fc.utobj().throwsException("was not able to verify Total Sales");
			}

			fc.utobj().printTestStep("Verify Royalty At Finance > Sales Page");
			
			boolean toRoyalty = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[.='" + franchiseId + "']/ancestor::tr/td[contains(text () , '" + royaltyVal + "')]");

			if (toRoyalty == false) {
				fc.utobj().throwsException("was not able to verify Royalty");
			}

			fc.utobj().printTestStep("Verify Adv. At Finance > Sales Page");
			
			boolean toAdv = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[.='" + franchiseId + "']/ancestor::tr/td[contains(text () , '" + advVal + "')]");

			if (toAdv == false) {
				fc.utobj().throwsException("was not able to verify Adv. ");
			}

			fc.utobj().printTestStep("Verify Invoice Date Status");

			boolean toInvoiceDate = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[.='" + franchiseId + "']/ancestor::tr/td[contains(text () , 'Waiting For Approval')]");

			if (toInvoiceDate == false) {
				fc.utobj().throwsException("was not able to verify Invoice Date Status");
			}
						
			fc.finance().finance_common().financeSalesPage(driver);
			
			sales_pageTest.filterSalesReport(driver, franchiseId);
			
			fc.utobj().printTestStep("Approve Adjustment Sales Report");
			
			fc.utobj().actionImgOption(driver, reportIdAdj, "Approve");

			fc.commonMethods().switch_cboxIframe_frameId(driver);
			
			fc.utobj().sendKeys(driver, pobj.commentTxtArea, "Test Approve Comments" + fc.utobj().generateRandomNumber());
			
			fc.utobj().clickElement(driver, pobj.SaveBtn);

			fc.utobj().clickElement(driver, pobj.cloaseBtn);
			
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Filter Sales Report By Franchise Id");
			
			sales_pageTest.filterSalesReport(driver, franchiseId);
			
			fc.utobj().printTestStep("Generate Invoice for Adjustment");
			
			fc.utobj().actionImgOption(driver, reportIdAdj, "Generate Invoice");
			
			fc.finance().finance_common().financeRoyaltiesPage(driver);

			fc.utobj().printTestStep("Filter Report By Franchise Id");
			
			sales_pageTest.filterSalesReport(driver, franchiseId);
			
			boolean adjustmmentOnRoyalty = fc.utobj().assertLinkPartialText(driver, reportIdAdj);
			
			if(adjustmmentOnRoyalty == false) {
				
				fc.utobj().throwsException("Royalty for adjustment is not generated");
			}
			
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	
	@Test(groups = { "finance_FreshDB" })
	@TestCase(createdOn = "2018-06-22", updatedOn = "2018-06-22", testCaseId = "TC_FC_Royalty_With_Late_Fee", testCaseDescription = "To verify Late Fee in Royalty Report")
	public void verifyRoyaltyWithLateFee() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());
		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		Map<String, String> dataSet = fc.utobj().readTestData("finance", testCaseId);
		
		String agrmntVrsnName = fc.utobj().generateTestData("AV");
		String royaltyPrcntg = "10";
		String advPercentage = "8";
		String royaltyAreaFranchiseValue = "5";
		String advAreaFranchiseValue = "3";
		String lateFeeType="F", lateFeeAmount="50";
		String categoryQuantity="20", categoryAmount = "5000";
		
		try {
			driver = fc.loginpage().login(driver);
			FinanceSalesPage pobj = new FinanceSalesPage(driver);
			AdminFinanceFinanceSetupPreferencesPageTest setup = new AdminFinanceFinanceSetupPreferencesPageTest();
			
			fc.utobj().printTestStep("Add Agreement Version");
			AdminFinanceAgreementVersionsPageTest agreement = new AdminFinanceAgreementVersionsPageTest();
			String agreementVersion = agreement.addAgreementWithLateFee(driver, config, agrmntVrsnName,  royaltyPrcntg, advPercentage, royaltyAreaFranchiseValue, advAreaFranchiseValue, lateFeeType, lateFeeAmount);
			
			fc.utobj().printTestStep("Navigate To Admin > Franchise Location > Add Franchise Location");
			fc.utobj().printTestStep("Add Franchise Location");

			String franchiseId = fc.utobj().generateTestData("GT");
			
			AdminFranchiseLocationAddNewFranchiseLocationPageTest franchise = new AdminFranchiseLocationAddNewFranchiseLocationPageTest();
			franchiseId = franchise.addFranchiseLocation(driver, franchiseId, agreementVersion, config);
			
			fc.utobj().printTestStep("Navigate To Admin > Finance > Finance Setup Preferences");
			fc.utobj().printTestStep("Setup Medium of submission of Sales Reports");
			String salesReportType = "webForm";
			setup.mediumOfSubmissionOfSalesReports(driver, config, salesReportType);
			
			fc.utobj().printTestStep("Navigate To Finance > Sales");
			fc.utobj().printTestStep("Enter Sales Report");
			FinanceSalesPageTest Sales = new FinanceSalesPageTest();
			String reportId = Sales.enterSalesReport(driver, franchiseId, categoryQuantity, categoryAmount);
			
			fc.utobj().printTestStep("Filter Sales Report By Franchise Id");
			Sales.filterSalesReport(driver, franchiseId);

			fc.utobj().printTestStep("Approve Sales Report");
			fc.utobj().actionImgOption(driver, reportId, "Approve");

			fc.commonMethods().switch_cboxIframe_frameId(driver);
			
			fc.utobj().sendKeys(driver, pobj.commentTxtArea,
					"Test Approve Comments" + fc.utobj().generateRandomNumber());
			
			fc.utobj().clickElement(driver, pobj.SaveBtn);

			fc.utobj().clickElement(driver, pobj.cloaseBtn);
			
			fc.utobj().switchFrameToDefault(driver);
			
			fc.utobj().printTestStep("Filter Sales Report By Franchise Id");
			
			Sales.filterSalesReport(driver, franchiseId);
			
			fc.utobj().printTestStep("Generate Invoice");
			
			fc.utobj().actionImgOption(driver, franchiseId, "Generate Invoice");

			fc.utobj().printTestStep("Navigate To Finance > Royalties");
			
			fc.finance().finance_common().financeRoyaltiesPage(driver);

			fc.utobj().printTestStep("Filter Report By Franchise Id");
			
			Sales.filterSalesReport(driver, franchiseId);

			fc.utobj().printTestStep("View Invoice Details");
			
			fc.utobj().actionImgOption(driver, franchiseId, "View Invoice Details");

			fc.utobj().printTestStep("Verify The Royalty Amount");
			
			double royaltyVal = Double.parseDouble(categoryAmount) * Double.parseDouble(royaltyPrcntg) / 100;

			String royaltyValAtInvoice = fc.utobj().getText(driver, driver.findElement(By.xpath(
					".//*[contains(text () , 'Invoice Report Details')]/ancestor::tr[2]//td[contains(text () , 'Royalty')]/following-sibling::td")));

			if (royaltyValAtInvoice.contains(",")) {
				royaltyValAtInvoice = royaltyValAtInvoice.replaceAll(",", "");
			}

			if (royaltyVal != Double.parseDouble(royaltyValAtInvoice)) {
				
				fc.utobj().throwsException("was not able to verify the royality value");
			}

			fc.utobj().printTestStep("Verify The Adv. Amount");
			
			double advVal = Double.parseDouble(categoryAmount) * Double.parseDouble(advPercentage) / 100;

			String AdvValAtInvoice = fc.utobj().getText(driver, driver.findElement(By.xpath(
					".//*[contains(text () , 'Invoice Report Details')]/ancestor::tr[2]//td[contains(text () , 'Adv')]/following-sibling::td")));

			if (AdvValAtInvoice.contains(",")) {
				
				AdvValAtInvoice = AdvValAtInvoice.replaceAll(",", "");
			}

			if (advVal != Double.parseDouble(AdvValAtInvoice)) {
				
				fc.utobj().throwsException("was not able to verify the Adv. value");
			}

			fc.utobj().printTestStep("Verify The Invoice Amount");

			String InvoiceAmountAtInvoice = fc.utobj().getText(driver, driver.findElement(By.xpath(
					".//*[contains(text () , 'Invoice Report Details')]/ancestor::tr[2]//td[contains(text () , 'Invoice Amount')]/following-sibling::td")));

			if (InvoiceAmountAtInvoice.contains(",")) {
				
				InvoiceAmountAtInvoice = InvoiceAmountAtInvoice.replaceAll(",", "");
			}

			double invoiceValueCal = royaltyVal + advVal;

			if (invoiceValueCal != Double.parseDouble(InvoiceAmountAtInvoice)) {
				
				fc.utobj().throwsException("was not able to verify the invoice amount at Invoice Report Details page");
			}					
	
			
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
			
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	
	@Test(groups = { "finance_FreshDB" })
	@TestCase(createdOn = "2018-06-25", updatedOn = "2018-06-25", testCaseId = "TC_FC_Royalty_With_Tax", testCaseDescription = "To verify Tax in Royalty Report")
	public void verifyRoyaltyWithTax() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());
		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		Map<String, String> dataSet = fc.utobj().readTestData("finance", testCaseId);
		
		String agrmntVrsnName = fc.utobj().generateTestData("AV");
		String royaltyPrcntg = "10";
		String advPercentage = "8";
		String royaltyAreaFranchiseValue = "5";
		String advAreaFranchiseValue = "3";
		String lateFeeType="F", lateFeeAmount="50";
		String categoryQuantity="20", categoryAmount = "5000";
		String taxTypeName ="TaxType", taxRateName ="TaxRate";
				
		try {
			driver = fc.loginpage().login(driver);
			FinanceSalesPage pobj = new FinanceSalesPage(driver);
			AdminFinanceFinanceSetupPreferencesPageTest setup = new AdminFinanceFinanceSetupPreferencesPageTest();
			
			fc.utobj().printTestStep("Add Agreement Version");
			AdminFinanceAgreementVersionsPageTest agreement = new AdminFinanceAgreementVersionsPageTest();
			String agreementVersion = agreement.addAgreementWithLateFee(driver, config, agrmntVrsnName,  royaltyPrcntg, advPercentage, royaltyAreaFranchiseValue, advAreaFranchiseValue, lateFeeType, lateFeeAmount);
			
			AdminFinanceConfigureTaxRate taxPageTest = new AdminFinanceConfigureTaxRate();
			taxTypeName = taxPageTest.addTaxType(driver, config, taxTypeName);
			taxRateName = taxPageTest.addTaxRate(driver, config, taxRateName, taxTypeName);
			
			fc.utobj().printTestStep("Navigate To Admin > Franchise Location > Add Franchise Location");
			fc.utobj().printTestStep("Add Franchise Location");
			
			String franchiseId = fc.utobj().generateTestData("GT");
			
			AdminFranchiseLocationAddNewFranchiseLocationPageTest franchise = new AdminFranchiseLocationAddNewFranchiseLocationPageTest();
			
			franchiseId = franchise.addFranchiseLocationWithTax(driver, franchiseId, agreementVersion, taxRateName, config);
			
			fc.utobj().printTestStep("Navigate To Admin > Finance > Finance Setup Preferences");
			fc.utobj().printTestStep("Setup Medium of submission of Sales Reports");
			String salesReportType = "webForm";
			setup.mediumOfSubmissionOfSalesReports(driver, config, salesReportType);
			
			fc.utobj().printTestStep("Navigate To Finance > Sales");
			fc.utobj().printTestStep("Enter Sales Report");
			FinanceSalesPageTest Sales = new FinanceSalesPageTest();
			String reportId = Sales.enterSalesReport(driver, franchiseId, categoryQuantity, categoryAmount);
			
			fc.utobj().printTestStep("Filter Sales Report By Franchise Id");
			Sales.filterSalesReport(driver, franchiseId);
			
			fc.utobj().printTestStep("Approve Sales Report");
			fc.utobj().actionImgOption(driver, reportId, "Approve");
			
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			
			fc.utobj().sendKeys(driver, pobj.commentTxtArea,
					"Test Approve Comments" + fc.utobj().generateRandomNumber());
			
			fc.utobj().clickElement(driver, pobj.SaveBtn);
			
			fc.utobj().clickElement(driver, pobj.cloaseBtn);
			
			fc.utobj().switchFrameToDefault(driver);
			
			fc.utobj().printTestStep("Filter Sales Report By Franchise Id");
			
			Sales.filterSalesReport(driver, franchiseId);
			
			fc.utobj().printTestStep("Generate Invoice");
			
			fc.utobj().actionImgOption(driver, franchiseId, "Generate Invoice");

			fc.utobj().printTestStep("Navigate To Finance > Royalties");
			
			fc.finance().finance_common().financeRoyaltiesPage(driver);
			
			fc.utobj().printTestStep("Filter Report By Franchise Id");
			
			Sales.filterSalesReport(driver, franchiseId);

			fc.utobj().printTestStep("View Invoice Details");
			
			fc.utobj().actionImgOption(driver, franchiseId, "View Invoice Details");

			fc.utobj().printTestStep("Verify Tax Amount");
			
			double royaltyVal = Double.parseDouble(categoryAmount) * Double.parseDouble(royaltyPrcntg) / 100;

			String royaltyValAtInvoice = fc.utobj().getText(driver, driver.findElement(By.xpath(
					".//*[contains(text () , 'Invoice Report Details')]/ancestor::tr[2]//td[contains(text () , 'Royalty')]/following-sibling::td")));

			if (royaltyValAtInvoice.contains(",")) {
				royaltyValAtInvoice = royaltyValAtInvoice.replaceAll(",", "");
			}

			if (royaltyVal != Double.parseDouble(royaltyValAtInvoice)) {
				
				fc.utobj().throwsException("was not able to verify tax value");
			}

			fc.utobj().printTestStep("Verify The Adv. Amount");
			
			double advVal = Double.parseDouble(categoryAmount) * Double.parseDouble(advPercentage) / 100;

			String AdvValAtInvoice = fc.utobj().getText(driver, driver.findElement(By.xpath(
					".//*[contains(text () , 'Invoice Report Details')]/ancestor::tr[2]//td[contains(text () , 'Adv')]/following-sibling::td")));

			if (AdvValAtInvoice.contains(",")) {
				
				AdvValAtInvoice = AdvValAtInvoice.replaceAll(",", "");
			}

			if (advVal != Double.parseDouble(AdvValAtInvoice)) {
				
				fc.utobj().throwsException("was not able to verify the Adv. value");
			}

			fc.utobj().printTestStep("Verify The Invoice Amount");

			String InvoiceAmountAtInvoice = fc.utobj().getText(driver, driver.findElement(By.xpath(
					".//*[contains(text () , 'Invoice Report Details')]/ancestor::tr[2]//td[contains(text () , 'Invoice Amount')]/following-sibling::td")));

			if (InvoiceAmountAtInvoice.contains(",")) {
				
				InvoiceAmountAtInvoice = InvoiceAmountAtInvoice.replaceAll(",", "");
			}

			double invoiceValueCal = royaltyVal + advVal;

			if (invoiceValueCal != Double.parseDouble(InvoiceAmountAtInvoice)) {
				
				fc.utobj().throwsException("was not able to verify the invoice amount at Invoice Report Details page");
			}
			
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
			
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	

}
