package com.builds.test.fin;

import java.io.File;
import java.text.DecimalFormat;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.Reporter;
import org.testng.annotations.Test;
import com.builds.test.admin.AdminFranchiseLocationAddFranchiseLocationPageTest;
import com.builds.uimaps.fin.AdminFinanceDeleteSalesReportsPage;
import com.builds.uimaps.fin.AdminFinanceFinanceSetupPreferencesPage;
import com.builds.uimaps.fin.FinanceSalesPage;
import com.builds.utilities.FranconnectUtil;
import com.builds.utilities.TestCase;

public class FinanceSalesPageTest {
	FranconnectUtil fc = new FranconnectUtil();

	@Test(groups = { "finance" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-05-24", testCaseId = "TC_FC_Enter_Sales_Report_050", testCaseDescription = "Verify Enter Sales Report")
	public void enterSalesReport() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());
		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		Map<String, String> dataSet = fc.utobj().readTestData("finance", testCaseId);
		
		String agrmntVrsnName = fc.utobj().generateTestData(dataSet.get("agreementVersionName"));
		String royaltyPrcntg = dataSet.get("royaltyPercentage");
		String advPercentage = dataSet.get("advertisementPercentage");
		String royaltyAreaFranchiseValue = dataSet.get("RoyaltyAreaFranchise");
		String advAreaFranchiseValue = dataSet.get("AdvAreaFranchise");
		String itemValue= "00", itemType="Variable";
		
		try {
			driver = fc.loginpage().login(driver);
			FinanceSalesPage pobj = new FinanceSalesPage(driver);
			
			fc.utobj().printTestStep("Navigate To Admin > Finance > Agreement Versions");

			fc.utobj().printTestStep("Add Agreement Version");
			AdminFinanceAgreementVersionsPageTest agreementforSales = new AdminFinanceAgreementVersionsPageTest();
			String agreementVersion = agreementforSales.addAgreement(driver, config, agrmntVrsnName,royaltyPrcntg, advPercentage, royaltyAreaFranchiseValue, advAreaFranchiseValue);

			fc.utobj().printTestStep("Navigate To Admin > Finance > Configure Categories for Sales Report");
			AdminFinanceConfigureCategoriesforSalesReportPageTest sales_Report_Page = new AdminFinanceConfigureCategoriesforSalesReportPageTest();

			fc.utobj().printTestStep("Add Main Category With Calculation Type Is Addition");
			String categoryName = fc.utobj().generateTestData("TestCategoryAddition");
			categoryName = sales_Report_Page.addNewCategory(driver, config, categoryName, "Addition");

			fc.utobj().printTestStep("Add Main Category With Calculation Type Is Subtraction");
			String categoryNameSubtraction = fc.utobj().generateTestData("TestCategorySubtraction");
			sales_Report_Page.addNewCategory(driver, config, categoryNameSubtraction, "Subtraction");

			fc.utobj().printTestStep("Navigate To Admin > Finance > Additional Invoice Item(s) Summary");
			fc.utobj().printTestStep("Add New Item");
			String invoicItem = fc.utobj().generateTestData("TestInvoiceItem");
			new AdminFinanceAdditionalInvoiceItemSummaryPageTest().addAdditionalInvoiceItems(driver, invoicItem, itemValue, itemType);

			fc.utobj().printTestStep("Navigate To Admin > Finance > Non-Financial / KPI Categories Summary");
			fc.utobj().printTestStep("Add New Non-Financial Category");
			String category = fc.utobj().generateTestData("TestKPICategory");
			new AdminFinanceNonFinancialKPICategoriesSummaryPageTest().addNewNonFinancialsCategory(driver, config,
					category);

			fc.utobj().printTestStep("Navigate To Admin > Finance >  Configure Finance Document(s) to be Uploaded");
			fc.utobj().printTestStep("Add Document Name");
			String documentName = fc.utobj().generateTestData("TestDoc");
			new AdminFinanceConfigureFinanceDocumentUploadedPageTest().addDocumentName(driver, documentName);

			fc.utobj().printTestStep("Navigate To Admin > Franchise Location > Add Franchise Location");
			fc.utobj().printTestStep("Add Franchise Location");

			String franchiseId = fc.utobj().generateTestData("GT");
			AdminFranchiseLocationAddNewFranchiseLocationPageTest franchise = new AdminFranchiseLocationAddNewFranchiseLocationPageTest();
			franchiseId = franchise.addFranchiseLocation(driver, franchiseId, agreementVersion, config);
			fc.utobj().printTestStep("Navigate To Admin > Finance > Finance Setup Preferences");
			fc.utobj().printTestStep("Setup Medium of submission of Sales Reports");
			String salesReportType = "webForm";
			new AdminFinanceFinanceSetupPreferencesPageTest().mediumOfSubmissionOfSalesReports(driver, config,
					salesReportType);

			fc.utobj().printTestStep("Navigate To Finance > Sales");
			
			fc.finance().finance_common().financeSalesPage(driver);

			fc.utobj().printTestStep("Enter Sales Report");
			
			fc.utobj().clickElement(driver, pobj.enterSalesReport);
			
			fc.utobj().selectDropDown(driver, pobj.selectFranchise, franchiseId);

			fc.utobj().selectDropDownByIndex(driver, pobj.selectReportPeriod, 0);

			fc.utobj().clickElement(driver, pobj.ContinueBtn);

			fc.utobj().sendKeys(driver, pobj.rcvdDate, fc.utobj().getFutureDateUSFormat(-1));

			String mainCategoryQuantity = "110";
			fc.utobj()
					.sendKeys(driver,
							driver.findElement(By.xpath(".//td[contains(text () , '" + categoryName
									+ "')]/following-sibling::td//input[contains(@name , 'quantity')]")),
							mainCategoryQuantity);

			String mainCategoryAmount = "6000";
			fc.utobj()
					.sendKeys(driver,
							driver.findElement(By.xpath(".//td[contains(text () , '" + categoryName
									+ "')]/following-sibling::td//input[contains(@name , 'amount')]")),
							mainCategoryAmount);

			String mainCategoryQuantity1 = "40";
			fc.utobj().sendKeys(driver,
					driver.findElement(By.xpath(".//td[contains(text () , '" + categoryNameSubtraction
							+ "')]/following-sibling::td//input[contains(@name , 'quantity')]")),
					mainCategoryQuantity1);

			String mainCategoryAmount1 = "4000";
			fc.utobj()
					.sendKeys(driver,
							driver.findElement(By.xpath(".//td[contains(text () , '" + categoryNameSubtraction
									+ "')]/following-sibling::td//input[contains(@name , 'amount')]")),
							mainCategoryAmount1);

			String invoiceItemAmount = "3000";
			fc.utobj()
					.sendKeys(driver,
							driver.findElement(By.xpath(".//td[contains(text () , '" + invoicItem
									+ "')]/following-sibling::td//input[contains(@name , 'amount')]")),
							invoiceItemAmount);

			String KPIAmount = "750";
			fc.utobj().sendKeys(driver, driver.findElement(By.xpath(".//td[contains(text () , '" + category
					+ "')]/following-sibling::td//input[contains(@name , 'amount')]")), KPIAmount);
			String fileName = fc.utobj().getFilePathFromTestData(dataSet.get("fileName"));

			WebElement elementFile = driver.findElement(By.xpath(".//*[contains(text () , '" + documentName
					+ "')]/following-sibling::td/input[contains(@name , 'File')]"));
			
			fc.utobj().moveToElement(driver, elementFile);
			
			fc.utobj().sendKeys(driver, elementFile, fileName);

			fc.utobj().sendKeys(driver, pobj.salesComment, "Automation Sales Report");
			
			fc.utobj().clickElement(driver, pobj.SaveBtn);

			fc.utobj().printTestStep("Verify The Total Sales");

			float totalSales = Float.parseFloat(mainCategoryAmount) - Float.parseFloat(mainCategoryAmount1);

			String actualSales = fc.utobj().getText(driver,
					driver.findElement(By.xpath(".//td[contains(text () , 'Total Sales')]/following-sibling::td[1]")));

			if (actualSales.contains(",")) {
				actualSales = actualSales.replaceAll(",", "");
			}

			float actualSalesFloat = 0;

			try {

				actualSalesFloat = Float.parseFloat(actualSales);

				if (totalSales != actualSalesFloat) {
					fc.utobj().throwsException("was not able to verify total sales");
				}

			} catch (Exception e) {
				fc.utobj().throwsException("was not able to verify total sales");
			}

			fc.utobj().printTestStep("Verify Royalty Percentage");

			String royalty = fc.utobj().getText(driver, driver.findElement(By.xpath(".//td[contains(text () , 'Royalty')][not(contains(text () , 'Royalty (Area Franchise)'))]/following-sibling::td[1]")));

			if (royalty.contains(",")) {
				royalty = royalty.replaceAll(",", "");
			}

			float royaltyActual = 0;

			try {

				royaltyActual = Float.parseFloat(royalty);

				float royaltyPrcntgVal = Float.parseFloat(royaltyPrcntg);
				float royaltyVal = actualSalesFloat * royaltyPrcntgVal / 100;
				
				if (royaltyActual != royaltyVal) {

					fc.utobj().throwsException("was not able to verify Royalty");
				}

			} catch (Exception e) {
				fc.utobj().throwsException("was not able to verify Royalty");
			}

			fc.utobj().printTestStep("Verify Adv. Percentage");

			String adv = fc.utobj().getText(driver,
					driver.findElement(By.xpath(".//td[contains(text () , 'Adv. ($)')]/following-sibling::td[1]")));

			if (adv.contains(",")) {
				adv = adv.replaceAll(",", "");
			}

			float advActual = 0;
			float advVal = 0;
			try {
				advActual = Float.parseFloat(adv);

				float advPrcntgVal = Float.parseFloat(advPercentage);
				advVal = actualSalesFloat * advPrcntgVal / 100;

				if (advActual != advVal) {

				}
			} catch (Exception e) {
				fc.utobj().throwsException("was not able to verify Adv. ");
			}

			fc.utobj().printTestStep("Verify Royalty (Area Franchise)Percentage");

			String royaltyArea = fc.utobj().getText(driver, fc.utobj().getElementByXpath(driver,(".//td[contains(text () , 'Royalty (Area Franchise)')]/following-sibling::td[1]")));

			if (royaltyArea.contains(",")) {
				royaltyArea = royaltyArea.replaceAll(",", "");
			}

			try {
				float royaltyAreaActual = Float.parseFloat(royaltyArea);

				float royaltyAreaPrcntgVal = Float.parseFloat(royaltyAreaFranchiseValue);
				float royaltyAreaVal = royaltyActual * royaltyAreaPrcntgVal / 100;

				if (royaltyAreaActual != royaltyAreaVal) {

					fc.utobj().throwsException("was not able to verify Royalty (Area Franchise) Percentage");
				}
			} catch (Exception e) {
				fc.utobj().throwsException("was not able to verify Royalty (Area Franchise) Percentage");
			}

			fc.utobj().printTestStep("Verify Adv. (Area Franchise) Percentage");

			String advArea = fc.utobj().getText(driver, fc.utobj().getElementByXpath(driver,(".//td[contains(text () , 'Adv. (Area Franchise)')]/following-sibling::td[1]")));

			if (advArea.contains(",")) {
				
				advArea = advArea.replaceAll(",", "");
			}

			try {
				float advAreaActual = Float.parseFloat(advArea);

				float advAreaPrcntgVal = Float.parseFloat(advAreaFranchiseValue);
				
				float advAreaVal = advVal * advAreaPrcntgVal / 100;

				if (advAreaActual != advAreaVal) {

					fc.utobj().throwsException("was not able to verify Adv. (Area Franchise) Percentage");
				}
			} catch (Exception e) {
				
				fc.utobj().throwsException("was not able to verify Adv. (Area Franchise) Percentage");
			}

			String invoiceItemFinal = fc.utobj().getText(driver, driver
					.findElement(By.xpath(".//td[contains(text () , '" + invoicItem + "')]/following-sibling::td[1]")));

			if (invoiceItemFinal.contains(",")) {
				
				invoiceItemFinal = invoiceItemFinal.replaceAll(",", "");
			}

			String totalInvoiceAmount = fc.utobj().getText(driver, fc.utobj().getElementByXpath(driver,(".//td[contains(text () , 'Total Invoice Amount')]/following-sibling::td[1]")));

			if (totalInvoiceAmount.contains(",")) {
				
				totalInvoiceAmount = totalInvoiceAmount.replaceAll(",", "");
			}

			try {
				float invoiceItemFinalActual = Float.parseFloat(invoiceItemFinal);
				
				float totalInvoiceAmountCal = invoiceItemFinalActual + advActual + royaltyActual;
				
				float totalInvoiceAmountActual = Float.parseFloat(totalInvoiceAmount);

				if (totalInvoiceAmountCal != totalInvoiceAmountActual) {
					
					fc.utobj().throwsException("was not able to verify Total Invoice Amount");
				}
			} catch (Exception e) {
				
				fc.utobj().throwsException("was not able to verify Total Invoice Amount");
			}

			if (!fc.utobj().isSelected(driver,pobj.acknowledgement)) {
				
				fc.utobj().clickElement(driver, pobj.acknowledgement);
			}

			fc.utobj().clickElement(driver, pobj.submitBtn);

			String reportId = fc.utobj().getText(driver,
					driver.findElement(By.xpath(".//td[contains(text () , 'Report ID')]/following-sibling::td[1]")));

			fc.utobj().printTestStep("Verify The Total Quantity");

			String totalQuantity = fc.utobj().getText(driver, driver.findElement(By.xpath(
					".//table[@class='summaryTbl']//td[contains(text () , 'Total Sales')]/following-sibling::td[1]")));

			if (totalQuantity.contains(",")) {
				
				totalQuantity = totalQuantity.replaceAll(",", "");
			}

			try {
				float totalQuantityActual = Float.parseFloat(totalQuantity);

				float finalQuantity = Float.parseFloat(mainCategoryQuantity) + Float.parseFloat(mainCategoryQuantity1);

				if (finalQuantity != totalQuantityActual) {
					fc.utobj()
							.throwsException("was not abel to verify the total quantity at Sales Report Details Page");
				}
			} catch (Exception e) {
				
				fc.utobj().throwsException("was not abel to verify the total quantity at Sales Report Details Page");
			}

			fc.utobj().printTestStep("Verify The Non Financial Category Details");

			String KPIamount = fc.utobj().getText(driver, driver
					.findElement(By.xpath(".//td[contains(text () , '" + category + "')]/following-sibling::td[1]")));

			if (KPIamount.contains(",")) {
				
				KPIamount = KPIamount.replaceAll(",", "");
			}

			try {
				float KPIamountyActual = Float.parseFloat(KPIamount);

				if (Float.parseFloat(KPIAmount) != KPIamountyActual) {
				
					fc.utobj().throwsException("was not able to verify KPI Amount");
				}
			} catch (Exception e) {
				
				fc.utobj().throwsException("was not able to verify KPI Amount");
			}

			fc.utobj().clickElement(driver, pobj.okayBtn);

			fc.utobj().printTestStep("Filter Sales Report");

			fc.utobj().clickElement(driver, pobj.showFilter);
			
			fc.utobj().setToDefault(driver, pobj.selectFranchiseIdFilter);
			
			fc.utobj().selectValFromMultiSelect(driver, pobj.selectFranchiseIdFilter, fc.utobj()
					.getElement(driver, pobj.selectFranchiseIdFilter).findElement(By.xpath("./div/div/input")),
					franchiseId);
			
			fc.utobj().clickElement(driver, pobj.searchBtnFilter);

			fc.utobj().clickElement(driver, pobj.hideFilter);

			boolean isLinkTextPresent = fc.utobj().assertLinkText(driver, reportId);

			if (isLinkTextPresent == false) {

				fc.utobj().throwsException("was not able to verify Report At Finance Sales Page");
			}

			fc.utobj().printTestStep("Verify Total Sales At Finance > Sales Page");
			
			String totalSales12 = fc.utobj().getText(driver,
					driver.findElement(By.xpath(".//a[contains(text () , '" + franchiseId + "')]/ancestor::tr/td[7]")));

			if (totalSales12.contains(",")) {
				
				totalSales12 = totalSales12.replaceAll(",", "");
			}

			if (!totalSales12.equalsIgnoreCase(actualSales)) {
				
				fc.utobj().throwsException("was not able to verify Sales");
			}

			fc.utobj().printTestStep("Verify Royalty At Finance > Sales Page");
			
			boolean toRoyalty = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[.='" + franchiseId + "']/ancestor::tr/td[contains(text () , '" + royalty + "')]");

			if (toRoyalty == false) {
				
				fc.utobj().throwsException("was not able to verify Royalty");
			}

			fc.utobj().printTestStep("Verify Adv. At Finance > Sales Page");
			
			boolean toAdv = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[.='" + franchiseId + "']/ancestor::tr/td[contains(text () , '" + adv + "')]");

			if (toAdv == false) {
				
				fc.utobj().throwsException("was not able to verify Adv. ");
			}

			fc.utobj().printTestStep("Verify Invoice Date Status");

			boolean toInvoiceDate = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[.='" + franchiseId + "']/ancestor::tr/td[contains(text () , 'Waiting For Approval')]");

			if (toInvoiceDate == false) {
				
				fc.utobj().throwsException("was not able to verify Invoice Date Status");
			}
			
			fc.finance().finance_common().financeRoyaltiesPage(driver);

			fc.utobj().printTestStep("Filter Report By Franchise Id");
			
			filterSalesReport(driver, franchiseId);
			
			boolean isReportIDPresent = fc.utobj().assertLinkText(driver, reportId);

			if (isReportIDPresent == true) {

				fc.utobj().throwsException("Royalty Report present without invoice At Finance Royalty Page");
			}
			
			fc.utobj().printTestStep("Navigate to Finance > Store Summary");
			
			fc.finance().finance_common().financeStoreSummaryPage(driver);
			
			fc.utobj().printTestStep("Select Franchise from drop down filter");
			
			fc.utobj().clickElement(driver, driver.findElement(By.id("franchiseID")));
			
			fc.utobj().clickElement(driver, driver.findElement(By.xpath(".//*[contains(text(), '"+franchiseId+"')]")));
			
			fc.utobj().printTestStep("Click on Get Summary Button");
			
			fc.utobj().clickElement(driver, driver.findElement(By.id("getSummary")));
			
			fc.utobj().printTestStep("Verify Agreement Version at Summary Page");
			
			boolean isAgrementPresent = fc.utobj().assertLinkText(driver, agreementVersion);
			
			if (isAgrementPresent== false) {
				
				fc.utobj().throwsException("Agreement Version is not visible at Store Summary Page");
			}

			fc.utobj().printTestStep("Verify Agreement Version at Summary Page");
			
			boolean isSalesPresent = fc.utobj().assertLinkText(driver, reportId);
			
			if (isSalesPresent== false) {
				
				fc.utobj().throwsException("Sales Report is present in Sales Summary section of Store Summary");
			}
		/*	
			String royaltyAmount = fc.utobj().getText(driver, driver.findElement(By.xpath(".//tr[td[contains(text(), '"+reportId+"')]]//td[5]")));
			
			if (royaltyAmount.contains(",")) {
				royaltyAmount = royaltyAmount.replaceAll(",", "");
			}
			
			float royaltyAtSummary = 0;

			try {

				royaltyAtSummary = Float.parseFloat(royaltyAmount);

				float royaltyPrcntgVal = Float.parseFloat(royaltyPrcntg);
				float royaltyVal = actualSalesFloat * royaltyPrcntgVal / 100;
				
				if (royaltyAtSummary != royaltyVal) {

					fc.utobj().throwsException("was not able to verify Royalty under Sales Summary section in Store Summary Tab");
				}

			} catch (Exception e) {
				fc.utobj().throwsException("was not able to verify Royalty under Sales Summary section in Store Summary Tab");
			}

			// Verify reporting frequency
			
	*/		
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
			
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	public String enterSalesReport(WebDriver driver, String franchiseId, String categoryQuantity, String categoryAmount)
			throws Exception {

		FinanceSalesPage pobj = new FinanceSalesPage(driver);
		
		fc.finance().finance_common().financeSalesPage(driver);
		
		fc.utobj().clickElement(driver, pobj.enterSalesReport);
		
		fc.utobj().selectDropDown(driver, pobj.selectFranchise, franchiseId);

		fc.utobj().selectDropDownByIndex(driver, pobj.selectReportPeriod, 0);
		
		fc.utobj().clickElement(driver, pobj.ContinueBtn);
		
		fc.utobj().sendKeys(driver, pobj.rcvdDate, fc.utobj().getFutureDateUSFormat(-1));
		
		fc.utobj().sendKeys(driver,
				fc.utobj().getElementByXpath(driver,".//input[@name='finSalesReportDetails_0quantity']"),
				categoryQuantity);
		
		fc.utobj().sendKeys(driver,fc.utobj().getElementByXpath(driver,".//input[@name='finSalesReportDetails_0amount']"),
				categoryAmount);
		
		fc.utobj().clickElement(driver, pobj.SaveBtn);

		if (!fc.utobj().isSelected(driver,pobj.acknowledgement)) {
		
			fc.utobj().clickElement(driver, pobj.acknowledgement);
		}
		fc.utobj().clickElement(driver, pobj.SaveBtn);

		String reportId = fc.utobj().getText(driver, pobj.reportId);
		
		fc.utobj().clickElement(driver, pobj.okayBtn);

		return reportId;
	}
	
	public String enterSalesReportFinanceCRMTransactionEnable(WebDriver driver, String franchiseId)
			throws Exception {

		String reportId="";
		try {
			FinanceSalesPage pobj = new FinanceSalesPage(driver);
			fc.finance().finance_common().financeSalesPage(driver);
			fc.utobj().clickElement(driver, pobj.enterSalesReport);
			fc.utobj().selectDropDown(driver, pobj.selectFranchise, franchiseId);
			fc.utobj().selectDropDownByIndex(driver, pobj.selectReportPeriod, 0);
			fc.utobj().clickElement(driver, pobj.ContinueBtn);
			
			fc.utobj().clickElement(driver, pobj.SaveBtn);
			if (!fc.utobj().isSelected(driver,pobj.acknowledgement)) {
				fc.utobj().clickElement(driver, pobj.acknowledgement);
			}
			fc.utobj().clickElement(driver, pobj.SaveBtn);
			reportId = fc.utobj().getText(driver, pobj.reportId);
			fc.utobj().clickElement(driver, pobj.okayBtn);
		} catch (Exception e) {
			Reporter.log("Not able to Generate Sales Report : "+e.getMessage().toString());
		}
		return reportId;
	}

	public String enterSalesReportWithAdditionInvoice(WebDriver driver, String franchiseId, String categoryQuantity,
			String categoryAmount, String additionalInvoiveItem, String additionalInvoiceValue) throws Exception {

		FinanceSalesPage pobj = new FinanceSalesPage(driver);
		fc.finance().finance_common().financeSalesPage(driver);
		fc.utobj().clickElement(driver, pobj.enterSalesReport);
		fc.utobj().selectDropDown(driver, pobj.selectFranchise, franchiseId);

		fc.utobj().selectDropDownByIndex(driver, pobj.selectReportPeriod, 0);

		fc.utobj().clickElement(driver, pobj.ContinueBtn);

		fc.utobj().sendKeys(driver, pobj.rcvdDate, fc.utobj().getFutureDateUSFormat(-1));

		fc.utobj().sendKeys(driver,
				driver.findElement(By
						.xpath(".//td[contains(text () , 'Service')]/following-sibling::td//input[contains(@name , 'quantity')]")),
				categoryQuantity);
		fc.utobj().sendKeys(driver,
				driver.findElement(By
						.xpath(".//td[contains(text () , 'Service')]/following-sibling::td//input[contains(@name , 'amount')]")),
				categoryAmount);

		fc.utobj().sendKeys(driver,
				driver.findElement(By.xpath(".//td[contains(text () , '" + additionalInvoiveItem
						+ "')]/following-sibling::td//input[contains(@name , 'finAddlFeesDetails') and @class='fTextBox']")),
				additionalInvoiceValue);

		fc.utobj().clickElement(driver, pobj.SaveBtn);

		if (!fc.utobj().isSelected(driver,pobj.acknowledgement)) {
			fc.utobj().clickElement(driver, pobj.acknowledgement);
		}
		fc.utobj().clickElement(driver, pobj.SaveBtn);

		String reportId = fc.utobj().getText(driver, pobj.reportId);

		fc.utobj().clickElement(driver, pobj.okayBtn);

		return reportId;
	}

	@Test(groups = { "finance" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-05-24", testCaseId = "TC_FC_View_Sales_Report_075", testCaseDescription = "Verify View Sales Report")
	public void viewSalesDetailsViaAction() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		Map<String, String> dataSet = fc.utobj().readTestData("finance", testCaseId);

		String agrmntVrsnName = fc.utobj().generateTestData(dataSet.get("agreementVersionName"));
		String royaltyPrcntg = dataSet.get("royaltyPercentage");
		String advPercentage = dataSet.get("advertisementPercentage");
		String royaltyAreaFranchiseValue = dataSet.get("RoyaltyAreaFranchise");
		String advAreaFranchiseValue = dataSet.get("AdvAreaFranchise");
		
		

		try {
			driver = fc.loginpage().login(driver);

			fc.utobj().printTestStep("Navigate To Admin > Finance > Agreement Versions");
			AdminFinanceAgreementVersionsPageTest agreementforSales = new AdminFinanceAgreementVersionsPageTest();

			fc.utobj().printTestStep("Add Agreement");
			String agreementVersion = agreementforSales.addAgreement(driver, config, agrmntVrsnName, 
					 royaltyPrcntg, advPercentage, royaltyAreaFranchiseValue, advAreaFranchiseValue);

			fc.utobj().printTestStep("Navigate To Admin > Franchise Location > Add Franchise Location");
			fc.utobj().printTestStep("Add Franchise Location");

			String regionName = fc.utobj().generateTestData("GT");
			String storeType = fc.utobj().generateTestData("GT");
			String franchiseId = fc.utobj().generateTestData("GT");
			AdminFranchiseLocationAddFranchiseLocationPageTest franchise = new AdminFranchiseLocationAddFranchiseLocationPageTest();
			franchiseId = franchise.addFranchiseLocation_All_Agreement(driver, franchiseId, regionName, storeType,
					agreementVersion, config);

			fc.utobj().printTestStep("Navigate To Admin > Finance > Finance Setup Preferences");
			fc.utobj().printTestStep("Setup Medium of submission of Sales Reports");
			String salesReportType = "webForm";
			new AdminFinanceFinanceSetupPreferencesPageTest().mediumOfSubmissionOfSalesReports(driver, config,
					salesReportType);

			fc.utobj().printTestStep("Navigate To Finance > Sales");
			fc.utobj().printTestStep("Enter Sales Report");
			String categoryQuantity = "110";
			String categoryAmount = "6000";
			String reportId = enterSalesReport(driver, franchiseId, categoryQuantity, categoryAmount);

			fc.utobj().printTestStep("Filter Sales Report By Franchise Id");
			filterSalesReport(driver, franchiseId);

			fc.utobj().printTestStep("View Sales Report By Action Image Icon");
			fc.utobj().actionImgOption(driver, reportId, "View Sales Details");

			fc.utobj().printTestStep("Verify Total Sales Quantity");
			String totalQuantity = fc.utobj().getText(driver,
					driver.findElement(By.xpath(".//td[contains(text () ,' Total Sales')]/following-sibling::td[1]")));

			try {

				if (totalQuantity.contains(",")) {

					totalQuantity = totalQuantity.replaceAll(",", "");
				}

				float quantitiyValue = Float.parseFloat(totalQuantity);
				
				float categoryQuantityFloat = Float.parseFloat(categoryQuantity);

				if (quantitiyValue != categoryQuantityFloat) {

					fc.utobj().throwsException("was not able to verify Total Sales Quantitiy Value");
				}
			} catch (Exception e) {
				fc.utobj().throwsException("was not able to verify Total Sales Quantitiy Value");
			}

			fc.utobj().printTestStep("Verify Total Sales Amount");
			
			String totalAmount = fc.utobj().getText(driver,
					driver.findElement(By.xpath(".//td[contains(text () ,' Total Sales')]/following-sibling::td[2]")));

			try {

				if (totalAmount.contains(",")) {

					totalAmount = totalAmount.replaceAll(",", "");
				}

				float amountValue = Float.parseFloat(totalAmount);
				
				float categoryAmountFloat = Float.parseFloat(categoryAmount);

				if (amountValue != categoryAmountFloat) {

					fc.utobj().throwsException("was not able to verify Total Sales Amount Value");
				}
			} catch (Exception e) {
				fc.utobj().throwsException("was not able to verify Total Sales Amount Value");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "finance" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-05-24", testCaseId = "TC_FC_Approve_Sales_Report_051", testCaseDescription = "Verify Approve Sales Report")
	public void approveSalesReport() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());
		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		Map<String, String> dataSet = fc.utobj().readTestData("finance", testCaseId);

		String agrmntVrsnName = fc.utobj().generateTestData(dataSet.get("agreementVersionName"));
		String royaltyPrcntg = dataSet.get("royaltyPercentage");
		String advPercentage = dataSet.get("advertisementPercentage");
		String royaltyAreaFranchiseValue = dataSet.get("RoyaltyAreaFranchise");
		String advAreaFranchiseValue = dataSet.get("AdvAreaFranchise");
		
		

		try {
			driver = fc.loginpage().login(driver);
			FinanceSalesPage pobj = new FinanceSalesPage(driver);

			fc.utobj().printTestStep("Navigate To Admin > Finance > Agreement Versions");
			AdminFinanceAgreementVersionsPageTest agreementforSales = new AdminFinanceAgreementVersionsPageTest();

			fc.utobj().printTestStep("Add Agreement");
			String agreementVersion = agreementforSales.addAgreement(driver, config, agrmntVrsnName, 
					 royaltyPrcntg, advPercentage, royaltyAreaFranchiseValue, advAreaFranchiseValue);

			fc.utobj().printTestStep("Navigate To Admin > Franchise Location > Add Franchise Location");
			fc.utobj().printTestStep("Add Franchise Location");

			String regionName = fc.utobj().generateTestData("GT");
			String storeType = fc.utobj().generateTestData("GT");
			String franchiseId = fc.utobj().generateTestData("GT");
			AdminFranchiseLocationAddFranchiseLocationPageTest franchise = new AdminFranchiseLocationAddFranchiseLocationPageTest();
			franchiseId = franchise.addFranchiseLocation_All_Agreement(driver, franchiseId, regionName, storeType,
					agreementVersion, config);

			fc.utobj().printTestStep("Navigate To Admin > Finance > Finance Setup Preferences");
			fc.utobj().printTestStep("Setup Medium of submission of Sales Reports");
			String salesReportType = "webForm";
			new AdminFinanceFinanceSetupPreferencesPageTest().mediumOfSubmissionOfSalesReports(driver, config,
					salesReportType);

			fc.utobj().printTestStep("Navigate To Finance > Sales");
			fc.utobj().printTestStep("Enter Sales Report");
			String categoryQuantity = "110";
			String categoryAmount = "6000";
			String reportId = enterSalesReport(driver, franchiseId, categoryQuantity, categoryAmount);

			fc.utobj().printTestStep("Filter Sales Report By Franchise Id");
			filterSalesReport(driver, franchiseId);

			fc.utobj().printTestStep("Approve Sales Report");
			fc.utobj().actionImgOption(driver, reportId, "Approve");

			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().sendKeys(driver, pobj.commentTxtArea,
					"Test Approve Comments" + fc.utobj().generateRandomNumber());
			fc.utobj().clickElement(driver, pobj.SaveBtn);

			fc.utobj().clickElement(driver, pobj.cloaseBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Verify Approval");
			filterSalesReport(driver, franchiseId);

			fc.utobj().moveToElement(driver, fc.utobj().getElementByXpath(driver,(".//*[contains(text () ,'" + reportId + "')]/ancestor::tr/td/div[@id='menuBar']/layer")));
			String alterText = driver
					.findElement(By.xpath(
							".//*[contains(text () ,'" + reportId + "')]/ancestor::tr/td/div[@id='menuBar']/layer"))
					.getAttribute("id").trim();
			alterText = alterText.replace("Actions_dynamicmenu", "");
			alterText = alterText.replace("Bar", "");
			fc.utobj().clickElement(driver, driver.findElement(By.xpath(
					".//*[contains(text () ,'" + reportId + "')]/ancestor::tr/td/div[@id='menuBar']/layer/a/img")));
			List<WebElement> optionList = driver.findElements(By.xpath(".//div[@id='Actions_dynamicmenu1Menu']/span"));

			String option = null;
			boolean isOptionPresent = false;

			for (int i = 0; i < optionList.size(); i++) {

				option = optionList.get(i).getText().trim();

				if (option.equalsIgnoreCase("Approve")) {
					isOptionPresent = true;
					break;
				}
			}

			if (isOptionPresent == true) {
				fc.utobj().throwsException("was not able to Verify Approval Of Finance Sales Report");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "finance")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-05-24", testCaseId = "TC_FC_Reject_Sales_Report", testCaseDescription = "Verify Rejection Of Sales Report By Action Menu Option")
	public void rejectSalesReportViaActionMenu() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		Map<String, String> dataSet = fc.utobj().readTestData("finance", testCaseId);

		String agrmntVrsnName = fc.utobj().generateTestData(dataSet.get("agreementVersionName"));
		String royaltyPrcntg = dataSet.get("royaltyPercentage");
		String advPercentage = dataSet.get("advertisementPercentage");
		String royaltyAreaFranchiseValue = dataSet.get("RoyaltyAreaFranchise");
		String advAreaFranchiseValue = dataSet.get("AdvAreaFranchise");
		
		

		try {
			driver = fc.loginpage().login(driver);

			FinanceSalesPage pobj = new FinanceSalesPage(driver);

			fc.utobj().printTestStep("Navigate To Admin > Finance > Agreement Versions");
			AdminFinanceAgreementVersionsPageTest agreementforSales = new AdminFinanceAgreementVersionsPageTest();

			fc.utobj().printTestStep("Add Agreement");
			String agreementVersion = agreementforSales.addAgreement(driver, config, agrmntVrsnName, 
					 royaltyPrcntg, advPercentage, royaltyAreaFranchiseValue, advAreaFranchiseValue);

			fc.utobj().printTestStep("Navigate To Admin > Franchise Location > Add Franchise Location");
			fc.utobj().printTestStep("Add Franchise Location");

			String regionName = fc.utobj().generateTestData("Test");
			String storeType = fc.utobj().generateTestData("Test");
			String franchiseId = fc.utobj().generateTestData("Test");
			AdminFranchiseLocationAddFranchiseLocationPageTest franchise = new AdminFranchiseLocationAddFranchiseLocationPageTest();
			franchiseId = franchise.addFranchiseLocation_All_Agreement(driver, franchiseId, regionName, storeType,
					agreementVersion, config);

			fc.utobj().printTestStep("Navigate To Admin > Finance > Finance Setup Preferences");
			fc.utobj().printTestStep("Setup Medium of submission of Sales Reports");
			String salesReportType = "webForm";
			new AdminFinanceFinanceSetupPreferencesPageTest().mediumOfSubmissionOfSalesReports(driver, config,
					salesReportType);

			fc.utobj().printTestStep("Navigate To Finance > Sales");
			fc.utobj().printTestStep("Enter Sales Report");
			String categoryQuantity = "110";
			String categoryAmount = "6000";
			String reportId = enterSalesReport(driver, franchiseId, categoryQuantity, categoryAmount);

			fc.utobj().printTestStep("Filter Sales Report By Franchise Id");
			filterSalesReport(driver, franchiseId);

			fc.utobj().printTestStep("Reject Sales Report");
			fc.utobj().actionImgOption(driver, reportId, "Reject");

			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().sendKeys(driver, pobj.commentTxtArea,
					"Test Approve Comments" + fc.utobj().generateRandomNumber());
			fc.utobj().clickElement(driver, pobj.SaveBtn);

			fc.utobj().clickElement(driver, pobj.cloaseBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Verify Rejection");
			filterSalesReport(driver, franchiseId);

			fc.utobj().moveToElement(driver, fc.utobj().getElementByXpath(driver,(".//*[contains(text () ,'" + reportId + "')]/ancestor::tr/td/div[@id='menuBar']/layer")));
			String alterText = driver
					.findElement(By.xpath(
							".//*[contains(text () ,'" + reportId + "')]/ancestor::tr/td/div[@id='menuBar']/layer"))
					.getAttribute("id").trim();
			alterText = alterText.replace("Actions_dynamicmenu", "");
			alterText = alterText.replace("Bar", "");
			fc.utobj().clickElement(driver, driver.findElement(By.xpath(
					".//*[contains(text () ,'" + reportId + "')]/ancestor::tr/td/div[@id='menuBar']/layer/a/img")));
			List<WebElement> optionList = driver.findElements(By.xpath(".//div[@id='Actions_dynamicmenu1Menu']/span"));

			String option = null;
			boolean isOptionPresent = false;

			for (int i = 0; i < optionList.size(); i++) {

				option = optionList.get(i).getText().trim();

				if (option.equalsIgnoreCase("Reject")) {
					isOptionPresent = true;
					break;
				}
			}

			if (isOptionPresent == true) {
				fc.utobj().throwsException("was not able to verify Rejection Of Finance Sales Report");
			}

			boolean isStatusPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () , '" + reportId + "')]/ancestor::tr/td[contains(text () , 'Rejected')]");

			if (isStatusPresent == false) {

				fc.utobj().throwsException("was not able verify the status Of Sales Report");
			}

			//Modify rejected Sale
			
			fc.utobj().printTestStep("Modify Rejected Sales Report By Action Image Icon");
			
			fc.utobj().actionImgOption(driver, reportId, "Modify");
			
			categoryQuantity = "220.00";
			categoryAmount = "12000.00";

			fc.utobj().sendKeys(driver,
					driver.findElement(By
							.xpath(".//td[contains(text () , 'Service')]/following-sibling::td//input[contains(@name , 'quantity')]")),
					categoryQuantity);
			fc.utobj().sendKeys(driver,
					driver.findElement(By
							.xpath(".//td[contains(text () , 'Service')]/following-sibling::td//input[contains(@name , 'amount')]")),
					categoryAmount);

			String comments = "Test Comments" + fc.utobj().generateRandomNumber();
			
			fc.utobj().sendKeys(driver, pobj.salesComment, comments);

			fc.utobj().clickElement(driver, pobj.SaveBtn);

			if (!fc.utobj().isSelected(driver,pobj.acknowledgement)) {
				
				fc.utobj().clickElement(driver, pobj.acknowledgement);
			}
			fc.utobj().clickElement(driver, pobj.SaveBtn);
			
			fc.utobj().clickElement(driver, pobj.okayBtn);

			fc.utobj().printTestStep("verify Modified Details Of Sales Report");

			float salesTotal = 0;
			float royaltyVal = 0;
			float advVal = 0;

			try {

				salesTotal = Float.parseFloat(categoryAmount);

				float royaltyPrcntgVal = Float.parseFloat(royaltyPrcntg);
				royaltyVal = salesTotal * royaltyPrcntgVal / 100;

				float advPrcntgVal = Float.parseFloat(advPercentage);
				advVal = salesTotal * advPrcntgVal / 100;

			} catch (Exception e) {
				
				fc.utobj().throwsException("Problem In Test Data Supplied");
			}

			boolean isLinkTextPresent = fc.utobj().assertLinkText(driver, reportId);

			if (isLinkTextPresent == false) {

				fc.utobj().throwsException("was not able to verify Report At Finance Sales Page");
			}

			fc.utobj().printTestStep("Verify Total Sales At Finance > Sales Page");
			
			String totalSaleVal = fc.utobj().getText(driver,
					driver.findElement(By.xpath(".//a[contains(text () , '" + franchiseId + "')]/ancestor::tr/td[7]")));

			if (totalSaleVal.contains(",")) {
				
				totalSaleVal = totalSaleVal.replaceAll(",", "");
			}
			if (Float.parseFloat(categoryAmount) != Float.parseFloat(totalSaleVal)) {
				fc.utobj().throwsException("was not able to verify Total Sales");
			}

			fc.utobj().printTestStep("Verify Royalty At Finance > Sales Page");
			
			String royaltySaleVal = fc.utobj().getText(driver,
					driver.findElement(By.xpath(".//a[contains(text () , '" + franchiseId + "')]/ancestor::tr/td[8]")));

			if (royaltySaleVal.contains(",")) {
				
				royaltySaleVal = royaltySaleVal.replaceAll(",", "");
			}

			if (royaltyVal != Float.parseFloat(royaltySaleVal)) {
				
				fc.utobj().throwsException("was not able to verify Royalty");
			}

			fc.utobj().printTestStep("Verify Adv. At Finance > Sales Page");
			
			boolean toAdv = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[.='" + franchiseId + "']/ancestor::tr/td[contains(text () , '" + advVal + "')]");

			if (toAdv == false) {
				fc.utobj().throwsException("was not able to verify Adv. ");
			}

			fc.utobj().printTestStep("Verify Invoice Date Status");

			boolean toInvoiceDate = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[.='" + franchiseId + "']/ancestor::tr/td[contains(text () , 'Waiting For Approval')]");

			if (toInvoiceDate == false) {
				fc.utobj().throwsException("was not able to verify Invoice Date Status");
			}

			fc.utobj().printTestStep("Verify The View Action Logs");
			fc.utobj().actionImgOption(driver, reportId, "View Action Logs");

			fc.commonMethods().switch_cboxIframe_frameId(driver);
			List<WebElement> listW = driver.findElements(By.xpath(".//td[contains(text () , '" + comments
					+ "')]/preceding-sibling::td[contains(text () , 'Modified')]"));
			fc.utobj().clickElement(driver, pobj.cloaseBtn);
			fc.utobj().switchFrameToDefault(driver);

			if (listW.size() == 0) {
				fc.utobj().throwsException("was not able to verify the Created Action Logs");
			}
			
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "finance")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-05-24", testCaseId = "TC_FC_Delete_Sales_Report_By_ActionMenu_083", testCaseDescription = "Delete Sales Report By Action Menu Option")
	public void deleteSalesReportViaActionMenu() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		Map<String, String> dataSet = fc.utobj().readTestData("finance", testCaseId);

		String agrmntVrsnName = fc.utobj().generateTestData(dataSet.get("agreementVersionName"));
		String royaltyPrcntg = dataSet.get("royaltyPercentage");
		String advPercentage = dataSet.get("advertisementPercentage");
		String royaltyAreaFranchiseValue = dataSet.get("RoyaltyAreaFranchise");
		String advAreaFranchiseValue = dataSet.get("AdvAreaFranchise");
		
		

		try {
			driver = fc.loginpage().login(driver);
			FinanceSalesPage pobj = new FinanceSalesPage(driver);

			fc.utobj().printTestStep("Navigate To Admin > Finance > Agreement Versions");
			AdminFinanceAgreementVersionsPageTest agreementforSales = new AdminFinanceAgreementVersionsPageTest();

			fc.utobj().printTestStep("Add Agreement");
			String agreementVersion = agreementforSales.addAgreement(driver, config, agrmntVrsnName, 
					 royaltyPrcntg, advPercentage, royaltyAreaFranchiseValue, advAreaFranchiseValue);

			fc.utobj().printTestStep("Navigate To Admin > Franchise Location > Add Franchise Location");
			fc.utobj().printTestStep("Add Franchise Location");

			String regionName = fc.utobj().generateTestData("Test");
			String storeType = fc.utobj().generateTestData("Test");
			String franchiseId = fc.utobj().generateTestData("Test");
			AdminFranchiseLocationAddFranchiseLocationPageTest franchise = new AdminFranchiseLocationAddFranchiseLocationPageTest();
			franchiseId = franchise.addFranchiseLocation_All_Agreement(driver, franchiseId, regionName, storeType,
					agreementVersion, config);

			fc.utobj().printTestStep("Navigate To Admin > Finance > Finance Setup Preferences");
			fc.utobj().printTestStep("Setup Medium of submission of Sales Reports");
			String salesReportType = "webForm";
			new AdminFinanceFinanceSetupPreferencesPageTest().mediumOfSubmissionOfSalesReports(driver, config,
					salesReportType);

			fc.utobj().printTestStep("Navigate To Finance > Sales");
			fc.utobj().printTestStep("Enter Sales Report");
			String categoryQuantity = "110";
			String categoryAmount = "6000";
			String reportId = enterSalesReport(driver, franchiseId, categoryQuantity, categoryAmount);

			fc.utobj().printTestStep("Filter Sales Report By Franchise Id");
			filterSalesReport(driver, franchiseId);

			fc.utobj().printTestStep("Delete Sales Report");
			fc.utobj().actionImgOption(driver, reportId, "Delete");

			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().sendKeys(driver, pobj.commentTxtArea, "Delete Sales Report" + fc.utobj().generateRandomNumber());
			fc.utobj().clickElement(driver, pobj.deleteBtn);

			fc.utobj().clickElement(driver, pobj.cloaseBtn);

			fc.utobj().printTestStep("Verify The Deleted Sales Report");
			filterSalesReport(driver, franchiseId);

			boolean isSalesIdPresent = fc.utobj().assertPageSource(driver, reportId);

			if (isSalesIdPresent == true) {
				fc.utobj().throwsException("was not able to Deleted Sales Report");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "finance")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-05-24", testCaseId = "TC_FC_Modify_Sales_Details_By_ActionMenu_084", testCaseDescription = "Verify modify Sales Report Via Action Menu")
	public void modifySalesReportViaActionMenu() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		Map<String, String> dataSet = fc.utobj().readTestData("finance", testCaseId);

		String agrmntVrsnName = fc.utobj().generateTestData(dataSet.get("agreementVersionName"));
		String royaltyPrcntg = dataSet.get("royaltyPercentage");
		String advPercentage = dataSet.get("advertisementPercentage");
		String royaltyAreaFranchiseValue = dataSet.get("RoyaltyAreaFranchise");
		String advAreaFranchiseValue = dataSet.get("AdvAreaFranchise");
		
		

		try {
			driver = fc.loginpage().login(driver);

			FinanceSalesPage pobj = new FinanceSalesPage(driver);

			fc.utobj().printTestStep("Navigate To Admin > Finance > Agreement Versions");
			AdminFinanceAgreementVersionsPageTest agreementforSales = new AdminFinanceAgreementVersionsPageTest();

			fc.utobj().printTestStep("Add Agreement");
			String agreementVersion = agreementforSales.addAgreement(driver, config, agrmntVrsnName, 
					 royaltyPrcntg, advPercentage, royaltyAreaFranchiseValue, advAreaFranchiseValue);

			fc.utobj().printTestStep("Navigate To Admin > Franchise Location > Add Franchise Location");
			fc.utobj().printTestStep("Add Franchise Location");

			String regionName = fc.utobj().generateTestData("GT");
			String storeType = fc.utobj().generateTestData("GT");
			String franchiseId = fc.utobj().generateTestData("GT");
			AdminFranchiseLocationAddFranchiseLocationPageTest franchise = new AdminFranchiseLocationAddFranchiseLocationPageTest();
			franchiseId = franchise.addFranchiseLocation_All_Agreement(driver, franchiseId, regionName, storeType,
					agreementVersion, config);

			fc.utobj().printTestStep("Navigate To Admin > Finance > Finance Setup Preferences");
			fc.utobj().printTestStep("Setup Medium of submission of Sales Reports");
			String salesReportType = "webForm";
			new AdminFinanceFinanceSetupPreferencesPageTest().mediumOfSubmissionOfSalesReports(driver, config,
					salesReportType);

			fc.utobj().printTestStep("Navigate To Finance > Sales");
			fc.utobj().printTestStep("Enter Sales Report");
			String categoryQuantity = "110";
			String categoryAmount = "6000";
			String reportId = enterSalesReport(driver, franchiseId, categoryQuantity, categoryAmount);

			fc.utobj().printTestStep("Filter Sales Report By Franchise Id");
			filterSalesReport(driver, franchiseId);

			fc.utobj().printTestStep("Modify Sales Details");
			fc.utobj().actionImgOption(driver, reportId, "Modify Sales Details");

			categoryQuantity = "220.00";
			categoryAmount = "12000.00";

			fc.utobj().sendKeys(driver,
					driver.findElement(By
							.xpath(".//td[contains(text () , 'Service')]/following-sibling::td//input[contains(@name , 'quantity')]")),
					categoryQuantity);
			fc.utobj().sendKeys(driver,
					driver.findElement(By
							.xpath(".//td[contains(text () , 'Service')]/following-sibling::td//input[contains(@name , 'amount')]")),
					categoryAmount);

			String comments = "Test Comments" + fc.utobj().generateRandomNumber();
			
			fc.utobj().sendKeys(driver, pobj.salesComment, comments);

			fc.utobj().clickElement(driver, pobj.SaveBtn);

			if (!fc.utobj().isSelected(driver,pobj.acknowledgement)) {
				
				fc.utobj().clickElement(driver, pobj.acknowledgement);
			}
			fc.utobj().clickElement(driver, pobj.SaveBtn);
			
			fc.utobj().clickElement(driver, pobj.okayBtn);

			fc.utobj().printTestStep("verify Modified Details Of Sales Report");

			float salesTotal = 0;
			float royaltyVal = 0;
			float advVal = 0;

			try {

				salesTotal = Float.parseFloat(categoryAmount);

				float royaltyPrcntgVal = Float.parseFloat(royaltyPrcntg);
				royaltyVal = salesTotal * royaltyPrcntgVal / 100;

				float advPrcntgVal = Float.parseFloat(advPercentage);
				advVal = salesTotal * advPrcntgVal / 100;

			} catch (Exception e) {
				
				fc.utobj().throwsException("Problem In Test Data Supplied");
			}

			boolean isLinkTextPresent = fc.utobj().assertLinkText(driver, reportId);

			if (isLinkTextPresent == false) {

				fc.utobj().throwsException("was not able to verify Report At Finance Sales Page");
			}

			fc.utobj().printTestStep("Verify Total Sales At Finance > Sales Page");
			
			String totalSaleVal = fc.utobj().getText(driver,
					driver.findElement(By.xpath(".//a[contains(text () , '" + franchiseId + "')]/ancestor::tr/td[7]")));

			if (totalSaleVal.contains(",")) {
				
				totalSaleVal = totalSaleVal.replaceAll(",", "");
			}
			if (Float.parseFloat(categoryAmount) != Float.parseFloat(totalSaleVal)) {
				fc.utobj().throwsException("was not able to verify Total Sales");
			}

			fc.utobj().printTestStep("Verify Royalty At Finance > Sales Page");
			
			String royaltySaleVal = fc.utobj().getText(driver,
					driver.findElement(By.xpath(".//a[contains(text () , '" + franchiseId + "')]/ancestor::tr/td[8]")));

			if (royaltySaleVal.contains(",")) {
				
				royaltySaleVal = royaltySaleVal.replaceAll(",", "");
			}

			if (royaltyVal != Float.parseFloat(royaltySaleVal)) {
				
				fc.utobj().throwsException("was not able to verify Royalty");
			}

			fc.utobj().printTestStep("Verify Adv. At Finance > Sales Page");
			
			boolean toAdv = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[.='" + franchiseId + "']/ancestor::tr/td[contains(text () , '" + advVal + "')]");

			if (toAdv == false) {
				fc.utobj().throwsException("was not able to verify Adv. ");
			}

			fc.utobj().printTestStep("Verify Invoice Date Status");

			boolean toInvoiceDate = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[.='" + franchiseId + "']/ancestor::tr/td[contains(text () , 'Waiting For Approval')]");

			if (toInvoiceDate == false) {
				fc.utobj().throwsException("was not able to verify Invoice Date Status");
			}

			fc.utobj().printTestStep("Verify The View Action Logs");
			fc.utobj().actionImgOption(driver, reportId, "View Action Logs");

			fc.commonMethods().switch_cboxIframe_frameId(driver);
			List<WebElement> listW = driver.findElements(By.xpath(".//td[contains(text () , '" + comments
					+ "')]/preceding-sibling::td[contains(text () , 'Modified')]"));
			fc.utobj().clickElement(driver, pobj.cloaseBtn);
			fc.utobj().switchFrameToDefault(driver);

			if (listW.size() == 0) {
				fc.utobj().throwsException("was not able to verify the Created Action Logs");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "finance")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-05-24", testCaseId = "TC_FIN_Upload_Sales_Document", testCaseDescription = "Verify Upload Sales Document By Action Image Option")
	public void uploadSalesDocument() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		Map<String, String> dataSet = fc.utobj().readTestData("finance", testCaseId);

		String agrmntVrsnName = fc.utobj().generateTestData(dataSet.get("agreementVersionName"));
		String royaltyPrcntg = dataSet.get("royaltyPercentage");
		String advPercentage = dataSet.get("advertisementPercentage");
		String royaltyAreaFranchiseValue = dataSet.get("RoyaltyAreaFranchise");
		String advAreaFranchiseValue = dataSet.get("AdvAreaFranchise");
		
		

		try {
			driver = fc.loginpage().login(driver);
			FinanceSalesPage pobj = new FinanceSalesPage(driver);

			fc.utobj().printTestStep("Navigate To Admin > Finance > Agreement Versions");
			AdminFinanceAgreementVersionsPageTest agreementforSales = new AdminFinanceAgreementVersionsPageTest();

			fc.utobj().printTestStep("Add Agreement");
			String agreementVersion = agreementforSales.addAgreement(driver, config, agrmntVrsnName, 
					 royaltyPrcntg, advPercentage, royaltyAreaFranchiseValue, advAreaFranchiseValue);

			fc.utobj().printTestStep("Navigate To Admin > Finance >  Configure Finance Document(s) to be Uploaded");
			fc.utobj().printTestStep("Add Document Name");
			String documentName = fc.utobj().generateTestData("TestDoc");
			new AdminFinanceConfigureFinanceDocumentUploadedPageTest().addDocumentName(driver, documentName);

			fc.utobj().printTestStep("Navigate To Admin > Franchise Location > Add Franchise Location");
			fc.utobj().printTestStep("Add Franchise Location");

			String regionName = fc.utobj().generateTestData("GT");
			String storeType = fc.utobj().generateTestData("GT");
			String franchiseId = fc.utobj().generateTestData("GT");
			AdminFranchiseLocationAddFranchiseLocationPageTest franchise = new AdminFranchiseLocationAddFranchiseLocationPageTest();
			franchiseId = franchise.addFranchiseLocation_All_Agreement(driver, franchiseId, regionName, storeType,
					agreementVersion, config);

			fc.utobj().printTestStep("Navigate To Admin > Finance > Finance Setup Preferences");
			fc.utobj().printTestStep("Setup Medium of submission of Sales Reports");
			String salesReportType = "webForm";
			new AdminFinanceFinanceSetupPreferencesPageTest().mediumOfSubmissionOfSalesReports(driver, config,
					salesReportType);

			fc.utobj().printTestStep("Navigate To Finance > Sales");
			fc.utobj().printTestStep("Enter Sales Report");
			String categoryQuantity = "110";
			String categoryAmount = "6000";
			String reportId = enterSalesReport(driver, franchiseId, categoryQuantity, categoryAmount);

			fc.utobj().printTestStep("Filter Sales Report By Franchise Id");
			filterSalesReport(driver, franchiseId);

			fc.utobj().printTestStep("Upload Sales Document");
			fc.utobj().actionImgOption(driver, reportId, "Upload Sales Document(s)");

			String fileName = fc.utobj().getFilePathFromTestData(dataSet.get("fileName"));
			fc.utobj().sendKeys(driver, driver.findElement(By.xpath(".//td[contains(text () , '" + documentName
					+ "')]/following-sibling::td/input[contains(@name , 'File')]")), fileName);
			fc.utobj().clickElement(driver, pobj.uploadBtn);

			filterSalesReport(driver, franchiseId);
			fc.utobj().actionImgOption(driver, reportId, "View Sales Document(s)");

			boolean isDocumentNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//td[contains(text () , '" + documentName
					+ "')]/following-sibling::td/a[contains(text () , '" + dataSet.get("fileName") + "')]");

			if (isDocumentNamePresent == false) {
				
				fc.utobj().throwsException("was not able to verify Uploaded Sales Document");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "finance")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-05-24", testCaseId = "TC_FIN_Delete_Uploaded_Doc", testCaseDescription = "Verify Delete Upload Sales Document By Action Image Option")
	public void deleteUploadedSalesDocument() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		Map<String, String> dataSet = fc.utobj().readTestData("finance", testCaseId);

		String agrmntVrsnName = fc.utobj().generateTestData(dataSet.get("agreementVersionName"));
		String royaltyPrcntg = dataSet.get("royaltyPercentage");
		String advPercentage = dataSet.get("advertisementPercentage");
		String royaltyAreaFranchiseValue = dataSet.get("RoyaltyAreaFranchise");
		String advAreaFranchiseValue = dataSet.get("AdvAreaFranchise");
		
		

		try {
			driver = fc.loginpage().login(driver);
			FinanceSalesPage pobj = new FinanceSalesPage(driver);

			fc.utobj().printTestStep("Navigate To Admin > Finance > Agreement Versions");
			AdminFinanceAgreementVersionsPageTest agreementforSales = new AdminFinanceAgreementVersionsPageTest();

			fc.utobj().printTestStep("Add Agreement");
			String agreementVersion = agreementforSales.addAgreement(driver, config, agrmntVrsnName, 
					 royaltyPrcntg, advPercentage, royaltyAreaFranchiseValue, advAreaFranchiseValue);

			fc.utobj().printTestStep("Navigate To Admin > Finance >  Configure Finance Document(s) to be Uploaded");
			fc.utobj().printTestStep("Add Document Name");
			String documentName = fc.utobj().generateTestData("TestDoc");
			new AdminFinanceConfigureFinanceDocumentUploadedPageTest().addDocumentName(driver, documentName);

			fc.utobj().printTestStep("Navigate To Admin > Franchise Location > Add Franchise Location");
			fc.utobj().printTestStep("Add Franchise Location");

			String regionName = fc.utobj().generateTestData("GT");
			String storeType = fc.utobj().generateTestData("GT");
			String franchiseId = fc.utobj().generateTestData("GT");
			AdminFranchiseLocationAddFranchiseLocationPageTest franchise = new AdminFranchiseLocationAddFranchiseLocationPageTest();
			franchiseId = franchise.addFranchiseLocation_All_Agreement(driver, franchiseId, regionName, storeType,
					agreementVersion, config);

			fc.utobj().printTestStep("Navigate To Admin > Finance > Finance Setup Preferences");
			fc.utobj().printTestStep("Setup Medium of submission of Sales Reports");
			String salesReportType = "webForm";
			new AdminFinanceFinanceSetupPreferencesPageTest().mediumOfSubmissionOfSalesReports(driver, config,
					salesReportType);

			fc.utobj().printTestStep("Navigate To Finance > Sales");
			fc.utobj().printTestStep("Enter Sales Report");
			String categoryQuantity = "110";
			String categoryAmount = "6000";
			String reportId = enterSalesReport(driver, franchiseId, categoryQuantity, categoryAmount);

			fc.utobj().printTestStep("Filter Sales Report By Franchise Id");
			filterSalesReport(driver, franchiseId);

			fc.utobj().printTestStep("Upload Sales Document");
			fc.utobj().actionImgOption(driver, reportId, "Upload Sales Document(s)");

			String fileName = fc.utobj().getFilePathFromTestData(dataSet.get("fileName"));
			fc.utobj().sendKeys(driver, driver.findElement(By.xpath(".//td[contains(text () , '" + documentName
					+ "')]/following-sibling::td/input[contains(@name , 'File')]")), fileName);
			fc.utobj().clickElement(driver, pobj.uploadBtn);

			filterSalesReport(driver, franchiseId);
			fc.utobj().actionImgOption(driver, reportId, "View Sales Document(s)");

			fc.utobj().clickElement(driver, driver.findElement(By.xpath(".//td[contains(text () , '" + documentName
					+ "')]/following-sibling::td/img[@title='Delete this Document']")));
			fc.utobj().acceptAlertBox(driver);

			fc.utobj().printTestStep("Verify The Deleted Document");

			boolean isDocumentNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//td[contains(text () , '" + documentName
					+ "')]/following-sibling::td/a[contains(text () , '" + dataSet.get("fileName") + "')]");

			if (isDocumentNamePresent == true) {
				fc.utobj().throwsException("was not able to verify Uploaded Sales Document");
			}

			fc.utobj().clickElement(driver, pobj.backBtn);

			filterSalesReport(driver, franchiseId);

			fc.utobj().printTestStep("Verify The View Document Logs");
			fc.utobj().actionImgOption(driver, reportId, "View Document Logs");

			fc.commonMethods().switch_cboxIframe_frameId(driver);

			List<WebElement> listE = driver.findElements(By.xpath(".//td[contains(text () , '" + documentName + "')]"));
			fc.utobj().clickElement(driver, pobj.cloaseBtn);
			fc.utobj().switchFrameToDefault(driver);

			if (listE.size() == 0) {
				fc.utobj().throwsException("was not able to verify the Added Documents Logs");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "finance" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-05-24", testCaseId = "TC_FIN_Waiting_For_Acknowledgement", testCaseDescription = "Verify The Status OF Sales Report Waiting For Acknowledgement And Acknowledgement The Sales Report")
	public void waitingForAcknowledgement() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		Map<String, String> dataSet = fc.utobj().readTestData("finance", testCaseId);

		String agrmntVrsnName = fc.utobj().generateTestData(dataSet.get("agreementVersionName"));
		String royaltyPrcntg = dataSet.get("royaltyPercentage");
		String advPercentage = dataSet.get("advertisementPercentage");
		String royaltyAreaFranchiseValue = dataSet.get("RoyaltyAreaFranchise");
		String advAreaFranchiseValue = dataSet.get("AdvAreaFranchise");
		
		

		try {
			driver = fc.loginpage().login(driver);
			FinanceSalesPage pobj = new FinanceSalesPage(driver);

			fc.utobj().printTestStep("Navigate To Admin > Finance > Agreement Versions");
			AdminFinanceAgreementVersionsPageTest agreementforSales = new AdminFinanceAgreementVersionsPageTest();

			fc.utobj().printTestStep("Add Agreement");
			String agreementVersion = agreementforSales.addAgreement(driver, config, agrmntVrsnName, 
					 royaltyPrcntg, advPercentage, royaltyAreaFranchiseValue, advAreaFranchiseValue);

			fc.utobj().printTestStep("Navigate To Admin > Franchise Location > Add Franchise Location");
			fc.utobj().printTestStep("Add Franchise Location");

			String regionName = fc.utobj().generateTestData("GT");
			String storeType = fc.utobj().generateTestData("GT");
			String franchiseId = fc.utobj().generateTestData("GT");
			AdminFranchiseLocationAddFranchiseLocationPageTest franchise = new AdminFranchiseLocationAddFranchiseLocationPageTest();
			franchiseId = franchise.addFranchiseLocation_All_Agreement(driver, franchiseId, regionName, storeType,
					agreementVersion, config);

			fc.utobj().printTestStep("Navigate To Admin > Finance > Finance Setup Preferences");
			fc.utobj().printTestStep("Setup Medium of submission of Sales Reports");
			String salesReportType = "webForm";
			new AdminFinanceFinanceSetupPreferencesPageTest().mediumOfSubmissionOfSalesReports(driver, config,
					salesReportType);

			fc.utobj().printTestStep("Navigate To Finance > Sales");
			fc.utobj().printTestStep("Enter Sales Report");
			String categoryQuantity = "110";
			String categoryAmount = "6000";

			fc.finance().finance_common().financeSalesPage(driver);
			fc.utobj().clickElement(driver, pobj.enterSalesReport);
			fc.utobj().selectDropDown(driver, pobj.selectFranchise, franchiseId);

			fc.utobj().selectDropDownByIndex(driver, pobj.selectReportPeriod, 0);

			fc.utobj().clickElement(driver, pobj.ContinueBtn);

			fc.utobj().sendKeys(driver,
					driver.findElement(By
							.xpath(".//td[contains(text () , 'Service')]/following-sibling::td//input[contains(@name , 'quantity')]")),
					categoryQuantity);
			fc.utobj().sendKeys(driver,
					driver.findElement(By
							.xpath(".//td[contains(text () , 'Service')]/following-sibling::td//input[contains(@name , 'amount')]")),
					categoryAmount);

			fc.utobj().sendKeys(driver, pobj.rcvdDate, fc.utobj().getFutureDateUSFormat(-1));

			fc.utobj().clickElement(driver, pobj.SaveBtn);

			if (fc.utobj().isSelected(driver,pobj.acknowledgement)) {
				fc.utobj().clickElement(driver, pobj.acknowledgement);
			}
			fc.utobj().clickElement(driver, pobj.backBtn);
			
			fc.finance().finance_common().financeStoreSummaryPage(driver);
			
			fc.utobj().clickElement(driver, driver.findElement(By.id("franchiseID")));
			
			fc.utobj().clickElement(driver, driver.findElement(By.xpath(".//*[contains(text(), '"+franchiseId+"')]")));
			
			fc.utobj().clickElement(driver, driver.findElement(By.id("getSummary")));
			
			fc.finance().finance_common().financeSalesPage(driver);
			
			fc.utobj().printTestStep("Filter Sales Report By Franchise Id");
			filterSalesReport(driver, franchiseId);

			fc.utobj().printTestStep("Verify The Status Of Sales Report");

			boolean isTextPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[contains(text () , '" + franchiseId
					+ "')]/ancestor::tr/td[contains(text () , 'Waiting For Acknowledgement')]");

			if (isTextPresent == false) {

				fc.utobj()
						.throwsException("was not able to verify 'Waiting For Acknowledgement' status of Sales Report");
			}

			fc.utobj().printTestStep("Acknowledge The Report");
			fc.utobj().actionImgOption(driver, franchiseId, "Acknowledge");

			fc.utobj().printTestStep("Verify The Status Of Sales Report");
			boolean isTextPresent1 = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[contains(text () , '" + franchiseId
					+ "')]/ancestor::tr/td[contains(text () , 'Waiting For Approval')]");

			if (isTextPresent1 == false) {

				fc.utobj().throwsException("was not able to verify 'Waiting For Approval' status of Sales Report");
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "finance")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-05-24", testCaseId = "TC_FC_Generate_Invoice_052", testCaseDescription = "To Verify Generated Invoice For The Sales Report")
	public void generateInvoice() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());
		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("finance", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		String agrmntVrsnName = fc.utobj().generateTestData(dataSet.get("agreementVersionName"));
		String royaltyPrcntg = dataSet.get("royaltyPercentage");
		String advPercentage = dataSet.get("advertisementPercentage");
		String royaltyAreaFranchiseValue = dataSet.get("RoyaltyAreaFranchise");
		String advAreaFranchiseValue = dataSet.get("AdvAreaFranchise");
		
		

		try {
			driver = fc.loginpage().login(driver);
			FinanceSalesPage pobj = new FinanceSalesPage(driver);

			fc.utobj().printTestStep("Navigate To Admin > Finance > Agreement Versions");
			AdminFinanceAgreementVersionsPageTest agreementforSales = new AdminFinanceAgreementVersionsPageTest();

			fc.utobj().printTestStep("Add Agreement");
			String agreementVersion = agreementforSales.addAgreement(driver, config, agrmntVrsnName, 
					 royaltyPrcntg, advPercentage, royaltyAreaFranchiseValue, advAreaFranchiseValue);

			fc.utobj().printTestStep("Navigate To Admin > Finance >  Configure Finance Document(s) to be Uploaded");
			fc.utobj().printTestStep("Add Document Name");
			String documentName = fc.utobj().generateTestData("TestDoc");
			new AdminFinanceConfigureFinanceDocumentUploadedPageTest().addDocumentName(driver, documentName);

			fc.utobj().printTestStep("Navigate To Admin > Franchise Location > Add Franchise Location");
			fc.utobj().printTestStep("Add Franchise Location");

			String regionName = fc.utobj().generateTestData("GT");
			String storeType = fc.utobj().generateTestData("GT");
			String franchiseId = fc.utobj().generateTestData("GT");
			AdminFranchiseLocationAddFranchiseLocationPageTest franchise = new AdminFranchiseLocationAddFranchiseLocationPageTest();
			franchiseId = franchise.addFranchiseLocation_All_Agreement(driver, franchiseId, regionName, storeType,
					agreementVersion, config);

			fc.utobj().printTestStep("Navigate To Admin > Finance > Finance Setup Preferences");
			fc.utobj().printTestStep("Setup Medium of submission of Sales Reports");
			String salesReportType = "webForm";
			new AdminFinanceFinanceSetupPreferencesPageTest().mediumOfSubmissionOfSalesReports(driver, config,
					salesReportType);

			fc.utobj().printTestStep("Set Invoice Frequency");
			new AdminFinanceFinanceSetupPreferencesPageTest().setInvoice(driver, config);

			fc.utobj().printTestStep("Navigate To Finance > Sales");
			fc.utobj().printTestStep("Enter Sales Report");
			String categoryQuantity = "110";
			String categoryAmount = "6000";
			String reportId = enterSalesReport(driver, franchiseId, categoryQuantity, categoryAmount);

			fc.utobj().printTestStep("Filter Sales Report By Franchise Id");
			filterSalesReport(driver, franchiseId);

			fc.utobj().printTestStep("Approve Sales Report");
			fc.utobj().actionImgOption(driver, reportId, "Approve");

			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().sendKeys(driver, pobj.commentTxtArea,
					"Test Approve Comments" + fc.utobj().generateRandomNumber());
			fc.utobj().clickElement(driver, pobj.SaveBtn);

			fc.utobj().clickElement(driver, pobj.cloaseBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Filter Sales Report By Franchise Id");
			filterSalesReport(driver, franchiseId);

			fc.utobj().printTestStep("Generate Invoice");
			fc.utobj().actionImgOption(driver, franchiseId, "Generate Invoice");

			fc.utobj().clickPartialLinkText(driver, reportId);
			boolean isInvoicedText = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//td[contains(text () , 'Invoiced')]");

			if (isInvoicedText == false) {
				fc.utobj().throwsException("Not verify Action Logs");
			}

			fc.utobj().clickElement(driver, pobj.royalityPage);
			fc.utobj().printTestStep("Filter Sales Report By Franchise Id");
			filterSalesReport(driver, franchiseId);

			fc.utobj().printTestStep("Verify The Generated Invoice At Royalty Page");

			boolean isTextPresent = fc.utobj().assertLinkText(driver, franchiseId);

			if (isTextPresent == false) {
				fc.utobj().throwsException("was not able to verify Generated Invoice");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "finance")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-05-28", testCaseId = "TC_FC_Enter_Adjustment_085", testCaseDescription = "Verify enter Adjustment")
	public void enterAdjustment() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		Map<String, String> dataSet = fc.utobj().readTestData("finance", testCaseId);

		String agrmntVrsnName = fc.utobj().generateTestData(dataSet.get("agreementVersionName"));
		String royaltyPrcntg = dataSet.get("royaltyPercentage");
		String advPercentage = dataSet.get("advertisementPercentage");
		String royaltyAreaFranchiseValue = dataSet.get("RoyaltyAreaFranchise");
		String advAreaFranchiseValue = dataSet.get("AdvAreaFranchise");
		
		

		try {
			driver = fc.loginpage().login(driver);
			FinanceSalesPage pobj = new FinanceSalesPage(driver);

			fc.utobj().printTestStep("Navigate To Admin > Finance > Agreement Versions");
			AdminFinanceAgreementVersionsPageTest agreementforSales = new AdminFinanceAgreementVersionsPageTest();

			fc.utobj().printTestStep("Add Agreement");
			String agreementVersion = agreementforSales.addAgreement(driver, config, agrmntVrsnName, 
					 royaltyPrcntg, advPercentage, royaltyAreaFranchiseValue, advAreaFranchiseValue);

			fc.utobj().printTestStep("Navigate To Admin > Finance > Configure Categories for Sales Report");
			AdminFinanceConfigureCategoriesforSalesReportPageTest sales_Report_Page = new AdminFinanceConfigureCategoriesforSalesReportPageTest();

			fc.utobj().printTestStep("Add Main Category With Calculation Type Is Addition");
			String categoryName = fc.utobj().generateTestData("testCat");
			categoryName = sales_Report_Page.addNewCategory(driver, config, categoryName, "Addition");
			
			fc.utobj().printTestStep("Add Sub Category With Calculation Type Is Addition");
			String subCategoryName = fc.utobj().generateTestData("testSubCat");
			subCategoryName = sales_Report_Page.addSubCategory(driver, config, categoryName, subCategoryName, "Addition");
			
			fc.utobj().printTestStep("Navigate To Admin > Finance >  Configure Finance Document(s) to be Uploaded");
			fc.utobj().printTestStep("Add Document Name");
			String documentName = fc.utobj().generateTestData("TestDoc");
			new AdminFinanceConfigureFinanceDocumentUploadedPageTest().addDocumentName(driver, documentName);

			fc.utobj().printTestStep("Navigate To Admin > Franchise Location > Add Franchise Location");
			fc.utobj().printTestStep("Add Franchise Location");

			String regionName = fc.utobj().generateTestData("Test");
			String storeType = fc.utobj().generateTestData("Test");
			String franchiseId = fc.utobj().generateTestData("Test");
			AdminFranchiseLocationAddFranchiseLocationPageTest franchise = new AdminFranchiseLocationAddFranchiseLocationPageTest();
			franchiseId = franchise.addFranchiseLocation_All_Agreement(driver, franchiseId, regionName, storeType,
					agreementVersion, config);

			fc.utobj().printTestStep("Navigate To Admin > Finance > Finance Setup Preferences");
			fc.utobj().printTestStep("Setup Medium of submission of Sales Reports");
			String salesReportType = "webForm";
			new AdminFinanceFinanceSetupPreferencesPageTest().mediumOfSubmissionOfSalesReports(driver, config,
					salesReportType);

			fc.utobj().printTestStep("Set Invoice Frequency");
			new AdminFinanceFinanceSetupPreferencesPageTest().setInvoice(driver, config);
			fc.utobj().printTestStep("Allow Adjustments after invoicing");
			String allowAdj ="Yes";
			
			new AdminFinanceFinanceSetupPreferencesPageTest().allowAdjustmentsAfterInvoicing(driver, config, allowAdj);

			fc.utobj().printTestStep("Navigate To Finance > Sales");
			fc.utobj().printTestStep("Enter Sales Report");
			String categoryQuantity = "110";
			String categoryAmount = "6000";
			String reportId = enterSalesReport(driver, franchiseId, categoryQuantity, categoryAmount);

			fc.utobj().printTestStep("Filter Sales Report By Franchise Id");
			filterSalesReport(driver, franchiseId);

			fc.utobj().printTestStep("Approve Sales Report");
			fc.utobj().actionImgOption(driver, reportId, "Approve");

			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().sendKeys(driver, pobj.commentTxtArea,
					"Test Approve Comments" + fc.utobj().generateRandomNumber());
			fc.utobj().clickElement(driver, pobj.SaveBtn);

			fc.utobj().clickElement(driver, pobj.cloaseBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Filter Sales Report By Franchise Id");
			filterSalesReport(driver, franchiseId);

			fc.utobj().printTestStep("Generate Invoice");
			fc.utobj().actionImgOption(driver, franchiseId, "Generate Invoice");

			fc.utobj().printTestStep("Enter Adjustment");
			fc.utobj().actionImgOption(driver, franchiseId, "Enter Adjustment");

			categoryQuantity = "200";
			categoryAmount = "400";
			fc.utobj().sendKeys(driver,	driver.findElement(By.xpath(".//td[contains(text () , 'Service')]/following-sibling::td//input[contains(@name , 'quantity')]")), categoryQuantity);
			fc.utobj().sendKeys(driver,	driver.findElement(By.xpath(".//td[contains(text () , 'Service')]/following-sibling::td//input[contains(@name , 'amount')]")), categoryAmount);

			fc.utobj().sendKeys(driver, pobj.rcvdDate, fc.utobj().getFutureDateUSFormat(-1));

			fc.utobj().clickElement(driver, pobj.SaveBtn);

			if (!fc.utobj().isSelected(driver,pobj.acknowledgement)) {
				
				fc.utobj().clickElement(driver, pobj.acknowledgement);
			}
			fc.utobj().clickElement(driver, pobj.SaveBtn);

			String reportIdAdj=fc.utobj().getText(driver, pobj.reportId);
			
			fc.utobj().clickElement(driver, pobj.okayBtn);

			float salesTotal = 0;
			float royaltyVal = 0;
			float advVal = 0;

			try {

				salesTotal = Float.parseFloat(categoryAmount);

				float royaltyPrcntgVal = Float.parseFloat(royaltyPrcntg);
				
				royaltyVal = salesTotal * royaltyPrcntgVal / 100;

				float advPrcntgVal = Float.parseFloat(advPercentage);
				
				advVal = salesTotal * advPrcntgVal / 100;

			} catch (Exception e) {
				fc.utobj().throwsException("Problem In Test Data Supplied");
			}

			boolean isLinkTextPresent = fc.utobj().assertLinkText(driver, reportId);

			if (isLinkTextPresent == false) {

				fc.utobj().throwsException("was not able to verify Report At Finance Sales Page");
			}

			fc.utobj().printTestStep("Verify Total Sales At Finance > Sales Page");
			
			boolean toSal = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[.='" + franchiseId + "']/ancestor::tr/td[contains(text () , '" + salesTotal + "')]");

			if (toSal == false) {
				fc.utobj().throwsException("was not able to verify Total Sales");
			}

			fc.utobj().printTestStep("Verify Royalty At Finance > Sales Page");
			
			boolean toRoyalty = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[.='" + franchiseId + "']/ancestor::tr/td[contains(text () , '" + royaltyVal + "')]");

			if (toRoyalty == false) {
				fc.utobj().throwsException("was not able to verify Royalty");
			}

			fc.utobj().printTestStep("Verify Adv. At Finance > Sales Page");
			
			boolean toAdv = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[.='" + franchiseId + "']/ancestor::tr/td[contains(text () , '" + advVal + "')]");

			if (toAdv == false) {
				fc.utobj().throwsException("was not able to verify Adv. ");
			}

			fc.utobj().printTestStep("Verify Invoice Date Status");

			boolean toInvoiceDate = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[.='" + franchiseId + "']/ancestor::tr/td[contains(text () , 'Waiting For Approval')]");

			if (toInvoiceDate == false) {
				fc.utobj().throwsException("was not able to verify Invoice Date Status");
			}

			//verify adjustment at Store Summary
			
			
			
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "finance")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-05-24", testCaseId = "TC_FC_Approve_Multiple_Sales_Report_080", testCaseDescription = "Verify Approve Sales By Bottom Button Option")
	public void approveMultipleSalesReports() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		Map<String, String> dataSet = fc.utobj().readTestData("finance", testCaseId);

		String agrmntVrsnName = fc.utobj().generateTestData(dataSet.get("agreementVersionName"));
		String royaltyPrcntg = dataSet.get("royaltyPercentage");
		String advPercentage = dataSet.get("advertisementPercentage");
		String royaltyAreaFranchiseValue = dataSet.get("RoyaltyAreaFranchise");
		String advAreaFranchiseValue = dataSet.get("AdvAreaFranchise");
		
		

		try {
			driver = fc.loginpage().login(driver);
			FinanceSalesPage pobj = new FinanceSalesPage(driver);

			fc.utobj().printTestStep("Navigate To Admin > Finance > Agreement Versions");
			AdminFinanceAgreementVersionsPageTest agreementforSales = new AdminFinanceAgreementVersionsPageTest();

			fc.utobj().printTestStep("Add Agreement");
			String agreementVersion = agreementforSales.addAgreement(driver, config, agrmntVrsnName, 
					 royaltyPrcntg, advPercentage, royaltyAreaFranchiseValue, advAreaFranchiseValue);

			fc.utobj().printTestStep("Navigate To Admin > Franchise Location > Add Franchise Location");
			fc.utobj().printTestStep("Add Franchise Location");

			String regionName = fc.utobj().generateTestData("Test");
			String storeType = fc.utobj().generateTestData("Test");
			String franchiseId = fc.utobj().generateTestData("Test");
			AdminFranchiseLocationAddFranchiseLocationPageTest franchise = new AdminFranchiseLocationAddFranchiseLocationPageTest();
			franchiseId = franchise.addFranchiseLocation_All_Agreement(driver, franchiseId, regionName, storeType,
					agreementVersion, config);

			fc.utobj().printTestStep("Navigate To Admin > Finance > Finance Setup Preferences");
			fc.utobj().printTestStep("Setup Medium of submission of Sales Reports");
			String salesReportType = "webForm";
			new AdminFinanceFinanceSetupPreferencesPageTest().mediumOfSubmissionOfSalesReports(driver, config,
					salesReportType);

			fc.utobj().printTestStep("Navigate To Finance > Sales");
			fc.utobj().printTestStep("Enter Sales Report");
			String categoryQuantity = "110";
			String categoryAmount = "6000";
			enterSalesReport(driver, franchiseId, categoryQuantity, categoryAmount);

			fc.utobj().printTestStep("Filter Sales Report By Franchise Id");
			filterSalesReport(driver, franchiseId);

			fc.utobj().printTestStep("Approve Sales Report");
			fc.utobj().clickElement(driver, pobj.allCheckedValue);
			fc.utobj().clickElement(driver, pobj.approveBtn);

			fc.utobj().printTestStep("Verify Approval");

			boolean isTextPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[contains(text () , '" + franchiseId
					+ "')]/ancestor::tr/td[contains(text () , 'Scheduled For')]");
			if (isTextPresent == false) {
				fc.utobj().throwsException("was not able to Verify Approval Of Finance Sales Report");
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "finance" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-05-24", testCaseId = "TC_FC_Reject_Multiple_Sales_081", testCaseDescription = "Verify Reject Sales Report By Bottom Button Option")
	public void rejectMultipleSalesReports() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		Map<String, String> dataSet = fc.utobj().readTestData("finance", testCaseId);

		String agrmntVrsnName = fc.utobj().generateTestData(dataSet.get("agreementVersionName"));
		String royaltyPrcntg = dataSet.get("royaltyPercentage");
		String advPercentage = dataSet.get("advertisementPercentage");
		String royaltyAreaFranchiseValue = dataSet.get("RoyaltyAreaFranchise");
		String advAreaFranchiseValue = dataSet.get("AdvAreaFranchise");
		
		

		try {
			driver = fc.loginpage().login(driver);
			FinanceSalesPage pobj = new FinanceSalesPage(driver);

			fc.utobj().printTestStep("Navigate To Admin > Finance > Agreement Versions");
			AdminFinanceAgreementVersionsPageTest agreementforSales = new AdminFinanceAgreementVersionsPageTest();

			fc.utobj().printTestStep("Add Agreement");
			String agreementVersion = agreementforSales.addAgreement(driver, config, agrmntVrsnName, 
					 royaltyPrcntg, advPercentage, royaltyAreaFranchiseValue, advAreaFranchiseValue);

			fc.utobj().printTestStep("Navigate To Admin > Franchise Location > Add Franchise Location");
			fc.utobj().printTestStep("Add Franchise Location");

			String regionName = fc.utobj().generateTestData("Test");
			String storeType = fc.utobj().generateTestData("Test");
			String franchiseId = fc.utobj().generateTestData("Test");
			AdminFranchiseLocationAddFranchiseLocationPageTest franchise = new AdminFranchiseLocationAddFranchiseLocationPageTest();
			franchiseId = franchise.addFranchiseLocation_All_Agreement(driver, franchiseId, regionName, storeType,
					agreementVersion, config);

			fc.utobj().printTestStep("Navigate To Admin > Finance > Finance Setup Preferences");
			fc.utobj().printTestStep("Setup Medium of submission of Sales Reports");
			String salesReportType = "webForm";
			new AdminFinanceFinanceSetupPreferencesPageTest().mediumOfSubmissionOfSalesReports(driver, config,
					salesReportType);

			fc.utobj().printTestStep("Navigate To Finance > Sales");
			fc.utobj().printTestStep("Enter Sales Report");
			String categoryQuantity = "110";
			String categoryAmount = "6000";
			String reportId = enterSalesReport(driver, franchiseId, categoryQuantity, categoryAmount);

			fc.utobj().printTestStep("Filter Sales Report By Franchise Id");
			filterSalesReport(driver, franchiseId);

			fc.utobj().printTestStep("Reject Sales Report");
			fc.utobj().clickElement(driver, pobj.allCheckedValue);
			fc.utobj().clickElement(driver, pobj.rejectBtn);

			fc.utobj().printTestStep("Verify Rejection");

			boolean isStatusPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () , '" + reportId + "')]/ancestor::tr/td[contains(text () , 'Rejected')]");

			if (isStatusPresent == false) {

				fc.utobj().throwsException("was not able verify the status Of Sales Report");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "finance" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-05-24", testCaseId = "TC_FC_Acknowledge_Multiple_Sales_082", testCaseDescription = "Verify Acknowledge Sales Report By Bottom Option")
	public void acknowledgeMultipleSalesReports() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		Map<String, String> dataSet = fc.utobj().readTestData("finance", testCaseId);

		String agrmntVrsnName = fc.utobj().generateTestData(dataSet.get("agreementVersionName"));
		String royaltyPrcntg = dataSet.get("royaltyPercentage");
		String advPercentage = dataSet.get("advertisementPercentage");
		String royaltyAreaFranchiseValue = dataSet.get("RoyaltyAreaFranchise");
		String advAreaFranchiseValue = dataSet.get("AdvAreaFranchise");
		
		

		try {
			driver = fc.loginpage().login(driver);
			FinanceSalesPage pobj = new FinanceSalesPage(driver);

			fc.utobj().printTestStep("Navigate To Admin > Finance > Agreement Versions");
			AdminFinanceAgreementVersionsPageTest agreementforSales = new AdminFinanceAgreementVersionsPageTest();

			fc.utobj().printTestStep("Add Agreement");
			String agreementVersion = agreementforSales.addAgreement(driver, config, agrmntVrsnName, 
					 royaltyPrcntg, advPercentage, royaltyAreaFranchiseValue, advAreaFranchiseValue);

			fc.utobj().printTestStep("Navigate To Admin > Franchise Location > Add Franchise Location");
			fc.utobj().printTestStep("Add Franchise Location");

			String regionName = fc.utobj().generateTestData("Test");
			String storeType = fc.utobj().generateTestData("Test");
			String franchiseId = fc.utobj().generateTestData("Test");
			AdminFranchiseLocationAddFranchiseLocationPageTest franchise = new AdminFranchiseLocationAddFranchiseLocationPageTest();
			franchiseId = franchise.addFranchiseLocation_All_Agreement(driver, franchiseId, regionName, storeType,
					agreementVersion, config);

			fc.utobj().printTestStep("Navigate To Admin > Finance > Finance Setup Preferences");
			fc.utobj().printTestStep("Setup Medium of submission of Sales Reports");
			String salesReportType = "webForm";
			new AdminFinanceFinanceSetupPreferencesPageTest().mediumOfSubmissionOfSalesReports(driver, config,
					salesReportType);

			fc.utobj().printTestStep("Navigate To Finance > Sales");
			fc.utobj().printTestStep("Enter Sales Report");
			String categoryQuantity = "110";
			String categoryAmount = "6000";

			fc.finance().finance_common().financeSalesPage(driver);
			fc.utobj().clickElement(driver, pobj.enterSalesReport);
			fc.utobj().selectDropDown(driver, pobj.selectFranchise, franchiseId);

			fc.utobj().selectDropDownByIndex(driver, pobj.selectReportPeriod, 0);

			fc.utobj().clickElement(driver, pobj.ContinueBtn);

			fc.utobj().sendKeys(driver, pobj.rcvdDate, fc.utobj().getFutureDateUSFormat(-1));

			fc.utobj().sendKeys(driver,
					driver.findElement(By
							.xpath(".//td[contains(text () , 'Service')]/following-sibling::td//input[contains(@name , 'quantity')]")),
					categoryQuantity);
			fc.utobj().sendKeys(driver,
					driver.findElement(By
							.xpath(".//td[contains(text () , 'Service')]/following-sibling::td//input[contains(@name , 'amount')]")),
					categoryAmount);

			fc.utobj().clickElement(driver, pobj.SaveBtn);

			if (fc.utobj().isSelected(driver,pobj.acknowledgement)) {
				fc.utobj().clickElement(driver, pobj.acknowledgement);
			}
			fc.utobj().clickElement(driver, pobj.backBtn);

			fc.utobj().printTestStep("Filter Sales Report By Franchise Id");
			filterSalesReport(driver, franchiseId);

			fc.utobj().printTestStep("Verify The Status Of Sales Report");

			boolean isTextPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[contains(text () , '" + franchiseId
					+ "')]/ancestor::tr/td[contains(text () , 'Waiting For Acknowledgement')]");

			if (isTextPresent == false) {

				fc.utobj()
						.throwsException("was not able to verify 'Waiting For Acknowledgement' status of Sales Report");
			}

			fc.utobj().printTestStep("Acknowledge The Report");
			fc.utobj().clickElement(driver, pobj.allCheckedValue);
			fc.utobj().clickElement(driver, pobj.AcknowledgeBtn);

			fc.utobj().printTestStep("Verify The Status Of Sales Report");
			boolean isTextPresent1 = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[contains(text () , '" + franchiseId
					+ "')]/ancestor::tr/td[contains(text () , 'Waiting For Approval')]");

			if (isTextPresent1 == false) {

				fc.utobj().throwsException("was not able to verify 'Waiting For Approval' status of Sales Report");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	public String enterSalesReport1(WebDriver driver, Map<String, String> config, String franchiseID, String reportPrd,
			String srvcQuantity1, String srvcAmount1) throws Exception {
		FinanceSalesPage pobj = new FinanceSalesPage(driver);

		fc.finance().finance_common().financeSalesPage(driver);
		fc.utobj().clickElement(driver, pobj.enterSalesReport);
		fc.utobj().selectDropDown(driver, pobj.selectFranchise, franchiseID);
		fc.utobj().selectDropDown(driver, pobj.selectReportPeriod, reportPrd);
		fc.utobj().clickElement(driver, pobj.ContinueBtn);

		fc.utobj().sendKeys(driver, pobj.rcvdDate, fc.utobj().getFutureDateUSFormat(-1));

		fc.utobj().sendKeys(driver, pobj.serviceQuantity, srvcQuantity1);
		fc.utobj().sendKeys(driver, pobj.serviceAmount, srvcAmount1);
		fc.utobj().sendKeys(driver, pobj.salesComment, "Automation Sales");
		fc.utobj().clickElement(driver, pobj.SaveBtn);
		fc.utobj().clickElement(driver, pobj.acknowledgement);
		fc.utobj().clickElement(driver, pobj.SaveBtn);
		String salesReportID = driver.findElement(By.xpath("//tr[td[contains(text(), 'Report ID')]]/td[4]")).getText();
		fc.utobj().clickElement(driver, pobj.okBtn);
		return salesReportID;

	}

	public String approveSalesReport(WebDriver driver, Map<String, String> config, String reportID) throws Exception {

		FinanceSalesPage pobj = new FinanceSalesPage(driver);

		fc.utobj().clickElement(driver, driver.findElement(
				By.xpath(".//*[contains(text(),'" + reportID + "')]/ancestor::tr/td/input[@name='selectedItem']")));

		fc.utobj().clickElement(driver, pobj.approveBtn);

		return reportID;
	}

	public String generateInvoice(WebDriver driver, Map<String, String> config, String reportID, String franchiseID)
			throws Exception {

		FinanceSalesPage pobj = new FinanceSalesPage(driver);
		String actionToTake = "Generate Invoice";

		/*
		  	WebElement e = driver.findElement(By.xpath(".//*[contains(text(),'"+reportID+"')]/ancestor::tr[@class='tb_data']//div[@id='menuBar']/layer"));
		 	fc.utobj().moveToElement(driver, e); 
		 	String layerId = e.getAttribute("id");
		 	WebElement element1 = driver.findElement(By.xpath(".//*[contains(text(),'"+reportID+"')]/ancestor::tr[@class='tb_data']//div[@id='menuBar']/layer/a"));
		 	fc.utobj().moveToElement(driver, element1);
		 	fc.utobj().clickElement(driver, element1);
			layerId = layerId.replace("Actions_dynamicmenu","");
			layerId = layerId.replace("Bar",""); 
			WebElement element2 = driver.findElement(By.xpath(".//*[@id='Actions_dynamicmenu"+layerId+"Menu']/span[contains(text(),'"+actionToTake+"')]"));
			fc.utobj().moveToElement(driver, element2);
			fc.utobj().clickElement(driver, element2);
		 */

		fc.utobj().actionImgOption(driver, reportID, actionToTake);

		fc.utobj().clickElement(driver, pobj.royaltiesTab);

		filterSalesReport(driver, franchiseID);
		String InvoiceID = "";
		try {
			InvoiceID = driver.findElement(By.xpath(".//div[@id='printReady']//*[contains(text(), '" + franchiseID
					+ "')]/ancestor::tr[@class='bText12' or @class='bText12 grAltRw4']/td[2]/a")).getText();
		} catch (Exception e2) {
			fc.utobj().throwsException("was not to verify Invoice Id");
		}
		return InvoiceID;
	}

	@Test(groups = { "finance" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-05-24", testCaseId = "TC_FC_Verify_TotalSales_Enter_Sales_Page_065", testCaseDescription = "Verify Total Sales On Enter Sales Page")
	public void verifyTotalSalesOnEnterSalesPage() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());
		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		Map<String, String> dataSet = fc.utobj().readTestData("finance", testCaseId);

		String agrmntVrsnName = fc.utobj().generateTestData(dataSet.get("agreementVersionName"));
		String royaltyPrcntg = dataSet.get("royaltyPercentage");
		String advPercentage = dataSet.get("advertisementPercentage");
		String royaltyAreaFranchiseValue = dataSet.get("RoyaltyAreaFranchise");
		String advAreaFranchiseValue = dataSet.get("AdvAreaFranchise");
		
		

		try {
			driver = fc.loginpage().login(driver);

			fc.utobj().printTestStep("Navigate To Admin > Finance > Agreement Versions");
			AdminFinanceAgreementVersionsPageTest agreementforSales = new AdminFinanceAgreementVersionsPageTest();

			fc.utobj().printTestStep("Add Agreement");
			String agreementVersion = agreementforSales.addAgreement(driver, config, agrmntVrsnName, 
					 royaltyPrcntg, advPercentage, royaltyAreaFranchiseValue, advAreaFranchiseValue);

			fc.utobj().printTestStep("Navigate To Admin > Franchise Location > Add Franchise Location");
			fc.utobj().printTestStep("Add Franchise Location");

			String regionName = fc.utobj().generateTestData("GT");
			String storeType = fc.utobj().generateTestData("GT");
			String franchiseId = fc.utobj().generateTestData("GT");
			AdminFranchiseLocationAddFranchiseLocationPageTest franchise = new AdminFranchiseLocationAddFranchiseLocationPageTest();
			franchiseId = franchise.addFranchiseLocation_All_Agreement(driver, franchiseId, regionName, storeType,
					agreementVersion, config);

			fc.utobj().printTestStep("Navigate To Admin > Finance > Finance Setup Preferences");
			fc.utobj().printTestStep("Setup Medium of submission of Sales Reports");
			String salesReportType = "webForm";
			new AdminFinanceFinanceSetupPreferencesPageTest().mediumOfSubmissionOfSalesReports(driver, config,
					salesReportType);

			fc.utobj().printTestStep("Navigate To Finance > Sales");
			fc.utobj().printTestStep("Enter Sales Report");
			String categoryQuantity = "325";
			String categoryAmount = "953625";

			fc.utobj().printTestStep("Enter Sales");
			fc.utobj().printTestStep("Verify The Total Sales At Finance > Sales Page");
			TotalSalesAtEnterSalesReport(driver, config, franchiseId, categoryQuantity, categoryAmount);

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	public void TotalSalesAtEnterSalesReport(WebDriver driver, Map<String, String> config, String franchiseID,
			String srvcQuantity1, String srvcAmount1) throws Exception {

		FinanceSalesPage pobj = new FinanceSalesPage(driver);

		fc.finance().finance_common().financeSalesPage(driver);
		fc.utobj().clickElement(driver, pobj.enterSalesReport);
		fc.utobj().selectDropDown(driver, pobj.selectFranchise, franchiseID);

		fc.utobj().selectDropDownByIndex(driver, pobj.selectReportPeriod, 0);

		fc.utobj().clickElement(driver, pobj.ContinueBtn);

		fc.utobj().sendKeys(driver, pobj.rcvdDate, fc.utobj().getFutureDateUSFormat(-1));

		fc.utobj().sendKeys(driver, pobj.serviceQuantity, srvcQuantity1);
		fc.utobj().sendKeys(driver, pobj.serviceAmount, srvcAmount1);
		WebElement e1 = driver.findElement(By.name("finSalesReportDetails_2quantity"));
		fc.utobj().clickElement(driver, e1);
		WebElement e2 = driver.findElement(By.name("finSalesReport_0totalSales"));
		fc.utobj().moveToElement(driver, e2);
		String sumSales = e2.getAttribute("value");
		double totalSales = Double.parseDouble(sumSales);
		fc.utobj().sendKeys(driver, pobj.salesComment, "Automation Sales");
		double Salestotal1 = Double.parseDouble(srvcAmount1);

		if (totalSales != Salestotal1) {
			fc.utobj().throwsException("Total Sales is Not Same as added!");
		}
		fc.utobj().clickElement(driver, pobj.SaveBtn);
		fc.utobj().clickElement(driver, pobj.cancelAtAck);
	}

	@Test(groups = { "finance", "finance_FailedTC" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-05-24", testCaseId = "TC_FC_Verify_Total_Quantity_Enter_Sales_Page_096", testCaseDescription = "To verify Total Quantity On Enter Sales Page")
	public void verifyTotalQuantityOnEnterSalesPage() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		Map<String, String> dataSet = fc.utobj().readTestData("finance", testCaseId);

		String agrmntVrsnName = fc.utobj().generateTestData(dataSet.get("agreementVersionName"));
		String royaltyPrcntg = dataSet.get("royaltyPercentage");
		String advPercentage = dataSet.get("advertisementPercentage");
		String royaltyAreaFranchiseValue = dataSet.get("RoyaltyAreaFranchise");
		String advAreaFranchiseValue = dataSet.get("AdvAreaFranchise");
		
		try {
			driver = fc.loginpage().login(driver);
			fc.utobj().printTestStep("Navigate To Admin > Finance > Agreement Versions");
			AdminFinanceAgreementVersionsPageTest agreementforSales = new AdminFinanceAgreementVersionsPageTest();

			fc.utobj().printTestStep("Add Agreement");
			String agreementVersion = agreementforSales.addAgreement(driver, config, agrmntVrsnName, 
					 royaltyPrcntg, advPercentage, royaltyAreaFranchiseValue, advAreaFranchiseValue);

			fc.utobj().printTestStep("Navigate To Admin > Franchise Location > Add Franchise Location");
			fc.utobj().printTestStep("Add Franchise Location");

			String regionName = fc.utobj().generateTestData("Test");
			String storeType = fc.utobj().generateTestData("Test");
			String franchiseId = fc.utobj().generateTestData("Test");
			AdminFranchiseLocationAddFranchiseLocationPageTest franchise = new AdminFranchiseLocationAddFranchiseLocationPageTest();
			franchiseId = franchise.addFranchiseLocation_All_Agreement(driver, franchiseId, regionName, storeType,
					agreementVersion, config);

			fc.utobj().printTestStep("Navigate To Admin > Finance > Finance Setup Preferences");
			fc.utobj().printTestStep("Setup Medium of submission of Sales Reports");
			String salesReportType = "webForm";
			new AdminFinanceFinanceSetupPreferencesPageTest().mediumOfSubmissionOfSalesReports(driver, config,
					salesReportType);

			fc.utobj().printTestStep("Navigate To Finance > Sales");
			fc.utobj().printTestStep("Enter Sales Report");
			String categoryQuantity = "325";
			String categoryAmount = "953625";

			fc.utobj().printTestStep("Total Quantity At Enter Sales Report");
			TotalQuantityAtEnterSalesReport(driver, config, franchiseId, categoryQuantity, categoryAmount);

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	public void TotalQuantityAtEnterSalesReport(WebDriver driver, Map<String, String> config, String franchiseID,
			String srvcQuantity1, String srvcAmount1) throws Exception {

		FinanceSalesPage pobj = new FinanceSalesPage(driver);
		fc.finance().finance_common().financeSalesPage(driver);
		fc.utobj().clickElement(driver, pobj.enterSalesReport);

		fc.utobj().selectDropDown(driver, pobj.selectFranchise, franchiseID);

		fc.utobj().selectDropDownByIndex(driver, pobj.selectReportPeriod, 0);

		fc.utobj().clickElement(driver, pobj.ContinueBtn);

		fc.utobj().sendKeys(driver, pobj.rcvdDate, fc.utobj().getFutureDateUSFormat(-1));

		fc.utobj().sendKeys(driver, pobj.serviceQuantity, srvcQuantity1);
		fc.utobj().sendKeys(driver, pobj.serviceAmount, srvcAmount1);
		WebElement e2 = driver.findElement(By.name("finSalesReport_0totalQuantity"));
		fc.utobj().moveToElement(driver, e2);
		String sumQuantity = e2.getAttribute("value");
		double quantity = Double.parseDouble(sumQuantity);
		fc.utobj().sendKeys(driver, pobj.salesComment, "Automation Sales");
		double quantitySrvc1 = Double.parseDouble(srvcQuantity1);
		if (quantity != quantitySrvc1) {
			fc.utobj().throwsException("Service Quantity is Not Same as added!");
		}

		fc.utobj().clickElement(driver, pobj.SaveBtn);
		fc.utobj().clickElement(driver, pobj.cancelAtAck);
	}

	@Test(groups = { "finance" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "TC_FC_Verify_Total_Sales_Amount_OnAcknowledgement_Page_066", testCaseDescription = "Verify Total Sales On Acknowledgement Page")
	public void verifyTotalSalesOnAcknowledgementPage() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		Map<String, String> dataSet = fc.utobj().readTestData("finance", testCaseId);

		String agrmntVrsnName = fc.utobj().generateTestData(dataSet.get("agreementVersionName"));
		String royaltyPrcntg = dataSet.get("royaltyPercentage");
		String advPercentage = dataSet.get("advertisementPercentage");
		String royaltyAreaFranchiseValue = dataSet.get("RoyaltyAreaFranchise");
		String advAreaFranchiseValue = dataSet.get("AdvAreaFranchise");
		
		

		try {
			driver = fc.loginpage().login(driver);

			fc.utobj().printTestStep("Navigate To Admin > Finance > Agreement Versions");
			AdminFinanceAgreementVersionsPageTest agreementforSales = new AdminFinanceAgreementVersionsPageTest();

			fc.utobj().printTestStep("Add Agreement");
			String agreementVersion = agreementforSales.addAgreement(driver, config, agrmntVrsnName, 
					 royaltyPrcntg, advPercentage, royaltyAreaFranchiseValue, advAreaFranchiseValue);

			fc.utobj().printTestStep("Navigate To Admin > Franchise Location > Add Franchise Location");
			fc.utobj().printTestStep("Add Franchise Location");

			String regionName = fc.utobj().generateTestData("Test");
			String storeType = fc.utobj().generateTestData("Test");
			String franchiseId = fc.utobj().generateTestData("Test");
			AdminFranchiseLocationAddFranchiseLocationPageTest franchise = new AdminFranchiseLocationAddFranchiseLocationPageTest();
			franchiseId = franchise.addFranchiseLocation_All_Agreement(driver, franchiseId, regionName, storeType,
					agreementVersion, config);

			fc.utobj().printTestStep("Navigate To Admin > Finance > Finance Setup Preferences");
			fc.utobj().printTestStep("Setup Medium of submission of Sales Reports");
			String salesReportType = "webForm";
			new AdminFinanceFinanceSetupPreferencesPageTest().mediumOfSubmissionOfSalesReports(driver, config,
					salesReportType);

			fc.utobj().printTestStep("Navigate To Finance > Sales");
			String categoryQuantity = "325";
			String categoryAmount = "953625";

			fc.utobj().printTestStep("Total Sales Amount on Acknowlegement Report");
			TotalSalesAmountOnAcknowledgementReport(driver, config, franchiseId, categoryQuantity, categoryAmount);

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	public void TotalSalesAmountOnAcknowledgementReport(WebDriver driver, Map<String, String> config,
			String franchiseID, String srvcQuantity1, String srvcAmount1) throws Exception {

		FinanceSalesPage pobj = new FinanceSalesPage(driver);
		fc.finance().finance_common().financeSalesPage(driver);
		fc.utobj().clickElement(driver, pobj.enterSalesReport);
		fc.utobj().selectDropDown(driver, pobj.selectFranchise, franchiseID);

		fc.utobj().selectDropDownByIndex(driver, pobj.selectReportPeriod, 0);

		fc.utobj().clickElement(driver, pobj.ContinueBtn);

		fc.utobj().sendKeys(driver, pobj.rcvdDate, fc.utobj().getFutureDateUSFormat(-1));

		fc.utobj().sendKeys(driver, pobj.serviceQuantity, srvcQuantity1);
		fc.utobj().sendKeys(driver, pobj.serviceAmount, srvcAmount1);
		fc.utobj().sendKeys(driver, pobj.salesComment, "Automation Sales");
		fc.utobj().clickElement(driver, pobj.SaveBtn);

		String totalSalesAck = driver.findElement(By.xpath(".//tr[td[contains(text(), 'Total Sales ($)')]]/td[2]"))
				.getText();
		totalSalesAck = totalSalesAck.replaceAll(",", "");

		double Salestotal1 = Double.parseDouble(srvcAmount1);

		double totalSalesAmount = Double.parseDouble(totalSalesAck);

		if (totalSalesAmount != Salestotal1) {

			fc.utobj().throwsException("Total Sales Amount at Acknowledgment Page is Not Same as added!");
		}

		fc.utobj().clickElement(driver, pobj.cancelAtAck);
	}

	@Test(groups = { "finance" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "TC_FC_Verify_Royalty_Amount_OnAcknowledgement_Page_067", testCaseDescription = "Verify Royalty On Acknowledgement Page")
	public void verifyRoyaltyOnAcknowledgementPage() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());
		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		Map<String, String> dataSet = fc.utobj().readTestData("finance", testCaseId);

		String agrmntVrsnName = fc.utobj().generateTestData(dataSet.get("agreementVersionName"));
		String royaltyPrcntg = dataSet.get("royaltyPercentage");
		String advPercentage = dataSet.get("advertisementPercentage");
		String royaltyAreaFranchiseValue = dataSet.get("RoyaltyAreaFranchise");
		String advAreaFranchiseValue = dataSet.get("AdvAreaFranchise");
		
		

		try {
			driver = fc.loginpage().login(driver);

			fc.utobj().printTestStep("Navigate To Admin > Finance > Agreement Versions");
			AdminFinanceAgreementVersionsPageTest agreementforSales = new AdminFinanceAgreementVersionsPageTest();

			fc.utobj().printTestStep("Add Agreement");
			String agreementVersion = agreementforSales.addAgreement(driver, config, agrmntVrsnName, 
					 royaltyPrcntg, advPercentage, royaltyAreaFranchiseValue, advAreaFranchiseValue);

			fc.utobj().printTestStep("Navigate To Admin > Franchise Location > Add Franchise Location");
			fc.utobj().printTestStep("Add Franchise Location");

			String regionName = fc.utobj().generateTestData("Test");
			String storeType = fc.utobj().generateTestData("Test");
			String franchiseId = fc.utobj().generateTestData("Test");
			AdminFranchiseLocationAddFranchiseLocationPageTest franchise = new AdminFranchiseLocationAddFranchiseLocationPageTest();
			franchiseId = franchise.addFranchiseLocation_All_Agreement(driver, franchiseId, regionName, storeType,
					agreementVersion, config);

			fc.utobj().printTestStep("Navigate To Admin > Finance > Finance Setup Preferences");
			fc.utobj().printTestStep("Setup Medium of submission of Sales Reports");
			String salesReportType = "webForm";
			new AdminFinanceFinanceSetupPreferencesPageTest().mediumOfSubmissionOfSalesReports(driver, config,
					salesReportType);

			fc.utobj().printTestStep("Navigate To Finance > Sales");
			String categoryQuantity = "325";
			String categoryAmount = "953625";

			RoyaltyAmountOnAcknowledgementReport(driver, config, franchiseId, categoryQuantity, categoryAmount,
					royaltyPrcntg);

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	public void RoyaltyAmountOnAcknowledgementReport(WebDriver driver, Map<String, String> config, String franchiseID,
			String srvcQuantity1, String srvcAmount1, String royaltyPrcntg) throws Exception {

		FinanceSalesPage pobj = new FinanceSalesPage(driver);

		/*
		 * fc.utobj().clickElement(driver, pobj.moreBtn);
		 * fc.utobj().clickElement(driver, pobj.financeBtn);
		 * fc.utobj().clickElement(driver, pobj.salesReport);
		 */
		fc.finance().finance_common().financeSalesPage(driver);
		fc.utobj().clickElement(driver, pobj.enterSalesReport);

		fc.utobj().selectDropDownByPartialText(driver, pobj.selectFranchise, franchiseID);

		fc.utobj().selectDropDownByIndex(driver, pobj.selectReportPeriod, 0);

		fc.utobj().clickElement(driver, pobj.ContinueBtn);

		fc.utobj().sendKeys(driver, pobj.rcvdDate, fc.utobj().getFutureDateUSFormat(-1));

		fc.utobj().sendKeys(driver, pobj.serviceQuantity, srvcQuantity1);
		fc.utobj().sendKeys(driver, pobj.serviceAmount, srvcAmount1);
		fc.utobj().sendKeys(driver, pobj.salesComment, "Automation Sales");
		fc.utobj().clickElement(driver, pobj.SaveBtn);

		// String royaltyAck =
		// driver.findElement(By.xpath(".//tr[td[contains(text(), 'Royalty
		// ($)')]]/td[2]")).getText();
		String royaltyAck = driver
				.findElement(By
						.xpath(".//td[contains(text(), 'Royalty')][not(contains(text () , 'Royalty (Area Franchise)'))]/following-sibling::td[1]"))
				.getText();

		if (royaltyAck.contains(",")) {
			royaltyAck = royaltyAck.replaceAll(",", "");
		}

		// double Salestotal1 =
		double RoyaltyAck = Double.parseDouble(royaltyAck);
		double royaltyPercent = Double.parseDouble(royaltyPrcntg);
		double enteredSalesAmount = Double.parseDouble(srvcAmount1);
		double calRoyaltyAmnt = ((enteredSalesAmount * royaltyPercent) / 100);
		DecimalFormat df = new DecimalFormat("#.##");

		calRoyaltyAmnt = Double.valueOf(df.format(calRoyaltyAmnt));

		if (RoyaltyAck != calRoyaltyAmnt) {

			fc.utobj().throwsException(
					"Calculated Royalty at Acknowledgment Page is Not Same as Royalty for added Sales!");
		}

		fc.utobj().clickElement(driver, pobj.cancelAtAck);
	}

	@Test(groups = { "finance" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "TC_FC_Verify_Advertisement_Amount_OnAcknowledgement_Page_068", testCaseDescription = "Verify Advertisement Fee On Acknowledgement Page")
	public void verifyAdvertisementFeeOnAcknowledgementPage() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		Map<String, String> dataSet = fc.utobj().readTestData("finance", testCaseId);

		String agrmntVrsnName = fc.utobj().generateTestData(dataSet.get("agreementVersionName"));
		String royaltyPrcntg = dataSet.get("royaltyPercentage");
		String advPercentage = dataSet.get("advertisementPercentage");
		String royaltyAreaFranchiseValue = dataSet.get("RoyaltyAreaFranchise");
		String advAreaFranchiseValue = dataSet.get("AdvAreaFranchise");
		
		

		try {
			driver = fc.loginpage().login(driver);

			fc.utobj().printTestStep("Navigate To Admin > Finance > Agreement Versions");
			AdminFinanceAgreementVersionsPageTest agreementforSales = new AdminFinanceAgreementVersionsPageTest();

			fc.utobj().printTestStep("Add Agreement");
			String agreementVersion = agreementforSales.addAgreement(driver, config, agrmntVrsnName, 
					 royaltyPrcntg, advPercentage, royaltyAreaFranchiseValue, advAreaFranchiseValue);

			fc.utobj().printTestStep("Navigate To Admin > Franchise Location > Add Franchise Location");
			fc.utobj().printTestStep("Add Franchise Location");

			String regionName = fc.utobj().generateTestData("Test");
			String storeType = fc.utobj().generateTestData("Test");
			String franchiseId = fc.utobj().generateTestData("Test");
			AdminFranchiseLocationAddFranchiseLocationPageTest franchise = new AdminFranchiseLocationAddFranchiseLocationPageTest();
			franchiseId = franchise.addFranchiseLocation_All_Agreement(driver, franchiseId, regionName, storeType,
					agreementVersion, config);

			fc.utobj().printTestStep("Navigate To Admin > Finance > Finance Setup Preferences");
			fc.utobj().printTestStep("Setup Medium of submission of Sales Reports");
			String salesReportType = "webForm";
			new AdminFinanceFinanceSetupPreferencesPageTest().mediumOfSubmissionOfSalesReports(driver, config,
					salesReportType);

			fc.utobj().printTestStep("Navigate To Finance > Sales");
			String categoryQuantity = "325";
			String categoryAmount = "953625";

			fc.utobj().printTestStep("Advertisement Amount On Acknowledgement Report");
			AdvertisementAmountOnAcknowledgementReport(driver, config, franchiseId, categoryQuantity, categoryAmount,
					advPercentage);

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	public String AdvertisementAmountOnAcknowledgementReport(WebDriver driver, Map<String, String> config,
			String franchiseID, String srvcQuantity1, String srvcAmount1, String advPercentage) throws Exception {

		FinanceSalesPage pobj = new FinanceSalesPage(driver);
		fc.finance().finance_common().financeSalesPage(driver);
		fc.utobj().clickElement(driver, pobj.enterSalesReport);
		fc.utobj().selectDropDown(driver, pobj.selectFranchise, franchiseID);

		fc.utobj().selectDropDownByIndex(driver, pobj.selectReportPeriod, 0);

		fc.utobj().clickElement(driver, pobj.ContinueBtn);

		fc.utobj().sendKeys(driver, pobj.serviceQuantity, srvcQuantity1);
		fc.utobj().sendKeys(driver, pobj.serviceAmount, srvcAmount1);

		fc.utobj().sendKeys(driver, pobj.rcvdDate, fc.utobj().getFutureDateUSFormat(-1));

		fc.utobj().sendKeys(driver, pobj.salesComment, "Automation Sales");
		fc.utobj().clickElement(driver, pobj.SaveBtn);

		String AdvAck = driver.findElement(By.xpath(".//tr[td[contains(text(), 'Adv. ($)')]]/td[2]")).getText();

		if (AdvAck.contains(",")) {
			AdvAck = AdvAck.replaceAll(",", "");
		}

		double Salestotal1 = Double.parseDouble(srvcAmount1);
		double advertsmntAck = Double.parseDouble(AdvAck);
		double adverPercentage = Double.parseDouble(advPercentage);
		double enteredSalesAmount = Salestotal1;
		double calAdvtsmntAmnt = ((enteredSalesAmount * adverPercentage) / 100);
		DecimalFormat df = new DecimalFormat("#.##");
		calAdvtsmntAmnt = Double.valueOf(df.format(calAdvtsmntAmnt));
		if (advertsmntAck != calAdvtsmntAmnt) {
			fc.utobj().throwsException(
					"Calculated Adevrtisemnt at Acknowledgment Page is Not Same as Advertisement for added Sales!");

		}

		fc.utobj().clickElement(driver, pobj.cancelAtAck);
		String reportID = driver
				.findElement(By.xpath(
						".//*[contains(text(),'" + franchiseID + "')]/ancestor::tr[@class='tb_data']//div[@id='sh']/a"))
				.getText();
		return reportID;
	}

	@Test(groups = { "finance" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "TC_FC_Verify_Royalty_Area_Franchise_Amount_OnAcknowledgement_Page_069", testCaseDescription = "Verify Royalty Area Franchise On Acknowledgement Page")
	public void verifyRoyaltyAreaFranchiseOnAcknowledgementPage() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		Map<String, String> dataSet = fc.utobj().readTestData("finance", testCaseId);

		String agrmntVrsnName = fc.utobj().generateTestData(dataSet.get("agreementVersionName"));
		String royaltyPrcntg = dataSet.get("royaltyPercentage");
		String advPercentage = dataSet.get("advertisementPercentage");
		String royaltyAreaFranchiseValue = dataSet.get("RoyaltyAreaFranchise");
		String advAreaFranchiseValue = dataSet.get("AdvAreaFranchise");
		
		

		try {
			driver = fc.loginpage().login(driver);

			fc.utobj().printTestStep("Navigate To Admin > Finance > Agreement Versions");
			AdminFinanceAgreementVersionsPageTest agreementforSales = new AdminFinanceAgreementVersionsPageTest();

			fc.utobj().printTestStep("Add Agreement");
			String agreementVersion = agreementforSales.addAgreement(driver, config, agrmntVrsnName, 
					 royaltyPrcntg, advPercentage, royaltyAreaFranchiseValue, advAreaFranchiseValue);

			fc.utobj().printTestStep("Navigate To Admin > Franchise Location > Add Franchise Location");
			fc.utobj().printTestStep("Add Franchise Location");

			String regionName = fc.utobj().generateTestData("Test");
			String storeType = fc.utobj().generateTestData("Test");
			String franchiseId = fc.utobj().generateTestData("Test");
			AdminFranchiseLocationAddFranchiseLocationPageTest franchise = new AdminFranchiseLocationAddFranchiseLocationPageTest();
			franchiseId = franchise.addFranchiseLocation_All_Agreement(driver, franchiseId, regionName, storeType,
					agreementVersion, config);

			fc.utobj().printTestStep("Navigate To Admin > Finance > Finance Setup Preferences");
			fc.utobj().printTestStep("Setup Medium of submission of Sales Reports");
			String salesReportType = "webForm";
			new AdminFinanceFinanceSetupPreferencesPageTest().mediumOfSubmissionOfSalesReports(driver, config,
					salesReportType);

			fc.utobj().printTestStep("Navigate To Finance > Sales");
			String categoryQuantity = "3215";
			String categoryAmount = "95365";

			fc.utobj().printTestStep("Royalty AF Amount On Acknowledgement Report");
			RoyaltyAFAmountOnAcknowledgementReport(driver, config, franchiseId, categoryQuantity, categoryAmount,
					royaltyPrcntg, royaltyAreaFranchiseValue);

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	public String RoyaltyAFAmountOnAcknowledgementReport(WebDriver driver, Map<String, String> config,
			String franchiseID, String srvcQuantity1, String srvcAmount1, String royaltyPrcntg,
			String royaltyAreaFranchiseValue) throws Exception {

		FinanceSalesPage pobj = new FinanceSalesPage(driver);
		fc.finance().finance_common().financeSalesPage(driver);
		fc.utobj().clickElement(driver, pobj.enterSalesReport);
		fc.utobj().selectDropDown(driver, pobj.selectFranchise, franchiseID);

		fc.utobj().selectDropDownByIndex(driver, pobj.selectReportPeriod, 0);

		fc.utobj().clickElement(driver, pobj.ContinueBtn);

		fc.utobj().sendKeys(driver, pobj.rcvdDate, fc.utobj().getFutureDateUSFormat(-1));

		fc.utobj().sendKeys(driver, pobj.serviceQuantity, srvcQuantity1);
		fc.utobj().sendKeys(driver, pobj.serviceAmount, srvcAmount1);
		fc.utobj().sendKeys(driver, pobj.salesComment, "Automation Sales");
		fc.utobj().clickElement(driver, pobj.SaveBtn);

		String royaltyAFAck = driver
				.findElement(By.xpath(".//tr[td[contains(text(), 'Royalty (Area Franchise) ($)')]]/td[2]")).getText();
		royaltyAFAck = royaltyAFAck.replaceAll(",", "");
		double Salestotal1 = Double.parseDouble(srvcAmount1);
		double RoyaltyAFAck = Double.parseDouble(royaltyAFAck);
		double royaltyPercent = Double.parseDouble(royaltyPrcntg);
		double royaltyAFPercent = Double.parseDouble(royaltyAreaFranchiseValue);
		double enteredSalesAmount = Salestotal1;
		double calRoyaltyAmnt = ((enteredSalesAmount * royaltyPercent) / 100.0);
		double calRoyaltyAFAmnt = (calRoyaltyAmnt * royaltyAFPercent) / 100.0;
		DecimalFormat df = new DecimalFormat("#.##");
		calRoyaltyAFAmnt = Double.valueOf(df.format(calRoyaltyAFAmnt));
		if (RoyaltyAFAck != calRoyaltyAFAmnt) {
			fc.utobj().throwsException(
					"Calculated Royalty Area Franchise at Acknowledgment Page is Not Same as Royalty Area Franchise for added Sales!");
		}

		fc.utobj().clickElement(driver, pobj.cancelAtAck);
		String reportID = driver
				.findElement(By.xpath(
						".//*[contains(text(),'" + franchiseID + "')]/ancestor::tr[@class='tb_data']//div[@id='sh']/a"))
				.getText();
		return reportID;
	}

	@Test(groups = { "finance" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2017-06-03", testCaseId = "TC_FC_Advertisement_Area_Franchise_Amount_OnAcknowledgement_Page_070", testCaseDescription = "Verify verify Advertisement Area Franchise Amount On Acknowledgement Page")
	public void verifyAdvertisementAreaFranchiseAmountOnAcknowledgementPage() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		Map<String, String> dataSet = fc.utobj().readTestData("finance", testCaseId);

		String agrmntVrsnName = fc.utobj().generateTestData(dataSet.get("agreementVersionName"));
		String royaltyPrcntg = dataSet.get("royaltyPercentage");
		String advPercentage = dataSet.get("advertisementPercentage");
		String royaltyAreaFranchiseValue = dataSet.get("RoyaltyAreaFranchise");
		String advAreaFranchiseValue = dataSet.get("AdvAreaFranchise");
		
		

		try {
			driver = fc.loginpage().login(driver);

			fc.utobj().printTestStep("Navigate To Admin > Finance > Agreement Versions");
			AdminFinanceAgreementVersionsPageTest agreementforSales = new AdminFinanceAgreementVersionsPageTest();

			fc.utobj().printTestStep("Add Agreement");
			String agreementVersion = agreementforSales.addAgreement(driver, config, agrmntVrsnName, 
					 royaltyPrcntg, advPercentage, royaltyAreaFranchiseValue, advAreaFranchiseValue);

			fc.utobj().printTestStep("Navigate To Admin > Franchise Location > Add Franchise Location");
			fc.utobj().printTestStep("Add Franchise Location");

			String regionName = fc.utobj().generateTestData("Test");
			String storeType = fc.utobj().generateTestData("Test");
			String franchiseId = fc.utobj().generateTestData("Test");
			AdminFranchiseLocationAddFranchiseLocationPageTest franchise = new AdminFranchiseLocationAddFranchiseLocationPageTest();
			franchiseId = franchise.addFranchiseLocation_All_Agreement(driver, franchiseId, regionName, storeType,
					agreementVersion, config);

			fc.utobj().printTestStep("Navigate To Admin > Finance > Finance Setup Preferences");
			fc.utobj().printTestStep("Setup Medium of submission of Sales Reports");
			String salesReportType = "webForm";
			new AdminFinanceFinanceSetupPreferencesPageTest().mediumOfSubmissionOfSalesReports(driver, config,
					salesReportType);

			String categoryQuantity = "110";
			String categoryAmount = "6000";

			AdvertisementAFAmountOnAcknowledgementReport(driver, config, franchiseId, categoryQuantity, categoryAmount,
					advPercentage, advAreaFranchiseValue);
			fc.utobj().printTestStep("Verify Advertisement Area Franchise Amount On Acknowledgement Page");
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	public String AdvertisementAFAmountOnAcknowledgementReport(WebDriver driver, Map<String, String> config,
			String franchiseID, String srvcQuantity1, String srvcAmount1, String advPercentage,
			String advAreaFranchiseValue) throws Exception {

		FinanceSalesPage pobj = new FinanceSalesPage(driver);
		fc.finance().finance_common().financeSalesPage(driver);
		fc.utobj().clickElement(driver, pobj.enterSalesReport);
		fc.utobj().selectDropDown(driver, pobj.selectFranchise, franchiseID);

		fc.utobj().selectDropDownByIndex(driver, pobj.selectReportPeriod, 0);

		fc.utobj().clickElement(driver, pobj.ContinueBtn);

		fc.utobj().sendKeys(driver, pobj.rcvdDate, fc.utobj().getFutureDateUSFormat(-1));

		fc.utobj().sendKeys(driver, pobj.serviceQuantity, srvcQuantity1);
		fc.utobj().sendKeys(driver, pobj.serviceAmount, srvcAmount1);
		fc.utobj().sendKeys(driver, pobj.salesComment, "Automation Sales");
		fc.utobj().clickElement(driver, pobj.SaveBtn);

		String AdvAFAck = driver.findElement(By.xpath(".//tr[td[contains(text(), 'Adv. (Area Franchise) ($)')]]/td[2]"))
				.getText();

		if (AdvAFAck.contains(",")) {
			AdvAFAck = AdvAFAck.replaceAll(",", "");
		}

		double enteredSalesAmount = Double.parseDouble(srvcAmount1);

		double advertsmntAFAck = Double.parseDouble(AdvAFAck);
		double adverPercentage = Double.parseDouble(advPercentage);
		double advAFPercent = Double.parseDouble(advAreaFranchiseValue);

		double calAdvtsmntAmnt = (enteredSalesAmount * adverPercentage) / 100.0;
		calAdvtsmntAmnt = Math.round(calAdvtsmntAmnt * 100.0) / 100.0;
		double calAdvtsmntAFAmnt = (calAdvtsmntAmnt * advAFPercent) / 100.0;
		DecimalFormat df = new DecimalFormat("#.##");
		calAdvtsmntAFAmnt = Double.valueOf(df.format(calAdvtsmntAFAmnt));

		if (advertsmntAFAck != calAdvtsmntAFAmnt) {
			fc.utobj().throwsException(
					"Calculated Adevrtisemnt Area Franchise at Acknowledgment Page is Not Same as Advertisement Area Franchise for added Sales!");
		}

		fc.utobj().clickElement(driver, pobj.cancelAtAck);
		String reportID = driver
				.findElement(By.xpath(
						".//*[contains(text(),'" + franchiseID + "')]/ancestor::tr[@class='tb_data']//div[@id='sh']/a"))
				.getText();
		return reportID;
	}

	@Test(groups = "finance")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "TC_FC_Verify_Total_Sales_Amount_Sales_Summary_Page_072", testCaseDescription = "Verify Total Sales Amount Sales Summary Page")
	public void verifyTotalSalesAmountSalesSummaryPage() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		Map<String, String> dataSet = fc.utobj().readTestData("finance", testCaseId);

		String agrmntVrsnName = fc.utobj().generateTestData(dataSet.get("agreementVersionName"));
		String royaltyPrcntg = dataSet.get("royaltyPercentage");
		String advPercentage = dataSet.get("advertisementPercentage");
		String royaltyAreaFranchiseValue = dataSet.get("RoyaltyAreaFranchise");
		String advAreaFranchiseValue = dataSet.get("AdvAreaFranchise");
		
		

		try {
			driver = fc.loginpage().login(driver);

			fc.utobj().printTestStep("Navigate To Admin > Finance > Agreement Versions");
			AdminFinanceAgreementVersionsPageTest agreementforSales = new AdminFinanceAgreementVersionsPageTest();

			fc.utobj().printTestStep("Add Agreement");
			String agreementVersion = agreementforSales.addAgreement(driver, config, agrmntVrsnName, 
					 royaltyPrcntg, advPercentage, royaltyAreaFranchiseValue, advAreaFranchiseValue);

			fc.utobj().printTestStep("Navigate To Admin > Franchise Location > Add Franchise Location");
			fc.utobj().printTestStep("Add Franchise Location");

			String regionName = fc.utobj().generateTestData("Test");
			String storeType = fc.utobj().generateTestData("Test");
			String franchiseId = fc.utobj().generateTestData("Test");
			AdminFranchiseLocationAddFranchiseLocationPageTest franchise = new AdminFranchiseLocationAddFranchiseLocationPageTest();
			franchiseId = franchise.addFranchiseLocation_All_Agreement(driver, franchiseId, regionName, storeType,
					agreementVersion, config);

			fc.utobj().printTestStep("Navigate To Admin > Finance > Finance Setup Preferences");
			fc.utobj().printTestStep("Setup Medium of submission of Sales Reports");
			String salesReportType = "webForm";
			new AdminFinanceFinanceSetupPreferencesPageTest().mediumOfSubmissionOfSalesReports(driver, config,
					salesReportType);

			fc.utobj().printTestStep("Navigate To Finance > Sales");
			fc.utobj().printTestStep("Enter Sales Report");
			String categoryQuantity = "110";
			String categoryAmount = "6000";
			String reportId = enterSalesReport(driver, franchiseId, categoryQuantity, categoryAmount);

			fc.utobj().printTestStep("Filter Sales Report By Franchise ID");
			filterSalesReport(driver, franchiseId);
			fc.utobj().printTestStep("Verify total sales amount at sales summary");
			verifyTotalSalesAmountSalesSummary(driver, config, reportId, royaltyPrcntg, advPercentage, categoryAmount);
			
			WebElement e = driver.findElement(By.xpath(".//*[contains(text(),'" + reportId + "')]"));
			fc.utobj().moveToElement(driver, e);
			
			e.click();
			
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	public void verifyTotalSalesAmountSalesSummary(WebDriver driver, Map<String, String> config, String reportID,
			String royaltyPrcntg, String advPercentage, String srvcAmount1) throws Exception {

		WebElement e = driver.findElement(
				By.xpath(".//*[contains(text(),'" + reportID + "')]/ancestor::tr[@class='tb_data']/td[7]"));
		fc.utobj().moveToElement(driver, e);
		String totalSalesatSummary = e.getText();

		if (totalSalesatSummary.contains(",")) {
			totalSalesatSummary = totalSalesatSummary.replaceAll(",", "");
		}

		double totalSales = Double.parseDouble(totalSalesatSummary);
		double enteredSalesAmount = Double.parseDouble(srvcAmount1);
		DecimalFormat df = new DecimalFormat("#.##");
		enteredSalesAmount = Double.valueOf(df.format(enteredSalesAmount));
		if (totalSales != enteredSalesAmount) {
			fc.utobj().throwsException(
					"Total Sales Amount at Sales Summary Page is not same as Calculated Total Sales Amount! ");
		}
	}

	@Test(groups = { "finance" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "TC_FC_Verify_Royalty_Amount_Sales_Summary_Page_097", testCaseDescription = "Verify Royalty Amount Sales Summary Page")
	public void verifyRoyaltyAmountSalesSummaryPage() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		Map<String, String> dataSet = fc.utobj().readTestData("finance", testCaseId);

		String agrmntVrsnName = fc.utobj().generateTestData(dataSet.get("agreementVersionName"));
		String royaltyPrcntg = dataSet.get("royaltyPercentage");
		String advPercentage = dataSet.get("advertisementPercentage");
		String royaltyAreaFranchiseValue = dataSet.get("RoyaltyAreaFranchise");
		String advAreaFranchiseValue = dataSet.get("AdvAreaFranchise");
		
		

		try {
			driver = fc.loginpage().login(driver);

			fc.utobj().printTestStep("Navigate To Admin > Finance > Agreement Versions");
			AdminFinanceAgreementVersionsPageTest agreementforSales = new AdminFinanceAgreementVersionsPageTest();

			fc.utobj().printTestStep("Add Agreement");
			String agreementVersion = agreementforSales.addAgreement(driver, config, agrmntVrsnName, 
					 royaltyPrcntg, advPercentage, royaltyAreaFranchiseValue, advAreaFranchiseValue);

			fc.utobj().printTestStep("Navigate To Admin > Franchise Location > Add Franchise Location");
			fc.utobj().printTestStep("Add Franchise Location");

			String regionName = fc.utobj().generateTestData("Test");
			String storeType = fc.utobj().generateTestData("Test");
			String franchiseId = fc.utobj().generateTestData("Test");
			AdminFranchiseLocationAddFranchiseLocationPageTest franchise = new AdminFranchiseLocationAddFranchiseLocationPageTest();
			franchiseId = franchise.addFranchiseLocation_All_Agreement(driver, franchiseId, regionName, storeType,
					agreementVersion, config);

			fc.utobj().printTestStep("Navigate To Admin > Finance > Finance Setup Preferences");
			fc.utobj().printTestStep("Setup Medium of submission of Sales Reports");
			String salesReportType = "webForm";
			new AdminFinanceFinanceSetupPreferencesPageTest().mediumOfSubmissionOfSalesReports(driver, config,
					salesReportType);

			fc.utobj().printTestStep("Navigate To Finance > Sales");
			fc.utobj().printTestStep("Enter Sales Report");
			String categoryQuantity = "110";
			String categoryAmount = "6000";
			String reportId = enterSalesReport(driver, franchiseId, categoryQuantity, categoryAmount);

			fc.utobj().printTestStep("Filter Sales Report By Franchise Id");
			filterSalesReport(driver, franchiseId);

			fc.utobj().printTestStep("Verify Royalty Amount Sales Summary");

			verifyRoyaltyAmountSalesSummary(driver, config, reportId, royaltyPrcntg, advPercentage, categoryAmount);

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	public void verifyRoyaltyAmountSalesSummary(WebDriver driver, Map<String, String> config, String reportID,
			String royaltyPrcntg, String advPercentage, String srvcAmount1) throws Exception {

		double enteredSalesAmount = Double.parseDouble(srvcAmount1);

		DecimalFormat df = new DecimalFormat("#.##");
		enteredSalesAmount = Double.valueOf(df.format(enteredSalesAmount));

		double royaltyPercent = Double.parseDouble(royaltyPrcntg);

		double calRoyaltyAmnt = ((enteredSalesAmount * royaltyPercent) / 100.0);

		DecimalFormat df1 = new DecimalFormat("#.##");
		calRoyaltyAmnt = Double.valueOf(df1.format(calRoyaltyAmnt));

		WebElement element2 = driver.findElement(
				By.xpath(".//*[contains(text(),'" + reportID + "')]/ancestor::tr[@class='tb_data']/td[8]"));

		fc.utobj().moveToElement(driver, element2);

		String totalRoyaltyatSummary = element2.getText();

		double totalRoyalty = Double.parseDouble(totalRoyaltyatSummary);

		if (calRoyaltyAmnt != totalRoyalty) {

			fc.utobj().throwsException("Royalty Amount is not same as royalty calculated");
		}

	}

	@Test(groups = "finance")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "TC_FC_Advertisement_Amount_Sales_Summary_Page_098", testCaseDescription = "Verify Advertisement Amount Sales Summary Page")
	public void verifyAdvertisementAmountSalesSummaryPage() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		Map<String, String> dataSet = fc.utobj().readTestData("finance", testCaseId);

		String agrmntVrsnName = fc.utobj().generateTestData(dataSet.get("agreementVersionName"));
		String royaltyPrcntg = dataSet.get("royaltyPercentage");
		String advPercentage = dataSet.get("advertisementPercentage");
		String royaltyAreaFranchiseValue = dataSet.get("RoyaltyAreaFranchise");
		String advAreaFranchiseValue = dataSet.get("AdvAreaFranchise");
		
		

		try {
			driver = fc.loginpage().login(driver);

			fc.utobj().printTestStep("Navigate To Admin > Finance > Agreement Versions");
			AdminFinanceAgreementVersionsPageTest agreementforSales = new AdminFinanceAgreementVersionsPageTest();

			fc.utobj().printTestStep("Add Agreement");
			String agreementVersion = agreementforSales.addAgreement(driver, config, agrmntVrsnName, 
					 royaltyPrcntg, advPercentage, royaltyAreaFranchiseValue, advAreaFranchiseValue);

			fc.utobj().printTestStep("Navigate To Admin > Franchise Location > Add Franchise Location");
			fc.utobj().printTestStep("Add Franchise Location");

			String regionName = fc.utobj().generateTestData("Test");
			String storeType = fc.utobj().generateTestData("Test");
			String franchiseId = fc.utobj().generateTestData("Test");
			AdminFranchiseLocationAddFranchiseLocationPageTest franchise = new AdminFranchiseLocationAddFranchiseLocationPageTest();
			franchiseId = franchise.addFranchiseLocation_All_Agreement(driver, franchiseId, regionName, storeType,
					agreementVersion, config);

			fc.utobj().printTestStep("Navigate To Admin > Finance > Finance Setup Preferences");
			fc.utobj().printTestStep("Setup Medium of submission of Sales Reports");
			String salesReportType = "webForm";
			new AdminFinanceFinanceSetupPreferencesPageTest().mediumOfSubmissionOfSalesReports(driver, config,
					salesReportType);

			fc.utobj().printTestStep("Navigate To Finance > Sales");
			fc.utobj().printTestStep("Enter Sales Report");
			String categoryQuantity = "110";
			String categoryAmount = "6000";
			String reportId = enterSalesReport(driver, franchiseId, categoryQuantity, categoryAmount);

			fc.utobj().printTestStep("Filter Sales Report By Franchise Id");
			filterSalesReport(driver, franchiseId);

			fc.utobj().printTestStep("Verify Advertisement Amount Sales Summary");
			verifyAdvertisementAmountSalesSummary(driver, config, reportId, royaltyPrcntg, advPercentage,
					categoryAmount);

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	public void verifyAdvertisementAmountSalesSummary(WebDriver driver, Map<String, String> config, String reportID,
			String royaltyPrcntg, String advPercentage, String srvcAmount1) throws Exception {

		double enteredSalesAmount = Double.parseDouble(srvcAmount1);

		DecimalFormat df = new DecimalFormat("#.##");
		enteredSalesAmount = Double.valueOf(df.format(enteredSalesAmount));

		double advPercent = Double.parseDouble(advPercentage);

		double calAdvAmnt = ((enteredSalesAmount * advPercent) / 100.0);

		DecimalFormat df2 = new DecimalFormat("#.##");

		calAdvAmnt = Double.valueOf(df2.format(calAdvAmnt));

		WebElement element = driver.findElement(
				By.xpath(".//*[contains(text(),'" + reportID + "')]/ancestor::tr[@class='tb_data']/td[9]"));

		fc.utobj().moveToElement(driver, element);

		String totalAdvatSummary = element.getText();

		double totalAdv = Double.parseDouble(totalAdvatSummary);

		if (calAdvAmnt != totalAdv) {
			fc.utobj().throwsException("Advertisement Amount is not same as Adv calculated");
		}
	}

	@Test(groups = { "finance" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "TC_FC_Total_Sales_Amount_Sales_Report_Details_Page_073", testCaseDescription = "Verify Total Sales Amount on Sales Report Details Page")
	public void verifyTotalSalesAmountonSalesReportDetailsPage() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		Map<String, String> dataSet = fc.utobj().readTestData("finance", testCaseId);

		String agrmntVrsnName = fc.utobj().generateTestData(dataSet.get("agreementVersionName"));
		String royaltyPrcntg = dataSet.get("royaltyPercentage");
		String advPercentage = dataSet.get("advertisementPercentage");
		String royaltyAreaFranchiseValue = dataSet.get("RoyaltyAreaFranchise");
		String advAreaFranchiseValue = dataSet.get("AdvAreaFranchise");
		
		

		try {
			driver = fc.loginpage().login(driver);

			fc.utobj().printTestStep("Navigate To Admin > Finance > Agreement Versions");
			AdminFinanceAgreementVersionsPageTest agreementforSales = new AdminFinanceAgreementVersionsPageTest();

			fc.utobj().printTestStep("Add Agreement");
			String agreementVersion = agreementforSales.addAgreement(driver, config, agrmntVrsnName, 
					 royaltyPrcntg, advPercentage, royaltyAreaFranchiseValue, advAreaFranchiseValue);

			fc.utobj().printTestStep("Navigate To Admin > Franchise Location > Add Franchise Location");
			fc.utobj().printTestStep("Add Franchise Location");

			String regionName = fc.utobj().generateTestData("Test");
			String storeType = fc.utobj().generateTestData("Test");
			String franchiseId = fc.utobj().generateTestData("Test");
			AdminFranchiseLocationAddFranchiseLocationPageTest franchise = new AdminFranchiseLocationAddFranchiseLocationPageTest();
			franchiseId = franchise.addFranchiseLocation_All_Agreement(driver, franchiseId, regionName, storeType,
					agreementVersion, config);

			fc.utobj().printTestStep("Navigate To Admin > Finance > Finance Setup Preferences");
			fc.utobj().printTestStep("Setup Medium of submission of Sales Reports");
			String salesReportType = "webForm";
			new AdminFinanceFinanceSetupPreferencesPageTest().mediumOfSubmissionOfSalesReports(driver, config,
					salesReportType);

			fc.utobj().printTestStep("Navigate To Finance > Sales");
			fc.utobj().printTestStep("Enter Sales Report");
			String categoryQuantity = "110";
			String categoryAmount = "6000";
			String reportId = enterSalesReport(driver, franchiseId, categoryQuantity, categoryAmount);

			fc.utobj().printTestStep("Filter Sales Report By Franchise Id");
			filterSalesReport(driver, franchiseId);

			fc.utobj().printTestStep("Verify total sales amount on sales details page");
			verifyTotalSalesAmountSalesDetails(driver, config, reportId, royaltyPrcntg, advPercentage, categoryAmount);

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	public void verifyTotalSalesAmountSalesDetails(WebDriver driver, Map<String, String> config, String reportID,
			String royaltyPrcntg, String advPercentage, String srvcAmount1) throws Exception {

		fc.utobj().clickLink(driver, reportID);
		String totalSalesAmount = driver.findElement(By.xpath(".//tr[td[contains(text(), 'Total Sales ($)')]]/td[2]"))
				.getText();
		totalSalesAmount = totalSalesAmount.replaceAll(",", "");
		double totalSalesAmnt = Double.parseDouble(totalSalesAmount);
		double enteredSalesAmount = Double.parseDouble(srvcAmount1);
		DecimalFormat df = new DecimalFormat("#.##");
		enteredSalesAmount = Double.valueOf(df.format(enteredSalesAmount));
		if (totalSalesAmnt != enteredSalesAmount) {
			fc.utobj().throwsException(
					"Total Sales Amount at Sales Report Details Page is not same as Calculated Total Sales Amount! ");
		}
	}

	@Test(groups = { "finance" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "TC_FC_Verify_Royalty_Amount_Sales_Report_Details_Page_099", testCaseDescription = "Verify Royalty Amount Sales Report Details Page")
	public void verifyRoyaltyAmountSalesReportDetailsPage() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		Map<String, String> dataSet = fc.utobj().readTestData("finance", testCaseId);

		String agrmntVrsnName = fc.utobj().generateTestData(dataSet.get("agreementVersionName"));
		String royaltyPrcntg = dataSet.get("royaltyPercentage");
		String advPercentage = dataSet.get("advertisementPercentage");
		String royaltyAreaFranchiseValue = dataSet.get("RoyaltyAreaFranchise");
		String advAreaFranchiseValue = dataSet.get("AdvAreaFranchise");
		
		

		try {
			driver = fc.loginpage().login(driver);

			fc.utobj().printTestStep("Navigate To Admin > Finance > Agreement Versions");
			AdminFinanceAgreementVersionsPageTest agreementforSales = new AdminFinanceAgreementVersionsPageTest();

			fc.utobj().printTestStep("Add Agreement");
			String agreementVersion = agreementforSales.addAgreement(driver, config, agrmntVrsnName, 
					 royaltyPrcntg, advPercentage, royaltyAreaFranchiseValue, advAreaFranchiseValue);

			fc.utobj().printTestStep("Navigate To Admin > Franchise Location > Add Franchise Location");
			fc.utobj().printTestStep("Add Franchise Location");

			String regionName = fc.utobj().generateTestData("Test");
			String storeType = fc.utobj().generateTestData("Test");
			String franchiseId = fc.utobj().generateTestData("Test");
			AdminFranchiseLocationAddFranchiseLocationPageTest franchise = new AdminFranchiseLocationAddFranchiseLocationPageTest();
			franchiseId = franchise.addFranchiseLocation_All_Agreement(driver, franchiseId, regionName, storeType,
					agreementVersion, config);

			fc.utobj().printTestStep("Navigate To Admin > Finance > Finance Setup Preferences");
			fc.utobj().printTestStep("Setup Medium of submission of Sales Reports");
			String salesReportType = "webForm";
			new AdminFinanceFinanceSetupPreferencesPageTest().mediumOfSubmissionOfSalesReports(driver, config,
					salesReportType);

			fc.utobj().printTestStep("Navigate To Finance > Sales");
			fc.utobj().printTestStep("Enter Sales Report");
			String categoryQuantity = "110";
			String categoryAmount = "6000";
			String reportId = enterSalesReport(driver, franchiseId, categoryQuantity, categoryAmount);

			fc.utobj().printTestStep("Filter Sales Report By Franchise Id");
			filterSalesReport(driver, franchiseId);

			fc.utobj().printTestStep("Verify Royalty Amount at Sales Details page");
			verifyRoyaltyAmountSalesDetails(driver, config, reportId, royaltyPrcntg, advPercentage, categoryAmount);

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	public void verifyRoyaltyAmountSalesDetails(WebDriver driver, Map<String, String> config, String reportID,
			String royaltyPrcntg, String advPercentage, String srvcAmount1) throws Exception {

		fc.utobj().clickLink(driver, reportID);
		double enteredSalesAmount = Double.parseDouble(srvcAmount1);

		DecimalFormat df = new DecimalFormat("#.##");
		enteredSalesAmount = Double.valueOf(df.format(enteredSalesAmount));
		double royaltyPercent = Double.parseDouble(royaltyPrcntg);
		double calRoyaltyAmt = ((enteredSalesAmount * royaltyPercent) / 100.0);
		DecimalFormat df1 = new DecimalFormat("#.##");
		calRoyaltyAmt = Double.valueOf(df1.format(calRoyaltyAmt));
		// WebElement element =
		// driver.findElement(By.xpath(".//tr[td[contains(text(), 'Royalty
		// ($)')]]/td[2]"));

		WebElement element = driver.findElement(By.xpath(
				".//td[contains(text(), 'Royalty')][not(contains(text () , 'Royalty (Area Franchise)'))]/following-sibling::td[1]"));

		fc.utobj().moveToElement(driver, element);
		String totalRoyaltyatSummary = element.getText();

		if (totalRoyaltyatSummary.contains(",")) {
			totalRoyaltyatSummary = totalRoyaltyatSummary.replaceAll(",", "");

		}

		double totalRoyalty = Double.parseDouble(totalRoyaltyatSummary);
		if (calRoyaltyAmt != totalRoyalty) {
			fc.utobj().throwsException("Royalty Amount is not same as royalty calculated");
		}
	}

	@Test(groups = "finance")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "TC_FC_Advertisement_Amount_Sales_Report_Details_Page_100", testCaseDescription = "Verify Advertisement Amount Sales Report Details Page")
	public void verifyAdvertisementAmountSalesReportDetailsPage() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		Map<String, String> dataSet = fc.utobj().readTestData("finance", testCaseId);

		String agrmntVrsnName = fc.utobj().generateTestData(dataSet.get("agreementVersionName"));
		String royaltyPrcntg = dataSet.get("royaltyPercentage");
		String advPercentage = dataSet.get("advertisementPercentage");
		String royaltyAreaFranchiseValue = dataSet.get("RoyaltyAreaFranchise");
		String advAreaFranchiseValue = dataSet.get("AdvAreaFranchise");
		
		

		try {
			driver = fc.loginpage().login(driver);

			fc.utobj().printTestStep("Navigate To Admin > Finance > Agreement Versions");
			AdminFinanceAgreementVersionsPageTest agreementforSales = new AdminFinanceAgreementVersionsPageTest();

			fc.utobj().printTestStep("Add Agreement");
			String agreementVersion = agreementforSales.addAgreement(driver, config, agrmntVrsnName, 
					 royaltyPrcntg, advPercentage, royaltyAreaFranchiseValue, advAreaFranchiseValue);

			fc.utobj().printTestStep("Navigate To Admin > Franchise Location > Add Franchise Location");
			fc.utobj().printTestStep("Add Franchise Location");

			String regionName = fc.utobj().generateTestData("Test");
			String storeType = fc.utobj().generateTestData("Test");
			String franchiseId = fc.utobj().generateTestData("Test");
			AdminFranchiseLocationAddFranchiseLocationPageTest franchise = new AdminFranchiseLocationAddFranchiseLocationPageTest();
			franchiseId = franchise.addFranchiseLocation_All_Agreement(driver, franchiseId, regionName, storeType,
					agreementVersion, config);

			fc.utobj().printTestStep("Navigate To Admin > Finance > Finance Setup Preferences");
			fc.utobj().printTestStep("Setup Medium of submission of Sales Reports");
			String salesReportType = "webForm";
			new AdminFinanceFinanceSetupPreferencesPageTest().mediumOfSubmissionOfSalesReports(driver, config,
					salesReportType);

			fc.utobj().printTestStep("Navigate To Finance > Sales");
			fc.utobj().printTestStep("Enter Sales Report");
			String categoryQuantity = "110";
			String categoryAmount = "6000";
			String reportId = enterSalesReport(driver, franchiseId, categoryQuantity, categoryAmount);

			fc.utobj().printTestStep("Filter Sales Report By Franchise Id");
			filterSalesReport(driver, franchiseId);

			fc.utobj().printTestStep("Verify Advertisement Amount");
			verifyAdvertisementAmountSalesDetails(driver, config, reportId, royaltyPrcntg, advPercentage,
					categoryAmount);

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	public void verifyAdvertisementAmountSalesDetails(WebDriver driver, Map<String, String> config, String reportID,
			String royaltyPrcntg, String advPercentage, String srvcAmount1) throws Exception {

		fc.utobj().clickLink(driver, reportID);
		double enteredSalesAmount = Double.parseDouble(srvcAmount1);
		DecimalFormat df = new DecimalFormat("#.##");
		enteredSalesAmount = Double.valueOf(df.format(enteredSalesAmount));
		double advPercent = Double.parseDouble(advPercentage);
		double calAdvAmnt = ((enteredSalesAmount * advPercent) / 100.0);
		DecimalFormat df2 = new DecimalFormat("#.##");
		calAdvAmnt = Double.valueOf(df2.format(calAdvAmnt));
		String totalAdvatSummary = driver.findElement(By.xpath(".//tr[td[contains(text(), 'Adv. ($)')]]/td[2]"))
				.getText();
		double totalAdv = Double.parseDouble(totalAdvatSummary);
		if (calAdvAmnt != totalAdv) {
			fc.utobj().throwsException("Advertisement Amount is not same as Adv calculated");
		}
	}

	@Test(groups = { "finance", "finance_FailedTC" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "TC_FC_Verify_RoyaltyAF_Sales_Report_Details_Page_074", testCaseDescription = "Verify Royalty AF Sales Report Details Page")
	public void verifyRoyaltyAFSalesReportDetailsPage() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		Map<String, String> dataSet = fc.utobj().readTestData("finance", testCaseId);

		String agrmntVrsnName = fc.utobj().generateTestData(dataSet.get("agreementVersionName"));
		String royaltyPrcntg = dataSet.get("royaltyPercentage");
		String advPercentage = dataSet.get("advertisementPercentage");
		String royaltyAreaFranchiseValue = dataSet.get("RoyaltyAreaFranchise");
		String advAreaFranchiseValue = dataSet.get("AdvAreaFranchise");
		
		try {
			driver = fc.loginpage().login(driver);

			fc.utobj().printTestStep("Navigate To Admin > Finance > Agreement Versions");
			AdminFinanceAgreementVersionsPageTest agreementforSales = new AdminFinanceAgreementVersionsPageTest();

			fc.utobj().printTestStep("Add Agreement");
			String agreementVersion = agreementforSales.addAgreement(driver, config, agrmntVrsnName, 
					 royaltyPrcntg, advPercentage, royaltyAreaFranchiseValue, advAreaFranchiseValue);

			fc.utobj().printTestStep("Navigate To Admin > Franchise Location > Add Franchise Location");
			fc.utobj().printTestStep("Add Franchise Location");

			String regionName = fc.utobj().generateTestData("Test");
			String storeType = fc.utobj().generateTestData("Test");
			String franchiseId = fc.utobj().generateTestData("Test");
			AdminFranchiseLocationAddFranchiseLocationPageTest franchise = new AdminFranchiseLocationAddFranchiseLocationPageTest();
			franchiseId = franchise.addFranchiseLocation_All_Agreement(driver, franchiseId, regionName, storeType,
					agreementVersion, config);

			fc.utobj().printTestStep("Navigate To Admin > Finance > Finance Setup Preferences");
			fc.utobj().printTestStep("Setup Medium of submission of Sales Reports");
			String salesReportType = "webForm";
			new AdminFinanceFinanceSetupPreferencesPageTest().mediumOfSubmissionOfSalesReports(driver, config,
					salesReportType);

			fc.utobj().printTestStep("Navigate To Finance > Sales");
			fc.utobj().printTestStep("Enter Sales Report");
			String categoryQuantity = "110";
			String categoryAmount = "6000";
			String reportId = enterSalesReport(driver, franchiseId, categoryQuantity, categoryAmount);

			fc.utobj().printTestStep("Filter Sales Report By Franchise Id");
			filterSalesReport(driver, franchiseId);

			verifyRoyaltyAFSalesDetails(driver, config, reportId, royaltyPrcntg, advPercentage,
					royaltyAreaFranchiseValue, advAreaFranchiseValue, categoryAmount);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	public void verifyRoyaltyAFSalesDetails(WebDriver driver, Map<String, String> config, String reportID,
			String royaltyPrcntg, String advPercentage, String royaltyAreaFranchiseValue, String advAreaFranchiseValue,
			String srvcAmount1) throws Exception {

		fc.utobj().clickLink(driver, reportID);
		double royaltyAFPercent = Double.parseDouble(royaltyAreaFranchiseValue);
		double enteredSalesAmount = Double.parseDouble(srvcAmount1);

		double royaltyPercent = Double.parseDouble(royaltyPrcntg);
		double calRoyaltyAmt = ((enteredSalesAmount * royaltyPercent) / 100.0);
		double calRoyaltyAFAmt = ((calRoyaltyAmt * royaltyAFPercent) / 100.0);
		DecimalFormat df = new DecimalFormat("#.##");
		calRoyaltyAFAmt = Double.valueOf(df.format(calRoyaltyAFAmt));
		WebElement element = driver
				.findElement(By.xpath(".//tr[td[contains(text(), 'Royalty (Area Franchise) ($)')]]/td[2]"));
		fc.utobj().moveToElement(driver, element);
		String totalRoyaltyAFatSummary = element.getText();
		double totalRoyaltyAF = Double.parseDouble(totalRoyaltyAFatSummary);
		if (calRoyaltyAFAmt != totalRoyaltyAF) {
			fc.utobj()
					.throwsException("Royalty Area Franchise Amount is not same as Royalty Area Franchise calculated");
		}
	}

	@Test(groups = "finance")
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "TC_FC_Verify_AdvAF_Sales_Report_Details_Page_101", testCaseDescription = "Verify Adv AF Sales Report Details Page")
	public void verifyAdvAFSalesReportDetailsPage() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		Map<String, String> dataSet = fc.utobj().readTestData("finance", testCaseId);

		String agrmntVrsnName = fc.utobj().generateTestData(dataSet.get("agreementVersionName"));
		String royaltyPrcntg = dataSet.get("royaltyPercentage");
		String advPercentage = dataSet.get("advertisementPercentage");
		String royaltyAreaFranchiseValue = dataSet.get("RoyaltyAreaFranchise");
		String advAreaFranchiseValue = dataSet.get("AdvAreaFranchise");
		
		

		try {
			driver = fc.loginpage().login(driver);

			fc.utobj().printTestStep("Navigate To Admin > Finance > Agreement Versions");
			AdminFinanceAgreementVersionsPageTest agreementforSales = new AdminFinanceAgreementVersionsPageTest();

			fc.utobj().printTestStep("Add Agreement");
			String agreementVersion = agreementforSales.addAgreement(driver, config, agrmntVrsnName, 
					 royaltyPrcntg, advPercentage, royaltyAreaFranchiseValue, advAreaFranchiseValue);

			fc.utobj().printTestStep("Navigate To Admin > Franchise Location > Add Franchise Location");
			fc.utobj().printTestStep("Add Franchise Location");

			String regionName = fc.utobj().generateTestData("Test");
			String storeType = fc.utobj().generateTestData("Test");
			String franchiseId = fc.utobj().generateTestData("Test");
			AdminFranchiseLocationAddFranchiseLocationPageTest franchise = new AdminFranchiseLocationAddFranchiseLocationPageTest();
			franchiseId = franchise.addFranchiseLocation_All_Agreement(driver, franchiseId, regionName, storeType,
					agreementVersion, config);

			fc.utobj().printTestStep("Navigate To Admin > Finance > Finance Setup Preferences");
			fc.utobj().printTestStep("Setup Medium of submission of Sales Reports");
			String salesReportType = "webForm";
			new AdminFinanceFinanceSetupPreferencesPageTest().mediumOfSubmissionOfSalesReports(driver, config,
					salesReportType);

			fc.utobj().printTestStep("Navigate To Finance > Sales");
			fc.utobj().printTestStep("Enter Sales Report");
			String categoryQuantity = "110";
			String categoryAmount = "6000";
			String reportId = enterSalesReport(driver, franchiseId, categoryQuantity, categoryAmount);

			fc.utobj().printTestStep("Filter Sales Report By Franchise Id");
			filterSalesReport(driver, franchiseId);

			fc.utobj().printTestStep("Verify Adv AF Sales Details");
			verifyAdvAFSalesDetails(driver, config, reportId, royaltyPrcntg, advPercentage, royaltyAreaFranchiseValue,
					advAreaFranchiseValue, categoryAmount);

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	public void verifyAdvAFSalesDetails(WebDriver driver, Map<String, String> config, String reportID,
			String royaltyPrcntg, String advPercentage, String royaltyAreaFranchiseValue, String advAreaFranchiseValue,
			String srvcAmount1) throws Exception {

		fc.utobj().clickLink(driver, reportID);
		double enteredSalesAmount = Double.parseDouble(srvcAmount1);
		double advAFPercent = Double.parseDouble(advAreaFranchiseValue);
		double advPercent = Double.parseDouble(advPercentage);
		double calAdvAmnt = ((enteredSalesAmount * advPercent) / 100.0);
		double calAdvAFAmnt = ((calAdvAmnt * advAFPercent) / 100.0);
		DecimalFormat df1 = new DecimalFormat("#.##");
		calAdvAFAmnt = Double.valueOf(df1.format(calAdvAFAmnt));
		WebElement element2 = driver
				.findElement(By.xpath(".//tr[td[contains(text(), 'Adv. (Area Franchise) ($)')]]/td[2]"));
		fc.utobj().moveToElement(driver, element2);
		String totalAdvAFAmnt = element2.getText();
		double totalAdvAFAmt = Double.parseDouble(totalAdvAFAmnt);
		if (calAdvAFAmnt != totalAdvAFAmt) {
			fc.utobj().throwsException(
					"Advertisement Area Franchise Amount is not same as Adv Area Franchise Calculated");
		}
	}

	public String approveSalesViaAction(WebDriver driver, Map<String, String> config, String reportID)
			throws Exception {

		FinanceSalesPage pobj = new FinanceSalesPage(driver);

		WebElement element = driver.findElement(By.xpath(".//*[contains(text(),'" + reportID
				+ "')]/ancestor::tr[@class='tb_data']//input[@name='selectedItem']"));
		element.click();

		String actionToTake = "Approve";

		WebElement e1 = driver.findElement(By.xpath(
				".//*[contains(text(),'" + reportID + "')]/ancestor::tr[@class='tb_data']//div[@id='menuBar']/layer"));

		fc.utobj().moveToElement(driver, e1);

		String layerId = e1.getAttribute("id");

		WebElement element1 = driver.findElement(By.xpath(".//*[contains(text(),'" + reportID
				+ "')]/ancestor::tr[@class='tb_data']//div[@id='menuBar']/layer/a"));

		element1.click();

		layerId = layerId.replace("Actions_dynamicmenu", "");

		layerId = layerId.replace("Bar", "");

		WebElement element2 = driver.findElement(By.xpath(
				".//*[@id='Actions_dynamicmenu" + layerId + "Menu']/span[contains(text(),'" + actionToTake + "')]"));

		fc.utobj().clickElement(driver, element2);

		driver.switchTo().frame(pobj.commentsIFrame);

		fc.utobj().sendKeys(driver, pobj.commentsApproveReject, "Sales Approved Via Action Menu");

		fc.utobj().clickElement(driver, pobj.saveBtnApproveReject);

		return reportID;
	}

	@Test(groups = { "finance" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "TC_FC_Reject_Sales_Report_078", testCaseDescription = "Verify Reject Sales Report")
	public void rejectSalesReport() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		Map<String, String> dataSet = fc.utobj().readTestData("finance", testCaseId);

		String agrmntVrsnName = fc.utobj().generateTestData(dataSet.get("agreementVersionName"));
		String royaltyPrcntg = dataSet.get("royaltyPercentage");
		String advPercentage = dataSet.get("advertisementPercentage");
		String royaltyAreaFranchiseValue = dataSet.get("RoyaltyAreaFranchise");
		String advAreaFranchiseValue = dataSet.get("AdvAreaFranchise");
		
		

		try {
			driver = fc.loginpage().login(driver);

			fc.utobj().printTestStep("Navigate To Admin > Finance > Agreement Versions");
			AdminFinanceAgreementVersionsPageTest agreementforSales = new AdminFinanceAgreementVersionsPageTest();

			fc.utobj().printTestStep("Add Agreement");
			String agreementVersion = agreementforSales.addAgreement(driver, config, agrmntVrsnName, 
					 royaltyPrcntg, advPercentage, royaltyAreaFranchiseValue, advAreaFranchiseValue);

			fc.utobj().printTestStep("Navigate To Admin > Franchise Location > Add Franchise Location");
			fc.utobj().printTestStep("Add Franchise Location");

			String regionName = fc.utobj().generateTestData("Test");
			String storeType = fc.utobj().generateTestData("Test");
			String franchiseId = fc.utobj().generateTestData("Test");
			AdminFranchiseLocationAddFranchiseLocationPageTest franchise = new AdminFranchiseLocationAddFranchiseLocationPageTest();
			franchiseId = franchise.addFranchiseLocation_All_Agreement(driver, franchiseId, regionName, storeType,
					agreementVersion, config);

			fc.utobj().printTestStep("Navigate To Admin > Finance > Finance Setup Preferences");
			fc.utobj().printTestStep("Setup Medium of submission of Sales Reports");
			String salesReportType = "webForm";
			new AdminFinanceFinanceSetupPreferencesPageTest().mediumOfSubmissionOfSalesReports(driver, config,
					salesReportType);

			fc.utobj().printTestStep("Navigate To Finance > Sales");
			fc.utobj().printTestStep("Enter Sales Report");
			String categoryQuantity = "110";
			String categoryAmount = "6000";
			String reportId = enterSalesReport(driver, franchiseId, categoryQuantity, categoryAmount);

			fc.utobj().printTestStep("Filter Sales Report By Franchise Id");
			filterSalesReport(driver, franchiseId);

			fc.utobj().printTestStep("Reject Sales report");
			rejectSalesReport(driver, config, reportId);

			fc.utobj().printTestStep("Verify The Rejected Sales Report");
			boolean isStatusPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text () , '" + reportId + "')]/ancestor::tr/td[contains(text () , 'Rejected')]");

			if (isStatusPresent == false) {
				fc.utobj().throwsException("was not able verify the status Of Sales Report");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	public String rejectSalesReport(WebDriver driver, Map<String, String> config, String reportID) throws Exception {

		FinanceSalesPage pobj = new FinanceSalesPage(driver);
		WebElement element = driver.findElement(By.xpath(".//*[contains(text(),'" + reportID
				+ "')]/ancestor::tr[@class='tb_data']//input[@name='selectedItem']"));
		fc.utobj().clickElement(driver, element);

		fc.utobj().clickElement(driver, pobj.rejectBtn);

		return reportID;
	}

	@Test(groups = { "finance" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "TC_FC_Enter_Sales_Report_079", testCaseDescription = "Verify Enter Multiple Sales Report")
	public void enterMultipleSalesReport() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		Map<String, String> dataSet = fc.utobj().readTestData("finance", testCaseId);
		String regionName = dataSet.get("region");
		regionName = fc.utobj().generateTestData(regionName);

		String agrmntVrsnName = dataSet.get("agreementVersionName");
		agrmntVrsnName = fc.utobj().generateTestData(agrmntVrsnName);
		String royaltyPrcntg = dataSet.get("royaltyPercentage");
		String advPercentage = dataSet.get("advertisementPercentage");
		String royaltyAreaFranchiseValue = dataSet.get("RoyaltyAreaFranchise");
		String advAreaFranchiseValue = dataSet.get("AdvAreaFranchise");
		String franchiseId = dataSet.get("franchiseName");
		franchiseId = fc.utobj().generateTestData(franchiseId);
		String cntrName = dataSet.get("centerName");
		cntrName = fc.utobj().generateTestData(cntrName);
		String loginID = dataSet.get("ownerLoginID");
		loginID = fc.utobj().generateTestData(loginID);
		String reportPrd1 = dataSet.get("reportPeriod1");
		String reportPrd2 = dataSet.get("reportPeriod2");
		String srvcQuantity1 = dataSet.get("serviceQuantity1");
		String srvcAmount1 = dataSet.get("serviceAmount1");
		String srvcQuantity2 = dataSet.get("serviceQuantity2");
		String srvcAmount2 = dataSet.get("serviceAmount2");

		try {
			driver = fc.loginpage().login(driver);

			AdminFinanceAgreementVersionsPageTest agreementforSales = new AdminFinanceAgreementVersionsPageTest();
			String agreementVersion = agreementforSales.addAgreement(driver, config, agrmntVrsnName, 
					 royaltyPrcntg, advPercentage, royaltyAreaFranchiseValue, advAreaFranchiseValue);
			fc.utobj().printTestStep("Agreement For Sales");

			AdminFranchiseLocationAddNewFranchiseLocationPageTest franchiseForSales = new AdminFranchiseLocationAddNewFranchiseLocationPageTest();
			String franchiseID = franchiseForSales.addFranchiseLocation(driver, franchiseId, agreementVersion, config);
			fc.utobj().printTestStep("Add new Franchise Location");

			fc.utobj().printTestStep("Enter Sales For Multiple Report Periods");
			fc.utobj().printTestStep("Verify The Sales Report");
			enterSalesForMultipleReportPeriods(driver, config, franchiseID, reportPrd1, reportPrd2, srvcQuantity1,
					srvcAmount1, srvcQuantity2, srvcAmount2);

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	public String enterSalesForMultipleReportPeriods(WebDriver driver, Map<String, String> config, String franchiseID,
			String reportPrd1, String reportPrd2, String srvcQuantity1, String srvcQuantity2, String srvcAmount1,
			String srvcAmount2) throws Exception {

		FinanceSalesPage pobj = new FinanceSalesPage(driver);
		fc.finance().finance_common().financeSalesPage(driver);
		fc.utobj().clickElement(driver, pobj.enterSalesReport);
		fc.utobj().selectDropDown(driver, pobj.selectFranchise, franchiseID);

		fc.utobj().selectDropDown(driver, pobj.selectReportPeriod, reportPrd1);

		fc.utobj().clickElement(driver, pobj.ContinueBtn);

		fc.utobj().sendKeys(driver, pobj.serviceQuantity, srvcQuantity1);
		fc.utobj().sendKeys(driver, pobj.serviceAmount, srvcAmount1);

		fc.utobj().sendKeys(driver, pobj.rcvdDate, fc.utobj().getFutureDateUSFormat(-1));

		fc.utobj().sendKeys(driver, pobj.salesComment, "Automation Sales");
		fc.utobj().clickElement(driver, pobj.SaveBtn);
		fc.utobj().clickElement(driver, pobj.acknowledgement);
		fc.utobj().clickElement(driver, pobj.SaveBtn);
		String salesReportID1 = driver.findElement(By.xpath("//tr[td[contains(text(), 'Report ID')]]/td[4]")).getText();
		fc.utobj().clickElement(driver, pobj.okBtn);

		boolean isTextPresent = fc.utobj().assertLinkText(driver, salesReportID1);

		if (isTextPresent == false) {

			fc.utobj().selectDropDownByVisibleText(driver, pobj.selectViewPerPage, "500");
			{

				boolean isTextPresent1 = fc.utobj().assertLinkText(driver, salesReportID1);

				if (isTextPresent1 == false) {

					fc.utobj().throwsException("Sales for First Report Period is Not added!");
				}
			}
		}

		fc.utobj().clickElement(driver, pobj.enterSalesReport);

		fc.utobj().selectDropDown(driver, pobj.selectFranchise, franchiseID);

		fc.utobj().selectDropDown(driver, pobj.selectReportPeriod, reportPrd2);

		fc.utobj().clickElement(driver, pobj.ContinueBtn);

		fc.utobj().sendKeys(driver, pobj.rcvdDate, fc.utobj().getFutureDateUSFormat(-1));

		fc.utobj().sendKeys(driver, pobj.serviceQuantity, srvcQuantity2);

		fc.utobj().sendKeys(driver, pobj.serviceAmount, srvcAmount2);

		fc.utobj().sendKeys(driver, pobj.salesComment, "Automation Sales");

		fc.utobj().clickElement(driver, pobj.SaveBtn);

		fc.utobj().clickElement(driver, pobj.acknowledgement);

		fc.utobj().clickElement(driver, pobj.SaveBtn);

		String salesReportID2 = driver.findElement(By.xpath("//tr[td[contains(text(), 'Report ID')]]/td[4]")).getText();

		fc.utobj().clickElement(driver, pobj.okBtn);

		boolean isTextPresent2 = fc.utobj().assertLinkText(driver, salesReportID2);

		if (isTextPresent2 == false) {

			fc.utobj().selectDropDownByVisibleText(driver, pobj.selectViewPerPage, "500");
			{

				boolean isTextPresent3 = fc.utobj().assertLinkText(driver, salesReportID2);

				if (isTextPresent3 == false) {

					fc.utobj().throwsException("Sales for Second Report Period is Not added!");
				}
			}
		}

		return salesReportID1;

	}

	public String enterAdjustmentViaActionMenu(WebDriver driver, Map<String, String> config, String reportID,
			String srvcQuantity3, String srvcAmount3) throws Exception {
		FinanceSalesPage pobj = new FinanceSalesPage(driver);
		WebElement element = driver.findElement(By.xpath(".//*[contains(text(),'" + reportID
				+ "')]/ancestor::tr[@class='tb_data']//input[@name='selectedItem']"));
		element.click();

		String actionToTake = "Enter Adjustment";
		WebElement element3 = driver.findElement(By.xpath(
				".//*[contains(text(),'" + reportID + "')]/ancestor::tr[@class='tb_data']//div[@id='menuBar']/layer"));
		fc.utobj().moveToElement(driver, element3);
		String layerId = element3.getAttribute("id");
		WebElement element1 = driver.findElement(By.xpath(".//*[contains(text(),'" + reportID
				+ "')]/ancestor::tr[@class='tb_data']//div[@id='menuBar']/layer/a"));
		element1.click();
		layerId = layerId.replace("Actions_dynamicmenu", "");
		layerId = layerId.replace("Bar", "");
		WebElement element2 = driver.findElement(By.xpath(
				".//*[@id='Actions_dynamicmenu" + layerId + "Menu']/span[contains(text(),'" + actionToTake + "')]"));
		fc.utobj().clickElement(driver, element2);

		fc.utobj().sendKeys(driver, pobj.serviceQuantity, srvcQuantity3);
		fc.utobj().sendKeys(driver, pobj.serviceAmount, srvcAmount3);

		fc.utobj().sendKeys(driver, pobj.rcvdDate, fc.utobj().getFutureDateUSFormat(-1));

		fc.utobj().sendKeys(driver, pobj.salesComment, "Adjustment Sales");
		fc.utobj().clickElement(driver, pobj.SaveBtn);
		fc.utobj().clickElement(driver, pobj.acknowledgement);
		fc.utobj().clickElement(driver, pobj.SaveBtn);
		reportID = driver.findElement(By.xpath("//tr[td[contains(text(), 'Report ID')]]/td[4]")).getText();
		fc.utobj().clickElement(driver, pobj.okBtn);

		return reportID;
	}

	@Test(groups = { "finance" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "TC_FC_Approve_Sales_forAdjustment_086", testCaseDescription = "Verify Approve Sales for Adjustment")
	public void ApproveSalesforAdjustment() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		Map<String, String> dataSet = fc.utobj().readTestData("finance", testCaseId);

		String agrmntVrsnName = fc.utobj().generateTestData(dataSet.get("agreementVersionName"));
		String royaltyPrcntg = dataSet.get("royaltyPercentage");
		String advPercentage = dataSet.get("advertisementPercentage");
		String royaltyAreaFranchiseValue = dataSet.get("RoyaltyAreaFranchise");
		String advAreaFranchiseValue = dataSet.get("AdvAreaFranchise");
		
		try {
			driver = fc.loginpage().login(driver);
			FinanceSalesPage pobj = new FinanceSalesPage(driver);

			fc.utobj().printTestStep("Navigate To Admin > Finance > Agreement Versions");
			AdminFinanceAgreementVersionsPageTest agreementforSales = new AdminFinanceAgreementVersionsPageTest();

			fc.utobj().printTestStep("Add Agreement");
			String agreementVersion = agreementforSales.addAgreement(driver, config, agrmntVrsnName, 
					 royaltyPrcntg, advPercentage, royaltyAreaFranchiseValue, advAreaFranchiseValue);

			fc.utobj().printTestStep("Navigate To Admin > Finance >  Configure Finance Document(s) to be Uploaded");
			fc.utobj().printTestStep("Add Document Name");
			String documentName = fc.utobj().generateTestData("TestDoc");
			new AdminFinanceConfigureFinanceDocumentUploadedPageTest().addDocumentName(driver, documentName);

			fc.utobj().printTestStep("Navigate To Admin > Franchise Location > Add Franchise Location");
			fc.utobj().printTestStep("Add Franchise Location");

			String regionName = fc.utobj().generateTestData("Test");
			String storeType = fc.utobj().generateTestData("Test");
			String franchiseId = fc.utobj().generateTestData("Test");
			AdminFranchiseLocationAddFranchiseLocationPageTest franchise = new AdminFranchiseLocationAddFranchiseLocationPageTest();
			franchiseId = franchise.addFranchiseLocation_All_Agreement(driver, franchiseId, regionName, storeType,
					agreementVersion, config);

			fc.utobj().printTestStep("Navigate To Admin > Finance > Finance Setup Preferences");
			fc.utobj().printTestStep("Setup Medium of submission of Sales Reports");
			String salesReportType = "webForm";
			new AdminFinanceFinanceSetupPreferencesPageTest().mediumOfSubmissionOfSalesReports(driver, config,
					salesReportType);

			fc.utobj().printTestStep("Set Invoice Frequency");
			new AdminFinanceFinanceSetupPreferencesPageTest().setInvoice(driver, config);

			fc.utobj().printTestStep("Allow Adjustments after invoicing");
			String allowAdj ="Yes";
			new AdminFinanceFinanceSetupPreferencesPageTest().allowAdjustmentsAfterInvoicing(driver, config, allowAdj);

			fc.utobj().printTestStep("Navigate To Finance > Sales");
			fc.utobj().printTestStep("Enter Sales Report");
			String categoryQuantity = "110";
			String categoryAmount = "6000";
			String reportId = enterSalesReport(driver, franchiseId, categoryQuantity, categoryAmount);

			fc.utobj().printTestStep("Approve Sales Report");
			
			String reportID = approveSalesReport(driver, config, reportId);

			fc.utobj().printTestStep("Generate Invoice");
			
			generateInvoice(driver, config, reportID, franchiseId);

			fc.utobj().clickElement(driver, pobj.salesReport);

			fc.utobj().printTestStep("Enter Adjustment Via Action Menu");
			
			categoryQuantity = "1100";
			categoryAmount = "600";
			reportID = enterAdjustmentViaActionMenu(driver, config, reportID, categoryQuantity,
					categoryAmount);

			fc.utobj().printTestStep("Approve Sales Report");
		
			reportID = approveSalesReport(driver, config, reportID);
			fc.utobj().printTestStep("Verify The Approve Sales Report");

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "finance" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "TC_FC_Generate_Invoice_forAdjustment_087", testCaseDescription = "Verify generate Invoice for Adjustment")
	public void generateInvoiceforAdjustment() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		Map<String, String> dataSet = fc.utobj().readTestData("finance", testCaseId);

		String agrmntVrsnName = fc.utobj().generateTestData(dataSet.get("agreementVersionName"));
		String royaltyPrcntg = dataSet.get("royaltyPercentage");
		String advPercentage = dataSet.get("advertisementPercentage");
		String royaltyAreaFranchiseValue = dataSet.get("RoyaltyAreaFranchise");
		String advAreaFranchiseValue = dataSet.get("AdvAreaFranchise");
		
		try {
			driver = fc.loginpage().login(driver);

			FinanceSalesPage pobj = new FinanceSalesPage(driver);

			fc.utobj().printTestStep("Navigate To Admin > Finance > Agreement Versions");
			AdminFinanceAgreementVersionsPageTest agreementforSales = new AdminFinanceAgreementVersionsPageTest();

			fc.utobj().printTestStep("Add Agreement");
			String agreementVersion = agreementforSales.addAgreement(driver, config, agrmntVrsnName, 
					 royaltyPrcntg, advPercentage, royaltyAreaFranchiseValue, advAreaFranchiseValue);

			fc.utobj().printTestStep("Navigate To Admin > Finance >  Configure Finance Document(s) to be Uploaded");
			fc.utobj().printTestStep("Add Document Name");
			String documentName = fc.utobj().generateTestData("TestDoc");
			new AdminFinanceConfigureFinanceDocumentUploadedPageTest().addDocumentName(driver, documentName);

			fc.utobj().printTestStep("Navigate To Admin > Franchise Location > Add Franchise Location");
			fc.utobj().printTestStep("Add Franchise Location");

			String regionName = fc.utobj().generateTestData("Test");
			String storeType = fc.utobj().generateTestData("Test");
			String franchiseId = fc.utobj().generateTestData("Test");
			AdminFranchiseLocationAddFranchiseLocationPageTest franchise = new AdminFranchiseLocationAddFranchiseLocationPageTest();
			franchiseId = franchise.addFranchiseLocation_All_Agreement(driver, franchiseId, regionName, storeType,
					agreementVersion, config);

			fc.utobj().printTestStep("Navigate To Admin > Finance > Finance Setup Preferences");
			fc.utobj().printTestStep("Setup Medium of submission of Sales Reports");
			String salesReportType = "webForm";
			new AdminFinanceFinanceSetupPreferencesPageTest().mediumOfSubmissionOfSalesReports(driver, config,
					salesReportType);

			fc.utobj().printTestStep("Set Invoice Frequency");
			new AdminFinanceFinanceSetupPreferencesPageTest().setInvoice(driver, config);

			fc.utobj().printTestStep("Allow Adjustments after invoicing");
			String allowAdj ="Yes";
			new AdminFinanceFinanceSetupPreferencesPageTest().allowAdjustmentsAfterInvoicing(driver, config, allowAdj);

			fc.utobj().printTestStep("Navigate To Finance > Sales");
			fc.utobj().printTestStep("Enter Sales Report");
			String categoryQuantity = "110";
			String categoryAmount = "6000";
			String reportId = enterSalesReport(driver, franchiseId, categoryQuantity, categoryAmount);

			fc.utobj().printTestStep("Filter Sales Report By Franchise Id");
			filterSalesReport(driver, franchiseId);

			String reportID = approveSalesReport(driver, config, reportId);
			
			generateInvoice(driver, config, reportID, franchiseId);
			
			fc.utobj().clickElement(driver, pobj.salesReport);

			fc.utobj().printTestStep("Enter Adjustment By Action Menu");
			
			categoryQuantity = "110001";
			categoryAmount = "60000";
			
			reportID = enterAdjustmentViaActionMenu(driver, config, reportID, categoryQuantity,
					categoryAmount);

			reportID = approveSalesReport(driver, config, reportID);

			fc.utobj().printTestStep("Verify The Invoice Id");
			
			generateInvoice(driver, config, reportID, franchiseId);

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "finance" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "TC_FC_Apply_Payment_forAdjustment_088", testCaseDescription = "Verify apply Payment for Adjustment")
	public void applyPaymentforAdjustment() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		Map<String, String> dataSet = fc.utobj().readTestData("finance", testCaseId);

		String agrmntVrsnName = fc.utobj().generateTestData(dataSet.get("agreementVersionName"));
		String royaltyPrcntg = dataSet.get("royaltyPercentage");
		String advPercentage = dataSet.get("advertisementPercentage");
		String royaltyAreaFranchiseValue = dataSet.get("RoyaltyAreaFranchise");
		String advAreaFranchiseValue = dataSet.get("AdvAreaFranchise");
		
		try {
			driver = fc.loginpage().login(driver);

			FinanceSalesPage pobj = new FinanceSalesPage(driver);

			fc.utobj().printTestStep("Navigate To Admin > Finance > Agreement Versions");
			AdminFinanceAgreementVersionsPageTest agreementforSales = new AdminFinanceAgreementVersionsPageTest();

			fc.utobj().printTestStep("Add Agreement");
			String agreementVersion = agreementforSales.addAgreement(driver, config, agrmntVrsnName, 
					 royaltyPrcntg, advPercentage, royaltyAreaFranchiseValue, advAreaFranchiseValue);

			fc.utobj().printTestStep("Navigate To Admin > Franchise Location > Add Franchise Location");
			fc.utobj().printTestStep("Add Franchise Location");

			
			String franchiseId = "GT";
			AdminFranchiseLocationAddNewFranchiseLocationPageTest franchise = new AdminFranchiseLocationAddNewFranchiseLocationPageTest();
			franchiseId = franchise.addFranchiseLocation(driver, franchiseId, agreementVersion, config);
			
			fc.utobj().printTestStep("Navigate To Admin > Finance > Finance Setup Preferences");
			fc.utobj().printTestStep("Setup Medium of submission of Sales Reports");
			String salesReportType = "webForm";
			new AdminFinanceFinanceSetupPreferencesPageTest().mediumOfSubmissionOfSalesReports(driver, config,
					salesReportType);

			fc.utobj().printTestStep("Set Invoice Frequency");
			new AdminFinanceFinanceSetupPreferencesPageTest().setInvoice(driver, config);

			fc.utobj().printTestStep("Allow Adjustments after invoicing");
			String allowAdj ="Yes";
			new AdminFinanceFinanceSetupPreferencesPageTest().allowAdjustmentsAfterInvoicing(driver, config, allowAdj);

			fc.utobj().printTestStep("Navigate To Finance > Sales");
			fc.utobj().printTestStep("Enter Sales Report");
			String categoryQuantity = "110";
			String categoryAmount = "6000";
			String reportId = enterSalesReport(driver, franchiseId, categoryQuantity, categoryAmount);

			fc.utobj().printTestStep("Filter Sales Report By Franchise Id");
			filterSalesReport(driver, franchiseId);

			fc.utobj().printTestStep("Approve Sales Report");
			fc.utobj().actionImgOption(driver, reportId, "Approve");

			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().sendKeys(driver, pobj.commentTxtArea,
					"Test Approve Comments" + fc.utobj().generateRandomNumber());
			fc.utobj().clickElement(driver, pobj.SaveBtn);

			fc.utobj().clickElement(driver, pobj.cloaseBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Filter Sales Report By Franchise Id");
			filterSalesReport(driver, franchiseId);

			fc.utobj().printTestStep("Generate Invoice");
			fc.utobj().actionImgOption(driver, franchiseId, "Generate Invoice");

			fc.utobj().printTestStep("Enter Adjustment");
			fc.utobj().actionImgOption(driver, franchiseId, "Enter Adjustment");

			categoryQuantity = "200";
			categoryAmount = "400";
			fc.utobj().sendKeys(driver,
					driver.findElement(By
							.xpath(".//td[contains(text () , 'Service')]/following-sibling::td//input[contains(@name , 'quantity')]")),
					categoryQuantity);
			fc.utobj().sendKeys(driver,
					driver.findElement(By
							.xpath(".//td[contains(text () , 'Service')]/following-sibling::td//input[contains(@name , 'amount')]")),
					categoryAmount);

			fc.utobj().sendKeys(driver, pobj.rcvdDate, fc.utobj().getFutureDateUSFormat(-1));

			fc.utobj().clickElement(driver, pobj.SaveBtn);

			if (!fc.utobj().isSelected(driver,pobj.acknowledgement)) {
				fc.utobj().clickElement(driver, pobj.acknowledgement);
			}
			fc.utobj().clickElement(driver, pobj.SaveBtn);
			reportId = fc.utobj().getText(driver, pobj.reportId);

			fc.utobj().clickElement(driver, pobj.okayBtn);

			fc.utobj().printTestStep("Approve Adjustment Sales Report");
			fc.utobj().actionImgOption(driver, reportId, "Approve");
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().sendKeys(driver, pobj.commentTxtArea,
					"Test Approve Comments" + fc.utobj().generateRandomNumber());
			fc.utobj().clickElement(driver, pobj.SaveBtn);

			fc.utobj().clickElement(driver, pobj.cloaseBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Filter Sales Report By Franchise Id");
			filterSalesReport(driver, franchiseId);

			fc.utobj().printTestStep("Generate Invoice");
			
			String invoiceID = generateInvoice(driver, config, reportId, franchiseId);

			fc.utobj().printTestStep("Apply Payment By Royalties Action Menu");
			
			FinanceRoyaltiesPageTest paymentAdj = new FinanceRoyaltiesPageTest();
			
			String paymentAmount = dataSet.get("payAmount");
			
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = "finance1111")
	@TestCase(createdOn = "2018-05-29", updatedOn = "2018-05-30", testCaseId = "TC_Admin_FIN_Setup_EnterSales", testCaseDescription = "To Verify mandatory to fill previous reports in Sales Report Page")
	public void mandatoryToFillPreviousReportsForSalesReport() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());
		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("finance", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		String regionName = fc.utobj().generateTestData("GT");
		String storeType = fc.utobj().generateTestData("GT");
		String franchiseId = fc.utobj().generateTestData("GT");
		String agrmntVrsnName = fc.utobj().generateTestData(dataSet.get("agreementVersionName"));
		String royaltyPrcntg = dataSet.get("royaltyPercentage");
		String advPercentage = dataSet.get("advertisementPercentage");
		String royaltyAreaFranchiseValue = dataSet.get("RoyaltyAreaFranchise");
		String advAreaFranchiseValue = dataSet.get("AdvAreaFranchise");
		
		
		String mandToFillPreRep = "No";
		
		try {
			driver = fc.loginpage().login(driver);
			
			FinanceSalesPage pobj = new FinanceSalesPage(driver);
			
			fc.utobj().printTestStep("Navigate To Admin > Finance > Sales Report Preferences > Mandatory to Fill Previous Reports");

			AdminFinanceFinanceSetupPreferencesPageTest setup = new AdminFinanceFinanceSetupPreferencesPageTest();
			setup.mandatoryToFillPreviousReports(driver, config, mandToFillPreRep);

			fc.utobj().printTestStep("Navigate To Admin > Finance > Agreement Versions");
			AdminFinanceAgreementVersionsPageTest agreementforSales = new AdminFinanceAgreementVersionsPageTest();

			fc.utobj().printTestStep("Add Agreement");
			String agreementVersion = agreementforSales.addAgreement(driver, config, agrmntVrsnName, 
					 royaltyPrcntg, advPercentage, royaltyAreaFranchiseValue, advAreaFranchiseValue);
			
			AdminFranchiseLocationAddFranchiseLocationPageTest franchise = new AdminFranchiseLocationAddFranchiseLocationPageTest();
			franchiseId = franchise.addFranchiseLocation_All_Agreement(driver, franchiseId, regionName, storeType,
					agreementVersion, config);
			
			fc.utobj().printTestStep("Navigate To Admin > Finance > Finance Setup Preferences");
			fc.utobj().printTestStep("Setup Medium of submission of Sales Reports");
			String salesReportType = "webForm";
			
			new AdminFinanceFinanceSetupPreferencesPageTest().mediumOfSubmissionOfSalesReports(driver, config,
					salesReportType);
			
			fc.utobj().printTestStep("Navigate To Finance > Sales");
			
			fc.finance().finance_common().financeSalesPage(driver);

			fc.utobj().printTestStep("Enter Sales Report");
			
			fc.utobj().clickElement(driver, pobj.enterSalesReport);
			
			fc.utobj().selectDropDown(driver, pobj.selectFranchise, franchiseId);
			
			fc.utobj().clickElement(driver, pobj.ContinueBtn);
			
			boolean reportPeriod = fc.utobj().assertPageSource(driver, "May 2017");
			
			if(reportPeriod == false) {
				
				fc.utobj().throwsException("Report Period is not selected By Default as per functionality");
			}
						
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}
	
	@Test(groups = "finance")
	@TestCase(createdOn = "2018-05-30", updatedOn = "2018-05-30", testCaseId = "TC_Admin_Fin_Set_First_DayOfWeek", 
			testCaseDescription = "To Verify preference locked alert for Set First Day of Week if Sales Report is already submitted")
	public void checkPreferencesLockedforFirstDayOfWeek() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());
		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("finance", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		String regionName = fc.utobj().generateTestData("GT");
		String storeType = fc.utobj().generateTestData("GT");
		String franchiseId = fc.utobj().generateTestData("GT");
		String agrmntVrsnName = fc.utobj().generateTestData("AV");
		String royaltyPrcntg = "10";
		String advPercentage = "8";
		String royaltyAreaFranchiseValue = "5";
		String advAreaFranchiseValue = "2.5";
		String categoryQuantity ="100", categoryAmount="500", mandToFillPreRep="No";
		
		try {
			driver = fc.loginpage().login(driver);
			
			FinanceSalesPage pobj = new FinanceSalesPage(driver);
			
			AdminFinanceFinanceSetupPreferencesPageTest setup = new AdminFinanceFinanceSetupPreferencesPageTest();

			fc.utobj().printTestStep("Navigate To Admin > Finance > Agreement Versions");
			AdminFinanceAgreementVersionsPageTest agreementforSales = new AdminFinanceAgreementVersionsPageTest();
			
			fc.utobj().printTestStep("Add Agreement");
			String agreementVersion = agreementforSales.addAgreement(driver, config, agrmntVrsnName, 
					 royaltyPrcntg, advPercentage, royaltyAreaFranchiseValue, advAreaFranchiseValue);
			
			AdminFranchiseLocationAddFranchiseLocationPageTest franchise = new AdminFranchiseLocationAddFranchiseLocationPageTest();
			franchiseId = franchise.addFranchiseLocation_All_Agreement(driver, franchiseId, regionName, storeType,
					agreementVersion, config);
			
			fc.utobj().printTestStep("Navigate To Admin > Finance > Finance Setup Preferences");
			
			fc.utobj().printTestStep("Setup Medium of submission of Sales Reports");
			
			String salesReportType = "webForm";
			
			new AdminFinanceFinanceSetupPreferencesPageTest().mediumOfSubmissionOfSalesReports(driver, config,
					salesReportType);
			
			fc.utobj().printTestStep("Navigate To Finance > Sales");
			
			fc.finance().finance_common().financeSalesPage(driver);

			fc.utobj().printTestStep("Enter Sales Report");
			
			enterSalesReport(driver, franchiseId, categoryQuantity, categoryAmount);
			
			fc.adminpage().openAdminFinanceSetupPreferencesLnk(driver);
			
			AdminFinanceFinanceSetupPreferencesPage setupPobj = new AdminFinanceFinanceSetupPreferencesPage(driver);
			
			fc.utobj().clickElement(driver, setupPobj.editStartFirstDayOfWeek);
			
			boolean prefrenceLocked = fc.utobj().assertPageSource(driver, "This preference is currently locked. Click here for details.");
			
			if (prefrenceLocked == false) {
				
				fc.utobj().throwsException("Preference is not locked for setting up First Day of Week");
				
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}
	
	@Test(groups = "finance_FreshDB")
	@TestCase(createdOn = "2018-06-01", updatedOn = "2018-06-01", testCaseId = "TC_Allow_Adjustment_In_Action_TC-22968", 
			testCaseDescription = "To verify allow adjustment option in action menu if it is disabled from admin")
	public void checkAllowAdjustmentOptionInAction() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());
		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("finance", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		String regionName = fc.utobj().generateTestData("GT");
		String storeType = fc.utobj().generateTestData("GT");
		String franchiseId = fc.utobj().generateTestData("GT");
		String agrmntVrsnName = fc.utobj().generateTestData("AV");
		String royaltyPrcntg = "10";
		String advPercentage = "8";
		String royaltyAreaFranchiseValue = "5";
		String advAreaFranchiseValue = "2.5";
		String categoryQuantity ="100", categoryAmount="500";
		
		try {
			driver = fc.loginpage().login(driver);
			
			FinanceSalesPage pobj = new FinanceSalesPage(driver);
			
			AdminFinanceFinanceSetupPreferencesPageTest setup = new AdminFinanceFinanceSetupPreferencesPageTest();
			
			fc.utobj().printTestStep("Navigate To Admin > Finance > Finance Setup Preferences > Allow Adjustment");
			
			String allowAdj ="No";
			setup.allowAdjustmentsAfterInvoicing(driver, config, allowAdj);
			
			fc.utobj().printTestStep("Navigate To Admin > Finance > Finance Setup Preferences");
			
			fc.utobj().printTestStep("Setup Medium of submission of Sales Reports");
			
			String salesReportType = "webForm";
			
			setup.mediumOfSubmissionOfSalesReports(driver, config, salesReportType);
			
			fc.utobj().printTestStep("Navigate To Admin > Finance > Agreement Versions");
			AdminFinanceAgreementVersionsPageTest agreementforSales = new AdminFinanceAgreementVersionsPageTest();
			
			fc.utobj().printTestStep("Add Agreement");
			String agreementVersion = agreementforSales.addAgreement(driver, config, agrmntVrsnName, 
					 royaltyPrcntg, advPercentage, royaltyAreaFranchiseValue, advAreaFranchiseValue);
			
			AdminFranchiseLocationAddFranchiseLocationPageTest franchise = new AdminFranchiseLocationAddFranchiseLocationPageTest();
			franchiseId = franchise.addFranchiseLocation_All_Agreement(driver, franchiseId, regionName, storeType,
					agreementVersion, config);
			
			fc.utobj().printTestStep("Navigate To Finance > Sales");
			
			fc.finance().finance_common().financeSalesPage(driver);

			fc.utobj().printTestStep("Enter Sales Report");
			
			String reportId = enterSalesReport(driver, franchiseId, categoryQuantity, categoryAmount);
			
			fc.utobj().printTestStep("Filter Sales Report By Franchise Id");
			
			filterSalesReport(driver, franchiseId);

			fc.utobj().printTestStep("Approve Sales Report");
			
			fc.utobj().actionImgOption(driver, reportId, "Approve");

			fc.commonMethods().switch_cboxIframe_frameId(driver);
			
			fc.utobj().sendKeys(driver, pobj.commentTxtArea, "Test Approve Comments" + fc.utobj().generateRandomNumber());
			
			fc.utobj().clickElement(driver, pobj.SaveBtn);

			fc.utobj().clickElement(driver, pobj.cloaseBtn);
			
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Filter Sales Report By Franchise Id");
			
			filterSalesReport(driver, franchiseId);

			fc.utobj().printTestStep("Generate Invoice");
			fc.utobj().actionImgOption(driver, franchiseId, "Generate Invoice");
			
			fc.utobj().printTestStep("Enter Adjustment");
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//*[contains(text () ,'" + franchiseId + "')]/ancestor::tr/td/div[@id='menuBar']/layer/a/img"));
			
			boolean isAdjustmentPresent = fc.utobj().assertLinkText(driver, "Enter Adjustment");
			
			if(isAdjustmentPresent == true) {
				
				fc.utobj().throwsException("Enter Adjustment Option is visible even setting is disabled from admin setup preferences");
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);	
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}
	
	public void filterSalesReport(WebDriver driver, String franchiseId) throws Exception {
		FinanceSalesPage pobj = new FinanceSalesPage(driver);
		fc.utobj().clickElement(driver, pobj.showFilter);
		fc.utobj().setToDefault(driver, pobj.selectFranchiseIdFilter);

		fc.utobj().selectValFromMultiSelect(driver, pobj.selectFranchiseIdFilter, franchiseId);
		fc.utobj().clickElement(driver, pobj.searchBtnFilter);

		fc.utobj().clickElement(driver, pobj.hideFilter);
	}
	
	@Test(groups = "finance_FreshDB")
	@TestCase(createdOn = "2018-06-15", updatedOn = "2018-07-30", testCaseId = "TC_FC_Upload_Sales_Report_XLS", testCaseDescription = "To verify Upload Sales Reort as XLS file")
	public void uploadSalesReportXLS() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());
		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("finance", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		String franchiseId = "001D";
		String agrmntVrsnName = "AV";
		String royaltyPrcntg = "10";
		String advPercentage = "8";
		String royaltyAreaFranchiseValue = "5";
		String advAreaFranchiseValue = "2.5";
		
		try {
			driver = fc.loginpage().login(driver);
			
			FinanceSalesPage pobj = new FinanceSalesPage(driver);
			
			AdminFinanceFinanceSetupPreferencesPageTest setup = new AdminFinanceFinanceSetupPreferencesPageTest();
			
			fc.utobj().printTestStep("Navigate To Admin > Finance > Agreement Versions");
			AdminFinanceAgreementVersionsPageTest agreementforSales = new AdminFinanceAgreementVersionsPageTest();
			
			fc.utobj().printTestStep("Add Agreement");
			String agreementVersion = agreementforSales.addAgreement(driver, config, agrmntVrsnName, 
					 royaltyPrcntg, advPercentage, royaltyAreaFranchiseValue, advAreaFranchiseValue);
			
			AdminFranchiseLocationAddNewFranchiseLocationPageTest franchise = new AdminFranchiseLocationAddNewFranchiseLocationPageTest();
			
			franchiseId = franchise.addFranchiseLocation(driver, franchiseId, agreementVersion, config);
			
			fc.utobj().printTestStep("Setup Medium of submission of Sales Reports");
			
			String salesReportType = "xls";
			
			setup.mediumOfSubmissionOfSalesReports(driver, config, salesReportType);
			
			fc.utobj().printTestStep("Navigate To Finance > Sales");
			
			fc.finance().finance_common().financeSalesPage(driver);

			fc.utobj().printTestStep("Upload Sales Report as XLS");
			
			fc.utobj().clickElement(driver, pobj.uploadSalesReport);

			fc.utobj().sendKeys(driver, pobj.clickBrowseBtnUploadFile, "C:\\Selenium_Test_Input\\testData\\FinSalesData.xls");
						
			String parentWindow = driver.getWindowHandle();
			
			fc.utobj().clickElement(driver, pobj.continueUploadBtn);
			
			fc.utobj().acceptAlertBox(driver);
			
			Set<String> handles =  driver.getWindowHandles();
			
			   for(String windowHandle  : handles)
			       {
			       if(!windowHandle.equals(parentWindow))
			          {
			          driver.switchTo().window(windowHandle);
			         			          
			          fc.utobj().clickElement(driver, pobj.uploadBtn);
			          
			          fc.utobj().clickElement(driver, pobj.okBtn);
			          
			          driver.switchTo().window(parentWindow);
			          }
			       }		

				fc.utobj().printTestStep("Filter Sales Report");

				fc.utobj().clickElement(driver, pobj.showFilter);
				
				fc.utobj().setToDefault(driver, pobj.selectFranchiseIdFilter);
				
				fc.utobj().selectValFromMultiSelect(driver, pobj.selectFranchiseIdFilter, fc.utobj().getElement(driver, pobj.selectFranchiseIdFilter).findElement(By.xpath("./div/div/input")), franchiseId);
				
				fc.utobj().clickElement(driver, pobj.searchBtnFilter);

				fc.utobj().clickElement(driver, pobj.hideFilter);

				boolean isLinkTextPresent = fc.utobj().assertLinkText(driver, franchiseId);

				if (isLinkTextPresent == false) {

					fc.utobj().throwsException("was not able to verify Report At Finance Sales Page");
				}
				fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}
		
	@Test(groups = {"finance_FreshDB", "finance_FailedTC"})
	@TestCase(createdOn = "2018-06-15", updatedOn = "2018-06-20", testCaseId = "TC_FC_Upload_Sales_Report_CSV", testCaseDescription = "To verify Upload Sales Reort as CSV file")
	public void uploadSalesReportCSV() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());
		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("finance", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		String categoryName = "TestCategory";
		String franchiseId = "001C";
		String agrmntVrsnName = "AV";
		String royaltyPrcntg = "10";
		String advPercentage = "8";
		String royaltyAreaFranchiseValue = "5";
		String advAreaFranchiseValue = "2.5";
				
		try {
			driver = fc.loginpage().login(driver);
			
			FinanceSalesPage pobj = new FinanceSalesPage(driver);
			
			AdminFinanceFinanceSetupPreferencesPageTest setup = new AdminFinanceFinanceSetupPreferencesPageTest();
			
			fc.utobj().printTestStep("Navigate To Admin > Finance > Configure Profit & Loss Categories Page");
			fc.utobj().printTestStep("Add Main Category");
			categoryName = new AdminFinanceProfitLossCategoriesPageTest().addNewMainCategory(driver, categoryName);
			
			fc.utobj().printTestStep("Navigate To Admin > Finance > Agreement Versions");
			AdminFinanceAgreementVersionsPageTest agreementforSales = new AdminFinanceAgreementVersionsPageTest();
			
			fc.utobj().printTestStep("Add Agreement");
			String agreementVersion = agreementforSales.addAgreement(driver, config, agrmntVrsnName, 
					 royaltyPrcntg, advPercentage, royaltyAreaFranchiseValue, advAreaFranchiseValue);
			
			AdminFranchiseLocationAddNewFranchiseLocationPageTest franchise = new AdminFranchiseLocationAddNewFranchiseLocationPageTest();
			franchiseId = franchise.addFranchiseLocation(driver, franchiseId, agreementVersion, config);
			
			fc.utobj().printTestStep("Setup Medium of submission of Sales Reports");
			
			String salesReportType = "csv";
			
			setup.mediumOfSubmissionOfSalesReports(driver, config, salesReportType);
			
			fc.utobj().printTestStep("Navigate To Finance > Sales");
			
			fc.finance().finance_common().financeSalesPage(driver);

			fc.utobj().printTestStep("Upload Sales Report as CSV");
			
			fc.utobj().clickElement(driver, pobj.uploadSalesReport);

			fc.utobj().sendKeys(driver, pobj.clickBrowseBtnUploadFile, "C:\\Selenium_Test_Input\\testData\\FinSalesData.csv");
			
			fc.utobj().clickElement(driver, pobj.skipDataMapping);
			
			String parentWindow = driver.getWindowHandle();
			
			fc.utobj().clickElement(driver, pobj.continueUploadBtn);
			
			fc.utobj().acceptAlertBox(driver);
					
			Set<String> handles =  driver.getWindowHandles();
			
			   for(String windowHandle  : handles)
			       {
			       if(!windowHandle.equals(parentWindow))
			          {
			          driver.switchTo().window(windowHandle);
			          
			          fc.utobj().clickElement(driver, pobj.fileUploadBtn);
			         
			          fc.utobj().clickElement(driver, pobj.OKBtn);
			         
			         driver.switchTo().window(parentWindow);
			          }
			       }		
		

				fc.utobj().printTestStep("Filter Sales Report");

				fc.utobj().clickElement(driver, pobj.showFilter);
				
				fc.utobj().setToDefault(driver, pobj.selectFranchiseIdFilter);
				
				fc.utobj().selectValFromMultiSelect(driver, pobj.selectFranchiseIdFilter, fc.utobj()
						.getElement(driver, pobj.selectFranchiseIdFilter).findElement(By.xpath("./div/div/input")),
						franchiseId);
				
				fc.utobj().clickElement(driver, pobj.searchBtnFilter);

				fc.utobj().clickElement(driver, pobj.hideFilter);

				boolean isLinkTextPresent = fc.utobj().assertLinkText(driver, franchiseId);

				if (isLinkTextPresent == false) {

					fc.utobj().throwsException("was not able to verify uploaded sales Report At Finance Sales Page");
				}
	
				fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}
	
		
	@Test(groups = "finance_FreshDB")
	@TestCase(createdOn = "2018-06-15", updatedOn = "2018-06-20", testCaseId = "TC_FC_Upload_Sales_Report_Blank_CSV", testCaseDescription = "To verify Upload Sales Reort as Blank CSV file")
	public void uploadSalesReportBlankCSV() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());
		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("finance", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		
		String franchiseId = "BlankCSV";
		String agrmntVrsnName = "AV";
		String royaltyPrcntg = "10";
		String advPercentage = "8";
		String royaltyAreaFranchiseValue = "5";
		String advAreaFranchiseValue = "2.5";
		
		try {
			driver = fc.loginpage().login(driver);
			
			FinanceSalesPage pobj = new FinanceSalesPage(driver);
			
			AdminFinanceFinanceSetupPreferencesPageTest setup = new AdminFinanceFinanceSetupPreferencesPageTest();
			
			fc.utobj().printTestStep("Navigate To Admin > Finance > Agreement Versions");
			AdminFinanceAgreementVersionsPageTest agreementforSales = new AdminFinanceAgreementVersionsPageTest();
			
			fc.utobj().printTestStep("Add Agreement");
			String agreementVersion = agreementforSales.addAgreement(driver, config, agrmntVrsnName, 
					 royaltyPrcntg, advPercentage, royaltyAreaFranchiseValue, advAreaFranchiseValue);
			
			AdminFranchiseLocationAddNewFranchiseLocationPageTest franchise = new AdminFranchiseLocationAddNewFranchiseLocationPageTest();
			
			franchiseId = franchise.addFranchiseLocation(driver, franchiseId, agreementVersion, config);
						
			fc.utobj().printTestStep("Setup Medium of submission of Sales Reports");
			
			String salesReportType = "csv";
				
				setup.mediumOfSubmissionOfSalesReports(driver, config, salesReportType);
				
				fc.utobj().printTestStep("Navigate To Finance > Sales");
				
				fc.finance().finance_common().financeSalesPage(driver);

				fc.utobj().printTestStep("Upload Sales Report as CSV");
				
				fc.utobj().clickElement(driver, pobj.uploadSalesReport);

				fc.utobj().sendKeys(driver, pobj.clickBrowseBtnUploadFile, "C:\\Selenium_Test_Input\\testData\\BlankFinSalesData.csv");
				
				fc.utobj().clickElement(driver, pobj.skipDataMapping);
				
				String parentWindow1 = driver.getWindowHandle();
				
				fc.utobj().clickElement(driver, pobj.continueUploadBtn);
				
				fc.utobj().acceptAlertBox(driver);
						
				Set<String> handles1 =  driver.getWindowHandles();
				
				   for(String windowHandle1  : handles1)
				       {
				       if(!windowHandle1.equals(parentWindow1))
				          {
				          driver.switchTo().window(windowHandle1);
				          
				          fc.utobj().clickElement(driver, pobj.fileUploadBtn);
				         
				          fc.utobj().clickElement(driver, pobj.OKBtn);
				         
				         driver.switchTo().window(parentWindow1);
				          }
				       }		
			
					fc.utobj().printTestStep("Filter Sales Report");

					fc.utobj().clickElement(driver, pobj.showFilter);
					
					fc.utobj().setToDefault(driver, pobj.selectFranchiseIdFilter);
					
					fc.utobj().selectValFromMultiSelect(driver, pobj.selectFranchiseIdFilter, fc.utobj()
							.getElement(driver, pobj.selectFranchiseIdFilter).findElement(By.xpath("./div/div/input")),
							franchiseId);
					
					fc.utobj().clickElement(driver, pobj.searchBtnFilter);

					fc.utobj().clickElement(driver, pobj.hideFilter);

					boolean isLinkTextPresent1 = fc.utobj().assertLinkText(driver, franchiseId);

					if (isLinkTextPresent1 == false) {

						fc.utobj().throwsException("was not able to verify uploaded sales Report At Finance Sales Page");
					}
	
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
				
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}
	
	
	@Test(groups = {"finance_FreshDB","finance_FailedTC"})
	@TestCase(createdOn = "2018-06-15", updatedOn = "2018-06-20", testCaseId = "TC_FC_Upload_Sales_Report_Blank_XLS", testCaseDescription = "To verify Upload Sales Reort as XLS file")
	public void uploadSalesReportBlankXLS() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());
		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("finance", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		
		String franchiseId = "BlankXLS";
		String agrmntVrsnName = "AV";
		String royaltyPrcntg = "10";
		String advPercentage = "8";
		String royaltyAreaFranchiseValue = "5";
		String advAreaFranchiseValue = "2.5";
		
		try {
			driver = fc.loginpage().login(driver);
			
			FinanceSalesPage pobj = new FinanceSalesPage(driver);
			
			AdminFinanceFinanceSetupPreferencesPageTest setup = new AdminFinanceFinanceSetupPreferencesPageTest();
			
			fc.utobj().printTestStep("Navigate To Admin > Finance > Agreement Versions");
			AdminFinanceAgreementVersionsPageTest agreementforSales = new AdminFinanceAgreementVersionsPageTest();
			
			fc.utobj().printTestStep("Add Agreement");
			String agreementVersion = agreementforSales.addAgreement(driver, config, agrmntVrsnName, 
					 royaltyPrcntg, advPercentage, royaltyAreaFranchiseValue, advAreaFranchiseValue);
			
			AdminFranchiseLocationAddNewFranchiseLocationPageTest franchise = new AdminFranchiseLocationAddNewFranchiseLocationPageTest();
			
			franchiseId = franchise.addFranchiseLocation(driver, franchiseId, agreementVersion, config);
						
			fc.utobj().printTestStep("Setup Medium of submission of Sales Reports");
			
			String salesReportType = "xls";
			
			setup.mediumOfSubmissionOfSalesReports(driver, config, salesReportType);
			
			fc.utobj().printTestStep("Navigate To Finance > Sales");
			
			fc.finance().finance_common().financeSalesPage(driver);

			fc.utobj().printTestStep("Upload Sales Report as XLS");
			
			fc.utobj().clickElement(driver, pobj.uploadSalesReport);

			fc.utobj().sendKeys(driver, pobj.clickBrowseBtnUploadFile, "C:\\Selenium_Test_Input\\testData\\BlankFinSalesData.xls");
						
			String parentWindow = driver.getWindowHandle();
			
			fc.utobj().clickElement(driver, pobj.continueUploadBtn);
			
			fc.utobj().acceptAlertBox(driver);
					
			Set<String> handles =  driver.getWindowHandles();
			
			   for(String windowHandle  : handles)
			       {
			       if(!windowHandle.equals(parentWindow))
			          {
			          driver.switchTo().window(windowHandle);
			         			          
			          fc.utobj().clickElement(driver, pobj.uploadBtn);
			          
			          fc.utobj().clickElement(driver, pobj.okBtn);
			          
			          driver.switchTo().window(parentWindow);
			          }
			       }		

				fc.utobj().printTestStep("Filter Sales Report");

				fc.utobj().clickElement(driver, pobj.showFilter);
				
				fc.utobj().setToDefault(driver, pobj.selectFranchiseIdFilter);
				
				fc.utobj().selectValFromMultiSelect(driver, pobj.selectFranchiseIdFilter, fc.utobj()
						.getElement(driver, pobj.selectFranchiseIdFilter).findElement(By.xpath("./div/div/input")),
						franchiseId);
				
				fc.utobj().clickElement(driver, pobj.searchBtnFilter);

				fc.utobj().clickElement(driver, pobj.hideFilter);

				boolean isLinkTextPresent = fc.utobj().assertLinkText(driver, franchiseId);

				if (isLinkTextPresent == false) {

					fc.utobj().throwsException("was not able to verify Report At Finance Sales Page");
				}
					
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
				
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}
			
	@Test(groups = "finance_FreshDB")
	@TestCase(createdOn = "2018-06-15", updatedOn = "2018-06-15", testCaseId = "TC_FC_Alert_Upload_Sales_Report_XLS", testCaseDescription = "To verify alert for Upload Sales Reort as XLS file if file format is wrong")
	public void verifyAlertForUploadSalesReportXLS() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());
		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("finance", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		String franchiseId = "001A";
		String agrmntVrsnName = "AV";
		String royaltyPrcntg = "10";
		String advPercentage = "8";
		String royaltyAreaFranchiseValue = "5";
		String advAreaFranchiseValue = "2.5";
		
		try {
			driver = fc.loginpage().login(driver);
			
			FinanceSalesPage pobj = new FinanceSalesPage(driver);
			
			AdminFinanceFinanceSetupPreferencesPageTest setup = new AdminFinanceFinanceSetupPreferencesPageTest();
			
			fc.utobj().printTestStep("Navigate To Admin > Finance > Agreement Versions");
			AdminFinanceAgreementVersionsPageTest agreementforSales = new AdminFinanceAgreementVersionsPageTest();
			
			fc.utobj().printTestStep("Add Agreement");
			String agreementVersion = agreementforSales.addAgreement(driver, config, agrmntVrsnName, 
					 royaltyPrcntg, advPercentage, royaltyAreaFranchiseValue, advAreaFranchiseValue);
			
			AdminFranchiseLocationAddNewFranchiseLocationPageTest franchise = new AdminFranchiseLocationAddNewFranchiseLocationPageTest();
			
			franchiseId = franchise.addFranchiseLocation(driver, franchiseId, agreementVersion, config);
			
			fc.utobj().printTestStep("Setup Medium of submission of Sales Reports");
			
			String salesReportType = "xls";
			
			setup.mediumOfSubmissionOfSalesReports(driver, config, salesReportType);
			
			fc.utobj().printTestStep("Navigate To Finance > Sales");
			
			fc.finance().finance_common().financeSalesPage(driver);

			fc.utobj().printTestStep("Upload Sales Report as XLS");
			
			fc.utobj().clickElement(driver, pobj.uploadSalesReport);

			fc.utobj().sendKeys(driver, pobj.clickBrowseBtnUploadFile, "C:\\Selenium_Test_Input\\testData\\FinSalesData.csv");
			
			fc.utobj().clickElement(driver, pobj.continueUploadBtn);
			
			fc.utobj().acceptAlertBox(driver);
			
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
			
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}
	
	@Test(groups = "finance_FreshDB")
	@TestCase(createdOn = "2018-06-15", updatedOn = "2018-06-15", testCaseId = "TC_FC_Alert_Upload_Sales_Report_CSV", testCaseDescription = "To verify alert for Upload Sales Reort as CSV file if file format is wrong")
	public void verifyAlertForUploadSalesReportCSV() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());
		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("finance", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		String franchiseId = "001A";
		String agrmntVrsnName = "AV";
		String royaltyPrcntg = "10";
		String advPercentage = "8";
		String royaltyAreaFranchiseValue = "5";
		String advAreaFranchiseValue = "2.5";

		try {
			driver = fc.loginpage().login(driver);

			FinanceSalesPage pobj = new FinanceSalesPage(driver);

			AdminFinanceFinanceSetupPreferencesPageTest setup = new AdminFinanceFinanceSetupPreferencesPageTest();

			fc.utobj().printTestStep("Navigate To Admin > Finance > Agreement Versions");
			AdminFinanceAgreementVersionsPageTest agreementforSales = new AdminFinanceAgreementVersionsPageTest();

			fc.utobj().printTestStep("Add Agreement");
			String agreementVersion = agreementforSales.addAgreement(driver, config, agrmntVrsnName, royaltyPrcntg,
					advPercentage, royaltyAreaFranchiseValue, advAreaFranchiseValue);

			AdminFranchiseLocationAddNewFranchiseLocationPageTest franchise = new AdminFranchiseLocationAddNewFranchiseLocationPageTest();

			franchiseId = franchise.addFranchiseLocation(driver, franchiseId, agreementVersion, config);

			fc.utobj().printTestStep("Setup Medium of submission of Sales Reports");

			String salesReportType = "csv";

			setup.mediumOfSubmissionOfSalesReports(driver, config, salesReportType);

			fc.utobj().printTestStep("Navigate To Finance > Sales");

			fc.finance().finance_common().financeSalesPage(driver);

			fc.utobj().printTestStep("Upload Sales Report as CSV");

			fc.utobj().clickElement(driver, pobj.uploadSalesReport);

			fc.utobj().sendKeys(driver, pobj.clickBrowseBtnUploadFile,
					"C:\\Selenium_Test_Input\\testData\\FinSalesData.xls");

			fc.utobj().clickElement(driver, pobj.skipDataMapping);

			fc.utobj().clickElement(driver, pobj.continueUploadBtn);

			fc.utobj().acceptAlertBox(driver);

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}
	
	
	@Test(groups = "finance11111")
	@TestCase(createdOn = "2018-06-15", updatedOn = "2018-06-15", testCaseId = "TC_FC_Download_of_Sample_File_XLS_CSV", testCaseDescription = "To verify download of sample file for Upload Sales and Upload Profit and Loss Statement")
	public void verifyDownloadSampleFile() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());
		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("finance", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			
			FinanceSalesPage pobj = new FinanceSalesPage(driver);
			
			AdminFinanceFinanceSetupPreferencesPageTest setup = new AdminFinanceFinanceSetupPreferencesPageTest();
						
			fc.utobj().printTestStep("Setup Medium of submission of Sales Reports");
			
			String salesReportType = "xls";
			
			setup.mediumOfSubmissionOfSalesReports(driver, config, salesReportType);
			
			fc.utobj().printTestStep("Navigate To Finance > Sales");
			
			fc.finance().finance_common().financeSalesPage(driver);
			
			fc.utobj().printTestStep("Download Sales Report Sample as XLS");
			
			fc.utobj().clickElement(driver, pobj.uploadSalesReport);
			
			fc.utobj().clickElement(driver, pobj.downloadSample);
			
			boolean isFileFoundXLS = fc.utobj().isFileFound(new File(FranconnectUtil.config.get("downloadFilepath")), "importFormat.xls");
			
			if (isFileFoundXLS==false) {
				
				fc.utobj().throwsException("Downloaded XLS Sale file is not found");
			}
			
			fc.utobj().printTestStep("Setup Medium of submission of Sales Reports");
			
			salesReportType = "csv";
			
			setup.mediumOfSubmissionOfSalesReports(driver, config, salesReportType);
			
			fc.utobj().printTestStep("Navigate To Finance > Sales");
			
			fc.finance().finance_common().financeSalesPage(driver);
			
			fc.utobj().printTestStep("Download Sales Report Sample as CSV");
			
			fc.utobj().clickElement(driver, pobj.uploadSalesReport);
			
			fc.utobj().clickElement(driver, pobj.downloadSample);
			
			boolean isFileFoundCSV = fc.utobj().isFileFound(new File(FranconnectUtil.config.get("downloadFilepath")), "salesDataSample.csv");
			
			if (isFileFoundCSV==false) {
				
				fc.utobj().throwsException("Downloaded CSV Sale file is not found");
				
			}
			
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
			
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}
	
	@Test(groups = { "finance_FreshDB" })
	@TestCase(createdOn = "2018-06-22", updatedOn = "2018-06-22", testCaseId = "TC_FC_Enter_Sales_Report_With_LateFee", testCaseDescription = "To verify Late Fee in Sales Report")
	public void enterSalesReportWithLateFee() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());
		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		Map<String, String> dataSet = fc.utobj().readTestData("finance", testCaseId);
		
		String agrmntVrsnName = fc.utobj().generateTestData("AV");
		String royaltyPrcntg = "10";
		String advPercentage = "8";
		String royaltyAreaFranchiseValue = "5";
		String advAreaFranchiseValue = "3";
		
		String lateFeeType="F", lateFeeAmount="50";
		String categoryQuantity="20", categoryAmount = "5000";
		
		try {
			driver = fc.loginpage().login(driver);
			FinanceSalesPage pobj = new FinanceSalesPage(driver);
			
			AdminFinanceFinanceSetupPreferencesPageTest setup = new AdminFinanceFinanceSetupPreferencesPageTest();
			
			fc.utobj().printTestStep("Add Agreement Version");
			AdminFinanceAgreementVersionsPageTest agreement = new AdminFinanceAgreementVersionsPageTest();
			String agreementVersion = agreement.addAgreementWithLateFee(driver, config, agrmntVrsnName,royaltyPrcntg, advPercentage, royaltyAreaFranchiseValue, advAreaFranchiseValue, lateFeeType, lateFeeAmount);
			
			fc.utobj().printTestStep("Navigate To Admin > Franchise Location > Add Franchise Location");
			fc.utobj().printTestStep("Add Franchise Location");
			
			String franchiseId = fc.utobj().generateTestData("GT");
			
			AdminFranchiseLocationAddNewFranchiseLocationPageTest franchise = new AdminFranchiseLocationAddNewFranchiseLocationPageTest();
			franchiseId = franchise.addFranchiseLocation(driver, franchiseId, agreementVersion, config);
			
			fc.utobj().printTestStep("Navigate To Admin > Finance > Finance Setup Preferences");
			fc.utobj().printTestStep("Setup Medium of submission of Sales Reports");
			String salesReportType = "webForm";
			setup.mediumOfSubmissionOfSalesReports(driver, config, salesReportType);
			
			fc.utobj().printTestStep("Navigate To Finance > Sales");
			fc.utobj().printTestStep("Enter Sales Report");
			
			String reportId = enterSalesReport(driver, franchiseId, categoryQuantity, categoryAmount);
			
			fc.utobj().printTestStep("Filter Sales Report By Franchise Id");
			filterSalesReport(driver, franchiseId);
			
			boolean isLateFeeDisplayed =fc.utobj().isElementPresent(driver, driver.findElement(By.xpath(".//*[contains(text(), '"+franchiseId+"')]/ancestor::tr/td[10]")));
			
			if (isLateFeeDisplayed==false) {
				fc.utobj().throwsException("Late Fee is not displayed on Sales Summary Page");
				
			}
			
			fc.utobj().printTestStep("Navigate to Finance > Store Summary");
			
			fc.finance().finance_common().financeStoreSummaryPage(driver);
			
			fc.utobj().printTestStep("Select Franchise from drop down filter");
			
			fc.utobj().clickElement(driver, driver.findElement(By.id("franchiseID")));
			
			fc.utobj().clickElement(driver, driver.findElement(By.xpath(".//*[contains(text(), '"+franchiseId+"')]")));
			
			fc.utobj().printTestStep("Click on Get Summary Button");
			
			fc.utobj().clickElement(driver, driver.findElement(By.id("getSummary")));
			
			fc.utobj().printTestStep("Verify Agreement Version at Summary Page");
			
			boolean isAgrementPresent = fc.utobj().assertLinkText(driver, agreementVersion);
			
			if (isAgrementPresent== false) {
				
				fc.utobj().throwsException("Agreement Version with late fee is not visible at Store Summary Page");
			}

			fc.utobj().printTestStep("Verify Late Fee at Summary Page");
			
			boolean isSalesPresent = fc.utobj().assertLinkText(driver, reportId);
			
			if (isSalesPresent== false) {
				
				fc.utobj().throwsException("Sales Report with Late Fee is not present in Sales Summary section of Store Summary");
			}
			
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
			
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}
	
	@Test(groups = { "finance_FreshDB" })
	@TestCase(createdOn = "2018-06-25", updatedOn = "2018-06-25", testCaseId = "TC_FC_Enter_Sales_Report_With_Late_Fee_Local_Curr", testCaseDescription = "To verify Late Fee in Sales Report when local currency is enabled")
	public void enterSalesReportWithLateFeeinLocalCurrency() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());
		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		Map<String, String> dataSet = fc.utobj().readTestData("finance", testCaseId);
		
		String agrmntVrsnName = fc.utobj().generateTestData("AV");
		String royaltyPrcntg = "10";
		String advPercentage = "8";
		String royaltyAreaFranchiseValue = "5";
		String advAreaFranchiseValue = "3";
		String lateFeeType="F", lateFeeAmount="50";
		String categoryQuantity="20", categoryAmount = "5000", allowLocalCurr ="Yes";
		
		try {
			driver = fc.loginpage().login(driver);
			FinanceSalesPage pobj = new FinanceSalesPage(driver);
			
			AdminFinanceFinanceSetupPreferencesPageTest setup = new AdminFinanceFinanceSetupPreferencesPageTest();
			
			fc.utobj().printTestStep("Navigate To Admin > Finance > Setup Preferences > Allow Sales Report in Local Currency");
			fc.utobj().printTestStep("Edit Option and choose Yes Option");
			setup.allowSalesReportEntryInLocalCurrency(driver, config, allowLocalCurr);
			
			fc.utobj().printTestStep("Add Agreement Version");
			AdminFinanceAgreementVersionsPageTest agreement = new AdminFinanceAgreementVersionsPageTest();
			String agreementVersion = agreement.addAgreementWithLateFee(driver, config, agrmntVrsnName,royaltyPrcntg, advPercentage, royaltyAreaFranchiseValue, advAreaFranchiseValue, lateFeeType, lateFeeAmount);
			
			fc.utobj().printTestStep("Navigate To Admin > Franchise Location > Add Franchise Location");
			fc.utobj().printTestStep("Add Franchise Location");
			
			
			String franchiseId = fc.utobj().generateTestData("GT");
			
			AdminFranchiseLocationAddNewFranchiseLocationPageTest franchise = new AdminFranchiseLocationAddNewFranchiseLocationPageTest();
			franchiseId = franchise.addFranchiseLocation(driver, franchiseId, agreementVersion, config);
			
			fc.utobj().printTestStep("Navigate To Admin > Finance > Finance Setup Preferences");
			fc.utobj().printTestStep("Setup Medium of submission of Sales Reports");
			String salesReportType = "webForm";
			setup.mediumOfSubmissionOfSalesReports(driver, config, salesReportType);
			
			fc.utobj().printTestStep("Navigate To Finance > Sales");
			fc.utobj().printTestStep("Enter Sales Report");
			
			enterSalesReport(driver, franchiseId, categoryQuantity, categoryAmount);
			
			//String franchiseId = " GTb25162918";
			fc.finance().finance_common().financeSalesPage(driver);
			fc.utobj().printTestStep("Filter Sales Report By Franchise Id");
			filterSalesReport(driver, franchiseId);
			
			boolean isLateFeeDisplayed =fc.utobj().isElementPresent(driver, driver.findElement(By.xpath(".//a[contains(text(), '"+franchiseId+"')]/ancestor::tr/td[10]")));
			
			if (isLateFeeDisplayed==false) {
				fc.utobj().throwsException("Late Fee is not displayed on Sales Summary Page");
				
			}
			
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
			
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}
	
	@Test(groups = { "finance_FreshDB" })
	@TestCase(createdOn = "2018-06-25", updatedOn = "2018-06-25", testCaseId = "TC_FC_Tax_at_Sales_Page", testCaseDescription = "To verify Tax on Sales Page")
	public void verifyTaxOnSalesPage() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());
		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		Map<String, String> dataSet = fc.utobj().readTestData("finance", testCaseId);
		
		String agrmntVrsnName = "AV";
		String royaltyPrcntg = "10";
		String advPercentage = "8";
		String royaltyAreaFranchiseValue = "5";
		String advAreaFranchiseValue = "3";
		
		String lateFeeType="F", lateFeeAmount="50";
		String categoryQuantity="20", categoryAmount = "5000", allowLocalCurr ="Yes";
		String taxTypeName ="taxType", taxRateName= "taxRate";
		
		try {
			driver = fc.loginpage().login(driver);
			FinanceSalesPage pobj = new FinanceSalesPage(driver);
			
			AdminFinanceFinanceSetupPreferencesPageTest setup = new AdminFinanceFinanceSetupPreferencesPageTest();
			
			fc.utobj().printTestStep("Navigate To Admin > Finance > Setup Preferences > Allow Sales Report in Local Currency");
			fc.utobj().printTestStep("Edit Option and choose Yes Option");
			setup.allowSalesReportEntryInLocalCurrency(driver, config, allowLocalCurr);
			
			fc.utobj().printTestStep("Add Agreement Version");
			AdminFinanceAgreementVersionsPageTest agreement = new AdminFinanceAgreementVersionsPageTest();
			String agreementVersion = agreement.addAgreementWithLateFee(driver, config, agrmntVrsnName,royaltyPrcntg, advPercentage, royaltyAreaFranchiseValue, advAreaFranchiseValue, lateFeeType, lateFeeAmount);
						
			AdminFinanceConfigureTaxRate taxPageTest = new AdminFinanceConfigureTaxRate();
			taxTypeName = taxPageTest.addTaxType(driver, config, taxTypeName);
			taxRateName = taxPageTest.addTaxRate(driver, config, taxRateName, taxTypeName);
			
			fc.utobj().printTestStep("Navigate To Admin > Franchise Location > Add Franchise Location");
			fc.utobj().printTestStep("Add Franchise Location");
			
			String franchiseId = fc.utobj().generateTestData("GT");
			
			AdminFranchiseLocationAddNewFranchiseLocationPageTest franchise = new AdminFranchiseLocationAddNewFranchiseLocationPageTest();
			
			franchiseId = franchise.addFranchiseLocationWithTax(driver, franchiseId, agreementVersion, taxRateName, config);
			
			fc.utobj().printTestStep("Navigate To Admin > Finance > Finance Setup Preferences");
			fc.utobj().printTestStep("Setup Medium of submission of Sales Reports");
			String salesReportType = "webForm";
			setup.mediumOfSubmissionOfSalesReports(driver, config, salesReportType);
			
			fc.utobj().printTestStep("Navigate To Finance > Sales");
			fc.utobj().printTestStep("Enter Sales Report");
			
			enterSalesReport(driver, franchiseId, categoryQuantity, categoryAmount);
			System.out.println(franchiseId);
			//String franchiseId = " GTb25162918";
			fc.finance().finance_common().financeSalesPage(driver);
			fc.utobj().printTestStep("Filter Sales Report By Franchise Id");
			filterSalesReport(driver, franchiseId);
			
			boolean isTaxDisplayed = fc.utobj().assertPageSource(driver, "Total Tax");
			
			if (isTaxDisplayed==false) {
				fc.utobj().throwsException("Total tax is not visible at Sales Summary page");
			}
			
			boolean isTaxDisplayed1 =fc.utobj().isElementPresent(driver, driver.findElement(By.xpath(".//a[contains(text(), '"+franchiseId+"')]/ancestor::tr/td[11]/a")));
			
			if (isTaxDisplayed1 ==false) {
				fc.utobj().throwsException("Late Fee is not displayed on Sales Summary Page");
				
			}
			
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
			
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}
	

	@Test(groups = "finance")
	@TestCase(createdOn = "2018-07-16", updatedOn = "2018-07-20", testCaseId = "TC_Adjustment_Delete_From_Admin", testCaseDescription = "Verify deleted Adjustment Report at Sales Page After Deleteting it from Admin")
	public void verifyAdjustmentAtSalesAfterDeletingFromAdmin() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		Map<String, String> dataSet = fc.utobj().readTestData("finance", testCaseId);

		String agrmntVrsnName = "AV";
		String royaltyPrcntg = "10";
		String advPercentage = "8";
		String royaltyAreaFranchiseValue = "5";
		String advAreaFranchiseValue = "3";

		try {
			driver = fc.loginpage().login(driver);
			FinanceSalesPage pobj = new FinanceSalesPage(driver);

			fc.utobj().printTestStep("Navigate To Admin > Finance > Agreement Versions");
			AdminFinanceAgreementVersionsPageTest agreementforSales = new AdminFinanceAgreementVersionsPageTest();

			fc.utobj().printTestStep("Add Agreement");
			String agreementVersion = agreementforSales.addAgreement(driver, config, agrmntVrsnName, 
					 royaltyPrcntg, advPercentage, royaltyAreaFranchiseValue, advAreaFranchiseValue);

			fc.utobj().printTestStep("Navigate To Admin > Finance > Configure Categories for Sales Report");
			AdminFinanceConfigureCategoriesforSalesReportPageTest sales_Report_Page = new AdminFinanceConfigureCategoriesforSalesReportPageTest();

			fc.utobj().printTestStep("Add Main Category With Calculation Type Is Addition");
			String categoryName = fc.utobj().generateTestData("testCat");
			categoryName = sales_Report_Page.addNewCategory(driver, config, categoryName, "Addition");
			
			fc.utobj().printTestStep("Add Sub Category With Calculation Type Is Addition");
			String subCategoryName = fc.utobj().generateTestData("testSubCat");
			subCategoryName = sales_Report_Page.addSubCategory(driver, config, categoryName, subCategoryName, "Addition");
			
			fc.utobj().printTestStep("Navigate To Admin > Finance >  Configure Finance Document(s) to be Uploaded");
			fc.utobj().printTestStep("Add Document Name");
			String documentName = fc.utobj().generateTestData("TestDoc");
			new AdminFinanceConfigureFinanceDocumentUploadedPageTest().addDocumentName(driver, documentName);

			fc.utobj().printTestStep("Navigate To Admin > Franchise Location > Add Franchise Location");
			fc.utobj().printTestStep("Add Franchise Location");

			String regionName = fc.utobj().generateTestData("Test");
			String storeType = fc.utobj().generateTestData("Test");
			String franchiseId = fc.utobj().generateTestData("Test");
			AdminFranchiseLocationAddFranchiseLocationPageTest franchise = new AdminFranchiseLocationAddFranchiseLocationPageTest();
			franchiseId = franchise.addFranchiseLocation_All_Agreement(driver, franchiseId, regionName, storeType,
					agreementVersion, config);

			fc.utobj().printTestStep("Navigate To Admin > Finance > Finance Setup Preferences");
			fc.utobj().printTestStep("Setup Medium of submission of Sales Reports");
			String salesReportType = "webForm";
			new AdminFinanceFinanceSetupPreferencesPageTest().mediumOfSubmissionOfSalesReports(driver, config,
					salesReportType);

			fc.utobj().printTestStep("Set Invoice Frequency");
			new AdminFinanceFinanceSetupPreferencesPageTest().setInvoice(driver, config);
			fc.utobj().printTestStep("Allow Adjustments after invoicing");
			String allowAdj ="Yes";
			
			new AdminFinanceFinanceSetupPreferencesPageTest().allowAdjustmentsAfterInvoicing(driver, config, allowAdj);

			fc.utobj().printTestStep("Navigate To Finance > Sales");
			fc.utobj().printTestStep("Enter Sales Report");
			String categoryQuantity = "110";
			String categoryAmount = "6000";
			String reportId = enterSalesReport(driver, franchiseId, categoryQuantity, categoryAmount);

			fc.utobj().printTestStep("Filter Sales Report By Franchise Id");
			filterSalesReport(driver, franchiseId);

			fc.utobj().printTestStep("Approve Sales Report");
			fc.utobj().actionImgOption(driver, reportId, "Approve");

			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().sendKeys(driver, pobj.commentTxtArea,
					"Test Approve Comments" + fc.utobj().generateRandomNumber());
			fc.utobj().clickElement(driver, pobj.SaveBtn);

			fc.utobj().clickElement(driver, pobj.cloaseBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Filter Sales Report By Franchise Id");
			filterSalesReport(driver, franchiseId);

			fc.utobj().printTestStep("Generate Invoice");
			fc.utobj().actionImgOption(driver, franchiseId, "Generate Invoice");

			fc.utobj().printTestStep("Enter Adjustment");
			fc.utobj().actionImgOption(driver, franchiseId, "Enter Adjustment");

			categoryQuantity = "200";
			categoryAmount = "400";
			fc.utobj().sendKeys(driver,	driver.findElement(By.xpath(".//td[contains(text () , 'Service')]/following-sibling::td//input[contains(@name , 'quantity')]")), categoryQuantity);
			fc.utobj().sendKeys(driver,	driver.findElement(By.xpath(".//td[contains(text () , 'Service')]/following-sibling::td//input[contains(@name , 'amount')]")), categoryAmount);

			fc.utobj().sendKeys(driver, pobj.rcvdDate, fc.utobj().getFutureDateUSFormat(-1));

			fc.utobj().clickElement(driver, pobj.SaveBtn);

			if (!fc.utobj().isSelected(driver,pobj.acknowledgement)) {
				
				fc.utobj().clickElement(driver, pobj.acknowledgement);
			}
			fc.utobj().clickElement(driver, pobj.SaveBtn);

			String reportIdAdj=fc.utobj().getText(driver, pobj.reportId);
			
			fc.utobj().clickElement(driver, pobj.okayBtn);
			
			fc.utobj().printTestStep("Navigate To Admin > Finance > Delete Sales Report");
			fc.finance().finance_common().adminFinDeleteSalesReportsLnk(driver);

			AdminFinanceDeleteSalesReportsPage delete_Sales = new AdminFinanceDeleteSalesReportsPage(driver);

			fc.utobj().printTestStep("Go to Sales Tab");
			fc.utobj().clickElement(driver, delete_Sales.sales);

			fc.utobj().selectDropDownByValue(driver, delete_Sales.selectViewPerPage, "500");

			fc.utobj().clickElement(driver, driver.findElement(By.xpath(
					".//td[contains(text () , '" + franchiseId + "')]/ancestor::tr/td/input[@name='selectedItem']")));
			fc.utobj().clickElement(driver, delete_Sales.deleteSales);
			fc.utobj().acceptAlertBox(driver);

			boolean isTextPresent = fc.utobj().assertPageSource(driver, reportIdAdj);
			
			if (isTextPresent == true) {
				fc.utobj().throwsException("was not able to verify the Deleted Sales Report");
			}

			fc.finance().finance_common().financeSalesPage(driver);
			
			fc.utobj().printTestStep("Filter Sales Report By Franchise Id");
			filterSalesReport(driver, franchiseId);
			
			boolean isTextPresent1 = fc.utobj().assertPageSource(driver, reportIdAdj);
			
			if (isTextPresent1 == true) {
				fc.utobj().throwsException("was not able to verify the Deleted Sales Report at Sales Summary Page");
			}
						
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}
	
}
