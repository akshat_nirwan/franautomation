package com.builds.test.fs;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.xml.parsers.ParserConfigurationException;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;
import org.testng.Reporter;
import org.xml.sax.SAXException;

import com.builds.test.common.SalesModule;
import com.builds.test.fs.RestAPI.BaseRestUtil;
import com.builds.test.fs.RestAPI.DataServicePage;
import com.builds.uimaps.fs.AdminFranchiseSalesManageWebFormGeneratorPage;
import com.builds.uimaps.fs.FSImportPage;
import com.builds.uimaps.fs.FSLeadSummaryAddLeadPage;
import com.builds.utilities.FranconnectUtil;

import au.com.bytecode.opencsv.CSVReader;
import au.com.bytecode.opencsv.CSVWriter;

public class AddLeadFromAllSources {
	FranconnectUtil fc = new FranconnectUtil();
	FSLeadSummaryPageTest leadSummaryobj = new FSLeadSummaryPageTest();

	/*
	 * public Map<String, String> addLeadFromSystem(WebDriver
	 * driver,Map<String,String> config,String firstName,String lastName,String
	 * ownerName) throws Exception { FsModulePageTest fsmod = new
	 * FsModulePageTest(driver); fsmod.leadManagement(driver);
	 * 
	 * leadSummaryobj.clickAddLeadLink(driver);
	 * 
	 * FSLeadSummaryPrimaryInfoPageTest p2 = new
	 * FSLeadSummaryPrimaryInfoPageTest(); Map<String,String> leadName =
	 * p2.fillLeadBasicInfo_OwnerNameBasic(driver,config,firstName,lastName,
	 * ownerName); return leadName; }
	 */

	public Map addLeadFromSystem(WebDriver driver, Map<String, String> leadInfo) throws Exception {

		try {

			SalesModule sm = new SalesModule();
			fc.sales().sales_common().fsModule(driver);
			Sales fsmod = new Sales();
			fsmod.leadManagement(driver);

			leadSummaryobj.clickAddLeadLink(driver);

			FSLeadSummaryAddLeadPage leadAddPage = new FSLeadSummaryAddLeadPage(driver);
			String i18n = FranconnectUtil.config.get("i18Files");
			if (i18n != null && !i18n.isEmpty()) {
				leadInfo.put("country", fc.utobj().translateString(leadInfo.get("country")));
				leadInfo.put("state", fc.utobj().translateString(leadInfo.get("state")));
				leadInfo.put("leadSourceCategory", fc.utobj().translateString(leadInfo.get("leadSourceCategory")));
				leadInfo.put("leadSourceDetails", fc.utobj().translateString(leadInfo.get("leadSourceDetails")));
				if (leadInfo.get("division") != null) {
					leadInfo.put("division", fc.utobj().translateString(leadInfo.get("division")));
				}
			}

			if (leadInfo.get("leadType") != null) {
				if (leadInfo.get("leadType") == "Existing Owner") {
					fc.utobj().selectDropDown(driver, leadAddPage.ownerType, leadInfo.get("leadType"));
					fc.utobj().sendKeys(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='autoComplete']"),
							leadInfo.get("existingOwner"));
					/*fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
							".//div[contains(text(),'" + leadInfo.get("existingOwner") + "')]"));*/
					fc.utobj().clickElement(driver, ".//li[@class='fc-menu-item']/div[contains(text(),'"+leadInfo.get("existingOwner")+"')]");
				} else if (leadInfo.get("leadType") == "Existing Lead") {
					fc.utobj().selectDropDown(driver, leadAddPage.ownerType, leadInfo.get("leadType"));
					fc.utobj().sendKeys(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='autoComplete']"),
							leadInfo.get("firstName") + " " + leadInfo.get("lastName"));

					try {
						fc.utobj().clickElement(driver,
								fc.utobj().getElementByXpath(driver, ".//*[@id='fc-id-1']/li[1]/div[1]"));
					} catch (Exception e) {
						fc.utobj().sendKeys(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='autoComplete']"),
								leadInfo.get("firstName") + " " + leadInfo.get("lastName"));

						fc.utobj().clickElement(driver,
								fc.utobj().getElementByXpath(driver, ".//*[@id='fc-id-1']/li[1]/div[1]"));
					}
				}

			}
			if (leadInfo.get("salutation") != null) {
				fc.utobj().selectDropDown(driver, leadAddPage.salutation, leadInfo.get("salutation"));
			}
			fc.utobj().sendKeys(driver, leadAddPage.firstName, leadInfo.get("firstName"));
			fc.utobj().sendKeys(driver, leadAddPage.lastName, leadInfo.get("lastName"));
			if (leadInfo.get("address") != null) {
				fc.utobj().sendKeys(driver, leadAddPage.address, leadInfo.get("address"));
			}
			if (leadInfo.get("address2") != null) {
				fc.utobj().sendKeys(driver, leadAddPage.address2, leadInfo.get("address2"));
			}
			if (leadInfo.get("city") != null) {
				fc.utobj().sendKeys(driver, leadAddPage.city, leadInfo.get("city"));
			}
			if (leadInfo.get("country") != null) {
				fc.utobj().selectDropDown(driver, leadAddPage.country, leadInfo.get("country"));
			}
			if (leadInfo.get("state") != null) {
				fc.utobj().selectDropDown(driver, leadAddPage.state, leadInfo.get("state"));
			}
			if (leadInfo.get("zipCode") != null) {
				fc.utobj().sendKeys(driver, leadAddPage.zip, leadInfo.get("zipCode"));
			}
			if (leadInfo.get("county") != null) {
				fc.utobj().selectDropDown(driver, leadAddPage.county, leadInfo.get("county"));
			}
			// fc.utobj().sendKeys(driver, leadAddPage.preferredModeofContact,
			// leadInfo.get("preferedModeOfContact"));
			if (leadInfo.get("bestTimeToContact") != null) {
				fc.utobj().selectDropDown(driver, leadAddPage.bestTimeToContact, leadInfo.get("bestTimeToContact"));
			}
			if (leadInfo.get("phone") != null) {
				fc.utobj().sendKeys(driver, leadAddPage.phone, leadInfo.get("phone"));
			}
			if (leadInfo.get("phoneExtension") != null) {
				fc.utobj().sendKeys(driver, leadAddPage.phoneExt, leadInfo.get("phoneExtension"));
			}
			if (leadInfo.get("homePhone") != null) {
				fc.utobj().sendKeys(driver, leadAddPage.homePhone, leadInfo.get("homePhone"));
			}
			if (leadInfo.get("homePhoneExtension") != null) {
				fc.utobj().sendKeys(driver, leadAddPage.homePhoneExt, leadInfo.get("homePhoneExtension"));
			}
			if (leadInfo.get("fax") != null) {
				fc.utobj().sendKeys(driver, leadAddPage.fax, leadInfo.get("fax"));
			}
			if (leadInfo.get("mobile") != null) {
				fc.utobj().sendKeys(driver, leadAddPage.mobile, leadInfo.get("mobile"));
			}
			fc.utobj().sendKeys(driver, leadAddPage.emailID, leadInfo.get("email"));
			// fc.utobj().sendKeys(driver, leadAddPage.companyName, "Company
			// Name");
			if (leadInfo.get("comments") != null) {
				fc.utobj().sendKeys(driver, leadAddPage.comments, leadInfo.get("comments"));
			}
			if (leadInfo.get("leadOwner") == "Based on Assignment Rules") {
				fc.utobj().clickElement(driver, leadAddPage.basedOnAssignment);
			} else {
				fc.utobj().selectDropDownByPartialText(driver, leadAddPage.leadOwnerID, leadInfo.get("leadOwner"));
			}

			// fc.utobj().selectDropDownByVisibleText(driver,
			// leadAddPage.leadRatingID, "Hot");
			// fc.utobj().selectDropDownByVisibleText(driver,
			// leadAddPage.marketingCode, marketingCode);
			fc.utobj().selectDropDownByVisibleText(driver, leadAddPage.leadSource2ID,
					leadInfo.get("leadSourceCategory").toString());

			fc.utobj().selectDropDownByVisibleText(driver, leadAddPage.leadSource3ID,
					leadInfo.get("leadSourceDetails").toString());
			if (leadInfo.get("division") != null) {
				fc.utobj().selectDropDownByVisibleText(driver, leadAddPage.divisionDropDown,
						leadInfo.get("division").toString());
			}
			fc.utobj().clickElement(driver, leadAddPage.assignAutomaticCampaign);

			fc.utobj().clickElement(driver, leadAddPage.save);

		} catch (Exception e) {
			fc.utobj().throwsException("Lead addtion not Succesful " + e.getMessage());
		}
		return leadInfo;
	}

	public Map addLeadFromImport(WebDriver driver, String fileName, Map<String, String> leadInfo) {

		// Map<String,String> leadInfo = new HashMap<String, String>();

		try {
			File inputFile = new File(fileName);

			// Read existing file
			CSVReader reader = new CSVReader(new FileReader(inputFile), ',');
			List<String[]> csvBody = reader.readAll();
			// get CSV row column and replace with by using row and column
			if (leadInfo.get("InquiryDate") != null) {
				csvBody.get(1)[0] = leadInfo.get("InquiryDate");
			}

			csvBody.get(1)[1] = leadInfo.get("firstName");
			csvBody.get(1)[2] = leadInfo.get("lastName");
			if (leadInfo.get("email") != null) {
				csvBody.get(1)[3] = leadInfo.get("email");
			}
			if (leadInfo.get("companyName") != null) {
				csvBody.get(1)[4] = leadInfo.get("companyName");
			}
			if (leadInfo.get("address1") != null) {
				csvBody.get(1)[5] = leadInfo.get("address1");
			}
			if (leadInfo.get("address2") != null) {
				csvBody.get(1)[6] = leadInfo.get("address2");
			}
			if (leadInfo.get("city") != null) {
				csvBody.get(1)[7] = leadInfo.get("city");
			}
			if (leadInfo.get("state") != null) {
				csvBody.get(1)[8] = leadInfo.get("state");
			}
			if (leadInfo.get("country") != null) {
				csvBody.get(1)[9] = leadInfo.get("country");
			}
			if (leadInfo.get("zipCode") != null) {
				csvBody.get(1)[10] = leadInfo.get("zipCode");
			}
			if (leadInfo.get("workPhone") != null) {
				csvBody.get(1)[11] = leadInfo.get("workPhone");
			}
			if (leadInfo.get("workPhoneExtension") != null) {
				csvBody.get(1)[12] = leadInfo.get("workPhoneExtension");
			}
			if (leadInfo.get("homePhone") != null) {
				csvBody.get(1)[13] = leadInfo.get("homePhone");
			}
			if (leadInfo.get("homePhoneExtension") != null) {
				csvBody.get(1)[14] = leadInfo.get("homePhoneExtension");
			}
			if (leadInfo.get("fax") != null) {
				csvBody.get(1)[15] = leadInfo.get("fax");
			}
			if (leadInfo.get("comments") != null) {
				csvBody.get(1)[16] = leadInfo.get("comments");
			}
			if (leadInfo.get("mobile") != null) {
				csvBody.get(1)[17] = leadInfo.get("mobile");
			}
			if (leadInfo.get("bestTimeToContact") != null) {
				csvBody.get(1)[18] = leadInfo.get("bestTimeToContact");
			}
			if (leadInfo.get("status") != null) {
				csvBody.get(1)[19] = leadInfo.get("status");
			} else {
				leadInfo.put("status", "New Lead");
			}
			/*
			 * if (leadInfo.get("leadSourceCategory")!=null) {
			 * csvBody.get(1)[20] = leadInfo.get("leadSourceCategory"); } if
			 * (leadInfo.get("leadSourceDetails")!=null) { csvBody.get(1)[21] =
			 * leadInfo.get("leadSourceDetails"); }
			 */
			if (leadInfo.get("assignTo") != null) {
				csvBody.get(1)[22] = leadInfo.get("assignTo");
			}
			if (leadInfo.get("preferedModeOfContact") != null) {
				csvBody.get(1)[23] = leadInfo.get("preferedModeOfContact");
			}
			if (leadInfo.get("currentNetWorth") != null) {
				csvBody.get(1)[24] = leadInfo.get("currentNetWorth");
			}
			if (leadInfo.get("cashAvailableForInvestment") != null) {
				csvBody.get(1)[25] = leadInfo.get("cashAvailableForInvestment");
			}
			if (leadInfo.get("investmentTimeframe") != null) {
				csvBody.get(1)[26] = leadInfo.get("investmentTimeframe");
			}
			if (leadInfo.get("sourceOfInvestment") != null) {
				csvBody.get(1)[27] = leadInfo.get("sourceOfInvestment");
			}
			if (leadInfo.get("background") != null) {
				csvBody.get(1)[28] = leadInfo.get("background");
			}
			/*
			 * if (leadInfo.get("division")!=null) { csvBody.get(1)[29] =
			 * leadInfo.get("background"); }
			 */
			if (leadInfo.get("brand") != null) {
				csvBody.get(1)[30] = leadInfo.get("brand");
			}

			reader.close();

			// Write to CSV file which is open
			CSVWriter writer = new CSVWriter(new FileWriter(inputFile), ',');
			writer.writeAll(csvBody);
			writer.flush();
			writer.close();

			Sales fsmod = new Sales();

			/*
			 * if(leadInfo.get("salutation")!=null){
			 * fc.utobj().selectDropDown(driver, leadAddPage.salutation,
			 * leadInfo.get("salutation")); }
			 */
			FSImportPage importPage = new FSImportPage(driver);
			fsmod.importPage(driver);
			fileName = fc.utobj().getFilePathFromTestData(fileName);
			fc.utobj().sendKeys(driver, importPage.importFile, fileName);
			fc.utobj().clickElement(driver, importPage.continueBtn);

			fc.utobj().selectDropDownByPartialText(driver, importPage.firstName, "First Name");
			fc.utobj().selectDropDownByPartialText(driver, importPage.lastName, "Last Name");
			fc.utobj().selectDropDownByPartialText(driver, importPage.email, "Email");
			fc.utobj().selectDropDownByPartialText(driver, importPage.companyName, "Company Name");
			fc.utobj().selectDropDownByPartialText(driver, importPage.address1, "Address");
			fc.utobj().selectDropDownByPartialText(driver, importPage.address2, "Address 2");
			fc.utobj().selectDropDownByPartialText(driver, importPage.city, "City");
			fc.utobj().selectDropDownByPartialText(driver, importPage.country, "Country");
			fc.utobj().selectDropDownByPartialText(driver, importPage.stateProvince, "State / Province");
			fc.utobj().selectDropDownByPartialText(driver, importPage.zipPostalCode, "Zip / Postal Code");
			fc.utobj().selectDropDownByPartialText(driver, importPage.workPhone, "Wrok Phone");
			fc.utobj().selectDropDownByPartialText(driver, importPage.workPhoneExt, "Work Phone Extension");
			fc.utobj().selectDropDownByPartialText(driver, importPage.homePhone, "Home Phone");
			fc.utobj().selectDropDownByPartialText(driver, importPage.homePhoneExt, "Home Phone Extension");
			fc.utobj().selectDropDownByPartialText(driver, importPage.fax, "Fax");
			fc.utobj().selectDropDownByPartialText(driver, importPage.comments, "Comments");
			fc.utobj().selectDropDownByPartialText(driver, importPage.mobile, "Mobile");
			fc.utobj().selectDropDownByPartialText(driver, importPage.bestTimetoContact, "Best Time To Contact");
			fc.utobj().selectDropDownByPartialText(driver, importPage.leadStatus, "Lead Status");
			fc.utobj().selectDropDownByPartialText(driver, importPage.leadSourceCatagory, "Lead Source Category");
			fc.utobj().selectDropDownByPartialText(driver, importPage.leadSourceDetails, "Lead Source Details");
			fc.utobj().selectDropDownByPartialText(driver, importPage.assignTo, "Assign To");
			fc.utobj().selectDropDownByPartialText(driver, importPage.preferedModeOfContact,
					"Preferred Mode of Contact");
			fc.utobj().selectDropDownByPartialText(driver, importPage.currentNetWorth, "Current Net Worth");
			fc.utobj().selectDropDownByPartialText(driver, importPage.cashAvailableForInves,
					"Cash Available for Investment");
			fc.utobj().selectDropDownByPartialText(driver, importPage.investmentTimeframe, "Investment Timeframe");
			fc.utobj().selectDropDownByPartialText(driver, importPage.sourceOfInvestment, "Source Of Investment");
			fc.utobj().selectDropDownByPartialText(driver, importPage.background, "Background");
			// fc.utobj().selectDropDownByPartialText(driver,
			// importPage.division, "Division");
			String inquiryDate = fc.utobj().getCurrentDateUSFormat();
			fc.utobj().sendKeys(driver, importPage.inquiryDate, inquiryDate);
			if (leadInfo.get("owner") != null) {
				if (leadInfo.get("owner").contentEquals("Based on Assignment Rules")) {
					fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='assignment']"));
				} else {
					fc.utobj().selectDropDownByPartialText(driver, importPage.defaultLeadOwner, leadInfo.get("owner"));
				}
			} else {
				fc.utobj().selectDropDownByPartialText(driver, importPage.defaultLeadOwner,
						"FranConnect Administrator");
			}
			fc.utobj().selectDropDownByPartialText(driver, importPage.defaultLeadStatus, leadInfo.get("status"));
			fc.utobj().selectDropDownByPartialText(driver, importPage.defaultLeadSourceCatagory, "Import");
			fc.utobj().selectDropDownByPartialText(driver, importPage.defaultLeadSourceDetails, "None");
			fc.utobj().clickElement(driver, importPage.caanadianLeadDisclaimer);
			fc.utobj().clickElement(driver, importPage.continueBtn);
			fc.utobj().clickElement(driver, importPage.continueBtn);

			fc.utobj().clickElement(driver, importPage.backBtn);
		} catch (Exception e) {
			System.out.println(e);
		}
		return leadInfo;
	}

	public void addLeadFromParsing(WebDriver driver, Map<String, String> config, String firstName, String lastName,
			String city, String state, String country, String zipCode, String phone, String email, String address,
			String leadSourceCatogry, String leadSourceDetails) {

		// Map<String,String> leadInfo = new HashMap<String, String>();

		try {/*
			Sales fsmod = new Sales();

			String mailSubject = "BISON";
			String mailBody = "<head></head>" + "<body>" + "<p>" + "firstName: " + firstName + "<br/>" + "lastName: "
					+ lastName + "<br/>" + "city: " + city + "</br>" + "state: " + state + "</br>" + "country: "
					+ country + "</br>" + "zipCode: " + zipCode + "</br>" + "phone: " + phone + "</br>" + "email: "
					+ email + "</br>" + "address: " + address + "</br>" + "Lead source Category: " + leadSourceCatogry
					+ "</br>" + "Lead source details: " + leadSourceDetails + "</br>" + "<p>" + "</body>" + "</html>";

			String mailTo = "anukaran.mishra@franconnect.net";
			String mailCC = "salesautoamtion@franqa.net";
			String mailBCC = "salesautoamtion@franqa.net";
			String mailFrom = "salesautoamtion@franqa.net";
			String replyTo = "anukaran.mishra@franconnect.net";
			String fileName = "";

			// fsmod.importPage(driver);
			fc.codeUtil().sendEmail(config.get("SMTP_HOST_NAME"), config.get("SMTP_PORT"), mailSubject, mailBody,
					mailTo, mailCC, mailBCC, mailFrom, replyTo, fileName);
			String expectedSubject = mailSubject;
			String expectedMessageBody = mailBody;
			String emailId = mailTo;

			Map<String, String> mailData = fc.utobj().readMailBox(expectedSubject, expectedMessageBody, emailId,
					"sdg@1a@Hfs");
			System.out.println(mailData);
			if (!mailData.get("mailBody").contains(firstName)) {
				fc.utobj().throwsSkipException("was not able to verify parsing email content");
			}
			fc.utobj().sleep(302000);
			FSSearchPageTest p3 = new FSSearchPageTest();
			p3.searchByLeadName(driver, firstName, lastName);
			fc.utobj().actionImgOption(driver, firstName + " " + lastName, "Send Email");
			boolean isfirstName = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//*[contains(text(),'" + firstName + "')]");
			if (isfirstName == false) {
				fc.utobj().throwsException("FirstName not verified of lead added through parsing");
			}
			boolean isLastName = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//*[contains(text(),'" + lastName + "')]");
			if (isLastName == false) {
				fc.utobj().throwsException("FirstName not verified of lead added through parsing");
			}
			boolean isCity = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//*[contains(text(),'" + city + "')]");
			if (isCity == false) {
				fc.utobj().throwsException("FirstName not verified of lead added through parsing");
			}
			boolean isState = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//*[contains(text(),'" + state + "')]");
			if (isState == false) {
				fc.utobj().throwsException("FirstName not verified of lead added through parsing");
			}
			boolean isCountry = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//*[contains(text(),'" + country + "')]");
			if (isCountry == false) {
				fc.utobj().throwsException("FirstName not verified of lead added through parsing");
			}
			boolean isZipcode = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//*[contains(text(),'" + zipCode + "')]");
			if (isZipcode == false) {
				fc.utobj().throwsException("FirstName not verified of lead added through parsing");
			}
			boolean isphone = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//*[contains(text(),'" + phone + "')]");
			if (isphone == false) {
				fc.utobj().throwsException("FirstName not verified of lead added through parsing");
			}
			boolean isEmail = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//*[contains(text(),'" + email + "')]");
			if (isEmail == false) {
				fc.utobj().throwsException("FirstName not verified of lead added through parsing");
			}
		*/} catch (Exception e) {
			System.out.println(e);
		}
	}

	public Map<String, String> addLeadFromWebForm(WebDriver driver, String formName, Map<String, String> leadInfo,
			String afterSubission) throws Exception

	{
		AdminSales admin = new AdminSales();
		admin.manageWebFormGenerator(driver);
		AdminFranchiseSalesManageWebFormGeneratorPage pobj = new AdminFranchiseSalesManageWebFormGeneratorPage(driver);
		fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//input[@value='Create New Form']"));
		fc.utobj().sendKeys(driver, fc.utobj().getElementByID(driver, "formName"), formName);
		fc.utobj().sendKeys(driver, fc.utobj().getElementByID(driver, "formDisplayTitle"), "WebFrom Title");
		fc.utobj().sendKeys(driver, fc.utobj().getElementByID(driver, "formUrl"), formName);
		fc.utobj().clickElement(driver, fc.utobj().getElementByID(driver, "detailsNextBtn"));

		fc.utobj().clickElement(driver, pobj.primaryInfo);
		/*
		 * fc.utobj().sendKeys(driver, pobj.searchField, "Title");
		 * ; fc.utobj().dragAndDropElement(driver,
		 * fc.utobj().getElementByXpath(driver,".//a[.='Title']")),
		 * pobj.defaultDropHere);
		 */
		// fc.utobj().sendKeys(driver, pobj.searchField, "Salutation");
		// ;
		// fc.utobj().dragAndDropElement(driver,
		// fc.utobj().getElementByXpath(driver,".//a[.='Salutation']")),
		// pobj.defaultDropHere);
		;
		if (leadInfo.get("address1") != null) {
			fc.utobj().sendKeys(driver, pobj.searchField, "Address1");
			;
			fc.utobj().dragAndDropElement(driver, fc.utobj().getElementByXpath(driver, ".//a[.='Address1']"),
					pobj.defaultDropHere);
			;
		}
		if (leadInfo.get("address2") != null) {
			fc.utobj().sendKeys(driver, pobj.searchField, "Address2");
			;
			fc.utobj().dragAndDropElement(driver, fc.utobj().getElementByXpath(driver, ".//a[.='Address2']"),
					pobj.defaultDropHere);
			;
		}
		if (leadInfo.get("city") != null) {
			fc.utobj().sendKeys(driver, pobj.searchField, "City");
			;
			fc.utobj().dragAndDropElement(driver, fc.utobj().getElementByXpath(driver, ".//a[.='City']"),
					pobj.defaultDropHere);
			;
		}
		if (leadInfo.get("country") != null) {
			fc.utobj().sendKeys(driver, pobj.searchField, "Country");
			;
			fc.utobj().dragAndDropElement(driver, fc.utobj().getElementByXpath(driver, ".//a[.='Country']"),
					pobj.defaultDropHere);
			;
		}
		if (leadInfo.get("zipCode") != null) {
			fc.utobj().sendKeys(driver, pobj.searchField, "Zip / Postal Code");
			;
			fc.utobj().dragAndDropElement(driver, fc.utobj().getElementByXpath(driver, ".//a[.='Zip / Postal Code']"),
					pobj.defaultDropHere);
			;
		}
		if (leadInfo.get("state") != null) {
			fc.utobj().sendKeys(driver, pobj.searchField, "State / Province");
			;
			fc.utobj().dragAndDropElement(driver, fc.utobj().getElementByXpath(driver, ".//a[.='State / Province']"),
					pobj.defaultDropHere);
			;
		}

		/*
		 * fc.utobj().sendKeys(driver, pobj.searchField, "Best Time to Contact"
		 * ); ; fc.utobj().dragAndDropElement(driver,
		 * fc.utobj().getElementByXpath(driver,
		 * ".//*[@id='bestTimeToContact_fld']/a")), pobj.defaultDropHere);
		 * ;
		 */
		if (leadInfo.get("phone") != null) {
			fc.utobj().sendKeys(driver, pobj.searchField, "Work Phone");
			;
			fc.utobj().dragAndDropElement(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='phone_fld']/a"),
					pobj.defaultDropHere);
			;
		}
		if (leadInfo.get("phoneExtension") != null) {
			fc.utobj().sendKeys(driver, pobj.searchField, "Work Phone Extension");
			;
			fc.utobj().dragAndDropElement(driver,
					fc.utobj().getElementByXpath(driver, ".//a[.='Work Phone Extension']"), pobj.defaultDropHere);
			;
		}
		if (leadInfo.get("homePhone") != null) {
			fc.utobj().sendKeys(driver, pobj.searchField, "Home Phone");
			;
			fc.utobj().dragAndDropElement(driver, fc.utobj().getElementByXpath(driver, ".//a[.='Home Phone']"),
					pobj.defaultDropHere);
			;
		}
		if (leadInfo.get("homePhoneExtension") != null) {
			fc.utobj().sendKeys(driver, pobj.searchField, "Home Phone Extension");
			;
			fc.utobj().dragAndDropElement(driver,
					fc.utobj().getElementByXpath(driver, ".//a[.='Home Phone Extension']"), pobj.defaultDropHere);
			;
		}
		if (leadInfo.get("fax") != null) {
			fc.utobj().sendKeys(driver, pobj.searchField, "Fax");
			;
			fc.utobj().dragAndDropElement(driver, fc.utobj().getElementByXpath(driver, ".//a[.='Fax']"),
					pobj.defaultDropHere);
			;
		}
		if (leadInfo.get("mobile") != null) {
			fc.utobj().sendKeys(driver, pobj.searchField, "Mobile");
			;
			fc.utobj().dragAndDropElement(driver, fc.utobj().getElementByXpath(driver, ".//a[.='Mobile']"),
					pobj.defaultDropHere);
		}
		/*
		 * ; fc.utobj().sendKeys(driver, pobj.searchField,
		 * "Company Name"); ;
		 * fc.utobj().dragAndDropElement(driver,
		 * fc.utobj().getElementByXpath(driver, ".//a[.='Company Name']")),
		 * pobj.defaultDropHere);
		 */
		if (leadInfo.get("comments") != null) {
			;
			fc.utobj().sendKeys(driver, pobj.searchField, "Comments");
			;
			fc.utobj().dragAndDropElement(driver, fc.utobj().getElementByXpath(driver, ".//a[.='Comments']"),
					pobj.defaultDropHere);
		}
		;

		fc.utobj().clickElement(driver, fc.utobj().getElementByID(driver, "designNextBtn"));

		/*
		 * fc.utobj().sendKeys(driver, pobj.searchField, "Lead Source Category"
		 * ); ; fc.utobj().dragAndDropElement(driver,
		 * fc.utobj().getElementByXpath(driver,".//a[.='Lead Source Category']"
		 * )), pobj.defaultDropHere); ;
		 * fc.utobj().sendKeys(driver, pobj.searchField, "Lead Source Details");
		 * ; fc.utobj().dragAndDropElement(driver,
		 * fc.utobj().getElementByXpath(driver,".//a[.='Lead Source Details']"
		 * )), pobj.defaultDropHere); ;
		 */
		if (leadInfo.get("leadStatus") != null) {
			fc.utobj().selectDropDownByPartialText(driver,
					fc.utobj().getElementByXpath(driver, ".//select[@id='leadStatusID']"), leadInfo.get("leadStatus"));
		}
		// String leadOwner=leadInfo.get("leadOwner");
		if (leadInfo.get("leadOwner") != null) {
			fc.utobj().clickElement(driver, fc.utobj().getElementByID(driver, "assignUser"));
			fc.utobj().selectDropDownByPartialText(driver,
					fc.utobj().getElementByXpath(driver, ".//select[@id='leadOwnerID']"), leadInfo.get("leadOwner"));
		}
		// fc.utobj().clickElement(driver,
		// fc.utobj().getElementByID(driver,"automatic")));
		if (leadInfo.get("leadSourceCategory") != null) {
			fc.utobj().selectDropDownByPartialText(driver,
					fc.utobj().getElementByXpath(driver, ".//select[@id='leadSource2ID']"),
					leadInfo.get("leadSourceCategory"));
		}
		if (leadInfo.get("leadSourceDetails") != null) {
			fc.utobj().selectDropDownByPartialText(driver,
					fc.utobj().getElementByXpath(driver, ".//select[@id='leadSource3ID']"),
					leadInfo.get("leadSourceDetails"));
		}
		if (leadInfo.get("divisionName") != null) {
			fc.utobj().selectDropDownByPartialText(driver,
					fc.utobj().getElementByXpath(driver, ".//select[@id='brandID']"), leadInfo.get("divisionName"));
		}
		if (leadInfo.get("campaignName") != null) {
			fc.utobj().selectDropDownByPartialText(driver,
					fc.utobj().getElementByXpath(driver, ".//select[@id='campaignID']"), leadInfo.get("campaignName"));
		}

		if (afterSubission.equalsIgnoreCase("Confirmation Message")) {
			fc.utobj().switchFrameById(driver, "textBlock_ifr");
			WebElement editorTextArea1 = fc.utobj().getElementByXpath(driver, ".//*[@id='tinymce']/p");
			Actions actions1 = new Actions(driver);
			actions1.moveToElement(editorTextArea1);
			actions1.click();
			actions1.sendKeys(
					"Thank you for submitting your information, we will get back to you shortly." + formName + ".");
			actions1.build().perform();
			fc.utobj().switchFrameToDefault(driver);
		} else if (afterSubission.equalsIgnoreCase("Redirect Url")) {
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//input[@name='afterSubmissionMsgType' and @value='url']"));
			fc.utobj().sendKeys(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='formCUrl']"),
					"http://www.franconnect.com");
		}
		fc.utobj().clickElement(driver, fc.utobj().getElementByID(driver, "settingsNextBtn"));

		fc.utobj().clickElement(driver, fc.utobj().getElementByID(driver, "hostURL"));
		String newFormUrl = fc.utobj().getText(driver, fc.utobj().getElementByID(driver, "hostCodeBox"));
		leadInfo.put("newFormURL", newFormUrl);
		fc.utobj().clickElement(driver, fc.utobj().getElementByID(driver, "confirmationNextBtn"));

		String parentWindow = driver.getWindowHandle();
		fc.utobj().sendKeys(driver, pobj.searchMyForm, formName);
		fc.utobj().clickElement(driver, pobj.searchMyFormBtn);
		fc.utobj().actionImgOption(driver, formName, "Launch & Test");
		fc.utobj().sleep();

		String formTitle = "WebFrom Title";
		Set<String> allWindows2 = driver.getWindowHandles();
		for (String currentWindow : allWindows2) {
			//
			if (!currentWindow.equalsIgnoreCase(parentWindow)) {
				driver.switchTo().window(currentWindow);
				//
				String titleTextCurrent = driver.getTitle();
				if (titleTextCurrent.contains(formTitle)) {
					// fill the form with data
					//

					fc.utobj().sendKeys(driver, pobj.leadFirstName, leadInfo.get("firstName"));
					fc.utobj().sendKeys(driver, pobj.leadLastName, leadInfo.get("lastName"));
					if (leadInfo.get("email") != null) {
						fc.utobj().sendKeys(driver, pobj.emailID, leadInfo.get("email"));
					}
					// fc.utobj().selectDropDownByPartialText(driver,
					// pobj.salutationField, "Mr.");
					if (leadInfo.get("address1") != null) {
						fc.utobj().sendKeys(driver, pobj.address, leadInfo.get("address1"));
					}
					if (leadInfo.get("address2") != null) {
						fc.utobj().sendKeys(driver, pobj.address2, leadInfo.get("address2"));
					}
					if (leadInfo.get("country") != null) {
						fc.utobj().selectDropDownByPartialText(driver, pobj.country, leadInfo.get("country"));
					}
					if (leadInfo.get("zipCode") != null) {
						fc.utobj().sendKeys(driver, pobj.zip, leadInfo.get("zipCode"));
					}
					if (leadInfo.get("state") != null) {
						fc.utobj().selectDropDownByPartialText(driver, pobj.stateID, leadInfo.get("state"));
					}
					if (leadInfo.get("phone") != null) {
						fc.utobj().sendKeys(driver, pobj.phone, leadInfo.get("phone"));
					}
					if (leadInfo.get("phoneExtension") != null) {
						fc.utobj().sendKeys(driver, pobj.phoneExt, leadInfo.get("phoneExtension"));
					}
					if (leadInfo.get("homePhone") != null) {
						fc.utobj().sendKeys(driver, pobj.homePhone, leadInfo.get("homePhone"));
					}
					if (leadInfo.get("homePhoneExtension") != null) {
						fc.utobj().sendKeys(driver, pobj.homePhoneExt, leadInfo.get("homePhoneExtension"));
					}
					if (leadInfo.get("homePhonefax") != null) {
						fc.utobj().sendKeys(driver, pobj.fax, leadInfo.get("homePhonefax"));
					}
					if (leadInfo.get("mobile") != null) {
						fc.utobj().sendKeys(driver, pobj.mobile, leadInfo.get("mobile"));
					}
					if (leadInfo.get("comments") != null) {
						fc.utobj().sendKeys(driver, pobj.comments, leadInfo.get("comments"));
					}

					fc.utobj().clickElement(driver, pobj.submitBtn);

					if (afterSubission.equalsIgnoreCase("Confirmation Message")) {
						fc.utobj().isTextDisplayed(driver,
								"Thank you for submitting your information, we will get back to you shortly." + formName
										+ ".",
								"was not able to verify confirmation Msg");
					} else if (afterSubission.equalsIgnoreCase("Redirect Url")) {
						String redirectUrl = driver.getCurrentUrl();
						if (!(redirectUrl.contains("franconnect.com"))) {
							fc.utobj().throwsException("Redirect URL not loaded");
						}
					}
					driver.close();
				} else {
					driver.close();
					driver.switchTo().window(parentWindow);
				}
				driver.switchTo().window(parentWindow);
			}
			//
		}

		return leadInfo;

	}

	// Code to call above method
	/*
	 * AddLeadFromAllSources addleaddall=new AddLeadFromAllSources(); String
	 * formName = "wb"+fc.utobj().generateRandomNumber(); String
	 * firstName=fc.utobj().generateTestData(dataSet.get("firstName")); String
	 * lastName=fc.utobj().generateTestData(dataSet.get("lastName")); String
	 * country=dataSet.get("country"); String state=dataSet.get("state"); String
	 * emailId=dataSet.get("emailId"); String
	 * leadOwner=dataSet.get("leadOwner"); String
	 * leadSourceCategory=dataSet.get("leadSourceCategory"); String
	 * leadSourceDetails=dataSet.get("leadSourceDetails");
	 * 
	 * Map<String,String> leadInfo = new HashMap<String, String>();
	 * leadInfo.put("firstName", firstName); leadInfo.put("lastName", lastName);
	 * leadInfo.put("country", country); leadInfo.put("state", state);
	 * leadInfo.put("email", emailId); leadInfo.put("leadOwner", leadOwner);
	 * leadInfo.put("leadSourceCategory", leadSourceCategory);
	 * leadInfo.put("leadSourceDetails", leadSourceDetails);
	 * addleaddall.addLeadFromWebForm(driver, formName, leadInfo);
	 */

	public Map<String, String> fillFormDataWithDataset(WebDriver driver, String testCaseId, Map<String, String> dataSet)
			throws Exception {
		Reporter.log("Filling TestCase Data = " + testCaseId);
		// fc.utobj().printTestStep(testCaseId, "Filling TestCase Data");
		String id = "id";
		if ("TC_FC_QA_Sale_Lead_QualificationDetails".equals(testCaseId)) {
			id = "name";
		}
		// Map<String,String> dataSet = new HashMap<String, String>();
		// dataSet = fc.utobj().readTestData(config, testCaseId);

		List<String> listItems = new ArrayList<String>();
		Map<String, String> printInfo = new HashMap<String, String>();

		List<WebElement> mandateBox = null;
		mandateBox = driver.findElements(By.xpath(
				".//td[@qat_tableid='addData']//*[@type='text' or @class='fTextBoxDate' or @class='fTextBox' or @class='multiList' or @type='radio' or @type='checkbox' or @type='checkBox' or @id='ms-parentsellerCombo' or @class='ms-choice' or @class='form-control']"));

		Map<String, String> alreadyitrateElement = new HashMap<>();
		Map<String, String> alreadyradio = new HashMap<>();
		Map<String, String> alreadycheckBox = new HashMap<>();

		try {
			for (WebElement webElement : mandateBox) {

				if (webElement.getAttribute("" + id) != null && !"".equals(webElement.getAttribute("" + id))
						&& !"null".equals(webElement.getAttribute("" + id))
						&& !":".equals(webElement.getAttribute("" + id))
						&& !"dateOfOpen".equals(webElement.getAttribute("" + id))
						&& !"fimCbOtherComplaintType".equals(webElement.getAttribute("" + id))) {
					// System.out.println(""+webElement.getAttribute(""+id));
					// System.out.println("Key==>>"+webElement.getAttribute("id")+"===values==>>"+webElement.getAttribute("value")+"=====Type===>>>"+webElement.getAttribute("type"));
					if (alreadyitrateElement.containsKey(webElement.getAttribute("" + id))) {

					} else {
						if (dataSet.containsKey(webElement.getAttribute("" + id))) {
							WebElement elementmovepostion = null;
							if ("name".equals(id)) {
								elementmovepostion = fc.utobj().getElementByName(driver,
										"" + webElement.getAttribute("" + id));
							} else {
								elementmovepostion = fc.utobj().getElementByID(driver,
										"" + webElement.getAttribute("" + id));

							}
							fc.utobj().moveToElement(driver, elementmovepostion);
							if (webElement.getAttribute("type").indexOf("select-one") != -1) {
								Select singledrop = null;
								List<WebElement> singleDropDown = null;
								try {
									singledrop = new Select(
											fc.utobj().getElementByID(driver, "" + webElement.getAttribute("" + id)));
									singleDropDown = driver.findElements(By.id("" + webElement.getAttribute("" + id)));
								} catch (Exception e) {
									singledrop = new Select(
											fc.utobj().getElementByName(driver, "" + webElement.getAttribute("" + id)));
									singleDropDown = driver
											.findElements(By.name("" + webElement.getAttribute("" + id)));
								}
								if (webElement.getAttribute("value") == null
										|| "".equals(webElement.getAttribute("value"))
										|| "null".equals(webElement.getAttribute("value"))
										|| "-1".equals(webElement.getAttribute("value"))) {

									int size = singleDropDown.size();
									if (dataSet.get("" + webElement.getAttribute("" + id)) != null
											&& !"".equals(dataSet.get("" + webElement.getAttribute("" + id)))) {
										if (size >= 1) {
											try {
												WebElement dropDownField = fc.utobj().getElementByID(driver,
														"" + webElement.getAttribute("id"));
												fc.utobj().selectDropDownByVisibleText(driver, dropDownField,
														dataSet.get("" + webElement.getAttribute("" + id)));

												if (!"birthMonth".equals(webElement.getAttribute("" + id))
														&& !"birthDate".equals(webElement.getAttribute("" + id))
														&& !"spouseBirthMonth".equals(webElement.getAttribute("" + id))
														&& !"spouseBirthDate"
																.equals(webElement.getAttribute("" + id))) {
													listItems.add(dataSet.get("" + webElement.getAttribute("" + id)));
												}

												printInfo.put("" + webElement.getAttribute("" + id),
														dataSet.get("" + webElement.getAttribute("" + id)));

											} catch (Exception eradio) {
												// Reporter.log("Problem in
												// selecting drop :
												// "+""+webElement.getAttribute("id")
												// +" Values getting from Excel
												// not match in desired
												// fomrat."+""+dataSet.get(""+webElement.getAttribute("id")));
											}
										}
									}
								}
							} else if (webElement.getAttribute("type").indexOf("select-multiple") != -1) {
								if (webElement.getAttribute("value") == null
										|| "".equals(webElement.getAttribute("value"))
										|| "null".equals(webElement.getAttribute("value"))
										|| "-1".equals(webElement.getAttribute("value"))) {
									try {
										WebElement element = driver
												.findElement(By.id("ms-parent" + webElement.getAttribute("" + id)));
										fc.utobj().clickElement(driver, element);
										fc.utobj().clickElement(driver,
												fc.utobj().getElementByXpath(driver, ".//*[@id='selectAll']"));
										fc.utobj().clickElement(driver, element);
									} catch (Exception emultipal) {
										// Reporter.log("Problem in multiple
										// value drop-down :
										// "+""+webElement.getAttribute("id"));
									}
								}
							} else if (webElement.getAttribute("type").indexOf("textarea") != -1) {
								if (webElement.getAttribute("value") == null
										|| "".equals(webElement.getAttribute("value"))
										|| "null".equals(webElement.getAttribute("value"))) {
									try {
										fc.utobj().getElementByName(driver, "" + webElement.getAttribute("" + id))
												.sendKeys(dataSet.get("" + webElement.getAttribute("" + id)));
										listItems.add(dataSet.get("" + webElement.getAttribute("" + id)));
										printInfo.put("" + webElement.getAttribute("id"),
												dataSet.get("" + webElement.getAttribute("" + id)));
									} catch (Exception eText) {
										// Reporter.log("Problem in entering
										// TextArea in :
										// "+""+webElement.getAttribute("id"));
									}
								}
							} else if (webElement.getAttribute("type").indexOf("radio") != -1) {
								try {
									String sValue = "";
									List<WebElement> rdBtn_Sex = driver
											.findElements(By.id("" + webElement.getAttribute("" + id))); //
									if (alreadyradio.containsKey(webElement.getAttribute("" + id))) {

									} else {

										int size = rdBtn_Sex.size();
										if (size > 1) {
											sValue = dataSet.get("" + webElement.getAttribute("" + id));
										}
										try {
											List<WebElement> rdBtn_Field = null;
											WebElement element = null;
											try {
												rdBtn_Field = driver.findElements(By.xpath(
														".//*[@name='" + webElement.getAttribute("" + id) + "']")); // work
												element = fc.utobj().getElementByXpath(driver,
														".//*[@name='" + webElement.getAttribute("" + id) + "']");
											} catch (Exception eTxt) {
												rdBtn_Field = driver.findElements(
														By.xpath(".//*[@id='" + webElement.getAttribute("id") + "']")); // work
												element = fc.utobj().getElementByXpath(driver,
														".//*[@id='" + webElement.getAttribute("id") + "']");

											}

											fc.utobj().moveToElement(driver, element);
											fc.utobj().clickRadioButton(driver, rdBtn_Field, sValue); // work
											listItems.add(dataSet.get("" + webElement.getAttribute("" + id)));
											printInfo.put("" + webElement.getAttribute("id"),
													dataSet.get("" + webElement.getAttribute("" + id)));
											alreadyradio.put(webElement.getAttribute("" + id),
													webElement.getAttribute("" + id));
										} catch (Exception eText) {
											// Reporter.log("Problem in
											// selecting radio value in :
											// "+""+webElement.getAttribute("id"));
										}
									}
								} catch (Exception exp) {
									Reporter.log(exp.toString());
									exp.printStackTrace();
								}

							} else if (webElement.getAttribute("type").toLowerCase().indexOf("checkbox") != -1) {
								List<WebElement> oCheckBox = driver
										.findElements(By.name("" + webElement.getAttribute("" + id)));
								String sValue = "";
								if (alreadycheckBox.containsKey(webElement.getAttribute("" + id))) {

								} else {

									int size = oCheckBox.size();
									for (int i = 0; i < size; i++) {
										sValue = oCheckBox.get(i).getAttribute("value");
										//
										if (i == (size - 1)) {
											// break;
										}

									}
									try {
										List<WebElement> rdBtn_Field = driver.findElements(By.xpath(
												"//*[contains(@id, '" + webElement.getAttribute("" + id) + "')]")); // work
										WebElement element = fc.utobj().getElementByXpath(driver,
												"//*[contains(@id, '" + webElement.getAttribute("" + id) + "')]");
										fc.utobj().moveToElement(driver, element);
										fc.utobj().clickRadioButton(driver, rdBtn_Field, sValue); // work
										alreadycheckBox.put(webElement.getAttribute("" + id),
												webElement.getAttribute("" + id));
									} catch (Exception eText) {
										// Reporter.log("Problem in selecting
										// CheckBox value in :
										// "+""+webElement.getAttribute("id"));
									}
								}
							} else if (webElement.getAttribute("type").indexOf("file") != -1) {
								String fileName = fc.utobj().getFilePathFromTestData("pictureFile");
								try {
									fc.utobj().sendKeys(driver,
											fc.utobj().getElementByID(driver, "" + webElement.getAttribute("" + id)),
											fileName);
									listItems.add(fileName);
									printInfo.put("" + webElement.getAttribute("" + id), fileName);
								} catch (Exception eFile) {
									// Reporter.log("Problem in uploading file
									// type value in :
									// "+""+webElement.getAttribute("id"));
								}
							} else if (webElement.getAttribute("type").indexOf("text") != -1) {
								if (webElement.getAttribute("value") == null
										|| "".equals(webElement.getAttribute("value"))
										|| "null".equals(webElement.getAttribute("value"))
										|| "0.00".equals(webElement.getAttribute("value"))) {
									if (webElement.getAttribute("class").indexOf("fTextBoxDate") != -1) {
										try {
											if (dataSet.get("" + webElement.getAttribute("" + id)) != null
													|| !"".equals(dataSet.get("" + webElement.getAttribute("" + id)))
													|| !"null".equals(
															dataSet.get("" + webElement.getAttribute("" + id)))) {
												fc.utobj().sendKeys(driver,
														driver.findElement(
																By.name("" + webElement.getAttribute("" + id))),
														dataSet.get("" + webElement.getAttribute("" + id))); // work
												listItems.add(dataSet.get("" + webElement.getAttribute("" + id)));
												printInfo.put("" + webElement.getAttribute("" + id),
														dataSet.get("" + webElement.getAttribute("" + id)));

											} else {
												// System.out.println(""+webElement.getAttribute("id")+"===Date
												// ==="+dataSet.get(""+webElement.getAttribute("id")));
												// fc.utobj().sendKeysDateField(driver,fc.utobj().getElementByName(driver,
												// ""+webElement.getAttribute("id"))),dataSet.get(""+webElement.getAttribute("id")));
												// // work
												// fc.utobj().sendKeysDateField(driver,fc.utobj().getElementByName(driver,
												// ""+webElement.getAttribute(""+id))),fc.utobj().getCurrentDateUSFormat());
												// // work
												// listItems.add(fc.utobj().getCurrentDateUSFormat());
												// leadInfo.put(""+webElement.getAttribute(""+id),
												// fc.utobj().getCurrentDateUSFormat());

											}
										} catch (Exception eDate) {
											// Reporter.log("Problem in Entering
											// Date fields value in :
											// "+""+webElement.getAttribute("id"));
										}
									} else {
										try {
											if ("0.00".equals(webElement.getAttribute("value"))) {
												fc.utobj()
														.sendKeys(driver,
																driver.findElement(
																		By.name("" + webElement.getAttribute("" + id))),
																"");

												// System.out.println(webElement.getAttribute("value")
												// +"======"+dataSet.get(""+webElement.getAttribute(""+id))
												// +"====="+""+webElement.getAttribute(""+id));

											}
											fc.utobj().sendKeys(driver,
													fc.utobj().getElementByName(driver,
															"" + webElement.getAttribute("" + id)),
													dataSet.get("" + webElement.getAttribute("" + id)));
											if (!"ssn".equals("" + webElement.getAttribute("" + id))
													&& !"spouseSsn".equals("" + webElement.getAttribute("" + id))) {
												listItems.add(dataSet.get("" + webElement.getAttribute("" + id)));
											}
											printInfo.put("" + webElement.getAttribute("" + id),
													dataSet.get("" + webElement.getAttribute("" + id)));

										} catch (Exception eText) {
											// Reporter.log("Problem in Entering
											// Text fileds value in :
											// "+""+webElement.getAttribute("id"));
										}

									}
								}
							}
							alreadyitrateElement.put(webElement.getAttribute("" + id),
									webElement.getAttribute("" + id));
						}
					}
				}
			}

			if ("TC_FC_QA_Sale_Lead_Primary_Owner_Assignment".equals(testCaseId)) {
				try {
					fc.utobj().sleep(10000);
					List<WebElement> rdBtn_Field = driver.findElements(By.xpath("//*[@id='automatic']")); // work
					fc.utobj().sleep(10000);
					WebElement element = fc.utobj().getElementByXpath(driver, "//*[@id='automatic']");
					fc.utobj().sleep(10000);
					fc.utobj().moveToElement(driver, element);
					;
					fc.utobj().clickRadioButton(driver, rdBtn_Field, dataSet.get("automatic")); // work
					fc.utobj().sleep();

					fc.utobj().sleep(10000);
					rdBtn_Field = driver.findElements(By.xpath("//*[@id='assignAutomaticCampaign1']")); // work
					fc.utobj().sleep(10000);
					element = fc.utobj().getElementByXpath(driver, "//*[@id='assignAutomaticCampaign1']");
					fc.utobj().sleep(10000);
					fc.utobj().moveToElement(driver, element);
					;
					fc.utobj().clickRadioButton(driver, rdBtn_Field, dataSet.get("assignAutomaticCampaign1")); // work
					fc.utobj().sleep();

				} catch (Exception ee) {

				}
			}

		} catch (Exception e) {
			fc.utobj().throwsException("Fields data are not available! " + e.getMessage());
		}
		try {
			// fc.utobj().printTestStep(testCaseId, "Submiting the Tab ");
			WebElement elementbutton = fc.utobj().getElementByXpath(driver,
					".//*[@id='Submit' or @name='Submit' or @name='button']");
			fc.utobj().clickElement(driver, elementbutton);

		} catch (Exception ee) {
			fc.utobj().printBugStatus("Unable to submit page " + ee);
		}
		// fc.utobj().printTestStep(testCaseId, "verifying The fields in page
		// soruce. ");
		/*
		 * boolean isLeadInfoFound =
		 * fc.utobj().assertPageSourceWithMultipleRecords(driver, listItems);
		 * if(isLeadInfoFound==false){ fc.utobj().printBugStatus(
		 * "Either some fields not matched in page source."); }
		 */
		return printInfo;
	}

	public boolean addleadFromRestApi(WebDriver driver, Map<String, String> dataSet, String testCaseId,
			Map<String, String> UniqueKey_PrimaryInfo)
			throws IOException, Exception, ParserConfigurationException, SAXException {
		boolean primaryinfostatus = false;
		try {
			BaseRestUtil resObj = new BaseRestUtil();
			dataSet.put("clientcode", "FRANC0NN3CT_API");
			driver.navigate().to(FranconnectUtil.config.get("buildUrl") + DataServicePage.restURL);
			// UniqueKey_PrimaryInfo.put("firstName", dataSet.get("firstname") +
			// fc.utobj().generateRandomNumber());
			// UniqueKey_PrimaryInfo.put("lastName", dataSet.get("lastname") +
			// fc.utobj().generateRandomNumber());
			UniqueKey_PrimaryInfo.put("basedOnAssignmentRule", "No");
			// UniqueKey_PrimaryInfo.put("leadSource2ID", "Advertisement");
			// UniqueKey_PrimaryInfo.put("leadSource3ID", "Magazine");
			UniqueKey_PrimaryInfo.put("nextCallDate", fc.utobj().generatefutureDatewithformat("yyyy-MM-dd", 45));
			UniqueKey_PrimaryInfo.put("sendAutomaticMail", "Yes");
			UniqueKey_PrimaryInfo.put("forecastClosureDate", fc.utobj().generatefutureDatewithformat("yyyy-MM-dd", 50));
			UniqueKey_PrimaryInfo.put("basedOnWorkflowAssignmentRule", "No");
			// UniqueKey_PrimaryInfo.put("emailID", "test@franconnect.net");

			if (UniqueKey_PrimaryInfo.get("division") != null && !"null".equals(UniqueKey_PrimaryInfo.get("division"))
					&& !"".equals(UniqueKey_PrimaryInfo.get("division"))) {
				UniqueKey_PrimaryInfo.put("division", UniqueKey_PrimaryInfo.get("division"));
				UniqueKey_PrimaryInfo.put("basedOnAssignmentRule", "Yes");
			} else {

				UniqueKey_PrimaryInfo.put("division", "All");
			}

			primaryinfostatus = resObj.ActivityPerformDuplicate(driver, dataSet, UniqueKey_PrimaryInfo,
					DataServicePage.FS, DataServicePage.PrimaryInfo, DataServicePage.Create, DataServicePage.createDiv,
					testCaseId);
			if (primaryinfostatus) {
				System.out.println("Primary Info Created Successfully");
				return primaryinfostatus;
			} else {
				fc.utobj().throwsException("FS Primary Info not created on RestApi" + testCaseId);
				return primaryinfostatus;
			}
		} catch (Exception ex) {
			fc.utobj().quitBrowserOnCatch(driver, ex, testCaseId);
		}
		return primaryinfostatus;
	}

}
