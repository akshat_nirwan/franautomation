package com.builds.test.fs;

import java.util.List;
import java.util.Map;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.Reporter;
import org.testng.annotations.Optional;
import org.testng.annotations.Test;

import com.builds.uimaps.fs.AdminFranchiseSalesConfigureCoApplicantRelationshipPage;
import com.builds.utilities.FranconnectUtil;
import com.builds.utilities.TestCase;

class AdminFranchiseSalesConfigureCoApplicantRelationshipPageTest {
	FranconnectUtil fc = new FranconnectUtil();

	@Test(groups = { "sales", "TC_Admin_FS_013" }) // Verified : Akshat
	@TestCase(createdOn = "2017-05-01", updatedOn = "2017-06-06", testCaseId = "TC_Admin_FS_013", testCaseDescription = "Add CoApplicant Relationship in Admin Sales")
	private String addCoApplicantRelationship() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("sales", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		String coApplicantRelationship = dataSet.get("coApplicantRelationship");

		try {
			driver = fc.loginpage().login(driver);
			fc.utobj().printTestStep("Go to Admin CoApplicant");
			addCoApplicantRelationship(driver, coApplicantRelationship);
			fc.sales().sales_common().fsModule(driver);
			Sales fs = new Sales();
			fs.leadManagement(driver);
			FSLeadSummaryPageTest fsPage = new FSLeadSummaryPageTest();
			fsPage.clickAddLeadLink(driver);
			fc.utobj().printTestStep("Add a lead and Go to Primary Info");

			FSLeadSummaryPrimaryInfoPageTest pInfoPage = new FSLeadSummaryPrimaryInfoPageTest();
			Map<String, String> leadInfo = pInfoPage.fillLeadBasicInfo(driver);
			Reporter.log("Lead Added with lead name : " + leadInfo.get("firstName") + " " + leadInfo.get("lastName"));

			FSLeadSummaryCoApplicantsPageTest fscoapp = new FSLeadSummaryCoApplicantsPageTest();
			fc.utobj().printTestStep("Add and validate CoApplicant");
			fscoapp.fillCoapplicantDetails(driver);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
		return coApplicantRelationship;
	}

	public String addCoApplicantRelationship(WebDriver driver, @Optional() String coApplicantRelationship)
			throws Exception {

		AdminSales adsales = new AdminSales();
		adsales.adminFranchiseSalesConfigureCoApplicantRelationship(driver);
		AdminFranchiseSalesConfigureCoApplicantRelationshipPage pobj = new AdminFranchiseSalesConfigureCoApplicantRelationshipPage(
				driver);

		fc.utobj().clickElement(driver, pobj.addCoApplicantRelationship);
		fc.commonMethods().switch_cboxIframe_frameId(driver);
		coApplicantRelationship = fc.utobj().generateTestData(coApplicantRelationship);
		fc.utobj().sendKeys(driver, pobj.coApplicantRelationshipName, coApplicantRelationship);
		fc.utobj().clickElement(driver, pobj.add);

		fc.utobj().clickElement(driver, pobj.close);
		fc.utobj().inPageSource(driver, coApplicantRelationship);
		return coApplicantRelationship;
	}

	@Test(groups = { "sales", "TC_Admin_FS_014" }) // Verified : Akshat
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "TC_Admin_FS_014", testCaseDescription = "Add and Modify CoApplicant Relationship in Admin Sales")
	private void modifyCoApplicantRelationship() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("sales", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		String coApplicantRelationship = dataSet.get("coApplicantRelationship");

		try {
			driver = fc.loginpage().login(driver);
			AdminFranchiseSalesConfigureCoApplicantRelationshipPageTest p1 = new AdminFranchiseSalesConfigureCoApplicantRelationshipPageTest();
			fc.utobj().printTestStep("Go to Admin CoApplicant \n Add and validate CoApplicant Relationship");
			coApplicantRelationship = p1.addCoApplicantRelationship(driver, coApplicantRelationship);
			AdminFranchiseSalesConfigureCoApplicantRelationshipPage pobj = new AdminFranchiseSalesConfigureCoApplicantRelationshipPage(
					driver);
			List<WebElement> list = pobj.coapplicantRelationshiplisting;
			fc.utobj().actionImgOption(driver, coApplicantRelationship, "Modify");

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "sales", "fssanity1234" , "sales_failed" , "TC_Admin_FS_015" }) // Verified : Akshat
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "TC_Admin_FS_015", testCaseDescription = "Add and Delete CoApplicant Relationship in Admin Sales")
	private void deleteCoApplicantRelationship() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("sales", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		String coApplicantRelationship = dataSet.get("coApplicantRelationship");

		try {
			driver = fc.loginpage().login(driver);
			AdminFranchiseSalesConfigureCoApplicantRelationshipPageTest p1 = new AdminFranchiseSalesConfigureCoApplicantRelationshipPageTest();
			fc.utobj().printTestStep("Go to Admin CoApplicant \n Add and validate CoApplicant Relationship");
			coApplicantRelationship = p1.addCoApplicantRelationship(driver, coApplicantRelationship);
			AdminFranchiseSalesConfigureCoApplicantRelationshipPage pobj = new AdminFranchiseSalesConfigureCoApplicantRelationshipPage(
					driver);
			List<WebElement> list = pobj.coapplicantRelationshiplisting;
			fc.utobj().actionImgOption(driver, coApplicantRelationship, "Delete");
			fc.utobj().printTestStep("Delete the added CoApplicant and Validate it is not in list");
			fc.utobj().acceptAlertBox(driver);
			fc.utobj().inPageSource(driver, coApplicantRelationship);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}
}
