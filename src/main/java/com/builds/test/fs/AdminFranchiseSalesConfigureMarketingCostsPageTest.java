package com.builds.test.fs;

import java.util.Map;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.Optional;
import org.testng.annotations.Test;

import com.builds.uimaps.fs.AdminFranchiseSalesConfigureMarketingCostsPage;
import com.builds.utilities.FranconnectUtil;
import com.builds.utilities.TestCase;

class AdminFranchiseSalesConfigureMarketingCostsPageTest {

	FranconnectUtil fc = new FranconnectUtil();

	@Test(groups = { "sales", "sales_sanity" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "TC_Admin_FS_034", testCaseDescription = "Add Marketing Cost sheet in Admin Sales")

	private void addMarketingCosts() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			fc.adminpage().adminPage(driver);

			fc.utobj().printTestStep("Go to Admin Sales Marketing Cost Section \nAdd Marketing Cost");
			// fc.utobj().clickLink(driver, "Configure Marketing Costs");
			AdminFranchiseSalesConfigureMarketingCostsPage pobj = new AdminFranchiseSalesConfigureMarketingCostsPage(
					driver);
			fc.utobj().clickElement(driver, pobj.marketingCostLink);

			fc.utobj().clickElement(driver, pobj.addMarketingCost);

			Select year = new Select(pobj.costSheetYear);
			fc.utobj().selectDropDownByIndex(driver, pobj.costSheetYear, 1);

			year.getFirstSelectedOption().getText();
			fc.utobj().sendKeys(driver, pobj.costVal, "5000");
			fc.utobj().clickElement(driver, pobj.addBtn);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	public String addMarketingCosts(WebDriver driver, @Optional() String yearVal) throws Exception {

		fc.adminpage().adminPage(driver);

		// fc.utobj().clickLink(driver, "Configure Marketing Costs");
		AdminFranchiseSalesConfigureMarketingCostsPage pobj = new AdminFranchiseSalesConfigureMarketingCostsPage(
				driver);
		fc.utobj().clickElement(driver, pobj.marketingCostLink);

		fc.utobj().clickElement(driver, pobj.addMarketingCost);

		Select year = new Select(pobj.costSheetYear);
		fc.utobj().selectDropDownByIndex(driver, pobj.costSheetYear, 1);

		yearVal = year.getFirstSelectedOption().getText();
		fc.utobj().sendKeys(driver, pobj.costVal, "5000");
		fc.utobj().clickElement(driver, pobj.addBtn);
		return yearVal;
	}

	@Test(groups = { "sales" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "TC_Admin_FS_035", testCaseDescription = "Add and Modify Marketing Cost sheet in Admin Sales")
	private String modifyMarketingCosts() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("sales", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		String yearVal = dataSet.get("yearVal");

		try {
			driver = fc.loginpage().login(driver);
			fc.utobj().printTestStep("Go to Admin Sales Marketing Cost Section \nAdd Marketing Cost");
			yearVal = addMarketingCosts(driver, yearVal);

			/*
			 * WebElement obj = fc.utobj().getElementByXpath(driver,
			 * ".//a[contains(text(),yearVal)]/../following-sibling::td/div/layer/a/img"
			 * )); obj.click();
			 * fc.utobj().rightActionsMenuSelectionInMultiple(driver,"Modify");
			 */
			fc.utobj().actionImgOption(driver, yearVal, "Modify");
			AdminFranchiseSalesConfigureMarketingCostsPage pobj = new AdminFranchiseSalesConfigureMarketingCostsPage(
					driver);
			fc.utobj().getElement(driver, pobj.costVal).clear();
			fc.utobj().sendKeys(driver, pobj.costVal, "1000");
			fc.utobj().clickElement(driver, pobj.modifyBtn);
			fc.utobj().printTestStep("Modify the Added Marketing Cost");
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
		return yearVal;

	}

	@Test(groups = { "sales" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "TC_Admin_FS_036", testCaseDescription = "Add and Delete Marketing Cost sheet in Admin Sales")
	private String deleteMarketingCosts() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("sales", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		String yearVal = dataSet.get("yearVal");

		try {
			driver = fc.loginpage().login(driver);
			fc.utobj().printTestStep("Go to Admin Sales Marketing Cost Section \nAdd Marketing Cost");
			yearVal = addMarketingCosts(driver, yearVal);
			System.out.println("Value of year : " + yearVal);

			// WebElement obj =
			// fc.utobj().getElementByXpath(driver,".//a[contains(text(),'"+yearVal+"')]/../following-sibling::td/div/layer/a/img"));
			// obj.click();
			// fc.utobj().printTestStep(testCaseId, "Delete the Added Marketing
			// Cost");
			// fc.utobj().rightActionsMenuSelectionInMultiple(driver,"Delete");
			fc.utobj().actionImgOption(driver, yearVal, "Delete");
			fc.utobj().acceptAlertBox(driver);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
		return yearVal;
	}
}
