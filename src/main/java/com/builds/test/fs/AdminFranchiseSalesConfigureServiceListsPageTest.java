package com.builds.test.fs;

import java.util.List;
import java.util.Map;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import com.builds.uimaps.fs.AdminFranchiseSalesConfigureServiceListsPage;
import com.builds.utilities.FranconnectUtil;
import com.builds.utilities.TestCase;

class AdminFranchiseSalesConfigureServiceListsPageTest {
	FranconnectUtil fc = new FranconnectUtil();

	@Test(groups = { "sales", "sales_smoke" , "TC_Admin_FS_004"}) // Fixed : Verified // Akshat
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "TC_Admin_FS_004", testCaseDescription = "Add Service List in Admin Sales")
	private String addServiceList() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("sales", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		String serviceList = dataSet.get("serviceList");

		try {
			driver = fc.loginpage().login(driver);
			AdminSales adsales = new AdminSales();
			adsales.adminFranchiseSalesConfigureServiceLists(driver);

			AdminFranchiseSalesConfigureServiceListsPage pobj = new AdminFranchiseSalesConfigureServiceListsPage(
					driver);

			fc.utobj().printTestStep("Go to Admin Service List \nAdd Service List");
			fc.utobj().clickElement(driver, pobj.addService);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			serviceList = "Test" + fc.utobj().generateTestData(serviceList);
			fc.utobj().sendKeys(driver, pobj.serviceNameDetail, serviceList);
			fc.utobj().clickElement(driver, pobj.add);

			fc.utobj().clickElement(driver, pobj.close);
			fc.utobj().assertPageSource(driver, serviceList);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			 fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
		return serviceList;
	}

	public String addServiceList(WebDriver driver, String serviceList) throws Exception {
		AdminSales adsales = new AdminSales();
		adsales.adminFranchiseSalesConfigureServiceLists(driver);
		AdminFranchiseSalesConfigureServiceListsPage pobj = new AdminFranchiseSalesConfigureServiceListsPage(driver);

		fc.utobj().clickElement(driver, pobj.addService);
		fc.commonMethods().switch_cboxIframe_frameId(driver);
		serviceList = "Test" + fc.utobj().generateTestData(serviceList);
		fc.utobj().sendKeys(driver, pobj.serviceNameDetail, serviceList);
		fc.utobj().clickElement(driver, pobj.add);

		fc.utobj().clickElement(driver, pobj.close);
		fc.utobj().assertPageSource(driver, serviceList);
		return serviceList;
	}

	@Test(groups = { "sales" , "TC_Admin_FS_005" }) // Verified : Akshat
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "TC_Admin_FS_005", testCaseDescription = "Add and Modify Service List in Admin Sales")
	private void modifyServiceList() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("sales", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		String serviceList = dataSet.get("serviceList");

		try {
			driver = fc.loginpage().login(driver);
			AdminFranchiseSalesConfigureServiceListsPageTest p1 = new AdminFranchiseSalesConfigureServiceListsPageTest();
			fc.utobj().printTestStep("Go to Admin Service List \nAdd Service List");
			serviceList = p1.addServiceList(driver, serviceList);

			AdminFranchiseSalesConfigureServiceListsPage pobj = new AdminFranchiseSalesConfigureServiceListsPage(
					driver);

			fc.utobj().printTestStep("Modify and Save Added Service List");
			// fc.utobj().selectActionIcon(driver,list, serviceList,"Modify");
			fc.utobj().actionImgOption(driver, serviceList, "Modify");
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			String updatedname = pobj.serviceNameDetail.getAttribute("value") + fc.utobj().generateRandomChar();
			fc.utobj().sendKeys(driver, pobj.serviceNameDetail, updatedname);
			fc.utobj().clickElement(driver, pobj.add);

			fc.utobj().clickElement(driver, pobj.close);
			fc.utobj().assertPageSource(driver, updatedname);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "fsmodule", "fssanity" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "TC_Admin_FS_006", testCaseDescription = "Add and Delete Service List in Admin Sales")
	private void deleteServiceList() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("sales", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		String serviceList = dataSet.get("serviceList");

		try {
			driver = fc.loginpage().login(driver);

			AdminFranchiseSalesConfigureServiceListsPageTest p1 = new AdminFranchiseSalesConfigureServiceListsPageTest();
			fc.utobj().printTestStep("Go to Admin Service List \nAdd Service List");
			serviceList = p1.addServiceList(driver, serviceList);

			AdminFranchiseSalesConfigureServiceListsPage pobj = new AdminFranchiseSalesConfigureServiceListsPage(
					driver);

			List<WebElement> list = pobj.serviceNameList;

			fc.utobj().actionImgOption(driver, serviceList, "Delete");
			fc.utobj().printTestStep("Delete Service List");
			fc.utobj().acceptAlertBox(driver);

			fc.utobj().assertNotInPageSource(driver, serviceList);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}
}
