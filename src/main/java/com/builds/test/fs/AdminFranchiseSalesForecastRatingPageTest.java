package com.builds.test.fs;

import java.util.Map;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Test;

import com.builds.uimaps.fs.AdminFranchiseSalesForecastRatingPage;
import com.builds.utilities.FranconnectUtil;
import com.builds.utilities.TestCase;

class AdminFranchiseSalesForecastRatingPageTest {
	FranconnectUtil fc = new FranconnectUtil();

	@Test(groups = { "sales", "sales_smoke123" , "TC_Admin_FS_022" }) // Verified : Akshat
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "TC_Admin_FS_022", testCaseDescription = "Add ForeCast Rating in Admin Sales")
	private String addForecastRating() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("sales", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		String forecastRating = dataSet.get("forecastRating");
		try {
			driver = fc.loginpage().login(driver);
			fc.utobj().printTestStep("Go to Admin Forecast Rating Section");
			fc.utobj().printTestStep("Add ForeCast Rating");
			forecastRating = addForecastRating(driver, forecastRating);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
		return forecastRating;
	}

	private String addForecastRating(WebDriver driver, String forecastRating) throws Exception {
		boolean isValFound = false;

		AdminSales adsales = new AdminSales();
		adsales.adminFranchiseSalesForecastRating(driver);
		AdminFranchiseSalesForecastRatingPage pobj = new AdminFranchiseSalesForecastRatingPage(driver);

		fc.utobj().clickElement(driver, pobj.addForecastRating);
		fc.commonMethods().switch_cboxIframe_frameId(driver);
		forecastRating = fc.utobj().generateTestData(forecastRating);
		fc.utobj().sendKeys(driver, pobj.forecastRatingName, forecastRating);
		fc.utobj().clickElement(driver, pobj.addBtn);

		fc.utobj().clickElement(driver, pobj.closeBtn);

		fc.utobj().switchFrameToDefault(driver);

		isValFound = fc.utobj().searchInSelectBoxSingleValue(driver, pobj.listing, forecastRating);
		if (isValFound == false) {
			fc.utobj().throwsException("Forecast Rating not added!");
		}

		return forecastRating;
	}

	@Test(groups = { "sales" , "TC_Admin_FS_023" }) // Verified : Akshat
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "TC_Admin_FS_023", testCaseDescription = "Add and Modify ForeCast Rating in Admin Sales")
	private void modifyForecastRating() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());
		boolean isValFound = false;

		Map<String, String> dataSet = fc.utobj().readTestData("sales", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		String forecastRating = dataSet.get("forecastRating");

		try {
			driver = fc.loginpage().login(driver);
			fc.utobj().printTestStep("Go to Admin Forecast Rating Section \nAdd ForeCast Rating");
			forecastRating = addForecastRating(driver, forecastRating);
			AdminFranchiseSalesForecastRatingPage pobj = new AdminFranchiseSalesForecastRatingPage(driver);
			fc.utobj().selectDropDownByPartialText(driver, pobj.listing, forecastRating);
			fc.utobj().clickElement(driver, pobj.modifyBtn);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			String actualStatusName = pobj.forecastRatingName.getAttribute("value");
			pobj.forecastRatingName.clear();
			String updatedName = actualStatusName + fc.utobj().generateRandomChar();
			fc.utobj().sendKeys(driver, pobj.forecastRatingName, updatedName);
			fc.utobj().clickElement(driver, pobj.modifyBtn);
			fc.utobj().printTestStep("Modify ForeCast Rating");

			fc.utobj().clickElement(driver, pobj.closeBtn);

			isValFound = fc.utobj().searchInSelectBoxSingleValue(driver, pobj.listing, updatedName);
			if (isValFound == false) {
				fc.utobj().throwsException("Modified Forecast Rating not found!");
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "sales00012" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "TC_Admin_FS_024", testCaseDescription = "Add and Delete ForeCast Rating in Admin Sales Add and Delete ForeCast Rating in Admin Sales")
	private void deleteForecastRating() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("sales", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		String forecastRating = dataSet.get("forecastRating");

		try {
			driver = fc.loginpage().login(driver);

			AdminSales adsales = new AdminSales();
			adsales.adminFranchiseSalesForecastRating(driver);
			AdminFranchiseSalesForecastRatingPage pobj = new AdminFranchiseSalesForecastRatingPage(driver);
			fc.utobj().printTestStep("Go to Admin Forecast Rating Section");
			fc.utobj().printTestStep("Add ForeCast Rating");
			forecastRating = addForecastRating(driver, forecastRating);
			fc.utobj().selectDropDownByPartialText(driver, pobj.listing, forecastRating);
			fc.utobj().clickElement(driver, pobj.deleteBtn);
			fc.utobj().printTestStep("Delete ForeCast Rating");
			String alertText = fc.utobj().acceptAlertBox(driver);

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}

	}
}