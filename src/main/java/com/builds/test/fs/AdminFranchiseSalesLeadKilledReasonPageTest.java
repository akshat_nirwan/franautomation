package com.builds.test.fs;

import java.util.Map;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Optional;
import org.testng.annotations.Test;

import com.builds.uimaps.fs.AdminFranchiseSalesLeadKilledReasonPage;
import com.builds.utilities.FranconnectUtil;
import com.builds.utilities.TestCase;

class AdminFranchiseSalesLeadKilledReasonPageTest {
	FranconnectUtil fc = new FranconnectUtil();

	@Test(groups = { "sales", "sales_smoke" , "TC_Admin_FS_028" }) // Verified : Akshat
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "TC_Admin_FS_028", testCaseDescription = "Add Lead Killed Reason in Admin Sales")
	private String addLeadKilledReason() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("sales", testCaseId);

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		String leadKilledReason = dataSet.get("leadKilledReason");

		try {
			driver = fc.loginpage().login(driver);
			AdminSales adsales = new AdminSales();
			adsales.adminFranchiseSalesLeadKilledReason(driver);

			fc.utobj().printTestStep("Go to Admin Sales Killed Reason\nAdd killed Reason");
			leadKilledReason = addLeadKilledReason(driver, leadKilledReason);
			fc.utobj().assertPageSource(driver, leadKilledReason);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
		return leadKilledReason;
	}

	public String addLeadKilledReason(WebDriver driver, @Optional() String leadKilledReason) throws Exception {

		AdminSales adsales = new AdminSales();
		adsales.adminFranchiseSalesLeadKilledReason(driver);
		AdminFranchiseSalesLeadKilledReasonPage pobj = new AdminFranchiseSalesLeadKilledReasonPage(driver);

		try {
			fc.utobj().clickElement(driver, pobj.addLeadKilledReason);
		} catch (Exception e) {
			fc.utobj().clickElement(driver, pobj.addLeadKilledReason2fr);
		}
		fc.commonMethods().switch_cboxIframe_frameId(driver);
		leadKilledReason = fc.utobj().generateTestData(leadKilledReason);
		fc.utobj().sendKeys(driver, pobj.leadKilledReasonName, leadKilledReason);
		fc.utobj().selectDropDown(driver, pobj.leadStatusID, 1);

		fc.utobj().clickElement(driver, pobj.add);
		fc.utobj().clickElement(driver, pobj.closeBtn);
		fc.utobj().assertPageSource(driver, leadKilledReason);
		return leadKilledReason;
	}

	@Test(groups = { "sales","sales_failed" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "TC_Admin_FS_029", testCaseDescription = "Add and Modify Lead Killed Reason in Admin Sales")
	private String modifyLeadKilledReason() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("sales", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		String leadKilledReason = dataSet.get("leadKilledReason");

		try {
			driver = fc.loginpage().login(driver);
			AdminFranchiseSalesLeadKilledReasonPageTest p1 = new AdminFranchiseSalesLeadKilledReasonPageTest();
			fc.utobj().printTestStep("Go to Admin Sales Killed Reason\nAdd killed Reason");
			leadKilledReason = p1.addLeadKilledReason(driver, leadKilledReason);
			AdminFranchiseSalesLeadKilledReasonPage pobj = new AdminFranchiseSalesLeadKilledReasonPage(driver);
			WebElement element = fc.utobj().getElementByXpath(driver,
					".//td[contains(text(),'" + leadKilledReason + "')]//ancestor::tr[1]/td/input[@type='checkbox']");
			fc.utobj().clickElement(driver, element);
			fc.utobj().printTestStep("Modify killed Reason");
			fc.utobj().clickElement(driver, pobj.modifyBtn);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			String updatedName = pobj.leadKilledReasonName.getAttribute("value") + fc.utobj().generateRandomNumber();
			fc.utobj().sendKeys(driver, pobj.leadKilledReasonName, updatedName);
			fc.utobj().clickElement(driver, pobj.modifyBtn);
			fc.utobj().clickElement(driver, pobj.closeBtn);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
		return leadKilledReason;
	}

	@Test(groups = { "sales" , "TC_Admin_FS_030" }) // Verified : Akshat
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "TC_Admin_FS_030", testCaseDescription = "Add and Delete Lead Killed Reason in Admin Sales")
	private void deleteLeadKilledReason() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("sales", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		String leadKilledReason = dataSet.get("leadKilledReason");

		try {
			driver = fc.loginpage().login(driver);
			AdminFranchiseSalesLeadKilledReasonPageTest p1 = new AdminFranchiseSalesLeadKilledReasonPageTest();
			fc.utobj().printTestStep("Go to Admin Sales Killed Reason\nAdd killed Reason");
			leadKilledReason = p1.addLeadKilledReason(driver, leadKilledReason);
			AdminFranchiseSalesLeadKilledReasonPage pobj = new AdminFranchiseSalesLeadKilledReasonPage(driver);
			WebElement element = fc.utobj().getElementByXpath(driver,
					".//td[contains(text(),'" + leadKilledReason + "')]//ancestor::tr[1]/td/input[@type='checkbox']");
			fc.utobj().clickElement(driver, element);
			fc.utobj().printTestStep("Delete killed Reason");
			fc.utobj().clickElement(driver, pobj.deleteBtn);
			fc.utobj().acceptAlertBox(driver);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	public String addLeadKilledReason_All(WebDriver driver, @Optional() String leadKilledReason,
			@Optional() String leadStatus) throws Exception {
		AdminFranchiseSalesLeadStatusPageTest p3 = new AdminFranchiseSalesLeadStatusPageTest();
		leadStatus = p3.addLeadStatusWithType(driver, leadStatus, "Killed Leads");
		AdminSales adsales = new AdminSales();
		adsales.adminFranchiseSalesLeadKilledReason(driver);
		AdminFranchiseSalesLeadKilledReasonPage pobj = new AdminFranchiseSalesLeadKilledReasonPage(driver);

		fc.utobj().clickElement(driver, pobj.addLeadKilledReason);
		fc.commonMethods().switch_cboxIframe_frameId(driver);
		leadKilledReason = "No Contact " + fc.utobj().generateRandomNumber();
		fc.utobj().sendKeys(driver, pobj.leadKilledReasonName, leadKilledReason);
		fc.utobj().selectDropDown(driver, pobj.leadStatusID, leadStatus);
		fc.utobj().clickElement(driver, pobj.add);
		fc.utobj().clickElement(driver, pobj.closeBtn);
		fc.utobj().assertPageSource(driver, leadKilledReason);
		return leadKilledReason;
	}

}
