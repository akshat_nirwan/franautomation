package com.builds.test.fs;

import java.util.Map;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Optional;
import org.testng.annotations.Test;

import com.builds.uimaps.fs.AdminFranchiseSalesLeadMarketingCodePage;
import com.builds.utilities.FranconnectUtil;
import com.builds.utilities.TestCase;

class AdminFranchiseSalesLeadMarketingCodePageTest {
	FranconnectUtil fc = new FranconnectUtil();

	@Test(groups = { "sales", "sales_smoke" , "TC_Admin_FS_037" }) // Verified : Akshat
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "TC_Admin_FS_037", testCaseDescription = "Add Marketing Code in Admin Sales")
	private void addMarketingCode() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("sales", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		String marketingCode = dataSet.get("marketingCode");

		try {
			driver = fc.loginpage().login(driver);
			fc.utobj().printTestStep("Go to Sales Marketing Code Section \nAdd Marketing Code");
			marketingCode = addMarketingCode(driver, marketingCode);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	public String addMarketingCode(WebDriver driver, @Optional() String marketingCode) throws Exception {
		boolean isValFound = false;
		AdminSales adsales = new AdminSales();
		adsales.adminFranchiseSalesLeadMarketingCode(driver);
		AdminFranchiseSalesLeadMarketingCodePage pobj = new AdminFranchiseSalesLeadMarketingCodePage(driver);

		fc.utobj().clickElement(driver, pobj.addLeadMarketingCode);
		fc.commonMethods().switch_cboxIframe_frameId(driver);
		marketingCode = fc.utobj().generateTestData(marketingCode);
		fc.utobj().sendKeys(driver, pobj.leadMarketingCodeName, marketingCode);
		fc.utobj().clickElement(driver, pobj.addBtn);

		fc.utobj().clickElement(driver, pobj.closeBtn);
		isValFound = fc.utobj().searchInSelectBoxSingleValue(driver, pobj.listing, marketingCode);
		if (isValFound == false) {
			fc.utobj().throwsException("Add Marketing Code not added!");
		}
		return marketingCode;
	}

	@Test(groups = { "sales" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "TC_Admin_FS_038", testCaseDescription = "Add and Modify Marketing Code in Admin Sales")
	private void modifyMarketingCode() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("sales", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		boolean isValFound = false;
		String marketingCode = dataSet.get("marketingCode");

		try {
			driver = fc.loginpage().login(driver);
			AdminSales adsales = new AdminSales();
			adsales.adminFranchiseSalesLeadMarketingCode(driver);
			AdminFranchiseSalesLeadMarketingCodePage pobj = new AdminFranchiseSalesLeadMarketingCodePage(driver);
			AdminFranchiseSalesLeadMarketingCodePageTest p1 = new AdminFranchiseSalesLeadMarketingCodePageTest();
			fc.utobj().printTestStep("Go to Sales Marketing Code Section \nAdd Marketing Code");
			marketingCode = p1.addMarketingCode(driver, marketingCode);
			fc.utobj().selectDropDownByVisibleText(driver, pobj.listing, marketingCode);
			fc.utobj().printTestStep("Modify Marketing Code");
			fc.utobj().clickElement(driver, pobj.modifyBtn);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			String actualStatusName = pobj.leadMarketingCodeName.getAttribute("value");
			String updatedName = actualStatusName + fc.utobj().generateRandomChar();
			fc.utobj().sendKeys(driver, pobj.leadMarketingCodeName, updatedName);
			fc.utobj().clickElement(driver, pobj.modifyBtn);

			fc.utobj().clickElement(driver, pobj.closeBtn);
			isValFound = fc.utobj().searchInSelectBoxSingleValue(driver, pobj.listing, updatedName);
			if (isValFound == false) {
				fc.utobj().throwsException("Modified Marketing Code Not Added!");
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "sales" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseDescription = "Add and Delete Marketing Code in Admin Sales", testCaseId = "TC_Admin_FS_039")
	private void deleteMarketingCode() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("sales", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		String marketingCode = dataSet.get("marketingCode");

		try {
			driver = fc.loginpage().login(driver);
			AdminSales adsales = new AdminSales();
			adsales.adminFranchiseSalesLeadMarketingCode(driver);
			AdminFranchiseSalesLeadMarketingCodePage pobj = new AdminFranchiseSalesLeadMarketingCodePage(driver);
			AdminFranchiseSalesLeadMarketingCodePageTest p1 = new AdminFranchiseSalesLeadMarketingCodePageTest();
			fc.utobj().printTestStep("Go to Sales Marketing Code Section \nAdd Marketing Code");
			marketingCode = p1.addMarketingCode(driver, marketingCode);
			fc.utobj().selectDropDownByVisibleText(driver, pobj.listing, marketingCode);
			fc.utobj().printTestStep("Delete Marketing Code");
			fc.utobj().clickElement(driver, pobj.deleteBtn);
			fc.utobj().acceptAlertBox(driver);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}
}
