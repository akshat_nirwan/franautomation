package com.builds.test.fs;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Optional;
import org.testng.annotations.Test;

import com.builds.uimaps.fs.AdminFranchiseSalesLeadRatingPage;
import com.builds.utilities.FranconnectUtil;
import com.builds.utilities.TestCase;

class AdminFranchiseSalesLeadRatingPageTest {
	FranconnectUtil fc = new FranconnectUtil();

	@Test(groups = { "sales","sales098" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "TC_Sales_LeadRating_001", testCaseDescription = "Add Lead Rating in Admin Sales")
	private void addLeadRating() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		String leadRating = "Rating";

		try {
			driver = fc.loginpage().login(driver);
			fc.utobj().printTestStep("Go to Admin Lead Rating Page");
			AdminSales adsales = new AdminSales();
			adsales.adminFranchiseSalesLeadRating(driver);
			fc.utobj().printTestStep("Add lead rating");
			leadRating = fc.utobj().generateTestData(leadRating);
			leadRating = addLeadRating(driver, leadRating);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	private String addLeadRating(WebDriver driver, @Optional() String leadRating) throws Exception {
		AdminFranchiseSalesLeadRatingPage pobj = new AdminFranchiseSalesLeadRatingPage(driver);

		fc.utobj().clickElement(driver, pobj.addLeadRating);
		fc.commonMethods().switch_cboxIframe_frameId(driver);
		fc.utobj().sendKeys(driver, pobj.leadRatingName, leadRating);
		fc.utobj().clickElement(driver, pobj.addBtn);

		fc.commonMethods().Click_Close_Input_ByValue(driver);
		fc.utobj().selectDropDownByVisibleText(driver, pobj.listing, leadRating);

		return leadRating;
	}

	@Test(groups = { "sales" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2017-07-17", testCaseId = "TC_Sales_LeadRating_002", testCaseDescription = "Add and Modify Lead Rating in Admin Sales")
	private void modifyleadRating() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		boolean isValFound = false;
		String leadRating = "Rating";

		try {
			driver = fc.loginpage().login(driver);
			fc.utobj().printTestStep("Go to Admin Lead Rating Page");
			AdminSales adsales = new AdminSales();
			adsales.adminFranchiseSalesLeadRating(driver);
			AdminFranchiseSalesLeadRatingPage pobj = new AdminFranchiseSalesLeadRatingPage(driver);
			fc.utobj().printTestStep("Add lead rating");
			leadRating = fc.utobj().generateTestData(leadRating);
			leadRating = addLeadRating(driver, leadRating);
			fc.utobj().selectDropDownByVisibleText(driver, pobj.listing, leadRating);
			fc.utobj().clickElement(driver, pobj.modifyBtn);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			String actualStatusName = pobj.leadRatingName.getAttribute("value");
			String updatedName = actualStatusName + fc.utobj().generateRandomChar();
			fc.utobj().sendKeys(driver, pobj.leadRatingName, updatedName);
			fc.utobj().printTestStep("Modify Lead Rating Name and Submit");
			fc.utobj().clickElement(driver, pobj.modifyBtn);

			fc.commonMethods().Click_Close_Input_ByValue(driver);
			isValFound = fc.utobj().searchInSelectBoxSingleValue(driver, pobj.listing, updatedName);
			if (isValFound == false) {
				fc.utobj().throwsException("Modified lead Rating not found!");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "sales" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "TC_Sales_LeadRating_003", testCaseDescription = "Add And Delete Lead Rating in Admin Sales")
	private void deleteleadRating() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		String leadRating = "Rating";

		try {
			driver = fc.loginpage().login(driver);
			AdminFranchiseSalesLeadRatingPage pobj = new AdminFranchiseSalesLeadRatingPage(driver);
			fc.utobj().printTestStep("Go to Admin Lead Rating Page");
			leadRating = fc.utobj().generateTestData(leadRating);
			fc.utobj().printTestStep("Add Lead Rating");
			AdminSales adsales = new AdminSales();
			adsales.adminFranchiseSalesLeadRating(driver);
			leadRating = addLeadRating(driver, leadRating);
			fc.utobj().selectDropDownByVisibleText(driver, pobj.listing, leadRating);
			fc.utobj().printTestStep("Delete lead rating");
			fc.utobj().clickElement(driver, pobj.deleteBtn);
			fc.utobj().acceptAlertBox(driver);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "sales" })
	@TestCase(createdOn = "2017-07-17", updatedOn = "2017-07-17", testCaseId = "TC_Sales_LeadRating_004", testCaseDescription = "Verify Duplicate Lead Rating is not getting added")
	private void addDuplicateLeadRating() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		String leadRating = "Rating";

		try {
			driver = fc.loginpage().login(driver);
			fc.utobj().printTestStep("Go to Admin Lead Rating Page");
			AdminSales adsales = new AdminSales();
			adsales.adminFranchiseSalesLeadRating(driver);
			fc.utobj().printTestStep("Add lead rating");
			leadRating = fc.utobj().generateTestData(leadRating);
			leadRating = addLeadRating(driver, leadRating);

			AdminFranchiseSalesLeadRatingPage pobj = new AdminFranchiseSalesLeadRatingPage(driver);

			fc.utobj().clickElement(driver, pobj.addLeadRating);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().sendKeys(driver, pobj.leadRatingName, leadRating);
			fc.utobj().clickElement(driver, pobj.addBtn);

			try {
				String msg = fc.utobj().acceptAlertBox(driver);
				if (msg != null && msg.toLowerCase().trim().contains("Lead Rating already exists")) {
					fc.utobj().printTestStep("Check if the alert message for duplicate Lead Rating is coming.");
				}
			} catch (Exception e) {
				fc.utobj().throwsException("Alert message not present as expected.");
			}

			fc.commonMethods().Click_Close_Input_ByValue(driver);

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "sales" })
	@TestCase(createdOn = "2017-07-17", updatedOn = "2017-07-17", testCaseId = "TC_Sales_LeadRating_005", testCaseDescription = "Verify Lead Rating is not getting added with blank info")
	private void verifyLeadRatingIsMandatory() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			fc.utobj().printTestStep("Go to Admin Lead Rating Page");
			AdminSales adsales = new AdminSales();
			adsales.adminFranchiseSalesLeadRating(driver);
			fc.utobj().printTestStep("Add lead rating");

			AdminFranchiseSalesLeadRatingPage pobj = new AdminFranchiseSalesLeadRatingPage(driver);

			fc.utobj().clickElement(driver, pobj.addLeadRating);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().sendKeys(driver, pobj.leadRatingName, "");
			fc.utobj().clickElement(driver, pobj.addBtn);

			try {
				String msg = fc.utobj().acceptAlertBox(driver);
				if (msg != null && msg.toLowerCase().trim().contains("Please enter Lead Rating")) {
					fc.utobj().printTestStep("Check if the alert message for blank Lead Rating is coming.");
				}
			} catch (Exception e) {
				fc.utobj().throwsException("Alert message not present as expected.");
			}

			fc.commonMethods().Click_Close_Input_ByValue(driver);

			fc.utobj().clickElement(driver, pobj.addLeadRating);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().sendKeys(driver, pobj.leadRatingName, "    ");
			fc.utobj().clickElement(driver, pobj.addBtn);

			try {
				String msg = fc.utobj().acceptAlertBox(driver);
				if (msg != null && msg.toLowerCase().trim().contains("Please enter Lead Rating")) {
					fc.utobj().printTestStep("Check if the alert message for blank Lead Rating is coming.");
				}
			} catch (Exception e) {
				fc.utobj().throwsException("Alert message not present as expected.");
			}

			fc.commonMethods().Click_Close_Input_ByValue(driver);

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "sales" })
	@TestCase(createdOn = "2017-07-17", updatedOn = "2017-07-17", testCaseId = "TC_Sales_LeadRating_006", testCaseDescription = "Verify Lead Rating is getting added / modified with special character")
	private void verifyModifyLeadRatingWithSpecialCharacter() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		boolean isValFound = false;
		String leadRating = fc.utobj().generateTestData("A") + fc.utobj().getMajorSpecialCharacter();

		try {
			driver = fc.loginpage().login(driver);
			fc.utobj().printTestStep("Go to Admin Lead Rating Page");
			AdminSales adsales = new AdminSales();
			adsales.adminFranchiseSalesLeadRating(driver);
			AdminFranchiseSalesLeadRatingPage pobj = new AdminFranchiseSalesLeadRatingPage(driver);
			fc.utobj().printTestStep("Add lead rating with special character");
			leadRating = addLeadRating(driver, leadRating);
			fc.utobj().selectDropDownByVisibleText(driver, pobj.listing, leadRating);
			fc.utobj().printTestStep("Now click on modify button to enter the special characters");
			fc.utobj().clickElement(driver, pobj.modifyBtn);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			String actualStatusName = pobj.leadRatingName.getAttribute("value");
			String updatedName = actualStatusName + fc.utobj().getRandomChar();
			fc.utobj().sendKeys(driver, pobj.leadRatingName, updatedName);
			fc.utobj().printTestStep("Modify Lead Rating Name and Submit");
			fc.utobj().clickElement(driver, pobj.modifyBtn);

			fc.commonMethods().Click_Close_Input_ByValue(driver);
			isValFound = fc.utobj().searchInSelectBoxSingleValue(driver, pobj.listing, updatedName);
			if (isValFound == false) {
				fc.utobj().throwsException("Modified lead Rating not found!");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "sales" })
	@TestCase(createdOn = "2017-07-17", updatedOn = "2017-07-17", testCaseId = "TC_Sales_LeadRating_007", testCaseDescription = "Verify Lead Rating is getting added / deleted with special character")
	private void verifyDeleteLeadRatingWithSpecialCharacter() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		String leadRating = fc.utobj().generateTestData("A") + fc.utobj().getMajorSpecialCharacter();

		try {
			driver = fc.loginpage().login(driver);
			fc.utobj().printTestStep("Go to Admin Lead Rating Page");
			AdminSales adsales = new AdminSales();
			adsales.adminFranchiseSalesLeadRating(driver);
			AdminFranchiseSalesLeadRatingPage pobj = new AdminFranchiseSalesLeadRatingPage(driver);
			fc.utobj().printTestStep("Add lead rating with special character");
			leadRating = addLeadRating(driver, leadRating);
			fc.utobj().selectDropDownByVisibleText(driver, pobj.listing, leadRating);
			fc.utobj().printTestStep("Delete lead rating");
			fc.utobj().clickElement(driver, pobj.deleteBtn);
			String alertText = fc.utobj().acceptAlertBox(driver);

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "sales","sales_failed" , "TC_Sales_LeadRating_009" }) // Bug : Logged // Akshat
	@TestCase(createdOn = "2017-07-17", updatedOn = "2017-07-17", testCaseId = "TC_Sales_LeadRating_009", testCaseDescription = "Verify Duplicate Lead Rating is not getting added")
	private void addDuplicateLeadRatingWithSpecialCharacterTemp() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		String leadRating = "Rating";

		try {
			driver = fc.loginpage().login(driver);
			fc.utobj().printTestStep("Go to Admin Lead Rating Page");
			AdminSales adsales = new AdminSales();
			adsales.adminFranchiseSalesLeadRating(driver);
			fc.utobj().printTestStep("Add lead rating with special character");
			leadRating = fc.utobj().generateTestData(leadRating) + fc.utobj().getMajorSpecialCharacter();
			leadRating = addLeadRating(driver, leadRating);

			AdminFranchiseSalesLeadRatingPage pobj = new AdminFranchiseSalesLeadRatingPage(driver);

			fc.utobj().clickElement(driver, pobj.addLeadRating);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().sendKeys(driver, pobj.leadRatingName, leadRating);
			fc.utobj().clickElement(driver, pobj.addBtn);

			try {
				String msg = fc.utobj().acceptAlertBox(driver);
				if (msg != null && msg.toLowerCase().trim().contains("Lead Rating already exists")) {
					fc.utobj().printTestStep("Check if the alert message for duplicate Lead Rating is coming.");
				}
			} catch (Exception e) {
				fc.utobj().throwsException("Alert message not present as expected.");
			}

			fc.commonMethods().Click_Close_Input_ByValue(driver);

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			// e.printStackTrace();
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "sales" , "TC_Sales_LeadRating_008" }) // Bug Logged // Akshat
	@TestCase(createdOn = "2017-07-17", updatedOn = "2017-07-17", testCaseId = "TC_Sales_LeadRating_008", testCaseDescription = "Verify Duplicate Lead Rating is not getting added")
	private void addDuplicateLeadRatingWithSpecialCharacter() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		String leadRating = "Rating";

		try {
			driver = fc.loginpage().login(driver);
			fc.utobj().printTestStep("Go to Admin Lead Rating Page");
			AdminSales adsales = new AdminSales();
			adsales.adminFranchiseSalesLeadRating(driver);
			fc.utobj().printTestStep("Add lead rating with special character");
			leadRating = fc.utobj().generateTestData(leadRating) + fc.utobj().getMajorSpecialCharacter();
			leadRating = addLeadRating(driver, leadRating);

			AdminFranchiseSalesLeadRatingPage pobj = new AdminFranchiseSalesLeadRatingPage(driver);

			fc.utobj().clickElement(driver, pobj.addLeadRating);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().sendKeys(driver, pobj.leadRatingName, leadRating);
			fc.utobj().clickElement(driver, pobj.addBtn);

			try {
				String msg = fc.utobj().acceptAlertBox(driver);
				if (msg != null && msg.toLowerCase().trim().contains("Lead Rating already exists")) {
					fc.utobj().printTestStep("Check if the alert message for duplicate Lead Rating is coming.");
				}
			} catch (Exception e) {
				fc.utobj().throwsException("Alert message not present as expected.");
			}

			fc.commonMethods().Click_Close_Input_ByValue(driver);

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

}
