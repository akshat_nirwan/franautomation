package com.builds.test.fs;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Optional;
import org.testng.annotations.Test;

import com.builds.uimaps.fs.AdminFranchiseSalesLeadSourceCategoryPage;
import com.builds.utilities.FranconnectUtil;
import com.builds.utilities.TestCase;

class AdminFranchiseSalesLeadSourceCategoryPageTest {
	FranconnectUtil fc = new FranconnectUtil();

	@Test(groups = { "sales", "sales_source001" , "sales_failed" , "TC_Sales_Source_001" }) // Verified : Akshat
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "TC_Sales_Source_001", testCaseDescription = "Add Lead Source Category in Admin Sales")
	private void addLeadSource() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		String leadSource = fc.utobj().generateTestData("Source");

		try {
			driver = fc.loginpage().login(driver);
			AdminSales adsales = new AdminSales();
			adsales.adminFranchiseSalesLeadSourceCategory(driver);
			addLeadSource(driver, leadSource);
			fc.utobj().assertPageSource(driver, leadSource);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	public String addLeadSource(WebDriver driver, @Optional() String leadSource) throws Exception {

		AdminFranchiseSalesLeadSourceCategoryPage pobj = new AdminFranchiseSalesLeadSourceCategoryPage(driver);

		fc.utobj().clickElement(driver, pobj.addLeadSourceCategory);

		fc.commonMethods().switch_cboxIframe_frameId(driver);

		fc.utobj().sendKeys(driver, pobj.leadSource2Name, leadSource);
		fc.utobj().clickElement(driver, pobj.addBtn);

		fc.utobj().clickElement(driver, pobj.closeBtn);

		return leadSource;
	}

	@Test(groups = { "sales" , "sales_failed" , "TC_Sales_Source_002" }) // Verified : Akshat
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "TC_Sales_Source_002", testCaseDescription = "Add and Modify Lead Source Category in Admin Sales")
	private void modifyLeadSource() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		String leadSource = fc.utobj().generateTestData("Source");

		try {
			driver = fc.loginpage().login(driver);
			fc.utobj().printTestStep("Go to Sales Source Category \nAdd Source Category");
			AdminSales adsales = new AdminSales();
			adsales.adminFranchiseSalesLeadSourceCategory(driver);
			addLeadSource(driver, leadSource);
			AdminFranchiseSalesLeadSourceCategoryPage pobj = new AdminFranchiseSalesLeadSourceCategoryPage(driver);
			WebElement element = fc.utobj().getElementByXpath(driver,".//td[contains(text(),'" + leadSource + "')]//ancestor::tr[1]/td/input[@type='checkbox']");
			fc.utobj().clickElement(driver, element);
			fc.utobj().printTestStep("Modify Source Category");
			fc.utobj().clickElement(driver, pobj.modifyBtn);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			String updatedName = fc.utobj().getElement(driver, pobj.leadSource2Name).getAttribute("value")
					+ fc.utobj().generateRandomNumber();
			fc.utobj().getElement(driver, pobj.leadSource2Name).clear();
			fc.utobj().sendKeys(driver, pobj.leadSource2Name, updatedName);
			fc.utobj().clickElement(driver, pobj.modifyBtn);
			fc.utobj().clickElement(driver, pobj.closeBtn);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "sales","sales_failed" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "TC_Sales_Source_003", testCaseDescription = "Add and Delete Lead Source Category in Admin Sales")
	private void deleteLeadSource() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		String leadSource = fc.utobj().generateTestData("Source");

		try {
			driver = fc.loginpage().login(driver);
			fc.utobj().printTestStep("Go to Sales Source Category \nAdd Source Category");
			AdminSales adsales = new AdminSales();
			adsales.adminFranchiseSalesLeadSourceCategory(driver);
			leadSource = addLeadSource(driver, leadSource);
			AdminFranchiseSalesLeadSourceCategoryPage pobj = new AdminFranchiseSalesLeadSourceCategoryPage(driver);
			WebElement element = fc.utobj().getElementByXpath(driver,
					".//td[contains(text(),'"+leadSource+"')]//ancestor::tr[1]/td/input[@type='checkbox']");
			fc.utobj().clickElement(driver, element);
			fc.utobj().printTestStep("Delete Source Category");
			fc.utobj().clickElement(driver, pobj.deleteBtn);
			fc.utobj().acceptAlertBox(driver);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}
}
