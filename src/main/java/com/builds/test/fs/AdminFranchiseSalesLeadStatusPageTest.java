package com.builds.test.fs;

import java.util.Map;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Optional;
import org.testng.annotations.Test;

import com.builds.uimaps.fs.AdminSalesLeadStatusUI;
import com.builds.utilities.FranconnectUtil;
import com.builds.utilities.TestCase;

class AdminFranchiseSalesLeadStatusPageTest {
	FranconnectUtil fc = new FranconnectUtil();

	@Test(groups = { "sales_smoke", "sales", "sales_failed"})

	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "TC_Admin_FS_016", testCaseDescription = "Add Lead Status in Admin sales")
	private void addLeadStatus() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("sales", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		String leadStatus = dataSet.get("leadStatus");

		try {
			driver = fc.loginpage().login(driver);
			fc.utobj().printTestStep("Go to Sales Lead Status \nAdd Lead Status");
			leadStatus = addLeadStatus(driver, leadStatus);

			fc.utobj().printTestStep("Lead Status is added successfully");
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	public String addLeadStatus(WebDriver driver, @Optional() String leadStatus) throws Exception {
		boolean isValFound = false;
		System.out.println("addLeadStatus----------------------" + FranconnectUtil.config);

		AdminSales adsales = new AdminSales();
		adsales.adminFranchiseSalesStatusPage(driver);
		AdminSalesLeadStatusUI pobj = new AdminSalesLeadStatusUI(driver);

		fc.utobj().clickElement(driver, pobj.addLeadStatusBtn);
		fc.commonMethods().switch_cboxIframe_frameId(driver);
		leadStatus = fc.utobj().generateTestData(leadStatus);
		fc.utobj().sendKeys(driver, pobj.leadStatusName, leadStatus);
		fc.utobj().clickElement(driver, pobj.addBtn);

		fc.utobj().clickElement(driver, pobj.closeBtn);
		fc.utobj().switchFrameToDefault(driver);
		isValFound = fc.utobj().searchInSelectBoxSingleValue(driver, pobj.allStatusListing, leadStatus);
		if (isValFound == false) {
			fc.utobj().throwsException("Lead Status Not added!");
		}
		return leadStatus;
	}

	/*
	 * public String addNewLeadStatus1(WebDriver driver,Map<String,String>
	 * config) throws Exception {
	 * 
	 * String testCaseId = "TC_Admin_AddDefaultLeadStatus_001";
	 * 
	 * Map<String,String> dataSet = fc.utobj().readTestData("sales",
	 * testCaseId); String leadStatus = dataSet.get("status"); String statusFor
	 * = dataSet.get("statusFor"); boolean isValFound =false;
	 * 
	 * if(fc.utobj().validate(config,testCaseId)==true){ try{
	 * 
	 * adsales.adminFranchiseSalesStatusPage( driver);
	 * AdminFranchiseSalesLeadStatusPage pobj = new
	 * AdminFranchiseSalesLeadStatusPage(driver);
	 * fc.utobj().clickElement(driver,pobj.addLeadStatus);
	 * fc.utobj().switchFrameById(driver, "cboxIframe1");
	 * leadStatus=fc.utobj().generateTestData(leadStatus);
	 * fc.utobj().sendKeys(driver,pobj.leadStatusName,leadStatus);
	 * fc.utobj().selectDropDownByPartialText(driver, pobj.statusFor,
	 * statusFor); fc.utobj().clickElement(driver,pobj.addBtn);
	 * fc.utobj().clickElement(driver,pobj.closeBtn); isValFound =
	 * fc.utobj().searchInSelectBoxSingleValue(driver,pobj.listing, leadStatus);
	 * if(isValFound ==false){ fc.utobj().throwsException(
	 * "Lead Status Not added successfully please refer screenshot!"); }
	 * fc.utobj().printTestResultExcel(config, testCaseId, null, null,"Passed",
	 * null, null); }catch(Exception e){
	 * fc.utobj().quitBrowserOnCatchBasicMethod(driver, config, e, testCaseId);
	 * } }else{ fc.utobj().throwsSkipException(
	 * "Lead Status Not added successfully please refer screenshot!"); }
	 * 
	 * return leadStatus; }
	 */

	public String addNewLeadStatus(WebDriver driver) throws Exception {
		String testCaseId = "TC_Admin_AddDefaultLeadStatus_001";

		Map<String, String> dataSet = fc.utobj().readTestData("sales", testCaseId);
		String leadStatus = dataSet.get("status");
		String statusFor = dataSet.get("statusFor");
		boolean isValFound = false;
		AdminSales adsales = new AdminSales();
		adsales.adminFranchiseSalesStatusPage(driver);
		AdminSalesLeadStatusUI pobj = new AdminSalesLeadStatusUI(driver);

		fc.utobj().clickElement(driver, pobj.addLeadStatusBtn);
		fc.commonMethods().switch_cboxIframe_frameId(driver);
		leadStatus = fc.utobj().generateTestData(leadStatus);
		fc.utobj().sendKeys(driver, pobj.leadStatusName, leadStatus);
		fc.utobj().selectDropDownByPartialText(driver, pobj.statusFor, statusFor);
		fc.utobj().clickElement(driver, pobj.addBtn);

		fc.utobj().clickElement(driver, pobj.closeBtn);
		isValFound = fc.utobj().searchInSelectBoxSingleValue(driver, pobj.allStatusListing, leadStatus);
		if (isValFound == false) {
			fc.utobj().throwsSkipException("Lead Status Not added successfully please refer screenshot!");
		}
		return leadStatus;
	}

	@Test(groups = { "sales" , "TC_Admin_FS_017" }) // Verified : Akshat
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "TC_Admin_FS_017", testCaseDescription = "Add and Modify Lead Status in Admin sales")
	private void modifyLeadStatus() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());
		boolean isValFound = false;

		Map<String, String> dataSet = fc.utobj().readTestData("sales", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		String leadStatus = dataSet.get("leadStatus");

		try {
			driver = fc.loginpage().login(driver);
			
			fc.utobj().printTestStep("Go to Sales Lead Status \nAdd Lead Status");
			AdminSalesLeadStatusUI pobj = new AdminSalesLeadStatusUI(driver);
			leadStatus = addLeadStatus(driver, leadStatus);
			fc.utobj().selectDropDownByPartialText(driver, pobj.allStatusListing, leadStatus);
			fc.utobj().clickElement(driver, pobj.modifyBtn);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			String actualStatusName = fc.utobj().getElement(driver, pobj.leadStatusName).getAttribute("value");
			fc.utobj().getElement(driver, pobj.leadStatusName).clear();
			String updatedName = actualStatusName + fc.utobj().generateRandomChar();
			fc.utobj().sendKeys(driver, pobj.leadStatusName, updatedName);
			fc.utobj().printTestStep("Modify Lead Status");
			fc.utobj().clickElement(driver, pobj.modifyBtn);

			fc.utobj().clickElement(driver, pobj.closeBtn);
			isValFound = fc.utobj().searchInSelectBoxSingleValue(driver, pobj.allStatusListing, updatedName);
			if (isValFound == false) {
				fc.utobj().throwsException("Modified lead status not found in the list!");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "sales", "sales5564" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "TC_Admin_FS_018", testCaseDescription = "Add and Delete Lead Status in Admin sales")
	private void deleteLeadStatus() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("sales", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		String leadStatus = dataSet.get("leadStatus");

		try {
			driver = fc.loginpage().login(driver);
			AdminSalesLeadStatusUI pobj = new AdminSalesLeadStatusUI(driver);
			fc.utobj().printTestStep("Go to Sales Lead Status \nAdd Lead Status");
			leadStatus = addLeadStatus(driver, leadStatus);
			fc.utobj().selectDropDownByPartialText(driver, pobj.allStatusListing, leadStatus);
			fc.utobj().printTestStep("Delete Lead Status");
			fc.utobj().clickElement(driver, pobj.deleteBtn);
			fc.utobj().acceptAlertBox(driver);

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	public String addLeadStatusWithType(WebDriver driver, @Optional() String leadStatus,
			@Optional() String statusCategory) throws Exception {
		AdminSales adsales = new AdminSales();
		adsales.adminFranchiseSalesStatusPage(driver);
		AdminSalesLeadStatusUI pobj = new AdminSalesLeadStatusUI(driver);

		fc.utobj().clickElement(driver, pobj.addLeadStatusBtn);
		fc.commonMethods().switch_cboxIframe_frameId(driver);
		leadStatus = fc.utobj().generateTestData(leadStatus);
		fc.utobj().sendKeys(driver, pobj.leadStatusName, leadStatus);
		fc.utobj().selectDropDown(driver, pobj.statusFor, statusCategory);
		fc.utobj().clickElement(driver, pobj.addBtn);

		fc.utobj().clickElement(driver, pobj.closeBtn);

		fc.utobj().searchInSelectBoxSingleValue(driver, pobj.allStatusListing, leadStatus);
		return leadStatus;
	}
}
