package com.builds.test.fs;

import java.util.List;
import java.util.Map;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.builds.uimaps.fs.AdminFranchiseSalesManageFormGeneratorPage;
import com.builds.utilities.FranconnectUtil;
import com.relevantcodes.extentreports.ExtentTest;

public class AdminFranchiseSalesManageFormGeneratorPageTest {
	FranconnectUtil fc = new FranconnectUtil();

	private WebElement ClickonSectionPageButton(WebDriver driver, String sectionName, String Button) throws Exception {
		return fc.utobj().getElementByXpath(driver, ".//*[contains(text(),'" + sectionName
				+ "')]/following-sibling::td/a[contains(text(),'" + Button + "')]");
	}

	@Parameters({ "tabName" })
	@Test(groups = { "commonModule", "add", "Lead Status" })
	public String addTab(@Optional() String tabName) throws Exception {
		WebDriver driver = fc.loginpage().login();
		AdminSales adsales = new AdminSales();
		adsales.manageFormGenerator(driver);
		AdminFranchiseSalesManageFormGeneratorPage pobj = new AdminFranchiseSalesManageFormGeneratorPage(driver);
		fc.utobj().clickElement(driver, pobj.continueBtn);
		fc.utobj().clickElement(driver, pobj.addNewTabBtn);
		fc.commonMethods().switch_cboxIframe_frameId(driver);
		tabName = fc.utobj().generateTestData(tabName);
		fc.utobj().sendKeys(driver, pobj.displayName, tabName);
		fc.utobj().selectDropDownByPartialText(driver, pobj.submoduleNameDrp, "Lead Management");
		fc.utobj().clickElement(driver, pobj.submitBtn);
		fc.utobj().assertSingleLinkText(driver, tabName);
		return tabName;
	}

	@Parameters({ "tabName" })
	@Test(groups = { "commonModule", "add", "Lead Status" })
	public String addSection(@Optional() String tabName) throws Exception {
		WebDriver driver = fc.loginpage().login();

		AdminFranchiseSalesManageFormGeneratorPageTest p1 = new AdminFranchiseSalesManageFormGeneratorPageTest();
		tabName = p1.addTab(tabName);
		fc.utobj().getElementByLinkText(driver, tabName).click();
		String sectionName = "Section " + fc.utobj().generateRandomNumber();
		AdminFranchiseSalesManageFormGeneratorPage pobj = new AdminFranchiseSalesManageFormGeneratorPage(driver);
		String windowHandle = driver.getWindowHandle();
		fc.utobj().clickElement(driver, pobj.addSectionBtn);
		fc.commonMethods().switch_cboxIframe_frameId(driver);

		fc.utobj().sendKeys(driver, pobj.sectionName, sectionName);
		fc.utobj().clickElement(driver, pobj.submitSectionCboxBtn);
		driver.switchTo().window(windowHandle);
		return sectionName;
	}

	@Parameters({ "tabName" })
	@Test(groups = { "commonModule", "add", "Lead Status" })
	public String addNewField(@Optional() String tabName) throws Exception {
		WebDriver driver = fc.loginpage().login();

		AdminFranchiseSalesManageFormGeneratorPageTest p1 = new AdminFranchiseSalesManageFormGeneratorPageTest();
		p1.addSection(tabName);
		String fieldName = "Test Field " + fc.utobj().generateRandomNumber();
		AdminFranchiseSalesManageFormGeneratorPage pobj = new AdminFranchiseSalesManageFormGeneratorPage(driver);

		String windowHandle = driver.getWindowHandle();
		fc.utobj().clickElement(driver, pobj.addNewFieldBtn);
		fc.commonMethods().switch_cboxIframe_frameId(driver);

		fc.utobj().sendKeys(driver, pobj.fieldDisplayName, fieldName);
		fc.utobj().clickElement(driver, pobj.submitFieldCBoxButton);
		driver.switchTo().window(windowHandle);
		return fieldName;
	}

	@Parameters({ "tabName" })
	@Test(groups = { "commonModule", "add", "Lead Status" })
	public String addDocumentField(@Optional() String tabName) throws Exception {

		WebDriver driver = fc.loginpage().login();

		AdminFranchiseSalesManageFormGeneratorPageTest p1 = new AdminFranchiseSalesManageFormGeneratorPageTest();
		p1.addSection(tabName);
		String docName = "Test Field " + fc.utobj().generateRandomNumber();
		AdminFranchiseSalesManageFormGeneratorPage pobj = new AdminFranchiseSalesManageFormGeneratorPage(driver);

		String windowHandle = driver.getWindowHandle();
		fc.utobj().clickElement(driver, pobj.addDocumentBtn);
		fc.commonMethods().switch_cboxIframe_frameId(driver);

		fc.utobj().sendKeys(driver, pobj.documentTitle, docName);
		fc.utobj().sendKeys(driver, pobj.documentLabel, docName);
		fc.utobj().clickElement(driver, pobj.addDocumentCboxBtn);
		driver.switchTo().window(windowHandle);
		return docName;
	}

	public String addTabName(WebDriver driver, String tabName) throws Exception {

		AdminFranchiseSalesManageFormGeneratorPage pobj = new AdminFranchiseSalesManageFormGeneratorPage(driver);
		fc.utobj().clickElement(driver, pobj.continueBtn);
		fc.utobj().clickElement(driver, pobj.addNewTabBtn);
		fc.commonMethods().switch_cboxIframe_frameId(driver);

		tabName = fc.utobj().generateTestData(tabName);
		fc.utobj().sendKeys(driver, pobj.displayName, tabName);
		fc.utobj().selectDropDownByPartialText(driver, pobj.submoduleNameDrp, "Lead Management");
		fc.utobj().clickElement(driver, pobj.submitBtn);
		fc.utobj().assertSingleLinkText(driver, tabName);
		return tabName;
	}

	public String addCRMTabName(WebDriver driver, String tabName) throws Exception {

		AdminFranchiseSalesManageFormGeneratorPage pobj = new AdminFranchiseSalesManageFormGeneratorPage(driver);
		fc.utobj().clickElement(driver, pobj.continueBtn);
		fc.utobj().clickElement(driver, pobj.addNewTabBtn);
		fc.commonMethods().switch_cboxIframe_frameId(driver);

		tabName = fc.utobj().generateTestData(tabName);
		fc.utobj().sendKeys(driver, pobj.displayName, tabName);
		fc.utobj().clickElement(driver, pobj.submitBtn);
		driver.switchTo().defaultContent();
		return tabName;
	}

	public String addTabNameSync(WebDriver driver, String tabName) throws Exception {

		AdminFranchiseSalesManageFormGeneratorPage pobj = new AdminFranchiseSalesManageFormGeneratorPage(driver);
		fc.utobj().clickElement(driver, pobj.continueBtn);
		fc.utobj().clickElement(driver, pobj.addNewTabBtn);
		fc.commonMethods().switch_cboxIframe_frameId(driver);
		tabName = fc.utobj().generateTestData(tabName);
		fc.utobj().sendKeys(driver, pobj.displayName, tabName);
		fc.utobj().selectDropDownByPartialText(driver, pobj.submoduleNameDrp, "Lead Management");
		fc.utobj().clickElement(driver,
				fc.utobj().getElementByXpath(driver, ".//*[@name='addMore' and @value='false']"));
		fc.utobj().clickElement(driver, pobj.submitBtn);
		fc.utobj().assertSingleLinkText(driver, tabName);
		return tabName;
	}

	public String addSectionName(WebDriver driver, String sectionName, Map<String, String> dataSet) throws Exception {

		AdminFranchiseSalesManageFormGeneratorPage pobj = new AdminFranchiseSalesManageFormGeneratorPage(driver);
		// String windowHandle = driver.getWindowHandle();
		sectionName = fc.utobj().generateTestData(sectionName);
		fc.utobj().clickElement(driver, pobj.addSectionBtn);
		fc.commonMethods().switch_cboxIframe_frameId(driver);

		fc.utobj().sendKeys(driver, pobj.sectionName, sectionName);
		boolean present = false;
		try {
			if (!fc.utobj().getElementByXpath(driver, ".//*[@id='Button']").isDisplayed()) {
				fc.utobj().throwsException("Close button is not found");
			}
			List<WebElement> radio = driver.findElements(By.name("isTabularSection"));
			if (radio.get(0).getAttribute("checked") != null) {
				present = true;
			}
			if (dataSet.get("SectionType").equals("Tabular")) {
				fc.utobj().clickElement(driver,
						fc.utobj().getElementByXpath(driver, ".//*[@name='isTabularSection' and @value='yes']"));
			}
		} catch (Exception ex) {
		}
		if (!present) {
			fc.utobj().throwsException("Close button not found on Add section ");
		}
		fc.utobj().clickElement(driver, pobj.submitSectionCboxBtn);
		// driver.switchTo().window(windowHandle);
		return sectionName;
	}

	public String addSectionName(WebDriver driver, String sectionName) throws Exception {

		AdminFranchiseSalesManageFormGeneratorPage pobj = new AdminFranchiseSalesManageFormGeneratorPage(driver);
		String windowHandle = driver.getWindowHandle();
		sectionName = fc.utobj().generateTestData(sectionName);

		fc.utobj().clickElement(driver, pobj.addSectionBtn);
		fc.commonMethods().switch_cboxIframe_frameId(driver);

		fc.utobj().sendKeys(driver, pobj.sectionName, sectionName);
		boolean present = false;
		try {
			if (!fc.utobj().getElementByXpath(driver, ".//*[@id='Button']").isDisplayed()) {
				fc.utobj().throwsException("Close button is not found");
			}
			List<WebElement> radio = driver.findElements(By.name("isTabularSection"));
			if (radio.get(0).getAttribute("checked") != null) {
				present = true;
			}
		} catch (Exception ex) {
		}
		if (!present) {
			fc.utobj().throwsException("Close button not found on Add section ");
		}
		fc.utobj().clickElement(driver, pobj.submitSectionCboxBtn);
		driver.switchTo().window(windowHandle);
		return sectionName;
	}

	public String addFieldName1(WebDriver driver, String sectionName, String fieldName, Map<String, String> dataSet,
			String testCaseId) throws Exception {

		AdminFranchiseSalesManageFormGeneratorPage pobj = new AdminFranchiseSalesManageFormGeneratorPage(driver);

		String windowHandle = driver.getWindowHandle();
		try {

			fc.utobj().clickElement(driver, pobj.addNewFieldBtn);

		} catch (Exception e) {
			fc.utobj().clickElement(driver, ClickonSectionPageButton(driver, sectionName, "Add New Field"));
		}
		fc.commonMethods().switch_cboxIframe_frameId(driver);

		fc.utobj().sendKeys(driver, pobj.fieldDisplayName, fieldName);
		fc.utobj().clickElement(driver, pobj.submitFieldCBoxButton);
		driver.switchTo().defaultContent();
		driver.switchTo().window(windowHandle);
		return fieldName;

	}

	public String addFieldName(WebDriver driver, String sectionName, String fieldName, Map<String, String> dataSet,
			String testCaseId, ExtentTest test) throws Exception {

		AdminFranchiseSalesManageFormGeneratorPage pobj = new AdminFranchiseSalesManageFormGeneratorPage(driver);
		AdminFranchiseSalesManageFormGeneratorPageTest p1 = new AdminFranchiseSalesManageFormGeneratorPageTest();

		String windowHandle = driver.getWindowHandle();
		try {

			fc.utobj().clickElement(driver, pobj.addNewFieldBtn);

		} catch (Exception e) {
			fc.utobj().clickElement(driver, ClickonSectionPageButton(driver, sectionName, "Add New Field"));
		}
		// test.log(LogStatus.PASS, "Clicked ON Add New Field Button");
		boolean validfieldname = true;
		boolean validsubmit = true;
		fc.commonMethods().switch_cboxIframe_frameId(driver);

		// CheckForCommonFieldsOnFrame(driver, pobj, p1, dataSet,testCaseId);
		p1.CheckInDropDown(driver, pobj.dbColumnType, dataSet.get("dbColumnType"));
		// test.log(LogStatus.PASS, "From Select Box click on
		// "+dataSet.get("dbColumnType") );
		fc.utobj().sendKeys(driver, pobj.fieldDisplayName, fieldName);
		if (dataSet.get("dbColumnType").equals("Text")) {
			// test.log(LogStatus.PASS, "Start Validating Frame After selecting
			// "+dataSet.get("dbColumnType"));
			CheckInTextFieldFrame(driver, pobj, p1, dataSet);
			// test.log(LogStatus.PASS, "End Validating Frame After selecting
			// "+dataSet.get("dbColumnType"));
			validsubmit = false;
		}
		if (dataSet.get("dbColumnType").equals("Text Area")) {
			// test.log(LogStatus.PASS, "Start Validating Frame After selecting
			// "+dataSet.get("dbColumnType"));
			CheckInTextAreaFieldFrame(driver, pobj, dataSet, p1);
			// test.log(LogStatus.PASS, "End Validating Frame After selecting
			// "+dataSet.get("dbColumnType"));
		}

		if (dataSet.get("dbColumnType").equals("Drop-down")) {
			// test.log(LogStatus.PASS, "Start Validating Frame After selecting
			// "+dataSet.get("dbColumnType"));
			CheckInTextDropdownFrame(driver, pobj, dataSet, p1, fieldName, sectionName);
			// test.log(LogStatus.PASS, "End Validating Frame After selecting
			// "+dataSet.get("dbColumnType"));
		}

		if (dataSet.get("dbColumnType").equals("Radio")) {
			// test.log(LogStatus.PASS, "Start Validating Frame After selecting
			// "+dataSet.get("dbColumnType"));
			CheckInTextRadioFrame(driver, pobj, dataSet, p1, fieldName, sectionName);
			// test.log(LogStatus.PASS, "end Validating Frame After selecting
			// "+dataSet.get("dbColumnType"));
		}
		if (dataSet.get("dbColumnType").equals("Checkbox")) {
			// test.log(LogStatus.PASS, "Start Validating Frame After selecting
			// "+dataSet.get("dbColumnType"));
			CheckInTextCheckBoxFrame(driver, pobj, dataSet, p1, fieldName, sectionName);
			// test.log(LogStatus.PASS, "End Validating Frame After selecting
			// "+dataSet.get("dbColumnType"));
		}
		if (dataSet.get("dbColumnType").equals("Date")) {
			// test.log(LogStatus.PASS, "Start Validating Frame After selecting
			// "+dataSet.get("dbColumnType"));
			CheckInDateFrame(driver, pobj, dataSet, p1, fieldName, sectionName);
			// test.log(LogStatus.PASS, "end Validating Frame After selecting
			// "+dataSet.get("dbColumnType"));
		}
		if (dataSet.get("dbColumnType").equals("Numeric")) {
			// test.log(LogStatus.PASS, "Start Validating Frame After selecting
			// "+dataSet.get("dbColumnType"));
			CheckInNumericFrame(driver, pobj, dataSet, p1, fieldName, sectionName);
			// test.log(LogStatus.PASS, "end Validating Frame After selecting
			// "+dataSet.get("dbColumnType"));
		}
		if (dataSet.get("dbColumnType").equals("Multi Select Drop-down")) {
			// test.log(LogStatus.PASS, "Start Validating Frame After selecting
			// "+dataSet.get("dbColumnType"));
			CheckInMultiSelectDropdownFrame(driver, pobj, dataSet, p1, fieldName, sectionName);
			// test.log(LogStatus.PASS, "End Validating Frame After selecting
			// "+dataSet.get("dbColumnType"));
		}
		if (dataSet.get("dbColumnType").equals("Document")) {
			// test.log(LogStatus.PASS, "Start Validating Frame After selecting
			// "+dataSet.get("dbColumnType"));
			CheckInDocumentFrame(driver, pobj, dataSet, p1, fieldName, sectionName);
			// test.log(LogStatus.PASS, "End Validating Frame After selecting
			// "+dataSet.get("dbColumnType"));
		}
		if (!dataSet.get("dropdownfieldoptions").equals("0") && validsubmit) {

			fc.utobj().clickElement(driver, pobj.submitFieldCBoxButton);
		} else {
			validfieldname = false;
		}

		if (fieldName.equals("")) {
			// test.log(LogStatus.PASS, "Start Validating fieldname with alert
			// msg");
			validfieldname = false;
			String msg = fc.utobj().acceptAlertBox(driver);
			if (dataSet.get("dropdownoption").equals("Country") && dataSet.get("dbColumnType").equals("Drop-down")) {
				// test.log(LogStatus.PASS, "Start Validating Msg \" Please
				// enter Country's Display Name. \" ");
				if (!msg.equals("Please enter Country's Display Name.")) {
					fc.utobj().throwsException("Correct validation msg is not coming while fieldname is empty");
				}
			}

			if (dataSet.get("dropdownoption").equals("Country & State")
					&& dataSet.get("dbColumnType").equals("Drop-down")) {
				// test.log(LogStatus.PASS, "Start Validating Msg \" Please
				// enter Country's Display Name. \" ");
				if (!msg.equals("Please enter Country's Display Name.")) {
					fc.utobj().throwsException("Correct validation msg is not coming while fieldname is empty");
				}

			} else {
				// test.log(LogStatus.PASS, "Start Validating Msg \" Please
				// enter Country's Display Name. \" ");
				if (!msg.equals("Please enter Display Name.")) {
					fc.utobj().throwsException("Correct validation msg is not coming while fieldname is empty");
				}
			}

		} else {
			String spcialChar[] = { "\"", "'", "!", "@", "#", "$", "%", "^", "&", "*", "(", ")", "+", "=", "[", "]",
					";", "/", "{", "}", "|", "<", ">", "?", "_", "\\", };
			for (int i = 0; i < spcialChar.length; i++) {
				if (fieldName.startsWith(spcialChar[i]) || Character.isDigit(fieldName.charAt(0))) {
					// test.log(LogStatus.PASS, "Start Validating fieldname with
					// alert msg");
					validfieldname = false;
					String msg = fc.utobj().acceptAlertBox(driver);
					if (dataSet.get("dropdownoption").equals("Country")
							&& dataSet.get("dbColumnType").equals("Drop-down")) {
						// test.log(LogStatus.PASS, "Start Validating \" Display
						// Name Alert Msg. \" ");
						if (!msg.equals("Country's Display Name must begin with a letter.")) {
							fc.utobj().throwsException("Correct validation msg is not coming while fieldname is empty");
						}
					}

					if (dataSet.get("dropdownoption").equals("Country & State")
							&& dataSet.get("dbColumnType").equals("Drop-down")) {
						// test.log(LogStatus.PASS, "Start Validating \" Display
						// Name Alert Msg. \" ");
						if (!msg.equals("Country's Display Name must begin with a letter.")) {
							fc.utobj().throwsException("Correct validation msg is not coming while fieldname is empty");
						}

					} else if (!msg.equals("Display Name must begin with a letter.")) {
						fc.utobj().throwsException("Correct validation msg is not coming while fieldname is empty");
					}
					break;
				}

			}
		}
		if (dataSet.get("dbColumnType").equals("Drop-down")) {
			// test.log(LogStatus.PASS, "Start Validating for dropdown \"
			// Display Name Alert Msg. \" ");
			if (dataSet.get("dropdownoption").equals("Country & State")
					&& dataSet.get("dbColumnType").equals("Drop-down")) {
				if (dataSet.get("StateInDropdown").equals("")) {
					String msg = fc.utobj().acceptAlertBox(driver);
					if (!msg.equals("Please enter State's Display Name.")) {
						fc.utobj().throwsException("invalid alert msg");
					}
				}

			}

			String spcialChar[] = { "\"", "'", "!", "@", "#", "$", "%", "^", "&", "*", "(", ")", "+", "=", "[", "]",
					";", "/", "{", "}", "|", "<", ">", "?", "_", "\\", };
			for (int i = 0; i < spcialChar.length; i++) {
				if (dataSet.get("StateInDropdown").contains(spcialChar[i])
						|| Character.isDigit(dataSet.get("StateInDropdown").charAt(0))) {
					// test.log(LogStatus.PASS, "Start Validating fieldname with
					// alert msg");
					validfieldname = false;
					String msg = fc.utobj().acceptAlertBox(driver);
					if (dataSet.get("dropdownoption").equals("Country")
							&& dataSet.get("dbColumnType").equals("Drop-down")) {
						if (!msg.equals("Country's Display Name must begin with a letter.")) {
							fc.utobj().throwsException("Correct validation msg is not coming while fieldname is empty");
						}
					}

					if (dataSet.get("dropdownoption").equals("Country & State")
							&& dataSet.get("dbColumnType").equals("Drop-down")) {
						// test.log(LogStatus.PASS, "Start Validating Country's
						// Display Name with alert msg");
						if (!msg.equals("Country's Display Name must begin with a letter.")) {
							fc.utobj().throwsException("Correct validation msg is not coming while fieldname is empty");
						}

					} else if (!msg.equals("Display Name must begin with a letter.")) {
						fc.utobj().throwsException("Correct validation msg is not coming while fieldname is empty");
					}
					break;
				}

			}

		}
		if (dataSet.get("dropdownoption").equals("Country & State")
				&& dataSet.get("dbColumnType").equals("Drop-down")) {

		}
		if (dataSet.get("dbColumnType").equals("Text Area") && validfieldname) {
			int i = Integer.parseInt(dataSet.get("UsersnoOfCols"));
			if (i > 60 || i < 1) {
				String msg = fc.utobj().acceptAlertBox(driver);
				if (!msg.equals("Please enter No. of Columns between 1 and 60.")) {
					fc.utobj().throwsException("Correct Validation is not displayed");
				}
				fc.utobj().clickElement(driver, pobj.closeSectionCboxBtn);
			} else {
				i = Integer.parseInt(dataSet.get("UsersnoOfRows"));
				if (i > 5 && i < 1) {
					String msg = fc.utobj().acceptAlertBox(driver);
					if (!msg.equals("Please enter No. of Rows between 1 and 5.")) {
						fc.utobj().throwsException("Correct Validation is not displayed");
					}
					fc.utobj().clickElement(driver, pobj.closeSectionCboxBtn);
				}
			}

		}
		driver.switchTo().window(windowHandle);
		if (validfieldname) {
			// test.log(LogStatus.PASS, "valid field name check ");
			fc.utobj().switchFrameToDefault(driver);
			return "created";
		} else {
			// test.log(LogStatus.PASS, "Not a valid field name check with alert
			// msg which is working fine");
			fc.utobj().switchFrameToDefault(driver);
			return "notCreated";
		}

	}

	public String addFieldName(WebDriver driver, String sectionName, String fieldName, Map<String, String> dataSet,
			String testCaseId) throws Exception {

		AdminFranchiseSalesManageFormGeneratorPage pobj = new AdminFranchiseSalesManageFormGeneratorPage(driver);
		AdminFranchiseSalesManageFormGeneratorPageTest p1 = new AdminFranchiseSalesManageFormGeneratorPageTest();

		String windowHandle = driver.getWindowHandle();
		try {
			fc.utobj().clickElement(driver, pobj.addNewFieldBtn);
		} catch (Exception e) {
			fc.utobj().clickElement(driver, ClickonSectionPageButton(driver, sectionName, "Add New Field"));
		}
		boolean validfieldname = true;
		boolean validsubmit = true;
		fc.commonMethods().switch_cboxIframe_frameId(driver);

		// CheckForCommonFieldsOnFrame(driver, pobj, p1, dataSet,testCaseId);
		p1.CheckInDropDown(driver, pobj.dbColumnType, dataSet.get("dbColumnType"));
		fc.utobj().sendKeys(driver, pobj.fieldDisplayName, fieldName);
		if (dataSet.get("dbColumnType").equals("Text")) {
			CheckInTextFieldFrame(driver, pobj, p1, dataSet);
			validsubmit = false;
		}
		if (dataSet.get("dbColumnType").equals("Text Area")) {
			CheckInTextAreaFieldFrame(driver, pobj, dataSet, p1);
		}

		if (dataSet.get("dbColumnType").equals("Drop-down")) {
			CheckInTextDropdownFrame(driver, pobj, dataSet, p1, fieldName, sectionName);
		}

		if (dataSet.get("dbColumnType").equals("Radio")) {
			CheckInTextRadioFrame(driver, pobj, dataSet, p1, fieldName, sectionName);
		}
		if (dataSet.get("dbColumnType").equals("Checkbox")) {
			CheckInTextCheckBoxFrame(driver, pobj, dataSet, p1, fieldName, sectionName);
		}
		if (dataSet.get("dbColumnType").equals("Date")) {
			CheckInDateFrame(driver, pobj, dataSet, p1, fieldName, sectionName);
		}
		if (dataSet.get("dbColumnType").equals("Numeric")) {
			CheckInNumericFrame(driver, pobj, dataSet, p1, fieldName, sectionName);
		}
		if (dataSet.get("dbColumnType").equals("Multi Select Drop-down")) {
			CheckInMultiSelectDropdownFrame(driver, pobj, dataSet, p1, fieldName, sectionName);
		}
		if (dataSet.get("dbColumnType").equals("Document")) {
			CheckInDocumentFrame(driver, pobj, dataSet, p1, fieldName, sectionName);
		}
		if (!dataSet.get("dropdownfieldoptions").equals("0") && validsubmit) {
			fc.utobj().clickElement(driver, pobj.submitFieldCBoxButton);
		} else {
			validfieldname = false;
		}

		if (fieldName.equals("")) {
			validfieldname = false;
			String msg = fc.utobj().acceptAlertBox(driver);
			if (dataSet.get("dropdownoption").equals("Country") && dataSet.get("dbColumnType").equals("Drop-down")) {
				if (!msg.equals("Please enter Country's Display Name.")) {
					fc.utobj().throwsException("Correct validation msg is not coming while fieldname is empty");
				}
			}

			if (dataSet.get("dropdownoption").equals("Country & State")
					&& dataSet.get("dbColumnType").equals("Drop-down")) {
				if (!msg.equals("Please enter Country's Display Name.")) {
					fc.utobj().throwsException("Correct validation msg is not coming while fieldname is empty");
				}

			} else {

				if (!msg.equals("Please enter Display Name.")) {
					fc.utobj().throwsException("Correct validation msg is not coming while fieldname is empty");
				}
			}

		} else {
			String spcialChar[] = { "\"", "'", "!", "@", "#", "$", "%", "^", "&", "*", "(", ")", "+", "=", "[", "]",
					";", "/", "{", "}", "|", "<", ">", "?", "_", "\\", };
			for (int i = 0; i < spcialChar.length; i++) {
				if (fieldName.startsWith(spcialChar[i]) || Character.isDigit(fieldName.charAt(0))) {
					validfieldname = false;
					String msg = fc.utobj().acceptAlertBox(driver);
					if (dataSet.get("dropdownoption").equals("Country")
							&& dataSet.get("dbColumnType").equals("Drop-down")) {
						if (!msg.equals("Country's Display Name must begin with a letter.")) {
							fc.utobj().throwsException("Correct validation msg is not coming while fieldname is empty");
						}
					}

					if (dataSet.get("dropdownoption").equals("Country & State")
							&& dataSet.get("dbColumnType").equals("Drop-down")) {
						if (!msg.equals("Country's Display Name must begin with a letter.")) {
							fc.utobj().throwsException("Correct validation msg is not coming while fieldname is empty");
						}

					} else if (!msg.equals("Display Name must begin with a letter.")) {
						fc.utobj().throwsException("Correct validation msg is not coming while fieldname is empty");
					}
					break;
				}

			}
		}
		if (dataSet.get("dbColumnType").equals("Drop-down")) {
			if (dataSet.get("dropdownoption").equals("Country & State")
					&& dataSet.get("dbColumnType").equals("Drop-down")) {
				if (dataSet.get("StateInDropdown").equals("")) {
					String msg = fc.utobj().acceptAlertBox(driver);
					if (!msg.equals("Please enter State's Display Name.")) {
						fc.utobj().throwsException("invalid alert msg");
					}
				}

			}

			String spcialChar[] = { "\"", "'", "!", "@", "#", "$", "%", "^", "&", "*", "(", ")", "+", "=", "[", "]",
					";", "/", "{", "}", "|", "<", ">", "?", "_", "\\", };
			for (int i = 0; i < spcialChar.length; i++) {
				if (dataSet.get("StateInDropdown").contains(spcialChar[i])
						|| Character.isDigit(dataSet.get("StateInDropdown").charAt(0))) {
					validfieldname = false;
					String msg = fc.utobj().acceptAlertBox(driver);
					if (dataSet.get("dropdownoption").equals("Country")
							&& dataSet.get("dbColumnType").equals("Drop-down")) {
						if (!msg.equals("Country's Display Name must begin with a letter.")) {
							fc.utobj().throwsException("Correct validation msg is not coming while fieldname is empty");
						}
					}

					if (dataSet.get("dropdownoption").equals("Country & State")
							&& dataSet.get("dbColumnType").equals("Drop-down")) {
						if (!msg.equals("Country's Display Name must begin with a letter.")) {
							fc.utobj().throwsException("Correct validation msg is not coming while fieldname is empty");
						}

					} else if (!msg.equals("Display Name must begin with a letter.")) {
						fc.utobj().throwsException("Correct validation msg is not coming while fieldname is empty");
					}
					break;
				}

			}

		}
		if (dataSet.get("dropdownoption").equals("Country & State")
				&& dataSet.get("dbColumnType").equals("Drop-down")) {

		}
		if (dataSet.get("dbColumnType").equals("Text Area") && validfieldname) {
			int i = Integer.parseInt(dataSet.get("UsersnoOfCols"));
			if (i > 60 || i < 1) {
				String msg = fc.utobj().acceptAlertBox(driver);
				if (!msg.equals("Please enter No. of Columns between 1 and 60.")) {
					fc.utobj().throwsException("Correct Validation is not displayed");
				}
				fc.utobj().clickElement(driver, pobj.closeSectionCboxBtn);
			} else {
				i = Integer.parseInt(dataSet.get("UsersnoOfRows"));
				if (i > 5 && i < 1) {
					String msg = fc.utobj().acceptAlertBox(driver);
					if (!msg.equals("Please enter No. of Rows between 1 and 5.")) {
						fc.utobj().throwsException("Correct Validation is not displayed");
					}
					fc.utobj().clickElement(driver, pobj.closeSectionCboxBtn);
				}
			}

		}
		driver.switchTo().window(windowHandle);
		if (validfieldname) {
			return "created";
		} else {
			return "notCreated";
		}

	}

	private void CheckInDocumentFrame(WebDriver driver, AdminFranchiseSalesManageFormGeneratorPage pobj,
			Map<String, String> dataSet, AdminFranchiseSalesManageFormGeneratorPageTest p1, String fieldName,
			String sectionName) throws Exception {

		p1.CheckInDropDown(driver, pobj.dbColumnType, fieldName);
		List<WebElement> radio = driver.findElements(By.name("exportable"));
		if (!radio.get(1).isDisplayed()) {
			fc.utobj().throwsException("Is Exportable and Searchable? is not having no option");
		}
		if (radio.get(0).getAttribute("checked") != null) {

		} else {
			fc.utobj().throwsException("By Default Yes hasn't been selected for Is Exportable and Searchable?");
		}
	}

	private void CheckInMultiSelectDropdownFrame(WebDriver driver, AdminFranchiseSalesManageFormGeneratorPage pobj,
			Map<String, String> dataSet, AdminFranchiseSalesManageFormGeneratorPageTest p1, String fieldName,
			String sectionName) throws Exception {

		p1.CheckInDropDown(driver, pobj.dbColumnType, "Multi Select Drop-down");
		List<WebElement> ddradio = driver.findElements(By.name("dropdownoption"));
		if (!ddradio.get(3).isDisplayed()) {
			fc.utobj().throwsException("Field Type is not having Dependent option");
		}
		if (ddradio.get(0).getAttribute("checked") != null) {

		} else {
			fc.utobj().throwsException("By Default New hasn't been selected for Drop-down Field Type");
		}

		if (dataSet.get("dropdownoption").equals("New")) {
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//*[@name='dropdownoption' and @value='0']"));
			int i = Integer.parseInt(dataSet.get("dropdownfieldoptions"));
			String[] str = dataSet.get("OptionName").split(",");
			if (i > 1) {
				int j = 1;
				while (i > 1) {

					fc.utobj().sendKeys(driver,
							fc.utobj().getElementByXpath(driver,
									".//*[@id='row_" + String.valueOf(j) + "']/td[2]/input[1]"),
							str[i % 7] + fc.utobj().generateRandomNumber6Digit());

					if (j != 1 && !fc.utobj()
							.getElementByXpath(driver, ".//*[@id='removeTD" + String.valueOf(j) + "']/a/img")
							.isDisplayed()) {
						fc.utobj().throwsException("Remove options button is not coming for some options");
					}
					if (j == 1 && driver.findElements(By.xpath(".//*[@id='removeTD" + String.valueOf(j) + "']/a/img"))
							.size() > 0) {
						fc.utobj().throwsException("Remove options button is coming for options 1");
					}
					if (!fc.utobj()
							.getElementByXpath(driver,
									".//*[@id='row_" + String.valueOf(j) + "']//span[@class='urgent_fields']")
							.isDisplayed()) {
						fc.utobj().throwsException("Mandatory field symbol is not coming");
					}

					fc.utobj().clickElement(driver,
							fc.utobj().getElementByXpath(driver, ".//*[@id='removeTDForAddMore']/a/span"));
					j++;
					i--;
				}
				fc.utobj().sendKeys(driver,
						fc.utobj().getElementByXpath(driver,
								".//*[@id='row_" + String.valueOf(j) + "']/td[2]/input[1]"),
						str[i % 7] + fc.utobj().generateRandomNumber6Digit());
				fc.utobj().clickElement(driver,
						fc.utobj().getElementByXpath(driver, ".//*[@id='removeTD" + String.valueOf(j) + "']/a/img"));
				if (driver.findElements(By.xpath(".//*[@id='row_" + String.valueOf(j) + "']/td[2]/input[1]"))
						.size() > 0) {
					fc.utobj().throwsException(
							"Options is not getting deleted by clicking on delete option kindly check");
				}
			} else {
				fc.utobj().sendKeys(driver,
						fc.utobj().getElementByXpath(driver,
								".//*[@id='row_" + String.valueOf(i) + "']/td[2]/input[1]"),
						str[i % 7] + fc.utobj().generateRandomNumber6Digit());
			}

		}
		if (dataSet.get("dropdownoption").equals("Dependent")) {
			boolean newfieldadded = true;
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//*[@name='dropdownoption' and @value='0']"));
			int i = Integer.parseInt(dataSet.get("dropdownfieldoptions"));
			String[] str = dataSet.get("OptionName").split(",");
			if (i > 1) {
				int j = 1;
				while (i > 1) {

					fc.utobj().sendKeys(driver,
							fc.utobj().getElementByXpath(driver,
									".//*[@id='row_" + String.valueOf(j) + "']/td[2]/input[1]"),
							str[i % 7] + fc.utobj().generateRandomNumber6Digit());

					if (j != 1 && !fc.utobj()
							.getElementByXpath(driver, ".//*[@id='removeTD" + String.valueOf(j) + "']/a/img")
							.isDisplayed()) {
						fc.utobj().throwsException("Remove options button is not coming for some options");
					}
					if (j == 1 && driver.findElements(By.xpath(".//*[@id='removeTD" + String.valueOf(j) + "']/a/img"))
							.size() > 0) {
						fc.utobj().throwsException("Remove options button is coming for options 1");
					}
					if (!driver
							.findElement(
									By.xpath(".//*[@id='row_" + String.valueOf(j) + "']//span[@class='urgent_fields']"))
							.isDisplayed()) {
						fc.utobj().throwsException("Mandatory field symbol is not coming");
					}

					fc.utobj().clickElement(driver,
							fc.utobj().getElementByXpath(driver, ".//*[@id='removeTDForAddMore']/a/span"));
					j++;
					i--;
				}
				fc.utobj().sendKeys(driver,
						fc.utobj().getElementByXpath(driver,
								".//*[@id='row_" + String.valueOf(j) + "']/td[2]/input[1]"),
						str[i % 7] + fc.utobj().generateRandomNumber6Digit());
				fc.utobj().clickElement(driver,
						fc.utobj().getElementByXpath(driver, ".//*[@id='removeTD" + String.valueOf(j) + "']/a/img"));
				if (driver.findElements(By.xpath(".//*[@id='row_" + String.valueOf(j) + "']/td[2]/input[1]"))
						.size() > 0) {
					fc.utobj().throwsException(
							"Options is not getting deleted by clicking on delete option kindly check");
				}
			} else {
				fc.utobj().sendKeys(driver,
						fc.utobj().getElementByXpath(driver,
								".//*[@id='row_" + String.valueOf(i) + "']/td[2]/input[1]"),
						str[i % 7] + fc.utobj().generateRandomNumber6Digit());
			}

			fc.utobj().clickElement(driver, pobj.submitFieldCBoxButton);
			if (fieldName == "") {
				String msg = fc.utobj().acceptAlertBox(driver);
				newfieldadded = false;
				if (!msg.equals("Please enter Display Name.")) {
					fc.utobj().throwsException("Correct validation msg is not coming while fieldname is empty");
				}

			}
			String spcialChar[] = { "\"", "'", "!", "@", "#", "$", "%", "^", "&", "*", "(", ")", "+", "=", "[", "]",
					";", "/", "{", "}", "|", "<", ">", "?", "_", "\\", };
			for (i = 0; i < spcialChar.length; i++) {
				if (fieldName.startsWith(spcialChar[i]) || Character.isDigit(fieldName.charAt(0))) {
					newfieldadded = false;
					String msg = fc.utobj().acceptAlertBox(driver);
					if (!msg.equals("Display Name must begin with a letter.")) {
						fc.utobj().throwsException("Correct validation msg is not coming while fieldname is empty");
					}
					break;
				}
			}
			if (newfieldadded) {

				fc.utobj().clickElement(driver, ClickonSectionPageButton(driver, sectionName, "Add New Field"));
				fc.commonMethods().switch_cboxIframe_frameId(driver);

				p1.CheckInDropDown(driver, pobj.dbColumnType, "Multi Select Drop-down");
				fc.utobj().clickElement(driver,
						fc.utobj().getElementByXpath(driver, ".//*[@name='dropdownoption' and @value='3']"));
				String parentFieldName = fieldName;
				fieldName = dataSet.get("DependentField");
				fc.utobj().sendKeys(driver, pobj.fieldDisplayName, fieldName);
				fc.utobj().selectDropDownByVisibleText(driver, pobj.ParentdependentField, parentFieldName);

				if (!fc.utobj().getElement(driver, pobj.closeSectionCboxBtn).isDisplayed()) {
					fc.utobj().throwsException("Cancel button is not displayable ");
				}
				i = Integer.parseInt(dataSet.get("dropdownfieldoptions"));
				if (i > 1) {
					int j = 1;
					while (i > 1) {
						fc.utobj().sendKeys(driver,
								driver.findElement(
										By.xpath(".//*[@id='row_" + String.valueOf(j) + "']/td[2]/input[1]")),
								str[i % 7] + fc.utobj().generateRandomNumber6Digit());
						fc.utobj().clickElement(driver,
								fc.utobj().getElementByXpath(driver, ".//*[@id='removeTDForAddMore']/a/span"));
						i--;
						j++;
					}
					fc.utobj().sendKeys(driver,
							fc.utobj().getElementByXpath(driver,
									".//*[@id='row_" + String.valueOf(j) + "']/td[2]/input[1]"),
							str[i % 7] + fc.utobj().generateRandomNumber6Digit());
					fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
							".//*[@id='removeTD" + String.valueOf(j) + "']/a/img"));
					if (fc.utobj().getElementByXpath(driver, ".//*[@id='row_" + String.valueOf(j) + "']/td[2]/input[1]")
							.isDisplayed()) {
						fc.utobj().throwsException(
								"Options is not getting deleted by clicking on delete option kindly check");
					}
				} else {
					fc.utobj().sendKeys(driver,
							fc.utobj().getElementByXpath(driver,
									".//*[@id='row_" + String.valueOf(i) + "']/td[2]/input[1]"),
							str[i % 7] + fc.utobj().generateRandomNumber6Digit());

				}
			}

		}
	}

	private void CheckInDateFrame(WebDriver driver, AdminFranchiseSalesManageFormGeneratorPage pobj,
			Map<String, String> dataSet, AdminFranchiseSalesManageFormGeneratorPageTest p1, String fieldName,
			String sectionName) throws Exception {
		p1.CheckInDropDown(driver, pobj.dbColumnType, fieldName);
		List<WebElement> radio = driver.findElements(By.name("exportable"));
		if (!radio.get(1).isDisplayed()) {
			fc.utobj().throwsException("Is Exportable and Searchable? is not having no option");
		}
		if (radio.get(0).getAttribute("checked") != null) {

		} else {
			fc.utobj().throwsException("By Default Yes hasn't been selected for Is Exportable and Searchable?");
		}
	}

	private void CheckInNumericFrame(WebDriver driver, AdminFranchiseSalesManageFormGeneratorPage pobj,
			Map<String, String> dataSet, AdminFranchiseSalesManageFormGeneratorPageTest p1, String fieldName,
			String sectionName) throws Exception {
		p1.CheckInDropDown(driver, pobj.dbColumnType, fieldName);
		List<WebElement> radio = driver.findElements(By.name("exportable"));
		if (!radio.get(1).isDisplayed()) {
			fc.utobj().throwsException("Is Exportable and Searchable? is not having no option");
		}
		if (radio.get(0).getAttribute("checked") != null) {

		} else {
			fc.utobj().throwsException("By Default Yes hasn't been selected for Is Exportable and Searchable?");
		}
		fc.utobj().selectDropDownByVisibleText(driver, pobj.numericValidation, dataSet.get("numericValidation"));
	}

	private void CheckInTextRadioFrame(WebDriver driver, AdminFranchiseSalesManageFormGeneratorPage pobj,
			Map<String, String> dataSet, AdminFranchiseSalesManageFormGeneratorPageTest p1, String fieldName,
			String sectionName) throws Exception {
		p1.CheckInDropDown(driver, pobj.dbColumnLength, "Radio");
		boolean validfieldname = true;
		List<WebElement> radiodd = driver.findElements(By.name("radioOption"));
		if (!radiodd.get(1).isDisplayed()) {
			fc.utobj().throwsException("Field Type is not having Dependent");
		}
		if (radiodd.get(0).getAttribute("checked") != null) {

		} else {
			fc.utobj().throwsException("By Default New hasn't been selected for Drop-down Field Type");
		}
		if (dataSet.get("radioOption").equals("New")) {
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//*[@name='radioOption' and @value='0']"));
			List<WebElement> fieldviewRad = driver.findElements(By.name("optviewtype"));
			if (!fieldviewRad.get(1).isDisplayed()) {
				fc.utobj().throwsException("Field view is not having 2nd option");
			}
			if (fieldviewRad.get(0).getAttribute("checked") != null) {

			} else {
				fc.utobj().throwsException("By Default Horizontal hasn't been selected for Drop-down Field Type");
			}
			int i = Integer.parseInt(dataSet.get("dropdownfieldoptions"));
			String[] str = dataSet.get("OptionName").split(",");
			if (i > 1) {
				int j = 1;
				while (i > 1) {

					fc.utobj().sendKeys(driver,
							fc.utobj().getElementByXpath(driver,
									".//*[@id='row_" + String.valueOf(j) + "']/td[2]/input[1]"),
							str[i % 7] + fc.utobj().generateRandomNumber6Digit());

					if (j != 1 && !fc.utobj()
							.getElementByXpath(driver, ".//*[@id='removeTD" + String.valueOf(j) + "']/a/img")
							.isDisplayed()) {
						fc.utobj().throwsException("Remove options button is not coming for some options");
					}
					if (j == 1 && driver.findElements(By.xpath(".//*[@id='removeTD" + String.valueOf(j) + "']/a/img"))
							.size() > 0) {
						fc.utobj().throwsException("Remove options button is coming for options 1");
					}
					if (!driver
							.findElement(
									By.xpath(".//*[@id='row_" + String.valueOf(j) + "']//span[@class='urgent_fields']"))
							.isDisplayed()) {
						fc.utobj().throwsException("Mandatory field symbol is not coming");
					}

					fc.utobj().clickElement(driver,
							fc.utobj().getElementByXpath(driver, ".//*[@id='removeTDForAddMore']/a/span"));
					j++;
					i--;
				}

				fc.utobj().sendKeys(driver,
						fc.utobj().getElementByXpath(driver,
								".//*[@id='row_" + String.valueOf(j) + "']/td[2]/input[1]"),
						str[i % 7] + fc.utobj().generateRandomNumber6Digit());
				fc.utobj().clickElement(driver,
						fc.utobj().getElementByXpath(driver, ".//*[@id='removeTD" + String.valueOf(j) + "']/a/img"));
				if (driver.findElements(By.xpath(".//*[@id='row_" + String.valueOf(j) + "']/td[2]/input[1]"))
						.size() > 0) {
					fc.utobj().throwsException(
							"Options is not getting deleted by clicking on delete option kindly check");
				}
			} else {
				if (i == 0) {
					validfieldname = false;
					fc.utobj().clickElement(driver, pobj.submitFieldCBoxButton);
					String msg = fc.utobj().acceptAlertBox(driver);
					if (!msg.equals("Please enter Option 1.")) {
						fc.utobj().throwsException("Proper Alert Msg not came while blank optionfield");
					}
				} else {
					fc.utobj().sendKeys(driver,
							fc.utobj().getElementByXpath(driver,
									".//*[@id='row_" + String.valueOf(i) + "']/td[2]/input[1]"),
							str[i % 7] + fc.utobj().generateRandomNumber6Digit());
				}
			}

		}

		if (dataSet.get("radioOption").equals("Dependent")) {
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//*[@name='radioOption' and @value='0']"));
			List<WebElement> fieldviewRad = driver.findElements(By.name("optviewtype"));
			if (!fieldviewRad.get(1).isDisplayed()) {
				fc.utobj().throwsException("Field view is not having 2nd option");
			}
			if (fieldviewRad.get(0).getAttribute("checked") != null) {

			} else {
				fc.utobj().throwsException("By Default Horizontal hasn't been selected for Drop-down Field Type");
			}
			int i = Integer.parseInt(dataSet.get("dropdownfieldoptions"));
			String[] str = dataSet.get("OptionName").split(",");
			if (i > 1) {
				int j = 1;
				while (i > 1) {

					fc.utobj().sendKeys(driver,
							fc.utobj().getElementByXpath(driver,
									".//*[@id='row_" + String.valueOf(j) + "']/td[2]/input[1]"),
							str[i % 7] + fc.utobj().generateRandomNumber6Digit());

					if (j != 1 && !fc.utobj()
							.getElementByXpath(driver, ".//*[@id='removeTD" + String.valueOf(j) + "']/a/img")
							.isDisplayed()) {
						fc.utobj().throwsException("Remove options button is not coming for some options");
					}
					if (j == 1 && driver.findElements(By.xpath(".//*[@id='removeTD" + String.valueOf(j) + "']/a/img"))
							.size() > 0) {
						fc.utobj().throwsException("Remove options button is coming for options 1");
					}
					if (!driver
							.findElement(
									By.xpath(".//*[@id='row_" + String.valueOf(j) + "']//span[@class='urgent_fields']"))
							.isDisplayed()) {
						fc.utobj().throwsException("Mandatory field symbol is not coming");
					}

					fc.utobj().clickElement(driver,
							fc.utobj().getElementByXpath(driver, ".//*[@id='removeTDForAddMore']/a/span"));
					j++;
					i--;
				}
				fc.utobj().sendKeys(driver,
						fc.utobj().getElementByXpath(driver,
								".//*[@id='row_" + String.valueOf(j) + "']/td[2]/input[1]"),
						str[i % 7] + fc.utobj().generateRandomNumber6Digit());
				fc.utobj().clickElement(driver,
						fc.utobj().getElementByXpath(driver, ".//*[@id='removeTD" + String.valueOf(j) + "']/a/img"));
				if (driver.findElements(By.xpath(".//*[@id='row_" + String.valueOf(j) + "']/td[2]/input[1]"))
						.size() > 0) {
					fc.utobj().throwsException(
							"Options is not getting deleted by clicking on delete option kindly check");
				}
			} else {
				fc.utobj().sendKeys(driver,
						fc.utobj().getElementByXpath(driver,
								".//*[@id='row_" + String.valueOf(i) + "']/td[2]/input[1]"),
						str[i % 7] + fc.utobj().generateRandomNumber6Digit());
			}

			fc.utobj().clickElement(driver, pobj.submitFieldCBoxButton);
			if (fieldName.equals("")) {
				validfieldname = false;
				String msg = fc.utobj().acceptAlertBox(driver);
				if (!msg.equals("Please enter Display Name.")) {
					fc.utobj().throwsException("Correct validation msg is not coming while fieldname is empty");
				}

			} else {
				String spcialChar[] = { "\"", "'", "!", "@", "#", "$", "%", "^", "&", "*", "(", ")", "+", "=", "[", "]",
						";", "/", "{", "}", "|", "<", ">", "?", "_", "\\", };
				for (i = 0; i < spcialChar.length; i++) {
					if (fieldName.contains(spcialChar[i]) || Character.isDigit(fieldName.charAt(0))) {
						String msg = fc.utobj().acceptAlertBox(driver);
						validfieldname = false;
						if (!msg.equals("Display Name must begin with a letter.")) {
							fc.utobj().throwsException("Correct validation msg is not coming while fieldname is empty");
						}
						break;
					}
				}
			}

			if (validfieldname) {
				fc.utobj().clickElement(driver, ClickonSectionPageButton(driver, sectionName, "Add New Field"));
				fc.commonMethods().switch_cboxIframe_frameId(driver);

				p1.CheckInDropDown(driver, pobj.dbColumnType, "Radio");
				String parentFieldName = fieldName;
				fieldName = dataSet.get("DependentField") + "On" + fieldName;
				fc.utobj().sendKeys(driver, pobj.fieldDisplayName, fieldName);
				fc.utobj().clickElement(driver,
						fc.utobj().getElementByXpath(driver, ".//*[@name='radioOption' and @value='1']"));
				Select select = new Select(pobj.ParentdependentField);
				String value = select.getFirstSelectedOption().getText();
				if (!value.equals("Select")) {
					fc.utobj().throwsException("Default Select is not Selected for Parent Field");
				}
				fc.utobj().selectDropDownByVisibleText(driver, pobj.ParentdependentField, parentFieldName);
				List<WebElement> fieldviewRadio = driver.findElements(By.name("optviewtype"));
				if (!fieldviewRadio.get(1).isDisplayed()) {
					fc.utobj().throwsException("Field view is not having 2nd option");
				}
				fieldviewRadio.get(0).getAttribute("checked");
				if (fieldviewRadio.get(0).getAttribute("checked") != null) {

				} else {
					fc.utobj().throwsException("By Default Horizontal hasn't been selected for Drop-down Field Type");
				}
				i = Integer.parseInt(dataSet.get("dropdownfieldoptions"));
				if (i > 1) {
					int j = 1;
					while (i > 1) {

						fc.utobj().sendKeys(driver,
								driver.findElement(
										By.xpath(".//*[@id='row_" + String.valueOf(j) + "']/td[2]/input[1]")),
								str[i % 7] + fc.utobj().generateRandomNumber6Digit());

						if (j != 1 && !fc.utobj()
								.getElementByXpath(driver, ".//*[@id='removeTD" + String.valueOf(j) + "']/a/img")
								.isDisplayed()) {
							fc.utobj().throwsException("Remove options button is not coming for some options");
						}
						if (j == 1
								&& driver.findElements(By.xpath(".//*[@id='removeTD" + String.valueOf(j) + "']/a/img"))
										.size() > 0) {
							fc.utobj().throwsException("Remove options button is coming for options 1");
						}
						if (!driver
								.findElement(By.xpath(
										".//*[@id='row_" + String.valueOf(j) + "']//span[@class='urgent_fields']"))
								.isDisplayed()) {
							fc.utobj().throwsException("Mandatory field symbol is not coming");
						}

						fc.utobj().clickElement(driver,
								fc.utobj().getElementByXpath(driver, ".//*[@id='removeTDForAddMore']/a/span"));
						j++;
						i--;
					}
					fc.utobj().sendKeys(driver,
							fc.utobj().getElementByXpath(driver,
									".//*[@id='row_" + String.valueOf(j) + "']/td[2]/input[1]"),
							str[i % 7] + fc.utobj().generateRandomNumber6Digit());
					fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
							".//*[@id='removeTD" + String.valueOf(j) + "']/a/img"));
					if (driver.findElements(By.xpath(".//*[@id='row_" + String.valueOf(j) + "']/td[2]/input[1]"))
							.size() > 0) {
						fc.utobj().throwsException(
								"Options is not getting deleted by clicking on delete option kindly check");
					}
				} else {
					fc.utobj().sendKeys(driver,
							fc.utobj().getElementByXpath(driver,
									".//*[@id='row_" + String.valueOf(i) + "']/td[2]/input[1]"),
							str[i % 7] + fc.utobj().generateRandomNumber6Digit());
				}

			}
		}
	}

	private void CheckInTextCheckBoxFrame(WebDriver driver, AdminFranchiseSalesManageFormGeneratorPage pobj,
			Map<String, String> dataSet, AdminFranchiseSalesManageFormGeneratorPageTest p1, String fieldName,
			String sectionName) throws Exception {
		p1.CheckInDropDown(driver, pobj.dbColumnLength, "Checkbox");
		boolean validfieldname = true;
		List<WebElement> radiodd = driver.findElements(By.name("checkboxOption"));
		if (!radiodd.get(1).isDisplayed()) {
			fc.utobj().throwsException("Field Type is not having Dependent");
		}
		if (radiodd.get(0).getAttribute("checked") != null) {

		} else {
			fc.utobj().throwsException("By Default New hasn't been selected for Drop-down Field Type");
		}
		if (dataSet.get("checkboxOption").equals("New")) {
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//*[@name='checkboxOption' and @value='0']"));
			List<WebElement> fieldviewRad = driver.findElements(By.name("optviewtype"));
			if (!fieldviewRad.get(1).isDisplayed()) {
				fc.utobj().throwsException("Field view is not having 2nd option");
			}
			if (fieldviewRad.get(0).getAttribute("checked") != null) {

			} else {
				fc.utobj().throwsException("By Default Horizontal hasn't been selected for Drop-down Field Type");
			}
			int i = Integer.parseInt(dataSet.get("dropdownfieldoptions"));
			String[] str = dataSet.get("OptionName").split(",");
			if (i > 1) {
				int j = 1;
				while (i > 1) {

					fc.utobj().sendKeys(driver,
							fc.utobj().getElementByXpath(driver,
									".//*[@id='row_" + String.valueOf(j) + "']/td[2]/input[1]"),
							str[i % 7] + fc.utobj().generateRandomNumber6Digit());

					if (j != 1 && !fc.utobj()
							.getElementByXpath(driver, ".//*[@id='removeTD" + String.valueOf(j) + "']/a/img")
							.isDisplayed()) {
						fc.utobj().throwsException("Remove options button is not coming for some options");
					}
					if (j == 1 && driver.findElements(By.xpath(".//*[@id='removeTD" + String.valueOf(j) + "']/a/img"))
							.size() > 0) {
						fc.utobj().throwsException("Remove options button is coming for options 1");
					}
					if (!driver
							.findElement(
									By.xpath(".//*[@id='row_" + String.valueOf(j) + "']//span[@class='urgent_fields']"))
							.isDisplayed()) {
						fc.utobj().throwsException("Mandatory field symbol is not coming");
					}

					fc.utobj().clickElement(driver,
							fc.utobj().getElementByXpath(driver, ".//*[@id='removeTDForAddMore']/a/span"));
					j++;
					i--;
				}

				fc.utobj().sendKeys(driver,
						fc.utobj().getElementByXpath(driver,
								".//*[@id='row_" + String.valueOf(j) + "']/td[2]/input[1]"),
						str[i % 7] + fc.utobj().generateRandomNumber6Digit());
				fc.utobj().clickElement(driver,
						fc.utobj().getElementByXpath(driver, ".//*[@id='removeTD" + String.valueOf(j) + "']/a/img"));
				if (driver.findElements(By.xpath(".//*[@id='row_" + String.valueOf(j) + "']/td[2]/input[1]"))
						.size() > 0) {
					fc.utobj().throwsException(
							"Options is not getting deleted by clicking on delete option kindly check");
				}
			} else {
				if (i == 0) {

					fc.utobj().clickElement(driver, pobj.submitFieldCBoxButton);
					String msg = fc.utobj().acceptAlertBox(driver);
					if (!msg.equals("Please enter Option 1.")) {
						fc.utobj().throwsException("Proper Alert Msg not came while blank optionfield");
					}
				} else {
					fc.utobj().sendKeys(driver,
							fc.utobj().getElementByXpath(driver,
									".//*[@id='row_" + String.valueOf(i) + "']/td[2]/input[1]"),
							str[i % 7] + fc.utobj().generateRandomNumber6Digit());
				}
			}

		}

		if (dataSet.get("checkboxOption").equals("Dependent")) {
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//*[@name='checkboxOption' and @value='0']"));
			List<WebElement> fieldviewRad = driver.findElements(By.name("optviewtype"));
			if (!fieldviewRad.get(1).isDisplayed()) {
				fc.utobj().throwsException("Field view is not having 2nd option");
			}
			if (fieldviewRad.get(0).getAttribute("checked") != null) {

			} else {
				fc.utobj().throwsException("By Default Horizontal hasn't been selected for Drop-down Field Type");
			}
			int i = Integer.parseInt(dataSet.get("dropdownfieldoptions"));
			String[] str = dataSet.get("OptionName").split(",");
			if (i > 1) {
				int j = 1;
				while (i > 1) {

					fc.utobj().sendKeys(driver,
							fc.utobj().getElementByXpath(driver,
									".//*[@id='row_" + String.valueOf(j) + "']/td[2]/input[1]"),
							str[i % 7] + fc.utobj().generateRandomNumber6Digit());

					if (j != 1 && !fc.utobj()
							.getElementByXpath(driver, ".//*[@id='removeTD" + String.valueOf(j) + "']/a/img")
							.isDisplayed()) {
						fc.utobj().throwsException("Remove options button is not coming for some options");
					}
					if (j == 1 && driver.findElements(By.xpath(".//*[@id='removeTD" + String.valueOf(j) + "']/a/img"))
							.size() > 0) {
						fc.utobj().throwsException("Remove options button is coming for options 1");
					}
					if (!driver
							.findElement(
									By.xpath(".//*[@id='row_" + String.valueOf(j) + "']//span[@class='urgent_fields']"))
							.isDisplayed()) {
						fc.utobj().throwsException("Mandatory field symbol is not coming");
					}

					fc.utobj().clickElement(driver,
							fc.utobj().getElementByXpath(driver, ".//*[@id='removeTDForAddMore']/a/span"));
					j++;
					i--;
				}
				fc.utobj().sendKeys(driver,
						fc.utobj().getElementByXpath(driver,
								".//*[@id='row_" + String.valueOf(j) + "']/td[2]/input[1]"),
						str[i % 7] + fc.utobj().generateRandomNumber6Digit());
				fc.utobj().clickElement(driver,
						fc.utobj().getElementByXpath(driver, ".//*[@id='removeTD" + String.valueOf(j) + "']/a/img"));
				if (driver.findElements(By.xpath(".//*[@id='row_" + String.valueOf(j) + "']/td[2]/input[1]"))
						.size() > 0) {
					fc.utobj().throwsException(
							"Options is not getting deleted by clicking on delete option kindly check");
				}
			} else {
				fc.utobj().sendKeys(driver,
						fc.utobj().getElementByXpath(driver,
								".//*[@id='row_" + String.valueOf(i) + "']/td[2]/input[1]"),
						str[i % 7] + fc.utobj().generateRandomNumber6Digit());
			}

			fc.utobj().clickElement(driver, pobj.submitFieldCBoxButton);
			if (fieldName.equals("")) {
				validfieldname = false;
				String msg = fc.utobj().acceptAlertBox(driver);
				if (!msg.equals("Please enter Display Name.")) {
					fc.utobj().throwsException("Correct validation msg is not coming while fieldname is empty");
				}

			} else {
				String spcialChar[] = { "\"", "'", "!", "@", "#", "$", "%", "^", "&", "*", "(", ")", "+", "=", "[", "]",
						";", "/", "{", "}", "|", "<", ">", "?", "_", "\\", };
				for (i = 0; i < spcialChar.length; i++) {
					if (fieldName.contains(spcialChar[i]) || Character.isDigit(fieldName.charAt(0))) {
						validfieldname = false;
						String msg = fc.utobj().acceptAlertBox(driver);
						if (!msg.equals("Display Name must begin with a letter.")) {
							fc.utobj().throwsException("Correct validation msg is not coming while fieldname is empty");
						}
						break;
					}

				}
			}
			if (validfieldname) {

				fc.utobj().clickElement(driver, ClickonSectionPageButton(driver, sectionName, "Add New Field"));
				fc.commonMethods().switch_cboxIframe_frameId(driver);

				p1.CheckInDropDown(driver, pobj.dbColumnType, "Checkbox");
				String parentFieldName = fieldName;
				fieldName = dataSet.get("DependentField") + "On" + fieldName;
				fc.utobj().sendKeys(driver, pobj.fieldDisplayName, fieldName);
				fc.utobj().clickElement(driver,
						fc.utobj().getElementByXpath(driver, ".//*[@name='checkboxOption' and @value='1']"));
				Select select = new Select(pobj.ParentdependentField);
				String value = select.getFirstSelectedOption().getText();
				if (!value.equals("Select")) {
					fc.utobj().throwsException("Default Select is not Selected for Parent Field");
				}
				fc.utobj().selectDropDownByVisibleText(driver, pobj.ParentdependentField, parentFieldName);
				List<WebElement> fieldviewRadio = driver.findElements(By.name("optviewtype"));
				if (!fieldviewRadio.get(1).isDisplayed()) {
					fc.utobj().throwsException("Field view is not having 2nd option");
				}
				fieldviewRadio.get(0).getAttribute("checked");
				if (fieldviewRadio.get(0).getAttribute("checked") != null) {

				} else {
					fc.utobj().throwsException("By Default Horizontal hasn't been selected for Drop-down Field Type");
				}
				i = Integer.parseInt(dataSet.get("dropdownfieldoptions"));
				if (i > 1) {
					int j = 1;
					while (i > 1) {

						fc.utobj().sendKeys(driver,
								driver.findElement(
										By.xpath(".//*[@id='row_" + String.valueOf(j) + "']/td[2]/input[1]")),
								str[i % 7] + fc.utobj().generateRandomNumber6Digit());

						if (j != 1 && !fc.utobj()
								.getElementByXpath(driver, ".//*[@id='removeTD" + String.valueOf(j) + "']/a/img")
								.isDisplayed()) {
							fc.utobj().throwsException("Remove options button is not coming for some options");
						}
						if (j == 1
								&& driver.findElements(By.xpath(".//*[@id='removeTD" + String.valueOf(j) + "']/a/img"))
										.size() > 0) {
							fc.utobj().throwsException("Remove options button is coming for options 1");
						}
						if (!driver
								.findElement(By.xpath(
										".//*[@id='row_" + String.valueOf(j) + "']//span[@class='urgent_fields']"))
								.isDisplayed()) {
							fc.utobj().throwsException("Mandatory field symbol is not coming");
						}

						fc.utobj().clickElement(driver,
								fc.utobj().getElementByXpath(driver, ".//*[@id='removeTDForAddMore']/a/span"));
						j++;
						i--;
					}
					fc.utobj().sendKeys(driver,
							fc.utobj().getElementByXpath(driver,
									".//*[@id='row_" + String.valueOf(j) + "']/td[2]/input[1]"),
							str[i % 7] + fc.utobj().generateRandomNumber6Digit());
					fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
							".//*[@id='removeTD" + String.valueOf(j) + "']/a/img"));
					if (driver.findElements(By.xpath(".//*[@id='row_" + String.valueOf(j) + "']/td[2]/input[1]"))
							.size() > 0) {
						fc.utobj().throwsException(
								"Options is not getting deleted by clicking on delete option kindly check");
					}
				} else {
					fc.utobj().sendKeys(driver,
							fc.utobj().getElementByXpath(driver,
									".//*[@id='row_" + String.valueOf(i) + "']/td[2]/input[1]"),
							str[i % 7] + fc.utobj().generateRandomNumber6Digit());
				}
			}

		}
	}

	private void CheckInTextDropdownFrame(WebDriver driver, AdminFranchiseSalesManageFormGeneratorPage pobj,
			Map<String, String> dataSet, AdminFranchiseSalesManageFormGeneratorPageTest p1, String fieldName,
			String sectionName) throws Exception {

		p1.CheckInDropDown(driver, pobj.dbColumnType, "Drop-down");
		List<WebElement> ddradio = driver.findElements(By.name("dropdownoption"));
		if (!ddradio.get(1).isDisplayed()) {
			fc.utobj().throwsException("Field Type is not having Country option");
		}
		if (!ddradio.get(2).isDisplayed()) {
			fc.utobj().throwsException("Field Type is not having Country & State option");
		}
		if (!ddradio.get(3).isDisplayed()) {
			fc.utobj().throwsException("Field Type is not having Dependent option");
		}
		if (ddradio.get(0).getAttribute("checked") != null) {

		} else {
			fc.utobj().throwsException("By Default New hasn't been selected for Drop-down Field Type");
		}

		if (dataSet.get("dropdownoption").equals("New")) {
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//*[@name='dropdownoption' and @value='0']"));
			int i = Integer.parseInt(dataSet.get("dropdownfieldoptions"));
			String[] str = dataSet.get("OptionName").split(",");
			if (i > 1) {
				int j = 1;
				while (i > 1) {

					fc.utobj().sendKeys(driver,
							fc.utobj().getElementByXpath(driver,
									".//*[@id='row_" + String.valueOf(j) + "']/td[2]/input[1]"),
							str[i % 7] + fc.utobj().generateRandomNumber6Digit());

					if (j != 1 && !fc.utobj()
							.getElementByXpath(driver, ".//*[@id='removeTD" + String.valueOf(j) + "']/a/img")
							.isDisplayed()) {
						fc.utobj().throwsException("Remove options button is not coming for some options");
					}
					if (j == 1 && driver.findElements(By.xpath(".//*[@id='removeTD" + String.valueOf(j) + "']/a/img"))
							.size() > 0) {
						fc.utobj().throwsException("Remove options button is coming for options 1");
					}
					if (!driver
							.findElement(
									By.xpath(".//*[@id='row_" + String.valueOf(j) + "']//span[@class='urgent_fields']"))
							.isDisplayed()) {
						fc.utobj().throwsException("Mandatory field symbol is not coming");
					}

					fc.utobj().clickElement(driver,
							fc.utobj().getElementByXpath(driver, ".//*[@id='removeTDForAddMore']/a/span"));
					j++;
					i--;
				}
				fc.utobj().sendKeys(driver,
						fc.utobj().getElementByXpath(driver,
								".//*[@id='row_" + String.valueOf(j) + "']/td[2]/input[1]"),
						str[i % 7] + fc.utobj().generateRandomNumber6Digit());
				fc.utobj().clickElement(driver,
						fc.utobj().getElementByXpath(driver, ".//*[@id='removeTD" + String.valueOf(j) + "']/a/img"));
				if (driver.findElements(By.xpath(".//*[@id='row_" + String.valueOf(j) + "']/td[2]/input[1]"))
						.size() > 0) {
					fc.utobj().throwsException(
							"Options is not getting deleted by clicking on delete option kindly check");
				}
			} else {
				fc.utobj().sendKeys(driver,
						fc.utobj().getElementByXpath(driver,
								".//*[@id='row_" + String.valueOf(i) + "']/td[2]/input[1]"),
						str[i % 7] + fc.utobj().generateRandomNumber6Digit());
			}

		}
		if (dataSet.get("dropdownoption").equals("Country") && dataSet.get("dbColumnType").equals("Drop-down")) {
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//*[@name='dropdownoption' and @value='1']"));
			if (!fc.utobj().getElement(driver, pobj.closeSectionCboxBtn).isDisplayed()) {
				fc.utobj().throwsException("Cancel button is not displayable ");
			}
		}
		if (dataSet.get("dropdownoption").equals("Country & State")
				&& dataSet.get("dbColumnType").equals("Drop-down")) {
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//*[@name='dropdownoption' and @value='2']"));
			if (!fc.utobj().getElement(driver, pobj.closeSectionCboxBtn).isDisplayed()) {
				fc.utobj().throwsException("Cancel button is not displayable ");
			}
			fc.utobj().sendKeys(driver, pobj.fieldDisplayName1, dataSet.get("StateInDropdown"));
		}

		if (dataSet.get("dropdownoption").equals("Dependent")) {
			boolean newfieldadded = true;
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//*[@name='dropdownoption' and @value='0']"));
			int i = Integer.parseInt(dataSet.get("dropdownfieldoptions"));
			String[] str = dataSet.get("OptionName").split(",");
			if (i > 1) {
				int j = 1;
				while (i > 1) {

					fc.utobj().sendKeys(driver,
							fc.utobj().getElementByXpath(driver,
									".//*[@id='row_" + String.valueOf(j) + "']/td[2]/input[1]"),
							str[i % 7] + fc.utobj().generateRandomNumber6Digit());

					if (j != 1 && !fc.utobj()
							.getElementByXpath(driver, ".//*[@id='removeTD" + String.valueOf(j) + "']/a/img")
							.isDisplayed()) {
						fc.utobj().throwsException("Remove options button is not coming for some options");
					}
					if (j == 1 && driver.findElements(By.xpath(".//*[@id='removeTD" + String.valueOf(j) + "']/a/img"))
							.size() > 0) {
						fc.utobj().throwsException("Remove options button is coming for options 1");
					}
					if (!driver
							.findElement(
									By.xpath(".//*[@id='row_" + String.valueOf(j) + "']//span[@class='urgent_fields']"))
							.isDisplayed()) {
						fc.utobj().throwsException("Mandatory field symbol is not coming");
					}

					fc.utobj().clickElement(driver,
							fc.utobj().getElementByXpath(driver, ".//*[@id='removeTDForAddMore']/a/span"));
					j++;
					i--;
				}
				fc.utobj().sendKeys(driver,
						fc.utobj().getElementByXpath(driver,
								".//*[@id='row_" + String.valueOf(j) + "']/td[2]/input[1]"),
						str[i % 7] + fc.utobj().generateRandomNumber6Digit());
				fc.utobj().clickElement(driver,
						fc.utobj().getElementByXpath(driver, ".//*[@id='removeTD" + String.valueOf(j) + "']/a/img"));
				if (driver.findElements(By.xpath(".//*[@id='row_" + String.valueOf(j) + "']/td[2]/input[1]"))
						.size() > 0) {
					fc.utobj().throwsException(
							"Options is not getting deleted by clicking on delete option kindly check");
				}
			} else {
				fc.utobj().sendKeys(driver,
						fc.utobj().getElementByXpath(driver,
								".//*[@id='row_" + String.valueOf(i) + "']/td[2]/input[1]"),
						str[i % 7] + fc.utobj().generateRandomNumber6Digit());
			}

			fc.utobj().clickElement(driver, pobj.submitFieldCBoxButton);
			if (fieldName == "") {
				String msg = fc.utobj().acceptAlertBox(driver);
				newfieldadded = false;
				if (!msg.equals("Please enter Display Name.")) {
					fc.utobj().throwsException("Correct validation msg is not coming while fieldname is empty");
				}

			}
			String spcialChar[] = { "\"", "'", "!", "@", "#", "$", "%", "^", "&", "*", "(", ")", "+", "=", "[", "]",
					";", "/", "{", "}", "|", "<", ">", "?", "_", "\\", };
			for (i = 0; i < spcialChar.length; i++) {
				if (fieldName.startsWith(spcialChar[i]) || Character.isDigit(fieldName.charAt(0))) {
					newfieldadded = false;
					String msg = fc.utobj().acceptAlertBox(driver);
					if (!msg.equals("Display Name must begin with a letter.")) {
						fc.utobj().throwsException("Correct validation msg is not coming while fieldname is empty");
					}
					break;
				}
			}
			if (newfieldadded) {

				fc.utobj().clickElement(driver, ClickonSectionPageButton(driver, sectionName, "Add New Field"));
				fc.commonMethods().switch_cboxIframe_frameId(driver);

				p1.CheckInDropDown(driver, pobj.dbColumnType, "Drop-down");
				fc.utobj().clickElement(driver,
						fc.utobj().getElementByXpath(driver, ".//*[@name='dropdownoption' and @value='3']"));
				String parentFieldName = fieldName;
				fieldName = dataSet.get("DependentField");
				fc.utobj().sendKeys(driver, pobj.fieldDisplayName, fieldName);
				fc.utobj().selectDropDownByVisibleText(driver, pobj.ParentdependentField, parentFieldName);

				if (!fc.utobj().getElement(driver, pobj.closeSectionCboxBtn).isDisplayed()) {
					fc.utobj().throwsException("Cancel button is not displayable ");
				}
				i = Integer.parseInt(dataSet.get("dropdownfieldoptions"));
				if (i > 1) {
					int j = 1;
					while (i > 1) {
						fc.utobj().sendKeys(driver,
								driver.findElement(
										By.xpath(".//*[@id='row_" + String.valueOf(j) + "']/td[2]/input[1]")),
								str[i % 7] + fc.utobj().generateRandomNumber6Digit());
						fc.utobj().clickElement(driver,
								fc.utobj().getElementByXpath(driver, ".//*[@id='removeTDForAddMore']/a/span"));
						i--;
						j++;
					}
					fc.utobj().sendKeys(driver,
							fc.utobj().getElementByXpath(driver,
									".//*[@id='row_" + String.valueOf(j) + "']/td[2]/input[1]"),
							str[i % 7] + fc.utobj().generateRandomNumber6Digit());
					fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
							".//*[@id='removeTD" + String.valueOf(j) + "']/a/img"));
					if (fc.utobj().getElementByXpath(driver, ".//*[@id='row_" + String.valueOf(j) + "']/td[2]/input[1]")
							.isDisplayed()) {
						fc.utobj().throwsException(
								"Options is not getting deleted by clicking on delete option kindly check");
					}
				} else {
					fc.utobj().sendKeys(driver,
							fc.utobj().getElementByXpath(driver,
									".//*[@id='row_" + String.valueOf(i) + "']/td[2]/input[1]"),
							str[i % 7] + fc.utobj().generateRandomNumber6Digit());

				}
			}

		}
	}

	private void CheckInTextAreaFieldFrame(WebDriver driver, AdminFranchiseSalesManageFormGeneratorPage pobj,
			Map<String, String> dataSet, AdminFranchiseSalesManageFormGeneratorPageTest p1) throws Exception {
		p1.CheckInDropDown(driver, pobj.dbColumnType, "Text Area");
		String DefaultnoOfCols = dataSet.get("DefaultnoOfCols");
		String TextAreaMaxColumnNote = dataSet.get("TextAreaMaxColumnNote");
		String TextAreaMaxRowsNote = dataSet.get("TextAreaMaxRowsNote");
		String actDefaultnoOfCols = pobj.noOfCols.getAttribute("value");
		if (!actDefaultnoOfCols.equals(DefaultnoOfCols)) {
			fc.utobj().throwsException("No of cols value is not as default said");
		}
		String DefaultnoOfRows = dataSet.get("DefaultnoOfRows");
		String actDefaultnoOfRoss = pobj.noOfRows.getAttribute("value");
		if (!actDefaultnoOfRoss.equals(DefaultnoOfRows)) {
			fc.utobj().throwsException("No of rows value is not as default said");
		}
		if (!fc.utobj().assertPageSource(driver, TextAreaMaxColumnNote)) {
			fc.utobj().throwsException("Max Column note not found");
		}
		if (!fc.utobj().assertPageSource(driver, TextAreaMaxRowsNote)) {
			fc.utobj().throwsException("Max Row note not found");
		}
		fc.utobj().sendKeys(driver, pobj.noOfCols, dataSet.get("UsersnoOfCols"));
		fc.utobj().sendKeys(driver, pobj.noOfRows, dataSet.get("UsersnoOfRows"));
	}

	private void CheckInTextFieldFrame(WebDriver driver, AdminFranchiseSalesManageFormGeneratorPage pobj,
			AdminFranchiseSalesManageFormGeneratorPageTest p1, Map<String, String> dataSet) throws Exception {
		String s = pobj.dbColumnLength.getAttribute("value");
		if (!s.equals("255")) {
			fc.utobj().throwsException("By Default 255 hasn't been selected for dbcolumnLength");
		}
		String defaulttag = "None,Phone,Email,Url";
		String[] options = null;
		boolean found = false;
		options = defaulttag.split(",");
		for (String optins : options) {
			found = p1.CheckInDropDown(driver, pobj.fldValidationType, optins);
			if (!found) {
				fc.utobj().throwsException("Value not found in Drop down ");
			}
		}
		p1.CheckInDropDown(driver, pobj.fldValidationType, dataSet.get("Valid"));
		fc.utobj().sendKeys(driver, pobj.dbColumnLength, dataSet.get("dbcolumnlength"));
		if (dataSet.get("IsExportable").equals("No")) {
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//*[@name='exportable'  and @value='1']"));
		}
		fc.utobj().clickElement(driver, pobj.submitFieldCBoxButton);

		int dblen = Integer.parseInt(dataSet.get("dbcolumnlength"));
		if (dblen < 1 || dblen > 255) {
			String msg = fc.utobj().acceptAlertBox(driver);
			if (!msg.equals("Please enter the length between 1 and 255.")) {
				fc.utobj().throwsException("Alert msg is not As per expectecd");
			}
		}
	}

	private void CheckForCommonFieldsOnFrame(WebDriver driver, AdminFranchiseSalesManageFormGeneratorPage pobj,
			AdminFranchiseSalesManageFormGeneratorPageTest p1, Map<String, String> dataSet, String testCaseId)
			throws Exception {

		String spclCharNote = dataSet.get("Special Character Note");
		String mandatoryNote = dataSet.get("Mandatory Note");
		fc.utobj().printTestStep("Validating Special Character Note");
		if (!fc.utobj().assertPageSource(driver, spclCharNote)) {
			fc.utobj().throwsException("Special Character Note is not showing ");
		}
		fc.utobj().printTestStep("Validating Mandatory Note");
		if (!fc.utobj().assertPageSource(driver, mandatoryNote)) {
			fc.utobj().throwsException("Special Character Note is not showing ");
		}
		fc.utobj().printTestStep("Validating Cancel Button is present or not");
		if (!fc.utobj().getElement(driver, pobj.closeSectionCboxBtn).isDisplayed()) {
			fc.utobj().throwsException("Cancel button is not displayable ");
		}

		Select select = new Select(fc.utobj().getElementByXpath(driver, ".//*[@name='dbColumnType']"));
		fc.utobj().printTestStep("Validating Default Text Drop-Down is present or not");
		if (!select.getFirstSelectedOption().getText().equals("Text")) {
			fc.utobj().throwsException("Default Text wasn't selected");
		}
		fc.utobj().printTestStep("Is Exportable and Searchable? is having no option or not");
		List<WebElement> radio = driver.findElements(By.name("exportable"));
		if (!radio.get(1).isDisplayed()) {
			fc.utobj().throwsException("Is Exportable and Searchable? is not having no option");
		}
		fc.utobj().printTestStep("Is Exportable and Searchable? is Default yes or not");
		if (radio.get(0).getAttribute("checked") != null) {

		} else {
			fc.utobj().throwsException("By Default Yes hasn't been selected for Is Exportable and Searchable?");
		}
		String defaulttag = "Text,Text Area,Date,Drop-down,Radio,Numeric,Checkbox,Multi Select Drop-down,Document";
		String[] options = defaulttag.split(",");
		boolean found = false;
		for (String optins : options) {
			fc.utobj().printTestStep("validation " + optins + " option is present or not in DropDown field Type");
			found = p1.CheckInDropDown(driver, pobj.dbColumnType, optins);
			if (!found) {
				fc.utobj().throwsException("Value not found in Drop down ");
			}

		}
	}

	public String addFieldName(WebDriver driver, String sectionName, String fieldName) throws Exception {

		AdminFranchiseSalesManageFormGeneratorPage pobj = new AdminFranchiseSalesManageFormGeneratorPage(driver);
		AdminFranchiseSalesManageFormGeneratorPageTest p1 = new AdminFranchiseSalesManageFormGeneratorPageTest();

		fieldName = fc.utobj().generateTestData(fieldName);
		String windowHandle = driver.getWindowHandle();
		fc.utobj().clickElement(driver, pobj.addNewFieldBtn);
		fc.commonMethods().switch_cboxIframe_frameId(driver);

		fc.utobj().sendKeys(driver, pobj.fieldDisplayName, fieldName);
		if (!fc.utobj().getElement(driver, pobj.closeSectionCboxBtn).isDisplayed()) {
			fc.utobj().throwsException("Cancel button is not displayable ");
		}
		List<WebElement> radio = driver.findElements(By.name("exportable"));
		Select select = new Select(fc.utobj().getElementByXpath(driver, ".//*[@name='dbColumnType']"));
		if (!select.getFirstSelectedOption().getText().equals("Text")) {
			fc.utobj().throwsException("Default Text wasn't selected");
		}
		if (radio.get(0).getAttribute("checked") != null) {

		} else {
			fc.utobj().throwsException("By Default Yes hasn't been selected for Is Exportable and Searchable?");
		}
		String s = pobj.dbColumnLength.getAttribute("value");
		if (!s.equals("255")) {
			fc.utobj().throwsException("By Default 255 hasn't been selected for dbcolumnLength");
		}
		boolean found = false;
		String defaulttag = "Text,Text Area,Date,Drop-down,Radio,Numeric,Checkbox,Multi Select Drop-down,Document";
		String[] options = defaulttag.split(",");
		for (String optins : options) {
			found = p1.CheckInDropDown(driver, pobj.dbColumnType, optins);
			if (!found) {
				fc.utobj().throwsException("Value not found in Drop down ");
			}

		}
		p1.CheckInDropDown(driver, pobj.dbColumnType, "Text");
		defaulttag = "None,Phone,Email,Url";
		options = null;
		options = defaulttag.split(",");
		for (String optins : options) {
			found = p1.CheckInDropDown(driver, pobj.fldValidationType, optins);
			if (!found) {
				fc.utobj().throwsException("Value not found in Drop down ");
			}

		}

		p1.CheckInDropDown(driver, pobj.fldValidationType, "Phone");
		fc.utobj().clickElement(driver, pobj.submitFieldCBoxButton);

		driver.switchTo().window(windowHandle);
		return fieldName;

	}

	public String addFieldNameMandatory(WebDriver driver, String sectionName, String fieldName) throws Exception {

		AdminFranchiseSalesManageFormGeneratorPage pobj = new AdminFranchiseSalesManageFormGeneratorPage(driver);

		fieldName = fc.utobj().generateTestData(fieldName);
		String windowHandle = driver.getWindowHandle();
		fc.utobj().clickElement(driver, pobj.addNewFieldBtn);
		fc.commonMethods().switch_cboxIframe_frameId(driver);

		fc.utobj().sendKeys(driver, pobj.fieldDisplayName, fieldName);
		fc.utobj().clickElement(driver, pobj.submitFieldCBoxButton);
		driver.switchTo().window(windowHandle);
		return fieldName;

	}

	public String addFieldNameAllType(WebDriver driver, String sectionName, String fieldName, String type)
			throws Exception {

		AdminFranchiseSalesManageFormGeneratorPage pobj = new AdminFranchiseSalesManageFormGeneratorPage(driver);

		fieldName = fc.utobj().generateTestData(fieldName);

		if (type == null || "".equals(type) || "null".equals(type)) {
			type = "Text";
		}

		String windowHandle = driver.getWindowHandle();
		if ("Text".equals(type))
			fc.utobj().clickElement(driver, pobj.addNewFieldBtn);
		else
			fc.utobj().clickElement(driver, pobj.addFiledtoBtnmultiple);
		fc.commonMethods().switch_cboxIframe_frameId(driver);

		fc.utobj().sendKeys(driver, pobj.fieldDisplayName, fieldName);
		fc.utobj().selectDropDownByVisibleText(driver, pobj.dbColumnType, type);

		// if("Drop-down".equals(type) || "Radio".equals(type) ||
		// "Checkbox".equals(type) || "Multi Select Drop-down".equals(type))
		// fc.utobj().sendKeys(driver,pobj.optionNameTemp,"A");
		if ("Drop-down".equals(type))
			fc.utobj().sendKeys(driver, pobj.optionNameTemp, "DropValue1");
		if ("Radio".equals(type))
			fc.utobj().sendKeys(driver, pobj.optionNameTemp, "Radio1");
		if ("Checkbox".equals(type))
			fc.utobj().sendKeys(driver, pobj.optionNameTemp, "Checkbox1");
		if ("Multi Select Drop-down".equals(type))
			fc.utobj().sendKeys(driver, pobj.optionNameTemp, "Multi1");

		fc.utobj().clickElement(driver, pobj.submitFieldCBoxButton);
		driver.switchTo().window(windowHandle);
		return fieldName;

	}

	public String addFieldSynchType(WebDriver driver, String sectionName, String fieldName, String type)
			throws Exception {

		AdminFranchiseSalesManageFormGeneratorPage pobj = new AdminFranchiseSalesManageFormGeneratorPage(driver);

		fieldName = fc.utobj().generateTestData(fieldName);

		if (type == null || "".equals(type) || "null".equals(type)) {
			type = "Text";
		}

		String windowHandle = driver.getWindowHandle();
		fc.utobj().clickElement(driver, pobj.addNewFieldBtn);
		fc.commonMethods().switch_cboxIframe_frameId(driver);

		fc.utobj().sendKeys(driver, pobj.fieldDisplayName, fieldName);
		fc.utobj().selectDropDownByVisibleText(driver, pobj.dbColumnType, type);

		fc.utobj().clickElement(driver,
				fc.utobj().getElementByXpath(driver, ".//*[@id='ms-parentfieldNamesForTabNew']/button"));
		fc.utobj().sendKeys(driver, pobj.searchBoxSynch, "City");
		// fc.utobj().clickElement(driver,
		// fc.utobj().getElementByXpath(driver,".//*[@id='ms-parentfieldNamesForTabNew']/div/ul/li/label[contains(text
		// () ,'Primary Info')]/input")));
		// fc.utobj().moveToElement(driver, pobj.PrimaryInfoSectionSearch);
		fc.utobj().clickElement(driver, driver.findElement(
				By.xpath(".//*[@id='ms-parentfieldNamesForTabNew']/div/ul/li/label[contains(text () ,'City')]/input")));
		fc.utobj().clickElement(driver,
				fc.utobj().getElementByXpath(driver, ".//*[@id='ms-parentfieldNamesForTabNew']/button"));

		fc.utobj().clickElement(driver, pobj.submitFieldCBoxButton);
		driver.switchTo().window(windowHandle);
		return fieldName;

	}

	public String addDocFieldName(WebDriver driver, String sectionName, String fieldName, String docName)
			throws Exception {

		docName = fc.utobj().generateTestData(docName);
		AdminFranchiseSalesManageFormGeneratorPage pobj = new AdminFranchiseSalesManageFormGeneratorPage(driver);

		String windowHandle = driver.getWindowHandle();
		fc.utobj().clickElement(driver, pobj.addDocumentSaleBtn);
		fc.commonMethods().switch_cboxIframe_frameId(driver);

		fc.utobj().sendKeys(driver, pobj.documentTitle, docName);
		fc.utobj().sendKeys(driver, pobj.documentLabel, docName);
		fc.utobj().clickElement(driver, pobj.addDocumentCboxBtn);
		driver.switchTo().window(windowHandle);
		return docName;

	}

	public String addLeadFromWebForm(WebDriver driver, String fieldName) throws Exception {

		AdminFranchiseSalesManageFormGeneratorPage pobj = new AdminFranchiseSalesManageFormGeneratorPage(driver);
		// adsales.manageWebFormGenerator( driver);
		fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//input[@value='Create New Form']"));
		String formName = "wb" + fc.utobj().generateRandomNumber();
		fc.utobj().sendKeys(driver, fc.utobj().getElementByID(driver, "formName"), formName);
		fc.utobj().sendKeys(driver, fc.utobj().getElementByID(driver, "formDisplayTitle"), "WebFrom Title");
		fc.utobj().sendKeys(driver, fc.utobj().getElementByID(driver, "formUrl"), formName);
		fc.utobj().clickElement(driver, fc.utobj().getElementByID(driver, "detailsNextBtn"));

		fc.utobj().clickElement(driver, pobj.primaryInfo);
		fc.utobj().sendKeys(driver, pobj.searchField, fieldName);

		fc.utobj().dragAndDropElement(driver, fc.utobj().getElementByXpath(driver, ".//a[.='" + fieldName + "']"),
				pobj.defaultDropHere);

		fc.utobj().clickElement(driver, pobj.saveAndNextBtnDesign);
		// fc.utobj().clickElement(driver,
		// fc.utobj().getElementByID(driver,"designNextBtn")));

		fc.utobj().clickElement(driver, fc.utobj().getElementByID(driver, "settingsNextBtn"));
		fc.utobj().clickElement(driver, fc.utobj().getElementByID(driver, "hostURL"));
		String newFormUrl = fc.utobj().getText(driver, fc.utobj().getElementByID(driver, "hostCodeBox"));
		fc.utobj().clickElement(driver, fc.utobj().getElementByID(driver, "confirmationNextBtn"));

		// driver.get(newFormUrl);

		return newFormUrl;

	}

	public String addFieldNamePrimaryInfo(WebDriver driver, String sectionName, String fieldName) throws Exception {

		AdminFranchiseSalesManageFormGeneratorPage pobj = new AdminFranchiseSalesManageFormGeneratorPage(driver);

		fieldName = fc.utobj().generateTestData(fieldName);
		String windowHandle = driver.getWindowHandle();
		// fc.utobj().clickElement(driver,pobj.addNewFieldBtn);
		fc.commonMethods().switch_cboxIframe_frameId(driver);

		fc.utobj().sendKeys(driver, pobj.fieldDisplayName, fieldName);
		fc.utobj().clickElement(driver, pobj.submitFieldCBoxButton);
		driver.switchTo().window(windowHandle);
		return fieldName;

	}

	public boolean CheckInDropDown(WebDriver driver, WebElement dropdown, String tabName) {
		try {
			Select opt = new Select(dropdown);
			opt.selectByVisibleText(tabName);
			return true;

		} catch (Exception e) {
			return false;
		}

	}

}
