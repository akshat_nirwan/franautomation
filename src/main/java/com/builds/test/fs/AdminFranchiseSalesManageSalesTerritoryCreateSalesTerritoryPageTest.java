package com.builds.test.fs;

import java.util.List;
import java.util.Map;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Optional;
import org.testng.annotations.Test;

import com.builds.test.admin.AdminUsersManageCorporateUsersAddCorporateUserPageTest;
import com.builds.test.common.CorporateUser;
import com.builds.uimaps.fs.AdminFranchiseSalesManageSalesTerritoriesPage;
import com.builds.uimaps.fs.AdminFranchiseSalesManageSalesTerritoryCreateSalesTerritoryPage;
import com.builds.utilities.FranconnectUtil;
import com.builds.utilities.TestCase;

class AdminFranchiseSalesManageSalesTerritoryCreateSalesTerritoryPageTest {

	@Test(groups = { "sales", "sales_moduleadd001" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "TC_Admin_FS_001", testCaseDescription = "Add Territory in Admin Sales")
	void addSalesTerritoryZip() throws Exception {
		FranconnectUtil fc = new FranconnectUtil();

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("sales", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		String salesTerritory = dataSet.get("salesTerritory");
		String zipCode = fc.utobj().generateRandomNumber();

		salesTerritory = fc.utobj().generateTestData(salesTerritory);
		try {
			driver = fc.loginpage().login(driver);
			fc.utobj().printTestStep("Go to Admin Sales territory section \nAdd Sales Territory");
			salesTerritory = addSalesTerritoryRandomZip(driver, salesTerritory, zipCode, "Domestic");
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	String addSalesTerritoryRandomZip(WebDriver driver, @Optional() String salesTerritory, String zipCode,
			String Category) throws Exception {
		FranconnectUtil fc = new FranconnectUtil();

		AdminSales adsales = new AdminSales();
		adsales.adminManageSalesTerritory(driver);
		AdminFranchiseSalesManageSalesTerritoriesPage pobj = new AdminFranchiseSalesManageSalesTerritoriesPage(driver);

		fc.utobj().clickElement(driver, pobj.addNewSalesTerritory);
		AdminFranchiseSalesManageSalesTerritoryCreateSalesTerritoryPage pobj2 = new AdminFranchiseSalesManageSalesTerritoryCreateSalesTerritoryPage(
				driver);
		if (Category.equalsIgnoreCase("Domestic")) {
			fc.utobj().selectDropDownByVisibleText(driver, pobj2.category, "Domestic");
			fc.utobj().sendKeys(driver, pobj2.salesTerritoryName, salesTerritory);
			fc.utobj().selectDropDownByVisibleText(driver, pobj2.groupBy, "Zip / Postal Code");
			fc.utobj().clickElement(driver, pobj2.selectAllCountry);
			fc.utobj().clickElement(driver, pobj2.commaSeparatedListZip);
			// String zipCode = fc.utobj().generateRandomNumber();
			fc.utobj().sendKeys(driver, pobj2.ziplist, zipCode);
			fc.utobj().clickElement(driver, pobj2.submitBtn);
		} else if (Category.equalsIgnoreCase("International")) {
			fc.utobj().selectDropDownByVisibleText(driver, pobj2.category, "International");
			fc.utobj().sendKeys(driver, pobj2.salesTerritoryName, salesTerritory);
			fc.utobj().selectDropDownByVisibleText(driver, pobj2.groupBy, "Zip / Postal Code");
			fc.utobj().clickElement(driver, pobj2.selectAllCountry);
			fc.utobj().clickElement(driver, pobj2.commaSeparatedListZip);
			fc.utobj().sendKeys(driver, pobj2.ziplist, zipCode);
			fc.utobj().clickElement(driver, pobj2.submitBtn);
		}

		return salesTerritory;
	}

	String addSalesTerritoryDomesticCountyBased(WebDriver driver, @Optional() String salesTerritory) throws Exception {
		FranconnectUtil fc = new FranconnectUtil();

		AdminSales adsales = new AdminSales();
		adsales.adminManageSalesTerritory(driver);
		AdminFranchiseSalesManageSalesTerritoriesPage pobj = new AdminFranchiseSalesManageSalesTerritoriesPage(driver);
		//
		fc.utobj().clickElement(driver, pobj.addNewSalesTerritory);
		AdminFranchiseSalesManageSalesTerritoryCreateSalesTerritoryPage pobj2 = new AdminFranchiseSalesManageSalesTerritoryCreateSalesTerritoryPage(
				driver);
		try {
			fc.utobj().selectDropDownByVisibleText(driver, pobj2.category, "Domestic");
			fc.utobj().sendKeys(driver, pobj2.salesTerritoryName, salesTerritory);
			fc.utobj().selectDropDownByVisibleText(driver, pobj2.groupBy, "County");
			fc.utobj().clickElement(driver, pobj2.selectAllCountry);
			fc.utobj().clickElement(driver, pobj2.firstStateChkBox);
			fc.utobj().clickElement(driver, pobj2.submitBtn);
			fc.utobj().clickElement(driver, pobj2.firstStateChkBox);
			try {
				fc.utobj().clickElement(driver, pobj2.nextBtn);
			} catch (Exception e) {
				fc.utobj().clickElement(driver, pobj2.nextBtn2fr);
			}
		} catch (Exception e) {
			boolean allStateAlreadyAssigned = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text(),'All the states for this country have already been assigned.')]");
			if (allStateAlreadyAssigned == true) {
				fc.utobj().throwsSkipException("All the states for this country have already been assigned.");
			}
			fc.utobj().throwsException("Unable to create Domestic County sales teritory ");
		}

		return salesTerritory;
	}

	String addSalesTerritoryWithZip(WebDriver driver, @Optional() String salesTerritory, String zipCode)
			throws Exception {
		FranconnectUtil fc = new FranconnectUtil();

		boolean isValFound = false;
		AdminSales adsales = new AdminSales();
		adsales.adminManageSalesTerritory(driver);
		AdminFranchiseSalesManageSalesTerritoriesPage pobj = new AdminFranchiseSalesManageSalesTerritoriesPage(driver);

		fc.utobj().clickElement(driver, pobj.addNewSalesTerritory);
		AdminFranchiseSalesManageSalesTerritoryCreateSalesTerritoryPage pobj2 = new AdminFranchiseSalesManageSalesTerritoryCreateSalesTerritoryPage(
				driver);
		fc.utobj().selectDropDownByVisibleText(driver, pobj2.category, "Domestic");
		fc.utobj().sendKeys(driver, pobj2.salesTerritoryName, salesTerritory);
		fc.utobj().sendKeys(driver, pobj2.groupBy, "Zip / Postal Code");
		fc.utobj().clickElement(driver, pobj2.selectAllCountry);
		fc.utobj().clickElement(driver, pobj2.commaSeparatedListZip);
		fc.utobj().sendKeys(driver, pobj2.ziplist, zipCode);
		fc.utobj().clickElement(driver, pobj2.submitBtn);
		fc.utobj().selectValFromMultiSelect(driver, pobj2.salesTerritoryMS, salesTerritory);
		fc.utobj().assertSingleLinkText(driver, salesTerritory);
		fc.utobj().getElementByLinkText(driver, salesTerritory).click();
		isValFound = fc.sales().sales_common().assertValuePresentInCbox(driver, zipCode);

		if (isValFound == false) {
			fc.utobj().throwsException("Sales Territory not added!");
		}

		return salesTerritory;
	}

	String addSalesTerritoryWithState(WebDriver driver, @Optional() String salesTerritory, String Country, String state)
			throws Exception {
		FranconnectUtil fc = new FranconnectUtil();

		AdminSales adsales = new AdminSales();
		adsales.adminManageSalesTerritory(driver);
		AdminFranchiseSalesManageSalesTerritoriesPage pobj = new AdminFranchiseSalesManageSalesTerritoriesPage(driver);

		fc.utobj().clickElement(driver, pobj.addNewSalesTerritory);
		AdminFranchiseSalesManageSalesTerritoryCreateSalesTerritoryPage pobj2 = new AdminFranchiseSalesManageSalesTerritoryCreateSalesTerritoryPage(
				driver);
		fc.utobj().selectDropDownByVisibleText(driver, pobj2.category, "Domestic");
		fc.utobj().sendKeys(driver, pobj2.salesTerritoryName, salesTerritory);
		fc.utobj().selectDropDownByVisibleText(driver, pobj2.groupBy, "States");
		fc.utobj().clickElement(driver, pobj2.selectAllCountry);

		List<WebElement> allStateRow = driver.findElements(By.xpath(".//tr[@id='countryRow1']//table//tr"));

		int sizeofstateListRow = allStateRow.size();

		// System.out.println("sizeofstateList : "+sizeofstateListRow);

		int trNum = 0;
		int tdNum = 0;

		for (int x = 2; x <= sizeofstateListRow; x++) {

			List<WebElement> tdStateListColumn = driver
					.findElements(By.xpath(".//tr[@id='countryRow1']//table//tr[" + x + "]//td"));
			int tdStateListColumnSize = tdStateListColumn.size();
			// System.out.println("tdStateListColumnSize :
			// "+tdStateListColumnSize);

			for (int y = 1; y <= tdStateListColumnSize; y++) {
				System.out.println("Value of y : " + y);
				String stateName = driver
						.findElement(By.xpath(".//tr[@id='countryRow1']//table//tr[" + x + "]//td[" + y + "]"))
						.getText();
				if (stateName.trim().equalsIgnoreCase(state)) {
					trNum = x;
					tdNum = y;
				}
			}
		}

		System.out.println("x : " + trNum);
		System.out.println("y : " + tdNum);

		fc.utobj().getElementByXpath(driver,
				".//tr[@id='countryRow1']//table//tr[" + trNum + "]//td[" + tdNum + "]/input").click();

		fc.utobj().clickElement(driver, pobj2.submitBtn);
		fc.utobj().selectValFromMultiSelect(driver, pobj.salesTerritoryMs, salesTerritory);

		return salesTerritory;
	}

	String addSalesTerritoryWithCounty(WebDriver driver, String salesTerritory, String countryName, String state,
			String county) throws Exception {
		FranconnectUtil fc = new FranconnectUtil();

		AdminSales adsales = new AdminSales();
		adsales.adminManageSalesTerritory(driver);
		AdminFranchiseSalesManageSalesTerritoriesPage pobj = new AdminFranchiseSalesManageSalesTerritoriesPage(driver);

		fc.utobj().clickElement(driver, pobj.addNewSalesTerritory);
		AdminFranchiseSalesManageSalesTerritoryCreateSalesTerritoryPage pobj2 = new AdminFranchiseSalesManageSalesTerritoryCreateSalesTerritoryPage(
				driver);
		fc.utobj().selectDropDownByVisibleText(driver, pobj2.category, "Domestic");
		fc.utobj().sendKeys(driver, pobj2.salesTerritoryName, salesTerritory);
		fc.utobj().selectDropDownByVisibleText(driver, pobj2.groupBy, "County");
		fc.utobj().clickElement(driver, pobj2.selectAllCountry);

		List<WebElement> allStateRow = driver.findElements(By.xpath(".//tr[@id='countryRow1']//table//tr"));

		int sizeofstateListRow = allStateRow.size();

		// System.out.println("sizeofstateList : "+sizeofstateListRow);

		int trNum = 0;
		int tdNum = 0;

		for (int x = 2; x <= sizeofstateListRow; x++) {

			List<WebElement> tdStateListColumn = driver
					.findElements(By.xpath(".//tr[@id='countryRow1']//table//tr[" + x + "]//td"));
			int tdStateListColumnSize = tdStateListColumn.size();
			// System.out.println("tdStateListColumnSize :
			// "+tdStateListColumnSize);

			for (int y = 1; y <= tdStateListColumnSize; y++) {
				System.out.println("Value of y : " + y);
				String stateName = driver
						.findElement(By.xpath(".//tr[@id='countryRow1']//table//tr[" + x + "]//td[" + y + "]"))
						.getText();
				if (stateName.trim().equalsIgnoreCase(state)) {
					trNum = x;
					tdNum = y;
				}
			}
		}

		System.out.println("x : " + trNum);
		System.out.println("y : " + tdNum);

		fc.utobj().getElementByXpath(driver,
				".//tr[@id='countryRow1']//table//tr[" + trNum + "]//td[" + tdNum + "]/input").click();

		fc.utobj().clickElement(driver, pobj2.submitBtn);

		List<WebElement> allCounty = driver.findElements(By.xpath(
				".//*[@id='siteMainTable']/tbody/tr/td/table/tbody/tr[4]/td[2]/table/tbody/tr[1]/td/table/tbody//tr"));

		int sizeofCountyRow = allCounty.size();

		// System.out.println("sizeofCounty : "+sizeofCountyRow);

		int trNum_1 = 0;
		int tdNum_1 = 0;

		for (int x = 4; x <= sizeofCountyRow; x++) {

			List<WebElement> tdColumnCount = driver.findElements(By
					.xpath(".//*[@id='siteMainTable']/tbody/tr/td/table/tbody/tr[4]/td[2]/table/tbody/tr[1]/td/table/tbody//tr["
							+ x + "]//td"));
			int tdColumnCountSize = tdColumnCount.size();
			// System.out.println("tdColumnCountSize : "+tdColumnCountSize);

			for (int y = 1; y <= tdColumnCountSize; y++) {
				System.out.println("Value of y_1 : " + y);
				String countyName = driver.findElement(By
						.xpath(".//*[@id='siteMainTable']/tbody/tr/td/table/tbody/tr[4]/td[2]/table/tbody/tr[1]/td/table/tbody//tr["
								+ x + "]/td[" + y + "]"))
						.getText();
				if (county.trim().equalsIgnoreCase(countyName.trim())) {
					trNum_1 = x;
					tdNum_1 = y;
				}
			}
		}

		System.out.println("x : " + trNum_1);
		System.out.println("y : " + tdNum_1);

		driver.findElement(By
				.xpath(".//*[@id='siteMainTable']/tbody/tr/td/table/tbody/tr[4]/td[2]/table/tbody/tr[1]/td/table/tbody//tr["
						+ trNum_1 + "]/td[" + tdNum_1 + "]/input"))
				.click();

		fc.utobj().clickElement(driver, pobj2.nextBtn);

		fc.utobj().selectValFromMultiSelect(driver, pobj2.salesTerritoryMS, salesTerritory);

		return salesTerritory;
	}

	@Test(groups = { "sales","sales_failed" , "TC_Sales_SalesTerritory_003" }) // Verified : Working Fine // Akshat
	@TestCase(createdOn = "2017-07-06", updatedOn = "2017-07-06", testCaseId = "TC_Sales_SalesTerritory_003", testCaseDescription = "Add and Modify Sales Territory in Admin Sales,Check that the Zip Based Domestic sales territory is added in the sales territory page.")

	void modifySalesTerritoryZip() throws Exception {
		FranconnectUtil fc = new FranconnectUtil();

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("sales", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		String salesTerritory = dataSet.get("salesTerritory");
		salesTerritory = fc.utobj().generateTestData(salesTerritory);
		String newSalesTerritory = fc.utobj().generateTestData(dataSet.get("salesTerritory"));
		String salesTerritory2 = fc.utobj().generateTestData(dataSet.get("salesTerritory"));
		String newSalesTerritory2 = fc.utobj().generateTestData(dataSet.get("salesTerritory"));
		String zipCode = fc.utobj().generateRandomNumber();
		String zipCode2 = fc.utobj().generateRandomNumber();

		try {
			driver = fc.loginpage().login(driver);
			fc.utobj().printTestStep("Go to Admin Sales territory section \nAdd Sales Territory");
			salesTerritory = addSalesTerritoryRandomZip(driver, salesTerritory, zipCode, "Domestic");
			AdminFranchiseSalesManageSalesTerritoriesPage pobj = new AdminFranchiseSalesManageSalesTerritoriesPage(
					driver);
			//
			fc.utobj().selectValFromMultiSelect(driver, pobj.salesTerritoryMs, salesTerritory);
			List<WebElement> list = pobj.domesticSalesTerritoriesList;
			fc.utobj().printTestStep("Modify Sales Territory");
			// fc.utobj().selectActionIcon(driver,list,
			// salesTerritory,"Modify");
			fc.utobj().actionImgOption(driver, salesTerritory, "Modify");

			AdminFranchiseSalesManageSalesTerritoryCreateSalesTerritoryPage pobj2 = new AdminFranchiseSalesManageSalesTerritoryCreateSalesTerritoryPage(
					driver);
			String updatedValue = pobj2.ziplist.getAttribute("value") + fc.utobj().generateRandomChar();
			pobj2.ziplist.clear();
			fc.utobj().sendKeys(driver, pobj2.ziplist, updatedValue);
			fc.utobj().clickElement(driver, pobj2.submitBtn);

			fc.utobj().selectValFromMultiSelect(driver, pobj.salesTerritoryMs, salesTerritory);

			fc.utobj().clickElement(driver, pobj2.searchBtn);
			// System.out.println("1");
			fc.utobj().sleep();
			fc.utobj().assertSingleLinkText(driver, salesTerritory);
			fc.utobj().getElementByLinkText(driver, salesTerritory).click();
			boolean isTrue = fc.sales().sales_common().assertValuePresentInCbox(driver, updatedValue);

			if (isTrue == false) {
				fc.utobj().throwsException("Sales territory info not found in cbox / overlay");
			}
			// newSalesTerritory=addSalesTerritoryRandomZip(driver,newSalesTerritory);
			fc.utobj().clickElement(driver, pobj.addNewSalesTerritory);
			fc.utobj().selectDropDownByVisibleText(driver, pobj2.category, "Domestic");
			fc.utobj().sendKeys(driver, pobj2.salesTerritoryName, newSalesTerritory);
			fc.utobj().selectDropDownByVisibleText(driver, pobj2.groupBy, "Zip / Postal Code");
			fc.utobj().clickElement(driver, pobj2.selectAllCountry);
			fc.utobj().clickElement(driver, pobj2.commaSeparatedListZip);
			fc.utobj().sendKeys(driver, pobj2.ziplist, updatedValue);
			fc.utobj().clickElement(driver, pobj2.submitBtn);
			fc.utobj().printTestStep("Option for Back, Proceed and Override and Proceed will come.");
			boolean isBack = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//input[@value='Back']");
			boolean isProceed = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//input[@value='Proceed']");
			boolean isOverideAndProceed = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//input[@value='Override and Proceed']");
			if (isBack == false || isProceed == false || isOverideAndProceed == false) {
				fc.utobj().throwsException("Option for Back, Proceed ,Override and Proceed not verified.");
			}
			fc.utobj().printTestStep("Click on Proceed.");
			fc.utobj().clickElement(driver, pobj2.proceedBtn);
			fc.utobj().selectValFromMultiSelect(driver, pobj.salesTerritoryMs, newSalesTerritory);
			fc.utobj().clickElement(driver, pobj2.searchBtn);
			fc.utobj().sleep();
			//System.out.println("2");

			// fc.utobj().clickElement(driver,
			// fc.utobj().getElementByXpath(driver,".//a[contains(text(),'"+newSalesTerritory+"')]")));
			fc.utobj().clickLink(driver, newSalesTerritory);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().printTestStep("Another territory should not get created with same Zip / Postal Code ");
			boolean isProceedWorked = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text(),'No details available for this Sales Territory.')]");
			if (isProceedWorked == false) {
				fc.utobj().throwsException(
						"New sales teritory added with old Zip code of another teritory,with proceed button funtionality not working");
			}
			fc.utobj().clickElement(driver, pobj.closeBtn);
			fc.utobj().selectValFromMultiSelect(driver, pobj.salesTerritoryMs, salesTerritory);
			fc.utobj().clickElement(driver, pobj2.searchBtn);
			fc.utobj().sleep();
			// fc.utobj().clickElement(driver,
			// fc.utobj().getElementByXpath(driver,".//a[contains(text(),'"+salesTerritory+"')]")));
			fc.utobj().clickLink(driver, salesTerritory);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().printTestStep("Another territory should not get created with same Zip / Postal Code ");
			isProceedWorked = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//td[contains(text(),'" + updatedValue + "')]");
			if (isProceedWorked == false) {
				fc.utobj().throwsException(
						"New sales teritory added with old Zip code of another teritory,with proceed button funtionality not working");
			}
			fc.utobj().clickElement(driver, pobj.closeBtn);

			fc.utobj().printTestStep("Go to Admin > Sales > Manage Sales Territory > Create Sales Territory");
			fc.utobj().printTestStep(
					"Select Category International, Enter Sales Territory Name, Group By as Zip / Postal Code");
			fc.utobj().printTestStep("Check Country Name for which the Zip / Postal Code is to be entered");
			fc.utobj().printTestStep("Enter the new zip code");
			fc.utobj().printTestStep("Submit");
			salesTerritory2 = addSalesTerritoryRandomZip(driver, salesTerritory2, zipCode2, "Domestic");
			fc.utobj().printTestStep(
					"Now follow the same step to create Territory and enter the same Zip / Postal Code ");
			fc.utobj().clickElement(driver, pobj.addNewSalesTerritory);
			fc.utobj().selectDropDownByVisibleText(driver, pobj2.category, "Domestic");
			fc.utobj().sendKeys(driver, pobj2.salesTerritoryName, newSalesTerritory2);
			fc.utobj().selectDropDownByVisibleText(driver, pobj2.groupBy, "Zip / Postal Code");
			fc.utobj().clickElement(driver, pobj2.selectAllCountry);
			fc.utobj().clickElement(driver, pobj2.commaSeparatedListZip);
			// String zipCode = fc.utobj().generateRandomNumber();
			fc.utobj().sendKeys(driver, pobj2.ziplist, zipCode2);
			fc.utobj().clickElement(driver, pobj2.submitBtn);
			fc.utobj().printTestStep("Option for Back, Proceed and Override and Proceed will come.");
			isBack = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//input[@value='Back']");
			isProceed = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//input[@value='Proceed']");
			isOverideAndProceed = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//input[@value='Override and Proceed']");
			if (isBack == false || isProceed == false || isOverideAndProceed == false) {
				fc.utobj().throwsException("Option for Back, Proceed ,Override and Proceed not verified.");
			}
			fc.utobj().printTestStep("Click on Override and Proceed.");
			fc.utobj().clickElement(driver, pobj2.overrideandProceedBtn);
			fc.utobj().selectValFromMultiSelect(driver, pobj.salesTerritoryMs, newSalesTerritory2);
			fc.utobj().clickElement(driver, pobj2.searchBtn);
			fc.utobj().sleep();
			
			// fc.utobj().clickElement(driver,
			// fc.utobj().getElementByXpath(driver,".//a[contains(text(),'"+newSalesTerritory2+"')]")));
			fc.utobj().clickLink(driver, newSalesTerritory2);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().printTestStep("Another territory should get created with same Zip / Postal Code ");
			boolean isOverrideAndProceedWorked = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text(),'" + zipCode2 + "')]");
			if (isOverrideAndProceedWorked == false) {
				fc.utobj().throwsException(
						"New sales teritory added with old Zip code of another teritory,with override and proceed button funtionality not working");
			}
			fc.utobj().clickElement(driver, pobj.closeBtn);
			fc.utobj().selectValFromMultiSelect(driver, pobj.salesTerritoryMs, salesTerritory2);
			fc.utobj().clickElement(driver, pobj2.searchBtn);
			fc.utobj().sleep();
			
			fc.utobj().clickLink(driver, salesTerritory2);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			isOverrideAndProceedWorked = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text(),'No details available for this Sales Territory.')]");
			if (isOverrideAndProceedWorked == false) {
				fc.utobj().throwsException(
						"New sales teritory added with old Zip code of another teritory,with override and proceed button funtionality not working");
			}
			fc.utobj().clickElement(driver, pobj.closeBtn);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			// e.printStackTrace();
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "sales", "sales_failed" , "TC_Sales_SalesTerritory_004" }) // Verified // Akshat
	@TestCase(createdOn = "2017-07-06", updatedOn = "2017-07-10", testCaseId = "TC_Sales_SalesTerritory_004", testCaseDescription = "Check that the Zip Based International sales territory is added in the sales territory page.")

	void checkZipBasedInternationalSalesTeritory() throws Exception {
		FranconnectUtil fc = new FranconnectUtil();

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("sales", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		String salesTerritory = dataSet.get("salesTerritory");
		salesTerritory = fc.utobj().generateTestData(salesTerritory);
		String newSalesTerritory = fc.utobj().generateTestData(dataSet.get("salesTerritory"));
		String salesTerritory2 = fc.utobj().generateTestData(dataSet.get("salesTerritory"));
		String newSalesTerritory2 = fc.utobj().generateTestData(dataSet.get("salesTerritory"));
		String zipCode = fc.utobj().generateRandomNumber();
		String zipCode2 = fc.utobj().generateRandomNumber();

		try {
			driver = fc.loginpage().login(driver);

			fc.utobj().printTestStep("Go to Admin Sales territory section \nAdd Sales Territory");
			salesTerritory = addSalesTerritoryRandomZip(driver, salesTerritory, zipCode, "International");
			AdminFranchiseSalesManageSalesTerritoriesPage pobj = new AdminFranchiseSalesManageSalesTerritoriesPage(
					driver);
			AdminFranchiseSalesManageSalesTerritoryCreateSalesTerritoryPage pobj2 = new AdminFranchiseSalesManageSalesTerritoryCreateSalesTerritoryPage(
					driver);

			fc.utobj().selectValFromMultiSelect(driver, pobj.salesTerritoryMs, salesTerritory);
			// List<WebElement> list = pobj.domesticSalesTerritoriesList;
			fc.utobj().printTestStep("Modify Sales Territory");
			
			fc.utobj().actionImgOption(driver, salesTerritory, "Modify");
			String updatedValue = pobj2.ziplist.getAttribute("value") + fc.utobj().generateRandomChar();
			fc.utobj().sendKeys(driver, pobj2.ziplist, updatedValue);
			fc.utobj().clickElement(driver, pobj2.submitBtn);

			fc.utobj().selectValFromMultiSelect(driver, pobj.salesTerritoryMs, salesTerritory);

			fc.utobj().clickElement(driver, pobj2.searchBtn);
			fc.utobj().sleep();
			fc.utobj().assertSingleLinkText(driver, salesTerritory);
			fc.utobj().getElementByLinkText(driver, salesTerritory).click();
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			/*
			 * String pageSource = driver.getPageSource();
			 * if(pageSource.contains(testData)){ recordFound = true; }
			 * sleep(1000); driver.switchTo().window(windowHandle);
			 * //fc.utobj().getElementByXpath(driver,".//*[@id='cboxClose']")).
			 * click(); clickElement(driver,
			 * fc.utobj().getElementByID(driver,"cboxClose")));
			 * driver.switchTo().window(windowHandle);
			 */
			fc.utobj().sleep();
			boolean isTrue = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//td[contains(text(),'" + updatedValue + "')]");
			if (isTrue == false) {
				fc.utobj().throwsException("Sales territory info not found in cbox / overlay");
			}
			fc.utobj().clickElement(driver, pobj.closeCboxBtn);
			fc.utobj().switchFrameToDefault(driver);
			// newSalesTerritory=addSalesTerritoryRandomZip(driver,newSalesTerritory);
			fc.utobj().sleep();
			fc.utobj().clickElement(driver, pobj.addNewSalesTerritory);
			fc.utobj().selectDropDownByVisibleText(driver, pobj2.category, "International");
			fc.utobj().sendKeys(driver, pobj2.salesTerritoryName, newSalesTerritory);
			fc.utobj().selectDropDownByVisibleText(driver, pobj2.groupBy, "Zip / Postal Code");
			fc.utobj().clickElement(driver, pobj2.selectAllCountry);
			fc.utobj().clickElement(driver, pobj2.commaSeparatedListZip);
			fc.utobj().sendKeys(driver, pobj2.ziplist, updatedValue);
			fc.utobj().clickElement(driver, pobj2.submitBtn);
			/*
			 * fc.utobj().sendKeys(driver,pobj2.salesTerritoryName,
			 * newSalesTerritory);
			 * fc.utobj().selectDropDownByVisibleText(driver,pobj2.groupBy,
			 * "Zip / Postal Code");
			 * fc.utobj().clickElement(driver,pobj2.selectAllCountry);
			 * fc.utobj().clickElement(driver,pobj2.commaSeparatedListZip);
			 * fc.utobj().sendKeys(driver,pobj2.ziplist,updatedValue);
			 * fc.utobj().clickElement(driver,pobj2.submitBtn);
			 */
			fc.utobj().printTestStep("Option for Back, Proceed and Override and Proceed will come.");
			boolean isBack = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//input[@value='Back']");
			boolean isProceed = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//input[@value='Proceed']");
			boolean isOverideAndProceed = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//input[@value='Override and Proceed']");
			if (isBack == false || isProceed == false || isOverideAndProceed == false) {
				fc.utobj().throwsException("Option for Back, Proceed ,Override and Proceed not verified.");
			}
			fc.utobj().printTestStep("Click on Proceed.");
			try {
				fc.utobj().clickElement(driver, pobj2.proceedBtn);
			} catch (Exception e) {
				fc.utobj().clickElement(driver, pobj2.proceedBtn2fr);
			}
			fc.utobj().selectValFromMultiSelect(driver, pobj.salesTerritoryMs, newSalesTerritory);
			fc.utobj().clickElement(driver, pobj2.searchBtn);
			// fc.utobj().clickElement(driver,
			// fc.utobj().getElementByXpath(driver,".//a[contains(text(),'"+newSalesTerritory+"')]")));
			//System.out.println("1");
			fc.utobj().sleep();
			fc.utobj().clickLink(driver, newSalesTerritory);
			// System.out.println("2");
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().printTestStep("Another territory should not get created with same Zip / Postal Code ");
			List<String> linkArray = fc.utobj().translate("No details available for this Sales Territory.");
			String option = ".//td[contains(text(),\"" + linkArray.get(0) + "\")]";
			boolean isProceedWorked = fc.utobj().verifyElementOnVisible_ByXpath(driver, option);
			if (isProceedWorked == false) {
				fc.utobj().throwsException(
						"New sales teritory added with old Zip code of another teritory,with proceed button funtionality not working");
			}
			fc.utobj().clickElement(driver, pobj.closeBtn);
			fc.utobj().selectValFromMultiSelect(driver, pobj.salesTerritoryMs, salesTerritory);
			fc.utobj().clickElement(driver, pobj2.searchBtn);
			// fc.utobj().clickElement(driver,
			// fc.utobj().getElementByXpath(driver,".//a[contains(text(),'"+salesTerritory+"')]")));
			fc.utobj().clickLink(driver, salesTerritory);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().printTestStep("Another territory should not get created with same Zip / Postal Code ");
			isProceedWorked = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//td[contains(text(),'" + updatedValue + "')]");
			if (isProceedWorked == false) {
				fc.utobj().throwsException(
						"New sales teritory added with old Zip code of another teritory,with proceed button funtionality not working");
			}
			fc.utobj().clickElement(driver, pobj.closeBtn);

			fc.utobj().printTestStep("Go to Admin > Sales > Manage Sales Territory > Create Sales Territory");
			fc.utobj().printTestStep(
					"Select Category International, Enter Sales Territory Name, Group By as Zip / Postal Code");
			fc.utobj().printTestStep("Check Country Name for which the Zip / Postal Code is to be entered");
			fc.utobj().printTestStep("Enter the new zip code");
			fc.utobj().printTestStep("Submit");
			salesTerritory2 = addSalesTerritoryRandomZip(driver, salesTerritory2, zipCode2, "International");
			fc.utobj().printTestStep(
					"Now follow the same step to create Territory and enter the same Zip / Postal Code ");
			fc.utobj().clickElement(driver, pobj.addNewSalesTerritory);
			fc.utobj().selectDropDownByVisibleText(driver, pobj2.category, "International");
			fc.utobj().sendKeys(driver, pobj2.salesTerritoryName, newSalesTerritory2);
			fc.utobj().selectDropDownByVisibleText(driver, pobj2.groupBy, "Zip / Postal Code");
			fc.utobj().clickElement(driver, pobj2.selectAllCountry);
			fc.utobj().clickElement(driver, pobj2.commaSeparatedListZip);
			fc.utobj().sendKeys(driver, pobj2.ziplist, zipCode2);
			fc.utobj().clickElement(driver, pobj2.submitBtn);

			// String zipCode = fc.utobj().generateRandomNumber();
			/*
			 * fc.utobj().sendKeys(driver,pobj2.ziplist,zipCode2);
			 * fc.utobj().clickElement(driver,pobj2.submitBtn);
			 */
			fc.utobj().printTestStep("Option for Back, Proceed and Override and Proceed will come.");
			isBack = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//input[@value='Back']");
			isProceed = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//input[@value='Proceed']");
			isOverideAndProceed = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//input[@value='Override and Proceed']");
			if (isBack == false || isProceed == false || isOverideAndProceed == false) {
				fc.utobj().throwsException("Option for Back, Proceed ,Override and Proceed not verified.");
			}
			fc.utobj().printTestStep("Click on Override and Proceed.");
			try {
				fc.utobj().clickElement(driver, pobj2.overrideandProceedBtn);
			} catch (Exception e) {
				fc.utobj().clickElement(driver, pobj2.overrideandProceedBtn2fr);
			}
			fc.utobj().selectValFromMultiSelect(driver, pobj.salesTerritoryMs, newSalesTerritory2);
			fc.utobj().clickElement(driver, pobj2.searchBtn);
			// fc.utobj().clickElement(driver,
			// fc.utobj().getElementByXpath(driver,".//a[contains(text(),'"+newSalesTerritory2+"')]")));
			fc.utobj().clickLink(driver, newSalesTerritory2);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().printTestStep("Another territory should get created with same Zip / Postal Code ");
			boolean isOverrideAndProceedWorked = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text(),'" + zipCode2 + "')]");
			if (isOverrideAndProceedWorked == false) {
				fc.utobj().throwsException(
						"New sales teritory added with old Zip code of another teritory,with override and proceed button funtionality not working");
			}
			fc.utobj().clickElement(driver, pobj.closeBtn);
			fc.utobj().selectValFromMultiSelect(driver, pobj.salesTerritoryMs, salesTerritory2);
			fc.utobj().clickElement(driver, pobj2.searchBtn);
			fc.utobj().clickLink(driver, salesTerritory2);
			fc.commonMethods().switch_cboxIframe_frameId(driver);

			linkArray = fc.utobj().translate("No details available for this Sales Territory.");
			option = ".//td[contains(text(),\"" + linkArray.get(0) + "\")]";
			isOverrideAndProceedWorked = fc.utobj().verifyElementOnVisible_ByXpath(driver, option);
			if (isOverrideAndProceedWorked == false) {
				fc.utobj().throwsException(
						"New sales teritory added with old Zip code of another teritory,with override and proceed button funtionality not working");
			}
			fc.utobj().clickElement(driver, pobj.closeBtn);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			e.printStackTrace();
			// fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "sales", "salersertmark" , "TC_Admin_FS_003" }) // Verified : Akshat
	@TestCase(createdOn = "2017-05-01", updatedOn = "2017-07-07", testCaseId = "TC_Admin_FS_003", testCaseDescription = "Add and Delete Sales Territory in Admin Sales")
	void deleteSalesTerritoryZip() throws Exception {
		FranconnectUtil fc = new FranconnectUtil();

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("sales", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		String salesTerritory = dataSet.get("salesTerritory");
		String zipCode = fc.utobj().generateRandomNumber();

		salesTerritory = fc.utobj().generateTestData(salesTerritory);
		try {
			driver = fc.loginpage().login(driver);
			AdminFranchiseSalesManageSalesTerritoryCreateSalesTerritoryPageTest p1 = new AdminFranchiseSalesManageSalesTerritoryCreateSalesTerritoryPageTest();
			fc.utobj().printTestStep("Go to Admin Sales territory section \nAdd Sales Territory");
			salesTerritory = p1.addSalesTerritoryRandomZip(driver, salesTerritory, zipCode, "Domestic");
			AdminFranchiseSalesManageSalesTerritoriesPage pobj = new AdminFranchiseSalesManageSalesTerritoriesPage(
					driver);

			fc.utobj().selectValFromMultiSelect(driver, pobj.salesTerritoryMs, salesTerritory);
			List<WebElement> list = pobj.domesticSalesTerritoriesList;
			fc.utobj().printTestStep("Delete Sales Territory");
			// fc.utobj().selectActionIcon(driver,list,
			// salesTerritory,"Delete");
			fc.utobj().actionImgOption(driver, salesTerritory, "Delete");
			fc.utobj().acceptAlertBox(driver);

			boolean valExist = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//*[contains(text(),'" + salesTerritory + "')]");
			if (valExist == true) {
				fc.utobj().throwsException("Sales Territory not getting deleted");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "sales", "salesjuly0012" })
	@TestCase(createdOn = "2017-07-05", updatedOn = "2017-07-19", testCaseId = "TC_Sales_SalesTerritory_001", testCaseDescription = "Check that the Domestic sales territory is added in the sales territory page.")
	void verifySalesTeritoryZipStateCountry() throws Exception {
		FranconnectUtil fc = new FranconnectUtil();

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("sales", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			AdminFranchiseSalesManageSalesTerritoryCreateSalesTerritoryPage pobj2 = new AdminFranchiseSalesManageSalesTerritoryCreateSalesTerritoryPage(
					driver);
			AdminFranchiseSalesManageSalesTerritoriesPage pobj = new AdminFranchiseSalesManageSalesTerritoriesPage(
					driver);

			String salesTerritory = fc.utobj().generateTestData(dataSet.get("salesTerritory"));
			String salesTerritory1 = fc.utobj().generateTestData(dataSet.get("salesTerritory"));

			fc.utobj().printTestStep("Create a Corporate User and login");
			/*
			 * String userFullName = corpuserObj.addCorporateUser(driver,
			 * userName, password, config, emailId);
			 * fc.home_page().logout(driver, config);
			 * fc.loginpage().loginWithParameter(driver, userName,
			 * "T0n1ght123");
			 */
			fc.utobj().printTestStep("Go to Admin > Sales > Manage Sales Territory > Create Sales Territory");
			AdminSales adsales = new AdminSales();
			adsales.adminManageSalesTerritory(driver);
			//
			fc.utobj().printTestStep("Select Category Domestic, Enter Sales Territory Name, Group By as State");
			fc.utobj().clickElement(driver, pobj.addNewSalesTerritory);
			fc.utobj().selectDropDownByVisibleText(driver, pobj2.category, "Domestic");
			fc.utobj().sendKeys(driver, pobj2.salesTerritoryName, salesTerritory);
			fc.utobj().selectDropDownByVisibleText(driver, pobj2.groupBy, "States");
			fc.utobj().clickElement(driver, pobj2.selectAllCountry);
			fc.utobj().printTestStep(
					"Check Country Name for which the state is to be selected,Check state name (if no state is available - print the test case as skip exception)");
			try {
				fc.utobj().printTestStep("if states are available select the first state and save the record");

				fc.utobj().clickElement(driver, pobj2.firstStateChkBox);
			} catch (Exception e) {
				fc.utobj().printTestStep(
						"Check state name (if no state is available - print the test case as skip exception)");
				boolean allStateAlreadyAssigned = fc.utobj().verifyElementOnVisible_ByXpath(driver,
						".//td[contains(text(),'All the states for this country have already been assigned.')]");
				if (allStateAlreadyAssigned == true) {
					fc.utobj().throwsSkipException("All the states for this country have already been assigned.");
				}
				fc.utobj().throwsException("Unable to create domestic sales teritory ");
			}
			fc.utobj().clickElement(driver, pobj2.submitBtn);
			fc.utobj().selectValFromMultiSelect(driver, pobj.salesTerritoryMs, salesTerritory);
			fc.utobj().clickElement(driver, pobj2.searchBtn);

			fc.utobj().printTestStep("Check that the sales territory is added in the sales territory page.");
			boolean isSalesTeritoryAdded = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text(),'" + salesTerritory + "')]");
			if (isSalesTeritoryAdded == false) {
				fc.utobj().throwsException("Sales teritory not got added");
			}

			boolean isSalesTeritoryAdded1 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text(),'" + salesTerritory
							+ "')]/ancestor::td/following-sibling::td[contains(text(),'"
							+ fc.utobj().translateString("States") + "')]");
			if (isSalesTeritoryAdded1 == false) {
				fc.utobj().throwsException("Sales states teritory not got added");
			}
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//a[contains(text(),'" + salesTerritory + "')]"));

			fc.commonMethods().switch_cboxIframe_frameId(driver);
			boolean isSalesTeritoryAdded3 = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text(),'" + fc.utobj().translateString("USA") + "')]");
			if (isSalesTeritoryAdded3 == false) {
				fc.utobj().throwsException("Details of Territory: Country USA not verified");
			}
			fc.utobj().clickElement(driver, pobj.closeBtn);
			fc.utobj().switchFrameToDefault(driver);
			// fc.utobj().selectActionIcon(driver,list,
			// salesTerritory,"Delete");
			fc.utobj().actionImgOption(driver, salesTerritory, "Modify");
			fc.utobj().sendKeys(driver, pobj2.salesTerritoryName, salesTerritory1);
			fc.utobj().clickElement(driver, pobj2.selectAllCountry);
			fc.utobj().clickElement(driver, pobj2.submitBtn);
			fc.utobj().selectValFromMultiSelect(driver, pobj.salesTerritoryMs, salesTerritory1);
			fc.utobj().clickElement(driver, pobj2.searchBtn);
			fc.utobj().printTestStep("Check that the sales territory is Deleted on the sales territory page.");

			isSalesTeritoryAdded = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[contains(text(),'" + salesTerritory1 + "')]");
			if (isSalesTeritoryAdded == false) {
				fc.utobj().throwsException("Sales teritory not got added");
			}
			fc.utobj().actionImgOption(driver, salesTerritory1, "Delete");
			fc.utobj().acceptAlertBox(driver);

			try {
				fc.utobj().selectValFromMultiSelect(driver, pobj.salesTerritoryMs, salesTerritory1);
				fc.utobj().throwsException("Deleted Sales territory still coming in multiselect search dropdown");
			} catch (Exception e) {
				fc.utobj().printTestStep("territory is Deleted is verified on the sales territory page.");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "sales","sales_failed" , "TC_Sales_SalesTerritory_002" }) // Fixed : Verified // Akshat
	@TestCase(createdOn = "2017-07-05", updatedOn = "2017-07-05", testCaseId = "TC_Sales_SalesTerritory_002", testCaseDescription = "Check that the International sales territory is added in the sales territory page.")
	void verifySalesTeritory() throws Exception {

		FranconnectUtil fc = new FranconnectUtil();

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("sales", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corpuserObj = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			AdminFranchiseSalesManageSalesTerritoryCreateSalesTerritoryPage pobj2 = new AdminFranchiseSalesManageSalesTerritoryCreateSalesTerritoryPage(
					driver);
			AdminFranchiseSalesManageSalesTerritoriesPage pobj = new AdminFranchiseSalesManageSalesTerritoriesPage(
					driver);

			String salesTerritory = fc.utobj().generateTestData(dataSet.get("salesTerritory"));
			String userName = fc.utobj().generateTestData(dataSet.get("userName"));

			fc.utobj().printTestStep("Create a Corporate User and login");
			String emailId = "salesautomation@staffex.com";
			CorporateUser corpUser = new CorporateUser();
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corpUser.setEmail(emailId);
			corpUser.setUserName(userName);
			corpUser.setPassword("T0n1ght123");
			corpUser.setConfirmPassword("T0n1ght123");
			corpUser = corpuserObj.createDefaultUser(driver, corpUser);
			fc.home_page().logout(driver);
			fc.loginpage().loginWithParameter(driver, userName, "T0n1ght123");

			fc.utobj().printTestStep("Go to Admin > Sales > Manage Sales Territory > Create Sales Territory");
			AdminSales adsales = new AdminSales();
			adsales.adminManageSalesTerritory(driver);
			//
			fc.utobj().printTestStep("Select Category International, Enter Sales Territory Name, Group By as State");
			fc.utobj().clickElement(driver, pobj.addNewSalesTerritory);
			fc.utobj().selectDropDownByVisibleText(driver, pobj2.category, "International");
			fc.utobj().sendKeys(driver, pobj2.salesTerritoryName, salesTerritory);
			fc.utobj().selectDropDownByVisibleText(driver, pobj2.groupBy, "States");
			// fc.utobj().clickElement(driver,pobj2.selectAllCountry);
			fc.utobj().printTestStep(
					"Check state name (if no state is available - print the test case as skip exception)");

			try {
				fc.utobj().printTestStep("if states are available select the first state and save the record");
				fc.utobj().clickElement(driver, pobj2.selectAllCountry);
				fc.utobj().clickElement(driver, pobj2.submitBtn);
				fc.utobj().clickElement(driver, pobj2.firstStateChkBox);
				fc.utobj().clickElement(driver, pobj2.nextBtn);

			} catch (Exception e) {
				boolean allStateAlreadyAssigned = fc.utobj().verifyElementOnVisible_ByXpath(driver,
						".//td[contains(text(),'All the states for this country have already been assigned.')]");
				if (allStateAlreadyAssigned == true) {
					fc.utobj().throwsSkipException("All the states for this country have already been assigned.");
				}
				fc.utobj().throwsException("Unable to create International sales teritory ");
			}
			fc.utobj().printTestStep("Check that the sales territory is added in the sales territory page.");
			boolean isSalesTeritoryAdded = fc.utobj().assertLinkPartialText(driver, salesTerritory);
			if (isSalesTeritoryAdded == false) {
				fc.utobj().throwsException("Sales teritory not got added");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			// e.printStackTrace();
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "sales","sales_failed" , "TC_Sales_SalesTerritory_005"}) // Fixed : Verified // Akshat
	@TestCase(createdOn = "2017-07-10", updatedOn = "2017-07-19", testCaseId = "TC_Sales_SalesTerritory_005", testCaseDescription = "Check that the Domestic county based sales territory is added in the sales territory page.")

	void checkCountyBasedDomesticSalesTeritory() throws Exception {

		FranconnectUtil fc = new FranconnectUtil();

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("sales", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corpuserObj = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			AdminFranchiseSalesManageSalesTerritoryCreateSalesTerritoryPage pobj2 = new AdminFranchiseSalesManageSalesTerritoryCreateSalesTerritoryPage(
					driver);
			AdminFranchiseSalesManageSalesTerritoriesPage pobj = new AdminFranchiseSalesManageSalesTerritoriesPage(
					driver);

			String salesTerritory = fc.utobj().generateTestData(dataSet.get("salesTerritory"));
			// String userName = fc.utobj().generateTestData(dataSet.get("userName"));

			fc.utobj().printTestStep("Create a Corporate User and login");
			String emailId = "salesautomation@staffex.com";
			CorporateUser corpUser = new CorporateUser();
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corpUser.setEmail(emailId);
			corpUser = corpuserObj.createDefaultUser(driver, corpUser);
			fc.home_page().logout(driver);
			fc.loginpage().loginWithParameter(driver, corpUser.getUserName() , corpUser.getPassword());

			fc.utobj().printTestStep("Go to Admin > Sales > Manage Sales Territory > Create Sales Territory");
			AdminSales adsales = new AdminSales();
			adsales.adminManageSalesTerritory(driver);
			//
			fc.utobj().printTestStep("Select Category Domestic, Enter Sales Territory Name, Group By as County");
			fc.utobj().printTestStep("Check Country Name for which the state and respective County is to be selected");
			fc.utobj().printTestStep(
					"Check state name and then County (if no state / County is available - print the test case as skip exception)");
			fc.utobj().printTestStep(
					"if states / Counties are available select the first state and its county and save the record");
			try {
				addSalesTerritoryDomesticCountyBased(driver, salesTerritory);
			} catch (Exception e) {
				boolean allStateAlreadyAssigned = fc.utobj().verifyElementOnVisible_ByXpath(driver,
						".//td[contains(text(),'All the states for this country have already been assigned.')]");
				if (allStateAlreadyAssigned == true) {
					fc.utobj().throwsSkipException("All the states for this country have already been assigned.");
				}
				fc.utobj().throwsException("Unable to create Domestic County sales teritory ");
			}
			fc.utobj().printTestStep("Check that the sales territory is added in the sales territory page.");
			fc.utobj().selectValFromMultiSelect(driver, pobj.salesTerritoryMs, salesTerritory);
			fc.utobj().clickElement(driver, pobj2.searchBtn);

			boolean isSalesTeritoryAdded = fc.utobj().assertLinkPartialText(driver, salesTerritory);
			if (isSalesTeritoryAdded == false) {
				fc.utobj().throwsException("Sales teritory not got added");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "sales","sales_failed" , "TC_Sales_SalesTerritory_006" }) // Fixed : Verified // Akshat
	@TestCase(createdOn = "2017-07-10", updatedOn = "2017-07-10", testCaseId = "TC_Sales_SalesTerritory_006", testCaseDescription = "Check that the International county based sales territory is added in the sales territory page.")

	void checkCountyBasedInternationalSalesTeritory() throws Exception {
		FranconnectUtil fc = new FranconnectUtil();

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("sales", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			// driver = fc.loginpage().login(driver,config);
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corpuserObj = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			AdminFranchiseSalesManageSalesTerritoryCreateSalesTerritoryPage pobj2 = new AdminFranchiseSalesManageSalesTerritoryCreateSalesTerritoryPage(
					driver);
			AdminFranchiseSalesManageSalesTerritoriesPage pobj = new AdminFranchiseSalesManageSalesTerritoriesPage(
					driver);

			String salesTerritory = fc.utobj().generateTestData(dataSet.get("salesTerritory"));
			// String userName = fc.utobj().generateTestData(dataSet.get("userName"));

			fc.utobj().printTestStep("Create a Corporate User and login");
			String emailId = "salesautomation@staffex.com";
			CorporateUser corpUser = new CorporateUser();
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corpUser.setEmail(emailId);
			corpUser = corpuserObj.createDefaultUser(driver, corpUser);
			fc.home_page().logout(driver);
			fc.loginpage().loginWithParameter(driver, corpUser.getUserName() , corpUser.getPassword());

			fc.utobj().printTestStep("Go to Admin > Sales > Manage Sales Territory > Create Sales Territory");
			AdminSales adsales = new AdminSales();
			adsales.adminManageSalesTerritory(driver);

			fc.utobj().clickElement(driver, pobj.addNewSalesTerritory);
			fc.utobj().selectDropDownByVisibleText(driver, pobj2.category, "International");
			fc.utobj().sendKeys(driver, pobj2.salesTerritoryName, salesTerritory);
			fc.utobj().selectDropDownByVisibleText(driver, pobj2.groupBy, "County");
			// fc.utobj().clickElement(driver,pobj2.selectAllCountry);
			// fc.utobj().clickElement(driver,pobj2.firstStateChkBox);
			fc.utobj().clickElement(driver, pobj2.firstCountryChkBox);
			// fc.utobj().clickElement(driver,pobj2.nextBtn);
			fc.utobj().clickElement(driver, pobj2.submitBtn);
			String msg = fc.utobj().acceptAlertBox(driver);
			String i18n = FranconnectUtil.config.get("i18Files");
			String alertMsg = null;
			if (i18n != null && !i18n.isEmpty()) {
				alertMsg = "Group by on county n'est pas pris en charge pour les pays autres que les etats-unis.";
			} else {
				alertMsg = "Group by on County is not supported for countries other than USA.";
			}
			if (!msg.equalsIgnoreCase(alertMsg)) {
				fc.utobj().throwsException(
						"Alert Box saying 'Group by on County is not supported for countries other than USA.' not visible");
			}
			if(fc.utobj().isAlertPresent(driver))
				{fc.utobj().acceptAlertBox(driver);}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}
}
