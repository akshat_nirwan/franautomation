package com.builds.test.fs;

import java.util.Map;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Optional;
import org.testng.annotations.Test;

import com.builds.uimaps.fs.AdminFranchiseSalesQualificationChecklistPage;
import com.builds.utilities.FranconnectUtil;
import com.builds.utilities.TestCase;

class AdminFranchiseSalesQualificationChecklistPageTest {
	FranconnectUtil fc = new FranconnectUtil();

	@Test(groups = { "sales", "TC_Admin_FS_031" }) // 
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "TC_Admin_FS_031", testCaseDescription = "Add Qualification Checklist in Admin Sales")
	private void addQualificationChecklist() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("sales", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		String qualificationChecklist = dataSet.get("qualificationChecklist");

		try {
			driver = fc.loginpage().login(driver);
			fc.utobj().printTestStep("Go to Sales Qualification Checklist \nAdd Qualification Checklist");
			addQualificationChecklist(driver, qualificationChecklist);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	String addQualificationChecklist(WebDriver driver, @Optional() String qualificationChecklist) throws Exception {
		boolean isValFound = false;
		AdminSales adsales = new AdminSales();
		adsales.adminFranchiseSalesConfigureQualificationChecklistsPage(driver);

		AdminFranchiseSalesQualificationChecklistPage pobj = new AdminFranchiseSalesQualificationChecklistPage(driver);

		try {
			fc.utobj().clickElement(driver, pobj.addQualificationChecklist);
		} catch (Exception e) {
			fc.utobj().clickElement(driver, pobj.addQualificationChecklist2fr);
		}
		fc.commonMethods().switch_cboxIframe_frameId(driver);
		qualificationChecklist = fc.utobj().generateTestData(qualificationChecklist);
		fc.utobj().sendKeys(driver, pobj.qualificationChecklistName, qualificationChecklist);
		fc.utobj().clickElement(driver, pobj.addBtn);

		fc.utobj().clickElement(driver, pobj.closeBtn);

		isValFound = fc.utobj().searchInSelectBoxSingleValue(driver, pobj.listing, qualificationChecklist);
		if (isValFound == false) {
			fc.utobj().throwsException("Qualification Checklist Not added!");
		}

		return qualificationChecklist;
	}

	@Test(groups = { "sales" , "TC_Admin_FS_032" }) // ok
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "TC_Admin_FS_032", testCaseDescription = "Add and Modify Qualification Checklist in Admin Sales")
	private void modifyQualificationChecklist() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());
		boolean isValFound = false;

		Map<String, String> dataSet = fc.utobj().readTestData("sales", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		String qualificationChecklist = dataSet.get("qualificationChecklist");

		try {
			driver = fc.loginpage().login(driver);
			AdminSales adsales = new AdminSales();
			adsales.adminFranchiseSalesConfigureQualificationChecklistsPage(driver);
			AdminFranchiseSalesQualificationChecklistPage pobj = new AdminFranchiseSalesQualificationChecklistPage(
					driver);
			fc.utobj().printTestStep("Go to Sales Qualification Checklist \nAdd Qualification Checklist");
			qualificationChecklist = addQualificationChecklist(driver, qualificationChecklist);
			fc.utobj().selectDropDown(driver, pobj.listing, qualificationChecklist);
			fc.utobj().clickElement(driver, pobj.modifyBtn);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			String actualStatusName = pobj.qualificationChecklistName.getAttribute("value");
			pobj.qualificationChecklistName.clear();
			String updatedName = actualStatusName + fc.utobj().generateRandomChar();
			fc.utobj().sendKeys(driver, pobj.qualificationChecklistName, updatedName);
			fc.utobj().printTestStep("Modify Qualification Checklist");
			fc.utobj().clickElement(driver, pobj.modifyBtn);

			fc.utobj().clickElement(driver, pobj.closeBtn);
			isValFound = fc.utobj().searchInSelectBoxSingleValue(driver, pobj.listing, updatedName);
			if (isValFound == false) {
				fc.utobj().throwsException("Modified Qualification checklist not found!");
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "sales" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "TC_Admin_FS_033", testCaseDescription = "Add and Delete Qualification Checklist in Admin Sales")
	private void deleteQualificationChecklist() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("sales", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		String qualificationChecklist = dataSet.get("qualificationChecklist");

		try {
			driver = fc.loginpage().login(driver);
			AdminSales adsales = new AdminSales();
			adsales.adminFranchiseSalesConfigureQualificationChecklistsPage(driver);
			AdminFranchiseSalesQualificationChecklistPage pobj = new AdminFranchiseSalesQualificationChecklistPage(
					driver);
			fc.utobj().printTestStep("Go to Sales Qualification Checklist \nAdd Qualification Checklist");
			qualificationChecklist = addQualificationChecklist(driver, qualificationChecklist);
			fc.utobj().selectDropDownByVisibleText(driver, pobj.listing, qualificationChecklist);
			fc.utobj().printTestStep("Delete Qualification Checklist");
			fc.utobj().clickElement(driver, pobj.deleteBtn);
			fc.utobj().acceptAlertBox(driver);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}
}
