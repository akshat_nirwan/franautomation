package com.builds.test.fs;

import org.openqa.selenium.WebDriver;

import com.builds.test.admin.AdminPageTest;
import com.builds.test.common.SalesModule;
import com.builds.utilities.FranconnectUtil;

class AdminSales {

	public void assignLeadOwnerbySalesTerritoriesSetPriority(SalesModule salesModule, WebDriver driver)
			throws Exception {
		FranconnectUtil fc = new FranconnectUtil();
		AdminPageTest admin = new AdminPageTest();
		admin.adminPage(driver);
		assignLeadOwnerbySalesTerritories(driver);
		fc.utobj().clickLink(driver, "Set Priority");
	}

	public void assignLeadOwnerbySalesTerritories(WebDriver driver) throws Exception {
		AdminPageTest admin = new AdminPageTest();
		admin.adminPage(driver);
		admin.openAssignLeadOwnerbySalesTerritories(driver);
	}

	public void adminFranchiseSalesStatusPage(WebDriver driver) throws Exception {
		AdminPageTest admin = new AdminPageTest();
		admin.adminPage(driver);
		admin.openFranchiseSalesStatusPage(driver);
	}

	public void adminConfigureProvenMatchIntegration(WebDriver driver) throws Exception {
		AdminPageTest admin = new AdminPageTest();
		admin.adminPage(driver);
		admin.openConfigureProvenMatchIntegration(driver);
	}

	public void adminConfigureNathanProfilerIntegration(WebDriver driver) throws Exception {
		AdminPageTest admin = new AdminPageTest();
		admin.adminPage(driver);
		admin.openConfigureNathanProfilerIntegration(driver);
	}

	public void adminConfigureProvenMatchIntegrationDetails(WebDriver driver) throws Exception {
		AdminPageTest admin = new AdminPageTest();
		admin.adminPage(driver);
		admin.openConfigureProvenMatchIntegrationDetails(driver);
	}

	public void adminConfigureNathanProfilerIntegrationDetails(WebDriver driver) throws Exception {
		AdminPageTest admin = new AdminPageTest();
		admin.adminPage(driver);
		admin.openConfigureNathanProfilerIntegration(driver);
	}

	public void adminFranchiseSalesLeadSourceCategory(WebDriver driver) throws Exception {
		AdminPageTest admin = new AdminPageTest();
		admin.adminPage(driver);
		admin.openFranchiseSourcePage(driver);
	}

	public void adminFranchiseSalesConfigureServiceLists(WebDriver driver) throws Exception {
		AdminPageTest admin = new AdminPageTest();
		admin.adminPage(driver);
		admin.openConfigureServiceListsPage(driver);
	}

	public void adminFranchiseSalesForecastRating(WebDriver driver) throws Exception {
		AdminPageTest admin = new AdminPageTest();
		admin.adminPage(driver);
		admin.openForecastRatingPage(driver);
	}

	public void adminFranchiseSalesLeadRating(WebDriver driver) throws Exception {
		AdminPageTest admin = new AdminPageTest();
		admin.adminPage(driver);
		admin.openLeadRatingPage(driver);
	}

	public void adminFranchiseSalesLeadKilledReason(WebDriver driver) throws Exception {
		AdminPageTest admin = new AdminPageTest();
		admin.adminPage(driver);
		admin.openConfigureLeadKilledReasonPage(driver);
	}

	public void adminManageSalesTerritory(WebDriver driver) throws Exception {
		AdminPageTest admin = new AdminPageTest();
		admin.adminPage(driver);
		admin.openSalesTerritoryPage(driver);
	}

	public void adminFranchiseSalesConfigureCoApplicantRelationship(WebDriver driver) throws Exception {
		AdminPageTest admin = new AdminPageTest();
		admin.adminPage(driver);
		admin.openConfigureCoApplicantRelationshipPage(driver);
	}

	public void adminFranchiseSalesManageWebFormGenerator(WebDriver driver) throws Exception {
		AdminPageTest admin = new AdminPageTest();
		admin.adminPage(driver);
		admin.openFranchiseSalesManageWebFormGenerator(driver);
	}

	public void adminFranchiseSalesConfigureOptOutStatusPage(WebDriver driver) throws Exception {
		AdminPageTest admin = new AdminPageTest();
		admin.adminPage(driver);
		admin.openConfigureOptOutStatusPage(driver);
	}

	public void adminFranchiseSalesBrokersTypeConfigurationPage(WebDriver driver) throws Exception {
		AdminPageTest admin = new AdminPageTest();
		admin.adminPage(driver);
		admin.openBrokersTypeConfigurationPage(driver);
	}

	public void adminFranchiseSalesLeadMarketingCode(WebDriver driver) throws Exception {
		AdminPageTest admin = new AdminPageTest();
		admin.adminPage(driver);
		admin.openMarketingCodePage(driver);
	}

	public void adminFranchiseSalesBrokersAgencyPage(WebDriver driver) throws Exception {
		AdminPageTest admin = new AdminPageTest();
		admin.adminPage(driver);
		admin.openBrokersAgencyPage(driver);
	}

	public void adminFranchiseSalesConfigureQualificationChecklistsPage(WebDriver driver) throws Exception {
		AdminPageTest admin = new AdminPageTest();
		admin.adminPage(driver);
		admin.openConfigureQualificationChecklistsPage(driver);
	}

	public void assignLeadOwners(WebDriver driver) throws Exception {
		AdminPageTest admin = new AdminPageTest();
		admin.adminPage(driver);
		admin.openAssignLeadOwnersPage(driver);
	}

	public void setupFranchiseSalesLeadOwners(WebDriver driver) throws Exception {
		AdminPageTest admin = new AdminPageTest();
		admin.adminPage(driver);
		admin.openSetupFranchiseSalesLeadOwnersPage(driver);
	}

	public void defineUnregisteredStatesProvinces(WebDriver driver) throws Exception {
		AdminPageTest admin = new AdminPageTest();
		admin.adminPage(driver);
		admin.openDefineUnregisteredStatesProvincesPage(driver);
	}

	/*
	 * public void setupEmailCampaignTriggers( WebDriver driver) throws
	 * Exception { Reporter.log("Navigating to setupEmailCampaignTriggers...");
	 * adminpage().adminPage( driver);
	 * 
	 * admin.openSetupEmailCampaignTriggersPage(driver); }
	 */

	/*
	 * public void setupAutomatedTasks( WebDriver driver) throws Exception {
	 * Reporter.log("Navigating to setupAutomatedTasks...");
	 * adminpage().adminPage( driver);
	 * 
	 * admin.openSetupAutomatedTasksPage(driver); }
	 */

	public void configureDuplicateCriteria(WebDriver driver) throws Exception {
		AdminPageTest admin = new AdminPageTest();
		admin.adminPage(driver);
		admin.openConfigureDuplicateCriteriaPage(driver);
	}

	public void manageFormGenerator(WebDriver driver) throws Exception {
		AdminPageTest admin = new AdminPageTest();
		admin.adminPage(driver);
		admin.openManageFormGeneratorPage(driver);
	}

	public void configureSearchCriteriaforTopSearch(WebDriver driver) throws Exception {
		AdminPageTest admin = new AdminPageTest();
		admin.adminPage(driver);
		admin.openConfigureSearchCriteriaforTopSearchPage(driver);
	}

	public void manageWebFormGenerator(WebDriver driver) throws Exception {
		AdminPageTest admin = new AdminPageTest();
		admin.adminPage(driver);
		admin.openManageWebFormGeneratorPage(driver);
	}

}
