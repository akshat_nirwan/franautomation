package com.builds.test.fs;

import org.openqa.selenium.WebDriver;

import com.builds.uimaps.fs.AdminSalesAssignLeadOwnerByTerritorySetupPriorityForOwnerAssignmentPage;
import com.builds.utilities.FranconnectUtil;

class AdminSalesAssignLeadOwnerByTerritorySetupPriorityForOwnerAssignmentPageTest {
	FranconnectUtil fc = new FranconnectUtil();

	public void setPriorityForOwnerAssignmentPageTest(WebDriver driver, String setPriority) throws Exception {

		AdminSales adsales = new AdminSales();
		adsales.assignLeadOwnerbySalesTerritoriesSetPriority(fc.sales(), driver);

		// adsales.assignLeadOwnerbySalesTerritoriesSetPriority(salesModule,
		// driver);

		AdminSalesAssignLeadOwnerByTerritorySetupPriorityForOwnerAssignmentPage pobj = new AdminSalesAssignLeadOwnerByTerritorySetupPriorityForOwnerAssignmentPage(
				driver);

		fc.utobj().selectDropDownByVisibleText(driver, pobj.changeSequence, setPriority);
		fc.utobj().clickElement(driver, pobj.MovetoUp);
		fc.utobj().clickElement(driver, pobj.saveBtn);
	}
}
