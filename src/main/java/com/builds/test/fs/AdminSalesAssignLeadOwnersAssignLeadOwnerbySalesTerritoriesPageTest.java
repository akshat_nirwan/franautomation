package com.builds.test.fs;

import java.util.Map;

import org.openqa.selenium.WebDriver;
import org.testng.Reporter;
import org.testng.annotations.Test;

import com.builds.test.admin.AdminUsersManageCorporateUsersAddCorporateUserPageTest;
import com.builds.test.admin.AdminUsersManageRegionalUsersAddRegionalUserPageTest;
import com.builds.test.common.CorporateUser;
import com.builds.utilities.FranconnectUtil;

class AdminSalesAssignLeadOwnersAssignLeadOwnerbySalesTerritoriesPageTest {
	FranconnectUtil fc = new FranconnectUtil();

	@Test(groups = { "ownerAssignmentConfiguration1" })
	public void configureLeadOwnerAssignment() throws Exception {
		Reporter.log(
				"TC_Admin_AssignmentScheme_001 >  Admin > Sales > Assign Lead Owners > Assign Lead Owner by Sales Territories\n");
		Reporter.log("###################################################################");
		String testCaseId = "TC_Admin_AssignmentScheme_001";

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("sales", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			AdminSales adsales = new AdminSales();
			adsales.assignLeadOwnerbySalesTerritories(driver);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	public void setPriority(WebDriver driver, String setPriority) throws Exception {
		AdminSalesAssignLeadOwnerByTerritorySetupPriorityForOwnerAssignmentPageTest p1 = new AdminSalesAssignLeadOwnerByTerritorySetupPriorityForOwnerAssignmentPageTest();
		p1.setPriorityForOwnerAssignmentPageTest(driver, setPriority);
	}

	public void createUser(WebDriver driver, String ownerType, String OwnersName, String randomNumber,
			Map<String, String> config) throws Exception {
		if (ownerType.trim().equalsIgnoreCase("Divisional User")) {

		} else if (ownerType.trim().equalsIgnoreCase("Corporate User")) {
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corpUserPageTest = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			String emailId = "salesautomation@staffex.com";
			CorporateUser corpUser = new CorporateUser();
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corpUser.setEmail(emailId);
			corpUser = corpUserPageTest.createDefaultUser(driver, corpUser);
		} else if (ownerType.trim().equalsIgnoreCase("Regional User")) {
			AdminUsersManageRegionalUsersAddRegionalUserPageTest pobj = new AdminUsersManageRegionalUsersAddRegionalUserPageTest();

			String regionName = "region" + randomNumber;
			String emailId = "salesautomation@staffex.com";
			pobj.addRegionalUser(driver, OwnersName, regionName, emailId);
		}
	}

}
