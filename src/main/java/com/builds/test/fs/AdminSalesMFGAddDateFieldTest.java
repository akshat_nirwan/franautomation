package com.builds.test.fs;

import java.util.Map;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.Reporter;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.builds.uimaps.fs.AdminFranchiseSalesManageFormGeneratorPage;
import com.builds.utilities.BaseUtil;
import com.builds.utilities.FranconnectUtil;
import com.builds.utilities.TestCase;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

public class AdminSalesMFGAddDateFieldTest {
	FranconnectUtil fc = new FranconnectUtil();

	@DataProvider(name = "Date")
	public Object[][] getData() {
		int count = 28;
		Object[][] data = new Object[count][2];
		for (int i = 1; i <= count; i++) {
			int j = i - 1;
			data[j][0] = String.valueOf(i);
			data[j][1] = FranconnectUtil.config.get("curTimeFolder") + "//configuration.xls";
		}

		return data;
	}

	@Test(groups = { "salesFormGenerate", "600Date" }, dataProvider = "Date")
	@TestCase(createdOn = "2017-07-26", updatedOn = "2017-08-17", testCaseId = "TC_Sales_ValidateFieldAllValue_Date", testCaseDescription = "Verify Tab in DropDown")
	private void ValidateFieldInDropDownTab(String count, String path) throws Exception {
		System.out.println(path + " " + count);
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData_FormGenerator_count(testCaseId, count);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		testCaseId = testCaseId + "_data" + count;
		Reporter.log("Test_WorkFlow_Id_ALT# : " + testCaseId);
		ExtentTest test = BaseUtil.reports.startTest(testCaseId);
		try {

			AdminFranchiseSalesManageFormGeneratorPage pobj = new AdminFranchiseSalesManageFormGeneratorPage(driver);
			fc.utobj().printTestStep(test, "Hitting Build Url and Then Login");
			driver = fc.loginpage().login(driver);
			String tabName = dataSet.get("tabDisplayName");
			String fieldName = dataSet.get("Display Name");
			String sectionName = dataSet.get("sectionName ");
			fc.utobj().printTestStep(test, "Go to Admin > Sales > Manage Form Generator");
			AdminSales adsales = new AdminSales();
			adsales.manageFormGenerator(driver);
			AdminFranchiseSalesManageFormGeneratorPageTest p1 = new AdminFranchiseSalesManageFormGeneratorPageTest();
			fc.utobj().printTestStep(test, "Manage Form Generator > Adding New Tab");
			tabName = p1.addTabName(driver, tabName);
			fc.utobj().clickPartialLinkText(driver, tabName);
			fc.utobj().printTestStep(test, "Adding Section Name");
			sectionName = p1.addSectionName(driver, sectionName);
			fc.utobj().printTestStep(test, "Adding DropDown Field Name with " + fieldName);
			String created = p1.addFieldName(driver, sectionName, fieldName, dataSet, testCaseId, test);
			if (created.equals("created")) {
				fc.utobj().printTestStep(test, "Clicking on Modify Field Button");
				fc.utobj().actionImgOption(driver, fieldName, "Modify");
				fc.commonMethods().switch_cboxIframe_frameId(driver);
				fc.utobj().clickElement(driver, pobj.closeSectionCboxBtn);
				driver.switchTo().defaultContent();
				fc.utobj().printTestStep(test, "Clicking on Delete Field Button");
				fc.utobj().actionImgOption(driver, fieldName, "Delete");
				fc.utobj().dismissAlertBox(driver);
				if (!ClickonSectionPageButton(driver, sectionName, "Add New Field").isDisplayed()
						|| !ClickonSectionPageButton(driver, sectionName, "Add Document").isDisplayed()
						|| !ClickonSectionPageButton(driver, sectionName, "Modify Section").isDisplayed()
						|| !fc.utobj().getElement(driver, pobj.previewformbtn).isDisplayed()
						|| !fc.utobj().getElement(driver, pobj.configuretabularview).isDisplayed()) {
					fc.utobj().throwsException("Some button is not displayed kindly check");
				}
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
			test.log(LogStatus.PASS, "Logout and quit");
			BaseUtil.reports.endTest(test);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
			test.log(LogStatus.FAIL, " Logout and quit with " + e.toString());
			BaseUtil.reports.endTest(test);
		}

	}

	public WebElement ClickonSectionPageButton(WebDriver driver, String sectionName, String Button) throws Exception {
		return fc.utobj().getElementByXpath(driver, ".//*[contains(text(),'" + sectionName
				+ "')]/following-sibling::td/a[contains(text(),'" + Button + "')]");
	}

}
