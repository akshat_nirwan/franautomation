package com.builds.test.fs;

import java.util.Map;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.builds.uimaps.fs.AdminFranchiseSalesManageFormGeneratorPage;
import com.builds.utilities.FranconnectUtil;
import com.builds.utilities.TestCase;

public class AdminSalesMFGAddNewFieldValidationTest {
	FranconnectUtil fc = new FranconnectUtil();

	@Test(groups = { "salesFormGenerate" })
	@TestCase(createdOn = "2017-07-26", updatedOn = "2017-07-26", testCaseId = "TC_Sales_ValidateNewField_15", testCaseDescription = "Verify Tab in DropDown")
	private void ValidateTabDropDown_valid() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());
		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("sales", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		try {
			AdminFranchiseSalesManageFormGeneratorPage pobj = new AdminFranchiseSalesManageFormGeneratorPage(driver);
			fc.utobj().printTestStep("Hitting Build Url and Then Login");
			driver = fc.loginpage().login(driver);
			String tabName = dataSet.get("tabDisplayName");
			String sectionName = dataSet.get("sectionName ");
			fc.utobj().printTestStep("Go to Admin > Sales > Manage Form Generator");

			AdminSales adsales = new AdminSales();
			adsales.manageFormGenerator(driver);
			AdminFranchiseSalesManageFormGeneratorPageTest p1 = new AdminFranchiseSalesManageFormGeneratorPageTest();
			fc.utobj().printTestStep("Manage Form Generator > Adding New Tab");
			tabName = p1.addTabName(driver, tabName);
			fc.utobj().clickPartialLinkText(driver, tabName);
			boolean found = p1.CheckInDropDown(driver, pobj.dropdown, tabName);
			if (!found) {
				fc.utobj().throwsException("Value not found in Drop down ");
			}
			String defaulttag = dataSet.get("DefaultTags");
			String[] options = defaulttag.split(",");
			for (String optins : options) {
				found = p1.CheckInDropDown(driver, pobj.dropdown, optins);
				if (!found) {
					fc.utobj().throwsException("Value not found in Drop down ");
				}
			}
			p1.CheckInDropDown(driver, pobj.dropdown, tabName);
			// hello
			fc.utobj().printTestStep("Manage Form Generator > In New Tab > Add Section ");
			sectionName = p1.addSectionName(driver, sectionName);
			p1.addFieldName(driver, sectionName, "Field", dataSet, testCaseId);
			String fieldName = "Field";

			fieldName = fc.utobj().generateTestData(fieldName);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//*[contains(text(),'" + sectionName
					+ "')]/following-sibling::td/a[contains(text(),'Add New Field')]"));
			fc.commonMethods().switch_cboxIframe_frameId(driver);

			fc.utobj().sendKeys(driver, pobj.fieldDisplayName, "");
			p1.CheckInDropDown(driver, pobj.fldValidationType, dataSet.get("Valid"));
			fc.utobj().clickElement(driver, pobj.submitFieldCBoxButton);
			String msg = fc.utobj().dismissAlertBox(driver);
			if (!msg.equals("Please enter Display Name.")) {
				fc.utobj().throwsException("validation msg not came");
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "salesFormGenerate" })
	@TestCase(createdOn = "2017-07-25", updatedOn = "2017-07-25", testCaseId = "TC_Sales_ValidateNewField_16", testCaseDescription = "Verify Tab in DropDown")
	private void ValidateTabDropDown_16() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("sales", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			AdminFranchiseSalesManageFormGeneratorPage pobj = new AdminFranchiseSalesManageFormGeneratorPage(driver);
			fc.utobj().printTestStep("Hitting Build Url and Then Login");
			driver = fc.loginpage().login(driver);
			String tabName = dataSet.get("tabDisplayName");
			String sectionName = dataSet.get("sectionName ");
			fc.utobj().printTestStep("Go to Admin > Sales > Manage Form Generator");

			AdminSales adsales = new AdminSales();
			adsales.manageFormGenerator(driver);
			AdminFranchiseSalesManageFormGeneratorPageTest p1 = new AdminFranchiseSalesManageFormGeneratorPageTest();
			fc.utobj().printTestStep("Manage Form Generator > Adding New Tab");
			tabName = p1.addTabName(driver, tabName);
			fc.utobj().clickPartialLinkText(driver, tabName);
			boolean found = p1.CheckInDropDown(driver, pobj.dropdown, tabName);
			if (!found) {
				fc.utobj().throwsException("Value not found in Drop down ");
			}
			String defaulttag = dataSet.get("DefaultTags");
			String[] options = defaulttag.split(",");
			for (String optins : options) {
				found = p1.CheckInDropDown(driver, pobj.dropdown, optins);
				if (!found) {
					fc.utobj().throwsException("Value not found in Drop down ");
				}
			}
			p1.CheckInDropDown(driver, pobj.dropdown, tabName);
			// hello
			fc.utobj().printTestStep("Manage Form Generator > In New Tab > Add Section ");
			sectionName = p1.addSectionName(driver, sectionName);
			p1.addFieldName(driver, sectionName, "Field", dataSet, testCaseId);
			String fieldName = "Field";

			fieldName = fc.utobj().generateTestData(fieldName);
			fc.utobj().clickElement(driver, ClickonSectionPageButton(driver, sectionName, "Add New Field"));
			fc.commonMethods().switch_cboxIframe_frameId(driver);

			fc.utobj().sendKeys(driver, pobj.fieldDisplayName, "!@#$");
			fc.utobj().clickElement(driver, pobj.submitFieldCBoxButton);
			String msg = fc.utobj().dismissAlertBox(driver);
			if (!msg.equals("Display Name must begin with a letter.")) {
				fc.utobj().throwsException("validation msg not came");
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "salesFormGenerate" })
	@TestCase(createdOn = "2017-07-25", updatedOn = "2017-07-25", testCaseId = "TC_Sales_ValidateNewField_17", testCaseDescription = "Verify Tab in DropDown")
	private void ValidateTabDropDown_17() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("sales", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			AdminFranchiseSalesManageFormGeneratorPage pobj = new AdminFranchiseSalesManageFormGeneratorPage(driver);
			fc.utobj().printTestStep("Hitting Build Url and Then Login");
			driver = fc.loginpage().login(driver);
			String tabName = dataSet.get("tabDisplayName");
			String sectionName = dataSet.get("sectionName ");
			fc.utobj().printTestStep("Go to Admin > Sales > Manage Form Generator");

			AdminSales adsales = new AdminSales();
			adsales.manageFormGenerator(driver);
			AdminFranchiseSalesManageFormGeneratorPageTest p1 = new AdminFranchiseSalesManageFormGeneratorPageTest();
			fc.utobj().printTestStep("Manage Form Generator > Adding New Tab");
			tabName = p1.addTabName(driver, tabName);
			fc.utobj().clickPartialLinkText(driver, tabName);
			boolean found = p1.CheckInDropDown(driver, pobj.dropdown, tabName);
			if (!found) {
				fc.utobj().throwsException("Value not found in Drop down ");
			}
			String defaulttag = dataSet.get("DefaultTags");
			String[] options = defaulttag.split(",");
			for (String optins : options) {
				found = p1.CheckInDropDown(driver, pobj.dropdown, optins);
				if (!found) {
					fc.utobj().throwsException("Value not found in Drop down ");
				}
			}
			p1.CheckInDropDown(driver, pobj.dropdown, tabName);
			// hello
			fc.utobj().printTestStep("Manage Form Generator > In New Tab > Add Section ");
			sectionName = p1.addSectionName(driver, sectionName);
			String fieldName = "Field";

			fieldName = fc.utobj().generateTestData(fieldName);
			fc.utobj().clickElement(driver, ClickonSectionPageButton(driver, sectionName, "Add New Field"));
			fc.commonMethods().switch_cboxIframe_frameId(driver);

			fc.utobj().sendKeys(driver, pobj.fieldDisplayName, "!@#$ABC");
			p1.CheckInDropDown(driver, pobj.fldValidationType, "Phone");
			fc.utobj().clickElement(driver, pobj.submitFieldCBoxButton);
			String msg = fc.utobj().dismissAlertBox(driver);
			if (!msg.equals("Display Name must begin with a letter.")) {
				fc.utobj().throwsException("validation msg not came");
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "salesFormGenerate" })
	@TestCase(createdOn = "2017-07-25", updatedOn = "2017-07-25", testCaseId = "TC_Sales_ValidateNewField_04", testCaseDescription = "Verify Tab in DropDown")
	private void ValidateTabDropDown_04() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("sales", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			AdminFranchiseSalesManageFormGeneratorPage pobj = new AdminFranchiseSalesManageFormGeneratorPage(driver);
			fc.utobj().printTestStep("Hitting Build Url and Then Login");
			driver = fc.loginpage().login(driver);
			String tabName = dataSet.get("tabDisplayName");
			String sectionName = dataSet.get("sectionName ");
			fc.utobj().printTestStep("Go to Admin > Sales > Manage Form Generator");

			AdminSales adsales = new AdminSales();
			adsales.manageFormGenerator(driver);
			AdminFranchiseSalesManageFormGeneratorPageTest p1 = new AdminFranchiseSalesManageFormGeneratorPageTest();
			fc.utobj().printTestStep("Manage Form Generator > Adding New Tab");
			tabName = p1.addTabName(driver, tabName);
			fc.utobj().clickPartialLinkText(driver, tabName);
			boolean found = p1.CheckInDropDown(driver, pobj.dropdown, tabName);
			if (!found) {
				fc.utobj().throwsException("Value not found in Drop down ");
			}
			String defaulttag = dataSet.get("DefaultTags");
			String[] options = defaulttag.split(",");
			for (String optins : options) {
				found = p1.CheckInDropDown(driver, pobj.dropdown, optins);
				if (!found) {
					fc.utobj().throwsException("Value not found in Drop down ");
				}
			}
			p1.CheckInDropDown(driver, pobj.dropdown, tabName);
			// hello
			fc.utobj().printTestStep("Manage Form Generator > In New Tab > Add Section ");
			sectionName = p1.addSectionName(driver, sectionName);
			p1.addFieldName(driver, sectionName, "Field", dataSet, testCaseId);
			String fieldName = "Field";

			fieldName = fc.utobj().generateTestData(fieldName);
			fc.utobj().clickElement(driver, ClickonSectionPageButton(driver, sectionName, "Add New Field"));
			fc.commonMethods().switch_cboxIframe_frameId(driver);

			fc.utobj().sendKeys(driver, pobj.fieldDisplayName, "A-B-C");
			fc.utobj().clickElement(driver, pobj.submitFieldCBoxButton);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "salesFormGenerate" })
	@TestCase(createdOn = "2017-07-25", updatedOn = "2017-07-25", testCaseId = "TC_Sales_ValidateNewField_05", testCaseDescription = "Verify Tab in DropDown")
	private void ValidateTabDropDown_05() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("sales", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			AdminFranchiseSalesManageFormGeneratorPage pobj = new AdminFranchiseSalesManageFormGeneratorPage(driver);
			fc.utobj().printTestStep("Hitting Build Url and Then Login");
			driver = fc.loginpage().login(driver);
			String tabName = dataSet.get("tabDisplayName");
			String sectionName = dataSet.get("sectionName ");
			fc.utobj().printTestStep("Go to Admin > Sales > Manage Form Generator");

			AdminSales adsales = new AdminSales();
			adsales.manageFormGenerator(driver);
			AdminFranchiseSalesManageFormGeneratorPageTest p1 = new AdminFranchiseSalesManageFormGeneratorPageTest();
			fc.utobj().printTestStep("Manage Form Generator > Adding New Tab");
			tabName = p1.addTabName(driver, tabName);
			fc.utobj().clickPartialLinkText(driver, tabName);
			boolean found = p1.CheckInDropDown(driver, pobj.dropdown, tabName);
			if (!found) {
				fc.utobj().throwsException("Value not found in Drop down ");
			}
			String defaulttag = dataSet.get("DefaultTags");
			String[] options = defaulttag.split(",");
			for (String optins : options) {
				found = p1.CheckInDropDown(driver, pobj.dropdown, optins);
				if (!found) {
					fc.utobj().throwsException("Value not found in Drop down ");
				}
			}
			p1.CheckInDropDown(driver, pobj.dropdown, tabName);
			// hello
			fc.utobj().printTestStep("Manage Form Generator > In New Tab > Add Section ");
			sectionName = p1.addSectionName(driver, sectionName);
			p1.addFieldName(driver, sectionName, "Field", dataSet, testCaseId);
			String fieldName = "Field";

			fieldName = fc.utobj().generateTestData(fieldName);
			fc.utobj().clickElement(driver, ClickonSectionPageButton(driver, sectionName, "Add New Field"));
			fc.commonMethods().switch_cboxIframe_frameId(driver);

			fc.utobj().sendKeys(driver, pobj.fieldDisplayName, "123456");
			fc.utobj().clickElement(driver, pobj.submitFieldCBoxButton);
			String msg = fc.utobj().dismissAlertBox(driver);
			if (msg == null) {
				fc.utobj().throwsException("validation msg not came");
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "salesFormGenerate" })
	@TestCase(createdOn = "2017-07-25", updatedOn = "2017-07-25", testCaseId = "TC_Sales_ValidateNewField_06", testCaseDescription = "Verify Tab in DropDown")
	private void ValidateTabDropDown_06() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("sales", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			AdminFranchiseSalesManageFormGeneratorPage pobj = new AdminFranchiseSalesManageFormGeneratorPage(driver);
			fc.utobj().printTestStep("Hitting Build Url and Then Login");
			driver = fc.loginpage().login(driver);
			String tabName = dataSet.get("tabDisplayName");
			String sectionName = dataSet.get("sectionName ");
			fc.utobj().printTestStep("Go to Admin > Sales > Manage Form Generator");

			AdminSales adsales = new AdminSales();
			adsales.manageFormGenerator(driver);
			AdminFranchiseSalesManageFormGeneratorPageTest p1 = new AdminFranchiseSalesManageFormGeneratorPageTest();
			fc.utobj().printTestStep("Manage Form Generator > Adding New Tab");
			tabName = p1.addTabName(driver, tabName);
			fc.utobj().clickPartialLinkText(driver, tabName);
			boolean found = p1.CheckInDropDown(driver, pobj.dropdown, tabName);
			if (!found) {
				fc.utobj().throwsException("Value not found in Drop down ");
			}
			String defaulttag = dataSet.get("DefaultTags");
			String[] options = defaulttag.split(",");
			for (String optins : options) {
				found = p1.CheckInDropDown(driver, pobj.dropdown, optins);
				if (!found) {
					fc.utobj().throwsException("Value not found in Drop down ");
				}
			}
			p1.CheckInDropDown(driver, pobj.dropdown, tabName);
			// hello
			fc.utobj().printTestStep("Manage Form Generator > In New Tab > Add Section ");
			sectionName = p1.addSectionName(driver, sectionName);
			p1.addFieldName(driver, sectionName, "Field", dataSet, testCaseId);
			String fieldName = "Field";

			fieldName = fc.utobj().generateTestData(fieldName);
			fc.utobj().clickElement(driver, ClickonSectionPageButton(driver, sectionName, "Add New Field"));
			fc.commonMethods().switch_cboxIframe_frameId(driver);

			fc.utobj().sendKeys(driver, pobj.fieldDisplayName, "75A");
			fc.utobj().clickElement(driver, pobj.submitFieldCBoxButton);
			String msg = fc.utobj().dismissAlertBox(driver);

			if (!msg.equals("Display Name must begin with a letter.")) {
				fc.utobj().throwsException("validation msg is not same");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "salesFormGenerate" })
	@TestCase(createdOn = "2017-07-25", updatedOn = "2017-07-25", testCaseId = "TC_Sales_ValidateNewField_07", testCaseDescription = "Verify Tab in DropDown")
	private void ValidateTabDropDown_07() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("sales", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			AdminFranchiseSalesManageFormGeneratorPage pobj = new AdminFranchiseSalesManageFormGeneratorPage(driver);
			fc.utobj().printTestStep("Hitting Build Url and Then Login");
			driver = fc.loginpage().login(driver);
			String tabName = dataSet.get("tabDisplayName");
			String sectionName = dataSet.get("sectionName ");
			fc.utobj().printTestStep("Go to Admin > Sales > Manage Form Generator");

			AdminSales adsales = new AdminSales();
			adsales.manageFormGenerator(driver);
			AdminFranchiseSalesManageFormGeneratorPageTest p1 = new AdminFranchiseSalesManageFormGeneratorPageTest();
			fc.utobj().printTestStep("Manage Form Generator > Adding New Tab");
			tabName = p1.addTabName(driver, tabName);
			fc.utobj().clickPartialLinkText(driver, tabName);
			boolean found = p1.CheckInDropDown(driver, pobj.dropdown, tabName);
			if (!found) {
				fc.utobj().throwsException("Value not found in Drop down ");
			}
			String defaulttag = dataSet.get("DefaultTags");
			String[] options = defaulttag.split(",");
			for (String optins : options) {
				found = p1.CheckInDropDown(driver, pobj.dropdown, optins);
				if (!found) {
					fc.utobj().throwsException("Value not found in Drop down ");
				}
			}
			p1.CheckInDropDown(driver, pobj.dropdown, tabName);
			// hello
			fc.utobj().printTestStep("Manage Form Generator > In New Tab > Add Section ");
			sectionName = p1.addSectionName(driver, sectionName);
			p1.addFieldName(driver, sectionName, "Field", dataSet, testCaseId);
			String fieldName = "Field";

			fieldName = fc.utobj().generateTestData(fieldName);
			fc.utobj().clickElement(driver, ClickonSectionPageButton(driver, sectionName, "Add New Field"));
			fc.commonMethods().switch_cboxIframe_frameId(driver);

			fc.utobj().sendKeys(driver, pobj.fieldDisplayName, "A75");
			fc.utobj().clickElement(driver, pobj.submitFieldCBoxButton);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "salesFormGenerate" })
	@TestCase(createdOn = "2017-07-25", updatedOn = "2017-07-25", testCaseId = "TC_Sales_ValidateNewField_08", testCaseDescription = "Verify Tab in DropDown")
	private void ValidateTabDropDown_08() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("sales", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			AdminFranchiseSalesManageFormGeneratorPage pobj = new AdminFranchiseSalesManageFormGeneratorPage(driver);
			fc.utobj().printTestStep("Hitting Build Url and Then Login");
			driver = fc.loginpage().login(driver);
			String tabName = dataSet.get("tabDisplayName");
			String sectionName = dataSet.get("sectionName ");
			fc.utobj().printTestStep("Go to Admin > Sales > Manage Form Generator");

			AdminSales adsales = new AdminSales();
			adsales.manageFormGenerator(driver);
			AdminFranchiseSalesManageFormGeneratorPageTest p1 = new AdminFranchiseSalesManageFormGeneratorPageTest();
			fc.utobj().printTestStep("Manage Form Generator > Adding New Tab");
			tabName = p1.addTabName(driver, tabName);
			fc.utobj().clickPartialLinkText(driver, tabName);
			boolean found = p1.CheckInDropDown(driver, pobj.dropdown, tabName);
			if (!found) {
				fc.utobj().throwsException("Value not found in Drop down ");
			}
			String defaulttag = dataSet.get("DefaultTags");
			String[] options = defaulttag.split(",");
			for (String optins : options) {
				found = p1.CheckInDropDown(driver, pobj.dropdown, optins);
				if (!found) {
					fc.utobj().throwsException("Value not found in Drop down ");
				}
			}
			p1.CheckInDropDown(driver, pobj.dropdown, tabName);
			// hello
			fc.utobj().printTestStep("Manage Form Generator > In New Tab > Add Section ");
			sectionName = p1.addSectionName(driver, sectionName);
			p1.addFieldName(driver, sectionName, "Field", dataSet, testCaseId);
			String fieldName = "Field";

			fieldName = fc.utobj().generateTestData(fieldName);

			fc.utobj().clickElement(driver, ClickonSectionPageButton(driver, sectionName, "Add New Field"));
			fc.commonMethods().switch_cboxIframe_frameId(driver);

			fc.utobj().sendKeys(driver, pobj.fieldDisplayName, "helo");
			fc.utobj().clickElement(driver, pobj.closeSectionCboxBtn);
			boolean val = fc.utobj().assertPageSource(driver, "helo");
			if (val) {
				fc.utobj().throwsException("value got added while clicking the close button");
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "salesFormGenerate" })
	@TestCase(createdOn = "2017-07-25", updatedOn = "2017-07-25", testCaseId = "TC_Sales_ValidateNewField_09", testCaseDescription = "Verify Tab in DropDown")
	private void ValidateTabDropDown_09() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("sales", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			AdminFranchiseSalesManageFormGeneratorPage pobj = new AdminFranchiseSalesManageFormGeneratorPage(driver);
			fc.utobj().printTestStep("Hitting Build Url and Then Login");
			driver = fc.loginpage().login(driver);
			String tabName = dataSet.get("tabDisplayName");
			String sectionName = dataSet.get("sectionName ");
			fc.utobj().printTestStep("Go to Admin > Sales > Manage Form Generator");

			AdminSales adsales = new AdminSales();
			adsales.manageFormGenerator(driver);
			AdminFranchiseSalesManageFormGeneratorPageTest p1 = new AdminFranchiseSalesManageFormGeneratorPageTest();
			fc.utobj().printTestStep("Manage Form Generator > Adding New Tab");
			tabName = p1.addTabName(driver, tabName);
			fc.utobj().clickPartialLinkText(driver, tabName);
			boolean found = p1.CheckInDropDown(driver, pobj.dropdown, tabName);
			if (!found) {
				fc.utobj().throwsException("Value not found in Drop down ");
			}
			String defaulttag = dataSet.get("DefaultTags");
			String[] options = defaulttag.split(",");
			for (String optins : options) {
				found = p1.CheckInDropDown(driver, pobj.dropdown, optins);
				if (!found) {
					fc.utobj().throwsException("Value not found in Drop down ");
				}
			}
			p1.CheckInDropDown(driver, pobj.dropdown, tabName);
			// hello
			fc.utobj().printTestStep("Manage Form Generator > In New Tab > Add Section ");
			sectionName = p1.addSectionName(driver, sectionName);
			p1.addFieldName(driver, sectionName, "Field", dataSet, testCaseId);
			String fieldName = "Field";

			fieldName = fc.utobj().generateTestData(fieldName);

			fc.utobj().clickElement(driver, ClickonSectionPageButton(driver, sectionName, "Add New Field"));
			fc.commonMethods().switch_cboxIframe_frameId(driver);

			fc.utobj().clickElement(driver, pobj.closeSectionCboxBtn);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "salesFormGenerate" })
	@TestCase(createdOn = "2017-07-25", updatedOn = "2017-07-25", testCaseId = "TC_Sales_ValidateNewField_10", testCaseDescription = "Verify Tab in DropDown")
	private void ValidateTabDropDown_10() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("sales", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			AdminFranchiseSalesManageFormGeneratorPage pobj = new AdminFranchiseSalesManageFormGeneratorPage(driver);
			fc.utobj().printTestStep("Hitting Build Url and Then Login");
			driver = fc.loginpage().login(driver);
			String tabName = dataSet.get("tabDisplayName");
			String sectionName = dataSet.get("sectionName ");
			fc.utobj().printTestStep("Go to Admin > Sales > Manage Form Generator");

			AdminSales adsales = new AdminSales();
			adsales.manageFormGenerator(driver);
			AdminFranchiseSalesManageFormGeneratorPageTest p1 = new AdminFranchiseSalesManageFormGeneratorPageTest();
			fc.utobj().printTestStep("Manage Form Generator > Adding New Tab");
			tabName = p1.addTabName(driver, tabName);
			fc.utobj().clickPartialLinkText(driver, tabName);
			boolean found = p1.CheckInDropDown(driver, pobj.dropdown, tabName);
			if (!found) {
				fc.utobj().throwsException("Value not found in Drop down ");
			}
			String defaulttag = dataSet.get("DefaultTags");
			String[] options = defaulttag.split(",");
			for (String optins : options) {
				found = p1.CheckInDropDown(driver, pobj.dropdown, optins);
				if (!found) {
					fc.utobj().throwsException("Value not found in Drop down ");
				}
			}
			p1.CheckInDropDown(driver, pobj.dropdown, tabName);
			// hello
			fc.utobj().printTestStep("Manage Form Generator > In New Tab > Add Section ");
			sectionName = p1.addSectionName(driver, sectionName);
			p1.addFieldName(driver, sectionName, "Field", dataSet, testCaseId);
			String fieldName = "Field";

			fieldName = fc.utobj().generateTestData(fieldName);

			fc.utobj().clickElement(driver, ClickonSectionPageButton(driver, sectionName, "Add New Field"));
			fc.commonMethods().switch_cboxIframe_frameId(driver);

			fc.utobj().sendKeys(driver, pobj.fieldDisplayName, "helo");
			fc.utobj().sendKeys(driver, pobj.dbColumnLength, "256");
			fc.utobj().clickElement(driver, pobj.submitSectionCboxBtn);
			String msg = fc.utobj().acceptAlertBox(driver);
			if (!msg.equals("Please enter the length between 1 and 255.")) {
				fc.utobj().throwsException("Validation msg is different");
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "salesFormGenerate" })
	@TestCase(createdOn = "2017-07-25", updatedOn = "2017-07-25", testCaseId = "TC_Sales_ValidateNewField_11", testCaseDescription = "Verify Tab in DropDown")
	private void ValidateTabDropDown_11() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("sales", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			AdminFranchiseSalesManageFormGeneratorPage pobj = new AdminFranchiseSalesManageFormGeneratorPage(driver);
			fc.utobj().printTestStep("Hitting Build Url and Then Login");
			driver = fc.loginpage().login(driver);
			String tabName = dataSet.get("tabDisplayName");
			String sectionName = dataSet.get("sectionName ");
			fc.utobj().printTestStep("Go to Admin > Sales > Manage Form Generator");

			AdminSales adsales = new AdminSales();
			adsales.manageFormGenerator(driver);
			AdminFranchiseSalesManageFormGeneratorPageTest p1 = new AdminFranchiseSalesManageFormGeneratorPageTest();
			fc.utobj().printTestStep("Manage Form Generator > Adding New Tab");
			tabName = p1.addTabName(driver, tabName);
			fc.utobj().clickPartialLinkText(driver, tabName);
			boolean found = p1.CheckInDropDown(driver, pobj.dropdown, tabName);
			if (!found) {
				fc.utobj().throwsException("Value not found in Drop down ");
			}
			String defaulttag = dataSet.get("DefaultTags");
			String[] options = defaulttag.split(",");
			for (String optins : options) {
				found = p1.CheckInDropDown(driver, pobj.dropdown, optins);
				if (!found) {
					fc.utobj().throwsException("Value not found in Drop down ");
				}
			}
			p1.CheckInDropDown(driver, pobj.dropdown, tabName);
			// hello
			fc.utobj().printTestStep("Manage Form Generator > In New Tab > Add Section ");
			sectionName = p1.addSectionName(driver, sectionName);
			p1.addFieldName(driver, sectionName, "Field", dataSet, testCaseId);
			String fieldName = "Field";

			fieldName = fc.utobj().generateTestData(fieldName);

			fc.utobj().clickElement(driver, ClickonSectionPageButton(driver, sectionName, "Add New Field"));
			fc.commonMethods().switch_cboxIframe_frameId(driver);

			fc.utobj().sendKeys(driver, pobj.fieldDisplayName, "helo");
			fc.utobj().sendKeys(driver, pobj.dbColumnLength, "0");
			fc.utobj().clickElement(driver, pobj.submitSectionCboxBtn);
			String msg = fc.utobj().acceptAlertBox(driver);
			if (!msg.equals("Please enter the length between 1 and 255.")) {
				fc.utobj().throwsException("Validation msg is different");
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "salesFormGenerate" })
	@TestCase(createdOn = "2017-07-25", updatedOn = "2017-07-25", testCaseId = "TC_Sales_ValidateNewField_12", testCaseDescription = "Verify Tab in DropDown")
	private void ValidateTabDropDown_12() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("sales", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			AdminFranchiseSalesManageFormGeneratorPage pobj = new AdminFranchiseSalesManageFormGeneratorPage(driver);
			fc.utobj().printTestStep("Hitting Build Url and Then Login");
			driver = fc.loginpage().login(driver);
			String tabName = dataSet.get("tabDisplayName");
			String sectionName = dataSet.get("sectionName ");
			fc.utobj().printTestStep("Go to Admin > Sales > Manage Form Generator");

			AdminSales adsales = new AdminSales();
			adsales.manageFormGenerator(driver);
			AdminFranchiseSalesManageFormGeneratorPageTest p1 = new AdminFranchiseSalesManageFormGeneratorPageTest();
			fc.utobj().printTestStep("Manage Form Generator > Adding New Tab");
			tabName = p1.addTabName(driver, tabName);
			fc.utobj().clickPartialLinkText(driver, tabName);
			boolean found = p1.CheckInDropDown(driver, pobj.dropdown, tabName);
			if (!found) {
				fc.utobj().throwsException("Value not found in Drop down ");
			}
			String defaulttag = dataSet.get("DefaultTags");
			String[] options = defaulttag.split(",");
			for (String optins : options) {
				found = p1.CheckInDropDown(driver, pobj.dropdown, optins);
				if (!found) {
					fc.utobj().throwsException("Value not found in Drop down ");
				}
			}
			p1.CheckInDropDown(driver, pobj.dropdown, tabName);
			// hello
			fc.utobj().printTestStep("Manage Form Generator > In New Tab > Add Section ");
			sectionName = p1.addSectionName(driver, sectionName);
			p1.addFieldName(driver, sectionName, "Field", dataSet, testCaseId);
			String fieldName = "Field";

			fieldName = fc.utobj().generateTestData(fieldName);

			fc.utobj().clickElement(driver, ClickonSectionPageButton(driver, sectionName, "Add New Field"));
			fc.commonMethods().switch_cboxIframe_frameId(driver);

			fc.utobj().sendKeys(driver, pobj.fieldDisplayName, "helo" + fc.utobj().getRandomChar());
			fc.utobj().sendKeys(driver, pobj.dbColumnLength, dataSet.get("dbColumnLength"));
			fc.utobj().clickElement(driver, pobj.submitSectionCboxBtn);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "salesFormGenerate" })
	@TestCase(createdOn = "2017-07-25", updatedOn = "2017-07-25", testCaseId = "TC_Sales_ValidateNewField_13", testCaseDescription = "Verify Tab in DropDown")
	private void ValidateTabDropDown_13() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("sales", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			AdminFranchiseSalesManageFormGeneratorPage pobj = new AdminFranchiseSalesManageFormGeneratorPage(driver);
			fc.utobj().printTestStep("Hitting Build Url and Then Login");
			driver = fc.loginpage().login(driver);
			String tabName = dataSet.get("tabDisplayName");
			String sectionName = dataSet.get("sectionName ");
			fc.utobj().printTestStep("Go to Admin > Sales > Manage Form Generator");

			AdminSales adsales = new AdminSales();
			adsales.manageFormGenerator(driver);
			AdminFranchiseSalesManageFormGeneratorPageTest p1 = new AdminFranchiseSalesManageFormGeneratorPageTest();
			fc.utobj().printTestStep("Manage Form Generator > Adding New Tab");
			tabName = p1.addTabName(driver, tabName);
			fc.utobj().clickPartialLinkText(driver, tabName);
			boolean found = p1.CheckInDropDown(driver, pobj.dropdown, tabName);
			if (!found) {
				fc.utobj().throwsException("Value not found in Drop down ");
			}
			String defaulttag = dataSet.get("DefaultTags");
			String[] options = defaulttag.split(",");
			for (String optins : options) {
				found = p1.CheckInDropDown(driver, pobj.dropdown, optins);
				if (!found) {
					fc.utobj().throwsException("Value not found in Drop down ");
				}
			}
			p1.CheckInDropDown(driver, pobj.dropdown, tabName);
			// hello
			fc.utobj().printTestStep("Manage Form Generator > In New Tab > Add Section ");
			sectionName = p1.addSectionName(driver, sectionName);
			p1.addFieldName(driver, sectionName, "Field", dataSet, testCaseId);
			String fieldName = "Field";

			fieldName = fc.utobj().generateTestData(fieldName);

			fc.utobj().clickElement(driver, ClickonSectionPageButton(driver, sectionName, "Add New Field"));
			fc.commonMethods().switch_cboxIframe_frameId(driver);

			fc.utobj().sendKeys(driver, pobj.fieldDisplayName, "helo" + fc.utobj().getRandomChar());
			fc.utobj().sendKeys(driver, pobj.dbColumnLength, dataSet.get("dbColumnLength"));
			fc.utobj().clickElement(driver, pobj.submitSectionCboxBtn);
			if (!fc.utobj().getElement(driver, pobj.addNewFieldBtn).isDisplayed()
					|| !fc.utobj().getElement(driver, pobj.modifySectionBtn).isDisplayed()
					|| fc.utobj().getElement(driver, pobj.addDocumentBtn).isDisplayed()
					|| !fc.utobj().getElement(driver, pobj.previewformbtn).isDisplayed()
					|| !fc.utobj().getElement(driver, pobj.configuretabularview).isDisplayed()
					|| !driver
							.findElement(By.xpath(".//*[contains(text(),'" + tabName
									+ "')]/following-sibling::*/a[contains(text(),'Modify Fields Position')]"))
							.isDisplayed()) {
				fc.utobj().throwsException("Some button is not displayed kindly check");
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "salesFormGenerate" })
	@TestCase(createdOn = "2017-07-26", updatedOn = "2017-07-26", testCaseId = "TC_Sales_ValidateNewField_14", testCaseDescription = "Verify Tab in DropDown")
	private void ValidateTabDropDown_14() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("sales", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			AdminFranchiseSalesManageFormGeneratorPage pobj = new AdminFranchiseSalesManageFormGeneratorPage(driver);
			fc.utobj().printTestStep("Hitting Build Url and Then Login");
			driver = fc.loginpage().login(driver);
			String tabName = dataSet.get("tabDisplayName");
			String fieldName = dataSet.get("Display Name");
			String sectionName = dataSet.get("sectionName ");
			fc.utobj().printTestStep("Go to Admin > Sales > Manage Form Generator");
			AdminSales adsales = new AdminSales();
			adsales.manageFormGenerator(driver);
			AdminFranchiseSalesManageFormGeneratorPageTest p1 = new AdminFranchiseSalesManageFormGeneratorPageTest();
			fc.utobj().printTestStep("Manage Form Generator > Adding New Tab");
			tabName = p1.addTabName(driver, tabName);
			fc.utobj().clickPartialLinkText(driver, tabName);
			sectionName = p1.addSectionName(driver, sectionName);
			fieldName = p1.addFieldName(driver, sectionName, fieldName);
			fc.utobj().actionImgOption(driver, fieldName, "Modify");
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().clickElement(driver, pobj.closeSectionCboxBtn);
			driver.switchTo().defaultContent();
			fc.utobj().actionImgOption(driver, fieldName, "Delete");
			fc.utobj().dismissAlertBox(driver);
			fc.utobj().clickElement(driver, pobj.closeSectionCboxBtn);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "salesFormGenerate" })
	@TestCase(createdOn = "2017-07-26", updatedOn = "2017-07-26", testCaseId = "TC_Sales_ValidateNewField_01", testCaseDescription = "Verify Tab in DropDown")
	private void ValidateTabDropDown() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("sales", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			AdminFranchiseSalesManageFormGeneratorPage pobj = new AdminFranchiseSalesManageFormGeneratorPage(driver);
			fc.utobj().printTestStep("Hitting Build Url and Then Login");
			driver = fc.loginpage().login(driver);
			String tabName = dataSet.get("tabDisplayName");
			String sectionName = dataSet.get("sectionName ");
			fc.utobj().printTestStep("Go to Admin > Sales > Manage Form Generator");
			AdminSales adsales = new AdminSales();
			adsales.manageFormGenerator(driver);
			AdminFranchiseSalesManageFormGeneratorPageTest p1 = new AdminFranchiseSalesManageFormGeneratorPageTest();
			fc.utobj().printTestStep("Manage Form Generator > Adding New Tab");
			tabName = p1.addTabName(driver, tabName);
			fc.utobj().clickPartialLinkText(driver, tabName);
			boolean found = p1.CheckInDropDown(driver, pobj.dropdown, tabName);
			if (!found) {
				fc.utobj().throwsException("Value not found in Drop down ");
			}
			String defaulttag = dataSet.get("DefaultTags");
			String[] options = defaulttag.split(",");
			for (String optins : options) {
				found = p1.CheckInDropDown(driver, pobj.dropdown, optins);
				if (!found) {
					fc.utobj().throwsException("Value not found in Drop down ");
				}
			}
			p1.CheckInDropDown(driver, pobj.dropdown, tabName);
			// hello
			fc.utobj().printTestStep("Manage Form Generator > In New Tab > Add Section ");
			sectionName = p1.addSectionName(driver, sectionName);
			p1.addFieldName(driver, sectionName, "Field", dataSet, testCaseId);
			String fieldName = "Field";

			fieldName = fc.utobj().generateTestData(fieldName);

			fc.utobj().clickElement(driver, ClickonSectionPageButton(driver, sectionName, "Add New Field"));
			fc.commonMethods().switch_cboxIframe_frameId(driver);

			fc.utobj().sendKeys(driver, pobj.fieldDisplayName, "");
			fc.utobj().clickElement(driver, pobj.submitFieldCBoxButton);
			String msg = fc.utobj().dismissAlertBox(driver);
			if (!msg.equals("Please enter Display Name.")) {
				fc.utobj().throwsException("validation msg not came");
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	public WebElement ClickonSectionPageButton(WebDriver driver, String sectionName, String Button) throws Exception {
		return fc.utobj().getElementByXpath(driver, ".//*[contains(text(),'" + sectionName
				+ "')]/following-sibling::td/a[contains(text(),'" + Button + "')]");
	}

	@Test(groups = { "salesFormGenerate" })
	@TestCase(createdOn = "2017-07-26", updatedOn = "2017-07-26", testCaseId = "TC_Sales_ValidateNewField_02", testCaseDescription = "Verify Tab in DropDown")
	private void ValidateTabDropDown_02() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("sales", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			AdminFranchiseSalesManageFormGeneratorPage pobj = new AdminFranchiseSalesManageFormGeneratorPage(driver);
			fc.utobj().printTestStep("Hitting Build Url and Then Login");
			driver = fc.loginpage().login(driver);
			String tabName = dataSet.get("tabDisplayName");
			String sectionName = dataSet.get("sectionName ");
			fc.utobj().printTestStep("Go to Admin > Sales > Manage Form Generator");
			AdminSales adsales = new AdminSales();
			adsales.manageFormGenerator(driver);
			AdminFranchiseSalesManageFormGeneratorPageTest p1 = new AdminFranchiseSalesManageFormGeneratorPageTest();
			fc.utobj().printTestStep("Manage Form Generator > Adding New Tab");
			tabName = p1.addTabName(driver, tabName);
			fc.utobj().clickPartialLinkText(driver, tabName);
			boolean found = p1.CheckInDropDown(driver, pobj.dropdown, tabName);
			if (!found) {
				fc.utobj().throwsException("Value not found in Drop down ");
			}
			String defaulttag = dataSet.get("DefaultTags");
			String[] options = defaulttag.split(",");
			for (String optins : options) {
				found = p1.CheckInDropDown(driver, pobj.dropdown, optins);
				if (!found) {
					fc.utobj().throwsException("Value not found in Drop down ");
				}
			}
			p1.CheckInDropDown(driver, pobj.dropdown, tabName);
			// hello
			fc.utobj().printTestStep("Manage Form Generator > In New Tab > Add Section ");
			sectionName = p1.addSectionName(driver, sectionName);
			p1.addFieldName(driver, sectionName, "Field", dataSet, testCaseId);
			String fieldName = "Field";

			fieldName = fc.utobj().generateTestData(fieldName);

			fc.utobj().clickElement(driver, ClickonSectionPageButton(driver, sectionName, "Add New Field"));
			fc.commonMethods().switch_cboxIframe_frameId(driver);

			fc.utobj().sendKeys(driver, pobj.fieldDisplayName, "!@#$");
			fc.utobj().clickElement(driver, pobj.submitFieldCBoxButton);
			String msg = fc.utobj().dismissAlertBox(driver);
			if (!msg.equals("Display Name must begin with a letter.")) {
				fc.utobj().throwsException("validation msg not came");
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "salesFormGenerate" })
	@TestCase(createdOn = "2017-07-26", updatedOn = "2017-07-26", testCaseId = "TC_Sales_ValidateNewField_03", testCaseDescription = "Verify Tab in DropDown")
	private void ValidateTabDropDown_03() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("sales", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			AdminFranchiseSalesManageFormGeneratorPage pobj = new AdminFranchiseSalesManageFormGeneratorPage(driver);
			fc.utobj().printTestStep("Hitting Build Url and Then Login");
			driver = fc.loginpage().login(driver);
			String tabName = dataSet.get("tabDisplayName");
			String sectionName = dataSet.get("sectionName ");
			fc.utobj().printTestStep("Go to Admin > Sales > Manage Form Generator");
			AdminSales adsales = new AdminSales();
			adsales.manageFormGenerator(driver);
			AdminFranchiseSalesManageFormGeneratorPageTest p1 = new AdminFranchiseSalesManageFormGeneratorPageTest();
			fc.utobj().printTestStep("Manage Form Generator > Adding New Tab");
			tabName = p1.addTabName(driver, tabName);
			fc.utobj().clickPartialLinkText(driver, tabName);
			boolean found = p1.CheckInDropDown(driver, pobj.dropdown, tabName);
			if (!found) {
				fc.utobj().throwsException("Value not found in Drop down ");
			}
			String defaulttag = dataSet.get("DefaultTags");
			String[] options = defaulttag.split(",");
			for (String optins : options) {
				found = p1.CheckInDropDown(driver, pobj.dropdown, optins);
				if (!found) {
					fc.utobj().throwsException("Value not found in Drop down ");
				}
			}
			p1.CheckInDropDown(driver, pobj.dropdown, tabName);
			// hello
			fc.utobj().printTestStep("Manage Form Generator > In New Tab > Add Section ");
			sectionName = p1.addSectionName(driver, sectionName);
			p1.addFieldName(driver, sectionName, "Field", dataSet, testCaseId);
			String fieldName = "Field";

			fieldName = fc.utobj().generateTestData(fieldName);

			fc.utobj().clickElement(driver, ClickonSectionPageButton(driver, sectionName, "Add New Field"));
			fc.commonMethods().switch_cboxIframe_frameId(driver);

			fc.utobj().sendKeys(driver, pobj.fieldDisplayName, "!@#$ABC");
			fc.utobj().clickElement(driver, pobj.submitFieldCBoxButton);
			String msg = fc.utobj().dismissAlertBox(driver);
			if (!msg.equals("Display Name must begin with a letter.")) {
				fc.utobj().throwsException("validation msg not came");
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "salesFormGenerate" })
	@TestCase(createdOn = "2017-07-26", updatedOn = "2017-07-26", testCaseId = "TC_Sales_ValidateNewField_18", testCaseDescription = "Verify Tab in DropDown")
	private void ValidateTabDropDown_18() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("sales", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			AdminFranchiseSalesManageFormGeneratorPage pobj = new AdminFranchiseSalesManageFormGeneratorPage(driver);
			fc.utobj().printTestStep("Hitting Build Url and Then Login");
			driver = fc.loginpage().login(driver);
			String tabName = dataSet.get("tabDisplayName");
			String sectionName = dataSet.get("sectionName ");
			fc.utobj().printTestStep("Go to Admin > Sales > Manage Form Generator");
			AdminSales adsales = new AdminSales();
			adsales.manageFormGenerator(driver);
			AdminFranchiseSalesManageFormGeneratorPageTest p1 = new AdminFranchiseSalesManageFormGeneratorPageTest();
			fc.utobj().printTestStep("Manage Form Generator > Adding New Tab");
			tabName = p1.addTabName(driver, tabName);
			fc.utobj().clickPartialLinkText(driver, tabName);
			boolean found = p1.CheckInDropDown(driver, pobj.dropdown, tabName);
			if (!found) {
				fc.utobj().throwsException("Value not found in Drop down ");
			}
			String defaulttag = dataSet.get("DefaultTags");
			String[] options = defaulttag.split(",");
			for (String optins : options) {
				found = p1.CheckInDropDown(driver, pobj.dropdown, optins);
				if (!found) {
					fc.utobj().throwsException("Value not found in Drop down ");
				}
			}
			p1.CheckInDropDown(driver, pobj.dropdown, tabName);
			// hello
			fc.utobj().printTestStep("Manage Form Generator > In New Tab > Add Section ");
			sectionName = p1.addSectionName(driver, sectionName);
			p1.addFieldName(driver, sectionName, "Field", dataSet, testCaseId);
			String fieldName = "Field";

			fieldName = fc.utobj().generateTestData(fieldName);

			fc.utobj().clickElement(driver, ClickonSectionPageButton(driver, sectionName, "Add New Field"));
			fc.commonMethods().switch_cboxIframe_frameId(driver);

			fc.utobj().sendKeys(driver, pobj.fieldDisplayName, "A-B-C");
			p1.CheckInDropDown(driver, pobj.fldValidationType, "Phone");
			fc.utobj().clickElement(driver, pobj.submitFieldCBoxButton);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "salesFormGenerate" })
	@TestCase(createdOn = "2017-07-26", updatedOn = "2017-07-26", testCaseId = "TC_Sales_ValidateNewField_19", testCaseDescription = "Verify Tab in DropDown")
	private void ValidateTabDropDown_19() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("sales", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			AdminFranchiseSalesManageFormGeneratorPage pobj = new AdminFranchiseSalesManageFormGeneratorPage(driver);
			fc.utobj().printTestStep("Hitting Build Url and Then Login");
			driver = fc.loginpage().login(driver);
			String tabName = dataSet.get("tabDisplayName");
			String sectionName = dataSet.get("sectionName ");
			fc.utobj().printTestStep("Go to Admin > Sales > Manage Form Generator");

			AdminSales adsales = new AdminSales();
			adsales.manageFormGenerator(driver);
			AdminFranchiseSalesManageFormGeneratorPageTest p1 = new AdminFranchiseSalesManageFormGeneratorPageTest();
			fc.utobj().printTestStep("Manage Form Generator > Adding New Tab");
			tabName = p1.addTabName(driver, tabName);
			fc.utobj().clickPartialLinkText(driver, tabName);
			boolean found = p1.CheckInDropDown(driver, pobj.dropdown, tabName);
			if (!found) {
				fc.utobj().throwsException("Value not found in Drop down ");
			}
			String defaulttag = dataSet.get("DefaultTags");
			String[] options = defaulttag.split(",");
			for (String optins : options) {
				found = p1.CheckInDropDown(driver, pobj.dropdown, optins);
				if (!found) {
					fc.utobj().throwsException("Value not found in Drop down ");
				}
			}
			p1.CheckInDropDown(driver, pobj.dropdown, tabName);
			// hello
			fc.utobj().printTestStep("Manage Form Generator > In New Tab > Add Section ");
			sectionName = p1.addSectionName(driver, sectionName);
			p1.addFieldName(driver, sectionName, "Field", dataSet, testCaseId);
			String fieldName = "Field";

			fieldName = fc.utobj().generateTestData(fieldName);

			fc.utobj().clickElement(driver, ClickonSectionPageButton(driver, sectionName, "Add New Field"));
			fc.commonMethods().switch_cboxIframe_frameId(driver);

			fc.utobj().sendKeys(driver, pobj.fieldDisplayName, "123456");
			p1.CheckInDropDown(driver, pobj.fldValidationType, "Phone");
			fc.utobj().clickElement(driver, pobj.submitFieldCBoxButton);
			String msg = fc.utobj().dismissAlertBox(driver);
			if (msg == null) {
				fc.utobj().throwsException("validation msg not came");
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "salesFormGenerate" })
	@TestCase(createdOn = "2017-07-26", updatedOn = "2017-07-26", testCaseId = "TC_Sales_ValidateNewField_20", testCaseDescription = "Verify Tab in DropDown")
	private void ValidateTabDropDown_20() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("sales", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			AdminFranchiseSalesManageFormGeneratorPage pobj = new AdminFranchiseSalesManageFormGeneratorPage(driver);
			fc.utobj().printTestStep("Hitting Build Url and Then Login");
			driver = fc.loginpage().login(driver);
			String tabName = dataSet.get("tabDisplayName");
			String sectionName = dataSet.get("sectionName ");
			fc.utobj().printTestStep("Go to Admin > Sales > Manage Form Generator");
			AdminSales adsales = new AdminSales();
			adsales.manageFormGenerator(driver);
			AdminFranchiseSalesManageFormGeneratorPageTest p1 = new AdminFranchiseSalesManageFormGeneratorPageTest();
			fc.utobj().printTestStep("Manage Form Generator > Adding New Tab");
			tabName = p1.addTabName(driver, tabName);
			fc.utobj().clickPartialLinkText(driver, tabName);
			boolean found = p1.CheckInDropDown(driver, pobj.dropdown, tabName);
			if (!found) {
				fc.utobj().throwsException("Value not found in Drop down ");
			}
			String defaulttag = dataSet.get("DefaultTags");
			String[] options = defaulttag.split(",");
			for (String optins : options) {
				found = p1.CheckInDropDown(driver, pobj.dropdown, optins);
				if (!found) {
					fc.utobj().throwsException("Value not found in Drop down ");
				}
			}
			p1.CheckInDropDown(driver, pobj.dropdown, tabName);
			// hello
			fc.utobj().printTestStep("Manage Form Generator > In New Tab > Add Section ");
			sectionName = p1.addSectionName(driver, sectionName);
			p1.addFieldName(driver, sectionName, "Field", dataSet, testCaseId);
			String fieldName = "Field";

			fieldName = fc.utobj().generateTestData(fieldName);

			fc.utobj().clickElement(driver, ClickonSectionPageButton(driver, sectionName, "Add New Field"));
			fc.commonMethods().switch_cboxIframe_frameId(driver);

			fc.utobj().sendKeys(driver, pobj.fieldDisplayName, "75A");

			p1.CheckInDropDown(driver, pobj.fldValidationType, "Phone");
			fc.utobj().clickElement(driver, pobj.submitFieldCBoxButton);
			String msg = fc.utobj().dismissAlertBox(driver);
			if (!msg.equals("Display Name must begin with a letter.")) {
				fc.utobj().throwsException("validation msg is not same");
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "salesFormGenerate" })
	@TestCase(createdOn = "2017-07-26", updatedOn = "2017-07-26", testCaseId = "TC_Sales_ValidateNewField_21", testCaseDescription = "Verify Tab in DropDown")
	private void ValidateTabDropDown_21() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("sales", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			AdminFranchiseSalesManageFormGeneratorPage pobj = new AdminFranchiseSalesManageFormGeneratorPage(driver);
			fc.utobj().printTestStep("Hitting Build Url and Then Login");
			driver = fc.loginpage().login(driver);
			String tabName = dataSet.get("tabDisplayName");
			String sectionName = dataSet.get("sectionName ");
			fc.utobj().printTestStep("Go to Admin > Sales > Manage Form Generator");

			AdminSales adsales = new AdminSales();
			adsales.manageFormGenerator(driver);
			AdminFranchiseSalesManageFormGeneratorPageTest p1 = new AdminFranchiseSalesManageFormGeneratorPageTest();
			fc.utobj().printTestStep("Manage Form Generator > Adding New Tab");
			tabName = p1.addTabName(driver, tabName);
			fc.utobj().clickPartialLinkText(driver, tabName);
			boolean found = p1.CheckInDropDown(driver, pobj.dropdown, tabName);
			if (!found) {
				fc.utobj().throwsException("Value not found in Drop down ");
			}
			String defaulttag = dataSet.get("DefaultTags");
			String[] options = defaulttag.split(",");
			for (String optins : options) {
				found = p1.CheckInDropDown(driver, pobj.dropdown, optins);
				if (!found) {
					fc.utobj().throwsException("Value not found in Drop down ");
				}
			}
			p1.CheckInDropDown(driver, pobj.dropdown, tabName);
			// hello
			fc.utobj().printTestStep("Manage Form Generator > In New Tab > Add Section ");
			sectionName = p1.addSectionName(driver, sectionName);
			p1.addFieldName(driver, sectionName, "Field", dataSet, testCaseId);
			String fieldName = "Field";

			fieldName = fc.utobj().generateTestData(fieldName);

			fc.utobj().clickElement(driver, ClickonSectionPageButton(driver, sectionName, "Add New Field"));
			fc.commonMethods().switch_cboxIframe_frameId(driver);

			fc.utobj().sendKeys(driver, pobj.fieldDisplayName, "A75");
			p1.CheckInDropDown(driver, pobj.fldValidationType, "Phone");
			fc.utobj().clickElement(driver, pobj.submitFieldCBoxButton);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "salesFormGenerate" })
	@TestCase(createdOn = "2017-07-26", updatedOn = "2017-07-26", testCaseId = "TC_Sales_ValidateNewField_22", testCaseDescription = "Verify Tab in DropDown")
	private void ValidateTabDropDown_22() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("sales", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			AdminFranchiseSalesManageFormGeneratorPage pobj = new AdminFranchiseSalesManageFormGeneratorPage(driver);
			fc.utobj().printTestStep("Hitting Build Url and Then Login");
			driver = fc.loginpage().login(driver);
			String tabName = dataSet.get("tabDisplayName");
			String sectionName = dataSet.get("sectionName ");
			fc.utobj().printTestStep("Go to Admin > Sales > Manage Form Generator");
			AdminSales adsales = new AdminSales();
			adsales.manageFormGenerator(driver);
			AdminFranchiseSalesManageFormGeneratorPageTest p1 = new AdminFranchiseSalesManageFormGeneratorPageTest();
			fc.utobj().printTestStep("Manage Form Generator > Adding New Tab");
			tabName = p1.addTabName(driver, tabName);
			fc.utobj().clickPartialLinkText(driver, tabName);
			boolean found = p1.CheckInDropDown(driver, pobj.dropdown, tabName);
			if (!found) {
				fc.utobj().throwsException("Value not found in Drop down ");
			}
			String defaulttag = dataSet.get("DefaultTags");
			String[] options = defaulttag.split(",");
			for (String optins : options) {
				found = p1.CheckInDropDown(driver, pobj.dropdown, optins);
				if (!found) {
					fc.utobj().throwsException("Value not found in Drop down ");
				}
			}
			p1.CheckInDropDown(driver, pobj.dropdown, tabName);
			// hello
			fc.utobj().printTestStep("Manage Form Generator > In New Tab > Add Section ");
			sectionName = p1.addSectionName(driver, sectionName);
			p1.addFieldName(driver, sectionName, "Field", dataSet, testCaseId);
			String fieldName = "Field";

			fieldName = fc.utobj().generateTestData(fieldName);

			fc.utobj().clickElement(driver, ClickonSectionPageButton(driver, sectionName, "Add New Field"));
			fc.commonMethods().switch_cboxIframe_frameId(driver);

			fc.utobj().sendKeys(driver, pobj.fieldDisplayName, "helo");
			fc.utobj().clickElement(driver, pobj.closeSectionCboxBtn);
			boolean val = fc.utobj().assertPageSource(driver, "helo");
			if (val) {
				fc.utobj().throwsException("value got added while clicking the close button");
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "salesFormGenerate" })
	@TestCase(createdOn = "2017-07-26", updatedOn = "2017-07-26", testCaseId = "TC_Sales_ValidateNewField_23", testCaseDescription = "Verify Tab in DropDown")
	private void ValidateTabDropDown_23() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("sales", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			AdminFranchiseSalesManageFormGeneratorPage pobj = new AdminFranchiseSalesManageFormGeneratorPage(driver);
			fc.utobj().printTestStep("Hitting Build Url and Then Login");
			driver = fc.loginpage().login(driver);
			String tabName = dataSet.get("tabDisplayName");
			String sectionName = dataSet.get("sectionName ");
			fc.utobj().printTestStep("Go to Admin > Sales > Manage Form Generator");

			AdminSales adsales = new AdminSales();
			adsales.manageFormGenerator(driver);
			AdminFranchiseSalesManageFormGeneratorPageTest p1 = new AdminFranchiseSalesManageFormGeneratorPageTest();
			fc.utobj().printTestStep("Manage Form Generator > Adding New Tab");
			tabName = p1.addTabName(driver, tabName);
			fc.utobj().clickPartialLinkText(driver, tabName);
			boolean found = p1.CheckInDropDown(driver, pobj.dropdown, tabName);
			if (!found) {
				fc.utobj().throwsException("Value not found in Drop down ");
			}
			String defaulttag = dataSet.get("DefaultTags");
			String[] options = defaulttag.split(",");
			for (String optins : options) {
				found = p1.CheckInDropDown(driver, pobj.dropdown, optins);
				if (!found) {
					fc.utobj().throwsException("Value not found in Drop down ");
				}
			}
			p1.CheckInDropDown(driver, pobj.dropdown, tabName);
			// hello
			fc.utobj().printTestStep("Manage Form Generator > In New Tab > Add Section ");
			sectionName = p1.addSectionName(driver, sectionName);
			p1.addFieldName(driver, sectionName, "Field", dataSet, testCaseId);
			String fieldName = "Field";

			fieldName = fc.utobj().generateTestData(fieldName);

			fc.utobj().clickElement(driver, ClickonSectionPageButton(driver, sectionName, "Add New Field"));
			fc.commonMethods().switch_cboxIframe_frameId(driver);

			fc.utobj().clickElement(driver, pobj.closeSectionCboxBtn);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "salesFormGenerate" })
	@TestCase(createdOn = "2017-07-26", updatedOn = "2017-07-26", testCaseId = "TC_Sales_ValidateNewField_24", testCaseDescription = "Verify Tab in DropDown")
	private void ValidateTabDropDown_24() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("sales", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			AdminFranchiseSalesManageFormGeneratorPage pobj = new AdminFranchiseSalesManageFormGeneratorPage(driver);
			fc.utobj().printTestStep("Hitting Build Url and Then Login");
			driver = fc.loginpage().login(driver);
			String tabName = dataSet.get("tabDisplayName");
			String sectionName = dataSet.get("sectionName ");
			fc.utobj().printTestStep("Go to Admin > Sales > Manage Form Generator");

			AdminSales adsales = new AdminSales();
			adsales.manageFormGenerator(driver);
			AdminFranchiseSalesManageFormGeneratorPageTest p1 = new AdminFranchiseSalesManageFormGeneratorPageTest();
			fc.utobj().printTestStep("Manage Form Generator > Adding New Tab");
			tabName = p1.addTabName(driver, tabName);
			fc.utobj().clickPartialLinkText(driver, tabName);
			boolean found = p1.CheckInDropDown(driver, pobj.dropdown, tabName);
			if (!found) {
				fc.utobj().throwsException("Value not found in Drop down ");
			}
			String defaulttag = dataSet.get("DefaultTags");
			String[] options = defaulttag.split(",");
			for (String optins : options) {
				found = p1.CheckInDropDown(driver, pobj.dropdown, optins);
				if (!found) {
					fc.utobj().throwsException("Value not found in Drop down ");
				}
			}
			p1.CheckInDropDown(driver, pobj.dropdown, tabName);
			// hello
			fc.utobj().printTestStep("Manage Form Generator > In New Tab > Add Section ");
			sectionName = p1.addSectionName(driver, sectionName);
			p1.addFieldName(driver, sectionName, "Field", dataSet, testCaseId);
			String fieldName = "Field";

			fieldName = fc.utobj().generateTestData(fieldName);

			fc.utobj().clickElement(driver, ClickonSectionPageButton(driver, sectionName, "Add New Field"));
			fc.commonMethods().switch_cboxIframe_frameId(driver);

			fc.utobj().sendKeys(driver, pobj.fieldDisplayName, "helo");
			fc.utobj().sendKeys(driver, pobj.dbColumnLength, "256");
			fc.utobj().clickElement(driver, pobj.submitSectionCboxBtn);
			String msg = fc.utobj().acceptAlertBox(driver);
			if (!msg.equals("Please enter the length between 1 and 255.")) {
				fc.utobj().throwsException("Validation msg is different");
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "salesFormGenerate" })
	@TestCase(createdOn = "2017-07-26", updatedOn = "2017-07-26", testCaseId = "TC_Sales_ValidateNewField_25", testCaseDescription = "Verify Tab in DropDown")
	private void ValidateTabDropDown_25() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("sales", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			AdminFranchiseSalesManageFormGeneratorPage pobj = new AdminFranchiseSalesManageFormGeneratorPage(driver);
			fc.utobj().printTestStep("Hitting Build Url and Then Login");
			driver = fc.loginpage().login(driver);
			String tabName = dataSet.get("tabDisplayName");
			String sectionName = dataSet.get("sectionName ");
			fc.utobj().printTestStep("Go to Admin > Sales > Manage Form Generator");

			AdminSales adsales = new AdminSales();
			adsales.manageFormGenerator(driver);
			AdminFranchiseSalesManageFormGeneratorPageTest p1 = new AdminFranchiseSalesManageFormGeneratorPageTest();
			fc.utobj().printTestStep("Manage Form Generator > Adding New Tab");
			tabName = p1.addTabName(driver, tabName);
			fc.utobj().clickPartialLinkText(driver, tabName);
			boolean found = p1.CheckInDropDown(driver, pobj.dropdown, tabName);
			if (!found) {
				fc.utobj().throwsException("Value not found in Drop down ");
			}
			String defaulttag = dataSet.get("DefaultTags");
			String[] options = defaulttag.split(",");
			for (String optins : options) {
				found = p1.CheckInDropDown(driver, pobj.dropdown, optins);
				if (!found) {
					fc.utobj().throwsException("Value not found in Drop down ");
				}
			}
			p1.CheckInDropDown(driver, pobj.dropdown, tabName);
			// hello
			fc.utobj().printTestStep("Manage Form Generator > In New Tab > Add Section ");
			sectionName = p1.addSectionName(driver, sectionName);
			p1.addFieldName(driver, sectionName, "Field", dataSet, testCaseId);
			String fieldName = "Field";

			fieldName = fc.utobj().generateTestData(fieldName);

			fc.utobj().clickElement(driver, ClickonSectionPageButton(driver, sectionName, "Add New Field"));
			fc.commonMethods().switch_cboxIframe_frameId(driver);

			fc.utobj().sendKeys(driver, pobj.fieldDisplayName, "helo");
			fc.utobj().sendKeys(driver, pobj.dbColumnLength, "0");
			fc.utobj().clickElement(driver, pobj.submitSectionCboxBtn);
			String msg = fc.utobj().acceptAlertBox(driver);
			if (!msg.equals("Please enter the length between 1 and 255.")) {
				fc.utobj().throwsException("Validation msg is different");
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "salesFormGenerate" })
	@TestCase(createdOn = "2017-07-26", updatedOn = "2017-07-26", testCaseId = "TC_Sales_ValidateNewField_26", testCaseDescription = "Verify Tab in DropDown")
	private void ValidateTabDropDown_26() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("sales", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			AdminFranchiseSalesManageFormGeneratorPage pobj = new AdminFranchiseSalesManageFormGeneratorPage(driver);
			fc.utobj().printTestStep("Hitting Build Url and Then Login");
			driver = fc.loginpage().login(driver);
			String tabName = dataSet.get("tabDisplayName");
			String sectionName = dataSet.get("sectionName ");
			fc.utobj().printTestStep("Go to Admin > Sales > Manage Form Generator");

			AdminSales adsales = new AdminSales();
			adsales.manageFormGenerator(driver);
			AdminFranchiseSalesManageFormGeneratorPageTest p1 = new AdminFranchiseSalesManageFormGeneratorPageTest();
			fc.utobj().printTestStep("Manage Form Generator > Adding New Tab");
			tabName = p1.addTabName(driver, tabName);
			fc.utobj().clickPartialLinkText(driver, tabName);
			boolean found = p1.CheckInDropDown(driver, pobj.dropdown, tabName);
			if (!found) {
				fc.utobj().throwsException("Value not found in Drop down ");
			}
			String defaulttag = dataSet.get("DefaultTags");
			String[] options = defaulttag.split(",");
			for (String optins : options) {
				found = p1.CheckInDropDown(driver, pobj.dropdown, optins);
				if (!found) {
					fc.utobj().throwsException("Value not found in Drop down ");
				}
			}
			p1.CheckInDropDown(driver, pobj.dropdown, tabName);
			// hello
			fc.utobj().printTestStep("Manage Form Generator > In New Tab > Add Section ");
			sectionName = p1.addSectionName(driver, sectionName);
			p1.addFieldName(driver, sectionName, "Field", dataSet, testCaseId);
			String fieldName = "Field";

			fieldName = fc.utobj().generateTestData(fieldName);

			fc.utobj().clickElement(driver, ClickonSectionPageButton(driver, sectionName, "Add New Field"));
			fc.commonMethods().switch_cboxIframe_frameId(driver);

			fc.utobj().sendKeys(driver, pobj.fieldDisplayName, "helo" + fc.utobj().getRandomChar());
			fc.utobj().sendKeys(driver, pobj.dbColumnLength, dataSet.get("dbColumnLength"));
			fc.utobj().clickElement(driver, pobj.submitSectionCboxBtn);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "salesFormGenerate" })
	@TestCase(createdOn = "2017-07-26", updatedOn = "2017-07-26", testCaseId = "TC_Sales_ValidateNewField_27", testCaseDescription = "Verify Tab in DropDown")
	private void ValidateTabDropDown_27() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("sales", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			AdminFranchiseSalesManageFormGeneratorPage pobj = new AdminFranchiseSalesManageFormGeneratorPage(driver);
			fc.utobj().printTestStep("Hitting Build Url and Then Login");
			driver = fc.loginpage().login(driver);
			String tabName = dataSet.get("tabDisplayName");
			String sectionName = dataSet.get("sectionName ");
			fc.utobj().printTestStep("Go to Admin > Sales > Manage Form Generator");

			AdminSales adsales = new AdminSales();
			adsales.manageFormGenerator(driver);
			AdminFranchiseSalesManageFormGeneratorPageTest p1 = new AdminFranchiseSalesManageFormGeneratorPageTest();
			fc.utobj().printTestStep("Manage Form Generator > Adding New Tab");
			tabName = p1.addTabName(driver, tabName);
			fc.utobj().clickPartialLinkText(driver, tabName);
			boolean found = p1.CheckInDropDown(driver, pobj.dropdown, tabName);
			if (!found) {
				fc.utobj().throwsException("Value not found in Drop down ");
			}
			String defaulttag = dataSet.get("DefaultTags");
			String[] options = defaulttag.split(",");
			for (String optins : options) {
				found = p1.CheckInDropDown(driver, pobj.dropdown, optins);
				if (!found) {
					fc.utobj().throwsException("Value not found in Drop down ");
				}
			}
			p1.CheckInDropDown(driver, pobj.dropdown, tabName);
			// hello
			fc.utobj().printTestStep("Manage Form Generator > In New Tab > Add Section ");
			sectionName = p1.addSectionName(driver, sectionName);
			p1.addFieldName(driver, sectionName, "Field", dataSet, testCaseId);
			String fieldName = "Field";

			fieldName = fc.utobj().generateTestData(fieldName);

			fc.utobj().clickElement(driver, ClickonSectionPageButton(driver, sectionName, "Add New Field"));
			fc.commonMethods().switch_cboxIframe_frameId(driver);

			fc.utobj().sendKeys(driver, pobj.fieldDisplayName, "helo" + fc.utobj().getRandomChar());
			fc.utobj().sendKeys(driver, pobj.dbColumnLength, dataSet.get("dbColumnLength"));
			fc.utobj().clickElement(driver, pobj.submitSectionCboxBtn);
			if (!fc.utobj().getElement(driver, pobj.addNewFieldBtn).isDisplayed()
					|| !fc.utobj().getElement(driver, pobj.modifySectionBtn).isDisplayed()
					|| fc.utobj().getElement(driver, pobj.addDocumentBtn).isDisplayed()
					|| !fc.utobj().getElement(driver, pobj.previewformbtn).isDisplayed()
					|| !fc.utobj().getElement(driver, pobj.configuretabularview).isDisplayed()
					|| !driver
							.findElement(By.xpath(".//*[contains(text(),'" + tabName
									+ "')]/following-sibling::*/a[contains(text(),'Modify Fields Position')]"))
							.isDisplayed()) {
				fc.utobj().throwsException("Some button is not displayed kindly check");
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}

	}

	@Test(groups = { "salesFormGenerate201450" })
	@TestCase(createdOn = "2017-07-26", updatedOn = "2017-07-26", testCaseId = "TC_Sales_ValidateNewField_28", testCaseDescription = "Verify Tab in DropDown")
	private void ValidateTabDropDown_28() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("sales", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			AdminFranchiseSalesManageFormGeneratorPage pobj = new AdminFranchiseSalesManageFormGeneratorPage(driver);
			fc.utobj().printTestStep("Hitting Build Url and Then Login");
			driver = fc.loginpage().login(driver);
			String tabName = dataSet.get("tabDisplayName");
			String fieldName = dataSet.get("Display Name");
			String sectionName = dataSet.get("sectionName ");
			fc.utobj().printTestStep("Go to Admin > Sales > Manage Form Generator");

			AdminSales adsales = new AdminSales();
			adsales.manageFormGenerator(driver);
			AdminFranchiseSalesManageFormGeneratorPageTest p1 = new AdminFranchiseSalesManageFormGeneratorPageTest();
			fc.utobj().printTestStep("Manage Form Generator > Adding New Tab");
			tabName = p1.addTabName(driver, tabName);
			fc.utobj().clickPartialLinkText(driver, tabName);
			fc.utobj().printTestStep("Manage Form Generator > Adding Section Name");
			sectionName = p1.addSectionName(driver, sectionName);
			fc.utobj().printTestStep("Adding Field Name");
			fieldName = p1.addFieldName(driver, sectionName, fieldName);
			fc.utobj().printTestStep("Modifying Field Name");
			fc.utobj().actionImgOption(driver, fieldName, "Modify");
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().clickElement(driver, pobj.closeSectionCboxBtn);
			driver.switchTo().defaultContent();
			fc.utobj().printTestStep("Deleting Field Name");
			fc.utobj().actionImgOption(driver, fieldName, "Delete");
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().clickElement(driver, pobj.closeSectionCboxBtn);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@DataProvider(name = "count")
	public Object[][] getData() {
		int count = 1;
		Object[][] data = new Object[count][2];
		for (int i = 1; i <= count; i++) {
			int j = i - 1;
			data[j][0] = String.valueOf(i);
			data[j][1] = FranconnectUtil.config.get("curTimeFolder") + "//configuration.xls";
		}

		return data;
	}

	@Test(groups = { "salesFormGenerate", "sales600" }, dataProvider = "count")
	@TestCase(createdOn = "2017-07-26", updatedOn = "2017-08-17", testCaseId = "TC_Sales_ValidateFieldAllValue", testCaseDescription = "Verify Tab in DropDown")
	private void ValidateFieldInTab(String count, String path) throws Exception {
		System.out.println(path + " " + count);
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData_FormGenerator_count(testCaseId, count);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			testCaseId = testCaseId + "_data" + count;
			AdminFranchiseSalesManageFormGeneratorPage pobj = new AdminFranchiseSalesManageFormGeneratorPage(driver);
			fc.utobj().printTestStep("Hitting Build Url and Then Login");
			driver = fc.loginpage().login(driver);
			String tabName = dataSet.get("tabDisplayName");
			String fieldName = dataSet.get("Display Name");
			String sectionName = dataSet.get("sectionName ");
			fc.utobj().printTestStep("Go to Admin > Sales > Manage Form Generator");

			AdminSales adsales = new AdminSales();
			adsales.manageFormGenerator(driver);
			AdminFranchiseSalesManageFormGeneratorPageTest p1 = new AdminFranchiseSalesManageFormGeneratorPageTest();
			fc.utobj().printTestStep("Manage Form Generator > Adding New Tab");
			tabName = p1.addTabName(driver, tabName);
			fc.utobj().clickPartialLinkText(driver, tabName);
			fc.utobj().printTestStep("Adding Section Name");
			sectionName = p1.addSectionName(driver, sectionName);
			fc.utobj().printTestStep("Adding Date Field Name " + fieldName);
			String created = p1.addFieldName(driver, sectionName, fieldName, dataSet, testCaseId);
			if (created.equals("created")) {
				fc.utobj().actionImgOption(driver, fieldName, "Modify");
				fc.commonMethods().switch_cboxIframe_frameId(driver);
				fc.utobj().clickElement(driver, pobj.closeSectionCboxBtn);
				driver.switchTo().defaultContent();
				fc.utobj().actionImgOption(driver, fieldName, "Delete");
				fc.utobj().dismissAlertBox(driver);
				if (!ClickonSectionPageButton(driver, sectionName, "Add New Field").isDisplayed()
						|| !ClickonSectionPageButton(driver, sectionName, "Add Document").isDisplayed()
						|| !ClickonSectionPageButton(driver, sectionName, "Modify Section").isDisplayed()
						|| !fc.utobj().getElement(driver, pobj.previewformbtn).isDisplayed()
						|| !fc.utobj().getElement(driver, pobj.configuretabularview).isDisplayed()) {
					fc.utobj().throwsException("Some button is not displayed kindly check");
				}
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

}
