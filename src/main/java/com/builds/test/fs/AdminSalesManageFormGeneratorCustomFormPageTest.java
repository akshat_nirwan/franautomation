package com.builds.test.fs;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Test;

import com.builds.uimaps.fs.AdminFranchiseSalesManageFormGeneratorPage;
import com.builds.utilities.FranconnectUtil;
import com.builds.utilities.TestCase;

public class AdminSalesManageFormGeneratorCustomFormPageTest {

	FranconnectUtil fc = new FranconnectUtil();

	@Test(groups = { "salesFormGenerate", "salesFormGenerateDummay" })
	@TestCase(createdOn = "2017-07-20", updatedOn = "2017-07-20", testCaseId = "TC_Sales_Validate_Del_From_InactiveActBtn", testCaseDescription = "Verify validation message is coming when user is trying to delete a deactivated custom tab from 'Action' Button \n Verify custom tab does not get deleted from 'Action' Button \n Verify custom tab gets deleted from 'Action' Button")
	private void ValidateDeactivate_From_ActBtn01() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("sales", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			fc.utobj().printTestStep("Hitting Build Url and Then Login");
			driver = fc.loginpage().login(driver);
			String tabName = dataSet.get("tabDisplayName");
			fc.utobj().printTestStep("Go to Admin > Sales > Manage Form Generator");
			AdminSales adsales = new AdminSales();
			adsales.manageFormGenerator(driver);
			AdminFranchiseSalesManageFormGeneratorPageTest p1 = new AdminFranchiseSalesManageFormGeneratorPageTest();
			fc.utobj().printTestStep("Manage Form Generator > Adding New Tab");
			tabName = p1.addTabName(driver, tabName);
			fc.utobj().printTestStep("Tab > Deactivate > Ok");
			fc.utobj().actionImgOption(driver, tabName, "Deactivate");
			fc.utobj().acceptAlertBox(driver);
			fc.utobj().printTestStep("Tab > Delete > Cancel");
			fc.utobj().actionImgOption(driver, tabName, "Delete");
			String msg = fc.utobj().dismissAlertBox(driver);
			if (!msg.equals("Are you sure you want to delete this tab?")) {
				fc.utobj().throwsException("Incorrect validation msg");
			}
			fc.utobj().assertPageSource(driver, tabName);
			fc.utobj().printTestStep("Tab > Delete > Ok");
			fc.utobj().actionImgOption(driver, tabName, "Delete");
			fc.utobj().acceptAlertBox(driver);
			boolean valid = fc.utobj().assertPageSource(driver, tabName);
			if (valid) {
				fc.utobj().throwsException("Tab not Deleted kindly check");
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "salesFormGenerate" })
	@TestCase(createdOn = "2017-07-20", updatedOn = "2017-07-20", testCaseId = "TC_Sales_Validate_Del_From_ActBtn", testCaseDescription = "Verify validation message is coming when user is trying to delete an active custom tab from 'Action' Button")
	private void ValidateDeletionofTab() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("sales", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			fc.utobj().printTestStep("Hitting Build Url and Then Login");
			driver = fc.loginpage().login(driver);
			String tabName = dataSet.get("tabDisplayName");
			fc.utobj().printTestStep("Go to Admin > Sales > Manage Form Generator");
			AdminSales adsales = new AdminSales();
			adsales.manageFormGenerator(driver);
			AdminFranchiseSalesManageFormGeneratorPageTest p1 = new AdminFranchiseSalesManageFormGeneratorPageTest();
			fc.utobj().printTestStep("Manage Form Generator > Adding New Tab");
			tabName = p1.addTabName(driver, tabName);
			fc.utobj().printTestStep("Tab > Delete > Cancel");
			fc.utobj().actionImgOption(driver, tabName, "Delete");
			String msg = fc.utobj().dismissAlertBox(driver);
			if (!msg.equals("Are you sure you want to delete this tab?")) {
				fc.utobj().throwsException("Incorrect validation msg");
			}
			fc.utobj().assertPageSource(driver, tabName);
			fc.utobj().printTestStep("Tab > Delete > Ok");
			fc.utobj().actionImgOption(driver, tabName, "Delete");
			fc.utobj().acceptAlertBox(driver);
			boolean valid = fc.utobj().assertPageSource(driver, tabName);
			if (valid) {
				fc.utobj().throwsException("Tab not Deleted kindly check");
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "salesFormGenerate" })
	@TestCase(createdOn = "2017-07-20", updatedOn = "2017-07-20", testCaseId = "TC_Sales_Validate_Del_From_ActBtnAfterAddSection", testCaseDescription = "Verify Deletion not perform while add section in Tab")
	private void ValidateDeletionofTab_01() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("sales", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			fc.utobj().printTestStep("Hitting Build Url and Then Login");
			driver = fc.loginpage().login(driver);
			String tabName = dataSet.get("tabDisplayName");
			String sectionName = dataSet.get("sectionName ");
			fc.utobj().printTestStep("Go to Admin > Sales > Manage Form Generator");
			AdminSales adsales = new AdminSales();
			adsales.manageFormGenerator(driver);
			AdminFranchiseSalesManageFormGeneratorPageTest p1 = new AdminFranchiseSalesManageFormGeneratorPageTest();
			fc.utobj().printTestStep("Manage Form Generator > Adding New Tab");
			tabName = p1.addTabName(driver, tabName);
			fc.utobj().printTestStep("Manage Form Generator > In New Tab > Add Section ");
			sectionName = p1.addSection(tabName);
			fc.utobj().printTestStep("Tab > Delete > Cancel");
			fc.utobj().actionImgOption(driver, tabName, "Delete");
			String msg = fc.utobj().dismissAlertBox(driver);
			if (!msg.equals("Are you sure you want to delete this tab?")) {
				fc.utobj().throwsException("Incorrect validation msg");
			}
			fc.utobj().assertPageSource(driver, tabName);
			fc.utobj().printTestStep("Tab > Delete > Ok");
			fc.utobj().actionImgOption(driver, tabName, "Delete");
			fc.utobj().acceptAlertBox(driver);
			boolean valid = fc.utobj().assertPageSource(driver, tabName);
			if (valid) {
				fc.utobj().throwsException("Tab not Deleted kindly check");
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "salesFormGenerate" })
	@TestCase(createdOn = "2017-07-20", updatedOn = "2017-07-20", testCaseId = "TC_Sales_ValidateTab_InDropDown", testCaseDescription = "Verify Tab in DropDown")
	private void ValidateTabDropDown() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("sales", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			AdminFranchiseSalesManageFormGeneratorPage pobj = new AdminFranchiseSalesManageFormGeneratorPage(driver);
			fc.utobj().printTestStep("Hitting Build Url and Then Login");
			driver = fc.loginpage().login(driver);
			String tabName = dataSet.get("tabDisplayName");
			String sectionName = dataSet.get("sectionName ");
			fc.utobj().printTestStep("Go to Admin > Sales > Manage Form Generator");
			AdminSales adsales = new AdminSales();
			adsales.manageFormGenerator(driver);
			AdminFranchiseSalesManageFormGeneratorPageTest p1 = new AdminFranchiseSalesManageFormGeneratorPageTest();
			fc.utobj().printTestStep("Manage Form Generator > Adding New Tab");
			tabName = p1.addTabName(driver, tabName);
			fc.utobj().clickPartialLinkText(driver, tabName);
			boolean found = p1.CheckInDropDown(driver, pobj.dropdown, tabName);
			if (!found) {
				fc.utobj().throwsException("Value not found in Drop down ");
			}
			String defaulttag = dataSet.get("DefaultTags");
			String[] options = defaulttag.split(",");
			for (String optins : options) {
				found = p1.CheckInDropDown(driver, pobj.dropdown, optins);
				if (!found) {
					fc.utobj().throwsException("Value not found in Drop down ");
				}

			}
			p1.CheckInDropDown(driver, pobj.dropdown, tabName);
			// hello
			fc.utobj().printTestStep("Manage Form Generator > In New Tab > Add Section ");
			sectionName = p1.addSectionName(driver, sectionName);
			p1.addFieldName(driver, sectionName, "Field", dataSet, testCaseId);
			String list = dataSet.get("FieldOptions");
			String[] fops = list.split(",");
			List<String> listItems = new LinkedList<String>();
			Collections.addAll(listItems, fops);
			fc.utobj().assertPageSourceWithMultipleRecords(driver, listItems);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "salesFormGenerate" })
	@TestCase(createdOn = "2017-07-24", updatedOn = "2017-07-24", testCaseId = "TC_Sales_MFG_addSecionValidationonBlank", testCaseDescription = "Verify Tab in DropDown")
	private void ValidationaddSecionalert() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("sales", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			fc.utobj().printTestStep("Hitting Build Url and Then Login");
			driver = fc.loginpage().login(driver);
			String tabName = dataSet.get("tabDisplayName");
			fc.utobj().printTestStep("Go to Admin > Sales > Manage Form Generator");
			AdminSales adsales = new AdminSales();
			adsales.manageFormGenerator(driver);
			AdminFranchiseSalesManageFormGeneratorPageTest p1 = new AdminFranchiseSalesManageFormGeneratorPageTest();
			fc.utobj().printTestStep("Manage Form Generator > Adding New Tab");
			tabName = p1.addTabName(driver, tabName);
			fc.utobj().clickPartialLinkText(driver, tabName);
			fc.utobj().printTestStep("Manage Form Generator > In New Tab > Add Section ");
			AdminFranchiseSalesManageFormGeneratorPage pobj = new AdminFranchiseSalesManageFormGeneratorPage(driver);
			String windowHandle = driver.getWindowHandle();
			fc.commonMethods().switch_cboxIframe_frameId(driver);

			fc.utobj().sendKeys(driver, pobj.sectionName, "");
			fc.utobj().clickElement(driver, pobj.submitSectionCboxBtn);
			String msg = fc.utobj().acceptAlertBox(driver);
			if (!msg.equals("Please enter Section Name..")) {
				fc.utobj().throwsException("Validation alert msg is not matched as per expected");
			}
			fc.utobj().clickElement(driver, pobj.closeSectionCboxBtn);

			driver.switchTo().window(windowHandle);

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "salesFormGenerate" })
	@TestCase(createdOn = "2017-07-24", updatedOn = "2017-07-24", testCaseId = "TC_Sales_MFG_addSecionValidationonSpclChar", testCaseDescription = "Verify Tab in DropDown")
	private void ValidationaddSectionOnSplChar() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("sales", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			fc.utobj().printTestStep("Hitting Build Url and Then Login");
			driver = fc.loginpage().login(driver);
			String tabName = dataSet.get("tabDisplayName");
			fc.utobj().printTestStep("Go to Admin > Sales > Manage Form Generator");
			AdminSales adsales = new AdminSales();
			adsales.manageFormGenerator(driver);
			AdminFranchiseSalesManageFormGeneratorPageTest p1 = new AdminFranchiseSalesManageFormGeneratorPageTest();
			fc.utobj().printTestStep("Manage Form Generator > Adding New Tab");
			tabName = p1.addTabName(driver, tabName);
			fc.utobj().clickPartialLinkText(driver, tabName);
			fc.utobj().printTestStep("Manage Form Generator > In New Tab > Add Section ");
			AdminFranchiseSalesManageFormGeneratorPage pobj = new AdminFranchiseSalesManageFormGeneratorPage(driver);
			String windowHandle = driver.getWindowHandle();
			fc.utobj().clickElement(driver, pobj.addSectionBtn);
			fc.commonMethods().switch_cboxIframe_frameId(driver);

			fc.utobj().sendKeys(driver, pobj.sectionName, "!@#$");
			fc.utobj().clickElement(driver, pobj.submitSectionCboxBtn);
			String msg = fc.utobj().acceptAlertBox(driver);
			if (!msg.equals("Display Name must begin with a letter.")) {
				fc.utobj().throwsException("Validation alert msg is not matched as per expected");
			}
			fc.utobj().clickElement(driver, pobj.closeSectionCboxBtn);

			driver.switchTo().window(windowHandle);

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "salesFormGenerate" })
	@TestCase(createdOn = "2017-07-24", updatedOn = "2017-07-24", testCaseId = "TC_Sales_MFG_addSecionValidationonSpclCharWithAlphabet", testCaseDescription = "Verify Tab in DropDown")
	private void ValidationaddSectionOnSplCharWithAlphabet() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("sales", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			fc.utobj().printTestStep("Hitting Build Url and Then Login");
			driver = fc.loginpage().login(driver);
			String tabName = dataSet.get("tabDisplayName");
			fc.utobj().printTestStep("Go to Admin > Sales > Manage Form Generator");
			AdminSales adsales = new AdminSales();
			adsales.manageFormGenerator(driver);
			AdminFranchiseSalesManageFormGeneratorPageTest p1 = new AdminFranchiseSalesManageFormGeneratorPageTest();
			fc.utobj().printTestStep("Manage Form Generator > Adding New Tab");
			tabName = p1.addTabName(driver, tabName);
			fc.utobj().clickPartialLinkText(driver, tabName);
			fc.utobj().printTestStep("Manage Form Generator > In New Tab > Add Section ");
			AdminFranchiseSalesManageFormGeneratorPage pobj = new AdminFranchiseSalesManageFormGeneratorPage(driver);
			String windowHandle = driver.getWindowHandle();
			fc.utobj().clickElement(driver, pobj.addSectionBtn);
			fc.commonMethods().switch_cboxIframe_frameId(driver);

			fc.utobj().sendKeys(driver, pobj.sectionName, "!@#$ABC");
			fc.utobj().clickElement(driver, pobj.submitSectionCboxBtn);
			String msg = fc.utobj().acceptAlertBox(driver);
			if (!msg.equals("Display Name must begin with a letter.")) {
				fc.utobj().throwsException("Validation alert msg is not matched as per expected");
			}
			fc.utobj().clickElement(driver, pobj.closeSectionCboxBtn);

			driver.switchTo().window(windowHandle);

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "salesFormGenerate" })
	@TestCase(createdOn = "2017-07-24", updatedOn = "2017-07-24", testCaseId = "TC_Sales_MFG_addSecionValidationonNumber", testCaseDescription = "Verify Tab in DropDown")
	private void ValidationaddSectionOnNumber() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("sales", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			fc.utobj().printTestStep("Hitting Build Url and Then Login");
			driver = fc.loginpage().login(driver);
			String tabName = dataSet.get("tabDisplayName");
			fc.utobj().printTestStep("Go to Admin > Sales > Manage Form Generator");
			AdminSales adsales = new AdminSales();
			adsales.manageFormGenerator(driver);
			AdminFranchiseSalesManageFormGeneratorPageTest p1 = new AdminFranchiseSalesManageFormGeneratorPageTest();
			fc.utobj().printTestStep("Manage Form Generator > Adding New Tab");
			tabName = p1.addTabName(driver, tabName);
			fc.utobj().clickPartialLinkText(driver, tabName);
			fc.utobj().printTestStep("Manage Form Generator > In New Tab > Add Section ");
			AdminFranchiseSalesManageFormGeneratorPage pobj = new AdminFranchiseSalesManageFormGeneratorPage(driver);
			String windowHandle = driver.getWindowHandle();
			fc.utobj().clickElement(driver, pobj.addSectionBtn);
			fc.commonMethods().switch_cboxIframe_frameId(driver);

			fc.utobj().sendKeys(driver, pobj.sectionName, "123456");
			fc.utobj().clickElement(driver, pobj.submitSectionCboxBtn);
			String msg = fc.utobj().acceptAlertBox(driver);
			if (!msg.equals("Display Name must begin with a letter.")) {
				fc.utobj().throwsException("Validation alert msg is not matched as per expected");
			}
			fc.utobj().clickElement(driver, pobj.closeSectionCboxBtn);

			driver.switchTo().window(windowHandle);

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "salesFormGenerate" })
	@TestCase(createdOn = "2017-07-24", updatedOn = "2017-07-24", testCaseId = "TC_Sales_MFG_addSecionValidationonSpclCharWithAlphabet_01", testCaseDescription = "Verify Tab in DropDown")
	private void ValidationaddSectionOnSplCharWithAlphabet_01() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("sales", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			fc.utobj().printTestStep("Hitting Build Url and Then Login");
			driver = fc.loginpage().login(driver);
			String tabName = dataSet.get("tabDisplayName");
			fc.utobj().printTestStep("Go to Admin > Sales > Manage Form Generator");
			AdminSales adsales = new AdminSales();
			adsales.manageFormGenerator(driver);
			AdminFranchiseSalesManageFormGeneratorPageTest p1 = new AdminFranchiseSalesManageFormGeneratorPageTest();
			fc.utobj().printTestStep("Manage Form Generator > Adding New Tab");
			tabName = p1.addTabName(driver, tabName);
			fc.utobj().clickPartialLinkText(driver, tabName);
			fc.utobj().printTestStep("Manage Form Generator > In New Tab > Add Section ");
			AdminFranchiseSalesManageFormGeneratorPage pobj = new AdminFranchiseSalesManageFormGeneratorPage(driver);
			String windowHandle = driver.getWindowHandle();
			fc.utobj().clickElement(driver, pobj.addSectionBtn);
			fc.commonMethods().switch_cboxIframe_frameId(driver);

			fc.utobj().sendKeys(driver, pobj.sectionName, "A-B-C");
			fc.utobj().clickElement(driver, pobj.submitSectionCboxBtn);
			driver.switchTo().window(windowHandle);

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "salesFormGenerate" })
	@TestCase(createdOn = "2017-07-24", updatedOn = "2017-07-24", testCaseId = "TC_Sales_MFG_addSecionValidationonNumberandName", testCaseDescription = "Verify Tab in DropDown")
	private void ValidationaddSectionOnNumberandName() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("sales", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			fc.utobj().printTestStep("Hitting Build Url and Then Login");
			driver = fc.loginpage().login(driver);
			String tabName = dataSet.get("tabDisplayName");
			fc.utobj().printTestStep("Go to Admin > Sales > Manage Form Generator");
			AdminSales adsales = new AdminSales();
			adsales.manageFormGenerator(driver);
			AdminFranchiseSalesManageFormGeneratorPageTest p1 = new AdminFranchiseSalesManageFormGeneratorPageTest();
			fc.utobj().printTestStep("Manage Form Generator > Adding New Tab");
			tabName = p1.addTabName(driver, tabName);
			fc.utobj().clickPartialLinkText(driver, tabName);
			fc.utobj().printTestStep("Manage Form Generator > In New Tab > Add Section ");
			AdminFranchiseSalesManageFormGeneratorPage pobj = new AdminFranchiseSalesManageFormGeneratorPage(driver);
			String windowHandle = driver.getWindowHandle();
			fc.utobj().clickElement(driver, pobj.addSectionBtn);
			fc.commonMethods().switch_cboxIframe_frameId(driver);

			fc.utobj().sendKeys(driver, pobj.sectionName, "75AB");
			fc.utobj().clickElement(driver, pobj.submitSectionCboxBtn);
			String msg = fc.utobj().acceptAlertBox(driver);
			if (!msg.equals("Display Name must begin with a letter.")) {
				fc.utobj().throwsException("Validation alert msg is not matched as per expected");
			}
			fc.utobj().clickElement(driver, pobj.closeSectionCboxBtn);

			driver.switchTo().window(windowHandle);

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "salesFormGenerate" })
	@TestCase(createdOn = "2017-07-24", updatedOn = "2017-07-24", testCaseId = "TC_Sales_MFG_addSecionValidationonNameandNumber", testCaseDescription = "Verify Tab in DropDown")
	private void ValidationaddSectionOnNameandNumber() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("sales", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			fc.utobj().printTestStep("Hitting Build Url and Then Login");
			driver = fc.loginpage().login(driver);
			String tabName = dataSet.get("tabDisplayName");
			fc.utobj().printTestStep("Go to Admin > Sales > Manage Form Generator");
			AdminSales adsales = new AdminSales();
			adsales.manageFormGenerator(driver);
			AdminFranchiseSalesManageFormGeneratorPageTest p1 = new AdminFranchiseSalesManageFormGeneratorPageTest();
			fc.utobj().printTestStep("Manage Form Generator > Adding New Tab");
			tabName = p1.addTabName(driver, tabName);
			fc.utobj().clickPartialLinkText(driver, tabName);
			fc.utobj().printTestStep("Manage Form Generator > In New Tab > Add Section ");
			AdminFranchiseSalesManageFormGeneratorPage pobj = new AdminFranchiseSalesManageFormGeneratorPage(driver);
			String windowHandle = driver.getWindowHandle();
			fc.utobj().clickElement(driver, pobj.addSectionBtn);
			fc.commonMethods().switch_cboxIframe_frameId(driver);

			fc.utobj().sendKeys(driver, pobj.sectionName, "A75");
			fc.utobj().clickElement(driver, pobj.submitSectionCboxBtn);
			driver.switchTo().window(windowHandle);

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "salesFormGenerate" })
	@TestCase(createdOn = "2017-07-24", updatedOn = "2017-07-24", testCaseId = "TC_Sales_MFG_addSecionValidationonClosebtn", testCaseDescription = "Verify Tab in DropDown")
	private void ValidationaddSectionOnClosebtn() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("sales", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			fc.utobj().printTestStep("Hitting Build Url and Then Login");
			driver = fc.loginpage().login(driver);
			String tabName = dataSet.get("tabDisplayName");
			String sectionName = dataSet.get("sectionName ");
			fc.utobj().printTestStep("Go to Admin > Sales > Manage Form Generator");
			AdminSales adsales = new AdminSales();
			adsales.manageFormGenerator(driver);
			AdminFranchiseSalesManageFormGeneratorPageTest p1 = new AdminFranchiseSalesManageFormGeneratorPageTest();
			fc.utobj().printTestStep("Manage Form Generator > Adding New Tab");
			tabName = p1.addTabName(driver, tabName);
			fc.utobj().clickPartialLinkText(driver, tabName);
			fc.utobj().printTestStep("Manage Form Generator > In New Tab > Add Section ");
			AdminFranchiseSalesManageFormGeneratorPage pobj = new AdminFranchiseSalesManageFormGeneratorPage(driver);
			String windowHandle = driver.getWindowHandle();
			sectionName = fc.utobj().generateTestData(sectionName);
			fc.utobj().clickElement(driver, pobj.addSectionBtn);
			fc.commonMethods().switch_cboxIframe_frameId(driver);

			fc.utobj().sendKeys(driver, pobj.sectionName, "A75");
			fc.utobj().clickElement(driver, pobj.closeSectionCboxBtn);
			driver.switchTo().window(windowHandle);
			boolean found = fc.utobj().assertPageSource(driver, sectionName);
			if (found) {
				fc.utobj().throwsException("Even on close button section got added");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "salesFormGenerate" })
	@TestCase(createdOn = "2017-07-24", updatedOn = "2017-07-24", testCaseId = "TC_Sales_MFG_validateSectionPage", testCaseDescription = "Verify Tab in DropDown")
	private void ValidationaddSectionPage() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("sales", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			fc.utobj().printTestStep("Hitting Build Url and Then Login");
			driver = fc.loginpage().login(driver);
			String tabName = dataSet.get("tabDisplayName");
			String sectionName = dataSet.get("sectionName ");
			fc.utobj().printTestStep("Go to Admin > Sales > Manage Form Generator");
			AdminSales adsales = new AdminSales();
			adsales.manageFormGenerator(driver);
			AdminFranchiseSalesManageFormGeneratorPageTest p1 = new AdminFranchiseSalesManageFormGeneratorPageTest();
			fc.utobj().printTestStep("Manage Form Generator > Adding New Tab");
			tabName = p1.addTabName(driver, tabName);
			fc.utobj().clickPartialLinkText(driver, tabName);
			fc.utobj().printTestStep("Manage Form Generator > In New Tab > Add Section ");
			AdminFranchiseSalesManageFormGeneratorPage pobj = new AdminFranchiseSalesManageFormGeneratorPage(driver);
			String windowHandle = driver.getWindowHandle();
			sectionName = fc.utobj().generateTestData(sectionName);
			fc.utobj().clickElement(driver, pobj.addSectionBtn);
			fc.commonMethods().switch_cboxIframe_frameId(driver);

			fc.utobj().sendKeys(driver, pobj.sectionName, sectionName);
			fc.utobj().clickElement(driver, pobj.submitSectionCboxBtn);
			driver.switchTo().window(windowHandle);
			if (fc.utobj().getElement(driver, pobj.addDocumentBtn).isDisplayed()
					&& fc.utobj().getElement(driver, pobj.modifySectionBtn).isDisplayed()
					&& fc.utobj().getElement(driver, pobj.addNewFieldBtn).isDisplayed()) {
			} else {
				fc.utobj().throwsException("some button not found on section");
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "salesFormGenerate" })
	@TestCase(createdOn = "2017-07-25", updatedOn = "2017-07-25", testCaseId = "TC_Sales_MFG_validateSectionDelete", testCaseDescription = "Verify Tab in DropDown")
	private void ValidationDeleteSectionPage() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("sales", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			fc.utobj().printTestStep("Hitting Build Url and Then Login");
			driver = fc.loginpage().login(driver);
			String tabName = dataSet.get("tabDisplayName");
			String sectionName = dataSet.get("sectionName ");

			fc.utobj().printTestStep("Go to Admin > Sales > Manage Form Generator");
			AdminSales adsales = new AdminSales();
			adsales.manageFormGenerator(driver);
			AdminFranchiseSalesManageFormGeneratorPageTest p1 = new AdminFranchiseSalesManageFormGeneratorPageTest();
			fc.utobj().printTestStep("Manage Form Generator > Adding New Tab");
			tabName = p1.addTabName(driver, tabName);
			fc.utobj().clickPartialLinkText(driver, tabName);
			fc.utobj().printTestStep("Manage Form Generator > In New Tab > Add Section ");
			AdminFranchiseSalesManageFormGeneratorPage pobj = new AdminFranchiseSalesManageFormGeneratorPage(driver);
			String windowHandle = driver.getWindowHandle();
			sectionName = fc.utobj().generateTestData(sectionName);
			fc.utobj().clickElement(driver, pobj.addSectionBtn);
			fc.commonMethods().switch_cboxIframe_frameId(driver);

			fc.utobj().sendKeys(driver, pobj.sectionName, sectionName);
			fc.utobj().clickElement(driver, pobj.submitSectionCboxBtn);
			driver.switchTo().window(windowHandle);
			if (fc.utobj().getElement(driver, pobj.addDocumentBtn).isDisplayed()
					&& fc.utobj().getElement(driver, pobj.modifySectionBtn).isDisplayed()
					&& fc.utobj().getElement(driver, pobj.addNewFieldBtn).isDisplayed()) {
			} else {
				fc.utobj().throwsException("some button not found on section");
			}
			fc.utobj().clickElement(driver, pobj.deleteSectionBtn);
			fc.utobj().dismissAlertBox(driver);
			fc.utobj().clickElement(driver, pobj.deleteSectionBtn);
			String msg = fc.utobj().acceptAlertBox(driver);
			if (msg == null) {
				fc.utobj().throwsException("Validation msg is not coming");
			}
			if (fc.utobj().getElement(driver, pobj.deleteSectionBtn).isDisplayed()) {
				fc.utobj().throwsException("Delete button is still showing");
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

}
