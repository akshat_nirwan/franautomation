package com.builds.test.fs;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.testng.Reporter;
import org.testng.annotations.Test;

import com.builds.uimaps.fs.AdminFranchiseSalesManageFormGeneratorPage;
import com.builds.utilities.FranconnectUtil;
import com.builds.utilities.TestCase;

public class AdminSalesManageFormGeneratorModifyActionBtnPageTest
		extends AdminFranchiseSalesManageFormGeneratorPageTest {

	FranconnectUtil fc = new FranconnectUtil();

	public static <T> boolean listEqualsNoOrder(List<T> l1, List<T> l2) {

		return l1.containsAll(l2);
	}

	public boolean verifyElementAbsent(String locator, WebDriver driver) throws Exception {
		try {

			WebElement actionbtn = fc.utobj().getElementByXpath(driver,".//a[text()='" + locator + "']/ancestor::tr[@qat_maintable='withoutheader']/td[4]//layer");
			fc.utobj().clickElement(driver, actionbtn);

			return false;

		} catch (NoSuchElementException e) {
			fc.utobj().throwsException("Element absent");

		}
		return true;
	}

	@Test(groups = { "salesFormGenerate" })
	@TestCase(createdOn = "2017-07-05", updatedOn = "2017-07-17", testCaseId = "TC_Sales_Validate_ActionBtnOptIn_MFG_Link", testCaseDescription = "User should have option to Modify, Deactivate and Delete the new tab added via 'Action' button")
	private void ValidateActionBtnOptInMFG() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("sales", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			fc.utobj().printTestStep("Hitting Build Url and Then Login");
			driver = fc.loginpage().login(driver);
			String tabName = dataSet.get("tabDisplayName");
			fc.utobj().printTestStep("Go to Admin > Sales > Manage Form Generator");
			AdminSales adsales = new AdminSales();
			adsales.manageFormGenerator(driver);
			AdminFranchiseSalesManageFormGeneratorPageTest p1 = new AdminFranchiseSalesManageFormGeneratorPageTest();
			tabName = p1.addTabName(driver, tabName);

			verifyElementAbsent(tabName, driver);
			String text = dataSet.get("Modify");
			fc.utobj().printTestStep("Click on Action > Modify");
			fc.utobj().actionImgOption(driver, tabName, text);
			fc.commonMethods().switch_cboxIframe_frameId(driver);

			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='Button']"));
			fc.utobj().printTestStep("Click on Action > Deactivate");
			fc.utobj().actionImgOption(driver, tabName, dataSet.get("Deactivate"));
			String msg1 = fc.utobj().dismissAlertBox(driver);
			if (!msg1.equals("Are you sure you want to deactivate this tab?")) {
				fc.utobj().throwsException("On Deactivating  wrong validation msg is coming kindly check");
			}
			fc.utobj().printTestStep("Click on Action > Delete");
			fc.utobj().actionImgOption(driver, tabName, dataSet.get("Delete"));
			fc.utobj().acceptAlertBox(driver);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "salesFormGenerate" })
	@TestCase(createdOn = "2017-07-05", updatedOn = "2017-07-17", testCaseId = "TC_Sales_Validate_NewTabAddedMfg_OnViewPage", testCaseDescription = "Verify Tab should be visible at Primary Info page of a lead")
	private void ValidateNewTabAdded() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("sales", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			fc.utobj().printTestStep("Hitting Build Url and Then Login");
			driver = fc.loginpage().login(driver);
			String tabName = dataSet.get("tabDisplayName");
			fc.utobj().printTestStep("Go to Admin > Sales > Manage Form Generator");
			AdminSales adsales = new AdminSales();
			adsales.manageFormGenerator(driver);
			AdminFranchiseSalesManageFormGeneratorPageTest p1 = new AdminFranchiseSalesManageFormGeneratorPageTest();
			fc.utobj().printTestStep(" Click on Add New Tab \n  Fill all mandatory fields and click on Save");
			tabName = p1.addTabName(driver, tabName);

			fc.loginpage().login(driver);

			fc.sales().sales_common().fsModule(driver);
			Sales fs = new Sales();
			fs.leadManagement(driver);
			FSLeadSummaryPageTest fsPage = new FSLeadSummaryPageTest();
			fsPage.clickAddLeadLink(driver);
			fc.utobj().printTestStep(" Go to Sales > Lead Management > Primary Info page of lead");
			FSLeadSummaryPrimaryInfoPageTest pInfoPage = new FSLeadSummaryPrimaryInfoPageTest();
			Map<String, String> leadInfo = pInfoPage.fillLeadBasicInfo(driver);
			Reporter.log("Lead Added with lead name : " + leadInfo.get("firstName") + " " + leadInfo.get("lastName"));
			fc.utobj().printTestStep(" Observe and Verify");
			fc.utobj().clickElement(driver, fc.utobj().getElementByPartialLinkText(driver, tabName));

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "salesFormGenerate" })
	@TestCase(createdOn = "2017-07-14", updatedOn = "2017-07-17", testCaseId = "TC_Sales_Validate_NewTabAddedMfg_OnAdvanceSearch_1", testCaseDescription = "Tab should not be visible at Export when a field does not exist in the Form")
	private void ValidateNewTabAddedInAdvanceSearch_1() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("sales", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			fc.utobj().printTestStep("Hitting Build Url and Then Login");
			driver = fc.loginpage().login(driver);
			String tabName = dataSet.get("tabDisplayName");
			fc.utobj().printTestStep("Go to Admin > Sales > Manage Form Generator");
			AdminSales adsales = new AdminSales();
			adsales.manageFormGenerator(driver);
			AdminFranchiseSalesManageFormGeneratorPageTest p1 = new AdminFranchiseSalesManageFormGeneratorPageTest();
			fc.utobj().printTestStep("Manage Form Generator > Adding New Tab");
			tabName = p1.addTabName(driver, tabName);

			fc.loginpage().login(driver);
			fc.sales().sales_common().fsModule(driver);
			Sales fs = new Sales();
			fs.search(driver);
			FSSearchPageTest fsPage = new FSSearchPageTest();
			fc.utobj().printTestStep("Manage Form Generator > Searching New Tab in Advance Search");
			boolean valid = fsPage.searchTabByAdavaceSearch(driver, tabName);
			if (valid) {
				fc.utobj().throwsException("Added Tag is visible in Advanced Search");
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "salesFormGenerate" })
	@TestCase(createdOn = "2017-07-14", updatedOn = "2017-07-17", testCaseId = "TC_Sales_Validate_NewTabAddedMfg_OnExportSearch_1", testCaseDescription = "Verify Tab should not be visible at Export when a field does not exist in the Form")
	private void ValidateNewTabAddedInExportSearch_1() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("sales", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			fc.utobj().printTestStep("Hitting Build Url and Then Login");
			driver = fc.loginpage().login(driver);
			String tabName = dataSet.get("tabDisplayName");
			fc.utobj().printTestStep("Go to Admin > Sales > Manage Form Generator");
			AdminSales adsales = new AdminSales();
			adsales.manageFormGenerator(driver);
			AdminFranchiseSalesManageFormGeneratorPageTest p1 = new AdminFranchiseSalesManageFormGeneratorPageTest();
			fc.utobj().printTestStep("Manage Form Generator > Adding New Tab");
			tabName = p1.addTabName(driver, tabName);

			fc.loginpage().login(driver);
			fc.sales().sales_common().fsModule(driver);
			Sales fs = new Sales();
			fs.search(driver);
			FSSearchPageTest fsPage = new FSSearchPageTest();
			fc.utobj().printTestStep("Manage Form Generator > Searching New Tab in Advance Search");
			boolean valid = fsPage.searchTabByExport(driver, tabName);
			if (valid) {
				fc.utobj().throwsException("Added Tag is visible in Advanced Search");
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "salesFormGenerate" })
	@TestCase(createdOn = "2017-07-14", updatedOn = "2017-07-14", testCaseId = "TC_Sales_Validate_inactiveTab", testCaseDescription = "Verify in Manage Form Generator > Action button is coming")
	private void ValidateInactiveTab() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("sales", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			fc.utobj().printTestStep("Hitting Build Url and Then Login");
			driver = fc.loginpage().login(driver);
			String tabName = dataSet.get("tabDisplayName");
			fc.utobj().printTestStep("Go to Admin > Sales > Manage Form Generator");

			AdminSales adsales = new AdminSales();
			adsales.manageFormGenerator(driver);
			fc.utobj().printTestStep("Manage Form Generator > Adding New Tab");
			AdminFranchiseSalesManageFormGeneratorPage pobj = new AdminFranchiseSalesManageFormGeneratorPage(driver);
			fc.utobj().clickElement(driver, pobj.continueBtn);
			fc.utobj().clickElement(driver, pobj.addNewTabBtn);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			tabName = fc.utobj().generateTestData(tabName);
			fc.utobj().sendKeys(driver, pobj.displayName, tabName);
			fc.utobj().selectDropDownByPartialText(driver, pobj.submoduleNameDrp, "Lead Management");
			fc.utobj().clickElement(driver, pobj.isactiveNo);
			fc.utobj().clickElement(driver, pobj.submitBtn);
			fc.utobj().assertSingleLinkText(driver, tabName);
			try {
				fc.utobj().actionImgOption(driver, tabName, "Modify");
				fc.utobj().throwsException("Modify option is coming for inactive Tab");
			} catch (Exception e) {
				try {
					fc.utobj().clickElement(driver, fc.utobj().getElementByLinkText(driver, tabName));
					fc.utobj().throwsException("Inactive tab is clickable");
				} catch (Exception ex) {
					fc.utobj().actionImgOption(driver, tabName, "Activate");
					fc.utobj().dismissAlertBox(driver);
					fc.utobj().actionImgOption(driver, tabName, "Activate");
					String msg = fc.utobj().acceptAlertBox(driver);
					if (!msg.equals(dataSet.get("msgOnActivate"))) {
						fc.utobj().throwsException("Validation msg is incorrect on Activating tab");
					}
					fc.utobj().actionImgOption(driver, tabName, "Delete");
					fc.utobj().acceptAlertBox(driver);
				}
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "salesFormGenerate" })
	@TestCase(createdOn = "2017-07-14", updatedOn = "2017-07-17", testCaseId = "TC_Sales_Validate_inactiveTabonActive", testCaseDescription = "Verify in Manage Form Generator > Once tab is made active then tab is clickable")
	private void ValidateInactiveTabonActive() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("sales", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			fc.utobj().printTestStep("Hitting Build Url and Then Login");
			driver = fc.loginpage().login(driver);
			String tabName = dataSet.get("tabDisplayName");
			fc.utobj().printTestStep("Go to Admin > Sales > Manage Form Generator");
			AdminSales adsales = new AdminSales();
			adsales.manageFormGenerator(driver);
			fc.utobj().printTestStep("Manage Form Generator > Adding New Inactive Tab");
			AdminFranchiseSalesManageFormGeneratorPage pobj = new AdminFranchiseSalesManageFormGeneratorPage(driver);
			fc.utobj().clickElement(driver, pobj.continueBtn);
			fc.utobj().clickElement(driver, pobj.addNewTabBtn);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			tabName = fc.utobj().generateTestData(tabName);
			fc.utobj().sendKeys(driver, pobj.displayName, tabName);
			fc.utobj().selectDropDownByPartialText(driver, pobj.submoduleNameDrp, "Lead Management");
			fc.utobj().clickElement(driver, pobj.isactiveNo);
			fc.utobj().clickElement(driver, pobj.submitBtn);
			fc.utobj().printTestStep("Manage Form Generator > checking Inactive Tab is Clickable or Not");
			boolean clickable = fc.utobj().assertLinkText(driver, tabName);
			if (clickable) {
				fc.utobj().throwsException("Exception as inActive tab is clickable");
			}
			try {
				fc.utobj().actionImgOption(driver, tabName, "Modify");
				fc.utobj().throwsException("Modify option is coming for inactive Tab");
			} catch (Exception e) {
				try {
					fc.utobj().clickElement(driver, fc.utobj().getElementByLinkText(driver, tabName));
					fc.utobj().throwsException("Inactive tab is clickable");
				} catch (Exception ex) {
					fc.utobj().throwsException("Activating Inactive tab with Alert Msg");
					fc.utobj().actionImgOption(driver, tabName, "Activate");
					fc.utobj().dismissAlertBox(driver);
					fc.utobj().actionImgOption(driver, tabName, "Activate");
					String msg = fc.utobj().acceptAlertBox(driver);
					if (!msg.equals(dataSet.get("msgOnActivate"))) {
						fc.utobj().throwsException("Validation msg is incorrect on Activating tab");
					}
					fc.utobj().throwsException("Verifying Weather it is clickable or not");
					fc.utobj().clickElement(driver, fc.utobj().getElementByLinkText(driver, tabName));
				}
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "salesFormGenerate" })
	@TestCase(createdOn = "2017-07-05", updatedOn = "2017-07-05", testCaseId = "TC_Sales_Validate_NewTabAddedMfg_OnAdvanceSearch", testCaseDescription = "Verify in Manage Form Generator > Action button is coming")
	private void ValidateNewTabAddedInAdvanceSearch() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("sales", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			fc.utobj().printTestStep("Hitting Build Url and Then Login");
			driver = fc.loginpage().login(driver);
			String tabName = dataSet.get("tabDisplayName");
			String sectionName = dataSet.get("sectionValue");
			String fieldName = dataSet.get("displayName");
			String docTitle = dataSet.get("docTitle");
			fc.utobj().printTestStep("Go to Admin > Sales > Manage Form Generator");
			AdminSales adsales = new AdminSales();
			adsales.manageFormGenerator(driver);
			AdminFranchiseSalesManageFormGeneratorPageTest p1 = new AdminFranchiseSalesManageFormGeneratorPageTest();
			fc.utobj().printTestStep("Manage Form Generator > Adding New Tab");
			tabName = p1.addTabName(driver, tabName);

			boolean itabNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[contains(text () , '" + tabName + "')]");
			if (itabNamePresent == false) {
				fc.utobj().throwsException("was not able to verify added Tab in Manage Form Generator");
			}
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//a[contains(text(),'" + tabName + "')]"));

			fc.utobj().printTestStep("Add a Section in Manage Form Generator ");
			sectionName = p1.addSectionName(driver, sectionName);
			boolean issectionNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () , '" + sectionName + "')]");
			if (issectionNamePresent == false) {
				fc.utobj().throwsException("was not able to verify added sectionName in Manage Form Generator");
			}

			fc.utobj().printTestStep("Add Field Name in Manage Form Generator ");
			fieldName = p1.addFieldName(driver, sectionName, fieldName);
			boolean isFieldNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () , '" + fieldName + "')]");
			if (isFieldNamePresent == false) {
				fc.utobj().throwsException("was not able to verify added fieldName in Manage Form Generator");
			}

			fc.utobj().printTestStep("Add Document with Subject in Manage Form Generator ");
			docTitle = p1.addDocFieldName(driver, sectionName, fieldName, docTitle);

			boolean isdocTitlePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//td[contains(text () , '" + docTitle + "')]");
			if (isdocTitlePresent == false) {
				fc.utobj().throwsException("was not able to verify added Document in Manage Form Generator");
			}

			fc.utobj().printTestStep("Navigate to sales module Lead summary ");

			fc.loginpage().login(driver);

			fc.sales().sales_common().fsModule(driver);
			Sales fs = new Sales();
			fs.search(driver);
			FSSearchPageTest fsPage = new FSSearchPageTest();
			fc.utobj().printTestStep("Manage Form Generator > Searching New Tab in Advance Search");
			boolean valid = fsPage.searchTabByAdavaceSearch(driver, tabName);
			if (!valid) {
				fc.utobj().throwsException("Added Tag is visible in Advanced Search");
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "salesFormGenerate" })
	@TestCase(createdOn = "2017-07-05", updatedOn = "2017-07-05", testCaseId = "TC_Sales_Validate_NewTabAddedMfg_OnExportSearch", testCaseDescription = "Verify in Manage Form Generator > Verifying Tab should be visible at Export when a field exists in the Form")
	private void ValidateNewTabAddedInExportSearch() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("sales", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			fc.utobj().printTestStep("Hitting Build Url and Then Login");
			driver = fc.loginpage().login(driver);
			String tabName = dataSet.get("tabDisplayName");
			String sectionName = dataSet.get("sectionValue");
			String fieldName = dataSet.get("displayName");
			String docTitle = dataSet.get("docTitle");
			fc.utobj().printTestStep("Go to Admin > Sales > Manage Form Generator");

			AdminSales adsales = new AdminSales();
			adsales.manageFormGenerator(driver);
			AdminFranchiseSalesManageFormGeneratorPageTest p1 = new AdminFranchiseSalesManageFormGeneratorPageTest();
			fc.utobj().printTestStep("Manage Form Generator > Adding New Tab");
			tabName = p1.addTabName(driver, tabName);

			boolean itabNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[contains(text () , '" + tabName + "')]");
			if (itabNamePresent == false) {
				fc.utobj().throwsException("was not able to verify added Tab in Manage Form Generator");
			}
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//a[contains(text(),'" + tabName + "')]"));

			fc.utobj().printTestStep("Add a Section in Manage Form Generator ");
			sectionName = p1.addSectionName(driver, sectionName);
			boolean issectionNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () , '" + sectionName + "')]");
			if (issectionNamePresent == false) {
				fc.utobj().throwsException("was not able to verify added sectionName in Manage Form Generator");
			}

			fc.utobj().printTestStep("Add Field Name in Manage Form Generator ");
			fieldName = p1.addFieldName(driver, sectionName, fieldName);
			boolean isFieldNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text () , '" + fieldName + "')]");
			if (isFieldNamePresent == false) {
				fc.utobj().throwsException("was not able to verify added fieldName in Manage Form Generator");
			}

			fc.utobj().printTestStep("Add Document with Subject in Manage Form Generator ");
			docTitle = p1.addDocFieldName(driver, sectionName, fieldName, docTitle);

			boolean isdocTitlePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//td[contains(text () , '" + docTitle + "')]");
			if (isdocTitlePresent == false) {
				fc.utobj().throwsException("was not able to verify added Document in Manage Form Generator");
			}

			fc.utobj().printTestStep("Navigate to sales module Lead summary ");

			fc.loginpage().login(driver);
			fc.sales().sales_common().fsModule(driver);
			Sales fs = new Sales();
			fs.search(driver);
			FSSearchPageTest fsPage = new FSSearchPageTest();
			fc.utobj().printTestStep("Manage Form Generator > Searching New Tab in Advance Search");
			boolean valid = fsPage.searchTabByExport(driver, tabName);
			if (!valid) {
				fc.utobj().throwsException("Added Tag is not visible in Export Search");
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "salesFormGenerate" })
	@TestCase(createdOn = "2017-07-07", updatedOn = "2017-07-17", testCaseId = "TC_Sales_Validate_DefaultYes_IN_ModifyWindowPopUp", testCaseDescription = "Verify in Manage Form Generator > Action button is coming")
	private void ValidateModifyWindowDefaultYesOption() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("sales", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			fc.utobj().printTestStep("Hitting Build Url and Then Login");
			driver = fc.loginpage().login(driver);
			String tabName = dataSet.get("tabDisplayName");
			fc.utobj().printTestStep("Go to Admin > Sales > Manage Form Generator");

			AdminSales adsales = new AdminSales();
			adsales.manageFormGenerator(driver);
			AdminFranchiseSalesManageFormGeneratorPage pobj = new AdminFranchiseSalesManageFormGeneratorPage(driver);
			fc.utobj().clickElement(driver, pobj.continueBtn);

			fc.utobj().printTestStep("On Add new Tab Frame > Checking all The Contents");
			List<String> listItems = new LinkedList<String>();
			List<String> listItem = new ArrayList<String>();
			listItems.add(dataSet.get("Mandatory Note"));
			listItems.add(dataSet.get("Module"));
			listItems.add(dataSet.get("Display Name"));
			listItems.add(dataSet.get("Special Character Note"));
			listItems.add(dataSet.get("SubModuleName"));
			listItems.add(dataSet.get("IsActive"));
			listItems.add(dataSet.get("Is Exportable and Searchable"));
			listItems.add(dataSet.get("Accept multiple form inputs"));
			listItems.add(dataSet.get("Do you want to make Tab accessible to all"));
			fc.utobj().clickElement(driver, pobj.addNewTabBtn);
			fc.commonMethods().switch_cboxIframe_frameId(driver);

			boolean valid = fc.utobj().assertPageSourceWithMultipleRecords(driver, listItems);
			// fc.utobj().clickElement(driver, pobj.closeBtn);
			fc.utobj().printTestStep("On Add new Tab Frame > After Checking all The Contents > Close");
			if (valid) {
				tabName = fc.utobj().generateTestData(tabName);
				fc.utobj().sendKeys(driver, pobj.displayName, tabName);
				fc.utobj().selectDropDownByPartialText(driver, pobj.submoduleNameDrp, "Lead Management");
				if (!fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//*[@name='addMore' and @value='false']")
						&& !fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//*[@name='is-active' and @value='N']")
						&& !fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//*[@name='roleBase' and @value='false']")
						&& !fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//*[@name='is-exportable' and @value='false']")) {
					fc.utobj().throwsException("NO opt not found");
				}
				if (!fc.utobj().getElement(driver, pobj.accessmultipleinputYes).isSelected()) {
					fc.utobj().throwsException("Default Yes is not Selected in Access Multiple Input");
				}
				if (!fc.utobj().getElement(driver, pobj.isexportableYes).isSelected())
					fc.utobj().throwsException("Default Yes is not Selected in Is Exportable");
				if (!fc.utobj().getElement(driver, pobj.isactiveYes).isSelected())
					fc.utobj().throwsException("Default Yes is not Selected in Is Active");
				if (fc.utobj().getElement(driver, pobj.maketabaccessibletoallYes).isSelected()) {
					fc.utobj().getElementByXpath(driver, ".//*[@name='roleBase' and @value='false']").click();
					listItems.clear();
					listItems.add("Can View ");
					listItems.add("Can Write ");
					boolean val = fc.utobj().assertPageSourceWithMultipleRecords(driver, listItems);
					fc.utobj().clickElement(driver,
							fc.utobj().getElementByXpath(driver, ".//*[@id='ms-parentviewRoles1']/button"));

					if (fc.utobj().verifyCaseID(driver, "viewRoles1") && fc.utobj().verifyCaseID(driver, "writeRoles1")
							&& val) {

						List<WebElement> liElements = driver
								.findElements(By.xpath(".//*[@id='ms-parentviewRoles1']/div/ul/li"));
						String optionViewRole = dataSet.get("ViewRole");
						listItems.clear();

						for (int i = 1; i < liElements.size(); i++) {
							WebElement linkElement = driver.findElement(
									By.xpath(".//*[@id='ms-parentviewRoles1']/div/ul/li[" + i + "]/label"));

							System.out.println(linkElement.getText());
							listItems.add(linkElement.getText());
						}
						String[] optionViewRoleList = optionViewRole.split(",");

						Collections.addAll(listItem, optionViewRoleList);
					}
					val = listEqualsNoOrder(listItems, listItem);
					if (!val) {
						fc.utobj().throwsException("Both List Are Not Equal");
					}
					fc.utobj().clickElement(driver,
							fc.utobj().getElementByXpath(driver, ".//*[@id='ms-parentviewRoles1']/button"));

					fc.utobj().clickElement(driver,
							fc.utobj().getElementByXpath(driver, ".//*[@id='ms-parentwriteRoles1']/button"));

					if (fc.utobj().verifyCaseID(driver, "viewRoles1") && fc.utobj().verifyCaseID(driver, "writeRoles1")
							&& val) {

						List<WebElement> liElements = driver
								.findElements(By.xpath(".//*[@id='ms-parentwriteRoles1']/div/ul/li"));
						String optionViewRole = dataSet.get("WriteRole");
						listItems.clear();

						for (int i = 1; i < liElements.size(); i++) {
							WebElement linkElement = driver.findElement(
									By.xpath(".//*[@id='ms-parentwriteRoles1']/div/ul/li[" + i + "]/label"));

							System.out.println(linkElement.getText());
							listItems.add(linkElement.getText());
						}
						String[] optionViewRoleList = optionViewRole.split(",");

						Collections.addAll(listItem, optionViewRoleList);
					}
					val = listEqualsNoOrder(listItems, listItem);
					if (!val) {
						fc.utobj().throwsException("Both List Are Not Equal");
					}
				}
				fc.utobj().clickElement(driver, pobj.submitBtn);
				fc.utobj().assertSingleLinkText(driver, tabName);
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "salesFormGenerate" })
	@TestCase(createdOn = "2017-07-07", updatedOn = "2017-07-10", testCaseId = "TC_Sales_Validate_ModifyWindowPopUp_Options_Link", testCaseDescription = "Verify in Manage Form Generator >"
			+ " Following fields should be visible in Add New Tab window:" + "\n Module (Name of module should come)"
			+ "\n Display Name (Text Field)" + "\n Sub Module Name (Drop-down)" + "\n Is Active (Radio field)"
			+ "\n Is Exportable and Searchable (Radio field)" + "\n Accept multiple form inputs (Radio field)"
			+ "\n Do you want to make Tab accessible to all? (Radio field)"
			+ "\n Special character Note Mandatory Note")
	private void ValidateModifyWindowFrameFieldInMFG() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("sales", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			fc.utobj().printTestStep("Hitting Build Url and Then Login");
			driver = fc.loginpage().login(driver);
			String tabName = dataSet.get("tabDisplayName");
			fc.utobj().printTestStep("Go to Admin > Sales > Manage Form Generator");

			AdminSales adsales = new AdminSales();
			adsales.manageFormGenerator(driver);
			AdminFranchiseSalesManageFormGeneratorPage pobj = new AdminFranchiseSalesManageFormGeneratorPage(driver);
			fc.utobj().clickElement(driver, pobj.continueBtn);

			List<String> listItems = new LinkedList<String>();
			listItems.add(dataSet.get("Mandatory Note"));
			listItems.add(dataSet.get("Module"));
			listItems.add(dataSet.get("Display Name"));
			listItems.add(dataSet.get("Special Character Note"));
			listItems.add(dataSet.get("SubModuleName"));
			listItems.add(dataSet.get("IsActive"));
			listItems.add(dataSet.get("Is Exportable and Searchable"));
			listItems.add(dataSet.get("Accept multiple form inputs"));
			listItems.add(dataSet.get("Do you want to make Tab accessible to all"));
			fc.utobj().clickElement(driver, pobj.addNewTabBtn);
			fc.commonMethods().switch_cboxIframe_frameId(driver);

			boolean valid = fc.utobj().assertPageSourceWithMultipleRecords(driver, listItems);
			fc.utobj().clickElement(driver, pobj.closeBtn);
			// if (valid) {
			// tabName = fc.utobj().generateTestData(tabName);
			// fc.utobj().sendKeys(driver, pobj.displayName, tabName);
			// fc.utobj().selectDropDownByPartialText(driver,
			// pobj.submoduleNameDrp, "Lead Management");
			// fc.utobj().clickElement(driver, pobj.accessmultipleinputYes);
			// fc.utobj().clickElement(driver, pobj.maketabaccessibletoallYes);
			// fc.utobj().clickElement(driver, pobj.isexportableYes);
			// fc.utobj().clickElement(driver, pobj.isactiveYes);
			// fc.utobj().clickElement(driver, pobj.submitBtn);
			fc.utobj().assertSingleLinkText(driver, tabName);
			// }

			if (!valid) {
				fc.utobj().throwsException("All element is not found in modified tab frame");
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "salesFormGenerate" })
	@TestCase(createdOn = "2017-07-07", updatedOn = "2017-07-10", testCaseId = "TC_Sales_ValidateAlertMsg_OnAddNewTabMFG", testCaseDescription = "Verify in Manage Form Generator > Correct validation message should come")
	private void ValidateAlertMsg_OnAddNewTabMFG() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("sales", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			fc.utobj().printTestStep("Hitting Build Url and Then Login");
			driver = fc.loginpage().login(driver);
			String tabName = dataSet.get("tabDisplayName");
			fc.utobj().printTestStep("Go to Admin > Sales > Manage Form Generator");

			AdminSales adsales = new AdminSales();
			adsales.manageFormGenerator(driver);
			AdminFranchiseSalesManageFormGeneratorPage pobj = new AdminFranchiseSalesManageFormGeneratorPage(driver);
			fc.utobj().clickElement(driver, pobj.continueBtn);

			List<String> listItems = new LinkedList<String>();
			listItems.add(dataSet.get("Mandatory Note"));
			listItems.add(dataSet.get("Module"));
			listItems.add(dataSet.get("Display Name"));
			listItems.add(dataSet.get("Special Character Note"));
			listItems.add(dataSet.get("SubModuleName"));
			listItems.add(dataSet.get("IsActive"));
			listItems.add(dataSet.get("Is Exportable and Searchable"));
			listItems.add(dataSet.get("Accept multiple form inputs"));
			listItems.add(dataSet.get("Do you want to make Tab accessible to all"));
			fc.utobj().clickElement(driver, pobj.addNewTabBtn);
			fc.commonMethods().switch_cboxIframe_frameId(driver);

			boolean valid = fc.utobj().assertPageSourceWithMultipleRecords(driver, listItems);

			if (valid) {
				tabName = fc.utobj().generateTestData(tabName);
				fc.utobj().sendKeys(driver, pobj.displayName, tabName);
				fc.utobj().selectDropDownByPartialText(driver, pobj.submoduleNameDrp, "Lead Management");
				fc.utobj().clickElement(driver, pobj.accessmultipleinputYes);
				fc.utobj().clickElement(driver, pobj.maketabaccessibletoallYes);
				fc.utobj().clickElement(driver, pobj.isexportableYes);
				fc.utobj().clickElement(driver, pobj.isactiveYes);
				fc.utobj().clickElement(driver, pobj.submitBtn);
				fc.utobj().assertSingleLinkText(driver, tabName);
			}
			fc.utobj().printTestStep("Check with Empty Module name");
			fc.utobj().clickElement(driver, pobj.addNewTabBtn);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().clickElement(driver, pobj.submitBtn);
			fc.utobj().printTestStep("Veryfying Display msg coming on alert box");
			String msg = fc.utobj().acceptAlertBox(driver);
			if (!msg.equals("Please enter Display Name ."))
				fc.utobj().throwsException("message is not being displayed");
			fc.utobj().printTestStep("Check with Empty Submodule Name");
			fc.utobj().sendKeys(driver, pobj.displayName, tabName);
			fc.utobj().clickElement(driver, pobj.submitBtn);
			msg = fc.utobj().acceptAlertBox(driver);
			if (!msg.equals("Please select Sub Module Name.."))
				fc.utobj().throwsException("Please select Sub Module Name..");

			fc.utobj().sendKeys(driver, pobj.displayName, tabName);
			fc.utobj().selectDropDownByPartialText(driver, pobj.submoduleNameDrp, "Lead Management");
			fc.utobj().clickElement(driver, pobj.submitBtn);
			msg = fc.utobj().acceptAlertBox(driver);
			if (!msg.equals("Tab already exist for this module."))
				fc.utobj().throwsException("Please select Sub Module Name..");

			if (!valid) {
				fc.utobj().throwsException("All element is not found in modified tab frame");
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "salesFormGenerate" })
	@TestCase(createdOn = "2017-07-07", updatedOn = "2017-07-10", testCaseId = "TC_Sales_Validate_ModifyWindowPopUp_Options_withModule_Link", testCaseDescription = "New Tab shouldn't get added and correct validation message should come")
	private void ValidateModifyWindowFrameFieldWithModulenameInMFG() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("sales", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			fc.utobj().printTestStep("Hitting Build Url and Then Login");
			driver = fc.loginpage().login(driver);
			String tabName = dataSet.get("tabDisplayName");
			fc.utobj().printTestStep("Go to Admin > Sales > Manage Form Generator");

			AdminSales adsales = new AdminSales();
			adsales.manageFormGenerator(driver);
			AdminFranchiseSalesManageFormGeneratorPage pobj = new AdminFranchiseSalesManageFormGeneratorPage(driver);
			fc.utobj().clickElement(driver, pobj.continueBtn);

			List<String> listItems = new LinkedList<String>();
			listItems.add(dataSet.get("Mandatory Note"));
			listItems.add(dataSet.get("Module"));
			listItems.add(dataSet.get("sales"));
			listItems.add(dataSet.get("Display Name"));
			listItems.add(dataSet.get("Special Character Note"));
			listItems.add(dataSet.get("SubModuleName"));
			listItems.add(dataSet.get("IsActive"));
			listItems.add(dataSet.get("sales"));
			listItems.add(dataSet.get("Is Exportable and Searchable"));
			listItems.add(dataSet.get("Accept multiple form inputs"));
			listItems.add(dataSet.get("Do you want to make Tab accessible to all"));
			fc.utobj().clickElement(driver, pobj.addNewTabBtn);
			fc.commonMethods().switch_cboxIframe_frameId(driver);

			boolean valid = fc.utobj().assertPageSourceWithMultipleRecords(driver, listItems);
			if (valid) {
				tabName = fc.utobj().generateTestData(tabName);
				fc.utobj().sendKeys(driver, pobj.displayName, tabName);
				fc.utobj().selectDropDownByPartialText(driver, pobj.submoduleNameDrp, "Lead Management");
				fc.utobj().clickElement(driver, pobj.accessmultipleinputYes);
				fc.utobj().clickElement(driver, pobj.maketabaccessibletoallYes);
				fc.utobj().clickElement(driver, pobj.isexportableYes);
				fc.utobj().clickElement(driver, pobj.isactiveYes);
				fc.utobj().clickElement(driver, pobj.submitBtn);
				fc.utobj().assertSingleLinkText(driver, tabName);
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "salesFormGenerate" })
	@TestCase(createdOn = "2017-07-07", updatedOn = "2017-07-06", testCaseId = "TC_Sales_Validate_ModifyWindowPopUp_MFG_Link", testCaseDescription = "Verify in Manage Form Generator > "
			+ "Name of module should come for Module field"
			+ "Buttons: 'Save' and 'Close', should be visible in 'Add New Tab' window" + "Modify window should pop-up"
			+ "Following fields should be visible in 'Modify' tab:" + "Module (Name of module should come)"
			+ "Display Name (Text Field)" + "Special Characters Note" + "Mandatory Note"
			+ "Save and Close buttons should be visible in 'Modify' tab")
	private void ValidateActionBtnInMFG() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("sales", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			fc.utobj().printTestStep("Hitting Build Url and Then Login");
			driver = fc.loginpage().login(driver);
			// String tabName = dataSet.get("tabDisplayName");
			fc.utobj().printTestStep("Go to Admin > Sales > Manage Form Generator");

			AdminSales adsales = new AdminSales();
			adsales.manageFormGenerator(driver);
			// tabName = p1.addTabName(driver, tabName);
			AdminFranchiseSalesManageFormGeneratorPage pobj = new AdminFranchiseSalesManageFormGeneratorPage(driver);
			fc.utobj().clickElement(driver, pobj.continueBtn);

			List<String> SelectList = new ArrayList<String>();
			WebElement selectElement = fc.utobj().getElementByXpath(driver, ".//*[@id='formNames']");
			Select select = new Select(selectElement);
			List<WebElement> allOptions = select.getOptions();
			for (WebElement elt : allOptions) {
				System.out.println(elt.getText());
				SelectList.add(elt.getText());
			}
			// SelectList.add(tabName);
			SelectList.remove(0);
			SelectList.remove("Remark");
			SelectList.remove("Task");
			SelectList.remove("Call");

			for (String elt : SelectList) {
				System.out.println(elt);
				AdminSalesManageFormGeneratorPageTest ASMformgen = new AdminSalesManageFormGeneratorPageTest();
				ASMformgen.verifyElementAbsent(elt, driver);
				fc.utobj().actionImgOption(driver, elt, "Modify");

				fc.commonMethods().switch_cboxIframe_frameId(driver);

				List<String> listItems = new LinkedList<String>();
				String val = fc.utobj().getElementByXpath(driver, ".//*[@id='tabDisplayName']").getAttribute("value");
				if (!val.equals(elt))
					fc.utobj().throwsException(elt + " not found");
				System.out.println("Exception Came");
				listItems.add(dataSet.get("Mandatory Note"));
				listItems.add(dataSet.get("Module"));
				listItems.add(dataSet.get("Display Name"));
				listItems.add(dataSet.get("Special Character Note"));
				boolean check = fc.utobj().assertPageSourceWithMultipleRecords(driver, listItems);

				if (!check)
					fc.utobj().throwsException("Some problem came  with Modify pop up window");
				try {
					WebElement saveBtn = fc.utobj().getElementByXpath(driver, ".//*[@id='submitButton']");
				} catch (Exception ex) {
					fc.utobj().throwsException("Save button is not found on modify iframe");
				}
				fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='Button']"));
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "salesFormGenerate" })
	@TestCase(createdOn = "2017-07-05", updatedOn = "2017-07-05", testCaseId = "TC_Sales_Modify_Tab_With_Invalid_Char", testCaseDescription = "Verify in Manage Form Generator >Correct validation message should come")
	private void ModifyTabWithInvalidSpclChar() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("sales", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			fc.utobj().printTestStep("Hitting Build Url and Then Login");
			driver = fc.loginpage().login(driver);
			String tabName = dataSet.get("tabDisplayName");
			fc.utobj().printTestStep("Go to Admin > Sales > Manage Form Generator");

			AdminSales adsales = new AdminSales();
			adsales.manageFormGenerator(driver);
			AdminFranchiseSalesManageFormGeneratorPageTest p1 = new AdminFranchiseSalesManageFormGeneratorPageTest();
			tabName = p1.addTabName(driver, tabName);

			fc.utobj().actionImgOption(driver, tabName, "Modify");

			fc.commonMethods().switch_cboxIframe_frameId(driver);

			fc.utobj().sendKeys(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='tabDisplayName']"),
					dataSet.get("InValid Special Character") + dataSet.get("Display Name"));
			WebElement saveBtn = fc.utobj().getElementByXpath(driver, ".//*[@id='submitButton']");
			saveBtn.click();
			String msg = fc.utobj().acceptAlertBox(driver);
			String validatemsg = dataSet.get("Validate Alert Msg");
			if (!msg.equals(validatemsg)) {
				fc.utobj().throwsException("Alert Msg Is Different");
			}
			try {
				fc.utobj().getElementByXpath(driver, ".//*[@id='Button']").click();
			} catch (Exception e) {
				fc.utobj().throwsException("Modify Tag Frame is not getting close");
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "salesFormGenerate" })
	@TestCase(createdOn = "2017-07-05", updatedOn = "2017-07-05", testCaseId = "TC_Sales_Modify_Tab_With_Valid_Char", testCaseDescription = "Verify in Manage Form Generator > Action button is coming")
	private void ModifyTabWithValidSpclChar() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("sales", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			fc.utobj().printTestStep("Hitting Build Url and Then Login");
			driver = fc.loginpage().login(driver);
			String tabName = dataSet.get("tabDisplayName");
			fc.utobj().printTestStep("Go to Admin > Sales > Manage Form Generator");

			AdminSales adsales = new AdminSales();
			adsales.manageFormGenerator(driver);
			AdminFranchiseSalesManageFormGeneratorPageTest p1 = new AdminFranchiseSalesManageFormGeneratorPageTest();
			tabName = p1.addTabName(driver, tabName);

			fc.utobj().actionImgOption(driver, tabName, "Modify");

			fc.commonMethods().switch_cboxIframe_frameId(driver);

			fc.utobj().sendKeys(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='tabDisplayName']"),
					dataSet.get("Valid Special Character"));
			WebElement saveBtn = fc.utobj().getElementByXpath(driver, ".//*[@id='submitButton']");
			saveBtn.click();
			String msg = fc.utobj().acceptAlertBox(driver);
			String ValidationMsg = dataSet.get("Validate Alert Msg");
			if (!msg.equals(ValidationMsg)) {
				fc.utobj().throwsException(
						"Correct Msg %------- Display Name must begin with a letter. ---% is not coming");
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "salesFormGenerate" })
	@TestCase(createdOn = "2017-07-05", updatedOn = "2017-07-05", testCaseId = "TC_Sales_Modify_Tab_With_Valid_Char_Displayname", testCaseDescription = "Verify in Manage Form Generator > Action button is coming")
	private void ModifyTabWithDisplayNameSpclChar() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("sales", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			fc.utobj().printTestStep("Hitting Build Url and Then Login");
			driver = fc.loginpage().login(driver);
			String tabName = dataSet.get("tabDisplayName");
			fc.utobj().printTestStep("Go to Admin > Sales > Manage Form Generator");

			AdminSales adsales = new AdminSales();
			adsales.manageFormGenerator(driver);
			AdminFranchiseSalesManageFormGeneratorPageTest p1 = new AdminFranchiseSalesManageFormGeneratorPageTest();
			tabName = p1.addTabName(driver, tabName);

			fc.utobj().actionImgOption(driver, tabName, "Modify");

			fc.commonMethods().switch_cboxIframe_frameId(driver);

			tabName = dataSet.get("Display Name") + dataSet.get("Valid Special Character") + "Updated";
			fc.utobj().sendKeys(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='tabDisplayName']"), tabName);
			fc.utobj().getElementByXpath(driver, ".//*[@id='Button']").click();
			boolean validate = fc.utobj().assertPageSource(driver, tabName);
			if (validate)
				fc.utobj().throwsException("Modified tag name with clicking on Save Button");
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "salesFormGenerate" })
	@TestCase(createdOn = "2017-07-05", updatedOn = "2017-07-05", testCaseId = "TC_Sales_CheckModify_On_CloseBtn", testCaseDescription = "Verify in Manage Form Generator > Action button is coming")
	private void CheckingModificationWithClosebtn() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("sales", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			fc.utobj().printTestStep("Hitting Build Url and Then Login");
			driver = fc.loginpage().login(driver);
			String tabName = dataSet.get("tabDisplayName");
			fc.utobj().printTestStep("Go to Admin > Sales > Manage Form Generator");

			AdminSales adsales = new AdminSales();
			adsales.manageFormGenerator(driver);
			AdminFranchiseSalesManageFormGeneratorPageTest p1 = new AdminFranchiseSalesManageFormGeneratorPageTest();
			tabName = p1.addTabName(driver, tabName);

			fc.utobj().actionImgOption(driver, tabName, "Modify");

			fc.commonMethods().switch_cboxIframe_frameId(driver);

			fc.utobj().sendKeys(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='tabDisplayName']"),
					dataSet.get("Display Name") + dataSet.get("Valid Special Character"));

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "salesFormGenerate" })
	@TestCase(createdOn = "2017-07-05", updatedOn = "2017-07-05", testCaseId = "TC_Sales_Modify_Tab_With_BlankSpace", testCaseDescription = "Verify in Manage Form Generator > Action button is coming")
	private void ModifyTabWithblankspace() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("sales", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			fc.utobj().printTestStep("Hitting Build Url and Then Login");
			driver = fc.loginpage().login(driver);
			String tabName = dataSet.get("tabDisplayName");
			fc.utobj().printTestStep("Go to Admin > Sales > Manage Form Generator");

			AdminSales adsales = new AdminSales();
			adsales.manageFormGenerator(driver);
			AdminFranchiseSalesManageFormGeneratorPageTest p1 = new AdminFranchiseSalesManageFormGeneratorPageTest();
			tabName = p1.addTabName(driver, tabName);

			fc.utobj().actionImgOption(driver, tabName, "Modify");

			fc.commonMethods().switch_cboxIframe_frameId(driver);

			fc.utobj().sendKeys(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='tabDisplayName']"), "");
			WebElement saveBtn = fc.utobj().getElementByXpath(driver, ".//*[@id='submitButton']");
			saveBtn.click();
			String msg = fc.utobj().acceptAlertBox(driver);
			String validatemsg = dataSet.get("Validate Alert Msg");
			if (!msg.equals(validatemsg)) {
				fc.utobj().throwsException("Alert Msg Is Different");
			}
			try {
				fc.utobj().getElementByXpath(driver, ".//*[@id='Button']").click();
			} catch (Exception e) {
				fc.utobj().throwsException("Modify Tag Frame is not getting close");
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "salesFormGenerate" })
	@TestCase(createdOn = "2017-07-05", updatedOn = "2017-07-05", testCaseId = "TC_Sales_Modify_Tab_With_Number", testCaseDescription = "Verify in Manage Form Generator > Action button is coming")
	private void ModifyTabWithNumber() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("sales", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			fc.utobj().printTestStep("Hitting Build Url and Then Login");
			driver = fc.loginpage().login(driver);
			String tabName = dataSet.get("tabDisplayName");
			fc.utobj().printTestStep("Go to Admin > Sales > Manage Form Generator");

			AdminSales adsales = new AdminSales();
			adsales.manageFormGenerator(driver);
			AdminFranchiseSalesManageFormGeneratorPageTest p1 = new AdminFranchiseSalesManageFormGeneratorPageTest();
			tabName = p1.addTabName(driver, tabName);

			fc.utobj().actionImgOption(driver, tabName, "Modify");

			fc.commonMethods().switch_cboxIframe_frameId(driver);

			fc.utobj().sendKeys(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='tabDisplayName']"),
					dataSet.get("Number"));
			WebElement saveBtn = fc.utobj().getElementByXpath(driver, ".//*[@id='submitButton']");
			saveBtn.click();
			String Msg = fc.utobj().acceptAlertBox(driver);
			String ValidateMsg = dataSet.get("Validate Alert Msg");
			if (!Msg.equals(ValidateMsg)) {
				fc.utobj().throwsException("Alert Msg Is Different");
			}
			try {
				fc.utobj().getElementByXpath(driver, ".//*[@id='Button']").click();
			} catch (Exception e) {
				fc.utobj().throwsException("Modify Tag Frame is not getting close");
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "salesFormGenerate" })
	@TestCase(createdOn = "2017-07-05", updatedOn = "2017-07-05", testCaseId = "TC_Sales_Modify_Tab_With_NameAndNumber", testCaseDescription = "Verify in Manage Form Generator > Action button is coming")
	private void ModifyTabWithNameAndNumber() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("sales", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			fc.utobj().printTestStep("Hitting Build Url and Then Login");
			driver = fc.loginpage().login(driver);
			String tabName = dataSet.get("tabDisplayName");
			fc.utobj().printTestStep("Go to Admin > Sales > Manage Form Generator");

			AdminSales adsales = new AdminSales();
			adsales.manageFormGenerator(driver);
			AdminFranchiseSalesManageFormGeneratorPageTest p1 = new AdminFranchiseSalesManageFormGeneratorPageTest();
			tabName = p1.addTabName(driver, tabName);

			fc.utobj().actionImgOption(driver, tabName, "Modify");

			fc.commonMethods().switch_cboxIframe_frameId(driver);

			fc.utobj().sendKeys(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='tabDisplayName']"),
					dataSet.get("Display Name") + dataSet.get("Number"));
			WebElement saveBtn = fc.utobj().getElementByXpath(driver, ".//*[@id='submitButton']");
			saveBtn.click();
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "salesFormGenerate" })
	@TestCase(createdOn = "2017-07-05", updatedOn = "2017-07-05", testCaseId = "TC_Sales_Modify_Tab_With_NumberAndName", testCaseDescription = "Verify in Manage Form Generator > Action button is coming")
	private void ModifyTabWithNumberAndName() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("sales", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			fc.utobj().printTestStep("Hitting Build Url and Then Login");
			driver = fc.loginpage().login(driver);
			String tabName = dataSet.get("tabDisplayName");
			fc.utobj().printTestStep("Go to Admin > Sales > Manage Form Generator");

			AdminSales adsales = new AdminSales();
			adsales.manageFormGenerator(driver);
			AdminFranchiseSalesManageFormGeneratorPageTest p1 = new AdminFranchiseSalesManageFormGeneratorPageTest();
			tabName = p1.addTabName(driver, tabName);

			fc.utobj().actionImgOption(driver, tabName, "Modify");

			fc.commonMethods().switch_cboxIframe_frameId(driver);

			fc.utobj().sendKeys(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='tabDisplayName']"),
					dataSet.get("Number") + dataSet.get("Display Name"));
			WebElement saveBtn = fc.utobj().getElementByXpath(driver, ".//*[@id='submitButton']");
			saveBtn.click();
			String msg = fc.utobj().acceptAlertBox(driver);
			String ValidateMsg = dataSet.get("Validate Alert Msg");
			if (!msg.equals(ValidateMsg)) {
				fc.utobj().throwsException("Alert Msg Is Different");
			}
			try {
				fc.utobj().getElementByXpath(driver, ".//*[@id='Button']").click();
			} catch (Exception e) {
				fc.utobj().throwsException("Modify Tag Frame is not getting close");
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}
}
