package com.builds.test.fs;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import com.builds.test.admin.AdminUsersRolesAddNewRolePageTest;
import com.builds.uimaps.fs.AdminFranchiseSalesManageFormGeneratorPage;
import com.builds.utilities.FranconnectUtil;
import com.builds.utilities.TestCase;

public class AdminSalesManageFormGeneratorModifyActionNewTab
		extends AdminSalesManageFormGeneratorModifyActionBtnPageTest {
	FranconnectUtil fc = new FranconnectUtil();

	@Test(groups = { "salesFormGenerate" })
	@TestCase(createdOn = "2017-07-14", updatedOn = "2017-07-17", testCaseId = "TC_Sales_Validate_NewroleAdded_InAddTabMFG", testCaseDescription = "Verify Tab should not be visible at Export when a field does not exist in the Form")
	private void ValidateNewTabAddedInExportSearch_1() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("sales", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			String roleName = dataSet.get("rolename") + fc.utobj().generateRandomNumber6Digit();
			driver = fc.loginpage().login(driver);
			AdminUsersRolesAddNewRolePageTest addrole = new AdminUsersRolesAddNewRolePageTest();
			addrole.addCorporateRoles(driver, roleName);
			fc.utobj().printTestStep("Hitting Build Url and Then Login");
			// driver = fc.loginpage().login(driver, config);
			String tabName = dataSet.get("tabDisplayName");
			fc.utobj().printTestStep("Go to Admin > Sales > Manage Form Generator");

			AdminSales adsales = new AdminSales();
			adsales.manageFormGenerator(driver);
			AdminFranchiseSalesManageFormGeneratorPage pobj = new AdminFranchiseSalesManageFormGeneratorPage(driver);
			fc.utobj().clickElement(driver, pobj.continueBtn);
			;
			fc.utobj().printTestStep("On Add new Tab Frame > Checking all The Contents");
			List<String> listItems = new LinkedList<String>();
			List<String> listItem = new ArrayList<String>();
			listItems.add(dataSet.get("Mandatory Note"));
			listItems.add(dataSet.get("Module"));
			listItems.add(dataSet.get("Display Name"));
			listItems.add(dataSet.get("Special Character Note"));
			listItems.add(dataSet.get("SubModuleName"));
			listItems.add(dataSet.get("IsActive"));
			listItems.add(dataSet.get("Is Exportable and Searchable"));
			listItems.add(dataSet.get("Accept multiple form inputs"));
			listItems.add(dataSet.get("Do you want to make Tab accessible to all"));
			fc.utobj().clickElement(driver, pobj.addNewTabBtn);
			fc.commonMethods().switch_cboxIframe_frameId(driver);

			boolean valid = fc.utobj().assertPageSourceWithMultipleRecords(driver, listItems);
			// fc.utobj().clickElement(driver, pobj.closeBtn);
			fc.utobj().printTestStep("On Add new Tab Frame > After Checking all The Contents > Close");
			if (valid) {
				tabName = fc.utobj().generateTestData(tabName);
				fc.utobj().sendKeys(driver, pobj.displayName, tabName);
				fc.utobj().selectDropDownByPartialText(driver, pobj.submoduleNameDrp, "Lead Management");

				if (!fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//*[@name='addMore' and @value='false']")
						&& !fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//*[@name='is-active' and @value='N']")
						&& !fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//*[@name='roleBase' and @value='false']")
						&& !fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//*[@name='is-exportable' and @value='false']")) {
					fc.utobj().throwsException("NO opt not found");
				}

				if (!fc.utobj().getElement(driver, pobj.accessmultipleinputYes).isSelected()) {
					fc.utobj().throwsException("Default Yes is not Selected in Access Multiple Input");
				}
				if (!fc.utobj().getElement(driver, pobj.isexportableYes).isSelected())
					fc.utobj().throwsException("Default Yes is not Selected in Is Exportable");
				if (!fc.utobj().getElement(driver, pobj.isactiveYes).isSelected())
					fc.utobj().throwsException("Default Yes is not Selected in Is Active");
				if (fc.utobj().getElement(driver, pobj.maketabaccessibletoallYes).isSelected()) {
					fc.utobj().getElementByXpath(driver, ".//*[@name='roleBase' and @value='false']").click();
					listItems.clear();
					listItems.add("Can View ");
					listItems.add("Can Write ");
					boolean val = fc.utobj().assertPageSourceWithMultipleRecords(driver, listItems);
					fc.utobj().clickElement(driver,
							fc.utobj().getElementByXpath(driver, ".//*[@id='ms-parentviewRoles1']/button"));
					fc.utobj().sleep(999);
					if (fc.utobj().verifyCaseID(driver, "viewRoles1") && fc.utobj().verifyCaseID(driver, "writeRoles1")
							&& val) {
						;
						List<WebElement> liElements = driver
								.findElements(By.xpath(".//*[@id='ms-parentviewRoles1']/div/ul/li"));
						String optionViewRole = dataSet.get("ViewRole");
						listItems.clear();

						for (int i = 1; i < liElements.size(); i++) {
							WebElement linkElement = driver.findElement(
									By.xpath(".//*[@id='ms-parentviewRoles1']/div/ul/li[" + i + "]/label"));

							System.out.println(linkElement.getText());
							listItems.add(linkElement.getText());
						}
						String[] optionViewRoleList = optionViewRole.split(",");

						Collections.addAll(listItem, optionViewRoleList);
						listItem.add(roleName);
					}
					val = listEqualsNoOrder(listItems, listItem);
					if (!val) {
						fc.utobj().throwsException("Both List Are Not Equal");
					}
					fc.utobj().clickElement(driver,
							fc.utobj().getElementByXpath(driver, ".//*[@id='ms-parentviewRoles1']/button"));
					fc.utobj().sleep(1500);
					fc.utobj().clickElement(driver,
							fc.utobj().getElementByXpath(driver, ".//*[@id='ms-parentwriteRoles1']/button"));
					fc.utobj().sleep(999);
					if (fc.utobj().verifyCaseID(driver, "viewRoles1") && fc.utobj().verifyCaseID(driver, "writeRoles1")
							&& val) {
						;
						List<WebElement> liElements = driver
								.findElements(By.xpath(".//*[@id='ms-parentwriteRoles1']/div/ul/li"));
						String optionViewRole = dataSet.get("WriteRole");
						listItems.clear();

						for (int i = 1; i < liElements.size(); i++) {
							WebElement linkElement = driver.findElement(
									By.xpath(".//*[@id='ms-parentwriteRoles1']/div/ul/li[" + i + "]/label"));

							System.out.println(linkElement.getText());
							listItems.add(linkElement.getText());
						}
						String[] optionViewRoleList = optionViewRole.split(",");

						Collections.addAll(listItem, optionViewRoleList);
						listItem.add(roleName);
					}
					val = listEqualsNoOrder(listItems, listItem);
					if (!val) {
						fc.utobj().throwsException("Both List Are Not Equal");
					}
				}
			}
			fc.utobj().clickElement(driver, pobj.submitBtn);
			fc.utobj().assertSingleLinkText(driver, tabName);
			addrole.deleteRoles(driver, roleName, config);
			adsales.manageFormGenerator(driver);
			fc.utobj().clickElement(driver, pobj.continueBtn);

			fc.utobj().clickElement(driver, pobj.addNewTabBtn);
			fc.commonMethods().switch_cboxIframe_frameId(driver);

			valid = fc.utobj().assertPageSourceWithMultipleRecords(driver, listItems);
			// fc.utobj().clickElement(driver, pobj.closeBtn);
			fc.utobj().printTestStep("On Add new Tab Frame > After Checking all The Contents > Close");
			tabName = fc.utobj().generateTestData(tabName);
			fc.utobj().sendKeys(driver, pobj.displayName, tabName);
			fc.utobj().selectDropDownByPartialText(driver, pobj.submoduleNameDrp, "Lead Management");

			if (!fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//*[@name='addMore' and @value='false']")
					&& !fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//*[@name='is-active' and @value='N']")
					&& !fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//*[@name='roleBase' and @value='false']")
					&& !fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//*[@name='is-exportable' and @value='false']")) {
				fc.utobj().throwsException("NO opt not found");
			}

			if (!fc.utobj().getElement(driver, pobj.accessmultipleinputYes).isSelected()) {
				fc.utobj().throwsException("Default Yes is not Selected in Access Multiple Input");
			}
			if (!fc.utobj().getElement(driver, pobj.isexportableYes).isSelected())
				fc.utobj().throwsException("Default Yes is not Selected in Is Exportable");
			if (!fc.utobj().getElement(driver, pobj.isactiveYes).isSelected())
				fc.utobj().throwsException("Default Yes is not Selected in Is Active");
			if (fc.utobj().getElement(driver, pobj.maketabaccessibletoallYes).isSelected()) {
				fc.utobj().getElementByXpath(driver, ".//*[@name='roleBase' and @value='false']").click();
				listItems.clear();
				listItems.add("Can View ");
				listItems.add("Can Write ");
				boolean val = fc.utobj().assertPageSourceWithMultipleRecords(driver, listItems);
				fc.utobj().clickElement(driver,
						fc.utobj().getElementByXpath(driver, ".//*[@id='ms-parentviewRoles1']/button"));
				fc.utobj().sleep(999);
				if (fc.utobj().verifyCaseID(driver, "viewRoles1") && fc.utobj().verifyCaseID(driver, "writeRoles1")
						&& val) {
					;
					List<WebElement> liElements = driver
							.findElements(By.xpath(".//*[@id='ms-parentviewRoles1']/div/ul/li"));
					String optionViewRole = dataSet.get("ViewRole");
					listItems.clear();

					for (int i = 1; i < liElements.size(); i++) {
						WebElement linkElement = driver
								.findElement(By.xpath(".//*[@id='ms-parentviewRoles1']/div/ul/li[" + i + "]/label"));

						System.out.println(linkElement.getText());
						listItems.add(linkElement.getText());
					}
					String[] optionViewRoleList = optionViewRole.split(",");

					Collections.addAll(listItem, optionViewRoleList);
					listItem.add(roleName);
				}
				val = listEqualsNoOrder(listItems, listItem);
				if (val) {
					fc.utobj().throwsException("Role wasn't Deleted");
				}
				fc.utobj().clickElement(driver,
						fc.utobj().getElementByXpath(driver, ".//*[@id='ms-parentviewRoles1']/button"));
				fc.utobj().sleep(1500);
				fc.utobj().clickElement(driver,
						fc.utobj().getElementByXpath(driver, ".//*[@id='ms-parentwriteRoles1']/button"));
				fc.utobj().sleep(999);
				if (fc.utobj().verifyCaseID(driver, "viewRoles1") && fc.utobj().verifyCaseID(driver, "writeRoles1")
						&& val) {
					;
					List<WebElement> liElements = driver
							.findElements(By.xpath(".//*[@id='ms-parentwriteRoles1']/div/ul/li"));
					String optionViewRole = dataSet.get("WriteRole");
					listItems.clear();

					for (int i = 1; i < liElements.size(); i++) {
						WebElement linkElement = driver
								.findElement(By.xpath(".//*[@id='ms-parentwriteRoles1']/div/ul/li[" + i + "]/label"));

						System.out.println(linkElement.getText());
						listItems.add(linkElement.getText());
					}
					String[] optionViewRoleList = optionViewRole.split(",");

					Collections.addAll(listItem, optionViewRoleList);
					listItem.add(roleName);
				}
				val = listEqualsNoOrder(listItems, listItem);
				if (val) {
					fc.utobj().throwsException("Role wasn't Deleted");
				}
			}
			fc.utobj().clickElement(driver, pobj.submitBtn);
			fc.utobj().assertSingleLinkText(driver, tabName);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "salesFormGenerate" })
	@TestCase(createdOn = "2017-07-14", updatedOn = "2017-07-17", testCaseId = "TC_Sales_Validate_DeactivateCancelAlert", testCaseDescription = "Tab should not be visible at Export when a field does not exist in the Form")
	private void ValidateNewTabAddedInAdvanceSearch_1() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("sales", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			fc.utobj().printTestStep("Hitting Build Url and Then Login");
			driver = fc.loginpage().login(driver);
			String tabName = dataSet.get("tabDisplayName");
			fc.utobj().printTestStep("Go to Admin > Sales > Manage Form Generator");
			AdminSales adsales = new AdminSales();
			adsales.manageFormGenerator(driver);
			AdminFranchiseSalesManageFormGeneratorPageTest p1 = new AdminFranchiseSalesManageFormGeneratorPageTest();
			fc.utobj().printTestStep("Manage Form Generator > Adding New Tab");
			tabName = p1.addTabName(driver, tabName);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,(".//*[contains(text(),'" + tabName + "')]/parent::td/following-sibling::td[1]/a")));
			fc.utobj().dismissAlertBox(driver);
			fc.utobj().assertSingleLinkText(driver, tabName);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "salesFormGenerate" })
	@TestCase(createdOn = "2017-07-14", updatedOn = "2017-07-17", testCaseId = "TC_Sales_Validate_Deactivate", testCaseDescription = "Tab should not be visible at Export when a field does not exist in the Form")
	private void ValidateNewTabAddedInAdvanceSearch() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("sales", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			fc.utobj().printTestStep("Hitting Build Url and Then Login");
			driver = fc.loginpage().login(driver);
			String tabName = dataSet.get("tabDisplayName");
			fc.utobj().printTestStep("Go to Admin > Sales > Manage Form Generator");
			AdminSales adsales = new AdminSales();
			adsales.manageFormGenerator(driver);
			AdminFranchiseSalesManageFormGeneratorPageTest p1 = new AdminFranchiseSalesManageFormGeneratorPageTest();
			fc.utobj().printTestStep("Manage Form Generator > Adding New Tab");
			tabName = p1.addTabName(driver, tabName);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,(".//*[contains(text(),'" + tabName + "')]/parent::td/following-sibling::td[1]/a")));
			fc.utobj().acceptAlertBox(driver);
			boolean val = fc.utobj().assertLinkText(driver, tabName);
			if (val) {
				fc.utobj().throwsException("Link is not deactivated");
			}
			fc.utobj().actionImgOption(driver, tabName, "Activate");
			fc.utobj().dismissAlertBox(driver);
			fc.utobj().actionImgOption(driver, tabName, "Delete");
			fc.utobj().dismissAlertBox(driver);
			fc.utobj().clickElement(driver, driver
					.findElement(By.xpath(".//*[contains(text(),'" + tabName + "')]/following-sibling::td[1]/a")));
			String msg = fc.utobj().dismissAlertBox(driver);
			if (!msg.equals("Are you sure you want to activate this tab?")) {
				fc.utobj().throwsException("While clicking on redcross validation msg is different or not coming");
			}
			val = fc.utobj().assertLinkText(driver, tabName);
			if (val) {
				fc.utobj().throwsException("Link got activated");
			}
			fc.utobj().clickElement(driver, driver
					.findElement(By.xpath(".//*[contains(text(),'" + tabName + "')]/following-sibling::td[1]/a")));
			fc.utobj().acceptAlertBox(driver);
			val = fc.utobj().assertLinkText(driver, tabName);
			if (!val) {
				fc.utobj().throwsException("Link is not activated");
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "salesFormGenerate" })
	@TestCase(createdOn = "2017-07-18", updatedOn = "2017-07-19", testCaseId = "TC_Sales_Validate_Deactivate_From_ActBtn", testCaseDescription = "Tab should not be visible at Export when a field does not exist in the Form")
	private void ValidateDeactivate_From_ActBtn() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("sales", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			fc.utobj().printTestStep("Hitting Build Url and Then Login");
			driver = fc.loginpage().login(driver);
			String tabName = dataSet.get("tabDisplayName");
			fc.utobj().printTestStep("Go to Admin > Sales > Manage Form Generator");
			AdminSales adsales = new AdminSales();
			adsales.manageFormGenerator(driver);
			AdminFranchiseSalesManageFormGeneratorPageTest p1 = new AdminFranchiseSalesManageFormGeneratorPageTest();
			fc.utobj().printTestStep("Manage Form Generator > Adding New Tab");
			tabName = p1.addTabName(driver, tabName);
			boolean val = fc.utobj().assertLinkText(driver, tabName);
			if (val) {
				fc.utobj().throwsException("Link is not deactivated");
			}
			fc.utobj().actionImgOption(driver, tabName, "DeActivate");
			String msg = fc.utobj().dismissAlertBox(driver);
			if (!msg.equals("Are you sure you want to deactivate this tab?")) {
				fc.utobj().throwsException("While deactivating alertmsg is different");
			}
			fc.utobj().assertSingleLinkText(driver, tabName);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "salesFormGenerate" })
	@TestCase(createdOn = "2017-07-19", updatedOn = "2017-07-19", testCaseId = "TC_Sales_Validate_Deactivate_From_ActBtn_01", testCaseDescription = "Tab should not be visible at Export when a field does not exist in the Form")
	private void ValidateDeactivate_From_ActBtn01() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("sales", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			fc.utobj().printTestStep("Hitting Build Url and Then Login");
			driver = fc.loginpage().login(driver);
			String tabName = dataSet.get("tabDisplayName");
			fc.utobj().printTestStep("Go to Admin > Sales > Manage Form Generator");
			AdminSales adsales = new AdminSales();
			adsales.manageFormGenerator(driver);
			AdminFranchiseSalesManageFormGeneratorPageTest p1 = new AdminFranchiseSalesManageFormGeneratorPageTest();
			fc.utobj().printTestStep("Manage Form Generator > Adding New Tab");
			tabName = p1.addTabName(driver, tabName);
			boolean val = fc.utobj().assertLinkText(driver, tabName);
			if (val) {
				fc.utobj().throwsException("Link is not deactivated");
			}
			fc.utobj().actionImgOption(driver, tabName, "DeActivate");
			String msg = fc.utobj().acceptAlertBox(driver);
			if (!msg.equals("Are you sure you want to deactivate this tab?")) {
				fc.utobj().throwsException("While deactivating alertmsg is different");
			}
			boolean valid = fc.utobj().assertLinkText(driver, tabName);
			if (valid) {
				fc.utobj().throwsException("tab not deactivated from actin button");
			}
			fc.utobj().actionImgOption(driver, tabName, "Activate");
			msg = fc.utobj().dismissAlertBox(driver);
			if (!msg.equals("Are you sure you want to activate this tab?")) {
				fc.utobj().throwsException("While deactivating alertmsg is different");
			}
			fc.utobj().actionImgOption(driver, tabName, "Activate");
			msg = fc.utobj().acceptAlertBox(driver);
			valid = fc.utobj().assertLinkText(driver, tabName);
			if (!valid) {
				fc.utobj().throwsException("tab not Activated from acti0n button");
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

}
