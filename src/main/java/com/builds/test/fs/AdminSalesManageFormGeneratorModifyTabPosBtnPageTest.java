package com.builds.test.fs;

import java.util.Map;

import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.Test;

import com.builds.uimaps.fs.AdminFranchiseSalesManageFormGeneratorPage;
import com.builds.utilities.FranconnectUtil;
import com.builds.utilities.TestCase;

public class AdminSalesManageFormGeneratorModifyTabPosBtnPageTest
		extends AdminFranchiseSalesManageFormGeneratorPageTest {
	FranconnectUtil fc = new FranconnectUtil();

	@Test(groups = { "salesFormGenerate" })
	@TestCase(createdOn = "2017-07-05", updatedOn = "2017-07-05", testCaseId = "TC_Sales_Validate_ModifyTabPosition", testCaseDescription = "Verify in Manage Form Generator > Action button is coming")
	private void ValidateActionBtnInMFG() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("sales", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			fc.utobj().printTestStep("Hitting Build Url and Then Login");
			AdminFranchiseSalesManageFormGeneratorPage pobj = new AdminFranchiseSalesManageFormGeneratorPage(driver);
			driver = fc.loginpage().login(driver);
			fc.utobj().printTestStep("Go to Admin > Sales > Manage Form Generator");

			AdminSales adsales = new AdminSales();
			adsales.manageFormGenerator(driver);
			fc.utobj().clickElement(driver, pobj.continueBtn);
			fc.utobj().clickElement(driver, pobj.ModifyTabPosBtn);
			WebElement sourceElement = fc.utobj().getElementByXpath(driver,
					".//*[contains(text(),'Candidate Portal')]");
			WebElement destinationElement = fc.utobj().getElementByXpath(driver, ".//*[contains(text(),'Visit')]");
			dragAndDrop(sourceElement, destinationElement, driver);

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	public void dragAndDrop(WebElement sourceElement, WebElement destinationElement, WebDriver driver) {
		try {
			if (sourceElement.isDisplayed() && destinationElement.isDisplayed()) {
				Actions action = new Actions(driver);
				action.dragAndDrop(sourceElement, destinationElement).build().perform();
			} else {
				System.out.println("Element was not displayed to drag");
			}
		} catch (StaleElementReferenceException e) {
			System.out.println("Element with " + sourceElement + "or" + destinationElement
					+ "is not attached to the page document " + e.getStackTrace());
		} catch (NoSuchElementException e) {
			System.out.println("Element " + sourceElement + "or" + destinationElement + " was not found in DOM "
					+ e.getStackTrace());
		} catch (Exception e) {
			System.out.println("Error occurred while performing drag and drop operation " + e.getStackTrace());
		}
	}
}
