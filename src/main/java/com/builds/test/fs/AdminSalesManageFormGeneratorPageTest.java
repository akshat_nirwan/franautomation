package com.builds.test.fs;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.Test;

import com.builds.uimaps.fs.AdminFranchiseSalesManageFormGeneratorPage;
import com.builds.utilities.FranconnectUtil;
import com.builds.utilities.TestCase;

public class AdminSalesManageFormGeneratorPageTest extends AdminFranchiseSalesManageFormGeneratorPageTest {

	FranconnectUtil fc = new FranconnectUtil();

	@Test(groups = { "salesFormGenerate" })
	@TestCase(createdOn = "2017-07-05", updatedOn = "2017-07-05", testCaseId = "TC_Sales_Validate_ActionBtnIn_MFG_Link", testCaseDescription = "Verify in Manage Form Generator > Action button is coming")
	private void ValidateActionBtnInMFG() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("sales", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			fc.utobj().printTestStep("Hitting Build Url and Then Login");
			driver = fc.loginpage().login(driver);
			String tabName = dataSet.get("tabDisplayName");
			fc.utobj().printTestStep("Go to Admin > Sales > Manage Form Generator");

			AdminSales adsales = new AdminSales();
			adsales.manageFormGenerator(driver);
			AdminFranchiseSalesManageFormGeneratorPageTest p1 = new AdminFranchiseSalesManageFormGeneratorPageTest();
			tabName = p1.addTabName(driver, tabName);

			String DefaultTags = dataSet.get("DefaultTags");

			String[] str = null;
			str = DefaultTags.split(",");
			List<String> listItem = new ArrayList<String>();
			Collections.addAll(listItem, str);
			listItem.add(tabName);
			for (String elt : listItem) {
				verifyElementAbsent(elt, driver);
				fc.utobj().actionImgOption(driver, elt, "Modify");
				fc.commonMethods().switch_cboxIframe_frameId(driver);

				fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='Button']"));
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "salesFormGenerate" })
	@TestCase(createdOn = "2017-07-04", updatedOn = "2017-07-04", testCaseId = "TC_Sales_Validate_Admin_MFG_Link", testCaseDescription = "Verify in Manage Form Generator > Admin Link is Clickable")
	private void verifyAdminLinkOnMFG() throws Exception {

		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("sales", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			fc.utobj().printTestStep("Hitting Build Url and Then Login");
			driver = fc.loginpage().login(driver);

			fc.utobj().printTestStep("Go to Admin > Sales > Manage Form Generator");

			AdminSales adsales = new AdminSales();
			adsales.manageFormGenerator(driver);
			AdminFranchiseSalesManageFormGeneratorPage pobj = new AdminFranchiseSalesManageFormGeneratorPage(driver);
			fc.utobj().printTestStep("Validating Form Generator Instructions");
			List<String> listItemsInstructions = new LinkedList<String>();
			listItemsInstructions.add("Tab Management");
			listItemsInstructions.add("Section Management");
			listItemsInstructions.add("Field Management");
			listItemsInstructions.add("Document Management");
			;
			fc.utobj().assertPageSourceWithMultipleRecords(driver, listItemsInstructions);
			fc.utobj().printTestStep("Validating Countinue Button");
			fc.utobj().clickElement(driver, pobj.continueBtn);
			fc.utobj().printTestStep("Validating Admin Link");
			fc.utobj().clickElement(driver, pobj.AdminLink);
			fc.utobj().printTestStep("Go to Admin > Sales > Manage Form Generator");
			adsales.manageFormGenerator(driver);
			fc.utobj().printTestStep("Validating Back Button");
			fc.utobj().clickElement(driver, pobj.BackBtn);

			JavascriptExecutor executor = (JavascriptExecutor) driver;
			Long value = (Long) executor.executeScript("return window.pageYOffset;");
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//*[@href='#Franchise_Sales']"));

			JavascriptExecutor executor1 = (JavascriptExecutor) driver;
			Long value1 = (Long) executor1.executeScript("return window.pageYOffset;");
			int diff = (int) (value - value1);
			if (diff < 0) {
				diff -= 1;
			}
			if (diff > 100) {
				fc.utobj().throwsException("Back button is not sending on the same position");
			}
			adsales.manageFormGenerator(driver);
			WebElement selectElement = fc.utobj().getElementByXpath(driver, ".//*[@id='formNames']");
			Select select = new Select(selectElement);
			WebElement option = select.getFirstSelectedOption();
			if (option.getText().equals("All")) {
				System.out.println("All is selected by default");
			}

			List<WebElement> allOptions = select.getOptions();
			System.out.println(allOptions);
			String DefaultTags = dataSet.get("DefaultTags");
			boolean valid = ValidateDeactivationDefaultTag(DefaultTags, driver);
			if (valid) {
				fc.utobj().throwsException("Default tags get active/deactive link is enable please check");
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	public boolean verifyElementAbsent(String locator, WebDriver driver) throws Exception {
		try {

			WebElement actionbtn = fc.utobj().getElementByXpath(driver,".//a[text()='" + locator + "']/ancestor::tr[@qat_maintable='withoutheader']/td[4]//layer");
			fc.utobj().clickElement(driver, actionbtn);
			return false;

		} catch (NoSuchElementException e) {
			fc.utobj().throwsException("Element absent");

		}
		return true;
	}

	private boolean ValidateDeactivationDefaultTag(String defaultTags, WebDriver driver) throws Exception {
		String[] str = defaultTags.split(",");
		for (String obj : str) {
			String xpath = ".//a[text()='" + obj + "']/ancestor::tr[@qat_maintable='withoutheader']/td[3]/a";
			try {
				fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, xpath));
				return true;
			} catch (Exception ex) {
				System.out.println("Hello");
			}
		}
		return false;
	}

	@Test(groups = { "salesFormGenerate" })
	@TestCase(createdOn = "2017-07-04", updatedOn = "2017-07-05", testCaseId = "TC_Sales_Validate_Admin_TabsDropDown_MFG", testCaseDescription = "Verify in Manage Form Generator > Admin Link is Clickable")
	private void VerifyAllTabInDropDown() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("sales", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		try {
			driver = fc.loginpage().login(driver);
			String tabName = dataSet.get("tabDisplayName");
			fc.utobj().printTestStep("Go to Admin > Sales > Manage Form Generator");

			AdminSales adsales = new AdminSales();
			adsales.manageFormGenerator(driver);
			fc.utobj().printTestStep("Add a Tab in Manage Form Generator ");
			AdminFranchiseSalesManageFormGeneratorPageTest p1 = new AdminFranchiseSalesManageFormGeneratorPageTest();
			tabName = p1.addTabName(driver, tabName);

			String DefaultTags = dataSet.get("DefaultTags");

			String[] str = null;
			str = DefaultTags.split(",");
			List<String> listItem = new ArrayList<String>();
			List<String> SelectList = new ArrayList<String>();
			Collections.addAll(listItem, str);
			listItem.add(tabName);
			WebElement selectElement = fc.utobj().getElementByXpath(driver, ".//*[@id='formNames']");
			Select select = new Select(selectElement);
			List<WebElement> allOptions = select.getOptions();
			for (WebElement elt : allOptions) {
				System.out.println(elt.getText());
				SelectList.add(elt.getText());
			}
			if (!SelectList.containsAll(listItem))
				fc.utobj().throwsException("All tags are not visible Kindly check in Manage Form Generator");

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception ex) {
			fc.utobj().quitBrowserOnCatch(driver, ex, testCaseId);
		}
	}

	@Test(groups = { "salesFormGenerate" })
	@TestCase(createdOn = "2017-07-04", updatedOn = "2017-07-05", testCaseId = "TC_Sales_Validate_Admin_TabsIn_MFG", testCaseDescription = "Verify in Manage Form Generator > Admin Link is Clickable")
	private void VerifyAllTab() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("sales", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		try {
			driver = fc.loginpage().login(driver);
			String tabName = dataSet.get("tabDisplayName");
			fc.utobj().printTestStep("Go to Admin > Sales > Manage Form Generator");

			AdminSales adsales = new AdminSales();
			adsales.manageFormGenerator(driver);
			fc.utobj().printTestStep("Add a Tab in Manage Form Generator ");
			AdminFranchiseSalesManageFormGeneratorPageTest p1 = new AdminFranchiseSalesManageFormGeneratorPageTest();
			tabName = p1.addTabName(driver, tabName);

			String DefaultTags = dataSet.get("DefaultTags");

			String[] str = null;
			str = DefaultTags.split(",");
			List<String> listItem = new ArrayList<String>();
			Collections.addAll(listItem, str);
			listItem.add(tabName);

			boolean visible = fc.utobj().assertPageSourceWithMultipleRecords(driver, listItem);
			if (!visible)
				fc.utobj().throwsException("All tags are not visible Kindly check in Manage Form Generator");

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception ex) {
			fc.utobj().quitBrowserOnCatch(driver, ex, testCaseId);
		}
	}

}
