package com.builds.test.fs;

import java.util.Map;
import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.Test;

import com.builds.uimaps.fs.AdminFranchiseSalesManageWebFormGeneratorPage;
import com.builds.utilities.FranconnectUtil;
import com.builds.utilities.TestCase;

public class AdminSalesManageWebFormGeneratorPageTest {
	FranconnectUtil fc = new FranconnectUtil();

	/* Harish Dwivedi */

	public String addLeadFromWebForm(WebDriver driver, String fieldName) throws Exception {

		fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//input[@value='Create New Form']"));
		String formName = "wb" + fc.utobj().generateRandomNumber();
		fc.utobj().sendKeys(driver, fc.utobj().getElementByID(driver, "formName"), formName);
		fc.utobj().sendKeys(driver, fc.utobj().getElementByID(driver, "formDisplayTitle"), "WebFrom Title");
		fc.utobj().sendKeys(driver, fc.utobj().getElementByID(driver, "formUrl"), formName);
		fc.utobj().clickElement(driver, fc.utobj().getElementByID(driver, "detailsNextBtn"));
		fc.utobj().clickElement(driver, fc.utobj().getElementByID(driver, "designNextBtn"));
		fc.utobj().clickElement(driver, fc.utobj().getElementByID(driver, "settingsNextBtn"));
		fc.utobj().clickElement(driver, fc.utobj().getElementByID(driver, "hostURL"));
		String newFormUrl = fc.utobj().getText(driver, fc.utobj().getElementByID(driver, "hostCodeBox"));
		fc.utobj().clickElement(driver, fc.utobj().getElementByID(driver, "confirmationNextBtn"));

		return formName;

	}

	public String createNewFormContactType(WebDriver driver, String formName, String formTitle, String description,
			String formFormat, String submissionType, String formUrl, String sectionName, String tabName, String header,
			String textLabel, String fieldLabel, String footer, String afterSubmission, String redirectedUrl,
			Map<String, String> config) throws Exception {

		String testCaseId = "TC_Create_New_Form_Contact";
		String urlText = null;

		if (fc.utobj().validate(testCaseId)) {
			try {
				AdminFranchiseSalesManageWebFormGeneratorPage pobj = new AdminFranchiseSalesManageWebFormGeneratorPage(
						driver);

				AdminSales adsales = new AdminSales();
				adsales.manageWebFormGenerator(driver);
				fc.utobj().clickElement(driver, pobj.createNewForm);
				fc.utobj().sendKeys(driver, pobj.formName, formName);
				fc.utobj().sendKeys(driver, pobj.formDisplayTitle, formTitle);
				if (!pobj.displayFormTitleCheckBox.isSelected()) {
					fc.utobj().clickElement(driver, pobj.displayFormTitleCheckBox);
				}
				fc.utobj().sendKeys(driver, pobj.formDescription, description);

				if (formFormat.equalsIgnoreCase("Single Page")) {
					fc.utobj().selectDropDown(driver, pobj.formFormat, "Single Page");

				} else if (formFormat.equalsIgnoreCase("Multi Page")) {
					fc.utobj().selectDropDown(driver, pobj.formFormat, "Multi Page");
					fc.utobj().selectDropDown(driver, pobj.submissionType, submissionType);
				}
				fc.utobj().selectDropDown(driver, pobj.columnCount, "1");
				fc.utobj().selectDropDown(driver, pobj.filedLabelAlignment, "Left");
				// fc.utobj().selectDropDown(driver, pobj.formLanguage,
				// "English");
				fc.utobj().sendKeys(driver, pobj.iframeWidth, "100%");
				fc.utobj().sendKeys(driver, pobj.iframeHeight, "500");
				fc.utobj().sendKeys(driver, pobj.formUrl, formUrl);
				fc.utobj().clickElement(driver, pobj.saveNextBtn);

				// 1.Details Page

				if (formFormat.equalsIgnoreCase("Single Page")) {
					// Add Section
					fc.utobj().clickElement(driver, pobj.addSection);
					fc.commonMethods().switch_cboxIframe_frameId(driver);
					fc.utobj().sendKeys(driver, pobj.sectionName, sectionName);
					// fc.utobj().selectDropDown(driver, pobj.noOfCols, "1");
					// fc.utobj().selectDropDown(driver,
					// pobj.fieldLabelAlignmentSection, "Left");
					fc.utobj().clickElement(driver, pobj.addBtn);
					fc.utobj().switchFrameToDefault(driver);
				} else if (formFormat.equalsIgnoreCase("Multi Page")) {
					fc.utobj().clickElement(driver, pobj.addTab);
					fc.commonMethods().switch_cboxIframe_frameId(driver);
					fc.utobj().sendKeys(driver, pobj.tabName, tabName);
					fc.utobj().clickElement(driver, pobj.addBtn);
					fc.utobj().switchFrameToDefault(driver);
				}
				// add Header
				fc.utobj().doubleClickElement(driver, pobj.addHeader);
				fc.commonMethods().switch_cboxIframe_frameId(driver);
				fc.utobj().switchFrameById(driver, "textBlock_ifr");
				WebElement editorTextArea = fc.utobj().getElementByXpath(driver, ".//*[@id='tinymce']/p");
				Actions actions = new Actions(driver);
				actions.moveToElement(editorTextArea);
				actions.click();
				actions.sendKeys(header);
				fc.utobj().logReport("Entering Value in Text Field", editorTextArea);
				fc.utobj().logReportMsg("Entered Text", header);
				actions.build().perform();
				fc.utobj().switchFrameToDefault(driver);
				fc.commonMethods().switch_cboxIframe_frameId(driver);
				fc.utobj().clickElement(driver, pobj.saveBtn);
				fc.utobj().switchFrameToDefault(driver);

				if (formFormat.equalsIgnoreCase("Multi Page")) {
					fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
							".//*[contains(@id , 'defaultTab') and contains(@id , 'Tab') and contains(text () ,'Default Tab')]"));
				}
				// Add Text
				fc.utobj().doubleClickElement(driver, pobj.defaultSectionHeaderAddText);
				fc.commonMethods().switch_cboxIframe_frameId(driver);
				fc.utobj().sendKeys(driver, pobj.labelAddText, textLabel);
				fc.utobj().selectDropDown(driver, pobj.headerLabelAlignment, "Left");
				fc.utobj().clickElement(driver, pobj.saveBtn);
				fc.utobj().switchFrameToDefault(driver);
				// FormTool
				fc.utobj().clickElement(driver, pobj.availableFieldsTab);
				fc.utobj().clickElement(driver, pobj.formTools);

				fc.utobj().dragAndDropElement(driver, pobj.formToolText, pobj.defaultDropHere);

				// fc.utobj().dragAndDropElement(driver, pobj.formToolCaptcha,
				// pobj.defaultDropHere);
				//
				fc.utobj().dragAndDropElement(driver, pobj.formToolIAgreeCheckBox, pobj.defaultDropHere);

				// Contact Info
				fc.utobj().clickElement(driver, pobj.primaryInfo);
				/*
				 * fc.utobj().sendKeys(driver, pobj.searchField, "Title");
				 * fc.utobj().dragAndDropElement(driver,
				 * fc.utobj().getElementByXpath(driver,".//a[.='Title']")),
				 * pobj.defaultDropHere);
				 */
				fc.utobj().sendKeys(driver, pobj.searchField, "Salutation");

				fc.utobj().dragAndDropElement(driver, fc.utobj().getElementByXpath(driver, ".//a[.='Salutation']"),
						pobj.defaultDropHere);

				fc.utobj().sendKeys(driver, pobj.searchField, "Address1");

				fc.utobj().dragAndDropElement(driver, fc.utobj().getElementByXpath(driver, ".//a[.='Address1']"),
						pobj.defaultDropHere);

				fc.utobj().sendKeys(driver, pobj.searchField, "Address2");

				fc.utobj().dragAndDropElement(driver, fc.utobj().getElementByXpath(driver, ".//a[.='Address2']"),
						pobj.defaultDropHere);

				fc.utobj().sendKeys(driver, pobj.searchField, "City");

				fc.utobj().dragAndDropElement(driver, fc.utobj().getElementByXpath(driver, ".//a[.='City']"),
						pobj.defaultDropHere);

				fc.utobj().sendKeys(driver, pobj.searchField, "Country");

				fc.utobj().dragAndDropElement(driver, fc.utobj().getElementByXpath(driver, ".//a[.='Country']"),
						pobj.defaultDropHere);

				fc.utobj().sendKeys(driver, pobj.searchField, "Zip / Postal Code");

				fc.utobj().dragAndDropElement(driver,
						fc.utobj().getElementByXpath(driver, ".//a[.='Zip / Postal Code']"), pobj.defaultDropHere);

				fc.utobj().sendKeys(driver, pobj.searchField, "County");

				fc.utobj().dragAndDropElement(driver, fc.utobj().getElementByXpath(driver, ".//a[.='County']"),
						pobj.defaultDropHere);

				fc.utobj().sendKeys(driver, pobj.searchField, "Best Time to Contact");

				fc.utobj().dragAndDropElement(driver,
						fc.utobj().getElementByXpath(driver, ".//*[@id='bestTimeToContact_fld']/a"),
						pobj.defaultDropHere);

				fc.utobj().sendKeys(driver, pobj.searchField, "Work Phone");

				fc.utobj().dragAndDropElement(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='phone_fld']/a"),
						pobj.defaultDropHere);

				fc.utobj().sendKeys(driver, pobj.searchField, "Work Phone Extension");

				fc.utobj().dragAndDropElement(driver,
						fc.utobj().getElementByXpath(driver, ".//a[.='Work Phone Extension']"), pobj.defaultDropHere);

				fc.utobj().sendKeys(driver, pobj.searchField, "Home Phone");

				fc.utobj().dragAndDropElement(driver, fc.utobj().getElementByXpath(driver, ".//a[.='Home Phone']"),
						pobj.defaultDropHere);

				fc.utobj().sendKeys(driver, pobj.searchField, "Home Phone Extension");

				fc.utobj().dragAndDropElement(driver,
						fc.utobj().getElementByXpath(driver, ".//a[.='Home Phone Extension']"), pobj.defaultDropHere);

				fc.utobj().sendKeys(driver, pobj.searchField, "Fax");

				fc.utobj().dragAndDropElement(driver, fc.utobj().getElementByXpath(driver, ".//a[.='Fax']"),
						pobj.defaultDropHere);

				fc.utobj().sendKeys(driver, pobj.searchField, "Mobile");

				fc.utobj().dragAndDropElement(driver, fc.utobj().getElementByXpath(driver, ".//a[.='Mobile']"),
						pobj.defaultDropHere);

				fc.utobj().sendKeys(driver, pobj.searchField, "Company Name");

				fc.utobj().dragAndDropElement(driver, fc.utobj().getElementByXpath(driver, ".//a[.='Company Name']"),
						pobj.defaultDropHere);

				fc.utobj().sendKeys(driver, pobj.searchField, "Comments");

				fc.utobj().dragAndDropElement(driver, fc.utobj().getElementByXpath(driver, ".//a[.='Comments']"),
						pobj.defaultDropHere);

				fc.utobj().sendKeys(driver, pobj.searchField, "Lead Source Category");

				fc.utobj().dragAndDropElement(driver,
						fc.utobj().getElementByXpath(driver, ".//a[.='Lead Source Category']"), pobj.defaultDropHere);

				fc.utobj().sendKeys(driver, pobj.searchField, "Lead Source Details");

				fc.utobj().dragAndDropElement(driver,
						fc.utobj().getElementByXpath(driver, ".//a[.='Lead Source Details']"), pobj.defaultDropHere);

				fc.utobj().sendKeys(driver, pobj.searchField, "Cash Available for Investment");

				fc.utobj().dragAndDropElement(driver,
						fc.utobj().getElementByXpath(driver, ".//a[.='Cash Available for Investment']"),
						pobj.defaultDropHere);

				fc.utobj().sendKeys(driver, pobj.searchField, "Investment Timeframe");

				fc.utobj().dragAndDropElement(driver,
						fc.utobj().getElementByXpath(driver, ".//a[.='Investment Timeframe']"), pobj.defaultDropHere);

				fc.utobj().sendKeys(driver, pobj.searchField, "Background");

				fc.utobj().dragAndDropElement(driver, fc.utobj().getElementByXpath(driver, ".//a[.='Background']"),
						pobj.defaultDropHere);

				fc.utobj().sendKeys(driver, pobj.searchField, "Source Of Investment");

				fc.utobj().dragAndDropElement(driver,
						fc.utobj().getElementByXpath(driver, ".//a[.='Source Of Investment']"), pobj.defaultDropHere);

				fc.utobj().sendKeys(driver, pobj.searchField, "# of Units / Locations Requested");

				fc.utobj().dragAndDropElement(driver,
						fc.utobj().getElementByXpath(driver, ".//a[.='# of Units / Locations Requested']"),
						pobj.defaultDropHere);

				fc.utobj().sendKeys(driver, pobj.searchField, "Preferred City 1");

				fc.utobj().dragAndDropElement(driver,
						fc.utobj().getElementByXpath(driver, ".//a[.='Preferred City 1']"), pobj.defaultDropHere);

				fc.utobj().sendKeys(driver, pobj.searchField, "Preferred Country 1");

				fc.utobj().dragAndDropElement(driver,
						fc.utobj().getElementByXpath(driver, ".//a[.='Preferred Country 1']"), pobj.defaultDropHere);

				fc.utobj().sendKeys(driver, pobj.searchField, "Preferred City 2");

				fc.utobj().dragAndDropElement(driver,
						fc.utobj().getElementByXpath(driver, ".//a[.='Preferred City 2']"), pobj.defaultDropHere);

				fc.utobj().sendKeys(driver, pobj.searchField, "Preferred Country 2");

				fc.utobj().dragAndDropElement(driver,
						fc.utobj().getElementByXpath(driver, ".//a[.='Preferred Country 2']"), pobj.defaultDropHere);

				fc.utobj().sendKeys(driver, pobj.searchField, "Preferred State / Province 2");

				fc.utobj().dragAndDropElement(driver,
						fc.utobj().getElementByXpath(driver, ".//a[.='Preferred State / Province 2']"),
						pobj.defaultDropHere);

				fc.utobj().sendKeys(driver, pobj.searchField, "Sms Campaign");

				fc.utobj().dragAndDropElement(driver, fc.utobj().getElementByXpath(driver, ".//a[.='Sms Campaign']"),
						pobj.defaultDropHere);

				fc.utobj().clickElement(driver, pobj.qualificationDetails);

				fc.utobj().sendKeys(driver, pobj.searchFieldQualification, "Date Submission");

				fc.utobj()
						.dragAndDropElement(driver,
								fc.utobj().getElementByXpath(driver,
										".//*[@id='fsLeadQualificationDetail_0dateSubmission_fld']/a"),
								pobj.defaultDropHere);

				fc.utobj().sendKeys(driver, pobj.searchFieldQualification, "Date");

				fc.utobj().dragAndDropElement(driver,
						fc.utobj().getElementByXpath(driver, ".//*[@id='fsLeadQualificationDetail_0date_fld']/a"),
						pobj.defaultDropHere);

				fc.utobj().sendKeys(driver, pobj.searchFieldQualification, "Gender");

				fc.utobj().dragAndDropElement(driver,
						fc.utobj().getElementByXpath(driver, ".//*[@id='fsLeadQualificationDetail_0gender_fld']/a"),
						pobj.defaultDropHere);

				fc.utobj().sendKeys(driver, pobj.searchFieldQualification, "Present Address");

				fc.utobj()
						.dragAndDropElement(driver,
								fc.utobj().getElementByXpath(driver,
										".//*[@id='fsLeadQualificationDetail_0presentAddress_fld']/a"),
								pobj.defaultDropHere);

				fc.utobj().sendKeys(driver, pobj.searchFieldQualification, "How many years at this address?");

				fc.utobj().dragAndDropElement(driver,
						fc.utobj().getElementByXpath(driver, ".//*[@id='fsLeadQualificationDetail_0howLong_fld']/a"),
						pobj.defaultDropHere);

				fc.utobj().sendKeys(driver, pobj.searchFieldQualification, "City");

				fc.utobj().dragAndDropElement(driver,
						fc.utobj().getElementByXpath(driver, ".//*[@id='fsLeadQualificationDetail_0city_fld']/a"),
						pobj.defaultDropHere);

				fc.utobj().sendKeys(driver, pobj.searchFieldQualification, "Country");

				fc.utobj().dragAndDropElement(driver,
						fc.utobj().getElementByXpath(driver, ".//*[@id='fsLeadQualificationDetail_0country_fld']/a"),
						pobj.defaultDropHere);

				fc.utobj().sendKeys(driver, pobj.searchFieldQualification, "State / Province");

				fc.utobj().dragAndDropElement(driver,
						fc.utobj().getElementByXpath(driver, ".//*[@id='fsLeadQualificationDetail_0stateID_fld']/a"),
						pobj.defaultDropHere);

				fc.utobj().sendKeys(driver, pobj.searchFieldQualification, "Zip / Postal Code");

				fc.utobj().dragAndDropElement(driver,
						fc.utobj().getElementByXpath(driver, ".//*[@id='fsLeadQualificationDetail_0zipCode_fld']/a"),
						pobj.defaultDropHere);

				fc.utobj().sendKeys(driver, pobj.searchFieldQualification, "Work Phone");

				fc.utobj().dragAndDropElement(driver,
						fc.utobj().getElementByXpath(driver, ".//*[@id='fsLeadQualificationDetail_0workPhone_fld']/a"),
						pobj.defaultDropHere);

				fc.utobj().sendKeys(driver, pobj.searchFieldQualification, "Work Phone Extension");

				fc.utobj().dragAndDropElement(driver,
						fc.utobj().getElementByXpath(driver, ".//*[@id='fsLeadQualificationDetail_0phoneExt_fld']/a"),
						pobj.defaultDropHere);

				fc.utobj().sendKeys(driver, pobj.searchFieldQualification, "Home Phone");

				fc.utobj().dragAndDropElement(driver,
						fc.utobj().getElementByXpath(driver, ".//*[@id='fsLeadQualificationDetail_0homePhone_fld']/a"),
						pobj.defaultDropHere);

				fc.utobj().sendKeys(driver, pobj.searchFieldQualification, "Email");

				fc.utobj()
						.dragAndDropElement(driver,
								fc.utobj().getElementByXpath(driver,
										".//*[@id='fsLeadQualificationDetail_0homePhoneExt_fld']/a"),
								pobj.defaultDropHere);

				fc.utobj().dragAndDropElement(driver,
						fc.utobj().getElementByXpath(driver, ".//*[@id='fsLeadQualificationDetail_0emailID_fld']/a"),
						pobj.defaultDropHere);
				fc.utobj().sendKeys(driver, pobj.searchFieldQualification, "US Citizen");

				fc.utobj().dragAndDropElement(driver,
						fc.utobj().getElementByXpath(driver, ".//*[@id='fsLeadQualificationDetail_0usCitizen_fld']/a"),
						pobj.defaultDropHere);

				fc.utobj().sendKeys(driver, pobj.searchFieldQualification, "Social Security #");

				fc.utobj().dragAndDropElement(driver,
						fc.utobj().getElementByXpath(driver, ".//*[@id='fsLeadQualificationDetail_0ssn_fld']/a"),
						pobj.defaultDropHere);

				fc.utobj().sendKeys(driver, pobj.searchFieldQualification, "Previous Address");

				fc.utobj()
						.dragAndDropElement(driver,
								fc.utobj().getElementByXpath(driver,
										".//*[@id='fsLeadQualificationDetail_0previousAddress_fld']/a"),
								pobj.defaultDropHere);

				fc.utobj().sendKeys(driver, pobj.searchFieldQualification, "Birth Date");

				fc.utobj().dragAndDropElement(driver, fc.utobj().getElementByXpath(driver, ".//a[.='Birth Date']"),
						pobj.defaultDropHere);

				fc.utobj().sendKeys(driver, pobj.searchFieldQualification, "Home Ownership");

				fc.utobj().dragAndDropElement(driver, fc.utobj().getElementByXpath(driver, ".//a[.='Home Ownership']"),
						pobj.defaultDropHere);

				fc.utobj().sendKeys(driver, pobj.searchFieldQualification, "Marital Status");

				fc.utobj().dragAndDropElement(driver, fc.utobj().getElementByXpath(driver, ".//a[.='Marital Status']"),
						pobj.defaultDropHere);

				fc.utobj().sendKeys(driver, pobj.searchFieldQualification, "Spouse Name");

				fc.utobj().dragAndDropElement(driver, fc.utobj().getElementByXpath(driver, ".//a[.='Spouse Name']"),
						pobj.defaultDropHere);

				fc.utobj().sendKeys(driver, pobj.searchFieldQualification, "Spouse Social Security #");

				fc.utobj().dragAndDropElement(driver,
						fc.utobj().getElementByXpath(driver, ".//a[.='Spouse Social Security #']"),
						pobj.defaultDropHere);

				fc.utobj().sendKeys(driver, pobj.searchFieldQualification, "Spouse US Citizen");

				fc.utobj().dragAndDropElement(driver,
						fc.utobj().getElementByXpath(driver, ".//a[.='Spouse US Citizen']"), pobj.defaultDropHere);

				fc.utobj().sendKeys(driver, pobj.searchFieldQualification, "Spouse Birth Date");

				fc.utobj().dragAndDropElement(driver,
						fc.utobj().getElementByXpath(driver, ".//a[.='Spouse Birth Date']"), pobj.defaultDropHere);

				fc.utobj().sendKeys(driver, pobj.searchFieldQualification, "Cash on Hand & in Banks");

				fc.utobj().dragAndDropElement(driver,
						fc.utobj().getElementByXpath(driver, ".//a[.='Cash on Hand & in Banks']"),
						pobj.defaultDropHere);

				fc.utobj().sendKeys(driver, pobj.searchFieldQualification, "Mortgages");

				fc.utobj().dragAndDropElement(driver, fc.utobj().getElementByXpath(driver, ".//a[.='Mortgages']"),
						pobj.defaultDropHere);

				fc.utobj().sendKeys(driver, pobj.searchFieldQualification, "Marketable Securities");

				fc.utobj().dragAndDropElement(driver,
						fc.utobj().getElementByXpath(driver, ".//a[.='Marketable Securities']"), pobj.defaultDropHere);

				fc.utobj().sendKeys(driver, pobj.searchFieldQualification, "Accounts Payable");

				fc.utobj().dragAndDropElement(driver,
						fc.utobj().getElementByXpath(driver, ".//a[.='Accounts Payable']"), pobj.defaultDropHere);

				fc.utobj().sendKeys(driver, pobj.searchFieldQualification, "Accounts / Notes Receivable");

				fc.utobj().dragAndDropElement(driver,
						fc.utobj().getElementByXpath(driver, ".//a[.='Accounts / Notes Receivable']"),
						pobj.defaultDropHere);

				fc.utobj().sendKeys(driver, pobj.searchFieldQualification, "Notes Payable");

				fc.utobj().dragAndDropElement(driver, fc.utobj().getElementByXpath(driver, ".//a[.='Notes Payable']"),
						pobj.defaultDropHere);

				fc.utobj().sendKeys(driver, pobj.searchFieldQualification, "Retirement Plans");

				fc.utobj().dragAndDropElement(driver,
						fc.utobj().getElementByXpath(driver, ".//a[.='Retirement Plans']"), pobj.defaultDropHere);

				fc.utobj().sendKeys(driver, pobj.searchFieldQualification, "Loans On Life Insurance");

				fc.utobj().dragAndDropElement(driver,
						fc.utobj().getElementByXpath(driver, ".//a[.='Loans On Life Insurance']"),
						pobj.defaultDropHere);

				fc.utobj().sendKeys(driver, pobj.searchFieldQualification, "Real Estate");

				fc.utobj().dragAndDropElement(driver, fc.utobj().getElementByXpath(driver, ".//a[.='Real Estate']"),
						pobj.defaultDropHere);

				fc.utobj().sendKeys(driver, pobj.searchFieldQualification, "Credit Cards (total balance)");

				fc.utobj().dragAndDropElement(driver,
						fc.utobj().getElementByXpath(driver, ".//a[.='Credit Cards (total balance)']"),
						pobj.defaultDropHere);

				fc.utobj().sendKeys(driver, pobj.searchFieldQualification, "Personal Property");

				fc.utobj().dragAndDropElement(driver,
						fc.utobj().getElementByXpath(driver, ".//a[.='Personal Property']"), pobj.defaultDropHere);

				fc.utobj().sendKeys(driver, pobj.searchFieldQualification, "Unpaid Taxes");

				fc.utobj().dragAndDropElement(driver, fc.utobj().getElementByXpath(driver, ".//a[.='Unpaid Taxes']"),
						pobj.defaultDropHere);

				fc.utobj().sendKeys(driver, pobj.searchFieldQualification, "Business Holdings");

				fc.utobj().dragAndDropElement(driver,
						fc.utobj().getElementByXpath(driver, ".//a[.='Business Holdings']"), pobj.defaultDropHere);

				fc.utobj().sendKeys(driver, pobj.searchFieldQualification, "Life Insurance($)");

				fc.utobj().dragAndDropElement(driver,
						fc.utobj().getElementByXpath(driver, ".//a[.='Life Insurance($)']"), pobj.defaultDropHere);

				fc.utobj().sendKeys(driver, pobj.searchFieldQualification, "Other Assets($)");

				fc.utobj().dragAndDropElement(driver, fc.utobj().getElementByXpath(driver, ".//a[.='Other Assets($)']"),
						pobj.defaultDropHere);

				fc.utobj().sendKeys(driver, pobj.searchFieldQualification, "Other Liabilities($)");

				fc.utobj().dragAndDropElement(driver,
						fc.utobj().getElementByXpath(driver, ".//a[.='Other Liabilities($)']"), pobj.defaultDropHere);

				fc.utobj().sendKeys(driver, pobj.searchFieldQualification, "Description");

				fc.utobj().dragAndDropElement(driver, fc.utobj().getElementByXpath(driver, ".//a[.='Description']"),
						pobj.defaultDropHere);

				fc.utobj().sendKeys(driver, pobj.searchFieldQualification, "Total Assets");

				fc.utobj().dragAndDropElement(driver, fc.utobj().getElementByXpath(driver, ".//a[.='Total Assets']"),
						pobj.defaultDropHere);

				fc.utobj().sendKeys(driver, pobj.searchFieldQualification, "Total Liabilities");

				fc.utobj().dragAndDropElement(driver,
						fc.utobj().getElementByXpath(driver, ".//a[.='Total Liabilities']"), pobj.defaultDropHere);

				fc.utobj().sendKeys(driver, pobj.searchFieldQualification, "Total Net Worth");

				fc.utobj().dragAndDropElement(driver, fc.utobj().getElementByXpath(driver, ".//a[.='Total Net Worth']"),
						pobj.defaultDropHere);

				fc.utobj().sendKeys(driver, pobj.searchFieldQualification, "Real Estate Address 1");

				fc.utobj().dragAndDropElement(driver,
						fc.utobj().getElementByXpath(driver, ".//a[.='Real Estate Address 1']"), pobj.defaultDropHere);

				fc.utobj().sendKeys(driver, pobj.searchFieldQualification, "Real Estate Date Purchased 1");

				fc.utobj().dragAndDropElement(driver,
						fc.utobj().getElementByXpath(driver, ".//a[.='Real Estate Date Purchased 1']"),
						pobj.defaultDropHere);

				fc.utobj().sendKeys(driver, pobj.searchFieldQualification, "Real Estate Original Cost 1");

				fc.utobj().dragAndDropElement(driver,
						fc.utobj().getElementByXpath(driver, ".//a[.='Real Estate Original Cost 1']"),
						pobj.defaultDropHere);

				fc.utobj().sendKeys(driver, pobj.searchFieldQualification, "Real Estate Present Value 1");

				fc.utobj().dragAndDropElement(driver,
						fc.utobj().getElementByXpath(driver, ".//a[.='Real Estate Present Value 1']"),
						pobj.defaultDropHere);

				fc.utobj().sendKeys(driver, pobj.searchFieldQualification, "Real Estate Mortgage Balance 1");

				fc.utobj().dragAndDropElement(driver,
						fc.utobj().getElementByXpath(driver, ".//a[.='Real Estate Mortgage Balance 1']"),
						pobj.defaultDropHere);

				fc.utobj().sendKeys(driver, pobj.searchFieldQualification, "Real Estate Address 2");

				fc.utobj().dragAndDropElement(driver,
						fc.utobj().getElementByXpath(driver, ".//a[.='Real Estate Address 2']"), pobj.defaultDropHere);

				fc.utobj().sendKeys(driver, pobj.searchFieldQualification, "Real Estate Date Purchased 2");

				fc.utobj().dragAndDropElement(driver,
						fc.utobj().getElementByXpath(driver, ".//a[.='Real Estate Date Purchased 2']"),
						pobj.defaultDropHere);

				fc.utobj().sendKeys(driver, pobj.searchFieldQualification, "Real Estate Original Cost 2");

				fc.utobj().dragAndDropElement(driver,
						fc.utobj().getElementByXpath(driver, ".//a[.='Real Estate Original Cost 2']"),
						pobj.defaultDropHere);

				fc.utobj().sendKeys(driver, pobj.searchFieldQualification, "Real Estate Present Value 2");

				fc.utobj().dragAndDropElement(driver,
						fc.utobj().getElementByXpath(driver, ".//a[.='Real Estate Present Value 2']"),
						pobj.defaultDropHere);

				fc.utobj().sendKeys(driver, pobj.searchFieldQualification, "Real Estate Mortgage Balance 2");

				fc.utobj().dragAndDropElement(driver,
						fc.utobj().getElementByXpath(driver, ".//a[.='Real Estate Mortgage Balance 2']"),
						pobj.defaultDropHere);

				fc.utobj().sendKeys(driver, pobj.searchFieldQualification, "Real Estate Address 3");

				fc.utobj().dragAndDropElement(driver,
						fc.utobj().getElementByXpath(driver, ".//a[.='Real Estate Address 3']"), pobj.defaultDropHere);

				fc.utobj().sendKeys(driver, pobj.searchFieldQualification, "Real Estate Date Purchased 3");

				fc.utobj().dragAndDropElement(driver,
						fc.utobj().getElementByXpath(driver, ".//a[.='Real Estate Date Purchased 3']"),
						pobj.defaultDropHere);

				fc.utobj().sendKeys(driver, pobj.searchFieldQualification, "Real Estate Original Cost 3");

				fc.utobj().dragAndDropElement(driver,
						fc.utobj().getElementByXpath(driver, ".//a[.='Real Estate Original Cost 3']"),
						pobj.defaultDropHere);

				fc.utobj().sendKeys(driver, pobj.searchFieldQualification, "Real Estate Present Value 3");

				fc.utobj().dragAndDropElement(driver,
						fc.utobj().getElementByXpath(driver, ".//a[.='Real Estate Present Value 3']"),
						pobj.defaultDropHere);

				fc.utobj().sendKeys(driver, pobj.searchFieldQualification, "Real Estate Mortgage Balance 3");

				fc.utobj().dragAndDropElement(driver,
						fc.utobj().getElementByXpath(driver, ".//a[.='Real Estate Mortgage Balance 3']"),
						pobj.defaultDropHere);

				fc.utobj().sendKeys(driver, pobj.searchFieldQualification, "Salary");

				fc.utobj().dragAndDropElement(driver, fc.utobj().getElementByXpath(driver, ".//a[.='Salary']"),
						pobj.defaultDropHere);

				fc.utobj().sendKeys(driver, pobj.searchFieldQualification, "Investment");

				fc.utobj().dragAndDropElement(driver, fc.utobj().getElementByXpath(driver, ".//a[.='Investment']"),
						pobj.defaultDropHere);

				fc.utobj().sendKeys(driver, pobj.searchFieldQualification, "Real Estate Income");

				fc.utobj().dragAndDropElement(driver,
						fc.utobj().getElementByXpath(driver, ".//a[.='Real Estate Income']"), pobj.defaultDropHere);

				fc.utobj().sendKeys(driver, pobj.searchFieldQualification, "Other");

				fc.utobj().dragAndDropElement(driver, fc.utobj().getElementByXpath(driver, ".//a[.='Other']"),
						pobj.defaultDropHere);

				fc.utobj().sendKeys(driver, pobj.searchFieldQualification, "Total");

				fc.utobj().dragAndDropElement(driver, fc.utobj().getElementByXpath(driver, ".//a[.='Total']"),
						pobj.defaultDropHere);

				fc.utobj().sendKeys(driver, pobj.searchFieldQualification, "Loan Co-signature($)");

				fc.utobj().dragAndDropElement(driver,
						fc.utobj().getElementByXpath(driver, ".//a[.='Loan Co-signature($)']"), pobj.defaultDropHere);

				fc.utobj().sendKeys(driver, pobj.searchFieldQualification, "Legal Judgement($)");

				fc.utobj().dragAndDropElement(driver,
						fc.utobj().getElementByXpath(driver, ".//a[.='Legal Judgement($)']"), pobj.defaultDropHere);

				fc.utobj().sendKeys(driver, pobj.searchFieldQualification, "Income Taxes($)");

				fc.utobj().dragAndDropElement(driver, fc.utobj().getElementByXpath(driver, ".//a[.='Income Taxes($)']"),
						pobj.defaultDropHere);

				fc.utobj().sendKeys(driver, pobj.searchFieldQualification, "Other Special Debt($)");

				fc.utobj().dragAndDropElement(driver,
						fc.utobj().getElementByXpath(driver, ".//a[.='Other Special Debt($)']"), pobj.defaultDropHere);

				fc.utobj().sendKeys(driver, pobj.searchFieldQualification, "Total($)");

				fc.utobj().dragAndDropElement(driver, fc.utobj().getElementByXpath(driver, ".//a[.='Total($)']"),
						pobj.defaultDropHere);

				fc.utobj().sendKeys(driver, pobj.searchFieldQualification,
						"When would you be ready to invest in your franchise if you were approved ?");

				fc.utobj().dragAndDropElement(driver,
						fc.utobj().getElementByXpath(driver,
								".//a[.='When would you be ready to invest in your franchise if you were approved ?']"),
						pobj.defaultDropHere);

				fc.utobj().sendKeys(driver, pobj.searchFieldQualification,
						"What skills/experience do you have that will help you be successful in this business ?");

				fc.utobj().dragAndDropElement(driver,
						fc.utobj().getElementByXpath(driver,
								".//a[.='What skills/experience do you have that will help you be successful in this business ?']"),
						pobj.defaultDropHere);

				fc.utobj().sendKeys(driver, pobj.searchFieldQualification,
						"Why do you think this franchise will enable you to reach your personal goals?");

				fc.utobj().dragAndDropElement(driver,
						fc.utobj().getElementByXpath(driver,
								".//a[.='Why do you think this franchise will enable you to reach your personal goals?']"),
						pobj.defaultDropHere);

				fc.utobj().sendKeys(driver, pobj.searchFieldQualification,
						"Who will be responsible for the daily operation of your store?");

				fc.utobj().dragAndDropElement(driver,
						fc.utobj().getElementByXpath(driver,
								".//a[.='Who will be responsible for the daily operation of your store?']"),
						pobj.defaultDropHere);

				fc.utobj().sendKeys(driver, pobj.searchFieldQualification, "Amount of cash available for investment?");

				fc.utobj().dragAndDropElement(driver,
						fc.utobj().getElementByXpath(driver, ".//a[.='Amount of cash available for investment?']"),
						pobj.defaultDropHere);

				fc.utobj().sendKeys(driver, pobj.searchFieldQualification, "Have you been approved for financing?");

				fc.utobj().dragAndDropElement(driver,
						fc.utobj().getElementByXpath(driver, ".//a[.='Have you been approved for financing?']"),
						pobj.defaultDropHere);

				fc.utobj().sendKeys(driver, pobj.searchFieldQualification, "Amount Approved");

				fc.utobj().dragAndDropElement(driver, fc.utobj().getElementByXpath(driver, ".//a[.='Amount Approved']"),
						pobj.defaultDropHere);

				fc.utobj().sendKeys(driver, pobj.searchFieldQualification,
						"Would this business be your sole income source?");

				fc.utobj()
						.dragAndDropElement(driver,
								fc.utobj().getElementByXpath(driver,
										".//a[.='Would this business be your sole income source?']"),
								pobj.defaultDropHere);

				fc.utobj().sendKeys(driver, pobj.searchFieldQualification,
						"Do you have any contingent liabilities for guarantees, endorsements, leases etc ?");

				fc.utobj().dragAndDropElement(driver,
						fc.utobj().getElementByXpath(driver,
								".//a[.='Do you have any contingent liabilities for guarantees, endorsements, leases etc ?']"),
						pobj.defaultDropHere);

				fc.utobj().sendKeys(driver, pobj.searchFieldQualification,
						"Are you now, or have you ever been party to any lawsuit - either as defendant or plaintiff ?");

				fc.utobj().dragAndDropElement(driver,
						fc.utobj().getElementByXpath(driver,
								".//a[.='Are you now, or have you ever been party to any lawsuit - either as defendant or plaintiff ?']"),
						pobj.defaultDropHere);

				fc.utobj().sendKeys(driver, pobj.searchFieldQualification,
						"Have you ever been convicted of any offense (including misdemeanors for which you have fined $ 200 or more) ?");

				fc.utobj().dragAndDropElement(driver,
						fc.utobj().getElementByXpath(driver,
								".//a[.='Have you ever been convicted of any offense (including misdemeanors for which you have fined $ 200 or more) ?']"),
						pobj.defaultDropHere);

				fc.utobj().sendKeys(driver, pobj.searchFieldQualification, "Have you ever been convicted of a felony?");

				fc.utobj().dragAndDropElement(driver,
						fc.utobj().getElementByXpath(driver, ".//a[.='Have you ever been convicted of a felony?']"),
						pobj.defaultDropHere);

				fc.utobj().sendKeys(driver, pobj.searchFieldQualification, "If so, explain");

				fc.utobj().dragAndDropElement(driver, fc.utobj().getElementByXpath(driver, ".//a[.='If so, explain']"),
						pobj.defaultDropHere);

				fc.utobj().clickElement(driver, pobj.personalProfile);

				fc.utobj().dragAndDropElement(driver,
						fc.utobj().getElementByXpath(driver, ".//*[@id='fsLeadPersonalProfile_0dateSubmission_fld']/a"),
						pobj.defaultDropHere);

				fc.utobj().dragAndDropElement(driver,
						fc.utobj().getElementByXpath(driver, ".//*[@id='fsLeadPersonalProfile_0gender_fld']/a"),
						pobj.defaultDropHere);

				fc.utobj().dragAndDropElement(driver,
						fc.utobj().getElementByXpath(driver, ".//*[@id='fsLeadPersonalProfile_0homeAddress_fld']/a"),
						pobj.defaultDropHere);

				fc.utobj()
						.dragAndDropElement(driver,
								fc.utobj().getElementByXpath(driver,
										".//*[@id='fsLeadPersonalProfile_0howLongAtAddress_fld']/a"),
								pobj.defaultDropHere);

				fc.utobj().dragAndDropElement(driver,
						fc.utobj().getElementByXpath(driver, ".//*[@id='fsLeadPersonalProfile_0homeCity_fld']/a"),
						pobj.defaultDropHere);

				fc.utobj().dragAndDropElement(driver,
						fc.utobj().getElementByXpath(driver, ".//*[@id='fsLeadPersonalProfile_0homeCountry_fld']/a"),
						pobj.defaultDropHere);

				fc.utobj().dragAndDropElement(driver,
						fc.utobj().getElementByXpath(driver, ".//*[@id='fsLeadPersonalProfile_0homeState_fld']/a"),
						pobj.defaultDropHere);

				fc.utobj().dragAndDropElement(driver,
						fc.utobj().getElementByXpath(driver, ".//*[@id='fsLeadPersonalProfile_0homeZip_fld']/a"),
						pobj.defaultDropHere);

				fc.utobj().dragAndDropElement(driver,
						fc.utobj().getElementByXpath(driver, ".//*[@id='fsLeadPersonalProfile_0birthMonth_fld']/a"),
						pobj.defaultDropHere);

				fc.utobj().dragAndDropElement(driver,
						fc.utobj().getElementByXpath(driver, ".//*[@id='fsLeadPersonalProfile_0homePhone_fld']/a"),
						pobj.defaultDropHere);

				fc.utobj().dragAndDropElement(driver,
						fc.utobj().getElementByXpath(driver, ".//*[@id='fsLeadPersonalProfile_0homePhoneExt_fld']/a"),
						pobj.defaultDropHere);

				fc.utobj().dragAndDropElement(driver,
						fc.utobj().getElementByXpath(driver, ".//*[@id='fsLeadPersonalProfile_0timeToCall_fld']/a"),
						pobj.defaultDropHere);

				fc.utobj().dragAndDropElement(driver,
						fc.utobj().getElementByXpath(driver, ".//*[@id='fsLeadPersonalProfile_0homeOwnership_fld']/a"),
						pobj.defaultDropHere);

				fc.utobj().dragAndDropElement(driver,
						fc.utobj().getElementByXpath(driver, ".//*[@id='fsLeadPersonalProfile_0email_fld']/a"),
						pobj.defaultDropHere);

				fc.utobj().dragAndDropElement(driver,
						fc.utobj().getElementByXpath(driver, ".//*[@id='fsLeadPersonalProfile_0maritalStatus_fld']/a"),
						pobj.defaultDropHere);

				fc.utobj().dragAndDropElement(driver,
						fc.utobj().getElementByXpath(driver, ".//*[@id='fsLeadPersonalProfile_0spouseName_fld']/a"),
						pobj.defaultDropHere);

				fc.utobj()
						.dragAndDropElement(driver,
								fc.utobj().getElementByXpath(driver,
										".//*[@id='fsLeadPersonalProfile_0heardProformaFrom_fld']/a"),
								pobj.defaultDropHere);

				fc.utobj()
						.dragAndDropElement(driver,
								fc.utobj().getElementByXpath(driver,
										".//*[@id='fsLeadPersonalProfile_0seekingOwnBusiness_fld']/a"),
								pobj.defaultDropHere);

				fc.utobj()
						.dragAndDropElement(driver,
								fc.utobj().getElementByXpath(driver,
										".//*[@id='fsLeadPersonalProfile_0fullTimeBusiness_fld']/a"),
								pobj.defaultDropHere);

				fc.utobj()
						.dragAndDropElement(driver,
								fc.utobj().getElementByXpath(driver,
										".//*[@id='fsLeadPersonalProfile_0otherInvestigation_fld']/a"),
								pobj.defaultDropHere);

				fc.utobj()
						.dragAndDropElement(driver,
								fc.utobj().getElementByXpath(driver,
										".//*[@id='fsLeadPersonalProfile_0presentEmployer_fld']/a"),
								pobj.defaultDropHere);

				fc.utobj().dragAndDropElement(driver,
						fc.utobj().getElementByXpath(driver, ".//*[@id='fsLeadPersonalProfile_0percentOwn_fld']/a"),
						pobj.defaultDropHere);

				fc.utobj().dragAndDropElement(driver,
						fc.utobj().getElementByXpath(driver, ".//*[@id='fsLeadPersonalProfile_0title_fld']/a"),
						pobj.defaultDropHere);

				fc.utobj().dragAndDropElement(driver,
						fc.utobj().getElementByXpath(driver, ".//*[@id='fsLeadPersonalProfile_0dateStarted_fld']/a"),
						pobj.defaultDropHere);

				fc.utobj()
						.dragAndDropElement(driver,
								fc.utobj().getElementByXpath(driver,
										".//*[@id='fsLeadPersonalProfile_0employerAddress_fld']/a"),
								pobj.defaultDropHere);

				fc.utobj().dragAndDropElement(driver,
						fc.utobj().getElementByXpath(driver, ".//*[@id='fsLeadPersonalProfile_0employerCity_fld']/a"),
						pobj.defaultDropHere);

				fc.utobj()
						.dragAndDropElement(driver,
								fc.utobj().getElementByXpath(driver,
										".//*[@id='fsLeadPersonalProfile_0employerCountry_fld']/a"),
								pobj.defaultDropHere);

				fc.utobj().dragAndDropElement(driver,
						fc.utobj().getElementByXpath(driver, ".//*[@id='fsLeadPersonalProfile_0employerState_fld']/a"),
						pobj.defaultDropHere);

				fc.utobj().dragAndDropElement(driver,
						fc.utobj().getElementByXpath(driver, ".//*[@id='fsLeadPersonalProfile_0employerZip_fld']/a"),
						pobj.defaultDropHere);

				fc.utobj().dragAndDropElement(driver,
						fc.utobj().getElementByXpath(driver, ".//*[@id='fsLeadPersonalProfile_0businessPhone_fld']/a"),
						pobj.defaultDropHere);

				fc.utobj().dragAndDropElement(driver,
						fc.utobj().getElementByXpath(driver, ".//*[@id='fsLeadPersonalProfile_0callAtWork_fld']/a"),
						pobj.defaultDropHere);

				fc.utobj().dragAndDropElement(driver,
						fc.utobj().getElementByXpath(driver, ".//*[@id='fsLeadPersonalProfile_0hourPerWeek_fld']/a"),
						pobj.defaultDropHere);

				fc.utobj().dragAndDropElement(driver,
						fc.utobj().getElementByXpath(driver, ".//*[@id='fsLeadPersonalProfile_0salary_fld']/a"),
						pobj.defaultDropHere);

				fc.utobj().dragAndDropElement(driver,
						fc.utobj().getElementByXpath(driver, ".//*[@id='fsLeadPersonalProfile_0responsibility_fld']/a"),
						pobj.defaultDropHere);

				fc.utobj().dragAndDropElement(driver,
						fc.utobj().getElementByXpath(driver, ".//*[@id='fsLeadPersonalProfile_0selfEmployed_fld']/a"),
						pobj.defaultDropHere);

				fc.utobj().dragAndDropElement(driver,
						fc.utobj().getElementByXpath(driver, ".//*[@id='fsLeadPersonalProfile_0limitProforma_fld']/a"),
						pobj.defaultDropHere);

				fc.utobj().dragAndDropElement(driver,
						fc.utobj().getElementByXpath(driver, ".//*[@id='fsLeadPersonalProfile_0similarWork_fld']/a"),
						pobj.defaultDropHere);

				fc.utobj()
						.dragAndDropElement(driver,
								fc.utobj().getElementByXpath(driver,
										".//*[@id='fsLeadPersonalProfile_0financeProforma_fld']/a"),
								pobj.defaultDropHere);

				fc.utobj().dragAndDropElement(driver,
						fc.utobj().getElementByXpath(driver, ".//*[@id='fsLeadPersonalProfile_0partner_fld']/a"),
						pobj.defaultDropHere);

				fc.utobj().dragAndDropElement(driver,
						fc.utobj().getElementByXpath(driver, ".//*[@id='fsLeadPersonalProfile_0supportHowLong_fld']/a"),
						pobj.defaultDropHere);

				fc.utobj().dragAndDropElement(driver,
						fc.utobj().getElementByXpath(driver, ".//*[@id='fsLeadPersonalProfile_0income_fld']/a"),
						pobj.defaultDropHere);

				fc.utobj().dragAndDropElement(driver,
						fc.utobj().getElementByXpath(driver, ".//*[@id='fsLeadPersonalProfile_0otherSalary_fld']/a"),
						pobj.defaultDropHere);

				fc.utobj().dragAndDropElement(driver,
						fc.utobj().getElementByXpath(driver,
								".//*[@id='fsLeadPersonalProfile_0otherIncomeExplaination_fld']/a"),
						pobj.defaultDropHere);

				fc.utobj().dragAndDropElement(driver,
						fc.utobj().getElementByXpath(driver, ".//*[@id='fsLeadPersonalProfile_0soleSource_fld']/a"),
						pobj.defaultDropHere);

				fc.utobj().dragAndDropElement(driver,
						fc.utobj().getElementByXpath(driver, ".//*[@id='fsLeadPersonalProfile_0howSoon_fld']/a"),
						pobj.defaultDropHere);

				fc.utobj().dragAndDropElement(driver,
						fc.utobj().getElementByXpath(driver, ".//*[@id='fsLeadPersonalProfile_0runYourself_fld']/a"),
						pobj.defaultDropHere);

				fc.utobj().dragAndDropElement(driver,
						fc.utobj().getElementByXpath(driver,
								".//*[@id='fsLeadPersonalProfile_0responsibleForOperation_fld']/a"),
						pobj.defaultDropHere);

				fc.utobj()
						.dragAndDropElement(driver,
								fc.utobj().getElementByXpath(driver,
										".//*[@id='fsLeadPersonalProfile_0convictedForFelony_fld']/a"),
								pobj.defaultDropHere);

				fc.utobj().dragAndDropElement(driver,
						fc.utobj().getElementByXpath(driver, ".//*[@id='fsLeadPersonalProfile_0liabilites_fld']/a"),
						pobj.defaultDropHere);

				fc.utobj().dragAndDropElement(driver,
						fc.utobj().getElementByXpath(driver, ".//*[@id='fsLeadPersonalProfile_0bankruptcy_fld']/a"),
						pobj.defaultDropHere);

				fc.utobj().dragAndDropElement(driver,
						fc.utobj().getElementByXpath(driver, ".//*[@id='fsLeadPersonalProfile_0lawsuit_fld']/a"),
						pobj.defaultDropHere);

				fc.utobj().dragAndDropElement(driver,
						fc.utobj().getElementByXpath(driver, ".//*[@id='fsLeadPersonalProfile_0convicted_fld']/a"),
						pobj.defaultDropHere);

				fc.utobj().dragAndDropElement(driver,
						fc.utobj().getElementByXpath(driver, ".//*[@id='fsLeadPersonalProfile_0familyFeelings_fld']/a"),
						pobj.defaultDropHere);

				fc.utobj().dragAndDropElement(driver,
						fc.utobj().getElementByXpath(driver, ".//*[@id='fsLeadPersonalProfile_0otherFacts_fld']/a"),
						pobj.defaultDropHere);

				fc.utobj().clickElement(driver, pobj.realEstate);
				fc.utobj().dragAndDropElement(driver,
						fc.utobj().getElementByXpath(driver, ".//*[@id='fsLeadRealEstate_0siteAddress1_fld']/a"),
						pobj.defaultDropHere);

				fc.utobj().dragAndDropElement(driver,
						fc.utobj().getElementByXpath(driver, ".//*[@id='fsLeadRealEstate_0siteAddress2_fld']/a"),
						pobj.defaultDropHere);

				fc.utobj().dragAndDropElement(driver,
						fc.utobj().getElementByXpath(driver, ".//*[@id='fsLeadRealEstate_0siteCity_fld']/a"),
						pobj.defaultDropHere);

				fc.utobj().dragAndDropElement(driver,
						fc.utobj().getElementByXpath(driver, ".//*[@id='fsLeadRealEstate_0siteCountry_fld']/a"),
						pobj.defaultDropHere);

				fc.utobj().dragAndDropElement(driver,
						fc.utobj().getElementByXpath(driver, ".//*[@id='fsLeadRealEstate_0siteState_fld']/a"),
						pobj.defaultDropHere);

				fc.utobj().dragAndDropElement(driver,
						fc.utobj().getElementByXpath(driver, ".//*[@id='fsLeadRealEstate_0buildingSize_fld']/a"),
						pobj.defaultDropHere);

				fc.utobj().dragAndDropElement(driver,
						fc.utobj().getElementByXpath(driver, ".//*[@id='fsLeadRealEstate_0buildingDimentionsX_fld']/a"),
						pobj.defaultDropHere);

				fc.utobj().dragAndDropElement(driver,
						fc.utobj().getElementByXpath(driver, ".//*[@id='fsLeadRealEstate_0buildingDimentionsY_fld']/a"),
						pobj.defaultDropHere);

				fc.utobj().dragAndDropElement(driver,
						fc.utobj().getElementByXpath(driver, ".//*[@id='fsLeadRealEstate_0buildingDimentionsZ_fld']/a"),
						pobj.defaultDropHere);

				fc.utobj().dragAndDropElement(driver,
						fc.utobj().getElementByXpath(driver, ".//*[@id='fsLeadRealEstate_0parkingSpaces_fld']/a"),
						pobj.defaultDropHere);

				fc.utobj().dragAndDropElement(driver,
						fc.utobj().getElementByXpath(driver, ".//*[@id='fsLeadRealEstate_0dealType_fld']/a"),
						pobj.defaultDropHere);

				fc.utobj().dragAndDropElement(driver,
						fc.utobj().getElementByXpath(driver, ".//*[@id='fsLeadRealEstate_0dealType_fld']/a"),
						pobj.defaultDropHere);

				fc.utobj().dragAndDropElement(driver,
						fc.utobj().getElementByXpath(driver, ".//*[@id='fsLeadRealEstate_0loiSigned_fld']/a"),
						pobj.defaultDropHere);

				fc.utobj().dragAndDropElement(driver,
						fc.utobj().getElementByXpath(driver, ".//*[@id='fsLeadRealEstate_0approvalDate_fld']/a"),
						pobj.defaultDropHere);

				fc.utobj().dragAndDropElement(driver,
						fc.utobj().getElementByXpath(driver, ".//*[@id='fsLeadRealEstate_0leaseCommencement_fld']/a"),
						pobj.defaultDropHere);

				fc.utobj().dragAndDropElement(driver,
						fc.utobj().getElementByXpath(driver, ".//*[@id='fsLeadRealEstate_0leaseExpiration_fld']/a"),
						pobj.defaultDropHere);

				fc.utobj().dragAndDropElement(driver,
						fc.utobj().getElementByXpath(driver, ".//*[@id='fsLeadRealEstate_0initialTerm_fld']/a"),
						pobj.defaultDropHere);

				fc.utobj().dragAndDropElement(driver,
						fc.utobj().getElementByXpath(driver, ".//*[@id='fsLeadRealEstate_0optionTerm_fld']/a"),
						pobj.defaultDropHere);

				fc.utobj().dragAndDropElement(driver,
						fc.utobj().getElementByXpath(driver, ".//*[@id='fsLeadRealEstate_0purchaseOption_fld']/a"),
						pobj.defaultDropHere);

				fc.utobj()
						.dragAndDropElement(driver,
								fc.utobj().getElementByXpath(driver,
										".//*[@id='fsLeadRealEstate_0projectedOpeningDate_fld']/a"),
								pobj.defaultDropHere);

				fc.utobj()
						.dragAndDropElement(driver,
								fc.utobj().getElementByXpath(driver,
										".//*[@id='fsLeadRealEstate_0generalContractorSelector_fld']/a"),
								pobj.defaultDropHere);

				fc.utobj()
						.dragAndDropElement(driver,
								fc.utobj().getElementByXpath(driver,
										".//*[@id='fsLeadRealEstate_0nameGeneralContractor_fld']/a"),
								pobj.defaultDropHere);

				fc.utobj()
						.dragAndDropElement(driver,
								fc.utobj().getElementByXpath(driver,
										".//*[@id='fsLeadRealEstate_0addressGeneralContractor_fld']/a"),
								pobj.defaultDropHere);

				fc.utobj()
						.dragAndDropElement(driver,
								fc.utobj().getElementByXpath(driver,
										".//*[@id='fsLeadRealEstate_0addressGeneralContractor_fld']/a"),
								pobj.defaultDropHere);

				fc.utobj().dragAndDropElement(driver,
						fc.utobj().getElementByXpath(driver, ".//*[@id='fsLeadRealEstate_0permitApplied_fld']/a"),
						pobj.defaultDropHere);

				fc.utobj().dragAndDropElement(driver,
						fc.utobj().getElementByXpath(driver, ".//*[@id='fsLeadRealEstate_0permitIssued_fld']/a"),
						pobj.defaultDropHere);

				fc.utobj().dragAndDropElement(driver,
						fc.utobj().getElementByXpath(driver, ".//*[@id='fsLeadRealEstate_0certificate_fld']/a"),
						pobj.defaultDropHere);

				fc.utobj().dragAndDropElement(driver,
						fc.utobj().getElementByXpath(driver, ".//*[@id='fsLeadRealEstate_0turnOverDate_fld']/a"),
						pobj.defaultDropHere);

				fc.utobj().dragAndDropElement(driver,
						fc.utobj().getElementByXpath(driver, ".//*[@id='fsLeadRealEstate_0grandOpeningDate_fld']/a"),
						pobj.defaultDropHere);

				if (formFormat.equalsIgnoreCase("Single Page")) {
					// add Text in custom section
					fc.utobj().doubleClickElement(driver, fc.utobj().getElementByXpath(driver,
							".//*[.='" + sectionName + "']/following-sibling::table//ul/li[@class='emptyList']"));
					fc.commonMethods().switch_cboxIframe_frameId(driver);
					fc.utobj().sendKeys(driver, pobj.labelAddText, fieldLabel);
					fc.utobj().selectDropDown(driver, pobj.headerLabelAlignment, "Left");
					fc.utobj().clickElement(driver, pobj.saveBtn);
					fc.utobj().switchFrameToDefault(driver);

					// add Text Drop Here Custom
					fc.utobj().clickElement(driver, pobj.formTools);
					fc.utobj().dragAndDropElement(driver, pobj.formToolText,
							fc.utobj().getElementByXpath(driver, ".//*[.='" + sectionName
									+ "']/following-sibling::table//li[@class='emptyList']//p[.='Drop here.']"));

				} else if (formFormat.equalsIgnoreCase("Multi Page")) {
					fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//li[.='" + tabName + "']"));

					// add Text in custom section
					// click over double click
					String text = fc.utobj().getElementByXpath(driver, ".//li[.='" + tabName + "']").getAttribute("id");

					String tabName1 = tabName.toLowerCase();
					text = text.replace("_" + tabName1 + "_", "");
					text = text.replace("_Tab", "");

					String text1 = driver
							.findElement(By.xpath(
									".//div[contains(@id , '" + text + "')]/ul[contains(@id , '" + text + "')]/li"))
							.getAttribute("id");
					text1 = text1.replace("defaultSection_", "");

					fc.utobj().doubleClickElement(driver, fc.utobj().getElementByXpath(driver, ".//ul[contains(@id , '"
							+ text1 + "')]/li[@class='emptyList']//p[.='Double click or drag & drop to add text.']"));

					fc.commonMethods().switch_cboxIframe_frameId(driver);
					fc.utobj().sendKeys(driver, pobj.labelAddText, fieldLabel);
					fc.utobj().selectDropDown(driver, pobj.headerLabelAlignment, "Left");
					fc.utobj().clickElement(driver, pobj.saveBtn);
					fc.utobj().switchFrameToDefault(driver);

					// add Text Drop Here Custom
					fc.utobj().clickElement(driver, pobj.formTools);

					fc.utobj().dragAndDropElement(driver, pobj.formToolText, driver
							.findElement(By.xpath(".//ul[contains(@id , '" + text + "')]/li//p[.='Drop here.']")));

				}
				// add Footer
				fc.utobj().doubleClickElement(driver, pobj.addFooterDefault);
				fc.commonMethods().switch_cboxIframe_frameId(driver);
				fc.utobj().switchFrameById(driver, "textBlock_ifr");

				WebElement editorTextArea1 = fc.utobj().getElementByXpath(driver, ".//*[@id='tinymce']/p");
				Actions actions1 = new Actions(driver);
				actions1.moveToElement(editorTextArea1);
				actions1.click();
				actions1.sendKeys(footer);
				fc.utobj().logReport("Entering Value in Text Field", editorTextArea1);
				fc.utobj().logReportMsg("Entered Text", footer);
				actions1.build().perform();
				fc.utobj().switchFrameToDefault(driver);
				fc.commonMethods().switch_cboxIframe_frameId(driver);
				fc.utobj().clickElement(driver, pobj.saveBtn);
				fc.utobj().switchFrameToDefault(driver);
				fc.utobj().clickElement(driver, pobj.saveAndNextBtnDesign);

				// Setting
				if (afterSubmission.equalsIgnoreCase("message")) {
					if (!fc.utobj().getElement(driver, pobj.afterSubmissionMsg).isSelected()) {
						fc.utobj().clickElement(driver, pobj.afterSubmissionMsg);
					}
				} else if (afterSubmission.equalsIgnoreCase("url")) {
					if (!fc.utobj().getElement(driver, pobj.afterSubmissionUrl).isSelected()) {
						fc.utobj().clickElement(driver, pobj.afterSubmissionUrl);
					}
					fc.utobj().sendKeys(driver, pobj.redirectedUrl, redirectedUrl);
				}

				fc.utobj().clickElement(driver, pobj.finishBtn);

				if (!pobj.hostURL.isSelected()) {
					fc.utobj().clickElement(driver, pobj.hostURL);
				}

				urlText = pobj.hostCodeBox.getText().trim();
				fc.utobj().clickElement(driver, pobj.okBtn);

				fc.utobj().printTestResultExcel(testCaseId, null, null, "Passed", null, null);
			} catch (Exception e) {
				fc.utobj().quitBrowserOnCatchBasicMethod(driver, e, testCaseId);
			}
		} else {
			fc.utobj().throwsSkipException(
					"was not able to Create Form For Contact Type :: Admin > CRM > Manage Web Form Generator");

		}
		return urlText;
	}

	@Test(groups = { "salesank" ,"TestCaptcha"})
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "TC_WebForm_FillAllFields_001", testCaseDescription = "Validate Fdd Email in the activity history > Validate email in mail box >Validate fdd login and download ")
	private void verifySalesWebFormFillAllFields() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("sales", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			fc.utobj().printTestStep("Go to Admin > Sales > Manage Web Form Generator  > Design");
			fc.adminpage().adminPage(driver);
			
			String formName = fc.utobj().generateTestData("FormName");
			String formTitle = fc.utobj().generateTestData("FormTitle");
			String description = fc.utobj().generateTestData("FormDescription");
			String formFormat = "Single Page";
			String submissionType = null;
			String formUrl = fc.utobj().generateTestData("FormURL");
			String sectionName = fc.utobj().generateTestData("SectionName");
			String tabName = fc.utobj().generateTestData("TabName");
			String header = fc.utobj().generateTestData("header");
			String textLabel = fc.utobj().generateTestData("textLabel");
			String fieldLabel = fc.utobj().generateTestData("FieldLabel");
			String footer = fc.utobj().generateTestData("Footer");
			String afterSubmission = fc.utobj().generateTestData("afterSub");
			String redirectedUrl = "www.franconnect.com";
			AdminFranchiseSalesManageWebFormGeneratorPage pobj = new AdminFranchiseSalesManageWebFormGeneratorPage(
					driver);
			fc.utobj().printTestStep("Select all fields in the form");
			createNewFormContactType(driver, formName, formTitle, description, formFormat, submissionType, formUrl,
					sectionName, tabName, header, textLabel, fieldLabel, footer, afterSubmission, redirectedUrl,
					FranconnectUtil.config);

			String firstName = fc.utobj().generateTestData(dataSet.get("FirstName"));
			String lastName = fc.utobj().generateTestData(dataSet.get("LastName"));
			String parentWindow = driver.getWindowHandle();
			fc.utobj().sendKeys(driver, pobj.searchMyForm, formName);
			fc.utobj().clickElement(driver, pobj.searchMyFormBtn);
			fc.utobj().printTestStep("Launch the form");
			fc.utobj().actionImgOption(driver, formName, "Launch & Test");

			fc.utobj().printTestStep("Launc And Test Form");
			fc.utobj().printTestStep("Fill all fields");
			fc.utobj().printTestStep("Submit");
			Set<String> allWindows2 = driver.getWindowHandles();
			for (String currentWindow : allWindows2) {
				//
				if (!currentWindow.equalsIgnoreCase(parentWindow)) {
					driver.switchTo().window(currentWindow);
					//
					String titleTextCurrent = driver.getTitle();
					if (titleTextCurrent.equalsIgnoreCase(formTitle)) {
						// fill the form with data
						//

						fc.utobj().sendKeys(driver, pobj.leadFirstName, firstName);
						fc.utobj().sendKeys(driver, pobj.leadLastName, lastName);

						fc.utobj().sendKeys(driver, pobj.emailID, "salesautomation@staffex.com");
						fc.utobj().sendKeys(driver, pobj.emailID, "salesautomation@staffex.com");
						// fc.utobj().selectDropDownByPartialText(driver,
						// pobj.salutationField, "Mr.");
						fc.utobj().sendKeys(driver, pobj.address, "address1");
						fc.utobj().sendKeys(driver, pobj.address2, "address2");
						fc.utobj().selectDropDownByPartialText(driver, pobj.country, "USA");
						fc.utobj().sendKeys(driver, pobj.zip, "224122");
						fc.utobj().selectDropDownByPartialText(driver, pobj.stateID, "Alabama");
						fc.utobj().selectDropDownByPartialText(driver, pobj.countyID, "Butler");
						fc.utobj().sendKeys(driver, pobj.bestTimeToContact, "Evening");
						fc.utobj().sendKeys(driver, pobj.phone, "1234667892");
						fc.utobj().sendKeys(driver, pobj.phoneExt, "91");
						fc.utobj().sendKeys(driver, pobj.homePhone, "9874563212");
						fc.utobj().sendKeys(driver, pobj.homePhoneExt, "92");
						fc.utobj().sendKeys(driver, pobj.fax, "44444444");
						fc.utobj().sendKeys(driver, pobj.mobile, "8416825736");
						fc.utobj().sendKeys(driver, pobj.companyName, "8416825736");
						fc.utobj().sendKeys(driver, pobj.comments, "Comments");
						fc.utobj().selectDropDownByPartialText(driver, pobj.leadSource2ID, "Friends");
						fc.utobj().selectDropDownByPartialText(driver, pobj.leadSource3ID, "Friends");
						fc.utobj().sendKeys(driver, pobj.liquidCapitalMax, "5000000");
						fc.utobj().sendKeys(driver, pobj.investTimeframe, "1 Month");
						fc.utobj().sendKeys(driver, pobj.background, "Backgorund");
						fc.utobj().sendKeys(driver, pobj.sourceOfFunding, "source Of Funding");
						fc.utobj().sendKeys(driver, pobj.noOfUnitReq, "1");
						fc.utobj().sendKeys(driver, pobj.preferredCity1, "Preferred City 1");
						fc.utobj().selectDropDownByPartialText(driver, pobj.preferredCountry1, "USA");
						fc.utobj().sendKeys(driver, pobj.preferredCity2, "Peferred City 2");
						fc.utobj().selectDropDownByPartialText(driver, pobj.preferredCountry2, "USA");
						fc.utobj().selectDropDownByPartialText(driver, pobj.preferredStateId2, "Alabama");
						// fc.utobj().selectDropDownByPartialText(driver,
						// pobj.preferredStateId2, "Alabama");
						String currentDate = fc.utobj().getCurrentDateUSFormat();
						String futureDate = fc.utobj().getFutureDateUSFormat(30);
						fc.utobj().sendKeys(driver, pobj.dateSubmission, currentDate);
						fc.utobj().sendKeys(driver, pobj.date, currentDate);
						fc.utobj().clickElement(driver, pobj.gender_Male);
						fc.utobj().sendKeys(driver, pobj.presentAddress, "Present Address");
						fc.utobj().sendKeys(driver, pobj.fsLeadQualificationDetail_0city, "City");
						fc.utobj().selectDropDownByPartialText(driver, pobj.fsLeadQualificationDetail_0country, "USA");
						fc.utobj().selectDropDownByPartialText(driver, pobj.fsLeadQualificationDetail_0stateID,
								"Alabama");
						fc.utobj().sendKeys(driver, pobj.fsLeadQualificationDetail_0zipCode, "224141");
						fc.utobj().sendKeys(driver, pobj.fsLeadQualificationDetail_0workPhone, "8568456515");
						fc.utobj().sendKeys(driver, pobj.fsLeadQualificationDetail_0phoneExt, "93");
						fc.utobj().sendKeys(driver, pobj.fsLeadQualificationDetail_0homePhone, "3265959645");
						fc.utobj().sendKeys(driver, pobj.fsLeadQualificationDetail_0emailID,
								"salesautomation@farnqa.net");
						fc.utobj().clickElement(driver, pobj.usCitizenYes);
						fc.utobj().sendKeys(driver, pobj.ssn, "1232342");
						fc.utobj().sendKeys(driver, pobj.previousAddress, "Previous Address");
						fc.utobj().selectDropDownByPartialText(driver, pobj.fsLeadQualificationDetail_0birthMonth,
								"Jul");
						fc.utobj().selectDropDownByPartialText(driver, pobj.fsLeadQualificationDetail_0birthDate, "13");
						fc.utobj().clickElement(driver, pobj.homeOwnership);
						fc.utobj().clickElement(driver, pobj.maritalStatus);
						fc.utobj().sendKeys(driver, pobj.fsLeadQualificationDetail_0spouseName, "Spouse Name");
						fc.utobj().sendKeys(driver, pobj.fsLeadQualificationDetail_0spouseSsn, "235245634");
						fc.utobj().clickElement(driver, pobj.spouseUsCitizenYes);
						fc.utobj().selectDropDownByPartialText(driver, pobj.fsLeadQualificationDetail_0spouseBirthMonth,
								"May");
						fc.utobj().selectDropDownByPartialText(driver, pobj.fsLeadQualificationDetail_0spouseBirthDate,
								"21");
						fc.utobj().sendKeys(driver, pobj.fsLeadQualificationDetail_0cashOnHand, "500000");
						fc.utobj().sendKeys(driver, pobj.fsLeadQualificationDetail_0mortgages, "5000");
						fc.utobj().sendKeys(driver, pobj.fsLeadQualificationDetail_0marketableSecurities, "50002");
						fc.utobj().sendKeys(driver, pobj.fsLeadQualificationDetail_0accountsPayable, "50003");
						fc.utobj().sendKeys(driver, pobj.fsLeadQualificationDetail_0accountsReceivable, "50004");
						fc.utobj().sendKeys(driver, pobj.fsLeadQualificationDetail_0notesPayable, "50005");
						fc.utobj().sendKeys(driver, pobj.fsLeadQualificationDetail_0retirementPlans, "50006");
						fc.utobj().sendKeys(driver, pobj.fsLeadQualificationDetail_0loansOnLifeInsurance, "50007");
						fc.utobj().sendKeys(driver, pobj.fsLeadQualificationDetail_0realEstate, "50008");
						fc.utobj().sendKeys(driver, pobj.fsLeadQualificationDetail_0creditCardBalance, "50009");
						fc.utobj().sendKeys(driver, pobj.fsLeadQualificationDetail_0personalProperty, "50010");
						fc.utobj().sendKeys(driver, pobj.fsLeadQualificationDetail_0unpaidTaxes, "50011");
						fc.utobj().sendKeys(driver, pobj.fsLeadQualificationDetail_0businessHoldings, "50012");
						fc.utobj().sendKeys(driver, pobj.fsLeadQualificationDetail_0lifeInsurance, "50013");
						fc.utobj().sendKeys(driver, pobj.fsLeadQualificationDetail_0otherAssets, "50014");
						fc.utobj().sendKeys(driver, pobj.fsLeadQualificationDetail_0otherLiabilities, "50015");
						fc.utobj().sendKeys(driver, pobj.fsLeadQualificationDetail_0liablitiesDescription,
								"Other Liablities Description");
						// fc.utobj().sendKeys(driver,
						// pobj.fsLeadQualificationDetail_0totalAssets,
						// "5000011");
						// fc.utobj().sendKeys(driver,
						// pobj.fsLeadQualificationDetail_0totalLiabilities,
						// "50015");
						// fc.utobj().sendKeys(driver,
						// pobj.fsLeadQualificationDetail_0totalNetworth,
						// "5000016");
						fc.utobj().sendKeys(driver, pobj.fsLeadQualificationDetail_0reAddress1, "Real State Address 1");
						// fc.utobj().sendKeys(driver,
						// pobj.fsLeadQualificationDetail_0reAddress1, "Real
						// State Address 2");
						fc.utobj().sendKeys(driver, pobj.fsLeadQualificationDetail_0reDatePurchased1,
								currentDate);
						fc.utobj().sendKeys(driver, pobj.fsLeadQualificationDetail_0reOrigCost1, "5000017");
						fc.utobj().sendKeys(driver, pobj.fsLeadQualificationDetail_0rePresentValue1, "5000018");
						fc.utobj().sendKeys(driver, pobj.fsLeadQualificationDetail_0reMortgageBalance1, "5000019");
						fc.utobj().sendKeys(driver, pobj.fsLeadQualificationDetail_0reAddress2,
								"QualificationDetail Address 2");
						fc.utobj().sendKeys(driver, pobj.fsLeadQualificationDetail_0reDatePurchased2,
								currentDate);
						fc.utobj().sendKeys(driver, pobj.fsLeadQualificationDetail_0reOrigCost2, "500021");
						fc.utobj().sendKeys(driver, pobj.fsLeadQualificationDetail_0rePresentValue2, "500021");
						fc.utobj().sendKeys(driver, pobj.fsLeadQualificationDetail_0reMortgageBalance2, "500021");
						fc.utobj().sendKeys(driver, pobj.fsLeadQualificationDetail_0reAddress3,
								"Qualification DetailAdress 3");
						fc.utobj().sendKeys(driver, pobj.fsLeadQualificationDetail_0reDatePurchased3,
								currentDate);
						fc.utobj().sendKeys(driver, pobj.fsLeadQualificationDetail_0reOrigCost3, "500022");
						fc.utobj().sendKeys(driver, pobj.fsLeadQualificationDetail_0rePresentValue3, "500023");
						fc.utobj().sendKeys(driver, pobj.fsLeadQualificationDetail_0reMortgageBalance3, "500022");
						fc.utobj().sendKeys(driver, pobj.fsLeadQualificationDetail_0annualSalary, "500023");
						fc.utobj().sendKeys(driver, pobj.fsLeadQualificationDetail_0annualInvestment, "500024");
						fc.utobj().sendKeys(driver, pobj.fsLeadQualificationDetail_0annualReIncome, "500025");
						fc.utobj().sendKeys(driver, pobj.fsLeadQualificationDetail_0otherAnnualSource, "500026");
						// fc.utobj().sendKeys(driver,
						// pobj.fsLeadQualificationDetail_0annualSourceTotal,
						// "500027");
						fc.utobj().sendKeys(driver, pobj.fsLeadQualificationDetail_0loanCoSign, "500027");
						fc.utobj().sendKeys(driver, pobj.fsLeadQualificationDetail_0legalJudgement, "500027");
						fc.utobj().sendKeys(driver, pobj.fsLeadQualificationDetail_0otherSpecialDebt, "500027");
						// fc.utobj().sendKeys(driver,
						// pobj.fsLeadQualificationDetail_0totalContingent,
						// "500027");
						fc.utobj().sendKeys(driver, pobj.fsLeadQualificationDetail_0whenReadyIfApproved,
								"When would you be ready to invest in your franchise if you were approved");
						fc.utobj().sendKeys(driver, pobj.fsLeadQualificationDetail_0skillsExperience,
								"What skills/experience do you have that will help you be successful in this");
						fc.utobj().sendKeys(driver, pobj.fsLeadQualificationDetail_0responsibleForDailyOperations,
								"Who will be responsible for the daily operation of your store?");
						fc.utobj().sendKeys(driver, pobj.fsLeadQualificationDetail_0cashAvailable, "500024");
						fc.utobj().clickElement(driver, pobj.fsLeadQualificationDetail_0approvedForFinancing0);
						fc.utobj().sendKeys(driver, pobj.fsLeadQualificationDetail_0amountApprovedForFinance, "500024");
						fc.utobj().clickElement(driver, pobj.fsLeadQualificationDetail_0soleIncomeSource0);
						fc.utobj().clickElement(driver, pobj.fsLeadQualificationDetail_0liabilites0);
						fc.utobj().clickElement(driver, pobj.fsLeadQualificationDetail_0lawsuit0);
						fc.utobj().clickElement(driver, pobj.fsLeadQualificationDetail_0convicted0);
						fc.utobj().clickElement(driver, pobj.fsLeadQualificationDetail_0convictedOfFelony0);
						fc.utobj().sendKeys(driver, pobj.fsLeadQualificationDetail_0explainConviction,
								"If so, explain ");
						fc.utobj().sendKeys(driver, pobj.fsLeadPersonalProfile_0dateSubmission, currentDate);
						fc.utobj().clickElement(driver, pobj.fsLeadPersonalProfile_0gender0);
						fc.utobj().clickElement(driver, pobj.fsLeadPersonalProfile_0gender0);
						fc.utobj().sendKeys(driver, pobj.fsLeadPersonalProfile_0homeAddress,
								"PersonalProfile Address  ");
						fc.utobj().sendKeys(driver, pobj.fsLeadPersonalProfile_0howLongAtAddress, "5");
						fc.utobj().sendKeys(driver, pobj.fsLeadPersonalProfile_0homeCity, "City");
						fc.utobj().selectDropDownByPartialText(driver, pobj.fsLeadPersonalProfile_0homeCountry, "USA");
						fc.utobj().sendKeys(driver, pobj.fsLeadPersonalProfile_0homeZip, "234453");
						fc.utobj().sendKeys(driver, pobj.fsLeadPersonalProfile_0homePhone, "4235234453");
						fc.utobj().sendKeys(driver, pobj.fsLeadPersonalProfile_0homePhoneExt, "94");
						fc.utobj().clickElement(driver, pobj.fsLeadPersonalProfile_0homeOwnership0);
						fc.utobj().sendKeys(driver, pobj.fsLeadPersonalProfile_0email,
								"salesautomation@staffex.com");
						fc.utobj().sendKeys(driver, pobj.fsLeadPersonalProfile_0spouseName, "Spouse Name");
						fc.utobj().sendKeys(driver, pobj.fsLeadPersonalProfile_0seekingOwnBusiness,
								"How long have you wanted to operate your own franchise");
						fc.utobj().clickElement(driver, pobj.personalProfileFullTimBuisness);
						fc.utobj().sendKeys(driver, pobj.fsLeadPersonalProfile_0otherInvestigation,
								"Are there any other franchise opportunities that you are looking into");
						fc.utobj().sendKeys(driver, pobj.fsLeadPersonalProfile_0presentEmployer, "Present Employer");
						fc.utobj().sendKeys(driver, pobj.fsLeadPersonalProfile_0title, "Title");
						fc.utobj().sendKeys(driver, pobj.fsLeadPersonalProfile_0dateStarted, currentDate);
						fc.utobj().sendKeys(driver, pobj.fsLeadPersonalProfile_0employerAddress, "employerAddress");
						fc.utobj().sendKeys(driver, pobj.fsLeadPersonalProfile_0employerCity, "employerCity");
						fc.utobj().selectDropDownByPartialText(driver, pobj.fsLeadPersonalProfile_0employerCountry,
								"USA");
						fc.utobj().sendKeys(driver, pobj.fsLeadPersonalProfile_0employerZip, "225155");
						fc.utobj().sendKeys(driver, pobj.fsLeadPersonalProfile_0businessPhone, "222225155");
						// fc.utobj().sendKeys(driver,
						// pobj.fsLeadPersonalProfile_0businessPhone,
						// "222225155");
						fc.utobj().clickElement(driver, pobj.fsLeadPersonalProfile_0callAtWork0);
						fc.utobj().sendKeys(driver, pobj.fsLeadPersonalProfile_0salary, "2225155");
						fc.utobj().sendKeys(driver, pobj.fsLeadPersonalProfile_0responsibility,
								"Brief description of your responsibilities ");
						fc.utobj().clickElement(driver, pobj.fsLeadPersonalProfile_0limitProforma0);
						fc.utobj().clickElement(driver, pobj.fsLeadPersonalProfile_0similarWork0);
						fc.utobj().sendKeys(driver, pobj.fsLeadPersonalProfile_0financeProforma,
								"How would you finance your franchise?");
						fc.utobj().clickElement(driver, pobj.fsLeadPersonalProfile_0partner0);
						fc.utobj().selectDropDownByPartialText(driver, pobj.fsLeadPersonalProfile_0supportHowLong, "1");
						fc.utobj().sendKeys(driver, pobj.fsLeadPersonalProfile_0otherSalary, "1222");
						fc.utobj().sendKeys(driver, pobj.fsLeadPersonalProfile_0otherIncomeExplaination,
								"If other income, explain");
						fc.utobj().clickElement(driver, pobj.fsLeadPersonalProfile_0soleSource0);
						fc.utobj().sendKeys(driver, pobj.fsLeadPersonalProfile_0howSoon,
								"If qualified, when would you be ready to start your Franchise Business");
						fc.utobj().clickElement(driver, pobj.fsLeadPersonalProfile_0runYourself0);
						fc.utobj().sendKeys(driver, pobj.fsLeadPersonalProfile_0responsibleForOperation,
								"If not, who will be responsible for the daily operation of your business");
						fc.utobj().clickElement(driver, pobj.fsLeadPersonalProfile_0convictedForFelony0);
						fc.utobj().clickElement(driver, pobj.fsLeadPersonalProfile_0bankruptcy0);
						fc.utobj().clickElement(driver, pobj.fsLeadPersonalProfile_0convicted0);
						fc.utobj().sendKeys(driver, pobj.fsLeadPersonalProfile_0otherFacts,
								"Other facts you want us to know");
						fc.utobj().sendKeys(driver, pobj.fsLeadRealEstate_0siteAddress2, "Site Street 2");
						fc.utobj().sendKeys(driver, pobj.fsLeadRealEstate_0siteCity, "Site City");
						fc.utobj().selectDropDownByPartialText(driver, pobj.fsLeadRealEstate_0siteCountry, "USA");
						fc.utobj().selectDropDownByPartialText(driver, pobj.fsLeadRealEstate_0siteState, "Alabama");
						fc.utobj().sendKeys(driver, pobj.fsLeadRealEstate_0buildingSize, "1000");
						fc.utobj().sendKeys(driver, pobj.fsLeadRealEstate_0buildingDimentionsX, "1000");
						fc.utobj().sendKeys(driver, pobj.fsLeadRealEstate_0buildingDimentionsZ, "1000");
						fc.utobj().sendKeys(driver, pobj.fsLeadRealEstate_0dealType, "Deal Type");
						fc.utobj().sendKeys(driver, pobj.fsLeadRealEstate_0loiSigned, futureDate);
						fc.utobj().sendKeys(driver, pobj.fsLeadRealEstate_0leaseCommencement, futureDate);
						fc.utobj().sendKeys(driver, pobj.fsLeadRealEstate_0initialTerm, "99");
						fc.utobj().clickElement(driver, pobj.fsLeadRealEstate_0purchaseOption0);
						fc.utobj().clickElement(driver, pobj.fsLeadRealEstate_0generalContractorSelector0);
						fc.utobj().sendKeys(driver, pobj.fsLeadRealEstate_0addressGeneralContractor, "Address");
						fc.utobj().sendKeys(driver, pobj.fsLeadRealEstate_0permitApplied, futureDate);
						fc.utobj().sendKeys(driver, pobj.fsLeadRealEstate_0certificate, futureDate);
						fc.utobj().sendKeys(driver, pobj.fsLeadRealEstate_0grandOpeningDate, futureDate);
						fc.utobj().clickElement(driver, pobj.submitBtn);

						fc.utobj().isTextDisplayed(driver,
								"Thank you for submitting your information, we will get back to you shortly.",
								"was not able to verify confirmation Msg");

						driver.close();
					} else {
						driver.close();
						driver.switchTo().window(parentWindow);
					}
					driver.switchTo().window(parentWindow);
				}
				//
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);

		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

}
