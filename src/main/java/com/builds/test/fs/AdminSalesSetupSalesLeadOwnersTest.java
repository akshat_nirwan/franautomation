package com.builds.test.fs;

import java.util.HashMap;
import java.util.Map;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import com.builds.test.admin.AdminUsersManageCorporateUsersAddCorporateUserPageTest;
import com.builds.test.admin.AdminUsersManageDivisionalUsersAddDivisionalUsersPageTest;
import com.builds.test.admin.AdminUsersManageRegionalUsersAddRegionalUserPageTest;
import com.builds.test.common.CorporateUser;
import com.builds.uimaps.fs.AdminSalesSetupLeadOwnersPage;
import com.builds.utilities.FranconnectUtil;
import com.builds.utilities.TestCase;

public class AdminSalesSetupSalesLeadOwnersTest {

	FranconnectUtil fc = new FranconnectUtil();

	@Test(groups = { "sales", "salessetup001" })
	@TestCase(createdOn = "2017-08-03", updatedOn = "2017-08-03", testCaseId = "TC_Sales_SetupSalesLeadOwners_001", testCaseDescription = "Verify by default option in Setup Sales Lead Owners is All users as Sales Lead Owners")
	private void verifyDefaultSalesLeadOwnerCheck() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			AdminSalesSetupLeadOwnersPage pobj = new AdminSalesSetupLeadOwnersPage(driver);
			fc.utobj().printTestStep("Go to Admin > Sales > Setup Sales Lead Owners");
			fc.adminpage().adminPage(driver);
			fc.adminpage().openSetupFranchiseSalesLeadOwnersPage(driver);
			WebElement alluserRadio = fc.utobj().getElement(driver, pobj.alluserSalesLeadOwners);
			fc.utobj().printTestStep("Check that All users as Sales Lead Owners is selected by default");
			if (alluserRadio.isSelected() == false) {
				fc.utobj().throwsException("All users as Sales Lead Owners is not checked by default");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "sales", "TC_Sales_SetupSalesLeadOwners_002" }) // ok
	@TestCase(createdOn = "2017-08-03", updatedOn = "2017-08-03", testCaseId = "TC_Sales_SetupSalesLeadOwners_002", testCaseDescription = "Verify that the Selected Sales Lead Owners shows all the user")
	private void verifyExistingRoles() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("sales", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			AdminSalesSetupLeadOwnersPage pobj = new AdminSalesSetupLeadOwnersPage(driver);
			String userNameDiv = fc.utobj().generateTestData(dataSet.get("userNameDiv"));
			String divisionName = fc.utobj().generateTestData(dataSet.get("divisionName"));
			String regionName = fc.utobj().generateTestData(dataSet.get("regionName"));
			String userNameReg = fc.utobj().generateTestData(dataSet.get("userNameReg"));
			String emailId = "salesautomation@staffex.com";

			fc.utobj().printTestStep("Go to Admin Users > Add all three user type Corporate, Divisional, Regional");
			fc.adminpage().adminPage(driver);
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corpTest = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser = new CorporateUser();
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corpUser.setEmail(emailId);
			corpUser = corpTest.createDefaultUser(driver, corpUser);

			AdminUsersManageDivisionalUsersAddDivisionalUsersPageTest divUser = new AdminUsersManageDivisionalUsersAddDivisionalUsersPageTest();
			String DivUserName = divUser.addDivisionalUser(driver, userNameDiv, divisionName, emailId);

			AdminUsersManageRegionalUsersAddRegionalUserPageTest RegUser = new AdminUsersManageRegionalUsersAddRegionalUserPageTest();
			RegUser.addRegionalUser(driver, userNameReg, regionName, emailId);

			fc.utobj().printTestStep(
					"Admin > Sales > Setup Sales Lead Owners > Click on Selected users as Sales Lead Owners");
			fc.adminpage().adminPage(driver);
			fc.adminpage().openSetupFranchiseSalesLeadOwnersPage(driver);
			fc.utobj().clickElement(driver, pobj.selectedUsersForFS);
			fc.utobj().printTestStep("All three user should be available in Selected Sales Lead Owners.");
			boolean isCorpUserPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//select[@name='selectedUsersRight']/option[contains(text(),'" + corpUser.getuserFullName()
							+ "')]");
			boolean isDivUserPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//select[@name='selectedUsersRight']/option[contains(text(),'" + DivUserName + "')]");
			boolean isRegUserPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//select[@name='selectedUsersRight']/option[contains(text(),'" + userNameReg + " " + userNameReg
							+ "')]");
			if (isCorpUserPresent == false) {
				fc.utobj().throwsException(
						"Created corporate user not present in available in Selected Sales Lead Owners.");
			}
			if (isDivUserPresent == false) {
				fc.utobj().throwsException(
						"created Divisional user not present in available in Selected Sales Lead Owners.");
			}
			if (isRegUserPresent == false) {
				fc.utobj().throwsException(
						"created Regional user not present in available in Selected Sales Lead Owners.");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "sales", "salessetup003" })
	@TestCase(createdOn = "2017-08-04", updatedOn = "2017-08-04", testCaseId = "TC_Sales_SetupSalesLeadOwners_003", testCaseDescription = "Verify the Corporate User associated with task cannot be moved back to active user list")
	private void verifyCorpUserAssociatedWithTask() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("sales", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			AdminSalesSetupLeadOwnersPage pobj = new AdminSalesSetupLeadOwnersPage(driver);
			AddLeadFromAllSources addlead = new AddLeadFromAllSources();

			String emailId = "salesautomation@staffex.com";

			fc.utobj().printTestStep("Go to Admin Users > Add all Corporate");
			fc.adminpage().adminPage(driver);
			AdminUsersManageCorporateUsersAddCorporateUserPageTest corpTest = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
			CorporateUser corpUser = new CorporateUser();
			corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
			corpUser.setEmail(emailId);
			corpUser = corpTest.createDefaultUser(driver, corpUser);
			String firstName = fc.utobj().generateTestData(dataSet.get("firstName"));
			String lastName = fc.utobj().generateTestData(dataSet.get("lastName"));
			String country = dataSet.get("country");
			String state = dataSet.get("state");
			String leadOwner = corpUser.getuserFullName();
			String leadSourceCategory = dataSet.get("leadSourceCategory");
			String leadSourceDetails = dataSet.get("leadSourceDetails");

			fc.utobj().printTestStep("Add a Lead for this user");
			Map<String, String> leadInfo = new HashMap<String, String>();
			leadInfo.put("firstName", firstName);
			leadInfo.put("lastName", lastName);
			leadInfo.put("country", country);
			leadInfo.put("state", state);
			leadInfo.put("email", emailId);
			leadInfo.put("leadOwner", leadOwner);
			leadInfo.put("leadSourceCategory", leadSourceCategory);
			leadInfo.put("leadSourceDetails", leadSourceDetails);
			addlead.addLeadFromSystem(driver, leadInfo);

			fc.utobj().printTestStep("Now go to Admin > Sales > Setup Sales Lead Owners");
			fc.adminpage().adminPage(driver);
			fc.adminpage().openSetupFranchiseSalesLeadOwnersPage(driver);
			fc.utobj().printTestStep("Click on Selected users as Sales Lead Owners");
			fc.utobj().clickElement(driver, pobj.selectedUsersForFS);
			fc.utobj().printTestStep("Select user in Selected Sales Lead Owners");
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver,
							".//select[@name='selectedUsersRight']/option[contains(text(),'"
									+ corpUser.getuserFullName() + "')]"));
			fc.utobj().printTestStep("Try to move this user to Available Users Section.");
			fc.utobj().clickElement(driver, pobj.moveToAvailable);
			fc.utobj().printTestStep("An alert should be thrown that the user should not be moved.");
			try {
				fc.utobj().acceptAlertBox(driver);
			} catch (Exception e) {
				fc.utobj().throwsException("An alert message not thrown that the user should not be moved.");
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "sales", "TC_Sales_SetupSalesLeadOwners_004" }) // 
	@TestCase(createdOn = "2017-08-04", updatedOn = "2017-08-04", testCaseId = "TC_Sales_SetupSalesLeadOwners_004", testCaseDescription = "Verify the Regional User associated with task cannot be moved back to active user list")
	private void verifyRegUserAssociatedWithTask() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("sales", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			AdminSalesSetupLeadOwnersPage pobj = new AdminSalesSetupLeadOwnersPage(driver);
			AddLeadFromAllSources addlead = new AddLeadFromAllSources();
			String regionName = fc.utobj().generateTestData(dataSet.get("regionName"));
			String userNameReg = fc.utobj().generateTestData(dataSet.get("userNameReg"));
			String emailId = "salesautomation@staffex.com";

			fc.utobj().printTestStep("Go to Admin Users > Add Regional user");
			fc.adminpage().adminPage(driver);
			AdminUsersManageRegionalUsersAddRegionalUserPageTest RegUser = new AdminUsersManageRegionalUsersAddRegionalUserPageTest();
			RegUser.addRegionalUser(driver, userNameReg, regionName, emailId);

			String firstName = fc.utobj().generateTestData(dataSet.get("firstName"));
			String lastName = fc.utobj().generateTestData(dataSet.get("lastName"));
			String country = dataSet.get("country");
			String state = dataSet.get("state");
			String leadOwner = userNameReg + " " + userNameReg;
			String leadSourceCategory = dataSet.get("leadSourceCategory");
			String leadSourceDetails = dataSet.get("leadSourceDetails");

			fc.utobj().printTestStep("Add a Lead for this user");
			Map<String, String> leadInfo = new HashMap<String, String>();
			leadInfo.put("firstName", firstName);
			leadInfo.put("lastName", lastName);
			leadInfo.put("country", country);
			leadInfo.put("state", state);
			leadInfo.put("email", emailId);
			leadInfo.put("leadOwner", leadOwner);
			leadInfo.put("leadSourceCategory", leadSourceCategory);
			leadInfo.put("leadSourceDetails", leadSourceDetails);
			addlead.addLeadFromSystem(driver, leadInfo);

			fc.utobj().printTestStep("Now go to Admin > Sales > Setup Sales Lead Owners");
			fc.adminpage().adminPage(driver);
			fc.adminpage().openSetupFranchiseSalesLeadOwnersPage(driver);
			fc.utobj().printTestStep("Click on Selected users as Sales Lead Owners");
			fc.utobj().clickElement(driver, pobj.selectedUsersForFS);
			fc.utobj().printTestStep("Select user in Selected Sales Lead Owners");
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,(".//select[@name='selectedUsersRight']/option[contains(text(),'" + leadOwner + "')]")));
			fc.utobj().printTestStep("Try to move this user to Available Users Section.");
			fc.utobj().clickElement(driver, pobj.moveToAvailable);
			fc.utobj().printTestStep("An alert should be thrown that the user should not be moved.");
			try {
				fc.utobj().acceptAlertBox(driver);
			} catch (Exception e) {
				fc.utobj().throwsException("An alert message not thrown that the user should not be moved.");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "sales", "TC_Sales_SetupSalesLeadOwners_005" }) // ok
	@TestCase(createdOn = "2017-08-04", updatedOn = "2017-08-04", testCaseId = "TC_Sales_SetupSalesLeadOwners_005", testCaseDescription = "Verify the Divisional User associated with task cannot be moved back to active user list")
	private void verifyDivUserAssociatedWithTask() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> config = fc.utobj().readConfigurationFile(FranconnectUtil.path);
		Map<String, String> dataSet = fc.utobj().readTestData("sales", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			AdminSalesSetupLeadOwnersPage pobj = new AdminSalesSetupLeadOwnersPage(driver);
			AddLeadFromAllSources addlead = new AddLeadFromAllSources();
			String divisionName = fc.utobj().generateTestData(dataSet.get("divisionName"));
			String userNameDiv = fc.utobj().generateTestData(dataSet.get("userNameDiv"));
			String emailId = "salesautomation@staffex.com";

			fc.utobj().printTestStep("Go to Admin Users > Add Divisinal user");
			fc.adminpage().adminPage(driver);
			AdminUsersManageDivisionalUsersAddDivisionalUsersPageTest divUser = new AdminUsersManageDivisionalUsersAddDivisionalUsersPageTest();
			String DivUserName = divUser.addDivisionalUser(driver, userNameDiv, divisionName, emailId);

			String firstName = fc.utobj().generateTestData(dataSet.get("firstName"));
			String lastName = fc.utobj().generateTestData(dataSet.get("lastName"));
			String country = dataSet.get("country");
			String state = dataSet.get("state");
			String leadOwner = DivUserName;
			String leadSourceCategory = dataSet.get("leadSourceCategory");
			String leadSourceDetails = dataSet.get("leadSourceDetails");

			fc.utobj().printTestStep("Add a Lead for this user");
			Map<String, String> leadInfo = new HashMap<String, String>();
			leadInfo.put("firstName", firstName);
			leadInfo.put("lastName", lastName);
			leadInfo.put("country", country);
			leadInfo.put("state", state);
			leadInfo.put("email", emailId);
			leadInfo.put("leadOwner", leadOwner);
			leadInfo.put("leadSourceCategory", leadSourceCategory);
			leadInfo.put("leadSourceDetails", leadSourceDetails);
			addlead.addLeadFromSystem(driver, leadInfo);

			fc.utobj().printTestStep("Now go to Admin > Sales > Setup Sales Lead Owners");
			fc.adminpage().adminPage(driver);
			fc.adminpage().openSetupFranchiseSalesLeadOwnersPage(driver);
			fc.utobj().printTestStep("Click on Selected users as Sales Lead Owners");
			fc.utobj().clickElement(driver, pobj.selectedUsersForFS);
			fc.utobj().printTestStep("Select user in Selected Sales Lead Owners");
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,(".//select[@name='selectedUsersRight']/option[contains(text(),'" + DivUserName + "')]")));
			fc.utobj().printTestStep("Try to move this user to Available Users Section.");
			fc.utobj().clickElement(driver, pobj.moveToAvailable);
			fc.utobj().printTestStep("An alert should be thrown that the user should not be moved.");
			try {
				fc.utobj().acceptAlertBox(driver);
			} catch (Exception e) {
				fc.utobj().throwsException("An alert message not thrown that the user should not be moved.");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

}
