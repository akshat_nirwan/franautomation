package com.builds.test.fs;

import java.util.Map;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Test;

import com.builds.test.salesTest.PrimaryInfoTest;
import com.builds.utilities.FranconnectUtil;
import com.builds.utilities.TestCase;

public class Akshat_Test {
	@Test(groups = { "salesfillallleadinfo"})
	@TestCase(createdOn = "2018-01-24", updatedOn = "2018-01-24", testCaseId = "Sales_PrimaryInfo_VerifyAllTabs", testCaseDescription = "Verify submit all tabs")
	private void Sales_PrimaryInfo_VerifyAllTabs() throws Exception {

		FranconnectUtil fc = new FranconnectUtil();
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("sales", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		PrimaryInfoTest pinfo = new PrimaryInfoTest(driver);
		Sales sales = new Sales();
		 LeadManagementTest lm = new LeadManagementTest(driver);
		 Lead ld2 = new Lead();
		 Lead ld = new Lead();
		 VisitTest visitTest = new VisitTest();
		 Visit visit = new Visit();
		 RealEstateTest realEstateTest = new RealEstateTest();
		 RealEstate realEstate = new RealEstate();
		 QualificationDetailsTest qualificationDetailsTest = new
		 QualificationDetailsTest();
		 QualificationDetails qualificationDetails = new
		 QualificationDetails();
		CoApplicantsTest coApplicantTest = new CoApplicantsTest();
		 CoApplicantWithAdditionLead coApplicant = new
		 CoApplicantWithAdditionLead();
		 CoApplicantWithoutSeperateLead additionalContact = new
		 CoApplicantWithoutSeperateLead();
		 Compliance compliance = new Compliance();
		 ComplianceTest complianceTest = new ComplianceTest();
		 PersonalProfile personalProfile = new PersonalProfile();
		 PersonalProfileTest personalProfileTest = new PersonalProfileTest();
		 Documents documents = new Documents();
		 DocumentsTest documentsTest = new DocumentsTest();

		Broker brokers = new Broker();
		BrokersTest brokersTest = new BrokersTest();

		try {

			driver = fc.loginpage().login(driver);
			fc.commonMethods().getModules().clickSalesModule(driver);
			 sales.leadManagement(driver);
			 lm.addLeadAndSave(fc.sales().sales_common().fillDefaultValue_LeadDetails(ld2));
			
			 sales.leadManagement(driver);
			 lm.addLeadAndSave(fc.sales().sales_common().fillDefaultValue_LeadDetails(ld));
			
			 pinfo.clickOnVisitTab();
			 fc.sales().sales_common().fillDefaultValue_Visit_FullInfo(visit);
			 visitTest.fillAndSubmitVisit(driver, visit);
			 visitTest.validateVisit(driver, visit);
			
			
			 pinfo.clickOnRealEstateTab();
			 fc.sales().sales_common().fillDefaultValue_RealEstate_FullInfo(realEstate);
			 realEstateTest.fillAndSubmitRealEstate(driver, realEstate);
			 realEstateTest.validateRealEstate(driver, realEstate);
			
			 pinfo.clickOnQualificationDetailsTab();
			 fc.sales().sales_common().fillDefaultValue_QualificationDetails_FullInfo(qualificationDetails);
			 qualificationDetailsTest.fillAndSubmitQualificationDetails(driver,
			 qualificationDetails);
			 //TODO : Qualification Details validation
			 pinfo.clickOnPrimaryInfoTab();
			 pinfo.verify_TextUnder_ActivityHistory("Lead Qualification Details Added");
			
			
			 pinfo.clickOnCo_ApplicantTab();
			
			 fc.sales().sales_common().fillDefaultValue_Coapplicant_AdditionalContactWithoutAddingSeparateLead_FullInfo(additionalContact);
			 coApplicantTest.fillAndSubmitCoApplicantsWithoutSeperateLead(driver,
			 additionalContact);
			 coApplicantTest.verifyCoapplicantWithoutSeparateLead(driver,
			 additionalContact);
			
			 fc.sales().sales_common().fillDefaultValue_Coapplicant_AdditionalContactWithAdditionOf_ExistingLead(coApplicant);
			 coApplicant.setCoApplicantName(ld2.getLeadFullName());
			 coApplicantTest.fillAndSubmitCoApplicantsWithAddition(driver,
			 coApplicant);
			 coApplicantTest.verifyCoapplicantWithAddition(driver,
			 coApplicant);
			
			 fc.sales().sales_common().fillDefaultValue_Coapplicant_AdditionalContactWithAdditionOf_NewLead(coApplicant);
			 coApplicantTest.fillAndSubmitCoApplicantsWithAddition(driver,
			 coApplicant);
			 coApplicantTest.verifyCoapplicantWithAddition(driver,
			 coApplicant);
			
			
			 pinfo.clickOnComplianceTab();
			 fc.sales().sales_common().fillDefaultValue_Compliance_FullInfo(compliance);
			 complianceTest.fillAndSubmitComplianceInfo(driver, compliance);
			 complianceTest.validateComplianceInfo(driver, compliance);
			
			 pinfo.clickOnPersonalProfileTab();
			 fc.sales().sales_common().fillDefaultValue_PersonalProfile_FullInfo(personalProfile);
			 personalProfileTest.fillAndSubmitPersonalProfile(driver,
			 personalProfile);
			 //TODO : Personal Profile tab validation
			 pinfo.clickOnPrimaryInfoTab();
			 pinfo.verify_TextUnder_ActivityHistory("Lead Personal Profile Added");
			
			 pinfo.clickOnDocumentsTab();
			 documents.setNumberOfDocuments(4);
			 documents.setDocumentTitle("document");
			 documents.setUploadDocument("0.pdf");
			 documentsTest.fillDocuments(driver, documents);
			 documentsTest.validateDocuments(driver, documents);

			// add and print verify broker
			sales.brokers(driver);
			fc.sales().sales_common().fillDefaultValue_Brokers_FullInfo(brokers);
			brokersTest.addBroker(driver, brokers);
			 brokersTest.printVerifyBroker_fromBottomPrintButton(driver,
			 brokers);
			brokersTest.logATask_fromRightAction_Broker(driver, brokers);

			// add and delete broker from bottom delete button
			/*
			 * sales.brokers(driver);
			 * fc.sales().sales_common().fillDefaultValue_Brokers_FullInfo(
			 * brokers); brokersTest.addBroker(driver, brokers);
			 * brokersTest.deleteBroker_fromBottom_DeleteButton(driver,
			 * brokers);
			 */

			// add and delete broker from top Actions button
			/*
			 * sales.brokers(driver);
			 * fc.sales().sales_common().fillDefaultValue_Brokers_FullInfo(
			 * brokers); brokersTest.addBroker(driver, brokers);
			 * brokersTest.deleteBroker_fromTopActions(driver, brokers);
			 */

			// add and archive broker from bottom button
			/*
			 * sales.brokers(driver);
			 * fc.sales().sales_common().fillDefaultValue_Brokers_FullInfo(
			 * brokers); brokersTest.addBroker(driver, brokers);
			 * brokersTest.archiveBroker_fromBottom_ArchiveButton(driver,
			 * brokers);
			 */

			// add and archive broker from top Actions
			/*
			 * sales.brokers(driver);
			 * fc.sales().sales_common().fillDefaultValue_Brokers_FullInfo(
			 * brokers); brokersTest.addBroker(driver, brokers);
			 * brokersTest.archiveBroker_fromTopActions(driver, brokers);
			 */
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}
}
