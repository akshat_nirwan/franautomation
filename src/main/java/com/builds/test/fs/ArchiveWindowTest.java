package com.builds.test.fs;

import org.openqa.selenium.WebDriver;

import com.builds.uimaps.fs.ArchiveLeadWindowUI;
import com.builds.utilities.FranconnectUtil;

public class ArchiveWindowTest {

	void addNotes_ClickArchive(WebDriver driver, String notes) throws Exception {
		FranconnectUtil fc = new FranconnectUtil();
		ArchiveLeadWindowUI ui = new ArchiveLeadWindowUI(driver);

		fc.commonMethods().switch_cboxIframe_frameId(driver);

		fc.utobj().printTestStep("Enter Remarks");
		fc.utobj().sendKeys(driver, ui.notes, notes);

		clickArchiveButton(driver);
		fc.commonMethods().Click_Close_Input_ByValue(driver);
		fc.utobj().switchFrameToDefault(driver);
	}

	private void clickArchiveButton(WebDriver driver) throws Exception {
		FranconnectUtil fc = new FranconnectUtil();
		ArchiveLeadWindowUI ui = new ArchiveLeadWindowUI(driver);

		fc.utobj().printTestStep("Click Archive");
		fc.utobj().clickElement(driver, ui.arvhiveButton);
	}

}
