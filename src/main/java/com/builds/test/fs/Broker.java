package com.builds.test.fs;

public class Broker {

	private String brokerType;
	private String firstName;
	private String lastName;
	private String address1;
	private String address2;
	private String city;
	private String country;
	private String zipPostalCode;
	private String stateProvince;
	private String homePhone;
	private String workPhone;
	private String fax;
	private String mobile;
	private String bestTimeToContact;
	private String primaryPhoneToCall;
	private String email;
	private String agency;
	private String priority;
	private String comments;
	private String associateWithCampaign;

	public String getAssociateWithCampaign() {
		return associateWithCampaign;
	}

	public void setAssociateWithCampaign(String associateWithCampaign) {
		this.associateWithCampaign = associateWithCampaign;
	}

	public String getBrokerType() {
		return brokerType;
	}

	public void setBrokerType(String brokerType) {
		this.brokerType = brokerType;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getAddress1() {
		return address1;
	}

	public void setAddress1(String address1) {
		this.address1 = address1;
	}

	public String getAddress2() {
		return address2;
	}

	public void setAddress2(String address2) {
		this.address2 = address2;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getZipPostalCode() {
		return zipPostalCode;
	}

	public void setZipPostalCode(String zipPostalCode) {
		this.zipPostalCode = zipPostalCode;
	}

	public String getStateProvince() {
		return stateProvince;
	}

	public void setStateProvince(String stateProvince) {
		this.stateProvince = stateProvince;
	}

	public String getHomePhone() {
		return homePhone;
	}

	public void setHomePhone(String homePhone) {
		this.homePhone = homePhone;
	}

	public String getWorkPhone() {
		return workPhone;
	}

	public void setWorkPhone(String workPhone) {
		this.workPhone = workPhone;
	}

	public String getFax() {
		return fax;
	}

	public void setFax(String fax) {
		this.fax = fax;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getBestTimeToContact() {
		return bestTimeToContact;
	}

	public void setBestTimeToContact(String bestTimeToContact) {
		this.bestTimeToContact = bestTimeToContact;
	}

	public String getPrimaryPhoneToCall() {
		return primaryPhoneToCall;
	}

	public void setPrimaryPhoneToCall(String primaryPhoneToCall) {
		this.primaryPhoneToCall = primaryPhoneToCall;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getAgency() {
		return agency;
	}

	public void setAgency(String agency) {
		this.agency = agency;
	}

	public String getPriority() {
		return priority;
	}

	public void setPriority(String priority) {
		this.priority = priority;
	}

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public String getBrokerFullName() {
		String firstName = getFirstName();
		String lastName = getLastName();

		if (firstName == null) {
			firstName = "";
		}
		if (lastName == null) {
			lastName = "";
		}
		return (firstName + " " + lastName).trim();
	}

}
