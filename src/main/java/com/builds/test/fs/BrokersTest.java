package com.builds.test.fs;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.builds.uimaps.fs.BrokersUI;
import com.builds.utilities.FranconnectUtil;

class BrokersTest {

	FranconnectUtil fc = new FranconnectUtil();

	void addBroker(WebDriver driver, Broker broker) throws Exception {
		FranconnectUtil fc = new FranconnectUtil();
		BrokersUI bui = new BrokersUI(driver);

		fc.utobj().printTestStep("Adding a Broker");

		fc.utobj().clickElement(driver, bui.addBrokersBtn);

		// Contact Information

		if (broker.getBrokerType() != null) {
			fc.utobj().selectDropDown(driver, bui.brokerType, broker.getBrokerType());
		}

		if (broker.getFirstName() != null) {
			fc.utobj().sendKeys(driver, bui.firstName, broker.getFirstName());
		}

		if (broker.getLastName() != null) {
			fc.utobj().sendKeys(driver, bui.lastName, broker.getLastName());
		}

		if (broker.getAddress1() != null) {
			fc.utobj().sendKeys(driver, bui.address1, broker.getAddress1());
		}

		if (broker.getAddress2() != null) {
			fc.utobj().sendKeys(driver, bui.address2, broker.getAddress2());
		}

		if (broker.getCity() != null) {
			fc.utobj().sendKeys(driver, bui.city, broker.getCity());
		}

		if (broker.getCountry() != null) {
			fc.utobj().selectDropDown(driver, bui.country, broker.getCountry());
		}

		if (broker.getZipPostalCode() != null) {
			fc.utobj().sendKeys(driver, bui.zipPostalCode, broker.getZipPostalCode());
		}

		if (broker.getStateProvince() != null) {
			fc.utobj().selectDropDown(driver, bui.stateProvince, broker.getStateProvince());
		}

		if (broker.getHomePhone() != null) {
			fc.utobj().sendKeys(driver, bui.homePhone, broker.getHomePhone());
		}

		if (broker.getWorkPhone() != null) {
			fc.utobj().sendKeys(driver, bui.workPhone, broker.getWorkPhone());
		}

		if (broker.getFax() != null) {
			fc.utobj().sendKeys(driver, bui.fax, broker.getFax());
		}

		if (broker.getMobile() != null) {
			fc.utobj().sendKeys(driver, bui.mobile, broker.getMobile());
		}

		if (broker.getBestTimeToContact() != null) {
			fc.utobj().sendKeys(driver, bui.bestTimeToContact, broker.getBestTimeToContact());
		}

		if (broker.getPrimaryPhoneToCall() != null) {
			fc.utobj().selectDropDown(driver, bui.primaryPhoneToCall, broker.getPrimaryPhoneToCall());
		}

		if (broker.getEmail() != null) {
			fc.utobj().sendKeys(driver, bui.email, broker.getEmail());
		}

		if (broker.getAgency() != null) {
			fc.utobj().selectDropDown(driver, bui.agency, broker.getAgency());
		}

		if (broker.getPriority() != null) {
			fc.utobj().selectDropDown(driver, bui.priority, broker.getPriority());
		}

		if (broker.getComments() != null) {
			fc.utobj().sendKeys(driver, bui.comments, broker.getComments());
		}

		// Email Campaigns

		if (broker.getAssociateWithCampaign() != null) {
			fc.utobj().selectDropDown(driver, bui.associateWithCampaign, broker.getAssociateWithCampaign());
		}

		fc.utobj().clickElement(driver, bui.submitBtn);

	}

	void modifyBroker(WebDriver driver, Broker broker) throws Exception {
		Sales sales = new Sales();
		BrokersUI bui = new BrokersUI(driver);

		fc.utobj().printTestStep("Modifying a Broker");

		sales.brokers(driver);
		searchBroker(driver, broker);
		fc.utobj().singleActionIcon(driver, "Modify");

		fc.utobj().sendKeys(driver, bui.firstName, broker.getFirstName().concat("modified"));
		fc.utobj().sendKeys(driver, bui.lastName, broker.getLastName().concat("modified"));
		fc.utobj().clickElement(driver, bui.submitBtn);

		broker.setFirstName(broker.getFirstName().concat("modified"));
		broker.setLastName(broker.getLastName().concat("modified"));

		searchBroker(driver, broker);
		fc.utobj().assertLinkText(driver, broker.getBrokerFullName());

	}

	void deleteBroker_fromBottom_DeleteButton(WebDriver driver, Broker broker) throws Exception {
		Sales sales = new Sales();
		BrokersUI brokersUI = new BrokersUI(driver);

		fc.utobj().printTestStep("Deleting a Broker from bottom Delete button");

		sales.brokers(driver);
		searchAndSelectBroker(driver, broker);
		fc.utobj().clickElement(driver, brokersUI.deleteBtn);
		fc.utobj().acceptAlertBox(driver);

		// Validating that broker has been deleted successfully
		fc.utobj().printTestStep("Validating broker deletion");
		sales.brokers(driver);
		searchBroker(driver, broker);
		fc.utobj().assertLinkTextNotPresent(driver, broker.getBrokerFullName());

	}

	void deleteBroker_fromTopActions(WebDriver driver, Broker broker) throws Exception {
		Sales sales = new Sales();
		BrokersUI brokersUI = new BrokersUI(driver);

		fc.utobj().printTestStep("Deleting a Broker from Top Actions");

		sales.brokers(driver);
		searchAndSelectBroker(driver, broker);
		clickLeftActionsMenu_Click_deleteBrokers(driver);

		// Validating that broker has been deleted successfully
		fc.utobj().printTestStep("Validating broker deletion");
		sales.brokers(driver);
		searchBroker(driver, broker);
		fc.utobj().assertLinkTextNotPresent(driver, broker.getBrokerFullName());

	}

	void archiveBroker_fromBottom_ArchiveButton(WebDriver driver, Broker broker) throws Exception {
		Sales sales = new Sales();
		BrokersUI brokersUI = new BrokersUI(driver);

		fc.utobj().printTestStep("Archiving a Broker from bottom Archive button");

		sales.brokers(driver);
		searchAndSelectBroker(driver, broker);
		fc.utobj().clickElement(driver, brokersUI.archiveBtn);
		fc.utobj().acceptAlertBox(driver);

		// Validation on Brokers tab
		searchBroker(driver, broker);
		fc.utobj().assertLinkTextNotPresent(driver, broker.getBrokerFullName());

		// Validation on Archived Brokers tab
		fc.utobj().clickElement(driver, brokersUI.archivedBrokersTab);
		searchBroker(driver, broker);
		fc.utobj().assertLinkText(driver, broker.getBrokerFullName());

	}

	void archiveBroker_fromTopActions(WebDriver driver, Broker broker) throws Exception {
		Sales sales = new Sales();
		BrokersUI brokersUI = new BrokersUI(driver);

		fc.utobj().printTestStep("Archiving a Broker from top Actions");

		sales.brokers(driver);
		searchAndSelectBroker(driver, broker);
		// clickLeftActionsMenu_Click_archiveBrokers(driver);
		clickLeftActionsMenu_Click_archiveBrokers(driver);

		// Validation on Brokers tab
		searchBroker(driver, broker);
		fc.utobj().assertLinkTextNotPresent(driver, broker.getBrokerFullName());

		// Validation on Archived Brokers tab
		fc.utobj().clickElement(driver, brokersUI.archivedBrokersTab);
		searchBroker(driver, broker);
		fc.utobj().assertLinkText(driver, broker.getBrokerFullName());

	}

	private void searchBroker(WebDriver driver, Broker broker) throws Exception {
		BrokersUI brokersUI = new BrokersUI(driver);
		fc.utobj().selectValFromMultiSelect(driver, brokersUI.brokerAgencySearch_MulitSelect, broker.getAgency());
		fc.utobj().sendKeys(driver, brokersUI.brokerNameSearch, broker.getBrokerFullName());
		fc.utobj().clickElement(driver, brokersUI.searchBtn);
	}

	private void searchAndSelectBroker(WebDriver driver, Broker broker) throws Exception {
		BrokersUI brokersUI = new BrokersUI(driver);
		searchBroker(driver, broker);
		fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
				brokersUI.selectCheckBox(broker.getFirstName(), broker.getLastName())));
	}

	void logATask_fromRightAction_Broker(WebDriver driver, Broker broker) throws Exception {
		Sales sales = new Sales();
		BrokersUI bui = new BrokersUI(driver);
		TaskTest taskTest = new TaskTest();
		Sales_Common salesCommon = new Sales_Common();
		Task task = new Task();

		fc.utobj().printTestStep("Log a Task for Broker from right actions button");

		sales.brokers(driver);
		searchAndSelectBroker(driver, broker);
		fc.utobj().singleActionIcon(driver, "Log a Task");
		fc.commonMethods().switch_cboxIframe_frameId(driver);
		salesCommon.fillDefaultValue_TaskDetails(task);
		taskTest.fillTaskAndClickCreate(driver, task);
		sales.tasks(driver);
		fc.utobj().assertLinkText(driver, task.getSubject());
		fc.utobj().assertLinkText(driver, broker.getBrokerFullName());
	}

	// ACTION MENU METHODS //
	void clickLeftActionsMenu_Click_archiveBrokers(WebDriver driver) throws Exception {
		selectActionsFromLeftActionMenu(driver, "Archive  Brokers");
		fc.utobj().acceptAlertBox(driver);
		// fc.CommonMethods().switch_cboxIframe_frameId(driver);
	}

	void clickLeftActionsMenu_Click_deleteBrokers(WebDriver driver) throws Exception {
		selectActionsFromLeftActionMenu(driver, "Delete Brokers");
		fc.utobj().acceptAlertBox(driver);
	}

	void clickLeftActionsMenu_Click_logATask(WebDriver driver) throws Exception {
		selectActionsFromLeftActionMenu(driver, "Log a Task");
		fc.utobj().acceptAlertBox(driver);
	}

	void clickLeftActionsMenu_Click_sendEmail(WebDriver driver) throws Exception {
		selectActionsFromLeftActionMenu(driver, "Send Email");
		fc.utobj().acceptAlertBox(driver);
	}

	void clickLeftActionsMenu_Click_print(WebDriver driver) throws Exception {
		selectActionsFromLeftActionMenu(driver, "Print");
		fc.utobj().acceptAlertBox(driver);
	}

	void clickLeftActionsMenu_Click_exportAsExcel(WebDriver driver) throws Exception {
		selectActionsFromLeftActionMenu(driver, "Export As Excel");
		fc.utobj().acceptAlertBox(driver);
	}

	private void clickLeftAction(WebDriver driver) throws Exception {
		BrokersUI brokersUI = new BrokersUI(driver);
		fc.utobj().printTestStep("Click Left Actions");
		fc.utobj().clickElement(driver, brokersUI.ActionsMenu_Left_Input_ByValue);
	}

	private void selectActionsFromLeftActionMenu(WebDriver driver, String actionName) throws Exception {

		clickLeftAction(driver);

		BrokersUI brokersUI = new BrokersUI(driver);
		fc.utobj().printTestStep("Select : " + actionName + ", from Left Action Menu");
		List<WebElement> actionMenu = brokersUI.actionsIn_LeftActionMenu_ByXpath;

		for (WebElement menu : actionMenu) {
			System.out.println("Menu Name : " + menu.getText().trim());
			if (actionName.equalsIgnoreCase(menu.getText().trim())) {
				fc.utobj().clickElement(driver, menu);
				break;
			}
		}
	}

	void printVerifyBroker_fromBottomPrintButton(WebDriver driver, Broker broker) throws Exception {

		Sales sales = new Sales();
		BrokersUI brokersUI = new BrokersUI(driver);
		SalesCommonFunctions salesCommon = new SalesCommonFunctions();

		fc.utobj().printTestStep("Click on print button and verify for brokers");
		sales.brokers(driver);
		searchAndSelectBroker(driver, broker);
		fc.utobj().clickElement(driver, brokersUI.printBtn);
		salesCommon.VerifyPrintPreview(driver, broker.getBrokerFullName());
	}

	// public void clickLeftActionsMenu_Click_ArchiveBrokers_ClickOK(WebDriver
	// driver) throws Exception
	// {
	// selectActionsFromLeftActionMenu(driver, "Archive Brokers");
	// fc.CommonMethods().switch_cboxIframe_frameId(driver);
	// fc.CommonMethods().Click_OK_Input_ByValue(driver);
	// }

}
