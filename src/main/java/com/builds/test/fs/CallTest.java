package com.builds.test.fs;

import org.openqa.selenium.WebDriver;

import com.builds.uimaps.fs.CallUI;
import com.builds.utilities.FranconnectUtil;

public class CallTest {

	private void fillCallDetails(WebDriver driver, Call call) throws Exception {
		FranconnectUtil fc = new FranconnectUtil();
		CallUI ui = new CallUI(driver);

		fc.utobj().printTestStep("Fill Call Details");

		if (call.getSubject() != null) {
			fc.utobj().sendKeys(driver, ui.subject, call.getSubject());
		}

		if (call.getDate() != null) {
			fc.utobj().sendKeys(driver, ui.date, call.getDate());
		}

		if (call.getTimeHH() != null) {
			fc.utobj().selectDropDownByPartialText(driver, ui.timeHH, call.getTimeHH());
		}

		if (call.getTimeMM() != null) {
			fc.utobj().selectDropDownByPartialText(driver, ui.timeMM, call.getTimeMM());
		}

		if (call.getTimeAMPM() != null) {
			fc.utobj().selectDropDownByPartialText(driver, ui.timeAMPM, call.getTimeAMPM());
		}

		if (call.getCallStatus() != null) {
			fc.utobj().selectDropDownByPartialText(driver, ui.callStatus, call.getCallStatus());
		}

		if (call.getCommunicationType() != null) {
			fc.utobj().selectDropDownByPartialText(driver, ui.communicationtype, call.getCommunicationType());
		}

		if (call.getComments() != null) {
			fc.utobj().sendKeys(driver, ui.comments, call.getComments());
		}
	}

	private void clickAdd(WebDriver driver) throws Exception {
		FranconnectUtil fc = new FranconnectUtil();

		fc.utobj().printTestStep("Click on Add Button");
		CallUI ui = new CallUI(driver);
		fc.utobj().clickElement(driver, ui.addButton);
	}

	public void fill_And_Add_Call_Details_ScheduleTask_No(WebDriver driver, Call call) throws Exception // confirm
																										// should
																										// mention
																										// yes
																										// or
																										// no
																										// only.
	{
		FranconnectUtil fc = new FranconnectUtil();
		fillCallDetails(driver, call);
		clickAdd(driver);
		ConfirmScheduleTask(driver, "no");
		fc.utobj().switchFrameToDefault(driver);
	}

	public void ConfirmScheduleTask(WebDriver driver, String confirm) throws Exception {
		FranconnectUtil fc = new FranconnectUtil();
		CallUI ui = new CallUI(driver);

		fc.utobj().printTestStep("Click Yes/No to Schedule Task : " + confirm);

		if ("yes".equalsIgnoreCase(confirm)) {
			fc.commonMethods().Click_Yes_Input_ByValue(driver);
		} else {
			fc.commonMethods().Click_No_Input_ByValue(driver);
		}
	}

	public Call validateCallDetails(WebDriver driver) throws Exception {
		FranconnectUtil fc = new FranconnectUtil();
		CallUI ui = new CallUI(driver);
		Call call = new Call();

		fc.utobj().printTestStep("Validating the Call Details");

		call.setSubject(fc.utobj().getTextTxtFld(driver, ui.subject));
		call.setDate(fc.utobj().getTextTxtFld(driver, ui.date));
		call.setTimeHH(fc.utobj().getSelectedOptioninDropDown(driver, ui.timeHH));
		call.setTimeMM(fc.utobj().getSelectedOptioninDropDown(driver, ui.timeMM));
		call.setTimeAMPM(fc.utobj().getSelectedOptioninDropDown(driver, ui.timeAMPM));
		call.setCallStatus(fc.utobj().getSelectedOptioninDropDown(driver, ui.callStatus));
		call.setCommunicationType(fc.utobj().getSelectedOptioninDropDown(driver, ui.communicationtype));
		call.setComments(fc.utobj().getTextTxtFld(driver, ui.comments));

		return call;
	}

}
