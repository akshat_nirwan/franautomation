package com.builds.test.fs;

import org.openqa.selenium.WebDriver;

import com.builds.uimaps.fs.CampaignCenterUI;
import com.builds.utilities.FranconnectUtil;

class CampaignCenterTest {
	FranconnectUtil fc = new FranconnectUtil();

	private void clickCreate_Ico(WebDriver driver) throws Exception {
		CampaignCenterUI ui = new CampaignCenterUI(driver);
		fc.utobj().printTestStep("Click Create");
		fc.utobj().clickElement(driver, ui.clickCreate_IconXpath);
	}

	public void clickCreateAndCreateCampaign_Link(WebDriver driver) throws Exception {
		CampaignCenterUI ui = new CampaignCenterUI(driver);
		clickCreate_Ico(driver);
		fc.utobj().printTestStep("Click Campaign");
		fc.utobj().clickElement(driver, ui.CreateCampaign_Link);
		fc.commonMethods().switch_frameClass_newLayoutcboxIframe(driver);
	}

	public void clickCreateAndCreateTemplate_Link(WebDriver driver) throws Exception {
		CampaignCenterUI ui = new CampaignCenterUI(driver);
		clickCreate_Ico(driver);
		fc.utobj().printTestStep("Click Template");
		fc.utobj().clickElement(driver, ui.CreateTemplate_Link);
		fc.commonMethods().switch_frameClass_newLayoutcboxIframe(driver);
	}

	public void clickCreateAndCreateRecipientsGroup_Link(WebDriver driver) throws Exception {
		CampaignCenterUI ui = new CampaignCenterUI(driver);
		clickCreate_Ico(driver);
		fc.utobj().printTestStep("Click Recipients Group");
		fc.utobj().clickElement(driver, ui.CreateRecipientsGroup_Link);
		fc.commonMethods().switch_frameClass_newLayoutcboxIframe(driver);
	}

	public void clickCreateAndCreateWorkflow_Link(WebDriver driver) throws Exception {
		CampaignCenterUI ui = new CampaignCenterUI(driver);
		clickCreate_Ico(driver);
		fc.utobj().printTestStep("Click Workflow");
		fc.utobj().clickElement(driver, ui.CreateWorkflow_Link);
		fc.commonMethods().switch_frameClass_newLayoutcboxIframe(driver);
	}

	public void clickCampaign_Button(WebDriver driver) throws Exception {
		CampaignCenterUI ui = new CampaignCenterUI(driver);
		fc.utobj().printTestStep("Click Campaign Button");
		fc.utobj().clickElement(driver, ui.CreateCampaign_Link);
		fc.commonMethods().switch_frameClass_newLayoutcboxIframe(driver);
	}

	public void clickCreateTemplate_Button(WebDriver driver) throws Exception {
		CampaignCenterUI ui = new CampaignCenterUI(driver);
		fc.utobj().printTestStep("Click Template Button");
		fc.utobj().clickElement(driver, ui.CreateTemplate_Link);
		fc.commonMethods().switch_frameClass_newLayoutcboxIframe(driver);
	}

	public void clickCreateGroup_Button(WebDriver driver) throws Exception {
		CampaignCenterUI ui = new CampaignCenterUI(driver);
		fc.utobj().printTestStep("Click Group Button");
		fc.utobj().clickElement(driver, ui.CreateRecipientsGroup_Link);
		fc.commonMethods().switch_frameClass_newLayoutcboxIframe(driver);
	}

	public void clickCreateWorkflow_Button(WebDriver driver) throws Exception {
		CampaignCenterUI ui = new CampaignCenterUI(driver);
		fc.utobj().printTestStep("Click Workflow Button");
		fc.utobj().clickElement(driver, ui.CreateWorkflow_Link);
		fc.commonMethods().switch_frameClass_newLayoutcboxIframe(driver);
	}

	/*
	 * Left Action Menu Commands
	 */

	public void clickLeftActions(WebDriver driver) throws Exception {
		CampaignCenterUI ui = new CampaignCenterUI(driver);
		fc.utobj().printTestStep("Click on Left Actions Menu");
		fc.utobj().clickElement(driver, ui.leftActionMenu);
	}

	public void clickTemplate_LeftActions(WebDriver driver) throws Exception {
		CampaignCenterUI ui = new CampaignCenterUI(driver);
		clickLeftActions(driver);
		fc.utobj().printTestStep("Click Templates");
		fc.utobj().clickElement(driver, ui.Templates_LinkInLeftAction);
	}

	public void clickDashboard_LeftActions(WebDriver driver) throws Exception {
		CampaignCenterUI ui = new CampaignCenterUI(driver);
		clickLeftActions(driver);
		fc.utobj().printTestStep("Click Dashboard");
		fc.utobj().clickElement(driver, ui.Dashboard_LinkInLeftAction);
	}

	public void clickCampaigns_LeftActions(WebDriver driver) throws Exception {
		CampaignCenterUI ui = new CampaignCenterUI(driver);
		clickLeftActions(driver);
		fc.utobj().printTestStep("Click Campaign");
		fc.utobj().clickElement(driver, ui.Campaigns_LinkInLeftAction);
	}

	public void clickRecipientGroups_LeftActions(WebDriver driver) throws Exception {
		CampaignCenterUI ui = new CampaignCenterUI(driver);
		clickLeftActions(driver);
		fc.utobj().printTestStep("Click Recipients Groups");
		fc.utobj().clickElement(driver, ui.Recipient_Groups_LinkInLeftAction);
	}

}
