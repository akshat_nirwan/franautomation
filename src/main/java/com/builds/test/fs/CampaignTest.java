package com.builds.test.fs;

import org.openqa.selenium.WebDriver;

import com.builds.uimaps.fs.CampaignUI;
import com.builds.utilities.FranconnectUtil;

class CampaignTest {

	public void fillCampaignDetails(WebDriver driver, Campaign campaign) throws Exception {
		FranconnectUtil fc = new FranconnectUtil();
		CampaignUI ui = new CampaignUI(driver);

		fc.utobj().printTestStep("Fill Campaign Details");

		if (campaign.getCampaignName() != null) {
			fc.utobj().sendKeys(driver, ui.campaignName, campaign.getCampaignName());
		}

		if (campaign.getDescription() != null) {
			fc.utobj().sendKeys(driver, ui.Description, campaign.getDescription());
		}

		if (campaign.getAccessibility() != null) {
			fc.utobj().selectDropDown(driver, ui.Accessibility, campaign.getAccessibility());
		}
		
		// Akshat
		if(campaign.getCampaignType() != null)
		{
			if(campaign.getCampaignType().equalsIgnoreCase("Promotional"))
			{
				fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//label[@for='quickCampaign1']"));
			}
			if(campaign.getCampaignType().equalsIgnoreCase("Status Driven"))
			{
				fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//label[@for='quickCampaign2']"));
			}
		}
	}

	public void fillCampaignDetailsAndClickStart(WebDriver driver, Campaign campaign) throws Exception {
		FranconnectUtil fc = new FranconnectUtil();
		CampaignUI ui = new CampaignUI(driver);
		fillCampaignDetails(driver, campaign);
		fc.utobj().printTestStep("Click Start Button");
		fc.utobj().clickElement(driver, ui.Start);
	}
	
	public void fillCampaignDetailsAndClickCancel(WebDriver driver, Campaign campaign) throws Exception
	{
		FranconnectUtil fc = new FranconnectUtil();
		CampaignUI ui = new CampaignUI(driver);
		fc.utobj().printTestStep("Click Cancel Button");
		fc.utobj().clickElement(driver, ui.Cancel);
	}
}
