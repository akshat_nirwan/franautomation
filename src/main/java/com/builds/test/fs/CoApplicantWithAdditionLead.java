package com.builds.test.fs;

public class CoApplicantWithAdditionLead extends Lead {

	private String coApplicantType;

	private String lead;
	private String coApplicantRelationship;
	private String coApplicantName;

	public String getCoApplicantType() {
		return coApplicantType;
	}

	public void setCoApplicantType(String coApplicantType) {
		this.coApplicantType = coApplicantType;
	}

	public String getCoApplicantName() {
		return coApplicantName;
	}

	public void setCoApplicantName(String coApplicantName) {
		this.coApplicantName = coApplicantName;
	}

	public String getCoApplicantRelationship() {
		return coApplicantRelationship;
	}

	public void setCoApplicantRelationship(String coApplicantRelationship) {
		this.coApplicantRelationship = coApplicantRelationship;
	}

	public String getLead() {
		return lead;
	}

	public void setLead(String lead) {
		this.lead = lead;
	}

}
