package com.builds.test.fs;

import org.openqa.selenium.WebDriver;

import com.builds.uimaps.fs.CoApplicantsUI;
import com.builds.utilities.FranconnectUtil;

public class CoApplicantsTest {

	public void fillAndSubmitCoApplicantsWithoutSeperateLead(WebDriver driver, CoApplicantWithoutSeperateLead cawsl)
			throws Exception {
		CoApplicantsUI caui = new CoApplicantsUI(driver);
		FranconnectUtil fc = new FranconnectUtil();

		fc.utobj().printTestStep("Fill Co-Applicants without separate lead Info");

		fc.utobj().clickElement(driver, caui.addCoApplicant_Btn);

		fc.utobj().clickElement(driver, caui.addCoApplicantWithoutSeparateLead_RadioBtn);

		if (cawsl.getFirstName() != null) {
			fc.utobj().sendKeys(driver, caui.firstName, cawsl.getFirstName());
		}

		if (cawsl.getLastName() != null) {
			fc.utobj().sendKeys(driver, caui.lastName, cawsl.getLastName());
		}

		if (cawsl.getCoApplicantRelationship() != null) {
			fc.utobj().selectDropDownByVisibleText(driver, caui.coApplicantRelationshipID,
					cawsl.getCoApplicantRelationship());
		}

		if (cawsl.getPhone() != null) {
			fc.utobj().sendKeys(driver, caui.phone, cawsl.getPhone());
		}

		if (cawsl.getExt() != null) {
			fc.utobj().sendKeys(driver, caui.ext, cawsl.getExt());
		}

		if (cawsl.getFax() != null) {
			fc.utobj().sendKeys(driver, caui.fax, cawsl.getFax());
		}

		if (cawsl.getEmailID() != null) {
			fc.utobj().sendKeys(driver, caui.emailID, cawsl.getEmailID());
		}

		if (cawsl.getAddress() != null) {
			fc.utobj().sendKeys(driver, caui.address, cawsl.getAddress());
		}

		if (cawsl.getCity() != null) {
			fc.utobj().sendKeys(driver, caui.city, cawsl.getCity());
		}

		if (cawsl.getCountry() != null) {
			fc.utobj().selectDropDownByVisibleText(driver, caui.country, cawsl.getCountry());
		}

		if (cawsl.getStateID() != null) {
			fc.utobj().selectDropDownByVisibleText(driver, caui.stateID, cawsl.getStateID());
		}

		if (cawsl.getZip() != null) {
			fc.utobj().sendKeys(driver, caui.zip, cawsl.getZip());
		}

		fc.utobj().clickElement(driver, caui.addBtn);

	}

	void fillAndSubmitCoApplicantsWithAddition(WebDriver driver, CoApplicantWithAdditionLead caal) throws Exception {
		CoApplicantsUI caui = new CoApplicantsUI(driver);
		FranconnectUtil fc = new FranconnectUtil();
		LeadManagementTest lm = new LeadManagementTest(driver);
		Lead ld = new Lead();
		LeadTest lt = new LeadTest();

		fc.utobj().clickElement(driver, caui.addCoApplicant_Btn);

		fc.utobj().printTestStep("Fill Co-Applicants with addition separate lead Info");
		fc.utobj().clickElement(driver, caui.addCoApplicantWithAdditionLead_RadioBtn);

		if (caal.getLead() != null) {
			if (caal.getLead() == "New Lead") {
				fc.utobj().clickElement(driver, caui.newLead_radioBtn);

				if (caal.getCoApplicantRelationship() != null) {
					fc.utobj().selectDropDownByVisibleText(driver, caui.coApplicantRelationshipID,
							caal.getCoApplicantRelationship());
				}

				lt.fillLeadInfo(driver, fc.sales().sales_common().fillDefaultValue_LeadDetails(ld));
				fc.utobj().clickElement(driver, fc.utobj().getElementByID(driver, "Submit"));

				// fc.utobj().clickElement(driver, caui.addBtn);
			}

			else if (caal.getLead() == "Existing Lead") {
				fc.utobj().clickElement(driver, caui.existingLead_radioBtn);

				if (caal.getCoApplicantName() != null) {
					fc.utobj().sendKeys(driver, caui.coApplicantSearch, caal.getCoApplicantName());
					fc.utobj().clickElement(driver, caui.getXpathOfCoApplicantInListBox(caal.getCoApplicantName()));
				}

				if (caal.getCoApplicantRelationship() != null) {
					fc.utobj().selectDropDownByVisibleText(driver, caui.coApplicantRelationshipID,
							caal.getCoApplicantRelationship());
				}
				fc.utobj().clickElement(driver, caui.addBtn);
			}

		}
	}

	public void verifyCoapplicantWithoutSeparateLead(WebDriver driver, CoApplicantWithoutSeperateLead additionalContact)
			throws Exception {
		FranconnectUtil fc = new FranconnectUtil();
		CoApplicantsUI ui = new CoApplicantsUI(driver);

		fc.utobj().printTestStep("Validating the Additional Contact is added");

		if (fc.utobj().assertLinkText(driver, additionalContact.getCoapplicantFullName()) == false) {
			fc.utobj().throwsException("Coapplicant name not found after addition.");
		}

		clickOnCoapplicantName(driver, additionalContact);

	}

	void verifyCoapplicantWithAddition(WebDriver driver, CoApplicantWithAdditionLead caal) throws Exception {
		FranconnectUtil fc = new FranconnectUtil();
		CoApplicantsUI ui = new CoApplicantsUI(driver);
		LeadManagementTest lm = new LeadManagementTest(driver);
		Lead ld = new Lead();
		LeadTest lt = new LeadTest();

		fc.utobj().printTestStep("Validating the Additional Contact is added");

		if (fc.utobj().assertLinkText(driver, caal.getLeadFullName()) == false) {
			fc.utobj().throwsException("Coapplicant name not found after addition.");
		}

		// clickOnCoapplicantName(driver, caal);

	}

	void clickOnCoapplicantName(WebDriver driver, CoApplicantWithoutSeperateLead additionalContact) throws Exception {
		FranconnectUtil fc = new FranconnectUtil();
		CoApplicantsUI ui = new CoApplicantsUI(driver);

		fc.utobj().printTestStep("Click on Co-Applicant name : " + additionalContact.getCoapplicantFullName());
		fc.utobj().clickLink(driver, additionalContact.getCoapplicantFullName());
	}

}
