package com.builds.test.fs;

class Compliance {

	// Disclosure Requirements
	private String dateOfFDD;
	private String dateFDDReceivedByFranchisee;
	private String dateHoldingPeriodRequirementsExpireForFDD;
	private String versionOfFDD;
	private String ipAddress;
	private String browserType;
	private String dateOfFirstFranchiseePayment;
	private String stateProvinceRegistrationRequired;
	private String dateOfSecondFranchiseePayment;
	private String stateProvinceAddendumRequired;
	private String franchiseCommitteeApproval;

	// Franchise Agreement
	private String faRequestedDate;
	private String faReceivedDate;
	private String dateFranchiseeReceivedAgreements;
	private String dateHoldingPeriodRequirementsAreMet;
	private String dateAgreementSignedByFranchisee;
	private String dateHoldingPeriodRuleOnCheckMet;
	private String dateHoldingPeriodRuleOnAgreementsMet;
	private String versionOfFranchiseeAgreement;

	// Franchise Fee and Signed Agreements Received
	private String amountFranchiseFee;
	private String dateFranchiseFee;

	// Area Development Fee and Signed Agreements Received
	private String amountAreaDevelopmentFee;
	private String dateAreaDevelopmentFee;
	private String adaExecutionDate;
	private String faExecutionDate;

	// Contract Signing Details
	private String contractReceivedSigned;
	private String leaseRiderSigned;
	private String licenseAgreementSigned;
	private String promissoryNoteSigned;
	private String personalCovenantsAgreementSigned;
	private String fddReceiptSigned;
	private String guaranteeSigned;
	private String otherDocumentsSigned;
	private String stateProvinceRequiredAddendumSigned;
	private String handWrittenChanges;
	private String otherAddendumSigned;
	private String proofOfControlOverRealEstate;

	public String getDateOfFDD() {
		return dateOfFDD;
	}

	public void setDateOfFDD(String dateOfFDD) {
		this.dateOfFDD = dateOfFDD;
	}

	public String getDateFDDReceivedByFranchisee() {
		return dateFDDReceivedByFranchisee;
	}

	public void setDateFDDReceivedByFranchisee(String dateFDDReceivedByFranchisee) {
		this.dateFDDReceivedByFranchisee = dateFDDReceivedByFranchisee;
	}

	public String getDateHoldingPeriodRequirementsExpireForFDD() {
		return dateHoldingPeriodRequirementsExpireForFDD;
	}

	public void setDateHoldingPeriodRequirementsExpireForFDD(String dateHoldingPeriodRequirementsExpireForFDD) {
		this.dateHoldingPeriodRequirementsExpireForFDD = dateHoldingPeriodRequirementsExpireForFDD;
	}

	public String getVersionOfFDD() {
		return versionOfFDD;
	}

	public void setVersionOfFDD(String versionOfFDD) {
		this.versionOfFDD = versionOfFDD;
	}

	public String getIpAddress() {
		return ipAddress;
	}

	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}

	public String getBrowserType() {
		return browserType;
	}

	public void setBrowserType(String browserType) {
		this.browserType = browserType;
	}

	public String getDateOfFirstFranchiseePayment() {
		return dateOfFirstFranchiseePayment;
	}

	public void setDateOfFirstFranchiseePayment(String dateOfFirstFranchiseePayment) {
		this.dateOfFirstFranchiseePayment = dateOfFirstFranchiseePayment;
	}

	public String getStateProvinceRegistrationRequired() {
		return stateProvinceRegistrationRequired;
	}

	public void setStateProvinceRegistrationRequired(String stateProvinceRegistrationRequired) {
		this.stateProvinceRegistrationRequired = stateProvinceRegistrationRequired;
	}

	public String getDateOfSecondFranchiseePayment() {
		return dateOfSecondFranchiseePayment;
	}

	public void setDateOfSecondFranchiseePayment(String dateOfSecondFranchiseePayment) {
		this.dateOfSecondFranchiseePayment = dateOfSecondFranchiseePayment;
	}

	public String getStateProvinceAddendumRequired() {
		return stateProvinceAddendumRequired;
	}

	public void setStateProvinceAddendumRequired(String stateProvinceAddendumRequired) {
		this.stateProvinceAddendumRequired = stateProvinceAddendumRequired;
	}

	public String getFranchiseCommitteeApproval() {
		return franchiseCommitteeApproval;
	}

	public void setFranchiseCommitteeApproval(String franchiseCommitteeApproval) {
		this.franchiseCommitteeApproval = franchiseCommitteeApproval;
	}

	public String getFaRequestedDate() {
		return faRequestedDate;
	}

	public void setFaRequestedDate(String faRequestedDate) {
		this.faRequestedDate = faRequestedDate;
	}

	public String getFaReceivedDate() {
		return faReceivedDate;
	}

	public void setFaReceivedDate(String faReceivedDate) {
		this.faReceivedDate = faReceivedDate;
	}

	public String getDateFranchiseeReceivedAgreements() {
		return dateFranchiseeReceivedAgreements;
	}

	public void setDateFranchiseeReceivedAgreements(String dateFranchiseeReceivedAgreements) {
		this.dateFranchiseeReceivedAgreements = dateFranchiseeReceivedAgreements;
	}

	public String getDateHoldingPeriodRequirementsAreMet() {
		return dateHoldingPeriodRequirementsAreMet;
	}

	public void setDateHoldingPeriodRequirementsAreMet(String dateHoldingPeriodRequirementsAreMet) {
		this.dateHoldingPeriodRequirementsAreMet = dateHoldingPeriodRequirementsAreMet;
	}

	public String getDateAgreementSignedByFranchisee() {
		return dateAgreementSignedByFranchisee;
	}

	public void setDateAgreementSignedByFranchisee(String dateAgreementSignedByFranchisee) {
		this.dateAgreementSignedByFranchisee = dateAgreementSignedByFranchisee;
	}

	public String getDateHoldingPeriodRuleOnCheckMet() {
		return dateHoldingPeriodRuleOnCheckMet;
	}

	public void setDateHoldingPeriodRuleOnCheckMet(String dateHoldingPeriodRuleOnCheckMet) {
		this.dateHoldingPeriodRuleOnCheckMet = dateHoldingPeriodRuleOnCheckMet;
	}

	public String getDateHoldingPeriodRuleOnAgreementsMet() {
		return dateHoldingPeriodRuleOnAgreementsMet;
	}

	public void setDateHoldingPeriodRuleOnAgreementsMet(String dateHoldingPeriodRuleOnAgreementsMet) {
		this.dateHoldingPeriodRuleOnAgreementsMet = dateHoldingPeriodRuleOnAgreementsMet;
	}

	public String getVersionOfFranchiseeAgreement() {
		return versionOfFranchiseeAgreement;
	}

	public void setVersionOfFranchiseeAgreement(String versionOfFranchiseeAgreement) {
		this.versionOfFranchiseeAgreement = versionOfFranchiseeAgreement;
	}

	public String getAmountFranchiseFee() {
		return amountFranchiseFee;
	}

	public void setAmountFranchiseFee(String amountFranchiseFee) {
		this.amountFranchiseFee = amountFranchiseFee;
	}

	public String getDateFranchiseFee() {
		return dateFranchiseFee;
	}

	public void setDateFranchiseFee(String dateFranchiseFee) {
		this.dateFranchiseFee = dateFranchiseFee;
	}

	public String getAmountAreaDevelopmentFee() {
		return amountAreaDevelopmentFee;
	}

	public void setAmountAreaDevelopmentFee(String amountAreaDevelopmentFee) {
		this.amountAreaDevelopmentFee = amountAreaDevelopmentFee;
	}

	public String getDateAreaDevelopmentFee() {
		return dateAreaDevelopmentFee;
	}

	public void setDateAreaDevelopmentFee(String dateAreaDevelopmentFee) {
		this.dateAreaDevelopmentFee = dateAreaDevelopmentFee;
	}

	public String getAdaExecutionDate() {
		return adaExecutionDate;
	}

	public void setAdaExecutionDate(String adaExecutionDate) {
		this.adaExecutionDate = adaExecutionDate;
	}

	public String getFaExecutionDate() {
		return faExecutionDate;
	}

	public void setFaExecutionDate(String faExecutionDate) {
		this.faExecutionDate = faExecutionDate;
	}

	public String getContractReceivedSigned() {
		return contractReceivedSigned;
	}

	public void setContractReceivedSigned(String contractReceivedSigned) {
		this.contractReceivedSigned = contractReceivedSigned;
	}

	public String getLeaseRiderSigned() {
		return leaseRiderSigned;
	}

	public void setLeaseRiderSigned(String leaseRiderSigned) {
		this.leaseRiderSigned = leaseRiderSigned;
	}

	public String getLicenseAgreementSigned() {
		return licenseAgreementSigned;
	}

	public void setLicenseAgreementSigned(String licenseAgreementSigned) {
		this.licenseAgreementSigned = licenseAgreementSigned;
	}

	public String getPromissoryNoteSigned() {
		return promissoryNoteSigned;
	}

	public void setPromissoryNoteSigned(String promissoryNoteSigned) {
		this.promissoryNoteSigned = promissoryNoteSigned;
	}

	public String getPersonalCovenantsAgreementSigned() {
		return personalCovenantsAgreementSigned;
	}

	public void setPersonalCovenantsAgreementSigned(String personalCovenantsAgreementSigned) {
		this.personalCovenantsAgreementSigned = personalCovenantsAgreementSigned;
	}

	public String getFddReceiptSigned() {
		return fddReceiptSigned;
	}

	public void setFddReceiptSigned(String fddReceiptSigned) {
		this.fddReceiptSigned = fddReceiptSigned;
	}

	public String getGuaranteeSigned() {
		return guaranteeSigned;
	}

	public void setGuaranteeSigned(String guaranteeSigned) {
		this.guaranteeSigned = guaranteeSigned;
	}

	public String getOtherDocumentsSigned() {
		return otherDocumentsSigned;
	}

	public void setOtherDocumentsSigned(String otherDocumentsSigned) {
		this.otherDocumentsSigned = otherDocumentsSigned;
	}

	public String getStateProvinceRequiredAddendumSigned() {
		return stateProvinceRequiredAddendumSigned;
	}

	public void setStateProvinceRequiredAddendumSigned(String stateProvinceRequiredAddendumSigned) {
		this.stateProvinceRequiredAddendumSigned = stateProvinceRequiredAddendumSigned;
	}

	public String getHandWrittenChanges() {
		return handWrittenChanges;
	}

	public void setHandWrittenChanges(String handWrittenChanges) {
		this.handWrittenChanges = handWrittenChanges;
	}

	public String getOtherAddendumSigned() {
		return otherAddendumSigned;
	}

	public void setOtherAddendumSigned(String otherAddendumSigned) {
		this.otherAddendumSigned = otherAddendumSigned;
	}

	public String getProofOfControlOverRealEstate() {
		return proofOfControlOverRealEstate;
	}

	public void setProofOfControlOverRealEstate(String proofOfControlOverRealEstate) {
		this.proofOfControlOverRealEstate = proofOfControlOverRealEstate;
	}

}
