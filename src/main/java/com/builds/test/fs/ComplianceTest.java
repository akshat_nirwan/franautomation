package com.builds.test.fs;

import org.openqa.selenium.WebDriver;

import com.builds.uimaps.fs.ComplianceUI;
import com.builds.utilities.FranconnectUtil;

class ComplianceTest {

	void fillAndSubmitComplianceInfo(WebDriver driver, Compliance c) throws Exception {
		ComplianceUI cui = new ComplianceUI(driver);
		FranconnectUtil fc = new FranconnectUtil();

		fc.utobj().printTestStep("Fill Compliance Info");

		// Disclosure Requirements
		if (c.getDateOfFDD() != null) {
			fc.utobj().sendKeys(driver, cui.dateOfFDD, c.getDateOfFDD());
		}

		if (c.getDateFDDReceivedByFranchisee() != null) {
			fc.utobj().sendKeys(driver, cui.dateFDDReceivedByFranchisee, c.getDateFDDReceivedByFranchisee());
		}

		if (c.getDateHoldingPeriodRequirementsExpireForFDD() != null) {
			fc.utobj().sendKeys(driver, cui.dateHoldingPeriodRequirementsExpireForFDD,
					c.getDateHoldingPeriodRequirementsExpireForFDD());
		}

		if (c.getVersionOfFDD() != null) {
			fc.utobj().sendKeys(driver, cui.versionOfFDD, c.getVersionOfFDD());
		}

		if (c.getIpAddress() != null) {
			fc.utobj().sendKeys(driver, cui.ipAddress, c.getIpAddress());
		}

		if (c.getBrowserType() != null) {
			fc.utobj().sendKeys(driver, cui.browserType, c.getBrowserType());
		}

		if (c.getDateOfFirstFranchiseePayment() != null) {
			fc.utobj().sendKeys(driver, cui.dateOfFirstFranchiseePayment, c.getDateOfFirstFranchiseePayment());
		}

		if (c.getStateProvinceRegistrationRequired() != null) {
			if (c.getStateProvinceRegistrationRequired().equalsIgnoreCase("Yes")) {
				fc.utobj().clickElement(driver, cui.stateProvinceRegistrationRequiredYes);
			} else if (c.getStateProvinceRegistrationRequired().equalsIgnoreCase("No")) {
				fc.utobj().clickElement(driver, cui.stateProvinceRegistrationRequiredNo);
			} else if (c.getStateProvinceRegistrationRequired().equalsIgnoreCase("NA")) {
				fc.utobj().clickElement(driver, cui.stateProvinceRegistrationRequiredNA);
			}
		}

		if (c.getDateOfSecondFranchiseePayment() != null) {
			fc.utobj().sendKeys(driver, cui.dateOfSecondFranchiseePayment, c.getDateOfSecondFranchiseePayment());
		}

		if (c.getStateProvinceAddendumRequired() != null) {
			if (c.getStateProvinceAddendumRequired().equalsIgnoreCase("Yes")) {
				fc.utobj().clickElement(driver, cui.stateProvinceAddendumRequiredYes);
			} else if (c.getStateProvinceAddendumRequired().equalsIgnoreCase("No")) {
				fc.utobj().clickElement(driver, cui.stateProvinceAddendumRequiredNo);
			} else if (c.getStateProvinceAddendumRequired().equalsIgnoreCase("NA")) {
				fc.utobj().clickElement(driver, cui.stateProvinceAddendumRequiredNA);
			}
		}

		if (c.getFranchiseCommitteeApproval() != null) {
			if (c.getFranchiseCommitteeApproval().equalsIgnoreCase("Yes")) {
				fc.utobj().clickElement(driver, cui.franchiseCommitteeApprovalYes);
			} else if (c.getFranchiseCommitteeApproval().equalsIgnoreCase("No")) {
				fc.utobj().clickElement(driver, cui.franchiseCommitteeApprovalNo);
			} else if (c.getFranchiseCommitteeApproval().equalsIgnoreCase("NA")) {
				fc.utobj().clickElement(driver, cui.franchiseCommitteeApprovalNA);
			}
		}

		// Franchise Agreement
		if (c.getFaRequestedDate() != null) {
			fc.utobj().sendKeys(driver, cui.faRequestedDate, c.getFaRequestedDate());
		}

		if (c.getFaReceivedDate() != null) {
			fc.utobj().sendKeys(driver, cui.faReceivedDate, c.getFaReceivedDate());
		}

		if (c.getDateFranchiseeReceivedAgreements() != null) {
			fc.utobj().sendKeys(driver, cui.dateFranchiseeReceivedAgreements, c.getDateFranchiseeReceivedAgreements());
		}

		if (c.getDateHoldingPeriodRequirementsAreMet() != null) {
			fc.utobj().sendKeys(driver, cui.dateHoldingPeriodRequirementsAreMet,
					c.getDateHoldingPeriodRequirementsAreMet());
		}

		if (c.getDateAgreementSignedByFranchisee() != null) {
			fc.utobj().sendKeys(driver, cui.dateAgreementSignedByFranchisee, c.getDateAgreementSignedByFranchisee());
		}

		if (c.getDateHoldingPeriodRuleOnCheckMet() != null) {
			fc.utobj().sendKeys(driver, cui.dateHoldingPeriodRuleOnCheckMet, c.getDateHoldingPeriodRuleOnCheckMet());
		}

		if (c.getDateHoldingPeriodRuleOnAgreementsMet() != null) {
			fc.utobj().sendKeys(driver, cui.dateHoldingPeriodRuleOnAgreementsMet,
					c.getDateHoldingPeriodRuleOnAgreementsMet());
		}

		if (c.getVersionOfFranchiseeAgreement() != null) {
			fc.utobj().sendKeys(driver, cui.versionOfFranchiseeAgreement, c.getVersionOfFranchiseeAgreement());
		}

		// Franchise Fee and Signed Agreements Received

		if (c.getAmountFranchiseFee() != null) {
			fc.utobj().sendKeys(driver, cui.amountFranchiseFee, c.getAmountFranchiseFee());
		}

		if (c.getDateFranchiseFee() != null) {
			fc.utobj().sendKeys(driver, cui.dateFranchiseFee, c.getDateFranchiseFee());
		}

		// Area Development Fee and Signed Agreements Received

		if (c.getAmountAreaDevelopmentFee() != null) {
			fc.utobj().sendKeys(driver, cui.amountAreaDevelopmentFee, c.getAmountAreaDevelopmentFee());
		}

		if (c.getDateAreaDevelopmentFee() != null) {
			fc.utobj().sendKeys(driver, cui.dateAreaDevelopmentFee, c.getDateAreaDevelopmentFee());
		}

		if (c.getAdaExecutionDate() != null) {
			fc.utobj().sendKeys(driver, cui.adaExecutionDate, c.getAdaExecutionDate());
		}

		if (c.getFaExecutionDate() != null) {
			fc.utobj().sendKeys(driver, cui.faExecutionDate, c.getFaExecutionDate());
		}

		// Contract Signing Details

		if (c.getContractReceivedSigned() != null) {
			if (c.getContractReceivedSigned().equalsIgnoreCase("Yes")) {
				fc.utobj().clickElement(driver, cui.contractReceivedSignedYes);
			} else if (c.getContractReceivedSigned().equalsIgnoreCase("No")) {
				fc.utobj().clickElement(driver, cui.contractReceivedSignedNo);
			} else if (c.getContractReceivedSigned().equalsIgnoreCase("NA")) {
				fc.utobj().clickElement(driver, cui.contractReceivedSignedNA);
			}
		}

		if (c.getLeaseRiderSigned() != null) {
			if (c.getLeaseRiderSigned().equalsIgnoreCase("Yes")) {
				fc.utobj().clickElement(driver, cui.leaseRiderSignedYes);
			} else if (c.getLeaseRiderSigned().equalsIgnoreCase("No")) {
				fc.utobj().clickElement(driver, cui.leaseRiderSignedNo);
			} else if (c.getLeaseRiderSigned().equalsIgnoreCase("NA")) {
				fc.utobj().clickElement(driver, cui.leaseRiderSignedNA);
			}
		}

		if (c.getLicenseAgreementSigned() != null) {
			if (c.getLicenseAgreementSigned().equalsIgnoreCase("Yes")) {
				fc.utobj().clickElement(driver, cui.licenseAgreementSignedYes);
			} else if (c.getLicenseAgreementSigned().equalsIgnoreCase("No")) {
				fc.utobj().clickElement(driver, cui.licenseAgreementSignedNo);
			} else if (c.getLicenseAgreementSigned().equalsIgnoreCase("NA")) {
				fc.utobj().clickElement(driver, cui.licenseAgreementSignedNA);
			}
		}

		if (c.getPromissoryNoteSigned() != null) {
			if (c.getPromissoryNoteSigned().equalsIgnoreCase("Yes")) {
				fc.utobj().clickElement(driver, cui.promissoryNoteSignedYes);
			} else if (c.getPromissoryNoteSigned().equalsIgnoreCase("No")) {
				fc.utobj().clickElement(driver, cui.promissoryNoteSignedNo);
			} else if (c.getPromissoryNoteSigned().equalsIgnoreCase("NA")) {
				fc.utobj().clickElement(driver, cui.promissoryNoteSignedNA);
			}
		}

		if (c.getPersonalCovenantsAgreementSigned() != null) {
			if (c.getPersonalCovenantsAgreementSigned().equalsIgnoreCase("Yes")) {
				fc.utobj().clickElement(driver, cui.personalCovenantsAgreementSignedYes);
			} else if (c.getPersonalCovenantsAgreementSigned().equalsIgnoreCase("No")) {
				fc.utobj().clickElement(driver, cui.personalCovenantsAgreementSignedNo);
			} else if (c.getPersonalCovenantsAgreementSigned().equalsIgnoreCase("NA")) {
				fc.utobj().clickElement(driver, cui.personalCovenantsAgreementSignedNA);
			}
		}

		if (c.getFddReceiptSigned() != null) {
			if (c.getFddReceiptSigned().equalsIgnoreCase("Yes")) {
				fc.utobj().clickElement(driver, cui.fddReceiptSignedYes);
			} else if (c.getFddReceiptSigned().equalsIgnoreCase("No")) {
				fc.utobj().clickElement(driver, cui.fddReceiptSignedNo);
			} else if (c.getFddReceiptSigned().equalsIgnoreCase("NA")) {
				fc.utobj().clickElement(driver, cui.fddReceiptSignedNA);
			}
		}

		if (c.getGuaranteeSigned() != null) {
			if (c.getGuaranteeSigned().equalsIgnoreCase("Yes")) {
				fc.utobj().clickElement(driver, cui.guaranteeSignedYes);
			} else if (c.getGuaranteeSigned().equalsIgnoreCase("No")) {
				fc.utobj().clickElement(driver, cui.guaranteeSignedNo);
			} else if (c.getGuaranteeSigned().equalsIgnoreCase("NA")) {
				fc.utobj().clickElement(driver, cui.guaranteeSignedNA);
			}
		}

		if (c.getOtherDocumentsSigned() != null) {
			if (c.getOtherDocumentsSigned().equalsIgnoreCase("Yes")) {
				fc.utobj().clickElement(driver, cui.otherDocumentsSignedYes);
			} else if (c.getOtherDocumentsSigned().equalsIgnoreCase("No")) {
				fc.utobj().clickElement(driver, cui.otherDocumentsSignedNo);
			} else if (c.getOtherDocumentsSigned().equalsIgnoreCase("NA")) {
				fc.utobj().clickElement(driver, cui.otherDocumentsSignedNA);
			}
		}

		if (c.getStateProvinceRequiredAddendumSigned() != null) {
			if (c.getStateProvinceRequiredAddendumSigned().equalsIgnoreCase("Yes")) {
				fc.utobj().clickElement(driver, cui.stateProvinceRequiredAddendumSignedYes);
			} else if (c.getStateProvinceRequiredAddendumSigned().equalsIgnoreCase("No")) {
				fc.utobj().clickElement(driver, cui.stateProvinceRequiredAddendumSignedNo);
			} else if (c.getStateProvinceRequiredAddendumSigned().equalsIgnoreCase("NA")) {
				fc.utobj().clickElement(driver, cui.stateProvinceRequiredAddendumSignedNA);
			}
		}

		if (c.getHandWrittenChanges() != null) {
			if (c.getHandWrittenChanges().equalsIgnoreCase("Yes")) {
				fc.utobj().clickElement(driver, cui.handWrittenChangesYes);
			} else if (c.getHandWrittenChanges().equalsIgnoreCase("No")) {
				fc.utobj().clickElement(driver, cui.handWrittenChangesNo);
			} else if (c.getHandWrittenChanges().equalsIgnoreCase("NA")) {
				fc.utobj().clickElement(driver, cui.handWrittenChangesNA);
			}
		}

		if (c.getOtherAddendumSigned() != null) {
			if (c.getOtherAddendumSigned().equalsIgnoreCase("Yes")) {
				fc.utobj().clickElement(driver, cui.otherAddendumSignedYes);
			} else if (c.getOtherAddendumSigned().equalsIgnoreCase("No")) {
				fc.utobj().clickElement(driver, cui.otherAddendumSignedNo);
			} else if (c.getOtherAddendumSigned().equalsIgnoreCase("NA")) {
				fc.utobj().clickElement(driver, cui.otherAddendumSignedNA);
			}
		}

		if (c.getProofOfControlOverRealEstate() != null) {
			if (c.getProofOfControlOverRealEstate().equalsIgnoreCase("Yes")) {
				fc.utobj().clickElement(driver, cui.proofOfControlOverRealEstateYes);
			} else if (c.getProofOfControlOverRealEstate().equalsIgnoreCase("No")) {
				fc.utobj().clickElement(driver, cui.proofOfControlOverRealEstateNo);
			} else if (c.getProofOfControlOverRealEstate().equalsIgnoreCase("NA")) {
				fc.utobj().clickElement(driver, cui.proofOfControlOverRealEstateNA);
			}
		}

		fc.utobj().clickElement(driver, cui.saveBtn);

	}

	void validateComplianceInfo(WebDriver driver, Compliance c) throws Exception {
		ComplianceUI cui = new ComplianceUI(driver);
		FranconnectUtil fc = new FranconnectUtil();

		fc.utobj().printTestStep("Validate Compliance Info submit");

		if (!fc.utobj().isElementPresent(driver, cui.modifyComplianceBtn)) {
			fc.utobj().throwsException("Modify button not present on Compliance tab page");
		}
		if (!fc.utobj().isElementPresent(driver, cui.printComplianceBtn)) {
			fc.utobj().throwsException("Print button not present on Compliance tab page");
		}
	}

}