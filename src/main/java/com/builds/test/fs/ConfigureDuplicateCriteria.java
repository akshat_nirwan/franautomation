package com.builds.test.fs;

import org.openqa.selenium.WebDriver;

import com.builds.uimaps.fs.ConfigureDuplicateCriteriaUI;
import com.builds.utilities.FranconnectUtil;

public class ConfigureDuplicateCriteria {

	public void setupDuplicateCriteria1(WebDriver driver) {

	}

	public void clickSubmit(WebDriver driver) throws Exception {
		FranconnectUtil fc = new FranconnectUtil();
		ConfigureDuplicateCriteriaUI ui = new ConfigureDuplicateCriteriaUI(driver);

		fc.utobj().clickElement(driver, ui.Submit_Input_ByValue);
	}

	public void clickCancel(WebDriver driver) throws Exception {
		FranconnectUtil fc = new FranconnectUtil();
		ConfigureDuplicateCriteriaUI ui = new ConfigureDuplicateCriteriaUI(driver);

		fc.utobj().clickElement(driver, ui.Cancel_Input_ByValue);
	}

}
