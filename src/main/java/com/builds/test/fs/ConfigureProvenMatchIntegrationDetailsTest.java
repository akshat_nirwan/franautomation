package com.builds.test.fs;

import org.openqa.selenium.WebDriver;

import com.builds.uimaps.fs.ConfigureProvenMatchIntegrationDetailsUI;
import com.builds.utilities.FranconnectUtil;

public class ConfigureProvenMatchIntegrationDetailsTest {

	public void configureProvenMatchIntegrationDetails_clickSave(WebDriver driver, String franchisorID)
			throws Exception {
		FranconnectUtil fc = new FranconnectUtil();
		ConfigureProvenMatchIntegrationDetailsUI ui = new ConfigureProvenMatchIntegrationDetailsUI(driver);

		fc.utobj().printTestStep("Configure Proven Match Integration Details : " + franchisorID);
		fc.utobj().sendKeys(driver, ui.FranchisorID, franchisorID);
		clickSave(driver);
	}

	private void clickSave(WebDriver driver) throws Exception {
		FranconnectUtil fc = new FranconnectUtil();
		fc.commonMethods().Click_Save_Input_ByValue(driver);
	}

	public void clickBack(WebDriver driver) throws Exception {
		FranconnectUtil fc = new FranconnectUtil();
		fc.commonMethods().Click_Back_Input_ByValue(driver);
	}

}
