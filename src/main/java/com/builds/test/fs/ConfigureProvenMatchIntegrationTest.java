package com.builds.test.fs;

import org.openqa.selenium.WebDriver;

import com.builds.uimaps.fs.ConfigureProvenMatchIntegrationUI;
import com.builds.utilities.FranconnectUtil;

class ConfigureProvenMatchIntegrationTest {

	public void configureProvenMatchIntegration_No_clickSave(WebDriver driver) throws Exception {
		FranconnectUtil fc = new FranconnectUtil();
		ConfigureProvenMatchIntegrationUI ui = new ConfigureProvenMatchIntegrationUI(driver);

		fc.utobj().printTestStep("Configure Proven Match Integration : No");
		fc.utobj().clickElement(driver, ui.EnableProvenMatchIntegration_Radio_OFF);
		clickSave(driver);
	}

	public void configureProvenMatchIntegration_Yes_clickSave(WebDriver driver) throws Exception {
		FranconnectUtil fc = new FranconnectUtil();
		ConfigureProvenMatchIntegrationUI ui = new ConfigureProvenMatchIntegrationUI(driver);
		fc.utobj().printTestStep("Configure Proven Match Integration : Yes");
		fc.utobj().clickElement(driver, ui.EnableProvenMatchIntegration_Radio_ON);
		clickSave(driver);
	}

	private void clickSave(WebDriver driver) throws Exception {
		FranconnectUtil fc = new FranconnectUtil();
		fc.commonMethods().Click_Save_Input_ByValue(driver);
	}

	public void clickBack(WebDriver driver) throws Exception {
		FranconnectUtil fc = new FranconnectUtil();
		fc.commonMethods().Click_Back_Input_ByValue(driver);
	}
}
