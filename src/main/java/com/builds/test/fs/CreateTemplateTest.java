package com.builds.test.fs;

import org.openqa.selenium.WebDriver;

import com.builds.uimaps.fs.CreateTemplateUI;
import com.builds.utilities.FranconnectUtil;

class CreateTemplateTest {
	FranconnectUtil fc = new FranconnectUtil();

	public void clickCodeYourOwn(WebDriver driver) throws Exception {
		CreateTemplateUI ui = new CreateTemplateUI(driver);
		fc.utobj().printTestStep("Click on Option : Code Your Own");
		fc.utobj().clickElement(driver, ui.codeYourOwn);

	}

	public void clickContinueButton(WebDriver driver) throws Exception {
		CreateTemplateUI ui = new CreateTemplateUI(driver);

		fc.utobj().printTestStep("Click Continue Button");
		fc.utobj().clickElement(driver, ui.continue_Btn);
	}
}
