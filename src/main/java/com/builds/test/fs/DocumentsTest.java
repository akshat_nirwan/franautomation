package com.builds.test.fs;

import org.openqa.selenium.WebDriver;

import com.builds.uimaps.fs.DocumentsUI;
import com.builds.utilities.FranconnectUtil;

class DocumentsTest {

	void fillDocuments(WebDriver driver, Documents doc) throws Exception {
		DocumentsUI dui = new DocumentsUI(driver);
		FranconnectUtil fc = new FranconnectUtil();

		fc.utobj().printTestStep("Fill Documents in documents tab");

		if (doc.getDocumentTitle() != null) {
			fc.utobj().sendKeys(driver, dui.docTitle, doc.getDocumentTitle());
		}

		if (doc.getUploadDocument() != null) {

			String file = fc.utobj().getFilePathFromTestData(doc.getUploadDocument());
			fc.utobj().sendKeys(driver, dui.uploadDocument, file);

		}

		for (int i = 1; i <= doc.getNumberOfDocuments(); i++) {

			fc.utobj().clickElement(driver, dui.addDocument);
			if (doc.getDocumentTitle() != null) {
				fc.utobj().sendKeys(driver, fc.utobj().getElementByXpath(driver, dui.getXpathOfDocumentTitle(i)),
						doc.getDocumentTitle() + i);
				String file2 = fc.utobj().getFilePathFromTestData(String.valueOf(i) + ".pdf");
				fc.utobj().sendKeys(driver, fc.utobj().getElementByXpath(driver, dui.getXpathOfUploadDocument(i)),
						file2);
			}

		}

		fc.utobj().clickElement(driver, dui.saveBtn);

	}

	void validateDocuments(WebDriver driver, Documents doc) throws Exception {
		FranconnectUtil fc = new FranconnectUtil();
		if (!fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//*[@name='Add More']")) {
			fc.utobj().throwsException("Add More button not present");
		}
		for (int i = 1; i <= doc.getNumberOfDocuments(); i++) {
			if (!fc.utobj().assertPageSource(driver, doc.getDocumentTitle() + i)) {
				fc.utobj().throwsException("Document title not found");
			}
			if (!fc.utobj().assertLinkText(driver, String.valueOf(i) + ".pdf")) {
				fc.utobj().throwsException("Document file not found");
			}
		}
	}

	// boolean[] validateDocuments(WebDriver driver, Documents doc) throws
	// Exception
	// {
	// boolean b1[]=new boolean[5];
	// FranconnectUtil fc = new FranconnectUtil();
	// b1[0]=fc.utobj().verifyCase(driver, ".//*[@name='Add More']");
	//
	// for(int i=1;i<=4;i++)
	// {
	// b1[i]=fc.utobj().assertPageSource(driver, doc.getDocumentTitle()+i);
	// }
	// return b1;
	// }

}
