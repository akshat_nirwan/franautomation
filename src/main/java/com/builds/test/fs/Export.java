package com.builds.test.fs;

class Export extends Lead {

	private String tabName; // Enter comma separated tabs.

	public String getTabName() {
		return tabName;
	}

	public void setTabName(String tabName) {
		this.tabName = tabName;
	}

}
