package com.builds.test.fs;

import java.util.Map;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Test;

import com.builds.utilities.FranconnectUtil;
import com.builds.utilities.TestCase;

class ExportPage_Test {

	@Test(groups = { "sales","salesfixing" , "sales_failed_11_2018" })
	@TestCase(createdOn = "2018-01-30", updatedOn = "2018-01-30", testCaseId = "Sales_Export_Lead_Basic", testCaseDescription = "Verify Export of the lead on view page.")
	private void Sales_Export_Lead_Basic() throws Exception {

		FranconnectUtil fc = new FranconnectUtil();
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		// Map<String, String> dataSet = fc.utobj().readTestData("sales", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		Sales sales = new Sales();
		LeadManagementTest lm = new LeadManagementTest(driver);
		
		Lead ld = new Lead();
		/*ld.setFirstName(dataSet.get("leadFirstName"));
		ld.setLastName(dataSet.get("leadLastName"));
		ld.setEmail(dataSet.get("leadEmailID"));
		ld.setLeadOwner(dataSet.get("leadOwner"));
		ld.setLeadSourceCategory(dataSet.get("leadSourceCategory"));
		ld.setLeadSourceDetails(dataSet.get("leadSourceDetail"));*/
		
		ExportTest exportTest = new ExportTest();
		Export export = new Export();
		FieldsToBeExported fields = new FieldsToBeExported();

		try {

			driver = fc.loginpage().login(driver);
			fc.sales().sales_common().fsModule(driver);
			sales.leadManagement(driver);
			lm.addLeadAndSave(fc.sales().sales_common().fillDefaultValue_LeadDetails(ld));
			sales.exportPage(driver);

			export.setFirstName(ld.getFirstName());
			export.setLastName(ld.getLastName());
			export.setEmail(ld.getEmail());
			exportTest.selectTabsToBeExported_Click_SearchData(driver, export);
			exportTest.fillExportSearchParameters_Click_ViewData(driver, export);
			if (!fc.utobj().assertPageSource(driver, ld.getFirstName())) {
				fc.utobj().throwsException("Searched record not found in export - view page.");
			}

			sales.exportPage(driver);
			exportTest.selectTabsToBeExported_Click_SearchData(driver, export);
			exportTest.fillExportSearchParameters_Click_SelectFieldsButton(driver, export);
			exportTest.selectFieldsToBeExported_Click_ViewButton(driver,
					fc.sales().sales_common().fillDefaultValue_ExportFields(driver, fields));

			if (!fc.utobj().assertPageSource(driver, ld.getFirstName())) {
				fc.utobj().throwsException("Searched record not found in export(View page)");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

}
