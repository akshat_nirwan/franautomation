package com.builds.test.fs;

import org.openqa.selenium.WebDriver;

import com.builds.uimaps.fs.ExportUI;
import com.builds.utilities.FranconnectUtil;

class ExportTest {

	public void selectTabsToBeExported_Click_SearchData(WebDriver driver, Export export) throws Exception {
		FranconnectUtil fc = new FranconnectUtil();
		ExportUI ui = new ExportUI(driver);

		fc.utobj().printTestStep("Select Tab");

		if (export.getTabName() != null) {
			// TODO : Write login for selection of checkbox
		}

		clickSearchDataButton(driver);
	}

	private void clickSearchDataButton(WebDriver driver) throws Exception {
		FranconnectUtil fc = new FranconnectUtil();
		ExportUI ui = new ExportUI(driver);

		fc.utobj().printTestStep("Click Search Button");
		fc.utobj().clickElement(driver, ui.searchData_Button);
	}

	public void fillExportSearchParameters_Click_SelectFieldsButton(WebDriver driver, Export export) throws Exception {
		FranconnectUtil fc = new FranconnectUtil();
		ExportUI ui = new ExportUI(driver);
		fillExportSearchParameter(driver, export);
		clickSelectFieldsButton(driver);
	}

	private void clickSelectFieldsButton(WebDriver driver) throws Exception {
		FranconnectUtil fc = new FranconnectUtil();
		ExportUI ui = new ExportUI(driver);

		fc.utobj().printTestStep("Click Select Fields Button");
		fc.utobj().clickElement(driver, ui.SelectFields_Button);
	}

	public void selectFieldsToBeExported_Click_ViewButton(WebDriver driver, FieldsToBeExported fields)
			throws Exception {
		FranconnectUtil fc = new FranconnectUtil();
		ExportUI ui = new ExportUI(driver);

		selectFieldsToBeExported(driver, fields);
		clickViewButton(driver);

	}

	private void selectFieldsToBeExported(WebDriver driver, FieldsToBeExported fields) throws Exception {
		FranconnectUtil fc = new FranconnectUtil();
		ExportUI ui = new ExportUI(driver);
		fc.utobj().printTestStep("Select Fields");

		FieldsToBeExportedTest fieldexport = new FieldsToBeExportedTest();
		fieldexport.selectFieldstoBeExported(driver, fields);

	}

	private void clickViewButton(WebDriver driver) throws Exception {
		FranconnectUtil fc = new FranconnectUtil();
		ExportUI ui = new ExportUI(driver);
		fc.utobj().printTestStep("Click View Button");
		fc.utobj().clickElement(driver, ui.View_Button);
	}

	private void clickViewDataButton(WebDriver driver) throws Exception {
		FranconnectUtil fc = new FranconnectUtil();
		ExportUI ui = new ExportUI(driver);

		fc.utobj().printTestStep("Click View Data Button");
		fc.utobj().clickElement(driver, ui.ViewData_Button);
	}

	public void fillExportSearchParameter(WebDriver driver, Export export) throws Exception {

		FranconnectUtil fc = new FranconnectUtil();
		ExportUI ui = new ExportUI(driver);

		fc.utobj().printTestStep("Fill Export Details");

		if (export.getFirstName() != null) {
			fc.utobj().sendKeys(driver, ui.FS_LEAD_DETAILS1_firstName, export.getFirstName());
		}

		if (export.getLastName() != null) {
			fc.utobj().sendKeys(driver, ui.FS_LEAD_DETAILS1_lastName, export.getLastName());
		}

		if (export.getEmail() != null) {
			fc.utobj().sendKeys(driver, ui.FS_LEAD_DETAILS1_emailID, export.getEmail());
		}

	}

	public void fillExportSearchParameters_Click_ViewData(WebDriver driver, Export export) throws Exception {

		FranconnectUtil fc = new FranconnectUtil();
		ExportUI ui = new ExportUI(driver);

		fillExportSearchParameter(driver, export);
		clickViewDataButton(driver);
	}

}
