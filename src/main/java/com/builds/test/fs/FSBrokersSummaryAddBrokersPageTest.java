package com.builds.test.fs;

import java.util.Map;

import org.openqa.selenium.WebDriver;

import com.builds.uimaps.fs.FSBrokersSummaryAddBrokersPage;
import com.builds.utilities.FranconnectUtil;

public class FSBrokersSummaryAddBrokersPageTest {
	FranconnectUtil fc = new FranconnectUtil();

	public String fillBrokersInfoWithFullRecords(WebDriver driver, Map<String, String> config) throws Exception {
		return "ab";

		/*
		 * String testCaseId = "TC_AddBroker_FullData_001";
		 * 
		 * 
		 * Map<String, String> dataSet = fc.utobj().readTestData(testCaseId);
		 * 
		 * FSBrokersSummaryAddBrokersPage pobj = new
		 * FSBrokersSummaryAddBrokersPage(driver);
		 * 
		 * String firstName =
		 * fc.utobj().generateTestData(dataSet.get("firstName")); String
		 * lastName = fc.utobj().generateTestData(dataSet.get("lastName"));
		 * String address1 = dataSet.get("address1"); String address2 =
		 * dataSet.get("address2"); String city = dataSet.get("city"); String
		 * country = dataSet.get("country"); String zip = dataSet.get("zip");
		 * String state = dataSet.get("state"); String homePhone =
		 * dataSet.get("homePhone"); String workPhone =
		 * dataSet.get("workPhone"); String fax = dataSet.get("fax"); String
		 * mobile = dataSet.get("mobile"); String bestTimeToContact =
		 * dataSet.get("bestTimeToContact"); String primaryPhoneToCall =
		 * dataSet.get("primaryPhoneToCall"); String email =
		 * dataSet.get("email"); String agency = dataSet.get("agency"); String
		 * priority = dataSet.get("priority"); String comments =
		 * dataSet.get("comments");
		 * 
		 * fc.utobj().sendKeys(driver,pobj.firstName,firstName);
		 * fc.utobj().sendKeys(driver,pobj.lastName,lastName);
		 * fc.utobj().sendKeys(driver,pobj.address1,address1);
		 * fc.utobj().sendKeys(driver,pobj.address2,address2);
		 * fc.utobj().sendKeys(driver,pobj.city,city);
		 * fc.utobj().selectDropDownByVisibleText(driver,pobj.countryID,country)
		 * ; fc.utobj().sendKeys(driver,pobj.city,zip);
		 * fc.utobj().selectDropDownByVisibleText(driver,pobj.stateID,state);
		 * fc.utobj().sendKeys(driver,pobj.homePhone,homePhone);
		 * fc.utobj().sendKeys(driver,pobj.workPhone,workPhone);
		 * fc.utobj().sendKeys(driver,pobj.fax,fax);
		 * fc.utobj().sendKeys(driver,pobj.mobile,mobile);
		 * fc.utobj().sendKeys(driver,pobj.bestTimeToContact,bestTimeToContact);
		 * fc.utobj().selectDropDownByVisibleText(driver,pobj.primaryPhoneToCall
		 * ,primaryPhoneToCall); fc.utobj().sendKeys(driver,pobj.emailID,email);
		 * fc.utobj().selectDropDownByVisibleText(driver,pobj.brokerAgencyId,
		 * agency);
		 * fc.utobj().selectDropDownByVisibleText(driver,pobj.priority,priority)
		 * ; fc.utobj().sendKeys(driver,pobj.comments,comments);
		 * 
		 * fc.utobj().selectDropDown(driver,pobj.brokerAgencyId, "None"); //
		 * fc.utobj().clickElement(driver,pobj.submit);
		 * 
		 * List<String> listItems = new ArrayList<String>();
		 * 
		 * listItems.add(firstName); listItems.add(lastName);
		 * listItems.add(address1); listItems.add(address2);
		 * listItems.add(city); listItems.add(zip); listItems.add(homePhone);
		 * listItems.add(workPhone); listItems.add(fax); listItems.add(mobile);
		 * listItems.add(zip); listItems.add(email); listItems.add(comments);
		 * 
		 * List<String> listItems2 = listItems;
		 * 
		 * boolean isPresent =
		 * fc.utobj().assertPageSourceWithMultipleRecords(driver, listItems);
		 * 
		 * if(isPresent==false){ fc.utobj().throwsException(
		 * "Some or few of the fields not found after submitting the page"); }
		 * 
		 * FSBrokersSummaryBrokersDetailsPageTest p1 = new
		 * FSBrokersSummaryBrokersDetailsPageTest(); p1.modifyLnk_top(driver);
		 * 
		 * isPresent = fc.utobj().assertMultipleInputBoxValue(driver,
		 * listItems2);
		 * 
		 * if(isPresent==false){ fc.utobj().throwsException(
		 * "Some or all of the fields missing from modify page"); }
		 * 
		 * String brokerName = firstName.concat(" ").concat(lastName);
		 * 
		 * if(brokerName!=null && brokerName.length()>0){ Reporter.log(
		 * "Broker Added with name :"+brokerName); }
		 * 
		 * return brokerName;
		 */
	}

	public String fillBrokersDetailsWithBasicData(WebDriver driver, Map<String, String> config) throws Exception {

		String testCaseId = "TC_AddBroker_BasicData_001";

		Map<String, String> dataSet = fc.utobj().readTestData("sales", testCaseId);

		FSBrokersSummaryAddBrokersPage pobj = new FSBrokersSummaryAddBrokersPage(driver);

		String firstName = fc.utobj().generateTestData(dataSet.get("firstName"));
		String lastName = fc.utobj().generateTestData(dataSet.get("lastName"));
		String email = dataSet.get("email");
		String agency = dataSet.get("agency");

		fc.utobj().sendKeys(driver, pobj.firstName, firstName);
		fc.utobj().sendKeys(driver, pobj.lastName, lastName);
		fc.utobj().sendKeys(driver, pobj.emailID, email);
		fc.utobj().selectDropDownByVisibleText(driver, pobj.brokerAgencyId, agency);

		fc.utobj().selectDropDown(driver, pobj.brokerAgencyId, "None");

		fc.utobj().clickElement(driver, pobj.submit);

		String brokerName = firstName.concat(" ").concat(lastName);
		return brokerName;
	}
}
