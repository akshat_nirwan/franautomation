package com.builds.test.fs;

import org.openqa.selenium.WebDriver;

import com.builds.uimaps.fs.FSBrokersSummaryBrokersDetailsPage;
import com.builds.utilities.FranconnectUtil;

public class FSBrokersSummaryBrokersDetailsPageTest {
	FranconnectUtil fc = new FranconnectUtil();

	public void modifyLnk_top(WebDriver driver) throws Exception {

		FSBrokersSummaryBrokersDetailsPage pobj = new FSBrokersSummaryBrokersDetailsPage(driver);
		fc.utobj().clickElement(driver, pobj.modifyLnkTop);
	}

	public void sendEmailLnk_top(WebDriver driver) throws Exception {

		FSBrokersSummaryBrokersDetailsPage pobj = new FSBrokersSummaryBrokersDetailsPage(driver);
		fc.utobj().clickElement(driver, pobj.sendEmailLnkTop);
	}

	public void logATaskLnk_top(WebDriver driver) throws Exception {

		FSBrokersSummaryBrokersDetailsPage pobj = new FSBrokersSummaryBrokersDetailsPage(driver);
		fc.utobj().clickElement(driver, pobj.logATaskLnkTop);
	}

	public void logACallLnk_top(WebDriver driver) throws Exception {

		FSBrokersSummaryBrokersDetailsPage pobj = new FSBrokersSummaryBrokersDetailsPage(driver);
		fc.utobj().clickElement(driver, pobj.logACallLnkTop);
	}

	public void logATaskLnk_OpenTask(WebDriver driver) throws Exception {

		FSBrokersSummaryBrokersDetailsPage pobj = new FSBrokersSummaryBrokersDetailsPage(driver);
		fc.utobj().clickElement(driver, pobj.logATaskOpenTaskLnk);
	}

	public void detailedHistory_ActivityHistory(WebDriver driver) throws Exception {

		FSBrokersSummaryBrokersDetailsPage pobj = new FSBrokersSummaryBrokersDetailsPage(driver);
		fc.utobj().clickElement(driver, pobj.detailedHistoryActivityHistoryLnk);
	}

	public void logACall_ActivityHistory(WebDriver driver) throws Exception {

		FSBrokersSummaryBrokersDetailsPage pobj = new FSBrokersSummaryBrokersDetailsPage(driver);
		fc.utobj().clickElement(driver, pobj.logACallActivityHistoryLnk);
	}

	public void remarks_AddRemarks(WebDriver driver) throws Exception {

		FSBrokersSummaryBrokersDetailsPage pobj = new FSBrokersSummaryBrokersDetailsPage(driver);
		fc.utobj().clickElement(driver, pobj.addRemarksLnk);
	}

	public void backBtn(WebDriver driver) throws Exception {

		FSBrokersSummaryBrokersDetailsPage pobj = new FSBrokersSummaryBrokersDetailsPage(driver);
		fc.utobj().clickElement(driver, pobj.backBtn);
	}

}
