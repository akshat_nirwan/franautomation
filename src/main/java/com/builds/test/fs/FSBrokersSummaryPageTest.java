package com.builds.test.fs;

import java.util.Map;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Test;

import com.builds.uimaps.fs.FSBrokersPage;
import com.builds.uimaps.fs.FSBrokersSummaryPage;
import com.builds.uimaps.fs.FSLeadSummarySendEmailPage;
import com.builds.uimaps.fs.SearchUI;
import com.builds.utilities.FranconnectUtil;
import com.builds.utilities.TestCase;

public class FSBrokersSummaryPageTest {
	FranconnectUtil fc = new FranconnectUtil();

	@Test(groups = { "salessadf" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "TC_AddBroker_FullData_001", testCaseDescription = "Add Broker with full record")
	private void addBrokers() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			fc.sales().sales_common().fsModule(driver);
			FSBrokersPage pobj = new FSBrokersPage(driver);

			fc.utobj().moveToElementThroughAction(driver, fc.utobj().getElement(driver, pobj.brokersLnk));
			fc.utobj().printTestStep("Go To Brokers Tabs and Add Brokers");
			fc.utobj().clickElement(driver, pobj.brokersLnk);
			try {
				fc.utobj().getElement(driver, pobj.addBrokersBtn).isEnabled();
			} catch (Exception e) {
				fc.utobj().clickElement(driver, pobj.brokersLnk);
			}

			try {
				fc.utobj().clickElement(driver, pobj.addBrokersBtn);
			} catch (Exception e) {
				fc.utobj().clickElement(driver, pobj.addBrokersBtn2fr);
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "sales_old", "salesbrokerSolr" })
	@TestCase(createdOn = "2017-10-31", updatedOn = "2017-10-31", testCaseId = "TC_Solr_Search_Brokers_001", testCaseDescription = "Top 5 Brokers are visible with 'More' link and on click details should be visible")
	private void checkBrokersMoreLink() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("sales", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			FSBrokersPage pobj = new FSBrokersPage(driver);
			SearchUI search_page = new SearchUI(driver);

			String brokerFirstName = fc.utobj().generateTestData(dataSet.get("brokerName5"));

			String brokerName = brokerFirstName + dataSet.get("brokerName");
			String brokerName2 = brokerFirstName + dataSet.get("brokerName2");
			String brokerName3 = brokerFirstName + dataSet.get("brokerName3");
			String brokerName4 = brokerFirstName + dataSet.get("brokerName4");
			String brokerName5 = brokerFirstName + dataSet.get("broker5");
			String brokerName6 = brokerFirstName + dataSet.get("broker6");
			String brokerName7 = brokerFirstName + dataSet.get("broker7");
			String brokerName8 = brokerFirstName + dataSet.get("broker8");
			boolean isBrokerVerified = false;
			fc.sales().sales_common().fsModule(driver);
			addBrokers(driver, brokerName);
			addBrokers(driver, brokerName2);
			addBrokers(driver, brokerName3);
			addBrokers(driver, brokerName4);
			addBrokers(driver, brokerName5);
			addBrokers(driver, brokerName6);
			addBrokers(driver, brokerName7);
			addBrokers(driver, brokerName8);

			boolean isSearchTrue = false;
			boolean isVeiwAll = false;
			for (int i = 0; i < 3; i++) {
				if (isSearchTrue == false) {
					fc.utobj().sendKeys(driver, search_page.topSearchField, brokerFirstName);

					if (i == 2) {

					}
					fc.utobj().clickEnterOnElement(driver, search_page.topSearchField);

					try {
						fc.utobj().moveToElement(driver, fc.utobj().getElementByXpath(driver,
								".//span/*[contains(text(),'" + brokerName8 + "')]"));
						isSearchTrue = true;
					} catch (Exception e) {
					}
				}
			}
			if (isSearchTrue == false) {
				fc.utobj().throwsException("was not able to search Lead through Top Search");
			}
			// fc.utobj().moveToElement(driver,
			// fc.utobj().getElementByXpath(driver,".//span/*[contains(text(),'"+brokerName2+"')]")));
			// fc.utobj().moveToElement(driver,
			// fc.utobj().getElementByXpath(driver,".//span/*[contains(text(),'"+brokerName3+"')]")));
			// fc.utobj().moveToElement(driver,
			// fc.utobj().getElementByXpath(driver,".//span/*[contains(text(),'"+brokerName8+"')]")));

			fc.utobj().clickElement(driver, search_page.viewAllBtn2);

			isVeiwAll = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[contains(text(),'" + brokerName + "')]");
			isVeiwAll = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[contains(text(),'" + brokerName2 + "')]");
			isVeiwAll = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[contains(text(),'" + brokerName3 + "')]");
			isVeiwAll = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[contains(text(),'" + brokerName4 + "')]");
			isVeiwAll = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[contains(text(),'" + brokerName5 + "')]");
			isVeiwAll = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[contains(text(),'" + brokerName6 + "')]");
			isVeiwAll = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[contains(text(),'" + brokerName7 + "')]");
			isVeiwAll = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[contains(text(),'" + brokerName8 + "')]");
			if (isVeiwAll == false) {
				fc.utobj().throwsException("All elements not visible on clicking view all");
			}
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//a[contains(text(),'" + brokerName + "')]"));
			isBrokerVerified = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//*[contains(text(),'" + brokerName + "')]");
			if (isBrokerVerified == false) {
				fc.utobj().throwsException("Broker not verified on click at search");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}

	}

	public String addBrokers(WebDriver driver, String brokerName) throws Exception {

		fc.sales().sales_common().fsModule(driver);
		FSBrokersPage pobj = new FSBrokersPage(driver);

		fc.utobj().moveToElementThroughAction(driver, pobj.brokersLnk);

		fc.utobj().clickElement(driver, pobj.brokersLnk);
		try {
			fc.utobj().getElement(driver, pobj.addBrokersBtn).isEnabled();
		} catch (Exception e) {
			fc.utobj().clickElement(driver, pobj.brokersLnk);
		}

		try {
			fc.utobj().clickElement(driver, pobj.addBrokersBtn);

		} catch (Exception e) {
			fc.utobj().clickElement(driver, pobj.addBrokersBtn2fr);
		}
		// brokerName = fc.utobj().generateTestData(brokerName);

		try {
			fc.utobj().selectDropDownByIndex(driver, pobj.brokerType, 1);
		} catch (Exception e) {

		}
		fc.utobj().sendKeys(driver, pobj.firstName, brokerName);
		fc.utobj().sendKeys(driver, pobj.lastName, brokerName);
		fc.utobj().sendKeys(driver, pobj.emailID, "test@gmail.com");
		fc.utobj().selectDropDown(driver, pobj.brokerAgencyId, fc.utobj().translateString("None"));
		//
		fc.utobj().clickElement(driver, pobj.submitBtn);
		// searchBrokers(driver,brokerName);
		return brokerName;
	}

	/*
	 * @Parameters({ "brokerName" })
	 * 
	 * @Test(groups = { "commonModule", "add", "Add Lead"}) public String
	 * searchBrokersWithAddBroker(@Optional()String brokerName) throws Exception
	 * { FSBrokersSummaryPageTest p1 = new FSBrokersSummaryPageTest();
	 * p1.addBrokers(brokerName); return brokerName; }
	 */

	@Test(groups = { "sales", "salescan007" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "TC_FC_Brokers_003", testCaseDescription = "Add Broker and Log a Task Action Menu")
	private String logATaskActionMenu() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("sales", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		String brokerName = fc.utobj().generateTestData(dataSet.get("brokerName"));

		try {
			driver = fc.loginpage().login(driver);

			FSBrokersPage pobj = new FSBrokersPage(driver);

			fc.utobj().printTestStep("Go To Brokers Tabs and Add Brokers");

			brokerName = addBrokers(driver, brokerName);
			fc.utobj().sendKeys(driver, pobj.brokerNameSearch, brokerName);
			fc.utobj().clickElement(driver, pobj.searchBtn);
			fc.utobj().clickElement(driver, pobj.checkBoxAll);
			fc.utobj().printTestStep("Log a Task");
			fc.utobj().actionImgOption(driver, brokerName, "Log a Task");
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			String taskSubject = "Task Subject ";
			taskSubject = fc.utobj().generateTestData(taskSubject);
			fc.utobj().sendKeys(driver, pobj.taskSubject, taskSubject);
			fc.utobj().clickElement(driver, pobj.addTaskBtn);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
		return brokerName;
	}

	@Test(groups = { "sales" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "TC_FC_Brokers_004", testCaseDescription = "Add Broker and Send Email")
	private void sendBrokerEmailActionMenu() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("sales", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		String brokerName = fc.utobj().generateTestData(dataSet.get("brokerName"));

		try {
			driver = fc.loginpage().login(driver);
			FSBrokersPage pobj = new FSBrokersPage(driver);
			fc.utobj().printTestStep("Go To Brokers Tabs and Add Brokers");

			brokerName = addBrokers(driver, brokerName);
			fc.utobj().sendKeys(driver, pobj.brokerNameSearch, brokerName);
			fc.utobj().clickElement(driver, pobj.searchBtn);
			fc.utobj().clickElement(driver, pobj.checkBoxAll);
			fc.utobj().printTestStep("Send Email");

			fc.utobj().actionImgOption(driver, brokerName, "Send Email");
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "sales", "salescheckert45" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "TC_FC_Brokers_005", testCaseDescription = "Add and Archive Brokers")
	public String archiveBrokerActionMenu() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("sales", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		String brokerName = fc.utobj().generateTestData(dataSet.get("brokerName"));

		try {
			driver = fc.loginpage().login(driver);

			FSBrokersPage pobj = new FSBrokersPage(driver);
			// String randomChar=fc.utobj().generateRandomChar();
			fc.utobj().printTestStep("Go To Brokers Tabs and Add Brokers");

			//
			brokerName = addBrokers(driver, brokerName);
			fc.utobj().sendKeys(driver, pobj.brokerNameSearch, brokerName);
			fc.utobj().clickElement(driver, pobj.searchBtn);
			fc.utobj().clickElement(driver, pobj.checkBoxAll);
			fc.utobj().printTestStep("Archive Brokers");

			fc.utobj().actionImgOption(driver, brokerName, "Archive");
			fc.utobj().acceptAlertBox(driver);

			fc.utobj().sendKeys(driver, pobj.brokerNameSearch, brokerName);
			fc.utobj().clickElement(driver, pobj.searchBtn);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
		return brokerName;
	}

	@Test(groups = { "sales" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "TC_FC_Brokers_006", testCaseDescription = "Add and Delete Broker")
	private String deleteBrokerActionMenu() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("sales", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		String brokerName = fc.utobj().generateTestData(dataSet.get("brokerName"));

		try {
			driver = fc.loginpage().login(driver);

			FSBrokersPage pobj = new FSBrokersPage(driver);
			fc.utobj().printTestStep("Go To Brokers Tabs and Add Brokers");

			brokerName = addBrokers(driver, brokerName);
			fc.utobj().sendKeys(driver, pobj.brokerNameSearch, brokerName);
			fc.utobj().clickElement(driver, pobj.searchBtn);
			fc.utobj().clickElement(driver, pobj.checkBoxAll);
			fc.utobj().printTestStep("Delete Brokers");

			fc.utobj().actionImgOption(driver, brokerName, "Delete");
			fc.utobj().acceptAlertBox(driver);
			fc.utobj().sendKeys(driver, pobj.brokerNameSearch, brokerName);
			fc.utobj().clickElement(driver, pobj.searchBtn);
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
		return brokerName;
	}

	@Test(groups = { "sales","sales_failed" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "TC_FC_Brokers_007", testCaseDescription = "Add Brokers and cancel Send Email from Action Menu")
	public String cancelBrokerEmailActionMenu() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("sales", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		String brokerName = fc.utobj().generateTestData(dataSet.get("brokerName"));

		try {
			driver = fc.loginpage().login(driver);

			FSBrokersPage pobj = new FSBrokersPage(driver);
			fc.utobj().printTestStep("Go To Brokers Tabs and Add Brokers");
			fc.utobj().printTestStep("Add Broker");
			fc.utobj().printTestStep("Add Another Broker");

			brokerName = addBrokers(driver, brokerName);
			fc.utobj().sendKeys(driver, pobj.brokerNameSearch, brokerName);
			fc.utobj().clickElement(driver, pobj.searchBtn);
			fc.utobj().clickElement(driver, pobj.checkBoxAll);
			fc.utobj().printTestStep("Send Email");

			fc.utobj().actionImgOption(driver, brokerName, "Send Email");

			fc.utobj().printTestStep("Cancel");
			fc.utobj().clickElement(driver, pobj.cancelBtn);
			fc.utobj().printTestStep("Click on Broker Name");
			fc.utobj().clickElement(driver, pobj.brokerNameSearch);

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
		return brokerName;
	}

	@Test(groups = { "sales,sales545" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "TC_FC_Brokers_008", testCaseDescription = "Verify Broker Log A Call")
	public void logACallActionIcon() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("sales", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		String brokerName = fc.utobj().generateTestData(dataSet.get("brokerName"));
		String callSubject = fc.utobj().generateTestData(dataSet.get("callSubject"));

		try {
			driver = fc.loginpage().login(driver);
			FSBrokersPage pobj = new FSBrokersPage(driver);
			fc.utobj().printTestStep("Go To Brokers Tabs and Add Brokers");

			brokerName = addBrokers(driver, brokerName);

			fc.utobj().sendKeys(driver, pobj.brokerNameSearch, brokerName);
			fc.utobj().clickElement(driver, pobj.searchBtn);
			fc.utobj().clickElement(driver, pobj.checkBoxAll);
			fc.utobj().printTestStep("Log A Call");

			fc.utobj().singleActionIcon(driver, "Log a Call");
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().sendKeys(driver, pobj.callSubject, callSubject);
			fc.utobj().clickElement(driver, pobj.addCallBtn);
			fc.utobj().clickElement(driver, pobj.noToScheduleTask);

			fc.utobj().sendKeys(driver, pobj.brokerNameSearch, brokerName);
			fc.utobj().clickElement(driver, pobj.searchBtn);
			fc.utobj().clickPartialLinkText(driver, brokerName);

			fc.utobj().clickPartialLinkText(driver, callSubject);

			boolean isValFound = fc.sales().sales_common().assertValuePresentInCbox(driver, callSubject);

			if (isValFound == false) {
				fc.utobj().throwsException("Not able to log a call!");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	public String sendEmail(WebDriver driver) throws Exception {
		String mailSubject = "Test Email Subject ";
		mailSubject = mailSubject + fc.utobj().generateTestData(mailSubject);
		String mailBody = "Test Email By FranConnect System ";
		mailBody = mailBody + fc.utobj().generateTestData(mailBody);
		FSLeadSummarySendEmailPage pobj = new FSLeadSummarySendEmailPage(driver);
		fc.utobj().sendKeys(driver, pobj.subject, mailSubject);
		String windowHandle = driver.getWindowHandle();
		driver.switchTo().frame(fc.utobj().getElement(driver, pobj.htmlFrame));
		fc.utobj().sendKeys(driver, pobj.mailText, mailBody);
		driver.switchTo().window(windowHandle);
		fc.utobj().clickElement(driver, pobj.sendBtn);
		System.out.println(mailSubject);
		return mailSubject;
	}

	public void addBrokers(WebDriver driver) throws Exception {

		FSBrokersSummaryPage pobj = new FSBrokersSummaryPage(driver);
		fc.utobj().clickElement(driver, pobj.addBrokers);
	}

	public void searchBrokers(WebDriver driver, String agency, String brokerName) throws Exception {

		FSBrokersSummaryPage pobj = new FSBrokersSummaryPage(driver);

		if (!agency.equalsIgnoreCase("Select All")) {
			fc.utobj().clickElement(driver, pobj.rolesBtn);
			fc.utobj().sendKeys(driver, pobj.searchTxtBox, agency);
			fc.utobj().clickElement(driver, pobj.selectAll);
		}

		fc.utobj().clickElement(driver, pobj.searchBtn);

		boolean isTextPresent = fc.utobj().assertLinkText(driver, brokerName);
		if (isTextPresent == false) {
			fc.utobj().throwsException("Broker Not Searched.");
		}
	}

	public void gotoArchivedBrokers(WebDriver driver) throws Exception {

		FSBrokersSummaryPage pobj = new FSBrokersSummaryPage(driver);
		fc.utobj().clickElement(driver, pobj.archivedBrokersLnk);
	}

}
