package com.builds.test.fs;

import java.util.HashMap;
import java.util.Map;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.builds.uimaps.fs.LeadSummaryUI;
import com.builds.utilities.FranconnectUtil;

public class FSCommonMethods {
	FranconnectUtil fc = new FranconnectUtil();

	public Map<String, String> fillTaskDetails_Mandatory(WebDriver driver, Map<String, String> config)
			throws Exception {
		Map<String, String> taskInfo = new HashMap<String, String>();

		String testCaseId = "TC_Fill_TaskDetails_001";
		Map<String, String> dataSet = fc.utobj().readTestData("sales", testCaseId);
		try {
			String assignTo = dataSet.get("assignTo");
			String status = dataSet.get("status");
			String subject = fc.utobj().generateTestData(dataSet.get("subject"));
			String taskType = dataSet.get("taskType");
			String comments = dataSet.get("comments");

			LeadSummaryUI pobj = new LeadSummaryUI(driver);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			if (assignTo.equalsIgnoreCase("Lead Owner")) {
				fc.utobj().clickElement(driver, pobj.assignTaskToLeadOwner);
			} else {
				fc.utobj().clickElement(driver, pobj.otherUserRadio);
				fc.utobj().selectValFromMultiSelect(driver, pobj.otherUserRadio, assignTo);
			}
			fc.utobj().selectDropDownByVisibleText(driver, pobj.taskStatusDrp, status);
			fc.utobj().selectDropDownByVisibleText(driver, pobj.taskType, taskType);
			fc.utobj().sendKeys(driver, pobj.subject, subject);
			fc.utobj().sendKeys(driver, pobj.taskComments, comments);
			fc.utobj().clickElement(driver, pobj.setTimeYourselfRadio);
			fc.utobj().clickElement(driver, pobj.createTaskBtn);

			taskInfo.put(assignTo, assignTo);
			taskInfo.put(status, status);
			taskInfo.put(subject, subject);
			taskInfo.put(taskType, taskType);
			taskInfo.put(comments, comments);

		} catch (Exception e) {
			fc.utobj().throwsSkipException("Unable to create Task");
		}

		return taskInfo;
	}

	public void clickDismissButtonOnReminderPopUp(WebDriver driver) throws Exception {
		WebElement element = fc.utobj().getElementByXpath(driver, ".//input[@value='Dismiss' and name='button1']");

		fc.utobj().clickElement(driver, element);
	}
}
