package com.builds.test.fs;

import org.openqa.selenium.WebDriver;

import com.builds.uimaps.fs.FSEmailCampaignConfirmationPage;
import com.builds.utilities.FranconnectUtil;

public class FSEmailCampaignConfirmationPageTest {
	FranconnectUtil fc = new FranconnectUtil();

	public void confirmBtn(WebDriver driver) throws Exception {

		FSEmailCampaignConfirmationPage pobj = new FSEmailCampaignConfirmationPage(driver);
		fc.utobj().clickElement(driver, pobj.confirmBtn);
	}

	public void cancelBtn(WebDriver driver) throws Exception {

		FSEmailCampaignConfirmationPage pobj = new FSEmailCampaignConfirmationPage(driver);
		fc.utobj().clickElement(driver, pobj.cancelBtn);
	}

	public void backBtn(WebDriver driver) throws Exception {

		FSEmailCampaignConfirmationPage pobj = new FSEmailCampaignConfirmationPage(driver);
		fc.utobj().clickElement(driver, pobj.backBtn);
	}
}
