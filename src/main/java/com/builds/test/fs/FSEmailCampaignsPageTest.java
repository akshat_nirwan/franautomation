package com.builds.test.fs;

import java.util.Map;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.Reporter;
import org.testng.annotations.Test;

import com.builds.uimaps.campaigncentercrm.CRMCampaignCenterCampaignsPage;
import com.builds.uimaps.fs.FSCampaignCenterPage;
import com.builds.uimaps.fs.FSEmailCampaignsPage;
import com.builds.uimaps.fs.FSGroupsPage;
import com.builds.uimaps.fs.LeadSummaryUI;
import com.builds.uimaps.fs.SearchUI;
import com.builds.utilities.FranconnectUtil;
import com.builds.utilities.TestCase;

public class FSEmailCampaignsPageTest {

	FranconnectUtil fc = new FranconnectUtil();

	@Test(groups = { "sales0001" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "TC_Sales_CampaignCenter_AddCampaign_001", testCaseDescription = "Validate C")
	private void verifyAddCampaign() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("sales", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			fc.sales().sales_common().fsModule(driver);
			Sales fsMod = new Sales();

			String firstName = fc.utobj().generateTestData(dataSet.get("FirstName"));
			String lastName = fc.utobj().generateTestData(dataSet.get("LastName"));
			LeadSummaryUI pobj = new LeadSummaryUI(driver);
			FSLeadSummaryPageTest leadSummaryTest = new FSLeadSummaryPageTest();
			String userName = "FranConnect Administrator";
			fc.utobj().printTestStep("Add a lead");

			leadSummaryTest.addLeadSummaryWithLeadNameOwnerName(driver, firstName, lastName, userName);
			fsMod.groups(driver);
			FSGroupsPage pobj1 = new FSGroupsPage(driver);
			FSSearchPageTest p3 = new FSSearchPageTest();
			String groupName = fc.utobj().generateTestData("GroupName");
			fc.utobj().clickElement(driver, pobj1.addGroupsBtn);
			fc.commonMethods().switch_frameClass_newLayoutcboxIframe(driver);
			fc.utobj().sendKeys(driver, pobj1.groupName, groupName);
			fc.utobj().sendKeys(driver, pobj1.groupDescription, groupName + " Description");
			fc.utobj().clickElement(driver, pobj1.addBtn);
			fc.utobj().switchFrameToDefault(driver);
			fsMod.leadManagement(driver);
			p3.searchByLeadName(driver, firstName, lastName);
			fc.utobj().clickElement(driver, pobj.checkBoxAll);
			fc.utobj().printTestStep("Add to Group from bottom button");
			fc.utobj().clickElement(driver, pobj.addToGroupBottomBtn);
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,(".//td[contains(text(),'" + groupName + "')]/ancestor::tr/td/input[@name='groups']")));
			fc.utobj().clickElement(driver, pobj.addToGroupCboxBtn);

			if (FranconnectUtil.config.get("i18Files") != null) {
				fc.utobj().clickElement(driver, pobj.okCboxBtn);
			} else {
				fc.utobj().clickElement(driver, pobj.okCboxBtn2fr);
			}

			fc.utobj().printTestStep("Go to Sales > Campaign Center");
			FSCampaignCenterPage campaignCenterPage = new FSCampaignCenterPage(driver);
			fsMod.emailCampaignTab(driver);
			fc.utobj().printTestStep("Fill Campaign Details");
			String campaignName = fc.utobj().generateTestData("campaignName");
			String camapignDescription = fc.utobj().generateTestData(campaignName);
			fc.utobj().printTestStep("Create > Campaign");
			try {
				fc.utobj().clickElement(driver, campaignCenterPage.manageCampaignBtn);
				fc.utobj().clickElement(driver, campaignCenterPage.btnCreateCampaign);
			} catch (Exception e) {
				fc.utobj().clickElement(driver, campaignCenterPage.createCampaignBtn);
			}

			fc.commonMethods().switch_frameClass_newLayoutcboxIframe(driver);
			fc.utobj().sendKeys(driver, campaignCenterPage.campaignName, campaignName);
			fc.utobj().sendKeys(driver, campaignCenterPage.description, camapignDescription);
			fc.utobj().clickElement(driver, campaignCenterPage.startBtn);

			String templateName = fc.utobj().generateTestData("templateName");
			String emailSubject = fc.utobj().generateTestData("emailSubject");
			String plainTextEditor = fc.utobj().generateTestData("plainTextEditor");

			fc.utobj().clickElement(driver, campaignCenterPage.codeYourOwn);
			fc.utobj().sendKeys(driver, campaignCenterPage.templateNameTitle, templateName);
			fc.utobj().sendKeys(driver, campaignCenterPage.emailSubject, emailSubject);

			fc.utobj().clickElement(driver, campaignCenterPage.CodeOwnplainTextEmailType);
			fc.utobj().sendKeys(driver, campaignCenterPage.textAreaPlainText, plainTextEditor);

			fc.utobj().clickElement(driver, campaignCenterPage.saveAndContinue);

			try {
				fc.utobj().clickElement(driver,
						fc.utobj().getElementByXpath(driver, ".//label[@class='checkbox-lbl']"));
			} catch (Exception e) {
			}
			fc.utobj().printTestStep(
					"Create and Customize Templates > Code your own > Fill template details > Select Recipients > Continue > Launch Campaign");
			fc.utobj().clickElement(driver, campaignCenterPage.continueBtn);
			fc.utobj().clickElement(driver, campaignCenterPage.LaunchCampaignBtn);
			fsMod.leadManagement(driver);
			try {
				p3.searchByLeadName(driver, firstName, lastName);
			} catch (Exception e) {
			}
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//*[contains(text(),'" + firstName + " " + lastName + "')]"));
			boolean isCampaignAssociated = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//*[@id='for_bar']//td[contains(text(),'Active Campaign')]/following-sibling::td[contains(text(),'"
							+ campaignName + "')]");
			if (isCampaignAssociated == false) {
				fc.utobj().throwsException("Campaign association with lead not verified");
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "sales", "TC_Campaign_001" }) // Verified : Akshat
	@TestCase(createdOn = "2017-05-01", updatedOn = "2017-06-06", testCaseId = "TC_Campaign_001", testCaseDescription = "Sales > Campaign Center > Campaigns	> Create Template > Code your own > Finish > Save and Continue > Associate Later > Launch Campaign")
	private void verifyFsCampaigns() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("sales", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			fc.sales().sales_common().fsModule(driver);
			Sales fsMod = new Sales();
			FSCampaignCenterPage campaignCenterPage = new FSCampaignCenterPage(driver);
			fsMod.emailCampaignTab(driver);
			fc.utobj().printTestStep(
					"Sales > Campaign Center > Campaigns	> Create Template > Code your own > Finish > Save and Continue > Associate Later > Launch Campaign");
			String campaignName = fc.utobj().generateTestData("campaignName");
			String camapignDescription = fc.utobj().generateTestData(campaignName);
			String newCcampaignName = fc.utobj().generateTestData("campaignName");
			String newCamapignDescription = fc.utobj().generateTestData(campaignName);
			String templateName = fc.utobj().generateTestData("templateName");
			String emailSubject = fc.utobj().generateTestData("emailSubject");
			String plainTextEditor = fc.utobj().generateTestData("plainTextEditor");

			try{
				fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//*[contains(text(),'Create Template')]"));
			} catch(Exception e) {
				fc.utobj().clickElement(driver, campaignCenterPage.managetTemplateBtn);
				fc.utobj().clickElement(driver, campaignCenterPage.createTemplateBtn);
			}

			fc.utobj().clickElement(driver, campaignCenterPage.codeYourOwn);

			fc.utobj().sendKeys(driver, campaignCenterPage.templateNameTitle, templateName);
			fc.utobj().sendKeys(driver, campaignCenterPage.emailSubject, emailSubject);

			fc.utobj().clickElement(driver, campaignCenterPage.CodeOwnplainTextEmailType);
			fc.utobj().sendKeys(driver, campaignCenterPage.textAreaPlainText, plainTextEditor);
			fc.utobj().clickElement(driver, campaignCenterPage.saveBtn);

			fsMod.emailCampaignTab(driver);

			/*fc.utobj().clickElement(driver, campaignCenterPage.manageCampaignBtn);
			fc.utobj().clickElement(driver, campaignCenterPage.btnCreateCampaign);
*/
			try{
				String str = fc.utobj().getText(driver, campaignCenterPage.manageCampaignBtn);
				if("Manage Campaigns".equalsIgnoreCase(str))
				{
					fc.utobj().clickElement(driver, campaignCenterPage.manageCampaignBtn);
					fc.utobj().clickElement(driver, campaignCenterPage.btnCreateCampaign);
				}
				else fc.utobj().clickElement(driver, campaignCenterPage.manageCampaignBtn);
			}
			catch(Exception e){
				Reporter.log(e.getMessage());
			}
			
			fc.commonMethods().switch_frameClass_newLayoutcboxIframe(driver);

			fc.utobj().sendKeys(driver, campaignCenterPage.campaignName, campaignName);
			fc.utobj().sendKeys(driver, campaignCenterPage.description, camapignDescription);
			// fc.utobj().selectDropDown(driver,
			// campaignCenterPage.campaignAccessibility, accessibility);
			fc.utobj().clickElement(driver, campaignCenterPage.startBtn);

			fc.utobj().clickElement(driver, campaignCenterPage.chooseFromExisting);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//td[contains(text(),'" + templateName + "')]/ancestor::tr/td/label[@class='checkbox-lbl']"));
			fc.utobj().clickElement(driver, campaignCenterPage.associateAndContinueBtn);
			// fc.utobj().clickElement(driver,
			// campaignCenterPage.saveAndContinue);
			fc.utobj().clickElement(driver, campaignCenterPage.codeOwnAssociateLater);
			fc.utobj().clickElement(driver, campaignCenterPage.LaunchCampaignBtn);

			fc.utobj().printTestStep("Click on Campaign Name > Top Right > ");

			fc.utobj().clickElement(driver, campaignCenterPage.veiwCampaings);

			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//a[contains(text(),'" + campaignName + "')]"));

			fc.utobj().clickElement(driver, campaignCenterPage.actionBtn);
			fc.utobj().printTestStep("Verify Modify");
			fc.utobj().clickElement(driver, campaignCenterPage.ModifyCampaign);
			fc.utobj().clickElement(driver, campaignCenterPage.modifyBasicInfo);
			fc.commonMethods().switch_frameClass_newLayoutcboxIframe(driver);
			fc.utobj().sendKeys(driver, campaignCenterPage.campaignName, newCcampaignName);
			fc.utobj().sendKeys(driver, campaignCenterPage.description, newCamapignDescription);
			fc.utobj().clickElement(driver, campaignCenterPage.saveModifyCbox);

			fc.utobj().switchFrameToDefault(driver);
			fc.utobj().clickElement(driver, campaignCenterPage.saveModifyCampaign);

			fc.utobj().clickElement(driver, campaignCenterPage.veiwCampaings);

			fc.utobj().clickElement(driver, campaignCenterPage.campaignFilter);
			fc.utobj().sendKeys(driver, campaignCenterPage.searchCampaignName, newCcampaignName);
			fc.utobj().clickElement(driver, campaignCenterPage.searchFilter);

			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//a[contains(text(),'" + newCcampaignName + "')]"));
			boolean isCampaignModified = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//div[contains(text(),'Name')]/following-sibling::div[contains(text(),'" + newCcampaignName
							+ "')]");
			if (isCampaignModified == false) {
				fc.utobj().throwsException("Campaingn modifacation not verified");
			}
			String emailId = "salesautomation@staffex.com";
			fc.utobj().printTestStep("Verify Test Campaign");
			fc.utobj().clickElement(driver, campaignCenterPage.actionBtn);
			fc.utobj().clickElement(driver, campaignCenterPage.testCampaign);
			fc.commonMethods().switch_frameClass_newLayoutcboxIframe(driver);
			fc.utobj().sendKeys(driver, campaignCenterPage.testEmailId, emailId);
			fc.utobj().clickElement(driver, campaignCenterPage.testBtn);

			fc.utobj().clickElement(driver, campaignCenterPage.closeCbox);

			fc.utobj().printTestStep("Verifying test campaign email");
			String expectedSubject = emailSubject;
			String expectedMessageBody = plainTextEditor;

			Map<String, String> mailData = fc.utobj().readMailBox(expectedSubject, expectedMessageBody, emailId,
					"sdg@1a@Hfs");
			System.out.println(mailData);

			if (!mailData.get("mailBody").contains(expectedMessageBody)) {
				fc.utobj().throwsSkipException("was not able to verify Lead First Name in owner assignment email");
			}

			if (!mailData.get("subject").contains(expectedSubject)) {
				fc.utobj().throwsSkipException("was not able to verify lead Last Name in owner assignment email");
			}
			fc.utobj().printTestStep("Verify Archive > Then Go to Archive Campaign Section > Click Un-archive ");
			fc.utobj().clickElement(driver, campaignCenterPage.actionBtn);
			fc.utobj().clickElement(driver, campaignCenterPage.archiveCampaign);
			fc.utobj().acceptAlertBox(driver);
			fc.utobj().clickElement(driver, campaignCenterPage.archivedCampaingsTab);

			boolean verifyArchivedCampaigns = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text(),'" + newCcampaignName + "')]");
			if (verifyArchivedCampaigns == false) {
				fc.utobj().throwsException("Archive Campaign not verifried");
			}
			fc.utobj().printTestStep("Verify the Campaign is un-archived");
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='detailActionMenu']/div"));
			fc.utobj().clickElement(driver, campaignCenterPage.unarchiveAction);
			fc.utobj().acceptAlertBox(driver);
			fc.utobj().clickElement(driver, campaignCenterPage.CampaingsTab);

			verifyArchivedCampaigns = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//*[contains(text(),'" + newCcampaignName + "')]");
			if (verifyArchivedCampaigns == false) {
				fc.utobj().throwsException("Unarchive Campaign not verifried");
			}

			fc.utobj().clickElement(driver, campaignCenterPage.campaignFilter);
			fc.utobj().sendKeys(driver, campaignCenterPage.searchCampaignName, newCcampaignName);
			fc.utobj().clickElement(driver, campaignCenterPage.searchFilter);

			// fc.utobj().clickElement(driver,
			// fc.utobj().getElementByXpath(driver,".//a[contains(text(),'"+newCcampaignName+"')]")));
			fc.utobj().getElementByXpath(driver, ".//a[contains(text(),'" + newCcampaignName + "')]").click();

			fc.utobj().printTestStep("Verify Delete");
			fc.utobj().clickElement(driver, campaignCenterPage.actionBtn);
			fc.utobj().clickElement(driver, campaignCenterPage.deleteCampaign);

			fc.utobj().acceptAlertBox(driver);

			verifyArchivedCampaigns = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text(),'" + newCcampaignName + "')]");
			if (verifyArchivedCampaigns == true) {
				fc.utobj().throwsException("Delete Campaign not verifried");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			// e.printStackTrace();
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "sales", "TC_Campaign_002" }) // Verified : Akshat
	@TestCase(createdOn = "2017-05-01", updatedOn = "2017-06-13", testCaseId = "TC_Campaign_002", testCaseDescription = "Validate Fdd Email in the activity history > Validate email in mail box >Validate fdd login and download ")
	private void verifyFsCampaignTemplate() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("sales", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			fc.utobj().printTestStep(
					"Sales > Campaign Center > Campaigns	> Create Campaign> Code your own > Finish > Save and Continue > Associate Later > Launch Campaign");
			fc.sales().sales_common().fsModule(driver);
			Sales fsMod = new Sales();
			FSCampaignCenterPage campaignCenterPage = new FSCampaignCenterPage(driver);
			fsMod.emailCampaignTab(driver);

			String campaignName = fc.utobj().generateTestData("campaignName");
			String camapignDescription = fc.utobj().generateTestData(campaignName);
			String newCcampaignName = fc.utobj().generateTestData("New Campaign Name");
			String newCamapignDescription = fc.utobj().generateTestData("New Campaign Description");

			try{
				String str = fc.utobj().getText(driver, campaignCenterPage.manageCampaignBtn);
				if("Manage Campaigns".equalsIgnoreCase(str))
				{
					fc.utobj().clickElement(driver, campaignCenterPage.manageCampaignBtn);
					fc.utobj().clickElement(driver, campaignCenterPage.btnCreateCampaign);
				}
				else fc.utobj().clickElement(driver, campaignCenterPage.manageCampaignBtn);
			}
			catch(Exception e){
				Reporter.log(e.getMessage());
			}
			
			fc.commonMethods().switch_frameClass_newLayoutcboxIframe(driver);

			fc.utobj().sendKeys(driver, campaignCenterPage.campaignName, campaignName);
			fc.utobj().sendKeys(driver, campaignCenterPage.description, camapignDescription);
			fc.utobj().clickElement(driver, campaignCenterPage.startBtn);

			String emailSubject = fc.utobj().generateTestData("emailSubject");
			String plainTextEditor = fc.utobj().generateTestData("plainTextEditor");

			fc.utobj().clickElement(driver, campaignCenterPage.codeYourOwn);
			fc.utobj().sendKeys(driver, campaignCenterPage.templateNameTitle, campaignName);
			fc.utobj().sendKeys(driver, campaignCenterPage.emailSubject, emailSubject);

			fc.utobj().clickElement(driver, campaignCenterPage.CodeOwnplainTextEmailType);
			fc.utobj().sendKeys(driver, campaignCenterPage.textAreaPlainText, plainTextEditor);
			fc.utobj().clickElement(driver, campaignCenterPage.saveAndContinue);
			fc.utobj().clickElement(driver, campaignCenterPage.codeOwnAssociateLater);
			fc.utobj().clickElement(driver, campaignCenterPage.LaunchCampaignBtn);

			fc.utobj().printTestStep("Click on Campaign Name > Top Right > ");
			fc.utobj().printTestStep("Verify Modify");

			fc.utobj().clickElement(driver, campaignCenterPage.veiwCampaings);

			WebElement campaignNameToBeClicked = fc.utobj().getElementByXpath(driver, ".//a[contains(text(),'"+campaignName+"')]");
			fc.utobj().clickElement(driver, campaignNameToBeClicked);
			fc.utobj().clickElement(driver, campaignCenterPage.actionBtn);
			fc.utobj().clickElement(driver, campaignCenterPage.ModifyCampaign);
			fc.utobj().clickElement(driver, campaignCenterPage.modifyBasicInfo);

			fc.commonMethods().switch_frameClass_newLayoutcboxIframe(driver);

			fc.utobj().sendKeys(driver, campaignCenterPage.campaignName, newCcampaignName);
			fc.utobj().sendKeys(driver, campaignCenterPage.description, newCamapignDescription);
			fc.utobj().clickElement(driver, campaignCenterPage.saveModifyCbox);
			fc.utobj().switchFrameToDefault(driver);
			fc.utobj().clickElement(driver, campaignCenterPage.saveModifyCampaign);

			boolean isCampaignModified = fc.utobj().assertPageSource(driver, newCcampaignName);
			if (isCampaignModified == false) {
				fc.utobj().throwsException("Campaingn modifacation not verified");
			}
			String emailId = "salesautomation@staffex.com";
			fc.utobj().printTestStep("Verify Test Campaign");

			fc.utobj().clickElement(driver, campaignCenterPage.veiwCampaings);

			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//a[contains(text(),'" + newCcampaignName + "')]"));
			fc.utobj().clickElement(driver, campaignCenterPage.actionBtn);
			fc.utobj().clickElement(driver, campaignCenterPage.testCampaign);
			fc.commonMethods().switch_frameClass_newLayoutcboxIframe(driver);
			fc.utobj().sendKeys(driver, campaignCenterPage.testEmailId, emailId);
			fc.utobj().clickElement(driver, campaignCenterPage.testBtn);
			fc.utobj().clickElement(driver, campaignCenterPage.closeCbox);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Verifying test campaign email");
			String expectedSubject = emailSubject;
			String expectedMessageBody = plainTextEditor;

			Map<String, String> mailData = fc.utobj().readMailBox(expectedSubject, expectedMessageBody, emailId,
					"sdg@1a@Hfs");
			System.out.println(mailData);

			if (!mailData.get("mailBody").contains(plainTextEditor)) {
				fc.utobj().throwsSkipException("was not able to verify Lead First Name in owner assignment email");
			}

			if (!mailData.get("subject").contains(expectedSubject)) {
				fc.utobj().throwsSkipException("was not able to verify lead Last Name in owner assignment email");
			}
			fc.utobj().printTestStep("Verify Archive > Then Go to Archive Campaign Section > Click Un-archive ");
			fc.utobj().clickElement(driver, campaignCenterPage.actionBtn);
			fc.utobj().clickElement(driver, campaignCenterPage.archiveCampaign);
			fc.utobj().acceptAlertBox(driver);
			fc.utobj().clickElement(driver, campaignCenterPage.archivedCampaingsTab);

			boolean verifyArchivedCampaigns = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text(),'" + newCcampaignName + "')]");
			if (verifyArchivedCampaigns == false) {
				fc.utobj().throwsException("Archive Campaign not verifried");
			}
			fc.utobj().printTestStep("Verify the Campaign is un-archived");
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='detailActionMenu']/div"));
			fc.utobj().clickElement(driver, campaignCenterPage.unarchiveAction);
			fc.utobj().acceptAlertBox(driver);
			fc.utobj().clickElement(driver, campaignCenterPage.CampaingsTab);
			fc.utobj().printTestStep("Verify Delete");
			WebElement campaingnClick = driver
					.findElement(By.xpath(".//a[contains(text(),'" + newCcampaignName + "')]"));
			campaingnClick.click();
			verifyArchivedCampaigns = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//*[contains(text(),'" + newCcampaignName + "')]");
			if (verifyArchivedCampaigns == false) {
				fc.utobj().throwsException("Unarchive Campaign not verifried");
			}
			// fc.utobj().clickElement(driver,
			// fc.utobj().getElementByXpath(driver,".//a[contains(text(),'"+newCcampaignName+"')]")));
			fc.utobj().clickElement(driver, campaignCenterPage.actionBtn);
			fc.utobj().clickElement(driver, campaignCenterPage.deleteCampaign);
			fc.utobj().acceptAlertBox(driver);
			verifyArchivedCampaigns = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//a[contains(text(),'" + newCcampaignName + "')]");
			if (verifyArchivedCampaigns == true) {
				fc.utobj().throwsException("Delete Campaign not verifried");
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			// e.printStackTrace();
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "sales" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "TC_Campaign_RecipientsGroup_001", testCaseDescription = "Validate Fdd Email in the activity history > Validate email in mail box >Validate fdd login and download ")
	private void verifyFscampaignRecipientGroup() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("sales", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			fc.utobj().printTestStep(
					"Go to Sales > Campaign Center > Top Right button > Create > click on Recipients Group");
			fc.sales().sales_common().fsModule(driver);
			Sales fsMod = new Sales();
			FSCampaignCenterPage campaignCenterPage = new FSCampaignCenterPage(driver);
			fsMod.emailCampaignTab(driver);
			fc.utobj().printTestStep("Enter data and Submit");
			String groupName = fc.utobj().generateTestData("Group Name");
			String groupDescription = fc.utobj().generateTestData("Group Description");
			fc.utobj().clickElement(driver, campaignCenterPage.createBtn);
			fc.utobj().clickElement(driver, campaignCenterPage.recipientGroup);
			fc.commonMethods().switch_frameClass_newLayoutcboxIframe(driver);
			fc.utobj().sendKeys(driver, campaignCenterPage.groupName, groupName);
			fc.utobj().sendKeys(driver, campaignCenterPage.groupDescription, groupDescription);
			fc.utobj().clickElement(driver, campaignCenterPage.createGroupBtn);
			fc.utobj().printTestStep("Page should redirect to groups tab > Search Group");

			boolean isGroupPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//*[contains(text(),'" + groupName + "')]");
			if (isGroupPresent == false) {
				fc.utobj().throwsException("Recipient Group not found on groups page");
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "sales_validate" })
	@TestCase(createdOn = "2017-11-08", updatedOn = "2017-11-08", testCaseId = "TC_Solr_Search_Campaign_001", testCaseDescription = " Top 5 Email Campaigns should be visible with 'More' link @ bottom Solr Search and if user clicks on any record below details should be visible")
	private void checkSolrSearchforCampaigns() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("sales", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			fc.utobj().printTestStep("Sales > Campaigns> Create Campaigns");
			fc.sales().sales_common().fsModule(driver);

			SearchUI search_page = new SearchUI(driver);

			String EmailCampaignPre = fc.utobj().generateTestData(dataSet.get("EmailCampaign"));
			String EmailCampaign1 = EmailCampaignPre + dataSet.get("EmailCampaign1");
			String EmailCampaign2 = EmailCampaignPre + dataSet.get("EmailCampaign2");
			String EmailCampaign3 = EmailCampaignPre + dataSet.get("EmailCampaign3");
			String EmailCampaign4 = EmailCampaignPre + dataSet.get("EmailCampaign4");
			String EmailCampaign5 = EmailCampaignPre + dataSet.get("EmailCampaign5");
			String EmailCampaign6 = EmailCampaignPre + dataSet.get("EmailCampaign6");
			String EmailCampaign7 = EmailCampaignPre + dataSet.get("EmailCampaign7");
			String EmailCampaign8 = EmailCampaignPre + dataSet.get("EmailCampaign8");

			String template1 = fc.utobj().generateTestData(dataSet.get("template"));
			String template2 = fc.utobj().generateTestData(dataSet.get("template"));
			String template3 = fc.utobj().generateTestData(dataSet.get("template"));
			String template4 = fc.utobj().generateTestData(dataSet.get("template"));
			String template5 = fc.utobj().generateTestData(dataSet.get("template"));
			String template6 = fc.utobj().generateTestData(dataSet.get("template"));
			String template7 = fc.utobj().generateTestData(dataSet.get("template"));
			String template8 = fc.utobj().generateTestData(dataSet.get("template"));

			// String campaignName = dataSet.get("campaignName");
			String emailSubject = dataSet.get("emailSubject");
			String accessibility = dataSet.get("accessibility");
			String camapignDescription = dataSet.get("camapignDescription");

			fc.utobj().printTestStep("Add 8 email campaign");
			createEmailCampaignCodeYourOwnTemp(driver, EmailCampaign1, camapignDescription, accessibility, template1,
					emailSubject);
			createEmailCampaignCodeYourOwnTemp(driver, EmailCampaign2, camapignDescription, accessibility, template2,
					emailSubject);
			createEmailCampaignCodeYourOwnTemp(driver, EmailCampaign3, camapignDescription, accessibility, template3,
					emailSubject);
			createEmailCampaignCodeYourOwnTemp(driver, EmailCampaign4, camapignDescription, accessibility, template4,
					emailSubject);
			createEmailCampaignCodeYourOwnTemp(driver, EmailCampaign5, camapignDescription, accessibility, template5,
					emailSubject);
			createEmailCampaignCodeYourOwnTemp(driver, EmailCampaign6, camapignDescription, accessibility, template6,
					emailSubject);
			createEmailCampaignCodeYourOwnTemp(driver, EmailCampaign7, camapignDescription, accessibility, template7,
					emailSubject);
			createEmailCampaignCodeYourOwnTemp(driver, EmailCampaign8, camapignDescription, accessibility, template8,
					emailSubject);

			boolean isSearchTrue = false;
			boolean isVeiwAll = false;
			for (int i = 0; i < 3; i++) {
				if (isSearchTrue == false) {
					fc.utobj().sendKeys(driver, search_page.topSearchField, EmailCampaignPre);

					if (i == 2) {

					}
					fc.utobj().clickEnterOnElement(driver, search_page.topSearchField);

					fc.utobj().moveToElement(driver, fc.utobj().getElementByXpath(driver,
							".//span/*[contains(text(),'" + EmailCampaign8 + "')]"));
					isSearchTrue = true;
				}
			}
			if (isSearchTrue == false) {
				fc.utobj().throwsException("was not able to search email campaign through Top Search");
			}
			// fc.utobj().moveToElement(driver,
			// fc.utobj().getElementByXpath(driver,".//span/*[contains(text(),'"+brokerName2+"')]")));
			// fc.utobj().moveToElement(driver,
			// fc.utobj().getElementByXpath(driver,".//span/*[contains(text(),'"+brokerName3+"')]")));
			// fc.utobj().moveToElement(driver,
			// fc.utobj().getElementByXpath(driver,".//span/*[contains(text(),'"+brokerName8+"')]")));
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//a[@ng-click='viewAll()']"));

			isVeiwAll = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[contains(text(),'" + EmailCampaign1 + "')]");
			isVeiwAll = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[contains(text(),'" + EmailCampaign2 + "')]");
			isVeiwAll = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[contains(text(),'" + EmailCampaign3 + "')]");
			isVeiwAll = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[contains(text(),'" + EmailCampaign4 + "')]");
			isVeiwAll = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[contains(text(),'" + EmailCampaign5 + "')]");
			isVeiwAll = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[contains(text(),'" + EmailCampaign6 + "')]");
			isVeiwAll = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[contains(text(),'" + EmailCampaign7 + "')]");
			isVeiwAll = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[contains(text(),'" + EmailCampaign8 + "')]");
			if (isVeiwAll == false) {
				fc.utobj().throwsException("All elements not visible on clicking view all");
			}
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//a[contains(text(),'" + EmailCampaign1 + "')]"));
			boolean isBrokerVerified = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//*[contains(text(),'" + EmailCampaign1 + "')]");
			if (isBrokerVerified == false) {
				fc.utobj().throwsException("Email campaign not verified on click at search");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "sales" })
	@TestCase(createdOn = "2017-11-28", updatedOn = "2017-11-28", testCaseId = "TC_Sales_Infomercials_001", testCaseDescription = "Verify infomercials")
	private void infomercials001() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("sales", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			FSEmailCampaignsPage pobj = new FSEmailCampaignsPage(driver);
			FSCampaignCenterPage campaignCenterPage = new FSCampaignCenterPage(driver);
			fc.utobj().printTestStep("Navigate To Sales > Campaign Center");
			Sales fsmode = new Sales();
			fc.sales().sales_common().fsModule(driver);
			fsmode.emailCampaignTab(driver);

			fc.utobj().printTestStep("Navigate To Templates > Infomercials");
			fc.utobj().clickElement(driver, pobj.headerBtn);
			fc.utobj().clickElement(driver, pobj.templateOption);
			fc.utobj().clickElement(driver, pobj.informacialTab);
			fc.utobj().printTestStep("Create Infomercial");
			fc.utobj().clickElement(driver, pobj.createInfomercial);
			fc.utobj().switchFrame(driver, fc.utobj().getElement(driver, pobj.iframe));
			String informName = fc.utobj().generateTestData(dataSet.get("informName"));
			String informIdenty = fc.utobj().generateRandomChar();

			fc.utobj().sendKeys(driver, pobj.informName, informName);
			fc.utobj().sendKeys(driver, pobj.informIdenty, informIdenty);

			String uploadFilePath = fc.utobj().getFilePathFromTestData("TestFile01.flv");
			String thumbnailImagePath = fc.utobj().getFilePathFromTestData("thumbnail_icon01.jpg");

			fc.utobj().sendKeys(driver, pobj.fileNameUpload, uploadFilePath);
			fc.utobj().sendKeys(driver, pobj.uploadThumb, thumbnailImagePath);
			fc.utobj().clickElement(driver, pobj.saveBtn);
			fc.utobj().clickElement(driver, pobj.closeBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Verify Added Infomercials");
			viewAllInformacials(driver);

			boolean isinformNamePresent = fc.utobj().assertLinkPartialText(driver, informName);
			if (isinformNamePresent == false) {
				fc.utobj().throwsException("Not able to add Infomercials");
			}

			fc.utobj().printTestStep("Add Template");
			fc.utobj().clickElement(driver, pobj.emailTamplateTab);
			fc.utobj().clickElement(driver, campaignCenterPage.createTemplateTab);
			fc.utobj().clickElement(driver, campaignCenterPage.codeYourOwn);

			fc.utobj().clickElement(driver, campaignCenterPage.insertKeyWord);

			String attributeValue = driver
					.findElement(By.xpath(".//*[@id='select']//optgroup/option[contains(text(),'" + informName + "')]"))
					.getAttribute("value");

			fc.utobj().selectDropDownByValue(driver, campaignCenterPage.keywordsDropDown, attributeValue);
			fc.utobj().clickElement(driver, campaignCenterPage.copyKeywordValue);
			Actions actions = new Actions(driver);
			actions.sendKeys(Keys.chord(Keys.LEFT_CONTROL, "a")).build().perform();
			actions.sendKeys(Keys.chord(Keys.LEFT_CONTROL, "c")).build().perform();

			fc.utobj().switchFrameById(driver, "ta_ifr");
			fc.utobj().getElement(driver, campaignCenterPage.editor).click(); // Set
																						// focus
																						// on
																						// target
																						// element
																						// by
																						// clicking
																						// on
																						// it

			// now paste your content from clipboard
			actions.sendKeys(Keys.chord(Keys.LEFT_CONTROL, "v")).build().perform();

			fc.utobj().printTestStep("Verify the infomercial keyword is available in keyword.");

			String pastedText = fc.utobj().getElement(driver, campaignCenterPage.editor).getText();
			fc.utobj().switchFrameToDefault(driver);

			if (!pastedText.contains(informIdenty.toUpperCase())) {
				fc.utobj().throwsException("Not able to verify Copy Keyword's Value");
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "sales" , "TC_Sales_Infomercials_002" })
	@TestCase(createdOn = "2017-11-28", updatedOn = "2017-11-28", testCaseId = "TC_Sales_Infomercials_002", testCaseDescription = "Verify Infomercials Add , Modify and Delete Operation")
	private void infomercials002() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("sales", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			FSEmailCampaignsPage pobj = new FSEmailCampaignsPage(driver);
			fc.utobj().printTestStep("Navigate To Sales > Campaign Center");
			Sales fsmode = new Sales();
			fc.sales().sales_common().fsModule(driver);
			fsmode.emailCampaignTab(driver);

			fc.utobj().printTestStep("Navigate To Templates > Infomercials");
			fc.utobj().clickElement(driver, pobj.headerBtn);
			fc.utobj().clickElement(driver, pobj.templateOption);
			fc.utobj().clickElement(driver, pobj.informacialTab);
			fc.utobj().printTestStep("Create Infomercial");
			fc.utobj().clickElement(driver, pobj.createInfomercial);
			fc.utobj().switchFrame(driver, fc.utobj().getElement(driver, pobj.iframe));
			String informName = fc.utobj().generateTestData(dataSet.get("informName"));
			String informIdenty = fc.utobj().generateRandomChar();

			fc.utobj().sendKeys(driver, pobj.informName, informName);
			fc.utobj().sendKeys(driver, pobj.informIdenty, informIdenty);

			String uploadFilePath = fc.utobj().getFilePathFromTestData("TestFile01.flv");
			String thumbnailImagePath = fc.utobj().getFilePathFromTestData("thumbnail_icon01.jpg");

			fc.utobj().sendKeys(driver, pobj.fileNameUpload, uploadFilePath);
			fc.utobj().sendKeys(driver, pobj.uploadThumb, thumbnailImagePath);
			fc.utobj().clickElement(driver, pobj.saveBtn);
			fc.utobj().clickElement(driver, pobj.closeBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Verify Added Infomercials");
			viewAllInformacials(driver);

			boolean isinformNamePresent = fc.utobj().assertLinkPartialText(driver, informName);
			if (isinformNamePresent == false) {
				fc.utobj().throwsException("Not able to add Infomercials");
			}

			fc.utobj().printTestStep("Modify Added Informacials");
			new FSGroupsPageTest().actionImgOption(driver, informName, "Modify");

			fc.utobj().switchFrame(driver, fc.utobj().getElement(driver, pobj.iframe));
			informName = fc.utobj().generateTestData("Minformcials");
			fc.utobj().sendKeys(driver, pobj.informName, informName);
			fc.utobj().sendKeys(driver, pobj.informIdenty, informIdenty);

			String uploadFilePath1 = fc.utobj().getFilePathFromTestData("TestFile02.flv");
			String thumbnailImagePath1 = fc.utobj().getFilePathFromTestData("thumbnail_icon01.jpg");

			fc.utobj().sendKeys(driver, pobj.fileNameUpload, uploadFilePath1);
			fc.utobj().sendKeys(driver, pobj.uploadThumb, thumbnailImagePath1);
			fc.utobj().clickElement(driver, pobj.saveBtn);
			fc.utobj().clickElement(driver, pobj.closeBtn);
			fc.utobj().switchFrameToDefault(driver);

			fc.utobj().printTestStep("Verify Modified Infomercials");
			viewAllInformacials(driver);

			boolean isinformNamePresent1 = fc.utobj().assertLinkPartialText(driver, informName);
			if (isinformNamePresent1 == false) {
				fc.utobj().throwsException("Not able to modify Infomercials");
			}

			fc.utobj().printTestStep("Delete Modified Informacials");
			new FSGroupsPageTest().actionImgOption(driver, informName, "Delete");
			fc.utobj().acceptAlertBox(driver);

			fc.utobj().printTestStep("Verify Modified Infomercials");
			viewAllInformacials(driver);

			boolean isinformNamePresent2 = fc.utobj().assertLinkPartialText(driver, informName);
			if (isinformNamePresent2 == true) {
				fc.utobj().throwsException("Not able to delete Informacial");
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	public String createEmailCampaignCodeYourOwnTemp(WebDriver driver, String campaignName, String camapignDescription,
			String accessibility, String templateName, String emailSubject) throws Exception {

		CRMCampaignCenterCampaignsPage pobj2 = new CRMCampaignCenterCampaignsPage(driver);
		AdminFranchiseSalesLeadStatusPageTest p1 = new AdminFranchiseSalesLeadStatusPageTest();
		FSCampaignCenterPage campaignCenterObj = new FSCampaignCenterPage(driver);
		Sales fsmod = new Sales();
		fc.sales().sales_common().fsModule(driver);
		fsmod.emailCampaignTab(driver);

		try {
			fc.utobj().clickElement(driver, campaignCenterObj.manageCampaignBtn);
			fc.utobj().clickElement(driver, campaignCenterObj.btnCreateCampaign);
		} catch (Exception e) {
			fc.utobj().clickElement(driver, campaignCenterObj.createCampaignBtn);
		}
		fc.commonMethods().switch_frameClass_newLayoutcboxIframe(driver);

		fc.utobj().sendKeys(driver, campaignCenterObj.campaignName, campaignName);
		fc.utobj().sendKeys(driver, campaignCenterObj.description, camapignDescription);
		fc.utobj().selectDropDown(driver, campaignCenterObj.campaignAccessibility, accessibility);
		fc.utobj().clickElement(driver, pobj2.startBtn);
		fc.utobj().switchFrameToDefault(driver);

		String plainTextEditor = fc.utobj().generateTestData("plainTextEditor");
		fc.utobj().clickElement(driver, campaignCenterObj.codeYourOwn);
		fc.utobj().sendKeys(driver, campaignCenterObj.templateNameTitle, templateName);
		fc.utobj().sendKeys(driver, campaignCenterObj.emailSubject, emailSubject);

		fc.utobj().clickElement(driver, campaignCenterObj.CodeOwnplainTextEmailType);
		fc.utobj().sendKeys(driver, campaignCenterObj.textAreaPlainText, plainTextEditor);

		fc.utobj().clickElement(driver, campaignCenterObj.saveAndContinue);
		fc.utobj().clickElement(driver, campaignCenterObj.codeOwnAssociateLater);
		fc.utobj().clickElement(driver, campaignCenterObj.LaunchCampaignBtn);

		return campaignName;
	}

	public void filterCampaignByCampaignName(WebDriver driver, String campaignName) throws Exception {
		FSCampaignCenterPage campaignCenterPage = new FSCampaignCenterPage(driver);
		fc.utobj().clickElement(driver, campaignCenterPage.campaignFilter);
		fc.utobj().sendKeys(driver, campaignCenterPage.searchCampaignName, campaignName);
		fc.utobj().clickElement(driver, campaignCenterPage.searchFilter);
	}

	public void viewAllInformacials(WebDriver driver) throws Exception {
		FSEmailCampaignsPage pobj = new FSEmailCampaignsPage(driver);

		fc.utobj().clickElement(driver, pobj.clickViewPerPage);
		fc.utobj().clickElement(driver, pobj.clickViewPerPage500);

	}

}
