package com.builds.test.fs;

import java.util.HashMap;
import java.util.Map;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.annotations.Test;

import com.builds.uimaps.fs.FSImportPage;
import com.builds.uimaps.fs.LeadSummaryUI;
import com.builds.uimaps.fs.SearchUI;
import com.builds.utilities.FranconnectUtil;
import com.builds.utilities.TestCase;

public class FSExportPageTest {
	FranconnectUtil fc = new FranconnectUtil();

	@Test(groups = { "salesank1234" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2018-06-14", testCaseId = "testcase123", testCaseDescription = "Validate Fdd Email in the activity history > Validate email in mail box >Validate fdd login and download ")
	private void verifyLogATask500Lead() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("sales", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			fc.sales().sales_common().fsModule(driver);
			Sales fsmod = new Sales();
			LeadSummaryUI pobj = new LeadSummaryUI(driver);
			FSImportPage importPage = new FSImportPage(driver);
			fsmod.exportPage(driver);
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//input[@value='Search Data']"));
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//input[@value='Export As Excel']"));
			fc.utobj().acceptAlertBox(driver);

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	public void setPrefrences(WebDriver driver) {

		String downloadFilepath = "D:\\DownloadFolder\\"/*
														 * config.get(
														 * "downloadFolder")
														 */;
		HashMap<String, Object> chromePrefs = new HashMap<String, Object>();
		chromePrefs.put("profile.default_content_settings.popups", 0);
		chromePrefs.put("download.default_directory", downloadFilepath);
		ChromeOptions options = new ChromeOptions();
		HashMap<String, Object> chromeOptionsMap = new HashMap<String, Object>();
		options.setExperimentalOption("prefs", chromePrefs);
		options.addArguments("--test-type");
		DesiredCapabilities cap = DesiredCapabilities.chrome();
		cap.setCapability(ChromeOptions.CAPABILITY, chromeOptionsMap);
		cap.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
		cap.setCapability(ChromeOptions.CAPABILITY, options);

		WebDriver driver1 = new ChromeDriver(cap);
		driver = driver1;
	}

	public void salesExportSelectTab(WebDriver driver, String tabName) throws Exception {
		SearchUI pobj = new SearchUI(driver);
		fc.utobj().clickElement(driver,
				fc.utobj().getElementByXpath(driver, ".//td[contains(text(),'" + tabName + "')]/input"));
		fc.utobj().clickElement(driver, pobj.searchData);
	}

	public void salesExportSearchRecordByGroupName(WebDriver driver, String groupName) throws Exception {

		SearchUI pobj = new SearchUI(driver);
		fc.utobj().selectValFromMultiSelectWithoutReset(driver, pobj.advSearchGrpDrpDwn, groupName);
		fc.utobj().clickElement(driver, pobj.viewData);
	}

}
