package com.builds.test.fs;

import java.util.HashMap;
import java.util.Map;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.Test;

import com.builds.test.salesTest.Sales_Common_New;
import com.builds.uimaps.fs.FSCampaignCenterPage;
import com.builds.uimaps.fs.FSGroupsPage;
import com.builds.uimaps.fs.FSLeadSummaryPrimaryInfoPage;
import com.builds.uimaps.fs.LeadSummaryUI;
import com.builds.utilities.FranconnectUtil;
import com.builds.utilities.TestCase;

class FSGroupsPageTest {
	FranconnectUtil fc = new FranconnectUtil();

	public String addGroup(WebDriver driver, String groupName) throws Exception {
		Sales p1 = new Sales();
		p1.groups(driver);
		FSGroupsPage pobj = new FSGroupsPage(driver);
		fc.utobj().printTestStep("Add Group");
		fc.utobj().clickElement(driver, pobj.addGroupsBtn);
		// fc.utobj().switchFrame(driver,
		// fc.CommonMethods().switch_frameClass_newLayoutcboxIframe(driver));
		fc.commonMethods().switch_frameClass_newLayoutcboxIframe(driver);
		fc.utobj().sendKeys(driver, pobj.groupName, groupName);
		fc.utobj().sendKeys(driver, pobj.groupDescription, groupName + " Description");
		fc.utobj().clickElement(driver, pobj.addBtn);

		searchGroup(driver, groupName);
		fc.utobj().assertSingleLinkText(driver, groupName);
		return groupName;
	}

	public String addGroup(WebDriver driver, String groupName, String accessibility) throws Exception {
		Sales p1 = new Sales();
		p1.groups(driver);
		FSGroupsPage pobj = new FSGroupsPage(driver);
		fc.utobj().printTestStep("Add Group");
		fc.utobj().clickElement(driver, pobj.addGroupsBtn);
		fc.commonMethods().switch_frameClass_newLayoutcboxIframe(driver);
		fc.utobj().sendKeys(driver, pobj.groupName, groupName);
		fc.utobj().sendKeys(driver, pobj.groupDescription, groupName + " Description");
		fc.utobj().selectDropDown(driver, pobj.accessibility, accessibility);
		fc.utobj().clickElement(driver, pobj.addBtn);

		searchGroup(driver, groupName);
		fc.utobj().assertSingleLinkText(driver, groupName);
		return groupName;
	}

	public String addGroupOnly(WebDriver driver, String groupName, String groupType) throws Exception {
		Sales p1 = new Sales();
		p1.groups(driver);
		FSGroupsPage pobj = new FSGroupsPage(driver);
		fc.utobj().printTestStep("Add Group");
		fc.utobj().clickElement(driver, pobj.addGroupsBtn);
		fc.commonMethods().switch_frameClass_newLayoutcboxIframe(driver);
		fc.utobj().sendKeys(driver, pobj.groupName, groupName);
		fc.utobj().sendKeys(driver, pobj.groupDescription, groupName + " Description");
		fc.utobj().selectDropDown(driver, pobj.accessibility, groupType);
		fc.utobj().clickElement(driver, pobj.addBtn);
		return groupName;
	}

	// Anukaran
	public String addSmartGroup(WebDriver driver, String groupName) throws Exception {

		fc.sales().sales_common().fsModule(driver);
		Sales p1 = new Sales();
		p1.groups(driver);
		FSGroupsPage pobj = new FSGroupsPage(driver);

		fc.utobj().clickElement(driver, pobj.addGroupsBtn);
		fc.commonMethods().switch_frameClass_newLayoutcboxIframe(driver);
		fc.utobj().sendKeys(driver, pobj.groupName, groupName);
		fc.utobj().sendKeys(driver, pobj.groupDescription, groupName + " Description");
		// fc.utobj().selectDropDownByVisibleText(driver,
		// pobj.accessibility,accessibility);
		fc.utobj().clickElement(driver, pobj.groupTypeSmart);
		fc.utobj().clickElement(driver, pobj.addBtn);
		fc.utobj().switchFrameToDefault(driver);
		return groupName;
	}

	public String searchGroup(WebDriver driver, String groupName) throws Exception {
		FSGroupsPage pobj = new FSGroupsPage(driver);
		fc.utobj().printTestStep("Click on Search Filters");
		fc.utobj().clickElement(driver, pobj.openSearchFilters);

		fc.utobj().printTestStep("Search for the group name : " + groupName);
		fc.utobj().sendKeys(driver, pobj.searchTxt, groupName);

		fc.utobj().clickElement(driver, pobj.searchBtn);

		return groupName;
	}

	@Test(groups = { "sales", "TC_FS_Groups_006" }) // ok
	@TestCase(createdOn = "2017-11-27", updatedOn = "2017-11-27", testCaseId = "TC_FS_Groups_006", testCaseDescription = "Verify The group already associated with the lead should come checked.")
	private void fsGroupLeadSearch006() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("sales", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			fc.utobj().printTestStep("Sales > Groups > Add Group");
			String groupName = fc.utobj().generateTestData(dataSet.get("groups"));

			fc.commonMethods().getModules().clickSalesModule(driver);
			addGroup(driver, groupName);
			fc.utobj().printTestStep("Add lead with above added group.");
			AddLeadFromAllSources addLead = new AddLeadFromAllSources();

			String firstName = fc.utobj().generateTestData("Firstname");
			String lastName = fc.utobj().generateTestData("Lastname");
			String country = "USA";
			String state = "Alabama";
			String emailId = "salesautomation@staffex.com";
			String leadOwner = "FranConnect Administrator";
			String leadSourceCategory = "Import";
			String leadSourceDetails = "None";

			Map<String, String> leadInfo = new HashMap<String, String>();
			leadInfo.put("firstName", firstName);
			leadInfo.put("lastName", lastName);
			leadInfo.put("country", country);
			leadInfo.put("state", state);
			leadInfo.put("email", emailId);
			leadInfo.put("leadOwner", leadOwner);
			leadInfo.put("leadSourceCategory", leadSourceCategory);
			leadInfo.put("leadSourceDetails", leadSourceDetails);
			leadInfo.put("group", leadSourceDetails);
			addLead.addLeadFromSystem(driver, leadInfo);
			associateLeadWithGroup(driver, firstName, lastName, groupName);

			fc.utobj().printTestStep("Search Lead By Name");
			FSSearchPageTest p3 = new FSSearchPageTest();
			p3.searchByLeadName(driver, firstName, lastName);

			fc.utobj().clickElement(driver, fc.utobj().getElementByPartialLinkText(driver, firstName));
			fc.utobj().printTestStep("Click on add to group from primary info > More Action");
			fc.utobj().selectMoreActionMenu(driver,
					fc.utobj().getElementByXpath(driver, ".//input[@value='More-Actions']"),
					driver.findElements(
							By.xpath(".//*[@id='actionListButtons1']/table/tbody/tr[2]/td[2]/table/tbody/tr/td")),
					"Add To Group");

			fc.utobj().printTestStep("The group already associated with the lead should come checked.");

			fc.commonMethods().switch_cboxIframe_frameId(driver);
			boolean isCheckBoxSelected = false;
			try {
				fc.utobj()
						.getElementByXpath(driver, ".//td[contains(text(),'" + groupName + "')]/ancestor::tr/td/input")
						.isSelected();
				isCheckBoxSelected = true;
			} catch (Exception e) {
				isCheckBoxSelected = false;
			}
			if (isCheckBoxSelected == false) {
				fc.utobj().throwsException("Group not associated with the lead");
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "sales", "TCFSGroups003" }) // Verified PASSED - akshat
	@TestCase(createdOn = "2017-11-27", updatedOn = "2017-11-27", testCaseId = "TC_FS_Groups_003", testCaseDescription = "Verify that by using Add to group button present on primary info page of a lead, the groups which are already associated with that particular lead should come checked.")
	private void fsGroupLeadSearch003() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("sales", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			fc.utobj().printTestStep("Sales > Groups > Add Group");
			String groupName = fc.utobj().generateTestData(dataSet.get("groups"));
			
			fc.commonMethods().getModules().clickSalesModule(driver);
			
			addGroup(driver, groupName);
			fc.utobj().printTestStep("Add lead with above added group.");
			AddLeadFromAllSources addLead = new AddLeadFromAllSources();

			String firstName = fc.utobj().generateTestData("Firstname");
			String lastName = fc.utobj().generateTestData("Lastname");
			String country = "USA";
			String state = "Alabama";
			String emailId = "salesautomation@staffex.com";
			String leadOwner = "FranConnect Administrator";
			String leadSourceCategory = "Import";
			String leadSourceDetails = "None";

			Map<String, String> leadInfo = new HashMap<String, String>();
			leadInfo.put("firstName", firstName);
			leadInfo.put("lastName", lastName);
			leadInfo.put("country", country);
			leadInfo.put("state", state);
			leadInfo.put("email", emailId);
			leadInfo.put("leadOwner", leadOwner);
			leadInfo.put("leadSourceCategory", leadSourceCategory);
			leadInfo.put("leadSourceDetails", leadSourceDetails);
			leadInfo.put("group", leadSourceDetails);
			addLead.addLeadFromSystem(driver, leadInfo);
			
			// To handle new navigation ui
			Sales_Common_New sales_Common_New = new Sales_Common_New();
			sales_Common_New.minimizeNotificationFooter_ifMaximised(driver);
			
			associateLeadWithGroup(driver, firstName, lastName, groupName);

			fc.utobj().printTestStep("Search Lead By Name");
			FSSearchPageTest p3 = new FSSearchPageTest();
			p3.searchByLeadName(driver, firstName, lastName);

			fc.utobj().clickElement(driver, fc.utobj().getElementByPartialLinkText(driver, firstName));
			fc.utobj().printTestStep("Click on add to group from primary info > Add to group button");
			fc.utobj().clickElement(driver, new FSLeadSummaryPrimaryInfoPage(driver).addToGroupBtn);

			fc.utobj().printTestStep("The group already associated with the lead should come checked.");

			fc.commonMethods().switch_cboxIframe_frameId(driver);
			boolean isCheckBoxSelected = false;
			try {
				fc.utobj()
						.getElementByXpath(driver, ".//td[contains(text(),'" + groupName + "')]/ancestor::tr/td/input")
						.isSelected();
				isCheckBoxSelected = true;
			} catch (Exception e) {
				isCheckBoxSelected = false;
			}
			if (isCheckBoxSelected == false) {
				fc.utobj().throwsException("Group not associated with the lead");
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "sales5241", "sales_group" })
	@TestCase(createdOn = "2017-11-27", updatedOn = "2017-11-27", testCaseId = "TC_FS_Groups_007", testCaseDescription = "Verify the campaign getting associated with lead of group")
	private void fsGroupLeadCampaign() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("sales", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			fc.utobj().printTestStep("Sales > Groups > Add Group");
			String groupName = fc.utobj().generateTestData(dataSet.get("groups"));

			addGroup(driver, groupName);
			fc.utobj().printTestStep("Add lead with above added group.");
			AddLeadFromAllSources addLead = new AddLeadFromAllSources();

			String firstName = fc.utobj().generateTestData("Firstname");
			String lastName = fc.utobj().generateTestData("Lastname");
			String country = "USA";
			String state = "Alabama";
			String emailId = "salesautomation@staffex.com";
			String leadOwner = "FranConnect Administrator";
			String leadSourceCategory = "Import";
			String leadSourceDetails = "None";

			Map<String, String> leadInfo = new HashMap<String, String>();
			leadInfo.put("firstName", firstName);
			leadInfo.put("lastName", lastName);
			leadInfo.put("country", country);
			leadInfo.put("state", state);
			leadInfo.put("email", emailId);
			leadInfo.put("leadOwner", leadOwner);
			leadInfo.put("leadSourceCategory", leadSourceCategory);
			leadInfo.put("leadSourceDetails", leadSourceDetails);
			leadInfo.put("group", leadSourceDetails);
			addLead.addLeadFromSystem(driver, leadInfo);
			associateLeadWithGroup(driver, firstName, lastName, groupName);

			fc.utobj().printTestStep("Navigate To Sales > Campaign Center");
			FSEmailCampaignsPageTest campaign_page = new FSEmailCampaignsPageTest();
			String campaignName = fc.utobj().generateTestData("campaignName");
			String camapignDescription = fc.utobj().generateTestData("camapignDescription");
			String templateName = fc.utobj().generateTestData("templateName");
			String emailSubject = fc.utobj().generateTestData("emailSubject");
			String accessibility = "Public";
			fc.utobj().printTestStep("Add Campaign");
			String createdCampaignName = campaign_page.createEmailCampaignCodeYourOwnTemp(driver, campaignName,
					camapignDescription, accessibility, templateName, emailSubject);

			fc.utobj().printTestStep("Go to Sales > Group > Associate Campaign with above created Group");
			Sales p1 = new Sales();
			p1.groups(driver);

			fc.utobj().printTestStep("Search Group");
			searchGroup(driver, groupName);

			fc.utobj().printTestStep("Associate Campaign With Group");
			actionImgOption(driver, groupName, "Associate with Campaign");
			FSCampaignCenterPage cPobj = new FSCampaignCenterPage(driver);
			campaign_page.filterCampaignByCampaignName(driver, createdCampaignName);
			fc.utobj().clickElement(driver, cPobj.associateCampaign);
			fc.utobj().clickElement(driver, cPobj.confirmBtn);

			fc.utobj().printTestStep("Verify That Campaign Should be associated to this group.");
			searchGroup(driver, groupName);

			boolean isCampaignNamePresent = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[contains(text(),'" + groupName
					+ "')]/ancestor::tr/td/a[contains(text(),'" + campaignName + "')]");

			if (isCampaignNamePresent == false) {
				fc.utobj().throwsException("campaign is not present after filter by group name");
			}

			fc.utobj().printTestStep("Click on the lead count of the group");
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
					".//a[contains(text(),'" + groupName + "')]/ancestor::tr/td/a[contains(@onclick , 'showLeads')]"));

			fc.utobj().printTestStep("Verify that the campaign is associated to this lead.");

			fc.utobj().clickPartialLinkText(driver, firstName);
			boolean isCampaignPresent = fc.utobj().assertPageSource(driver, campaignName);
			if (isCampaignPresent == false) {
				fc.utobj().throwsException("campaign is not assocaited with lead");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "sales", "TC_FS_Groups_009" })
	@TestCase(createdOn = "2017-11-27", updatedOn = "2017-11-27", testCaseId = "TC_FS_Groups_009", testCaseDescription = "Verify the Cancel Button at Add Group Page.")
	private void fsGroupOthers() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("sales", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		String groupName = fc.utobj().generateTestData("Test Group");

		try {
			driver = fc.loginpage().login(driver);

			fc.sales().sales_common().fsModule(driver);
			Sales p1 = new Sales();
			p1.groups(driver);
			FSGroupsPage pobj = new FSGroupsPage(driver);
			fc.utobj().printTestStep("Add Group");
			fc.utobj().clickElement(driver, pobj.addGroupsBtn);
			fc.commonMethods().switch_frameClass_newLayoutcboxIframe(driver);
			fc.utobj().sendKeys(driver, pobj.groupName, groupName);
			fc.utobj().sendKeys(driver, pobj.groupDescription, groupName + " Description");
			fc.utobj().clickElement(driver, pobj.cancelBtn);
			try {
				fc.utobj().clickElement(driver, pobj.addGroupsBtn);
			} catch (Exception cExp) {
				fc.utobj().throwsException(
						"After clicking on cancel button at add group button expected window was not found.");
			}

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	public void associateLeadWithGroup(WebDriver driver, String firstName, String lastName, String groupName)
			throws Exception {

		LeadSummaryUI leadPageTest = new LeadSummaryUI(driver);
		FSSearchPageTest p3 = new FSSearchPageTest();
		FSGroupsPage pobj = new FSGroupsPage(driver);
		p3.searchByLeadName(driver, firstName, lastName);
		fc.utobj().clickElement(driver,
				fc.utobj().getElementByXpath(driver, ".//a[contains(text(),'" + firstName + " " + lastName + "')]"));
		// fc.utobj().selectMoreActionMenu(driver,leadPageTest.moreActionsLink,
		// leadPageTest.menu, "Add To Group");
			
		fc.utobj().clickElement(driver, leadPageTest.addToGroupBottomBtn);

		fc.commonMethods().switch_cboxIframe_frameId(driver);

		fc.utobj().clickElement(driver, driver.findElement(
				By.xpath(".//td[contains(text(),'" + groupName + "')]/ancestor::tr/td/input[@type='checkbox']")));
		fc.utobj().clickElement(driver, pobj.addToGrpBtn);

		fc.utobj().clickElement(driver, pobj.okBtn);

		fc.utobj().switchFrameToDefault(driver);
	}

	public void actionImgOption(WebDriver driver, String groupName, String option) throws Exception {
		fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
				".//*[contains(text () , '" + groupName + "')]/ancestor::tr/td/div/div[@id='detailActionMenu']"));

		fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//a[contains(text(),'" + groupName
				+ "')]/ancestor::td/following-sibling::td/div/ul/li/a[contains(text(),'" + option + "')]"));
	}

}
