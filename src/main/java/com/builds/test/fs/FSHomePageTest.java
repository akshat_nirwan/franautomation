package com.builds.test.fs;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.builds.uimaps.fs.FSHomePage;
import com.builds.utilities.FranconnectUtil;

public class FSHomePageTest {
	FranconnectUtil fc = new FranconnectUtil();

	public void addLeadLnk(WebDriver driver) throws Exception {

		FSHomePage pobj = new FSHomePage(driver);
		try {
			fc.utobj().clickLink(driver, "Add Lead");
		} catch (Exception e) {
			try {
				fc.utobj().clickElement(driver, pobj.addLeadLnk);
			} catch (Exception e2) {
				fc.utobj().throwsException("Add lead link not found!");
			}
		}
	}

	public void manageWidgets(WebDriver driver, String widget) throws Exception {

		widget = widget.trim(); // remove space

		FSHomePage pobj = new FSHomePage(driver);
		fc.utobj().clickElement(driver, pobj.manageWidgets);

		if (widget.equalsIgnoreCase("Dash Board")) {
			fc.utobj().check(pobj.dashboardc, "yes");
			fc.utobj().clickElement(driver, pobj.saveWidgetBtn);

		} else if (widget.equalsIgnoreCase("Event Calendar")) {
			fc.utobj().check(pobj.eventCalenderc, "yes");
			fc.utobj().clickElement(driver, pobj.saveWidgetBtn);
		} else if (widget.equalsIgnoreCase("Hottest Leads")) {
			fc.utobj().check(pobj.heatMeterc, "yes");
			fc.utobj().clickElement(driver, pobj.saveWidgetBtn);
		} else if (widget.equalsIgnoreCase("My Forecasts")) {
			fc.utobj().check(pobj.forecastsc, "yes");
			fc.utobj().clickElement(driver, pobj.saveWidgetBtn);
		} else if (widget.equalsIgnoreCase("Recent Posts On Candidate Portal")) {
			fc.utobj().check(pobj.candidatePortalHomec, "yes");
			fc.utobj().clickElement(driver, pobj.saveWidgetBtn);
		} else if (widget.equalsIgnoreCase("Recently Viewed Leads")) {
			fc.utobj().check(pobj.recentlyViewedLeadsc, "yes");
			fc.utobj().clickElement(driver, pobj.saveWidgetBtn);
		} else if (widget.equalsIgnoreCase("Sales Funnel")) {
			fc.utobj().check(pobj.salesFunnelc, "yes");
			fc.utobj().clickElement(driver, pobj.saveWidgetBtn);
		} else if (widget.equalsIgnoreCase("Task View")) {
			fc.utobj().check(pobj.tasksFSc, "yes");
			fc.utobj().clickElement(driver, pobj.saveWidgetBtn);
		} else if (widget.equalsIgnoreCase("Today's Events")) {
			fc.utobj().check(pobj.todaysEventc, "yes");
			fc.utobj().clickElement(driver, pobj.saveWidgetBtn);
		} else {
			fc.utobj().throwsException("Widget not found");
		}

		driver.quit();
	}

	public void mostRecentLeads_15(WebDriver driver, String leadName) throws Exception {

		Sales fs = new Sales();
		fs.homePage(driver);

		FSHomePage pobj = new FSHomePage(driver);
		try {
			WebElement element = pobj.recentLeadDiv.findElement(By.xpath(".//a[contains(text(),'" + leadName + "')]"));
			fc.utobj().clickElement(driver, element);
		} catch (Exception e) {
			fc.utobj().throwsException("Lead Not Found in - 15 Most Recent Leads Section");
		}
	}
}
