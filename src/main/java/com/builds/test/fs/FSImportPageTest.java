package com.builds.test.fs;

import java.util.Map;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Test;

import com.builds.uimaps.fs.FSImportPage;
import com.builds.uimaps.fs.LeadSummaryUI;
import com.builds.utilities.FranconnectUtil;
import com.builds.utilities.TestCase;

public class FSImportPageTest {

	FranconnectUtil fc = new FranconnectUtil();

	@Test(groups = { "salesfail", "sales1" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2017-06-06", testCaseId = "TC_Sales_LogATask_500Lead", testCaseDescription = "Validate Fdd Email in the activity history > Validate email in mail box >Validate fdd login and download ")
	private void verifyLogATask500Lead() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		Map<String, String> dataSet = fc.utobj().readTestData("sales", testCaseId);
		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			fc.sales().sales_common().fsModule(driver);
			Sales fsmod = new Sales();
			LeadSummaryUI pobj = new LeadSummaryUI(driver);
			FSImportPage importPage = new FSImportPage(driver);
			fsmod.importPage(driver);
			String fileName = fc.utobj().getFilePathFromTestData(dataSet.get("fileName"));
			fc.utobj().printTestStep("Import Sheet of more than 500 lead of same name");
			fc.utobj().sendKeys(driver, importPage.importFile, fileName);
			fc.utobj().clickElement(driver, importPage.continueBtn);
			fc.utobj().selectDropDownByPartialText(driver, importPage.firstName, "First Name");
			fc.utobj().selectDropDownByPartialText(driver, importPage.lastName, "Last Name");
			fc.utobj().selectDropDownByPartialText(driver, importPage.email, "Email");
			fc.utobj().selectDropDownByPartialText(driver, importPage.companyName, "Company Name");
			fc.utobj().selectDropDownByPartialText(driver, importPage.address1, "Address");
			fc.utobj().selectDropDownByPartialText(driver, importPage.address2, "Address 2");
			fc.utobj().selectDropDownByPartialText(driver, importPage.city, "City");
			fc.utobj().selectDropDownByPartialText(driver, importPage.country, "Country");
			fc.utobj().selectDropDownByPartialText(driver, importPage.stateProvince, "State / Province");
			fc.utobj().selectDropDownByPartialText(driver, importPage.zipPostalCode, "Zip / Postal Code");
			fc.utobj().selectDropDownByPartialText(driver, importPage.workPhone, "Wrok Phone");
			fc.utobj().selectDropDownByPartialText(driver, importPage.workPhoneExt, "Work Phone Extension");
			fc.utobj().selectDropDownByPartialText(driver, importPage.homePhone, "Home Phone");
			fc.utobj().selectDropDownByPartialText(driver, importPage.homePhoneExt, "Home Phone Extension");
			fc.utobj().selectDropDownByPartialText(driver, importPage.fax, "Fax");
			fc.utobj().selectDropDownByPartialText(driver, importPage.comments, "Comments");
			fc.utobj().selectDropDownByPartialText(driver, importPage.mobile, "Mobile");
			fc.utobj().selectDropDownByPartialText(driver, importPage.bestTimetoContact, "Best Time To Contact");
			fc.utobj().selectDropDownByPartialText(driver, importPage.leadStatus, "Lead Status");
			fc.utobj().selectDropDownByPartialText(driver, importPage.leadSourceCatagory, "Lead Source Category");
			fc.utobj().selectDropDownByPartialText(driver, importPage.leadSourceDetails, "Lead Source Details");
			fc.utobj().selectDropDownByPartialText(driver, importPage.assignTo, "Assign To");
			fc.utobj().selectDropDownByPartialText(driver, importPage.preferedModeOfContact,
					"Preferred Mode of Contact");
			fc.utobj().selectDropDownByPartialText(driver, importPage.currentNetWorth, "Current Net Worth");
			fc.utobj().selectDropDownByPartialText(driver, importPage.cashAvailableForInves,
					"Cash Available for Investment");
			fc.utobj().selectDropDownByPartialText(driver, importPage.investmentTimeframe, "Investment Timeframe");
			fc.utobj().selectDropDownByPartialText(driver, importPage.sourceOfInvestment, "Source Of Investment");
			fc.utobj().selectDropDownByPartialText(driver, importPage.background, "Background");
			// fc.utobj().selectDropDownByPartialText(driver,
			// importPage.division, "Division");
			String inquiryDate = fc.utobj().getCurrentDateUSFormat();
			fc.utobj().sendKeys(driver, importPage.inquiryDate, inquiryDate);
			fc.utobj().selectDropDownByPartialText(driver, importPage.defaultLeadOwner, "FranConnect Administrator");
			fc.utobj().selectDropDownByPartialText(driver, importPage.defaultLeadStatus, "New Lead");
			fc.utobj().selectDropDownByPartialText(driver, importPage.defaultLeadSourceCatagory, "Import");
			fc.utobj().selectDropDownByPartialText(driver, importPage.defaultLeadSourceDetails, "None");
			fc.utobj().clickElement(driver, importPage.caanadianLeadDisclaimer);
			fc.utobj().clickElement(driver, importPage.continueBtn);
			fc.utobj().clickElement(driver, importPage.continueBtn);

			try {
				fc.utobj().clickElement(driver, importPage.backBtn);

			} catch (Exception e) {

				fc.utobj().clickElement(driver, importPage.backBtn);
			}

			// fsmod.leadManagement(driver);
			fsmod.search(driver);
			fc.utobj().printTestStep(
					"Go to Advanced Search > Include Activity History > Enter name and the remarks Leads added through import Search");
			fc.utobj().clickElement(driver, pobj.advancedSearch);
			fc.utobj().printTestStep("Search for 500 lead");
			fc.utobj().clickElement(driver, pobj.advSearchActivityHistoryRemarks);
			fc.utobj().clickElement(driver, pobj.searchData);
			fc.utobj().sendKeys(driver, pobj.searchReamrk, "Lead added from Import");
			fc.utobj().clickElement(driver, pobj.veiwDataBtn);

			fc.utobj().printTestStep("Change view per page to 500");
			String taskSubject = fc.utobj().generateTestData("TaskSubject");
			fc.utobj().selectDropDownByPartialText(driver, pobj.viewPerPage, "500");
			fc.utobj().clickElement(driver, pobj.selectAllCheckBox);
			fc.utobj().clickElement(driver, pobj.actionsBtn);
			fc.utobj().selectActionMenuItems(driver, "Log a Task");

			fc.utobj().printTestStep("Log a task");
			fc.utobj().printTestStep("Log A Task");
			fc.commonMethods().switch_cboxIframe_frameId(driver);
			fc.utobj().clickElement(driver,
					fc.utobj().getElementByXpath(driver, ".//*[@id='radioOwner' and @value='0']"));
			fc.utobj().selectDropDownByVisibleText(driver, fc.utobj().getElementByID(driver, "status"), "Not Started");
			fc.utobj().sendKeys(driver, fc.utobj().getElementByID(driver, "subject"), taskSubject);
			fc.utobj().clickElement(driver, fc.utobj().getElementByName(driver, "add"));

			fc.utobj().switchFrameToDefault(driver);
			fsmod.tasks(driver);
			boolean isTaskAdded = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[contains(text(),'" + taskSubject + "')]");
			if (isTaskAdded == false) {
				fc.utobj().throwsException("Task added was not verified at task page");
			}
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

}
