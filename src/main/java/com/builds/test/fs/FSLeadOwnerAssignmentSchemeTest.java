package com.builds.test.fs;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.builds.test.admin.AdminDivisionAddDivisionPageTest;
import com.builds.test.admin.AdminUsersManageCorporateUsersAddCorporateUserPageTest;
import com.builds.test.admin.AdminUsersManageRegionalUsersAddRegionalUserPageTest;
import com.builds.test.common.CorporateUser;
import com.builds.uimaps.fs.LeadSummaryUI;
import com.builds.utilities.FranconnectUtil;

public class FSLeadOwnerAssignmentSchemeTest {
	FranconnectUtil fc = new FranconnectUtil();

	public String getOwnerByDivison(WebDriver driver, String testCaseId, String UserType, String fromWhere,
			boolean addDataforroundrobin, boolean noownerset) throws Exception {

		String actualOner = "";
		String testCaseIdInternal = "" + testCaseId;
		Map<String, String> dataSet = fc.utobj().readTestData("sales", testCaseId);
		Map<String, String> printInfo = null;
		AddLeadFromAllSources addLeadFromAllSourecesObj = new AddLeadFromAllSources();
		LeadSummaryUI pobj = new LeadSummaryUI(driver);
		AdminUsersManageCorporateUsersAddCorporateUserPageTest corpTest = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
		AdminUsersManageRegionalUsersAddRegionalUserPageTest regUser = new AdminUsersManageRegionalUsersAddRegionalUserPageTest();
		String firstOwner = "";
		String secondOwner = "";
		String thirdOwner = "";
		List<String> listInfo = new ArrayList<String>();
		// Sales Territories

		String leadSource2ID = "Internet";// fc.utobj().generateTestData("ST");
		String divisionName = "Dios23171381";
		String zipCode = fc.utobj().generateRandomNumber();
		String country = "USA";
		String state = "Alaska";
		AdminDivisionAddDivisionPageTest division_page = new AdminDivisionAddDivisionPageTest();

		try {
			fc.adminpage().adminDivision_USManageDivision_USPage(driver);
			fc.utobj().printTestStep("Go to Admin >  Division > Manage Division ");
			boolean isSalesTeritoryalready = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//td[contains(text(),'" + divisionName + "')]");
			if (isSalesTeritoryalready == false) {
				division_page.addDivision(driver, divisionName);
				// fc.utobj().throwsException("Sales states teritory not got
				// added");
			} else {
				divisionName = "Dios23171381";
			}
		} catch (Exception e) {

		}

		fc.adminpage().adminPage(driver);

		AdminSales adsales = new AdminSales();
		adsales.assignLeadOwners(driver);
		List<WebElement> oRadioButton = driver.findElements(By.name("radiobutton"));
		boolean bValue = oRadioButton.get(3).isSelected();
		if (oRadioButton.get(3).isSelected()) {

		} else {
			oRadioButton.get(3).click();
		}

		fc.utobj().printTestStep("Click Continue");
		fc.utobj().clickElement(driver, pobj.continueButton);

		System.out.println("divisionName=====" + divisionName);

		try {
			boolean isSalesTeritoryalready = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//td[contains(text(),'" + divisionName
					+ "')]/following-sibling::td[contains(text(),'DivisionOwner')]");
			if (isSalesTeritoryalready == false) {

				// fc.utobj().throwsException("Sales states teritory not got
				// added");
			} else {
				fc.utobj().printTestStep("Delete Division Name");
				fc.utobj().actionImgOption(driver, divisionName, "Delete");
				fc.utobj().acceptAlertBox(driver);
				// division_page.addDivision(driver, divisionName, config);

			}
		} catch (Exception e) {

		}
		// End Lead Source

		if (addDataforroundrobin) {

			if ((leadSource2ID != null)) {
				for (int i = 0; i < 3; i++) {
					fc.adminpage().adminPage(driver);
					// adsales.assignLeadOwners(driver);

					String userNameOwner = "FranConnect Administrator";
					fc.utobj().printTestStep("Verify the Create user");
					String userNameRC = fc.utobj().generateTestData("IZ"); // "AssignOwner";
					userNameOwner = userNameRC + " " + userNameRC;
					String emailId = "salesautomation@staffex.com";
					if ("CorporateUser".equals(UserType)) {
						CorporateUser corpUser = new CorporateUser();
						corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
						corpUser.setEmail(emailId);
						corpUser = corpTest.createDefaultUser(driver, corpUser);
					} else if ("RegionalUser".equals(UserType)) {
						String regionName = fc.utobj().generateTestData("RG");
						regUser.addRegionalUser(driver, userNameRC, regionName, emailId);
						// userNameOwner = userNameRC+" "+userNameRC+" (RU)";
					}
					userNameOwner = userNameRC + " " + userNameRC;
					listInfo.add(userNameOwner);
					fc.utobj().printTestStep("Verify the Create user");
					fc.adminpage().adminPage(driver);
					adsales.assignLeadOwners(driver);

					fc.utobj().printTestStep("Verify the user is available in Default Owner drop down");

					boolean isLeadOwnerPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
							".//select[@name='area1']/option[contains(text(),'" + userNameOwner + "')]");
					if (isLeadOwnerPresent == false) {
						fc.utobj().throwsException("User not present in Default Owner dropdown");
					}

					oRadioButton = driver.findElements(By.name("radiobutton"));
					bValue = false;
					bValue = oRadioButton.get(3).isSelected();
					if (oRadioButton.get(3).isSelected()) {

					} else {
						oRadioButton.get(3).click();
					}

					fc.utobj().printTestStep("Click Continue");
					fc.utobj().clickElement(driver, pobj.continueButton);

					boolean availableLeadOwner = fc.utobj().verifyElementOnVisible_ByXpath(driver,
							".//tr/td[contains(text(),'" + divisionName
									+ "')]/following-sibling::td/select/option[contains(text(),'" + userNameOwner
									+ "')]");
					if (availableLeadOwner == false) {
						fc.utobj().throwsException("User is  not available in Sales Terriotries Lead Owners drop-down");
					}

				}

				fc.utobj().printTestStep("fill the owners with respect to sale terriortires drop down");
				fc.adminpage().adminPage(driver);
				adsales.assignLeadOwners(driver);

				oRadioButton = driver.findElements(By.name("radiobutton"));
				bValue = false;
				bValue = oRadioButton.get(3).isSelected();
				if (oRadioButton.get(3).isSelected()) {

				} else {
					oRadioButton.get(3).click();
				}

				fc.utobj().printTestStep("Click Continue");
				fc.utobj().clickElement(driver, pobj.continueButton);

				fc.utobj()
						.selectMultipleValFromMultiSelect(driver,
								fc.utobj()
										.getElementByXpath(driver,
												".//tr/td[contains(text(),'" + divisionName
														+ "')]/following-sibling::td/div[@class='ms-parent']"),
								listInfo);

				fc.utobj().clickElement(driver, pobj.updateLeadOwnerBtn);

				boolean configredLeadOwner = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//tr/td[contains(text(),'" + divisionName
						+ "')]/following-sibling::td[contains(text(),'" + listInfo.get(0) + "')]");
				if (configredLeadOwner == false) {
					fc.utobj().throwsException("User is  not available in  Configured Lead Owners");
				}

			}

			// allroundrobin=
			// driver.findElements(By.xpath(".//*[@id='templateOrder']/option"));

			// Start here to check Lead

			if (leadSource2ID != null) {

				if ("SystemLead".equals(fromWhere)) {
					// First Lead
					fc.sales().sales_common().fsModule(driver);
					Sales fs = new Sales();
					fs.leadManagement(driver);
					FSLeadSummaryPageTest fsPage = new FSLeadSummaryPageTest();
					fsPage.clickAddLeadLink(driver);
					fc.utobj().printTestStep("Add a lead and Go to Primary Info");
					testCaseIdInternal = "TC_FC_QA_Sale_Lead_Primary_Owner_Assignment";
					dataSet = fc.utobj().readTestData("sales", testCaseIdInternal);
					String firstName = fc.utobj().generateTestData("AK");
					String lastName = fc.utobj().generateTestData("BK");
					dataSet.put("firstName", firstName);
					dataSet.put("lastName", lastName);
					dataSet.put("automatic", "As per Assignment Scheme");
					dataSet.put("zip", zipCode);
					dataSet.put("stateID", state);
					dataSet.put("leadSource2ID", "Internet");
					dataSet.put("leadSource3ID", "BISON");
					dataSet.put("brandMapping_0brandID", divisionName);

					printInfo = addLeadFromAllSourecesObj.fillFormDataWithDataset(driver, testCaseIdInternal, dataSet);

					if (printInfo != null && printInfo.size() > 0) {
						fc.utobj().printTestStep("Verify a lead and Go to get First Lead Owner");
						// .//tr/td[contains(text(),'Lead Owner
						// :')]/following-sibling::td[contains (text()
						// ,'"+userNameOwner+"')]
						WebElement ownerelement = driver.findElement(
								By.xpath(".//tr/td[contains(text(),'Lead Owner :')]/following-sibling::td"));
						firstOwner = ownerelement.getText();

					}

					// End First Lead
					// Enter Second Lead
					fc.sales().sales_common().fsModule(driver);
					fs = new Sales();
					fs.leadManagement(driver);
					fsPage.clickAddLeadLink(driver);
					fc.utobj().printTestStep("Add a lead and Go to Primary Info");
					testCaseIdInternal = "TC_FC_QA_Sale_Lead_Primary_Owner_Assignment";
					dataSet = fc.utobj().readTestData("sales", testCaseIdInternal);
					firstName = fc.utobj().generateTestData("AK");
					lastName = fc.utobj().generateTestData("BK");
					dataSet.put("firstName", firstName);
					dataSet.put("lastName", lastName);
					dataSet.put("automatic", "As per Assignment Scheme");
					dataSet.put("zip", zipCode);
					dataSet.put("stateID", state);
					dataSet.put("leadSource2ID", "Internet");
					dataSet.put("leadSource3ID", "BISON");
					dataSet.put("brandMapping_0brandID", divisionName);

					printInfo = addLeadFromAllSourecesObj.fillFormDataWithDataset(driver, testCaseIdInternal, dataSet);

					if (printInfo != null && printInfo.size() > 0) {
						fc.utobj().printTestStep("Verify a lead and Go to get Second Lead Owner");
						// .//tr/td[contains(text(),'Lead Owner
						// :')]/following-sibling::td[contains (text()
						// ,'"+userNameOwner+"')]
						WebElement ownerelement = driver.findElement(
								By.xpath(".//tr/td[contains(text(),'Lead Owner :')]/following-sibling::td"));
						secondOwner = ownerelement.getText();

					}
					// End Second Lead
					// Enter Third Lead
					fc.sales().sales_common().fsModule(driver);
					fs = new Sales();
					fs.leadManagement(driver);
					fsPage.clickAddLeadLink(driver);
					fc.utobj().printTestStep("Add a lead and Go to Primary Info");
					testCaseIdInternal = "TC_FC_QA_Sale_Lead_Primary_Owner_Assignment";
					dataSet = fc.utobj().readTestData("sales", testCaseIdInternal);
					firstName = fc.utobj().generateTestData("AK");
					lastName = fc.utobj().generateTestData("BK");
					dataSet.put("firstName", firstName);
					dataSet.put("lastName", lastName);
					dataSet.put("automatic", "As per Assignment Scheme");
					dataSet.put("zip", zipCode);
					dataSet.put("stateID", state);
					dataSet.put("leadSource2ID", "Internet");
					dataSet.put("leadSource3ID", "BISON");
					dataSet.put("brandMapping_0brandID", divisionName);

					printInfo = addLeadFromAllSourecesObj.fillFormDataWithDataset(driver, testCaseIdInternal, dataSet);

					if (printInfo != null && printInfo.size() > 0) {
						fc.utobj().printTestStep("Verify a lead and Go to get Third Lead Owner");
						// .//tr/td[contains(text(),'Lead Owner
						// :')]/following-sibling::td[contains (text()
						// ,'"+userNameOwner+"')]
						WebElement ownerelement = driver.findElement(
								By.xpath(".//tr/td[contains(text(),'Lead Owner :')]/following-sibling::td"));
						thirdOwner = ownerelement.getText();

					}
					// End Second Lead

					System.out.println(firstOwner + "====" + secondOwner + "====" + thirdOwner);

					if (!(firstOwner.equalsIgnoreCase(secondOwner)) && !(secondOwner.equalsIgnoreCase(thirdOwner))
							&& !(firstOwner.equalsIgnoreCase(thirdOwner))) {
						actualOner = firstOwner + " / " + secondOwner + " / " + thirdOwner;
						fc.utobj().printTestStep("Assign Lead Owner by Sales Territories is working as excpeted");
					} else {
						System.out.println("Fails");
						fc.utobj().throwsException(
								"Assign Lead Owner by Sales Territories  is  not working as expcected");
					}

				} // End system Lead method
				else if ("WebForm".equals(fromWhere)) {
					testCaseIdInternal = "TC_FC_QA_Sale_Lead_Primary_Owner_Assignment";
					dataSet = fc.utobj().readTestData("sales", testCaseIdInternal);
					FSSearchPageTest p3 = new FSSearchPageTest();
					fc.utobj().printTestStep("Adding lead from Web Form");
					Map<String, String> leadInfo = new HashMap<String, String>();
					String firstName = fc.utobj().generateTestData("Wk");
					String lastName = fc.utobj().generateTestData("Wk");
					String formName = fc.utobj().generateTestData("FORM");
					leadInfo.put("firstName", firstName);
					leadInfo.put("lastName", lastName);
					leadInfo.put("country", dataSet.get("country"));
					// leadInfo.put("state", dataSet.get("stateID"));
					leadInfo.put("email", dataSet.get("emailID"));
					// leadInfo.put("leadOwner", "FranConnect Administrator");
					leadInfo.put("leadSourceCategory", "Internet");
					leadInfo.put("leadSourceDetails", "BISON");
					leadInfo.put("zipCode", zipCode);
					leadInfo.put("state", state);
					leadInfo.put("divisionName", divisionName);

					addLeadFromAllSourecesObj.addLeadFromWebForm(driver, formName, leadInfo, "Confirmation Message");
					p3.searchByLeadName(driver, firstName, lastName);
					fc.utobj().clickElement(driver, driver
							.findElement(By.xpath(".//*[contains(text(),'" + firstName + " " + lastName + "')]")));

					fc.utobj().printTestStep("Verify a lead and Go to get Lead Owner");
					WebElement ownerelement = driver
							.findElement(By.xpath(".//tr/td[contains(text(),'Lead Owner :')]/following-sibling::td"));
					firstOwner = ownerelement.getText();

					testCaseIdInternal = "TC_FC_QA_Sale_Lead_Primary_Owner_Assignment";
					dataSet = fc.utobj().readTestData("sales", testCaseIdInternal);
					fc.utobj().printTestStep("Adding lead from Web Form");
					leadInfo = new HashMap<String, String>();
					firstName = fc.utobj().generateTestData("Wk");
					lastName = fc.utobj().generateTestData("Wk");
					formName = fc.utobj().generateTestData("FORM");
					leadInfo.put("firstName", firstName);
					leadInfo.put("lastName", lastName);
					leadInfo.put("country", dataSet.get("country"));
					// leadInfo.put("state", dataSet.get("stateID"));
					leadInfo.put("email", dataSet.get("emailID"));
					// leadInfo.put("leadOwner", "FranConnect Administrator");
					/*
					 * leadInfo.put("leadSourceCategory",
					 * dataSet.get("leadSource2ID"));
					 * leadInfo.put("leadSourceDetails",
					 * dataSet.get("leadSource3ID"));
					 */
					leadInfo.put("leadSourceCategory", "Internet");
					leadInfo.put("leadSourceDetails", "BISON");
					leadInfo.put("divisionName", divisionName);

					leadInfo.put("zipCode", zipCode);
					leadInfo.put("state", state);
					addLeadFromAllSourecesObj.addLeadFromWebForm(driver, formName, leadInfo, "Confirmation Message");
					p3.searchByLeadName(driver, firstName, lastName);
					fc.utobj().clickElement(driver, driver
							.findElement(By.xpath(".//*[contains(text(),'" + firstName + " " + lastName + "')]")));

					fc.utobj().printTestStep("Verify a lead and Go to get Lead Owner");
					ownerelement = driver
							.findElement(By.xpath(".//tr/td[contains(text(),'Lead Owner :')]/following-sibling::td"));
					secondOwner = ownerelement.getText();

					testCaseIdInternal = "TC_FC_QA_Sale_Lead_Primary_Owner_Assignment";
					dataSet = fc.utobj().readTestData("sales", testCaseIdInternal);
					fc.utobj().printTestStep("Adding lead from Web Form");
					leadInfo = new HashMap<String, String>();
					firstName = fc.utobj().generateTestData("Wk");
					lastName = fc.utobj().generateTestData("Wk");
					formName = fc.utobj().generateTestData("FORM");
					leadInfo.put("firstName", firstName);
					leadInfo.put("lastName", lastName);
					leadInfo.put("country", dataSet.get("country"));
					leadInfo.put("state", dataSet.get("stateID"));
					leadInfo.put("email", dataSet.get("emailID"));
					// leadInfo.put("leadOwner", "FranConnect Administrator");
					/*
					 * leadInfo.put("leadSourceCategory",
					 * dataSet.get("leadSource2ID"));
					 * leadInfo.put("leadSourceDetails",
					 * dataSet.get("leadSource3ID"));
					 */
					leadInfo.put("leadSourceCategory", "Internet");
					leadInfo.put("leadSourceDetails", "BISON");
					leadInfo.put("divisionName", divisionName);

					leadInfo.put("zipCode", zipCode);
					leadInfo.put("state", state);
					addLeadFromAllSourecesObj.addLeadFromWebForm(driver, formName, leadInfo, "Confirmation Message");
					p3.searchByLeadName(driver, firstName, lastName);
					fc.utobj().clickElement(driver, driver
							.findElement(By.xpath(".//*[contains(text(),'" + firstName + " " + lastName + "')]")));

					fc.utobj().printTestStep("Verify a lead and Go to get Lead Owner");
					ownerelement = driver
							.findElement(By.xpath(".//tr/td[contains(text(),'Lead Owner :')]/following-sibling::td"));
					thirdOwner = ownerelement.getText();

					System.out.println(firstOwner + "====" + secondOwner + "====" + thirdOwner);

					if (!(firstOwner.equalsIgnoreCase(secondOwner)) && !(secondOwner.equalsIgnoreCase(thirdOwner))
							&& !(firstOwner.equalsIgnoreCase(thirdOwner))) {
						actualOner = firstOwner + " / " + secondOwner + " / " + thirdOwner;
						fc.utobj().printTestStep("Assign Lead Owner by Sales Territories  is working as excpeted");
					} else {
						System.out.println("Fails");
						fc.utobj().throwsException(
								"Assign Lead Owner by Sales Territories  is  not working as expcected");
					}

				} else if ("Import".equals(fromWhere)) {
					testCaseIdInternal = "TC_FC_QA_Sale_Lead_Primary_Owner_Assignment";
					dataSet = fc.utobj().readTestData("sales", testCaseIdInternal);
					FSSearchPageTest p3 = new FSSearchPageTest();
					fc.utobj().printTestStep("Adding lead from Web Form");
					Map<String, String> leadInfo = new HashMap<String, String>();
					String firstName = fc.utobj().generateTestData("Wk");
					String lastName = fc.utobj().generateTestData("Wk");
					String formName = fc.utobj().generateTestData("FORM");
					leadInfo.put("firstName", firstName);
					leadInfo.put("lastName", lastName);
					leadInfo.put("country", dataSet.get("country"));
					// leadInfo.put("state", dataSet.get("stateID"));
					leadInfo.put("email", dataSet.get("emailID"));
					// leadInfo.put("leadOwner", "FranConnect Administrator");
					leadInfo.put("leadSourceCategory", "Internet");
					leadInfo.put("leadSourceDetails", "BISON");
					leadInfo.put("zipCode", zipCode);
					leadInfo.put("state", state);
					leadInfo.put("brand", divisionName);
					leadInfo.put("owner", "Based on Assignment Rules");
					String fileName = "OneLeadImportFile.csv";
					fc.utobj().printTestStep("Adding Lead from Import");
					fileName = fc.utobj().getFilePathFromTestData(fileName);
					try {
						addLeadFromAllSourecesObj.addLeadFromImport(driver, fileName, leadInfo);
					} catch (Exception e) {
						System.out.println("==Sales teriotries ==" + e.getMessage());
					}
					p3.searchByLeadName(driver, firstName, lastName);
					fc.utobj().clickElement(driver, driver
							.findElement(By.xpath(".//*[contains(text(),'" + firstName + " " + lastName + "')]")));

					fc.utobj().printTestStep("Verify a lead and Go to get Lead Owner");
					WebElement ownerelement = driver
							.findElement(By.xpath(".//tr/td[contains(text(),'Lead Owner :')]/following-sibling::td"));
					firstOwner = ownerelement.getText();

					testCaseIdInternal = "TC_FC_QA_Sale_Lead_Primary_Owner_Assignment";
					dataSet = fc.utobj().readTestData("sales", testCaseIdInternal);
					fc.utobj().printTestStep("Adding lead from Web Form");
					leadInfo = new HashMap<String, String>();
					firstName = fc.utobj().generateTestData("Wk");
					lastName = fc.utobj().generateTestData("Wk");
					formName = fc.utobj().generateTestData("FORM");
					leadInfo.put("firstName", firstName);
					leadInfo.put("lastName", lastName);
					leadInfo.put("country", dataSet.get("country"));
					// leadInfo.put("state", dataSet.get("stateID"));
					leadInfo.put("email", dataSet.get("emailID"));
					// leadInfo.put("leadOwner", "FranConnect Administrator");
					/*
					 * leadInfo.put("leadSourceCategory",
					 * dataSet.get("leadSource2ID"));
					 * leadInfo.put("leadSourceDetails",
					 * dataSet.get("leadSource3ID"));
					 */
					leadInfo.put("leadSourceCategory", "Internet");
					leadInfo.put("leadSourceDetails", "BISON");
					leadInfo.put("brand", divisionName);
					leadInfo.put("owner", "Based on Assignment Rules");
					leadInfo.put("zipCode", zipCode);
					leadInfo.put("state", state);
					// addLeadFromAllSourecesObj.addLeadFromWebForm(driver,
					// formName, leadInfo,"Confirmation Message");
					addLeadFromAllSourecesObj.addLeadFromImport(driver, fileName, leadInfo);
					p3.searchByLeadName(driver, firstName, lastName);
					fc.utobj().clickElement(driver, driver
							.findElement(By.xpath(".//*[contains(text(),'" + firstName + " " + lastName + "')]")));

					fc.utobj().printTestStep("Verify a lead and Go to get Lead Owner");
					ownerelement = driver
							.findElement(By.xpath(".//tr/td[contains(text(),'Lead Owner :')]/following-sibling::td"));
					secondOwner = ownerelement.getText();

					testCaseIdInternal = "TC_FC_QA_Sale_Lead_Primary_Owner_Assignment";
					dataSet = fc.utobj().readTestData("sales", testCaseIdInternal);
					fc.utobj().printTestStep("Adding lead from Web Form");
					leadInfo = new HashMap<String, String>();
					firstName = fc.utobj().generateTestData("Wk");
					lastName = fc.utobj().generateTestData("Wk");
					formName = fc.utobj().generateTestData("FORM");
					leadInfo.put("firstName", firstName);
					leadInfo.put("lastName", lastName);
					leadInfo.put("country", dataSet.get("country"));
					leadInfo.put("state", dataSet.get("stateID"));
					leadInfo.put("email", dataSet.get("emailID"));
					// leadInfo.put("leadOwner", "FranConnect Administrator");
					/*
					 * leadInfo.put("leadSourceCategory",
					 * dataSet.get("leadSource2ID"));
					 * leadInfo.put("leadSourceDetails",
					 * dataSet.get("leadSource3ID"));
					 */
					leadInfo.put("leadSourceCategory", "Internet");
					leadInfo.put("leadSourceDetails", "BISON");
					leadInfo.put("brand", divisionName);
					leadInfo.put("owner", "Based on Assignment Rules");
					leadInfo.put("zipCode", zipCode);
					leadInfo.put("state", state);
					addLeadFromAllSourecesObj.addLeadFromImport(driver, fileName, leadInfo);
					// addLeadFromAllSourecesObj.addLeadFromWebForm(driver,
					// formName, leadInfo,"Confirmation Message");
					p3.searchByLeadName(driver, firstName, lastName);
					fc.utobj().clickElement(driver, driver
							.findElement(By.xpath(".//*[contains(text(),'" + firstName + " " + lastName + "')]")));

					fc.utobj().printTestStep("Verify a lead and Go to get Lead Owner");
					ownerelement = driver
							.findElement(By.xpath(".//tr/td[contains(text(),'Lead Owner :')]/following-sibling::td"));
					thirdOwner = ownerelement.getText();

					System.out.println(firstOwner + "====" + secondOwner + "====" + thirdOwner);

					if (!(firstOwner.equalsIgnoreCase(secondOwner)) && !(secondOwner.equalsIgnoreCase(thirdOwner))
							&& !(firstOwner.equalsIgnoreCase(thirdOwner))) {
						actualOner = firstOwner + " / " + secondOwner + " / " + thirdOwner;
						fc.utobj().printTestStep("Assign Lead Owner by Sales Territories  is working as excpeted");
					} else {
						System.out.println("Fails");
						fc.utobj().throwsException(
								"Assign Lead Owner by Sales Territories  is  not working as expcected");
					}

				} else if ("RestAPI".equals(fromWhere)) {
					FSSearchPageTest p3 = new FSSearchPageTest();
					testCaseIdInternal = "TC_Sales_SmartGroup_002";
					dataSet = fc.utobj().readTestData("sales", testCaseIdInternal);
					String firstName = fc.utobj().generateTestData("Re");
					String lastName = fc.utobj().generateTestData("Re");
					country = "USA";
					// String state=dataSet.get("state");
					String leadOwner = "FranConnect Administrator";
					String leadSourceCategory = "Friends";
					String leadSourceDetails = "Friends";
					Map<String, String> UniqueKey_PrimaryInfo = new HashMap<String, String>();
					UniqueKey_PrimaryInfo.put("firstName", firstName);
					UniqueKey_PrimaryInfo.put("lastName", lastName);
					/*
					 * UniqueKey_PrimaryInfo.put("leadSource2ID",
					 * leadSourceCategory);
					 * UniqueKey_PrimaryInfo.put("leadSource3ID",
					 * leadSourceDetails);
					 */
					UniqueKey_PrimaryInfo.put("leadSource2ID", "Internet");
					UniqueKey_PrimaryInfo.put("leadSource3ID", "BISON");
					UniqueKey_PrimaryInfo.put("emailID", "harish@test.net");
					UniqueKey_PrimaryInfo.put("country", country);
					UniqueKey_PrimaryInfo.put("stateID", state);
					UniqueKey_PrimaryInfo.put("zip", zipCode);
					UniqueKey_PrimaryInfo.put("division", divisionName);

					fc.utobj().printTestStep("Adding lead from Rest API");
					addLeadFromAllSourecesObj.addleadFromRestApi(driver, dataSet, testCaseId, UniqueKey_PrimaryInfo);

					driver.navigate().to(FranconnectUtil.config.get("buildUrl"));
					fc.loginpage().login(driver);
					fc.sales().sales_common().fsModule(driver);
					p3.searchByLeadName(driver, firstName, lastName);

					fc.utobj().clickElement(driver, driver
							.findElement(By.xpath(".//*[contains(text(),'" + firstName + " " + lastName + "')]")));

					fc.utobj().printTestStep("Verify a lead and Go to get Lead Owner");
					WebElement ownerelement = driver
							.findElement(By.xpath(".//tr/td[contains(text(),'Lead Owner :')]/following-sibling::td"));
					firstOwner = ownerelement.getText();

					testCaseIdInternal = "TC_Sales_SmartGroup_002";
					dataSet = fc.utobj().readTestData("sales", testCaseIdInternal);
					firstName = fc.utobj().generateTestData("Re");
					lastName = fc.utobj().generateTestData("Re");
					country = "USA";
					// String state=dataSet.get("state");
					leadOwner = "FranConnect Administrator";
					UniqueKey_PrimaryInfo = new HashMap<String, String>();
					UniqueKey_PrimaryInfo.put("firstName", firstName);
					UniqueKey_PrimaryInfo.put("lastName", lastName);
					/*
					 * UniqueKey_PrimaryInfo.put("leadSource2ID",
					 * leadSourceCategory);
					 * UniqueKey_PrimaryInfo.put("leadSource3ID",
					 * leadSourceDetails);
					 */
					UniqueKey_PrimaryInfo.put("leadSource2ID", "Internet");
					UniqueKey_PrimaryInfo.put("leadSource3ID", "BISON");
					UniqueKey_PrimaryInfo.put("emailID", "harish@test.net");
					UniqueKey_PrimaryInfo.put("country", country);
					UniqueKey_PrimaryInfo.put("stateID", state);
					UniqueKey_PrimaryInfo.put("zip", zipCode);
					UniqueKey_PrimaryInfo.put("division", divisionName);

					fc.utobj().printTestStep("Adding lead from Rest API");
					addLeadFromAllSourecesObj.addleadFromRestApi(driver, dataSet, testCaseId, UniqueKey_PrimaryInfo);

					driver.navigate().to(FranconnectUtil.config.get("buildUrl"));
					fc.loginpage().login(driver);
					fc.sales().sales_common().fsModule(driver);
					p3.searchByLeadName(driver, firstName, lastName);

					fc.utobj().clickElement(driver, driver
							.findElement(By.xpath(".//*[contains(text(),'" + firstName + " " + lastName + "')]")));
					fc.utobj().printTestStep("Verify a lead and Go to get Lead Owner");
					ownerelement = driver
							.findElement(By.xpath(".//tr/td[contains(text(),'Lead Owner :')]/following-sibling::td"));
					secondOwner = ownerelement.getText();

					testCaseIdInternal = "TC_Sales_SmartGroup_002";
					dataSet = fc.utobj().readTestData("sales", testCaseIdInternal);
					firstName = fc.utobj().generateTestData("Re");
					lastName = fc.utobj().generateTestData("Re");
					country = "USA";
					// String state=dataSet.get("state");
					leadOwner = "FranConnect Administrator";
					UniqueKey_PrimaryInfo = new HashMap<String, String>();
					UniqueKey_PrimaryInfo.put("firstName", firstName);
					UniqueKey_PrimaryInfo.put("lastName", lastName);
					/*
					 * UniqueKey_PrimaryInfo.put("leadSource2ID",
					 * leadSourceCategory);
					 * UniqueKey_PrimaryInfo.put("leadSource3ID",
					 * leadSourceDetails);
					 */
					UniqueKey_PrimaryInfo.put("leadSource2ID", "Internet");
					UniqueKey_PrimaryInfo.put("leadSource3ID", "BISON");

					UniqueKey_PrimaryInfo.put("emailID", "harish@test.net");
					UniqueKey_PrimaryInfo.put("country", country);
					UniqueKey_PrimaryInfo.put("stateID", state);
					UniqueKey_PrimaryInfo.put("zip", zipCode);
					UniqueKey_PrimaryInfo.put("division", divisionName);

					fc.utobj().printTestStep("Adding lead from Rest API");
					addLeadFromAllSourecesObj.addleadFromRestApi(driver, dataSet, testCaseId, UniqueKey_PrimaryInfo);

					driver.navigate().to(FranconnectUtil.config.get("buildUrl"));
					fc.loginpage().login(driver);
					fc.sales().sales_common().fsModule(driver);
					p3.searchByLeadName(driver, firstName, lastName);

					fc.utobj().clickElement(driver, driver
							.findElement(By.xpath(".//*[contains(text(),'" + firstName + " " + lastName + "')]")));

					fc.utobj().printTestStep("Verify a lead and Go to get Lead Owner");
					ownerelement = driver
							.findElement(By.xpath(".//tr/td[contains(text(),'Lead Owner :')]/following-sibling::td"));
					thirdOwner = ownerelement.getText();

					System.out.println(firstOwner + "====" + secondOwner + "====" + thirdOwner);

					if (!(firstOwner.equalsIgnoreCase(secondOwner)) && !(secondOwner.equalsIgnoreCase(thirdOwner))
							&& !(firstOwner.equalsIgnoreCase(thirdOwner))) {
						actualOner = firstOwner + " / " + secondOwner + " / " + thirdOwner;
						fc.utobj().printTestStep("Assign Lead Owner by Sales Territories  is working as excpeted");
					} else {
						System.out.println("Fails");
						fc.utobj().throwsException(
								"Assign Lead Owner by Sales Territories  is  not working as expcected");
					}
				}
			}

		} else {
			state = "Florida";
			// In case when round robin is set but no user set for owner should
			// goes to default owner
			fc.adminpage().adminPage(driver);
			// adsales.assignLeadOwners(driver);

			String userNameOwner = "FranConnect Administrator";
			fc.utobj().printTestStep("Create user to Assign Owner");
			String userNameRC = fc.utobj().generateTestData("IZ"); // "AssignOwner";
			userNameOwner = userNameRC + " " + userNameRC;
			String emailId = "salesautomation@staffex.com";
			if ("CorporateUser".equals(UserType)) {
				CorporateUser corpUser = new CorporateUser();
				corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
				corpUser.setEmail(emailId);
				corpUser = corpTest.createDefaultUser(driver, corpUser);
			} else if ("RegionalUser".equals(UserType)) {
				String regionName = fc.utobj().generateTestData("RG");
				regUser.addRegionalUser(driver, userNameRC, regionName, emailId);
				userNameOwner = userNameRC + " " + userNameRC + " (RU)";
			}
			// userNameOwner = userNameRC+" "+userNameRC;
			fc.utobj().printTestStep("Verify the Create user");
			fc.adminpage().adminPage(driver);
			adsales.assignLeadOwners(driver);

			fc.utobj().printTestStep("Verify the user is available in Default Owner drop down");

			boolean isLeadOwnerPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//select[@name='area1']/option[contains(text(),'" + userNameOwner + "')]");
			if (isLeadOwnerPresent == false) {
				fc.utobj().throwsException("User not present in Default Owner dropdown");
			}

			if (noownerset) {
				fc.utobj().selectDropDown(driver, pobj.defaultOwnerDrpDown, "Select");
				userNameOwner = "FranConnect Administrator";
			} else {
				fc.utobj().selectDropDown(driver, pobj.defaultOwnerDrpDown, userNameOwner);
			}

			fc.utobj().clickElement(driver, pobj.updateLeadOwnerBtn);

			if ("SystemLead".equals(fromWhere)) {
				fc.sales().sales_common().fsModule(driver);
				Sales fs = new Sales();
				fs.leadManagement(driver);
				FSLeadSummaryPageTest fsPage = new FSLeadSummaryPageTest();
				fsPage.clickAddLeadLink(driver);
				fc.utobj().printTestStep("Add a lead and Go to Primary Info");
				testCaseIdInternal = "TC_FC_QA_Sale_Lead_Primary_Owner_Assignment";
				dataSet = fc.utobj().readTestData("sales", testCaseIdInternal);
				String firstName = fc.utobj().generateTestData("AK");
				String lastName = fc.utobj().generateTestData("BK");
				dataSet.put("firstName", firstName);
				dataSet.put("lastName", lastName);
				dataSet.put("automatic", "As per Assignment Scheme");
				dataSet.put("zip", zipCode);
				dataSet.put("stateID", state);
				dataSet.put("leadSource2ID", "Internet");
				dataSet.put("leadSource3ID", "BISON");
				dataSet.put("brandMapping_0brandID", divisionName);

				printInfo = addLeadFromAllSourecesObj.fillFormDataWithDataset(driver, testCaseIdInternal, dataSet);
				fc.utobj().printTestStep("Verify a lead and Go to get Lead Owner");
				if (printInfo != null && printInfo.size() > 0) {
					isLeadOwnerPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
							".//tr/td[contains(text(),'Lead Owner :')]/following-sibling::td[contains (text() ,'"
									+ userNameOwner + "')]");
					if (isLeadOwnerPresent == false) {
						fc.utobj().throwsException("As per Assignment Scheme owner is not assigned");
					}

				}
			} else if ("WebForm".equals(fromWhere)) {
				testCaseIdInternal = "TC_FC_QA_Sale_Lead_Primary_Owner_Assignment";
				dataSet = fc.utobj().readTestData("sales", testCaseIdInternal);
				FSSearchPageTest p3 = new FSSearchPageTest();
				fc.utobj().printTestStep("Adding lead from Web Form");
				Map<String, String> leadInfo = new HashMap<String, String>();
				String firstName = fc.utobj().generateTestData("Wk");
				String lastName = fc.utobj().generateTestData("Wk");
				String formName = fc.utobj().generateTestData("FORM");
				leadInfo.put("firstName", firstName);
				leadInfo.put("lastName", lastName);
				leadInfo.put("country", dataSet.get("country"));
				// leadInfo.put("state", dataSet.get("stateID"));
				leadInfo.put("state", state);

				leadInfo.put("email", dataSet.get("emailID"));
				leadInfo.put("leadOwner", userNameOwner);
				/*
				 * leadInfo.put("leadSourceCategory",
				 * dataSet.get("leadSource2ID"));
				 * leadInfo.put("leadSourceDetails",
				 * dataSet.get("leadSource3ID"));
				 */
				leadInfo.put("leadSourceCategory", "Internet");
				leadInfo.put("leadSourceDetails", "BISON");
				leadInfo.put("divisionName", divisionName);
				leadInfo.put("zipCode", zipCode);
				addLeadFromAllSourecesObj.addLeadFromWebForm(driver, formName, leadInfo, "Confirmation Message");
				p3.searchByLeadName(driver, firstName, lastName);
				fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
						".//*[contains(text(),'" + firstName + " " + lastName + "')]"));

				fc.utobj().printTestStep("Verify a lead and Go to get Lead Owner");
				isLeadOwnerPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
						".//tr/td[contains(text(),'Lead Owner :')]/following-sibling::td[contains (text() ,'"
								+ userNameOwner + "')]");
				if (isLeadOwnerPresent == false) {
					fc.utobj().throwsException("As per Assignment Scheme owner is not assigned");
				}

			} else if ("Import".equals(fromWhere)) {
				testCaseIdInternal = "TC_FC_QA_Sale_Lead_Primary_Owner_Assignment";
				dataSet = fc.utobj().readTestData("sales", testCaseIdInternal);
				FSSearchPageTest p3 = new FSSearchPageTest();
				fc.utobj().printTestStep("Adding lead from Web Form");
				Map<String, String> leadInfo = new HashMap<String, String>();
				String firstName = fc.utobj().generateTestData("Wk");
				String lastName = fc.utobj().generateTestData("Wk");
				String formName = fc.utobj().generateTestData("FORM");
				leadInfo.put("firstName", firstName);
				leadInfo.put("lastName", lastName);
				leadInfo.put("country", dataSet.get("country"));
				// leadInfo.put("state", dataSet.get("stateID"));
				leadInfo.put("state", state);

				leadInfo.put("email", dataSet.get("emailID"));
				leadInfo.put("leadOwner", userNameOwner);
				/*
				 * leadInfo.put("leadSourceCategory",
				 * dataSet.get("leadSource2ID"));
				 * leadInfo.put("leadSourceDetails",
				 * dataSet.get("leadSource3ID"));
				 */
				leadInfo.put("leadSourceCategory", "Internet");
				leadInfo.put("leadSourceDetails", "BISON");
				leadInfo.put("brand", divisionName);
				leadInfo.put("zipCode", zipCode);
				leadInfo.put("owner", "Based on Assignment Rules");
				String fileName = "OneLeadImportFile.csv";
				fc.utobj().printTestStep("Adding Lead from Import");
				fileName = fc.utobj().getFilePathFromTestData(fileName);

				try {
					addLeadFromAllSourecesObj.addLeadFromImport(driver, fileName, leadInfo);
				} catch (Exception e) {

				}
				// addLeadFromAllSourecesObj.addLeadFromWebForm(driver,
				// formName, leadInfo,"Confirmation Message");
				p3.searchByLeadName(driver, firstName, lastName);
				fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
						".//*[contains(text(),'" + firstName + " " + lastName + "')]"));

				fc.utobj().printTestStep("Verify a lead and Go to get Lead Owner");
				isLeadOwnerPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
						".//tr/td[contains(text(),'Lead Owner :')]/following-sibling::td[contains (text() ,'"
								+ userNameOwner + "')]");
				if (isLeadOwnerPresent == false) {
					fc.utobj().throwsException("As per Assignment Scheme owner is not assigned");
				}

			} else if ("RestAPI".equals(fromWhere)) {
				FSSearchPageTest p3 = new FSSearchPageTest();
				testCaseIdInternal = "TC_Sales_SmartGroup_002";
				dataSet = fc.utobj().readTestData("sales", testCaseIdInternal);
				String firstName = fc.utobj().generateTestData("Re");
				String lastName = fc.utobj().generateTestData("Re");
				country = "USA";
				// String state=dataSet.get("state");
				String leadOwner = "FranConnect Administrator";
				String leadSourceCategory = "Friends";
				String leadSourceDetails = "Friends";
				Map<String, String> UniqueKey_PrimaryInfo = new HashMap<String, String>();
				UniqueKey_PrimaryInfo.put("firstName", firstName);
				UniqueKey_PrimaryInfo.put("lastName", lastName);
				/*
				 * UniqueKey_PrimaryInfo.put("leadSource2ID",
				 * leadSourceCategory);
				 * UniqueKey_PrimaryInfo.put("leadSource3ID",
				 * leadSourceDetails);
				 */
				UniqueKey_PrimaryInfo.put("leadSource2ID", "Internet");
				UniqueKey_PrimaryInfo.put("leadSource3ID", "BISON");
				UniqueKey_PrimaryInfo.put("emailID", emailId);
				UniqueKey_PrimaryInfo.put("country", country);
				UniqueKey_PrimaryInfo.put("stateID", state);
				UniqueKey_PrimaryInfo.put("zip", zipCode);
				UniqueKey_PrimaryInfo.put("division", divisionName);

				fc.utobj().printTestStep("Adding lead from Rest API");
				addLeadFromAllSourecesObj.addleadFromRestApi(driver, dataSet, testCaseId, UniqueKey_PrimaryInfo);

				driver.navigate().to(FranconnectUtil.config.get("buildUrl"));
				fc.loginpage().login(driver);
				fc.sales().sales_common().fsModule(driver);
				p3.searchByLeadName(driver, firstName, lastName);

				fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
						".//*[contains(text(),'" + firstName + " " + lastName + "')]"));

				fc.utobj().printTestStep("Verify a lead and Go to get Lead Owner");
				isLeadOwnerPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
						".//tr/td[contains(text(),'Lead Owner :')]/following-sibling::td[contains (text() ,'"
								+ userNameOwner + "')]");
				if (isLeadOwnerPresent == false) {
					fc.utobj().throwsException("As per Assignment Scheme owner is not assigned");
				}
			}
		}
		// End Here
		return actualOner;
	}

	public String getOwnerLeadBySource(WebDriver driver, String testCaseId, String UserType, String fromWhere,
			boolean addDataforroundrobin, boolean noownerset) throws Exception {
		String actualOner = "";
		String testCaseIdInternal = "" + testCaseId;
		Map<String, String> dataSet = fc.utobj().readTestData("sales", testCaseId);
		Map<String, String> printInfo = null;
		AddLeadFromAllSources addLeadFromAllSourecesObj = new AddLeadFromAllSources();
		LeadSummaryUI pobj = new LeadSummaryUI(driver);
		AdminFranchiseSalesManageSalesTerritoryCreateSalesTerritoryPageTest p1 = new AdminFranchiseSalesManageSalesTerritoryCreateSalesTerritoryPageTest();
		AdminUsersManageCorporateUsersAddCorporateUserPageTest corpTest = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
		AdminUsersManageRegionalUsersAddRegionalUserPageTest regUser = new AdminUsersManageRegionalUsersAddRegionalUserPageTest();
		String firstOwner = "";
		String secondOwner = "";
		String thirdOwner = "";
		List<String> listInfo = new ArrayList<String>();
		// Sales Territories

		String leadSource2ID = "Internet";// fc.utobj().generateTestData("ST");
		String zipCode = fc.utobj().generateRandomNumber();
		String country = "USA";
		String state = "Alaska";

		// fc.adminpage().adminPage( driver);

		AdminSales adsales = new AdminSales();
		adsales.assignLeadOwners(driver);
		List<WebElement> oRadioButton = driver.findElements(By.name("radiobutton"));
		boolean bValue = false;
		bValue = oRadioButton.get(2).isSelected();
		if (oRadioButton.get(2).isSelected()) {

		} else {
			oRadioButton.get(2).click();
		}

		fc.utobj().printTestStep("Click Continue");
		fc.utobj().clickElement(driver, pobj.continueButton);

		System.out.println("leadSource2ID=====" + leadSource2ID);

		try {
			boolean isSalesTeritoryalready = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//td[contains(text(),'" + leadSource2ID
					+ "')]/following-sibling::td[contains(text(),'Source Owner')]");
			if (isSalesTeritoryalready == false) {
				leadSource2ID = "Internet";
				// fc.utobj().throwsException("Sales states teritory not got
				// added");
			} else {
				fc.utobj().printTestStep("Delete Lead Source Name");
				fc.utobj().actionImgOption(driver, leadSource2ID, "Delete");
				fc.utobj().acceptAlertBox(driver);
				leadSource2ID = "Internet";

			}
		} catch (Exception e) {

		}
		// End Lead Source

		if (addDataforroundrobin) {

			if ((leadSource2ID != null)) {
				for (int i = 0; i < 3; i++) {
					fc.adminpage().adminPage(driver);
					// adsales.assignLeadOwners(driver);

					String userNameOwner = "FranConnect Administrator";
					fc.utobj().printTestStep("Verify the Create user");
					String userNameRC = fc.utobj().generateTestData("IZ"); // "AssignOwner";
					userNameOwner = userNameRC + " " + userNameRC;
					String emailId = "salesautomation@staffex.com";
					if ("CorporateUser".equals(UserType)) {
						CorporateUser corpUser = new CorporateUser();
						corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
						corpUser.setEmail(emailId);
						corpUser = corpTest.createDefaultUser(driver, corpUser);
					} else if ("RegionalUser".equals(UserType)) {
						String regionName = fc.utobj().generateTestData("RG");
						regUser.addRegionalUser(driver, userNameRC, regionName, emailId);
						// userNameOwner = userNameRC+" "+userNameRC+" (RU)";
					}
					userNameOwner = userNameRC + " " + userNameRC;
					listInfo.add(userNameOwner);
					fc.utobj().printTestStep("Verify the Create user");
					// fc.adminpage().adminPage( driver);
					adsales.assignLeadOwners(driver);

					fc.utobj().printTestStep("Verify the user is available in Default Owner drop down");

					boolean isLeadOwnerPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
							".//select[@name='area1']/option[contains(text(),'" + userNameOwner + "')]");
					if (isLeadOwnerPresent == false) {
						fc.utobj().throwsException("User not present in Default Owner dropdown");
					}

					oRadioButton = driver.findElements(By.name("radiobutton"));
					bValue = false;
					bValue = oRadioButton.get(2).isSelected();
					if (oRadioButton.get(2).isSelected()) {

					} else {
						oRadioButton.get(2).click();
					}

					fc.utobj().printTestStep("Click Continue");
					fc.utobj().clickElement(driver, pobj.continueButton);

					boolean availableLeadOwner = fc.utobj().verifyElementOnVisible_ByXpath(driver,
							".//tr/td[contains(text(),'" + leadSource2ID
									+ "')]/following-sibling::td/select/option[contains(text(),'" + userNameOwner
									+ "')]");
					if (availableLeadOwner == false) {
						fc.utobj().throwsException("User is  not available in Sales Terriotries Lead Owners drop-down");
					}

				}

				fc.utobj().printTestStep("fill the owners with respect to sale terriortires drop down");
				// fc.adminpage().adminPage( driver);
				adsales.assignLeadOwners(driver);

				oRadioButton = driver.findElements(By.name("radiobutton"));
				bValue = false;
				bValue = oRadioButton.get(2).isSelected();
				if (oRadioButton.get(2).isSelected()) {

				} else {
					oRadioButton.get(2).click();
				}

				fc.utobj().printTestStep("Click Continue");
				fc.utobj().clickElement(driver, pobj.continueButton);

				fc.utobj()
						.selectMultipleValFromMultiSelect(driver,
								fc.utobj()
										.getElementByXpath(driver,
												".//tr/td[contains(text(),'" + leadSource2ID
														+ "')]/following-sibling::td/div[@class='ms-parent']"),
								listInfo);

				fc.utobj().clickElement(driver, pobj.updateLeadOwnerBtn);

				boolean configredLeadOwner = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//tr/td[contains(text(),'" + leadSource2ID
						+ "')]/following-sibling::td[contains(text(),'" + listInfo.get(0) + "')]");
				if (configredLeadOwner == false) {
					fc.utobj().throwsException("User is  not available in  Configured Lead Owners");
				}

			}

			// allroundrobin=
			// driver.findElements(By.xpath(".//*[@id='templateOrder']/option"));

			// Start here to check Lead

			if (leadSource2ID != null) {

				if ("SystemLead".equals(fromWhere)) {
					// First Lead
					fc.sales().sales_common().fsModule(driver);
					Sales fs = new Sales();
					fs.leadManagement(driver);
					FSLeadSummaryPageTest fsPage = new FSLeadSummaryPageTest();
					fsPage.clickAddLeadLink(driver);
					fc.utobj().printTestStep("Add a lead and Go to Primary Info");
					testCaseIdInternal = "TC_FC_QA_Sale_Lead_Primary_Owner_Assignment";
					dataSet = fc.utobj().readTestData("sales", testCaseIdInternal);
					String firstName = fc.utobj().generateTestData("AK");
					String lastName = fc.utobj().generateTestData("BK");
					dataSet.put("firstName", firstName);
					dataSet.put("lastName", lastName);
					dataSet.put("automatic", "As per Assignment Scheme");
					dataSet.put("zip", zipCode);
					dataSet.put("stateID", state);
					dataSet.put("leadSource2ID", "Internet");
					dataSet.put("leadSource3ID", "BISON");

					printInfo = addLeadFromAllSourecesObj.fillFormDataWithDataset(driver, testCaseIdInternal, dataSet);

					if (printInfo != null && printInfo.size() > 0) {
						fc.utobj().printTestStep("Verify a lead and Go to get First Lead Owner");
						// .//tr/td[contains(text(),'Lead Owner
						// :')]/following-sibling::td[contains (text()
						// ,'"+userNameOwner+"')]
						WebElement ownerelement = driver.findElement(
								By.xpath(".//tr/td[contains(text(),'Lead Owner :')]/following-sibling::td"));
						firstOwner = ownerelement.getText();

					}

					// End First Lead
					// Enter Second Lead
					fc.sales().sales_common().fsModule(driver);
					fs = new Sales();
					fs.leadManagement(driver);
					fsPage.clickAddLeadLink(driver);
					fc.utobj().printTestStep("Add a lead and Go to Primary Info");
					testCaseIdInternal = "TC_FC_QA_Sale_Lead_Primary_Owner_Assignment";
					dataSet = fc.utobj().readTestData("sales", testCaseIdInternal);
					firstName = fc.utobj().generateTestData("AK");
					lastName = fc.utobj().generateTestData("BK");
					dataSet.put("firstName", firstName);
					dataSet.put("lastName", lastName);
					dataSet.put("automatic", "As per Assignment Scheme");
					dataSet.put("zip", zipCode);
					dataSet.put("stateID", state);
					dataSet.put("leadSource2ID", "Internet");
					dataSet.put("leadSource3ID", "BISON");
					printInfo = addLeadFromAllSourecesObj.fillFormDataWithDataset(driver, testCaseIdInternal, dataSet);

					if (printInfo != null && printInfo.size() > 0) {
						fc.utobj().printTestStep("Verify a lead and Go to get Second Lead Owner");
						// .//tr/td[contains(text(),'Lead Owner
						// :')]/following-sibling::td[contains (text()
						// ,'"+userNameOwner+"')]
						WebElement ownerelement = driver.findElement(
								By.xpath(".//tr/td[contains(text(),'Lead Owner :')]/following-sibling::td"));
						secondOwner = ownerelement.getText();

					}
					// End Second Lead
					// Enter Third Lead
					fc.sales().sales_common().fsModule(driver);
					fs = new Sales();
					fs.leadManagement(driver);
					fsPage.clickAddLeadLink(driver);
					fc.utobj().printTestStep("Add a lead and Go to Primary Info");
					testCaseIdInternal = "TC_FC_QA_Sale_Lead_Primary_Owner_Assignment";
					dataSet = fc.utobj().readTestData("sales", testCaseIdInternal);
					firstName = fc.utobj().generateTestData("AK");
					lastName = fc.utobj().generateTestData("BK");
					dataSet.put("firstName", firstName);
					dataSet.put("lastName", lastName);
					dataSet.put("automatic", "As per Assignment Scheme");
					dataSet.put("zip", zipCode);
					dataSet.put("stateID", state);
					dataSet.put("leadSource2ID", "Internet");
					dataSet.put("leadSource3ID", "BISON");
					printInfo = addLeadFromAllSourecesObj.fillFormDataWithDataset(driver, testCaseIdInternal, dataSet);

					if (printInfo != null && printInfo.size() > 0) {
						fc.utobj().printTestStep("Verify a lead and Go to get Third Lead Owner");
						// .//tr/td[contains(text(),'Lead Owner
						// :')]/following-sibling::td[contains (text()
						// ,'"+userNameOwner+"')]
						WebElement ownerelement = driver.findElement(
								By.xpath(".//tr/td[contains(text(),'Lead Owner :')]/following-sibling::td"));
						thirdOwner = ownerelement.getText();

					}
					// End Second Lead

					System.out.println(firstOwner + "====" + secondOwner + "====" + thirdOwner);

					if (!(firstOwner.equalsIgnoreCase(secondOwner)) && !(secondOwner.equalsIgnoreCase(thirdOwner))
							&& !(firstOwner.equalsIgnoreCase(thirdOwner))) {
						actualOner = firstOwner + " / " + secondOwner + " / " + thirdOwner;
						fc.utobj().printTestStep("Assign Lead Owner by Sales Territories is working as excpeted");
					} else {
						System.out.println("Fails");
						fc.utobj().throwsException(
								"Assign Lead Owner by Sales Territories  is  not working as expcected");
					}

				} // End system Lead method
				else if ("WebForm".equals(fromWhere)) {
					testCaseIdInternal = "TC_FC_QA_Sale_Lead_Primary_Owner_Assignment";
					dataSet = fc.utobj().readTestData("sales", testCaseIdInternal);
					FSSearchPageTest p3 = new FSSearchPageTest();
					fc.utobj().printTestStep("Adding lead from Web Form");
					Map<String, String> leadInfo = new HashMap<String, String>();
					String firstName = fc.utobj().generateTestData("Wk");
					String lastName = fc.utobj().generateTestData("Wk");
					String formName = fc.utobj().generateTestData("FORM");
					leadInfo.put("firstName", firstName);
					leadInfo.put("lastName", lastName);
					leadInfo.put("country", dataSet.get("country"));
					// leadInfo.put("state", dataSet.get("stateID"));
					leadInfo.put("email", dataSet.get("emailID"));
					// leadInfo.put("leadOwner", "FranConnect Administrator");
					leadInfo.put("leadSourceCategory", "Internet");
					leadInfo.put("leadSourceDetails", "BISON");
					leadInfo.put("zipCode", zipCode);
					leadInfo.put("state", state);
					addLeadFromAllSourecesObj.addLeadFromWebForm(driver, formName, leadInfo, "Confirmation Message");
					p3.searchByLeadName(driver, firstName, lastName);
					fc.utobj().clickElement(driver, driver
							.findElement(By.xpath(".//*[contains(text(),'" + firstName + " " + lastName + "')]")));

					fc.utobj().printTestStep("Verify a lead and Go to get Lead Owner");
					WebElement ownerelement = driver
							.findElement(By.xpath(".//tr/td[contains(text(),'Lead Owner :')]/following-sibling::td"));
					firstOwner = ownerelement.getText();

					testCaseIdInternal = "TC_FC_QA_Sale_Lead_Primary_Owner_Assignment";
					dataSet = fc.utobj().readTestData("sales", testCaseIdInternal);
					fc.utobj().printTestStep("Adding lead from Web Form");
					leadInfo = new HashMap<String, String>();
					firstName = fc.utobj().generateTestData("Wk");
					lastName = fc.utobj().generateTestData("Wk");
					formName = fc.utobj().generateTestData("FORM");
					leadInfo.put("firstName", firstName);
					leadInfo.put("lastName", lastName);
					leadInfo.put("country", dataSet.get("country"));
					// leadInfo.put("state", dataSet.get("stateID"));
					leadInfo.put("email", dataSet.get("emailID"));
					// leadInfo.put("leadOwner", "FranConnect Administrator");
					/*
					 * leadInfo.put("leadSourceCategory",
					 * dataSet.get("leadSource2ID"));
					 * leadInfo.put("leadSourceDetails",
					 * dataSet.get("leadSource3ID"));
					 */
					leadInfo.put("leadSourceCategory", "Internet");
					leadInfo.put("leadSourceDetails", "BISON");

					leadInfo.put("zipCode", zipCode);
					leadInfo.put("state", state);
					addLeadFromAllSourecesObj.addLeadFromWebForm(driver, formName, leadInfo, "Confirmation Message");
					p3.searchByLeadName(driver, firstName, lastName);
					fc.utobj().clickElement(driver, driver
							.findElement(By.xpath(".//*[contains(text(),'" + firstName + " " + lastName + "')]")));

					fc.utobj().printTestStep("Verify a lead and Go to get Lead Owner");
					ownerelement = driver
							.findElement(By.xpath(".//tr/td[contains(text(),'Lead Owner :')]/following-sibling::td"));
					secondOwner = ownerelement.getText();

					testCaseIdInternal = "TC_FC_QA_Sale_Lead_Primary_Owner_Assignment";
					dataSet = fc.utobj().readTestData("sales", testCaseIdInternal);
					fc.utobj().printTestStep("Adding lead from Web Form");
					leadInfo = new HashMap<String, String>();
					firstName = fc.utobj().generateTestData("Wk");
					lastName = fc.utobj().generateTestData("Wk");
					formName = fc.utobj().generateTestData("FORM");
					leadInfo.put("firstName", firstName);
					leadInfo.put("lastName", lastName);
					leadInfo.put("country", dataSet.get("country"));
					leadInfo.put("state", dataSet.get("stateID"));
					leadInfo.put("email", dataSet.get("emailID"));
					// leadInfo.put("leadOwner", "FranConnect Administrator");
					/*
					 * leadInfo.put("leadSourceCategory",
					 * dataSet.get("leadSource2ID"));
					 * leadInfo.put("leadSourceDetails",
					 * dataSet.get("leadSource3ID"));
					 */
					leadInfo.put("leadSourceCategory", "Internet");
					leadInfo.put("leadSourceDetails", "BISON");

					leadInfo.put("zipCode", zipCode);
					leadInfo.put("state", state);
					addLeadFromAllSourecesObj.addLeadFromWebForm(driver, formName, leadInfo, "Confirmation Message");
					p3.searchByLeadName(driver, firstName, lastName);
					fc.utobj().clickElement(driver, driver
							.findElement(By.xpath(".//*[contains(text(),'" + firstName + " " + lastName + "')]")));

					fc.utobj().printTestStep("Verify a lead and Go to get Lead Owner");
					ownerelement = driver
							.findElement(By.xpath(".//tr/td[contains(text(),'Lead Owner :')]/following-sibling::td"));
					thirdOwner = ownerelement.getText();

					System.out.println(firstOwner + "====" + secondOwner + "====" + thirdOwner);

					if (!(firstOwner.equalsIgnoreCase(secondOwner)) && !(secondOwner.equalsIgnoreCase(thirdOwner))
							&& !(firstOwner.equalsIgnoreCase(thirdOwner))) {
						actualOner = firstOwner + " / " + secondOwner + " / " + thirdOwner;
						fc.utobj().printTestStep("Assign Lead Owner by Sales Territories  is working as excpeted");
					} else {
						System.out.println("Fails");
						fc.utobj().throwsException(
								"Assign Lead Owner by Sales Territories  is  not working as expcected");
					}

				} else if ("Import".equals(fromWhere)) {
					testCaseIdInternal = "TC_FC_QA_Sale_Lead_Primary_Owner_Assignment";
					dataSet = fc.utobj().readTestData("sales", testCaseIdInternal);
					FSSearchPageTest p3 = new FSSearchPageTest();
					fc.utobj().printTestStep("Adding lead from Web Form");
					Map<String, String> leadInfo = new HashMap<String, String>();
					String firstName = fc.utobj().generateTestData("Wk");
					String lastName = fc.utobj().generateTestData("Wk");
					String formName = fc.utobj().generateTestData("FORM");
					leadInfo.put("firstName", firstName);
					leadInfo.put("lastName", lastName);
					leadInfo.put("country", dataSet.get("country"));
					// leadInfo.put("state", dataSet.get("stateID"));
					leadInfo.put("email", dataSet.get("emailID"));
					// leadInfo.put("leadOwner", "FranConnect Administrator");
					leadInfo.put("leadSourceCategory", "Internet");
					leadInfo.put("leadSourceDetails", "BISON");
					leadInfo.put("zipCode", zipCode);
					leadInfo.put("state", state);
					leadInfo.put("owner", "Based on Assignment Rules");
					String fileName = "OneLeadImportFile.csv";
					fc.utobj().printTestStep("Adding Lead from Import");
					fileName = fc.utobj().getFilePathFromTestData(fileName);
					try {
						addLeadFromAllSourecesObj.addLeadFromImport(driver, fileName, leadInfo);
					} catch (Exception e) {
						System.out.println("==Sales teriotries ==" + e.getMessage());
					}
					// addLeadFromAllSourecesObj.addLeadFromWebForm(driver,
					// formName, leadInfo,"Confirmation Message");
					p3.searchByLeadName(driver, firstName, lastName);
					fc.utobj().clickElement(driver, driver
							.findElement(By.xpath(".//*[contains(text(),'" + firstName + " " + lastName + "')]")));

					fc.utobj().printTestStep("Verify a lead and Go to get Lead Owner");
					WebElement ownerelement = driver
							.findElement(By.xpath(".//tr/td[contains(text(),'Lead Owner :')]/following-sibling::td"));
					firstOwner = ownerelement.getText();

					testCaseIdInternal = "TC_FC_QA_Sale_Lead_Primary_Owner_Assignment";
					dataSet = fc.utobj().readTestData("sales", testCaseIdInternal);
					fc.utobj().printTestStep("Adding lead from Web Form");
					leadInfo = new HashMap<String, String>();
					firstName = fc.utobj().generateTestData("Wk");
					lastName = fc.utobj().generateTestData("Wk");
					formName = fc.utobj().generateTestData("FORM");
					leadInfo.put("firstName", firstName);
					leadInfo.put("lastName", lastName);
					leadInfo.put("country", dataSet.get("country"));
					// leadInfo.put("state", dataSet.get("stateID"));
					leadInfo.put("email", dataSet.get("emailID"));
					// leadInfo.put("leadOwner", "FranConnect Administrator");
					/*
					 * leadInfo.put("leadSourceCategory",
					 * dataSet.get("leadSource2ID"));
					 * leadInfo.put("leadSourceDetails",
					 * dataSet.get("leadSource3ID"));
					 */
					leadInfo.put("leadSourceCategory", "Internet");
					leadInfo.put("leadSourceDetails", "BISON");

					leadInfo.put("zipCode", zipCode);
					leadInfo.put("state", state);
					leadInfo.put("owner", "Based on Assignment Rules");
					addLeadFromAllSourecesObj.addLeadFromImport(driver, fileName, leadInfo);

					p3.searchByLeadName(driver, firstName, lastName);
					fc.utobj().clickElement(driver, driver
							.findElement(By.xpath(".//*[contains(text(),'" + firstName + " " + lastName + "')]")));

					fc.utobj().printTestStep("Verify a lead and Go to get Lead Owner");
					ownerelement = driver
							.findElement(By.xpath(".//tr/td[contains(text(),'Lead Owner :')]/following-sibling::td"));
					secondOwner = ownerelement.getText();

					testCaseIdInternal = "TC_FC_QA_Sale_Lead_Primary_Owner_Assignment";
					dataSet = fc.utobj().readTestData("sales", testCaseIdInternal);
					fc.utobj().printTestStep("Adding lead from Web Form");
					leadInfo = new HashMap<String, String>();
					firstName = fc.utobj().generateTestData("Wk");
					lastName = fc.utobj().generateTestData("Wk");
					formName = fc.utobj().generateTestData("FORM");
					leadInfo.put("firstName", firstName);
					leadInfo.put("lastName", lastName);
					leadInfo.put("country", dataSet.get("country"));
					leadInfo.put("state", dataSet.get("stateID"));
					leadInfo.put("email", dataSet.get("emailID"));
					// leadInfo.put("leadOwner", "FranConnect Administrator");
					/*
					 * leadInfo.put("leadSourceCategory",
					 * dataSet.get("leadSource2ID"));
					 * leadInfo.put("leadSourceDetails",
					 * dataSet.get("leadSource3ID"));
					 */
					leadInfo.put("leadSourceCategory", "Internet");
					leadInfo.put("leadSourceDetails", "BISON");

					leadInfo.put("zipCode", zipCode);
					leadInfo.put("state", state);
					leadInfo.put("owner", "Based on Assignment Rules");
					addLeadFromAllSourecesObj.addLeadFromImport(driver, fileName, leadInfo);

					p3.searchByLeadName(driver, firstName, lastName);
					fc.utobj().clickElement(driver, driver
							.findElement(By.xpath(".//*[contains(text(),'" + firstName + " " + lastName + "')]")));

					fc.utobj().printTestStep("Verify a lead and Go to get Lead Owner");
					ownerelement = driver
							.findElement(By.xpath(".//tr/td[contains(text(),'Lead Owner :')]/following-sibling::td"));
					thirdOwner = ownerelement.getText();

					System.out.println(firstOwner + "====" + secondOwner + "====" + thirdOwner);

					if (!(firstOwner.equalsIgnoreCase(secondOwner)) && !(secondOwner.equalsIgnoreCase(thirdOwner))
							&& !(firstOwner.equalsIgnoreCase(thirdOwner))) {
						actualOner = firstOwner + " / " + secondOwner + " / " + thirdOwner;
						fc.utobj().printTestStep("Assign Lead Owner by Sales Territories  is working as excpeted");
					} else {
						System.out.println("Fails");
						fc.utobj().throwsException(
								"Assign Lead Owner by Sales Territories  is  not working as expcected");
					}

				} else if ("RestAPI".equals(fromWhere)) {
					FSSearchPageTest p3 = new FSSearchPageTest();
					testCaseIdInternal = "TC_Sales_SmartGroup_002";
					dataSet = fc.utobj().readTestData("sales", testCaseIdInternal);
					String firstName = fc.utobj().generateTestData("Re");
					String lastName = fc.utobj().generateTestData("Re");
					country = "USA";
					// String state=dataSet.get("state");
					String leadOwner = "FranConnect Administrator";
					String leadSourceCategory = "Friends";
					String leadSourceDetails = "Friends";
					Map<String, String> UniqueKey_PrimaryInfo = new HashMap<String, String>();
					UniqueKey_PrimaryInfo.put("firstName", firstName);
					UniqueKey_PrimaryInfo.put("lastName", lastName);
					/*
					 * UniqueKey_PrimaryInfo.put("leadSource2ID",
					 * leadSourceCategory);
					 * UniqueKey_PrimaryInfo.put("leadSource3ID",
					 * leadSourceDetails);
					 */
					UniqueKey_PrimaryInfo.put("leadSource2ID", "Internet");
					UniqueKey_PrimaryInfo.put("leadSource3ID", "BISON");
					UniqueKey_PrimaryInfo.put("emailID", "harish@test.net");
					UniqueKey_PrimaryInfo.put("country", country);
					UniqueKey_PrimaryInfo.put("stateID", state);
					UniqueKey_PrimaryInfo.put("zip", zipCode);
					fc.utobj().printTestStep("Adding lead from Rest API");
					addLeadFromAllSourecesObj.addleadFromRestApi(driver, dataSet, testCaseId, UniqueKey_PrimaryInfo);

					driver.navigate().to(FranconnectUtil.config.get("buildUrl"));
					fc.loginpage().login(driver);
					fc.sales().sales_common().fsModule(driver);
					p3.searchByLeadName(driver, firstName, lastName);

					fc.utobj().clickElement(driver, driver
							.findElement(By.xpath(".//*[contains(text(),'" + firstName + " " + lastName + "')]")));

					fc.utobj().printTestStep("Verify a lead and Go to get Lead Owner");
					WebElement ownerelement = driver
							.findElement(By.xpath(".//tr/td[contains(text(),'Lead Owner :')]/following-sibling::td"));
					firstOwner = ownerelement.getText();

					testCaseIdInternal = "TC_Sales_SmartGroup_002";
					dataSet = fc.utobj().readTestData("sales", testCaseIdInternal);
					firstName = fc.utobj().generateTestData("Re");
					lastName = fc.utobj().generateTestData("Re");
					country = "USA";
					// String state=dataSet.get("state");
					leadOwner = "FranConnect Administrator";
					leadSourceCategory = "Friends";
					leadSourceDetails = "Friends";
					UniqueKey_PrimaryInfo = new HashMap<String, String>();
					UniqueKey_PrimaryInfo.put("firstName", firstName);
					UniqueKey_PrimaryInfo.put("lastName", lastName);
					/*
					 * UniqueKey_PrimaryInfo.put("leadSource2ID",
					 * leadSourceCategory);
					 * UniqueKey_PrimaryInfo.put("leadSource3ID",
					 * leadSourceDetails);
					 */
					UniqueKey_PrimaryInfo.put("leadSource2ID", "Internet");
					UniqueKey_PrimaryInfo.put("leadSource3ID", "BISON");
					UniqueKey_PrimaryInfo.put("emailID", "harish@test.net");
					UniqueKey_PrimaryInfo.put("country", country);
					UniqueKey_PrimaryInfo.put("stateID", state);
					UniqueKey_PrimaryInfo.put("zip", zipCode);
					fc.utobj().printTestStep("Adding lead from Rest API");
					addLeadFromAllSourecesObj.addleadFromRestApi(driver, dataSet, testCaseId, UniqueKey_PrimaryInfo);

					driver.navigate().to(FranconnectUtil.config.get("buildUrl"));
					fc.loginpage().login(driver);
					fc.sales().sales_common().fsModule(driver);
					p3.searchByLeadName(driver, firstName, lastName);

					fc.utobj().clickElement(driver, driver
							.findElement(By.xpath(".//*[contains(text(),'" + firstName + " " + lastName + "')]")));
					fc.utobj().printTestStep("Verify a lead and Go to get Lead Owner");
					ownerelement = driver
							.findElement(By.xpath(".//tr/td[contains(text(),'Lead Owner :')]/following-sibling::td"));
					secondOwner = ownerelement.getText();

					testCaseIdInternal = "TC_Sales_SmartGroup_002";
					dataSet = fc.utobj().readTestData("sales", testCaseIdInternal);
					firstName = fc.utobj().generateTestData("Re");
					lastName = fc.utobj().generateTestData("Re");
					country = "USA";
					// String state=dataSet.get("state");
					leadOwner = "FranConnect Administrator";
					leadSourceCategory = "Friends";
					leadSourceDetails = "Friends";
					UniqueKey_PrimaryInfo = new HashMap<String, String>();
					UniqueKey_PrimaryInfo.put("firstName", firstName);
					UniqueKey_PrimaryInfo.put("lastName", lastName);
					/*
					 * UniqueKey_PrimaryInfo.put("leadSource2ID",
					 * leadSourceCategory);
					 * UniqueKey_PrimaryInfo.put("leadSource3ID",
					 * leadSourceDetails);
					 */
					UniqueKey_PrimaryInfo.put("leadSource2ID", "Internet");
					UniqueKey_PrimaryInfo.put("leadSource3ID", "BISON");

					UniqueKey_PrimaryInfo.put("emailID", "harish@test.net");
					UniqueKey_PrimaryInfo.put("country", country);
					UniqueKey_PrimaryInfo.put("stateID", state);
					UniqueKey_PrimaryInfo.put("zip", zipCode);
					fc.utobj().printTestStep("Adding lead from Rest API");
					addLeadFromAllSourecesObj.addleadFromRestApi(driver, dataSet, testCaseId, UniqueKey_PrimaryInfo);

					driver.navigate().to(FranconnectUtil.config.get("buildUrl"));
					fc.loginpage().login(driver);
					fc.sales().sales_common().fsModule(driver);
					p3.searchByLeadName(driver, firstName, lastName);

					fc.utobj().clickElement(driver, driver
							.findElement(By.xpath(".//*[contains(text(),'" + firstName + " " + lastName + "')]")));

					fc.utobj().printTestStep("Verify a lead and Go to get Lead Owner");
					ownerelement = driver
							.findElement(By.xpath(".//tr/td[contains(text(),'Lead Owner :')]/following-sibling::td"));
					thirdOwner = ownerelement.getText();

					System.out.println(firstOwner + "====" + secondOwner + "====" + thirdOwner);

					if (!(firstOwner.equalsIgnoreCase(secondOwner)) && !(secondOwner.equalsIgnoreCase(thirdOwner))
							&& !(firstOwner.equalsIgnoreCase(thirdOwner))) {
						actualOner = firstOwner + " / " + secondOwner + " / " + thirdOwner;
						fc.utobj().printTestStep("Assign Lead Owner by Sales Territories  is working as excpeted");
					} else {
						System.out.println("Fails");
						fc.utobj().throwsException(
								"Assign Lead Owner by Sales Territories  is  not working as expcected");
					}
				}
			}

		} else {
			state = "Florida";
			// In case when round robin is set but no user set for owner should
			// goes to default owner
			fc.adminpage().adminPage(driver);
			// adsales.assignLeadOwners(driver);

			String userNameOwner = "FranConnect Administrator";
			fc.utobj().printTestStep("Create user to Assign Owner");
			String userNameRC = fc.utobj().generateTestData("IZ"); // "AssignOwner";
			userNameOwner = userNameRC + " " + userNameRC;
			String emailId = "salesautomation@staffex.com";
			if ("CorporateUser".equals(UserType)) {
				CorporateUser corpUser = new CorporateUser();
				corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
				corpUser.setEmail(emailId);
				corpUser = corpTest.createDefaultUser(driver, corpUser);
			} else if ("RegionalUser".equals(UserType)) {
				String regionName = fc.utobj().generateTestData("RG");
				regUser.addRegionalUser(driver, userNameRC, regionName, emailId);
				userNameOwner = userNameRC + " " + userNameRC + " (RU)";
			}
			// userNameOwner = userNameRC+" "+userNameRC;
			fc.utobj().printTestStep("Verify the Create user");
			fc.adminpage().adminPage(driver);
			adsales.assignLeadOwners(driver);

			fc.utobj().printTestStep("Verify the user is available in Default Owner drop down");

			boolean isLeadOwnerPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//select[@name='area1']/option[contains(text(),'" + userNameOwner + "')]");
			if (isLeadOwnerPresent == false) {
				fc.utobj().throwsException("User not present in Default Owner dropdown");
			}

			if (noownerset) {
				fc.utobj().selectDropDown(driver, pobj.defaultOwnerDrpDown, "Select");
				userNameOwner = "FranConnect Administrator";
			} else {
				fc.utobj().selectDropDown(driver, pobj.defaultOwnerDrpDown, userNameOwner);
			}

			fc.utobj().clickElement(driver, pobj.updateLeadOwnerBtn);

			if ("SystemLead".equals(fromWhere)) {
				fc.sales().sales_common().fsModule(driver);
				Sales fs = new Sales();
				fs.leadManagement(driver);
				FSLeadSummaryPageTest fsPage = new FSLeadSummaryPageTest();
				fsPage.clickAddLeadLink(driver);
				fc.utobj().printTestStep("Add a lead and Go to Primary Info");
				testCaseIdInternal = "TC_FC_QA_Sale_Lead_Primary_Owner_Assignment";
				dataSet = fc.utobj().readTestData("sales", testCaseIdInternal);
				String firstName = fc.utobj().generateTestData("AK");
				String lastName = fc.utobj().generateTestData("BK");
				dataSet.put("firstName", firstName);
				dataSet.put("lastName", lastName);
				dataSet.put("automatic", "As per Assignment Scheme");
				dataSet.put("zip", zipCode);
				dataSet.put("stateID", state);
				dataSet.put("leadSource2ID", "Internet");
				dataSet.put("leadSource3ID", "BISON");

				printInfo = addLeadFromAllSourecesObj.fillFormDataWithDataset(driver, testCaseIdInternal, dataSet);
				fc.utobj().printTestStep("Verify a lead and Go to get Lead Owner");
				if (printInfo != null && printInfo.size() > 0) {
					isLeadOwnerPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
							".//tr/td[contains(text(),'Lead Owner :')]/following-sibling::td[contains (text() ,'"
									+ userNameOwner + "')]");
					if (isLeadOwnerPresent == false) {
						fc.utobj().throwsException("As per Assignment Scheme owner is not assigned");
					}

				}
			} else if ("WebForm".equals(fromWhere)) {
				testCaseIdInternal = "TC_FC_QA_Sale_Lead_Primary_Owner_Assignment";
				dataSet = fc.utobj().readTestData("sales", testCaseIdInternal);
				FSSearchPageTest p3 = new FSSearchPageTest();
				fc.utobj().printTestStep("Adding lead from Web Form");
				Map<String, String> leadInfo = new HashMap<String, String>();
				String firstName = fc.utobj().generateTestData("Wk");
				String lastName = fc.utobj().generateTestData("Wk");
				String formName = fc.utobj().generateTestData("FORM");
				leadInfo.put("firstName", firstName);
				leadInfo.put("lastName", lastName);
				leadInfo.put("country", dataSet.get("country"));
				// leadInfo.put("state", dataSet.get("stateID"));
				leadInfo.put("state", state);

				leadInfo.put("email", dataSet.get("emailID"));
				leadInfo.put("leadOwner", userNameOwner);
				/*
				 * leadInfo.put("leadSourceCategory",
				 * dataSet.get("leadSource2ID"));
				 * leadInfo.put("leadSourceDetails",
				 * dataSet.get("leadSource3ID"));
				 */
				leadInfo.put("leadSourceCategory", "Internet");
				leadInfo.put("leadSourceDetails", "BISON");

				leadInfo.put("zipCode", zipCode);
				addLeadFromAllSourecesObj.addLeadFromWebForm(driver, formName, leadInfo, "Confirmation Message");
				p3.searchByLeadName(driver, firstName, lastName);
				fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
						".//*[contains(text(),'" + firstName + " " + lastName + "')]"));

				fc.utobj().printTestStep("Verify a lead and Go to get Lead Owner");
				isLeadOwnerPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
						".//tr/td[contains(text(),'Lead Owner :')]/following-sibling::td[contains (text() ,'"
								+ userNameOwner + "')]");
				if (isLeadOwnerPresent == false) {
					fc.utobj().throwsException("As per Assignment Scheme owner is not assigned");
				}

			} else if ("Import".equals(fromWhere)) {
				testCaseIdInternal = "TC_FC_QA_Sale_Lead_Primary_Owner_Assignment";
				dataSet = fc.utobj().readTestData("sales", testCaseIdInternal);
				FSSearchPageTest p3 = new FSSearchPageTest();
				fc.utobj().printTestStep("Adding lead from Web Form");
				Map<String, String> leadInfo = new HashMap<String, String>();
				String firstName = fc.utobj().generateTestData("Wk");
				String lastName = fc.utobj().generateTestData("Wk");
				String formName = fc.utobj().generateTestData("FORM");
				leadInfo.put("firstName", firstName);
				leadInfo.put("lastName", lastName);
				leadInfo.put("country", dataSet.get("country"));
				// leadInfo.put("state", dataSet.get("stateID"));
				leadInfo.put("state", state);

				leadInfo.put("email", dataSet.get("emailID"));
				leadInfo.put("leadOwner", userNameOwner);
				/*
				 * leadInfo.put("leadSourceCategory",
				 * dataSet.get("leadSource2ID"));
				 * leadInfo.put("leadSourceDetails",
				 * dataSet.get("leadSource3ID"));
				 */
				leadInfo.put("leadSourceCategory", "Internet");
				leadInfo.put("leadSourceDetails", "BISON");

				leadInfo.put("zipCode", zipCode);
				// addLeadFromAllSourecesObj.addLeadFromWebForm(driver,
				// formName, leadInfo,"Confirmation Message");
				leadInfo.put("owner", "Based on Assignment Rules");
				String fileName = "OneLeadImportFile.csv";
				fc.utobj().printTestStep("Adding Lead from Import");
				fileName = fc.utobj().getFilePathFromTestData(fileName);

				try {
					addLeadFromAllSourecesObj.addLeadFromImport(driver, fileName, leadInfo);
				} catch (Exception e) {

				}
				p3.searchByLeadName(driver, firstName, lastName);
				fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
						".//*[contains(text(),'" + firstName + " " + lastName + "')]"));

				fc.utobj().printTestStep("Verify a lead and Go to get Lead Owner");
				isLeadOwnerPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
						".//tr/td[contains(text(),'Lead Owner :')]/following-sibling::td[contains (text() ,'"
								+ userNameOwner + "')]");
				if (isLeadOwnerPresent == false) {
					fc.utobj().throwsException("As per Assignment Scheme owner is not assigned");
				}

			} else if ("RestAPI".equals(fromWhere)) {
				FSSearchPageTest p3 = new FSSearchPageTest();
				testCaseIdInternal = "TC_Sales_SmartGroup_002";
				dataSet = fc.utobj().readTestData("sales", testCaseIdInternal);
				String firstName = fc.utobj().generateTestData("Re");
				String lastName = fc.utobj().generateTestData("Re");
				country = "USA";
				// String state=dataSet.get("state");
				String leadOwner = "FranConnect Administrator";
				String leadSourceCategory = "Friends";
				String leadSourceDetails = "Friends";
				Map<String, String> UniqueKey_PrimaryInfo = new HashMap<String, String>();
				UniqueKey_PrimaryInfo.put("firstName", firstName);
				UniqueKey_PrimaryInfo.put("lastName", lastName);
				/*
				 * UniqueKey_PrimaryInfo.put("leadSource2ID",
				 * leadSourceCategory);
				 * UniqueKey_PrimaryInfo.put("leadSource3ID",
				 * leadSourceDetails);
				 */
				UniqueKey_PrimaryInfo.put("leadSource2ID", "Internet");
				UniqueKey_PrimaryInfo.put("leadSource3ID", "BISON");
				UniqueKey_PrimaryInfo.put("emailID", emailId);
				UniqueKey_PrimaryInfo.put("country", country);
				UniqueKey_PrimaryInfo.put("stateID", state);
				UniqueKey_PrimaryInfo.put("zip", zipCode);
				fc.utobj().printTestStep("Adding lead from Rest API");
				addLeadFromAllSourecesObj.addleadFromRestApi(driver, dataSet, testCaseId, UniqueKey_PrimaryInfo);

				driver.navigate().to(FranconnectUtil.config.get("buildUrl"));
				fc.loginpage().login(driver);
				fc.sales().sales_common().fsModule(driver);
				p3.searchByLeadName(driver, firstName, lastName);

				fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
						".//*[contains(text(),'" + firstName + " " + lastName + "')]"));

				fc.utobj().printTestStep("Verify a lead and Go to get Lead Owner");
				isLeadOwnerPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
						".//tr/td[contains(text(),'Lead Owner :')]/following-sibling::td[contains (text() ,'"
								+ userNameOwner + "')]");
				if (isLeadOwnerPresent == false) {
					fc.utobj().throwsException("As per Assignment Scheme owner is not assigned");
				}
			}
		}
		// End Here
		return actualOner;
	}

	public String getSalesTerritoriesOwnerState(WebDriver driver, String testCaseId, String UserType, String fromWhere,
			boolean addDataforroundrobin, boolean noownerset) throws Exception {
		String actualOner = "";
		String testCaseIdInternal = "" + testCaseId;
		Map<String, String> dataSet = fc.utobj().readTestData("sales", testCaseId);
		Map<String, String> printInfo = null;
		AddLeadFromAllSources addLeadFromAllSourecesObj = new AddLeadFromAllSources();
		LeadSummaryUI pobj = new LeadSummaryUI(driver);
		AdminFranchiseSalesManageSalesTerritoryCreateSalesTerritoryPageTest p1 = new AdminFranchiseSalesManageSalesTerritoryCreateSalesTerritoryPageTest();
		AdminUsersManageCorporateUsersAddCorporateUserPageTest corpTest = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
		AdminUsersManageRegionalUsersAddRegionalUserPageTest regUser = new AdminUsersManageRegionalUsersAddRegionalUserPageTest();
		String firstOwner = "";
		String secondOwner = "";
		String thirdOwner = "";
		List<String> listInfo = new ArrayList<String>();
		// Sales Territories

		String salesTerritory = "STe10114856";// fc.utobj().generateTestData("ST");
		String zipCode = fc.utobj().generateRandomNumber();
		String country = "USA";
		String state = "Alaska";

		AdminSales adsales = new AdminSales();

		try {
			adsales.adminManageSalesTerritory(driver);
			fc.utobj().printTestStep("Go to Admin Sales territory section \nAdd Sales Territory");
			boolean isSalesTeritoryalready = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//a[contains(text(),'" + salesTerritory
					+ "')]/ancestor::td/following-sibling::td[contains(text(),'States')]");
			if (isSalesTeritoryalready == false) {
				salesTerritory = p1.addSalesTerritoryWithState(driver, salesTerritory, country, state);
				// fc.utobj().throwsException("Sales states teritory not got
				// added");
			} else {
				fc.utobj().printTestStep("Delete Sales Territory");
				fc.utobj().actionImgOption(driver, salesTerritory, "Delete");
				fc.utobj().acceptAlertBox(driver);
				salesTerritory = p1.addSalesTerritoryWithState(driver, salesTerritory, country, state);

			}
		} catch (Exception e) {

		}
		// End Sales Territories

		fc.adminpage().adminPage(driver);
		adsales.assignLeadOwners(driver);
		List<WebElement> oRadioButton = driver.findElements(By.name("radiobutton"));
		boolean bValue = false;
		bValue = oRadioButton.get(1).isSelected();
		if (oRadioButton.get(1).isSelected()) {

		} else {
			oRadioButton.get(1).click();
		}

		fc.utobj().printTestStep("Click Continue");
		fc.utobj().clickElement(driver, pobj.continueButton);

		System.out.println("salesTerritory=====" + salesTerritory);

		if (addDataforroundrobin) {

			if ((salesTerritory != null)) {
				for (int i = 0; i < 3; i++) {
					fc.adminpage().adminPage(driver);
					// adsales.assignLeadOwners(driver);

					String userNameOwner = "FranConnect Administrator";
					fc.utobj().printTestStep("Verify the Create user");
					String userNameRC = fc.utobj().generateTestData("IZ"); // "AssignOwner";

					String emailId = "salesautomation@staffex.com";
					if ("CorporateUser".equals(UserType)) {
						CorporateUser corpUser = new CorporateUser();
						corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
						corpUser.setEmail(emailId);
						corpUser = corpTest.createDefaultUser(driver, corpUser);
					} else if ("RegionalUser".equals(UserType)) {
						String regionName = fc.utobj().generateTestData("RG");
						regUser.addRegionalUser(driver, userNameRC, regionName, emailId);
					}
					userNameOwner = userNameRC + " " + userNameRC;
					listInfo.add(userNameOwner);
					fc.utobj().printTestStep("Verify the Create user");
					fc.adminpage().adminPage(driver);
					adsales.assignLeadOwners(driver);

					fc.utobj().printTestStep("Verify the user is available in Default Owner drop down");

					boolean isLeadOwnerPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
							".//select[@name='area1']/option[contains(text(),'" + userNameOwner + "')]");
					if (isLeadOwnerPresent == false) {
						fc.utobj().throwsException("User not present in Default Owner dropdown");
					}

					oRadioButton = driver.findElements(By.name("radiobutton"));
					bValue = false;
					bValue = oRadioButton.get(1).isSelected();
					if (oRadioButton.get(1).isSelected()) {

					} else {
						oRadioButton.get(1).click();
					}

					fc.utobj().printTestStep("Click Continue");
					fc.utobj().clickElement(driver, pobj.continueButton);

					boolean availableLeadOwner = fc.utobj().verifyElementOnVisible_ByXpath(driver,
							".//tr/td[contains(text(),'" + salesTerritory
									+ "')]/following-sibling::td/select/option[contains(text(),'" + userNameOwner
									+ "')]");
					if (availableLeadOwner == false) {
						fc.utobj().throwsException("User is  not available in Sales Terriotries Lead Owners drop-down");
					}

				}

				fc.utobj().printTestStep("fill the owners with respect to sale terriortires drop down");
				fc.adminpage().adminPage(driver);
				adsales.assignLeadOwners(driver);

				oRadioButton = driver.findElements(By.name("radiobutton"));
				bValue = false;
				bValue = oRadioButton.get(1).isSelected();
				if (oRadioButton.get(1).isSelected()) {

				} else {
					oRadioButton.get(1).click();
				}

				fc.utobj().printTestStep("Click Continue");
				fc.utobj().clickElement(driver, pobj.continueButton);

				fc.utobj()
						.selectMultipleValFromMultiSelect(driver,
								fc.utobj()
										.getElementByXpath(driver,
												".//tr/td[contains(text(),'" + salesTerritory
														+ "')]/following-sibling::td/div[@class='ms-parent']"),
								listInfo);

				fc.utobj().clickElement(driver, pobj.updateLeadOwnerBtn);

				boolean configredLeadOwner = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//tr/td[contains(text(),'" + salesTerritory
						+ "')]/following-sibling::td[contains(text(),'" + listInfo.get(0) + "')]");
				if (configredLeadOwner == false) {
					fc.utobj().throwsException("User is  not available in  Configured Lead Owners");
				}

			}

			// allroundrobin=
			// driver.findElements(By.xpath(".//*[@id='templateOrder']/option"));

			// Start here to check Lead

			if (salesTerritory != null) {

				if ("SystemLead".equals(fromWhere)) {
					// First Lead
					fc.sales().sales_common().fsModule(driver);
					Sales fs = new Sales();
					fs.leadManagement(driver);
					FSLeadSummaryPageTest fsPage = new FSLeadSummaryPageTest();
					fsPage.clickAddLeadLink(driver);
					fc.utobj().printTestStep("Add a lead and Go to Primary Info");
					testCaseIdInternal = "TC_FC_QA_Sale_Lead_Primary_Owner_Assignment";
					dataSet = fc.utobj().readTestData("sales", testCaseIdInternal);
					String firstName = fc.utobj().generateTestData("AK");
					String lastName = fc.utobj().generateTestData("BK");
					dataSet.put("firstName", firstName);
					dataSet.put("lastName", lastName);
					dataSet.put("automatic", "As per Assignment Scheme");
					dataSet.put("zip", zipCode);
					dataSet.put("stateID", state);
					printInfo = addLeadFromAllSourecesObj.fillFormDataWithDataset(driver, testCaseIdInternal, dataSet);

					if (printInfo != null && printInfo.size() > 0) {
						fc.utobj().printTestStep("Verify a lead and Go to get First Lead Owner");
						// .//tr/td[contains(text(),'Lead Owner
						// :')]/following-sibling::td[contains (text()
						// ,'"+userNameOwner+"')]
						WebElement ownerelement = driver.findElement(
								By.xpath(".//tr/td[contains(text(),'Lead Owner :')]/following-sibling::td"));
						firstOwner = ownerelement.getText();

					}

					// End First Lead
					// Enter Second Lead
					fc.sales().sales_common().fsModule(driver);
					fs = new Sales();
					fs.leadManagement(driver);
					fsPage.clickAddLeadLink(driver);
					fc.utobj().printTestStep("Add a lead and Go to Primary Info");
					testCaseIdInternal = "TC_FC_QA_Sale_Lead_Primary_Owner_Assignment";
					dataSet = fc.utobj().readTestData("sales", testCaseIdInternal);
					firstName = fc.utobj().generateTestData("AK");
					lastName = fc.utobj().generateTestData("BK");
					dataSet.put("firstName", firstName);
					dataSet.put("lastName", lastName);
					dataSet.put("automatic", "As per Assignment Scheme");
					dataSet.put("zip", zipCode);
					dataSet.put("stateID", state);
					printInfo = addLeadFromAllSourecesObj.fillFormDataWithDataset(driver, testCaseIdInternal, dataSet);

					if (printInfo != null && printInfo.size() > 0) {
						fc.utobj().printTestStep("Verify a lead and Go to get Second Lead Owner");
						// .//tr/td[contains(text(),'Lead Owner
						// :')]/following-sibling::td[contains (text()
						// ,'"+userNameOwner+"')]
						WebElement ownerelement = driver.findElement(
								By.xpath(".//tr/td[contains(text(),'Lead Owner :')]/following-sibling::td"));
						secondOwner = ownerelement.getText();

					}
					// End Second Lead
					// Enter Third Lead
					fc.sales().sales_common().fsModule(driver);
					fs = new Sales();
					fs.leadManagement(driver);
					fsPage.clickAddLeadLink(driver);
					fc.utobj().printTestStep("Add a lead and Go to Primary Info");
					testCaseIdInternal = "TC_FC_QA_Sale_Lead_Primary_Owner_Assignment";
					dataSet = fc.utobj().readTestData("sales", testCaseIdInternal);
					firstName = fc.utobj().generateTestData("AK");
					lastName = fc.utobj().generateTestData("BK");
					dataSet.put("firstName", firstName);
					dataSet.put("lastName", lastName);
					dataSet.put("automatic", "As per Assignment Scheme");
					dataSet.put("zip", zipCode);
					dataSet.put("stateID", state);
					printInfo = addLeadFromAllSourecesObj.fillFormDataWithDataset(driver, testCaseIdInternal, dataSet);

					if (printInfo != null && printInfo.size() > 0) {
						fc.utobj().printTestStep("Verify a lead and Go to get Third Lead Owner");
						// .//tr/td[contains(text(),'Lead Owner
						// :')]/following-sibling::td[contains (text()
						// ,'"+userNameOwner+"')]
						WebElement ownerelement = driver.findElement(
								By.xpath(".//tr/td[contains(text(),'Lead Owner :')]/following-sibling::td"));
						thirdOwner = ownerelement.getText();

					}
					// End Second Lead

					System.out.println(firstOwner + "====" + secondOwner + "====" + thirdOwner);

					if (!(firstOwner.equalsIgnoreCase(secondOwner)) && !(secondOwner.equalsIgnoreCase(thirdOwner))
							&& !(firstOwner.equalsIgnoreCase(thirdOwner))) {
						actualOner = firstOwner + " / " + secondOwner + " / " + thirdOwner;
						fc.utobj().printTestStep("Assign Lead Owner by Sales Territories is working as excpeted");
					} else {
						System.out.println("Fails");
						fc.utobj().throwsException(
								"Assign Lead Owner by Sales Territories  is  not working as expcected");
					}

				} // End system Lead method
				else if ("WebForm".equals(fromWhere)) {
					testCaseIdInternal = "TC_FC_QA_Sale_Lead_Primary_Owner_Assignment";
					dataSet = fc.utobj().readTestData("sales", testCaseIdInternal);
					FSSearchPageTest p3 = new FSSearchPageTest();
					fc.utobj().printTestStep("Adding lead from Web Form");
					Map<String, String> leadInfo = new HashMap<String, String>();
					String firstName = fc.utobj().generateTestData("Wk");
					String lastName = fc.utobj().generateTestData("Wk");
					String formName = fc.utobj().generateTestData("FORM");
					leadInfo.put("firstName", firstName);
					leadInfo.put("lastName", lastName);
					leadInfo.put("country", dataSet.get("country"));
					// leadInfo.put("state", dataSet.get("stateID"));
					leadInfo.put("email", dataSet.get("emailID"));
					// leadInfo.put("leadOwner", "FranConnect Administrator");
					leadInfo.put("leadSourceCategory", dataSet.get("leadSource2ID"));
					leadInfo.put("leadSourceDetails", dataSet.get("leadSource3ID"));
					leadInfo.put("zipCode", zipCode);
					leadInfo.put("state", state);
					addLeadFromAllSourecesObj.addLeadFromWebForm(driver, formName, leadInfo, "Confirmation Message");
					p3.searchByLeadName(driver, firstName, lastName);
					fc.utobj().clickElement(driver, driver
							.findElement(By.xpath(".//*[contains(text(),'" + firstName + " " + lastName + "')]")));

					fc.utobj().printTestStep("Verify a lead and Go to get Lead Owner");
					WebElement ownerelement = driver
							.findElement(By.xpath(".//tr/td[contains(text(),'Lead Owner :')]/following-sibling::td"));
					firstOwner = ownerelement.getText();

					testCaseIdInternal = "TC_FC_QA_Sale_Lead_Primary_Owner_Assignment";
					dataSet = fc.utobj().readTestData("sales", testCaseIdInternal);
					fc.utobj().printTestStep("Adding lead from Web Form");
					leadInfo = new HashMap<String, String>();
					firstName = fc.utobj().generateTestData("Wk");
					lastName = fc.utobj().generateTestData("Wk");
					formName = fc.utobj().generateTestData("FORM");
					leadInfo.put("firstName", firstName);
					leadInfo.put("lastName", lastName);
					leadInfo.put("country", dataSet.get("country"));
					// leadInfo.put("state", dataSet.get("stateID"));
					leadInfo.put("email", dataSet.get("emailID"));
					// leadInfo.put("leadOwner", "FranConnect Administrator");
					leadInfo.put("leadSourceCategory", dataSet.get("leadSource2ID"));
					leadInfo.put("leadSourceDetails", dataSet.get("leadSource3ID"));
					leadInfo.put("zipCode", zipCode);
					leadInfo.put("state", state);
					addLeadFromAllSourecesObj.addLeadFromWebForm(driver, formName, leadInfo, "Confirmation Message");
					p3.searchByLeadName(driver, firstName, lastName);
					fc.utobj().clickElement(driver, driver
							.findElement(By.xpath(".//*[contains(text(),'" + firstName + " " + lastName + "')]")));

					fc.utobj().printTestStep("Verify a lead and Go to get Lead Owner");
					ownerelement = driver
							.findElement(By.xpath(".//tr/td[contains(text(),'Lead Owner :')]/following-sibling::td"));
					secondOwner = ownerelement.getText();

					testCaseIdInternal = "TC_FC_QA_Sale_Lead_Primary_Owner_Assignment";
					dataSet = fc.utobj().readTestData("sales", testCaseIdInternal);
					fc.utobj().printTestStep("Adding lead from Web Form");
					leadInfo = new HashMap<String, String>();
					firstName = fc.utobj().generateTestData("Wk");
					lastName = fc.utobj().generateTestData("Wk");
					formName = fc.utobj().generateTestData("FORM");
					leadInfo.put("firstName", firstName);
					leadInfo.put("lastName", lastName);
					leadInfo.put("country", dataSet.get("country"));
					leadInfo.put("state", dataSet.get("stateID"));
					leadInfo.put("email", dataSet.get("emailID"));
					// leadInfo.put("leadOwner", "FranConnect Administrator");
					leadInfo.put("leadSourceCategory", dataSet.get("leadSource2ID"));
					leadInfo.put("leadSourceDetails", dataSet.get("leadSource3ID"));
					leadInfo.put("zipCode", zipCode);
					leadInfo.put("state", state);
					addLeadFromAllSourecesObj.addLeadFromWebForm(driver, formName, leadInfo, "Confirmation Message");
					p3.searchByLeadName(driver, firstName, lastName);
					fc.utobj().clickElement(driver, driver
							.findElement(By.xpath(".//*[contains(text(),'" + firstName + " " + lastName + "')]")));

					fc.utobj().printTestStep("Verify a lead and Go to get Lead Owner");
					ownerelement = driver
							.findElement(By.xpath(".//tr/td[contains(text(),'Lead Owner :')]/following-sibling::td"));
					thirdOwner = ownerelement.getText();

					System.out.println(firstOwner + "====" + secondOwner + "====" + thirdOwner);

					if (!(firstOwner.equalsIgnoreCase(secondOwner)) && !(secondOwner.equalsIgnoreCase(thirdOwner))
							&& !(firstOwner.equalsIgnoreCase(thirdOwner))) {
						actualOner = firstOwner + " / " + secondOwner + " / " + thirdOwner;
						fc.utobj().printTestStep("Assign Lead Owner by Sales Territories  is working as excpeted");
					} else {
						System.out.println("Fails");
						fc.utobj().throwsException(
								"Assign Lead Owner by Sales Territories  is  not working as expcected");
					}

				} else if ("Import".equals(fromWhere)) {
					testCaseIdInternal = "TC_FC_QA_Sale_Lead_Primary_Owner_Assignment";
					dataSet = fc.utobj().readTestData("sales", testCaseIdInternal);
					FSSearchPageTest p3 = new FSSearchPageTest();
					fc.utobj().printTestStep("Adding lead from Web Form");
					Map<String, String> leadInfo = new HashMap<String, String>();
					String firstName = fc.utobj().generateTestData("Wk");
					String lastName = fc.utobj().generateTestData("Wk");
					String formName = fc.utobj().generateTestData("FORM");
					leadInfo.put("firstName", firstName);
					leadInfo.put("lastName", lastName);
					leadInfo.put("country", dataSet.get("country"));
					// leadInfo.put("state", dataSet.get("stateID"));
					leadInfo.put("email", dataSet.get("emailID"));
					// leadInfo.put("leadOwner", "FranConnect Administrator");
					leadInfo.put("leadSourceCategory", dataSet.get("leadSource2ID"));
					leadInfo.put("leadSourceDetails", dataSet.get("leadSource3ID"));
					leadInfo.put("zipCode", zipCode);
					leadInfo.put("state", state);
					leadInfo.put("owner", "Based on Assignment Rules");
					String fileName = "OneLeadImportFile.csv";
					fc.utobj().printTestStep("Adding Lead from Import");
					fileName = fc.utobj().getFilePathFromTestData(fileName);
					try {
						addLeadFromAllSourecesObj.addLeadFromImport(driver, fileName, leadInfo);
					} catch (Exception e) {
						System.out.println("==Sales teriotries ==" + e.getMessage());
					}

					// addLeadFromAllSourecesObj.addLeadFromWebForm(driver,
					// formName, leadInfo,"Confirmation Message");
					p3.searchByLeadName(driver, firstName, lastName);
					fc.utobj().clickElement(driver, driver
							.findElement(By.xpath(".//*[contains(text(),'" + firstName + " " + lastName + "')]")));

					fc.utobj().printTestStep("Verify a lead and Go to get Lead Owner");
					WebElement ownerelement = driver
							.findElement(By.xpath(".//tr/td[contains(text(),'Lead Owner :')]/following-sibling::td"));
					firstOwner = ownerelement.getText();

					testCaseIdInternal = "TC_FC_QA_Sale_Lead_Primary_Owner_Assignment";
					dataSet = fc.utobj().readTestData("sales", testCaseIdInternal);
					fc.utobj().printTestStep("Adding lead from Web Form");
					leadInfo = new HashMap<String, String>();
					firstName = fc.utobj().generateTestData("Wk");
					lastName = fc.utobj().generateTestData("Wk");
					formName = fc.utobj().generateTestData("FORM");
					leadInfo.put("firstName", firstName);
					leadInfo.put("lastName", lastName);
					leadInfo.put("country", dataSet.get("country"));
					// leadInfo.put("state", dataSet.get("stateID"));
					leadInfo.put("email", dataSet.get("emailID"));
					// leadInfo.put("leadOwner", "FranConnect Administrator");
					leadInfo.put("leadSourceCategory", dataSet.get("leadSource2ID"));
					leadInfo.put("leadSourceDetails", dataSet.get("leadSource3ID"));
					leadInfo.put("zipCode", zipCode);
					leadInfo.put("state", state);
					leadInfo.put("owner", "Based on Assignment Rules");
					addLeadFromAllSourecesObj.addLeadFromImport(driver, fileName, leadInfo);
					// addLeadFromAllSourecesObj.addLeadFromWebForm(driver,
					// formName, leadInfo,"Confirmation Message");
					p3.searchByLeadName(driver, firstName, lastName);
					fc.utobj().clickElement(driver, driver
							.findElement(By.xpath(".//*[contains(text(),'" + firstName + " " + lastName + "')]")));

					fc.utobj().printTestStep("Verify a lead and Go to get Lead Owner");
					ownerelement = driver
							.findElement(By.xpath(".//tr/td[contains(text(),'Lead Owner :')]/following-sibling::td"));
					secondOwner = ownerelement.getText();

					testCaseIdInternal = "TC_FC_QA_Sale_Lead_Primary_Owner_Assignment";
					dataSet = fc.utobj().readTestData("sales", testCaseIdInternal);
					fc.utobj().printTestStep("Adding lead from Web Form");
					leadInfo = new HashMap<String, String>();
					firstName = fc.utobj().generateTestData("Wk");
					lastName = fc.utobj().generateTestData("Wk");
					formName = fc.utobj().generateTestData("FORM");
					leadInfo.put("firstName", firstName);
					leadInfo.put("lastName", lastName);
					leadInfo.put("country", dataSet.get("country"));
					leadInfo.put("state", dataSet.get("stateID"));
					leadInfo.put("email", dataSet.get("emailID"));
					// leadInfo.put("leadOwner", "FranConnect Administrator");
					leadInfo.put("leadSourceCategory", dataSet.get("leadSource2ID"));
					leadInfo.put("leadSourceDetails", dataSet.get("leadSource3ID"));
					leadInfo.put("zipCode", zipCode);
					leadInfo.put("state", state);
					// addLeadFromAllSourecesObj.addLeadFromWebForm(driver,
					// formName, leadInfo,"Confirmation Message");
					leadInfo.put("owner", "Based on Assignment Rules");
					addLeadFromAllSourecesObj.addLeadFromImport(driver, fileName, leadInfo);
					p3.searchByLeadName(driver, firstName, lastName);
					fc.utobj().clickElement(driver, driver
							.findElement(By.xpath(".//*[contains(text(),'" + firstName + " " + lastName + "')]")));

					fc.utobj().printTestStep("Verify a lead and Go to get Lead Owner");
					ownerelement = driver
							.findElement(By.xpath(".//tr/td[contains(text(),'Lead Owner :')]/following-sibling::td"));
					thirdOwner = ownerelement.getText();

					System.out.println(firstOwner + "====" + secondOwner + "====" + thirdOwner);

					if (!(firstOwner.equalsIgnoreCase(secondOwner)) && !(secondOwner.equalsIgnoreCase(thirdOwner))
							&& !(firstOwner.equalsIgnoreCase(thirdOwner))) {
						actualOner = firstOwner + " / " + secondOwner + " / " + thirdOwner;
						fc.utobj().printTestStep("Assign Lead Owner by Sales Territories  is working as excpeted");
					} else {
						System.out.println("Fails");
						fc.utobj().throwsException(
								"Assign Lead Owner by Sales Territories  is  not working as expcected");
					}

				} else if ("RestAPI".equals(fromWhere)) {
					FSSearchPageTest p3 = new FSSearchPageTest();
					testCaseIdInternal = "TC_Sales_SmartGroup_002";
					dataSet = fc.utobj().readTestData("sales", testCaseIdInternal);
					String firstName = fc.utobj().generateTestData("Re");
					String lastName = fc.utobj().generateTestData("Re");
					country = "USA";
					// String state=dataSet.get("state");
					String leadOwner = "FranConnect Administrator";
					String leadSourceCategory = "Friends";
					String leadSourceDetails = "Friends";
					Map<String, String> UniqueKey_PrimaryInfo = new HashMap<String, String>();
					UniqueKey_PrimaryInfo.put("firstName", firstName);
					UniqueKey_PrimaryInfo.put("lastName", lastName);
					UniqueKey_PrimaryInfo.put("leadSource2ID", leadSourceCategory);
					UniqueKey_PrimaryInfo.put("leadSource3ID", leadSourceDetails);
					UniqueKey_PrimaryInfo.put("emailID", "harish@test.net");
					UniqueKey_PrimaryInfo.put("country", country);
					UniqueKey_PrimaryInfo.put("stateID", state);
					UniqueKey_PrimaryInfo.put("zip", zipCode);
					fc.utobj().printTestStep("Adding lead from Rest API");
					addLeadFromAllSourecesObj.addleadFromRestApi(driver, dataSet, testCaseId, UniqueKey_PrimaryInfo);

					driver.navigate().to(FranconnectUtil.config.get("buildUrl"));
					fc.loginpage().login(driver);
					fc.sales().sales_common().fsModule(driver);
					p3.searchByLeadName(driver, firstName, lastName);

					fc.utobj().clickElement(driver, driver
							.findElement(By.xpath(".//*[contains(text(),'" + firstName + " " + lastName + "')]")));

					fc.utobj().printTestStep("Verify a lead and Go to get Lead Owner");
					WebElement ownerelement = driver
							.findElement(By.xpath(".//tr/td[contains(text(),'Lead Owner :')]/following-sibling::td"));
					firstOwner = ownerelement.getText();

					testCaseIdInternal = "TC_Sales_SmartGroup_002";
					dataSet = fc.utobj().readTestData("sales", testCaseIdInternal);
					firstName = fc.utobj().generateTestData("Re");
					lastName = fc.utobj().generateTestData("Re");
					country = "USA";
					// String state=dataSet.get("state");
					leadOwner = "FranConnect Administrator";
					leadSourceCategory = "Friends";
					leadSourceDetails = "Friends";
					UniqueKey_PrimaryInfo = new HashMap<String, String>();
					UniqueKey_PrimaryInfo.put("firstName", firstName);
					UniqueKey_PrimaryInfo.put("lastName", lastName);
					UniqueKey_PrimaryInfo.put("leadSource2ID", leadSourceCategory);
					UniqueKey_PrimaryInfo.put("leadSource3ID", leadSourceDetails);
					UniqueKey_PrimaryInfo.put("emailID", "harish@test.net");
					UniqueKey_PrimaryInfo.put("country", country);
					UniqueKey_PrimaryInfo.put("stateID", state);
					UniqueKey_PrimaryInfo.put("zip", zipCode);
					fc.utobj().printTestStep("Adding lead from Rest API");
					addLeadFromAllSourecesObj.addleadFromRestApi(driver, dataSet, testCaseId, UniqueKey_PrimaryInfo);

					driver.navigate().to(FranconnectUtil.config.get("buildUrl"));
					fc.loginpage().login(driver);
					fc.sales().sales_common().fsModule(driver);
					p3.searchByLeadName(driver, firstName, lastName);

					fc.utobj().clickElement(driver, driver
							.findElement(By.xpath(".//*[contains(text(),'" + firstName + " " + lastName + "')]")));
					fc.utobj().printTestStep("Verify a lead and Go to get Lead Owner");
					ownerelement = driver
							.findElement(By.xpath(".//tr/td[contains(text(),'Lead Owner :')]/following-sibling::td"));
					secondOwner = ownerelement.getText();

					testCaseIdInternal = "TC_Sales_SmartGroup_002";
					dataSet = fc.utobj().readTestData("sales", testCaseIdInternal);
					firstName = fc.utobj().generateTestData("Re");
					lastName = fc.utobj().generateTestData("Re");
					country = "USA";
					// String state=dataSet.get("state");
					leadOwner = "FranConnect Administrator";
					leadSourceCategory = "Friends";
					leadSourceDetails = "Friends";
					UniqueKey_PrimaryInfo = new HashMap<String, String>();
					UniqueKey_PrimaryInfo.put("firstName", firstName);
					UniqueKey_PrimaryInfo.put("lastName", lastName);
					UniqueKey_PrimaryInfo.put("leadSource2ID", leadSourceCategory);
					UniqueKey_PrimaryInfo.put("leadSource3ID", leadSourceDetails);
					UniqueKey_PrimaryInfo.put("emailID", "harish@test.net");
					UniqueKey_PrimaryInfo.put("country", country);
					UniqueKey_PrimaryInfo.put("stateID", state);
					UniqueKey_PrimaryInfo.put("zip", zipCode);
					fc.utobj().printTestStep("Adding lead from Rest API");
					addLeadFromAllSourecesObj.addleadFromRestApi(driver, dataSet, testCaseId, UniqueKey_PrimaryInfo);

					driver.navigate().to(FranconnectUtil.config.get("buildUrl"));
					fc.loginpage().login(driver);
					fc.sales().sales_common().fsModule(driver);
					p3.searchByLeadName(driver, firstName, lastName);

					fc.utobj().clickElement(driver, driver
							.findElement(By.xpath(".//*[contains(text(),'" + firstName + " " + lastName + "')]")));

					fc.utobj().printTestStep("Verify a lead and Go to get Lead Owner");
					ownerelement = driver
							.findElement(By.xpath(".//tr/td[contains(text(),'Lead Owner :')]/following-sibling::td"));
					thirdOwner = ownerelement.getText();

					System.out.println(firstOwner + "====" + secondOwner + "====" + thirdOwner);

					if (!(firstOwner.equalsIgnoreCase(secondOwner)) && !(secondOwner.equalsIgnoreCase(thirdOwner))
							&& !(firstOwner.equalsIgnoreCase(thirdOwner))) {
						actualOner = firstOwner + " / " + secondOwner + " / " + thirdOwner;
						fc.utobj().printTestStep("Assign Lead Owner by Sales Territories  is working as excpeted");
					} else {
						System.out.println("Fails");
						fc.utobj().throwsException(
								"Assign Lead Owner by Sales Territories  is  not working as expcected");
					}
				}
			}

		} else {
			state = "Florida";
			// In case when round robin is set but no user set for owner should
			// goes to default owner
			fc.adminpage().adminPage(driver);
			// adsales.assignLeadOwners(driver);

			String userNameOwner = "FranConnect Administrator";
			fc.utobj().printTestStep("Create user to Assign Owner");
			String userNameRC = fc.utobj().generateTestData("IZ"); // "AssignOwner";
			userNameOwner = userNameRC + " " + userNameRC;
			String emailId = "salesautomation@staffex.com";
			if ("CorporateUser".equals(UserType)) {
				CorporateUser corpUser = new CorporateUser();
				corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
				corpUser.setEmail(emailId);
				corpUser = corpTest.createDefaultUser(driver, corpUser);
			} else if ("RegionalUser".equals(UserType)) {
				String regionName = fc.utobj().generateTestData("RG");
				regUser.addRegionalUser(driver, userNameRC, regionName, emailId);
				userNameOwner = userNameRC + " " + userNameRC + " (RU)";
			}
			// userNameOwner = userNameRC+" "+userNameRC;
			fc.utobj().printTestStep("Verify the Create user");
			fc.adminpage().adminPage(driver);
			adsales.assignLeadOwners(driver);

			fc.utobj().printTestStep("Verify the user is available in Default Owner drop down");

			boolean isLeadOwnerPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//select[@name='area1']/option[contains(text(),'" + userNameOwner + "')]");
			if (isLeadOwnerPresent == false) {
				fc.utobj().throwsException("User not present in Default Owner dropdown");
			}

			if (noownerset) {
				fc.utobj().selectDropDown(driver, pobj.defaultOwnerDrpDown, "Select");
				userNameOwner = "FranConnect Administrator";
			} else {
				fc.utobj().selectDropDown(driver, pobj.defaultOwnerDrpDown, userNameOwner);
			}

			fc.utobj().clickElement(driver, pobj.updateLeadOwnerBtn);

			if ("SystemLead".equals(fromWhere)) {
				fc.sales().sales_common().fsModule(driver);
				Sales fs = new Sales();
				fs.leadManagement(driver);
				FSLeadSummaryPageTest fsPage = new FSLeadSummaryPageTest();
				fsPage.clickAddLeadLink(driver);
				fc.utobj().printTestStep("Add a lead and Go to Primary Info");
				testCaseIdInternal = "TC_FC_QA_Sale_Lead_Primary_Owner_Assignment";
				dataSet = fc.utobj().readTestData("sales", testCaseIdInternal);
				String firstName = fc.utobj().generateTestData("AK");
				String lastName = fc.utobj().generateTestData("BK");
				dataSet.put("firstName", firstName);
				dataSet.put("lastName", lastName);
				dataSet.put("automatic", "As per Assignment Scheme");
				dataSet.put("zip", zipCode);
				dataSet.put("stateID", state);

				printInfo = addLeadFromAllSourecesObj.fillFormDataWithDataset(driver, testCaseIdInternal, dataSet);
				fc.utobj().printTestStep("Verify a lead and Go to get Lead Owner");
				if (printInfo != null && printInfo.size() > 0) {
					isLeadOwnerPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
							".//tr/td[contains(text(),'Lead Owner :')]/following-sibling::td[contains (text() ,'"
									+ userNameOwner + "')]");
					if (isLeadOwnerPresent == false) {
						fc.utobj().throwsException("As per Assignment Scheme owner is not assigned");
					}

				}
			} else if ("WebForm".equals(fromWhere)) {
				testCaseIdInternal = "TC_FC_QA_Sale_Lead_Primary_Owner_Assignment";
				dataSet = fc.utobj().readTestData("sales", testCaseIdInternal);
				FSSearchPageTest p3 = new FSSearchPageTest();
				fc.utobj().printTestStep("Adding lead from Web Form");
				Map<String, String> leadInfo = new HashMap<String, String>();
				String firstName = fc.utobj().generateTestData("Wk");
				String lastName = fc.utobj().generateTestData("Wk");
				String formName = fc.utobj().generateTestData("FORM");
				leadInfo.put("firstName", firstName);
				leadInfo.put("lastName", lastName);
				leadInfo.put("country", dataSet.get("country"));
				// leadInfo.put("state", dataSet.get("stateID"));
				leadInfo.put("state", state);

				leadInfo.put("email", dataSet.get("emailID"));
				leadInfo.put("leadOwner", userNameOwner);
				leadInfo.put("leadSourceCategory", dataSet.get("leadSource2ID"));
				leadInfo.put("leadSourceDetails", dataSet.get("leadSource3ID"));
				leadInfo.put("zipCode", zipCode);
				addLeadFromAllSourecesObj.addLeadFromWebForm(driver, formName, leadInfo, "Confirmation Message");
				p3.searchByLeadName(driver, firstName, lastName);
				fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
						".//*[contains(text(),'" + firstName + " " + lastName + "')]"));

				fc.utobj().printTestStep("Verify a lead and Go to get Lead Owner");
				isLeadOwnerPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
						".//tr/td[contains(text(),'Lead Owner :')]/following-sibling::td[contains (text() ,'"
								+ userNameOwner + "')]");
				if (isLeadOwnerPresent == false) {
					fc.utobj().throwsException("As per Assignment Scheme owner is not assigned");
				}

			} else if ("Import".equals(fromWhere)) {
				testCaseIdInternal = "TC_FC_QA_Sale_Lead_Primary_Owner_Assignment";
				dataSet = fc.utobj().readTestData("sales", testCaseIdInternal);
				FSSearchPageTest p3 = new FSSearchPageTest();
				fc.utobj().printTestStep("Adding lead from Web Form");
				Map<String, String> leadInfo = new HashMap<String, String>();
				String firstName = fc.utobj().generateTestData("Wk");
				String lastName = fc.utobj().generateTestData("Wk");
				String formName = fc.utobj().generateTestData("FORM");
				leadInfo.put("firstName", firstName);
				leadInfo.put("lastName", lastName);
				leadInfo.put("country", dataSet.get("country"));
				// leadInfo.put("state", dataSet.get("stateID"));
				leadInfo.put("state", state);

				leadInfo.put("email", dataSet.get("emailID"));
				leadInfo.put("leadOwner", userNameOwner);
				leadInfo.put("leadSourceCategory", dataSet.get("leadSource2ID"));
				leadInfo.put("leadSourceDetails", dataSet.get("leadSource3ID"));
				leadInfo.put("zipCode", zipCode);

				leadInfo.put("owner", "Based on Assignment Rules");
				String fileName = "OneLeadImportFile.csv";
				fc.utobj().printTestStep("Adding Lead from Import");
				fileName = fc.utobj().getFilePathFromTestData(fileName);

				try {
					addLeadFromAllSourecesObj.addLeadFromImport(driver, fileName, leadInfo);
				} catch (Exception e) {

				}

				// addLeadFromAllSourecesObj.addLeadFromWebForm(driver,
				// formName, leadInfo,"Confirmation Message");
				p3.searchByLeadName(driver, firstName, lastName);
				fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
						".//*[contains(text(),'" + firstName + " " + lastName + "')]"));

				fc.utobj().printTestStep("Verify a lead and Go to get Lead Owner");
				isLeadOwnerPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
						".//tr/td[contains(text(),'Lead Owner :')]/following-sibling::td[contains (text() ,'"
								+ userNameOwner + "')]");
				if (isLeadOwnerPresent == false) {
					fc.utobj().throwsException("As per Assignment Scheme owner is not assigned");
				}

			} else if ("RestAPI".equals(fromWhere)) {
				FSSearchPageTest p3 = new FSSearchPageTest();
				testCaseIdInternal = "TC_Sales_SmartGroup_002";
				dataSet = fc.utobj().readTestData("sales", testCaseIdInternal);
				String firstName = fc.utobj().generateTestData("Re");
				String lastName = fc.utobj().generateTestData("Re");
				country = "USA";
				// String state=dataSet.get("state");
				String leadSourceCategory = "Friends";
				String leadSourceDetails = "Friends";
				Map<String, String> UniqueKey_PrimaryInfo = new HashMap<String, String>();
				UniqueKey_PrimaryInfo.put("firstName", firstName);
				UniqueKey_PrimaryInfo.put("lastName", lastName);
				UniqueKey_PrimaryInfo.put("leadSource2ID", leadSourceCategory);
				UniqueKey_PrimaryInfo.put("leadSource3ID", leadSourceDetails);
				UniqueKey_PrimaryInfo.put("emailID", emailId);
				UniqueKey_PrimaryInfo.put("country", country);
				UniqueKey_PrimaryInfo.put("stateID", state);
				UniqueKey_PrimaryInfo.put("zip", zipCode);
				fc.utobj().printTestStep("Adding lead from Rest API");
				addLeadFromAllSourecesObj.addleadFromRestApi(driver, dataSet, testCaseId, UniqueKey_PrimaryInfo);

				driver.navigate().to(FranconnectUtil.config.get("buildUrl"));
				fc.loginpage().login(driver);
				fc.sales().sales_common().fsModule(driver);
				p3.searchByLeadName(driver, firstName, lastName);

				fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
						".//*[contains(text(),'" + firstName + " " + lastName + "')]"));

				fc.utobj().printTestStep("Verify a lead and Go to get Lead Owner");
				isLeadOwnerPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
						".//tr/td[contains(text(),'Lead Owner :')]/following-sibling::td[contains (text() ,'"
								+ userNameOwner + "')]");
				if (isLeadOwnerPresent == false) {
					fc.utobj().throwsException("As per Assignment Scheme owner is not assigned");
				}
			}
		}
		// End Here
		return actualOner;
	}

	public String getSalesTerritoriesOwnerZip(WebDriver driver, String testCaseId, String UserType, String fromWhere,
			boolean addDataforroundrobin, boolean noownerset) throws Exception {
		String actualOner = "";
		String testCaseIdInternal = "" + testCaseId;
		Map<String, String> dataSet = fc.utobj().readTestData("sales", testCaseId);
		Map<String, String> printInfo = null;
		AddLeadFromAllSources addLeadFromAllSourecesObj = new AddLeadFromAllSources();
		LeadSummaryUI pobj = new LeadSummaryUI(driver);
		AdminFranchiseSalesManageSalesTerritoryCreateSalesTerritoryPageTest p1 = new AdminFranchiseSalesManageSalesTerritoryCreateSalesTerritoryPageTest();
		AdminUsersManageCorporateUsersAddCorporateUserPageTest corpTest = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
		AdminUsersManageRegionalUsersAddRegionalUserPageTest regUser = new AdminUsersManageRegionalUsersAddRegionalUserPageTest();
		String firstOwner = "";
		String secondOwner = "";
		String thirdOwner = "";
		List<String> listInfo = new ArrayList<String>();
		// Sales Territories

		String salesTerritory = fc.utobj().generateTestData("ST");
		String zipCode = fc.utobj().generateRandomNumber();
		fc.utobj().printTestStep("Go to Admin Sales territory section \nAdd Sales Territory");
		salesTerritory = p1.addSalesTerritoryRandomZip(driver, salesTerritory, zipCode, "Domestic");

		// End Sales Territories

		AdminSales adsales = new AdminSales();
		adsales.assignLeadOwners(driver);
		List<WebElement> oRadioButton = driver.findElements(By.name("radiobutton"));
		boolean bValue = false;
		bValue = oRadioButton.get(1).isSelected();
		if (oRadioButton.get(1).isSelected()) {

		} else {
			oRadioButton.get(1).click();
		}

		fc.utobj().printTestStep("Click Continue");
		fc.utobj().clickElement(driver, pobj.continueButton);

		System.out.println("salesTerritory=====" + salesTerritory);

		if (addDataforroundrobin) {

			if ((salesTerritory != null)) {
				for (int i = 0; i < 3; i++) {
					fc.adminpage().adminPage(driver);
					// adsales.assignLeadOwners(driver);

					String userNameOwner = "FranConnect Administrator";
					fc.utobj().printTestStep("Verify the Create user");
					String userNameRC = fc.utobj().generateTestData("IZ"); // "AssignOwner";

					String emailId = "salesautomation@staffex.com";
					if ("CorporateUser".equals(UserType)) {
						CorporateUser corpUser = new CorporateUser();
						corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
						corpUser.setEmail(emailId);
						corpUser = corpTest.createDefaultUser(driver, corpUser);
					} else if ("RegionalUser".equals(UserType)) {
						String regionName = fc.utobj().generateTestData("RG");
						regUser.addRegionalUser(driver, userNameRC, regionName, emailId);
					}
					userNameOwner = userNameRC + " " + userNameRC;
					listInfo.add(userNameOwner);
					fc.utobj().printTestStep("Verify the Create user");
					fc.adminpage().adminPage(driver);

					adsales.assignLeadOwners(driver);

					fc.utobj().printTestStep("Verify the user is available in Default Owner drop down");

					boolean isLeadOwnerPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
							".//select[@name='area1']/option[contains(text(),'" + userNameOwner + "')]");
					if (isLeadOwnerPresent == false) {
						fc.utobj().throwsException("User not present in Default Owner dropdown");
					}

					oRadioButton = driver.findElements(By.name("radiobutton"));
					bValue = false;
					bValue = oRadioButton.get(1).isSelected();
					if (oRadioButton.get(1).isSelected()) {

					} else {
						oRadioButton.get(1).click();
					}

					fc.utobj().printTestStep("Click Continue");
					fc.utobj().clickElement(driver, pobj.continueButton);

					boolean availableLeadOwner = fc.utobj().verifyElementOnVisible_ByXpath(driver,
							".//tr/td[contains(text(),'" + salesTerritory
									+ "')]/following-sibling::td/select/option[contains(text(),'" + userNameOwner
									+ "')]");
					if (availableLeadOwner == false) {
						fc.utobj().throwsException("User is  not available in Sales Terriotries Lead Owners drop-down");
					}

				}

				fc.utobj().printTestStep("fill the owners with respect to sale terriortires drop down");
				fc.adminpage().adminPage(driver);

				adsales.assignLeadOwners(driver);

				oRadioButton = driver.findElements(By.name("radiobutton"));
				bValue = false;
				bValue = oRadioButton.get(1).isSelected();
				if (oRadioButton.get(1).isSelected()) {

				} else {
					oRadioButton.get(1).click();
				}

				fc.utobj().printTestStep("Click Continue");
				fc.utobj().clickElement(driver, pobj.continueButton);

				fc.utobj()
						.selectMultipleValFromMultiSelect(driver,
								fc.utobj()
										.getElementByXpath(driver,
												".//tr/td[contains(text(),'" + salesTerritory
														+ "')]/following-sibling::td/div[@class='ms-parent']"),
								listInfo);

				fc.utobj().clickElement(driver, pobj.updateLeadOwnerBtn);

				boolean configredLeadOwner = fc.utobj().verifyElementOnVisible_ByXpath(driver, ".//tr/td[contains(text(),'" + salesTerritory
						+ "')]/following-sibling::td[contains(text(),'" + listInfo.get(0) + "')]");
				if (configredLeadOwner == false) {
					fc.utobj().throwsException("User is  not available in  Configured Lead Owners");
				}

			}

			// allroundrobin=
			// driver.findElements(By.xpath(".//*[@id='templateOrder']/option"));

			// Start here to check Lead

			if (salesTerritory != null) {

				if ("SystemLead".equals(fromWhere)) {
					// First Lead
					fc.sales().sales_common().fsModule(driver);
					Sales fs = new Sales();
					fs.leadManagement(driver);
					FSLeadSummaryPageTest fsPage = new FSLeadSummaryPageTest();
					fsPage.clickAddLeadLink(driver);
					fc.utobj().printTestStep("Add a lead and Go to Primary Info");
					testCaseIdInternal = "TC_FC_QA_Sale_Lead_Primary_Owner_Assignment";
					dataSet = fc.utobj().readTestData("sales", testCaseIdInternal);
					String firstName = fc.utobj().generateTestData("AK");
					String lastName = fc.utobj().generateTestData("BK");
					dataSet.put("firstName", firstName);
					dataSet.put("lastName", lastName);
					dataSet.put("automatic", "As per Assignment Scheme");
					dataSet.put("zip", zipCode);
					printInfo = addLeadFromAllSourecesObj.fillFormDataWithDataset(driver, testCaseIdInternal, dataSet);

					if (printInfo != null && printInfo.size() > 0) {
						fc.utobj().printTestStep("Verify a lead and Go to get Lead Owner");
						// .//tr/td[contains(text(),'Lead Owner
						// :')]/following-sibling::td[contains (text()
						// ,'"+userNameOwner+"')]
						WebElement ownerelement = driver.findElement(
								By.xpath(".//tr/td[contains(text(),'Lead Owner :')]/following-sibling::td"));
						firstOwner = ownerelement.getText();

					}

					// End First Lead
					// Enter Second Lead
					fc.sales().sales_common().fsModule(driver);
					fs = new Sales();
					fs.leadManagement(driver);
					fsPage.clickAddLeadLink(driver);
					fc.utobj().printTestStep("Add a lead and Go to Primary Info");
					testCaseIdInternal = "TC_FC_QA_Sale_Lead_Primary_Owner_Assignment";
					dataSet = fc.utobj().readTestData("sales", testCaseIdInternal);
					firstName = fc.utobj().generateTestData("AK");
					lastName = fc.utobj().generateTestData("BK");
					dataSet.put("firstName", firstName);
					dataSet.put("lastName", lastName);
					dataSet.put("automatic", "As per Assignment Scheme");
					dataSet.put("zip", zipCode);
					printInfo = addLeadFromAllSourecesObj.fillFormDataWithDataset(driver, testCaseIdInternal, dataSet);

					if (printInfo != null && printInfo.size() > 0) {
						fc.utobj().printTestStep("Verify a lead and Go to get Lead Owner");
						// .//tr/td[contains(text(),'Lead Owner
						// :')]/following-sibling::td[contains (text()
						// ,'"+userNameOwner+"')]
						WebElement ownerelement = driver.findElement(
								By.xpath(".//tr/td[contains(text(),'Lead Owner :')]/following-sibling::td"));
						secondOwner = ownerelement.getText();

					}
					// End Second Lead
					// Enter Third Lead
					fc.sales().sales_common().fsModule(driver);
					fs = new Sales();
					fs.leadManagement(driver);
					fsPage.clickAddLeadLink(driver);
					fc.utobj().printTestStep("Add a lead and Go to Primary Info");
					testCaseIdInternal = "TC_FC_QA_Sale_Lead_Primary_Owner_Assignment";
					dataSet = fc.utobj().readTestData("sales", testCaseIdInternal);
					firstName = fc.utobj().generateTestData("AK");
					lastName = fc.utobj().generateTestData("BK");
					dataSet.put("firstName", firstName);
					dataSet.put("lastName", lastName);
					dataSet.put("automatic", "As per Assignment Scheme");
					dataSet.put("zip", zipCode);
					printInfo = addLeadFromAllSourecesObj.fillFormDataWithDataset(driver, testCaseIdInternal, dataSet);

					if (printInfo != null && printInfo.size() > 0) {
						fc.utobj().printTestStep("Verify a lead and Go to get Lead Owner");
						// .//tr/td[contains(text(),'Lead Owner
						// :')]/following-sibling::td[contains (text()
						// ,'"+userNameOwner+"')]
						WebElement ownerelement = driver.findElement(
								By.xpath(".//tr/td[contains(text(),'Lead Owner :')]/following-sibling::td"));
						thirdOwner = ownerelement.getText();

					}
					// End Second Lead

					System.out.println(firstOwner + "====" + secondOwner + "====" + thirdOwner);

					if (!(firstOwner.equalsIgnoreCase(secondOwner)) && !(secondOwner.equalsIgnoreCase(thirdOwner))
							&& !(firstOwner.equalsIgnoreCase(thirdOwner))) {
						actualOner = firstOwner + " / " + secondOwner + " / " + thirdOwner;
						fc.utobj().printTestStep(
								"Assign Lead Owner by Sales Territories via ZIP  is working as excpeted");
					} else {
						System.out.println("Fails");
						fc.utobj().throwsException(
								"Assign Lead Owner by Sales Territories via ZIP  is  not working as expcected");
					}

				} // End system Lead method
				else if ("WebForm".equals(fromWhere)) {
					testCaseIdInternal = "TC_FC_QA_Sale_Lead_Primary_Owner_Assignment";
					dataSet = fc.utobj().readTestData("sales", testCaseIdInternal);
					FSSearchPageTest p3 = new FSSearchPageTest();
					fc.utobj().printTestStep("Adding lead from Web Form");
					Map<String, String> leadInfo = new HashMap<String, String>();
					String firstName = fc.utobj().generateTestData("Wk");
					String lastName = fc.utobj().generateTestData("Wk");
					String formName = fc.utobj().generateTestData("FORM");
					leadInfo.put("firstName", firstName);
					leadInfo.put("lastName", lastName);
					leadInfo.put("country", dataSet.get("country"));
					leadInfo.put("state", dataSet.get("stateID"));
					leadInfo.put("email", dataSet.get("emailID"));
					// leadInfo.put("leadOwner", "FranConnect Administrator");
					leadInfo.put("leadSourceCategory", dataSet.get("leadSource2ID"));
					leadInfo.put("leadSourceDetails", dataSet.get("leadSource3ID"));
					leadInfo.put("zipCode", zipCode);
					addLeadFromAllSourecesObj.addLeadFromWebForm(driver, formName, leadInfo, "Confirmation Message");
					p3.searchByLeadName(driver, firstName, lastName);
					fc.utobj().clickElement(driver, driver
							.findElement(By.xpath(".//*[contains(text(),'" + firstName + " " + lastName + "')]")));

					fc.utobj().printTestStep("Verify a lead and Go to get Lead Owner");
					WebElement ownerelement = driver
							.findElement(By.xpath(".//tr/td[contains(text(),'Lead Owner :')]/following-sibling::td"));
					firstOwner = ownerelement.getText();

					testCaseIdInternal = "TC_FC_QA_Sale_Lead_Primary_Owner_Assignment";
					dataSet = fc.utobj().readTestData("sales", testCaseIdInternal);
					fc.utobj().printTestStep("Adding lead from Web Form");
					leadInfo = new HashMap<String, String>();
					firstName = fc.utobj().generateTestData("Wk");
					lastName = fc.utobj().generateTestData("Wk");
					formName = fc.utobj().generateTestData("FORM");
					leadInfo.put("firstName", firstName);
					leadInfo.put("lastName", lastName);
					leadInfo.put("country", dataSet.get("country"));
					leadInfo.put("state", dataSet.get("stateID"));
					leadInfo.put("email", dataSet.get("emailID"));
					// leadInfo.put("leadOwner", "FranConnect Administrator");
					leadInfo.put("leadSourceCategory", dataSet.get("leadSource2ID"));
					leadInfo.put("leadSourceDetails", dataSet.get("leadSource3ID"));
					leadInfo.put("zipCode", zipCode);
					addLeadFromAllSourecesObj.addLeadFromWebForm(driver, formName, leadInfo, "Confirmation Message");
					p3.searchByLeadName(driver, firstName, lastName);
					fc.utobj().clickElement(driver, driver
							.findElement(By.xpath(".//*[contains(text(),'" + firstName + " " + lastName + "')]")));

					fc.utobj().printTestStep("Verify a lead and Go to get Lead Owner");
					ownerelement = driver
							.findElement(By.xpath(".//tr/td[contains(text(),'Lead Owner :')]/following-sibling::td"));
					secondOwner = ownerelement.getText();

					testCaseIdInternal = "TC_FC_QA_Sale_Lead_Primary_Owner_Assignment";
					dataSet = fc.utobj().readTestData("sales", testCaseIdInternal);
					fc.utobj().printTestStep("Adding lead from Web Form");
					leadInfo = new HashMap<String, String>();
					firstName = fc.utobj().generateTestData("Wk");
					lastName = fc.utobj().generateTestData("Wk");
					formName = fc.utobj().generateTestData("FORM");
					leadInfo.put("firstName", firstName);
					leadInfo.put("lastName", lastName);
					leadInfo.put("country", dataSet.get("country"));
					leadInfo.put("state", dataSet.get("stateID"));
					leadInfo.put("email", dataSet.get("emailID"));
					// leadInfo.put("leadOwner", "FranConnect Administrator");
					leadInfo.put("leadSourceCategory", dataSet.get("leadSource2ID"));
					leadInfo.put("leadSourceDetails", dataSet.get("leadSource3ID"));
					leadInfo.put("zipCode", zipCode);
					addLeadFromAllSourecesObj.addLeadFromWebForm(driver, formName, leadInfo, "Confirmation Message");
					p3.searchByLeadName(driver, firstName, lastName);
					fc.utobj().clickElement(driver, driver
							.findElement(By.xpath(".//*[contains(text(),'" + firstName + " " + lastName + "')]")));

					ownerelement = driver
							.findElement(By.xpath(".//tr/td[contains(text(),'Lead Owner :')]/following-sibling::td"));
					thirdOwner = ownerelement.getText();

					System.out.println(firstOwner + "====" + secondOwner + "====" + thirdOwner);

					if (!(firstOwner.equalsIgnoreCase(secondOwner)) && !(secondOwner.equalsIgnoreCase(thirdOwner))
							&& !(firstOwner.equalsIgnoreCase(thirdOwner))) {
						actualOner = firstOwner + " / " + secondOwner + " / " + thirdOwner;
						fc.utobj().printTestStep(
								"Assign Lead Owner by Sales Territories via ZIP  is working as excpeted");
					} else {
						System.out.println("Fails");
						fc.utobj().throwsException(
								"Assign Lead Owner by Sales Territories via ZIP is  not working as expcected");
					}

				} else if ("Import".equals(fromWhere)) {
					testCaseIdInternal = "TC_FC_QA_Sale_Lead_Primary_Owner_Assignment";
					dataSet = fc.utobj().readTestData("sales", testCaseIdInternal);
					FSSearchPageTest p3 = new FSSearchPageTest();
					fc.utobj().printTestStep("Adding lead from Web Form");
					Map<String, String> leadInfo = new HashMap<String, String>();
					String firstName = fc.utobj().generateTestData("Wk");
					String lastName = fc.utobj().generateTestData("Wk");
					String formName = fc.utobj().generateTestData("FORM");
					leadInfo.put("firstName", firstName);
					leadInfo.put("lastName", lastName);
					leadInfo.put("country", dataSet.get("country"));
					leadInfo.put("state", dataSet.get("stateID"));
					leadInfo.put("email", dataSet.get("emailID"));
					// leadInfo.put("leadOwner", "FranConnect Administrator");
					leadInfo.put("leadSourceCategory", dataSet.get("leadSource2ID"));
					leadInfo.put("leadSourceDetails", dataSet.get("leadSource3ID"));
					leadInfo.put("zipCode", zipCode);
					leadInfo.put("owner", "Based on Assignment Rules");
					String fileName = "OneLeadImportFile.csv";
					fc.utobj().printTestStep("Adding Lead from Import");
					fileName = fc.utobj().getFilePathFromTestData(fileName);
					try {
						addLeadFromAllSourecesObj.addLeadFromImport(driver, fileName, leadInfo);
					} catch (Exception e) {
						System.out.println("=round robin===" + e.getMessage());
					}
					// addLeadFromAllSourecesObj.addLeadFromWebForm(driver,
					// formName, leadInfo,"Confirmation Message");
					p3.searchByLeadName(driver, firstName, lastName);
					fc.utobj().clickElement(driver, driver
							.findElement(By.xpath(".//*[contains(text(),'" + firstName + " " + lastName + "')]")));

					fc.utobj().printTestStep("Verify a lead and Go to get Lead Owner");
					WebElement ownerelement = driver
							.findElement(By.xpath(".//tr/td[contains(text(),'Lead Owner :')]/following-sibling::td"));
					firstOwner = ownerelement.getText();

					testCaseIdInternal = "TC_FC_QA_Sale_Lead_Primary_Owner_Assignment";
					dataSet = fc.utobj().readTestData("sales", testCaseIdInternal);
					fc.utobj().printTestStep("Adding lead from Web Form");
					leadInfo = new HashMap<String, String>();
					firstName = fc.utobj().generateTestData("Wk");
					lastName = fc.utobj().generateTestData("Wk");
					formName = fc.utobj().generateTestData("FORM");
					leadInfo.put("firstName", firstName);
					leadInfo.put("lastName", lastName);
					leadInfo.put("country", dataSet.get("country"));
					leadInfo.put("state", dataSet.get("stateID"));
					leadInfo.put("email", dataSet.get("emailID"));
					// leadInfo.put("leadOwner", "FranConnect Administrator");
					leadInfo.put("leadSourceCategory", dataSet.get("leadSource2ID"));
					leadInfo.put("leadSourceDetails", dataSet.get("leadSource3ID"));
					leadInfo.put("zipCode", zipCode);
					// addLeadFromAllSourecesObj.addLeadFromWebForm(driver,
					// formName, leadInfo,"Confirmation Message");
					leadInfo.put("owner", "Based on Assignment Rules");
					addLeadFromAllSourecesObj.addLeadFromImport(driver, fileName, leadInfo);

					p3.searchByLeadName(driver, firstName, lastName);
					fc.utobj().clickElement(driver, driver
							.findElement(By.xpath(".//*[contains(text(),'" + firstName + " " + lastName + "')]")));

					fc.utobj().printTestStep("Verify a lead and Go to get Lead Owner");
					ownerelement = driver
							.findElement(By.xpath(".//tr/td[contains(text(),'Lead Owner :')]/following-sibling::td"));
					secondOwner = ownerelement.getText();

					testCaseIdInternal = "TC_FC_QA_Sale_Lead_Primary_Owner_Assignment";
					dataSet = fc.utobj().readTestData("sales", testCaseIdInternal);
					fc.utobj().printTestStep("Adding lead from Web Form");
					leadInfo = new HashMap<String, String>();
					firstName = fc.utobj().generateTestData("Wk");
					lastName = fc.utobj().generateTestData("Wk");
					formName = fc.utobj().generateTestData("FORM");
					leadInfo.put("firstName", firstName);
					leadInfo.put("lastName", lastName);
					leadInfo.put("country", dataSet.get("country"));
					leadInfo.put("state", dataSet.get("stateID"));
					leadInfo.put("email", dataSet.get("emailID"));
					// leadInfo.put("leadOwner", "FranConnect Administrator");
					leadInfo.put("leadSourceCategory", dataSet.get("leadSource2ID"));
					leadInfo.put("leadSourceDetails", dataSet.get("leadSource3ID"));
					leadInfo.put("zipCode", zipCode);
					// addLeadFromAllSourecesObj.addLeadFromWebForm(driver,
					// formName, leadInfo,"Confirmation Message");
					leadInfo.put("owner", "Based on Assignment Rules");
					addLeadFromAllSourecesObj.addLeadFromImport(driver, fileName, leadInfo);

					p3.searchByLeadName(driver, firstName, lastName);
					fc.utobj().clickElement(driver, driver
							.findElement(By.xpath(".//*[contains(text(),'" + firstName + " " + lastName + "')]")));

					ownerelement = driver
							.findElement(By.xpath(".//tr/td[contains(text(),'Lead Owner :')]/following-sibling::td"));
					thirdOwner = ownerelement.getText();

					System.out.println(firstOwner + "====" + secondOwner + "====" + thirdOwner);

					if (!(firstOwner.equalsIgnoreCase(secondOwner)) && !(secondOwner.equalsIgnoreCase(thirdOwner))
							&& !(firstOwner.equalsIgnoreCase(thirdOwner))) {
						actualOner = firstOwner + " / " + secondOwner + " / " + thirdOwner;
						fc.utobj().printTestStep(
								"Assign Lead Owner by Sales Territories via ZIP  is working as excpeted");
					} else {
						System.out.println("Fails");
						fc.utobj().throwsException(
								"Assign Lead Owner by Sales Territories via ZIP is  not working as expcected");
					}

				} else if ("RestAPI".equals(fromWhere)) {
					FSSearchPageTest p3 = new FSSearchPageTest();
					testCaseIdInternal = "TC_Sales_SmartGroup_002";
					dataSet = fc.utobj().readTestData("sales", testCaseIdInternal);
					String firstName = fc.utobj().generateTestData("Re");
					String lastName = fc.utobj().generateTestData("Re");
					String country = "USA";
					// String state=dataSet.get("state");
					String state = "Florida";
					String leadOwner = "FranConnect Administrator";
					String leadSourceCategory = "Friends";
					String leadSourceDetails = "Friends";
					Map<String, String> UniqueKey_PrimaryInfo = new HashMap<String, String>();
					UniqueKey_PrimaryInfo.put("firstName", firstName);
					UniqueKey_PrimaryInfo.put("lastName", lastName);
					UniqueKey_PrimaryInfo.put("leadSource2ID", leadSourceCategory);
					UniqueKey_PrimaryInfo.put("leadSource3ID", leadSourceDetails);
					UniqueKey_PrimaryInfo.put("emailID", "harish@test.net");
					UniqueKey_PrimaryInfo.put("country", country);
					UniqueKey_PrimaryInfo.put("stateID", state);
					UniqueKey_PrimaryInfo.put("zip", zipCode);
					fc.utobj().printTestStep("Adding lead from Rest API");
					addLeadFromAllSourecesObj.addleadFromRestApi(driver, dataSet, testCaseId, UniqueKey_PrimaryInfo);

					driver.navigate().to(FranconnectUtil.config.get("buildUrl"));
					fc.loginpage().login(driver);
					fc.sales().sales_common().fsModule(driver);
					p3.searchByLeadName(driver, firstName, lastName);

					fc.utobj().clickElement(driver, driver
							.findElement(By.xpath(".//*[contains(text(),'" + firstName + " " + lastName + "')]")));

					fc.utobj().printTestStep("Verify a lead and Go to get Lead Owner");
					WebElement ownerelement = driver
							.findElement(By.xpath(".//tr/td[contains(text(),'Lead Owner :')]/following-sibling::td"));
					firstOwner = ownerelement.getText();

					testCaseIdInternal = "TC_Sales_SmartGroup_002";
					dataSet = fc.utobj().readTestData("sales", testCaseIdInternal);
					firstName = fc.utobj().generateTestData("Re");
					lastName = fc.utobj().generateTestData("Re");
					country = "USA";
					// String state=dataSet.get("state");
					state = "Florida";
					leadOwner = "FranConnect Administrator";
					leadSourceCategory = "Friends";
					leadSourceDetails = "Friends";
					UniqueKey_PrimaryInfo = new HashMap<String, String>();
					UniqueKey_PrimaryInfo.put("firstName", firstName);
					UniqueKey_PrimaryInfo.put("lastName", lastName);
					UniqueKey_PrimaryInfo.put("leadSource2ID", leadSourceCategory);
					UniqueKey_PrimaryInfo.put("leadSource3ID", leadSourceDetails);
					UniqueKey_PrimaryInfo.put("emailID", "harish@test.net");
					UniqueKey_PrimaryInfo.put("country", country);
					UniqueKey_PrimaryInfo.put("stateID", state);
					UniqueKey_PrimaryInfo.put("zip", zipCode);
					fc.utobj().printTestStep("Adding lead from Rest API");
					addLeadFromAllSourecesObj.addleadFromRestApi(driver, dataSet, testCaseId, UniqueKey_PrimaryInfo);

					driver.navigate().to(FranconnectUtil.config.get("buildUrl"));
					fc.loginpage().login(driver);
					fc.sales().sales_common().fsModule(driver);
					p3.searchByLeadName(driver, firstName, lastName);

					fc.utobj().clickElement(driver, driver
							.findElement(By.xpath(".//*[contains(text(),'" + firstName + " " + lastName + "')]")));

					fc.utobj().printTestStep("Verify a lead and Go to get Lead Owner");
					ownerelement = driver
							.findElement(By.xpath(".//tr/td[contains(text(),'Lead Owner :')]/following-sibling::td"));
					secondOwner = ownerelement.getText();

					testCaseIdInternal = "TC_Sales_SmartGroup_002";
					dataSet = fc.utobj().readTestData("sales", testCaseIdInternal);
					firstName = fc.utobj().generateTestData("Re");
					lastName = fc.utobj().generateTestData("Re");
					country = "USA";
					// String state=dataSet.get("state");
					state = "Florida";
					leadOwner = "FranConnect Administrator";
					leadSourceCategory = "Friends";
					leadSourceDetails = "Friends";
					UniqueKey_PrimaryInfo = new HashMap<String, String>();
					UniqueKey_PrimaryInfo.put("firstName", firstName);
					UniqueKey_PrimaryInfo.put("lastName", lastName);
					UniqueKey_PrimaryInfo.put("leadSource2ID", leadSourceCategory);
					UniqueKey_PrimaryInfo.put("leadSource3ID", leadSourceDetails);
					UniqueKey_PrimaryInfo.put("emailID", "harish@test.net");
					UniqueKey_PrimaryInfo.put("country", country);
					UniqueKey_PrimaryInfo.put("stateID", state);
					UniqueKey_PrimaryInfo.put("zip", zipCode);
					fc.utobj().printTestStep("Adding lead from Rest API");
					addLeadFromAllSourecesObj.addleadFromRestApi(driver, dataSet, testCaseId, UniqueKey_PrimaryInfo);

					driver.navigate().to(FranconnectUtil.config.get("buildUrl"));
					fc.loginpage().login(driver);
					fc.sales().sales_common().fsModule(driver);
					p3.searchByLeadName(driver, firstName, lastName);

					fc.utobj().clickElement(driver, driver
							.findElement(By.xpath(".//*[contains(text(),'" + firstName + " " + lastName + "')]")));
					fc.utobj().printTestStep("Verify a lead and Go to get Lead Owner");
					ownerelement = driver
							.findElement(By.xpath(".//tr/td[contains(text(),'Lead Owner :')]/following-sibling::td"));
					thirdOwner = ownerelement.getText();

					System.out.println(firstOwner + "====" + secondOwner + "====" + thirdOwner);

					if (!(firstOwner.equalsIgnoreCase(secondOwner)) && !(secondOwner.equalsIgnoreCase(thirdOwner))
							&& !(firstOwner.equalsIgnoreCase(thirdOwner))) {
						actualOner = firstOwner + " / " + secondOwner + " / " + thirdOwner;
						fc.utobj()
								.printTestStep("Assign Lead Owner by Sales Territories Via ZIP is working as excpeted");
					} else {
						System.out.println("Fails");
						fc.utobj().throwsException(
								"Assign Lead Owner by Sales Territories via ZIP  is  not working as expcected");
					}
				}
			}

		} else {

			// In case when round robin is set but no user set for owner should
			// goes to default owner
			fc.adminpage().adminPage(driver);
			// adsales.assignLeadOwners(driver);

			String userNameOwner = "FranConnect Administrator";
			fc.utobj().printTestStep("Create user to Assign Owner");
			String userNameRC = fc.utobj().generateTestData("IZ"); // "AssignOwner";
			userNameOwner = userNameRC + " " + userNameRC;
			String emailId = "salesautomation@staffex.com";
			if ("CorporateUser".equals(UserType)) {
				CorporateUser corpUser = new CorporateUser();
				corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
				corpUser.setEmail(emailId);
				corpUser = corpTest.createDefaultUser(driver, corpUser);
			} else if ("RegionalUser".equals(UserType)) {
				String regionName = fc.utobj().generateTestData("RG");
				regUser.addRegionalUser(driver, userNameRC, regionName, emailId);
				userNameOwner = userNameRC + " " + userNameRC + " (RU)";
			}
			// userNameOwner = userNameRC+" "+userNameRC;
			fc.utobj().printTestStep("Verify the Create user");

			adsales.assignLeadOwners(driver);

			fc.utobj().printTestStep("Verify the user is available in Default Owner drop down");

			boolean isLeadOwnerPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//select[@name='area1']/option[contains(text(),'" + userNameOwner + "')]");
			if (isLeadOwnerPresent == false) {
				fc.utobj().throwsException("User not present in Default Owner dropdown");
			}

			if (noownerset) {
				fc.utobj().selectDropDown(driver, pobj.defaultOwnerDrpDown, "Select");
				userNameOwner = "FranConnect Administrator";
			} else {
				fc.utobj().selectDropDown(driver, pobj.defaultOwnerDrpDown, userNameOwner);
			}

			fc.utobj().clickElement(driver, pobj.updateLeadOwnerBtn);

			if ("SystemLead".equals(fromWhere)) {
				fc.sales().sales_common().fsModule(driver);
				Sales fs = new Sales();
				fs.leadManagement(driver);
				FSLeadSummaryPageTest fsPage = new FSLeadSummaryPageTest();
				fsPage.clickAddLeadLink(driver);
				fc.utobj().printTestStep("Add a lead and Go to Primary Info");
				testCaseIdInternal = "TC_FC_QA_Sale_Lead_Primary_Owner_Assignment";
				dataSet = fc.utobj().readTestData("sales", testCaseIdInternal);
				String firstName = fc.utobj().generateTestData("AK");
				String lastName = fc.utobj().generateTestData("BK");
				dataSet.put("firstName", firstName);
				dataSet.put("lastName", lastName);
				dataSet.put("automatic", "As per Assignment Scheme");
				dataSet.put("zip", zipCode);
				printInfo = addLeadFromAllSourecesObj.fillFormDataWithDataset(driver, testCaseIdInternal, dataSet);
				fc.utobj().printTestStep("Verify a lead and Go to get Lead Owner");
				if (printInfo != null && printInfo.size() > 0) {
					isLeadOwnerPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
							".//tr/td[contains(text(),'Lead Owner :')]/following-sibling::td[contains (text() ,'"
									+ userNameOwner + "')]");
					if (isLeadOwnerPresent == false) {
						fc.utobj().throwsException("As per Assignment Scheme owner is not assigned");
					}

				}
			} else if ("WebForm".equals(fromWhere)) {
				testCaseIdInternal = "TC_FC_QA_Sale_Lead_Primary_Owner_Assignment";
				dataSet = fc.utobj().readTestData("sales", testCaseIdInternal);
				FSSearchPageTest p3 = new FSSearchPageTest();
				fc.utobj().printTestStep("Adding lead from Web Form");
				Map<String, String> leadInfo = new HashMap<String, String>();
				String firstName = fc.utobj().generateTestData("Wk");
				String lastName = fc.utobj().generateTestData("Wk");
				String formName = fc.utobj().generateTestData("FORM");
				leadInfo.put("firstName", firstName);
				leadInfo.put("lastName", lastName);
				leadInfo.put("country", dataSet.get("country"));
				leadInfo.put("state", dataSet.get("stateID"));
				leadInfo.put("email", dataSet.get("emailID"));
				leadInfo.put("leadOwner", userNameOwner);
				leadInfo.put("leadSourceCategory", dataSet.get("leadSource2ID"));
				leadInfo.put("leadSourceDetails", dataSet.get("leadSource3ID"));
				leadInfo.put("zipCode", zipCode);
				addLeadFromAllSourecesObj.addLeadFromWebForm(driver, formName, leadInfo, "Confirmation Message");
				p3.searchByLeadName(driver, firstName, lastName);
				fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
						".//*[contains(text(),'" + firstName + " " + lastName + "')]"));

				fc.utobj().printTestStep("Verify a lead and Go to get Lead Owner");
				isLeadOwnerPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
						".//tr/td[contains(text(),'Lead Owner :')]/following-sibling::td[contains (text() ,'"
								+ userNameOwner + "')]");
				if (isLeadOwnerPresent == false) {
					fc.utobj().throwsException("As per Assignment Scheme owner is not assigned");
				}

			} else if ("Import".equals(fromWhere)) {
				testCaseIdInternal = "TC_FC_QA_Sale_Lead_Primary_Owner_Assignment";
				dataSet = fc.utobj().readTestData("sales", testCaseIdInternal);
				FSSearchPageTest p3 = new FSSearchPageTest();
				fc.utobj().printTestStep("Adding lead from Web Form");
				Map<String, String> leadInfo = new HashMap<String, String>();
				String firstName = fc.utobj().generateTestData("Wk");
				String lastName = fc.utobj().generateTestData("Wk");
				String formName = fc.utobj().generateTestData("FORM");
				leadInfo.put("firstName", firstName);
				leadInfo.put("lastName", lastName);
				leadInfo.put("country", dataSet.get("country"));
				leadInfo.put("state", dataSet.get("stateID"));
				leadInfo.put("email", dataSet.get("emailID"));
				leadInfo.put("leadOwner", userNameOwner);
				leadInfo.put("leadSourceCategory", dataSet.get("leadSource2ID"));
				leadInfo.put("leadSourceDetails", dataSet.get("leadSource3ID"));
				leadInfo.put("zipCode", zipCode);
				// addLeadFromAllSourecesObj.addLeadFromWebForm(driver,
				// formName, leadInfo,"Confirmation Message");
				leadInfo.put("owner", "Based on Assignment Rules");
				String fileName = "OneLeadImportFile.csv";
				fc.utobj().printTestStep("Adding Lead from Import");
				fileName = fc.utobj().getFilePathFromTestData(fileName);

				try {
					addLeadFromAllSourecesObj.addLeadFromImport(driver, fileName, leadInfo);
				} catch (Exception e) {

				}
				p3.searchByLeadName(driver, firstName, lastName);
				fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
						".//*[contains(text(),'" + firstName + " " + lastName + "')]"));

				fc.utobj().printTestStep("Verify a lead and Go to get Lead Owner");
				isLeadOwnerPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
						".//tr/td[contains(text(),'Lead Owner :')]/following-sibling::td[contains (text() ,'"
								+ userNameOwner + "')]");
				if (isLeadOwnerPresent == false) {
					fc.utobj().throwsException("As per Assignment Scheme owner is not assigned");
				}

			} else if ("RestAPI".equals(fromWhere)) {
				FSSearchPageTest p3 = new FSSearchPageTest();
				testCaseIdInternal = "TC_Sales_SmartGroup_002";
				dataSet = fc.utobj().readTestData("sales", testCaseIdInternal);
				String firstName = fc.utobj().generateTestData("Re");
				String lastName = fc.utobj().generateTestData("Re");
				String country = "USA";
				// String state=dataSet.get("state");
				String state = "Florida";
				String leadOwner = "FranConnect Administrator";
				String leadSourceCategory = "Friends";
				String leadSourceDetails = "Friends";
				Map<String, String> UniqueKey_PrimaryInfo = new HashMap<String, String>();
				UniqueKey_PrimaryInfo.put("firstName", firstName);
				UniqueKey_PrimaryInfo.put("lastName", lastName);
				UniqueKey_PrimaryInfo.put("leadSource2ID", leadSourceCategory);
				UniqueKey_PrimaryInfo.put("leadSource3ID", leadSourceDetails);
				UniqueKey_PrimaryInfo.put("emailID", emailId);
				UniqueKey_PrimaryInfo.put("country", country);
				UniqueKey_PrimaryInfo.put("stateID", state);
				UniqueKey_PrimaryInfo.put("zip", zipCode);
				fc.utobj().printTestStep("Adding lead from Rest API");
				addLeadFromAllSourecesObj.addleadFromRestApi(driver, dataSet, testCaseId, UniqueKey_PrimaryInfo);

				driver.navigate().to(FranconnectUtil.config.get("buildUrl"));
				fc.loginpage().login(driver);
				fc.sales().sales_common().fsModule(driver);
				p3.searchByLeadName(driver, firstName, lastName);

				fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
						".//*[contains(text(),'" + firstName + " " + lastName + "')]"));

				fc.utobj().printTestStep("Verify a lead and Go to get Lead Owner");
				isLeadOwnerPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
						".//tr/td[contains(text(),'Lead Owner :')]/following-sibling::td[contains (text() ,'"
								+ userNameOwner + "')]");
				if (isLeadOwnerPresent == false) {
					fc.utobj().throwsException("As per Assignment Scheme owner is not assigned");
				}
			}
		}
		// End Here
		return actualOner;
	}

	public String getRoundRobinOwner(WebDriver driver, String testCaseId, String UserType, String fromWhere,
			boolean addDataforroundrobin, boolean noownerset) throws Exception {
		String actualOner = "";
		String testCaseIdInternal = "" + testCaseId;
		Map<String, String> dataSet = fc.utobj().readTestData("sales", testCaseId);
		Map<String, String> printInfo = null;
		AddLeadFromAllSources addLeadFromAllSourecesObj = new AddLeadFromAllSources();
		LeadSummaryUI pobj = new LeadSummaryUI(driver);
		AdminFranchiseSalesManageSalesTerritoryCreateSalesTerritoryPageTest p1 = new AdminFranchiseSalesManageSalesTerritoryCreateSalesTerritoryPageTest();
		AdminUsersManageCorporateUsersAddCorporateUserPageTest corpTest = new AdminUsersManageCorporateUsersAddCorporateUserPageTest();
		AdminUsersManageRegionalUsersAddRegionalUserPageTest regUser = new AdminUsersManageRegionalUsersAddRegionalUserPageTest();
		String firstOwner = "";
		String secondOwner = "";
		String thirdOwner = "";
		fc.utobj().printTestStep("Go to Admin");
		fc.adminpage().adminPage(driver);
		fc.utobj().printTestStep("Go to AssignLeadOwners Setting");

		AdminSales adsales = new AdminSales();
		adsales.assignLeadOwners(driver);
		List<WebElement> oRadioButton = driver.findElements(By.name("radiobutton"));
		boolean bValue = false;
		bValue = oRadioButton.get(0).isSelected();
		if (oRadioButton.get(0).isSelected()) {

		} else {
			oRadioButton.get(0).click();
		}

		fc.utobj().printTestStep("Click Continue");
		fc.utobj().clickElement(driver, pobj.continueButton);
		fc.utobj().printTestStep(" Admin > Sales >  Assign Lead Owners");
		List<WebElement> allroundrobin = driver.findElements(By.xpath(".//*[@id='templateOrder']/option"));

		if (allroundrobin != null && allroundrobin.size() > 0) {
			for (int k = 0; k < allroundrobin.size(); k++) {
				// System.out.println("Values
				// ==="+allroundrobin.get(k).getText());
				WebElement element = driver
						.findElement(By.xpath(".//select[@id='templateOrder']/option[contains(text(),'"
								+ allroundrobin.get(k).getText() + "')]"));
				element.click();
				fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//input[@value='<<']"));

			}
			fc.utobj().clickElement(driver, pobj.updateLeadOwnerBtn);

		}
		allroundrobin = driver.findElements(By.xpath(".//*[@id='templateOrder']/option"));
		// System.out.println("allroundrobin====="+allroundrobin.size());

		if (addDataforroundrobin) {

			if ((allroundrobin == null) || allroundrobin.size() < 3) {

				for (int i = 0; i < 3; i++) {
					fc.adminpage().adminPage(driver);
					// adsales.assignLeadOwners(driver);

					String userNameOwner = "FranConnect Administrator";
					fc.utobj().printTestStep("Create user to Assign Owner");
					String userNameRC = fc.utobj().generateTestData("IZ"); // "AssignOwner";

					String emailId = "salesautomation@staffex.com";
					if ("CorporateUser".equals(UserType)) {
						CorporateUser corpUser = new CorporateUser();
						corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
						corpUser.setEmail(emailId);
						corpUser = corpTest.createDefaultUser(driver, corpUser);
					} else if ("RegionalUser".equals(UserType)) {
						String regionName = fc.utobj().generateTestData("RG");
						regUser.addRegionalUser(driver, userNameRC, regionName, emailId);
					}
					userNameOwner = userNameRC + " " + userNameRC;
					// fc.utobj().printTestStep(testCaseId, "Verify the Create
					// user");
					fc.adminpage().adminPage(driver);

					adsales.assignLeadOwners(driver);

					fc.utobj().printTestStep("Verify the user is available in Default Owner drop down");

					boolean isLeadOwnerPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
							".//select[@name='area1']/option[contains(text(),'" + userNameOwner + "')]");
					if (isLeadOwnerPresent == false) {
						fc.utobj().throwsException("User not present in Default Owner dropdown");
					}

					oRadioButton = driver.findElements(By.name("radiobutton"));
					bValue = false;
					bValue = oRadioButton.get(0).isSelected();
					if (oRadioButton.get(0).isSelected()) {

					} else {
						oRadioButton.get(0).click();
					}

					fc.utobj().printTestStep("Click Continue");
					fc.utobj().clickElement(driver, pobj.continueButton);
					fc.utobj().printTestStep("Verify the user is available in Available Lead Owners");
					boolean availableLeadOwner = fc.utobj().verifyElementOnVisible_ByXpath(driver,
							".//select[@id='allOwners']/option[contains(text(),'" + userNameOwner + "')]");
					if (availableLeadOwner == false) {
						fc.utobj().throwsException("User is  not available in Available Lead Owners");
					}

					WebElement element = driver.findElement(
							By.xpath(".//select[@id='allOwners']/option[contains(text(),'" + userNameOwner + "')]"));
					element.click();
					fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//input[@value='>>']"));
					fc.utobj().clickElement(driver, pobj.updateLeadOwnerBtn);

					boolean configredLeadOwner = fc.utobj().verifyElementOnVisible_ByXpath(driver,
							".//select[@id='templateOrder']/option[contains(text(),'" + userNameOwner + "')]");
					if (configredLeadOwner == false) {
						fc.utobj().throwsException("User is  not available in  Configured Lead Owners");
					}
				}

			}

			allroundrobin = driver.findElements(By.xpath(".//*[@id='templateOrder']/option"));

			// Start here to check Lead

			if (allroundrobin != null && allroundrobin.size() >= 3) {

				if ("SystemLead".equals(fromWhere)) {
					// First Lead
					fc.sales().sales_common().fsModule(driver);
					Sales fs = new Sales();
					fs.leadManagement(driver);
					FSLeadSummaryPageTest fsPage = new FSLeadSummaryPageTest();
					fsPage.clickAddLeadLink(driver);
					fc.utobj().printTestStep("Add a lead and Go to Primary Info");
					testCaseIdInternal = "TC_FC_QA_Sale_Lead_Primary_Owner_Assignment";
					dataSet = fc.utobj().readTestData("sales", testCaseIdInternal);
					String firstName = fc.utobj().generateTestData("AK");
					String lastName = fc.utobj().generateTestData("BK");
					dataSet.put("firstName", firstName);
					dataSet.put("lastName", lastName);
					dataSet.put("automatic", "As per Assignment Scheme");
					printInfo = addLeadFromAllSourecesObj.fillFormDataWithDataset(driver, testCaseIdInternal, dataSet);

					if (printInfo != null && printInfo.size() > 0) {
						fc.utobj().printTestStep("Verify a lead and Go to get First Lead Owner");
						// .//tr/td[contains(text(),'Lead Owner
						// :')]/following-sibling::td[contains (text()
						// ,'"+userNameOwner+"')]
						WebElement ownerelement = driver.findElement(
								By.xpath(".//tr/td[contains(text(),'Lead Owner :')]/following-sibling::td"));
						firstOwner = ownerelement.getText();

					}

					// End First Lead
					// Enter Second Lead
					fc.sales().sales_common().fsModule(driver);
					fs = new Sales();
					fs.leadManagement(driver);
					fsPage.clickAddLeadLink(driver);
					fc.utobj().printTestStep("Add a lead and Go to Primary Info");
					testCaseIdInternal = "TC_FC_QA_Sale_Lead_Primary_Owner_Assignment";
					dataSet = fc.utobj().readTestData("sales", testCaseIdInternal);
					firstName = fc.utobj().generateTestData("AK");
					lastName = fc.utobj().generateTestData("BK");
					dataSet.put("firstName", firstName);
					dataSet.put("lastName", lastName);
					dataSet.put("automatic", "As per Assignment Scheme");
					printInfo = addLeadFromAllSourecesObj.fillFormDataWithDataset(driver, testCaseIdInternal, dataSet);

					if (printInfo != null && printInfo.size() > 0) {
						fc.utobj().printTestStep("Verify a lead and Go to get Second Lead Owner");
						// .//tr/td[contains(text(),'Lead Owner
						// :')]/following-sibling::td[contains (text()
						// ,'"+userNameOwner+"')]
						WebElement ownerelement = driver.findElement(
								By.xpath(".//tr/td[contains(text(),'Lead Owner :')]/following-sibling::td"));
						secondOwner = ownerelement.getText();

					}
					// End Second Lead
					// Enter Third Lead
					fc.sales().sales_common().fsModule(driver);
					fs = new Sales();
					fs.leadManagement(driver);
					fsPage.clickAddLeadLink(driver);
					fc.utobj().printTestStep("Add a lead and Go to Primary Info");
					testCaseIdInternal = "TC_FC_QA_Sale_Lead_Primary_Owner_Assignment";
					dataSet = fc.utobj().readTestData("sales", testCaseIdInternal);
					firstName = fc.utobj().generateTestData("AK");
					lastName = fc.utobj().generateTestData("BK");
					dataSet.put("firstName", firstName);
					dataSet.put("lastName", lastName);
					dataSet.put("automatic", "As per Assignment Scheme");
					printInfo = addLeadFromAllSourecesObj.fillFormDataWithDataset(driver, testCaseIdInternal, dataSet);

					if (printInfo != null && printInfo.size() > 0) {
						fc.utobj().printTestStep("Verify a lead and Go to get Third Lead Owner");
						// .//tr/td[contains(text(),'Lead Owner
						// :')]/following-sibling::td[contains (text()
						// ,'"+userNameOwner+"')]
						WebElement ownerelement = driver.findElement(
								By.xpath(".//tr/td[contains(text(),'Lead Owner :')]/following-sibling::td"));
						thirdOwner = ownerelement.getText();

					}
					// End Second Lead

					System.out.println(firstOwner + "====" + secondOwner + "====" + thirdOwner);

					if (!(firstOwner.equalsIgnoreCase(secondOwner)) && !(secondOwner.equalsIgnoreCase(thirdOwner))
							&& !(firstOwner.equalsIgnoreCase(thirdOwner))) {
						actualOner = firstOwner + " / " + secondOwner + " / " + thirdOwner;
						fc.utobj().printTestStep("Round robin  is working as excpeted. ");
					} else {
						System.out.println("Fails");
						fc.utobj().throwsException("Round robin  is  not working as expcected");
					}

				} // End system Lead method
				else if ("WebForm".equals(fromWhere)) {
					testCaseIdInternal = "TC_FC_QA_Sale_Lead_Primary_Owner_Assignment";
					dataSet = fc.utobj().readTestData("sales", testCaseIdInternal);
					FSSearchPageTest p3 = new FSSearchPageTest();
					fc.utobj().printTestStep("Adding lead from Web Form");
					Map<String, String> leadInfo = new HashMap<String, String>();
					String firstName = fc.utobj().generateTestData("Wk");
					String lastName = fc.utobj().generateTestData("Wk");
					String formName = fc.utobj().generateTestData("FORM");
					leadInfo.put("firstName", firstName);
					leadInfo.put("lastName", lastName);
					leadInfo.put("country", dataSet.get("country"));
					leadInfo.put("state", dataSet.get("stateID"));
					leadInfo.put("email", dataSet.get("emailID"));
					// leadInfo.put("leadOwner", "FranConnect Administrator");
					leadInfo.put("leadSourceCategory", dataSet.get("leadSource2ID"));
					leadInfo.put("leadSourceDetails", dataSet.get("leadSource3ID"));
					addLeadFromAllSourecesObj.addLeadFromWebForm(driver, formName, leadInfo, "Confirmation Message");
					p3.searchByLeadName(driver, firstName, lastName);
					fc.utobj().clickElement(driver, driver
							.findElement(By.xpath(".//*[contains(text(),'" + firstName + " " + lastName + "')]")));

					WebElement ownerelement = driver
							.findElement(By.xpath(".//tr/td[contains(text(),'Lead Owner :')]/following-sibling::td"));
					firstOwner = ownerelement.getText();
					fc.utobj().printTestStep("Verify a lead and Go to get First Lead Owner");

					testCaseIdInternal = "TC_FC_QA_Sale_Lead_Primary_Owner_Assignment";
					dataSet = fc.utobj().readTestData("sales", testCaseIdInternal);
					fc.utobj().printTestStep("Adding lead from Web Form");
					leadInfo = new HashMap<String, String>();
					firstName = fc.utobj().generateTestData("Wk");
					lastName = fc.utobj().generateTestData("Wk");
					formName = fc.utobj().generateTestData("FORM");
					leadInfo.put("firstName", firstName);
					leadInfo.put("lastName", lastName);
					leadInfo.put("country", dataSet.get("country"));
					leadInfo.put("state", dataSet.get("stateID"));
					leadInfo.put("email", dataSet.get("emailID"));
					// leadInfo.put("leadOwner", "FranConnect Administrator");
					leadInfo.put("leadSourceCategory", dataSet.get("leadSource2ID"));
					leadInfo.put("leadSourceDetails", dataSet.get("leadSource3ID"));
					addLeadFromAllSourecesObj.addLeadFromWebForm(driver, formName, leadInfo, "Confirmation Message");
					p3.searchByLeadName(driver, firstName, lastName);
					fc.utobj().clickElement(driver, driver
							.findElement(By.xpath(".//*[contains(text(),'" + firstName + " " + lastName + "')]")));

					ownerelement = driver
							.findElement(By.xpath(".//tr/td[contains(text(),'Lead Owner :')]/following-sibling::td"));
					secondOwner = ownerelement.getText();
					fc.utobj().printTestStep("Verify a lead and Go to get Second Lead Owner");

					testCaseIdInternal = "TC_FC_QA_Sale_Lead_Primary_Owner_Assignment";
					dataSet = fc.utobj().readTestData("sales", testCaseIdInternal);
					fc.utobj().printTestStep("Adding lead from Web Form");
					leadInfo = new HashMap<String, String>();
					firstName = fc.utobj().generateTestData("Wk");
					lastName = fc.utobj().generateTestData("Wk");
					formName = fc.utobj().generateTestData("FORM");
					leadInfo.put("firstName", firstName);
					leadInfo.put("lastName", lastName);
					leadInfo.put("country", dataSet.get("country"));
					leadInfo.put("state", dataSet.get("stateID"));
					leadInfo.put("email", dataSet.get("emailID"));
					// leadInfo.put("leadOwner", "FranConnect Administrator");
					leadInfo.put("leadSourceCategory", dataSet.get("leadSource2ID"));
					leadInfo.put("leadSourceDetails", dataSet.get("leadSource3ID"));
					addLeadFromAllSourecesObj.addLeadFromWebForm(driver, formName, leadInfo, "Confirmation Message");
					p3.searchByLeadName(driver, firstName, lastName);
					fc.utobj().clickElement(driver, driver
							.findElement(By.xpath(".//*[contains(text(),'" + firstName + " " + lastName + "')]")));

					ownerelement = driver
							.findElement(By.xpath(".//tr/td[contains(text(),'Lead Owner :')]/following-sibling::td"));
					thirdOwner = ownerelement.getText();
					fc.utobj().printTestStep("Verify a lead and Go to get Third Lead Owner");

					System.out.println(firstOwner + "====" + secondOwner + "====" + thirdOwner);

					if (!(firstOwner.equalsIgnoreCase(secondOwner)) && !(secondOwner.equalsIgnoreCase(thirdOwner))
							&& !(firstOwner.equalsIgnoreCase(thirdOwner))) {
						actualOner = firstOwner + " / " + secondOwner + " / " + thirdOwner;
						fc.utobj().printTestStep("Round robin  is working as excpeted");
					} else {
						System.out.println("Fails");
						fc.utobj().throwsException("Round robin  is  not working as expcected");
					}

				} else if ("Import".equals(fromWhere)) {
					testCaseIdInternal = "TC_FC_QA_Sale_Lead_Primary_Owner_Assignment";
					dataSet = fc.utobj().readTestData("sales", testCaseIdInternal);
					FSSearchPageTest p3 = new FSSearchPageTest();
					fc.utobj().printTestStep("Adding lead from Web Form");
					Map<String, String> leadInfo = new HashMap<String, String>();
					String firstName = fc.utobj().generateTestData("Im");
					String lastName = fc.utobj().generateTestData("Po");
					String formName = fc.utobj().generateTestData("FORM");
					leadInfo.put("firstName", firstName);
					leadInfo.put("lastName", lastName);
					leadInfo.put("country", dataSet.get("country"));
					leadInfo.put("state", dataSet.get("stateID"));
					leadInfo.put("email", dataSet.get("emailID"));
					// leadInfo.put("leadOwner", "FranConnect Administrator");
					leadInfo.put("leadSourceCategory", dataSet.get("leadSource2ID"));
					leadInfo.put("leadSourceDetails", dataSet.get("leadSource3ID"));
					leadInfo.put("owner", "Based on Assignment Rules");
					String fileName = "OneLeadImportFile.csv";
					fc.utobj().printTestStep("Adding Lead from Import");
					fileName = fc.utobj().getFilePathFromTestData(fileName);
					try {
						addLeadFromAllSourecesObj.addLeadFromImport(driver, fileName, leadInfo);
					} catch (Exception e) {
						System.out.println("=round robin===" + e.getMessage());
					}
					// .addLeadFromWebForm(driver, formName,
					// leadInfo,"Confirmation Message");
					p3.searchByLeadName(driver, firstName, lastName);
					fc.utobj().clickElement(driver, driver
							.findElement(By.xpath(".//*[contains(text(),'" + firstName + " " + lastName + "')]")));

					WebElement ownerelement = driver
							.findElement(By.xpath(".//tr/td[contains(text(),'Lead Owner :')]/following-sibling::td"));
					firstOwner = ownerelement.getText();
					fc.utobj().printTestStep("Verify a lead and Go to get First Lead Owner");

					testCaseIdInternal = "TC_FC_QA_Sale_Lead_Primary_Owner_Assignment";
					dataSet = fc.utobj().readTestData("sales", testCaseIdInternal);
					fc.utobj().printTestStep("Adding lead from Web Form");
					leadInfo = new HashMap<String, String>();
					firstName = fc.utobj().generateTestData("Wk");
					lastName = fc.utobj().generateTestData("Wk");
					formName = fc.utobj().generateTestData("FORM");
					leadInfo.put("firstName", firstName);
					leadInfo.put("lastName", lastName);
					leadInfo.put("country", dataSet.get("country"));
					leadInfo.put("state", dataSet.get("stateID"));
					leadInfo.put("email", dataSet.get("emailID"));
					// leadInfo.put("leadOwner", "FranConnect Administrator");
					leadInfo.put("leadSourceCategory", dataSet.get("leadSource2ID"));
					leadInfo.put("leadSourceDetails", dataSet.get("leadSource3ID"));
					leadInfo.put("owner", "Based on Assignment Rules");
					// addLeadFromAllSourecesObj.addLeadFromWebForm(driver,
					// formName, leadInfo,"Confirmation Message");
					addLeadFromAllSourecesObj.addLeadFromImport(driver, fileName, leadInfo);
					p3.searchByLeadName(driver, firstName, lastName);
					fc.utobj().clickElement(driver, driver
							.findElement(By.xpath(".//*[contains(text(),'" + firstName + " " + lastName + "')]")));

					ownerelement = driver
							.findElement(By.xpath(".//tr/td[contains(text(),'Lead Owner :')]/following-sibling::td"));
					secondOwner = ownerelement.getText();
					fc.utobj().printTestStep("Verify a lead and Go to get Second Lead Owner");

					testCaseIdInternal = "TC_FC_QA_Sale_Lead_Primary_Owner_Assignment";
					dataSet = fc.utobj().readTestData("sales", testCaseIdInternal);
					fc.utobj().printTestStep("Adding lead from Web Form");
					leadInfo = new HashMap<String, String>();
					firstName = fc.utobj().generateTestData("Wk");
					lastName = fc.utobj().generateTestData("Wk");
					formName = fc.utobj().generateTestData("FORM");
					leadInfo.put("firstName", firstName);
					leadInfo.put("lastName", lastName);
					leadInfo.put("country", dataSet.get("country"));
					leadInfo.put("state", dataSet.get("stateID"));
					leadInfo.put("email", dataSet.get("emailID"));
					// leadInfo.put("leadOwner", "FranConnect Administrator");
					leadInfo.put("leadSourceCategory", dataSet.get("leadSource2ID"));
					leadInfo.put("leadSourceDetails", dataSet.get("leadSource3ID"));
					leadInfo.put("owner", "Based on Assignment Rules");
					// addLeadFromAllSourecesObj.addLeadFromWebForm(driver,
					// formName, leadInfo,"Confirmation Message");
					addLeadFromAllSourecesObj.addLeadFromImport(driver, fileName, leadInfo);
					p3.searchByLeadName(driver, firstName, lastName);
					fc.utobj().clickElement(driver, driver
							.findElement(By.xpath(".//*[contains(text(),'" + firstName + " " + lastName + "')]")));

					ownerelement = driver
							.findElement(By.xpath(".//tr/td[contains(text(),'Lead Owner :')]/following-sibling::td"));
					thirdOwner = ownerelement.getText();
					fc.utobj().printTestStep("Verify a lead and Go to get Third Lead Owner");

					System.out.println(firstOwner + "====" + secondOwner + "====" + thirdOwner);

					if (!(firstOwner.equalsIgnoreCase(secondOwner)) && !(secondOwner.equalsIgnoreCase(thirdOwner))
							&& !(firstOwner.equalsIgnoreCase(thirdOwner))) {
						actualOner = firstOwner + " / " + secondOwner + " / " + thirdOwner;
						fc.utobj().printTestStep("Round robin  is working as excpeted");
					} else {
						System.out.println("Fails");
						fc.utobj().throwsException("Round robin  is  not working as expcected");
					}

				} else if ("RestAPI".equals(fromWhere)) {
					FSSearchPageTest p3 = new FSSearchPageTest();
					testCaseIdInternal = "TC_Sales_SmartGroup_002";
					dataSet = fc.utobj().readTestData("sales", testCaseIdInternal);
					String firstName = fc.utobj().generateTestData("Re");
					String lastName = fc.utobj().generateTestData("Re");
					String country = "USA";
					// String state=dataSet.get("state");
					String state = "Florida";
					String leadOwner = "FranConnect Administrator";
					String leadSourceCategory = "Friends";
					String leadSourceDetails = "Friends";
					Map<String, String> UniqueKey_PrimaryInfo = new HashMap<String, String>();
					UniqueKey_PrimaryInfo.put("firstName", firstName);
					UniqueKey_PrimaryInfo.put("lastName", lastName);
					UniqueKey_PrimaryInfo.put("leadSource2ID", leadSourceCategory);
					UniqueKey_PrimaryInfo.put("leadSource3ID", leadSourceDetails);
					UniqueKey_PrimaryInfo.put("emailID", "harish@test.net");
					UniqueKey_PrimaryInfo.put("country", country);
					UniqueKey_PrimaryInfo.put("stateID", state);
					fc.utobj().printTestStep("Adding lead from Rest API");
					addLeadFromAllSourecesObj.addleadFromRestApi(driver, dataSet, testCaseId, UniqueKey_PrimaryInfo);

					driver.navigate().to(FranconnectUtil.config.get("buildUrl"));
					fc.loginpage().login(driver);
					fc.sales().sales_common().fsModule(driver);
					p3.searchByLeadName(driver, firstName, lastName);

					fc.utobj().clickElement(driver, driver
							.findElement(By.xpath(".//*[contains(text(),'" + firstName + " " + lastName + "')]")));

					WebElement ownerelement = driver
							.findElement(By.xpath(".//tr/td[contains(text(),'Lead Owner :')]/following-sibling::td"));
					firstOwner = ownerelement.getText();

					fc.utobj().printTestStep("Verify a lead and Go to get First Lead Owner");

					testCaseIdInternal = "TC_Sales_SmartGroup_002";
					dataSet = fc.utobj().readTestData("sales", testCaseIdInternal);
					firstName = fc.utobj().generateTestData("Re");
					lastName = fc.utobj().generateTestData("Re");
					country = "USA";
					// String state=dataSet.get("state");
					state = "Florida";
					leadOwner = "FranConnect Administrator";
					leadSourceCategory = "Friends";
					leadSourceDetails = "Friends";
					UniqueKey_PrimaryInfo = new HashMap<String, String>();
					UniqueKey_PrimaryInfo.put("firstName", firstName);
					UniqueKey_PrimaryInfo.put("lastName", lastName);
					UniqueKey_PrimaryInfo.put("leadSource2ID", leadSourceCategory);
					UniqueKey_PrimaryInfo.put("leadSource3ID", leadSourceDetails);
					UniqueKey_PrimaryInfo.put("emailID", "harish@test.net");
					UniqueKey_PrimaryInfo.put("country", country);
					UniqueKey_PrimaryInfo.put("stateID", state);

					fc.utobj().printTestStep("Adding lead from Rest API");
					addLeadFromAllSourecesObj.addleadFromRestApi(driver, dataSet, testCaseId, UniqueKey_PrimaryInfo);

					driver.navigate().to(FranconnectUtil.config.get("buildUrl"));
					fc.loginpage().login(driver);
					fc.sales().sales_common().fsModule(driver);
					p3.searchByLeadName(driver, firstName, lastName);

					fc.utobj().clickElement(driver, driver
							.findElement(By.xpath(".//*[contains(text(),'" + firstName + " " + lastName + "')]")));

					ownerelement = driver
							.findElement(By.xpath(".//tr/td[contains(text(),'Lead Owner :')]/following-sibling::td"));
					secondOwner = ownerelement.getText();

					fc.utobj().printTestStep("Verify a lead and Go to get Second Lead Owner");

					testCaseIdInternal = "TC_Sales_SmartGroup_002";
					dataSet = fc.utobj().readTestData("sales", testCaseIdInternal);
					firstName = fc.utobj().generateTestData("Re");
					lastName = fc.utobj().generateTestData("Re");
					country = "USA";
					// String state=dataSet.get("state");
					state = "Florida";
					leadOwner = "FranConnect Administrator";
					leadSourceCategory = "Friends";
					leadSourceDetails = "Friends";
					UniqueKey_PrimaryInfo = new HashMap<String, String>();
					UniqueKey_PrimaryInfo.put("firstName", firstName);
					UniqueKey_PrimaryInfo.put("lastName", lastName);
					UniqueKey_PrimaryInfo.put("leadSource2ID", leadSourceCategory);
					UniqueKey_PrimaryInfo.put("leadSource3ID", leadSourceDetails);
					UniqueKey_PrimaryInfo.put("emailID", "harish@test.net");
					UniqueKey_PrimaryInfo.put("country", country);
					UniqueKey_PrimaryInfo.put("stateID", state);
					fc.utobj().printTestStep("Adding lead from Rest API");
					addLeadFromAllSourecesObj.addleadFromRestApi(driver, dataSet, testCaseId, UniqueKey_PrimaryInfo);

					driver.navigate().to(FranconnectUtil.config.get("buildUrl"));
					fc.loginpage().login(driver);
					fc.sales().sales_common().fsModule(driver);
					p3.searchByLeadName(driver, firstName, lastName);

					fc.utobj().clickElement(driver, driver
							.findElement(By.xpath(".//*[contains(text(),'" + firstName + " " + lastName + "')]")));

					ownerelement = driver
							.findElement(By.xpath(".//tr/td[contains(text(),'Lead Owner :')]/following-sibling::td"));
					thirdOwner = ownerelement.getText();

					fc.utobj().printTestStep("Verify a lead and Go to get Third Lead Owner");

					System.out.println(firstOwner + "====" + secondOwner + "====" + thirdOwner);

					if (!(firstOwner.equalsIgnoreCase(secondOwner)) && !(secondOwner.equalsIgnoreCase(thirdOwner))
							&& !(firstOwner.equalsIgnoreCase(thirdOwner))) {
						actualOner = firstOwner + " / " + secondOwner + " / " + thirdOwner;
						fc.utobj().printTestStep("Round robin  is working as excpeted");
					} else {
						System.out.println("Fails");
						fc.utobj().throwsException("Round robin  is  not working as expcected");
					}
				}
			}

		} else {

			// In case when round robin is set but no user set for owner should
			// goes to default owner
			fc.adminpage().adminPage(driver);
			// adsales.assignLeadOwners(driver);

			String userNameOwner = "FranConnect Administrator";
			fc.utobj().printTestStep("Create user to Assign Owner");
			String userNameRC = fc.utobj().generateTestData("IZ"); // "AssignOwner";
			userNameOwner = userNameRC + " " + userNameRC;
			String emailId = "salesautomation@staffex.com";
			if ("CorporateUser".equals(UserType)) {
				CorporateUser corpUser = new CorporateUser();
				corpUser = fc.commonMethods().fillDefaultDataFor_CorporateUser(corpUser);
				corpUser.setEmail(emailId);
				corpUser = corpTest.createDefaultUser(driver, corpUser);
			} else if ("RegionalUser".equals(UserType)) {
				String regionName = fc.utobj().generateTestData("RG");
				regUser.addRegionalUser(driver, userNameRC, regionName, emailId);
				userNameOwner = userNameRC + " " + userNameRC + " (RU)";
			}
			// userNameOwner = userNameRC+" "+userNameRC;
			fc.utobj().printTestStep("Verify the Create user");
			fc.adminpage().adminPage(driver);

			adsales.assignLeadOwners(driver);

			fc.utobj().printTestStep("Verify the user is available in Default Owner drop down");

			boolean isLeadOwnerPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
					".//select[@name='area1']/option[contains(text(),'" + userNameOwner + "')]");
			if (isLeadOwnerPresent == false) {
				fc.utobj().throwsException("User not present in Default Owner dropdown");
			}

			if (noownerset) {
				fc.utobj().selectDropDown(driver, pobj.defaultOwnerDrpDown, "Select");
				userNameOwner = "FranConnect Administrator";
			} else {
				fc.utobj().selectDropDown(driver, pobj.defaultOwnerDrpDown, userNameOwner);
			}

			fc.utobj().clickElement(driver, pobj.updateLeadOwnerBtn);

			if ("SystemLead".equals(fromWhere)) {
				fc.sales().sales_common().fsModule(driver);
				Sales fs = new Sales();
				fs.leadManagement(driver);
				FSLeadSummaryPageTest fsPage = new FSLeadSummaryPageTest();
				fsPage.clickAddLeadLink(driver);
				fc.utobj().printTestStep("Add a lead and Go to Primary Info");
				testCaseIdInternal = "TC_FC_QA_Sale_Lead_Primary_Owner_Assignment";
				dataSet = fc.utobj().readTestData("sales", testCaseIdInternal);
				String firstName = fc.utobj().generateTestData("AK");
				String lastName = fc.utobj().generateTestData("BK");
				dataSet.put("firstName", firstName);
				dataSet.put("lastName", lastName);
				dataSet.put("automatic", "As per Assignment Scheme");
				printInfo = addLeadFromAllSourecesObj.fillFormDataWithDataset(driver, testCaseIdInternal, dataSet);
				fc.utobj().printTestStep("Verify a lead and get Lead Owner");
				if (printInfo != null && printInfo.size() > 0) {
					isLeadOwnerPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
							".//tr/td[contains(text(),'Lead Owner :')]/following-sibling::td[contains (text() ,'"
									+ userNameOwner + "')]");
					if (isLeadOwnerPresent == false) {
						fc.utobj().throwsException("As per Assignment Scheme owner is not assigned");
					}

				}
			} else if ("WebForm".equals(fromWhere)) {
				testCaseIdInternal = "TC_FC_QA_Sale_Lead_Primary_Owner_Assignment";
				dataSet = fc.utobj().readTestData("sales", testCaseIdInternal);
				FSSearchPageTest p3 = new FSSearchPageTest();
				fc.utobj().printTestStep("Adding lead from Web Form");
				Map<String, String> leadInfo = new HashMap<String, String>();
				String firstName = fc.utobj().generateTestData("Wk");
				String lastName = fc.utobj().generateTestData("Wk");
				String formName = fc.utobj().generateTestData("FORM");
				leadInfo.put("firstName", firstName);
				leadInfo.put("lastName", lastName);
				leadInfo.put("country", dataSet.get("country"));
				leadInfo.put("state", dataSet.get("stateID"));
				leadInfo.put("email", dataSet.get("emailID"));
				leadInfo.put("leadOwner", userNameOwner);
				leadInfo.put("leadSourceCategory", dataSet.get("leadSource2ID"));
				leadInfo.put("leadSourceDetails", dataSet.get("leadSource3ID"));
				addLeadFromAllSourecesObj.addLeadFromWebForm(driver, formName, leadInfo, "Confirmation Message");
				p3.searchByLeadName(driver, firstName, lastName);
				fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
						".//*[contains(text(),'" + firstName + " " + lastName + "')]"));
				fc.utobj().printTestStep("Verify a lead and get Lead Owner");

				isLeadOwnerPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
						".//tr/td[contains(text(),'Lead Owner :')]/following-sibling::td[contains (text() ,'"
								+ userNameOwner + "')]");
				if (isLeadOwnerPresent == false) {
					fc.utobj().throwsException("As per Assignment Scheme owner is not assigned");
				}

			} else if ("Import".equals(fromWhere)) {
				testCaseIdInternal = "TC_FC_QA_Sale_Lead_Primary_Owner_Assignment";
				dataSet = fc.utobj().readTestData("sales", testCaseIdInternal);
				FSSearchPageTest p3 = new FSSearchPageTest();
				fc.utobj().printTestStep("Adding lead from Web Form");
				Map<String, String> leadInfo = new HashMap<String, String>();
				String firstName = fc.utobj().generateTestData("Im");
				String lastName = fc.utobj().generateTestData("Po");
				leadInfo.put("firstName", firstName);
				leadInfo.put("lastName", lastName);
				leadInfo.put("country", dataSet.get("country"));
				leadInfo.put("state", dataSet.get("stateID"));
				leadInfo.put("email", dataSet.get("emailID"));
				leadInfo.put("leadOwner", userNameOwner);
				leadInfo.put("leadSourceCategory", dataSet.get("leadSource2ID"));
				leadInfo.put("leadSourceDetails", dataSet.get("leadSource3ID"));
				leadInfo.put("owner", "Based on Assignment Rules");

				String fileName = "OneLeadImportFile.csv";
				fc.utobj().printTestStep("Adding Lead from Import");
				fileName = fc.utobj().getFilePathFromTestData(fileName);

				try {
					addLeadFromAllSourecesObj.addLeadFromImport(driver, fileName, leadInfo);
				} catch (Exception e) {

				}

				// addLeadFromAllSourecesObj.addLeadFromWebForm(driver,
				// formName, leadInfo,"Confirmation Message");
				p3.searchByLeadName(driver, firstName, lastName);
				fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
						".//*[contains(text(),'" + firstName + " " + lastName + "')]"));
				fc.utobj().printTestStep("Verify a lead and get Lead Owner");

				isLeadOwnerPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
						".//tr/td[contains(text(),'Lead Owner :')]/following-sibling::td[contains (text() ,'"
								+ userNameOwner + "')]");
				if (isLeadOwnerPresent == false) {
					fc.utobj().throwsException("As per Assignment Scheme owner is not assigned");
				}

			} else if ("RestAPI".equals(fromWhere)) {
				FSSearchPageTest p3 = new FSSearchPageTest();
				testCaseIdInternal = "TC_Sales_SmartGroup_002";
				dataSet = fc.utobj().readTestData("sales", testCaseIdInternal);
				String firstName = fc.utobj().generateTestData("Re");
				String lastName = fc.utobj().generateTestData("Re");
				String country = "USA";
				// String state=dataSet.get("state");
				String state = "Florida";
				String leadSourceCategory = "Friends";
				String leadSourceDetails = "Friends";
				Map<String, String> UniqueKey_PrimaryInfo = new HashMap<String, String>();
				UniqueKey_PrimaryInfo.put("firstName", firstName);
				UniqueKey_PrimaryInfo.put("lastName", lastName);
				UniqueKey_PrimaryInfo.put("leadSource2ID", leadSourceCategory);
				UniqueKey_PrimaryInfo.put("leadSource3ID", leadSourceDetails);
				UniqueKey_PrimaryInfo.put("emailID", emailId);
				UniqueKey_PrimaryInfo.put("country", country);
				UniqueKey_PrimaryInfo.put("stateID", state);
				fc.utobj().printTestStep("Adding lead from Rest API");
				addLeadFromAllSourecesObj.addleadFromRestApi(driver, dataSet, testCaseId, UniqueKey_PrimaryInfo);

				driver.navigate().to(FranconnectUtil.config.get("buildUrl"));
				fc.loginpage().login(driver);
				fc.sales().sales_common().fsModule(driver);
				p3.searchByLeadName(driver, firstName, lastName);

				fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver,
						".//*[contains(text(),'" + firstName + " " + lastName + "')]"));

				fc.utobj().printTestStep("Verify a lead and get Lead Owner");
				isLeadOwnerPresent = fc.utobj().verifyElementOnVisible_ByXpath(driver,
						".//tr/td[contains(text(),'Lead Owner :')]/following-sibling::td[contains (text() ,'"
								+ userNameOwner + "')]");
				if (isLeadOwnerPresent == false) {
					fc.utobj().throwsException("As per Assignment Scheme owner is not assigned");
				}
			}
		}

		// End Here
		return actualOner;
	}
}