package com.builds.test.fs;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Test;

import com.builds.utilities.FranconnectUtil;
import com.builds.utilities.TestCase;

public class FSLeadOwnerByDivisonTest {

	FranconnectUtil fc = new FranconnectUtil();

	@Test(groups = { "salesLeadOwner" })
	@TestCase(createdOn = "2017-08-21", updatedOn = "2017-08-23", testCaseId = "TC_FC_Lead_Owner_Division_1", testCaseDescription = "Verify Lead Owner  by Division via System Lead with corp user corporate owner case.")
	private void leadOwnerByDivision_1() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			FSLeadOwnerAssignmentSchemeTest fsOwner = new FSLeadOwnerAssignmentSchemeTest();

			fsOwner.getOwnerByDivison(driver, testCaseId, "CorporateUser", "SystemLead", true, false); // corporate
																										// owner
																										// case

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "salesLeadOwner" })
	@TestCase(createdOn = "2017-08-21", updatedOn = "2017-08-23", testCaseId = "TC_FC_Lead_Owner_Division_2", testCaseDescription = "Verify Lead Owner  by Division via System Lead with regional user regional owner case.")
	private void leadOwnerByDivision_2() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			FSLeadOwnerAssignmentSchemeTest fsOwner = new FSLeadOwnerAssignmentSchemeTest();

			fsOwner.getOwnerByDivison(driver, testCaseId, "RegionalUser", "SystemLead", true, false); // regional
																										// owner
																										// case

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "salesLeadOwner" })
	@TestCase(createdOn = "2017-08-21", updatedOn = "2017-08-23", testCaseId = "TC_FC_Lead_Owner_Division_3", testCaseDescription = "Verify Lead Owner  by Division via System Lead with corp user default owner case.")
	private void leadOwnerByDivision_3() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			FSLeadOwnerAssignmentSchemeTest fsOwner = new FSLeadOwnerAssignmentSchemeTest();

			fsOwner.getOwnerByDivison(driver, testCaseId, "CorporateUser", "SystemLead", false, false); // regional
																										// owner
																										// case

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "salesLeadOwner" })
	@TestCase(createdOn = "2017-08-21", updatedOn = "2017-08-23", testCaseId = "TC_FC_Lead_Owner_Division_4", testCaseDescription = "Verify Lead Owner  by Division via System Lead with regional user default owner case.")
	private void leadOwnerByDivision_4() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			FSLeadOwnerAssignmentSchemeTest fsOwner = new FSLeadOwnerAssignmentSchemeTest();

			fsOwner.getOwnerByDivison(driver, testCaseId, "RegionalUser", "SystemLead", false, false); // regional
																										// owner
																										// case

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "salesLeadOwner" })
	@TestCase(createdOn = "2017-08-21", updatedOn = "2017-08-23", testCaseId = "TC_FC_Lead_Owner_Division_5", testCaseDescription = "Verify Lead Owner  by Division via System Lead with corp user default case.")
	private void leadOwnerByDivision_5() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			FSLeadOwnerAssignmentSchemeTest fsOwner = new FSLeadOwnerAssignmentSchemeTest();

			fsOwner.getOwnerByDivison(driver, testCaseId, "CorporateUser", "SystemLead", false, true); // defult
																										// case

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "salesLeadOwner" })
	@TestCase(createdOn = "2017-08-21", updatedOn = "2017-08-23", testCaseId = "TC_FC_Lead_Owner_Division_6", testCaseDescription = "Verify Lead Owner  by Division via System Lead with regional user default case.")
	private void leadOwnerByDivision_6() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			FSLeadOwnerAssignmentSchemeTest fsOwner = new FSLeadOwnerAssignmentSchemeTest();

			fsOwner.getOwnerByDivison(driver, testCaseId, "RegionalUser", "SystemLead", false, true); // regional
																										// case

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "salesLeadOwner" })
	@TestCase(createdOn = "2017-08-21", updatedOn = "2017-08-23", testCaseId = "TC_FC_Lead_Owner_Division_7", testCaseDescription = "Verify Lead Owner  by Division via Web form with corp user corporate owner case.")
	private void leadOwnerByDivision_7() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			FSLeadOwnerAssignmentSchemeTest fsOwner = new FSLeadOwnerAssignmentSchemeTest();

			fsOwner.getOwnerByDivison(driver, testCaseId, "CorporateUser", "WebForm", true, false); // corporate
																									// owner
																									// case

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "salesLeadOwner" })
	@TestCase(createdOn = "2017-08-21", updatedOn = "2017-08-23", testCaseId = "TC_FC_Lead_Owner_Division_8", testCaseDescription = "Verify Lead Owner  by Division via Web form with regional user regional owner case.")
	private void leadOwnerByDivision_8() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			FSLeadOwnerAssignmentSchemeTest fsOwner = new FSLeadOwnerAssignmentSchemeTest();

			fsOwner.getOwnerByDivison(driver, testCaseId, "RegionalUser", "WebForm", true, false); // regional
																									// owner
																									// case

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "salesLeadOwner" })
	@TestCase(createdOn = "2017-08-21", updatedOn = "2017-08-23", testCaseId = "TC_FC_Lead_Owner_Division_9", testCaseDescription = "Verify Lead Owner  by Division via Web form with corp user default owner case.")
	private void leadOwnerByDivision_9() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			FSLeadOwnerAssignmentSchemeTest fsOwner = new FSLeadOwnerAssignmentSchemeTest();

			fsOwner.getOwnerByDivison(driver, testCaseId, "CorporateUser", "WebForm", false, false); // regional
																										// owner
																										// case

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "salesLeadOwner" })
	@TestCase(createdOn = "2017-08-21", updatedOn = "2017-08-23", testCaseId = "TC_FC_Lead_Owner_Division_10", testCaseDescription = "Verify Lead Owner  by Division via Web form with regional user default owner case.")
	private void leadOwnerByDivision_10() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			FSLeadOwnerAssignmentSchemeTest fsOwner = new FSLeadOwnerAssignmentSchemeTest();

			fsOwner.getOwnerByDivison(driver, testCaseId, "RegionalUser", "WebForm", false, false); // regional
																									// owner
																									// case

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "salesLeadOwner" })
	@TestCase(createdOn = "2017-08-21", updatedOn = "2017-08-23", testCaseId = "TC_FC_Lead_Owner_Division_11", testCaseDescription = "Verify Lead Owner  by Division via Web form with corp user default case.")
	private void leadOwnerByDivision_11() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			FSLeadOwnerAssignmentSchemeTest fsOwner = new FSLeadOwnerAssignmentSchemeTest();

			fsOwner.getOwnerByDivison(driver, testCaseId, "CorporateUser", "WebForm", false, true); // defult
																									// case

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "salesLeadOwner" })
	@TestCase(createdOn = "2017-08-21", updatedOn = "2017-08-23", testCaseId = "TC_FC_Lead_Owner_Division_12", testCaseDescription = "Verify Lead Owner  by Division via Web form with regional user default case.")
	private void leadOwnerByDivision_12() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			FSLeadOwnerAssignmentSchemeTest fsOwner = new FSLeadOwnerAssignmentSchemeTest();

			fsOwner.getOwnerByDivison(driver, testCaseId, "RegionalUser", "WebForm", false, true); // regional
																									// case

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	//// Rest API

	@Test(groups = { "salesLeadOwner" })
	@TestCase(createdOn = "2017-08-21", updatedOn = "2017-08-23", testCaseId = "TC_FC_Lead_Owner_Division_13", testCaseDescription = "Verify Lead Owner  by Division via Rest API with corp user corporate owner case.")
	private void leadOwnerByDivision_13() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			FSLeadOwnerAssignmentSchemeTest fsOwner = new FSLeadOwnerAssignmentSchemeTest();

			fsOwner.getOwnerByDivison(driver, testCaseId, "CorporateUser", "RestAPI", true, false); // corporate
																									// owner
																									// case

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "salesLeadOwner" })
	@TestCase(createdOn = "2017-08-21", updatedOn = "2017-08-23", testCaseId = "TC_FC_Lead_Owner_Division_14", testCaseDescription = "Verify Lead Owner  by Division via Rest API with regional user regional owner case.")
	private void leadOwnerByDivision_14() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			FSLeadOwnerAssignmentSchemeTest fsOwner = new FSLeadOwnerAssignmentSchemeTest();

			fsOwner.getOwnerByDivison(driver, testCaseId, "RegionalUser", "RestAPI", true, false); // regional
																									// owner
																									// case

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "salesLeadOwner" })
	@TestCase(createdOn = "2017-08-21", updatedOn = "2017-08-23", testCaseId = "TC_FC_Lead_Owner_Division_15", testCaseDescription = "Verify Lead Owner  by Division via Rest API with corp user default owner case.")
	private void leadOwnerByDivision_15() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			FSLeadOwnerAssignmentSchemeTest fsOwner = new FSLeadOwnerAssignmentSchemeTest();

			fsOwner.getOwnerByDivison(driver, testCaseId, "CorporateUser", "RestAPI", false, false); // regional
																										// owner
																										// case

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "salesLeadOwner" })
	@TestCase(createdOn = "2017-08-21", updatedOn = "2017-08-23", testCaseId = "TC_FC_Lead_Owner_Division_16", testCaseDescription = "Verify Lead Owner  by Division via Rest API with regional user default owner case.")
	private void leadOwnerByDivision_16() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			FSLeadOwnerAssignmentSchemeTest fsOwner = new FSLeadOwnerAssignmentSchemeTest();

			fsOwner.getOwnerByDivison(driver, testCaseId, "RegionalUser", "RestAPI", false, false); // regional
																									// owner
																									// case

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "salesLeadOwner" })
	@TestCase(createdOn = "2017-08-21", updatedOn = "2017-08-23", testCaseId = "TC_FC_Lead_Owner_Division_17", testCaseDescription = "Verify Lead Owner  by Division via Rest API with corp user default case.")
	private void leadOwnerByDivision_17() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			FSLeadOwnerAssignmentSchemeTest fsOwner = new FSLeadOwnerAssignmentSchemeTest();

			fsOwner.getOwnerByDivison(driver, testCaseId, "CorporateUser", "RestAPI", false, true); // defult
																									// case

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "salesLeadOwner" })
	@TestCase(createdOn = "2017-08-21", updatedOn = "2017-08-23", testCaseId = "TC_FC_Lead_Owner_Division_18", testCaseDescription = "Verify Lead Owner  by Division via Rest API with regional user default case.")
	private void leadOwnerByDivision_18() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			FSLeadOwnerAssignmentSchemeTest fsOwner = new FSLeadOwnerAssignmentSchemeTest();

			fsOwner.getOwnerByDivison(driver, testCaseId, "RegionalUser", "RestAPI", false, true); // regional
																									// case

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "salesLeadOwner" })
	@TestCase(createdOn = "2017-08-21", updatedOn = "2017-08-23", testCaseId = "TC_FC_Lead_Owner_Division_19", testCaseDescription = "Verify Lead Owner  by Division via Import with corp user corporate owner case.")
	private void leadOwnerByDivision_19() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			FSLeadOwnerAssignmentSchemeTest fsOwner = new FSLeadOwnerAssignmentSchemeTest();

			fsOwner.getOwnerByDivison(driver, testCaseId, "CorporateUser", "Import", true, false); // corporate
																									// owner
																									// case

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "salesLeadOwner" })
	@TestCase(createdOn = "2017-08-21", updatedOn = "2017-08-23", testCaseId = "TC_FC_Lead_Owner_Division_20", testCaseDescription = "Verify Lead Owner  by Division via Import with regional user regional owner case.")
	private void leadOwnerByDivision_20() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			FSLeadOwnerAssignmentSchemeTest fsOwner = new FSLeadOwnerAssignmentSchemeTest();

			fsOwner.getOwnerByDivison(driver, testCaseId, "RegionalUser", "Import", true, false); // regional
																									// owner
																									// case

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "salesLeadOwner" })
	@TestCase(createdOn = "2017-08-21", updatedOn = "2017-08-23", testCaseId = "TC_FC_Lead_Owner_Division_21", testCaseDescription = "Verify Lead Owner  by Division via Import with corp user default owner case.")
	private void leadOwnerByDivision_21() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			FSLeadOwnerAssignmentSchemeTest fsOwner = new FSLeadOwnerAssignmentSchemeTest();

			fsOwner.getOwnerByDivison(driver, testCaseId, "CorporateUser", "Import", false, false); // regional
																									// owner
																									// case

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "salesLeadOwner" })
	@TestCase(createdOn = "2017-08-21", updatedOn = "2017-08-23", testCaseId = "TC_FC_Lead_Owner_Division_22", testCaseDescription = "Verify Lead Owner  by Division via Import with regional user default owner case.")
	private void leadOwnerByDivision_22() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			FSLeadOwnerAssignmentSchemeTest fsOwner = new FSLeadOwnerAssignmentSchemeTest();

			fsOwner.getOwnerByDivison(driver, testCaseId, "RegionalUser", "Import", false, false); // regional
																									// owner
																									// case

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "salesLeadOwner" })
	@TestCase(createdOn = "2017-08-21", updatedOn = "2017-08-23", testCaseId = "TC_FC_Lead_Owner_Division_23", testCaseDescription = "Verify Lead Owner  by Division via Import with corp user default case.")
	private void leadOwnerByDivision_23() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			FSLeadOwnerAssignmentSchemeTest fsOwner = new FSLeadOwnerAssignmentSchemeTest();

			fsOwner.getOwnerByDivison(driver, testCaseId, "CorporateUser", "Import", false, true); // defult
																									// case

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "salesLeadOwner" })
	@TestCase(createdOn = "2017-08-21", updatedOn = "2017-08-23", testCaseId = "TC_FC_Lead_Owner_Division_24", testCaseDescription = "Verify Lead Owner  by Division via Import with regional user default case.")
	private void leadOwnerByDivision_24() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			FSLeadOwnerAssignmentSchemeTest fsOwner = new FSLeadOwnerAssignmentSchemeTest();

			fsOwner.getOwnerByDivison(driver, testCaseId, "RegionalUser", "Import", false, true); // regional
																									// case

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

}