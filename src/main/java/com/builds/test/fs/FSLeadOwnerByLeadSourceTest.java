package com.builds.test.fs;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Test;

import com.builds.utilities.FranconnectUtil;
import com.builds.utilities.TestCase;

public class FSLeadOwnerByLeadSourceTest {

	FranconnectUtil fc = new FranconnectUtil();

	@Test(groups = { "salesLeadOwner" })
	@TestCase(createdOn = "2017-08-21", updatedOn = "2017-08-22", testCaseId = "TC_FC_Lead_Owner_LeadSource_1", testCaseDescription = "Verify Lead Owner by Lead Source via System Lead with corp user corporate owner case.")
	private void leadOwnerLeadSource_1() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			FSLeadOwnerAssignmentSchemeTest fsOwner = new FSLeadOwnerAssignmentSchemeTest();

			fsOwner.getOwnerLeadBySource(driver, testCaseId, "CorporateUser", "SystemLead", true, false); // corporate
																											// owner
																											// case

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "salesLeadOwner" })
	@TestCase(createdOn = "2017-08-21", updatedOn = "2017-08-22", testCaseId = "TC_FC_Lead_Owner_LeadSource_2", testCaseDescription = "Verify Lead Owner by Lead Source via System Lead with regional user regional owner case.")
	private void leadOwnerLeadSource_2() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			FSLeadOwnerAssignmentSchemeTest fsOwner = new FSLeadOwnerAssignmentSchemeTest();

			fsOwner.getOwnerLeadBySource(driver, testCaseId, "RegionalUser", "SystemLead", true, false); // regional
																											// owner
																											// case

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "salesLeadOwner" })
	@TestCase(createdOn = "2017-08-21", updatedOn = "2017-08-22", testCaseId = "TC_FC_Lead_Owner_LeadSource_3", testCaseDescription = "Verify Lead Owner by Lead Source via System Lead with corp user default owner case.")
	private void leadOwnerLeadSource_3() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			FSLeadOwnerAssignmentSchemeTest fsOwner = new FSLeadOwnerAssignmentSchemeTest();

			fsOwner.getOwnerLeadBySource(driver, testCaseId, "CorporateUser", "SystemLead", false, false); // regional
																											// owner
																											// case

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "salesLeadOwner" })
	@TestCase(createdOn = "2017-08-21", updatedOn = "2017-08-22", testCaseId = "TC_FC_Lead_Owner_LeadSource_4", testCaseDescription = "Verify Lead Owner by Lead Source via System Lead with regional user default owner case.")
	private void leadOwnerLeadSource_4() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			FSLeadOwnerAssignmentSchemeTest fsOwner = new FSLeadOwnerAssignmentSchemeTest();

			fsOwner.getOwnerLeadBySource(driver, testCaseId, "RegionalUser", "SystemLead", false, false); // regional
																											// owner
																											// case

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "salesLeadOwner1" })
	@TestCase(createdOn = "2017-08-21", updatedOn = "2017-08-22", testCaseId = "TC_FC_Lead_Owner_LeadSource_5", testCaseDescription = "Verify Lead Owner by Lead Source via System Lead with corp user default case.")
	private void leadOwnerLeadSource_5() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			FSLeadOwnerAssignmentSchemeTest fsOwner = new FSLeadOwnerAssignmentSchemeTest();

			fsOwner.getOwnerLeadBySource(driver, testCaseId, "CorporateUser", "SystemLead", false, true); // defult
																											// case

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "salesLeadOwner" })
	@TestCase(createdOn = "2017-08-21", updatedOn = "2017-08-22", testCaseId = "TC_FC_Lead_Owner_LeadSource_6", testCaseDescription = "Verify Lead Owner by Lead Source via System Lead with regional user default case.")
	private void leadOwnerLeadSource_6() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			FSLeadOwnerAssignmentSchemeTest fsOwner = new FSLeadOwnerAssignmentSchemeTest();

			fsOwner.getOwnerLeadBySource(driver, testCaseId, "RegionalUser", "SystemLead", false, true); // regional
																											// case

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "salesLeadOwner" })
	@TestCase(createdOn = "2017-08-21", updatedOn = "2017-08-22", testCaseId = "TC_FC_Lead_Owner_LeadSource_7", testCaseDescription = "Verify Lead Owner by Lead Source via Web form with corp user corporate owner case.")
	private void leadOwnerLeadSource_7() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			FSLeadOwnerAssignmentSchemeTest fsOwner = new FSLeadOwnerAssignmentSchemeTest();

			fsOwner.getOwnerLeadBySource(driver, testCaseId, "CorporateUser", "WebForm", true, false); // corporate
																										// owner
																										// case

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "salesLeadOwner" })
	@TestCase(createdOn = "2017-08-21", updatedOn = "2017-08-22", testCaseId = "TC_FC_Lead_Owner_LeadSource_8", testCaseDescription = "Verify Lead Owner by Lead Source via Web form with regional user regional owner case.")
	private void leadOwnerLeadSource_8() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			FSLeadOwnerAssignmentSchemeTest fsOwner = new FSLeadOwnerAssignmentSchemeTest();

			fsOwner.getOwnerLeadBySource(driver, testCaseId, "RegionalUser", "WebForm", true, false); // regional
																										// owner
																										// case

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "salesLeadOwner" })
	@TestCase(createdOn = "2017-08-21", updatedOn = "2017-08-22", testCaseId = "TC_FC_Lead_Owner_LeadSource_9", testCaseDescription = "Verify Lead Owner by Lead Source via Web form with corp user default owner case.")
	private void leadOwnerLeadSource_9() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			FSLeadOwnerAssignmentSchemeTest fsOwner = new FSLeadOwnerAssignmentSchemeTest();

			fsOwner.getOwnerLeadBySource(driver, testCaseId, "CorporateUser", "WebForm", false, false); // regional
																										// owner
																										// case

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "salesLeadOwner" })
	@TestCase(createdOn = "2017-08-21", updatedOn = "2017-08-22", testCaseId = "TC_FC_Lead_Owner_LeadSource_10", testCaseDescription = "Verify Lead Owner by Lead Source via Web form with regional user default owner case.")
	private void leadOwnerLeadSource_10() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			FSLeadOwnerAssignmentSchemeTest fsOwner = new FSLeadOwnerAssignmentSchemeTest();

			fsOwner.getOwnerLeadBySource(driver, testCaseId, "RegionalUser", "WebForm", false, false); // regional
																										// owner
																										// case

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "salesLeadOwner1" })
	@TestCase(createdOn = "2017-08-21", updatedOn = "2017-08-22", testCaseId = "TC_FC_Lead_Owner_LeadSource_11", testCaseDescription = "Verify Lead Owner by Lead Source via Web form with corp user default case.")
	private void leadOwnerLeadSource_11() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			FSLeadOwnerAssignmentSchemeTest fsOwner = new FSLeadOwnerAssignmentSchemeTest();

			fsOwner.getOwnerLeadBySource(driver, testCaseId, "CorporateUser", "WebForm", false, true); // defult
																										// case

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "salesLeadOwner" })
	@TestCase(createdOn = "2017-08-21", updatedOn = "2017-08-22", testCaseId = "TC_FC_Lead_Owner_LeadSource_12", testCaseDescription = "Verify Lead Owner by Lead Source via Web form with regional user default case.")
	private void leadOwnerLeadSource_12() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			FSLeadOwnerAssignmentSchemeTest fsOwner = new FSLeadOwnerAssignmentSchemeTest();

			fsOwner.getOwnerLeadBySource(driver, testCaseId, "RegionalUser", "WebForm", false, true); // regional
																										// case

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	//// Rest API

	@Test(groups = { "salesLeadOwner" })
	@TestCase(createdOn = "2017-08-21", updatedOn = "2017-08-22", testCaseId = "TC_FC_Lead_Owner_LeadSource_13", testCaseDescription = "Verify Lead Owner by Lead Source via Rest API with corp user corporate owner case.")
	private void leadOwnerLeadSource_13() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			FSLeadOwnerAssignmentSchemeTest fsOwner = new FSLeadOwnerAssignmentSchemeTest();

			fsOwner.getOwnerLeadBySource(driver, testCaseId, "CorporateUser", "RestAPI", true, false); // corporate
																										// owner
																										// case

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "salesLeadOwner" })
	@TestCase(createdOn = "2017-08-21", updatedOn = "2017-08-22", testCaseId = "TC_FC_Lead_Owner_LeadSource_14", testCaseDescription = "Verify Lead Owner by Lead Source via Rest API with regional user regional owner case.")
	private void leadOwnerLeadSource_14() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			FSLeadOwnerAssignmentSchemeTest fsOwner = new FSLeadOwnerAssignmentSchemeTest();

			fsOwner.getOwnerLeadBySource(driver, testCaseId, "RegionalUser", "RestAPI", true, false); // regional
																										// owner
																										// case

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "salesLeadOwner" })
	@TestCase(createdOn = "2017-08-21", updatedOn = "2017-08-22", testCaseId = "TC_FC_Lead_Owner_LeadSource_15", testCaseDescription = "Verify Lead Owner by Lead Source via Rest API with corp user default owner case.")
	private void leadOwnerLeadSource_15() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			FSLeadOwnerAssignmentSchemeTest fsOwner = new FSLeadOwnerAssignmentSchemeTest();

			fsOwner.getOwnerLeadBySource(driver, testCaseId, "CorporateUser", "RestAPI", false, false); // regional
																										// owner
																										// case

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "salesLeadOwner" })
	@TestCase(createdOn = "2017-08-21", updatedOn = "2017-08-22", testCaseId = "TC_FC_Lead_Owner_LeadSource_16", testCaseDescription = "Verify Lead Owner by Lead Source via Rest API with regional user default owner case.")
	private void leadOwnerLeadSource_16() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			FSLeadOwnerAssignmentSchemeTest fsOwner = new FSLeadOwnerAssignmentSchemeTest();

			fsOwner.getOwnerLeadBySource(driver, testCaseId, "RegionalUser", "RestAPI", false, false); // regional
																										// owner
																										// case

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "salesLeadOwner1" })
	@TestCase(createdOn = "2017-08-21", updatedOn = "2017-08-22", testCaseId = "TC_FC_Lead_Owner_LeadSource_17", testCaseDescription = "Verify Lead Owner by Lead Source via Rest API with corp user default case.")
	private void leadOwnerLeadSource_17() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			FSLeadOwnerAssignmentSchemeTest fsOwner = new FSLeadOwnerAssignmentSchemeTest();

			fsOwner.getOwnerLeadBySource(driver, testCaseId, "CorporateUser", "RestAPI", false, true); // defult
																										// case

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "salesLeadOwner" })
	@TestCase(createdOn = "2017-08-21", updatedOn = "2017-08-22", testCaseId = "TC_FC_Lead_Owner_LeadSource_18", testCaseDescription = "Verify Lead Owner by Lead Source via Rest API with regional user default case.")
	private void leadOwnerLeadSource_18() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			FSLeadOwnerAssignmentSchemeTest fsOwner = new FSLeadOwnerAssignmentSchemeTest();

			fsOwner.getOwnerLeadBySource(driver, testCaseId, "RegionalUser", "RestAPI", false, true); // regional
																										// case

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "salesLeadOwner34" })
	@TestCase(createdOn = "2017-08-21", updatedOn = "2017-08-22", testCaseId = "TC_FC_Lead_Owner_LeadSource_19", testCaseDescription = "Verify Lead Owner by Lead Source via Import with corp user corporate owner case.")
	private void leadOwnerLeadSource_19() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			FSLeadOwnerAssignmentSchemeTest fsOwner = new FSLeadOwnerAssignmentSchemeTest();

			fsOwner.getOwnerLeadBySource(driver, testCaseId, "CorporateUser", "Import", true, false); // corporate
																										// owner
																										// case

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "salesLeadOwner" })
	@TestCase(createdOn = "2017-08-21", updatedOn = "2017-08-22", testCaseId = "TC_FC_Lead_Owner_LeadSource_20", testCaseDescription = "Verify Lead Owner by Lead Source via Import with regional user regional owner case.")
	private void leadOwnerLeadSource_20() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			FSLeadOwnerAssignmentSchemeTest fsOwner = new FSLeadOwnerAssignmentSchemeTest();

			fsOwner.getOwnerLeadBySource(driver, testCaseId, "RegionalUser", "Import", true, false); // regional
																										// owner
																										// case

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "salesLeadOwner" })
	@TestCase(createdOn = "2017-08-21", updatedOn = "2017-08-22", testCaseId = "TC_FC_Lead_Owner_LeadSource_21", testCaseDescription = "Verify Lead Owner by Lead Source via Import with corp user default owner case.")
	private void leadOwnerLeadSource_21() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			FSLeadOwnerAssignmentSchemeTest fsOwner = new FSLeadOwnerAssignmentSchemeTest();

			fsOwner.getOwnerLeadBySource(driver, testCaseId, "CorporateUser", "Import", false, false); // regional
																										// owner
																										// case

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "salesLeadOwner" })
	@TestCase(createdOn = "2017-08-21", updatedOn = "2017-08-22", testCaseId = "TC_FC_Lead_Owner_LeadSource_22", testCaseDescription = "Verify Lead Owner by Lead Source via Import with regional user default owner case.")
	private void leadOwnerLeadSource_22() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			FSLeadOwnerAssignmentSchemeTest fsOwner = new FSLeadOwnerAssignmentSchemeTest();

			fsOwner.getOwnerLeadBySource(driver, testCaseId, "RegionalUser", "Import", false, false); // regional
																										// owner
																										// case

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "salesLeadOwner" })
	@TestCase(createdOn = "2017-08-21", updatedOn = "2017-08-22", testCaseId = "TC_FC_Lead_Owner_LeadSource_23", testCaseDescription = "Verify Lead Owner by Lead Source via Import with corp user default case.")
	private void leadOwnerLeadSource_23() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			FSLeadOwnerAssignmentSchemeTest fsOwner = new FSLeadOwnerAssignmentSchemeTest();

			fsOwner.getOwnerLeadBySource(driver, testCaseId, "CorporateUser", "Import", false, true); // defult
																										// case

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "salesLeadOwner" })
	@TestCase(createdOn = "2017-08-21", updatedOn = "2017-08-22", testCaseId = "TC_FC_Lead_Owner_LeadSource_24", testCaseDescription = "Verify Lead Owner by Lead Source via Import with regional user default case.")
	private void leadOwnerLeadSource_24() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			FSLeadOwnerAssignmentSchemeTest fsOwner = new FSLeadOwnerAssignmentSchemeTest();

			fsOwner.getOwnerLeadBySource(driver, testCaseId, "RegionalUser", "Import", false, true); // regional
																										// case

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

}