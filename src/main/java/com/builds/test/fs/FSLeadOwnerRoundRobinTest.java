package com.builds.test.fs;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Test;

import com.builds.utilities.FranconnectUtil;
import com.builds.utilities.TestCase;

public class FSLeadOwnerRoundRobinTest {
	FranconnectUtil fc = new FranconnectUtil();

	@Test(groups = { "salesLeadOwner" })
	@TestCase(createdOn = "2017-08-21", updatedOn = "2017-08-21", testCaseId = "TC_FC_Lead_Owner_RoundRobin_1", testCaseDescription = "Verify Lead Owner Round Robin via System Lead with corp user.")
	private void leadOwnerRoundRobin_1() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			FSLeadOwnerAssignmentSchemeTest fsOwner = new FSLeadOwnerAssignmentSchemeTest();

			fsOwner.getRoundRobinOwner(driver, testCaseId, "CorporateUser", "SystemLead", true, false);// corporate
																										// owner
																										// case
																										// with
																										// round
																										// robin
																										// setting

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "salesLeadOwner" })
	@TestCase(createdOn = "2017-08-21", updatedOn = "2017-08-21", testCaseId = "TC_FC_Lead_Owner_RoundRobin_2", testCaseDescription = "Verify Lead Owner Round Robin via System Lead with regional user.")
	private void leadOwnerRoundRobin_2() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			FSLeadOwnerAssignmentSchemeTest fsOwner = new FSLeadOwnerAssignmentSchemeTest();

			fsOwner.getRoundRobinOwner(driver, testCaseId, "RegionalUser", "SystemLead", true, false);// regional
																										// owner
																										// case
																										// with
																										// round
																										// robin
																										// setting

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "salesLeadOwner" })
	@TestCase(createdOn = "2017-08-21", updatedOn = "2017-08-21", testCaseId = "TC_FC_Lead_Owner_RoundRobin_3", testCaseDescription = "Verify Lead Owner Round Robin via System Lead corporate with default owner.")
	private void leadOwnerRoundRobin_3() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			FSLeadOwnerAssignmentSchemeTest fsOwner = new FSLeadOwnerAssignmentSchemeTest();

			fsOwner.getRoundRobinOwner(driver, testCaseId, "CorporateUser", "SystemLead", false, false);// corporate
																										// owner
																										// case
																										// with
																										// out
																										// round
																										// robin
																										// setting
																										// assigned
																										// to
																										// default
																										// owner

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "salesLeadOwner" })
	@TestCase(createdOn = "2017-08-21", updatedOn = "2017-08-21", testCaseId = "TC_FC_Lead_Owner_RoundRobin_4", testCaseDescription = "Verify Lead Owner Round Robin via System Lead regional with default owner.")
	private void leadOwnerRoundRobin_4() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			FSLeadOwnerAssignmentSchemeTest fsOwner = new FSLeadOwnerAssignmentSchemeTest();

			fsOwner.getRoundRobinOwner(driver, testCaseId, "RegionalUser", "SystemLead", false, false); // regional
																										// owner
																										// case
																										// with
																										// out
																										// round
																										// robin
																										// setting
																										// assigned
																										// to
																										// default
																										// owner

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "salesLeadOwner" })
	@TestCase(createdOn = "2017-08-21", updatedOn = "2017-08-21", testCaseId = "TC_FC_Lead_Owner_RoundRobin_5", testCaseDescription = "Verify Lead Owner Round Robin via System Lead corporate with default case.")
	private void leadOwnerRoundRobin_5() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			FSLeadOwnerAssignmentSchemeTest fsOwner = new FSLeadOwnerAssignmentSchemeTest();

			fsOwner.getRoundRobinOwner(driver, testCaseId, "CorporateUser", "SystemLead", false, true); // corporate
																										// owner
																										// No
																										// owner
																										// Set
																										// in
																										// default
																										// case.

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "salesLeadOwner" })
	@TestCase(createdOn = "2017-08-21", updatedOn = "2017-08-21", testCaseId = "TC_FC_Lead_Owner_RoundRobin_6", testCaseDescription = "Verify Lead Owner Round Robin via System Lead regional with default case.")
	private void leadOwnerRoundRobin_6() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			FSLeadOwnerAssignmentSchemeTest fsOwner = new FSLeadOwnerAssignmentSchemeTest();

			fsOwner.getRoundRobinOwner(driver, testCaseId, "RegionalUser", "SystemLead", false, true); // regional
																										// owner
																										// No
																										// owner
																										// Set
																										// in
																										// default
																										// case.

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "salesLeadOwner" })
	@TestCase(createdOn = "2017-08-21", updatedOn = "2017-08-21", testCaseId = "TC_FC_Lead_Owner_RoundRobin_7", testCaseDescription = "Verify Lead Owner Round Robin via web form corp with default case.")
	private void leadOwnerRoundRobin_7() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			FSLeadOwnerAssignmentSchemeTest fsOwner = new FSLeadOwnerAssignmentSchemeTest();

			fsOwner.getRoundRobinOwner(driver, testCaseId, "CorporateUser", "WebForm", false, true); // corporate
																										// owner
																										// No
																										// owner
																										// Set
																										// in
																										// default
																										// owner
																										// case
																										// through
																										// web
																										// form.

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "salesLeadOwner" })
	@TestCase(createdOn = "2017-08-21", updatedOn = "2017-08-21", testCaseId = "TC_FC_Lead_Owner_RoundRobin_8", testCaseDescription = "Verify Lead Owner Round Robin via web form regional with default case.")
	private void leadOwnerRoundRobin_8() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			FSLeadOwnerAssignmentSchemeTest fsOwner = new FSLeadOwnerAssignmentSchemeTest();

			fsOwner.getRoundRobinOwner(driver, testCaseId, "RegionalUser", "WebForm", false, true); // regional
																									// owner
																									// No
																									// owner
																									// Set
																									// in
																									// default
																									// owner
																									// case
																									// through
																									// web
																									// form.

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "salesLeadOwner" })
	@TestCase(createdOn = "2017-08-21", updatedOn = "2017-08-21", testCaseId = "TC_FC_Lead_Owner_RoundRobin_9", testCaseDescription = "Verify Lead Owner Round Robin via web form corp with default owner.")
	private void leadOwnerRoundRobin_9() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			FSLeadOwnerAssignmentSchemeTest fsOwner = new FSLeadOwnerAssignmentSchemeTest();

			fsOwner.getRoundRobinOwner(driver, testCaseId, "CorporateUser", "WebForm", false, false); // corporate
																										// owner
																										// case
																										// with
																										// out
																										// round
																										// robin
																										// setting
																										// assigned
																										// to
																										// default
																										// owner

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "salesLeadOwner" })
	@TestCase(createdOn = "2017-08-21", updatedOn = "2017-08-21", testCaseId = "TC_FC_Lead_Owner_RoundRobin_10", testCaseDescription = "Verify Lead Owner Round Robin via web form regional with default owner.")
	private void leadOwnerRoundRobin_10() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			FSLeadOwnerAssignmentSchemeTest fsOwner = new FSLeadOwnerAssignmentSchemeTest();

			fsOwner.getRoundRobinOwner(driver, testCaseId, "RegionalUser", "WebForm", false, false); // regional
																										// owner
																										// case
																										// with
																										// out
																										// round
																										// robin
																										// setting
																										// assigned
																										// to
																										// default
																										// owner

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "salesLeadOwner" })
	@TestCase(createdOn = "2017-08-21", updatedOn = "2017-08-21", testCaseId = "TC_FC_Lead_Owner_RoundRobin_11", testCaseDescription = "Verify Lead Owner Round Robin via web form corp with corporate owner.")
	private void leadOwnerRoundRobin_11() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			FSLeadOwnerAssignmentSchemeTest fsOwner = new FSLeadOwnerAssignmentSchemeTest();

			fsOwner.getRoundRobinOwner(driver, testCaseId, "CorporateUser", "WebForm", true, false);// corporate
																									// owner
																									// case
																									// with
																									// round
																									// robin
																									// setting

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "salesLeadOwner" })
	@TestCase(createdOn = "2017-08-21", updatedOn = "2017-08-21", testCaseId = "TC_FC_Lead_Owner_RoundRobin_12", testCaseDescription = "Verify Lead Owner Round Robin via web form regional with regional owner.")
	private void leadOwnerRoundRobin_12() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			FSLeadOwnerAssignmentSchemeTest fsOwner = new FSLeadOwnerAssignmentSchemeTest();

			fsOwner.getRoundRobinOwner(driver, testCaseId, "RegionalUser", "WebForm", true, false);// RegionalUser
																									// owner
																									// case
																									// with
																									// round
																									// robin
																									// setting

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "salesLeadOwner" })
	@TestCase(createdOn = "2017-08-21", updatedOn = "2017-08-21", testCaseId = "TC_FC_Lead_Owner_RoundRobin_13", testCaseDescription = "Verify Lead Owner Round Robin via RestAPI corp case with No owner Set.")
	private void leadOwnerRoundRobin_13() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			FSLeadOwnerAssignmentSchemeTest fsOwner = new FSLeadOwnerAssignmentSchemeTest();

			fsOwner.getRoundRobinOwner(driver, testCaseId, "CorporateUser", "RestAPI", false, true); // corporate
																										// owner
																										// No
																										// owner
																										// Set
																										// in
																										// default
																										// owner
																										// case
																										// through
																										// web
																										// form.

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "salesLeadOwner" })
	@TestCase(createdOn = "2017-08-21", updatedOn = "2017-08-21", testCaseId = "TC_FC_Lead_Owner_RoundRobin_14", testCaseDescription = "Verify Lead Owner Round Robin via RestAPI regional case with No owner Set.")
	private void leadOwnerRoundRobin_14() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			FSLeadOwnerAssignmentSchemeTest fsOwner = new FSLeadOwnerAssignmentSchemeTest();

			fsOwner.getRoundRobinOwner(driver, testCaseId, "RegionalUser", "RestAPI", false, true); // regional
																									// owner
																									// No
																									// owner
																									// Set
																									// in
																									// default
																									// owner
																									// case
																									// through
																									// web
																									// form.

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "salesLeadOwner" })
	@TestCase(createdOn = "2017-08-21", updatedOn = "2017-08-21", testCaseId = "TC_FC_Lead_Owner_RoundRobin_15", testCaseDescription = "Verify Lead Owner Round Robin via RestAPI corp case with default owner.")
	private void leadOwnerRoundRobin_15() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			FSLeadOwnerAssignmentSchemeTest fsOwner = new FSLeadOwnerAssignmentSchemeTest();

			fsOwner.getRoundRobinOwner(driver, testCaseId, "CorporateUser", "RestAPI", false, false); // corporate
																										// owner
																										// case
																										// with
																										// out
																										// round
																										// robin
																										// setting
																										// assigned
																										// to
																										// default
																										// owner

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "salesLeadOwner" })
	@TestCase(createdOn = "2017-08-21", updatedOn = "2017-08-21", testCaseId = "TC_FC_Lead_Owner_RoundRobin_16", testCaseDescription = "Verify Lead Owner Round Robin via RestAPI regional case with default owner.")
	private void leadOwnerRoundRobin_16() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			FSLeadOwnerAssignmentSchemeTest fsOwner = new FSLeadOwnerAssignmentSchemeTest();

			fsOwner.getRoundRobinOwner(driver, testCaseId, "RegionalUser", "RestAPI", false, false); // regional
																										// owner
																										// case
																										// with
																										// out
																										// round
																										// robin
																										// setting
																										// assigned
																										// to
																										// default
																										// owner

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "salesLeadOwner" })
	@TestCase(createdOn = "2017-08-21", updatedOn = "2017-08-21", testCaseId = "TC_FC_Lead_Owner_RoundRobin_17", testCaseDescription = "Verify Lead Owner Round Robin via RestAPI corp case with corporate owner.")
	private void leadOwnerRoundRobin_17() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			FSLeadOwnerAssignmentSchemeTest fsOwner = new FSLeadOwnerAssignmentSchemeTest();

			fsOwner.getRoundRobinOwner(driver, testCaseId, "CorporateUser", "RestAPI", true, false);// corporate
																									// owner
																									// case
																									// with
																									// round
																									// robin
																									// setting

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "salesLeadOwner" })
	@TestCase(createdOn = "2017-08-21", updatedOn = "2017-08-21", testCaseId = "TC_FC_Lead_Owner_RoundRobin_18", testCaseDescription = "Verify Lead Owner Round Robin via RestAPI regioanl case with regional owner.")
	private void leadOwnerRoundRobin_18() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			FSLeadOwnerAssignmentSchemeTest fsOwner = new FSLeadOwnerAssignmentSchemeTest();

			fsOwner.getRoundRobinOwner(driver, testCaseId, "RegionalUser", "RestAPI", true, false); // regional
																									// owner
																									// case
																									// with
																									// out
																									// round
																									// robin
																									// setting
																									// assigned
																									// to
																									// default
																									// owner

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "salesLeadOwner" })
	@TestCase(createdOn = "2017-08-24", updatedOn = "2017-08-24", testCaseId = "TC_FC_Lead_Owner_RoundRobin_19", testCaseDescription = "Verify Lead Owner Round Robin via Import with corp user.")
	private void leadOwnerRoundRobin_19() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			FSLeadOwnerAssignmentSchemeTest fsOwner = new FSLeadOwnerAssignmentSchemeTest();

			fsOwner.getRoundRobinOwner(driver, testCaseId, "CorporateUser", "Import", true, false);// corporate
																									// owner
																									// case
																									// with
																									// round
																									// robin
																									// setting

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "salesLeadOwner" })
	@TestCase(createdOn = "2017-08-21", updatedOn = "2017-08-21", testCaseId = "TC_FC_Lead_Owner_RoundRobin_20", testCaseDescription = "Verify Lead Owner Round Robin via Import with regional user.")
	private void leadOwnerRoundRobin_20() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			FSLeadOwnerAssignmentSchemeTest fsOwner = new FSLeadOwnerAssignmentSchemeTest();

			fsOwner.getRoundRobinOwner(driver, testCaseId, "RegionalUser", "Import", true, false);// regional
																									// owner
																									// case
																									// with
																									// round
																									// robin
																									// setting

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "salesLeadOwner" })
	@TestCase(createdOn = "2017-08-21", updatedOn = "2017-08-21", testCaseId = "TC_FC_Lead_Owner_RoundRobin_21", testCaseDescription = "Verify Lead Owner Round Robin via Import Lead corporate with default owner.")
	private void leadOwnerRoundRobin_21() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			FSLeadOwnerAssignmentSchemeTest fsOwner = new FSLeadOwnerAssignmentSchemeTest();

			fsOwner.getRoundRobinOwner(driver, testCaseId, "CorporateUser", "Import", false, false);// corporate
																									// owner
																									// case
																									// with
																									// out
																									// round
																									// robin
																									// setting
																									// assigned
																									// to
																									// default
																									// owner

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "salesLeadOwner" })
	@TestCase(createdOn = "2017-08-21", updatedOn = "2017-08-21", testCaseId = "TC_FC_Lead_Owner_RoundRobin_22", testCaseDescription = "Verify Lead Owner Round Robin via Import Lead regional with default owner.")
	private void leadOwnerRoundRobin_22() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			FSLeadOwnerAssignmentSchemeTest fsOwner = new FSLeadOwnerAssignmentSchemeTest();

			fsOwner.getRoundRobinOwner(driver, testCaseId, "RegionalUser", "Import", false, false); // regional
																									// owner
																									// case
																									// with
																									// out
																									// round
																									// robin
																									// setting
																									// assigned
																									// to
																									// default
																									// owner

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "salesLeadOwner" })
	@TestCase(createdOn = "2017-08-21", updatedOn = "2017-08-21", testCaseId = "TC_FC_Lead_Owner_RoundRobin_23", testCaseDescription = "Verify Lead Owner Round Robin via Import Lead corporate with default case.")
	private void leadOwnerRoundRobin_23() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			FSLeadOwnerAssignmentSchemeTest fsOwner = new FSLeadOwnerAssignmentSchemeTest();

			fsOwner.getRoundRobinOwner(driver, testCaseId, "CorporateUser", "Import", false, true); // corporate
																									// owner
																									// No
																									// owner
																									// Set
																									// in
																									// default
																									// case.

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "salesLeadOwner" })
	@TestCase(createdOn = "2017-08-21", updatedOn = "2017-08-21", testCaseId = "TC_FC_Lead_Owner_RoundRobin_24", testCaseDescription = "Verify Lead Owner Round Robin via Import Lead regional with default case.")
	private void leadOwnerRoundRobin_24() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			FSLeadOwnerAssignmentSchemeTest fsOwner = new FSLeadOwnerAssignmentSchemeTest();

			fsOwner.getRoundRobinOwner(driver, testCaseId, "RegionalUser", "Import", false, true); // regional
																									// owner
																									// No
																									// owner
																									// Set
																									// in
																									// default
																									// case.

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

}