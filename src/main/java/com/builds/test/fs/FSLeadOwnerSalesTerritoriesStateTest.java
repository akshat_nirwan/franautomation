package com.builds.test.fs;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import com.builds.utilities.FranconnectUtil;
import com.builds.utilities.TestCase;

public class FSLeadOwnerSalesTerritoriesStateTest {
	FranconnectUtil fc = new FranconnectUtil();

	@Test(groups = { "salesLeadOwner", "saleTestTest" })
	@TestCase(createdOn = "2017-08-21", updatedOn = "2017-08-22", testCaseId = "TC_FC_Lead_Owner_SalesTerritories_State_1", testCaseDescription = "Verify Lead Owner Sales Territories with state basis via System Lead with corp user corporate owner case.")
	private void leadOwnerSalesTerritoriesState_1() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			fc.sales().sales_common().adminpage().openSalesTerritoryPage(driver);

			fc.utobj().getElementByXpath(driver,".//*[@id='siteMainTable']/tbody/tr/td/form[2]/table/tbody/tr[7]/td[2]/table/tbody/tr[1]/td/table/tbody/tr/td[2]/input")
					.click();
			fc.utobj().clickElement(driver, fc.utobj().getElementByXpath(driver, ".//*[@id='all']"));

			List<WebElement> listElement = driver.findElements(By.xpath(".//input[@name='stateId']/parent::*"));
			WebElement ele = null;

			for (WebElement webElement : listElement) {

				if (webElement.getText().trim().equalsIgnoreCase("Oklahoma")) {
					ele = webElement.findElement(By.xpath("./input[@name='stateId']"));
				}
			}

			fc.utobj().clickElement(driver, ele);

			/*
			 * FSLeadOwnerAssignmentSchemeTest fsOwner = new
			 * FSLeadOwnerAssignmentSchemeTest();
			 * 
			 * fsOwner.getSalesTerritoriesOwnerState(driver,testCaseId,
			 * "CorporateUser","SystemLead",true,false);// corporate owner case
			 * with round robin setting // ok
			 */
			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "salesLeadOwner" })
	@TestCase(createdOn = "2017-08-21", updatedOn = "2017-08-22", testCaseId = "TC_FC_Lead_Owner_SalesTerritories_State_2", testCaseDescription = "Verify Lead Owner Sales Territories with state basis via System Lead with regional user regional owner case.")
	private void leadOwnerSalesTerritoriesState_2() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			FSLeadOwnerAssignmentSchemeTest fsOwner = new FSLeadOwnerAssignmentSchemeTest();

			fsOwner.getSalesTerritoriesOwnerState(driver, testCaseId, "RegionalUser", "SystemLead", true, false);// regional
																													// owner
																													// case
																													// with
																													// round
																													// robin
																													// setting

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "salesLeadOwner" })
	@TestCase(createdOn = "2017-08-21", updatedOn = "2017-08-22", testCaseId = "TC_FC_Lead_Owner_SalesTerritories_State_3", testCaseDescription = "Verify Lead Owner Sales Territories with state basis via System Lead with corp user default owner case.")
	private void leadOwnerSalesTerritoriesState_3() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			FSLeadOwnerAssignmentSchemeTest fsOwner = new FSLeadOwnerAssignmentSchemeTest();

			fsOwner.getSalesTerritoriesOwnerState(driver, testCaseId, "CorporateUser", "SystemLead", false, false);// corporate
																													// owner
																													// case
																													// with
																													// out
																													// SalesTerritories
																													// setting
																													// assigned
																													// to
																													// default
																													// owner
																													// //
																													// ok

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "salesLeadOwner" })
	@TestCase(createdOn = "2017-08-21", updatedOn = "2017-08-22", testCaseId = "TC_FC_Lead_Owner_SalesTerritories_State_4", testCaseDescription = "Verify Lead Owner Sales Territories with state basis via System Lead with regional user default owner case.")
	private void leadOwnerSalesTerritoriesState_4() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			FSLeadOwnerAssignmentSchemeTest fsOwner = new FSLeadOwnerAssignmentSchemeTest();

			fsOwner.getSalesTerritoriesOwnerState(driver, testCaseId, "RegionalUser", "SystemLead", false, false); // regional
																													// owner
																													// case
																													// with
																													// out
																													// round
																													// robin
																													// setting
																													// assigned
																													// to
																													// default
																													// owner

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "salesLeadOwner" })
	@TestCase(createdOn = "2017-08-21", updatedOn = "2017-08-22", testCaseId = "TC_FC_Lead_Owner_SalesTerritories_State_5", testCaseDescription = "Verify Lead Owner Sales Territories with state basis via System Lead with corp user default case.")
	private void leadOwnerSalesTerritoriesState_5() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			FSLeadOwnerAssignmentSchemeTest fsOwner = new FSLeadOwnerAssignmentSchemeTest();

			fsOwner.getSalesTerritoriesOwnerState(driver, testCaseId, "CorporateUser", "SystemLead", false, true); // corporate
																													// owner
																													// No
																													// owner
																													// Set
																													// in
																													// default
																													// case.
																													// //
																													// ok

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "salesLeadOwner" })
	@TestCase(createdOn = "2017-08-21", updatedOn = "2017-08-22", testCaseId = "TC_FC_Lead_Owner_SalesTerritories_State_6", testCaseDescription = "Verify Lead Owner Sales Territories with state basis via System Lead with regional user default case.")
	private void leadOwnerSalesTerritoriesState_6() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			FSLeadOwnerAssignmentSchemeTest fsOwner = new FSLeadOwnerAssignmentSchemeTest();

			fsOwner.getSalesTerritoriesOwnerState(driver, testCaseId, "RegionalUser", "SystemLead", false, true); // regional
																													// owner
																													// No
																													// owner
																													// Set
																													// in
																													// default
																													// case.

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "salesLeadOwner" })
	@TestCase(createdOn = "2017-08-21", updatedOn = "2017-08-22", testCaseId = "TC_FC_Lead_Owner_SalesTerritories_State_7", testCaseDescription = "Verify Lead Owner Sales Territories with state basis via Web Form Lead with corp user corporate owner case .")
	private void leadOwnerSalesTerritoriesState_7() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			FSLeadOwnerAssignmentSchemeTest fsOwner = new FSLeadOwnerAssignmentSchemeTest();

			fsOwner.getSalesTerritoriesOwnerState(driver, testCaseId, "CorporateUser", "WebForm", false, true); // corporate
																												// owner
																												// No
																												// owner
																												// Set
																												// in
																												// default
																												// owner
																												// case
																												// through
																												// web
																												// form.
																												// //
																												// ok

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "salesLeadOwner" })
	@TestCase(createdOn = "2017-08-21", updatedOn = "2017-08-22", testCaseId = "TC_FC_Lead_Owner_SalesTerritories_State_8", testCaseDescription = "Verify Lead Owner Sales Territories with state basis via Web Form Lead with regional user regional owner case.")
	private void leadOwnerSalesTerritoriesState_8() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			FSLeadOwnerAssignmentSchemeTest fsOwner = new FSLeadOwnerAssignmentSchemeTest();

			fsOwner.getSalesTerritoriesOwnerState(driver, testCaseId, "RegionalUser", "WebForm", false, true); // regional
																												// owner
																												// No
																												// owner
																												// Set
																												// in
																												// default
																												// owner
																												// case
																												// through
																												// web
																												// form.

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "salesLeadOwner" })
	@TestCase(createdOn = "2017-08-21", updatedOn = "2017-08-22", testCaseId = "TC_FC_Lead_Owner_SalesTerritories_State_9", testCaseDescription = "Verify Lead Owner Sales Territories with state basis via Web Form Lead with default owner case .")
	private void leadOwnerSalesTerritoriesState_9() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			FSLeadOwnerAssignmentSchemeTest fsOwner = new FSLeadOwnerAssignmentSchemeTest();

			fsOwner.getSalesTerritoriesOwnerState(driver, testCaseId, "CorporateUser", "WebForm", false, false); // corporate
																													// owner
																													// case
																													// with
																													// out
																													// round
																													// robin
																													// setting
																													// assigned
																													// to
																													// default
																													// owner
																													// //
																													// ok

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "salesLeadOwner" })
	@TestCase(createdOn = "2017-08-21", updatedOn = "2017-08-22", testCaseId = "TC_FC_Lead_Owner_SalesTerritories_State_10", testCaseDescription = "Verify Lead Owner Sales Territories with state basis via Web Form Lead regional user with default owner case")
	private void leadOwnerSalesTerritoriesState_10() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			FSLeadOwnerAssignmentSchemeTest fsOwner = new FSLeadOwnerAssignmentSchemeTest();

			fsOwner.getSalesTerritoriesOwnerState(driver, testCaseId, "RegionalUser", "WebForm", false, false); // regional
																												// owner
																												// case
																												// with
																												// out
																												// round
																												// robin
																												// setting
																												// assigned
																												// to
																												// default
																												// owner
																												// //
																												// checking

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "salesLeadOwner" })
	@TestCase(createdOn = "2017-08-21", updatedOn = "2017-08-22", testCaseId = "TC_FC_Lead_Owner_SalesTerritories_State_11", testCaseDescription = "Verify Lead Owner Sales Territories with state basis via Web form Lead with corp user corporate owner case .")
	private void leadOwnerSalesTerritoriesState_11() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			FSLeadOwnerAssignmentSchemeTest fsOwner = new FSLeadOwnerAssignmentSchemeTest();

			fsOwner.getSalesTerritoriesOwnerState(driver, testCaseId, "CorporateUser", "WebForm", true, false);// corporate
																												// owner
																												// case
																												// with
																												// round
																												// robin
																												// setting
																												// //
																												// ok

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "salesLeadOwner" })
	@TestCase(createdOn = "2017-08-21", updatedOn = "2017-08-22", testCaseId = "TC_FC_Lead_Owner_SalesTerritories_State_12", testCaseDescription = "Verify Lead Owner Sales Territories with state basis via Web form Lead with regional user regional owner case .")
	private void leadOwnerSalesTerritoriesState_12() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			FSLeadOwnerAssignmentSchemeTest fsOwner = new FSLeadOwnerAssignmentSchemeTest();

			fsOwner.getSalesTerritoriesOwnerState(driver, testCaseId, "RegionalUser", "WebForm", true, false);// regional
																												// owner
																												// case
																												// with
																												// round
																												// robin
																												// setting

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "salesLeadOwner" })
	@TestCase(createdOn = "2017-08-21", updatedOn = "2017-08-22", testCaseId = "TC_FC_Lead_Owner_SalesTerritories_State_13", testCaseDescription = "Verify Lead Owner Sales Territories with state basis via Rest API with corp user default case .")
	private void leadOwnerSalesTerritoriesState_13() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			FSLeadOwnerAssignmentSchemeTest fsOwner = new FSLeadOwnerAssignmentSchemeTest();

			fsOwner.getSalesTerritoriesOwnerState(driver, testCaseId, "CorporateUser", "RestAPI", false, true); // corporate
																												// owner
																												// No
																												// owner
																												// Set
																												// in
																												// default
																												// owner
																												// case
																												// through
																												// web
																												// form.
																												// //
																												// ok

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "salesLeadOwner" })
	@TestCase(createdOn = "2017-08-21", updatedOn = "2017-08-22", testCaseId = "TC_FC_Lead_Owner_SalesTerritories_State_14", testCaseDescription = "Verify Lead Owner Sales Territories with state basis via Rest API with regional user default case.")
	private void leadOwnerSalesTerritoriesState_14() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			FSLeadOwnerAssignmentSchemeTest fsOwner = new FSLeadOwnerAssignmentSchemeTest();

			fsOwner.getSalesTerritoriesOwnerState(driver, testCaseId, "RegionalUser", "RestAPI", false, true); // regional
																												// owner
																												// No
																												// owner
																												// Set
																												// in
																												// default
																												// owner
																												// case
																												// through
																												// web
																												// form.

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "salesLeadOwner" })
	@TestCase(createdOn = "2017-08-21", updatedOn = "2017-08-22", testCaseId = "TC_FC_Lead_Owner_SalesTerritories_State_16", testCaseDescription = "Verify Lead Owner Sales Territories with state basis via Rest API with corp user default owner case.")
	private void leadOwnerSalesTerritoriesState_16() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			FSLeadOwnerAssignmentSchemeTest fsOwner = new FSLeadOwnerAssignmentSchemeTest();

			fsOwner.getSalesTerritoriesOwnerState(driver, testCaseId, "CorporateUser", "RestAPI", false, false); // corporate
																													// owner
																													// case
																													// with
																													// out
																													// round
																													// robin
																													// setting
																													// assigned
																													// to
																													// default
																													// owner

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "salesLeadOwner" })
	@TestCase(createdOn = "2017-08-21", updatedOn = "2017-08-22", testCaseId = "TC_FC_Lead_Owner_SalesTerritories_State_17", testCaseDescription = "Verify Lead Owner Sales Territories with state basis via Rest API with regional user default owner case .")
	private void leadOwnerSalesTerritoriesState_17() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			FSLeadOwnerAssignmentSchemeTest fsOwner = new FSLeadOwnerAssignmentSchemeTest();

			fsOwner.getSalesTerritoriesOwnerState(driver, testCaseId, "RegionalUser", "RestAPI", false, false); // regional
																												// owner
																												// case
																												// with
																												// out
																												// round
																												// robin
																												// setting
																												// assigned
																												// to
																												// default
																												// owner

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "salesLeadOwner" })
	@TestCase(createdOn = "2017-08-21", updatedOn = "2017-08-22", testCaseId = "TC_FC_Lead_Owner_SalesTerritories_State_18", testCaseDescription = "Verify Lead Owner Sales Territories with state basis via Rest API Lead with corp user corporate owner case.")
	private void leadOwnerSalesTerritoriesState_18() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			FSLeadOwnerAssignmentSchemeTest fsOwner = new FSLeadOwnerAssignmentSchemeTest();

			fsOwner.getSalesTerritoriesOwnerState(driver, testCaseId, "CorporateUser", "RestAPI", true, false);// corporate
																												// owner
																												// case
																												// with
																												// round
																												// robin
																												// setting

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "salesLeadOwner" })
	@TestCase(createdOn = "2017-08-21", updatedOn = "2017-08-22", testCaseId = "TC_FC_Lead_Owner_SalesTerritories_State_19", testCaseDescription = "Verify Lead Owner Sales Territories with state basis via System Lead with regional user regional owner case.")
	private void leadOwnerSalesTerritoriesState_19() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			FSLeadOwnerAssignmentSchemeTest fsOwner = new FSLeadOwnerAssignmentSchemeTest();

			fsOwner.getSalesTerritoriesOwnerState(driver, testCaseId, "RegionalUser", "RestAPI", true, false); // regional
																												// owner
																												// case
																												// with
																												// out
																												// round
																												// robin
																												// setting
																												// assigned
																												// to
																												// default
																												// owner

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "salesLeadOwner" })
	@TestCase(createdOn = "2017-08-21", updatedOn = "2017-08-22", testCaseId = "TC_FC_Lead_Owner_SalesTerritories_State_20", testCaseDescription = "Verify Lead Owner Sales Territories with state basis via Import with corp user default case .")
	private void leadOwnerSalesTerritoriesState_20() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			FSLeadOwnerAssignmentSchemeTest fsOwner = new FSLeadOwnerAssignmentSchemeTest();

			fsOwner.getSalesTerritoriesOwnerState(driver, testCaseId, "CorporateUser", "Import", false, true); // corporate
																												// owner
																												// No
																												// owner
																												// Set
																												// in
																												// default
																												// owner
																												// case
																												// through
																												// web
																												// form.
																												// //
																												// ok

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "salesLeadOwner" })
	@TestCase(createdOn = "2017-08-21", updatedOn = "2017-08-22", testCaseId = "TC_FC_Lead_Owner_SalesTerritories_State_21", testCaseDescription = "Verify Lead Owner Sales Territories with state basis via Import with regional user default case.")
	private void leadOwnerSalesTerritoriesState_21() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			FSLeadOwnerAssignmentSchemeTest fsOwner = new FSLeadOwnerAssignmentSchemeTest();

			fsOwner.getSalesTerritoriesOwnerState(driver, testCaseId, "RegionalUser", "Import", false, true); // regional
																												// owner
																												// No
																												// owner
																												// Set
																												// in
																												// default
																												// owner
																												// case
																												// through
																												// web
																												// form.

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "salesLeadOwner" })
	@TestCase(createdOn = "2017-08-21", updatedOn = "2017-08-22", testCaseId = "TC_FC_Lead_Owner_SalesTerritories_State_22", testCaseDescription = "Verify Lead Owner Sales Territories with state basis via Import with corp user default owner case.")
	private void leadOwnerSalesTerritoriesState_22() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			FSLeadOwnerAssignmentSchemeTest fsOwner = new FSLeadOwnerAssignmentSchemeTest();

			fsOwner.getSalesTerritoriesOwnerState(driver, testCaseId, "CorporateUser", "Import", false, false); // corporate
																												// owner
																												// case
																												// with
																												// out
																												// round
																												// robin
																												// setting
																												// assigned
																												// to
																												// default
																												// owner

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "salesLeadOwner" })
	@TestCase(createdOn = "2017-08-21", updatedOn = "2017-08-22", testCaseId = "TC_FC_Lead_Owner_SalesTerritories_State_23", testCaseDescription = "Verify Lead Owner Sales Territories with state basis via Import with regional user default owner case .")
	private void leadOwnerSalesTerritoriesState_23() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			FSLeadOwnerAssignmentSchemeTest fsOwner = new FSLeadOwnerAssignmentSchemeTest();

			fsOwner.getSalesTerritoriesOwnerState(driver, testCaseId, "RegionalUser", "Import", false, false); // regional
																												// owner
																												// case
																												// with
																												// out
																												// round
																												// robin
																												// setting
																												// assigned
																												// to
																												// default
																												// owner

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "salesLeadOwner" })
	@TestCase(createdOn = "2017-08-21", updatedOn = "2017-08-22", testCaseId = "TC_FC_Lead_Owner_SalesTerritories_State_24", testCaseDescription = "Verify Lead Owner Sales Territories with state basis via Import Lead with corp user corporate owner case.")
	private void leadOwnerSalesTerritoriesState_24() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			FSLeadOwnerAssignmentSchemeTest fsOwner = new FSLeadOwnerAssignmentSchemeTest();

			fsOwner.getSalesTerritoriesOwnerState(driver, testCaseId, "CorporateUser", "Import", true, false);// corporate
																												// owner
																												// case
																												// with
																												// round
																												// robin
																												// setting

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "salesLeadOwner" })
	@TestCase(createdOn = "2017-08-21", updatedOn = "2017-08-22", testCaseId = "TC_FC_Lead_Owner_SalesTerritories_State_25", testCaseDescription = "Verify Lead Owner Sales Territories with state basis via Import with regional user regional owner case.")
	private void leadOwnerSalesTerritoriesState_25() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);

			FSLeadOwnerAssignmentSchemeTest fsOwner = new FSLeadOwnerAssignmentSchemeTest();

			fsOwner.getSalesTerritoriesOwnerState(driver, testCaseId, "RegionalUser", "Import", true, false); // regional
																												// owner
																												// case
																												// with
																												// out
																												// round
																												// robin
																												// setting
																												// assigned
																												// to
																												// default
																												// owner

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

}