package com.builds.test.fs;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Test;

import com.builds.utilities.FranconnectUtil;
import com.builds.utilities.TestCase;

public class FSLeadOwnerSalesTerritoriesZipTest {
	FranconnectUtil fc = new FranconnectUtil();

	@Test(groups = { "salesLeadOwner" })
	@TestCase(createdOn = "2017-08-21", updatedOn = "2017-08-21", testCaseId = "TC_FC_Lead_Owner_SalesTerritories_ZIp_1", testCaseDescription = "Verify Lead Owner Sales Territories with Zip basis via System Lead with corp user corporate owner .")
	private void leadOwnerSalesTerritoriesZip_1() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			FSLeadOwnerAssignmentSchemeTest fsOwner = new FSLeadOwnerAssignmentSchemeTest();

			fsOwner.getSalesTerritoriesOwnerZip(driver, testCaseId, "CorporateUser", "SystemLead", true, false);// corporate
																												// owner
																												// case
																												// with
																												// round
																												// robin
																												// setting
																												// //
																												// ok

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "salesLeadOwner" })
	@TestCase(createdOn = "2017-08-21", updatedOn = "2017-08-21", testCaseId = "TC_FC_Lead_Owner_SalesTerritories_ZIp_2", testCaseDescription = "Verify Lead Owner Sales Territories with Zip basis via System Lead with regional user regional owner.")
	private void leadOwnerSalesTerritoriesZip_2() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			FSLeadOwnerAssignmentSchemeTest fsOwner = new FSLeadOwnerAssignmentSchemeTest();

			fsOwner.getSalesTerritoriesOwnerZip(driver, testCaseId, "RegionalUser", "SystemLead", true, false);// regional
																												// owner
																												// case
																												// with
																												// round
																												// robin
																												// setting

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "salesLeadOwner" })
	@TestCase(createdOn = "2017-08-21", updatedOn = "2017-08-21", testCaseId = "TC_FC_Lead_Owner_SalesTerritories_ZIp_3", testCaseDescription = "Verify Lead Owner Sales Territories with Zip basis via System Lead with corp user default owner.")
	private void leadOwnerSalesTerritoriesZip_3() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			FSLeadOwnerAssignmentSchemeTest fsOwner = new FSLeadOwnerAssignmentSchemeTest();

			fsOwner.getSalesTerritoriesOwnerZip(driver, testCaseId, "CorporateUser", "SystemLead", false, false);// corporate
																													// owner
																													// case
																													// with
																													// out
																													// SalesTerritories
																													// setting
																													// assigned
																													// to
																													// default
																													// owner
																													// //
																													// ok

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "salesLeadOwner" })
	@TestCase(createdOn = "2017-08-21", updatedOn = "2017-08-21", testCaseId = "TC_FC_Lead_Owner_SalesTerritories_ZIp_4", testCaseDescription = "Verify Lead Owner Sales Territories with Zip basis via System Lead with regional user default owner")
	private void leadOwnerSalesTerritoriesZip_4() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			FSLeadOwnerAssignmentSchemeTest fsOwner = new FSLeadOwnerAssignmentSchemeTest();
			fsOwner.getSalesTerritoriesOwnerZip(driver, testCaseId, "RegionalUser", "SystemLead", false, false); // regional
																													// owner
																													// case
																													// with
																													// out
																													// round
																													// robin
																													// setting
																													// assigned
																													// to
																													// default
																													// owner

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "salesLeadOwner" })
	@TestCase(createdOn = "2017-08-21", updatedOn = "2017-08-21", testCaseId = "TC_FC_Lead_Owner_SalesTerritories_ZIp_5", testCaseDescription = "Verify Lead Owner Sales Territories with Zip basis via System Lead with corp user default case.")
	private void leadOwnerSalesTerritoriesZip_5() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			FSLeadOwnerAssignmentSchemeTest fsOwner = new FSLeadOwnerAssignmentSchemeTest();

			fsOwner.getSalesTerritoriesOwnerZip(driver, testCaseId, "CorporateUser", "SystemLead", false, true); // corporate
																													// owner
																													// No
																													// owner
																													// Set
																													// in
																													// default
																													// case.
																													// //
																													// ok

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "salesLeadOwner" })
	@TestCase(createdOn = "2017-08-21", updatedOn = "2017-08-21", testCaseId = "TC_FC_Lead_Owner_SalesTerritories_ZIp_6", testCaseDescription = "Verify Lead Owner Sales Territories with Zip basis via System Lead with regional user default case.")
	private void leadOwnerSalesTerritoriesZip_6() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			FSLeadOwnerAssignmentSchemeTest fsOwner = new FSLeadOwnerAssignmentSchemeTest();

			fsOwner.getSalesTerritoriesOwnerZip(driver, testCaseId, "RegionalUser", "SystemLead", false, true); // regional
																												// owner
																												// No
																												// owner
																												// Set
																												// in
																												// default
																												// case.

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "salesLeadOwner" })
	@TestCase(createdOn = "2017-08-21", updatedOn = "2017-08-21", testCaseId = "TC_FC_Lead_Owner_SalesTerritories_ZIp_8", testCaseDescription = "Verify Lead Owner Sales Territories with Zip basis via Web form with corp user default owner.")
	private void leadOwnerSalesTerritoriesZip_8() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			FSLeadOwnerAssignmentSchemeTest fsOwner = new FSLeadOwnerAssignmentSchemeTest();

			fsOwner.getSalesTerritoriesOwnerZip(driver, testCaseId, "CorporateUser", "WebForm", false, true); // corporate
																												// owner
																												// No
																												// owner
																												// Set
																												// in
																												// default
																												// owner
																												// case
																												// through
																												// web
																												// form.
																												// //
																												// ok

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "salesLeadOwner" })
	@TestCase(createdOn = "2017-08-21", updatedOn = "2017-08-21", testCaseId = "TC_FC_Lead_Owner_SalesTerritories_ZIp_9", testCaseDescription = "Verify Lead Owner Sales Territories with Zip basis via Web Form with regional user default owner.")
	private void leadOwnerSalesTerritoriesZip_9() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			FSLeadOwnerAssignmentSchemeTest fsOwner = new FSLeadOwnerAssignmentSchemeTest();

			fsOwner.getSalesTerritoriesOwnerZip(driver, testCaseId, "RegionalUser", "WebForm", false, true); // regional
																												// owner
																												// No
																												// owner
																												// Set
																												// in
																												// default
																												// owner
																												// case
																												// through
																												// web
																												// form.

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "salesLeadOwner" })
	@TestCase(createdOn = "2017-08-21", updatedOn = "2017-08-21", testCaseId = "TC_FC_Lead_Owner_SalesTerritories_ZIp_10", testCaseDescription = "Verify Lead Owner Sales Territories with Zip basis via Web form with corp user default owner.")
	private void leadOwnerSalesTerritoriesZip_10() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			FSLeadOwnerAssignmentSchemeTest fsOwner = new FSLeadOwnerAssignmentSchemeTest();

			fsOwner.getSalesTerritoriesOwnerZip(driver, testCaseId, "CorporateUser", "WebForm", false, false); // corporate
																												// owner
																												// case
																												// with
																												// out
																												// round
																												// robin
																												// setting
																												// assigned
																												// to
																												// default
																												// owner
																												// //
																												// ok

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "salesLeadOwner" })
	@TestCase(createdOn = "2017-08-21", updatedOn = "2017-08-21", testCaseId = "TC_FC_Lead_Owner_SalesTerritories_ZIp_11", testCaseDescription = "Verify Lead Owner Sales Territories with Zip basis via Web form with regional user default owner.")
	private void leadOwnerSalesTerritoriesZip_11() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			FSLeadOwnerAssignmentSchemeTest fsOwner = new FSLeadOwnerAssignmentSchemeTest();

			fsOwner.getSalesTerritoriesOwnerZip(driver, testCaseId, "RegionalUser", "WebForm", false, false); // regional
																												// owner
																												// case
																												// with
																												// out
																												// round
																												// robin
																												// setting
																												// assigned
																												// to
																												// default
																												// owner

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "salesLeadOwner" })
	@TestCase(createdOn = "2017-08-21", updatedOn = "2017-08-21", testCaseId = "TC_FC_Lead_Owner_SalesTerritories_ZIp_12", testCaseDescription = "Verify Lead Owner Sales Territories with Zip basis via Web form with corp user corporate owner.")
	private void leadOwnerSalesTerritoriesZip_12() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			FSLeadOwnerAssignmentSchemeTest fsOwner = new FSLeadOwnerAssignmentSchemeTest();

			fsOwner.getSalesTerritoriesOwnerZip(driver, testCaseId, "CorporateUser", "WebForm", true, false);// corporate
																												// owner
																												// case
																												// with
																												// round
																												// robin
																												// setting
																												// //
																												// ok

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "salesLeadOwner" })
	@TestCase(createdOn = "2017-08-21", updatedOn = "2017-08-21", testCaseId = "TC_FC_Lead_Owner_SalesTerritories_ZIp_13", testCaseDescription = "Verify Lead Owner Sales Territories with Zip basis via Web form with regioanl user regional owner.")
	private void leadOwnerSalesTerritoriesZip_13() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			FSLeadOwnerAssignmentSchemeTest fsOwner = new FSLeadOwnerAssignmentSchemeTest();

			fsOwner.getSalesTerritoriesOwnerZip(driver, testCaseId, "RegionalUser", "WebForm", true, false);// regional
																											// owner
																											// case
																											// with
																											// round
																											// robin
																											// setting

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "salesLeadOwner" })
	@TestCase(createdOn = "2017-08-21", updatedOn = "2017-08-21", testCaseId = "TC_FC_Lead_Owner_SalesTerritories_ZIp_14", testCaseDescription = "Verify Lead Owner Sales Territories with Zip basis via Rest API with corp use default Case.")
	private void leadOwnerSalesTerritoriesZip_14() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			FSLeadOwnerAssignmentSchemeTest fsOwner = new FSLeadOwnerAssignmentSchemeTest();

			fsOwner.getSalesTerritoriesOwnerZip(driver, testCaseId, "CorporateUser", "RestAPI", false, true); // corporate
																												// owner
																												// No
																												// owner
																												// Set
																												// in
																												// default
																												// owner
																												// case
																												// through
																												// web
																												// form.
																												// //
																												// ok

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "salesLeadOwner" })
	@TestCase(createdOn = "2017-08-21", updatedOn = "2017-08-21", testCaseId = "TC_FC_Lead_Owner_SalesTerritories_ZIp_15", testCaseDescription = "Verify Lead Owner Sales Territories with Zip basis via Rest API with regional user default case.")
	private void leadOwnerSalesTerritoriesZip_15() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			FSLeadOwnerAssignmentSchemeTest fsOwner = new FSLeadOwnerAssignmentSchemeTest();

			fsOwner.getSalesTerritoriesOwnerZip(driver, testCaseId, "RegionalUser", "RestAPI", false, true); // regional
																												// owner
																												// No
																												// owner
																												// Set
																												// in
																												// default
																												// owner
																												// case
																												// through
																												// web
																												// form.

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "salesLeadOwner" })
	@TestCase(createdOn = "2017-08-21", updatedOn = "2017-08-21", testCaseId = "TC_FC_Lead_Owner_SalesTerritories_ZIp_16", testCaseDescription = "Verify Lead Owner Sales Territories with Zip basis via Rest API with corp user default owner .")
	private void leadOwnerSalesTerritoriesZip_16() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			FSLeadOwnerAssignmentSchemeTest fsOwner = new FSLeadOwnerAssignmentSchemeTest();

			fsOwner.getSalesTerritoriesOwnerZip(driver, testCaseId, "CorporateUser", "RestAPI", false, false); // corporate
																												// owner
																												// case
																												// with
																												// out
																												// round
																												// robin
																												// setting
																												// assigned
																												// to
																												// default
																												// owner

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "salesLeadOwner" })
	@TestCase(createdOn = "2017-08-21", updatedOn = "2017-08-21", testCaseId = "TC_FC_Lead_Owner_SalesTerritories_ZIp_17", testCaseDescription = "Verify Lead Owner Sales Territories with Zip basis via Rest API with regional user default owner.")
	private void leadOwnerSalesTerritoriesZip_17() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			FSLeadOwnerAssignmentSchemeTest fsOwner = new FSLeadOwnerAssignmentSchemeTest();

			fsOwner.getSalesTerritoriesOwnerZip(driver, testCaseId, "RegionalUser", "RestAPI", false, false); // regional
																												// owner
																												// case
																												// with
																												// out
																												// round
																												// robin
																												// setting
																												// assigned
																												// to
																												// default
																												// owner

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "salesLeadOwner" })
	@TestCase(createdOn = "2017-08-21", updatedOn = "2017-08-21", testCaseId = "TC_FC_Lead_Owner_SalesTerritories_ZIp_18", testCaseDescription = "Verify Lead Owner Sales Territories with Zip basis via Rest API with corp user corporate owner case.")
	private void leadOwnerSalesTerritoriesZip_18() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			FSLeadOwnerAssignmentSchemeTest fsOwner = new FSLeadOwnerAssignmentSchemeTest();

			fsOwner.getSalesTerritoriesOwnerZip(driver, testCaseId, "CorporateUser", "RestAPI", true, false);// corporate
																												// owner
																												// case
																												// with
																												// round
																												// robin
																												// setting

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "salesLeadOwner" })
	@TestCase(createdOn = "2017-08-21", updatedOn = "2017-08-21", testCaseId = "TC_FC_Lead_Owner_SalesTerritories_ZIp_19", testCaseDescription = "Verify Lead Owner Sales Territories with Zip basis via Rest API with regional user regional owner case.")
	private void leadOwnerSalesTerritoriesZip_19() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			FSLeadOwnerAssignmentSchemeTest fsOwner = new FSLeadOwnerAssignmentSchemeTest();

			fsOwner.getSalesTerritoriesOwnerZip(driver, testCaseId, "RegionalUser", "RestAPI", true, false); // regional
																												// owner
																												// case
																												// with
																												// out
																												// round
																												// robin
																												// setting
																												// assigned
																												// to
																												// default
																												// owner

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "salesLeadOwner" })
	@TestCase(createdOn = "2017-08-21", updatedOn = "2017-08-21", testCaseId = "TC_FC_Lead_Owner_SalesTerritories_ZIp_20", testCaseDescription = "Verify Lead Owner Sales Territories with Zip basis via Import with corp use default Case.")
	private void leadOwnerSalesTerritoriesZip_20() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			FSLeadOwnerAssignmentSchemeTest fsOwner = new FSLeadOwnerAssignmentSchemeTest();

			fsOwner.getSalesTerritoriesOwnerZip(driver, testCaseId, "CorporateUser", "Import", false, true); // corporate
																												// owner
																												// No
																												// owner
																												// Set
																												// in
																												// default
																												// owner
																												// case
																												// through
																												// web
																												// form.
																												// //
																												// ok

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "salesLeadOwner" })
	@TestCase(createdOn = "2017-08-21", updatedOn = "2017-08-21", testCaseId = "TC_FC_Lead_Owner_SalesTerritories_ZIp_21", testCaseDescription = "Verify Lead Owner Sales Territories with Zip basis via Import with regional user default case.")
	private void leadOwnerSalesTerritoriesZip_21() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			FSLeadOwnerAssignmentSchemeTest fsOwner = new FSLeadOwnerAssignmentSchemeTest();

			fsOwner.getSalesTerritoriesOwnerZip(driver, testCaseId, "RegionalUser", "Import", false, true); // regional
																											// owner
																											// No
																											// owner
																											// Set
																											// in
																											// default
																											// owner
																											// case
																											// through
																											// web
																											// form.

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "salesLeadOwner" })
	@TestCase(createdOn = "2017-08-21", updatedOn = "2017-08-21", testCaseId = "TC_FC_Lead_Owner_SalesTerritories_ZIp_22", testCaseDescription = "Verify Lead Owner Sales Territories with Zip basis via Import with corp user default owner .")
	private void leadOwnerSalesTerritoriesZip_22() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			FSLeadOwnerAssignmentSchemeTest fsOwner = new FSLeadOwnerAssignmentSchemeTest();

			fsOwner.getSalesTerritoriesOwnerZip(driver, testCaseId, "CorporateUser", "Import", false, false); // corporate
																												// owner
																												// case
																												// with
																												// out
																												// round
																												// robin
																												// setting
																												// assigned
																												// to
																												// default
																												// owner

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "salesLeadOwner" })
	@TestCase(createdOn = "2017-08-21", updatedOn = "2017-08-21", testCaseId = "TC_FC_Lead_Owner_SalesTerritories_ZIp_23", testCaseDescription = "Verify Lead Owner Sales Territories with Zip basis via Import with regional user default owner.")
	private void leadOwnerSalesTerritoriesZip_23() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			FSLeadOwnerAssignmentSchemeTest fsOwner = new FSLeadOwnerAssignmentSchemeTest();

			fsOwner.getSalesTerritoriesOwnerZip(driver, testCaseId, "RegionalUser", "Import", false, false); // regional
																												// owner
																												// case
																												// with
																												// out
																												// round
																												// robin
																												// setting
																												// assigned
																												// to
																												// default
																												// owner

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "salesLeadOwner" })
	@TestCase(createdOn = "2017-08-21", updatedOn = "2017-08-21", testCaseId = "TC_FC_Lead_Owner_SalesTerritories_ZIp_24", testCaseDescription = "Verify Lead Owner Sales Territories with Zip basis via Import with corp user corporate owner case.")
	private void leadOwnerSalesTerritoriesZip_24() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			FSLeadOwnerAssignmentSchemeTest fsOwner = new FSLeadOwnerAssignmentSchemeTest();

			fsOwner.getSalesTerritoriesOwnerZip(driver, testCaseId, "CorporateUser", "Import", true, false);// corporate
																											// owner
																											// case
																											// with
																											// round
																											// robin
																											// setting

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	@Test(groups = { "salesLeadOwner" })
	@TestCase(createdOn = "2017-08-21", updatedOn = "2017-08-21", testCaseId = "TC_FC_Lead_Owner_SalesTerritories_ZIp_25", testCaseDescription = "Verify Lead Owner Sales Territories with Zip basis via Import with regional user regional owner case.")
	private void leadOwnerSalesTerritoriesZip_25() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();

		try {
			driver = fc.loginpage().login(driver);
			FSLeadOwnerAssignmentSchemeTest fsOwner = new FSLeadOwnerAssignmentSchemeTest();

			fsOwner.getSalesTerritoriesOwnerZip(driver, testCaseId, "RegionalUser", "Import", true, false); // regional
																											// owner
																											// case
																											// with
																											// out
																											// round
																											// robin
																											// setting
																											// assigned
																											// to
																											// default
																											// owner

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

}