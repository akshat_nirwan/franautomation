package com.builds.test.fs;

import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.testng.Reporter;
import org.testng.annotations.Test;

import com.builds.uimaps.fs.FSLeadSummaryCoApplicantsPage;
import com.builds.uimaps.fs.FSLeadSummaryPrimaryInfoPage;
import com.builds.uimaps.fs.LeadSummaryUI;
import com.builds.utilities.FranconnectUtil;
import com.builds.utilities.TestCase;

public class FSLeadQA {
	FranconnectUtil fc = new FranconnectUtil();

	@Test(groups = { "sales", "TC_FC_QA_Sale_Lead_Primary", "SalesFullLeadInfo","sales_failed" }) // To be fixed // akshat
	@TestCase(createdOn = "2017-05-01", updatedOn = "2017-08-01", testCaseId = "TC_FC_QA_Sale_Lead_Primary", testCaseDescription = "Verify Lead All Tab Data with QA input File")
	private void LeadPrimaryInfoFill() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		Map<String, String> printInfo = null;
		String testCaseIdInternal = "" + testCaseId;
		try {
			driver = fc.loginpage().login(driver);

			fc.sales().sales_common().fsModule(driver);
			Sales fs = new Sales();
			fs.leadManagement(driver);
			FSLeadSummaryPageTest fsPage = new FSLeadSummaryPageTest();
			fsPage.clickAddLeadLink(driver);
			fc.utobj().printTestStep("Add a lead and Go to Primary Info");

			FSLeadSummaryPrimaryInfoPageTest pInfoPage = new FSLeadSummaryPrimaryInfoPageTest();
			testCaseIdInternal = "TC_FC_QA_Sale_Lead_Primary";
			printInfo = fillFormData(driver, testCaseIdInternal);
			Reporter.log("Map == for =" + testCaseIdInternal + "===" + printInfo);

			fc.utobj().printTestStep("Go to Personal Profile");

			FSLeadSummaryPrimaryInfoPage pobjnew = new FSLeadSummaryPrimaryInfoPage(driver);
			testCaseIdInternal = "TC_FC_QA_Sale_Lead_PersonalProfile";
			fc.utobj().clickElement(driver, pobjnew.personalProfileTab);
			printInfo = fillFormData(driver, testCaseIdInternal);
			Reporter.log("Map == for =" + testCaseIdInternal + "===" + printInfo);

			fc.utobj().printTestStep("Go to Qualification Details");

			FSLeadSummaryPrimaryInfoPage pobj = new FSLeadSummaryPrimaryInfoPage(driver);
			testCaseIdInternal = "TC_FC_QA_Sale_Lead_QualificationDetails";
			fc.utobj().clickElement(driver, pobj.qualificationDetailsTab);
			printInfo = fillFormData(driver, testCaseIdInternal);
			Reporter.log("Map == for " + testCaseIdInternal + "===" + printInfo);

			fc.utobj().printTestStep("Go to Real Estate");

			testCaseIdInternal = "TC_FC_QA_Sale_Lead_RealEstate";
			LeadSummaryUI pobjreal = new LeadSummaryUI(driver);
			fc.utobj().printTestStep("Add real estate");
			fc.utobj().clickElement(driver, pobjreal.realEstate);
			printInfo = fillFormData(driver, testCaseIdInternal);
			Reporter.log("Map == for " + testCaseIdInternal + "===" + printInfo);

			fc.utobj().printTestStep("Go to Compliance");

			testCaseIdInternal = "TC_FC_QA_Sale_Lead_Compliance";
			fc.utobj().printTestStep("Add compliance");
			fc.utobj().clickElement(driver, pobj.complianceTab);
			printInfo = fillFormData(driver, testCaseIdInternal);
			Reporter.log("Map == for " + testCaseIdInternal + "===" + printInfo);

			fc.utobj().printTestStep("Go to Visit");

			testCaseIdInternal = "TC_FC_QA_Sale_Lead_Visit";
			fc.utobj().printTestStep("Add Visit ");
			fc.utobj().clickElement(driver, pobj.visitTab);
			printInfo = fillFormData(driver, testCaseIdInternal);
			Reporter.log("Map == for " + testCaseId + "===" + printInfo);

			testCaseIdInternal = "TC_FC_QA_Sale_Lead_Document";
			fc.utobj().printTestStep("Add Document  ");
			fc.utobj().clickElement(driver, pobj.documentTab);
			printInfo = fillFormData(driver, testCaseIdInternal);
			Reporter.log("Map == for " + testCaseIdInternal + "===" + printInfo);

			fc.utobj().printTestStep("Go to Co-Applicant ");

			FSLeadSummaryCoApplicantsPage pobjcap = new FSLeadSummaryCoApplicantsPage(driver);
			testCaseIdInternal = "TC_FC_QA_Sale_Lead_CoApplicantt";
			fc.utobj().printTestStep("Add Co-Applicant  ");
			fc.utobj().clickElement(driver, pobj.coApplicantTab);
			fc.utobj().clickElement(driver, pobjcap.addCoApplicantBtn);
			printInfo = fillFormData(driver, testCaseIdInternal);
			Reporter.log("Map == for " + testCaseIdInternal + "===" + printInfo);

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {
			e.printStackTrace();
			// fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

	public Map<String, String> fillFormData(WebDriver driver, String testCaseId) throws Exception {
		Reporter.log("Filling TestCase Data = " + testCaseId);
		// fc.utobj().printTestStep(testCaseId, "Filling TestCase Data");
		String id = "id";
		if ("TC_FC_QA_Sale_Lead_QualificationDetails".equals(testCaseId)) {
			id = "name";
		}
		Map<String, String> dataSet = new HashMap<String, String>();
		dataSet = fc.utobj().readTestData("sales", testCaseId);

		List<String> listItems = new ArrayList<String>();
		Map<String, String> printInfo = new HashMap<String, String>();

		List<WebElement> mandateBox = null;
		mandateBox = driver.findElements(By.xpath(
				".//td[@qat_tableid='addData']//*[@type='text' or @class='fTextBoxDate' or @class='fTextBox' or @class='multiList' or @type='radio' or @type='checkbox' or @type='checkBox' or @id='ms-parentsellerCombo' or @class='ms-choice' or @class='form-control']"));

		Map<String, String> alreadyitrateElement = new HashMap<>();
		Map<String, String> alreadyradio = new HashMap<>();
		Map<String, String> alreadycheckBox = new HashMap<>();

		try {
			for (WebElement webElement : mandateBox) {
				
				// System.out.println(webElement);

				try {
					System.out.println(webElement.getAttribute("" + id)); // ------------------------------------------------------------------------------------------------------------   akshat
					if (webElement.getAttribute("" + id) != null && !"".equals(webElement.getAttribute("" + id))
							&& !"null".equals(webElement.getAttribute("" + id))
							&& !":".equals(webElement.getAttribute("" + id))
							&& !"dateOfOpen".equals(webElement.getAttribute("" + id))
							&& !"fimCbOtherComplaintType".equals(webElement.getAttribute("" + id))) {
						// System.out.println(""+webElement.getAttribute(""+id));
						// System.out.println("Key==>>"+webElement.getAttribute("id")+"===values==>>"+webElement.getAttribute("value")+"=====Type===>>>"+webElement.getAttribute("type"));
						if (alreadyitrateElement.containsKey(webElement.getAttribute("" + id))) {

						} else {
							if (dataSet.containsKey(webElement.getAttribute("" + id))) {
								WebElement elementmovepostion = null;
								if ("name".equals(id)) {
									elementmovepostion = driver
											.findElement(By.name("" + webElement.getAttribute("" + id)));

								} else {
									elementmovepostion = fc.utobj().getElementByID(driver,
											"" + webElement.getAttribute("" + id));

								}
								fc.utobj().moveToElement(driver, elementmovepostion);
								if (webElement.getAttribute("type").indexOf("select-one") != -1) {
									Select singledrop = null;
									List<WebElement> singleDropDown = null;
									try {
										singledrop = new Select(fc.utobj().getElementByID(driver,
												"" + webElement.getAttribute("" + id)));
										singleDropDown = driver
												.findElements(By.id("" + webElement.getAttribute("" + id)));
									} catch (Exception e) {
										singledrop = new Select(fc.utobj().getElementByName(driver,
												"" + webElement.getAttribute("" + id)));
										singleDropDown = driver
												.findElements(By.name("" + webElement.getAttribute("" + id)));
									}
									if (webElement.getAttribute("value") == null
											|| "".equals(webElement.getAttribute("value"))
											|| "null".equals(webElement.getAttribute("value"))
											|| "-1".equals(webElement.getAttribute("value"))) {

										int size = singleDropDown.size();
										if (dataSet.get("" + webElement.getAttribute("" + id)) != null
												&& !"".equals(dataSet.get("" + webElement.getAttribute("" + id)))) {
											if (size >= 1) {
												try {
													WebElement dropDownField = fc.utobj().getElementByID(driver,
															"" + webElement.getAttribute("id"));
													fc.utobj().selectDropDownByVisibleText(driver, dropDownField,
															dataSet.get("" + webElement.getAttribute("" + id)));

													if (!"birthMonth".equals(webElement.getAttribute("" + id))
															&& !"birthDate".equals(webElement.getAttribute("" + id))
															&& !"spouseBirthMonth"
																	.equals(webElement.getAttribute("" + id))
															&& !"spouseBirthDate"
																	.equals(webElement.getAttribute("" + id))) {
														listItems.add(
																dataSet.get("" + webElement.getAttribute("" + id)));
													}

													printInfo.put("" + webElement.getAttribute("" + id),
															dataSet.get("" + webElement.getAttribute("" + id)));

												} catch (Exception eradio) {
													// Reporter.log("Problem in
													// selecting drop :
													// "+""+webElement.getAttribute("id")
													// +" Values getting from
													// Excel not match in
													// desired
													// fomrat."+""+dataSet.get(""+webElement.getAttribute("id")));
												}
											}
										}
									}
								} else if (webElement.getAttribute("type").indexOf("select-multiple") != -1) {
									if (webElement.getAttribute("value") == null
											|| "".equals(webElement.getAttribute("value"))
											|| "null".equals(webElement.getAttribute("value"))
											|| "-1".equals(webElement.getAttribute("value"))) {
										try {
											WebElement element = fc.utobj().getElementByID(driver,
													"ms-parent" + webElement.getAttribute("" + id));
											fc.utobj().clickElement(driver, element);
											fc.utobj().clickElement(driver,
													fc.utobj().getElementByXpath(driver, ".//*[@id='selectAll']"));
											fc.utobj().clickElement(driver, element);
										} catch (Exception emultipal) {
											// Reporter.log("Problem in multiple
											// value drop-down :
											// "+""+webElement.getAttribute("id"));
										}
									}
								} else if (webElement.getAttribute("type").indexOf("textarea") != -1) {
									if (webElement.getAttribute("value") == null
											|| "".equals(webElement.getAttribute("value"))
											|| "null".equals(webElement.getAttribute("value"))) {
										try {
											fc.utobj().getElementByName(driver, "" + webElement.getAttribute("" + id))
													.sendKeys(dataSet.get("" + webElement.getAttribute("" + id)));
											listItems.add(dataSet.get("" + webElement.getAttribute("" + id)));
											printInfo.put("" + webElement.getAttribute("id"),
													dataSet.get("" + webElement.getAttribute("" + id)));
										} catch (Exception eText) {
											// Reporter.log("Problem in entering
											// TextArea in :
											// "+""+webElement.getAttribute("id"));
										}
									}
								} else if (webElement.getAttribute("type").indexOf("radio") != -1) {
									try {
										String sValue = "";
										List<WebElement> rdBtn_Sex = driver
												.findElements(By.id("" + webElement.getAttribute("" + id))); //
										if (alreadyradio.containsKey(webElement.getAttribute("" + id))) {

										} else {

											int size = rdBtn_Sex.size();
											if (size > 1) {
												sValue = dataSet.get("" + webElement.getAttribute("" + id));
											}
											try {
												List<WebElement> rdBtn_Field = null;
												WebElement element = null;
												try {
													rdBtn_Field = driver.findElements(By.xpath(
															".//*[@name='" + webElement.getAttribute("" + id) + "']")); // work
													element = fc.utobj().getElementByXpath(driver,
															".//*[@name='" + webElement.getAttribute("" + id) + "']");
												} catch (Exception eTxt) {
													rdBtn_Field = driver.findElements(By.xpath(
															".//*[@id='" + webElement.getAttribute("id") + "']")); // work
													element = fc.utobj().getElementByXpath(driver,
															".//*[@id='" + webElement.getAttribute("id") + "']");

												}

												fc.utobj().moveToElement(driver, element);
												fc.utobj().clickRadioButton(driver, rdBtn_Field, sValue); // work
												listItems.add(dataSet.get("" + webElement.getAttribute("" + id)));
												printInfo.put("" + webElement.getAttribute("id"),
														dataSet.get("" + webElement.getAttribute("" + id)));
												alreadyradio.put(webElement.getAttribute("" + id),
														webElement.getAttribute("" + id));
											} catch (Exception eText) {
												// Reporter.log("Problem in
												// selecting radio value in :
												// "+""+webElement.getAttribute("id"));
											}
										}
									} catch (Exception exp) {
										Reporter.log(exp.toString());
										exp.printStackTrace();
									}

								} else if (webElement.getAttribute("type").toLowerCase().indexOf("checkbox") != -1) {
									List<WebElement> oCheckBox = driver
											.findElements(By.name("" + webElement.getAttribute("" + id)));
									String sValue = "";
									if (alreadycheckBox.containsKey(webElement.getAttribute("" + id))) {

									} else {

										int size = oCheckBox.size();
										for (int i = 0; i < size; i++) {
											sValue = oCheckBox.get(i).getAttribute("value");
											//
											if (i == (size - 1)) {

												// break;
											}

										}
										try {
											List<WebElement> rdBtn_Field = driver.findElements(By.xpath(
													"//*[contains(@id, '" + webElement.getAttribute("" + id) + "')]")); // work
											WebElement element = fc.utobj().getElementByXpath(driver,
													"//*[contains(@id, '" + webElement.getAttribute("" + id) + "')]");
											fc.utobj().moveToElement(driver, element);
											fc.utobj().clickRadioButton(driver, rdBtn_Field, sValue); // work
											alreadycheckBox.put(webElement.getAttribute("" + id),
													webElement.getAttribute("" + id));
										} catch (Exception eText) {
											// Reporter.log("Problem in
											// selecting CheckBox value in :
											// "+""+webElement.getAttribute("id"));
										}
									}
								} else if (webElement.getAttribute("type").indexOf("file") != -1) {
									String fileName = fc.utobj().getFilePathFromTestData("pictureFile");
									try {
										fc.utobj().sendKeys(driver, fc.utobj().getElementByID(driver,
												"" + webElement.getAttribute("" + id)), fileName);
										listItems.add(fileName);
										printInfo.put("" + webElement.getAttribute("" + id), fileName);
									} catch (Exception eFile) {
										// Reporter.log("Problem in uploading
										// file type value in :
										// "+""+webElement.getAttribute("id"));
									}
								} else if (webElement.getAttribute("type").indexOf("text") != -1) {
									if (webElement.getAttribute("value") == null
											|| "".equals(webElement.getAttribute("value"))
											|| "null".equals(webElement.getAttribute("value"))
											|| "0.00".equals(webElement.getAttribute("value"))) {
										if (webElement.getAttribute("class").indexOf("fTextBoxDate") != -1) {
											try {
												if (dataSet.get("" + webElement.getAttribute("" + id)) != null
														|| !"".equals(
																dataSet.get("" + webElement.getAttribute("" + id)))
														|| !"null".equals(
																dataSet.get("" + webElement.getAttribute("" + id)))) {
													fc.utobj().sendKeys(driver,
															driver.findElement(
																	By.name("" + webElement.getAttribute("" + id))),
															dataSet.get("" + webElement.getAttribute("" + id))); // work
													listItems.add(dataSet.get("" + webElement.getAttribute("" + id)));
													printInfo.put("" + webElement.getAttribute("" + id),
															dataSet.get("" + webElement.getAttribute("" + id)));

												}
											} catch (Exception eDate) {
												// Reporter.log("Problem in
												// Entering Date fields value in
												// :
												// "+""+webElement.getAttribute("id"));
											}
										} else {
											try {
												if ("0.00".equals(webElement.getAttribute("value"))) {
													fc.utobj().sendKeys(driver,
															driver.findElement(
																	By.name("" + webElement.getAttribute("" + id))),
															"");

													// System.out.println(webElement.getAttribute("value")
													// +"======"+dataSet.get(""+webElement.getAttribute(""+id))
													// +"====="+""+webElement.getAttribute(""+id));

												}
												fc.utobj().sendKeys(driver,
														driver.findElement(
																By.name("" + webElement.getAttribute("" + id))),
														dataSet.get("" + webElement.getAttribute("" + id)));
												if (!"ssn".equals("" + webElement.getAttribute("" + id))
														&& !"spouseSsn".equals("" + webElement.getAttribute("" + id))) {
													listItems.add(dataSet.get("" + webElement.getAttribute("" + id)));
												}
												printInfo.put("" + webElement.getAttribute("" + id),
														dataSet.get("" + webElement.getAttribute("" + id)));

											} catch (Exception eText) {
												// Reporter.log("Problem in
												// Entering Text fileds value in
												// :
												// "+""+webElement.getAttribute("id"));
											}

										}
									}
								}
								alreadyitrateElement.put(webElement.getAttribute("" + id),
										webElement.getAttribute("" + id));
							}
						}
					}
				} catch (Exception ee) {
					ee.printStackTrace();
				}
			}
		} catch (Exception e) {
			fc.utobj().throwsException("Fields data are not available! " + e.getMessage());
		}
		try {
			fc.utobj().printTestStep("Submiting the Tab ");
			WebElement elementbutton = driver
					.findElement(By.xpath(".//*[@id='Submit' or @name='Submit' or @name='button']"));
			fc.utobj().clickElement(driver, elementbutton);

		} catch (Exception ee) {
			fc.utobj().printBugStatus("Unable to submit page " + ee);
		}
		fc.utobj().printTestStep("Verifying The fields in page source.");
		boolean isLeadInfoFound = fc.utobj().assertPageSourceWithMultipleRecords(driver, listItems);
		if (isLeadInfoFound == false) {
			fc.utobj().printBugStatus("Either some fields not matched in page source.");
		}
		return printInfo;
	}

	public Map<String, String> fillFormDataExcel(WebDriver driver, Map<String, String> config, String testCaseId)
			throws Exception {
		Reporter.log("Filling TestCase Data = " + testCaseId);

		String id = "id";
		Map<String, String> dataSet = new HashMap<String, String>();
		dataSet = fc.utobj().readTestData("sales", testCaseId);

		if ("TC_FC_QA_Sale_Lead_QualificationDetails".equals(testCaseId)) {
			id = "name";
		}

		List<String> listItems = new ArrayList<String>();
		Map<String, String> leadInfo = new HashMap<String, String>();

		List<WebElement> mandateBox = null;
		mandateBox = driver.findElements(By.xpath(
				".//*[@qat_tableid='addData']//*[@type='text' or @class='fTextBoxDate' or @class='fTextBox' or @class='multiList' or @type='radio' or @type='checkbox' or @type='checkBox' or @id='ms-parentsellerCombo' or @class='ms-choice' or @class='form-control']"));

		Map<String, String> alreadyitrateElement = new HashMap<>();
		Map<String, String> alreadyradio = new HashMap<>();
		Map<String, String> alreadycheckBox = new HashMap<>();
		String filePath = "C:\\Users\\admin\\Downloads\\" + testCaseId + ".xls";
		try {

			FileOutputStream fout = new FileOutputStream(filePath);
			Workbook wb = new HSSFWorkbook();
			Sheet sh = wb.createSheet();

			CellStyle style = wb.createCellStyle();
			style.setWrapText(true);

			Row r = sh.createRow(0);
			r.setRowStyle(style);

			Cell c1 = r.createCell(0);
			c1.setCellStyle(style);

			c1.setCellValue(testCaseId);

			Row r2 = sh.createRow(1);
			r2.setRowStyle(style);

			int counter = 0;
			for (WebElement webElement : mandateBox) {

				System.out.println("ID==>>" + webElement.getAttribute("id") + "===Name==>>"
						+ webElement.getAttribute("name") + "=====Type===>>>" + webElement.getAttribute("type"));

				if (webElement.getAttribute("" + id) != null && !"".equals(webElement.getAttribute("" + id))
						&& !"null".equals(webElement.getAttribute("" + id))
						&& !":".equals(webElement.getAttribute("" + id))
						&& !"dateOfOpen".equals(webElement.getAttribute("" + id))
						&& !"fimCbOtherComplaintType".equals(webElement.getAttribute("" + id))) {

					// System.out.println("Key==>>"+webElement.getAttribute("id")+"===values==>>"+webElement.getAttribute("value")+"=====Type===>>>"+webElement.getAttribute("type"));
					if (alreadyitrateElement.containsKey(webElement.getAttribute("" + id))) {

					} else {
						Cell c2 = r2.createCell(counter);
						c2.setCellStyle(style);
						sh.setColumnWidth(counter, 10000);
						// System.out.println(""+webElement.getAttribute("id"));
						alreadyitrateElement.put(webElement.getAttribute("" + id), webElement.getAttribute("" + id));
						c2.setCellValue("" + webElement.getAttribute("" + id));
						counter++;
					}
				}

			}
			wb.write(fout);
			wb.close();

		} catch (Exception e) {
			fc.utobj().throwsException("Fields data are not available! " + e.getMessage());
		}

		return leadInfo;
	}

	@Test(groups = { "sales_2525" })
	@TestCase(createdOn = "2017-05-01", updatedOn = "2017-07-07", testCaseId = "TC_FC_QA_Sale_Lead_Primary", testCaseDescription = "Verify Lead All Tab Data with QA input File")
	private void LeadPrimaryInfoOnlyFill() throws Exception {
		String testCaseId = fc.utobj().readTestCaseInfo(this.getClass().getName() + "." + new Object() {
		}.getClass().getEnclosingMethod().getName());

		WebDriver driver = fc.commonMethods().browsers().openBrowser();
		Map<String, String> printInfo = null;
		String testCaseIdInternal = "" + testCaseId;
		try {
			driver = fc.loginpage().login(driver);

			fc.sales().sales_common().fsModule(driver);
			Sales fs = new Sales();
			fs.leadManagement(driver);
			FSLeadSummaryPageTest fsPage = new FSLeadSummaryPageTest();
			fsPage.clickAddLeadLink(driver);
			fc.utobj().printTestStep("Add a lead and Go to Primary Info");

			FSLeadSummaryPrimaryInfoPageTest pInfoPage = new FSLeadSummaryPrimaryInfoPageTest();
			testCaseIdInternal = "TC_FC_QA_Sale_Lead_Primary";
			printInfo = fillFormData(driver, testCaseIdInternal);
			Reporter.log("Map == for =" + testCaseIdInternal + "===" + printInfo);

			/*
			 * fc.utobj().printTestStep(testCaseId, "Go to Personal Profile");
			 * 
			 * FSLeadSummaryPrimaryInfoPage pobjnew = new
			 * FSLeadSummaryPrimaryInfoPage(driver); testCaseIdInternal =
			 * "TC_FC_QA_Sale_Lead_PersonalProfile";
			 * fc.utobj().clickElement(driver, pobjnew.personalProfileTab);
			 * printInfo = fillFormData(driver,config,testCaseIdInternal);
			 * Reporter.log("Map == for ="+testCaseIdInternal+"==="+printInfo);
			 * 
			 * fc.utobj().printTestStep(testCaseId,
			 * "Go to Qualification Details");
			 * 
			 * FSLeadSummaryPrimaryInfoPage pobj = new
			 * FSLeadSummaryPrimaryInfoPage(driver); testCaseIdInternal =
			 * "TC_FC_QA_Sale_Lead_QualificationDetails";
			 * fc.utobj().clickElement(driver, pobj.qualificationDetailsTab);
			 * printInfo = fillFormData(driver,config,testCaseIdInternal);
			 * Reporter.log("Map == for "+testCaseIdInternal+"==="+printInfo);
			 * 
			 * fc.utobj().printTestStep(testCaseId, "Go to Real Estate");
			 * 
			 * testCaseIdInternal="TC_FC_QA_Sale_Lead_RealEstate";
			 * FSLeadSummaryPage pobjreal = new FSLeadSummaryPage(driver);
			 * fc.utobj().printTestStep(testCaseId, "Add real estate");
			 * fc.utobj().clickElement(driver, pobjreal.realEstate); printInfo =
			 * fillFormData(driver,config,testCaseIdInternal); Reporter.log(
			 * "Map == for "+testCaseIdInternal+"==="+printInfo);
			 * 
			 * fc.utobj().printTestStep(testCaseId, "Go to Compliance");
			 * 
			 * testCaseIdInternal="TC_FC_QA_Sale_Lead_Compliance";
			 * fc.utobj().printTestStep(testCaseId, "Add compliance");
			 * fc.utobj().clickElement(driver,pobj.complianceTab); printInfo =
			 * fillFormData(driver,config,testCaseIdInternal); Reporter.log(
			 * "Map == for "+testCaseIdInternal+"==="+printInfo);
			 * 
			 * fc.utobj().printTestStep(testCaseId, "Go to Visit");
			 * 
			 * testCaseIdInternal="TC_FC_QA_Sale_Lead_Visit";
			 * fc.utobj().printTestStep(testCaseId, "Add Visit ");
			 * fc.utobj().clickElement(driver,pobj.visitTab); printInfo =
			 * fillFormData(driver,config,testCaseIdInternal); Reporter.log(
			 * "Map == for "+testCaseId+"==="+printInfo);
			 * 
			 * testCaseIdInternal="TC_FC_QA_Sale_Lead_Document";
			 * fc.utobj().printTestStep(testCaseId, "Add Document  ");
			 * fc.utobj().clickElement(driver,pobj.documentTab); printInfo =
			 * fillFormData(driver,config,testCaseIdInternal); Reporter.log(
			 * "Map == for "+testCaseIdInternal+"==="+printInfo);
			 * 
			 * fc.utobj().printTestStep(testCaseId, "Go to Co-Applicant ");
			 * 
			 * FSLeadSummaryCoApplicantsPage pobjcap = new
			 * FSLeadSummaryCoApplicantsPage(driver);
			 * testCaseIdInternal="TC_FC_QA_Sale_Lead_CoApplicantt";
			 * fc.utobj().printTestStep(testCaseId, "Add Co-Applicant  ");
			 * fc.utobj().clickElement(driver,pobj.coApplicantTab);
			 * fc.utobj().clickElement(driver, pobjcap.addCoApplicantBtn);
			 * printInfo = fillFormData(driver,config,testCaseIdInternal);
			 * Reporter.log("Map == for "+testCaseIdInternal+"==="+printInfo);
			 */

			fc.utobj().logoutAndQuitBrowser(driver, testCaseId);
		} catch (Exception e) {

			fc.utobj().quitBrowserOnCatch(driver, e, testCaseId);
		}
	}

}