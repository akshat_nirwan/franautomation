package com.builds.test.fs;

import org.openqa.selenium.WebDriver;

import com.builds.uimaps.fs.FSLeadSummaryCoApplicantsPage;
import com.builds.utilities.FranconnectUtil;

public class FSLeadSummaryCoApplicantsPageTest {
	FranconnectUtil fc = new FranconnectUtil();

	public void fillCoapplicantDetails(WebDriver driver) throws Exception {
		FSLeadSummaryCoApplicantsPage pobj = new FSLeadSummaryCoApplicantsPage(driver);
		fc.utobj().clickLink(driver, "Co-Applicants");
		fc.utobj().clickElement(driver, pobj.addCoApplicantBtn);
		fc.utobj().sendKeys(driver, pobj.firstName, "CoAppFName");
		fc.utobj().sendKeys(driver, pobj.lastName, "CoAppLName");
		fc.utobj().selectDropDownByVisibleText(driver, pobj.coApplicantRelationshipID, "None");
		fc.utobj().clickElement(driver, pobj.submit);
		boolean isCoAppAdded = fc.utobj().assertPageSource(driver, "CoAppFName");

		if (isCoAppAdded == false) {
			fc.utobj().throwsException("Co Applicant not getting Added");
		} else {
			fc.utobj().clickPartialLinkText(driver, "CoAppFName");
		}
	}
}
